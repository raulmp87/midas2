package mx.com.afirme.midas.wsc.core;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.ResourceBundle;

import org.apache.struts2.json.JSONPopulator;
import org.apache.struts2.json.JSONUtil;
import org.springframework.web.client.RestTemplate;

import mx.com.afirme.midas.wsc.dto.seguridad.LoginParameter;
import mx.com.afirme.midas.wsc.dto.seguridad.LoginResponse;

public class Client {

	private RestTemplate template;
	
	private String token;
	
	private String baseUrl;
	
	private String namespace;
	
	private static final String AUTH_NAMESPACE = "/rest/auth";
	
	private static final String SUFFIX = ".action";
	
	private static final String SEPARATOR = "/";
	
	private final ResourceBundle resourceBundle = ResourceBundle.getBundle("global");
		
	public Client(String baseUrl, String namespace) {
		
		super();
		
		this.template = new RestTemplate();
		
		this.baseUrl =  baseUrl;

		this.namespace = namespace;
		
		//authenticates App user
		this.token = getToken();
		
	}
	
	protected <T> T invoke(String methodName, Object paramObj, T responseObject) {
		
		return invoke(methodName, this.namespace, paramObj, responseObject);
		
	}
		

	@SuppressWarnings({ "rawtypes", "unchecked" })
	private <T extends Object> T invoke(String methodName, String namespace, Object paramObj, T responseObject) {
		
		try {
			
			StringBuilder sb = new StringBuilder();
			
			sb.append(serializeToKeyValuePairs(paramObj));
			
			if (this.token != null) {
				
				sb.append((sb.length() > 0?"&":"?"))
					.append("token=")
					.append(this.token);
				
			}
						
			String params = sb.toString();
			
			sb = new StringBuilder();
			
			sb.append(this.baseUrl)
				.append(namespace)
				.append(SEPARATOR)
				.append(methodName)
				.append(SUFFIX);
			
			String actualUrl = sb.append(params).toString();
			
			Response response = template.getForObject(actualUrl, Response.class);
			
			if (response.getSuccess().equals("false")) {
							
				ErrorBuilder eb = new ErrorBuilder();
				
				eb.setErrors(response.getActionErrors());
				eb.setFieldErrors(response.getFieldErrors());
				
				throw new ApplicationException(eb);
				
			}
			
			if (responseObject.getClass().isPrimitive() || !response.getData().getValue().startsWith("{")) {
				
				return (T) responseObject.getClass().getConstructor(String.class).newInstance(response.getData().getValue());
				
			}
			
			JSONPopulator populator = new JSONPopulator();
						
			populator.populateObject(responseObject, (HashMap) JSONUtil.deserialize(response.getData().getValue()));
			
			return responseObject;
								
		} catch (Exception e) {

			throw new RuntimeException(e);
			
		}
		
	}
	
	
	private String getToken() {
		
		String user = resourceBundle.getString("app.username"); 
		
		String password = resourceBundle.getString("app.password"); 
		
		// invoke /rest/auth/login.action and retrieve token
		
		LoginParameter paramObj = new LoginParameter();
		
		paramObj.setUsuario(user);
		paramObj.setPassword(password);
		paramObj.setApplicationId(0);
				
		LoginResponse result = invoke("login", AUTH_NAMESPACE, paramObj, new LoginResponse());
		
		return result.getToken();
		
	}
	
	private String serializeToKeyValuePairs (Object obj) throws IllegalArgumentException, IllegalAccessException {
				
		if (obj == null) return "";
		
		StringBuilder sb = new StringBuilder().append("?");
		
		List<Field> fields = getAllFields(new ArrayList<Field>(), obj.getClass());
		
		for (Field field : fields) {
			if (!Modifier.isFinal(field.getModifiers()) && !field.getName().startsWith("_")) {
								
				if (sb.length() > 1) {
					
		            sb.append("&");
		            
		        }
		        
				field.setAccessible(true);
				
				sb.append(field.getName()).append("=").append(field.get(obj));
										
			}
		}
				
		return sb.toString();
		
	}
	
	private List<Field> getAllFields(List<Field> fields, Class<?> type) {
	    fields.addAll(Arrays.asList(type.getDeclaredFields()));

	    if (type.getSuperclass() != null) {
	        fields = getAllFields(fields, type.getSuperclass());
	    }

	    return fields;
	}
	
	
}
