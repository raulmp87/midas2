package mx.com.afirme.midas.wsc.core;

import java.io.IOException;

import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.ObjectCodec;
import org.codehaus.jackson.map.DeserializationContext;
import org.codehaus.jackson.map.JsonDeserializer;

public class ResponseDataDeserializer extends JsonDeserializer<ResponseData> {
   	
	@Override
    public ResponseData deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
        ObjectCodec oc = jsonParser.getCodec();
        JsonNode node = oc.readTree(jsonParser);
        
        String data = node.toString();
        
        ResponseData responseData = new ResponseData();
        
        responseData.setValue(data);
    		   
        return responseData; 
    	
    }
		
}