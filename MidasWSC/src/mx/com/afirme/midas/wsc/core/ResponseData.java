package mx.com.afirme.midas.wsc.core;

import org.codehaus.jackson.map.annotate.JsonDeserialize;

@JsonDeserialize(using = ResponseDataDeserializer.class)
public class ResponseData {

	String value;
	
	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

}
