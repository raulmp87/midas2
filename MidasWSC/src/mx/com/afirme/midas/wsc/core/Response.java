package mx.com.afirme.midas.wsc.core;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Response {
		
	private String success;
	
	private String mensaje;
	
	private Object id;
		
	private ResponseData data;
		
	private List<String> actionErrors = new ArrayList<String>();
	
	private Map<String, List<String>> fieldErrors = new HashMap<String, List<String>>();
		
	public String getSuccess() {
		return success;
	}

	public void setSuccess(String success) {
		this.success = success;
	}

	public String getMensaje() {
		return mensaje;
	}

	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}

	public Object getId() {
		return id;
	}

	public void setId(Object id) {
		this.id = id;
	}

	public ResponseData getData() {
		return data;
	}

	public void setData(ResponseData data) {
		this.data = data;
	}

	public List<String> getActionErrors() {
		return actionErrors;
	}

	public void setActionErrors(List<String> actionErrors) {
		this.actionErrors = actionErrors;
	}

	public Map<String, List<String>> getFieldErrors() {
		return fieldErrors;
	}

	public void setFieldErrors(Map<String, List<String>> fieldErrors) {
		this.fieldErrors = fieldErrors;
	}
		

}
