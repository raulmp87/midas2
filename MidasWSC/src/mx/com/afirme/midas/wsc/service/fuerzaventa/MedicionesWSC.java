package mx.com.afirme.midas.wsc.service.fuerzaventa;

import mx.com.afirme.midas.wsc.core.Client;
import mx.com.afirme.midas.wsc.dto.fuerzaventa.mediciones.ReporteResponse;
import mx.com.afirme.midas.wsc.dto.seguridad.UserParameter;

public class MedicionesWSC extends Client {

	private static final String NAMESPACE = "/rest/fuerzaventa/mediciones";
	
	public MedicionesWSC(String baseUrl) {
		
		super(baseUrl, NAMESPACE);
		
	}
	
	public ReporteResponse reporteSemanal(int userId) {
		
		UserParameter userParameter = new UserParameter();
		 
		userParameter.setId(userId);		
		
		return invoke("reporteSemanal", userParameter, new ReporteResponse());
		
	}
	
	
	public Boolean esUsuarioValido (int userId) {
		
		UserParameter userParameter = new UserParameter();
		 
		userParameter.setId(userId);
		
		return invoke("esUsuarioValido", userParameter, new Boolean(false));
		
	}
	
}
