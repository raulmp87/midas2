package mx.com.afirme.midas.wsc.dto.seguridad;

public class Rol implements java.io.Serializable {

	private static final long serialVersionUID = 1L;
	private Integer id;
	private String descripcion;

	
	public Rol() {

	}
	
	public Rol(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}
	
	public String toString() {
		return this.id.toString();
	}
	
	/**
	 * @return the id
	 */
	public Integer getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		this.id = id;
	}
	/**
	 * @return the descripcion
	 */
	public String getDescripcion() {
		return descripcion;
	}
	/**
	 * @param descripcion the descripcion to set
	 */
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
}
