package mx.com.afirme.midas.wsc.dto.seguridad;

import java.util.List;

public class LoginResponse {		
	public static class Usuario {
		private Integer id;
		private String nombreUsuario;
		private Ajustador ajustador;
		private List<Rol> roles;
		private String nombreCompleto;
		private String nombre;
		private String apellidoPaterno;
		private String apellidoMaterno;
		private String cellPhone;
		private String email;
		private String promoCode;
		private String deviceUuid;
		
		public Integer getId() {
			return id;
		}
		public void setId(Integer id) {
			this.id = id;
		}
		public String getNombreUsuario() {
			return nombreUsuario;
		}
		public void setNombreUsuario(String nombreUsuario) {
			this.nombreUsuario = nombreUsuario;
		}
		public Ajustador getAjustador() {
			return ajustador;
		}
		public void setAjustador(Ajustador ajustador) {
			this.ajustador = ajustador;
		}
		public void setRoles(List<Rol> roles) {
			this.roles = roles;
		}
		public List<Rol> getRoles() {
			return roles;
		}
		public String getNombreCompleto() {
			return nombreCompleto;
		}
		public void setNombreCompleto(String nombreCompleto) {
			this.nombreCompleto = nombreCompleto;
		}
		public String getNombre() {
			return nombre;
		}
		public void setNombre(String nombre) {
			this.nombre = nombre;
		}
		public String getApellidoPaterno() {
			return apellidoPaterno;
		}
		public void setApellidoPaterno(String apellidoPaterno) {
			this.apellidoPaterno = apellidoPaterno;
		}
		public String getApellidoMaterno() {
			return apellidoMaterno;
		}
		public void setApellidoMaterno(String apellidoMaterno) {
			this.apellidoMaterno = apellidoMaterno;
		}
		public String getCellPhone() {
			return cellPhone;
		}
		public void setCellPhone(String cellPhone) {
			this.cellPhone = cellPhone;
		}
		public String getEmail() {
			return email;
		}
		public void setEmail(String email) {
			this.email = email;
		}
		public String getPromoCode() {
			return promoCode;
		}
		public void setPromoCode(String promoCode) {
			this.promoCode = promoCode;
		}
		public String getDeviceUuid() {
			return deviceUuid;
		}
		public void setDeviceUuid(String deviceUuid) {
			this.deviceUuid = deviceUuid;
		}			
	}
	
	private Usuario usuario;
	private String token;
	public Usuario getUsuario() {
		return usuario;
	}
	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
	public String getToken() {
		return token;
	}
	public void setToken(String token) {
		this.token = token;
	}
}
