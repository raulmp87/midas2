package mx.com.afirme.midas.wsc.dto.fuerzaventa.mediciones;

import java.io.Serializable;

public class ReporteResponse implements Serializable {
	
	private static final long serialVersionUID = 7189735333089571945L;

	private String fileName;
	private String contentType;
	private String corte; //representa la fecha de corte en un formato YYYYMMDD
	private byte[] fileBytes;
		
	
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getContentType() {
		return contentType;
	}
	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	public String getCorte() {
		return corte;
	}
	public void setCorte(String corte) {
		this.corte = corte;
	}	
	public byte[] getFileBytes() {
		return fileBytes;
	}
	public void setFileBytes(byte[] fileBytes) {
		this.fileBytes = fileBytes;
	}
			
}
