package mx.com.afirme.midas.wsc.dto.seguridad;

public class LoginParameter {
	private String usuario;
	private String password;
	private Integer applicationId;
	private String ipAddress;
	private String registrationId;
	private String deviceUuid;
	private String deviceApplicationId;
	private boolean portalUser = false;
		
	public String getUsuario() {
		return usuario;
	}
	
	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}
		
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Integer getApplicationId() {
		return applicationId;
	}
	
	public void setApplicationId(Integer applicationId) {
		this.applicationId = applicationId;
	}
		
	public String getIpAddress() {
		return ipAddress;
	}
	
	public void setIpAddress(String ipAddress) {
		this.ipAddress = ipAddress;
	}
	
	/**
	 * Google Cloud Messaging Registration Id
	 * @return
	 */
	public String getRegistrationId() {
		return registrationId;
	}
	
	/**
	 * Google Cloud Messaging Registration Id
	 * @param registrationId
	 */
	public void setRegistrationId(String registrationId) {
		this.registrationId = registrationId;
	}
	
	public String getDeviceUuid() {
		return deviceUuid;
	}
	
	public void setDeviceUuid(String deviceUuid) {
		this.deviceUuid = deviceUuid;
	}
	
	public String getDeviceApplicationId() {
		return deviceApplicationId;
	}
	
	public void setDeviceApplicationId(String deviceApplicationId) {
		this.deviceApplicationId = deviceApplicationId;
	}
	
	public boolean isPortalUser() {
		return portalUser;
	}
	
	public void setPortalUser(boolean portalUser) {
		this.portalUser = portalUser;
	}
	
}