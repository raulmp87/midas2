package mx.com.afirme.midas.wsc.dto.seguridad;

import java.io.Serializable;

public class Ajustador implements Serializable {

	private static final long serialVersionUID = 8892639276618528703L;
	
	private Long id;
	private String nombre;
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNombre() {
		return nombre;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
}
