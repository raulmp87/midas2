#!/bin/bash
echo Exporting environments
export ANT_OPTS="-Xms768m -Xmx1024m -XX:MaxPermSize=512m"
export JAVA_HOME="/opt/IBM/WebSphere/AppServer/java"
echo ANT_OPTS=$ANT_OPTS
echo JAVA_HOME=$JAVA_HOME
echo Done!
