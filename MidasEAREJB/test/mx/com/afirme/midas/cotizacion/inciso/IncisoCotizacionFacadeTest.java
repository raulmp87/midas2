package mx.com.afirme.midas.cotizacion.inciso;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;

import org.junit.Before;
import org.junit.Test;

public class IncisoCotizacionFacadeTest {
	IncisoCotizacionFacade tested;
	CoberturaCotizacionDTO coberturaCotizacionDTO;
	CoberturaCotizacionId coberturaCotizacionId;

	@Before
	public void setUp() throws Exception {
		tested = new IncisoCotizacionFacade();
	}

	@Test
	public void testObtenerIdCobertura_oneparameter() {
		coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		coberturaCotizacionId = new CoberturaCotizacionId();
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
		coberturaCotizacionId.setIdToCobertura(BigDecimal.valueOf(123));
		coberturaCotizacionDTO.setId(coberturaCotizacionId);
		coberturas.add(coberturaCotizacionDTO);
		assertEquals("123", tested.obtenerIdCobertura(coberturas));
	}
	@Test
	public void testObtenerIdCobertura_twoparameters() {
		coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		coberturaCotizacionId = new CoberturaCotizacionId();
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
		coberturaCotizacionId.setIdToCobertura(BigDecimal.valueOf(123));
		coberturaCotizacionDTO.setId(coberturaCotizacionId);
		coberturas.add(coberturaCotizacionDTO);
		coberturaCotizacionDTO = new CoberturaCotizacionDTO();
		coberturaCotizacionId = new CoberturaCotizacionId();
		coberturaCotizacionId.setIdToCobertura(BigDecimal.valueOf(321));
		coberturaCotizacionDTO.setId(coberturaCotizacionId);
		coberturas.add(coberturaCotizacionDTO);
		assertEquals("123,321", tested.obtenerIdCobertura(coberturas));
	}
	@Test
	public void testObtenerIdCobertura_nullparameters() {
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>();
		assertEquals("", tested.obtenerIdCobertura(coberturas));
	}
}
