package mx.com.afirme.midas.cotizacion.inciso.cargamasiva;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CargaMasivaCotFacadeTest {
	CargaMasivaCotFacade tested;

	@Before
	public void setUp() throws Exception {
		tested = new CargaMasivaCotFacade();
	}

	@Test
	public void testObtenerid() {
	assertEquals("00001", tested.obtenerId("1"));
	}
	
	@Test
	public void testObtenerid_5() {
	assertEquals("00001", tested.obtenerId("00001"));
	}
	
	@Test
	public void testObtenerid_mayor5() {
	assertEquals("0000001", tested.obtenerId("0000001"));
	}

}