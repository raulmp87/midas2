package mx.com.afirme.midas.cotizacion.cobertura;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class CoberturaCotizacionFacadeTest {

	CoberturaCotizacionFacade tested;
	@Before
	public void setUp() throws Exception {
		tested = new CoberturaCotizacionFacade();
	}

	@Test
	public void testObtenerIds() {
		List<BigDecimal> idsCoberturas = new ArrayList<BigDecimal>();
		idsCoberturas.add(BigDecimal.valueOf(1));
		idsCoberturas.add(BigDecimal.valueOf(2));
		idsCoberturas.add(BigDecimal.valueOf(3));
		idsCoberturas.add(BigDecimal.valueOf(4));
		assertEquals("1,2,3,4", tested.obtenerIds(idsCoberturas ));

	}
	@Test
	public void testObtenerIdsUno() {
		List<BigDecimal> idsCoberturas = new ArrayList<BigDecimal>();
		idsCoberturas.add(BigDecimal.valueOf(1));
		assertEquals("1", tested.obtenerIds(idsCoberturas ));

	}

	@Test
	public void testObtenerIds_null() {
		List<BigDecimal> idsCoberturas = new ArrayList<BigDecimal>();
		
		assertEquals("", tested.obtenerIds(idsCoberturas ));

	}


}
