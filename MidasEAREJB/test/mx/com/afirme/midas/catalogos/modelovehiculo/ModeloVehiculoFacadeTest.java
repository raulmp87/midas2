package mx.com.afirme.midas.catalogos.modelovehiculo;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoId;

import org.junit.Before;
import org.junit.Test;

public class ModeloVehiculoFacadeTest {
	ModeloVehiculoFacade tested;
	SeccionTipoVehiculoDTO seccionTipoVehiculoDTO;
	SeccionTipoVehiculoId seccionTipoVehiculoId;

	@Before
	public void setUp() throws Exception {
		tested = new ModeloVehiculoFacade();
		}

	@Test
	public void testObtenerQueryTipoVehiculo_oneparameter() {
		seccionTipoVehiculoDTO = new SeccionTipoVehiculoDTO();
		seccionTipoVehiculoId = new SeccionTipoVehiculoId();
		seccionTipoVehiculoId.setIdTcTipoVehiculo(BigDecimal.valueOf(123));
		seccionTipoVehiculoDTO.setId(seccionTipoVehiculoId);
		List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList = new ArrayList<SeccionTipoVehiculoDTO>();
		seccionTipoVehiculoList.add(seccionTipoVehiculoDTO);
		assertEquals(" AND EV.IDTCTIPOVEHICULO IN (123) ", tested.obtenerQueryTipoVehiculo(seccionTipoVehiculoList));

	}
	
	@Test
	public void testObtenerQueryTipoVehiculo_twoparameters() {
		seccionTipoVehiculoDTO = new SeccionTipoVehiculoDTO();
		seccionTipoVehiculoId = new SeccionTipoVehiculoId();
		List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList = new ArrayList<SeccionTipoVehiculoDTO>();
		seccionTipoVehiculoId.setIdTcTipoVehiculo(BigDecimal.valueOf(123));
		seccionTipoVehiculoDTO.setId(seccionTipoVehiculoId);
		seccionTipoVehiculoList.add(seccionTipoVehiculoDTO);
		seccionTipoVehiculoDTO = new SeccionTipoVehiculoDTO();
		seccionTipoVehiculoId = new SeccionTipoVehiculoId();
		seccionTipoVehiculoId.setIdTcTipoVehiculo(BigDecimal.valueOf(321));
		seccionTipoVehiculoDTO.setId(seccionTipoVehiculoId);
		seccionTipoVehiculoList.add(seccionTipoVehiculoDTO);
		assertEquals(" AND EV.IDTCTIPOVEHICULO IN (123, 321) ", tested.obtenerQueryTipoVehiculo(seccionTipoVehiculoList));

	}
	@Test
	public void testObtenerQueryTipoVehiculo_nullparameters() {
		List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList = new ArrayList<SeccionTipoVehiculoDTO>();
		assertEquals(" AND EV.IDTCTIPOVEHICULO IN () ", tested.obtenerQueryTipoVehiculo(seccionTipoVehiculoList));

	}

}
