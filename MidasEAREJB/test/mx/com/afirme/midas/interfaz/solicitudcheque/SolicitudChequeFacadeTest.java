package mx.com.afirme.midas.interfaz.solicitudcheque;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SolicitudChequeFacadeTest {
	SolicitudChequeFacade sol;

	@Before
	public void setUp() throws Exception {
		sol = new SolicitudChequeFacade();
	}

	@Test
	public void testObtenerDescripcionParametros() {
		String[] nombreParametros = {
				"pId_Pago_Midas","pTrans_cont","pId_moneda",
				"pTipo_Cambio","pId_prest_serv","pBeneficiario",
				"pTipo_pago","pConcepto_pol","pUsuario"};
		Object[] valoresParametros = {
				1,12,2,
				3,4,5,
				6,"ejemplopoliza","ejemplo usuario"};
	String re="pId_Pago_Midas: 1, pTrans_cont: 12, pId_moneda: 2, pTipo_Cambio: 3, pId_prest_serv: 4, pBeneficiario: 5, pTipo_pago: 6, pConcepto_pol: ejemplopoliza, pUsuario: ejemplo usuario";
		assertEquals(re, sol.obtenerDescripcionParametros(nombreParametros, valoresParametros));
	}

	@Test
	public void testObtenerDescripcionParametros_valorvacio() {
		String[] nombreParametros = {
				"pId_Pago_Midas","pTrans_cont","pId_moneda",
				"pTipo_Cambio","pId_prest_serv","pBeneficiario",
				"pTipo_pago","pConcepto_pol","pUsuario"};
		Object[] valoresParametros = {"","","","","","","","","",};
	String re="pId_Pago_Midas: , pTrans_cont: , pId_moneda: , pTipo_Cambio: , pId_prest_serv: , pBeneficiario: , pTipo_pago: , pConcepto_pol: , pUsuario: ";
		assertEquals(re, sol.obtenerDescripcionParametros(nombreParametros, valoresParametros));
	}
	
	@Test
	public void testObtenerDescripcionParametros_vacio() {
		String[] nombreParametros = {
				"","","",
				"","","",
				"","",""};
		Object[] valoresParametros = {
				1,12,2,
				3,4,5,
				6,"ejemplopoliza","ejemplo usuario"};
	String re=": 1, : 12, : 2, : 3, : 4, : 5, : 6, : ejemplopoliza, : ejemplo usuario";
		assertEquals(re, sol.obtenerDescripcionParametros(nombreParametros, valoresParametros));
	}
	
	@Test
	public void testObtenerDescripcionParametros_vacios() {
		String[] nombreParametros = {};
		Object[] valoresParametros = {};
	String re="";
		assertEquals(re, sol.obtenerDescripcionParametros(nombreParametros, valoresParametros));
	}
}
