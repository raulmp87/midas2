package mx.com.afirme.midas.interfaz.cliente;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;


public class ClienteFacadeTest {
	ClienteFacade tested;

	@Before
	public void setUp() throws Exception {
		tested = new ClienteFacade();
	}

	@Test
	public void testObtenerId_menosdecinco() {
		assertEquals("00001",tested.obtenerId("001"));
	}

	@Test
	public void testObtenerId_masdecinco() {
		assertEquals("001009",tested.obtenerId("001009"));
	}
	
	@Test
	public void testObtenerId_cinco() {
		assertEquals("00100",tested.obtenerId("00100"));
	}
	@Test
	public void testObtenerId_null() {
		assertEquals("00000",tested.obtenerId(""));
	}

}
