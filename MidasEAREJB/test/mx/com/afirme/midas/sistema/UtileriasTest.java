package mx.com.afirme.midas.sistema;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class UtileriasTest {
	private Utilerias tested;
	
	@Before
	public void setUp(){
		tested = new Utilerias();
	}

	@Test
	public void testLlenarIzquierda() {
		String coberturaCompuesta = "uno,dos,tres";
		assertEquals("-uno,dos,tres", tested.llenarIzquierda(coberturaCompuesta, "-", coberturaCompuesta.length()+1));
	}
	@Test
	public void testLlenarIzquierda_null() {
		String coberturaCompuesta = "";
		assertEquals("...", tested.llenarIzquierda(coberturaCompuesta, ".", 3));
	}
	
	@Test
	public void testLlenarIzquierda_MasDos() {
		String coberturaCompuesta = "uno,dos,tres";
		assertEquals("----uno,dos,tres", tested.llenarIzquierda(coberturaCompuesta, "-", coberturaCompuesta.length()+4));
	}

	@Test
	public void testLlenarIzquierda_MenosDos() {
		String coberturaCompuesta = "uno,dos,tres";
		assertEquals("uno,dos,tres", tested.llenarIzquierda(coberturaCompuesta, "-", coberturaCompuesta.length()-4));
	}
	@Test
	public void testLlenarIzquierda_otro() {
		String coberturaCompuesta = "uno,dos,tres";
		assertEquals("uno,dos,tres", tested.llenarIzquierda(coberturaCompuesta, "-", 2));
	}
}
