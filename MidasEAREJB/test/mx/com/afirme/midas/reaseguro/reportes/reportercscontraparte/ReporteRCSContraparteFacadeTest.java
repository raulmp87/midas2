package mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ReporteRCSContraparteFacadeTest {
	private ReporteRCSContraparteFacade tested;
	ReporteRCSContraparteDTO reporteRCSContraparteDTO;
	
	@Before
	public void setUp(){
	 tested = new ReporteRCSContraparteFacade();
	}
	@Test
	public void testObtenerDescripcionParametros() {
		String[] nombreParametros = {"fechaCorte", "reporte"};
		Object[] valorParametros={"16/43/2016", 1000};
		assertEquals(" [fechaCorte] : 16/43/2016 [reporte] : 1000", tested.obtenerDescripcionParametros(nombreParametros, valorParametros));

	}
	@Test
	public void testObtenerDescripcionParametros_null() {
		String[] nombreParametros = {};
		Object[] valorParametros={};
		assertEquals("", tested.obtenerDescripcionParametros(nombreParametros, valorParametros));

	}
	
	@Test
	public void testObtieneResultado() {
		List<ReporteRCSContraparteDTO> listaResultado = new ArrayList<ReporteRCSContraparteDTO>();
		reporteRCSContraparteDTO = new ReporteRCSContraparteDTO();
		reporteRCSContraparteDTO.setResultado("Ejemplo 1");
		listaResultado.add(reporteRCSContraparteDTO);
		reporteRCSContraparteDTO = new ReporteRCSContraparteDTO();
		reporteRCSContraparteDTO.setResultado("Ejemplo 2");
		listaResultado.add(reporteRCSContraparteDTO);
		reporteRCSContraparteDTO = new ReporteRCSContraparteDTO();
		reporteRCSContraparteDTO.setResultado("Ejemplo 3");
		listaResultado.add(reporteRCSContraparteDTO);

	}
	@Test
	public void testObtieneResultado_uno() {
		List<ReporteRCSContraparteDTO> listaResultado = new ArrayList<ReporteRCSContraparteDTO>();
		reporteRCSContraparteDTO = new ReporteRCSContraparteDTO();
		reporteRCSContraparteDTO.setResultado("Ejemplo 1");
		listaResultado.add(reporteRCSContraparteDTO);

	}
	

}
