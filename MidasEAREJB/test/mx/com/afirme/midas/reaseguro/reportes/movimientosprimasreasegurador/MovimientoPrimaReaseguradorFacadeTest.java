package mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreasegurador;

import static org.junit.Assert.*;
import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle.MovimientoPrimaReaseguradorDetalleDTO;

import org.junit.Before;
import org.junit.Test;

public class MovimientoPrimaReaseguradorFacadeTest {
private MovimientoPrimaReaseguradorFacade movimiento;
	
	@Before
	public void setUp(){
		movimiento = new MovimientoPrimaReaseguradorFacade();
	}
	@Test
	public void testObtenerDescripcionParametro(){
		final String [] nombreParametrosSP = {"pFechaInicial","pFechaFinal","pIdToPoliza","pIdMoneda","pIdRamo","pIdSubRamo"};
		MovimientoPrimaReaseguradorDTO movimientoPrimaReaseguradorDTO = new MovimientoPrimaReaseguradorDTO();
		final Object [] parametrosSP = {
				((movimientoPrimaReaseguradorDTO .getFechaInicioVigencia()==null)?"":movimientoPrimaReaseguradorDTO.getFechaInicioVigencia()),
				((movimientoPrimaReaseguradorDTO.getFechaFinVigencia()==null)?"":movimientoPrimaReaseguradorDTO.getFechaFinVigencia()),
				((movimientoPrimaReaseguradorDTO.getIdToPoliza()==null)?"":movimientoPrimaReaseguradorDTO.getIdToPoliza()),
				((movimientoPrimaReaseguradorDTO.getIdMoneda()==null)?"":movimientoPrimaReaseguradorDTO.getIdMoneda()),
				((movimientoPrimaReaseguradorDTO.getIdRamo()==null)?"":movimientoPrimaReaseguradorDTO.getIdRamo()),
				((movimientoPrimaReaseguradorDTO.getIdSubRamo()==null)?"":movimientoPrimaReaseguradorDTO.getIdSubRamo())
		};
			assertEquals("(pFechaInicial: , pFechaFinal: , pIdToPoliza: , pIdMoneda: , pIdRamo: , pIdSubRamo: , )", movimiento.obtenerDescripcionParametros(nombreParametrosSP, parametrosSP));
			//assertEquals("...", movimiento.obtenerMovimientosPrimaDetalladosPorReasegurador(movimientoPrimaReaseguradorDetalleDTO, "GRAMIBA"));

}
	@Test
	public void testObtenerDescripcionParametro_null() {
	final String nombreParametrosSP[] = {};
			MovimientoPrimaReaseguradorDetalleDTO movimientoPrimaReaseguradorDetalleDTO=new MovimientoPrimaReaseguradorDetalleDTO();
			final Object[] parametrosSP ={	};
			assertEquals("()", movimiento.obtenerDescripcionParametros(nombreParametrosSP, parametrosSP));
			//assertEquals("...", movimiento.obtenerMovimientosPrimaDetalladosPorReasegurador(movimientoPrimaReaseguradorDetalleDTO, "GRAMIBA"));

}

}
