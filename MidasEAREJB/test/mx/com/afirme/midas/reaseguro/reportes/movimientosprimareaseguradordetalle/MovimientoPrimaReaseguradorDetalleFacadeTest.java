package mx.com.afirme.midas.reaseguro.reportes.movimientosprimareaseguradordetalle;

import static org.junit.Assert.*;
import mx.com.afirme.midas.reaseguro.reportes.movimientosprimasreaseguradordetalle.MovimientoPrimaReaseguradorDetalleDTO;

import org.junit.Before;
import org.junit.Test;

public class MovimientoPrimaReaseguradorDetalleFacadeTest {

	private MovimientoPrimaReaseguradorDetalleFacade tested;
	
	@Before
	public void setUp(){
		tested = new MovimientoPrimaReaseguradorDetalleFacade();
	}
	@Test
	public void testObtenerDescripcionParametro(){
	final String nombreParametrosSP[] = {"pFechaInicial","pFechaFinal","pIdToPoliza","pIdMoneda","pIdRamo","pIdSubRamo","pRetencion"};
			MovimientoPrimaReaseguradorDetalleDTO movimientoPrimaReaseguradorDetalleDTO=new MovimientoPrimaReaseguradorDetalleDTO();
			final Object[] parametrosSP ={
					(movimientoPrimaReaseguradorDetalleDTO.getFechaInicioVigencia()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getFechaInicioVigencia(),
					(movimientoPrimaReaseguradorDetalleDTO.getFechaFinVigencia()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getFechaFinVigencia(),
					(movimientoPrimaReaseguradorDetalleDTO.getIdToPoliza()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getIdToPoliza(),
					(movimientoPrimaReaseguradorDetalleDTO.getIdMoneda()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getIdMoneda(),
					(movimientoPrimaReaseguradorDetalleDTO.getIdRamo()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getIdRamo(),
					(movimientoPrimaReaseguradorDetalleDTO.getIdSubRamo()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getIdSubRamo(),
					(movimientoPrimaReaseguradorDetalleDTO.getRetencion()==null)?"":movimientoPrimaReaseguradorDetalleDTO.getRetencion()
			};
			assertEquals("(pFechaInicial: , pFechaFinal: , pIdToPoliza: , pIdMoneda: , pIdRamo: , pIdSubRamo: , pRetencion: , )", tested.obtenerDescripcionParametro(nombreParametrosSP, parametrosSP));
			//assertEquals("...", movimiento.obtenerMovimientosPrimaDetalladosPorReasegurador(movimientoPrimaReaseguradorDetalleDTO, "GRAMIBA"));

}
	@Test
	public void testObtenerDescripcionParametro_null() {
	final String nombreParametrosSP[] = {};
			MovimientoPrimaReaseguradorDetalleDTO movimientoPrimaReaseguradorDetalleDTO=new MovimientoPrimaReaseguradorDetalleDTO();
			final Object[] parametrosSP ={	};
			assertEquals("()", tested.obtenerDescripcionParametro(nombreParametrosSP, parametrosSP));
			//assertEquals("...", movimiento.obtenerMovimientosPrimaDetalladosPorReasegurador(movimientoPrimaReaseguradorDetalleDTO, "GRAMIBA"));

}
}
