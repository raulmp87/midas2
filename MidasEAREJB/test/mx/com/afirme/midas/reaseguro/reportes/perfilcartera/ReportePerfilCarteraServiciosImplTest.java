package mx.com.afirme.midas.reaseguro.reportes.perfilcartera;

import static org.junit.Assert.*;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import org.junit.Before;
import org.junit.Test;

public class ReportePerfilCarteraServiciosImplTest {
	private ReportePerfilCarteraServiciosImpl reporte;
	

	@Before
	public void setUp(){
	reporte = new ReportePerfilCarteraServiciosImpl();
	
	}
	
	@Test
	public void testObtenerDdescripcionParametros() throws ParseException {
		java.util.Date fecha = new java.util.Date();
		SimpleDateFormat formatoDeFecha = new SimpleDateFormat("dd/MM/yyyy");
		String fechaString= formatoDeFecha.format(fecha);
		final String [] nombreParametrosSP = {"pFechaInicial","pFechaFinal","pTipoCambio","pTipoReporte"};
		final Object [] parametrosSP = {formatoDeFecha.format(fecha),formatoDeFecha.format(fecha),12.4,123};
	String resul="(pFechaInicial: "+ fechaString +", pFechaFinal: "+fechaString+", pTipoCambio: 12.4, pTipoReporte: 123, )";
		assertEquals(resul, reporte.obtenerDescripcionParametros(nombreParametrosSP, parametrosSP));

	}
	@Test
	public void testObtenerDdescripcionParametros_null() throws ParseException {
		final String [] nombreParametrosSP = {};
		final Object [] parametrosSP = {};
	
		assertEquals("()", reporte.obtenerDescripcionParametros(nombreParametrosSP, parametrosSP));

	}

}
