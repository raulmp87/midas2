package mx.com.afirme.midas.reaseguro.soporte;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class LineaSoporteReaseguroFacadeTest {
	LineaSoporteReaseguroFacade tested;

	@Before
	public void setUp() throws Exception {
		tested = new LineaSoporteReaseguroFacade();
	}

	@Test
	public void testObtenerDescripcionIds() {
		List<BigDecimal> listaIdTmLineaSoporteReaseguro = new ArrayList<BigDecimal>();
		listaIdTmLineaSoporteReaseguro.add(BigDecimal.valueOf(1));
		listaIdTmLineaSoporteReaseguro.add(BigDecimal.valueOf(2));
		listaIdTmLineaSoporteReaseguro.add(BigDecimal.valueOf(3));
		assertEquals("1,2,3", tested.obtenerDescripcionIds(listaIdTmLineaSoporteReaseguro));

	}
	
	@Test
	public void testObtenerDescripcionIds_null() {
		List<BigDecimal> listaIdTmLineaSoporteReaseguro = new ArrayList<BigDecimal>();
		assertEquals("", tested.obtenerDescripcionIds(listaIdTmLineaSoporteReaseguro));

	}

}
