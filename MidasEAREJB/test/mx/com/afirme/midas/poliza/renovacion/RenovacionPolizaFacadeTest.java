package mx.com.afirme.midas.poliza.renovacion;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.siniestro.cabina.ReporteSiniestroDTO;

import org.junit.Before;
import org.junit.Test;

public class RenovacionPolizaFacadeTest {
	RenovacionPolizaFacade tested;
	ReporteSiniestroDTO reporteSiniestroDTO;

	@Before
	public void setUp() throws Exception {
		tested  = new RenovacionPolizaFacade();
	}

	@Test
	public void testObtenerReportesString() {
	List<ReporteSiniestroDTO> reportes = new ArrayList<ReporteSiniestroDTO>();
	reporteSiniestroDTO = new ReporteSiniestroDTO();
	reporteSiniestroDTO.setNumeroReporte("1");
	reportes.add(reporteSiniestroDTO);
	reporteSiniestroDTO = new ReporteSiniestroDTO();
	reporteSiniestroDTO.setNumeroReporte("2");
	reportes.add(reporteSiniestroDTO);
	reporteSiniestroDTO = new ReporteSiniestroDTO();
	reporteSiniestroDTO.setNumeroReporte("3");
	reportes.add(reporteSiniestroDTO);
	assertEquals("| 1 | 2 | 3 | ", tested.obtenerReportesString(reportes));
	}

	@Test
	public void testObtenerReportesString_null() {
	List<ReporteSiniestroDTO> reportes = new ArrayList<ReporteSiniestroDTO>();
	assertEquals("| ", tested.obtenerReportesString(reportes));
	}
}
