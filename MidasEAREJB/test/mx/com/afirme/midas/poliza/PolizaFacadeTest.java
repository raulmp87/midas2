package mx.com.afirme.midas.poliza;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;

import org.junit.Before;
import org.junit.Test;

public class PolizaFacadeTest {
	PolizaFacade tested;
	SeccionDTO seccionDTO;

	@Before
	public void setUp() throws Exception {
		 tested = new PolizaFacade();
		 
	}

	@Test
	public void testObtenerIds() {
		List<SeccionDTO> seccionList = new ArrayList<SeccionDTO>();
		seccionDTO = new SeccionDTO();
		seccionDTO.setIdToSeccion(BigDecimal.ZERO);
		seccionList.add(seccionDTO);
		seccionDTO = new SeccionDTO();
		seccionDTO.setIdToSeccion(BigDecimal.ONE);
		seccionList.add(seccionDTO);
		assertEquals("0,1",tested.obtenerIds(seccionList));
	}
	

	@Test
	public void testObtenerIds_null() {
		List<SeccionDTO> seccionList = new ArrayList<SeccionDTO>();
		assertEquals("",tested.obtenerIds(seccionList));
	}

}
