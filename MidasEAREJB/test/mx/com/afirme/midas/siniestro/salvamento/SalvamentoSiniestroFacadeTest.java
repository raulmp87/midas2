package mx.com.afirme.midas.siniestro.salvamento;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class SalvamentoSiniestroFacadeTest {
	SalvamentoSiniestroFacade salvamentos;

	@Before
	public void setUp() throws Exception {
		salvamentos  = new SalvamentoSiniestroFacade();
	}

	@Test
	public void testObtenerValoresEstatus() {
		Object[] params = {
				1,2,
				3,"ejemplo"};
		
		assertEquals("1,2,3,ejemplo,", salvamentos.obtenerValoresEstatus(params));

	}
	

	@Test
	public void testObtenerValoresEstatus_null() {
		Object[] params = {};
		
		assertEquals("", salvamentos.obtenerValoresEstatus(params));

	}

}
