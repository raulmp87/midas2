package mx.com.afirme.midas.danios.reportes.reportebasesemision;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ReporteBasesEmisionFacadeTest {
	
	ReporteBasesEmisionFacade reporte;

	@Before
	public void setUp() throws Exception {
		reporte = new ReporteBasesEmisionFacade();
	}

	@Test
	public void testObtenerDescripcionParametros() {
		
		String[] nombreParametros = {"pFechaInicio", 
				"pFechaFin",
				"pIdRamo",
				"pIdSubRamo",
				"pNivelContratoReaseguro",
				"pNivelAgrupamiento"};
		Object[] valorParametros={"02/02/2015",
				"02/02/2016",
			2,
			3,
			1,
			3};
		String re= ", [pFechaInicio] : 02/02/2015, [pFechaFin] : 02/02/2016, [pIdRamo] : 2, [pIdSubRamo] : 3, [pNivelContratoReaseguro] : 1, [pNivelAgrupamiento] : 3";
		
		assertEquals(re, reporte.obtenerDescripcionParametros(nombreParametros, valorParametros));

	}
	@Test
	public void testObtenerDescripcionParametros_null() {
		
		String[] nombreParametros = {};
		Object[] valorParametros={};
		String re= "";
		
		assertEquals(re, reporte.obtenerDescripcionParametros(nombreParametros, valorParametros));

	}

}
