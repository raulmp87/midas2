package mx.com.afirme.midas.danios.reportes.reporterr6;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ReporteRR6FacadeTest {
	
	ReporteRR6Facade reporte;

	@Before
	public void setUp() throws Exception {
		reporte = new ReporteRR6Facade();
	}

	@Test
	public void testObtenerDescripcionParametros() {
		String[] nombreParametros = {"pfechainicial","pfechafinal","ptipocambio", "pidUsuario"};
		Object[] valorParametros={"02/02/2015", "02/02/2016", 1, 2};
	String re = " [pfechainicial] : 02/02/2015 [pfechafinal] : 02/02/2016 [ptipocambio] : 1 [pidUsuario] : 2";
		assertEquals(re, reporte.obtenerDescripcionParametros(nombreParametros, valorParametros));
	}
	
	@Test
	public void testObtenerDescripcionParametros_null() {
		String[] nombreParametros = {};
		Object[] valorParametros={};
	String re = "";
		assertEquals(re, reporte.obtenerDescripcionParametros(nombreParametros, valorParametros));
	}


}
