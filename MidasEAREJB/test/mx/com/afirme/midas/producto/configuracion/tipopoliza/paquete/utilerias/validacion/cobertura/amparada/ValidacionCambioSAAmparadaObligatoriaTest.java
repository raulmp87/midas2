package mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.utilerias.validacion.cobertura.amparada;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.paquete.PaquetePolizaFacade;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;

import org.junit.Before;
import org.junit.Test;

public class ValidacionCambioSAAmparadaObligatoriaTest {
	ValidacionCambioSAAmparadaObligatoria tested;
	CoberturaSeccionDTO CoberturaSeccionDTO;
	CoberturaDTO CoberturaDTO;
	@Before
	public void setUp() throws Exception {
		PaquetePolizaFacade paquetePolizaFacade = null;
		tested = new ValidacionCambioSAAmparadaObligatoria(paquetePolizaFacade);
	}

	@Test
	public void testObtenerDescripcionListaCoberturas() {
		CoberturaSeccionDTO = new CoberturaSeccionDTO();
		CoberturaDTO = new CoberturaDTO();
		List<CoberturaSeccionDTO> listaCoberturas = new ArrayList<CoberturaSeccionDTO>();
		CoberturaDTO.setNombreComercial("ejemplo");
		CoberturaSeccionDTO.setCoberturaDTO(CoberturaDTO);
		listaCoberturas.add(CoberturaSeccionDTO);
		CoberturaSeccionDTO = new CoberturaSeccionDTO();
		CoberturaDTO = new CoberturaDTO();
		CoberturaDTO.setNombreComercial("ejemplo2");
		CoberturaSeccionDTO.setCoberturaDTO(CoberturaDTO);
		listaCoberturas.add(CoberturaSeccionDTO);
		assertEquals("EJEMPLO, EJEMPLO2", ValidacionCambioSAAmparadaObligatoria.obtenerDescripcionListaCoberturas(listaCoberturas));

	}
	
	@Test
	public void testObtenerDescripcionListaCoberturas_null() {
		List<CoberturaSeccionDTO> listaCoberturas = new ArrayList<CoberturaSeccionDTO>();
		
		assertEquals("", ValidacionCambioSAAmparadaObligatoria.obtenerDescripcionListaCoberturas(listaCoberturas));

	}

}
