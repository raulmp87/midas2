package mx.com.afirme.midas.contratos.linea;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ConfiguracionLineaFacadeTest {

	ConfiguracionLineaFacade tested;
	@Before
	public void setUp() throws Exception {
		tested = new ConfiguracionLineaFacade();
	}

	@Test
	public void testObtenerInSQL() {
		List<String> idLineas =new ArrayList<String>();
		idLineas.add("lista 1");
		idLineas.add("lista 2");
		idLineas.add("lista 3");
		idLineas.add("lista 4");
		assertEquals("lista 1,lista 2,lista 3,lista 4", tested.obtenerInSQL(idLineas));

	}

	
	@Test
	public void testObtenerInSQL_vacio() {
		List<String> idLineas =new ArrayList<String>();
		
		assertEquals("", tested.obtenerInSQL(idLineas));

	}

}
