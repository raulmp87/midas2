package mx.com.afirme.midas.contratos.movimiento;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class MovimientoReaseguroServiciosTest {
	MovimientoReaseguroServicios tested;

	@Before
	public void setUp() throws Exception {
		tested = new MovimientoReaseguroServicios();
	}

	@Test
	public void testObtenerIdToMovimientosString() {
		List<BigDecimal> listaIdToMovimientos = new ArrayList<BigDecimal>();
		listaIdToMovimientos.add(BigDecimal.ZERO);
		listaIdToMovimientos.add(BigDecimal.ONE);
		listaIdToMovimientos.add(BigDecimal.TEN);
		assertEquals("0,1,10", tested.obtenerIdToMovimientosString(listaIdToMovimientos));
	}
	@Test
	public void testObtenerIdToMovimientosString_one() {
		List<BigDecimal> listaIdToMovimientos = new ArrayList<BigDecimal>();
		listaIdToMovimientos.add(BigDecimal.ONE);
		assertEquals("1", tested.obtenerIdToMovimientosString(listaIdToMovimientos));
	}
	
	@Test
	public void testObtenerIdToMovimientosString_vaciol() {
		List<BigDecimal> listaIdToMovimientos = new ArrayList<BigDecimal>();
		assertEquals("0", tested.obtenerIdToMovimientosString(listaIdToMovimientos));
	}

	@Test
	public void testObtenerIdToMovimientosString_null() {
		List<BigDecimal> listaIdToMovimientos = new ArrayList<BigDecimal>();
		listaIdToMovimientos.add(null);
		assertEquals("0", tested.obtenerIdToMovimientosString(listaIdToMovimientos));
	}
	
	@Test
	public void testObtenerIdMovimientos() {
		List<Object> lista = new ArrayList <Object> ();
		lista.add(BigDecimal.ONE);
		lista.add(2L);
		lista.add(Double.valueOf(3.1));
		lista.add("4");
		lista.add(5.0f);
		assertEquals("1,2,3,4,5", tested.obtenerIdMovimientos(lista.iterator()));
	}
	@Test
	public void testObtenerIdMovimientos_null() {
		List<Object> lista = new ArrayList <Object> ();
		assertEquals("0", tested.obtenerIdMovimientos(lista.iterator()));
	}
	
	
}