package mx.com.afirme.midas.contratos.estadocuenta;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EstadoCuentaFacadeTest {
	EstadoCuentaFacade tested;

	@Before
	public void setUp() throws Exception {
		tested = new EstadoCuentaFacade();
	}

	@Test
	public void testObtenerIdsEdoCta() {
		String[] idsEdosCta = {"1","2","3"			
		};
		assertEquals("1,2,3,", tested.obtenerIdsEdoCta(idsEdosCta));

	}
	

	@Test
	public void testObtenerIdsEdoCta_null() {
		String[] idsEdosCta = {	};
		assertEquals("", tested.obtenerIdsEdoCta(idsEdosCta));

	}
	@Test
	public void obtenerReaseguroStr() {
		int[] tiposReaseguro =  {1,2,3};
		assertEquals("1,2,3,", tested.obtenerReaseguroStr(tiposReaseguro));

	}
	
	@Test
	public void obtenerReaseguroStr_null() {
		int[] tiposReaseguro =  {};
		assertEquals("", tested.obtenerReaseguroStr(tiposReaseguro));

	}

}
