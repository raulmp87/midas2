package mx.com.afirme.midas.contratos.estadocuenta.reportes;

import static org.junit.Assert.*;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;

public class ReporteEstadoCuentaServiciosTest {
	
	ReporteEstadoCuentaServicios reporte;

	@Before
	public void setUp() throws Exception {
		reporte = new ReporteEstadoCuentaServicios();
	}

	@Test
	public void testObtenerListaSubRamos() {
		BigDecimal[] idTcSubRamos = { BigDecimal.valueOf(1), BigDecimal.valueOf(2),BigDecimal.valueOf(3),BigDecimal.valueOf(4)};
		assertEquals("1,2,3,4", reporte.obtenerListaSubRamos(idTcSubRamos));
	}
	@Test
	public void testObtenerListaSubRamos_null() {
		BigDecimal[] idTcSubRamos = { };
		assertEquals("", reporte.obtenerListaSubRamos(idTcSubRamos));
	}

}
