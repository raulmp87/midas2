package mx.com.afirme.midas.contratos.estadocuenta.presentacion;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class PresentacionEstadoCuentaServiciosTest {
	PresentacionEstadoCuentaServicios presentacion;

	@Before
	public void setUp() throws Exception {
		presentacion =  new PresentacionEstadoCuentaServicios();
	}

	@Test
	public void testObtenerDescripcionSubramos() {
		BigDecimal[] idTcSubRamos = {
				BigDecimal.valueOf(1),BigDecimal.valueOf(2),BigDecimal.valueOf(3)				
		};
		assertEquals("1,2,3,",presentacion.obtenerDescripcionSubramos(idTcSubRamos));

	}
	
	@Test
	public void testObtenerDescripcionSubramos_null() {
		BigDecimal[] idTcSubRamos = {};
		assertEquals("",presentacion.obtenerDescripcionSubramos(idTcSubRamos));

	}
	
	@Test
	public void testObtenerMesePorIncluir_onedate() {
		List<Date> mesesPorIncluir = new ArrayList<Date>();
		Date object = new Date();
		Calendar calendar = Calendar.getInstance();
		mesesPorIncluir.add(object);
		assertEquals(" (model.mes = "+(calendar.get(Calendar.MONTH)+1)+" and model.anio = "+calendar.get(Calendar.YEAR)+") ",presentacion.obtenerMesesPorIncluir(mesesPorIncluir));
	}
	
	@Test
	public void testObtenerMesePorIncluir_twodate() {
		List<Date> mesesPorIncluir = new ArrayList<Date>();
		Date object = new Date();

		@SuppressWarnings("deprecation")
		java.util.Date fechaMenor = new java.util.Date(111, 03, 15, 8, 15, 23);
		mesesPorIncluir.add(object);
		mesesPorIncluir.add(fechaMenor);
		Calendar calendar = Calendar.getInstance();
		assertEquals(" (model.mes = "+(calendar.get(Calendar.MONTH)+1)+" and model.anio = "+calendar.get(Calendar.YEAR)+")  or  (model.mes = 4 and model.anio = 2011) ",presentacion.obtenerMesesPorIncluir(mesesPorIncluir));

	}
	@Test
	public void testObtenerMesePorIncluir_nulldate() {
		List<Date> mesesPorIncluir = new ArrayList<Date>();
		assertEquals("",presentacion.obtenerMesesPorIncluir(mesesPorIncluir));

	}

}
