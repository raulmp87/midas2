package mx.com.afirme.midas2.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CommonUtilsTest {

	private CommonUtils tested;
	
	@Before
	public void setUp(){
		tested = new CommonUtils();
	}

	@Test
	public void testIsValid() {
		assertTrue(CommonUtils.isValid("1"));
	}
	
	@Test
	public void testIsValid_null() {
		assertFalse(CommonUtils.isValid(null));
	}
	
	@Test
	public void testIsValid_empty() {
		assertFalse(CommonUtils.isValid(""));
	}
	
	@Test
	public void testIsValid_spaces() {
		assertFalse(CommonUtils.isValid("  "));
	}

}
