package mx.com.afirme.midas2.utils;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ConvertidorImplTest {

	private ConvertidorImpl tested;

	@Before
	public void setUp(){
		tested = new ConvertidorImpl();
	}
	@Test
	public void testNuevoAtributoMapeo() {
		String[] mapeo =new String[4];
		mapeo[0]="ejemplo1";
		mapeo[1]="ejemplo2";
		mapeo[2]="ejemplo3";
		mapeo[3]="ejemplo4";

		assertEquals("ejemplo2.ejemplo3.ejemplo4", tested.obtenerNuevoAtributoMapeo(mapeo));
	}
	@Test
	public void testNuevoAtributoMapeo_null() {
		String[] mapeo =new String[0];
		assertEquals("", tested.obtenerNuevoAtributoMapeo(mapeo));
	}


}
