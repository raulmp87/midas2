package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CedulaSubramo;

import org.junit.Before;
import org.junit.Test;

public class ClienteAgenteDaoImplTest {
	private ClienteAgenteDaoImpl tested;
	CedulaSubramo cedula;
	SubRamoDTO subramoDTO;

	@Before
	public void setUp() throws Exception {
		tested = new ClienteAgenteDaoImpl();
		cedula = new CedulaSubramo();
		subramoDTO = new SubRamoDTO();
	}

	@Test
	public void testGetSubramosAescluir() {
	subramoDTO.setIdTcSubRamo(BigDecimal.valueOf(12345));
		cedula.setSubramoDTO(subramoDTO);
		List<CedulaSubramo> subramosExtra = new ArrayList<CedulaSubramo>();
		subramosExtra.add(cedula);
		assertEquals("12345",tested.getSubramosAescluir(subramosExtra));

	}

	@Test
	public void testGetSubramosAescluir_null() {
		List<CedulaSubramo> subramosExtra = new ArrayList<CedulaSubramo>();
			assertEquals("",tested.getSubramosAescluir(subramosExtra));

	}
	@Test
	public void testGetIdClientesAexcluir() {
		List<BigDecimal> idsclientes =  new ArrayList<BigDecimal>();
		idsclientes.add(BigDecimal.valueOf(12));
		idsclientes.add(BigDecimal.valueOf(123));
		assertEquals("12,123", tested.getIdClientesAexcluir(idsclientes));
	}
	@Test
	public void testGetIdClientesAexcluir_null() {
		List<BigDecimal> idsclientes =  new ArrayList<BigDecimal>();
		assertEquals("", tested.getIdClientesAexcluir(idsclientes));
	}

}
