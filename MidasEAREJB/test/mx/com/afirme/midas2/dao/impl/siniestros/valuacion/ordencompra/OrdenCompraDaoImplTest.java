package mx.com.afirme.midas2.dao.impl.siniestros.valuacion.ordencompra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.util.WriterUtil;

import org.junit.Before;
import org.junit.Test;

public class OrdenCompraDaoImplTest {
	private OrdenCompraDaoImpl tested;


	@Before
	public void setUp(){
		tested = new OrdenCompraDaoImpl();
	}
	@Test
	public void testgetCoberturaCompuesta() {
		String coberturaCompuesta = "uno,dos,tres";
		assertEquals("'uno','dos','tres'", tested.getCoberturaCompuesta(coberturaCompuesta));
	}
	@Test
	public void testgetCoberturaCompuesta_onevalue() {
		String coberturaCompuesta = "uno,";
		assertEquals("'uno'", tested.getCoberturaCompuesta(coberturaCompuesta));
	}
	@Test
	public void testgetCoberturaCompuesta_two_value() {
			String coberturaCompuesta = "uno,dos";
		assertEquals("'uno','dos'", tested.getCoberturaCompuesta(coberturaCompuesta));
	}

	@Test
	public void testgetPasesAtencion_threelong() {
		List<Long> param =new ArrayList<Long>();
		param.add(Long.valueOf(1));
		param.add(Long.valueOf(2));
		param.add(Long.valueOf(3));
		assertEquals("'1','2','3'", tested.getPasesAtencion(param));
	}
	@Test
	public void testgetPasesAtencion_twolong() {
		List<Long> param =new ArrayList<Long>();
		param.add(Long.valueOf(1));
		param.add(Long.valueOf(2));
		assertEquals("'1','2'", tested.getPasesAtencion(param));
	}
	@Test
	public void testgetPasesAtencion_onelong() {
		List<Long> param =new ArrayList<Long>();
		param.add(Long.valueOf(1));
		assertEquals("'1'", tested.getPasesAtencion(param));
	}
}
