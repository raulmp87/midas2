package mx.com.afirme.midas2.dao.impl.persona;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class PersonaSeycosFacadeTest {
	PersonaSeycosFacade tested;

	@Before
	public void setUp() throws Exception {
		tested = new PersonaSeycosFacade();
	}

	@Test
	public void testObtenerId_menosdecinco() {
		assertEquals("00001",tested.obtenerId("001"));
	}

	@Test
	public void testObtenerId_masdecinco() {
		assertEquals("001009",tested.obtenerId("001009"));
	}
	
	@Test
	public void testObtenerId_cinco() {
		assertEquals("00100",tested.obtenerId("00100"));
	}
	@Test
	public void testObtenerId_null() {
		assertEquals("00000",tested.obtenerId(""));
	}

}
