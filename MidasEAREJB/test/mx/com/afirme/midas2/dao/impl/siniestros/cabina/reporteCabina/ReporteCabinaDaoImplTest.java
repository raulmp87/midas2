package mx.com.afirme.midas2.dao.impl.siniestros.cabina.reporteCabina;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;

import org.junit.Before;
import org.junit.Test;

public class ReporteCabinaDaoImplTest {
	
	ReporteCabinaDaoImpl reporte;

	@Before
	public void setUp() throws Exception {
		reporte = new ReporteCabinaDaoImpl();
	}

	@Test
	public void testGetInCodUsuario() {
		List<String> param =new ArrayList<String>();
		param.add("lista 1");
		param.add("lista 2");
		param.add("lista 3");
		param.add("lista 4");
		assertEquals("'lista 1','lista 2','lista 3','lista 4'",reporte.getInCodUsuario(param));

	}
	
	@Test
	public void testGetInCodUsuario_null() {
		List<String> param =new ArrayList<String>();
		assertEquals(null,reporte.getInCodUsuario(param));

	}

}
