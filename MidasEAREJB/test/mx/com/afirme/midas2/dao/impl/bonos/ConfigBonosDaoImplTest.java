package mx.com.afirme.midas2.dao.impl.bonos;

import static org.junit.Assert.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.bonos.ConfigBonoCentroOperacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoProducto;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRamo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSeccion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

import org.junit.Before;
import org.junit.Test;

public class ConfigBonosDaoImplTest {
	ConfigBonosDaoImpl tested;
	ConfigBonoSeccion configBonoSeccion;
	ConfigBonoRamo configBonoRamo;
	ConfigBonoProducto configBonoProducto;
	ValorCatalogoAgentes valorCatalogoAgentes;
	ConfigBonoPoliza configBonoPoliza;
	ConfigBonos configBonos;
	ConfigBonoCentroOperacion configBonoCentroOperacion;

	@Before
	public void setUp() throws Exception {
		tested = new ConfigBonosDaoImpl();
	}


	@Test
	public void testObtenerIdNegocios() {
		List<ConfigBonoSeccion> lineasNegocio = new ArrayList<ConfigBonoSeccion>();
		configBonoSeccion = new ConfigBonoSeccion();
		configBonoSeccion.setIdSeccion(1L);
		lineasNegocio.add(configBonoSeccion);
		configBonoSeccion = new ConfigBonoSeccion();
		configBonoSeccion.setIdSeccion(2L);
		lineasNegocio.add(configBonoSeccion);
		configBonoSeccion = new ConfigBonoSeccion();
		configBonoSeccion.setIdSeccion(3L);
		lineasNegocio.add(configBonoSeccion);
		assertEquals("3,2,1,", tested.obtenerIdNegocios(lineasNegocio));
	}	
	@Test
	public void testObtenerIdNegocios_uno() {
		List<ConfigBonoSeccion> lineasNegocio = new ArrayList<ConfigBonoSeccion>();
		configBonoSeccion = new ConfigBonoSeccion();
		configBonoSeccion.setIdSeccion(1L);
		lineasNegocio.add(configBonoSeccion);
		assertEquals("1,", tested.obtenerIdNegocios(lineasNegocio));
	}
	@Test
	public void testObtenerIdNegocios_vacio() {
		List<ConfigBonoSeccion> lineasNegocio = new ArrayList<ConfigBonoSeccion>();
		assertEquals("", tested.obtenerIdNegocios(lineasNegocio));
	}
	
	
	@Test
	public void testObtenerIdRamos() {
		List<ConfigBonoRamo> ramos = new ArrayList<ConfigBonoRamo>();
		configBonoRamo = new ConfigBonoRamo();
		configBonoRamo.setIdRamo(1L);
		ramos.add(configBonoRamo);
		configBonoRamo = new ConfigBonoRamo();
		configBonoRamo.setIdRamo(2L);
		ramos.add(configBonoRamo);
		configBonoRamo = new ConfigBonoRamo();
		configBonoRamo.setIdRamo(3L);
		ramos.add(configBonoRamo);
		assertEquals("3,2,1,", tested.obtenerIdRamos(ramos));
	}
	@Test
	public void testObtenerIdRamos_uno() {
		List<ConfigBonoRamo> ramos = new ArrayList<ConfigBonoRamo>();
		configBonoRamo = new ConfigBonoRamo();
		configBonoRamo.setIdRamo(1L);
		ramos.add(configBonoRamo);
		assertEquals("1,", tested.obtenerIdRamos(ramos));
	}
	@Test
	public void testObtenerIdRamos_vacio() {
		List<ConfigBonoRamo> ramos = new ArrayList<ConfigBonoRamo>();
		assertEquals("", tested.obtenerIdRamos(ramos));
	}
	
	@Test
	public void testObtenerIdProductos() {
		List<ConfigBonoProducto> productos = new ArrayList<ConfigBonoProducto>();
		configBonoProducto = new ConfigBonoProducto();
		configBonoProducto.setIdProducto(1L);
		productos.add(configBonoProducto);
		configBonoProducto = new ConfigBonoProducto();
		configBonoProducto.setIdProducto(2L);
		productos.add(configBonoProducto);
		configBonoProducto = new ConfigBonoProducto();
		configBonoProducto.setIdProducto(3L);
		productos.add(configBonoProducto);
		assertEquals("3,2,1,", tested.obtenerIdProductos(productos));
	}
	@Test
	public void testObtenerIdProductos_uno() {
		List<ConfigBonoProducto> productos = new ArrayList<ConfigBonoProducto>();
		configBonoProducto = new ConfigBonoProducto();
		configBonoProducto.setIdProducto(1L);
		productos.add(configBonoProducto);
		assertEquals("1,", tested.obtenerIdProductos(productos));
	}
	@Test
	public void testObtenerIdProductos_vacio() {
		List<ConfigBonoProducto> productos = new ArrayList<ConfigBonoProducto>();
		assertEquals("", tested.obtenerIdProductos(productos));
	}

	@Test
	public void testObetenerIdsSeleccionado() {
		List<ValorCatalogoAgentes> listaLineaVenta = new ArrayList<ValorCatalogoAgentes>();
		valorCatalogoAgentes = new ValorCatalogoAgentes();
		valorCatalogoAgentes.setClave("clave 1");
		valorCatalogoAgentes.setChecado(1);
		listaLineaVenta.add(valorCatalogoAgentes);
		valorCatalogoAgentes = new ValorCatalogoAgentes();
		valorCatalogoAgentes.setClave("clave 2");
		valorCatalogoAgentes.setChecado(1);
		listaLineaVenta.add(valorCatalogoAgentes);
		valorCatalogoAgentes = new ValorCatalogoAgentes();
		valorCatalogoAgentes.setClave("clave 3");
		valorCatalogoAgentes.setChecado(1);
		listaLineaVenta.add(valorCatalogoAgentes);
		assertEquals("clave 3,clave 2,clave 1,", tested.obtenerIdsSeleccionados(listaLineaVenta));
	}
	@Test
	public void testObetenerIdsSeleccionado_uno() {
		List<ValorCatalogoAgentes> listaLineaVenta = new ArrayList<ValorCatalogoAgentes>();
		valorCatalogoAgentes = new ValorCatalogoAgentes();
		valorCatalogoAgentes.setClave("clave 1");
		valorCatalogoAgentes.setChecado(0);
		listaLineaVenta.add(valorCatalogoAgentes);
		valorCatalogoAgentes = new ValorCatalogoAgentes();
		valorCatalogoAgentes.setClave("clave 3");
		valorCatalogoAgentes.setChecado(1);
		listaLineaVenta.add(valorCatalogoAgentes);
		assertEquals("clave 3,", tested.obtenerIdsSeleccionados(listaLineaVenta));
	}
	@Test
	public void testObetenerIdsSeleccionado_null() {
		List<ValorCatalogoAgentes> listaLineaVenta = new ArrayList<ValorCatalogoAgentes>();
		assertEquals("", tested.obtenerIdsSeleccionados(listaLineaVenta));
	}
	
	@Test
	public void testObtenerIdsCotizacion() {
			List<ConfigBonoPoliza> lista = new ArrayList<ConfigBonoPoliza>();
			configBonoPoliza = new ConfigBonoPoliza();
			configBonoPoliza.setIdCotizacion(BigDecimal.ZERO);
			lista.add(configBonoPoliza);
			configBonoPoliza = new ConfigBonoPoliza();
			configBonoPoliza.setIdCotizacion(BigDecimal.ONE);
			lista.add(configBonoPoliza);
			configBonoPoliza = new ConfigBonoPoliza();
			configBonoPoliza.setIdCotizacion(BigDecimal.TEN);
			lista.add(configBonoPoliza);
			assertEquals("10,1,0", tested.obtenerIdsCotizacion(lista));
	}
	@Test
	public void testObtenerIdsCotizacion_uno() {
			List<ConfigBonoPoliza> lista = new ArrayList<ConfigBonoPoliza>();
			configBonoPoliza = new ConfigBonoPoliza();
			configBonoPoliza.setIdCotizacion(BigDecimal.ZERO);
			lista.add(configBonoPoliza);
			assertEquals("0", tested.obtenerIdsCotizacion(lista));
	}
	@Test
	public void testObtenerIdsCotizacion_null() {
			List<ConfigBonoPoliza> lista = new ArrayList<ConfigBonoPoliza>();
			assertEquals("", tested.obtenerIdsCotizacion(lista));
	}
	
	@Test
	public void testObtenerIdsPoliza_masdeuno() {
		List<ConfigBonoPoliza> listaConfigPolizas = new ArrayList<ConfigBonoPoliza>();
		configBonoPoliza = new ConfigBonoPoliza();
		configBonoPoliza.setIdPoliza(BigDecimal.ZERO);
		listaConfigPolizas.add(configBonoPoliza);
		configBonoPoliza = new ConfigBonoPoliza();
		configBonoPoliza.setIdPoliza(BigDecimal.ONE);
		listaConfigPolizas.add(configBonoPoliza);
		configBonos = new ConfigBonos();
		configBonos.setListaConfigPolizas(listaConfigPolizas);
			assertEquals("1,0", tested.obtenerIdsPoliza(configBonos));
	}
	@Test
	public void testObtenerIdsPoliza_uno() {
		List<ConfigBonoPoliza> listaConfigPolizas = new ArrayList<ConfigBonoPoliza>();
		configBonoPoliza = new ConfigBonoPoliza();
		configBonoPoliza.setIdPoliza(BigDecimal.ZERO);
		listaConfigPolizas.add(configBonoPoliza);
		configBonos = new ConfigBonos();
		configBonos.setListaConfigPolizas(listaConfigPolizas);
			assertEquals("0", tested.obtenerIdsPoliza(configBonos));
	}
	@Test
	public void testObtenerIdsPoliza_null() {
		List<ConfigBonoPoliza> listaConfigPolizas = new ArrayList<ConfigBonoPoliza>();
		configBonos = new ConfigBonos();
		configBonos.setListaConfigPolizas(listaConfigPolizas);
			assertEquals("", tested.obtenerIdsPoliza(configBonos));
	}
	

}
