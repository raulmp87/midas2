package mx.com.afirme.midas2.service.impl.siniestros.cabina.reportecabina;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class EstimacionCoberturaSiniestroServiceImplTest {
	EstimacionCoberturaSiniestroServiceImpl estimacion;

	@Before
	public void setUp() throws Exception {
		estimacion = new EstimacionCoberturaSiniestroServiceImpl();
	}

	@Test
	public void testObtenerMontoRechazo() {
		String [] montosRC={"ejemplo1","ejemplo2","ejemplo3"};
		assertEquals("ejemplo1|ejemplo2|ejemplo3|",estimacion.obtenerMontoRechazo(montosRC));

	}
	
	@Test
	public void testObtenerMontoRechazo_null() {
		String [] montosRC={};
		assertEquals("",estimacion.obtenerMontoRechazo(montosRC));

	}

}
