package mx.com.afirme.midas2.service.impl.informacionVehicular;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta;

import org.junit.Before;
import org.junit.Test;

public class InformacionVehicularLotesServiceImplTest {
	InformacionVehicularLotesServiceImpl tested;
	Respuesta respuesta;

	@Before
	public void setUp() throws Exception {
		tested = new InformacionVehicularLotesServiceImpl();
	}

	@Test
	public void testObtenerCadenaVin() {
		List<Respuesta> listaVin = new ArrayList<Respuesta>();
		respuesta = new Respuesta();
		respuesta.setVIN("Ejemplovin1");
		listaVin.add(respuesta);
		respuesta = new Respuesta();
		respuesta.setVIN("Ejemplovin2");
		listaVin.add(respuesta);
		respuesta = new Respuesta();
		respuesta.setVIN("Ejemplovin3");
		listaVin.add(respuesta);
		assertEquals("Ejemplovin1,Ejemplovin2,Ejemplovin3", tested.obtenerCadenaVin(listaVin));
	}
	@Test
	public void testObtenerCadenaVin_null() {
		List<Respuesta> listaVin = new ArrayList<Respuesta>();
		assertEquals("", tested.obtenerCadenaVin(listaVin));
	}

}
