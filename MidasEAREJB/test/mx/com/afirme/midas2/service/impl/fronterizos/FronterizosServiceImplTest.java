package mx.com.afirme.midas2.service.impl.fronterizos;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class FronterizosServiceImplTest {
	FronterizosServiceImpl fronterizos;
	
	@Before
	public void setUp(){
	fronterizos = new FronterizosServiceImpl();
	}

	@Test
	public void testTraerError() {
		List<String> errores =new ArrayList<String>();
		errores.add("error 1");
		errores.add("error 2");
		errores.add("error 3");
		errores.add("error 4");
		String resul="error 1error 2error 3error 4";
		assertEquals(resul, fronterizos.traerError(errores));

	}
	
	@Test
	public void testTraerError_uno() {
		List<String> errores =new ArrayList<String>();
		errores.add("error 1");
		String resul="error 1";
		assertEquals(resul, fronterizos.traerError(errores));

	}
	@Test
	public void testTraerError_null() {
		List<String> errores =new ArrayList<String>();
		assertEquals("", fronterizos.traerError(errores));

	}

}
