package mx.com.afirme.midas2.service.impl.suscripcion.solicitud.impresiones;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class GeneraPlantillaCartaRenovacionServiceImplTest {

	GeneraPlantillaCartaRenovacionServiceImpl tested;
	@Before
	public void setUp() throws Exception {
		tested = new GeneraPlantillaCartaRenovacionServiceImpl();
	}

	@Test
	public void testObtenerBanco() {
		String[] parts={"ejemplo1","ejemplo2"	};
		assertEquals("Ejemplo1 Ejemplo2 ", tested.obtenerBanco(parts));

	}
	

	@Test
	public void testObtenerBanco_null() {
		String[] parts={};
		assertEquals("", tested.obtenerBanco(parts));
		

	}
	
	@Test
	public void testObtenerBancos() {
		String[] parts = {
				"banco1", "banco2", "banco3"
				
		};
		assertEquals("Banco1 o Banco2, Banco3", tested.obtenerBancos(parts));

	}
	
	@Test
	public void testObtenerBancos_null() {
		String[] parts = {};
		assertEquals("", tested.obtenerBancos(parts));

	}
	

}
