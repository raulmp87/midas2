package mx.com.afirme.midas2.service.impl.emision.consulta;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;

import org.junit.Before;
import org.junit.Test;

public class ConsultaEmisionServiceImplTest {
	ConsultaEmisionServiceImpl tested;
	Agente agente;

	@Before
	public void setUp() throws Exception {
		tested = new ConsultaEmisionServiceImpl();
	}

	@Test
	public void testFormatoFiltroAutomaticoAgentes() {
		List<Agente> agentes = new ArrayList<Agente>();
		agente = new Agente();
		agente.setIdAgente(1L);
		agentes.add(agente);
		agente = new Agente();
		agente.setIdAgente(2L);
		agentes.add(agente);
		assertEquals("1,2",tested.formatoFiltroAutomaticoAgentes(agentes));
	}
	
	@Test
	public void testFormatoFiltroAutomaticoAgentes_null() {
		List<Agente> agentes = new ArrayList<Agente>();
		assertEquals(null,tested.formatoFiltroAutomaticoAgentes(agentes));
	}

}
