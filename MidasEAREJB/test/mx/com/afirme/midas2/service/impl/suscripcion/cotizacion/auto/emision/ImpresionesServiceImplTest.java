package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.emision;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;

import org.junit.Before;
import org.junit.Test;

public class ImpresionesServiceImplTest {
	ImpresionesServiceImpl tested;
	Agente agente;
	Gerencia gerencia;
	CentroOperacion centroOperacion;
	Promotoria promotoria;
	Ejecutivo ejecutivo;

	@Before
	public void setUp() throws Exception {
		tested = new ImpresionesServiceImpl();
	}
	

	@Test
	public void testObtenerIdAgentes() {
		List<Agente> listaAgentes = new ArrayList<Agente>();
		agente = new Agente();
		agente.setId(1L);
		listaAgentes.add(agente);
		agente = new Agente();
		agente.setId(2L);
		listaAgentes.add(agente);
		agente = new Agente();
		agente.setId(3L);
		listaAgentes.add(agente);
		assertEquals("1,2,3", tested.obtenerIdAgentes(listaAgentes));
	}
	@Test
	public void testObtenerIdAgentes_null() {
		List<Agente> listaAgentes = new ArrayList<Agente>();
		assertEquals("", tested.obtenerIdAgentes(listaAgentes));
	}
	
	@Test
	public void testObtenerGerenciaStr() {
		List<Gerencia> gerenciaList = new ArrayList<Gerencia>();
		gerencia = new Gerencia();
		gerencia.setId(1L);
		gerenciaList.add(gerencia);
		gerencia = new Gerencia();
		gerencia.setId(2L);
		gerenciaList.add(gerencia);
		gerencia = new Gerencia();
		gerencia.setId(3L);
		gerenciaList.add(gerencia);
		assertEquals("1,2,3", tested.obtenerGerenciaStr(gerenciaList,0));
	}
	@Test
	public void testObtenerGerenciaStr_null() {
		List<Gerencia> gerenciaList = new ArrayList<Gerencia>();
		assertEquals("", tested.obtenerGerenciaStr(gerenciaList,0));
	}
	
	@Test
	public void testObtenerCentroOpstr() {
		List<CentroOperacion> centroOperacionesSeleccionados = new ArrayList<CentroOperacion>();
		centroOperacion = new CentroOperacion();
		centroOperacion.setId(1L);
		centroOperacionesSeleccionados.add(centroOperacion);
		centroOperacion = new CentroOperacion();
		centroOperacion.setId(2L);
		centroOperacionesSeleccionados.add(centroOperacion);
		centroOperacion = new CentroOperacion();
		centroOperacion.setId(3L);
		centroOperacionesSeleccionados.add(centroOperacion);
		assertEquals("1,2,3", tested.obtenerCentroOpstr(centroOperacionesSeleccionados,0));
	}
	@Test
	public void testObtenerCentroOpstr_null() {
		List<CentroOperacion> centroOperacionesSeleccionados = new ArrayList<CentroOperacion>();
		assertEquals("", tested.obtenerCentroOpstr(centroOperacionesSeleccionados,0));
	}
	
	@Test
	public void testObtenerPromotoria() {
		List<Promotoria> promotoriaList = new ArrayList<Promotoria>();
		promotoria = new Promotoria();
		promotoria.setId(1L);
		promotoriaList.add(promotoria);
		promotoria = new Promotoria();
		promotoria.setId(2L);
		promotoriaList.add(promotoria);
		promotoria = new Promotoria();
		promotoria.setId(3L);
		promotoriaList.add(promotoria);
		assertEquals("1,2,3", tested.obtenerPromotoria(promotoriaList, 0));
	}
	@Test
	public void testObtenerPromotoria_null() {
		List<Promotoria> promotoriaList = new ArrayList<Promotoria>();
		assertEquals("", tested.obtenerPromotoria(promotoriaList, 0));
	}
	
	@Test
	public void testObtenerEjecutivos() {
		List<Ejecutivo> ejecutivoList = new ArrayList<Ejecutivo>();
		ejecutivo = new Ejecutivo();
		ejecutivo.setId(1L);
		ejecutivoList.add(ejecutivo);
		ejecutivo = new Ejecutivo();
		ejecutivo.setId(2L);
		ejecutivoList.add(ejecutivo);
		ejecutivo = new Ejecutivo();
		ejecutivo.setId(3L);
		ejecutivoList.add(ejecutivo);
		assertEquals("1,2,3", tested.obtenerEjecutivos(ejecutivoList, 0));
	}
	@Test
	public void testObtenerEjecutivos_null() {
		List<Ejecutivo> ejecutivoList = new ArrayList<Ejecutivo>();
		assertEquals("", tested.obtenerEjecutivos(ejecutivoList, 0));
	}
	
	
}
