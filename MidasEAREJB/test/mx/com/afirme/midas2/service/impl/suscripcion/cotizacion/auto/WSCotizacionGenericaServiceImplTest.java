package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.EstiloView;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

public class WSCotizacionGenericaServiceImplTest {
	
	private WSCotizacionGenericaServiceImpl tested;
	@Mock
	private ListadoService listadoService;
	@Mock
	private UsuarioService usuarioService;
	
	private final String token = "token"; 
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		tested = new WSCotizacionGenericaServiceImpl();
		tested.setListadoService(listadoService);
		tested.setUsuarioService(usuarioService);
	}
	

	@Test
	public void testGetListTiposUso() throws Exception {
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		tested.getListTiposUso(BigDecimal.ONE, token);
		verifyBuscarUsuario();
		verify(listadoService).getMapTipoUsoVehiculoByNegocio(BigDecimal.ONE);
	}
	
	@Test(expected = SystemException.class)
	public void testGetListTiposUso_Null() throws Exception {
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		tested.getListTiposUso(null, token);
		verifyBuscarUsuario();
		verify(listadoService, never()).getMapTipoUsoVehiculoByNegocio(null);
	}
	
	@Test(expected = SystemException.class)
	public void testGetListTiposUso_ZERO() throws Exception {
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		tested.getListTiposUso(BigDecimal.ZERO, token);
		verifyBuscarUsuario();
		verify(listadoService, never()).getMapTipoUsoVehiculoByNegocio(
				BigDecimal.ONE);
	}
	
	@Test
	public void testBuscarEstilo() throws SystemException{
		final EstiloView filtro = new EstiloView();
		filtro.setIdMarca(BigDecimal.TEN);
		filtro.setModelo(Short.valueOf("2018"));
		filtro.setDescripcion("Tesla - Model S");
		filtro.setIdLineaNegocio(BigDecimal.ONE);
		
		List<EstiloVehiculoDTO> returnValue = new ArrayList<EstiloVehiculoDTO>();
		EstiloVehiculoDTO estilo = new EstiloVehiculoDTO();
		EstiloVehiculoId id = new EstiloVehiculoId("AUTOS", "06659", BigDecimal.ONE);
		estilo.setId(id);
		estilo.setDescripcionEstilo("Tesla - Model S");
		returnValue.add(estilo);
		
		when(listadoService.getListarEstilo(
				filtro.getIdMarca(), filtro.getModelo(),
				filtro.getIdLineaNegocio(), new BigDecimal(MonedaDTO.MONEDA_PESOS), null,
				filtro.getDescripcion())).thenReturn(returnValue);
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		Map<String, String> result = tested.buscarEstilo(filtro, token);
		assertTrue(result.containsKey("06659"));
		assertEquals("Tesla - Model S".toUpperCase(), result.get("06659"));
		
		verifyBuscarUsuario();
		verify(listadoService)
				.getListarEstilo(
						BigDecimal.TEN, Short.valueOf("2018"),
						BigDecimal.ONE, new BigDecimal(MonedaDTO.MONEDA_PESOS), null,
						"Tesla - Model S");

	}
	
	@Test(expected = SystemException.class)
	public void testBuscarEstilo_MarcaNull() throws SystemException{
		final EstiloView filtro = new EstiloView();
		filtro.setIdMarca(null);
		filtro.setModelo(Short.valueOf("2018"));
		filtro.setDescripcion("Tesla - Model S");
		filtro.setIdLineaNegocio(BigDecimal.ONE);
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		tested.buscarEstilo(filtro, token);
	}
	
	@Test(expected = SystemException.class)
	public void testBuscarEstilo_FiltroNull() throws SystemException{
		final EstiloView filtro = null;
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		tested.buscarEstilo(filtro, token);
	}
	
	@Test()
	public void testGetListFormasPago() throws SystemException{
		BigDecimal idToNegTipoPoliza = BigDecimal.ONE;
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		tested.getListFormasPago(idToNegTipoPoliza, token);
		verifyBuscarUsuario();
		verify(listadoService).getMapFormasdePagoByNegTipoPoliza(idToNegTipoPoliza);
	}
	
	@Test(expected = SystemException.class)
	public void testGetListFormasPago_NegTipoPolNull() throws SystemException{
		BigDecimal idToNegTipoPoliza = null;
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		tested.getListFormasPago(idToNegTipoPoliza, token);
		verifyBuscarUsuario();
		verify(listadoService, never()).getMapFormasdePagoByNegTipoPoliza(idToNegTipoPoliza);
	}
	
	@Test
	public void testValidateToken() throws Exception {
		when(usuarioService.isTokenActive(token)).thenReturn(true);
		tested.validateToken("token");
		verifyBuscarUsuario();
	}
	
	private void verifyBuscarUsuario(){
		verify(usuarioService).isTokenActive(token);
	}

}
