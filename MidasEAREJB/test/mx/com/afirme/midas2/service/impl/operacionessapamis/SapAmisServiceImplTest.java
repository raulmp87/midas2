package mx.com.afirme.midas2.service.impl.operacionessapamis;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.dto.general.Email;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocio;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioConfig;

import org.junit.Before;
import org.junit.Test;

public class SapAmisServiceImplTest {
	SapAmisServiceImpl tested;
	EmailNegocioConfig emailNegocioConfig;
	EmailNegocio emailNegocio;
	 Email email;

	@Before
	public void setUp() throws Exception {
		tested = new SapAmisServiceImpl();
	}

	@Test
	public void testObtenerCorreosNotificar() {
		List<EmailNegocioConfig> lEmailNegocioConfig = new ArrayList<EmailNegocioConfig>();
		emailNegocioConfig = new EmailNegocioConfig();
		 emailNegocio = new EmailNegocio();
		 emailNegocio.setEstatus(0l);
		 email = new Email();
		 email.setEmail("ejemplo@afirme.com");
		emailNegocio.setEmail(email);
		emailNegocioConfig.setEmailNegocio(emailNegocio);
		lEmailNegocioConfig.add(emailNegocioConfig);
		emailNegocioConfig = new EmailNegocioConfig();
		 emailNegocio = new EmailNegocio();
		 emailNegocio.setEstatus(0l);
		 email = new Email();
		 email.setEmail("ejemplo1@afirme.com");
		emailNegocio.setEmail(email);
		emailNegocioConfig.setEmailNegocio(emailNegocio);
		lEmailNegocioConfig.add(emailNegocioConfig);
		assertEquals("ejemplo@afirme.com,ejemplo1@afirme.com,",tested.obtenerCorreosNotificar(lEmailNegocioConfig));
	}
	
	@Test
	public void testObtenerCorreosNotificar_null() {
		List<EmailNegocioConfig> lEmailNegocioConfig = new ArrayList<EmailNegocioConfig>();
		assertEquals("",tested.obtenerCorreosNotificar(lEmailNegocioConfig));
	}

}
