package mx.com.afirme.midas2.service.impl.fuerzaventa;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgenteDet;

import org.junit.Before;
import org.junit.Test;

public class EnvioFacturaAgenteServiceImplTest {
	EnvioFacturaAgenteServiceImpl tested;
	EnvioFacturaAgente envio;
	EnvioFacturaAgenteDet envioFacturaAgenteDet;

	@Before
	public void setUp() throws Exception {
		tested = new EnvioFacturaAgenteServiceImpl();
	}

	@Test
	public void testObtenerListaMensajes() {
		envio = new EnvioFacturaAgente();
		List<EnvioFacturaAgenteDet> mensajes = new ArrayList<EnvioFacturaAgenteDet>();
		envioFacturaAgenteDet = new EnvioFacturaAgenteDet();
		envioFacturaAgenteDet.setTipoMensaje("error");
		envioFacturaAgenteDet.setMensaje("error1");
		mensajes.add(envioFacturaAgenteDet);
		envioFacturaAgenteDet = new EnvioFacturaAgenteDet();
		envioFacturaAgenteDet.setTipoMensaje("error");
		envioFacturaAgenteDet.setMensaje("error2");
		mensajes.add(envioFacturaAgenteDet);
		envio.setMensajes(mensajes);
		assertEquals("error  error1<br />error  error2<br />", tested.obtenerListaMensajes(envio));
	}
	
	@Test
	public void testObtenerListaMensajes_null() {
		envio = new EnvioFacturaAgente();
		List<EnvioFacturaAgenteDet> mensajes = new ArrayList<EnvioFacturaAgenteDet>();
		envio.setMensajes(mensajes);
		assertEquals("", tested.obtenerListaMensajes(envio));
	}

}
