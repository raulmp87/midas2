package mx.com.afirme.midas2.service.impl.excepcion;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.cargacombo.ComboBean;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.dto.excepcion.auto.ExcepcionCotizacionEvaluacionAutoDTO;

import org.junit.Before;
import org.junit.Test;

public class ExcepcionSuscripcionNegocioAutosServiceImplTest {
	
	ExcepcionSuscripcionNegocioAutosServiceImpl tested;
	ExcepcionCotizacionEvaluacionAutoDTO evaluacion;
	ComboBean object;
	CondicionEspecial condicionEspecial;

	@Before
	public void setUp() throws Exception {
		tested = new  ExcepcionSuscripcionNegocioAutosServiceImpl();
	}

	@Test
	public void testObtenerDescripcion() {
		List<String> modelo =new ArrayList<String>();
		modelo.add("modelo 1");
		modelo.add("modelo 2");
		modelo.add("modelo 3");
		modelo.add("modelo 4");
		assertEquals(" modelo 1 modelo 2 modelo 3 modelo 4", tested.obtenerDescripcion(modelo));

	}
	@Test
	public void testObtenerDescripcion_null() {
		List<String> modelo =new ArrayList<String>();
		
		assertEquals("", tested.obtenerDescripcion(modelo));

	}
	
	@Test
	public void testObtenerIds() {
		evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<ComboBean> texAdicionalMap = new ArrayList<ComboBean>();
		object = new ComboBean();
		object.setId("1");
		texAdicionalMap.add(object);
		object = new ComboBean();
		object.setId("2");
		texAdicionalMap.add(object);
		evaluacion.setTexAdicionalMap(texAdicionalMap);
		assertEquals("1-2", tested.obtenerIds(evaluacion));

	}
	
	@Test
	public void testObtenerIds_null() {
		evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<ComboBean> texAdicionalMap = new ArrayList<ComboBean>();
		evaluacion.setTexAdicionalMap(texAdicionalMap);
		assertEquals("", tested.obtenerIds(evaluacion));

	}
	
	@Test
	public void testObtenerCondicionesEspeciales() {
		evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<CondicionEspecial> condicionesInciso = new ArrayList<CondicionEspecial>();
		condicionEspecial = new CondicionEspecial();
		condicionEspecial.setNombre("ejemplo1");
		condicionesInciso.add(condicionEspecial);
		condicionEspecial = new CondicionEspecial();
		condicionEspecial.setNombre("ejemplo2");
		condicionesInciso.add(condicionEspecial);
		evaluacion.setCondicionesInciso(condicionesInciso);
			assertEquals("ejemplo1-ejemplo2", tested.obtenerCondicionesEspecialesInciso(evaluacion));

	}
	@Test
	public void testObtenerCondicionesEspeciales_null() {
		evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<CondicionEspecial> condicionesInciso = new ArrayList<CondicionEspecial>();
		evaluacion.setCondicionesInciso(condicionesInciso);
			assertEquals("", tested.obtenerCondicionesEspecialesInciso(evaluacion));

	}
	
	@Test
	public void testObtenerCondicionesEspecialesCotizacion() {
		evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<CondicionEspecial> condicionesCotizacion = new ArrayList<CondicionEspecial>();
		condicionEspecial = new CondicionEspecial();
		condicionEspecial.setNombre("ejemplo1");
		condicionesCotizacion.add(condicionEspecial);
		condicionEspecial = new CondicionEspecial();
		condicionEspecial.setNombre("ejemplo2");
		condicionesCotizacion.add(condicionEspecial);
		evaluacion.setCondicionesCotizacion(condicionesCotizacion);
		assertEquals("ejemplo1-ejemplo2", tested.obtenerCondicionesEspecialesCotizacion(evaluacion));

	}
	@Test
	public void testObtenerCondicionesEspecialesCotizacion_null() {
		evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<CondicionEspecial> condicionesCotizacion = new ArrayList<CondicionEspecial>();
		evaluacion.setCondicionesCotizacion(condicionesCotizacion);
		assertEquals("", tested.obtenerCondicionesEspecialesCotizacion(evaluacion));

	}
	
}
