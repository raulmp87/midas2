package mx.com.afirme.midas2.service.impl.bitemporal.excepcion;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.dto.excepcion.auto.ExcepcionCotizacionEvaluacionAutoDTO;

import org.junit.Before;
import org.junit.Test;

public class ExcepcionSuscripNegAutoBitemporalServiceImplTest {
	ExcepcionSuscripNegAutoBitemporalServiceImpl tested;
	CondicionEspecial condicionEspecial;

	@Before
	public void setUp() throws Exception {
		tested = new ExcepcionSuscripNegAutoBitemporalServiceImpl();
	}

	@Test
	public void testObtenerCondicionesEspeciales() {
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<CondicionEspecial> condicionesCotizacion = new ArrayList<CondicionEspecial>();
		condicionEspecial = new CondicionEspecial();
		condicionEspecial.setNombre("ejemplo1");
		condicionesCotizacion.add(condicionEspecial);
		condicionEspecial = new CondicionEspecial();
		condicionEspecial.setNombre("ejemplo2");
		condicionesCotizacion.add(condicionEspecial);
		evaluacion.setCondicionesCotizacion(condicionesCotizacion);
		assertEquals("ejemplo1-ejemplo2",tested.obtenerCondicionesEspecialesCotizacion(evaluacion));
	}
	@Test
	public void testObtenerCondicionesEspeciales_null() {
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		assertEquals("",tested.obtenerCondicionesEspecialesCotizacion(evaluacion));
	}
	@Test
	public void testObtenerCondicionesEspecialesinciso() {
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<CondicionEspecial> condicionesInciso = new ArrayList<CondicionEspecial>();
		condicionEspecial = new CondicionEspecial();
		condicionEspecial.setNombre("ejemplo1");
		condicionesInciso.add(condicionEspecial);
		condicionEspecial = new CondicionEspecial();
		condicionEspecial.setNombre("ejemplo2");
		condicionesInciso.add(condicionEspecial);
		evaluacion.setCondicionesInciso(condicionesInciso);
		assertEquals("ejemplo1-ejemplo2",tested.obtenerCondicionesEspecialesInciso(evaluacion));
	}

	@Test
	public void testObtenerCondicionesEspecialesinciso_null() {
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion = new ExcepcionCotizacionEvaluacionAutoDTO();
		List<CondicionEspecial> condicionesInciso = new ArrayList<CondicionEspecial>();
		evaluacion.setCondicionesInciso(condicionesInciso);
		assertEquals("",tested.obtenerCondicionesEspecialesInciso(evaluacion));
	}

}
