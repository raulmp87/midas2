package mx.com.afirme.midas2.service.impl.ocra;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas2.wsClient.ocra.robo.MensajeBean;

import org.junit.Before;
import org.junit.Test;

public class EnvioOcraServiceImplTest {
	private EnvioOcraServiceImpl tested;
	MensajeBean mensaje;

	@Before
	public void setUp() throws Exception {
		tested = new EnvioOcraServiceImpl();
		mensaje = new MensajeBean();
	}

	@Test
	public void testValidarHora_correcto() {
		String hora="10:51:57";		
		assertEquals("10:51:57", tested.validarHora(hora));
	}
	@Test
	public void testValidarHora_incorrecto() {
		String hora="10:5";		
		assertEquals("10:00010:00", tested.validarHora(hora));
	}
	
	@Test
	public void testValidarHora_null() {
		String hora="";		
		assertEquals(":00", tested.validarHora(hora));
	}

	@Test
	public void testValidarHora_uno() {
		String hora="1";		
		assertEquals("01:00", tested.validarHora(hora));
	}

	@Test
	public void testObtenerMensajeGerente_error() {
		List<MensajeBean> lMensaje = new ArrayList<MensajeBean>();
		mensaje.setDescripcion("ejemplo 1");
		mensaje.setId(1);
		lMensaje.add(0, mensaje);
		mensaje = new MensajeBean();
		mensaje.setDescripcion("ejemplo 2");
		mensaje.setId(2);
		lMensaje.add(1,mensaje);
				assertEquals("ERROR | 1 ejemplo 1 /// ERROR | 2 ejemplo 2 /// ", tested.obtenerMensajeGerente(lMensaje ));
	}
	@Test
	public void testObtenerMensajeGerente_ok() {
		List<MensajeBean> lMensaje = new ArrayList<MensajeBean>();
		mensaje.setDescripcion("ejemplo 1");
		mensaje.setId(0);
		lMensaje.add(0, mensaje);
		mensaje = new MensajeBean();
		mensaje.setDescripcion("ejemplo 2");
		mensaje.setId(1);
		lMensaje.add(1,mensaje);
				assertEquals("OK | ", tested.obtenerMensajeGerente(lMensaje ));
	}

	@Test
	public void testObtenerMensajeGerente_null() {
		List<MensajeBean> lMensaje = new ArrayList<MensajeBean>();
		assertEquals("", tested.obtenerMensajeGerente(lMensaje ));
	}
}
