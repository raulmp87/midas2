package mx.com.afirme.midas2.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;


public class ListadoServiceImplTest {

	private ListadoServiceImpl tested;
	@Mock
	private ImpresionRecibosService impresionRecibosService;
	
	@Before
	public void setUp(){
		MockitoAnnotations.initMocks(this);
		tested = new ListadoServiceImpl();
		tested.setImpresionRecibosService(impresionRecibosService);
	}
	
	@Test
	public void getIncisosProgPago() {
		final Map<BigDecimal, String> incisosList = new LinkedHashMap<BigDecimal, String>();
		incisosList.put(BigDecimal.ZERO, "cero");
		incisosList.put(BigDecimal.ONE, "uno");
		when(impresionRecibosService.getMapIncisosEndoso(BigDecimal.ZERO, BigDecimal.ZERO)).thenReturn(incisosList);
		
		final String result = tested.getIncisosProgPago(BigDecimal.ZERO, BigDecimal.ZERO);
		assertEquals("cero,uno", result);
	}

	@Test
	public void getIncisosProgPago_null() {
		final Map<BigDecimal, String> incisosList = new LinkedHashMap<BigDecimal, String>();
		when(impresionRecibosService.getMapIncisosEndoso(BigDecimal.ZERO, BigDecimal.ZERO)).thenReturn(incisosList);
		
		final String result = tested.getIncisosProgPago(BigDecimal.ZERO, BigDecimal.ZERO);
		assertEquals("", result);
	}
}
