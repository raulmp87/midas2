package mx.com.afirme.midas2.service.impl.reaseguro.reporteRR6;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class ComplementoRR6ServiceImplTest {
	ComplementoRR6ServiceImpl tested;

	@Before
	public void setUp() throws Exception {
		tested = new ComplementoRR6ServiceImpl();
	}

	@Test
	public void testObtenerResultado() {
		List<String> listaErrores = new ArrayList<String>();
		listaErrores.add("error1");
		listaErrores.add("error2");
		listaErrores.add("error3");
		assertEquals("error1<br>error2<br>error3<br>", tested.obtenerResultado(listaErrores));
	}
	
	@Test
	public void testObtenerResultado_null() {
		List<String> listaErrores = new ArrayList<String>();
		assertEquals("", tested.obtenerResultado(listaErrores));
	}

}
