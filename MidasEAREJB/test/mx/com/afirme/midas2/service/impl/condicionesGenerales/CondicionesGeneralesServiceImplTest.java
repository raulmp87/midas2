package mx.com.afirme.midas2.service.impl.condicionesGenerales;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class CondicionesGeneralesServiceImplTest {
	CondicionesGeneralesServiceImpl tested;

	@Before
	public void setUp() throws Exception {
		tested = new CondicionesGeneralesServiceImpl();
	}

	@Test
	public void testObtenerCentrosNuevos() {
		String[] centros ={"1", "2","3", "4"};
		assertEquals("2,3,4", tested.obtenerCentrosNuevos(centros , "1"));
	}
	@Test
	public void testObtenerCentrosNuevos_equals() {
		String[] centros ={"1"};
		assertEquals("", tested.obtenerCentrosNuevos(centros , "1"));
	}
	@Test
	public void testObtenerCentrosNuevos_null() {
		String[] centros ={""};
		assertEquals("", tested.obtenerCentrosNuevos(centros , ""));
	}

}
