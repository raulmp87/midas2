package mx.com.afirme.midas2.ws.axosnet;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import javax.xml.ws.Action;

public class WSValidacionesSATPortProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private mx.com.afirme.midas2.ws.axosnet.WSValidacionesSAT_Service _service = null;
        private mx.com.afirme.midas2.ws.axosnet.WSValidacionesSAT _proxy = null;
        private Dispatch<Source> _dispatch = null;
        private boolean _useJNDIOnly = false;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new mx.com.afirme.midas2.ws.axosnet.WSValidacionesSAT_Service(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            try
            {
                InitialContext ctx = new InitialContext();
                _service = (mx.com.afirme.midas2.ws.axosnet.WSValidacionesSAT_Service)ctx.lookup("java:comp/env/service/WSValidacionesSAT");
            }
            catch (NamingException e)
            {
                if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
                    System.out.println("JNDI lookup failure: javax.naming.NamingException: " + e.getMessage());
                    e.printStackTrace(System.out);
                }
            }

            if (_service == null && !_useJNDIOnly)
                _service = new mx.com.afirme.midas2.ws.axosnet.WSValidacionesSAT_Service();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getWSValidacionesSATPort();
        }

        public mx.com.afirme.midas2.ws.axosnet.WSValidacionesSAT getProxy() {
            return _proxy;
        }

        public void useJNDIOnly(boolean useJNDIOnly) {
            _useJNDIOnly = useJNDIOnly;
            init();
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://sat.ws.axosnet.com/", "WSValidacionesSATPort");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

        public void setMTOMEnabled(boolean enable) {
            SOAPBinding binding = (SOAPBinding) ((BindingProvider) _proxy).getBinding();
            binding.setMTOMEnabled(enable);
        }
    }

    public WSValidacionesSATPortProxy() {
        _descriptor = new Descriptor();
        _descriptor.setMTOMEnabled(true);
    }

    public WSValidacionesSATPortProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
        _descriptor.setMTOMEnabled(true);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public AxnResponse verificaDisponibilidad(String palabra) {
        return _getDescriptor().getProxy().verificaDisponibilidad(palabra);
    }

    public ValResponse verificaComprobanteFiscalDigital(String comprobante, String razonSocial, String rfc, String importe, String iva, String isr, String ivaRetenido, String ivaTrasladado, String descuento, String total, String tolerancia, String idSociedad, String idDepartamento, String referencia, String concepto) {
        return _getDescriptor().getProxy().verificaComprobanteFiscalDigital(comprobante,razonSocial,rfc,importe,iva,isr,ivaRetenido,ivaTrasladado,descuento,total,tolerancia,idSociedad,idDepartamento,referencia,concepto);
    }

    public WsValidacionesSATResponse pruebaConectividad(String palabra) {
        return _getDescriptor().getProxy().pruebaConectividad(palabra);
    }

    public WsValidacionesSATResponse vEstructuraCFD(String xmlComprobante) {
        return _getDescriptor().getProxy().vEstructuraCFD(xmlComprobante);
    }

    public WsValidacionesSATResponse vFolioCFD(String rfc, String noAprobacion, String anoAprobacion, String serie, int folio) {
        return _getDescriptor().getProxy().vFolioCFD(rfc,noAprobacion,anoAprobacion,serie,folio);
    }

    public WsValidacionesSATResponse vSerieCSD(String noCert, String fecha, String rfc) {
        return _getDescriptor().getProxy().vSerieCSD(noCert,fecha,rfc);
    }

    public WsValidacionesSATResponse vSello(String id, String cadenaOriginal, String sello, String certificado) {
        return _getDescriptor().getProxy().vSello(id,cadenaOriginal,sello,certificado);
    }

}