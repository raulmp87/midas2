//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-05/12/2011 11:54 AM(foreman)-)
//


package mx.com.afirme.midas2.ws.axosnet;

import java.net.URL;
import javax.xml.namespace.QName;
import javax.xml.ws.Service;
import javax.xml.ws.WebEndpoint;
import javax.xml.ws.WebServiceClient;
import javax.xml.ws.WebServiceException;
import javax.xml.ws.WebServiceFeature;

@WebServiceClient(name = "WSValidacionesSAT", targetNamespace = "http://sat.ws.axosnet.com/", wsdlLocation = "META-INF/wsdl/WSValidacionesSAT.wsdl")
public class WSValidacionesSAT_Service
    extends Service
{

    private final static URL WSVALIDACIONESSAT_WSDL_LOCATION;
    private final static WebServiceException WSVALIDACIONESSAT_EXCEPTION;
    private final static QName WSVALIDACIONESSAT_QNAME = new QName("http://sat.ws.axosnet.com/", "WSValidacionesSAT");

    static {
            WSVALIDACIONESSAT_WSDL_LOCATION = mx.com.afirme.midas2.ws.axosnet.WSValidacionesSAT_Service.class.getResource("/META-INF/wsdl/WSValidacionesSAT.wsdl");
        WebServiceException e = null;
        if (WSVALIDACIONESSAT_WSDL_LOCATION == null) {
            e = new WebServiceException("Cannot find 'META-INF/wsdl/WSValidacionesSAT.wsdl' wsdl. Place the resource correctly in the classpath.");
        }
        WSVALIDACIONESSAT_EXCEPTION = e;
    }

    public WSValidacionesSAT_Service() {
        super(__getWsdlLocation(), WSVALIDACIONESSAT_QNAME);
    }

    public WSValidacionesSAT_Service(URL wsdlLocation) {
        super(wsdlLocation, WSVALIDACIONESSAT_QNAME);
    }

    public WSValidacionesSAT_Service(URL wsdlLocation, QName serviceName) {
        super(wsdlLocation, serviceName);
    }

    /**
     * 
     * @return
     *     returns WSValidacionesSAT
     */
    @WebEndpoint(name = "WSValidacionesSATPort")
    public WSValidacionesSAT getWSValidacionesSATPort() {
        return super.getPort(new QName("http://sat.ws.axosnet.com/", "WSValidacionesSATPort"), WSValidacionesSAT.class);
    }

    /**
     * 
     * @param features
     *     A list of {@link javax.xml.ws.WebServiceFeature} to configure on the proxy.  Supported features not in the <code>features</code> parameter will have their default values.
     * @return
     *     returns WSValidacionesSAT
     */
    @WebEndpoint(name = "WSValidacionesSATPort")
    public WSValidacionesSAT getWSValidacionesSATPort(WebServiceFeature... features) {
        return super.getPort(new QName("http://sat.ws.axosnet.com/", "WSValidacionesSATPort"), WSValidacionesSAT.class, features);
    }

    private static URL __getWsdlLocation() {
        if (WSVALIDACIONESSAT_EXCEPTION!= null) {
            throw WSVALIDACIONESSAT_EXCEPTION;
        }
        return WSVALIDACIONESSAT_WSDL_LOCATION;
    }

}
