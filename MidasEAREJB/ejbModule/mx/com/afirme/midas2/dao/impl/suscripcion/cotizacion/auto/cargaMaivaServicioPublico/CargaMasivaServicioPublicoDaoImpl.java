package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.cargaMaivaServicioPublico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoDao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaSPDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


@Stateless
public class CargaMasivaServicioPublicoDaoImpl  implements CargaMasivaServicioPublicoDao{

	protected EntidadService entidadService;

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@PersistenceContext
	protected EntityManager entityManager;

	/**
	 * Método de implementado de DAO para la consulta de archivos de carga masiva almacenados
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublicoDTO} archivoAdjunto
	 * @return {@link CargaMasivaServicioPublico}
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<CargaMasivaServicioPublico> consultaArchivosCargaMasivaDao(CargaMasivaServicioPublicoDTO archivoAdjunto) {
		
		Query query = null;
		StringBuilder queryString = new StringBuilder();
		List<HashMap> parameters = new ArrayList<HashMap>(1);
		
		queryString.append(" SELECT archivo FROM CargaMasivaServicioPublico archivo WHERE ")
				   .append("archivo.codigoUsuarioCreacion =:codigoUsuarioCreacion")
				   .append(" ORDER BY archivo.idArchivoCargaMasiva DESC");
		
		Utilerias.agregaHashLista(parameters, "codigoUsuarioCreacion", archivoAdjunto.getUsuarioActual());
		
		query = entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, parameters);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		if(archivoAdjunto.getPosStar() != null){
			query.setFirstResult(archivoAdjunto.getPosStar());
		}
		if(archivoAdjunto.getCount() != null){
			query.setMaxResults(archivoAdjunto.getCount());
		}

		return query.getResultList();
	}
	
	/**
	 * Método de implementado de DAO para verifiar si un MD5 ya existe en la tabla y en que estatus esta
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param String md5CheckSum
	 * @return Lista de {@link CargaMasivaServicioPublico}
	 */
	public List<CargaMasivaServicioPublico> getMd5ArchivoDao(String md5CheckSum) {
		return entidadService.findByProperty(CargaMasivaServicioPublico.class, "md5CargaMasiva", md5CheckSum);
	}
	
	/**
	 * Método de implementado de DAO para almacenar el archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 *
	 * @param {@link CargaMasivaServicioPublico} archivoCargaMasiva
	 * @return {@link CargaMasivaServicioPublico}
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CargaMasivaServicioPublico saveArchivoDao(CargaMasivaServicioPublico archivoCargaMasiva) {
		return archivoCargaMasiva = entidadService.save(archivoCargaMasiva);
	}
	
	/**
	 * Método de implementado de DAO para almacenar el detalle del archivo en la base de datos
	 * @author SOFTNET - ISCJJBV 
	 *
	 * @param {@link CargaMasivaServicioPublicoDetalle} detalleArchivoCargaMasiva
	 * @return {@link CargaMasivaServicioPublicoDetalle}
	 */
	public CargaMasivaServicioPublicoDetalle saveDetalleArchivoDao(CargaMasivaServicioPublicoDetalle detalleArchivoCargaMasiva) {
		CargaMasivaServicioPublicoDetalle detalle = new CargaMasivaServicioPublicoDetalle();
		detalle = entidadService.save(detalleArchivoCargaMasiva);
		return detalle;
	}
	
	/**
	 * Método implementado de DAO para contar los registros de la tabla de archivo
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublicoDTO} archivoAdjunto
	 * @return Long
	 */
	public Long conteoBusquedaArchivosDao(CargaMasivaServicioPublicoDTO  archivoAdjunto) {
		
		Query query = null;
		StringBuilder queryString = new StringBuilder();
		List<HashMap> parameters = new ArrayList<HashMap>(1);
		
		queryString.append(" SELECT COUNT(archivo.idArchivoCargaMasiva) FROM CargaMasivaServicioPublico archivo WHERE ")
				   .append(" archivo.codigoUsuarioCreacion =:codigoUsuarioCreacion ");
		
		Utilerias.agregaHashLista(parameters, "codigoUsuarioCreacion", archivoAdjunto.getUsuarioActual());
		
		query = entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, parameters);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return ((Long)query.getSingleResult());
	}
	
	/**
	 * Método implementado de DAO para actualizar los estatus de los registros de la tabla de detalle de archivos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublicoDetalle} registroDetalle
	 */
	public void updateEstatusDetalleDao(CargaMasivaServicioPublicoDetalle  registroDetalle){
		entidadService.save(registroDetalle);
	}
	
	/**
	 * Método implementado de DAO para actualizar los estatus de los archivos de la tabla de archivos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link CargaMasivaServicioPublico} registroArchivo
	 */
	public void updateEstatusArchivoDao(CargaMasivaServicioPublico registroArchivo){
		entidadService.save(registroArchivo);
	}
	
	/**
	 * Método implementado de DAO para hacer traducción de id's SEYCOS a id's MIDAS
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param String tipoConversion
	 * @param String identificadorSeycos
	 * @return BigDecimal
	 */
	@SuppressWarnings("unchecked")
	public BigDecimal obtieneConversionSeycosMidas(String tipoConversion, Long identificadorSeycos) {
		BigDecimal result = null;
		String queryString = "SELECT ID_MIDAS FROM SEYCOS.CONV_MIDAS_SEYCOS WHERE TIPO = '" + tipoConversion + "' " + 
							 "AND ID_SEYCOS = " + identificadorSeycos;
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		
		List<Object> res = query.getResultList();
		if(!res.isEmpty()){
			result = new BigDecimal(res.get(0).toString());
		}
		
		return result;
		
	}

	/*********************************/
	
	
	@SuppressWarnings("rawtypes")
	private Query generarQuery(Long archivoId, boolean esCuenta){
		Query query 							= null;
		StringBuilder queryString 				= new StringBuilder();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>(1);	
		
		
		if(esCuenta){
			queryString.append(" SELECT count(detalle.idDetalleCargaMasiva) ");
		}else{
			
			queryString.append(" SELECT new mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaSPDetalleDTO( ");
			queryString.append(" detalle.archivoCargaMasivaSP_ID.idArchivoCargaMasiva, detalle.numToPoliza, detalle.tipoPersonaCliente,  detalle.numeroSerie, ");
			queryString.append(" detalle.descripcionVehiculo, detalle.modelo, detalle.primaTotal, detalle.estatusProceso, ");
			queryString.append(" detalle.descripcionEstatusProceso, detalle.idDetalleCargaMasiva)");
		}
		queryString.append(" FROM CargaMasivaServicioPublicoDetalle detalle WHERE detalle.archivoCargaMasivaSP_ID.idArchivoCargaMasiva = :archivoSPId ");
		Utilerias.agregaHashLista(listaParametrosValidos, "archivoSPId", archivoId);
	

		query = entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;	
	}

	@Override
	public Long contarDetalle(Long archivoId, Integer count, Integer posStart) {
		Query query = generarQuery(archivoId, true);
		return ((Long)query.getSingleResult());	
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<CargaMasivaSPDetalleDTO> busquedaDetalle(Long archivoId, Integer count, Integer posStart) {
		Query query = generarQuery(archivoId, false);
		
		if(posStart != null){
			query.setFirstResult(posStart);
		}
		if(count != null){
			query.setMaxResults(count);
		}
		return query.getResultList();
	}
		 
	@Override
	public Boolean existePolizaSeycos(Long numToPoliza, Long idCentroEmisor){
		
		String spName = "MIDAS.PKGAUT_CARGAMASIVASERVPUB.existePolizaSeycos"; 
        StoredProcedureHelper storedHelper 			= null;
        String propiedades 							= "";
        String columnasBaseDatos 						= "";
        CargaMasivaServicioPublicoDetalle resultado = new CargaMasivaServicioPublicoDetalle();
        Boolean existePoliza = null;
        
        try {
      	  propiedades = "exitePolizaSeycosNumero";
      	  columnasBaseDatos = "existePolizaSeycos"; 
            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(CargaMasivaServicioPublicoDetalle.class.getCanonicalName(), propiedades, columnasBaseDatos);
            storedHelper.estableceParametro("P_NUMERO_DE_POLIZA", numToPoliza);
			storedHelper.estableceParametro("P_ID_CENTRO_EMISOR", idCentroEmisor);
			resultado = (CargaMasivaServicioPublicoDetalle)storedHelper.obtieneResultadoSencillo();
			if(resultado != null
					&& resultado.getExitePolizaSeycosNumero() != null){
				existePoliza = (resultado.getExitePolizaSeycosNumero().equals(new Long(0)))? Boolean.FALSE : Boolean.TRUE;
				resultado.setExistePolizaSeycos(existePoliza);
			}else{
				existePoliza = null;
				resultado.setExistePolizaSeycos(existePoliza);
			}
        } catch (Exception e) {
        	existePoliza = null;
        	resultado.setExistePolizaSeycos(existePoliza);
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en obtenerMontoSiniestrosAnteriores..."+descErr
                                           , Level.WARNING, e);    
             }
        }
		return resultado.getExistePolizaSeycos();
	}
	
	@Override
	public Date obtenerFinVigenciaPoliza(Date fechaInicioVigencia, Long vigencia){
		Date fechaFinVigencia = null;
		
		String spName = "MIDAS.PKGAUT_CARGAMASIVASERVPUB.calcularFechaFinVigencia"; 
        StoredProcedureHelper storedHelper 			= null;
        String propiedades 							= "";
        String columnasBaseDatos 						= "";
        CargaMasivaServicioPublicoDetalle resultado = new CargaMasivaServicioPublicoDetalle();
        
        try {
      	  propiedades = "fechaFinVigencia";
      	  columnasBaseDatos = "fechaFinVigencia"; 
            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(CargaMasivaServicioPublicoDetalle.class.getCanonicalName(), propiedades, columnasBaseDatos);
            storedHelper.estableceParametro("P_FECHA_INICIO_VIGENCIA", fechaInicioVigencia);
			storedHelper.estableceParametro("P_CODIGO_VIGENCIA", vigencia);
			resultado = (CargaMasivaServicioPublicoDetalle)storedHelper.obtieneResultadoSencillo();
			if(resultado != null){
				fechaFinVigencia = resultado.getFechaFinVigencia();
			}
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en obtenerMontoSiniestrosAnteriores..."+descErr
                                           , Level.WARNING, e);    
             }
        }
		
		return fechaFinVigencia;
	}
	
}
