package mx.com.afirme.midas2.dao.impl.comisiones;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.comisiones.ConfigComisionesDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComCentroOperacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComEjecutivo;
import mx.com.afirme.midas2.domain.comisiones.ConfigComGerencia;
import mx.com.afirme.midas2.domain.comisiones.ConfigComMotivoEstatus;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPrioridad;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComSituacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoAgente;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.domain.comisiones.ItemsConfigComision;
import mx.com.afirme.midas2.dto.comisiones.ConfigComisionesDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroOperacionService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ConfigComisionesDaoImpl extends EntidadDaoImpl implements ConfigComisionesDao{
	private static final String SEPARADOR_COMA = ",";

	private static final BigDecimal GRUPO_PARAMETRO_BANCO_AFIRME = new BigDecimal("29");
	private static final BigDecimal PARAMETRO_CLAVE_AFIRME = new BigDecimal("290010");
	
	private EntidadService entidadService;
	private ValorCatalogoAgentesService catalogoService;
	private CentroOperacionService centroOperacionService;
	private GerenciaJPAService gerenciaService;
	private EjecutivoService ejecutivoService;
	private PromotoriaJPAService promotoriaService;
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@Override
	public List<ValorCatalogoAgentes> getCatalogoModoEjecucion()throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Modos de Ejecucion de Comisiones");
	}

	@Override
	public List<ValorCatalogoAgentes> getCatalogoPeriodo() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Periodos de Ejecucion de Comisiones");
	}

	@Override
	public List<CentroOperacionView> getCentroOperacionList() throws Exception {
		//return centroOperacionService.findByFiltersView(filtroCentroOperacion);
		return centroOperacionService.getList(true);
	}

	@Override
	public List<EjecutivoView> getEjecutivoList() throws Exception {
		//return entidadService.findAll(Ejecutivo.class);
		return ejecutivoService.getList(true);
	}

	@Override
	public List<GerenciaView> getGerenciaList() throws Exception {
		//return entidadService.findAll(Gerencia.class);
		return gerenciaService.getList(true);
	}

	@Override
	public List<PromotoriaView> getPromotoriaList() throws Exception {
		//return entidadService.findAll(Promotoria.class);
		return promotoriaService.getList(true);
	}

	@Override
	public List<ValorCatalogoAgentes> getPrioridadList() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Prioridades de Agente");
	}

	@Override
	public List<ValorCatalogoAgentes> getSituacionList() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Estatus de Agente (Situacion)");
	}

	@Override
	public List<ValorCatalogoAgentes> getTipoDeAgenteList() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Tipo de Agente");
	}

	@Override
	public List<ValorCatalogoAgentes> getTipoPromotoriaList() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Tipos de Promotoria");
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ConfigComisiones saveConfiguration(ConfigComisiones config) throws Exception{
		if(config==null){
			onError("Favor de proporcionar la configuracion a guardar");
		}
		/*if(config.getAgenteInicio()!=null && config.getAgenteInicio().getIdAgente()!=null){
			List<Agente> agentesIn =entidadService.findByProperty(Agente.class, "idAgente",config.getAgenteInicio().getIdAgente());
	
			if(!agentesIn.isEmpty()){
				Agente agenteInicio=agentesIn.get(0);
				if(agenteInicio.getPromotoria()!=null){
					Promotoria promotoriaAgenteIni = entidadService.findById(Promotoria.class,agenteInicio.getPromotoria().getId());
					entidadService.refresh(promotoriaAgenteIni);
					agenteInicio.setPromotoria(promotoriaAgenteIni);
				}
				config.setAgenteInicio(agenteInicio);
			}else{
				config.setAgenteInicio(null);
			}
		}else{
			config.setAgenteInicio(null);
		}
		
		if(config.getAgenteFin()!=null && config.getAgenteFin().getIdAgente()!=null){
			List<Agente> agentesFi =entidadService.findByProperty(Agente.class, "idAgente",config.getAgenteFin().getIdAgente());
			if(!agentesFi.isEmpty()){
				Agente agenteFin=agentesFi.get(0);
				if(agenteFin.getPromotoria()!=null){
					Promotoria promotoriaAgenteFin = entidadService.findById(Promotoria.class,agenteFin.getPromotoria().getId());
					entidadService.refresh(promotoriaAgenteFin);
					agenteFin.setPromotoria(promotoriaAgenteFin);
				}
				config.setAgenteFin(agenteFin);
			}else{
				config.setAgenteFin(null);
			}
		}else{
			config.setAgenteFin(null);
		}*/
		//Si esta activa la configuracion, la fecha de inicio de vigencia es desde que se registra la configuracion.
//		if(config.getActivoBoolean() || 1==config.getActivo()){
//			config.setFechaInicioVigencia(new Date());
//		}
//		config.setModoEjecucion(entidadService.findById(ValorCatalogoAgentes.class, config.getPeriodoEjecucion().getId()));
//		config.setModoEjecucion(entidadService.findById(ValorCatalogoAgentes.class, config.getModoEjecucion().getId()));
		config=entidadService.save(config);
		if(config!=null){
			List<ConfigComCentroOperacion> listaCentroOperaciones=config.getListaCentroOperaciones();
			List<ConfigComGerencia> listaGerencias=config.getListaGerencias();
			List<ConfigComEjecutivo> listaEjecutivos=config.getListaEjecutivos();
			List<ConfigComPromotoria> listaPromotorias=config.getListaPromotorias();
			List<ConfigComTipoPromotoria> listaTiposPromotoria=config.getListaTiposPromotoria();
			List<ConfigComPrioridad> listaPrioridades=config.getListaPrioridades();
			List<ConfigComSituacion> listaSituaciones=config.getListaSituaciones();
			List<ConfigComTipoAgente> listaTipoAgentes=config.getListaTipoAgentes();
			List<ConfigComMotivoEstatus> listaMotivoEstatus = config.getListaMotivoEstatus();

			saveCentroOperacionConfig(config, listaCentroOperaciones);
			saveMotivoEstatus(config, listaMotivoEstatus);
			saveGerenciaConfig(config, listaGerencias);
			saveEjecutivoConfig(config, listaEjecutivos);
			savePromotoriaConfig(config, listaPromotorias);
			savePrioridadConfig(config, listaPrioridades);
			saveTipoAgenteConfig(config, listaTipoAgentes);
			saveTipoPromotoriaConfig(config, listaTiposPromotoria);
			saveSituacionConfig(config, listaSituaciones);
		}
		return config;
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ConfigComisiones saveCentroOperacionConfig(ConfigComisiones config,List<ConfigComCentroOperacion> centroOperaciones) throws Exception{
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		List<ConfigComCentroOperacion> relacionesActuales=obtenerCentrosOperacionPorConfiguracion(config.getId());
		List<ConfigComCentroOperacion> relacionesActuales=findByProperty(ConfigComCentroOperacion.class, "configuracionComisiones.id", config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComCentroOperacion relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(centroOperaciones!=null && !centroOperaciones.isEmpty()){
			for(ConfigComCentroOperacion elemento:centroOperaciones){
				if(isNotNull(elemento)){
					if(elemento.getCentroOperacion()==null || elemento.getCentroOperacion().getId()==null){
						onError("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con un centro de operacion");
					}
					elemento.setConfiguracionComisiones(config);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	/**
	 * Obtiene la lista de centros de operacion actuales en una configuracion por su id
	 * @param idConfiguracionComision
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<ConfigComCentroOperacion> obtenerCentrosOperacionPorConfiguracion(Long idConfiguracionComision){
		List<ConfigComCentroOperacion> lista=new ArrayList<ConfigComCentroOperacion>();
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ConfigComCentroOperacion model "); 
		queryString.append(" where ");
		if(isNotNull(idConfiguracionComision)){
			addCondition(queryString, " model.configuracionComisiones.id=:idConfiguracionComision");
			params.put("idConfiguracionComision", idConfiguracionComision);
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		lista=query.getResultList();
		return lista;
	}
	/**
	 * Se guardan las relaciones entre la configuracion y los ejecutivos
	 * Si es una actualizacion de una configuracion, primero se eliminan las relaciones actuales,y se insertan las nuevas relaciones.
	 */
	@Override
	public ConfigComisiones saveEjecutivoConfig(ConfigComisiones config,List<ConfigComEjecutivo> ejecutivos) throws Exception{
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		
		List<ConfigComEjecutivo> relacionesActuales=findByProperty(ConfigComEjecutivo.class,"configuracionComisiones.id",config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComEjecutivo relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(ejecutivos!=null && !ejecutivos.isEmpty()){
			for(ConfigComEjecutivo elemento:ejecutivos){
				if(isNotNull(elemento)){
					if(elemento.getEjecutivo()==null || elemento.getEjecutivo().getId()==null){
						onError("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con un ejecutivo");
					}
					elemento.setConfiguracionComisiones(config);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	/**
	 * Guarda la relacion de gerencias y la configuracion de comisiones
	 */
	@Override
	public ConfigComisiones saveGerenciaConfig(ConfigComisiones config,List<ConfigComGerencia> lista) throws Exception{
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		List<ConfigComGerencia> relacionesActuales=findByProperty(ConfigComGerencia.class,"configuracionComisiones.id",config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComGerencia relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(lista!=null && !lista.isEmpty()){
			for(ConfigComGerencia elemento:lista){
				if(isNotNull(elemento)){
					if(elemento.getGerencia()==null || elemento.getGerencia().getId()==null){
						onError("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con una gerencia");
					}
					elemento.setConfiguracionComisiones(config);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	/**
	 * Guarda la relacion entre una configuracion y las prioridades
	 */
	@Override
	public ConfigComisiones savePrioridadConfig(ConfigComisiones config,List<ConfigComPrioridad> lista) throws Exception{
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		List<ConfigComPrioridad> relacionesActuales=findByProperty(ConfigComPrioridad.class,"configuracionComisiones.id",config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComPrioridad relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(lista!=null && !lista.isEmpty()){
			for(ConfigComPrioridad elemento:lista){
				if(isNotNull(elemento)){
					if(elemento.getPrioridadAgente()==null || elemento.getPrioridadAgente().getId()==null){
						onError("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con una prioridad");
					}
					elemento.setConfiguracionComisiones(config);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	/**
	 * Guarda la relacion entre la configuracion de comisiones y promotorias
	 */
	@Override
	public ConfigComisiones savePromotoriaConfig(ConfigComisiones config,List<ConfigComPromotoria> lista) throws Exception{
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		List<ConfigComPromotoria> relacionesActuales=findByProperty(ConfigComPromotoria.class,"configuracionComisiones.id",config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComPromotoria relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}				
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(lista!=null && !lista.isEmpty()){
			for(ConfigComPromotoria elemento:lista){
				if(isNotNull(elemento)){
					if(elemento.getPromotoria()==null || elemento.getPromotoria().getId()==null){
						onError("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con una promotoria");
					}
					elemento.setConfiguracionComisiones(config);
					Promotoria promotoriaAgenteIni = entidadService.findById(Promotoria.class,elemento.getPromotoria().getId());
					entidadService.refresh(promotoriaAgenteIni);
					elemento.setPromotoria(promotoriaAgenteIni);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	/**
	 * Elimina la relacion entre configuracion de comisiones y situaciones
	 */
	@Override
	public ConfigComisiones saveSituacionConfig(ConfigComisiones config,List<ConfigComSituacion> lista)throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		List<ConfigComSituacion> relacionesActuales=findByProperty(ConfigComSituacion.class,"configuracionComisiones.id",config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComSituacion relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(lista!=null && !lista.isEmpty()){
			for(ConfigComSituacion elemento:lista){
				if(isNotNull(elemento)){
					if(elemento.getSituacionAgente()==null || elemento.getSituacionAgente().getId()==null){
						System.out.println("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con una situacion");
						continue;
					}
					elemento.setConfiguracionComisiones(config);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	/**
	 * Guarda la relacion entre una configuracion y los tipos de agentes
	 */
	@Override
	public ConfigComisiones saveTipoAgenteConfig(ConfigComisiones config,List<ConfigComTipoAgente> lista)throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		List<ConfigComTipoAgente> relacionesActuales=findByProperty(ConfigComTipoAgente.class,"configuracionComisiones.id",config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComTipoAgente relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(lista!=null && !lista.isEmpty()){
			for(ConfigComTipoAgente elemento:lista){
				if(isNotNull(elemento)){
					if(elemento.getTipoAgente()==null || elemento.getTipoAgente().getId()==null){
						onError("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con un tipo agente");
					}
					elemento.setConfiguracionComisiones(config);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	/**
	 * Se guarda una relacion entre una configuracion y lista de tipos de promotorias
	 */
	@Override
	public ConfigComisiones saveTipoPromotoriaConfig(ConfigComisiones config, List<ConfigComTipoPromotoria> lista) throws Exception{
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		List<ConfigComTipoPromotoria> relacionesActuales=findByProperty(ConfigComTipoPromotoria.class,"configuracionComisiones.id",config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComTipoPromotoria relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(lista!=null && !lista.isEmpty()){
			for(ConfigComTipoPromotoria elemento:lista){
				if(isNotNull(elemento)){
					if(elemento.getTipoPromotoria()==null || elemento.getTipoPromotoria().getId()==null){
						onError("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con un tipo promotoria con id:"+elemento.getTipoPromotoria().getId());
					}
					elemento.setConfiguracionComisiones(config);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	
	/**
	 * Consulta para filtrar de acuerdo a la ventana de listado en configuracion de comisiones
	 * @param filtro
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ConfigComisionesDTO> findByFiltersView(ConfigComisionesDTO filtro){
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<ConfigComisionesDTO> lista=new ArrayList<ConfigComisionesDTO>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(
		   " select distinct "+ 
		   " COM.ID as ID,"+
		   " COM.CLAVEACTIVO as claveActivo,"+
		   " COM.CLAVEPAGOSINFACTURA as clavePagoSinFactura,"+
		   " COM.CLAVETODOSAGENTES as claveTodosLosAgentes,"+
		   " COM.DIAEJECUCION as diaEjecucion,"+
		   " COM.FECHAINICIOVIGENCIA as fechaInicioVigencia,"+
		   " COM.HORARIOEJECUCION as horarioEjecucion,"+
		   " COM.IDAGENTEFIN,"+
		   " COM.IDAGENTEINICIO,"+
		   " COM.IDMODOEJECUCION,"+
		   " COM.IDPERIODOEJECUCION,"+
		   " COM.IMPORTEMINIMO "+
		   " from MIDAS.TOCONFIGCOMISIONES com ");
			if(filtro!=null){
				if(filtro.getIdCentroOperacion()!=null){
					queryString.append(" left join MIDAS.TRCONFIGCOMCentroOperacion centro on(centro.configCom_id=COM.ID) ");
				}
				if(filtro.getIdGerencia()!=null){
					queryString.append(" left join MIDAS.TRCONFIGCOMGerencia gerencia on(gerencia.configCom_id=COM.ID)  ");
				}
				if(filtro.getIdEjecutivo()!=null){
					queryString.append(" left join MIDAS.TRCONFIGCOMEjecutivo ejecutivo on(ejecutivo.configCom_id=COM.ID)  ");
				}
				if(filtro.getIdPromotoria()!=null){
					queryString.append(" left join MIDAS.TRCONFIGCOMPromotoria promotoria on(promotoria.configCom_id=COM.ID)  ");
				}
				if(filtro.getIdTipoAgente()!=null){
					queryString.append(" left join MIDAS.TRCONFIGCOMTipoAgente clasificacionAgente on(clasificacionAgente.configCom_id=COM.ID)  ");
				}
				if(filtro.getIdTipoPromotoria()!=null){
					queryString.append(" left join MIDAS.TRCONFIGCOMTipoPromotoria tipoPromotoria on(tipoPromotoria.configCom_id=COM.ID) ");
				}
				if(filtro.getIdProridad()!=null){
					queryString.append(" left join MIDAS.TRCONFIGCOMPrioridad prioridad on(prioridad.configCom_id=COM.ID)  ");
				}
				if(filtro.getIdSituacion()!=null){
					queryString.append(" left join MIDAS.TRCONFIGCOMSituacion situacion on(situacion.configCom_id=COM.ID) ");
				}
				}
			queryString.append(" where ");
		if(filtro!=null){
			int index=1;
			if(filtro.getId()!=null){
					addCondition(queryString, " COM.ID=? ");
					params.put(index, filtro.getId());
					index++;
			}else{
				ConfigComisionesDTO filter=(ConfigComisionesDTO)filtro;
				if(filter.getIdCentroOperacion()!=null){
					addCondition(queryString, " centro.CENTROOPERACION_ID=? ");
					params.put(index,  filter.getIdCentroOperacion());
					index++;
				}
				if(filter.getIdGerencia()!=null){
					addCondition(queryString, " gerencia.GERENCIA_ID=? ");
					params.put(index,  filter.getIdGerencia());
					index++;
				}
				if(filter.getIdEjecutivo()!=null){
					addCondition(queryString, " ejecutivo.EJECUTIVO_ID=?");
					params.put(index,  filter.getIdEjecutivo());
					index++;
				}
				if(filter.getIdPromotoria()!=null){
					addCondition(queryString, " promotoria.PROMOTORIA_ID=?");
					params.put(index,  filter.getIdPromotoria());
					index++;
				}
				if(filter.getIdTipoAgente()!=null){
					addCondition(queryString, " clasificacionAgente.IDTIPOAGENTE=?");
					params.put(index,  filter.getIdTipoAgente());
					index++;
				}
				if(filter.getIdTipoPromotoria()!=null){
					addCondition(queryString, " tipoPromotoria.IDTIPOPROMOTORIA=?");
					params.put(index,  filter.getIdTipoPromotoria());
					index++;
				}
				if(filter.getIdProridad()!=null){
					addCondition(queryString, " prioridad.IDPRIORIDADAGENTE=?");
					params.put(index,  filter.getIdProridad());
					index++;
				}
				if(filter.getIdSituacion()!=null){
					addCondition(queryString, " situacion.IDSITUACIONAGENTE=?");
					params.put(index,  filter.getIdSituacion());
					index++;
				}
				ValorCatalogoAgentes modoEjecucion=filter.getModoEjecucion();
				if(modoEjecucion!=null && modoEjecucion.getId()!=null){
					addCondition(queryString, " com.idModoEjecucion=?");
					params.put(index,  modoEjecucion.getId());
					index++;
				}
				ValorCatalogoAgentes periodoEjecucion=filter.getPeriodoEjecucion();
				if(periodoEjecucion!=null && periodoEjecucion.getId()!=null){
					addCondition(queryString, " com.idPeriodoEjecucion=?");
					params.put(index,  periodoEjecucion.getId());
					index++;
				}
				if(filter.getFechaAltaInicio()!=null){
					addCondition(queryString, " com.fechaInicioVigencia>=? ");
					params.put(index, filter.getFechaAltaInicio());
					index++;
				}
				if(filter.getFechaAltaFin()!=null){
					addCondition(queryString, " com.fechaInicioVigencia<=?");
					params.put(index,  filter.getFechaAltaFin());
					index++;
				}
				if(filter.getAgenteInicio()!=null){
					addCondition(queryString, " com.idAgenteInicio=?");
					params.put(index,filter.getAgenteInicio());
					index++;
				}
				if(filter.getAgenteFin()!=null){
					addCondition(queryString, " com.idAgenteFin=?");
					params.put(index, filter.getAgenteFin());
					index++;
				}
			}
		}
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		String finalQuery = getQueryString(queryString)+" ORDER BY COM.ID ASC";
		Query query=entityManager.createNativeQuery(finalQuery,"ConfigComisionesView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		lista=query.getResultList();
		return lista;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComisiones> findByFilters(ConfigComisiones filtro) {
		Map<String,Object> params=new HashMap<String, Object>();
		List<ConfigComisiones> lista=new ArrayList<ConfigComisiones>();
		final StringBuilder queryString=new StringBuilder("");
		//queryString.append("select model from ConfigComisiones model left join fetch model.agenteInicio left join fetch model.agenteFin ");
		queryString.append("select model from ConfigComisiones model ");
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, " model.id=:id");
				params.put("id", filtro.getId());
			}else{
				ValorCatalogoAgentes modoEjecucion=filtro.getModoEjecucion();
				if(modoEjecucion!=null && modoEjecucion.getId()!=null){
					addCondition(queryString, " model.modoEjecucion.id=:idModoEjecucion");
					params.put("idModoEjecucion",  modoEjecucion.getId());
				}
				ValorCatalogoAgentes periodoEjecucion=filtro.getPeriodoEjecucion();
				if(periodoEjecucion!=null && periodoEjecucion.getId()!=null){
					addCondition(queryString, " model.periodoEjecucion.id=:idPeriodoEjecucion");
					params.put("idPeriodoEjecucion",  periodoEjecucion.getId());
				}
				if(filtro.getAgenteInicio()!=null){
					addCondition(queryString, " model.agenteInicio=:idAgenteInicio");
					params.put("idAgenteInicio",filtro.getAgenteInicio());
				}
				if(filtro.getAgenteFin()!=null){
					addCondition(queryString, " model.agenteFin=:idAgenteFin");
					params.put("idAgenteFin", filtro.getAgenteFin());
				}
			}
		}
		 Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
//			query =entityManager.createNativeQuery(null);
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		lista=query.getResultList();
		return lista;
	}

	@Override
	public ConfigComisiones loadById(ConfigComisiones config) throws Exception{
		if(config==null ||config.getId()==null){
			throw new Exception("Favor de proporcionar una configuracion de comisiones");
		}
		ConfigComisiones filtro=new ConfigComisiones();
		filtro.setId(config.getId());
		List<ConfigComisiones> list=findByFilters(filtro);
		if(list!=null && !list.isEmpty()){
			filtro=list.get(0);
			if(filtro!=null && filtro.getId()!=null){
				List<ConfigComCentroOperacion> centros=getCentrosOperacionPorConfiguracion(filtro);
				List<ConfigComGerencia> gerencias=getGerenciasPorConfiguracion(filtro);
				List<ConfigComEjecutivo> ejecutivos=getEjecutivosPorConfiguracion(filtro);
				List<ConfigComPromotoria> promotorias=getPromotoriasPorConfiguracion(filtro);
				List<ConfigComTipoAgente> tiposAgente=getTiposAgentePorConfiguracion(filtro);
				List<ConfigComTipoPromotoria> tiposPromotoria=getTiposPromotoriaPorConfiguracion(filtro);
				List<ConfigComPrioridad> prioridades=getPrioridadesPorConfiguracion(filtro);
				List<ConfigComSituacion> situaciones=getSituacionesPorConfiguracion(filtro);
				List<ConfigComMotivoEstatus> motivoEstatus=getMotivoEstatusPorConfiguracion(filtro);
				filtro.setListaCentroOperaciones(centros);
				filtro.setListaGerencias(gerencias);
				filtro.setListaEjecutivos(ejecutivos);
				filtro.setListaPromotorias(promotorias);
				filtro.setListaTipoAgentes(tiposAgente);
				filtro.setListaTiposPromotoria(tiposPromotoria);
				filtro.setListaPrioridades(prioridades);
				filtro.setListaSituaciones(situaciones);
				filtro.setListaMotivoEstatus(motivoEstatus);
			}
		}
		return filtro;
	}
	
	/**
	 * Desactiva una configuracion de comisiones por su id
	 */
	@Override
	public void deleteConfiguration(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion que desea eliminar");
		}
		ConfigComisiones configuracion=findById(ConfigComisiones.class,config.getId());
		if(configuracion==null){
			onError("La configuracion con la clave "+config.getId()+" no existe");
		}
		configuracion.setActivo(0);//Se inactiva la configuracion
		update(configuracion);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComCentroOperacion> getCentrosOperacionPorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComCentroOperacion.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT CENTROOPERACION_ID ");
		queryString.append("FROM MIDAS.trConfigComCentroOperacion  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object list  = query.getResultList();
		return (List<ConfigComCentroOperacion>) list;	
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getCentrosOperacionPorConfiguracionView(ConfigComisiones config) throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT CENTROOPERACION_ID as idCentroOperacion FROM MIDAS.trConfigComCentroOperacion  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return list;	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComEjecutivo> getEjecutivosPorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComEjecutivo.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT EJECUTIVO_ID ");
		queryString.append("FROM MIDAS.trConfigComEjecutivo  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object list  = query.getResultList();
		return (List<ConfigComEjecutivo>) list;	
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getEjecutivosPorConfiguracionView(ConfigComisiones config) throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT EJECUTIVO_ID as idEjecutivo FROM MIDAS.trConfigComEjecutivo  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return list;	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComGerencia> getGerenciasPorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComGerencia.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT GERENCIA_ID ");
		queryString.append("FROM MIDAS.trConfigComGerencia  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object list  = query.getResultList();
		return (List<ConfigComGerencia>) list;	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getGerenciasPorConfiguracionView(ConfigComisiones config) throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT GERENCIA_ID as idGerencia FROM MIDAS.trConfigComGerencia  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return list;	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComPrioridad> getPrioridadesPorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComPrioridad.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT IDPRIORIDADAGENTE ");
		queryString.append(" FROM MIDAS.trConfigComPrioridad ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object list  = query.getResultList();
		return (List<ConfigComPrioridad>) list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getPrioridadesPorConfiguracionView(ConfigComisiones config) throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT IDPRIORIDADAGENTE as idPrioridad FROM MIDAS.trConfigComPrioridad ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComPromotoria> getPromotoriasPorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComPromotoria.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT PROMOTORIA_ID ");
		queryString.append(" FROM MIDAS.trConfigComPromotoria ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object list  = query.getResultList();
		return (List<ConfigComPromotoria>) list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getPromotoriasPorConfiguracionView(ConfigComisiones config) throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT PROMOTORIA_ID as idPromotoria FROM MIDAS.trConfigComPromotoria ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComSituacion> getSituacionesPorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComSituacion.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT IDSITUACIONAGENTE ");
		queryString.append(" FROM MIDAS.trConfigComSituacion ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object list  = query.getResultList();
		return (List<ConfigComSituacion>) list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getSituacionesPorConfiguracionView(ConfigComisiones config) throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT IDSITUACIONAGENTE as idSituacion FROM MIDAS.trConfigComSituacion ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComTipoAgente> getTiposAgentePorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComTipoAgente.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT IDTIPOAGENTE ");
		queryString.append(" FROM MIDAS.trConfigComTipoAgente  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object list  = query.getResultList();
		return (List<ConfigComTipoAgente>) list;	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getTiposAgentePorConfiguracionView(ConfigComisiones config) throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(""); 
		queryString.append(" SELECT IDTIPOAGENTE as idTipoAgente FROM MIDAS.trConfigComTipoAgente  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return list;	
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigComTipoPromotoria> getTiposPromotoriaPorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComTipoPromotoria.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT IDTIPOPROMOTORIA ");
		queryString.append(" FROM MIDAS.trConfigComTipoPromotoria ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object list  = query.getResultList();
		return (List<ConfigComTipoPromotoria>) list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Long> getTiposPromotoriaPorConfiguracionView(ConfigComisiones config) throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT IDTIPOPROMOTORIA as idTipoPromotoria FROM MIDAS.trConfigComTipoPromotoria ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return (List<Long>) list;	
	}
	
	/**
	 * Obtiene la lista de gerencias que no esten en la lista de centros de operacion a excluir
	 * @param configGerencias
	 * @param configCentrosOperacion
	 * @return
	 */
	public List<GerenciaView> getGerenciasConCentrosOperacionExcluyentes(List<Long> configGerencias,List<Long> configCentrosOperacion){
		/*List<Gerencia> gerencias=new ArrayList<Gerencia>();
		List<CentroOperacion> centrosOperacion=new ArrayList<CentroOperacion>();
		if(!isEmptyList(configGerencias)){
			gerencias=ConfigComGerencia.toGerenciaList(configGerencias);
		}
		if(!isEmptyList(configCentrosOperacion)){
			centrosOperacion=ConfigComCentroOperacion.toCentroOperacionList(configCentrosOperacion);
		}*/
		return gerenciaService.findGerenciasConCentroOperacionExcluyentes(configGerencias, configCentrosOperacion);
	}
	/**
	 * Obtiene la lista de ejecutivos excluyendo aquellos que pertenezcan a la lista de gerencias a excluir
	 * @param configEjecutivos
	 * @param configGerencias
	 * @return
	 */
	@Override
	public List<EjecutivoView> getEjecutivosConGerenciasExcluyentes(List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros){
//		List<Ejecutivo> ejecutivos=new ArrayList<Ejecutivo>();
//		List<Gerencia> gerencias=new ArrayList<Gerencia>();
//		List<CentroOperacion> centros=new ArrayList<CentroOperacion>();
//		if(!isEmptyList(configEjecutivos)){
//			ejecutivos=ConfigComEjecutivo.toEjecutivoList(configEjecutivos);
//		}
//		if(!isEmptyList(configGerencias)){
//			gerencias=ConfigComGerencia.toGerenciaList(configGerencias);
//		}
//		if(!isEmptyList(configCentros)){
//			centros=ConfigComCentroOperacion.toCentroOperacionList(configCentros);
//		}
		return ejecutivoService.findEjecutivosConGerenciasExcluyentes(configEjecutivos, configGerencias,configCentros);
	}
	/**
	 * Obtiene la lista de promotorias excluyendo aquellas que pertenezcan a la lista de ejecutivos proporcionada.
	 * @param configPromotorias
	 * @param configEjecutivos
	 * @return
	 */
	public List<PromotoriaView> getPromotoriasConEjecutivosExcluyentes(List<Long> configPromotorias,List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros){
//		List<Ejecutivo> ejecutivos=new ArrayList<Ejecutivo>();
//		List<Gerencia> gerencias=new ArrayList<Gerencia>();
//		List<CentroOperacion> centros=new ArrayList<CentroOperacion>();
//		List<Promotoria> promotorias=new ArrayList<Promotoria>();
//		if(!isEmptyList(configPromotorias)){
//			promotorias=ConfigComPromotoria.toPromotoriaList(configPromotorias);
//		}
//		if(!isEmptyList(configEjecutivos)){
//			ejecutivos=ConfigComEjecutivo.toEjecutivoList(configEjecutivos);
//		}
//		if(!isEmptyList(configGerencias)){
//			gerencias=ConfigComGerencia.toGerenciaList(configGerencias);
//		}
//		if(!isEmptyList(configCentros)){
//			centros=ConfigComCentroOperacion.toCentroOperacionList(configCentros);
//		}
		return promotoriaService.findPromotoriaConEjecutivosExcluyentes(configPromotorias, configEjecutivos,configGerencias,configCentros);
	}
	
	public List<AgenteView> obtenerAgentesPorConfiguracion(ConfigComisiones configuracion) throws Exception{
		List<AgenteView> list=new ArrayList<AgenteView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Gerencia filtro=new Gerencia();
		final StringBuilder queryString=new StringBuilder("");
		List<CentroOperacionView> centros=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaCentroOperacionView()))?configuracion.getListaCentroOperacionView():null;
		List<EjecutivoView> ejecutivos=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaEjecutivoView()))?configuracion.getListaEjecutivoView():null;
		List<GerenciaView> gerencias=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaGerenciaView()))?configuracion.getListaGerenciaView():null;
		List<PromotoriaView> promotorias=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaPromotoriaView()))?configuracion.getListaPromotoriaView():null;
		List<ConfigComSituacion> situaciones=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaSituaciones()))?configuracion.getListaSituaciones():null;
		List<ConfigComTipoAgente> tiposAgente=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaTipoAgentes()))?configuracion.getListaTipoAgentes():null;
		List<ConfigComTipoPromotoria> tiposPromotoria=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaTiposPromotoria()))?configuracion.getListaTiposPromotoria():null;
		List<ConfigComPrioridad> prioridades=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaPrioridades()))?configuracion.getListaPrioridades():null;
		List<ConfigComMotivoEstatus> motivosEstatus=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaMotivoEstatus()))?configuracion.getListaMotivoEstatus():null;
		queryString.append(" select distinct ");
		queryString.append(" agente.id,");
		queryString.append(" agente.idAgente as idAgente,");
//		queryString.append(" tipoAgente.valor as tipoAgente,");
//		queryString.append(" tipoAgente.id as idTipoAgente, ");
		queryString.append(" prioridad.valor as prioridad,");
//		queryString.append(" prioridad.id as idPrioridad,");
		queryString.append(" estatus.valor as tipoSituacion,");
//		queryString.append(" estatus.id as idEstatus,");
		queryString.append(" persona.nombreCompleto as nombreCompleto,");
		queryString.append(" promo.descripcion as promotoria,");
//		queryString.append(" promo.id as idPromotoria,");
//		queryString.append(" tipoPromotoria.valor as tipoPromotoria,");
//		queryString.append(" tipoPromotoria.id as idTipoPromotoria,"); 
//		queryString.append(" personaEj.nombreCompleto as ejecutivo,");
//		queryString.append(" ej.id as idEjecutivo,");
		queryString.append(" ger.descripcion as gerencia ");
//		queryString.append(" ger.id as idGerencia,");
//		queryString.append(" co.descripcion as centroOperacion,");
//		queryString.append(" co.idCentroOperacion  ");
		queryString.append(" from MIDAS.toAgente agente ");
		queryString.append(" inner join MIDAS.vw_persona persona on (persona.idpersona=agente.idpersona) ");
		queryString.append(" inner join MIDAS.toPromotoria promo on(agente.idpromotoria=promo.id)  ");
		queryString.append(" inner join MIDAS.toEjecutivo ej on(ej.id=promo.ejecutivo_id)  ");
		queryString.append(" inner join MIDAS.vw_persona personaEj on (personaEj.idpersona=ej.idpersona) ");
		queryString.append(" inner join MIDAS.toGerencia ger on (ej.gerencia_id=ger.id) ");
		queryString.append(" inner join MIDAS.toCentroOperacion co on (ger.centroOperacion_id=co.id) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes clasificacionAgente on(clasificacionAgente.id=agente.clasificacionagentes) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoPromotoria on (tipoPromotoria.id=promo.idTipoPromotoria) "); 
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes prioridad on (prioridad.id=agente.idPrioridadAgente) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes estatus on (estatus.id=agente.idSituacionAgente) ");
		//se agrego el motivo de estatus
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes motivoEstatus on (motivoEstatus.id=agente.IDMOTIVOESTATUSAGENTE) ");
		queryString.append(" where ");
		boolean isEmptyParams=true;
		if(isNotNull(configuracion)){
			int index=1;
			if(filtro.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtro.getClaveEstatus());
				index++;
				isEmptyParams=false;
			}
			String agentes = configuracion.getAgentes();		
			if (agentes != (null)) {
				agentes = generaidAgentes(agentes);	
			}else{
				agentes = "";
			}
			/*
			 * Agregar nuevo codigo para el envio del String guardado asignando
			 * valor de Ejemplo in(8115,8125,8165,8455,8562,8050-8053) _Ozamora!
			 */
			boolean isAgentes = (isNotNull(agentes)) ? true : false;
			if(agentes !=""){
				if (isNotNull(agentes)) {
					isEmptyParams = false;
					queryString.append(" agente.idAgente in(" + agentes + ") and ");
				}
			}
			if (configuracion.getTipoCedulaId() != null) {
				if (!StringUtils.contains(queryString.toString(),"where")) {
					queryString.append("where agente.idtipocedulaagente = " + configuracion.getTipoCedulaId());
				}else {
					queryString.append("agente.idtipocedulaagente = " + configuracion.getTipoCedulaId() + " and ");
				}
			}
			if(!isEmptyList(centros)){
//				addOrCondition(queryString, " co.id in (?) ");
				queryString.append(" co.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(CentroOperacionView centro:centros){
					if(isNotNull(centro) && isNotNull(centro.getId())){
						if(i<(centros.size()-1)){
							lista.append(centro.getId()+",");
						}else{
							lista.append(centro.getId());
						}
					}
					i++;
				}
				//params.put(index, lista.toString());
				//index++;
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(gerencias)){
//				addOrCondition(queryString, " ger.id in (?) ");
				queryString.append(" ger.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(GerenciaView gerencia:gerencias){
					if(isNotNull(gerencia) && isNotNull(gerencia.getId())){
						if(i<(gerencias.size()-1)){
							lista.append(gerencia.getId()+",");
						}else{
							lista.append(gerencia.getId());
						}
					}
					i++;
				}
//				params.put(index, lista.toString());
//				index++;
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(ejecutivos)){
//				addOrCondition(queryString, " ej.id in (?) ");
				queryString.append(" ej.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(EjecutivoView ejecutivo:ejecutivos){
					if(isNotNull(ejecutivo) && isNotNull(ejecutivo.getId())){
						if(i<(ejecutivos.size()-1)){
							lista.append(ejecutivo.getId()+",");
						}else{
							lista.append(ejecutivo.getId());
						}
					}
					i++;
				}
//				params.put(index, lista.toString());
//				index++;
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(promotorias)){
//				addOrCondition(queryString, " promo.id in (?) ");
				queryString.append(" promo.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(PromotoriaView promotoria:promotorias){
					if(isNotNull(promotoria) && isNotNull(promotoria.getId())){
						if(i<(promotorias.size()-1)){
							lista.append(promotoria.getId()+",");
						}else{
							lista.append(promotoria.getId());
						}
					}
					i++;
				}
//				params.put(index, lista.toString());
//				index++;
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(situaciones)){
//				int position=queryString.lastIndexOf(" or");
//				if(position>0){
//					queryString.replace(queryString.lastIndexOf(" or"),queryString.lastIndexOf(" or")+2,"");
//				}
				queryString.append("  estatus.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComSituacion situacion:situaciones){
					ValorCatalogoAgentes catalogo=(isNotNull(situacion))?situacion.getSituacionAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(situaciones.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
//				params.put(index, lista.toString());
//				index++;
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(tiposAgente)){
//				int position=queryString.lastIndexOf(" or");
//				if(position>0){
//					queryString.replace(queryString.lastIndexOf(" or"),queryString.lastIndexOf(" or")+2,"");
//				}
				queryString.append("  clasificacionAgente.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComTipoAgente clasificacionagente:tiposAgente){
					ValorCatalogoAgentes catalogo=(isNotNull(clasificacionagente))?clasificacionagente.getTipoAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(tiposAgente.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
//				params.put(index, lista.toString());
//				index++;
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(tiposPromotoria)){
//				int position=queryString.lastIndexOf(" or");
//				if(position>0){
//					queryString.replace(queryString.lastIndexOf(" or"),queryString.lastIndexOf(" or")+2,"");
//				}
				queryString.append("  tipoPromotoria.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComTipoPromotoria tipoPromotoria:tiposPromotoria){
					ValorCatalogoAgentes catalogo=(isNotNull(tipoPromotoria))?tipoPromotoria.getTipoPromotoria():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(tiposPromotoria.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
//				params.put(index, lista.toString());
//				index++;
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			
			if(!isEmptyList(motivosEstatus)){
				queryString.append("  motivoEstatus.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComMotivoEstatus motivoEstatus:motivosEstatus){
					ValorCatalogoAgentes catalogo=(isNotNull(motivoEstatus))?motivoEstatus.getIdMotivoEstatus():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(motivosEstatus.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
//				params.put(index, lista.toString());
//				index++;
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			
			if(!isEmptyList(prioridades)){
//				int position=queryString.lastIndexOf(" or");
//				if(position>0){
//					queryString.replace(queryString.lastIndexOf(" or"),queryString.lastIndexOf(" or")+2,"");
//				}
				queryString.append("  prioridad.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComPrioridad prioridad:prioridades){
					ValorCatalogoAgentes catalogo=(isNotNull(prioridad))?prioridad.getPrioridadAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(prioridades.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
//				params.put(index, lista.toString());
//				index++;
				queryString.append(lista.toString()+") ");
				isEmptyParams=false;
			}
			if(isEmptyParams){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			
			
			String finalQuery=getQueryString(queryString)+" order by agente.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,AgenteView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
		}
		return list;	
	}
	
	public String obtenerAgentesPorConfiguracionQuery(ConfigComisiones configuracion) throws Exception{
		String finalQuery=null;
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		List<CentroOperacionView> centros=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaCentroOperacionView()))?configuracion.getListaCentroOperacionView():null;
		List<EjecutivoView> ejecutivos=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaEjecutivoView()))?configuracion.getListaEjecutivoView():null;
		List<GerenciaView> gerencias=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaGerenciaView()))?configuracion.getListaGerenciaView():null;
		List<PromotoriaView> promotorias=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaPromotoriaView()))?configuracion.getListaPromotoriaView():null;
		List<ConfigComSituacion> situaciones=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaSituaciones()))?configuracion.getListaSituaciones():null;
		List<ConfigComTipoAgente> tiposAgente=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaTipoAgentes()))?configuracion.getListaTipoAgentes():null;
		List<ConfigComTipoPromotoria> tiposPromotoria=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaTiposPromotoria()))?configuracion.getListaTiposPromotoria():null;
		List<ConfigComPrioridad> prioridades=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaPrioridades()))?configuracion.getListaPrioridades():null;
		List<ConfigComMotivoEstatus> motivosEstatus=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaMotivoEstatus()))?configuracion.getListaMotivoEstatus():null;
		queryString.append(" select /*+INDEX(promo TOPROMOTORIA_PK) INDEX(ej TOEJECUTIVO_PK) INDEX(ger TOGERENCIA_PK) INDEX() INDEX(agente TOAGENTE_PK)*/ distinct ");
		queryString.append(" agente.id as idRegistro,");
		queryString.append(" 'AGENTE' as catalogo ");
		queryString.append(" from MIDAS.toAgente agente ");
		queryString.append(" inner join MIDAS.vw_persona persona on (persona.idpersona=agente.idpersona) ");
		queryString.append(" inner join MIDAS.toPromotoria promo on(agente.idpromotoria=promo.id)  ");
		queryString.append(" inner join MIDAS.toEjecutivo ej on(ej.id=promo.ejecutivo_id)  ");
		queryString.append(" inner join MIDAS.vw_persona personaEj on (personaEj.idpersona=ej.idpersona) ");
		queryString.append(" inner join MIDAS.toGerencia ger on (ej.gerencia_id=ger.id) ");
		queryString.append(" inner join MIDAS.toCentroOperacion co on (ger.centroOperacion_id=co.id) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes clasificacionagente on(clasificacionagente.id=agente.clasificacionagentes) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoPromotoria on (tipoPromotoria.id=promo.idTipoPromotoria) "); 
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes prioridad on (prioridad.id=agente.idPrioridadAgente) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes estatus on (estatus.id=agente.idSituacionAgente) ");
		//se agrego el motivo de estatus
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes motivoEstatus on (motivoEstatus.id=agente.IDMOTIVOESTATUSAGENTE) ");
		queryString.append(" where ");
		boolean isEmptyParams=true;
		if(isNotNull(configuracion)){
			int index=1;
			
			String agentes = configuracion.getAgentes();
			if (agentes != (null)) {
				agentes = generaidAgentes(agentes);	
			}else{
				agentes = "";
			}
			/*
			 * Agregar nuevo codigo para el envio del String guardado asignando
			 * valor de Ejemplo in(8115,8125,8165,8455,8562,8050-8053) _Ozamora!
			 */
			boolean isAgentes = (isNotNull(agentes)) ? true : false;
			if(agentes !=""){
				if (isNotNull(agentes)) {
					isEmptyParams = false;
					queryString.append(" agente.idAgente in(" + agentes + ") and ");
				}
			}
			
			if(!isEmptyList(centros)){
				queryString.append(" co.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(CentroOperacionView centro:centros){
					if(isNotNull(centro) && isNotNull(centro.getId())){
						if(i<(centros.size()-1)){
							lista.append(centro.getId()+",");
						}else{
							lista.append(centro.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(gerencias)){
				queryString.append(" ger.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(GerenciaView gerencia:gerencias){
					if(isNotNull(gerencia) && isNotNull(gerencia.getId())){
						if(i<(gerencias.size()-1)){
							lista.append(gerencia.getId()+",");
						}else{
							lista.append(gerencia.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(ejecutivos)){
				queryString.append(" ej.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(EjecutivoView ejecutivo:ejecutivos){
					if(isNotNull(ejecutivo) && isNotNull(ejecutivo.getId())){
						if(i<(ejecutivos.size()-1)){
							lista.append(ejecutivo.getId()+",");
						}else{
							lista.append(ejecutivo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(promotorias)){
				queryString.append(" promo.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(PromotoriaView promotoria:promotorias){
					if(isNotNull(promotoria) && isNotNull(promotoria.getId())){
						if(i<(promotorias.size()-1)){
							lista.append(promotoria.getId()+",");
						}else{
							lista.append(promotoria.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(situaciones)){
				queryString.append("  estatus.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComSituacion situacion:situaciones){
					ValorCatalogoAgentes catalogo=(isNotNull(situacion))?situacion.getSituacionAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(situaciones.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(tiposAgente)){
				queryString.append("  clasificacionagente.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComTipoAgente clasificacionAgente:tiposAgente){
					ValorCatalogoAgentes catalogo=(isNotNull(clasificacionAgente))?clasificacionAgente.getTipoAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(tiposAgente.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(tiposPromotoria)){
				queryString.append("  tipoPromotoria.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComTipoPromotoria tipoPromotoria:tiposPromotoria){
					ValorCatalogoAgentes catalogo=(isNotNull(tipoPromotoria))?tipoPromotoria.getTipoPromotoria():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(tiposPromotoria.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			
			if(!isEmptyList(motivosEstatus)){
				queryString.append("  motivoEstatus.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComMotivoEstatus motivoEstatus:motivosEstatus){
					ValorCatalogoAgentes catalogo=(isNotNull(motivoEstatus))?motivoEstatus.getIdMotivoEstatus():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(motivosEstatus.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(prioridades)){
				queryString.append("  prioridad.id in ( ");
				StringBuilder lista=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(ConfigComPrioridad prioridad:prioridades){
					ValorCatalogoAgentes catalogo=(isNotNull(prioridad))?prioridad.getPrioridadAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(prioridades.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") ");
				isEmptyParams=false;
			}
			if(isEmptyParams){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			finalQuery=getQueryString(queryString)+" order by agente.id desc ";
		}
		return finalQuery;	
	}
	
	public String generaidAgentes(String agentes) {
		ArrayList<String> listStr = new ArrayList<String>();
		ArrayList<Integer> listInt = new ArrayList<Integer>();
		String output ="";
		
		int min = 0, max = 0;
		int ya = 0;
		
		if(!agentes.equalsIgnoreCase("")){
			
		String[] elementos = agentes.split(",");
		String strelementos;

		String[] numeros;

		String numeroAgregado;
		String mina, maxi;
		
		
		for (int i = 0; i < elementos.length; i++)// separas por COMAS
		{

			strelementos = elementos[i];

			if (strelementos.indexOf("-") != -1) {

				numeros = strelementos.split("-");// separas por GUION

				if (numeros.length > 0) {
					for (ya = Integer.parseInt(numeros[0]); ya <= Integer
							.parseInt(numeros[1]); ya++)

					{

						String strRango = Integer.toString(ya);
						listStr.add(strRango);// en vez de uno pones el numero
												// que corresponde a Y
					}

				}

			} else
				listStr.add(elementos[i]);

		}

		ArrayList<String> list = listGet(listStr);
		
		for (String str : list) {
			output = output + str + ",";
		}

		String ultimoCaracter = output.substring(output.length() - 1);

		if (ultimoCaracter.equals(",")) {
			output = output.substring(0, output.length() - 1);
		}
	}
		
		return output;
	}

	public static ArrayList<String> listGet(List listStr) {
		ArrayList<String> listSt = (ArrayList<String>) listStr;
		ArrayList<String> lista = new ArrayList<String>();
		// Al utilizar un HashSet se eliminan todos los duplicados y luego lo
		lista = new ArrayList<String>(new HashSet<String>(listSt));
		Collections.sort(lista);
		return lista;
	}
	
	
	@Override
	public List<ConfigComMotivoEstatus> getMotivoEstatusPorConfiguracion(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ID as id, IDMOTIVOESTATUS as idMotivoEstatus ");
		queryString.append(" FROM MIDAS.TRCONFIGCOMMOTESTATUS  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(),ConfigComMotivoEstatus.class);
		List<ConfigComMotivoEstatus> list  = query.getResultList();
		return  list;
	}
	
	@Override
	public List<Long> getMotivoEstatusPorConfiguracionView(ConfigComisiones config)	throws Exception {
		List<Long> list=new ArrayList<Long>();
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" select idMotivoEstatus from MIDAS.TRCONFIGCOMMOTESTATUS ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			for(BigDecimal elem:lista){
				if(isNotNull(elem)){
					list.add(elem.longValue());
				}
			}
		}
		return list;
	}

	@Override
	public ConfigComisiones saveMotivoEstatus(ConfigComisiones config,List<ConfigComMotivoEstatus> listMotivoEstatus) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		
		List<ConfigComMotivoEstatus> relacionesActuales=findByProperty(ConfigComMotivoEstatus.class,"configuracionComisiones.id",config.getId());
		//Si no esta vacia las relaciones con esa configuracion,
		//Entonces se borran y luego se vuelven a insertar
		if(relacionesActuales!=null && !relacionesActuales.isEmpty()){
			for(ConfigComMotivoEstatus relacion:relacionesActuales){
				if(relacion==null || relacion.getId()==null){
					continue;
				}
				entidadService.remove(relacion);
			}
		}
		//Se vuelven a insertar
		if(listMotivoEstatus!=null && !listMotivoEstatus.isEmpty()){
			for(ConfigComMotivoEstatus elemento:listMotivoEstatus){
				if(isNotNull(elemento)){
					if(elemento.getIdMotivoEstatus()==null || elemento.getIdMotivoEstatus().getId()==null){
						onError("No se ha logrado agregar la configuracion de la comision "+config.getId()+" con un motivo de estatus");
					}
					elemento.setConfiguracionComisiones(config);
					entidadService.save(elemento);
				}
			}
		}
		return config;
	}
	
//**********************************************************************************
	@Override
	public List<ItemsConfigComision> getCentrosOperacionPorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ROWNUM as id, CENTROOPERACION_ID as idElemento ");
		queryString.append(" FROM MIDAS.trConfigComCentroOperacion  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		
		return list;	
	}

	@Override
	public List<ItemsConfigComision> getEjecutivosPorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ROWNUM as id, EJECUTIVO_ID as idElemento ");
		queryString.append(" FROM MIDAS.trConfigComEjecutivo  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(),ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		return list;	
	}

	@Override
	public List<ItemsConfigComision> getGerenciasPorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ROWNUM as id, GERENCIA_ID as idElemento");
		queryString.append(" FROM MIDAS.trConfigComGerencia  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		return list;
	}

	@Override
	public List<ItemsConfigComision> getMotivoEstatusPorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ID as id, IDMOTIVOESTATUS as idElemento ");
		queryString.append(" FROM MIDAS.TRCONFIGCOMMOTESTATUS  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(),ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		return  list;
	}

	@Override
	public List<ItemsConfigComision> getPrioridadesPorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ROWNUM as id, IDPRIORIDADAGENTE as idElemento ");
		queryString.append(" FROM MIDAS.trConfigComPrioridad ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		return list;
	}

	@Override
	public List<ItemsConfigComision> getPromotoriasPorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT  ROWNUM as id, PROMOTORIA_ID as idElemento ");
		queryString.append(" FROM MIDAS.trConfigComPromotoria ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(),ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		return list;
	}

	@Override
	public List<ItemsConfigComision> getSituacionesPorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComSituacion.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ROWNUM as id, IDSITUACIONAGENTE as idElemento ");
		queryString.append(" FROM MIDAS.trConfigComSituacion ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(),ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		return list;
	}

	@Override
	public List<ItemsConfigComision> getTiposAgentePorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ROWNUM as id, IDTIPOAGENTE as idElemento ");
		queryString.append(" FROM MIDAS.trConfigComTipoAgente  ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(),ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		return  list;
	}

	@Override
	public List<ItemsConfigComision> getTiposPromotoriaPorConfigChecked(ConfigComisiones config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append(" SELECT ROWNUM as id, IDTIPOPROMOTORIA as idElemento ");
		queryString.append(" FROM MIDAS.trConfigComTipoPromotoria ");
		queryString.append(" WHERE  CONFIGCOM_ID = "+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(),ItemsConfigComision.class);
		List<ItemsConfigComision> list  = query.getResultList();
		return list;
	}
	
	@Override
	public String obtenerAgentesCuentaAfirme() throws Exception {
		
		String claveAfirme = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_BANCO_AFIRME, PARAMETRO_CLAVE_AFIRME), true).getValor();
		
		StringBuffer queryString = new StringBuffer();
		queryString.append(" SELECT TA.IDAGENTE FROM MIDAS.TOPRODUCTOBANCARIOAGENTE TPBA ");
		queryString.append(" INNER JOIN MIDAS.TOAGENTE TA ON TA.ID= TPBA.AGENTE_ID AND TA.IDSITUACIONAGENTE = '112' ");
		queryString.append(" INNER JOIN MIDAS.TCPRODUCTOBANCARIO TPB ON TPB.ID = TPBA.PRODUCTOBANCARIO_ID ");
		queryString.append(" INNER JOIN MIDAS.VW_BANCO VB ON VB.IDBANCO = TPB.IDBANCO ");
		queryString.append(" INNER JOIN SEYCOS.ING_BANCO INBA  ON INBA.ID_BANCO = VB.IDBANCO AND INBA.CVEL_EXT_TRANSF = '"+claveAfirme+"' ");
		
		Query query = entityManager.createNativeQuery(queryString.toString());

		List<BigDecimal> listaAgentes =  query.getResultList();
		
		StringBuilder noAgentesBuilder = new StringBuilder();

		for (BigDecimal agente :listaAgentes) {
			noAgentesBuilder.append(agente);
			noAgentesBuilder.append(SEPARADOR_COMA);
		}

		String noAgentes = noAgentesBuilder.toString();
		noAgentes = noAgentes.substring(0, noAgentes.length() - SEPARADOR_COMA.length());

		return noAgentes;
	}
	
	/************************************************************************************
	 * ==================================================================================
	 * 	Common methods , sets and gets
	 * ==================================================================================
	 * 
	 */
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		if(query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private void addOrCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" or ");
	}
	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	/**
	 * metodo para lanzar errores
	 * @param msg
	 * @throws Exception
	 */
	private void onError(String msg)throws Exception{
		throw new Exception(msg);
	}
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	@EJB
	public void setCentroOperacionService(
			CentroOperacionService centroOperacionService) {
		this.centroOperacionService = centroOperacionService;
	}
	@EJB
	public void setGerenciaService(GerenciaJPAService gerenciaService) {
		this.gerenciaService = gerenciaService;
	}
	@EJB
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}
	@EJB
	public void setPromotoriaService(PromotoriaJPAService promotoriaService) {
		this.promotoriaService = promotoriaService;
	}
	@EJB
	public void setParametroGeneralFacade(ParametroGeneralFacadeRemote parametroGeneralFacade) {
		this.parametroGeneralFacade = parametroGeneralFacade;
	}

	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> arg0, String arg1, Map<String, Object> arg2,
			Map<String, Object> arg3, StringBuilder arg4, StringBuilder arg5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> arg0, Map<String, Object> arg1, Map<String, Object> arg2,
			StringBuilder arg3, StringBuilder arg4) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}
}
