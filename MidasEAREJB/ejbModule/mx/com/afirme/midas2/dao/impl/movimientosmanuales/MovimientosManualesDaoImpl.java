package mx.com.afirme.midas2.dao.impl.movimientosmanuales;

import static mx.com.afirme.midas2.utils.CommonUtils.addCondition;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;
import static mx.com.afirme.midas2.utils.CommonUtils.setQueryParametersByProperties;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.impl.prestamos.PagarePrestamoAnticipoDaoImpl;
import mx.com.afirme.midas2.dao.movimientosmanuales.MovimientosManualesDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movimientosmanuales.MovimientosManuales;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class MovimientosManualesDaoImpl  extends EntidadDaoImpl implements MovimientosManualesDao{
	
	
	
	@Override
	public List<MovimientosManuales> findByFilters(MovimientosManuales filtro) throws Exception{
		List<MovimientosManuales> list=new ArrayList<MovimientosManuales>();
		final StringBuilder queryString=new StringBuilder("");
		Agente agente=filtro.getAgente();
		
		queryString.append("select model from MovimientosManuales model ");
		
		if(agente!=null){
			queryString.append("join fetch model.agente ");
		}
			
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){			
			if(filtro.getId()!=null && filtro.getId()!= 0L){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}else{
				if(filtro.getIdEmpresa()!=null && filtro.getIdEmpresa()!= 0L){				
                    addCondition(queryString, "model.idEmpresa=:idEmpresa");
                    params.put("idEmpresa", filtro.getIdEmpresa());
                }
				/*if(filtro.getIdConsecMovto()!=null && filtro.getIdConsecMovto()!= 0L){
					addCondition(queryString, "model.idConsecMovto=:idConsecMovto");
					params.put("idConsecMovto", filtro.getIdConsecMovto());
				}*/
				if(filtro.getIdConcepto() != null && filtro.getIdConcepto()!= 0L){
					addCondition(queryString, "model.idConcepto=:idConcepto");
					params.put("idConcepto", filtro.getIdConcepto());										
				}
				
				if(filtro.getIdRamoContable() != null && filtro.getIdRamoContable().length()!=0){
					addCondition(queryString, "model.idRamoContable=:idRamoContable");
					params.put("idRamoContable", filtro.getIdConcepto());										
				}
				
				if(filtro.getIdUsuarioCreacion() != null && !filtro.getIdUsuarioCreacion().isEmpty())
				{
					addCondition(queryString, "TRIM(model.idUsuarioCreacion) = :idUsuarioCreacion");
					params.put("idUsuarioCreacion", filtro.getIdUsuarioCreacion().trim());					
				}
				
				if(filtro.getFechaInicioPeriodo() != null)
				{
					addCondition(queryString, "model.fechaCreacion >= :fechaInicioPeriodo");
					params.put("fechaInicioPeriodo", filtro.getFechaInicioPeriodo());					
				}
				
				if(filtro.getFechaFinPeriodo() != null)
				{
					addCondition(queryString, "model.fechaCreacion <= :fechaFinPeriodo");
					params.put("fechaFinPeriodo", filtro.getFechaFinPeriodo());					
				}
				
				if(agente!=null){
					if(agente.getIdAgente()!=null && agente.getIdAgente()!=0L){
						addCondition(queryString, "model.agente.idAgente=:idAgente");
						params.put("idAgente",agente.getIdAgente());
					}
				}				
			}
		}
		
		addCondition(queryString, "model.estatusRegistro in (0,2)");
		
		String q=getQueryString(queryString);
		//q+=" order by model.fechaCreacion desc ";
		Query query = entityManager.createQuery(q);
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}
	
	@Override
	public int aplicarMovimientoManual(Long idMovimientoManual, String idUsuarioAplica) throws Exception
	{
		int resultadoMovimientoAplicado = 0;
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS.PKGCALCULOS_AGENTES.spAplicaMovimientoManual";
		
		try {
			LogDeMidasInterfaz.log("Entrando a aplicarMovimientoManual..." + this, Level.INFO, null);
							
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pIdMovManual",idMovimientoManual);
			storedHelper.estableceParametro("pUsuarioAplica",idUsuarioAplica);
				
//			resultadoMovimientoAplicado =  
				storedHelper.ejecutaActualizar();
			
			LogDeMidasInterfaz.log("Saliendo de aplicarMovimientoManual, id movimiento aplicado: "+idMovimientoManual+"..." + this, Level.INFO, null);
			
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,MovimientosManualesDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de stp_aplicarMovimientoManualTest..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en stp_aplicarMovimientoManualTest..." + this, Level.WARNING, e);
			onError(e);
		}		
		
		return resultadoMovimientoAplicado;
	}	
	
	@Override
	public int eliminarMovimientoManual(Long idMovimientoManual)
	{
		int resultadoMovimientoEliminado = 0;
		
		MovimientosManuales movimiento = this.findById(MovimientosManuales.class, idMovimientoManual);
		this.remove(movimiento);
		
		return resultadoMovimientoEliminado;		
	}
}
