package mx.com.afirme.midas2.dao.impl.catalogos.riesgo;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas2.dao.catalogos.riesgo.RiesgoDTODao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class RiesgoDTODaoImpl  extends JpaDao<BigDecimal,RiesgoDTO> implements RiesgoDTODao  {
	
	protected EntidadService entidadService;

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}


	@Override
	public List<RiesgoDTO> getListarFiltrado(RiesgoDTO riesgoDTO,Boolean mostrarInactivos) {		
		Map<String, Object> map;
		String queryString = "";
		map = new HashMap<String, Object>();
		
		queryString = "select model from RiesgoDTO model, " +
				"SubRamoDTO as sub  where model.subRamoDTO.idTcSubRamo=sub.idTcSubRamo ";		
		if (riesgoDTO.getIdTcRamo()!=null){
			queryString = queryString + "and sub.ramoDTO.idTcRamo = :idTcRamo ";
			map.put("idTcRamo", riesgoDTO.getIdTcRamo().intValue());
		}
		if (riesgoDTO.getCodigo()!=null && !riesgoDTO.getCodigo().equals("")){
			queryString = queryString + "and model.codigo like '%" + riesgoDTO.getCodigo() + "%' ";
			//map.put("codigo", riesgoDTO.getCodigo());
		}
		if (riesgoDTO.getVersion()!=null && !riesgoDTO.getVersion().toString().equals("")){
			queryString = queryString + "and model.version = :version ";
			map.put("version", riesgoDTO.getVersion());
		}
		if (riesgoDTO.getDescripcion()!=null && !riesgoDTO.getDescripcion().equals("")){
			queryString = queryString + "and model.descripcion = :descripcion ";
			map.put("descripcion", riesgoDTO.getDescripcion());
		}
		if (riesgoDTO.getNombreComercial()!=null && !riesgoDTO.getNombreComercial().equals("")){
			queryString = queryString + "and model.nombreComercial = :nombreComercial ";
			map.put("nombreComercial", riesgoDTO.getNombreComercial());
		}
		if (riesgoDTO.getSubRamoDTO().getIdTcSubRamo()!=null){
			queryString = queryString + "and model.subRamoDTO.idTcSubRamo = :subRamo ";
			map.put("subRamo", riesgoDTO.getSubRamoDTO().getIdTcSubRamo());
		}
		if(!mostrarInactivos) {
			queryString = queryString + " and model.claveEstatus IN (0,1) ";
		}else{
			queryString = queryString + " and model.claveEstatus IN (1,2,3) ";
		}
		@SuppressWarnings({ "unchecked" })
		List<RiesgoDTO> riesgos = entidadService.executeQueryMultipleResult(queryString, map);
		return riesgos;
	}
	
	public List<RiesgoDTO> listarVigentesAutos(){
		Map<String, Object> map;
		String queryString = "";
		map = new HashMap<String, Object>();
		
		queryString = "select model from RiesgoDTO as model, " +
			"SubRamoDTO as sub  where model.subRamoDTO.idTcSubRamo=sub.idTcSubRamo " +
			"and sub.ramoDTO.idTcRamo = :idTcRamo " +
			"and model.claveEstatus <> 3";
		map.put("idTcRamo", 4);
		
		@SuppressWarnings({ "unchecked" })
		List<RiesgoDTO> riesgos = entidadService.executeQueryMultipleResult(queryString, map);
		return riesgos;
	}

}
