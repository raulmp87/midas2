package mx.com.afirme.midas2.dao.impl.domicilio;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.DomicilioSeycos;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;

import org.apache.commons.collections.CollectionUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.springframework.beans.BeanUtils;

@Stateless
public class DomicilioFacade implements DomicilioFacadeRemote{
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Domicilio findById(Long idDomicilio, Long idPersona) throws Exception{
		if(idDomicilio==null){
			throw new Exception("IdDomicilio is null");
		}
		Domicilio filtro=new Domicilio();
		DomicilioPk pkDomicilio=new DomicilioPk();
		pkDomicilio.setIdPersona(idPersona);
		pkDomicilio.setIdDomicilio(idDomicilio);
		filtro.setIdDomicilio(pkDomicilio);
		Domicilio domicilio=null;
		List<Domicilio> domicilios=findByFilters(filtro);
		if(domicilios!=null && !domicilios.isEmpty()){
			domicilio=domicilios.get(0);
		}
//		StoredProcedureHelper storedHelper = null;	
//		String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATDOMICILIO";
//		try {
//			LogDeMidasInterfaz.log("Entrando a DomicilioFacade.findById..." + this, Level.INFO, null);
//			storedHelper = new StoredProcedureHelper(sp);
//			Long idPersona=(domicilio.getPersona()!=null)?domicilio.getPersona().getIdPersona():null;
//			StringBuilder propiedades=new StringBuilder("");
//			StringBuilder columnas=new StringBuilder("");
//			propiedades.append("idDomicilio,");
//			columnas.append("ID_DOMICILIO,");
//			
//			propiedades.append("nombreCalle,");
//			columnas.append("CALLE_NUMERO,");
//			
//			propiedades.append("nombreColonia,");
//			columnas.append("COLONIA,");
//			
//			propiedades.append("codigoPostal,");
//			columnas.append("CODIGO_POSTAL,");
//			
//			propiedades.append("idPais,");
//			columnas.append("CVE_PAIS,");
//			
//			propiedades.append("idEstado,");
//			columnas.append("CVE_ESTADO,");
//			
//			propiedades.append("idCiudad,");
//			columnas.append("CVE_CIUDAD,");
//			
//			propiedades.append("idPersona");
//			columnas.append("ID_PERSONA,");
//			storedHelper.estableceMapeoResultados(Domicilio.class.getCanonicalName(),propiedades.toString(),columnas.toString());
//			storedHelper.estableceParametro("pid_domicilio", val(domicilio.getIdDomicilio()));
//			storedHelper.estableceParametro("pid_persona", val(idPersona));
//			domicilio=(Domicilio)storedHelper.obtieneResultadoSencillo();
//			LogDeMidasInterfaz.log("Saliendo de DomicilioFacade.findById..." + this, Level.INFO, null);
//		} catch (SQLException e) {				
//			Integer codErr = null;
//			String descErr = null;
//			if (storedHelper != null) {
//				codErr = storedHelper.getCodigoRespuesta();
//				descErr = storedHelper.getDescripcionRespuesta();
//			}
//			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, domicilio.getClass(), codErr, descErr);
//			LogDeMidasInterfaz.log("Excepcion en BD de DomicilioFacade.findById..." + this, Level.WARNING, e);
//			throw e;
//		} catch (Exception e) {
//			LogDeMidasInterfaz.log("Excepcion general en DomicilioFacade.findById..." + this, Level.WARNING, e);
//			throw e;
//		}
		return domicilio;
	}
	
	@Override
	public DomicilioImpresion findDomicilioImpresionById(Long idDomicilio, Long idPersona) throws Exception{
		if(idDomicilio==null){
			throw new Exception("IdDomicilio is null");
		}
		DomicilioImpresion filtro=new DomicilioImpresion();
		DomicilioPk pkDomicilio=new DomicilioPk();
		pkDomicilio.setIdPersona(idPersona);
		pkDomicilio.setIdDomicilio(idDomicilio);
		filtro.setIdDomicilio(pkDomicilio);
		DomicilioImpresion domicilio=null;
		List<DomicilioImpresion> domicilios=findDomicilioImpresionByFilters(filtro);
		if(domicilios!=null && !domicilios.isEmpty()){
			domicilio=domicilios.get(0);
		}
		return domicilio;
	}
	
	@Override
	public Domicilio findById(Long idDomicilio) throws Exception{
		return findById(idDomicilio, null);
	}
	
	@Override
	public DomicilioImpresion findDomicilioImpresionById(Long idDomicilio) throws Exception{
		return findDomicilioImpresionById(idDomicilio, null);
	}
	
	private Object val(Object str){
		return (str!=null)?str:"";
	}
	/**
	 * Metodo para guardar un domicilio para una persona
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long save(Domicilio domicilio, String nombreUsuario)throws SQLException,Exception{
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATDOMICILIO";
		Long idDomicilio=null;
		if(domicilio==null){
			throw new Exception("Domicilio is null");
		}
		try {
			LogDeMidasInterfaz.log("Entrando a DomicilioFacade.save..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			Long idPersona=(domicilio.getIdPersona()!=null)?domicilio.getIdPersona():null;
			storedHelper.estableceParametro("pid_domicilio", val(idDomicilio));
			storedHelper.estableceParametro("pcodigo_postal", val(domicilio.getCodigoPostal()));
			String idColoniaCP=domicilio.getIdColonia();
			String[] claveColonia=(idColoniaCP!=null && !idColoniaCP.isEmpty())?idColoniaCP.split("-"):null;
			String nuevaColonia=(domicilio.getNuevaColonia()!=null && !domicilio.getNuevaColonia().trim().isEmpty())?domicilio.getNuevaColonia():null;
			String nombreColonia=(domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().trim().isEmpty())?domicilio.getNombreColonia():null;
			nombreColonia=(nuevaColonia!=null)?nuevaColonia:nombreColonia;
			idColoniaCP=(claveColonia!=null && claveColonia.length>1)?claveColonia[1]:null;
			storedHelper.estableceParametro("pid_colonia", val(null));//Siempre se manda como null ya que hace una consulta por nombre de la colonia
			storedHelper.estableceParametro("pnom_colonia", val(nombreColonia));
			storedHelper.estableceParametro("pcalle_numero", val(domicilio.getCalleNumero()));
			storedHelper.estableceParametro("ptipo_domicilio", val(domicilio.getTipoDomicilio()));
			storedHelper.estableceParametro("pid_persona", val(idPersona));
			int id=storedHelper.ejecutaActualizar();
			idDomicilio=new Long(id);
			DomicilioPk pkDomicilio=new DomicilioPk();
			pkDomicilio.setIdDomicilio(new Long(id));			
			domicilio.setIdDomicilio(pkDomicilio);
			LogDeMidasInterfaz.log("Saliendo de DomicilioFacade.save..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(nombreUsuario,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, domicilio.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de DomicilioFacade.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en DomicilioFacade.save..." + this, Level.WARNING, e);
			throw e;
		}
		return idDomicilio;
	}
	/**
	 * Metodo para guardar un domicilio
	 */
	@Override
	public Long update(Domicilio domicilio, String nombreUsuario)throws SQLException,Exception{
		Long idDomicilio=null;
		if(domicilio!=null){
			idDomicilio=save(domicilio, nombreUsuario);
		}
		return idDomicilio;
	}
	/**
	 * INdica si esta o no vacio
	 * @param str
	 * @return
	 */
	private boolean isEmpty(String str){
		return (str!=null && !str.isEmpty())?false:true;
	}
	
	@Override
	public List<Domicilio> findByFilters(Domicilio domicilio) {
		List<Domicilio> domicilios=new ArrayList<Domicilio>();
		if(domicilio!=null){
			StringBuilder queryString=new StringBuilder("select distinct model from Domicilio model ");
			StringBuilder where=new StringBuilder("");
			Map<String,Object> parameters=new HashMap<String, Object>();
			if(!isEmpty(domicilio.getClavePais())){
				add(where, "model.clavePais=:clavePais");
				parameters.put("clavePais",domicilio.getClavePais());
			}
			if(!isEmpty(domicilio.getClaveEstado())){
				add(where, "model.claveEstado=:claveEstado");
				parameters.put("claveEstado",domicilio.getClaveEstado());
			}
			if(!isEmpty(domicilio.getClaveCiudad())){
				add(where, "model.claveCiudad=:claveCiudad");
				parameters.put("claveCiudad",domicilio.getClaveCiudad());
			}
			if(!isEmpty(domicilio.getCodigoPostal())){
				add(where, "model.codigoPostal LIKE :codigoPostal");
				parameters.put("codigoPostal",domicilio.getCodigoPostal()+"%");
			}
			if(!isEmpty(domicilio.getCalleNumero())){
				add(where, "model.calleNumero like :calleNumero");
				parameters.put("calleNumero",domicilio.getCalleNumero()+"%");
			}
			if(!isEmpty(domicilio.getNombreColonia())){
				add(where, "model.nombreColonia like :nombreColonia");
				parameters.put("nombreColonia",domicilio.getNombreColonia()+"%");
			}
			if(domicilio.getIdPersona()!=null){
				add(where, "model.idPersona=:idPersona");
				parameters.put("idPersona",domicilio.getIdPersona());
			}
			if(domicilio.getIdDomicilio()!=null && domicilio.getIdDomicilio().getIdDomicilio() != null){
				add(where, "model.idDomicilio.idDomicilio=:idDomicilio");
				parameters.put("idDomicilio",domicilio.getIdDomicilio().getIdDomicilio());
			}
			if(domicilio.getIdDomicilio()!=null && domicilio.getIdDomicilio().getIdPersona() != null){
				add(where, "model.idDomicilio.idPersona=:idP");
				parameters.put("idP",domicilio.getIdDomicilio().getIdPersona());
			}
			String statement="";
			if(!where.toString().isEmpty()){
				statement=" where "+where.toString();
			}
			queryString.append(statement);
			queryString.append(" order by model.codigoPostal ");
			Query query = entityManager.createQuery(queryString.toString());
			setQueryParameters(query, parameters);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		}
		return domicilios;
	}
	
	@Override
	public List<DomicilioImpresion> findDomicilioImpresionByFilters(DomicilioImpresion domicilio) {
		List<DomicilioImpresion> domicilios=new ArrayList<DomicilioImpresion>();
		if(domicilio!=null){
			StringBuilder queryString=new StringBuilder("select distinct model from DomicilioImpresion model ");
			StringBuilder where=new StringBuilder("");
			Map<String,Object> parameters=new HashMap<String, Object>();
			if(!isEmpty(domicilio.getClavePais())){
				add(where, "model.clavePais=:clavePais");
				parameters.put("clavePais",domicilio.getClavePais());
			}
			if(!isEmpty(domicilio.getClaveEstado())){
				add(where, "model.claveEstado=:claveEstado");
				parameters.put("claveEstado",domicilio.getClaveEstado());
			}
			if(!isEmpty(domicilio.getClaveCiudad())){
				add(where, "model.claveCiudad=:claveCiudad");
				parameters.put("claveCiudad",domicilio.getClaveCiudad());
			}
			if(!isEmpty(domicilio.getCodigoPostal())){
				add(where, "model.codigoPostal LIKE :codigoPostal");
				parameters.put("codigoPostal",domicilio.getCodigoPostal()+"%");
			}
			if(!isEmpty(domicilio.getCalleNumero())){
				add(where, "model.calleNumero like :calleNumero");
				parameters.put("calleNumero",domicilio.getCalleNumero()+"%");
			}
			if(!isEmpty(domicilio.getNombreColonia())){
				add(where, "model.nombreColonia like :nombreColonia");
				parameters.put("nombreColonia",domicilio.getNombreColonia()+"%");
			}
			if(domicilio.getIdPersona()!=null){
				add(where, "model.idPersona=:idPersona");
				parameters.put("idPersona",domicilio.getIdPersona());
			}
			if(domicilio.getIdDomicilio()!=null && domicilio.getIdDomicilio().getIdDomicilio() != null){
				add(where, "model.idDomicilio.idDomicilio=:idDomicilio");
				parameters.put("idDomicilio",domicilio.getIdDomicilio().getIdDomicilio());
			}
			if(domicilio.getIdDomicilio()!=null && domicilio.getIdDomicilio().getIdPersona() != null){
				add(where, "model.idDomicilio.idPersona=:idP");
				parameters.put("idP",domicilio.getIdDomicilio().getIdPersona());
			}
			String statement="";
			if(!where.toString().isEmpty()){
				statement=" where "+where.toString();
			}
			queryString.append(statement);
			queryString.append(" order by model.codigoPostal ");
			Query query = entityManager.createQuery(queryString.toString());
			setQueryParameters(query, parameters);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		}
		return domicilios;
	}
	
	private void add(StringBuilder str,String var){
		if(str.toString().isEmpty()){
			str.append(" "+var);
		}else{
			str.append(" and "+var);
		}
	}
	
	@Override
	public List<Domicilio> findByPerson(Long idPersona) throws Exception {
		List<Domicilio> domicilios=new ArrayList<Domicilio>();
		if(idPersona!=null){
			Domicilio domicilio=new Domicilio();
			domicilio.setIdPersona(idPersona);
			domicilios=findByFilters(domicilio);
		}
		return domicilios;
	}
	
	private void setQueryParameters(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			entityQuery.setParameter(pairs.getKey(), pairs.getValue());
		}		
	}

	@Override
	public void remove(Long idDomicilio) throws Exception {
		if(idDomicilio==null){
			throw new Exception("IdDomicilio is null");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_delDomicilio";
		try {
			LogDeMidasInterfaz.log("Entrando a DomicilioFacade.remove..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pid_domicilio", val(idDomicilio));
			int id=storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz.log("Saliendo de DomicilioFacade.remove...removed id="+id + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, Domicilio.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de DomicilioFacade.remove..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en DomicilioFacade.remove..." + this, Level.WARNING, e);
			throw e;
		}
	}

	@Override
	public TipoDomicilio getTipoDomicilioPorClave(String clave) {
		TipoDomicilio tipoDomicilio=null;
		for(TipoDomicilio tipo:TipoDomicilio.values()){
			if(tipo.getValue().equals(clave)){
				tipoDomicilio=tipo;
				break;
			}
		}
		return tipoDomicilio;
	}

	/*
	 * Si se envia solo el idPersona y los demas nulos se obtienen todos los tipos de domicilio
	 * que esten vigentes(su ultima version)
	 * Si se envia el idPersona y el tipoDomicilio se obtendra el domicilio mas reciente del tipo especificado
	 * Si se envia la fecha se obtendran la ultima version del domicilio hasta la fecha especificada
	 */
	@Override
	public List<Domicilio> findDomiciliosActualesPorPersona(Long idPersona,String tipoDomicilio,String fechaConsulta){
		List<Domicilio> domicilios=new ArrayList<Domicilio>();
		Domicilio[] nulls = { null, null, null}; 
		CollectionUtils.addAll(domicilios, nulls); //Se rellena con nulos para prevenir indexOutOfBoundsException en logica posterior
		
		if(idPersona!=null){
			Map<Integer,Object> params=new HashMap<Integer, Object>();
			StringBuilder queryString=new StringBuilder("SELECT * from seycos.domicilio D " +
					"where D.ID_DOMICILIO IN(select max(id_domicilio) from seycos.domicilio model where ");
			int index=1;			
			addCondition(queryString, " model.ID_PERSONA=? ");
			params.put(index, idPersona);
			index++;			
			if(tipoDomicilio!=null){
				addCondition(queryString, " TRIM(model.TIPO_DOMICILIO) like TRIM(UPPER(?)) ");
				params.put(index,tipoDomicilio);
				index++;
			}
			if(fechaConsulta!=null){
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss.SSSSSS a");
                Date convertedDate = new Date();
                try {
                    convertedDate = dateFormat.parse(fechaConsulta);
                    addCondition(queryString, " model.FH_MODIFICACION<? ");
    				params.put(index, convertedDate);
    				index++;
                } catch (ParseException e) {
                    e.printStackTrace();
                }				
			}	
			String finalString=new String();
			if(queryString.toString().endsWith(" and ")){
				finalString=queryString.toString().substring(0,(queryString.toString().length())-(" and ").length());
			}
			if(queryString.toString().endsWith(" or ")){
				finalString=queryString.toString().substring(0,(queryString.toString().length())-(" or ").length());
			}
			finalString+=" group by model.TIPO_DOMICILIO) order by decode (TRIM(D.TIPO_DOMICILIO),'PERS',1,'FISC',2,'OFIC',3 )  ASC";	
			Query query = entityManager.createNativeQuery(finalString,DomicilioSeycos.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			List<DomicilioSeycos>domiciliosSeycos = query.getResultList();
			for(DomicilioSeycos domSeycos:domiciliosSeycos){
				Domicilio domicilioNuevo= new Domicilio();
				BeanUtils.copyProperties(domSeycos, domicilioNuevo);
				
				if(domicilioNuevo.getTipoDomicilio() == null)
				{
					if(domicilios.get(0) == null)
					{
						domicilios.add(0,domicilioNuevo);
					}					
					
				}else if(domicilioNuevo.getTipoDomicilio().equalsIgnoreCase("PERS"))
				{
					domicilios.add(0,domicilioNuevo);	
					
				}else if(domicilioNuevo.getTipoDomicilio().equalsIgnoreCase("FISC"))
				{
					domicilios.add(1,domicilioNuevo);
					
				}else if(domicilioNuevo.getTipoDomicilio().equalsIgnoreCase("OFIC"))
				{
					domicilios.add(2,domicilioNuevo);					
				}
			}
		}
		return domicilios;
	}
	
	public void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
}
