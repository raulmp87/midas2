package mx.com.afirme.midas2.dao.impl.fortimax;
import static mx.com.afirme.midas2.utils.CommonUtils.addCondition;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;
import static mx.com.afirme.midas2.utils.CommonUtils.setQueryParametersByProperties;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import mx.com.afirme.midas2.dao.fortimax.CarpetaAplicacionFortimaxDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CarpetaAplicacionFortimax;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.MidasException;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class CarpetaAplicacionFortimaxDaoImpl extends EntidadDaoImpl implements CarpetaAplicacionFortimaxDao{
	private EntidadService entidadService;

	@Override
	public void delete(Long id) throws MidasException {
		if(isNull(id)){
			onError("Favor de proporcionar la clave de la carpeta a eliminar");
		}
		CarpetaAplicacionFortimax jpaObject=entidadService.findById(CarpetaAplicacionFortimax.class,id);
		entidadService.remove(jpaObject);
	}

	@Override
	public Long delete(CarpetaAplicacionFortimax carpeta) throws MidasException {
		Long id=(isNotNull(carpeta) && isNotNull(carpeta.getId()))?carpeta.getId():null;
		delete(id);
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public CarpetaAplicacionFortimax obtenerCarpetaEspecificaPorAplicacion(String nombreAplicacion,String nombreCarpeta) throws MidasException{
		CarpetaAplicacionFortimax carpeta=null;
		List<CarpetaAplicacionFortimax> list=new ArrayList<CarpetaAplicacionFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la carpeta");
		}
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from CarpetaAplicacionFortimax model ");
		addCondition(queryString, " UPPER(model.aplicacion.nombreAplicacion) like UPPER(:nombreAplicacion)");
		params.put("nombreAplicacion",nombreAplicacion);
		addCondition(queryString, " UPPER(model.nombreCarpeta) like UPPER(:nombreCarpeta)");
		params.put("nombreCarpeta",nombreCarpeta);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		if(!isEmptyList(list)){
			carpeta=list.get(0);
		}
		return carpeta;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CarpetaAplicacionFortimax> obtenerCarpetasPorAplicacion(Long idAplicacion) throws MidasException {
		List<CarpetaAplicacionFortimax> list=new ArrayList<CarpetaAplicacionFortimax>();
		if(isNull(idAplicacion)){
			onError("Favor de proporcionar la clave de la aplicacion");
		}
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from CarpetaAplicacionFortimax model ");
		addCondition(queryString, "model.aplicacion.id = :idAplicacion");
		params.put("idAplicacion",idAplicacion);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CarpetaAplicacionFortimax> obtenerCarpetasPorAplicacion(String nombreAplicacion) throws MidasException {
		List<CarpetaAplicacionFortimax> list=new ArrayList<CarpetaAplicacionFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from CarpetaAplicacionFortimax model ");
		addCondition(queryString, " UPPER(model.aplicacion.nombreAplicacion) like UPPER(:nombreAplicacion)");
		params.put("nombreAplicacion",nombreAplicacion);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}

	@Override
	public Long save(CarpetaAplicacionFortimax carpeta) throws MidasException {
		if(isNull(carpeta)){
			onError("Favor de proporcionar la carpeta");
		}
		CarpetaAplicacionFortimax jpaObject= entidadService.save(carpeta);
		return (isNotNull(jpaObject))?jpaObject.getId():null;
	}
	/**
	 * =====================================================================
	 * Setters and getters
	 * =====================================================================
	 */
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
