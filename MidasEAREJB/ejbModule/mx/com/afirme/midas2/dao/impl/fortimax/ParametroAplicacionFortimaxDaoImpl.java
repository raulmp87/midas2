package mx.com.afirme.midas2.dao.impl.fortimax;
import static mx.com.afirme.midas2.utils.CommonUtils.addCondition;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;
import static mx.com.afirme.midas2.utils.CommonUtils.setQueryParametersByProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.fortimax.ParametroAplicacionFortimaxDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ParametroAplicacionFortimax;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.MidasException;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ParametroAplicacionFortimaxDaoImpl extends EntidadDaoImpl implements ParametroAplicacionFortimaxDao{
	private EntidadService entidadService;
	@Override
	public void delete(Long id) throws MidasException {
		if(isNull(id)){
			onError("Favor de proporcionar el parametro");
		}
		ParametroAplicacionFortimax jpaObject=entidadService.findById(ParametroAplicacionFortimax.class,id);
		entidadService.remove(jpaObject);
	}

	@Override
	public Long delete(ParametroAplicacionFortimax parametro) throws MidasException {
		Long id=(isNotNull(parametro) && isNotNull(parametro.getId()))?parametro.getId():null;
		if(isNull(id)){
			onError("Favor de proporcionar el parametro");
		}
		delete(id);
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(String nombreAplicacion)throws MidasException {
		List<ParametroAplicacionFortimax> list=new ArrayList<ParametroAplicacionFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
				
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ParametroAplicacionFortimax model ");
		addCondition(queryString, "model.aplicacion.nombreAplicacion = :nombreAplicacion");
		params.put("nombreAplicacion",nombreAplicacion);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}

	@Override
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(CatalogoAplicacionFortimax aplicacion)throws MidasException {
		if(isNull(aplicacion) || !isValid(aplicacion.getNombreAplicacion())){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		String nombreAplicacion=aplicacion.getNombreAplicacion();
		return obtenerParametrosPorAplicacion(nombreAplicacion);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(Long idAplicacion)throws MidasException {
		List<ParametroAplicacionFortimax> list=new ArrayList<ParametroAplicacionFortimax>();
		if(isNull(idAplicacion)){
			onError("Favor de proporcionar la clave de la aplicacion");
		}
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ParametroAplicacionFortimax model ");
		addCondition(queryString, "model.aplicacion.id = :idAplicacion");
		params.put("idAplicacion",idAplicacion);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}
	@Override
	@SuppressWarnings("unchecked")
	public ParametroAplicacionFortimax obtenerParametroLlavePorAplicacion(String nombreAplicacion) throws MidasException{
		ParametroAplicacionFortimax parametro=null;
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		List<ParametroAplicacionFortimax> list=new ArrayList<ParametroAplicacionFortimax>();
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ParametroAplicacionFortimax model ");
		addCondition(queryString, " UPPER(model.aplicacion.nombreAplicacion) like UPPER(:nombreAplicacion) ");
		params.put("nombreAplicacion",nombreAplicacion);
		addCondition(queryString, "model.esLlave = :esLlave");
		params.put("esLlave",1);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		if(!isEmptyList(list)){
			parametro=list.get(0);
		}
		return parametro;
	}

	@Override
	public Long save(ParametroAplicacionFortimax parametro) throws MidasException {
		if(isNull(parametro)){
			onError("Favor de proporcionar el parametro a guardar");
		}
		return (Long)entidadService.saveAndGetId(parametro);
	}
	/**
	 * Setters and getters
	 */
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
