package mx.com.afirme.midas2.dao.impl.vida;

import java.sql.Date;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.danios.reportes.reportercs.caducidad.CaducidadesDTO;
import mx.com.afirme.midas.danios.reportes.reportercs.calificaciones.CalificacionesREASDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.vida.ArchivosCompDao;
import mx.com.afirme.midas2.domain.vida.ValoresRescate;

@Stateless
public class ArchivosCompDaoImpl implements ArchivosCompDao { 
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaCaducidad(CaducidadesDTO caducidadesDTO) {
		
		LogDeMidasEJB3.log("saving CaducidadesDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(caducidadesDTO);
			
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaValoresRescate(ValoresRescate valorRescate) {
				
		LogDeMidasEJB3.log("saving DocumentoDigitalEsquemasDTO instance",
				Level.INFO, null);
		try {
			entityManager.persist(valorRescate);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public int actualizaCalReas(CalificacionesREASDTO calificacionesREASDTO) {
		
				
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF.get_ActualizaCalREAS", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pRgre", calificacionesREASDTO.getRgre());
			storedHelper.estableceParametro("pAmbest", calificacionesREASDTO.getAmbest());
			storedHelper.estableceParametro("pSnp", calificacionesREASDTO.getSnp());
			storedHelper.estableceParametro("pOtras", calificacionesREASDTO.getOtras());
			
			respuesta = storedHelper.ejecutaActualizar();
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el PKG_SOLVENCIA_CNSF.get_ActualizaCalREAS", Level.SEVERE, e);
			respuesta = 0;
		}
		return respuesta;
	}
	
	public void borraTablasVida(int fcorte, int nArchivo) {
			
		LogDeMidasEJB3.log(
				"deleting CaducidadesDTO instance with property: anio "
						 + ", value: " + fcorte, Level.INFO, null);
		try {
			final String queryString = "delete from CaducidadesDTO model where model.anio "
					+  "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			
			query.setParameter("propertyValue", fcorte);
			query.executeUpdate();
			entityManager.flush();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

}

