package mx.com.afirme.midas2.dao.impl.siniestros.catalogo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.catalogo.PrestadorServicioDao;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste.PrestadorGastoAjusteDTO;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.util.JpaUtil;
import mx.com.afirme.midas2.util.StringUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class PrestadorServicioDaoImpl extends JpaDao<Long, CondicionEspecial> implements PrestadorServicioDao{
	@EJB
	EntidadDao entidadDao;
	
	@Override
	@SuppressWarnings("unchecked")
	public List<PrestadorGastoAjusteDTO> buscarPrestadorPorNombre(String nombre,
			String tipoPrestador) {
		// TODO Auto-generated method stub
		Map<String, Object> parameters = new HashMap<String, Object>();	

		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + PrestadorGastoAjusteDTO.class.getSimpleName() + " model ");	
		queryString.append(" WHERE UPPER(model.nombre) LIKE :nombre");
		queryString.append(" AND model.tipo = :tipoPrestador");
		
		parameters.put("nombre", "%" + nombre.toUpperCase() + "%");
		parameters.put("tipoPrestador", tipoPrestador);
		List<PrestadorGastoAjusteDTO>  lst =	(List<PrestadorGastoAjusteDTO>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);

		return lst;
	}

	@SuppressWarnings("unchecked")
	@Override
	public TipoPrestadorServicio buscarTipoPrestadorbyNombre(String nombre) {

		Map<String, Object> parameters = new HashMap<String, Object>();	
		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + TipoPrestadorServicio.class.getSimpleName() + " model ");
		queryString.append(" WHERE UPPER(model.nombre) LIKE :nombre");
		
		parameters.put("nombre", "%" + nombre.toUpperCase() + "%");
		List<TipoPrestadorServicio>  lst =	(List<TipoPrestadorServicio>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		TipoPrestadorServicio tp = null;
		if(lst != null && lst.size() > 0){
			tp = lst.get(0);
		}
		return tp;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> consultarPrestadoresGastoAjuste(){
		Map<String, Object> parameters = new HashMap<String, Object>();
		Map<String, String> map = new HashMap<String, String>();
		List<String> nombres = new ArrayList<String>();
		nombres.add("PERIT");
		nombres.add("GRUA");
		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + TipoPrestadorServicio.class.getSimpleName() + " model ");
		queryString.append(" WHERE UPPER(model.nombre) IN :nombres");
		
		parameters.put("nombres", nombres);
		List<TipoPrestadorServicio>  lst =	(List<TipoPrestadorServicio>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		for(TipoPrestadorServicio valor : lst){
			map.put(valor.getNombre(), valor.getDescripcion());
		}
		return map;
	}

	@Override
	public List<PrestadorServicioRegistro> buscar(PrestadorServicioFiltro filtro) {
		Map<String, Object> parametros = new HashMap<String, Object>();		
//		StringBuilder queryString = new StringBuilder("SELECT model from PrestadorServicio model ");

		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro( ");
		queryString.append("model.id, model.oficina, model.personaMidas, FUNC('MIDAS.FN_OBTENTIPOPRESTADOR', model.id), model.estatus ");
		queryString.append(") FROM PrestadorServicio model ");
		if(filtro != null){			
			
			if(!StringUtil.isEmpty(filtro.getTipoPrestadorStr())){
				queryString.append("JOIN model.tiposPrestador tipo ");				
			}
			if(!StringUtil.isEmpty(filtro.getEstado())){
				queryString.append("JOIN model.personaMidas.personaDireccion persDir ");									
			}
			if(!StringUtil.isEmpty(filtro.getTipoPrestadorStr())){
				//JpaUtil.addParameter(queryString, "tipo.nombre", filtro.getTipoPrestadorStr(), "tipoPrestadorNombre", parametros);
				JpaUtil.addParameterIn(queryString, "tipo.nombre", filtro.getTipoPrestadorStr());
			}
			if(!StringUtil.isEmpty(filtro.getEstado())){
				JpaUtil.addParameter(queryString, "persDir.direccion.colonia.ciudad.estado.id", filtro.getEstado(), "estado", parametros);
			}
			JpaUtil.addFilterParameter("model", queryString, filtro, parametros);
		}
		queryString.append(" ORDER BY model.personaMidas.nombre");
		TypedQuery<PrestadorServicioRegistro> query = entityManager.createQuery(queryString.toString(), PrestadorServicioRegistro.class);
		setQueryParameters(query, parametros);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);		
		return query.getResultList();
	}
	

}
