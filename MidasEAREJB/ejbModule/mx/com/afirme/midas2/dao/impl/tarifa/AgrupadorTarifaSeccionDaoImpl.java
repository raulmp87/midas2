package mx.com.afirme.midas2.dao.impl.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.catalogos.AgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccionId_;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion_;

@Stateless
public class AgrupadorTarifaSeccionDaoImpl extends EntidadDaoImpl implements
		AgrupadorTarifaSeccionDao {

	@Override
	public List<AgrupadorTarifaSeccion> findByFilters(
			AgrupadorTarifaSeccion filter) {
		
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT relacion FROM AgrupadorTarifaSeccion relacion ")
		   .append("WHERE relacion.id.idToAgrupadorTarifa = :idToSeccion ")
		   .append("AND relacion.id.idMoneda = :idMoneda ")
		   .append("AND relacion.claveDefault = :claveDefault ");
		
		TypedQuery<AgrupadorTarifaSeccion> query = entityManager.createQuery(sql.toString(), AgrupadorTarifaSeccion.class);
		query.setParameter("idToSeccion", filter.getId().getIdToSeccion());
		query.setParameter("idMoneda", filter.getId().getIdMoneda());
		query.setParameter("claveDefault", filter.getClaveDefault());
		
		return query.getResultList();
	}
	
	public List<AgrupadorTarifaSeccion> findByBusiness(String claveNegocio, AgrupadorTarifaSeccion filter){
		
		Long idToAgrupadorTarifa = (long) 0 ;
		if (filter.getId().getIdToAgrupadorTarifa() != null){
			idToAgrupadorTarifa = filter.getId().getIdToAgrupadorTarifa();
		}

		StringBuilder sql = new StringBuilder();
		if (filter.getId().getIdToAgrupadorTarifa() == null){
		sql.append("SELECT DISTINCT relacion FROM AgrupadorTarifaSeccion relacion, AgrupadorTarifa agrupador ")
		   .append("WHERE relacion.id.idToAgrupadorTarifa = agrupador.id.idToAgrupadorTarifa ")
		   .append("AND agrupador.claveNegocio = :claveNegocio ")
		   .append("AND relacion.id.idToSeccion = :idToSeccion ")
		   .append("AND relacion.id.idMoneda = :idMoneda  ");
		}
		if (filter.getId().getIdToAgrupadorTarifa() != null){
			sql.append("SELECT DISTINCT relacion FROM AgrupadorTarifaSeccion relacion, AgrupadorTarifa agrupador ")
			   .append("WHERE relacion.id.idToAgrupadorTarifa = agrupador.id.idToAgrupadorTarifa ")
			   .append("AND agrupador.claveNegocio = :claveNegocio ")
			   .append("AND relacion.id.idToSeccion = :idToSeccion ")
			   .append("AND relacion.id.idMoneda = :idMoneda  ")
			   .append("AND relacion.id.idToAgrupadorTarifa = :idToAgrupadorTarifa ");
		}
		
		TypedQuery<AgrupadorTarifaSeccion> query = entityManager.createQuery(sql.toString(), AgrupadorTarifaSeccion.class);
		query.setParameter("claveNegocio", claveNegocio);
		query.setParameter("idToSeccion", filter.getId().getIdToSeccion());
		query.setParameter("idMoneda", filter.getId().getIdMoneda());
		if (filter.getId().getIdToAgrupadorTarifa() != null){
			query.setParameter("idToAgrupadorTarifa", idToAgrupadorTarifa);
		}
		
		return query.getResultList();
		
	}

	@Override
	public List<AgrupadorTarifaSeccion> findBySeccionMoneda(
			AgrupadorTarifaSeccion filter) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<AgrupadorTarifaSeccion> criteriaQuery = cb.createQuery(AgrupadorTarifaSeccion.class);
		Root<AgrupadorTarifaSeccion> root = criteriaQuery.from(AgrupadorTarifaSeccion.class);
		criteriaQuery.distinct(true);
		Predicate predicado = cb.and(cb.equal(root.get(AgrupadorTarifaSeccion_.id).get(AgrupadorTarifaSeccionId_.idMoneda),filter.getId().getIdMoneda()),
				cb.equal(root.get(AgrupadorTarifaSeccion_.id).get(AgrupadorTarifaSeccionId_.idToSeccion),filter.getId().getIdToSeccion()));
		
		
		criteriaQuery.where(predicado);
		
		
		TypedQuery<AgrupadorTarifaSeccion> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}

}
