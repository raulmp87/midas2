package mx.com.afirme.midas2.dao.impl.componente.scrollable;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Parameter;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.base.Scrollable;
import mx.com.afirme.midas2.dao.componente.scrollable.ScrollableNavigationDao;

import org.eclipse.persistence.internal.jpa.QueryImpl;
import org.eclipse.persistence.jpa.JpaHelper;
import org.eclipse.persistence.queries.DataReadQuery;
import org.eclipse.persistence.queries.DatabaseQuery;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.queries.ReportQuery;

@Stateless
public class ScrollableNavigationDaoImpl implements ScrollableNavigationDao {

	@SuppressWarnings("unchecked")
	@Override
	public <T extends Scrollable> List<T> getResultList(Query query, T scrollableFilter) {
		
		Scrollable scrollable = null;
		
		if (scrollableFilter != null) {
		
			scrollable = (Scrollable) scrollableFilter;
			
			//Si es la primera vez que se consulta se obtiene el total de registros
			if (scrollable.getPosStart() == null || scrollable.getCount() == null || scrollable.getPosStart().intValue() == 0) {
				
				//Sale de este metodo por referencia en scrollableFilter, por manejar en MIDAS solamente interfaces locales para los Beans, 
				//se puede regresar este objeto actualizado hasta el Action
				scrollable.setTotal(calculateSize(query));
				
				if (scrollable.getCount() == null) {
					scrollable.setCount(NUMERO_REGISTROS_DEFAULT);
				}
				
				if (scrollable.getPosStart() == null) {
					scrollable.setPosStart(0);
				}
			
			}
			
			//Se agrega el ordenamiento customizado
			query = orderResult(scrollable, query);
			
			if(scrollable.getPosStart() != null && scrollable.getCount() != null) {
				query.setFirstResult(scrollable.getPosStart().intValue());
				query.setMaxResults(scrollable.getCount().intValue());
			}
		
		}
		
		return query.getResultList();

	}
	
	
	private boolean isNativeQuery (Query query) {
		
		return query instanceof DataReadQuery;
		
	}
	
	
	/**
     * Using the provided query to calculate the size. The query is
     * copied to create a new query which just retrieves the count.
     */
    @SuppressWarnings({ "unchecked", "rawtypes" })
	private int calculateSize(Query query) {
    	
    	if (isNativeQuery(query)) {
    		
    		String queryString = ((DataReadQuery) query).getSQLString();
    		
    		queryString = capitalizeBasicReservedWords(queryString);
    		
    		queryString = "SELECT COUNT(1) " + queryString.substring(queryString.indexOf("FROM"));
    		
    		queryString = queryString.substring(0, (queryString.lastIndexOf("ORDER BY") > -1 ? queryString.lastIndexOf("ORDER BY") : queryString.length()));
    		
    		Query queryCount = entityManager.createNativeQuery(queryString);
    		
    		return ((Integer)queryCount.getSingleResult()).intValue();
    		
    		
    	} else {
    	
	    	ReadAllQuery raq = JpaHelper.getReadAllQuery(query);
	    	
	        ReportQuery rq = null;
	        
	        if (raq.isReportQuery()) {
	            rq = (ReportQuery) raq.clone();
	            rq.getItems().clear();
	            rq.addCount();
	            rq.getGroupByExpressions().clear();
	            rq.getOrderByExpressions().clear();
	        } else {
	            rq = new ReportQuery();
	            rq.setReferenceClass(raq.getReferenceClass());
	            rq.addCount();
	            rq.setShouldReturnSingleValue(true);
	            rq.setSelectionCriteria(raq.getSelectionCriteria());
	        }
	        // Wrap new report query as JPA query for execution with parameters
	        TypedQuery<Number> countQuery = (TypedQuery<Number>) JpaHelper.createQuery(rq, entityManager);
	
	        // Copy parameters
	        for (Parameter p : query.getParameters()) {
	            countQuery.setParameter(p, query.getParameterValue(p));
	        }
	
	        return countQuery.getSingleResult().intValue();
	        
    	}
    	
    	
    }
        
    @SuppressWarnings("rawtypes")
	private Query orderResult (Scrollable scrollable, Query query) {
		
    	DataReadQuery dataReadQuery = null;
    	DatabaseQuery dbQuery = null;
    	Query queryOrdered = null;
    	String sortedProperty = null;
		String direct = null;
		String queryString = null;
		    	
    	if (scrollable != null && scrollable.getOrderBy() != null && !scrollable.getOrderBy().isEmpty()) {
			
			sortedProperty = scrollable.getOrderBy().trim();

			if (!isNativeQuery(query)) {
				
				dbQuery = ((QueryImpl) query).getDatabaseQuery();
				
				queryString = dbQuery.getJPQLString();
				
			} else {
				
				dataReadQuery = ((DataReadQuery) query);
				
				queryString = dataReadQuery.getSQLString();
				
			}
			
			queryString = capitalizeBasicReservedWords(queryString);
			
			if (scrollable.getDirect() != null && scrollable.getDirect().toUpperCase().trim().equals("DES")) {
				direct = "DESC";
			} else {
				direct = "ASC";
			}
			
			if (!isNativeQuery(query)) {
				
				sortedProperty = queryString.substring(queryString.indexOf("SELECT") + 6, queryString.indexOf("FROM")).trim() + "." + sortedProperty;
				
			}
			
			if (queryString.indexOf("GROUP") > -1 && queryString.substring(queryString.indexOf("GROUP")).indexOf(sortedProperty) == -1) {
				queryString += "," + sortedProperty;
			}
			
			queryString += " ORDER BY " + sortedProperty  + " " + direct;
			
			if (!isNativeQuery(query)) {
				
				queryOrdered = entityManager.createQuery(queryString);
				
			} else {
				
				queryOrdered = entityManager.createNativeQuery(queryString);
				
			}
			
			for (Parameter parameter: query.getParameters()) {
				
				queryOrdered.setParameter(parameter.getName(), query.getParameterValue(parameter.getName()));
				
			}
			
			return queryOrdered;
			
		}
    	
    	return query;
     
	}
    
    private String capitalizeBasicReservedWords(String queryString) {
    	
    	queryString = capitalizeReservedWord(queryString, "SELECT");
    	queryString = capitalizeReservedWord(queryString, "FROM");
    	queryString = capitalizeReservedWord(queryString, "WHERE");
    	queryString = capitalizeReservedWord(queryString, "INNER");
    	queryString = capitalizeReservedWord(queryString, "LEFT");
    	queryString = capitalizeReservedWord(queryString, "JOIN");
    	queryString = capitalizeReservedWord(queryString, "FETCH");
    	queryString = capitalizeReservedWord(queryString, "GROUP");
    	queryString = capitalizeReservedWord(queryString, "ORDER");
    	queryString = capitalizeReservedWord(queryString, "BY");
    	
    	return queryString;
    	
    }
    
    private String capitalizeReservedWord(String original, String reservedWord) {
    	
    	String originalReservedWord = null;
    	
    	String lowercase = original.toLowerCase();
    	
    	reservedWord = reservedWord.toLowerCase().trim();
    	
    	while (lowercase.indexOf(reservedWord) > -1) {
    		
    		originalReservedWord = original.substring(lowercase.indexOf(reservedWord), lowercase.indexOf(reservedWord) + reservedWord.length());
    		
    		original = original.replaceFirst(originalReservedWord, reservedWord.toUpperCase());
    		
    		lowercase = lowercase.replaceFirst(reservedWord, reservedWord.toUpperCase());
    		
    	}
    	
    	return original;
        	
    }
    
    private final Integer NUMERO_REGISTROS_DEFAULT = 20;
	
	
	@PersistenceContext
	private EntityManager entityManager;


}
