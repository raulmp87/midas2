package mx.com.afirme.midas2.dao.impl.condicionesGenerales;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.condicionesGenerales.CgOrdenDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgOrden;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class CgOrdenDaoImpl  extends JpaDao<Long, CgOrden> implements CgOrdenDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<CgOrden> findByFilter(CgOrden cgOrden) throws SQLException,
			Exception {		
		List<CgOrden> list = new ArrayList<CgOrden>();
		String whereStr = "";
		if ( cgOrden.getFechaInicio() != null && !cgOrden.getFechaInicio().equals("") 
				&&  cgOrden.getFechaFin() != null && !cgOrden.getFechaFin().equals("") ){
			whereStr = "where cgo.fecha between :startDate and :endDate ";
		}
		
		String queryString = "" +		
		"select cgo " +
		"from CgOrden cgo " +
		whereStr +
		"order by cgo.fecha desc ";
		
		Query query = entityManager.createQuery(queryString);
		if ( cgOrden.getFechaInicio() != null && !cgOrden.getFechaInicio().equals("") ){
			query.setParameter("startDate", cgOrden.getFechaInicio());
			query.setParameter("endDate", cgOrden.getFechaFin());
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list = (List<CgOrden>)query.getResultList();
		
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CgOrden> findByFilterQuery( String usuario ) throws SQLException,
		Exception {
	
		String where="";
		List<CgOrden> list = new ArrayList<CgOrden>();
			
		where = "where cga.agente.codigoUsuario = " + usuario
		+ " and cgo.tocgagente_id = cga.id ";
		
		String queryString = "" +
		"" +
		"select cgo " +
		"from CgOrden cgo, cgAgente cga " +
		where +
		"order by cgo.id asc ";
		
		Query query = entityManager.createQuery(queryString);		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list = (List<CgOrden>)query.getResultList();
	
	return list;
	}

}
