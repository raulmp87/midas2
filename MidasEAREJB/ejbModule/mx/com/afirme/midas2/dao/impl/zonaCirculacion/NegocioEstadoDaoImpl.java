package mx.com.afirme.midas2.dao.impl.zonaCirculacion;

import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.zonaCirculacion.NegocioEstadoDao;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;

@Stateless
public class NegocioEstadoDaoImpl extends EntidadDaoImpl implements
		NegocioEstadoDao {
	/**
	 * Obtiene un listado de los estados relacionados con un negocio
	 * 
	 * @param idToNegocio
	 * @return List<NegocioEstado>
	 * @author martin
	 */
	@Override
	public List<NegocioEstado> obtenerEstadosPorNegocioId(Long idToNegocio) {
		return findByProperty(NegocioEstado.class, "negocio", idToNegocio);
	}

}
