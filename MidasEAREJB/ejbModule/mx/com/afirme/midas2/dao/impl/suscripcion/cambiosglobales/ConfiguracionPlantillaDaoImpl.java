package mx.com.afirme.midas2.dao.impl.suscripcion.cambiosglobales;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.cambiosglobales.ConfiguracionPlantillaDao;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNeg;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNegCobertura;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ConfiguracionPlantillaDaoImpl  extends JpaDao<BigDecimal, ConfiguracionPlantillaNeg>  implements ConfiguracionPlantillaDao {

	@Override
	public List<ConfiguracionPlantillaNeg> findByFilters(
			ConfiguracionPlantillaNeg filtro) {	
		String queryString = "select model from ConfiguracionPlantillaNeg model where " + 
		" model.cotizacionId = :cotizacionId " + 
		" and model.lineaId = :lineaId " + 
		" and model.paqueteId = :paqueteId ";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("cotizacionId", filtro.getCotizacionId());
		query.setParameter("lineaId", filtro.getLineaId());
		query.setParameter("paqueteId", filtro.getPaqueteId());
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@Override
	public List<ConfiguracionPlantillaNegCobertura> listarCoberturas(Long idPlantilla) {
		String queryString = "select model from ConfiguracionPlantillaNegCobertura model where " + 
		" model.id.plantillaId = :idPlantilla ";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idPlantilla", idPlantilla);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@Override
	public ConfiguracionPlantillaNeg findByCotizacionLineaPaquete(
			BigDecimal cotizacionId, BigDecimal lineaId, BigDecimal paqueteId) {
		final String jpql = "select model from ConfiguracionPlantillaNeg model where " +
				"model.cotizacionId = :cotizacionId and model.lineaId = :lineaId and model.paqueteId = :paqueteId";
		TypedQuery<ConfiguracionPlantillaNeg> query = entityManager.createQuery(jpql, ConfiguracionPlantillaNeg.class);
		query.setParameter("cotizacionId", cotizacionId);
		query.setParameter("lineaId", lineaId);
		query.setParameter("paqueteId", paqueteId);
		List<ConfiguracionPlantillaNeg> list = query.getResultList();
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}

}
