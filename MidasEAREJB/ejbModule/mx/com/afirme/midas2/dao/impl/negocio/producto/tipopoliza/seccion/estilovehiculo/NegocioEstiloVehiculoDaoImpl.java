package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza.seccion.estilovehiculo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculoDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioEstiloVehiculoDaoImpl extends
		JpaDao<Long, NegocioEstiloVehiculo> implements NegocioEstiloVehiculoDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<NegocioEstiloVehiculo> getEstilosVehiculo(
			BigDecimal idToNegSeccion) {
		try {
			String queryString = "select model from NegocioEstiloVehiculo model where "
					+ " model.negocioSeccion.idToNegSeccion = :idToNegSeccion ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegSeccion", idToNegSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@SuppressWarnings("unchecked")	
	@Override
	public List<NegocioEstiloVehiculo> getEstilosVehiculo(BigDecimal idToNegSeccion,
			BigDecimal idTcMarcaVehiculo, BigDecimal tipoVehiculo) {
		try {
			String queryString = "select model from NegocioEstiloVehiculo model where "
					+ " model.negocioSeccion.idToNegSeccion = :idToNegSeccion "
					+ " and model.estiloVehiculoDTO.marcaVehiculoDTO.idTcMarcaVehiculo = :idTcMarcaVehiculo "
					+ " and model.estiloVehiculoDTO.idTcTipoVehiculo = :tipoVehiculo ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegSeccion", idToNegSeccion);
			query.setParameter("idTcMarcaVehiculo", idTcMarcaVehiculo);
			query.setParameter("tipoVehiculo", tipoVehiculo);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}

}
