package mx.com.afirme.midas2.dao.impl.cobranza.prorroga;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.cobranza.prorroga.ProrrogaDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.cobranza.prorroga.ToProrroga;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ProrrogaDaoImpl extends JpaDao<Long, ToProrroga> implements ProrrogaDao{
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<ToProrroga> findByFilters(ToProrroga entity){
		String queryString = "select model from ToProrroga model ";
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		String sWhere = "";
		
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere,  "idToCotizacion", entity.getIdToCotizacion());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos, sWhere, "numeroInciso", entity.getNumeroInciso());
		
		if (Utilerias.esAtributoQueryValido(sWhere))
			queryString = queryString.concat(" where ").concat(sWhere);
		
		Query query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
	}

}
