/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaRamoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaRamo;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Stateless
public class CaBancaRamoDaoImpl implements CaBancaRamoDao {
	
	public static final String ID = "id";
	public static final String CABANCACONFIGURACION_ID = "configuracionBancaId";
	public static final String RAMOSEYCOS_ID = "ramoSeycosId";
	
	public static final String BORRADOLOGICO = "borradologico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = LoggerFactory.getLogger(CaBancaRamoDaoImpl.class);
	
	public void save(CaBancaRamo entity) {	
		try {
			LOG.info(">> save()");
			entityManager.persist(entity);
			LOG.info("<< save()");
        } catch (RuntimeException re) {     
        	LOG.error("Información del Error", re);
            throw re;
        }
	}
	
    public void delete(CaBancaRamo entity) {
    	try {
    		LOG.info(">> delete()");
    		entity = entityManager.getReference(CaBancaRamo.class, entity.getId());
    		LOG.info("<< delete()");
		    entityManager.remove(entity);
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
		    throw re;
		}
    }
    
    public CaBancaRamo update(CaBancaRamo entity) {    
    	try {
    		LOG.info(">> update()");
            CaBancaRamo result = entityManager.merge(entity);
            LOG.info("<< update()");
            return result;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        	throw re;
        }
    }
    
    public CaBancaRamo findById( Long id) {
    	CaBancaRamo instance = null;
    	try {
    		LOG.info(">> findById()");
    		instance = entityManager.find(CaBancaRamo.class, id);
    		LOG.info("<< findById()");
    	} catch (RuntimeException re) {    		
    		LOG.error("Información del Error", re);
    	}
    	return instance;
    }

    @SuppressWarnings("unchecked")
    public List<CaBancaRamo> findByProperty(String propertyName, final Object value) {
    	LOG.info(">> findByProperty()");
		try {
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaBancaRamo model ");
			queryString.append("  WHERE model.").append(propertyName).append(" = :propertyValue");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("propertyValue", value);		
			LOG.info("<< findByProperty()");
			return query.getResultList();
		} catch (RuntimeException re) {		
			LOG.error("Información del Error", re);
			return null;
		}
	}			

	@SuppressWarnings("unchecked")
	public List<CaBancaRamo> findAll() {	
		LOG.info(">> findAll()");
		List<CaBancaRamo> list = null;
		try {
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaBancaRamo model ");
			Query query = entityManager.createQuery(queryString.toString());
			list = query.getResultList();			
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			list = new ArrayList<CaBancaRamo>();
		}
		LOG.info("<< findAll()");
		return list;
	}
}