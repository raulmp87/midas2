package mx.com.afirme.midas2.dao.impl.siniestros.indemnizacion.perdidatotal;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.siniestros.indemnizacion.perdidatotal.PerdidaTotalDao;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RolesIndemnizacionAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;

import org.apache.commons.lang.StringUtils;

@Stateless
public class PerdidaTotalDaoImpl implements  PerdidaTotalDao{
	
	private static final int	CANT_ELEMENTOS_NUMEROPOLIZA	= 3;
	private static final int	POSICIONES_NUMPOLIZA	= 8;
	
	@EJB
	private PerdidaTotalService perdidaTotalService;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings({"unchecked"})
	@Override
	public List<PerdidaTotalFiltro> buscarIndemnizaciones(PerdidaTotalFiltro filtroPT){
		List<PerdidaTotalFiltro> indemnizaciones = new ArrayList<PerdidaTotalFiltro>();
		
		String spName = "MIDAS.PKGSIN_INDEMNIZACIONES.buscarIndemnizaciones"; 
		StoredProcedureHelper storedHelper  = null;
		try {
			String propiedades = "idOrdenCompra,idIndemnizacion,idSiniestro,tipoBajaPlacas,oficinaNombre,fechaSiniestro,tipoSiniestro," +
					"tipoSiniestroDesc,numeroSiniestro,numeroReporte,numeroPoliza,numeroSerie,numeroValuacion,etapa," +
					"etapaDesc,estatusIndemnizacion,estatusIndemnizacionDesc,estatusPerdidaTotal,estatusPerdidaTotalDesc," +
					"existeOrdenCompraFinal,existeRecepcionDocumentos,cobertura,numPaseAtencion";

			String columnasBaseDatos = "idOrdenCompra,idIndemnizacion,idSiniestro,tipoBajaPlacas,oficinaNombre,fechaSiniestro,tipoSiniestro," +
			"tipoSiniestroDesc,numeroSiniestro,numeroReporte,numeroPoliza,numeroSerie,numeroValuacion,etapa," +
			"etapaDesc,estatusIndemnizacion,estatusIndemnizacionDesc,estatusPerdidaTotal,estatusPerdidaTotalDesc," +
			"existeOrdenCompraFinal,existeRecepcionDocumentos,cobertura,numPaseAtencion";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(PerdidaTotalFiltro.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltro(storedHelper, filtroPT);
			indemnizaciones = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de INDEMNIZACIONES ..."+descErr
						, Level.WARNING, e);    
			}
		}
		
		return indemnizaciones;
	}
	
	private Object convertToNull(Object obj){
		if( obj instanceof String){
			obj = (obj != null && ((String)obj).equals(""))?null:obj;
		}else if ( obj instanceof Boolean){
			obj = (obj != null && ((Boolean)obj)== Boolean.FALSE)?null:obj;
		}
		return obj;
	}

	private void agregaParametrosDelFiltro(StoredProcedureHelper storedHelper, PerdidaTotalFiltro filtro){

		storedHelper.estableceParametro("p_idIndemnizacion", 				this.convertToNull( filtro.getIdIndemnizacion() ));
		storedHelper.estableceParametro("p_idOficina"     , 				this.convertToNull( filtro.getIdOficina() ));
		storedHelper.estableceParametro("p_tipoSiniestro" , 				this.convertToNull( filtro.getTipoSiniestro() ));
		storedHelper.estableceParametro("p_siniestroClaveOficina"      , 	this.convertToNull( filtro.getSiniestroClaveOficina() ));
		storedHelper.estableceParametro("p_siniestroConsecutivoReporte"   , this.convertToNull( filtro.getSiniestroConsecutivoReporte() ));
		storedHelper.estableceParametro("p_siniestroAnioReporte"  , 		this.convertToNull( filtro.getSiniestroAnioReporte() ));
		storedHelper.estableceParametro("p_reporteClaveOficina"   , 		this.convertToNull( filtro.getReporteClaveOficina() ));
		storedHelper.estableceParametro("p_reporteConsecutivoReporte"   , 	this.convertToNull( filtro.getReporteConsecutivoReporte() ));
		storedHelper.estableceParametro("p_reporteAnioReporte", 			this.convertToNull( filtro.getReporteAnioReporte() ));
		storedHelper.estableceParametro("p_cobertura", 						this.convertToNull( filtro.getCobertura() ));
		storedHelper.estableceParametro("p_numeroPoliza" , 					this.convertToNull( filtro.getNumeroPoliza() ));
		storedHelper.estableceParametro("p_numeroSerie" , 					this.convertToNull( filtro.getNumeroSerie() ));
		storedHelper.estableceParametro("p_numeroValuacion"    , 			this.convertToNull( filtro.getNumeroValuacion() ));
		storedHelper.estableceParametro("p_fechaSiniestro"      , 			this.convertToNull( filtro.getFechaSiniestro() ));
		storedHelper.estableceParametro("p_etapa", 							this.convertToNull( filtro.getEtapa() ));
		storedHelper.estableceParametro("p_estatusPerdidaTotal"        , 	this.convertToNull( filtro.getEstatusPerdidaTotal() ));
		storedHelper.estableceParametro("p_numPaseAtencion"       , 		this.convertToNull( filtro.getNumPaseAtencion() ));
		storedHelper.estableceParametro("p_estatusIndemnizacion"          , this.convertToNull( filtro.getEstatusIndemnizacion() ));
	}
	
	
	
	@SuppressWarnings({"rawtypes","unchecked"})
	@Override
	public List<OrdenCompra> buscarOrdenesCompraDePerdidaTotal(PerdidaTotalFiltro filtroPT) {
		List<OrdenCompra> ordenes 	= new ArrayList<OrdenCompra>();
		StringBuilder queryString	= new StringBuilder();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		
		queryString.append(" SELECT DISTINCT model.ordenCompra FROM DetalleOrdenCompra AS model ");
		queryString.append(" WHERE model.conceptoAjuste.tipoConcepto IN :conceptosPerdidaTotal ");
		queryString.append(" AND model.ordenCompra.estatus IN :estatusesIndemnizacion ");

		Utilerias.agregaHashLista(listaParametrosValidos, "estatusesIndemnizacion", obtenerEstatusesIndemnizacion());
		Utilerias.agregaHashLista(listaParametrosValidos, "conceptosPerdidaTotal", obtenerConceptosDePerdidaTotal());
		
		if(filtroPT.getIdOficina() != null && filtroPT.getIdOficina() != 0){
			queryString.append(" AND model.ordenCompra.reporteCabina.oficina.id = :idOficina ");
			Utilerias.agregaHashLista(listaParametrosValidos, "idOficina", filtroPT.getIdOficina());
		}
		if(filtroPT.getTipoSiniestro() != null && !filtroPT.getTipoSiniestro().isEmpty()){
			queryString.append(" AND model.conceptoAjuste.tipoConcepto = :tipoSiniestro ");
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoSiniestro", filtroPT.getTipoSiniestro());
		}
		if(filtroPT.getSiniestroClaveOficina() != null && !filtroPT.getSiniestroClaveOficina().isEmpty()){
			queryString.append(" AND model.ordenCompra.reporteCabina.siniestroCabina.claveOficina = :siniestroClaveOficina ");
			Utilerias.agregaHashLista(listaParametrosValidos, "siniestroClaveOficina", filtroPT.getSiniestroClaveOficina());
		}
		if(filtroPT.getSiniestroConsecutivoReporte() != null && !filtroPT.getSiniestroConsecutivoReporte().isEmpty()){
			queryString.append(" AND model.ordenCompra.reporteCabina.siniestroCabina.consecutivoReporte = :siniestroConsecutivoReporte ");
			Utilerias.agregaHashLista(listaParametrosValidos, "siniestroConsecutivoReporte", filtroPT.getSiniestroConsecutivoReporte());
		}
		if(filtroPT.getSiniestroAnioReporte() != null && !filtroPT.getSiniestroAnioReporte().isEmpty()){
			queryString.append(" AND model.ordenCompra.reporteCabina.siniestroCabina.anioReporte = :siniestroAnioReporte ");
			Utilerias.agregaHashLista(listaParametrosValidos, "siniestroAnioReporte", filtroPT.getSiniestroAnioReporte());
		}
		if(filtroPT.getReporteClaveOficina() != null && !filtroPT.getReporteClaveOficina().isEmpty()){
			queryString.append(" AND model.ordenCompra.reporteCabina.claveOficina= :reporteClaveOficina ");
			Utilerias.agregaHashLista(listaParametrosValidos, "reporteClaveOficina", filtroPT.getReporteClaveOficina());
		}
		
		if(filtroPT.getReporteConsecutivoReporte() != null && !filtroPT.getReporteConsecutivoReporte().isEmpty()){
			queryString.append(" AND model.ordenCompra.reporteCabina.consecutivoReporte = :reporteConsecutivoReporte ");
			Utilerias.agregaHashLista(listaParametrosValidos, "reporteConsecutivoReporte", filtroPT.getReporteConsecutivoReporte());
		}
		if(filtroPT.getReporteAnioReporte() != null && !filtroPT.getReporteAnioReporte().isEmpty()){
			queryString.append(" AND model.ordenCompra.reporteCabina.anioReporte = :reporteAnioReporte ");
			Utilerias.agregaHashLista(listaParametrosValidos, "reporteAnioReporte", filtroPT.getReporteAnioReporte());
		}
		if(filtroPT.getCobertura() != null && !filtroPT.getCobertura().isEmpty()){
			queryString.append(" AND model.ordenCompra.coberturaReporteCabina.claveTipoCalculo = :cobertura ");
			Utilerias.agregaHashLista(listaParametrosValidos, "cobertura", filtroPT.getCobertura());
		}
		if(filtroPT.getNumeroPoliza() != null && !filtroPT.getNumeroPoliza().isEmpty()){
			String[] elementosNumPoliza = filtroPT.getNumeroPoliza().split("-");
			
			if(elementosNumPoliza.length == CANT_ELEMENTOS_NUMEROPOLIZA){
				String codigoProducto 		=  StringUtils.rightPad(elementosNumPoliza[0].substring(0,2),POSICIONES_NUMPOLIZA);
				String codigoTipoPoliza 	= StringUtils.rightPad(elementosNumPoliza[0].substring(2),POSICIONES_NUMPOLIZA);
				int numeroPoliza 		= Integer.parseInt( elementosNumPoliza[1] );
				int numeroRenovacion 	= Integer.parseInt(  elementosNumPoliza[2]);
			
				queryString.append(" AND model.ordenCompra.reporteCabina.poliza.codigoProducto = :codigoProducto "); 
				queryString.append(" AND model.ordenCompra.reporteCabina.poliza.codigoTipoPoliza = :codigoTipoPoliza ");
				queryString.append(" AND UPPER(model.ordenCompra.reporteCabina.poliza.numeroPoliza)   LIKE '%");
				queryString.append(numeroPoliza); queryString.append("%'");
				queryString.append(" AND model.ordenCompra.reporteCabina.poliza.numeroRenovacion = :numeroRenovacion ");
				Utilerias.agregaHashLista(listaParametrosValidos, "codigoProducto", codigoProducto);
				Utilerias.agregaHashLista(listaParametrosValidos, "codigoTipoPoliza", codigoTipoPoliza);
				Utilerias.agregaHashLista(listaParametrosValidos, "numeroRenovacion", numeroRenovacion);
			}else{
				queryString.append(" AND  UPPER(model.ordenCompra.reporteCabina.poliza.numeroPoliza)  LIKE '%");
				queryString.append(filtroPT.getNumeroPoliza()); queryString.append("%' ");
			}
		}	
		
		if(filtroPT.getNumeroSerie() != null && !filtroPT.getNumeroSerie().isEmpty()){
			queryString.append(" AND model.ordenCompra.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie = :numeroSerie ");
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroSerie", filtroPT.getNumeroSerie());
		}
		if(filtroPT.getNumeroValuacion() != null && filtroPT.getNumeroValuacion() != 0){
			queryString.append(" AND model.ordenCompra.numeroValuacion = :numeroValuacion ");
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroValuacion", filtroPT.getNumeroValuacion());
		}
		if(filtroPT.getFechaSiniestro() != null){
			queryString.append(" AND model.ordenCompra.reporteCabina.fechaOcurrido = :fechaOcurrido ");
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaOcurrido", filtroPT.getFechaSiniestro());
		}
		if(filtroPT.getEtapa() != null && !filtroPT.getEtapa().isEmpty()){
			queryString.append(" AND model.ordenCompra IN (SELECT indemnizacion.ordenCompra FROM IndemnizacionSiniestro indemnizacion WHERE indemnizacion.ordenCompra = model.ordenCompra AND indemnizacion.etapa = :etapa ) ");
			Utilerias.agregaHashLista(listaParametrosValidos, "etapa", filtroPT.getEtapa());
		}
		if(filtroPT.getEstatusPerdidaTotal() != null && !filtroPT.getEstatusPerdidaTotal().isEmpty()){
			queryString.append(" AND model.ordenCompra IN (SELECT indemnizacion.ordenCompra FROM IndemnizacionSiniestro indemnizacion WHERE indemnizacion.ordenCompra = model.ordenCompra AND indemnizacion.estatusPerdidaTotal = :estatusPT ) ");
			Utilerias.agregaHashLista(listaParametrosValidos, "estatusPT", filtroPT.getEstatusPerdidaTotal());
		}
		if(filtroPT.getNumPaseAtencion() != null && !filtroPT.getNumPaseAtencion().isEmpty()){
			queryString.append(" AND model.ordenCompra.idTercero IN (SELECT estimacion.id FROM EstimacionCoberturaReporteCabina estimacion WHERE estimacion.id =  model.ordenCompra.idTercero AND estimacion.folio = :numPaseAtencion ) ");
			Utilerias.agregaHashLista(listaParametrosValidos, "numPaseAtencion", filtroPT.getNumPaseAtencion());
		}
		if(filtroPT.getEstatusIndemnizacion() != null && !filtroPT.getEstatusIndemnizacion().isEmpty()){
			queryString.append(" AND model.ordenCompra IN (SELECT indemnizacion.ordenCompra FROM IndemnizacionSiniestro indemnizacion WHERE indemnizacion.ordenCompra = model.ordenCompra AND indemnizacion.estatusIndemnizacion = :estatusIndemnizacion ) ");
			Utilerias.agregaHashLista(listaParametrosValidos, "estatusIndemnizacion", filtroPT.getEstatusIndemnizacion());
		}
		
		Query query 				= entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		ordenes = (List<OrdenCompra>)query.getResultList();
		
		return ordenes;
	}
	
	/**
	 * obtiene la lista de los tipos de concepto de ajuste que pertenecen a indemnizaciones
	 * @return
	 */
	private List<String> obtenerConceptosDePerdidaTotal(){
		List<String> conceptosPT = new ArrayList<String>();
		
		Map<String,String> mapaConceptos = perdidaTotalService.obtenerTiposSiniestroPerdidaTotal();
		for(String key: mapaConceptos.keySet()){
			conceptosPT.add(key);
		}
		
		return conceptosPT;
	}
	
	/**
	 * Genera una lista de los estatus de las ordenes de compra que pertenencen a indemnizaciones
	 * @return
	 */
	private List<String> obtenerEstatusesIndemnizacion(){
		List<String> estatusesIndemnizacion = new ArrayList<String>();
		estatusesIndemnizacion.add(OrdenCompraService.ESTATUS_INDEMNIZACION);
		estatusesIndemnizacion.add(OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA);
		estatusesIndemnizacion.add(OrdenCompraService.ESTATUS_INDEMNIZACION_CANCELADA);
		return estatusesIndemnizacion;
	}
	
	@SuppressWarnings({"unchecked"})
	@Override
	public List<RolesIndemnizacionAutorizacion> obtenerListaRolesAutorizacionSinAutorizacionFinal() {
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.domain.siniestros.indemnizacion.RolesIndemnizacionAutorizacion( model.rol, model.descripcionRol ) FROM RolesIndemnizacionAutorizacion model ");
		queryString.append(" WHERE model.autorizacionFinal='N' AND model.requerida='N' AND model.activa='S' GROUP BY model.rol, model.descripcionRol ");
		Query query = entityManager.createQuery(queryString.toString());
		return query.getResultList();
	}
	
	
	@Override
	public IndemnizacionSiniestro obtenerOrdenCompraRobo(Long roboId){
		StringBuilder queryString = new StringBuilder("SELECT model FROM IndemnizacionSiniestro model JOIN ReporteRoboSiniestro robo ");
		queryString.append(" ON model.ordenCompraGenerada.coberturaReporteCabina.id = robo.coberturaReporteCabinaId AND robo.id = :roboId ");		
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("roboId", roboId);
		return (IndemnizacionSiniestro) query.getSingleResult();
	}
	
	@Override
	public IndemnizacionSiniestro obtenerIndemnizacionActivaPorEstimacion(Long idEstimacion){
		IndemnizacionSiniestro indemnizacion = null;
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT model FROM IndemnizacionSiniestro model ");
		queryString.append(" WHERE model.ordenCompra.idTercero = :idEstimacion " );
		queryString.append(" AND model.ordenCompra.estatus <> :estatusCancelado");
		
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("idEstimacion", idEstimacion);
		query.setParameter("estatusCancelado", OrdenCompraService.ESTATUS_INDEMNIZACION_CANCELADA);
		try{
			indemnizacion = (IndemnizacionSiniestro)query.getSingleResult();
		}catch(Exception ex){			
		}
		
		return indemnizacion;
	}
}
