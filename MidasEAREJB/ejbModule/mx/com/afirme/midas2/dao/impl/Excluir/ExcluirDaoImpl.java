package mx.com.afirme.midas2.dao.impl.Excluir;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.Excluir.ExcluirDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ExcluirDaoImpl extends EntidadDaoImpl implements ExcluirDao {

	private static final Logger LOG = Logger.getLogger(ExcluirDaoImpl.class);

	private EntidadService entidadService;

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.afirme.midas2.dao.Excluir.ExcluirDao#eliminarExcluir(mx.com.afirme
	 * .midas2.domain.catalogos.ExcluirAgentes.Excluir)
	 */
	public void eliminarExcluir(Excluir excluir) {
		entidadService.remove(excluir);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.afirme.midas2.dao.Excluir.ExcluirDao#getListExcluirAgentes(mx.
	 * com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir)
	 */
	@SuppressWarnings({ "unchecked" })
	public List<Excluir> getListExcluirAgentes(Excluir excluir) {
		List<Excluir> listExcluir = new ArrayList<Excluir>();
		try {
			StringBuilder queryString = new StringBuilder();
			Map<String, Object> params = new HashMap<String, Object>();
			queryString
					.append("Select model from Excluir model where model.estatus = 1");

			if (excluir.getIdAgente() != null
					&& !UtileriasWeb.esCadenaVacia(excluir.getIdAgente())) {
				queryString.append(" and model.idAgente = :idAgenteAdd ");
				params.put("idAgenteAdd", excluir.getIdAgente());
			}

			if (excluir.isComision()) {
				queryString.append(" and model.comision = :chkComision ");
				params.put("chkComision", excluir.isComision());

			}
			if (excluir.isBono()) {
				queryString.append(" and model.bono = :chkBono ");
				params.put("chkBono", excluir.isBono());

			}
			// Order
			queryString.append(" ORDER BY model.id desc");

			// listExcluir = entidadService.findAll(Excluir.class);//Regresa
			Query query = this.entityManager
					.createQuery(queryString.toString());
			if (!params.isEmpty()) {
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);

			listExcluir = query.getResultList();

		} catch (Exception e) {
			LOG.info("Error " + e);
		}
		return listExcluir;
	}

	public void setQueryParametersByProperties(Query entityQuery,
			Map<String, Object> parameters) {
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>) it
					.next();
			String key = getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}
	}

}
