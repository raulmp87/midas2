package mx.com.afirme.midas2.dao.impl.compensaciones;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import mx.com.afirme.midas2.dao.compensaciones.CaBancaPrimPagDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPag;
import java.util.List;
import org.apache.log4j.Logger;

@Stateless
public class CaBancaPrimPagDaoImpl implements CaBancaPrimPagDao{

	
	public static final String OFSUC = "ofsuc";
	public static final String NOM_CANAL = "nomCanal";
	public static final String COD_CANAL = "codCanal";
	public static final String CANAL_NVO = "canalNvo";
	public static final String NOM_GRUPO = "nomGrupo";
	public static final String AGTE = "agte";
	public static final String ID_AGENTE = "idAgente";
	public static final String NOM_AGTE = "nomAgte";
	public static final String NOM_EJEC = "nomEjec";
	public static final String NOM_RAMO = "nomRamo";
	public static final String ID_CENTRO_EMISION = "idCentroEmision";
	public static final String NUM_POLIZA = "numPoliza";
	public static final String NUM_RENOV_POLIZ = "numRenovPoliz";
	public static final String SEC = "sec";
	public static final String MON = "mon";
	public static final String ASEGURADO = "asegurado";
	public static final String LINEA_NEG = "lineaNeg";
	public static final String PMA_EMI = "pmaEmi";
	public static final String PMA_PAGADA = "pmaPagada";
	public static final String ORDIMP = "ordimp";
	public static final String ANIO = "anio";
	public static final String MES = "mes";
	public static final String BANCA_PRIMPAG_CP_ID = "bancaPrimpagCpId";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = Logger.getLogger(CaBancaPrimPagDaoImpl.class);
	
	public void save(CaBancaPrimPag entity){
		LOG.info(">> save()");
		try{
			entityManager.persist(entity);
			LOG.info("<< save()");
		}catch(RuntimeException re){
			LOG.error("Información del Error", re);
			throw re;
		}
	}
	
	public void delete(CaBancaPrimPag entity){
		LOG.info(">> delete()");
		try{
			entity = entityManager.getReference(CaBancaPrimPag.class,entity.getBancaPrimpagCpId() );
			entityManager.remove(entity);
			LOG.info("<< delete()");
		} catch(RuntimeException re){
			LOG.error("Información del Error",re);
			throw re;
		}
	}
	
	public CaBancaPrimPag update(CaBancaPrimPag entity) {
		LOG.info(">> update()");
		try {
			CaBancaPrimPag result = entityManager.merge(entity);
			LOG.info("<< update()");
			return result;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}
	
	public CaBancaPrimPag findById(Long id) {
		LOG.info(">> findById()");
		try {
			CaBancaPrimPag instance = entityManager.find(CaBancaPrimPag.class, id);
			LOG.info("<< findById()");
			return instance;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<CaBancaPrimPag> findByProperty(String propertyName,final Object value) {
		LOG.info(">> findByProperty()");
		try {
			final String queryString = "select model from CaBancaPrimPag model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOG.info("<< findByProperty()");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}
	
	public int deleteRecordsByProperty(String propertyName, Object value){
		LOG.info(">> deleteRecordsByProperty()");
		int deleteCount = 0;
		try{
			StringBuilder queryString = new StringBuilder("DELETE FROM CaBancaPrimPag model");
			queryString.append(" WHERE model.").append(propertyName).append(" = ").append(" :propertyValue ");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("propertyValue", value);
			
			deleteCount = query.executeUpdate();
		} catch(RuntimeException re){
			LOG.error("Informacion del Error", re);
			throw re;
		}
		LOG.info("<< deleteRecordsByProperty()");
		return deleteCount;
	}

}
