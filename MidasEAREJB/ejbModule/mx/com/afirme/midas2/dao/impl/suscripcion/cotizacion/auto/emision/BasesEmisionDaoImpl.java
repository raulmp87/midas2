package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.emision;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.emision.BasesEmisionDao;
import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO.DatosBasesEmisionParametrosDTO;


import javax.annotation.Generated;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.3.0.v20110604-r9504", date="2012-08-11T10:51:44")
@StaticMetamodel(AgenteDTO.class)

@Stateless
public class BasesEmisionDaoImpl implements BasesEmisionDao {

	@Override
	public List<DatosBasesEmisionDTO> obtenerBasesEmision(DatosBasesEmisionParametrosDTO parametros)  throws Exception {
		List<DatosBasesEmisionDTO> listaResultado = new ArrayList<DatosBasesEmisionDTO>(1);
		StoredProcedureHelper storedHelper = null;
		
		String nombreSP = "MIDAS.pkgAUT_Reportes.spAUT_RepBasesEmision";
		
		try {
			
			String[] nombreParametros = {
					"pFechaInicio", 
					"pFechaFin",
					"pIdRamo",
					"pIdSubRamo",
					"pNivelContratoReas",
					"pNivelAgrupamiento"};
			Object[] valorParametros={parametros.getFechaInicio(),
					parametros.getFechaFin(),
					parametros.getIdRamo(),
					parametros.getIdSubRamo(),
					parametros.getIdNivelContratoReaseguro(),
					parametros.getNivelAgrupamiento().intValue()};
			
			
			//TODO: falta agregar la relación de columnas
			String[] props = obtenerPropiedades();
			String[] cols = obtenerColumnas();
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			//se establecen los parametros.8
			storedHelper.estableceMapeoResultados(
					DatosBasesEmisionDTO.class.getCanonicalName(),props,cols);
			
			
			LogDeMidasEJB3.log("Consultando reporte bases emision auto. SP a invocar: "+nombreSP+", Parametros: "+parametros.toString(), Level.INFO, null);
			//se obtiene la lista de objetos.
			listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log("Finaliza consulta reporte bases emision auto. SP invocado: "+nombreSP+", Parametros: "+parametros.toString()+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);
	
		} catch (SQLException e) {
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.BUSCAR,nombreSP, DatosBasesEmisionDTO.class, codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de BasesEmisionDaoImpl.obtenerBasesEmision..." + this, Level.WARNING, e);
				throw e;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Ocurrio un error al consultar reporte bases emision. SP invocado: "+nombreSP, Level.SEVERE, e);
			throw e;
		}
		return listaResultado;
	}
	
	private String[] obtenerColumnas(){
		String[] cols = new String[]{
				"OFICINAEMISION",
				"DESCOFICINAEMISION",
				"DESCRAMO",
				"CTACONTABLERAMO",
				"DESCSUBRAMO",
				"CTACONTABLESUBRAMO",
				"NUMEROPOLIZA",
				"NUMERORENOVACION",
				"MONEDAPOLIZA",
				"CODIGOPRODUCTO",
				"DESCPRODUCTO",
				"CODIGOTIPONEGOCIO",
				"DESCTIPONEGOCIO",
				"CODIGOUSUARIOASEGURADO",
				"NOMBREUSUARIOASEGURADO",
				"CLAVEAGENTE",
				"NOMBREAGENTE",
				"FORMAPAGO",
				"ESTATUSPOLIZA",
				"GIROASOCIADOPOLIZA",
				"NUMEROENDOSO",
				"DESCTIPOENDOSO",
				"DESCMOTIVOENDOSO",
				"FECHAEMISION",
				"FECHAINICIOVIGENCIA",
				"FECHAFINVIGENCIA",
				"TIPOCAMBIO",
				"IDPROMOTORIA",
				"IDGERENCIA",
				"IDCONTRATANTE",
				"CODIGOUSUARIOEMISION",
				"TIPOUSOVEHICULO",
				"NUMEROINCISO",
				"DESCSECCION",
				"DESCCOBERTURA",
				"IMPORTEPRIMANETA",
				"IMPORTEDERECHOS",
				"IMPORTECOMISIONES",
				"IMPORTEBONIFCOM",
				"IMPORTECOMISIONESRPF",
				"IMPORTEBONIFCOMRPF",
				"IMPORTERECARGOS",
				"IMPORTEIVA",
				"BONIFICACION",
				"PROMDESCRIPCION",
				"GERENCIANOMBRE",
				"CONTRATANTENOMBRE",
				"CONTRATANTETIPOPERSONA",
				"IDMEDIOPAGO",
				"MEDIOPAGO",
				"AGENTETIPOPERSONA",
				"COMISIONPROMOTORIAFISICA",
				"COMISIONPROMOTORIAMORAL",
				"COMISIONAGENTEFISICA",
				"COMISIONAGENTEMORAL",
				"COMRECARGOAGENTEFISICA",
				"COMRECARGOAGENTEMORAL",
				"IMPORTETOTAL",
				"COBERTURARCUSA",
				"CODIGOTIPOUSOVEHICULO",
				"DESCRIPCIONTIPOUSOVEHICULO",
				"CODIGOTIPOSERVICIOVEHICULO",
				"DESCRIPCIONTIPOSERVVEHICULO",
				"PORCENTAJEDESCUENTO",
				"PARTE"
		};
		return cols;
	}
	private String[] obtenerPropiedades(){
		String[] props = new String[]{
				"oficinaEmision",
				"descOficinaEmision",
				"descRamo",
				"ctaContableRamo",
				"descSubramo",
				"ctaContableSubramo",
				"numeroPoliza",
				"numeroRenovacion",
				"monedaPoliza",
				"codigoProducto",
				"descProducto",
				"codigoTipoNegocio",
				"descTipoNegocio",
				"codigoUsuarioAsegurado",
				"nombreUsuarioAsegurado",
				"claveAgente",
				"nombreAgente",
				"formaPago",
				"estatusPoliza",
				"giroAsociadoPoliza",
				"numeroEndoso",
				"descTipoEndoso",
				"descMotivoEndoso",
				"fechaEmision",
				"fechaInicioVigencia",
				"fechaFinVigencia",
				"tipoCambio",
				"idPromotoria",
				"idGerencia",
				"idContratante",
				"codigoUsuarioEmision",
				"tipoUsoVehiculo",
				"numeroInciso",
				"descSeccion",
				"descCobertura",
				"importePrimaNeta",
				"importeDerechos",
				"importeComisiones",
				"importeBonifCom",
				"importeComisionesRPF",
				"importeBonifcomRPF",
				"importeRecargos",
				"importeIva",
				"bonificacion",
				"promDescripcion",
				"gerenciaNombre",
				"contratanteNombre",
				"contratanteTipoPersona",
				"idMedioPago",
				"medioPago",
				"agenteTipoPersona",
				"comisionPromotoriaFisica",
				"comisionPromotoriaMoral",
				"comisionAgenteFisica",
				"comisionAgenteMoral",
				"comRecargoAgenteFisica",
				"comRecargoAgenteMoral",
				"importeTotal",
				"coberturaRcUSA",
				"codigoTipoUsoVehiculo",
				"descripcionTipoUsoVehiculo",
				"codigoTipoServicioVehiculo",
				"descripcionTipoServVehiculo",
				"porcentajeDescuento",
				"parte"
		};
		
		return props;
	}
	
	
	
}
