package mx.com.afirme.midas2.dao.impl.siniestros.notificaciones;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.notificaciones.BitacoraNotificacionDao;
import mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraDetalleNotificacion;
import mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraNotificacion;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService.BitacoraNotificacionFiltroDTO;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService.BitacoraRegistroDetalleNotificacionDTO;
import mx.com.afirme.midas2.util.JpaUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class BitacoraNotificacionDaoImpl extends JpaDao<Long, BitacoraNotificacion> implements BitacoraNotificacionDao {

	@Override
	public List<BitacoraRegistroDetalleNotificacionDTO> obtenerRecurrentes() {		
		Map<String, Object> parametros = new HashMap<String, Object>();		
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService.BitacoraRegistroDetalleNotificacionDTO( ");
		queryString.append("detalle.destino.bitacora.id, detalle.destino.bitacora.folio, detalle.destino.bitacora.configuracion, detalle.destino.bitacora.recurrente, ");
		queryString.append("detalle.destino.destinatario, detalle.mapaDatos, detalle.id.secuenciaEnvio, detalle.codigoUsuarioCreacion, detalle.fechaCreacion ");
		queryString.append(") FROM BitacoraDetalleNotificacion detalle WHERE detalle.destino.bitacora.recurrente = 1 AND detalle.id.secuenciaEnvio = ");
		queryString.append("(SELECT max(m.id.secuenciaEnvio) FROM BitacoraDetalleNotificacion m WHERE m.id.destinoId = detalle.id.destinoId AND detalle.destino.bitacora.id = m.destino.bitacora.id) ");						
		TypedQuery<BitacoraRegistroDetalleNotificacionDTO> query = entityManager.createQuery(queryString.toString(), BitacoraRegistroDetalleNotificacionDTO.class);
		setQueryParameters(query, parametros);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);		
		return query.getResultList();	
	}
	
	@Override
	public List<BitacoraDetalleNotificacion> obtenerRegistros(
			BitacoraNotificacionFiltroDTO filtro) {
		Map<String, Object> parametros = new HashMap<String, Object>();		
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraDetalleNotificacion( ");
		queryString.append("detalle.id.secuenciaEnvio, detalle.mapaDatos, detalle.destino, detalle.codigoUsuarioCreacion ");
		queryString.append(") FROM BitacoraDetalleNotificacion detalle ");
		if(filtro != null){						
			JpaUtil.addFilterParameter("detalle", queryString, filtro, parametros);
		}
		queryString.append(" ORDER BY detalle.destino.bitacora.fechaCreacion desc ");
		TypedQuery<BitacoraDetalleNotificacion> query = entityManager.createQuery(queryString.toString(), BitacoraDetalleNotificacion.class);
		setQueryParameters(query, parametros);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);		
		return query.getResultList();	
	}

}
