package mx.com.afirme.midas2.dao.impl.siniestros.pagos;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.bitemporal.siniestros.PolizaSiniestroDaoImpl;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.pagos.PagosSiniestroDao;
import mx.com.afirme.midas2.dao.siniestros.pagos.facturas.RecepcionFacturaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.IncisoPolizaDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.OrdenPagosDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.bloqueo.BloqueoPagoService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.FacturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.JpaUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;



@Stateless
public class PagosSiniestroDaoImpl extends EntidadDaoImpl implements PagosSiniestroDao{
	@EJB
	EntidadDao entidadDao;
	
	
	@EJB
	CotizacionEndosoService cotizacionEndosoService;
	@EJB
	CatalogoGrupoValorService  catalogoGrupoValorService;

	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private OrdenCompraService ordenCompraService;
	
	@EJB
	private MonedaFacadeRemote monedaFacadeRemote;

	@EJB
	private PagosSiniestroService pagosSiniestroService;
	
	
	@EJB
	private BloqueoPagoService bloqueoPagoService;
	
	@EJB
	private RecepcionFacturaDao recepcionFacturaDao;
	
	private static final Logger LOG = Logger.getLogger(PagosSiniestroDaoImpl.class);
	
	public String SERV_PARTICULAR = "1"; 
	public String SERV_PUBLICO = "2";
	public String SERV_AMBOS = "3"; 
	
	public String TIPO_PAGOPROVEEDOR = "PP"; 
	public String TIPO_PAGOBENEFICIARIO = "PB"; 
	
	public String APLICA_DEDUCIBLE = "SI";
	public String NOAPLICA_DEDUCIBLE = "NO";
	
	public String ESTATUS_FACTURA_REGISTRADA = "R";
	public String ESTATUS_FACTURA_PAGADA = "P";
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoFiscal> getFacturasConOrdenesCompraOrderByDateLow(Long idProveedor){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_ORDENCOMPRA.GETFACTURASXPROVEEDOR";
		List<DocumentoFiscal> facturasWithOrdersCompra = new ArrayList<DocumentoFiscal>();
		
		try{
			String columnaBd = "ID,FOLIO,DETALLE,SUBTOTAL";
			String atributos = "id,numeroFactura,facturaConOrdenesCompra,montoTotal";
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(DocumentoFiscal.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("pIdProveedor", idProveedor);
			
			facturasWithOrdersCompra = spHelper.obtieneListaResultados();
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		return facturasWithOrdersCompra;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DesglosePagoConceptoDTO> obtenerDetalleOrdenPago (Long idOrdenPago, Boolean calcularReserva){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_ORDENCOMPRA.GETDETALLEORDENPAGO";
		List<DesglosePagoConceptoDTO> listResultado = new ArrayList<DesglosePagoConceptoDTO> (6);
		
		try{
			//String columnaBd = "CONCEPTO,IDCOMPRA,IDPAGO,SUBTOTALXPAGAR,PORCENTAX,TAX,TAXTOTALXPAGAR,TOTAL,IMPORTPAGADO,PORCENTISR,IMPSOBRERENTA,ISRTOTALXPAG,PORCENTAXRET,IVARETENIDO,IVARETOTALXPAG,TOTALESXPAGAR,TIPOORDEN";
			//String atributos = "conceptoPago,idDetalleOrdenCompra,idDetalleOrdenPago,subTotalPorPagar,porIva,iva,ivaTotalPorPagar,total,importePagado,porIsr,isr,isrTotalPorPagar,porIvaRetenido,ivaRetenido,ivaRetenidoTotalPorPagar,totalesPorPagar,tipoOrden";
			String columnaBd = "CONCEPTO,IDCOMPRA,IDPAGO,DEDUCIBLETOTAL,SALVAMENTO,SUBTOTALXPAGAR,PORCENTAX,TAX,TAXTOTALXPAGAR,TOTAL,IMPORTPAGADO,PORCENTISR,IMPSOBRERENTA,ISRTOTALXPAG,PORCENTAXRET,IVARETENIDO,IVARETOTALXPAG,TOTALESXPAGAR,TIPOORDEN,DESCUENTO";
			String atributos = "conceptoPago,idDetalleOrdenCompra,idDetalleOrdenPago,deduciblesTotal,salvamentoTotalPorPagar,subTotalPorPagar,porIva,iva,ivaTotalPorPagar,total,importePagado,porIsr,isr,isrTotalPorPagar,porIvaRetenido,ivaRetenido,ivaRetenidoTotalPorPagar,totalesPorPagar,tipoOrden,descuentosTotal";
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(DesglosePagoConceptoDTO.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("IDORDENPAGO", idOrdenPago);
			
			listResultado = spHelper.obtieneListaResultados();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		return listResultado;
	}
	
	
	@Override
	public List<DesglosePagoConceptoDTO> obtenerPagosPorLiquidacion (Long idLiquidacion){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_ORDENCOMPRA.PAGOSPORLIQUIDACION";
		List<DesglosePagoConceptoDTO> listResultado = new ArrayList<DesglosePagoConceptoDTO> (6);
		
		try{
			String columnaBd = "CONCEPTO,IDCOMPRA,IDPAGO,SUBTOTALXPAGAR,PORCENTAX,TAX,TAXTOTALXPAGAR,TOTAL,IMPORTPAGADO,PORCENTISR,IMPSOBRERENTA,ISRTOTALXPAG,PORCENTAXRET,IVARETENIDO,IVARETOTALXPAG,TOTALESXPAGAR,DEDUCIBLE,DESCUENTO";
			String atributos = "conceptoPago,idDetalleOrdenCompra,idDetalleOrdenPago,subTotalPorPagar,porIva,iva,ivaTotalPorPagar,total,importePagado,porIsr,isr,isrTotalPorPagar,porIvaRetenido,ivaRetenido,ivaRetenidoTotalPorPagar,totalesPorPagar,deduciblesTotal,descuentosTotal";
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(DesglosePagoConceptoDTO.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("pIdLiquidacion", idLiquidacion);
			
			listResultado = spHelper.obtieneListaResultados();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		return listResultado;
	}
	
	/**
	 * Obtiene el detalle de una orden de pago
	 * 
	 * @param OrdenPagoSiniestro ordenPago
	 */
	@Override 
	public List<OrdenPagosDTO> buscarOrdenesPago(OrdenPagosDTO filtro,boolean soloOrdenCompra) {
		Map<String, Object> parameters = new HashMap<String, Object>();	

		//Descriptor de ordenes de pago
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.pagos.OrdenPagosDTO(");

		queryString.append("siniestroCab.claveOficina, siniestroCab.consecutivoReporte, siniestroCab.anioReporte,");
		queryString.append("compra.reporteCabina.oficina.id, compra.reporteCabina.oficina.nombreOficina, "); 
		queryString.append( "func('CONCAT', siniestroCab.claveOficina, func('CONCAT', '-', func('CONCAT', siniestroCab.consecutivoReporte, func('CONCAT', '-', siniestroCab.anioReporte)))), ");
		queryString.append("cast(null as char), FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TERMINO_AJUSTE_SINIESTRO', autoIncisoRepCab.terminoAjuste), ");
		queryString.append("compra.id, FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'ESTATUS_ORDEN_PAGO', ordenPago.estatus), compra.nomBeneficiario, ");
		queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_PERSONA', compra.tipoPersona), compra.idBeneficiario, ");
		queryString.append("(select tipoPrestador.descripcion FROM ").append(TipoPrestadorServicio.class.getSimpleName()).append(" tipoPrestador where tipoPrestador.nombre = compra.tipoProveedor), ");
		queryString.append("persona.nombre, compra.reporteCabina.id, ordenPago.id,  ");
		queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'ESTATUS_ORDEN_PAGO', ordenPago.estatus) , cobertura.descripcion,");
		queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_ORDEN_PAGO', ordenPago.tipoPago), cast(null as char), ");
		queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_ORDEN_COMPRA', compra.tipo) , compra.tipoPago,  ");
		queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_PAGOORDEN_COMPRA', compra.tipoPago), compra.tipo, ");
		queryString.append("ordenPago.fechaPago, ordenPago.origenPago,  ordenPago.fechaCreacion, compra.fechaCreacion, ");
		queryString.append("compra.curp, compra.rfc, (select banco.nombreBanco from ").append(BancoEmisorDTO.class.getSimpleName()).append(" banco where banco.idBanco = coalesce(compra.bancoId,0)), ");
		queryString.append("compra.clabe, compra.correo, compra.telefono, ordenPago.fechaActualizacionSeycos,  ");
		queryString.append("compra, compra.reporteCabina.siniestroCabina, ordenPago.motivoCancelacion, ");
		queryString.append("ordenPago.factura, ordenPago.comentarios,  ordenPago.fechaCancelacion, ordenPago.codigoUsuarioCancela ");
		//queryString.append("(select count(bloqueo.id) from BloqueoPago bloqueo where bloqueo.ordenPago.id = ordenPago.id)"); 
		queryString.append(") FROM ").append(OrdenCompra.class.getSimpleName());
		queryString.append(" compra LEFT JOIN ").append(PrestadorServicio.class.getSimpleName()).append(" proveedor ON compra.idBeneficiario = proveedor.id ");
		queryString.append(" LEFT JOIN proveedor.personaMidas persona ");
		queryString.append(" LEFT JOIN compra.ordenPago ordenPago ");
		queryString.append(" LEFT JOIN compra.coberturaReporteCabina cobRepCabina ");
		queryString.append(" LEFT JOIN cobRepCabina.coberturaDTO cobertura ");
		queryString.append(" LEFT JOIN compra.reporteCabina.siniestroCabina siniestroCab ");
		queryString.append(" LEFT JOIN compra.reporteCabina.seccionReporteCabina seccionRepCab ");
		queryString.append(" LEFT JOIN seccionRepCab.incisoReporteCabina incisoRepCab ");
		queryString.append(" LEFT JOIN incisoRepCab.autoIncisoReporteCabina autoIncisoRepCab ");		
		queryString.append(" WHERE (compra.origen IN ('").append(OrdenCompraService.ORIGENHGS_AUTOMATICO).append("', '")
				.append(OrdenCompraService.ORIGENMANUAL).append("', '")
				.append(OrdenCompraService.ORIGEN_INDEMNIZACION_AUTOMATICA)
				.append("', '").append(OrdenCompraService.ORIGENHGS_PERDIDATOTAL)
				.append("', '").append(OrdenCompraService.ORIGENHGS_DAÑOS).append("') ");
		queryString.append("AND exists ( select detalle.id FROM ").append(DetalleOrdenCompra.class.getSimpleName())
				.append(" detalle WHERE detalle.ordenCompra.id = compra.id) ");
		queryString.append(" AND  ((compra.estatus IN ('").append(OrdenCompraService.ESTATUS_AUTORIZADA).append("', '").append(OrdenCompraService.ESTATUS_ASOCIADA)
				.append("', '").append(OrdenCompraService.ESTATUS_PAGADA).append("') OR (compra.estatus = '").append(OrdenCompraService.ESTATUS_CANCELADA)
				.append("' AND exists (select pago.id from OrdenPagoSiniestro pago where compra.id = pago.ordenCompra.id))))) ");				
		
		if(null!=filtro ){
			//numero siniestro
			//JpaUtil.addParameter(queryString, modelOrdenCompra + "estatus", OrdenCompraService.ESTATUS_AUTORIZADA  , "estatusOrden", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, "compra.reporteCabina.siniestroCabina.claveOficina", filtro.getClaveOficinaSiniestro()  , "claveOficinaSin", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, "compra.reporteCabina.siniestroCabina.consecutivoReporte", filtro.getConsecutivoReporteSiniestro()  , "consecutivoReporteSin", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, "compra.reporteCabina.siniestroCabina.anioReporte", filtro.getAnioReporteSiniestro()  , "anioReporteSin", parameters, OperationType.LIKE.toString());
			//oficina
			JpaUtil.addParameter(queryString, "compra.reporteCabina.oficina.id", filtro.getOficinaId() , "oficinaId", parameters, JpaUtil.EQUAL );
			if(soloOrdenCompra==false ){
				//estatus
				JpaUtil.addParameter(queryString, "compra.ordenPago.estatus", filtro.getEstatus()  , "estatus", parameters, OperationType.LIKE.toString());
				//id orden pago
				JpaUtil.addParameter(queryString, "compra.ordenPago.id", filtro.getId() , "ordenPagoId", parameters, JpaUtil.EQUAL );
				//id orden pago seycos
				JpaUtil.addParameter(queryString, "compra.ordenPago.idOrdenPagoSeycos", filtro.getIdOrdenPagoSeycos() , "ordenPagoSeycos", parameters, JpaUtil.EQUAL );
			}
			//proveedor
			JpaUtil.addParameter(queryString, "compra.idBeneficiario", filtro.getIdBeneficiario() , "proveedor", parameters, JpaUtil.EQUAL );
			
			JpaUtil.addParameter(queryString, "compra.tipoProveedor", filtro.getTipoProveedor() , "tipoproveedor", parameters, OperationType.LIKE.toString() );
			
			//Factura 
			JpaUtil.addParameter(queryString, "compra.ordenPago.factura", filtro.getFactura() , "factura", parameters, OperationType.LIKE.toString());

			//servicio particular
			
			if((null!=filtro.getServParticular() && null!=filtro.getServPublico() )  &&(filtro.getServParticular() && filtro.getServPublico())){
				JpaUtil.addParameter(queryString, "compra.reporteCabina.oficina.tipoServicio", SERV_AMBOS, "tipoServicio1", parameters, JpaUtil.EQUAL);
			}else if( (null!=filtro.getServParticular())&&filtro.getServParticular()){
				JpaUtil.addParameter(queryString, "compra.reporteCabina.oficina.tipoServicio", SERV_PARTICULAR, "tipoServicio2", parameters, JpaUtil.EQUAL);
			}else if( (null!=filtro.getServPublico() ) &&filtro.getServPublico()){
				JpaUtil.addParameter(queryString, "compra.reporteCabina.oficina.tipoServicio", SERV_PUBLICO, "tipoServicio3", parameters, JpaUtil.EQUAL);
			}	
			//tipo
			JpaUtil.addParameter(queryString, "compra.tipo", filtro.getTipoPago() , "tipoOc", parameters, OperationType.LIKE.toString());
			//Tipo Pago Orden Compra
			JpaUtil.addParameter(queryString, "compra.tipoPago", filtro.getTipoPagoOrdenCompra() , "tipopagoOc", parameters, OperationType.LIKE.toString());
			//Liquidacion
			
			
			//termino ajuste
			JpaUtil.addParameter(queryString, "compra.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.terminoAjuste", filtro.getTerminoAjuste() , "termino", parameters, OperationType.LIKE.toString());
			//id orden compra
			JpaUtil.addParameter(queryString, "compra.id", filtro.getNumOrdenCompra() , "ordenCompraId", parameters, JpaUtil.EQUAL );
			//beneficiario
			JpaUtil.addParameterLike(queryString, "compra.nomBeneficiario", filtro.getBeneficiario() , "beneficiario", parameters);
			
			//concepto pago
			if(null!=filtro.getConceptoAjusteId()){
				queryString.append(" AND exists ( select detalle.conceptoAjuste.id FROM DetalleOrdenCompra detalle WHERE detalle.conceptoAjuste.id = :concepto AND detalle.ordenCompra.id = compra.id)");
				parameters.put("concepto", filtro.getConceptoAjusteId());				
				//JpaUtil.addParameter(queryString, "compra.detalle.conceptoAjuste.id", filtro.getConceptoAjusteId() , "concepto", parameters, JpaUtil.EQUAL);
			}

			
		}
	
		queryString.append(" ORDER BY ordenPago.estatus ASC , ordenPago.fechaCreacion ASC, compra.fechaCreacion DESC");
		
		@SuppressWarnings("unchecked")
		List<OrdenPagosDTO>  ordenesPagos =	(List<OrdenPagosDTO>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		List<MonedaDTO> monedas = monedaFacadeRemote.findAll();
		
		for(OrdenPagosDTO pago : ordenesPagos){
			OrdenCompra orden = pago.getOrdenCompra();
			if(pago.getId() != null){
				if(bloqueoPagoService.estaElPagoBloqueadoDirectamente(pago.getId()) || 
						bloqueoPagoService.estaElPagoBloqueadoPorProovedor(pago.getId())	){
					pago.setPagoBloqueado(true);
				}else {
					pago.setPagoBloqueado(false);

				}
				DesglosePagoConceptoDTO desglosePagoConceptoDTO=this.pagosSiniestroService.obtenerTotales(pago.getId(),false);
				if(null!=desglosePagoConceptoDTO){
					 pago.setTotalesPorPagar(desglosePagoConceptoDTO.getTotalesPorPagar());
				}
				if(!StringUtil.isEmpty(pago.getCancealadaPor())){
					Usuario user = usuarioService.buscarUsuarioPorNombreUsuario(pago.getCancealadaPor());
					if(null!=user){
						if(!StringUtil.isEmpty(user.getNombreCompleto())){
							pago.setCancealadaPor(user.getNombreCompleto());							
						}
					}
				}
				if(null!=orden.getOrdenPago()){
					pago.setIdEstatus(orden.getOrdenPago().getEstatus());
				}
				
				
				
				
			}
			if(null!=orden.getBancoId()){
				BancoMidas banco =this.entidadDao.findById(BancoMidas.class, new Long( orden.getBancoId()));
				if(null!=banco && !StringUtil.isEmpty(banco.getNombre())){
					pago.setBancoReceptor(banco.getNombre());
				}
			}
			
			pago.setRegistrarNuevaOrdenPago(this.isRegistrarOrdenPago( orden, orden.getOrdenPago()));
			pago.setEditarOrdenPago(this.isEditarOrdenPago( orden, orden.getOrdenPago()));
			pago.setConsultarOrdenPago(this.isConsultarOrdenPago( orden, orden.getOrdenPago()));
			pago.setCancelarOrdenPago(this.isCancelarOrdenPago( orden, orden.getOrdenPago()));
			pago.setRegistrarFactura(this.isRegistrarFacturaOrdenPago( orden, orden.getOrdenPago()));
			pago.setDevolverFactura(this.isDevolverFacturaOrdenPago( orden, orden.getOrdenPago()));
			pago.setBloquearPago(this.isBloquearPago(orden, orden.getOrdenPago()));
			ImportesOrdenCompraDTO importes= this.ordenCompraService.calcularImportes(orden.getId(),null,false);
			pago.setTotalOrdenCompra(importes.getTotal());
			EstimacionCoberturaReporteCabina estimacion=null;
			if (null!= orden.getIdTercero() ){
				estimacion= entidadDao.findById(EstimacionCoberturaReporteCabina.class,orden.getIdTercero() );
			}
			/*if (null!=estimacion && null != orden.getCoberturaReporteCabina() 
					&& null!= orden.getCoberturaReporteCabina().getCoberturaDTO()
					&& EnumUtil.equalsValue(ClaveTipoCalculo.DANIOS_MATERIALES, orden.getCoberturaReporteCabina().getCoberturaDTO().getClaveTipoCalculo())){
				TerceroDanosMateriales terceroDanosMateriales=null;
				try{
				 terceroDanosMateriales= ((TerceroDanosMateriales) estimacion);
				}catch (Exception e ){
					terceroDanosMateriales=null;
				}
				
				if(null!=terceroDanosMateriales && null!=terceroDanosMateriales.getAplicaDeducible() && terceroDanosMateriales.getAplicaDeducible()){
					pago.setAplicaDeducible(APLICA_DEDUCIBLE);
				}else
					pago.setAplicaDeducible(NOAPLICA_DEDUCIBLE);
			}else{
				pago.setAplicaDeducible(NOAPLICA_DEDUCIBLE);
			}*/
			if(null!=orden.getAplicaDeducible() && orden.getAplicaDeducible()){
				pago.setAplicaDeducible(APLICA_DEDUCIBLE);
			}else{
				pago.setAplicaDeducible(NOAPLICA_DEDUCIBLE);
			}
			DocumentoFiscal factura = null;
			List<DocumentoFiscal>lstFacturas =new ArrayList<DocumentoFiscal>();
			List<DocumentoFiscal>lstFacturasRegistradas =this.recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(orden.getId(), FacturaSiniestroService.ESTATUS_FACTURA_REGISTRADA);
			List<DocumentoFiscal>lstFacturasPagadas =this.recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(orden.getId(), FacturaSiniestroService.ESTATUS_FACTURA_PAGADA) ;
			
			
			if(!lstFacturasPagadas.isEmpty()){
				lstFacturas.addAll(lstFacturasPagadas);		
			}
			if(!lstFacturasRegistradas.isEmpty()){
				lstFacturas.addAll(lstFacturasRegistradas);		
			}
			
			if(!lstFacturas.isEmpty()){
				factura=lstFacturas.get(0);					
			}
			
			if(null!=factura){
				pago.setNumFactura(factura.getNumeroFactura());
				pago.setFechaFactura(factura.getFechaFactura());
				pago.setMonedaPago(factura.getMonedaPago());
				
				if( !StringUtil.isEmpty(factura.getMonedaPago())  &&  StringUtils.isNumeric(factura.getMonedaPago()) ){
					for(MonedaDTO moneda : monedas){
						if(moneda.getIdTcMoneda().toString().equals(factura.getMonedaPago())){
							pago.setMonedaPago(moneda.getDescripcion());
							break;
						}
					}						
				}					
				pago.setFechaRecepcionMatriz(factura.getFechaRecepcionMatriz());
				BigDecimal zero = new BigDecimal(0.0);
				if(null==factura.getMontoTotal()){
					pago.setMontoFactura (zero);
				}else
					pago.setMontoFactura(factura.getMontoTotal());
				
				if(!StringUtil.isEmpty( factura.getEstatus())){
					pago.setEstatusfactura(pago.getPago());								
				}
				pago.setFechaRegistroFactura(factura.getFechaRegistro());
			}
			
		}
		
		return ordenesPagos;
	}
//joksrc separator
	@SuppressWarnings("unused")
	private Map<String, String> generaValoresCatalogoValorFijo(TIPO_CATALOGO tipo){
		Map<String, String> map = new HashMap<String, String>();
		CatGrupoFijo grupo = catalogoGrupoValorService.obtenerGrupo(tipo);
		if(grupo != null){
			for(CatValorFijo valor : grupo.getValores()){
				map.put(valor.getCodigo(), valor.getDescripcion());
			}
		}
		return map;
	}
	
	@SuppressWarnings("unused")
	private String obtenerDescripcion(Map<String, String> map, String key){
		if(map != null && !StringUtil.isEmpty(key) && map.containsKey(key)){
			return map.get(key);
		}
		return "";
	}
	
	/**
	 *Registrar Nueva Orden de Pago - Cuando no exista un ID de la orden de pago relacionada a la orden de compra. 
	 * */
	@SuppressWarnings("unchecked")
	private boolean isRegistrarOrdenPago(OrdenCompra ordenCompra,OrdenPagoSiniestro ordenPago){
		String modelOrdenPago ="pago."; 
		Map<String, Object> parameters = new HashMap<String, Object>();	
		//**si la oden de compra es de tipo proveedor y no tiene factura registrada asociada. 
		//no debe permitirse registrar
		
		if(ordenCompra.getTipoPago().equalsIgnoreCase(TIPO_PAGOPROVEEDOR)){
			Map<String, Object> params = new HashMap<String, Object>();	
			params.put("ordenCompra.id", ordenCompra.getId());
			params.put("estatus", ESTATUS_FACTURA_REGISTRADA);//Preguntar Estatus
			List<DocumentoFiscal>lstFacturas =new ArrayList<DocumentoFiscal>();
			List<DocumentoFiscal>lstFacturasRegistradas =recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(ordenCompra.getId(), FacturaSiniestroService.ESTATUS_FACTURA_REGISTRADA);
			List<DocumentoFiscal>lstFacturasPagadas =recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(ordenCompra.getId(), FacturaSiniestroService.ESTATUS_FACTURA_PAGADA);
			if(null!=lstFacturasPagadas && !lstFacturasPagadas.isEmpty()){
				lstFacturas.addAll(lstFacturasPagadas);		
			}
			if(null!=lstFacturasRegistradas &&!lstFacturasRegistradas.isEmpty()){
				lstFacturas.addAll(lstFacturasRegistradas);	
			}
			/*
			
			//SE CORRIGE LA OBTENCION DE LAS FACTURAS
			lstFacturasRegistradas =this.entidadDao.findByProperties(FacturaSiniestro.class, params);
			params.clear();
			params.put("ordenCompra.id", ordenCompra.getId());
			params.put("estatus", ESTATUS_FACTURA_PAGADA);//Preguntar Estatus
			
			lstFacturasPagadas =this.entidadDao.findByProperties(FacturaSiniestro.class, params);
			if(!lstFacturasPagadas.isEmpty()){
				lstFacturas.addAll(lstFacturasPagadas);		
			}
			if(!lstFacturasRegistradas.isEmpty()){
				lstFacturas.addAll(lstFacturasRegistradas);		
			}*/
			if(lstFacturas.isEmpty()){
				return false; 			
			}
		}
		StringBuilder queryString = new StringBuilder( 
				"SELECT pago from " + OrdenPagoSiniestro.class.getSimpleName()  + " pago ");
				queryString.append(" WHERE  pago.estatus is not null AND    pago.estatus not in ('5') ");
		JpaUtil.addParameter(queryString, modelOrdenPago + "ordenCompra.id", ordenCompra.getId()  , "ordenCompraId", parameters,JpaUtil.EQUAL);
		 List<OrdenPagoSiniestro> listaPago= entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		 
		  if (listaPago.isEmpty()){
			  return true;
		  }else{
			  return false; 
		  }
	}
	
	
	/**
	 *Editar Orden de Pago - Cuando Existe el registro de una orden de pago 
	 * */
	private boolean isEditarOrdenPago(OrdenCompra ordenCompra,OrdenPagoSiniestro ordenPago){
		if(null==ordenPago || null== ordenPago.getId()){
			return false;
		}else{
			if(StringUtil.isEmpty(ordenPago.getEstatus())){
				return false;
			}else {
				if(ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_CANCELADA ) ||  ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_ASOCIADA )  ||  ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_PAGADA )||  ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_PREREGISTRO )   )
					return false;
				else
					return true;
			}
			
		}
	}
	
	/**
	 *Cancelar Orden de Pago -   - Cuando Existe el registro de una orden de pago 
	 * */
	private boolean isCancelarOrdenPago(OrdenCompra ordenCompra,OrdenPagoSiniestro ordenPago){
		if(null==ordenPago || null== ordenPago.getId()){
			return false;
		}else{
			if(StringUtil.isEmpty(ordenPago.getEstatus())){
				return false;
			}//Si la orden de pago ya esta asociada no se puede cancelar. 
			if(ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_ASOCIADA)  || ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_CANCELADA) ||  ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_PAGADA) ){
				return false;
			}
			if(ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_PREREGISTRO)){
				return false;
			}
			return true;
		}
	}
	
	/**
	 *Consultar Orden de Pago - Cuando Existe el registro de una orden de pago
	 * */
	private boolean isConsultarOrdenPago(OrdenCompra ordenCompra,OrdenPagoSiniestro ordenPago){
		if(null==ordenPago || null== ordenPago.getId()){
			return false;
		}else{
			if(StringUtil.isEmpty(ordenPago.getEstatus())){
				return false;
			}
			if(ordenPago.getEstatus().equalsIgnoreCase(PagosSiniestroService.ESTATUS_PREREGISTRO)){
				return false;
			}
			return true;
			
		}
	}

	/**
	 *Registrar Factura - Cuando no Exista Orden de Pago, aplica solo tipo pago proveedor.
	 * */
	private boolean isRegistrarFacturaOrdenPago(OrdenCompra ordenCompra,OrdenPagoSiniestro ordenPago){
		if(!ordenCompra.getTipoPago().equalsIgnoreCase(TIPO_PAGOPROVEEDOR))
			return Boolean.FALSE;
		if(null == ordenPago ){			
			List<DocumentoFiscal>lstFacturas 	=new ArrayList<DocumentoFiscal>();
			lstFacturas.addAll(this.recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(ordenCompra.getId(), FacturaSiniestroService.ESTATUS_FACTURA_PAGADA) );
			lstFacturas.addAll(this.recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(ordenCompra.getId(), FacturaSiniestroService.ESTATUS_FACTURA_REGISTRADA) );
			/*//SE CORRIGE LA OBTENCION DE LAS FACTURAS
			params.put("ordenCompra.id", ordenCompra.getId());
			params.put("estatus", ESTATUS_FACTURA_REGISTRADA);//Preguntar Estatus
			lstFacturas.addAll( this.entidadDao.findByProperties(FacturaSiniestro.class, params) );
			params.clear();
			params.put("ordenCompra.id", ordenCompra.getId());
			params.put("estatus", ESTATUS_FACTURA_PAGADA);//Preguntar Estatus
			lstFacturas.addAll( this.entidadDao.findByProperties(FacturaSiniestro.class, params) );
			 */
			if(lstFacturas!= null && lstFacturas.size()>0){
				return Boolean.FALSE;
			}else{
				return Boolean.TRUE;
			}
		}else{//Si existe orden de pago, pero el estatus es nulo, hay un pre registro incompleto por lo tanto se puede sustituir.
			if(StringUtil.isEmpty(ordenPago.getEstatus())){
				return Boolean.TRUE;
			}
			return Boolean.FALSE;
		}
	}
	
	/**
	 *Devolver Factura - Cuando no Exista una Factura registrada a la orden de compra.  aplica solo tipo pago proveedor.
	 * */
	private boolean isDevolverFacturaOrdenPago(OrdenCompra ordenCompra,OrdenPagoSiniestro ordenPago){
		
		if(!ordenCompra.getTipoPago().equalsIgnoreCase(TIPO_PAGOPROVEEDOR))
			return Boolean.FALSE;
		else{ 			
			List<DocumentoFiscal>lstFacturas 	=new ArrayList<DocumentoFiscal>();
			lstFacturas.addAll( this.recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(ordenCompra.getId(), FacturaSiniestroService.ESTATUS_FACTURA_PAGADA) );
			
			
			/*SE CORRIGE LA OBTENCION DE LAS FACTURAS
			params.put("ordenCompra.id", ordenCompra.getId());
			params.put("estatus", ESTATUS_FACTURA_PAGADA);//Preguntar Estatus
			lstFacturas.addAll( this.entidadDao.findByProperties(FacturaSiniestro.class, params) );
			*/
			if(lstFacturas != null & lstFacturas.size()>0){
				return Boolean.FALSE;
			}else{
				return Boolean.TRUE;
			}
			
			}
		
	}
	
	private boolean isBloquearPago(OrdenCompra ordenCompra,OrdenPagoSiniestro ordenPago){
		if(null==ordenPago){
			return false;
		}
		else {
			if( !StringUtil.isEmpty(ordenPago.getEstatus()) && (!ordenPago.getEstatus().equals(PagosSiniestroService.ESTATUS_ASOCIADA) && !ordenPago.getEstatus().equals(PagosSiniestroService.ESTATUS_CANCELADA))  && !ordenPago.getEstatus().equals(PagosSiniestroService.ESTATUS_PREREGISTRO) ){
				return true;
			}else{
				return false;
			}
		}
		
	}

	public EntidadDao getEntidadDao() {
		return entidadDao;
	}



	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}



	public CotizacionEndosoService getCotizacionEndosoService() {
		return cotizacionEndosoService;
	}



	public void setCotizacionEndosoService(
			CotizacionEndosoService cotizacionEndosoService) {
		this.cotizacionEndosoService = cotizacionEndosoService;
	}



	public CatalogoGrupoValorService getCatalogoGrupoValorService() {
		return catalogoGrupoValorService;
	}



	public void setCatalogoGrupoValorService(
			CatalogoGrupoValorService catalogoGrupoValorService) {
		this.catalogoGrupoValorService = catalogoGrupoValorService;
	}



	public UsuarioService getUsuarioService() {
		return usuarioService;
	}



	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}



	public ListadoService getListadoService() {
		return listadoService;
	}



	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}



	public OrdenCompraService getOrdenCompraService() {
		return ordenCompraService;
	}



	public void setOrdenCompraService(OrdenCompraService ordenCompraService) {
		this.ordenCompraService = ordenCompraService;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoFiscal>  buscarFacturasLiquidacion(Long idProveedor)
	{   
		List<DocumentoFiscal> listaResultados = new ArrayList<DocumentoFiscal>();
		Query query;
		
		String queryStr = "SELECT new mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal( " +
        " factura.id, factura.numeroFactura, factura.estatus, factura.fechaCreacion, " +
        " factura.iva, factura.ivaRetenido, factura.isr, factura.subTotal, " +
        " factura.montoTotal, " +
        " (SELECT FUNC('TO_CHAR',COUNT(c.factura.id)) FROM ConjuntoOrdenCompraNota c WHERE c.factura.id= factura.id group by c.factura.id) , " +
        " factura.nombreAgrupador )" + 
        " FROM DocumentoFiscal factura " + 
        " JOIN factura.ordenesCompra ordenCompra " +
        " WHERE factura.estatus = 'R' " +                           
        " AND  factura.proveedor.id =  " + idProveedor +
        " AND ordenCompra.ordenPago.estatus = '1' " +
        " AND (SELECT COUNT(liquidacion.id) " +
        " FROM LiquidacionSiniestro liquidacion" +
        " JOIN liquidacion.facturas factLiquidacion WHERE factLiquidacion.id = factura.id) = 0 "+ 
        " GROUP BY factura.id, factura.numeroFactura, factura.estatus, factura.fechaCreacion, " +
        " factura.iva, factura.ivaRetenido, factura.isr, factura.subTotal, " +
        " factura.montoTotal,factura.nombreAgrupador" +
        " ORDER BY factura.numeroFactura";

		
		query = entityManager.createQuery(queryStr);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		System.out.println("HOLA:  "+query.toString());
		listaResultados = query.getResultList();
		
		return listaResultados;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FacturaLiquidacionDTO> buscarOrdenesPagoFactura(Long idFactura)
	{
		List<FacturaLiquidacionDTO> listaResultados = new ArrayList<FacturaLiquidacionDTO>();
		Query query;  
		  
		String queryStr = "SELECT new mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO(factura.id, " +
				"factura.numeroFactura, siniestro, ordenPago)" + 
				" FROM OrdenPagoSiniestro ordenPago," +
				" DocumentoFiscal factura," +
				" OrdenCompra ordenCompra " +						
				" JOIN factura.ordenesCompra ocf " +
				" JOIN ordenCompra.reporteCabina reporteCabina " +
				" LEFT JOIN reporteCabina.siniestroCabina siniestro" +
				" WHERE " +
				" factura.id = " + idFactura + " " +
				" AND ordenCompra.id = ocf.id " +
				" AND factura.estatus IN ('R','P') " +
				" AND ordenpago.ordenCompra.id = ordenCompra.id" +
				" AND ordenPago.estatus IN('1','4','2') " +
				" ORDER BY ordenCompra.fechaCreacion ASC ";  
		
		query = entityManager.createQuery(queryStr);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		  
		listaResultados = query.getResultList();
		
		return listaResultados;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenPagoSiniestro> buscarOrdenesPagoIndemnizacion(String nombreBeneficiario)
	{
		List<OrdenPagoSiniestro> listaResultados = new ArrayList<OrdenPagoSiniestro>();
		Query query;      	 
		
		String queryStr = "SELECT new mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro(" +
		" ordenPago.id," +
		" siniestro," +
		" recepcionDocs.formaPago," +
		" CASE WHEN ((SELECT COUNT(indemnizacion.id) from IndemnizacionSiniestro i where i.id = (indemnizacion.id) " +     
				" AND i.estatusIndemnizacion = 'A' " +
				" AND i.ordenCompra.coberturaReporteCabina.claveTipoCalculo IN ('DM','RT') ) > 0)" +
		"			THEN true ELSE false END, " +
		" CASE WHEN (indemnizacion.esPagoDanios = true) THEN 'DA' ELSE 'PT' END)" + 
		" FROM " +
		" OrdenCompra ordenCompra " +
		" LEFT JOIN ordenCompra.indemnizacion indemnizacion" +
		" LEFT JOIN indemnizacion.recepcionDocumentosSiniestro recepcionDocs " +		
		" JOIN ordenCompra.reporteCabina reporteCabina " +
		" JOIN reporteCabina.siniestroCabina siniestro" +
		"      JOIN ordenCompra.ordenPago ordenPago" +
		" WHERE " +
		" 	  ordenCompra.tipoPago = 'PB' " +
		" AND ordenPago.estatus IN('1') " +		
		" AND ordenCompra.nomBeneficiario = '"+ nombreBeneficiario +"' " +
		" AND ordenPago.id NOT IN " +
		"	(SELECT op.id FROM LiquidacionSiniestro liquidacion" +
		"				 JOIN liquidacion.ordenesPago op)" +		
		" ORDER BY ordenPago.id"; 
		
		query = entityManager.createQuery(queryStr);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		listaResultados = query.getResultList();
		
		return listaResultados;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<String> buscarBeneficiario(String beneficiario) {
		
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("estatusLiberada", PagosSiniestroService.ESTATUS_LIBERADA);
		StringBuilder queryString = new StringBuilder("SELECT TRIM(ordenCompra.nomBeneficiario)  ");
		queryString.append(" FROM " +OrdenCompra.class.getSimpleName() +" ordenCompra");
		queryString.append(" JOIN "+ OrdenPagoSiniestro.class.getSimpleName() +" ordenPago ON ordenPago.ordenCompra.id = ordenCompra.id ");
		queryString.append(" WHERE  ordenPago.estatus = :estatusLiberada AND ordenCompra.nomBeneficiario IS NOT NULL");  
		if(!StringUtil.isEmpty(beneficiario)){
			queryString.append(" AND  ordenCompra.nomBeneficiario like '%"+beneficiario+"%'");
		}
		queryString.append(" GROUP BY TRIM(ordenCompra.nomBeneficiario) ");  
		List<String>  lista = this.executeQueryMultipleResult(queryString.toString(), parameters);
		return lista;
	}
	/**Deberá obtener dado un nombre de beneficiario, sus órdenes de pago que no aparezcan en alguna liquidación.
	 * 
	 * @param beneficiario
	 * @return List<OrdenPagoSiniestro>
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenPagoSiniestro> buscarOrdenPagoIndemnizacion(
			String beneficiario) {
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("estatusLiberada", PagosSiniestroService.ESTATUS_LIBERADA);
		StringBuilder queryString = new StringBuilder("SELECT ordenPago  ");
		queryString.append(" FROM " +OrdenCompra.class.getSimpleName() +" ordenCompra");
		queryString.append(" JOIN "+ OrdenPagoSiniestro.class.getSimpleName() +" ordenPago ON ordenPago.ordenCompra.id = ordenCompra.id ");
		queryString.append(" WHERE  ordenPago.estatus = :estatusLiberada ");
		if(!StringUtil.isEmpty(beneficiario)){
			queryString.append(" AND  ordenCompra.nomBeneficiario like '%"+beneficiario+"%'");
		}
		queryString.append(" ORDER BY ordenPago.id ");  
		List<OrdenPagoSiniestro>  lista = this.executeQueryMultipleResult(queryString.toString(), parameters);
		return lista;
	}
}
