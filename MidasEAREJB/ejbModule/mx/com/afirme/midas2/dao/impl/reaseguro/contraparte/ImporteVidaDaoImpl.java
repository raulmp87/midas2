package mx.com.afirme.midas2.dao.impl.reaseguro.contraparte;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.reaseguro.reportes.reportercscontraparte.CargaContraParteDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.reaseguro.contraparte.ImporteVidaDao;
@Stateless
public class ImporteVidaDaoImpl extends EntidadDaoImpl implements ImporteVidaDao {
	
	private static final String TIPO_PENDIENTE = "1";
	private static final String TIPO_POL_FACULTADAS = "2";
	private static final String TIPO_CONT_AUTOMATICOS = "3";
	private static final String TIPO_SISE = "4";
	private static final String TIPO_AJUSTES = "5";
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaSiniestrosPendientes(String poliza, Integer moneda, Integer anio_siniestro, Integer num_siniestro, BigDecimal reserva,
			BigDecimal costo_siniestro,BigDecimal afirme, BigDecimal hannover_life, BigDecimal mafre, String status, Date fcorte, Date inicio_vigencia, String cobertura) {
		
				
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF.SP_CNSF_SINIESTROS_PENDIENTES", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("poliza", poliza);
			storedHelper.estableceParametro("moneda", moneda);
			storedHelper.estableceParametro("anio_siniestro", anio_siniestro);
			storedHelper.estableceParametro("num_siniestro", num_siniestro);
			storedHelper.estableceParametro("reserva", reserva);
			storedHelper.estableceParametro("costo_siniestro", costo_siniestro);
			storedHelper.estableceParametro("afirme", afirme);
			storedHelper.estableceParametro("hananover_life", hannover_life);
			storedHelper.estableceParametro("mafre", mafre);
			storedHelper.estableceParametro("status", status);
			storedHelper.estableceParametro("fCorte", fcorte);
			storedHelper.estableceParametro("inicio_vigencia", inicio_vigencia);
			storedHelper.estableceParametro("cobertura", cobertura);
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_SINIESTROS_PENDIENTES ", Level.SEVERE, e);
		}

	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaPolizasFacultadas(String poliza, Integer moneda, Integer anio_siniestro, Integer num_siniestro, BigDecimal reserva,
			BigDecimal costo_siniestro, BigDecimal afirme, BigDecimal monto_distribucion, String status, String rgre, String reasegurador, Date fCorte) {
			 	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			 
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF.SP_CNSF_POLIZAS_FACULTADAS", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("poliza", poliza);
			storedHelper.estableceParametro("moneda", moneda);
			storedHelper.estableceParametro("anio_siniestro", anio_siniestro);
			storedHelper.estableceParametro("num_siniestro", num_siniestro);
			storedHelper.estableceParametro("reserva", reserva);
			storedHelper.estableceParametro("costo_siniestro", costo_siniestro);
			storedHelper.estableceParametro("afirme", afirme);
			storedHelper.estableceParametro("monto_distribucion", monto_distribucion);
			storedHelper.estableceParametro("status", status);
			storedHelper.estableceParametro("rgre", rgre);
			storedHelper.estableceParametro("reasegurador", reasegurador);
			storedHelper.estableceParametro("fCorte", fCorte);
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_POLIZAS_FACULTADAS", Level.SEVERE, e);
		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaContratosAutomaticos(String contrato, String reaseguradores, String rgre, BigDecimal participacion, Date vigencia_ini, Date vigencia_fin, Date fCorte, String cobertura) {
		
				
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF.SP_CNSF_CONTRATOS_AUTOMATICOS", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("contrato", contrato);
			storedHelper.estableceParametro("reaseguradores", reaseguradores);
			storedHelper.estableceParametro("rgre", rgre);
			storedHelper.estableceParametro("participacion", participacion);
			storedHelper.estableceParametro("vigencia_ini", vigencia_ini);
			storedHelper.estableceParametro("vigencia_fin", vigencia_fin);
			storedHelper.estableceParametro("fCorte", fCorte);
			storedHelper.estableceParametro("cobertura", cobertura);
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_CONTRATOS_AUTOMATICOS", Level.SEVERE, e);
		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaDistribucionSise(Integer siniestro, String reasegurador, String rgre, BigDecimal reserva, String moneda, BigDecimal tipoCambio, Date fCorte) {
		
				
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF.SP_CNSF_DISTRIB_SISE", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("siniestro", siniestro);
			storedHelper.estableceParametro("reasegurador", reasegurador);
			storedHelper.estableceParametro("rgre", rgre);
			storedHelper.estableceParametro("reserva", reserva);
			storedHelper.estableceParametro("moneda", moneda);
			storedHelper.estableceParametro("tipoCambio", tipoCambio);
			storedHelper.estableceParametro("fCorte", fCorte);
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_DISTRIB_SISE", Level.SEVERE, e);
		}

	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaAjustes(Integer siniestro, String reasegurador, String cnsf, String moneda, BigDecimal reserva, String anio, String sistema, BigDecimal tipoCambio, Date fCorte){
		   
		
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF.SP_CNSF_AJUSTES_MANUALES", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("siniestro", siniestro);
			storedHelper.estableceParametro("reasegurador", reasegurador);
			storedHelper.estableceParametro("cnsf", cnsf);
			storedHelper.estableceParametro("moneda", moneda);
			storedHelper.estableceParametro("reserva", reserva);
			storedHelper.estableceParametro("anio", anio);
			storedHelper.estableceParametro("sistema", sistema);
			storedHelper.estableceParametro("tipoCambio", tipoCambio);
			storedHelper.estableceParametro("fCorte", fCorte);
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_AJUSTES_MANUALES", Level.SEVERE, e);
		}

	}
	
	
	/**
	 * Obtiene la cantidad de registros encontrados en las tablas de Cargas.
	 * @param fechaCorte
	 * @param archivo
	 * return Cantidad de registros
	 */
	public int getRegistros(Date fechaCorte, String archivo) {
		int registros = 0;
		String tabla = "";
		try {
			StringBuilder queryString=new StringBuilder("");
			
			if(archivo.equalsIgnoreCase(TIPO_PENDIENTE)){
				queryString.append("SELECT  count(model) FROM SiniestrosPendientes model");
				tabla = "MIDAS.CNSF_SIN_SINIESTROSPENDIENTES";
			}else if(archivo.equalsIgnoreCase(TIPO_POL_FACULTADAS)){
				queryString.append("SELECT  count(model) FROM PolizasFacultadas model");
				tabla = "MIDAS.CNSF_SIN_POL_FACULTADAS";
			}else if(archivo.equalsIgnoreCase(TIPO_CONT_AUTOMATICOS)){
				queryString.append("SELECT  count(model) FROM ContratosAutomaticos model");
				tabla = "MIDAS.CNSF_SIN_CONTRATOS_AUTOMATICOS";
			}else if(archivo.equalsIgnoreCase(TIPO_SISE)){
				queryString.append("SELECT  count(model) FROM DistribucionSise model");
				tabla = "MIDAS.CNSF_SIN_DISTRIB_SISE";
			}else if(archivo.equalsIgnoreCase(TIPO_AJUSTES)){
				queryString.append("SELECT  count(model) FROM AjustesManuales model");
				tabla = "MIDAS.CNSF_SIN_AJUSTES_MANUALES";
			}	
			
			queryString.append(" WHERE model.fCorte = :fecha");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("fecha", fechaCorte, TemporalType.DATE);
			
			registros = ((Long)query.getSingleResult()).intValue();
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla "+tabla, Level.SEVERE, e);
		}
		return registros;

	}
	
	/**
	 * Obtiene el estatus de la carga.
	 * @param fechaCorte
	 * @param reporte
	 * @return estatus candado
	 */
	public int estatusCarga(Date fechaCorte, String reporte){
		int estatus = 0;
		try {			
			
			StringBuilder queryString=new StringBuilder("");
			queryString.append("SELECT  model.candado FROM CargaContraParteDTO model");
			queryString.append(" WHERE model.fechaFin = :fecha");
			queryString.append(" AND model.reporte = :archivo");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("fecha", fechaCorte, TemporalType.DATE);
			query.setParameter("archivo", reporte);
			
			@SuppressWarnings("unchecked")
			List<Integer> resultado = (List<Integer>) query.getResultList();
			if(resultado == null || resultado.isEmpty()){
				estatus = 0;
			}else{
				estatus = resultado.get(0);
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla MIDAS.CNSF_SIN_ESTATUS_CARGA", Level.SEVERE, e);
		}
		return estatus;
	}
	
	/**
	 * Elimina registros en la tabla de Carga.
	 * @param fechaCorte
	 * @param reporte
	 * @param fecha
	 * @param negocio
	 */
	public void borraRegistros(Date fechaCorte, String reporte){
		String tabla = "";
		
		try {
			StringBuilder queryString=new StringBuilder("");
			
			if(reporte.equalsIgnoreCase(TIPO_PENDIENTE)){
				queryString.append("DELETE FROM SiniestrosPendientes model");
				tabla = "MIDAS.CNSF_SIN_SINIESTROSPENDIENTES";
			}else if(reporte.equalsIgnoreCase(TIPO_POL_FACULTADAS)){
				queryString.append("DELETE FROM PolizasFacultadas model");
				tabla = "MIDAS.CNSF_SIN_POL_FACULTADAS";
			}else if(reporte.equalsIgnoreCase(TIPO_CONT_AUTOMATICOS)){
				queryString.append("DELETE FROM ContratosAutomaticos model");
				tabla = "MIDAS.CNSF_SIN_CONTRATOS_AUTOMATICOS";
			}else if(reporte.equalsIgnoreCase(TIPO_SISE)){
				queryString.append("DELETE FROM DistribucionSise model");
				tabla = "MIDAS.CNSF_SIN_DISTRIB_SISE";
			}else if(reporte.equalsIgnoreCase(TIPO_AJUSTES)){
				queryString.append("DELETE FROM AjustesManuales model");
				tabla = "MIDAS.CNSF_SIN_AJUSTES_MANUALES";
			}
			
			queryString.append(" WHERE model.fCorte = :fecha");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("fecha", fechaCorte, TemporalType.DATE);
			query.executeUpdate();
			entityManager.flush();
			
			LogDeMidasEJB3.log("Elimando registros de la tabla "+tabla+" :fecha " + fechaCorte, Level.INFO, null);
						
						
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla "+tabla, Level.SEVERE, e);
		}
	}
	
	/**
	 * Inserta registros en la tabla de EstatusCarga.
	 * @param cargaDTO
	 */
	public void insertaEstatusCarga(CargaContraParteDTO cargaDTO){
		LogDeMidasEJB3.log(
				"Insertando instancia de cargaDTO.", Level.INFO, null);
		try {			
			
			entityManager.persist(cargaDTO);
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de guadar el dto "+cargaDTO.getReporte(), Level.SEVERE, e);
		}
		
	}
	
	/**
	 * Actualiza registros en la tabla de EstatusCarga.
	 * @param cargaDTO
	 */
	public void actualizaEstatusCarga(CargaContraParteDTO cargaDTO){
		LogDeMidasEJB3.log(
				"Actualizando registros de la tabla MIDAS.CNSF_SIN_ESTATUS_CARGA"
						 + cargaDTO.getFechaFin(), Level.INFO, null);
		try {
			StringBuilder queryString=new StringBuilder("");
			queryString.append("UPDATE CargaContraParteDTO model");
			queryString.append(" SET model.usuario = :usuario,");
			queryString.append(" model.fechaCarga = :fechaCarga,");
			queryString.append(" model.idArchivo = :idControlArchivo");
			queryString.append(" WHERE model.fechaFin = :fechaCorte");
			queryString.append(" AND model.reporte = :negocio");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("usuario", cargaDTO.getUsuario());
			query.setParameter("fechaCarga", cargaDTO.getFechaCarga(), TemporalType.TIMESTAMP);
			query.setParameter("fechaCorte", cargaDTO.getFechaFin(), TemporalType.DATE);
			query.setParameter("negocio", cargaDTO.getReporte());
			query.setParameter("idControlArchivo", cargaDTO.getIdArchivo());
			query.executeUpdate();
			entityManager.flush();					
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de actualizar el estatus de la carga. ", Level.SEVERE, e);
		}
		
	}
	
}

