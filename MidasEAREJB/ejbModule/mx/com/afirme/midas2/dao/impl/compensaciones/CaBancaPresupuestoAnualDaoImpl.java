package mx.com.afirme.midas2.dao.impl.compensaciones;


import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaPresupuestoAnualDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPresupuestoAnual;

import org.apache.log4j.Logger;


@Stateless
public class CaBancaPresupuestoAnualDaoImpl implements CaBancaPresupuestoAnualDao {

	public static final String ID = "id";
	public static final String CA_CONFIGURACION_BANCA = "cabancaconfiguracionId";
	public static final String RAMOSEYCOS_ID = "ramoseycosId";
	public static final String MES = "mes";
	public static final String MONTO = "monto";
 

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = Logger.getLogger(CaCompensacionDaoImpl.class);

	public void save(CaBancaPresupuestoAnual entity) {
		LOG.info(">> save()");
		try {
			entityManager.persist(entity);
			LOG.info("<< save()");
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}


	public void delete(CaBancaPresupuestoAnual entity) {
		LOG.info(">> delete()");
		try {
			entity = entityManager.getReference(CaBancaPresupuestoAnual.class,entity.getId());
			entityManager.remove(entity);
			LOG.info("<< delete()");
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}

	
	public CaBancaPresupuestoAnual update(CaBancaPresupuestoAnual entity) {
		LOG.info(">> update()");
		try {
			CaBancaPresupuestoAnual result = entityManager.merge(entity);
			LOG.info("<< update()");
			return result;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}

	public CaBancaPresupuestoAnual findById(Long id) {
		LOG.info(">> findById()");
		try {
			CaBancaPresupuestoAnual instance = entityManager.find(CaBancaPresupuestoAnual.class, id);
			LOG.info("<< findById()");
			return instance;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}


	@SuppressWarnings("unchecked")
	public List<CaBancaPresupuestoAnual> findByProperty(String propertyName,final Object value) {
		LOG.info(">> findByProperty()");
		try {
			final String queryString = "select model from CaBancaPresupuestoAnual model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOG.info("<< findByProperty()");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}


	@SuppressWarnings("unchecked")
	public List<CaBancaPresupuestoAnual> findAll() {
		LOG.info(">> findAll()");
		try {
			final String queryString = "select model from CaBancaPresupuestoAnual model";
			Query query = entityManager.createQuery(queryString);
			LOG.info("<< findAll()");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
	}
	
	public int deleteRecordsByProperty(String propertyName, Object value){
		LOG.info(">> deleteRecordsByProperty()");
		int deleteCount = 0;
		try {
			StringBuilder queryString = new StringBuilder("DELETE FROM CaBancaPresupuestoAnual model");
			queryString.append(" WHERE model.").append(propertyName).append(" = ").append(" :propertyValue ");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("propertyValue", value);
			
			deleteCount = query.executeUpdate();
			
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			throw re;
		}
		LOG.info("<< deleteRecordsByProperty()");
		return deleteCount;
	}

}