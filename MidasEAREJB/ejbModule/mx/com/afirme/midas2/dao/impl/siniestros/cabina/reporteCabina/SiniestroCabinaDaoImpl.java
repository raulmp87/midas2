package mx.com.afirme.midas2.dao.impl.siniestros.cabina.reporteCabina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.SiniestroCabinaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
@Stateless
public class SiniestroCabinaDaoImpl extends JpaDao<Long, CatValorFijo> implements SiniestroCabinaDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<CatValorFijo> obtenerTerminosAjuste(String codigoTipoSiniestro,
			String codigoResponsabilidad) {
		StringBuilder queryString = new StringBuilder("SELECT model FROM CatValorFijo model, TerminoAjusteConfiguracion config, CatGrupoFijo grupo ");
		queryString.append("WHERE config.codigoTipoSiniestro = :codigoTipoSiniestro AND config.codigoResponsabilidad = :codigoResponsabilidad AND config.codigoTerminoAjuste = model.codigo AND config.activo = :activo ");
		queryString.append("AND grupo.codigo = :tipoGrupo AND grupo.id = model.grupo.id AND model.activo = :activo ORDER BY model.orden");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("codigoTipoSiniestro", codigoTipoSiniestro);
		query.setParameter("codigoResponsabilidad", codigoResponsabilidad);
		query.setParameter("activo", Boolean.TRUE);
		query.setParameter("tipoGrupo", CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);		
		return query.getResultList();		
	}

	@Override
	public String obtenerConsecutivoReporte(SiniestroCabina siniestroCabina) {
		StringBuilder queryString = new StringBuilder("SELECT COALESCE(MAX(model.consecutivoReporte),0) + 1 FROM ");
		queryString.append(SiniestroCabina.class.getSimpleName());
		queryString.append(" model WHERE model.claveOficina = :claveOficina and model.anioReporte = :anioActual");
				//TypedQuery<SiniestroCabina> query = entityManager.createQuery(queryString.toString(), SiniestroCabina.class);	
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("claveOficina", siniestroCabina.getClaveOficina());
		query.setParameter("anioActual", siniestroCabina.getAnioReporte());
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);
		BigDecimal consecutivo = (BigDecimal)query.getSingleResult();		
		return consecutivo.toString();
		
		
	}
}
