package mx.com.afirme.midas2.dao.impl.condicionesGenerales;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.condicionesGenerales.CgAgenteDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgente;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgenteView;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.eclipse.persistence.exceptions.DatabaseException;

@Stateless
public class CgAgenteDaoImpl extends JpaDao<Long, CgAgente> implements CgAgenteDao {
	
	private ValorCatalogoAgentesService catalogoService;
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}


	@Override
	public CgAgenteView findCgAgenteViewById( Long id ) throws SQLException,
			Exception {
		
		CgAgenteView cgAgenteView = new CgAgenteView();
		CgAgente cgAgente = new CgAgente();
		Agente agente = new Agente();
		agente.setId( id );
		cgAgente.setAgente( agente );
		
		List<CgAgenteView> lista = this.findByFilter( cgAgente );
		if ( lista!=null && lista.size() > 0 ){
			cgAgenteView = lista.get( 0 );
		}
		return cgAgenteView;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CgAgenteView> findByFilter( CgAgente cgAgente ) throws SQLException,
	Exception{
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<CgAgenteView> lista=new ArrayList<CgAgenteView>();
		Persona persona=(isNotNull(cgAgente.getAgente()))?cgAgente.getAgente().getPersona():null;
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select distinct id, ");
		queryString.append(" dias_entrega,");
		queryString.append(" idAgente,");
		queryString.append(" promotoria,");
		queryString.append(" idPromotoria,");
		queryString.append(" idPersona,");
		queryString.append(" nombreCompleto,");
		queryString.append(" codigoRfc,");
		queryString.append(" tipoCedulaAgente,");
		queryString.append(" ejecutivo,");
		queryString.append(" tipoSituacion, ");
		queryString.append(" gerencia ");
		queryString.append(" from (");
		queryString.append(" SELECT /*+ INDEX(ejecutivo TOEJECUTIVO_PK) INDEX(promotoria TOPROMOTORIA_PK) INDEX(agente IDX_TOAGT_CALCBONOEXCLUSION)*/ distinct rownum as renglon,"); 
		queryString.append(" cgAgente.toagente_id as id,");
		queryString.append(" cgAgente.dias_entrega as dias_entrega, ");
		queryString.append(" agente.idAgente as idAgente,");
		queryString.append(" promotoria.descripcion as promotoria,");
		queryString.append(" agente.idPromotoria as idPromotoria,");
		queryString.append(" agente.idPersona,");
		queryString.append(" PERSONA.NOMBRECOMPLETO as nombreCompleto,");
		queryString.append(" PERSONA.CODIGORFC as codigoRfc,");
		queryString.append(" tipoCedula.valor as tipoCedulaAgente ,");
		queryString.append(" personaEjecutivo.nombreCompleto as ejecutivo,");
		queryString.append(" tipoSituacion.valor as tipoSituacion, ");
		queryString.append(" gerencia.descripcion as gerencia ");
		queryString.append(" from MIDAS.tocgAgente cgAgente ");
		queryString.append(" inner join MIDAS.toAgente agente on(agente.id=cgAgente.toagente_id) ");
		queryString.append(" inner join MIDAS.toPromotoria promotoria on(promotoria.id=agente.idPromotoria) ");
		queryString.append(" inner join MIDAS.VW_PERSONA persona on(persona.idpersona=agente.idPersona) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoCedula on(tipoCedula.id=agente.idTipoCedulaAgente and tipoCedula.grupoCatalogoAgentes_id=(select id from MIDAS.tcGrupoCatalogoAgentes where descripcion='Tipos de Cedula de Agente'))");
		queryString.append(" inner join MIDAS.toEjecutivo ejecutivo on(ejecutivo.id=promotoria.ejecutivo_id) ");
		queryString.append(" inner join MIDAS.toGerencia gerencia on(gerencia.id=ejecutivo.gerencia_id) ");
		queryString.append(" inner join MIDAS.VW_PERSONA personaEjecutivo on(personaEjecutivo.idPersona=ejecutivo.idPersona)");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoSituacion on(tipoSituacion.id=agente.idSituacionAgente and tipoSituacion.grupoCatalogoAgentes_id=(select id from MIDAS.tcGrupoCatalogoAgentes where descripcion='Estatus de Agente (Situacion)'))");
		queryString.append(" where ");
		
		int index=1;
		if(isNotNull(cgAgente.getAgente())){
			
			addCondition(queryString, " cgAgente.estatus=? ");
			params.put(index, 1L);
			index++;
			
			if(isNotNull(cgAgente.getAgente().getCodigoUsuario())){
				addCondition(queryString, " agente.codigoUsuario=? ");
				params.put(index, cgAgente.getAgente().getCodigoUsuario());
				index++;
			}
			
			if(isNotNull(cgAgente.getAgente().getId())){
				addCondition(queryString, " agente.id=? ");
				params.put(index, cgAgente.getAgente().getId());
				index++;
			}else if(isNotNull(cgAgente.getAgente().getIdAgente())){
				addCondition(queryString, " agente.idAgente=? ");
				params.put(index, cgAgente.getAgente().getIdAgente());
				index++;
			}
			if(isNotNull(persona)){
					if(isValid(persona.getNombreCompleto())){
						addCondition(queryString, " UPPER(persona.nombreCompleto) like UPPER(?)");
						params.put(index,  "%"+persona.getNombreCompleto()+"%");
						index++;
					}

			}

			Promotoria promotoria=cgAgente.getAgente().getPromotoria();
			if(isNotNull(promotoria)){
					if(isNotNull(promotoria) && isNotNull(promotoria.getId())){
						addCondition(queryString, " promotoria.id=? ");
						params.put(index, promotoria.getId());
						index++;
					}
					Ejecutivo ejecutivo=promotoria.getEjecutivo();
					if(isNotNull(ejecutivo)){
						if(isNotNull(ejecutivo.getId())){
							addCondition(queryString, " ejecutivo.id=? ");
							params.put(index, ejecutivo.getId());
							index++;
						}
						Gerencia gerencia= ejecutivo.getGerencia();
						if(isNotNull(gerencia) && isNotNull(gerencia.getId())){
							addCondition(queryString, " gerencia.id=? ");
							params.put(index, gerencia.getId());
							index++;
						}
					}				
			}
			ValorCatalogoAgentes tipoSituacion=cgAgente.getAgente().getTipoSituacion();
			if(isNotNull(tipoSituacion) && isNotNull(tipoSituacion.getId())){
				addCondition(queryString, " tipoSituacion.id=? ");
				params.put(index, tipoSituacion.getId());
				index++;
			}			
			if(params.isEmpty()){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			else
			{   
			    ValorCatalogoAgentes tipoAgentePromotor=null;
				try {
					tipoAgentePromotor=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "AGENTE PROMOTOR");
					if(isNotNull(tipoAgentePromotor) && isNotNull(tipoAgentePromotor.getId())){
						addCondition(queryString," NOT(agente.idTipoAgente in(?) and agente.idAgente is null)");
						params.put(index, tipoAgentePromotor.getId());
						index++;
					}
				} catch (Exception e) {
					e.printStackTrace(); 
				}			
			}
			
			String queryReal=getQueryString(queryString);
			queryString.delete(0,queryString.length());
			queryString.append(queryReal);
			queryString.append(")");
			String finalQuery=getQueryString(queryString)+" order by id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,CgAgenteView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			lista=(List<CgAgenteView>)query.getResultList();
		}
		return lista;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}

	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	@Override
	public CgAgente updateNew( CgAgente entity ) throws DatabaseException, Exception{
		
		CgAgente cgAgente = entityManager.merge(entity);	
		entityManager.flush();
		return cgAgente;
	}
	
}
