package mx.com.afirme.midas2.dao.impl.siniestros.cabina;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.cabina.AjustadorEstatusDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService.ReporteEstatusDTO;
import mx.com.afirme.midas2.util.JpaUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.LocalDate;

@Stateless
public class AjustadorEstatusDaoImpl extends JpaDao<Long, AjustadorEstatus>   implements  AjustadorEstatusDao{
	
	
	@EJB
	EntidadDao entidadDao;
	
	@Override
	public AjustadorEstatus obtenerUltimoAjustadorEstatus(Long idAjustador) {
		if( idAjustador != null && idAjustador != 0 ){	
			Date fechaMax = obtenerMaxFechaAjustador(idAjustador);

			if(fechaMax != null){
				StringBuilder queryString = new StringBuilder("SELECT model FROM " +  
						AjustadorEstatus.class.getSimpleName() + " model");
				queryString.append(" WHERE model.ajustador.id = :idAjustador AND model.fecha = :fechaMax");								
				TypedQuery<AjustadorEstatus> query = entityManager.createQuery(
						queryString.toString(), AjustadorEstatus.class);
				query.setParameter("idAjustador", idAjustador.longValue());
				query.setParameter("fechaMax", fechaMax);
				
				return query.getResultList().get(0);
			}
		}
		return null;
	}
	
	
	/**
	 * Obtener ultimo estatus reportado para un ajustador en la fecha dada. Si la fecha es nula retornara el ultimo estatus activo
	 * @param idAjustador
	 * @param fecha
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public AjustadorEstatus obtenerUltimoAjustadorEstatus(Long idAjustador, Date fecha) {
		if( idAjustador != null && idAjustador != 0 ){			
			Map<String, Object> params = new HashMap<String, Object>();
			StringBuilder queryString = new StringBuilder("SELECT model FROM " +  
						AjustadorEstatus.class.getSimpleName() + " model WHERE model.estatus = 1 ");				
			JpaUtil.addParameter(queryString, "model.ajustador.id", idAjustador, "idAjustador", params);
			if(fecha != null){
				JpaUtil.addDateParameter(queryString, "model.fecha", fecha, "desde", params, JpaUtil.GREATEROREQUAL);
			}
			queryString.append(" ORDER BY model.fecha desc");
			List<AjustadorEstatus> estatus = entidadDao.executeQueryMultipleResult(queryString.toString(), params);
			if(estatus != null && estatus.size() > 0){
				return estatus.get(0);
			}			
		}
		return null;
	}
	
	/**
	 * Obtener ultimo estatus reportado para cada ajustador de una oficina en particular que haya generado un reporte en la fecha seleccionada.
	 * Si la fecha es nula se toma el dia actual.
	 * @param oficinaId
	 * @param fecha
	 * @return
	 */
	@Override
	public List<AjustadorEstatus> obtenerEstatusAjustadores(Long oficinaId, Date fecha) {		
		if(fecha == null){
			LocalDate today = LocalDate.now();
			fecha = today.toDate();
		}
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder queryString = new StringBuilder("SELECT model FROM " +  
					AjustadorEstatus.class.getSimpleName() + " model WHERE model.ajustador.servicioSiniestroId = 1 ");	
		JpaUtil.addParameter(queryString, "model.ajustador.oficina.id", oficinaId, "oficinaId", params);
		JpaUtil.addParameter(queryString, "model.estatus",Integer.valueOf(1), "estatus", params);
		JpaUtil.addDateParameter(queryString, "model.fecha", fecha, "desde", params, JpaUtil.GREATEROREQUAL);
		queryString.append(" AND model.fecha in (SELECT max(m.fecha) FROM ").append(
				AjustadorEstatus.class.getSimpleName()).append(" m WHERE model.ajustador.id = m.ajustador.id) ");
		queryString.append(" ORDER BY model.ajustador.personaMidas.nombre ");		
		TypedQuery<AjustadorEstatus> query = entityManager.createQuery(
				queryString.toString(), AjustadorEstatus.class);
		query.setParameter("oficinaId", params.get("oficinaId"));
		query.setParameter("estatus", params.get("estatus"));
		query.setParameter("desde", params.get("desde"));
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();		
	}
	
	@Override
	public List<AjustadorEstatus> obtenerHistoricoPorAjustador(Long idAjustador, Date desde){
		if(desde == null || idAjustador == null){
			throw new RuntimeException("Se requiere un ajustador y una fecha de inicio para obtener el historico");
		}
		Map<String, Object> params = new HashMap<String, Object>();			
		StringBuilder queryString = new StringBuilder("Select new mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus(");
		queryString.append("model.id, model.fecha, model.estatus, model.ajustador, model.coordenadas) FROM ");
		queryString.append("mx.com.afirme.midas2.domain.siniestros.cabina.AjustadorEstatus model ");
		JpaUtil.addParameter(queryString, "model.ajustador.id", idAjustador, "idAjustador", params);
		JpaUtil.addDateParameter(queryString, "model.fecha", desde, "desde", params, JpaUtil.GREATEROREQUAL);		
		TypedQuery<AjustadorEstatus> query = entityManager.createQuery(
				queryString.toString(), AjustadorEstatus.class);
		query.setParameter("idAjustador", params.get("idAjustador"));
		query.setParameter("desde", params.get("desde"));
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	/**
	 * Metodo para obtener la fecha del ultimo registro guardado en la tabla de TCAJUSTADORESTATUS
	 * para un ajustador determinado
	 * Si no se ha registrado nunca la posicion del ajustador en esta tabla, este metodo devuelve null
	 * @param idAjustador
	 * @return
	 */	
	private Date obtenerMaxFechaAjustador(Long idAjustador) {
		StringBuilder queryString = new StringBuilder(
				"SELECT MAX(model.fecha) FROM " + AjustadorEstatus.class.getSimpleName() + " model ");
		queryString.append(" WHERE model.ajustador.id = :idAjustador ");
		queryString.append(" GROUP BY model.ajustador.id");

		TypedQuery<Date> query = entityManager.createQuery(
				queryString.toString(), Date.class);
		query.setParameter("idAjustador", idAjustador.longValue());

		List<Date> result = query.getResultList();
		if(result != null && result.size() > 0){
			return query.getResultList().get(0);
		}
		return null;
	}

	/**
	 * listar reportes en proceso que tienen coordenadas definidas y que no tiene una cita programada
	 * @param idOficina
	 * @return
	 */
	@Override
	public List<ReporteEstatusDTO> obtenerReportesSinTerminacion(Long idOficina) {
		Map<String, Object> params = new HashMap<String, Object>();			
		StringBuilder queryString = new StringBuilder("Select new ").append(ReporteEstatusDTO.class.getCanonicalName());
		queryString.append("(model.id, model.claveOficina, model.consecutivoReporte, model.anioReporte, model.fechaHoraOcurrido, ")
		.append("model.fechaHoraAsignacion, model.fechaHoraContacto, model.fechaHoraTerminacion, model.fechaHoraReporte, ")
		.append("model.nombreReporta, model.poliza, model.lugarAtencion) FROM ")
		.append(ReporteCabina.class.getCanonicalName())
		.append(" model WHERE model.lugarAtencion.id is not null AND model.lugarAtencion.coordenadas.latitud is not null AND model.fechaHoraTerminacion is null");		
		JpaUtil.addParameter(queryString, "model.oficina.id", idOficina, "idOficina", params);				
		TypedQuery<ReporteEstatusDTO> query = entityManager.createQuery(
				queryString.toString(), ReporteEstatusDTO.class);
		query.setParameter("idOficina", params.get("idOficina"));		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	/**
	 * Obtener un listado de reportes que el ajustador tiene asignados. 
	 * Todos los parametros son opcionales pudiendo ser <code>null</code>. Aquellos que se envien se incluiran en la busqueda 
	 * @param ajustadorId
	 * @param reporteId
	 * @param oficinaId
	 * @return resumen de los reportes que cumplen con los parametros, que estan en tramite y que no tienen fecha de terminacion
	 */
	@Override
	public List<ReporteEstatusDTO> obtenerReportesAsignados(Long ajustadorId, Long reporteId, Long oficinaId){
		StringBuilder queryString = new StringBuilder("Select new  ").append(ReporteEstatusDTO.class.getCanonicalName());
		queryString.append("(model.id, model.claveOficina, model.consecutivoReporte, model.anioReporte, model.fechaOcurrido, ")
			.append("model.fechaHoraAsignacion, model.fechaHoraContacto, model.fechaHoraTerminacion, model.fechaHoraReporte, ")
			.append("model.personaReporta, model.poliza, model.lugarAtencion, model.ajustador) FROM ")
			.append(ReporteCabina.class.getCanonicalName())
			.append(" model WHERE model.fechaHoraTerminacion is null AND model.estatus = 1 ");
		if(ajustadorId != null){
			queryString.append("AND model.ajustador.id = :ajustadorId ");
		}
		if(oficinaId != null){
			queryString.append("AND model.oficina.id = :oficinaId ");
		}
		if(reporteId != null){
			queryString.append("AND model.id <> :reporteId ");
		}
		queryString.append(" ORDER BY model.fechaHoraReporte ");
		TypedQuery<ReporteEstatusDTO> query = entityManager.createQuery(
				queryString.toString(), ReporteEstatusDTO.class);			
		if(ajustadorId != null){
			query.setParameter("ajustadorId", ajustadorId);
		}
		if(oficinaId != null){
			query.setParameter("oficinaId", oficinaId);
		}
		if(reporteId != null){
			query.setParameter("reporteId", reporteId);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}


	@Override
	public int eliminarHistoricoAjustadores(Date fecha) {
		StringBuilder queryString = new StringBuilder("DELETE FROM ").append(AjustadorEstatus.class.getCanonicalName());
		queryString.append(" model WHERE model.ajustador.servicioSiniestroId = 1 AND model.fecha < :fecha");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("fecha", fecha);		
		int count = query.executeUpdate(); //como se esta utilizando batch-writing en eclipselink siempre retornara 1
		return count;
	}
}
