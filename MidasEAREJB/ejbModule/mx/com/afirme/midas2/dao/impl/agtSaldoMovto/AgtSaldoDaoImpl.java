package mx.com.afirme.midas2.dao.impl.agtSaldoMovto;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.agtSaldoMovto.AgtSaldoDao;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.impl.prestamos.PagarePrestamoAnticipoDaoImpl;
import mx.com.afirme.midas2.domain.agtSaldoMovto.AgtSaldo;
import mx.com.afirme.midas2.dto.agtSaldoMovto.AgtSaldoView;
import mx.com.afirme.midas2.util.MidasException;
@Stateless
public class AgtSaldoDaoImpl extends EntidadDaoImpl implements AgtSaldoDao {

	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> arg0, String arg1, Map<String, Object> arg2,
			Map<String, Object> arg3, StringBuilder arg4, StringBuilder arg5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> arg0, Map<String, Object> arg1, Map<String, Object> arg2,
			StringBuilder arg3, StringBuilder arg4) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<AgtSaldo> findByFilter(AgtSaldo agtSaldo) throws Exception {
		List<AgtSaldo>lista = new ArrayList<AgtSaldo>();
		Map<Integer,Object>params = new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT * FROM MIDAS.toAgtSaldo ");
		Integer index =1;
		if(agtSaldo.getIdAgente().getIdAgente()!=null){
			addCondition(queryString, " IDAGENTE=? ");
			params.put(index, agtSaldo.getIdAgente());
			index++;
		}
		if(agtSaldo.getIdMoneda()!=null){
			addCondition(queryString, " IDMONEDA=? ");
			params.put(index, agtSaldo.getIdMoneda());
			index++;
		}
		if(agtSaldo.getAnio()!=null){
			String anioMes_1 =agtSaldo.getAnio().toString()+"01";
			String anioMes_2 =agtSaldo.getAnio().toString()+"12";
			
			addCondition(queryString, " ANIOMES>=? ");
			params.put(index, convertirStringToLong(anioMes_1));
			index++;
			
			addCondition(queryString, " ANIOMES<=? ");
			params.put(index, convertirStringToLong(anioMes_2));
			index++;
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString), AgtSaldo.class);
		lista = query.getResultList();
		return lista;
	}
	
	private Long convertirStringToLong(String obj){
		Long objLong=(long) Integer.parseInt(obj);
		return objLong;
	}
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	@Override
	public  List<AgtSaldoView> findByFilterview(AgtSaldo agtSaldo) throws Exception{
		
		StringBuilder queryString= new StringBuilder();
		Map<Integer,Object>params = new HashMap<Integer, Object>();
		List<AgtSaldoView>lista = new ArrayList<AgtSaldoView>();
		queryString
				.append(" SELECT IDMONEDA as idMoneda, ANIOMES as anioMes, IDAGENTE as idAgente, ROWNUM as id, To_Char(FECHAINICIOPERIODO,'Month','NLS_DATE_LANGUAGE = SPANISH') as mes, FECHAINICIOPERIODO as fechaInicioPeriodo, " +
						" FECHAFINPERIODO as fechaFinPeriodo, IMPORTESALDOINICIAL saldoInicial, IMPORTESALDOFINAL as saldoFinal, " +
						" cat.VALOR as situacionSaldo,FECHASITUACION as fechaSituacion "+  
                        " FROM MIDAS.toAgtSaldo agtsdo " +
                        " inner join MIDAS.toValorCatalogoAgentes cat on cat.ID = agtsdo.IDSITUACIONSALDO ");
		
		Integer index =1;
		if(agtSaldo.getIdAgente().getIdAgente()!=null){
			addCondition(queryString, " IDAGENTE=? ");
			params.put(index, agtSaldo.getIdAgente().getIdAgente());
			index++;
		}
		if(agtSaldo.getIdMoneda()!=null && agtSaldo.getIdMoneda().getId()!=null){
			addCondition(queryString, " IDMONEDA=? ");
			params.put(index, agtSaldo.getIdMoneda().getId());
			index++;
		}
		if(agtSaldo.getAnio()!=null){
			String anioMes_1 =agtSaldo.getAnio().toString()+"01";
			String anioMes_2 =agtSaldo.getAnio().toString()+"12";
			
			addCondition(queryString, " ANIOMES>=? ");
			params.put(index, convertirStringToLong(anioMes_1));
			index++;
			
			addCondition(queryString, " ANIOMES<=? ");
			params.put(index, convertirStringToLong(anioMes_2));
			index++;
		}
		if(agtSaldo.getAnioMes()!=null){
			addCondition(queryString, " ANIOMES=? ");
			params.put(index,agtSaldo.getAnioMes());
			index++;
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString), AgtSaldoView.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		lista = query.getResultList();
		return lista;
	}

	
	@Override
	public AgtSaldoView obtenerMovimientoSaldoPorAnioMesYAgente(AgtSaldo agtSaldo) throws MidasException {
		if(agtSaldo==null){
			onError("No existen datos de busqueda");
		}
		List<AgtSaldoView> list = new ArrayList<AgtSaldoView>();
		AgtSaldoView obj = new AgtSaldoView();
		try {
			list = getMovtoSaldoPorAnioMesYIdAgente(agtSaldo);
//			list=findByFilterview(agtSaldo);
			
			if(list!=null && !list.isEmpty()){
				obj = list.get(0);
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return obj;
	}
	
	private List<AgtSaldoView> getMovtoSaldoPorAnioMesYIdAgente(AgtSaldo objAgtsaldo){
		StringBuilder queryString= new StringBuilder();
		Map<Integer,Object>params = new HashMap<Integer, Object>();
		List<AgtSaldoView>lista = new ArrayList<AgtSaldoView>();
		queryString
				.append(" SELECT ROWNUM as id, p.NOMBRECOMPLETO as nombreCompleto, agtsdo.IDAGENTE as idAgente, sit.VALOR as situacionSaldo,"+
					       " sitAgt.valor as situacionAgente, motEstatusAgt.valor as motivoEstatus, agt.FECHAALTA as fechaSituacion, "+
					       " agtsdo.IDMONEDA as idMoneda, agtsdo.ANIOMES as anioMes, To_Char(FECHAINICIOPERIODO,'FMMONTH') as mes, "+
					       " agtsdo.FECHAINICIOPERIODO as fechaInicioPeriodo, agtsdo.FECHAFINPERIODO as fechaFinPeriodo, "+
					       " agtsdo.IMPORTESALDOINICIAL as saldoInicial ,agtsdo.IMPORTESALDOFINAL as saldoFinal "+
					       " FROM MIDAS.toAgtSaldo agtsdo  "+
					       " inner join MIDAS.toValorCatalogoAgentes sit on sit.ID = agtsdo.IDSITUACIONSALDO "+
					       " inner join MIDAS.toAgente agt on agt.IDAGENTE = agtsdo.IDAGENTE "+
					       " inner join MIDAS.vw_persona p on p.IDPERSONA = agt.IDPERSONA  "+
					       " inner join MIDAS.toValorCatalogoAgentes sitAgt on sitAgt.ID = agt.IDSITUACIONAGENTE "+
					       " inner join MIDAS.toValorCatalogoAgentes motEstatusAgt on motEstatusAgt.ID = agt.IDMOTIVOESTATUSAGENTE ");
		Integer index =1;
		if(objAgtsaldo.getIdAgente().getIdAgente()!=null){
			addCondition(queryString, " IDAGENTE=? ");
			params.put(index, objAgtsaldo.getIdAgente().getIdAgente());
			index++;
		}
		if(objAgtsaldo.getAnioMes()!=null){
			addCondition(queryString, " ANIOMES=? ");
			params.put(index,objAgtsaldo.getAnioMes());
			index++;
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString), AgtSaldoView.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		lista = query.getResultList();
		return lista;
	}
	
	public Long actualizarSaldosMesAbierto(Long idAgente, Long anio) throws Exception{
		Long periodoActualizado = 0L;
		if (isNotNull(idAgente) && isNotNull(anio)){
			
			StoredProcedureHelper storedHelper = null;
			String sp="MIDAS.PKGCALCULOS_AGENTES.stp_UpdateSaldosMesAbierto";
			
			try {
				LogDeMidasInterfaz.log("Entrando a actualizarSaldosMesAbierto..." + this, Level.INFO, null);
								
				storedHelper = new StoredProcedureHelper(sp);
				storedHelper.estableceParametro("pAnio",anio);
				storedHelper.estableceParametro("pId_Agente",idAgente);
					
				periodoActualizado =  new Long(storedHelper.ejecutaActualizar());
				
				LogDeMidasInterfaz.log("Saliendo de actualizarSaldosMesAbierto, periodo actualizado - "+periodoActualizado+"..." + this, Level.INFO, null);
				
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,PagarePrestamoAnticipoDaoImpl.class, codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de actualizarSaldosMesAbierto..." + this, Level.WARNING, e);
				onError(e);
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en actualizarSaldosMesAbierto..." + this, Level.WARNING, e);
				onError(e);
			}
		}
		
		return periodoActualizado;
	}
}
