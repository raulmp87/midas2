package mx.com.afirme.midas2.dao.impl.fortimax;
import static mx.com.afirme.midas2.utils.CommonUtils.addCondition;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.fortimax.DocumentoEntidadFortimaxDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CarpetaAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.recepcionDocumentos.RecepcionDocumentosService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class DocumentoEntidadFortimaxDaoImpl extends EntidadDaoImpl implements DocumentoEntidadFortimaxDao{
	private EntidadService entidadService;
	private FortimaxService fortimaxService;
	private ValorCatalogoAgentesService catalogoService;
	private RecepcionDocumentosService recepDocsService;
	
	private static String nombreDocumentoFortimax;

	@Override
	public void delete(Long idDocumentoEntidad) throws MidasException {
		if(isNull(idDocumentoEntidad)){
			onError("Favor de proporcionar la clave del documento a eliminar");
		}
		DocumentoEntidadFortimax jpaObject=entidadService.findById(DocumentoEntidadFortimax.class,idDocumentoEntidad);
		entidadService.remove(jpaObject);
	}

	@Override
	public Long delete(DocumentoEntidadFortimax documentoEntidad) throws MidasException {
		Long idDocumentoEntidad=(isNotNull(documentoEntidad))?documentoEntidad.getId():null;
		delete(idDocumentoEntidad);
		return null;
	}
	
	@Override
	public List<DocumentoEntidadFortimax> getListaDocumentosGuardadosPorEntidad(Long idRegistro,String nombreAplicacion) throws MidasException {
		List<DocumentoEntidadFortimax> list=new ArrayList<DocumentoEntidadFortimax>();
		if(isNull(idRegistro)){
			onError("Favor de proporcionar la clave de la entidad a obtener sus documentos");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		DocumentoEntidadFortimax filtro=new DocumentoEntidadFortimax();
		CatalogoDocumentoFortimax documento=new CatalogoDocumentoFortimax();
		CarpetaAplicacionFortimax carpeta=new CarpetaAplicacionFortimax();
		CatalogoAplicacionFortimax aplicacion=new CatalogoAplicacionFortimax();
		aplicacion.setNombreAplicacion(nombreAplicacion);
		carpeta.setAplicacion(aplicacion);
		documento.setCarpeta(carpeta);
		filtro.setCatalogoDocumentoFortimax(documento);
		filtro.setIdRegistro(idRegistro);
		list=findByFilters(filtro);
//		if(isNull(idRegistro)){
//			onError("Favor de proporcionar la clave de la entidad a obtener sus documentos");
//		}
//		if(!isValid(nombreAplicacion)){
//			onError("Favor de proporcionar el nombre de la aplicacion");
//		}
//		Map<Integer,Object> params=new HashMap<Integer, Object>();
//		final StringBuilder queryString=new StringBuilder("");
//		queryString.append(" select ");
//		queryString.append(" docEntidad.id, ");
//		queryString.append(" docEntidad.idDocumento,");
//		queryString.append(" docEntidad.idRegistro, ");
//		queryString.append(" docEntidad.existeDocumento ");
//		queryString.append(" from MIDAS.toDocumentoEntidadFortimax docEntidad ");
//		queryString.append(" inner join MIDAS.tcCatalogoDocumentoFortimax doc on (docEntidad.idDocumento=doc.id)");
//		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on (doc.idCarpeta=carpeta.id)");
//		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax app on (carpeta.idAplicacion=app.id) ");
//		queryString.append(" where ");
//		int index=1;
//		addCondition(queryString, " app.nombreAplicacion like ? ");
//		params.put(index,nombreAplicacion);
//		index++;
//		addCondition(queryString, " docEntidad.idRegistro=? ");
//		params.put(index,idRegistro);
//		index++;
//		Query query=entityManager.createNativeQuery(getQueryString(queryString),"documentoEntidadFortimaxView");
//		if(!params.isEmpty()){
//			for(Integer key:params.keySet()){
//				query.setParameter(key,params.get(key));
//			}
//		}
//		list=query.getResultList();
		return list;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<DocumentoEntidadFortimax> findByFilters(DocumentoEntidadFortimax filtro) throws MidasException{
		List<DocumentoEntidadFortimax> list=new ArrayList<DocumentoEntidadFortimax>();
		if(isNotNull(filtro)){
			Long idRegistro=filtro.getIdRegistro();
			CatalogoDocumentoFortimax documento=filtro.getCatalogoDocumentoFortimax();
			CarpetaAplicacionFortimax carpeta=(isNotNull(documento))?documento.getCarpeta():null;
			CatalogoAplicacionFortimax aplicacion=(isNotNull(carpeta))?carpeta.getAplicacion():null;
			String nombreAplicacion=(isNotNull(aplicacion) && isValid(aplicacion.getNombreAplicacion()))?aplicacion.getNombreAplicacion():null;
			String nombreCarpeta=(isNotNull(carpeta) && isValid(carpeta.getNombreCarpeta()))?carpeta.getNombreCarpeta():null;
			Integer requerido=(isNotNull(documento) && isNotNull(documento.getRequerido()))?documento.getRequerido():null;
			Integer existeDocumento=(isNotNull(filtro.getExisteDocumento()))?filtro.getExisteDocumento():null;
			Map<Integer,Object> params=new HashMap<Integer, Object>();
			final StringBuilder queryString=new StringBuilder("");
			queryString.append(" select ");
			queryString.append(" docEntidad.id, ");
			queryString.append(" docEntidad.idDocumento,");
			queryString.append(" docEntidad.idRegistro, ");
			queryString.append(" docEntidad.existeDocumento ");
			queryString.append(" from MIDAS.toDocumentoEntidadFortimax docEntidad ");
			queryString.append(" inner join MIDAS.tcCatalogoDocumentoFortimax doc on (docEntidad.idDocumento=doc.id)");
			queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on (doc.idCarpeta=carpeta.id)");
			queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax app on (carpeta.idAplicacion=app.id) ");
			queryString.append(" where ");
			int index=1;
			if(isValid(nombreAplicacion)){
				addCondition(queryString, " TRIM(UPPER(app.nombreAplicacion)) like UPPER(?) ");
				params.put(index,nombreAplicacion);
				index++;
			}
			if(isValid(nombreCarpeta)){
				addCondition(queryString, " TRIM(UPPER(carpeta.nombreCarpeta)) like UPPER(?) ");
				params.put(index,nombreCarpeta);
				index++;
			}
			if(isNotNull(idRegistro)){
				addCondition(queryString, " docEntidad.idRegistro=? ");
				params.put(index,idRegistro);
				index++;
			}
			if(isNotNull(requerido)){
				addCondition(queryString, " doc.requerido=? ");
				params.put(index,requerido);
				index++;
			}
			if(isNotNull(existeDocumento)){
				addCondition(queryString, " docEntidad.existeDocumento=? ");
				params.put(index,existeDocumento);
				index++;
			}
			Query query=entityManager.createNativeQuery(getQueryString(queryString),"documentoEntidadFortimaxView");
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
			for(DocumentoEntidadFortimax doc:list){
				entityManager.refresh(doc);
				System.out.println("Documento existe:"+doc.getExisteDocumento());
			}
		}   
		return list;
	}
	
	@Override
	public List<DocumentoEntidadFortimax> getListaDocumentosGuardadosPorEntidadYCarpeta(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException{
		List<DocumentoEntidadFortimax> list=new ArrayList<DocumentoEntidadFortimax>();
		if(isNull(idRegistro)){
			onError("Favor de proporcionar la clave de la entidad a obtener sus documentos");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la carpeta");
		}
		DocumentoEntidadFortimax filtro=new DocumentoEntidadFortimax();
		CatalogoDocumentoFortimax documento=new CatalogoDocumentoFortimax();
		CarpetaAplicacionFortimax carpeta=new CarpetaAplicacionFortimax();
		CatalogoAplicacionFortimax aplicacion=new CatalogoAplicacionFortimax();
		aplicacion.setNombreAplicacion(nombreAplicacion);
		carpeta.setAplicacion(aplicacion);
		carpeta.setNombreCarpeta(nombreCarpeta);
		documento.setCarpeta(carpeta);
		filtro.setCatalogoDocumentoFortimax(documento);
		filtro.setIdRegistro(idRegistro);
		list=findByFilters(filtro);
//		Map<Integer,Object> params=new HashMap<Integer, Object>();
//		final StringBuilder queryString=new StringBuilder("");
//		queryString.append(" select ");
//		queryString.append(" docEntidad.id, ");
//		queryString.append(" docEntidad.idDocumento,");
//		queryString.append(" docEntidad.idRegistro, ");
//		queryString.append(" docEntidad.existeDocumento ");
//		queryString.append(" from MIDAS.toDocumentoEntidadFortimax docEntidad ");
//		queryString.append(" inner join MIDAS.tcCatalogoDocumentoFortimax doc on (docEntidad.idDocumento=doc.id)");
//		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on (doc.idCarpeta=carpeta.id)");
//		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax app on (carpeta.idAplicacion=app.id) ");
//		queryString.append(" where ");
//		int index=1;
//		addCondition(queryString, " TRIM(UPPER(app.nombreAplicacion)) like UPPER(?) ");
//		params.put(index,nombreAplicacion);
//		index++;
//		addCondition(queryString, " TRIM(UPPER(carpeta.nombreCarpeta)) like UPPER(?) ");
//		params.put(index,nombreCarpeta);
//		index++;
//		addCondition(queryString, " docEntidad.idRegistro=? ");
//		params.put(index,idRegistro);
//		index++;
//		Query query=entityManager.createNativeQuery(getQueryString(queryString),"documentoEntidadFortimaxView");
//		if(!params.isEmpty()){
//			for(Integer key:params.keySet()){
//				query.setParameter(key,params.get(key));
//			}
//		}
		//list=query.getResultList();
		return list;
	}

	@Override
	public Long save(DocumentoEntidadFortimax documentoEntidad) throws MidasException {
		if(isNull(documentoEntidad)){
			onError("Favor de proporcionar el documento a guardar");
		}
		DocumentoEntidadFortimax jpaObject=entidadService.save(documentoEntidad);
		return (isNotNull(jpaObject))?jpaObject.getId():null;
	}
	
	/**
	 * Sincroniza Fortimax vs Base de datos consultando el servicio de documentos de fortimax y comparando con los que se tiene en base de datos,
	 * actualizando si existe o no el documento.
	 * @param idRegistro id del agente, o cliente , o entidad en question.
	 * @param nombreAplicacion nombre de la aplicacion segun la entidad, por ejemplo AGENTES, o CLIENTES, revisar tcCatalogoAplicacionFortimax columna "nombreAplicacion", NO checar columna "nombreAplicacionFortimax"
	 * @param nombreCarpeta nombre de la carpeta de la entidad 
	 */
	@SuppressWarnings("null")
	@Override
	public void sincronizarDocumentos(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException,Exception{
		
		if(isNull(idRegistro)){
			onError("Favor de proporcionar la clave del registro");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la carpeta de documentos");
		}
		
		String[] documentosFortimax=fortimaxService.getDocumentFortimax(idRegistro,nombreAplicacion);
		
		if(isNull(documentosFortimax)|| documentosFortimax.length==0){
			onError("No hay documentos para la aplicacion "+nombreAplicacion+" con el numero de expediente "+idRegistro);
		}
		Map<String,Object> params = null;
		List<CatalogoDocumentoFortimax> catalogosDocumentosFortimax = new ArrayList<CatalogoDocumentoFortimax>();
		
		Predicate nomDocFortimaxPredicate = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				DocumentoEntidadFortimax obj = (DocumentoEntidadFortimax)arg0; 
				
				String nombreDocumento = (isNotNull(obj) && isNotNull(obj.getCatalogoDocumentoFortimax()) 
						&& isValid(obj.getCatalogoDocumentoFortimax().getNombreDocumentoFortimax()))
						?obj.getCatalogoDocumentoFortimax().getNombreDocumentoFortimax() + "_" + obj.getIdRegistro() :"";
				
				return nombreDocumento.equalsIgnoreCase(DocumentoEntidadFortimaxDaoImpl.getNombreDocumentoFortimax());
			}
		};
		
		DocumentoEntidadFortimax documentoEntidadFortimax = null;
		List<DocumentoEntidadFortimax> listaDocumentoDB = getListaDocumentosGuardadosPorEntidadYCarpeta(idRegistro, nombreAplicacion, nombreCarpeta);
		
		for (DocumentoEntidadFortimax docEntidadFortimax : listaDocumentoDB) {
			if (docEntidadFortimax.getExisteDocumento().intValue() == 1) {
				docEntidadFortimax.setExisteDocumento(0);
				save(docEntidadFortimax);
			}
		}
		
		for(String docFortimax : documentosFortimax) {
			if (isValid(docFortimax)) {
				DocumentoEntidadFortimaxDaoImpl.setNombreDocumentoFortimax(docFortimax);
				documentoEntidadFortimax = (DocumentoEntidadFortimax)CollectionUtils.find(listaDocumentoDB, nomDocFortimaxPredicate);
				
				if (documentoEntidadFortimax != null) {
					actualizarSubidaDocumentoFortimax(documentoEntidadFortimax);
				} else {
					params = new HashMap<String, Object>();
					params.put("carpeta.nombreCarpeta", nombreCarpeta);
					params.put("nombreDocumentoFortimax", docFortimax.substring(0, docFortimax.indexOf("_")));
					catalogosDocumentosFortimax = entidadService.findByProperties(CatalogoDocumentoFortimax.class, params);
					
					DocumentoEntidadFortimax nuevoDocumentoEntidadFortimax= new DocumentoEntidadFortimax();
					if(!catalogosDocumentosFortimax.isEmpty()){
						nuevoDocumentoEntidadFortimax.setCatalogoDocumentoFortimax(catalogosDocumentosFortimax.get(0));
						nuevoDocumentoEntidadFortimax.setExisteDocumento(1);
						nuevoDocumentoEntidadFortimax.setIdRegistro(idRegistro);
						save(nuevoDocumentoEntidadFortimax);
					}
				}
				
			}
		}
	}
	
	@Override
	public boolean existeEstructuraPorEntidadAplicacion(Long idRegistro,String nombreAplicacion) throws MidasException{
		boolean existeEstructura=false;
		List<DocumentoEntidadFortimax> list=new ArrayList<DocumentoEntidadFortimax>();
		if(isNull(idRegistro)){
			onError("Favor de proporcionar la clave de la entidad a obtener sus documentos");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		DocumentoEntidadFortimax filtro=new DocumentoEntidadFortimax();
		CatalogoDocumentoFortimax documento=new CatalogoDocumentoFortimax();
		CarpetaAplicacionFortimax carpeta=new CarpetaAplicacionFortimax();
		CatalogoAplicacionFortimax aplicacion=new CatalogoAplicacionFortimax();
		aplicacion.setNombreAplicacion(nombreAplicacion);
		carpeta.setAplicacion(aplicacion);
		documento.setCarpeta(carpeta);
		filtro.setCatalogoDocumentoFortimax(documento);
		filtro.setIdRegistro(idRegistro);
		list=findByFilters(filtro);
//		if(isNull(idRegistro)){
//			onError("Favor de proporcionar la clave de la entidad a obtener sus documentos");
//		}
//		if(!isValid(nombreAplicacion)){
//			onError("Favor de proporcionar el nombre de la aplicacion");
//		}
//		Map<Integer,Object> params=new HashMap<Integer, Object>();
//		final StringBuilder queryString=new StringBuilder("");
//		queryString.append(" select ");
//		queryString.append(" docEntidad.id, ");
//		queryString.append(" docEntidad.idDocumento,");
//		queryString.append(" docEntidad.idRegistro, ");
//		queryString.append(" docEntidad.existeDocumento ");
//		queryString.append(" from MIDAS.toDocumentoEntidadFortimax docEntidad ");
//		queryString.append(" inner join MIDAS.tcCatalogoDocumentoFortimax doc on (docEntidad.idDocumento=doc.id)");
//		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on (doc.idCarpeta=carpeta.id)");
//		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax app on (carpeta.idAplicacion=app.id) ");
//		queryString.append(" where ");
//		int index=1;
//		addCondition(queryString, " TRIM(UPPER(app.nombreAplicacion)) like UPPER(?) ");
//		params.put(index,nombreAplicacion);
//		index++;
//		addCondition(queryString, " docEntidad.idRegistro=? ");
//		params.put(index,idRegistro);
//		index++;
//		Query query=entityManager.createNativeQuery(getQueryString(queryString),"documentoEntidadFortimaxView");
//		if(!params.isEmpty()){
//			for(Integer key:params.keySet()){
//				query.setParameter(key,params.get(key));
//			}
//		}
//		list=query.getResultList();
		if(!isEmptyList(list)){
			existeEstructura=true;
		}
		return existeEstructura;
	}
	
	@Override
	public boolean existeEstructuraPorEntidadAplicacionYCarpeta(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException{
		boolean existeEstructura=false;
		List<DocumentoEntidadFortimax> list=new ArrayList<DocumentoEntidadFortimax>();
		if(isNull(idRegistro)){
			onError("Favor de proporcionar la clave de la entidad a obtener sus documentos");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		DocumentoEntidadFortimax filtro=new DocumentoEntidadFortimax();
		CatalogoDocumentoFortimax documento=new CatalogoDocumentoFortimax();
		CarpetaAplicacionFortimax carpeta=new CarpetaAplicacionFortimax();
		CatalogoAplicacionFortimax aplicacion=new CatalogoAplicacionFortimax();
		aplicacion.setNombreAplicacion(nombreAplicacion);
		carpeta.setAplicacion(aplicacion);
		carpeta.setNombreCarpeta(nombreCarpeta);
		documento.setCarpeta(carpeta);
		filtro.setCatalogoDocumentoFortimax(documento);
		filtro.setIdRegistro(idRegistro);
		list=findByFilters(filtro);
		if(!isEmptyList(list)){
			existeEstructura=true;
		}
		return existeEstructura;
	}
	/**
	 * Actualiza el documento y lo marca como ya subido en fortimax
	 * @param doc
	 * @throws MidasException
	 */
	private void actualizarSubidaDocumentoFortimax(DocumentoEntidadFortimax doc) throws MidasException{
		doc.setExisteDocumento(1);
		save(doc);
	}
	/**
	 * Metodo que obtiene los documentos pendientes por subir
	 * @param idRegistro
	 * @param nombreAplicacion
	 * @param nombreCarpeta
	 * @return
	 * @throws MidasException
	 */
	@Override
	public List<DocumentoEntidadFortimax> getListaDocumentosFaltantes(Long idRegistro,String nombreAplicacion,String nombreCarpeta) throws MidasException{
		List<DocumentoEntidadFortimax> list=new ArrayList<DocumentoEntidadFortimax>();
		if(isNull(idRegistro)){
			onError("Favor de proporcionar la clave de la entidad a obtener sus documentos");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la carpeta");
		}
		DocumentoEntidadFortimax filtro=new DocumentoEntidadFortimax();
		CatalogoDocumentoFortimax documento=new CatalogoDocumentoFortimax();
		CarpetaAplicacionFortimax carpeta=new CarpetaAplicacionFortimax();
		CatalogoAplicacionFortimax aplicacion=new CatalogoAplicacionFortimax();
		aplicacion.setNombreAplicacion(nombreAplicacion);
		carpeta.setNombreCarpeta(nombreCarpeta);
		carpeta.setAplicacion(aplicacion);
		documento.setCarpeta(carpeta);
		documento.setRequerido(1);
		filtro.setCatalogoDocumentoFortimax(documento);
		filtro.setIdRegistro(idRegistro);
		filtro.setExisteDocumento(0);
		list=findByFilters(filtro);
//		if(isNull(idRegistro)){
//			onError("Favor de proporcionar la clave del registro");
//		}
//		if(!isValid(nombreAplicacion)){
//			onError("Favor de proporcionar el nombre de la aplicacion");
//		}
//		if(!isValid(nombreCarpeta)){
//			onError("Favor de proporcionar el nombre de la carpeta de documentos");
//		}
//		List<DocumentoEntidadFortimax> list=new ArrayList<DocumentoEntidadFortimax>();
//		Map<Integer,Object> params=new HashMap<Integer, Object>();
//		final StringBuilder queryString=new StringBuilder("");
//		queryString.append(" select ");
//		queryString.append(" docEntidad.id, ");
//		queryString.append(" docEntidad.idDocumento,");
//		queryString.append(" docEntidad.idRegistro, ");
//		queryString.append(" docEntidad.existeDocumento ");
//		queryString.append(" from MIDAS.toDocumentoEntidadFortimax docEntidad ");
//		queryString.append(" inner join MIDAS.tcCatalogoDocumentoFortimax doc on (docEntidad.idDocumento=doc.id)");
//		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on (doc.idCarpeta=carpeta.id)");
//		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax app on (carpeta.idAplicacion=app.id) ");
//		queryString.append(" where ");
//		int index=1;
//		addCondition(queryString, " TRIM(UPPER(app.nombreAplicacion)) like UPPER(?) ");
//		params.put(index,nombreAplicacion);
//		index++;
//		addCondition(queryString, " TRIM(UPPER(carpeta.nombreCarpeta)) like UPPER(?) ");
//		params.put(index,nombreCarpeta);
//		index++;
//		addCondition(queryString, " docEntidad.idRegistro=? ");
//		params.put(index,idRegistro);
//		index++;
//		addCondition(queryString, " doc.requerido=? ");
//		params.put(index,1);//QUe sea requerido
//		index++;
//		addCondition(queryString, " docEntidad.existeDocumento=? ");//Donde el documento no existe en Fortimax, es decir, esta pendiente por subir
//		params.put(index,0);
//		index++;
//		Query query=entityManager.createNativeQuery(getQueryString(queryString),"documentoEntidadFortimaxView");
//		if(!params.isEmpty()){
//			for(Integer key:params.keySet()){
//				query.setParameter(key,params.get(key));
//			}
//		}
//		list=query.getResultList();
		return list;
	}
	
	@Override
	public void saveDocumentosAgrupados(DocumentosAgrupados documentos, Integer numeroRegistros, String nombreDocumento, String nombreAplicacion, String nombreCarpeta){
		try {
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("nombreDocumento", nombreDocumento);
			parametros.put("carpeta.nombreCarpeta", nombreCarpeta);			
			for(int i=1;i<=numeroRegistros;i++){
				List<CatalogoDocumentoFortimax>document=new ArrayList<CatalogoDocumentoFortimax>();
				document=entidadService.findByProperties(CatalogoDocumentoFortimax.class, parametros);
				documentos.setIdDocumento(document.get(0).getId());
				documentos.setNombre(document.get(0).getNombreDocumentoFortimax()+"_"+documentos.getIdAgrupador()+"_"+i+"_"+documentos.getIdEntidad());
				DocumentosAgrupados docAgrupados = new DocumentosAgrupados();
				BeanUtils.copyProperties(docAgrupados,documentos);
				entidadService.save(docAgrupados);
				String[]resp=fortimaxService.generateDocument(documentos.getIdEntidad(), nombreAplicacion, 
				document.get(0).getNombreDocumentoFortimax()+"_"+documentos.getIdAgrupador()+"_"+i+"_"+documentos.getIdEntidad(), 
				document.get(0).getCarpeta().getNombreCarpetaFortimax());	
				System.out.println(resp[0]);
			}			
		} catch (Exception e) {			
			e.printStackTrace();
		}
	}
	
	@Override
	public String sincronizarGrupoDocumentos(String nombreDocumento, String nombreAplicacion, String nombreCarpeta, DocumentosAgrupados documentos){
		final StringBuilder documentosFaltantes=new StringBuilder("");
		String listaDocumentosFaltantes=new String();
		try {
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("nombreDocumento", nombreDocumento);
		parametros.put("carpeta.nombreCarpeta", nombreCarpeta);	
		List<CatalogoDocumentoFortimax>document=new ArrayList<CatalogoDocumentoFortimax>();
		document=entidadService.findByProperties(CatalogoDocumentoFortimax.class, parametros);
		
		Map<String, Object> par = new LinkedHashMap<String, Object>();
		par.put("idDocumento", document.get(0).getId());
		par.put("idEntidad", documentos.getIdEntidad());
		if(documentos.getIdAgrupador()!=null){
			par.put("idAgrupador", documentos.getIdAgrupador());
		}
		List<DocumentosAgrupados>documentAgrupados=new ArrayList<DocumentosAgrupados>();
		documentAgrupados=entidadService.findByProperties(DocumentosAgrupados.class, par);
		String[] documentosFortimax=fortimaxService.getDocumentFortimax(documentos.getIdEntidad(),nombreAplicacion);
		System.out.println(documentosFortimax.length);
		List<String>documentosFortimaxList=Arrays.asList(documentosFortimax);
		for(DocumentosAgrupados documentosAgrupados:documentAgrupados){
			Integer existe=documentosFortimaxList.indexOf(documentosAgrupados.getNombre().trim());
			if(existe.equals(-1)){
				documentosFaltantes.append(documentosAgrupados.getNombre());
				documentosFaltantes.append(",");
				documentosAgrupados.setExisteDocumento(0);
				entidadService.save(documentosAgrupados);	
			}
			else{
				documentosAgrupados.setExisteDocumento(1);
				entidadService.save(documentosAgrupados);			
			}
		}		
		if(!documentosFaltantes.toString().equals("")){
			listaDocumentosFaltantes=documentosFaltantes.substring(0,documentosFaltantes.length()-1);
		}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listaDocumentosFaltantes;
	}
	
	@Override
	public void auditarDocumentosEntregadosAnio(Long idAgente,Long anio,String nombreAplicacion) throws MidasException {
		if(isNull(idAgente)){
			onError("Favor de proporcionar la clave del registro");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(isNull(anio)){
			onError("Favor de proporcionar el año a consultar");
		}
				
		String[] documentosFortimax;
		try {
			/*lista de documentos que estan dados de alta en el portal de fortimax*/
			documentosFortimax = fortimaxService.getDocumentFortimax(idAgente,nombreAplicacion);
			if(isNull(documentosFortimax)|| documentosFortimax.length==0){
				onError("No hay documentos para la aplicacion "+nombreAplicacion+" con el numero de expediente "+idAgente);
			}					
			/*lista de documentos guardados en base de datos midas*/
			List<EntregoDocumentosView> lista=recepDocsService.listaFacturasMesAnio(anio, idAgente);
			if(isEmptyList(lista)){
				onError("No hay registros de documentos para la aplicacion "+nombreAplicacion+" con el expediente "+idAgente);
			}
			
			for(EntregoDocumentosView doc:lista){
				String nombreDocumento=doc.getValor();
				Long idDocumento = doc.getId();
				Integer checado = doc.getChecado();
				if(isNotNull(doc) && isValid(nombreDocumento)){
					for(String docFortimax:documentosFortimax){
						//Si coincide el documento de base de datos con lo de fortimax y no ha sido subido, entonces lo marca como subido
						//if(isValid(docFortimax) && !docFortimax.equalsIgnoreCase(nombreDocumento)){
						DocumentosAgrupados rowDocumentos = entityManager.find(DocumentosAgrupados.class, idDocumento);	
						if(nombreDocumento.equalsIgnoreCase(docFortimax)){
							//cif(!docFortimax.equalsIgnoreCase(nombreDocumento)){															
								rowDocumentos.setExisteDocumento(1);
								entidadService.save(rowDocumentos);
								break;
							//}
						}
						
					}						
				}
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	/**
	 * ===================================================================
	 * Setters and getters
	 * ===================================================================
	 */
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@EJB
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	
	@EJB
	public void setRecepDocsService(RecepcionDocumentosService recepDocsService) {
		this.recepDocsService = recepDocsService;
	}
	
	public static String getNombreDocumentoFortimax() {
		return nombreDocumentoFortimax;
	}
	
	public static void setNombreDocumentoFortimax(String nombreDocumentoFortimax) {
		DocumentoEntidadFortimaxDaoImpl.nombreDocumentoFortimax = nombreDocumentoFortimax;
	}
	
}
