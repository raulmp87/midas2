/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaBaseCalculoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless
public class CaBaseCalculoDaoImpl implements CaBaseCalculoDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaBaseCalculoDaoImpl.class);
	
	public void save(CaBaseCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaBaseCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaBaseCalculo.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaBaseCalculo update(CaBaseCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaBaseCalculo result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaBaseCalculo 	::		CaBaseCalculoDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaBaseCalculo findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaBaseCalculoDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaBaseCalculo instance = entityManager.find(CaBaseCalculo.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaBaseCalculoDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaBaseCalculoDaoImpl	::	findById	::	ERROR	::	",re);
	            throw re;
        }
    }
    @SuppressWarnings("unchecked")
	public List<CaBaseCalculo> findByProperties(String propertyName, int ...isValues){
		LOGGER.info("	::	[INF]	::	Buscando por Propiedades 	::		CaBaseCalculoDaoImpl	::	findByProperties	::	INICIO	::	");
		try {
			StringBuffer strQuery = new StringBuffer();
//			strQuery.append("select model from CaBaseCalculo where model."+ propertyName + " in (");
			strQuery.append("select * from midas.ca_basecalculo where "+ propertyName + " in (");
			
			for (int i = 0; i < isValues.length; i++){
				if(i != 0 )
					strQuery.append(",");
				strQuery.append(isValues[i]);
			}			
			strQuery.append(" )");
			strQuery.append(" and borradologico = 0");
			Query query = entityManager.createNativeQuery(strQuery.toString(), CaBaseCalculo.class);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedades 	::		CaBaseCalculoDaoImpl	::	findByProperties	::	FIN	::	");
			return query.getResultList();
		}catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedades 	::		CaBaseCalculoDaoImpl	::	findByProperties	::	ERROR	::	",re);
			return null;
			}
		}	
    @SuppressWarnings("unchecked")
    public List<CaBaseCalculo> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaBaseCalculoDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaBaseCalculo model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaBaseCalculoDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaBaseCalculoDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaBaseCalculo> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaBaseCalculo> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaBaseCalculo> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaBaseCalculo> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaBaseCalculo> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaBaseCalculoDaoImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaBaseCalculo model where " + BORRADOLOGICO + "";

//			Query query = entityManager.createQuery(queryString, CaBaseCalculo.class);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaBaseCalculoDaoImpl	::	findAll	::	FIN	::	");
//			return query.getResultList();
			return findByProperty(BORRADOLOGICO, ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaBaseCalculoDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}