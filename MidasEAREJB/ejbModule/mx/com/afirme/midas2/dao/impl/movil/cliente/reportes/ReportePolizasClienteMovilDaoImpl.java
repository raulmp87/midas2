package mx.com.afirme.midas2.dao.impl.movil.cliente.reportes;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.movil.cliente.reportes.ReportePolizasClienteMovilDao;
import mx.com.afirme.midas2.domain.movil.cliente.PolizasClienteMovil;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class ReportePolizasClienteMovilDaoImpl extends EntidadDaoImpl implements ReportePolizasClienteMovilDao  {

	public static final Logger LOG = Logger.getLogger(ReportePolizasClienteMovilDaoImpl.class);
	
	@PersistenceContext
	private EntityManager em;	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PolizasClienteMovil> getReportePolizasClienteMovil(String tipoPoliza){
		LOG.info("Entrando a getReportePolizasClienteMovil ");
		try {
			if(tipoPoliza.equals("D")){
				final String queryString = "select model from PolizasClienteMovil model where "
						+ " model.tipo " + " = :propertyTipoPoliza or model.tipo " + " = :propertyTipoPoliza2 "
						+ " or model.tipo " + " = :propertyTipoPoliza3";
				Query query = em.createQuery(queryString);
				query.setParameter("propertyTipoPoliza", tipoPoliza);
				query.setParameter("propertyTipoPoliza2", "P");
				query.setParameter("propertyTipoPoliza3", "C");
				return query.getResultList();
			}else {
				final String queryString = "select model from PolizasClienteMovil model where "
						+ " model.tipo " + " = :propertyTipoPoliza";
				Query query = em.createQuery(queryString);
				query.setParameter("propertyTipoPoliza", tipoPoliza);
				return query.getResultList();
			}
		}catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by tipoPoliza failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<NegocioSeguros> getTipoPolizas(){
		try {
			final String queryString = "select model from NegocioSeguros model";
			Query query = em.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	
	
	@EJB
	private EntidadDao entidadDao;

	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
}
