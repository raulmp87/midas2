package mx.com.afirme.midas2.dao.impl.fortimax;
import static mx.com.afirme.midas2.utils.CommonUtils.addCondition;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.fortimax.DocumentoCarpetaFortimaxDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class DocumentoCarpetaFortimaxDaoImpl extends EntidadDaoImpl implements DocumentoCarpetaFortimaxDao{
	private EntidadService entidadService;
	@Override
	public void delete(Long id) throws MidasException {
		if(isNull(id)){
			onError("Favor de proporcionar la clave del documento a eliminar");
		}
		CatalogoDocumentoFortimax jpaObject=entidadService.findById(CatalogoDocumentoFortimax.class,id);
		entidadService.remove(jpaObject);
	}

	@Override
	public Long delete(CatalogoDocumentoFortimax documento) throws MidasException {
		Long id=(isNotNull(documento))?documento.getId():null;
		delete(id);
		return null;
	}
	/**
	 * Obtiene la lista de documentos por aplicacion, es decir, toda los documentos de la estructura de X aplicacion
	 * @param nombreAplicacion
	 * @return
	 * @throws MidasException
	 */
	@SuppressWarnings("unchecked")
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorAplicacion(String nombreAplicacion) throws MidasException{
		List<CatalogoDocumentoFortimax> list=new ArrayList<CatalogoDocumentoFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select ");
		queryString.append(" doc.id,");
		queryString.append(" doc.idCarpeta,");
		queryString.append(" doc.nombreDocumento,");
		queryString.append(" doc.descripcion,");
		queryString.append(" doc.nombreDocumentoFortimax,");
		queryString.append(" doc.clavePf,");
		queryString.append(" doc.clavePm,");
		queryString.append(" doc.requerido ");
		queryString.append(" from MIDAS.tcCatalogoDocumentoFortimax doc ");
		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on(doc.idCarpeta=carpeta.id)");
		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax aplicacion on (aplicacion.id=carpeta.idAplicacion) ");
		queryString.append(" where ");
		int index=1;
		addCondition(queryString, " aplicacion.nombreAplicacion like ? ");
		params.put(index,nombreAplicacion);
		index++;
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"catalogoDocumentoFortimaxView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list=query.getResultList();
		return list;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorAplicacionTipoPersona(String nombreAplicacion,Long idTipoPersona) throws MidasException{
		List<CatalogoDocumentoFortimax> list=new ArrayList<CatalogoDocumentoFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(isNull(idTipoPersona)){
			onError("Favor de proporcionar el tipo de persona a consultar");
		}
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select ");
		queryString.append(" doc.id,");
		queryString.append(" doc.idCarpeta,");
		queryString.append(" doc.nombreDocumento,");
		queryString.append(" doc.descripcion,");
		queryString.append(" doc.nombreDocumentoFortimax,");
		queryString.append(" doc.clavePf,");
		queryString.append(" doc.clavePm,");
		queryString.append(" doc.requerido ");
		queryString.append(" from MIDAS.tcCatalogoDocumentoFortimax doc ");
		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on(doc.idCarpeta=carpeta.id)");
		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax aplicacion on (aplicacion.id=carpeta.idAplicacion) ");
		queryString.append(" where ");
		int index=1;
		addCondition(queryString, " aplicacion.nombreAplicacion like ? ");
		params.put(index,nombreAplicacion);
		index++;
		if(idTipoPersona==1){
			addCondition(queryString, " doc.clavePf = ? ");
		}else{
			addCondition(queryString, " doc.clavePm = ? ");
		}
		params.put(index,1);//Si es cualquier tipo de persona, su valor debe de ser 1 para que aplique o 0 para que no aplique
		index++;
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"catalogoDocumentoFortimaxView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list=query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorCarpeta(String nombreAplicacion,String nombreCarpeta) throws MidasException {
		List<CatalogoDocumentoFortimax> list=new ArrayList<CatalogoDocumentoFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la carpeta");
		}
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select ");
		queryString.append(" doc.id,");
		queryString.append(" doc.idCarpeta,");
		queryString.append(" doc.nombreDocumento,");
		queryString.append(" doc.descripcion,");
		queryString.append(" doc.nombreDocumentoFortimax,");
		queryString.append(" doc.clavePf,");
		queryString.append(" doc.clavePm,");
		queryString.append(" doc.requerido ");
		queryString.append(" from MIDAS.tcCatalogoDocumentoFortimax doc ");
		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on(doc.idCarpeta=carpeta.id)");
		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax aplicacion on (aplicacion.id=carpeta.idAplicacion) ");
		queryString.append(" where ");
		int index=1;
		addCondition(queryString, " aplicacion.nombreAplicacion like ? ");
		params.put(index,nombreAplicacion);
		index++;
		addCondition(queryString, " carpeta.nombreCarpeta like ? ");
		params.put(index,nombreCarpeta);
		index++;
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"catalogoDocumentoFortimaxView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list=query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosPorCarpetaConTipoPersona(String nombreAplicacion,String nombreCarpeta,Long idTipoPersona) throws MidasException {
		List<CatalogoDocumentoFortimax> list=new ArrayList<CatalogoDocumentoFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la carpeta");
		}
		if(isNull(idTipoPersona)){
			onError("Favor de proporcionar el tipo de persona");
		}
		if(idTipoPersona!=1 && idTipoPersona!=2){
			onError("Tipo de persona indefinido");
		} 
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select ");
		queryString.append(" doc.id,");
		queryString.append(" doc.idCarpeta,");
		queryString.append(" doc.nombreDocumento,");
		queryString.append(" doc.descripcion,");
		queryString.append(" doc.nombreDocumentoFortimax,");
		queryString.append(" doc.clavePf,");
		queryString.append(" doc.clavePm,");
		queryString.append(" doc.requerido ");
		queryString.append(" from MIDAS.tcCatalogoDocumentoFortimax doc ");
		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on(doc.idCarpeta=carpeta.id)");
		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax aplicacion on (aplicacion.id=carpeta.idAplicacion) ");
		queryString.append(" where ");
		int index=1;
		addCondition(queryString, " aplicacion.nombreAplicacion like ? ");
		params.put(index,nombreAplicacion);
		index++;
		addCondition(queryString, " carpeta.nombreCarpeta like ? ");
		params.put(index,nombreCarpeta);
		index++;
		if(idTipoPersona==1){
			addCondition(queryString, " doc.clavePf = ? ");
		}else{
			addCondition(queryString, " doc.clavePm = ? ");
		}
		params.put(index,1);//Si es cualquier tipo de persona, su valor debe de ser 1 para que aplique o 0 para que no aplique
		index++;
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"catalogoDocumentoFortimaxView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list=query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosRequeridosPorCarpeta(String nombreAplicacion,String nombreCarpeta) throws MidasException {
		List<CatalogoDocumentoFortimax> list=new ArrayList<CatalogoDocumentoFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la carpeta");
		}
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select ");
		queryString.append(" doc.id,");
		queryString.append(" doc.idCarpeta,");
		queryString.append(" doc.nombreDocumento,");
		queryString.append(" doc.descripcion,");
		queryString.append(" doc.nombreDocumentoFortimax,");
		queryString.append(" doc.clavePf,");
		queryString.append(" doc.clavePm,");
		queryString.append(" doc.requerido ");
		queryString.append(" from MIDAS.tcCatalogoDocumentoFortimax doc ");
		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on(doc.idCarpeta=carpeta.id)");
		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax aplicacion on (aplicacion.id=carpeta.idAplicacion) ");
		queryString.append(" where ");
		int index=1;
		addCondition(queryString, " aplicacion.nombreAplicacion like ? ");
		params.put(index,nombreAplicacion);
		index++;
		addCondition(queryString, " carpeta.nombreCarpeta like ? ");
		params.put(index,nombreCarpeta);
		index++;
		addCondition(queryString, " doc.requerido=? ");
		params.put(index,1);//Es 1 si aplica que unicamente sea el requerido
		index++;
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"catalogoDocumentoFortimaxView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list=query.getResultList();
		return list;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoDocumentoFortimax> obtenerDocumentosRequeridosPorCarpetaConTipoPersona(String nombreAplicacion,String nombreCarpeta,Long idTipoPersona) throws MidasException {
		List<CatalogoDocumentoFortimax> list=new ArrayList<CatalogoDocumentoFortimax>();
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		if(!isValid(nombreCarpeta)){
			onError("Favor de proporcionar el nombre de la carpeta");
		}
		if(isNull(idTipoPersona)){
			onError("Favor de proporcionar el tipo de persona");
		}
		if(idTipoPersona!=1 && idTipoPersona!=2){
			onError("Tipo de persona indefinido");
		}
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select ");
		queryString.append(" doc.id,");
		queryString.append(" doc.idCarpeta,");
		queryString.append(" doc.nombreDocumento,");
		queryString.append(" doc.descripcion,");
		queryString.append(" doc.nombreDocumentoFortimax,");
		queryString.append(" doc.clavePf,");
		queryString.append(" doc.clavePm,");
		queryString.append(" doc.requerido ");
		queryString.append(" from MIDAS.tcCatalogoDocumentoFortimax doc ");
		queryString.append(" inner join MIDAS.tcCarpetaAplicacionFortimax carpeta on(doc.idCarpeta=carpeta.id)");
		queryString.append(" inner join MIDAS.tcCatalogoAplicacionFortimax aplicacion on (aplicacion.id=carpeta.idAplicacion) ");
		queryString.append(" where ");
		int index=1;
		addCondition(queryString, " aplicacion.nombreAplicacion like ? ");
		params.put(index,nombreAplicacion);
		index++;
		addCondition(queryString, " carpeta.nombreCarpeta like ? ");
		params.put(index,nombreCarpeta);
		index++;
		if(idTipoPersona==1){
			addCondition(queryString, " doc.clavePf = ? ");
		}else{
			addCondition(queryString, " doc.clavePm = ? ");
		}
		params.put(index,1);//Si es cualquier tipo de persona, su valor debe de ser 1 para que aplique o 0 para que no aplique
		index++;
		addCondition(queryString, " doc.requerido=? ");
		params.put(index,1);//Es 1 si aplica que unicamente sea el requerido
		index++;
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"catalogoDocumentoFortimaxView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list=query.getResultList();
		return list;
	}

	@Override
	public Long save(CatalogoDocumentoFortimax documento) throws MidasException {
		if(isNull(documento)){
			onError("Favor de proporcionar el documento a guardar");
		}
		CatalogoDocumentoFortimax jpaObjecto=entidadService.save(documento);
		return (isNotNull(jpaObjecto))?jpaObjecto.getId():null;
	}
	/**
	 * ==============================================================================
	 * Setters and getters
	 * ==============================================================================
	 */
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
