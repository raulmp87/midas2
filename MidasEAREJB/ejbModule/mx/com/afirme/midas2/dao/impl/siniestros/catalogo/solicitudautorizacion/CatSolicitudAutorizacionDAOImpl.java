package mx.com.afirme.midas2.dao.impl.siniestros.catalogo.solicitudautorizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionAjusteAntiguedad;
import mx.com.afirme.midas2.dto.siniestros.catalogos.solicitudautorizacion.SolicitudAutorizacionAntiguedadDTO;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionService.SolicitudAutorizacionFiltro;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.commons.lang.StringUtils;
import org.joda.time.LocalDate;
import org.joda.time.Months;


@Stateless
public class CatSolicitudAutorizacionDAOImpl  extends EntidadDaoImpl implements CatSolicitudAutorizacionDao {
	
	private static final int	CANT_ELEMENTOS_NUMEROPOLIZA	= 3;
	private static final int	POSICIONES_NUMPOLIZA	= 8;

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private AutorizacionReservaService autorizacionReservaService;
	
	
	
	
	
	private static final String IGUAL = " = ";
	private static final String MAYOR_QUE = " > " ;
	private static final String MENOR_QUE = " < ";
	private static final String MAYOR_IGUAL = " >= ";
	private static final String MENOR_IGUAL = " <= ";
	private static final String LIKE = " LIKE ";
	
	private static final String WHERE_CONDITION = " WHERE ";
	private static final String AND_CONDITION = " AND ";
	private static final int PRIMER_ELEMENTO = 0;

	@SuppressWarnings("unchecked")
	@Override
	public List<SolicitudAutorizacionAjusteAntiguedad> buscarAutorizacionAntiguedad(SolicitudAutorizacionAntiguedadDTO filtro) {

		List<SolicitudAutorizacionAjusteAntiguedad> solicitudes = null;
		
		Long oficinaId = filtro.getOficinaId();
		BigDecimal coberturaId = null;

		if(filtro.getCoberturaId()!=null && !filtro.getCoberturaId().equals("")){
			String[] coberturaInfo = filtro.getCoberturaId().trim().split("-");
			coberturaId = new BigDecimal(coberturaInfo[PRIMER_ELEMENTO]);
		}
		String tipoEstimacion = filtro.getTipoEstimacion();
		String tipoAjuste = filtro.getTipoAjuste();
		String numeroSiniestro = filtro.getNumeroSiniestro();
		String condicionMonto = filtro.getCondicionMonto();
		BigDecimal monto = filtro.getMonto();
		String condicionAntiguedad = filtro.getCondicionAntiguedad();
		Integer meses = filtro.getMeses();
		Date fechaIni = filtro.getFechaIni();
		Date fechaFin = filtro.getFechaFin();
		String estatus = filtro.getEstatus();
		String tipoSiniestro = filtro.getTipoSiniestro();
		
		Map<String,Object> parametersMap = new HashMap<String,Object>();
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT solicitud ");
		queryString.append(" FROM   SolicitudAutorizacionAjusteAntiguedad solicitud ");
		queryString = this.addCondition(queryString, "solicitud.estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.oficina.id", "idOficina",IGUAL, oficinaId, parametersMap);
		queryString = this.addCondition(queryString, "solicitud.estimacionCobertura.coberturaReporteCabina.coberturaDTO.idToCobertura", "coberturaId",IGUAL,  coberturaId, parametersMap);
		queryString = this.addCondition(queryString, "solicitud.estimacionCobertura.tipoEstimacion", "tipoEstimacion",IGUAL,tipoEstimacion, parametersMap);
		queryString = this.addCondition(queryString, "solicitud.tipoAjuste", "tipoAjuste",IGUAL,tipoAjuste, parametersMap);
		String operadorMonto = this.creaOperadorPorCondicion(condicionMonto);
		queryString = this.addCondition(queryString, "solicitud.montoAjuste", "monto",operadorMonto, monto, parametersMap);
		queryString = this.addCondition(queryString, "solicitud.estatus", "estatus",IGUAL,estatus, parametersMap);
		queryString = this.addCondition(queryString, "solicitud.estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.causaSiniestro", "tipoSiniestro",IGUAL,tipoSiniestro, parametersMap);
		queryString = this.addConditionByRol(queryString, parametersMap, fechaIni, fechaFin);
		Query query = this.entityManager.createQuery(queryString.toString());
		this.addParametersToQuery(query, parametersMap);
		solicitudes =  query.getResultList();
		String operadorMesesConFechaCreacionReporte = this.creaOperadorPorCondicion(condicionAntiguedad);
		solicitudes = this.filtrarPorDiferenciaDeMeses(solicitudes, meses, operadorMesesConFechaCreacionReporte);
		solicitudes = this.filtrarPorNumeroDeSiniestro(solicitudes, numeroSiniestro);
		return solicitudes;
	}


	private List<SolicitudAutorizacionAjusteAntiguedad> filtrarPorDiferenciaDeMeses(List<SolicitudAutorizacionAjusteAntiguedad> solicitudes,Integer meses,String condicion){
		List<SolicitudAutorizacionAjusteAntiguedad> solicitudesFiltradas = new ArrayList<SolicitudAutorizacionAjusteAntiguedad>();
		if(meses!=null){
			for(SolicitudAutorizacionAjusteAntiguedad solicitud : solicitudes){
				Date fechaCreacionSiniestro = solicitud.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getFechaCreacion();
				int monthsBetween = Months.monthsBetween(new LocalDate(fechaCreacionSiniestro), new LocalDate()).getMonths();
				if(condicion.equalsIgnoreCase(IGUAL)){
					if(meses.intValue()== monthsBetween){
						solicitudesFiltradas.add(solicitud);
					}
				}else if(condicion.equalsIgnoreCase(MAYOR_QUE)){
					if(meses.intValue()> monthsBetween){
						solicitudesFiltradas.add(solicitud);
					}
				}else if(condicion.equalsIgnoreCase(MAYOR_IGUAL)){
					if(meses.intValue()>= monthsBetween){
						solicitudesFiltradas.add(solicitud);
					}
				}else if(condicion.equalsIgnoreCase(MENOR_QUE)){
					if(meses.intValue()< monthsBetween){
						solicitudesFiltradas.add(solicitud);
					}
				}else if(condicion.equalsIgnoreCase(MENOR_IGUAL) && meses.intValue()<= monthsBetween){
						solicitudesFiltradas.add(solicitud);
				}
			}
		}else{
			solicitudesFiltradas = solicitudes;
		}
			
		return solicitudesFiltradas;
	}
	
	private List<SolicitudAutorizacionAjusteAntiguedad> filtrarPorNumeroDeSiniestro(List<SolicitudAutorizacionAjusteAntiguedad> solicitudes,String filtroNumeroDeSiniestro){
		List<SolicitudAutorizacionAjusteAntiguedad> solicitudesFiltradas = new ArrayList<SolicitudAutorizacionAjusteAntiguedad>();
		if(filtroNumeroDeSiniestro!=null && !filtroNumeroDeSiniestro.equals("")){
			for(SolicitudAutorizacionAjusteAntiguedad solicitud : solicitudes){
				SiniestroCabina siniestroCabina = solicitud.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getSiniestroCabina();
				if(siniestroCabina!=null && siniestroCabina.getNumeroSiniestro().equals(filtroNumeroDeSiniestro)){
						solicitudesFiltradas.add(solicitud);
				}
			}
		}else{
			solicitudesFiltradas = solicitudes;
		}
		return solicitudesFiltradas;
	}
	
	
	private void addParametersToQuery(Query query, Map<String,Object> parametersMap){
		Set<String> keys = parametersMap.keySet();
		for(String key: keys){
			Object value = parametersMap.get(key);
			query.setParameter(key,value);
		}
	}

	@Override
	public List<String> obtenerRolesAutorizador(){
		List<String> rolesAutorizador = new ArrayList<String>();
		Map<String,String> rolesAutorizadoresMap = autorizacionReservaService.obtenerRolesAutorizacion();
		Set<String> rolesSet = rolesAutorizadoresMap.keySet();
		Usuario usuarioActual = usuarioService.getUsuarioActual();
		for(String rol : rolesSet){
			if(usuarioService.tieneRol(rol, usuarioActual)){
				rolesAutorizador.add(rol);
			}
		}
		return rolesAutorizador;
	}
	
	private String creaOperadorPorCondicion(String condicion){
		String operador = "Sin Operador";
		if(condicion!=null){
			if(condicion.equalsIgnoreCase("IG")){
				operador = IGUAL;
			}else if(condicion.equalsIgnoreCase("MAY")){
				operador = MAYOR_QUE;
			}else if(condicion.equalsIgnoreCase("MAQ")){
				operador = MAYOR_IGUAL;
			}else if(condicion.equalsIgnoreCase("MEN")){
				operador = MENOR_QUE;
			}else if(condicion.equalsIgnoreCase("MEQ")){
				operador = MENOR_IGUAL;
			}
		}
		return operador;
	}
	


	private StringBuilder addCondition(StringBuilder query, String propiedad, String identificador, String operador , Object valor,Map<String,Object> parametersMap){
		StringBuilder queryAux = query;
		if(valor!=null){
			if(valor instanceof String && valor.equals("")){
					return queryAux;
			}
			if(queryAux.indexOf(WHERE_CONDITION, 0)<0){
				queryAux = queryAux.append(WHERE_CONDITION);
			}else{
				queryAux = queryAux.append(AND_CONDITION);
			}
			if(valor instanceof Date){
				queryAux = queryAux.append(" FUNC('trunc', "+propiedad+" ) "+operador+" :"+identificador);
				parametersMap.put(identificador, valor);
			}else if(operador.compareTo(LIKE) == 0){
				queryAux = queryAux.append(" UPPER(" + propiedad + ") " + operador + " UPPER('%" + valor + "%')");
			}else{
				queryAux = queryAux.append(" "+propiedad+" "+operador+" :"+identificador);
				parametersMap.put(identificador, valor);
			}
		}
		return queryAux;
	}
	
	private StringBuilder addConditionByRol(StringBuilder query, Map<String,Object> parametersMap, Date fechaIni, Date fechaFin ){
		StringBuilder queryAux = query;
		if(fechaIni != null || fechaFin  != null){
			if(queryAux.indexOf(WHERE_CONDITION, 0)<0){
				queryAux = queryAux.append(WHERE_CONDITION);
			}else{
				queryAux = queryAux.append(AND_CONDITION);
			}
			queryAux = queryAux.append(" ( ");
			queryAux = queryAux.append(" ( solicitud.estatusPrimeraAutorizacion = 'AUT' ");
			queryAux = addDateCondition(queryAux,  "solicitud.fechaEstatusPrimera", fechaIni, fechaFin);
			queryAux = queryAux.append(" ) ");
			
			queryAux = queryAux.append("OR ( solicitud.estatusSegundaAutorizacion = 'AUT' ");
			queryAux = addDateCondition(queryAux,  "solicitud.fechaEstatusSegunda", fechaIni, fechaFin);
			queryAux = queryAux.append(" ) ");
			
			queryAux = queryAux.append("OR ( solicitud.estatusTercerAutorizacion = 'AUT' ");
			queryAux = addDateCondition(queryAux,  "solicitud.fechaEstatusTercera", fechaIni, fechaFin);
			queryAux = queryAux.append(" ) ");
			
			queryAux = queryAux.append("OR ( solicitud.estatusCuartaAutorizacion = 'AUT' ");
			queryAux = addDateCondition(queryAux,  "solicitud.fechaEstatusCuarta", fechaIni, fechaFin);
			queryAux = queryAux.append(" ) ");
			
			queryAux = queryAux.append(" ) ");
			
			if(fechaIni!=null){
				parametersMap.put("fechaIni",fechaIni);
			}
			if(fechaFin!=null){
				parametersMap.put("fechaFin",fechaFin);
			}
		}
		return queryAux;
	}
	
	
	private StringBuilder addDateCondition(StringBuilder query, String property, Date fechaIni, Date fechaFin ){
		StringBuilder queryAux = query;
		if(fechaIni!=null){
			queryAux = queryAux.append(" AND "+property+" >= FUNC('trunc', :fechaIni) ");
		}
		if(fechaFin!=null){
			queryAux = queryAux.append(" AND "+property+" <= FUNC('trunc', :fechaFin) ");
		}
		return queryAux;
	}

	private StringBuilder addConditionSolicitudAutorizacion(StringBuilder query, String propiedad1, String identificador1, String propiedad2, String identificador2, String operador , Object valor,Map<String,Object> parametersMap){
		StringBuilder queryAux = query;
		if(valor!=null){
			if(valor instanceof String && valor.equals("")){
					return queryAux;
			}
			if(queryAux.indexOf(WHERE_CONDITION, 0)<0){
				queryAux = queryAux.append(WHERE_CONDITION);
			}else{
				queryAux = queryAux.append(AND_CONDITION);
			}
			if(operador.compareTo(LIKE) == 0){
				queryAux = queryAux.append(" ( UPPER(" + propiedad1 + ") " + operador + " UPPER('%" + valor + "%') OR  UPPER(" + propiedad2 + ") " + operador + " UPPER('%" + valor + "%') )");
			}else{
				queryAux = queryAux.append(" ( " + propiedad1 + " " + operador + " :" + identificador1 + " OR " + propiedad2 + " " + operador + " :" + identificador2 + " ) ");
				parametersMap.put(identificador1, valor);
				parametersMap.put(identificador2, valor);
			}
		}
		return queryAux;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SolicitudAutorizacion> buscar(SolicitudAutorizacionFiltro filtroSA){
		List<SolicitudAutorizacion> solicitudes = new ArrayList<SolicitudAutorizacion>();
		
		StringBuilder queryString	= new StringBuilder();
		Map<String,Object> parametersMap = new HashMap<String,Object>();
		
		queryString.append(" SELECT solicitud FROM SolicitudAutorizacion solicitud ");
		queryString.append(" LEFT JOIN solicitud.solicitudAutorizacionVigencia solicitudVigencia ");
		queryString.append(" LEFT JOIN solicitud.solicitudAutorizacionAjuste solicitudAjuste ");
		queryString.append(" LEFT JOIN solicitudAjuste.estimacionCobertura estimacionCobRepCab ");
		queryString.append(" LEFT JOIN estimacionCobRepCab.coberturaReporteCabina coberturaRepCab ");
		queryString.append(" LEFT JOIN coberturaRepCab.incisoReporteCabina incisoRepCab ");
		queryString.append(" LEFT JOIN incisoRepCab.seccionReporteCabina seccionRepCab ");
		queryString.append(" LEFT JOIN seccionRepCab.reporteCabina reporteCab ");
		queryString.append(" LEFT JOIN reporteCab.poliza pa ");
		queryString.append(" LEFT JOIN solicitudVigencia.poliza pv ");	
		
		queryString = this.addCondition(queryString, "solicitud.fechaCreacion", "fechaSolicitudDe", MAYOR_IGUAL, filtroSA.getFechaSolicitudDe(), parametersMap);
		queryString = this.addCondition(queryString, "solicitud.fechaCreacion", "fechaSolicitudA", MENOR_IGUAL, filtroSA.getFechaSolicitudA(), parametersMap);
		queryString = this.addCondition(queryString, "solicitud.codigoUsuarioCreacion", "usuarioCreacion", LIKE, filtroSA.getUsuarioSolicitante(), parametersMap);
		queryString = this.addCondition(queryString, "solicitud.estatus", "solicitudEstatus", IGUAL, filtroSA.getEstatus(), parametersMap);
		queryString = this.addCondition(queryString, "solicitud.codUsuarioAutorizador", "usuarioAutorizador", LIKE, filtroSA.getUsuarioAutorizador(), parametersMap);
		queryString = this.addCondition(queryString, "solicitud.tipoSolicitud", "tipoSolicitud", IGUAL, filtroSA.getTipoSolicitud(), parametersMap);
			
		// BUSQUEDA POR REPORTE DE CABINA PARA AUTORIZACIÓN DE VIGENCIA
		if( !StringUtil.isEmpty(filtroSA.getNoReporteCabina())) {
			String[] aNumReporteCabina = filtroSA.getNoReporteCabina().split("-");
			if( aNumReporteCabina.length == 3 ) {
				queryString = this.addCondition(queryString, "solicitudVigencia.oficina.claveOficina", "cveOficina", IGUAL, aNumReporteCabina[0], parametersMap);
				queryString = this.addCondition(queryString, "solicitudVigencia.reporte.consecutivoReporte", "consecutivoReporte", IGUAL, aNumReporteCabina[1], parametersMap);
				queryString = this.addCondition(queryString, "solicitudVigencia.reporte.anioReporte", "anioReporte", IGUAL, aNumReporteCabina[2], parametersMap);
			}
		}
		
		// BUSQUEDA DE NÚMERO DE FOLIO PARA AJUSTE DE RESERVA
		if( !StringUtil.isEmpty(filtroSA.getNoFolio())) {
			String[] aNumFolio = filtroSA.getNoFolio().split("-");
			if( aNumFolio.length == 3 ) {
				queryString = this.addCondition(queryString, "solicitudAjuste.estimacionCobertura.folio", "folio", IGUAL, filtroSA.getNoFolio(), parametersMap);
			}
		}		
		
		if(filtroSA.getNumeroPoliza() != null && !filtroSA.getNumeroPoliza().isEmpty()){
			String[] elementosNumPoliza = filtroSA.getNumeroPoliza().split("-");
			
			if(elementosNumPoliza.length == CANT_ELEMENTOS_NUMEROPOLIZA 
					&& StringUtils.isNumeric(elementosNumPoliza[1]) 
					&& StringUtils.isNumeric(elementosNumPoliza[2])){
				String codigoProducto 		=  StringUtils.rightPad(elementosNumPoliza[0].substring(0,2),POSICIONES_NUMPOLIZA);
				String codigoTipoPoliza 	= StringUtils.rightPad(elementosNumPoliza[0].substring(2),POSICIONES_NUMPOLIZA);
				int numeroPoliza 		= Integer.parseInt( elementosNumPoliza[1] );
				int numeroRenovacion 	= Integer.parseInt(  elementosNumPoliza[2]);
				
				queryString = this.addConditionSolicitudAutorizacion(queryString, "pv.codigoProducto", "codigoProducto1", 
						"pa.codigoProducto", "codigoProducto2", 
						IGUAL, codigoProducto, parametersMap);
				queryString = this.addConditionSolicitudAutorizacion(queryString, "pv.codigoTipoPoliza", "codigoTipoPol1", 
						"pa.codigoTipoPoliza", "codigoTipoPol2", 
						IGUAL, codigoTipoPoliza, parametersMap);
				queryString = this.addConditionSolicitudAutorizacion(queryString, "pv.numeroPoliza", "numPoliza1", 
						"pa.numeroPoliza", "numPoliza2", 
						LIKE, numeroPoliza, parametersMap);
				queryString = this.addConditionSolicitudAutorizacion(queryString, "pv.numeroRenovacion", "numRenovacion1", 
						"pa.numeroRenovacion", "numRenovacion2", 
						IGUAL, numeroRenovacion, parametersMap);
			}else if(StringUtils.isNumeric(filtroSA.getNumeroPoliza())){
				queryString = this.addConditionSolicitudAutorizacion(queryString, "pv.numeroPoliza", "numPoliza1", 
						"pa.numeroPoliza", "numPoliza2", 
						LIKE, filtroSA.getNumeroPoliza(), parametersMap);
			}else{
				return solicitudes;
			}
		}
			
		queryString = this.addConditionSolicitudAutorizacion(queryString, "solicitudVigencia.id", "idSolicitud1", 
				"solicitudAjuste.id", "idSolicitud2", IGUAL, filtroSA.getNumeroSolicitud(), parametersMap);
		queryString = this.addConditionSolicitudAutorizacion(queryString, "solicitudVigencia.oficina.id", "oficina1", 
				"reporteCab.oficina.id", "oficina2", 
				IGUAL, filtroSA.getOficina(), parametersMap);
		
		
		queryString.append(" ORDER BY solicitud.id DESC");
		Query query = this.entityManager.createQuery(queryString.toString());
		this.addParametersToQuery(query, parametersMap);
		solicitudes = query.getResultList();
		
		return solicitudes;
		
	}


}
