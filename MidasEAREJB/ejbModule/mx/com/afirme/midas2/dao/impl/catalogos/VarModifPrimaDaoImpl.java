package mx.com.afirme.midas2.dao.impl.catalogos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.catalogos.VarModifPrimaDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima_;
import mx.com.afirme.midas2.domain.catalogos.VarModifPrima;
import mx.com.afirme.midas2.domain.catalogos.VarModifPrima_;

@Stateless
public class VarModifPrimaDaoImpl extends JpaDao<Long, VarModifPrima> implements
VarModifPrimaDao {

	public List<VarModifPrima> findByFilters(VarModifPrima filtroTcVarModifPrima) {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			Integer intNull = null;
			Expression<Long> claveExp = cb.literal(filtroTcVarModifPrima.getId());
			Expression<Integer> intNullExp = cb.literal(intNull);
		
			CriteriaQuery<VarModifPrima> criteriaQuery = cb.createQuery(entityClass);
			Root<VarModifPrima> root = criteriaQuery.from(VarModifPrima.class);
			Predicate predicado = cb.and(cb.or(cb.equal(claveExp, intNullExp),cb.equal(root.get(VarModifPrima_.id), filtroTcVarModifPrima.getId())),
		    cb.like(cb.upper(root.get(VarModifPrima_.valor)),"%" + filtroTcVarModifPrima.getValor() + "%"));		
			 criteriaQuery.where(predicado);
			
			TypedQuery<VarModifPrima> query = entityManager.createQuery(criteriaQuery);
			return query.getResultList();
		
	}

	
	public List<VarModifPrima> findByGrupoModifPrima(Long idGrupo) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
	
		CriteriaQuery<VarModifPrima> criteriaQuery = cb.createQuery(entityClass);
		Root<VarModifPrima> root = criteriaQuery.from(VarModifPrima.class);
		Predicate predicado = cb.equal(root.get(VarModifPrima_.grupo).get(GrupoVariablesModificacionPrima_.id), idGrupo);
					
		 criteriaQuery.where(predicado);
		
		TypedQuery<VarModifPrima> query = entityManager.createQuery(criteriaQuery);
		
		return query.getResultList();
	}
}
