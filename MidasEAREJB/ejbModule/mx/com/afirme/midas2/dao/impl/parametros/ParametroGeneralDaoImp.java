package mx.com.afirme.midas2.dao.impl.parametros;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas2.dao.parametros.ParametroGeneralDao;

@Stateless
public class ParametroGeneralDaoImp implements ParametroGeneralDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public ParametroGeneralDTO findById(ParametroGeneralId id) {
		
		ParametroGeneralDTO parametroGeneral = entityManager.find(ParametroGeneralDTO.class, id);		
		entityManager.refresh(parametroGeneral);
		
		return parametroGeneral;
	}

}
