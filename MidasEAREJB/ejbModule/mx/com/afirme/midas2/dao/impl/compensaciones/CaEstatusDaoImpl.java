/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaEstatusDao;
import mx.com.afirme.midas2.domain.compensaciones.CaEstatus;

import org.apache.log4j.Logger;

@Stateless
public class CaEstatusDaoImpl implements CaEstatusDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaEstatusDaoImpl.class);
	
	public void save(CaEstatus entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaEstatus 	::		CaEstatusDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaEstatus 	::		CaEstatusDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaEstatus 	::		CaEstatusDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaEstatus entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaEstatus 	::		CaEstatusDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaEstatus.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaEstatus 	::		CaEstatusDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaEstatus 	::		CaEstatusDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaEstatus update(CaEstatus entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaEstatus 	::		CaEstatusDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaEstatus result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaEstatus 	::		CaEstatusDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaEstatus 	::		CaEstatusDaoImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaEstatus findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaEstatusDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaEstatus instance = entityManager.find(CaEstatus.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaEstatusDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaEstatusDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<CaEstatus> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaEstatusDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaEstatus model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);			
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaEstatusDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaEstatusDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaEstatus> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaEstatus> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaEstatus> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaEstatus> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaEstatus> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaEstatusDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaEstatus model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaEstatusDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		|	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}	 
}
