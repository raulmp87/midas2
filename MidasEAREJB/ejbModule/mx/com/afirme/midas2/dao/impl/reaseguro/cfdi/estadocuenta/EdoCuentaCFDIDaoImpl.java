package mx.com.afirme.midas2.dao.impl.reaseguro.cfdi.estadocuenta;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dao.reaseguro.cfdi.estadocuenta.EdoCuentaCFDIDao;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.Contrato;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.Folio;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta.EstadoCuenta;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class EdoCuentaCFDIDaoImpl implements EdoCuentaCFDIDao {
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaComprobanteEC(String serie, BigDecimal folio, String descripcionContrato, String descripcionSuscripcion, String descripcionRamo
			, Date fechaInicioPeriodo, Date fechaFinPeriodo, String descripcionMoneda, BigDecimal idReasCoas, BigDecimal total) {

		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.SP_INS_COMPEC", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pSerie", serie);
			storedHelper.estableceParametro("pFolioInterno", folio);
			storedHelper.estableceParametro("pContrato", descripcionContrato);
			storedHelper.estableceParametro("pSuscripcion", descripcionSuscripcion);
			storedHelper.estableceParametro("pRamo", descripcionRamo);
			storedHelper.estableceParametro("pFechaIni", fechaInicioPeriodo);
			storedHelper.estableceParametro("pFechaFin", fechaFinPeriodo);
			storedHelper.estableceParametro("pMoneda", descripcionMoneda);
			storedHelper.estableceParametro("pIdReasCoas", idReasCoas);
			storedHelper.estableceParametro("pTotal", total);
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaConceptoEC(String serie, Folio folio, Integer secuencia, String textoPeriodoAnterior, String descripcionConcepto
			, BigDecimal debe, BigDecimal haber, BigDecimal saldo) {
		
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.SP_INS_CONCEPTOEC", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pSerie", serie);
			storedHelper.estableceParametro("pFolioInterno", folio.getFolio());
			storedHelper.estableceParametro("pSecuencia", secuencia);
			storedHelper.estableceParametro("pPeriodoAnterior", textoPeriodoAnterior);
			storedHelper.estableceParametro("pDescConcepto", descripcionConcepto);
			storedHelper.estableceParametro("pDebe", debe);
			storedHelper.estableceParametro("pHaber", haber);
			storedHelper.estableceParametro("pSaldo", saldo);
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		

	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaConceptoFiscal(String serie, Folio folio, String descripcionConcepto, BigDecimal valor) {
		
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.SP_INS_CONCEPTO", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pSerie", serie);
			storedHelper.estableceParametro("pFolioInterno", folio.getFolio());
			storedHelper.estableceParametro("pDescConcepto", descripcionConcepto);
			storedHelper.estableceParametro("pValor", valor);
					
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		

	}

	@Override
	public void generaSolicitudCancelacionCFDI(String llaveComprobante) {
	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.SP_CANCELA_CFDI", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pLlaveComprobante", llaveComprobante);
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<EstadoCuenta> filtrar(Contrato contrato, EstadoCuenta estadoCuenta, BigDecimal folio) {
		
		String sWhere = "";
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String queryString = "SELECT ec FROM EstadoCuenta ec " ;
		
		if(contrato.getId() != null && !contrato.getId().equals(BigDecimal.ZERO)) {
			if(!sWhere.trim().equals("")) {
				sWhere += " AND ";
			}
			sWhere += "ec.contratoHistorico.idOrigen = :idOrigen";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "idOrigen", contrato.getId());
		}
		
		if(estadoCuenta.getSubRamo().getId() != null && estadoCuenta.getSubRamo().getId().compareTo(BigDecimal.ZERO) > 0) {
			if(!sWhere.trim().equals("")) {
				sWhere += " AND ";
			}
			sWhere += "ec.subRamo.id = :idSubRamo";
			Utilerias.agregaHashLista(listaParametrosValidos, "idSubRamo", estadoCuenta.getSubRamo().getId());
		}
		
		if(estadoCuenta.getNombreReasCorr() != null && !estadoCuenta.getNombreReasCorr().equals("0")) {
			if(!sWhere.trim().equals("")) {
				sWhere += " AND ";
			}
			
			sWhere += "(ec.idReaseguradorCoaseguradorHist IN " +
			" (SELECT r.id FROM ReaseguradorCorredorDTOHistorico r WHERE r.nombre = '" + estadoCuenta.getNombreReasCorr() + "')" +
			" OR ec.idReaseguradorCoaseguradorHist IN " +
			" (SELECT c.id FROM CoaseguradorHistorico c WHERE c.nombre = '" + estadoCuenta.getNombreReasCorr() + "'))";
			
			//Utilerias.agregaHashLista(listaParametrosValidos, "nombre", "'" + estadoCuenta.getNombreReasCorr() + "'");
		}
		
		if(estadoCuenta.getIdReaseguradorCoaseguradorHist() != null && estadoCuenta.getIdReaseguradorCoaseguradorHist().compareTo(BigDecimal.ZERO) > 0) {
			if(!sWhere.trim().equals("")) {
				sWhere += " AND ";
			}
			sWhere += "ec.idReaseguradorCoaseguradorHist = :idReaseguradorCoaseguradorHist";
			Utilerias.agregaHashLista(listaParametrosValidos, "idReaseguradorCoaseguradorHist", estadoCuenta.getIdReaseguradorCoaseguradorHist());
		}
		
		if(estadoCuenta.getMoneda() != null && estadoCuenta.getMoneda().intValue() > 0) {
			if(!sWhere.trim().equals("")) {
				sWhere += " AND ";
			}
			sWhere += "ec.moneda = :moneda";
			Utilerias.agregaHashLista(listaParametrosValidos, "moneda", estadoCuenta.getMoneda());
		}
		
		if(estadoCuenta.getFechaInicioPeriodo() != null) {
			if(!sWhere.trim().equals("")) {
				sWhere += " AND ";
			}
			sWhere += "ec.fechaInicioPeriodo = :fechaInicioPeriodo";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaInicioPeriodo", estadoCuenta.getFechaInicioPeriodo());
		}
		
		if(estadoCuenta.getFechaFinPeriodo() != null) {
			if(!sWhere.trim().equals("")) {
				sWhere += " AND ";
			}
			sWhere += "ec.fechaFinPeriodo = :fechaFinPeriodo";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFinPeriodo", estadoCuenta.getFechaFinPeriodo());
		}
		
		if(folio != null) {
			if(!sWhere.trim().equals("")) {
				sWhere += " AND ";
			}
			sWhere += ":folio = ANY (SELECT f.folio FROM ec.folios f)";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "folio", folio);
			
		} 
				
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			queryString = queryString.concat(" WHERE ").concat(sWhere);				
		}
		
		queryString = queryString.concat(" GROUP BY ec ");
		
		System.out.println("JECL- QUERY EC : " + queryString);
				
		Query query = entityManager.createQuery(queryString);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();

	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Integer validaFolio (BigDecimal estadoCuentaId, Integer esEgreso, BigDecimal folio) {
		
		Query query = null;
		
		if (folio != null) {
			
			query = prepareQueryValidaFolio(folio);
		
		} else {
			
			query = prepareQueryValidaFolio(estadoCuentaId, esEgreso);
			
		}
				
		return ((BigDecimal)query.getSingleResult()).intValue();
		
	}
	
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EstadoCuenta guardaEstadoCuenta (EstadoCuenta estadoCuenta) {
		
		if (estadoCuenta.getId() == null) {
			
			estadoCuenta.setId((BigDecimal)entidadDao.persistAndReturnId(estadoCuenta));
			estadoCuenta = entidadDao.getReference(EstadoCuenta.class, estadoCuenta.getId());
		
		} else {
			
			estadoCuenta = entidadDao.update(estadoCuenta);
							
		}
				
		return estadoCuenta;
		
	}
	
	
	private Query prepareQueryValidaFolio(BigDecimal folio) {
		
		/*
		
		SELECT  count(1) 
        FROM 
        MIDAS.CFDI_EC_REAS ec 
        inner join (MIDAS.CFDI_FOLIO folio 
                    inner join (WFACTURA.WFACTURAS factura 
                                left outer join WFACTURA.CANCELACIONES canc 
                                on canc.uuid = factura.uuid 
                    )on factura.SERIE = 'EC' and factura.FOLIO = folio.FOLIO 
        )on folio.CFDI_EC_REAS_ID = ec.id  
        WHERE 
         folio.FOLIO = :folio
         and (canc.ESTATUS IS NULL or canc.ESTATUS <> 201);
				
		*/
		
		StringBuilder sb = new StringBuilder();
		
		sb
		.append(" SELECT  count(1)")
		.append(" FROM")
		.append(" MIDAS.CFDI_EC_REAS ec")
		.append(" inner join (MIDAS.CFDI_FOLIO folio")
		.append(" inner join (SELECT factura.SERIE,FACTURA.FOLIO,canc.ESTATUS FROM")
		.append(" WFACTURA.WFACTURAS factura")
		.append(" left outer join WFACTURA.CANCELACIONES canc")
		.append(" on canc.uuid = factura.uuid")
		.append(" where (factura.FOLIO IS NOT NULL and (canc.ESTATUS IS NULL or canc.ESTATUS <> 201))")
		.append(" UNION")
		.append(" SELECT facturaAFI.SERIE,facturaAFI.FOLIO,cancAFI.ESTATUS FROM")
		.append(" AFIFACTELEC.tbl_facturas facturaAFI")
		.append(" left outer join AFIFACTELEC.tbl_cancelaciones cancAFI")
		.append(" on cancAFI.uuid = facturaAFI.uuid")
		.append(" where (facturaAFI.FOLIO IS NOT NULL and (cancAFI.ESTATUS IS NULL or cancAFI.ESTATUS <> 201))")
		.append(" )factura")
		.append(" on factura.SERIE = 'EC' and factura.FOLIO = folio.FOLIO")
		.append(" )on folio.CFDI_EC_REAS_ID = ec.id")
		.append(" WHERE")
		.append(" folio.FOLIO = ?1");
				
		Query query = entityManager.createNativeQuery(sb.toString());
		
		query.setParameter(1, folio);
		
		return query;
				
	}
	
	private Query prepareQueryValidaFolio(BigDecimal estadoCuentaId, Integer esEgreso) {
		
		/*
		
		SELECT  count(1) 
        FROM 
        MIDAS.CFDI_EC_REAS ec 
        inner join (MIDAS.CFDI_FOLIO folio 
                    left outer join (WFACTURA.WFACTURAS factura 
                                left outer join WFACTURA.CANCELACIONES canc 
                                on canc.uuid = factura.uuid 
                    )on factura.SERIE = 'EC' and factura.FOLIO = folio.FOLIO 
                    left outer join WFACTURA.COMPROBANTE comprobante
                    on comprobante.SERIE = 'EC' and comprobante.FOLIO = folio.FOLIO 
        )on folio.CFDI_EC_REAS_ID = ec.id  
        WHERE 
         ec.id =  :estadoCuentaId  and folio.es_egreso =  :esEgreso
         and 
        (
            (factura.FOLIO IS NOT NULL 
                and (canc.ESTATUS IS NULL or canc.ESTATUS <> 201))
            or 
                comprobante.FOLIO IS NOT NULL
        );
		
		
		*/
		
		StringBuilder sb = new StringBuilder();
		
		sb
		.append("SELECT  count(1)")
		.append(" FROM")
		.append(" MIDAS.CFDI_EC_REAS ec")
		.append(" inner join (MIDAS.CFDI_FOLIO folio")
		.append(" left outer join (SELECT factura.SERIE,FACTURA.FOLIO,canc.ESTATUS FROM")
		.append(" WFACTURA.WFACTURAS factura")
		.append(" left outer join WFACTURA.CANCELACIONES canc")
		.append(" on canc.uuid = factura.uuid")
		.append(" UNION")
		.append(" SELECT facturaAFI.SERIE,facturaAFI.FOLIO,cancAFI.ESTATUS FROM")
		.append(" AFIFACTELEC.tbl_facturas facturaAFI")
		.append(" left outer join AFIFACTELEC.tbl_cancelaciones cancAFI")
		.append(" on cancAFI.uuid = facturaAFI.uuid")
		.append(" ) factura")
		.append(" on factura.SERIE = 'EC' and factura.FOLIO = folio.FOLIO")
		.append(" left outer join WFACTURA.COMPROBANTE comprobante")
		.append(" on comprobante.SERIE = 'EC' and comprobante.FOLIO = folio.FOLIO")
		.append(" )on folio.CFDI_EC_REAS_ID = ec.id")
		.append(" WHERE")
		.append(" ec.id =  ?1  and folio.es_egreso =  ?2")
		.append(" and (")
		.append(" (factura.FOLIO IS NOT NULL") 
		.append(" and (factura.ESTATUS IS NULL or factura.ESTATUS <> 201))")
		.append(" or")
		.append(" comprobante.FOLIO IS NOT NULL")
		.append(" )");
		
				
		Query query = entityManager.createNativeQuery(sb.toString());
		
		query.setParameter(1, estadoCuentaId);
		query.setParameter(2, esEgreso);
		
		return query;
				
	}
	
	
	@EJB
	private EntidadHistoricoDao entidadDao;

	@PersistenceContext
	private EntityManager entityManager;
	
	
}
