/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoEmisorDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEmisor;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoEmisorDaoImpl implements CaTipoEmisorDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoEmisorDaoImpl.class);

	public void save(CaTipoEmisor entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaTipoEmisor entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoEmisor.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }

    public CaTipoEmisor update(CaTipoEmisor entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoEmisor result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoEmisor 	::		CaTipoEmisorDaoImpl	::	update	::	ERROR	::	",re);
	        throw re;
        }
    }
    
    public CaTipoEmisor findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoEmisorDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoEmisor instance = entityManager.find(CaTipoEmisor.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoEmisorDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoEmisorDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoEmisor> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoEmisorDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoEmisor model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoEmisorDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoEmisorDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoEmisor> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoEmisor> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoEmisor> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoEmisor> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoEmisor> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoEmisorDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoEmisor model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoEmisorDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoEmisorDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
