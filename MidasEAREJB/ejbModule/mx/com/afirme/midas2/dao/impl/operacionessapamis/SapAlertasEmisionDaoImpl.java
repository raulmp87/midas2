package mx.com.afirme.midas2.dao.impl.operacionessapamis;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasEmisionDao;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasEmision;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class SapAlertasEmisionDaoImpl implements SapAlertasEmisionDao {	
	private EntidadService entidadService;
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void guardarAlertasEmision(List<SapAlertasEmision> alertasEmision) {
		entidadService.saveAll(alertasEmision);
	}
}