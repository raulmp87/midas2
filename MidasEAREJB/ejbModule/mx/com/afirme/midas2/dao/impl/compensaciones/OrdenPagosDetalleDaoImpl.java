package mx.com.afirme.midas2.dao.impl.compensaciones;
// default package

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.compensaciones.OrdenPagosDetalleDao;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
/**
 * Facade for entity OrdenPagosDetalle.
 * 
 * @see .OrdenPagosDetalle
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class OrdenPagosDetalleDaoImpl  implements OrdenPagosDetalleDao{
	// property constants
	public static final String IDTOPOLIZA = "idtopoliza";
	public static final String ID_RECIBO = "idRecibo";
	public static final String NOMBRE_BENEFICIARIO = "nombreBeneficiario";
	public static final String CLAVE_NOMBRE = "claveNombre";
	public static final String NOMBRE_CONTRATANTE = "nombreContratante";
	public static final String IMPORTE_PRIMA = "importePrima";
	public static final String IMPORTE_BS = "importeBs";
	public static final String IMPORTE_DERPOL = "importeDerpol";
	public static final String IMPORTE_CUM_META = "importeCumMeta";
	public static final String IMPORTE_UTILIDAD = "importeUtilidad";
	public static final String SUBTOTAL = "subtotal";
	public static final String IVA = "iva";
	public static final String IVA_RETENIDO = "ivaRetenido";
	public static final String ISR = "isr";
	public static final String ESTATUS_FACTURA = "estatusFactura";
	public static final String ESTATUS_ORDENPAGO = "estatusOrdenpago";
	public static final String ESTATUS_ORDPAG_GEN = "estatusOrdpagGen";
	public static final String IMPORTE_TOTAL = "importeTotal";

	@PersistenceContext
	private EntityManager entityManager;

	private static final Logger LOGGER = Logger.getLogger(OrdenPagosDetalleDaoImpl.class);
	
	public void save(OrdenPagosDetalle entity) {
		LOGGER.debug(">>Guardando OrdenPagosDetalle	");
		try {
			entityManager.persist(entity);
			LOGGER.debug("Se Guardo OrdenPagosDetalle");
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al guardar OrdenPagosDetalle ",
					re);
			throw re;
		}
	}

	/**
	 * Delete a persistent OrdenPagosDetalle entity.
	 * 
	 * @param entity
	 *            OrdenPagosDetalle entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(OrdenPagosDetalle entity) {
		LOGGER.debug(">>Eliminando OrdenPagosDetalle");
		try {
			entity = entityManager.getReference(OrdenPagosDetalle.class,
					entity.getId());
			entityManager.remove(entity);
			LOGGER.debug("Se Elimino OrdenPagosDetalle ");
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al eliminar OrdenPagosDetalle ",
					re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved OrdenPagosDetalle entity and return it or a
	 * copy of it to the sender. A copy of the OrdenPagosDetalle entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            OrdenPagosDetalle entity to update
	 * @return OrdenPagosDetalle the persisted OrdenPagosDetalle entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public OrdenPagosDetalle update(OrdenPagosDetalle entity) {
		LOGGER.debug(">>Actualizando OrdenPagosDetalle update()");
		try {
			entity.setFechaModificacion(new Date());
			OrdenPagosDetalle result = entityManager.merge(entity);
			LOGGER.debug("Se Actualizo OrdenPagosDetalle	");
			return result;
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al actualizar OrdenPagosDetalle",
					re);
			throw re;
		}
	}

	public OrdenPagosDetalle findById(Long id) {
		LOGGER.debug("	Buscando por Id ");
				
		try {
			OrdenPagosDetalle instance = entityManager.find(
					OrdenPagosDetalle.class, id);
			LOGGER.debug("	Se busco por Id ");
			return instance;
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al buscar por Id ",
					re);
			throw re;
		}
	}

	/**
	 * Find all OrdenPagosDetalle entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the OrdenPagosDetalle property to query
	 * @param value
	 *            the property value to match
	 * @return List<OrdenPagosDetalle> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<OrdenPagosDetalle> findByProperty(String propertyName,
			final Object value) {
		LOGGER.debug(">>Buscando por Propiedad 	findByProperty");
		try {
			final String queryString = "select model from OrdenPagosDetalle model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error(
					"Error al buscar por Propiedad",
					re);
			throw re;
		}
	}

	/**
	 * Find all OrdenPagosDetalle entities.
	 * 
	 * @return List<OrdenPagosDetalle> all OrdenPagosDetalle entities
	 */
	@SuppressWarnings("unchecked")
	public List<OrdenPagosDetalle> findAll() {
		LOGGER.debug(">>buscar todo()");
		try {
			final String queryString = "select model from OrdenPagosDetalle model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error(
					" Error al buscar todo ",
					re);
			throw re;
		}
	}
	public  List<OrdenPagosDetalle >  obtenerOrdenPagosDetalleProcesoPorId(Long liquidacionId){ 
		LOGGER.debug("obtenerOrdenPagosDetalleProcesoPorId() identificadorBenId => {}"+liquidacionId);
	    List<OrdenPagosDetalle > lista = new ArrayList<OrdenPagosDetalle>();
	    try {
	    	lista=obtenerOrdenPagosDetallePorId(liquidacionId,false);
	    } catch (RuntimeException re) {
	    	LOGGER.error("-- obtenerOrdenPagosDetalleProcesoPorId()", re);
	        throw re; 
	    }    
	    return lista;
	}
	
	public  List<OrdenPagosDetalle >  obtenerOrdenPagosDetalleGeneradoPorId(Long folio){ 
		LOGGER.debug("obtenerOrdenPagosDetalleGeneradoPorId() folio => {"+folio+"}");
	    List<OrdenPagosDetalle > lista = new ArrayList<OrdenPagosDetalle>();
	    try {
	    	lista=obtenerOrdenPagosDetallePorId(folio,true);
	    } catch (RuntimeException re) {
	    	LOGGER.error("-- obtenerOrdenPagosDetalleGeneradoPorId()", re);
	        throw re; 
	    }    
	    return lista;
	}
	
	@SuppressWarnings("unchecked")
	private  List<OrdenPagosDetalle>  obtenerOrdenPagosDetallePorId(Long liquidacionId,Boolean soloGenerados){  
		LOGGER.debug("obtenerOrdenPagosDetallePorId() folio => {"+liquidacionId+"}  soloGenerados =>{"+soloGenerados+"}");
	    List<OrdenPagosDetalle > lista = new ArrayList<OrdenPagosDetalle>();
	    try {
	      final StringBuilder queryString = new StringBuilder(200);
	      queryString.append(" SELECT ORDD.LIQUIDACION_ID AS ID, ORDD.ID_RECIBO,ORDD.RAMO_ID, lpad(TO_CHAR(POL.id_centro_emis),3,0) ");
	      queryString.append(" || '-'||lpad(TO_CHAR(POL.num_poliza),12,0)|| '-' || lpad(TO_CHAR(POL.num_renov_pol),2,0) AS POLIZA, ORDD.ESTATUS_ORDPAG_GEN, POLREC.NUM_FOLIO_RBO, ");
	      queryString.append("  SUM(NVL(ORDD.PORC_ASIG_SINIESTRALIDAD,0)), SUM(NVL(ORDD.MONTO_ASIG_BANCA,0)),SUM(ORDD.IMPORTE_PRIMA + ORDD.IMPORTE_BS) AS IMPORTE_COMP");
	      queryString.append(" FROM MIDAS.CA_ORDENPAGOS_DETALLE ORDD ");
		  queryString.append(" INNER JOIN MIDAS.ca_compensacion CO ON (ORDD.COMPENSACION_ID=CO.ID) ");
		  queryString.append(" INNER JOIN MIDAS.ca_ramo RM ON (CO.ramo_id = rm.id) ");
		  queryString.append("INNER JOIN SEYCOS.POL_POLIZA POL ON (POL.ID_COTIZACION = ORDD.ID_COTIZACION)");
		  queryString.append(" INNER JOIN MIDAS.ca_entidadpersona EP ON (EP.compensacion_id   = CO.id AND ORDD.ENTIDADPERSONA_ID=EP.ID) ");
		  queryString.append(" INNER JOIN MIDAS.ca_tipoentidad TE ON (EP.tipoentidad_id = TE.id AND TE.ID = ORDD.TIPOENTIDAD_ID) ");
		  queryString.append(" INNER JOIN MIDAS.CA_TIPOCOMPENSACION TIPOC ON(TIPOC.ID =ORDD.TIPOCOMPENSACION_ID AND TIPOC.ID=ORDD.TIPOCOMPENSACION_ID) ");
		  queryString.append(" INNER JOIN seycos.pol_recibo POLREC ON( POLREC.ID_RECIBO  =ORDD.ID_RECIBO ");
		  queryString.append(" AND POLREC.ID_COTIZACION = ORDD.ID_COTIZACION ");
		  queryString.append(" AND POLREC.f_ter_reg = to_date ('31/12/4712','dd/MM/yyyy'))");
		  queryString.append(" AND ORDD.LIQUIDACION_ID = ?1 ");
		  queryString.append(" GROUP BY ORDD.LIQUIDACION_ID, ORDD.ID_RECIBO,ORDD.RAMO_ID, lpad(TO_CHAR(POL.id_centro_emis),3,0)");
		  queryString.append(" || '-'||lpad(TO_CHAR(POL.num_poliza),12,0)|| '-' || lpad(TO_CHAR(POL.num_renov_pol),2,0), ORDD.ESTATUS_ORDPAG_GEN, POLREC.NUM_FOLIO_RBO ");
		  queryString.append(" ORDER BY ORDD.ID_RECIBO ");		  
		  Query query=entityManager.createNativeQuery(queryString.toString());
	      query.setParameter(1, liquidacionId);
	      LOGGER.debug("OrdenPagosDetalle size="+lista.size());
	      LOGGER.debug("query="+query.toString());
	      query.setMaxResults(100); 
	      
	      List<Object[]> resultList=query.getResultList();
	      for (Object[] result : resultList){ 
              OrdenPagosDetalle ordenTmp= new OrdenPagosDetalle();
              ordenTmp.setId(new Long(((BigDecimal)result[0]).toString()));
              ordenTmp.setIdRecibo(new Long(((BigDecimal)result[1]).toString()));
              ordenTmp.setCaRamo(new CaRamo(result[2].toString(),null,null,null,null,null,null));
              ordenTmp.setNumeroPoliza(result[3].toString());         
              ordenTmp.setEstatusOrdpagGen(new Short(((BigDecimal)result[4]).toString()));
              ordenTmp.setNumFolioRbo(new Long(((BigDecimal)result[5]).toString()));
              ordenTmp.setPorcentajeSiniestralidad(new Double(result[6].toString()));
              ordenTmp.setMontoBanca(new Double(result[7].toString()));
              ordenTmp.setImporteCompensacion(new Double(result[8].toString()));
                lista.add(ordenTmp);
              }
	    } catch (RuntimeException re) {
	    	LOGGER.error("-- error obtenerOrdenPagosDetallePorId()", re);
	        throw re; 
	    }    
	    
	    return lista;
	  }
	public Boolean excluirRecibosOrdenPagosDetallePorId(Long ordenPagoId ,Long folioId ){
		LOGGER.debug("excluirRecibosobtenerOrdenPagosDetallePorId() OrdenPagoId => {"+ordenPagoId+"} reciboId => {"+folioId+"}");
		final StringBuilder queryString=new StringBuilder("");
			try{
				queryString.append(" UPDATE  MIDAS.CA_ORDENPAGOS_DETALLE entidad ");
				queryString.append(" SET entidad.FECHA_MODIFICACION = SYSDATE,entidad.ESTATUS_ORDPAG_GEN=");
				queryString.append(OrdenPagosDetalle.ESTATUS_ORDPAG_GEN_EXCLUIDO);
				queryString.append(" where ");
				queryString.append(" entidad.ID_RECIBO=");
				queryString.append(ordenPagoId);	
				queryString.append(" and  ");
				queryString.append(" entidad.FOLIO=");
				queryString.append(folioId);
				queryString.append(" and  ");
				queryString.append(" entidad.ESTATUS_ORDPAG_GEN=");
				queryString.append(OrdenPagosDetalle.ESTATUS_ORDPAG_GEN_INCLUIDO);
			    String finalQuery=queryString.toString();
				Query query=entityManager.createNativeQuery(finalQuery);
				query.executeUpdate();
				return true;
			
			}catch (RuntimeException re) {
			LOGGER.error("-- excluirRecibosobtenerOrdenPagosDetallePorId()", re);
			throw re;
		}
	}
	public Boolean incluirRecibosOrdenPagosDetallePorId(Long ordenPagoId ,Long folioId ){
		LOGGER.debug("excluirRecibosobtenerOrdenPagosDetallePorId() OrdenPagoId => {"+ordenPagoId+"} folioId => {"+folioId+"}");
		final StringBuilder queryString=new StringBuilder("");
		try{
				queryString.append(" UPDATE  MIDAS.CA_ORDENPAGOS_DETALLE entidad ");
				queryString.append(" SET entidad.FECHA_MODIFICACION = SYSDATE,entidad.ESTATUS_ORDPAG_GEN=");
				queryString.append(OrdenPagosDetalle.ESTATUS_ORDPAG_GEN_INCLUIDO);
				queryString.append(" where ");
				queryString.append(" entidad.ID_RECIBO=");
				queryString.append(ordenPagoId);
				queryString.append(" and  ");
				queryString.append(" entidad.FOLIO=");
				queryString.append(folioId);
				queryString.append(" and  ");
				queryString.append(" entidad.ESTATUS_ORDPAG_GEN=");
				queryString.append(OrdenPagosDetalle.ESTATUS_ORDPAG_GEN_EXCLUIDO);
			    String finalQuery=queryString.toString();
				Query query=entityManager.createNativeQuery(finalQuery);
				query.executeUpdate();
				return true;
			
		} catch (RuntimeException re) {
			LOGGER.error("-- excluirRecibosobtenerOrdenPagosDetallePorId()", re);
			throw re;
		}
	}

	public Boolean actualizarEstatusOrdenPagosDetalle(Long folio){
		LOGGER.debug("actualizarEstatusOrdenPagosDetalle() OrdenPagoId => {"+folio+"}");
		final StringBuilder queryString=new StringBuilder("");
		try{
			queryString.append(" UPDATE  MIDAS.CA_ORDENPAGOS_DETALLE entidad ");
			queryString.append(" SET entidad.FECHA_MODIFICACION = SYSDATE,entidad.ESTATUS_ORDPAG_GEN=");
			queryString.append(OrdenPagosDetalle.ESTATUS_ORDPAG_GEN_INCLUIDO);
			queryString.append(" where ");
			queryString.append(" entidad.folio=");
			queryString.append(folio);
			queryString.append(" and  ");
			queryString.append(" entidad.ESTATUS_ORDPAG_GEN=");
			queryString.append(OrdenPagosDetalle.ESTATUS_ORDPAG_GEN_PROCESO);
		    String finalQuery=queryString.toString();
			Query query=entityManager.createNativeQuery(finalQuery);
			query.executeUpdate();
			return true;
		
	} catch (RuntimeException re) {
		LOGGER.error("-->actualizarEstatusOrdenPagosDetalle()", re);
		throw re;
	}
		
	}
	
	public Boolean actualizarEstatusOrdenPagosDetalleAGenerado(Long folio){
		LOGGER.debug("actualizarEstatusOrdenPagosDetalle() OrdenPagoId => {"+folio+"}");
		final StringBuilder queryString=new StringBuilder("");
		try{
			queryString.append(" UPDATE  MIDAS.CA_ORDENPAGOS_DETALLE entidad ");
			queryString.append(" SET entidad.FECHA_MODIFICACION = SYSDATE,entidad.ESTATUS_ORDPAG_GEN=");
			queryString.append(OrdenPagosDetalle.ESTATUS_ORDPAG_GEN_GENERADO);
			queryString.append(" ,entidad.estatus_ordenpago= ");
			queryString.append(OrdenPagosDetalle.ESTATUS_ORDENPAGO_VALIDA);
			queryString.append(" where ");
			queryString.append(" entidad.FOLIO=");
			queryString.append(folio);
			queryString.append(" and  ");
			queryString.append(" entidad.ESTATUS_ORDPAG_GEN=");
			queryString.append(OrdenPagosDetalle.ESTATUS_ORDPAG_GEN_INCLUIDO);
		    String finalQuery=queryString.toString();
			Query query=entityManager.createNativeQuery(finalQuery);
			query.executeUpdate();
			return true;
		
	} catch (RuntimeException re) {
		LOGGER.error("-->actualizarEstatusOrdenPagosDetalle()", re);
		throw re;
	}
		
	}
	
	@SuppressWarnings("unchecked")
	public List<OrdenPagosDetalle> obtenerHonorariosOrdenPagos(Long idParametro, String estatusRecibo){  
		LOGGER.debug("obtenerHonorariosOrdenPagos() idAgente => {"+idParametro+"}  estatusRecibo =>{"+estatusRecibo+"}");
	    List<OrdenPagosDetalle > lista = new ArrayList<OrdenPagosDetalle>();
	    try {
	      final StringBuilder queryString = new StringBuilder(200);
	      queryString.append(" SELECT distinct ordd.id, ordd.folio, ordd.importe_prima,ordd.iva,ordd.subtotal,ordd.isr,ordd.iva_retenido,ordd.importe_total ");
	      queryString.append(" FROM MIDAS.CA_ORDENPAGOS_DETALLE ORDD ");
	      queryString.append(" where ORDD.folio = ?1 ");
	      queryString.append(" and ORDD.estatus_ordenpago = ?2 ");
	      queryString.append(" GROUP BY ordd.id, ordd.folio, ordd.importe_prima,ordd.iva,ordd.subtotal,ordd.isr,ordd.iva_retenido,ordd.importe_total ");
	      queryString.append(" order by ordd.folio desc ");
		  
	      Query query = entityManager.createNativeQuery(queryString.toString(), OrdenPagosDetalle.class);
	      query.setParameter(1, idParametro); 
	      query.setParameter(2, estatusRecibo); 
	      lista=query.getResultList();
	      LOGGER.debug("OrdenPagosDetalle size="+lista.size());
	      LOGGER.debug("query="+query.toString());                
	    } catch (RuntimeException re) {
	    	LOGGER.error("--error obtenerHonorariosOrdenPagos()", re);
	        throw re; 
	    }    
	    
	    return lista;
	  }
	
	public BigDecimal consultarPagoPorUtilidad(Long compensacionId, Long entidadPersonaId){
		
		LOGGER.debug(">> consultarPagoPorUtilidad()");
		BigDecimal pago = null;
		
		StringBuilder queryString;
	
		try{
			int index = 1;
			queryString = new StringBuilder(" SELECT ");
			queryString.append(" COALESCE(SUM(importe_utilidad), 0) AS pago ");
			queryString.append(" FROM MIDAS.ca_ordenpagos_detalle ");
			queryString.append(" WHERE compensacion_id = ? ");
			queryString.append(" AND tipocompensacion_id = ? ");
			queryString.append(" AND tipoentidad_id = ? ");
			queryString.append(" AND entidadpersona_id = ?");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(index++, compensacionId);
			query.setParameter(index++, ConstantesCompensacionesAdicionales.PORUTILIDAD);
			query.setParameter(index++, ConstantesCompensacionesAdicionales.PROVEEDOR);
			query.setParameter(index++, entidadPersonaId);
			
			Object result = query.getSingleResult();
			
			if(result instanceof BigDecimal){
				pago = (BigDecimal) result;
			}else if(result instanceof Long){
				pago = new BigDecimal(((Long) result));
			}else if(result instanceof Integer){
				pago = new BigDecimal(((Integer) result));
			}else{
				pago = (BigDecimal) result;
			}
			
		}catch(RuntimeException re){
			LOGGER.error("Información del Error", re);
			pago = BigDecimal.ZERO;
		}		
		LOGGER.debug("<< consultarPagoPorUtilidad()");
		
		return pago;
		
	}
	

	public BigDecimal consultarProvisionPorUtilidad(Long compensacionId, Long entidadPersonaId){		
		
		LOGGER.debug(">> consultarProvisionPorUtilidad()");
		
		BigDecimal provision = null;
		StringBuilder queryString;
		
		try{
			int index = 1;
			queryString = new StringBuilder(" SELECT ");
			queryString.append(" COALESCE(SUM(provpor.importe), 0) AS provision ");
			queryString.append(" FROM MIDAS.ca_calculoprovision prov ");
			queryString.append(" INNER JOIN MIDAS.ca_calculoprovisionporcent provpor ON(prov.id = provpor.calculoprovision_id) ");
			queryString.append(" INNER JOIN MIDAS.ca_parametros param ON(param.id = provpor.parametros_id) ");
			queryString.append(" WHERE prov.compensacion_id = ? ");
			queryString.append(" AND param.entidadpersona_id = ? ");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(index++, compensacionId);
			query.setParameter(index++, entidadPersonaId);
			
			Object result = query.getSingleResult();
			
			if(result instanceof BigDecimal){
				provision = (BigDecimal) result;
			}else if(result instanceof Long){
				provision = new BigDecimal(((Long) result));
			}else if(result instanceof Integer){
				provision = new BigDecimal(((Integer) result));
			}else{
				provision = (BigDecimal) result;
			}
			
			
		}catch (Exception e) {
			LOGGER.error("Información del Error", e);
			provision = BigDecimal.ZERO;
		}
		
		LOGGER.debug("<< consultarProvisionPorUtilidad()");
		return provision;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public String guardarSiniestralidad(String datos){
		String respuesta="";
		LOGGER.debug(">> guardarSiniestralidad()");
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper("MIDAS.PKG_CA_VIDA_COMPENSACIONES.generarCalculoSiniestralidad", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados("string","pOutMensajeSin","pOutMensajeSin");
			storedHelper.estableceParametro("pArrayDatosSiniestralidad", datos);
			respuesta=storedHelper.ejecutaActualizarString();
		} catch (Exception e) {
			LOGGER.debug("Información del Error", e);
			respuesta="1";
		}
		LOGGER.debug("<< guardarSiniestralidad()");
		return respuesta;
	}
}