package mx.com.afirme.midas2.dao.impl.operacionessapamis;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.operacionessapamis.SapAmisBitacoraSiniestroDao;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class SapAmisBitacoraSiniestroDaoImpl extends EntidadDaoImpl implements SapAmisBitacoraSiniestroDao {
	private EntidadService entidadService;
	private final String CLASS_NAME =  "SapAmisBitacoraSiniestroDao_impl";

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void guardarEnBitacoraSiniestro(SapAmisBitacoraSiniestros arg0) {
		entidadService.save(arg0);
	}

	public List<SapAmisBitacoraSiniestros> obtenerBitacoraSiniestrosFiltrada(String bitacoraPoliza, String bitacoraVin, String bitacoraFechaEnvio, String estatusEnvio, String cesvi, String cii, String emision, String ocra, String prevencion, String pt, String csd, String siniestro, String sipac, String valuacion) {
		String methodName = CLASS_NAME + " :: obtenerBitacoraSiniestrosFiltrada():: ";
		LogDeMidasEJB3.log(methodName + "Inicializa.", Level.INFO, null);

		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		LogDeMidasEJB3.log(methodName + "Datos recibidos por la consulta", Level.INFO, null);
		LogDeMidasEJB3.log(methodName + "Poliza: " + bitacoraPoliza + "Vin: " + bitacoraVin + "Fecha envio: " + bitacoraFechaEnvio + "estatusEnvio: " + estatusEnvio, Level.INFO, null);

		List<SapAmisBitacoraSiniestros> lista = new ArrayList<SapAmisBitacoraSiniestros>();
		Map<Integer, Object> params = new HashMap<Integer, Object>();

		final StringBuilder queryString = new StringBuilder("");
		queryString.append(" select be.USUARIO_SAP, be.PASSWORD_SAP, be.NUMEROPOLIZA, be.INCISO, be.FECHASINIESTRO, be.NUMEROSERIE, be.CAUSASINIESTRO, be.MONTOOCURRIDOSINIESTRO , be.UBICACIONDELSINIESTRO_PAIS, be.UBICACIONDELSINIESTRO_ESTADO  , be.UBICACIONDELSINIESTRO_MUNI, be.AFECTADO, be.AFECTADOPATERNO, be.AFECTADOMATERNO, be.AFECTADONOMBRE, be.AFECTADORFC, be.AFECTADOCURP, be.CONDUCTORPATERNO, be.CONDUCTORNOMBRE, be.CONDUCTORRFC, be.CONDUCTORCURP, be.FECHACANCELACION, be.DESISTIMIENTO, be.MONTO_RECHAZO, be.DETECTADO_SAP, be.OBSERVACIONES, be.BANDERA_OPERACION, be.FECHA_ENVIO_SAP, be.ESTATUS_RESP_OPERACION, be.FECHAFIN_RESP_SAP, be.ESTATUS_RESP_VALIDACION, be.ESTATUS_RESP_ALERTAS, be.MENSAGE_VALIDACION, be.IDSAP_ALERTASISTEMAS_ENVIO, be.IDSAP_AMIS_BITACORA_SINIESTROS, be.NUMEROSINIESTRO ");
		queryString.append(" from MIDAS.sap_amis_bitacora_siniestros be ");
		queryString.append(" where ");
		int index = 1;

		if (bitacoraPoliza != null && bitacoraVin == null && bitacoraFechaEnvio == null) {
			addCondition(queryString, "be.NUMEROPOLIZA=?");
			params.put(index, bitacoraPoliza);
			index++;

			LogDeMidasEJB3.log(methodName + "Flujo1", Level.INFO, null);

			if (estatusEnvio.equals("0")) {
				addCondition(queryString, "be.estatus_resp_operacion=?");
				params.put(index, estatusEnvio);
				index++;
			} else if (estatusEnvio.equals("2")) {
				// No se agrega Condicion
			} else {
				addCondition(queryString, "be.estatus_resp_operacion!=?");
				params.put(index, "0");
				index++;
			}

		} else if (bitacoraVin != null && bitacoraPoliza == null && bitacoraFechaEnvio == null) {
			addCondition(queryString, "be.NUMEROSERIE=?");
			params.put(index, bitacoraVin);
			index++;

			LogDeMidasEJB3.log(methodName + "Flujo2", Level.INFO, null);

			if (estatusEnvio.equals("0")) {
				addCondition(queryString, "be.estatus_resp_operacion=?");
				params.put(index, estatusEnvio);
				index++;
			} else if (estatusEnvio.equals("2")) {
				// No se agrega Condicion
			} else {
				addCondition(queryString, "be.estatus_resp_operacion!=?");
				params.put(index, "0");
				index++;
			}

		} else if (bitacoraFechaEnvio != null && bitacoraPoliza == null && bitacoraVin == null) {

			try {
				Date fechaCanvertida = formatter.parse(bitacoraFechaEnvio);
				queryString.append(" be.fecha_envio_sap = to_date('" + bitacoraFechaEnvio + "', 'dd/mm/yyyy') and ");

				LogDeMidasEJB3.log(methodName + "Flujo3", Level.INFO, null);

				if (estatusEnvio.equals("0")) {
					addCondition(queryString, "be.estatus_resp_operacion=?");
					params.put(index, estatusEnvio);
					index++;
				} else if (estatusEnvio.equals("2")) {
					// No se agrega Condicion
				} else {
					addCondition(queryString, "be.estatus_resp_operacion!=?");
					params.put(index, "0");
					index++;
				}

			} catch (ParseException e) {
				LogDeMidasEJB3.log(methodName + "Error al convertir la fecha de para la consulta de la bitacora de siniestros", Level.INFO, null);
				e.printStackTrace();
			}

		} else {

			LogDeMidasEJB3.log(methodName + "Flujo4", Level.INFO, null);

			if (bitacoraPoliza != null) {
				addCondition(queryString, "be.NUMEROPOLIZA=?");
				params.put(index, bitacoraPoliza);
				index++;
			}
			if (bitacoraVin != null) {
				addCondition(queryString, "be.NUMEROSERIE=?");
				params.put(index, bitacoraVin);
				index++;
			}
			if (bitacoraFechaEnvio != null) {

				try {
					Date fechaCanvertida = formatter.parse(bitacoraFechaEnvio);
					queryString.append(" be.fecha_envio_sap = to_date('" + bitacoraFechaEnvio + "', 'dd/mm/yyyy') and ");
				} catch (ParseException e) {
					LogDeMidasEJB3.log(methodName + "Error al convertir la fecha de para la consulta de la bitacora de siniestros", Level.INFO, null);
					e.printStackTrace();
				}

			}
			if (bitacoraFechaEnvio == null) {

				SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
				queryString.append(" be.fecha_envio_sap = to_date('" + sdf.format(new Date((Calendar.getInstance().getTimeInMillis()))) + "', 'dd/mm/yyyy') and ");

			}

			if (estatusEnvio != null) {
				if (estatusEnvio.equals("0")) {
					addCondition(queryString, "be.estatus_resp_operacion=?");
					params.put(index, estatusEnvio);
					index++;
				} else if (estatusEnvio.equals("2")) {
					// No se agrega Condicion
				} else {
					addCondition(queryString, "be.estatus_resp_operacion!=?");
					params.put(index, "0");
					index++;
				}
			}
		}

		if (bitacoraPoliza == null && bitacoraVin == null && bitacoraFechaEnvio == null && estatusEnvio == null) {
			int lengthWhere = "where ".length();
			queryString.replace(queryString.length() - lengthWhere, queryString.length(), " ");
		}

		String finalQuery = getQueryString(queryString);// +" order by s.id asc ";
		finalQuery = finalQuery + " group by be.USUARIO_SAP, be.PASSWORD_SAP, be.NUMEROPOLIZA, be.INCISO, be.FECHASINIESTRO, be.NUMEROSERIE, be.CAUSASINIESTRO, be.MONTOOCURRIDOSINIESTRO , be.UBICACIONDELSINIESTRO_PAIS, be.UBICACIONDELSINIESTRO_ESTADO  , be.UBICACIONDELSINIESTRO_MUNI, be.AFECTADO, be.AFECTADOPATERNO, be.AFECTADOMATERNO, be.AFECTADONOMBRE, be.AFECTADORFC, be.AFECTADOCURP, be.CONDUCTORPATERNO, be.CONDUCTORNOMBRE, be.CONDUCTORRFC, be.CONDUCTORCURP, be.FECHACANCELACION, be.DESISTIMIENTO, be.MONTO_RECHAZO, be.DETECTADO_SAP, be.OBSERVACIONES, be.BANDERA_OPERACION, be.FECHA_ENVIO_SAP, be.ESTATUS_RESP_OPERACION, be.FECHAFIN_RESP_SAP, be.ESTATUS_RESP_VALIDACION, be.ESTATUS_RESP_ALERTAS, be.MENSAGE_VALIDACION, be.IDSAP_ALERTASISTEMAS_ENVIO, be.IDSAP_AMIS_BITACORA_SINIESTROS, be.NUMEROSINIESTRO order by be.IDSAP_AMIS_BITACORA_SINIESTROS";

		LogDeMidasEJB3.log(methodName + "Consulta final", Level.INFO, null);
		LogDeMidasEJB3.log(methodName + finalQuery, Level.INFO, null);
		LogDeMidasEJB3.log(methodName + "Parametros de la consulta", Level.INFO, null);
		LogDeMidasEJB3.log(methodName + "Poliza: " + bitacoraPoliza + "Vin: " + bitacoraVin + "Fecha envio: " + "'" + bitacoraFechaEnvio + "'" + "estatusEnvio: " + estatusEnvio, Level.INFO, null);

		Query query = entityManager.createNativeQuery(finalQuery, SapAmisBitacoraSiniestros.class);
		if (!params.isEmpty()) {
			for (Integer key : params.keySet()) {
				query.setParameter(key, params.get(key));
			}
		}
		lista = query.getResultList();
		return lista;
	}

	public String getQueryString(StringBuilder queryString) {
		String query = queryString.toString();
		if (query.endsWith(" and ")) {
			query = query.substring(0, (query.length()) - (" and ").length());
		}
		if (query.endsWith(" or ")) {
			query = query.substring(0, (query.length()) - (" or ").length());
		}
		return query;
	}

	public void addCondition(StringBuilder queryString, String conditional) {
		if (!queryString.toString().contains("where")) {
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}