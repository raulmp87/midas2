package mx.com.afirme.midas2.dao.impl.bitemporal;

import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import mx.com.afirme.midas2.dao.bitemporal.BitemporalDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspIncContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.Bitemporal;
import com.anasoft.os.daofusion.bitemporal.Continuity;
import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class BitemporalDaoImpl implements BitemporalDao {
	@PersistenceContext
	protected EntityManager entityManager;
	private final String insertedAction = "inserted";
	private final String updatedAction = "updated";
	private final String deletedAction = "deleted";

	@Override
	public <E extends EntidadBitemporal> void executeActionGrid(String accion,
			E entity) {
		if (accion.equals(insertedAction)) {
			this.persist(entity);
		} else if (accion.equals(deletedAction)) {
			this.remove(entity);
		} else if (accion.equals(updatedAction)) {
			this.update(entity);
		}
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public List executeNativeQueryMultipleResult(String query) {
		Query nativeQuery = entityManager.createNativeQuery(query);
		return nativeQuery.getResultList();
	}

	@Override
	public Object executeNativeQuerySimpleResult(String query) {
		Query nativeQuery = entityManager.createNativeQuery(query);
		return nativeQuery.getSingleResult();
	}

	@SuppressWarnings({ "rawtypes" })
	@Override
	public List executeQueryMultipleResult(String query,
			Map<String, Object> parameters) {
		Query entityQuery = entityManager.createQuery(query);
		this.setQueryParameters(entityQuery, parameters);
		return entityQuery.getResultList();
	}

	@Override
	public Object executeQuerySimpleResult(String query,
			Map<String, Object> parameters) {
		try {
			Query entityQuery = entityManager.createQuery(query);
			this.setQueryParameters(entityQuery, parameters);
			return entityQuery.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	@Override
	public <E extends EntidadBitemporal> List<E> findAll(Class<E> entityClass) {
		CriteriaQuery<E> criteriaQuery = entityManager.getCriteriaBuilder()
				.createQuery(entityClass);
		criteriaQuery.from(entityClass);
		TypedQuery<E> query = entityManager.createQuery(criteriaQuery);
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}

	@Override
	public <E extends EntidadBitemporal, K> E findById(Class<E> entityClass,
			K key) {
		return (E) entityManager.find(entityClass, key);
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E extends EntidadBitemporal> List<E> findByProperties(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn) {
		String[] orderBy = null;
		Query query = this.createQuery(entityClass, params, validOn,
				TimeUtils.now(), orderBy);
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public <E extends EntidadBitemporal> List<E> findByPropertiesWithOrder(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn,
			String... orderBy) {
		Query query = this.createQuery(entityClass, params, validOn, TimeUtils.now(),
				orderBy);
		return query.getResultList();
	}

	@Override
	public <E extends EntidadBitemporal> List<E> findByProperty(Class<E> entityClass,
			String attributte, Object value, DateTime validOn) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(attributte, value);		
		return this.findByProperties(entityClass, params, validOn);
	}

	@Override
	public <E extends EntidadBitemporal, K> E getReference(
			Class<E> entityClass, K key) {
		return (E) entityManager.getReference(entityClass, key);
	}

	@Override
	public <E extends EntidadBitemporal> void persist(E entity) {
		entityManager.persist(entity);

	}

	@Override
	public <E extends EntidadBitemporal> Object persistAndReturnId(E entity) {
		persist(entity);
		return entityManager.getEntityManagerFactory().getPersistenceUnitUtil()
				.getIdentifier(entity);

	}

	@Override
	public <E extends EntidadBitemporal> void refresh(E entity) {
		entityManager.refresh(entity);

	}

	@Override
	public <E extends EntidadBitemporal> void remove(E entity) {
		entity = entityManager.merge(entity);
		entityManager.remove(entity);

	}

	@Override
	public <E extends EntidadBitemporal> E update(E entity) {
		return entityManager.merge(entity);
	}

	@Override
	public void updateWithQuery(String query, Map<String, Object> parameters) {
		try {
			Query entityQuery = entityManager.createQuery(query);
			this.setQueryParameters(entityQuery, parameters);
			entityQuery.executeUpdate();
		} catch (NoResultException e) {
			e.printStackTrace();
		}
	}

	private void setQueryParameters(Query entityQuery,
			Map<String, Object> parameters) {
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>) it
					.next();
			entityQuery.setParameter(pairs.getKey(), pairs.getValue());
		}
	}

	private void setQueryParametersByProperties(Query entityQuery,
			Map<String, Object> parameters) {
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>) it
					.next();
			String key = getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}
	}

	public static String getValidProperty(String property) {
		String validProperty = property;
		StringBuilder str = new StringBuilder("");
		if (property.contains(".")) {
			int i = 0;
			for (String word : property.split("\\.")) {
				if (i == 0) {
					word = word.toLowerCase();
				} else {
					word = StringUtils.capitalize(word);
				}
				str.append(word);
				i++;
			}
			validProperty = str.toString();
		}
		return validProperty;
	}

	private <E extends EntidadBitemporal> Query createQuery(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn,
			DateTime knownOn, String... orderByAttributes) {
		final StringBuilder queryString = new StringBuilder(
				"select model from " + entityClass.getSimpleName() + " model ");
		queryString.append(" where ");
		queryString.append(" :validOn >= model.validityInterval.from  and :validOn < model.validityInterval.to ");
		queryString.append(" and :knownOn >= model.recordInterval.from and :knownOn < model.recordInterval.to ");
		boolean setParameters = false;
		int size = 0;
		int index = 0;
		if (params != null && !params.isEmpty()) {
			setParameters = true;
			size = params.size();
			for (String property : params.keySet()) {
				String propertyMap = getValidProperty(property);// checa si
																// tiene "." la
																// propiedad
				queryString.append(" and model." + property + " = :" + propertyMap);
		/*		if (index != (size - 1)) {
					queryString.append(" and ");
				}*/
				index++;
			}
		}

		if (orderByAttributes != null && orderByAttributes.length > 0) {
			size = orderByAttributes.length;
			index = 0;
			queryString.append(" ORDER BY ");
			for (String orderAttribute : orderByAttributes) {
				queryString.append("model." + orderAttribute);
				if (index != (size - 1)) {
					queryString.append(",");
				}
				index++;
			}
		}

		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("validOn", validOn == null ? TimeUtils.current().toDate()
				: validOn.toDate());
		query.setParameter("knownOn", knownOn == null ? TimeUtils.current().toDate()
				: knownOn.toDate());
 
		if (setParameters) {
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);

		return query;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E extends EntidadBitemporal> List<E> findByPropertiesInProcess(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn) {
		String[] orderBy = null;
		params.put("recordStatus", RecordStatus.TO_BE_ADDED);
		Query query = this.createQuery(entityClass, params, validOn,
				TimeUtils.now(), orderBy);
		return query.getResultList();

	}

	@SuppressWarnings("unchecked")
	@Override
	public <E extends EntidadBitemporal> List<E> findByPropertiesWithOrderInProcess(
			Class<E> entityClass, Map<String, Object> params, DateTime validOn,
			String... orderBy) {
		params.put("recordStatus", RecordStatus.TO_BE_ADDED);
		Query query = this.createQuery(entityClass, params, validOn, TimeUtils.now(),
				orderBy);
		return query.getResultList();
	}

	@Override
	public <E extends EntidadBitemporal> List<E> findByPropertyInProcess(Class<E> entityClass,
			String attributte, Object value, DateTime validOn) {
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(attributte, value);		
		params.put("recordStatus", RecordStatus.TO_BE_ADDED);
		return this.findByProperties(entityClass, params, validOn);
	}

	public <B extends Bitemporal, T extends Continuity<?, B>> List<T> getContinuitiesInProcessA(Class<T> continuityClass, Class<B> bitemporalClass) {
		return getContinuitiesInProcessA(continuityClass, bitemporalClass, null);
	}
	
	public <B extends Bitemporal, T extends Continuity<?, B>> List<T> getContinuitiesInProcessA(Class<T> continuityClass, Class<B> bitemporalClass, String a) {
		return null;
	}
	
	public <T extends Continuity<?, ?>> List<T> getContinuitiesInProcess(Class<T> continuityClass, CotizacionContinuity cotizacionContinuity) {
		if (continuityClass == null && cotizacionContinuity == null) {
			throw new IllegalArgumentException("Argumentos inválidos. No se aceptan nulos.");
		}
		Class<? extends Bitemporal> bitemporalClass = null;
		String cotizacionContinuityPath = null;
		if (continuityClass.equals(IncisoContinuity.class)) {
			bitemporalClass = BitemporalInciso.class;
			cotizacionContinuityPath = "cotizacionContinuity";
		}else if(continuityClass.equals(AutoIncisoContinuity.class)){
			bitemporalClass = BitemporalAutoInciso.class;
			cotizacionContinuityPath = "incisoContinuity.cotizacionContinuity";
		}else if(continuityClass.equals(CoberturaSeccionContinuity.class)) {
			bitemporalClass = BitemporalCoberturaSeccion.class;
			cotizacionContinuityPath = "seccionIncisoContinuity.incisoContinuity.cotizacionContinuity";
		}else if(continuityClass.equals(CondicionEspIncContinuity.class)) {
			bitemporalClass = BitemporalCondicionEspInc.class;
			cotizacionContinuityPath = "incisoContinuity.cotizacionContinuity";
		}
		else {
			throw new IllegalArgumentException("No se encontró la clase bitemporal asociada al continuity enviado. " +
					"Se debe agregar a la lista la clase bitemporal y el path a la CotizacionContinuity.");
		}
		return getContinuitiesInProcess(continuityClass, bitemporalClass, cotizacionContinuityPath, cotizacionContinuity);
	}
	
	public <B extends Bitemporal, T extends Continuity<?, ?>> List<T> getContinuitiesInProcess(
			Class<T> continuityClass, Class<B> bitemporalClass,
			String cotizacionContinuityPath,
			CotizacionContinuity cotizacionContinuity) {
		String strQuery = "select distinct c from " + bitemporalClass.getSimpleName() + " b join b.continuity c " +
				"where c." + cotizacionContinuityPath + " = :cotizacionContinuity and (b.recordStatus = :recordStatus1 or b.recordStatus = :recordStatus2)";
		TypedQuery<T> query = entityManager.createQuery(strQuery, continuityClass);
		query.setParameter("cotizacionContinuity", cotizacionContinuity);
		query.setParameter("recordStatus1", RecordStatus.TO_BE_ENDED);
		query.setParameter("recordStatus2", RecordStatus.TO_BE_ADDED);
		return query.getResultList();
	}
	
}
