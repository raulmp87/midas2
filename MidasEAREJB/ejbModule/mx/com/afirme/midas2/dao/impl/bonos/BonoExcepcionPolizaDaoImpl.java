package mx.com.afirme.midas2.dao.impl.bonos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.bonos.BonoExcepcionPolizaDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.BonoExcepcionPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.dto.fuerzaventa.ExcepcionesPolizaView;

/**
 * 
 * @author admin
 *
 */

@Stateless
public class BonoExcepcionPolizaDaoImpl extends EntidadDaoImpl implements BonoExcepcionPolizaDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<BonoExcepcionPoliza> loadByConfigBono(ConfigBonos configBono) throws Exception {
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append(" SELECT distinct trconfig.ID as IDEXCEPCIONPOLIZA, trconfig.DESCUENTOPOLIZA as DESCUENTOPOLIZA, trconfig.NOAPLICADESCEXCEPCION as NOAPLICADESCUENTOEXCEPCION, trconfig.VALORMONTOPCTEAGENTE as VALORMONTOPCTEAGENTE, trconfig.VALORMONTOPCTEPROMOTORIA as VALORMONTOPCTEPROMOTORIA, trconfig.IDCONFIG as IDCONFIG, trconfig.IDPOLIZA as IDPOLIZA, "); 
		queryString.append(" lpad(p.id_centro_emis,3,0)||'-'||lpad(p.num_poliza,10,0)||'-'||lpad(p.num_renov_pol,2,0) as NUMEROPOLIZASEYCOSSTRING,  ");
		queryString.append(" p.NUM_POLIZA as NUMEROPOLIZA, p.ID_COTIZACION as IDCOTIZACION");
		queryString.append(" from midas.TRCONFIGBONOEXCEPCIONPOLIZA trconfig ");
		queryString.append(" INNER JOIN seycos.pol_poliza p on p.ID_COTIZACION = trconfig.IDPOLIZA ");   
		int index=1;
		if(configBono.getId()!=null){
				addCondition(queryString, " trconfig.IDCONFIG=? ");
				params.put(index, configBono.getId());
				index++;
		}
		//INICIO REQUERIMIENTO PARA EXCLUIR LAS PÓLIZAS CON BONIFICACIÓN CERO [BSD 18-07-2018]
		queryString.append(" UNION ");
		queryString.append("SELECT  DISTINCT trexclusion.ID AS IDEXCEPCIONPOLIZA, null, null, null, null, 0, rownum as IDPOLIZA, ");
		queryString.append("lpad(p.id_centro_emis,3,0)||'-'||lpad(p.num_poliza,10,0)||'-'||lpad(p.num_renov_pol,2,0) as NUMEROPOLIZASEYCOSSTRING,  ");
		queryString.append("p.NUM_POLIZA as NUMEROPOLIZA, p.ID_COTIZACION as IDCOTIZACION, trexclusion.IDAGENTE  ");
		queryString.append("FROM midas.TREXCLUSIONPOLIZABONOCERO trexclusion ");
		queryString.append("INNER JOIN MIDAS.TOPOLIZA  pol ON pol.IDTOPOLIZA= trexclusion.IDCOTIZACION ");
		queryString.append("inner join SEYCOS.conv_midas_seycos conv on conv.id_midas =   pol.IDTOPOLIZA||'|0'   ");
		queryString.append("inner join seycos.pol_poliza p on p.ID_COTIZACION = conv.id_seycos    ");
		
		if(configBono.getId()!=null){
			queryString.append("  WHERE conv.tipo ='POLIZA' AND  trexclusion.IDAGENTE IN (SELECT AGENTE_ID idAgente FROM MIDAS.TRCONFIGBONOAGENTE WHERE CONFIGBONO_ID =? ) "); //Mediante el id de configuración, se obtienen los agentes para obtener las pólizas y con ello excluirlas y no puedan ser eliminadas en la configuración de bonos. [BSD 18-07-2018]
		}
		//FIN 

		Query query = entityManager.createNativeQuery(getQueryString(queryString),ExcepcionesPolizaView.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		query.setMaxResults(100);
		return getListaBonoExcepcionPoliza(query.getResultList());
		
	}
	
	private List<BonoExcepcionPoliza> getListaBonoExcepcionPoliza(List<ExcepcionesPolizaView> lista){
		List<BonoExcepcionPoliza> listaNueva = new ArrayList<BonoExcepcionPoliza>();
		if (!lista.isEmpty() && lista.get(0)!= null){
			for(ExcepcionesPolizaView obj :lista){
				BonoExcepcionPoliza objNew = new BonoExcepcionPoliza();
				objNew.setIdExcepcionPoliza(obj.getIdExcepcionPoliza());
				objNew.setIdConfig(obj.getIdConfig());
				objNew.setIdCotizacion(obj.getIdCotizacion());
				objNew.setValorMontoPcteAgente(obj.getValorMontoPcteAgente());
				objNew.setValorMontoPctePromotoria(obj.getValorMontoPctePromotoria());
				objNew.setNoAplicaDescuentoExcepcion(obj.getNoAplicaDescuentoExcepcion());
				objNew.setDescuentoPoliza(obj.getDescuentoPoliza());
				objNew.setPolizaFormateada(obj.getNumeroPolizaSeycosString());
				listaNueva.add(objNew);
			}
		}
		return listaNueva;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
}
