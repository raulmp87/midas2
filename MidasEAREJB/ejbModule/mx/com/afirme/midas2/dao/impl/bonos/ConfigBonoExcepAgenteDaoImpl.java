package mx.com.afirme.midas2.dao.impl.bonos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.bonos.ConfigBonoExcepAgenteDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoExcepAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;

@Stateless
public class ConfigBonoExcepAgenteDaoImpl extends EntidadDaoImpl implements ConfigBonoExcepAgenteDao{
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigBonoExcepAgente> loadByConfigBono(ConfigBonos configBono)	throws Exception {
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<ConfigBonoExcepAgente> lista=new ArrayList<ConfigBonoExcepAgente>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append(" select id,idconfig,idagente,valormontopcteagente,noaplicadescexcepcion from MIDAS.TRCONFIGBONOEXCEPCIONAGENTE ");
		queryString.append(" where ");
		int index=1;
		if(configBono.getId()!=null){
				addCondition(queryString, " idconfig=? ");
				params.put(index, configBono.getId());
				index++;
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString),ConfigBonoExcepAgente.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		query.setMaxResults(100);
		lista=query.getResultList();
		return lista;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
}
