package mx.com.afirme.midas2.dao.impl.calculos;
import static mx.com.afirme.midas2.utils.CommonUtils.addCondition;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;
import static mx.com.afirme.midas2.utils.CommonUtils.setQueryParametersByProperties;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.calculos.CalculoComisionesDao.EstatusCalculoComisiones;
import mx.com.afirme.midas2.dao.calculos.DetalleCalculoComisionesDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.calculos.DetalleCalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.util.MidasException;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
@Stateless
public class DetalleCalculoComisionesDaoImpl extends EntidadDaoImpl implements DetalleCalculoComisionesDao{
	private static String CATALOGO_ESTATUS_CALCULO="Estatus Calculo Comisiones";
	private EntidadService entidadService;
	private ValorCatalogoAgentesService catalogoService;
	
	@Override
	public void deleteDetallePorCalculo(Long idCalculo) throws MidasException {
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo");
		}
		List<DetalleCalculoComisiones> lista=listarDetalleDeCalculo(idCalculo);
		if(!isEmptyList(lista)){
			for(DetalleCalculoComisiones detalle:lista){
				if(isNotNull(detalle)){
					entidadService.remove(detalle);
				}
			}
		}
	}

	@Override
	public void deleteDetallePorCalculo(CalculoComisiones calculo)throws MidasException {
		if(isNull(calculo)){
			onError("Favor de proporcionar el calculo");
		}
		Long idCalculo=calculo.getId();
		deleteDetallePorCalculo(idCalculo);
	}
	
	@SuppressWarnings("unchecked")
	public void generarDetalleCalculoComisiones(Long idConfigComisiones,List<AgenteView> agentes,Long idCalculoTemporal,Long idCalculo) throws MidasException{
		List<DetalleCalculoComisiones> list=new ArrayList<DetalleCalculoComisiones>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		if(isEmptyList(agentes)){
			onError("Favor de proporcionar la lista de agentes a obtener el calculo");
		}
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select    ");
		queryString.append(" MIDAS.IDTODETALLECALCULOCOM_SEQ.nextval as id,");
		queryString.append(" gerencia.id as idGerencia, ");
		queryString.append(" prom.id as idPromotoria, ");
		queryString.append(" idAgente, ");
		queryString.append(" "+idCalculo+" as idCalculo, ");
//		queryString.append(" numeroCuenta");
		queryString.append(" (totalImporteComisionAgente+totalImporteIva - totalImporteIvaRet-totalImporteISR) as importeCalculo ");  
		queryString.append(" from ( ");
		queryString.append(" select  ");
		queryString.append(" agente.id as idAgente, ");
		queryString.append(" agente.idPromotoria as idPromotoria, ");
		queryString.append(" sum(movs.importeComisionAgente) as totalImporteComisionAgente, ");
		queryString.append(" sum(movs.importeIva) as totalImporteIva, ");
		queryString.append(" sum(movs.importeIvaRet) as totalImporteIvaRet, ");
		queryString.append(" sum(movs.importeIsr) as totalImporteISR ");      
		queryString.append(" from MIDAS.toAgenteMovimientos movs  ");
		queryString.append(" inner join MIDAS.toAgente agente on(movs.idAgente=agente.idAgente) ");
		queryString.append(" inner join MIDAS.Gt_AgentesCalculoTemporal prev on(prev.idCalculo="+idCalculoTemporal+" and prev.idRegistro=agente.id ) ");
		queryString.append(" inner join ( ");
		queryString.append(" select id,valor from MIDAS.toValorCatalogoAgentes where grupoCatalogoAgentes_id in (select id from MIDAS.tcGrupoCatalogoAgentes where descripcion like 'Situacion Movimiento Agente') ");    
		queryString.append(" ) situacionMov on (upper(trim(situacionMov.valor)) like 'PENDIENTE' and situacionMov.id=movs.estatusMovimiento)    ");
		queryString.append(" where  claveComision=1 "); 
		queryString.append(" group by agente.id ,agente.idPromotoria ");
		queryString.append(" ) resumen ");
		queryString.append(" inner join MIDAS.toConfigComisiones conf on(conf.id="+idConfigComisiones+")   ");
		queryString.append(" inner join MIDAS.toPromotoria prom on(prom.id=resumen.idPromotoria)  ");
		queryString.append(" left join MIDAS.toEjecutivo ejecutivo on (ejecutivo.id=prom.ejecutivo_id)  ");
		queryString.append(" left join MIDAS.toGerencia gerencia on(gerencia.id=ejecutivo.gerencia_id) ");
		queryString.append(" where (totalImporteComisionAgente+totalImporteIva - totalImporteIvaRet-totalImporteISR) >=conf.importeMinimo ");        
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"detalleCalculoComisionesView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list=query.getResultList();
		if(!isEmptyList(list)){
			for(DetalleCalculoComisiones det:list){
				if(isNotNull(det)){
					entityManager.detach(det);
					entidadService.save(det);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DetalleCalculoComisiones> listarDetalleDeCalculo(Long idCalculo)throws MidasException {
		List<DetalleCalculoComisiones> list=new ArrayList<DetalleCalculoComisiones>();
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo");
		}
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from DetalleCalculoComisiones model ");
		addCondition(queryString, "model.calculoComisiones.id =:idCalculo");
		params.put("idCalculo", idCalculo);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<DetalleCalculoComisiones> listarDetalleDeCalculoNoAutorizado(Long idCalculo) {
		
		List<DetalleCalculoComisiones> list = new ArrayListNullAware<DetalleCalculoComisiones>();
		
		List<DetalleCalculoComisiones> list2 = new ArrayListNullAware<DetalleCalculoComisiones>();
		
		Map<String,Object> params = new HashMap<String, Object>();
		StringBuilder queryString = new StringBuilder("");
		queryString.append("select model from DetalleCalculoComisiones model ");
		addCondition(queryString, "model.calculoComisiones.id =:idCalculo");
		addCondition(queryString, "model.claveEstatus.valor <> :claveEstatus");
		params.put("idCalculo", idCalculo);
		params.put("claveEstatus", EstatusCalculoComisiones.AUTORIZADO.getValue());
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		
		params = new HashMap<String, Object>();
		queryString = new StringBuilder("");
		queryString.append("select model from DetalleCalculoComisiones model ");
		addCondition(queryString, "model.calculoComisiones.id =:idCalculo");
		addCondition(queryString, "model.claveEstatus IS NULL");
		params.put("idCalculo", idCalculo);
		query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list2=query.getResultList();
		
		list.addAll(list2);
		
		return list;
		
		
	}
	
	/**
	 * Obtiene el detalle de un calculo de comisiones para comparar por parte de midas.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDetalleCalculoMidasDiferencia(Long idCalculo) throws MidasException{
		List<DetalleCalculoComisiones> list=new ArrayList<DetalleCalculoComisiones>();
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS.Pkg_int_midas_E2.stpConsultarChequesPorCalculo";
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			String[] fields={"id","idSolicitudCheque","clageAgente","importe","agente.id","nombreAgente","tieneDiferencia"};
			String[] columns={"id","idSolicitudCheque","claveAgente","importe","idAgente","nombreAgente","diferente"};
			storedHelper.estableceMapeoResultados(DetalleCalculoComisiones.class.getCanonicalName(),fields,columns);
			storedHelper.estableceParametro("pIdCalculoComisiones",idCalculo);
			storedHelper.estableceParametro("pTipo","MIDAS");
			list=(List<DetalleCalculoComisiones>)storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.WARNING, e);
			onError(e);
		}
		return list;
	}
	/**
	 * Obtiene el detalle de un calculo de comisiones para comparar por parte de mizar.
	 * @param idCalculo
	 * @return	
	 * @throws MidasException
	 */
	public List<DetalleCalculoComisiones> listarDetalleCalculoMizarDiferencia(Long idCalculo) throws MidasException{
		List<DetalleCalculoComisiones> list=new ArrayList<DetalleCalculoComisiones>();
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS.Pkg_int_midas_E2.stpConsultarChequesPorCalculo";
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			String[] fields={"id","idSolicitudChequeStr","clageAgente","importe","agente.id","nombreAgente","tieneDiferencia"};
			String[] columns={"id","idSolicitudCheque","claveAgente","importe","idAgente","nombreAgente","diferente"};
			storedHelper.estableceMapeoResultados(DetalleCalculoComisiones.class.getCanonicalName(),fields,columns);
			storedHelper.estableceParametro("pIdCalculoComisiones",idCalculo);
			storedHelper.estableceParametro("pTipo","MIZAR");
			list=(List<DetalleCalculoComisiones>)storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.WARNING, e);
			onError(e);
		}
		return list;
	}
	
	public List<DetalleCalculoComisiones> listarDiferenciasMidasMizar(Long idCalculo) throws MidasException{
		List<DetalleCalculoComisiones> list=new ArrayList<DetalleCalculoComisiones>();
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS.Pkg_int_midas_E2.stpConsultarChequesPorCalculo";
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			String[] fields={"id","idSolicitudChequeStr","clageAgente","importe","agente.id","nombreAgente","tieneDiferencia"};
			String[] columns={"id","idSolicitudCheque","claveAgente","importe","idAgente","nombreAgente","diferente"};
			storedHelper.estableceMapeoResultados(DetalleCalculoComisiones.class.getCanonicalName(),fields,columns);
			storedHelper.estableceParametro("pIdCalculoComisiones",idCalculo);
			storedHelper.estableceParametro("pTipo","DIFERENCIA");
			list=(List<DetalleCalculoComisiones>)storedHelper.obtieneListaResultados();
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.obtenerSolcitudesChequeSeycos..." + this, Level.WARNING, e);
			onError(e);
		}
		return list;
	}
	/**
	 * 
	 * @param calculo
	 * @return
	 * @throws MidasException
	 */
	@Override
	public List<DetalleCalculoComisiones> listarDetalleDeCalculo(CalculoComisiones calculo) throws MidasException {
		if(isNull(calculo)){
			onError("Favor de proporcionar el calculo");
		}
		Long idCalculo=calculo.getId();
		return listarDetalleDeCalculo(idCalculo);
	}

	@Override
	public DetalleCalculoComisiones loadById(Long idDetalleCalculo) throws MidasException {
		if(isNull(idDetalleCalculo)){
			onError("Favor de proporcionar la clave del detalle del calculo");
		}
		DetalleCalculoComisiones detalle=entidadService.findById(DetalleCalculoComisiones.class, idDetalleCalculo);
		return detalle;
	}

	@Override
	public DetalleCalculoComisiones loadById(DetalleCalculoComisiones detalle) throws MidasException {
		if(isNull(detalle)){
			onError("Favor de ");
		}
		return null;
	}
	@Deprecated
	@Override
	public List<DetalleCalculoComisiones> obtenerDetalleCalculoPorConfiguracion(Long arg0, List<AgenteView> arg1) {
		// TODO Ya no aplica
		return null;
	}

	@Override
	public void rehabilitarDetallePorCalculo(Long idDetalleCalculo) throws MidasException {
		if(isNull(idDetalleCalculo)){
			onError("Favor de proporcionar el detalle del calculo a habilitar");
		}
		DetalleCalculoComisiones detalleCalculo=loadById(idDetalleCalculo);
		if(isNull(detalleCalculo)){
			onError("El detalle del calculo con clave "+idDetalleCalculo+" no existe");
		}
	}

	@Override
	public void rehabilitarDetallePorCalculo(CalculoComisiones arg0) throws MidasException {
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long saveDetalleCalculoComisiones(DetalleCalculoComisiones detalleCalculo)throws MidasException {
		if(isNull(detalleCalculo)){
			onError("Favor de proporcionar el detalle del calculo a guardar");
		}
		DetalleCalculoComisiones jpaObject=entidadService.save(detalleCalculo);
		Long id=(isNotNull(jpaObject))?jpaObject.getId():null;
		return id;
	}

	@Override
	public Long saveListDetalleCalculoComisiones(List<DetalleCalculoComisiones> lista) throws MidasException {
		if(isEmptyList(lista)){
			onError("Favor de proporcionar la lista de detalle de comisiones a pagar");
		}
		for(DetalleCalculoComisiones detalle:lista){
			if(isNotNull(detalle)){
				saveDetalleCalculoComisiones(detalle);
			}
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DetalleCalculoComisiones> findByFilters(DetalleCalculoComisiones filtro) {
		List<DetalleCalculoComisiones> list=new ArrayList<DetalleCalculoComisiones>();
		if(isNotNull(filtro)){
			Map<String,Object> params=new HashMap<String, Object>();
			final StringBuilder queryString=new StringBuilder("");
			queryString.append("select model from DetalleCalculoComisiones model ");
			if(isNotNull(filtro.getId())){
				addCondition(queryString, "model.id = :id");
				params.put("id", filtro.getId());
			}else{
				if(isNotNull(filtro.getCalculoComisiones()) && isNotNull(filtro.getCalculoComisiones().getId())){
					addCondition(queryString, "model.calculoComisiones.id = :idCalculo");
					params.put("idCalculo", filtro.getCalculoComisiones().getId());
				}
				Long idSolicitudCheque=filtro.getIdSolicitudCheque();
				if(isNotNull(idSolicitudCheque)){
					addCondition(queryString, "model.idSolicitudCheque = :idSolicitudCheque");
					params.put("idSolicitudCheque", idSolicitudCheque);
				}
				Agente agente=filtro.getAgente();
				if(isNotNull(agente)){
					if(isNotNull(agente.getId())){
						addCondition(queryString, "model.agente.id = :idAgente");
						params.put("idAgente", filtro.getCalculoComisiones().getId());
					}
					if(isNotNull(agente.getIdAgente())){
						addCondition(queryString, "model.agente.idAgente = :claveAgente");
						params.put("claveAgente", filtro.getCalculoComisiones().getId());
					}
				}
				
				if(isNotNull(filtro.getClaveEstatus()) && isNotNull(filtro.getClaveEstatus().getValor())){
					addCondition(queryString, "model.claveEstatus.valor = :claveEstatus");
					params.put("claveEstatus", filtro.getClaveEstatus().getValor());
				}
			
			}
			Query query = entityManager.createQuery(getQueryString(queryString));
			setQueryParametersByProperties(query, params);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			list=query.getResultList();
		}
		return list;
	}
	
	/**
	 * Obtiene el registro de detalle de calculo de comisiones por medio de la solicitud de cheque
	 * @param idSolicitudCheque
	 * @throws MidasException
	 */
	@Override
	public DetalleCalculoComisiones obtenerDetallePorSolicitudCheque(Long idSolicitudCheque) throws MidasException{
		DetalleCalculoComisiones detalle=null;
		if(isNull(idSolicitudCheque)){
			onError("Favor de proporcionar la solicitud de cheque");
		}
		DetalleCalculoComisiones filtro=new DetalleCalculoComisiones();
		filtro.setIdSolicitudCheque(idSolicitudCheque);
		List<DetalleCalculoComisiones> list=findByFilters(filtro);
		if(isEmptyList(list)){
			onError("No se encontro solicitud de cheque con clave:"+idSolicitudCheque);
		}else{
			detalle=list.get(0);
		}
		return detalle;
	}
	/**
	 * Actualiza el estatus de todos los registros del detalle de un calculo
	 * @param idCalculo
	 * @param estatus
	 * @throws MidasException
	 */
	@Override
	public void actualizarDetallePorCalculo(Long idCalculo,EstatusCalculoComisiones estatus) throws MidasException{
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo para actualizar su detalle");
		}
		if(isNull(estatus)){
			onError("Favor de proporcionar el estatus que desea asignar a los detalles del calculo");
		}
		ValorCatalogoAgentes claveEstatus=null;
		try{
			claveEstatus=catalogoService.obtenerElementoEspecifico(CATALOGO_ESTATUS_CALCULO,estatus.getValue());
		}catch(Exception e){
			onError(e);
		}
		if(isNull(claveEstatus)){
			onError("No existe el estatus "+estatus.getValue()+" como estatus del calculo");
		}
		List<DetalleCalculoComisiones> list=listarDetalleDeCalculo(idCalculo);
		if(!isEmptyList(list)){
			for(DetalleCalculoComisiones det:list){
				det.setClaveEstatus(claveEstatus);
			}
			saveListDetalleCalculoComisiones(list);
		}
	}
	/**
	 * Actualiza el estatus de todos los registros del detalle de un calculo
	 * @param calculo
	 * @param estatus
	 * @throws MidasException
	 */
	@Override
	public void actualizarDetallePorCalculo(CalculoComisiones calculo,EstatusCalculoComisiones estatus) throws MidasException{
		Long idCalculo=(isNotNull(calculo))?calculo.getId():null;
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo");
		}
		actualizarDetallePorCalculo(idCalculo, estatus);
	}
	
	@Override
	public List<DetalleCalculoComisionesView> listarReporteComisionAgente(Long idCalculo) throws MidasException {
		List<DetalleCalculoComisionesView> list = new ArrayList<DetalleCalculoComisionesView>();
		final StringBuilder queryString=new StringBuilder("");
        queryString.append(" select c.id,v.idgerencia,v.gerencia as nombreGerencia,v.idPromotoria,v.promotoria,v.claveAgente as idAgente,v.agente as nombreAgente,trunc(c.importe,2) as importeComision ");
		queryString.append(" ,to_char(nvl(pba.numeroclabe,NUMEROCUENTA)) numCta, banc.NOM_BANCO banco");
		queryString.append(" from MIDAS.todetallecalculocomisiones c ");
		queryString.append(" inner join MIDAS.vw_agenteInfo v on(v.idAgente=c.idAgente) ");
		queryString.append(" left join midas.toproductoBancarioAgente pba on (pba.agente_id = v.idagente and CLAVEDEFAULT =1) ");
		queryString.append(" left join midas.tcProductoBancario pb on (pb.id = pba.PRODUCTOBANCARIO_ID) ");
		queryString.append(" left join seycos.ING_BANCO banc on banc.ID_BANCO =pb.IDBANCO ");
		queryString.append(" where idcalculocomisiones= "+idCalculo);
		queryString.append(" order by claveAgente asc ");
		
		Query query=entityManager.createNativeQuery(getQueryString(queryString),DetalleCalculoComisionesView.class);
		list = query.getResultList();
		return list; 
	}	
	
	/**
	 * ==========================================================
	 * Setters and getters
	 * ==========================================================
	 */
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}	
}