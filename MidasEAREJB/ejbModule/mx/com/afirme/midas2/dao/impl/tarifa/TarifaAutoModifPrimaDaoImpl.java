package mx.com.afirme.midas2.dao.impl.tarifa;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaAutoModifPrimaDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrima;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrimaId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
@Stateless
public class TarifaAutoModifPrimaDaoImpl extends JpaDao<TarifaAutoModifPrimaId,TarifaAutoModifPrima> implements TarifaAutoModifPrimaDao{
	/**
	 * Se obtiene la lista de tarifas por version id
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaAutoModifPrima> findByTarifaVersionId(TarifaVersionId tarifaVersionId) {
		StringBuilder stm=new StringBuilder("");
		stm.append("SELECT tarifa FROM TarifaAutoModifPrima tarifa where tarifa.id.idMoneda=:idMoneda ");
		stm.append("and tarifa.id.idRiesgo=:idRiesgo ");
		stm.append("and tarifa.id.idConcepto=:idConcepto ");
		stm.append("and tarifa.id.version=:version");
		TypedQuery<TarifaAutoModifPrima> query =(TypedQuery<TarifaAutoModifPrima>)entityManager.createQuery(stm.toString());
		query.setParameter("idMoneda",tarifaVersionId.getIdMoneda());
		query.setParameter("idRiesgo",tarifaVersionId.getIdRiesgo());
		query.setParameter("idConcepto",tarifaVersionId.getIdConcepto());
		query.setParameter("version",tarifaVersionId.getVersion());
		return query.getResultList();
	}

	@Override
	public List<TarifaAutoModifPrima> findByFilters(TarifaAutoModifPrima arg0) {
		// TODO Apéndice de método generado automáticamente
		return null;
	}
}
