package mx.com.afirme.midas2.dao.impl.sapamis.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.sapamis.catalogos.CatSapAmisSupervisionCampoDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSapAmisSupervisionCampo;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSapAmisSupervisionCampo_;

@Stateless
public class CatSapAmisSupervisionCampoDaoImpl extends JpaDao<Long, CatSapAmisSupervisionCampo> implements CatSapAmisSupervisionCampoDao{
	public List<CatSapAmisSupervisionCampo> findByFilters(CatSapAmisSupervisionCampo filtroCatSapAmisSupervisionCampo) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CatSapAmisSupervisionCampo> criteriaQuery = cb.createQuery(CatSapAmisSupervisionCampo.class);
		Root<CatSapAmisSupervisionCampo> root = criteriaQuery.from(CatSapAmisSupervisionCampo.class);
		List<Predicate> predicates = new ArrayList<Predicate>();
		predicates.add(cb.and(cb.like(cb.upper(root.get(CatSapAmisSupervisionCampo_.supervisionCampo)),"%" + filtroCatSapAmisSupervisionCampo.getSupervisionCampo() + "%")));
		criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
		TypedQuery<CatSapAmisSupervisionCampo> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}
}
