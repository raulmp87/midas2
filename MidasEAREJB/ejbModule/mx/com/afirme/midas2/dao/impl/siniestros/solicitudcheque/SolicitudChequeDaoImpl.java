package mx.com.afirme.midas2.dao.impl.siniestros.solicitudcheque;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.sql.SQLException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.calculos.SolicitudChequesMizarDaoImpl;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.solicitudcheque.SolicitudChequeDao;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.PaseRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranza;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranzaDevolucion;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro.OrigenSolicitud;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestroDetalle;
import mx.com.afirme.midas2.dto.siniestros.solicitudcheque.GeneraChequeDTO;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.lang.StringUtils;
@Stateless
public class SolicitudChequeDaoImpl extends EntidadDaoImpl implements SolicitudChequeDao{
	@EJB
	EntidadDao entidadDao;

	/**
	 * Actualiza el estatus de una solicitud de cheque de MIZAR de un calculo, este cheque representa lo que se le va a pagar 
	 * @param cheque
	 * @throws MidasException
	 */
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizarSolitudChequeMizar(
			SolicitudChequeSiniestro solicitudCheque) throws MidasException {
		StoredProcedureHelper storedHelper = null;
		String sp="spinshead_md";
		Long idCalculo=solicitudCheque.getId(); // cual IDCALCULO
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitudChequesMizarDaoImpl.actualizarSolitudChequeMizar..." + this, Level.INFO, null);
			//ValorCatalogoAgentes tipoMoneda=catalogoService.obtenerElementoEspecifico("Tipo Moneda", "NACIONAL");
			//String tipoMonedaValor=(isNotNull(tipoMoneda) && isValid(tipoMoneda.getClave()))?tipoMoneda.getClave():null;	
			storedHelper = new StoredProcedureHelper(sp,StoredProcedureHelper.DATASOURCE_MIZAR);
			storedHelper.estableceParametro("id_sesion",idCalculo);//idCalculo
			storedHelper.estableceParametro("id_sol_cheque",solicitudCheque.getIdSolCheque());
			storedHelper.estableceParametro("imp_pago",solicitudCheque.getImporteCheque());
			storedHelper.estableceParametro("fecha",solicitudCheque.getFechaSolicitud());
			storedHelper.estableceParametro("moneda",solicitudCheque.getIdMoneda());//484
			storedHelper.estableceParametro("tipo_cambio",1);//Como es moneda nacional el multiplo es 1
			storedHelper.estableceParametro("nom_beneficiario",solicitudCheque.getBeneficiario());
			storedHelper.estableceParametro("concepto",solicitudCheque.getConcepto());
			storedHelper.estableceParametro("tipo_solicitud","SIN");//Para que aparezca como agente en Mizar
			storedHelper.estableceParametro("id_agente",0);//
			storedHelper.ejecutaActualizarSQLServer();
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequesMizarDaoImpl.actualizarSolitudChequeMizar..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequesMizarDaoImpl.actualizarSolitudChequeMizar..." + this, Level.WARNING, e);
			onError(e);
		}catch(NullPointerException e){
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.actualizarSolitudChequeMizar [solicitud de cheque:"+solicitudCheque.getIdSolCheque() +" |calculo:"+idCalculo+"]..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.actualizarSolitudChequeMizar..." + this, Level.WARNING, e);
			onError(e);
		}
	
	}



	@Override
	public void importarSolicitudesChequesAMizar(Long idCalculo)
			throws MidasException {
		if(isNull(idCalculo)){
			onError("Favor de proporcioanr el calculo para obtener las solicitudes de cheques que se importaran a Mizar");
		}
		StoredProcedureHelper storedHelper = null;
		String sp="spinsCheck";
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitudChequesMizarDaoImpl.importarSolicitudesChequesAMizar..." + this, Level.INFO, null);
			LogDeMidasInterfaz.log("id_sesion --> "+ idCalculo, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp,StoredProcedureHelper.DATASOURCE_MIZAR);
			storedHelper.estableceParametro("id_sesion",idCalculo);
			storedHelper.ejecutaActualizarSQLServer();
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequesMizarDaoImpl.importarSolicitudesChequesAMizar..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequesMizarDaoImpl.importarSolicitudesChequesAMizar..." + this, Level.WARNING, e);
			if( !StringUtil.isEmpty(descErr) )
				 throw new MidasException(String.format(descErr));
			else
				onError(e);
		}catch(NullPointerException e){
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.importarSolicitudesChequesAMizar [Calculo:"+idCalculo+"]..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.importarSolicitudesChequesAMizar..." + this, Level.WARNING, e);
			onError(e);
		}
	
	}


	@Override
	public SolicitudChequeSiniestro solicitarChequeMizar(Long identificador,
			OrigenSolicitud tipo) throws Exception {
		String spName 							=null;
		String parametro 						=null;
		StoredProcedureHelper storedHelper 		=null;
		if(tipo==null){
			throw new Exception(String.format("Error interno debe especificarse el tipo de solicitud"));
		}else if (tipo.toString().equalsIgnoreCase(OrigenSolicitud.LIQUIDACION_SINIESTRO_BENEFICIARIO.toString())   ||  
					tipo.toString().equalsIgnoreCase(OrigenSolicitud.LIQUIDACION_SINIESTRO_PROVEEDOR.toString())){
			spName 								= "MIDAS.PKG_CONTABILIDAD.SP_GENERA_CHEQUE_SIN"; 
			parametro 							= "pIdLiqegreso"; 
		}else if (tipo.toString().equalsIgnoreCase(OrigenSolicitud.DEVOLUCION_INGRESO.toString())){
			spName 								= "MIDAS.PKG_CONTABILIDAD.SPGENERACHEQUExDEV";
			parametro 							= "pAplicacionId";
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("devolucion.id", identificador);
			List<AplicacionCobranzaDevolucion>  list=this.entidadDao.findByPropertiesWithOrder(AplicacionCobranzaDevolucion.class, params, "id.aplicacionId desc");
			if (CollectionUtils.isEmpty(list)) {
				throw new Exception(String.format("No se encuentran devoluciones de ingresos por aplicar "));
			}
			AplicacionCobranzaDevolucion aplicacion = list.get(0);
			if(aplicacion.getAplicacion()==null || !aplicacion.getAplicacion().getTipo().toString().equalsIgnoreCase(AplicacionCobranza.TipoAplicacionCobranza.D.toString())){
				throw new Exception(String.format("La aplicacion cobranza no proviene de una devolucion de ingreso (cheque)"));
			}
			identificador=aplicacion.getAplicacion().getId();
		}else{
			throw new Exception(String.format("Error interno, debe especificarse el tipo de solicitud"));
		}
		GeneraChequeDTO resultado = new GeneraChequeDTO();
        try {
			LogDeMidasEJB3.log("Entrando a"+spName +"...Tipo:" +tipo.toString() +" "+ this, Level.INFO, null);			
            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceParametro(parametro, identificador);
            storedHelper.estableceMapeoResultados(
					GeneraChequeDTO.class.getCanonicalName(),
					"idsolcheque", "idsolcheque");
            resultado = (GeneraChequeDTO) storedHelper.obtieneResultadoSencillo();
            LogDeMidasEJB3.log("Saliendo de "+spName +", identificador - " + identificador + this, Level.INFO, null);
        } catch (Exception e) {
             if (storedHelper != null) {
            	   String descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log("Excepcion general ..."+descErr, Level.WARNING, e);
                   if(!StringUtil.isEmpty(descErr))
                	   throw new Exception(String.format("Excepcion al generar la solicitud : "+descErr));
                   else
                	   throw new Exception(String.format("Excepcion al generar la solicitud."));
             }else{
            	 LogDeMidasInterfaz.log("Excepcion general en CONTABILIZA_LIQUIDACION...", Level.WARNING, e);
				 throw new Exception(String.format("Error al enviar la solicitud de cheque "));
             }
        }
        if (null==resultado || null==resultado.getIdsolcheque()){
     	   	throw new Exception(String.format("No se Genero el numero de Solicitud de Cheque"));
        }else {
        	SolicitudChequeSiniestro solicitudCheque=null;
        	solicitudCheque=this.entidadDao.findById(SolicitudChequeSiniestro.class, resultado.getIdsolcheque());
        	if(null!=solicitudCheque){
            	return solicitudCheque;
        	}else {
         	   	throw new Exception(String.format("No se Genero el numero de Solicitud de Cheque"));
        	}
        }
	
	}


	/**
	 * Actualiza en MIZAR los detalles de una solicitud de cheque de un calculo, es decir, una solicitud de cheque puede tener
	 * al menos un o mas detalles de solicitud de cheque, estos se actualizan su estatus por medio de un stored procedure de SQLSERVER
	 * utilizando el datasource de Mizar
	 * @param cheque
	 * @param idCalculo
	 * @throws MidasException
	 */
	
	

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizarDetalleSolicitudChequeMizar(
			SolicitudChequeSiniestroDetalle cheque, Long idSolicitudCheque,
			Long idCalculo, Long claveAgente, Integer pctIVAAcreditable,
			Integer pctIVARetenido, Integer pctISRRetenido,
			Integer claveTipoOperacion) throws MidasException {
		StoredProcedureHelper storedHelper = null;
		String sp="spinsDetail";
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitudChequesMizarDaoImpl.actualizarDetalleSolicitudChequeMizar...Calculo|Id_Sesion:["+idCalculo+"]/Sol_Cheque:["+idSolicitudCheque+"]/Rubro:["+cheque.getRubro()+"]" + this, Level.INFO, null);
			LogDeMidasInterfaz.log("id_sesion:["+idCalculo+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("id_sol_cheque:["+idSolicitudCheque+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("auxiliar:["+claveAgente+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("cvel_t_rubro:["+(cheque.getRubro())+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("id_cuenta_cont:["+cheque.getCuentaContable()+"]", Level.INFO, null);
			//LogDeMidasInterfaz.log("cve_c_a:["+cheque.getConcepto()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("imp_pago:["+cheque.getImporte()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("cuenta_contable:["+cheque.getCuentaContable()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("iva_pct:["+pctIVAAcreditable+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("pct_iva_ret:["+pctIVARetenido+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("pct_isr_ret:["+pctISRRetenido+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("tipo_operacion:["+ StringUtils.leftPad(claveTipoOperacion.toString(), 2, "0")+"]", Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp,StoredProcedureHelper.DATASOURCE_MIZAR);
			
			storedHelper.estableceParametro("id_sesion",idCalculo);//integer
			storedHelper.estableceParametro("id_sol_cheque",idSolicitudCheque);//integer
			storedHelper.estableceParametro("auxiliar", new Integer (claveAgente.toString()));//integer
			storedHelper.estableceParametro("iva_pct",pctIVAAcreditable);//integer
			storedHelper.estableceParametro("pct_iva_ret",pctIVARetenido);//integer
			storedHelper.estableceParametro("pct_isr_ret",pctISRRetenido);//integer
			storedHelper.estableceParametro("tipo_operacion",StringUtils.leftPad(claveTipoOperacion.toString(), 2, "0"));//varchar(2),
			storedHelper.estableceParametro("cvel_t_rubro",(cheque.getRubro()));//varchar(16),
			storedHelper.estableceParametro("id_cuenta_cont",new Integer(0));//integer
			storedHelper.estableceParametro("cve_c_a",cheque.getNaturaleza());//char(1),
			storedHelper.estableceParametro("imp_pago",cheque.getImporte());//money
			storedHelper.estableceParametro("cuenta_contable",cheque.getCuentaContable());//varchar(30),
			storedHelper.ejecutaActualizarSQLServer();
			LogDeMidasInterfaz.log("Saliendo de SolicitudChequesMizarDaoImpl.actualizarDetalleSolicitudChequeMizar..." + this, Level.INFO, null);
		} catch (SQLException e) {
			//FIXME
			e.printStackTrace();
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de SolicitudChequesMizarDaoImpl.actualizarDetalleSolicitudChequeMizar..." + this, Level.WARNING, e);
			onError(e);
		}catch(NullPointerException e){
			//FIXME
			e.printStackTrace();
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.actualizarDetalleSolicitudChequeMizar [solicitud de cheque:"+idSolicitudCheque+" |calculo:"+idCalculo+"]..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			//FIXME
			e.printStackTrace();
			LogDeMidasInterfaz.log("Excepcion general en SolicitudChequesMizarDaoImpl.actualizarDetalleSolicitudChequeMizar..." + this, Level.WARNING, e);
			onError(e);
		}
	}
	
	
	

}
