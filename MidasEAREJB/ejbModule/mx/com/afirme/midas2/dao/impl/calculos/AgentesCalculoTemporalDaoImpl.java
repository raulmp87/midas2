package mx.com.afirme.midas2.dao.impl.calculos;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.calculos.AgentesCalculoTemporalDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.calculos.AgentesCalculoTemporal;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.MidasException;
@Stateless
public class AgentesCalculoTemporalDaoImpl extends EntidadDaoImpl implements AgentesCalculoTemporalDao{
	private EntidadService entidadService;

	@Override
	public void delete(Long idCalculo) throws MidasException {
		if(isNull(idCalculo)){
			throw new MidasException("Favor de proporcionar la clave del calculo para eliminar de la tabla temporal");
		}
		List<AgentesCalculoTemporal> list=findByProperty(AgentesCalculoTemporal.class, "idCalculo", idCalculo);
		if(!isEmptyList(list)){
			for(AgentesCalculoTemporal calculoTemporal:list){
				remove(calculoTemporal);
			}
		}
	}

	@Override
	public List<AgentesCalculoTemporal> findByCatalogo(String catalogo, Long idCalculo) throws MidasException {
		List<AgentesCalculoTemporal> list=new ArrayList<AgentesCalculoTemporal>();
		if(isNull(idCalculo)){
			throw new MidasException("Favor de proporcionar la clave del calculo para eliminar de la tabla temporal");
		}
		if(!isValid(catalogo)){
			throw new MidasException("Favor de proporcionar el catalogo que desea obtener");
		}
		Map<String, Object> params=new HashMap<String, Object>();
		params.put("idCalculo",idCalculo);
		params.put("catalogo", catalogo);
		list=findByProperties(AgentesCalculoTemporal.class,params);
		return list;
	}

	@Override
	public Long getNextIdCalculo() {
		Long idCalculo=null;
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT MIDAS.idGt_AgentesCalcTemporal_seq.nextval as idCalculo from dual ");
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			idCalculo=(isNotNull(lista.get(0)))?lista.get(0).longValue():null;
		}
		return idCalculo;
	}

	@Override
	public Long save(AgentesCalculoTemporal calculo) throws MidasException {
		if(isNull(calculo)){
			onError("Favor ");
		}
		calculo=entidadService.save(calculo);
		return calculo.getId();
	}

	@Override
	public Long save(Long idRegistro, String catalogo,Long idCalculo)throws MidasException {
		if(isNull(idRegistro)){
			onError("Favor de proporcionar el id de registro");
		}
		if(!isValid(catalogo)){
			onError("Favor de proporcionar el catalogo");
		}
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo");
		}
		AgentesCalculoTemporal calculo=new AgentesCalculoTemporal();
		calculo.setCatalogo(catalogo);
		calculo.setIdRegistro(idRegistro);
		calculo.setIdCalculo(idCalculo);
		return save(calculo);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void saveAll(List<Long> idRegistros, String catalogo,Long idCalculo) throws MidasException {
		if(isEmptyList(idRegistros)){
			onError("Favor de proporcionar los registros a guardar");
		}
		if(!isValid(catalogo)){
			onError("Favor de proporcionar el catalogo");
		}
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo");
		}
		for(Long idRegistro:idRegistros){
//			save(idRegistro, catalogo, idCalculo);
			AgentesCalculoTemporal calculo=new AgentesCalculoTemporal();
			calculo.setCatalogo(catalogo);
			calculo.setIdRegistro(idRegistro);
			calculo.setIdCalculo(idCalculo);
			entidadService.save(calculo);
		}
		entityManager.flush();
	}
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	private void onError(String msg)throws MidasException{
		throw new MidasException(msg);
	}
}
