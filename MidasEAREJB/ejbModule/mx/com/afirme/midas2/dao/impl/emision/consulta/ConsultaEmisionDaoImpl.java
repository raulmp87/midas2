package mx.com.afirme.midas2.dao.impl.emision.consulta;

import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.emision.consulta.ConsultaEmisionDao;
import mx.com.afirme.midas2.dto.emision.consulta.Consulta;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaAgente;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaAnexo;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaCobertura;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaCobranza;
import mx.com.afirme.midas2.dto.emision.consulta.ConsultaEndoso;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ConsultaEmisionDaoImpl implements ConsultaEmisionDao {
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<Consulta> getConsultaEmision(Consulta filtro) {
		
		Query query = null;
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		String sWhere = "";
		String queryString = "";
		String queryStringCount = "";
		String sOrderBy = "";
		Long totalRegistros = 0L;
		
		try {
				
			queryString = "SELECT model FROM " + filtro.getClass().getSimpleName() + " model";
			
			sWhere = generateWhereString (filtro, listaParametrosValidos, false);
			
			sOrderBy = generateOrderByString (filtro, false);
			
			if (Utilerias.esAtributoQueryValido(sWhere)) {
				queryString = queryString.concat(" WHERE ").concat(sWhere);				
			}
			
			queryString = queryString.concat(sOrderBy);
			
			//Si es la primera vez que se consulta
			if(filtro.getPrimerRegistroACargar() == null || filtro.getNumeroMaximoRegistrosACargar() == null
					|| filtro.getPrimerRegistroACargar().intValue() == 0) {
				//Se obtiene el total de registros
				queryStringCount = "SELECT COUNT(model) FROM " + filtro.getClass().getSimpleName() + " model";
				
				if (Utilerias.esAtributoQueryValido(sWhere)) {
					queryStringCount = queryStringCount.concat(" WHERE ").concat(sWhere);				
				}
				
				query = entityManager.createQuery(queryStringCount);
				Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				totalRegistros = (Long)query.getSingleResult();
				filtro.setTotalRegistros(totalRegistros);
			}
			
			//Se realiza la consulta
			query = entityManager.createQuery(queryString);
			
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			
			if(filtro.getPrimerRegistroACargar() != null && filtro.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(filtro.getPrimerRegistroACargar().intValue());
				query.setMaxResults(filtro.getNumeroMaximoRegistrosACargar().intValue());
			}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return query.getResultList();
			
		
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public List<Consulta> getConsultaCobranza(ConsultaCobranza filtro) {
		
		Query query = null;
		
		String sWhere = "";
		String sBody = "";
		String queryString = "";
		String queryStringCount = "";
		String sOrderBy = "";
		BigDecimal totalRegistros = BigDecimal.ZERO;
		
		try {
			
			sWhere = " DESG.ID_COTIZACION  = MIDAS.FN_AUT_CONVERT_SEYCOS (" + filtro.getPolizaId() + ","+ 
			" NULL,"+ 
			" NULL,"+
			" 1)";
			
			if (filtro.getNumeroEndoso() != null) {
				sWhere += " AND PROG.ID_DOCTO_VERS  = MIDAS.FN_AUT_CONVERT_SEYCOS (" + filtro.getPolizaId() + ","+ 
				filtro.getNumeroEndoso() + ","+ 
				" NULL,"+
				" 2)";
			}
			
			if (filtro.getNumeroInciso() != null) {
				sWhere += " AND DESG.ID_INCISO  = MIDAS.FN_AUT_CONVERT_SEYCOS (" + filtro.getPolizaId() + ","+ 
				" NULL,"+ 
				filtro.getNumeroInciso() +","+
				" 0)";
			}
			
			sOrderBy = generateOrderByString (filtro, true);
			
			sOrderBy = sOrderBy.replaceAll("serie", "REC.NUM_EXHIBICION || '/' || PROG.NUM_RECIBOS");
			sOrderBy = sOrderBy.replaceAll("numeroRecibo", "REC.NUM_FOLIO_RBO");
			sOrderBy = sOrderBy.replaceAll("monto", "SUM(DESG.IMP_PRIMA_TOTAL)");
			sOrderBy = sOrderBy.replaceAll("fechaInicioVigencia", "TO_CHAR(REC.F_CUBRE_DESDE, 'DD/MM/YYYY')");
			sOrderBy = sOrderBy.replaceAll("fechaFinVigencia", "TO_CHAR(REC.F_CUBRE_HASTA, 'DD/MM/YYYY')");
			sOrderBy = sOrderBy.replaceAll("estatus", "MIDAS.FN_AUT_GET_DESC_SIT_RECIBO(REC.SIT_RECIBO)");
			sOrderBy = sOrderBy.replaceAll("polizaId", "MIDAS.FN_AUT_CONVERT_MIDAS (DESG.ID_COTIZACION,DESG.ID_LIN_NEGOCIO,DESG.ID_INCISO,1)");
			sOrderBy = sOrderBy.replaceAll("numeroEndoso", "NVL (MIDAS.FN_AUT_CONVERT_MIDAS (PROG.ID_DOCTO_VERS,DESG.ID_LIN_NEGOCIO,DESG.ID_INCISO,2),0)");
			sOrderBy = sOrderBy.replaceAll("numeroInciso", "NVL (MIDAS.FN_AUT_CONVERT_MIDAS (DESG.ID_COTIZACION,DESG.ID_LIN_NEGOCIO,DESG.ID_INCISO,0),0)");
			
			sBody = " FROM"+
			" SEYCOS.POL_PROG_PAGO PROG"+
			" INNER JOIN (SEYCOS.POL_RECIBO REC"+
			" INNER JOIN SEYCOS.POL_DESG_RECIBO DESG"+
			" ON DESG.ID_RECIBO = REC.ID_RECIBO"+
			" AND DESG.ID_COTIZACION = REC.ID_COTIZACION"+
			" AND DESG.F_TER_REG = TO_DATE('31/12/4712','DD/MM/YYYY')"+
			" ) ON REC.ID_PROG_PAGO = PROG.ID_PROG_PAGO"+
			" AND REC.ID_COTIZACION = PROG.ID_COTIZACION"+
			" AND REC.F_TER_REG = TO_DATE('31/12/4712','DD/MM/YYYY')"+
			" INNER JOIN SEYCOS.CONV_MIDAS_SEYCOS CONV"+
			" ON CONV.ID_SEYCOS = PROG.ID_COTIZACION"+
			" AND CONV.TIPO IN ('POLIZA','ENDOSO')"+  
			" WHERE"+ sWhere +
			" GROUP BY"+
			" RTRIM(LTRIM(REC.ID_RECIBO)) || '-' || NVL (MIDAS.FN_AUT_CONVERT_MIDAS (PROG.ID_DOCTO_VERS, DESG.ID_LIN_NEGOCIO, DESG.ID_INCISO, 2),0)," +
			" REC.NUM_EXHIBICION || '/' || PROG.NUM_RECIBOS,"+
			" REC.NUM_FOLIO_RBO,"+
			" TO_CHAR(REC.F_CUBRE_DESDE, 'DD/MM/YYYY'),"+
			" TO_CHAR(REC.F_CUBRE_HASTA, 'DD/MM/YYYY'),"+
			" MIDAS.FN_AUT_GET_DESC_SIT_RECIBO(REC.SIT_RECIBO),"+
			" MIDAS.FN_AUT_CONVERT_MIDAS (DESG.ID_COTIZACION,"+ 
			" DESG.ID_LIN_NEGOCIO,"+ 
			" DESG.ID_INCISO,"+
			" 1),"+
			" NVL (MIDAS.FN_AUT_CONVERT_MIDAS (PROG.ID_DOCTO_VERS,"+ 
			" DESG.ID_LIN_NEGOCIO,"+ 
			" DESG.ID_INCISO,"+
			" 2),0),"+
			" NVL (MIDAS.FN_AUT_CONVERT_MIDAS (DESG.ID_COTIZACION,"+ 
			" DESG.ID_LIN_NEGOCIO,"+ 
			" DESG.ID_INCISO,"+
			" 0),0)";
		
			queryString = "SELECT"+
			" RTRIM(LTRIM(REC.ID_RECIBO)) || '-' || NVL (MIDAS.FN_AUT_CONVERT_MIDAS (PROG.ID_DOCTO_VERS, DESG.ID_LIN_NEGOCIO, DESG.ID_INCISO, 2),0) AS indice," +
		    " REC.NUM_EXHIBICION || '/' || PROG.NUM_RECIBOS AS serie,"+
			" REC.NUM_FOLIO_RBO AS numeroRecibo,"+
			" SUM(DESG.IMP_PRIMA_TOTAL) AS monto,"+ 
			" TO_CHAR(REC.F_CUBRE_DESDE, 'DD/MM/YYYY') AS fechaInicioVigencia,"+
			" TO_CHAR(REC.F_CUBRE_HASTA, 'DD/MM/YYYY') AS fechaFinVigencia,"+
			" MIDAS.FN_AUT_GET_DESC_SIT_RECIBO(REC.SIT_RECIBO) AS estatus,"+
			" MIDAS.FN_AUT_CONVERT_MIDAS (DESG.ID_COTIZACION,"+ 
			" DESG.ID_LIN_NEGOCIO,"+ 
			" DESG.ID_INCISO,"+
			" 1) AS polizaId,"+
			" NVL (MIDAS.FN_AUT_CONVERT_MIDAS (PROG.ID_DOCTO_VERS,"+ 
			" DESG.ID_LIN_NEGOCIO,"+ 
			" DESG.ID_INCISO,"+
			" 2),0) AS numeroEndoso,"+
			" NVL (MIDAS.FN_AUT_CONVERT_MIDAS (DESG.ID_COTIZACION,"+ 
			" DESG.ID_LIN_NEGOCIO,"+ 
			" DESG.ID_INCISO,"+
			" 0),0) AS numeroInciso" + sBody;
			
			//Si es la primera vez que se consulta
			if(filtro.getPrimerRegistroACargar() == null || filtro.getNumeroMaximoRegistrosACargar() == null
					|| filtro.getPrimerRegistroACargar().intValue() == 0) {
				//Se obtiene el total de registros
				queryStringCount = "SELECT COUNT (1) AS TOTAL FROM (" + queryString + ")";
				
				query = entityManager.createNativeQuery(queryStringCount);
				//query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				totalRegistros = (BigDecimal)query.getSingleResult();
				filtro.setTotalRegistros(totalRegistros.longValue());
			}
			
			//Se realiza la consulta
			query = entityManager.createNativeQuery(queryString + sOrderBy, ConsultaCobranza.class);
			
			if(filtro.getPrimerRegistroACargar() != null && filtro.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(filtro.getPrimerRegistroACargar().intValue());
				query.setMaxResults(filtro.getNumeroMaximoRegistrosACargar().intValue());
			}
			
			return query.getResultList();
		
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		
	}
	
	
	@PersistenceContext
	private EntityManager entityManager;


	@Override
	public List<Consulta> getConsultaCobertura(ConsultaCobertura filtro) {
				
		String sBody = " SELECT" +
	    " 	COB.IDTOCOBERTURA coberturaId," +
	    " 	INCC.NUMERO numeroInciso," +
	    " 	POL.IDTOPOLIZA polizaId," +
	    "   COB.NOMBRECOMERCIALCOBERTURA nombre," +
	    "   MIDAS.FN_AUT_GET_DEDUCIBLE_COBERTURA(COBC.ID) deducible," +
	    "   MIDAS.FN_AUT_GET_SA_COBERTURA(COBC.ID) sumaAsegurada," +
	    "   COBB.VALORPRIMANETA primaNeta" +
	    " FROM" +
	    "    MIDAS.MCOBERTURASECCIONB COBB" +
	    "    INNER JOIN (MIDAS.MCOBERTURASECCIONC COBC" +
	    "                INNER JOIN (MIDAS.MSECCIONINCISOC SECC" +
	    "                            INNER JOIN (MIDAS.MINCISOC INCC" +
	    "                                        INNER JOIN (MIDAS.MCOTIZACIONC COTC" +
	    "                                                    INNER JOIN MIDAS.TOPOLIZA POL" +
	    "                                                    ON POL.IDTOCOTIZACION = COTC.NUMERO" +
	    "                                        ) ON COTC.ID = INCC.MCOTIZACIONC_ID" +
	    "                            ) ON INCC.ID = SECC.MINCISOC_ID" +
	    "                ) ON SECC.ID = COBC.MSECCIONINCISOC_ID" +
	    "                INNER JOIN MIDAS.TOCOBERTURA COB" +
	    "                ON COB.IDTOCOBERTURA = COBC.COBERTURA_ID" +
	    "    ) ON COBB.ID = MIDAS.FN_AUT_GET_BITEMPORAL(COBC.ID, 'COB')" +
	    " WHERE" +
	    "    COBB.CLAVECONTRATO = 1";
		
		return getConsultaNativeQuery (filtro, sBody);
	}

	@Override
	public List<Consulta> getConsultaEndoso(ConsultaEndoso filtro) {
				
		String sBody = " SELECT"+
	    "    ENDOSO.NUMEROENDOSO numeroEndoso,"+
	    "    ENDOSO.IDTOPOLIZA polizaId,"+
	    "    MIDAS.FN_AUT_GET_DESC_VALORFIJO(331, SOL.CLAVETIPOENDOSO) tipoEndoso,"+
	    "    MIDAS.FN_AUT_GET_DESC_VALORFIJO(332, SOL.CLAVEMOTIVOENDOSO) motivo,"+
	    "    MIDAS.FN_AUT_GET_STATUS_ENDOSO(ENDOSO.FECHAINICIOVIGENCIA, ENDOSO.FECHAFINVIGENCIA) estatus,"+
	    "    TO_CHAR(ENDOSO.RECORDFROM, 'DD/MM/YYYY') fechaEmision,"+
	    "    TO_CHAR(ENDOSO.FECHAINICIOVIGENCIA, 'DD/MM/YYYY') fechaInicioVigencia,"+
	    "    TO_CHAR(ENDOSO.FECHAFINVIGENCIA, 'DD/MM/YYYY') fechaFinVigencia,"+
	    "    ENDOSO.VALORPRIMANETA primaNeta,"+
	    "    ENDOSO.VALORDERECHOS derechos,"+
	    "    ENDOSO.VALORIVA iva,"+
	    "    ENDOSO.VALORRECARGOPAGOFRAC recargo,"+
	    "    ENDOSO.VALORPRIMATOTAL primaTotal"+
	    " FROM"+
	    "    MIDAS.TOENDOSO ENDOSO"+
	    "    INNER JOIN (MIDAS.CONTROLENDOSOCOT CONTROL"+
	    "                INNER JOIN MIDAS.TOSOLICITUD SOL"+
	    "                ON SOL.IDTOSOLICITUD = CONTROL.SOLICITUD_ID"+
	    "    ) ON CONTROL.COTIZACION_ID = ENDOSO.IDTOCOTIZACION"+
	    "      AND CONTROL.VALIDFROM = ENDOSO.VALIDFROM"+
	    "      AND CONTROL.RECORDFROM = ENDOSO.RECORDFROM";
		
		return getConsultaNativeQuery (filtro, sBody);

	}
	
	@Override
	public List<Consulta> getConsultaAnexo(ConsultaAnexo filtro) {
				
		String sBody = " SELECT" +
		"    TEXTOB.ID id," +
		"    'TEXTO' tipo," +
		"    POL.IDTOPOLIZA polizaId," +
		"    TEXTOB.DESCRIPCIONTEXTO descripcion" +
		" FROM" +
		"    MIDAS.MTEXADICIONALCOTB TEXTOB" +
		"    INNER JOIN (MIDAS.MTEXADICIONALCOTC TEXTOC" +
		"                INNER JOIN (MIDAS.MCOTIZACIONC COTC" +
		"                            INNER JOIN MIDAS.TOPOLIZA POL" +
		"                            ON POL.IDTOCOTIZACION = COTC.NUMERO" +
		"                ) ON COTC.ID = TEXTOC.MCOTIZACIONC_ID" +
		"    ) ON TEXTOB.ID = MIDAS.FN_AUT_GET_BITEMPORAL(TEXTOC.ID, 'TEXTO')" +
		" UNION" +
		" SELECT" +
		"    ANEXOB.ID id," +
		"    'ANEXO' tipo," +
		"    POL.IDTOPOLIZA polizaId," +
		"    ANEXOB.DESCRIPCIONDOCUMENTOANEXO descripcion" +
		" FROM" +
		"    MIDAS.MDOCANEXOCOTB ANEXOB" +
		"    INNER JOIN (MIDAS.MDOCANEXOCOTC ANEXOC" +
		"                INNER JOIN (MIDAS.MCOTIZACIONC COTC" +
		"                            INNER JOIN MIDAS.TOPOLIZA POL" +
		"                            ON POL.IDTOCOTIZACION = COTC.NUMERO" +
		"                ) ON COTC.ID = ANEXOC.MCOTIZACIONC_ID" +
		"    ) ON ANEXOB.ID = MIDAS.FN_AUT_GET_BITEMPORAL(ANEXOC.ID, 'ANEXO')";
		
		
		return getConsultaNativeQuery (filtro, sBody);
	}
	
	@SuppressWarnings("unchecked")
	private List<Consulta> getConsultaNativeQuery(Consulta filtro, String sBody) {
		
		Query query = null;
		
		String sWhere = "";
		String queryString = "";
		String queryStringCount = "";
		String sOrderBy = "";
		BigDecimal totalRegistros = BigDecimal.ZERO;
		
		try {
			
			sWhere = generateWhereString (filtro, null, true);
			
			sOrderBy = generateOrderByString (filtro, true);
			
			sBody = " FROM ( "+ sBody + " )"+
		    " WHERE "+ sWhere;
		
			queryString = "SELECT * " + sBody + sOrderBy;
			
			//Si es la primera vez que se consulta
			if(filtro.getPrimerRegistroACargar() == null || filtro.getNumeroMaximoRegistrosACargar() == null
					|| filtro.getPrimerRegistroACargar().intValue() == 0) {
				//Se obtiene el total de registros
				queryStringCount = "SELECT COUNT (1) " + sBody;
				
				query = entityManager.createNativeQuery(queryStringCount);
				totalRegistros = (BigDecimal)query.getSingleResult();
				filtro.setTotalRegistros(totalRegistros.longValue());
			}
			
			//Se realiza la consulta
			query = entityManager.createNativeQuery(queryString, filtro.getClass());
			
			if(filtro.getPrimerRegistroACargar() != null && filtro.getNumeroMaximoRegistrosACargar() != null) {
				query.setFirstResult(filtro.getPrimerRegistroACargar().intValue());
				query.setMaxResults(filtro.getNumeroMaximoRegistrosACargar().intValue());
			}
			
			return query.getResultList();
		
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
	}
	
	
	@SuppressWarnings("rawtypes")
	private String generateWhereString(Consulta filtro, List<HashMap> listaParametrosValidos, boolean esNativo) {
		StringBuilder sWhere = new StringBuilder("");
		
		try {
			
			PropertyUtilsBean utilsBean = new PropertyUtilsBean();
			
			for (Field field : filtro.getClass().getDeclaredFields()) {
				
				if (!field.getType().isArray() 
						&& !field.getName().equals("serialVersionUID")
						&& !field.getName().startsWith("_persistence")
				) {
					
					if(utilsBean.getProperty(filtro, field.getName()) != null) {
						
						if ((field.getType().equals(String.class) 
								&& !((String)utilsBean.getProperty(filtro, field.getName())).trim().equals(""))
							|| (!field.getType().equals(String.class))) {
							
							if(sWhere.toString().equals("")) {
								sWhere.append("");
							} else {
								sWhere.append(" AND ");
							}
							
							if (field.getType().equals(String.class)) {
								
								//Se revisa que sea id de Agente para usar un filtro multiple
								if ((filtro instanceof ConsultaAgente && field.getName().equals("id"))
										|| field.getName().equals("agenteId")) {
									
									if (esNativo) {
										sWhere.append(" ");
									} else {
										sWhere.append(" model.");
									}
									
									sWhere.append(field.getName()).append(" IN (").append( ((String)utilsBean.getProperty(filtro, field.getName())).trim()).append( ")");
									
								} else {
									if (esNativo) {
										sWhere.append(" ").append(field.getName()).append(" LIKE '%").append(((String)utilsBean.getProperty(filtro, field.getName())).trim()).append("%'");
									} else {
										sWhere.append(" model.").append(field.getName()).append(" LIKE :" ).append(field.getName());
										Utilerias.agregaHashLista(listaParametrosValidos, 
												field.getName(), 
												"%" + ((String)utilsBean.getProperty(filtro, field.getName())).trim() + "%");
									}
								}
								
							} else {
								
								if (esNativo) {
									sWhere.append(" ").append(field.getName()).append(" = ").append(utilsBean.getProperty(filtro, field.getName()));
								} else {
									sWhere.append(" model.").append(field.getName()).append(" = :").append(field.getName());
									Utilerias.agregaHashLista(listaParametrosValidos, 
											field.getName(), utilsBean.getProperty(filtro, field.getName()));
								}
								
							}
							
						}
					}
				}
			}
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return sWhere.toString();
		
	}
	
	private String generateOrderByString(Consulta filtro, boolean esNativo) {
		
		String sOrderBy = "";
		
		if (filtro != null && filtro.getOrderBy() != null && !filtro.getOrderBy().isEmpty()) {
			
			String propiedad = null;
			int lastDot = 0;
			String direccion = "";
			
			//Se deja solo el nombre de la propiedad a ordenar
			propiedad = filtro.getOrderBy().trim();
			
			lastDot = propiedad.lastIndexOf(".");
			
			propiedad = propiedad.substring(lastDot + 1);
			
			if (!esNativo) {
				propiedad = "model." + propiedad;
			}
			
			if (filtro.getDirect().trim().equals("des")) {
				direccion = "DESC";
			} else {
				direccion = "ASC";
			}
			
			sOrderBy = " ORDER BY " + propiedad  + " " + direccion;
			
		}
				
		return sOrderBy;
		
	}
	
}
