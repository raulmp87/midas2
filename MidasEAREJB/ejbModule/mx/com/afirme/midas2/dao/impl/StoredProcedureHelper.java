package mx.com.afirme.midas2.dao.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;

import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;

/**
 * Class to help implementing Stored Procedure calls with Spring framework
 * @author aavacor
 *
 */

public class StoredProcedureHelper extends StoredProcedure {

	protected List<SqlParameter> inputParameterList;
	protected List<SqlOutParameter> outputParameterList;
	protected Object[] outputValues;
	
	/**
	 * Constructor
	 * @param dataSource Oracle DataSource
	 * @param procedureFullName Full name of procedure, including the package name
	 * @param inputParameterList Input Parameters for Stored Procedure, in the same order as they are declared in the SP itself
	 * @param outputParameterList Output Parameters for Stored Procedure, in the same order as they are declared in the SP itself
	 */
	public StoredProcedureHelper(DataSource dataSource, String procedureFullName, 
			List<SqlParameter> inputParameterList, List<SqlOutParameter> outputParameterList){
		
		super(dataSource, procedureFullName);
		
		List<Object> outputObjects = new ArrayList<Object>();
		
		for (SqlParameter inputParameter : inputParameterList) {
			declareParameter(inputParameter);
		}
		
		for (SqlOutParameter outputParameter : outputParameterList) {
			declareParameter(outputParameter);
			outputObjects.add(new Object());
		}
		
		this.inputParameterList = inputParameterList;
		this.outputParameterList = outputParameterList;
		this.outputValues = outputObjects.toArray();
		
	}
	
	/**
	 * Executes a Stored Procedure Call
	 * @param inputValues Input values as parameters of Stored Procedure call
	 */
	public void executeSP(Object[] inputValues) {
		executeStoredProcedure (inputValues);
	}
	
	/**
	 * Executes a Stored Procedure Call
	 * @param inputValues Input values as parameters of Stored Procedure call
	 * @return List specified by a CURSOR type output parameter
	 */
	@SuppressWarnings({"rawtypes" })
	public List executeSPForList(Object[] inputValues){	
		
		//In this case it has to be a CURSOR output parameter to retrieve
		List resultList = executeStoredProcedure (inputValues);
		
		return resultList;
		
	}
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List executeStoredProcedure(Object[] inputValues) {
		
		List resultList = null;
		
		Map inputParams = prepareExecute(inputValues);
		
		Map outputParams= execute(inputParams);
		
		
		int index = 0;
		for (SqlOutParameter outputParameter : outputParameterList) {
			
			if (outputParameter.getSqlType() == OracleTypes.CURSOR) {
				resultList = (List) outputParams.get(outputParameter.getName());
			} else {
				if (outputValues != null)
					outputValues[index] = outputParams.get(outputParameter.getName());
			}
			index++;
		}
		
		return resultList;
		
	}
	
	
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private Map prepareExecute(Object[] inputValues) {
		
		//Rule is input parameters has to be in the same order as they are declared in the SP itself
		Map inputParams =new HashMap();
		
		int index = 0;
		
		for (SqlParameter inputParameter : this.inputParameterList) {
			inputParams.put(inputParameter.getName(), inputValues[index]);
			index++;
		}
		
		return inputParams;
		
	}
	
	

	/**
	 * Get the output values of Stored Procedure call
	 * @return Output values of Stored Procedure call
	 */
	public Object[] getOutputValues() {
		return outputValues;
	}
	
	
}
