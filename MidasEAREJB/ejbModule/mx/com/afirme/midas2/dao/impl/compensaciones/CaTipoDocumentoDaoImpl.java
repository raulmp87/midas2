/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoDocumentoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoDocumento;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoDocumentoDaoImpl implements CaTipoDocumentoDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoDocumentoDaoImpl.class);
	
	public void save(CaTipoDocumento entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaTipoDocumento entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoDocumento.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoDocumento entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoDocumento entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoDocumento entity to update
	 @return CaTipoDocumento the persisted CaTipoDocumento entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoDocumento update(CaTipoDocumento entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoDocumento result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoDocumento 	::		CaTipoDocumentoDaoImpl	::	update	::	ERROR	::	",re);
	        throw re;
        }
    }
    
    public CaTipoDocumento findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoDocumentoDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoDocumento instance = entityManager.find(CaTipoDocumento.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoDocumentoDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoDocumentoDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoDocumento> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoDocumentoDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoDocumento model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoDocumentoDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoDocumentoDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoDocumento> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoDocumento> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoDocumento> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoDocumento> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoDocumento> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoDocumentoDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoDocumento model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoDocumentoDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoDocumentoDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
