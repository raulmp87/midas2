package mx.com.afirme.midas2.dao.impl.tarifa;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaAutoRangoSADao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoRangoSA;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoRangoSAId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;

@Stateless
public class TarifaAutoRangoSADaoImpl extends JpaDao<TarifaAutoRangoSAId, TarifaAutoRangoSA> implements TarifaAutoRangoSADao {

	@Override
	public List<TarifaAutoRangoSA> findByFilters(TarifaAutoRangoSA arg0) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaAutoRangoSA> findByTarifaVersionId(
			TarifaVersionId tarifaVersionId) {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT tarifa FROM TarifaAutoRangoSA tarifa ")
		   .append(" WHERE tarifa.id.idMoneda = :idMoneda")
		   .append(" AND tarifa.id.idRiesgo = :idRiesgo")
		   .append(" AND tarifa.id.idConcepto = :idConcepto")
		   .append(" AND tarifa.id.version = :version");
		
		Query query =(TypedQuery<TarifaAutoRangoSA>)entityManager.createQuery(sql.toString());
		
		query.setParameter("idMoneda", tarifaVersionId.getIdMoneda());
		query.setParameter("idRiesgo", tarifaVersionId.getIdRiesgo());
		query.setParameter("idConcepto", tarifaVersionId.getIdConcepto());
		query.setParameter("version", tarifaVersionId.getVersion());
		
		return query.getResultList();
	}

}
