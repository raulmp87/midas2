package mx.com.afirme.midas2.dao.impl.siniestros.pagos.notasDeCredito;

import static mx.com.afirme.midas2.utils.CommonUtils.setQueryParametersByProperties;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.pagos.notasDeCredito.NotasCreditoDAO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.BusquedaNotasCreditoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.FacturaSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro;
import mx.com.afirme.midas2.utils.CommonUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


/**
 * Crear el Query en JPQL para traer aquellas Facturas en Estatus Registrada.
 * 
 * Crear el Query en JPQL para traer aquellas Notas de Credito que cumplan las
 * condiciones  en el objeto de BusquedaCreditoDTO.
 * 
 * Nota: La tabla de notas de credito es la Clase DocumentoFiscal donde el tipo =
 * "NTACRED"
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:45:07 p. m.
 */
@Stateless
public class NotasCreditoDAOImpl extends EntidadDaoImpl implements NotasCreditoDAO {

	private static final Logger log = Logger.getLogger(NotasCreditoDAOImpl.class);

	/**
	 * crear un JPQL din�mico de acuerdo a los valores del DTO que cumpla con las
	 * condiciones seleccionadas por el usuario
	 * 
	 * @param notasCreditoFIltro
	 */
	public List<NotasCreditoRegistro> buscarNotasCeCredito(BusquedaNotasCreditoDTO notasCreditoFiltro){
        Map<String,Object> parameters = new HashMap<String,Object>(); 
          
          parameters.put("tipoDocumentoFiscal", DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue());
          
          if( CommonUtils.isNullOrEmpty( notasCreditoFiltro.getEstatus() ) ){
        	  parameters.put("estatus", null );
          }else{
        	  parameters.put("estatus", notasCreditoFiltro.getEstatus() );
          }
          
          parameters.put("estatusProcesada", DocumentoFiscal.EstatusDocumentoFiscal.PROCESADA.getValue());
          parameters.put("estatusError", DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
          
          parameters.put("fechaBajaNotaCreditoIni", notasCreditoFiltro.getFechaBajaNotaCreditoIni());
          parameters.put("fechaBajaNotaCreditoFin", notasCreditoFiltro.getFechaBajaNotaCreditoFin() );
          
          parameters.put("fechaRegNotaCreditoIni", notasCreditoFiltro.getFechaRegNotaCreditoIni() );
          parameters.put("fechaRegNotaCreditoFin", notasCreditoFiltro.getFechaRegNotaCreditoFin() );
          
          if( CommonUtils.isNullOrEmpty( notasCreditoFiltro.getFolioNotaCredito() ) ){
        	  parameters.put("folioNotaCredito", null );
          }else{
        	  parameters.put("folioNotaCredito", notasCreditoFiltro.getFolioNotaCredito() );
          }
          
          parameters.put("montoNotaCreditoIni", notasCreditoFiltro.getMontoNotaCreditoIni() );
          parameters.put("montoNotaCreditoFin", notasCreditoFiltro.getMontoNotaCreditoFin() );
          
          if( CommonUtils.isNullOrEmpty( notasCreditoFiltro.getNoFactura() ) ){
        	  parameters.put("noFactura", null );
          }else{
        	  parameters.put("noFactura", notasCreditoFiltro.getNoFactura() );
          }
          
          parameters.put("nombreProveedor", (StringUtils.isEmpty(notasCreditoFiltro.getNombreProveedor()))? null : "%" + notasCreditoFiltro.getNombreProveedor() + "%" );
          parameters.put("noProveedor", notasCreditoFiltro.getNoProveedor() );
          
          if( CommonUtils.isNullOrEmpty( notasCreditoFiltro.getSiniestro() ) ){
        	  parameters.put("siniestro", null );
          }else{
        	  parameters.put("siniestro", notasCreditoFiltro.getSiniestro() );
          }
          
          
          StringBuilder queryString = new StringBuilder();
          queryString.append(" SELECT new mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro( ");
          queryString.append(" conjunto.id, notaCredito.id, factura.id, notaCredito.numeroFactura, notaCredito.montoTotal, "); 
          queryString.append(" ordenCompra.idBeneficiario, ");
          queryString.append(" personaMidas.nombre,  ");
          queryString.append(" factura.numeroFactura, ");
          //queryString.append(" ordenCompra.reporteCabina.siniestroCabina.claveOficina , ");
          //queryString.append( "func('CONCAT', siniestroCab.claveOficina, func('CONCAT', '-', func('CONCAT', siniestroCab.consecutivoReporte, func('CONCAT', '-', siniestroCab.anioReporte)))), ");
          queryString.append(" (CASE WHEN (   ");
          queryString.append("  (SELECT COUNT(DISTINCT ( ");
          queryString.append("    func('CONCAT', siniestroCab.claveOficina, func('CONCAT', '-', func('CONCAT', siniestroCab.consecutivoReporte, func('CONCAT', '-', siniestroCab.anioReporte)))) ");
          queryString.append("   )) FROM DocumentoFiscal nota  ");
          queryString.append("    LEFT JOIN nota.conjuntoDeNota.ordenesCompra orden ");
          queryString.append("    LEFT JOIN orden.reporteCabina.siniestroCabina siniestroCab ");
          queryString.append("   WHERE nota.id =  notaCredito.id ) > 1 ");
          queryString.append("  ) THEN ( 'VARIOS' )");
          queryString.append("    ELSE ( ");
          queryString.append("   (SELECT DISTINCT ( ");
          queryString.append("     func('CONCAT', siniestroCab.claveOficina, func('CONCAT', '-', func('CONCAT', siniestroCab.consecutivoReporte, func('CONCAT', '-', siniestroCab.anioReporte)))) ");
          queryString.append("    ) FROM DocumentoFiscal nota  ");
          queryString.append("    LEFT JOIN nota.conjuntoDeNota.ordenesCompra orden ");
          queryString.append("    LEFT JOIN orden.reporteCabina.siniestroCabina siniestroCab ");
          queryString.append("   WHERE nota.id =  notaCredito.id ) ");
          queryString.append("   )  ");
          queryString.append("  END), ");
          queryString.append(" notaCredito.fechaCreacion, notaCredito.fechaBaja, ");
        //  queryString.append(" 'siniestro' ,notaCredito.fechaCreacion, notaCredito.fechaCreacion, ");
          queryString.append(" notaCredito.estatus) ");
          queryString.append(" FROM DocumentoFiscal notaCredito ");
        /*  queryString.append(" JOIN notaCredito.conjuntoDeNota conjunto ");
          queryString.append(" JOIN conjunto.ordenesCompra ordenCompra ");
          queryString.append(" JOIN PrestadorServicio prestador ON (prestador.id = ordenCompra.idBeneficiario )");
        */
          //queryString.append(" LEFT JOIN notaCredito.conjuntoDeNota conjuntoDeNota ");
          queryString.append(" LEFT JOIN notaCredito.conjuntoDeNota conjunto");
          queryString.append(" LEFT JOIN conjunto.factura factura");
          queryString.append(" LEFT JOIN conjunto.ordenesCompra ordenCompra ");
          queryString.append(" LEFT JOIN ordenCompra.reporteCabina reporteCabina ");
          queryString.append(" LEFT JOIN reporteCabina.siniestroCabina siniestroCabina ");
          //queryString.append(" LEFT JOIN ordenCompra.prestador prestador ");
          //queryString.append(" LEFT JOIN ordenCompra.prestador.personaMidas personaMidas ");
          queryString.append(" LEFT JOIN PrestadorServicio prestador ON (prestador.id = ordenCompra.idBeneficiario )");
          queryString.append(" LEFT JOIN prestador.personaMidas personaMidas ");
          
          
          queryString.append(" WHERE  notaCredito.tipo = :tipoDocumentoFiscal ");
          
          queryString.append(" AND   ( notaCredito.estatus NOT IN (:estatusProcesada, :estatusError ) ) ");
          queryString.append(" AND   ( :estatus IS NULL OR notaCredito.estatus = :estatus ) ");
          
          queryString.append(" AND   ( :fechaRegNotaCreditoIni IS  NULL OR FUNC('trunc', notaCredito.fechaCreacion ) >= :fechaRegNotaCreditoIni)  ");
          queryString.append(" AND   ( :fechaRegNotaCreditoFin IS  NULL OR FUNC('trunc', notaCredito.fechaCreacion ) <= :fechaRegNotaCreditoFin)  ");

          queryString.append(" AND   ( :fechaBajaNotaCreditoIni IS  NULL OR FUNC('trunc', notaCredito.fechaBaja ) >= :fechaBajaNotaCreditoIni)  ");
          queryString.append(" AND   ( :fechaBajaNotaCreditoFin IS  NULL OR FUNC('trunc', notaCredito.fechaBaja ) <= :fechaBajaNotaCreditoFin)  ");
          
          queryString.append(" AND   ( :montoNotaCreditoIni IS  NULL OR notaCredito.montoTotal >= :montoNotaCreditoIni ) ");
          queryString.append(" AND   ( :montoNotaCreditoFin IS  NULL OR notaCredito.montoTotal <= :montoNotaCreditoFin )  ");
          
          queryString.append(" AND   ( :folioNotaCredito IS  NULL OR notaCredito.numeroFactura = :folioNotaCredito ) ");
          
          queryString.append(" AND   ( :noFactura IS  NULL OR factura.numeroFactura = :noFactura ) ");
          
          queryString.append(" AND   ( :nombreProveedor IS  NULL OR UPPER(personaMidas.nombre) like  UPPER(:nombreProveedor) ) ");
          queryString.append(" AND   ( :noProveedor IS  NULL OR prestador.id = :noProveedor ) ");
          
          queryString.append(" AND  ( :siniestro IS  NULL OR :siniestro =  ");
          queryString.append("    SELECT func('CONCAT', orden.reporteCabina.siniestroCabina.claveOficina, func('CONCAT', '-', ");
          queryString.append("     FUNC('CONCAT', orden.reporteCabina.siniestroCabina.consecutivoReporte, ");
          queryString.append("     FUNC('CONCAT', '-', orden.reporteCabina.siniestroCabina.anioReporte)))) ");
          queryString.append("    FROM  OrdenCompra orden where orden.id = ordenCompra.id) ");
          
          queryString.append(" GROUP BY   ");
          queryString.append(" conjunto.id,notaCredito.id, factura.id, notaCredito.numeroFactura, notaCredito.montoTotal, ordenCompra.idBeneficiario, ");
          queryString.append(" personaMidas.nombre, factura.numeroFactura, ");
          queryString.append(" siniestroCabina.claveOficina ,notaCredito.fechaBaja, notaCredito.fechaCreacion, ");
          queryString.append(" notaCredito.estatus  ");

          Query query= this.entityManager.createQuery(queryString.toString());
          setQueryParametersByProperties(query, parameters);
          query.setHint(QueryHints.READ_ONLY, HintValues.TRUE); 
          return query.getResultList();
  }
	
	/**
	 * Ejecutar un JPQL para cambiar el estatus a liberada de todas las ordenes de
	 * pago relacionadas con las ordenes de pago recibidas como par�metro
	 * 
	 * 
	 * 
	 * @param ordenesDeCompra
	 */
	/*
	 * this.idFactura = idFactura;
		this.noFactura = noFactura;
		this.iva = iva;
		this.ivaRetenido = ivaRetenido;
		this.isr = isr;
		this.subTotal = subTotal;
		this.montoTotal = montoTotal;
	 * */
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorFactura(Long idFactura){
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("idFactura", idFactura);
		          
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT new mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro( ");
		queryString.append(" nota.id, factura.id, nota.numeroFactura, nota.iva, nota.ivaRetenido, nota.isr, nota.subTotal, nota.montoTotal ) ");
		queryString.append(" FROM "+DocumentoFiscal.class.getSimpleName()+" nota ");
		queryString.append(" JOIN nota.conjuntoDeNota conjuntoDeNota ");
		queryString.append(" JOIN conjuntoDeNota.factura factura ");
		queryString.append(" WHERE  factura.id = :idFactura ");
		Query query= this.entityManager.createQuery(queryString.toString());
		setQueryParametersByProperties(query, parameters);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);	
		return query.getResultList();
		
	}
	
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorRecuperacion(Long idRecuperacion)
	{	System.out.println("obtenerNotasDeCreditoPorRecuperacion  ");
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("idRecuperacion", idRecuperacion);
		          
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT new mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro( ");
		queryString.append(" nota.id, recuperacionProveedor.id, nota.numeroFactura, nota.iva, nota.ivaRetenido, nota.isr, nota.subTotal, nota.montoTotal ) ");
		queryString.append(" FROM "+DocumentoFiscal.class.getSimpleName()+" nota ");
		queryString.append(" JOIN nota.recuperacion recuperacionProveedor ");
		queryString.append(" WHERE recuperacionProveedor.id = :idRecuperacion ");
		Query query= this.entityManager.createQuery(queryString.toString());
		setQueryParametersByProperties(query, parameters);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);	
		return query.getResultList();
		
	}
	
	
	
	@Override
	public List<NotasCreditoRegistro> obtenerRecuperacionesNotasDeCreditoPorLiquidacion(Long idLiquidacion){	
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("idLiquidacion", idLiquidacion);
		          
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT new mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro( ");
		queryString.append(" nota.id, nota.id, nota.numeroFactura, nota.iva, nota.ivaRetenido, nota.isr, nota.subTotal, nota.montoTotal,\"(-)Recuperacion\" ) ");
		queryString.append(" FROM "+LiquidacionSiniestro.class.getSimpleName()+" liquidacion ");
		queryString.append(" JOIN liquidacion.recuperaciones recuperacion ");
		queryString.append(" JOIN recuperacion.notasdeCredito nota ");
		queryString.append(" WHERE liquidacion.id = :idLiquidacion ");
		Query query= this.entityManager.createQuery(queryString.toString());
		setQueryParametersByProperties(query, parameters);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);	
		return query.getResultList();
	}
	
	
	@Override
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorLiquidacion(Long idLiquidacion){
		System.out.println("obtenerNotasDeCreditoPorLiquidacion  ");
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKG_LIQUIDACIONES_SINIESTROS.OBTENER_NOTAS_POR_LIQUIDACION";
		List<NotasCreditoRegistro> listResultado = new ArrayList<NotasCreditoRegistro>();
		
		String columnaBd = "ID,NUMERO_FACTURA,TIPONOTA,TOTAL";
		String atributos = "idFactura,noFactura,tipo,montoTotal";
		try {
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(NotasCreditoRegistro.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("pIdLiquidacion", idLiquidacion);
			listResultado = spHelper.obtieneListaResultados();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return listResultado;
	}

	/**
	 * Crear un query en JPQL de la entidad DocumentoFiscal. creando un unmanaged
	 * object de tipo FacturaSiniestroRegistro.
	 * 
	 * Consideraciones:
	 * 
	 * DocumentoFiscal.estatus = R
	 * DocumentoFiscal.tipo = FACT
	 * Que tenga al menos una orden de compra asociada.
	 * Y la orden de compra se encuentre en estatus AUTORIZADA
	 */
	@SuppressWarnings("unchecked")
	public List<FacturaSiniestroRegistro> obtenerFacturasParaCrearNotasDeCredito(){
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("tipoDocumentoFiscal", DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue());
		parameters.put("estatusDocumentoFiscal", DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue());
		parameters.put("origenDocumentoFiscal", DocumentoFiscal.OrigenDocumentoFiscal.LSN);
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT new mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.FacturaSiniestroRegistro( ");
		queryString.append(" factura.id, factura.numeroFactura, factura.fechaCreacion, ordenCompra.idBeneficiario, prestador.personaMidas.nombre ) ");
		queryString.append(" FROM "+DocumentoFiscal.class.getSimpleName()+" factura ");
		queryString.append(" JOIN factura.ordenesCompra ordenCompra ");
		queryString.append(" JOIN PrestadorServicio prestador ON (prestador.id = ordenCompra.idBeneficiario )");
		queryString.append(" WHERE  factura.tipo = :tipoDocumentoFiscal ");
		queryString.append(" AND  factura.estatus = :estatusDocumentoFiscal ");
		queryString.append(" AND  factura.origen = :origenDocumentoFiscal ");
		
		queryString.append(" GROUP BY factura.id, factura.numeroFactura,factura.fechaCreacion,ordenCompra.idBeneficiario,prestador.personaMidas.nombre  ");
		
		
		
		Query query= this.entityManager.createQuery(queryString.toString());
		setQueryParametersByProperties(query, parameters);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);	
		return query.getResultList();
	}

	@Override
	@SuppressWarnings("unchecked")
	public LiquidacionSiniestro obtenerLiquidacionDeFactura(Long facturaId){
		LiquidacionSiniestro liquidacion = null;
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("facturaId", facturaId);
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT liquidacion FROM LiquidacionSiniestro liquidacion ");
		queryString.append(" JOIN liquidacion.facturas factura where factura.id = :facturaId ");
		
		Query query= this.entityManager.createQuery(queryString.toString());
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		setQueryParametersByProperties(query, parameters);
		List<LiquidacionSiniestro> resultado = query.getResultList();
		if(resultado != null 
				&& !resultado.isEmpty()){
			liquidacion = resultado.get(0);
		}
		return liquidacion;
	}



	@Override
	public List<DocumentoFiscal> obtenerNotasDeCreditoParaAsociarRecuperacion(
			Integer idProveedor) {
		PrestadorServicio proveedor = this.findById(PrestadorServicio.class, idProveedor);
		String rfc = proveedor.getPersonaMidas().getRfc();
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("tipoDocumentoFiscal", DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue());
		parameters.put("estatusDocumentoFiscal", DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue());
		parameters.put("origenDocumentoFiscal", DocumentoFiscal.OrigenDocumentoFiscal.LSN);
		parameters.put("rfc", rfc);
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT nota ");
		queryString.append(" FROM "+DocumentoFiscal.class.getSimpleName()+" nota ");
		queryString.append(" WHERE  nota.tipo = :tipoDocumentoFiscal ");
		queryString.append(" AND  nota.estatus = :estatusDocumentoFiscal ");
		queryString.append(" AND  nota.rfcEmisor = :rfc ");
		queryString.append(" AND  nota.conjuntoDeNota IS NULL  ");
		queryString.append(" AND  nota.recuperacion IS NULL  ");
		queryString.append(" AND  nota.origen = :origenDocumentoFiscal ");		
		Query query= this.entityManager.createQuery(queryString.toString());
		setQueryParametersByProperties(query, parameters);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);	
		return query.getResultList();
	}
	
}