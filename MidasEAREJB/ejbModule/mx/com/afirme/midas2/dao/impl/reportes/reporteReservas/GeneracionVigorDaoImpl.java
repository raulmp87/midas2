package mx.com.afirme.midas2.dao.impl.reportes.reporteReservas;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.reportes.reporteReservas.GeneracionVigorDao;
import mx.com.afirme.midas2.domain.reportes.reporteReservas.GeneracionVigorDTO;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class GeneracionVigorDaoImpl extends JpaDao<Long, GeneracionVigorDTO> implements GeneracionVigorDao{
 
 
	@SuppressWarnings("unchecked")
	@Override
	public List<GeneracionVigorDTO> getListTareas(BigDecimal tipoArchivo) throws SQLException {
		
		List<GeneracionVigorDTO> list = new ArrayList<GeneracionVigorDTO>();

		Query query = entityManager.createQuery(
				"select model from  GeneracionVigorDTO model where " +
				"" +
				" model.id in " +
				"(" +
				" select model.id from GeneracionVigorDTO model where" +
					" model.id = ( select max(model.id) from GeneracionVigorDTO model where model.tipoArchivo = ?1 )" +
				")" +
				"" +
				" or model.id in " +
				" (" +
				" select model.id from GeneracionVigorDTO model where" +
				" model.id = ( select max(model.id) from GeneracionVigorDTO model where model.tipoArchivo = ?2 " +
				")" +
				")" +
				"" +
				" and model.tipoArchivo = ?1 " +
				" order by model.id desc"
		);
		query.setParameter(1, tipoArchivo);
		query.setParameter(2, tipoArchivo);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list = (List<GeneracionVigorDTO>)query.getResultList();
		
		return list;
	}
	
	@Override
	public GeneracionVigorDTO getLast() throws SQLException{

		Query query = entityManager.createQuery(
				"select model from GeneracionVigorDTO model where" +
				" model.id = ( select max(model.id) from GeneracionVigorDTO model )"
		);

		return (GeneracionVigorDTO)query.getSingleResult();
	}
}
