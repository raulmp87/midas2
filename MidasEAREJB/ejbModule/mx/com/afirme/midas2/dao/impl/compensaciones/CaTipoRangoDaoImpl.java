/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoRangoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoRango;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoRangoDaoImpl implements CaTipoRangoDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoRangoDaoImpl.class);
	
	public void save(CaTipoRango entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoRango 	::		CaTipoRangoDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoRango 	::		CaTipoRangoDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoRango 	::		CaTipoRangoDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaTipoRango entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoRango 	::		CaTipoRangoDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoRango.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoRango 	::		CaTipoRangoDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoRango 	::		CaTipoRangoDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoRango update(CaTipoRango entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoRango 	::		CaTipoRangoDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoRango result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoRango 	::		CaTipoRangoDaoImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoRango 	::		CaTipoRangoDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoRango findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoRangoDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoRango instance = entityManager.find(CaTipoRango.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoRangoDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoRangoDaoImpl	::	findById	::	ERROR	::	",re);
	        return null;
        }
    }    
    
    @SuppressWarnings("unchecked")
    public List<CaTipoRango> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoRangoDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoRango model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoRangoDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoRangoDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoRango> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoRango> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoRango> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoRango> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoRango> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoRangoDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoRango model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoRangoDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoRangoDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
