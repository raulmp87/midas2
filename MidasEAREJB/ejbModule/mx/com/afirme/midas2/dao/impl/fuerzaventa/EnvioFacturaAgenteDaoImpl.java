package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.fuerzaventa.EnvioFacturaAgenteDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteFacturaPendiente;

@Stateless
public class EnvioFacturaAgenteDaoImpl extends EntidadDaoImpl implements EnvioFacturaAgenteDao {
	
	public EnvioFacturaAgente guardaEnvio (EnvioFacturaAgente envio) {
		
		if (envio.getIdEnvio() == null) {
			
			envio.setIdEnvio((Long) persistAndReturnId(envio));
			return getReference(EnvioFacturaAgente.class, envio.getIdEnvio());
		
		} else {
			
			return update(envio);
							
		}
		
	}
	
	public void saveRelationXMLCheque(Long idAgente, String aniomes, Long idEnvio){
		final StringBuilder queryUpdate=new StringBuilder("");
		
		queryUpdate.append(" update MIDAS.TOAGENTEMOVIMIENTOS set IDENVIOXML= ?3");
		queryUpdate.append(" where  IDAGENTE= ?1");
		queryUpdate.append(" and ANIOMES = ?2");
		queryUpdate.append(" and IDCONCEPTO = 4");
		
		Query query = entityManager.createNativeQuery(queryUpdate.toString());
		int param = 0;
		query.setParameter(++param, idAgente);
		query.setParameter(++param, aniomes);
		query.setParameter(++param, idEnvio);
		
		query.executeUpdate();
		
	}
	
	@Override
	public Agente getDatosAgente(String codigoUsuario, Integer idPersona, Long idAgente) throws Exception {
		Agente agente = new Agente();
		Map<String,Object>parameters = new HashMap<String, Object>();
		
		StringBuilder queryString = new StringBuilder("SELECT model FROM Agente model ");
		
		if(codigoUsuario!=null && idPersona!=null){
			queryString.append(" WHERE trim(FROM model.codigoUsuario) = :codigoUsuario ");
			parameters.put("codigoUsuario",  codigoUsuario);
			queryString.append(" AND model.persona.idPersona = :idPersona ");
			parameters.put("idPersona", idPersona);
		}
		
		if(idAgente!=null){
			queryString.append(" WHERE model.idAgente = :idAgente ");
			parameters.put("idAgente", idAgente);
		}
		
		agente = (Agente)this.executeQuerySimpleResult(queryString.toString(), parameters);
		if (agente!=null){
			return agente;
		}else{
			return null;
		}
	}
	@Override
	public PrestadorServicio getDatosPrestadorServicio(Integer idPersona, Long idProveedor) throws Exception {
		PrestadorServicio prestador = new PrestadorServicio();
		Map<String,Object>parameters = new HashMap<String, Object>();
		StringBuilder queryString = new StringBuilder("SELECT model FROM PrestadorServicio model ");
		if(idPersona!=null){
			queryString.append(" WHERE model.personaMidas.id = :idPersona ");
			parameters.put("idPersona", idPersona);
		}
		if( idProveedor!=null){
			queryString.append(" WHERE model.id = :idProveedor ");
			parameters.put("idProveedor", idProveedor);
		}
		prestador = (PrestadorServicio)this.executeQuerySimpleResult(queryString.toString(), parameters);
		if (prestador!=null){
			return prestador;
		}else{
			return null;
		}
	}
	
	/**
	 * Obtiene agentes vigentes y autorizados, que contabilicen, 
	 * que se les generen cheques y que tengan facturas pendientes por entregar desde Enero del 2015 o posteriores.
	 * @return Agentes vigentes y autorizados, que contabilicen, 
	 * que se les generen cheques y que tengan facturas pendientes por entregar desde Enero del 2015 o posteriores.
	 * Tambien regresa el periodo mas antiguo en el cual tengan facturas pendientes.
	 */
	@SuppressWarnings("unchecked")
	public List<AgenteFacturaPendiente> getAgentesFacturaPendiente() {
			
		StoredProcedureHelper storedHelper = null;
		List<AgenteFacturaPendiente> respuesta = new ArrayListNullAware<AgenteFacturaPendiente>();
		
		try {
			String [] atributos = {
					"idAgente",
					"anioMes"
			};
			
			String [] columnasResultSet = {
					"IDAGENTE",
					"ANIOMES"
			};
			
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.SP_GET_AGTS_FAC_PEND", StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceMapeoResultados(AgenteFacturaPendiente.class.getCanonicalName(), atributos, columnasResultSet);
			
			respuesta = storedHelper.obtieneListaResultados();
			
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return respuesta;
				
	}
	
		
}

