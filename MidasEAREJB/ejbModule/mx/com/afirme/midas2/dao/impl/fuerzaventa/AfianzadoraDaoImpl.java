package mx.com.afirme.midas2.dao.impl.fuerzaventa;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.sector.SectorDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.fuerzaventa.AfianzadoraDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.impl.domicilio.DomicilioUtils;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Afianzadora;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote.TipoDomicilio;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote.TipoAccionFuerzaVenta;

import org.apache.commons.beanutils.BeanUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;
@Stateless
public class AfianzadoraDaoImpl extends EntidadDaoImpl implements AfianzadoraDao{
	private DomicilioFacadeRemote domicilioFacade;
	private ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade;
	
	@Override
	public Afianzadora saveFull(Afianzadora afianzadora) throws Exception {
		/*
		if(afianzadora==null){
			throw new Exception("Afianzadora is null");
		}
		Domicilio domicilio=afianzadora.getDomicilio();
		//TODO Remove after stored procedure 
		if(domicilio==null){
			domicilio=Domicilio.getDummy();
		}
		Long idDomicilio=domicilio.getIdDomicilio();
		//Si no existe el domicilio entonces se inserta
		//if(idDomicilio==null){
		TipoDomicilio tipoDomicilio = domicilioFacade.getTipoDomicilioPorClave("AFIA");
		domicilio.setTipoDomicilio(tipoDomicilio.getValue());
		idDomicilio=domicilioFacade.save(domicilio, null);
		domicilio.setIdDomicilio(idDomicilio);
		//}
		Domicilio persistAddress=entityManager.find(Domicilio.class, domicilio.getIdDomicilio());
		if(persistAddress!=null){
			domicilio=persistAddress;
		}
		if(afianzadora.getId()!=null){
			update(afianzadora);
		}else{
			persist(afianzadora);
		}		
		//Sección de replicar info a Seycos.
		Long idSeycos=replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(afianzadora,TipoAccionFuerzaVenta.GUARDAR);//Indica que va a replicar pero va a guardar en seycos
		afianzadora.setDomicilio(domicilio);
		afianzadora.setIdAfianzadora(idSeycos);//En este caso el idSeycos es el id persona insertada en Seycos.persona que es el id de afianzadora en seycos-midas.
		update(afianzadora);//se actualiza la afianzadora persistida con el id de afianzadora de seycos
		return afianzadora;
		*/
		return guardarAfianzadora(afianzadora);
	}
	
	private Object val(Object o){
		return (o==null)?"":o;
	}
	
	private Afianzadora guardarAfianzadora(Afianzadora afianzadora) throws Exception{
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stpGuardarAfianzadora";
		try {
			Domicilio domicilio=afianzadora.getDomicilio();
			DomicilioPk pkDomicilio=(domicilio!=null)?domicilio.getIdDomicilio():null;
			Long idDomicilio=(pkDomicilio!=null)?pkDomicilio.getIdDomicilio():null;
			String codigoPostal=(domicilio!=null)?domicilio.getCodigoPostal():null;
			String idColonia=(domicilio!=null)?domicilio.getIdColonia():null;
			String nuevaColonia=(domicilio.getNuevaColonia()!=null && !domicilio.getNuevaColonia().trim().isEmpty())?domicilio.getNuevaColonia():null;
			String nombreColonia=(domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().trim().isEmpty())?domicilio.getNombreColonia():null;
			nombreColonia=(nuevaColonia!=null)?nuevaColonia:nombreColonia;
			String calleNumero=(domicilio!=null)?domicilio.getCalleNumero():null;
			TipoDomicilio tipoDomicilio = domicilioFacade.getTipoDomicilioPorClave("AFIA");
			String tipoDomi=tipoDomicilio.getValue();
			SectorDTO sector=afianzadora.getSector();
			String idSector=(sector!=null)?sector.getIdSector():null;
			String clavePais = (afianzadora.getDomicilio()!=null &&!afianzadora.getDomicilio().getClavePais().equals(""))?afianzadora.getDomicilio().getClavePais():"PAMEXI";
			LogDeMidasInterfaz.log("Entrando a AfianzadoraDaoImpl.save..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pId",val(afianzadora.getId()));
			storedHelper.estableceParametro("pIdAfianzadora",val(afianzadora.getIdAfianzadora()));
			storedHelper.estableceParametro("pIdSector",val(idSector));
			storedHelper.estableceParametro("pFechaAlta",val(afianzadora.getFechaAlta()));
			storedHelper.estableceParametro("pFechaBaja",val(afianzadora.getFechaBaja()));
			storedHelper.estableceParametro("pRazonSocial",val(afianzadora.getRazonSocial()));
			storedHelper.estableceParametro("pSiglasRFC",val(afianzadora.getSiglasRfc()));
			storedHelper.estableceParametro("pFechaRFC",val(afianzadora.getFechaRfc()));
			storedHelper.estableceParametro("pHomoclaveRFC",val(afianzadora.getHomoclaveRfc()));
			storedHelper.estableceParametro("pTelefonoParticular",val(afianzadora.getTelefonoParticular()));
			storedHelper.estableceParametro("pTelefonoCelular",val(afianzadora.getTelefonoCelular()));
			storedHelper.estableceParametro("pTelefonoOficina",val(afianzadora.getTelefonoOficina()));
			storedHelper.estableceParametro("pExtensionOficina",val(afianzadora.getExtensionOficina()));
			storedHelper.estableceParametro("pFaxOficina",val(afianzadora.getFaxOficina()));
			storedHelper.estableceParametro("pCorreoElectronico",val(afianzadora.getCorreoElectronico()));
			storedHelper.estableceParametro("pPaginaWeb",val(afianzadora.getPaginaWeb()));
			storedHelper.estableceParametro("pClaveEstatus",val(afianzadora.getClaveEstatus()));
			storedHelper.estableceParametro("pIdDomicilio",val(idDomicilio));
			storedHelper.estableceParametro("pcodigo_postal",val(codigoPostal));
			storedHelper.estableceParametro("pid_colonia",val(idColonia));
			storedHelper.estableceParametro("pnom_colonia",val(nombreColonia));
			storedHelper.estableceParametro("pcalle_numero",val(calleNumero));
			storedHelper.estableceParametro("ptipo_domicilio",val(tipoDomi));
			storedHelper.estableceParametro("pid_persona",null);
			storedHelper.estableceParametro("pclavePais",clavePais);
			int id=storedHelper.ejecutaActualizar();
			Long llave=new Long(id);
			afianzadora.setId(llave);
			LogDeMidasInterfaz.log("Se ha guardado Afianzadora con id:"+id+"..."+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de AfianzadoraDaoImpl.save..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, afianzadora.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AfianzadoraDaoImpl.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AfianzadoraDaoImpl.save..." + this, Level.WARNING, e);
			throw e;
		}
		return afianzadora;
	}
	
	@Override
	public void unsubscribe(Afianzadora afianzadora) throws Exception{
		if(afianzadora==null || afianzadora.getId()==null){
			throw new Exception("Afianzadora is null");
		}
		afianzadora=findById(Afianzadora.class,afianzadora.getId());
		afianzadora.setClaveEstatus(0l);
		update(afianzadora);//Se actualiza su estatus 
//		replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(afianzadora, TipoAccionFuerzaVenta.INACTIVAR);
	}
	@Override
	public List<Afianzadora> findByFiltersView(Afianzadora filtroAfianzadora){
		List<Afianzadora> lista=new ArrayList<Afianzadora>();
		//Si trae datos de fecha entonces busca por medio de sql, sino con jpql
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Domicilio domicilio=(isNotNull(filtroAfianzadora) && isNotNull(filtroAfianzadora.getDomicilio()))?filtroAfianzadora.getDomicilio():null;
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select distinct ");
		queryString.append(" entidad.id as id,");
		queryString.append(" entidad.idAfianzadora as idAfianzadora,");
		queryString.append(" entidad.razonSocial as razonSocial,");
		queryString.append(" entidad.siglasRfc as siglasRfc,");
		queryString.append(" entidad.fechaRfc as fechaRfc,");
		queryString.append(" entidad.homoClaveRfc as homoClaveRfc,");
		queryString.append(" sector.sector_name as nombreSector, ");
		queryString.append(" sector.sector_id as SECTOR_ID ,entidad.claveEstatus ");
		queryString.append(" from MIDAS.toAfianzadora entidad ");  
		queryString.append(" inner join MIDAS.VW_SECTOR sector on(sector.sector_id=entidad.idsector) ");   
		queryString.append(" inner join MIDAS.TCGRUPOACTUALIZACIONAGENTES grupo on(grupo.descripcion='Afianzadora') "); 
		queryString.append(" left join MIDAS.TLACTUALIZACIONAGENTES act on(act.grupoActualizacionAgentes_id=grupo.id and act.idRegistro=entidad.id and (act.tipoMovimiento is null or act.tipoMovimiento like 'ALTA' )) ");
		boolean domicilioEmpty=DomicilioUtils.isEmptyAddress(domicilio);
		if(!domicilioEmpty){
			queryString.append(" LEFT JOIN MIDAS.VW_DOMICILIO dom ON (dom.idPersona=entidad.idAfianzadora and dom.idDomicilio=entidad.idDomicilio) ");
		}
		queryString.append(" where ");
		if(isNotNull(filtroAfianzadora)){
			int index=1;
			if(isNotNull(filtroAfianzadora.getId())){
				addCondition(queryString, " entidad.id=? ");
				params.put(index, filtroAfianzadora.getId());
				index++;
			}else if(isNotNull(filtroAfianzadora.getIdAfianzadora())){
				addCondition(queryString, " entidad.idAfianzadora=? ");
				params.put(index, filtroAfianzadora.getIdAfianzadora());
				index++;
			}else{
				if(isValid(filtroAfianzadora.getSiglasRfc())){
					addCondition(queryString, " UPPER(TRIM(entidad.siglasRfc))=UPPER(TRIM(?)) ");
					params.put(index, filtroAfianzadora.getSiglasRfc());
					index++;
				}
				if(isValid(filtroAfianzadora.getRazonSocial())){
					addCondition(queryString, " UPPER(TRIM(entidad.razonSocial)) like UPPER(TRIM(?))||'%' ");
					params.put(index, filtroAfianzadora.getRazonSocial());
					index++;
				}
				if(isValid(filtroAfianzadora.getFechaRfc())){
					addCondition(queryString, " UPPER(TRIM(entidad.fechaRfc))=UPPER(TRIM(?)) ");
					params.put(index, filtroAfianzadora.getFechaRfc());
					index++;
				}
				if(isValid(filtroAfianzadora.getHomoclaveRfc())){
					addCondition(queryString, " UPPER(TRIM(entidad.HomoclaveRfc))=UPPER(TRIM(?)) ");
					params.put(index, filtroAfianzadora.getHomoclaveRfc());
					index++;
				}
				if(filtroAfianzadora.getClaveEstatus()!=null){
					addCondition(queryString, " UPPER(TRIM(entidad.claveEstatus))=UPPER(TRIM(?)) ");
					params.put(index, filtroAfianzadora.getClaveEstatus());
					index++;
				}
				if(isNotNull(domicilio)){
					if(isValid(domicilio.getClaveEstado())){
						addCondition(queryString, " dom.claveEstado like UPPER(?)");
						params.put(index,  domicilio.getClaveEstado());
						index++;
					}
					if(isValid(domicilio.getClaveCiudad())){
						addCondition(queryString, " dom.claveCiudad like UPPER(?)");
						params.put(index,  domicilio.getClaveCiudad());
						index++;
					}
					if(isValid(domicilio.getCodigoPostal())){
						addCondition(queryString, " TRIM(dom.codigoPostal) like TRIM(UPPER(?))");
						params.put(index,  domicilio.getCodigoPostal());
						index++;
					}
					if(isValid(domicilio.getNombreColonia())){
						addCondition(queryString, " TRIM(dom.colonia) like TRIM(UPPER(?))||'%'");
						params.put(index,  domicilio.getNombreColonia());
						index++;
					}
				}
				if(isNotNull(filtroAfianzadora.getFechaInicio())){
					Date fecha=filtroAfianzadora.getFechaInicio();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroAfianzadora.getFechaFin())){
					Date fecha=filtroAfianzadora.getFechaFin();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
					params.put(index,  fechaString);
					index++;
				}
			}
			if(params.isEmpty()){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,"afianzadoraView");
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			query.setMaxResults(100);
			lista=query.getResultList();		
		}
		return lista;
	}
	
	@Override
	public List<Afianzadora> findByFilters(Afianzadora filtroAfianzadora, String fechaHistorico) {
		
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;		
		
		List<Afianzadora> lista=new ArrayList<Afianzadora>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from Afianzadora model ");
		
		if(isHistorico)
		{
			queryString.append("left join fetch model.domicilioHistorico ");	
		}
		
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtroAfianzadora!=null){
			if(filtroAfianzadora.getRazonSocial()!=null && !filtroAfianzadora.getRazonSocial().isEmpty()){
				addCondition(queryString, "UPPER(model.razonSocial) like UPPER(:razonSocial)");
				params.put("razonSocial", "%"+filtroAfianzadora.getRazonSocial()+"%");
			}
			if(filtroAfianzadora.getFechaRfc()!=null && !filtroAfianzadora.getFechaRfc().isEmpty()){
				addCondition(queryString, "model.fechaRfc=:fechaRfc");
				params.put("fechaRfc", filtroAfianzadora.getFechaRfc());
			}
			if(filtroAfianzadora.getHomoclaveRfc()!=null && !filtroAfianzadora.getHomoclaveRfc().isEmpty()){
				addCondition(queryString, "UPPER(model.homoclaveRfc)=UPPER(:homoclaveRfc)");
				params.put("homoclaveRfc", filtroAfianzadora.getHomoclaveRfc());
			}
			if(filtroAfianzadora.getSiglasRfc()!=null && !filtroAfianzadora.getSiglasRfc().isEmpty()){
				addCondition(queryString, "UPPER(model.siglasRfc)=UPPER(:siglasRfc)");
				params.put("siglasRfc", filtroAfianzadora.getSiglasRfc());
			}
			Domicilio domicilio=filtroAfianzadora.getDomicilio();
			if(domicilio!=null){
				if(domicilio.getClavePais()!=null && !domicilio.getClavePais().isEmpty()){
					addCondition(queryString, "model.domicilio.clavePais=:clavePais");
					params.put("clavePais", domicilio.getClavePais());
				}
				if(domicilio.getClaveEstado()!=null && !domicilio.getClaveEstado().isEmpty()){
					addCondition(queryString, "model.domicilio.claveEstado=:claveEstado");
					params.put("claveEstado", domicilio.getClaveEstado());
				}
				if(domicilio.getClaveCiudad()!=null && !domicilio.getClaveCiudad().isEmpty()){
					addCondition(queryString, "model.domicilio.claveCiudad=:claveCiudad");
					params.put("claveCiudad", domicilio.getClaveCiudad());
				}
				if(domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().isEmpty()){
					addCondition(queryString, "UPPER(model.domicilio.nombreColonia) like UPPER(:nombreColonia)");
					params.put("nombreColonia", "%"+domicilio.getNombreColonia()+"%");
				}
				if(domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().isEmpty()){
					addCondition(queryString, "model.domicilio.codigoPostal like :codigoPostal");
					params.put("codigoPostal", domicilio.getCodigoPostal()+"%");
				}
			}
			SectorDTO sector=filtroAfianzadora.getSector();
			if(sector!=null){
				if(sector.getIdSector()!=null && !sector.getIdSector().isEmpty()){
					addCondition(queryString, "model.sector.idSector =:idSector");
					params.put("idSector", sector.getIdSector());
				}
			}
			if(filtroAfianzadora.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtroAfianzadora.getId());
			}
		}
		
		if(!isHistorico)
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista=query.getResultList();						
		}
		else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
			
			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString).toString());
			historicalQuery.addArgument("id");
			historicalQuery.addArgumentValue(filtroAfianzadora.getId());
			historicalQuery.refreshIdentityMapResult();
			lista = (List<Afianzadora>)historicalSession.executeQuery(historicalQuery);			
		}		
		
		return lista;
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	@Override
	public Afianzadora loadById(Afianzadora entidad, String fechaHistorico){		
		
		if(entidad!=null && entidad.getId()!=null){
			Afianzadora filtro=new Afianzadora();
			filtro.setId(entidad.getId());
			List<Afianzadora> list=findByFilters(filtro, fechaHistorico);
			if(list!=null && !list.isEmpty()){
				entidad=list.get(0);
				
				if (fechaHistorico != null && !fechaHistorico.isEmpty()) {
					Domicilio domicilioTemp = new Domicilio();
					try {
						BeanUtils.copyProperties(domicilioTemp,
								entidad.getDomicilioHistorico());
					} catch (Exception e) {
						e.printStackTrace();
					}

					entidad.setDomicilio(domicilioTemp);
				}
			}
		}
		return entidad;
	}
	/***Sets and gets**********************************************/

	@EJB
	public void setDomicilioFacade(DomicilioFacadeRemote domicilioFacade) {
		this.domicilioFacade = domicilioFacade;
	}
	
	@EJB
	public void setReplicarFuerzaVentaSeycosFacade(
			ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade) {
		this.replicarFuerzaVentaSeycosFacade = replicarFuerzaVentaSeycosFacade;
	}
}
