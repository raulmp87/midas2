package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.fortimax.CarpetaAplicacionFortimaxDao;
import mx.com.afirme.midas2.dao.fortimax.DocumentoCarpetaFortimaxDao;
import mx.com.afirme.midas2.dao.fuerzaventa.SuspensionAgenteDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.SuspensionAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CarpetaAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.notificaciones.MovimientosProcesos;
import mx.com.afirme.midas2.domain.notificaciones.ProcesosAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote.TipoAccionFuerzaVenta;
import mx.com.afirme.midas2.dto.fuerzaventa.SuspensionesView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.SuspensionAgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.impl.prestamos.ConfigPrestamoAnticipoServiceImpl;
import mx.com.afirme.midas2.utils.MailServiceSupport;

import org.apache.commons.collections.Predicate;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class SuspensionAgenteDaoImpl extends EntidadDaoImpl implements SuspensionAgenteDao{
	private EntidadService entidadService;
	private ValorCatalogoAgentesService catalogoService;
	@EJB
	private AgenteMidasService agenteMidasService;
	
	private FortimaxService fortimaxService;
	@EJB
	private SuspensionAgenteService suspensionAgenteService;
	
	@EJB
	private DocumentoCarpetaFortimaxDao documentoCarpetaFortimaxDao;
	
	@EJB
	private CarpetaAplicacionFortimaxDao carpetaAplicacionFortimaxDao;
	
	@EJB
	private ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade;
	
	
	private static String regEx;
	
	private Predicate nombreDocumentoPredicate = new Predicate() {
		@Override
		public boolean evaluate(Object arg0) {
			
			Matcher matcher;
							
			Pattern pattern = Pattern.compile(ConfigPrestamoAnticipoServiceImpl.getRegEx());
			
			if (arg0 instanceof DocumentosAgrupados) {
				matcher = pattern.matcher(((DocumentosAgrupados)arg0).getNombre());
			} else if (arg0 instanceof String) {
				matcher = pattern.matcher((String)arg0);
			} else if (arg0 instanceof EntregoDocumentosView) {
				matcher = pattern.matcher(((EntregoDocumentosView)arg0).getValor());
			} else {
				return false;
			}
			
			return matcher.find();
		}
	};

	@Override
	public List<SuspensionAgente> findByFiltersView(SuspensionAgente filtro)	throws Exception {
		List<SuspensionAgente> lista = new ArrayList<SuspensionAgente>();
		Map< Integer,Object> params = new HashMap< Integer,Object>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append("select s.id,ag.idAgente,to_date(s.FECHASOLICITUD,'dd/mm/yyyy') as fechaSolicitud,per.nombreCompleto as nombreAgente,s.NOMBRESOLICITANTE " +
				" ,estatusmov.valor as estatusMovimiento,sit.valor as estatusAgente from MIDAS.toSuspensionAgente s " +
				" left join MIDAS.toagente ag on s.IDAGENTE = ag.id " +
				" left join MIDAS.vw_persona per on per.IDPERSONA = ag.idpersona " +
				" left join MIDAS.tovalorcatalogoagentes estatusmov on estatusmov.id=s.CLAVEESTATUSMOVIMIENTO " +
				" left join MIDAS.tovalorcatalogoagentes sit on sit.id = s.ESTATUSAGTSOLICITADO ");
		queryString.append(" where ");
		if(isNotNull(filtro)){
			int index=1;
			if(filtro.getAgente()!=null && filtro.getAgente().getIdAgente()!=null){
				addCondition(queryString, "ag.idAgente=?");
				params.put(index, filtro.getAgente().getIdAgente());
				index++;
			}
			if(!filtro.getAgente().getPersona().getNombreCompleto().equals("") && !filtro.getAgente().getPersona().getNombreCompleto().isEmpty()){
				addCondition(queryString, "UPPER(per.nombreCompleto) like UPPER(?)");
				params.put(index, "%"+filtro.getAgente().getPersona().getNombreCompleto()+"%");
				index++;
			}
			if(filtro.getEstatusMovimiento()!=null && filtro.getEstatusMovimiento().getId()!=null){
				addCondition(queryString, "estatusmov.id=?");
				params.put(index, filtro.getEstatusMovimiento().getId());
				index++;
			}
//			if(filtro.getAgente().getTipoSituacion()!=null && filtro.getAgente().getTipoSituacion().getId()!=null){
//				addCondition(queryString, "sit.id=?");
//				params.put(index, filtro.getAgente().getTipoSituacion().getId());
//				index++;
//			}
			if(filtro.getEstatusAgtSolicitado()!=null && filtro.getEstatusAgtSolicitado().getId()!=null){
				addCondition(queryString, "sit.id=?");
				params.put(index, filtro.getEstatusAgtSolicitado().getId());
				index++;
			}
			if(filtro.getNombreSolicitante()!=null && !filtro.getNombreSolicitante().isEmpty()){
				addCondition(queryString, "UPPER(s.nombreSolicitante) like UPPER(?)");
				params.put(index, "%"+filtro.getNombreSolicitante()+"%");
				index++;
			}
			
			if(isNotNull(filtro.getFechaInicio())){
				Date fecha=filtro.getFechaInicio();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(s.fechaSolicitud) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");						
				params.put(index,  fechaString);				
				index++;
			}
			if(isNotNull(filtro.getFechaFin())){
				Date fecha=filtro.getFechaFin();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(s.fechaSolicitud) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				params.put(index, fechaString);
				index++;
			}
		}
		
		
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		String finalQuery=getQueryString(queryString)+" order by s.id asc ";
		Query query=entityManager.createNativeQuery(finalQuery,SuspensionesView.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		lista=query.getResultList();	
		return lista;
	}
	
	
	@Override
	public List<SuspensionAgente> findByFilters(SuspensionAgente solicitudSuspension) throws Exception {
		List<SuspensionAgente> list = new ArrayList<SuspensionAgente>();
		Map< String,Object> params = new HashMap< String,Object>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append("Select model from SuspensionAgente model left join fetch model.agente left join fetch model.estatusMovimiento ");
		if(solicitudSuspension!=null){
			if(solicitudSuspension.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", solicitudSuspension.getId());
			}
			if(solicitudSuspension.getAgente()!=null){
				
				if(solicitudSuspension.getAgente().getId()!=null){
					addCondition(queryString, "model.agente.idAgente=:idAgente");
					params.put("idAgente", solicitudSuspension.getAgente().getId());
				}
				if(solicitudSuspension.getAgente().getPersona().getNombreCompleto()!=null && !solicitudSuspension.getAgente().getPersona().getNombreCompleto().isEmpty()){
					addCondition(queryString, "UPPER(model.agente.persona.nombreCompleto) like UPPER(:nombreAgente)");
					params.put("nombreAgente", "%"+solicitudSuspension.getAgente().getPersona().getNombreCompleto()+"%");
				}
				if(solicitudSuspension.getAgente().getTipoSituacion()!=null && solicitudSuspension.getAgente().getTipoSituacion().getId()!=null){
					addCondition(queryString, "model.agente.tipoSituacion.id=:estatusAgente)");
					params.put("estatusAgente", solicitudSuspension.getAgente().getTipoSituacion().getId());
				}
			}
			if(solicitudSuspension.getEstatusMovimiento()!=null && solicitudSuspension.getEstatusMovimiento().getId()!=null){
				addCondition(queryString, "model.estatusMovimiento.id=:estatusMov)");
				params.put("estatusMov", solicitudSuspension.getEstatusMovimiento().getId());
			}
			if(solicitudSuspension.getNombreSolicitante()!=null && !solicitudSuspension.getNombreSolicitante().isEmpty()){
				addCondition(queryString, "UPPER(model.nombreSolicitante) like UPPER(:solicitante)");
				params.put("solicitante", "%"+solicitudSuspension.getNombreSolicitante()+"%");
			}
			
			if(isNotNull(solicitudSuspension.getFechaInicio())){
				Date fecha=solicitudSuspension.getFechaInicio();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(model.fechaSolicitud) >= TRUNC(TO_DATE(:fechaInicio,'DD/MM/YYYY')) ");						
				params.put("fechaInicio",  fechaString);				
			}
			if(isNotNull(solicitudSuspension.getFechaFin())){
				Date fecha=solicitudSuspension.getFechaFin();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(model.fechaSolicitud) <= TRUNC(TO_DATE(:fechaFin,'DD/MM/YYYY')) ");
				params.put("fechaFin", fechaString);
			}
		}
		
		String finalQuery=getQueryString(queryString)+" order by model.fechaSolicitud ASC ";
		Query query = entityManager.createQuery(finalQuery);
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}

	@Override
	public SuspensionAgente guardarSolicitudDeSuspension(SuspensionAgente solicitudSuspension) throws Exception {
		
		if(solicitudSuspension != null){
			if(solicitudSuspension.getId()!=null){
				update(solicitudSuspension);
			}else{
				persist(solicitudSuspension);
			}	
		}
		return solicitudSuspension;
	}

	@Override
	public SuspensionAgente loadById(Long idSolicitudSuspension) throws Exception {
		SuspensionAgente filtro = new SuspensionAgente();
		SuspensionAgente suspension = new SuspensionAgente();
		if(idSolicitudSuspension!=null){
			filtro.setId(idSolicitudSuspension);
			List<SuspensionAgente> list = findByFilters(filtro);
			if(list!=null && !list.isEmpty()){
				suspension=list.get(0);
			}
		}		
		return suspension;
	}

	@Override
	public SuspensionAgente rechazaSolicitudDeSuspension(SuspensionAgente solicitudSuspension) throws Exception {
		if(solicitudSuspension !=null){
			SuspensionAgente suspension = entidadService.findById(SuspensionAgente.class, solicitudSuspension.getId());
			//se cambia el estatus de la suspension a autorizada
			ValorCatalogoAgentes estatus=catalogoService.obtenerElementoEspecifico("Estatus de Suspension de Agentes", "RECHAZADA");		
			suspension.setEstatusMovimiento(estatus);
			entidadService.save(suspension);
			/*
			 * se envia notificasiones
			 */
//			sendByMail(suspension, estatus);
		}
		return solicitudSuspension;
	}

	 public void sendByMail(SuspensionAgente suspension, ValorCatalogoAgentes estatus){

		 agenteMidasService.enviarCorreo(agenteMidasService.obtenerCorreos(
			suspension.getAgente().getId(),getIdProceso(),getIdMovimiento()), 
			MailServiceSupport.mensajeCambioStatus(suspension.getAgente().getPersona().getNombreCompleto()), estatus.getValor(),
			GenericMailService.T_GENERAL, null);
			
	}
	 public Long getIdMovimiento(){
		 Long idProceso = null;
		 StringBuilder queryString = new StringBuilder();
		 queryString.append(" SELECT MOV.* FROM MIDAS.tcMovsProcesosAgentes MOV "+
		 " INNER JOIN MIDAS.tcProcesosAgentes PROC ON  MOV.IDPROCESO=PROC.ID "+
		 " Where MOV.descripcion ='ALTA DE REGISTRO' "+
		 " AND PROC.DESCRIPCION='CAMBIO DE ESTATUS' ");
		 Query query = entityManager.createNativeQuery(queryString.toString(),MovimientosProcesos.class);
		 List<MovimientosProcesos> movimientos = query.getResultList();
		 if(movimientos.isEmpty()||movimientos.get(0).getId()!=null){
			idProceso = movimientos.get(0).getId();
		 }
		 return idProceso;
	 }
	 public Long getIdProceso(){
		 Map<String,Object> param = new HashMap<String, Object>();
		 List<ProcesosAgentes> movtos = new ArrayList <ProcesosAgentes>();
		 param.put("descripcion", "CAMBIO DE ESTATUS");
		 movtos = entidadService.findByProperties(ProcesosAgentes.class, param);
		 Long idMovimiento = null;
		 if(movtos.isEmpty()||movtos.get(0).getId()!=null){
			 idMovimiento = movtos.get(0).getId();
		 }
		 return idMovimiento;
	 }
	@Override
	public SuspensionAgente autorizaSolicitudDeSuspension(SuspensionAgente solicitudSuspension)  throws Exception {
		if(solicitudSuspension !=null){
			validaDocumentosDigitalizados(solicitudSuspension);
			SuspensionAgente suspension = entidadService.findById(SuspensionAgente.class, solicitudSuspension.getId());
			
			//se relacion el agente con el id de la suspension, se cambia estatus del agente a suspendido y se modifica el motivo de estatus del agente			
			Agente agente = entidadService.findById(Agente.class, solicitudSuspension.getAgente().getId());
			//FIXME agregar el campo idSuspension en la entidad de agente
			ValorCatalogoAgentes  tipoSituacion= agente.getTipoSituacion();
			Long MotivoEstatus = agente.getIdMotivoEstatusAgente();
			agente.setIdSuspension(suspension.getId());
			agente.setTipoSituacion(suspension.getEstatusAgtSolicitado());
			agente.setIdMotivoEstatusAgente(suspension.getMotivoEstatus().getId());
			entidadService.save(agente);
			
			//se cambia el estatus de la suspension a autorizada
			ValorCatalogoAgentes estatus=catalogoService.obtenerElementoEspecifico("Estatus de Suspension de Agentes", "AUTORIZADA");		
			suspension.setEstatusMovimiento(estatus);
			entidadService.save(suspension);
			
			try{
				replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(agente, TipoAccionFuerzaVenta.GUARDAR);
			}catch(Exception e){
				agente.setTipoSituacion(tipoSituacion);
				agente.setIdMotivoEstatusAgente(MotivoEstatus);
				entidadService.save(agente);
				onError("Ha ocurrido un error al intentar replicar los datos del agente. Favor de reportar el problema al departamento de sistemas");
				LogDeMidasInterfaz.log("Ha ocurrido un error al intentar replicar los datos del agente" + this, Level.INFO, e);
			}
			/*
			 * se envia notificasiones
			 */
			sendByMail(suspension, estatus);
			
			
		}
		return solicitudSuspension;
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}


	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	@EJB
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}

	

	@Override
	public void auditarDocumentosEntregadosSuspension(Long idSuspension, Long idAgente,	String nombreAplicacion) throws Exception {
		if(isNull(idAgente)){
			onError("Favor de proporcionar la clave del registro");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}		
				
		String[] documentosFortimax;
		try {
			/*lista de documentos que estan dados de alta en el portal de fortimax*/
			documentosFortimax = fortimaxService.getDocumentFortimax(idAgente,nombreAplicacion);
			if(isNull(documentosFortimax)|| documentosFortimax.length==0){
				onError("No hay documentos para la aplicacion "+nombreAplicacion+" con el numero de expediente "+idAgente);
			}					
			/*lista de documentos guardados en base de datos midas*/
			List<EntregoDocumentosView> lista=suspensionAgenteService.consultaEstatusDocumentosSuspension(idSuspension, idAgente);
			if(isEmptyList(lista)){
				onError("No hay registros de documentos para la aplicacion "+nombreAplicacion+" con el expediente "+idAgente);
			}
			
			for(EntregoDocumentosView doc:lista){
				String nombreDocumento=doc.getValor();
				Long idDocumento = doc.getId();				
				if(isNotNull(doc) && isValid(nombreDocumento)){
					for(String docFortimax:documentosFortimax){
						//Si coincide el documento de base de datos con lo de fortimax y no ha sido subido, entonces lo marca como subido						
						DocumentosAgrupados rowDocumentos = entityManager.find(DocumentosAgrupados.class, idDocumento);	
						if(nombreDocumento.equalsIgnoreCase(docFortimax)){													
								rowDocumentos.setExisteDocumento(1);
								entidadService.save(rowDocumentos);
								break;							
						}
						
					}						
				}
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}


	@Override
	public List<EntregoDocumentosView> consultaEstatusDocumentosSuspension(Long idSuspension, Long idAgente) throws Exception {
		if(isNull(idAgente)){
			SuspensionAgente  suspension =  findById(SuspensionAgente.class, idSuspension);
			idAgente=suspension.getAgente().getId();
		}
		Agente agente = findById(Agente.class, idAgente);
		List<EntregoDocumentosView> listaDocumentosAgrupadosView = new ArrayList<EntregoDocumentosView>();
		StringBuilder queryString = new StringBuilder("");							
		queryString.append("select id,nombre as valor,existeDocumento as checado from MIDAS.trDocumentosAgrupados where idEntidad="+agente.getIdAgente()+" and (upper(nombre) like upper('SUSP"+idSuspension+"_%') " +
				"or upper(nombre) like upper('SUSP"+idSuspension+"_%')) order by id asc ");  // and idDocumento in(40,41,42)
		Query query=entityManager.createNativeQuery(queryString.toString(),EntregoDocumentosView.class);
		listaDocumentosAgrupadosView=query.getResultList();
		return listaDocumentosAgrupadosView;
	}


	@Override
	public List<DocumentosAgrupados> documentosFortimaxGuardadosDBSuspension(Long idSuspension, Long idAgente)throws Exception {
		List<DocumentosAgrupados> listaDocumentosAgrupados = new ArrayList<DocumentosAgrupados>();
		StringBuilder queryString = new StringBuilder("");							
		queryString.append("select id,idDocumento,idEntidad,idAgrupador,nombre,existeDocumento from MIDAS.trDocumentosAgrupados where idEntidad="+idAgente+" and (upper(nombre) like upper('SUSP"+idSuspension+"_%') " +
				"or upper(nombre) like upper('SUSP"+idSuspension+"_%')) order by id asc"); //and idDocumento in(40,41,42)
		Query query=entityManager.createNativeQuery(queryString.toString(),DocumentosAgrupados.class);
		listaDocumentosAgrupados=query.getResultList();
		return listaDocumentosAgrupados;
	}
	
	@Override
	public void crearYGenerarDocumentosFortimaxSuspension(SuspensionAgente suspension)throws Exception {
		
		if (suspension == null) {
			throw new Exception("No se encuentra la solicitud de suspension");
		} 
		
		if (suspension.getId() == null) {	
			throw new Exception("No se encuentra el id de la suspension");
		}
		
		if (suspension.getAgente() == null || suspension.getAgente().getId() == null) {
			throw new Exception("No se indico el id del agente a suspender");
		}
		
						
		List<CatalogoDocumentoFortimax> catalogoDocumentos;
		List<DocumentosAgrupados> documentosOriginales;
		List<String> documentosFortimax;
		List<DocumentosAgrupados> documentosOriginalesFiltrado;
		List<DocumentosAgrupados> documentosOriginalesAEliminar;
		List<String> documentosFortimaxFiltrado;
		CarpetaAplicacionFortimax carpeta;
		String nombreDocumentoGenerado;
		DocumentosAgrupados documento;
				
		String prefijoTipoMovimiento = "SUSP";
		Agente agente = new Agente();
		suspension.setAgente(entidadService.findById(Agente.class, suspension.getAgente().getId()));
		
		//obtener la carpeta donde se almacenaran los documentos en fortimax
		carpeta = carpetaAplicacionFortimaxDao.obtenerCarpetaEspecificaPorAplicacion(CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX,SuspensionAgente.CARPETA_SUSPENSIONES);

		//obtener los documentos requeridos para la solicitud de suspension
		catalogoDocumentos = documentoCarpetaFortimaxDao.
		obtenerDocumentosRequeridosPorCarpetaConTipoPersona(CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX, carpeta.getNombreCarpeta(), suspension.getAgente().getPersona().getClaveTipoPersona());

		//Se crea la carpeta del agente en caso de no existir en fortimax
		agenteMidasService.generateExpedientAgent(suspension.getAgente().getId());
		
		//Lista de documentos que estan dados de alta en el portal de Fortimax
		documentosFortimax = Arrays.asList(fortimaxService.getDocumentFortimax(suspension.getAgente().getId(), CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX));
		
		documentosOriginales = suspensionAgenteService.documentosFortimaxGuardadosDBSuspension(suspension.getId(), suspension.getAgente().getIdAgente());
		
		
//		for (CatalogoDocumentoFortimax documentoCatalogo : catalogoDocumentos) {
//			String nombreDocumento = prefijoTipoMovimiento+ suspension.getId() + "_" + suspension.getAgente().getIdAgente() + "_" + documentoCatalogo.getNombreDocumento();
//			
//			for(String docFortimax : documentosFortimax){
//				if(docFortimax!=null){
//					if(!docFortimax.equals(nombreDocumento)){				
//						//se generar los documentos en fortimax en caso de no existir
//	//					fortimaxService.generateDocument(suspension.getAgente().getId(),CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX,	nombreDocumento , carpeta.getNombreCarpetaFortimax());
//					}
//				}
//			}
//		}
		for (CatalogoDocumentoFortimax documentoCatalogo : catalogoDocumentos) {
			String nombreDocumentoDB = prefijoTipoMovimiento+ suspension.getId() + "_" + suspension.getAgente().getIdAgente() + "_" + documentoCatalogo.getNombreDocumento();
			Boolean existeEnBaseDatos = existeDocumentoEnBaseDeDatos(nombreDocumentoDB, documentosOriginales);
			
			String nombreDocumentoFortimax = prefijoTipoMovimiento+suspension.getId()+ "_" +suspension.getAgente().getIdAgente()+ "_" +documentoCatalogo.getNombreDocumento();
			Boolean existeEnFortimax = existeDocumentoEnFortimax(nombreDocumentoFortimax,documentosFortimax);
			
			if(existeEnFortimax==false){
				fortimaxService.generateDocument(suspension.getAgente().getId(), 
				CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX,	nombreDocumentoFortimax , carpeta.getNombreCarpetaFortimax());
			}
			
			if(existeEnBaseDatos==false){
				
				documento = new DocumentosAgrupados();
				documento.setIdAgrupador(null);
				documento.setIdDocumento(documentoCatalogo.getId());
				documento.setIdEntidad(suspension.getAgente().getIdAgente());
				documento.setNombre(nombreDocumentoDB);
				documento.setExisteDocumento(0);
				entidadService.save(documento);		
			}
						
		}
	}
		
	private Boolean existeDocumentoEnBaseDeDatos(String nombreDocumento,List<DocumentosAgrupados> listDocGuardados){
		boolean existe = false;
		for(DocumentosAgrupados docGuardado : listDocGuardados){
			if(docGuardado.getNombre().equals(nombreDocumento)){
				existe = true;
				break;
			}			
		}
		return existe;
	}
	private Boolean existeDocumentoEnFortimax(String nombreDocumento,List<String> listDocGuardados){
		boolean existe = false;
		for(String docGuardado : listDocGuardados){
			if(docGuardado != null && !docGuardado.isEmpty()  && docGuardado.equals(nombreDocumento)){
				existe = true;
				break;
			}			
		}
		return existe;
	}
	
	@SuppressWarnings("unchecked")
	private void validaDocumentosDigitalizados(SuspensionAgente suspension) throws Exception{
		if (suspension == null) {
			throw new Exception("No se encuentra la solicitud de suspension");
		} 
		
		if (suspension.getId() == null) {	
			throw new Exception("No se encuentra el id de la suspension");
		}
		
		if (suspension.getAgente() == null || suspension.getAgente().getId() == null) {
			throw new Exception("No se indico el id del agente a suspender");
		}
		
						
		List<CatalogoDocumentoFortimax> catalogoDocumentos;
		List<DocumentosAgrupados> documentosOriginales;
		List<String> documentosFortimax;
		CarpetaAplicacionFortimax carpeta;
				
		String prefijoTipoMovimiento = "SUSP";
		suspension.setAgente(entidadService.findById(Agente.class, suspension.getAgente().getId()));
		
		//obtener la carpeta donde se almacenaran los documentos en fortimax
		carpeta = carpetaAplicacionFortimaxDao.obtenerCarpetaEspecificaPorAplicacion(CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX,SuspensionAgente.CARPETA_SUSPENSIONES);

		//obtener los documentos requeridos para la solicitud de suspension
		catalogoDocumentos = documentoCarpetaFortimaxDao.
		obtenerDocumentosRequeridosPorCarpetaConTipoPersona(CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX, carpeta.getNombreCarpeta(), suspension.getAgente().getPersona().getClaveTipoPersona());

		//Se crea la carpeta del agente en caso de no existir en fortimax
		agenteMidasService.generateExpedientAgent(suspension.getAgente().getId());
		
		//Lista de documentos que estan dados de alta en el portal de Fortimax
		documentosFortimax = Arrays.asList(fortimaxService.getDocumentFortimax(suspension.getAgente().getId(), CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX));
		
		documentosOriginales = suspensionAgenteService.documentosFortimaxGuardadosDBSuspension(suspension.getId(), suspension.getAgente().getIdAgente());
		
		
		for (CatalogoDocumentoFortimax documentoCatalogo : catalogoDocumentos) {
			String nombreDocumentoDB = prefijoTipoMovimiento+ suspension.getId() + "_" + suspension.getAgente().getIdAgente() + "_" + documentoCatalogo.getNombreDocumento();
			Boolean existeEnBaseDatos = existeDocumentoEnBaseDeDatos(nombreDocumentoDB, documentosOriginales);
			
			String nombreDocumentoFortimax = prefijoTipoMovimiento+suspension.getId()+ "_" +suspension.getAgente().getIdAgente()+ "_" +documentoCatalogo.getNombreDocumento();
			Boolean existeEnFortimax = existeDocumentoEnFortimax(nombreDocumentoFortimax,documentosFortimax);
			
			if(existeEnFortimax==false || existeEnBaseDatos==false){
				throw new Exception("Hay documentos por digitalizar");
			}
		}
	}
	
}
