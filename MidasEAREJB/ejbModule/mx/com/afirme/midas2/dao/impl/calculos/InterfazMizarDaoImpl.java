package mx.com.afirme.midas2.dao.impl.calculos;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.sql.Types;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.calculos.InterfazMizarDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.util.MidasException;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.microsoft.sqlserver.jdbc.SQLServerException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class InterfazMizarDaoImpl extends EntidadDaoImpl implements InterfazMizarDao{
	private ValorCatalogoAgentesService catalogoService;
	@SuppressWarnings("unused")
	private DataSource dataSource;
	private SimpleJdbcCall procChequeDetail;
	private SimpleJdbcCall procChequeHead;
	private SimpleJdbcCall procChequeCheck;
	private enum ChequeHead_Params{
		ID_SESION("id_sesion"),
		ID_SOL_CHEQUE("id_sol_cheque"),
		IMP_PAGO("imp_pago"),
		FECHA("fecha"),
		MONEDA("moneda"),
		TIPO_CAMBIO("tipo_cambio"),
		NOM_BENEFICIARIO("nom_beneficiario"),
		CONCEPTO("concepto"),
		TIPO_SOLICITUD("tipo_solicitud"),
		ID_AGENTE("idagente"),
		ESTATUS("estatus"),
		ID_COD_RESP("id_cod_resp"),
		DESC_ERR("desc_err")
		;
		public String value;
		ChequeHead_Params(String value){
			this.value=value;
		}
	};
	private enum ChequeDetails_Params{
		ID_SESION("id_sesion"),
		ID_SOL_CHEQUE("id_sol_cheque"),
		AUXILIAR("auxiliar"),
		IVA_PCT("iva_pct"),
		PCT_IVA_RET("pct_iva_ret"),
		PCT_ISR_RET("pct_isr_ret"),
		TIPO_OPERACION("tipo_operacion"),
		CVEL_T_RUBRO("cvel_t_rubro"),
		ID_CUENTA_CONT("id_cuenta_cont"),
		CVE_C_A("cve_c_a"),
		IMP_PAGO("imp_pago"),
		CUENTA_CONTABLE("cuenta_contable"),
		ESTATUS("estatus"),
		ID_COD_RESP("id_cod_resp"),
		DESC_ERR("desc_err")
		;
		public String value;
		ChequeDetails_Params(String value){
			this.value=value;
		}
	};
	private enum ChequeCheck_Params{
		ID_SESION("id_sesion"),
		ESTATUS("estatus"),
		ID_COD_RESP("id_cod_resp"),
		DESC_ERR("desc_err")
		;
		public String value;
		ChequeCheck_Params(String value){
			this.value=value;
		}
	};
	private static ValorCatalogoAgentes tipoMoneda;
	/**
	 * Catalogo estatico para no volver a consultar en la base de datos.
	 * @return
	 */
	private ValorCatalogoAgentes getTipoMoneda(){
		if(tipoMoneda==null){
			try {
				tipoMoneda=catalogoService.obtenerElementoEspecifico("Tipo Moneda", "NACIONAL");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return tipoMoneda;
	}
	
	@Resource(name="jdbc/MizarDataSource")
	public void setDataSource(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
        this.procChequeHead = new SimpleJdbcCall(jdbcTemplate)
		.withCatalogName("dbo")
		.withProcedureName("spinsHead_md")
		.withoutProcedureColumnMetaDataAccess()
//		.useInParameterNames("id_sesion","id_sol_cheque","imp_pago","fecha","moneda","tipo_cambio","nom_beneficiario","concepto","tipo_solicitud")
		.useInParameterNames(
				ChequeHead_Params.ID_SESION.value,
				ChequeHead_Params.ID_SOL_CHEQUE.value,
				ChequeHead_Params.IMP_PAGO.value,
				ChequeHead_Params.FECHA.value,
				ChequeHead_Params.MONEDA.value,
				ChequeHead_Params.TIPO_CAMBIO.value,
				ChequeHead_Params.NOM_BENEFICIARIO.value,
				ChequeHead_Params.CONCEPTO.value,
				ChequeHead_Params.TIPO_SOLICITUD.value,
				ChequeHead_Params.ID_AGENTE.value
		)
		.declareParameters(new SqlParameter(ChequeHead_Params.ID_SESION.value, Types.INTEGER))
		.declareParameters(new SqlParameter(ChequeHead_Params.ID_SOL_CHEQUE.value, Types.INTEGER))
		.declareParameters(new SqlParameter(ChequeHead_Params.IMP_PAGO.value, Types.DECIMAL))
		.declareParameters(new SqlParameter(ChequeHead_Params.FECHA.value, Types.DATE))
		.declareParameters(new SqlParameter(ChequeHead_Params.MONEDA.value, Types.INTEGER))
		.declareParameters(new SqlParameter(ChequeHead_Params.TIPO_CAMBIO.value, Types.DECIMAL))
		.declareParameters(new SqlParameter(ChequeHead_Params.NOM_BENEFICIARIO.value, Types.VARCHAR))
		.declareParameters(new SqlParameter(ChequeHead_Params.CONCEPTO.value, Types.VARCHAR))
		.declareParameters(new SqlParameter(ChequeHead_Params.TIPO_SOLICITUD.value, Types.VARCHAR))
		.declareParameters(new SqlParameter(ChequeHead_Params.ID_AGENTE.value, Types.INTEGER))
		.declareParameters(new SqlOutParameter(ChequeHead_Params.ESTATUS.value,Types.VARCHAR))
		.declareParameters(new SqlOutParameter(ChequeHead_Params.ID_COD_RESP.value,Types.VARCHAR))
		.declareParameters(new SqlOutParameter(ChequeHead_Params.DESC_ERR.value,Types.VARCHAR));
		this.procChequeDetail = new SimpleJdbcCall(jdbcTemplate)
			.withCatalogName("dbo")
			.withProcedureName("spinsDetail")
			.withoutProcedureColumnMetaDataAccess()
			.useInParameterNames(
					ChequeDetails_Params.ID_SESION.value,
					ChequeDetails_Params.ID_SOL_CHEQUE.value,
					ChequeDetails_Params.AUXILIAR.value,
					ChequeDetails_Params.IVA_PCT.value,
					ChequeDetails_Params.PCT_IVA_RET.value,
					ChequeDetails_Params.PCT_ISR_RET.value,
					ChequeDetails_Params.TIPO_OPERACION.value,
					ChequeDetails_Params.CVEL_T_RUBRO.value,
					ChequeDetails_Params.ID_CUENTA_CONT.value,
					ChequeDetails_Params.CVE_C_A.value,
					ChequeDetails_Params.IMP_PAGO.value,
					ChequeDetails_Params.CUENTA_CONTABLE.value
			)
			.declareParameters(new SqlParameter(ChequeDetails_Params.ID_SESION.value, Types.INTEGER))
			.declareParameters(new SqlParameter(ChequeDetails_Params.ID_SOL_CHEQUE.value, Types.INTEGER))
			.declareParameters(new SqlParameter(ChequeDetails_Params.AUXILIAR.value, Types.INTEGER))
			.declareParameters(new SqlParameter(ChequeDetails_Params.IVA_PCT.value, Types.INTEGER))
			.declareParameters(new SqlParameter(ChequeDetails_Params.PCT_IVA_RET.value, Types.INTEGER))
			.declareParameters(new SqlParameter(ChequeDetails_Params.PCT_ISR_RET.value, Types.INTEGER))
			.declareParameters(new SqlParameter(ChequeDetails_Params.TIPO_OPERACION.value, Types.VARCHAR))
			.declareParameters(new SqlParameter(ChequeDetails_Params.CVEL_T_RUBRO.value, Types.VARCHAR))
			.declareParameters(new SqlParameter(ChequeDetails_Params.ID_CUENTA_CONT.value, Types.BIGINT))
			.declareParameters(new SqlParameter(ChequeDetails_Params.CVE_C_A.value, Types.VARCHAR))
			.declareParameters(new SqlParameter(ChequeDetails_Params.IMP_PAGO.value, Types.DECIMAL))
			.declareParameters(new SqlParameter(ChequeDetails_Params.CUENTA_CONTABLE.value, Types.VARCHAR))
			.declareParameters(new SqlOutParameter(ChequeDetails_Params.ESTATUS.value,Types.VARCHAR))
			.declareParameters(new SqlOutParameter(ChequeDetails_Params.ID_COD_RESP.value,Types.VARCHAR))
			.declareParameters(new SqlOutParameter(ChequeDetails_Params.DESC_ERR.value,Types.VARCHAR));;
		this.procChequeCheck = new SimpleJdbcCall(jdbcTemplate)
			.withCatalogName("dbo")
			.withProcedureName("spinsCheck")
			.withoutProcedureColumnMetaDataAccess()
			.useInParameterNames(ChequeCheck_Params.ID_SESION.value)
			.declareParameters(new SqlParameter(ChequeCheck_Params.ID_SESION.value, Types.INTEGER))
			.declareParameters(new SqlOutParameter(ChequeCheck_Params.ESTATUS.value,Types.VARCHAR))
			.declareParameters(new SqlOutParameter(ChequeCheck_Params.ID_COD_RESP.value,Types.VARCHAR))
			.declareParameters(new SqlOutParameter(ChequeCheck_Params.DESC_ERR.value,Types.VARCHAR));
	}
	/**
	 * Actualiza el estatus de una solicitud de cheque de MIZAR de un calculo, este cheque representa lo que se le va a pagar en comisiones
	 * a un agente.
	 * @param cheque
	 * @throws MidasException
	 */
	@Override
	public void actualizarSolitudChequeMizar(DetalleCalculoComisiones cheque) throws MidasException {
		Long idCalculo=null;
		try{
			LogDeMidasInterfaz.log("Entrando a InterfazMizarDaoImpl.actualizarSolitudChequeMizar..." + this, Level.INFO, null);
			CalculoComisiones calculo=cheque.getCalculoComisiones();
			idCalculo=(calculo!=null)?calculo.getId():null;
			ValorCatalogoAgentes tipoMoneda=getTipoMoneda();
			String tipoMonedaValor=(isNotNull(tipoMoneda) && isValid(tipoMoneda.getClave()))?tipoMoneda.getClave():null;
			String beneficiario=(isNotNull(cheque)&& isNotNull(cheque.getAgente()) && isNotNull(cheque.getAgente().getPersona()) && isValid(cheque.getAgente().getPersona().getNombreCompleto()))?cheque.getAgente().getPersona().getNombreCompleto():null;
			SqlParameterSource in = new MapSqlParameterSource()
			.addValue(ChequeHead_Params.ID_SESION.value,idCalculo)
			.addValue(ChequeHead_Params.ID_SOL_CHEQUE.value,cheque.getIdSolicitudCheque())
			.addValue(ChequeHead_Params.IMP_PAGO.value,cheque.getImporte())
			.addValue(ChequeHead_Params.FECHA.value,calculo.getFechaCalculo())
			.addValue(ChequeHead_Params.MONEDA.value,tipoMonedaValor)
			.addValue(ChequeHead_Params.TIPO_CAMBIO.value,1)
			.addValue(ChequeHead_Params.NOM_BENEFICIARIO.value,beneficiario)
//			.addValue(ChequeHead_Params.CONCEPTO.value,"PAGO 1ER EXD")
			.addValue(ChequeHead_Params.CONCEPTO.value,"PAGO MIDAS COMIS AGT "+cheque.getAgente().getIdAgente())			
			.addValue(ChequeHead_Params.TIPO_SOLICITUD.value,"AGT")//Para que aparezca como agente en Mizar
			.addValue(ChequeHead_Params.ID_AGENTE.value,cheque.getAgente().getIdAgente());//16-06-2014 se agrega paramatro id_Agente
			
			LogDeMidasInterfaz.log("Entrando a InterfazMizarDaoImpl.actualizarSolitudChequeMizar...Calculo|Id_Sesion:["+idCalculo+"]/Sol_Cheque:["+cheque.getIdSolicitudCheque()+"]"+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("id_sesion:["+idCalculo+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("id_sol_cheque:["+cheque.getIdSolicitudCheque()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("imp_pago:["+cheque.getImporte()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("fecha:["+calculo.getFechaCalculo()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("moneda:["+tipoMonedaValor+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("tipo_cambio:["+1+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("nom_beneficiario:["+beneficiario+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("concepto:[PAGO MIDAS COMIS AGT "+cheque.getAgente().getIdAgente()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("tipo_solicitud:[AGT]", Level.INFO, null);
			LogDeMidasInterfaz.log("idagente:"+cheque.getAgente().getIdAgente(), Level.INFO, null);
			//log.info("Ejecutando procAutMigraEndoso.");
//			procChequeHead.withoutProcedureColumnMetaDataAccess();
//			procChequeHead.setReturnValueRequired(false);
//			procChequeHead.withoutProcedureColumnMetaDataAccess();
//			procChequeHead.declareParameters(new SqlParameter("id_sesion", Types.INTEGER))
//			.declareParameters(new SqlParameter("id_sol_cheque", Types.INTEGER))
//			.declareParameters(new SqlParameter("imp_pago", Types.DECIMAL))
//			.declareParameters(new SqlParameter("fecha", Types.DATE))
//			.declareParameters(new SqlParameter("moneda", Types.INTEGER))
//			.declareParameters(new SqlParameter("tipo_cambio", Types.DECIMAL))
//			.declareParameters(new SqlParameter("nom_beneficiario", Types.VARCHAR))
//			.declareParameters(new SqlParameter("concepto", Types.VARCHAR))
//			.declareParameters(new SqlParameter("tipo_solicitud", Types.VARCHAR));
			
			Map<String,Object>  data = procChequeHead.execute(in);
			
			if(isNotNull(data)){
				Object est=data.get("estatus");
				String estatus=(est!=null)?est.toString().toString():"null";
				Object desc_err = data.get("desc_err");
				String desc = (desc_err!=null)? desc_err.toString():"null";
				Object id_cod_resp = data.get("id_cod_resp");
				String id_cod = (id_cod_resp!=null)?id_cod_resp.toString():"null";
				LogDeMidasInterfaz.log("procChequeHead | estatus - "+ estatus +"  id_cod_resp: "+desc+" | id_cod_resp: "+id_cod, Level.INFO,null);
//			if(isNotNull(data)){
//				Object id=data.get("pIdCalculo");
//				idCalculo=(id!=null)?Long.parseLong(id.toString()):null;
			}
			LogDeMidasInterfaz.log("Saliendo de InterfazMizarDaoImpl.actualizarSolitudChequeMizar..." + this, Level.INFO, null);
		}catch (Exception e) {
			e.printStackTrace();
			LogDeMidasInterfaz.log("Excepcion general en InterfazMizarDaoImpl.actualizarSolitudChequeMizar..." + this, Level.WARNING, e);
			onError(e);
		}
	}
	/**
	 * Ejecuta spInsHead en SQL Server el cheque de MIZAR-Cabecera
	 * @param prestamo
	 * @throws MidasException
	 */
	public void actualizarSolitudChequeMizarPrestamo(ConfigPrestamoAnticipo prestamo) throws MidasException {
		Long idCalculo=null;
		try{
			if(isNull(prestamo) || isNull(prestamo.getId())){
				onError("Favor de proporcionar el prestamo");
			}
			LogDeMidasInterfaz.log("Entrando a InterfazMizarDaoImpl.actualizarSolitudChequeMizarPrestamo..." + this, Level.INFO, null);
			idCalculo=prestamo.getId();
			ValorCatalogoAgentes tipoMoneda=getTipoMoneda();
			String tipoMonedaValor=(isNotNull(tipoMoneda) && isValid(tipoMoneda.getClave()))?tipoMoneda.getClave():null;
			String beneficiario=(isNotNull(prestamo)&& isNotNull(prestamo.getAgente()) && isNotNull(prestamo.getAgente().getPersona()) && isValid(prestamo.getAgente().getPersona().getNombreCompleto()))?prestamo.getAgente().getPersona().getNombreCompleto():null;
			SqlParameterSource in = new MapSqlParameterSource()
			.addValue(ChequeHead_Params.ID_SESION.value,idCalculo)
			.addValue(ChequeHead_Params.ID_SOL_CHEQUE.value,prestamo.getIdSolicitudCheque())
			.addValue(ChequeHead_Params.IMP_PAGO.value,prestamo.getImporteOtorgado())
			.addValue(ChequeHead_Params.FECHA.value,prestamo.getFechaAltaMovimiento())
			.addValue(ChequeHead_Params.MONEDA.value,tipoMonedaValor)
			.addValue(ChequeHead_Params.TIPO_CAMBIO.value,1)
			.addValue(ChequeHead_Params.NOM_BENEFICIARIO.value,beneficiario)
			.addValue(ChequeHead_Params.CONCEPTO.value,"PAGO MIDAS PRES/ANTI AGENTE "+prestamo.getAgente().getIdAgente())
			.addValue(ChequeHead_Params.TIPO_SOLICITUD.value,"AGT")//Para que aparezca como agente en Mizar
			.addValue(ChequeHead_Params.ID_AGENTE.value,prestamo.getAgente().getIdAgente());//Para que aparezca como agente en Mizar
			
			LogDeMidasInterfaz.log("Entrando a InterfazMizarDaoImpl.actualizarSolitudChequeMizarPrestamo...Calculo|Id_Sesion:["+idCalculo+"]/Sol_Cheque:["+prestamo.getIdSolicitudCheque()+"]"+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("id_sesion:["+idCalculo+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("id_sol_cheque:["+prestamo.getIdSolicitudCheque()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("imp_pago:["+prestamo.getImporteOtorgado()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("fecha:["+prestamo.getFechaAltaMovimiento()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("moneda:["+tipoMonedaValor+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("tipo_cambio:["+1+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("nom_beneficiario:["+beneficiario+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("concepto:[PAGO MIDAS PRES/ANTI AGENTE "+prestamo.getAgente().getIdAgente()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("tipo_solicitud:[AGT]", Level.INFO, null);
			LogDeMidasInterfaz.log("idagente:["+prestamo.getAgente().getIdAgente()+"]", Level.INFO, null);
			procChequeHead.execute(in);
			LogDeMidasInterfaz.log("Saliendo de InterfazMizarDaoImpl.actualizarSolitudChequeMizarPrestamo..." + this, Level.INFO, null);
		}catch (Exception e) {
			e.printStackTrace();
			LogDeMidasInterfaz.log("Excepcion general en InterfazMizarDaoImpl.actualizarSolitudChequeMizarPrestamo..." + this, Level.WARNING, e);
			onError(e);
		}
	}
	/**
	 * Actualiza en MIZAR los detalles de una solicitud de cheque de un calculo, es decir, una solicitud de cheque puede tener
	 * al menos un o mas detalles de solicitud de cheque, estos se actualizan su estatus por medio de un stored procedure de SQLSERVER
	 * utilizando el datasource de Mizar
	 * @param cheque
	 * @param idCalculo
	 * @throws MidasException
	 */
	@Override
	public void actualizarDetalleSolitudChequeMizar(DetalleSolicitudCheque cheque,Long idSolicitudCheque,Long idCalculo,Long claveAgente)throws MidasException {
		try{
			SqlParameterSource in = new MapSqlParameterSource()
			.addValue(ChequeDetails_Params.ID_SESION.value,idCalculo)
			.addValue(ChequeDetails_Params.ID_SOL_CHEQUE.value,idSolicitudCheque)
			.addValue(ChequeDetails_Params.AUXILIAR.value,claveAgente)
			.addValue(ChequeDetails_Params.IVA_PCT.value, 16)
			.addValue(ChequeDetails_Params.PCT_IVA_RET.value, 0)
			.addValue(ChequeDetails_Params.PCT_ISR_RET.value, 0)
			.addValue(ChequeDetails_Params.TIPO_OPERACION.value, "00")
			.addValue(ChequeDetails_Params.CVEL_T_RUBRO.value,cheque.getRubro())
			.addValue(ChequeDetails_Params.ID_CUENTA_CONT.value,cheque.getIdCuentaContable())
			.addValue(ChequeDetails_Params.CVE_C_A.value,cheque.getConcepto())
			.addValue(ChequeDetails_Params.IMP_PAGO.value,cheque.getImporte())
			.addValue(ChequeDetails_Params.CUENTA_CONTABLE.value,cheque.getCuentaContable());
			LogDeMidasInterfaz.log("Entrando a InterfazMizarDaoImpl.actualizarDetalleSolitudChequeMizar...Calculo|Id_Sesion:["+idCalculo+"]/Sol_Cheque:["+idSolicitudCheque+"]/Rubro:["+cheque.getRubro()+"]" + this, Level.INFO, null);
			LogDeMidasInterfaz.log("id_sesion:["+idCalculo+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("id_sol_cheque:["+idSolicitudCheque+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("auxiliar:["+claveAgente+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("cvel_t_rubro:["+cheque.getRubro()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("id_cuenta_cont:["+cheque.getIdCuentaContable()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("cve_c_a:["+cheque.getConcepto()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("imp_pago:["+cheque.getImporte()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("cuenta_contable:["+cheque.getCuentaContable()+"]", Level.INFO, null);
			//log.info("Ejecutando procAutMigraEndoso.");
			Map<String,Object> data = procChequeDetail.execute(in);
			if(isNotNull(data)){
				Object est=data.get("estatus");
				String estatus=(est!=null)?est.toString().toString():"null";
				Object id_cod=data.get("id_cod_resp");
				String id_cod_resp=(id_cod!=null)?id_cod.toString().toString():"null";
				Object desc = data.get("desc_err");
				String desc_err=(desc!=null)?desc.toString():"null";
				LogDeMidasInterfaz.log("procChequeDetail | estatus - "+estatus+" | id_cod_resp - "+id_cod_resp+" | desc_err - "+desc_err, Level.INFO, null);
			}
			LogDeMidasInterfaz.log("Saliendo de InterfazMizarDaoImpl.actualizarDetalleSolitudChequeMizar..." + this, Level.INFO, null);
		}catch (Exception e) {
			e.printStackTrace();
			LogDeMidasInterfaz.log("Excepcion general en InterfazMizarDaoImpl.actualizarDetalleSolitudChequeMizar..." + this, Level.WARNING, e);
			onError(e);
		}
	}
	

	@Override
	public void importarSolicitudesChequesAMizar(Long idCalculo) throws MidasException {
		if(isNull(idCalculo)){
			onError("Favor de proporcioanr el calculo para obtener las solicitudes de cheques que se importaran a Mizar");
		}
		try{
			SqlParameterSource in = new MapSqlParameterSource()
			.addValue(ChequeCheck_Params.ID_SESION.value,idCalculo);
			LogDeMidasInterfaz.log("Entrando a InterfazMizarDaoImpl.importarSolicitudesChequesAMizar...Calculo|Id_Sesion:["+idCalculo+"]" + this, Level.INFO, null);
			LogDeMidasInterfaz.log("id_sesion:["+idCalculo+"]", Level.INFO, null);
			//log.info("Ejecutando procAutMigraEndoso.");
			Map<String,Object> data=procChequeCheck.execute(in);
			if(isNotNull(data)){
				Object est=data.get("estatus");
				String estatus=(est!=null)?est.toString().toString():"null";
				Object id_cod=data.get("id_cod_resp");
				String id_cod_resp=(id_cod!=null)?id_cod.toString().toString():"null";
				Object desc = data.get("desc_err");
				String desc_err=(desc!=null)?desc.toString():"null";
				LogDeMidasInterfaz.log("procChequeCheck | estatus - "+estatus+" estatus | id_cod_resp - "+id_cod_resp+" | desc_err - "+desc_err, Level.INFO, null);
			}
			LogDeMidasInterfaz.log("Saliendo de InterfazMizarDaoImpl.importarSolicitudesChequesAMizar..." + this, Level.INFO, null);
		}catch (Exception e) {
			e.printStackTrace();
			LogDeMidasInterfaz.log("Excepcion general en InterfazMizarDaoImpl.importarSolicitudesChequesAMizar..." + this, Level.WARNING, e);
			//Si no es una excepcion de SQL Server entonces se muestra en pantalla, sino es un problema de Mizar 
			if(!(e instanceof SQLServerException)){
				onError(e);
			}
		}
	}
	
	/**
	 * ===============================================================================
	 * 	Sets & gets
	 * ===============================================================================
	 * 
	 */
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}

}
