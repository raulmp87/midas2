package mx.com.afirme.midas2.dao.impl.tarifa;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaAgrupadorTarifaDao;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifaId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;
import mx.com.afirme.midas2.util.UtileriasWeb;

@Stateless
public class TarifaAgrupadorTarifaDaoImpl extends JpaDao<TarifaAgrupadorTarifaId, TarifaAgrupadorTarifa> implements TarifaAgrupadorTarifaDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<AgrupadorTarifa>  getListAgrupadorTarifaPorNegocio(
			String claveNegocio) {
			try {
				
				final String queryString = "Select model from AgrupadorTarifa model where " +
				"model.claveNegocio = :claveNegocio order by model.descripcionAgrupador, model.id.idVerAgrupadorTarifa asc";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("claveNegocio", claveNegocio);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				return query.getResultList();
			} catch (RuntimeException re) {
				LogDeMidasEJB3.log("find all getMapAgrupadorTarifaPorNegocio failed", Level.SEVERE, re);
				throw re;
			}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgrupadorTarifaSeccion> getListLineaNegocioPorAgrupadorTarifa(
			String idToAgrupadorTarifaFromString) {
		
		try {
			String[] idToAgrupadorTarifaFromStringSplit = idToAgrupadorTarifaFromString.split("_");
			
			final String queryString =  "Select model from AgrupadorTarifaSeccion model where " +
			"model.id.idToAgrupadorTarifa = :idToAgrupadorTarifa order by model.id.idToSeccion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToAgrupadorTarifa", Long.valueOf(idToAgrupadorTarifaFromStringSplit[0]));
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all getMapLineaNegocioPorAgrupadorTarifa failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<AgrupadorTarifaSeccion> getListMomendaPorAgrupadorTarifaLineaNegocio(
			String idToAgrupadorTarifaFromString, BigDecimal idToSeccion) {
		try {
			String[] idToAgrupadorTarifaFromStringSplit = idToAgrupadorTarifaFromString.split("_");
			
			final String queryString =  "Select model from AgrupadorTarifaSeccion model where " +
			" (model.id.idToAgrupadorTarifa = :idToAgrupadorTarifa) and (model.id.idToSeccion = :idToSeccion ) order by model.id.idMoneda asc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToAgrupadorTarifa", Long.valueOf(idToAgrupadorTarifaFromStringSplit[0]));
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all getMapMomendaPorAgrupadorTarifaLineaNegocio failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaAgrupadorTarifa> getTarifaAgrupadorTarifaPorAgrupadorTarifaLineaNegocio(
			TarifaAgrupadorTarifa tarifaAgrupadorTarifa, String claveNegocio) {
		
		try {
			
			final String queryString =  "Select model from TarifaAgrupadorTarifa model where (" +
			"(model.id.idToAgrupadorTarifa = :idToAgrupadorTarifa) and (model.id.idVerAgrupadorTarifa = :idVerAgrupadorTarifa) " +
			" and (model.id.idMoneda = :idMoneda)) " +
			"order by model.id.idMoneda, model.id.idToRiesgo, model.id.idConcepto asc ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToAgrupadorTarifa", tarifaAgrupadorTarifa.getId().getIdToAgrupadorTarifa());
			query.setParameter("idVerAgrupadorTarifa", tarifaAgrupadorTarifa.getId().getIdVerAgrupadorTarifa());
			query.setParameter("idMoneda", tarifaAgrupadorTarifa.getId().getIdMoneda());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all getMapMomendaPorAgrupadorTarifaLineaNegocio failed", Level.SEVERE, re);
			throw re;
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaVersion> getTarifaVersionPorLineaNegocio(String claveNegocio) {
	
		try {
			final String queryString;
			if (claveNegocio.equals("A")){
				queryString =   "Select model from TarifaVersion model, TarifaConcepto concepto where " +
				"(model.id.idConcepto = concepto.id.idConcepto) and (model.id.idRiesgo = concepto.id.idRiesgo) " +
				"and (concepto.claveNegocio = :claveNegocio or concepto.claveNegocio = ' ')  " +
				"order by model.id.idMoneda, model.id.idRiesgo, model.id.idConcepto asc";
			}else{
				queryString =   "Select model from TarifaVersion model, TarifaConcepto concepto where " +
				"(model.id.idConcepto = concepto.id.idConcepto) and (model.id.idRiesgo = concepto.id.idRiesgo) " +
				"and (concepto.claveNegocio = :claveNegocio )  " +
				"order by model.id.idMoneda, model.id.idRiesgo, model.id.idConcepto asc";
			}
			Query query = entityManager.createQuery(queryString);
			query.setParameter("claveNegocio", claveNegocio);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all getTarifaVersionPorLineaNegocio failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MonedaDTO> getListMonedaPorAgrupadorTarifa(
			String idToAgrupadorTarifaFromString) {
		try {
			String[] idToAgrupadorTarifaFromStringSplit = idToAgrupadorTarifaFromString.split(UtileriasWeb.SEPARADOR);
			
			final String queryString =  "Select distinct model.monedaDTO " +
					" from AgrupadorTarifaSeccion model where " +
			" (model.id.idToAgrupadorTarifa = :idToAgrupadorTarifa) order by model.id.idMoneda asc";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToAgrupadorTarifa", Long.valueOf(idToAgrupadorTarifaFromStringSplit[0]));
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all getMapMonedaPorAgrupadorTarifa failed", Level.SEVERE, re);
			throw re;
		}
	}

}
