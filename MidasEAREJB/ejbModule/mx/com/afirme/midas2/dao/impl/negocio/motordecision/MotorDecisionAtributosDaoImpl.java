package mx.com.afirme.midas2.dao.impl.negocio.motordecision;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.negocio.motordecision.MotorDecisionAtributosDao;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO.FiltrosMotorDecisionDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO.TipoConsulta;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO.TipoResultado;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO.TipoValor;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class MotorDecisionAtributosDaoImpl implements MotorDecisionAtributosDao{
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	@EJB
	protected NegocioDerechosService negocioDerechosService;
	
	@EJB
	private EntidadService entidadService;
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<ResultadoMotorDecisionAtributosDTO> consultaValoresDao(MotorDecisionDTO.TipoConsulta tipoConsulta, Long idToNegocio, List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtroReq, List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtroNoReq) {
		
		List<ResultadoMotorDecisionAtributosDTO> resultadoMotorDesicionAtributosDtoList = new ArrayList<ResultadoMotorDecisionAtributosDTO>();
		Map<String, Object> parameters = null;
		
		String query = "";
		List tempList = new ArrayList();
		
		switch (tipoConsulta){
			case TIPO_CONSULTA_DERECHOS:
					parameters = new HashMap<String, Object>(1);
					query += "Select model from NegocioConfiguracionDerecho model where ";
					if(!filtroReq.isEmpty() && !filtroNoReq.isEmpty()){
						query = this.creaQuery(query, filtroReq, parameters);
						tempList = this.execQueryFilReq(query, parameters);
					}
				break;
			case TIPO_CONSULTA_SUMASASEGURADAS:
				parameters = new HashMap<String, Object>(1);
				query += "Select model from NegocioCobPaqSeccion model where ";
				if(!filtroReq.isEmpty()){
					query = this.creaQuery(query, filtroReq, parameters);
					if(!filtroNoReq.isEmpty()){
						query += " AND ";
						query = this.creaQueryNoReq(query, filtroNoReq, parameters);
					}
					tempList = this.execQueryFilReq(query, parameters);
				}
				break;	
		}
		
		ResultadoMotorDecisionAtributosDTO resultadoMotorDesicionAtributosDto = new ResultadoMotorDecisionAtributosDTO();
		
		if (!tempList.isEmpty()){
			switch (tipoConsulta){
				case TIPO_CONSULTA_DERECHOS:
						List<Object> valTipDerList = new ArrayList<Object>();
			        	for(FiltrosMotorDecisionDTO registro : filtroNoReq){
			        		List filterList = getListFiltrosNoRequeridos(registro.getNombreFiltro(), 
			        				(registro.getValorFiltro() == null)? null : registro.getValorFiltro().toString(), 
			        				tempList, tipoConsulta);
			        		if(!filterList.isEmpty()){
			        			tempList = filterList;
			        		}else{
			        			tempList = getListFiltrosNoRequeridos(registro.getNombreFiltro(), null, tempList, tipoConsulta);
			        		}
			        	}
						for(NegocioConfiguracionDerecho registro : (List<NegocioConfiguracionDerecho>)tempList){
							
							ResultadoMotorDecisionAtributosDTO res = new ResultadoMotorDecisionAtributosDTO();
							ResultadoMotorDecisionAtributosDTO.ValorTipo valTip = res.new ValorTipo();
							
							valTip.setValor(registro.getImporteDerecho().toString());
							valTip.setIdKey(registro.getIdToNegDerecho());
							
							if(registro.isImporteDefault()){
								valTip.setTipValor(TipoValor.TIPO_VALOR_DEFAULT);
							}else{
								valTip.setTipValor(TipoValor.TIPO_VALOR_GENERICO);
							}
							valTipDerList.add(valTip);
						}
						resultadoMotorDesicionAtributosDto.setValores(valTipDerList);
						resultadoMotorDesicionAtributosDto.setTexto("");
						resultadoMotorDesicionAtributosDto.setTipoResultado(TipoResultado.RES_TIPO_DERECHOS);
						
						resultadoMotorDesicionAtributosDtoList.add(resultadoMotorDesicionAtributosDto);
					break;
				case TIPO_CONSULTA_SUMASASEGURADAS:
					parameters = new HashMap<String, Object>(1);
					NegocioCobPaqSeccion resCerti = (NegocioCobPaqSeccion) tempList.get(0);
					List listResult = new ArrayList();
					
					StringBuffer queryValores = new StringBuffer("Select model from NegocioCobPaqSeccion model where ");
					queryValores.append( this.creaQuery(new String(), filtroReq, parameters) );
					
					if(resCerti.getEstadoDTO() != null){
						queryValores.append(" AND model.estadoDTO=:idEstado ");
						parameters.put("idEstado", resCerti.getEstadoDTO());
					}else{
						queryValores.append(" AND model.estadoDTO IS NULL");
					}
					if(resCerti.getCiudadDTO() != null){
						queryValores.append(" AND model.ciudadDTO=:idCiudad ");
						parameters.put("idCiudad", resCerti.getCiudadDTO());
					}else{
						queryValores.append(" AND model.ciudadDTO IS NULL");
					}
					if(resCerti.getTipoUsoVehiculo() != null){
						queryValores.append(" AND model.tipoUsoVehiculo=:tUsoVeh ");
						parameters.put("tUsoVeh", resCerti.getTipoUsoVehiculo());
					}else{
						queryValores.append(" AND model.tipoUsoVehiculo IS NULL");
					}
					if(resCerti.getAgente() != null){
						queryValores.append(" AND model.agente=:agente ");
						parameters.put("agente", resCerti.getAgente());
					}else{
						queryValores.append(" AND model.agente IS NULL");
					}
					if(resCerti.isRenovacion() != null){
						queryValores.append(" AND model.renovacion=:renovacion ");
						parameters.put("renovacion", resCerti.isRenovacion());
					}else{
						queryValores.append(" AND model.renovacion IS NULL");
					}
					
					listResult = this.execQueryFilReq(queryValores.toString(), parameters);
					
					
					List<NegocioCobPaqSeccion> resultados = (List<NegocioCobPaqSeccion>)listResult;
					for(NegocioCobPaqSeccion result : resultados){
						List<NegocioCobSumAse> sumas = entidadService.findByProperty(NegocioCobSumAse.class, "negocioCobPaqSeccion", result);
						result.setNegocioCobSumAseList(sumas);
					}
					
					resultadoMotorDesicionAtributosDto.setValores((List)resultados);
					resultadoMotorDesicionAtributosDto.setTexto("");
					resultadoMotorDesicionAtributosDto.setTipoResultado(TipoResultado.RES_TIPO_SUMASE_VALORES);
					
					resultadoMotorDesicionAtributosDtoList.add(resultadoMotorDesicionAtributosDto);
					break;
			}
		}
		
		if(tipoConsulta.equals(TipoConsulta.TIPO_CONSULTA_DERECHOS)
				&& tempList.isEmpty()){
			List <NegocioDerechoPoliza> listDerechos = negocioDerechosService.obtenerDerechosPoliza(idToNegocio);
			List<Object> valTipList = new ArrayList<Object>();
			for(NegocioDerechoPoliza derecho : listDerechos){
				ResultadoMotorDecisionAtributosDTO.ValorTipo valTip = resultadoMotorDesicionAtributosDto.new ValorTipo();
				valTip.setIdKey(derecho.getIdToNegDerechoPoliza());
				valTip.setValor(derecho.getImporteDerecho().toString());
				if(derecho.getClaveDefault()){
					valTip.setTipValor(TipoValor.TIPO_VALOR_DEFAULT);
				}else{
					valTip.setTipValor(TipoValor.TIPO_VALOR_GENERICO);
				}
				valTipList.add(valTip);
			}
			resultadoMotorDesicionAtributosDto.setValores(valTipList);
			resultadoMotorDesicionAtributosDto.setTexto("");
			resultadoMotorDesicionAtributosDto.setTipoResultado(TipoResultado.RES_TIPO_DERECHOS);
			
			resultadoMotorDesicionAtributosDtoList.add(resultadoMotorDesicionAtributosDto);
		}
		
		return resultadoMotorDesicionAtributosDtoList;
	}
	
	protected void setQueryParameters(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			entityQuery.setParameter(pairs.getKey(), pairs.getValue());
		}		
	}
	
	@SuppressWarnings("rawtypes")
	protected List getListFiltrosNoRequeridos(final String attribute, final Object value, Collection<Object> collection, final TipoConsulta tipoConsulta) {
		List returnList = new ArrayList();
		
		Predicate filterPredicate = new Predicate() {        
	        public boolean evaluate(Object arg0) {
	        	Object bean = new Object();
	        	switch (tipoConsulta) {
					case TIPO_CONSULTA_DERECHOS:
						bean = (NegocioConfiguracionDerecho)arg0;
						break;
					case TIPO_CONSULTA_SUMASASEGURADAS:
						bean = (NegocioCobPaqSeccion)arg0;
						break;
				}
	        	Object predicateValue = null;
        		try {
//        			System.out.println("attribute="+attribute);
	        		predicateValue = BeanUtils.getProperty(bean, attribute);
	        	} catch (IllegalAccessException e) {
	        		e.printStackTrace();
	        	} catch (InvocationTargetException e) { 
	        		e.printStackTrace();
	        	} catch (NoSuchMethodException e) { 
	        		e.printStackTrace();
	        	}
	        	
	        	if (value == null){
	        		return predicateValue == null;
	        	}else{
	        		return predicateValue != null && value != null && predicateValue.equals(value.toString());
	        	}
        	}
		};
		returnList = (List)CollectionUtils.select(collection, filterPredicate);
		return returnList;
	}
		
	@SuppressWarnings("rawtypes")
	protected List execQueryFilReq(String query, Map<String, Object> parameters){
		Query entityQuery = entityManager.createQuery(query);
		entityQuery.setHint(QueryHints.REFRESH, HintValues.TRUE);
		this.setQueryParameters(entityQuery, parameters);
		return entityQuery.getResultList();
	}
	
	protected String creaQuery(String query, List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtroReq, Map<String, Object> parameters ){
		if (!filtroReq.isEmpty()){
			int tamReq = 0;
			for(MotorDecisionDTO.FiltrosMotorDecisionDTO filVal : filtroReq){
				tamReq ++;
				if(filtroReq.size() == tamReq){
					query += " model."+filVal.getNombreFiltro()+"=:"+filVal.getNombreFiltro().replaceAll("\\.", "");
				}else{
					query += " model."+filVal.getNombreFiltro()+"=:"+filVal.getNombreFiltro().replaceAll("\\.", "")+" and ";
				}
				parameters.put(filVal.getNombreFiltro().replaceAll("\\.", ""), filVal.getValorFiltro()); 
			}
		}
		return query;
	}
	
	protected String creaQueryNoReq(String query, List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtroNoReq, Map<String, Object> parameters ){
		if (!filtroNoReq.isEmpty()){
			String order = "";
			int tamReq = 0;
			for(MotorDecisionDTO.FiltrosMotorDecisionDTO filVal : filtroNoReq){
				tamReq ++;
				query += " (model."+filVal.getNombreFiltro()+"=:"+filVal.getNombreFiltro().replaceAll("\\.", "")+" OR model."+filVal.getNombreFiltro()+" IS NULL ";
				query += ") ";
				if(filtroNoReq.size() == tamReq){				
					order += " model."+filVal.getNombreFiltro();
				}else{
					query += "  and ";
					order += " model."+filVal.getNombreFiltro()+", ";
				}
				parameters.put(filVal.getNombreFiltro().replaceAll("\\.", ""), filVal.getValorFiltro()); 
			}
			query += " ORDER BY " + order;
		}
		return query;
	}
	
}
