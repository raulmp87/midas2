package mx.com.afirme.midas2.dao.impl.reportes.reporteSesas;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.reportes.reporteSesas.GeneracionSesasDao;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.GeneracionSesasDTO;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class GeneracionSesasDaoImpl extends JpaDao<Long, GeneracionSesasDTO> implements GeneracionSesasDao{


	@SuppressWarnings("unchecked")
	@Override
	public List<GeneracionSesasDTO> getListTareas(String claveNeg) throws SQLException, Exception {
		
		List<GeneracionSesasDTO> list = new ArrayList<GeneracionSesasDTO>();
		
		String queryString = "" +
		"" +
		"select model from  GeneracionSesasDTO model where model.claveNeg = " + "'" + claveNeg  + "' and (" +
		"" +
		" model.id in " +
		"(" +
		" select model.id from GeneracionSesasDTO model where" +
			" model.resultado <> 'EXITOSA'" +
		")" +
		"" +
		" or model.id in " +
		" (" +
		" select model.id from GeneracionSesasDTO model where" +
		" model.resultado = 'EXITOSA' " +
		")" +
		")" +			
		" order by model.id desc";

		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list = (List<GeneracionSesasDTO>)query.getResultList();
		
		return list;
	}
	
	@Override
	public GeneracionSesasDTO getLast() throws SQLException, Exception{

		String queryString = "select model from GeneracionSesasDTO model where" +
		" model.id = ( select max(model.id) from GeneracionSesasDTO model )";

		Query query = entityManager.createQuery(queryString);

		return (GeneracionSesasDTO)query.getSingleResult();
	}
}
