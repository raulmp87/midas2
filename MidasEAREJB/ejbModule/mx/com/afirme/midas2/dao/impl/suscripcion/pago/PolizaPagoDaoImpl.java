package mx.com.afirme.midas2.dao.impl.suscripcion.pago;

import java.math.BigDecimal;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.pago.PolizaPagoDao;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.FolioReciboDTO;

@Stateless
public class PolizaPagoDaoImpl extends JpaDao<Long, ExcepcionSuscripcion> implements PolizaPagoDao {

	@Override
	public FolioReciboDTO obtenerInformacionPrimerRecibo() throws Exception{
		FolioReciboDTO recibo = null;
		StoredProcedureHelper storedHelper = null;
		try {
			LogDeMidasInterfaz.log("Obteniendo numero de recibo " + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(
					"seycos.pkg_int_midas_e2.STPOBTIENEFOLIOSEYCOS");

			storedHelper
					.estableceMapeoResultados(
							FolioReciboDTO.class.getCanonicalName(), 
							new String[]{"numRecibo", "digitoVerificador"},
							new String[]{"recibo", "digito"});						
			recibo = (FolioReciboDTO)storedHelper.obtieneResultadoSencillo();
			if(recibo != null){
				LogDeMidasInterfaz.log("Numero de recibo obtenido " + recibo.getFolioConDigito() + ". " + this, Level.INFO, null);
			}					
		}catch (Exception e){	
			LogDeMidasInterfaz.log("Excepcion en BD de obtenerInformacionPrimerRecibo..." + this, Level.WARNING, e);
			throw e;
		}
		return recibo;	
	}

	@Override
	public CuentaPagoDTO obtenerInformacionConductoCobro(
			BigDecimal idToCotizacion, BigDecimal idToSeccion,
			BigDecimal idSeccion) {
		// TODO Auto-generated method stub
		return null;
	}

	

}
