package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.fuerzaventa.ProductoBancarioDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
@Stateless
public class ProductoBancarioDaoImpl extends EntidadHistoricoDaoImpl implements ProductoBancarioDao{
	@Override
	public List<ProductoBancario> findByBanco(Long idBanco) {
		LogDeMidasInterfaz.log("Entrando a findByBanco..." + this, Level.INFO, null);
		ProductoBancario productoBancario=new ProductoBancario();
		BancoDTO banco=new BancoDTO();
		banco.setIdBanco(idBanco.intValue());
		productoBancario.setBanco(banco);
		LogDeMidasInterfaz.log("saliendo a findByBanco..." + this, Level.INFO, null);
		return findByFilters(productoBancario);
	}

	@Override
	public List<ProductoBancario> findByFilters(ProductoBancario filtro) {
		List<ProductoBancario> lista=new ArrayList<ProductoBancario>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ProductoBancario model join fetch model.banco ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){
			if(filtro.getDescripcion()!=null && !filtro.getDescripcion().isEmpty()){
				addCondition(queryString, "model.descripcion like :descripcion");
				params.put("descripcion", filtro.getDescripcion()+"%");
			}
			BancoDTO banco=filtro.getBanco();
			if(banco!=null){
				if(banco.getIdBanco()!=null){
					addCondition(queryString, "model.banco.idBanco=:idBanco");
					params.put("idBanco", banco.getIdBanco());
				}
			}
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		lista=query.getResultList();
		return lista;
	}
	
}
