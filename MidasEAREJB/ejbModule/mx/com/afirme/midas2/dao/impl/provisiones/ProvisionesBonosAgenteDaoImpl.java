package mx.com.afirme.midas2.dao.impl.provisiones;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.provisiones.ProvisionesBonosAgenteDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProvisionesAgentes;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.domain.provisiones.ToAjusteProvision;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ProvisionesBonoAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ProvisionesImportePorRamo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AjusteProvisionDto;
import mx.com.afirme.midas2.service.bonos.ProgramacionBonoService;
import mx.com.afirme.midas2.service.calculos.CalculoBonosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.impl.jobAgentes.ProgramacionCalculoBonosServiceImpl.conceptoEjecucionAutomatica;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCalculoBonosService;
import mx.com.afirme.midas2.service.provisiones.ProvisionesBonosAgenteService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.MidasException;
import mx.com.afirme.midas2.utils.MailServiceSupport;
@Stateless
public class ProvisionesBonosAgenteDaoImpl extends EntidadDaoImpl implements ProvisionesBonosAgenteDao{
	private static final String TRANSACCION_CONTABILIDAD="MOVAGT";
	private ValorCatalogoAgentesService catalogoService;
	private EntidadService entidadService;
	private CalculoBonosService calculoBonoService;
	private ProgramacionBonoService programacionBonoService;
	private ProgramacionCalculoBonosService programacionCalculoBonoService; 
	@EJB
	private MailService mailService;
	@EJB
	private ProvisionesBonosAgenteService provisionesBonosAgenteService;
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	
	@Override
	public ProvisionesAgentes autorizaProvision(ProvisionesAgentes arg0)
			throws Exception {
		return null;
	}
	
	
	/*servicio para insertar el calculo de la provision que se ejecuta con el job*/
	@Override
	public void creaProvisionAutomatica() throws Exception {

		
		List<TareaProgramada> tasks=programacionCalculoBonoService.getTaskToDo(conceptoEjecucionAutomatica.PROVISION.getValue());
		List<ProgramacionBono> bonosProgramados=new ArrayList<ProgramacionBono>();
		Date currentDate=new Date();
		if(!isEmptyList(tasks)){
			for(TareaProgramada task:tasks){
//				if(compareOnlyDatesWithoutTime(currentDate, task.getFechaEjecucion())){
					try{
						Long idProgramacion=task.getId();
						ProgramacionBono programacion=programacionBonoService.loadById(idProgramacion);
						
						if(isNull(programacion)){
							onError("No existe la programacion con clave ["+idProgramacion+"]");
						}
						bonosProgramados.add(programacion);
						
					}catch(Exception e){
						e.printStackTrace();
					}
//				}
			}
			if(!isEmptyList(bonosProgramados)){
				try {
					calculoBonoService.ejecutarCalculosAutomaticos(bonosProgramados,conceptoEjecucionAutomatica.PROVISION.getValue());
				} catch (MidasException e) {
					e.printStackTrace();
				}
			}
		}
		
		// TODO: Falta agregar el movimiento en la configuracion de notificaciones
//		mailService.sendMailToAdminAgentes("GENERACION PREVIEW",
//				MailServiceSupport.mensajeProvisionesGeneracionPreview(),
//				"GENERACION PREVIEW", null);

	}

	/*servicio para imprimir la lista de las provisiones insertadas*/
	@Override
	public List<ProvisionesBonoAgentesView> findByFilterWiew(ProvisionesAgentes filtro) throws Exception {
		List<ProvisionesBonoAgentesView> lista = new ArrayList<ProvisionesBonoAgentesView>();
		Map<Integer, Object> params = new HashMap<Integer, Object>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append(" select  p.id,p.nombreProvision,p.fechaProvision,p.importeProvision,modoej.valor as modoEjecucion,estatus.valor as estatusProvision ");
		queryString.append(" from midas.toprovisionesBonoAgente p ");
		queryString.append(" join midas.tovalorcatalogoagentes estatus on estatus.id = p.claveEstatus ");
		queryString.append(" left join midas.tovalorcatalogoagentes modoej on modoej.id = p.modoEjecucion ");
		queryString.append(" where ");
		
		if(filtro!=null){
			int index=1;
			if(filtro.getId()!=null){
				addCondition(queryString, " p.id=? ");
				params.put(index, filtro.getId());
				index++;
			}
			if(filtro.getClaveEstatus()!=null && filtro.getClaveEstatus().getId()!=null){
				addCondition(queryString, " p.CLAVEESTATUS=? ");
				params.put(index, filtro.getClaveEstatus().getId());
				index++;
			}
			if(filtro.getFechaProvisionInicio()!=null){
				addCondition(queryString, " trunc(p.fechaProvision)>=? ");
				params.put(index, filtro.getFechaProvisionInicio());
				index++;
			}
			if(filtro.getFechaProvisionFin()!=null){
				addCondition(queryString, " trunc(p.fechaProvision)<=? ");
				params.put(index, filtro.getFechaProvisionFin());
				index++;
			}
		}
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		Query query=entityManager.createNativeQuery(getQueryString(queryString),ProvisionesBonoAgentesView.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
	
		lista=query.getResultList();
		return lista;
	}
	
	@Override
	public ProvisionesAgentes loadById(Long idProvision) throws Exception {
		ProvisionesAgentes provision = new ProvisionesAgentes();
		provision=entidadService.findById(ProvisionesAgentes.class, idProvision);
		return provision;
	}
	
	
	/*servicio para obtener los ramos relacionados a la provision que se va a consultar y saber el importe de cada ramo segun los filtros 
	 * que aparecen en el detalle de la provision
	 * */
	@Override
	public List<ProvisionesImportePorRamo> getRamosByFilterProvision(ProvisionImportesRamo filtrosDetalleProvision) throws Exception {
		List<ProvisionesImportePorRamo> listaRamos = new ArrayList<ProvisionesImportePorRamo>();
		Map<Integer,Object> params  = new HashMap<Integer, Object>();
		StringBuilder queryString = new StringBuilder("");
	
		queryString.append(" select rec.idRamo as id,rec.idRamo,calc.idprovision,ramo.descripcionRamo as descripcion,trunc(sum(det.bonoPagar),2) as importe "); 
		queryString.append(" from midas.toProvisionesBonoAgente prov ");
		queryString.append(" inner join midas.tocalculoprovision calc on (prov.id=calc.idProvision) ");
		queryString.append(" inner join midas.todetallecalculoprovision det on(calc.id = det.idCalculo) ");
		queryString.append(" inner join midas.torecibocalculoprovision rec on(det.id=rec.iddetallecalculobonos) ");
		queryString.append(" inner join midas.tocalculoprov_ejecuciones ejec on (ejec.idcalculobono=calc.id) ");
		queryString.append(" left join MIDAS.tcramo ramo on rec.idRamo = ramo.codigoRamo ");
		queryString.append(" inner join midas.vw_AgenteInfo fv on fv.idagente = rec.idagente ");
		if(filtrosDetalleProvision.getIdLineaVenta()!=null){
			queryString.append(" inner join midas.toValorCatalogoAgentes lineavta on lineavta.clave = rec.TIPOLINEAVENTA and lineavta.grupocatalogoagentes_id = (select id from midas.tcGrupoCatalogoAgentes where descripcion ='Lineas de Venta') ");
		}
		queryString.append(" where ");
		
		if(filtrosDetalleProvision!=null){
			int index=1;
			if(filtrosDetalleProvision.getProvisionAgentes()!=null && filtrosDetalleProvision.getProvisionAgentes().getId()!=null){
				addCondition(queryString, " prov.id=? ");
				params.put(index, filtrosDetalleProvision.getProvisionAgentes().getId());
				index++;
			}
			
			if(filtrosDetalleProvision.getConfigBono()!=null && filtrosDetalleProvision.getConfigBono().getId()!=null){
				addCondition(queryString, " ejec.idConfiguracion=? ");
				params.put(index, filtrosDetalleProvision.getConfigBono().getId());
				index++;
			}	
			if(filtrosDetalleProvision.getIdGerencia()!=null){
				addCondition(queryString, " fv.idgerencia=? ");
				params.put(index, filtrosDetalleProvision.getIdGerencia());
				index++;
			}		
			if(filtrosDetalleProvision.getEjecutivo()!=null && filtrosDetalleProvision.getEjecutivo().getId()!=null){
				addCondition(queryString, " fv.idejecutivo=? ");
				params.put(index, filtrosDetalleProvision.getEjecutivo().getId());
				index++;
			}
			if(filtrosDetalleProvision.getPromotoria()!=null && filtrosDetalleProvision.getPromotoria().getId()!=null){
				addCondition(queryString, " fv.idpromotoria=? ");
				params.put(index, filtrosDetalleProvision.getPromotoria().getId());
				index++;
			}
			if(filtrosDetalleProvision.getAgenteOrigen()!=null && filtrosDetalleProvision.getAgenteOrigen().getIdAgente()!=null){
				addCondition(queryString, " det.idBeneficiario=?");
				params.put(index, filtrosDetalleProvision.getAgenteOrigen().getIdAgente());
				index++;
			}
			if(filtrosDetalleProvision.getIdLineaVenta()!=null){
				addCondition(queryString, " lineavta.id=? ");
				params.put(index, filtrosDetalleProvision.getIdLineaVenta());
				index++;
			}	
			if(filtrosDetalleProvision.getProducto()!=null && filtrosDetalleProvision.getProducto().getId()!=null){
				addCondition(queryString, " rec.idproducto=? ");
				params.put(index, filtrosDetalleProvision.getProducto().getId());
				index++;
			}
			if(filtrosDetalleProvision.getIdLineaNegocio()!=null){
				addCondition(queryString, " rec.idlineanegocio=? ");
				params.put(index, filtrosDetalleProvision.getIdLineaNegocio());
				index++;
			}
		}
		
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
				
		Query query=entityManager.createNativeQuery(getQueryString(queryString)+" group by rec.idRamo,ramo.descripcionRamo,calc.idprovision order by ramo.descripcionRamo asc ",ProvisionesImportePorRamo.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
	
		listaRamos=query.getResultList();
		return listaRamos;
	}

	/*servicio para insertar o actualizar el importe del ramo**/
	@Override
	public ProvisionImportesRamo updateImporteRamo(ProvisionImportesRamo ramo)	throws Exception {
		ProvisionImportesRamo provisionRamo = new ProvisionImportesRamo();
//		if(ramo!=null){
//			provisionRamo = entidadService.save(ramo);
//		}
		if(ramo.getId()!=null){
			update(ramo);
		}else{
			persist(ramo);
		}	
		return provisionRamo;
	}

	@Override
	public List<ProvisionImportesRamo> updateImporteXRamo(List<ProvisionImportesRamo> ramos,ProvisionImportesRamo filtrosDetalleProvision) throws Exception {
		
		if(ramos!=null){
			for(ProvisionImportesRamo ramo : ramos){
				
				if(filtrosDetalleProvision.getProvisionAgentes()!=null && filtrosDetalleProvision.getProvisionAgentes().getId()!=null){
					ramo.setProvisionAgentes(filtrosDetalleProvision.getProvisionAgentes());
				}
				
				ConfigBonos configBono  = new ConfigBonos();
				if(filtrosDetalleProvision.getConfigBono()!=null && filtrosDetalleProvision.getConfigBono().getId()!=null){
					configBono = entidadService.findById(ConfigBonos.class,filtrosDetalleProvision.getConfigBono().getId());
					ramo.setConfigBono(configBono);
				}else{
					configBono=null;
					ramo.setConfigBono(null);
				}
				
				ProductoDTO producto = new ProductoDTO();
				if(filtrosDetalleProvision.getProducto()!=null && filtrosDetalleProvision.getProducto().getId()!=null){
					producto = entidadService.findById(ProductoDTO.class,filtrosDetalleProvision.getProducto().getId());
					ramo.setProducto(producto);
				}else{
					producto =null;
					ramo.setProducto(null);
				}
				
				Ejecutivo ejecutivo = new Ejecutivo();
				if(filtrosDetalleProvision.getEjecutivo()!=null && filtrosDetalleProvision.getEjecutivo().getId()!=null){
					 ejecutivo = entidadService.findById(Ejecutivo.class,filtrosDetalleProvision.getEjecutivo().getId());
					 ramo.setEjecutivo(ejecutivo);
				}else{
					ejecutivo=null;
					ramo.setEjecutivo(null);
				}
				
				Promotoria promotoria = new Promotoria();
				if(filtrosDetalleProvision.getPromotoria()!=null && filtrosDetalleProvision.getPromotoria().getId()!=null){
					promotoria = entidadService.findById(Promotoria.class,filtrosDetalleProvision.getPromotoria().getId());
					ramo.setPromotoria(promotoria);
				}else{
					promotoria=null;
					ramo.setPromotoria(null);
				}
								
//				ProvisionImportesRamo findProvisionImporteRamo = entidadService.findById(ProvisionImportesRamo.class,ramo.getId());
//			
				ProvisionImportesRamo existe = existeRegistroImporteRamo(ramo);
				ProvisionImportesRamo nuevoRamo = new ProvisionImportesRamo();
				
				if(existe!=null && existe.getId()!=null){
					nuevoRamo.setId(existe.getId());
				}else{
					nuevoRamo.setId(null);
				}
				
				nuevoRamo.setProvisionAgentes(ramo.getProvisionAgentes());
				nuevoRamo.setIdRamo(ramo.getIdRamo());
				nuevoRamo.setImporte(ramo.getImporte());
				nuevoRamo.setConfigBono(configBono);
				nuevoRamo.setIdLineaVenta(filtrosDetalleProvision.getIdLineaVenta());
				nuevoRamo.setProducto(producto);
				nuevoRamo.setIdLineaNegocio(filtrosDetalleProvision.getIdLineaNegocio());
				nuevoRamo.setIdCobertura(filtrosDetalleProvision.getIdCobertura());
				nuevoRamo.setIdPoliza(filtrosDetalleProvision.getIdPoliza());
				nuevoRamo.setIdCentroOperacion(filtrosDetalleProvision.getIdCentroOperacion());
				nuevoRamo.setIdGerencia(filtrosDetalleProvision.getIdGerencia());
				nuevoRamo.setEjecutivo(ejecutivo);
				nuevoRamo.setPromotoria(promotoria);
				
				updateImporteRamo(nuevoRamo);				
			}
		}
		return ramos;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
	
	
	@Override
	public List<GenericaAgentesView> creaFiltroAgente(Long idProvision) throws Exception {
		
		return null;
	}

	@Override
	public List<GenericaAgentesView> creaFiltroBonos(Long idProvision) throws Exception {
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		queryString.append("select ejec.idConfiguracion as id,ejec.nombreConfiguracionBono as valor from midas.tocalculoprovision calc ");
		queryString.append(" inner join midas.tocalculoprov_ejecuciones ejec on(ejec.idcalculoBono=calc.id)");
		queryString.append(" where calc.idprovision= "+idProvision);
		queryString.append(" order by ejec.nombreConfiguracionBono asc ");		   
		Query query=entityManager.createNativeQuery(getQueryString(queryString),GenericaAgentesView.class);
		lista = query.getResultList();
		return lista;
	}

	@Override
	public List<GenericaAgentesView> creaFiltroEjecutivo(Long idProvision) throws Exception {
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		queryString.append(" select distinct ejec.id as id,per.nombre as valor");
		queryString.append(" from  midas.toejecutivo ejec  ");
		queryString.append(" inner join midas.topromotoria prom on ejec.id=prom.ejecutivo_id ");
		queryString.append(" inner join seycos.persona per on per.id_persona=ejec.idPersona ");
		queryString.append(" where prom.id in( ");
		queryString.append(" select distinct rec.idPromotoria  ");
		queryString.append(" from midas.tocalculoprovision  prov ");
		queryString.append(" inner join midas.todetallecalculoprovision det on det.idcalculo = prov.id ");
		queryString.append(" inner join midas.torecibocalculoprovision rec on rec.iddetallecalculobonos = det.id ");
		queryString.append(" where prov.idprovision = "+idProvision);
		queryString.append(" ) order by per.nombre asc ");
		Query query=entityManager.createNativeQuery(getQueryString(queryString),GenericaAgentesView.class);
		lista = query.getResultList();
		return lista;
	}

	@Override
	public List<GenericaAgentesView> creaFiltroGerencia(Long idProvision)	throws Exception {		
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		queryString.append(" select distinct ger.id as id, ger.descripcion as valor"); 
		queryString.append(" from midas.togerencia ger ");
		queryString.append(" inner join midas.toejecutivo ejec on ger.id=ejec.gerencia_id ");
		queryString.append(" inner join midas.topromotoria prom on ejec.id=prom.ejecutivo_id ");
		queryString.append(" where prom.id in( ");
		queryString.append(" select distinct rec.idPromotoria  ");
		queryString.append(" from midas.tocalculoprovision  prov ");
		queryString.append(" inner join midas.todetallecalculoprovision det on det.idcalculo = prov.id ");
		queryString.append(" inner join midas.torecibocalculoprovision rec on rec.iddetallecalculobonos = det.id ");
		queryString.append(" where prov.idprovision = "+idProvision);
		queryString.append(" ) order by ger.descripcion asc ");
		Query query=entityManager.createNativeQuery(getQueryString(queryString),GenericaAgentesView.class);
		lista = query.getResultList();
		return lista;
	}

	@Override
	public List<GenericaAgentesView> creaFiltroGerente(Long idProvision)
			throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<GenericaAgentesView> creaFiltroLineaNegocio(Long idProvision)
			throws Exception {
		StringBuilder queryString = new StringBuilder("");
		queryString.append(" select  distinct sec.idtoseccion as id, ");
		queryString.append(" 'LN'||TRIM(SEC.CODIGOSECCION)||' - '||sec.NOMBRECOMERCIALSECCION||' - (P'||max(trim(p.CODIGOPRODUCTO))||', R'||max(trim(R.CODIGORAMO))||')' as valor, ");  
		queryString.append(" sec.NOMBRECOMERCIALSECCION ");
		queryString.append(" from MIDAS.TOSECCION sec ");
		queryString.append(" inner join MIDAS.TOTIPOPOLIZA tp on(tp.IDTOTIPOPOLIZA=sec.IDTOTIPOPOLIZA) ");  
		queryString.append(" inner join MIDAS.TOPRODUCTO p on(P.IDTOPRODUCTO =TP.IDTOPRODUCTO) ");
		queryString.append(" inner join MIDAS.TRRAMOTIPOPOLIZA rtp on (RTP.IDTOTIPOPOLIZA = TP.IDTOTIPOPOLIZA ) ");  
		queryString.append(" inner join MIDAS.TRRAMOSECCION rs on (RS.IDTOSECCION = SEC.IDTOSECCION  ) ");
		queryString.append(" inner join MIDAS.TCRAMO r on (R.IDTCRAMO = RS.IDTCRAMO and RTP.IDTCRAMO = R.IDTCRAMO ) ");  
		queryString.append(" where  sec.claveActivo=1 ");
		queryString.append(" and sec.idtoseccion in(select distinct rec.idlineanegocio from "); 
		queryString.append(" midas.toProvisionesBonoAgente prov ");
		queryString.append(" inner join midas.tocalculoprovision calc on (prov.id=calc.idProvision) ");  
		queryString.append(" inner join midas.todetallecalculoprovision det on(calc.id = det.idCalculo) ");  
		queryString.append(" inner join midas.torecibocalculoprovision rec on(det.id=rec.iddetallecalculobonos) ");
		queryString.append(" where prov.ID="+idProvision+" and IDLINEANEGOCIO is not null) ");
		queryString.append(" group by sec.idtoseccion, SEC.CODIGOSECCION, sec.NOMBRECOMERCIALSECCION ");  
		queryString.append(" order by  sec.NOMBRECOMERCIALSECCION ");
//		tp.idtoproducto in(222)
		return this.entityManager.createNativeQuery(queryString.toString(), GenericaAgentesView.class).getResultList();
	}

	@Override
	public List<GenericaAgentesView> creaFiltroLineaVenta(Long idProvision)
			throws Exception {
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select valor as valor,id as id, clave ");
		queryString.append(" from midas.toValorCatalogoAgentes ");
		queryString.append(" where ");
		queryString.append(" grupocatalogoagentes_id = (select id from midas.tcGrupoCatalogoAgentes where descripcion ='Lineas de Venta') ");
		queryString.append(" and clave in ");
		queryString.append(" (select distinct rec.tipolineaventa from  ");
		queryString.append("  midas.toProvisionesBonoAgente prov  ");
		queryString.append(" inner join midas.tocalculoprovision calc on (prov.id=calc.idProvision) ");
		queryString.append(" inner join midas.todetallecalculoprovision det on(calc.id = det.idCalculo) ");
		queryString.append(" inner join midas.torecibocalculoprovision rec on(det.id=rec.iddetallecalculobonos) ");
		queryString.append(" where prov.ID= "+idProvision+" ) ");
		return this.entityManager.createNativeQuery(queryString.toString(), GenericaAgentesView.class).getResultList();
	}

	@Override
	public List<GenericaAgentesView> creaFiltroProducto(Long idProvision)
			throws Exception {
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select idToProducto AS id,'P'||TRIM(CODIGOPRODUCTO)||' - '||descripcionProducto AS valor "); 
		queryString.append(" from MIDAS.toproducto "); 
//		queryString.append(" where claveNegocio in('A') "); 
		queryString.append(" where CLAVEACTIVO = 1 "); 
		queryString.append(" and idToProducto in(select distinct rec.idproducto from "); 
		queryString.append(" midas.toProvisionesBonoAgente prov ");  
		queryString.append(" inner join midas.tocalculoprovision calc on (prov.id=calc.idProvision) ");  
		queryString.append(" inner join midas.todetallecalculoprovision det on(calc.id = det.idCalculo) ");  
		queryString.append(" inner join midas.torecibocalculoprovision rec on(det.id=rec.iddetallecalculobonos) ");
		queryString.append(" where prov.ID="+ idProvision +" ) ");
		queryString.append(" order by descripcionProducto ");
		return this.entityManager.createNativeQuery(queryString.toString(), GenericaAgentesView.class).getResultList();
	}

	@Override
	public List<GenericaAgentesView> creaFiltroPromotoria(Long idProvision) throws Exception {		
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");

		queryString.append(" select prom.id as id, prom.descripcion as valor "); 
		queryString.append(" from midas.topromotoria prom ");
		queryString.append(" where prom.id in( ");
		queryString.append(" select distinct rec.idPromotoria  ");
		queryString.append(" from midas.tocalculoprovision  prov ");
		queryString.append(" inner join midas.todetallecalculoprovision det on det.idcalculo = prov.id ");
		queryString.append(" inner join midas.torecibocalculoprovision rec on rec.iddetallecalculobonos = det.id ");
		queryString.append(" where prov.idprovision = "+idProvision+" ) ");
		queryString.append(" order by prom.descripcion asc ");
		
		Query query=entityManager.createNativeQuery(getQueryString(queryString),GenericaAgentesView.class);
		lista = query.getResultList();
		return lista;
	}

	@Override
	public List<ProvisionesImportePorRamo> importeTotalPorRamo(Long idProvision) throws Exception {
		List<ProvisionesImportePorRamo> lista = new ArrayList<ProvisionesImportePorRamo>();
		
		StringBuilder queryString = new StringBuilder("");
		queryString.append(" select rec.idRamo as id,rec.idRamo,calc.idprovision,ramo.descripcionRamo as descripcion,trunc(sum(det.bonoPagar),2) as importe "); 
		queryString.append(" from midas.toProvisionesBonoAgente prov ");
		queryString.append(" inner join midas.tocalculoprovision calc on (prov.id=calc.idProvision) ");
		queryString.append(" inner join midas.todetallecalculoprovision det on(calc.id = det.idCalculo) ");
		queryString.append(" inner join midas.torecibocalculoprovision rec on(det.id=rec.iddetallecalculobonos) ");
		queryString.append(" inner join midas.tocalculoprov_ejecuciones ejec on (ejec.idcalculobono=calc.id) ");
		queryString.append(" left join MIDAS.tcramo ramo on rec.idRamo = ramo.codigoRamo ");	
		queryString.append(" where prov.id=" + idProvision );
		queryString.append(" group by rec.idRamo,ramo.descripcionRamo,calc.idprovision order by ramo.descripcionRamo asc ");
		Query query=entityManager.createNativeQuery(getQueryString(queryString),ProvisionesImportePorRamo.class);		
		lista = query.getResultList();
		
		return lista;
	}
	/**
	 * Contabiliza en las cuentas correspondientes los conceptos o rubros de provisiones, por medio del id de la provision.
	 * @param idProvision
	 */
	private void contabilizarProviciones(Long idProvision) throws MidasException{
		if(isNull(idProvision)){
			onError("Favor de proporcionar la clave de la provision a contabilizar.");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS.STPCONTABILIZA_MOVS";
		try {
			LogDeMidasInterfaz.log("Entrando a ProvisionesBonosAgenteDaoImpl.contabilizarProviciones... params:[pidentificador:"+idProvision+"|pCve_trans_cont:"+TRANSACCION_CONTABILIDAD+"]" + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pidentificador", idProvision);
			storedHelper.estableceParametro("pCve_trans_cont", TRANSACCION_CONTABILIDAD);
			storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz.log("Saliendo de ProvisionesBonosAgenteDaoImpl.contabilizarProviciones..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog("midas",StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, ProvisionesBonosAgenteDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ProvisionesBonosAgenteDaoImpl.contabilizarProviciones..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ProvisionesBonosAgenteDaoImpl.contabilizarProviciones..." + this, Level.WARNING, e);
			onError(e);
		}
	}
	
	@Override
	public ProvisionesAgentes autorizarProvision(Long idProvision) throws Exception {
		ProvisionesAgentes provision = null;
		try {
			provision = new ProvisionesAgentes();
			ValorCatalogoAgentes estatus=catalogoService.obtenerElementoEspecifico("Estatus de Provision de Bonos (Agentes)", "AUTORIZADA");
			provision = entidadService.findById(ProvisionesAgentes.class, idProvision);
//			provision.setClaveEstatus(estatus);
			entidadService.save(provision);
			//[BEGIN#07-01-2012-VMHS: Se agrega contabilidad en provisiones]
			contabilizarProviciones(idProvision);
			//[END#07-01-2012-VMHS: Se agrega contabilidad en provisiones]
			// Genera el reporte de provisiones
			ProvisionImportesRamo filtro = new ProvisionImportesRamo();
			filtro.setProvisionAgentes(new ProvisionesAgentes());
			filtro.getProvisionAgentes().setId(idProvision);
			List<ByteArrayAttachment> attachment = null;
			TransporteImpresionDTO reporte = generarPlantillaReporte
					.imprimirReporteProvision(
							"" + Calendar.getInstance().get(Calendar.YEAR),
							""
									+ (Calendar.getInstance().get(
											Calendar.MONTH) + 1), filtro, TipoSalidaReportes.TO_EXCEL.getValue());
			if (reporte != null && reporte.getByteArray() != null) {
				ByteArrayAttachment file = new ByteArrayAttachment();
				file.setNombreArchivo("reporteProvisiones.xls");
				file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
				attachment = new ArrayList<ByteArrayAttachment>();
				attachment.add(file);
			}
			Map<String, Map<String, List<String>>> mapCorreos = provisionesBonosAgenteService
					.obtenerCorreos(
							null,
							GenericMailService.P_PROVISION,
							GenericMailService.M_PROVISION_AUTORIZACION_DEL_MOVIMIENTO);
			provisionesBonosAgenteService.enviarCorreo(mapCorreos,
					MailServiceSupport.mensajeProvisionesAutorizacionMov(),
					"PROVISIONES AUTORIZACION MOVIMIENTO",
					GenericMailService.T_GENERAL, attachment);
		} catch(RuntimeException exception) {
			exception.printStackTrace();
		}
		return provision;
	}

	
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	
	@EJB
	public void setCalculoBonoService(CalculoBonosService calculoBonoService) {
		this.calculoBonoService = calculoBonoService;
	}
	
	@EJB
	public void setProgramacionBonoService(ProgramacionBonoService programacionBonoService) {
		this.programacionBonoService = programacionBonoService;
	}

	@EJB
	public void setProgramacionCalculoBonoService(
			ProgramacionCalculoBonosService programacionCalculoBonoService) {
		this.programacionCalculoBonoService = programacionCalculoBonoService;
	}


	@Override
	public ProvisionImportesRamo existeRegistroImporteRamo(ProvisionImportesRamo filtrosDetalleProvision) throws Exception {
		List<ProvisionImportesRamo> lista = new ArrayList<ProvisionImportesRamo>();
		Map<Integer,Object> params  = new HashMap<Integer, Object>();
		StringBuilder queryString = new StringBuilder("");
		
//		ProvisionImportesRamo registro = entidadService.findById(ProvisionImportesRamo.class, filtrosDetalleProvision.getId());
	
		queryString.append(" select id from MIDAS.TOPROVISIONIMPORTESRAMO ");
		queryString.append(" where ");
		if(filtrosDetalleProvision!=null){
			int index=1;	
			if(filtrosDetalleProvision.getProvisionAgentes()!=null && filtrosDetalleProvision.getProvisionAgentes().getId()!=null){
				addCondition(queryString, " idProvision=? ");
				params.put(index, filtrosDetalleProvision.getProvisionAgentes().getId());
				index++;
			}else{
				addCondition(queryString, "idProvision is null ");
			}	
			if(filtrosDetalleProvision.getIdRamo()!=null){
				addCondition(queryString, " idRamo=? ");
				params.put(index, filtrosDetalleProvision.getIdRamo());
				index++;
			}else{
				addCondition(queryString, "idRamo is null ");
			}
			if(filtrosDetalleProvision.getConfigBono()!=null && filtrosDetalleProvision.getConfigBono().getId()!=null){
				addCondition(queryString, " idBono=? ");
				params.put(index, filtrosDetalleProvision.getConfigBono().getId());
				index++;
			}else{
				addCondition(queryString, " idBono is null ");
			}		
			//FIXME falta canal de venta
			if(filtrosDetalleProvision.getIdGerencia()!=null){
				addCondition(queryString, " idGerencia=? ");
				params.put(index, filtrosDetalleProvision.getIdGerencia());
				index++;
			}else{
				addCondition(queryString, " idGerencia is null ");
			}		
			//FIXME falta gerente
			if(filtrosDetalleProvision.getEjecutivo()!=null && filtrosDetalleProvision.getEjecutivo().getId()!=null){
				addCondition(queryString, " idEjecutivo=? ");
				params.put(index, filtrosDetalleProvision.getEjecutivo().getId());
				index++;
			}else{
				addCondition(queryString, " idEjecutivo is null ");
			}
			if(filtrosDetalleProvision.getPromotoria()!=null && filtrosDetalleProvision.getPromotoria().getId()!=null){
				addCondition(queryString, " idPromotoria=? ");
				params.put(index, filtrosDetalleProvision.getPromotoria().getId());
				index++;
			}else{
				addCondition(queryString, " idPromotoria is null ");
			}		
			//FIXME falta agente
			if(filtrosDetalleProvision.getIdLineaVenta()!=null){
				addCondition(queryString, " idLineaVenta=? ");
				params.put(index, filtrosDetalleProvision.getIdLineaVenta());
				index++;
			}else{
				addCondition(queryString, " idLineaVenta is null ");
			}	
			if(filtrosDetalleProvision.getProducto()!=null && filtrosDetalleProvision.getProducto().getId()!=null){
				addCondition(queryString, " idProducto=? ");
				params.put(index, filtrosDetalleProvision.getProducto().getId());
				index++;
			}else{
				addCondition(queryString, " idProducto is null ");
			}	
			if(filtrosDetalleProvision.getIdLineaNegocio()!=null){
				addCondition(queryString, " idLineaNegocio=? ");
				params.put(index, filtrosDetalleProvision.getIdLineaNegocio());
				index++;
			}else{
				addCondition(queryString, " idLineaNegocio is null ");
			}
			if(filtrosDetalleProvision.getIdCobertura()!=null){
				addCondition(queryString, " idCobertura=? ");
				params.put(index, filtrosDetalleProvision.getIdCobertura());
				index++;
			}else{
				addCondition(queryString, " idCobertura is null ");
			}
			if(filtrosDetalleProvision.getIdPoliza()!=null){
				addCondition(queryString, " idPoliza=? ");
				params.put(index, filtrosDetalleProvision.getIdPoliza());
				index++;
			}else{
				addCondition(queryString, " idPoliza is null ");
			}
			if(filtrosDetalleProvision.getIdCentroOperacion()!=null){
				addCondition(queryString, " idCentroOperacion=? ");
				params.put(index, filtrosDetalleProvision.getIdCentroOperacion());
				index++;
			}else{
				addCondition(queryString, " idCentroOperacion is null ");
			}
			if(filtrosDetalleProvision.getAgenteOrigen()!=null){
				addCondition(queryString, " idAgenteOrigen =? ");
				params.put(index, filtrosDetalleProvision.getAgenteOrigen());
				index++;
			}else{
				addCondition(queryString, " idAgenteOrigen  is null ");
			}
			if(filtrosDetalleProvision.getAgenteFin()!=null){
				addCondition(queryString, " idAgenteFin =? ");
				params.put(index, filtrosDetalleProvision.getAgenteFin());
				index++;
			}else{
				addCondition(queryString, " idAgenteFin  is null ");
			}
			if(filtrosDetalleProvision.getTipoPersona()!=null && filtrosDetalleProvision.getTipoPersona().getId()!=null){
				addCondition(queryString, " tipoPersona =? ");
				params.put(index, filtrosDetalleProvision.getTipoPersona().getId());
				index++;
			}else{
				addCondition(queryString, " tipoPersona  is null ");
			}
			
		}
		
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
				
		Query query=entityManager.createNativeQuery(getQueryString(queryString),ProvisionImportesRamo.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
	
		lista = query.getResultList();
		if(!isEmptyList(lista)){
//			Long id = lista.get(0).getId();
//			ProvisionImportesRamo nuevo =new ProvisionImportesRamo();
//			nuevo.setId(id);
			return lista.get(0);
		}else{
			return null;
		}
	
	}
	@Override
	public AjusteProvisionDto listAjusteProvision(Long idProvision)
			throws Exception {
		if(idProvision==null){
			throw new Exception("Favor de Proprcionar un id de Provisión");
		}
		StringBuilder queryString = new StringBuilder(); 
		queryString.append(" select  1 as id ");
		queryString.append(" ,max(case when idramo = 1 then importe end) incendioPuro ");
		queryString.append(" ,max(case when idramo = 2 then importe end) maritimoTransportes ");
		queryString.append(" ,max(case when idramo = 3 then importe end) diversos ");
		queryString.append(" ,max(case when idramo = 4 then importe end) automoviles ");
		queryString.append(" ,max(case when idramo = 5 then importe end) responsabilidadCivilGeneral ");
		queryString.append(" ,max(case when idramo = 6 then importe end) catastroficos ");
		queryString.append(" ,max(case when idramo = 7 then importe end) vidaGrupoColectivo ");
		queryString.append(" ,max(case when idramo = 8 then importe end) vidaInd ");
		queryString.append(" from toajusteprovision ");
		queryString.append(" where idProvision = 2 ");
		AjusteProvisionDto obj = new AjusteProvisionDto();
		obj = (AjusteProvisionDto) this.entityManager.createNativeQuery(queryString.toString(), AjusteProvisionDto.class).getSingleResult();
		
		return obj;
	}

	@Override
	public String saveAjusteProvision(List<ToAjusteProvision> listaAjusteProvision)
			throws Exception {
		
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("provision.id", listaAjusteProvision.get(0).getProvision().getId());
		List<ToAjusteProvision>  existeAjusteProvision = this.entidadService.findByProperties(ToAjusteProvision.class, param);
		String resultado ="";
		try{
			if(!existeAjusteProvision.isEmpty() && existeAjusteProvision.size()>=1){
				for(ToAjusteProvision obj:existeAjusteProvision){
					this.entidadService.remove(obj);
				}
			}
			if(!listaAjusteProvision.isEmpty()&& listaAjusteProvision.size()>=1){
				for(ToAjusteProvision obj:listaAjusteProvision){
					this.entidadService.save(obj);
				}
			}
			
			 resultado ="SUCCESS";
		}catch (Exception e) {
			resultado = "INPUT";
		}
		
		return resultado;
	}
	
	
//	@Override
//	public List<GenericaAgentesView> listaDeRamos() throws Exception {
//		List<GenericaAgentesView> listRamos = new ArrayList<GenericaAgentesView>();
//		StringBuilder queryString = new StringBuilder("");
//		queryString.append("select codigoRamo as id,descripcionRamo as valor from midas.tcramo ");
//		Query query = entityManager.createNativeQuery(queryString.toString(),GenericaAgentesView.class);
//		listRamos=query.getResultList();		
//		return listRamos;
//	}
}
