package mx.com.afirme.midas2.dao.impl.negocio.producto;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioProductoDaoImpl extends JpaDao<Long, NegocioProducto> implements NegocioProductoDao {

	@SuppressWarnings("unchecked")
	public List<NegocioTipoPoliza> listarNegTipoPolizaPorIdToNegProducto(Long idToNegProducto){
		LogDeMidasEJB3.log("finding all NegocioTipoPoliza instances by idNegProducto " + idToNegProducto, Level.INFO, null);
		try {
			String queryString = "select model from NegocioTipoPoliza model where " + 
				" model.negocioProducto.idToNegProducto = :idToNegProducto order by model.idToNegTipoPoliza";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegProducto", idToNegProducto);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<NegocioProducto> listarNegocioProductoPorNegocio(Long idToNegocio, Integer activo){
		LogDeMidasEJB3.log("finding all NegocioProducto instances by idToNegocio " + idToNegocio, Level.INFO, null);
		try {
			String queryString = "select model from NegocioProducto model where model.negocio.idToNegocio = :idToNegocio ";
			if(activo != null){		
				if(activo.equals(new Integer(1))){
					queryString  = queryString  + " and model.productoDTO.claveEstatus IN (2,1)";
				}else{
					queryString  = queryString  + " and model.productoDTO.claveEstatus IN (1,2,3)";
				}
			}
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegocio", idToNegocio);
			
			//TODO: ver como mejorar esta comprobacion.
			//if(activo != null){
			//	query.setParameter("cveActivo", activo);
			//}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	
	@Override
	public NegocioProducto getNegocioProductoByIdNegocioIdProducto(Long idToNegocio, BigDecimal idToProducto){
		LogDeMidasEJB3.log("finding NegocioProducto instances by idToNegocio idToProducto" + idToNegocio, Level.INFO, null);
		try {
			String queryString = "select model from NegocioProducto model where model.negocio.idToNegocio = :idToNegocio ";
			queryString  = queryString  + " and model.productoDTO.idToProducto = :idToProducto";			
			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegocio", idToNegocio);
			query.setParameter("idToProducto", idToProducto);			
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return (NegocioProducto) query.getSingleResult();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<NegocioProducto> listarNegocioProductoUsablesPorNegocio(Long idToNegocio){
		LogDeMidasEJB3.log("finding all NegocioProducto instances by idToNegocio " + idToNegocio, Level.INFO, null);
		try {
			Query query = null;
			if(idToNegocio != null && idToNegocio > 0) {//JFGG.20180327
				String queryString = "select model from NegocioProducto model where model.negocio.idToNegocio = :idToNegocio ";
				queryString  += " and model.productoDTO.claveEstatus IN (1,2)";
				
				query = entityManager.createQuery(queryString);
				query.setParameter("idToNegocio", idToNegocio);
				
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			} else {//JFGG.20180327
				String queryString = "select model from NegocioProducto model where  model.productoDTO.claveEstatus IN (1,2)";				
				query = entityManager.createQuery(queryString);
				
				query.setHint(QueryHints.REFRESH, HintValues.FALSE); 
			}//JFGG.20180327
			
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
}