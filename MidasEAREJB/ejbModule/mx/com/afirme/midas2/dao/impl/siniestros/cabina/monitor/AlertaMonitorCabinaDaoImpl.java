package mx.com.afirme.midas2.dao.impl.siniestros.cabina.monitor;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.cabina.monitor.AlertaMonitorCabinaDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.monitor.AlertaMonitorCabina;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;

@Stateless
public class AlertaMonitorCabinaDaoImpl extends
		JpaDao<Long, AlertaMonitorCabina> implements AlertaMonitorCabinaDao {

	@EJB
	private EntidadService entidadService;

	@EJB
	private EnvioNotificacionesService notifService;

	@Override
	public List<AlertaMonitorCabina> obtenerAlertas() {
		List<AlertaMonitorCabina> resultList = findAll();
		notifService.enviarNotificacion("", null);
		return resultList;
	}

	@Override
	public AlertaMonitorCabina obtenerAlerta(Long alertaId) {
		return entidadService.findById(AlertaMonitorCabina.class, alertaId);
	}

	@Override
	public List<AlertaMonitorCabina> obtenerAlertasPorFiltros(
			AlertaMonitorCabina alertaMonitorCabina) {
		// ver: CatalogoSiniestroDaoImpl
		return super.findByFilterObject(AlertaMonitorCabina.class,
				alertaMonitorCabina);
	}

	@Override
	public List<AlertaMonitorCabina> obtenerAlertasPorRol(String rol) {
		List<AlertaMonitorCabina> alertasARetornar = new ArrayList<AlertaMonitorCabina>();
		alertasARetornar = findByProperty("rol", rol);
		return alertasARetornar;
	}
}
