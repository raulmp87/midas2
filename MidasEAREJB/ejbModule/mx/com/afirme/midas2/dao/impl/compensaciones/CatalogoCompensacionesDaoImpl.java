package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.math.BigDecimal;
import java.util.List;
import java.util.ArrayList;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.compensaciones.CatalogoCompensacionesDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;
import mx.com.afirme.midas2.domain.compensaciones.Proveedor;
import mx.com.afirme.midas2.domain.negocio.Negocio;

@Stateless
public class CatalogoCompensacionesDaoImpl implements CatalogoCompensacionesDao {
	
	private static final Logger LOG = LoggerFactory
			.getLogger(CatalogoCompensacionesDaoImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CaBaseCalculo> listarBaseCalculo() {
		List<CaBaseCalculo> listaBaseCalculo = new ArrayList<CaBaseCalculo>();
		String queryString = "";
		try{
			queryString = "select bc from CaBaseCalculo bc order by bc.nombre";
			Query query = entityManager.createQuery(queryString, CaBaseCalculo.class);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			listaBaseCalculo = query.getResultList();
		}catch(RuntimeException re){
			
		}
		return listaBaseCalculo;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaRamo> listarRamos() {
		List<CaRamo> listaRamos = new ArrayList<CaRamo>();
		String queryString = "";
		try{
			queryString = "select r from CaRamo r order by r.nombre";
			Query query = entityManager.createQuery(queryString, CaRamo.class);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			listaRamos = query.getResultList();
		}catch(RuntimeException re){
			
		}
		return listaRamos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CaTipoCompensacion> listarTipoCompensacion() {
		List<CaTipoCompensacion> listaTipoCompensacion = new ArrayList<CaTipoCompensacion>();
		String queryString = "";
		try{
			queryString = "select tc from CaTipoCompensacion tc order by tc.nombre";
			Query query = entityManager.createQuery(queryString, CaTipoCompensacion.class);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			listaTipoCompensacion = query.getResultList();
		}catch(RuntimeException re){
			
		}
		return listaTipoCompensacion;
	}	

	public List<Proveedor> listarProveedores(String nombreProveedor){
		List<Proveedor> listaProveedores = null;
		nombreProveedor = nombreProveedor.trim().toUpperCase();
		String queryString = "";
		try{
			//queryString = "select p from Proveedor p where p.nombre like :pnombre order by p.nombre ";
			queryString = "SELECT P.ID, PMM.NOMBRE_RAZON_SOCIAL NOMBRE FROM MIDAS.TCPRESTADORSERVICIO P"
						+ " INNER JOIN MIDAS.PERSONA_MIDAS PM ON PM.ID = P.PERSONA_ID"
						+ " INNER JOIN MIDAS.PERSONA_MORAL_MIDAS PMM ON PMM.ID = PM.ID" 
						+ " WHERE (P.AUTOSCA = 1 OR P.DANIOSCA = 1 OR P.VIDACA = 1 ) AND PMM.NOMBRE_RAZON_SOCIAL LIKE '%" + nombreProveedor.trim() + "%'"
						+ " UNION"
						+ " SELECT P.ID, (PMM.NOMBRE || ' ' ||  PMM.APELLIDO_PATERNO || ' ' || PMM.APELLIDO_MATERNO) NOMBRE FROM MIDAS.TCPRESTADORSERVICIO P"
						+ " INNER JOIN MIDAS.PERSONA_MIDAS PM ON PM.ID = P.PERSONA_ID"
						+ " INNER JOIN MIDAS.PERSONA_FISICA_MIDAS PMM ON PMM.ID = PM.ID"
						+ " WHERE (P.AUTOSCA = 1 OR P.DANIOSCA = 1 OR P.VIDACA = 1 ) AND (PMM.NOMBRE LIKE '%" + nombreProveedor.trim() + "%' OR PMM.APELLIDO_PATERNO LIKE '%" + nombreProveedor.trim() + "%' OR PMM.APELLIDO_MATERNO LIKE '%" + nombreProveedor.trim() + "%')"
						+ " ";
			Query query = entityManager.createNativeQuery(queryString);
			//query.setParameter("pnombre", "%" + nombreProveedor.trim() + "%" );
			//query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			//listaProveedores = query.getResultList();
			List<Object[]> lista = query.getResultList();
			if (lista != null && lista.size() > 0 ) {
				listaProveedores = new ArrayList<Proveedor>();
				for (Object[] obj : lista){
					Proveedor proveedor = new Proveedor();
					BigDecimal id = (BigDecimal)obj[0];
					proveedor.setId(id.longValue());
					proveedor.setNombre((String)obj[1]);
					listaProveedores.add(proveedor);
				}
			}
		}catch(RuntimeException re){
			throw re;
		}
		return listaProveedores;
	}
	
	public List<Negocio> listarNegocios(String descripcion){
		descripcion = descripcion.toUpperCase();
		List<Negocio> lista=null;
		String queryString = "";
		try{
			queryString = " SELECT n.IDTONEGOCIO,n.PCTMAXIMODESCUENTO,n.PCTPRIMACEDER,n.FECHAINICIOVIGENCIA,n.FECHAFINVIGENCIA,n.FORMULADIVIDENDOS, "
						+ " n.DIASRETROACTIVIDAD,n.DIASDIFERIMIENTO,n.DIASGRACIAPOLIZA,n.DIASGRACIAPOLIZASUBSECUENTES, "
						+ " n.DIASPLAZOUSUARIO,n.DIASGRACIAENDOSOSUBSECUENTES,n.CLAVEMANEJAUDI,n.CLAVEPORCENTAJEMONTOUDI,n.VALORUDI, "
						+ " n.PCTUDIPROMOTOR,n.PCTUDIAGENTE,n.CLAVEUDIINCLUYEIVA,n.DESCRIPCIONNEGOCIO, "
						+ " n.VERSIONNEGOCIO,n.FECHACREACION,n.CLAVEMANEJATARIFABASE,n.CLAVEESTATUS, "
						+ " n.CLAVENEGOCIO,n.CLAVEDEFAULT,n.APLICAUSUARIOEXTERNO,n.APLICAUSUARIOINTERNO, "
						+ " n.FECHAACTIVACION,n.CODIGOUSUARIOACTIVACION,n.APLICAPCTPAGOFRACCIONADO,n.FECHAFINVIGENCIAFIJA, "
						+ " n.PCTDESCUENTODEFAULT,n.APLICACURPOBLIGATORIO,n.APLICAVALIDACIONEXTERNO,n.APLICAVALIDACIONRECIBO, "
						+ " n.HABILITAAGRUPACIONRECIBOS,n.APLICARECIBOGLOBAL,n.DESCUENTOMAXORIGINACION, "
						+ " n.APLICAVALIDACIONNUMSERIE,n.APLICAVALIDACIONARTICULO140,n.APLICAVALIDACIONIGUALACION,n.APLICAPOLIZAGOBIERNO, "
						+ " n.APLICASUCURSAL,n.NUMERO_DIAS_ESPERA,n.CORREO_OBLIGATORIO,n.DATOS_COMPLEMENTARIOS_VEHICULO, "
						+ " n.APLICAENLACE,n.APLICAENDOSOCONDUCTOCOBRO " 
					    + " FROM MIDAS.TONEGOCIO n "
						+ " INNER JOIN MIDAS.CA_COMPENSACION c ON C.NEGOCIO_ID = n.IDTONEGOCIO "
						+ " WHERE n.DESCRIPCIONNEGOCIO LIKE '%" + descripcion.trim() + "%'"
						+ " UNION ALL "
						+ " SELECT ID AS IDTONEGOCIO,0 PCTMAXIMODESCUENTO,0 PCTPRIMACEDER,null FECHAINICIOVIGENCIA,null FECHAFINVIGENCIA,"
						+ " null FORMULADIVIDENDOS,0 DIASRETROACTIVIDAD,0 DIASDIFERIMIENTO,0 DIASGRACIAPOLIZA,0 DIASGRACIAPOLIZASUBSECUENTES,"
					    + " 0 DIASPLAZOUSUARIO,0 DIASGRACIAENDOSOSUBSECUENTES,0 CLAVEMANEJAUDI,0 CLAVEPORCENTAJEMONTOUDI,0 VALORUDI,"
					    + " 0 PCTUDIPROMOTOR,0 PCTUDIAGENTE,0 CLAVEUDIINCLUYEIVA,DESCRIPCIONNEGOCIOVIDA DESCRIPCIONNEGOCIO,0 VERSIONNEGOCIO, FECHACREACION,"
					    + " 0 CLAVEMANEJATARIFABASE,0 CLAVEESTATUS,'' CLAVENEGOCIO,0 CLAVEDEFAULT,  0 APLICAUSUARIOEXTERNO,0 APLICAUSUARIOINTERNO,"
					    + " null FECHAACTIVACION,'' CODIGOUSUARIOACTIVACION,0 APLICAPCTPAGOFRACCIONADO,null FECHAFINVIGENCIAFIJA,0 PCTDESCUENTODEFAULT,"
					    + " 0 APLICACURPOBLIGATORIO,0 APLICAVALIDACIONEXTERNO,0 APLICAVALIDACIONRECIBO,0 HABILITAAGRUPACIONRECIBOS,0 APLICARECIBOGLOBAL,"
					    + " 0 DESCUENTOMAXORIGINACION,0 APLICAVALIDACIONNUMSERIE,0 APLICAVALIDACIONARTICULO140,0 APLICAVALIDACIONIGUALACION,"
					    + " 0 APLICAPOLIZAGOBIERNO,0 NUMERO_DIAS_ESPERA,0 CORREO_OBLIGATORIO,0 DATOS_COMPLEMENTARIOS_VEHICULO,0 APLICASUCURSAL,"
					    + " 0 APLICAENDOSOCONDUCTOCOBRO,0 APLICAENLACE FROM MIDAS.CA_NEGOCIOVIDA WHERE DESCRIPCIONNEGOCIOVIDA LIKE '%" + descripcion.trim() + "%'";
			Query query = entityManager.createNativeQuery(queryString, Negocio.class);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			lista = query.getResultList();
		}catch(RuntimeException re){
			throw re;
		}
		return lista;
	}
	public List<Proveedor> listarProveedoresId(String id){
		List<Proveedor> listaProveedores = null;
		id = id.trim().toUpperCase();
		String queryString = "";
		try{
//			queryString = "selectP.ID, from TCPRESTADORSERVICIO p where p.id =" + id;
			queryString = "SELECT P.ID, PMM.NOMBRE_RAZON_SOCIAL NOMBRE FROM MIDAS.TCPRESTADORSERVICIO P"
						+ " INNER JOIN MIDAS.PERSONA_MIDAS PM ON PM.ID = P.PERSONA_ID"
						+ " INNER JOIN MIDAS.PERSONA_MORAL_MIDAS PMM ON PMM.ID = PM.ID" 
						+ " WHERE P.ID = " + id
						+ " UNION"
						+ " SELECT P.ID, (PMM.NOMBRE || ' ' ||  PMM.APELLIDO_PATERNO || ' ' || PMM.APELLIDO_MATERNO) NOMBRE FROM MIDAS.TCPRESTADORSERVICIO P"
						+ " INNER JOIN MIDAS.PERSONA_MIDAS PM ON PM.ID = P.PERSONA_ID"
						+ " INNER JOIN MIDAS.PERSONA_FISICA_MIDAS PMM ON PMM.ID = PM.ID"
						+ " WHERE P.ID = " + id;
			Query query = entityManager.createNativeQuery(queryString);
			//query.setParameter("pnombre", "%" + nombreProveedor.trim() + "%" );
			//query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			//listaProveedores = query.getResultList();
			List<Object[]> lista = query.getResultList();
			if (lista != null && lista.size() > 0 ) {
				listaProveedores = new ArrayList<Proveedor>();
				for (Object[] obj : lista){
					Proveedor proveedor = new Proveedor();
					BigDecimal idP = (BigDecimal)obj[0];
					proveedor.setId(idP.longValue());
					proveedor.setNombre((String)obj[1]);
					listaProveedores.add(proveedor);
				}
			}
		}catch(RuntimeException re){
			throw re;
		}
		return listaProveedores;
	}
	
	public List<CaEntidadPersona> listarAgentesProveedores(String nombre,Long tipo){
		LOG.info("---> listarAgentesProveedores()");
	    List<CaEntidadPersona > lista = new ArrayList<CaEntidadPersona>();
	    try {
		   if(nombre!=null){
		      nombre=nombre.toUpperCase();
		    }
	      final StringBuilder queryString = new StringBuilder(200);	      
	      if(tipo==CaTipoEntidad.TIPO_AGENTE){
	    	  queryString.append(" SELECT DISTINCT agt.IDAGENTE, EP.NOMBRES AS NOMBRES  ");
	    	  queryString.append(" FROM MIDAS.CA_ENTIDADPERSONA EP ");
	    	  queryString.append(" INNER JOIN midas.TOAGENTE agt ON(agt.ID=ep.TOAGENTE_ID) ");
		      queryString.append(" INNER JOIN midas.CA_ORDENPAGOS_DETALLE ord ON (ord.ENTIDADPERSONA_ID=ep.id) ");		      
		      queryString.append(" INNER JOIN MIDAS.CA_TIPOENTIDAD TIPO ON (TIPO.ID = EP.TIPOENTIDAD_ID ");
		      queryString.append(" AND TIPO.ID = ORD.TIPOENTIDAD_ID)");
		      queryString.append(" WHERE EP.NOMBRES LIKE UPPER(?1) ");
		      queryString.append(" AND ORD.ESTATUS_ORDPAG_GEN IN(1,2) ");
		      queryString.append(" AND TIPO.VALOR = ?2  ");		      
	      }
	      
	      else if (tipo==CaTipoEntidad.TIPO_PROVEEDOR){
	    	  queryString.append(" SELECT DISTINCT pre.id ,pm.NOMBRE");
	    	  queryString.append(" FROM MIDAS.TCPRESTADORSERVICIO pre ");
	    	  queryString.append(" INNER JOIN MIDAS.persona_midas pm ON (pre.PERSONA_ID = pm.ID) ");
	    	  queryString.append(" INNER JOIN MIDAS.CA_ORDENPAGOS_DETALLE op ON (pre.ID = op.IDENTIFICADOR_BEN_ID ");
	    	  queryString.append(" AND op.CVE_TIPO_ENTIDAD = 'PROVEEDOR') ");
	    	  queryString.append(" INNER JOIN MIDAS.CA_TIPOENTIDAD TE ON(TE.ID   = OP.TIPOENTIDAD_ID) ");
	    	  queryString.append(" WHERE  pm.nombre LIKE UPPER(?1) ");
	    	  queryString.append(" AND  OP.ESTATUS_ORDPAG_GEN IN (1,2) ");
	    	  queryString.append(" AND te.valor = ?2");
	      }
	      
	     else if(tipo==CaTipoEntidad.TIPO_PROMOTOR){	    	  
	    	  queryString.append(" SELECT DISTINCT agt.IDPROMOTORIA,ord.nombre_beneficiario ");
              queryString.append(" FROM midas.CA_ENTIDADPERSONA ep ");
              queryString.append(" INNER JOIN midas.TOAGENTE agt ON(agt.ID=ep.TOAGENTE_ID) ");
	    	  queryString.append(" INNER JOIN midas.CA_ORDENPAGOS_DETALLE ord ON (ord.ENTIDADPERSONA_ID=ep.id) ");
	    	  queryString.append(" INNER JOIN midas.CA_TIPOENTIDAD tipo ON (tipo.ID=ep.TIPOENTIDAD_ID ");
	    	  queryString.append(" AND tipo.id=ord.TIPOENTIDAD_ID) ");
		      queryString.append(" WHERE ord.NOMBRE_BENEFICIARIO LIKE UPPER(?1) ");
		      queryString.append(" AND ord.ESTATUS_ORDPAG_GEN IN(1,2) ");
		      queryString.append(" AND tipo.valor = 1  ");
		      queryString.append(" AND ord.CVE_TIPO_ENTIDAD    = 'PROMOTOR' ");
		      }
	       else if(tipo ==CaTipoEntidad.TIPO_NEGOCIO){
	    	  queryString.append(" select DISTINCT NEG.IDTONEGOCIO, NEG.DESCRIPCIONNEGOCIO from MIDAS.TONEGOCIO NEG ");
	    	  queryString.append(" WHERE IDTONEGOCIO IN( ");
	    	  queryString.append(" SELECT DISTINCT CO.NEGOCIO_ID FROM MIDAS.CA_ORDENPAGOS ORD ");
	    	  queryString.append(" INNER JOIN MIDAS.CA_ORDENPAGOS_DETALLE ORDD ON(ORDD.FOLIO=ORD.FOLIO) ");
	    	  queryString.append(" and ORDD.ESTATUS_ORDPAG_GEN<>3 ");
	    	  queryString.append(" INNER JOIN MIDAS.ca_compensacion CO  ON (ORD.COMPENSACION_ID=CO.ID) ");
	    	  queryString.append(" INNER JOIN MIDAS.ca_ramo RM ON (CO.ramo_id = rm.id) ");
	    	  queryString.append(" INNER JOIN MIDAS.ca_entidadpersona EP ON (EP.compensacion_id = CO.id AND ORD.ENTIDADPERSONA_ID=EP.ID)  ");
	    	  queryString.append(" INNER JOIN MIDAS.ca_tipoentidad TE ON (EP.tipoentidad_id = TE.id AND TE.ID=ORD.TIPOENTIDAD_ID) ");
	    	  queryString.append(" WHERE NEG.DESCRIPCIONNEGOCIO LIKE UPPER (?1) ) ");	    	    
	      }
	      else if(tipo == CaTipoEntidad.TIPO_CONTRATANTE){
		    	  queryString.append("  SELECT DISTINCT 0 AS ID ,NOMBRE_CONTRATANTE ");
		    	  queryString.append("  FROM MIDAS.CA_ORDENPAGOS_DETALLE  ");
		    	  queryString.append("  WHERE ESTATUS_ORDPAG_GEN<>3 ");
		    	  queryString.append("  AND NOMBRE_CONTRATANTE like UPPER(?1) ");	    	    
		      }
	      else if(tipo == CaTipoEntidad.TIPO_GERENCIA){
	    	  queryString.append("  SELECT DISTINCT POL_p.ID_GERENCIA, GER.NOM_GERENCIA  ");
	    	  queryString.append("  FROM MIDAS.CA_ORDENPAGOS_DETALLE OPD  ");
	    	  queryString.append("  INNER JOIN SEYCOS.POL_POLIZA POL_p ON(OPD.ID_COTIZACION = POL_p.ID_COTIZACION) ");
	    	  queryString.append("  INNER JOIN SEYCOS.GERENCIA GER ON(POL_p.ID_GERENCIA = GER.ID_GERENCIA) ");
	    	  queryString.append("  WHERE GER.NOM_GERENCIA like UPPER(?1) ");
	      }	  
	      Query query = entityManager.createNativeQuery(queryString.toString());
	      
	      LOG.info("Resultado ----->" + query.toString());
	      query.setParameter(1, "%" +nombre+"%"); 
	      query.setParameter(2, tipo); 
	      LOG.info("Resultado ----->" + tipo.toString());
	      List<Object[]> resultList=query.getResultList();
          for (Object[] result : resultList){ 
        	  CaEntidadPersona object= new CaEntidadPersona();
        	  //object.setId(new Long(((BigDecimal)result[0]).toString()));
        	  object.setId(Utilerias.obtenerLong(result[0]));
        	  object.setNombres((String)result[1]);
              lista.add(object);
              
          }
	    } catch (RuntimeException re) {
	    	LOG.error("-- listarAgentesProveedores()", re);
	        throw re; 
	    }
	    LOG.info("lista"+lista.size());
	    return lista;
	  }
	
	public List<Promotoria> listarPromotores(String nombrePromotor){
		List<Promotoria> listaPromotores = null;
		final StringBuilder queryString = new StringBuilder();
		try{			
			queryString.append(" SELECT PROM.* ");
			queryString.append(" FROM MIDAS.CA_ENTIDADPERSONA EP ");
			queryString.append(" INNER JOIN MIDAS.TOAGENTE AGE ON(AGE.ID = EP.TOAGENTE_ID) ");
			queryString.append(" INNER JOIN MIDAS.TOPROMOTORIA PROM ON(PROM.ID = AGE.IDPROMOTORIA AND PROM.IDPERSONARESPONSABLE IS NOT NULL) ");
			queryString.append(" WHERE PROM.DESCRIPCION LIKE ?1 ");
			Query query = entityManager.createNativeQuery(queryString.toString(), Promotoria.class);
			query.setParameter(1, "%"+nombrePromotor.trim().toUpperCase()+"%");
			listaPromotores = query.getResultList();
		}catch(Exception e){
			LOG.error("InformaciÃƒÆ’Ã‚Â³n del Error", e);			
		}
		return listaPromotores;		
	}
}
