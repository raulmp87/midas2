package mx.com.afirme.midas2.dao.impl.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionDao;
import mx.com.afirme.midas2.domain.catalogos.banco.ConfigCuentaMidas;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ReferenciaBancaria;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.VentaSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.VentaSalvamento.EstatusSalvamento;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionDTO;
import mx.com.afirme.midas2.util.MidasException;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.js.util.StringUtil;




/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 12:37:36 p.m.
 */
@Stateless
public class  RecuperacionDaoImpl extends EntidadDaoImpl implements RecuperacionDao{

	@EJB
	EntidadDao entidadDao;

	
	/**
	 * Desactiva todas las referencias bancarias de una recuperacion. Se utiliza cuando se genera un nuevo set de referencias.
	 * @param recuperacionId
	 */
	public void desactivarReferenciasBancarias(Long recuperacionId){
		StringBuilder queryString = new StringBuilder("UPDATE ").append(ReferenciaBancaria.class.getCanonicalName());
		queryString.append(" model SET model.activo = 0 WHERE model.recuperacion.id = :recuperacionId ");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("recuperacionId", recuperacionId);		
		query.executeUpdate(); 		
	}

	/**
	 * Invocar al procedure MIDAS.PKGSIN_RECUPERACIONES.buscarRecuperaciones
	 * 
	 * @param filtro
	 */

	@SuppressWarnings("unchecked")
	@Override
	public List<RecuperacionDTO> buscarRecuperaciones(RecuperacionDTO filtro) {


		List<RecuperacionDTO> lista  = new ArrayList<RecuperacionDTO>();
		String spName = "MIDAS.PKGSIN_RECUPERACIONES.buscarRecuperaciones"; 
		StoredProcedureHelper storedHelper 			= null;
		try {
			
			String propiedades = 
			"numeroRecuperacion," +
			"tipoRecuperacionDesc," +
			"numeroSiniestro," +
			"numeroReporte," +
			"numeroPrestador," +
			"nombrePrestador," +
			"monto," +
			"fechaRegistro," +
			"fechaSiniestro," +
			"medioRecuperacionDesc," +
			"fechaCancelacion," +
			"fechaAplicacion," +
			"estatusDesc," +
			"usuario," +
			"idRecuperacion," +
			"estatus," +
			"tipoRecuperacion," +
			"estatusPreventa," +
			"estatusCartaCia," +
			"folioCarta," +
			"enSubasta," +
			"ptDocumentada," +
			"paseAtencion," +
			"esIndemnizado," +
			"nombreOficina," +
			"origenRegistroDesc," +
			"subasta," +
			"conceptoRecuperacionRefacciones," +
			"tipoServicioPoliza," +
			"complemento," + 
			"montoProvision," + 
			"dua," + 
			"costoEstimadoCobertura," + 
			"motivoCancelacion";
			
			String columnasBaseDatos = 
				"numero," +
				"tipoRecuperacionDesc," +
				"numeroSiniestro," +
				"numeroReporte," +
				"numeroPrestador," +
				"nombrePrestador," +
				"monto," +
				"fecha_registro," +
				"fecha_siniestro," +
				"medio," +
				"fecha_cancelacion," +
				"fecha_aplicacion," +
				"estatusDesc," +
				"usuario," +
				"idRecuperacion," +
				"estatus," +
				"tipoRecuperacion," +
				"estatusPreventa," +
				"estatusCartaCia," +
				"folioRecuperaciones," +
				"subasta," +
				"ptDocumentada," +
				"paseAtencion," +
				"indemnizada," +
				"nombreOficina," +
				"origenRegistroDesc," +
				"numSubasta," +
				"conceptoRefacciones," +
				"tipoServicio," +
				"complementoCia," + 
				"montoProvision," +
				"dua," + 
				"costoEstimadoCobertura," + 
				"motivoCancelacion";
			

			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(RecuperacionDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltro(storedHelper, filtro);
			lista = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion busqueda recuperaciones ..."+descErr
						, Level.WARNING, e);    
			}
		}


		return lista;

	}


	private void agregaParametrosDelFiltro(StoredProcedureHelper storedHelper,RecuperacionDTO filtro){
		
		storedHelper.estableceParametro("p_numeroRecuperacion", this.convertToNull( filtro.getNumeroRecuperacion() ));
		storedHelper.estableceParametro("p_tipoRecuperacion",   this.convertToNull( filtro.getTipoRecuperacion()));
		storedHelper.estableceParametro("p_estatusRecuperacion",this.convertToNull( filtro.getEstatus() ));
		storedHelper.estableceParametro("p_nombrePrestador",    this.convertToNull( filtro.getNombrePrestador() ));
		storedHelper.estableceParametro("p_PaseAtencion",       this.convertToNull( filtro.getPaseAtencion() ));
		storedHelper.estableceParametro("p_numeroSiniestro",    this.convertToNull( filtro.getNumeroSiniestro() ));
		storedHelper.estableceParametro("p_usuario",            this.convertToNull( filtro.getUsuario() ));
		storedHelper.estableceParametro("p_EstatusCarta",       this.convertToNull( filtro.getEstatusCarta() ));
		storedHelper.estableceParametro("p_indemnizada",        this.convertToNull(  this.validaParamSiNo(filtro.getEsIndemnizado()) ));
		storedHelper.estableceParametro("p_montoInicial",		this.convertToNull( filtro.getMontoInicial() ));
		storedHelper.estableceParametro("p_montoFinal",			this.convertToNull( filtro.getMontoFinal() ));
		storedHelper.estableceParametro("p_fechaIniAplicacion", this.convertToNull( filtro.getFechaAplicacionDe() ));
		storedHelper.estableceParametro("p_fechaFinAplicacion", this.convertToNull( filtro.getFechaAplicacionHasta() ));
		storedHelper.estableceParametro("p_ptDocumentada",      this.convertToNull( this.validaParamSiNo(filtro.getPtDocumentada())));
		storedHelper.estableceParametro("p_fechaIniRegistro",   this.convertToNull( filtro.getFechaIniRegistro() ));
		storedHelper.estableceParametro("p_fechaFinRegistro",   this.convertToNull( filtro.getFechaFinRegistro() ));
		storedHelper.estableceParametro("p_fechaIniSiniestro",  this.convertToNull( filtro.getFechaIniSiniestro() ));
		storedHelper.estableceParametro("p_fechaFinSiniestro",  this.convertToNull( filtro.getFechaFinSiniestro() ));
		storedHelper.estableceParametro("p_numeroPrestador",    this.convertToNull( filtro.getNumeroPrestador() ));
		
	}
	
	
	private Object validaParamSiNo(String valor ){
		if( valor == null ) {
			return null;
		}else if( valor.equals("") ){
			return null;
		}else{
			return valor.equals("S") ? 1:0;
		}
	}	

	private Object convertToNull(Object obj){
		if( obj instanceof String){
			obj = (obj != null && ((String)obj).equals(""))?null:obj;
		}else if ( obj instanceof Boolean){
			obj = (obj != null && ((Boolean)obj)== Boolean.FALSE)?null:obj;
		}
		return obj;
	}


	@Override
	public String generarReferenciaBancaria(String referenciaPreliminar, ConfigCuentaMidas cuenta, Date fecha, BigDecimal importe) {
		String funcionSeycos = null;

		if( cuenta.getCuenta().getAlgoritmo().equals("1") ){
			funcionSeycos ="SEYCOS.pkg_algoritmos_dv.fn_referencia_45";
		}else if( cuenta.getCuenta().getAlgoritmo().equals("2") ){
			funcionSeycos ="SEYCOS.pkg_algoritmos_dv.fn_referencia_97";					

		}else if( cuenta.getCuenta().getAlgoritmo().equals("3") ){
			funcionSeycos ="SEYCOS.pkg_algoritmos_dv.fn_referencia_22";
		}

		String queryStr = "select "+funcionSeycos+" (?1,?2,?3) from dual";
		Query query = entityManager.createNativeQuery(queryStr.toString());
		query.setParameter(1, referenciaPreliminar);
		query.setParameter(2, fecha);
		query.setParameter(3, importe);
		return (String)query.getSingleResult();		

	}


	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigCuentaMidas> listadoCuentasBancarias(){
		Query query;  

		String queryStr = "SELECT config" + 
		" FROM ConfigCuentaMidas config" +						
		" JOIN config.cuenta cuenta " +
		" JOIN cuenta.bancoMidas banco" +				
		" WHERE " +
		" config.nivel = 3 and cuenta.monedaId = 484 " +	
		" AND cuenta.activo = true" +	
		" ORDER BY config.orden";  

		query = entityManager.createQuery(queryStr);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);	
		return query.getResultList();

	}

	@Override
	public List<ReferenciaBancariaDTO> obtenerCuentasBancarias() {

		List<ReferenciaBancariaDTO> listaResultados = new ArrayList<ReferenciaBancariaDTO>();
		List<ConfigCuentaMidas> tstConfigCuentaMidas = this.listadoCuentasBancarias();
		for(ConfigCuentaMidas conf :tstConfigCuentaMidas ){
			ReferenciaBancariaDTO dto=new ReferenciaBancariaDTO();
			dto.setBancoMidas(conf.getCuenta().getBancoMidas());
			dto.setClabe(conf.getCuenta().getClabe());
			dto.setCuentaBancaria(conf.getCuenta().getNumeroCuenta());
			listaResultados.add(dto);
		}	
		return listaResultados;

	}

	
	public IndemnizacionSiniestro obtenerDeterminacionPorRecuperacion(Long estimacionId){
		
		try{
		
			StringBuilder queryString = new StringBuilder(" SELECT indem FROM IndemnizacionSiniestro indem  WHERE indem.ordenCompraGenerada.idTercero = :keyEstimacion  AND indem.estatusIndemnizacion NOT IN ('R','C')  ");
			
			TypedQuery<IndemnizacionSiniestro> query = entityManager.createQuery(queryString.toString(), IndemnizacionSiniestro.class);
			
			query.setParameter("keyEstimacion", estimacionId);
			
			return query.getSingleResult();
		
		}catch(Exception e){
			return null;
		}

	}
	
	@Override
	public BigDecimal obtenerMontoRecuperacionSalvamento(Long siniestroId,EstatusRecuperacion estatus) {
		
		BigDecimal monto = BigDecimal.ZERO;			
		Map<String, Object>  params = new HashMap<String, Object>();
		StringBuilder query = new StringBuilder("SELECT COALESCE(SUM(venta.totalVenta),0) ");
		query.append(" FROM RecuperacionSalvamento recuperacionSalvamento ");
		query.append(" JOIN recuperacionSalvamento.listaVentasSalvamentos venta ");
		query.append(" WHERE ");
		query.append(" recuperacionSalvamento.reporteCabina.siniestroCabina.id = :siniestroId ");
		query.append(" AND recuperacionSalvamento.estatus = :estatus ");
		query.append(" AND venta.estatus = :estatusSalvamento ");
		params.put("siniestroId", siniestroId);
		params.put("estatus", estatus.toString());
		params.put("estatusSalvamento", EstatusSalvamento.ACT );
		List<BigDecimal> totales =  this.executeQueryMultipleResult(query.toString(), params);
		if(totales != null && totales.size() > 0){
			monto = totales.get(0);
		}
		return monto;
		
	}

	@Override
	public BigDecimal obtenerIvaRecuperacionSalvamento(Long siniestroId, EstatusRecuperacion estatus) {
		
		BigDecimal monto = BigDecimal.ZERO;			
		Map<String, Object>  params = new HashMap<String, Object>();
		StringBuilder query = new StringBuilder("SELECT COALESCE(SUM(venta.iva),0) ");
		query.append(" FROM RecuperacionSalvamento recuperacionSalvamento  ");
		query.append(" JOIN recuperacionSalvamento.listaVentasSalvamentos venta ");
		query.append(" WHERE ");
		query.append(" recuperacionSalvamento.reporteCabina.siniestroCabina.id = :siniestroId ");
		query.append(" AND recuperacionSalvamento.estatus = :estatus ");
		query.append(" AND venta.estatus = :estatusSalvamento ");
		params.put("siniestroId", siniestroId);
		params.put("estatus", estatus.toString());
		params.put("estatusSalvamento", EstatusSalvamento.ACT );
		List<BigDecimal> totales =  this.executeQueryMultipleResult(query.toString(), params);
		if(totales != null && totales.size() > 0){
			monto = totales.get(0);
		}
		return monto;
	}
	

	@Override
	public void invocaProvision(Long movimientoId)  throws MidasException  {
		String spName = "MIDAS.pkg_contabilidad.stpregistraprovisiones";                  
		StoredProcedureHelper storedHelper = null;
		Recuperacion resultado = new Recuperacion();
		try { 
			LogDeMidasEJB3.log("Entrando a stpregistraprovisiones..." + this, Level.INFO, null);                                       
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("idRecuperacion", movimientoId);
			storedHelper
			.estableceMapeoResultados(
					Recuperacion.class.getCanonicalName(),
					"id", "lote");
			resultado = (Recuperacion) storedHelper.obtieneResultadoSencillo();

			LogDeMidasEJB3.log("Saliendo de stpregistraprovisiones, provision id - " + resultado.getId() 
					+ this, Level.INFO, null);

		} catch (Exception e) {
			String descErr = null;
			String mj ="Excepcion general al registrar la provision. ";
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta(); 
				LogDeMidasInterfaz.log(mj+descErr, Level.WARNING, e);    
			}
			throw new MidasException(mj+( (!StringUtil.isEmpty(descErr) ) ? descErr : ""));
		}
	}


	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> entityClass,
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> entityClass,
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> entityClass, String columns, Map<String, Object> params,
			Map<String, Object> queryParams, StringBuilder queryWhere,
			StringBuilder queryOrder) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> entityClass, Map<String, Object> params,
			Map<String, Object> queryParams, StringBuilder queryWhere,
			StringBuilder queryOrder) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(
			Class<E> entityClass, Map<String, Object> params,
			String... orderByAttributes) {
		// TODO Auto-generated method stub
		return null;
	}


	@Override
	public List<RecuperacionSalvamento> obtenerVentasPorConfirmarIngresoProrroga() {
		
		List<RecuperacionSalvamento> lRecuperaciones = new ArrayList<RecuperacionSalvamento>();		
		Map<String, Object>  params = new HashMap<String, Object>();
		
		StringBuilder queryString = new StringBuilder("SELECT recuperacionSalvamento ");
		queryString.append(" FROM RecuperacionSalvamento recuperacionSalvamento  ");
		queryString.append(" JOIN recuperacionSalvamento.listaVentasSalvamentos venta ");
		queryString.append(" WHERE ");
		queryString.append(" venta.estatus = :estatus ");
		queryString.append(" AND recuperacionSalvamento.cuentaConfirmacionIngreso = null ");
		
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("estatus", VentaSalvamento.EstatusSalvamento.ACT );
		lRecuperaciones = query.getResultList();
		
		return lRecuperaciones;
	}
	
	@Override
	public List<RecuperacionProveedor> obtenerRecuperacionesProveedorPendientes(Long estimacionId) {
		
		StringBuilder queryString =null;
		
		queryString = new StringBuilder("SELECT recuperacion from RecuperacionProveedor recuperacion ")
		.append(" JOIN EstimacionCoberturaReporteCabina estimacion ON estimacion.id = recuperacion.ordenCompra.idTercero ")
		.append(" WHERE recuperacion.estatus IN ( :estatusRegistrado,:estatusPendiente ) ")
		.append(" AND estimacion.id = :estimacionId");
		
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("estimacionId", estimacionId);
		query.setParameter("estatusRegistrado", Recuperacion.EstatusRecuperacion.REGISTRADO.getValue());
		query.setParameter("estatusPendiente" , Recuperacion.EstatusRecuperacion.PENDIENTE.getValue());
		
		List<RecuperacionProveedor> lProveedor = new ArrayList<RecuperacionProveedor>();	
		lProveedor =  query.getResultList();
		return lProveedor;
	}
	
	/**
	 *  Lista de Recuperaciones que cuentan con referencias bancarias ligadas a un reporte cabina
	 * 
	 * @param idReporteCabina
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RecuperacionDTO> buscarReferenciasBancarias(Long idReporteCabina) {
		List<RecuperacionDTO> lista = new ArrayList<RecuperacionDTO>();
		String spName = "MIDAS.PKGSIN_RECUPERACIONES.REFERENCIASBANCARIAS"; 
		StoredProcedureHelper storedHelper = null;
		
		try {
			StringBuilder propiedades = new StringBuilder();
			propiedades.append("numeroRecuperacion,");
			propiedades.append("tipoRecuperacionDesc,");
			propiedades.append("fechaRegistro,");
			propiedades.append("nombrePrestador,");
			propiedades.append("fechaSiniestro,");
			propiedades.append("monto,");
			propiedades.append("estatusDesc,");
			propiedades.append("paseAtencion,");
			propiedades.append("idRecuperacion");
					
			StringBuilder columnasBaseDatos = new StringBuilder();
			columnasBaseDatos.append("numero,");
			columnasBaseDatos.append("tipoRecuperacionDesc,");
			columnasBaseDatos.append("fecha_registro,");
			columnasBaseDatos.append("nombrePrestador,");
			columnasBaseDatos.append("fecha_siniestro,");
			columnasBaseDatos.append("monto,");
			columnasBaseDatos.append("estatusDesc,");
			columnasBaseDatos.append("paseAtencion,");
			columnasBaseDatos.append("idRecuperacion");
			
			
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(RecuperacionDTO.class.getCanonicalName(), propiedades.toString().split(","), columnasBaseDatos.toString().split(","));
			storedHelper.estableceParametro("p_idReporteCabina", idReporteCabina);
			lista = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			if (storedHelper != null) {
				String descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log("Excepcion busqueda recuperaciones ..."+descErr, Level.WARNING, e);    
			}
		}
 		return lista;
	}
}