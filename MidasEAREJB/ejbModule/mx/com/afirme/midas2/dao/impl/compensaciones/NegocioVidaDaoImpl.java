/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.ArrayList;
import java.util.List;
import javax.ejb.Stateless;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.NegocioVidaDao;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;

import org.apache.log4j.Logger;

@Stateless
public class NegocioVidaDaoImpl implements NegocioVidaDao {	

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = Logger.getLogger(CaCompensacionDaoImpl.class);

	public List<DatosSeycos> findAgenteSeycosConPromotor(DatosSeycos datosSeycos) {
		
		LOG.info(">> findAgenteSeycos()");
		List<DatosSeycos> listaAgenteSeycos = new ArrayList<DatosSeycos>();	
		
		try {
			
			int index = 1;
			
			final StringBuilder queryString = new StringBuilder(" SELECT ");
			queryString.append(" AGE.ID_AGENTE ");
			queryString.append(" , VMEF.NOMBRE AS NOMBREAGENTE ");
			queryString.append(" , AGE.ID_EMPRESA  ");
			queryString.append(" , AGE.F_SIT ");
			queryString.append(" , AGE.ID_SUPERVISORIA ");
			queryString.append(" , VMEF.NOM_SUPERVISORIA AS NOMBRESUPERVISORIA");
			queryString.append(" FROM SEYCOS.VW_EST_FV VMEF");
			queryString.append(" INNER JOIN SEYCOS.AGENTE AGE ON (AGE.ID_AGENTE = VMEF.ID_AGENTE) ");
			queryString.append(" WHERE AGE.SIT_AGENTE = 'A' ");
			queryString.append(" AND AGE.ID_AGENTE = ? ");
			queryString.append(" AND AGE.ID_EMPRESA = ? ");
			queryString.append(" AND AGE.F_SIT = ? ");
			queryString.append(" AND EXISTS( ");
			queryString.append(" SELECT 'EXISTE' ");
			queryString.append(" FROM MIDAS.TOPROMOTORIA PROM ");
			queryString.append(" INNER JOIN MIDAS.VW_PERSONA PER ");
			queryString.append(" ON(PROM.IDPERSONARESPONSABLE=PER.IDPERSONA) ");
			queryString.append(" WHERE PROM.IDPROMOTORIA=AGE.ID_SUPERVISORIA ");
			queryString.append(" ) ");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			
			query.setParameter(index++, datosSeycos.getIdAgente());
			query.setParameter(index++, datosSeycos.getIdEmpresa());
			query.setParameter(index++, datosSeycos.getDateFsit());
			
			List<Object[]> resultList = query.getResultList();
			DatosSeycos agenteSeycos = null;
			
			for (Object[] result : resultList){ 
				agenteSeycos = new DatosSeycos();				
				agenteSeycos.setIdAgente(result[0].toString());		   
				agenteSeycos.setNombreCompleto(result[1].toString());
				agenteSeycos.setIdEmpresa(result[2].toString());
				agenteSeycos.setFsit(result[3].toString());				
				agenteSeycos.setIdSupervisoria(result[4].toString());
				agenteSeycos.setNombreSupervisoria(result[5].toString());
				listaAgenteSeycos.add(agenteSeycos);
			}
		      
		} catch (RuntimeException e) {
			LOG.error("Informacion del Error", e);
		}
		LOG.info(">> findAgenteSeycos()");
		return listaAgenteSeycos;
	}
}
