package mx.com.afirme.midas2.dao.impl.negocio.cliente;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionGrupoDao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionGrupo;

@Stateless
public class ConfiguracionGrupoDaoImpl extends JpaDao<Long, ConfiguracionGrupo> implements ConfiguracionGrupoDao {

}
