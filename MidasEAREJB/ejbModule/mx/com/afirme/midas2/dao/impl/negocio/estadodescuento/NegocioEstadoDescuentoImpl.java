package mx.com.afirme.midas2.dao.impl.negocio.estadodescuento;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.negocio.estadodescuento.NegocioEstadoDescuentoDao;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.exeption.ApplicationException;

@Stateless
public class NegocioEstadoDescuentoImpl extends EntidadDaoImpl implements
NegocioEstadoDescuentoDao{
	@Override
	public List<NegocioEstadoDescuento> obtenerEstadosPorNegocioId(Long idToNegocio) {
		return findByProperty(NegocioEstadoDescuento.class, "negocio", idToNegocio);
	}
	
	@Override
	public NegocioEstadoDescuento findByNegocioAndEstado(Long idToNegocio, String idToEstado) {
		NegocioEstadoDescuento negocioEstadoDescuento = null;
		final String jpql = "select model from NegocioEstadoDescuento model where " +
				"model.negocio.idToNegocio = :idToNegocio " +
				"and model.estadoDTO.stateId = :idToEstado";
		TypedQuery<NegocioEstadoDescuento> query = entityManager.createQuery(jpql, NegocioEstadoDescuento.class);
		query.setParameter("idToNegocio", idToNegocio)
			.setParameter("idToEstado",  StringUtils.leftPad(idToEstado, 5,"0"));
		 List<NegocioEstadoDescuento> resultList = query.getResultList();
		 if (resultList.size() == 1) {
			 negocioEstadoDescuento = resultList.get(0);
		 }
		 return negocioEstadoDescuento;
	}
	
	@Override
	public List<NegocioEstadoDescuento> findByNegocioAndEstadosAsociados(Long idToNegocio) {
		String queryString = "select ned.id, ned.idtonegocio, ned.idestado, zona, pctdescuento, pctdescuentodefault " +
			"from Midas.toNegEstadoDescuento ned " + 
			"inner join Midas.toNegEstado ne on (ne.idtonegocio = ned.idtonegocio and ne.idestado = ned.idestado) " +
			"where ned.idToNegocio = " + idToNegocio + " order by ne.idtonegocio, ne.idestado";
		
		Query query = entityManager.createNativeQuery(queryString,
				NegocioEstadoDescuento.class);
		List<NegocioEstadoDescuento> lista = (List<NegocioEstadoDescuento>)query.getResultList();
		return lista;
	}
	

	public String obtenerZipCode (Long idToNegocio, String stateId) {
		String zipCode = null;
		
		NegocioEstadoDescuento negocioEstadoDescuento = this.findByNegocioAndEstado(
				idToNegocio, stateId);
		if (negocioEstadoDescuento != null && negocioEstadoDescuento.getZipCode() != null) {
			zipCode = negocioEstadoDescuento.getZipCode();
		} else {
			throw new ApplicationException("El estado no tiene asociado un c\u00F3digo postal fronterizo.");
		}
		
		return zipCode;
	}
}