package mx.com.afirme.midas2.dao.impl.siniestros.valuacion.ordencompra;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.valuacion.ordencompra.OrdenCompraDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionValeRefaccionFinal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacion;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.OrdenCompraRecuperacionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.BandejaOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.OrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.OrdenesCompraAutorizarDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas.RecepcionFacturaServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.valuacion.hgs.HgsService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacionService;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.JpaUtil;
import mx.com.afirme.midas2.wsClient.hgs.Reporte.Vehiculo;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;



@Stateless
public class OrdenCompraDaoImpl extends EntidadDaoImpl implements OrdenCompraDao{
	@EJB
	EntidadDao entidadDao;





	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;


	@EJB
	CatalogoGrupoValorService  catalogoGrupoValorService;


	@EJB
	HgsService hgsService;

	@EJB
	ListadoService  listadoService;

	@EJB
	PagosSiniestroService pagosSiniestroService;

	@EJB
	OrdenCompraService ordenCompraService;


	@EJB 
	EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;


	@EJB
	OrdenCompraAutorizacionService ordenCompraAutorizacionService;
	String ESTATUS_CONCEPTO_TRAMITE ="Tramite";
	
	
	private static final String ROL_PROVEEDOR = "Rol_M2_Proveedor";
	private static final String USUARIO_MIGRACION = "MIGSINIE";


	private int DIASBANDEJA  =30;
	public String SERV_PARTICULAR = "1"; 
	public String SERV_PUBLICO = "2";
	public String SERV_AMBOS = "3"; 
	
	public String TIPO_PAGOPROVEEDOR = "PP"; 
	public String TIPO_PAGOBENEFICIARIO = "PB"; 
	
	public String APLICA_DEDUCIBLE = "SI";
	public String NOAPLICA_DEDUCIBLE = "NO";
	
	public String ESTATUS_FACTURA_REGISTRADA = "R";
	public String ESTATUS_FACTURA_PAGADA = "P";
	
	private static final Logger LOG = Logger.getLogger(OrdenCompraDaoImpl.class);
	//get all buy orders through stored procedure
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenesCompraAutorizarDTO> getOrdenesCompraPorAuthViaSP(){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_ORDENCOMPRA.GET_ORDEN_POR_AUTORIZAR";
		List<OrdenesCompraAutorizarDTO> listaOCxAuth = new ArrayList<OrdenesCompraAutorizarDTO> (6);
		
		try{
			String columnaBd = "ID,SINIESTRO,COBERTURA,TIPO,DETALLE,FECHA_CREACION,ESTATUS,TOTALPAGADO,TOTALPENDIENTE,TOTAL,SUBTOTAL,TAX";
			String atributos = "noOrdenCompra,noSiniestro,cobertura,tipoOrdenCompra,conceptoPago,fechaElaboracion,estatus,totalPagado,totalPendiente,importe,subtotal,iva";
			
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(OrdenesCompraAutorizarDTO.class.getCanonicalName(), atributos, columnaBd);
			
			listaOCxAuth = spHelper.obtieneListaResultados();
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		return listaOCxAuth;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenCompraDTO> obtenerOrdenesCompraSiniestro(
			Long idReporteCabina, String tipo) {
		List<OrdenCompraDTO>  listaDto = new ArrayList<OrdenCompraDTO>();
		Map<String, Object> parameters = new HashMap<String, Object>();	
		List<CatValorFijo>  catRefacciones= catalogoGrupoValorService.obtenerCatalogoValores(TIPO_CATALOGO.TIPO_CONCEPTO_REFACCIONES, null); 
		List<OrdenCompra> ordenesCompras  = new ArrayList<OrdenCompra>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT compra from " + OrdenCompra.class.getSimpleName() + " compra ");
		queryString.append(" WHERE  compra.reporteCabina.id ="+idReporteCabina);
		queryString.append(" AND compra.estatus not in ( ");
		queryString.append(" '"+OrdenCompraService.ESTATUS_INDEMNIZACION_CANCELADA+"', ");
		queryString.append(" '"+OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA+"', ");
		queryString.append(" '"+OrdenCompraService.ESTATUS_INDEMNIZACION+"' ");
		queryString.append(" ) ");
		if(StringUtils.isNotEmpty(tipo)){
			queryString.append(" AND compra.tipo =  ");
			queryString.append(" '"+tipo+"' ");
		}
		queryString.append(" Order by compra.id Desc ");
		ordenesCompras = entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);

		for( OrdenCompra ordenCompra : ordenesCompras ){
			if(ordenCompra.getEstatus().equalsIgnoreCase( OrdenCompraService.ESTATUS_INDEMNIZACION)||ordenCompra.getEstatus().equalsIgnoreCase( OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA)){
				continue;
			}
			if( ordenCompra.getEstatus().equalsIgnoreCase( OrdenCompraService.ESTATUS_RECHAZADA)){
				String fechaOrden=Utilerias.cadenaDeFecha(ordenCompra.getFechaCreacion() ,"dd/MM/yyyy");
				String fechaActual=Utilerias.cadenaDeFecha(new Date() ,"dd/MM/yyyy");				
				int dias=DateUtils.diferenciaDiasFechas(fechaOrden, fechaActual);
				if (dias >DIASBANDEJA){
					continue;
				}
			}
			OrdenCompraDTO dto = new OrdenCompraDTO(); 
			dto.setReporteCabinaId(ordenCompra.getReporteCabina().getId());
			if( null!=ordenCompra.getIdBeneficiario()){
				Integer id = ordenCompra.getIdBeneficiario().intValue();
				PrestadorServicio prestador=entidadDao.findById(PrestadorServicio.class, id);
				try {
					if(null!=prestador &&  StringUtils.isNotEmpty( prestador.getNombrePersona())){
						dto.setBeneficiario(prestador.getNombrePersona());
					}
				}catch (Exception e ){
					dto.setBeneficiario(null);
				}

			}else if (StringUtils.isNotEmpty(ordenCompra.getNomBeneficiario())){
				dto.setBeneficiario(ordenCompra.getNomBeneficiario());
			} 
			List<DetalleOrdenCompra>  DetalleOrdenCompras =entidadDao.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", ordenCompra.getId());
			dto.setRefacciones("NO");
			for( DetalleOrdenCompra detalleOrdenCompra : DetalleOrdenCompras ){
				for( CatValorFijo catR : catRefacciones ){
					if(null!=detalleOrdenCompra.getConceptoAjuste() &&
							null!=detalleOrdenCompra.getConceptoAjuste().getTipoConcepto() &&
							catR.getCodigo().equalsIgnoreCase(detalleOrdenCompra.getConceptoAjuste().getTipoConcepto()) ){
						dto.setRefacciones("SI");
					}
				}
			}
			ImportesOrdenCompraDTO importes= this.ordenCompraService.calcularImportes( ordenCompra.getId(), null, false);
			dto.setImpPagado((ordenCompra.getEstatus().equals(OrdenCompraService.ESTATUS_PAGADA)?importes.getSubtotal():BigDecimal.ZERO));
			dto.setImpPresupuesto(importes.getSubtotal());
			dto.setEstatus(ordenCompra.getEstatus());
			dto.setOrdenCompraId(ordenCompra.getId());
			dto.setNoOrdenCompra(ordenCompra.getId().toString());
			if(StringUtils.isNotEmpty(ordenCompra.getOrigen())){
				CatValorFijo cat=catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ORIGEN_ORDENCOMPRA, ordenCompra.getOrigen());
				if(null==cat){
					continue;
				}
				dto.setCveOrigen(ordenCompra.getOrigen());
				dto.setOrigen(cat.getDescripcion());
			}
			//Se agrega consulta del tipo
			if(StringUtils.isNotEmpty(ordenCompra.getTipo())){
				CatValorFijo cat=catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA, ordenCompra.getTipo());
				if(null!=cat){
					dto.setTipoOrdenCompra(cat.getDescripcion());
				}

			}
			List<OrdenCompraAutorizacion>  lstAutorizacion= this.entidadDao.findByProperty(OrdenCompraAutorizacion.class, "orderCompra.id", ordenCompra.getId());
			/*Se valida que la orden de compra tenga al menos una autorizacion*/
			if(!lstAutorizacion.isEmpty()){
				dto.setContieneFirma(true);
			}else{
				dto.setContieneFirma(false);

			}
			listaDto.add(dto);	
		}

		return listaDto;

	}

	@Override
	public List<OrdenCompra> obtenerOrdenesCompraParaFacturarPorProveedor(Integer idProveedor){
		StringBuilder queryString =null;
		
		Usuario usuarioActual = usuarioService.getUsuarioActual();
		
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("idProveedor", idProveedor);
		parameters.put("estatusAutorizada", OrdenCompraService.ESTATUS_AUTORIZADA);
		parameters.put("estatusAsociada", OrdenCompraService.ESTATUS_ASOCIADA);
		if(usuarioService.tieneRol(ROL_PROVEEDOR, usuarioActual)){
			parameters.put("usuarioMigracion", USUARIO_MIGRACION);
		}
		
		queryString = new StringBuilder("SELECT ordenCompra from " + OrdenCompra.class.getSimpleName() + " ordenCompra ");
		queryString.append(" LEFT JOIN ordenCompra.facturasSiniestroList factura ");
		queryString.append(" WHERE  ordenCompra.idBeneficiario = :idProveedor ");
		queryString.append(" AND  ordenCompra.estatus in ( :estatusAutorizada, :estatusAsociada )");
		if(usuarioService.tieneRol(ROL_PROVEEDOR, usuarioActual)){
			queryString.append(" AND  ordenCompra.codigoUsuarioCreacion <> :usuarioMigracion ");
		}
		queryString.append(" AND ( factura.id IS NULL OR ");
		queryString.append(" ( factura.id = ( ");
		queryString.append(" SELECT MAX(subFactura.id) FROM " + DocumentoFiscal.class.getSimpleName() + " subFactura ");
		queryString.append(" JOIN subFactura.ordenesCompra subOrdenCompra ");
		queryString.append(" WHERE subOrdenCompra.id = ordenCompra.id ");
		queryString.append(" )   AND factura.estatus = '"+RecepcionFacturaServiceImpl.FACTURA_DEVUELTA+"') )");
		@SuppressWarnings("unchecked")
		List<OrdenCompra>  ordenesCompraList = this.entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		List<OrdenCompra>  ordenesCompraParaFacturar = new ArrayList<OrdenCompra>();
		for(OrdenCompra ordenCompra : ordenesCompraList){
			String fechaOrden=Utilerias.cadenaDeFecha(ordenCompra.getFechaCreacion() ,"dd/MM/yyyy");
			String fechaActual=Utilerias.cadenaDeFecha(new Date() ,"dd/MM/yyyy");				
			int dias=DateUtils.diferenciaDiasFechas(fechaOrden, fechaActual);
			if(dias < 180){
				ordenesCompraParaFacturar.add(ordenCompra);
			}
		}
		return ordenesCompraParaFacturar;
	}
	
	
	@Override
	public List<OrdenCompra> obtenerOrdenesCompraParaAgruparParaCompania(Integer idProveedor,Long oficinaSeleccionada, String tipoAgrupador){
		StringBuilder queryString =null;
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("idProveedor", idProveedor);
		parameters.put("estatusAutorizada", OrdenCompraService.ESTATUS_AUTORIZADA);
		parameters.put("estatusAsociada", OrdenCompraService.ESTATUS_ASOCIADA);
		parameters.put("tipoPago", OrdenCompraService.PAGO_A_PROVEEDOR);
		parameters.put("tipo", OrdenCompraService.TIPO_AFECTACION_RESERVA);
		parameters.put("origenSipac", OrdenCompraService.ORIGENSIPAC_AUTOMATICO);
		parameters.put("tipoProveedor", "CIA");
		queryString = new StringBuilder("SELECT ordenCompra from " + OrdenCompra.class.getSimpleName() + " ordenCompra ");
		queryString.append(" LEFT JOIN ordenCompra.facturasSiniestroList factura ");
		queryString.append(" LEFT JOIN EstimacionCoberturaReporteCabina estimacion ON (ordenCompra.idTercero = estimacion.id) ");
		queryString.append(" WHERE  ordenCompra.idBeneficiario = :idProveedor ");
		queryString.append(" AND  ordenCompra.estatus in ( :estatusAutorizada, :estatusAsociada ) ");
		queryString.append(" AND  ordenCompra.tipoPago = :tipoPago ");
		queryString.append(" AND  ordenCompra.tipo = :tipo ");
		queryString.append(" AND  ordenCompra.tipoProveedor = :tipoProveedor ");

		if(DocumentoFiscal.TipoAgrupadorDocumentoFiscal.SIPAC.toString().equals(tipoAgrupador)){
			queryString.append(" AND  ordenCompra.origen = :origenSipac ");
		} else {
			queryString.append(" AND  ordenCompra.origen <> :origenSipac ");
		}
		queryString.append(" AND ( factura.id IS NULL OR ");
		queryString.append(" ( factura.id = ( ");
		queryString.append(" SELECT MAX(subFactura.id) FROM " + DocumentoFiscal.class.getSimpleName() + " subFactura ");
		queryString.append(" JOIN subFactura.ordenesCompra subOrdenCompra ");
		queryString.append(" WHERE subOrdenCompra.id = ordenCompra.id ");
		queryString.append(" )   AND factura.estatus = '"+RecepcionFacturaServiceImpl.FACTURA_DEVUELTA+"') )");
		if(oficinaSeleccionada!=null){
			parameters.put("oficinaSeleccionada", oficinaSeleccionada);
			queryString.append(" AND  estimacion.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.oficina.id = :oficinaSeleccionada");
		}
		@SuppressWarnings("unchecked")
		List<OrdenCompra>  ordenesCompraList = this.entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		return ordenesCompraList;
	}


	public EntidadDao getEntidadDao() {
		return entidadDao;
	}


	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}


	public UsuarioService getUsuarioService() {
		return usuarioService;
	}


	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}


	public CatalogoGrupoValorService getCatalogoGrupoValorService() {
		return catalogoGrupoValorService;
	}


	public void setCatalogoGrupoValorService(
			CatalogoGrupoValorService catalogoGrupoValorService) {
		this.catalogoGrupoValorService = catalogoGrupoValorService;
	}


	public ListadoService getListadoService() {
		return listadoService;
	}


	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}


	public EstimacionCoberturaSiniestroService getEstimacionCoberturaSiniestroService() {
		return estimacionCoberturaSiniestroService;
	}


	public void setEstimacionCoberturaSiniestroService(
			EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService) {
		this.estimacionCoberturaSiniestroService = estimacionCoberturaSiniestroService;
	}


	public OrdenCompraAutorizacionService getOrdenCompraAutorizacionService() {
		return ordenCompraAutorizacionService;
	}


	public void setOrdenCompraAutorizacionService(
			OrdenCompraAutorizacionService ordenCompraAutorizacionService) {
		this.ordenCompraAutorizacionService = ordenCompraAutorizacionService;
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenCompraRecuperacionDTO> obtenerOrdenCompraRecuperacionProveedor(Long idReporteCabina, String coberturaCompuesta, 
			List<Long>   PasesAtencionId, String tipoOrdenCompra,  Long idOrdenCompra )
			{
		
		String strCoberturas=getCoberturaCompuesta(coberturaCompuesta);
		String strPasesAtencion = getPasesAtencion(PasesAtencionId);


		Map<String, Object> parameters = new HashMap<String, Object>();	
		List<OrdenCompra> ordenesCompras  = new ArrayList<OrdenCompra>();
		List<OrdenCompraRecuperacionDTO> lstOrdenCompraRecuperacionDTO  = new ArrayList<OrdenCompraRecuperacionDTO>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT compra from " + OrdenCompra.class.getSimpleName() + " compra ");;
		queryString.append(" WHERE  compra.estatus  in ( ");
		queryString.append(" '"+OrdenCompraService.ESTATUS_PAGADA+"' ");
		queryString.append(" ) ");

		if(null!=idReporteCabina){			
			queryString.append(" AND compra.reporteCabina.id  in ( "+idReporteCabina+")");			
		}

		if(StringUtils.isNotEmpty(tipoOrdenCompra)  && null!=tipoOrdenCompra){	
			queryString.append(" AND compra.tipo  in ( '"+tipoOrdenCompra+"' )");
		}else{
			queryString.append(" AND compra.tipo not in ( ");
			queryString.append(" '"+OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE+"' ");
			queryString.append(" ) ");
		}
		if(StringUtils.isNotEmpty(strCoberturas)){			
			queryString.append(" AND compra.idCoberturaCompuesta  in ( "+strCoberturas+")");			
		}
		if(StringUtils.isNotEmpty(strPasesAtencion)){			
			queryString.append(" AND compra.idTercero  in ( "+strPasesAtencion+")");			
		}
		if(null!=idOrdenCompra){
			queryString.append(" AND compra.id  in ( "+idOrdenCompra+")");

		}else{
			queryString.append("AND compra.id not in  ( SELECT recuperacion.ordenCompra.id from " + RecuperacionProveedor.class.getSimpleName() + 
			" recuperacion  WHERE  recuperacion.estatus <> 'CANCELADO' )  " );
		}

		queryString.append(" AND compra.tipoPago  in ( '"+OrdenCompraService.PAGO_A_PROVEEDOR+"' )");	

		queryString.append(" Order by compra.id Desc ");

		ordenesCompras = entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		for( OrdenCompra ordenCompra : ordenesCompras ){
			OrdenCompraRecuperacionDTO dto = new OrdenCompraRecuperacionDTO();	
			dto.setFactura(ordenCompra.getFactura());
			dto.setFechaOrdenCompra(ordenCompra.getFechaCreacion());
			dto.setEsDanosMateriale(false);
			dto.setEsResponsabilidadCivil(false);
			if(null!=ordenCompra.getOrdenPago()){
				dto.setFechaOrdenPago(ordenCompra.getOrdenPago().getFechaCreacion());
				DesglosePagoConceptoDTO desglose = this.pagosSiniestroService.obtenerTotales(ordenCompra.getOrdenPago().getId(),false);
				if(null!= desglose){
					dto.setMontoOrdenPago( desglose.getTotalesPagados());
				}
			}
			if(null!=ordenCompra.getIdBeneficiario()){
				Integer prestadorID = new Integer (ordenCompra.getIdBeneficiario().toString());
				PrestadorServicio prestador = this.entidadDao.findById(PrestadorServicio.class, prestadorID);
				if(null!=prestador){
					dto.setNombreProveedor(prestador.getPersonaMidas().getNombre());
					dto.setNoProveedor(prestador.getId().toString());
					if(null!=prestador.getPersonaMidas() && null!=prestador.getPersonaMidas().getContacto()){
						if(StringUtils.isNotEmpty(prestador.getPersonaMidas().getContacto().getCorreoPrincipal())){
							dto.setCorreoProveedor(prestador.getPersonaMidas().getContacto().getCorreoPrincipal());
						}
						if(StringUtils.isNotEmpty(prestador.getPersonaMidas().getContacto().getTelCasaCompleto())){
							dto.setTelefonoProveedor(prestador.getPersonaMidas().getContacto().getTelCasaCompleto());
						}
					}
				}
			}			
			ImportesOrdenCompraDTO importes = ordenCompraService.calcularImportes(ordenCompra.getId(), null, false);
			if(null!=importes && null!= importes.getTotal()){
				dto.setMontoOrdenCompra( importes.getTotal());
			}
			//----------CONSULTAR SI ES REFACCIONES 			
			List<CatValorFijo>  catRefacciones= catalogoGrupoValorService.obtenerCatalogoValores(TIPO_CATALOGO.TIPO_CONCEPTO_REFACCIONES, null);
			List<DetalleOrdenCompra>  DetalleOrdenCompras =entidadDao.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", ordenCompra.getId());
			dto.setEsRefaccion(false);
			for( DetalleOrdenCompra detalleOrdenCompra : DetalleOrdenCompras ){
				for( CatValorFijo catR : catRefacciones ){
					if(null!=detalleOrdenCompra.getConceptoAjuste() &&
							null!=detalleOrdenCompra.getConceptoAjuste().getTipoConcepto() &&
							catR.getCodigo().equalsIgnoreCase(detalleOrdenCompra.getConceptoAjuste().getTipoConcepto()) ){
						dto.setEsRefaccion(true);
					}
				}
			}
			//----------------
			List<ValuacionValeRefaccionFinal> lstvaluacion=  this.entidadDao.findByProperty(ValuacionValeRefaccionFinal.class, "ordenCompraId", ordenCompra.getId());
			if(null!=lstvaluacion && !lstvaluacion.isEmpty()){
				ValuacionValeRefaccionFinal valuacion = lstvaluacion.get(0);	
				//dto.setEsRefaccion(valuacion.getRefaccion());
				dto.setNombreTaller(valuacion.getNombreProveedor());
				
				
				if(null!=valuacion.getValuacionHgs()){
					dto.setNumValuacion(valuacion.getValuacionHgs().getId());
					if(StringUtils.isNotEmpty(valuacion.getValuacionHgs().getValuadorNombre())){
						dto.setNombreValuador(valuacion.getValuacionHgs().getValuadorNombre());
					}
				}
			}		
			dto.setNumOrdenCompra(ordenCompra.getId());	
			if(null!=ordenCompra.getIdTercero()){
				TerceroRCVehiculo trc = (TerceroRCVehiculo) this.obtenerTercero( TerceroRCVehiculo.class,ordenCompra.getIdTercero() );
				if ( trc != null ){
					dto.setEsResponsabilidadCivil(true);
				}
				TerceroDanosMateriales tdm = (TerceroDanosMateriales) this.obtenerTercero( TerceroDanosMateriales.class,ordenCompra.getIdTercero() );
				if( tdm != null ){
					dto.setEsDanosMateriale(true);
				}
				if(dto.isEsDanosMateriale() ||dto.isEsResponsabilidadCivil()){ 
					ReporteCabina reporte = this.entidadDao.findById(ReporteCabina.class, ordenCompra.getReporteCabina().getId());
					Map<String,Object> datosVehiculo =hgsService.obtenerDatosVehiculo(reporte, ordenCompra.getIdTercero());
					if( datosVehiculo.get("reporteVehiculo") != null && datosVehiculo.get("reporteVehiculo") instanceof Vehiculo){
						Vehiculo vehiculo = (Vehiculo) datosVehiculo.get("reporteVehiculo");
						dto.setMarcaVehiculo (vehiculo.getMarca() );
						dto.setModeloVehiculo( vehiculo.getModelo());
						dto.setTipoVehiculo  ( vehiculo.getTipo() );

					}
				}

				/*TerceroRCVehiculo trc = (TerceroRCVehiculo) this.obtenerTercero( TerceroRCVehiculo.class,ordenCompra.getIdTercero() );
				if ( trc != null ){
					dto.setMarcaVehiculo ( trc.getMarca() );
					dto.setModeloVehiculo( trc.getModeloVehiculo().toString() );
					//reporteVehiculo.setSerie ( trc.getNumeroSerie() );
					//reporteVehiculo.setPlacas( trc.getPlacas() );
					dto.setTipoVehiculo  ( trc.getEstiloVehiculo() );
					dto.setEsResponsabilidadCivil(true);
				}else{
					TerceroDanosMateriales tdm = (TerceroDanosMateriales) this.obtenerTercero( TerceroDanosMateriales.class,ordenCompra.getIdTercero() );

					if( tdm != null ){
						dto.setEsDanosMateriale(true);
					}
					// # BUSCAR MARCA
					dto.setMarcaVehiculo ( this.buscaMarca( ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getMarcaId() ) );
					dto.setModeloVehiculo( ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getModeloVehiculo().toString() );
					//reporteVehiculo.setSerie ( ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie() );
					//reporteVehiculo.setPlacas( ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getPlaca() );
					dto.setTipoVehiculo ( ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getEstiloId() );
				}*/
			}
			lstOrdenCompraRecuperacionDTO.add(dto);
		}
		return lstOrdenCompraRecuperacionDTO;
			}


	public String getCoberturaCompuesta(String coberturaCompuesta) {
		StringBuilder result = new StringBuilder();
		if(StringUtils.isNotEmpty(coberturaCompuesta)){
			StringTokenizer st = new StringTokenizer(coberturaCompuesta.trim(), ",");
			while (st.hasMoreTokens()) {
				result.append("'").append(st.nextToken()).append("'");
				if (st.hasMoreTokens()) {
					result.append(",");
				}
			}
		}
		return result.toString();
	}

	public String getPasesAtencion(List<Long> pasesAtencionId) {
		StringBuilder result = new StringBuilder();
		if (null!= pasesAtencionId && !pasesAtencionId.isEmpty()){
			Iterator<Long> iterator = pasesAtencionId.iterator();
			while (iterator.hasNext()) {		 
				result.append("'").append(iterator.next()).append("'");
				if (iterator.hasNext()) {
					result.append(",");
				}
			}
		}
		return result.toString();
	}

	public Object obtenerTercero( Class c,Long keyIdTercero ){
		try{
			return this.entidadDao.findById( c , keyIdTercero);
		}catch(Exception e){

			return null;
		}
	}

	

	
	public BigDecimal obtenerTotalesOrdenesCompra(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo, Date fechaCreacionUltimoMovimiento, String tipoOrdenCompra, String estatus) {
		final StringBuilder sb 				= new StringBuilder();
		final Map<Integer, Object> params = new HashMap<Integer, Object>();
		
		sb.append(" SELECT SUM(detalle.costo_unitario) ");
		sb.append(" FROM MIDAS.TOORDENCOMPRA ordencompra ");
		sb.append(" INNER JOIN midas.TOESTIMACIONCOBERTURAREPCAB estimacion ");
		sb.append("   on (estimacion.ID = ordencompra.ID_TERCERO_AFECTADO) ");
		sb.append(" INNER JOIN midas.todetalleordencompra detalle ");
		sb.append("   on (detalle.id_orden_compra = ordencompra.id) ");
		sb.append(" INNER JOIN midas.TOCOBERTURAREPORTECABINA cobertura ");
		sb.append("   on (cobertura.id = estimacion.COBERTURAREPORTECABINA_ID) ");
		sb.append(" WHERE ordencompra.fecha_creacion is not null ");
		
		int i = 1;
		if (reporteCabinaId != null) {
			sb.append(" AND ordenCompra.REPORTE_CABINA_ID = ?").append(i).append(" ");
			params.put(i++, reporteCabinaId);
		}

		if (coberturaId != null) {
			sb.append(" AND estimacion.COBERTURAREPORTECABINA_ID = ?").append(i).append(" ");
			params.put(i++, coberturaId);
		}
		
		if (estimacionId != null) {
			sb.append(" AND estimacion.id = ?").append(i).append(" ");
			params.put(i++, estimacionId);
		}

		if (StringUtils.isNotEmpty(claveTipoCalculo)) {
			sb.append(" AND cobertura.clavetipocalculo = ?").append(i).append(" ");
			params.put(i++, claveTipoCalculo);
		}
		
		if(StringUtils.isNotEmpty(claveSubCalculo)) {
			sb.append(" AND estimacion.CLAVE_SUB_TIPO_CALCULO = ?").append(i).append(" ");
			params.put(i++, claveSubCalculo);
		}
		
		if (StringUtils.isNotEmpty(tipoOrdenCompra)) {
			sb.append(" AND ordenCompra.tipo = ?").append(i).append(" ");
			params.put(i++, tipoOrdenCompra);
		}	

		if (StringUtils.isNotEmpty(estatus)) {
			sb.append(" AND ordenCompra.estatus = ?").append(i).append(" ");
			params.put(i++, estatus);
		}	
		
		if (fechaCreacionUltimoMovimiento != null) {
			sb.append(" AND TRUNC(ordenCompra.fecha_creacion) >= ?").append(i).append(" ");
			params.put(i++, fechaCreacionUltimoMovimiento);
		}
		
		System.out.println(sb);

		final Query query = entityManager.createNativeQuery(sb.toString());
		for (int key : params.keySet()) {
			query.setParameter(key, params.get(key));
		}	
		
		return (BigDecimal)query.getSingleResult();
	}
	
	
	@Override
	public OrdenCompra obtenerOrdenCompra(Long keyOrdenCompra) {
		 
		OrdenCompra ordenCompra = null;
		
		StringBuilder queryString = new StringBuilder("SELECT ordenCompra from OrdenCompra ordenCompra where ordenCompra.id = :id ");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("id", keyOrdenCompra );
		ordenCompra = (OrdenCompra) query.getSingleResult();
		
		return ordenCompra;
		
	}
	
	@Override
	public List<BandejaOrdenCompraDTO> buscarOrdenesCompra(
			BandejaOrdenCompraDTO filtro) {
		Query query = createQueryBandejaOrdenes(filtro, false);
		if(filtro.getPosStart() != null){
			query.setFirstResult(filtro.getPosStart());
		}
		if(filtro.getCount() != null){
			query.setMaxResults(filtro.getCount());
		}
		return query.getResultList();
	}
	
	@Override
	public Long contarOrdenesCompraBandeja(BandejaOrdenCompraDTO filtro){	
		Query query = createQueryBandejaOrdenes(filtro, true);
		return ((Long)query.getSingleResult());		
	}
	
	
	@SuppressWarnings("rawtypes")
	private Query createQueryBandejaOrdenes( BandejaOrdenCompraDTO filtro, boolean isCount){
		Query query 							= null;
		StringBuilder queryString 				= new StringBuilder("");
		Map<String, Object> parameters 			= new HashMap<String, Object>();
		
	
		if(isCount){
			queryString.append(" SELECT COUNT(compra.id) ");
		}else{ 
			//busqueda de los registros
			queryString.append("SELECT new mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.BandejaOrdenCompraDTO(");
			queryString.append("siniestroCab.claveOficina, siniestroCab.consecutivoReporte, siniestroCab.anioReporte,");
			queryString.append("compra.reporteCabina.oficina.id, compra.reporteCabina.oficina.nombreOficina, "); 
			queryString.append( "func('CONCAT', siniestroCab.claveOficina, func('CONCAT', '-', func('CONCAT', siniestroCab.consecutivoReporte, func('CONCAT', '-', siniestroCab.anioReporte)))), ");
			queryString.append("cast(null as char), FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TERMINO_AJUSTE_SINIESTRO', autoIncisoRepCab.terminoAjuste), ");
			queryString.append("compra.id, FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_PAGOORDEN_COMPRA', compra.tipoPago), compra.nomBeneficiario, ");
			queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_PERSONA', compra.tipoPersona), compra.idBeneficiario, ");
			queryString.append("(select tipoPrestador.descripcion FROM ").append(TipoPrestadorServicio.class.getSimpleName()).append(" tipoPrestador where tipoPrestador.nombre = compra.tipoProveedor), ");
			queryString.append("persona.nombre, compra.reporteCabina.id, ordenPago.id,  ");
			queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'ESTATUS_ORDEN_COMPRA', compra.estatus) , cobertura.descripcion,");
			queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_ORDEN_PAGO', ordenPago.tipoPago), cast(null as char), ");
			queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_ORDEN_COMPRA', compra.tipo) , compra.tipoPago,  ");
			queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_PAGOORDEN_COMPRA', compra.tipoPago), compra.tipo, ");
			queryString.append("ordenPago.fechaPago, ordenPago.origenPago,  ordenPago.fechaCreacion, compra.fechaCreacion, ");
			queryString.append("compra.curp, compra.rfc, (select banco.nombreBanco from ").append(BancoEmisorDTO.class.getSimpleName()).append(" banco where banco.idBanco = coalesce(compra.bancoId,0)), ");
			queryString.append("compra.clabe, compra.correo, compra.telefono, ordenPago.fechaActualizacionSeycos,  ");
			queryString.append("compra, compra.reporteCabina.siniestroCabina, ordenPago.motivoCancelacion, ");
			queryString.append("ordenPago.factura, ordenPago.comentarios,  ordenPago.fechaCancelacion, ordenPago.codigoUsuarioCancela , ");
			queryString.append( "func('CONCAT', compra.reporteCabina.claveOficina, func('CONCAT', '-', func('CONCAT', compra.reporteCabina.consecutivoReporte, func('CONCAT', '-', compra.reporteCabina.anioReporte)))), ");
			queryString.append( "FUNC('MIDAS.PKGSIN_ORDENCOMPRA.GET_IMPORTE_ORDEN_COMPRA',compra.id ,'IMPORTE') ");
			queryString.append(") ");			
		}		
		queryString.append("FROM ").append(OrdenCompra.class.getSimpleName());
		queryString.append(" compra LEFT JOIN ").append(PrestadorServicio.class.getSimpleName()).append(" proveedor ON compra.idBeneficiario = proveedor.id ");
		queryString.append(" LEFT JOIN proveedor.personaMidas persona ");
		queryString.append(" LEFT JOIN compra.ordenPago ordenPago ");
		queryString.append(" LEFT JOIN compra.coberturaReporteCabina cobRepCabina ");
		queryString.append(" LEFT JOIN cobRepCabina.coberturaDTO cobertura ");
		queryString.append(" LEFT JOIN compra.reporteCabina.siniestroCabina siniestroCab ");
		queryString.append(" LEFT JOIN compra.reporteCabina.seccionReporteCabina seccionRepCab ");
		queryString.append(" LEFT JOIN seccionRepCab.incisoReporteCabina incisoRepCab ");
		queryString.append(" LEFT JOIN incisoRepCab.autoIncisoReporteCabina autoIncisoRepCab ");
		queryString.append(" WHERE  compra.estatus IN ('").append(OrdenCompraService.ESTATUS_AUTORIZADA).append("', '")
		.append(OrdenCompraService.ESTATUS_ASOCIADA).append("', '")
		.append(OrdenCompraService.ESTATUS_PAGADA)
		.append("', '").append(OrdenCompraService.ESTATUS_CANCELADA)
		.append("', '").append(OrdenCompraService.ESTATUS_RECHAZADA)
		.append("', '").append(OrdenCompraService.ESTATUS_TRAMITE).append("') ");		
		
		if(null!=filtro ){
			JpaUtil.addParameter(queryString, "compra.reporteCabina.claveOficina", filtro.getClaveReporte()  , "claveOficina", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, "compra.reporteCabina.consecutivoReporte", filtro.getConseReporte()  , "consecutivoReporte", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, "compra.reporteCabina.anioReporte", filtro.getAnioReporte()  , "anioReporte", parameters, OperationType.LIKE.toString());
			
			//numero siniestro
			//JpaUtil.addParameter(queryString, modelOrdenCompra + "estatus", OrdenCompraService.ESTATUS_AUTORIZADA  , "estatusOrden", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, "compra.reporteCabina.siniestroCabina.claveOficina", filtro.getClaveOficinaSiniestro()  , "claveOficinaSin", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, "compra.reporteCabina.siniestroCabina.consecutivoReporte", filtro.getConsecutivoReporteSiniestro()  , "consecutivoReporteSin", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, "compra.reporteCabina.siniestroCabina.anioReporte", filtro.getAnioReporteSiniestro()  , "anioReporteSin", parameters, OperationType.LIKE.toString());
			//oficina
			JpaUtil.addParameter(queryString, "compra.reporteCabina.oficina.id", filtro.getOficinaId() , "oficinaId", parameters, JpaUtil.EQUAL );
				//estatus
				JpaUtil.addParameter(queryString, "compra.estatus", filtro.getEstatus()  , "estatus", parameters, OperationType.LIKE.toString());
				//id orden pago
				//JpaUtil.addParameter(queryString, "compra.ordenPago.id", filtro.getId() , "ordenPagoId", parameters, JpaUtil.EQUAL );
			//proveedor
			JpaUtil.addParameter(queryString, "compra.idBeneficiario", filtro.getIdBeneficiario() , "proveedor", parameters, JpaUtil.EQUAL );
			
			JpaUtil.addParameter(queryString, "compra.tipoProveedor", filtro.getTipoProveedor() , "tipoproveedor", parameters, OperationType.LIKE.toString() );
			//servicio particular
			
			if((null!=filtro.getServParticular() && null!=filtro.getServPublico() )  &&(filtro.getServParticular() && filtro.getServPublico())){
				JpaUtil.addParameter(queryString, "compra.reporteCabina.oficina.tipoServicio", SERV_AMBOS, "tipoServicio1", parameters, JpaUtil.EQUAL);
			}else if( (null!=filtro.getServParticular())&&filtro.getServParticular()){
				JpaUtil.addParameter(queryString, "compra.reporteCabina.oficina.tipoServicio", SERV_PARTICULAR, "tipoServicio2", parameters, JpaUtil.EQUAL);
			}else if( (null!=filtro.getServPublico() ) &&filtro.getServPublico()){
				JpaUtil.addParameter(queryString, "compra.reporteCabina.oficina.tipoServicio", SERV_PUBLICO, "tipoServicio3", parameters, JpaUtil.EQUAL);
			}	
			//tipo
			JpaUtil.addParameter(queryString, "compra.tipoPago", filtro.getTipoPago() , "tipoOc", parameters, OperationType.LIKE.toString());
			//Tipo Pago Orden Compra
			JpaUtil.addParameter(queryString, "compra.tipo", filtro.getTipoPagoOrdenCompra() , "tipopagoOc", parameters, OperationType.LIKE.toString());
			//termino ajuste
			JpaUtil.addParameter(queryString, "compra.reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.terminoAjuste", filtro.getTerminoAjuste() , "termino", parameters, OperationType.LIKE.toString());
			//id orden compra
			JpaUtil.addParameter(queryString, "compra.id", filtro.getNumOrdenCompra() , "ordenCompraId", parameters, JpaUtil.EQUAL );
			//beneficiario
			JpaUtil.addParameterLike(queryString, "compra.nomBeneficiario", filtro.getBeneficiario() , "beneficiario", parameters);
		}
		
		//Por motivo de la paginacion y carga dinamica de dhtmlx se requiere hacer un ordenamiento manual
		if(!isCount){
			if(StringUtils.isEmpty(filtro.getOrderByAttribute())){			
				queryString.append(" ORDER BY compra.fechaCreacion  DESC");
			}else{
				queryString.append(" ORDER BY ");
				String direction = filtro.getOrderByAttribute().contains("asc") ? "ASC" : "DESC";		
				if(filtro.getOrderByAttribute().contains("numSiniestroCabina")){
					queryString.append( "func('CONCAT', siniestroCab.claveOficina, func('CONCAT', '-', func('CONCAT', siniestroCab.consecutivoReporte, func('CONCAT', '-', siniestroCab.anioReporte)))) ")
						.append(direction);	
				}else if(filtro.getOrderByAttribute().contains("numReporteCabina")){
					queryString.append( "func('CONCAT', '-', func('CONCAT', compra.reporteCabina.consecutivoReporte, func('CONCAT', '-', compra.reporteCabina.anioReporte))) ")
					.append(direction);	
				}else if(filtro.getOrderByAttribute().contains("terminoAjusteSiniestro")){
					queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TERMINO_AJUSTE_SINIESTRO', autoIncisoRepCab.terminoAjuste) ")
						.append(direction);
				}else if(filtro.getOrderByAttribute().contains("totalOrdenCompra")){
					queryString.append("FUNC('MIDAS.PKGSIN_ORDENCOMPRA.GET_IMPORTE_ORDEN_COMPRA',compra.id ,'IMPORTE') ")
						.append(direction);
				}else if(filtro.getOrderByAttribute().contains("tipoPago")){
					queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_ORDEN_COMPRA', compra.tipo) ").append(direction);
				}else if(filtro.getOrderByAttribute().contains("tipoOrdenCompra")){
					queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'TIPO_PAGOORDEN_COMPRA', compra.tipoPago) ").append(direction);
				}else if(filtro.getOrderByAttribute().contains("compra.estatus")){
					queryString.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'ESTATUS_ORDEN_COMPRA', compra.estatus) ").append(direction);
				}else if(filtro.getOrderByAttribute().contains("noAplica")){				
					queryString.append(" compra.fechaCreacion ").append(direction);
				}else{
					queryString.append(filtro.getOrderByAttribute());
				}	
			}
		}
							
		query = entityManager.createQuery(queryString.toString());		
		JpaUtil.setQueryParametersByProperties(query, parameters);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
	}
	
	
	public BigDecimal obtenerMontoOrdenCompra(Long ordenCompraId, String columna){
		
		String queryStr =  " select MIDAS.PKGSIN_ORDENCOMPRA.GET_IMPORTE_ORDEN_COMPRA(?,?) from dual  ";
		Query query = entityManager.createNativeQuery(queryStr);
		query.setParameter(1, ordenCompraId);
		query.setParameter(2, columna);
		BigDecimal monto = ((BigDecimal) query.getSingleResult());
		return monto == null ? BigDecimal.ZERO : monto;
	}
	
	
	@Override
	public  OrdenCompra obtenerOrdenDeCompraExistente( Long estimacionId, BigDecimal subtotal, String tipoPago, String beneficiario, Long prestadorId, Boolean validaExiteOC  ){
		
		
		OrdenCompra ordenCompraExistente = null;
		if(validaExiteOC){
			BigDecimal tolerancia = new BigDecimal(1);
			StringBuilder queryString = new StringBuilder("SELECT ordenCompra from OrdenCompra ordenCompra ");
			queryString.append("where ordenCompra.idTercero = :idTercero ");
			queryString.append("and ordenCompra.tipoPago = :tipoPago ");
			queryString.append("and ordenCompra.estatus in ('"+OrdenCompraService.ESTATUS_AUTORIZADA+"' , '"+OrdenCompraService.ESTATUS_PAGADA+"' , ' "+OrdenCompraService.ESTATUS_TRAMITE+" ' , '"+OrdenCompraService.ESTATUS_ASOCIADA+"' ) ");
			if(tipoPago.equals("PB")){
				queryString.append("and ordenCompra.nomBeneficiario = :nomBeneficiario");
			}else{
				queryString.append("and ordenCompra.idBeneficiario = :idBeneficiario");
			}
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("idTercero"           , estimacionId  );
			query.setParameter("tipoPago"          , tipoPago );
			if(tipoPago.equals("PB")){
				query.setParameter("nomBeneficiario" , beneficiario);
			}else{
				query.setParameter("idBeneficiario" , prestadorId);
			}
			List<OrdenCompra>  ordenesCompraList = query.getResultList();
			if(!ordenesCompraList.isEmpty()){
					for(OrdenCompra ordenCompra : ordenesCompraList){
						BigDecimal subTotalActual = this.obtenerMontoOrdenCompra(ordenCompra.getId(),"COSTO_UNITARIO");
						//SI NO TIENE EL MISMO MONTO ENTONCES ES ORDEN DE COMPRA DIFERENTE
						if(subTotalActual.subtract(tolerancia).compareTo(subtotal)<=0 && subtotal.compareTo(subTotalActual.add(tolerancia))<=0){
							ordenCompraExistente = ordenCompra;
						}
					}
			}
		}else{
			StringBuilder queryString = new StringBuilder("SELECT ordenCompra from OrdenCompra ordenCompra ");
			queryString.append("where ordenCompra.idTercero = :idTercero ");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("idTercero"           , estimacionId  );
			List<OrdenCompra>  ordenesCompraList = query.getResultList();
			if(!ordenesCompraList.isEmpty()){
				ordenCompraExistente = ordenesCompraList.get(0);
			}
		}
		return ordenCompraExistente;
	}
	

}
