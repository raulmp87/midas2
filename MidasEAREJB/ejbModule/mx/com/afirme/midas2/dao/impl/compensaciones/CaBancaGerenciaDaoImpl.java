/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaGerenciaDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaGerencia;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Stateless
public class CaBancaGerenciaDaoImpl implements CaBancaGerenciaDao {
	
	public static final String ID = "nombre";
	public static final String CABANCACONFIGURACION_ID = "configuracionBancaId";
	public static final String ID_EMPRESA = "idEmpresa";
	public static final String ID_GERENCIA = "idGerencia";
	public static final String FSIT = "fsit";
	
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = LoggerFactory.getLogger(CaBancaGerenciaDaoImpl.class);
	
	public void save(CaBancaGerencia entity) {	
		try {
			LOG.info(">> save()");
			entityManager.persist(entity);
			LOG.info("<< save()");
        } catch (RuntimeException re) {     
        	LOG.error("Información del Error", re);
            throw re;
        }
	}
	
    public void delete(CaBancaGerencia entity) {
    	try {
    		LOG.info(">> delete()");
    		entity = entityManager.getReference(CaBancaGerencia.class, entity.getId());
    		LOG.info("<< delete()");
		    entityManager.remove(entity);
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
		    throw re;
		}
    }
    
    public CaBancaGerencia update(CaBancaGerencia entity) {    
    	try {
    		LOG.info(">> update()");
            CaBancaGerencia result = entityManager.merge(entity);
            LOG.info("<< update()");
            return result;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        	throw re;
        }
    }
    
    public CaBancaGerencia findById( Long id) {
    	CaBancaGerencia instance = null;
    	try {
    		LOG.info(">> findById()");
    		instance = entityManager.find(CaBancaGerencia.class, id);
    		LOG.info("<< findById()");
    	} catch (RuntimeException re) {    		
    		LOG.error("Información del Error", re);
    	}
    	return instance;
    }

    @SuppressWarnings("unchecked")
    public List<CaBancaGerencia> findByProperty(String propertyName, final Object value) {
    	LOG.info(">> findByProperty()");
		try {
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaBancaGerencia model ");
			queryString.append("  WHERE model.").append(propertyName).append(" = :propertyValue");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("propertyValue", value);		
			LOG.info("<< findByProperty()");
			return query.getResultList();
		} catch (RuntimeException re) {		
			LOG.error("Información del Error", re);
			return null;
		}
	}			

    @SuppressWarnings("unchecked")
	public List<CaBancaGerencia> findAll() {	
		LOG.info(">> findAll()");
		List<CaBancaGerencia> list = null;
		try {
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaBancaGerencia model ");
			Query query = entityManager.createQuery(queryString.toString());
			list = query.getResultList();			
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			list = new ArrayList<CaBancaGerencia>();
		}
		LOG.info("<< findAll()");
		return list;
	}
}