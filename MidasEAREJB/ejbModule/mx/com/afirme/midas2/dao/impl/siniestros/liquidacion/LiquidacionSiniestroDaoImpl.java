package mx.com.afirme.midas2.dao.impl.siniestros.liquidacion;



import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.liquidacion.LiquidacionSiniestroDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro.EstatusLiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros.ParametroAutorizacionLiquidacion;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal.EstatusDocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.DetalleOrdenExpedicionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.ImpresionOrdenExpedicionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.LiquidacionSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionRegistro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService.LiquidacionSiniestroFiltro;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class LiquidacionSiniestroDaoImpl extends EntidadDaoImpl implements LiquidacionSiniestroDao{
	@EJB
	EntidadDao entidadDao;
	
	private static final Logger LOG = Logger.getLogger(LiquidacionSiniestroDaoImpl.class);
	
	@SuppressWarnings("unchecked")
	@Override
	public ImpresionOrdenExpedicionDTO getDatosImportesLiquidacionProv(Long idLiquidacion){
		ImpresionOrdenExpedicionDTO datosEimportesPDF = new ImpresionOrdenExpedicionDTO();
		String spName = "MIDAS.PKG_LIQUIDACIONES_SINIESTROS.getInfoOrdenExpChequeProv"; 
		StoredProcedureHelper spHelper = null;
		
		try{
			String columnaBd = "SUBTOTAL,DESCUENTO,IVA,IVARET,ISR,NETOXPAGAR,AFAVOR,TELEFONO,CORREO,CONCEPTO,FECHASOLICITUD,LOCALIDAD,SOLICITADOPOR,NUMLIQ,MIZAR,TIPO,CUENTA,TIPOOPERACION,CHEQUEREF,SOLICITA,FECHAPAGO,BANCO";
			String atributos = "subtotal,descuento,ivaAcred,ivaRet,isr,aPagar,aFavor,telefono,correo,porConcepto,fechaSolicitud,localidad,usuarioSolicita,liquidacionEgreso,solicitudChequeMizar,tipo,cuenta,tipoOperacion,cheque,nombreCompletoUsuarioSolicita,fechaElaboracionCheque,banco";
			
			//String columnaBd = "SUBTOTAL,DESCUENTO,IVA,IVARET,ISR,NETOXPAGAR,AFAVOR,TELEFONO,CORREO";
			//String atributos = "subtotal,descuento,ivaAcred,ivaRet,isr,aPagar,aFavor,telefono,correo";
						
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(ImpresionOrdenExpedicionDTO.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("pIdLiquidacion", idLiquidacion);
			datosEimportesPDF = (ImpresionOrdenExpedicionDTO)spHelper.obtieneResultadoSencillo();
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		return datosEimportesPDF;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DetalleOrdenExpedicionDTO> getResumenLiquidacionProveedor(Long idLiquidacion){
		List<DetalleOrdenExpedicionDTO> resumenLiqProv = new ArrayList<DetalleOrdenExpedicionDTO>();
		String spName = "MIDAS.PKG_LIQUIDACIONES_SINIESTROS.getResumeLiquidacionProv"; 
		StoredProcedureHelper spHelper = null;
		try{
			String columnaBd = "SINIESTRO,ORDENPAGO,CONCEPTO,TERMINOAJUSTE,FACTURA,SUBTOTAL,IVA,IVARET,ISR,APAGAR";
			String atributos = "siniestro,ordenPago,concepto,terminoAjuste,factura,subtotal,ivaAcred,ivaRet,isrRet,aPagar";
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(DetalleOrdenExpedicionDTO.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("pIdLiquidacion", idLiquidacion);
			
			resumenLiqProv = spHelper.obtieneListaResultados();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return resumenLiqProv;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LiquidacionSiniestroRegistro> buscarLiquidacionDetalle(LiquidacionSiniestroFiltro filtro){
		List<LiquidacionSiniestroRegistro> lista  = new ArrayList<LiquidacionSiniestroRegistro>();
		String spName = "MIDAS.PKG_LIQUIDACIONES_SINIESTROS.spBusquedaLiquidaciones"; 
        StoredProcedureHelper storedHelper 			= null;
        try {
      	  	String propiedades = "idLiquidacion,oficina,estatus,tipoLiquidacion,tipoOperacion,noProveedor,proveedor,totalAPagar,"
      	  		+"solicitadaPor,fechaSolicitud,autorizadoPor,fechaDeAutorizacion,fechaDeElaboracion,noSolCheque,"
      	  		+"siniestro,factura,noDetalle,noCheque,estatusId,tipoCancelacion,nombreBeneficiario,origenLiquidacion,noLiquidacion";
      	  	
      	  	String columnasBaseDatos = "ID,NOMBRE_OFICINA,ESTATUS,TIPO_LIQUIDACION,TIPO_OPERACION,ID_PROVEEDOR,NOMBRE,NETO_POR_PAGAR,"
      	  		+"SOLICITADO_POR,FECHA_POR_AUT,AUTORIZADO_POR,FECHA_LIBERADA_CON,FECHA_CREACION,CHEQUE,"
      	  		+"SINIESTRO,FOLIO_FACTURA,NO_DETALLE,NO_SOLICITUD_CHEQUE,ESTATUS_ID,TIPOCANCELACION,BENEFICIARIO,ORIGEN_LIQUIDACION,NO_LIQUIDACION";

            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(LiquidacionSiniestroRegistro.class.getCanonicalName(), propiedades, columnasBaseDatos);
            agregaParametrosDelFiltro(storedHelper, filtro);
            lista = storedHelper.obtieneListaResultados();
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en busqueda de liquidaciones ..."+descErr
                                           , Level.WARNING, e);    
             }
        }
		return lista;
	}
	
	private Object convertToNull(Object obj){
		if( obj instanceof String){
			obj = (obj != null && ((String)obj).equals(""))?null:obj;
		}else if ( obj instanceof Boolean){
			obj = (obj != null && ((Boolean)obj)== Boolean.FALSE)?null:obj;
		}
		return obj;
	}
	
	private void agregaParametrosDelFiltro(StoredProcedureHelper storedHelper,LiquidacionSiniestroFiltro filtro){
		storedHelper.estableceParametro("pOficinaId", this.convertToNull(filtro.getOficinaId()));
		storedHelper.estableceParametro("pEstatus",this.convertToNull( filtro.getEstatusLiquidacion()));
		storedHelper.estableceParametro("pTipoLiquidacion",this.convertToNull( filtro.getTipoLiquidacion()));
		storedHelper.estableceParametro("pTipoOperacion",this.convertToNull( filtro.getTipoOperacion()));
		storedHelper.estableceParametro("pNoLiquidacion",this.convertToNull( filtro.getNoLiquidacion()));
		storedHelper.estableceParametro("pNoProveedor",this.convertToNull( filtro.getNoProveedor()));
		storedHelper.estableceParametro("pNombreProveedor",this.convertToNull( filtro.getNombreProveedor()));
		storedHelper.estableceParametro("pNoSiniestro",this.convertToNull( filtro.getNoSiniestro()));
		storedHelper.estableceParametro("pFolioFactura",this.convertToNull( filtro.getFolioFactura()));
		storedHelper.estableceParametro("pSolicitadoPor",this.convertToNull( filtro.getSolicitadoPor()));
		storedHelper.estableceParametro("pNumeroChequeReferencia",this.convertToNull( filtro.getNumeroChequeReferencia()));
		storedHelper.estableceParametro("pAutorizadoPor",this.convertToNull( filtro.getAutorizadoPor()));
		storedHelper.estableceParametro("pTipoServicio",this.convertToNull( filtro.getTipoServicio()));
		
		storedHelper.estableceParametro("pCondicionMontoTotal",this.convertToNull( filtro.getCondicionMontoTotal()));
		storedHelper.estableceParametro("pEsRangoMontoTotal",this.convertToNull( filtro.getEsRangoMontoTotal()));
		storedHelper.estableceParametro("pMontoTotalDe",this.convertToNull( filtro.getMontoTotalDe()));
		storedHelper.estableceParametro("pMontoTotalHasta",this.convertToNull( filtro.getMontoTotalHasta()));
		
		storedHelper.estableceParametro("pCondicionFechaSolicitud",this.convertToNull( filtro.getCondicionFechaSolicitud()));
		storedHelper.estableceParametro("pEsRangoFechaSolicitud",this.convertToNull( filtro.getEsRangoFechaSolicitud()));
		storedHelper.estableceParametro("pFechaSolicitudDe",this.convertToNull( filtro.getFechaSolicitudDe()));
		storedHelper.estableceParametro("pFechaSolicitudHasta",this.convertToNull( filtro.getFechaSolicitudHasta()));
		
		
		storedHelper.estableceParametro("pCondicionFechaAutorizacion",this.convertToNull( filtro.getCondicionFechaAutorizacion()));
		storedHelper.estableceParametro("pEsRangoFechaAutorizacion",this.convertToNull( filtro.getEsRangoFechaAutorizacion()));
		storedHelper.estableceParametro("pFechaAutorizacionDe",this.convertToNull( filtro.getFechaAutorizacionDe()));
		storedHelper.estableceParametro("pFechaAutorizacionHasta",this.convertToNull( filtro.getFechaAutorizacionHasta()));
		
		storedHelper.estableceParametro("pCondicionFechaElaboracion",this.convertToNull( filtro.getCondicionFechaElaboracion()));
		storedHelper.estableceParametro("pEsRangoFechaElaboracion",this.convertToNull( filtro.getEsRangoFechaElaboracion()));
		storedHelper.estableceParametro("pFechaElaboracionDe",this.convertToNull( filtro.getFechaElaboracionDe()));
		storedHelper.estableceParametro("pFechaElaboracionHasta",this.convertToNull( filtro.getFechaElaboracionHasta()));
		
		storedHelper.estableceParametro("pSolicitudCheque",this.convertToNull( filtro.getNoSolicitudCheque() ));
		
		storedHelper.estableceParametro("pNombreBeneficiario",this.convertToNull( filtro.getNombreBeneficiario() ));
		storedHelper.estableceParametro("pOrigenLiquidacion",this.convertToNull( filtro.getOrigenLiquidacion() ));
	}
	
	private void agregaParametrosDelFiltro2(StoredProcedureHelper storedHelper,LiquidacionSiniestroFiltro filtro){
		storedHelper.estableceParametro("pOficinaId", null);
		storedHelper.estableceParametro("pEstatus", null);
		storedHelper.estableceParametro("pTipoLiquidacion", null);
		storedHelper.estableceParametro("pTipoOperacion", null);
		storedHelper.estableceParametro("pNoLiquidacion", null);
		storedHelper.estableceParametro("pNoProveedor", null);
		storedHelper.estableceParametro("pNombreProveedor", null);
		storedHelper.estableceParametro("pNoSiniestro", null);
		storedHelper.estableceParametro("pFolioFactura", null);
		storedHelper.estableceParametro("pSolicitadoPor", null);
		storedHelper.estableceParametro("pNumeroChequeReferencia", null);
		storedHelper.estableceParametro("pAutorizadoPor", null);
		storedHelper.estableceParametro("pTipoServicio", null);
		
		storedHelper.estableceParametro("pCondicionMontoTotal", null);
		storedHelper.estableceParametro("pEsRangoMontoTotal", null);
		storedHelper.estableceParametro("pMontoTotalDe", null);
		storedHelper.estableceParametro("pMontoTotalHasta", null);
		
		storedHelper.estableceParametro("pCondicionFechaSolicitud", null);
		storedHelper.estableceParametro("pEsRangoFechaSolicitud", null);
		storedHelper.estableceParametro("pFechaSolicitudDe", null);
		storedHelper.estableceParametro("pFechaSolicitudHasta", null);
		
		storedHelper.estableceParametro("pCondicionFechaAutorizacion", null);
		storedHelper.estableceParametro("pEsRangoFechaAutorizacion", null);
		storedHelper.estableceParametro("pFechaAutorizacionDe", null);
		storedHelper.estableceParametro("pFechaAutorizacionHasta", null);
		
		storedHelper.estableceParametro("pCondicionFechaElaboracion", null);
		storedHelper.estableceParametro("pEsRangoFechaElaboracion", null);
		storedHelper.estableceParametro("pFechaElaboracionDe", null);
		storedHelper.estableceParametro("pFechaElaboracionHasta", null);
		
		storedHelper.estableceParametro("pSolicitudCheque", null);
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoFiscal> buscarFacturasLiquidacion(Long idLiquidacion)
	{
		List<DocumentoFiscal> listaResultados = new ArrayList<DocumentoFiscal>();
		Query query;
		
		String queryStr = "SELECT new mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal(" +
				"factura.id, factura.numeroFactura, factura.estatus, factura.fechaCreacion, " +
				"factura.iva, factura.ivaRetenido, factura.isr, factura.subTotal, " +
				"factura.montoTotal, SELECT FUNC('TO_CHAR',COUNT(c.id)) FROM ConjuntoOrdenCompraNota c WHERE c.factura.id= factura.id,COALESCE(factura.nombreAgrupador,'SIN_AGRUPADOR'),FUNC('MIDAS.PKGSIN_ORDENCOMPRA.getDetalleFactura',factura.id))  " +
				" FROM " +  
				" LiquidacionSiniestro liquidacion" +
				" JOIN liquidacion.facturas factura " +
				" WHERE " +
				" liquidacion.id = " + idLiquidacion +
				" ORDER BY factura.numeroFactura";  
		
		query = entityManager.createQuery(queryStr);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		listaResultados = query.getResultList();
		
		return listaResultados;				
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FacturaLiquidacionDTO> buscarOrdenesPago(Long idLiquidacion)
	{
		List<FacturaLiquidacionDTO> listaResultados = new ArrayList<FacturaLiquidacionDTO>();
		Query query;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		
		String queryStr = "SELECT new mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO(factura.id, " +
				"factura.numeroFactura, siniestro, ordenPago)" +
				" FROM " +
				" LiquidacionSiniestro liquidacion, " +							
				" JOIN liquidacion.detalleLiquidacionSiniestro detalleLiquidacion " +
				" JOIN detalleLiquidacion.ordenCompra ordenCompra" +
				" JOIN detalleLiquidacion.ordenPagoSiniestro ordenPago" +
				" JOIN ordenCompra.facturasSiniestroList factura " +
				" JOIN ordenCompra.ReporteCabina reporteCabina " +
				" JOIN reporteCabina.siniestroCabina siniestro" +
				" WHERE " +
				" liquidacion.id = :idLiquidacion " +								
				" AND factura.estatus in (:estatusRegistrada, :estatusPagada)" +
				" AND ordenPago.estatus in(:estatusAsociada, estatusOPagada')";
		
		Utilerias.agregaHashLista(listaParametrosValidos, "idLiquidacion", idLiquidacion);
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistrada", EstatusDocumentoFiscal.REGISTRADA.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusPagada", EstatusDocumentoFiscal.PAGADA.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusAsociada", PagosSiniestroService.ESTATUS_ASOCIADA);
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusOPagada", PagosSiniestroService.ESTATUS_PAGADA);
		
		query = entityManager.createQuery(queryStr);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		listaResultados = query.getResultList();
		
		return listaResultados;
	}
	
	@Override
	public Long getNumeroLiquidacion() 
	{
		String queryString = "SELECT MIDAS.FOLIO_LIQ_SINIESTROS_SEQ.NEXTVAL FROM DUAL";
		
		Query query = entityManager.createNativeQuery(queryString);
		Object result = query.getSingleResult();
						
		return ((BigDecimal)result).longValue();		
	}
	

	
	@SuppressWarnings("unchecked")
	@Override
	public List<PrestadorServicioRegistro> buscarProveedoresLiquidacion() {
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("esActivo", 1);  
		parameters.put("estatusLiberada", PagosSiniestroService.ESTATUS_LIBERADA);
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro(prestador.id, prestador.personaMidas, prestador.oficina.nombreOficina ) ");
		queryString.append(" FROM " + PrestadorServicio.class.getSimpleName() + " prestador ");
		queryString.append(" JOIN "+ OrdenCompra.class.getSimpleName() +" ordenCompra ON ordenCompra.idBeneficiario = prestador.id ");
		queryString.append(" JOIN "+ OrdenPagoSiniestro.class.getSimpleName() +" ordenPago ON ordenPago.ordenCompra.id = ordenCompra.id ");
		queryString.append(" WHERE  ordenPago.estatus = :estatusLiberada ");
		queryString.append(" AND  prestador.estatus = :esActivo ");
		queryString.append(" GROUP BY prestador.id, prestador.personaMidas, prestador.oficina.nombreOficina ");  
		List<PrestadorServicioRegistro>  prestadorRegistroList = this.executeQueryMultipleResult(queryString.toString(), parameters);
		return prestadorRegistroList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ParamAutLiquidacionRegistro> buscarParametrosAutorizacion(ParamAutLiquidacionDTO filtro) {
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("noConfiguracion", filtro.getNumero());
		parameters.put("liquidacion", filtro.getLiquidacion());
		parameters.put("tipoLiquidacion", filtro.getTipoLiquidacion());
		parameters.put("estatus", (filtro.getEstatus() == null)? null : filtro.getEstatus() == 1 ? true : false);
		parameters.put("nombreUsuarioConfigurador", (StringUtils.isEmpty(filtro.getNombreUsuarioConfigurador()))? null : "%" + filtro.getNombreUsuarioConfigurador() + "%" );
		parameters.put("tipoPago", filtro.getTipoPago());
		parameters.put("nombreConfiguracion", filtro.getNombreConfiguracion());
		//TODO PENDIENTE EL USUARIO CON QUIEN SE VA A BUSCAR
		
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionRegistro( ");
		queryString.append(" model.id, model.tipoLiquidacion, model.estatus, model.fechaCreacion, model.fechaInactivo, model.origenLiquidacion, ") 
		.append(" model.nombreConfiguracion, model.nombreUsuarioConfigurador, model.numero, model.tipoPago ) ")
		.append(" FROM " + ParametroAutorizacionLiquidacion.class.getSimpleName() + " model ")
		.append(" WHERE  (:noConfiguracion IS NULL OR model.numero = :noConfiguracion )")
		.append(" AND  ( :liquidacion IS NULL OR model.origenLiquidacion = :liquidacion  ) ")
		.append(" AND  ( :tipoLiquidacion IS NULL OR model.tipoLiquidacion = :tipoLiquidacion  ) ")
		.append(" AND  ( :estatus IS NULL OR model.estatus = :estatus  ) ")
		.append(" AND  ( :nombreUsuarioConfigurador IS NULL OR UPPER(model.nombreUsuarioConfigurador) LIKE UPPER(:nombreUsuarioConfigurador)  ) ")
		.append(" AND  ( :tipoPago IS NULL OR model.tipoPago = :tipoPago  ) ")
		.append(" AND  ( :nombreConfiguracion IS NULL OR model.nombreConfiguracion = :nombreConfiguracion  ) ");
		
		if(filtro.getCriterioFechaActivo()!=null && !filtro.getCriterioFechaActivo().equals("")){
			String operador = convierteAOperador(filtro.getCriterioFechaActivo());
			parameters.put("fechaActivoIni", filtro.getFechaActivoIni());
			queryString.append(" AND  (  FUNC('trunc', model.fechaCreacion )  "+operador+" :fechaActivoIni  ) ");
		}else if(filtro.getFechaActivoIni()!=null && filtro.getFechaActivoFin()!=null){
			parameters.put("fechaActivoIni", filtro.getFechaActivoIni());
			parameters.put("fechaActivoFin", filtro.getFechaActivoFin());
			queryString.append(" AND  (  FUNC('trunc', model.fechaCreacion )  >= :fechaActivoIni  ) ");
			queryString.append(" AND  (  FUNC('trunc', model.fechaCreacion )  <= :fechaActivoFin  ) ");
		}
		
		if(filtro.getCriterioFechaInactivo()!=null && !filtro.getCriterioFechaInactivo().equals("")){
			String operador = convierteAOperador(filtro.getCriterioFechaInactivo());
			parameters.put("fechaInactivoIni", filtro.getFechaInactivoIni());
			queryString.append(" AND  (  FUNC('trunc', model.fechaInactivo )  "+operador+" :fechaInactivoIni  ) ");
		}else if(filtro.getFechaInactivoIni()!=null && filtro.getFechaInactivoFin()!=null){
			parameters.put("fechaInactivoIni", filtro.getFechaInactivoIni());
			parameters.put("fechaInactivoFin", filtro.getFechaInactivoFin());
			queryString.append(" AND  (  FUNC('trunc', model.fechaInactivo )  >= :fechaInactivoIni  ) ");
			queryString.append(" AND  (  FUNC('trunc', model.fechaInactivo )  <= :fechaInactivoFin  ) ");
		}
		queryString.append(" ORDER BY  model.numero DESC ");
		List<ParamAutLiquidacionRegistro>  resultados = this.executeQueryMultipleResult(queryString.toString(), parameters);
		return resultados;
	}
	private String convierteAOperador(String codigoCriterio){
		String valor = "";
		if(codigoCriterio.equals("IG")){
			valor = "=";
		}else if(codigoCriterio.equals("MAY")){
			valor = ">";
		}else if(codigoCriterio.equals("MEN")){
			valor = "<";
		}else if(codigoCriterio.equals("MAQ")){
			valor = ">=";
		}else if(codigoCriterio.equals("MEQ")){
			valor = "<=";
		}
		return valor;
	}
	
	@Override
	public Long obtenerSiguienteNumeroDeLiquidacion(){
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT MAX(model.numero)	");
		queryString.append(" FROM ParametroAutorizacionLiquidacion model");
		Query query = this.entityManager.createQuery(queryString.toString());
		Object obj = query.getSingleResult();
		Long numeroSecuencia =  ((obj==null)?1L:((Long)obj)+1);
		return numeroSecuencia;
	}
	
	public Boolean validarUsuarioEsAutorizador(Long idLiquidacion, Long idUsuario, Date fechaVigencia) {
		String spName 								= "MIDAS.PKG_LIQUIDACIONES_SINIESTROS.VALIDARPARAMETROSLIQUIDACION"; 
        StoredProcedureHelper storedHelper 			= null;
        ParamAutLiquidacionRegistro resultado = new ParamAutLiquidacionRegistro();
        try {
			LogDeMidasEJB3.log("Entrando a validarUsuarioEsAutorizador..." + this, Level.INFO, null);			
            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            
            storedHelper.estableceParametro("p_liquidacionID ", idLiquidacion);
            storedHelper.estableceParametro("p_codigoUsuario ", idUsuario);
            storedHelper.estableceParametro("p_fechaVigencia ", fechaVigencia);
            
            storedHelper.estableceMapeoResultados(ParamAutLiquidacionRegistro.class.getCanonicalName(),
												"puedeAutorizar", 
												"p_puedeAutorizar");
            
            resultado = (ParamAutLiquidacionRegistro) storedHelper.obtieneResultadoSencillo();
            LogDeMidasEJB3.log("Saliendo de validarUsuarioEsAutorizador, idLiquidacion - " + idLiquidacion 
					+ this, Level.INFO, null);
            
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en VALIDAR_PARAMETROS_LIQUIDACION..."+descErr
                                           , Level.WARNING, e);
             }else{
            	 LogDeMidasInterfaz.log(
                     "Excepcion general en VALIDAR_PARAMETROS_LIQUIDACION..."
                     , Level.WARNING, e);
             }
        }
       
        if (resultado.getPuedeAutorizar() == 1) {
        	return true;
        } else {
        	return false;
        }
	}
	
	public Short cancelarLiquidacion(Long idLiquidacion, Short tipoCancelacion) {

		Short resultado = 0;
		StoredProcedureHelper storedHelper = null;

		String spNameSolicitar = "dbo.spCancelaSolicitud";
		String spNameCancelar = "MIDAS.PKG_LIQUIDACIONES_SINIESTROS.SOLICITARCANCELARLIQUIDACION";
		int respuesta = 0;
		Long idSolicitud = null;
		try {
			LiquidacionSiniestro liquidacion = entidadDao.findById(LiquidacionSiniestro.class, idLiquidacion);

			if (liquidacion.getSolicitudCheque() != null) {
				idSolicitud = liquidacion.getSolicitudCheque().getIdSolCheque();

				
				try {
					LogDeMidasInterfaz.log("Entrando a dbo.spCancelaSolicitud..." + this, Level.INFO, null);
					storedHelper = new StoredProcedureHelper(spNameSolicitar,StoredProcedureHelper.DATASOURCE_MIZAR);
					storedHelper.estableceParametro("Idsolicitud", idSolicitud);
					respuesta = storedHelper.ejecutaActualizarSQLServer();
				} catch (Exception e) {
					LogDeMidasInterfaz.log("Excepcion general en LiquidacionSiniestroDaoImpl.cancelarLiquidacion..." + this, Level.WARNING, e);
					e.printStackTrace();
					
					respuesta = -1;
				}	
				if (respuesta == -1) {
					return 1;
				} else if (respuesta > 0) {
					return 2;
				} else if (respuesta != 0){
					return 4;
				} 
				
			} 
			
			if (respuesta == 0) {
				storedHelper = new StoredProcedureHelper(spNameCancelar, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("p_liquidacionID", idLiquidacion);
				storedHelper.estableceParametro("p_tipoCancelacion", tipoCancelacion);

				respuesta = storedHelper.ejecutaActualizar();
				if (respuesta == 0) {
					return 0;
				} else if (respuesta == 1) {
					return 3;
				} else {
					return 4;
				}
			}

		} catch (Exception e) {
			resultado = 4;
		}

		return resultado;

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenPagoSiniestro> buscarOrdenesPagoIndemnizacion(Long idLiquidacion)
	{
		List<OrdenPagoSiniestro> listaResultados = new ArrayList<OrdenPagoSiniestro>();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		Query query;
		
		String queryStr = "SELECT new mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro(ordenPago.id, " +
		" siniestro," +
		" recepcionDocs.formaPago," +
		" CASE WHEN ((SELECT COUNT(indemnizacion.id) from IndemnizacionSiniestro i where i.id = (indemnizacion.id) " +     
		" 	AND i.estatusIndemnizacion = :estatusIndemnizacion " +
		" 	AND i.ordenCompra.coberturaReporteCabina.claveTipoCalculo IN (:claveDanios, :claveRobo) ) > 0)" +
        " 		THEN true ELSE false END, " +
		"CASE WHEN (indemnizacion.esPagoDanios = true) THEN 'DA' ELSE 'PT' END)" + 
		" FROM " +
		" LiquidacionSiniestro liquidacion " +
		" JOIN liquidacion.ordenesPago ordenPago" +			
		" JOIN ordenPago.ordenCompra ordenCompra " +
		" LEFT JOIN ordenCompra.indemnizacion indemnizacion" +
		" LEFT JOIN indemnizacion.recepcionDocumentosSiniestro recepcionDocs " +
		" JOIN ordenCompra.reporteCabina reporteCabina " +
		" JOIN reporteCabina.siniestroCabina siniestro" +
		" WHERE " +
		" 	  ordenCompra.tipoPago = :pagoBeneficiario " +
		" AND ordenPago.estatus IN(:estatusAsociada,:estatusOPagada)" +
		" AND liquidacion.id  = :idLiquidacion " +		
		" ORDER BY ordenPago.id";  

		Utilerias.agregaHashLista(listaParametrosValidos, "estatusIndemnizacion", EstatusAutorizacionIndemnizacion.AUTORIZADA.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "pagoBeneficiario", OrdenCompraService.PAGO_A_BENEFICIARIO);
		Utilerias.agregaHashLista(listaParametrosValidos, "claveDanios", ClaveTipoCalculo.DANIOS_MATERIALES.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "claveRobo", ClaveTipoCalculo.ROBO_TOTAL.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusAsociada", PagosSiniestroService.ESTATUS_ASOCIADA);
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusOPagada", PagosSiniestroService.ESTATUS_PAGADA);
		Utilerias.agregaHashLista(listaParametrosValidos, "idLiquidacion", idLiquidacion);
		
		query = entityManager.createQuery(queryStr);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		listaResultados = query.getResultList();
		
		return listaResultados;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RecepcionDocumentosSiniestro> obtenerInformacionBancariaIndemnizacionPorOrdenesPago(List<Long> listaOrdenes)
	{	    
		List<RecepcionDocumentosSiniestro> listaResultados = new ArrayList<RecepcionDocumentosSiniestro>();
		   
		if(listaOrdenes != null && !listaOrdenes.isEmpty())
		{
			Query query;  
			
			String queryStr = "SELECT recepcion " +  
			" FROM " +
			"  RecepcionDocumentosSiniestro recepcion " +
			"  JOIN recepcion.indemnizacion indemnizacion" +
			"  JOIN indemnizacion.ordenCompraGenerada ordenCompra " +
			"  JOIN ordenCompra.ordenPago ordenPago" +
			" WHERE " +
			" 	  ordenPago.id IN :idsOrdenesPago";
				
			query = entityManager.createQuery(queryStr);
			query.setParameter("idsOrdenesPago", listaOrdenes);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			listaResultados = query.getResultList();			
		}				
	
		return listaResultados;
	}
	
	@Override
	public List<RecuperacionProveedor> obtenerRecuperacionesPendientesPorAsignar(Integer idProveedor)
	{
		List<RecuperacionProveedor> listaResultados = new ArrayList<RecuperacionProveedor>();
		
		Query query;
		
		String queryStr = "SELECT new mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor(" +
				"recuperacion.id, recuperacion.numero, recuperacion.estatus, recuperacion.fechaCreacion, recuperacion.iva, " +
				"recuperacion.ivaRetenido, recuperacion.isr, recuperacion.subTotal, recuperacion.montoTotal)" + 
		" FROM " +
		" RecuperacionProveedor recuperacion " +
		" JOIN recuperacion.ordenCompra ordenCompra " +
		" WHERE " +
		" 	  recuperacion.tipo = :tipoRecuperacion" +
		" AND recuperacion.estatus = :estatusRecuperacion " + 
		" AND recuperacion.medio = :medioRecuperacion" + 
		" AND ordenCompra.idBeneficiario  = :idProveedor" +  
		" AND recuperacion.id NOT IN (SELECT recuperacion.id " +
		" 	FROM LiquidacionSiniestro liquidacion " +
		" 	JOIN liquidacion.recuperaciones recuperacion)" +
		" ORDER BY recuperacion.numero";  
		
		query = entityManager.createQuery(queryStr);
		query.setParameter("tipoRecuperacion", Recuperacion.TipoRecuperacion.PROVEEDOR.toString());
		query.setParameter("estatusRecuperacion", Recuperacion.EstatusRecuperacion.PENDIENTE.toString());
		query.setParameter("medioRecuperacion", Recuperacion.MedioRecuperacion.NOTACRED.toString());
		query.setParameter("idProveedor", idProveedor);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		listaResultados = query.getResultList();	
		
		return listaResultados;
	}
	
	@Override
	public LiquidacionSiniestro obtenerLiquidacionPorEstimacion(Long idEstimacion){
		
		try{
			StringBuilder queryString = new StringBuilder(" SELECT liq " +
															" FROM LiquidacionSiniestro liq  " +
															" JOIN liq.ordenesPago ordenPago" +			
															" JOIN  ordenPago.ordenCompra ordenCompra " +
															" WHERE ordenCompra.idTercero = :keyEstimacion AND liq.estatus = :estatusAplicada  ");
			
			TypedQuery<LiquidacionSiniestro> query = entityManager.createQuery(queryString.toString(), LiquidacionSiniestro.class);
			
			query.setParameter("keyEstimacion", idEstimacion);
			query.setParameter("estatusAplicada", EstatusLiquidacionSiniestro.APLICADA.getValue());
			
			return query.getSingleResult();
		
		}catch(Exception e){
			return null;
		}
	}
	
	
	/*
	liquidacion.setSubtotal(subtotal);
	liquidacion.setDeducible(deducible);
	liquidacion.setDescuento(descuento);
	liquidacion.setIva(iva);
	liquidacion.setIvaRet(ivaRetenido);
	liquidacion.setIsr(isr);
	liquidacion.setTotal(total);
	if(liquidacion.getOrigenLiquidacion().equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue()))
	{
		liquidacion.setNetoPorPagar(total);
	}*/
	
	@Override
	public LiquidacionSiniestro recalculaLiquidacion(Long idLiquidacion){
		LiquidacionSiniestro respuesta  = null;
		String spName = "MIDAS.PKG_LIQUIDACIONES_SINIESTROS.RECALCULA_LIQUIDACION"; 
        StoredProcedureHelper storedHelper 			= null;
        try {
      	  	String propiedades = "subtotal,total,iva,ivaRetenido,isr";
      	  	String columnasBaseDatos = "v_SubtotalFinal,v_TotalFinal,v_IvaFinal,v_IvaRetenidoFinal,v_IsrFinal";

            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(LiquidacionSiniestro.class.getCanonicalName(), propiedades, columnasBaseDatos);
            storedHelper.estableceParametro("pIdLiquidacion", idLiquidacion);
            respuesta = (LiquidacionSiniestro)storedHelper.obtieneResultadoSencillo();
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en busqueda de liquidaciones ..."+descErr
                                           , Level.WARNING, e);    
             }
        }
		return respuesta;
	}
	@Override
	public LiquidacionSiniestro asociaDesasociaFacturas(String listaFacturas, Long idLiquidacion, Short tipoOperacion){
		LiquidacionSiniestro liquidacion  = null;
		String spName = "MIDAS.PKG_LIQUIDACIONES_SINIESTROS.asociaDesaFacturas"; 
        StoredProcedureHelper storedHelper = null;
        try {
        	String propiedades = "subtotal,total,iva,ivaRetenido,isr";
      	  	String columnasBaseDatos = "v_SubtotalFinal,v_TotalFinal,v_IvaFinal,v_IvaRetenidoFinal,v_IsrFinal";

            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(LiquidacionSiniestro.class.getCanonicalName(), propiedades, columnasBaseDatos);
            storedHelper.estableceParametro("pListaFacturas", listaFacturas);
            storedHelper.estableceParametro("pLiquidacionId", idLiquidacion);
            storedHelper.estableceParametro("pTipoOperacion", tipoOperacion);
            liquidacion = (LiquidacionSiniestro)storedHelper.obtieneResultadoSencillo();
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general al asociar o desasociar facturas ..."+descErr
                                           , Level.WARNING, e);    
             }
        }
		return liquidacion;
	}
}
