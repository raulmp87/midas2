package mx.com.afirme.midas2.dao.impl.siniestros.catalogo.conceptos;


import java.util.List;
import java.util.Set;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.catalogo.conceptoajuste.*;
import mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos.*;

@Stateless

public class ConceptoAjusteDaoImp  extends EntidadDaoImpl  implements ConceptoAjusteDao {
	
	
	@Override
	public List<SeccionDTOConcepto> getListarSeccionesVigentesAutosUsablesSiniestros() {
		
		try {
			String queryString = "select " +
								" distinct new mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos.SeccionDTOConcepto( model.idToSeccion , model.descripcion) " +
								" from SeccionDTO model " +
								" join model.tipoPolizaDTO pol " +
								" join pol.productoDTO prod " +
								" where  model.claveEstatus IN (1,2) and prod.claveNegocio='A' " +
								" and model.claveActivo = 1" +
								" and pol.claveActivo = 1" +
								" and prod.claveActivo = 1" +
								" order by model.descripcion asc";
			
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find list active sections to vehicles failed", Level.SEVERE, re);
			throw re;
		}		
		
	}

}
