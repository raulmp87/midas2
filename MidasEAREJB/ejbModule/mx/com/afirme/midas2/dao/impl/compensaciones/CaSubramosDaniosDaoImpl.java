/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;


import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Map;


import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import oracle.jdbc.driver.OracleTypes;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import mx.com.afirme.midas2.dao.compensaciones.CaSubramosDaniosDao;
import mx.com.afirme.midas2.domain.compensaciones.CaSubramosDanios;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.SubRamosDaniosView;
import mx.com.afirme.midas2.dto.compensaciones.SubRamosDaniosViewDTO;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

@Stateless
public class CaSubramosDaniosDaoImpl implements CaSubramosDaniosDao {
	public static final String COTIZACION_ID = "cotizacionDTO.idToCotizacion";
	public static final String CA_PARAMETROS = "caParametros";
	public static final String PORCENTAJECOMPENSACION = "porcentajeCompensacion";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@SuppressWarnings("unused")
	private DataSource dataSource;
	
	private SimpleJdbcCall calculosProvision;
	private SimpleJdbcCall viewSubRamosDanios;
    private JdbcTemplate jdbcTemplate;

	@PersistenceContext
	private EntityManager entityManager;

	
	/** Log de CaSubramosDaniosDaoImpl */
	private static final Logger LOGGER = LoggerFactory
			.getLogger(CaSubramosDaniosDaoImpl.class);
	
	
	public void save(CaSubramosDanios entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaSubramosDanios entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaSubramosDanios.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaSubramosDanios update(CaSubramosDanios entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaSubramosDanios result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaSubramosDanios 	::		CaSubramosDaniosDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaSubramosDanios findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaSubramosDaniosDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaSubramosDanios instance = entityManager.find(CaSubramosDanios.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaSubramosDaniosDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaSubramosDaniosDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<CaSubramosDanios> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaSubramosDaniosDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaSubramosDanios model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaSubramosDaniosDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaSubramosDaniosDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaSubramosDanios> findByPorcentajecompensacion(Object porcentajecompensacion
	) {
		return findByProperty(PORCENTAJECOMPENSACION, porcentajecompensacion
		);
	}
	
	public List<CaSubramosDanios> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaSubramosDanios> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaSubramosDanios> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaSubramosDaniosDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaSubramosDanios model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaSubramosDaniosDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaSubramosDaniosDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}

    @Resource(name ="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {		
			this.jdbcTemplate = new JdbcTemplate(dataSource);
	this.calculosProvision = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("MIDAS")
				.withCatalogName("PKG_CA_DAN_COMPENSACIONES")
				.withProcedureName("obtenerCalculosCom").declareParameters(		
							new SqlParameter("pIdToCotizacion", Types.NUMERIC),
							new SqlParameter("pIdCotizacionendoso", Types.NUMERIC),
							new SqlParameter("pIdToPoliza", Types.NUMERIC),
						    new SqlParameter("pIdEntidadPer", Types.NUMERIC),
						    new SqlParameter("pIdCompensacion", Types.NUMERIC),
						    new SqlParameter("pReciboId", Types.NUMERIC),
							new SqlOutParameter("pImporteTotal",  Types.NUMERIC),
							new SqlOutParameter("pId_Cod_Resp",  Types.NUMERIC),
							new SqlOutParameter("pDesc_Resp", Types.VARCHAR),					
							new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
								@Override
								public SubRamosDaniosView mapRow(ResultSet rs, int index) throws SQLException {
									SubRamosDaniosView subRamosDaniosView = new SubRamosDaniosView();
									subRamosDaniosView.setId(rs.getBigDecimal("ID"));
									subRamosDaniosView.setIdTcRamo(rs.getBigDecimal("IDTCRAMO"));
									subRamosDaniosView.setDescripcionRamo(rs.getString("DESCRIPCIONRAMO"));
									subRamosDaniosView.setIdTcSubRamo(rs.getBigDecimal("IDTCSUBRAMO"));
									subRamosDaniosView.setSubRamo(rs.getString("DESCRIPCIONSUBRAMO"));									
									subRamosDaniosView.setPrimaBase(rs.getBigDecimal("VALORPRIMANETA"));
									subRamosDaniosView.setPorcentajeCompensacion(rs.getBigDecimal("PORCENTAJECOMPENSACION"));
									subRamosDaniosView.setPorcentajeCompensacionImporte(rs.getBigDecimal("IMPORTE"));
									subRamosDaniosView.setExiste(rs.getString("EXISTE"));
									return subRamosDaniosView;
							  }
					          }));
	
  this.viewSubRamosDanios = new SimpleJdbcCall(jdbcTemplate)
	.withSchemaName("MIDAS")
	.withCatalogName("PKG_CA_DAN_COMPENSACIONES")
	.withProcedureName("viewSubRamos").declareParameters(		
			new SqlParameter("pIdCompensacion", Types.NUMERIC),
				new SqlParameter("pIdCotizacion", Types.NUMERIC),
			    new SqlParameter("pIdCotizacionendoso", Types.NUMERIC),
			    new SqlParameter("pIdEntidadpersona", Types.NUMERIC),
			    new SqlParameter("pReciboId", Types.NUMERIC),
				new SqlOutParameter("pImpTotal",  Types.NUMERIC),
				new SqlOutParameter("pId_Cod_Resp",  Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR),					
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
					@Override
					public SubRamosDaniosView mapRow(ResultSet rs, int index) throws SQLException {
						SubRamosDaniosView subRamosDaniosView = new SubRamosDaniosView();
						subRamosDaniosView.setId(rs.getBigDecimal("ID"));
						subRamosDaniosView.setIdTcRamo(rs.getBigDecimal("IDTCRAMO"));
						subRamosDaniosView.setDescripcionRamo(rs.getString("DESCRIPCIONRAMO"));
						subRamosDaniosView.setIdTcSubRamo(rs.getBigDecimal("IDTCSUBRAMO"));
						subRamosDaniosView.setSubRamo(rs.getString("DESCRIPCIONSUBRAMO"));									
						subRamosDaniosView.setPrimaBase(rs.getBigDecimal("VALORPRIMANETA"));
						subRamosDaniosView.setPorcentajeCompensacion(rs.getBigDecimal("PORCENTAJECOMPENSACION"));
						subRamosDaniosView.setPorcentajeCompensacionImporte(rs.getBigDecimal("IMPORTE"));
						subRamosDaniosView.setExiste(rs.getString("EXISTE"));

						LOGGER.info(">> setDataSource return subRamosDaniosView");
						return subRamosDaniosView;
					}
	        	}));	
	           }
    
	@SuppressWarnings("unchecked")
	public void getViewSubRamos(SubRamosDaniosViewDTO subDaniosViewDTO, CompensacionesDTO compensacionesDTO){
		
		List<SubRamosDaniosView>  subRamosDaniosViews = null;
		try{
			LOGGER.info(">> getViewSubRamos");
				MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
				sqlParameter.addValue("pIdCompensacion", subDaniosViewDTO.getIdCompensacion());
				sqlParameter.addValue("pIdCotizacion", subDaniosViewDTO.getIdToCotizacion());
				sqlParameter.addValue("pIdCotizacionendoso", subDaniosViewDTO.getIdCotizacionEndoso());
				sqlParameter.addValue("pIdEntidadpersona", subDaniosViewDTO.getIdEntidadPer());
				if(subDaniosViewDTO.getIdRecibo()>0){
					sqlParameter.addValue("pReciboId", subDaniosViewDTO.getIdRecibo());
				}else{
					sqlParameter.addValue("pReciboId", null);
				}
				Map<String, Object> execute = this.viewSubRamosDanios.execute(sqlParameter);			
				subRamosDaniosViews = (List<SubRamosDaniosView>) execute.get("pCursor");	
				BigDecimal importeTotal = (BigDecimal) ( execute.get("pImpTotal"));
				compensacionesDTO.setListSubRamosDaniosView(subRamosDaniosViews);
				compensacionesDTO.setImporteTotalSubRamosDanios(importeTotal);				
				LOGGER.info("<< getViewSubRamos");
		}catch (Exception e) {
			compensacionesDTO.setListSubRamosDaniosView(new  ArrayList<SubRamosDaniosView>());
			LOGGER.error("-- viewSubRamosDanios()", e);
	       }	
        }    
	
	@SuppressWarnings("unchecked")
	public void calcularProvisionDanios(SubRamosDaniosViewDTO subDaniosViewDTO, CompensacionesDTO compensacionesDTO){
		List<SubRamosDaniosView>  subRamosDaniosViews = null;
		try{
				MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
				sqlParameter.addValue("pIdToCotizacion", subDaniosViewDTO.getIdToCotizacion());
				sqlParameter.addValue("pIdCotizacionendoso", subDaniosViewDTO.getIdCotizacionEndoso());
				sqlParameter.addValue("pIdToPoliza", subDaniosViewDTO.getIdToPoliza());
				sqlParameter.addValue("pIdEntidadPer", subDaniosViewDTO.getIdEntidadPer());
				sqlParameter.addValue("pIdCompensacion", subDaniosViewDTO.getIdCompensacion());
				if(subDaniosViewDTO.getIdRecibo()>0){
					sqlParameter.addValue("pReciboId", subDaniosViewDTO.getIdRecibo());
				}else{
					sqlParameter.addValue("pReciboId", null);
				}
				
				Map<String, Object> execute = this.calculosProvision.execute(sqlParameter);			
				subRamosDaniosViews = (List<SubRamosDaniosView>) execute.get("pCursor");	
				BigDecimal importeTotal = (BigDecimal) ( execute.get("pImporteTotal"));	
				compensacionesDTO.setListSubRamosDaniosView(subRamosDaniosViews);
				compensacionesDTO.setImporteTotalSubRamosDanios(importeTotal);
				
		}catch (Exception e) {
			compensacionesDTO.setListSubRamosDaniosView(new  ArrayList<SubRamosDaniosView>());
			LOGGER.error("-- calcularProvisionDanios()", e);
		}
	  }			
	
	public int deleteRecordsSubRamosDanios(Long cotizacionId, Long entidadPersonaId, Long compensacionId){
		LOGGER.info(">> deleteRecordsSubRamosDanios()");
		int deleteCount = 0;
		try {
			StringBuilder queryString = new StringBuilder("DELETE FROM CaSubramosDanios model");
			queryString.append(" WHERE model.").append("cotizacionDTO.idToCotizacion").append(" = ").append(" :cotizacionId ");
			queryString.append(" AND model.").append("caParametros.caCompensacion.id").append(" = ").append(" :compensacionId ");
			queryString.append(" AND model.").append("caParametros.caTipoCompensacion.id").append(" = ").append(" :tipoCompensacionId ");
			queryString.append(" AND model.").append("caParametros.caEntidadPersona.id").append(" = ").append(" :entidadPersonaId ");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("cotizacionId", cotizacionId);
			query.setParameter("compensacionId", compensacionId);
			query.setParameter("tipoCompensacionId", ConstantesCompensacionesAdicionales.PORPRIMA);
			query.setParameter("entidadPersonaId", entidadPersonaId);
			
			deleteCount = query.executeUpdate();
			
		} catch (RuntimeException re) {
			LOGGER.error("Información del Error", re);
			throw re;
		}
		LOGGER.info("<< deleteRecordsSubRamosDanios()");
		return deleteCount;
	
	  }
}




