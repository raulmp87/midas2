package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.sql.SQLException;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionSeycosFacadeRemote;
@Stateless
public class CentroOperacionSeycosFacade implements CentroOperacionSeycosFacadeRemote{
	/**
	 * Metodo que replica el centro de operacion en Seycos.
	 * Si tiene id el centro de operacion entonces se actualiza, sino, se inserta.
	 */
	@Override
	public Long save(CentroOperacion centroOperacion) throws Exception {
		if(centroOperacion==null){
			throw new Exception("Centro Operacion is null!");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_catCOper";
		Long idCentroOperacion=null;
		try {
			LogDeMidasInterfaz.log("Entrando a CentroOperacionSeycosFacade.save..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			Long idPersona=(centroOperacion.getPersonaResponsable()!=null)?centroOperacion.getPersonaResponsable().getIdPersona():null;
			Domicilio domicilio=centroOperacion.getDomicilio();
			DomicilioPk pkDomicilio=(domicilio!=null)?domicilio.getIdDomicilio():null;
			Long idDomicilio=(pkDomicilio!=null)?pkDomicilio.getIdDomicilio():null;
			storedHelper.estableceParametro("pid_centro_oper", val(centroOperacion.getIdCentroOperacion()));
			storedHelper.estableceParametro("pdescripcion",val(centroOperacion.getDescripcion()));
			storedHelper.estableceParametro("pid_persona_resp",val(idPersona));
			storedHelper.estableceParametro("pid_domicilio_resp",val(idDomicilio));
			storedHelper.estableceParametro("pcorreo_electronico",val(centroOperacion.getCorreoElectronico()));
			System.out.println("pid_centro_oper:= "+ val(centroOperacion.getIdCentroOperacion()));
			System.out.println("pdescripcion:= "+ val(centroOperacion.getDescripcion()));
			System.out.println("pid_persona_resp:= "+ val(idPersona));
			System.out.println("pid_domicilio_resp:= "+ val(idDomicilio));
			System.out.println("pcorreo_electronico:= "+ val(centroOperacion.getCorreoElectronico()));
			int id=storedHelper.ejecutaActualizar();
			idCentroOperacion= centroOperacion.getIdCentroOperacion();
//			centroOperacion.setIdCentroOperacion(idCentroOperacion);
			LogDeMidasInterfaz.log("Se ha guardado centro operacion con id:"+id+"..."+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de CentroOperacionSeycosFacade.save..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, centroOperacion.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de CentroOperacionSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en CentroOperacionSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		}
		return idCentroOperacion;
	}
	private Object val(Object value){
		return (value!=null)?value:"";
	}

	@Override
	public void unsubscribe(CentroOperacion centroOperacion) throws Exception {
		centroOperacion.setClaveEstatus(0l);//se inactiva el centro de operacion.
		save(centroOperacion);//Se actualiza el estatus del centro de operacion en Seycos
	}

}
