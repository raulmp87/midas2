package mx.com.afirme.midas2.dao.impl.endoso.cotizacion.auto;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.EndosoWSMidasAutosDao;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class EndosoWSMidasAutosDaoImpl implements EndosoWSMidasAutosDao{

	@EJB
	private EntidadService entidadService;
	
	public MovimientoEndoso findByControlEndosoCotId(Long controlEndosoCotId) {			
		List<MovimientoEndoso> instance = this.findByProperty("controlEndosoCot.id", controlEndosoCotId);
		if (instance!=null && instance.size() > 0){
			return instance.get(0);
		}
		return null;
	}
	
	public List<MovimientoEndoso>  findByProperty(String propertyName, Object propertyValue) {
		return	 entidadService.findByProperty(MovimientoEndoso.class ,propertyName, propertyValue);	
	}
	
	@Override
	public MovimientoEndoso actualizaObservaciones(Long controlEndosoCotId,String observaciones) throws Exception {
		LogDeMidasInterfaz.log("actualizaObservaciones() >>" + this, Level.INFO, null);
		
		MovimientoEndoso entity = this.findByControlEndosoCotId(controlEndosoCotId);
		
		String descripcion =  entity.getDescripcion()+" "+ observaciones;
		
		entity.setDescripcion(descripcion);
		
		MovimientoEndoso result = null;
		
		try {
			result = entidadService.save(entity);
			
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
		LogDeMidasInterfaz.log("<< actualizaObservaciones()" + this, Level.INFO, null);	
		return result;
	}
}
