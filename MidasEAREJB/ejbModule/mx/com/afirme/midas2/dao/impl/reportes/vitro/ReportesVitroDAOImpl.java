package mx.com.afirme.midas2.dao.impl.reportes.vitro;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.reportes.vitro.ReportesVitroDAO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.reportes.vitro.ReportesVitroDTO;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;
import mx.com.afirme.midas2.service.reportes.vitro.ReportesVitroService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.afirme.nomina.model.*;

@Stateless
public class ReportesVitroDAOImpl implements ReportesVitroDAO, Serializable {
	
	/**
	 * <p>Cadena que contiene el nombre del stp para consultar las polizas del negocio vitro.</p>
	 */
	public final static String CONSULTA_POLIZAS_VITRO = "MIDAS.PKG_REPORTES_VITRO.GENERAR_REPORTE";
	
	/**
	 * <p>Nombres de los campos de la clase <code>ReportesVitroDTO</code></p>
	 */
	public final String [] PROPIEDADES_CLASE = {"numeroPoliza", "nombreAsegurado", "numeroEmpleado", "montoTotal", "montoApagar", "idRecibo", "numeroFolioRecibo", "idCotizacionSeycos"};
	
	/**
	 * <p>Nombres de los campos del cursor del reporte.</p>
	 */
	public final String [] CAMPOS_CURSOR = {"NUMPOLIZASEYCOS", "NOMBREASEGURADO", "NUMERO_EMPLEADO", "MONTO_TOTAL", "MONTO_A_PAGO", "id_recibo", "num_folio_rbo", "id_cotizacion"};
	
	private static final long serialVersionUID = -1494074679387192399L;

	private static Logger log = LoggerFactory.getLogger(ReportesVitroDAOImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private AgenteService agenteService;
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Date> consultarCalendarioGrupo(Long arg0) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(" SELECT *  FROM (SELECT DTC.FECHACOBRO ")
			.append(" FROM MIDAS.DPNCALENDARIODETALLE DTC ")
			.append(" INNER JOIN MIDAS.DPNCALENDARIO C ON (C.ID = DTC.CALENDARIO_ID) ")
			.append(" INNER JOIN MIDAS.DPNGRUPO GPO ON (GPO.CALENDARIO_ID = C.ID) ")
			.append(" WHERE GPO.ID = ?1 ")
			.append(" AND GPO.ESTATUS_ID = (SELECT ID FROM MIDAS.DPNESTATUS WHERE UPPER(TRIM(DESCRIPCION)) = 'ACTIVO' AND ACTIVO = 1 AND MODULO_ID = (SELECT ID FROM MIDAS.DPNMODULO WHERE UPPER(TRIM(DESCRIPCION)) = 'GRUPOS' AND ACTIVO = 1))")
			.append(" AND GPO.ACTIVO = 0 ")
			.append(" AND C.ACTIVO = 0 ")
			.append(" AND DTC.ACTIVO = 0 ")
			.append(" AND DTC.FECHACOBRO <= TRUNC(SYSDATE) ")
			.append(" ORDER BY DTC.FECHACOBRO DESC) FECHA_C ")
			.append(" WHERE ROWNUM <= 5 ")
			.append(" ORDER BY FECHACOBRO DESC ");
		Query query = entityManager.createNativeQuery(sqlQuery.toString());
		query.setParameter(1, arg0);
		
		return (List<Date>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Dpngrupo> consultarGruposVitro() {
		StringBuilder sqlQuery = new StringBuilder();
		List<Dpngrupo> grupos = new ArrayList<Dpngrupo>();
		sqlQuery.append(" SELECT GPO.ID, GPO.NOMBREGRUPO ")
			.append(" FROM MIDAS.DPNGRUPO GPO ")
			.append(" INNER JOIN MIDAS.DPNESTATUS EST ON (EST.ID = GPO.ESTATUS_ID AND EST.ACTIVO = 1 AND UPPER(TRIM(EST.DESCRIPCION)) = 'ACTIVO') ")
			.append(" INNER JOIN MIDAS.DPNMODULO MODU ON (MODU.ID = EST.MODULO_ID AND MODU.ACTIVO = 1 AND UPPER(TRIM(MODU.DESCRIPCION))= 'GRUPOS') ")
			.append(" WHERE GPO.NOMBREGRUPO LIKE ?1 ")
			.append(" AND GPO.ACTIVO = ?2 ");
		Query query = entityManager.createNativeQuery(sqlQuery.toString());
		query.setParameter(1 , ReportesVitroService.LIKE_GRUPOS_VITRO);
		query.setParameter(2 , "0");
		List<Object[]> res = (List<Object[]>) query.getResultList();
		for(Object[] elemento: res){
			Dpngrupo grupo = new Dpngrupo();
			BigDecimal id = elemento[0] != null ? (BigDecimal) elemento[0] : null;
			grupo.setDpnIdgrupo(id != null ? id.intValue() : 0);
			grupo.setDpnNombregrupo(elemento[1] != null ? (String) elemento[1] : null);
			grupos.add(grupo);
		}
		return grupos;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Dpncliente> consultarCliente(String nombreClienteDPN, String idNegocio){
		StringBuilder sqlQuery = new StringBuilder();
		List<Dpncliente> clientes = new ArrayList<Dpncliente>();
		sqlQuery.append("SELECT CLI.ID, UPPER(TRIM(CLI.RAZONSOCIAL)), UPPER(TRIM(CLI.CONTACTO)) ")
				.append(" FROM MIDAS.DPNCLIENTE CLI ")
				.append(" INNER JOIN MIDAS.DPNESTATUS EST ON (EST.ID = CLI.ESTATUS_ID AND EST.ACTIVO = 1 AND UPPER(TRIM(EST.DESCRIPCION)) = 'ACTIVO') ")
				.append(" INNER JOIN MIDAS.DPNMODULO MODU ON (MODU.ID = EST.MODULO_ID AND MODU.ACTIVO = 1 AND UPPER(TRIM(MODU.DESCRIPCION))= 'CLIENTES') ")
				.append(" WHERE CLI.RAZONSOCIAL LIKE '")
				.append(nombreClienteDPN)
				.append("' ")
				.append(" AND CLI.CONTACTO = ?1 " )
				.append(" AND CLI.ACTIVO = ?2 ")
				.append(" ORDER BY UPPER(TRIM(CLI.RAZONSOCIAL)) ");
		Query q = entityManager.createNativeQuery(sqlQuery.toString());
		q.setParameter(1, idNegocio);
		q.setParameter(2, "0");
		List<Object[]> res = (List<Object[]>) q.getResultList();
		for(Object[] elemento : res){
			BigDecimal id = elemento[0] != null ? (BigDecimal) elemento[0] : null;
			Dpncliente cl = new Dpncliente();
			cl.setDpnIdcliente(id != null ? id.intValue() : 0);
			cl.setDpnRazonsocial(elemento[1] != null ? (String) elemento[1] : null);
			cl.setDpnContacto(elemento[2] != null ? (String) elemento[2] : null);
			clientes.add(cl);
		}
		return clientes;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<ReportesVitroDTO> consultarPolizasVitro(Date fechaCorte, String tipoNomina, Long idNegocio, Long idFormaPago, Long numeroDivide, String nombreGrupo){
		List<ReportesVitroDTO> polizas = new ArrayList<ReportesVitroDTO>();
		try{
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(CONSULTA_POLIZAS_VITRO, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(ReportesVitroDTO.class.getCanonicalName(), PROPIEDADES_CLASE, CAMPOS_CURSOR);
			
			storedHelper.estableceParametro("p_fechaCorte", fechaCorte);
			storedHelper.estableceParametro("p_tipoNomina", tipoNomina);
			storedHelper.estableceParametro("p_idNegocio", idNegocio);
			storedHelper.estableceParametro("p_idFormaPago", idFormaPago);
			storedHelper.estableceParametro("p_numero_divide", numeroDivide);
			
			polizas = storedHelper.obtieneListaResultados();
		} catch(Exception err){
			log.debug("error al obtener el stp " +  CONSULTA_POLIZAS_VITRO, err);
		}
		return polizas;
	}
	
	@Override
	public List<Negocio> consultarNegocios(Long idAgente){
		List<Negocio> resultados = new ArrayList<Negocio>();
		StringBuilder query = new StringBuilder();
		query.append("SELECT NEG.IDTONEGOCIO, NEG.DESCRIPCIONNEGOCIO  ")
			.append(" FROM MIDAS.TONEGAGENTE NEGAGEN ")
			.append(" INNER JOIN MIDAS.VNCLIENTEASOCIADO CLIENTE  ON CLIENTE.IDTONEGOCIO =  NEGAGEN.IDTONEGOCIO ")
			.append(" INNER JOIN MIDAS.TONEGOCIO NEG  ON NEG.IDTONEGOCIO = NEGAGEN.IDTONEGOCIO ")
			.append(" INNER JOIN MIDAS.DPNCLIENTE DPNCL  ON DPNCL.RFC = CLIENTE.CODIGORFC ")
			.append(" WHERE NEGAGEN.IDAGENTE = ?1 ")
			.append(" AND NEG.CLAVEESTATUS = 1  ORDER BY NEG.DESCRIPCIONNEGOCIO ");
		Query q = entityManager.createNativeQuery(query.toString());
		q.setParameter(1, idAgente);
		@SuppressWarnings("unchecked")
		List<Object[]> result = (List<Object[]>) q.getResultList();
		for(Object[] elemento: result){
			Negocio neg = new Negocio();
			BigDecimal id = elemento[0] != null ? (BigDecimal)elemento[0] : null;
			neg.setIdToNegocio(id != null ? id.longValue() : null);
			neg.setDescripcionNegocio(elemento[1] != null ? (String) elemento[1] : null);
			resultados.add(neg);
		}
		return resultados;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

	public AgenteService getAgenteService() {
		return agenteService;
	}

	public void setAgenteService(AgenteService agenteService) {
		this.agenteService = agenteService;
	}
}
