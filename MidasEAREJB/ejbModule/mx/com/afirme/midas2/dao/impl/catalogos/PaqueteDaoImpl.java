package mx.com.afirme.midas2.dao.impl.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.catalogos.PaqueteDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.Paquete_;

@Stateless
public class PaqueteDaoImpl extends JpaDao<Long, Paquete> implements PaqueteDao{

	public List<Paquete> findByFilters(Paquete filtroPaquete) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Paquete> criteriaQuery = cb.createQuery(Paquete.class);
		Root<Paquete> root = criteriaQuery.from(Paquete.class);
		Predicate predicado = cb.and(cb.like(cb.upper(root.get(Paquete_.descripcion)),"%" + filtroPaquete.getDescripcion() + "%"));
		if(filtroPaquete.getClaveAmisTipoSeguro() != null && filtroPaquete.getClaveAmisTipoSeguro() >= 0){
			List<Predicate> predicates = new ArrayList<Predicate>();
			predicates.add(predicado);
			predicates.add(cb.and(cb.equal(root.get(Paquete_.claveAmisTipoSeguro), filtroPaquete.getClaveAmisTipoSeguro())));	
			criteriaQuery.where(predicates.toArray(new Predicate[predicates.size()]));
		} else {
			criteriaQuery.where(predicado);
		}
		TypedQuery<Paquete> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}
}
