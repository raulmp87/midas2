package mx.com.afirme.midas2.dao.impl.sapamis.sistemas;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas2.dao.sapamis.sistemas.SapAmisBitacorasDao;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;

/*******************************************************************************
 * Nombre Clase: 		SapAmisBitacorasDaoImpl.
 * 
 * Descripcion: 		Esta clase se utiliza para la ejecución de transacciones
 * 						de Base de Datos para las Bitacoras de los Procesos
 * 						ejecutados.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisBitacorasDaoImpl implements SapAmisBitacorasDao{
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext private EntityManager entityManager;
    @EJB private SapAmisUtilsService sapAmisUtilsService;
    
	private static final String CAMPO_NOMBRE_FECHA_OP = "FECHAOPERACION";
	private static final String CAMPO_NOMBRE_FECHA_REG = "FECHAREGISTRO";
	private static final String CAMPO_NOMBRE_FECHA_ENV = "FECHAENVIO";
	private static final String CAMPO_NOMBRE_SISTEMA = "IDSAPAMISCATSISTEMAS";
	private static final String CAMPO_NOMBRE_OPERACION = "IDSAPAMISCATOPERACIONES";
	private static final String CAMPO_NOMBRE_ESTATUS = "ESTATUS";
	private static final String CAMPO_NOMBRE_OBSERV = "OBSERVACIONES";
	private static final String CAMPO_NOMBRE_MODULO = "MODULO"; 
	private static final String FORMATO_FECHA = "YYYY/MM/DD HH24:MI:SS";
	private static final String FECHA_INICIAL_PREFIJO = " 00:00:00";
	private static final String FECHA_FINAL_PREFIJO = " 23:59:59";
	private static final String ALIAS_SELECT_PADRE = " BIT.";
	private static final String ALIAS_SELECT_ANIDADO = " SAB.";
	private static final String CLAUSULA_WHERE = "WHERE";
	private static final String CLAUSULA_AND = "AND";
	private static final String CLAUSULA_BETWEEN = " BETWEEN";
	private static final String CLAUSULA_ASC = " ASC";
	private static final String PARAMETRO_SIMPLE_SEGMENTO = " = ? ";
	private static final String PARAMETRO_TO_DATE_SEGMENTO_INICIAL = " TO_DATE(?,'";
	private static final String PARAMETRO_TO_DATE_SEGMENTO_INICIAL_SEGMENTO_FINAL = "')";

	@Override
	@SuppressWarnings("unchecked")
	public List<SapAmisBitacoras> obtenerCifras(ParametrosConsulta parametrosConsulta){
		Query query = buildQueryObtenerCifras(parametrosConsulta);
		int contadorParametros = 1;
		if(parametrosConsulta.getFechaOperacionIni() != null && parametrosConsulta.getFechaOperacionFin()!= null){
			query.setParameter(contadorParametros, sapAmisUtilsService.convertToStrDate(parametrosConsulta.getFechaOperacionIni()) + FECHA_INICIAL_PREFIJO);
			contadorParametros++;
			query.setParameter(contadorParametros, sapAmisUtilsService.convertToStrDate(parametrosConsulta.getFechaOperacionFin()) + FECHA_FINAL_PREFIJO);
			contadorParametros++;	
		}
		if(parametrosConsulta.getFechaRegistroIni() != null && parametrosConsulta.getFechaRegistroFin()!= null){
			query.setParameter(contadorParametros, sapAmisUtilsService.convertToStrDate(parametrosConsulta.getFechaRegistroIni()) + FECHA_INICIAL_PREFIJO);
			contadorParametros++;
			query.setParameter(contadorParametros, sapAmisUtilsService.convertToStrDate(parametrosConsulta.getFechaRegistroFin()) + FECHA_FINAL_PREFIJO);
			contadorParametros++;	
		}
		if(parametrosConsulta.getFechaEnvioIni() != null && parametrosConsulta.getFechaEnvioFin()!= null){
			query.setParameter(contadorParametros, sapAmisUtilsService.convertToStrDate(parametrosConsulta.getFechaEnvioIni()) + FECHA_INICIAL_PREFIJO);
			contadorParametros++;
			query.setParameter(contadorParametros, sapAmisUtilsService.convertToStrDate(parametrosConsulta.getFechaEnvioFin()) + FECHA_FINAL_PREFIJO);
			contadorParametros++;	
		}
		if(parametrosConsulta.getSistema() != null){
			query.setParameter(contadorParametros, parametrosConsulta.getSistema());
			contadorParametros++;	
		}
		if(parametrosConsulta.getOperacion() != null){
			query.setParameter(contadorParametros, parametrosConsulta.getOperacion());
			contadorParametros++;
		}
		if(parametrosConsulta.getEstatus() != null){
			query.setParameter(contadorParametros, parametrosConsulta.getEstatus());
			contadorParametros++;
		}
		if(parametrosConsulta.getObservaciones() != null){
			query.setParameter(contadorParametros, parametrosConsulta.getObservaciones());
			contadorParametros++;
		}
		if(parametrosConsulta.getModulo() != null){
			query.setParameter(contadorParametros, parametrosConsulta.getModulo());
		}
		return query.getResultList();
	}
	
	private Query buildQueryObtenerCifras(ParametrosConsulta parametrosConsulta){
		StringBuilder queryStringSelectPadre = new StringBuilder("SELECT ROWNUM AS IDSAPAMISBITACORAS, ");
		StringBuilder queryStringSelectAnidado = new StringBuilder("SELECT ");
		StringBuilder queryStringWhere = new StringBuilder("");
		StringBuilder queryStringGroupBy = new StringBuilder("");
		StringBuilder queryStringOrderBy = new StringBuilder("");
		boolean banderaWhere = true;
		
		//Se crean elementos(CAMPOS) para el SELECT
		queryStringSelectPadre.append(ALIAS_SELECT_PADRE + CAMPO_NOMBRE_SISTEMA + ", ");
		queryStringSelectAnidado.append(ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_SISTEMA + ", ");
		queryStringSelectPadre.append(ALIAS_SELECT_PADRE + CAMPO_NOMBRE_OPERACION + ", ");
		queryStringSelectAnidado.append(ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_OPERACION + ", ");
		queryStringSelectPadre.append(ALIAS_SELECT_PADRE + CAMPO_NOMBRE_ESTATUS + ", ");
		queryStringSelectAnidado.append(ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_ESTATUS + ", ");
		
		queryStringSelectPadre.append(ALIAS_SELECT_PADRE + "CIFRAS AS IDSAPAMISREGISTRO FROM (");
		queryStringSelectAnidado.append(" COUNT(*) AS CIFRAS FROM MIDAS.SAPAMISBITACORAS SAB ");
		//Se crean elementos para la Clausula WHERE
		if(parametrosConsulta.getFechaOperacionIni()!= null && parametrosConsulta.getFechaOperacionFin()!= null){
			queryStringWhere.append(CLAUSULA_WHERE + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_FECHA_OP + CLAUSULA_BETWEEN + PARAMETRO_TO_DATE_SEGMENTO_INICIAL + FORMATO_FECHA + PARAMETRO_TO_DATE_SEGMENTO_INICIAL_SEGMENTO_FINAL);
			queryStringWhere.append(CLAUSULA_AND + PARAMETRO_TO_DATE_SEGMENTO_INICIAL + FORMATO_FECHA + "') ");
			banderaWhere = false;
		}
		if(parametrosConsulta.getFechaRegistroIni() != null && parametrosConsulta.getFechaRegistroFin() != null){
			queryStringWhere.append((banderaWhere?CLAUSULA_WHERE:CLAUSULA_AND) + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_FECHA_REG + CLAUSULA_BETWEEN + PARAMETRO_TO_DATE_SEGMENTO_INICIAL + FORMATO_FECHA + PARAMETRO_TO_DATE_SEGMENTO_INICIAL_SEGMENTO_FINAL);
			queryStringWhere.append(CLAUSULA_AND + PARAMETRO_TO_DATE_SEGMENTO_INICIAL + FORMATO_FECHA + "') ");
			banderaWhere = false;
		}
		if(parametrosConsulta.getFechaEnvioIni() != null && parametrosConsulta.getFechaEnvioFin() != null){
			queryStringWhere.append((banderaWhere?CLAUSULA_WHERE:CLAUSULA_AND) + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_FECHA_ENV + CLAUSULA_BETWEEN + PARAMETRO_TO_DATE_SEGMENTO_INICIAL + FORMATO_FECHA + PARAMETRO_TO_DATE_SEGMENTO_INICIAL_SEGMENTO_FINAL);
			queryStringWhere.append(CLAUSULA_AND + PARAMETRO_TO_DATE_SEGMENTO_INICIAL + FORMATO_FECHA + "') ");
			banderaWhere = false;
		}
		if(parametrosConsulta.getSistema() != null){
			queryStringWhere.append((banderaWhere?CLAUSULA_WHERE:CLAUSULA_AND) + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_SISTEMA + PARAMETRO_SIMPLE_SEGMENTO);
			banderaWhere = false;
		}
		if(parametrosConsulta.getOperacion() != null){
			queryStringWhere.append((banderaWhere?CLAUSULA_WHERE:CLAUSULA_AND) + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_OPERACION + PARAMETRO_SIMPLE_SEGMENTO);
			banderaWhere = false;
		}
		if(parametrosConsulta.getEstatus() != null){
			queryStringWhere.append((banderaWhere?CLAUSULA_WHERE:CLAUSULA_AND) + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_ESTATUS + PARAMETRO_SIMPLE_SEGMENTO);
			banderaWhere = false;
		}
		if(parametrosConsulta.getObservaciones() != null){
			queryStringWhere.append((banderaWhere?CLAUSULA_WHERE:CLAUSULA_AND) + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_OBSERV + PARAMETRO_SIMPLE_SEGMENTO);
			banderaWhere = false;
		}
		if(parametrosConsulta.getModulo() != null){
			queryStringWhere.append((banderaWhere?CLAUSULA_WHERE:CLAUSULA_AND) + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_MODULO + PARAMETRO_SIMPLE_SEGMENTO);
		}
		queryStringGroupBy.append(" GROUP BY " + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_SISTEMA + "," + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_OPERACION + "," + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_ESTATUS);
		queryStringOrderBy.append(" ORDER BY " + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_SISTEMA + CLAUSULA_ASC + "," + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_OPERACION + CLAUSULA_ASC + "," + ALIAS_SELECT_ANIDADO + CAMPO_NOMBRE_ESTATUS + CLAUSULA_ASC);
		queryStringSelectPadre.append("" + queryStringSelectAnidado + queryStringWhere + queryStringGroupBy + queryStringOrderBy + ") BIT");
		return entityManager.createNativeQuery(queryStringSelectPadre.toString(), SapAmisBitacoras.class);
	}
}