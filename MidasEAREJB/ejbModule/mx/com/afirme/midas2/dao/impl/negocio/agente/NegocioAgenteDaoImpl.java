package mx.com.afirme.midas2.dao.impl.negocio.agente;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.agente.AgenteDTO_;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.agente.NegocioAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.Negocio_;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente_;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioAgenteDaoImpl extends JpaDao<BigDecimal, NegocioAgente> implements NegocioAgenteDao {

	@Override
	public List<NegocioAgente> listarAgentesAsociados(Long idToNegocio) {
		String queryString = "select model from NegocioAgente model where " + 
			" model.negocio.idToNegocio = :idToNegocio order by model.idToNegAgente";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToNegocio", idToNegocio);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

	@Override
	public List<Negocio> listarNegociosPorAgente(Integer idTcAgente) {
		String queryString = "select model."+NegocioAgente_.negocio.getName()+" from NegocioAgente model where model." + 
		AgenteDTO_.idTcAgente.getName()+" = :idTcAgente order by model."+NegocioAgente_.negocio.getName()+"."+Negocio_.descripcionNegocio.getName();
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTcAgente", idTcAgente);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	@Override
	public List<Negocio> listarNegociosPorAgenteClaveNegocio(Integer idTcAgente, String claveNegocio) {
		String queryString = "select model."+NegocioAgente_.negocio.getName()+" from NegocioAgente model where model." + 
		AgenteDTO_.idTcAgente.getName()+" = :idTcAgente and model."+NegocioAgente_.negocio.getName()+"."+Negocio_.claveNegocio.getName()+" = '"+claveNegocio+"' "+ 
				" order by model."+NegocioAgente_.negocio.getName()+"."+Negocio_.descripcionNegocio.getName();
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idTcAgente", idTcAgente);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

	@Override
	public List<Negocio> listarNegociosPorAgente(Integer idTcAgente, String cveTipoNegocio,
			Integer status, Boolean esExterno) {
		//Comentario integracion
		StringBuffer buffer = new StringBuffer();
		buffer.append("select model." + NegocioAgente_.negocio.getName()+ 
			" from NegocioAgente model where model.idTcAgente = :idTcAgente");
		if(cveTipoNegocio != null){
			buffer.append(" and model." + NegocioAgente_.negocio.getName() + "." + Negocio_.claveNegocio.getName() + "= :cveTipoNegocio");
		}
		if(status != null){
			buffer.append(" and model." + NegocioAgente_.negocio.getName() + "." + Negocio_.claveEstatus.getName() + "= :status");
		}
		if(status != null && status.equals(new Integer(1))){
			buffer.append(" and model." + NegocioAgente_.negocio.getName() + "." + Negocio_.fechaInicioVigencia.getName() + " < :fechaActual");
			buffer.append(" and ( model." + NegocioAgente_.negocio.getName() + "." + Negocio_.fechaFinVigencia.getName() + " > :fechaActual or "
					+ " model." + NegocioAgente_.negocio.getName() + "." + Negocio_.fechaFinVigencia.getName() + " is null) ");
		}
		if(esExterno != null){
			if(esExterno){
				buffer.append(" and model." + NegocioAgente_.negocio.getName() + "." + Negocio_.aplicaUsuarioExterno.getName() + " = true");
			}else{
				buffer.append(" and model." + NegocioAgente_.negocio.getName() + "." + Negocio_.aplicaUsuarioInterno.getName() + " = true");
			}
		}
		
		
		buffer.append(" order by model." + NegocioAgente_.negocio.getName() + "." + 
			Negocio_.descripcionNegocio.getName());
		Query query = entityManager.createQuery(buffer.toString());
		
		query.setParameter("idTcAgente", idTcAgente);
		if(cveTipoNegocio != null){
			query.setParameter("cveTipoNegocio", cveTipoNegocio);
		}
		if(status != null){
			query.setParameter("status", status);
		}
		if(status != null && status.equals(new Integer(1))){
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(new Date());
			gcFechaInicio.add(GregorianCalendar.DATE, 1);
			gcFechaInicio.add(GregorianCalendar.MILLISECOND, -1);
			
			System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
			
			query.setParameter("fechaActual", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
		}
		
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	@Override
	public long getTotalNegocioAgente(Negocio negocio) {
		String jpql = "select count(m) from NegocioAgente m where m.negocio = :negocio ";
		TypedQuery<Long> query = entityManager.createQuery(jpql, Long.class);
		query.setParameter("negocio", negocio);
		return query.getSingleResult();		
	}

	@Override
	public NegocioAgente findByNegocioAndAgente(Negocio negocio, Agente agente) {
		final String jpql = "select m from NegocioAgente m where" +
				" m.negocio = :negocio and m.idTcAgente = :idTcAgente";
		TypedQuery<NegocioAgente> query = entityManager.createQuery(jpql, NegocioAgente.class);
		query.setParameter("negocio", negocio);
		query.setParameter("idTcAgente", agente.getId());
		List<NegocioAgente> list = query.getResultList();
		if (list.size() == 0) {
			return null;
		}
		return list.get(0);		
	}
	
	
	


}
