package mx.com.afirme.midas2.dao.impl.siniestros.emisionFacturas;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.emisionFacturas.EmisionFacturaDao;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFactura;
import mx.com.afirme.midas2.dto.RespuestaBoolean_SP;
import mx.com.afirme.midas2.dto.emisionFactura.FacturaResultadoDTO;
import mx.com.afirme.midas2.dto.emisionFactura.FiltroFacturaDTO;

@Stateless
public class EmisionFacturaDaoImpl extends JpaDao<Long, EmisionFactura> implements EmisionFacturaDao{
	@EJB
	EntidadDao entidadDao;
	
	
	@Override
	public List<FacturaResultadoDTO> buscarFacturas(FiltroFacturaDTO filtroFacturaDTO) {
		
		List<FacturaResultadoDTO> lista  = new ArrayList<FacturaResultadoDTO>();
		String spName = "MIDAS.PKGSIN_FACTURACION.spBuscarFacturas"; 
		StoredProcedureHelper storedHelper 			= null;
		try {
			
			String propiedades = 
					"emisionId," +
					"estatusFactura," +
					"estatusFacturaDesc," +
					"estatusIngresoDesc," +
					"tipoRecuperacion," +
					"noSiniestro," +
					"oficinaDesc," +
					"totalFacturado," +
					"noFactura," +
					"tipoRecuperacion," +
					"fechaCancelacion," +
					"fechaFacturacion," +
					"nombreRazonSocial," +
					"facturadaPor";
			
			String columnasBaseDatos = 
				"emisionId," +
				"estatusFactura," +
				"estatusFacturaDesc," +
				"estatusIngresoDesc," +
				"tipoRecuperacionDesc," +
				"noSiniestro," +
				"oficinaDesc," +
				"totalFacturado," +
				"noFactura," +
				"tipoRecuperacion," +
				"fechaCancelacion," +
				"fechaFacturacion," +
				"nombreRazonSocial," +
				"facturadaPor";
			

			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(FacturaResultadoDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltro(storedHelper, filtroFacturaDTO);
			lista = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion busqueda recuperaciones ..."+descErr
						, Level.WARNING, e);    
			}
		}


		return lista;
		
	}
	
	private void agregaParametrosDelFiltro(StoredProcedureHelper storedHelper,
			FiltroFacturaDTO filtroFacturaDTO) {
		
		storedHelper.estableceParametro("p_NoFactura",        this.convertToNull( filtroFacturaDTO.getNoFactura()));
		storedHelper.estableceParametro("p_TipoRecuperacion", this.convertToNull( filtroFacturaDTO.getConcepto()));
		storedHelper.estableceParametro("p_EstatusFactura",   this.convertToNull( filtroFacturaDTO.getEstatusFactura()));
		storedHelper.estableceParametro("p_Oficina",          this.convertToNull( filtroFacturaDTO.getOficina()));
		storedHelper.estableceParametro("p_EstatusIngreso",   this.convertToNull( filtroFacturaDTO.getEstatusIngreso()));
		storedHelper.estableceParametro("p_FacturadoPor",     this.convertToNull( filtroFacturaDTO.getFacturadoPor()));
		storedHelper.estableceParametro("p_NombreRazonSocial",this.convertToNull( filtroFacturaDTO.getNombreRazonSocial()));
		storedHelper.estableceParametro("p_FechaInicioFacturacion", this.convertToNull( filtroFacturaDTO.getFechaFacturacionDe()));
		storedHelper.estableceParametro("p_FechaFinFacturacion",    this.convertToNull( filtroFacturaDTO.getFechaFacturacionHasta() ));
		storedHelper.estableceParametro("p_FechaInicioCancelacion", this.convertToNull( filtroFacturaDTO.getFechaCancelacionDe() ));
		storedHelper.estableceParametro("p_FechaFinCancelacion",    this.convertToNull( filtroFacturaDTO.getFechaFacturacionHasta() ));
		
	}
	
	private Object convertToNull(Object obj){
		if( obj instanceof String){
			obj = (obj != null && ((String)obj).equals(""))?null:obj;
		}else if ( obj instanceof Boolean){
			obj = (obj != null && ((Boolean)obj)== Boolean.FALSE)?null:obj;
		}
		return obj;
	}

	@Override
	public void insertarInformacion(EmisionFactura emisionFactura) 
	{			
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS.PKGSIN_FACTURACION.spEnviarFacturas";
		
		try {
			LogDeMidasEJB3.log("Entrando a spEnviarFacturas..." + this, Level.INFO, null);
								
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdEmisionFactura",emisionFactura.getId());
			
			storedHelper
			.estableceMapeoResultados(
					"mx.com.afirme.midas2.dao.siniestros.emisionFacturas.EmisionFacturaDao$Respuesta",
					"mensaje", "RESULTADO");
			
			storedHelper.obtieneResultadoSencillo();		

			LogDeMidasEJB3.log("Saliendo de spEnviarFacturas, pIdEmisionFactura - " 
					+ emisionFactura.getId() + "..." + this, Level.INFO, null);
			
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			LogDeMidasEJB3.log("Excepcion en BD de spEnviarFacturas..." + this, Level.WARNING, e);
			throw new RuntimeException(e);	
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en spEnviarFacturas..." + this, Level.WARNING, e);	
			throw new RuntimeException(e);	
		}			
	}
	
	@Override
	public void insertarRegistroCancelacion(String folioFactura)
	{
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS.PKGSIN_FACTURACION.spInsertarRegistroCancelacion";
		
		try {
			LogDeMidasEJB3.log("Entrando a spInsertarRegistroCancelacion..." + this, Level.INFO, null);
								
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pFolioFactura",folioFactura);
			
			storedHelper
			.estableceMapeoResultados(
					"mx.com.afirme.midas2.dao.siniestros.emisionFacturas.EmisionFacturaDao$Respuesta",
					"mensaje", "RESULTADO");
			
			storedHelper.obtieneResultadoSencillo();		

			LogDeMidasEJB3.log("Saliendo de spInsertarRegistroCancelacion, pFolioFactura - " 
					+ folioFactura + "..." + this, Level.INFO, null);
			
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			LogDeMidasEJB3.log("Excepcion en BD de spInsertarRegistroCancelacion..." + this, Level.WARNING, e);
			throw new RuntimeException(e);	
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en spInsertarRegistroCancelacion..." + this, Level.WARNING, e);	
			throw new RuntimeException(e);	
		}					
	}

	@Override
	public void priorizarInformacion(EmisionFactura emisionFactura) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List<String> seEncuentraEnTablaDeError(String folioFactura) {
		
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS.PKGSIN_FACTURACION.spObtenerErrorFactura";
		List<Respuesta> mensajesError = new ArrayList<Respuesta>();		
		
		try {
			LogDeMidasEJB3.log("Entrando a spObtenerErrorFactura..." + this, Level.INFO, null);
								
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pFolioFactura",folioFactura);
			
			storedHelper
			.estableceMapeoResultados(
					"mx.com.afirme.midas2.dao.siniestros.emisionFacturas.EmisionFacturaDao$Respuesta",
					"mensaje", "MENSAJEERROR");
				
			mensajesError = storedHelper.obtieneListaResultados();			
			
			LogDeMidasEJB3.log("Saliendo de spObtenerErrorFactura, pFolioFactura - " 
					+ folioFactura + "..." + this, Level.INFO, null);
			
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			LogDeMidasEJB3.log("Excepcion en BD de spObtenerErrorFactura..." + this, Level.WARNING, e);
			throw new RuntimeException(e);	
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en spObtenerErrorFactura..." + this, Level.WARNING, e);	
			throw new RuntimeException(e);	
		}		
		
		List<String> listaMensajes = new ArrayList<String>();
		
		for(Respuesta mensajeError:mensajesError)
		{
			listaMensajes.add(mensajeError.getMensaje());			
		}
		
		return listaMensajes;		
	}

	@Override
	public Boolean seGeneraFacturaConExito(EmisionFactura emisionFactura) {
		
		Boolean resultado;
		
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS.PKGSIN_FACTURACION.spEsFacturaCorrecta";
		RespuestaBoolean_SP respuesta;	
		
		try {
			LogDeMidasEJB3.log("Entrando a spEsFacturaCorrecta..." + this, Level.INFO, null);
								
			storedHelper = new StoredProcedureHelper(sp,  StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pFolioFactura",emisionFactura.getFolioFactura());
			
			storedHelper
			.estableceMapeoResultados(
					RespuestaBoolean_SP.class.getCanonicalName(),
					"resultadoSP", "RESULTADO");
				
			respuesta = (RespuestaBoolean_SP) storedHelper.obtieneResultadoSencillo();			
			resultado = respuesta.getResultadoSPBoolean();
			
			LogDeMidasEJB3.log("Saliendo de spEsFacturaCorrecta, pFolioFactura - " 
					+ emisionFactura.getFolioFactura() + "..." + this, Level.INFO, null);
			
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			LogDeMidasEJB3.log("Excepcion en BD de spEsFacturaCorrecta..." + this, Level.WARNING, e);
			throw new RuntimeException(e);	
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en spEsFacturaCorrecta..." + this, Level.WARNING, e);	
			throw new RuntimeException(e);	
		}				
		
		return resultado;
	}
	
	@Override
	public List<EmisionFactura> validaCancelacionesEnWFactura(){
		
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS.PKGSIN_FACTURACION.spValidarCancelaciones";
		List<EmisionFactura> emisionesFacturaRespuesta = null;	
		
		try {
			LogDeMidasEJB3.log("Entrando a spValidarCancelaciones..." + this, Level.INFO, null);
								
			storedHelper = new StoredProcedureHelper(sp,  StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper
			.estableceMapeoResultados(
					EmisionFactura.class.getCanonicalName(),
					new String[] {"id","folioFactura","estatusCancelacion" } , new String[] {"ID","FOLIO_FACTURA","ESTATUSCANCELACION" });
				
			emisionesFacturaRespuesta = storedHelper.obtieneListaResultados();		
			
			
			LogDeMidasEJB3.log("Saliendo de spValidarCancelaciones..." + this, Level.INFO, null);
			
		} catch (SQLException e) {				
			LogDeMidasEJB3.log("Excepcion en BD de spValidarCancelaciones..." + this, Level.WARNING, e);
			throw new RuntimeException(e);	
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en spEsFacturaCorrecta..." + this, Level.WARNING, e);	
			throw new RuntimeException(e);	
		}
		
		return emisionesFacturaRespuesta;
	}
}
