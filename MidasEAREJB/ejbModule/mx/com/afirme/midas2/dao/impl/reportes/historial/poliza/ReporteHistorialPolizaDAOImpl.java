package mx.com.afirme.midas2.dao.impl.reportes.historial.poliza;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.reportes.historial.poliza.ReporteHistorialPolizaDAO;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialPolizaDTO;

/**
 * Clase que implementa la l\u00f3gica de la interfaz ocupada de <b>ReporteHistorialPolizaDAO</b>
 * 
 * @author AFIRME
 * 
 * @since 27072016
 * 
 * @version 1.0
 *
 */
@Stateless
public class ReporteHistorialPolizaDAOImpl implements Serializable, ReporteHistorialPolizaDAO {
	
	/**
	 * <p>Cadena que contiene el nombre del STP para realizar la consulta del historial de polizas por agente </p>
	 */
	public static final String CONSULTA_HISTORIAL_POLIZA_POR_AGENTE =  "SEYCOS.PKGSIS_GRAL_REPORTES.REPORTE_HISTORIAL_POLIZAS";
	
	/**
	 * <p>Atributos de la clase de <b>ReporteHistorialPolizasDTO</b> que contendr\u00e1 la informaci\u00f3n deeste reporte</p> 
	 */
	public static final String [] ATRIBUTOS_CLASE_HISTORIAL_REPORTE = {"numCotizacion", "numCentroEmis", "numPoliza", "sitPoliza", 
		"numRenovPol", "numEndoso", "cvetDoctoVerp", "numLinNegocio", "numInciso", "sitIncisoPol", "motSitIncPol", "numContratante", 
		"fecEmision", "horaEmision", "iniVigencia", "finVigencia", "numAgente", "numMoneda", "cveAmis", "descVehiculo", "modelo", 
		"primaEndoso", "primaInciCot", "primaDM", "primaRT", "primaRC", "saRC", "primaGM", "sagm", "primaee", "saEndoso", "primaaj", 
		"primaav", "primaExecdeDM", "primaPerdidaTotalDM", "primaacccond", "saacccond", "primaextencionRC", "primaAdapacionConversion", 
		"saAdapacionConversion", "primaExecdedRT", "nombreConductor", "nombreTitular", "serie", "motor", "placas", "pctBonifComis", 
		"pctDesctoPol", "factDctoIgpmCob", "factDctoIgpmEnd", "factDctoIgpmPol", "pctDescCobDM", "pctDescCobRT", "pctDescCobRC", 
		"pctDescCobGM", "pctDescBobEnd", "pctDescCobAJ", "pctDescCobAV", "pctDescCobExecdedDM", "pctDescCobptDM", "pctDescCobaccCond", 
		"pctDescCobextRC", "pctDescCobAdapConv", "pctDescCobexecdedRT", "idOficina", "nombreOficina", "idGerencia", "nombreGerencia", 
		"idCentroOper", "nomCentroOper", "sitIncisoPolAct", "nomProducto", "nomContratante", "sistemaEmision", "polRCExt", 
		"valorVehiculo", "deducibleDM", "deducibleRT", "deducibleRC", "asistenciaJuridica", "asistenciaVial", "exencionDM", "exencionRT", 
		"extencionRC", "rcExcesoMuerte", "saRcExcesoMuerte", "deducibleRCExmu", "capacidadPasajeros", "servicio", "uso", 
		"tipoMercanciaancia", "numeroRemolques", "financiamiento", "gastosExpedicion", "formaPago", "iva", "adaptacionesConversiones", 
		"equipoEspecial", "idTopoliza", "idInciso", "idCotizacion", "idLinNegocio", "idRTEndoso", "idSolicitud", "observacionesEndoso", 
		"observacionesInciso", "idTitular", "cvelServicioAuto", "cvelUsoAuto", "tipoMercancia", "idFormaPago", "descripcionEndoso", 
		"cvetEndoso", "idPaquete", "descripcionPaquete", "idVersionPoliza"};
	
	/**
	 * <p>Campos que pertenecen al cursor del cual se obtiene del stored que se manda a llamar.</p>
	 */
	public static final String [] CAMPOS_CURSOR_HISTORIAL = {"NUM_COTIZACION", "NUM_CENTRO_EMIS", "NUM_POLIZA", "SIT_POLIZA", 
		"NUM_RENOV_POL", "NUM_ENDOSO", "CVE_T_DOCTO_VERP", "NUM_LIN_NEGOCIO", "NUM_INCISO", "SIT_INCISO_POL", "MOT_SIT_INC_POL", 
		"NUM_CONTRATANTE", "FEC_EMISION", "HORA_EMISION", "F_INI_VIGENCIA", "F_FIN_VIGENCIA", "NUM_AGENTE", "NUM_MONEDA", "CVE_AMIS", 
		"DESC_VEHICULO", "MODELO", "PRIMA_ENDOSO", "PRIMA_INCISO_T", "PRIMA_DM", "PRIMA_RT", "PRIMA_RC", "SA_RC", "PRIMA_GM", 
		"SA_GM", "PRIMA_EE", "SA_EE", "PRIMA_AJ", "PRIMA_AV", "PRIMA_EXEC_DED_DM", "PRIMA_PT_DM", "PRIMA_ACC_COND", "SA_ACC_COND", 
		"PRIMA_EXT_RC", "PRIMA_ADAP_CONV", "SA_ADAP_CONV", "PRIMA_EXEC_DED_RT", "NOMBRE_CONDUCTOR", "NOM_TITULAR", "SERIE", "MOTOR", 
		"PLACAS", "PCT_BONIF_COMIS", "PCT_DESCTO_POL", "FACT_DCTO_IGPMCOB", "FACT_DCTO_IGPMEND", "FACT_DCTO_IGPMPOL", 
		"PCT_DESC_COB_DM", "PCT_DESC_COB_RT", "PCT_DESC_COB_RC", "PCT_DESC_COB_GM", "PCT_DESC_COB_EE", "PCT_DESC_COB_AJ", 
		"PCT_DESC_COB_AV", "PCT_DESC_COB_EXEC_DED_DM", "PCT_DESC_COB_PT_DM", "PCT_DESC_COB_ACC_COND", "PCT_DESC_COB_EXT_RC", 
		"PCT_DESC_COB_ADAP_CONV", "PCT_DESC_COB_EXEC_DED_RT", "ID_OFICINA", "NOM_OFICINA", "ID_GERENCIA", "NOM_GERENCIA", 
		"ID_CENTRO_OPER", "NOM_CENTRO_OPER", "SIT_INCISO_POL_ACT", "NOM_PRODUCTO", "NOM_CONTRATANTE", "SISTEMA_EMISION", 
		"POL_RC_EXT", "VALOR_VEHICULO", "DEDUCIBLE_DM", "DEDUCIBLE_RT", "DEDUCIBLE_RC", "ASISTENCIA_JURIDICA", "ASISTENCIA_VIAL", 
		"EXENCION_DM", "EXENCION_RT", "EXTENCION_RC", "RC_EXCESO_MUERTE", "SA_RC_EXCESO_MUERTE", "DEDUCIBLE_RC_EXMU", 
		"CAPACIDAD_PASAJEROS", "SERVICIO", "USO", "TIPOMERCANCIA", "NUMERO_REMOLQUES", "FINANCIAMIENTO", "GASTOS_EXPEDICION", 
		"FORMA_PAGO", "IVA", "ADAPTACIONES_CONVERSIONES", "EQUIPO_ESPECIAL", "IDTOPOLIZA", "ID_INCISO", "ID_COTIZACION", 
		"ID_LIN_NEGOCIO", "ID_RT_ENDOSO", "ID_SOLICITUD", "OBSERVACIONESENDOSO", "OBSERVACIONESINCISO", "ID_TITULAR", 
		"CVEL_SERV_AUTO", "CVEL_USO_AUTO", "TIPO_MERC", "ID_FORMA_PAGO", "DESCENDOSO", "CVE_T_ENDOSO", "ID_PAQUETE", "NOM_PAQUETE", "ID_VERSION_POL"};

	/**
	 * SerialVerion.
	 */
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = LoggerFactory.getLogger(ReporteHistorialPolizaDAOImpl.class);

	public ReporteHistorialPolizaDAOImpl() {
	}

	@Override
	public List<ReporteHistorialPolizaDTO> consultarHistorialPolizaPorAgente(Long idAgente){
		return consultarHistoricoPolizasPorNumeroPoliza(idAgente, null);
	}

	@Override
	public List<ReporteHistorialPolizaDTO> consultarHistorialPorNumeroPoliza(Long numeroPoliza){
		return consultarHistoricoPolizasPorNumeroPoliza(null, numeroPoliza);
	}	
	
	
	@SuppressWarnings("unchecked")
	private List<ReporteHistorialPolizaDTO> consultarHistoricoPolizasPorNumeroPoliza(Long idAgente, Long numeroPoliza){
		List<ReporteHistorialPolizaDTO> historial = new ArrayList<ReporteHistorialPolizaDTO>();
		try{
    		StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper(CONSULTA_HISTORIAL_POLIZA_POR_AGENTE, StoredProcedureHelper.DATASOURCE_MIDAS);
    		storedProcedureHelper.estableceMapeoResultados(ReporteHistorialPolizaDTO.class.getCanonicalName(), ATRIBUTOS_CLASE_HISTORIAL_REPORTE, CAMPOS_CURSOR_HISTORIAL);
    		
    		storedProcedureHelper.estableceParametro("p_idAgente", idAgente);
    		storedProcedureHelper.estableceParametro("p_numeroPoliza", numeroPoliza);
    		
    		historial = storedProcedureHelper.obtieneListaResultados();

    	} catch(Exception err){
    		LOG.debug("error al obtener el stp " +  CONSULTA_HISTORIAL_POLIZA_POR_AGENTE, err);
    		throw new RuntimeException(err);
    	}
		return historial;
	}
}
