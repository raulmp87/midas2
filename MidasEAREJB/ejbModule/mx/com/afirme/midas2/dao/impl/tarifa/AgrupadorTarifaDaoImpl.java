package mx.com.afirme.midas2.dao.impl.tarifa;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.tarifa.AgrupadorTarifaDao;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaId_;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa_;

@Stateless
public class AgrupadorTarifaDaoImpl extends EntidadDaoImpl implements AgrupadorTarifaDao {

	@Override
	public List<AgrupadorTarifa> findByFilters(
			AgrupadorTarifa filtroAgrupadorTarifa) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<AgrupadorTarifa> criteriaQuery = cb.createQuery(AgrupadorTarifa.class);
		Root<AgrupadorTarifa> root = criteriaQuery.from(AgrupadorTarifa.class);
		criteriaQuery.distinct(true);
		Predicate predicado1 = cb.equal(cb.upper(root.get(AgrupadorTarifa_.claveNegocio)), filtroAgrupadorTarifa.getClaveNegocio());
		Predicate predicado2 = cb.equal(cb.upper(root.get(AgrupadorTarifa_.descripcionAgrupador)),filtroAgrupadorTarifa.getDescripcionAgrupador());
		if(filtroAgrupadorTarifa.getDescripcionAgrupador() != null){
			criteriaQuery.where(cb.and(predicado1, predicado2));
		}else{
			criteriaQuery.where(cb.or(predicado1, predicado2));
		}
				
		TypedQuery<AgrupadorTarifa> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long createNewVersion(BigDecimal idAgrupadorTarifa,BigDecimal idVersion) {
		Long nuevaVersion = null;
		String spName = "MIDAS.pkgPYT_Servicios.spPYT_NuevaVerAgrupadorTarifa";
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);

			storedHelper.estableceParametro("pIdAgrupadorTarifa",idAgrupadorTarifa);
			storedHelper.estableceParametro("pIdVersion", idVersion);

			nuevaVersion = Long.valueOf(storedHelper.ejecutaActualizar());
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName);
		}
		return nuevaVersion;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<AgrupadorTarifa> findByNegocio(
			String claveNegocio, String filtro) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<AgrupadorTarifa> criteriaQuery = cb.createQuery(AgrupadorTarifa.class);
		Root<AgrupadorTarifa> root = criteriaQuery.from(AgrupadorTarifa.class);
		criteriaQuery.multiselect(root.get(AgrupadorTarifa_.id).get(AgrupadorTarifaId_.idToAgrupadorTarifa), root.get(AgrupadorTarifa_.descripcionAgrupador)).distinct(true);
		Predicate predicado = cb.and(cb.equal(cb.upper(root.get(AgrupadorTarifa_.claveNegocio)), claveNegocio),
									cb.like(cb.upper(root.get(AgrupadorTarifa_.descripcionAgrupador)), "%"+filtro+"%"));
		
		criteriaQuery.where(predicado);
		
		Query query = entityManager.createQuery(criteriaQuery);
		 ((org.eclipse.persistence.jpa.JpaQuery)query).getDatabaseQuery().dontMaintainCache();
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public AgrupadorTarifa consultaVersionActiva(BigDecimal id) {
		final String query = "SELECT agrupador FROM AgrupadorTarifa agrupador "
				+ "WHERE agrupador.id.idToAgrupadorTarifa = :id  "
				+ "AND agrupador.claveEstatus = :claveStatus ";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		params.put("claveStatus", AgrupadorTarifa.ESTATUS_ACTIVO);
		
		List<AgrupadorTarifa> agrupadores = super.executeQueryMultipleResult(query, params);
		if(!agrupadores.isEmpty()){
			return agrupadores.get(0);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public AgrupadorTarifa consultaVersionActiva(Long id) {
		final String query = "SELECT agrupador FROM AgrupadorTarifa agrupador "
				+ "WHERE agrupador.id.idToAgrupadorTarifa = :id  "
				+ "AND agrupador.claveEstatus = :claveStatus ";
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("id", id);
		params.put("claveStatus", AgrupadorTarifa.ESTATUS_ACTIVO);
		List<AgrupadorTarifa> agrupadores = super.executeQueryMultipleResult(query, params);
		if(!agrupadores.isEmpty()){
			return agrupadores.get(0);
		}
		return null;
	}

}
