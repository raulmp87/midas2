package mx.com.afirme.midas2.dao.impl.tarifa;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaAutoDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;

@Stateless
public class TarifaAutoDaoImpl extends JpaDao<TarifaAutoId, TarifaAuto> implements TarifaAutoDao{

	/**
	 * Se obtiene la lista de tarifas por version id
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaAuto> findByTarifaVersionId(TarifaVersionId tarifaVersionId) {
		StringBuilder stm=new StringBuilder("");
		stm.append("SELECT tarifa FROM TarifaAuto tarifa where ");
		stm.append("tarifa.id.idMoneda=:idMoneda ");
		stm.append("and tarifa.id.idToRiesgo =:idToRiesgo ");
		stm.append("and tarifa.id.idConcepto =:idConcepto ");
		stm.append("and tarifa.id.idVerTarifa=:idVerTarifa");
		
		Query query =entityManager.createQuery(stm.toString());
		query.setParameter("idMoneda",tarifaVersionId.getIdMoneda());
		query.setParameter("idToRiesgo",tarifaVersionId.getIdRiesgo());
		query.setParameter("idConcepto",tarifaVersionId.getIdConcepto());
		query.setParameter("idVerTarifa",tarifaVersionId.getVersion());
		
		return query.getResultList();
	}
}
