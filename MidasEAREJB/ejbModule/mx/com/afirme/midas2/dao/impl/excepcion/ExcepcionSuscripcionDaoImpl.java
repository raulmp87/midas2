package mx.com.afirme.midas2.dao.impl.excepcion;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.excepcion.ExcepcionSuscripcionDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.excepcion.CondicionExcepcion;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.excepcion.ValorExcepcion;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ExcepcionSuscripcionDaoImpl extends JpaDao<Long, ExcepcionSuscripcion> implements ExcepcionSuscripcionDao {

	@Override
	public List<ExcepcionSuscripcion> listarExcepciones(String cveNegocio, Short nivelEvaluacion) {
		String queryString = "SELECT model FROM ExcepcionSuscripcion model WHERE " + 
		" model.cveNegocio = :cveNegocio  AND model.habilitado = true ";
		if(nivelEvaluacion != null){
			queryString = queryString + " AND model.nivelEvaluacion = :nivelEvaluacion";
		}
		queryString = queryString + " ORDER BY model.id";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("cveNegocio", cveNegocio);
		if(nivelEvaluacion != null){
			query.setParameter("nivelEvaluacion", nivelEvaluacion);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	@Override
	public List<ExcepcionSuscripcion> listarExcepciones(ExcepcionSuscripcion filtro) {
		String queryString = "SELECT model FROM ExcepcionSuscripcion model WHERE " + 
		" model.cveNegocio = :cveNegocio AND model.id >= 100 "; 
		if(filtro.getHabilitado()){
			queryString = queryString + " and model.habilitado = true ";
		}
		queryString = queryString + " order by model.id";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("cveNegocio", filtro.getCveNegocio());
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

	@Override
	public List<CondicionExcepcion> obtenerCondiciones(Long idExcepcion) {
		Object obj = null;
		String queryString = "select model from CondicionExcepcion model where " + 
		" model.excepcionSuscripcion.id = :idExcepcion ";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idExcepcion", idExcepcion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	
	@Override
	public CondicionExcepcion obtenerCondicion(Long idExcepcion, Integer tipoCondicion) {
		Object obj = null;
		String queryString = "select model from CondicionExcepcion model where " + 
		" model.excepcionSuscripcion.id = :idExcepcion " + 
		" and model.tipoCondicion = :tipoCondicion";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idExcepcion", idExcepcion);
		query.setParameter("tipoCondicion", tipoCondicion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		try{
			obj = query.getSingleResult();
		}catch(Exception e){			
		}
		return obj != null ? (CondicionExcepcion)obj : null;
	}

	@Override
	public List<ValorExcepcion> obtenerValoresDeCondicion(Long idToCondicion) {
		String queryString = "select model from ValorExcepcion model where " + 
		" model.condicionExcepcion.id = :idToCondicion " + 
		" order by model.id";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCondicion", idToCondicion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

}
