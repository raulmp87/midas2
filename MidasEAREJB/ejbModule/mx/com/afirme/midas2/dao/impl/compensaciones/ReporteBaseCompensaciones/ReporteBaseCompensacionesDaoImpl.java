package mx.com.afirme.midas2.dao.impl.compensaciones.ReporteBaseCompensaciones;

import java.sql.SQLException;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.compensaciones.reporteBaseCompensacionesDao.ReporteBaseCompensacionesDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaSegurosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaPagosSaldosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesCompDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesCompDTO.DatosReporteCompParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO.DatosReporteEstadoCuentaParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDerPolDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDerPolDTO.DatosRepDerPolParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagDevengarDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagDevengarDTO.DatosReportePagDevDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagoCompContra;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagoCompContra.DatosReportePagoCompContraParametrosDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
@Stateless
public class ReporteBaseCompensacionesDaoImpl implements ReporteBaseCompensacionesDao {
	private static final Logger LOG = LoggerFactory.getLogger(ReporteBaseCompensacionesDaoImpl.class);
	@Override
	
	public List<CaReportesPagDevengarDTO> obtenerRepPagadasDevengar(DatosReportePagDevDTO parametros) throws Exception {
		List<CaReportesPagDevengarDTO> listaResultado = null;//new CaReportesCompDTO>();
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "MIDAS.PKG_CA_REPORTES.obtenerRepPagadasDevengar";
		try {
			String[] nombreParametros = {
					"pRamo",
					"pCveAgente", 
					"pCvePromotor",
					"pCveProveedor",
					"pCveGerencia",
					"pIdNegocio",
					"pNomNegocio",
					"pAnioMes",
					"pPoliza"};
			Object[] valorParametros={
					parametros.getRamo(),
					parametros.getAgente(),
					parametros.getPromotor(),
					parametros.getProveedor(),
					parametros.getGerencia(),
					parametros.getIdNegocio(),
					parametros.getNombreNegocio(),
					parametros.getAnioMes(),
					parametros.getNoPoliza()};
			//TODO: falta agregar la relación de columnas
			String[] props = obtenerPropiedadesPagDev();
			String[] cols = obtenerColumnasPagDev();
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				
			}
			//se establecen los parametros.8
			storedHelper.estableceMapeoResultados(CaReportesPagDevengarDTO.class.getCanonicalName(),props,cols);
			
			LOG.info("Consultando reporte Compensaciones invocar: "+nombreSP+", Parametros: "+parametros.toString());
			
			//se obtiene la lista de objetos.
			listaResultado = storedHelper.obtieneListaResultados();
			
			LOG.info("Finaliza consulta reporte compensaciones. SP invocado: "+nombreSP+", Parametros: "+parametros.toString()+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0));
			
		} catch (SQLException e) {
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.BUSCAR,nombreSP, DatosBasesEmisionDTO.class, codErr, descErr);
				LOG.info("Excepcion en BD de PKG_CA_REPORTES.obtenerReporteCompensaciones..." + this, Level.WARNING, e);
				throw e;
		} catch (Exception e) {
			LOG.info("Ocurrio un error al generar Reporte Compensaciones. SP invocado: "+nombreSP, Level.SEVERE, e);
			throw e;
		}
		return listaResultado;
	}

	
	public List<CaReportesCompDTO> obtenerRepCompensaciones(DatosReporteCompParametrosDTO parametros)  throws Exception {
		List<CaReportesCompDTO> listaResultado = null;//new CaReportesCompDTO>();
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "MIDAS.PKG_CA_REPORTES.obtenerReporteCompensaciones";
		try {
			String[] nombreParametros = {
					"pNomAgente", 
					"pNomPromotor",
					"pNomProveedor",
					"pGerencia",
					"pRamo",
					"pIdNegocio",
					"pNomNegocio",
					"pFechaIni",
					"pFechaFin"};
			Object[] valorParametros={
					parametros.getAgente(),
					parametros.getPromotor(),
					parametros.getProveedor(),
					parametros.getGerencia(),
					parametros.getRamo(),
					parametros.getIdNegocio(),
					parametros.getNombreNegocio(),
					parametros.getFechaInicio(),
					parametros.getFechaFin()};
			//TODO: falta agregar la relación de columnas
			String[] props = obtenerPropiedades();
			String[] cols = obtenerColumnas();
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				
			}
			//se establecen los parametros.8
			storedHelper.estableceMapeoResultados(CaReportesCompDTO.class.getCanonicalName(),props,cols);
			
			LOG.info("Consultando reporte Compensaciones invocar: "+nombreSP+", Parametros: "+parametros.toString());
			
			//se obtiene la lista de objetos.
			listaResultado = storedHelper.obtieneListaResultados();
			
			LOG.info("Finaliza consulta reporte compensaciones. SP invocado: "+nombreSP+", Parametros: "+parametros.toString()+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0));
			
		} catch (SQLException e) {
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.BUSCAR,nombreSP, DatosBasesEmisionDTO.class, codErr, descErr);
				LOG.info("Excepcion en BD de PKG_CA_REPORTES.obtenerReporteCompensaciones..." + this, Level.WARNING, e);
				throw e;
		} catch (Exception e) {
			LOG.info("Ocurrio un error al generar Reporte Compensaciones. SP invocado: "+nombreSP, Level.SEVERE, e);
			throw e;
		}
		return listaResultado;
	}
	
	/**/
	public List<CaReportesPagoCompContra> obtenerRepPagoCompContra(DatosReportePagoCompContraParametrosDTO parametros) throws Exception{
		List<CaReportesPagoCompContra> listaResultado = null;
		StoredProcedureHelper storedHelper = null;
		String nombreSP="MIDAS.PKG_CA_REPORTES.obtenerReportePagosComp_Contra";
		try{
			String [] nombreParametros = {
					"pFechaInicio",
					"pFechaFinal",
					"pCveAgente",
					"pCvePromotor",
					"pCveProveedor",
					"pCveGerencia",
					"pRamo",
					"pPoliza"
					};
			
			Object [] valorParametros = {
					parametros.getFechaInicio() ,
					parametros.getFechaFinal(),					
					parametros.getClaveNombreAgente(),
					parametros.getClaveNombrePromotor(),
					parametros.getClaveNombreProveedor(),
					parametros.getClaveNombreGerencia(),
					parametros.getRamo(),
					parametros.getPoliza()					
			};
			
			String[] props = obtenerPropiedadesPagoCompensacionesContraprestaciones();
			String[] cols  = obtenerColumnasPagoCompensacionesContraprestaciones();
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			for(int i=0; i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
			}
			storedHelper.estableceMapeoResultados(CaReportesPagoCompContra.class.getCanonicalName(),props, cols);
			
			LOG.info("Consultando reporte Pago de Compensaciones y ContraPrestaciones invocar: "+nombreSP+", Parametros: "+parametros.toString());
			
			listaResultado = storedHelper.obtieneListaResultados();
			LOG.info("Finaliza consulta reporte Pago Compensacioens y ContraPrestaciones.SP invocado: "+nombreSP+", Parametros: "+parametros.toString()+"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0));
		}catch(SQLException e){	
			Integer codErr = null;
			String descErr = null;
			if(storedHelper != null){
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta(); 
			}
			StoredProcedureErrorLog.doLog(null, StoredProcedureErrorLog.TipoAccion.BUSCAR, nombreSP, DatosBasesEmisionDTO.class, codErr, descErr);
			LOG.info("Excepcion en BD de PKG_CA_REPORTES.obtenerReportePagosComp_Contra..."+ this,Level.WARNING,e);
			throw e;
		}
		catch(Exception e){
			LOG.info("Ocurrio un error al generar reporte Pago Compensaciones y Contraprestaciones .SP invocado: "+nombreSP,Level.SEVERE,e);
			throw e;
		}
		return listaResultado;
	}
/**/
	public List<CaBancaSegurosDTO> obtenerRepBancaSeguros(Map<String,Object> parametros) throws Exception{
		List<CaBancaSegurosDTO> listaResultado = new ArrayList<CaBancaSegurosDTO>();
		StoredProcedureHelper storedHelper = null;
		String nombreSP="MIDAS.PKG_CA_REPORTES.obtenerReporteBancaSeguros";
		try{
			String [] nombreParametros = {
				    "pAnio",
				    "pMes",
				    "pGerencia"
				    };
			
		Object [] valorParametros = {
				parametros.get("anio"),
				parametros.get("mes"),
				parametros.get("gerencia")
				
		};
		
		String[] props = obtenerPropiedadesBancaSeguros(); 
		String[] cols =  obtenerColumnasBancaSeguros();
		storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		for(int i=0; i<nombreParametros.length;i++){
			storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
          }
		storedHelper.estableceMapeoResultados(CaBancaSegurosDTO.class.getCanonicalName(), props, cols);
		LOG.info("Consultando reporte banca seguros invocar: "+nombreSP+", Parametros : "+parametros.toString());
		listaResultado = storedHelper.obtieneListaResultados();
		LOG.info("Finaliza consulta reporte banca seguros.SP invocado: "+nombreSP+", Parametros : "+parametros.toString()+"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0));
		}catch(SQLException e){
			Integer codErr = null;
			String descErr = null;
			if(storedHelper != null){
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta(); 
			}
			StoredProcedureErrorLog.doLog(null, StoredProcedureErrorLog.TipoAccion.BUSCAR, nombreSP, DatosBasesEmisionDTO.class, codErr, descErr);
			LOG.info("Excepcion en BD de PKG_CA_REPORTES.obtenerReporteBancaSeguros"+ this,Level.WARNING,e);
			throw e;
		}
		catch(Exception e){
			LOG.info("Ocurrio un error al generar reporte Banca Seguros .SP invocado: "+nombreSP,Level.SEVERE,e);
			throw e;
		}
		return listaResultado;
		
	}
	
	public List<CaPagosSaldosDTO> obtenerRepPagosSaldos(Map<String,Object> parametros) throws Exception{
    	List<CaPagosSaldosDTO> listaResultado = new ArrayList<CaPagosSaldosDTO>();
    	StoredProcedureHelper storedHelper = null;
    	String nombreSP="MIDAS.PKG_CA_REPORTES.obtenerRepPagosSaldos";
    	try{
    		String [] nombreParametros = {
    		    "pIdCompensacion",
    		    "pCotizacion",
    		    "pPoliza",
    		    "pAgente",
    		    "pPromotor",
    		    "pProveedor",
    		    "pFechaInicio",
    		    "pFechaFinal"
    		};
    		Object [] valorParametros = {
    				parametros.get("IdCompensacion"),
    				parametros.get("Cotizacion"),
    				parametros.get("Poliza"),
    				parametros.get("Agente"),
    				parametros.get("Promotor"),
    				parametros.get("Proveedor"),
    				parametros.get("FechaInicio"),
    				parametros.get("FechaFinal")
    		};
    		String[] props = obtenerPropiedadesPagosSaldos();
    		String[] cols = obtenerColumnasPagosSaldos();
    		storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
    		for(int i=0; i<nombreParametros.length;i++){
    			storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
    		}
    		storedHelper.estableceMapeoResultados(CaPagosSaldosDTO.class.getCanonicalName(),props , cols);
    		LOG.info("Consultando reporte pagos y saldos invocar: "+nombreSP+", Parametros : "+parametros.toString());
    		listaResultado = storedHelper.obtieneListaResultados();
    		LOG.info("Finaliza consulta reporte pagos y saldos.SP invocado: "+nombreSP+", Parametros : "+parametros.toString
    				()+"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0));
    	}catch(SQLException e){
			Integer codErr = null;
			String descErr = null;
			if(storedHelper != null){
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta(); 
			}
			StoredProcedureErrorLog.doLog(null, StoredProcedureErrorLog.TipoAccion.BUSCAR, nombreSP, DatosBasesEmisionDTO.class, codErr, descErr);
			LOG.info("Excepcion en BD de PKG_CA_REPORTES.obtenerReportePagosSaldos"+ this,Level.WARNING,e);
			throw e;
		}
		catch(Exception e){
			LOG.info("Ocurrio un error al generar reporte pagos y saldos .SP invocado: "+nombreSP,Level.SEVERE,e);
			throw e;
		}
		return listaResultado;
		
	}	
		
	public List<CaReportesDTO> obtenerRepEstadoCuenta(DatosReporteEstadoCuentaParametrosDTO parametros) throws Exception{
		List<CaReportesDTO> listaResultado = null;
		StoredProcedureHelper storedHelper = null;
		String nombreSP= "MIDAS.PKG_CA_REPORTES.obtenerReporteEstadoCuenta";
		try{
			String[] nombreParametros = {
					"pAnioMes",
		      		"pCveAgente",
		      		"pCvePromotor",
		      		"pCveProveedor"};
			
			Object[] valorParametros = {
					parametros.getAnioMes(),
					parametros.getCveAgente(),
					parametros.getCvePromotor(),
					parametros.getCveProveedor()};
			
			String[] props = obtenerPropiedadesEstadoCuenta();
			String[] cols = obtenerColumnasEstadoCuenta();
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for (int i=0; i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);	
			}
			
			storedHelper.estableceMapeoResultados(CaReportesDTO.class.getCanonicalName(), props, cols);
			
			LOG.info("Consultando reporte Estado Cuenta invocar: "+nombreSP+", Parametros: "+parametros.toString());
			
			listaResultado = storedHelper.obtieneListaResultados();
			
			LOG.info("Finaliza consulta reporte Estado Cuenta.SP invocado: "+nombreSP+", Parametros: "+parametros.toString()+"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0));
			} catch(SQLException e){
				Integer codErr = null;
				String descErr = null;
				if(storedHelper != null){
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta(); 
				}
				StoredProcedureErrorLog.doLog(null, StoredProcedureErrorLog.TipoAccion.BUSCAR, nombreSP, DatosBasesEmisionDTO.class, codErr, descErr);
				LOG.info("Excepcion en BD de PKG_CA_REPORTES.obtenerReporteEstadoCuenta..."+ this,Level.WARNING,e);
				throw e;
			} catch(Exception e){
				LOG.info("Ocurrio un error al generar reporte Estado Cuenta .SP invocado: "+nombreSP,Level.SEVERE,e);
				throw e;
			}
			return listaResultado;
		}
	
	/**/
	
	
	public List<CaReportesDerPolDTO> obtenerRepDerechoPoliza(DatosRepDerPolParametrosDTO parametros) throws Exception {

		List<CaReportesDerPolDTO> listaResultado = null;//new CaReportesCompDTO>();
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "MIDAS.PKG_CA_REPORTES.obtenerReporteDerechoPoliza";
		try {
			String[] nombreParametros = {
					"pNomAgente", 
					"pNomPromotor",
					"pRamo",
					"pIdNegocio",
					"pNomNegocio",
					"pFechaIni",
					"pFechaFin"};
			Object[] valorParametros={
					parametros.getAgente(),
					parametros.getPromotor(),
					parametros.getRamo(),
					parametros.getIdNegocio(),
					parametros.getNombreNegocio(),
					parametros.getFechaInicio(),
					parametros.getFechaFin()};
			//TODO: falta agregar la relación de columnas
			String[] props = obtenerPropiedadesDp();
			String[] cols = obtenerColumnasDp();
			
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				
			}
			//se establecen los parametros.8
			storedHelper.estableceMapeoResultados(CaReportesDerPolDTO.class.getCanonicalName(),props,cols);
			
			LOG.info("Consultando reporte Compensaciones invocar: "+nombreSP+", Parametros: "+parametros.toString());
			
			//se obtiene la lista de objetos.
			listaResultado = storedHelper.obtieneListaResultados();
			
			LOG.info("Finaliza consulta reporte compensaciones. SP invocado: "+nombreSP+", Parametros: "+parametros.toString()+
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0));
			
		} catch (SQLException e) {
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.BUSCAR,nombreSP, DatosBasesEmisionDTO.class, codErr, descErr);
				LOG.info("Excepcion en BD de PKG_CA_REPORTES.obtenerReporteCompensaciones..." + this, Level.WARNING, e);
				throw e;
		} catch (Exception e) {
			LOG.info("Ocurrio un error al generar Reporte Compensaciones. SP invocado: "+nombreSP, Level.SEVERE, e);
			throw e;
		}
		return listaResultado;
	
	}
	private String[] obtenerColumnasPagoCompensacionesContraprestaciones(){
		String[] cols = new String[]{
				"FECHAEMISION",
				"GERENCIA",
				"PROMOTORIA",
				"NOMBRE_BENEFICIARIO",
				"CVE_TIPO_PERSONA",
				"POLIZA",
				"RECIBO",
				"FECHARECIBODESDE",
				"FECHARECIBOHASTA",
				"MONEDA",
				"RAMO",
				"SUBRAMO",
				"NOMBRE_SOL_ASEGURADO",
				"POLCONT_REGISTRO_PROV_EMIS",
				"POLCONT_REGISTRO_PAGTRCH",
				"IMPORTEPAGO",
				"TIPO",
				"RAMOPRODUCTO",
				"PRIMA_NETA",
				"PRIMANETAEXTRAPRIMA",
				"DERECHOSPOLIZA",
				"PRIMATOTAL",
				"REFERENCIAPAGO",
				"PORCENTAJECOMISION",
				"CANTIDADCOMISIONBONO",
				"SALDOINI_BONOS",
				"BONOSDELMES",
				"BONOSPORDEVENGAR",
				"SALDOFIN_BONO_PORDEVENGAR",
				"SALDOINI_BONO_PORPAGAR",
				"BONOS_POR_PAGAR_MES",
				"BONOS_PAG_MES",
				"SALDOFIN_BONOS_PAGAR",
				"SALDOINI_BONOS_AMORTIZAR",
				"BONOS_AMORTIZAR_MES",
				"AMORTIZACION",
				"SALDOFIN_BONOS_AMORTIZAR",
				"ARTICULO_102",
				"CTA_1622_04",
				"CTA_1622_05",
				"CTA_2408_09",
				"CTA_2408_10",
				"CTA_1633_01",
				"CTA_2605_02",
				"CTA_2605_12",
				"CTA_1502_01",
				"TIPO_CONTRATO", 
				"UEN",
				"CVE_TIPO_ENTIDAD",
				"ID_NEGOCIO"
		};
		return cols;
	}
	
	
	
	private String[] obtenerColumnasEstadoCuenta(){
		String[] cols = new String[]{
				"IDENTIFICADOR_BEN_ID",
				"RFC_BENEFICIARIO",
				"NOMBRE_BENEFICIARIO",
				"DOMICILIO",
				"COLONIA",
				"CODIGOPOSTAL",
				"PROMOTORIA",
				"GERENCIA",
				"EJECUTIVO",
				"CENTROOPERACION",
				"NUMEROCEDULA",
				"NOMBRE_OFICINA",
				"SALDOINICIAL",
				"IMPORTE_COMGRABADASIVA",
				"IMPORTE_COMEXENTASIVA",
				"SUBTOTAL",
				"IMPORTE_IVAPAGADA",
				"IMPORTE_IVARETENIDO",
				"IMPORTE_ISRRETENIDO",
				"IMPORTE_NETOPAGO",
				"SALDO",
				"SALDOINICIAL_MESANTERIOR",
				"IMP_COMGRAB_MESANTERIOR",
				"IMP_COMGRAB_ACOMEJERCICIO",
				"IMP_COMEXEN_MESANTERIOR",
				"IMP_COMEXEN_ACOMEJERCICIO",
				"SUBTOTAL_MESANTERIOR",
				"SUBTOTAL_ACOMEJERCICIO",
				"IMP_IVAPAGADA_MESANTERIOR",
				"IMP_IVAPAGADA_ACOMEJERCICIO",
				"IMP_IVARETENIDO_MESANTERIOR",
				"IMP_IVARETENIDO_ACOMEJERCICIO",
				"IMP_ISRRETENIDO_MESANTERIOR",
				"IMP_ISRRETENIDO_ACOMEJERCICIO",
				"IMP_NETOPAGO_MESANTERIOR",
				"IMPORTE_NETOPAGO_ACOMEJERCICIO",
				"SALDO_MESANTERIOR",
				"FECHA_TRAMITE"
               
		};
		return cols;
	}
	private String[] obtenerPropiedadesPagoCompensacionesContraprestaciones(){
		String[] props = new String[]{
				"fechaEmision",
				"gerencia",
				"promotoria",
				"nombreAgentePromotorProveedor",
				"personaFisicaMoral",
				"numeroPoliza",
				"recibo",
				"fechaReciboDesde",
				"fechaReciboHasta",
				"moneda",
				"ramo",
				"subRamo",
				"nombreSolicitanteAsegurado",
				"polizaContableRegistroProvisionEmision",
				"polizaContableRegistroPagoTrCh",
				"importePago",
				"tipoContrato",
				"ramoProducto",
				"primaNeta",
				"primaNetaExtraPrima",
				"derechosPoliza",
				"primaTotal",
				"referenciaPago",
				"porcentajeComision",
				"cantidadComisionBono",
				"saldoInicialBonos",
				"bonosDelMes",
				"bonosPorDevengar",
				"saldoFinalBonosPorDevengar",
				"saldoInicialBonosPorPagar",
				"bonosPorPagarDelMes",
				"bonosPagadosEnElMes",
				"saldoFinalBonosPorPagar",
				"saldoInicialBonosPorAmortizar",
				"bonosPorAmortizarDelMes",
				"amortizacion",
				"saldoFinalDeBonosPorAmortizar",
				"articulo_102",
				"cta_1622_04",
				"cta_1622_05",
				"cta_2408_09",
				"cta_2408_10",
				"cta_1633_01",
				"cta_2605_02",
				"cta_2605_12",
				"cta_1502_01",
				"tipo_contrato",
				"uen",
				"cve_tipo_entidad",
				"id_negocio"
				
		};
		return props;
	}
	
	private String[] obtenerPropiedadesEstadoCuenta(){
		String[] props = new String[]{
				"identificador_ben_id",
				"rfc_beneficiario",
				"nombre_beneficiario",
				"domicilio",
				"colonia",
				"codigoPostal",
				"promotoria",
				"gerencia",
				"ejecutivo",
				"centroOperacion",
				"numeroCedula",
				"nombre_oficina",
				"saldoInicial",
				"importe_comGrabadasIva",
				"importe_comExentasIva",
				"subTotal",
				"importe_ivaPagada",
				"importe_ivaRetenido",
				"importe_isrRetenido",
				"importe_netoPago",
				"saldo",
				"saldoInicial_mesAnterior",
				"imp_comGrab_mesAnterior",
				"imp_comGrab_acomEjercicio",
				"imp_comExen_mesAnterior",
				"imp_comExen_acomEjercicio",
				"subTotal_mesAnterior",
				"subTotal_acomEjercicio",
				"imp_ivaPagada_mesAnterior",
				"imp_ivaPagada_acomEjercicio",
				"imp_ivaRetenido_mesAnterior",
				"imp_ivaRetenido_acomEjercicio",
				"imp_isrRetenido_mesAnterior",
				"imp_isrRetenido_acomEjercicio",
				"importe_netoPago_mesAnterior",
				"importe_netoPago_acomEjercicio",
				"saldo_mesAnterior",
				"fecha_tramite"
		};
		return props;
	}
	private String[] obtenerColumnas(){
		String[] cols = new String[]{
				"RAMO",
				"POLIZA",
				"FECHAEMISION",
				"IDCOMPENSACION",
				"CLAVEBENEFICIARIO",
				"NOMBRERAZONSOCIAL",
				"TIPOCONTRATO",
				"FRECUANCIAPAGO",
				"PRIMABASECALCULO",
				"MONTOPRIMABASE",
				"PORCENTAJECOMPRIMA",
				"MONTOCOMPRIMA",
				"PRIMACOMPSINIESTRALIDAD",
				"PORCCOMSINIESTRALIDAD",
				"DERECHOPOLIZA",
				"PORCENTAJEDERPOL",
				"MONTOCOMDERPOL",
				"IDNEGOCIO",
				"NOMBRENEGOCIO",
				"NUMEROENDOSO"
		};
		return cols;
	}
	
	
	private String[] obtenerPropiedades(){
		String[] props = new String[]{
				"ramo",
				"noPoliza",
				"fechaEmision",
				"idCompensacion",
				"clave",
				"razonSocial",
				"tipoContrato",
				"frec_pago",
				"prima_base",
				"imp_prima_base",
				"pct_com_prima",
				"mon_com_prima",
				"prima_com_sin",
				"pct_com_sin",
				"importeDerpol",
				"pct_com_dp",
				"mon_com_dp",
				"idNegocio",				
				"nombreNegocio",
				"numeroEndoso"
		};
		
		return props;
	}
	
	private String[] obtenerPropiedadesDp(){
		String[] props = new String[]{
				"ramo",
				"idCompensacion",
				"noPoliza",
				"numeroEndoso",
				"fechaEmision",
				"clave",
				"razonSocial",
				"frec_pago",
				"importeDerpol",
				"mto_pag_dp",
				"pct_pag_dp",
				"idNegocio",				
				"nombreNegocio"
		};
		
		return props;
	}
	
	private String[] obtenerColumnasDp(){
		String[] cols = new String[]{
				"RAMO",
				"IDCOMPENSACION",
				"POLIZA",
				"NUMEROENDOSO",
				"FECHAEMISION",
				"CLAVEBENEFICIARIO",
				"NOMBRERAZONSOCIAL",
				"FRECUANCIAPAGO",
				"DERECHOPOLIZA",
				"MONTOCOMDERPOL",
				"PORCENTAJEDERPOL",				
				"IDNEGOCIO",
				"NOMBRENEGOCIO"
		};
		return cols;
	}
	
	private String[] obtenerPropiedadesPagDev(){
		String[] props = new String[]{
				"noPoliza",
				"recibo",
				"fechaReciboDesde",
				"fechaReciboHasta",
				"moneda",
				"descMoneda",
				"ramo",
				"subRamo",
				"fechaContmizar",
				"polizaContReg",
				"agenteNoAgente",
				"perFisoPerMor",
				"tipoContrato",
				"claveAgente",
				"nombreAgente",
				"gerencia",
				"nomSolAseg",
				"ramoProducto",
				"prima_neta",
				"recargos",
				"derechos",
				"pct_comision",
				"sald_inicial_bon",
				"bonos_mes",
				"bon_x_pag_mes",
				"bon_pag_mes",
				"bon_x_devengar",
				"sal_final_bon_dev",
				"sal_ini_bon_pagar",
				"sal_fin_bon_x_pag",
				"sal_fin_bon_x_amort",
				"bon_amort_mes",
				"amortizar",
				"sal_ini_x_amortizar"
		};
		
		return props;
	}
	
	private String[] obtenerColumnasPagDev(){
		String[] cols = new String[]{
				"NUMEROPOLIZA", 
				"RECIBO", 
				"FECHA_RECIBO_DESDE", 
				"FECHA_RECIBO_HASTA",
				"MONEDA",   
				"DESCRIPCION_MONEDA", 
				"RAMO",     
				"SUBRAMO", 
				"FECHA_CONTABILIZA_MIZAR", 
				"POLIZA_CONTABLE_REGISTRO", 
				"AGENTE_NO_AGENTE",
				"PERSONA_FISICA_MORAL", 
				"TIPO_CONTRATO", 
				"CLAVE_AGENTE", 
				"NOMBRE_AGENTE", 
				"GERENCIA", 
				"NOM_SOLI_ASEG", 
				"RAMO_PRODUCTO", 
				"PRIMA_NETA", 
				"RECARGOS", 
				"DERECHOS", 
				"PORCENTAJE_COMISION",
				"SALDO_INICIO_BONOS", 
				"BONOS_MES", 
				"BONOS_POR_PAGAR_MES", 
				"BONOS_PAGADOS_MES",
				"BONOS_POR_DEVENGAR", 
				"SALDO_FIN_BONOS_POR_DEVENGAR", 
				"SALDO_INICIO_BONOS_PAGAR", 
				"SALDO_FIN_BONOS_POR_PAGAR", 
				"SALDO_INICIO_BONOS_AMORTIZAR", 
				"BONOS_AMORTIZAR_MES", 
				"AMORTIZACION",
				"SALDO_FIN_BONOS_AMORTIZAR"

		};
		return cols;
	}

private String[] obtenerPropiedadesBancaSeguros(){
	String[] props = new  String[]{
	         "numeroPoliza",
	         "gerencia",
	         "contratante",
	         "primaEmitida",
	         "primaPagada",
	         "primaDevengada",
	         "costoSiniestralidad",
	         "fechaCreacion"
	};
	return props;
}

 private  String[] obtenerColumnasBancaSeguros(){
	     String[] cols = new String[]{
	    		 "NUMEROPOLIZA",
	    		 "GERENCIA",
	    		 "CONTRATANTE",
	    		 "PRIMAEMITIDA",
	    		 "PRIMAPAGADA",
	    		 "PRIMADEVENGADA",
	    		 "COSTOSINIESTRALIDAD",
	    		 "FECHACREACION"
	     };
	     return cols;
    }

private String[] obtenerPropiedadesPagosSaldos(){
    String[] props = new String[]{
    	"nombreContratante",
    	"subRamo",
    	"poliza",
    	"fechaInicial",
    	"fechaFinal",
    	"numeroEndoso",
    	"centroCosto",
    	"nombreBeneficiario",
    	"tipoPersona",
    	"tipoContrato",
    	"saldoInicial",
    	"pagado",
    	"fechaPago",
    	"moneda",
    	"tipoCambio",
    	"saldoFinal"
   };
    
 return props;
}

private String[] obtenerColumnasPagosSaldos(){
String[] cols = new String[]{
		"NOMBRECONTRATANTE",
    	"SUBRAMO",
    	"POLIZA",
    	"FECHAINICIAL",
    	"FECHAFINAL",
    	"NUMEROENDOSO",
    	"CENTROCOSTO",
    	"NOMBREBENEFICIARIO",
    	"TIPOPERSONA",
    	"TIPOCONTRATO",
    	"SALDOINICIAL",
    	"PAGADO",
    	"FECHAPAGO",
    	"MONEDA",
    	"TIPOCAMBIO",
    	"SALDOFINAL"	
};
return cols;
}

}
