/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaBitacoraDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;

import org.apache.log4j.Logger;

@Stateless
public class CaBitacoraDaoImpl implements CaBitacoraDao {	
	public static final String USUARIO = "usuario";
	public static final String HORA = "hora";
	public static final String MOVIMIENTO = "movimiento";
	public static final String VALOR_ANTERIOR = "valorAnterior";
	public static final String VALOR_POSTERIOR = "valorPosterior";
	public static final String CACONFIGURACIONBANCA = "configuracionBancaId";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaBitacoraDaoImpl.class);
	
	public void save(CaBitacora entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaBitacora 	::		CaBitacoraDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaBitacora 	::		CaBitacoraDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaBitacora 	::		CaBitacoraDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaBitacora entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaBitacora 	::		CaBitacoraDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaBitacora.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaBitacora 	::		CaBitacoraDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaBitacora 	::		CaBitacoraDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaBitacora update(CaBitacora entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaBitacora 	::		CaBitacoraDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaBitacora result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaBitacora 	::		CaBitacoraDaoImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaBitacora 	::		CaBitacoraDaoImpl	::	update	::	ERROR	::	",re);
	        throw re;
        }
    }
    
    public CaBitacora findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaBitacoraDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaBitacora instance = entityManager.find(CaBitacora.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaBitacoraDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaBitacoraDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<CaBitacora> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaBitacoraDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaBitacora model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaBitacoraDaoImpl	::	findByProperty	::	FIN	::	");
					return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaBitacoraDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaBitacora> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaBitacora> findByHora(Object hora
	) {
		return findByProperty(HORA, hora
		);
	}
	
	public List<CaBitacora> findByMovimiento(Object movimiento
	) {
		return findByProperty(MOVIMIENTO, movimiento
		);
	}
	
	public List<CaBitacora> findByValorAnterior(Object valorAnterior
	) {
		return findByProperty(VALOR_ANTERIOR, valorAnterior
		);
	}
	
	public List<CaBitacora> findByValorPosterior(Object valorPosterior
	) {
		return findByProperty(VALOR_POSTERIOR, valorPosterior
		);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaBitacora> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaBitacoraDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaBitacora model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaBitacoraDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaBitacoraDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
