package mx.com.afirme.midas2.dao.impl.siniestros.catalogo;

import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.catalogo.CatalogoSiniestroDao;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService.CatalogoFiltro;

@Stateless
public class CatalogoSiniestroDaoImpl extends JpaDao<Long, String> implements
		CatalogoSiniestroDao {

	@Override
	public <E extends Entidad, K extends CatalogoFiltro> List<E> buscar(
			Class<E> entidad, K filtro) {	
		return super.findByFilterObject(entidad, filtro);		
	}

	public <E extends Entidad, K extends CatalogoFiltro, T extends Object>  List<E> 
		buscar( Class<E> entidad, K filtro, String ordenadoPor){
		return super.findByFilterObject(entidad, filtro, ordenadoPor);			
	}
		
}
