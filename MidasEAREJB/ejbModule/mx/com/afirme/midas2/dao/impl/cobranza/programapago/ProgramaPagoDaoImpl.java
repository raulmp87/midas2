package mx.com.afirme.midas2.dao.impl.cobranza.programapago;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.cobranza.programapago.ProgramaPagoDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToProgPago;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.cobranza.programapago.ProgramaPagoDTO;
import mx.com.afirme.midas2.service.cobranza.programapago.ProgramaPagoService.Saldo;
@Stateless
public class ProgramaPagoDaoImpl extends JpaDao<Long, ToProgPago> implements ProgramaPagoDao{
	
	private int simulaStoreCall = 0;
	public static final String CLAVE_CANAL_VENTA_MIDAS = "2";

	@Override
	public List<IncisoAutoCot> obtenerAseguradosCot(BigDecimal idToCotizacion) {

		List<IncisoAutoCot> incisoAutoCot = new ArrayList<IncisoAutoCot>();
		
		/*
		final String queryString = "select model from "
				+ IncisoAutoCot.class.getSimpleName() + " model where model."
				+ "incisoCotizacionDTO.id.idToCotizacion" + "= :propertyValue";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("propertyValue", idToCotizacion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		incisoAutoCot =  query.getResultList();
		*/
		List<IncisoAutoCot> listaEndosos = new ArrayList<IncisoAutoCot>();
		try {
			listaEndosos = this.obtenerAsegurados(idToCotizacion);
			//System.out.println("TOTAL DE CLIENTES: " + listaEndosos.size());
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion al obtener el listado de autos: " + e.getMessage(), Level.INFO, null);
		}
		
		//incisoAutoCot.addAll(listaEndosos);
		
		//incisoAutoCot = listaEndosos;
		
		return listaEndosos;
	}
	
	@SuppressWarnings("unchecked")
	private List<IncisoAutoCot> obtenerAsegurados(BigDecimal idToCotizacion) throws Exception{
		List<IncisoAutoCot> resList = new ArrayList<IncisoAutoCot>();
		 
		StoredProcedureHelper storedHelper = new StoredProcedureHelper("MIDAS.PKGAUT_RECUOTIFICAII.obtenerClientes","jdbc/MidasDataSource");

		String [] propiedades = new String[]{"id", "nombreAsegurado"};
		String [] columnas = new String[]{"ID", "NOMBREASEGURADO"};
		
		storedHelper.estableceMapeoResultados(IncisoAutoCot.class.getCanonicalName(),
				propiedades,
			    columnas);
		
		/*Se agregan los parametros de entrada*/
		storedHelper.estableceParametro("pidToCotizacion", idToCotizacion);
		resList = storedHelper.obtieneListaResultados();
		//System.out.println("Lista de Resultados EJB: " + resList.size());
		//for(IncisoAutoCot e : resList){
		//	System.out.println("e: " + e.getId() + " : " + e.getNombreAsegurado());
		//}
		return resList;
	}
	
	@SuppressWarnings("unchecked")
	public List<ClienteGenericoDTO> obtenerTodosLosAsegurados(BigDecimal idToCotizacion){
		List<ClienteGenericoDTO> resList = new ArrayList<ClienteGenericoDTO>();
		 
		StoredProcedureHelper storedHelper;
		try {
			storedHelper = new StoredProcedureHelper("MIDAS.PKGAUT_RECUOTIFICAII.obtenerClientes","jdbc/MidasDataSource");

		String [] propiedades = new String[]{"idClienteString", "nombreCompleto"};
		String [] columnas = new String[]{"ID", "NOMBREASEGURADO"};
		
		storedHelper.estableceMapeoResultados(ClienteGenericoDTO.class.getCanonicalName(),
				propiedades,
			    columnas);
		
		/*Se agregan los parametros de entrada*/
		storedHelper.estableceParametro("pidToCotizacion", idToCotizacion);
		resList = storedHelper.obtieneListaResultados();
		//System.out.println("Lista de Resultados EJB: " + resList.size());
		//for(ClienteGenericoDTO e : resList){
		//	System.out.println("e: " + e.getIdClienteString() + " : " + e.getNombreCompleto());
		//}
		return resList;
		} catch (Exception e1) {
			LogDeMidasEJB3.log("Excepcion generada al obtener el listado de asegurados: " + e1.getMessage(), Level.WARNING, null);
			return resList;
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> simulaEmitePoliza(CotizacionDTO cotizacionDTO,
			String canalVenta, Integer recuotificaFlag) {

		if (simulaStoreCall == 1) {

			//System.out.println("Inicio de de invocacion SP spDAN_SimulaEmitePoliza");

			StoredProcedureHelper storedHelper = null;
			Map<String, String> mensaje = new HashMap<String, String>();
			try {
				LogDeMidasEJB3.log("Datos de Emision: pIdCotizacion ="
						+ cotizacionDTO.getIdToCotizacion()
						+ " pDiasPorDevengar="
						+ cotizacionDTO.getDiasPorDevengar()
						+ " pGastosExpedicion="
						+ cotizacionDTO.getDerechosPoliza()
						+ " pPorcentajeRPF="
						+ cotizacionDTO.getPorcentajePagoFraccionado()
						+ " pCodigoUsuario="
						+ cotizacionDTO.getCodigoUsuarioModificacion()
						+ " pFolioPolizaAsociada="
						+ cotizacionDTO.getFolioPolizaAsociada()
						+ " claveCanalVenta="
						+ CLAVE_CANAL_VENTA_MIDAS, Level.INFO, null);

				storedHelper = new StoredProcedureHelper(
						"MIDAS.PKGDAN_RECUOTIFICA.spDAN_SimulaEmitePoliza");

				storedHelper.estableceParametro("pIdCotizacion", cotizacionDTO
						.getIdToCotizacion());
				storedHelper.estableceParametro("pDiasPorDevengar",
						cotizacionDTO.getDiasPorDevengar());
				storedHelper.estableceParametro("pGastosExpedicion",
						cotizacionDTO.getDerechosPoliza());
				storedHelper.estableceParametro("pPorcentajeRPF", cotizacionDTO
						.getPorcentajePagoFraccionado());
				storedHelper.estableceParametro("pCodigoUsuario", cotizacionDTO
						.getCodigoUsuarioModificacion());
				storedHelper.estableceParametro("pFolioPolizaAsociada",
						cotizacionDTO.getFolioPolizaAsociada());
				storedHelper.estableceParametro("pCanalVenta", canalVenta);
				storedHelper
						.estableceParametro("pRecuotifica", recuotificaFlag);

				Integer idPoliza = Integer.valueOf(storedHelper
						.ejecutaActualizar());

				LogDeMidasEJB3.log("Valore de resultado de operacion: " + idPoliza, Level.INFO, null);

				if (idPoliza != null) {
					mensaje.put("res", "30");// Exito
					mensaje.put("codigoRes", idPoliza.toString());
					mensaje
							.put(
									"resDesc",
									"La p\u00f3liza se emitio correctamente.</br> El n\u00famero de  P\u00f3liza es: ");
				} else {
					String descErr = null;
					mensaje.put("res", "10");// Error
					if (storedHelper != null) {
						descErr = storedHelper.getDescripcionRespuesta();
						mensaje.put("resDesc",
								"Error al ejecutar store de simulacion de emicion de poliza: "
										+ descErr);
					}
				}
				return mensaje;
			} catch (SQLException e) {

				Integer codErr = null;
				String descErr = null;

				mensaje.put("res", "10");// Error
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
					mensaje.put("resDesc", "Error al generar la P\u00f3liza: "
							+ descErr);
				}

				StoredProcedureErrorLog.doLog(cotizacionDTO
						.getCodigoUsuarioModificacion(),
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						"MIDAS.pkgDAN_Recuotifica.spDAN_SimulaEmitePoliza",
						CotizacionDTO.class, codErr, descErr);
				LogDeMidasEJB3.log(
						"Excepcion en BD de spDAN_SimulaEmitePoliza..." + this, Level.WARNING, null);
				return mensaje;
			} catch (Exception e) {
				LogDeMidasEJB3.log("Excepcion general en PolizaFacade.emitePoliza...", Level.WARNING, null);
				mensaje.put("res", "10");// Error
				mensaje.put("resDesc", "spDAN_SimulaEmitePoliza: "
						+ e.getMessage());
				return mensaje;
			}

		} // fin if

		else {
			// Llamada a store STP generar Recibos

			StoredProcedureHelper storedHelper = null;
			Map<String, String> mensaje = new HashMap<String, String>();
			try {
				LogDeMidasEJB3.log("Datos de Emision: pIdCotizacion ="
						+ cotizacionDTO.getIdToCotizacion()
						+ " pDiasPorDevengar="
						+ cotizacionDTO.getDiasPorDevengar()
						+ " pGastosExpedicion="
						+ cotizacionDTO.getDerechosPoliza()
						+ " pPorcentajeRPF="
						+ cotizacionDTO.getPorcentajePagoFraccionado()
						+ " pCodigoUsuario="
						+ cotizacionDTO.getCodigoUsuarioModificacion()
						+ " pFolioPolizaAsociada="
						+ cotizacionDTO.getFolioPolizaAsociada()
						+ " claveCanalVenta="
						+ CLAVE_CANAL_VENTA_MIDAS, Level.INFO, null);
						
				//storedHelper = new StoredProcedureHelper("MIDAS.PKG_RECIBOS.StpGenerarRecibos");
				storedHelper = new StoredProcedureHelper("MIDAS.PKG_RECUOTIFICACION.StpMascaraGenerarProgramaPago");

				// MAPEO DE CURSOR
				//storedHelper.estableceMapeoResultados(ProgramaPagoDTO.class.getCanonicalName(),
				//		"primaNeta," + "recargos," + "derechos," + "iva," + "ivaTotal,",
				//		"IMPPRIMANETA," + "IMPRCGOSPAGOFR," + "IMPDERECHOS," + "IMPIVA," + "IMPPRIMATOTAL,");

				storedHelper.estableceParametro("pIdTOCotizacion",cotizacionDTO.getIdToCotizacion());
				storedHelper.estableceParametro("pOrigen", "D");
				storedHelper.estableceParametro("pRecuotifica", recuotificaFlag);
				storedHelper.estableceParametro("pDiasPorDevengar",cotizacionDTO.getDiasPorDevengar());
				storedHelper.estableceParametro("pGastosExpedicion",cotizacionDTO.getDerechosPoliza());
				storedHelper.estableceParametro("pPorcentajeRPF", cotizacionDTO.getPorcentajePagoFraccionado());
				storedHelper.estableceParametro("pCodigoUsuario", cotizacionDTO.getCodigoUsuarioModificacion());
				storedHelper.estableceParametro("pFolioPolizaAsociada",cotizacionDTO.getFolioPolizaAsociada());
				storedHelper.estableceParametro("pCanalVenta", canalVenta);
				

				 Integer idPoliza = Integer.valueOf(storedHelper.ejecutaActualizar());

				//ProgramaPagoDTO res = (ProgramaPagoDTO) storedHelper
				//		.obtieneResultadoSencillo();
				//Integer idPoliza = null;

				 /*
				if (res != null) {
					LOG.info("Operacion ejecutada exitosamente");
					LOG.info("Prima neta " + res.getPrimaNeta());
					LOG.info("Recargos " + res.getRecargos());
					LOG.info("Derechos " + res.getDerechos());
					LOG.info("Iva " + res.getIva());
					LOG.info("Total: " + res.getIvaTotal());
					idPoliza = 1;
				} else {
					LOG.info("Resultado de operacion nulo");
				}*/

				if (idPoliza != null) {
					mensaje.put("res", "30");// Exito
					mensaje.put("codigoRes", idPoliza.toString());
					mensaje
							.put("resDesc",
									"Resultado de StpGenerarRecibos para generar informacion exitoso");
				} else {
					String descErr = null;
					mensaje.put("res", "10");// Error
					if (storedHelper != null) {
						descErr = storedHelper.getDescripcionRespuesta();
						mensaje.put("resDesc","Error al ejecutar store de simulacion de emicion de poliza: "
										+ descErr);
					}
				}
				return mensaje;
			
			} catch (SQLException e) {

				Integer codErr = null;
				String descErr = null;

				mensaje.put("res", "10");// Error
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
					mensaje.put("resDesc",
							"Error al generar informaicon de cotizacion: "
									+ descErr);
				}

				StoredProcedureErrorLog.doLog(cotizacionDTO
						.getCodigoUsuarioModificacion(),
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						"MIDAS.pkgDAN_Recuotifica.StpGenerarRecibos",
						CotizacionDTO.class, codErr, descErr);
				LogDeMidasEJB3.log("Excepcion en BD de StpGenerarRecibos..." + this, Level.INFO, null);
				return mensaje;
			} catch (Exception e) {
				LogDeMidasEJB3.log("Excepcion general en StpGenerarRecibos..." + this, Level.INFO, null);
				mensaje.put("res", "10");// Error
				mensaje.put("resDesc", "StpGenerarRecibos: " + e.getMessage());
				return mensaje;
			}
		}// fin else
	}
	
	public Saldo obtenerSaldoOriginal(CotizacionDTO cotizacionDTO, BigDecimal idProgPago, Long numInciso ){
		
		Saldo saldoOriginal = new Saldo();
		boolean executeStore = true;
		StoredProcedureHelper storedHelper = null;

		LogDeMidasEJB3.log("Datos de Emision: pIdCotizacion ="
				+ cotizacionDTO.getIdToCotizacion() + " pDiasPorDevengar="
				+ cotizacionDTO.getDiasPorDevengar() + " pGastosExpedicion="
				+ cotizacionDTO.getDerechosPoliza() + " pPorcentajeRPF="
				+ cotizacionDTO.getPorcentajePagoFraccionado()
				+ " pCodigoUsuario="
				+ cotizacionDTO.getCodigoUsuarioModificacion()
				+ " pFolioPolizaAsociada="
				+ cotizacionDTO.getFolioPolizaAsociada() 
				+ " claveCanalVenta="
				+ CLAVE_CANAL_VENTA_MIDAS,Level.INFO, null);

		if (simulaStoreCall == 1) {


			String packageName = "MIDAS.PKGAUT_RECUOTIFICAII.getImportesTotales";
			// String packageName = "MIDAS.PKG_RECIBOS.StpGenerarRecibos";
			// CotizacionDTO cotizacionDTO =
			// this.getCotizacionDTOInfo(idToCotizacion);

			if (executeStore) {
				try {

					storedHelper = new StoredProcedureHelper(packageName);

					storedHelper.estableceMapeoResultados(ProgramaPagoDTO.class
							.getCanonicalName(),

					"primaNeta," + "recargos," + "derechos," + "iva,"
							+ "ivaTotal,",

					"IMPPRIMANETA," + "IMPRCGOSPAGOFR," + "IMPDERECHOS,"
							+ "IMPIVA," + "IMPPRIMATOTAL,");
					
					/*Se agregan los parametros de entrada*/
					storedHelper.estableceParametro("pidToCotizacion", 
							cotizacionDTO.getIdToCotizacion());
					
					/*Cuando se busque los totales de un programa de pago y un inciso*/
					storedHelper.estableceParametro("pprogPagoId", 
							idProgPago);
					storedHelper.estableceParametro("pnumInciso", 
							new BigDecimal(numInciso));


					ProgramaPagoDTO res = (ProgramaPagoDTO) storedHelper
							.obtieneResultadoSencillo();

					saldoOriginal.setImpPrimaNeta(res.getPrimaNeta());
					saldoOriginal.setImpDerechos(res.getDerechos());
					saldoOriginal.setImpRcgosPagoFR(res.getRecargos());
					saldoOriginal.setImpIVA(res.getIva());
					saldoOriginal.setImpPrimaTotal(res.getIvaTotal());

					/*
					if (saldoOriginal != null) {
						System.out.println((saldoOriginal.getImpPrimaNeta() != null ? saldoOriginal
										.getImpPrimaNeta().toString()
										: "Prima Neta Nula"));
						System.out.println((saldoOriginal.getImpRcgosPagoFR() != null ? saldoOriginal
										.getImpRcgosPagoFR().toString()
										: "Recargos Nula"));
						System.out.println((saldoOriginal.getImpDerechos() != null ? saldoOriginal
										.getImpDerechos().toString()
										: "Derechos Nula"));
						System.out.println((saldoOriginal.getImpIVA() != null ? saldoOriginal
										.getImpIVA().toString()
										: "IVA Nula"));
						System.out.println((saldoOriginal.getImpPrimaTotal() != null ? saldoOriginal
										.getImpPrimaTotal().toString()
										: "Prima Total Nula"));
					} else {
						System.out.println("Saldo Original Nulo");
					}
					*/

				} catch (SQLException e) {

					Integer codErr = null;
					String descErr = null;
					
					/*
					if (storedHelper != null) {
						codErr = storedHelper.getCodigoRespuesta();
						descErr = storedHelper.getDescripcionRespuesta();
					}
					*/
					/*
					StoredProcedureErrorLog.doLog("user",
							StoredProcedureErrorLog.TipoAccion.BUSCAR,
							packageName, Saldo.class, codErr, descErr);
					*/
					LogDeMidasEJB3.log("Excepcion generada al obtener Saldo original", Level.WARNING, null);
					
					/*
					System.out.println(
							"-- obtenerSaldoOriginal() codErr {}, descErr {} " +
							codErr + descErr + e);
					*/
					
				} catch (Exception e) {
					LogDeMidasEJB3.log(
									"-- Excepcion generada en invocacion a store, obtener saldo original" +
									e, Level.WARNING, null);

				}
			}

		} else {

			//LOG.info("Invocacion Obtener Saldo original Store: StpGenerarRecibos");
			//System.out.println("Invocacion Obtener Saldo original Store: StpGenerarProgramaPago");

			// String packageName = "MIDAS.PKGDAN_RECUOTIFICA.STPOBTENERPRIMASCOTIZACION";
			//String packageName = "MIDAS.PKG_RECIBOS.StpGenerarRecibos";
			String packageName = "MIDAS.PKGAUT_RECUOTIFICAII.getImportesTotales";
			//System.out.println("Ejecucion de procedimiento: MIDAS.PKGAUT_RECUOTIFICAII.getImportesTotales" );

			LogDeMidasEJB3.log("Datos de Emision: pIdCotizacion ="
					+ cotizacionDTO.getIdToCotizacion() + " pDiasPorDevengar="
					+ cotizacionDTO.getDiasPorDevengar()
					+ " pGastosExpedicion=" + cotizacionDTO.getDerechosPoliza()
					+ " pPorcentajeRPF="
					+ cotizacionDTO.getPorcentajePagoFraccionado()
					+ " pCodigoUsuario="
					+ cotizacionDTO.getCodigoUsuarioModificacion()
					+ " pFolioPolizaAsociada="
					+ cotizacionDTO.getFolioPolizaAsociada()
					+ " claveCanalVenta=" 
					+ CLAVE_CANAL_VENTA_MIDAS, Level.INFO, null);

			if (executeStore) {
				try {

					storedHelper = new StoredProcedureHelper(packageName,"jdbc/MidasDataSource");
					storedHelper.estableceMapeoResultados(ProgramaPagoDTO.class.getCanonicalName(),
					"idtocotizacion,"+ "numProgPago," + "numinciso," + "primaNeta," + "impComAgt," + "derechos," + "recargos,"  + "iva," + "ivaTotal,",
					"IDTOCOTIZACION," + "PROGPAGOID," + "NUMINCISO," + "IMPPRIMANETA," + "IMPCOMAGT," + "IMPDERECHOS," + "IMPRCGOSPAGOFR,"  + "IMPIVA," + "IMPPRIMATOTAL,");

					/*Se agregan los parametros de entrada*/
					storedHelper.estableceParametro("pidToCotizacion", cotizacionDTO.getIdToCotizacion());
					/*Cuando se busque los totales de un programa de pago y un inciso*/
					
					BigDecimal pprogPagoId = null;
					BigDecimal pnumInciso = null;
					
					storedHelper.estableceParametro("pprogPagoId", pprogPagoId);
					storedHelper.estableceParametro("pnumInciso", pnumInciso);

					ProgramaPagoDTO res = (ProgramaPagoDTO) storedHelper.obtieneResultadoSencillo();

					saldoOriginal.setImpPrimaNeta(res.getPrimaNeta());
					saldoOriginal.setImpDerechos(res.getDerechos());
					saldoOriginal.setImpRcgosPagoFR(res.getRecargos());
					saldoOriginal.setImpIVA(res.getIva());
					saldoOriginal.setImpPrimaTotal(res.getIvaTotal());
					/*
					if (saldoOriginal != null) {
						System.out.println((saldoOriginal.getImpPrimaNeta() != null ? saldoOriginal
										.getImpPrimaNeta().toString()
										: "Prima Neta Nula"));
						System.out.println((saldoOriginal.getImpRcgosPagoFR() != null ? saldoOriginal
										.getImpRcgosPagoFR().toString()
										: "Recargos Nula"));
						System.out.println((saldoOriginal.getImpDerechos() != null ? saldoOriginal
										.getImpDerechos().toString()
										: "Derechos Nula"));
						System.out.println((saldoOriginal.getImpIVA() != null ? saldoOriginal
										.getImpIVA().toString()
										: "IVA Nula"));
						System.out.println((saldoOriginal.getImpPrimaTotal() != null ? saldoOriginal
										.getImpPrimaTotal().toString()
										: "Prima Total Nula"));
					} else {
						System.out.println("Saldo Original Nulo");
					}
					*/
				} catch (SQLException e) {
					
					/*
					Integer codErr = null;
					String descErr = null;
					*/

					if (storedHelper != null) {
						/*codErr = storedHelper.getCodigoRespuesta();
						descErr = storedHelper.getDescripcionRespuesta();*/
					}
					/*
					StoredProcedureErrorLog.doLog("user",
							StoredProcedureErrorLog.TipoAccion.BUSCAR,
							packageName, Saldo.class, codErr, descErr);
					*/
					LogDeMidasEJB3.log("Excepcion generada al obtener Saldo original: " + e.getMessage(), Level.WARNING, null);
					/*
					System.out.println(
							"-- obtenerSaldoOriginal() codErr {}, descErr {}" +
							codErr + descErr + e);
					*/
				} catch (Exception e) {
					LogDeMidasEJB3.log(
									"-- Excepcion generada en invocacion a store, obtener saldo original" +
									e, Level.INFO, null);

				}
			}
			// Si la bandera esta apagada mandamos los valores en duro
			else {

				saldoOriginal.setImpBomoProm(saldoOriginal.getImpBomoProm()
						.add(new BigDecimal("0")));
				saldoOriginal.setImpBonComis(saldoOriginal.getImpBonComis()
						.add(new BigDecimal("0")));
				saldoOriginal.setImpBonComPN(saldoOriginal.getImpBonComPN()
						.add(new BigDecimal("0")));
				saldoOriginal.setImpBonComRPF(saldoOriginal.getImpBonComRPF()
						.add(new BigDecimal("0")));
				saldoOriginal.setImpBonoAgt(saldoOriginal.getImpBonoAgt().add(
						new BigDecimal("0")));
				saldoOriginal.setImpCesionDerechosAgt(saldoOriginal
						.getImpCesionDerechosAgt().add(new BigDecimal("0")));
				saldoOriginal.setImpCesionDerechosProm(saldoOriginal
						.getImpCesionDerechosProm().add(new BigDecimal("0")));
				saldoOriginal.setImpComAgt(saldoOriginal.getImpComAgt().add(
						new BigDecimal("0")));
				saldoOriginal.setImpComAgtPN(saldoOriginal.getImpComAgtPN()
						.add(new BigDecimal("0")));
				saldoOriginal.setImpComAgtRPF(saldoOriginal.getImpComAgtRPF()
						.add(new BigDecimal("0")));
				saldoOriginal.setImpComSup(saldoOriginal.getImpComSup().add(
						new BigDecimal("0")));
				saldoOriginal.setImpComSupPN(saldoOriginal.getImpComSupPN()
						.add(new BigDecimal("0")));
				saldoOriginal.setImpComSupRPF(saldoOriginal.getImpComSupRPF()
						.add(new BigDecimal("0")));
				saldoOriginal.setImpDerechos(saldoOriginal.getImpDerechos()
						.add(new BigDecimal("20")));
				saldoOriginal.setImpIVA(saldoOriginal.getImpIVA().add(
						new BigDecimal("0")));
				saldoOriginal.setImpOtrosImptos(saldoOriginal
						.getImpOtrosImptos().add(new BigDecimal("0")));
				saldoOriginal.setImpPrimaNeta(saldoOriginal.getImpPrimaNeta()
						.add(new BigDecimal("138591.19")));
				saldoOriginal.setImpPrimaTotal(saldoOriginal.getImpPrimaTotal()
						.add(new BigDecimal("144336")));
				saldoOriginal.setImpRcgosPagoFR(saldoOriginal
						.getImpRcgosPagoFR().add(new BigDecimal("5744.81")));
				saldoOriginal.setImpSobreComAgt(saldoOriginal
						.getImpSobreComAgt().add(new BigDecimal("0")));
				saldoOriginal.setImpSobreComProm(saldoOriginal
						.getImpSobreComProm().add(new BigDecimal("0")));
				saldoOriginal.setImpSobreComUdiAgt(saldoOriginal
						.getImpSobreComUdiAgt().add(new BigDecimal("0")));
				saldoOriginal.setImpSobreComUdiProm(saldoOriginal
						.getImpSobreComUdiProm().add(new BigDecimal("0")));
			}
		}// fin del else
		return saldoOriginal;
		
		
	}
	
	
	
public Saldo obtenerSaldoOriginalPorInciso(CotizacionDTO cotizacionDTO, BigDecimal idProgPago, Long numInciso ){
		
		Saldo saldoOriginal = new Saldo();
		StoredProcedureHelper storedHelper = null;

		String packageName = "MIDAS.PKGAUT_RECUOTIFICAII.getImportesTotales";

		try {

			storedHelper = new StoredProcedureHelper(packageName,"jdbc/MidasDataSource");
			storedHelper.estableceMapeoResultados(ProgramaPagoDTO.class.getCanonicalName(),
			"idtocotizacion,"+ "numProgPago," + "numinciso," + "primaNeta," + "impComAgt," + "derechos," + "recargos,"  + "iva," + "ivaTotal,",
			"IDTOCOTIZACION," + "PROGPAGOID," + "NUMINCISO," + "IMPPRIMANETA," + "IMPCOMAGT," + "IMPDERECHOS," + "IMPRCGOSPAGOFR,"  + "IMPIVA," + "IMPPRIMATOTAL,");

			/*Se agregan los parametros de entrada*/
			storedHelper.estableceParametro("pidToCotizacion", cotizacionDTO.getIdToCotizacion());
			/*Cuando se busque los totales de un programa de pago y un inciso*/
					
			//BigDecimal pprogPagoId = null;
			//BigDecimal pnumInciso = null;
					
			storedHelper.estableceParametro("pprogPagoId", idProgPago);
			storedHelper.estableceParametro("pnumInciso", numInciso);

			ProgramaPagoDTO res = (ProgramaPagoDTO) storedHelper.obtieneResultadoSencillo();

			saldoOriginal.setImpPrimaNeta(res.getPrimaNeta());
			saldoOriginal.setImpDerechos(res.getDerechos());
			saldoOriginal.setImpRcgosPagoFR(res.getRecargos());
			saldoOriginal.setImpIVA(res.getIva());
			saldoOriginal.setImpPrimaTotal(res.getIvaTotal());

			/*
			if (saldoOriginal != null) {
				System.out.println("SALDO TOTAL DE INCISO EN LLAMADA A STORE");
				System.out.println((saldoOriginal.getImpPrimaNeta() != null ? saldoOriginal.getImpPrimaNeta().toString(): "Prima Neta Nula"));
				System.out.println((saldoOriginal.getImpRcgosPagoFR() != null ? saldoOriginal.getImpRcgosPagoFR().toString(): "Recargos Nula"));
				System.out.println((saldoOriginal.getImpDerechos() != null ? saldoOriginal.getImpDerechos().toString(): "Derechos Nula"));
				System.out.println((saldoOriginal.getImpIVA() != null ? saldoOriginal.getImpIVA().toString(): "IVA Nula"));
				System.out.println((saldoOriginal.getImpPrimaTotal() != null ? saldoOriginal.getImpPrimaTotal().toString(): "Prima Total Nula"));
			} else {
				System.out.println("Saldo Original Nulo");
			}
			*/

				} catch (SQLException e) {
					
					/*
					Integer codErr = null;
					String descErr = null;
					*/

					if (storedHelper != null) {
						/*codErr = storedHelper.getCodigoRespuesta();
						descErr = storedHelper.getDescripcionRespuesta();*/
					}
					/*
					StoredProcedureErrorLog.doLog("user",
							StoredProcedureErrorLog.TipoAccion.BUSCAR,
							packageName, Saldo.class, codErr, descErr);
					*/
					LogDeMidasEJB3.log("Excepcion generada al obtener Saldo original: " + e.getMessage(), Level.WARNING,null);
					/*
					System.out.println(
							"-- obtenerSaldoOriginal() codErr {}, descErr {}" +
							codErr + descErr + e);
					*/
				} catch (Exception e) {
					LogDeMidasEJB3.log("-- Excepcion generada en invocacion a store, obtener saldo original" + e.getMessage(), Level.WARNING, null);

				} 
		return saldoOriginal;
	}
}
