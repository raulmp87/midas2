package mx.com.afirme.midas2.dao.impl.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.tarifa.GruposDMDao;
import mx.com.afirme.midas2.dto.ClaveGrupoComboDTO;

@Stateless
public class GruposDMDaoImpl implements GruposDMDao {

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public List<ClaveGrupoComboDTO> findAll() {
		String spName="MIDAS.pkgAUT_Servicios.spAUT_GruposDM";
		String [] atributosDTO = { "idGrupo" };
		String [] columnasCursor = { "IDGRUPODM"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.ClaveGrupoComboDTO", atributosDTO, columnasCursor);
			
			return storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName,e);
		}
	}

	@Override
	public List<ClaveGrupoComboDTO> listRelated(Object id) {
		return null;
	}

	@Override
	public ClaveGrupoComboDTO findById(BigDecimal id) {
		return null;
	}

	@Override
	public ClaveGrupoComboDTO findById(CatalogoValorFijoId id) {
		return null;
	}

	@Override
	public ClaveGrupoComboDTO findById(double id) {
		return null;
	}

}
