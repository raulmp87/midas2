package mx.com.afirme.midas2.dao.impl.siniestros.pagos.facturas;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaPteComplMonitor;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.pagos.facturas.RecepcionFacturaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.dto.siniestros.BitacoraEventoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
import mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas.RecepcionFacturaServiceImpl;

import org.apache.log4j.Logger;

import com.js.util.StringUtil;

@Stateless
public class RecepcionFacturaDaoImpl  extends EntidadDaoImpl implements RecepcionFacturaDao {

	private static final Logger log = Logger.getLogger(RecepcionFacturaDaoImpl.class);
	
	@Override
	public Long obtenerBatchId(){
		Query query = entityManager.createNativeQuery("SELECT MIDAS.FACELE_BATCH_ID_SEQ.NEXTVAL FROM DUAL");
		Object result = query.getSingleResult();
		return ((BigDecimal)result).longValue();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public DocumentoFiscal validaSiExiste(Integer idProveedor, DocumentoFiscal facturaSiniestro){
		DocumentoFiscal docFiscal = null;
		if(facturaSiniestro.getTipo().equals(DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue())){
			docFiscal = this.validaSiExisteFactura(idProveedor, facturaSiniestro);
		}else if (facturaSiniestro.getTipo().equals(DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue())){
			docFiscal = this.validaSiExisteNotaCredito(idProveedor, facturaSiniestro);
		}
		return docFiscal;
	}
	
	
	private DocumentoFiscal validaSiExisteNotaCredito(Integer idProveedor, DocumentoFiscal notaCredito){
//		DocumentoFiscal notaCreditoAsociada = validacionAntigua(idProveedor, notaCredito);
		DocumentoFiscal notaCreditoAsociada = validacionNueva(idProveedor, notaCredito);
		return notaCreditoAsociada;
	}
	
	private DocumentoFiscal validacionNueva(Integer idProveedor, DocumentoFiscal notaCredito){
		StringBuilder queryString =null;
		DocumentoFiscal notaCreditoAsociada = null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		PrestadorServicio proveedor = this.findById(PrestadorServicio.class, idProveedor);
		String rfc = proveedor.getPersonaMidas().getRfc();
		parameters.put("rfc", rfc);
		parameters.put("numeroFactura", notaCredito.getNumeroFactura());
		parameters.put("estatusProcesado", DocumentoFiscal.EstatusDocumentoFiscal.PROCESADA.getValue());
		parameters.put("estatusError", DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
		parameters.put("tipo", DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue());
		parameters.put("origen", DocumentoFiscal.OrigenDocumentoFiscal.LSN);
		queryString = new StringBuilder("SELECT nota from " + DocumentoFiscal.class.getSimpleName() + " nota ");
		queryString.append(" WHERE  nota.rfcEmisor = :rfc ");
		queryString.append(" AND  nota.numeroFactura = :numeroFactura ");
		queryString.append(" AND  nota.estatus NOT IN (:estatusProcesado,:estatusError )");
		queryString.append(" AND  nota.tipo = :tipo ");
		queryString.append(" AND  nota.origen = :origen ");
		List<DocumentoFiscal>  notas = this.executeQueryMultipleResult(queryString.toString(), parameters);
		log.info(" NOTA de Credito - Valida el proveedor "+idProveedor+" con RFC "+notaCredito.getRfcEmisor());
		if(notas !=null && notas.size()>0){
			log.info("Ya existe la nota de credito");
			notaCreditoAsociada = notas.get(0);
		}else{
			log.info("No Existe Notas de Credito");
		}
		return notaCreditoAsociada;
	}
	
	
	private DocumentoFiscal validacionAntigua(Integer idProveedor, DocumentoFiscal notaCredito){
		StringBuilder queryString =null;
		DocumentoFiscal notaCreditoAsociada = null;
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("idProveedor", idProveedor);
		parameters.put("numeroFactura", notaCredito.getNumeroFactura());
		parameters.put("estatusProcesado", DocumentoFiscal.EstatusDocumentoFiscal.PROCESADA.getValue());
		parameters.put("estatusError", DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
		parameters.put("tipo", DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue());
		parameters.put("origen", DocumentoFiscal.OrigenDocumentoFiscal.LSN);
		queryString = new StringBuilder("SELECT nota from " + DocumentoFiscal.class.getSimpleName() + " nota ");
		queryString.append(" JOIN nota.conjuntoDeNota conjunto ");
		queryString.append(" JOIN conjunto.ordenesCompra ordenCompra ");
		queryString.append(" WHERE  ordenCompra.idBeneficiario = :idProveedor ");
		queryString.append(" AND  nota.numeroFactura = :numeroFactura ");
		queryString.append(" AND  nota.estatus NOT IN (:estatusProcesado,:estatusError )");
		queryString.append(" AND  nota.tipo = :tipo ");
		queryString.append(" AND  nota.origen = :origen ");
		List<DocumentoFiscal>  notas = this.executeQueryMultipleResult(queryString.toString(), parameters);
		log.info(" NOTA de Credito - Valida el proveedor "+idProveedor+" con RFC "+notaCredito.getNumeroFactura());
		if(notas !=null && notas.size()>0){
			log.info("Ya existe la nota de credito");
			notaCreditoAsociada = notas.get(0);
		}else{
			log.info("No Existe Notas de Credito");
		}
		return notaCreditoAsociada;
	}
	
	private DocumentoFiscal validaSiExisteFactura(Integer idProveedor, DocumentoFiscal facturaSiniestro){
		
		StringBuilder queryString =null;
		DocumentoFiscal facturaAsociada = null;
		Map<String, Object> parameters = new HashMap<String, Object>();	
		parameters.put("idProveedor", idProveedor);
		parameters.put("numeroFactura", facturaSiniestro.getNumeroFactura());
		parameters.put("estatusProcesado", DocumentoFiscal.EstatusDocumentoFiscal.PROCESADA.getValue());
		parameters.put("estatusError", DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
		parameters.put("tipo", DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue());
		parameters.put("origen", DocumentoFiscal.OrigenDocumentoFiscal.LSN);
		queryString = new StringBuilder("SELECT factura from " + DocumentoFiscal.class.getSimpleName() + " factura ");
		queryString.append(" JOIN factura.ordenesCompra ordenCompra ");
		queryString.append(" WHERE  ordenCompra.idBeneficiario = :idProveedor ");
		queryString.append(" AND  factura.numeroFactura = :numeroFactura ");
		queryString.append(" AND  factura.estatus NOT IN (:estatusProcesado,:estatusError ) ");
		queryString.append(" AND  factura.tipo = :tipo ");
		queryString.append(" AND  factura.origen = :origen ");
		List<DocumentoFiscal>  facturas = this.executeQueryMultipleResult(queryString.toString(), parameters);
		log.info(" Valida el proveedor "+idProveedor+" con RFC "+facturaSiniestro.getNumeroFactura());
		if(facturas !=null && facturas.size()>0){
			log.info("Tiene "+facturas.size()+" Asociadas");
			facturaAsociada = facturas.get(0);
		}else{
			log.info("No tiene Facturas Asociadas");
		}
		return facturaAsociada;
		
	}
	
	
	@Override
	public List<EnvioValidacionFactura> obtenerFacturasAsignables(Long batchId){
		List<String>  estatusValidos = new ArrayList<String>();
		estatusValidos.add(RecepcionFacturaServiceImpl.FACTURA_PROCESADA);
		estatusValidos.add(RecepcionFacturaServiceImpl.FACTURA_ERROR);
		Map<String, Object> params = new HashMap<String, Object>();	
		params.put("batchId",batchId );
		StringBuilder queryString =null;
		/*queryString = new StringBuilder("SELECT envio from " + EnvioValidacionFactura.class.getSimpleName() + " envio ");
		queryString.append(" WHERE envio.idBatch = :batchId");
		queryString.append(" AND envio.factura.estatus IN ( '"+RecepcionFacturaServiceImpl.FACTURA_PROCESADA+"' , '"
				+RecepcionFacturaServiceImpl.FACTURA_ERROR+"' )");*/
		
		queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura( envio.factura ) ");
		queryString.append(" FROM "+ EnvioValidacionFactura.class.getSimpleName() + " envio ");
		queryString.append(" WHERE envio.idBatch = :batchId ");
		queryString.append(" AND envio.factura.estatus =  '"+RecepcionFacturaServiceImpl.FACTURA_PROCESADA+"' ");
		return this.executeQueryMultipleResult(queryString.toString(), params);
//		queryString = new StringBuilder("SELECT envio.factura.id ");
//		queryString.append(" FROM "+ EnvioValidacionFactura.class.getSimpleName() + " envio ");
//		queryString.append(" WHERE envio.idBatch = "+batchId);
//		queryString.append(" AND envio.factura.estatus IN ( '"+RecepcionFacturaServiceImpl.FACTURA_PROCESADA+"' , '"
//				+RecepcionFacturaServiceImpl.FACTURA_ERROR+"' )");
//		TypedQuery<Long> query = this.entityManager.createQuery(queryString.toString(), Long.class);
//		List<Long> lista = query.getResultList();
	}
	
	public List<EnvioValidacionFactura> obtenerFacturasProcesadas(Long idBatch){
		Map<String, Object> params = new HashMap<String, Object>();	
		params.put("batchId",idBatch );
		StringBuilder queryString = new StringBuilder(" SELECT envio FROM " +EnvioValidacionFactura.class.getSimpleName()+" envio ");
		queryString.append(" WHERE envio.idBatch = :batchId ");
		/*StringBuilder queryString = new StringBuilder(" SELECT new mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura " +
		" ( envio.id, envio.factura, envio.id ) " );
		queryString.append(" FROM "+ EnvioValidacionFactura.class.getSimpleName() + " envio ");
		queryString.append(" WHERE envio.idBatch =  :batchId ");*/
		return this.executeQueryMultipleResult(queryString.toString(), params);
	}

	@Override
	public List<DocumentoFiscal>  obtenerFacturasRegistradaByOrdenCompra(Long idOrdenCompra, String estatusFactura) {
		
		StringBuilder queryString =null;
        Map<String, Object> parameters = new HashMap<String, Object>();  
        parameters.put("idOrdenCompra", idOrdenCompra);
        if (!StringUtil.isEmpty(estatusFactura)){
        	parameters.put("estatus", estatusFactura);
        }
        queryString = new StringBuilder("SELECT factura from " + DocumentoFiscal.class.getSimpleName() + " factura ");
        queryString.append(" JOIN factura.ordenesCompra ordenCompra ");
        queryString.append(" WHERE  ordenCompra.id = :idOrdenCompra ");
        if (!StringUtil.isEmpty(estatusFactura)){
            queryString.append(" AND  factura.estatus  = :estatus ");

        }
        List<DocumentoFiscal>  facturas = this.executeQueryMultipleResult(queryString.toString(), parameters);
        log.info(" Valida el idOrdenCompra "+idOrdenCompra+ " Estatus : "+estatusFactura);
        return facturas;
	}
	
	@Override
	public List<DocumentoFiscal> recepcionFacturaService(OrdenCompraProveedorDTO filtroAgrupador){
		StringBuilder queryString =null;
		String claveOficinaSin      =  null;
		String consecutivoReporteSin= null;
		String anioReporteSin		= null;
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("estatusCancelada", DocumentoFiscal.EstatusDocumentoFiscal.CANCELADA.getValue());
		parameters.put("estatusPagada", DocumentoFiscal.EstatusDocumentoFiscal.PAGADA.getValue());
		parameters.put("estatusRegistrada", DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue());
		parameters.put("tipo", DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue());
		parameters.put("origen", DocumentoFiscal.OrigenDocumentoFiscal.LSN);
		parameters.put("nombreAgrupador", "%"+filtroAgrupador.getNombreAgrupador()+"%");
		parameters.put("tipoAgrupador", filtroAgrupador.getTipoAgrupador());
		parameters.put("compania", filtroAgrupador.getCompania());
		parameters.put("totalDe", filtroAgrupador.getTotalDe());
		parameters.put("totalHasta", filtroAgrupador.getTotalHasta());
		parameters.put("fechaRegistroDe", filtroAgrupador.getFechaRegistroDe());
		parameters.put("fechaRegistroHasta", filtroAgrupador.getFechaRegistroHasta());
		parameters.put("numeroDeFactura", "0");
		parameters.put("numeroSiniestroTercero", (filtroAgrupador.getSiniestroTercero()!=null && !filtroAgrupador.getSiniestroTercero().equals(""))?"%"+filtroAgrupador.getSiniestroTercero()+"%":null);
		try{
			if(filtroAgrupador.getNumeroSiniestro()!=null && !filtroAgrupador.getNumeroSiniestro().equals("")){
				String[] elementosNumeroSiniestro = filtroAgrupador.getNumeroSiniestro().split("-");
				SimpleDateFormat sdfCorto 	= new SimpleDateFormat("yy");
				SimpleDateFormat sdf 		= new SimpleDateFormat("yyyy");
				claveOficinaSin      =  elementosNumeroSiniestro[0];
				consecutivoReporteSin= elementosNumeroSiniestro[1] ;
				Date fechaAnioReporte		= sdfCorto.parse( elementosNumeroSiniestro[2]);
				anioReporteSin		= sdf.format(fechaAnioReporte).toString() ;
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		parameters.put("claveOficinaSiniestro", claveOficinaSin);
		parameters.put("consecutivoReporteSin", consecutivoReporteSin);
		parameters.put("anioReporteSin", anioReporteSin);
		
		
		
		
		queryString = new StringBuilder();
		queryString.append("SELECT fact FROM "+ DocumentoFiscal.class.getSimpleName() + " fact WHERE fact.id in ( ");
		
		queryString.append("SELECT DISTINCT(factura.id) from " + DocumentoFiscal.class.getSimpleName() + " factura ");
		queryString.append(" JOIN factura.ordenesCompra ordenCompra ");
		queryString.append(" LEFT OUTER JOIN "+EstimacionCoberturaReporteCabina.class.getSimpleName()+" estimacion ");
		queryString.append(" LEFT OUTER JOIN estimacion.coberturaReporteCabina cobertura ");
		queryString.append(" LEFT OUTER JOIN cobertura.incisoReporteCabina inciso ");
		queryString.append(" LEFT OUTER JOIN inciso.seccionReporteCabina seccion ");
		queryString.append(" LEFT OUTER JOIN seccion.reporteCabina reporte ");
		queryString.append(" LEFT OUTER JOIN reporte.siniestroCabina siniestro ");
		
		queryString.append(" WHERE  factura.estatus  IN (:estatusCancelada,:estatusPagada,:estatusRegistrada ) ");
		queryString.append(" AND  ordenCompra.idTercero = estimacion.id ");
		queryString.append(" AND  factura.numeroFactura = :numeroDeFactura ");
		queryString.append(" AND  factura.tipo = :tipo ");
		queryString.append(" AND  factura.origen = :origen ");
		queryString.append(" AND  ( :nombreAgrupador IS NULL OR factura.nombreAgrupador LIKE :nombreAgrupador ) ");
		queryString.append(" AND  ( :tipoAgrupador IS NULL OR factura.tipoAgrupador = :tipoAgrupador ) ");
		queryString.append(" AND  ( :compania IS NULL OR factura.proveedor.id = :compania ) ");
		queryString.append(" AND  ( :totalDe  IS NULL OR factura.montoTotal >= :totalDe ) ");
		queryString.append(" AND  ( :totalHasta  IS NULL OR factura.montoTotal <= :totalHasta ) ");
		queryString.append(" AND  ( :fechaRegistroDe IS NULL OR FUNC('trunc', factura.fechaCreacion) >= :fechaRegistroDe ) " );
		queryString.append(" AND  ( :fechaRegistroHasta IS NULL OR FUNC('trunc', factura.fechaCreacion) <= :fechaRegistroHasta ) " );
		
		queryString.append(" AND  ( :claveOficinaSiniestro IS NULL OR siniestro.claveOficina = :claveOficinaSiniestro ) ");
		queryString.append(" AND  ( :consecutivoReporteSin IS NULL OR siniestro.consecutivoReporte = :consecutivoReporteSin ) ");
		queryString.append(" AND  ( :anioReporteSin IS NULL OR siniestro.anioReporte = :anioReporteSin ) ");
		
		queryString.append(" AND  ( :numeroSiniestroTercero IS NULL OR ordenCompra.cartaPago.siniestroTercero LIKE :numeroSiniestroTercero ) ");
		
		queryString.append(" )");
		List<DocumentoFiscal> facturas =  this.executeQueryMultipleResult(queryString.toString(), parameters);
		return facturas;
	}
	
	
	private void agregaCondicion(Object value,String condicion){
		
	}
	
	@Override
	public Boolean validaNombreDelAgrupador(String nombreAgrupador){

		StringBuilder queryString =null;
        Map<String, Object> parameters = new HashMap<String, Object>();  
        parameters.put("nombreAgrupador", nombreAgrupador.trim());
        parameters.put("estatus", DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue());
        
        queryString = new StringBuilder("SELECT agrupador from " + DocumentoFiscal.class.getSimpleName() + " agrupador ");
        queryString.append(" WHERE  TRIM(agrupador.nombreAgrupador) = :nombreAgrupador ");
        queryString.append(" AND    agrupador.estatus = :estatus ");
        
        List<DocumentoFiscal>  facturas = this.executeQueryMultipleResult(queryString.toString(), parameters);
        return (facturas!=null)?(facturas.size()>0)?Boolean.TRUE:Boolean.FALSE:Boolean.FALSE;
	}
	
	@Override
	public List<FacturaDTO> obtenerFacturasPendientesComplementoProveedor(Integer idProveedor, String origenEnvio) throws Exception{		
		List<FacturaDTO> facturas = new ArrayList<FacturaDTO>();
		StoredProcedureHelper storedHelper = null;
		String spName = "MIDAS.PKG_RECEPCION_CP.SPGETFACTURASPTESCOMPLPROV";		
		String propiedades = "numeroFactura,fechaFactura,importeSubTotal,importeIva,importeIvaRet,importeIsr,importeTotal,estatus,importeRestante,uuidCfdi,idProveedor";
		String columnasBaseDatos = "NUMERO_FACTURA,FECHA_FACTURA,SUBTOTAL,IVA,IVA_RETENIDO,ISR,MONTO_TOTAL,ESTATUS,MONTO_RESTANTE,UUID_CFDI,ID_PROVEEDOR";
		storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
		storedHelper.estableceMapeoResultados(FacturaDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);			
		storedHelper.estableceParametro("p_proveedorid",idProveedor);
		storedHelper.estableceParametro("p_origenenvio",origenEnvio);
    	facturas.addAll(storedHelper.obtieneListaResultados());
		return facturas;	
	}

	public List<FacturaDTO> obtenerFacturasPendientesComplementoAgente(Integer idAgente) throws Exception{		
		List<FacturaDTO> facturas = new ArrayList<FacturaDTO>();
		StoredProcedureHelper storedHelper = null;
		String spName = " MIDAS.PKG_RECEPCION_CP.SPGETFACTURASPTESCOMPLAGT";		
		String propiedades = "numeroFactura,fechaFactura, importeSubTotal, importeIva,importeIvaRet,importeIsr,importeTotal,estatus,importeRestante,uuidCfdi,idProveedor";
		String columnasBaseDatos = "NUMERO_FACTURA,FECHA_FACTURA,SUBTOTAL,IVA,IVA_RETENIDO,ISR,MONTO_TOTAL,ESTATUS,MONTO_RESTANTE,UUID_CFDI,ID_PROVEEDOR";
		storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
		storedHelper.estableceMapeoResultados(FacturaDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);			
		storedHelper.estableceParametro("p_idagente",idAgente);
		storedHelper.estableceParametro("p_origenenvio","COMP");
    	facturas.addAll(storedHelper.obtieneListaResultados());
		return facturas;	
	}
	
	@Override
	public Long obtenerIdLoteCarga() throws Exception{		
		List<EnvioFactura> facturas = new ArrayList<EnvioFactura>();
		StoredProcedureHelper storedHelper = null;
		String spName = "MIDAS.PKG_RECEPCION_CP.SPOBTENERNUMEROLOTE";		
		String propiedades = "idLote";
		String columnasBaseDatos = "NUMEROLOTE";
		storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
		storedHelper.estableceMapeoResultados(EnvioFactura.class.getCanonicalName(), propiedades, columnasBaseDatos);
    	facturas.addAll(storedHelper.obtieneListaResultados());
    	if (facturas!=null && !facturas.isEmpty()){
    		return facturas.get(0).getIdLote();
    	}
		return 0L;
	}
	
	@Override
	public List<FacturaPteComplMonitor> obtenerFacturasPendientesComplementoMonitor(Date fechaPagoInicio, Date fechaPagoFin, String origenEnvio) throws Exception{		
		List<FacturaPteComplMonitor> facturas = new ArrayList<FacturaPteComplMonitor>();
		StoredProcedureHelper storedHelper = null;
		String spName = "";		
		String propiedades = "noLiquidacion,tipoLiquidacion,noPrestadorServicio,prestadorServicio,factura,noSolCheque,noCheque,uuidFactura,fechaPago,uuidComplementoPago,importePagado";
		String columnasBaseDatos = "";
		if(origenEnvio.equals("ZIPFACT")){
			spName = "MIDAS.PKG_RECEPCION_CP.spGetFacturasPtesComplMonitor";		
			columnasBaseDatos = "NO_LIQUIDACION,TIPO_LIQUIDACION,ID_PROVEEDOR,NOMBRE_PROVEEDOR,FOLIO_FACTURA,NO_SOLICITUD_CHEQUE,NUMERO_CHEQUE,UUID_FACTURA,FECHA_PAGO,UUID_COMPLEMENTO,IMPORTE_PAGADO";
		}else if(origenEnvio.equals("COMP")){
			spName = "MIDAS.PKG_RECEPCION_CP.spGetFactPtesComplMonitorComp";		
			columnasBaseDatos = "NO_LIQUIDACION,TIPO_LIQUIDACION,CLAVE_AGENTE,NOMBRE_AGENTE,FOLIO_FACTURA,NO_SOLICITUD_CHEQUE,NUMERO_CHEQUE,UUID_FACTURA,FECHA_PAGO,UUID_COMPLEMENTO,IMPORTE_PAGADO";

		}
		
		storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
		storedHelper.estableceMapeoResultados(FacturaPteComplMonitor.class.getCanonicalName(), propiedades, columnasBaseDatos);			
		storedHelper.estableceParametro("pFechaPagoDe",fechaPagoInicio);
		storedHelper.estableceParametro("pFechaPagoHasta",fechaPagoFin);
		storedHelper.estableceParametro("pOrigenenvio",origenEnvio);		
    	facturas.addAll(storedHelper.obtieneListaResultados());
		return facturas;	
	}
	
	@Override
	public List<FacturaPteComplMonitor> obtenerFacturasPendientesComplementoNotificacion(Date fechaPagoInicio, Date fechaPagoFin, String origenEnvio) throws Exception{		
		List<FacturaPteComplMonitor> facturas = new ArrayList<FacturaPteComplMonitor>();
		StoredProcedureHelper storedHelper = null;
		String spName = "";		
		String propiedades = "noLiquidacion,tipoLiquidacion,noPrestadorServicio,prestadorServicio,factura,noSolCheque,noCheque,uuidFactura,fechaPago,importeRestante";
		String columnasBaseDatos = "";
		if(origenEnvio.equals("ZIPFACT")){
			spName = "MIDAS.PKG_RECEPCION_CP.spGetFacturasPtesComplNotif";		
			columnasBaseDatos = "NO_LIQUIDACION,TIPO_LIQUIDACION,ID_PROVEEDOR,NOMBRE_PROVEEDOR,FOLIO_FACTURA,NO_SOLICITUD_CHEQUE,NUMERO_CHEQUE,UUID_FACTURA,FECHA_PAGO,MONTO_RESTANTE";
		}else if(origenEnvio.equals("COMP")){
			spName = "MIDAS.PKG_RECEPCION_CP.spGetFactPtesComplNotifComp";		
			columnasBaseDatos = "NO_LIQUIDACION,TIPO_LIQUIDACION,CLAVE_AGENTE,NOMBRE_AGENTE,FOLIO_FACTURA,NO_SOLICITUD_CHEQUE,NUMERO_CHEQUE,UUID_FACTURA,FECHA_PAGO,MONTO_RESTANTE";
		}		
		storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
		storedHelper.estableceMapeoResultados(FacturaPteComplMonitor.class.getCanonicalName(), propiedades, columnasBaseDatos);			
		storedHelper.estableceParametro("pFechaPagoDe",fechaPagoInicio);
		storedHelper.estableceParametro("pFechaPagoHasta",fechaPagoFin);
		storedHelper.estableceParametro("pOrigenenvio",origenEnvio);		
    	facturas.addAll(storedHelper.obtieneListaResultados());
		return facturas;	
	}
}
