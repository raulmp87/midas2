package mx.com.afirme.midas2.dao.impl.cargos;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.cargos.ConfigCargosDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.dto.Cargos.DetalleCargosView;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.prestamos.ConfigPrestamoAnticipoView;
import mx.com.afirme.midas2.service.cargos.ConfigCargosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

@Stateless
public class ConfigCargosDaoImpl extends EntidadDaoImpl implements ConfigCargosDao {

	private EntidadService entidadService;
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	private AgenteMidasService agenteMidasService;
	private FortimaxService fortimaxService;
	private List<DocumentoEntidadFortimax> listaDocFortimaxGuardados = new ArrayList<DocumentoEntidadFortimax>();
	
	@Override
	public ConfigCargos aplicar(ConfigCargos arg0) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public ConfigCargos loadById(ConfigCargos configCargos) throws Exception {
		ConfigCargos objConfigCargos = new ConfigCargos();
		List<ConfigCargos> listConfigCargos = new ArrayList<ConfigCargos>();
		if(configCargos!=null && configCargos.getId()!=null){
			String queryString = "select model from ConfigCargos model left join fetch model.agente left join fetch model.tipoCargo left join fetch model.estatusCargo left join fetch model.plazo where model.id="+configCargos.getId();
			Query query = entityManager.createQuery(queryString);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			listConfigCargos=query.getResultList();
			objConfigCargos = listConfigCargos.get(0);
		}
		return objConfigCargos;
	}

	@Override
	public ConfigCargos save(ConfigCargos obj) throws Exception {
		ConfigCargos objConfigCargos = new ConfigCargos();
		ValorCatalogoAgentes estatus =entidadService.findById(ValorCatalogoAgentes.class, obj.getEstatusCargo().getId());
		ValorCatalogoAgentes plazo = entidadService.findById(ValorCatalogoAgentes.class, obj.getPlazo().getId());
		obj.setPlazo(plazo);
		obj.setEstatusCargo(estatus);
		objConfigCargos = entidadService.save(obj);
		return objConfigCargos;
	}

	@Override
	public ConfigCargos updateEstatus(ConfigCargos configCargos, String elementoCatalogo)throws Exception {
		ValorCatalogoAgentes estatusCargo = new ValorCatalogoAgentes();
		ConfigCargos objConfigCargos = new ConfigCargos();
		try {
			estatusCargo = valorCatalogoAgentesService.obtenerElementoEspecifico("Estatus Cargo de Agentes", elementoCatalogo);
			objConfigCargos=entidadService.findById(ConfigCargos.class, configCargos.getId());
			objConfigCargos.setEstatusCargo(estatusCargo);
			objConfigCargos = entidadService.save(objConfigCargos);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objConfigCargos;
	}
	
	@Override
	public List<DetalleCargosView> findByFilters(ConfigCargos filtro)throws Exception {
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<DetalleCargosView> lista=new ArrayList<DetalleCargosView>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT  distinct");
		queryString.append(" configCargos.ID as id,");
		queryString.append(" agente.IDAGENTE as idAgente,");
		queryString.append(" persona.NOMBRECOMPLETO as nombreCompleto,");
		queryString.append(" tipoCargo.VALOR as tipoCargo,");
		queryString.append(" configCargos.FECHAALTACARGO as fechaAltaCargo,");
		queryString.append(" configCargos.IMPORTE as importe,");
		queryString.append(" configCargos.NUMEROCARGOS as numCargos,");
		queryString.append(" estatus.VALOR as estatus");
		
		queryString.append(" FROM MIDAS.TOCONFIGCARGOS configCargos");
		queryString.append(" INNER JOIN MIDAS.TOAGENTE agente ON  (agente.ID=configCargos.IDAGENTE)");
		queryString.append(" INNER JOIN  MIDAS.VW_PERSONA persona ON (persona.IDPERSONA=agente.IDPERSONA)");
		queryString.append(" INNER JOIN  MIDAS.TOVALORCATALOGOAGENTES estatus ON (estatus.ID = configCargos.IDESTATUSCARGO)");
		queryString.append(" INNER JOIN  MIDAS.TOVALORCATALOGOAGENTES tipoCargo ON (tipoCargo.ID = configCargos.IDTIPOCARGO)");
		queryString.append(" INNER JOIN  MIDAS.TOVALORCATALOGOAGENTES ClasificacionAgente ON (ClasificacionAgente.ID = agente.CLASIFICACIONAGENTES)");
		queryString.append(" INNER JOIN  MIDAS.TOPROMOTORIA promotoria ON (agente.IDPROMOTORIA = promotoria.ID)");
		queryString.append(" INNER JOIN  MIDAS.TOEJECUTIVO ejecutivo ON (ejecutivo.ID = promotoria.EJECUTIVO_ID)");
		queryString.append(" INNER JOIN  MIDAS.TOGERENCIA gerencia  ON ( gerencia.ID = ejecutivo.GERENCIA_ID)");
		queryString.append(" INNER JOIN  MIDAS.TOCENTROOPERACION centroOper ON (centroOper.ID= gerencia.CENTROOPERACION_ID)");
		queryString.append(" where ");
		if(filtro!=null){
			int index=1;
			if(filtro.getId()!=null){
				addCondition(queryString, " (configCargos.id=?) ");
				params.put(index, filtro.getId());
				index++;
			}
			if(filtro.getFechaInicioCoBro()!=null){
				addCondition(queryString, " (configCargos.fechaInicioCoBro>=?) ");
				params.put(index, filtro.getFechaInicioCoBro());
				index++;
			}
			if(filtro.getFechaFinCobro()!=null){
				addCondition(queryString, " (configCargos.fechaInicioCoBro<=?) ");
				params.put(index, filtro.getFechaFinCobro());
				index++;
			}
			if(filtro.getAgente()!=null){
				if(filtro.getAgente().getId()!=null){
					addCondition(queryString, " (agente.idAgente=?) ");
					params.put(index,  filtro.getAgente().getId());
					index++;
				}
				if(filtro.getAgente().getPersona()!=null){
					if(!"".equals(filtro.getAgente().getPersona().getNombreCompleto())){
						addCondition(queryString, " (persona.nombreCompleto like ?) ");
						params.put(index,  "%"+filtro.getAgente().getPersona().getNombreCompleto()+"%");
						index++;
					}
				}
			}
			if(filtro.getFechaAltaCargo()!=null){
				addCondition(queryString, " (configCargos.fechaAltaCargo=?) ");
				params.put(index,  filtro.getFechaAltaCargo());
				index++;
			}
			if(filtro.getTipoCargo()!=null){
				if(filtro.getTipoCargo().getId()!=null){
					addCondition(queryString, " (tipoCargo.id=?) ");
					params.put(index,  filtro.getTipoCargo().getId());
					index++;
				}
			}
			if(filtro.getEstatusCargo()!=null){
				if(filtro.getEstatusCargo().getId()!=null){
					addCondition(queryString, " (configCargos.estatusCargo.id=?) ");
					params.put(index,  filtro.getEstatusCargo().getId());
					index++;
				}
			}
			if(filtro.getCentroOperacion()!=null){
				if(filtro.getCentroOperacion().getId()!=null){
					addCondition(queryString, " (centroOper.id=?) ");
					params.put(index,  filtro.getCentroOperacion().getId());
					index++;
				}
			}
			if(filtro.getGerencia()!=null){
				if(filtro.getGerencia().getId()!=null){
					addCondition(queryString, " (gerencia.idgerencia=?) ");
					params.put(index,  filtro.getGerencia().getId());
					index++;
				}
			}
			if(filtro.getPromotoria()!=null){
				if(filtro.getPromotoria().getId()!=null){
					addCondition(queryString, " (promotoria.idpromotoria=?) ");
					params.put(index,  filtro.getPromotoria().getId());
					index++;
				}
			}
			if(filtro.getEjecutivo()!=null){
				if(filtro.getEjecutivo().getId()!=null){
					addCondition(queryString, " (ejecutivo.idejecutivo=?) ");
					params.put(index,  filtro.getEjecutivo().getId());
					index++;
				}
			}
			if(filtro.getTipoAgente()!=null){//filtro clasificacion de agente
				if(filtro.getTipoAgente().getId()!=null){
					addCondition(queryString, " (ClasificacionAgente.ID=?) ");
					params.put(index,  filtro.getTipoAgente().getId());
					index++;
				}
			}
		}
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		Query query=entityManager.createNativeQuery(getQueryString(queryString),DetalleCargosView.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
	
		lista=query.getResultList();
		return lista;
	}
	
	@Override
	public void auditarDocumentosEntregadosCargos(Long idCargo,Long idAgente,String nombreAplicacion) throws Exception {
		if(isNull(idAgente)){
			onError("Favor de proporcionar la clave del registro");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}		
				
		String[] documentosFortimax;
		try {
			/*lista de documentos que estan dados de alta en el portal de fortimax*/
			documentosFortimax = fortimaxService.getDocumentFortimax(idAgente,nombreAplicacion);
			if(isNull(documentosFortimax)|| documentosFortimax.length==0){
				onError("No hay documentos para la aplicacion "+nombreAplicacion+" con el numero de expediente "+idAgente);
			}					
			/*lista de documentos guardados en base de datos midas*/
			List<EntregoDocumentosView> lista=consultaEstatusDocumentos(idCargo,idAgente);			
			if(isEmptyList(lista)){
				onError("No hay registros de documentos para la aplicacion "+nombreAplicacion+" con el expediente "+idAgente);
			}
			
			for(EntregoDocumentosView doc:lista){
				String nombreDocumento=doc.getValor();
				Long idDocumento = doc.getId();
				Integer checado = doc.getChecado();
				if(isNotNull(doc) && isValid(nombreDocumento)){
					for(String docFortimax:documentosFortimax){
						//Si coincide el documento de base de datos con lo de fortimax y no ha sido subido, entonces lo marca como subido
						//if(isValid(docFortimax) && !docFortimax.equalsIgnoreCase(nombreDocumento)){
						DocumentosAgrupados rowDocumentos = entityManager.find(DocumentosAgrupados.class, idDocumento);	
						if(nombreDocumento.equalsIgnoreCase(docFortimax)){
							//cif(!docFortimax.equalsIgnoreCase(nombreDocumento)){															
								rowDocumentos.setExisteDocumento(1);
								entidadService.save(rowDocumentos);
								break;
							//}
						}
						
					}						
				}
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	@Override
	public List<EntregoDocumentosView> consultaEstatusDocumentos(Long idCargo,Long idAgente) throws Exception {
		List<EntregoDocumentosView> listaDocumentosAgrupadosView = new ArrayList<EntregoDocumentosView>();
		StringBuilder queryString = new StringBuilder("");							
		queryString.append("select id,nombre as valor,existeDocumento as checado from MIDAS.trDocumentosAgrupados where idEntidad="+idAgente+" and upper(nombre) like upper('%CARGO"+idCargo+"_AGT"+idAgente+"%') order by id asc "); 
		Query query=entityManager.createNativeQuery(queryString.toString(),EntregoDocumentosView.class);
		listaDocumentosAgrupadosView=query.getResultList();
		return listaDocumentosAgrupadosView;
	}

	@Override
	public void crearYGenerarDocumentosFortimax(ConfigCargos config) throws Exception {
		if(config==null){
			if(config.getAgente()==null && config.getAgente().getId()==null){
				throw new Error("No se indico el id del agente");
			}
		}
		
		/***************************************************************************
		 * se crea la carpeta del agente en caso de no existir
		 * **************************************************************************/
		 String []respExp=agenteMidasService.generateExpedientAgent(config.getAgente().getId());
		
		 /*lista de documentos que estan dados de alta en el portal de fortimax*/
		String[] documentosFortimax = fortimaxService.getDocumentFortimax(config.getAgente().getId(),"AGENTES");
		
		
		/***********************************************************************************************
		 * Se revisa que ya existan los documentos creados en fortimax
		 ****************************************************************************************************/
		boolean existeDocCargoFortimax=false;		
		for(String docs:documentosFortimax){
			if(docs!=null){
				if(docs.equals(config.getTipoCargo().getValor()+"_CARGO"+config.getId()+"_AGT"+config.getAgente().getId())){
					existeDocCargoFortimax = true;					
				}					
			}
		}		
		/***********************************************************************************************
		 * Se crean los documentos que falten en fortimax
		 ****************************************************************************************************/
		if(existeDocCargoFortimax==false){	
			String []respDocSolMov=fortimaxService.generateDocument(config.getAgente().getId(), "AGENTES", config.getTipoCargo().getValor()+"_CARGO"+config.getId()+"_AGT"+config.getAgente().getId(), "04 CARGOS");
		}				
		
		/***********************************************************************************************
		 * Se obtiene la lista de documentos guardados en base de datos
		 ****************************************************************************************************/
		 List<DocumentosAgrupados> listaDocFortimaxGuardados=documentosFortimaxGuardadosDB(config.getId(),config.getAgente().getId());		
		
		/***********************************************************************************************
		 * Se revisa que los documentos de la base de datos esten completos
		 ****************************************************************************************************/
		boolean existeDocCargoDB=false;
		for(DocumentosAgrupados docDB:listaDocFortimaxGuardados){
			if(docDB!=null){
				if(docDB.getNombre().equals(config.getTipoCargo().getValor()+"_CARGO"+config.getId()+"_AGT"+config.getAgente().getId())){
					existeDocCargoDB= true;
				}				
			}
		}		
				
		/***********************************************************************************************
		 * Se crea el registro en la base de datos de los documentos que falten para que sean los mismos 
		 * que estan creados en fortimax
		 ****************************************************************************************************/
		if(existeDocCargoDB==false){
				DocumentosAgrupados guardaDocSolicitudMov = new DocumentosAgrupados();
				Long idDocumentoSM = (long) 40;
				guardaDocSolicitudMov.setIdAgrupador(null);
				guardaDocSolicitudMov.setIdDocumento(idDocumentoSM);
				guardaDocSolicitudMov.setIdEntidad(config.getAgente().getId());
				guardaDocSolicitudMov.setNombre(config.getTipoCargo().getValor()+"_CARGO"+config.getId()+"_AGT"+config.getAgente().getId());
				guardaDocSolicitudMov.setExisteDocumento(0);
				entidadService.save(guardaDocSolicitudMov);
		}		
				
	}
	
	private List<DocumentosAgrupados> documentosFortimaxGuardadosDB(Long idCargo,Long idAgente){
		List<DocumentosAgrupados> listaDocumentosAgrupados = new ArrayList<DocumentosAgrupados>();
		StringBuilder queryString = new StringBuilder("");							
		queryString.append("select id,idDocumento,idEntidad,idAgrupador,nombre,existeDocumento from MIDAS.trDocumentosAgrupados where idEntidad="+idAgente+" and upper(nombre) like upper('%CARGO"+idCargo+"_AGT"+idAgente+"%') order by id asc"); //and idDocumento in(40,41,42)
		Query query=entityManager.createNativeQuery(queryString.toString(),DocumentosAgrupados.class);
		listaDocumentosAgrupados=query.getResultList();
		return listaDocumentosAgrupados;
	}
	
	//***************************************************************************************************
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	@EJB
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}

	public List<DocumentoEntidadFortimax> getListaDocFortimaxGuardados() {
		return listaDocFortimaxGuardados;
	}

	public void setListaDocFortimaxGuardados(
			List<DocumentoEntidadFortimax> listaDocFortimaxGuardados) {
		this.listaDocFortimaxGuardados = listaDocFortimaxGuardados;
	}

}
