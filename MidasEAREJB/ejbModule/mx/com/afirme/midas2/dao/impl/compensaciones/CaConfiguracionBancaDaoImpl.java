/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaConfiguracionBancaDao;
import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionBanca;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Stateless
public class CaConfiguracionBancaDaoImpl implements CaConfiguracionBancaDao {
	
	public static final String ID = "id";
	public static final String NOMBRE = "nombre";
	public static final String USUARIO = "usuario";
	public static final String FECHAMODIFICACION = "fechamodificacion";
	public static final String BORRADOLOGICO = "borradologico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = LoggerFactory.getLogger(CaConfiguracionBancaDaoImpl.class);
	
	public void save(CaConfiguracionBanca entity) {	
		try {
			LOG.info(">> save()");
			entityManager.persist(entity);
			LOG.info("<< save()");
        } catch (RuntimeException re) {     
        	LOG.error("Información del Error", re);
            throw re;
        }
	}
	
    public void delete(CaConfiguracionBanca entity) {
    	try {
    		LOG.info(">> delete()");
    		entity = entityManager.getReference(CaConfiguracionBanca.class, entity.getId());
    		LOG.info("<< delete()");
		    entityManager.remove(entity);
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
		    throw re;
		}
    }
    
    public CaConfiguracionBanca update(CaConfiguracionBanca entity) {    
    	try {
    		LOG.info(">> update()");
            CaConfiguracionBanca result = entityManager.merge(entity);
            LOG.info("<< update()");
            return result;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        	throw re;
        }
    }
    
    public CaConfiguracionBanca findById( Long id) {
    	CaConfiguracionBanca instance = null;
    	try {
    		LOG.info(">> findById()");
    		instance = entityManager.find(CaConfiguracionBanca.class, id);
    		LOG.info("<< findById()");
    	} catch (RuntimeException re) {    		
    		LOG.error("Información del Error", re);
    	}
    	return instance;
    }

    @SuppressWarnings("unchecked")
    public List<CaConfiguracionBanca> findByProperty(String propertyName, final Object value) {
    	LOG.info(">> findByProperty()");
		try {
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaConfiguracionBanca model ");
			queryString.append("  WHERE model.").append(propertyName).append(" = :propertyValue");
			Query query = entityManager.createQuery(queryString.toString(), CaConfiguracionBanca.class);
			query.setParameter("propertyValue", value);		
			LOG.info("<< findByProperty()");
			return query.getResultList();
		} catch (RuntimeException re) {		
			LOG.error("Información del Error", re);
			return null;
		}
	}			

    @SuppressWarnings("unchecked")
	public List<CaConfiguracionBanca> findAll() {	
		LOG.info(">> findAll()");
		List<CaConfiguracionBanca> list = null;
		try {
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaConfiguracionBanca model ORDER BY model.id");
			Query query = entityManager.createQuery(queryString.toString());
			list = query.getResultList();			
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			list = new ArrayList<CaConfiguracionBanca>();
		}
		LOG.info("<< findAll()");
		return list;
	}
}