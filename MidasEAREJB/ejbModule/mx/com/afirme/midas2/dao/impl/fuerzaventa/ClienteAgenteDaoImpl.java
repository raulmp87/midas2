package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.ClienteAgenteDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CedulaSubramo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteJPA;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HerenciaClientes;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteCarteraClientesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ClienteAgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.HerenciaClientesView;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.CedulaSubramoService;
import mx.com.afirme.midas2.service.fuerzaventa.ClienteJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.poliza.PolizaService;

import org.apache.commons.beanutils.BeanUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
@Stateless
public class ClienteAgenteDaoImpl extends EntidadHistoricoDaoImpl implements ClienteAgenteDao{
	private ClienteJPAService clienteJpaService;
	private ValorCatalogoAgentesService catalogoService;
	private CedulaSubramoService cedulaSubramoService;
	private EntidadService entidadService;
	private PolizaService polizaService;
	private EndosoService endosoService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	
	@EJB
	public void setCedulaSubramoService(CedulaSubramoService cedulaSubramoService) {
		this.cedulaSubramoService = cedulaSubramoService;
	}

	@EJB
	public void setClienteJpaService(ClienteJPAService clienteJpaService) {
		this.clienteJpaService = clienteJpaService;
	}
	
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	@EJB
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

	@EJB
	public void setPolizaService(PolizaService polizaService) {
		this.polizaService = polizaService;
	}

	@Override
	public List<ClienteAgente> findByFilters(ClienteAgente filtroClienteAgente) {
		/*List<ClienteAgente> lista=new ArrayList<ClienteAgente>();
		
		final StringBuilder queryString=new StringBuilder("select model from ClienteAgente model left join fetch model.agente left join fetch model.cliente left join fetch model.herenciaClientes ");
		Map<String,Object> params=new HashMap<String, Object>();
		
		if(filtroClienteAgente!=null ){
			if( filtroClienteAgente.getAgente()!=null && filtroClienteAgente.getAgente().getId()!=null){
				addCondition(queryString, "model.agente.id=:idAgente");
				params.put("idAgente", filtroClienteAgente.getAgente().getId());
			}
			if( filtroClienteAgente.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtroClienteAgente.getId());
			}
			if( filtroClienteAgente.getClaveEstatus()!=null){
				addCondition(queryString, "model.claveEstatus=:claveEstatus");
				params.put("claveEstatus", filtroClienteAgente.getClaveEstatus());
			}
			if( filtroClienteAgente.getCliente()!=null){
				if( filtroClienteAgente.getCliente().getIdCliente()!=null && !filtroClienteAgente.getCliente().getIdCliente().equals("")){
					addCondition(queryString, "model.cliente.idCliente=:idCliente");
					params.put("idCliente", filtroClienteAgente.getCliente().getIdCliente());
				}
				if( filtroClienteAgente.getCliente().getApellidoMaterno()!=null && !filtroClienteAgente.getCliente().getApellidoMaterno().equals("")){
					addCondition(queryString, "model.cliente.apellidoMaterno like :apellidoMaterno");
					params.put("apellidoMaterno", filtroClienteAgente.getCliente().getApellidoMaterno()+"%");
				}
				if( filtroClienteAgente.getCliente().getApellidoPaterno()!=null && !filtroClienteAgente.getCliente().getApellidoPaterno().equals("")){
					addCondition(queryString, "model.cliente.apellidoPaterno like :apellidoPaterno");
					params.put("apellidoPaterno", filtroClienteAgente.getCliente().getApellidoPaterno()+"%");
				}
				if( filtroClienteAgente.getCliente().getNombre()!=null && !filtroClienteAgente.getCliente().getNombre().equals("")){
					addCondition(queryString, "model.cliente.nombre like :nombre");
					params.put("nombre", filtroClienteAgente.getCliente().getNombre()+"%");
				}
			}
		}
		try{
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista = query.getResultList();
		}catch(Exception e){
				e.printStackTrace();
		}
		return lista;*/
		
		return this.findByFilters(filtroClienteAgente, null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ClienteAgente> findByFilters(ClienteAgente filtroClienteAgente, String fechaHistorico) {
		
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;
		
		List<ClienteAgente> lista=new ArrayList<ClienteAgente>();		
		
		final StringBuilder queryString=new StringBuilder("select model from ClienteAgente model left join fetch model.agente left join fetch model.cliente left join fetch model.herenciaClientes ");
		Map<String,Object> params=new HashMap<String, Object>();
		
		if(filtroClienteAgente!=null ){
			if( filtroClienteAgente.getAgente()!=null && filtroClienteAgente.getAgente().getId()!=null){
				addCondition(queryString, "model.agente.id=:idAgente");
				params.put("idAgente", filtroClienteAgente.getAgente().getId());
			}
			if( filtroClienteAgente.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtroClienteAgente.getId());
			}
			if( filtroClienteAgente.getClaveEstatus()!=null){
				addCondition(queryString, "model.claveEstatus=:claveEstatus");
				params.put("claveEstatus", filtroClienteAgente.getClaveEstatus());
			}
			if( filtroClienteAgente.getCliente()!=null){
				if( filtroClienteAgente.getCliente().getIdCliente()!=null && !filtroClienteAgente.getCliente().getIdCliente().equals("")){
					addCondition(queryString, "model.cliente.idCliente=:idCliente");
					params.put("idCliente", filtroClienteAgente.getCliente().getIdCliente());
				}
				if( filtroClienteAgente.getCliente().getApellidoMaterno()!=null && !filtroClienteAgente.getCliente().getApellidoMaterno().equals("")){
					addCondition(queryString, "model.cliente.apellidoMaterno like :apellidoMaterno");
					params.put("apellidoMaterno", filtroClienteAgente.getCliente().getApellidoMaterno()+"%");
				}
				if( filtroClienteAgente.getCliente().getApellidoPaterno()!=null && !filtroClienteAgente.getCliente().getApellidoPaterno().equals("")){
					addCondition(queryString, "model.cliente.apellidoPaterno like :apellidoPaterno");
					params.put("apellidoPaterno", filtroClienteAgente.getCliente().getApellidoPaterno()+"%");
				}
				if( filtroClienteAgente.getCliente().getNombre()!=null && !filtroClienteAgente.getCliente().getNombre().equals("")){
					addCondition(queryString, "model.cliente.nombre like :nombre");
					params.put("nombre", filtroClienteAgente.getCliente().getNombre()+"%");
				}
			}
		}
		
		if(!isHistorico)
		{
			try{
				Query query = entityManager.createQuery(getQueryString(queryString),ClienteAgente.class);
				if(!params.isEmpty()){
					setQueryParametersByProperties(query, params);
				}
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				lista = query.getResultList();
				for(ClienteAgente obj:lista){
					entidadService.refresh(obj.getCliente());
				}
			}catch(Exception e){
					e.printStackTrace();
			}			
			
		}else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
			
			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString).toString());
			historicalQuery.addArgument("agente.id");
			historicalQuery.addArgumentValue(filtroClienteAgente.getAgente().getId());
			historicalQuery.refreshIdentityMapResult();
			lista = (List<ClienteAgente>)historicalSession.executeQuery(historicalQuery);		
		}	
		
		return lista;
	}

	@Override
	public ClienteAgente loadById(ClienteAgente clienteAgente) {
		if(clienteAgente != null && clienteAgente.getId() != null){
			List<ClienteAgente> list=findByFilters(clienteAgente);
			if(list!=null && !list.isEmpty()){
				clienteAgente=list.get(0);
			}
		}
		return clienteAgente;
	}
	
	@Override
	public ClienteAgente loadByIdCheckAgent(ClienteAgente clienteAgente) {
		ClienteAgente entidad = new ClienteAgente();
		List<ClienteAgente> list = new ArrayList<ClienteAgente>();
		
		if(clienteAgente!=null && clienteAgente.getCliente()!=null && clienteAgente.getCliente().getIdCliente()!=null && clienteAgente.getAgente()!=null && clienteAgente.getAgente().getId()!=null){
			entidad.setCliente(clienteAgente.getCliente());
			entidad.setAgente(clienteAgente.getAgente());
			list=findByFilters(entidad);
			if(list!=null && !list.isEmpty()){
				entidad=list.get(0);
			}else{ // si no encontro la asociacion con el agente, aun asi traera el cliente con el id cliente otorgado pero el agente estara en null.
				entidad.setAgente(null);
				ClienteJPA clienteJpa = new ClienteJPA(); 
				clienteJpa.setIdCliente(clienteAgente.getCliente().getIdCliente());
				clienteJpa = clienteJpaService.loadById(clienteJpa);
				entidad.setCliente(clienteJpa);
			}
		}
		clienteAgente = entidad;
		return clienteAgente;
	}

	@Override
	public ClienteAgente saveFull(ClienteAgente arg0) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void unsubscribe(ClienteAgente clienteAgente) throws Exception {
		if(clienteAgente==null || clienteAgente.getId()==null){
			throw new Exception("clienteAgente is null");
		}
		clienteAgente.setClaveEstatus((int)0l);
		update(clienteAgente);//Se actualiza su estatus
	}
	
	private void onError(String msg)throws Exception{
		throw new Exception(msg);
	}

	@Override
	public void cancelarHerenciaCliente(ClienteAgente clienteAgente, Agente agenteAnterior) throws Exception{
		if(clienteAgente==null || clienteAgente.getId()==null){
			onError("Favor de elegir el cliente a asignar");
		}
		clienteAgente=findById(ClienteAgente.class,clienteAgente.getId());
		if(clienteAgente==null || clienteAgente.getId()==null){
			onError("No existe la relacion de cliente-agente ");
		}
		if(agenteAnterior==null || agenteAnterior.getId()==null){
			onError("Favor de elegir el agente al quel se le asignara el cliente.");
		}
		ClienteAgente nuevo=new ClienteAgente();
		nuevo.setAgente(agenteAnterior);
		nuevo.setCliente(clienteAgente.getCliente());
		//la relacion actual se pone como inactiva
		heredarClientes(clienteAgente, clienteAgente.getAgente(), "INACTIVO");
		//se agrega otra relacion de cliente-agente con el nuevo agente que en este caso es el agente antes de la cesion del cliente
		//y se pone con estatus de activo.
		heredarClientes(nuevo, agenteAnterior, "ACTIVO");
	}
	/**
	 * Se cancela la herencia de clientes.
	 */
	@Override
	public void cancelarHerenciaClientes(List<ClienteAgente> clientes, Agente agenteAnterior,HerenciaClientes herencia) throws Exception{
		if(clientes==null || clientes.isEmpty()){
			onError("Favor de elegir clientes a heredar");
		}
		if(agenteAnterior==null || agenteAnterior.getId()==null){
			onError("Favor de elegir el agente a quien se heredara la lista de clientes");
		}
		if(herencia==null || herencia.getId()==null){
			onError("Favor de inidcar el identificador de herencia");
		}
		for(ClienteAgente cliente:clientes){
			cliente.setHerenciaClientes(herencia);
			heredarClientes(cliente, agenteAnterior,"INACTIVO");
		}
	}
	/**
	 * Se hereda la lista de clientes a un agente indicando la accion, puede ser ACTIVO|INACTIVO, que realmente es el estatus para
	 * habilitar o deshabilitar una relacion
	 * @param clientes
	 * @param agenteDestino
	 * @param accion
	 * @throws Exception
	 */
	@Override
	public void heredarClientes(List<ClienteAgente> clientes, Agente agenteDestino,HerenciaClientes herencia,String accion) throws Exception {
		if(clientes==null || clientes.isEmpty()){
			onError("Favor de elegir clientes a heredar");
		}
		if(agenteDestino==null || agenteDestino.getId()==null){
			onError("Favor de elegir el agente a quien se heredara la lista de clientes");
		}
		if(accion==null || accion.isEmpty()){
			accion="ACTIVO";
		}
		if(herencia==null || herencia.getId()==null){
			onError("No se recibio el identificador de herencia");
		}
		
		
		for(ClienteAgente cliente:clientes){			
			cliente.setHerenciaClientes(herencia);			
			String estatusInverso=("ACTIVO".equals(accion))?"INACTIVO":"ACTIVO";
			//primero se inactiva la relacion del cliente con el agente actual
			heredarClientes(cliente,cliente.getAgente(),estatusInverso);			
			//se inserta una nueva relacion pero con el nuevo agente de destino
			//nuevaRelacion.setId(cliente.getId());
			heredarClientes(cliente,agenteDestino,accion);
		}
	}
	/**
	 * Se guarda la relacion de clienteAgente con el agente de destino
	 */
	@Override
	
	public void heredarClientes(ClienteAgente clienteAgente,Agente agenteDestino,String accion) throws Exception {
		if(clienteAgente==null){
            onError("Favor de proporcionar el cliente a heredar");
		}
		HerenciaClientes cesion=clienteAgente.getHerenciaClientes();
		clienteAgente=loadById(clienteAgente);
		if(clienteAgente==null || clienteAgente.getCliente()==null || clienteAgente.getCliente().getIdCliente()==null){
			onError("Favor de proporcionar el cliente a heredar");
		}
		if(agenteDestino==null || agenteDestino.getId()==null){
			onError("Favor de proporcionar el agente a asignarle el cliente");
		}
		Agente agente=getReference(Agente.class,agenteDestino.getId());
		if(agente==null || agente.getId()==null){
			onError("El agente con la clave "+agenteDestino.getId()+" no existe");
		}
		ClienteJPA cliente=clienteJpaService.loadById(clienteAgente.getCliente());
		if(cliente==null){
			onError("El cliente no existe con la clave "+clienteAgente.getCliente().getIdCliente());
		}
		Long idEstatus= catalogoService.obtenerIdElementEspecifico("Tipos de Estatus",accion);
		if(idEstatus==null){
			onError("No existe el estatus de ACTIVO para el catalogo de Estauts de Agente (Situacion)");
		}
		//Si es accion para dar de alta una nueva herencia se limpia el objeto para persistirlo
		if("ACTIVO".equals(accion)){
			clienteAgente=new ClienteAgente();
		}
		clienteAgente.setAgente(agente);//Setting persistent agente
		clienteAgente.setCliente(cliente);//Setting persisent client
		clienteAgente.setClaveEstatus(idEstatus.intValue());
		clienteAgente.setHerenciaClientes(cesion);
		if(clienteAgente.getId()!=null){
			update(clienteAgente);
		}else{
			persist(clienteAgente);
		}
	}
	/**
	 * Asigna un cliente a un agente.
	 * @param clienteHeredar
	 * @param agenteDestino
	 * @param accion
	 * @throws Exception
	 */
	@Override
	public void asignarCliente(ClienteAgente clienteHeredar,Agente agenteDestino,String accion) throws Exception{
		if(agenteDestino==null || agenteDestino.getId()==null){
			onError("Favor de proporcionar el agente a asignarle el cliente");
		}
		Agente agente=getReference(Agente.class,agenteDestino.getId());
		if(agente==null || agente.getId()==null){
			onError("El agente con la clave "+agenteDestino.getId()+" no existe");
		}
		ClienteJPA cliente=clienteJpaService.loadById(clienteHeredar.getCliente().getIdCliente());
		if(cliente==null){
			onError("El cliente no existe con la clave "+clienteHeredar.getCliente().getIdCliente());
		}
		Long idEstatus= catalogoService.obtenerIdElementEspecifico("Tipos de Estatus",accion);
		if(idEstatus==null){
			onError("No existe el estatus de ACTIVO para el catalogo de Estauts de Agente (Situacion)");
		}
		//Si es accion para dar de alta una nueva herencia se limpia el objeto para persistirlo
		if("ACTIVO".equals(accion)){
			clienteHeredar=new ClienteAgente();
		}
		clienteHeredar.setAgente(agente);//Setting persistent agente
		clienteHeredar.setCliente(cliente);//Setting persisent client
		clienteHeredar.setClaveEstatus(idEstatus.intValue());
		clienteHeredar.setHerenciaClientes(null);
		if(clienteHeredar.getId()!=null){
			update(clienteHeredar);
		}else{
			persist(clienteHeredar);
		}
	}
	
	@Override
	public void desAsignarCliente(ClienteAgente clienteAgente) throws RuntimeException {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("agente.id",clienteAgente.getAgente().getId());
		params.put("cliente.idCliente",clienteAgente.getCliente().getIdCliente());
		List<ClienteAgente> clienteAgentes = entidadService.findByProperties(ClienteAgente.class, params);
		if (clienteAgente != null && !clienteAgentes.isEmpty()) {
			clienteAgente = clienteAgentes.get(0);
			entityManager.remove(clienteAgente);
		}
	}
	/**
	 * Enlista los clientes que son aptos para poder heredarse de un agente origen a un agente destino.En base a ciertas reglas de negocio:
	 * 1.- Que el agente origen y destino tengan el mismo tipo de cedula
	 * 2.- Que el agente origen y destino tengan los mismos subramos,o que al menos el agente destino contenga todos los subramos del agente origen
	 * 3.- Si no es ningun caso anterior, se verifica que cada poliza de cada cliente del agente origen contenga los subramos que el agente destino son 
	 * iguales en el agente de origen.
	 */
	@Override
	public List<ClienteAgente> listarClientesAHeredar(Agente agenteOrigen, Agente agenteDestino) throws Exception {
		List<ClienteAgente> list=new ArrayList<ClienteAgente>();
		if(agenteOrigen==null || agenteOrigen.getId()==null){
			onError("Favor de proporcionar el agente de origen");
		}
		if(agenteDestino==null || agenteDestino.getId()==null){
			onError("Favor de proporcionar el agente de destino");
		}
		ValorCatalogoAgentes tipoCedulaOrigen=agenteOrigen.getTipoCedula();
		ValorCatalogoAgentes tipoCedulaDestino=agenteDestino.getTipoCedula();
		if(tipoCedulaOrigen==null || tipoCedulaOrigen.getId()==null){
			onError("TipoCedula origen is null");
		}
		if(tipoCedulaDestino==null || tipoCedulaDestino.getId()==null){
			onError("TipoCedula destino is null");
		}
		
		tipoCedulaOrigen=entidadService.findById(ValorCatalogoAgentes.class,tipoCedulaOrigen.getId());
		tipoCedulaDestino=entidadService.findById(ValorCatalogoAgentes.class,tipoCedulaDestino.getId());

		//Si contienen el mismo tipo de cedula entonces la lista de clientes se puede ceder completamente
		if(tipoCedulaDestino.getId().longValue()==tipoCedulaOrigen.getId().longValue()){
			list=listarClientesAgente(agenteOrigen);
		}else{//Validar por ramo-subramo
			list=listarClientesAgentePorRamoSubramo(agenteOrigen,tipoCedulaOrigen,tipoCedulaDestino);
		}
		return list;
	}
	
	/**
	 * Obtiene la lista de clientes por ramo-subramo para ver si un agente destino maneja las mismas lineas de negocio del agente origen,
	 * si es asi, entonces puede ceder todos sus clientes, de lo contrario tiene que revisar subramo por subramo cada poliza del cliente cual
	 * de las polizas maneja el mismo subramo.
	 * @param agenteOrigen
	 * @param cedulaOrigen
	 * @param cedulaDestino
	 * @return
	 * @throws Exception
	 */
	private List<ClienteAgente> listarClientesAgentePorRamoSubramo(Agente agenteOrigen,ValorCatalogoAgentes cedulaOrigen, ValorCatalogoAgentes cedulaDestino) throws Exception{
		List<ClienteAgente> list=new ArrayList<ClienteAgente>();
		CedulaSubramo filtro=new CedulaSubramo();
		filtro.setTipoCedula(cedulaOrigen);
		List<CedulaSubramo> subramosOrigen=cedulaSubramoService.findByFilters(filtro);
		filtro.setTipoCedula(cedulaDestino);
		List<CedulaSubramo> subramosDestino=cedulaSubramoService.findByFilters(filtro);
		if(subramosOrigen==null || subramosOrigen.isEmpty()){
			onError("La cedula de Origen["+cedulaOrigen.getValor()+"] no esta ligada a subramos");
		}
		if(subramosDestino==null || subramosDestino.isEmpty() ){
			onError("La cedula de Destino["+cedulaDestino.getValor()+"] no esta ligada a subramos");
		}
		List<CedulaSubramo> listaSubramosAExcluir=obtenerSubramosAgenteOrigenExtras(subramosOrigen, subramosDestino);
		//Si la lista de subramos a excluir esta vacia entonces todos los subramos del agente de origen son incluidos en el agente destino
		//es decir, que el agente destino maneja las mismas lineas de negocio del agente origen o mas, por ende, si puede heredar todos los clientes
		//del agente origen
		if(listaSubramosAExcluir.isEmpty()){
			list=listarClientesAgente(agenteOrigen);
		}else{
			//FIXME Pendiente, una vez que se tienen los ramos a excluir se debe de traer la lista de clientes de un agente por su id donde 
			//los clientes tienen polizas vigentes cuyos subramos no contengan la lista de subramos "listaSubramosAExcluir"
			//de tal manera que la lista de clientes contenga unicamente los subramos que sean iguales para ambos agentes.
			//TODO
			list = obtenerClientesExcluyendoSubramosExtras(agenteOrigen.getId(),listaSubramosAExcluir);
		}
		return list;
	}
	
	public List<ClienteAgente> obtenerClientesExcluyendoSubramosExtras(Long idAgente,List<CedulaSubramo> subramosExtra){
		List<ClienteAgente> list=new ArrayList<ClienteAgente>();
		//FIXME Call storedprocedure seycos para obtener la lista de clientes de un agente con polizas vigentes y excluyendo subramosExtra
		List<BigDecimal> idClientesExcluir = new ArrayList<BigDecimal>();
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select distinct cotb.PERSONACONTRATANTE_ID"); //per.nombrecompleto, IDTOPOLIZA, sol.CODIGOAGENTE as idAgente, cotb.PERSONACONTRATANTE_ID as cliente, cob.IDTCSUBRAMO as subramo
		queryString.append(" from MIDAS.topoliza p");
		queryString.append(" inner join midas.tocoberturacot cob on cob.IDTOCOTIZACION = p.IDTOCOTIZACION");
		queryString.append(" inner join midas.mcotizacionc  cotc on cotc.numero = p.IDTOCOTIZACION");
		queryString.append(" inner join midas.mcotizacionb  cotb on cotb.MCOTIZACIONC_ID =cotc.id and cotb.id = (select max(id) from midas.mcotizacionb mb where MB.MCOTIZACIONC_ID = cotc.id)");
		queryString.append(" inner join midas.toSolicitud sol on sol.IDTOSOLICITUD = cotb.SOLICITUD_ID");
		queryString.append(" inner join seycos.cliente client on client.ID_CLIENTE = cotb.PERSONACONTRATANTE_ID");
		queryString.append(" inner join midas.vw_persona per on per.IDPERSONA = client.ID_PERSONA");
		queryString.append(" where CODIGOAGENTE = "+idAgente);
		queryString.append(" and p.CLAVEESTATUS = 1");
		queryString.append(" and cob.IDTCSUBRAMO in ("+getSubramosAescluir(subramosExtra)+")");
		idClientesExcluir = this.entityManager.createNativeQuery(queryString.toString()).getResultList();
		
		queryString = new StringBuilder();
		queryString.append(" select model from ClienteAgente model");
		queryString.append(" where model.agente.id = "+ idAgente);
		String idsclient = getIdClientesAexcluir(idClientesExcluir);
		if(!idsclient.equals("")){
			queryString.append(" and model.cliente.idCliente not in ( "+idsclient+")");
		}
		list = this.entityManager.createQuery(queryString.toString(), ClienteAgente.class).getResultList();

		
		return list;
	}
	
	public String getSubramosAescluir(List<CedulaSubramo> subramosExtra){
		StringBuilder subramoStringBuilder = new StringBuilder("");
		String subramoString = "";
		for (CedulaSubramo obj : subramosExtra){
			subramoStringBuilder.append(",").append(obj.getSubramoDTO().getId().toString());
		}
		subramoString = subramoStringBuilder.toString();
		return (subramoString.equals(""))?"":subramoString.substring(1, subramoString.length());
	}
	
	public String getIdClientesAexcluir(List <BigDecimal> idsclientes){
		String idclintesString= "";
		StringBuilder idclintesStringBuilder = new StringBuilder("");
		for (BigDecimal obj : idsclientes){
			idclintesStringBuilder.append(",").append(obj.toString());
		}
		idclintesString = idclintesStringBuilder.toString();
		return (idclintesString.equals(""))?"":idclintesString.substring(1, idclintesString.length());
	}
	/**
	 * Obtiene la lista de cedulas del agente de origen cuyos subramos no son contenidos dentro de la lista de los subramos de las cedulas del agente destino.
	 * @return
	 */
	private List<CedulaSubramo> obtenerSubramosAgenteOrigenExtras(List<CedulaSubramo> cedulasOrigen,List<CedulaSubramo> cedulasDestino){
		List<CedulaSubramo> lista=new ArrayList<CedulaSubramo>();
		for(CedulaSubramo cedulaOrigen:cedulasOrigen){
			boolean existInDestino=false;
			cicloDestino:
			for(CedulaSubramo cedulaDestino:cedulasDestino){
				if(cedulaOrigen.getSubramoDTO().getId().equals(cedulaDestino.getSubramoDTO().getId())){
					existInDestino=true;
					break cicloDestino;
				}
			}
			if(!existInDestino){
				lista.add(cedulaOrigen);
			}
		}
		return lista;
	}
	/**
	 * Lista de clientes por agente
	 * @param agente
	 * @return
	 */
    @Override
    public List<ClienteAgente> listarClientesAgente(Agente agente) throws Exception {
          List<ClienteAgente> list=new ArrayList<ClienteAgente>();
          ClienteAgente filtroClienteAgente=new ClienteAgente();
          filtroClienteAgente.setAgente(agente);
          Long idEstatus= catalogoService.obtenerIdElementEspecifico("Tipos de Estatus","ACTIVO");
          Integer claveEstatus=(idEstatus!=null)?idEstatus.intValue():null;
          
          filtroClienteAgente.setClaveEstatus(claveEstatus);
          list=findByFilters(filtroClienteAgente);
          return list;
    }
	
	@Override
	public HerenciaClientes saveHerenciaCliente(HerenciaClientes herencia) throws Exception {
		Date fechaAlta = new Date(); 
		herencia.setFechaAlta(fechaAlta);		
		HerenciaClientes idHerencia = entidadService.save(herencia);
		return idHerencia;
	}
	
	@Override
	public List<HerenciaClientes> findHerenciasByFilters(HerenciaClientes filtro,String isAgente) throws Exception{
		List<HerenciaClientes> lista = new ArrayList<HerenciaClientes>();
		final StringBuilder queryString = new StringBuilder("select model from HerenciaClientes model left join fetch model.agenteDestino left join fetch model.agenteOrigen");
		Map<String, Object> params = new HashMap<String, Object>();
		
		if(filtro!=null){
			if(isAgente.equals("ORIGEN")){
				if(filtro.getAgenteOrigen()!=null && filtro.getAgenteOrigen().getId()!=null){
					addCondition(queryString, "model.agenteOrigen.id=:idAgente");
					params.put("idAgente", filtro.getAgenteOrigen().getId());
				}
				if(filtro.getAgenteOrigen().getPersona()!=null && filtro.getAgenteOrigen().getPersona().getNombreCompleto()!=null){
					addCondition(queryString, "UPPER(model.agenteOrigen.persona.nombreCompleto) like UPPER(:nombreAgente) ");
					params.put("nombreAgente", "%"+filtro.getAgenteOrigen().getPersona().getNombreCompleto()+"%");
				}
				if(filtro.getAgenteOrigen().getPersona()!=null && filtro.getAgenteOrigen().getPersona().getCurp()!=null){
					addCondition(queryString, "UPPER(model.agenteOrigen.persona.curp) like UPPER(:curp) ");
					params.put("curp", "%"+filtro.getAgenteOrigen().getPersona().getCurp()+"%");
				}
				if(filtro.getAgenteOrigen().getPromotoria().getId()!=null){
					addCondition(queryString, "model.agenteOrigen.promotoria.id = idPromotoria");
					params.put("idPromotoria", filtro.getAgenteOrigen().getPromotoria().getId());
				}
				if(filtro.getAgenteOrigen().getTipoCedula().getId()!=null){
					addCondition(queryString, "model.agenteOrigen.tipoCedula.id = tipoCedula");
					params.put("tipoCedula", filtro.getAgenteOrigen().getTipoCedula().getId());
				}
				if(filtro.getMotivoCesion().getId()!=null){
					addCondition(queryString, "model.motivoCesion.id = motivoCesion");
					params.put("motivoCesion", filtro.getMotivoCesion().getId());
				}
//			}else{
//				HerenciaClientes filtroDestino = new HerenciaClientes();
//				filtroDestino.setAgenteDestino(filtro.getAgenteOrigen());
//				filtro.setAgenteOrigen(null);
//				if(filtroDestino.getAgenteDestino()!=null && filtroDestino.getAgenteDestino().getId()!=null){
//					addCondition(queryString, "model.agenteDestino.id=:idAgente");
//					params.put("idAgente", filtroDestino.getAgenteDestino().getId());
//				}
//				if(filtroDestino.getAgenteDestino().getPersona()!=null && filtroDestino.getAgenteDestino().getPersona().getNombreCompleto()!=null){
//					addCondition(queryString, "model.agenteDestino.persona.nombreCompleto like :nombreAgente");
//					params.put("nombreAgente", filtroDestino.getAgenteDestino().getPersona().getNombreCompleto()+"%");
//				}
			}
		}
		
		try {
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista = query.getResultList();
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return lista;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String buscarAsociacionClienteAgente(ClienteAgente clienteAgente, Long idAgente)
			throws Exception {
		ValorCatalogoAgentes valorCatalogoAgente =catalogoService.obtenerElementoEspecifico("Tipos de Estatus", "ACTIVO");
		clienteAgente.setClaveEstatus(valorCatalogoAgente.getId().intValue());		
		clienteAgente.setAgente(new Agente());
		List<ClienteAgente> clienteAgenteList=findByFilters(clienteAgente);
		String asociado = "DESASOCIADO";
		for(ClienteAgente cliente:clienteAgenteList){
			if(cliente.getAgente()!=null&&cliente.getAgente().getId()!=null){
				if(cliente.getAgente().getId().equals(idAgente)){
					asociado="ASOCIADO"+'-'+cliente.getAgente().getId();
					break;
				}
				else{
					asociado="ASOCIADO"+'-'+cliente.getAgente().getId();
				}
					
			}
		}
		return asociado;
	}
	
	@Override
	public List<HerenciaClientesView> findByFiltersView(HerenciaClientes filtro,String isAgente) throws Exception {
		List<HerenciaClientesView> list = new ArrayList<HerenciaClientesView>();
		Map<Integer, Object> params = new HashMap<Integer, Object>();
		final StringBuilder queryString = new StringBuilder("");
		String tipoAgente = "";
		if(isAgente.equals("ORIGEN")){
			tipoAgente = "IDAGENTEORIGEN";
		}else{
			tipoAgente = "IDAGENTEDESTINO";
		}
		queryString.append(" SELECT entidad.id as idHerencia,persona.NOMBRECOMPLETO as nombreCompletoAgente,cedula.VALOR as descripcionTipoCedula,pro.DESCRIPCION as nombrePromotoria ");
		queryString.append(" ,motivo.VALOR as descripcionMotivoCesion,to_date(entidad.FECHAALTA,'dd/mm/YY') as fechaCesion ");
		queryString.append(" from MIDAS.toHerenciaClientes entidad ");
		queryString.append(" INNER JOIN MIDAS.toAgente agente on (entidad."+tipoAgente+" = agente.ID) ");
		queryString.append(" INNER JOIN MIDAS.VW_PERSONA persona on persona.idpersona = agente.IDPERSONA ");
		queryString.append(" left join MIDAS.TOPROMOTORIA pro on(pro.id=agente.idpromotoria) ");
		queryString.append(" left join MIDAS.TOEJECUTIVO ejecutivo on (pro.ejecutivo_id=ejecutivo.id) ");
		queryString.append(" left join MIDAS.TOGERENCIA gerencia on (gerencia.id=ejecutivo.gerencia_id) ");
		queryString.append(" left join MIDAS.toValorCatalogoAgentes cedula on(agente.idTipoCedulaAgente=cedula.id) ");
		queryString.append(" left join MIDAS.toValorCatalogoAgentes motivo on(entidad.IDMOTIVOCESION=motivo.id) ");
		queryString.append(" INNER JOIN MIDAS.TCGRUPOACTUALIZACIONAGENTES grupo on(grupo.descripcion='Herencia de Clientes') "); 
		queryString.append(" LEFT JOIN MIDAS.TLACTUALIZACIONAGENTES act on(act.grupoActualizacionAgentes_id=grupo.id and act.idRegistro=entidad.id and (act.tipoMovimiento is null or act.tipoMovimiento like 'ALTA' )) ");
		queryString.append(" where ");
		
		if(filtro!=null){
			int index = 1;
			if(filtro.getAgenteOrigen().getPersona()!=null && !filtro.getAgenteOrigen().getPersona().getNombreCompleto().isEmpty()){
				addCondition(queryString, " UPPER(persona.NOMBRECOMPLETO) like UPPER(?) ");
				params.put(index, "%"+filtro.getAgenteOrigen().getPersona().getNombreCompleto()+"%");
				index++;
			}
			if(filtro.getAgenteOrigen().getPersona()!=null && !filtro.getAgenteOrigen().getPersona().getCurp().isEmpty()){
				addCondition(queryString, "UPPER(persona.curp) like UPPER(?) ");
				params.put(index, "%"+filtro.getAgenteOrigen().getPersona().getCurp()+"%");
				index++;
			}
			if(filtro.getAgenteOrigen().getPromotoria().getId()!=null){
				addCondition(queryString, " pro.id = ?");
				params.put(index, filtro.getAgenteOrigen().getPromotoria().getId());
				index++;
			}
			if(filtro.getAgenteOrigen().getPromotoria().getEjecutivo().getId()!=null){
				addCondition(queryString, " ejecutivo.id = ?");
				params.put(index, filtro.getAgenteOrigen().getPromotoria().getEjecutivo().getId());
				index++;
			}
			if(filtro.getAgenteOrigen().getPromotoria().getEjecutivo().getGerencia().getId()!=null){
				addCondition(queryString, " gerencia.id = ?");
				params.put(index, filtro.getAgenteOrigen().getPromotoria().getEjecutivo().getGerencia().getId());
				index++;
			}
			if(filtro.getAgenteOrigen().getTipoCedula().getId()!=null){
				addCondition(queryString, " cedula.id = ? ");
				params.put(index, filtro.getAgenteOrigen().getTipoCedula().getId());
				index++;
			}
			if(filtro.getMotivoCesion().getId()!=null){
				addCondition(queryString, " motivo.id = ?");
				params.put(index, filtro.getMotivoCesion().getId());
				index++;
			}		
			if(isNotNull(filtro.getFechaInicio())){
				Date fecha=filtro.getFechaInicio();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				params.put(index,  fechaString);
				index++;
			}
			if(isNotNull(filtro.getFechaFin())){
				Date fecha=filtro.getFechaFin();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
				params.put(index,  fechaString);
				index++;
			}
					
		}
		
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		String finalQuery=getQueryString(queryString)+" order by entidad.id ASC ";
        Query query=entityManager.createNativeQuery(finalQuery,HerenciaClientesView.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		query.setMaxResults(100);
		list=query.getResultList();
		return list;
		
	}
	
	@Override
	public HerenciaClientes obtenerHerenciaPorId(HerenciaClientes herencia) throws Exception {
		List<HerenciaClientes> list = new ArrayList<HerenciaClientes>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append(" SELECT h.ID,h.IDAGENTEORIGEN,h.IDAGENTEDESTINO,h.IDMOTIVOCESION,h.FECHAALTA from MIDAS.toherenciaclientes h ");
		queryString.append(" where h.ID = " + herencia.getId());			
		
		Query query = entityManager.createNativeQuery(getQueryString(queryString),HerenciaClientes.class);
		list = query.getResultList();
		if(list!=null && !list.isEmpty()){
			return list.get(0);
		}		
		return null;
	}
	
	@Override
	public List<ClienteAgente> consultaDetalleHerenciaClientes(Long herenciaId,Long agenteId) throws Exception{
		List<ClienteAgente> list = new ArrayList<ClienteAgente>();
		Map<Integer, Object> params =  new HashMap<Integer, Object>(); 
		final StringBuilder queryString = new StringBuilder("");
		queryString.append(" select entidad.id as id,entidad.agente_id as idAgente,entidad.idcliente as idCliente ");
		queryString.append(" ,entidad.claveestatus as claveEstatus,entidad.idherencia as idHerencia,persona.nombreCompleto as nombreCompleto "); 
		queryString.append(" from MIDAS.toclienteagente entidad ");		
		queryString.append(" inner join SEYCOS.cliente c on entidad.idCliente = c.id_cliente ");
		queryString.append(" inner join MIDAS.vw_persona persona on persona.IDPERSONA = c.ID_PERSONA ");
//		queryString.append(" inner join toagente agente on agente.id = entidad.AGENTE_ID ");
//		queryString.append(" inner join vw_persona persona on persona.IDPERSONA = agente.IDPERSONA ");
		queryString.append(" where ");
			
		int index = 1;
		if(herenciaId!=null){
			addCondition(queryString, " idherencia = ? ");
			params.put(index, herenciaId);
			index++;
		}
		if(agenteId!=null){
			addCondition(queryString, " agente_id = ? ");
			params.put(index, agenteId);
			index++;
		}
		if(params.isEmpty()){
			int lengthWhere = "where ".length();
			queryString.replace(queryString.length()-lengthWhere, queryString.length(), " ");			
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString),ClienteAgenteView.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list = query.getResultList();
		return list;
	}
	
	@Override
	public void herenciaClientes(List<ClienteAgente> clientes, Agente agenteDestino,HerenciaClientes herencia,String accion) throws Exception {
		if(clientes==null || clientes.isEmpty()){
			onError("Favor de elegir clientes a heredar");
		}
		if(agenteDestino==null || agenteDestino.getId()==null){
			onError("Favor de elegir el agente a quien se heredara la lista de clientes");
		}
		if(accion==null || accion.isEmpty()){
			accion="ACTIVO";
		}
		if(herencia==null || herencia.getId()==null){
			onError("No se recibio el identificador de herencia");
		}		

		if(herencia.getStatus().getValor().equals("INACTIVO")){
			for(ClienteAgente cliente:clientes){			
				cliente.setHerenciaClientes(herencia);			
				//se inserta una nueva relacion pero con el nuevo agente de destino
				//nuevaRelacion.setId(cliente.getId());
				herenciaClientes(cliente,agenteDestino,accion);
			}		
		}
		else{
			for(ClienteAgente cliente:clientes){			
				cliente.setHerenciaClientes(herencia);			
				//se inserta una nueva relacion pero con el nuevo agente de destino
				//nuevaRelacion.setId(cliente.getId());
				herenciaClientes(cliente,agenteDestino,accion);
				//se busca la relacion anterior de cliente-agente y se cambia a inactiva
				ValorCatalogoAgentes status =catalogoService.obtenerElementoEspecifico("Tipos de Estatus", "INACTIVO");
				Map<String,Object> properties = new HashMap<String,Object>();
				properties.put("agente", herencia.getAgenteOrigen());
				properties.put("cliente",cliente.getCliente());
				List<ClienteAgente> list=entidadService.findByProperties(ClienteAgente.class, properties);
				for(ClienteAgente cli:list){
					cli.setClaveEstatus(status.getId().intValue());
					entidadService.save(cli);		
				}				
			}
		}
	}
	/**
	 * Se guarda la relacion de clienteAgente con el agente de destino
	 */
	@Override
	
	public void herenciaClientes(ClienteAgente clienteAgente,Agente agenteDestino,String accion) throws Exception {
		
		ClienteAgente clienteAgenteActual = new ClienteAgente(); // Auxiliar para generar el endoso de cambio de agente correspondiente
		BeanUtils.copyProperties(clienteAgenteActual, clienteAgente);
		
		if(clienteAgente==null){
            onError("Favor de proporcionar el cliente a heredar");
		}
		HerenciaClientes cesion=clienteAgente.getHerenciaClientes();
		clienteAgente=loadById(clienteAgente);
		if(clienteAgente==null || clienteAgente.getCliente()==null || clienteAgente.getCliente().getIdCliente()==null){
			onError("Favor de proporcionar el cliente a heredar");
		}
		if(agenteDestino==null || agenteDestino.getId()==null){
			onError("Favor de proporcionar el agente a asignarle el cliente");
		}
		Agente agente=getReference(Agente.class,agenteDestino.getId());
		if(agente==null || agente.getId()==null){
			onError("El agente con la clave "+agenteDestino.getId()+" no existe");
		}
		ClienteJPA cliente=clienteJpaService.loadById(clienteAgente.getCliente());
		if(cliente==null){
			onError("El cliente no existe con la clave "+clienteAgente.getCliente().getIdCliente());
		}
		//Si es accion para dar de alta una nueva herencia se limpia el objeto para persistirlo
		if("ACTIVO".equals(accion)){
			clienteAgente=new ClienteAgente();
		}
		
		clienteAgente.setAgente(agente);//Setting persistent agente
		clienteAgente.setCliente(cliente);//Setting persisent client
		clienteAgente.setClaveEstatus(cesion.getStatus().getId().intValue());
		clienteAgente.setHerenciaClientes(cesion);
		if(clienteAgente.getId()!=null){
			update(clienteAgente);
			this.generarEndososCambioAgente(clienteAgenteActual, agenteDestino);
		}else{
			persist(clienteAgente);
		}
		
		
	}
	
	@Override
	public void generarEndososCambioAgente(ClienteAgente clienteAgenteActual,
			Agente agenteDestino) {
		List<PolizaDTO> listaPolizasCliente = new ArrayList<PolizaDTO>();

		Date fechaIniVigenciaEndoso = new Date();

		PolizaDTO filtro = new PolizaDTO();
	
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setIdToPersonaContratante(new BigDecimal(
				clienteAgenteActual.getCliente().getIdCliente()));
		filtro.setCotizacionDTO(cotizacionDTO);
		listaPolizasCliente = polizaService.buscarPoliza(filtro, null, null);

		for (PolizaDTO poliza : listaPolizasCliente) {
			if (poliza.getClaveEstatus() == 1
					&& poliza.getCotizacionDTO().getSolicitudDTO()
							.getCodigoAgente() != null
					&& !(poliza.getCotizacionDTO().getSolicitudDTO()
							.getCodigoAgente().longValue() == agenteDestino
							.getId()))// 1 = Estus Activo
			{
				// prepare
				BitemporalCotizacion cotizacion = endosoService
						.getCotizacionEndosoCambioAgente(
								poliza.getIdToPoliza(), fechaIniVigenciaEndoso,
								TipoAccionDTO.getNuevoEndosoCot());

				ArrayList<Comentario> comentarios = new ArrayList<Comentario>();
				Comentario comentario = new Comentario();
				comentario.setValor("Cambio de Agente por asignacion a nuevo Agente");
				comentario.setSolicitudDTO(cotizacion.getValue().getSolicitud());
				comentarios.add(comentario);
				
				cotizacion
						.getValue()
						.getSolicitud()
						.setCodigoAgente(
								new BigDecimal(agenteDestino
										.getId()));
				cotizacion
						.getValue()
						.getSolicitud()
						.setNombreAgente(
								agenteDestino.getPersona().getNombreCompleto());
				cotizacion.getValue().getSolicitud()
						.setComentarioList(comentarios);
				if (agenteDestino.getPromotoria() != null) 
				{
					cotizacion
							.getValue()
							.getSolicitud()
							.setNombreOficinaAgente(
									agenteDestino.getPromotoria()
											.getDescripcion());
					cotizacion
							.getValue()
							.getSolicitud()
							.setCodigoEjecutivo(
									new BigDecimal(agenteDestino
											.getPromotoria().getEjecutivo()
											.getIdEjecutivo()));
					cotizacion
							.getValue()
							.getSolicitud()
							.setNombreEjecutivo(
									agenteDestino.getPromotoria()
											.getEjecutivo()
											.getPersonaResponsable()
											.getNombreCompleto());
					cotizacion
							.getValue()
							.getSolicitud()
							.setIdOficina(
									new BigDecimal(agenteDestino
											.getPromotoria().getEjecutivo()
											.getGerencia().getId()));
					cotizacion
							.getValue()
							.getSolicitud()
							.setNombreOficina(
									agenteDestino.getPromotoria()
											.getEjecutivo().getGerencia()
											.getPersonaResponsable()
											.getNombreCompleto());
				}
				// cotizar
				endosoService.guardaCotizacionEndosoCambioAgente(cotizacion);

				// emitir
				emisionEndosoBitemporalService.emiteEndoso(cotizacion
						.getContinuity().getId(), TimeUtils
						.getDateTime(fechaIniVigenciaEndoso), cotizacion
						.getValue().getSolicitud().getClaveTipoEndoso()
						.shortValue());
			}
		}
	}
	@Override
	public List<AgenteCarteraClientesView> findByFiltersCarteraClientesGrid(ClienteAgente clienteAgente, String historico) throws Exception {
		if(clienteAgente==null){
			onError("Favor de proporcionar el cliente");
		}
		StringBuilder queryString = new StringBuilder("");		
		if(historico==null){			
			queryString.append("SELECT t1.id,t2.IDCLIENTE,t2.NOMBRE as nombreCliente, v.valor as situacion,concat(concat(t2.SIGLASRFC,t2.FECHARFC),t2.HOMOCLAVE) rfc ");
			queryString.append(" FROM MIDAS.toClienteAgente t1 "); 
			queryString.append(" LEFT OUTER JOIN MIDAS.VNCLIENTE t2 ON (t2.IDCLIENTE = t1.IDCLIENTE) "); 
			queryString.append(" inner join MIDAS.tovalorcatalogoagentes v on v.id=t1.claveEstatus "); 
			queryString.append(" WHERE t1.AGENTE_ID = "+clienteAgente.getAgente().getId());			
			queryString.append(" order by t1.id desc ");
		}else{
			//consulta el historico
			queryString.append("SELECT t1.id,t2.IDCLIENTE,t2.NOMBRE as nombreCliente, v.valor as situacion,concat(concat(t2.SIGLASRFC,t2.FECHARFC),t2.HOMOCLAVE) rfc ");
			queryString.append(" FROM MIDAS.toClienteAgente_hist t1 "); 
			queryString.append(" LEFT OUTER JOIN MIDAS.VNCLIENTE t2 ON (t2.IDCLIENTE = t1.IDCLIENTE) "); 
			queryString.append(" inner join MIDAS.tovalorcatalogoagentes v on v.id=t1.claveEstatus "); 
			queryString.append(" WHERE ((t1.AGENTE_ID = "+clienteAgente.getAgente().getId()+")");
			queryString.append(" AND (((t1.h_start_date <= to_timestamp('"+historico.substring(0, 26)+"','DD/MM/YY HH24:MI:SS.FF')) ");  
			queryString.append(" AND ( (t1.H_END_DATE IS NULL) OR (t1.h_end_date >= to_timestamp('"+historico.substring(0, 26)+"','DD/MM/YY HH24:MI:SS.FF')))))) ");
//			queryString.append(" order by t1.id desc ");
		}
		Query query =entityManager.createNativeQuery(queryString.toString(), AgenteCarteraClientesView.class);
		
		List<AgenteCarteraClientesView> listaClientes = new ArrayList<AgenteCarteraClientesView>();
		listaClientes=query.getResultList();
		return  listaClientes;	
	}
}