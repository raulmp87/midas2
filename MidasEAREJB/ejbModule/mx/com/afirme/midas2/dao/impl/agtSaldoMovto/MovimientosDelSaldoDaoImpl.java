package mx.com.afirme.midas2.dao.impl.agtSaldoMovto;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.agtSaldoMovto.MovimientosDelSaldoDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.agtSaldoMovto.MovimientosDelSaldo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.SuspensionAgente;
import mx.com.afirme.midas2.dto.agtSaldoMovto.MovimientosDelSaldoView;
import mx.com.afirme.midas2.dto.fuerzaventa.SuspensionesView;
import mx.com.afirme.midas2.util.MidasException;
@Stateless
public class MovimientosDelSaldoDaoImpl extends EntidadDaoImpl implements MovimientosDelSaldoDao{

	
	@Override
	public List<MovimientosDelSaldo> findByFilters(MovimientosDelSaldo filtro) throws MidasException {
//		if(filter==null){
//			onError("No se indicaron datos de busqueda");
//		}
		
		List<MovimientosDelSaldo> lista = new ArrayList<MovimientosDelSaldo>();		 
		Map<String,Object> params=new HashMap<String,Object>();
		
		final StringBuilder queryString =new StringBuilder("");
		queryString.append(" select model from MovimientosDelSaldo model ");
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}
			if(filtro.getAgente()!=null){				
				if(filtro.getAgente().getIdAgente()!=null){
					addCondition(queryString, "model.agente.idAgente=:idAgente");
					params.put("idAgente", filtro.getAgente().getIdAgente());					
				}				
			}
			if(filtro.getAnioMes()!=null){
				addCondition(queryString, "model.anioMes=:anioMes");
				params.put("anioMes", filtro.getAnioMes());	
			}
		}
						
		
		String finalQuery = getQueryString(queryString);// + " order by model.id desc ";
		Query query = entityManager.createQuery(finalQuery);
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
//		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		query.setMaxResults(100);
		lista=query.getResultList();
 		return lista;
	}

	
	@Override
	public List<MovimientosDelSaldoView> findByFiltersView(MovimientosDelSaldo filtro) throws MidasException {
		List<MovimientosDelSaldoView> lista = new ArrayList<MovimientosDelSaldoView>();
		Map< Integer,Object> params = new HashMap< Integer,Object>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append(" select a.id,a.idEmpresa,v.agente as nombreAgente,to_char(a.fhaplicacion,'DD/MM/YYYY HH24:MI:ss') fehcaCorteEdoctaString,a.idconsecmovto,a.aniomes,a.idconcepto,m.valor as moneda,mo.valor as monedaOrigen ");
		queryString.append(" ,a.tipocambio,a.descripcionmovto  ");
		queryString.append(" ,a.idramocontable,a.idsubramocontable,a.idlineanegocio,to_char(a.fechamovimiento,'DD/MM/YYYY') fechamovimientoString,a.idcentroemisor,a.numpoliza,a.numrenovacionpoliza,a.idcotizacion,a.idversionpol,a.idcentroemisore ");
		queryString.append(" ,a.tipoendoso,a.numeroendoso,a.idsolicitud,a.idversionendoso,a.referencia,a.idcentrooperacion as centroOperacion,a.idremesa,a.idconsecmovtor,a.claveorigenremesa,a.seriefoliorbo ");
		queryString.append(" ,a.numfoliorbo,a.idrecibo,a.idversionrbo,to_char(a.fechavencimientorec,'DD/MM/YYYY') fechavencimientorecString,a.porcentajepartagente,a.importeprimaneta,a.imprcgosspagofr,a.importecomisionagente ");
		queryString.append(" ,a.claveorigenaplic,a.claveorigenmovto,to_char(a.fechacortepagocom,'DD/MM/YYYY') fechacortepagocomString,a.estatusmovimiento,a.idusuariointegeg,to_char(a.fhintegracion,'DD/MM/YYYY') fhintegracionString,to_char(a.fhaplicacion,'DD/MM/YYYY')fhaplicacionString,a.naturalezaconcepto "); 
		queryString.append(" ,a.tipomovtocobranza,a.ftransfdepv,a.idtransacdepv,a.idtreansaccorig,a.idagenteorigen,a.idcobertura,a.esfacultativo,a.aniovigenciapoliza,a.clavesistadmon ");
		queryString.append(" ,a.claveexpcalcdiv,a.importeprimadcp,a.importeprimatotal,a.cvetcptoaco,a.idconceptoo,a.esmasivo,a.importebasecalculo,a.porcentajecomisionagente,a.nummovtomanagente ");
		queryString.append(" ,a.importepagoantimptos,a.porcentajeiva,a.porcentageivaretenido,a.porcentajeisrret,a.importeiva,a.importeivaret,a.importeisr,a.cvesdoimpto,a.esimpuesto ");
		queryString.append(" ,a.aniomespago,a.idcalculo, vca.valor descripcionConcepto ");
		queryString.append(" from midas.toagenteMovimientos a ");
		queryString.append(" inner join MIDAS.vw_agenteInfo v on a.idAgente=v.claveAgente ");
		queryString.append(" inner join MIDAS.tovalorcatalogoagentes m on m.id=a.idMoneda ");
		queryString.append(" left join MIDAS.tovalorcatalogoagentes mo on mo.id=a.idMonedaorigen");
		queryString.append(" left join MIDAS.tovalorcatalogoagentes vca on  vca.idregistro = a.idconcepto and vca.clave = a.tipomovtocobranza");
		queryString.append(" left join MIDAS.tcgrupocatalogoagentes gca on gca.id = vca.grupocatalogoagentes_id and gca.descripcion = 'Concepto Movimiento Agente' ");
		queryString.append(" where a.estatusmovimiento NOT IN (922,122) and ");
		if(isNotNull(filtro)){
			int index=1;
			if(filtro.getAgente()!=null && filtro.getAgente().getIdAgente()!=null){
				addCondition(queryString, "a.idAgente=?");
				params.put(index, filtro.getAgente().getIdAgente());
				index++;
			}
			if(filtro.getAnioMes()!=null){
				addCondition(queryString, "a.anioMes=?");
				params.put(index, filtro.getAnioMes());
				index++;
			}
			
		}
		
		
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		String finalQuery=getQueryString(queryString)+" order by a.idagente, a.fhaplicacion, a.idconsecmovto ";
		Query query=entityManager.createNativeQuery(finalQuery,MovimientosDelSaldoView.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		lista=query.getResultList();	
		return lista;
	}
	
	public String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		if(query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
	public void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	public void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
}
