package mx.com.afirme.midas2.dao.impl.siniestros.cabina.reporteCabina;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoPaseAtencion;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionExportacionDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.utils.CommonUtils;

@Stateless
public class EstimacionCoberturaSiniestroDaoImpl extends EntidadDaoImpl implements EstimacionCoberturaSiniestroDao {
	
	public static final String RUTA_REPORTE   = "estimacion.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina";
	public static final String RUTA_SINIESTRO = "estimacion.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.siniestroCabina";
		
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	private static final String ROL_COORDINADOR_CABINA_AJUSTES_SINIESTROS = "Rol_M2_Coordinador_Cabina_Ajustes_Siniestros";
	private static final String ROL_COORDINADOR_VALUACION_SINIESTROS = "Rol_M2_Coordinador_Valuacion_Siniestros";
	private static final String ROL_COORDINADOR_AJUSTES_AUTOS = "Rol_M2_Coordinador_Ajustes_Autos";
	private static final String ROL_SUBDIRECTOR_SINIESTROS_AUTOS = "Rol_M2_SubDirector_Siniestros_Autos";

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<PaseAtencionSiniestroDTO> buscarPasesAtencion(	PaseAtencionSiniestroDTO filtro ) {

		List<PaseAtencionSiniestroDTO> pasesAtencionLst = new ArrayList<PaseAtencionSiniestroDTO>();
		String claveOficinaReporte = null;
		String claveOficinaSiniestro= null;
		String consecutivoReporte = null;
		String consecutivoSiniestro = null;
		String anioReporte = null;
		String anioSiniestro = null;
		Long idReporteCabina = filtro.getIdReporteCabina();

		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString = new StringBuilder();		
		Query query = null;

		if (filtro.getNumeroReporte() != null && !filtro.getNumeroReporte().equals("")) {
			try{
				String[] numeroReporte = filtro.getNumeroReporte().split( "-" );
				claveOficinaReporte = numeroReporte[0] ;
				consecutivoReporte = numeroReporte[1] ;
				anioReporte = numeroReporte[2] ;
			} catch ( ArrayIndexOutOfBoundsException ex ) {

			}

		}

		if (filtro.getNumeroSiniestro() != null && !filtro.getNumeroSiniestro().equals("")) {
			try{
				String[] numeroSiniestro = filtro.getNumeroSiniestro().split( "-" );
				claveOficinaSiniestro = numeroSiniestro[0];
				consecutivoSiniestro =  numeroSiniestro[1];
				anioSiniestro = numeroSiniestro[2];
			} catch ( ArrayIndexOutOfBoundsException ex ) {

			}

		}


		queryString.append( " SELECT new  mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO( " )
		.append( " estimacion, " )
		.append(" reporte.id, ")
		.append( "func('CONCAT', reporte.claveOficina, func('CONCAT', '-', func('CONCAT', reporte.consecutivoReporte, func('CONCAT', '-', reporte.anioReporte)))), ")
		.append( "func('CONCAT', siniestro.claveOficina, func('CONCAT', '-', func('CONCAT', siniestro.consecutivoReporte, func('CONCAT', '-', siniestro.anioReporte)))), ")
		.append( "(SELECT valorFijo.descripcion FROM CatValorFijo valorFijo" )
		.append( " 	WHERE valorFijo.grupo.codigo = :catTipoPase " )
		.append( " 	AND valorFijo.codigo = estimacion.tipoPaseAtencion), " )
		.append( " func('MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerCoberturaPorEstimacion', estimacion.id), ")
		.append( " estimacion.fechaCreacion, " )
		.append( " reporte.estatus, " )
		.append( " (CASE WHEN (estimacion.tipoEstimacion = :daniosMateriales OR estimacion.tipoEstimacion = :rcVehiculo)")
		.append( "	THEN :true" )
		.append( "  ELSE :false")
		.append( "	END), " )
		.append( " (CASE WHEN (estimacion.tipoEstimacion = :daniosMateriales OR estimacion.tipoEstimacion = :rcVehiculo)")
		.append( "	THEN ")
		.append( "		CASE WHEN(SELECT COALESCE(bitacora.id, 0) FROM CommBitacora bitacora" )
		.append( "  	  WHERE bitacora.status = :estatusBitacora ")
		.append( "  	  AND bitacora.proceso.codigo = :procesoBitacora ")
		.append( "  	  AND bitacora.folio = estimacion.folio ")
		.append( "        ) > 0 THEN :true" )
		.append( "  	ELSE :false")
		.append( "		END " )
		.append( "  ELSE :false")
		.append( "	END), " )
		.append( " (CASE WHEN (:puedeImprimir = 1 AND estimacion.esEditable = 'EDT' AND estimacion.tipoPaseAtencion != :soloRegistro)")
		.append( "		THEN :true" )
		.append( "  	ELSE :false")
		.append( "	END) " )
		.append( " )" )
		.append( " FROM EstimacionCoberturaReporteCabina estimacion ")
		.append( " JOIN estimacion.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina reporte " )
		.append( " LEFT JOIN reporte.siniestroCabina siniestro " )

		.append( " WHERE estimacion.tipoEstimacion <> (:estimacionDirecta) ");
		
		
		Utilerias.agregaHashLista(listaParametrosValidos, "catTipoPase", CatGrupoFijo.TIPO_CATALOGO.PASES_DE__ATENCION);
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusBitacora", CommBitacora.STATUS_BITACORA.TERMINADO.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "procesoBitacora", CommProceso.TIPO_PROCESO.HGS_ALTA_PASE.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "true", Boolean.TRUE);
		Utilerias.agregaHashLista(listaParametrosValidos, "false", Boolean.FALSE);
		Utilerias.agregaHashLista(listaParametrosValidos, "soloRegistro", TipoPaseAtencion.SOLO_REGISTRO.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estimacionDirecta", TipoEstimacion.DIRECTA.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "rcVehiculo", TipoEstimacion.RC_VEHICULO.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "daniosMateriales", TipoEstimacion.DANIOS_MATERIALES.getValue());

		if (CommonUtils.isValid(claveOficinaReporte) &&
				CommonUtils.isValid(consecutivoReporte) &&
				CommonUtils.isValid(anioReporte)) {

			queryString.append( " AND " + RUTA_REPORTE + ".claveOficina = :claveOficinaRpt" );
			queryString.append( " AND " + RUTA_REPORTE + ".consecutivoReporte = :consecutivoReporteRpt" );
			queryString.append( " AND " + RUTA_REPORTE + ".anioReporte = :anioReporteRpt" );

			Utilerias.agregaHashLista(listaParametrosValidos, "claveOficinaRpt", claveOficinaReporte);
			Utilerias.agregaHashLista(listaParametrosValidos, "consecutivoReporteRpt", consecutivoReporte);
			Utilerias.agregaHashLista(listaParametrosValidos, "anioReporteRpt", anioReporte);
		}

		if (CommonUtils.isValid(claveOficinaSiniestro) &&
				CommonUtils.isValid(consecutivoSiniestro) &&
				CommonUtils.isValid(anioSiniestro)) {

			queryString.append( " AND " + RUTA_SINIESTRO + ".claveOficina = :claveOficinaSto" );
			queryString.append( " AND " + RUTA_SINIESTRO + ".consecutivoReporte = :consecutivoReporteSto" );
			queryString.append( " AND " + RUTA_SINIESTRO + ".anioReporte = :anioReporteSto" );

			Utilerias.agregaHashLista(listaParametrosValidos, "claveOficinaSto", claveOficinaSiniestro);
			Utilerias.agregaHashLista(listaParametrosValidos, "consecutivoReporteSto", consecutivoSiniestro);
			Utilerias.agregaHashLista(listaParametrosValidos, "anioReporteSto", anioSiniestro);
		}

		EstimacionCoberturaReporteCabina estimacion = filtro.getEstimacionCoberturaReporte();
		String tipoPaseAtencion = "";

		if( estimacion != null ){
			String folio = estimacion.getFolio();
			if( folio != null && !folio.isEmpty() ){
				queryString.append( " AND  estimacion.folio = :folio" );
				Utilerias.agregaHashLista(listaParametrosValidos, "folio", folio);
			}
		}

		if( estimacion != null ){
			String tipoEstimacion = estimacion.getTipoEstimacion();
			tipoPaseAtencion = estimacion.getTipoPaseAtencion();

			if( tipoEstimacion != null && !tipoEstimacion.isEmpty() ){
				queryString.append( " AND  estimacion.tipoEstimacion = :tipoEstimacion" );
				Utilerias.agregaHashLista(listaParametrosValidos, "tipoEstimacion", tipoEstimacion);
			}
			if( tipoPaseAtencion != null && !tipoPaseAtencion.equals("") ){
				queryString.append( " AND  estimacion.tipoPaseAtencion = :tipoPaseAtencion" );
				Utilerias.agregaHashLista(listaParametrosValidos, "tipoPaseAtencion", tipoPaseAtencion);
			}
		}

		if( filtro.getFechaInicial() != null && filtro.getFechaFinal() != null ){
			queryString.append( " AND  FUNC('trunc', estimacion.fechaCreacion) BETWEEN FUNC('trunc', :fechaInicial) AND FUNC('trunc', :fechaFinal)" );
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaInicial", filtro.getFechaInicial());
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFinal", filtro.getFechaFinal() );
		} else if( filtro.getFechaInicial() != null ){
			queryString.append( " AND  FUNC('trunc', estimacion.fechaCreacion) = FUNC('trunc', :fechaInicial) " );
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaInicial", filtro.getFechaInicial());
		} else if( filtro.getFechaFinal() != null ){
			queryString.append( " AND  FUNC('trunc', estimacion.fechaCreacion) = FUNC('trunc', :fechaFinal) " );
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFinal", filtro.getFechaFinal() );
		}
		
		if (idReporteCabina != null) {
			queryString.append( " AND reporte.id = :idReporteCabina   " );
			Utilerias.agregaHashLista(listaParametrosValidos, "idReporteCabina", idReporteCabina );
		}
	
		queryString.append(" ORDER BY estimacion.folio ");
		
		
		String[] listaRolesImpresionSoloCopia = new String[]{ROL_COORDINADOR_CABINA_AJUSTES_SINIESTROS,
				ROL_COORDINADOR_VALUACION_SINIESTROS,
				ROL_COORDINADOR_AJUSTES_AUTOS,
				ROL_SUBDIRECTOR_SINIESTROS_AUTOS}; 
		
		Boolean usuarioPuedeImprimir = usuarioService.tieneRolUsuarioActual(listaRolesImpresionSoloCopia);
		
		Utilerias.agregaHashLista(listaParametrosValidos, "puedeImprimir", usuarioPuedeImprimir ? 1 : 0 );
		
		
		//		----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

		query = this.entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

		pasesAtencionLst = query.getResultList();

		return pasesAtencionLst;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PaseAtencionExportacionDTO> buscarPasesAtencionExportacion(PaseAtencionSiniestroDTO filtro ) {

		final String spName = "MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerPasesDetallado"; 
		StoredProcedureHelper storedHelper = null;
		List<PaseAtencionExportacionDTO> pases = null;
		try{
			EstimacionCoberturaReporteCabina estimacion = filtro.getEstimacionCoberturaReporte();
			storedHelper = new StoredProcedureHelper(spName,StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
			.estableceMapeoResultados(
					PaseAtencionExportacionDTO.class.getCanonicalName(),
					"numeroSiniestro," +               
					"anioSiniestro," +                   
					"numeroReporte," +                   
					"nombreAjustador," +                   
					"tallerAsignado," +                 
					"marca," +                   
					"desVehiculo," +                      
					"modelo," +                  
					"numeroSerie," +                       
					"placas," +                  
					"color," +                       
					"nombreContratante," +                 
					"nombreAfectado," +            
					"telefonoUno," +               
					"telefonoDos," +                  
					"fechaOcurrido," +                  
					"mesReporte," +                
					"fechaReportado," +               
					"horaReportada," +               
					"nombreCobertura," +                
					"tipoPase," +              
					"folio," +                     
					"numeroPoliza," +                      
					"numeroInciso," +                 
					"moneda," +                 
					"inicioVigencia," +                    
					"responsabilidad," +               
					"causaSiniestro," +              
					"terminoAjuste," +               
					"hospital," +                
					"ingenieroAsignado," +                 
					"fechaPaseAtencion," +            
					"estado," +            
					"ciudad," +                            
					"estatusPase," +                      
					"operacion," +                  
					"nombreProducto," +                    
					"companiaTercero," +               
					"folioTercero," +              
					"siniestroTercero," +                 
					"estimadoInicial," +             
					"ajusteMas," +          
					"ajusteMenos," +                
					"estimacionFinal," + 
					"reciboOrdenCia," +                    
					"nombreGerencia," +                    
					"codigoOficina," +                     
					"nombreAgente," +                      
					"centroEmisor," +                      
					"proceso," +                           
					"valuacion," +                         
					"fechaIngresoTaller," +                
					"fechaValuacion," +                    
					"fechaReingresoReparacion," +          
					"fechaUltimoSurtido," +                
					"fechaTerminacion," +                  
					"importeRefacciones," +            
					"importeManoObra," +               
					"importeTotalValuacion," +         
					"nombreValuador," +                   
					"sumaAsegurada",        
					                                  	
					
					"numero_siniestro," +               
					"anio_siniestro," +                   
					"numero_reporte," +                   
					"nombre_ajustador," +                   
					"taller_asignado," +                 
					"marca," +                   
					"vehiculo," +                      
					"modelo," +                  
					"numero_serie," +                       
					"placas," +                  
					"color," +                       
					"nombre_contratante," +                 
					"nombre_afectado," +            
					"telefono_uno," +               
					"telefono_dos," +                  
					"fecha_ocurrido," +                  
					"mes_reporte," +                
					"fecha_reportado," +               
					"hora_reportado," +               
					"cobertura," +                
					"tipo_pase," +              
					"folio," +                     
					"numero_poliza," +                      
					"numero_inciso," +                 
					"moneda," +                 
					"inicio_vigencia," +                    
					"responsabilidad," +               
					"causa_siniestro," +              
					"termino_ajuste," +               
					"hospital," +                
					"ingeniero_asignado," +                 
					"fecha_pase," +            
					"estado," +            
					"ciudad," +                            
					"estatus," +                      
					"operacion," +                  
					"nombre_producto," +                    
					"compania_seguro_tercero," +               
					"folio_tercero," +              
					"siniestro_tercero," +                 
					"estimacion_inicial," +             
					"ajuste_mas," +          
					"ajuste_menos," +                
					"estimacion_final," +
					"recibida_orden_cia," +                    
					"gerencia," +                    
					"codigo_oficina," +                     
					"agente," +                      
					"centro_emisor," +                      
					"proceso," +                           
					"valuacion," +                         
					"fecha_ingreso_taller," +                
					"fecha_valuacion," +                    
					"fecha_reingreso_reparacion," +          
					"fecha_ultimo_surtido," +                
					"fecha_terminacion," +                  
					"importe_refacciones," +            
					"importe_mano_obra," +               
					"importe_total_valuacion," +         
					"nombre_valuador," +                   
					"suma_asegurada"       
					); 
			
			storedHelper.estableceParametro("p_reporteId", filtro.getIdReporteCabina() != null ? filtro.getIdReporteCabina() : null);
			storedHelper.estableceParametro("p_numeroReporte", filtro.getNumeroReporte() != null 
											&& !filtro.getNumeroReporte().equals("") ? filtro.getNumeroReporte() : null);
			storedHelper.estableceParametro("p_numeroSiniestro", filtro.getNumeroSiniestro() != null 
											&& !filtro.getNumeroSiniestro().equals("")?  filtro.getNumeroSiniestro() : null);
			storedHelper.estableceParametro("p_fechaInicial", filtro.getFechaInicial() != null ? filtro.getFechaInicial() : null);
			storedHelper.estableceParametro("p_fechaFinal", filtro.getFechaFinal() != null ? filtro.getFechaFinal() : null);	
				
			storedHelper.estableceParametro("p_folio", (estimacion != null  
											&& StringUtils.isNotEmpty(estimacion.getFolio())) ? estimacion.getFolio() : null);
			storedHelper.estableceParametro("p_tipoPase", (estimacion != null  
											&& StringUtils.isNotEmpty(estimacion.getTipoPaseAtencion())) ? estimacion.getTipoPaseAtencion() : null);
			storedHelper.estableceParametro("p_tipoEstimacion", (estimacion != null  
											&& StringUtils.isNotEmpty(estimacion.getTipoEstimacion())) ? estimacion.getTipoEstimacion() : null);
	

			

			pases = storedHelper.obtieneListaResultados();
		}catch(Exception e){
			LogDeMidasInterfaz.log(
					"Excepcion general en PolizaSiniestroDAO.buscarIncisos..."
							+ this, Level.WARNING, e);
		}
		return pases;
	}

	

	@Override
	public Integer obtenerSecuenciaPaseAtencion(Long idReporteCabina) {
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT MAX(EstCobRepCab.secuenciaPaseAtencion)	");
		queryString.append(" FROM EstimacionCoberturaReporteCabina EstCobRepCab");
		queryString.append(" WHERE EstCobRepCab.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id = :idReporteCabina");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idReporteCabina"     , idReporteCabina);
		Object obj = query.getSingleResult();
		return ((obj==null)?1:((Integer)obj)+1);
	}
	
	@Override
	public Integer obtenerSecuenciaPaseDeCobertura(Long idCoberturaReporte) {
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT MAX(EstCobRepCab.secuenciaPaseDeCobertura)	");
		queryString.append(" FROM EstimacionCoberturaReporteCabina EstCobRepCab");
		queryString.append(" WHERE EstCobRepCab.coberturaReporteCabina.id = :idCoberturaReporte");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idCoberturaReporte"     , idCoberturaReporte);
		Object obj = query.getSingleResult();
		return ((obj==null)?1:((Integer)obj)+1);
	}

	
	

	
	
	@SuppressWarnings("unchecked")
	@Deprecated
	@Override
	public List<CatValorFijo> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad) {
		StringBuilder queryString = new StringBuilder("SELECT model FROM CatValorFijo model, TipoPaseAtencionConf config, CatGrupoFijo grupo ");
		queryString.append("WHERE config.tipoEstimacion = :codigoTipoEstimacion AND config.tipoResponsabilidad = :codigoTipoResponsabilidad AND config.tipoPase = model.codigo AND config.activo = :activo ");
		queryString.append("AND grupo.codigo = :tipoGrupo AND grupo.id = model.grupo.id AND model.activo = :activo ORDER BY model.orden");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("codigoTipoEstimacion", codigoTipoEstimacion);
		query.setParameter("codigoTipoResponsabilidad", codigoTipoResponsabilidad);
		query.setParameter("activo", Boolean.TRUE);
		query.setParameter("tipoGrupo", CatGrupoFijo.TIPO_CATALOGO.PASES_DE__ATENCION);		
		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CatValorFijo> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad, String codigoCausaSiniestro, String codigoTerminoAjuste) {
		StringBuilder queryString = new StringBuilder("SELECT model FROM CatValorFijo model, TipoPaseAtencionConf config, CatGrupoFijo grupo ");
		queryString.append("WHERE config.tipoEstimacion = :codigoTipoEstimacion AND config.tipoResponsabilidad = :codigoTipoResponsabilidad AND config.tipoPase = model.codigo AND config.activo = :activo ");
		queryString.append(" AND config.causaSiniestro = :codigoCausaSiniestro AND config.terminoAjuste = :codigoTerminoAjuste ");
		queryString.append("AND grupo.codigo = :tipoGrupo AND grupo.id = model.grupo.id AND model.activo = :activo ORDER BY model.orden");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("codigoTipoEstimacion", codigoTipoEstimacion);
		query.setParameter("codigoTipoResponsabilidad", codigoTipoResponsabilidad);
		query.setParameter("codigoCausaSiniestro", codigoCausaSiniestro);
		query.setParameter("codigoTerminoAjuste", codigoTerminoAjuste);
		query.setParameter("activo", Boolean.TRUE);
		query.setParameter("tipoGrupo", CatGrupoFijo.TIPO_CATALOGO.PASES_DE__ATENCION);		
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CatValorFijo> obtenerTipoDePaseMenosSoloRegistro() {
		StringBuilder queryString = new StringBuilder("SELECT model FROM CatValorFijo model, CatGrupoFijo grupo ");
		queryString.append("WHERE grupo.codigo = :tipoGrupo AND grupo.id = model.grupo.id AND model.activo = :activo ");
		queryString.append("AND model.codigo NOT IN ( 'SRE','SIPACSRE' )");
		queryString.append(" ORDER BY model.orden ");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("activo", Boolean.TRUE);
		query.setParameter("tipoGrupo", CatGrupoFijo.TIPO_CATALOGO.PASES_DE__ATENCION);	
		return query.getResultList();
	}
	
	@Override
	public void actualizaEdicionDeEstimaciones(Long reporteCabinaId, String tipoSiniestro, String tipoResponsabilidad,String terminoAjuste) {
		String spName = "MIDAS.PKGCONF_ESTIMACIONES.spConf_estimaciones"; 
        StoredProcedureHelper storedHelper 			= null;
        String propiedades 							= "";
        String columnasBaseDatos 						= "";
        EstimacionCoberturaReporteCabina estimacionResponse = null;
        try {
      	  propiedades = "id";
      	  columnasBaseDatos = "RESPONSE";
            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(EstimacionCoberturaReporteCabina.class.getCanonicalName(), propiedades, columnasBaseDatos);
            storedHelper.estableceParametro("pReporteCabinaId", reporteCabinaId);
            storedHelper.estableceParametro("pTipoResponsabilidad", tipoResponsabilidad);
            estimacionResponse  = (EstimacionCoberturaReporteCabina)storedHelper.obtieneResultadoSencillo();
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en configura estimaciones ..."+descErr
                                           , Level.WARNING, e);    
             }
        }
	}
	
	@SuppressWarnings("unchecked")
	public List<EstimacionCoberturaReporteCabina> pasesRCVehiculoAnteriores(Long estimacionId, String numeroSerie) {
		StringBuilder queryString = new StringBuilder("SELECT model FROM EstimacionCoberturaReporteCabina model, TerceroRCVehiculo pase ");
		queryString.append("WHERE pase.id = model.id ");
		queryString.append("AND pase.numeroSerie = :numeroSerie ");
		if (estimacionId != null) {
			queryString.append("AND model.id <> :estimacionId ");
		}
		queryString.append("ORDER BY model.fechaCreacion desc ");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("numeroSerie", numeroSerie);
		if (estimacionId != null) {
			query.setParameter("estimacionId", estimacionId);
		}
		
		return query.getResultList();
	}
	
	@Override
	public List<CoberturaSeccionDTO> obtenerCoberturasPorSeccion(Long idSeccion){
		//Esto regresa un listado de objetos unmanaged lo que lo hace mas ligero y sirve para consultas
		StringBuilder queryString = new StringBuilder(
				"SELECT new mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO(").
				append("model.id, model.coberturaDTO, model.seccionDTO, model.claveObligatoriedad) from CoberturaSeccionDTO model ");
		if(idSeccion != null){
			queryString.append("WHERE model.seccionDTO.idToSeccion = :idToSeccion");
		}else{
			 //no se pq aplica solo con Id null, pero asi estaba en el servicio original
			queryString.append(" ORDER BY model.coberturaDTO.claveTipoCalculo");
		}		
		TypedQuery<CoberturaSeccionDTO> query = entityManager.createQuery(queryString.toString(), CoberturaSeccionDTO.class);
		if(idSeccion != null){
			query.setParameter("idToSeccion", idSeccion);
		}
		return query.getResultList();			
	}
	
	/**
	 * Metodo mejorado para retorno rapido de coberturas por seccion.
	 * Se utilzaron unmanaged entities para hacerlo más ligero ya que solo es para consulta.
	 */
	@Override
	public List<CoberturaSeccionSiniestroDTO> obtenerCoberturasSiniestrosPorSeccion( Long idToSeccion ){		
		//Esto regresa un listado de objetos UNMANAGED lo que lo hace mas ligero y sirve para consultas
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO("); 
		queryString.append("conf.cveSubTipoCalculoCobertura, cobSeccion,  func('CONCAT',coalesce(conf.nombreCobertura, cobSeccion.coberturaDTO.nombreComercial), func('CONCAT', ' - ', cobSeccion.seccionDTO.nombreComercial))) ");
		//Se requirio usar CONCAT con func debido a un bug en eclipselink 2.4 donde el concat propio de JPA retorna un Boolean en lugar de un String
		queryString.append("FROM CoberturaSeccionDTO cobSeccion LEFT JOIN ConfiguracionCalculoCoberturaSiniestro conf ");
		queryString.append("WHERE (conf.cveTipoCalculoCobertura is null or conf.cveTipoCalculoCobertura = cobSeccion.coberturaDTO.claveTipoCalculo) ");
		queryString.append("AND (conf.tipoConfiguracion is null or conf.tipoConfiguracion = cobSeccion.coberturaDTO.tipoConfiguracion) ");
		if(idToSeccion != null){			
			queryString.append("AND cobSeccion.seccionDTO.idToSeccion = :idToSeccion");
		}else{
			//no se pq aplica solo con Id null, pero asi estaba en el servicio original
			queryString.append(" ORDER BY cobSeccion.coberturaDTO.claveTipoCalculo");
		}		
		TypedQuery<CoberturaSeccionSiniestroDTO> query = entityManager.createQuery(queryString.toString(), CoberturaSeccionSiniestroDTO.class);
		if(idToSeccion != null){
			query.setParameter("idToSeccion", idToSeccion);
		}
		return query.getResultList();		
	}



}
