package mx.com.afirme.midas2.dao.impl.notificaciones;

import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.notificaciones.NotificacionesDao;
import mx.com.afirme.midas2.domain.notificaciones.ConfiguracionNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.DestinatariosNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.MovimientosProcesos;
import mx.com.afirme.midas2.domain.notificaciones.ProcesosAgentes;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

@Stateless
public class NotificacionesDaoImpl extends EntidadDaoImpl implements NotificacionesDao{
	
	private EntidadService entidadService;
	private ValorCatalogoAgentesService catalogoService;
	
	@Override
	public List<ConfiguracionNotificaciones> findAllConfig() {
//		return entidadService.findAll(ConfiguracionNotificaciones.class);
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select model from ConfiguracionNotificaciones model where model.id > 0");
		return  this.entityManager.createQuery(queryString.toString()+" ORDER BY model.id ASC").getResultList();
//		return entidadService.findByPropertiesWithOrder(ConfiguracionNotificaciones.class, null, "id ASC");
	}

	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
	}

	@Override
	public ConfiguracionNotificaciones findByIdConfig(Long id) {
		ConfiguracionNotificaciones conf=entidadService.findById(ConfiguracionNotificaciones.class, id);
		return conf;
	}

	@Override
	public List<DestinatariosNotificaciones> findDestinatariosByIdConfig(ConfiguracionNotificaciones idConfiguracion){
		return entidadService.findByProperty(DestinatariosNotificaciones.class, "idConfigNotificacion", idConfiguracion);
	}

	@Override
	public Long saveConfigNotificaciones(ConfiguracionNotificaciones configNotificaciones){
		Long idConfig=null;
		configNotificaciones.setFechaCreacion(new Date());
		List<DestinatariosNotificaciones>destList=configNotificaciones.getDestinatariosList();
		try{
			if(configNotificaciones.getId()!=null){
				List<DestinatariosNotificaciones>listDestEliminar=entidadService.findByProperty(DestinatariosNotificaciones.class, "idConfigNotificacion.id", configNotificaciones.getId());
				for(DestinatariosNotificaciones dest:listDestEliminar){
					entidadService.remove(dest);
				}
			}
			configNotificaciones.setIdProceso(entidadService.findById(ProcesosAgentes.class, configNotificaciones.getIdProceso().getId()));
			configNotificaciones.setIdMovimiento(entidadService.findById(MovimientosProcesos.class, configNotificaciones.getIdMovimiento().getId()));			
			configNotificaciones.setDestinatariosList(null);
			configNotificaciones=entidadService.save(configNotificaciones);		
		for(int i=0;i<destList.size();i++){
			if(destList.get(i)!=null){				
				destList.get(i).setIdConfigNotificacion(configNotificaciones);
				destList.get(i).setIdModoEnvio(catalogoService.loadById(destList.get(i).getIdModoEnvio()));
				destList.get(i).setIdTipoDestinatario(catalogoService.loadById(destList.get(i).getIdTipoDestinatario()));
				destList.get(i).setIdTipoCorreoEnvio(catalogoService.loadById(destList.get(i).getIdTipoCorreoEnvio()));
				entidadService.save(destList.get(i));
			}
		}		
//		entidadService.refresh(configNotificaciones);
		}
		catch(Exception e){			
			System.out.println(e.toString());
		}
		return idConfig;
	}
	
	@Override
	public List<ConfiguracionNotificaciones> findByFilters(ConfiguracionNotificaciones config) {
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		if(config.getIdProceso().getId()!=null){
			parametros.put("idProceso.id", config.getIdProceso().getId());
		}
		if(config.getIdMovimiento().getId()!=null){
			parametros.put("idMovimiento.id", config.getIdMovimiento().getId());
		}
		return entidadService.findByPropertiesWithOrder(ConfiguracionNotificaciones.class, parametros, "id ASC");
	}
	
	@Override
	public void eliminarNotificacion(ConfiguracionNotificaciones config) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("DELETE FROM DestinatariosNotificaciones model where model.idConfigNotificacion.id ="+config.getId());
		this.entityManager.createQuery(queryString.toString()).executeUpdate();
		config=findByIdConfig(config.getId());
		entidadService.remove(config);		
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}

	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> arg0, String arg1, Map<String, Object> arg2,
			Map<String, Object> arg3, StringBuilder arg4, StringBuilder arg5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> arg0, Map<String, Object> arg1, Map<String, Object> arg2,
			StringBuilder arg3, StringBuilder arg4) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}
	
}
