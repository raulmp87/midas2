package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza.seccion.paquete.cobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionDAO;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;

import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioCobPaqSeccionDaoImpl extends JpaDao<Long, ExcepcionSuscripcion> implements NegocioCobPaqSeccionDAO {

	@Override
	public List<NegocioCobPaqSeccion> getNegocioCobPaqSeccionAsociados(
			NegocioCobPaqSeccion negocioCobPaqSeccion) {
		return getNegocioCobPaqSeccionAsociados(null , negocioCobPaqSeccion.getEstadoDTO().getStateId(),
				negocioCobPaqSeccion.getCiudadDTO().getCityId(), negocioCobPaqSeccion.getMonedaDTO().getIdTcMoneda(),
				negocioCobPaqSeccion.getNegocioPaqueteSeccion().getIdToNegPaqueteSeccion(),
				negocioCobPaqSeccion.getTipoUsoVehiculo()!=null?negocioCobPaqSeccion.getTipoUsoVehiculo().getIdTcTipoUsoVehiculo():null,
				negocioCobPaqSeccion.getAgente()!=null?negocioCobPaqSeccion.getAgente().getId():null,
				negocioCobPaqSeccion.isRenovacion());
	}
	
	public List<NegocioCobPaqSeccion> getNegocioCobPaqSeccionAsociados(BigDecimal idToCobertura,String stateId,
			String cityId, Short idTcMoneda,Long idToNegPaqueteSeccion, BigDecimal IdTcTipoUsoVehiculo, Long agenteId, Boolean esRenovacion){

		Map<String, Object> parameters = new HashMap<String, Object>(1);
		List<NegocioCobPaqSeccion> negocioCobPaqSeccionList = new ArrayList<NegocioCobPaqSeccion>(1);
		String query = "Select model from NegocioCobPaqSeccion model where "
				+ "model.negocioPaqueteSeccion.idToNegPaqueteSeccion = :idToNegPaqueteSeccion and "
				+ "model.monedaDTO.idTcMoneda = :idTcMoneda and ";

		parameters.put("idToNegPaqueteSeccion", idToNegPaqueteSeccion);
		parameters.put("idTcMoneda", idTcMoneda);
		
		if (stateId != null) {
			query += "model.estadoDTO.stateId = :stateId and ";
			parameters.put("stateId", stateId);
		} else {
			query += "model.estadoDTO is null and ";
		}
		if (cityId != null) {
			query += "model.ciudadDTO.cityId = :cityId and ";
			parameters.put("cityId", cityId);
		} else {
			query += "model.ciudadDTO is null and ";
		}

		if (idToCobertura != null) {
			query += "model.coberturaDTO.idToCobertura = :idToCobertura and ";
			parameters.put("idToCobertura", idToCobertura);
		}
		
		if (IdTcTipoUsoVehiculo != null){
			query += "model.tipoUsoVehiculo.idTcTipoUsoVehiculo = :IdTcTipoUsoVehiculo and ";
			parameters.put("IdTcTipoUsoVehiculo", IdTcTipoUsoVehiculo);
		} else {
			query += "model.tipoUsoVehiculo is null and ";
		}
		
		if (agenteId != null){
			query += "model.agente.id = :agenteId and ";
			parameters.put("agenteId", agenteId);
		} else {
			query += "model.agente is null and ";
		}
		
		if (esRenovacion != null){
			query += "model.renovacion = :esRenovacion";
			parameters.put("esRenovacion", esRenovacion);
		}  else {
			query += "model.renovacion is null";
		}
		
		@SuppressWarnings("unchecked")
		List<Object> tempList = this.executeQueryMultipleResult(query,
				parameters);
		for (Object item : tempList) {
			NegocioCobPaqSeccion tempNegocioCobPaqSeccion = (NegocioCobPaqSeccion) item;
			negocioCobPaqSeccionList.add(tempNegocioCobPaqSeccion);
		}

		return negocioCobPaqSeccionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NegocioCobPaqSeccion> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza, 
			BigDecimal idToSeccion, Long idPaquete, String idEstado,
			String idMunicipio, Short idMoneda) {
		StringBuilder stm = new StringBuilder("");
		stm.append("SELECT model FROM NegocioCobPaqSeccion model ");
		stm.append("where model.negocioPaqueteSeccion.negocioSeccion.negocioTipoPoliza.negocioProducto.negocio.idToNegocio =:idToNegocio ");
		stm.append("and model.negocioPaqueteSeccion.negocioSeccion.negocioTipoPoliza.negocioProducto.productoDTO.idToProducto =:idToProducto ");
		stm.append("and model.negocioPaqueteSeccion.negocioSeccion.negocioTipoPoliza.tipoPolizaDTO.idToTipoPoliza =:idTipoPoliza ");
		stm.append("and model.negocioPaqueteSeccion.negocioSeccion.seccionDTO.idToSeccion =:idToSeccion ");
		stm.append("and model.negocioPaqueteSeccion.paquete.id =:idPaquete ");
		stm.append("and model.monedaDTO.idTcMoneda =:idMoneda ");
		if (idEstado == null) {
			stm.append("and model.estadoDTO is null ");
			
		} else {
			if(idEstado.equals("notNull"))
				stm.append("and model.estadoDTO is not null ");
			else 
				stm.append("and model.estadoDTO.stateId = :stateId  ");
		}
		if (idMunicipio == null) {
			stm.append("and model.ciudadDTO is null ");
		} else {
			if (idMunicipio.equals("notNull")) {
				stm.append("and model.ciudadDTO is not null ");
			} else {
				stm.append("and model.ciudadDTO.cityId = :cityId ");
			}
		}
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		parameters.put("idToNegocio", IdNegocio);
		parameters.put("idToProducto", idToProducto);
		parameters.put("idTipoPoliza", idTipoPoliza);
		parameters.put("idToSeccion", idToSeccion);
		parameters.put("idPaquete", idPaquete);
		parameters.put("idMoneda", idMoneda);
		if (idEstado != null && !idEstado.equals("notNull") ) {
			parameters.put("stateId", idEstado);
		}
		if (idMunicipio != null && !idMunicipio.equals("notNull")) {
			parameters.put("cityId", idMunicipio);
		}

		Query q = createQuery(stm.toString(), parameters);
		q.setHint(QueryHints.BATCH, "model.negocioDeducibleCobList")
				.setHint(QueryHints.BATCH, "model.coberturaDTO")
				.setHint(QueryHints.BATCH_TYPE, "IN");
		@SuppressWarnings("rawtypes")
		List resultList = q.getResultList();
		return resultList;
	}
	
	public Query createQuery(String query, Map<String, Object> parameters) {
		Query entityQuery = entityManager.createQuery(query);
		this.setQueryParameters(entityQuery, parameters);
		return entityQuery;
	}

	@SuppressWarnings({ "rawtypes" })
	public List executeQueryMultipleResult(String query,
			Map<String, Object> parameters) {
		Query entityQuery = createQuery(query, parameters);
		List resultList = entityQuery.getResultList();
		return resultList;
	}

	@Override
	public NegocioCobPaqSeccion getLimitesSumaAseguradaPorCobertura(
			BigDecimal idToCobertura,String stateId,String cityId, BigDecimal idTcMoneda,Long idToNegPaqueteSeccion) {
		final String baseJpql = "select m from NegocioCobPaqSeccion m where " +
				"m.negocioPaqueteSeccion = :negocioPaqueteSeccion and m.coberturaDTO = :cobertura and m.monedaDTO = :moneda ";
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = entityManager.getReference(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);
		CoberturaDTO cobertura =  entityManager.getReference(CoberturaDTO.class, idToCobertura);
		MonedaDTO moneda = entityManager.getReference(MonedaDTO.class, idTcMoneda.shortValue());
		
		List<NegocioCobPaqSeccion> list = new ArrayList<NegocioCobPaqSeccion>();		
		if (stateId != null && cityId != null) {
			final String jpqlWithStateAndCityIncluded = baseJpql + "and m.estadoDTO = :estado and m.ciudadDTO = :ciudad";
			EstadoDTO estado = entityManager.getReference(EstadoDTO.class, stateId);
			CiudadDTO ciudad = entityManager.getReference(CiudadDTO.class, cityId);
			TypedQuery<NegocioCobPaqSeccion> query = entityManager.createQuery(jpqlWithStateAndCityIncluded, NegocioCobPaqSeccion.class);
			list = query.setParameter("negocioPaqueteSeccion", negocioPaqueteSeccion)
				.setParameter("cobertura", cobertura)
				.setParameter("moneda", moneda)
				.setParameter("estado", estado)
				.setParameter("ciudad", ciudad)
				.setHint(QueryHints.BATCH, "model.negocioDeducibleCobList")
				.setHint(QueryHints.BATCH, "model.coberturaDTO")
				.setHint(QueryHints.BATCH_TYPE, "IN")
				.getResultList();
		}
		
		if (list.isEmpty()) {
			final String jpqlWithStateAndCityExcluded = baseJpql + "and m.estadoDTO = :estado and m.ciudadDTO is null";
			EstadoDTO estado = entityManager.getReference(EstadoDTO.class, stateId);
			TypedQuery<NegocioCobPaqSeccion> query = entityManager.createQuery(jpqlWithStateAndCityExcluded, NegocioCobPaqSeccion.class);
			list = query.setParameter("negocioPaqueteSeccion", negocioPaqueteSeccion)
				.setParameter("cobertura", cobertura)
				.setParameter("moneda", moneda)
				.setParameter("estado", estado)
				.setHint(QueryHints.BATCH, "model.negocioDeducibleCobList")
				.setHint(QueryHints.BATCH, "model.coberturaDTO")
				.setHint(QueryHints.BATCH_TYPE, "IN")
				.getResultList();
		}
		
		//Could be that state and city was not passed to the method (null) or no NegocioCobPaqSeccion couldn't be found using that combination.
		//We will try finding the NegocioCobPaqSeccion now excluding the city and state.
		if (list.isEmpty()) {
			final String jpqlWithStateAndCityExcluded = baseJpql + "and m.estadoDTO is null and m.ciudadDTO is null";
			TypedQuery<NegocioCobPaqSeccion> query = entityManager.createQuery(jpqlWithStateAndCityExcluded, NegocioCobPaqSeccion.class);
			list = query.setParameter("negocioPaqueteSeccion", negocioPaqueteSeccion)
				.setParameter("cobertura", cobertura)
				.setParameter("moneda", moneda)
				.setHint(QueryHints.BATCH, "model.negocioDeducibleCobList")
				.setHint(QueryHints.BATCH, "model.coberturaDTO")
				.setHint(QueryHints.BATCH_TYPE, "IN")
				.getResultList();
		}
		
		//Still empty just return null.
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
	

	public NegocioCobPaqSeccion executeNativeQuerySimpleResult(String query) {
		try{
			Query nativeQuery = entityManager.createNativeQuery(query,NegocioCobPaqSeccion.class);
			return (NegocioCobPaqSeccion)nativeQuery.getSingleResult();
		}
		catch(NoResultException e){
			return null;
		}
	}
}
