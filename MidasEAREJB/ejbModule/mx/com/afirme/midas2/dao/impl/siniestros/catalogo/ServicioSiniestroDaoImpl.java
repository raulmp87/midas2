package mx.com.afirme.midas2.dao.impl.siniestros.catalogo;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.catalogo.ServicioSiniestroDao;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService.ServicioSiniestroFiltro;
import mx.com.afirme.midas2.util.JpaUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ServicioSiniestroDaoImpl extends JpaDao<Long, ServicioSiniestro>  implements ServicioSiniestroDao {

	public ServicioSiniestro getServicioSiniestroById(Long id) {
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro( ");
		queryString.append("model.id, model.prestadorServicio, model.oficina, model.servicioSiniestroId, model.ambitoPrestadorServicio, model.estatus, ");
		queryString.append("model.certificacion, model.fechaCambioEstatus, model.fechaCreacion, model.fechaModificacion, model.codigoUsuarioCreacion, model.codigoUsuarioModificacion, model.personaMidas, model.idSeycos ");		
		queryString.append(") FROM ServicioSiniestro model ");
		queryString.append("JOIN model.personaMidas.personaDireccion persDir ");
		queryString.append("WHERE model.id = :idSiniestro ");
		TypedQuery<ServicioSiniestro> query = entityManager.createQuery(queryString.toString(), ServicioSiniestro.class);
		query.setParameter("idSiniestro", id);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);	
		if(query.getResultList().size() > 0)
			return query.getResultList().get(0);
		else
			return null;
	}
	
	public List<ServicioSiniestro> buscar(ServicioSiniestroFiltro filtro) {
		Map<String, Object> parametros = new HashMap<String, Object>();		
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro( ");
		queryString.append("model.id, model.prestadorServicio, model.oficina, model.servicioSiniestroId, model.ambitoPrestadorServicio, model.estatus, ");
		queryString.append("model.certificacion, model.fechaCambioEstatus, model.fechaCreacion, model.fechaModificacion, model.codigoUsuarioCreacion, model.codigoUsuarioModificacion, model.personaMidas, model.idSeycos ");		
		queryString.append(") FROM ServicioSiniestro model ");
		
		if(filtro != null){						
			if(!StringUtil.isEmpty(filtro.getEstado()) || !StringUtil.isEmpty(filtro.getMunicipio())){
				queryString.append("JOIN model.personaMidas.personaDireccion persDir ");									
			}			
			if(!StringUtil.isEmpty(filtro.getEstado())){
				JpaUtil.addParameter(queryString, "persDir.direccion.colonia.ciudad.estado.id", filtro.getEstado(), "estado", parametros);
			}
			if(!StringUtil.isEmpty(filtro.getMunicipio())){
				JpaUtil.addParameter(queryString, "persDir.direccion.colonia.ciudad.id", filtro.getMunicipio(), "municipio", parametros);
			}			
			JpaUtil.addFilterParameter("model", queryString, filtro, parametros);			
		}
		queryString.append(" ORDER BY model.personaMidas.nombre");
		TypedQuery<ServicioSiniestro> query = entityManager.createQuery(queryString.toString(), ServicioSiniestro.class);
		setQueryParameters(query, parametros);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);		
		return query.getResultList();
	}
}
