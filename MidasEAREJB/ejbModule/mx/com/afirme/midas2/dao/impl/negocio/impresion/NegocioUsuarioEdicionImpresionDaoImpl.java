package mx.com.afirme.midas2.dao.impl.negocio.impresion;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.negocio.impresion.NegocioUsuarioEdicionImpresionDao;
import mx.com.afirme.midas2.domain.negocio.impresion.NegocioUsuarioEdicionImpresion;

@Stateless
public class NegocioUsuarioEdicionImpresionDaoImpl implements
		NegocioUsuarioEdicionImpresionDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<NegocioUsuarioEdicionImpresion> obtenerUsuariosDisponibles(
			Long idToNegocio) {
		List<NegocioUsuarioEdicionImpresion> usuariosDisponibles = null;
		
		String spName = "MIDAS.PKGAUT_NEGOCIOS.usuariosDisponiblesEdImpresion";
		StoredProcedureHelper storedHelper  = null;
		try {
			String propiedades = "claveUsuario,nombreUsuario";

			String columnasBaseDatos = "claveUsuario,nombreUsuario";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(NegocioUsuarioEdicionImpresion.class.getCanonicalName(), propiedades, columnasBaseDatos);
			storedHelper.estableceParametro("p_idToNegocio",  idToNegocio );
			usuariosDisponibles = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"NegocioUsuarioEdicionImpresionDao: Excepcion general en busqueda de Usuarios De Edicion De Impresion Disponibles ..."+descErr
						, Level.WARNING, e);    
			}
		}
		
		return usuariosDisponibles;
	}

}
