/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoProvisionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoProvision;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoProvisionDaoImpl implements CaTipoProvisionDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoProvisionDaoImpl.class);
	
	public void save(CaTipoProvision entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaTipoProvision entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoProvision.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }

    public CaTipoProvision update(CaTipoProvision entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoProvision result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoProvision 	::		CaTipoProvisionDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoProvision findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoProvisionDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoProvision instance = entityManager.find(CaTipoProvision.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoProvisionDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoProvisionDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoProvision> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoProvisionDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoProvision model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoProvisionDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoProvisionDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoProvision> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoProvision> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoProvision> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoProvision> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoProvision> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoProvisionDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoProvision model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoProvisionDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoProvisionDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
