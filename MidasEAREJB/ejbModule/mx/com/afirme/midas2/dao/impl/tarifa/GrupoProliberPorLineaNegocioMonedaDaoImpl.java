package mx.com.afirme.midas2.dao.impl.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.tarifa.GrupoProliberPorLineaNegocioMonedaDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoId;
import mx.com.afirme.midas2.dto.GrupoProliberComboDTO;

@Stateless
public class GrupoProliberPorLineaNegocioMonedaDaoImpl implements GrupoProliberPorLineaNegocioMonedaDao{

	@Override
	public List<GrupoProliberComboDTO> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public List<GrupoProliberComboDTO> listRelated(Object id) {
		String spName="MIDAS.pkgAUT_Servicios.spAUT_GrupoProliberPorLNMon";
		String [] atributosDTO = { "claveGrupoProliber" };
		String [] columnasCursor = { "CLAVEGRUPOPROLIBER"};
		try {
			
			TarifaAutoId tarifaAutoId = (TarifaAutoId)id;
			
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.GrupoProliberComboDTO", atributosDTO, columnasCursor);
			
			storedHelper.estableceParametro("pIdMoneda", tarifaAutoId.getIdMoneda());
			
			return storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName,e);
		}
	}

	@Override
	public GrupoProliberComboDTO findById(BigDecimal id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GrupoProliberComboDTO findById(CatalogoValorFijoId id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public GrupoProliberComboDTO findById(double id) {
		// TODO Auto-generated method stub
		return null;
	}

}
