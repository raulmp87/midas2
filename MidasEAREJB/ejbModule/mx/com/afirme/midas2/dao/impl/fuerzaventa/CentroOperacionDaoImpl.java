package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.validation.ConstraintViolationException;
import javax.validation.ValidationException;

import mx.com.afirme.midas2.dao.fuerzaventa.CentroOperacionDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.dao.impl.domicilio.DomicilioUtils;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote.TipoDomicilio;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote.TipoAccionFuerzaVenta;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.commons.beanutils.BeanUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;
@Stateless
public class CentroOperacionDaoImpl extends EntidadHistoricoDaoImpl implements CentroOperacionDao{
	private DomicilioFacadeRemote domicilioFacade;
	private ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade;
	@EJB
	private EntidadService entidadService;
	@Override
	public CentroOperacion saveFull(CentroOperacion centroOperacion) throws ValidationException,ConstraintViolationException, Exception {
		if(centroOperacion==null){
			throw new Exception("CentroOperacion is null");
		}
		validaRelacion(centroOperacion);
		boolean esUpdate=false;
		Domicilio domicilio=centroOperacion.getDomicilio();
		
		if(domicilio==null){
			domicilio=Domicilio.getDummy();
		}
		DomicilioPk pkDomicilio=(domicilio!=null)?domicilio.getIdDomicilio():null;
		Long idDomicilio=(pkDomicilio!=null)?pkDomicilio.getIdDomicilio():null;
		//Si no existe el domicilio entonces se inserta
		//if(idDomicilio==null){
			Long idPersona=(centroOperacion.getPersonaResponsable()!=null && centroOperacion.getPersonaResponsable().getIdPersona()!=null)?centroOperacion.getPersonaResponsable().getIdPersona():null;
			domicilio.setIdPersona(idPersona);
			TipoDomicilio tipoDomicilio = domicilioFacade.getTipoDomicilioPorClave("CE_OP");
			domicilio.setTipoDomicilio(tipoDomicilio.getValue());
			idDomicilio=domicilioFacade.save(domicilio, null);
		//}
		DomicilioPk pk=new DomicilioPk();
		pk.setIdDomicilio(idDomicilio);
		pk.setIdPersona(idPersona);
		Domicilio persistAddress=entityManager.getReference(Domicilio.class, pk);
		if(idPersona != null)
		{
			Persona contratante = this.findById(Persona.class, idPersona);
			centroOperacion.setPersonaResponsable(contratante);			
		}
		
		centroOperacion.setDomicilio(persistAddress);
		if(centroOperacion.getId()!=null){
			update(centroOperacion);			
			esUpdate=true;
		}else{
			persist(centroOperacion);
		}
		
		if(!esUpdate){//le setea la clave igual al id 
			centroOperacion.setIdCentroOperacion(centroOperacion.getId());
			update(centroOperacion);
		}
		replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(centroOperacion,TipoAccionFuerzaVenta.GUARDAR);//Indica que va a replicar pero va a guardar en seycos
		return centroOperacion;
	}
	
	@Override
	public void unsubscribe(CentroOperacion centroOperacion) throws Exception {
		if(centroOperacion==null || centroOperacion.getId()==null){
			throw new Exception("Centro Operacion is null");
		}
			centroOperacion.setClaveEstatus(0l);
			validaRelacion(centroOperacion);
			update(centroOperacion);//Se actualiza su estatus
			replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(centroOperacion, TipoAccionFuerzaVenta.INACTIVAR);			
	}
	/***Sets && gets***********************************************************/
	@EJB
	public void setDomicilioFacade(DomicilioFacadeRemote domicilioFacade) {
		this.domicilioFacade = domicilioFacade;
	}
	@EJB
	public void setReplicarFuerzaVentaSeycosFacade(
			ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade) {
		this.replicarFuerzaVentaSeycosFacade = replicarFuerzaVentaSeycosFacade;
	}
	
	
	/**
	 * Obtiene la lista de los centros de operacion
	 * @param onlyActive indica si se va a traer unicamente los centros de operacion activos o sino TODOS. 
	 * @return
	 */
	public List<CentroOperacionView> getList(boolean onlyActive){
		List<CentroOperacionView> lista=new ArrayList<CentroOperacionView>();
		//Si trae datos de fecha entonces busca por medio de sql, sino con jpql		
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		CentroOperacion filtroCentroOperacion=new CentroOperacion();
		if(onlyActive){
			filtroCentroOperacion.setClaveEstatus(1L);
		}
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT DISTINCT entidad.id as id, entidad.idCentroOperacion as idCentroOperacion, entidad.descripcion as descripcion ");
		queryString.append(" from MIDAS.toCentroOperacion entidad "); 
		queryString.append(" where ");
		if(isNotNull(filtroCentroOperacion)){
			int index=1;
			if(filtroCentroOperacion.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtroCentroOperacion.getClaveEstatus());
				index++;
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.descripcion asc ";
			Query query=entityManager.createNativeQuery(finalQuery,CentroOperacionView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			lista=query.getResultList();
		}
		return lista;
	}

	@Override
	public List<CentroOperacionView> findByFiltersView(CentroOperacion filtroCentroOperacion){
		List<CentroOperacionView> lista=new ArrayList<CentroOperacionView>();
		//Si trae datos de fecha entonces busca por medio de sql, sino con jpql		
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Domicilio domicilio=(isNotNull(filtroCentroOperacion) && isNotNull(filtroCentroOperacion.getDomicilio()))?filtroCentroOperacion.getDomicilio():null;
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT DISTINCT entidad.id as id, entidad.idCentroOperacion as idCentroOperacion, entidad.descripcion as descripcion,entidad.claveestatus as claveestatus,persona.nombreCompleto as nombreCompleto,entidad.idPersonaResponsable as idPersonaResponsable ");
		queryString.append(" from MIDAS.toCentroOperacion entidad "); 
		queryString.append(" inner join MIDAS.VW_PERSONA persona on(persona.idpersona=entidad.idPersonaResponsable) "); 
		queryString.append(" inner join MIDAS.TCGRUPOACTUALIZACIONAGENTES grupo on(grupo.descripcion='Centro de Operacion') ");
		queryString.append(" left join MIDAS.TLACTUALIZACIONAGENTES act on(act.grupoActualizacionAgentes_id=grupo.id and act.idRegistro=entidad.id and (act.tipoMovimiento is null or act.tipoMovimiento like 'ALTA' )) ");
		boolean domicilioEmpty=DomicilioUtils.isEmptyAddress(domicilio);
		if(!domicilioEmpty){
			queryString.append(" LEFT JOIN MIDAS.VW_DOMICILIO dom ON (dom.idDomicilio=entidad.idDomicilio) ");
		}
		queryString.append(" where ");
		if(isNotNull(filtroCentroOperacion)){
			int index=1;
			if(isNotNull(filtroCentroOperacion.getId())){
				addCondition(queryString, " entidad.id=? ");
				params.put(index, filtroCentroOperacion.getId());
				index++;
			}else if(isNotNull(filtroCentroOperacion.getIdCentroOperacion())){
				addCondition(queryString, " entidad.idCentroOperacion=? ");
				params.put(index, filtroCentroOperacion.getIdCentroOperacion());
				index++;
			}else{
				if(filtroCentroOperacion.getClaveEstatus()!=null){
					addCondition(queryString, " entidad.claveEstatus=? ");
					params.put(index, filtroCentroOperacion.getClaveEstatus());
					index++;
				}
				if(filtroCentroOperacion.getDescripcion()!=null && !filtroCentroOperacion.getDescripcion().isEmpty()){
					addCondition(queryString, "UPPER(entidad.descripcion) like UPPER(?)");
					params.put(index, "%"+filtroCentroOperacion.getDescripcion()+"%");
					index++;
				}
				
				Persona personaResponsable=filtroCentroOperacion.getPersonaResponsable();
				if(personaResponsable!=null){
					if(personaResponsable.getNombreCompleto()!=null && !personaResponsable.getNombreCompleto().isEmpty()){
						addCondition(queryString, "UPPER(persona.nombreCompleto) like UPPER(?)");
						params.put(index, "%"+personaResponsable.getNombreCompleto()+"%");
					}
				}
				
				if(!domicilioEmpty){
					if(isValid(domicilio.getClaveEstado())){
						addCondition(queryString, " dom.claveEstado like UPPER(?)");
						params.put(index,  domicilio.getClaveEstado());
						index++;
					}
					if(isValid(domicilio.getClaveCiudad())){
						addCondition(queryString, " dom.claveCiudad like UPPER(?)");
						params.put(index,  domicilio.getClaveCiudad());
						index++;
					}
					if(isValid(domicilio.getCodigoPostal())){
						addCondition(queryString, " TRIM(dom.codigoPostal) like TRIM(UPPER(?))");
						params.put(index,  domicilio.getCodigoPostal());
						index++;
					}
					if(isValid(domicilio.getNombreColonia())){
						addCondition(queryString, " TRIM(dom.colonia) like TRIM(UPPER(?))||'%'");
						params.put(index,  domicilio.getNombreColonia());
						index++;
					}
				}
				if(isNotNull(filtroCentroOperacion.getFechaInicio())){
					Date fecha=filtroCentroOperacion.getFechaInicio();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");						
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroCentroOperacion.getFechaFin())){
					Date fecha=filtroCentroOperacion.getFechaFin();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");						
					params.put(index,  fechaString);
					index++;
				}
			}
			if(params.isEmpty()){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.id desc";
			Query query=entityManager.createNativeQuery(finalQuery,CentroOperacionView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			
			query.setMaxResults(100);
			lista=query.getResultList();
		}
		return lista;
	}
	
	@Override
	public List<CentroOperacion> findByFilters(CentroOperacion filtroCentroOperacion){		
		
		return this.findByFilters(filtroCentroOperacion,null);
	}
	
	@SuppressWarnings("unchecked")
	public List<CentroOperacion> findByFilters(CentroOperacion filtroCentroOperacion, String fechaHistorico){
		
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;
		
		List<CentroOperacion> lista=new ArrayList<CentroOperacion>();
		final StringBuilder queryString=new StringBuilder("");
		
		String entityDomicilio = "domicilio";
		
		if(isHistorico)
		{
			entityDomicilio = "domicilioHistorico";	
		}
					
		queryString.append("select model from CentroOperacion model left join fetch model." + entityDomicilio + " left join fetch model.personaResponsable ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtroCentroOperacion!=null){
			if(filtroCentroOperacion.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtroCentroOperacion.getId());
			}else{
				if(filtroCentroOperacion.getClaveEstatus()!=null){
					addCondition(queryString, "model.claveEstatus=:claveEstatus");
					params.put("claveEstatus", filtroCentroOperacion.getClaveEstatus());
				}
				if(filtroCentroOperacion.getIdCentroOperacion()!=null){
					addCondition(queryString, "model.idCentroOperacion=:idCentroOperacion");
					params.put("idCentroOperacion", filtroCentroOperacion.getIdCentroOperacion());
				}
				if(filtroCentroOperacion.getDescripcion()!=null && !filtroCentroOperacion.getDescripcion().isEmpty()){
					addCondition(queryString, "UPPER(model.descripcion) like UPPER(:descripcion)");
					params.put("descripcion", "%"+filtroCentroOperacion.getDescripcion()+"%");
				}
				Domicilio domicilio=filtroCentroOperacion.getDomicilio();
				if(domicilio!=null){
					if(domicilio.getClaveEstado()!=null && !domicilio.getClaveEstado().isEmpty()){
						addCondition(queryString, "model.domicilio.claveEstado=:claveEstado");
						params.put("claveEstado", domicilio.getClaveEstado());
					}
					if(domicilio.getClaveCiudad()!=null && !domicilio.getClaveCiudad().isEmpty()){
						addCondition(queryString, "model.domicilio.claveCiudad=:claveCiudad");
						params.put("claveCiudad", domicilio.getClaveCiudad());
					}
					if(domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().isEmpty()){
						addCondition(queryString, "model.domicilio.nombreColonia like :nombreColonia");
						params.put("nombreColonia", domicilio.getNombreColonia()+"%");
					}
					if(domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().isEmpty()){
						addCondition(queryString, "model.domicilio.codigoPostal like :codigoPostal");
						params.put("codigoPostal", domicilio.getCodigoPostal()+"%");
					}
				}
				Persona personaResponsable=filtroCentroOperacion.getPersonaResponsable();
				if(personaResponsable!=null){
					if(personaResponsable.getNombreCompleto()!=null && !personaResponsable.getNombreCompleto().isEmpty()){
						addCondition(queryString, "UPPER(model.personaResponsable.nombreCompleto) like UPPER(:nombreResponsable)");
						params.put("nombreResponsable", "%"+personaResponsable.getNombreCompleto()+"%");
					}
				}
			}
		}
		
		if(!isHistorico)
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			lista=query.getResultList();			
		}
		else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
			
			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString).toString());
			historicalQuery.addArgument("id");
			historicalQuery.addArgumentValue(filtroCentroOperacion.getId());
			historicalQuery.refreshIdentityMapResult();
			lista = (List<CentroOperacion>)historicalSession.executeQuery(historicalQuery);			
		}	
		
		return lista;
	}
	
	@Override
	public CentroOperacion loadById(CentroOperacion entidad,
			String fechaHistorico){

		if (entidad != null && entidad.getId() != null) {
			List<CentroOperacion> list = this.findByFilters(entidad,
					fechaHistorico);
			if (list != null && !list.isEmpty()) {
				entidad = list.get(0);
			}
		}

		if (fechaHistorico != null && !fechaHistorico.isEmpty()) {
			Domicilio domicilioTemp = new Domicilio();
			try {
				BeanUtils.copyProperties(domicilioTemp,entidad.getDomicilioHistorico());
			} catch (Exception e) {				
				e.printStackTrace();
			}
			
			entidad.setDomicilio(domicilioTemp);
			return entidad;
		} else {
			
			return entityManager.getReference(CentroOperacion.class,
					entidad.getId());
		}
	}
	
	private void validaRelacion(CentroOperacion centroOperacion) throws Exception {
		if (centroOperacion.getClaveEstatus().equals(0l)) {
			
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("centroOperacion.id",centroOperacion.getId());
			parametros.put("claveEstatus", 1);	
			List<Gerencia> checaRelcionCO = entidadService.findByProperties(Gerencia.class, parametros);
			if(checaRelcionCO != null && !checaRelcionCO.isEmpty()){
				throw new Exception("El Centro de Operacion est\u00E1 relacionado con una Gerencia");
			}
		}
	}
}
