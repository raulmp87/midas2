package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.cobertura;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cobertura.CoberturaDao;

@Stateless
public class CoberturaDaoImpl implements CoberturaDao {
	@PersistenceContext
	protected EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CoberturaCotizacionDTO> listarCoberturasSeccionCotizacion(
			SeccionCotizacionDTOId id, boolean soloContratadas) {
		if (id == null || id.getIdToCotizacion() == null
				|| id.getNumeroInciso() == null || id.getIdToSeccion() == null)
			return null;

		String queryString = "select model from CoberturaCotizacionDTO model where";
		queryString += " model.id.idToCotizacion =:idToCotizacion ";
		queryString += " and model.id.idToSeccion =:idToSeccion ";
		queryString += " and model.id.numeroInciso =:numeroInciso ";
		queryString += " and model.seccionCotizacionDTO.claveContrato = 1 ";
		if(soloContratadas)
			queryString += " and model.claveContrato = 1 ";
				
		queryString += "order by model.coberturaSeccionDTO.coberturaDTO.codigo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCotizacion", id.getIdToCotizacion());
		query.setParameter("numeroInciso", id.getNumeroInciso());
		query.setParameter("idToSeccion", id.getIdToSeccion());
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<CoberturaCotizacionDTO> listarCoberturasCotizacion(
			BigDecimal idToCotizacion, boolean soloContratadas) {
		String queryString = "select model from CoberturaCotizacionDTO model where";
		queryString += " model.id.idToCotizacion =:idToCotizacion ";	
		if(soloContratadas){
			queryString += " and model.claveContrato = 1 ";
		}	
		queryString += "order by model.coberturaSeccionDTO.coberturaDTO.codigo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCotizacion", idToCotizacion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CoberturaCotizacionDTO> listarCoberturasCotizacionDistinct(
			BigDecimal idToCotizacion, boolean soloContratadas) {
		String queryString = "select distinct model from CoberturaCotizacionDTO model where";
		queryString += " model.id.idToCotizacion =:idToCotizacion ";	
		if(soloContratadas){
			queryString += " and model.claveContrato = 1 ";
		}	
//		queryString += "order by model.coberturaSeccionDTO.coberturaDTO.codigo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCotizacion", idToCotizacion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CoberturaCotizacionDTO> listarCoberturasSeccionCotizacionPrimaCero(
			BigDecimal idToCotizacion, boolean soloContratadas) {
		if (idToCotizacion == null)
			return null;

		String queryString = "select model from CoberturaCotizacionDTO model where";
		queryString += " model.id.idToCotizacion =:idToCotizacion ";
		queryString += " and model.seccionCotizacionDTO.claveContrato = 1 ";
		queryString += " and model.valorPrimaNeta = 0  ";
		if(soloContratadas)
			queryString += " and model.claveContrato = 1 ";
				
		queryString += "order by model.id.numeroInciso, model.coberturaSeccionDTO.coberturaDTO.codigo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToCotizacion", idToCotizacion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

}
