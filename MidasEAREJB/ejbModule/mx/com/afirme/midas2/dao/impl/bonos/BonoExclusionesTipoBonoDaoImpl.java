package mx.com.afirme.midas2.dao.impl.bonos;

import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.bonos.BonoExclusionesTipoBonoDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionesTipoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.util.MidasException;



@Stateless
public class BonoExclusionesTipoBonoDaoImpl extends EntidadDaoImpl implements BonoExclusionesTipoBonoDao {
	
	@Override
	public List<BonoExclusionesTipoBono> loadByConfiguration(Long idBono) throws MidasException {
		if(isNull(idBono)){
			onError("El id de la configuracion no puede ser nulo");
		}
		
		BonoExclusionesTipoBono bono = new BonoExclusionesTipoBono();
		ConfigBonos configbono= new ConfigBonos();
		configbono.setId(idBono);
		bono.setIdConfig(configbono);
		List<BonoExclusionesTipoBono> lista = new ArrayList<BonoExclusionesTipoBono>();
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select model from BonoExclusionesTipoBono model ");
		addCondition(queryString, " model.idConfig=:idConfig");
		
		Query query = entityManager.createQuery(getQueryString(queryString));
		query.setParameter("idConfig",configbono);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		lista=query.getResultList();
		return lista;
	}

	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}	

}
