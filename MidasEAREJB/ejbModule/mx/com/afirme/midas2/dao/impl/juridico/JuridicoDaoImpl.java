package mx.com.afirme.midas2.dao.impl.juridico;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.juridico.JuridicoDao;

@Stateless
public class JuridicoDaoImpl extends EntidadDaoImpl implements JuridicoDao{

	@Override
	public Integer obtenerSecuenciaOficioJuridico(Long idReclamacion){
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT MAX(reclamacionOficio.id.oficio)	");
		queryString.append(" FROM ReclamacionOficioJuridico  reclamacionOficio");
		queryString.append(" WHERE reclamacionOficio.id.reclamacionJuridicoId = :idReclamacion");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idReclamacion"     , idReclamacion);
		Object obj = query.getSingleResult();
		Integer secuencia =  ((obj==null)?1:((Integer)obj)+1);
		return secuencia;
	}
	
}
