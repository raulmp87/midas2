/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaLineaVentaDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaLineaVenta;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;



@Stateless
public class CaBancaLineaVentaDaoImpl implements CaBancaLineaVentaDao {
	
	public static final String ID = "id";
	public static final String CABANCACONFIGURACION_ID = "configuracionBancaId";	
	public static final String RAMO_ID = "ramoId";
	
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = LoggerFactory.getLogger(CaBancaLineaVentaDaoImpl.class);
	
	public void save(CaBancaLineaVenta entity) {	
		try {
			LOG.info(">> save()");
			entityManager.persist(entity);
			LOG.info("<< save()");
        } catch (RuntimeException re) {     
        	LOG.error("Información del Error", re);
            throw re;
        }
	}
	
    public void delete(CaBancaLineaVenta entity) {
    	try {
    		LOG.info(">> delete()");
    		entity = entityManager.getReference(CaBancaLineaVenta.class, entity.getId());
    		LOG.info("<< delete()");
		    entityManager.remove(entity);
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
		    throw re;
		}
    }
    
    public CaBancaLineaVenta update(CaBancaLineaVenta entity) {    
    	try {
    		LOG.info(">> update()");
            CaBancaLineaVenta result = entityManager.merge(entity);
            LOG.info("<< update()");
            return result;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        	throw re;
        }
    }
    
    public CaBancaLineaVenta findById( Long id) {
    	CaBancaLineaVenta instance = null;
    	try {
    		LOG.info(">> findById()");
    		instance = entityManager.find(CaBancaLineaVenta.class, id);
    		LOG.info("<< findById()");
    	} catch (RuntimeException re) {    		
    		LOG.error("Información del Error", re);
    	}
    	return instance;
    }

    @SuppressWarnings("unchecked")
    public List<CaBancaLineaVenta> findByProperty(String propertyName, final Object value) {
    	LOG.info(">> findByProperty()");
		try {
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaBancaLineaVenta model ");			
			queryString.append("  WHERE model.").append(propertyName).append(" = :propertyValue");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("propertyValue", value);		
			LOG.info("<< findByProperty()");
			return query.getResultList();
		} catch (RuntimeException re) {		
			LOG.error("Información del Error", re);
			return null;
		}
	}			

	@SuppressWarnings("unchecked")
	public List<CaBancaLineaVenta> findAll() {		
		LOG.info(">> findAll()");		
		List<CaBancaLineaVenta> list = null;
		try {			
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaBancaLineaVenta model ");	
			Query query = entityManager.createQuery(queryString.toString(), CaBancaLineaVenta.class);			 
			list = query.getResultList();						
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			list = new ArrayList<CaBancaLineaVenta>();
		}
		LOG.info("<< findAll()");
		return list;
	}
}