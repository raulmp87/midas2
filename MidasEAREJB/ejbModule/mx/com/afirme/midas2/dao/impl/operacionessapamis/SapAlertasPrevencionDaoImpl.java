package mx.com.afirme.midas2.dao.impl.operacionessapamis;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasPrevencionDao;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasPrevencion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class SapAlertasPrevencionDaoImpl implements SapAlertasPrevencionDao {	
	private EntidadService entidadService;
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void guardarAlertasPrevencion(List<SapAlertasPrevencion> alertasPrevencion) {
		entidadService.saveAll(alertasPrevencion);
	}
}
