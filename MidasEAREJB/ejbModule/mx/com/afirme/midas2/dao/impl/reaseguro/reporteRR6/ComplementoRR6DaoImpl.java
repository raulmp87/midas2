package mx.com.afirme.midas2.dao.impl.reaseguro.reporteRR6;

import java.math.BigDecimal;
import java.sql.Date;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.danios.reportes.reporterr6.CargaRR6DTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.reaseguro.reporteRR6.ComplementoRR6Dao;

@Stateless
public class ComplementoRR6DaoImpl extends EntidadDaoImpl implements ComplementoRR6Dao{
	
	/**
	 * Carga archivos RTRE.
	 * @param clave
	 * @param negCubiertos
	 * @param claveEstrategia
	 * @param ordenCobertura
	 * @param identificador
	 * @param fCorte
	 * @param negocio
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaReporteRTRE(String clave,String negCubiertos,Integer claveEstrategia,Integer ordenCobertura,String identificador, 
			Date fCorte,String negocio){
	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRE_CARGA", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("clave", clave);
			storedHelper.estableceParametro("negCubiertos", negCubiertos);
			storedHelper.estableceParametro("claveEstrategia", claveEstrategia);
			storedHelper.estableceParametro("ordenCobertura", ordenCobertura);
			storedHelper.estableceParametro("identificador", identificador);
			storedHelper.estableceParametro("fCorte", fCorte);
			storedHelper.estableceParametro("negocio", negocio);
			
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_RR6_RTRE_CARGA ", Level.SEVERE, e);
		}

	}
	
	/**
	 * Carga archivos RTRC.
	 * @param identificador
	 * @param clave
	 * @param negCubiertos
	 * @param moneda
	 * @param captura
	 * @param modificados
	 * @param fechainicial
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaReporteRTRC(String identificador,String clave,String negCubiertos,String moneda,String captura,String modificados,String fechainicial,
			String fechafinal,Integer tipocontrato,String capas,BigDecimal porcionCedida,BigDecimal retencionPrioridad,String rentencionFianzas,
			BigDecimal capacidadMaxima,String capMaxFianzas,BigDecimal importe,String comision,String utilidades,String reasInscrito,String tipoReas,
			String reasNacional,String noInscrito,String participacion,String tipointermediario,String id_Intermediario,String intermNoAutorizado, 
			String suscriptor,String aclaracion,Date fCorte,String negocio,Integer orden){
	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
				
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRC_CARGA", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("identificador", identificador);
			storedHelper.estableceParametro("clave", clave);
			storedHelper.estableceParametro("negCubiertos", negCubiertos);
			storedHelper.estableceParametro("moneda", moneda);
			storedHelper.estableceParametro("captura", captura);
			storedHelper.estableceParametro("modificados", modificados);
			storedHelper.estableceParametro("fechainicial", fechainicial);
			storedHelper.estableceParametro("fechafinal", fechafinal);
			storedHelper.estableceParametro("tipocontrato", tipocontrato);
			storedHelper.estableceParametro("capas", capas);
			storedHelper.estableceParametro("porcionCedida", porcionCedida);
			storedHelper.estableceParametro("retencionPrioridad", retencionPrioridad);
			storedHelper.estableceParametro("rentencionFianzas", rentencionFianzas);
			storedHelper.estableceParametro("capacidadMaxima", capacidadMaxima);
			storedHelper.estableceParametro("capMaxFianzas", capMaxFianzas);
			storedHelper.estableceParametro("importe", importe);
			storedHelper.estableceParametro("comision", comision);
			storedHelper.estableceParametro("utilidades", utilidades);
			storedHelper.estableceParametro("reasInscrito", reasInscrito);
			storedHelper.estableceParametro("tipoReas", tipoReas);
			storedHelper.estableceParametro("reasNacional", reasNacional);
			storedHelper.estableceParametro("noInscrito", noInscrito);
			storedHelper.estableceParametro("participacion", participacion);
			storedHelper.estableceParametro("tipointermediario", tipointermediario);
			storedHelper.estableceParametro("id_Intermediario", id_Intermediario);
			storedHelper.estableceParametro("intermNoAutorizado", intermNoAutorizado);
			storedHelper.estableceParametro("suscriptor", suscriptor);
			storedHelper.estableceParametro("aclaracion", aclaracion);
			storedHelper.estableceParametro("fCorte", fCorte);
			storedHelper.estableceParametro("negocio", negocio);
			storedHelper.estableceParametro("orden", orden);
			
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_RR6_RTRC_CARGA ", Level.SEVERE, e);
		}

	}
	
	/**
	 * Carga archivos RTRF.
	 * @param identificador
	 * @param clave
	 * @param negCubiertos
	 * @param asegurado
	 * @param sumaAsegurada
	 * @param moneda
	 * @param primaEmitida
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaReporteRTRF(String identificador, String clave, String negCubiertos, String asegurado, String sumaAsegurada, Integer moneda, 
			BigDecimal primaEmitida, BigDecimal primaFacultada, BigDecimal primaCedProporc, BigDecimal primaRetenida, 
			String fechaInicial, String fechaFinal, String tipoContrato, String capas, BigDecimal retPrioridad, String retpriorfian, 
			BigDecimal capacidadMaxReas, String capacidadMaxFian, BigDecimal comision, String partUtilidades, String reasInscrito, 
			String tipoReas, String clvReasNac, String reasNoInscrito, BigDecimal participacionReas, String tipoIntermediario, 
			String clvIntermediario, String intNoAutorizado, String suscriptor, String aclaraciones, String entidades, String municipio, 
			String sector, Date fechaFinCorte, String negocio){
	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRF_CARGA", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("identificador", identificador);
			storedHelper.estableceParametro("clave", clave);
			storedHelper.estableceParametro("negCubiertos", negCubiertos);
			storedHelper.estableceParametro("asegurado", asegurado);
			storedHelper.estableceParametro("sumaAsegurada", sumaAsegurada);
			storedHelper.estableceParametro("moneda", moneda);
			storedHelper.estableceParametro("primaEmitida", primaEmitida);
			storedHelper.estableceParametro("primaFacultada", primaFacultada);
			storedHelper.estableceParametro("primaCedProporc", primaCedProporc);
			storedHelper.estableceParametro("primaRetenida", primaRetenida);
			storedHelper.estableceParametro("fechaInicial", fechaInicial);
			storedHelper.estableceParametro("fechaFinal", fechaFinal);
			storedHelper.estableceParametro("tipoContrato", tipoContrato);
			storedHelper.estableceParametro("capas", capas);
			storedHelper.estableceParametro("retPrioridad", retPrioridad);
			storedHelper.estableceParametro("retpriorfian", retpriorfian);
			storedHelper.estableceParametro("capacidadMaxReas", capacidadMaxReas);
			storedHelper.estableceParametro("capacidadMaxFian", capacidadMaxFian);
			storedHelper.estableceParametro("comision", comision);
			storedHelper.estableceParametro("partUtilidades", partUtilidades);
			storedHelper.estableceParametro("reasInscrito", reasInscrito);
			storedHelper.estableceParametro("tipoReas", tipoReas);
			storedHelper.estableceParametro("clvReasNac", clvReasNac);
			storedHelper.estableceParametro("reasNoInscrito", reasNoInscrito);
			storedHelper.estableceParametro("participacionReas", participacionReas);
			storedHelper.estableceParametro("tipoIntermediario", tipoIntermediario);
			storedHelper.estableceParametro("clvIntermediario", clvIntermediario);
			storedHelper.estableceParametro("intNoAutorizado", intNoAutorizado);
			storedHelper.estableceParametro("suscriptor", suscriptor);
			storedHelper.estableceParametro("aclaraciones", aclaraciones);
			storedHelper.estableceParametro("entidades", entidades);
			storedHelper.estableceParametro("municipio", municipio);
			storedHelper.estableceParametro("sector", sector);
			storedHelper.estableceParametro("fechaFinCorte", fechaFinCorte);
			storedHelper.estableceParametro("negocio", negocio);
			
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_RR6_RTRF_CARGA ", Level.SEVERE, e);
		}

	}
	
	/**
	 * Carga archivos RTRS.
	 * @param consecutivo
	 * @param siniestro
	 * @param claveNegocio
	 * @param siniestroReclamacion
	 * @param asegurado
	 * @param fechaReclamacion
	 * @param importeReclamacion
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaReporteRTRS(Integer consecutivo, Integer siniestro, String claveNegocio, String siniestroReclamacion, String asegurado, String fechaReclamacion, 
			BigDecimal importeReclamacion, BigDecimal importeRecSiniestro, String reasInscrito, String tipoReasNac, 
			String claveReasNac, String reasNoInscrito, BigDecimal importeProporcional, BigDecimal importeNoProporcional, BigDecimal importeFacultativo, 
			String entidades, String municipio, String sector, Date fechaCorte, String negocio){
	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRS_CARGA", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("consecutivo", consecutivo);
			storedHelper.estableceParametro("siniestro", siniestro);
			storedHelper.estableceParametro("idNegocio", claveNegocio);
			storedHelper.estableceParametro("sinReclamacion", siniestroReclamacion);
			storedHelper.estableceParametro("asegurado", asegurado);
			storedHelper.estableceParametro("fSinReclamacion", fechaReclamacion);
			storedHelper.estableceParametro("importeReclamacion", importeReclamacion);
			storedHelper.estableceParametro("importeRecuperado", importeRecSiniestro);
			storedHelper.estableceParametro("reasInscrito", reasInscrito);
			storedHelper.estableceParametro("tipoReasNacional", tipoReasNac);
			storedHelper.estableceParametro("claveReasNacional", claveReasNac);
			storedHelper.estableceParametro("reasNoInscrito", reasNoInscrito);
			storedHelper.estableceParametro("importePropor", importeProporcional);
			storedHelper.estableceParametro("importeNoPropor", importeNoProporcional);
			storedHelper.estableceParametro("importeFac", importeFacultativo);
			storedHelper.estableceParametro("entidades", entidades);
			storedHelper.estableceParametro("municipio", municipio);
			storedHelper.estableceParametro("sector", sector);
			storedHelper.estableceParametro("fCorte", fechaCorte);
			storedHelper.estableceParametro("negocio", negocio);
			
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_RR6_RTRS_CARGA ", Level.SEVERE, e);
		}

	}
	
	/**
	 * Carga archivos RTRR.
	 * @param identificador
	 * @param tipoContrato
	 * @param anio
	 * @param clave
	 * @param iniciovigencia
	 * @param comisiones
	 * @param utilidades
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaReporteRTRR(String identificador, Integer tipoContrato, String anio, String clave, String iniciovigencia, BigDecimal comisiones, 
			BigDecimal utilidades, BigDecimal sinReclamaFac, BigDecimal sinReclamaNoProp, BigDecimal ingresos, BigDecimal otrosIngresos, 
			BigDecimal montoPrima, BigDecimal costoCobertura, BigDecimal partSalvamento, BigDecimal recursosRetenidos, BigDecimal reasFinanciero, 
			BigDecimal castigo, BigDecimal egresos, BigDecimal gastosReas, String aclaraciones, Date fechaCorte, String negocio){
	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRR_CARGA", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("identificador", identificador);
			storedHelper.estableceParametro("tipoContrato", tipoContrato);
			storedHelper.estableceParametro("anio", anio);
			storedHelper.estableceParametro("clave", clave);
			storedHelper.estableceParametro("iniciovigencia", iniciovigencia);
			storedHelper.estableceParametro("comisiones", comisiones);
			storedHelper.estableceParametro("utilidades", utilidades);
			storedHelper.estableceParametro("sinReclamaFac", sinReclamaFac);
			storedHelper.estableceParametro("sinReclamaNoProp", sinReclamaNoProp);
			storedHelper.estableceParametro("ingresos", ingresos);
			storedHelper.estableceParametro("otrosIngresos", otrosIngresos);
			storedHelper.estableceParametro("montoPrima", montoPrima);
			storedHelper.estableceParametro("costoCobertura", costoCobertura);
			storedHelper.estableceParametro("partSalvamento", partSalvamento);
			storedHelper.estableceParametro("recursosRetenidos", recursosRetenidos);
			storedHelper.estableceParametro("reasFinanciero", reasFinanciero);
			storedHelper.estableceParametro("castigo", castigo);
			storedHelper.estableceParametro("egresos", egresos);
			storedHelper.estableceParametro("gastosReas", gastosReas);
			storedHelper.estableceParametro("aclaraciones", aclaraciones);
			storedHelper.estableceParametro("fechaCorte", fechaCorte);
			storedHelper.estableceParametro("negocio", negocio);
			
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_RR6_RTRR_CARGA ", Level.SEVERE, e);
		}

	}
	
	/**
	 * Carga archivos RARN.
	 * @param mes
	 * @param concepto
	 * @param ramo
	 * @param rgre
	 * @param tipo
	 * @param reasNacional
	 * @param noInscrito
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaReporteRARN(String mes, String concepto, String ramo, String rgre, String tipo, String reasNacional, 
			String noInscrito, BigDecimal primaCedida, BigDecimal coberturaXL, BigDecimal sumaAsegurada,
			BigDecimal sumaAseguradaRetenida, Date fechaIniCorte, Date fechaFinCorte, String negocio){
	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RARN_CARGA", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("mes", mes);
			storedHelper.estableceParametro("concepto", concepto);
			storedHelper.estableceParametro("ramo", ramo);
			storedHelper.estableceParametro("rgre", rgre);
			storedHelper.estableceParametro("tipo", tipo);
			storedHelper.estableceParametro("reasNacional", reasNacional);
			storedHelper.estableceParametro("noInscrito", noInscrito);
			storedHelper.estableceParametro("primaCedida", primaCedida);
			storedHelper.estableceParametro("coberturaXL", coberturaXL);
			storedHelper.estableceParametro("sumaAsegurada", sumaAsegurada);
			storedHelper.estableceParametro("sumaAseguradaRetenida", sumaAseguradaRetenida);
			storedHelper.estableceParametro("fechaIniCorte", fechaIniCorte);
			storedHelper.estableceParametro("fechaFinCorte", fechaFinCorte);
			storedHelper.estableceParametro("negocio", negocio);
			
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_RR6_RARN_CARGA ", Level.SEVERE, e);
		}

	}
	
	/**
	 * Carga archivos RTRI.
	 * @param idContrato
	 * @param corretajeContratos
	 * @param corretajeFacultativos
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void insertaReporteRTRI(String idcontrato, BigDecimal corretajeContratos, BigDecimal corretajeFacultativos,Date fechaFinCorte, String negocio){
	
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_SOLVENCIA_CNSF_RR6.SP_CNSF_RR6_RTRI_CARGA", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdContrato", idcontrato);
			storedHelper.estableceParametro("pCorretajeContratos", corretajeContratos);
			storedHelper.estableceParametro("pCorretajeFacultativos", corretajeFacultativos);
			storedHelper.estableceParametro("pFechaFinCorte", fechaFinCorte);
			storedHelper.estableceParametro("negocio", negocio);
			
			
			respuesta = storedHelper.ejecutaActualizar();
			
			if (respuesta != 0) {
				throw new Exception(storedHelper.getDescripcionRespuesta());
			}
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al ejecutar el SP_CNSF_RR6_RTRI_CARGA ", Level.SEVERE, e);
		}

	}
	
	/**
	 * Obtiene la cantidad de registros encontrados en las tablas de Cargas.
	 * @param fechaCorte
	 * @param tabla
	 * @param fecha
	 * @param negocio
	 * return Cantidad de registros
	 */
	public int getRegistros(java.util.Date fechaCorte, String tabla, String fecha, String negocio) {
		Integer registros = 0;
		try {			
			
			StringBuilder queryString=new StringBuilder("");
			queryString.append("SELECT  count(*) ");
			queryString.append("FROM "+tabla);
			queryString.append(" WHERE "+fecha+" = ?1");
			queryString.append(" AND negocio = ?2");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaCorte, TemporalType.DATE);
			query.setParameter(2, negocio);
			
			registros = ((BigDecimal)query.getSingleResult()).intValue();
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla "+tabla, Level.SEVERE, e);
		}
		return registros;

	}
	
	/**
	 * Elimina registros en la tabla de Carga.
	 * @param fechaCorte
	 * @param tabla
	 * @param fecha
	 * @param negocio
	 */
	public void borraRegistros(java.util.Date fechaCorte, String tabla, String fecha, String negocio){
		
		LogDeMidasEJB3.log(
				"Elimando registros de la tabla "+tabla+" :fecha "
						 + fechaCorte, Level.INFO, null);
		
		try {			
			
			StringBuilder queryString=new StringBuilder("");
			queryString.append("DELETE ");
			queryString.append("FROM "+tabla);
			queryString.append(" WHERE "+fecha+" = ?1");
			queryString.append(" AND negocio = ?2");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaCorte, TemporalType.DATE);
			query.setParameter(2, negocio);
			query.executeUpdate();
			entityManager.flush();
			
						
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla "+tabla, Level.SEVERE, e);
		}
	}
	
	/**
	 * Inserta registros en la tabla de EstatusCarga.
	 * @param cargaDTO
	 */
	public void insertaEstatusCarga(CargaRR6DTO cargaDTO){
		LogDeMidasEJB3.log(
				"Insertando instancia de cargaDTO.", Level.INFO, null);
		try {			
			
			entityManager.persist(cargaDTO);
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de guadar el dto "+cargaDTO.getReporte(), Level.SEVERE, e);
		}
		
	};
	

	/**
	 * Actualiza registros en la tabla de EstatusCarga.
	 * @param cargaDTO
	 */
	public void actualizaEstatusCarga(CargaRR6DTO cargaDTO){
		LogDeMidasEJB3.log(
				"Actualizando registros de la tabla MIDAS.CNSF_RR6_ESTATUS_CARGA :fecha "
						 + cargaDTO.getFechaFin(), Level.INFO, null);
		try {
			StringBuilder queryString=new StringBuilder("");
			queryString.append("UPDATE CargaRR6DTO model");
			queryString.append(" SET model.usuario = :usuario,");
			queryString.append(" model.fechaCarga = :fechaCarga,");
			queryString.append(" model.idArchivo = :idControlArchivo");
			queryString.append(" WHERE model.fechaFin = :fechaCorte");
			queryString.append(" AND model.reporte = :negocio");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("usuario", cargaDTO.getUsuario());
			query.setParameter("fechaCarga", cargaDTO.getFechaCarga(), TemporalType.TIMESTAMP);
			query.setParameter("fechaCorte", cargaDTO.getFechaFin(), TemporalType.DATE);
			query.setParameter("negocio", cargaDTO.getReporte());
			query.setParameter("idControlArchivo", cargaDTO.getIdArchivo());
			query.executeUpdate();
			entityManager.flush();
					
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de actualizar el estatus de la carga. ", Level.SEVERE, e);
		}
		
	};
	
	/**
	 * Obtiene el estatus de la carga.
	 * @param fechaCorte
	 * @param negocio
	 * @return estatus candado
	 */
	public int estatusCarga(java.util.Date fechaCorte, String negocio){
		Integer estatus = 0;
		try {			
			
			StringBuilder queryString=new StringBuilder("");
			queryString.append("SELECT  candado FROM MIDAS.CNSF_RR6_ESTATUS_CARGA");
			queryString.append(" WHERE fechafin = ?1");
			queryString.append(" AND reporte = ?2");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, fechaCorte, TemporalType.DATE);
			query.setParameter(2, negocio);
			
			estatus = ((BigDecimal)query.getSingleResult()).intValue();
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Falla al momento de consultar la tabla MIDAS.CNSF_RR6_ESTATUS_CARGA", Level.SEVERE, e);
		}
		return estatus;
	};
	
}
