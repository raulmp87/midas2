package mx.com.afirme.midas2.dao.impl.catalogos.condicionesespeciales;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.catalogos.condicionesespeciales.CondicionEspecialDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspInc;
import mx.com.afirme.midas2.domain.condicionesespeciales.AreaCondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CoberturaCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.EstatusCondicion;
import mx.com.afirme.midas2.domain.condicionesespeciales.VarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.VariableAjuste;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;
import mx.com.afirme.midas2.util.JpaUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.DateTime;

@Stateless
public class CondicionEspecialDaoImpl extends JpaDao<Long, CondicionEspecial>   implements  CondicionEspecialDao {

	@EJB
	private EntidadDao entidadDao;

	@Override
	@SuppressWarnings("unchecked")
	public List<CondicionEspecial> buscarCondicionFiltro(CondicionEspecialDTO condicionFiltro) {
		String modelName = "model.";
		Map<String, Object> parameters = new HashMap<String, Object>();	

		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + CondicionEspecial.class.getSimpleName() + " model " +
				"WHERE model.id is not null ");	
		if(condicionFiltro != null){
			JpaUtil.addParameter(queryString, modelName + "id",  condicionFiltro.getCondicion().getId(),  "id", parameters);	
			JpaUtil.addParameter(queryString, modelName + "nombre",  condicionFiltro.getCondicion().getNombre(),  "nombre", parameters);
			JpaUtil.addParameter(queryString, modelName + "codigo",  condicionFiltro.getCondicion().getCodigo(),  "codigo", parameters);
			JpaUtil.addParameter(queryString, modelName + "descripcion",  condicionFiltro.getCondicion().getDescripcion(),  "descripcion", parameters);	
			JpaUtil.addParameter(queryString, modelName + "estatus",  condicionFiltro.getCondicion().getEstatus(),  "estatus", parameters);		
			JpaUtil.addTruncParameter(queryString, modelName + "fechaAlta", condicionFiltro.getFechaIniAlta(), "fIniAlta", parameters, JpaUtil.GREATEROREQUAL);
			JpaUtil.addTruncParameter(queryString, modelName + "fechaAlta", condicionFiltro.getFechaFinAlta(), "fFinAlta", parameters, JpaUtil.LESSOREQUAL);
			JpaUtil.addTruncParameter(queryString, modelName + "fechaBaja", condicionFiltro.getFechaIniBaja(), "fIniBaja", parameters, JpaUtil.GREATEROREQUAL);
			JpaUtil.addTruncParameter(queryString, modelName + "fechaBaja", condicionFiltro.getFechaFinBaja(), "fFinBaja", parameters, JpaUtil.LESSOREQUAL);
			JpaUtil.addTruncParameter(queryString, modelName + "fechaRegistro", condicionFiltro.getFechaIniRegistro(), "fIniReg", parameters, JpaUtil.GREATEROREQUAL);
			JpaUtil.addTruncParameter(queryString, modelName + "fechaRegistro", condicionFiltro.getFechaFinRegistro(), "fFinReg", parameters ,JpaUtil.LESSOREQUAL);			
			
		}
		queryString.append(" ORDER BY model.codigo DESC");
		List<CondicionEspecial>  lst =	(List<CondicionEspecial>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);

		return lst;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CondicionEspecial> buscarCondicionCodigoNombre(String param) {
		Map<String, Object> parameters = new HashMap<String, Object>();	

		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + CondicionEspecial.class.getSimpleName() + " model ");	
		queryString.append(" WHERE UPPER(model.nombre) LIKE :nombre");
		
		parameters.put("nombre", "%" + param.toUpperCase() + "%");
		
		try {
			Long codigo = Long.parseLong(param);
			queryString.append(" OR model.codigo = :codigo");
			parameters.put("codigo", codigo );
		} catch (NumberFormatException ex) {
			
		}

		queryString.append(" ORDER BY model.codigo DESC");
		List<CondicionEspecial>  lst =	(List<CondicionEspecial>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);

		return lst;
	}

	@Override
	public Long generarCodigo() {
		Long codigo = null;
		StringBuilder queryString = new StringBuilder(
				"SELECT MAX(model.codigo) FROM " + CondicionEspecial.class.getSimpleName() + " model ");

		Map<String, Object> map = new HashMap<String, Object>();

		Object object = entidadDao.executeQuerySimpleResult(queryString.toString(), map);

		if (object != null) {
			codigo = (Long) object + 1;
		} else {
			codigo = 1L;
		}

		return codigo;

	}

	@Override
	public List<CondicionEspecial> buscarCondicionRankingFiltro(
			CondicionEspecialDTO condicionFiltro) {
		return null;
	}

	@Override
	public List<CondicionEspecial> obtenerCondicionEspecialPorLinea( BigDecimal idSeccion ) {
		if( idSeccion != null && idSeccion.intValue() != 0 ){
			StringBuilder queryString = new StringBuilder("SELECT cobertura.condicionEspecial FROM " +  
					CoberturaCondicionEsp.class.getSimpleName() + " cobertura");
			queryString.append(" WHERE cobertura.seccion.idToSeccion = :idSeccion");
			queryString.append(" AND cobertura.condicionEspecial.estatus = 1");
			TypedQuery<CondicionEspecial> query = entityManager.createQuery(
					queryString.toString(), CondicionEspecial.class);
			query.setParameter("idSeccion", idSeccion.longValue());
			query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);

			return query.getResultList();
		} else {
			CondicionEspecial cond = new CondicionEspecial();
			CondicionEspecialDTO cDTO = new CondicionEspecialDTO();
			cond.setEstatus(EstatusCondicion.ALTA.getValue());
			cDTO.setCondicion(cond);
			return this.buscarCondicionFiltro(cDTO);
		}
	}

	@SuppressWarnings("unchecked")
	public List<CoberturaCondicionEsp> obtenerCoberturasCondicionEspecial(Long idCondicionEspecial) {
		String modelName = "model.";
		Map<String, Object> parameters = new HashMap<String, Object>();	

		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + CoberturaCondicionEsp.class.getSimpleName() + " model " + "WHERE model.id is not null ");

		JpaUtil.addParameter(queryString, modelName + "condicionEspecial.id",  idCondicionEspecial,  "condicionEspecialId", parameters);	
		
		queryString.append(" ORDER BY model.seccion.idToSeccion");
		List<CoberturaCondicionEsp>  lst =	(List<CoberturaCondicionEsp>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);

		return lst;
	}
	
	@SuppressWarnings("unchecked")
	public List<VariableAjuste> obtenerVariablesDisponibles(Long idCondicionEspecial, String nombre) {
		
		Map<String, Object> parameters = new HashMap<String, Object>();	

		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + VariableAjuste.class.getSimpleName() + " model " + "WHERE model.id NOT IN ( ");
		queryString.append(" SELECT variable.variableAjuste.id FROM ").append(VarAjusteCondicionEsp.class.getSimpleName())
		.append(" variable WHERE variable.condicionEspecial.id IS NOT NULL ");

		JpaUtil.addParameter(queryString, "variable.condicionEspecial.id",  idCondicionEspecial,  "condicionEspecialId", parameters);
		
		queryString.append(") ");
		
		if (nombre != null) {
			JpaUtil.addParameterLike(queryString, "model.nombre", "%" + nombre + "%", "nombre", parameters);
		}
		
		queryString.append(" ORDER BY model.codigo");
		List<VariableAjuste>  lst =	(List<VariableAjuste>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);

		return lst;
	}
	
	@Override
	public List<AreaCondicionEspecial> obtenerAreasImpactoLigadas( Long idCondicionEspecial , Boolean readOnly ){
		StringBuilder queryString = new StringBuilder("SELECT DISTINCT areacond FROM " +  
				AreaCondicionEspecial.class.getSimpleName() + " areacond");
		queryString.append(" WHERE areaCond.condicionEspecial.id = :idCondicionEspecial");
		TypedQuery<AreaCondicionEspecial> query = entityManager.createQuery(queryString.toString(), AreaCondicionEspecial.class);
		query.setParameter("idCondicionEspecial", idCondicionEspecial);
		
		if( readOnly )
			query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);

		return query.getResultList();
	}

	@Override
	public List<SeccionDTO> obtenerNegociosLigados( Long idCondicionEspecial , Boolean readOnly ){
		StringBuilder queryString = new StringBuilder("SELECT cobcond.seccion FROM " +  
				CoberturaCondicionEsp.class.getSimpleName() + " cobcond");
		queryString.append(" WHERE cobcond.condicionEspecial.id = :idCondicionEspecial");

		TypedQuery<SeccionDTO> query = entityManager.createQuery(queryString.toString(), SeccionDTO.class);
		query.setParameter("idCondicionEspecial", idCondicionEspecial);
		
		if( readOnly )
			query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);

		return query.getResultList();
	}
	

	@SuppressWarnings("unchecked")
	public List<VarAjusteCondicionEsp> obtenerVarAjusteCondicionEspecial(Long idCondicionEspecial) {
		String modelName = "model.";
		Map<String, Object> parameters = new HashMap<String, Object>();	

		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + VarAjusteCondicionEsp.class.getSimpleName() + " model " + "WHERE model.id is not null ");

		JpaUtil.addParameter(queryString, modelName + "condicionEspecial.id",  idCondicionEspecial,  "condicionEspecialId", parameters);	
		List<VarAjusteCondicionEsp>  lst =	(List<VarAjusteCondicionEsp>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);

		return lst;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CondicionEspecial> obtenerCondicionesCotizacionValidas(Long cotizacionContinuityId, DateTime validOn, DateTime knownOn) {

		final StringBuilder queryString = new StringBuilder(
				"select model from " + CondicionEspecial.class.getSimpleName() + " model,  ")
		.append(BitemporalCondicionEspCot.class.getSimpleName()).append( " condBitemporal ")		
		.append(" where ")
		.append(" :validOn >= condBitemporal.validityInterval.from  and :validOn < condBitemporal.validityInterval.to ")
		.append(" and :knownOn >= condBitemporal.recordInterval.from and :knownOn < condBitemporal.recordInterval.to ")
		.append(" and condBitemporal.continuity.cotizacionContinuity.id = :cotizacionContinuityId")
		.append(" AND condBitemporal.continuity.condicionEspecialId = model.id");			
		
		Query query = entityManager.createQuery(queryString.toString());
		
		query.setParameter("validOn", validOn.toDate());
		query.setParameter("knownOn", knownOn.toDate());
		query.setParameter("cotizacionContinuityId", cotizacionContinuityId);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CondicionEspecial> obtenerCondicionesIncisoValidas(Long cotizacionContinuityId, DateTime validOn, DateTime knownOn,
			int incisoInicial, int incisoFinal) {

		final StringBuilder queryString = 
		new StringBuilder(" select model from " + CondicionEspecial.class.getSimpleName() + " model ")
		.append(" where model.id in (")
		.append(" select distinct(condBitemporal.continuity.condicionEspecialId) from ")		
		.append( BitemporalCondicionEspInc.class.getSimpleName()).append( " condBitemporal ")
		.append(" where ")
		.append("  :validOn >= condBitemporal.validityInterval.from  and :validOn < condBitemporal.validityInterval.to ")
		.append(" and :knownOn >= condBitemporal.recordInterval.from and :knownOn < condBitemporal.recordInterval.to ")
		.append(" and condBitemporal.continuity.incisoContinuity.cotizacionContinuity.id = :cotizacionContinuityId")
		;		
		
		if (incisoInicial > 0 && incisoFinal > 0 && incisoFinal >= incisoInicial) {
			queryString.append(" and condBitemporal.continuity.incisoContinuity.numero between :incisoInicial and :incisoFinal");
		}
		
		queryString.append(")");
		
		Query query = entityManager.createQuery(queryString.toString());
		
		query.setParameter("validOn", validOn.toDate());
		query.setParameter("knownOn", knownOn.toDate());
		query.setParameter("cotizacionContinuityId", cotizacionContinuityId);
		
		if (incisoInicial > 0 && incisoFinal > 0 && incisoFinal >= incisoInicial) {
			query.setParameter("incisoInicial", incisoInicial);
			query.setParameter("incisoFinal", incisoFinal);
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
		
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<CondicionEspecial> obtenerCondicionesIncisoValidasInciso(Long incisoContinuityId, DateTime validOn, DateTime knownOn) {

		final StringBuilder queryString = 
		new StringBuilder(" select model from " + CondicionEspecial.class.getSimpleName() + " model ")
		.append(" where model.id in (")
		.append(" select distinct(condBitemporal.continuity.condicionEspecialId) from ")		
		.append( BitemporalCondicionEspInc.class.getSimpleName()).append( " condBitemporal ")
		.append(" where ")
		.append("  :validOn >= condBitemporal.validityInterval.from  and :validOn < condBitemporal.validityInterval.to ")
		.append(" and :knownOn >= condBitemporal.recordInterval.from and :knownOn < condBitemporal.recordInterval.to ")
		.append(" and condBitemporal.continuity.incisoContinuity.id = :incisoContinuityId")
		;		
		
		
		queryString.append(")");
		
		Query query = entityManager.createQuery(queryString.toString());
		
		query.setParameter("validOn", validOn.toDate());
		query.setParameter("knownOn", knownOn.toDate());
		query.setParameter("incisoContinuityId", incisoContinuityId);
		
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
		
	}

}