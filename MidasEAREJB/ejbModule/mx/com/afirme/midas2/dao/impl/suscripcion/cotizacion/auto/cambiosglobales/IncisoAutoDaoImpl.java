package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.cambiosglobales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cambiosglobales.IncisoAutoDao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.IncisoNumeroSerieDTO;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class IncisoAutoDaoImpl extends JpaDao<Long, IncisoAutoCot> implements IncisoAutoDao{



	@SuppressWarnings("unchecked")
	@Override
	public List<Paquete> getPaquete(BigDecimal idToCotizacion) {		
		StringBuilder stm = new StringBuilder("");
		stm.append("SELECT DISTINCT model.paquete FROM IncisoAutoCot as model where ");
		stm.append("model.incisoCotizacionDTO.id.idToCotizacion=:idToCotizacion ");

		Query query = entityManager.createQuery(stm.toString());
		query.setParameter("idToCotizacion", idToCotizacion);

		query.setHint(QueryHints.REFRESH, HintValues.FALSE);
		query.setHint(QueryHints.CACHE_RETRIEVE_MODE, HintValues.TRUE);
		return query.getResultList();		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<IncisoNumeroSerieDTO> numerosSerieRepetidos(BigDecimal idToCotizacion, BigDecimal numeroInciso, String numeroSerie,
			Date fechaIniVigencia, Date fechaFinVigencia) throws Exception {
		List<IncisoNumeroSerieDTO> repetidos = new ArrayList<IncisoNumeroSerieDTO>();
		StoredProcedureHelper storedHelper = null;

		try {
			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgAUT_Servicios.spAUT_ValidaNumSerieRepetidos");
			String[] atributosDTO = { "idCotizacion", "idSeccion", "numeroInciso", 
					"numeroSerie", "fechaIniVigencia", "fechaFinVigencia" };
			String[] columnasResulset = { "id_cotizacion", "id_seccion", "numero_inciso",
					"numero_serie", "fecha_ini_vigencia", "fecha_fin_vigencia"};			
			storedHelper.estableceMapeoResultados(
					IncisoNumeroSerieDTO.class.getCanonicalName(), atributosDTO,
					columnasResulset);
			storedHelper.estableceParametro("pIdCotizacion", idToCotizacion);
			storedHelper.estableceParametro("pNumeroInciso", numeroInciso);
			storedHelper.estableceParametro("pNumeroSerie", numeroSerie);
			storedHelper.estableceParametro("pIniVigencia", fechaIniVigencia);
			storedHelper.estableceParametro("pFinVigencia", fechaFinVigencia);
			repetidos = (List<IncisoNumeroSerieDTO>)storedHelper.obtieneListaResultados();			
		} catch (Exception e) {			
			LogDeMidasInterfaz.log("Excepcion en BD de IncisoAutoDaoImpl.numerosSerieRepetidos...." + this,
					Level.WARNING, e);
			throw e;
		}
		return repetidos;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@SuppressWarnings("unchecked")
	public List<IncisoNumeroSerieDTO> numerosSerieRepetidosPoliza(BigDecimal idToCotizacion, BigDecimal numeroInciso, String numeroSerie)  
			throws Exception{
		
		List<IncisoNumeroSerieDTO> repetidos = new ArrayList<IncisoNumeroSerieDTO>();
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(
					"MIDAS.pkgAUT_Servicios.spAUT_ValidaNumSerieRepePol");
			String[] atributosDTO = { "incisoContinuityId", "numeroInciso"};
			String[] columnasResulset = { "mincisoc_id", "numero_inciso"};			
			storedHelper.estableceMapeoResultados(
					IncisoNumeroSerieDTO.class.getCanonicalName(), atributosDTO,
					columnasResulset);
			storedHelper.estableceParametro("pIdCotizacion", idToCotizacion);
			storedHelper.estableceParametro("pNumeroInciso", numeroInciso);
			storedHelper.estableceParametro("pNumeroSerie", numeroSerie);
			repetidos = (List<IncisoNumeroSerieDTO>)storedHelper.obtieneListaResultados();			
		} catch (Exception e) {			
			LogDeMidasInterfaz.log("Excepcion en BD de IncisoAutoDaoImpl.numerosSerieRepetidos...." + this,
					Level.WARNING, e);
			throw e;
		}
		return repetidos;
	}
}
