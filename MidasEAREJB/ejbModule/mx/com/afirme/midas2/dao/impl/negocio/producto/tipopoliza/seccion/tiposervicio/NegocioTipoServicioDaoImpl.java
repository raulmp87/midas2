package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza.seccion.tiposervicio;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicioDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio_;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioTipoServicioDaoImpl extends JpaDao<BigDecimal, NegocioTipoServicio> implements NegocioTipoServicioDao {

	@Override
	public NegocioTipoServicio buscarDefault(
			NegocioSeccion negocioSeccion) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<NegocioTipoServicio> cq = cb.createQuery(NegocioTipoServicio.class);
		Root<NegocioTipoServicio> ntu = cq.from(NegocioTipoServicio.class);
		Predicate p = cb.equal(ntu.get(NegocioTipoServicio_.negocioSeccion), negocioSeccion);
		p = cb.and(p, cb.equal(ntu.get(NegocioTipoServicio_.claveDefault), true));
		cq.where(p);
		TypedQuery<NegocioTipoServicio> q = entityManager.createQuery(cq);
		return getSingleResult2(q);
	}
	
	@Override
	public NegocioTipoServicio getDefaultByNegSeccionEstiloVehiculo(
			BigDecimal idToNegSeccion, BigDecimal idTcTipoVehiculo) {
		StringBuffer queryString = new StringBuffer("SELECT model FROM NegocioTipoServicio model");
		queryString.append(" WHERE model.tipoServicioVehiculoDTO.idTcTipoVehiculo = :idTcTipoVehiculo");
		queryString.append(" AND model.negocioSeccion.idToNegSeccion = :idToNegSeccion");
		queryString.append(" AND model.claveDefault = 1");

		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("idTcTipoVehiculo", idTcTipoVehiculo);
		query.setParameter("idToNegSeccion", idToNegSeccion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		try{
			return (NegocioTipoServicio) query.getSingleResult();
		}catch(NoResultException e){
			return null;
		}
	}
	
}
