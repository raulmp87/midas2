package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza;

import java.math.BigDecimal;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.NegocioTipoPolizaDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;

@Stateless
public class NegocioTipoPolizaDaoImpl extends JpaDao<Long, NegocioTipoPoliza> implements NegocioTipoPolizaDao {

	@Override
	public NegocioTipoPoliza getPorIdNegocioProductoIdToTipoPoliza(Long idToNegProducto,
			BigDecimal idToTipoPoliza) {
		LogDeMidasEJB3.log("finding NegocioTipoPoliza instances by idNegProducto " + idToNegProducto+" and idToTipoPoliza:"+idToTipoPoliza, Level.INFO, null);
		try {
			String queryString = "select model from NegocioTipoPoliza model where " + 
				" model.negocioProducto.idToNegProducto = :idToNegProducto " +
				" and model.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegProducto", idToNegProducto);
			query.setParameter("idToTipoPoliza", idToTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return (NegocioTipoPoliza) query.getSingleResult();
		} catch (RuntimeException re) {
			throw re;
		}
	}

	@Override
	public NegocioTipoPoliza getPorIdNegocioProductoIdToNegTipoPoliza(Long idToNegProducto, BigDecimal idToNegTipoPoliza) {
		LogDeMidasEJB3.log("finding NegocioTipoPoliza instances by idNegProducto "+idToNegProducto+" and idToNegTipoPoliza" + idToNegTipoPoliza, Level.INFO, null);
		try {
			String queryString = "select model from NegocioTipoPoliza model where " + 
				" model.negocioProducto.idToNegProducto = :idToNegProducto " +
				" and model.idToNegTipoPoliza = :idToNegTipoPoliza ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegProducto", idToNegProducto);
			query.setParameter("idToNegTipoPoliza", idToNegTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return (NegocioTipoPoliza) query.getSingleResult();
		} catch (RuntimeException re) {
			throw re;
		}
	}
}
