package mx.com.afirme.midas2.dao.impl.bitemporal.endoso.inciso;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.dao.bitemporal.endoso.inciso.IncisoDao;
import mx.com.afirme.midas2.dao.impl.bitemporal.EntidadContinuityDaoImpl;

import org.joda.time.DateTime;

@Stateless
public class IncisoDaoImpl extends EntidadContinuityDaoImpl implements IncisoDao {

	@Override
	public Integer getMaxIncisos(Long cotizacionContinuityId, DateTime validoEn) {
			
		Integer resultado = null;
		
		final String queryString = 	"select max(model.numero) from IncisoContinuity model " +
		" where model.cotizacionContinuity.id = :cotizacionContinuityId";
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("cotizacionContinuityId", cotizacionContinuityId);
		
		resultado =  (Integer) this.executeQuerySimpleResult(queryString, parameters);
		
		return (resultado != null?resultado:0);
	
	}

	@Override
	public Integer getMaxNumeroSecuencia(Long cotizacionContinuityId, DateTime validoEn) {
			
		Integer resultado = null;
		
		final String queryString = 	"Select MAX(model.value.numeroSecuencia) from BitemporalInciso model" +
		" where model.continuity.cotizacionContinuity.id = :cotizacionContinuityId ";
				
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("cotizacionContinuityId", cotizacionContinuityId);
		
		resultado =  (Integer) this.executeQuerySimpleResult(queryString, parameters);
		
		return (resultado != null?resultado:0);
		
	}
	
	public BigDecimal getTotalIncisosCotizacion(BigDecimal idToCotizacion){
		BigDecimal resultado = BigDecimal.ZERO;
		
		Query query;
		
		final String queryString = "Select count(1) from MIDAS.TOINCISOCOT " + 
								    " where idToCotizacion = ?";
		query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, idToCotizacion);
		
		resultado = (BigDecimal) query.getSingleResult();
		
		return resultado;
	}
	
	public void guardarConductoCobroCotizacionAInciso(CotizacionDTO cotizacionDTO){

		Query query;
		final String queryString = "UPDATE MIDAS.TOINCISOCOT " +
								   "SET idMedioPago = ?1 , " +
								   		"idClienteCob = ?2 , " +
								   		"idClienteConductoCobro = ?3 " +
								   	"WHERE idToCotizacion = ?4";  
		
		query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, cotizacionDTO.getIdMedioPago());
		query.setParameter(2, cotizacionDTO.getIdToPersonaContratante());
		query.setParameter(3, cotizacionDTO.getIdConductoCobroCliente());
		query.setParameter(4, cotizacionDTO.getIdToCotizacion());
		
		query.executeUpdate();
	}
	

}
