package mx.com.afirme.midas2.dao.impl.domicilio;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
/**
 * Utileria para ver si un domicilio esta vacio, esto se usa para los filtros de domicilio
 * @author vmhersil
 *
 */
public class DomicilioUtils {
	public static boolean isEmptyAddress(Domicilio domicilio){
		boolean isEmpty=true;
		if(isNotNull(domicilio)){
			if(isValid(domicilio.getCalleNumero())){
				isEmpty=false;
			}
			if(isValid(domicilio.getCodigoPostal())){
				isEmpty=false;
			}
			if(isValid(domicilio.getCiudad())){
				isEmpty=false;
			}
			if(isValid(domicilio.getClaveCiudad())){
				isEmpty=false;
			}
			if(isValid(domicilio.getClaveEstado())){
				isEmpty=false;
			}
			if(isValid(domicilio.getNombreColonia())){
				isEmpty=false;
			}
		}
		return isEmpty;
	}
}
