package mx.com.afirme.midas2.dao.impl.catalogos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.catalogos.ValorSeccionCobAutosDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutosId;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutosId_;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos_;

@Stateless
public class ValorSeccionCobAutosDaoImpl extends JpaDao<ValorSeccionCobAutosId, ValorSeccionCobAutos> implements
		ValorSeccionCobAutosDao {

	@Override
	public List<ValorSeccionCobAutos> getPorIdToSeccion(
			Long idToSeccion) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ValorSeccionCobAutos> criteriaQuery = cb.createQuery(ValorSeccionCobAutos.class);
		Root<ValorSeccionCobAutos> root = criteriaQuery.from(ValorSeccionCobAutos.class);
		Predicate predicado = cb.equal(root.get(ValorSeccionCobAutos_.id).get(ValorSeccionCobAutosId_.idToSeccion), idToSeccion);
		criteriaQuery.where(predicado);
		
		TypedQuery<ValorSeccionCobAutos> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}





}
