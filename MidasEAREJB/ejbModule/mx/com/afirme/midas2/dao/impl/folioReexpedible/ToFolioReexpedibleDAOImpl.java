package mx.com.afirme.midas2.dao.impl.folioReexpedible;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.math.BigDecimal;

import mx.com.afirme.midas2.dao.folioReexpedible.TcTipoFolioDAO;
import mx.com.afirme.midas2.dao.folioReexpedible.ToFolioReexpedibleDAO;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.folioReexpedible.FiltroFolioReexpedible;
import mx.com.afirme.midas2.domain.folioReexpedible.TcTipoFolio;
import mx.com.afirme.midas2.domain.folioReexpedible.ToFolioReexpedible;
import mx.com.afirme.midas2.domain.negocio.Negocio;

/**
 * Clase que contiene la implementaci\u00f3n de la l\u00f3gica de la interfaz
 * ToFolioReexpedibleDAO.
 * 
 * @since 11022016
 * 
 * @author AFIRME
 *
 */
@Stateless
public class ToFolioReexpedibleDAOImpl extends EntidadDaoImpl implements ToFolioReexpedibleDAO {

	private static Logger log = LoggerFactory.getLogger(ToFolioReexpedibleDAOImpl.class);
	
	@EJB
	private TcTipoFolioDAO tipoFolioDAO;
	
	@Override
	public void eliminarToFolioReexpedible(ToFolioReexpedible entity) {
		entity.setActivo(Boolean.FALSE);
		update(entity);
	}

	@Override
	public void eliminarVarios(List<ToFolioReexpedible> entities) {
		for (ToFolioReexpedible entity : entities) {
			eliminarToFolioReexpedible(entity);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ToFolioReexpedible> findAll() {
		final String queryString = "select model from ToFolioReexpedible model";
		Query query = entityManager.createQuery(queryString);
		query.setMaxResults(100);
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}

	@Override
	public ToFolioReexpedible findById(Long idFolio) {
		ToFolioReexpedible entity = entityManager.find(
				ToFolioReexpedible.class, idFolio);
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ToFolioReexpedible> findByProperty(String propertyName, Object value) {
		final StringBuilder queryString = new StringBuilder("select model from ToFolioReexpedible model where  model.");
		queryString.append(propertyName).append("= :propertyValue and model.tipoFolio.activo = :activo");
		log.info(": [  INFO  ] : LA CONSULTA DE LOS FOLIOS REEXPEDIBLES ES " + queryString.toString());
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("propertyValue", value);
		query.setParameter("activo", Boolean.TRUE);
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ToFolioReexpedible> findByProperties(FiltroFolioReexpedible folioFiltro){
		final StringBuilder queryString = new StringBuilder("select model from ToFolioReexpedible model where ");
		Map<String, Object> mapaValores = new HashMap<String,Object>();
				
		if((folioFiltro.getFolioInicio()!= null && folioFiltro.getFolioInicio().trim().length() > 5) && 
				(folioFiltro.getFolioFin()!= null && folioFiltro.getFolioFin().trim().length() > 5)){
			log.info(" :  [INFO]   :  >>>>>>>>>>>>> RANGOS DE LOS FOLIOS -----  >>");
			
			String[] folioInicio = folioFiltro.getFolioInicio().split("_");
			String[] folioFin = folioFiltro.getFolioFin().split("_");
			Integer folioInicioInt = Integer.parseInt(folioInicio[1]);
			Integer folioFinInt = Integer.parseInt(folioFin[1]);
			List<TcTipoFolio> tiposfolios = tipoFolioDAO.findAll();
			TcTipoFolio tipoFolio = null;
			for(TcTipoFolio tFolio: tiposfolios){
				if(tFolio.getClaveTipoFolio().trim().equals(folioInicio[0].trim())){
					tipoFolio = tFolio;
					break;
				}
			}
			
			if(tipoFolio != null){
    			if(folioFiltro.getTipoFolio() == null || folioFiltro.getTipoFolio().getIdTipoFolio() <= 0 || folioFiltro.getTipoFolio().getIdTipoFolio() != tipoFolio.getIdTipoFolio()){
    				folioFiltro.setTipoFolio(new TcTipoFolio(tipoFolio.getIdTipoFolio()));
    			}
    			
    			queryString.append(MODEL)
    				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[9])
    				.append(BETWEEN)
    				.append(DOSPUNTOS)
    				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[9])
    				.append(AND)
    				.append(DOSPUNTOS)
    				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[14])
    				.append(AND);
    			
    			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[9], folioInicioInt);
    			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[14], folioFinInt);
			}
		} else if (folioFiltro.getFolioInicio()!= null && folioFiltro.getFolioInicio().trim().length() > 5){
			
			String[] folioInicio = folioFiltro.getFolioInicio().split("_");
			Integer folioInicioInt = Integer.parseInt(folioInicio[1]);
			List<TcTipoFolio> tiposfolios = tipoFolioDAO.findAll();
			TcTipoFolio tipoFolio = null;
			for(TcTipoFolio tFolio: tiposfolios){
				if(tFolio.getClaveTipoFolio().trim().equals(folioInicio[0].trim())){
					tipoFolio = tFolio;
					break;
				}
			}
			
			if(tipoFolio != null){
    			if(folioFiltro.getTipoFolio() == null || folioFiltro.getTipoFolio().getIdTipoFolio() <= 0 || folioFiltro.getTipoFolio().getIdTipoFolio() != tipoFolio.getIdTipoFolio()){
    				folioFiltro.setTipoFolio(new TcTipoFolio(tipoFolio.getIdTipoFolio()));
    			}
    			
    			queryString.append(MODEL)
    				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[9])
    				.append(IGUALPARAMETRO)
    				.append(DOSPUNTOS)
    				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[9])
    				.append(AND);
    			
    			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[9], folioInicioInt);
			}
		}
		
		if(folioFiltro.getTipoFolio() != null && folioFiltro.getTipoFolio().getIdTipoFolio() > 0L){
			log.info(" :  [INFO]   :  >>>>>>>>>>>>> Entr\u00f3 a obtener parametro de tipo folio");
			log.info(" :  [INFO]   :  >>>>>>>>>>>>> el id del tipo de folio es: -----  >>" + folioFiltro.getTipoFolio().getIdTipoFolio());
			queryString.append(MODEL)
				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[10])
				.append(".idTipoFolio")
				.append(IGUALPARAMETRO)
				.append(DOSPUNTOS)
				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[10])
				.append(AND);
			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[10], folioFiltro.getTipoFolio().getIdTipoFolio());
		}
		
		if(folioFiltro.getNegocio() != null && folioFiltro.getNegocio().getIdToNegocio() != null && folioFiltro.getNegocio().getIdToNegocio() > 0L){
			log.info(": [  INFO  ] : >>>>>>> INGRESA A OBTENER EL NEGOCIO EL CUAL ES: " +folioFiltro.getNegocio().getIdToNegocio());
			queryString.append(MODEL)
				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[3])
				.append(IGUALPARAMETRO)
				.append(DOSPUNTOS)
				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[3])
				.append(AND);
			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[3], folioFiltro.getNegocio().getIdToNegocio());
		}
		
		if(folioFiltro.getProducto() != null && folioFiltro.getProducto().getIdToNegProducto() != null && folioFiltro.getProducto().getIdToNegProducto() > 0L){
			log.info(": [  INFO  ] : >>>>>>> INGRESA A OBTENER EL PRODUCTO: " + folioFiltro.getProducto().getIdToNegProducto());
		
			queryString.append(MODEL)
				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[2])
				.append(IGUALPARAMETRO)
				.append(DOSPUNTOS)
				.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[2])
				.append(AND);
			
			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[2], folioFiltro.getProducto().getIdToNegProducto());
		}
		Integer idVigencia = (folioFiltro.getVigencia() != null && folioFiltro.getVigencia().getIdTcVigencia() != null 
					? folioFiltro.getVigencia().getIdTcVigencia().intValue() : null);
		if(idVigencia != null && idVigencia > 0){
			log.info(": [  INFO  ] : >>>>>>> INGRSA A OBTENER VIGENCIAS " + folioFiltro.getVigencia().getId());
			log.info(": [  INFO  ] : >>>>>>> VALOR IDTCVIGENCIA EN INTEGER ::::::>>>>  " + idVigencia);
			
			queryString.append(MODEL)
    			.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[5])
    			.append(IGUALPARAMETRO)
    			.append(DOSPUNTOS)
    			.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[5])
    			.append(AND);
			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[5], folioFiltro.getVigencia().getId());
		}
		
		if(folioFiltro.getMoneda()!= null && folioFiltro.getMoneda().getIdTcMoneda() > 0L){
			log.info(": [  INFO  ] : >>>>>>> INGRESA A OBTENER MONEDAS " + folioFiltro.getMoneda().getIdTcMoneda() );
			
			queryString.append(MODEL)
    			.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[1])
    			.append(IGUALPARAMETRO)
    			.append(DOSPUNTOS)
    			.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[1])
    			.append(AND);
			
			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[1], folioFiltro.getMoneda().getIdTcMoneda());
		}
		if(folioFiltro.getFechaValidez() != null){
			log.info(": [  INFO  ] : >>>>>>> TRAE VALIDEZ");	
			
			queryString.append(MODEL)
    			.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[7])
    			.append(IGUALPARAMETRO)
    			.append(DOSPUNTOS)
    			.append(ATRIBUTOS_FOLIO_REEXPEDIBLE[7])
    			.append(AND);
			
			mapaValores.put(ATRIBUTOS_FOLIO_REEXPEDIBLE[7], folioFiltro.getFechaValidez());
		}
		
		
		queryString.append(" model.tipoFolio.activo = :activo ")
			.append(" order by model.idFolioReexpedible ");
		log.info(": [  INFO  ] : LA CONSULTA DE LOS FOLIOS REEXPEDIBLES ES " + queryString.toString());
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("activo", Boolean.TRUE);
		for(Map.Entry<String, Object> entry: mapaValores.entrySet()){
			log.info(": [  INFO  ] : >>>>>>> LLAVE ---------> "+ entry.getKey() + " VALOR " + entry.getValue());	
			query.setParameter(entry.getKey(), entry.getValue());
		}
		
		return query.getResultList();
	}

	@Override
	public void persist(ToFolioReexpedible entity) {
		entityManager.persist(entity);
	}

	@Override
	public List<ToFolioReexpedible> saveAll(List<ToFolioReexpedible> entities) {
		for(ToFolioReexpedible entity: entities){
			entityManager.persist(entity);
		}
		return entities;
	}

	@Override
	public ToFolioReexpedible update(ToFolioReexpedible entity) {
		return entityManager.merge(entity);
	}

	@Override
	public List<ToFolioReexpedible> updateAll(List<ToFolioReexpedible> entities) {
		for(ToFolioReexpedible entity: entities){
			update(entity);
		}
		return entities;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	private Query generaSQLConsulta(FiltroFolioReexpedible filtro, boolean isCount){
		StringBuilder sql;
		
		if(isCount)
			sql = new StringBuilder(SQL_COUNT_REGISTROS);
		else
			sql = new StringBuilder(SQL_QUERY);
		
		Integer parameterCount = 0;
		Map<Integer, Object> mapaValores = new HashMap<Integer, Object>();
		if(filtro.getNegocio() != null && filtro.getNegocio().getIdToNegocio() != null && filtro.getNegocio().getIdToNegocio() > 0L){
			parameterCount++;
			sql.append(ID_NEGOCIO)
				.append(IGUALPARAMETRO)
				.append(INTERROGACION)
				.append(parameterCount)
				.append(AND);
			mapaValores.put(parameterCount, filtro.getNegocio().getIdToNegocio());
		}
		
		if(filtro.getProducto() != null && filtro.getProducto().getIdToNegProducto() != null && filtro.getProducto().getIdToNegProducto() > 0L){
			parameterCount++;
			sql.append(ID_NEG_PROD)
				.append(IGUALPARAMETRO)
				.append(INTERROGACION)
				.append(parameterCount)
				.append(AND);
			mapaValores.put(parameterCount, filtro.getProducto().getIdToNegProducto());
		}
		
		if(filtro.getTipoPoliza() != null && filtro.getTipoPoliza().getIdToNegTipoPoliza() != null && filtro.getTipoPoliza().getIdToNegTipoPoliza().intValue() > 0){
			parameterCount++;
			sql.append(ID_NEG_T_POLIZA)
				.append(IGUALPARAMETRO)
				.append(INTERROGACION)
				.append(parameterCount)
				.append(AND);
			mapaValores.put(parameterCount, filtro.getTipoPoliza().getIdToNegTipoPoliza());
		}
		
		if(filtro.getVigencia() != null && filtro.getVigencia().getIdTcVigencia() !=null && filtro.getVigencia().getIdTcVigencia().intValue() > 0){
			parameterCount++;
			sql.append(ID_VIGENCIA)
				.append(IGUALPARAMETRO)
				.append(INTERROGACION)
				.append(parameterCount)
				.append(AND);
			mapaValores.put(parameterCount, filtro.getVigencia().getIdTcVigencia());
		}
		
		if(filtro.getMoneda() != null && filtro.getMoneda().getIdTcMoneda() != null && filtro.getMoneda().getIdTcMoneda().intValue() >0){
			parameterCount++;
			sql.append(ID_MONEDA)
				.append(IGUALPARAMETRO)
				.append(INTERROGACION)
				.append(parameterCount)
				.append(AND);
			mapaValores.put(parameterCount, filtro.getMoneda().getIdTcMoneda());
		}
		
		if(filtro.getFechaValidez() !=null){
			parameterCount++;
			sql.append(FECHA_VALIDEZ)
				.append(IGUALPARAMETRO)
				.append(INTERROGACION)
				.append(parameterCount)
				.append(AND);
			mapaValores.put(parameterCount, filtro.getFechaValidez());
		}
		
		if(filtro.getFolioInicio() != null && filtro.getFolioInicio().trim().length() > 5 && filtro.getFolioFin() != null && filtro.getFolioFin().trim().length() > 5){
			parameterCount++;
			String[] folioInicio = filtro.getFolioInicio().split("_");
			String[] folioFin = filtro.getFolioFin().split("_");
			Integer folioInicioInt = Integer.parseInt(folioInicio[1]);
			Integer folioFinInt = Integer.parseInt(folioFin[1]);
			List<TcTipoFolio> tiposfolios = tipoFolioDAO.findAll();
			TcTipoFolio tipoFolio = new TcTipoFolio();
			for(TcTipoFolio tFolio: tiposfolios){
				if(tFolio.getClaveTipoFolio().trim().equals(folioInicio[0].trim())){
					tipoFolio = tFolio;
					break;
				}
			}
			if(filtro.getTipoFolio() == null || filtro.getTipoFolio().getIdTipoFolio() <= 0 || filtro.getTipoFolio().getIdTipoFolio() != tipoFolio.getIdTipoFolio()){
				filtro.setTipoFolio(new TcTipoFolio(tipoFolio.getIdTipoFolio()));
			}
			sql.append(NUMERO_FOLIO)
				.append(BETWEEN)
				.append(INTERROGACION)
				.append(parameterCount);
			parameterCount++;
			sql.append(AND)
				.append(INTERROGACION)
				.append(parameterCount)
				.append(AND);
			mapaValores.put((parameterCount - 1), folioInicioInt);
			mapaValores.put(parameterCount, folioFinInt);
		} else if (filtro.getFolioInicio() != null && filtro.getFolioInicio().trim().length() > 5){
			parameterCount++;
			sql.append(NUMERO_FOLIO)
    			.append(IGUALPARAMETRO)
    			.append(INTERROGACION)
    			.append(parameterCount)
    			.append(AND);
			
			String[] folioInicio = filtro.getFolioInicio().split("_");
			Integer folioInicioInt = Integer.parseInt(folioInicio[1]);
			List<TcTipoFolio> tiposfolios = tipoFolioDAO.findAll();
			TcTipoFolio tipoFolio = new TcTipoFolio();
			for(TcTipoFolio tFolio: tiposfolios){
				if(tFolio.getClaveTipoFolio().trim().equals(folioInicio[0].trim())){
					tipoFolio = tFolio;
					break;
				}
			}
			if(filtro.getTipoFolio() == null || filtro.getTipoFolio().getIdTipoFolio() <= 0 || filtro.getTipoFolio().getIdTipoFolio() != tipoFolio.getIdTipoFolio()){
				filtro.setTipoFolio(new TcTipoFolio(tipoFolio.getIdTipoFolio()));
			}
			mapaValores.put(parameterCount - 1, folioInicioInt);
		}
		
		if (filtro.getFolioInicioL() != null && filtro.getFolioInicioL() > 0 && filtro.getFolioFinL() != null && filtro.getFolioFinL() > 0){
			parameterCount++;
			sql.append(NUMERO_FOLIO)
			.append(BETWEEN)
			.append(INTERROGACION)
			.append(parameterCount);
    		parameterCount++;
    		sql.append(AND)
    			.append(INTERROGACION)
    			.append(parameterCount)
    			.append(AND);
    		mapaValores.put((parameterCount - 1), filtro.getFolioInicioL());
    		mapaValores.put(parameterCount, filtro.getFolioFinL());
		} else if (filtro.getFolioInicioL() != null && filtro.getFolioInicioL() > 0) {
			parameterCount++;
			sql.append(NUMERO_FOLIO)
			.append(IGUALPARAMETRO)
			.append(INTERROGACION)
			.append(parameterCount)
			.append(AND);
			
			mapaValores.put(parameterCount, filtro.getFolioInicioL());
		} else if(filtro.getFolioFinL() != null && filtro.getFolioFinL() > 0){
			parameterCount++;
			sql.append(NUMERO_FOLIO)
			.append(IGUALPARAMETRO)
			.append(INTERROGACION)
			.append(parameterCount)
			.append(AND);
			
			mapaValores.put(parameterCount, filtro.getFolioFinL());
		}
		
		if(filtro.getTipoFolio() != null && filtro.getTipoFolio().getIdTipoFolio() > 0L){
			parameterCount++;
			sql.append(ID_TIPO_FOLIO)
				.append(IGUALPARAMETRO)
				.append(INTERROGACION)
				.append(parameterCount)
				.append(AND);
			mapaValores.put(parameterCount, filtro.getTipoFolio().getIdTipoFolio());
		}
		
		parameterCount++;
		sql.append(" FOLIO.ACTIVO = ?")
			.append(parameterCount)
			.append(SQL_ORDER);
		mapaValores.put(parameterCount, 1);
		log.info(sql.toString());
		Query query = entityManager.createNativeQuery(sql.toString());
		
		for(Map.Entry<Integer, Object> entry: mapaValores.entrySet()){
			query.setParameter(entry.getKey(), entry.getValue());
		}
		
		if(filtro.getPosStart() != null){
			query.setFirstResult(filtro.getPosStart());
		}
		
		if(filtro.getCount() != null){
			query.setMaxResults(filtro.getCount());
		}
		
		return query;
	}
	
	private List<FiltroFolioReexpedible> parcearLista(List<Object[]> resultados){
		List<FiltroFolioReexpedible> foliosResult = new ArrayList<FiltroFolioReexpedible>();
		FiltroFolioReexpedible folio = null;
		for(Object[] object: resultados){
			folio=new FiltroFolioReexpedible();
			folio.setTipoFolio(new TcTipoFolio());
			folio.getTipoFolio().setClaveTipoFolio((String)object[0]);
			folio.setNegocio(new Negocio());
			folio.getNegocio().setDescripcionNegocio((String)object[1]);
			folio.setDescripcionProducto((String)object[2]);
			folio.setDesctipcionTipoPoliza((String)object[3]);
			folio.setDescVigencia((String)object[4]);
			BigDecimal id = (BigDecimal)object[5];
			folio.setFolioInicio(folio.getTipoFolio().getClaveTipoFolio() + "-" + id.intValue());
			folio.setFechaValidez((Date)object[6]);
			folio.setNombreAgente((object[8] != null ? (String) object[8] : null ));
			folio.setComentarios((object[9] != null ? (String) object[9] : null));
			foliosResult.add(folio);
		}
		return foliosResult;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<FiltroFolioReexpedible> consultarFolioReexpediblesSQL(FiltroFolioReexpedible filtro){
		
		Query query = generaSQLConsulta(filtro, Boolean.FALSE);
		
		@SuppressWarnings("rawtypes")
		List result  = query.getResultList();
		return parcearLista((List<Object[]>) result);
	}
	
	public Long contarRegistrosFolios(FiltroFolioReexpedible filtro){
		
		Query query = generaSQLConsulta(filtro, Boolean.TRUE);
		
		BigDecimal numeroRegistros = (BigDecimal) query.getSingleResult();
		return numeroRegistros.longValue();
	}

	public TcTipoFolioDAO getTipoFolioDAO() {
		return tipoFolioDAO;
	}

	public void setTipoFolioDAO(TcTipoFolioDAO tipoFolioDAO) {
		this.tipoFolioDAO = tipoFolioDAO;
	}
}
