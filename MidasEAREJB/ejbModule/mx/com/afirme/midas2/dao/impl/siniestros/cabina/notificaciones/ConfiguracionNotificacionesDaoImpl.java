package mx.com.afirme.midas2.dao.impl.siniestros.cabina.notificaciones;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Expression;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.cabina.notificaciones.ConfiguracionNotificacionesDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina_;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.MovimientoProcesoCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.MovimientoProcesoCabina_;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ProcesoCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ProcesoCabina_;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina_;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabinaDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.ConfiguracionNotificacionDTO;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ConfiguracionNotificacionesDaoImpl extends JpaDao<Long, ConfiguracionNotificacionCabina> implements ConfiguracionNotificacionesDao{

	@EJB
	private EntidadDao entidadDao;

	@Override
	public Long guardarConfiguracion(
			ConfiguracionNotificacionCabina configuracion) {
		Long idConfig=null;
		List<DestinatarioConfigNotificacionCabina>destList=configuracion.getDestinatariosNotificacion();
		ConfiguracionNotificacionCabina config = new ConfiguracionNotificacionCabina();
	
		if(configuracion.getId()!=null){
			config = obtenerConfiguracion(configuracion.getId()) ;
			config.getDestinatariosNotificacion().clear();
		}else{
			config.setFechaCreacion(configuracion.getFechaCreacion());
			config.setCodigoUsuarioCreacion(configuracion.getCodigoUsuarioCreacion());
			config.setEsActivo(ConfiguracionNotificacionCabina.Estatus.ACTIVA.getValue());
			for(DestinatarioConfigNotificacionCabina destinatario : configuracion.getDestinatariosNotificacion()){
				destinatario.setId(null);
			}
		}
		config.setNotas(configuracion.getNotas());
		config.setOficina(configuracion.getOficina());
		config.setProcesoCabina(entidadDao.findById(ProcesoCabina.class, configuracion.getProcesoCabina().getId()));
		config.setMovimientoProceso(entidadDao.findById(MovimientoProcesoCabina.class, configuracion.getMovimientoProceso().getId()));
		config.setAgenteId(configuracion.getAgenteId());
		config.setNumInciso(configuracion.getNumInciso());
		config.setNumPoliza(configuracion.getNumPoliza());
		if(config.getId() != null){
			config.setId((Long)entidadDao.persistAndReturnId(config));
		}else{
			config = entidadDao.update(config);
		}		
		for(int i=0;i<destList.size();i++){
			if(destList.get(i)!=null){	
				destList.get(i).setConfNotificacionCabina(config);
				entidadDao.persist(destList.get(i));
				config.getDestinatariosNotificacion().add(destList.get(i));
			}
		}				
		return idConfig;
	}

	@Override
	public ConfiguracionNotificacionCabina obtenerConfiguracion(String codigo) {
		return null;
	}

	@Override
	public ConfiguracionNotificacionCabina obtenerConfiguracion(
			Long configuracionId) {
		return entidadDao.findById(ConfiguracionNotificacionCabina.class, configuracionId);
	}

	@Override
	public List<ConfiguracionNotificacionCabina> obtenerConfiguraciones() {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfiguracionNotificacionCabina> obtenerConfiguracionesPorFiltros(ConfiguracionNotificacionDTO configuracionDTO) {
		
		if(configuracionDTO.getEsBusquedaPorPantalla() != null && configuracionDTO.getEsBusquedaPorPantalla().equals(Boolean.TRUE)){
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<ConfiguracionNotificacionCabina> cq = cb.createQuery(ConfiguracionNotificacionCabina.class);
			Root<ConfiguracionNotificacionCabina> root = cq.from(ConfiguracionNotificacionCabina.class);
			Join<ConfiguracionNotificacionCabina,Oficina> ofi = root.join(ConfiguracionNotificacionCabina_.oficina, JoinType.LEFT);		
			Join<ConfiguracionNotificacionCabina,ProcesoCabina> pc = root.join(ConfiguracionNotificacionCabina_.procesoCabina);
			Join<ConfiguracionNotificacionCabina,MovimientoProcesoCabina> mpc = root.join(ConfiguracionNotificacionCabina_.movimientoProceso);
			List<Predicate> predicates=new ArrayList<Predicate>();
			if(configuracionDTO.getNotas() != null && !"".equalsIgnoreCase(configuracionDTO.getNotas())){
				predicates.add(cb.like(cb.upper(root.get(ConfiguracionNotificacionCabina_.notas)), "%" + configuracionDTO.getNotas().toUpperCase() + "%"));
			}
				if(!configuracionDTO.getOficinasId().isEmpty() && !configuracionDTO.getOficinasId().get(0).isEmpty()){
		        	Predicate predicado;
		            Expression<?> listaOficinasId;
		            listaOficinasId = ofi.get(Oficina_.id.getName());
		            predicado = listaOficinasId.in(configuracionDTO.getOficinasId());
		            predicates.add(predicado);       
		        }
			
			if(configuracionDTO.getProcesoCabinaId() != null){
				predicates.add(cb.equal(pc.get(ProcesoCabina_.id), configuracionDTO.getProcesoCabinaId()));
			}
			if(configuracionDTO.getMovimientoId() != null){
				predicates.add(cb.equal(mpc.get(MovimientoProcesoCabina_.id), configuracionDTO.getMovimientoId()));
			}
			if(configuracionDTO.getCodigo() != null && !"".equalsIgnoreCase(configuracionDTO.getCodigo())){
				predicates.add(cb.equal(mpc.get(MovimientoProcesoCabina_.codigo), configuracionDTO.getCodigo()));
			}
			if(configuracionDTO.getAgenteId() != null ){
				predicates.add(cb.equal(root.get(ConfiguracionNotificacionCabina_.agenteId), configuracionDTO.getAgenteId().toString() ));
			}
			
				if( configuracionDTO.getNumPoliza() != null && !configuracionDTO.getNumPoliza().equals("")){
					predicates.add(cb.equal(root.get(ConfiguracionNotificacionCabina_.numPoliza), configuracionDTO.getNumPoliza().toString() ));
				}
			
			
			
				if(configuracionDTO.getNumInciso() != null && !configuracionDTO.getNumInciso().equals("")){
					predicates.add(cb.equal(root.get(ConfiguracionNotificacionCabina_.numInciso), configuracionDTO.getNumInciso().toString() ));
				}
			
			
			
			cq.where(predicates.toArray(new Predicate[predicates.size()]));	
			cq.orderBy(cb.asc(mpc.get(MovimientoProcesoCabina_.tiempoTranscurrir)));
			Query query = entityManager.createQuery(cq);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		}else{
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			CriteriaQuery<ConfiguracionNotificacionCabina> cq = cb.createQuery(ConfiguracionNotificacionCabina.class);
			Root<ConfiguracionNotificacionCabina> root = cq.from(ConfiguracionNotificacionCabina.class);
			Join<ConfiguracionNotificacionCabina,Oficina> ofi = root.join(ConfiguracionNotificacionCabina_.oficina, JoinType.LEFT);		
			Join<ConfiguracionNotificacionCabina,ProcesoCabina> pc = root.join(ConfiguracionNotificacionCabina_.procesoCabina);
			Join<ConfiguracionNotificacionCabina,MovimientoProcesoCabina> mpc = root.join(ConfiguracionNotificacionCabina_.movimientoProceso);
			//Join<ConfiguracionNotificacionCabina,DestinatarioConfigNotificacionCabina> dcn = root.join(ConfiguracionNotificacionCabina_.destinatariosNotificacion);
			List<Predicate> predicates=new ArrayList<Predicate>();
			if(configuracionDTO.getNotas() != null && !"".equalsIgnoreCase(configuracionDTO.getNotas())){
				predicates.add(cb.like(cb.upper(root.get(ConfiguracionNotificacionCabina_.notas)), "%" + configuracionDTO.getNotas().toUpperCase() + "%"));
			}
			if(configuracionDTO.getOficinaId() != null){
				predicates.add(cb.equal(ofi.get(Oficina_.id), configuracionDTO.getOficinaId()));
			}
			if(configuracionDTO.getProcesoCabinaId() != null){
				predicates.add(cb.equal(pc.get(ProcesoCabina_.id), configuracionDTO.getProcesoCabinaId()));
			}
			if(configuracionDTO.getMovimientoId() != null){
				predicates.add(cb.equal(mpc.get(MovimientoProcesoCabina_.id), configuracionDTO.getMovimientoId()));
			}
			if(configuracionDTO.getCodigo() != null && !"".equalsIgnoreCase(configuracionDTO.getCodigo())){
				predicates.add(cb.equal(mpc.get(MovimientoProcesoCabina_.codigo), configuracionDTO.getCodigo()));
			}
			/*if(configuracionDTO.getModoEnvioId() != null && !"".equalsIgnoreCase(configuracionDTO.getModoEnvioId())){
				predicates.add(cb.equal(dcn.get(DestinatarioConfigNotificacionCabina_.modoEnvio), Integer.parseInt(configuracionDTO.getModoEnvioId())));
			}
			if(configuracionDTO.getTipoDestinatarioId() != null && !"".equalsIgnoreCase(configuracionDTO.getTipoDestinatarioId())){
				predicates.add(cb.equal(dcn.get(DestinatarioConfigNotificacionCabina_.tipoDestinatario), Integer.parseInt(configuracionDTO.getTipoDestinatarioId())));
			}
			if(configuracionDTO.getCorreoEnvio() != null && !"".equalsIgnoreCase(configuracionDTO.getCorreoEnvio())){
				predicates.add(cb.equal(dcn.get(DestinatarioConfigNotificacionCabina_.correosEnvio), Integer.parseInt(configuracionDTO.getCorreoEnvio())));
			}
			if(configuracionDTO.getPuesto() != null && !"".equalsIgnoreCase(configuracionDTO.getPuesto())){
				predicates.add(cb.like(cb.upper(dcn.get(DestinatarioConfigNotificacionCabina_.puesto)), "%" + configuracionDTO.getPuesto().toUpperCase() + "%"));
			}
			if(configuracionDTO.getNombre() != null && !"".equalsIgnoreCase(configuracionDTO.getNombre())){
				predicates.add(cb.like(cb.upper(dcn.get(DestinatarioConfigNotificacionCabina_.nombre)), "%" + configuracionDTO.getNombre().toUpperCase() + "%"));
			}
			if(configuracionDTO.getCorreo() != null && !"".equalsIgnoreCase(configuracionDTO.getCorreo())){
				predicates.add(cb.like(cb.upper(dcn.get(DestinatarioConfigNotificacionCabina_.correo)), "%" + configuracionDTO.getCorreo().toUpperCase() + "%"));
			}*/
			cq.where(predicates.toArray(new Predicate[predicates.size()]));	
			cq.orderBy(cb.asc(mpc.get(MovimientoProcesoCabina_.tiempoTranscurrir)));
			Query query = entityManager.createQuery(cq);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		}
	}
	
	@Override
	public List<AdjuntoConfigNotificacionCabinaDTO> obtenerAdjuntosSimpleList(ConfiguracionNotificacionCabina config){
		Map<String, Object> parameters = new HashMap<String, Object>();	
		
		StringBuilder queryString = new StringBuilder( 
				"SELECT new mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabinaDTO(c.id,c.descripcion) from " + AdjuntoConfigNotificacionCabina.class.getSimpleName()  + " c ");
				queryString.append(" WHERE  c.confNotificacionCabina.id =  "+config.getId());
		 @SuppressWarnings("unchecked")
		List<AdjuntoConfigNotificacionCabinaDTO> lista = entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		return lista;
	}
	
	

}
