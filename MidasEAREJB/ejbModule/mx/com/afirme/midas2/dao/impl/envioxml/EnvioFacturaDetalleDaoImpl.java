package mx.com.afirme.midas2.dao.impl.envioxml;

import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.envioxml.EnvioFacturaDetalleDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;

@Stateless
public class EnvioFacturaDetalleDaoImpl extends EntidadDaoImpl implements EnvioFacturaDetalleDao {
	
	public EnvioFacturaDet guardarDetalleEnvio (EnvioFacturaDet envioFacturaDet) {
		
		if (envioFacturaDet.getIdEnvioDet() == null) {	
			envioFacturaDet.setIdEnvioDet((Long) persistAndReturnId(envioFacturaDet));
		}
		return envioFacturaDet;	
	}
	
	
	public void guardarDetalleEnvio (List<EnvioFacturaDet> envioFacturaDets) {
		
		for(EnvioFacturaDet envioFacturaDet : envioFacturaDets){
			this.guardarDetalleEnvio(envioFacturaDet);
		}		
	}
		
}

