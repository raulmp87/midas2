package mx.com.afirme.midas2.dao.impl.reportes.historial.cobranza;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.reportes.historial.cobranza.ReporteHistorialCobranzaDAO;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteHistorialCobranzaDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless 
public class ReporteHistorialCobranzaDAOImpl implements Serializable, ReporteHistorialCobranzaDAO {

	private static final long serialVersionUID = 1L;
	private static Logger log = LoggerFactory.getLogger(ReporteHistorialCobranzaDAOImpl.class);	
	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<ReporteHistorialCobranzaDTO> consultarHistPorAgente(Long idAgente) {
		List<ReporteHistorialCobranzaDTO> historial = new ArrayList<ReporteHistorialCobranzaDTO>();
		try {
			StoredProcedureHelper storedProcedureHelper = new StoredProcedureHelper(CONSULTA_HISTORIAL_COBRANZA_POR_AGENTE, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedProcedureHelper.estableceMapeoResultados(ReporteHistorialCobranzaDTO.class.getCanonicalName(), ATRIBUTOS_CLASE_HISTORIAL_REPORTE, CAMPOS_CURSOR_HISTORIAL);
			
			storedProcedureHelper.estableceParametro("p_idAgente", idAgente);
			
			historial = storedProcedureHelper.obtieneListaResultados();
		} catch (Exception e) {
			log.error("Ocurrio una excepcion en consultarHistPorAgente", e);
		}
		return historial;
	}
	
	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}

}
