package mx.com.afirme.midas2.dao.impl.condicionesGenerales;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.condicionesGenerales.CgCentroDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgCentro;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class CgCentroDaoImpl extends JpaDao<Long, CgCentro> implements CgCentroDao  {

	@SuppressWarnings("unchecked")
	@Override
	public List<CgCentro> findByFilter( CgCentro cgCentro ) throws SQLException,
			Exception {
		
		String where="";
		List<CgCentro> list = new ArrayList<CgCentro>();
		if ( cgCentro != null && cgCentro.getGerencia() != null && cgCentro.getGerencia().getDescripcion() != null && !cgCentro.getGerencia().getDescripcion().trim().equals("")){
			where = "where upper(cgC.gerencia.descripcion) like upper('%"+cgCentro.getGerencia().getDescripcion().trim()+"%') ";
		}
		
		String queryString = "" +
		"" +
		"select cgC " +
		"from CgCentro cgC " +
		where +
		"order by cgC.gerencia.descripcion asc ";

		Query query = entityManager.createQuery(queryString);		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list = (List<CgCentro>)query.getResultList();
		
		return list;
	}

}
