package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.fuerzaventa.EjecutivoDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.dao.impl.domicilio.DomicilioUtils;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote.TipoAccionFuerzaVenta;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.apache.commons.beanutils.BeanUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;
@Stateless
public class EjecutivoDaoImpl extends EntidadHistoricoDaoImpl implements EjecutivoDao{
	private DomicilioFacadeRemote domicilioFacade;
	private ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade;
	private ValorCatalogoAgentesService catalogoService;
	
	@Override
	public List<EjecutivoView> getList(boolean onlyActive){
		List<EjecutivoView> lista=new ArrayList<EjecutivoView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Ejecutivo filtro=new Ejecutivo();
		if(onlyActive){
			filtro.setClaveEstatus(1L);//Mostrar unicamnete los activos.
		}
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT DISTINCT entidad.id as id,entidad.idEjecutivo as idEjecutivo,persona.nombrecompleto as nombreCompleto ");
		queryString.append(" FROM MIDAS.toejecutivo entidad ");
		queryString.append(" INNER JOIN MIDAS.VW_PERSONA persona on persona.idpersona = entidad.IDPERSONA ");
		queryString.append(" where ");
		if(isNotNull(filtro)){
			int index=1;
			if(filtro.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtro.getClaveEstatus());
				index++;
			}
			String finalQuery=getQueryString(queryString)+" order by persona.nombrecompleto asc ";
			Query query=entityManager.createNativeQuery(finalQuery,EjecutivoView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			lista=query.getResultList();
		}
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EjecutivoView> findByFiltersView(Ejecutivo filtroEjecutivo){
		List<EjecutivoView> lista=new ArrayList<EjecutivoView>();
		//Si trae datos de fecha entonces busca por medio de sql, sino con jpql
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Domicilio domicilio=(isNotNull(filtroEjecutivo) && isNotNull(filtroEjecutivo.getDomicilio()))?filtroEjecutivo.getDomicilio():null;
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT /*+INDEX(gerencia TOGERENCIA_PK)*/ DISTINCT entidad.id as id,entidad.idEjecutivo as idEjecutivo,gerencia.descripcion as descripcion,gerencia.id as gerencia_id,persona.nombrecompleto as nombreCompleto ");
		queryString.append(" ,entidad.claveEstatus as claveestatus,persona.idPersona as idPersona,cat.id as idTipoEjecutivo,cat.valor as tipoEjecutivo ");
		queryString.append(" FROM MIDAS.toejecutivo entidad ");
		queryString.append(" INNER JOIN MIDAS.toGerencia gerencia on entidad.gerencia_id = gerencia.id ");
		queryString.append(" INNER JOIN MIDAS.VW_PERSONA persona on persona.idpersona = entidad.IDPERSONA ");
		queryString.append(" LEFT JOIN MIDAS.toValorCatalogoAgentes cat on( cat.id=entidad.idTipoEjecutivo ) ");
		boolean domicilioEmpty=DomicilioUtils.isEmptyAddress(domicilio);
		if(!domicilioEmpty){
			queryString.append(" LEFT JOIN MIDAS.VW_DOMICILIO_IMPRESION dom ON (dom.idDomicilio=entidad.idDomicilio and dom.idpersona=persona.idpersona) ");
		}
		queryString.append(" INNER JOIN MIDAS.TCGRUPOACTUALIZACIONAGENTES grupo on(grupo.id=30) ");
		queryString.append(" LEFT JOIN MIDAS.TLACTUALIZACIONAGENTES act on(act.grupoActualizacionAgentes_id=grupo.id and act.idRegistro=entidad.id and (act.tipoMovimiento is null or act.tipoMovimiento like 'ALTA' )) ");
		queryString.append(" where ");
		if(isNotNull(filtroEjecutivo)){
			int index=1;
			if(filtroEjecutivo.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtroEjecutivo.getClaveEstatus());
				index++;
			}
			if(isNotNull(filtroEjecutivo.getId())){
				addCondition(queryString, " entidad.id=? ");
				params.put(index, filtroEjecutivo.getId());
				index++;
			}else if(isNotNull(filtroEjecutivo.getIdEjecutivo())){
				addCondition(queryString, " entidad.idEjecutivo=? ");
				params.put(index, filtroEjecutivo.getIdEjecutivo());
				index++;
			}else{
				
				Gerencia gerencia=filtroEjecutivo.getGerencia();
				if(gerencia!=null){
					if(gerencia.getDescripcion()!=null && !gerencia.getDescripcion().isEmpty()){
						addCondition(queryString, "UPPER(gerencia.descripcion) like UPPER(?)");
						params.put(index, "%"+gerencia.getDescripcion()+"%");
						index++;
					}
					if(gerencia.getId()!=null){
						addCondition(queryString, "gerencia.id=?");
						params.put(index, gerencia.getId());
						index++;
					}
				}
				
				Persona personaResponsable=filtroEjecutivo.getPersonaResponsable();
				if(personaResponsable!=null){
					if(personaResponsable.getNombreCompleto()!=null && !personaResponsable.getNombreCompleto().isEmpty()){
						addCondition(queryString, "UPPER(persona.nombreCompleto) like UPPER(?)");
						params.put(index, "%"+personaResponsable.getNombreCompleto()+"%");
						index++;
					}
				}
				
				if(isNotNull(domicilio)){
					if(isValid(domicilio.getClaveEstado())){
						addCondition(queryString, " dom.claveEstado like UPPER(?)");
						params.put(index,  domicilio.getClaveEstado());
						index++;
					}
					if(isValid(domicilio.getClaveCiudad())){
						addCondition(queryString, " dom.claveCiudad like UPPER(?)");
						params.put(index,  domicilio.getClaveCiudad());
						index++;
					}
					if(isValid(domicilio.getCodigoPostal())){
						addCondition(queryString, " TRIM(dom.codigoPostal) like TRIM(UPPER(?))");
						params.put(index,  domicilio.getCodigoPostal());
						index++;
					}
					if(isValid(domicilio.getNombreColonia())){
						addCondition(queryString, " TRIM(dom.colonia) like TRIM(UPPER(?))||'%'");
						params.put(index,  domicilio.getNombreColonia());
						index++;
					}
				}
				if(isNotNull(filtroEjecutivo.getFechaInicio())){
					Date fecha=filtroEjecutivo.getFechaInicio();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");						
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroEjecutivo.getFechaFin())){
					Date fecha=filtroEjecutivo.getFechaFin();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");						
					params.put(index,  fechaString);
					index++;
				}
			}
			if(params.isEmpty()){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,EjecutivoView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			query.setMaxResults(100);
			lista=query.getResultList();
		}
		return lista;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<Ejecutivo> findByFilters(Ejecutivo filtroEjecutivo, String fechaHistorico) {
		
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;
		
        String entityDomicilio = "domicilio";
		
		if(isHistorico)
		{
			entityDomicilio = "domicilioHistorico";	
		}
		
		List<Ejecutivo> lista=new ArrayList<Ejecutivo>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from Ejecutivo model left join fetch model.gerencia left join fetch model." + entityDomicilio + " left join fetch model.personaResponsable ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtroEjecutivo!=null){
			Gerencia gerencia=filtroEjecutivo.getGerencia();
			if(gerencia!=null){
				if(gerencia.getDescripcion()!=null && !gerencia.getDescripcion().isEmpty()){
					addCondition(queryString, "UPPER(model.gerencia.descripcion) like UPPER(:nombreGerencia)");
					params.put("nombreGerencia", "%"+gerencia.getDescripcion()+"%");
				}
				if(gerencia.getId()!=null){
					addCondition(queryString, "model.gerencia.id=:idGerencia");
					params.put("idGerencia", gerencia.getId());
				}
			}
//			if(filtroEjecutivo.getDescripcion()!=null && !filtroEjecutivo.getDescripcion().isEmpty()){
//				addCondition(queryString, "model.descripcion like :descripcion");
//				params.put("descripcion", filtroEjecutivo.getDescripcion()+"%");
//			}
			Domicilio domicilio=filtroEjecutivo.getDomicilio();
			if(domicilio!=null){
				if(domicilio.getClaveEstado()!=null && !domicilio.getClaveEstado().isEmpty()){
					addCondition(queryString, "model.domicilio.claveEstado=:claveEstado");
					params.put("claveEstado", domicilio.getClaveEstado());
				}
				if(domicilio.getClaveCiudad()!=null && !domicilio.getClaveCiudad().isEmpty()){
					addCondition(queryString, "model.domicilio.claveCiudad=:claveCiudad");
					params.put("claveCiudad", domicilio.getClaveCiudad());
				}
				if(domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().isEmpty()){
					addCondition(queryString, "model.domicilio.nombreColonia like :nombreColonia");
					params.put("nombreColonia", domicilio.getNombreColonia()+"%");
				}
				if(domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().isEmpty()){
					addCondition(queryString, "model.domicilio.codigoPostal like :codigoPostal");
					params.put("codigoPostal", domicilio.getCodigoPostal()+"%");
				}
			}
			Persona personaResponsable=filtroEjecutivo.getPersonaResponsable();
			if(personaResponsable!=null){
				if(personaResponsable.getNombreCompleto()!=null && !personaResponsable.getNombreCompleto().isEmpty()){
					addCondition(queryString, "UPPER(model.personaResponsable.nombreCompleto) like UPPER(:nombreResponsable)");
					params.put("nombreResponsable", "%"+personaResponsable.getNombreCompleto()+"%");
				}
			}
			if(filtroEjecutivo.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtroEjecutivo.getId());
			}
			if(filtroEjecutivo.getClaveEstatus()!=null){
				addCondition(queryString, "model.claveEstatus=:claveEstatus");
				params.put("claveEstatus", filtroEjecutivo.getClaveEstatus());
			}
		}
		
		if(!isHistorico)
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
//			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			query.setHint ("eclipselink.sql.hint", "/*+ PUSH_PRED(t3)*/ ");
			lista=query.getResultList();			
		}
		else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
			
			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString).toString());
			historicalQuery.addArgument("id");
			historicalQuery.addArgumentValue(filtroEjecutivo.getId());
			historicalQuery.refreshIdentityMapResult();
			lista = (List<Ejecutivo>)historicalSession.executeQuery(historicalQuery);			
		}	
		
		return lista;
	}
	
	
	@Override
	public List<Ejecutivo> findAllEjecutivoResponsable() {
		List<Ejecutivo> lista=new ArrayList<Ejecutivo>(1);
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("SELECT t1.id, t1.gerencia_id, t1.idejecutivo, t1.idtipoejecutivo, t1.idpersona, t1.claveestatus, t1.iddomicilio, ");
		queryString.append("	CASE WHEN t3.cve_per_juridica = 'PF' THEN 1 ELSE 2 END AS claveTipoPersona, t3.nombre as nombrecompleto,  ");
		queryString.append("	pf.nombre_persona AS nombre, pf.apellido_paterno AS apellidoPaterno, pf.apellido_materno AS apellidoMaterno, rfc.siglas_rfc || rfc.f_rfc || rfc.homoclave_rfc AS codigoRfc ");
		queryString.append(" FROM MIDAS.TOEJECUTIVO t1, SEYCOS.RFC rfc, SEYCOS.PERSONA t3 ");
		queryString.append(" LEFT JOIN SEYCOS.PERSONA_FISICA pf ON t3.ID_PERSONA = pf.ID_PERSONA");
		queryString.append(" WHERE t3.ID_PERSONA = t1.IDPERSONA ");
		queryString.append(" AND t3.ID_PERSONA = rfc.ID_PERSONA ");
		queryString.append(" order by t3.nombre ");
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		Object result=query.getResultList();
		if(result instanceof List)  {
			List<Object> listaResultados = (List<Object>) result;				
			for(Object object : listaResultados){
				Object[] singleResult = (Object[]) object;
				
				Ejecutivo ejecutivo = new Ejecutivo();
				Persona responsable = new Persona();

				Long id = null;
				if(singleResult[0] instanceof BigDecimal){
					BigDecimal idBd = (BigDecimal)singleResult[0];
					id = idBd.longValue();
				} else if (singleResult[0] instanceof Long){
					id = (Long)singleResult[0];
				}else if (singleResult[0] instanceof Double){
					Double idD = (Double)singleResult[0];
					id = idD.longValue();
				}
				
				ejecutivo.setId(id);
				
				Gerencia gerencia = new Gerencia();
				Long idGerencia = null;
				if(singleResult[1] instanceof BigDecimal){
					BigDecimal idBd = (BigDecimal)singleResult[1];
					idGerencia = idBd.longValue();
				} else if (singleResult[1] instanceof Long){
					idGerencia = (Long)singleResult[1];
				}else if (singleResult[1] instanceof Double){
					Double idD = (Double)singleResult[1];
					idGerencia = idD.longValue();
				}
				gerencia.setId(idGerencia);
				ejecutivo.setGerencia(gerencia);
				
				Long idEjecutivo = null;
				if(singleResult[2] instanceof BigDecimal){
					BigDecimal idBd = (BigDecimal)singleResult[2];
					idEjecutivo = idBd.longValue();
				} else if (singleResult[2] instanceof Long){
					idEjecutivo = (Long)singleResult[2];
				}else if (singleResult[2] instanceof Double){
					Double idD = (Double)singleResult[2];
					idEjecutivo = idD.longValue();
				}
				ejecutivo.setIdEjecutivo(idEjecutivo);
				
				ValorCatalogoAgentes tipoEjecutivo = new ValorCatalogoAgentes();
				Long idTipoEjecutivo = null;
				if(singleResult[3] instanceof BigDecimal){
					BigDecimal idBd = (BigDecimal)singleResult[3];
					idTipoEjecutivo = idBd.longValue();
				} else if (singleResult[3] instanceof Long){
					idTipoEjecutivo = (Long)singleResult[3];
				}else if (singleResult[3] instanceof Double){
					Double idD = (Double)singleResult[3];
					idTipoEjecutivo = idD.longValue();
				}
				tipoEjecutivo.setId(idTipoEjecutivo);
				ejecutivo.setTipoEjecutivo(tipoEjecutivo);
				
				Long idPersona = null;
				if(singleResult[4] instanceof BigDecimal){
					BigDecimal idBd = (BigDecimal)singleResult[4];
					idPersona = idBd.longValue();
				} else if (singleResult[4] instanceof Long){
					idPersona = (Long)singleResult[4];
				}else if (singleResult[4] instanceof Double){
					Double idD = (Double)singleResult[4];
					idPersona = idD.longValue();
				}
				responsable.setIdPersona(idPersona);
				
				Long claveEstatus = null;
				if(singleResult[5] instanceof BigDecimal){
					BigDecimal idBd = (BigDecimal)singleResult[5];
					claveEstatus = idBd.longValue();
				} else if (singleResult[5] instanceof Long){
					claveEstatus = (Long)singleResult[5];
				}else if (singleResult[5] instanceof Double){
					Double idD = (Double)singleResult[5];
					claveEstatus = idD.longValue();
				}				
				ejecutivo.setClaveEstatus(claveEstatus);
				
				Domicilio domicilio = new Domicilio();
				Long idDomicilio = null;
				if(singleResult[6] instanceof BigDecimal){
					BigDecimal idBd = (BigDecimal)singleResult[6];
					idDomicilio = idBd.longValue();
				} else if (singleResult[6] instanceof Long){
					idDomicilio = (Long)singleResult[6];
				}else if (singleResult[6] instanceof Double){
					Double idD = (Double)singleResult[6];
					idDomicilio = idD.longValue();
				}
				DomicilioPk pkDomicilio=new DomicilioPk();
				pkDomicilio.setIdDomicilio(idDomicilio);
				domicilio.setIdDomicilio(pkDomicilio);
				ejecutivo.setDomicilio(domicilio);
				
				Long claveTipoPersona = null;
				if(singleResult[7] instanceof BigDecimal){
					BigDecimal idBd = (BigDecimal)singleResult[7];
					claveTipoPersona = idBd.longValue();
				} else if (singleResult[7] instanceof Long){
					claveTipoPersona = (Long)singleResult[7];
				}else if (singleResult[7] instanceof Double){
					Double idD = (Double)singleResult[7];
					claveTipoPersona = idD.longValue();
				}
				responsable.setClaveTipoPersona(claveTipoPersona);
				

				try{
					String nombreCompleto = singleResult[8].toString();
					responsable.setNombreCompleto(nombreCompleto);
				}catch(Exception e){}
				
				try{
					String nombre = singleResult[9].toString();
					responsable.setNombre(nombre);
				}catch(Exception e){}
				
				try{
					String apellidoPaterno = singleResult[10].toString();
					responsable.setApellidoPaterno(apellidoPaterno);
				}catch(Exception e){}
				
				try{
					String apellidoMaterno = singleResult[11].toString();
					responsable.setApellidoMaterno(apellidoMaterno);
				}catch(Exception e){}
				
				try{
					String rfc = singleResult[12].toString();
					responsable.setRfc(rfc);
				}catch(Exception e){}
				
				ejecutivo.setPersonaResponsable(responsable);
				lista.add(ejecutivo);
			}
		}
		
		return lista;
	}

	@Override
	public List<Ejecutivo> findByGerencia(Long idGerencia) {
		Ejecutivo filtroEjecutivo=new Ejecutivo();
		if(idGerencia!=null){
			Gerencia gerencia=new Gerencia();
			gerencia.setId(idGerencia);			
			filtroEjecutivo.setClaveEstatus(1l);//Solo activos
			filtroEjecutivo.setGerencia(gerencia);		
		}
		return findByFilters(filtroEjecutivo, null);
	}
	
	@Override
	public void unsubscribe(Ejecutivo ejecutivo) throws Exception {
		if(ejecutivo==null || ejecutivo.getId()==null){
			throw new Exception("Ejecutivo is null");
		}	ejecutivo.setClaveEstatus(0l);
			validaRelacion(ejecutivo);
			ejecutivo=findById(Ejecutivo.class,ejecutivo.getId());
			ejecutivo.setClaveEstatus(0l);
			update(ejecutivo);//Se actualiza su estatus
	}

	@Override
	public Ejecutivo saveFull(Ejecutivo ejecutivo) throws Exception {
		if(ejecutivo==null){
			throw new Exception("Favor de proporcionar el ejecutivo");
		}
		// Valida que el ejecutivo no tenga asociadas promotorias
		validaRelacion(ejecutivo);
		
		boolean esUpdate=false;
			Domicilio domicilio=ejecutivo.getDomicilio();
			Domicilio persistAddress=null;
			DomicilioPk pk=new DomicilioPk();
			Gerencia persistGerencia = ejecutivo.getGerencia();
			Ejecutivo jecutivoB = new Ejecutivo();
			jecutivoB = merge(jecutivoB,ejecutivo);
			Long idDomicilio=null;
			//Si no existe el domicilio entonces se inserta
			if(domicilio!=null){
				Long idPersona=(ejecutivo.getPersonaResponsable()!=null && ejecutivo.getPersonaResponsable().getIdPersona()!=null)?ejecutivo.getPersonaResponsable().getIdPersona():null;
				domicilio.getIdDomicilio().setIdPersona(idPersona);
				domicilio.setIdPersona(idPersona);
//				idDomicilio=domicilioFacade.save(domicilio, null);
				idDomicilio=(isNotNull(domicilio) && isNotNull(domicilio.getIdDomicilio()))?domicilio.getIdDomicilio().getIdDomicilio():null;
				pk.setIdDomicilio(idDomicilio);
				pk.setIdPersona(idPersona);
				persistAddress=entityManager.getReference(Domicilio.class, pk);
				if(idPersona != null)
				{
					Persona contratante = new Persona(); 
					contratante = this.findById(Persona.class, idPersona);
					jecutivoB.setPersonaResponsable(contratante);			
				}
			}
			persistGerencia = entityManager.getReference(Gerencia.class, persistGerencia.getId());
			if(ejecutivo.getId()!=null){
				ejecutivo = entityManager.getReference(Ejecutivo.class, ejecutivo.getId());
				ejecutivo = merge(ejecutivo,jecutivoB);
			}
			ejecutivo.setDomicilio(persistAddress);
			ejecutivo.setGerencia(persistGerencia);
		if(ejecutivo.getId()!=null){
			esUpdate=true;
			update(ejecutivo);
		}else{
			persist(ejecutivo);
		}
		//Se agrega esta parte para que el objeto tipoEjecutivo tenga el atributo clave, ya que incialmente solo se tiene el id del tipo de ejecutivo
		ValorCatalogoAgentes tipoEjecutivo=catalogoService.loadById(ejecutivo.getTipoEjecutivo());
		//una vez que se recupera se setea al ejecutivo para obtener su valor real que va en seycos.
		ejecutivo.setTipoEjecutivo(tipoEjecutivo);
		
		if(!esUpdate){//Entonces es insert y debe de actualizar el id del ejecutivo con el id de la secuencia.
			ejecutivo.setDomicilio(persistAddress);
			ejecutivo.setGerencia(persistGerencia);
			ejecutivo.setIdEjecutivo(ejecutivo.getId());
			update(ejecutivo);
		}
		replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(ejecutivo,TipoAccionFuerzaVenta.GUARDAR);
		return ejecutivo;
	}
	/**
	 * Carga la lista de los ejecutivos
	 * @param ejecutivo
	 * @return
	 */
	@Override
	public Ejecutivo loadById(Ejecutivo ejecutivo, String fechaHistorico){
		if(ejecutivo!=null && ejecutivo.getId()!=null){
			Ejecutivo filtro=new Ejecutivo();
			filtro.setId(ejecutivo.getId());
			List<Ejecutivo> list=findByFilters(filtro, fechaHistorico);
			if(list!=null && !list.isEmpty()){
				
				ejecutivo = list.get(0);
				
				if (fechaHistorico != null && !fechaHistorico.isEmpty()) {
					Domicilio domicilioTemp = new Domicilio();
					try {
						BeanUtils.copyProperties(domicilioTemp,
								ejecutivo.getDomicilioHistorico());
					} catch (Exception e) {
						e.printStackTrace();
					}

					ejecutivo.setDomicilio(domicilioTemp);
				}				
			}
		}		
		
		return ejecutivo;
	}
	@Override
	public List<EjecutivoView> findByGerenciaLightWeight(Long idParent){
		List<EjecutivoView> list=new ArrayList<EjecutivoView>();
		if(isNotNull(idParent)){
			Gerencia parent=new Gerencia();
			parent.setId(idParent);
			Ejecutivo filtro=new Ejecutivo();
			filtro.setClaveEstatus(1l);//Solo activos
			filtro.setGerencia(parent);
			return findByFiltersView(filtro);
		}
		return list;
	}
	@Override
	public List<EjecutivoView> findEjecutivosConGerenciasExcluyentes(List<Long> ejecutivos,List<Long> gerenciasExcluyentes,List<Long> centrosExcluyentes){
		List<EjecutivoView> list=new ArrayList<EjecutivoView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Gerencia filtro=new Gerencia();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT DISTINCT entidad.id as id,entidad.idEjecutivo as idEjecutivo,persona.nombrecompleto as nombreCompleto ");
		queryString.append(" FROM MIDAS.toejecutivo entidad ");
		queryString.append(" INNER JOIN MIDAS.VW_PERSONA persona on persona.idpersona = entidad.IDPERSONA ");
		queryString.append(" INNER JOIN MIDAS.toGerencia gerencia on entidad.gerencia_id = gerencia.id ");
		queryString.append(" INNER JOIN MIDAS.toCentroOperacion centro on gerencia.centroOperacion_id = centro.id ");
		queryString.append(" where ");
		if(isNotNull(filtro)){
			int index=1;
			if(filtro.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtro.getClaveEstatus());
				index++;
			}
			if(!isEmptyList(ejecutivos)){
				queryString.append(" entidad.id in ( ");
				StringBuilder conditionalList=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idEjecutivo:ejecutivos){
					if(isNotNull(idEjecutivo)){
						if(i<(ejecutivos.size()-1)){
							conditionalList.append(idEjecutivo+",");
						}else{
							conditionalList.append(idEjecutivo);
						}
					}
					i++;
				}
//				params.put(index, conditionalList.toString());
//				index++;
				queryString.append(conditionalList.toString()+") or ");
			}
			if(!isEmptyList(gerenciasExcluyentes)){
				queryString.append(" gerencia.id  in ( ");
				StringBuilder listaExcluyente=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idGerencia:gerenciasExcluyentes){
					if(isNotNull(idGerencia)){
						if(i<(gerenciasExcluyentes.size()-1)){
							listaExcluyente.append(idGerencia+",");
						}else{
							listaExcluyente.append(idGerencia);
						}
					}
					i++;
				}
//				params.put(index, listaExcluyente.toString());
//				index++;
				queryString.append(listaExcluyente.toString()+") or ");
			}
			if(!isEmptyList(centrosExcluyentes)){
//				addCondition(queryString, " centro.id not in (?) ");
				queryString.append(" centro.id  in ( ");
				StringBuilder listaExcluyente=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idCentro:centrosExcluyentes){
					if(isNotNull(idCentro) && isNotNull(idCentro)){
						if(i<(centrosExcluyentes.size()-1)){
							listaExcluyente.append(idCentro+",");
						}else{
							listaExcluyente.append(idCentro);
						}
					}
					i++;
				}
//				params.put(index, listaExcluyente.toString());
//				index++;
				queryString.append(listaExcluyente.toString()+") or ");
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,EjecutivoView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
		}
		return list;	
	}
	
	/***Sets && gets***********************************************************/
	@EJB
	public void setDomicilioFacade(DomicilioFacadeRemote domicilioFacade) {
		this.domicilioFacade = domicilioFacade;
	}
	@EJB
	public void setReplicarFuerzaVentaSeycosFacade(ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade) {
		this.replicarFuerzaVentaSeycosFacade = replicarFuerzaVentaSeycosFacade;
	}
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	/**
	 * Valida que la entidad no tenga asociado
	 * un dato 
	 * @param ejecutivo
	 * @throws Exception
	 */
	private void validaRelacion(Ejecutivo ejecutivo) throws Exception {
		if (ejecutivo.getClaveEstatus().equals(0l)) {
			List<Promotoria> checarRelacionEjecutivo = findByProperty(
					Promotoria.class, "ejecutivo.id", ejecutivo.getId());
			if (checarRelacionEjecutivo != null && !checarRelacionEjecutivo.isEmpty()) {
				throw new Exception(
						"El ejecutivo est\u00E1 relacionado con una Promotor\u00EDa");
			}
		}
	}
	
	private Ejecutivo merge(Ejecutivo oldObject, Ejecutivo newObject ){
		oldObject.setClaveEstatus(newObject.getClaveEstatus());
		oldObject.setDomicilio(newObject.getDomicilio());
		oldObject.setDomicilioHistorico(newObject.getDomicilioHistorico());
		oldObject.setFechaFin(newObject.getFechaFin());
		oldObject.setFechaInicio(newObject.getFechaInicio());
		oldObject.setGerencia(newObject.getGerencia());
		oldObject.setPersonaResponsable(newObject.getPersonaResponsable());
		oldObject.setTipoEjecutivo(entityManager.getReference(ValorCatalogoAgentes.class, newObject.getTipoEjecutivo().getId()));
		
		return oldObject;
	}
}
