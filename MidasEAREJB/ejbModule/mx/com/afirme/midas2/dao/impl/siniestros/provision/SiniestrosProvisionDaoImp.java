package mx.com.afirme.midas2.dao.impl.siniestros.provision;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.provision.SiniestrosProvisionDao;
import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.service.impl.siniestros.recuperacion.RecuperacionSalvamentoServiceImpl;

@Stateless
public class SiniestrosProvisionDaoImp extends EntidadDaoImpl  implements SiniestrosProvisionDao {

	private static final Logger LOG = Logger.getLogger(SiniestrosProvisionDaoImp.class);
	
	@Override
	public List<MovimientoProvisionSiniestros> obtenerMovimientosProvisionadosPorPreporte(Long reporteId,MovimientoProvisionSiniestros.Origen origen){
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("reporteId", reporteId);
		parameters.put("origen", origen.getValue());
		parameters.put("provisionActiva", Boolean.TRUE);
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros( ");
		queryString.append(" movimiento.id, movimiento.idRefrencia, movimiento.origen, movimiento.causa, movimiento.montoMovimiento, movimiento.provisionActual,movimiento.estaProvisionado ) ");
		queryString.append(" FROM MovimientoProvisionSiniestros movimiento ");
		queryString.append(" JOIN EstimacionCoberturaReporteCabina estimacion ON ( estimacion.id = movimiento.idRefrencia ) ");
		queryString.append(" WHERE  movimiento.origen = :origen ");
		queryString.append(" AND    movimiento.estaProvisionado = :provisionActiva ");
		queryString.append(" AND    estimacion.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id = :reporteId ");
		List<MovimientoProvisionSiniestros>  movimienntos = this.executeQueryMultipleResult(queryString.toString(), parameters);
		return movimienntos;
	}
	
	@Override
	public BigDecimal obtenerSumaGruasPorEstimacion( Long estimacionId ){
		
		BigDecimal totalMovimientos = BigDecimal.ZERO;
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("origen"      , MovimientoProvisionSiniestros.Origen.PASE.getValue());
		parameters.put("causa"       , MovimientoProvisionSiniestros.CAUSA.GRUA.getValue() );
		parameters.put("estimacionId", estimacionId);
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT  SUM(movimiento.montoMovimiento) total ");
		queryString.append(" FROM MovimientoProvisionSiniestros movimiento ");
		queryString.append(" JOIN EstimacionCoberturaReporteCabina estimacion ON ( estimacion.id = movimiento.idRefrencia ) ");
		queryString.append(" WHERE  movimiento.origen = :origen ");
		queryString.append(" AND    movimiento.idRefrencia = :estimacionId ");
		queryString.append(" AND    movimiento.causa = :causa ");
		
		
		Object resultado =  this.executeQuerySimpleResult(queryString.toString(), parameters);
		if( resultado != null){
			return (BigDecimal) resultado;
		}else{
			return totalMovimientos;
		}
				
	}
	
	@Override	
	public BigDecimal obtenerSumaRecuperacionesRecuperada( Long estimacionId ){
		
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("estimacionId", estimacionId);
		parameters.put("estatus", Recuperacion.EstatusRecuperacion.RECUPERADO.getValue());
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT  SUM(recuperacion.importe) total ");
		queryString.append(" FROM RecuperacionCompania recuperacion ");
		queryString.append(" JOIN CoberturaRecuperacion cobertura ON ( cobertura.recuperacion.id = recuperacion.id ) ");
		queryString.append(" JOIN PaseRecuperacion pases ON ( pases.coberturaRecuperacion.id = cobertura.id) ");
		queryString.append(" WHERE  recuperacion.estatus = :estatus ");
		queryString.append(" AND    pases.estimacionCobertura.id = :estimacionId ");
		
		
		Object resultado =  this.executeQuerySimpleResult(queryString.toString(), parameters);
		if( resultado != null){
			return (BigDecimal) resultado;
		}else{
			return BigDecimal.ZERO;
		}
		
	}

	@Override	
	public RecuperacionCompania obtenerRecuperacionCiaPorEstimacion( Long estimacionId){
		
		List<RecuperacionCompania> lRecuperacion = null;
		RecuperacionCompania recuperacion = null;
		try{
			
			Map<String, Object> parameters = new HashMap<String, Object>(); 
			parameters.put("estimacionId", estimacionId);
			
			StringBuilder queryString = new StringBuilder();
			queryString.append(" SELECT  recuperacion ");
			queryString.append(" FROM RecuperacionCompania recuperacion ");
			queryString.append(" JOIN CoberturaRecuperacion cobertura ON ( cobertura.recuperacion.id = recuperacion.id ) ");
			queryString.append(" JOIN PaseRecuperacion pases ON ( pases.coberturaRecuperacion.id = cobertura.id) ");
			queryString.append(" WHERE ");
			queryString.append(" pases.estimacionCobertura.id = :estimacionId ");
			
			lRecuperacion = this.executeQueryMultipleResult(queryString.toString(), parameters);
			
			if( !lRecuperacion.isEmpty() ){
				recuperacion = lRecuperacion.get(0);
			}
		
		}catch(Exception e){
			LOG.error("SiniestrosProvisionDaoImp error - obtenerRecuperacionCiaPorEstimacion", e);
		}
		
		return recuperacion;
	}
	
	@Override	
	public BigDecimal obtenerSumaPiezas( Long estimacionId ){
		
		Map<String, Object> parameters = new HashMap<String, Object>(); 
		parameters.put("estimacionId", estimacionId);
		parameters.put("recuperado", Recuperacion.EstatusRecuperacion.RECUPERADO.getValue());
		
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT  SUM(recuperacion.montoPiezas) total ");
		queryString.append(" FROM RecuperacionCompania recuperacion ");
		queryString.append(" JOIN CoberturaRecuperacion cobertura ON ( cobertura.recuperacion.id = recuperacion.id ) ");
		queryString.append(" JOIN PaseRecuperacion pases ON ( pases.coberturaRecuperacion.id = cobertura.id) ");
		queryString.append(" WHERE ");
		queryString.append(" recuperacion.estatus = :recuperado ");
		queryString.append(" AND    pases.estimacionCobertura.id = :estimacionId ");
		
		
		Object resultado =  this.executeQuerySimpleResult(queryString.toString(), parameters);
		if( resultado != null){
			return (BigDecimal) resultado;
		}else{
			return BigDecimal.ZERO;
		}
		
	}
	
	
	
	
}
