/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaLineaNegocioDao;
import mx.com.afirme.midas2.domain.compensaciones.CaLineaNegocio;

import org.apache.log4j.Logger;

@Stateless
public class CaLineaNegocioDaoImpl implements CaLineaNegocioDao {
	public static final String CONTRAPRESTACION = "contraprestacion";
	public static final String NEGOCIO_ID = "negocioId";
	public static final String VALORPORCENTAJE = "valorPorcentaje";
	public static final String VALORID = "valorId";
	public static final String VALORDESCRIPCION = "valorDescripcion";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaLineaNegocioDaoImpl.class);
	
	public void save(CaLineaNegocio entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaLineaNegocio entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaLineaNegocio.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.info("	::	[INF]	::	Error al eliminar CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaLineaNegocio update(CaLineaNegocio entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaLineaNegocio result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaLineaNegocio 	::		CaLineaNegocioDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaLineaNegocio findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaLineaNegocioDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaLineaNegocio instance = entityManager.find(CaLineaNegocio.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaLineaNegocioDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaLineaNegocioDaoImpl	::	findById	::	ERROR	::	",re);
	            return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<CaLineaNegocio> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaLineaNegocioDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaLineaNegocio model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaLineaNegocioDaoImpl	::	findByProperty	::	FIN	::	");
					return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaLineaNegocioDaoImpl	::	findByProperty	::	ERROR	::	",re);
				return null;
		}
	}
    public List<CaLineaNegocio> findByContraprestacion(Object contraprestacion
	) {
		return findByProperty(CONTRAPRESTACION, contraprestacion
		);
	}
	public List<CaLineaNegocio> findByNegocioId(Object negocioId
	) {
		return findByProperty(NEGOCIO_ID, negocioId
		);
	}
	
	public List<CaLineaNegocio> findByValorporcentaje(Object valorporcentaje
	) {
		return findByProperty(VALORPORCENTAJE, valorporcentaje
		);
	}
	
	public List<CaLineaNegocio> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	public List<CaLineaNegocio> findByValorid(Object valorid
	) {
		return findByProperty(VALORID, valorid
		);
	}
	public List<CaLineaNegocio> findByValordescripcion(Object valordescripcion
	) {
		return findByProperty(VALORDESCRIPCION, valordescripcion
		);
	}
	
	public List<CaLineaNegocio> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaLineaNegocio> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaLineaNegocioDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaLineaNegocio model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaLineaNegocioDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaLineaNegocioDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
	public CaLineaNegocio findByPropertyUniqueResult(String propertyName, Object value
	){
		CaLineaNegocio result;
	      LOGGER.info(" ::  [INF] ::  Buscando por Propiedad  ::    CaLineaNegocioDaoImpl  ::  findByPropertyUniqueResult  ::  INICIO  ::  ");
	      try {
	        final String queryString = "select model from CaLineaNegocio model where model." 
	          + propertyName + "= :propertyValue";
	      Query query = entityManager.createQuery(queryString);
	      query.setParameter("propertyValue", value);     
	      result = (CaLineaNegocio) query.getSingleResult();
	    } catch (RuntimeException re) {
	      LOGGER.error("  ::  [ERR] ::  Error al buscar por Propiedad   ::    CaLineaNegocioDaoImpl  ::  findByPropertyUniqueResult  ::  ERROR ::  ",re);
	      result = null;
	    }
	    LOGGER.info(" ::  [INF] ::  Se busco por Propiedad  ::    CaLineaNegocioDaoImpl  ::  findByPropertyUniqueResult  ::  FIN ::  ");
	    return result;    
		
	}
	public CaLineaNegocio findByNegocioIdValorId(Long idNegocio, String idValor	){
		LOGGER.info(" ::  [INF] ::  Buscando por idNegocio & idValor  ::    CaLineaNegocioDaoImpl ::  findByNegocioIdValorId  ::  INICIO  ::  ");
		CaLineaNegocio result = null;
		try {
			
			final String queryString = "select model from CaLineaNegocio model where model." 
		          						+ NEGOCIO_ID + "= :idNegocio and model." + VALORID + " = :idValor";
			Query query = entityManager.createQuery(queryString, CaLineaNegocio.class);
			query.setParameter("idNegocio", idNegocio);
			query.setParameter("idValor", idValor);
			LOGGER.info(" ::  [INF] ::  Error al buscar por idNegocio & idValor  ::    CaLineaNegocioDaoImpl ::  findByNegocioIdValorId  ::  FIN ::  ");
			result = (CaLineaNegocio) query.getSingleResult();
			} catch (Exception e) {
				LOGGER.error("  ::  [ERR] ::  Se busco por idNegocio & idValor  ::    CaLineaNegocioDaoImpl ::  findByNegocioIdValorId  ::  ERROR ::  ",e);
				result = null;
			}
			return result;
	}
	@SuppressWarnings("unchecked")
	public List<CaLineaNegocio> findByNegocioIdandContraprestacion(Long negocioId, boolean contraprestacion){
		List<CaLineaNegocio> result = null;
		LOGGER.info(" ::  [INF] ::  Buscando por idNegocio & Contraprestacion  ::    CaLineaNegocioDaoImpl ::  findByNegocioIdandContraprestacion  ::  INICIO  ::  ");
		try{
			final String queryString = "select model from CaLineaNegocio model where model." 
					+ NEGOCIO_ID + "= :idNegocio and model." + CONTRAPRESTACION + " = :contraprestacion";
			Query query = entityManager.createQuery(queryString, CaLineaNegocio.class);
			query.setParameter("idNegocio", negocioId);
			query.setParameter("contraprestacion", contraprestacion);
			result = query.getResultList();
		}catch (Exception e) {
			LOGGER.error("  ::  [ERR] ::  Error al buscar por idNegocio & Contraprestacion    ::    CaLineaNegocioDaoImpl ::  findByNegocioIdandContraprestacion  ::  ERROR ::  ",e);
			result = null;
		}
		LOGGER.info(" ::  [INF] ::  Se busco por idNegocio & Contraprestacion    ::    CaLineaNegocioDaoImpl ::  findByNegocioIdandContraprestacion  ::  FIN ::  ");
		return result;
	}
}
