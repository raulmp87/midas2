package mx.com.afirme.midas2.dao.impl.siniestros.cabina.reporteCabina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.MovimientoSiniestroDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO;



import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class MovimientoSiniestroDaoImpl extends EntidadDaoImpl implements MovimientoSiniestroDao {

	@EJB
	private EntidadDao entidadDao;
	
	private enum TIPO_BUSQUEDA{IMPORTE, DTO, ENTITY};
	
	@Override
	public BigDecimal obtenerMontoSiniestrosAnteriores(Long idCoberturaReporte) {
		String spName = "MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerMontoAnteriores"; 
        StoredProcedureHelper storedHelper 			= null;
        String propiedades 							= "";
        String columnasBaseDatos 						= "";
        BigDecimal sumaAcumulada = BigDecimal.ZERO;
        CoberturaReporteCabina coberturaReporteCabina =  entidadDao.findById( CoberturaReporteCabina.class, idCoberturaReporte);
        
        try {
      	  
      	  propiedades = "importeMovimiento";
      	  columnasBaseDatos = "SUMADEHISTORICO";
            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(MovimientoCoberturaSiniestro.class.getCanonicalName(), propiedades, columnasBaseDatos);
            if(coberturaReporteCabina!=null){
            	ReporteCabina reporteCabina = coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina();
                storedHelper.estableceParametro("P_IDTOPOLIZA", reporteCabina.getPoliza().getIdToPoliza());
    			storedHelper.estableceParametro("P_NUMEROINCISO", coberturaReporteCabina.getIncisoReporteCabina().getNumeroInciso());
    			storedHelper.estableceParametro("P_IDTOCOBERTURA", coberturaReporteCabina.getCoberturaDTO().getIdToCobertura());
    			storedHelper.estableceParametro("P_IDREPORTECABINA", reporteCabina.getId());
                MovimientoCoberturaSiniestro movimiento = (MovimientoCoberturaSiniestro)storedHelper.obtieneResultadoSencillo();
                if(movimiento != null ){
                	sumaAcumulada = movimiento.getImporteMovimiento();
                	if(sumaAcumulada == null){
                		sumaAcumulada = BigDecimal.ZERO; 
                	}
                }
            }
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en obtenerMontoSiniestrosAnteriores..."+descErr
                                           , Level.WARNING, e);    
             }
        }
        return sumaAcumulada;
	}
	
	
	@Override
	public MovimientoCoberturaSiniestro contabilizaReserva(Long movimientoId) {
		String spName = "MIDAS.PKG_CONTABILIDAD.SP_CONTABILIZA_RESERVA"; 
        StoredProcedureHelper storedHelper 			= null;
        String propiedades 							= "";
        String columnasBaseDatos 						= "";
        MovimientoCoberturaSiniestro movimientoResponse = null;
        try {
      	  propiedades = "loteContable,fechaContabilizacion";
      	  columnasBaseDatos = "LOTECONT,FECHACONT";
            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(MovimientoCoberturaSiniestro.class.getCanonicalName(), propiedades, columnasBaseDatos);
            storedHelper.estableceParametro("pidMovSinMidas", movimientoId);
            movimientoResponse  = (MovimientoCoberturaSiniestro)storedHelper.obtieneResultadoSencillo();
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en contabilizaReserva..."+descErr
                                           , Level.WARNING, e);    
             }
        }
	
	
        return movimientoResponse;
	}
	
	
	@Override
	public BigDecimal obtenerMontoCoberturaReporteCabina(Long idCoberturaReporte,Long estimacionId, String claveSubTipoCalculo) {
		String spName = "MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerMontosPorCobertura"; 
        StoredProcedureHelper storedHelper 			= null;
        String propiedades 							= "";
        String columnasBaseDatos 						= "";
        BigDecimal sumaAcumulada = BigDecimal.ZERO;
        
        CoberturaReporteCabina coberturaReporteCabina =  entidadDao.findById( CoberturaReporteCabina.class, idCoberturaReporte);
        
        try {
      	  
      	  propiedades = "importeMovimiento";
      	  columnasBaseDatos = "SUMADEHISTORICO";
            storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(MovimientoCoberturaSiniestro.class.getCanonicalName(), propiedades, columnasBaseDatos);
            if(coberturaReporteCabina!=null){
            	ReporteCabina reporteCabina = coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina();
                storedHelper.estableceParametro("P_IDTOPOLIZA", reporteCabina.getPoliza().getIdToPoliza());
    			storedHelper.estableceParametro("P_NUMEROINCISO", coberturaReporteCabina.getIncisoReporteCabina().getNumeroInciso());
    			storedHelper.estableceParametro("P_IDCOBERTURAREP", coberturaReporteCabina.getId());
    			storedHelper.estableceParametro("P_IDTOCOBERTURA", coberturaReporteCabina.getCoberturaDTO().getIdToCobertura());
    			storedHelper.estableceParametro("P_IDREPORTECABINA", reporteCabina.getId());
    			storedHelper.estableceParametro("P_IDESTIMACION", estimacionId);
    			storedHelper.estableceParametro("P_CLAVESUBTIPOCALC", claveSubTipoCalculo);
                MovimientoCoberturaSiniestro movimiento = (MovimientoCoberturaSiniestro)storedHelper.obtieneResultadoSencillo();
                if(movimiento != null ){
                	sumaAcumulada = movimiento.getImporteMovimiento();
                	if(sumaAcumulada == null){
                		sumaAcumulada = BigDecimal.ZERO; 
                	}
                }
            }
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en obtenerMontoCoberturaReporteCabina..."+descErr
                                           , Level.WARNING, e);    
             }
        }
        return sumaAcumulada;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoSiniestroDTO> obtenerMovimientos(MovimientoSiniestroDTO filtroBusqueda) {
		Query query = createQueryHistoricoMovimientos(filtroBusqueda, TIPO_BUSQUEDA.DTO);

		return query.getResultList();
	}
	
	@Override
	public BigDecimal obtenerImporteMovimientos(MovimientoSiniestroDTO filtroBusqueda) {			

		final StringBuilder sb = new StringBuilder();
		sb.append(" SELECT NVL(SUM(movimientoCobSin.importe_movimiento), 0) "); 
		sb.append(" FROM MIDAS.TOMOVIMIENTOCOBERTURASINIESTRO movimientoCobSin ");
		sb.append(" INNER JOIN MIDAS.TOESTIMACIONCOBERTURAREPCAB estimacionCobertura on (movimientoCobSin.ESTIMACIONCOBREPCABINA_ID = estimacionCobertura.id ) ");
		sb.append(" INNER JOIN MIDAS.TOCOBERTURAREPORTECABINA coberturaReporteCabina on (estimacionCobertura.COBERTURAREPORTECABINA_ID  = coberturaReporteCabina.id) ");
		sb.append(" INNER JOIN MIDAS.TOINCISOREPORTECABINA incisoReporteCabina on (coberturaReporteCabina.INCISOREPORTECABINA_ID = incisoReporteCabina.id) ");
		sb.append(" INNER JOIN MIDAS.TOSECCIONREPORTECABINA seccionReporteCabina on (incisoReporteCabina.SECCIONREPORTE_ID = seccionReporteCabina.id) ");
		sb.append(" INNER JOIN MIDAS.TOREPORTECABINA reporteCabina ON (seccionReporteCabina.REPORTECABINA_ID = reporteCabina.id) ");
		sb.append(" LEFT JOIN MIDAS.TOSINIESTROCABINA siniestroCabina ON (reporteCabina.id = siniestroCabina.REPORTECABINA_ID) ");
		sb.append(" WHERE movimientoCobSin.fecha_creacion is not null ");
		
		final Map<Integer, Object> params = new HashMap<Integer, Object>();
		int i = 1;
		if (filtroBusqueda.getIdToReporte() != null) { 
			sb.append(" AND reporteCabina.id = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getIdToReporte());
		}
		
		if (filtroBusqueda.getIdCoberturaReporte() != null) {
			sb.append(" AND coberturaReporteCabina.id = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getIdCoberturaReporte());
		}
		
		if (filtroBusqueda.getIdEstimacion() != null) {
			sb.append(" AND estimacionCobertura.id = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getIdEstimacion());
		}

		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoEstimacion())) {
			sb.append(" AND estimacionCobertura.TIPO_ESTIMACION = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getTipoEstimacion());
		}
		
		if(StringUtils.isNotEmpty(filtroBusqueda.getClaveTipoCalculo())) {
			sb.append(" AND coberturaReporteCabina.claveTipoCalculo = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getClaveTipoCalculo());
		}
		
		if(StringUtils.isNotEmpty(filtroBusqueda.getClaveSubCalculo())) {
			sb.append(" AND estimacionCobertura.CLAVE_SUB_TIPO_CALCULO = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getClaveSubCalculo());
		}
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoDocumento())) {
			sb.append(" AND movimientoCobSin.TIPO_DOCUMENTO = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getTipoDocumento());
		}	

		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoMovimiento())) {
			sb.append(" AND movimientoCobSin.TIPO_MOVIMIENTO = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getTipoMovimiento());
		}	
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoCausaMovimiento())) {
			sb.append(" AND movimientoCobSin.CAUSA_MOVIMIENTO = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getTipoCausaMovimiento());
		}	

		if (filtroBusqueda.getFechaIni() != null) {
			sb.append(" AND movimientoCobSin.FECHA_CREACION >= TRUNC(?").append(i).append(") ");
			params.put(i++, filtroBusqueda.getFechaIni());
		}

		if (filtroBusqueda.getFechaFin() != null ) {
			sb.append(" AND movimientoCobSin.FECHA_CREACION < TRUNC(?").append(i).append(" + 1) ");
			params.put(i++, filtroBusqueda.getFechaFin());
		}
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getUsuario())) {
			sb.append(" AND movimientoCobSin.CODIGO_USUARIO_CREACION = ?").append(i).append(" ");
			params.put(i++, filtroBusqueda.getUsuario());
		}
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getDescripcion())) {
			sb.append(" AND movimientoCobSin.descripcion LIKE ?").append(i).append(" ");
			params.put(i++, "%" + filtroBusqueda.getDescripcion() + "%");
		}
		
		final Query query = entityManager.createNativeQuery(sb.toString());
		for (int key : params.keySet()) {
			query.setParameter(key, params.get(key));
		}	
		
		return (BigDecimal)query.getSingleResult();
	
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoCoberturaSiniestro> obtenerMovimientos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo, String claveSubCalculo, TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, 
			CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento) {
		
		MovimientoSiniestroDTO filtro = new MovimientoSiniestroDTO();
		filtro.setIdToReporte(reporteCabinaId);
		filtro.setIdCoberturaReporte(coberturaId);
		filtro.setIdEstimacion(estimacionId);
		filtro.setClaveTipoCalculo(claveTipoCalculo);
		filtro.setClaveSubCalculo(claveSubCalculo);
		filtro.setTipoDocumento(tipoDocumento != null ? tipoDocumento.toString() : null);
		filtro.setTipoMovimiento(tipoMovimiento != null ? tipoMovimiento.toString() : null);
		filtro.setTipoCausaMovimiento(causaMovimiento != null ? causaMovimiento.toString() : null);
		filtro.setFechaFin(fechaUltimoMovimiento);
		
		Query query = createQueryHistoricoMovimientos(filtro, TIPO_BUSQUEDA.ENTITY);

		return query.getResultList();
	}
	
	@Override
	public BigDecimal obtenerImporteMovimientos(Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String claveTipoCalculo,String claveSubCalculo, TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, 
			CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento) {
			
		
		MovimientoSiniestroDTO filtro = new MovimientoSiniestroDTO();
		
		filtro.setIdToReporte(reporteCabinaId);
		filtro.setIdCoberturaReporte(coberturaId);
		filtro.setIdEstimacion(estimacionId);
		filtro.setClaveTipoCalculo(claveTipoCalculo);
		filtro.setClaveSubCalculo(claveSubCalculo);
		filtro.setTipoDocumento(tipoDocumento != null ? tipoDocumento.toString() : null);
		filtro.setTipoMovimiento(tipoMovimiento != null ? tipoMovimiento.toString() : null);
		filtro.setTipoCausaMovimiento(causaMovimiento != null ? causaMovimiento.toString() : null);
		filtro.setFechaFin(fechaUltimoMovimiento);
		
		return obtenerImporteMovimientos(filtro);
		
	}
	

	@SuppressWarnings("rawtypes")
	private Query createQueryHistoricoMovimientos( MovimientoSiniestroDTO filtroBusqueda, TIPO_BUSQUEDA tipo){
		
		Query query 							= null;
		StringBuilder queryString 				= new StringBuilder();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		
		if (TIPO_BUSQUEDA.IMPORTE.equals(tipo)) {
			queryString.append(" SELECT SUM(movimientoCobSin.importeMovimiento) ");
		} else if (TIPO_BUSQUEDA.ENTITY.equals(tipo)) {
			queryString.append(" SELECT movimientoCobSin ");
		} else if (TIPO_BUSQUEDA.DTO.equals(tipo)) {
			queryString.append(" SELECT new mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO( ")
			.append(" movimientoCobSin.id, ")
			.append(" movimientoCobSin.codigoUsuarioCreacion ,")
			.append( "func('CONCAT', siniestroCab.claveOficina, func('CONCAT', '-', func('CONCAT', siniestroCab.consecutivoReporte, func('CONCAT', '-', siniestroCab.anioReporte)))), ")
			.append(" func('MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerCoberturaPorEstimacion',estimacionCobRepCab.id), ")
			.append(" estimacionCobRepCab.folio, ")
			.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catTipoDocumento, movimientoCobSin.tipoDocumento), ")	
			.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catTipoMovimiento, movimientoCobSin.tipoMovimiento), ")
			.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catCausaMovimiento, movimientoCobSin.causaMovimiento), ")
			.append(" func('MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.FN_OBTENER_RESERVA_DISPONIBLE', estimacionCobRepCab.id, movimientoCobSin.fechaCreacion), ")
			.append(" movimientoCobSin.importeMovimiento, ")
			.append(" movimientoCobSin.fechaCreacion, ")
			.append(" movimientoCobSin.descripcion")
			.append(" )");
			
			Utilerias.agregaHashLista(listaParametrosValidos, "catTipoDocumento", CatGrupoFijo.TIPO_CATALOGO.TIPO_DE_DOCUMENTO.toString());			
			Utilerias.agregaHashLista(listaParametrosValidos, "catTipoMovimiento", CatGrupoFijo.TIPO_CATALOGO.TIPO_DE_MOVIMIENTO.toString());
			Utilerias.agregaHashLista(listaParametrosValidos, "catCausaMovimiento", CatGrupoFijo.TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO.toString());
		}
		queryString.append(" FROM MovimientoCoberturaSiniestro movimientoCobSin ")
		.append(" INNER JOIN movimientoCobSin.estimacionCobertura estimacionCobRepCab ")
		.append(" INNER JOIN estimacionCobRepCab.coberturaReporteCabina coberturaRepCab ")
		.append(" INNER JOIN coberturaRepCab.incisoReporteCabina incisoRepCab ")
		.append(" INNER JOIN incisoRepCab.seccionReporteCabina seccionRepCab ")
		.append(" INNER JOIN seccionRepCab.reporteCabina reporteCab ")
		.append(" LEFT JOIN reporteCab.siniestroCabina siniestroCab ")
		.append(" WHERE movimientoCobSin.fechaCreacion IS NOT NULL");
		
		if (filtroBusqueda.getIdToReporte() != null) {
			
			queryString.append(" AND reporteCab.id = :reporteId");
			Utilerias.agregaHashLista(listaParametrosValidos, "reporteId", filtroBusqueda.getIdToReporte());
		}

		if (filtroBusqueda.getIdCoberturaReporte() != null) {
			
			queryString.append(" AND coberturaRepCab.id = :coberturaId"); 
			Utilerias.agregaHashLista(listaParametrosValidos, "coberturaId", filtroBusqueda.getIdCoberturaReporte() );
		}
		
		if (filtroBusqueda.getIdEstimacion() != null) {
			
			queryString.append(" AND estimacionCobRepCab.id = :estimacionId"); 
			Utilerias.agregaHashLista(listaParametrosValidos, "estimacionId", filtroBusqueda.getIdEstimacion() );
		}

		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoEstimacion())) {
			
			queryString.append(" AND estimacionCobRepCab.tipoEstimacion = :tipoEstimacion "); 
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoEstimacion", filtroBusqueda.getTipoEstimacion());
		}
		
		if(StringUtils.isNotEmpty(filtroBusqueda.getClaveTipoCalculo())) {
			
			queryString.append(" AND coberturaRepCab.claveTipoCalculo = :claveTipoCalculo ");
			Utilerias.agregaHashLista(listaParametrosValidos, "claveTipoCalculo", filtroBusqueda.getClaveTipoCalculo());
		}
		
		if(StringUtils.isNotEmpty(filtroBusqueda.getClaveSubCalculo())) {
			
			queryString.append(" AND estimacionCobRepCab.cveSubCalculo= :cveSubCalculo ");
			Utilerias.agregaHashLista(listaParametrosValidos, "cveSubCalculo", filtroBusqueda.getClaveSubCalculo());
		}
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoDocumento())) {
			
			queryString.append(" AND movimientoCobSin.tipoDocumento = :tipoDocumento "); 
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoDocumento", filtroBusqueda.getTipoDocumento());
		}	

		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoMovimiento())) {
			
			queryString.append(" AND movimientoCobSin.tipoMovimiento = :tipoMovimiento "); 
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoMovimiento", filtroBusqueda.getTipoMovimiento());
		}	
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoCausaMovimiento())) {
			
			queryString.append(" AND movimientoCobSin.causaMovimiento = :causaMovimiento "); 
			Utilerias.agregaHashLista(listaParametrosValidos, "causaMovimiento", filtroBusqueda.getTipoCausaMovimiento());
		}	
	
		if (filtroBusqueda.getFechaIni()!= null) {
			queryString.append(" AND  FUNC('trunc', movimientoCobSin.fechaCreacion ) >= :fechaIni ");		
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaIni", filtroBusqueda.getFechaIni());
		}

		if (filtroBusqueda.getFechaFin() != null ) {
			queryString.append(" AND  FUNC('trunc', movimientoCobSin.fechaCreacion ) <= :fechaFin ");

			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFin", filtroBusqueda.getFechaFin());
		}
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getUsuario())) {
			queryString.append(" AND movimientoCobSin.codigoUsuarioCreacion = :usuario ");

			Utilerias.agregaHashLista(listaParametrosValidos, "usuario", filtroBusqueda.getUsuario());
		}
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getDescripcion())) {
			queryString.append(" AND movimientoCobSin.descripcion LIKE '%");
				queryString.append(filtroBusqueda.getDescripcion()); queryString.append("%'");
		}

		queryString.append(" ORDER BY movimientoCobSin.fechaCreacion DESC");

		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		

		return query;
	}
	

	
	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoSiniestroDTO> obtenerMovimientosGA(MovimientoSiniestroDTO filtroBusqueda) {
		Query query = createQueryMovimientosGA(filtroBusqueda, TIPO_BUSQUEDA.DTO);

		return query.getResultList();
	}
	
	@Override
	public BigDecimal obtenerImporteMovimientosGA(MovimientoSiniestroDTO filtroBusqueda) {
		
		Query query = createQueryMovimientosGA(filtroBusqueda, TIPO_BUSQUEDA.IMPORTE);
		return (BigDecimal) query.getSingleResult();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoSiniestro> obtenerMovimientosGA(Long reporteCabinaId, Long ordenCompraId, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento) {
		

		MovimientoSiniestroDTO filtro = new MovimientoSiniestroDTO();
		
		filtro.setIdToReporte(reporteCabinaId);
		filtro.setIdOrdenCompra(ordenCompraId);
		filtro.setTipoDocumento(tipoDocumento != null ? tipoDocumento.toString() : null);
		filtro.setTipoMovimiento(tipoMovimiento != null ? tipoMovimiento.toString() : null);
		filtro.setTipoCausaMovimiento(causaMovimiento != null ? causaMovimiento.toString() : null);
		filtro.setFechaFin(fechaUltimoMovimiento);
		
		Query query = createQueryMovimientosGA(filtro, TIPO_BUSQUEDA.ENTITY);
		return query.getResultList();
	}
	
	@Override
	public BigDecimal obtenerImporteMovimientosGA(Long reporteCabinaId, Long ordenCompraId, 
			TipoDocumentoMovimiento tipoDocumento, TipoMovimiento tipoMovimiento, CausaMovimiento causaMovimiento, Date fechaUltimoMovimiento) {
		
		MovimientoSiniestroDTO filtro = new MovimientoSiniestroDTO();
		filtro.setIdToReporte(reporteCabinaId);
		filtro.setIdOrdenCompra(ordenCompraId);
		filtro.setTipoDocumento(tipoDocumento != null ? tipoDocumento.toString() : null);
		filtro.setTipoMovimiento(tipoMovimiento != null ? tipoMovimiento.toString() : null);
		filtro.setTipoCausaMovimiento(causaMovimiento != null ? causaMovimiento.toString() : null);
		filtro.setFechaFin(fechaUltimoMovimiento);
		
		return obtenerImporteMovimientos(filtro);
	}
	

	@SuppressWarnings("rawtypes")
	private Query createQueryMovimientosGA(MovimientoSiniestroDTO filtroBusqueda, TIPO_BUSQUEDA tipo) {
		
		Query query 							= null;
		StringBuilder queryString 				= new StringBuilder();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		
		if (TIPO_BUSQUEDA.IMPORTE.equals(tipo)) {
			queryString.append(" SELECT SUM(movimientoSin.importeMovimiento) ");
		} else if (TIPO_BUSQUEDA.ENTITY.equals(tipo)) {
			queryString.append(" SELECT movimientoSin ");
		} else if (TIPO_BUSQUEDA.DTO.equals(tipo)) {
			queryString.append(" SELECT new mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO( ")
			.append(" movimientoSin.id, ")
			.append(" movimientoSin.codigoUsuarioCreacion , ")
			.append( "func('CONCAT', siniestroCab.claveOficina, func('CONCAT', '-', func('CONCAT', siniestroCab.consecutivoReporte, func('CONCAT', '-', siniestroCab.anioReporte)))), ")
			.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catTipoDocumento, movimientoSin.tipoDocumento), ")	
			.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catTipoMovimiento, movimientoSin.tipoMovimiento), ")
			.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catCausaMovimiento, movimientoSin.causaMovimiento), ")
			.append(" movimientoSin.importeMovimiento, ")
			.append(" movimientoSin.fechaCreacion, ")
			.append(" movimientoSin.descripcion ")
			.append(" )");
			
			Utilerias.agregaHashLista(listaParametrosValidos, "catTipoDocumento", CatGrupoFijo.TIPO_CATALOGO.TIPO_DE_DOCUMENTO.toString());			
			Utilerias.agregaHashLista(listaParametrosValidos, "catTipoMovimiento", CatGrupoFijo.TIPO_CATALOGO.TIPO_DE_MOVIMIENTO.toString());
			Utilerias.agregaHashLista(listaParametrosValidos, "catCausaMovimiento", CatGrupoFijo.TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO.toString());
		}
		queryString.append(" FROM MovimientoSiniestro movimientoSin ")
		.append(" INNER JOIN movimientoSin.reporteCabina reporteCab ")
		.append(" LEFT JOIN reporteCab.siniestroCabina siniestroCab ")
		.append(" WHERE movimientoSin.fechaCreacion IS NOT NULL");
		
		if (filtroBusqueda.getIdToReporte() != null) {
			
			queryString.append(" AND reporteCab.id = :reporteId");
			Utilerias.agregaHashLista(listaParametrosValidos, "reporteId", filtroBusqueda.getIdToReporte());
		}


		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoDocumento())) {
			
			queryString.append(" AND movimientoSin.tipoDocumento = :tipoDocumento "); 
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoDocumento", filtroBusqueda.getTipoDocumento());
		}	

		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoMovimiento())) {
			
			queryString.append(" AND movimientoSin.tipoMovimiento = :tipoMovimiento "); 
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoMovimiento", filtroBusqueda.getTipoMovimiento());
		}	
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getTipoCausaMovimiento())) {
			
			queryString.append(" AND movimientoSin.causaMovimiento = :causaMovimiento "); 
			Utilerias.agregaHashLista(listaParametrosValidos, "causaMovimiento", filtroBusqueda.getTipoCausaMovimiento());
		}		
	
		if (filtroBusqueda.getFechaIni()!= null) {
			queryString.append(" AND  FUNC('trunc', movimientoSin.fechaCreacion ) >= :fechaIni ");		
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaIni", filtroBusqueda.getFechaIni());
		}

		if (filtroBusqueda.getFechaFin() != null ) {
			queryString.append(" AND  FUNC('trunc', movimientoSin.fechaCreacion ) <= :fechaFin ");

			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFin", filtroBusqueda.getFechaFin());
		}
		
		if (StringUtils.isNotEmpty(filtroBusqueda.getUsuario())) {
			queryString.append(" AND movimientoSin.codigoUsuarioCreacion = :usuario ");

			Utilerias.agregaHashLista(listaParametrosValidos, "usuario", filtroBusqueda.getUsuario());
		}

		if (StringUtils.isNotEmpty(filtroBusqueda.getDescripcion())) {
			queryString.append(" AND movimientoSin.descripcion LIKE '%");
				queryString.append(filtroBusqueda.getDescripcion()); queryString.append("%'");
		}
		
		queryString.append(" ORDER BY movimientoSin.fechaCreacion DESC");

		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		

		return query;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String,String> obtenerUsuariosMovSiniestros(){
		Query query = null;
		Map<String,String> usuariosMap = new LinkedHashMap<String,String>();
		StringBuilder queryString = new StringBuilder();
		
		queryString.append(" SELECT DISTINCT model1.codigoUsuarioCreacion as usuario FROM MovimientoSiniestro model1 ");
		queryString.append(" UNION ALL ");
		queryString.append(" SELECT DISTINCT model2.codigoUsuarioCreacion as usuario FROM MovimientoCoberturaSiniestro model2 ");
		queryString.append(" WHERE usuario NOT IN ( SELECT DISTINCT model3.codigoUsuarioCreacion FROM MovimientoSiniestro model3 ) ");
		query = entityManager.createQuery(queryString.toString());
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		List<String> usuariosList = query.getResultList();
		
		if(usuariosList != null){
			for(String usuario : usuariosList){
				usuariosMap.put(usuario, usuario);
			}
		}
		
		return usuariosMap;
	}
}
