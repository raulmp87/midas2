package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.sql.SQLException;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoSeycosFacadeRemote;
@Stateless
public class EjecutivoSeycosFacade implements EjecutivoSeycosFacadeRemote{

	@Override
	public Long save(Ejecutivo ejecutivo) throws Exception {
		if(ejecutivo==null){
			throw new Exception("Ejecutivo is null!");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_catOfi";
		Long idSeycos=null;
		try {
			LogDeMidasInterfaz.log("Entrando a EjecutivoSeycosFacade.save..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			//Domicilio domicilio=ejecutivo.getDomicilio();
//			String nombreColonia=(domicilio!=null && domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().isEmpty())?domicilio.getNombreColonia():null;
//			String nombreCalle=(domicilio!=null && domicilio.getCalleNumero()!=null && !domicilio.getCalleNumero().isEmpty())?domicilio.getCalleNumero():null;
//			String idMunicipio = (domicilio!=null && domicilio.getClaveCiudad()!=null && !domicilio.getClaveCiudad().isEmpty())?domicilio.getClaveCiudad():null;
//			String codigoPostal=(domicilio!=null && domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().isEmpty())?domicilio.getCodigoPostal():null;
			String claveEstatus=(ejecutivo.getClaveEstatus()==1)?"A":"I";//clave de estatus en seycos
//			if(idMunicipio!=null){
//				while (idMunicipio.length()<5){	
//					idMunicipio= "0"+idMunicipio;
//				}
//			}
			String descripcion=(ejecutivo.getPersonaResponsable()!=null && ejecutivo.getPersonaResponsable().getNombreCompleto()!=null)?ejecutivo.getPersonaResponsable().getNombreCompleto():null;
			String tipoOficina=(ejecutivo.getTipoEjecutivo()!=null && ejecutivo.getTipoEjecutivo().getClave()!=null)?ejecutivo.getTipoEjecutivo().getClave():null;
			tipoOficina=(tipoOficina!=null && !tipoOficina.isEmpty())?tipoOficina.trim():null;
			Long idGerencia=(ejecutivo.getGerencia()!=null)?ejecutivo.getGerencia().getIdGerencia():null;
			Long idPersonaResponsable=(ejecutivo.getPersonaResponsable()!=null)?ejecutivo.getPersonaResponsable().getIdPersona():null;
			Domicilio domicilio=ejecutivo.getDomicilio();
			DomicilioPk pkDomicilio=(domicilio!=null)?domicilio.getIdDomicilio():null;
			Long idDomicilio=(pkDomicilio!=null)?pkDomicilio.getIdDomicilio():null;
			String correo=(ejecutivo.getPersonaResponsable()!=null)?ejecutivo.getPersonaResponsable().getEmail():null;
			storedHelper.estableceParametro("pid_oficina", val(ejecutivo.getIdEjecutivo()));
			storedHelper.estableceParametro("pdescripcion",val(descripcion));
			storedHelper.estableceParametro("ptipo_oficina",val(tipoOficina));
			storedHelper.estableceParametro("pid_gerencia",val(idGerencia));
			storedHelper.estableceParametro("pid_persona_resp",val(idPersonaResponsable));
			storedHelper.estableceParametro("pid_domicilio_resp",val(idDomicilio));
			storedHelper.estableceParametro("pcorreo_electronico",val(correo));
			storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz.log("Se ha guardado ejecutivo "+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de EjecutivoSeycosFacade.save..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, ejecutivo.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de EjecutivoSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en EjecutivoSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		}
		return idSeycos;
	}
	private Object val(Object value){
		return (value!=null)?value:"";
	}

	@Override
	public void unsubscribe(Ejecutivo ejecutivo) throws Exception {
		ejecutivo.setClaveEstatus(0l);//se inactiva el centro de operacion.(0 inactiva, 1 activa)
		save(ejecutivo);//Se actualiza el estatus del centro de operacion en Seycos
	}

}
