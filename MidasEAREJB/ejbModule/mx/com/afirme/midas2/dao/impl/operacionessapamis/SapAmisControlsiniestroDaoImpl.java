package mx.com.afirme.midas2.dao.impl.operacionessapamis;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.operacionessapamis.SapAmisControlsiniestroDao;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisControlSiniestros;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class SapAmisControlsiniestroDaoImpl implements SapAmisControlsiniestroDao {
	private EntidadService entidadService;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<SapAmisControlSiniestros> findAllControlSiniestro() {
		return entidadService.findAll(SapAmisControlSiniestros.class);
	}

	@Override
	public void limpiarRegistrosControlEnvios(List<SapAmisControlSiniestros> arg0) {
		entidadService.removeAll(arg0);
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
