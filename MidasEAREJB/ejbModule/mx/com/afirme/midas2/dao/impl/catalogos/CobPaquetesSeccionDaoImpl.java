package mx.com.afirme.midas2.dao.impl.catalogos;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.catalogos.CobPaquetesSeccionDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId_;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion_;

@Stateless
public class CobPaquetesSeccionDaoImpl extends JpaDao<CobPaquetesSeccionId, CobPaquetesSeccion> implements
CobPaquetesSeccionDao {

	public List<CobPaquetesSeccion> findByFilters(CobPaquetesSeccion cobPaquetesSeccion) {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			Long longNull = null;
			Short shortNull = null;
			Expression<Long> idToSeccionExp = cb.literal(cobPaquetesSeccion.getId().getIdToSeccion());
			Expression<Long> idPaqueteExp = cb.literal(cobPaquetesSeccion.getId().getIdPaquete());
			Expression<Long> idToCoberturaExp = cb.literal(cobPaquetesSeccion.getId().getIdToCobertura());
			Expression<Short> cveObligatoriedadExp = cb.literal(cobPaquetesSeccion.getClaveObligatoriedad());
			Expression<Long> longNullExp = cb.literal(longNull);
			Expression<Short> shortNullExp = cb.literal(shortNull);
			
			CriteriaQuery<CobPaquetesSeccion> criteriaQuery = cb.createQuery(entityClass);
			Root<CobPaquetesSeccion> root = criteriaQuery.from(CobPaquetesSeccion.class);
			Predicate predicado = cb.and(cb.or(cb.equal(idToSeccionExp, longNullExp),cb.equal(root.get(CobPaquetesSeccion_.id).get(CobPaquetesSeccionId_.idToSeccion), cobPaquetesSeccion.getId().getIdToSeccion())),
					cb.or(cb.equal(idPaqueteExp, longNullExp),cb.equal(root.get(CobPaquetesSeccion_.id).get(CobPaquetesSeccionId_.idPaquete), cobPaquetesSeccion.getId().getIdPaquete())),
					cb.or(cb.equal(idToCoberturaExp, longNullExp),cb.equal(root.get(CobPaquetesSeccion_.id).get(CobPaquetesSeccionId_.idToCobertura), cobPaquetesSeccion.getId().getIdToCobertura())),
					cb.or(cb.equal(cveObligatoriedadExp, shortNullExp),cb.equal(root.get(CobPaquetesSeccion_.claveObligatoriedad), cobPaquetesSeccion.getClaveObligatoriedad())));
			criteriaQuery.where(predicado);
			
			TypedQuery<CobPaquetesSeccion> query = entityManager.createQuery(criteriaQuery);
			return query.getResultList();
		
	}
	
	public List<CobPaquetesSeccion> getPorSeccionPaquete(CobPaquetesSeccion cobPaquetesSeccion){
		return getPorSeccionPaquete(cobPaquetesSeccion.getId().getIdToSeccion(),
				cobPaquetesSeccion.getId().getIdPaquete());
	}
	
	@SuppressWarnings("unchecked")
	public List<CobPaquetesSeccion> getPorSeccionPaquete(Long idToSeccion,Long idPaquete){
		try {
			
			final String queryString = "select model from CobPaquetesSeccion model where model.id.idToSeccion= :idToSeccion "+
			 "and model.id.idPaquete = :idPaquete";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", idToSeccion);
			query.setParameter("idPaquete", idPaquete);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all CobPaquetesSeccion by idToSeccion and idPaquete failed", Level.SEVERE, re);
			throw re;
		}
	}
}
