package mx.com.afirme.midas2.dao.impl.bonos;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.bonos.ConfigBonoRangoAplicaDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class ConfigBonoRangoAplicaDaoImpl extends EntidadDaoImpl implements ConfigBonoRangoAplicaDao{
	private EntidadService entidadService;
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}


	@Override
	public List<ConfigBonoRangoAplica> loadByConfigBono(ConfigBonos configBono) throws Exception {
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<ConfigBonoRangoAplica> lista=new ArrayList<ConfigBonoRangoAplica>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append(" SELECT id,configBono_id,importePrimaInicial,importePrimaFinal,pctsiniestralidadInicial,pctsiniestralidadFinal,pctCrecimientoInicial "); 
		queryString.append(" ,pctCrecimientoFinal,pctSupmetaInicial,pctSupmetaFinal,valorAplicacion,numpolizasEmitidasInicial,numpolizasEmitidasFinal ");
		queryString.append(" from MIDAS.TRCONFIGBONORANGOAPLICA ");
		queryString.append(" where ");
		int index=1;
		if(configBono.getId()!=null){
				addCondition(queryString, " configBono_id=? ");
				params.put(index, configBono.getId());
				index++;
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString),ConfigBonoRangoAplica.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		query.setMaxResults(100);
		lista=query.getResultList();
		return lista;
	}
	
	public Boolean rangosCerrados (List<ConfigBonoRangoAplica> configBonoRangoAplicaList){
		Integer cont = 0;
		Boolean rangosCerrados = false;
		for (ConfigBonoRangoAplica obj:configBonoRangoAplicaList){
			if(obj!=null){
				if (isNotNull(obj.getImportePrimaInicial()) || isNotNull(obj.getImportePrimaFinal())){
					if (isNull(obj.getImportePrimaInicial()) || isNull(obj.getImportePrimaFinal())){
							cont = cont+1;
					}else
					 if(obj.getImportePrimaInicial().compareTo(obj.getImportePrimaFinal()) == 1 ){//|| obj.getImportePrimaInicial().compareTo(obj.getImportePrimaFinal()) == 0){
						cont = cont+1;
					 }
				}
				if (isNotNull(obj.getNumPolizasEmitidasInicial()) || isNotNull(obj.getNumPolizasEmitidasFinal())){
					if (isNull(obj.getNumPolizasEmitidasInicial()) || isNull(obj.getNumPolizasEmitidasFinal())){
						cont = cont+1;
					}else
						if(obj.getNumPolizasEmitidasInicial().compareTo(obj.getNumPolizasEmitidasFinal())==1 ){//|| obj.getNumPolizasEmitidasInicial().compareTo(obj.getNumPolizasEmitidasFinal())==0){
							cont = cont+1;
						}
				}
				if (isNotNull(obj.getPctCrecimientoInicial()) || isNotNull(obj.getPctCrecimientoFinal())){
					if (isNull(obj.getPctCrecimientoInicial()) || isNull(obj.getPctCrecimientoFinal())){
						cont = cont+1;
					}else
					if(obj.getPctCrecimientoInicial().compareTo(obj.getPctCrecimientoFinal())==1 ){//|| obj.getPctCrecimientoInicial().compareTo(obj.getPctCrecimientoFinal())==0){
						cont = cont+1;
					}
				}
				if (isNotNull(obj.getPctSiniestralidadInicial()) || isNotNull(obj.getPctSiniestralidadFinal())){
					if (isNull(obj.getPctSiniestralidadInicial()) || isNull(obj.getPctSiniestralidadFinal())){
						cont = cont+1;
					}else
					if(obj.getPctSiniestralidadInicial().compareTo(obj.getPctSiniestralidadFinal())==1 ){//|| obj.getPctSiniestralidadInicial().compareTo(obj.getPctSiniestralidadFinal())==0){
						cont = cont+1;
					}
				}
				if (isNotNull(obj.getPctSupMetaInicial()) || isNotNull(obj.getPctSupMetaFinal())){
					if (isNull(obj.getPctSupMetaInicial()) || isNull(obj.getPctSupMetaFinal())){
						cont = cont+1;
					}else
					if(obj.getPctSupMetaInicial().compareTo(obj.getPctSupMetaFinal())==1 ){//|| obj.getPctSupMetaInicial().compareTo(obj.getPctSupMetaFinal())==0){
						cont = cont+1;
					}
				}
			}
		}
		
		return rangosCerrados = (cont==0)?false:true;
	}
	
	public boolean validarTamanioCampoFactores (List<ConfigBonoRangoAplica> configBonoRangoAplicaList){
		int contarErrores = 0;
		
		for(ConfigBonoRangoAplica obj:configBonoRangoAplicaList){
			if(obj!=null){
				if (obj.getImportePrimaInicial()!=null){
					if(!tamanioCampoRangoFactores(obj.getImportePrimaFinal()) || !tamanioCampoRangoFactores(obj.getImportePrimaInicial())){
						contarErrores++;
					}
				}
				if (obj.getPctCrecimientoInicial()!=null){
					if(!tamanioCampoRangoFactores(obj.getPctCrecimientoInicial()) || !tamanioCampoRangoFactores(obj.getPctCrecimientoFinal())){
						contarErrores++;
					}
				}
				if (obj.getPctSiniestralidadInicial()!=null){
					if(!tamanioCampoRangoFactoresNeg(obj.getPctSiniestralidadInicial()) || !tamanioCampoRangoFactoresNeg(obj.getPctSiniestralidadFinal())){
						contarErrores++;
					}
				}
				if (obj.getPctSupMetaInicial()!=null){
					if(!tamanioCampoRangoFactores(obj.getPctSupMetaInicial()) || !tamanioCampoRangoFactores(obj.getPctSupMetaFinal())){
						contarErrores++;
					}
				}
				if (obj.getValorAplicacion()!=null){
					if(!tamanioCampoRangoFactores(obj.getValorAplicacion())){
						contarErrores++;
					}
				}
			}
		}
		return (contarErrores==0)?false:true;
	}
	private  boolean tamanioCampoRangoFactores (BigDecimal object){ 
		 Pattern p = Pattern.compile("^([0-9]{0,14})(\\.[0-9]{1,2})?$");
		 if(object!=null){
			 Matcher m = p.matcher(object.toString());
			 return m.find();
		 }
	     return true;
	}
	private  boolean tamanioCampoRangoFactoresNeg (BigDecimal object){ 
		 Pattern p = Pattern.compile("^-?(\\d{1,14})(\\.\\d{1,2})?$");//compile("^-?([0-9]{0,14})(\\.[0-9]{1,2})?$");
		 if(object!=null){
			 Matcher m = p.matcher(object.toString());
			 return m.find();
		 }
	     return true;
	}
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
	@Override
	public void validarRangofactores(List<ConfigBonoRangoAplica> configBonoRangoAplicaList) throws Exception {
		
		if(configBonoRangoAplicaList==null || configBonoRangoAplicaList.isEmpty()){
			throw new Exception("la lista de configBonoRangoAplicaList is null");
		}
	
		if(rangosCerrados(configBonoRangoAplicaList) == true){
			throw new Exception("Hay rangos sin cerrar ó el rango final es menor al inicial");
		}
		if(validarTamanioCampoFactores(configBonoRangoAplicaList)==true){
			throw new Exception("Existen campos con mas de 14 enteros y/o 2 decimales");
		}
		noExisteBonificacion(configBonoRangoAplicaList);
		//se validan los rangos de importe de prima
		compararRangosImportePrimaInicial(configBonoRangoAplicaList);
		//se validan los rangos de % de siniestralidad
		compararRangosPctSiniestralidad(configBonoRangoAplicaList);
		//se validan los rangos del % de Crecimiento
		compararRangosPctCrecimiento(configBonoRangoAplicaList);
		//se validan los rangos del % de superacion de meta.
		compararRangosPctSupMeta(configBonoRangoAplicaList);
		// se validan los rangos de las polizas emitidas pagadas
		compararRangosNoPolEmitPag(configBonoRangoAplicaList);

		for(ConfigBonoRangoAplica conf:configBonoRangoAplicaList){
			if(conf!=null){
				if(conf.getId()!=null && (conf.getImportePrimaInicial().compareTo(conf.getImportePrimaFinal())==1)){					
						throw new Exception("El importe inicial debe ser menor que el importe final, favor de verificar");
										
				}
				if(conf.getPctSiniestralidadInicial()!=null && (conf.getPctSiniestralidadInicial().compareTo(conf.getPctSiniestralidadFinal())==1)){
					throw new Exception("El porcentaje de siniestralidad inicial debe ser menor que el porcentaje de siniestralidad final, favor de verificar");
				}
				if(conf.getPctCrecimientoInicial()!=null && (conf.getPctCrecimientoInicial().compareTo(conf.getPctCrecimientoFinal())==1)){
					throw new Exception("El porcentaje de crecimiento inicial debe ser menor que el porcentaje de crecimiento final, favor de verificar");
				}
				if(conf.getPctSupMetaInicial()!=null && (conf.getPctSupMetaInicial().compareTo(conf.getPctSupMetaFinal())==1)){
					throw new Exception("El porcentaje de superacion de meta inicial debe ser menor que el porcentaje de superacion de meta final, favor de verificar");
				}
			}
		}
		
	}
/**
 * 
 * @param configBonoRangoAplicaList
 * @throws Exception
 * valida los rangos en las columnas de Y hasta de importe de prima
 */
	public void compararRangosImportePrimaInicial(List<ConfigBonoRangoAplica> configBonoRangoAplicaList) throws Exception{
		
		Collections.sort(configBonoRangoAplicaList,  
				new Comparator<ConfigBonoRangoAplica>(){
			public int compare(ConfigBonoRangoAplica object1,ConfigBonoRangoAplica object2) {
				if(object1==null && object2==null){
					return 1;
				}else if(object1==null){
					return 1;
				}else if(object2==null){
					return -1;
				}else if(object1.getImportePrimaInicial()==null && object2.getImportePrimaInicial()==null){
					return 0;
			    }else if(object1.getImportePrimaInicial()==null){
						return -1;
				}else if(object2.getImportePrimaInicial()==null){
					return 1;
				}else{
					return object1.getImportePrimaInicial().compareTo(object2.getImportePrimaInicial());
				}
			}
		});
		int cont = 0;
		int valorRetornoImportePrima;
		int pctSiniestralidad;
		int pctCrecimiento;
		int pctSupMeta;
		int numPolEmi;
		configBonoRangoAplicaList = quitarNulos(configBonoRangoAplicaList);
		if (!configBonoRangoAplicaList.isEmpty() && configBonoRangoAplicaList.size()>1){
			for(ConfigBonoRangoAplica a : configBonoRangoAplicaList){
				if(isNotNull(a)){
					cont++;
					ConfigBonoRangoAplica b;
					if(cont<=configBonoRangoAplicaList.size()-1){
						b =configBonoRangoAplicaList.get(cont);
						if(cont < configBonoRangoAplicaList.size()){
							//**********$ importe prima**********
							if(isNotNull(a.getImportePrimaInicial()) && isNotNull(a.getImportePrimaFinal()) && 
								isNotNull(b.getImportePrimaInicial()) && isNotNull(b.getImportePrimaFinal())){
								 valorRetornoImportePrima = compararDosRangosBigdecimal(a.getImportePrimaInicial(),a.getImportePrimaFinal(),b.getImportePrimaInicial(),b.getImportePrimaFinal());
								if	(valorRetornoImportePrima==-1){
									throw new Exception("Error en la configuracion de Rangos, Favor de verificar");
								}else if(valorRetornoImportePrima==0){//se valida el % de siniestralidad en caso de que sean iguales
									pctSiniestralidad = comparaRangosDependientes(a.getPctSiniestralidadInicial(),a.getPctSiniestralidadFinal(),
											b.getPctSiniestralidadInicial(),b.getPctSiniestralidadFinal());
									if(pctSiniestralidad==0){
										pctCrecimiento = comparaRangosDependientes(a.getPctCrecimientoInicial(),a.getPctCrecimientoFinal(),
												b.getPctCrecimientoInicial(),b.getPctCrecimientoFinal());
										if(pctCrecimiento==0){
											pctSupMeta = comparaRangosDependientes(a.getPctSupMetaInicial(),a.getPctSupMetaFinal(),
													b.getPctSupMetaInicial(),b.getPctSupMetaFinal());
											if(pctSupMeta==0){
												numPolEmi = compararDosRangosLong(a.getNumPolizasEmitidasInicial(),a.getNumPolizasEmitidasFinal(),b.getNumPolizasEmitidasInicial(),b.getNumPolizasEmitidasFinal());
//												if(isNotNull(b.getNumPolizasEmitidasInicial())&&isNotNull(a.getNumPolizasEmitidasInicial())&&isNotNull(b.getNumPolizasEmitidasFinal())&&isNotNull(a.getNumPolizasEmitidasFinal())){
//													if(b.getNumPolizasEmitidasInicial().compareTo(a.getNumPolizasEmitidasInicial())==0 && b.getNumPolizasEmitidasFinal().compareTo(a.getNumPolizasEmitidasFinal())==0){
//												System.out.println("numPolEmi**************************************************"+numPolEmi);
													if(numPolEmi==0||numPolEmi==-1){
														throw new Exception("Error en la configuracion de Rangos, Favor de verificar ");
													}
//												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/**
	 * 
	 * @param configBonoRangoAplicaList
	 * @throws Exception
	 * valida los rangos del % de siniestralidad
	 */
	public void compararRangosPctSiniestralidad(List<ConfigBonoRangoAplica> configBonoRangoAplicaList) throws Exception{
		
		Collections.sort(configBonoRangoAplicaList,  
				new Comparator<ConfigBonoRangoAplica>(){
			public int compare(ConfigBonoRangoAplica object1,ConfigBonoRangoAplica object2) {
				if(object1==null && object2==null){
					return 0;
				}else if(object1==null){
					return -1;
				}else if(object2==null){
					return 1;
				}else if(object1.getPctSiniestralidadInicial()==null && object2.getPctSiniestralidadInicial()==null){
					return 0;
			    }else if(object1.getPctSiniestralidadInicial()==null){
						return -1;
				}else if(object2.getPctSiniestralidadInicial()==null){
					return 1;
				}else{
					return object1.getPctSiniestralidadInicial().compareTo(object2.getPctSiniestralidadInicial());
				}
			}
		});
		int cont = 0;
		int valorRetornoImportePrima;
		int pctSiniestralidad;
		int pctCrecimiento;
		int pctSupMeta;
		int numPolEmi;
		configBonoRangoAplicaList = quitarNulos(configBonoRangoAplicaList);
		
		if (!configBonoRangoAplicaList.isEmpty() && configBonoRangoAplicaList.size()>1){
			for(ConfigBonoRangoAplica a : configBonoRangoAplicaList){
				if(isNotNull(a)){
					cont++;
					if(cont<=configBonoRangoAplicaList.size()-1){
						ConfigBonoRangoAplica b =configBonoRangoAplicaList.get(cont);
						if(cont < configBonoRangoAplicaList.size()){
							//**********% Siniestralidad**********
							if(isNotNull(a.getPctSiniestralidadInicial()) && isNotNull(a.getPctSiniestralidadFinal()) && 
								isNotNull(b.getPctSiniestralidadInicial()) && isNotNull(b.getPctSiniestralidadFinal())){
								 //se valida el % de siniestralidad
									pctSiniestralidad = compararDosRangosBigdecimal(a.getPctSiniestralidadInicial(),a.getPctSiniestralidadFinal(),
											b.getPctSiniestralidadInicial(),b.getPctSiniestralidadFinal());
									if(pctSiniestralidad==-1){
										throw new Exception("Error en la configuracion de Rangos, Favor de verificar");
									}
									if(pctSiniestralidad==0){
										pctCrecimiento = comparaRangosDependientes(a.getPctCrecimientoInicial(),a.getPctCrecimientoFinal(),
												b.getPctCrecimientoInicial(),b.getPctCrecimientoFinal());
									if(pctCrecimiento==0){
										pctSupMeta = comparaRangosDependientes(a.getPctSupMetaInicial(),a.getPctSupMetaFinal(),
												b.getPctSupMetaInicial(),b.getPctSupMetaFinal());
										if(pctSupMeta==0){
											numPolEmi = compararDosRangosLong(a.getNumPolizasEmitidasInicial(),a.getNumPolizasEmitidasFinal(),b.getNumPolizasEmitidasInicial(),b.getNumPolizasEmitidasFinal());
//											if(isNotNull(b.getNumPolizasEmitidasInicial())&&isNotNull(a.getNumPolizasEmitidasInicial())&&isNotNull(b.getNumPolizasEmitidasFinal())&&isNotNull(a.getNumPolizasEmitidasFinal())){
//												if(b.getNumPolizasEmitidasInicial().compareTo(a.getNumPolizasEmitidasInicial())==0 && b.getNumPolizasEmitidasFinal().compareTo(a.getNumPolizasEmitidasFinal())==0){
											if(numPolEmi==0){
													valorRetornoImportePrima=comparaRangosDependientes(a.getImportePrimaInicial(),a.getImportePrimaFinal(),b.getImportePrimaInicial(),b.getImportePrimaFinal());
													if(valorRetornoImportePrima==0){
														throw new Exception("Error en la configuracion de Rangos, Favor de verificar");												
													}
												}
//											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/**
	 * 
	 * @param configBonoRangoAplicaList
	 * @throws Exception
	 * valida los rangos del % de crecimiento
	 */
	public void compararRangosPctCrecimiento(List<ConfigBonoRangoAplica> configBonoRangoAplicaList) throws Exception{
		
		Collections.sort(configBonoRangoAplicaList,  
				new Comparator<ConfigBonoRangoAplica>(){
			public int compare(ConfigBonoRangoAplica object1,ConfigBonoRangoAplica object2) {
				if(object1==null && object2==null){
					return 0;
				}else if(object1==null){
					return -1;
				}else if(object2==null){
					return 1;
				}else if(object1.getPctCrecimientoInicial()==null && object2.getPctCrecimientoInicial()==null){
					return 0;
			    }else if(object1.getPctCrecimientoInicial()==null){
						return -1;
				}else if(object2.getPctCrecimientoInicial()==null){
					return 1;
				}else{
					return object1.getPctCrecimientoInicial().compareTo(object2.getPctCrecimientoInicial());
				}
			}
		});
		int cont = 0;
		int valorRetornoImportePrima;
		int pctSiniestralidad;
		int pctCrecimiento;
		int pctSupMeta;
		int numPolEmi;
		configBonoRangoAplicaList = quitarNulos(configBonoRangoAplicaList);
		
		if (!configBonoRangoAplicaList.isEmpty() && configBonoRangoAplicaList.size()>1){
			for(ConfigBonoRangoAplica a : configBonoRangoAplicaList){
				if(isNotNull(a)){
					cont++;
					if(cont<=configBonoRangoAplicaList.size()-1){
						ConfigBonoRangoAplica b =configBonoRangoAplicaList.get(cont);
						if(cont < configBonoRangoAplicaList.size()){
							//**********% crecimiento**********
						if(isNotNull(a.getPctCrecimientoInicial()) && isNotNull(a.getPctCrecimientoFinal()) && 
							isNotNull(b.getPctCrecimientoInicial()) && isNotNull(b.getPctCrecimientoFinal())){
							 //se valida el % de crecimiento
								pctCrecimiento = compararDosRangosBigdecimal(a.getPctCrecimientoInicial(),a.getPctCrecimientoFinal(),
										b.getPctCrecimientoInicial(),b.getPctCrecimientoFinal());
								if(pctCrecimiento==-1){
									throw new Exception("Error en la configuracion de Rangos, Favor de verificar");
								}
								if(pctCrecimiento==0){
									pctSupMeta = comparaRangosDependientes(a.getPctSupMetaInicial(),a.getPctSupMetaFinal(),
											b.getPctSupMetaInicial(),b.getPctSupMetaFinal());
									if(pctSupMeta==0){
//										if(isNotNull(b.getNumPolizasEmitidasInicial())&&isNotNull(a.getNumPolizasEmitidasInicial())&&isNotNull(b.getNumPolizasEmitidasFinal())&&isNotNull(a.getNumPolizasEmitidasFinal())){
										numPolEmi = compararDosRangosLong(a.getNumPolizasEmitidasInicial(),a.getNumPolizasEmitidasFinal(),b.getNumPolizasEmitidasInicial(),b.getNumPolizasEmitidasFinal());
//											if(b.getNumPolizasEmitidasInicial().compareTo(a.getNumPolizasEmitidasInicial())==0 && b.getNumPolizasEmitidasFinal().compareTo(a.getNumPolizasEmitidasFinal())==0){
										if(numPolEmi==0){
												pctSiniestralidad = comparaRangosDependientes(a.getPctSiniestralidadInicial(),a.getPctSiniestralidadFinal(),
														b.getPctSiniestralidadInicial(),b.getPctSiniestralidadFinal());
												if(pctSiniestralidad==0){
													valorRetornoImportePrima = comparaRangosDependientes(a.getImportePrimaInicial(),a.getImportePrimaFinal(),
															b.getImportePrimaInicial(),b.getImportePrimaFinal());
													if(valorRetornoImportePrima==0){
														throw new Exception("Error en la configuracion de Rangos, Favor de verificar");												
													}
												}
											}
//										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/**
	 * @param configBonoRangoAplicaList
	 * @throws Exception
	 * valida los rangos de superacion de meta
	 */
	public void compararRangosPctSupMeta(List<ConfigBonoRangoAplica> configBonoRangoAplicaList) throws Exception{
		
		Collections.sort(configBonoRangoAplicaList,  
				new Comparator<ConfigBonoRangoAplica>(){
			public int compare(ConfigBonoRangoAplica object1,ConfigBonoRangoAplica object2) {
				if(object1==null && object2==null){
					return 0;
				}else if(object1==null){
					return -1;
				}else if(object2==null){
					return 1;
				}else if(object1.getPctSupMetaInicial()==null && object2.getPctSupMetaInicial()==null){
					return 0;
			    }else if(object1.getPctSupMetaInicial()==null){
						return -1;
				}else if(object2.getPctSupMetaInicial()==null){
					return 1;
				}else{
					return object1.getPctSupMetaInicial().compareTo(object2.getPctSupMetaInicial());
				}
			}
		});
		int cont = 0;
		int valorRetornoImportePrima;
		int pctSiniestralidad;
		int pctCrecimiento;
		int pctSupMeta;
		int noPolEmitPag;
		int numPolEmi;
		configBonoRangoAplicaList = quitarNulos(configBonoRangoAplicaList);
		
		if (!configBonoRangoAplicaList.isEmpty() && configBonoRangoAplicaList.size()>1){
			for(ConfigBonoRangoAplica a : configBonoRangoAplicaList){
				if(isNotNull(a)){
					cont++;
					if(cont<=configBonoRangoAplicaList.size()-1){
						ConfigBonoRangoAplica b =configBonoRangoAplicaList.get(cont);
						if(cont < configBonoRangoAplicaList.size()){
							//**********% sup. de meta**********
						if(isNotNull(a.getPctSupMetaInicial()) && isNotNull(a.getPctSupMetaFinal()) && 
							isNotNull(b.getPctSupMetaInicial()) && isNotNull(b.getPctSupMetaFinal())){
							 //se valida el % sup. de meta
								pctSupMeta = compararDosRangosBigdecimal(a.getPctSupMetaInicial(),a.getPctSupMetaFinal(),
										b.getPctSupMetaInicial(),b.getPctSupMetaFinal());
								if(pctSupMeta==-1){
									throw new Exception("Error en la configuracion de Rangos, Favor de verificar");
								}
								if(pctSupMeta==0){
									numPolEmi = compararDosRangosLong(a.getNumPolizasEmitidasInicial(),a.getNumPolizasEmitidasFinal(),b.getNumPolizasEmitidasInicial(),b.getNumPolizasEmitidasFinal());
//									if(isNotNull(b.getNumPolizasEmitidasInicial())&&isNotNull(a.getNumPolizasEmitidasInicial())&&isNotNull(b.getNumPolizasEmitidasFinal())&&isNotNull(a.getNumPolizasEmitidasFinal())){
//										if(b.getNumPolizasEmitidasInicial().compareTo(a.getNumPolizasEmitidasInicial())==0 && b.getNumPolizasEmitidasFinal().compareTo(a.getNumPolizasEmitidasFinal())==0){
									if(numPolEmi==0){
											pctCrecimiento = comparaRangosDependientes(a.getPctCrecimientoInicial(),a.getPctCrecimientoFinal(),
													b.getPctCrecimientoInicial(),b.getPctCrecimientoFinal());
											if(pctCrecimiento==0){
												pctSiniestralidad = comparaRangosDependientes(a.getPctSiniestralidadInicial(),a.getPctSiniestralidadFinal(),
														b.getPctSiniestralidadInicial(),b.getPctSiniestralidadFinal());
												if(pctSiniestralidad==0){
													valorRetornoImportePrima = comparaRangosDependientes(a.getImportePrimaInicial(),a.getImportePrimaFinal(),
															b.getImportePrimaInicial(),b.getImportePrimaFinal());
													if(valorRetornoImportePrima==0){
														throw new Exception("Error en la configuracion de Rangos, Favor de verificar");												
													}
												}
											}
										}
//									}
								}
							}
						}
					}
				}
			}
		}
	}
	public void compararRangosNoPolEmitPag(List<ConfigBonoRangoAplica> configBonoRangoAplicaList) throws Exception{
		
		Collections.sort(configBonoRangoAplicaList,  
				new Comparator<ConfigBonoRangoAplica>(){
			public int compare(ConfigBonoRangoAplica object1,ConfigBonoRangoAplica object2) {
				if(object1==null && object2==null){
					return 0;
				}else if(object1==null){
					return -1;
				}else if(object2==null){
					return 1;
				}else if(object1.getNumPolizasEmitidasInicial()==null && object2.getNumPolizasEmitidasInicial()==null){
					return 0;
			    }else if(object1.getNumPolizasEmitidasInicial()==null){
						return -1;
				}else if(object2.getNumPolizasEmitidasInicial()==null){
					return 1;
				}else{
					return object1.getNumPolizasEmitidasInicial().compareTo(object2.getNumPolizasEmitidasInicial());
				}
			}
		});
		int cont = 0;
		int valorRetornoImportePrima;
		int pctSiniestralidad;
		int pctCrecimiento;
		int pctSupMeta;
		int noPolEmitPag;
		configBonoRangoAplicaList = quitarNulos(configBonoRangoAplicaList);
		
		if (!configBonoRangoAplicaList.isEmpty() && configBonoRangoAplicaList.size()>1){
			for(ConfigBonoRangoAplica a : configBonoRangoAplicaList){
				if(isNotNull(a)){
					cont++;
					if(cont<=configBonoRangoAplicaList.size()-1){
						ConfigBonoRangoAplica b =configBonoRangoAplicaList.get(cont);
						if(cont < configBonoRangoAplicaList.size()){
							//**********% sup. de meta**********
						if(isNotNull(a.getNumPolizasEmitidasInicial()) && isNotNull(a.getNumPolizasEmitidasFinal()) && 
							isNotNull(b.getNumPolizasEmitidasInicial()) && isNotNull(b.getNumPolizasEmitidasFinal())){
							 //se valida el % sup. de meta
								noPolEmitPag = compararDosRangosLong(a.getNumPolizasEmitidasInicial(),a.getNumPolizasEmitidasFinal(),
										b.getNumPolizasEmitidasInicial(),b.getNumPolizasEmitidasFinal());
								if(noPolEmitPag==-1){
									throw new Exception("Error en la configuracion de Rangos, Favor de verificar");
								}
								if(noPolEmitPag==0){
									pctSupMeta = comparaRangosDependientes(a.getPctSupMetaInicial(),a.getPctSupMetaFinal(),
											b.getPctSupMetaInicial(),b.getPctSupMetaFinal());
									if(pctSupMeta==0){
										if(isNotNull(b.getNumPolizasEmitidasInicial())&&isNotNull(a.getNumPolizasEmitidasInicial())&&isNotNull(b.getNumPolizasEmitidasFinal())&&isNotNull(a.getNumPolizasEmitidasFinal())){
											if(b.getNumPolizasEmitidasInicial()==a.getNumPolizasEmitidasInicial() && b.getNumPolizasEmitidasFinal()==a.getNumPolizasEmitidasFinal()){
												pctCrecimiento = comparaRangosDependientes(a.getPctCrecimientoInicial(),a.getPctCrecimientoFinal(),
														b.getPctCrecimientoInicial(),b.getPctCrecimientoFinal());
												if(pctCrecimiento==0){
													pctSiniestralidad = comparaRangosDependientes(a.getPctSiniestralidadInicial(),a.getPctSiniestralidadFinal(),
															b.getPctSiniestralidadInicial(),b.getPctSiniestralidadFinal());
													if(pctSiniestralidad==0){
														valorRetornoImportePrima = comparaRangosDependientes(a.getImportePrimaInicial(),a.getImportePrimaFinal(),
																b.getImportePrimaInicial(),b.getImportePrimaFinal());
														if(valorRetornoImportePrima==0){
															throw new Exception("Error en la configuracion de Rangos, Favor de verificar");												
														}
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	/**
	 * 
	 * @param a1
	 * @param a2
	 * @param b1
	 * @param b2
	 * @return 0 si los rangos son iguales
	 * 		  -1 si contiene error
	 * 		   1 si todo va bien
	 * 		   2 cuando alguno de los elementos contiene 0 (null)
	 */
	
	private  Integer compararDosRangosBigdecimal(BigDecimal a1, BigDecimal a2, BigDecimal b1, BigDecimal b2){
		if(isNotNull(a1) && isNotNull(a2) && isNotNull(b1) && isNotNull(b2)){
			if(a1.compareTo(b1)==0 && a2.compareTo(b2)==0){
				return 0;//se revisan los rangos dependientes del row
			}
			if(b1.compareTo(a2)==0){
				return -1;//ERROR
			}
			if(//b1.compareTo(a2)==1 && b1.compareTo(a2.add(new BigDecimal(0.01).setScale(2, RoundingMode.DOWN)))==-1 ||
					 b1.compareTo(a2.add(new BigDecimal(0.01).setScale(2, RoundingMode.DOWN)))==0){
				return 1;//OK
			}else
			if(//a1.compareTo(b2)==1 && a1.compareTo(b2.add(new BigDecimal(0.01).setScale(2,RoundingMode.DOWN)))==-1 ||
					 a1.compareTo(b2.add(new BigDecimal(0.01)))==0){
				return 1;//OK
			}else{
				return -1;
			}
		}else if((isNotNull(a1) && isNotNull(a2)) || (isNotNull(b1) && isNotNull(b2))){
			return 1;
		}else{
			return 0;
		}
	}
	private int comparaRangosDependientes(BigDecimal a1, BigDecimal a2, BigDecimal b1, BigDecimal b2){
		if(isNotNull(a1) && isNotNull(a2) && isNotNull(b1) && isNotNull(b2)){
			if(a1.compareTo(b1)==0 && a2.compareTo(b2)==0){
				return 0;//se revisan los rangos dependientes del row
			}else{ 
				return 1;
			}
		}
		else 
			if((isNotNull(a1) && isNotNull(a2)) || (isNotNull(b1) && isNotNull(b2))){
			return 1;
		}else{
			return 0;
		}
	}
	/**
	 * 
	 * @param a1
	 * @param a2
	 * @param b1
	 * @param b2
	 * @return 0 si los rangos son iguales
	 * 		  -1 si contiene error
	 * 		   1 si todo va bien
	 * 		   2 cuando alguno de los elementos contiene 0 (null)
	 */
	private  Integer compararDosRangosLong(Integer a1, Integer a2, Integer b1, Integer b2){
		System.out.println("-a1: "+a1+" -a2: "+a2+" -b1: "+b1+" -b2: "+b2);
		if(isNotNull(a1) && isNotNull(a2) && isNotNull(b1) && isNotNull(b2)){
//			if(a1.compareTo(0)==0 && a2.compareTo(0)==0 || b1.compareTo(0)==0 && b2.compareTo(0)==0){
//				return 1;
//			}
			if(a1.compareTo(b1)==0 && a2.compareTo(b2)==0){
				return 0;
			}
			if(b1.compareTo(a2+1)==0){
				return 1;
			}else if(a1.compareTo(b2+1)==0){
				return 1;
			}else{
				return -1; 
			}
		}else 
		if((isNotNull(a1) && isNotNull(a2)) || (isNotNull(b1) && isNotNull(b2))){
			System.out.println("a1: "+a1+" a2: "+a2+" b1: "+b1+" b2: "+b2);
			return 1;
		}else{
			return 0;
		}
	}
	private List<ConfigBonoRangoAplica> quitarNulos(List<ConfigBonoRangoAplica> lista){
		List<ConfigBonoRangoAplica> listaNueva = new ArrayList<ConfigBonoRangoAplica>();
		for(ConfigBonoRangoAplica obj :lista){
			if (isNotNull(obj)){
				listaNueva.add(obj);
			}
		}
		return listaNueva;
	}
	
	public void noExisteBonificacion(List<ConfigBonoRangoAplica> configBonoRangoAplicaList) throws Exception{
		for(ConfigBonoRangoAplica obj:configBonoRangoAplicaList){
			int cont = 0;
			if(obj!=null){
				if(obj.getValorAplicacion()==null){
					throw new Exception("El campo Bonificacion no puede ser Vacio");
				}
				if(obj.getImportePrimaFinal()==null){
					cont = cont+1;
				}
				if(obj.getImportePrimaInicial()==null){
					cont = cont+1;
				}
				if(obj.getNumPolizasEmitidasFinal()==null){
					cont = cont+1;
				}
				if(obj.getNumPolizasEmitidasInicial()==null){
					cont = cont+1;
				}
				if(obj.getPctCrecimientoFinal()==null){
					cont = cont+1;
				}
				if(obj.getPctCrecimientoInicial()==null){
					cont = cont+1;
				}
				if(obj.getPctSiniestralidadFinal()==null){
					cont = cont+1;
				}
				if(obj.getPctSiniestralidadInicial()==null){
					cont = cont+1;
				}
				if(obj.getPctSupMetaFinal()==null){
					cont = cont+1;
				}
				if(obj.getPctSupMetaInicial()==null){
					cont = cont+1;
				}
				if(cont==10){
					throw new Exception("Error en la configuracion de Rangos, Favor de verificar");
				}
			}
		}
	}
}
