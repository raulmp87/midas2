package mx.com.afirme.midas2.dao.impl.catalogos.estiloVehiculo;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.catalogos.estiloVehiculo.EstiloVehiculoDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;

@Stateless
public class EstiloVehiculoDaoImpl  extends JpaDao<EstiloVehiculoId, EstiloVehiculoDTO> implements EstiloVehiculoDao  {
	
	private EntidadDao entidadDao;
	
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	@Override
	public String getDescripcionPorClaveAmis(String claveEstilo) {
		List<EstiloVehiculoDTO> descEstilo; 
		try{
			descEstilo = entidadDao.findByProperty(EstiloVehiculoDTO.class, "id.claveEstilo", claveEstilo);
		}catch(RuntimeException e){
			throw e;
		}
		if(descEstilo != null && !descEstilo.isEmpty()){
			return descEstilo.get(0).getDescripcionEstilo();
		}else{
			descEstilo = new ArrayList<EstiloVehiculoDTO>();
		}
		return descEstilo.get(0).getDescripcionEstilo();
	}

}
