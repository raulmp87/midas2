package mx.com.afirme.midas2.dao.impl.cargos;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.cargos.DetalleCargosDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.impl.prestamos.PagarePrestamoAnticipoDaoImpl;
import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.domain.cargos.DetalleCargos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.Cargos.DetalleCargosView;
import mx.com.afirme.midas2.service.cargos.ConfigCargosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

@Stateless
public class DetalleCargosDaoImpl extends EntidadDaoImpl implements DetalleCargosDao {

	private EntidadService entidadService;
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	private ValorCatalogoAgentesService catalogoService;
	private AgenteMidasService agenteMidasService;
	private ConfigCargosService configCargosService;
	
	private static String CLAVE_TRANSACCION_AGENTES="AGT";
	private static String CLAVE_CONCEPTO_TRANSACCION="AGCOB";
	private static String USUARIO_TRANSACCION="MIDAS";
	private static String EMPRESA_DEFAULT="8";
	private static String CLAVE_CLASE_AGENTE_ABONO="A";
	private static String CLAVE_CLASE_AGENTE_CARGO="C";
	private static String SITUACION_MOVIMIENTO="PEN";
	
	//****************setter and getter******************************
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	@EJB
	public void setConfigCargosService(ConfigCargosService configCargosService) {
		this.configCargosService = configCargosService;
	}

	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	private Object val(Object str){
		return (str!=null)?str:"";
	}	
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}

	@Override
	public boolean llenarYGuardarListaDetalles(String detalleCargo, ConfigCargos config)throws Exception {
		try{
			int cont=0;
			if(detalleCargo!=null && !detalleCargo.equals("")){
				List<DetalleCargos> listadetalleCargos = new ArrayList<DetalleCargos>();
				listadetalleCargos=loadByIdConfigCargos(config);
				if(!listadetalleCargos.isEmpty()){
					if (cont==0 && !listadetalleCargos.isEmpty()){
						for(DetalleCargos objDetalleCargos:listadetalleCargos){
							entidadService.remove(objDetalleCargos);
							cont++;
						}
					}
				}
				
				String[]arrayPagares=detalleCargo.split(",");
					for(int i=0;i<=arrayPagares.length-1;i+=7){
						DetalleCargos detalleCargosGuardar=new DetalleCargos();
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						Date  fechCargo = df.parse(arrayPagares[i+2]);
						Double capitalInicial=(isValid(arrayPagares[i+3]))?Double.parseDouble(arrayPagares[i+3]):null;
						Double importeCargo = (isValid(arrayPagares[i+4])) ? Double.parseDouble(arrayPagares[i+4]) : null;
						Double importeRestante = (isValid(arrayPagares[i+5])) ? Double.parseDouble(arrayPagares[i+5]) : null;
						
						ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();
						estatus = valorCatalogoAgentesService.obtenerElementoEspecifico("Estatus Cargo de Agentes", arrayPagares[i+6]);
						detalleCargosGuardar.setFechaCargo(fechCargo);
						detalleCargosGuardar.setCapitalInicial(capitalInicial);
						detalleCargosGuardar.setImporteCargo(importeCargo);
						detalleCargosGuardar.setImporteRestante(importeRestante);
						
						detalleCargosGuardar.setConfigCargos(config);
						detalleCargosGuardar.setEstatus(estatus);
//						System.out.println(detalleCargosGuardar.getFechaCargo()+" "+detalleCargosGuardar.getCapitalInicial()+" "+detalleCargosGuardar.getImporteCargo()+" "+detalleCargosGuardar.getImporteRestante()+" "+detalleCargosGuardar.getConfigCargos().getId()+" "+detalleCargosGuardar.getEstatus().getValor());
						entidadService.save(detalleCargosGuardar);
						cont=0;
					}			
				}
			return true;
			}catch(Exception e){
				return false;
			}
	}

	@Override
	public DetalleCargos loadById(DetalleCargos arg0) throws Exception {
		return null;
	}

	@Override
	public List<DetalleCargos> loadByIdConfigCargos(ConfigCargos arg0)throws Exception {
		
		List<DetalleCargos> detCargos = new ArrayList<DetalleCargos>();
		detCargos = entidadService.findByProperty(DetalleCargos.class, "configCargos.id", arg0.getId());
		return detCargos;
	}

	@Override
	public DetalleCargos save(DetalleCargos arg0, Long arg1) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DetalleCargos save(List<DetalleCargos> arg0, Long arg1)throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public DetalleCargos updateEstatusDetalleCargos(DetalleCargos ObjDetalleCargos,String arg1) throws Exception {
		LogDeMidasInterfaz.log("Entrando a DetalleCargosDaoImpl.updateEstatusDetalleCargos..." + this, Level.INFO, null);
		if(ObjDetalleCargos!=null && !"".equals(arg1)){
			ValorCatalogoAgentes estatus = valorCatalogoAgentesService.obtenerElementoEspecifico("Estatus Cargo de Agentes", arg1);
			ObjDetalleCargos.setEstatus(estatus);
			entidadService.save(ObjDetalleCargos);
		}
		LogDeMidasInterfaz.log("Saliendo de DetalleCargosDaoImpl.updateEstatusDetalleCargos..." + this, Level.INFO, null);
		return ObjDetalleCargos;
	}

	@Override
	public List<DetalleCargos> updateEstatusDetalleCargos(List<DetalleCargos> listDetalleCargos, String arg1) throws Exception {
		ConfigCargos configCargos = new ConfigCargos();
		List<DetalleCargos> listaDetalle = new ArrayList<DetalleCargos>();
		for (DetalleCargos objDetalleCargo: listDetalleCargos){
			updateEstatusDetalleCargos(objDetalleCargo,arg1);
		}
//		listaDetalle = loadByIdConfigCargos();
		return listDetalleCargos;
	}

	@Override
	public void ejecutarCargo() throws Exception{
		LogDeMidasInterfaz.log("entrando a DetalleCargosDaoImpl.ejecutarCargo... "+ this, Level.INFO, null);
		List<DetalleCargosView> listaDetalleCargosView = new ArrayList<DetalleCargosView>();
		
		ValorCatalogoAgentes estatusActivo = catalogoService.obtenerElementoEspecifico("Estatus Cargo de Agentes", "ACTIVO");
		StringBuilder queryString = new StringBuilder();
		queryString.append("select ID as id, IDCONFIGCARGOS as idConfigCargos from MIDAS.toDetalleCArgos "); 
		queryString.append(" where IDESTATUS = "+estatusActivo.getId());
		queryString.append(" and TO_CHAR(FECHACARGO, 'DD-MM-YYYY') = TO_CHAR(SYSDATE, 'DD-MM-YYYY') ");
		
		Query query = entityManager.createNativeQuery(queryString.toString(),DetalleCargosView.class);
		listaDetalleCargosView=query.getResultList();
		if(!listaDetalleCargosView.isEmpty()){
			
			iterarAplicarMovtoEdocta(getListaDetalleCargos(listaDetalleCargosView), CLAVE_CLASE_AGENTE_CARGO);
		}
		LogDeMidasInterfaz.log("saliendo de DetalleCargosDaoImpl.ejecutarCargo... "+ this, Level.INFO, null);
	}
	public List<DetalleCargos> getListaDetalleCargos(List<DetalleCargosView> lista){
		List<DetalleCargos> listaDetalleCargos = new ArrayList<DetalleCargos>();
		for(DetalleCargosView obj : lista){
			DetalleCargos obj2 = new DetalleCargos();
			ConfigCargos configcargos = new ConfigCargos();
			configcargos.setId(obj.getIdConfigCargos());
			obj2.setConfigCargos(configcargos);
			obj2.setId(obj.getId());
			listaDetalleCargos.add(obj2);
		}
		return listaDetalleCargos;
	}
	@Override
	public void aplicarCargosEdocta(ConfigCargos config, DetalleCargos objDetalleCargos, String cargoAbono) throws Exception{
		LogDeMidasInterfaz.log("aplicarCargosEdocta "+ this, Level.INFO, null);
		ValorCatalogoAgentes estatusActivo = catalogoService.obtenerElementoEspecifico("Estatus Cargo de Agentes", "ACTIVO");
		if(isNotNull(objDetalleCargos) && isNotNull(objDetalleCargos.getId())){
			objDetalleCargos = this.entidadService.findById(DetalleCargos.class, objDetalleCargos.getId());
		}
		if(estatusActivo.getId()!=config.getEstatusCargo().getId()){
		
			if(objDetalleCargos!=null && objDetalleCargos.getId()!=null){
				Long idAgente = config.getAgente().getId();
				Agente agente=null;
				if(isNotNull(idAgente)){
					agente=agenteMidasService.loadById(config.getAgente());
				}
				String nombreAgente=(isNotNull(agente) && isNotNull(agente.getPersona()))?agente.getPersona().getNombreCompleto():null;
				String tipoCArgo = config.getTipoCargo().getValor();
				StoredProcedureHelper storedHelper = null;
				String sp="MIDAS.PKGCALCULOS_AGENTES.stp_insertaMovimientoAgente";//"SEYCOS.pkg_int_midas_E2.stpInserta_Movto_Edocta_midas
				try {
					LogDeMidasInterfaz.log("Entrando a DetalleCargosDaoImpl.aplicarCargo..." + this, Level.INFO, null);
					ValorCatalogoAgentes tipoMoneda=catalogoService.obtenerElementoEspecifico("Tipo Moneda", "NACIONAL");
					Long idTipoAgente=agente.getIdTipoAgente();
					ValorCatalogoAgentes tipoAgente=new ValorCatalogoAgentes();
					if(isNotNull(idTipoAgente)){
						tipoAgente.setId(idTipoAgente);
						tipoAgente=catalogoService.loadById(tipoAgente);
					}
					String claveTipoAgente=(isNotNull(tipoAgente) && isValid(tipoAgente.getClave()))?tipoAgente.getClave().trim():null;
					Long idTipoMoneda=(isNotNull(tipoMoneda) && isNotNull(tipoMoneda.getClave()))?Long.parseLong(tipoMoneda.getClave()):null;
					Long claveAgente=(isNotNull(agente))?agente.getIdAgente():null;
					ValorCatalogoAgentes concepto=catalogoService.obtenerElementoEspecifico("Concepto Movimiento Agente", "CARGOS");
					/*se utilizo el concepto de CARGO IVA AGENTES para cargos*/
					Long idConcepto = concepto.getIdRegistro();
					
					Calendar ahora = Calendar.getInstance();
					Integer anio = ahora.get(Calendar.YEAR);
					Integer mes = ahora.get(Calendar.MONTH) + 1;
					
					String mesCompleto="";
					if(mes<=9){
						mesCompleto = "0"+mes;
					}else{
						mesCompleto = Integer.toString(mes);
					}
						
					String anioMes = Integer.toString(anio)+mesCompleto;
					ValorCatalogoAgentes estatusMovimiento=catalogoService.obtenerElementoEspecifico("Situacion Movimiento Agente", "PENDIENTE");
					Long idEstatusMovimiento = estatusMovimiento.getId();
					
					storedHelper = new StoredProcedureHelper(sp);
					storedHelper.estableceParametro("pId_Empresa",EMPRESA_DEFAULT);
					storedHelper.estableceParametro("pId_Agente",val(claveAgente));
					storedHelper.estableceParametro("pF_Corte_Edocta",objDetalleCargos.getFechaCargo());//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD, se validó con Juan Manuel que se inserta la fecha actual
					storedHelper.estableceParametro("pIdConsecMovto",null);
					storedHelper.estableceParametro("pAno_Mes",anioMes);////////////HJC 23/05/14: No importa lo que se envíe, se calcula el periodo contable abierto en la BD
					storedHelper.estableceParametro("pCve_T_Cpto_A_C","MISC");//FIXME que debe de ir aqui
					storedHelper.estableceParametro("pId_Concepto",idConcepto);/////////FIXME De donde viene este concepto, segun esto viene a nivel recibo, pero esto es acomulativo, cual de todos los conceptos de cada recibo pongo???
					storedHelper.estableceParametro("pCve_C_A",cargoAbono);
					storedHelper.estableceParametro("pId_Moneda",val(idTipoMoneda));//FIXME Revisar si es nacional siempre
					storedHelper.estableceParametro("pId_Moneda_Orig",val(idTipoMoneda));//FIXME Revisar si es nacional siempre
					storedHelper.estableceParametro("pTipo_Cambio",null);//FIXME como obtengo el tipo de cambio al dia?
					storedHelper.estableceParametro("pDesc_Movto",tipoCArgo);///////////nombreAgente
					storedHelper.estableceParametro("pId_Ramo_Contable",null);//null
					storedHelper.estableceParametro("pId_SubR_Contable",null);//null
					storedHelper.estableceParametro("pId_LIn_Negocio",null);
					storedHelper.estableceParametro("pF_Movto",new Date());//FIXME que fecha va aqui?//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
					storedHelper.estableceParametro("pId_Centro_Emis",null);
					storedHelper.estableceParametro("pNum_Poliza",null);
					storedHelper.estableceParametro("pNum_Renov_Pol",null);
					storedHelper.estableceParametro("pId_Cotizacion",null);
					storedHelper.estableceParametro("pId_Version_Pol",null);
					storedHelper.estableceParametro("pId_Centro_Emis_E",null);
					storedHelper.estableceParametro("pCve_T_Endoso",null);
					storedHelper.estableceParametro("pNum_Endoso",null);
					storedHelper.estableceParametro("pId_Solicitud",null);
					storedHelper.estableceParametro("pId_Version_End",null);
					storedHelper.estableceParametro("pReferencia","");
					storedHelper.estableceParametro("pId_Centro_Oper",null);
					storedHelper.estableceParametro("pId_Remesa","01");
					storedHelper.estableceParametro("pId_Consec_MovtoR",null);
					storedHelper.estableceParametro("pCve_Origen_Remes",null);
					storedHelper.estableceParametro("pSerie_Folio_Rbo",null);
					storedHelper.estableceParametro("pNum_Folio_Rbo",null);
					storedHelper.estableceParametro("pId_Recibo",null);
					storedHelper.estableceParametro("pId_Version_Rbo",null);
					storedHelper.estableceParametro("pF_Vencto_Recibo",null);
					storedHelper.estableceParametro("pPct_Partic_Agte",null);
					storedHelper.estableceParametro("pImp_Prima_Neta",null);
					storedHelper.estableceParametro("pImp_Rcgos_PagoFr",null);
					storedHelper.estableceParametro("pImp_Comis_Agte",objDetalleCargos.getImporteCargo());
					storedHelper.estableceParametro("pCve_Origen_Aplic","SU");
					storedHelper.estableceParametro("pCve_Origen_Movto","SUS");
					storedHelper.estableceParametro("pF_Corte_Pagocom",null);
					storedHelper.estableceParametro("pSit_Movto_Edocta",idEstatusMovimiento);
					storedHelper.estableceParametro("pId_Usuario_Integ",USUARIO_TRANSACCION);//FIXME revisar si puede ser midas.
					storedHelper.estableceParametro("pFH_Integracion",new Date());//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
					storedHelper.estableceParametro("pFH_Aplicacion",null);//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
					storedHelper.estableceParametro("pF_Transf_DepV",null);
					storedHelper.estableceParametro("pid_transac_depv",null);
					storedHelper.estableceParametro("pId_Transacc_Orig",objDetalleCargos.getId());
					storedHelper.estableceParametro("pId_Agente_Orig",val(claveAgente));
					storedHelper.estableceParametro("pCve_T_Cpto_A_C_O","AGAUT");
					storedHelper.estableceParametro("pId_Concepto_O",null);
					storedHelper.estableceParametro("pImp_Prima_Total",null);
					storedHelper.estableceParametro("pB_Facultativo","F");
					storedHelper.estableceParametro("pB_Masivo","F");
					storedHelper.estableceParametro("pId_Cobertura",null);
					storedHelper.estableceParametro("pAnio_Vig_Poliza","1");
					storedHelper.estableceParametro("pImp_Prima_D_CP",null);
					storedHelper.estableceParametro("pCve_Sist_Admon","NA");
					storedHelper.estableceParametro("pCve_Exp_Calc_Div","NA");
					storedHelper.estableceParametro("pImp_Base_Calculo",null);
					storedHelper.estableceParametro("pPctComisAgt",null);
					storedHelper.estableceParametro("pNum_Mov_Man_Agte",null);
					storedHelper.estableceParametro("pImpPagoAntImptos",null);
					storedHelper.estableceParametro("pPct_IVA",null);
					storedHelper.estableceParametro("ppct_iva_ret",null);
					storedHelper.estableceParametro("ppct_isr_ret",null);
					storedHelper.estableceParametro("pimp_iva",null);
					storedHelper.estableceParametro("ppimp_iva_ret",null);
					storedHelper.estableceParametro("pimp_isr",null);
					storedHelper.estableceParametro("pcve_sdo_impto ",null);
					storedHelper.estableceParametro("pB_Impuesto","F");
					storedHelper.estableceParametro("pAno_Mes_Pago",null);
					
					storedHelper.ejecutaActualizar();
					
					LogDeMidasInterfaz.log("Saliendo de DetalleCargosDaoImpl.aplicarCargo..." + this, Level.INFO, null);
					//Se actualiza el detalle a Aplicado
					updateEstatusDetalleCargos(objDetalleCargos, "APLICADO");
					//Si hay mas registros de esa configuracion se actualiza el siguiente registro a Activo
					activarSigRegistro(objDetalleCargos);
				
			
				} catch (SQLException e) {				
					Integer codErr = null;
					String descErr = null;
					if (storedHelper != null) {
						codErr = storedHelper.getCodigoRespuesta();
						descErr = storedHelper.getDescripcionRespuesta();
					}
					StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,PagarePrestamoAnticipoDaoImpl.class, codErr, descErr);
					LogDeMidasInterfaz.log("Excepcion en BD de DetalleCargosDaoImpl.aplicarCargo..." + this +" codErr = "+codErr+", descErr"+descErr, Level.WARNING, e);
					onError(e);
				} catch (Exception e) {
					LogDeMidasInterfaz.log("Excepcion general en DetalleCargosDaoImpl.aplicarCargo..." + this, Level.WARNING, e);
					onError(e);
				}
			}
		}
	}
	
	public DetalleCargos activarSigRegistro(DetalleCargos detalle){	
		LogDeMidasInterfaz.log("entrando a DetalleCargosDaoImpl.activarSigRegistro..." + this, Level.INFO, null);
		String queryString;
		try {
			ValorCatalogoAgentes estatusActivo = catalogoService.obtenerElementoEspecifico("Estatus Cargo de Agentes", "PENDIENTE");
			HashMap<String,Object> properties = new HashMap<String,Object>();
			properties.put("configCargos.id",detalle.getConfigCargos().getId());
			properties.put("estatus.id",estatusActivo.getId());
			List<DetalleCargos> listaDetalles = entidadService.findByPropertiesWithOrder(DetalleCargos.class, properties, "id ASC");
			if(!listaDetalles.isEmpty()){
				updateEstatusDetalleCargos(listaDetalles.get(0), "ACTIVO");
			}
			LogDeMidasInterfaz.log("Saliendo de DetalleCargosDaoImpl.activarSigRegistro..." + this, Level.INFO, null);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LogDeMidasInterfaz.log("Error en DetalleCargosDaoImpl.activarSigRegistro..." + this, Level.INFO, e);
		}
		return null;
	}
	
	@Override
	public void iterarAplicarMovtoEdocta(List<DetalleCargos> listaDetalleCargos, String tipoMovto) throws Exception{
		LogDeMidasInterfaz.log("entrando a DetalleCargosDaoImpl.iterarAplicarMovtoEdocta... "+ this, Level.INFO, null);
		ValorCatalogoAgentes estatusActvo = catalogoService.obtenerElementoEspecifico("Estatus Cargo de Agentes", "ACTIVO");
		for(DetalleCargos objDetalleCargos :listaDetalleCargos){
			ConfigCargos config = new ConfigCargos();
			config = configCargosService.loadById(objDetalleCargos.getConfigCargos());
			objDetalleCargos.setEstatus(estatusActvo);
			aplicarCargosEdocta(config, objDetalleCargos,tipoMovto);
		}
		LogDeMidasInterfaz.log("saliendo de DetalleCargosDaoImpl.iterarAplicarMovtoEdocta... "+ this, Level.INFO, null);
	}
	
	@Override
	public List<DetalleCargos> cancelarMovimientoAplicado(ConfigCargos configCargos)throws Exception {
		StringBuilder queryString = new StringBuilder();
		List <DetalleCargos>listaDetalleCargos = new ArrayList<DetalleCargos>();
		
		queryString.append("select * from MIDAS.toDetalleCArgos where IDESTATUS IN (select id from MIDAS.tovalorCatalogoAgentes ");
		queryString.append("Where VALOR in ('ACTIVO','COBRADO') and GRUPOCATALOGOAGENTES_ID = (select ID from MIDAS.tcGrupoCatalogoAgentes where DESCRIPCION = 'Estatus del Pagare de Prestamos')) ");
		queryString.append("and IDCONFIGCARGOS = "+configCargos.getId());
		
		Query query = entityManager.createNativeQuery(queryString.toString(),DetalleCargos.class);
		
		listaDetalleCargos=query.getResultList();
		if(!listaDetalleCargos.isEmpty()){
			iterarAplicarMovtoEdocta(listaDetalleCargos, CLAVE_CLASE_AGENTE_ABONO);
		}
		return null;
	}
}
