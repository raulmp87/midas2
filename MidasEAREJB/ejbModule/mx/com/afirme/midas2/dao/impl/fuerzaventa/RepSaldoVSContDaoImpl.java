package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.StoredProcedureHelperSQL;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.RepSaldoVSContDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dto.reportesAgente.RepMizarSelectMizar;
import mx.com.afirme.midas2.dto.reportesAgente.SaldosVsContabilidadView;

@Stateless
public class RepSaldoVSContDaoImpl extends EntidadDaoImpl implements RepSaldoVSContDao{
	@Override
	public <E extends Entidad, K> E evictAndFindById(Class<E> arg0, K arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> void executeActionGrid(String arg0, E arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public List executeNativeQueryMultipleResult(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object executeNativeQuerySimpleResult(String arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List executeQueryMultipleResult(String arg0, Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Object executeQuerySimpleResult(String arg0, Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findAll(Class<E> arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> arg0, String arg1, Map<String, Object> arg2,
			Map<String, Object> arg3, StringBuilder arg4, StringBuilder arg5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> arg0, Map<String, Object> arg1, Map<String, Object> arg2,
			StringBuilder arg3, StringBuilder arg4) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad, K> E findById(Class<E> arg0, K arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByProperty(Class<E> arg0,
			String arg1, Object arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad, K> E getReference(Class<E> arg0, K arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> void persist(E arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <E extends Entidad> Object persistAndReturnId(E arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> void refresh(E arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <E extends Entidad> void remove(E arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public <E extends Entidad> E update(E arg0) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void updateWithQuery(String arg0, Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void deleteTPSaldosVsContabilidad() throws Exception {
		StringBuilder queryInsertString = new StringBuilder();
		queryInsertString.append("DELETE FROM midas.g_saldos_1622 "); 
		Query queryInsert = entityManager.createNativeQuery(queryInsertString.toString());
		queryInsert.executeUpdate();
		entityManager.flush();
	}

	@Override
	public void insertTPSaldosVsContabilidad(List<RepMizarSelectMizar> lista)
			throws Exception {
		LogDeMidasInterfaz.log("Entrando al metodo insertTPSaldosVsContabilidad... "+lista.size()+" " + this, Level.INFO, null);
		String queryString = "";
		if(!lista.isEmpty()){
			for (RepMizarSelectMizar agente : lista) {
				queryString =" INSERT INTO midas.g_saldos_1622 (ID_AGENTE, COB_MIZAR,AGT_MIZAR, GG_MIZAR)" +
				" VALUES("+agente.getAgente()+", "+agente.getCob_importe()+", "+agente.getAgt_importe()+", "+agente.getGg_importe()+") ";
				Query query = entityManager.createNativeQuery(queryString);
				query.executeUpdate();
				entityManager.flush();
			}
		}
		LogDeMidasInterfaz.log("Saliendo del metodo insertTPSaldosVsContabilidad..." + this, Level.INFO, null);
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<RepMizarSelectMizar> consultaMizar(String anioMes, String fechaString)
			throws Exception {
		LogDeMidasInterfaz.log("entrando al metodo RepSaldoVSContDaoImpl.consultaMizar()..." + this, Level.INFO, null);
		StoredProcedureHelperSQL interfazSql = new StoredProcedureHelperSQL();
		List<RepMizarSelectMizar> lista = new ArrayList<RepMizarSelectMizar>();
		StringBuilder stm = new StringBuilder();
//		stm.append(" select case when isnumeric(agente) = 1 then agente else 0 end agente, ");
//		stm.append(" sum(case when tipo = 'COB' then importe else 0 end) cob_importe, ");
//		stm.append(" sum(case when tipo = 'AGT' then importe else 0 end) agt_importe, ");
//		stm.append(" sum(case when tipo = 'GG' then importe else 0 end) gg_importe ");
//		stm.append(" from ( ");
//		stm.append(" Select  Fecha         = convert(varchar(12),DMov.Fecha ,103), ");
//		stm.append(" substring(mpol.pol,1,charindex('-',mpol.pol)-1) tipo, ");
//		stm.append(" substring(mpol.pol,charindex('-',mpol.pol)+1,10) polizamizar, ");
//		stm.append(" mpol.concepto, ");
//		stm.append(" case when mpol.pol like 'AGT%' then ");
//		stm.append(" dmov.concepto+'/Agt-'+substring(mpol.concepto,14,5) ");
//		stm.append(" when mpol.pol like 'GG%' then ");
//		stm.append(" substring(dmov.concepto,1,10)+'/Agt-'+substring(dmov.concepto,17,5) ");
//		stm.append(" when mpol.pol like 'COB%' then ");
//		stm.append(" dmov.concepto ");
//		stm.append(" end Concepto_mov, ");
//		stm.append(" case when mpol.pol like 'AGT%' then ");
//		stm.append(" substring(mpol.concepto,14,5) ");
//		stm.append(" when mpol.pol like 'GG%' then ");
//		stm.append(" substring(dmov.concepto,17,5) ");
//		stm.append(" when mpol.pol like 'COB%' then ");
//		stm.append(" substring(dmov.concepto,40,5) ");
//		stm.append(" else ");
//		stm.append(" '0' ");
//		stm.append(" end Agente, ");
//		stm.append(" MCta.Cta Cuenta, ");
//		stm.append(" dmov.mon, ");
//		stm.append(" dmov.Neto importeMO, ");
//		stm.append(" dmov.tcambio, ");
//		stm.append(" dmov.NetoCG importe ");
//		stm.append(" From       MCta with(nolock),  MPol with(nolock), DMov with(nolock) ");
//		stm.append(" Where    (MPol.Cia                = DMov.Cia) ");
//		stm.append(" And         (MPol.FPol              = DMov.FPol) ");
//		stm.append(" And         (MPol.NPol             = DMov.NPol) ");
//		stm.append(" And         (MPol.TPol              = DMov.TPol) ");
//		stm.append(" And         (DMov.Libro            = 'ME') ");
//		stm.append(" And         (MCta.IDCta            = DMov.IDCta) ");
//		stm.append(" And         (DMov.Cia               = 1)  ");
//		stm.append(" And         (DMov.Fecha          Between '"+anioMes+"/01' and '"+fechaString+"') "); //Armado dinamico.
//		stm.append(" And         (DMov.PFiscal        <= 12) ");
//		stm.append(" And         (DMov.STS              = '3-7') ");
//		stm.append(" and         (DMov.fpol  IN('GG','AGT','COB')) ");
//		stm.append(" And         left(MCta.Cta,4) = 1622 ");
//		stm.append(" ) a ");
//		stm.append(" group by case when isnumeric(agente) = 1 then agente else 0 end;");
		stm.append(" select case when isnumeric(agente) = 1 then agente else 0 end agente, ");
		stm.append(" sum(case when tipo = 'COB' then importe else 0 end) cob_importe, ");
		stm.append(" sum(case when tipo = 'AGT' then importe else 0 end) agt_importe, ");
		stm.append(" sum(case when tipo = 'GG' then importe else 0 end) gg_importe ");
		stm.append(" from ( ");
		stm.append(" Select Fecha = convert(varchar(12),DMov.Fecha ,103), ");
		stm.append(" substring(mpol.pol,1,charindex('-',mpol.pol)-1) tipo, ");
		stm.append(" substring(mpol.pol,charindex('-',mpol.pol)+1,10) polizamizar, ");
		stm.append(" mpol.concepto, ");
		stm.append(" case when mpol.pol like 'AGT%' then ");
		stm.append(" dmov.concepto+'/Agt-'+substring(mpol.concepto,23,5) ");
		stm.append(" when mpol.pol like 'GG%' then ");
		stm.append(" substring(dmov.concepto,1,10)+'/Agt-'+substring(dmov.concepto,22,5) ");
		stm.append(" when mpol.pol like 'COB%' then ");
		stm.append(" dmov.concepto ");
		stm.append(" end Concepto_mov, ");
		stm.append(" case when mpol.pol like 'AGT%' then ");
		stm.append(" substring(mpol.concepto,23,5) ");
		stm.append(" when mpol.pol like 'GG%' then ");
		stm.append(" substring(dmov.concepto,22,5) ");
		stm.append(" when mpol.pol like 'COB%' then ");
		stm.append(" substring(dmov.concepto,40,5) ");
		stm.append(" else ");
		stm.append(" '0' ");
		stm.append(" end Agente, ");
		stm.append(" MCta.Cta Cuenta, ");
		stm.append(" dmov.mon, ");
		stm.append(" dmov.Neto importeMO, ");
		stm.append(" dmov.tcambio, ");
		stm.append(" dmov.NetoCG importe ");
		stm.append(" From MCta with(nolock), MPol with(nolock), DMov with(nolock) ");
		stm.append(" Where (MPol.Cia = DMov.Cia) ");
		stm.append(" And (MPol.FPol = DMov.FPol) ");
		stm.append(" And (MPol.NPol = DMov.NPol) ");
		stm.append(" And (MPol.TPol = DMov.TPol) ");
		stm.append(" And (DMov.Libro = 'ME') ");
		stm.append(" And (MCta.IDCta = DMov.IDCta) ");
		stm.append(" And (DMov.Cia = 1) ");
		stm.append(" And (DMov.Fecha Between '"+anioMes+"/01' and '"+fechaString+"') "); //Armado dinamico.
        stm.append(" And (DMov.PFiscal <= 12) ");
		stm.append(" And (DMov.STS = '3-7') ");
		stm.append(" and (DMov.fpol IN('GG','AGT','COB')) ");
		stm.append(" And left(MCta.Cta,4) = 1622 ");
		stm.append(" ) a ");
		stm.append(" group by case when isnumeric(agente) = 1 then agente else 0 end; ");
		
		lista = interfazSql.executeSelect(stm.toString(),RepMizarSelectMizar.class);
		LogDeMidasInterfaz.log("tamaño de la lista consulta a mizar: "+lista.size()+"saliendo del metodo RepSaldoVSContDaoImpl.consultaMizar()..." + this, Level.INFO,null);
		return lista;
	}

	@Override
	public List<SaldosVsContabilidadView> executeSpSaldosVSContabilidad(String pfechaCorte) throws Exception {
		List<SaldosVsContabilidadView> resultado = new ArrayList<SaldosVsContabilidadView>();
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_repSaldosVsContabilidad";
		LogDeMidasInterfaz.log("Entrando al sp "+sp+"..." + this, Level.INFO, null);
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;
		
	
			String[] colsTemp ={ "id","id_agente","nombre","b_contab_comis",
								 "saldo_inicial","cobranza","manuales",
								 "pagos","saldo_final","cob_mizar",
								 "agt_mizar","gg_mizar",
								 "dif_cob", "dif_man", "dif_pag"};
			String[] propsTemp ={"id","id_agente","nombre","b_contab_comis",
								 "saldo_inicial","cobranza","manuales",
								 "pagos","saldo_final","cob_mizar",
								 "agt_mizar","gg_mizar",
								 "dif_cob", "dif_man", "dif_pag"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(
					SaldosVsContabilidadView.class.getCanonicalName(),
					propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("pfechaCorte", val(pfechaCorte));	
			resultado = storedHelper.obtieneListaResultados();
			if( resultado !=null && !resultado.isEmpty()){
				LogDeMidasInterfaz.log("resultado OK .... Saliendo del sp "+sp+"..." + this, Level.INFO, null);
				return resultado;
			}
			LogDeMidasInterfaz.log("resultado NULL ....  Saliendo del sp "+sp+"..." + this, Level.INFO, null);
			return null;
	}
	private Object val(Object o){
		return (o==null)?"":o;
	}

}
