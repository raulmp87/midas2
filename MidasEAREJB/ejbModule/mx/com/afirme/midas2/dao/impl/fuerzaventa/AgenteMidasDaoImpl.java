package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.apache.log4j.Logger;
import org.eclipse.persistence.annotations.Customizer;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.usuario.UsuarioDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.fuerzaventa.AgenteMidasDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.impl.domicilio.DomicilioUtils;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Afianzadora;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente.Situacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.DocumentoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EntretenimientoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HijoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.TipoDocumentoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CrearDireccionPorCpYColonia;
import mx.com.afirme.midas2.dto.fuerzaventa.DomicilioView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote.TipoAccionFuerzaVenta;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioAgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService.TipoContrato;
import mx.com.afirme.midas2.service.tarifa.DescuentoAgenteService;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.LeerArchivo;
import mx.com.afirme.midas2.utils.MailServiceSupport;
@Stateless
public class AgenteMidasDaoImpl extends EntidadDaoImpl implements AgenteMidasDao {
	
	private static final Logger LOG = Logger.getLogger(AgenteMidasDaoImpl.class);
	
	private final String insertedAction = "inserted";
	private PersonaSeycosFacadeRemote personaSeycosFacadeRemote;
	private EntidadService entidadService;
	private DomicilioFacadeRemote domicilioFacadeRemote;
	private EntidadDao entidadDao;
	private ValorCatalogoAgentesService catalogoService;
	private ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade;
	private FortimaxService fortimaxService;
	
	private EstadoCivilDTO estadoCivil;
	private List<EstadoDTO> estadoNacimiento = new ArrayList<EstadoDTO>();
	private List<MunicipioDTO> municipioNacimiento = new ArrayList<MunicipioDTO>();
	
	private EstadoFacadeRemote estadoFacadeRemote;
	
	@EJB
	private DescuentoAgenteService descuentoAgenteService;
		
	private List<Promotoria> promotoriaId = new ArrayList<Promotoria>();
	
	
	private PersonaSeycosFacadeRemote personaSeycosFacade;
	private AgenteMidasService agenteMidasService;
	private int pagingCount;
	private Long pagingStart;
	private Long pagingLimit;
	private List<Date>fechas = new ArrayList<Date>();
	private List<Afianzadora> idAfianzadora  = new ArrayList<Afianzadora>();
	private ValorCatalogoAgentes tipoCed;
	private MunicipioDTO municipionac;
	private EstadoDTO edoNac;
	private List<String> registrosAfectados = new ArrayList<String>();
	
	
	@EJB
	private ProductoBancarioAgenteService productoService;
	
	private List<Map<String,String>>cargaMasiva = new ArrayList<Map<String,String>>();
	private Map<String, String> obtenerDato;
	private SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");
	private UsuarioService usuarioService;
	@EJB
	private MailService mailService;
	@EJB
	private ImpresionesService impresionesService;
	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public List<Agente> findByFilters(Agente filtroAgente) {
		
		return this.findByFilters(filtroAgente, null);
	}
	
	public List<AgenteView> findByFiltersWithAddressSupportPaging(Agente filtroAgente,Integer pagingCount,Long pagingStart){
//		this.pagingCount=pagingCount;
//		this.pagingStart=(isNotNull(pagingStart))?pagingStart:0l;
//		this.pagingLimit=(isNotNull(this.pagingStart) && isNotNull(this.pagingCount))?this.pagingStart+this.pagingCount:null;
		return findByFiltersWithAddressSupport(filtroAgente);
	}
	/**
	 * Obtiene el numero de registros de los agentes
	 * @param filtroAgente
	 * @return
	 */
	public Long getMaxRowNumFindByFiltersWithAddressSupportPaging(Agente filtroAgente){
		Long maxRowNum=null;
		List<AgenteView> lista=new ArrayList<AgenteView>();
		Persona persona=(isNotNull(filtroAgente))?filtroAgente.getPersona():null;
		Domicilio domicilio=(isNotNull(persona) && !isEmptyList(persona.getDomicilios()))?persona.getDomicilios().get(0):null;
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT distinct "); 
		queryString.append(" agente.id as id,");
		queryString.append(" agente.idAgente as idAgente,");
		queryString.append(" promotoria.descripcion as promotoria,");
		queryString.append(" agente.idPromotoria as idPromotoria,");
		queryString.append(" agente.idPersona,");
		queryString.append(" PERSONA.NOMBRECOMPLETO as nombreCompleto,");
		queryString.append(" PERSONA.CODIGORFC as codigoRfc,");
		queryString.append(" tipoCedula.valor as tipoCedulaAgente ,");
		queryString.append(" personaEjecutivo.nombreCompleto as ejecutivo,");
		queryString.append(" tipoSituacion.valor as tipoSituacion ");
		queryString.append(" from MIDAS.toAgente agente ");
		queryString.append(" inner join MIDAS.toPromotoria promotoria on(promotoria.id=agente.idPromotoria) ");
		queryString.append(" inner join MIDAS.VW_PERSONA persona on(persona.idpersona=agente.idPersona) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoCedula on(tipoCedula.id=agente.idTipoCedulaAgente and tipoCedula.grupoCatalogoAgentes_id=(select id from MIDAS.tcGrupoCatalogoAgentes where descripcion='Tipos de Cedula de Agente'))");
		queryString.append(" inner join MIDAS.toEjecutivo ejecutivo on(ejecutivo.id=promotoria.ejecutivo_id) ");
		queryString.append(" inner join MIDAS.toGerencia gerencia on(gerencia.id=ejecutivo.gerencia_id) ");
		queryString.append(" inner join MIDAS.VW_PERSONA personaEjecutivo on(personaEjecutivo.idPersona=ejecutivo.idPersona)");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoSituacion on(tipoSituacion.id=agente.idSituacionAgente and tipoSituacion.grupoCatalogoAgentes_id=(select id from MIDAS.tcGrupoCatalogoAgentes where descripcion='Estatus de Agente (Situacion)'))");
		boolean domicilioEmpty=DomicilioUtils.isEmptyAddress(domicilio);
		if(!domicilioEmpty){
			queryString.append(" LEFT JOIN MIDAS.VW_DOMICILIO dom ON (dom.idpersona=persona.idpersona) ");
		}
		queryString.append(" where ");
		int index=1;
		if(isNotNull(filtroAgente)){
			if(isNotNull(filtroAgente.getId())){
				addCondition(queryString, " agente.id=? ");
				params.put(index, filtroAgente.getId());
				index++;
			}else if(isNotNull(filtroAgente.getIdAgente())){
				addCondition(queryString, " agente.idAgente=? ");
				params.put(index, filtroAgente.getIdAgente());
				index++;
			}
			
//				Persona persona=filtroAgente.getPersona();
			if(isNotNull(persona)){
					if(isValid(persona.getRazonSocial())){
						addCondition(queryString, " UPPER(persona.nombreCompleto) like UPPER(?)");
						params.put(index,"%"+persona.getRazonSocial()+"%");
						index++;
					}
					if(isValid(persona.getNombreCompleto())){
						addCondition(queryString, " UPPER(persona.nombreCompleto) like UPPER(?)");
						params.put(index,  "%"+persona.getNombreCompleto()+"%");
						index++;
					}
					if(isValid(persona.getRfc())){
						addCondition(queryString, " UPPER(persona.codigoRfc) like UPPER(?)");
						params.put(index,  "%"+persona.getRfc()+"%");
						index++;
					}
					
					if(filtroAgente.getPersona().getTelefonoOficina()!=null && !filtroAgente.getPersona().getTelefonoOficina().isEmpty()){
						addCondition(queryString, "  (persona.TELEFONOCASA like ? or persona.TELEFONOCELULAR like ? or persona.TELEFONOOFICINA like ?) ");
						params.put(index,  "%"+filtroAgente.getPersona().getTelefonoOficina()+"%");
						index++;
						params.put(index,  "%"+filtroAgente.getPersona().getTelefonoOficina()+"%");
						index++;
						params.put(index,  "%"+filtroAgente.getPersona().getTelefonoOficina()+"%");
						index++;
					}
//					Domicilio domicilio=(!isEmptyList(persona.getDomicilios()))?persona.getDomicilios().get(0):null;
					if(isNotNull(domicilio)){
						if(isValid(domicilio.getClaveEstado())){
							addCondition(queryString, " UPPER(dom.claveEstado) like UPPER(?)");
							params.put(index,  domicilio.getClaveEstado());
							index++;
						}
						if(isValid(domicilio.getClaveCiudad())){
							addCondition(queryString, " UPPER(dom.claveCiudad) like UPPER(?)");
							params.put(index,  domicilio.getClaveCiudad());
							index++;
						}
						if(isValid(domicilio.getCodigoPostal())){
							addCondition(queryString, " TRIM(dom.codigoPostal) like TRIM(UPPER(?))");
							params.put(index,  domicilio.getCodigoPostal());
							index++;
						}
						if(isValid(domicilio.getNombreColonia())){
							addCondition(queryString, " TRIM(UPPER(dom.colonia)) like TRIM(UPPER(?))||'%'");
							params.put(index,  domicilio.getNombreColonia());
							index++;
						}
					}
				}
				if(isNotNull(filtroAgente.getFechaAlta())){
					Date fecha=filtroAgente.getFechaAlta();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(agente.fechaAlta) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroAgente.getFechaAltaFin())){
					Date fecha=filtroAgente.getFechaAltaFin();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(agente.fechaAlta) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroAgente.getFechaAutorizacionFianza())){
					Date fecha=filtroAgente.getFechaAutorizacionFianza();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(agente.fechaAutorizacionFianza) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroAgente.getFechaAutorizacionFianzaFin())){
					Date fecha=filtroAgente.getFechaAutorizacionFianzaFin();					
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(agente.fechaAutorizacionFianza) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
					params.put(index, fechaString);
					index++;
				}
				if(isNotNull(filtroAgente.getFechaAutorizacionAgenteInicio())){
					Date fecha=filtroAgente.getFechaAutorizacionAgenteInicio();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(agente.fechaActivacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroAgente.getFechaAutorizacionAgenteFin())){
					Date fecha=filtroAgente.getFechaAutorizacionAgenteFin();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(agente.fechaActivacion) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
					params.put(index,  fechaString);
					index++;
				}
				Promotoria promotoria=filtroAgente.getPromotoria();
				if(isNotNull(promotoria)){
					if(isNotNull(promotoria) && isNotNull(promotoria.getId())){
						addCondition(queryString, " promotoria.id=? ");
						params.put(index, promotoria.getId());
						index++;
					}
					Ejecutivo ejecutivo=promotoria.getEjecutivo();
					if(isNotNull(ejecutivo)){
						if(isNotNull(ejecutivo.getId())){
							addCondition(queryString, " ejecutivo.id=? ");
							params.put(index, ejecutivo.getId());
							index++;
						}
						Gerencia gerencia= ejecutivo.getGerencia();
						if(isNotNull(gerencia) && isNotNull(gerencia.getId())){
							addCondition(queryString, " gerencia.id=? ");
							params.put(index, gerencia.getId());
							index++;
						}
					}
				}
				ValorCatalogoAgentes tipoSituacion=filtroAgente.getTipoSituacion();
				if(isNotNull(tipoSituacion) && isNotNull(tipoSituacion.getId())){
					addCondition(queryString, " tipoSituacion.id=? ");
					params.put(index, tipoSituacion.getId());
					index++;
				}					
		}
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		else
		{
			ValorCatalogoAgentes tipoAgentePromotor=null;
			try {
				tipoAgentePromotor=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "AGENTE PROMOTOR");
				if(isNotNull(tipoAgentePromotor) && isNotNull(tipoAgentePromotor.getId())){
					//addCondition(queryString, " agente.IDTIPOAGENTE <> ? ");
					addCondition(queryString," NOT(agente.idTipoAgente in(?) and agente.idAgente is null)");
					params.put(index, tipoAgentePromotor.getId());
					index++;
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}			
		}
		
		Query query=entityManager.createNativeQuery(getQueryString(queryString),AgenteView.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		lista=query.getResultList();
		maxRowNum=(isNotNull(lista))?new Long(lista.size()):null;
		return maxRowNum;
	}
	
	/**
	 * Metodo para mostrar el grid de catalogo de agentes con filtros de domicilio
	 * @param filtroAgente
	 * @return
	 */
	public List<AgenteView> findByFiltersWithAddressSupport(Agente filtroAgente){
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<AgenteView> lista=new ArrayList<AgenteView>();
		Persona persona=(isNotNull(filtroAgente))?filtroAgente.getPersona():null;
		Domicilio domicilio=(isNotNull(persona) && !isEmptyList(persona.getDomicilios()))?persona.getDomicilios().get(0):null;
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select distinct id, ");
		queryString.append(" idAgente,");
		queryString.append(" promotoria,");
		queryString.append(" idPromotoria,");
		queryString.append(" idPersona,");
		queryString.append(" nombreCompleto,");
		queryString.append(" codigoRfc,");
		queryString.append(" tipoCedulaAgente,");
		queryString.append(" ejecutivo,");
		queryString.append(" tipoSituacion, ");
		queryString.append(" gerencia ");
		queryString.append(" from (");
		queryString.append(" SELECT /*+ INDEX(ejecutivo TOEJECUTIVO_PK) INDEX(promotoria TOPROMOTORIA_PK) INDEX(agente IDX_TOAGT_CALCBONOEXCLUSION)*/ distinct rownum as renglon,"); 
		queryString.append(" agente.id as id,");
		queryString.append(" agente.idAgente as idAgente,");
		queryString.append(" promotoria.descripcion as promotoria,");
		queryString.append(" agente.idPromotoria as idPromotoria,");
		queryString.append(" agente.idPersona,");
		queryString.append(" PERSONA.NOMBRECOMPLETO as nombreCompleto,");
		queryString.append(" PERSONA.CODIGORFC as codigoRfc,");
		queryString.append(" tipoCedula.valor as tipoCedulaAgente ,");
		queryString.append(" personaEjecutivo.nombreCompleto as ejecutivo,");
		queryString.append(" tipoSituacion.valor as tipoSituacion, ");
		queryString.append(" gerencia.idGerencia as gerencia");
		queryString.append(" from MIDAS.toAgente agente ");
		queryString.append(" inner join MIDAS.toPromotoria promotoria on(promotoria.id=agente.idPromotoria) ");
		queryString.append(" inner join MIDAS.VW_PERSONA persona on(persona.idpersona=agente.idPersona) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoCedula on(tipoCedula.id=agente.idTipoCedulaAgente and tipoCedula.grupoCatalogoAgentes_id=(select id from MIDAS.tcGrupoCatalogoAgentes where descripcion='Tipos de Cedula de Agente'))");
		queryString.append(" inner join MIDAS.toEjecutivo ejecutivo on(ejecutivo.id=promotoria.ejecutivo_id) ");
		queryString.append(" inner join MIDAS.toGerencia gerencia on(gerencia.id=ejecutivo.gerencia_id) ");
		queryString.append(" inner join MIDAS.VW_PERSONA personaEjecutivo on(personaEjecutivo.idPersona=ejecutivo.idPersona)");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoSituacion on(tipoSituacion.id=agente.idSituacionAgente and tipoSituacion.grupoCatalogoAgentes_id=(select id from MIDAS.tcGrupoCatalogoAgentes where descripcion='Estatus de Agente (Situacion)'))");
		boolean domicilioEmpty=DomicilioUtils.isEmptyAddress(domicilio);
		if(!domicilioEmpty){
			queryString.append(" LEFT JOIN MIDAS.VW_DOMICILIO dom ON (dom.idpersona=persona.idpersona) ");
		}
		queryString.append(" where ");
		int index=1;
		if(isNotNull(filtroAgente)){
			if(isNotNull(filtroAgente.getId())){
				addCondition(queryString, " agente.id=? ");
				params.put(index, filtroAgente.getId());
				index++;
			}else if(isNotNull(filtroAgente.getIdAgente())){
				addCondition(queryString, " agente.idAgente=? ");
				params.put(index, filtroAgente.getIdAgente());
				index++;
			}
			if(isNotNull(persona)){
					if(isValid(persona.getRazonSocial())){
						addCondition(queryString, " UPPER(persona.nombreCompleto) like UPPER(?)");
						params.put(index,"%"+persona.getRazonSocial()+"%");
						index++;
					}
					if(isValid(persona.getNombreCompleto())){
						addCondition(queryString, " UPPER(persona.nombreCompleto) like UPPER(?)");
						params.put(index,  "%"+persona.getNombreCompleto()+"%");
						index++;
					}
					if(isValid(persona.getRfc())){
						addCondition(queryString, " UPPER(persona.codigoRfc) like UPPER(?)");
						params.put(index,  "%"+persona.getRfc()+"%");
						index++;
					}
					
					if(filtroAgente.getPersona().getTelefonoOficina()!=null && !filtroAgente.getPersona().getTelefonoOficina().isEmpty()){
						addCondition(queryString, "  (persona.TELEFONOCASA like ? or persona.TELEFONOCELULAR like ? or persona.TELEFONOOFICINA like ?) ");
						params.put(index,  "%"+filtroAgente.getPersona().getTelefonoOficina()+"%");
						index++;
						params.put(index,  "%"+filtroAgente.getPersona().getTelefonoOficina()+"%");
						index++;
						params.put(index,  "%"+filtroAgente.getPersona().getTelefonoOficina()+"%");
						index++;
					}
					//Domicilio domicilio=(!isEmptyList(persona.getDomicilios()))?persona.getDomicilios().get(0):null;
					if(isNotNull(domicilio)){
						if(isValid(domicilio.getClaveEstado())){
							addCondition(queryString, " UPPER(dom.claveEstado) like UPPER(?)");
							params.put(index,  domicilio.getClaveEstado());
							index++;
						}
						if(isValid(domicilio.getClaveCiudad())){
							addCondition(queryString, " UPPER(dom.claveCiudad) like UPPER(?)");
							params.put(index,  domicilio.getClaveCiudad());
							index++;
						}
						if(isValid(domicilio.getCodigoPostal())){
							addCondition(queryString, " TRIM(dom.codigoPostal) like TRIM(UPPER(?))");
							params.put(index,  domicilio.getCodigoPostal());
							index++;
						}
						if(isValid(domicilio.getNombreColonia())){
							addCondition(queryString, " TRIM(UPPER(dom.colonia)) like TRIM(UPPER(?))||'%'");
							params.put(index,  domicilio.getNombreColonia());
							index++;
						}
					}
			}
			if(isNotNull(filtroAgente.getFechaAlta())){
				Date fecha=filtroAgente.getFechaAlta();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(agente.fechaAlta) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
				params.put(index,  fechaString);
				index++;
			}
			if(isNotNull(filtroAgente.getFechaAltaFin())){
				Date fecha=filtroAgente.getFechaAltaFin();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(agente.fechaAlta) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
				params.put(index,  fechaString);
				index++;
			}
			if(isNotNull(filtroAgente.getFechaAutorizacionFianza())){
				Date fecha=filtroAgente.getFechaAutorizacionFianza();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(agente.fechaAutorizacionFianza) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
				params.put(index,  fechaString);
				index++;
			}
			if(isNotNull(filtroAgente.getFechaAutorizacionAgenteInicio())){
				Date fecha=filtroAgente.getFechaAutorizacionAgenteInicio();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(agente.fechaActivacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
				params.put(index,  fechaString);
				index++;
			}
			if(isNotNull(filtroAgente.getFechaAutorizacionAgenteFin())){
				Date fecha=filtroAgente.getFechaAutorizacionAgenteFin();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(agente.fechaActivacion) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
				params.put(index,  fechaString);
				index++;
			}
			if(isNotNull(filtroAgente.getFechaAutorizacionFianzaFin())){
				Date fecha=filtroAgente.getFechaAutorizacionFianzaFin();					
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(agente.fechaAutorizacionFianza) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
				params.put(index, fechaString);
				index++;
			}
			Promotoria promotoria=filtroAgente.getPromotoria();
			if(isNotNull(promotoria)){
					if(isNotNull(promotoria) && isNotNull(promotoria.getId())){
						addCondition(queryString, " promotoria.id=? ");
						params.put(index, promotoria.getId());
						index++;
					}
					Ejecutivo ejecutivo=promotoria.getEjecutivo();
					if(isNotNull(ejecutivo)){
						if(isNotNull(ejecutivo.getId())){
							addCondition(queryString, " ejecutivo.id=? ");
							params.put(index, ejecutivo.getId());
							index++;
						}
						Gerencia gerencia= ejecutivo.getGerencia();
						if(isNotNull(gerencia)){
							if(isNotNull(gerencia.getId())){
								addCondition(queryString, " gerencia.id=? ");
								params.put(index, gerencia.getId());
								index++;
							}
							if(isNotNull(gerencia.getIdGerencia())){
								addCondition(queryString, " gerencia.idGerencia=? ");
								params.put(index, gerencia.getIdGerencia());
								index++;
							}
						}
					}				
			}
			ValorCatalogoAgentes tipoSituacion=filtroAgente.getTipoSituacion();
			if(isNotNull(tipoSituacion) && isNotNull(tipoSituacion.getId())){
				addCondition(queryString, " tipoSituacion.id=? ");
				params.put(index, tipoSituacion.getId());
				index++;
			}			
			if(params.isEmpty()){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			else
			{   
			    ValorCatalogoAgentes tipoAgentePromotor=null;
				try {
					tipoAgentePromotor=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "AGENTE PROMOTOR");
					if(isNotNull(tipoAgentePromotor) && isNotNull(tipoAgentePromotor.getId())){
						//addCondition(queryString, " agente.IDTIPOAGENTE not in(?) ");
						addCondition(queryString," NOT(agente.idTipoAgente in(?) and agente.idAgente is null)");
						params.put(index, tipoAgentePromotor.getId());
						index++;
					}
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}			
			}
			
			String queryReal=getQueryString(queryString);
			queryString.delete(0,queryString.length());
			queryString.append(queryReal);
			queryString.append(")");// entidad where ");
//			if(isNotNull(pagingStart) && isNotNull(pagingLimit)){
//				addCondition(queryString, " renglon>? ");
//				params.put(index, pagingStart);
//				index++;
//				addCondition(queryString, " renglon<=? ");
//				params.put(index, pagingLimit);
//				index++;
//			}
			String finalQuery=getQueryString(queryString)+" order by id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,AgenteView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			lista=query.getResultList();
		}
		return lista;
	}
	
	public List <Agente> getAgenteHistory(Agente agente, String History){
		List<Agente> lista = new ArrayList<Agente>();
		
		return lista;
	}
	
	
	/**
	 * Valida si la clave de un agente es valida o no esta repetida 
	 * @param agente
	 * @return
	 * @throws Exception
	 */
	@Override
	public boolean validarClaveAgente(Agente agente)throws Exception{
		boolean isValid=true;
		if(agente==null){
			onError("Favor de proporcionar los datos del agente");
		}
		if(agente.getIdAgente()==null){
			onError("Favor de proporcionar la clave del Agente");
		}
		//-1 indica que es un agente dado de alta por internet, y cualquier otro deberia de ser por alta por medio de los catalogos.
		if(agente.getIdAgente()!=-1){
			Agente filtro=new Agente();
			filtro.setIdAgente(agente.getIdAgente());
			List<Agente> lista=findByFilters(filtro);
			//Si la lista esta vacia o es nula, significa que no hay ninguna agente con esa clave, por lo tanto es valida.
			if(lista!=null&& !lista.isEmpty()){
				//Sino hay que validar que si existe un agente con esa clave no sea el mismo agente(en el caso de actualizacion ya que se vuelve a validar la clave del agente)
				Long idAgenteValidar=agente.getIdAgente();
				ciclo:
				for(Agente a:lista){
					if(a.getIdAgente().equals(idAgenteValidar)){
						//Si ya hay un agente con esta clave se verifica que no sea el mismo
						//Si hay un agente con la misma clave pero diferente id, significa que otro agente ya tiene asignado esa clave, entonces no es valido.
						if(!a.getId().equals(agente.getId())){
							isValid=false;
							break ciclo;
						}
					}
				}
			}
		}else{
			agente.setIdAgente(null);
		}
		return isValid;
	}
	
	@Override
	public List<AgenteView> agentesPorAutorizar(Agente filtroAgente) throws Exception{
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<AgenteView> lista=new ArrayList<AgenteView>();
		int index = 0;
		final StringBuilder queryString=new StringBuilder("");
				queryString.append(" SELECT DISTINCT agente.id as id,agente.idAgente as idAgente, ");
				queryString.append(" TRUNC(sysdate - AGENTE.FECHAALTA) as diasTranscurridos, ");
//				queryString.append(" case when ACT.USUARIOACTUALIZACION like 'agenteGenerico' then 'AltaXInternet' else 'Administrativo' end as conductoAlta, ");
				queryString.append(" case when agente.conducto_alta is null then 'ADMINISTRATIVO' else conductoAlta.valor end as conductoAlta, ");
				queryString.append(" case when ACT.USUARIOACTUALIZACION like 'agenteGenerico' then 'AGENTE' else 'ADMINISTRADOR' end as perfil, ");
				queryString.append(" pro.descripcion as promotoria,pers.nombreCompleto as nombreCompleto,pers.CODIGORFC as codigoRfc, ");
				queryString.append(" v.valor as tipoCedulaAgente,persEjec.nombreCompleto as ejecutivo , ");
				queryString.append(" tipoSituacion.valor as tipoSituacion,gerencia.descripcion as gerencia,prioridad.valor as prioridad ");   
				queryString.append(" from MIDAS.toAgente agente  ");
				queryString.append(" left join MIDAS.tlActualizacionAgentes act on(act.idRegistro=agente.id ) ");                
				queryString.append(" left join MIDAS.TOPROMOTORIA pro on(pro.id=agente.idpromotoria) "); 
				queryString.append(" left join MIDAS.VW_PERSONA pers on(pers.idpersona=agente.idpersona) ");  
				queryString.append(" left join MIDAS.toValorCatalogoAgentes v on(agente.idTipoCedulaAgente=v.id) ");                 
				queryString.append(" left join MIDAS.toEjecutivo ej on(ej.id=pro.ejecutivo_id) ");
				queryString.append(" left join MIDAS.toGerencia gerencia on(gerencia.id=ej.gerencia_id) "); 
				queryString.append(" left join MIDAS.vw_persona persEjec on(persEjec.idpersona=ej.idpersona) "); 
				queryString.append(" left join MIDAS.toValorCatalogoAgentes tipoSituacion on(tipoSituacion.id=agente.idSituacionAgente) "); 
				queryString.append(" left join MIDAS.toValorCatalogoAgentes prioridad on(prioridad.id=agente.idPrioridadAgente) ");
				queryString.append(" LEFT JOIN MIDAS.VW_DOMICILIO dom ON (dom.idpersona=pers.idpersona) ");
				queryString.append(" left join MIDAS.toValorCatalogoAgentes conductoAlta on(conductoAlta.id=agente.conducto_alta) ");
				queryString.append(" where ");
				
	
		if(filtroAgente!=null){
			 index=1;
			if(filtroAgente.getFechaAlta()!=null){
				Date fecha=filtroAgente.getFechaAlta();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fecha);
				addCondition(queryString, " TRUNC(agente.fechaAlta) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
				params.put(index,  fechaString);
				index++;
			}
			if(filtroAgente.getFechaAltaFin()!=null){
				Date fechaFin=filtroAgente.getFechaAltaFin();
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				String fechaString=format.format(fechaFin);
				addCondition(queryString, " TRUNC(agente.fechaAlta) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");					
				params.put(index,  fechaString);
				index++;
			}
			if(filtroAgente.getPersona()!=null){
				if(filtroAgente.getPersona().getRazonSocial()!=null && !filtroAgente.getPersona().getRazonSocial().isEmpty()){
					addCondition(queryString, " UPPER(pers.razonSocial) like UPPER(?) ");
					params.put(index, "%"+filtroAgente.getPersona().getRazonSocial()+"%");
					index++;
				}
				if(filtroAgente.getPersona().getNombreCompleto()!=null && !filtroAgente.getPersona().getNombreCompleto().isEmpty()){
					addCondition(queryString, " UPPER(pers.nombreCompleto) like UPPER(?)");
					params.put(index,  "%"+filtroAgente.getPersona().getNombreCompleto()+"%");
					index++;
				}	
				if(filtroAgente.getPersona().getTelefonoCasa()!=null && !filtroAgente.getPersona().getTelefonoCasa().isEmpty()){
					addCondition(queryString, "  pers.TELEFONOCASA like ? or pers.TELEFONOCELULAR  like ? or pers.TELEFONOOFICINA like ? ");
					params.put(index,  "%"+filtroAgente.getPersona().getTelefonoCasa()+"%");
					index++;
					params.put(index,  "%"+filtroAgente.getPersona().getTelefonoCasa()+"%");
					index++;
					params.put(index,  "%"+filtroAgente.getPersona().getTelefonoCasa()+"%");
					index++;
				}
				if(filtroAgente.getPersona().getRfc()!=null && !filtroAgente.getPersona().getRfc().isEmpty()){
					addCondition(queryString, " UPPER(pers.codigorfc) like UPPER(?)");
					params.put(index,  "%"+filtroAgente.getPersona().getRfc()+"%");
					index++;
				}
			}
			if(filtroAgente.getPromotoria()!=null){
				if(filtroAgente.getPromotoria().getId()!=null && !filtroAgente.getPromotoria().getId().equals("")){
					addCondition(queryString, " pro.id=?");
					params.put(index, filtroAgente.getPromotoria().getId());
					index++;
				}
				if(filtroAgente.getPromotoria().getEjecutivo().getId()!=null && !filtroAgente.getPromotoria().getEjecutivo().getId().equals("")){
					addCondition(queryString, " pro.ejecutivo_id=?");
					params.put(index, filtroAgente.getPromotoria().getEjecutivo().getId());
					index++;
				}			
				if(filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId()!=null && !filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId().equals("")){
					addCondition(queryString, " ej.gerencia_id=?");
					params.put(index, filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId());
					index++;
				}
			}
			if(filtroAgente.getTipoSituacion() != null && filtroAgente.getTipoSituacion().getId()!=null){
					addCondition(queryString, " tipoSituacion.id=? ");
					params.put(index, filtroAgente.getTipoSituacion().getId());
					index++;
				
			}
			if(filtroAgente.getIdAgente()!=null){
				addCondition(queryString, " agente.idAgente=? ");
				params.put(index, filtroAgente.getIdAgente());
				index++;
			}
			List<Domicilio> domicilio=(filtroAgente.getPersona()!=null)?filtroAgente.getPersona().getDomicilios():null;
			if(domicilio!=null && domicilio.get(0)!=null){
				if(domicilio.get(0).getClaveEstado()!=null && !domicilio.get(0).getClaveEstado().isEmpty()){
					addCondition(queryString, " dom.claveEstado=? ");
					params.put(index, domicilio.get(0).getClaveEstado());
					index++;
				}
				if(domicilio.get(0).getClaveCiudad()!=null && !domicilio.get(0).getClaveCiudad().isEmpty()){
					addCondition(queryString, " dom.claveCiudad=? ");
					params.put(index, domicilio.get(0).getClaveCiudad());
					index++;
				}
				if(domicilio.get(0).getNombreColonia()!=null && !domicilio.get(0).getNombreColonia().isEmpty()){
					addCondition(queryString, " UPPER(dom.colonia) like UPPER(?) ");
					params.put(index, "%"+domicilio.get(0).getNombreColonia()+"%");
					index++;
				}
				if(domicilio.get(0).getCodigoPostal()!=null && !domicilio.get(0).getCodigoPostal().isEmpty()){
					addCondition(queryString, " dom.codigoPostal like ?");
					params.put(index, "%"+domicilio.get(0).getCodigoPostal()+"%");
					index++;
				}
			}
		}
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}else{
			ValorCatalogoAgentes tipoAgentePromotor=null;
			try {
				tipoAgentePromotor=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "AGENTE PROMOTOR");
				if(isNotNull(tipoAgentePromotor) && isNotNull(tipoAgentePromotor.getId())){
//					addCondition(queryString, " agente.IDTIPOAGENTE <> ? ");
					addCondition(queryString," NOT(agente.idTipoAgente in(?) and agente.idAgente is null)");
					params.put(index, tipoAgentePromotor.getId());
					index++;
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		String finalQuery=getQueryString(queryString)+" order by diasTranscurridos desc ";
        Query query=entityManager.createNativeQuery(finalQuery,AgenteView.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
//		query.setMaxResults(100);
		lista=query.getResultList();
		return lista;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	@Override
	public Agente saveFull(Agente agente) throws Exception {
		if(!validarClaveAgente(agente)){
			throw new Exception("La clave asignada para el agente ya existe, favor de introducir otra clave.");
		}
		
		validarCambioClaveAgente(agente);
		
		validarCambioPromotoria(agente);
		
		if(agente.getPersona()!=null){			
				Long idPersona=agente.getPersona().getIdPersona();
				Persona persona = agente.getPersona();
				if(idPersona==null){
					persona=null;//Se limpia el agente de la persistencia
				}else{
					persona=entityManager.find(Persona.class,idPersona);//lo toma del contexto de persistencia	
					entityManager.refresh(persona);
					agente.setPersona(persona);
					Promotoria promotoria=agente.getPromotoria();
					if(promotoria!=null && (promotoria.getEjecutivo()!=null && promotoria.getEjecutivo().getId()==null)){
						promotoria.setEjecutivo(null);
					}
//					promotoria=(promotoria!=null && promotoria.getId()!=null)?entityManager.find(Promotoria.class,promotoria.getId()):null;
//					if(promotoria!=null){
//						entityManager.refresh(promotoria);
//					}
					promotoria=entityManager.find(Promotoria.class,promotoria.getId());
					entityManager.refresh(promotoria);
					agente.setPromotoria(promotoria);
//					persistentPromotoria.setAgentePromotor(agente);
					
					ValorCatalogoAgentes tipoSit=agente.getTipoSituacion();
					if(tipoSit!=null){
						tipoSit=entityManager.find(ValorCatalogoAgentes.class,tipoSit.getId());
						entityManager.refresh(tipoSit);
						agente.setTipoSituacion(tipoSit);
					}
				}
		}
//		Boolean envioNotificacionBoolean = (agente.getIdAgente()==null)?true:false;
		
		//[CAVALOS] se agrego campo conducto de alta para saber si el agente fue dado de alta por administrativo o por alta por internet
		//si es null significa que esta siendo dado de alta por primera vez por algun usuario, ya que si se dio de alta por internet ya se encuentra guardado el id de alta por internet
		//y si ya tiene dato no se actualiza este valor para que no cambie su origen en que se dio de alta
		if(agente.getConductoAlta()==null){
			ValorCatalogoAgentes conductoAltaDefault = catalogoService.obtenerElementoEspecifico("Conducto Alta Agente", "ADMINISTRATIVO");	
			Long idConductoAlta=(conductoAltaDefault!=null && conductoAltaDefault.getId()!=null)?conductoAltaDefault.getId():null;
			agente.setConductoAlta(idConductoAlta);
		}
		Agente persistentObject=entidadService.save(agente);
		this.guardarDescuentosAgenteDTO(agente);
		//Siempre se inserta un nuevo agente con los cambios actualizados.
		try{
			replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(agente, TipoAccionFuerzaVenta.GUARDAR);
		}catch(Exception e){
			entidadService.remove(persistentObject);
			onError("Ha ocurrido un error al intentar replicar los datos del agente. Favor de reportar el problema al departamento de sistemas");
			LogDeMidasInterfaz.log("Ha ocurrido un error al intentar replicar los datos del agente" + this, Level.INFO, e);
			//onError("El agente con la clave["+agente.getIdAgente()+"] se a guardado exitosamente, pero ocurri\u00F3 un problema al replicar la informaci\u00F3n del mismo.");
		}
		if (agente.getEnvioDeCorreo()) {
			String mensaje = MailServiceSupport.mensajeAltaAgente(agente.getPersona().getNombreCompleto(), agente.getIdAgente());
			Map<String, Map<String, List<String>>> mapCorreos = agenteMidasService
					.obtenerCorreos(agente.getId(),
							GenericMailService.P_ALTA_AGENTE,
							GenericMailService.M_ALTA_REGISTRO);
			agenteMidasService.enviarCorreo(mapCorreos, mensaje, "Notificacion",
					GenericMailService.T_GENERAL, null);
		}
		return persistentObject;
	}
	private DescuentosAgenteDTO poblarDescuentosAgenteDTO(Agente param) {
	    Long idAgente=param.getIdAgente();
	    String numeroTelefono=param.getPersona().getTelefonoCelular()!=null ? param.getPersona().getTelefonoCelular():param.getPersona().getTelefonoCasa();
	        DescuentosAgenteDTO descuentoAgenteMovil= new DescuentosAgenteDTO();
	        descuentoAgenteMovil.setBajalogica(new Short("1"));
	        descuentoAgenteMovil.setPorcentaje("0.000001");
	        descuentoAgenteMovil.setClaveagente(idAgente!=null?idAgente.toString():"");
	        descuentoAgenteMovil.setClavepromo(idAgente!=null?idAgente.toString():"");
	        descuentoAgenteMovil.setEmail(param.getPersona().getEmail()!=null ?param.getPersona().getEmail(): " ");
	        descuentoAgenteMovil.setNombre(param.getPersona().getNombre()!=null?param.getPersona().getNombre(): " ");
	        descuentoAgenteMovil.setNumerotelefono(numeroTelefono!=null?numeroTelefono:"0");
	        descuentoAgenteMovil.setEsNuevoAgente(true);
	        LOG.info("NUMERO teL»"+descuentoAgenteMovil.getNumerotelefono());
	        return descuentoAgenteMovil;
	  }
	
	private void guardarDescuentosAgenteDTO(Agente agente) {
		try{
			DescuentosAgenteDTO descuentoAgente = poblarDescuentosAgenteDTO(agente);
			descuentoAgenteService.save(descuentoAgente);
		}catch (ApplicationException e){
			LogDeMidasInterfaz.log("Ha ocurrido un error en le metodo descuentoAgenteservice.save" + this, Level.INFO, e);
		}catch (Exception e){
			LogDeMidasInterfaz.log("Ha ocurrido un error en le metodo descuentoAgenteservice.save" + this, Level.INFO, e);
		}
	}

	
	public Agente saveAltaPorInternet(Agente agente) throws Exception{
//		if(!validarClaveAgente(agente)){
//			throw new Exception("La clave asignada para el agente ya existe, favor de introducir otra clave.");
//		}
		setDefaultValuesAltaInternet(agente);
		if(agente.getPersona()!=null){			
				Long idPersona=agente.getPersona().getIdPersona();
				Persona persona = agente.getPersona();
				if(idPersona==null){
					persona=null;//Se limpia el agente de la persistencia
				}else{
					persona=entityManager.find(Persona.class,idPersona);//lo toma del contexto de persistencia	
					entityManager.refresh(persona);
					agente.setPersona(persona);
					ValorCatalogoAgentes tipoAgenteDefault = new ValorCatalogoAgentes();
					if (persona.getClaveTipoPersona()==1){
						 tipoAgenteDefault=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "PERSONA FÍSICA");
					}else if (persona.getClaveTipoPersona()==2){
						 tipoAgenteDefault=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "PERSONA MORAL");
					}
					Long idTipoAgente = tipoAgenteDefault.getId();
					agente.setIdTipoAgente(idTipoAgente);
					Promotoria promotoria = agente.getPromotoria();
					promotoria=entityManager.find(Promotoria.class, agente.getPromotoria().getId());
					entityManager.refresh(promotoria);
					agente.setPromotoria(promotoria);
					promotoria.setAgentePromotor(agente);
				}
		}	
//		ValorCatalogoAgentes tipoSituacion=agente.getTipoSituacion();
//		if(isNotNull(tipoSituacion) && isNotNull(tipoSituacion.getId())){
//			tipoSituacion=entityManager.find(ValorCatalogoAgentes.class,tipoSituacion.getId());
//			entityManager.refresh(tipoSituacion);
//			agente.setTipoSituacion(tipoSituacion);
//		}
//		ValorCatalogoAgentes tipoCedula=agente.getTipoCedula();
//		if(isNotNull(tipoCedula) && isNotNull(tipoCedula.getId())){
//			tipoCedula=entityManager.find(ValorCatalogoAgentes.class,tipoCedula.getId());
//			entityManager.refresh(tipoCedula);
//			agente.setTipoCedula(tipoCedula);
//		}
		Afianzadora afianzadora=agente.getAfianzadora();
		if(isNotNull(afianzadora) && isNotNull(afianzadora.getId())){
			afianzadora=entityManager.find(Afianzadora.class,afianzadora.getId());
			entityManager.refresh(afianzadora);
			agente.setAfianzadora(afianzadora);
		}	
		Agente persistentObject =entidadService.save(agente);
		this.guardarDescuentosAgenteDTO(agente);
		//TODO Ya no se replica cuando es alta por internet
		//FIXME Se debe de replicar al autorizar el agente y si es conducto de alta por internet
		return persistentObject;
	}
	
	private void setDefaultValuesAltaInternet(Agente altaPorInternet) throws Exception{
		ValorCatalogoAgentes tipoSituacionDefault=catalogoService.obtenerElementoEspecifico("Estatus de Agente (Situacion)", "PENDIENTE POR AUTORIZAR");
		ValorCatalogoAgentes motivoEstatusDefault=catalogoService.obtenerElementoEspecifico("Motivo de Estatus del Agente","ALTA");// "TR\u00C1MITE"
		ValorCatalogoAgentes prioridadDefault=catalogoService.obtenerElementoEspecifico("Prioridades de Agente", "ALTA");
//		ValorCatalogoAgentes tipoAgenteDefault=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "ALTA POR INTERNET");
		ValorCatalogoAgentes tipoclasificacionAgenteDefault=catalogoService.obtenerElementoEspecifico("Clasificacion de Agente", "ALTA POR INTERNET");
		//[CAVALOS] se agrego campo conducto de alta para saber si el agente fue dado de alta por administrativo o por alta por internet		
		ValorCatalogoAgentes conductoAltaDefault = catalogoService.obtenerElementoEspecifico("Conducto Alta Agente", "ALTA POR INTERNET");		

//		Long idTipoAgente=(tipoAgenteDefault!=null && tipoAgenteDefault.getId()!=null)?tipoAgenteDefault.getId():null;
		Long idPrioridad=(prioridadDefault!=null && prioridadDefault.getId()!=null)?prioridadDefault.getId():null;
		Long idMotivo=(motivoEstatusDefault!=null && motivoEstatusDefault.getId()!=null)?motivoEstatusDefault.getId():null;
		Long idConductoAlta=(conductoAltaDefault!=null && conductoAltaDefault.getId()!=null)?conductoAltaDefault.getId():null;
		altaPorInternet.setTipoSituacion(tipoSituacionDefault);
		altaPorInternet.setIdMotivoEstatusAgente(idMotivo);
		altaPorInternet.setIdPrioridadAgente(idPrioridad);
//		altaPorInternet.setIdTipoAgente(idTipoAgente);
		altaPorInternet.setClasificacionAgentes(tipoclasificacionAgenteDefault);
		altaPorInternet.setIdAgente(null);
		altaPorInternet.setPorcentajeDividendos(null);
		altaPorInternet.setConductoAlta(idConductoAlta);
	}	
	/*
	 * Carga Masiva 
	 */
	
	public List<String> cargaMasiva(String archivo) throws Exception{
		int cont = 0;
		int registrosNoInsertados=0;
		int registrosInsertados=0;
		Date fechaNacimiento = new Date(); 
		Date fechaAutoCedula = new Date();
		Date fechaVencCedula = new Date();
		Date fechaAutorFianza = new Date();
		Date fechaVencFianza = new Date(); 
		String sexo="";
		cargaMasiva.addAll(LeerArchivo.leer(archivo));
		registrosAfectados.clear();
		for(Map<String, String> iterador:cargaMasiva){
			try {
				Persona persona = new Persona();
				Persona persona_ = new Persona();
				persona.setIdPersona(null);
				obtenerDato = new HashMap<String, String>();
				obtenerDato.putAll(cargaMasiva.get(cont));
//				if(obtenerDato.containsValue("")){
//					registrosNoInsertados++;
//					cont++;
//				}else{
					if(obtenerDato.get("SEXO").equals("MASCULINO")){
						sexo = "M";
					}else if(obtenerDato.get("SEXO").equals("FEMENINO")){
						sexo = "F";
					}
					if(!obtenerDato.get("FECHA DE NACIMIENTO").trim().equals("")){
						fechaNacimiento=sdf.parse(obtenerDato.get("FECHA DE NACIMIENTO").trim()); 
						fechas.add(fechaNacimiento);
					}else{
						fechas.add(null);
					}
					if(!obtenerDato.get("FECHA DE AUTORIZACIÓN CEDULA").trim().equals("")){
						fechaAutoCedula=sdf.parse(obtenerDato.get("FECHA DE AUTORIZACIÓN CEDULA").trim());
						fechas.add(1,fechaAutoCedula);
					}else{
						fechas.add(null);
					}
					if(!obtenerDato.get("FECHA DE VENCIMIENTO CEDULA").trim().equals("")){
						fechaVencCedula=sdf.parse(obtenerDato.get("FECHA DE VENCIMIENTO CEDULA").trim());
						fechas.add(2,fechaVencCedula);
					}else{
						fechas.add(null);
					}
					if(!obtenerDato.get("FECHA DE AUTORIZACION FIANZA").trim().equals("")){
						fechaAutorFianza=sdf.parse(obtenerDato.get("FECHA DE AUTORIZACION FIANZA").trim());
						fechas.add(3,fechaAutorFianza);
					}else{
						fechas.add(null);
					}
					if(!obtenerDato.get("FECHA DE VENCIMIENTO FIANZA").trim().equals("")){
						fechaVencFianza=sdf.parse(obtenerDato.get("FECHA DE VENCIMIENTO FIANZA").trim()); 
						fechas.add(4,fechaVencFianza);
					}else{
						fechas.add(null);
					}
					
					if(!obtenerDato.get("AFIANZADORA").trim().equals("")){
						idAfianzadora = entidadService.findByProperty(Afianzadora.class, "razonSocial", obtenerDato.get("AFIANZADORA").trim());
					}
					if(!obtenerDato.get("TIPO DE CEDULA").equals("")){
						tipoCed=catalogoService.obtenerElementoEspecifico("Tipos de Cedula de Agente",obtenerDato.get("TIPO DE CEDULA").trim());
					}
					if(!obtenerDato.get("PROMOTORÍA").trim().equals("")){
						promotoriaId = entidadService.findByProperty(Promotoria.class, "descripcion",obtenerDato.get("PROMOTORÍA").trim());
					}
					if(!obtenerDato.get("ESTADO CIVIL").trim().equals("")){
						estadoCivil=getEstadoCivil(obtenerDato.get("ESTADO CIVIL").trim());
					}
					if(!obtenerDato.get("CIUDAD DE NACIMIENTO").trim().equals("")){
						municipioNacimiento = entidadService.findByProperty(MunicipioDTO.class, "municipalityName", obtenerDato.get("CIUDAD DE NACIMIENTO").trim());
					}
					if(!municipioNacimiento.isEmpty()){
						municipionac =	municipioNacimiento.get(0);
					}
					Domicilio domicilioNuevo = direccionPorCPyColonia(obtenerDato.get("CÓDIGO POSTAL").trim(), obtenerDato.get("COLONIA").trim(), obtenerDato.get("CALLE Y NÚMERO"));
					estadoNacimiento =estadoFacadeRemote.findByProperty("stateName", obtenerDato.get("ESTADO DE NACIMIENTO"));
					if(!estadoNacimiento.isEmpty()){
						edoNac = estadoNacimiento.get(0);
					}
					//se valida que no valla ni un campo vacio en el layout
					String fieldError = validaRegistroCargaMasiva(obtenerDato, fechas,idAfianzadora,tipoCed,sexo ,edoNac,municipionac,estadoCivil);
					fechas.clear();
					if(!fieldError.equals("")){
						throw new Exception(fieldError);
					}
					List <Domicilio> domic = new ArrayList<Domicilio>();
					domic.add(domicilioNuevo);
					domic.add(domicilioNuevo);
					domic.add(domicilioNuevo);
					persona.setDomicilios(domic);
					persona.setNombre(obtenerDato.get("﻿NOMBRE"));
					persona.setApellidoPaterno(obtenerDato.get("APELLIDO PATERNO"));
					persona.setApellidoMaterno(obtenerDato.get("APELLIDO MATERNO"));
					persona.setClaveSexo(sexo);
					persona.setClaveEstadoCivil(estadoCivil.getIdEstadoCivil());
					persona.setClaveNacionalidad("PAMEXI");
					persona.setClaveEstadoNacimiento(edoNac.getStateId());
					persona.setClaveMunicipioNacimiento(municipionac.getStateId());
					persona.setFechaNacimiento(fechaNacimiento);
					persona.setRfc(obtenerDato.get("RFC").trim());
					persona.setCurp(obtenerDato.get("CURP").trim());
					persona.setTelefonoOficina(obtenerDato.get("TELEFONO DE OFICINA").trim());
					persona.setEmail(obtenerDato.get("CORREO ELECTRONICO").trim());
					persona.setTelefonoCelular(obtenerDato.get("TELEFONO CELULAR").trim());
					persona.setTelefonoCasa(obtenerDato.get("TELEFONO CASA").trim());
					persona.setClaveTipoPersona(1L);
					//
					PersonaSeycosDTO personaSeycosDTO = new PersonaSeycosDTO();
					personaSeycosDTO = agenteMidasService.llenarPersonaSeycos(persona);		
					personaSeycosDTO=personaSeycosFacade.save(personaSeycosDTO);
					List<Domicilio>domicilioOficina=new ArrayList<Domicilio>();
//
					domicilioOficina.add(domicilioNuevo);
					personaSeycosDTO.setDomicilios(domicilioOficina);
					personaSeycosDTO=personaSeycosFacade.saveContactData(personaSeycosDTO);
					persona_=entidadService.getReference(Persona.class, personaSeycosDTO.getIdPersona());
					//
					Date fechaActual = new Date();
					BigDecimal porcenDividendos = new BigDecimal(1000);
					Agente agenteCargaMasiva = new Agente();
					
					agenteCargaMasiva.setFechaAlta(fechaActual);
					agenteCargaMasiva.setPorcentajeDividendos(porcenDividendos);
					agenteCargaMasiva.setPromotoria(promotoriaId.get(0));
					agenteCargaMasiva.setNumeroCedula(obtenerDato.get("NÚMERO DE CEDULA").trim());
					agenteCargaMasiva.setFechaAutorizacionCedula(fechaAutoCedula);
					agenteCargaMasiva.setFechaVencimientoCedula(fechaVencCedula);
					agenteCargaMasiva.setTipoCedula(tipoCed);
					agenteCargaMasiva.setNumeroFianza(obtenerDato.get("NÚMERO DE FIANZA").trim());
					agenteCargaMasiva.setMontoFianza(BigDecimal.valueOf(Integer.parseInt(obtenerDato.get("MONTO DE FIANZA").trim())));
					agenteCargaMasiva.setFechaAutorizacionFianza(fechaAutorFianza);
					agenteCargaMasiva.setFechaVencimientoFianza(fechaVencFianza);
					agenteCargaMasiva.setAfianzadora(idAfianzadora.get(0));
					agenteCargaMasiva.setPersona(persona_);
					saveAltaPorInternet(agenteCargaMasiva);
					registrosInsertados++;
					cont++;				
//				}
			} catch (Exception e) {
				e.toString();
				
				String error = e.toString().replace("java.lang.Exception:", "");
				registrosNoInsertados++;
				cont++;
				
				registrosAfectados.add("Registro "+String.valueOf(cont)+" "+error);
			}
		}
		registrosAfectados.add(String.valueOf(registrosInsertados));
		registrosAfectados.add(String.valueOf(registrosNoInsertados));
		cargaMasiva.clear();
		return registrosAfectados;
	}
	/**
	 * Guarda los domicilios de una persona
	 * @param persona
	 * @return
	 * @throws Exception
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private Agente guardarDomicilios(Agente agente) throws Exception{
		if(agente.getPersona()!=null){			
			Long idPersona=agente.getPersona().getIdPersona();
			if(agente.getPersona().getDomicilios()!=null){
				for(Domicilio domicilio:agente.getPersona().getDomicilios()){
					if(domicilio!=null){
						domicilio.setIdPersona(idPersona);
						if(domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().equals("")){
							domicilioFacadeRemote.save(domicilio, insertedAction);
						}
					}
				}
			}
		}
		return agente;
	}

	/**
	 * Guarda los datos de la fianza
	 */
	@Override
	public Agente saveDatosContablesAgente(Agente agente) throws Exception {
		if(agente==null || agente.getId()==null){
			onError("Favor de elegir un agente");
		}
		Agente persistenceObject=loadById(agente);
		if(persistenceObject==null){
			onError("Agente with id:"+agente.getId()+" is not exist!");
		}
		if(agente.getAfianzadora()==null || agente.getAfianzadora().getId()==null){
			onError("Favor de proporcionar la afianzadora");
		}
		if(agente.getMontoFianza()==null){
			onError("Favor de proporcionar el monto de la fianza");
		}
		Afianzadora afianzadora= new Afianzadora();
		if(agente.getAfianzadora()!=null){
			afianzadora=entidadService.getReference(Afianzadora.class, agente.getAfianzadora().getId());
		}
		persistenceObject.setAfianzadora(afianzadora);
		persistenceObject.setMontoFianza(agente.getMontoFianza());
		persistenceObject.setNumeroFianza(agente.getNumeroFianza());
		persistenceObject.setFechaAutorizacionFianza(agente.getFechaAutorizacionFianza());
		persistenceObject.setFechaVencimientoFianza(agente.getFechaVencimientoFianza());		
		return entidadService.save(persistenceObject);
	}
	
	public void disable(Agente agente) throws Exception{
		if(agente==null || agente.getId()==null){
			throw new Exception("Agente is null");
		}
		agente=loadById(agente);
		ValorCatalogoAgentes estatusInactivo=catalogoService.obtenerElementoEspecifico("Estatus de Agente (Situacion)","INACTIVO");
		ValorCatalogoAgentes motivoEstatus=catalogoService.obtenerElementoEspecifico("Motivo de Estatus del Agente","SOLICITUD DE LA EMPRESA");		
		Long idMotivoEstatus =motivoEstatus.getId();
		
		if(isNull(estatusInactivo)){
			onError("No existe el estatus INACTIVO en el catalogo de Estatus de Agente ('Situacion')");
		}
		agente.setTipoSituacion(estatusInactivo);
		agente.setIdMotivoEstatusAgente(idMotivoEstatus);
		update(agente);//Se actualiza su estatus
		//Se inserta otro registro del mismo agente pero con estatus de inactivo
		replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(agente, TipoAccionFuerzaVenta.INACTIVAR);
	}
	
	private void onError(String msg)throws Exception{
		throw new Exception(msg);
	}

	@Override
	public Agente saveDatosFiscalesAgente(Agente agente) throws Exception {
		try{
			Persona persona = agente.getPersona();
			Long idPersona=persona.getIdPersona();
			List<Domicilio> domicilios=persona.getDomicilios();
			Domicilio domicilio=domicilios.get(0);
			domicilio.setIdPersona(idPersona);
			domicilio.setTipoDomicilio("FISC");
			domicilioFacadeRemote.save(domicilio, null);
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return agente;
	}

	@Override
	public Agente saveDatosGeneralesAgente(Agente agente) throws Exception {
		return saveFull(agente);
	}

	@Override
	public Agente saveDomiciliosAgente(Agente agente) throws Exception {
		return guardarDomicilios(agente);
	}

	@Override
	public Agente unsuscribe(Agente agente,String accion) throws Exception {
		ValorCatalogoAgentes valorCatalogoAgentes =null;
		ValorCatalogoAgentes motivoEstatusAgente =null;
		Integer diasParaEmitirConRechazo = (agente.getDiasParaEmitirEnRechazo()!=null)?agente.getDiasParaEmitirEnRechazo():null;
		String estatus="";
		String motivoEstatus="";
		Date fechaActual=new Date();
		agente = loadById(agente);
		agente.setDiasParaEmitirEnRechazo(diasParaEmitirConRechazo);
		//se decidio que siempre se va a crear el codigo de usuario al autorizar o rechazar un agente
		Long claveAgente=(isNotNull(agente) && isNotNull(agente.getIdAgente()))?agente.getIdAgente():null;
		
		//Map<String, Object> params=new HashMap<String, Object>();
		//params.put("grupoCatalogoAgente.descripcion", "Estatus de Agente (Situacion)");
		if("rechazar".equalsIgnoreCase(accion)){
			agente.setFechaAutorizacionRechazo(fechaActual);
			if(agente.getDiasParaEmitirEnRechazo() > 0){
				//params.put("valor", "RECHAZADO CON EMISION");
				estatus="RECHAZADO CON EMISION";
				motivoEstatus="SIN MOTIVO";
				//crea su usuario de emision Delegada y de Midas 2
				String claveUsuario=crearUsuarioAgente(claveAgente, accion);
				agente.setCodigoUsuario(claveUsuario);
			}else{
				//params.put("valor", "RECHAZADO");
				estatus="RECHAZADO";
				motivoEstatus="NUEVO";//INFORMACION INCOMPLETA
			}
			agenteMidasService.enviarCorreo(agenteMidasService.obtenerCorreos(
					agente.getId(), GenericMailService.P_ALTA_AGENTE,
					GenericMailService.M_RECHAZO_ALTA_AGENTE), MailServiceSupport.mensajeAgenteAltaRechazo(), estatus,
					GenericMailService.T_GENERAL, null);
		}
		if("autorizar".equalsIgnoreCase(accion)){
			//params.put("valor", "AUTORIZADO");
			estatus="AUTORIZADO";
			motivoEstatus="REGULAR";
			agente.setFechaAutorizacionFianza(fechaActual);
			agente.setFechaActivacion(fechaActual);
			//Si ya se autorizo el agente, entonces se crea su usuario de emision Delegada y de Midas 2
			System.out.println("[AutorizacionAgentesAction]-entra a crearUsuarioAgente" );
			String claveUsuario=crearUsuarioAgente(claveAgente, accion);
			agente.setCodigoUsuario(claveUsuario);
			System.out.println("[AutorizacionAgentesAction]-sale de crearUsuarioAgente con claveUsuario="+claveUsuario);
			//se envia por correo la carta de bienvenida al correo principal del agente 
			List<String> address =  MailServiceSupport.getCorreoAgente(agente.getPersona());
			if (address != null && !address.isEmpty()) {
				mailService.sendMailAgenteNotificacion(address, null, null,
						"Notificacion", "", null, "Notificacion", null, GenericMailService.T_BIENVENIDO);
			}
			//se activa el envio de notificacion de autorizacion del movimiento de agentes en el autorizar agentes. JMMP 28/02/2014
			Map<String, Map<String, List<String>>> mapCorreos= new HashMap<String, Map<String, List<String>>>();
			mapCorreos = agenteMidasService
					.obtenerCorreos(agente.getId(),
							GenericMailService.P_ALTA_AGENTE,
							GenericMailService.M_AUTORIZACION_DEL_MOVIMIENTO_ALTA_AGENTE);
			agenteMidasService.enviarCorreo(mapCorreos, MailServiceSupport.mensajeAgenteAltaAutorizacion(), null,
					GenericMailService.T_GENERAL, null);
		}
		valorCatalogoAgentes=catalogoService.obtenerElementoEspecifico("Estatus de Agente (Situacion)",estatus);
		motivoEstatusAgente=catalogoService.obtenerElementoEspecifico("Motivo de Estatus del Agente",motivoEstatus);
		agente.setTipoSituacion(valorCatalogoAgentes);
		agente.setIdMotivoEstatusAgente(motivoEstatusAgente.getId());
		return entidadService.save(agente);
	}

	public List<Agente> findAgentesByPromotoria(Long idPromotoria) {
		if(idPromotoria!=null){
			Promotoria promotoria=new Promotoria();
			promotoria.setId(idPromotoria);
			Agente filtroAgente=new Agente();
			filtroAgente.setPromotoria(promotoria);
			return findByFilters(filtroAgente);
		}
		return new ArrayList<Agente>();
	}
	
	/**
     * Carga un agente
     * @param idAgente
     * @return
     */
    @Override
    public Agente loadById(Agente agente){
    	
    	return this.loadById(agente, null);
    }
    
	/**
     * Carga un agente
     * @param idAgente
     * @return
     */
    @Override
    public Agente loadByClave(Agente agente){
    	if(agente==null){
    		return null;
    	}
		Agente filtro=new Agente();
		filtro.setIdAgente(agente.getIdAgente());
		//Si especifica autorizado
		if(agente.getTipoSituacion() != null && agente.getTipoSituacion().getIdRegistro() != null){
			filtro.setTipoSituacion(agente.getTipoSituacion());
		}
		List<Agente> list=findByFilters(filtro);
		if(list!=null && !list.isEmpty()){
		      agente=list.get(0);
		      if(agente!=null){
		    	  Persona persona=agente.getPersona();
		    	  if(persona!=null && persona.getIdPersona()!=null){
		    		  persona=findById(Persona.class, persona.getIdPersona());
		    		  List<Domicilio> domicilios = this.findByProperty(Domicilio.class, "idPersona", persona.getIdPersona());
		    		  persona.setDomicilios(domicilios);
		    		  refresh(persona);
		    	  }
		    	refresh(agente);  
		      }		      
		}else{
			return null;
		}
		return agente;
    }
    
	/**
     * Carga un agente listado Solicitudes
     * @param idAgente
     * @return
     */
    @Override
    public Agente loadByClaveSolicitudes(Agente agente){
    	if(agente==null){
    		return null;
    	}
		Agente filtro=new Agente();
		filtro.setIdAgente(agente.getIdAgente());
		//Si especifica autorizado
		if(agente.getTipoSituacion() != null && agente.getTipoSituacion().getIdRegistro() != null){
			agente.getTipoSituacion().setId(null);
			filtro.setTipoSituacion(agente.getTipoSituacion());
		}
		List<Agente> list=findByFiltersSolicitudes(filtro);
		if(list!=null && !list.isEmpty()){
		      agente=list.get(0);
		      if(agente!=null){
		    	  Persona persona=agente.getPersona();
		    	  if(persona!=null && persona.getIdPersona()!=null){
		    		  persona=findById(Persona.class, persona.getIdPersona());
		    		  List<Domicilio> domicilios = this.findByProperty(Domicilio.class, "idPersona", persona.getIdPersona());
		    		  persona.setDomicilios(domicilios);
		    		  refresh(persona);
		    	  }
		    	refresh(agente);  
		      }		      
		}else{
			return null;
		}
		return agente;
    }

	@Override
	public List<TipoDocumentoAgente> findAllTypeDocumentsAgent() {
		return entidadService.findAll(TipoDocumentoAgente.class);
	}

	@Override
	public List<DocumentoAgente> findAllDocumentsAgent(Long id) {
		return entidadService.findByProperty(DocumentoAgente.class, "agente.id", id);
	}

	@Override
	public DocumentoAgente saveDocumentosAgente(DocumentoAgente documentoAgente) {
		return entidadService.save(documentoAgente);
	}

	@Override
	public boolean llenarYGuardarListaEntretenimientosAgente(String entretenimientosAgregados, Agente agenteInternet){
		try{
		if(entretenimientosAgregados!=null&&!entretenimientosAgregados.equals("")){
			String[]arrayEntretenimientos=entretenimientosAgregados.split(",");
				for(int i=0;i<=arrayEntretenimientos.length-1;i+=5){
					EntretenimientoAgente entretenimientoAgenteGuardar=new EntretenimientoAgente();
					ValorCatalogoAgentes valorCatalogoAgentes = new ValorCatalogoAgentes();
					Long idEntretenimiento=Long.valueOf(arrayEntretenimientos[i+2]);
					valorCatalogoAgentes=entityManager.getReference(ValorCatalogoAgentes.class, idEntretenimiento);						
					entretenimientoAgenteGuardar.setTipoEntretenimiento(valorCatalogoAgentes);
					entretenimientoAgenteGuardar.setComentarios(arrayEntretenimientos[i+4]);
					entretenimientoAgenteGuardar.setAgente(agenteInternet);			
					entidadService.save(entretenimientoAgenteGuardar);
				}			
				updateEntretenimientoAgente_hist(agenteInternet.getId());
			}
		return true;
		}catch(Exception e){
			return false;
		}		
	}
	public void updateEntretenimientoAgente_hist(Long idAgente){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
		Date date = new Date();
		String fechaFormateada = "to_timestamp('"+dateFormat.format(date)+"','DD/MM/YYYY HH24:MI:SS.FF')";
		StringBuilder queryUpdate = new StringBuilder();
		queryUpdate.append(" UPDATE midas.toentretenimientoagente_hist SET h_end_date = "+fechaFormateada);
		queryUpdate.append(" WHERE AGENTE_ID = "+idAgente);
		queryUpdate.append(" AND h_end_date IS NULL ");
		Query query = entityManager.createNativeQuery(queryUpdate.toString());
		query.executeUpdate();
		
		List<EntretenimientoAgente> lista = new ArrayList<EntretenimientoAgente>();
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("agente.id", idAgente);
		lista = entidadService.findByProperties(EntretenimientoAgente.class, param);
		for(EntretenimientoAgente obj : lista){
			StringBuilder queryInsertString = new StringBuilder();
			queryInsertString.append(" insert into midas.toentretenimientoagente_hist ");
			queryInsertString.append(" (ID, AGENTE_ID, IDTIPOENTRETENIMIENTO, COMENTARIOS, H_START_DATE) ");
			queryInsertString.append(" VALUES("+obj.getId()+", "+obj.getAgente().getId()+","+obj.getTipoEntretenimiento().getId()+",'"+obj.getComentarios()+"', "+fechaFormateada+")");
			Query queryInsert = entityManager.createNativeQuery(queryInsertString.toString());
			queryInsert.executeUpdate();
		}
	}		
	@Override
	public boolean llenarYGuardarListaHijosAgente(String hijosAgregados, Agente agenteInternet){
		try {
			if(hijosAgregados!=null&&!hijosAgregados.equals("")){
			String[]arrayHijos=hijosAgregados.split(",");
				for(int i=0;i<=arrayHijos.length-1;i+=7){
					HijoAgente hijoAgenteGuardar=new HijoAgente();
					hijoAgenteGuardar.setNombres(arrayHijos[i+2]);
					hijoAgenteGuardar.setApellidoPaterno(arrayHijos[i+3]);
					hijoAgenteGuardar.setApellidoMaterno(arrayHijos[i+4]);			
					hijoAgenteGuardar.setFechaNacimiento(Utilerias.obtenerFechaDeCadena(arrayHijos[i+5]));			
					hijoAgenteGuardar.setUltimaEscolaridad(arrayHijos[i+6]);
					hijoAgenteGuardar.setAgente(agenteInternet);
					entidadService.save(hijoAgenteGuardar);
				}
				updateListaHijosAgente_hist(agenteInternet.getId());
			}
			return true;
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				return false;
			}
	}
	/**
	 * metodo que sirve para el manejo del historico de la lista de hijos del agente
	 * @param idAgente
	 */
	public void updateListaHijosAgente_hist(Long idAgente){
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSS");
		Date date = new Date();
		String fechaFormateada = "to_timestamp('"+dateFormat.format(date)+"','DD/MM/YYYY HH24:MI:SS.FF')";
		StringBuilder queryUpdate = new StringBuilder();
		queryUpdate.append(" UPDATE midas.tohijoagente_hist SET h_end_date = "+fechaFormateada);
		queryUpdate.append(" WHERE AGENTE_ID = "+idAgente);
		queryUpdate.append(" AND h_end_date IS NULL ");
		Query query = entityManager.createNativeQuery(queryUpdate.toString());
		query.executeUpdate();
		
		List<HijoAgente> lista = new ArrayList<HijoAgente>();
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("agente.id", idAgente);
		lista = entidadService.findByProperties(HijoAgente.class, param);
		for(HijoAgente obj : lista){
			DateFormat formatoFech = new SimpleDateFormat("dd/MM/yyyy");
			StringBuilder queryInsertString = new StringBuilder();
			queryInsertString.append(" insert into midas.tohijoagente_hist ");
			queryInsertString.append(" (ID, AGENTE_ID, APELLIDOPATERNO, APELLIDOMATERNO, NOMBRES, FECHANACIMIENTO, ULTIMAESCOLARIDAD, H_START_DATE) ");
			queryInsertString.append(" VALUES("+obj.getId()+", "+obj.getAgente().getId()+",'"+obj.getApellidoPaterno()+"','"+obj.getApellidoMaterno()+"', '");
			queryInsertString.append(obj.getNombres()+"',to_date('"+formatoFech.format(obj.getFechaNacimiento())+"','DD/MM/YYYY'),'"+obj.getUltimaEscolaridad()+"', "+fechaFormateada+")");
			Query queryInsert = entityManager.createNativeQuery(queryInsertString.toString());
			queryInsert.executeUpdate();
		}
		
	}
		
	@Override
	public void eliminarDocumentosAgente(Long idAgente) {
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("delete from ProductoBancarioAgente model where model.agente.id= :idAgente");
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("idAgente", idAgente);
		entidadDao.updateWithQuery(queryString.toString(), parametros);
	}

	@Override
	public void eliminarEntretenimientosAgente(Long idAgente) {
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("delete from EntretenimientoAgente model where model.agente.id= :idAgente");
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("idAgente", idAgente);
		entidadDao.updateWithQuery(queryString.toString(), parametros);		
	}
	
	@Override
	public void eliminarHijosAgente(Long idAgente) {
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("delete from HijoAgente model where model.agente.id= :idAgente");
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("idAgente", idAgente);
		entidadDao.updateWithQuery(queryString.toString(), parametros);
	}
	
	@Override
	public Agente findByClaveAgente(Agente agente)throws Exception{
		if(agente==null){
			onError("Favor de proporcionar los datos del agente");
		}
		if(agente.getIdAgente()==null){
			onError("Favor de proporcionar la clave del Agente");
		}
		//-1 indica que es un agente dado de alta por internet, y cualquier otro deberia de ser por alta por medio de los catalogos.
		if(agente.getIdAgente()!=null){
			Agente filtro=new Agente();
			filtro.setIdAgente(agente.getIdAgente());
			List<Agente> lista=findByFilters(filtro);
			//Si la lista esta vacia o es nula, significa que no hay ninguna agente con esa clave, por lo tanto es valida.
			if(lista!=null && !lista.isEmpty()){
				agente = lista.get(0);
			}
		}
		return agente;
	}

	@Override
	public List<TipoDocumentoAgente> findTypeDocuments(Long idAgente, String tipoDocumento) {		
		try {			
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();			
			ValorCatalogoAgentes moduloDocumento = catalogoService.obtenerElementoEspecifico("Tipo Seccion Documentos",tipoDocumento);		
			parametros.put("idTipoSeccion", moduloDocumento);
				if(tipoDocumento.equals("Alta de Agentes")){
					Agente agente=findById(Agente.class, idAgente);
					Persona tipoPersDocumento= new Persona();
					tipoPersDocumento=findById(Persona.class, agente.getPersona().getIdPersona());
					if(tipoPersDocumento.getClaveTipoPersona()==1){
						parametros.put("claveAplicaPersonaFisica", 1);		
					}
					else{
						parametros.put("claveAplicaPersonaMoral", 1);			
					}	
				}	
			 List<TipoDocumentoAgente> lista=entidadService.findByProperties(TipoDocumentoAgente.class, parametros);
			 return lista;
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				return new ArrayList<TipoDocumentoAgente>(); 
			}
	}
	public void saveInSeycos(Agente agente)throws Exception{
		if(isNull(agente)){
			onError("Agente is null!");
		}
		if(isNull(agente.getIdAgente())){
			onError("Favor de proporcionar la clave del agente");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_cCatAgte";
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteMidas.replicanEnSeycos..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			Persona persona=agente.getPersona();
			Long idPersona=(persona!=null)?persona.getIdPersona():null;

			ValorCatalogoAgentes tipoSituacion=(isNotNull(agente.getTipoSituacion()) && isNotNull(agente.getTipoSituacion().getId()))?agente.getTipoSituacion():null;
			String situacionAgte=null;
			if(isNotNull(tipoSituacion)){
				tipoSituacion=catalogoService.loadById(tipoSituacion);
				//Si el tipo de situacion del agente no es Autorizado, entonces se registra en Seycos como inactivo o baja
				//Hasta que se autorice el agente, se marcara como activo en Seycos
				situacionAgte=(isNotNull(tipoSituacion) && isValid(tipoSituacion.getValor()) && "AUTORIZADO".equalsIgnoreCase(tipoSituacion.getValor()))?"A":"B";
			}
			
			Promotoria promotoria=agente.getPromotoria();
			Long idPromotoria=(isNotNull(promotoria))?promotoria.getId():null;
			Long clavePromotoria=(isNotNull(promotoria))?promotoria.getIdPromotoria():null;
			if(isNull(promotoria)){
				onError("Favor de proporcionar la promotoria a la que pertenece el agente "+agente.getId());
			}else if(isNull(idPromotoria)|| isNull(clavePromotoria)){
				onError("El agente con la clave:"+agente.getIdAgente()+" no esta relacionado a una promotoria.");
			}
			Afianzadora afianzadora=agente.getAfianzadora();
			Long idAfianzadora=(isNotNull(afianzadora) && isNotNull(afianzadora.getIdAfianzadora()))?afianzadora.getIdAfianzadora():null;
			String email=null;
			if(isNotNull(persona) && isNotNull(idPersona)){
				PersonaSeycosDTO personaSeycos=personaSeycosFacadeRemote.findById(idPersona);
				if(isNull(personaSeycos) || isNull(personaSeycos.getIdPersona())){
					onError("No se encontro al agente con la clave:"+agente.getId()+", no esta relacionado a una persona valida");
				}

				email = personaSeycos.getEmail();
			}
			ValorCatalogoAgentes tipoAgente = new ValorCatalogoAgentes();
			if(agente.getIdTipoAgente()!=null){
				tipoAgente = this.entidadService.findById(ValorCatalogoAgentes.class, agente.getIdTipoAgente());
			}
			ValorCatalogoAgentes tipoCedula=agente.getTipoCedula();
			String tipoDeCedula=null;
			if(isNotNull(tipoCedula)){
				tipoCedula=catalogoService.loadById(tipoCedula);
				tipoDeCedula=(isNotNull(tipoCedula) && isValid(tipoCedula.getClave()))?tipoCedula.getClave().trim():"DEF";
			}
			String situacionCedula="D";//Definitiva por default
			String aplicaComision=(agente.getClaveComision()==1)?"V":"F";
//			String aplicaComision="F";
			String aplicaGeneraCheque=(agente.getClaveGeneraCheque()==1)?"V":"F";
//			String aplicaGeneraCheque="F";
			String aplicaEdoCta=(agente.getClaveImprimeEstadoCta()==1)?"V":"F";
//			String aplicaEdoCta="F";
			String aplicaContabComision=(agente.getClaveContabilizaComision()==1)?"V":"F";
//			String aplicaContabComision="F";
			Date fechaInicioFianza=(agente.getFechaAutorizacionFianza()!=null)?agente.getFechaAutorizacionFianza(): null;
			Date fechaFinFianza=(isNotNull(agente.getFechaVencimientoFianza()))?agente.getFechaVencimientoFianza(): null;
			String numFianza=(isValid(agente.getNumeroFianza()))?agente.getNumeroFianza():".";
			//se obtienen los datos del producto bancario 
			ProductoBancarioAgente prodBancario = new ProductoBancarioAgente();
			List<ProductoBancarioAgente> listProductoBancario = new ArrayList<ProductoBancarioAgente>();
			prodBancario.setAgente(agente);
			prodBancario.setClaveDefault(1);
			listProductoBancario = productoService.findByFilters(prodBancario);
			if(!listProductoBancario.isEmpty() && listProductoBancario.get(0)!=null){
				prodBancario = listProductoBancario.get(0);
			}else{
				prodBancario=null;
			}
			Integer idProductoBancario = null;
			String numCta ="";
			if(prodBancario!=null){
				if(prodBancario.getProductoBancario()!=null && prodBancario.getProductoBancario().getBanco()!=null && prodBancario.getProductoBancario().getBanco().getIdBanco()!=null){
					idProductoBancario = prodBancario.getProductoBancario().getBanco().getIdBanco();
				}
				if(prodBancario.getNumeroClabe()!=null){
					numCta =prodBancario.getNumeroClabe();
				}else if(prodBancario.getNumeroCuenta()!=null){
					numCta =prodBancario.getNumeroCuenta();
				}
			}
			
			
			//********
			storedHelper.estableceParametro("ptipomov", val(situacionAgte));
			storedHelper.estableceParametro("pidagente", val(agente.getIdAgente()));
			storedHelper.estableceParametro("pidpersona", val(idPersona));
			storedHelper.estableceParametro("pcvetagente", val(tipoAgente.getClave().trim()));//val(claveTipoAgente));
			storedHelper.estableceParametro("pidsupervisoria", val(clavePromotoria));
			storedHelper.estableceParametro("pnumcedula", val(agente.getNumeroCedula()));
			storedHelper.estableceParametro("pfautorizacion", val(agente.getFechaAutorizacionCedula()));
			storedHelper.estableceParametro("pfvenctoautor", val(agente.getFechaVencimientoCedula()));
			storedHelper.estableceParametro("pnumfianza", val(numFianza));
			storedHelper.estableceParametro("pfinifianza", val(fechaInicioFianza));
			storedHelper.estableceParametro("pffinfianza", val(fechaFinFianza));
			storedHelper.estableceParametro("pidafianza", val(idAfianzadora));
			storedHelper.estableceParametro("pemail", val(email));
			storedHelper.estableceParametro("pfalta", val(agente.getFechaAlta()));
			storedHelper.estableceParametro("ptipocedula", val(tipoDeCedula));
			storedHelper.estableceParametro("psitcedula", val(situacionCedula));
			storedHelper.estableceParametro("pbcomision", val(aplicaComision));//Este campo no se considera en MIDAS.toAgente
			storedHelper.estableceParametro("pgeneracheque", val(aplicaGeneraCheque));
			storedHelper.estableceParametro("pbimpedocta", val(aplicaEdoCta));
			storedHelper.estableceParametro("pbcontabcomis", val(aplicaContabComision));
			storedHelper.estableceParametro("pimpfianza", val(agente.getMontoFianza()));
			storedHelper.estableceParametro("pid_banco", val(idProductoBancario));
			storedHelper.estableceParametro("pnumcuenta", val(numCta));
			storedHelper.estableceParametro("pidmotsitagt",val(agente.getIdMotivoEstatusAgente()));
			
			LogDeMidasInterfaz.log("ptipomov=: "+val(situacionAgte)
					+"; pidagente=: "+ val(agente.getIdAgente())
					+"; pidpersona=: "+ val(idPersona)
					+"; pcvetagente=: "+ val(tipoAgente.getClave().trim())
					+"; pidsupervisoria=: "+ val(clavePromotoria)
					+"; pnumcedula=: "+ val(agente.getNumeroCedula())
					+"; pfautorizacion=: "+ val(agente.getFechaAutorizacionCedula())
					+"; pfvenctoautor=: "+ val(agente.getFechaVencimientoCedula())
					+"; pnumfianza=: "+ val(numFianza)
					+"; pfinifianza=: "+ val(fechaInicioFianza)
					+"; pffinfianza=: "+ val(fechaFinFianza)
					+"; pidafianza=: "+ val(idAfianzadora)
					+"; pemail=: "+ val(email)
					+"; pfalta=: "+ val(agente.getFechaAlta())
					+"; ptipocedula=: "+ val(tipoDeCedula)
					+"; psitcedula=: "+ val(situacionCedula)
					+"; pbcomision=: "+ val(aplicaComision)//Este campo no se considera en MIDAS.toAgente
					+"; pgeneracheque=: "+ val(aplicaGeneraCheque)
					+"; pbimpedocta=: "+ val(aplicaEdoCta)
					+"; pbcontabcomis=: "+ val(aplicaContabComision)
					+"; pimpfianza=: "+ val(agente.getMontoFianza())
					+"; pid_banco=: "+ val(idProductoBancario)
					+"; pnumcuenta=: "+ val(numCta)
					+"; pidmotsitagt=: "+ val(agente.getIdMotivoEstatusAgente())
					+"SEYCOS.PKG_INT_MIDAS_E2.stp_cCatAgte(ptipomov, pidagente, pidpersona, pcvetagente, pidsupervisoria, pnumcedula, pfautorizacion, pfvenctoautor, pnumfianza ,pfinifianza, pffinfianza, pidafianza, pemail, pfalta, ptipocedula, psitcedula, pbcomision, pgeneracheque, pbimpedocta, pbcontabcomis, pimpfianza, pid_banco, pnumcuenta, pidmotsitagt);"+ this, Level.INFO, null);
			storedHelper.ejecutaActualizar();
			
			LogDeMidasInterfaz.log("Se ha guardado Agente "+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de AgenteMidas.replicanEnSeycos..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
				throw new Exception(descErr);
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, agente.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AgenteMidas.replicanEnSeycos..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AgenteMidas.replicanEnSeycos..." + this, Level.WARNING, e);
			throw e;
		}
	}
	
	
	private Object val(Object value){
		return (value!=null)?value:"";
	}
	/****Sets && gets**************************************************/
	
	
	@EJB
	public void setPersonaSeycosFacade(PersonaSeycosFacadeRemote personaSeycosFacade) {
		this.personaSeycosFacade = personaSeycosFacade;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@EJB
	public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}
	
	@EJB
	public void setPersonaFacadeRemote(PersonaSeycosFacadeRemote personaSeycosFacadeRemote) {
		this.personaSeycosFacadeRemote = personaSeycosFacadeRemote;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setDomicilioFacadeRemote(DomicilioFacadeRemote domicilioFacadeRemote) {
		this.domicilioFacadeRemote = domicilioFacadeRemote;
	}
	
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	@EJB	
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	@EJB
	public void setReplicarFuerzaVentaSeycosFacade(ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade) {
		this.replicarFuerzaVentaSeycosFacade = replicarFuerzaVentaSeycosFacade;
	}
	@EJB
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	public String[] generateExpedientAgent(Long id) throws Exception {
		Agente agente=entidadService.findById(Agente.class, id);
		Persona tipoPersDocumento= new Persona();
		tipoPersDocumento=entidadService.findById(Persona.class, agente.getPersona().getIdPersona());				
		String[] fieldValues = new String[6];
		
		fieldValues[0]=id.toString();
		if(tipoPersDocumento.getClaveTipoPersona()==1){
		fieldValues[1]=tipoPersDocumento.getNombre();		
		fieldValues[2]=tipoPersDocumento.getApellidoPaterno();
		fieldValues[3]=tipoPersDocumento.getApellidoMaterno();
		}
		else{
			fieldValues[1]=tipoPersDocumento.getRazonSocial();		
			fieldValues[2]=tipoPersDocumento.getRazonSocial();
			fieldValues[3]=tipoPersDocumento.getRazonSocial();
		}
		fieldValues[4]="1";
		fieldValues[5]="1";	
		return fortimaxService.generateExpedient("AGENTES", fieldValues);		
	}

	@Override
	public String[] generateDocument(Long id,String tituloAplicacion, String documentName,String folderName) throws Exception {		
		return fortimaxService.generateDocument(id,tituloAplicacion, documentName, folderName);
	}

	@Override
	public String[] getDocumentFortimax(Long id, String tituloAplicacion) throws Exception {
		return fortimaxService.getDocumentFortimax(id,tituloAplicacion);
	}

	@Override
	public String[] generateLinkToDocument(Long id)	throws Exception {
		return fortimaxService.generateLinkToDocument(id, "AGENTES", "");
	}

	public Integer getPagingCount() {
		return pagingCount;
	}
 
	public void setPagingCount(Integer pagingCount) {
		this.pagingCount = pagingCount;
	}
	public Long getPagingStart() {
		return pagingStart;
	}

	public void setPagingStart(Long pagingStart) {
		this.pagingStart = pagingStart;
	}

	public Long getPagingLimit() {
		return pagingLimit;
	}

	public void setPagingLimit(Long pagingLimit) {
		this.pagingLimit = pagingLimit;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<AgenteView> findByFilterLightWeight(Map<String, Object> filters){ 
		final Object id = filters.get("id");
		final Object idAgente = filters.get("idAgente");
		final Object razonSocial = filters.get("razonSocial");
		final Object nombreCompleto = filters.get("nombreCompleto");
		final Object rfc = filters.get("rfc");
		final Object idTipoSituacion = filters.get("idTipoSituacion");
		final Object idNegocio = filters.get("idNegocio");
		final Object codigoUsuario = filters.get("codigoUsuario");
		final Object idPromotoria = filters.get("idPromotoria");
		final Object fechaVencimientoCedula = filters.get("fechaVencimientoCedula");
		
		
		final StringBuilder sb = new StringBuilder();
		sb.append(" SELECT /*+ INDEX(pext PK_PERSONA_EXT) INDEX(p PK_PERSONA) INDEX(rfc PK_RFC)*/ ");
		sb.append(" DISTINCT "); 
		sb.append(" agente.id as id, ");
		sb.append(" agente.idAgente as idAgente, ");
		sb.append(" NULL as promotoria, ");
		sb.append(" tipoSituacion.id as idPromotoria, ");
		sb.append(" agente.idPersona,");
		sb.append(" persona.nombre as nombreCompleto, ");
		sb.append(" rfc.siglas_rfc || rfc.f_rfc || rfc.homoclave_rfc  as codigoRfc, ");
		sb.append(" perExt.curp as codigoCurp, ");
		sb.append(" tipoCedula.valor as tipoCedulaAgente , ");
		sb.append(" NULL as ejecutivo, ");
		sb.append(" tipoSituacion.valor as tipoSituacion ");
		sb.append(" FROM MIDAS.toAgente agente ");
		sb.append(" LEFT JOIN seycos.persona persona ON (persona.id_persona = agente.idPersona) ");
		sb.append(" LEFT JOIN seycos.persona_ext perExt on perExt.id_persona = persona.id_persona  ");
		sb.append(" LEFT JOIN seycos.rfc rfc on rfc.ID_PERSONA = persona.ID_PERSONA "); 
		sb.append(" INNER JOIN MIDAS.toValorCatalogoAgentes tipoCedula ON (tipoCedula.id = agente.idTipoCedulaAgente " );
		sb.append(" AND tipoCedula.grupoCatalogoAgentes_id = (SELECT id FROM MIDAS.tcGrupoCatalogoAgentes WHERE descripcion = 'Tipos de Cedula de Agente')) ");
		sb.append(" INNER JOIN MIDAS.toValorCatalogoAgentes tipoSituacion ON (tipoSituacion.id = agente.idSituacionAgente ");
		sb.append(" AND tipoSituacion.grupoCatalogoAgentes_id = (SELECT id FROM MIDAS.tcGrupoCatalogoAgentes WHERE descripcion = 'Estatus de Agente (Situacion)')) ");
		if(idNegocio != null) {
			sb.append (" INNER JOIN MIDAS.TONEGAGENTE toNegAgente on (toNegAgente.idAgente = agente.id) ");
		}
		final Map<Integer, Object> params = new HashMap<Integer, Object>();
		int i = 1;
		// Para simplificar la logica agrego un where que siempre cumple
		sb.append(" WHERE agente.id = agente.id ");
		if (id != null) {
			sb.append(" AND agente.id = ? ");
			params.put(i++, id);
		}
		if (idAgente != null) {
			sb.append(" AND agente.idAgente = ? ");
			params.put(i++, idAgente);
		}
		if (razonSocial != null) {
			sb.append(" AND UPPER(persona.nombre) LIKE UPPER(?) ");
			params.put(i++, '%' + razonSocial.toString() + '%');
		}
		if (nombreCompleto != null) {
			sb.append(" AND UPPER(persona.nombre) LIKE UPPER(?) ");
			params.put(i++, '%' + nombreCompleto.toString() + '%');
		}
		if (rfc != null) {
			sb.append(" AND UPPER(rfc.codigoRfc) LIKE UPPER(?) ");
			params.put(i++, '%' + rfc.toString() + '%');
		}
		if (idTipoSituacion != null) {
			sb.append(" AND tipoSituacion.id = ? ");
			params.put(i++, idTipoSituacion);
		}
		if (idNegocio != null) {
			sb.append(" AND toNegAgente.idToNegocio = ? ");
			params.put(i++, idNegocio);
		}
		
		if (codigoUsuario != null) {
			sb.append(" AND agente.codigoUsuario = ? ");
			params.put(i++, codigoUsuario);
		}
		
		if (idPromotoria != null) {
			sb.append(" AND idPromotoria = ? ");
			params.put(i++, idPromotoria);
		}
		if(fechaVencimientoCedula != null) {
			sb.append(" AND fechaVencimientoCedula >= ? ");
			params.put(i++, fechaVencimientoCedula);			
		}
		
		try {
			ValorCatalogoAgentes tipoAgentePromotor=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "AGENTE PROMOTOR");
			if(isNotNull(tipoAgentePromotor) && isNotNull(tipoAgentePromotor.getId())){
//				sb.append(" AND agente.IDTIPOAGENTE NOT IN (?) ");
				sb.append(" AND NOT(agente.idTipoAgente in(?) and agente.idAgente is null)");
				params.put(i++, tipoAgentePromotor.getId());
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
        sb.append(" ORDER BY persona.nombre");
		final Query query = entityManager.createNativeQuery(sb.toString(), AgenteView.class);
		for (Integer key : params.keySet()) {
			query.setParameter(key, params.get(key));
		}
		return query.getResultList();
		
	}
	
	@Override
	public List<AgenteView> findByFilterLightWeight(Agente filtroAgente) {
		final Map<String, Object> filters = new HashMap<String, Object>();
		if (filtroAgente != null) {
			filters.put("id", filtroAgente.getId());
			filters.put("codigoUsuario", filtroAgente.getCodigoUsuario());
			filters.put("fechaVencimientoCedula", filtroAgente.getFechaVencimientoCedula());
			if (filtroAgente.getPromotoria() != null) {
				filters.put("idPromotoria", filtroAgente.getPromotoria().getId());
			}
			if (filtroAgente.getId() == null) {
				filters.put("idAgente", filtroAgente.getIdAgente());
				if (filtroAgente.getIdAgente() == null) {
					if (filtroAgente.getPersona() != null) {
						final Persona persona = filtroAgente.getPersona();
						filters.put("razonSocial", persona.getRazonSocial());
						filters.put("nombreCompleto", persona.getNombreCompleto());
						filters.put("rfc", persona.getRfc());
					}
					final ValorCatalogoAgentes tipoSituacion = filtroAgente.getTipoSituacion();
					if (tipoSituacion != null) {
						filters.put("idTipoSituacion", tipoSituacion.getId());
					}
				}
			}
		}
		return findByFilterLightWeight(filters);
	}
	
	public String validaRegistroCargaMasiva(Map<String, String> obtenerDato2, List<Date>fechas,
			List<Afianzadora> idAfianzadora, ValorCatalogoAgentes tipoCed, String sexo, EstadoDTO edoNac, MunicipioDTO municipionac, EstadoCivilDTO idEstadoCivil){
		
		String fieldError="";
		if(idEstadoCivil==null||idEstadoCivil.getIdEstadoCivil()==null){
				fieldError="ESTADO CIVIL,";	
		}
		if(fechas.get(3)==null){
			fieldError=fieldError +" FECHA DE AUTORIZACION FIANZA,";
		}
		if(fechas.get(4)==null){
			fieldError=fieldError +" FECHA DE VENCIMIENTO FIANZA,";
		}
		if(fechas.get(2)==null){
			fieldError=fieldError +" FECHA DE VENCIMIENTO CEDULA,";
		}
		if(fechas.get(1)==null){
			fieldError=fieldError +" FECHA DE AUTORIZACIÓN CEDULA,";
		}
		if(fechas.get(0)==null){
			fieldError=fieldError +" FECHA DE NACIMIENTO,";
		}
		if(idAfianzadora.isEmpty()){
			fieldError=fieldError +" AFIANZADORA,";
		}
		if(tipoCed==null || tipoCed.getId()==null){
			fieldError=fieldError +" TIPO DE CEDULA,";
		}
		if(sexo==""){
			fieldError=fieldError +" SEXO,";
		}
		if(edoNac==null || edoNac.getId()==null){
			fieldError=fieldError +" ESTADO DE NACIMIENTO,";
		}
		if(municipionac==null || municipionac.getId()==null){
			fieldError=fieldError +" CIUDAD DE NACIMIENTO,";
		}
		if(obtenerDato2.get("NÚMERO DE FIANZA").trim().equals("")){
			fieldError=fieldError +" NÚMERO DE FIANZA,";
		}
		if(promotoriaId.isEmpty()){
			fieldError=fieldError +" PROMOTORÍA,";
		}
		if(obtenerDato2.get("NÚMERO DE CEDULA").trim().equals("")){
			fieldError=fieldError +" NÚMERO DE CEDULA,";
		}
		if(obtenerDato2.get("MONTO DE FIANZA").trim().equals("")){
			fieldError=fieldError +" MONTO DE FIANZA,";
		}
		if(obtenerDato2.get("﻿NOMBRE").trim().equals("")){
			fieldError=fieldError +" NOMBRE,";
		}
		if(obtenerDato2.get("APELLIDO PATERNO").trim().equals("")){
			fieldError=fieldError +" APELLIDO PATERNO,";
		}
		if(obtenerDato2.get("APELLIDO MATERNO").trim().equals("")){
			fieldError=fieldError +" APELLIDO MATERNO,";
		}
		if(obtenerDato2.get("RFC").trim().equals("")){
			fieldError=fieldError +" RFC,";
		}
		if(obtenerDato2.get("CURP").trim().equals("")){
			fieldError=fieldError +" CURP,";
		}
		if(obtenerDato2.get("TELEFONO DE OFICINA").trim().equals("") || obtenerDato2.get("TELEFONO DE OFICINA").length()>10){
			fieldError=fieldError +" TELEFONO DE OFICINA,";
		}
		if(obtenerDato2.get("CORREO ELECTRONICO").trim().equals("")){
			fieldError=fieldError +" CORREO ELECTRONICO,";
		}
		if(obtenerDato2.get("TELEFONO CELULAR").trim().equals("") || obtenerDato2.get("TELEFONO CELULAR").length()>13){
			fieldError=fieldError +" TELEFONO CELULAR,";
		}
		if(obtenerDato2.get("TELEFONO CASA").trim().equals("") || obtenerDato2.get("TELEFONO CASA").length()>10){
			fieldError=fieldError +" TELEFONO CASA,";
		}
		return fieldError;
	}
	
	//metodo para crear un domcilio en carga masiva de agentes
	public Domicilio  direccionPorCPyColonia(String cp, String colonyname, String calleNumero){
		Domicilio domicilioNuevo = new Domicilio();
		List<CrearDireccionPorCpYColonia> listaDomicilio = new ArrayList<CrearDireccionPorCpYColonia>();
		String queryString;
		if(!cp.equals("") || !colonyname.equals("")){
			queryString="SELECT VW_STATE.COUNTRY_ID as idPais, VW_STATE.STATE_ID as idEstado, VW_COLONY.CITY_ID as idCiudad, VW_COLONY.COLONY_ID as idcolonia, ZIP_CODE as cp "+
							"from MIDAS.VW_STATE " +
							"INNER JOIN MIDAS.VW_MUNICIPALITY ON VW_STATE.STATE_ID = VW_MUNICIPALITY.STATE_ID "+
							"INNER JOIN MIDAS.VW_COLONY on  VW_COLONY.CITY_ID = VW_MUNICIPALITY.MUNICIPALITY_ID " +
							"WHERE ZIP_CODE LIKE '"+cp+"%' and COLONY_NAME LIKE '"+colonyname+"%'";
			
			Query query=entityManager.createNativeQuery(queryString,CrearDireccionPorCpYColonia.class);
			listaDomicilio = query.getResultList();
			if(!listaDomicilio.isEmpty()){
				domicilioNuevo.setClavePais(listaDomicilio.get(0).getIdPais());
				domicilioNuevo.setClaveCiudad(listaDomicilio.get(0).getIdCiudad());
				domicilioNuevo.setClaveEstado(listaDomicilio.get(0).getIdEstado());
				domicilioNuevo.setCodigoPostal(listaDomicilio.get(0).getCp());
				domicilioNuevo.setIdColonia(listaDomicilio.get(0).getIdcolonia());
				domicilioNuevo.setCalleNumero(calleNumero);
				domicilioNuevo.setNombreColonia(colonyname);
			}
		}
		return domicilioNuevo;
	}
	
	//metodo para obtener el estado civil de una persona
	public EstadoCivilDTO getEstadoCivil(String estadoCivil){
		
		List<EstadoCivilDTO>listaEdoCivil=new ArrayList<EstadoCivilDTO>();
		EstadoCivilDTO edoCivil = new EstadoCivilDTO();
		String queryString;
		queryString="SELECT idEstadoCivil, nombreEstadoCivil FROM MIDAS.VW_ESTADO_CIVIL WHERE UPPER(nombreEstadoCivil) LIKE '%"+estadoCivil+"%'";
		Query query = entityManager.createNativeQuery(queryString, EstadoCivilDTO.class);
		listaEdoCivil = query.getResultList();
		if(!listaEdoCivil.isEmpty()){
			edoCivil = listaEdoCivil.get(0);
		}
		return edoCivil;
	}		

	/**
	 * Se crea el usuario de Agentes para ASM y Emision Delegada.
	 * @param claveAgente
	 * @throws Exception
	 * @return claveUsuario
	 */
	private String crearUsuarioAgente(Long claveAgente, String accion) throws Exception{
		if(isNull(claveAgente)){
			onError("Agente is null!");
		}
		StoredProcedureHelper storedHelper = null;
		String claveUsuario=null;
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stpAgregarUsuarioAgenteASM";
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteMidas.crearUsuarioAgente..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(UsuarioDTO.class.getCanonicalName(),new String[]{"usuarionombre"},new String[]{"claveUsuario"});
			storedHelper.estableceParametro("pClaveAgente", val(claveAgente));
			UsuarioDTO usuario=(UsuarioDTO)storedHelper.obtieneResultadoSencillo();
			claveUsuario=(isNotNull(usuario))?usuario.getUsuarionombre():null;
			LogDeMidasInterfaz.log("Se ha creado el usuario ["+claveUsuario+"] "+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de AgenteMidas.crearUsuarioAgente..." + this, Level.INFO, null);
			//Una vez que se crea el usuario del ASM se activa el agente en seycos. El usuario es igual al password del agente.
			activarAgenteSeycos(claveAgente);
			Agente agente = new Agente();
			agente.setIdAgente(claveAgente);
			agente = agenteMidasService.loadByClave(agente);
			List<String> address =  MailServiceSupport.getCorreoAgente(agente.getPersona());
			StringBuilder mensaje = new StringBuilder();
			mensaje.append(MailServiceSupport.mensajeAgenteAltaAutorizacion());
			mensaje.append("<br/> Usuario: " + claveUsuario + "<br/> Clave:" + claveUsuario);
			TransporteImpresionDTO contrato =  impresionesService.imprimirContratoAgente(agente, TipoContrato.AGENTE, new Locale("es", "MX"));
			List<ByteArrayAttachment> attachment = null;
			if("autorizar".equalsIgnoreCase(accion)){
				if (contrato != null && contrato.getByteArray() != null) {
					attachment =  new ArrayList<ByteArrayAttachment>();
					ByteArrayAttachment file = new ByteArrayAttachment();
					file.setContenidoArchivo(contrato.getByteArray());
					file.setNombreArchivo("contratoAgente.pdf");
					file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
					attachment.add(file);
				} 
				if (address != null && !address.isEmpty()) {
					mailService.sendMailAgenteNotificacion(address, null, null,
							"Nuevo Usuario", mensaje.toString(), attachment,
							"Nuevo Usuario", "", GenericMailService.T_GENERAL);
				}
			}
			//Se agrega codigo que permite crear un registro de saldos cuando se autoriza o rechaza con emision un agente
			crearRegistroSaldoAgenteMidas(claveAgente);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, Agente.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AgenteMidas.crearUsuarioAgente..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AgenteMidas.crearUsuarioAgente..." + this, Level.WARNING, e);
			throw e;
		}
		return claveUsuario;
	}
	
	/**
	 * Activa agente de seycos al momento de autoriazar un agente, esto es para la replica de agentes.
	 * @param claveAgente Clave del agente a actualizar en seycos.
	 * @throws Exception
	 */
	private void activarAgenteSeycos(Long claveAgente) throws Exception{
		if(isNull(claveAgente)){
			onError("Agente is null!");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stpActivarAgenteSeycos";
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteMidas.activarAgenteSeycos..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pClaveAgente", val(claveAgente));
			storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz.log("Se ha guardado ejecutivo "+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de stpActivarAgenteSeycos..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, Agente.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de stpActivarAgenteSeycos..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en stpActivarAgenteSeycos..." + this, Level.WARNING, e);
			throw e;
		}
	}
	
	/**
	 * Crea un registro de saldo para el agente solo si no existe.
	 * @param claveAgente Clave del agente a actualizar en seycos.
	 * @throws Exception
	 */
	private void crearRegistroSaldoAgenteMidas(Long claveAgente) throws Exception{
		if(isNull(claveAgente)){
			onError("Agente is null!");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="MIDAS.PKGCALCULOS_AGENTES.stp_CrearRegSaldoAgente";
		try {
			LogDeMidasInterfaz.log("Entrando a crearRegistroSaldoAgenteMidas..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pId_Agente", val(claveAgente));
			storedHelper.ejecutaActualizar();			
			LogDeMidasInterfaz.log("Saliendo de crearRegistroSaldoAgenteMidas..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, Agente.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de crearRegistroSaldoAgenteMidas..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en crearRegistroSaldoAgenteMidas..." + this, Level.WARNING, e);
			throw e;
		}
	}
	
	@Override
    public Agente loadById(Agente agente, String fechaHistorico){
    	if(agente==null){
    		return null;
    	}
		Agente filtro=new Agente();
		filtro.setId(agente.getId());
		List<Agente> list=findByFilters(filtro, fechaHistorico);
		if(list!=null && !list.isEmpty()){
		      agente=list.get(0);
		      if(agente!=null){
		    	  Persona persona=agente.getPersona();
		    	  if(persona!=null && persona.getIdPersona()!=null){
		    		  persona=findById(Persona.class, persona.getIdPersona());
		    		  
//		    		  if (fechaHistorico != null && !fechaHistorico.isEmpty()) 
//		    		  {		    			  
//		    			  List<DomicilioSeycos> domicilios = this.findByProperty(DomicilioSeycos.class, "idPersona", persona.getIdPersona());
//		    			  
//		    			  List<Domicilio> domiciliosHistoricos = new ArrayList<Domicilio>();
//		    			  
//		    			  for(DomicilioSeycos domicilioTmp: domicilios)
//		    			  {
//		    				  Domicilio domicilio= new Domicilio();
//		    				  
//		    				  try {
//			    					BeanUtils.copyProperties(domicilio,domicilioTmp);
//			    				} catch (Exception e) {				
//			    					LOG.error(e.getMessage(), e);
//			    				}
//			    				
//			    				domiciliosHistoricos.add(domicilio);
//			    				persona.setDomicilios(domiciliosHistoricos);
//		    			  }		    			
//		    		  }
//		    		  else
//		    		  {
		    			  List<Domicilio> domicilios = this.findByProperty(Domicilio.class, "idPersona", persona.getIdPersona());
			    		  persona.setDomicilios(domicilios);
			    		  refresh(persona);
		    		 // }			    		  
		    	  }
//		    	  if (fechaHistorico == null || fechaHistorico.isEmpty()) 
//	    		  {	
		    		 // refresh(agente); 		    		  
	    		  //}		    	 
		    	  
		    	  if(agente.getPromotoria()==null || agente.getPromotoria().getId()==null){
	    			  agente.setPromotoria(obtenerPromotoria(agente.getId()));
		    	  }
		      }		      
		}
		return agente;
    }
	
	public Promotoria obtenerPromotoria(Long idAgente){
		Promotoria promotoria = new Promotoria();
		Map<Integer,Object>params = new HashMap<Integer, Object>();
		StringBuilder queryStr = new StringBuilder();
		queryStr.append(" select p.ID, p.EJECUTIVO_ID, p.IDPROMOTORIA, p.DESCRIPCION, p.IDTIPOPROMOTORIA, p.IDAGRUPADORPROMOTORIA, " +
				" p.CORREOELECTRONICO, p.IDDOMICILIO, p.AGENTEPROMOTOR_ID, p.CLAVEESTATUS, p.IDPERSONARESPONSABLE from MIDAS.toPromotoria p ");
		queryStr.append(" inner join MIDAS.toAgente a on a.IDPROMOTORIA = p.ID where");
		if(idAgente!= null){
			addCondition(queryStr, " a.id = ? ");
			params.put(1, idAgente);
		}
		
		 Query query = entityManager.createNativeQuery(getQueryString(queryStr), Promotoria.class);
			for (Integer key : params.keySet()) {
				query.setParameter(key, params.get(key));
			} 
			List<Promotoria>promotoriaList = query.getResultList();
			if(!promotoriaList.isEmpty()){
				promotoria = promotoriaList.get(0);
			}else{
				promotoria=null;
			}
		return promotoria;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Agente> findByFilters(Agente filtroAgente, String fechaHistorico)
	{
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;				
		ValorCatalogoAgentes tipoAgentePromotor=null;
		Map<String,Object> params=new HashMap<String, Object>();
		List<Agente> lista=new ArrayList<Agente>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from Agente model left join fetch model.persona left join fetch model.afianzadora");
		if(!isNull(filtroAgente)){
			if(filtroAgente.getPersona()!=null){
				if(filtroAgente.getPersona().getRazonSocial()!=null && !filtroAgente.getPersona().getRazonSocial().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.razonSocial) like UPPER(:razonSocial)");
					params.put("razonSocial", "%"+filtroAgente.getPersona().getRazonSocial()+"%");
				}
				//if(filtroAgente.getPersona().getRfc()!=null && !filtroAgente.getPersona().getRfc().isEmpty()){
				//addCondition(queryString, "model.persona.rfc=:rfc");
				//params.put("rfc", filtroAgente.getPersona().getRfc());
				//}
				if(filtroAgente.getPersona().getNombreCompleto()!=null && !filtroAgente.getPersona().getNombreCompleto().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.nombreCompleto) like UPPER(:nombreCompleto)");
					params.put("nombreCompleto", "%"+filtroAgente.getPersona().getNombreCompleto()+"%");
				}
			}
			
			try{
				if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
					//Limitado a los agentes de la promotoria.
					Agente agente = usuarioService.getAgenteUsuarioActual();
					Promotoria promotoria = agente.getPromotoria();
					filtroAgente.setPromotoria(promotoria);
				} 
			}catch(Exception e){
				//LOG.error(e.getMessage(), e);
			}
			
			if(filtroAgente.getPromotoria()!=null){
				if(filtroAgente.getPromotoria().getEjecutivo() != null){
					if(filtroAgente.getPromotoria().getEjecutivo().getId() != null){
						addCondition(queryString, "model.promotoria.ejecutivo.id = :idOficina");
						params.put("idOficina", filtroAgente.getPromotoria().getEjecutivo().getId());
					}					
					if(filtroAgente.getPromotoria().getEjecutivo().getGerencia() != null){
						if(filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId() != null){
							addCondition(queryString, "model.promotoria.ejecutivo.gerencia.id = :idGerencia");
							params.put("idGerencia", filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId());
						}						
					}
				}
				
				if(filtroAgente.getPromotoria().getId()!=null && !filtroAgente.getPromotoria().getId().equals("")){
					addCondition(queryString, "model.promotoria.id=:id");
					params.put("id", filtroAgente.getPromotoria().getId());
				}
			}
			//
			
			try {
				tipoAgentePromotor=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "AGENTE PROMOTOR");
				if(isNotNull(tipoAgentePromotor) && isNotNull(tipoAgentePromotor.getId())){
//					addCondition(queryString, "model.idTipoAgente not in (:idTipoAgente)");
					addCondition(queryString, " not(model.idTipoAgente in(:idTipoAgente) and model.idAgente is null)");
					params.put("idTipoAgente", tipoAgentePromotor.getId());
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			if(filtroAgente.getTipoSituacion() != null && filtroAgente.getTipoSituacion().getId()!=null){
					addCondition(queryString, "model.tipoSituacion.id=:idTipoSituacion");
					params.put("idTipoSituacion", filtroAgente.getTipoSituacion().getId());
				
			}
			if(filtroAgente.getTipoSituacion() != null && filtroAgente.getTipoSituacion().getIdRegistro()!=null){
				addCondition(queryString, "model.tipoSituacion.idRegistro=:idRegistro");
				params.put("idRegistro", filtroAgente.getTipoSituacion().getIdRegistro());
			
		}
			if(filtroAgente.getId()!=null){
				addCondition(queryString, "model.id=:idAgente");
				params.put("idAgente", filtroAgente.getId());
			}
			if(filtroAgente.getIdAgente()!=null){
				addCondition(queryString, "model.idAgente=:claveAgente");
				params.put("claveAgente", filtroAgente.getIdAgente());
			}
			
			List<Domicilio> domicilio=(filtroAgente.getPersona()!=null)?filtroAgente.getPersona().getDomicilios():null;
			if(domicilio!=null && domicilio.get(0)!=null){
				if(domicilio.get(0).getClaveEstado()!=null && !domicilio.get(0).getClaveEstado().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.claveEstado)=UPPER(:claveEstado)");
					params.put("claveEstado", domicilio.get(0).getClaveEstado());
				}
				if(domicilio.get(0).getClaveCiudad()!=null && !domicilio.get(0).getClaveCiudad().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.claveCiudad)=UPPER(:claveCiudad)");
					params.put("claveCiudad", domicilio.get(0).getClaveCiudad());
				}
				if(domicilio.get(0).getNombreColonia()!=null && !domicilio.get(0).getNombreColonia().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.nombreColonia) like UPPER(:nombreColonia)");
					params.put("nombreColonia", domicilio.get(0).getNombreColonia()+"%");
				}
				if(domicilio.get(0).getCodigoPostal()!=null && !domicilio.get(0).getCodigoPostal().isEmpty()){
					addCondition(queryString, "model.persona.domicilios.codigoPostal like :codigoPostal");
					params.put("codigoPostal", domicilio.get(0).getCodigoPostal()+"%");
				}
				if(domicilio.get(0).getCalleNumero()!=null && !domicilio.get(0).getCalleNumero().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.calleNumero) like UPPER(:calleNumero)");
					params.put("calleNumero", domicilio.get(0).getCalleNumero()+"%");
				}
			}
		}
		
		if(!isHistorico)
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			setQueryParametersByProperties(query, params);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista=query.getResultList();			
		}
		else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
//				System.out.println(fechaHistorico);
				convertedDate = dateFormat.parse(fechaHistorico);
				convertedDate.setSeconds(convertedDate.getSeconds()+1);
				LOG.debug(convertedDate);
			} catch (ParseException e) {
				LOG.error(e.getMessage(), e);
			}
			JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
			
			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString).toString());
			historicalQuery.addArgument("idTipoAgente");
			historicalQuery.addArgument("idAgente");
			historicalQuery.addArgumentValue(tipoAgentePromotor.getId());			
			historicalQuery.addArgumentValue(filtroAgente.getId());			
			historicalQuery.refreshIdentityMapResult();
			lista = (List<Agente>)historicalSession.executeQuery(historicalQuery);			
		}
//		System.out.println("tamanio de la lista "+lista.size());
		if (!lista.isEmpty()){
			if(lista.get(0).getPromotoria()==null || lista.get(0).getPromotoria().getId()==null){
				lista.get(0).setPromotoria(obtenerPromotoria(lista.get(0).getId()));
	  	  }
		}
		return lista;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Agente> findByFiltersSolicitudes(Agente filtroAgente)
	{				
		ValorCatalogoAgentes tipoAgentePromotor=null;
		Map<String,Object> params=new HashMap<String, Object>();
		List<Agente> lista=new ArrayList<Agente>(1);
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from Agente model left join fetch model.persona left join fetch model.afianzadora");
		if(!isNull(filtroAgente)){
			if(filtroAgente.getPersona()!=null){
				if(filtroAgente.getPersona().getRazonSocial()!=null && !filtroAgente.getPersona().getRazonSocial().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.razonSocial) like UPPER(:razonSocial)");
					params.put("razonSocial", "%"+filtroAgente.getPersona().getRazonSocial()+"%");
				}
				if(filtroAgente.getPersona().getNombreCompleto()!=null && !filtroAgente.getPersona().getNombreCompleto().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.nombreCompleto) like UPPER(:nombreCompleto)");
					params.put("nombreCompleto", "%"+filtroAgente.getPersona().getNombreCompleto()+"%");
				}
			}
			
			try{
				if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
					//Limitado a los agentes de la promotoria.
					Agente agente = usuarioService.getAgenteUsuarioActual();
					Promotoria promotoria = agente.getPromotoria();
					filtroAgente.setPromotoria(promotoria);
				} 
			}catch(Exception e){
				LOG.error(e.getMessage(), e);
			}
			
			if(filtroAgente.getPromotoria()!=null){
				if(filtroAgente.getPromotoria().getEjecutivo() != null){
					if(filtroAgente.getPromotoria().getEjecutivo().getId() != null){
						addCondition(queryString, "model.promotoria.ejecutivo.id = :idOficina");
						params.put("idOficina", filtroAgente.getPromotoria().getEjecutivo().getId());
					}					
					if(filtroAgente.getPromotoria().getEjecutivo().getGerencia() != null){
						if(filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId() != null){
							addCondition(queryString, "model.promotoria.ejecutivo.gerencia.id = :idGerencia");
							params.put("idGerencia", filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId());
						}						
					}
				}
				
				if(filtroAgente.getPromotoria().getId()!=null && !filtroAgente.getPromotoria().getId().equals("")){
					addCondition(queryString, "model.promotoria.id=:id");
					params.put("id", filtroAgente.getPromotoria().getId());
				}
			}
			//
			
			try {
				tipoAgentePromotor=catalogoService.obtenerElementoEspecifico("Tipo de Agente", "AGENTE PROMOTOR");
				if(isNotNull(tipoAgentePromotor) && isNotNull(tipoAgentePromotor.getId())){
//					addCondition(queryString, "model.idTipoAgente not in (:idTipoAgente)");
					addCondition(queryString, " not(model.idTipoAgente in(:idTipoAgente) and model.idAgente is null)");
					params.put("idTipoAgente", tipoAgentePromotor.getId());
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
			if(filtroAgente.getTipoSituacion() != null && filtroAgente.getTipoSituacion().getIdRegistro()!=null){
				addCondition(queryString, "model.tipoSituacion.idRegistro=:idRegistro");
				params.put("idRegistro", filtroAgente.getTipoSituacion().getIdRegistro());
				addCondition(queryString, "model.fechaVencimientoCedula >= :fechaVencimientoCedula");
				params.put("fechaVencimientoCedula", new Date());
			}
			if(filtroAgente.getId()!=null){
				addCondition(queryString, "model.id=:idAgente");
				params.put("idAgente", filtroAgente.getId());
			}
			if(filtroAgente.getIdAgente()!=null){
				addCondition(queryString, "model.idAgente=:claveAgente");
				params.put("claveAgente", filtroAgente.getIdAgente());
			}
			
			List<Domicilio> domicilio=(filtroAgente.getPersona()!=null)?filtroAgente.getPersona().getDomicilios():null;
			if(domicilio!=null && domicilio.get(0)!=null){
				if(domicilio.get(0).getClaveEstado()!=null && !domicilio.get(0).getClaveEstado().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.claveEstado)=UPPER(:claveEstado)");
					params.put("claveEstado", domicilio.get(0).getClaveEstado());
				}
				if(domicilio.get(0).getClaveCiudad()!=null && !domicilio.get(0).getClaveCiudad().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.claveCiudad)=UPPER(:claveCiudad)");
					params.put("claveCiudad", domicilio.get(0).getClaveCiudad());
				}
				if(domicilio.get(0).getNombreColonia()!=null && !domicilio.get(0).getNombreColonia().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.nombreColonia) like UPPER(:nombreColonia)");
					params.put("nombreColonia", domicilio.get(0).getNombreColonia()+"%");
				}
				if(domicilio.get(0).getCodigoPostal()!=null && !domicilio.get(0).getCodigoPostal().isEmpty()){
					addCondition(queryString, "model.persona.domicilios.codigoPostal like :codigoPostal");
					params.put("codigoPostal", domicilio.get(0).getCodigoPostal()+"%");
				}
				if(domicilio.get(0).getCalleNumero()!=null && !domicilio.get(0).getCalleNumero().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.calleNumero) like UPPER(:calleNumero)");
					params.put("calleNumero", domicilio.get(0).getCalleNumero()+"%");
				}
			}
		}
		
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		lista = query.getResultList();			
		
		return lista;		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <E extends Entidad> List<E> findByPropertyWithHistorySupport(Class<E> entityClass, String propertyName,
			final Object value, String fechaHistorico) {
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico
				.isEmpty()) ? true : false;

		List<E> lista = new ArrayList<E>();
		StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from "+entityClass.getSimpleName()+" model where model."
				+ propertyName + "= :propertyValue");

		if (!isHistorico) 
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			lista = query.getResultList();
		} 
		else 
		{
			Customizer anotation = entityClass.getAnnotation(Customizer.class);			
			if(anotation == null) {
			    
				throw new RuntimeException("La clase " + entityClass.getClass().getName() + " no cuenta con la anotacion @Customizer " +
						"ni la configuracion adecuada para soportar la funcionalidad HistoryPolicy de Eclipse Link");
			}
			
			SimpleDateFormat dateFormat = new SimpleDateFormat(
					"dd/MM/yyyy h:mm:ss.SSSSSS a");
			Date convertedDate = new Date();

			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				LOG.error(e.getMessage(), e);
			}

			JpaEntityManager jpaEntityManager = entityManager
					.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession()
					.acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession
					.acquireHistoricalSession(asOfClause);

			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString)
					.toString());
			historicalQuery.addArgument("propertyValue");
			historicalQuery.addArgumentValue(value);
			historicalQuery.refreshIdentityMapResult();
			lista = (List<E>)historicalSession.executeQuery(historicalQuery);
		}

		return lista;
	}
	
	@Override
	public Agente findByCodigoUsuario(String codigoUsuario) {
		LOG.error("Entrando a findByCodigoUsuario y buscar el agente del usuario: "+codigoUsuario);
		/*
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Agente> cq = cb.createQuery(Agente.class);
		Root<Agente> a = cq.from(Agente.class);
		cq.where(cb.equal(a.get(Agente_.codigoUsuario), codigoUsuario));
		TypedQuery<Agente> q = entityManager.createQuery(cq);
		if (q.getResultList().size() == 1) {
			return q.getResultList().get(0);
		}
		*/
		
		StringBuilder queryString = new StringBuilder("");
		Map<String,Object> params=new HashMap<String, Object>();
		
		queryString.append("SELECT  model  FROM Agente model ");
		queryString.append("WHERE TRIM(model.codigoUsuario) = TRIM(:codigoUsuario) ");
		params.put("codigoUsuario", codigoUsuario);
		queryString.append("AND model.tipoSituacion.valor = :tipoSituacion ");
		params.put("tipoSituacion", Situacion.AUTORIZADO.getValue());
		queryString.append("AND model.fechaVencimientoCedula >= :fechaVencimientoCedula ");
		params.put("fechaVencimientoCedula", new Date());
		queryString.append("ORDER BY model.id DESC ");
		
		
		TypedQuery<Agente> query = entityManager.createQuery(queryString.toString(), Agente.class);
		setQueryParametersByProperties(query, params);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		List<Agente> list = query.getResultList();			
		LOG.error(query);
		if(list != null && !list.isEmpty()){
			LOG.error("Saliendo de findByCodigoUsuario() \n Agente encontrado: "+list.get(0).getPersona().getNombreCompleto());
			return list.get(0);
		}
		LOG.error("Saliendo de findByCodigoUsuario() \n No se encontro Agente para el Usuario: "+codigoUsuario);
		return null;
	}
	
	@Override
	public DomicilioView findDomicilioAgente(Agente agente) throws Exception {
		if(agente!=null){
			StringBuilder queryString = new StringBuilder("");
			queryString.append("select dom.idDomicilio,dom.CALLENUMERO as calleNumero,dom.COLONIA as colonia,dom.CIUDAD as ciudad,dom.ESTADO as estado,dom.CODIGOPOSTAL as codigoPostal ");
			queryString.append(" from midas.toAgente agt ");
			queryString.append(" inner join MIDAS.vw_persona pers on pers.idpersona=agt.idpersona ");
			queryString.append(" inner join MIDAS.VW_DOMICILIO dom on dom.iddomicilio = pers.iddomicilio and dom.tipoDomicilio like 'PERS' ");
			queryString.append(" where agt.idAgente="+agente.getIdAgente());
			
			Query query = entityManager.createNativeQuery(getQueryString(queryString),DomicilioView.class);
			
			List<DomicilioView> list = new ArrayList<DomicilioView>();
			if(query.getResultList()!=null){
				list = query.getResultList();
			}
			return list.get(0);
		}
			return null;
	}
	
	@Override
    public Agente loadByIdImpresiones(Agente agente){
    	if(agente==null){
    		return null;
    	}
		Agente filtro=new Agente();
		filtro.setId(agente.getId());
		List<Agente> list=findByFiltersImpresiones(filtro);
		if(list!=null && !list.isEmpty()){
		      agente=list.get(0);
		      if(agente!=null){
		    	  Persona persona=agente.getPersona();
		    	  if(persona!=null && persona.getIdPersona()!=null){
		    		persona=findById(Persona.class, persona.getIdPersona());
		    		List<Domicilio> domicilios = this.findByProperty(Domicilio.class, "idPersona", persona.getIdPersona());
			    	persona.setDomicilios(domicilios);
			    	refresh(persona);			    		  
		    	  }	    	 
		      }		      
		}
		return agente;
    }
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Agente> findByFiltersImpresiones(Agente filtroAgente)
	{				
		Map<String,Object> params=new HashMap<String, Object>();
		List<Agente> lista=new ArrayList<Agente>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from Agente model left join fetch model.persona left join fetch model.afianzadora");
		if(!isNull(filtroAgente)){
			if(filtroAgente.getPersona()!=null){
				if(filtroAgente.getPersona().getRazonSocial()!=null && !filtroAgente.getPersona().getRazonSocial().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.razonSocial) like UPPER(:razonSocial)");
					params.put("razonSocial", "%"+filtroAgente.getPersona().getRazonSocial()+"%");
				}
				if(filtroAgente.getPersona().getNombreCompleto()!=null && !filtroAgente.getPersona().getNombreCompleto().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.nombreCompleto) like UPPER(:nombreCompleto)");
					params.put("nombreCompleto", "%"+filtroAgente.getPersona().getNombreCompleto()+"%");
				}
			}
			
			if(filtroAgente.getPromotoria()!=null){
				if(filtroAgente.getPromotoria().getEjecutivo() != null){
					if(filtroAgente.getPromotoria().getEjecutivo().getId() != null){
						addCondition(queryString, "model.promotoria.ejecutivo.id = :idOficina");
						params.put("idOficina", filtroAgente.getPromotoria().getEjecutivo().getId());
					}					
					if(filtroAgente.getPromotoria().getEjecutivo().getGerencia() != null){
						if(filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId() != null){
							addCondition(queryString, "model.promotoria.ejecutivo.gerencia.id = :idGerencia");
							params.put("idGerencia", filtroAgente.getPromotoria().getEjecutivo().getGerencia().getId());
						}						
					}
				}
				
				if(filtroAgente.getPromotoria().getId()!=null && !filtroAgente.getPromotoria().getId().equals("")){
					addCondition(queryString, "model.promotoria.id=:id");
					params.put("id", filtroAgente.getPromotoria().getId());
				}
			}

			if(filtroAgente.getTipoSituacion() != null && filtroAgente.getTipoSituacion().getId()!=null){
					addCondition(queryString, "model.tipoSituacion.id=:idTipoSituacion");
					params.put("idTipoSituacion", filtroAgente.getTipoSituacion().getId());
				
			}
			if(filtroAgente.getTipoSituacion() != null && filtroAgente.getTipoSituacion().getIdRegistro()!=null){
				addCondition(queryString, "model.tipoSituacion.idRegistro=:idRegistro");
				params.put("idRegistro", filtroAgente.getTipoSituacion().getIdRegistro());
			
		}
			if(filtroAgente.getId()!=null){
				addCondition(queryString, "model.id=:idAgente");
				params.put("idAgente", filtroAgente.getId());
			}
			if(filtroAgente.getIdAgente()!=null){
				addCondition(queryString, "model.idAgente=:claveAgente");
				params.put("claveAgente", filtroAgente.getIdAgente());
			}
			
			List<Domicilio> domicilio=(filtroAgente.getPersona()!=null)?filtroAgente.getPersona().getDomicilios():null;
			if(domicilio!=null && domicilio.get(0)!=null){
				if(domicilio.get(0).getClaveEstado()!=null && !domicilio.get(0).getClaveEstado().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.claveEstado)=UPPER(:claveEstado)");
					params.put("claveEstado", domicilio.get(0).getClaveEstado());
				}
				if(domicilio.get(0).getClaveCiudad()!=null && !domicilio.get(0).getClaveCiudad().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.claveCiudad)=UPPER(:claveCiudad)");
					params.put("claveCiudad", domicilio.get(0).getClaveCiudad());
				}
				if(domicilio.get(0).getNombreColonia()!=null && !domicilio.get(0).getNombreColonia().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.nombreColonia) like UPPER(:nombreColonia)");
					params.put("nombreColonia", domicilio.get(0).getNombreColonia()+"%");
				}
				if(domicilio.get(0).getCodigoPostal()!=null && !domicilio.get(0).getCodigoPostal().isEmpty()){
					addCondition(queryString, "model.persona.domicilios.codigoPostal like :codigoPostal");
					params.put("codigoPostal", domicilio.get(0).getCodigoPostal()+"%");
				}
				if(domicilio.get(0).getCalleNumero()!=null && !domicilio.get(0).getCalleNumero().isEmpty()){
					addCondition(queryString, "UPPER(model.persona.domicilios.calleNumero) like UPPER(:calleNumero)");
					params.put("calleNumero", domicilio.get(0).getCalleNumero()+"%");
				}
			}
		}

		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		lista=query.getResultList();			
		
		return lista;		
	}
	
	
	@Override
	public void rechazarAgentesVenceDiasEmision() throws Exception {
		List<GenericaAgentesView> idAgentes = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		queryString.append("select id from MIDAS.toagente where idsituacionAgente=(select id from MIDAS.tovalorcatalogoagentes where valor like '%RECHAZADO CON EMISION%') ");
		queryString.append(" and fechaAutorizacionRechazo+diasparaemitirenrechazo<=trunc(sysdate) ");
		
		Query query = entityManager.createNativeQuery(queryString.toString(), GenericaAgentesView.class);
		idAgentes= query.getResultList();
		
		if(idAgentes==null){
			onError("No hay agentes para rechazar");
		}else{
			LogDeMidasInterfaz.log("Entrando a AgenteMidas.rechazarAgentesConEmisionVencida..." + this, Level.INFO, null);
			for(GenericaAgentesView agentes:idAgentes){
				Agente agente = entidadService.findById(Agente.class, agentes.getId());				
				ValorCatalogoAgentes situacion = catalogoService.obtenerElementoEspecifico("Estatus de Agente (Situacion)", "RECHAZADO");				
				agente.setTipoSituacion(situacion);
				entidadService.save(agente);
			}
			LogDeMidasInterfaz.log("Saliendo a AgenteMidas.rechazarAgentesConEmisionVencida..." + this, Level.INFO, null);
		}
		
	}

	@Override
	public Agente getAgenteByIdSucursal(Integer idSucursal) {
		BigDecimal idAgente = null;
		Agente agente = new Agente();
		StringBuilder queryString = new StringBuilder("");
		queryString.append("select nvl(agte_id_orig,agte_id) from SEYCOS.CONV_SUCURSALES where suc_id = " + idSucursal);
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		try{
			idAgente= (BigDecimal) query.getSingleResult();
		} catch (Exception e) {				
			LOG.error(e.getMessage(), e);
			return null;
		}
		
		if (idAgente != null){
			agente.setIdAgente(idAgente.longValue());
			try {
				agente = this.findByClaveAgente(agente);
			} catch (Exception e) {				
				LOG.error(e.getMessage(), e);
			}
		}
		
		return agente;
	}
	
	

	@Override
	public boolean isSucursal(Integer idAgente) {		
		final String queryString = "select count(*) from SEYCOS.CONV_SUCURSALES suc where suc.AGTE_ID = ?1 or suc.AGTE_ID_ORIG=?1";
		Query query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, idAgente);
		String re = query.getSingleResult().toString();
		return !re.equals("0");
		}

	public void updateEstatusReporteDetPrimas(String estatus) {
		StringBuilder queryUpdateString = new StringBuilder();
		queryUpdateString.append("UPDATE MIDAS.TOPARAMETROGENERAL SET VALOR =" + estatus + " WHERE CODIGOPARAMETROGENERAL =" + 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_REPORTE_AGENTE_DETALLE_PRIMAS);
		Query queryUpdate = entityManager.createNativeQuery(queryUpdateString.toString());
		queryUpdate.executeUpdate();
	}
	
	/**
	 * Busca en el historial del agente si este estuvo alguna vez autorizado
	 * @param id Surrogate key del agente (id del sistema MIDAS)
	 * @return Numero de modificaciones del agente con estatus autorizado en su historial
	 */
	public Integer buscarAgenteAutorizadoEnHistorial(Long id) {
		
		StoredProcedureHelper storedHelper = null;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_AGENTE.buscaAgenteAutorizadoHistorico", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pSurrogateIdAgente", id);
			
			return storedHelper.ejecutaActualizar();
			
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
		
	}
	
	@Override
	public Integer buscarIdRolAgenteMidas() {
	
		return buscarIdRol("buscaIdRolAgenteMidas");
		
	}
	
	@Override
	public Integer buscarIdRolPromotorMidas() {
		
		return buscarIdRol("buscaIdRolPromotorMidas");
		
	}
	
	@Override
	public Integer buscarIdRolAgenteED() {
		
		return buscarIdRol("buscaIdRolAgenteED");
		
	}
	
	@Override
	public Integer buscarIdRolPromotorED() {
		
		return buscarIdRol("buscaIdRolPromotorED");
		
	}
		
	private Integer buscarIdRol(String spName) {
		
		StoredProcedureHelper storedHelper = null;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_AGENTE." + spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			return storedHelper.ejecutaActualizar();
			
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
		
	}
	
	
	private void validarCambioClaveAgente(Agente agente) {

		if (agente.getId() != null) {

			Agente agenteOriginal = entidadService.evictAndFindById(Agente.class, agente.getId());		

			if (agenteOriginal.getIdAgente() != null && !agenteOriginal.getIdAgente().equals(agente.getIdAgente())
					&& agenteMidasService.isAgenteAutorizadoPreviamente(agente.getId())) {
				
				throw new RuntimeException("El cambio de clave no es valido para un agente que ya ha sido autorizado previamente.");
				
			}
			
		}
		
	}
	
	
	private void validarCambioPromotoria(Agente agente) {
		
		if (agente.getId() != null) {

			Agente agenteOriginal = entidadService.evictAndFindById(Agente.class, agente.getId());		

			if (agenteOriginal.getPromotoria() != null && !agenteOriginal.getPromotoria().getId().equals(agente.getPromotoria().getId())
					&& agenteMidasService.isAgentePromotor(agente.getId())) {
				
				throw new RuntimeException("El cambio de promotoria no es valido ya que el agente es el promotor.");
				
			}
			
		}
				
	}
	
}
