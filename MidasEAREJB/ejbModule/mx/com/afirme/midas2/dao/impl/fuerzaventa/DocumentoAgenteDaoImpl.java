
package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.HashMap;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.DocumentoAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.ConfiguracionAgenteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.GuiaHonorariosAgenteView;

@Stateless
public class DocumentoAgenteDaoImpl implements DocumentoAgenteDao  {
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	private static final String IN = " in ( ";
	private static final String AND = " ) and ";
	private static final String WHERE = " where ";
	
	private enum TipoComplemento{
		CENTRO,
		EJECUTIVO,
		GERENCIA,
		PROMOTORIA,
		SITUACION,
		TIPOSAGENTE, 
		TIPOSPROMOTORIA,
		AGENTE
	}
	
	private StringBuilder queryString;
	private Map<String, Object> map = new HashMap<String, Object>();
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public List<GuiaHonorariosAgenteView> obtenerGuiasHonorarios(ConfiguracionAgenteDTO configuracion, String anioMes) {
		
		List<GuiaHonorariosAgenteView> list;
		
		List<CentroOperacion> centros = configuracion.getCentrosOperacion();
		List<Ejecutivo> ejecutivos = configuracion.getEjecutivos();
		List<Gerencia> gerencias = configuracion.getGerencias();
		List<Promotoria> promotorias  = configuracion.getPromotorias();
		List<ValorCatalogoAgentes> situaciones = configuracion.getSituaciones();
		List<ValorCatalogoAgentes> tiposAgente = configuracion.getTiposAgente();
		List<ValorCatalogoAgentes> tiposPromotoria = configuracion.getTiposPromotoria();
		List<Agente> agentes = configuracion.getAgentesEspecificos();
		
		queryString = new StringBuilder("");
		queryString.append(guiasHonorariosAgentesQuery());
		queryString.append(" where ho.anioMes = ?1 and ");
		
		if(isNotNull(configuracion)){
			complemento(centros,TipoComplemento.CENTRO);
			complemento(ejecutivos, TipoComplemento.EJECUTIVO);
			complemento(gerencias, TipoComplemento.GERENCIA);
			complemento(promotorias, TipoComplemento.PROMOTORIA);
			complemento(situaciones, TipoComplemento.SITUACION);
			complemento(tiposAgente, TipoComplemento.TIPOSAGENTE);
			complemento(tiposPromotoria, TipoComplemento.TIPOSPROMOTORIA);
			complemento(agentes, TipoComplemento.AGENTE);
		}
		
		String finalQuery = getQueryString(queryString)+" order by ho.id desc ";
		Query query = entityManager.createNativeQuery(finalQuery,GuiaHonorariosAgenteView.class);
		query.setParameter(1, anioMes);
		
		Iterator it = map.entrySet().iterator();
	    while (it.hasNext()) {
	        Map.Entry pair = (Map.Entry) it.next();
	        query.setParameter(pair.getKey().toString(), pair.getValue());
	        it.remove();
	    }
		
		list = query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public List<GuiaHonorariosAgenteView> obtenerGuiasHonorariosPorIds(List<Long> ids) {
		
		List<GuiaHonorariosAgenteView> list;
		
		StringBuilder queryStr = new StringBuilder("");
		queryStr.append(guiasHonorariosAgentesQuery());
		queryStr.append(" where ho.id IN ( ");
		
		for (int posCount = 1; posCount <= ids.size(); posCount++) {
			queryStr.append("?"+posCount + " ,");
		}
		queryStr.delete(queryStr.length()-1, queryStr.length());
		
		String finalQuery = getQueryString(queryStr) + ") ";
		Query query = entityManager.createNativeQuery(finalQuery,GuiaHonorariosAgenteView.class);
		
		int posCount = 1;
		for (Long id: ids) {
			query.setParameter(posCount, id);
			posCount++;
		}
		
		list = query.getResultList();
		return list;
	}
	
	/* COMMON METHODS */
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}else if (query.endsWith(WHERE)){
			query=query.substring(0,(query.length())-(WHERE).length());
		}
		return query;
	}
	
	private <E extends Entidad> void complemento( List <E> param, TipoComplemento tipo) {
		if (!isEmptyList(param)){
			
			boolean validacion = true;
			String identificador = "";
			
			switch (tipo){
				case CENTRO:
					queryString.append(" co.id "+IN);
					identificador = "co";
					break;
				case EJECUTIVO:
					queryString.append(" ej.id "+IN);
					identificador = "ej";
					break;
				case GERENCIA:
					queryString.append(" ger.id "+IN);
					identificador = "ger";
					break;
				case PROMOTORIA:
					queryString.append(" promo.id "+IN);
					identificador = "promo";
					break;
				case SITUACION:
					queryString.append(" estatus.id "+IN);
					identificador = "est";
					break;
				case TIPOSAGENTE:
					queryString.append(" clasificacionAgente.id "+IN);
					identificador = "clasAg";
					break;
				case TIPOSPROMOTORIA:
					queryString.append(" tipoPromotoria.id "+IN);
					identificador = "tipoPromo";
					break;
				case AGENTE:
					queryString.append(" agente.id "+IN);
					identificador = "ag";
					break;
				default:
					validacion = false;
					break;
			}
			
			if (validacion){
				int posCount = 1;
				for (E p : param) {
					if(isNotNull(p) && isNotNull(p.getKey())){
						queryString.append("?"+identificador+posCount +",");
						map.put(identificador+posCount, p.getKey());
						posCount++;
					}
				}
				queryString.delete(queryString.length()-1, queryString.length());
				queryString.append(AND);	
			}
		}
	}
	
	public String guiasHonorariosAgentesQuery() {
		StringBuilder query= new StringBuilder("");
		query.append(" select distinct ");
		query.append(" ho.id as id,");
		query.append(" agente.id as idAgente,");
		query.append(" agente.idAgente as clave,");
		query.append(" persona.nombreCompleto as nombreCompleto,");
		query.append(" promo.descripcion as nombrePromotoria,");
		query.append(" ch.num_cheque as numeroCheque, ");
		query.append(" ch.f_aplicacion as fechaAplicacion, ");
		query.append(" ho.cheque_id as solicitudChequeId ");
		query.append(" from MIDAS.TOHONORARIOSAGENTES ho ");
		query.append(" inner join MIDAS.toAgente agente on  agente.IDAGENTE =  ho.IDAGENTE ");
		query.append(" inner join MIDAS.vw_persona persona on (persona.idpersona=agente.idpersona) ");
		query.append(" inner join MIDAS.toPromotoria promo on(agente.idpromotoria=promo.id)  ");
		query.append(" inner join MIDAS.toEjecutivo ej on(ej.id=promo.ejecutivo_id)  ");
		query.append(" inner join MIDAS.vw_persona personaEj on (personaEj.idpersona=ej.idpersona) ");
		query.append(" inner join MIDAS.toGerencia ger on (ej.gerencia_id=ger.id) ");
		query.append(" inner join MIDAS.toCentroOperacion co on (ger.centroOperacion_id=co.id) ");
		query.append(" inner join MIDAS.toValorCatalogoAgentes clasificacionAgente on(clasificacionAgente.id=agente.clasificacionAgentes and agente.clasificacionAgentes  <> (select id from midas.toValorCatalogoAgentes where grupocatalogoagentes_id = (select id from midas.tcgrupocatalogoagentes where descripcion ='Clasificacion de Agente') and valor = 'ALTA POR INTERNET')) ");
		query.append(" inner join MIDAS.toValorCatalogoAgentes tipoPromotoria on (tipoPromotoria.id=promo.idTipoPromotoria) "); 
		query.append(" inner join MIDAS.toValorCatalogoAgentes estatus on (estatus.id=agente.idSituacionAgente) ");
		query.append(" inner join  SEYCOS.ING_SOLIC_CHEQUE ch on ch.ID_SOL_CHEQUE = ho.CHEQUE_ID ");
		return query.toString();
	}
}
