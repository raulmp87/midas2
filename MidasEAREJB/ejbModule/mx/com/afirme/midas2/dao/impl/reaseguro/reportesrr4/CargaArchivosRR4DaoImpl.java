package mx.com.afirme.midas2.dao.impl.reaseguro.reportesrr4;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.reaseguro.reportesrr4.CargaArchivosRR4Dao;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsf;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;

@Stateless 
public class CargaArchivosRR4DaoImpl extends EntidadDaoImpl implements CargaArchivosRR4Dao{
	 
	public void save(EsquemasDTO entity) {
		LogDeMidasEJB3.log("saving EsquemasDTO instance", Level.INFO, null);
		try {
		entityManager.persist(entity);
					LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
					LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
		    throw re;
		}
	}
	
	public int deleteEsquemas(int anio, int mes, int dia , int negocio){
		
		String queryString = "delete from EsquemasDTO model" +
		" where model.anio = :parameter" +
		" and model.mes = :parameter2" +
		" and model.dia = :parameter3" +
		" and model.cveNegocio = :parameter4" ;
		Query query = entityManager.createQuery(queryString);
		query.setParameter("parameter", anio);
		query.setParameter("parameter2", mes);
		query.setParameter("parameter3", dia);
		query.setParameter("parameter4", negocio);
		return query.executeUpdate();
	}
	
	public void save(ReaseguradorCnsfMov entity) {
		LogDeMidasEJB3.log("saving ReaseguradorCnsfMov instance", Level.INFO, null);
		try {
		entityManager.persist(entity);
					LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
					LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
		    throw re;
		}
	}
	
	public void save(ReaseguradorCnsf entity) {
		LogDeMidasEJB3.log("saving ReaseguradorCnsf instance", Level.INFO, null);
		try {
		entityManager.persist(entity);
					LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
					LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
		    throw re;
		}
	}
	
	public int deleteReaseguradorMovs(Date fechacorte){
		
		String queryString = "delete from ReaseguradorCnsfMov model" +
		" where model.fechacorte = :parameter";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("parameter", fechacorte);
		return query.executeUpdate();
	}
	
	public List<ReaseguradorCnsf> findByCNSF(String value) {
				LogDeMidasEJB3.log("finding ReaseguradorCnsf instance with property: " +  ", value: " + value, Level.INFO, null);
		try {
		final String queryString = "select model from ReaseguradorCnsf model where model.claveCnsf" 
		 						 + "= :propertyValue";
							Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				return query.getResultList();
		} catch (RuntimeException re) {
			return new ArrayList<ReaseguradorCnsf>();
		}
	}			
	
	public ReaseguradorCnsfMov findByIdReaseguradorCnsf(BigDecimal value, Date fechacorte){
		ReaseguradorCnsfMov cnsfMov = new ReaseguradorCnsfMov();
		LogDeMidasEJB3.log("finding ReaseguradorCnsfMov instance with property: " +  ", value: " + value, Level.INFO, null);
		try {
		final String queryString = "select model from ReaseguradorCnsfMov model where model.idreasegurador" 
		 						 + "= :propertyValue" +
		 						 		" and model.fechacorte < :propertyValue2" +
		 						 		" order by model.fechacorte desc";
							Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				query.setParameter("propertyValue2", fechacorte);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE)
				;
				if (query.getResultList().size() > 0){
					cnsfMov = (ReaseguradorCnsfMov)query.getResultList().get(0);
				}
				return cnsfMov;
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}
	
	public ReaseguradorCnsfMov findByCveReaseguradorCnsf(String cveCnsf, Date corte){
		ReaseguradorCnsfMov corredor = new ReaseguradorCnsfMov();
		LogDeMidasEJB3.log("finding ReaseguradorCorredorDTO instance with property: " +  ", value: " + cveCnsf, Level.INFO, null);
		
		try { 
		final String queryString = "select model from ReaseguradorCnsf model " +
							
								   "INNER JOIN ReaseguradorCnsfMov as MOVS " +
								   "on model.idCnsfReasegurador = movs.idreasegurador " +
									"where model.claveCnsf" + " ='"+cveCnsf+"' " +
									"AND FECHACORTE < :corte " +
									"order by FECHACORTE DESC";
							Query query = entityManager.createQuery(queryString);
				query.setParameter("corte", corte);
				
				if (query.getResultList().size() > 0){
					corredor = (ReaseguradorCnsfMov)query.getResultList().get(0);
				}
				
				return corredor;
		} catch (RuntimeException re) {
						LogDeMidasEJB3.log("find by property name failed " + cveCnsf, Level.SEVERE, re);
				throw re;
		}
	} 
	
	public List<BigDecimal> getMaxCER(String propertyName,
			final Object value,String propertyName2,
			final Object value2) {
		LogDeMidasEJB3.log(
				"finding EsquemasDTO instance with property: "
						+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select max(model.cer) from EsquemasDTO model where model."
					+ propertyName + "= :propertyValue and model."
					+ propertyName2 + "= :propertyValue2";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setParameter("propertyValue2", value2);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3
					.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

}
