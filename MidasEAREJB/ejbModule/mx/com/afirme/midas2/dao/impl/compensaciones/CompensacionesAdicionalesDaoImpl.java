package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CompensacionesAdicionalesDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaGerencia;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionCompensacionLista;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.NegocioVida;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dto.compensaciones.FiltroCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.Respuesta;
import mx.com.afirme.midas2.service.compensaciones.CaBancaGerenciaService;
import mx.com.afirme.midas2.service.compensaciones.negociovida.NegocioVidaService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class CompensacionesAdicionalesDaoImpl implements CompensacionesAdicionalesDao {

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private CaBancaGerenciaService caBancaGerenciaService;	

	@EJB
	private NegocioVidaService negocioVidaService;
	
	@Override
	public Respuesta guardarCompensacionAdicional(CaCompensacion compensacionAdicional) {
		// TODO Auto-generated method stub
		return null;
	}

	private Query createQuery(FiltroCompensacion filtro, String selectColumn){
		String queryString = "SELECT " + selectColumn + " FROM CaCompensacion ca ";
		String operadorQuery = " WHERE ";
		
		if (filtro.getIdCompensacion() != null && filtro.getIdCompensacion().intValue() > 0 ){
			queryString += operadorQuery + " ca.id = :pid";
			operadorQuery = " AND ";
		}
		if (filtro.getIdRamo() != null && filtro.getIdRamo().intValue() > 0 ) {
			queryString += operadorQuery + " ca.caRamo.id = :pidRamo";
			operadorQuery = " AND ";
		}
		if (filtro.getIdNegocio() != null && filtro.getIdNegocio().intValue() > 0 ) {
			queryString += operadorQuery + " ca.negocioId = :pidNegocio";
			operadorQuery = " AND ";
		}
		if (filtro.getIdCotizacion() != null && filtro.getIdCotizacion().intValue() > 0 ) {
			queryString += operadorQuery + " ca.cotizacionId = :pidCotizacion";
			operadorQuery = " AND ";
		}
		

		Query query = entityManager.createQuery(queryString, CaCompensacion.class);
		
		if (filtro.getIdCompensacion() != null && filtro.getIdCompensacion().intValue() > 0 ){
			query.setParameter("pid", filtro.getIdCompensacion());
		}
		if (filtro.getIdRamo() != null && filtro.getIdRamo().intValue() > 0 ) {
			query.setParameter("pidRamo", filtro.getIdRamo());
		}
		if (filtro.getIdNegocio() != null && filtro.getIdNegocio().intValue() > 0 ) {
			query.setParameter("pidNegocio", filtro.getIdNegocio());
		}
		if (filtro.getIdCotizacion() != null && filtro.getIdCotizacion().intValue() > 0 ) {
			query.setParameter("pidCotizacion", filtro.getIdCotizacion());
		}		

		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
	}
	
	@Override
	public Long totalCompensaciones(FiltroCompensacion filtro){
		Long totalRegistros=null;
		filtro.setPageNumber(0);
		try{
			Query query = this.createQuery(filtro, "count(ca.id)");
			totalRegistros = (Long)query.getSingleResult();
		}catch(RuntimeException re){
			totalRegistros=null;
		}
		return totalRegistros;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CaCompensacion> buscarCompensaciones(FiltroCompensacion filtro) {
		List<CaCompensacion> lista = null;
		try{
			Query query = this.createQuery(filtro, "ca");
			if (filtro.getPageNumber() > 0 ) {
				query.setFirstResult(filtro.getPageNumber());
				query.setMaxResults(filtro.getPageSize());
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		    lista = (List<CaCompensacion>) query.getResultList();
		}catch(RuntimeException re){
			
		}
		return lista;
	}
	
	private Query createQueryEntiades(FiltroCompensacion filtro, String selectColumn){
		String queryString = "select " + selectColumn + " from CaEntidadPersona ep ";
		String operadorQuery = " where ";
		
		if (filtro.getIdCompensacion() != null && filtro.getIdCompensacion().intValue() > 0 ){
			queryString += operadorQuery + " ep.caCompensacion.id = :pidCompensacion";
			operadorQuery = " and ";
		}
		if (filtro.getIdPoliza() != null && filtro.getIdPoliza().intValue() > 0 ) {
			queryString += operadorQuery + " ep.caCompensacion.polizaId = :ppolizaId";
			operadorQuery = " and ";
		}
		if (filtro.getIdCotizacion() != null && filtro.getIdCotizacion().intValue() > 0 ) {
			queryString += operadorQuery + " ep.caCompensacion.cotizacionId = :pidCotizacion";
			operadorQuery = " and ";
		}
		if (filtro.getIdNegocio() != null && filtro.getIdNegocio() > 0 ) {
			queryString += operadorQuery + " ep.caCompensacion.negocioId = :pidNegocio or ep.caCompensacion.negocioVidaId = :pidNegocio ";
			operadorQuery = " and ";
		}
		if(filtro.getIdAgente() != null && filtro.getIdAgente() > 0 ) {
			//tipoEntidadca
			queryString += operadorQuery + " ep.caTipoEntidad.valor = :ptipoentidadvalor";
			operadorQuery = " and ";
			queryString += operadorQuery + " ep.agente.id = :pidAgente";
			operadorQuery = " and ";
		}
		if(filtro.getIdPromotor() != null && filtro.getIdPromotor() > 0 ) {
			//tipoEntidadca
			queryString += operadorQuery + " ep.caTipoEntidad.valor = :ptipoentidadvalor";
			operadorQuery = " and ";
			queryString += operadorQuery + " ep.agente.promotoria.id = :pidPromotoria";
			operadorQuery = " and ";
		}		
		if(filtro.getIdProveedor() != null && filtro.getIdProveedor() > 0 ) {
			//tipoEntidadca
			queryString += operadorQuery + " ep.caTipoEntidad.valor = :ptipoentidadvalor";
			operadorQuery = " and ";
			queryString += operadorQuery + " ep.idProveedor = :pidProveedor";
			operadorQuery = " and ";
		}
		if (filtro.getIdEstatus() != null && filtro.getIdEstatus() > 0 ) {
			queryString += operadorQuery + " ep.caCompensacion.caEstatus.id = :pidEstatus";
			operadorQuery = " and ";
		}
		if (filtro.getIdRamo() != null && filtro.getIdRamo() > 0 ) {
			queryString += operadorQuery + " ep.caCompensacion.caRamo.id = :pidRamo";
			operadorQuery = " and ";
		}
		
		queryString += operadorQuery + " ep.caCompensacion.borradoLogico = " + ConstantesCompensacionesAdicionales.BORRADOLOGICONO;		
		
		Query query = entityManager.createQuery(queryString, CaEntidadPersona.class);
		
		if (filtro.getIdCompensacion() != null && filtro.getIdCompensacion().intValue() > 0 ){
			query.setParameter("pidCompensacion", filtro.getIdCompensacion());
		}
		if (filtro.getIdPoliza() != null && filtro.getIdPoliza().intValue() > 0 ) {
			query.setParameter("ppolizaId", filtro.getIdPoliza());
		}
		if (filtro.getIdCotizacion() != null && filtro.getIdCotizacion().intValue() > 0 ) {
			query.setParameter("pidCotizacion", filtro.getIdCotizacion());
		}
		if (filtro.getIdNegocio() != null && filtro.getIdNegocio() > 0 ) {
			query.setParameter("pidNegocio", filtro.getIdNegocio());
		}
		if(filtro.getIdAgente() != null && filtro.getIdAgente() > 0 ) {
			query.setParameter("ptipoentidadvalor", 1);
			query.setParameter("pidAgente", filtro.getIdAgente());
		}
		if(filtro.getIdPromotor() != null && filtro.getIdPromotor() > 0 ) {
			query.setParameter("ptipoentidadvalor", 1);
			query.setParameter("pidPromotoria", filtro.getIdPromotor());
		}
		if(filtro.getIdGerencia() != null && filtro.getIdGerencia() > 0){
			//Se Buscan las generecias que esten relacionadas a la Configuracion de Banca
			List<CaBancaGerencia> listCaBancaGerencia = caBancaGerenciaService.findByProperty(CaBancaGerenciaDaoImpl.ID_GERENCIA, filtro.getIdGerencia());
			List<Long> listConfiguracionesBanca = new ArrayList<Long>();
			for(CaBancaGerencia caBancaGerencia: listCaBancaGerencia){
				listConfiguracionesBanca.add(caBancaGerencia.getConfiguracionBancaId());
			}			
			query.setParameter("pconfiguracionBancaId", listConfiguracionesBanca);
		}
		if(filtro.getIdProveedor() != null && filtro.getIdProveedor() > 0 ) {
			query.setParameter("ptipoentidadvalor", 3);
			query.setParameter("pidProveedor", filtro.getIdProveedor());
		}
		if (filtro.getIdEstatus() != null && filtro.getIdEstatus() > 0 ) {
			query.setParameter("pidEstatus", filtro.getIdEstatus());
		}
		if (filtro.getIdRamo() != null && filtro.getIdRamo() > 0 ) {
			query.setParameter("pidRamo", filtro.getIdRamo());
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
	}
	
	private Query createQueryEntiades2(FiltroCompensacion filtro){
		String queryString = "select c.id,nvl(to_char(c.Negocio_id),'0') as idnegocio " +
		",NVL(to_char(n.DESCRIPCIONNEGOCIO),'0') as negocio " +
		",NVL(to_char(c.COTIZACION_ID),'0') as cotizacion,NVL(to_char(c.POLIZA_ID),'0') as poliza , ram.NOMBRE as ramo, est.nombre as estatus, cot.SINCOMPENSACION as sinCompensacion " +
		"from MIDAS.CA_COMPENSACION c " + 
		"left join MIDAS.TONEGOCIO n on n.idtonegocio = c.NEGOCIO_ID " +
		"inner join MIDAS.CA_ESTATUS est on est.id=c.estatus_id "+
		"inner join MIDAS.CA_RAMO ram on ram.id=c.ramo_id "+
		"inner join MIDAS.CA_ESTATUS cae on cae.ID=c.ESTATUS_ID "+
		"left join MIDAS.TOCOTIZACION cot on cot.IDTOCOTIZACION = c.COTIZACION_ID ";
		String operadorQuery = " where ";
		
		//Verificar como traer los datos de entidad persona
		
		if (filtro.getIdCompensacion() != null && filtro.getIdCompensacion().intValue() > 0 ){
			queryString += operadorQuery + " c.id = " + filtro.getIdCompensacion();
			operadorQuery = " and ";
		}
		if (filtro.getIdPoliza() != null && filtro.getIdPoliza().intValue() > 0 ) {
			queryString += operadorQuery + " c.POLIZA_ID = " + filtro.getIdPoliza();
			operadorQuery = " and ";
		}
		if (filtro.getIdCotizacion() != null && filtro.getIdCotizacion().intValue() > 0 ) {
			queryString += operadorQuery + " c.COTIZACION_ID = " + filtro.getIdCotizacion();
			operadorQuery = " and ";
		}
		if (filtro.getIdNegocio() != null && filtro.getIdNegocio() > 0 ) {
			queryString += operadorQuery + " c.NEGOCIO_ID = " + filtro.getIdNegocio();
			operadorQuery = " and ";
		}		
		if (filtro.getIdEstatus() != null && filtro.getIdEstatus() > 0 ) {
			queryString += operadorQuery + " cae.ID = " + filtro.getIdEstatus();
			operadorQuery = " and ";
		}
		if (filtro.getIdRamo() != null && filtro.getIdRamo() > 0 ) {
			queryString += operadorQuery + " ram.ID = " + filtro.getIdRamo();
			operadorQuery = " and ";
		}
		
		queryString += operadorQuery + " c.BORRADOLOGICO = 0 ";		
				
		Query query = entityManager.createNativeQuery(queryString,CaConfiguracionCompensacionLista.class);		
		
		return query;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CaConfiguracionCompensacionLista> buscarEntidadesPersona(FiltroCompensacion filtro){
		List<CaConfiguracionCompensacionLista> lista=null;
		try{			
			Query query = this.createQueryEntiades2(filtro);
			if (filtro.getPageNumber() > 0 ) {
				query.setFirstResult(filtro.getPageNumber());
				query.setMaxResults(filtro.getPageSize());
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		    lista = (List<CaConfiguracionCompensacionLista>) query.getResultList();
		    if (lista != null && !lista.isEmpty() ) {
		    	for (CaConfiguracionCompensacionLista CaEntidadPersona : lista){
		    		
		    		if(CaEntidadPersona.getCaCompensacion() != null){
		    			CaEntidadPersona.setCaCompensacion(this.setNombreNegocio(CaEntidadPersona.getCaCompensacion(), filtro));
			    		
			    		if (CaEntidadPersona.getCaCompensacion().getPolizaId() != null && CaEntidadPersona.getCaCompensacion().getPolizaId().intValue() > 0 ){
			    			PolizaDTO poliza = entityManager.find(PolizaDTO.class, CaEntidadPersona.getCaCompensacion().getPolizaId());
			    			if (poliza != null && poliza.getIdToPoliza() != null ) {
			    				CaEntidadPersona.getCaCompensacion().setNumeroPoliza(poliza.getNumeroPolizaFormateada());
			    			}
			    		}
		    		}		    				    	
		    	}
		    }    
		    
		}catch(RuntimeException re){
			throw re;
		}
		return lista;
	}
	
	private Query createQueryParametroGeneral(FiltroCompensacion filtro, String selectColumn){
		String queryString = "select " + selectColumn + " from CaParametros pg ";
		String operadorQuery = " where ";
		if (filtro.getIdCompensacion() != null && filtro.getIdCompensacion().intValue() > 0 ){
			queryString += operadorQuery + " pg.caCompensacion.id = :pidCompensacion";
			operadorQuery = " and ";
		}
		if (filtro.getIdCotizacion() != null && filtro.getIdCotizacion().compareTo(BigDecimal.ZERO) > 0 ) {
			queryString += operadorQuery + " pg.caCompensacion.cotizacionId = :pidCotizacion";
			operadorQuery = " and ";
		}
		if (filtro.getIdNegocio() != null && filtro.getIdNegocio() > 0 ) {
			queryString += operadorQuery + " pg.caCompensacion.negocioId = :pidNegocio or pg.caCompensacion.negocioVidaId = :pidNegocio";
			operadorQuery = " and ";
		}
		if (filtro.getIdEstatus() != null && filtro.getIdEstatus() > 0 ) {
			queryString += operadorQuery + " pg.caCompensacion.caEstatus.id = :pidEstatus";
			operadorQuery = " and ";
		}
		if (filtro.getIdRamo() != null && filtro.getIdRamo() > 0 ) {
			queryString += operadorQuery + " pg.caCompensacion.caRamo.id = :pidRamo";
			operadorQuery = " and ";
		}
		Query query = entityManager.createQuery(queryString, CaParametros.class);
		
		if (filtro.getIdCompensacion() != null && filtro.getIdCompensacion().intValue() > 0 ){
			query.setParameter("pidCompensacion", filtro.getIdCompensacion());
		}
		if (filtro.getIdCotizacion() != null && filtro.getIdCotizacion().compareTo(BigDecimal.ZERO) > 0 ) {
			query.setParameter("pidCotizacion", filtro.getIdCotizacion());
		}
		if (filtro.getIdNegocio() != null && filtro.getIdNegocio() > 0 ) {
			query.setParameter("pidNegocio", filtro.getIdNegocio());
		}
		if (filtro.getIdEstatus() != null && filtro.getIdEstatus() > 0 ) {
			query.setParameter("pidEstatus", filtro.getIdEstatus());
		}
		if (filtro.getIdRamo() != null && filtro.getIdRamo() > 0 ) {
			query.setParameter("pidRamo", filtro.getIdRamo());
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CaParametros> buscarParametrosGenerales(FiltroCompensacion filtro){
		List<CaParametros> lista = null;
		try{
			Query query = this.createQueryParametroGeneral(filtro, "pg");
			if (filtro.getPageNumber() > 0 ) {
				query.setFirstResult(filtro.getPageNumber());
				query.setMaxResults(filtro.getPageSize());
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		    lista = (List<CaParametros>) query.getResultList();
		    if (lista != null && lista.size() > 0 ) {
		    	for (CaParametros parametroGeneral : lista){
		    		
		    		parametroGeneral.setCaCompensacion(this.setNombreNegocio(parametroGeneral.getCaCompensacion(), filtro));
		    		
		    		if (parametroGeneral.getCaCompensacion().getPolizaId() != null && parametroGeneral.getCaCompensacion().getPolizaId().intValue() > 0 ){
		    			PolizaDTO poliza = entityManager.find(PolizaDTO.class, parametroGeneral.getCaCompensacion().getPolizaId());
		    			if (poliza != null && poliza.getIdToPoliza() != null ) {
		    			  parametroGeneral.getCaCompensacion().setNumeroPoliza(poliza.getNumeroPolizaFormateada());
		    			}
		    		}
		    		
		    	}
		    }
		}catch(RuntimeException re){
			lista = null;
		}
		return lista;
	}
	
	/**
	 * 
	 * @param caCompensacion
	 * @return
	 */
	private CaCompensacion setNombreNegocio(CaCompensacion caCompensacion, FiltroCompensacion filtro){
		if(RAMO.AUTOS.getValor().equals(caCompensacion.getCaRamo().getId()) && caCompensacion.getNegocioId() != null){
			Negocio negocio = entityManager.find(Negocio.class, caCompensacion.getNegocioId());
			if (negocio != null && negocio.getIdToNegocio() != null ){
				caCompensacion.setNombreNegocio(negocio.getDescripcionNegocio());
			}
		}else if(RAMO.VIDA.getValor().equals(caCompensacion.getCaRamo().getId()) && caCompensacion.getNegocioVidaId() != null){
			NegocioVida negocioVida = entityManager.find(NegocioVida.class, caCompensacion.getNegocioVidaId());
			if(negocioVida != null){
				caCompensacion.setNombreNegocio(negocioVida.getDescripcionnegociovida());
			}
		}
		
		if(filtro.getIdGerencia() != null && filtro.getIdGerencia() > 0 && caCompensacion.getConfiguracionBancaId() != null && caCompensacion.getConfiguracionBancaId() > 0){
		    List<GerenciaSeycos> listGerencias = this.negocioVidaService.getGerenciaVida(filtro.getIdGerencia());
		    if(!listGerencias.isEmpty()){
		    	caCompensacion.setNombreNegocio(listGerencias.get(0).getNomGerencia());
		    }
		}
		
		return caCompensacion;
	}
	
}
