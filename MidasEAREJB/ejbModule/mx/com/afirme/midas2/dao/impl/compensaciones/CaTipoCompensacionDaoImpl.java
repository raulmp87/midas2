/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoCompensacionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoCompensacionDaoImpl implements CaTipoCompensacionDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoCompensacionDaoImpl.class);
	
	public void save(CaTipoCompensacion entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }

    public void delete(CaTipoCompensacion entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoCompensacion.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }

    public CaTipoCompensacion update(CaTipoCompensacion entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoCompensacion result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoCompensacion 	::		CaTipoCompensacionDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoCompensacion findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoCompensacionDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoCompensacion instance = entityManager.find(CaTipoCompensacion.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoCompensacionDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoCompensacionDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoCompensacion> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoCompensacionDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoCompensacion model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoCompensacionDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoCompensacionDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoCompensacion> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoCompensacion> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoCompensacion> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoCompensacion> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoCompensacion> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoCompensacionDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoCompensacion model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoCompensacionDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoCompensacionDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}