package mx.com.afirme.midas2.dao.impl.siniestros.catalogo.oficina;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.catalogo.oficina.CatalogoSiniestroOficinaDao;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;



@Stateless
public class CatalogoSiniestroOficinaDaoImpl extends JpaDao<Long, Oficina> implements CatalogoSiniestroOficinaDao  {
	

	@Override
	public List<EstadoMidas> estadosDisponiblesPorPais(String paisId, Long oficinaId) {
		
		String jpql = "select m from EstadoMidas m where m.pais.id = :paisId and m.id not in ( select o.estado.id from OficinaEstado o where o.oficina.id = :oficinaId  ) ";
		
		Query query = entityManager.createQuery(jpql);
		query.setParameter("paisId", paisId);
		query.setParameter("oficinaId", oficinaId);
		List<EstadoMidas> estadosDisponibles = query.getResultList();
//		
//		TypedQuery<EstadoMidas> query = entityManager.createQuery(jpql, EstadoMidas.class);
//		query.setParameter("paisId", paisId);
//		List<EstadoMidas> estadosDisponibles = query.getResultList();
//		
		return estadosDisponibles;
	}
	
	

}
