package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza.seccion.agrupadortarifa;

import java.math.BigDecimal;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion_;

@Stateless
public class NegocioAgrupadorTarifaSeccionImpl extends JpaDao<BigDecimal, NegocioAgrupadorTarifaSeccion> implements NegocioAgrupadorTarifaSeccionDao {

	@Override
	public NegocioAgrupadorTarifaSeccion buscarPorNegocioSeccionYMoneda(
			NegocioSeccion negocioSeccion, BigDecimal idMoneda) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<NegocioAgrupadorTarifaSeccion> cq = cb.createQuery(NegocioAgrupadorTarifaSeccion.class);
		Root<NegocioAgrupadorTarifaSeccion> nats = cq.from(NegocioAgrupadorTarifaSeccion.class);
		cq.where(cb.and(cb.equal(nats.get(NegocioAgrupadorTarifaSeccion_.negocioSeccion), negocioSeccion),
					cb.equal(nats.get(NegocioAgrupadorTarifaSeccion_.idMoneda), idMoneda)));
		TypedQuery<NegocioAgrupadorTarifaSeccion> q = entityManager.createQuery(cq);
		return getSingleResult2(q);
	}


}
