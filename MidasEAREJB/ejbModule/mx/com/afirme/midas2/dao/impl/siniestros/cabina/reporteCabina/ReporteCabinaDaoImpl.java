package mx.com.afirme.midas2.dao.impl.siniestros.cabina.reporteCabina;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.annotation.FilterPersistenceAnnotation.OperationType;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.ReporteCabinaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina.CausaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.BitacoraEventoSiniestro.TipoEvento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteRoboSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.siniestros.BitacoraEventoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroDTO;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.JpaUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.CommonUtils;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ReporteCabinaDaoImpl extends JpaDao<Long, ReporteCabina>  implements ReporteCabinaDao {
	
	private static Logger LOG = Logger.getLogger(ReporteCabinaDaoImpl.class);
	
	private static final int	POSICIONES_NUMPOLIZA	= 8;
	private static final int	CANT_ELEMENTOS_NUMEROPOLIZA	= 3;
	private static final int	CANT_ELEMENTOS_NUMEROREPORTESINIESTRO	= 3;
	private static final String	FORMATO_FECHA	= "dd/MM/yyyy HH:mm";


	@EJB
	private EntidadDao entidadDao;
	
	
	@EJB
	private CotizacionEndosoService cotizacionEndosoService;
	@EJB
	private CatalogoGrupoValorService  catalogoGrupoValorService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	//@EJB
	//CotizacionEndosoDaoImpl  CotizacionDao;
	private static final String SERV_PARTICULAR = "1"; 
	private static final String SERV_PUBLICO = "2";
	private static final String SERV_AMBOS = "3"; 
	
	@Override
	public String obtenerConsecutivoReporte( ReporteCabina reporteCabina ){		
		
		Oficina oficina = entidadDao.findById(Oficina.class,reporteCabina.getOficina().getId() );
		
		/*
		StringBuilder queryString = new StringBuilder("SELECT COALESCE(MAX(model.consecutivoReporte),0) + 1 FROM ");
		queryString.append(ReporteCabina.class.getSimpleName());
		queryString.append(" model WHERE model.oficina.id = :idOficina and model.anioReporte = :anioActual");		
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("idOficina", reporteCabina.getOficina().getId());
		query.setParameter("anioActual", obtenerAnioActual());
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);
		BigDecimal consecutivo = (BigDecimal)query.getSingleResult();*/
		/*StringBuilder queryString = new StringBuilder("SELECT COALESCE(MIN(repTmp.added),1)  FROM ");
		queryString.append(" ( SELECT tmp.id, tmp.consecutivoReporte + 1 as added, tmp.claveOficina, tmp.anioReporte FROM  ");
		queryString.append(ReporteCabina.class.getSimpleName());
		queryString.append(" tmp ) repTmp   ");
		queryString.append(" LEFT JOIN  ");
		queryString.append(ReporteCabina.class.getSimpleName());
		queryString.append(" model on model.consecutivoReporte =  repTmp.added ");
		queryString.append(" WHERE repTmp.added > 0 ");
		queryString.append(" AND model.id IS NULL ");
		queryString.append(" AND repTmp.claveOficina = :claveOficina");
		queryString.append(" AND repTmp.anioReporte = :anioActual");*/
		
		
		StringBuilder queryString = new StringBuilder(" select  nvl(min (siguiente),1) siguiente from ( \n");
		queryString.append("        select added  siguiente \n");
		queryString.append("        from (select repca.id, consecutivo_reporte + 1 as added,clave_oficina,anio_reporte   \n");
		queryString.append("                        from midas.toreportecabina repca \n");
		queryString.append("                            where repca.clave_oficina = \'"+oficina.getClaveOficina()+"\' \n");
		queryString.append("                                and repca.anio_reporte = "+obtenerAnioActual()+" \n");
		queryString.append("        and repca.consecutivo_reporte > 0)  recaj \n");
		queryString.append("         minus \n");
		queryString.append("        select consecutivo_reporte from midas.toreportecabina recab \n");
		queryString.append("                        where recab.clave_oficina = \'"+oficina.getClaveOficina()+"\' \n");
		queryString.append("                            and recab.anio_reporte = "+obtenerAnioActual()+" \n");
		queryString.append("                            and recab.consecutivo_reporte > 0 \n");
		queryString.append("        )");     
		
		Query query = entityManager.createNativeQuery(queryString.toString());
//		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);
//		query.setParameter(1, reporteCabina.getClaveOficina());
//		query.setParameter(2, obtenerAnioActual());
//		query.setParameter(3, reporteCabina.getClaveOficina());
//		query.setParameter(4, obtenerAnioActual());
				
		/*Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("claveOficina", reporteCabina.getClaveOficina());
		query.setParameter("anioActual", obtenerAnioActual());
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);
		BigDecimal consecutivo = (BigDecimal)query.getSingleResult();*/
		int consecutivo = ((BigDecimal) query.getSingleResult()).intValue();
		return Integer.toString(consecutivo);
	}
	
	/*Método que regresa el incisoContinuityId a partir de un reporteCabina. Traducir un query similar para la obtención*/
	@SuppressWarnings("unchecked")
	@Override
	public Long obtenerIncisoReporteCabina(Long id){
		long idInciso =new Long(0);
//		List<ReporteCabina> reporte = new ArrayList<ReporteCabina>();
		StringBuilder queryString = new StringBuilder(				
				"  select incisoc.id from midas.toreportecabina reporte ");			
		queryString.append(" join midas.toseccionreportecabina seccion on seccion.reportecabina_id = reporte.id ");
		queryString.append(" join midas.toincisoreportecabina inciso on inciso.SECCIONREPORTE_ID = seccion.id ");
		queryString.append(" join midas.topoliza poliza on reporte.idtopoliza = poliza.idtopoliza ");
		queryString.append(" join midas.mcotizacionc cotizacionc on  poliza.idtocotizacion = cotizacionc.numero ");
		queryString.append(" join midas.mincisoc incisoc on (cotizacionc.id = incisoc.mcotizacionc_id and incisoc.numero = inciso.NUMEROINCISO) ");
		queryString.append(" where reporte.id = "+ id );				
		Query query = entityManager.createNativeQuery(queryString.toString());
		try {
			List<BigDecimal> lista= query.getResultList();
			if(!isEmptyList(lista)){
				 idInciso = (isNotNull(lista.get(0)))?lista.get(0).longValue():null;
				 return idInciso;
			}
			else return new Long(0);
		}catch (Exception e ){
			return new Long(0); 
		}
		
	}  
	
	@SuppressWarnings("unchecked")
	public List<String> getListaUsuarios(String nombre){
		StringBuilder queryString = new StringBuilder(				
				"Select USERNAME FROM MIDAS.VW_USUARIOMIDAS WHERE TRIM(NOMBRE) LIKE '%"+nombre.trim()+"%'");
		Query query = entityManager.createNativeQuery(queryString.toString());
		 query.getResultList();
		List<String> lista= query.getResultList();
		if(null!=lista && lista.isEmpty())
			return null;
		else return lista;
	}	
	public String getInCodUsuario(List<String> lista){
		if(lista.isEmpty())
			return null;
		else {
			StringBuilder ids = new StringBuilder("");
			for( String id : lista)				
				ids.append("'").append(id).append("',");
			ids.append(".");
			String idR=ids.toString();
			idR=idR.replace(",.", "");
			return idR;
		}
	}  
	
	@Override
	public List<BitacoraEventoSiniestroDTO> buscarEventos(
			BitacoraEventoSiniestroDTO filtro, Long idReporte) {
		List<String> listaUsuarios = new ArrayList<String>();
		String usuariosString = null;
		if (!StringUtil.isEmpty(filtro.getNombreUsuario())){
			listaUsuarios=this.getListaUsuarios(filtro.getNombreUsuario());
			if (null!= listaUsuarios && !listaUsuarios.isEmpty()){
				usuariosString = getInCodUsuario(listaUsuarios);				
			}else{ return null;}
		}
		List<BitacoraEventoSiniestroDTO> bitacora  = new ArrayList<BitacoraEventoSiniestroDTO>();	
		if (!StringUtil.isEmpty(filtro.getModulo()) && filtro.getModulo().equalsIgnoreCase("SI") )
			this.buscarEventosBitacora(bitacora,idReporte, filtro,usuariosString);
		/*ES EMISION*/
		else if (!StringUtil.isEmpty(filtro.getModulo()) && filtro.getModulo().equalsIgnoreCase("EM")){
			/*Emisión de Póliza*/
			if(!StringUtil.isEmpty(filtro.getEvento()) && filtro.getEvento().equalsIgnoreCase(TipoEvento.EVENTO_1.toString()))
				this.buscarEventosEmisionPoliza(bitacora,idReporte, filtro,usuariosString);
			/*Emisión de Endoso*/
			else if(!StringUtil.isEmpty(filtro.getEvento()) && filtro.getEvento().equalsIgnoreCase(TipoEvento.EVENTO_2.toString()))
				this.buscarEventosEmisionEndoso(bitacora,idReporte, filtro,listaUsuarios);
			else{
				this.buscarEventosEmisionPoliza(bitacora,idReporte, filtro,usuariosString);
				this.buscarEventosEmisionEndoso(bitacora,idReporte, filtro,listaUsuarios);
			}
		}else{
			this.buscarEventosEmisionPoliza(bitacora,idReporte, filtro,usuariosString);
			this.buscarEventosEmisionEndoso(bitacora,idReporte, filtro,listaUsuarios);
			this.buscarEventosBitacora(bitacora,idReporte, filtro,usuariosString);
			
		}
		return bitacora;
	}
	
	private List<BitacoraEventoSiniestroDTO> buscarEventosEmisionEndoso(List<BitacoraEventoSiniestroDTO> bitacora, 
											Long idReporte,BitacoraEventoSiniestroDTO filtro,List<String> listaUsuarios){
		
		
			long idInciso =this.obtenerIncisoReporteCabina(idReporte);
			if(0 ==  idInciso) 
				return bitacora;
			 List<CotizacionEndosoDTO> cotizacionList=cotizacionEndosoService.obtenerEndosoInciso(idInciso,filtro.getFechaInicio(), filtro.getFechaFin(),  listaUsuarios);
				 for( CotizacionEndosoDTO endosoDTO : cotizacionList ){
					 BitacoraEventoSiniestroDTO dto = new BitacoraEventoSiniestroDTO();
					 getEventoModuloUsuarioDescByBitacora(dto,TipoEvento.EVENTO_2.toString(),true,endosoDTO.getCodigoUsuarioCreacion(),true);
				     dto.setFechaHora(Utilerias.cadenaDeFecha(endosoDTO.getFechaInicioEndoso(),FORMATO_FECHA));
					 dto.setObservaciones(endosoDTO.getDescripcionMovimiento()  );
					 bitacora.add(dto);
					 
				 }
		return bitacora;
	}
	
	@SuppressWarnings("unchecked")
	private List<BitacoraEventoSiniestroDTO> buscarEventosEmisionPoliza(List<BitacoraEventoSiniestroDTO> bitacora, 
											Long idReporte,BitacoraEventoSiniestroDTO filtro, String codUsuarios){		
		BitacoraEventoSiniestroDTO dto =new BitacoraEventoSiniestroDTO ();			
			String modelName = "model.";
			Map<String, Object> parameters = new HashMap<String, Object>();	
			StringBuilder queryString = new StringBuilder(
					"SELECT model from " + ReporteCabina.class.getSimpleName() + " model " +
					"WHERE model.id is not null AND model.id="+idReporte);
			if(null!=filtro ){
				JpaUtil.addTruncParameter(queryString, modelName + "poliza.fechaCreacion", filtro.getFechaInicio() , "fechaInicio", parameters, JpaUtil.GREATEROREQUAL);
				JpaUtil.addTruncParameter(queryString, modelName + "poliza.fechaCreacion", filtro.getFechaFin() , "fechaFin", parameters, JpaUtil.LESSOREQUAL);
				if (!StringUtil.isEmpty(filtro.getNombreUsuario())){
					if(!StringUtils.isEmpty(codUsuarios)){
						queryString.append(" AND FUNC('trim',model.poliza.codigoUsuarioCreacion)  IN (" + codUsuarios +")");
					}else
						return null;	
				}
			}			
			List<ReporteCabina>  lst =	(List<ReporteCabina>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
			for( ReporteCabina reporte : lst ){
				if(null !=reporte.getPoliza()){	
					if(null!=reporte.getPoliza().getFechaCreacion())
						dto.setFechaHora(Utilerias.cadenaDeFecha(reporte.getPoliza().getFechaCreacion(),FORMATO_FECHA));
					getEventoModuloUsuarioDescByBitacora(dto,TipoEvento.EVENTO_1.toString(),true,reporte.getPoliza().getCodigoUsuarioCreacion(),true);
					if (!StringUtil.isEmpty(dto.getObservaciones()))
						dto.setObservaciones(dto.getObservaciones()+" " +reporte.getPoliza().getNumeroPolizaFormateada() );
					bitacora.add(dto);
				}
			}
			return bitacora;
		}
	
	@SuppressWarnings("unchecked")
	private List<BitacoraEventoSiniestroDTO> buscarEventosBitacora(List<BitacoraEventoSiniestroDTO> bitacora, 
											Long idReporte,BitacoraEventoSiniestroDTO filtro,String codUsuarios){						
		StoredProcedureHelper storedHelper = null;
		String spName = " MIDAS.PKGSIN_POLIZAS.buscarEventosSiniestros";

		try {
			String propiedades = "evento,modulo,fechaHora,observaciones,nombreUsuario";
			String columnasBaseDatos = "evento,modulo,fechaHora,observaciones,nombreUsuario";

			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(BitacoraEventoSiniestroDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			
        	storedHelper.estableceParametro("p_reporteId", idReporte);
        	storedHelper.estableceParametro("p_fechaIni", filtro.getFechaInicio());
        	storedHelper.estableceParametro("p_fechaFin", filtro.getFechaFin());
        	storedHelper.estableceParametro("p_modulo", filtro.getModulo());
        	storedHelper.estableceParametro("p_evento", filtro.getEvento()) ;
        	storedHelper.estableceParametro("p_usuario", filtro.getNombreUsuario());
        	
        	//se dejo esto porque lo ocupan ciertos metodos preexistentes
        	bitacora.addAll(storedHelper.obtieneListaResultados());
		} catch (Exception e) {
			
		}		
		return bitacora;
	}
	private BitacoraEventoSiniestroDTO getEventoModuloUsuarioDescByBitacora(BitacoraEventoSiniestroDTO dto,String idEvento, 
			boolean getNombre,String username, boolean getObservacion){
		HashMap<String,Object> propertiesEvento = new HashMap<String,Object>();
		if (null!=idEvento){
			//llena el Evento mediante CatalogoValorFijo donde codigo coincida con idEvento. el valor fijo del id = 21
			propertiesEvento.put("codigo",idEvento);
			CatValorFijo catEvento=(CatValorFijo) entidadDao.findByProperties(CatValorFijo.class, propertiesEvento).get(0);
			if (null!=catEvento && !StringUtil.isEmpty(catEvento.getDescripcion()) )
				dto.setEvento(catEvento.getDescripcion());
			
			propertiesEvento.clear();
			//llena el Modulo mediante CatalogoValorFijo donde codigo coincida con el codigoPadre del evento. el valor fijo del id = 20
			if (!StringUtil.isEmpty(catEvento.getCodigo()) ){
				propertiesEvento.put("codigo",catEvento.getCodigoPadre());
				catEvento=(CatValorFijo) entidadDao.findByProperties(CatValorFijo.class, propertiesEvento).get(0);
				if (null!=catEvento && !StringUtil.isEmpty(catEvento.getDescripcion()) )
					dto.setModulo(catEvento.getDescripcion());
			}
			propertiesEvento.clear();
			if (getObservacion && !StringUtil.isEmpty(catEvento.getCodigo())){
				
				propertiesEvento.put("codigoPadre",idEvento);
				catEvento=(CatValorFijo) entidadDao.findByProperties(CatValorFijo.class, propertiesEvento).get(0);
				if (null!=catEvento && !StringUtil.isEmpty(catEvento.getDescripcion()) )
					dto.setObservaciones(catEvento.getDescripcion());
			}
		}		
		if(getNombre){
			Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(username.trim());
			if(usuario != null){
				dto.setNombreUsuario(usuario.getNombreCompleto());
			}						
		}
		return dto;
	}
	

	private String obtenerAnioActual(){
	    String date = obtenerFechaActual();
	    return date.substring(date.length()-2, date.length());
	}

	private String obtenerFechaActual(){
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	    return sdf.format(new Date());
	}
	
	@Override
	public List<ReporteCabina> findAll() {
		return null;
	}

	@Override
	public List<ReporteCabina> findByFilters(ReporteCabina arg0) {
		return null;
	}

	@Override
	public ReporteCabina findById(Long arg0) {
		return null;
	}

	@Override
	public List<ReporteCabina> findByProperty(String arg0, Object arg1) {
		return null;
	}

	@Override
	public ReporteCabina getReference(Long arg0) {
		return null;
	}

	@Override
	public void persist(ReporteCabina arg0) {
	
	}

	@Override
	public void remove(ReporteCabina arg0) {
		
	}

	@Override
	public ReporteCabina update(ReporteCabina arg0) {
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<ReporteSiniestroDTO> buscarReporteSiniestro(ReporteSiniestroDTO filtro) {
		List<ReporteSiniestroDTO> listReporteCabina 		= new ArrayList<ReporteSiniestroDTO>(1);		
		Query query = createQuery(filtro, false);
		if(filtro.getPosStart() != null){
			query.setFirstResult(filtro.getPosStart());
		}
		if(filtro.getCount() != null){
			query.setMaxResults(filtro.getCount());
		}
		listReporteCabina = query.getResultList();
	
		return listReporteCabina;
	}
	
	@Override
	public Long contarReporteSiniestro(ReporteSiniestroDTO filtro){		
		Query query = createQuery(filtro, true);
		return ((Long)query.getSingleResult());
	}
	
	/**
	 * Se utiliza para generar un Query en base un objeto filtro de parametros
	 * @param filtro
	 * @param count
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private Query createQuery( ReporteSiniestroDTO filtro, boolean count){
			
			Query query 							= null;
			StringBuilder queryString 				= new StringBuilder();
			List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>(1);				
	
			//para obtener el total de registros (paginacion)
			if(count){
				queryString.append(" SELECT COUNT(reporteCabina.id) ");
			}else{ //busqueda de los registros
				//se requirio hacer el select de los alias para poder hacer el ordenamiento requerido de la paginacion
				queryString.append(" SELECT new mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroDTO(")
				.append(" reporteCabina.id, ")
				.append(" oficina.nombreOficina, ")
				.append(" oficina.id, ")
				.append(" reporteCabina.fechaHoraReporte, ")
				.append(" func('CONCAT', TRIM(reporteCabina.claveOficina), func('CONCAT', '-', func('CONCAT',  reporteCabina.consecutivoReporte, func('CONCAT', '-', reporteCabina.anioReporte)))),  ")
				.append(" reporteCabina.claveOficina, ")
				.append(" reporteCabina.consecutivoReporte, ")
				.append(" reporteCabina.anioReporte, ")
				.append(" reporteCabina.personaReporta, ")
				.append(" reporteCabina.personaConductor, ")
				.append(" reporteCabina.estatus, ")
				.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catEstatusReporte, reporteCabina.estatus), ")			
				.append(" (CASE WHEN poliza.idToPoliza IS NOT NULL")
				.append(" THEN func('CONCAT', TRIM(poliza.codigoProducto), func('CONCAT', TRIM(poliza.codigoTipoPoliza), func('CONCAT', '-', func('CONCAT',  func('LPAD',poliza.numeroPoliza,8,0), func('CONCAT', '-', func('LPAD',poliza.numeroRenovacion,2,0))))))  ")
				.append(" ELSE NULL")
				.append(" END), ")
				.append(" incisoReporteCabina.nombreAsegurado, ")
				.append(" incisoReporteCabina.numeroInciso, ")
				.append(" autoIncisoReporteCabina.numeroSerie, ")
				.append(" (CASE WHEN siniestro.id IS NOT NULL")
				.append(" THEN func('CONCAT', TRIM(siniestro.claveOficina), func('CONCAT', '-', func('CONCAT', siniestro.consecutivoReporte, func('CONCAT', '-', siniestro.anioReporte))))  ")
				.append(" ELSE NULL")
				.append(" END), ")
				
				.append(" siniestro.id, ")
				.append(" seccionDTO.servicioPublico, ")
				.append(" personaajustador.nombre ")
				.append(" )");			
			}
			queryString.append(" FROM ReporteCabina reporteCabina ")
			.append(" LEFT OUTER JOIN reporteCabina.poliza  poliza ")
			.append(" LEFT OUTER JOIN reporteCabina.oficina  oficina ")
			.append(" LEFT OUTER JOIN reporteCabina.seccionReporteCabina seccionReporteCabina ")
			.append(" LEFT OUTER JOIN seccionReporteCabina.incisoReporteCabina incisoReporteCabina ")
			.append(" LEFT OUTER JOIN incisoReporteCabina.autoIncisoReporteCabina autoIncisoReporteCabina ")
			.append(" LEFT OUTER JOIN seccionReporteCabina.seccionDTO seccionDTO ")
			.append(" LEFT OUTER JOIN reporteCabina.ajustador ajustador ")
			.append(" LEFT OUTER JOIN ajustador.personaMidas personaajustador ");
			if( filtro.isSiniestrado() ){
				queryString.append(" INNER JOIN reporteCabina.siniestroCabina siniestro");
			}else{
				queryString.append(" LEFT OUTER JOIN reporteCabina.siniestroCabina siniestro");
			}
			

			
			queryString.append(" WHERE reporteCabina.claveOficina is not null ");
			
			if(!count){
				Utilerias.agregaHashLista(listaParametrosValidos, "catEstatusReporte", CatGrupoFijo.TIPO_CATALOGO.ESTATUS_REPORTE_CABINA.toString());
			}
	
			if( filtro.getOficinaId() != null){
				queryString.append(" AND oficina.id = :oficinaId");		
				Utilerias.agregaHashLista(listaParametrosValidos, "oficinaId", filtro.getOficinaId());
			}
			
			if( CommonUtils.isValid(filtro.getNumeroSiniestro())){
			
				if (filtro.getNumeroSiniestro().contains("-")) {						
					String[] elementosNumeroSiniestro = filtro.getNumeroSiniestro().split("-");
					
					try{
						if(elementosNumeroSiniestro.length == CANT_ELEMENTOS_NUMEROREPORTESINIESTRO){
							SimpleDateFormat sdfCorto 	= new SimpleDateFormat("yy");
							SimpleDateFormat sdf 		= new SimpleDateFormat("yyyy");
							String claveOficinaSin      =  elementosNumeroSiniestro[0];
							String consecutivoReporteSin= elementosNumeroSiniestro[1] ;
							Date fechaAnioReporte		= sdfCorto.parse( elementosNumeroSiniestro[2]);
							String anioReporteSin		= sdf.format(fechaAnioReporte).toString() ;
							
							queryString.append(" AND siniestro.claveOficina = :claveOficinaSiniestro")
										.append(" AND siniestro.consecutivoReporte = :consecutivoReporteSiniestro")
										.append(" AND siniestro.anioReporte = :anioReporteSiniestro");
							
							Utilerias.agregaHashLista( listaParametrosValidos, "claveOficinaSiniestro",       claveOficinaSin );
							Utilerias.agregaHashLista( listaParametrosValidos, "consecutivoReporteSiniestro", consecutivoReporteSin );
							Utilerias.agregaHashLista( listaParametrosValidos, "anioReporteSiniestro",        anioReporteSin );

						}
					}catch (ParseException e) {

					}
				} else {
					queryString.append(" AND siniestro.consecutivoReporte = :consecutivoReporteSiniestro)");					
					
					Utilerias.agregaHashLista( listaParametrosValidos, "consecutivoReporteSiniestro", filtro.getNumeroSiniestro());
				}
			}
			
			
			if(CommonUtils.isValid(filtro.getNumeroReporte())){
				
				
				if (filtro.getNumeroReporte().contains("-")) {		
					String[] elementosNumeroReporte = filtro.getNumeroReporte().split("-");
					
					if( elementosNumeroReporte.length == CANT_ELEMENTOS_NUMEROREPORTESINIESTRO ){
						SimpleDateFormat sdfCorto 	= new SimpleDateFormat("yy");
						String claveOficina = elementosNumeroReporte[0] ;
						String consecutivoReporte = elementosNumeroReporte[1] ;
						String anioReporte = "";
						try {
							anioReporte = sdfCorto.format(sdfCorto.parse( elementosNumeroReporte[2])).toString();
						} catch (ParseException e) {

						}
						
						queryString.append(" AND reporteCabina.claveOficina = :claveOficina")
									.append(" AND reporteCabina.consecutivoReporte = :consecutivoReporte")
									.append(" AND reporteCabina.anioReporte = :anioReporte ");						
						
						Utilerias.agregaHashLista( listaParametrosValidos, "claveOficina",       claveOficina );
						Utilerias.agregaHashLista( listaParametrosValidos, "consecutivoReporte", consecutivoReporte );
						Utilerias.agregaHashLista( listaParametrosValidos, "anioReporte",        anioReporte );

					}
				} else {
					queryString.append(" AND reporteCabina.consecutivoReporte = :consecutivoReporte");		
					
					Utilerias.agregaHashLista( listaParametrosValidos, "consecutivoReporte", filtro.getNumeroReporte());
				}				
			}
			
			
			if( CommonUtils.isValid(filtro.getNumeroPoliza())){				
				
				if (filtro.getNumeroPoliza().contains("-")) {
					String[] elementosNumPoliza = filtro.getNumeroPoliza().split("-");
					int numeroPoliza = 0;
					int numeroRenovacion = 0;
					String codigoProducto = null;
					String codigoTipoPoliza  = null;
					
					if (elementosNumPoliza.length == CANT_ELEMENTOS_NUMEROPOLIZA) {						
						codigoProducto 		=  StringUtils.rightPad(elementosNumPoliza[0].substring(0,2), POSICIONES_NUMPOLIZA);
						codigoTipoPoliza 	= StringUtils.rightPad(elementosNumPoliza[0].substring(2), POSICIONES_NUMPOLIZA);
						try {						
							numeroPoliza 		= Integer.parseInt( elementosNumPoliza[1] );
							numeroRenovacion 	= Integer.parseInt(  elementosNumPoliza[2]);
							
						} catch (NumberFormatException e) {
							numeroPoliza = 0;
							numeroRenovacion = 0;	
						}
					}
					
					queryString.append(" AND poliza.codigoProducto = :codigoProducto ")
					.append(" AND poliza.codigoTipoPoliza = :codigoTipoPoliza ")
					.append(" AND poliza.numeroPoliza = :numeroPoliza")
					.append(" AND poliza.numeroRenovacion = :numeroRenovacion ");
		
					Utilerias.agregaHashLista(listaParametrosValidos, "codigoProducto", codigoProducto);
					Utilerias.agregaHashLista(listaParametrosValidos, "codigoTipoPoliza", codigoTipoPoliza);
					Utilerias.agregaHashLista(listaParametrosValidos, "numeroPoliza", numeroPoliza);
					Utilerias.agregaHashLista(listaParametrosValidos, "numeroRenovacion", numeroRenovacion);
					
					
				}else{					
					queryString.append(" AND poliza.numeroPoliza = :numeroPoliza");			
					
					int numeroPoliza = 0;					
					try {						
						numeroPoliza 		= Integer.parseInt(filtro.getNumeroPoliza() );						
					} catch (NumberFormatException e) {
						numeroPoliza = 0;
					}
					
					Utilerias.agregaHashLista(listaParametrosValidos, "numeroPoliza", numeroPoliza);
				}
			}
			
			if(CommonUtils.isValid(filtro.getNombrePersona())){
				queryString.append(" AND UPPER(reporteCabina.personaReporta)  LIKE '%")
							.append(filtro.getNombrePersona()).append("%'");
			}
			
			if(CommonUtils.isValid(filtro.getNombreConductor())){
				queryString.append(" AND UPPER(reporteCabina.personaConductor)  LIKE '%")
							.append(filtro.getNombreConductor()).append("%'");
			}
			
			if( filtro.getEstatus() != null) {
				queryString.append(" AND reporteCabina.estatus = :estatus ");
				Utilerias.agregaHashLista(listaParametrosValidos, "estatus", filtro.getEstatus());
			}
			
			if(CommonUtils.isNotNullNorZero(filtro.getNumeroInciso())){
				queryString.append(" AND incisoReporteCabina.numeroInciso = :numeroInciso ");
				Utilerias.agregaHashLista(listaParametrosValidos, "numeroInciso", filtro.getNumeroInciso());
			}
			
			if(CommonUtils.isValid(filtro.getNumeroSerie())){
				queryString.append(" AND UPPER(autoIncisoReporteCabina.numeroSerie) LIKE '%").append(filtro.getNumeroSerie()).append( "%'  ");
			}
			
			
			if(CommonUtils.isValid(filtro.getNombreAsegurado())){
				queryString.append(" AND UPPER(incisoReporteCabina.nombreAsegurado)  LIKE '%")
							.append(filtro.getNombreAsegurado()).append("%'");
			}
			
			
			if( filtro.getFechaIniReporte() !=null ){
				queryString.append(" AND FUNC('trunc',reporteCabina.fechaHoraReporte) >= FUNC('trunc', :fechaIniReporte) ");		
				Utilerias.agregaHashLista(listaParametrosValidos, "fechaIniReporte", filtro.getFechaIniReporte());
			}
			
			if( filtro.getFechaFinReporte() !=null ){
				queryString.append(" AND FUNC('trunc',reporteCabina.fechaHoraReporte) <= FUNC('trunc',:fechaFinReporte) ");	
				Utilerias.agregaHashLista(listaParametrosValidos, "fechaFinReporte", filtro.getFechaFinReporte());
			}
			
			if( filtro.getTipoServicio() !=null ){
				// SI TIPO SERVICIO ES 3 SE BUSCA POR AMBOS TIPOS (1,2)
				if( filtro.getTipoServicio() == 3 ){
					
					final List<Integer> tipos = new ArrayList<Integer>();
					tipos.add(1); tipos.add(2);
					
					queryString.append(" AND seccionReporteCabina.seccionDTO.servicioPublico IN :tipos ");
					Utilerias.agregaHashLista(listaParametrosValidos, "tipos", tipos);
					
				}else{
					queryString.append(" AND seccionReporteCabina.seccionDTO.servicioPublico = :tipoServicio ");
					Utilerias.agregaHashLista(listaParametrosValidos, "tipoServicio", filtro.getTipoServicio());
				}

			}
			
			//Por motivo de la paginacion y carga dinamica de dhtmlx se requiere hacer un ordenamiento manual
			if(!count){
				if(StringUtil.isEmpty(filtro.getOrderByAttribute())){
					queryString.append(" ORDER BY reporteCabina.fechaHoraReporte DESC ");
				}else{
					queryString.append(" ORDER BY ");
					String direction = filtro.getOrderByAttribute().contains("asc") ? "ASC" : "DESC";											
					if(filtro.getOrderByAttribute().contains("numSiniestroCabina")){						
						queryString.append(" CASE WHEN siniestro.id IS NOT NULL")
						.append(" THEN func('CONCAT', TRIM(siniestro.claveOficina), func('CONCAT', '-', func('CONCAT', siniestro.consecutivoReporte, func('CONCAT', '-', siniestro.anioReporte))))  ")
						.append(" ELSE NULL")
						.append(" END ")
						.append(direction);						
					}else if(filtro.getOrderByAttribute().contains("numReporteCabina")){
						queryString.append("func('CONCAT', TRIM(reporteCabina.claveOficina), func('CONCAT', '-', func('CONCAT',  reporteCabina.consecutivoReporte, func('CONCAT', '-', reporteCabina.anioReporte)))) ");
						queryString.append(direction);
					}else if(filtro.getOrderByAttribute().contains("numPoliza")){
						queryString.append(" CASE WHEN poliza.idToPoliza IS NOT NULL")
						.append(" THEN func('CONCAT', TRIM(poliza.codigoProducto), func('CONCAT', TRIM(poliza.codigoTipoPoliza), func('CONCAT', '-', func('CONCAT',  func('LPAD',poliza.numeroPoliza,8,0), func('CONCAT', '-', func('LPAD',poliza.numeroRenovacion,2,0))))))  ")
						.append(" ELSE NULL")
						.append(" END ")
						.append(direction);
					}else if(filtro.getOrderByAttribute().contains("estatus")){
						queryString.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catEstatusReporte, reporteCabina.estatus) ")
						.append(direction);
					}else{
						queryString.append(filtro.getOrderByAttribute());
					}												
				}
			}
			
			query = entityManager.createQuery(queryString.toString());
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return query;
	} 
	

	/**
	 * Obtiene Reportes de Siniestrosa de cobertura tipo Robo RT con causa de siniestro Robo Con violencia y Robo Estacionada, aunque no esten estimados.
	 * @param ReporteSiniestroRoboDTO
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ReporteSiniestroRoboDTO> buscarReportesRobo(
			ReporteSiniestroRoboDTO reporteSiniestroDTO) {
		List<ReporteSiniestroRoboDTO> listaRobo  = new ArrayList<ReporteSiniestroRoboDTO>();
		String modelName = "reportecabina.";
		String reporteRobo ="model.reporteRoboSiniestro.";
		String tipoServicio = "oficina.tipoServicio";
		Map<String, Object> parameters = new HashMap<String, Object>();	
				
		StringBuilder queryString = new StringBuilder("SELECT reportecabina from " + ReporteCabina.class.getSimpleName() + " reportecabina ");
		queryString.append(" LEFT JOIN reportecabina.seccionReporteCabina.incisoReporteCabina.coberturaReporteCabina model ON (model.claveTipoCalculo = '" + ClaveTipoCalculo.ROBO_TOTAL.getValue() + "')");
		queryString.append(" WHERE reportecabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.causaSiniestro IN ('" 
				+ CausaSiniestro.ROBO_CON_VIOLENCIA.getValue() + "','" + CausaSiniestro.ROBO_ESTACIONADO.getValue() + "') ");
		
		if(null!=reporteSiniestroDTO ){ 
			JpaUtil.addTruncParameter(queryString, modelName + "fechaOcurrido", reporteSiniestroDTO.getFechaIniOcurrido() , "fechaIniOcurrido", parameters, JpaUtil.GREATEROREQUAL);
			JpaUtil.addTruncParameter(queryString, modelName + "fechaOcurrido", reporteSiniestroDTO.getFechaFinOcurrido() , "fechaFinOcurrido", parameters, JpaUtil.LESSOREQUAL);
			JpaUtil.addTruncParameter(queryString, modelName + "fechaHoraReporte", reporteSiniestroDTO.getFechaIniReporte() , "fechaIniHoraReporte", parameters, JpaUtil.GREATEROREQUAL);
			JpaUtil.addTruncParameter(queryString, modelName + "fechaHoraReporte", reporteSiniestroDTO.getFechaFinReporte() , "fechaFinHoraReporte", parameters, JpaUtil.LESSOREQUAL);
			
			JpaUtil.addParameter(queryString, modelName + "claveOficina", reporteSiniestroDTO.getClaveOficina()  , "claveOficina", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, modelName + "consecutivoReporte", reporteSiniestroDTO.getConsecutivoReporte()  , "consecutivoReporte", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, modelName + "anioReporte", reporteSiniestroDTO.getAnioReporte()  , "anioReporte", parameters, OperationType.LIKE.toString());
			
			JpaUtil.addParameter(queryString, modelName + "siniestroCabina.claveOficina", reporteSiniestroDTO.getClaveOficinaSiniestro()  , "claveOficinaSin", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, modelName + "siniestroCabina.consecutivoReporte", reporteSiniestroDTO.getConsecutivoReporteSiniestro()  , "consecutivoReporteSin", parameters, OperationType.LIKE.toString());
			JpaUtil.addParameter(queryString, modelName + "siniestroCabina.anioReporte", reporteSiniestroDTO.getAnioReporteSiniestro()  , "anioReporteSin", parameters, OperationType.LIKE.toString());
			
			
			 if(CommonUtils.isValid(reporteSiniestroDTO.getNumPoliza())){
				String[] elementosNumPoliza = reporteSiniestroDTO.getNumPoliza().split("-");
				
				if(elementosNumPoliza.length == CANT_ELEMENTOS_NUMEROPOLIZA){
					
					String codigoProducto 		=  StringUtils.rightPad(elementosNumPoliza[0].substring(0,2), POSICIONES_NUMPOLIZA);
					String codigoTipoPoliza 	= StringUtils.rightPad(elementosNumPoliza[0].substring(2),POSICIONES_NUMPOLIZA);
					int numeroPoliza 		= Integer.parseInt( elementosNumPoliza[1] );
					int numeroRenovacion 	= Integer.parseInt(  elementosNumPoliza[2]);
					JpaUtil.addParameter(queryString, modelName + "poliza.codigoProducto", codigoProducto, "codigoProducto", parameters, JpaUtil.LIKE);
					JpaUtil.addParameter(queryString, modelName + "poliza.codigoTipoPoliza", codigoTipoPoliza, "codigoTipoPoliza", parameters, JpaUtil.LIKE);
					JpaUtil.addParameter(queryString, modelName + "poliza.numeroPoliza", numeroPoliza, "numeroPolizaX", parameters, JpaUtil.EQUAL);
					JpaUtil.addParameter(queryString, modelName + "poliza.numeroRenovacion", numeroRenovacion, "numeroRenovacion", parameters, JpaUtil.EQUAL);

				}else{
					int numeroPoliza 		= 0;
					try {
						 numeroPoliza 		= Integer.parseInt( reporteSiniestroDTO.getNumPoliza() );
					}
					catch (Exception e ){
						LOG.error(e.getMessage(), e);
					}
					JpaUtil.addParameter(queryString, modelName + "poliza.numeroPoliza",numeroPoliza , "numeroPoliza", parameters, JpaUtil.EQUAL);

				}
			}
			
			 
			JpaUtil.addParameter(queryString, modelName + "seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie", reporteSiniestroDTO.getNumSerie(), "numeroSerie", parameters, JpaUtil.EQUAL);
			if( (null!=reporteSiniestroDTO.getServParticular()&& reporteSiniestroDTO.getServParticular() )&& (null!=reporteSiniestroDTO.getServParticular()&&reporteSiniestroDTO.getServPublico())){
				JpaUtil.addParameter(queryString, modelName + tipoServicio, SERV_AMBOS, "tipoServicio1", parameters, JpaUtil.EQUAL);
			}else if(null!= reporteSiniestroDTO.getServParticular()    && reporteSiniestroDTO.getServParticular()){
				JpaUtil.addParameter(queryString, modelName + tipoServicio, SERV_PARTICULAR, "tipoServicio2", parameters, JpaUtil.EQUAL);
			}else if(null!= reporteSiniestroDTO.getServPublico() && reporteSiniestroDTO.getServPublico()){
				JpaUtil.addParameter(queryString, modelName + tipoServicio, SERV_PUBLICO, "tipoServicio3", parameters, JpaUtil.EQUAL);
			}	
			JpaUtil.addParameter(queryString, modelName + "oficina.id", reporteSiniestroDTO.getOficinaId() , "oficinaId", parameters, JpaUtil.EQUAL );
			JpaUtil.addParameter(queryString, modelName + "lugarOcurrido.estado.id", reporteSiniestroDTO.getEstado() , "estado", parameters, JpaUtil.EQUAL );
			JpaUtil.addParameter(queryString, modelName + "lugarOcurrido.ciudad.id", reporteSiniestroDTO.getMunicipio() , "ciudad", parameters, JpaUtil.EQUAL );
			
			JpaUtil.addParameter(queryString, reporteRobo + "numeroActa", reporteSiniestroDTO.getNumActa() , "numeroActa", parameters, JpaUtil.EQUAL);
			JpaUtil.addParameter(queryString, reporteRobo + "tipoRobo", reporteSiniestroDTO.getTipoRobo() , "tipoRobo", parameters, JpaUtil.EQUAL);
			JpaUtil.addParameter(queryString, reporteRobo + "estatusVehiculo", reporteSiniestroDTO.getEstatus() , "estatusVehiculo", parameters, JpaUtil.EQUAL);
		}	 
		List<ReporteCabina>  lst =	(List<ReporteCabina>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		for( ReporteCabina reporte : lst ){
			ReporteSiniestroRoboDTO dto =new ReporteSiniestroRoboDTO ();
			if (!StringUtil.isEmpty(reporteSiniestroDTO.getClaveOficina())   &&  !StringUtil.isEmpty(reporteSiniestroDTO.getConsecutivoReporte())   && !StringUtil.isEmpty(reporteSiniestroDTO.getAnioReporte()) ){
				dto.setNumReporte(reporteSiniestroDTO.getClaveOficina().trim() +"-"+ reporteSiniestroDTO.getConsecutivoReporte().trim() +"-"+ reporteSiniestroDTO.getAnioReporte()) ;

			}else{
				dto.setNumReporte(null);
			}
				
			if(!StringUtil.isEmpty(dto.getNumReporte())  &&!dto.getNumReporte().equalsIgnoreCase(reporte.getNumeroReporte())){
				continue;
			}
			dto.setNumPoliza(reporte.getPoliza().getNumeroPolizaFormateada());
			dto.setNumReporte(reporte.getNumeroReporte());
			dto.setNumSerie(reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie());
			
			
			queryString = new StringBuilder("SELECT model from " + ReporteCabina.class.getSimpleName() + " reportecabina ");
			queryString.append(" INNER JOIN reportecabina.seccionReporteCabina.incisoReporteCabina.coberturaReporteCabina model ON (model.claveTipoCalculo = '" + ClaveTipoCalculo.ROBO_TOTAL.getValue() + "')");
			parameters = new HashMap<String, Object>();
			JpaUtil.addParameter(queryString,  "reportecabina.id", reporte.getId() , "id", parameters, JpaUtil.EQUAL);
			
			List<CoberturaReporteCabina> listEstimacion =	(List<CoberturaReporteCabina>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
			if(!listEstimacion.isEmpty()){
				CoberturaReporteCabina cobertura = listEstimacion.get(0);
				dto.setIdCobertura(cobertura.getId());				
				List<ReporteRoboSiniestro> listRobos = entidadDao.findByProperty(ReporteRoboSiniestro.class, "coberturaReporteCabinaId", cobertura.getId());
		    	if(!listRobos.isEmpty()){
		    		ReporteRoboSiniestro robo=listRobos.get(0);
		    		if (null!=robo){
						dto.setNumActa(robo.getNumeroActa());
						if(null!=robo.getTipoRobo() && robo.getTipoRobo().length()>0){
							CatValorFijo catValor =catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ROBO, robo.getTipoRobo());
							dto.setTipoRobo(catValor.getDescripcion());
						}
						if(null!=robo.getEstatusVehiculo() && robo.getEstatusVehiculo().length()>0){
							CatValorFijo catValor =catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_VEHICULO, robo.getEstatusVehiculo());
							if(null != catValor){
								dto.setEstatus(catValor.getDescripcion());
							}
						}
						dto.setReporteRoboId(robo.getId());
					}
		    	}
	    	}
			SiniestroCabina siniestro=reporte.getSiniestroCabina();
			if (null!=siniestro){
				dto.setNumSiniestro(siniestro.getNumeroSiniestro());
						
			}
		    	
			dto.setReporteCabinaId(reporte.getId());
			if (null!=reporte.getOficina()){
				dto.setOficina(reporte.getOficina().getNombreOficina());
			}
			if (null!=reporte.getLugarOcurrido() && null!=reporte.getLugarOcurrido().getEstado()){
				dto.setEstado(reporte.getLugarOcurrido().getEstado().getDescripcion());
			}
			if (null!=reporte.getLugarOcurrido() && null!=reporte.getLugarOcurrido().getCiudad()){
				dto.setMunicipio(reporte.getLugarOcurrido().getCiudad().getDescripcion());
			}
			if (null!=reporte.getFechaOcurrido() ){
				String fechaO=Utilerias.cadenaDeFecha(reporte.getFechaOcurrido(),"dd/MM/yyyy");
				dto.setFechaOcurrido(fechaO );
			}
			if (null!=reporte.getFechaHoraReporte() ){
				String fechaR=Utilerias.cadenaDeFecha(reporte.getFechaHoraReporte(),FORMATO_FECHA);
				dto.setFechaReporte(fechaR );
			}				
			if (null!=reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina() && null!=reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getMarcaId()){
				MarcaVehiculoDTO marcaVehiculoDTO=entidadDao.findById(MarcaVehiculoDTO.class, reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getMarcaId());
				if(null!=marcaVehiculoDTO){
					marcaVehiculoDTO.getTipoBienAutosDTO();
					if(null!=marcaVehiculoDTO.getDescripcionMarcaVehiculo()){
						dto.setTipoVehiculo(marcaVehiculoDTO.getDescripcionMarcaVehiculo());
					}
				}
			}
			listaRobo.add(dto);
		}
		return listaRobo;
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<PersonaDireccionMidas> obtenerDireccionPersona( Long idPersona ) {
		List<PersonaDireccionMidas> direcciones = new ArrayList<PersonaDireccionMidas>();
		StringBuilder queryString = new StringBuilder();
		
		queryString.append(" SELECT pDireccion ");
		queryString.append(" FROM PersonaDireccionMidas pDireccion ");
		queryString.append(" WHERE pDireccion.persona.id = :idPersona ");

		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idPersona" , idPersona);
		
		direcciones = query.getResultList();
		
		return direcciones;
	}
	
	@Override
	public String buscarNombreAseguradoAutos(BigDecimal idToPoliza, Integer numeroInciso, Date fechaValidez){						
		StoredProcedureHelper storedHelper = null;
		String spName = " MIDAS.PKGSIN_POLIZAS.buscarNombreAseguradoAutos";
		
		String nombreAsegurado = " ";
		try {
			String propiedades = "mensaje";
			String columnasBaseDatos = "valido";

			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(MensajeDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			
        	storedHelper.estableceParametro("p_idToPoliza", idToPoliza);
        	storedHelper.estableceParametro("p_numeroInciso", numeroInciso);
        	storedHelper.estableceParametro("p_fechaValidez", fechaValidez);
        	
        	//se dejo esto porque lo ocupan ciertos metodos preexistentes
        	MensajeDTO respuesta = (MensajeDTO) storedHelper.obtieneResultadoSencillo();
        	nombreAsegurado = respuesta.getMensaje();
		} catch (Exception e) {
			
		}		
		return nombreAsegurado;
	}
}