package mx.com.afirme.midas2.dao.impl.tarifa;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.tarifa.ConfiguracionTarifaAutoDao;
import mx.com.afirme.midas2.domain.tarifa.ConfiguracionTarifaAuto;
import mx.com.afirme.midas2.domain.tarifa.ConfiguracionTarifaAutoId;
import mx.com.afirme.midas2.domain.tarifa.TarifaAuto;

@Stateless
public class ConfiguracionTarifaAutoDaoImpl extends JpaDao<ConfiguracionTarifaAutoId, ConfiguracionTarifaAuto> implements
		ConfiguracionTarifaAutoDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfiguracionTarifaAuto> getConfiguracionTarifaAuto(TarifaAuto tarifaAuto) {
		StringBuilder stm=new StringBuilder("");
		stm.append("SELECT config FROM ConfiguracionTarifaAuto config where config.id.idToRiesgo=:idToRiesgo ");
		stm.append("and config.id.idConcepto =:idConcepto ");
		stm.append("order by config.id.idDato");
		
		Query query =entityManager.createQuery(stm.toString());
		query.setParameter("idToRiesgo",tarifaAuto.getId().getIdToRiesgo());
		query.setParameter("idConcepto",tarifaAuto.getId().getIdConcepto());
		
		query.setHint(QueryHints.REFRESH, HintValues.FALSE);
		query.setHint(QueryHints.CACHE_RETRIEVE_MODE, HintValues.TRUE);
		return query.getResultList();
	}


}
