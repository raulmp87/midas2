package mx.com.afirme.midas2.dao.impl.vehiculo;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas2.dao.vehiculo.ValorComercialVehiculoDao;
import mx.com.afirme.midas2.domain.vehiculo.ValorComercialVehiculo;
import mx.com.afirme.midas2.dto.vehiculo.ValorReferenciaComercialAutoDTO;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;

@Stateless
public class ValorComercialVehiculoDaoImp implements ValorComercialVehiculoDao {

	@PersistenceContext
	private EntityManager entityManager;

	@SuppressWarnings("unchecked")
	@Override
	public List<ValorComercialVehiculo> findByFilters(
			ValorComercialVehiculo valorComercialVehiculo, boolean tipo) {
		
		Query query     = null;
		int contador    = 0;
		String sqlQuery = "";
		
		// # TRUE - RECIENTE | FALSE - TODAS
		if ( tipo ){
			sqlQuery = " SELECT " +
						" MAX(vcv.id)" +
						" ,vcv.proveedorId " +
						" ,vcv.claveAmis " +
						" ,vcv.modeloVehiculo " +
						" ,MAX(vcv.descripcionVehiculo) " +
						" ,MAX(vcv.valorComercial) " +
						" ,vcv.valorComercialAnio " +
						" ,vcv.valorComercialMes " +
						" ,MAX(vcv.estatusRegistro) " +
						" ,MAX(vcv.fechaCreacion) " +
						" ,MAX(vcv.valorComercialNuevo) " +
						" FROM ValorComercialVehiculo vcv ";
		}else{
			sqlQuery = " SELECT vcv FROM ValorComercialVehiculo vcv ";
		}
		
		if ( valorComercialVehiculo.getClaveAmis() != null & !valorComercialVehiculo.getClaveAmis().isEmpty() & !valorComercialVehiculo.getClaveAmis().equals("") ){
			sqlQuery +=validateWhereOrAnd(contador) +" vcv.claveAmis = :claveAmis ";
			contador++;
		}
		
		if ( valorComercialVehiculo.getDescripcionVehiculo() != null & !StringUtils.isBlank(valorComercialVehiculo.getDescripcionVehiculo()) ){
			sqlQuery +=validateWhereOrAnd(contador) +" UPPER(vcv.descripcionVehiculo) like '%"+valorComercialVehiculo.getDescripcionVehiculo()+"%' ";
			contador++;
		}
		
		if ( valorComercialVehiculo.getValorComercialAnio() != 0 ){
			sqlQuery +=validateWhereOrAnd(contador) +" vcv.valorComercialAnio = :valorComercialAnio ";
			contador++;
		}
		
		if ( valorComercialVehiculo.getValorComercialMes() != 0 ){
			sqlQuery +=validateWhereOrAnd(contador) +" vcv.valorComercialMes = :valorComercialMes ";
			contador++;
		}
		
		if ( valorComercialVehiculo.getModeloVehiculo() != 0 ){
			sqlQuery +=validateWhereOrAnd(contador) +" vcv.modeloVehiculo = :valorModeloVehiculo ";
			contador++;
		}
		
		// # TRUE - RECIENTE | FALSE - TODAS
		if ( tipo ){
			sqlQuery +=" GROUP BY vcv.proveedorId,vcv.claveAmis,vcv.modeloVehiculo, vcv.valorComercialAnio, vcv.valorComercialMes ";
		}else{
			sqlQuery +=" ORDER BY vcv.fechaCreacion DESC ";
		}
		
		
		query = this.entityManager.createQuery(sqlQuery);
		
		if ( valorComercialVehiculo.getClaveAmis() != null & !valorComercialVehiculo.getClaveAmis().isEmpty() ){
			query.setParameter("claveAmis", valorComercialVehiculo.getClaveAmis() );
		}
		
		if ( valorComercialVehiculo.getValorComercialAnio() != 0 ){
			query.setParameter("valorComercialAnio", valorComercialVehiculo.getValorComercialAnio() );
		}
		
		if ( valorComercialVehiculo.getValorComercialMes() != 0 ){
			query.setParameter("valorComercialMes", valorComercialVehiculo.getValorComercialMes() );
		}
		
		if ( valorComercialVehiculo.getModeloVehiculo() != 0 ){
			query.setParameter("valorModeloVehiculo", valorComercialVehiculo.getModeloVehiculo() );
		}
		
		
		// # TRUE - RECIENTE | FALSE - TODAS
		if ( tipo ){
			
				List<ValorComercialVehiculo> lValorComercialVehiculo = new ArrayList<ValorComercialVehiculo>();
				List<Object[]> lValorComercial = query.getResultList();
				
				for(Object[] lvc : lValorComercial  ){
					ValorComercialVehiculo valData = new ValorComercialVehiculo();
					
					valData.setId                 ( (Long) lvc[0] );
					valData.setProveedorId        ( (Long) lvc[1] );
					valData.setClaveAmis          ( lvc[2].toString() ) ;
					valData.setModeloVehiculo     ( Integer.parseInt(lvc[3].toString())  );
					valData.setDescripcionVehiculo( lvc[4].toString() );
					valData.setValorComercial     ( (Double) lvc[5] );
					valData.setValorComercialAnio ( Integer.parseInt( lvc[6].toString() ));
					valData.setValorComercialMes  ( Integer.parseInt( lvc[7].toString() ) );
					valData.setEstatusRegistro    ( (Short) lvc[8]);
					valData.setFechaCreacion      ( (Date) lvc[9] );
					valData.setValorComercialNuevo( (Double) lvc[10]  );
					
					lValorComercialVehiculo.add(valData);
				}
				return lValorComercialVehiculo;
		}else{
			return 	(List<ValorComercialVehiculo>) query.getResultList();
		}
		
	}
	
	@Override
	public List<Integer> obtenerAnioValorComercial() {
		
		Query query = null;
		
		// # OBTENER ULTIMOS 5 AÑOS REGISTRADOS EN LA BD
		String sQuery = " SELECT valComVehiculo.valorComercialAnio  " +
						" FROM   ValorComercialVehiculo valComVehiculo" +
						" GROUP BY valComVehiculo.valorComercialAnio ORDER BY  valComVehiculo.valorComercialAnio DESC ";

		
		query = this.entityManager.createQuery(sQuery).setMaxResults(5);
		
		List<Integer> lValorComercialVehiculo =  query.getResultList();
		
		return lValorComercialVehiculo;
	}
	
	public List<Integer> obtenerAnioModelo() {
		
		Query query = null;
		
		// # OBTENER TODOS LOS AÑOS REGISTRADOS DE LOS MODELOS
		String sQuery = " SELECT valComVehiculo.modeloVehiculo  " +
						" FROM   ValorComercialVehiculo valComVehiculo" +
						" GROUP BY valComVehiculo.modeloVehiculo ORDER BY  valComVehiculo.modeloVehiculo DESC ";

		
		query = this.entityManager.createQuery(sQuery);
		
		List<Integer> lValorComercialVehiculo =  query.getResultList();
		
		return lValorComercialVehiculo;
	}	
	
	
	private String validateWhereOrAnd(int contador){
		String resultado = "";
		if(contador==0){
			resultado = " WHERE ";
		}else{
			resultado = " AND ";
		}
		
		return resultado;
	}

	
	
	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.vehiculo.ValorComercialVehiculoDao#getValorComercial(java.lang.Long, short, java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<ValorReferenciaComercialAutoDTO> getValorComercial(Long claveAmis, short modelo, Date fechaConsulta) {

		 String spName 										= "MIDAS.PKGSIN_SERVICIOS.SP_VAL_REF_COMERCIALAUTO"; 
		 List<ValorReferenciaComercialAutoDTO> resultado 	= null;
         StoredProcedureHelper storedHelper 				= null;
         String propiedades 								= "";
         String columnasBaseDatos 							= "";
         
          
         try {
        	 
       	  	propiedades = "idProveedor,claveAmis,modeloVehiculo,valorComercial,anio,mes";
       	  	columnasBaseDatos = "PROVEEDOR_ID,CLAVE_AMIS,MODELOVEHICULO,VALORCOMERCIAL,VALORCOMERCIALANIO,VALORCOMERCIALMES";
       	  	storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
          	storedHelper.estableceMapeoResultados(ValorReferenciaComercialAutoDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
             
             storedHelper.estableceParametro("P_ClaveAmis", claveAmis);
             storedHelper.estableceParametro("P_Modelo", modelo);
             storedHelper.estableceParametro("P_fechaConsulta", fechaConsulta);
             
             resultado = storedHelper.obtieneListaResultados();
            
         } catch (Exception e) {
              if (storedHelper != null) {
                    LogDeMidasInterfaz.log("Excepcion general en getValorComercial..."+ storedHelper.getDescripcionRespuesta() , Level.WARNING, e);    
              }
         }
         
		return resultado;
	}

}
