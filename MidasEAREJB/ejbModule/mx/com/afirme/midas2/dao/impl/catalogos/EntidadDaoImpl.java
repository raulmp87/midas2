package mx.com.afirme.midas2.dao.impl.catalogos;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.Type;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.Transient;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.util.JpaUtil;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class EntidadDaoImpl implements EntidadDao {
	@PersistenceContext
	protected EntityManager entityManager;
	
	@EJB
	protected EntidadHistoricoDao entidadHistoricoDao;
	
	
	private final String insertedAction = "inserted";
	private final String updatedAction = "updated";
	private final String deletedAction = "deleted";

	
	
	@Override
	public <E extends Entidad> void persist(E entity) {
		entityManager.persist(entity);				
	}

	@Override
	public <E extends Entidad> E update(E entity) {		
		return entityManager.merge(entity);
	}

	@Override
	public <E extends Entidad> void remove(E entity) {
		entity = entityManager.merge(entity);
		entityManager.remove(entity);
	}
	
	@Override
	public <E extends Entidad> void detach(E entity) {
		entityManager.detach(entity);
	}

	@Override
	public <E extends Entidad, K> E findById(Class<E> entityClass, K key) {
		return (E) entityManager.find(entityClass, key);
	}
	
	@Override
	public <E extends Entidad, K> E evictAndFindById(Class<E> entityClass, K id) {
		entityManager.getEntityManagerFactory().getCache().evict(entityClass, id);		
		return findById(entityClass, id);
	}

	@Override
	public <E extends Entidad> List<E> findAll(Class<E> entityClass) {
		CriteriaQuery<E> criteriaQuery = entityManager.getCriteriaBuilder()
				.createQuery(entityClass);
		criteriaQuery.from(entityClass);
		TypedQuery<E> query = entityManager.createQuery(criteriaQuery);
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}
	
	@Override
	public <E extends Entidad> List<E> findAll(Class<E> entityClass, int maxrows){
		CriteriaQuery<E> criteriaQuery = entityManager.getCriteriaBuilder()
				.createQuery(entityClass);
		criteriaQuery.from(entityClass);
		TypedQuery<E> query = entityManager.createQuery(criteriaQuery);
		query.setMaxResults(maxrows);
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}

	@Override
	public <E extends Entidad, K> E getReference(Class<E> entityClass, K key) {
		return (E) entityManager.getReference(entityClass, key);
	}

	@Override
	public Object executeNativeQuerySimpleResult(String query) {
		Query nativeQuery = entityManager.createNativeQuery(query);
		return nativeQuery.getSingleResult();
	}

	@Override
	public Object executeQuerySimpleResult(String query,
			Map<String, Object> parameters) {
		try{
			Query entityQuery = entityManager.createQuery(query);
			this.setQueryParameters(entityQuery, parameters);
			return entityQuery.getSingleResult();
		}
		catch(NoResultException e){
			e.printStackTrace();
			return null;
		}
	}
	
	private void setQueryParameters(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			entityQuery.setParameter(pairs.getKey(), pairs.getValue());
		}		
	}
	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	/**
	 * No setea los parametros nulos
	 * @param entityQuery
	 * @param parameters
	 */
	private void setQueryParametersByPropertiesNotNull(Query entityQuery, Map<String, Object> parameters){
        this.setQueryParametersByPropertiesNotNull(entityQuery, parameters, false);
    }

	private void setQueryParametersByPropertiesNotNull(Query entityQuery, Map<String, Object> parameters, boolean casoEspecial){
        Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
        while(it.hasNext()){
            Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
            if (casoEspecial && pairs.getValue() != null && !pairs.getValue().equals("")) {
                    if(!(pairs.getValue() instanceof String) && !(pairs.getValue() instanceof Date)) {
                        String key=getValidProperty(pairs.getKey());
                        entityQuery.setParameter(key, pairs.getValue());
                    }
              }else{
            	  if (pairs.getValue() != null && !pairs.getValue().equals("")){
            		  String key=getValidProperty(pairs.getKey());
                      entityQuery.setParameter(key, pairs.getValue());
            	  }
            	
              }
        }
    }


	@SuppressWarnings({ "rawtypes" })
	@Override
	public List executeNativeQueryMultipleResult(String query) {
		Query nativeQuery = entityManager.createNativeQuery(query);
		return nativeQuery.getResultList();
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Override
	public List executeNativeQueryMultipleResult(String query,
			Map<String, Object> parameters) {
		Query nativeQuery = entityManager.createNativeQuery(query);
		for(String key:parameters.keySet()){
			nativeQuery.setParameter(key,parameters.get(key));
		}
		return nativeQuery.getResultList();
	}
	@SuppressWarnings({ "rawtypes" })
	@Override
	public List executeQueryMultipleResult(String query,
			Map<String, Object> parameters) {
		Query entityQuery = entityManager.createQuery(query);
		if (parameters != null)
			this.setQueryParameters(entityQuery, parameters);
		
		return entityQuery.getResultList();
	}
	
	@SuppressWarnings({ "rawtypes" })
	@Override
	public List executeQueryMultipleResultWithRefresh(String query,
			Map<String, Object> parameters) {
		Query entityQuery = entityManager.createQuery(query);
		entityQuery.setHint(QueryHints.REFRESH, HintValues.TRUE);
		if (parameters != null)
			this.setQueryParameters(entityQuery, parameters);
		
		return entityQuery.getResultList();
	}
	
	
	@Override
	public <E extends Entidad> void executeActionGrid(String accion,
			E entity) {
		if (accion.equals(insertedAction)) {
			this.persist(entity);
		}else if (accion.equals(deletedAction)) {
			this.remove(entity);
		}else if (accion.equals(updatedAction)) {
			this.update(entity);
		}
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E extends Entidad> List<E> findByProperty(Class<E> entityClass,
			String propertyName, Object value) {
		final String queryString = "select model from "+entityClass.getSimpleName()+" model where model."
			+ propertyName + "= :propertyValue";
	Query query = entityManager.createQuery(queryString);
	query.setParameter("propertyValue", value);
	query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
	return query.getResultList();
	}

	@Override
	public <E extends Entidad> Object persistAndReturnId(E entity) {
		persist(entity);
		return entityManager.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);		
	}
	
	/**
	 * Busca un objeto de una entidad por medio de sus atributos del modelo, atributo no encontrado en el modelo marcara una excepcion de JPA
	 * @return losta de objetos como resultado de la consulta sobre el modelo especificado en entityClass 
	 */
	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> entityClass,Map<String, Object> params) {
		final StringBuilder queryString = new StringBuilder("select model from "+entityClass.getSimpleName()+" model ");
		boolean setParameters=false;
		if(params!=null && !params.isEmpty()){
			setParameters=true;
			queryString.append(" where ");
			int size=params.size();
			int index=0;
			for(String property:params.keySet()){
				String propertyMap=getValidProperty(property);//checa si tiene "." la propiedad
				
				Object value = params.get(property);
				if (value != null) {
					queryString.append("model."+property+"=:"+propertyMap);
				} else {
					queryString.append("model."+property+" IS NULL");
				}
				
				if(index!=(size-1)){
					queryString.append(" and ");
				}
				index++;
			}
		}
		Query query = entityManager.createQuery(queryString.toString());
		if(setParameters){
			setQueryParametersByPropertiesNotNull(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	/**
	 * Obtiene el total de elementos
	 * encontrados en la busqueda
	 * @author martin
	 */
	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> entityClass,Map<String, Object> params) {
		final StringBuilder queryString = new StringBuilder("SELECT count(model) FROM "+entityClass.getSimpleName()+" model ");
		boolean setParameters=false;
		if(params!=null && !params.isEmpty()){
			setParameters=true;
			queryString.append(" WHERE ");
			int size=params.size();
			int index=0;
			for(String property:params.keySet()){
				String propertyMap=getValidProperty(property);//checa si tiene "." la propiedad
				queryString.append("model."+property+"=:"+propertyMap);
				if(index!=(size-1)){
					queryString.append(" AND ");
				}
				index++;
			}
		}
		Query query = entityManager.createQuery(queryString.toString());
		if(setParameters){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return (Long)query.getSingleResult();
	}
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> entityClass,Map<String, Object> params,String...orderByAttributes) {
		final StringBuilder queryString = new StringBuilder("select model from "+entityClass.getSimpleName()+" model ");
		boolean setParameters=false;
		int size=0;
		int index=0;
		if(params!=null && !params.isEmpty()){
			setParameters=true;
			queryString.append(" where ");
			size=params.size();
			for(String property:params.keySet()){
				String propertyMap=getValidProperty(property);//checa si tiene "." la propiedad
				queryString.append("model."+property+"=:"+propertyMap);
				if(index!=(size-1)){
					queryString.append(" and ");
				}
				index++;
			}
		}
		if(orderByAttributes!=null && orderByAttributes.length>0){
			size=orderByAttributes.length;
			index=0;
			queryString.append(" ORDER BY ");
			for(String orderAttribute:orderByAttributes){
				queryString.append("model."+orderAttribute);
				if(index!=(size-1)){
					queryString.append(",");
				}
				index++;
			}
			//queryString.append(" DESC");
		}
		Query query = entityManager.createQuery(queryString.toString());
		if(setParameters){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	public static String getValidProperty(String property){
		String validProperty=property;
		StringBuilder str=new StringBuilder("");
		if(property.contains(".")){
			int i=0;
			for(String word:property.split("\\.")){
				if(i==0){
					word=word.toLowerCase();
				}else{
					word=StringUtils.capitalize(word);
				}
				str.append(word);
				i++;
			}
			validProperty=str.toString();
		}
		return validProperty;
	}
	

	@Override
	public <E extends Entidad> void refresh(E entity) {
		entityManager.refresh(entity);		
	}

	@Override
	public void updateWithQuery(String query, Map<String, Object> parameters) {
		try{
			Query entityQuery = entityManager.createQuery(query);
			this.setQueryParameters(entityQuery, parameters);
			entityQuery.executeUpdate();
		}
		catch(NoResultException e){
			e.printStackTrace();			
		}		
	}

	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> entityClass,Map<String, Object> params,
			Map<String,Object> queryParams,StringBuilder queryWhere,StringBuilder queryOrder) {
		final StringBuilder queryString = new StringBuilder("SELECT COUNT(model)");
		boolean setParameters = false;
		// Add From
		queryString.append(" FROM " + entityClass.getSimpleName()+ " model");
		// Adds Where
		if (params != null && !params.isEmpty()) { 
			setParameters = makeWhere(params, setParameters, queryString,queryWhere);
		}else{
			setParameters = makeWhere(null, setParameters, queryString,queryWhere);
		}		
		// Add Order By
		if(queryOrder!=null && StringUtils.isNotBlank(queryOrder.toString())) {
			queryString.append(" " + queryOrder);
		}
		
		Query query = entityManager.createQuery(queryString.toString(),entityClass.getClass());
		
		if(setParameters && params != null){
			setQueryParametersByPropertiesNotNull(query, params, true);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		try {
			return (Long) query.getSingleResult();
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> entityClass,String columns,Map<String, Object> params,
			Map<String,Object> queryParams,StringBuilder queryWhere,StringBuilder queryOrder) {
		final StringBuilder queryString = new StringBuilder("SELECT ");
		boolean setParameters = false;
		// Adds Columns
		final String[] arrayC = makeColumns(columns, queryString);
		// Add From
		queryString.append(" FROM " + entityClass.getSimpleName()+ " model");
		// Adds Where
		if (params != null && !params.isEmpty()) { 
			setParameters = makeWhere(params, setParameters, queryString,queryWhere);
		}else{
			setParameters = makeWhere(null, setParameters, queryString,queryWhere);
		}		
		// Add Order By
		if(queryOrder!=null && StringUtils.isNotBlank(queryOrder.toString())) {
			queryString.append(" " + queryOrder);
		}
		
		Query query = entityManager.createQuery(queryString.toString(),entityClass.getClass());
		// Add Paginate
		if (queryParams != null && !queryParams.isEmpty()) {
			if (queryParams.get("firstResult") == null) {
				query.setFirstResult(0);
			}else{
				query.setFirstResult((Integer)queryParams.get("firstResult"));
			}
			if(queryParams.get("maxResults") == null || (Integer)queryParams.get("maxResults") == 0 ){
				query.setMaxResults(20);
			} else{
				query.setMaxResults((Integer)queryParams.get("maxResults"));
			}
		}
		if(setParameters && params != null){
			setQueryParametersByPropertiesNotNull(query, params,true);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		List<Object> resultList = query.getResultList(); 
		try {
			return makeResult(resultList, entityClass, arrayC);
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (NoSuchFieldException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InstantiationException e) {
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * Contruye el select
	 * @param columns
	 * @param queryString
	 * @return queryString con las columnas a seleccionar
	 * @author martin
	 */
	private String[] makeColumns(String columns,StringBuilder queryString) {
		String[] arrayC = columns.split(",");
		if (columns != null && !columns.isEmpty()) {
			for (int x = 0; x < arrayC.length; x++) {
				queryString.append("model." + arrayC[x] + ",");
			}
			queryString.delete(queryString.length()-1,queryString.length());
		}else{
			throw new RuntimeException("No hay columnas para seleccionar");
		}
		return arrayC;
	}
	/**
	 * Contruye el Where
	 * @param params
	 * @param setParameters
	 * @param queryString
	 * @return queryString con los filtros ha aplicar
	 * @author martin
	 */
	private boolean makeWhere(Map<String, Object> params,boolean setParameters,StringBuilder queryString,StringBuilder queryWhere){
		if(params!=null && !params.isEmpty()){
			Map<String, Object> paramsNotNull = new HashMap<String, Object>();
			setParameters=true;
			queryString.append(" WHERE ");
			int index=0;
			// Remueve todos los parametros nulos o vacios
			for (String key : params.keySet()) {
				if(params.get(key) != null && !params.get(key).equals("")) {
					paramsNotNull.put(key, params.get(key));
				}
			}
			int size=paramsNotNull.size();
			for(String property:paramsNotNull.keySet()){
				if(paramsNotNull.get(property) != null && !paramsNotNull.get(property).equals("")) {
					String propertyMap=getValidProperty(property);//checa si tiene "." la propiedad
					if(paramsNotNull.get(property) instanceof String){
						queryString.append("model."+property+" LIKE '%"+paramsNotNull.get(property) +"%'");
						if(index!=(size-1)){
							queryString.append(" AND ");
						}
						index++;
						continue;
					} 
					if(paramsNotNull.get(property) instanceof Date) {
						String operator = "";
						if (!property.contains("<") || !property.contains(">") || !property.contains(">=")
								|| !property.contains("<=")
								|| !property.contains("<=")
								|| !property.contains("<>")){
							operator = "=";
						}
							queryString
									.append("model."
											+ property + operator
											+ "'"
											+ Utilerias.cadenaDeFecha(
													(Date) paramsNotNull.get(property),
													SistemaPersistencia.FORMATO_FECHA_ORACLE)
											+ "'");
						
						
						if(index!=(size-1)){
							queryString.append(" AND ");
						}
						index++;
						continue;
					}
					queryString.append("model."+property+"=:"+propertyMap);
					
					if(index!=(size-1)){
						queryString.append(" AND ");
					}
					index++;
				}
			}
			if (queryWhere != null && StringUtils.isNotBlank(queryWhere.toString())){
				queryString.append(" " + queryWhere);
		}
		} else {
			if (StringUtils.isNotBlank(queryWhere.toString())) {
				setParameters=true;
				queryString.append(" WHERE ");
				queryString.append(" " + queryWhere);
			}
		}
		return setParameters;
	}
	/**
	 * Crea el Where apartir de un objeto
	 * y sus valores
	 * @param <E>
	 * @param param
	 * @param params
	 * @param setParameters
	 * @param queryString
	 * @return
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws InvocationTargetException
	 * @throws ClassNotFoundException
	 * @autor martin
	 */
	private <E extends Entidad> boolean makeWhere(Object param,Map<String,Object> params,boolean setParameters,StringBuilder queryString,StringBuilder queryWhere) throws IllegalAccessException, InstantiationException, InvocationTargetException, ClassNotFoundException {
		E o = (E) param;
		Field[] fields =  o.getClass().getDeclaredFields();
		final Method[] m = o.getClass().getDeclaredMethods();
			for(Method method : m) {
				String mname = method.getName();
				if(!mname.startsWith("get") || method.isAnnotationPresent(Transient.class)) {
					continue;
				}
				if(method.getGenericReturnType().getClass().isPrimitive()
						|| (method.getGenericReturnType() == Boolean.class)
						|| (method.getGenericReturnType() == Character.class)
						|| (method.getGenericReturnType() == Short.class)
						|| (method.getGenericReturnType() == Long.class)
						|| (method.getGenericReturnType() == Float.class)
						|| (method.getGenericReturnType() == Double.class)
						|| (method.getGenericReturnType() == Integer.class)
						|| (method.getGenericReturnType() == String.class)){
					method.setAccessible(true);
					for(int x = 0; x < fields.length; x++){
						String fname = fields[x].getName();
						if(fname.equalsIgnoreCase(mname.replace("get", ""))){
							params.put(fname, method.invoke(o));
						}
					}
				}else{
					continue;
				}
			}
		return	makeWhere(params, setParameters, queryString,queryWhere);
	}
	
	/**
	 * Construye el resultado del query
	 * @param <E>
	 * @param resultList
	 * @param entityClass
	 * @param columns
	 * @return Lista con los objetos creados
	 * @throws IllegalAccessException
	 * @throws InstantiationException
	 * @throws SecurityException
	 * @throws NoSuchFieldException
	 * @throws IllegalArgumentException
	 * @throws InvocationTargetException
	 * @author martin
	 */
	private <E extends Entidad> List<E> makeResult(List<Object> resultList,
			Class<E> entityClass, String[] columns)
			throws IllegalAccessException, InstantiationException,
			SecurityException, NoSuchFieldException, IllegalArgumentException,
			InvocationTargetException {
		List<E> finalResultList = new LinkedList<E>();
		E tempInstance = entityClass.newInstance();
		final Method[] m = tempInstance.getClass().getDeclaredMethods();
		for(int y = 0; y < resultList.size();y++){
			E o = entityClass.newInstance();
			Object[] valuesResult = null;
			try{
				valuesResult = (Object[])resultList.get(y);
			} catch (RuntimeException e) {
				valuesResult = new Object[resultList.size()];
				valuesResult[y] = resultList.get(y);
			}
			for(int x = 0; x < columns.length; x++) {
				for(Method method : m) {
					String mname = method.getName();
					if(!mname.equalsIgnoreCase("set"+columns[x])) {
						continue;
					}
					Type[] pType = method.getGenericParameterTypes();
			 		if ((pType.length != 1)
					    || Locale.class.isAssignableFrom(pType[0].getClass())) {
			 		    continue;
			 		}
					method.setAccessible(true);
					if(valuesResult[x] != null && (valuesResult[x].getClass() == pType[0])) {
						method.invoke(o,valuesResult[x]);
					}
				}
			}
			finalResultList.add(o);
		}
		return finalResultList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter) {
		return findByFilterObject(entityClass, filter, null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter, String orderBy) {
		Map<String, Object> params = new HashMap<String, Object>();		
		final StringBuilder query = new StringBuilder("");		
		JpaUtil.createSimpleFilterQuery(entityClass, query, filter, params, orderBy);
		Query queryResult = entityManager.createQuery(query.toString(), entityClass);
		JpaUtil.setQueryParametersByProperties(queryResult, params);
		return queryResult.getResultList();
	}

	
	@Override
	public  <E extends Entidad, T>  Long getIncrementedProperty(Class<E> entityClass, String property, T filter){
		Long autonumber = null;		
		StringBuilder query = new StringBuilder("");
		Map<String, Object> params = new HashMap<String, Object>();			
		try{
			query.append("SELECT MAX(model.").append(property).append(") FROM ").append(entityClass.getSimpleName()).append(" model ");		
			JpaUtil.addFilterParameter("model", query, filter, params);
			Query queryResult = entityManager.createQuery(query.toString(), entityClass);
			JpaUtil.setQueryParametersByProperties(queryResult, params);				
			Object object = queryResult.getSingleResult(); 				
			if(object != null){
				autonumber = (Long)object + 1;
			}else{
				autonumber = 1l;
			}
		}catch(Exception ex){
			return null;
		}
		return autonumber;
	}
	
	/**
	 * Gets next value for a property. Useful when entity has an autonumber column as well as an autogenerated id or getting the max actual
	 * value for some filters
	 * @param <E>
	 * @param entityClass
	 * @param property
	 * @return
	 */
	@Override
	public  <E extends Entidad, T> Object getIncrementedProperty(
			Class<E> entityClass, String property, Map<String, Object> filter){
		Integer autonumber = null;		
		StringBuilder query = new StringBuilder("");
		Map<String, Object> params = new HashMap<String, Object>();
		boolean setParameters = false;
		query = query.append("SELECT MAX(model.").append(property).append(") FROM ").append(entityClass.getSimpleName()).append(" model ");				
		if(filter!=null && !filter.isEmpty()){
			setParameters=true;
			query.append(" where ");
			int size= filter.size();
			int index=0;
			for(String prop : filter.keySet()){
				String propertyMap=getValidProperty(prop);//checa si tiene "." la propiedad					
				Object value = filter.get(prop);
				if (value != null) {
					query.append("model."+prop+"=:"+propertyMap);
					params.put(propertyMap, value);
				} else {
					query.append("model."+prop+" IS NULL");
				}				
				if(index!=(size-1)){
					query.append(" and ");
				}
				index++;
			}
		}								
		Query queryResult = entityManager.createQuery(query.toString(), entityClass);
		if(setParameters){
			JpaUtil.setQueryParametersByProperties(queryResult, params);
		}
		Object object = queryResult.getSingleResult(); 				
		if(object != null){
			autonumber = (Integer)object + 1;
		}else{
			autonumber = 1;
		}		
		return autonumber;
	}
	

	/**
	 * Gets next value for a named sequence. Useful when there is a need for a sequenced value
	 * 
	 * @param sequenceName, use schema in the paramenter. Ex. MIDAS.SEQUENCE
	 * @return
	 */
	@Override
	public Long getIncrementedSequence(String sequenceName) {
		Long autonumber = null;		
		StringBuilder query = new StringBuilder("SELECT ");			
		query.append(sequenceName).append(".NEXTVAL FROM DUAL");		
		Query queryResult = entityManager.createNativeQuery(query.toString());					
		Object object = queryResult.getSingleResult(); 				
		if(object != null){
			autonumber = ((BigDecimal)object).longValue();
		}
		return autonumber;
	}

}
