package mx.com.afirme.midas2.dao.impl.repuve;

import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TemporalType;

import java.util.Map;

import mx.com.afirme.midas2.dao.repuve.RepuveSisDao;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisEmision;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisPTT;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRecuperacion;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRobo;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatSistemasService;

/*******************************************************************************
 * Nombre Clase: 		RepuveSisDaoImpl.
 * 
 * Descripcion: 		Esta clase se utiliza para la ejecución de transacciones
 * 						de Base de Datos para el modulo de Emision.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class RepuveSisDaoImpl implements RepuveSisDao{
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext private EntityManager entityManager;
	@EJB private CatSistemasService catSistemasService;
	private static final int HORAS_24 = 86400000;
	private static final String SISTEMA_EMISION_DESC = "EMISION";
	private static final String SISTEMA_ROBO_DESC = "ROBO";
	private static final String SISTEMA_RECUPERACION_DESC = "RECUPERACION";
	private static final String SISTEMA_PTT_DESC = "PPT";
	private static final String SISTEMA_EMISION_OBJETO = "RepuveSisEmision";
	private static final String SISTEMA_ROBO_OBJETO = "RepuveSisRobo";
	private static final String SISTEMA_RECUPERACION_OBJETO = "RepuveSisRecuperacion";
	private static final String SISTEMA_PTT_OBJETO = "RepuveSisPTT";
	@SuppressWarnings("rawtypes")
	private static final Class SISTEMA_EMISION_CLASS = RepuveSisEmision.class;
	@SuppressWarnings("rawtypes")
	private static final Class SISTEMA_ROBO_CLASS = RepuveSisRobo.class;
	@SuppressWarnings("rawtypes")
	private static final Class SISTEMA_RECUPERACION_CLASS = RepuveSisRecuperacion.class;
	@SuppressWarnings("rawtypes")
	private static final Class SISTEMA_PTT_CLASS = RepuveSisPTT.class;

	@SuppressWarnings("unchecked")
	@Override
	public List<RepuveSisEmision> obtenerEmisionPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return construirConsulta(parametrosConsulta, numRegXPag, numPagina, SISTEMA_EMISION_DESC).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RepuveSisRobo> obtenerRoboPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return construirConsulta(parametrosConsulta, numRegXPag, numPagina, SISTEMA_ROBO_DESC).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RepuveSisRecuperacion> obtenerRecuperacionPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return construirConsulta(parametrosConsulta, numRegXPag, numPagina, SISTEMA_RECUPERACION_DESC).getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RepuveSisPTT> obtenerPTTPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return construirConsulta(parametrosConsulta, numRegXPag, numPagina, SISTEMA_PTT_DESC).getResultList();
	}

	private Query construirConsulta(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina, String sistema){
		Map<String, String> sistemasObjectMap = new HashMap<String, String>();
		sistemasObjectMap.put(SISTEMA_EMISION_DESC, SISTEMA_EMISION_OBJETO);
		sistemasObjectMap.put(SISTEMA_ROBO_DESC, SISTEMA_ROBO_OBJETO);
		sistemasObjectMap.put(SISTEMA_RECUPERACION_DESC, SISTEMA_RECUPERACION_OBJETO);
		sistemasObjectMap.put(SISTEMA_PTT_DESC, SISTEMA_PTT_OBJETO);
		@SuppressWarnings("rawtypes")
		Map<String, Class> sistemasClassMap = new HashMap<String, Class>();
		sistemasClassMap.put(SISTEMA_EMISION_DESC, SISTEMA_EMISION_CLASS);
		sistemasClassMap.put(SISTEMA_ROBO_DESC, SISTEMA_ROBO_CLASS);
		sistemasClassMap.put(SISTEMA_RECUPERACION_DESC, SISTEMA_RECUPERACION_CLASS);
		sistemasClassMap.put(SISTEMA_PTT_DESC, SISTEMA_PTT_CLASS);
		StringBuilder queryString = new StringBuilder(" SELECT sas FROM " + sistemasObjectMap.get(sistema) + " sas where sas.sapAmisBitacoras.catSistemas.id = " + catSistemasService.obtenerIdSistemaPorDescripcion(sistema));
		if(parametrosConsulta.getEstatus() != null){
			queryString.append(" and sas.sapAmisBitacoras.catEstatusBitacora.id = :estatus ");
		}
		if(parametrosConsulta.getFechaEnvioIni() != null && parametrosConsulta.getFechaEnvioFin() != null){
			queryString.append(" and sas.sapAmisBitacoras.fechaEnvio BETWEEN :fechaEnvioIni and :fechaEnvioFin ");		
		}
		if(parametrosConsulta.getFechaOperacionIni() != null && parametrosConsulta.getFechaOperacionFin() != null){
			queryString.append(" and sas.sapAmisBitacoras.fechaOperacion BETWEEN :fechaOperacionIni and :fechaOperacionFin ");
		}
		if(parametrosConsulta.getFechaRegistroIni() != null && parametrosConsulta.getFechaRegistroFin() != null){
			queryString.append(" and sas.sapAmisBitacoras.fechaRegistro BETWEEN :fechaRegistroIni and :fechaRegistroFin ");
		}
		if(parametrosConsulta.getModulo() != null){
			queryString.append(" and sas.sapAmisBitacoras.modulo = :modulo ");
		}
		if(parametrosConsulta.getObservaciones() != null){
			queryString.append(" and sas.sapAmisBitacoras.observaciones = :observaciones ");
		}
		if(parametrosConsulta.getOperacion() != null){
			queryString.append(" and sas.sapAmisBitacoras.catOperaciones.id = :operacion ");		
		}
		queryString.append(" ORDER BY sas.sapAmisBitacoras.fechaOperacion asc ");
		@SuppressWarnings("unchecked")
		Query query = entityManager.createQuery(queryString.toString(), sistemasClassMap.get(sistema));
		if(parametrosConsulta.getEstatus() != null){
			query.setParameter("estatus", parametrosConsulta.getEstatus());
		}
		if(parametrosConsulta.getFechaEnvioIni() != null && parametrosConsulta.getFechaEnvioFin() != null){
			query.setParameter("fechaEnvioIni", parametrosConsulta.getFechaEnvioIni(), TemporalType.DATE);
			query.setParameter("fechaEnvioFin", new Date(parametrosConsulta.getFechaEnvioFin().getTime() + HORAS_24), TemporalType.DATE);
		}
		if(parametrosConsulta.getFechaOperacionIni() != null && parametrosConsulta.getFechaOperacionFin() != null){
			query.setParameter("fechaOperacionIni", parametrosConsulta.getFechaOperacionIni(), TemporalType.DATE);
			query.setParameter("fechaOperacionFin", new Date(parametrosConsulta.getFechaOperacionFin().getTime() + HORAS_24), TemporalType.DATE);
		}
		if(parametrosConsulta.getFechaRegistroIni() != null && parametrosConsulta.getFechaRegistroFin() != null){
			query.setParameter("fechaRegistroIni", parametrosConsulta.getFechaRegistroIni(), TemporalType.DATE);
			query.setParameter("fechaRegistroFin", new Date(parametrosConsulta.getFechaRegistroFin().getTime() + HORAS_24), TemporalType.DATE);
		}
		if(parametrosConsulta.getModulo() != null){
			query.setParameter("modulo", parametrosConsulta.getModulo());
		}
		if(parametrosConsulta.getObservaciones() != null){
			query.setParameter("observaciones", parametrosConsulta.getObservaciones());
		}
		if(parametrosConsulta.getOperacion() != null){
			query.setParameter("operacion", parametrosConsulta.getOperacion());
		}
		query.setMaxResults((int)numRegXPag);
		query.setFirstResult((int)((numPagina - 1) * numRegXPag));
		return query;
	}
}
