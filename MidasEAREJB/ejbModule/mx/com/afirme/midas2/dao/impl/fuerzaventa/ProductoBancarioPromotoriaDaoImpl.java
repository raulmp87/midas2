package mx.com.afirme.midas2.dao.impl.fuerzaventa;
import static mx.com.afirme.midas2.utils.CommonUtils.*;

import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.ProductoBancarioPromotoriaDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioPromotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
@Stateless
public class ProductoBancarioPromotoriaDaoImpl extends EntidadDaoImpl implements ProductoBancarioPromotoriaDao {
	@SuppressWarnings("unchecked")
	@Override
	public List<ProductoBancarioPromotoria> findByFilters(ProductoBancarioPromotoria filtro) {
		List<ProductoBancarioPromotoria> lista=new ArrayList<ProductoBancarioPromotoria>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ProductoBancarioPromotoria model left join fetch model.promotoria left join fetch model.productoBancario ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}
			Promotoria promotoria=filtro.getPromotoria();
			if(promotoria!=null){
				if(promotoria.getId()!=null){
					addCondition(queryString, "model.promotoria.id=:idPromotoria");
					params.put("idPromotoria",promotoria.getId());
				}
			}
			ProductoBancario productoBancario=filtro.getProductoBancario();
			if(productoBancario!=null){
				if(productoBancario.getId()!=null){
					addCondition(queryString, "model.productoBancario.id=:idProducto");
					params.put("idProducto",promotoria.getId());
				}
			}
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		lista=query.getResultList();
		return lista;
	}

	@Override
	public List<ProductoBancarioPromotoria> findByPromotoria(Long idPromotoria) {
		Promotoria promotoria=new Promotoria();
		promotoria.setId(idPromotoria);
		ProductoBancarioPromotoria filtro=new ProductoBancarioPromotoria();
		filtro.setPromotoria(promotoria);
		return findByFilters(filtro);
	}

	@Override
	public ProductoBancarioPromotoria saveProducto(ProductoBancarioPromotoria productoBancarioPromotoria)throws Exception {
		if(isNull(productoBancarioPromotoria)){
			throw new Exception("El producto es nulo");
		}
		if(isNotNull(productoBancarioPromotoria.getPromotoria())){
			Promotoria promotoria=productoBancarioPromotoria.getPromotoria();
			Long idPromtoria=(isNotNull(promotoria))?promotoria.getId():null;
			if(isNotNull(idPromtoria)){
				try{
					promotoria=entityManager.getReference(Promotoria.class,idPromtoria);
				}catch(EntityNotFoundException e){
					promotoria=entityManager.find(Promotoria.class,idPromtoria);
				}
			}
			productoBancarioPromotoria.setPromotoria(promotoria);
		}
		if(isNotNull(productoBancarioPromotoria.getProductoBancario())){
			ProductoBancario producto=productoBancarioPromotoria.getProductoBancario();
			Long idProducto=(isNotNull(producto))?producto.getId():null;
			if(isNotNull(idProducto)){
				try{
					producto=entityManager.getReference(ProductoBancario.class,idProducto);
				}catch(EntityNotFoundException e){
					producto=entityManager.find(ProductoBancario.class,idProducto);
				}
			}
			productoBancarioPromotoria.setProductoBancario(producto);
		}
		if(productoBancarioPromotoria.getId()!=null){
			productoBancarioPromotoria=update(productoBancarioPromotoria);
		}else{
			persist(productoBancarioPromotoria);
		}
		return productoBancarioPromotoria;
	}

	@Override
	public void delete(Long id) throws Exception {
		if(id==null){
			throw new Exception("Producto es nulo");
		}
		ProductoBancarioPromotoria producto=findById(ProductoBancarioPromotoria.class,id);
		if(isNull(producto)){
			throw new Exception("Producto con id:"+id+" no existe");
		}
		entityManager.refresh(producto);
		if(producto!=null){
			remove(producto);
		}
	}
	@Override
	public void saveListProductoBanc(List<ProductoBancarioPromotoria> lista) throws Exception {
		int cont =0; 
		List<ProductoBancarioPromotoria> listProdancAEliminar = new ArrayList<ProductoBancarioPromotoria>();
	    for(ProductoBancarioPromotoria obj:lista){
	    	if(cont==0){
	    		Map<String, Object>params = new HashMap<String, Object>();
	    		params.put("promotoria.id", obj.getPromotoria().getId());
	    		listProdancAEliminar = this.findByProperties(ProductoBancarioPromotoria.class, params);
	    		cont++;
	    	}
	    	obj.getProductoBancario().setId(obj.getIdProductoBancario());
	    	saveProducto(obj);
	    }
	    for(ProductoBancarioPromotoria objAEliminar:listProdancAEliminar){
	    	this.remove(objAEliminar);
	    }
	}
	
	public void deleteAllProductosByIdPromotoria(Long promotoriaId){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("promotoria.id", promotoriaId);
		Long existe = this.findByPropertiesCount(ProductoBancarioPromotoria.class, params);
		if(existe>0){
			StringBuilder queryString = new StringBuilder();
			queryString.append(" delete from  ProductoBancarioPromotoria model where model.promotoria.id = :id");
			Query query = this.entityManager.createQuery(queryString.toString());
			query.setParameter("id", promotoriaId);
			query.executeUpdate();
		}
	}
	
	public String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	public void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	public void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	@Override
	public List<ProductoBancarioPromotoria> findByPromotoria(Long idPromotoria,	String fechaHistorico, String tipoAccion) {
		
		boolean isHistorico = (Integer.parseInt(tipoAccion.trim())==5)?true:false;
		final StringBuilder queryString=new StringBuilder("");
	    Map<String,Object>params = new HashMap<String, Object>(); 
	    List<ProductoBancarioPromotoria>lista = new ArrayList<ProductoBancarioPromotoria>();
	    queryString.append("select model from ProductoBancarioPromotoria model left join fetch model.promotoria left join fetch model.productoBancario");
	    addCondition(queryString, "model.promotoria.id=:idProm");
	    params.put("idProm",idPromotoria);
		if(!isHistorico)
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista=query.getResultList();			
		}
		else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			StringBuilder consulta = new StringBuilder();
			consulta.append(" select Count(*) from MIDAS.toProductoBancarioProm_Hist ");
			consulta.append(" where PROMOTORIA_ID = "+idPromotoria );//10335
			consulta.append(" and H_START_DATE <=  to_timestamp('"+fechaHistorico.substring(0,fechaHistorico.length()-3)+"','DD/MM/YYYY HH24:MI:SS.FF')"); //
			BigDecimal ExisteHistorico =(BigDecimal) this.entityManager.createNativeQuery(consulta.toString()).getSingleResult();
			if(ExisteHistorico.compareTo(BigDecimal.ZERO)==1){	
				JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
				ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
				AsOfClause asOfClause = new AsOfClause(convertedDate);
				Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
				
				ReadAllQuery historicalQuery = new ReadAllQuery();
				historicalQuery.setJPQLString(getQueryString(queryString).toString());
				historicalQuery.addArgument("idProm");
				historicalQuery.addArgumentValue(idPromotoria);
				historicalQuery.refreshIdentityMapResult();
				lista = (List<ProductoBancarioPromotoria>)historicalSession.executeQuery(historicalQuery);
			}
		}
		return lista;
	}
}
