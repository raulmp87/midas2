package mx.com.afirme.midas2.dao.impl.calculos;
import static mx.com.afirme.midas2.utils.CommonUtils.addCondition;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;
import static mx.com.afirme.midas2.utils.CommonUtils.setQueryParametersByProperties;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas2.dao.calculos.CalculoComisionesDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.calculos.CalculoComisiones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComMotivoEstatus;
import mx.com.afirme.midas2.domain.comisiones.ConfigComPrioridad;
import mx.com.afirme.midas2.domain.comisiones.ConfigComSituacion;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoAgente;
import mx.com.afirme.midas2.domain.comisiones.ConfigComTipoPromotoria;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.dto.calculos.DetalleCalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.calculos.AgentesCalculoTemporalService;
import mx.com.afirme.midas2.service.calculos.DetalleCalculoComisionesService;
import mx.com.afirme.midas2.service.calculos.SolicitudChequesMizarService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.comisiones.ConfigComisionesService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.impuestosestatales.RetencionImpuestosService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.util.MidasException;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class CalculoComisionesDaoImpl extends EntidadDaoImpl implements CalculoComisionesDao{
	@PersistenceContext
	private EntityManager em;
	private EntidadService entidadService;
	private ValorCatalogoAgentesService catalogoService;
	private ConfigComisionesService configComisionesService;
	private AgentesCalculoTemporalService calculoTemporalService;
	private DetalleCalculoComisionesService detalleCalculoComisionesService;
	private SolicitudChequesMizarService solicitudChequesMizarService;
	private AgenteMidasService agenteMidasService;
	private ImpresionesService impresionesService;
	private RetencionImpuestosService retencionImpuestosService;
	/**
	 * ========================================================
	 * Constants
	 * ========================================================
	 */
	private static String ERROR_CALCULO_NULO;//se declaran las variables comunes como estaticas para no volver a crear objetos para mostrar estos mensajes
	private static String ERROR_CONFIGURACION_NULL;
	private static String ERROR_CALCULO_NOTFOUND;
	private static String ERROR_CONFIGURACION_NOT_FOUND;
	private static String CATALOGO_ESTATUS_CALCULO="Estatus Calculo Comisiones";
	private static String ESTATUS_CALCULO_CANCELADO="CANCELADO";
	private static String EXCEPTION_RESOURCES=SistemaPersistencia.RECURSO_MENSAJES_EXCEPCIONES;
	private static String CLAVE_TRANSACCION_AGENTES="AGT  ";
	private static String CLAVEL_CONCEPTO_TRANSACCION="PCOM ";
	private static String USUARIO_TRANSACCION="MIDAS";
	private static String EMPRESA_DEFAULT="8";
	private static String CLAVE_CLASE_AGENTE="A";
	static{
		ERROR_CALCULO_NULO=Utilerias.getMensajeRecurso(EXCEPTION_RESOURCES, "midas.calculos.calculoComisiones.error.idCalculoNull");
		ERROR_CONFIGURACION_NULL=Utilerias.getMensajeRecurso(EXCEPTION_RESOURCES,"midas.calculos.calculoComisiones.error.idConfiguracionNull");
		ERROR_CALCULO_NOTFOUND=Utilerias.getMensajeRecurso(EXCEPTION_RESOURCES,"midas.calculos.calculoComisiones.error.calculoNotFound");
		ERROR_CONFIGURACION_NOT_FOUND=Utilerias.getMensajeRecurso(EXCEPTION_RESOURCES,"midas.calculos.calculoComisiones.error.configComisionesNotFound");
	}
	private static final String PKGCALCULOS_AGENTES="PKGCALCULOS_AGENTES";
	private static final String PKG_INT_MIDAS_E2="Pkg_int_midas_E2";
	
	/**
	 * Carga la configuracion del pago de comisiones, obteniendo los agentes como 
	 * resultado de la configuracion, y de el listado de agentes se consulta a la tabla de movimientos donde 
	 * este involucrado cada agente y se obtiene el monto total del importe de comisiones por cada agente. 
	 * Dicho monto total se sumara por todos los agentes para obtener el monto total que se pagara de comisiones
	 * y representara el importe del pago de comision del calculo realizado.
	 * @param idConfigComisiones
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long generarCalculo(Long idConfigComisiones,List<AgenteView> agentesDelCalculo,Long idCalculoTemporal,String queryAgentes) throws MidasException{
		Long idCalculo=null;
		if(isNull(idConfigComisiones)){
			onError(ERROR_CONFIGURACION_NULL);
		}
		ConfigComisiones filtro=new ConfigComisiones();
		filtro.setId(idConfigComisiones);
		try {
			ConfigComisiones persistentObjectConfigComisiones=configComisionesService.loadById(filtro);
			if(isNull(persistentObjectConfigComisiones)|| isNull(persistentObjectConfigComisiones.getId())){
				onError(ERROR_CONFIGURACION_NOT_FOUND);
			}
			//Por cada elemento de la lista se va a buscar sus agentes, se debe de verificar primero si tiene hijos o no seleccionados, si si, entonces
			//se excluyen sino no.
			if(!isValid(queryAgentes)){
				onError("No hay agentes para considerar en el calculo de la configuracion no."+persistentObjectConfigComisiones.getId());
			}
			//Se comenta pk se hace desde el action.
			//Long idCalculoTemporal=calculoTemporalService.getNextIdCalculo();
			idCalculo=obtenerCalculoPorMovimientosAgentes(idConfigComisiones, agentesDelCalculo,idCalculoTemporal,null,queryAgentes);
		} catch (Exception e) {
			onError(e);
		}
		return idCalculo;
	}
	/**
	 * Metodo para recalcular las comisiones por medio del id de calculo de comisiones
	 * @param idConfigComisiones
	 * @param agentesDelCalculo
	 * @param idCalculoComisiones
	 * @return
	 * @throws MidasException
	 */
//	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long recalcularCalculoComisiones(Long idConfigComisiones,List<AgenteView> agentesDelCalculo,Long idCalculoComisiones,Long idCalculoTemporal,String queryAgentes) throws MidasException{
		Long idCalculo=null;
		if(isNull(idConfigComisiones)){
			onError(ERROR_CONFIGURACION_NULL);
		}
		if(isNull(idCalculoComisiones)){
			onError("Favor de proporcionar el calculo a recalcular");
		}
		ConfigComisiones filtro=new ConfigComisiones();
		filtro.setId(idConfigComisiones);
		try {
			ConfigComisiones persistentObjectConfigComisiones=configComisionesService.loadById(filtro);
			if(isNull(persistentObjectConfigComisiones)|| isNull(persistentObjectConfigComisiones.getId())){
				onError(ERROR_CONFIGURACION_NOT_FOUND);
			}
			//Por cada elemento de la lista se va a buscar sus agentes, se debe de verificar primero si tiene hijos o no seleccionados, si si, entonces
			//se excluyen sino no.
			if(!isValid(queryAgentes)){
				onError("No hay agentes para considerar en el calculo de la configuracion no."+persistentObjectConfigComisiones.getId());
			}
			//Long idCalculoTemporal=calculoTemporalService.getNextIdCalculo();
			//Se comenta pk se hace desde el action. 
			//Long idCalculoTemporal=getNextIdCalculoTemporal();
			em.flush();
			idCalculo=obtenerCalculoPorMovimientosAgentes(idConfigComisiones, agentesDelCalculo,idCalculoTemporal,idCalculoComisiones,queryAgentes);
		} catch (Exception e) {
			onError(e);
		}
		return idCalculo;
	}
	
	
	
	public Long getNextIdCalculoTemporal() throws MidasException{
		Long idCalculo=null;
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS."+PKGCALCULOS_AGENTES+".stp_getNextIdCalculoTmp";
		try {
			LogDeMidasInterfaz.log("Entrando a CalculoComisionesDaoImpl.getNextIdCalculoTemporal..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			int id=storedHelper.ejecutaActualizar();
			idCalculo=Long.valueOf(id);
			LogDeMidasInterfaz.log("Saliendo de CalculoComisionesDaoImpl.getNextIdCalculoTemporal..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,CalculoComisionesDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de CalculoComisionesDaoImpl.getNextIdCalculoTemporal..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en CalculoComisionesDaoImpl.getNextIdCalculoTemporal..." + this, Level.WARNING, e);
			onError(e);
		}
		return idCalculo;
	}
	
	public List<AgenteView> cargarAgentesPorConfiguracion(Long idConfigComisiones) throws MidasException{
		List<AgenteView> list=new ArrayList<AgenteView>();
		if(isNull(idConfigComisiones)){
			onError(ERROR_CONFIGURACION_NULL);
		}
		ConfigComisiones filtro=new ConfigComisiones();
		filtro.setId(idConfigComisiones);
		try {
			ConfigComisiones persistentObjectConfigComisiones=configComisionesService.loadById(filtro);
			if(isNull(persistentObjectConfigComisiones)|| isNull(persistentObjectConfigComisiones.getId())){
				onError(ERROR_CONFIGURACION_NOT_FOUND);
			}
			list=obtenerAgentes(persistentObjectConfigComisiones);
			if(isEmptyList(list)){
				onError("No hay agentes para considerar en el calculo de la configuracion no."+persistentObjectConfigComisiones.getId());
			}
		} catch (Exception e) {
			onError(e);
		}
		return list;
	}
	@Override
	public String cargarAgentesPorConfiguracionQuery(Long idConfigComisiones) throws MidasException{
		String query=null;
		if(isNull(idConfigComisiones)){
			onError(ERROR_CONFIGURACION_NULL);
		}
		ConfigComisiones filtro=new ConfigComisiones();
		filtro.setId(idConfigComisiones);
		try {
			ConfigComisiones persistentObjectConfigComisiones=configComisionesService.loadById(filtro);
			if(isNull(persistentObjectConfigComisiones)|| isNull(persistentObjectConfigComisiones.getId())){
				onError(ERROR_CONFIGURACION_NOT_FOUND);
			}
			query=obtenerAgenteQuery(persistentObjectConfigComisiones);
			if(!isValid(query)){
				onError("No hay agentes para considerar en el calculo de la configuracion no."+persistentObjectConfigComisiones.getId());
			}
		} catch (Exception e) {
			onError(e);
		}
		return query;
	}
	
	
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long guardarAgentesEnTemporal(List<AgenteView> agentes,Long idCalculoTemporal) throws MidasException{
		List<Long> idAgentes=new ArrayList<Long>();
		if(!isEmptyList(agentes)){
			//Se agregan las gerencias separadas por ,
			for(AgenteView agente:agentes){
				if(isNotNull(agente) && isNotNull(agente.getId())){
					idAgentes.add(agente.getId());
				}
			}
		}
		//Long idCalculo=calculoTemporalService.getNextIdCalculo();
		calculoTemporalService.saveAll(idAgentes, "AGENTE", idCalculoTemporal);
		em.flush();
		return idCalculoTemporal;
	}
	/**
	 * Obtiene los movimientos de los agentes del calculo
	 * @param idConfigComisiones
	 * @param agentes
	 * @param idCalculoTemporal
	 * @param idCalculoComisiones Si no es nulo, entonces se va a actualizar lsod atos del calculo. Si es nulo, se registra un nuevo calculo
	 * @return
	 * @throws MidasException
	 */
	private Long obtenerCalculoPorMovimientosAgentes(Long idConfigComisiones,List<AgenteView> agentes,Long idCalculoTemporal,Long idCalculoComisiones,String queryAgentes) throws MidasException{
		if(retencionImpuestosService.isCalculoRetencionImpuestosEnProceso()) {
			throw new RuntimeException("Alguna configraci\u00F3n de Impuestos Cedulares esta siendo procesada en este momento. Intente de nuevo m\u00E1s tarde.");
		}
		
		if(!isValid(queryAgentes)){
			onError("Favor de proporcionar la lista de agentes a obtener el calculo");
		}
		//Se comenta pk se hace desde el action.
		//guardarAgentesEnTemporal(agentes,idCalculoTemporal);
		em.flush();
		//Si el idCalculoComisiones no es nulo, entonces se considera ese, y sino, se obtiene como un nuevo calculo de la secuencia.
//		idCalculoComisiones=(isNotNull(idCalculoComisiones))?idCalculoComisiones:getNextIdCalculoComisiones();
		idCalculoComisiones=(isNotNull(idCalculoComisiones))?idCalculoComisiones:null;
		//validarAgentesEnCalculosPendientes(idCalculoTemporal);
		Long resultado=iniciarCalculo(idConfigComisiones, idCalculoComisiones, idCalculoTemporal,queryAgentes);
		return resultado;
	}
	/**
	 * Metodo que hace el calculo de pago de comisiones
	 * @param idConfiguracionComisiones
	 * @param idCalculoComisiones
	 * @param idCalculoTemporal
	 * @return
	 * @throws MidasException
	 */
	private Long iniciarCalculo(Long idConfiguracionComisiones,Long idCalculoComisiones,Long idCalculoTemporal,String queryAgentes) throws MidasException{
		Long idCalculo=null;
//		StoredProcedureHelper storedHelper = null;
//		String sp="MIDAS.PKGCALCULOS_AGENTES.stp_generarCalculoComisiones";
//		try {
//			LogDeMidasInterfaz.log("Entrando a CalculoComisionesDaoImpl.inciarCalculo..." + this, Level.INFO, null);
//			storedHelper = new StoredProcedureHelper(sp);
//			storedHelper.estableceParametro("pIdConfigComisiones",idConfiguracionComisiones);
//			storedHelper.estableceParametro("pIdCalculoComisiones",idCalculoComisiones);
//			storedHelper.estableceParametro("pIdCalculoTemporal",idCalculoTemporal);
//			int id=storedHelper.ejecutaActualizar();
//			idCalculo=Long.valueOf(id);
//			//FIXME Agregar update de calculo en la tabla temporal de calculos-Agentes
//			actualizarTemporalConCalculoGenerado(idCalculo,idCalculoTemporal);
//			LogDeMidasInterfaz.log("Saliendo de CalculoComisionesDaoImpl.inciarCalculo..." + this, Level.INFO, null);
//		} catch (SQLException e) {				
//			Integer codErr = null;
//			String descErr = null;
//			if (storedHelper != null) {
//				codErr = storedHelper.getCodigoRespuesta();
//				descErr = storedHelper.getDescripcionRespuesta();
//			}
//			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,CalculoComisionesDaoImpl.class, codErr, descErr);
//			LogDeMidasInterfaz.log("Excepcion en BD de CalculoComisionesDaoImpl.inciarCalculo..." + this, Level.WARNING, e);
//			onError(e);
//		} catch (Exception e) {
//			LogDeMidasInterfaz.log("Excepcion general en CalculoComisionesDaoImpl.inciarCalculo..." + this, Level.WARNING, e);
//			onError(e);
//		}
		em.flush();
		idCalculo=solicitudChequesMizarService.iniciarCalculoComisiones(idConfiguracionComisiones,idCalculoComisiones,idCalculoTemporal,queryAgentes);
		actualizarTemporalConCalculoGenerado(idCalculo,idCalculoTemporal);
		return idCalculo;
	}
	
	@SuppressWarnings("unchecked")
	private Long getNextIdCalculoComisiones(){
		Long idCalculo=null;
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT MIDAS.idToCalculoComisiones_seq.nextval as idCalculo from dual ");
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			idCalculo=(isNotNull(lista.get(0)))?lista.get(0).longValue():null;
		}
		return idCalculo;
	} 
	/**
	 * metodo que actualiza la tabla temporal para con el id del calculo real.
	 * @param idCalculo
	 * @param idCalculoTemporal
	 */
	private void actualizarTemporalConCalculoGenerado(Long idCalculo,Long idCalculoTemporal){
		
	}
	
	private String obtenerAgenteQuery(ConfigComisiones configuracion) throws Exception{
		List<AgenteView> agentes=new ArrayList<AgenteView>();
		List<CentroOperacionView> centros=new ArrayList<CentroOperacionView>();
		List<GerenciaView> gerencias=new ArrayList<GerenciaView>();
		List<EjecutivoView> ejecutivos=new ArrayList<EjecutivoView>();
		List<PromotoriaView> promotorias=new ArrayList<PromotoriaView>();
		List<Long> configCentros=configComisionesService.getCentrosOperacionPorConfiguracionView(configuracion);
		List<Long> configGerencias=configComisionesService.getGerenciasPorConfiguracionView(configuracion);
		List<Long> configEjecutivos=configComisionesService.getEjecutivosPorConfiguracionView(configuracion);
		List<Long> configPromotorias=configComisionesService.getPromotoriasPorConfiguracionView(configuracion);
		List<Long> configPrioridades=configComisionesService.getPrioridadesPorConfiguracionView(configuracion);
		List<Long> configSituaciones=configComisionesService.getSituacionesPorConfiguracionView(configuracion);
		List<Long> configTiposAgente=configComisionesService.getTiposAgentePorConfiguracionView(configuracion);
		List<Long> configTiposPromotoria=configComisionesService.getTiposPromotoriaPorConfiguracionView(configuracion);
		List<Long> configMotivoEstatus=configComisionesService.getMotivoEstatusPorConfiguracionView(configuracion);
		if(!isEmptyList(configCentros)){
			for(Long idCentro:configCentros){
				//Long idCentro=(isNotNull(centro) && isNotNull(centro.getCentroOperacion()) && isNotNull(centro.getCentroOperacion().getId()))?centro.getCentroOperacion().getId():null;
				if(isNotNull(idCentro)){
					CentroOperacionView element=new CentroOperacionView();
					element.setId(idCentro);
					centros.add(element);
				}
			}
		}
		if(!isEmptyList(configGerencias)){
			//FIXME una vez obteniendo los Centros de operacion de la configuracion, se checa la
			//lista de las gerencias de la configuracion y se vuelven a obtener aquellas excluyendo los centros de operacion
			//obtenidos de la configuracion, se hace lo mismo con los ejecutivos, pasandole la lista de las gerencias del resultado
			//de la exclusion y se guardan esos ejecutivos, y aplica lo mismo para las promotorias, de tal manera que se tenga una lista de 
			//centros de operacion,gerencias, ejecutivos y promotorias, y despues de eso se obtienen las lista de los agentes de cada una de las listas
			//despues, se consultada en la tabla de movimientos la lista de los agentes obtenidos y  se calcula la suma total
			//del importe de comisiones.
			gerencias=configComisionesService.getGerenciasConCentrosOperacionExcluyentes(configGerencias, configCentros);
		}
		if(!isEmptyList(configEjecutivos)){
			ejecutivos=configComisionesService.getEjecutivosConGerenciasExcluyentes(configEjecutivos, configGerencias,configCentros);
		}
		if(!isEmptyList(configPromotorias)){
			promotorias=configComisionesService.getPromotoriasConEjecutivosExcluyentes(configPromotorias, configEjecutivos,configGerencias,configCentros);
		}
		List<ConfigComTipoAgente> tiposAgente=new ArrayList<ConfigComTipoAgente>();
		if(!isEmptyList(configTiposAgente)){
			for(Long idTipoAgente:configTiposAgente){
				ConfigComTipoAgente configTipoAgente=new ConfigComTipoAgente();
				ValorCatalogoAgentes tipoAgente=new ValorCatalogoAgentes();
				tipoAgente.setId(idTipoAgente);
				configTipoAgente.setTipoAgente(tipoAgente);
				tiposAgente.add(configTipoAgente);
			}
		}
		List<ConfigComTipoPromotoria> tiposPromotoria=new ArrayList<ConfigComTipoPromotoria>();
		if(!isEmptyList(configTiposPromotoria)){
			for(Long id:configTiposPromotoria){
				ConfigComTipoPromotoria config=new ConfigComTipoPromotoria();
				ValorCatalogoAgentes tipoPromotoria=new ValorCatalogoAgentes();
				tipoPromotoria.setId(id);
				config.setTipoPromotoria(tipoPromotoria);
				tiposPromotoria.add(config);
			}
		}
		List<ConfigComMotivoEstatus> motivosEstatus=new ArrayList<ConfigComMotivoEstatus>();
		if(!isEmptyList(configMotivoEstatus)){
			for(Long id:configMotivoEstatus){
				ConfigComMotivoEstatus config=new ConfigComMotivoEstatus();
				ValorCatalogoAgentes motivoEstatus=new ValorCatalogoAgentes();
				motivoEstatus.setId(id);
				config.setIdMotivoEstatus(motivoEstatus);
				motivosEstatus.add(config);
			}
		}
		
		List<ConfigComSituacion> situaciones=new ArrayList<ConfigComSituacion>();
		if(!isEmptyList(configSituaciones)){
			for(Long id:configSituaciones){
				ConfigComSituacion config=new ConfigComSituacion();
				ValorCatalogoAgentes situacion=new ValorCatalogoAgentes();
				situacion.setId(id);
				config.setSituacionAgente(situacion);
				situaciones.add(config);
			}
		}
		List<ConfigComPrioridad> prioridades=new ArrayList<ConfigComPrioridad>();
		if(!isEmptyList(configPrioridades)){
			for(Long id:configPrioridades){
				ConfigComPrioridad config=new ConfigComPrioridad();
				ValorCatalogoAgentes prioridad=new ValorCatalogoAgentes();
				prioridad.setId(id);
				config.setPrioridadAgente(prioridad);
				prioridades.add(config);
			}
		}
		ConfigComisiones temp=new ConfigComisiones();
		temp.setListaCentroOperacionView(centros);
		temp.setListaGerenciaView(gerencias);
		temp.setListaEjecutivoView(ejecutivos);
		temp.setListaPromotoriaView(promotorias);
		temp.setAgenteFin(configuracion.getAgenteFin());
		temp.setAgenteInicio(configuracion.getAgenteInicio());
		temp.setAgentes(configuracion.getAgentes());
		temp.setListaTipoAgentes(tiposAgente);
		temp.setListaTiposPromotoria(tiposPromotoria);
		temp.setListaSituaciones(situaciones);
		temp.setListaPrioridades(prioridades);
		temp.setListaMotivoEstatus(motivosEstatus);
		return configComisionesService.obtenerAgentesPorConfiguracionQuery(temp);
	}
	
	public List<AgenteView> obtenerAgentes(ConfigComisiones configuracion) throws Exception{
		List<AgenteView> agentes=new ArrayList<AgenteView>();
		List<CentroOperacionView> centros=new ArrayList<CentroOperacionView>();
		List<GerenciaView> gerencias=new ArrayList<GerenciaView>();
		List<EjecutivoView> ejecutivos=new ArrayList<EjecutivoView>();
		List<PromotoriaView> promotorias=new ArrayList<PromotoriaView>();
		List<Long> configCentros=configComisionesService.getCentrosOperacionPorConfiguracionView(configuracion);
		List<Long> configGerencias=configComisionesService.getGerenciasPorConfiguracionView(configuracion);
		List<Long> configEjecutivos=configComisionesService.getEjecutivosPorConfiguracionView(configuracion);
		List<Long> configPromotorias=configComisionesService.getPromotoriasPorConfiguracionView(configuracion);
		List<Long> configPrioridades=configComisionesService.getPrioridadesPorConfiguracionView(configuracion);
		List<Long> configSituaciones=configComisionesService.getSituacionesPorConfiguracionView(configuracion);
		List<Long> configClasificacionAgente=configComisionesService.getTiposAgentePorConfiguracionView(configuracion);
		List<Long> configTiposPromotoria=configComisionesService.getTiposPromotoriaPorConfiguracionView(configuracion);
		List<Long> configMotivoEstatus = configComisionesService.getMotivoEstatusPorConfiguracionView(configuracion);
		
		if(!isEmptyList(configCentros)){
			for(Long idCentro:configCentros){
				//Long idCentro=(isNotNull(centro) && isNotNull(centro.getCentroOperacion()) && isNotNull(centro.getCentroOperacion().getId()))?centro.getCentroOperacion().getId():null;
				if(isNotNull(idCentro)){
					CentroOperacionView element=new CentroOperacionView();
					element.setId(idCentro);
					centros.add(element);
				}
			}
		}
		if(!isEmptyList(configGerencias)){
			//FIXME una vez obteniendo los Centros de operacion de la configuracion, se checa la
			//lista de las gerencias de la configuracion y se vuelven a obtener aquellas excluyendo los centros de operacion
			//obtenidos de la configuracion, se hace lo mismo con los ejecutivos, pasandole la lista de las gerencias del resultado
			//de la exclusion y se guardan esos ejecutivos, y aplica lo mismo para las promotorias, de tal manera que se tenga una lista de 
			//centros de operacion,gerencias, ejecutivos y promotorias, y despues de eso se obtienen las lista de los agentes de cada una de las listas
			//despues, se consultada en la tabla de movimientos la lista de los agentes obtenidos y  se calcula la suma total
			//del importe de comisiones.
			gerencias=configComisionesService.getGerenciasConCentrosOperacionExcluyentes(configGerencias, configCentros);
		}
		if(!isEmptyList(configEjecutivos)){
			ejecutivos=configComisionesService.getEjecutivosConGerenciasExcluyentes(configEjecutivos, configGerencias,configCentros);
		}
		if(!isEmptyList(configPromotorias)){
			promotorias=configComisionesService.getPromotoriasConEjecutivosExcluyentes(configPromotorias, configEjecutivos,configGerencias,configCentros);
		}
		List<ConfigComTipoAgente> listClasificacionAgente=new ArrayList<ConfigComTipoAgente>();
		if(!isEmptyList(configClasificacionAgente)){
			for(Long idClasificacionAgente:configClasificacionAgente){
				ConfigComTipoAgente configClasifAgente=new ConfigComTipoAgente();
				ValorCatalogoAgentes clasifAgente=new ValorCatalogoAgentes();
				clasifAgente.setId(idClasificacionAgente);
				configClasifAgente.setTipoAgente(clasifAgente);
				listClasificacionAgente.add(configClasifAgente);
			}
		}
		List<ConfigComTipoPromotoria> tiposPromotoria=new ArrayList<ConfigComTipoPromotoria>();
		if(!isEmptyList(configTiposPromotoria)){
			for(Long id:configTiposPromotoria){
				ConfigComTipoPromotoria config=new ConfigComTipoPromotoria();
				ValorCatalogoAgentes tipoPromotoria=new ValorCatalogoAgentes();
				tipoPromotoria.setId(id);
				config.setTipoPromotoria(tipoPromotoria);
				tiposPromotoria.add(config);
			}
		}
		List<ConfigComMotivoEstatus> motivosEstatus=new ArrayList<ConfigComMotivoEstatus>();
		if(!isEmptyList(configMotivoEstatus)){
			for(Long id:configMotivoEstatus){
				ConfigComMotivoEstatus config=new ConfigComMotivoEstatus();
				ValorCatalogoAgentes motivoEstatus=new ValorCatalogoAgentes();
				motivoEstatus.setId(id);
				config.setIdMotivoEstatus(motivoEstatus);
				motivosEstatus.add(config);
			}
		}
		
		List<ConfigComSituacion> situaciones=new ArrayList<ConfigComSituacion>();
		if(!isEmptyList(configSituaciones)){
			for(Long id:configSituaciones){
				ConfigComSituacion config=new ConfigComSituacion();
				ValorCatalogoAgentes situacion=new ValorCatalogoAgentes();
				situacion.setId(id);
				config.setSituacionAgente(situacion);
				situaciones.add(config);
			}
		}
		List<ConfigComPrioridad> prioridades=new ArrayList<ConfigComPrioridad>();
		if(!isEmptyList(configPrioridades)){
			for(Long id:configPrioridades){
				ConfigComPrioridad config=new ConfigComPrioridad();
				ValorCatalogoAgentes prioridad=new ValorCatalogoAgentes();
				prioridad.setId(id);
				config.setPrioridadAgente(prioridad);
				prioridades.add(config);
			}
		}
		ConfigComisiones temp=new ConfigComisiones();
		temp.setListaCentroOperacionView(centros);
		temp.setListaGerenciaView(gerencias);
		temp.setListaEjecutivoView(ejecutivos);
		temp.setListaPromotoriaView(promotorias);
		temp.setAgenteFin(configuracion.getAgenteFin());
		temp.setAgenteInicio(configuracion.getAgenteInicio());
		temp.setAgentes(configuracion.getAgentes());
		temp.setListaTipoAgentes(listClasificacionAgente);
		temp.setListaTiposPromotoria(tiposPromotoria);
		temp.setListaSituaciones(situaciones);
		temp.setListaPrioridades(prioridades);
		temp.setListaMotivoEstatus(motivosEstatus);
		//FIXME Aplicar filtros para cargar
		agentes=configComisionesService.obtenerAgentesPorConfiguracion(temp);
		return agentes;
	}
	
	/**
	 * Carga la configuracion del pago de comisiones, obteniendo los agentes como 
	 * resultado de la configuracion, y de el listado de agentes se consulta a la tabla de movimientos donde 
	 * este involucrado cada agente y se obtiene el monto total del importe de comisiones por cada agente. 
	 * Dicho monto total se sumara por todos los agentes para obtener el monto total que se pagara de comisiones
	 * y representara el importe del pago de comision del calculo realizado.
	 * @param calculo
	 * @return
	 * @throws MidasException
	 */
	public Long generarCalculo(ConfigComisiones configuracion,List<AgenteView> agentesDelCalculo,Long idCalculoTemporal,String queryAgentes) throws MidasException{
		Long idCalculo=null;
		if(isNull(configuracion)|| isNull(configuracion.getId())){
			onError(ERROR_CONFIGURACION_NULL);
		}
		idCalculo=generarCalculo(configuracion.getId(),agentesDelCalculo,idCalculoTemporal,queryAgentes);
		return idCalculo;
	}
	/**
	 * Permite cargar por medio del id del calculo los datos del calculo de la comision.
	 * @param idCalculo
	 * @return
	 * @throws MidasException
	 */
	public CalculoComisiones loadById(Long idCalculo) throws MidasException{
		CalculoComisiones persistentObject=null;
		if(isNull(idCalculo)){
			onError(ERROR_CALCULO_NULO);
		}
		CalculoComisiones filtro=new CalculoComisiones();
		filtro.setId(idCalculo);
		List<CalculoComisiones> list=findByFilters(filtro);
		if(!isEmptyList(list)){
			persistentObject=list.get(0);
		}
		return persistentObject;
	}
	/**
	 * Permite cargar los datos del calculo de la comision.
	 * @param calculo
	 * @return
	 * @throws MidasException
	 */
	public CalculoComisiones loadById(CalculoComisiones calculo) throws MidasException{
		CalculoComisiones persistentObject=null;
		if(isNull(calculo)|| isNull(calculo.getId())){
			onError(ERROR_CALCULO_NULO);
		}
		persistentObject=loadById(calculo.getId());
		return persistentObject;
	}
	/**
	 * Enlista los calculos de comisiones para mostrarlos en el preview de comisiones
	 * @param filtro
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<CalculoComisiones> findByFilters(CalculoComisiones filtro){
		List<CalculoComisiones> list=new ArrayList<CalculoComisiones>();
		if(isNotNull(filtro)){
			Map<String,Object> params=new HashMap<String, Object>();
			final StringBuilder queryString=new StringBuilder("");
			queryString.append("select model from CalculoComisiones model ");
			if(isNotNull(filtro.getId())){
				addCondition(queryString, "model.id >= :id");
				params.put("id", filtro.getId());
			}else{
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				if(filtro.getFechaCalculo()!=null){
					Date fecha=filtro.getFechaCalculo();					
					String fechaString=format.format(fecha);
					if(isValid(fechaString)){
						addCondition(queryString, "model.fechaCalculo >= :fechaCorteInicio");
						params.put("fechaCorteInicio", fecha);
					}
				}
				if(filtro.getFechaCalculoFin()!=null){
					Date fechaFin=filtro.getFechaCalculoFin();
					String fechaFinString=format.format(fechaFin);
					if(isValid(fechaFinString)){
						addCondition(queryString, "model.fechaCalculo <= :fechaCorteFin");
						params.put("fechaCorteFin", fechaFin);
					}
				}
				ValorCatalogoAgentes estatus=filtro.getClaveEstatus();
				if(isNotNull(estatus) && isNotNull(estatus.getId())){
					addCondition(queryString, "model.claveEstatus.id= :idEstatus");
					params.put("idEstatus", estatus.getId());
				}
				ConfigComisiones configuracionComision=filtro.getConfiguracionComisiones();
				if(isNotNull(configuracionComision)){
					if(isNotNull(configuracionComision.getId())){
						addCondition(queryString, "model.configuracionComisiones.id=:idConfiguracion");
						params.put("idConfiguracion", configuracionComision.getId());
					}
					else{
						ValorCatalogoAgentes modoEjecucion=configuracionComision.getModoEjecucion();
						if(isNotNull(modoEjecucion) && isNotNull(modoEjecucion.getId())){
							addCondition(queryString, "model.configuracionComisiones.modoEjecucion.id=:idModoEjecucion");
							params.put("idModoEjecucion", modoEjecucion.getId());
						}
						if(isNotNull(configuracionComision.getPagoSinFacturaBoolean()) && configuracionComision.getPagoSinFacturaBoolean()){
							addCondition(queryString, "model.configuracionComisiones.pagoSinFactura=:esPagoSinFactura");
							Integer pagoSinFactura=(configuracionComision.getPagoSinFacturaBoolean())?1:0;
							params.put("esPagoSinFactura",pagoSinFactura);
						}
					}
				}
			}
			String  finalQuery = getQueryString(queryString)+" ORDER BY model.id ASC";
			Query query = entityManager.createQuery(finalQuery);
			setQueryParametersByProperties(query, params);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			list=query.getResultList();
		}
		return list;
	}
	/**
	 * Inserta o actualiza los datos del calculo, se debe de utilizar para registrar
	 * un nuevo calculo, actualizar el calculo o eliminar de forma logica el calculo cambiando
	 * el estatus del mismo
	 * @param calculo
	 * @return
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long saveCalculoComisiones(CalculoComisiones calculo){
		calculo=entidadService.save(calculo);
		return calculo.getId();
	}
	/**
	 * Elimina logicamente un preview del calculo de comisiones
	 * cambiando el estatus a inactivo
	 * @param idCalculo
	 */
	public void deleteCalculoComisiones(Long idCalculo) throws MidasException{
		if(isNull(idCalculo)){
			onError(ERROR_CALCULO_NULO);
		}
		CalculoComisiones persistentObject=entidadService.findById(CalculoComisiones.class, idCalculo);
		if(isNull(persistentObject)){
			onError(ERROR_CALCULO_NOTFOUND);
		}
		try {
			ValorCatalogoAgentes estatus=catalogoService.obtenerElementoEspecifico(CATALOGO_ESTATUS_CALCULO,ESTATUS_CALCULO_CANCELADO);
			persistentObject.setClaveEstatus(estatus);
			//Se actualiza el estatus a eliminar el calculo de comisiones
			entidadService.save(persistentObject);
		} catch (Exception e) {
			onError(e);
		}
	}
	/**
	 * Actualiza el estatus del calculo
	 * @param idCalculo
	 * @param estatus
	 * @throws Exception
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizarEstatusCalculo(Long idCalculo,EstatusCalculoComisiones estatus) throws MidasException{
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del preview de calculo que desea autorizar");
		}
		if(isNull(estatus) || !isValid(estatus.getValue())){
			onError("Favor de proporcionar el estatus a cambiar");
		}
		ValorCatalogoAgentes estatusCalculo=null;
		try{
			estatusCalculo=catalogoService.obtenerElementoEspecifico(CATALOGO_ESTATUS_CALCULO,estatus.getValue());
		}catch(Exception e){
			onError(e);
		}
		if(isNull(estatusCalculo)){
			onError("No existe el estatus "+estatus.getValue()+" como estatus del calculo");
		}
		CalculoComisiones calculo=loadById(idCalculo);
		ValorCatalogoAgentes estatusActual=(isNotNull(calculo) && isNotNull(calculo.getClaveEstatus()))?calculo.getClaveEstatus():null;		
		//Si el estatus es autorizar calculo, entonces se manda a llamar el stored procedure para generar solicitudes de cheques para que aparezca como 
		//cheques pendientes por aplicar en MIZAR
		switch(estatus){
			case CANCELADO:
				//ValorCatalogoAgentes estatusActual=calculo.getClaveEstatus();
				//Si el estatus del calculo es diferente a pendiente por autorizar o recalcular entonces no se puede cambiar su estatus
				if(isNull(estatusActual) ||(!"PENDIENTE POR AUTORIZAR".equals(estatusActual.getValor()) && !"PENDIENTE-RECALCULAR".equals(estatusActual.getValor()))){
					onError("No se puede cancelar el calculo, ya que este ya fue Autorizado y/o Aplicado");
				}
			break;
		}
		calculo.setClaveEstatus(estatusCalculo);
		saveCalculoComisiones(calculo);
		//Una vez que se actualiza el estatus del calculo, se debe de actualizar tambien el detalle de los mismos
		actualizarEstatusDetallePorCalculo(idCalculo, estatus);//Se actualiza el estatus de los detalles
	}
	/**
	 * Actualiza el estatus del detalle de los calculos
	 * @param idCalculo
	 */
	private void actualizarEstatusDetallePorCalculo(Long idCalculo,EstatusCalculoComisiones estatus) throws MidasException{
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo para actualizar el estatus de todos los detalles del mismo");
		}
		if(isNull(estatus)){
			onError("Favor de proporcionar el estatus del calculo a actualizar");
		}
		detalleCalculoComisionesService.actualizarDetallePorCalculo(idCalculo, estatus);
	}
	
	/**
	 * Metodo que genera todas las solicitudes de cheque de cada pago de comisiones que se le hara al agente por medio del id del calculo.
	 * @param cheque
	 * @throws MidasException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void generarSolicitudDeChequePorCalculo(Long idCalculo) {
		
		if (isNull(idCalculo)) {
			
			throw new RuntimeException("Favor de proporcionar el calculo");
			
		}
		
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS."+PKG_INT_MIDAS_E2+".stpCrearSolChequesPorCalculo";
		
		try {
			LogDeMidasInterfaz.log("Entrando a CalculoComisionesDaoImpl.generarSolicitudDeChequePorCalculo..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pIdCalculoComisiones",idCalculo);
			storedHelper.ejecutaActualizar();
			//TODO Modificar procedure para agregar el estatus en el detalle al actualizar 
			LogDeMidasInterfaz.log("Saliendo de CalculoComisionesDaoImpl.generarSolicitudDeChequePorCalculo..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,CalculoComisionesDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de CalculoComisionesDaoImpl.generarSolicitudDeChequePorCalculo..." + this, Level.WARNING, e);
			throw new RuntimeException(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en CalculoComisionesDaoImpl.generarSolicitudDeChequePorCalculo..." + this, Level.WARNING, e);
			throw new RuntimeException(e);
		}
	}

	/**
	 * Elimina logicamente un preview del calculo de comisiones
	 * cambiando el estatus a inactivo
	 * @param calculo
	 */
	public void deleteCalculoComisiones(CalculoComisiones calculo) throws MidasException{
		if(isNull(calculo) || isNull(calculo.getId())){
			onError(ERROR_CALCULO_NULO);
		}
		deleteCalculoComisiones(calculo.getId());
	}
	
	/**
	 * indica si del listado de agentes alguno de ellos ya existe en algun calculo previo pendiente por autorizar, si es asi,
	 * entonces en cada calculo se va a cambiar de estatus a Pendiente-Recalcular, para que al momento de consultar un calculo si
	 * tiene este estatus se pida recalcular. 
	 * @param listaAgenteCalculo
	 * @return
	 * @throws MidasException
	 */
	@SuppressWarnings("unchecked")
	public void validarAgentesEnCalculosPendientes(Long idCalculoTemporal)throws MidasException{
		List<CalculoComisiones> list=new ArrayList<CalculoComisiones>();
		if(isNull(idCalculoTemporal)){
			onError("Favor de proporcionar la clave del calculo temporal");
		}
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select  ");
		queryString.append(" distinct calculo.id, ");
		queryString.append(" calculo.idConfiguracionComisiones as idConfigComisiones,");
		queryString.append(" calculo.fechaCalculo as fechaCalculo,");
		queryString.append(" calculo.numComisionesProcesadas as numComisionesProcesadas,");
		queryString.append(" calculo.importeTotal as importeTotal ");
		queryString.append(" from MIDAS.toCalculoComisiones calculo ");
		queryString.append(" inner join ( ");
		queryString.append(" 	select id,valor from MIDAS.toValorCatalogoAgentes  ");
		queryString.append(" 	WHERE grupoCatalogoAgentes_id in(select id FROM MIDAS.TCGRUPOCATALOGOAGENTES g where descripcion like 'Estatus Calculo Comisiones') ");
		queryString.append(" 	and valor='PENDIENTE POR AUTORIZAR' ");
		queryString.append(" ) estatusCalculoPendiente on (estatusCalculoPendiente.id=calculo.claveEstatus)  ");
		queryString.append(" inner join MIDAS.toDetalleCalculoComisiones detalle on (detalle.idCalculoComisiones=calculo.id) ");
		queryString.append(" inner join MIDAS.GT_AgentesCalculoTemporal listaAgentes on (listaAgentes.idCalculo="+idCalculoTemporal+" and listaAgentes.idRegistro=detalle.idAgente)");
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"calculoComisionesView");
		list=query.getResultList();
		if(!isEmptyList(list)){
			ValorCatalogoAgentes estatusPendienteRecalcular=null;
			try {
				estatusPendienteRecalcular=catalogoService.obtenerElementoEspecifico("Estatus Calculo Comisiones","PENDIENTE-RECALCULAR");
				if(isNull(estatusPendienteRecalcular)){
					onError("No existe el estatus de Pendiente-Recalcular");
				}
			} catch (Exception e) {
				onError(e);
			}
			for(CalculoComisiones calculo:list){
				if(isNotNull(calculo)){
					calculo.setClaveEstatus(estatusPendienteRecalcular);//Se actualiza el estatus de los que estan como pendientes y que corresponden a los agentes del nuevo calculo
					saveCalculoComisiones(calculo);
				}
			}
		}
	}
		
	@Override
	public TransporteImpresionDTO generarReporteComisiones(CalculoComisiones calculoComisiones)
			throws Exception {
//		List<DetalleCalculoComisiones> listaCalculos=detalleCalculoComisionesService.listarDetalleDeCalculo(calculoComisiones);
		Long idCalculo = calculoComisiones.getId();
		List<DetalleCalculoComisionesView> listaCalculos = detalleCalculoComisionesService.listarReporteComisionAgente(idCalculo);
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("P_IMAGE_LOGO",SistemaPersistencia.AFIRME_SEGUROS);	
		params.put("totalComisiones",1000.00);	
		return impresionesService.getExcel(listaCalculos, "midas.agente.reporte.calculo.comisiones.plantilla.archivo.nombre",params,ConstantesReporte.TIPO_XLS);
	}
	
	/**
	 * Muestra el listado de calculos que pueden pagarse a MIZAR. Estos deben de ser los unicos cheques 
	 * marcados en MIZAR como transferencias electronicas
	 * @param filtro Permite filtrar por fechas los calculso pendientes por pagar
	 * @return
	 */
	@Override
	public List<CalculoComisiones> obtenerCalculosPendientesPorPagar(CalculoComisiones filter) throws MidasException{
		List<CalculoComisiones> list=new ArrayList<CalculoComisiones>();
		CalculoComisiones filtro=new CalculoComisiones();
		if(isNotNull(filter)){
			if(isNotNull(filter.getFechaCalculo())){
				filtro.setFechaCalculo(filter.getFechaCalculo());
			}
			if(isNotNull(filter.getFechaCalculoFin())){
				filtro.setFechaCalculoFin(filter.getFechaCalculoFin());
			}
		}
		ValorCatalogoAgentes estatus=null;
		try {
			estatus=catalogoService.obtenerElementoEspecifico(CATALOGO_ESTATUS_CALCULO, EstatusCalculoComisiones.APLICADO.getValue());
			if(isNull(estatus)){
				onError("El estatus de Aplicado no existe en "+CATALOGO_ESTATUS_CALCULO);
			}
		} catch (Exception e) {
			onError(e);
		}
		filtro.setClaveEstatus(estatus);
		list=findByFilters(filtro);
		return list;
	}
	
	/**
	 * Obtiene la lista de configuraciones programadas activas que tienen modo automatico.
	 * @return
	 * @throws MidasException
	 */
	public List<ConfigComisiones> obtenerConfiguracionesAutomaticasActivas() throws MidasException{
		List<ConfigComisiones> list=new ArrayList<ConfigComisiones>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select ");
		queryString.append(" config.id as id,  "); 
		queryString.append(" case when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('DIARIO'))) then ");
		queryString.append(" 	sysdate ");  
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('SEMANAL'))) then  ");
		queryString.append(" 	NEXT_DAY(sysdate,decode(config.diaEjecucion,1,config.diaEjecucion+1,2,config.diaEjecucion+1,3,config.diaEjecucion+1,4,config.diaEjecucion+1,5,config.diaEjecucion+1,6,config.diaEjecucion+1,1)) ");  
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('QUINCENAL'))) then "); 
		queryString.append(" 	DECODE( ");
		queryString.append(" 		MOD(TO_NUMBER(to_date(sysdate) - to_date(config.fechaInicioVigencia)),15) ");
		queryString.append(" 		,0, ");
		queryString.append(" 		sysdate, ");
		queryString.append(" 		sysdate-1"); 
		queryString.append("	)    ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('MENSUAL'))) then "); 
		queryString.append(" 	DECODE( ");
		queryString.append(" 		to_date(ADD_MONTHS(config.fechaInicioVigencia, floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) )), ");
		queryString.append(" 		to_date(sysdate), ");
		queryString.append(" 		sysdate, ");
		queryString.append(" 		sysdate-1 ");
		queryString.append(" 	)    ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('BIMESTRAL')) AND MOD(floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) ,2)=0) then ");
		queryString.append(" 	DECODE( ");
		queryString.append(" 		to_date(ADD_MONTHS(config.fechaInicioVigencia, floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) )), ");
		queryString.append(" 		to_date(sysdate), ");
		queryString.append(" 		sysdate, ");
		queryString.append(" 		sysdate-1");
		queryString.append(" 	)   ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('TRIMESTRAL')) AND MOD(floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) ,3)=0) then ");
		queryString.append(" 	DECODE( ");
		queryString.append(" 		to_date(ADD_MONTHS(config.fechaInicioVigencia, floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) )), ");
		queryString.append(" 		to_date(sysdate), ");
		queryString.append(" 		sysdate, ");
		queryString.append(" 		sysdate-1");
		queryString.append(" 	)   ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('SEMESTRAL')) AND MOD(floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) ,6)=0) then ");
		queryString.append(" 	DECODE( ");
		queryString.append(" 		to_date(ADD_MONTHS(config.fechaInicioVigencia, floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) )), ");
		queryString.append(" 		to_date(sysdate), ");
		queryString.append(" 		sysdate,");
		queryString.append(" 		sysdate-1 ");
		queryString.append(" 	)   ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('ANUAL')) AND MOD(floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) ,12)=0) then ");
		queryString.append(" 	DECODE( ");
		queryString.append(" 		to_date(ADD_MONTHS(config.fechaInicioVigencia, floor(MONTHS_BETWEEN(to_date(sysdate),config.fechaInicioVigencia)) )), ");
		queryString.append(" 		to_date(sysdate), ");
		queryString.append(" 		sysdate, ");
		queryString.append(" 		sysdate-1 ");
		queryString.append(" 	)   ");
		queryString.append(" else  ");
		queryString.append(" 	sysdate-1  ");
		queryString.append(" end as fechaInicioVigencia  ");
		queryString.append(" from MIDAS.toConfigComisiones config, ");
		queryString.append(" MIDAS.tcGrupoCatalogoAgentes gModosEjecucion, ");
		queryString.append(" MIDAS.toValorCatalogoAgentes catModosEjecucion, ");
		queryString.append(" MIDAS.tcGrupoCatalogoAgentes gPeriodo, ");
		queryString.append(" MIDAS.toValorCatalogoAgentes catPeriodo  ");
		queryString.append(" where gModosEjecucion.descripcion like 'Modos de Ejecucion de Comisiones' "); 
		queryString.append(" and gModosEjecucion.id=catModosEjecucion.grupoCatalogoAgentes_id ");
		queryString.append(" and trim(upper(catModosEjecucion.valor))=trim(upper('Automatico')) ");  
		queryString.append(" and config.idModoEjecucion=catModosEjecucion.id ");
		queryString.append(" and gPeriodo.descripcion like 'Periodos de Ejecucion de Comisiones' "); 
		queryString.append(" and gPeriodo.id=catPeriodo.grupoCatalogoAgentes_id ");  
		queryString.append(" and config.idPeriodoEjecucion=catPeriodo.id  ");
		queryString.append(" and config.fechaInicioVigencia<sysdate and config.claveActivo=1 ");
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"configComisionesProgramacionView");
		list=query.getResultList();
		return list;
	}
	
	
	@Override
	public List<CalculoComisionesView> findByFiltersView(CalculoComisiones filtro) {
		List<CalculoComisionesView> lista=new ArrayList<CalculoComisionesView>();
		
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT c.ID, c.FECHACALCULO as fechaCorte, trunc(c.IMPORTETOTAL,2) as importeTotalComisiones, c.NUMCOMISIONESPROCESADAS as numComisionesProcesadas, cat.valor as estatusCalculo");
		queryString.append(" FROM MIDAS.toCalculoComisiones c ");
		queryString.append(" inner join midas.TOCONFIGCOMISIONES config on (config.id= c.IDCONFIGURACIONCOMISIONES) ");	
		queryString.append(" inner join midas.tovalorCatalogoagentes cat on (cat.id=c.claveEstatus) ");
		queryString.append(" where ");
		if(isNotNull(filtro)){
			int index=1;
				SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
				if(filtro.getFechaCalculo()!=null){
					Date fecha=filtro.getFechaCalculo();					
					String fechaString=format.format(fecha);
					if(isValid(fechaString)){
						addCondition(queryString, "TRUNC(FECHACALCULO) >= ?");
						params.put(index, filtro.getFechaCalculo());
						index++;
					}
				}
				if(filtro.getFechaCalculoFin()!=null){
					Date fechaFin=filtro.getFechaCalculoFin();
					String fechaFinString=format.format(fechaFin);
					if(isValid(fechaFinString)){
						addCondition(queryString, "TRUNC(FECHACALCULO) <=?");
						params.put(index, filtro.getFechaCalculoFin());
						index++;
					}
				}
				
				if(filtro.getConfiguracionComisiones()!=null && filtro.getConfiguracionComisiones().getModoEjecucion()!=null && filtro.getConfiguracionComisiones().getModoEjecucion().getId()!=null){
					addCondition(queryString, " config.idModoEjecucion=? ");
					params.put(index, filtro.getConfiguracionComisiones().getModoEjecucion().getId());
					index++;
				}
				
				if(filtro.getConfiguracionComisiones()!=null && filtro.getConfiguracionComisiones().getPagoSinFactura()!=null){
					addCondition(queryString, " config.CLAVEPAGOSINFACTURA=? ");
					params.put(index, filtro.getConfiguracionComisiones().getPagoSinFactura());
					index++;
				}
			
			if(params.isEmpty()){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			String finalQuery=getQueryString(queryString);
			Query query=entityManager.createNativeQuery(finalQuery,CalculoComisionesView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			lista=query.getResultList();		
		}
		return lista;
	}
	
	@Override
	public Long EliminarComisionesCero(Long idCalculo) throws MidasException {
		
		if (idCalculo == null) {
			return 0L;
		}	
			
		CalculoComisiones calculo = entidadService.findById(CalculoComisiones.class, idCalculo);
		
		if (calculo == null) {
			return 0L;
		}
		
		Integer comProcesadas = calculo.getNumComisionesProcesadas(); 
		
		if (comProcesadas.intValue() == 0) {
			
			entidadService.remove(calculo);
			
		}
		
		return comProcesadas.longValue();
		
	}
	
	@Override
	public Integer obtenerNumeroCalculosComisionPrevios (Long idCalculo) {
		
		StoredProcedureHelper storedHelper = null;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS." + PKGCALCULOS_AGENTES + ".stp_conteoCalculosComPrevios", StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro("pIdCalculoComisiones", idCalculo);
			
			return Integer.valueOf(storedHelper.ejecutaActualizar());
			
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void marcaMovimientosAgente(Long idCalculo) {
		
		StoredProcedureHelper storedHelper = null;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS." + PKGCALCULOS_AGENTES + ".stp_marcaMovsComision", StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro("pIdCalculoComisiones", idCalculo);
			
			storedHelper.ejecutaActualizar();
			
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ValorCatalogoAgentes obtenerClaveEstatusCalculo(EstatusCalculoComisiones estatus) throws Exception {

		return catalogoService.obtenerElementoEspecifico(CATALOGO_ESTATUS_CALCULO, estatus.getValue());
		
	}
	
	@Override
	public List<CalculoBonoEjecucionesView> listaCalculoComisionesMonitorEjecucion() throws MidasException
	{
		List<CalculoBonoEjecucionesView> listaComisionesEjecutadas = new ArrayList<CalculoBonoEjecucionesView>();
		StringBuilder queryString =  new StringBuilder();
	
		queryString.append("SELECT e.id, e.descripcion as nombreConfiguracionBono, modo.valor as modoEjecucion ," +
				" est.valor as estatusEjecucion, e.idcalculocomisiones as idPreviewCalculo, e.fechaEjecucion, e.usuario," +
				"e.PCTAVANCEAPROX as avance " +
				" FROM MIDAS.TOCALCULOCOMISIONEJECUCION e" +
				" inner join midas.tovalorcatalogoagentes modo on(e.idmodoEjecucion = modo.id) " +
				" inner join midas.tovalorcatalogoagentes est on(e.idEstatusEjecucion = est.id)" +
				" ORDER BY e.id DESC");
		
		Query query = entityManager.createNativeQuery(queryString.toString(), CalculoBonoEjecucionesView.class);
		listaComisionesEjecutadas = query.getResultList();
		return listaComisionesEjecutadas;
		
	}
	
	
	/**
	 * ==========================================================================
	 * Common methods
	 * ==========================================================================
	 */
	private Object val(Object str){
		return (str!=null)?str:"";
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	@EJB
	public void setConfigComisionesService(ConfigComisionesService configComisionesService) {
		this.configComisionesService = configComisionesService;
	}
	@EJB
	public void setCalculoTemporalService(AgentesCalculoTemporalService calculoTemporalService) {
		this.calculoTemporalService = calculoTemporalService;
	}
	@EJB
	public void setDetalleCalculoComisionesService(DetalleCalculoComisionesService detalleCalculoComisionesService) {
		this.detalleCalculoComisionesService = detalleCalculoComisionesService;
	}
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	@EJB
	public void setSolicitudChequesMizarService(
			SolicitudChequesMizarService solicitudChequesMizarService) {
		this.solicitudChequesMizarService = solicitudChequesMizarService;
	}
	@EJB
	public void setImpresionesService(ImpresionesService impresionesService) {
		this.impresionesService = impresionesService;
	}
	
	@EJB
	public void setRetencionImpuestosService(RetencionImpuestosService retencionImpuestosService) {
		this.retencionImpuestosService = retencionImpuestosService;
	}
	
}
