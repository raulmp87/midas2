package mx.com.afirme.midas2.dao.impl.cobranza.programapago;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.cobranza.programapago.ReciboDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToRecibo;

@Stateless
public class ReciboDaoImpl extends JpaDao<Long, ToRecibo> implements ReciboDao{

}
