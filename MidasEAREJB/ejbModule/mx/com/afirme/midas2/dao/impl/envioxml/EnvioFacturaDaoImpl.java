package mx.com.afirme.midas2.dao.impl.envioxml;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.envioxml.EnvioFacturaDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;

@Stateless
public class EnvioFacturaDaoImpl extends EntidadDaoImpl implements EnvioFacturaDao {
	
	public EnvioFactura guardaEnvio (EnvioFactura envio) {
		
		if (envio.getIdEnvio() == null) {	
			envio.setIdEnvio((Long) persistAndReturnId(envio));
		}
				
		return envio;
		
	}
	
	
	@SuppressWarnings("unchecked")
	public List<EnvioFactura> listarFilradoSolicitudGrid(
			EnvioFactura envioFactura, Date fechaInicial, Date fechaFinal) {
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		StringBuilder queryString = new StringBuilder();

		queryString.append("select model from EnvioFactura model ");
		queryString.append("where model.origenEnvio = :origenEnvio ");	
		
		if(envioFactura.getReaseguroTipo() != null) {
			params.put("origenEnvio", envioFactura.getReaseguroTipo());
		} else {
			params.put("origenEnvio", envioFactura.getOrigenEnvio());
		}
		if (envioFactura.getIdOrigenEnvio() != null) {
			queryString.append("and model.idOrigenEnvio = :idOrigenEnvio ");
			params.put("idOrigenEnvio", envioFactura.getIdOrigenEnvio());
		}
		if (envioFactura.getFolioCfdi() != null && !"".equals(envioFactura.getFolioCfdi().trim()) ) {
			queryString.append("and concat(model.serieCfdi,model.folioCfdi) like :folio ");
			params.put("folio", "%"+envioFactura.getFolioCfdi()+"%");
		}
		if (envioFactura.getRfcEmisorCfdi() != null && !"".equals(envioFactura.getRfcEmisorCfdi().trim()) ){
			queryString.append(" and model.rfcEmisorCfdi like :rfcEmisorCfdi ");
			params.put("rfcEmisorCfdi", "%"+envioFactura.getRfcEmisorCfdi()+"%");
		}
		if(envioFactura.getCuentaAfectada() != null && !"".equals(envioFactura.getCuentaAfectada().trim())) {
			queryString.append("and model.cuentaAfectada = :cuentaAfectada");
			params.put("cuentaAfectada", envioFactura.getCuentaAfectada());
		}
		if (fechaInicial != null && fechaFinal==null) {
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(fechaInicial);
			gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
			gcFechaInicio.set(GregorianCalendar.SECOND, 0);
			gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			queryString.append(" and func('TO_TIMESTAMP', model.fechaCfdi, 'YYYY-MM-DD\"T\"HH24:MI:SS') > func('to_date', :fechaCfdiIni, 'dd/mm/yyyy') ");
			params.put("fechaCfdiIni", Utilerias.cadenaDeFecha(gcFechaInicio.getTime()));
		}else if (fechaFinal != null && fechaInicial==null) {
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(fechaFinal);
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			queryString.append("  and func('TO_TIMESTAMP', model.fechaCfdi, 'YYYY-MM-DD\"T\"HH24:MI:SS') < func('to_date', :fechaCfdiFin, 'dd/mm/yyyy') ");
			params.put("fechaCfdiFin", Utilerias.cadenaDeFecha(gcFechaFin.getTime()));
		}else if(fechaInicial != null && fechaFinal!=null){
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(fechaFinal);
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(fechaInicial);
			gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
			gcFechaInicio.set(GregorianCalendar.SECOND, 0);
			gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			
			queryString.append(" and func('TO_TIMESTAMP', model.fechaCfdi, 'YYYY-MM-DD\"T\"HH24:MI:SS') BETWEEN func('to_date', :fechaCfdiIni, 'dd/mm/yyyy') and func('to_date', :fechaCfdiFin, 'dd/mm/yyyy') ");
			
			params.put("fechaCfdiIni", Utilerias.cadenaDeFecha(gcFechaInicio.getTime()));
			params.put("fechaCfdiFin", Utilerias.cadenaDeFecha(gcFechaFin.getTime()));
		}
		
		queryString.append(" order by model.estatusEnvio desc, model.idEnvio desc ");
		
		Query query = entityManager.createQuery(queryString.toString(),EnvioFactura.class);
		
		for (String key : params.keySet()) {
			query.setParameter(key, params.get(key));
		}
		
		return query.getResultList(); 
	}

}

