package mx.com.afirme.midas2.dao.impl.siniestros.recuperacion;


import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionCiaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania.EstatusCartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CoberturaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso.EstatusIngreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.OrdenCompraCartaCia;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.PaseRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.MedioRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.TipoRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.CartaCiaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionCiaDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.JpaUtil;
import mx.com.afirme.midas2.utils.CommonUtils;

import org.apache.commons.lang.StringUtils;
/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 10-jul-2015 08:08:25 p.m.
 */
@Stateless
public class  RecuperacionCiaDaoImpl extends EntidadDaoImpl implements RecuperacionCiaDao {
	@EJB
	EntidadDao entidadDao;

	@EJB 
	private OrdenCompraService ordenCompraService;

	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private RecepcionFacturaService recepcionFacturaService;
	
	@EJB
	private EntidadService entidadService;
	@EJB
	private ParametroGlobalService parametroGlobalService ;


	/**
	 * Crear un JPQL que obtenga las Ordenes de Compra asociadas a la Estimacion
	 * deseada.
	 * 
	 * El objeto a retornar debe contener lo siguiente:
	 * 
	 * <b>ordenCompra</b>: Orden de Compra asociada
	 * 
	 * <b>subTotalOC</b>: Suma del importe del subtotal de los Detalles de la OC
	 * 
	 * <b>conceptoDesc</b>: Si tiene mas de un concepto asociado debe mostrar la
	 * palabra VARIOS, de lo contario nel nombre del concepto asociado.
	 * 
	 * <b>subTotalRecupProveedor</b>: Buscar si la OC esta asociada a una Recuperaci�n
	 * de Proveedor que se encuentre en estatus RECUPERADO.
	 * 
	 * <b>porcentajeParticipacion</b>: Es el que se recibe de par�metro:
	 * 
	 * <b>montoARecuperar</b>: (SubTotalOc- <b>subTotalRecupProveedor ) *
	 * porcentajeParticipacion</b>
	 * 
	 * Si el par�metro ordenesConcat no es nulo, quiere decir que se deben marcar
	 * estas ordenes de compas como <b>seleccionadas = true</b>
	 * 
	 * Si el par�metro <b>soloSeleccionados </b>es true, entonces solo filtrar por
	 * estos ID
	 * 
	 * @param estimacionId
	 * @param ordenesConcat
	 * @param porcentajeParticipacion
	 * @param soloSeleccionados
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenCompraCartaCia>obtenerOCestimacion(Long recuperacionId, Long reporteCabinaId, Long coberturaId, Long estimacionId, 
			String ordenesConcat, Integer porcentajeParticipacion, Boolean soloSeleccionados) {
		if(porcentajeParticipacion==null){
			porcentajeParticipacion=0;
		}
		
		List<OrdenCompraCartaCia> LstOrdenCompraCartaCia  = new ArrayList<OrdenCompraCartaCia>();
		StringBuilder srtOrdenesCompraId=new StringBuilder("");
		Map<Long, Long> mapOrdenesConcat = new HashMap<Long, Long>();
		if(!StringUtil.isEmpty(ordenesConcat)){
			ordenesConcat= ordenesConcat.trim();
			String[] lstOrdenesConcat=  ordenesConcat.split(",") ;
			int contador = 0;
			for (String id: lstOrdenesConcat){				 
				if(!StringUtil.isEmpty(id)){
					Long ordenId=new Long (id.trim());
					if (contador ==0) 
						srtOrdenesCompraId.append(ordenId);
					else  
						srtOrdenesCompraId.append(", ").append(ordenId);
						contador++;
						mapOrdenesConcat.put(ordenId,ordenId);		              
				}
			}
		}

		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		List<OrdenCompra> ordenesCompras  = new ArrayList<OrdenCompra>();
		
		queryString = new StringBuilder("SELECT compra from " + OrdenCompra.class.getSimpleName() + " compra ");
		if (!soloSeleccionados) {	
			queryString.append(" WHERE compra.estatus = :estatusPagada ");
			if ( reporteCabinaId != null){
				queryString.append(" AND compra.reporteCabina.id = :reporteCabinaId");
				Utilerias.agregaHashLista(listaParametrosValidos, "reporteCabinaId", reporteCabinaId);
			}	
			
			queryString.append(" AND ((compra.tipo = :tipoReserva ");
			
			if( coberturaId != null) {			
				queryString.append(" AND compra.coberturaReporteCabina.id = :coberturaId");		
				Utilerias.agregaHashLista(listaParametrosValidos, "coberturaId", coberturaId);
			}
			if (estimacionId != null) {			
				queryString.append(" AND compra.idTercero = :estimacionId");	
				Utilerias.agregaHashLista(listaParametrosValidos, "estimacionId", estimacionId);
			}
			
			queryString.append(" )")
			.append(" OR (compra.tipo = :tipoGasto AND compra.tipoProveedor = :tipoGrua) )");
			
			Utilerias.agregaHashLista(listaParametrosValidos, "estatusPagada", OrdenCompraService.ESTATUS_PAGADA);
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoReserva", OrdenCompraService.TIPO_AFECTACION_RESERVA);
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoGasto", OrdenCompraService.TIPO_GASTOAJUSTE);
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoGrua", "GRUA");
			
		}else{
			if(StringUtil.isEmpty(srtOrdenesCompraId.toString())){
				return null;
			}else{
				queryString.append(" WHERE compra.id  in ( "+srtOrdenesCompraId.toString()+")");			
			}

		}
		
		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		ordenesCompras =  query.getResultList();
		
		for( OrdenCompra ordenCompra : ordenesCompras ){
			if(this.validaSiLaOrdenCompraEsAsociableRecuperacionCia(recuperacionId, ordenCompra)){
				ImportesOrdenCompraDTO importe =ordenCompraService.calcularImportes(ordenCompra.getId(), null, false);
				OrdenCompraCartaCia dto = new OrdenCompraCartaCia();
				if(mapOrdenesConcat.containsKey(ordenCompra.getId())){
					dto.setSeleccionado(Boolean.TRUE);
				}else{
					dto.setSeleccionado(Boolean.FALSE);
				}
				if(ordenCompra.getIndemnizacion()!=null){
					dto.setIsIndemnizacion(true);
				}else{
					dto.setIsIndemnizacion(false);
				}
				
				BigDecimal subtotal= importe.getSubtotal();
				BigDecimal subtotalRecuperacion= BigDecimal.ZERO;
				if(!StringUtil.isEmpty(ordenCompra.getDetalleConceptos())){
					String[] conceptos=  ordenCompra.getDetalleConceptos().split(",") ;
					if(conceptos.length>1){
						dto.setConceptoDesc("VARIOS.");
					}
					else {
						dto.setConceptoDesc(ordenCompra.getDetalleConceptos());
	
					}
				}
				
				if (StringUtils.equals(OrdenCompraService.TIPO_AFECTACION_RESERVA, ordenCompra.getTipo())) {
					dto.setTipoOcDesc("Reserva");
				} else if (StringUtils.equals(OrdenCompraService.TIPO_GASTOAJUSTE, ordenCompra.getTipo())) {
					dto.setTipoOcDesc("Gasto Ajuste Grúa");
				}
	
	
				dto.setMontoARecuperar(subtotal);
				dto.setOrdenCompra(ordenCompra);
				Map<String, Object> param = new HashMap<String, Object>();	
				param.put("ordenCompra.id", ordenCompra.getId());
				param.put("estatus", Recuperacion.EstatusRecuperacion.RECUPERADO.toString());
				List<RecuperacionProveedor>  lstRecuperacion  = this.entidadDao.findByProperties(RecuperacionProveedor.class, param);
				for(RecuperacionProveedor recuperacion :lstRecuperacion ){
					if( recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.REEMBOLSO.toString()) ){
						if(null!=recuperacion.getMontoTotal())
							subtotalRecuperacion=subtotalRecuperacion.add(recuperacion.getSubTotal());
					}else if( recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.VENTA.toString()) ){
						if(null!=recuperacion.getMontoVenta())
							subtotalRecuperacion=subtotalRecuperacion.add(recuperacion.getSubTotalVenta());
					}
				}
				dto.setPorcentajeParticipacion(porcentajeParticipacion);
				BigDecimal porcentaje = new BigDecimal (porcentajeParticipacion);
				BigDecimal multiplo = porcentaje.divide(new BigDecimal (100));
				dto.setRecuperacion(null);
				dto.setSubtotalOC(subtotal);
				dto.setMontoARecuperar(subtotal.multiply(multiplo));
				dto.setSubTotalRecupProveedor(subtotalRecuperacion);
				dto.setMontoPiezasConPorcentaje(subtotalRecuperacion.multiply(multiplo));
				LstOrdenCompraCartaCia.add(dto);
			}
		}
		return LstOrdenCompraCartaCia;

	}
	
	
	private Boolean validaSiLaOrdenCompraEsAsociableRecuperacionCia(Long recuperacionId, OrdenCompra ordenCompra){
		Boolean respuesta = Boolean.TRUE;
		OrdenCompraCartaCia ocCia = obtenerOrdenCompraCartaCia(ordenCompra.getId());
		if(ocCia==null){
			respuesta = Boolean.TRUE;
		}else if(recuperacionId!=null){
			respuesta = (ocCia.getRecuperacion().getId().longValue()==recuperacionId.longValue())?Boolean.TRUE:Boolean.FALSE;
		}
		/*OrdenCompraCartaCia ocCia = obtenerOrdenCompraCartaCia(ordenCompra.getId());
			if(ocCia!=null){ 
				if(recuperacionId!=null){
					respuesta = (ocCia.getRecuperacion().getId().longValue()==recuperacionId.longValue())?Boolean.TRUE:Boolean.FALSE;
				}else{
					respuesta = Boolean.FALSE;
				}
			}*/
		return respuesta;
	}
	
	
	private OrdenCompraCartaCia obtenerOrdenCompraCartaCia(Long ordenCompraId){
		OrdenCompraCartaCia ocCia = null;
		List<OrdenCompraCartaCia>  lista=this.entidadDao.findByProperty(OrdenCompraCartaCia.class, "ordenCompra.id", ordenCompraId);
		if(lista!=null && !lista.isEmpty()){
			ocCia = lista.get(0);
		}
		return ocCia;
	}
	/**
	 * <b>SE DEFINE EN DISE�O DE MOSTRAR RECUPERACIONES CIA POR VENCER</b>
	 * 
	 * M�todo que obtiene las Recuperaciones de Compa��a que estan pr�ximas a vencer
	 * en base a la Fecha L�mite de Cobro.
	 * 
	 * Hacer un JPQL sobre RecuperacionCompania pero los siguientes datos transient
	 * sedeben obtener de la siguiente manera:
	 * 
	 * diasVencimiento: Obtener la ultima Carta Compania que tengas estatus REGISTRADO
	 * O PEND_ELAB
	 * 
	 * Condicionar que la diferencia entre la Fecha Limite de Cobro y la Fecha Actual
	 * es de menor o igual a 90 dias y que tenga una Carta Cia en estatus REGISTRADO o
	 * PEND_ELAB
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RecuperacionCompania> obtenerRecuperacionesVencer() {
		List<RecuperacionCompania> LstRecuperacionCompania  = new ArrayList<RecuperacionCompania>();
		List<RecuperacionCompania> lista  = new ArrayList<RecuperacionCompania>();
		Map<String, Object> parameters = new HashMap<String, Object>();	
		
		int diasVencimiento = this.diasVencimiento();
		
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT recuperacion from " + RecuperacionCompania.class.getSimpleName() + " recuperacion ");
		queryString.append(" WHERE  recuperacion.estatus  in (:recuperacionRegistrado, :recuperacionPendiente) ");
		//queryString.append(" AND  ((TRUNC (  recuperacion.fechaLimiteCobro ) - TRUNC (:fechaActual))   <= 90 )");
		//queryString.append(" AND  ((TRUNC (  recuperacion.fechaLimiteCobro ) - TRUNC (SYSDATE))   <= 90 )");
		queryString.append(" AND FUNC('MIDAS.FN_DAYS_DIFFERENCE', recuperacion.fechaLimiteCobro, :fechaActual) <= :diasVencimiento ");
		queryString.append(" AND EXISTS  (");
		queryString.append(" Select carta.recuperacion.id from " + CartaCompania.class.getSimpleName() + " carta ");
		queryString.append(" WHERE  carta.estatus  in ( :cartaRegistrada, :cartaPendiente) ");
		queryString.append(" AND carta.recuperacion.id = recuperacion.id)");

		queryString.append(" Order by recuperacion.fechaLimiteCobro Desc ");
		
		parameters.put("recuperacionRegistrado", Recuperacion.EstatusRecuperacion.REGISTRADO.toString());
		parameters.put("recuperacionPendiente", Recuperacion.EstatusRecuperacion.PENDIENTE.toString());
		parameters.put("cartaRegistrada", CartaCompania.EstatusCartaCompania.REGISTRADO.toString());
		parameters.put("cartaPendiente", CartaCompania.EstatusCartaCompania.PENDIENTE_ELABORACION.toString());
		parameters.put("cartaPendiente", CartaCompania.EstatusCartaCompania.PENDIENTE_ELABORACION.toString());
		parameters.put("fechaActual", new Date());
		parameters.put("diasVencimiento", diasVencimiento);
		
		lista = entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		
		
		for( RecuperacionCompania recuperacion : lista ){
			recuperacion.setFechaModificacion(new Date());
			SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
			String fechaInicial=sdf.format(recuperacion.getFechaLimiteCobro()); ;
			String fechaFinal=sdf.format(new Date()) ;
			int dif =DateUtils.diferenciaDiasFechas(fechaFinal , fechaInicial);
			recuperacion.setDiasVencimiento(dif);
			if (dif <= diasVencimiento){
				LstRecuperacionCompania.add(recuperacion);
			}

		}
		return LstRecuperacionCompania;

	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RecuperacionCompania> buscarRecuperacionesCia(RecuperacionCiaDTO recuperacionCiaFiltro){		
		Map<String, Object> parameters = llenarFiltros(recuperacionCiaFiltro);
		Map<String, Object> parameters2 = new HashMap<String, Object>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania( "+
				" e.numero,e.id,carta, e.fechaCreacion, e.compania, e.reporteCabina, e.referenciaBancaria, "+
				" e.estatus, e.importe, e.codigoUsuarioCreacion, coberturas , e.oficinaReasignada  "+
				") "+
				" from " + RecuperacionCompania.class.getSimpleName() + " e ");
		queryString.append(" JOIN e.reporteCabina rep ");
		queryString.append(" LEFT JOIN rep.siniestroCabina sin ");
		queryString.append(" LEFT JOIN e.cartas carta ");
		queryString.append(" LEFT JOIN e.coberturas coberturas ");
		queryString.append(" WHERE  e.estatus in ('" + EstatusRecuperacion.REGISTRADO.getValue() + "','" + EstatusRecuperacion.PENDIENTE.getValue() + "') ");
		queryString.append(" AND  e.medio = '" +  MedioRecuperacion.REEMBOLSO.toString() + "'");


		for(Map.Entry<String, Object> entry : parameters.entrySet()){
			if(entry.getValue().getClass().getName().equals("java.lang.String")){
				queryString.append(" AND " + entry.getKey() + " LIKE '%" + entry.getValue() + "%'");
			}else{
				queryString.append(" AND " + entry.getKey() + " = " + entry.getValue() + " ");
			}
		}
		
		
		if(recuperacionCiaFiltro.getTipoModulo() != null
				&& recuperacionCiaFiltro.getTipoModulo().equals(new Short("1"))){
			queryString.append(" AND  e.referenciaBancaria IS NULL ");
		}

		if(CommonUtils.isNotNull(recuperacionCiaFiltro.getFechaIniRegistro())){
			JpaUtil.addTruncParameter(queryString, "e.fechaCreacion", recuperacionCiaFiltro.getFechaIniRegistro() , "fechaIniRegistro", parameters, JpaUtil.GREATEROREQUAL);
			parameters2.put("fechaIniRegistro", recuperacionCiaFiltro.getFechaIniRegistro());
		}
		if(CommonUtils.isNotNull(recuperacionCiaFiltro.getFechaFinRegistro())){
			JpaUtil.addTruncParameter(queryString, "e.fechaCreacion", recuperacionCiaFiltro.getFechaFinRegistro() , "fechaFinRegistro", parameters, JpaUtil.LESSOREQUAL);
			parameters2.put("fechaFinRegistro", recuperacionCiaFiltro.getFechaFinRegistro());
		}

		if(CommonUtils.isNotNull(recuperacionCiaFiltro.getMontoInicial())){
			JpaUtil.addParameter(queryString, "e.importe", recuperacionCiaFiltro.getMontoInicial() , "montoInicial", parameters, JpaUtil.GREATEROREQUAL);
			parameters2.put("montoInicial", recuperacionCiaFiltro.getMontoInicial());
		}
		if(CommonUtils.isNotNull(recuperacionCiaFiltro.getMontoFinal())){
			JpaUtil.addParameter(queryString, "e.importe", recuperacionCiaFiltro.getMontoFinal() , "montoFinal", parameters, JpaUtil.LESSOREQUAL);
			parameters2.put("montoFinal", recuperacionCiaFiltro.getMontoFinal());
		}

		queryString.append(" Order by e.fechaCreacion Desc ");
		System.out.println(queryString);
		return  entidadDao.executeQueryMultipleResult(queryString.toString(), parameters2);
	}


	private Map<String, Object> llenarFiltros(RecuperacionCiaDTO recuperacionCiaFiltro){
		Map<String, Object> parameters = new HashMap<String, Object>();
		if(CommonUtils.isNotNullNorZero(recuperacionCiaFiltro.getNumeroRecuperacion()))
			parameters.put("e.numero", recuperacionCiaFiltro.getNumeroRecuperacion());
		if(CommonUtils.isValid(recuperacionCiaFiltro.getTipoRecuperacion()))
			parameters.put("e.tipo", recuperacionCiaFiltro.getTipoRecuperacion());
		if(CommonUtils.isValid(recuperacionCiaFiltro.getMedioRecuperacion()))
			parameters.put("e.medio", recuperacionCiaFiltro.getMedioRecuperacion());
		if(CommonUtils.isValid(recuperacionCiaFiltro.getEstatus()))
			parameters.put("e.estatus", recuperacionCiaFiltro.getEstatus());
		if(CommonUtils.isNotNullNorZero(recuperacionCiaFiltro.getOficinaId()))
			parameters.put("e.reporteCabina.oficina.id", recuperacionCiaFiltro.getOficinaId());
		if(CommonUtils.isValid(recuperacionCiaFiltro.getTipoServicioPoliza()) && recuperacionCiaFiltro.getTipoServicioPoliza().equals(Recuperacion.TipoServicioPoliza.PUBLICO.toString()))
			parameters.put("e.reporteCabina.poliza.cotizacionDTO.tipoCotizacion", CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO);
		if(CommonUtils.isValid(recuperacionCiaFiltro.getNumeroCia()))
			parameters.put("e.compania.id", new Integer(recuperacionCiaFiltro.getNumeroCia()));
		if(CommonUtils.isValid(recuperacionCiaFiltro.getCompania()))
			parameters.put("e.compania.personaMidas.nombre", recuperacionCiaFiltro.getCompania());
		if(CommonUtils.isValid(recuperacionCiaFiltro.getUsuario()))
			parameters.put("e.codigoUsuarioCreacion", recuperacionCiaFiltro.getUsuario());
		if(CommonUtils.isValid(recuperacionCiaFiltro.getEstatusCarta()))
			parameters.put("carta.estatus", recuperacionCiaFiltro.getEstatusCarta());
		if(CommonUtils.isValid(recuperacionCiaFiltro.getReferenciaBancaria())){
			if(recuperacionCiaFiltro.getTipoModulo() == null
					|| !recuperacionCiaFiltro.getTipoModulo().equals(new Short("1"))){
				parameters.put("e.referenciaBancaria", recuperacionCiaFiltro.getReferenciaBancaria());
			}
		}
		if(CommonUtils.isValid(recuperacionCiaFiltro.getNumeroSiniestro()))
			parameters.put("CONCAT(sin.claveOficina, '-', sin.consecutivoReporte, '-', sin.anioReporte)", recuperacionCiaFiltro.getNumeroSiniestro());		
		if(CommonUtils.isNotNullNorZero(recuperacionCiaFiltro.getOficinaRecepcionId()))
			parameters.put("carta.oficinaRecepcion.id", recuperacionCiaFiltro.getOficinaRecepcionId());
		if(CommonUtils.isNotNullNorZero(recuperacionCiaFiltro.getOficinaReasignacionId()))
			parameters.put("e.oficinaReasignada.id", recuperacionCiaFiltro.getOficinaReasignacionId());
		if(CommonUtils.isNotNullNorZero(recuperacionCiaFiltro.getReporteCabinaId()))
			parameters.put("e.reporteCabina.id", recuperacionCiaFiltro.getReporteCabinaId());
		return parameters;
	}

	/**
	 * 
	Método que obtiene las Compañías que tienen asociadas Recuperaciones de Tipo Compañía que contienen Cartas Elaboradas con Fecha de Acuse y que aun no han sido Pagadas(Recuperadas)
	 * @return
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<PrestadorServicio> obtenerCiasPendientePago() {

		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT compania from " + PrestadorServicio.class.getSimpleName() + " compania ")
		.append(" WHERE  compania.id  IN ( ")
		.append(" SELECT prestador.id FROM ").append(PrestadorServicio.class.getSimpleName()).append(" prestador")
		.append(" JOIN ").append(RecuperacionCompania.class.getSimpleName())
		.append(" recuperacioncia ON recuperacioncia.compania.id = prestador.id")
		.append(" JOIN ").append(CartaCompania.class.getSimpleName())
		.append(" carta ON carta.recuperacion.id = recuperacioncia.id ")
		.append(" WHERE recuperacioncia.estatus NOT IN( :estatusRecuperado, :estatusCancelado)")
		.append(" AND carta.folio = (SELECT MAX(cartacia.folio) FROM ").append(CartaCompania.class.getSimpleName())
		.append(" cartacia WHERE cartacia.recuperacion.id = recuperacioncia.id)")
		.append(" AND carta.fechaAcuse IS NOT NULL)");


		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRecuperado", EstatusRecuperacion.RECUPERADO.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusCancelado", EstatusRecuperacion.CANCELADO.toString());

		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);


		return query.getResultList();


	}


	/**
	 * Método que obtiene las Cartas Compañías actuales que cuentes con fecha de acuse y 
	 * cuya recuperacion no se encuentre en estatus Recuperado o Cancelado.
	 * @param companiaId
	 * @return
	 */
	@Override
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<CartaCompania> obtenerCartasCiaSinPago(Long companiaId) {

		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT carta from " + CartaCompania.class.getSimpleName() + " carta ")
		.append(" JOIN ").append(RecuperacionCompania.class.getSimpleName())
		.append(" recuperacioncia ON carta.recuperacion.id = recuperacioncia.id ")
		.append(" WHERE recuperacioncia.estatus NOT IN( :estatusRecuperado, :estatusCancelado)")
		.append(" AND carta.folio = (SELECT MAX(cartacia.folio) FROM ").append(CartaCompania.class.getSimpleName())
		.append(" cartacia WHERE cartacia.recuperacion.id = recuperacioncia.id)")
		.append(" AND carta.fechaAcuse IS NOT NULL")
		.append(" AND carta.estatus = :estatusEntregado")
		.append(" AND recuperacioncia.compania.id = :companiaid");


		Utilerias.agregaHashLista(listaParametrosValidos, "companiaid", companiaId);
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusEntregado", EstatusCartaCompania.ENTREGADO.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRecuperado", EstatusRecuperacion.RECUPERADO.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusCancelado", EstatusRecuperacion.CANCELADO.toString());

		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);


		return query.getResultList();


	}
	/**
	 * Método que obtiene el listado de Cartas de Compañía que necesitan ser Redocumentadas 
	 * que cuenten con más de 90 días sin Pago desde la última Fecha de Acuse
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<CartaCompania> obtenerCartasARedoc() {
		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT carta from " + CartaCompania.class.getSimpleName() + " carta ")
		.append(" JOIN ").append(RecuperacionCompania.class.getSimpleName())
		.append(" recuperacioncia ON carta.recuperacion.id = recuperacioncia.id ")
		.append(" WHERE recuperacioncia.estatus = :estatusRegistrado")
		.append(" AND carta.folio = (SELECT MAX(cartacia.folio) FROM ").append(CartaCompania.class.getSimpleName())
		.append(" cartacia WHERE cartacia.recuperacion.id = recuperacioncia.id)")
		.append(" AND carta.fechaAcuse IS NOT NULL")
		.append(" AND carta.estatus = :estatusEntregado")
		.append(" AND func('MIDAS.PKGSIN_RECUPERACIONES.GET_DIFERENCIA_DIAS', carta.fechaAcuse, :fechaActual) >= 90");
		Utilerias.agregaHashLista(listaParametrosValidos, "fechaActual", new Date());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusEntregado", EstatusCartaCompania.ENTREGADO.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistrado", EstatusRecuperacion.REGISTRADO.toString());
		query = entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		return query.getResultList();

	}
	
	/**
	 * Método que obtiene las Cartas Compañía pendientes a Elaborar .donde hayan pasado 15 días 
	 * desde que se registró el último pago del Pase de Atención y la Reserva sea cero
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<CartaCiaDTO> obtenerCartasAElaborar() {
		List<CartaCiaDTO> lista  = new ArrayList<CartaCiaDTO>();
		String spName = "MIDAS.PKGSIN_RECUPERACIONES.obtenerCartasAElaborar"; 
		StoredProcedureHelper storedHelper 			= null;
		try {

			String propiedades = "cartaId, oficinaDesc, numeroRecuperacion, fechaSiniestro, siniestro, nombreCobertura, folioPase, nombreCia, importe";

			String columnasBaseDatos = "cartaid, oficina, numeroRecuperacion, fechaSiniestro, numeroSiniestro, nombreCobertura, folio, nombreCompania, importe";

			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(CartaCiaDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			
			lista = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion busqueda recuperaciones ..."+descErr
						, Level.WARNING, e);    
			}
		}


		return lista;
		
	}
	
	
	@Override
	public List<CartaCompania> obtenerCartasRedoc(Long recuperacionId) {
		List<CartaCompania>  lstCartaCompania= new ArrayList<CartaCompania>();		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("recuperacion.id", recuperacionId);
		List<CartaCompania>  lista=this.entidadDao.findByPropertiesWithOrder(CartaCompania.class, params, "folio asc");	
		for(CartaCompania carta : lista){
			if(!StringUtil.isEmpty(carta.getEstatus()) &&  ( carta.getEstatus().equalsIgnoreCase(CartaCompania.EstatusCartaCompania.RECHAZADO.codigo) || 
					carta.getEstatus().equalsIgnoreCase(CartaCompania.EstatusCartaCompania.ENTREGADO.codigo) )	){
				CartaCompania cartaCia=new CartaCompania();
				String motivo = catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.MOTIVO_RECHAZO_CARTACIA, carta.getCausaRechazo());
				motivo+=" "+catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.MOTIVO_EXCUSION_CARTACIA, carta.getMotivoExclusion());
				String estatus= catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.ESTATUS_CARTA_CIA, carta.getEstatus());
				String rechazo=catalogoGrupoValorService.obtenerDescripcionValorPorCodigo(TIPO_CATALOGO.TIPO_RECHAZO_RED_CIA, carta.getTipoRechazo());
				cartaCia.setMotivoExclusion(motivo );
				cartaCia.setEstatus(estatus);	
				cartaCia.setTipoRechazo(rechazo);
				cartaCia.setRecuperacion(carta.getRecuperacion());
				cartaCia.setMontoARecuperar(carta.getMontoARecuperar());
				cartaCia.setFolio(carta.getFolio());	
				cartaCia.setFechaImpresion(carta.getFechaImpresion());
				cartaCia.setFechaAcuse(carta.getFechaAcuse());
				cartaCia.setFechaCreacion(carta.getFechaCreacion());
				cartaCia.setMontoExclusion(carta.getMontoExclusion());
				cartaCia.setFechaRechazo(carta.getFechaRechazo());
				cartaCia.setFechaElaboracion(carta.getFechaElaboracion());
				cartaCia.setId(carta.getId());
				lstCartaCompania.add(cartaCia);
				
			}
		}
		return lstCartaCompania;
	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<String> obtenerReferenciasIdentificadas() {
		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT DISTINCT(recuperacioncia.referenciaBancaria) FROM ").append(Ingreso.class.getSimpleName()).append(" ingreso ")
		.append(" INNER JOIN ").append(RecuperacionCompania.class.getSimpleName()).append(" recuperacioncia  ON ingreso.recuperacion.id = recuperacioncia.id ")
		.append(" WHERE recuperacioncia.estatus IN (:estatusRegistrado, :estatusPendiente)")
		.append(" AND ingreso.estatus = :estatusIngresoPendiente ")
		.append(" AND recuperacioncia.tipo = :tipoCompania ")
		.append(" AND recuperacioncia.referenciaBancaria IS NOT NULL ");
		
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusIngresoPendiente", EstatusIngreso.PENDIENTE.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusPendiente", EstatusRecuperacion.PENDIENTE.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoCompania", TipoRecuperacion.COMPANIA.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistrado", EstatusRecuperacion.REGISTRADO.toString());

		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

		return query.getResultList();
		
	}/*
	
	public List<RecuperacionCompania> obtenerRecuperacionesAfectadas() {
		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT recuperacioncia FROM " + RecuperacionCompania.class.getSimpleName() + " recuperacioncia ")
		.append(" JOIN CoberturaRecuperacion coberturaCia ON coberturaCia.recuperacion.id = recuperacioncia.id")		
		.append(" WHERE recuperacioncia.estatus IN (:estatusRegistrado, :estatusPendiente)")		
		.append(" AND coberturaCia.coberturaReporte.id = :coberturaId ")
		.append(" AND coberturaCia.claveSubCalculo = :claveSubCalculo");
		
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusPendiente", ESTATUSRECUPERACION.PENDIENTE.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistrado", ESTATUSRECUPERACION.REGISTRADO.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "coberturaId", compania.getCoberturaReporte().getId());
		Utilerias.agregaHashLista(listaParametrosValidos, "claveSubCalculo", compania.getClaveSubCalculo());

		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

		return query.getResultList();
		
	}	*/
	
	/**
	 * Método que obtiene el listado de Recuperaciones asociadas a la Compania y Cobertura de la Recuperacion donde ha cambiado el 
	 * porcentaje de participacion las cuales deberan recalcular sus montos
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RecuperacionCompania> obtenerRecuperacionesActualizables(Long estimacionId, Integer companiaId) {
		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT recuperacioncia from " + RecuperacionCompania.class.getSimpleName() + " recuperacioncia ")
		.append(" JOIN ").append(CartaCompania.class.getSimpleName()).append(" carta ON carta.recuperacion.id = recuperacioncia.id ")
		.append(" JOIN ").append(CoberturaRecuperacion.class.getSimpleName()).append(" cobertura ON cobertura.recuperacion.id = recuperacioncia.id ")
		.append(" JOIN ").append(PaseRecuperacion.class.getSimpleName()).append(" pase ON cobertura.id= pase.coberturaRecuperacion.id ")
		.append(" WHERE recuperacioncia.estatus = :estatusRegistrado ")
		.append(" AND pase.estimacionCobertura.id = :estimacionId")
		.append(" AND carta.folio = (SELECT MAX(cartacia.folio) FROM ").append(CartaCompania.class.getSimpleName())
		.append(" cartacia WHERE cartacia.recuperacion.id = recuperacioncia.id)")
		.append(" AND carta.estatus IN(:estatusRegistradoCarta, :estatusPendElaboracion)");


		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistrado", EstatusRecuperacion.REGISTRADO.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistradoCarta", EstatusCartaCompania.REGISTRADO.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusPendElaboracion", EstatusCartaCompania.PENDIENTE_ELABORACION.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estimacionId", estimacionId);

		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);

		return query.getResultList();

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RecuperacionCompania> obtenerRecuperacionActiva(Long estimacionId) {
		
		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT recuperacion from " + RecuperacionCompania.class.getSimpleName() + " recuperacion ")
		.append(" WHERE recuperacion.numero IN(")
		.append(" SELECT MAX(recuperacioncia.numero) from " + RecuperacionCompania.class.getSimpleName() + " recuperacioncia ")
		.append(" JOIN ").append(CoberturaRecuperacion.class.getSimpleName()).append(" cobertura ON cobertura.recuperacion.id = recuperacioncia.id ")
		.append(" JOIN ").append(PaseRecuperacion.class.getSimpleName()).append(" pase ON cobertura.id= pase.coberturaRecuperacion.id ")
		.append(" WHERE pase.estimacionCobertura.id = :estimacionId ")
		.append(" AND recuperacioncia.esComplemento = false")
		.append(" ) ");


		Utilerias.agregaHashLista(listaParametrosValidos, "estimacionId", estimacionId);
		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);


		return query.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<RecuperacionCompania> obtenerRecuperacionPendientePorRecuperar(Long estimacionId) {
		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString =null;
		queryString = new StringBuilder(" SELECT recuperacioncia from " + RecuperacionCompania.class.getSimpleName() + " recuperacioncia ")
		.append(" JOIN ").append(CoberturaRecuperacion.class.getSimpleName()).append(" cobertura ON cobertura.recuperacion.id = recuperacioncia.id ")
		.append(" JOIN ").append(PaseRecuperacion.class.getSimpleName()).append(" pase ON cobertura.id= pase.coberturaRecuperacion.id ")
		.append(" WHERE pase.estimacionCobertura.id = :estimacionId ")
		.append(" AND recuperacioncia.estatus in (:estatusRegistrado , :estatusPendiente)");
		
		Utilerias.agregaHashLista(listaParametrosValidos, "estimacionId", estimacionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistrado", Recuperacion.EstatusRecuperacion.REGISTRADO.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusPendiente", Recuperacion.EstatusRecuperacion.PENDIENTE.getValue());
		query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		return query.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenCompraCartaCia> obtenerOrdenesCompraCia(Long cartaCiaId){
		
		List<OrdenCompraCartaCia> LstOrdenCompraCartaCia  = new ArrayList<OrdenCompraCartaCia>();
		
		CartaCompania carta = entidadService.findById(CartaCompania.class, cartaCiaId);
		RecuperacionCompania recuperacionCia = carta.getRecuperacion();
		
		EstimacionCoberturaReporteCabina estimacion = null;
		
		CoberturaReporteCabina cobertura = null;
		
		if (recuperacionCia.getCoberturas() != null && !recuperacionCia.getCoberturas().isEmpty() 
				&& recuperacionCia.getCoberturas().get(0).getPasesAtencion() != null
				&& !recuperacionCia.getCoberturas().get(0).getPasesAtencion().isEmpty()) {
			estimacion = recuperacionCia.getCoberturas().get(0).getPasesAtencion().get(0).getEstimacionCobertura();
		}
		
		if (recuperacionCia.getCoberturas() != null && !recuperacionCia.getCoberturas().isEmpty()
				&& recuperacionCia.getCoberturas().get(0) != null && recuperacionCia.getCoberturas().get(0).getCoberturaReporte() != null) {
			cobertura = recuperacionCia.getCoberturas().get(0).getCoberturaReporte();
		}
		
		Map<String, Object> parameters = new HashMap<String, Object>();	
		List<OrdenCompra> ordenesCompras  = new ArrayList<OrdenCompra>();
		StringBuilder queryString =null;
		queryString = new StringBuilder("SELECT compra from " + OrdenCompra.class.getSimpleName() + " compra ");;
		
		queryString.append(" WHERE  compra.estatus  in ( ");
		queryString.append(" '"+OrdenCompraService.ESTATUS_PAGADA+"' ");
		queryString.append(" ) ");			
		queryString.append(" AND compra.tipo  in ( '"+OrdenCompraService.TIPO_AFECTACION_RESERVA+"' )");
		//queryString.append(" AND compra.tipoPago  in ( '"+OrdenCompraService.PAGO_A_PROVEEDOR+"' )");

		if(null!=cobertura.getId()){			
			queryString.append(" AND compra.coberturaReporteCabina.id  in ( "+cobertura.getId()+")");			
		}else{
			return LstOrdenCompraCartaCia;
		}
		if(null!=estimacion.getId()){			
			queryString.append(" AND compra.idTercero  in ( "+estimacion.getId()+")");			
		}

		queryString.append(" Order by compra.id Desc ");
		ordenesCompras = entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
		for( OrdenCompra ordenCompra : ordenesCompras ){
			ImportesOrdenCompraDTO importe =ordenCompraService.calcularImportes(ordenCompra.getId(), null, false);
			OrdenCompraCartaCia dto = new OrdenCompraCartaCia();
			
			BigDecimal subtotal= importe.getSubtotal();
			BigDecimal subtotalRecuperacion= BigDecimal.ZERO;
			if(!StringUtil.isEmpty(ordenCompra.getDetalleConceptos())){
				String[] conceptos=  ordenCompra.getDetalleConceptos().split(",") ;
				if(conceptos.length>1){
					dto.setConceptoDesc("Varios...");
				}
				else {
					dto.setConceptoDesc(ordenCompra.getDetalleConceptos());

				}
			}

			dto.setMontoARecuperar(subtotal);
			dto.setOrdenCompra(ordenCompra);
			Map<String, Object> param = new HashMap<String, Object>();	
			param.put("ordenCompra.id", ordenCompra.getId());
			param.put("estatus", Recuperacion.EstatusRecuperacion.RECUPERADO.toString());
			List<RecuperacionProveedor>  lstRecuperacion  = this.entidadDao.findByProperties(RecuperacionProveedor.class, param);
			for(RecuperacionProveedor recuperacion :lstRecuperacion ){
				if( recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.REEMBOLSO.toString()) ){
					if(null!=recuperacion.getMontoTotal())
						subtotalRecuperacion=subtotalRecuperacion.add(recuperacion.getSubTotal());
					if(recuperacion.getOrdenCompra() != null 
							&& recuperacion.getOrdenCompra().getIdBeneficiario() != null){
						PrestadorServicio proveedor = entidadService.findById(PrestadorServicio.class, recuperacion.getOrdenCompra().getIdBeneficiario().intValue());
						if(proveedor != null){
							dto.setNombreProveedor(proveedor.getNombrePersona());
						}
					}
				}else if( recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.VENTA.toString()) ){
					if(null!=recuperacion.getMontoVenta())
						subtotalRecuperacion=subtotalRecuperacion.add(recuperacion.getSubTotalVenta());
					if(recuperacion.getComprador() != null)
						dto.setNombreProveedor(recuperacion.getComprador().getNombrePersona());
				}
			}
			List<DocumentoFiscal> facturas = recepcionFacturaService.obtenerFacturasRegistradaByOrdenCompra(ordenCompra.getId(), DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue());
			DocumentoFiscal factura = null;
			if(facturas != null
					&& !facturas.isEmpty()){
				factura = facturas.get(0);
			}
			if(factura != null){
				dto.setNumeroFactura(factura.getNumeroFactura());
			}
			dto.setPorcentajeParticipacion(recuperacionCia.getPorcentajeParticipacion());
			BigDecimal porcentaje = new BigDecimal (recuperacionCia.getPorcentajeParticipacion());
			BigDecimal multiplo = porcentaje.divide(new BigDecimal (100));
			dto.setRecuperacion(null);
			dto.setSubtotalOC(subtotal);
			dto.setMontoARecuperar( ((subtotal.subtract(subtotalRecuperacion)).multiply(multiplo)));
			dto.setSubTotalRecupProveedor(subtotalRecuperacion);
			LstOrdenCompraCartaCia.add(dto);
		}
		return LstOrdenCompraCartaCia;

	}
	public static final int MIDAS_APP_ID = 5;
	public static final String PARAMETRO = "DIAS_VENCIMIENTO_REC_CIA";
	private int diasVencimiento(){
		String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , PARAMETRO);  
		if(!StringUtil.isEmpty(value)){
			try {
				return	Integer.parseInt(value);
				
			}catch (Exception e){
				return 1;
			}
		}else{
			return 1;
		}
		
	}
	
}