package mx.com.afirme.midas2.dao.impl.fuerzaventa;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.sql.SQLException;
import java.util.logging.Level;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaSeycosFacadeRemote;
@Stateless
public class PromotoriaSeycosFacade implements PromotoriaSeycosFacadeRemote{

	@Override
	public Long save(Promotoria promotoria) throws Exception {
		if(promotoria==null){
			throw new Exception("Promotoria is null!");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_catSup";
		Long idSeycos=null;
		try {
			LogDeMidasInterfaz.log("Entrando a PromotoriaSeycosFacade.save..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			Long idEjecutivo=(promotoria.getEjecutivo().getId()!=null)?promotoria.getEjecutivo().getIdEjecutivo():null;
			Long idAgente=(promotoria.getAgentePromotor()!=null && promotoria.getAgentePromotor().getIdAgente()!=null)?promotoria.getAgentePromotor().getIdAgente():null;
			Persona personaResponsable=promotoria.getPersonaResponsable();
			Domicilio domicilio=(isNotNull(personaResponsable) && !isEmptyList(personaResponsable.getDomicilios()))?personaResponsable.getDomicilios().get(0):null;
			DomicilioPk pkDomicilio=(domicilio!=null)?domicilio.getIdDomicilio():null;
			Long idDomicilio=(pkDomicilio!=null)?pkDomicilio.getIdDomicilio():null;
			//Long idDomicilio=(isNotNull(personaResponsable) && !isEmptyList(personaResponsable.getDomicilios()))?personaResponsable.getDomicilios().get(0).getIdDomicilio():null;
			////Long idDomicilio=(isNotNull(domicilio))?domicilio.getIdDomicilio():null;
////			Long idDomicilio=(promotoria.getDomicilio()!=null)?promotoria.getDomicilio().getIdDomicilio():null;
			ValorCatalogoAgentes tipoPromotoria=promotoria.getTipoPromotoria();
			String tipoPromotoriaValor=(tipoPromotoria!=null && tipoPromotoria.getClave()!=null && !tipoPromotoria.getClave().isEmpty())?tipoPromotoria.getClave().trim():null;
			String correo=(promotoria.getPersonaResponsable()!=null && promotoria.getPersonaResponsable().getEmail()!=null)?promotoria.getPersonaResponsable().getEmail():null;
			String agrupador=(promotoria.getAgrupadorPromotoria()!=null && promotoria.getAgrupadorPromotoria().getClave()!=null)?promotoria.getAgrupadorPromotoria().getClave():null;
		
			storedHelper.estableceParametro("pid_supervisoria", val(promotoria.getIdPromotoria()));
			storedHelper.estableceParametro("pdescripcion",val(promotoria.getDescripcion()));
			storedHelper.estableceParametro("pcve_superv",val(tipoPromotoriaValor));
			storedHelper.estableceParametro("pid_oficina",val(StringUtils.leftPad(idEjecutivo.toString(), 3, "0")));
			storedHelper.estableceParametro("pid_agt_sup",val(idAgente));
			storedHelper.estableceParametro("pid_domicilio_resp",val(idDomicilio));
			storedHelper.estableceParametro("pcorreo_electronico",val(correo));
			storedHelper.estableceParametro("pfuerzav",val(agrupador));
			storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz.log("Se ha guardado promotoria..."+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de PromotoriaSeycosFacade.save..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, promotoria.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PromotoriaSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PromotoriaSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		}
		return idSeycos;
	}
	private Object val(Object value){
		return (value!=null)?value:"";
	}
	/**
	 * Actualiza el status de la promotoria a inhabilitado
	 */
	@Override
	public void unsubscribe(Promotoria promotoria) throws Exception {
		promotoria.setClaveEstatus(0);//se inactiva la promotoria.(0 inactiva, 1 activa)
		save(promotoria);//Se actualiza el estatus de la promotoria en Seycos
	}

}
