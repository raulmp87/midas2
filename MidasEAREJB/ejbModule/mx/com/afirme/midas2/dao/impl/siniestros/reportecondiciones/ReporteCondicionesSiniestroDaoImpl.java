package mx.com.afirme.midas2.dao.impl.siniestros.reportecondiciones;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.siniestros.reportecondiciones.ReporteCondicionesSiniestroDao;
import mx.com.afirme.midas2.dto.siniestros.reportecondiciones.ReporteCondicionesSiniestroDTO;

@Stateless
public class ReporteCondicionesSiniestroDaoImpl implements ReporteCondicionesSiniestroDao {

	@Override
	public List<ReporteCondicionesSiniestroDTO> obtenerCondicionesSiniestro(
			String numeroPoliza, Short tipoPoliza, Long idGerencia,
			Long idCondicionEspecial, Date fechaSiniestroIni,
			Date fechaSiniestroFin) {

		List<ReporteCondicionesSiniestroDTO> repCondicionesGenerallst = new ArrayList<ReporteCondicionesSiniestroDTO>();
		String spName = "MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerDatosSinCondicionEsp";
		StoredProcedureHelper storedHelper = null;
		try{
			storedHelper = new StoredProcedureHelper(spName,
					StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
			.estableceMapeoResultados(
					ReporteCondicionesSiniestroDTO.class.getCanonicalName(),
					"numeroPoliza," +
					"codigoCondicion," +
					"nombreCondicion," +
					"siniestralidad," +
					"costoSiniestros," +
					"porcPartCosto," +
					"numeroSiniestros," + 
					"porcPartNumSiniestros," + 
					"primaDevengada," + 
					"numeroPolizas",
					"NUMEROPOLIZA," +
					"CODIGOCONDICION," +
					"NOMBRECONDICION," +
					"SINIESTRALIDAD," +
					"COSTOSINIESTROS," +
					"PORCPARTCOSTO," +
					"NUMEROSINIESTROS," + 
					"PORCPARTNUMSINIESTROS," + 
					"PRIMADEVENGADA," + 
					"NUMEROPOLIZAS"
					);
			storedHelper.estableceParametro("p_numeroPoliza", numeroPoliza);
			storedHelper.estableceParametro("p_tipoPoliza ", tipoPoliza);
			storedHelper.estableceParametro("p_idGerencia", idGerencia);
			storedHelper.estableceParametro("p_idCondicion", idCondicionEspecial);
			storedHelper.estableceParametro("p_fechaIniSiniestro", fechaSiniestroIni);
			storedHelper.estableceParametro("p_fechaFinSiniestro", fechaSiniestroFin);
			repCondicionesGenerallst = storedHelper.obtieneListaResultados();
		}catch(Exception e){
			LogDeMidasInterfaz.log(
					"Excepcion general en ReporteCondicionesSiniestroDao.obtenerCondicionesPolizaSiniestro..."
							+ this, Level.WARNING, e);
		}
		return repCondicionesGenerallst;
	}
	
	

	@Override
	public List<ReporteCondicionesSiniestroDTO>  obtenerCondicionesPolizaSiniestro(
			String numeroPoliza, Short tipoPoliza, Long idGerencia,
			Long idCondicionEspecial, Date fechaSiniestroIni,
			Date fechaSiniestroFin) {
		
		List<ReporteCondicionesSiniestroDTO> repCondicionesDetailLst = new ArrayList<ReporteCondicionesSiniestroDTO>();
		String spName = "MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerDatosSinCondicionPoliza";
		StoredProcedureHelper storedHelper = null;
		try{
			storedHelper = new StoredProcedureHelper(spName,
					StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
			.estableceMapeoResultados(
					ReporteCondicionesSiniestroDTO.class.getCanonicalName(),
					"numeroPoliza," +
					"codigoCondicion," +
					"nombreCondicion," +
					"siniestralidad," +
					"costoSiniestros," +
					"porcPartCosto," +
					"numeroSiniestros," + 
					"porcPartNumSiniestros," + 
					"primaDevengada," + 
					"numeroPolizas",
					"NUMEROPOLIZA," +
					"CODIGOCONDICION," +
					"NOMBRECONDICION," +
					"SINIESTRALIDAD," +
					"COSTOSINIESTROS," +
					"PORCPARTCOSTO," +
					"NUMEROSINIESTROS," + 
					"PORCPARTNUMSINIESTROS," + 
					"PRIMADEVENGADA," + 
					"NUMEROPOLIZAS"
					);
			storedHelper.estableceParametro("p_numeroPoliza", numeroPoliza);
			storedHelper.estableceParametro("p_tipoPoliza ", tipoPoliza);
			storedHelper.estableceParametro("p_idGerencia", idGerencia);
			storedHelper.estableceParametro("p_idCondicion", idCondicionEspecial);
			storedHelper.estableceParametro("p_fechaIniSiniestro", fechaSiniestroIni);
			storedHelper.estableceParametro("p_fechaFinSiniestro", fechaSiniestroFin);
			repCondicionesDetailLst = storedHelper.obtieneListaResultados();
		}catch(Exception e){
			LogDeMidasInterfaz.log(
					"Excepcion general en ReporteCondicionesSiniestroDao.obtenerCondicionesPolizaSiniestro..."
							+ this, Level.WARNING, e);
		}
		return repCondicionesDetailLst;
	}
	


}
