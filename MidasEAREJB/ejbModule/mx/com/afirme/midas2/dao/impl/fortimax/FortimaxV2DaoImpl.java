package mx.com.afirme.midas2.dao.impl.fortimax;


import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.fortimax.FortimaxV2Dao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ParametroAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax.PROCESO_FORTIMAX;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.UsuariosFortimax;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.fortimax.CatalogoAplicacionFortimaxService;
import mx.com.afirme.midas2.service.fortimax.ParametroAplicacionFortimaxService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap11BindingStub;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap11BindingStub;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap11BindingStub;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap11BindingStub;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap11BindingStub;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap11BindingStub;

import org.apache.commons.lang.BooleanUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;



@Stateless
public class FortimaxV2DaoImpl extends EntidadDaoImpl implements FortimaxV2Dao{
	
	@EJB
	EntidadDao entidadDao;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private ListadoService listadoService;
	
	private static final int Privilegio_Nivel_Administrador = 1;
	private static final int Privilegio_Nivel_Generico = 2;
	private static final int Privilegio_Nivel_Consulta= 3;
	public static final String TIPO_DOC_DOCUMENTO = "D";
	public static final String PROCESO_TIPO_LISTA = "L";
	@EJB
	private ParametroGlobalService parametroGlobalService ;
	
	List<ParametroGeneralDTO> parametros = new ArrayList<ParametroGeneralDTO>();
	
	private final static int TIME_OUT=100000;
	private SistemaContext sistemaContext;
	private CatalogoAplicacionFortimaxService aplicacionFortimaxService;
	private ParametroAplicacionFortimaxService parametroFortimaxService;
	
	//TODO Solo se debe de enviar el titulo de aplicacion y los valores de los parametros, lo de mas se obtiene de los servicios.
	@Override
	public String[] generateExpedient(String tituloAplicacion, String[] fieldValues) {
		
		String[]respuestas = new String[4];		
		try{	
			String endpoint=sistemaContext.getFortimaxV2BaseEndpointUrl() +"/ExpedienteService?wsdl";
			CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
			String[] parametros=aplicacionFortimaxService.obtenerParametrosAplicacion(tituloAplicacion);
			String rootFolder=aplicacion.getNombreCarpetaRaiz();			
			ExpedienteServiceLocator expedienteServiceLocator = new ExpedienteServiceLocator();
			ExpedienteServiceSoap11BindingStub serviceExpedientes = (ExpedienteServiceSoap11BindingStub) expedienteServiceLocator.getExpedienteServiceHttpSoap11Endpoint(new URL(endpoint));
			serviceExpedientes.setTimeout(TIME_OUT);
			respuestas=serviceExpedientes.generateExpediente(sistemaContext.getFortimaxV2User(),
					aplicacion.getNombreFortimax(), 
					rootFolder, 
					parametros, 
					fieldValues, 
					true);
			System.out.print("Respuesta generateExpedient Fortimax " + respuestas.toString());
			if (!BooleanUtils.toBoolean(respuestas[0]) && respuestas[2].equals("Error. El expediente ya existe.")) {
				respuestas[0] = "true";
			}
		}
		catch(Exception e){
			System.out.println(e);
		}
		return respuestas;
	}

	@Override
	public String[] generateDocument(Long id,String tituloAplicacion, String documentName,String folderName) throws Exception {		
		String endpointDocumentos=sistemaContext.getFortimaxV2BaseEndpointUrl()+"/DocumentoService?wsdl";
		
		DocumentoServiceLocator locator=new DocumentoServiceLocator();
		DocumentoServiceSoap11BindingStub service = (DocumentoServiceSoap11BindingStub)locator.getDocumentoServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		ParametroAplicacionFortimax fieldName=parametroFortimaxService.obtenerParametroLlavePorAplicacion(tituloAplicacion);
		return service.generateDocument(sistemaContext.getFortimaxV2User(), 
				   aplicacion.getNombreFortimax(), 
				   fieldName.getNombreParametro(),
				   id.toString(), 
				   Utilerias.getMensajeRecurso(SistemaPersistencia.RECURSOS_FORTIMAX, "midas.fortimax.agentes.fileType"),
				   folderName,
				   documentName);			
	}

	
	
	
	@Override
	public String generateLinkToDocument(Long id, String tituloAplicacion, String nombreDocumento, String user,String pass, String nodo)	throws Exception {
		String endpointLiga = sistemaContext.getFortimaxV2BaseEndpointUrl() + "/LinkService?wsdl";
		
		LinkServiceLocator locator=new LinkServiceLocator();
		LinkServiceSoap11BindingStub service = (LinkServiceSoap11BindingStub)locator.getLinkServiceHttpSoap11Endpoint(new URL(endpointLiga));
		service.setTimeout(TIME_OUT);
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		
		try{
			String[]resultados= service.generateLink(user,pass, 
					this.isEncrypted(), 
					aplicacion.getNombreFortimax(),
					id.toString(), 
					nombreDocumento, 
					3600L,  
					true,nodo); 
			
			String result = resultados[0];
			if (null==result || StringUtil.isEmpty(result) ) {
				throw new Exception(String.format("No se pudo Generar Liga, "+ resultados[1]+" ,"+ resultados[2]));
			}
			return result;
			
		}catch(Exception e){
			throw new Exception(String.format("No se pudo Generar Liga, "+e.getMessage()));
		}
	}
	@Override
	public String[] getDocumentFortimax(Long id,String tituloAplicacion) throws Exception {
		String endpointDocumentos = sistemaContext.getFortimaxV2BaseEndpointUrl() + "/DocumentoService?wsdl";
		DocumentoServiceLocator locator=new DocumentoServiceLocator();
		DocumentoServiceSoap11BindingStub service = (DocumentoServiceSoap11BindingStub)locator.getDocumentoServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		ParametroAplicacionFortimax fieldName=parametroFortimaxService.obtenerParametroLlavePorAplicacion(tituloAplicacion);
		try{
		String[]resultados=service.getDocuments(aplicacion.getNombreFortimax(),
				fieldName.getNombreParametro(), 
				id.toString());
		return resultados;
		}
		catch(Exception e){
			System.out.println(e);
			return new String[0];
		}
	}
	
	@Override
	public String[] uploadFile(String fileName, TransporteImpresionDTO transporteImpresionDTO, String tituloAplicacion, String[] expediente, String carpeta){
		String endpointDocumentos = sistemaContext.getFortimaxV2BaseEndpointUrl() + "/DocumentoService?wsdl";
		DocumentoServiceLocator locator=new DocumentoServiceLocator();
		String[] respuestaWS= new String[4];
		try{
		DocumentoServiceSoap11BindingStub service = (DocumentoServiceSoap11BindingStub)locator.getDocumentoServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);		
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		respuestaWS=service.uploadFile(fileName, transporteImpresionDTO.getByteArray(), aplicacion.getNombreFortimax(), expediente, carpeta);
		}
		catch(Exception e){
			respuestaWS[0]="error";
		}
		return respuestaWS;
		
	}

	
	
	@Override
	public String getToken(long duration) throws Exception{
		try {
			String endpoint = sistemaContext.getFortimaxV2BaseEndpointUrl() +"/LoginService?wsdl";
			LoginServiceLocator locator=new LoginServiceLocator();
			LoginServiceSoap11BindingStub service = (LoginServiceSoap11BindingStub)locator.getLoginServiceHttpSoap11Endpoint(new URL(endpoint));
			service.setTimeout(TIME_OUT);
			 String[] response= service.login(sistemaContext.getFortimaxV2User(),sistemaContext.getFortimaxV2Password(),false, 3600L);
			 String result = response[0];
				if (null==result || StringUtil.isEmpty(result) ) {
					throw new Exception(String.format("No se pudo consultar Nodo, "+ response[1]+" ,"+ response[2]));
				}
				return result;
				
			}catch(Exception e){
				throw new Exception(String.format("No se pudo consultar Nodo, "+e.getMessage()));
			}
			 
			 
		}	
	/**
	 * Obtiene el nodo de un documento
	 * @param Long id
	 * @param String tituloAplicacion
	 * @param String nombreElemento
	 * @param String tipoElemento
	 * @return String
	 */
	@Override
	public String getNodo(Long id,String tituloAplicacion, String nombreElemento, String tipoElemento, boolean nodoOnly )throws Exception {
		String endpointDocumentos = sistemaContext.getFortimaxV2BaseEndpointUrl() + "/BusquedaService?wsdl";
		BusquedaServiceLocator locator=new BusquedaServiceLocator();
		BusquedaServiceSoap11BindingStub service = (BusquedaServiceSoap11BindingStub)locator.getBusquedaServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		ParametroAplicacionFortimax fieldName=parametroFortimaxService.obtenerParametroLlavePorAplicacion(tituloAplicacion);
		try{
		String[]resultados=service.getNodo(this.getToken(3600), aplicacion.getNombreFortimax(), fieldName.getNombreParametro(), id.toString(), 
				nombreElemento, tipoElemento)  ;
		String result = resultados[0];
			if (nodoOnly){
			 result = resultados[0].substring(0, resultados[0].indexOf(" "));
			}
		 
		return result;
		}
		catch(Exception e){
			System.out.println(e);
			return null;
		}
	}
		
	/**
	 * Descarga un archivo desde fortimax
	 * @param fileName
	 * @return
	 */
	@Override
	public byte[] downloadFile(String fileName,String tipoElemento , String tituloAplicacion,Long id ) throws Exception {
		String endpointDocumentos = sistemaContext.getFortimaxV2BaseEndpointUrl() + "/MakeZipService?wsdl";
		MakeZipServiceLocator locator=new MakeZipServiceLocator();
		MakeZipServiceSoap11BindingStub service = (MakeZipServiceSoap11BindingStub)locator.getMakeZipServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);
		try {
			 String token = getToken(60);
			 String nodo = getNodo(id,tituloAplicacion, fileName, tipoElemento, true);
			 byte[] byteArray = service.descargaBytesZIP(token, nodo, true);
			if( byteArray == null) {
				throw new Exception(String.format("No se encontró el archivo [%s] en el repositorio, " , fileName));
			}
			return byteArray;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	/**
	 * Descarga un archivo de Fortimax
	 * @param fileName
	 * @param tipoElemento
	 * @param tituloAplicacion
	 * @param id	 
	 * @return byte[]
	 */
	@Override
	public byte[] downloadFileBP(String fileName, String tipoElemento, String tituloAplicacion, Long id) throws Exception {
		String endpointDocumentos = sistemaContext.getFortimaxV2BaseEndpointUrl() + "/MakeZipService?wsdl";
		MakeZipServiceLocator locator = new MakeZipServiceLocator();
		MakeZipServiceSoap11BindingStub service = (MakeZipServiceSoap11BindingStub)locator.getMakeZipServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);
		try {
			String token = getToken(60);
			String nodo = getNodo(id, tituloAplicacion, fileName, tipoElemento, true);
			byte[] byteArray = service.descargaBytesPagina(token, nodo, 0);
			if(byteArray == null) {
				throw new Exception(String.format("No se encontró el archivo [%s] en el repositorio, ", fileName));
			}
			return byteArray;
		} catch (Exception e) {
			throw new Exception(e);
		}
	}
	
	@EJB	
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	@EJB
	public void setAplicacionFortimaxService(
			CatalogoAplicacionFortimaxService aplicacionFortimaxService) {
		this.aplicacionFortimaxService = aplicacionFortimaxService;
	}
	@EJB
	public void setParametroFortimaxService(
			ParametroAplicacionFortimaxService parametroFortimaxService) {
		this.parametroFortimaxService = parametroFortimaxService;
	}
 
	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> entityClass,
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	
	@Override
	public Map<String, String> getListaArchivosByProceso(
			PROCESO_FORTIMAX proceso) throws Exception {
		ProcesosFortimax procesoFortimax=this.getProceso(proceso);
		Map<String, String> mapArchivos = new LinkedHashMap<String, String>();
		if (   null!= procesoFortimax && procesoFortimax.getTipo().equalsIgnoreCase("L")  &&!StringUtil.isEmpty(procesoFortimax.getCodigoGpoFijo())  ){
			mapArchivos=listadoService.obtenerCatalogoValorFijoByStr(procesoFortimax.getCodigoGpoFijo(), null);
		}
		return mapArchivos;
	}


	@Override
	public Map<String, String> getDocumentosAlmacenadosByProceso(
			PROCESO_FORTIMAX proceso, Long fortimaxID,String aplicacion) throws Exception {
			HashMap<String,String> map = new HashMap<String,String>();
			ProcesosFortimax procesoFortimax=this.getProceso(proceso);
			String[]lista=this.getDocumentFortimax(fortimaxID,aplicacion);
			for( String doc : lista){
				if(!StringUtil.isEmpty(doc)){
					if(!procesoFortimax.getTipo().equalsIgnoreCase(PROCESO_TIPO_LISTA) && doc.startsWith(procesoFortimax.getNombreArchivo())){
						String nodo = this.getNodo(fortimaxID, aplicacion, doc, TIPO_DOC_DOCUMENTO, true);
						map.put(nodo, doc);
					}else if (procesoFortimax.getTipo().equalsIgnoreCase(PROCESO_TIPO_LISTA)){
						String nodo = this.getNodo(fortimaxID, aplicacion, doc, TIPO_DOC_DOCUMENTO, true);
						map.put(nodo, doc);
						
					}
				}
			}
			
			
			return map; 
	}
	
	
	@Override
	public ProcesosFortimax getProceso  (PROCESO_FORTIMAX proceso){
		HashMap<String,Object> properties = new HashMap<String,Object>();
		ProcesosFortimax procesoFortimax= null;
		properties.put("proceso",proceso.toString());
		List <ProcesosFortimax>procesos =entidadDao.findByProperties(ProcesosFortimax.class, properties);
		if(!procesos.isEmpty()){
			procesoFortimax= procesos.get(0);
		}
		return procesoFortimax;
	}
	
	
	@Override
	public String generateLinkToDocumentByProceso(Long id, String aplicacion, String nombreDocumento,boolean defaultUser, PROCESO_FORTIMAX proceso, String nodo )throws Exception {
		String user= null;
		String pass = null;
		
		if (defaultUser){
			user=sistemaContext.getFortimaxV2User();
			pass=sistemaContext.getFortimaxV2Password();
		}else{
			UsuariosFortimax usuario = this.obtenerUsuarios(proceso);
			if (null ==usuario )
				throw new Exception("El usuario no tiene permisos para acceder al expediente");
			 user= usuario.getUser();
			 pass = usuario.getPass();
			 System.out.print("---USER : "+user+"  "+pass);
		}
		return this.generateLinkToDocument(id, aplicacion, nombreDocumento, user, pass,nodo);
	}
	
	
	private UsuariosFortimax obtenerUsuarios(PROCESO_FORTIMAX proceso){
		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + UsuariosFortimax.class.getSimpleName() + " model ");	
		queryString.append(" WHERE model.idAplicacion = :idAplicacion AND model.nivel=:nivel order by model.nivel asc ");		
		ProcesosFortimax procesoFx = this.getProceso( proceso);
		TypedQuery<UsuariosFortimax> query = entityManager.createQuery(queryString.toString(), UsuariosFortimax.class);		
		query.setParameter("idAplicacion",procesoFx.getIdAplicacion());		
		query.setParameter("nivel",Privilegio_Nivel_Administrador);//buscar nivel 1 de mas alto privilegios	
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);
		List <UsuariosFortimax> listUSer  = query.getResultList();
		for (UsuariosFortimax user :  listUSer ){			
			if (usuarioService.tieneRolUsuarioActual(user.getCodASM())){
				return user;
			}
		}
		query.setParameter("idAplicacion",procesoFx.getIdAplicacion());		
		query.setParameter("nivel",Privilegio_Nivel_Generico);//buscar nivel 1 de mas alto privilegios	
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);
		List <UsuariosFortimax> listUSerNv2  = query.getResultList();
		for (UsuariosFortimax user :  listUSerNv2 ){			
			if (usuarioService.tieneRolUsuarioActual(user.getCodASM())){
				return user;
			}
		}
		query.setParameter("idAplicacion",procesoFx.getIdAplicacion());		
		query.setParameter("nivel",Privilegio_Nivel_Consulta);//buscar nivel  mas bajo de privilegios	
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);
		List <UsuariosFortimax> listUSerNv3  = query.getResultList();
		for (UsuariosFortimax user :  listUSerNv3 ){			
			if (usuarioService.tieneRolUsuarioActual(user.getCodASM())){
				return user;
			}
		}
		
		return null;
	}
	
	public static final int MIDAS_APP_ID = 5;
	public static final String PARAMETRO = "FORTIMAX_IS_ENCRRYPTED";
	private Boolean isEncrypted(){
		String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , PARAMETRO);  
		if (!StringUtil.isEmpty(value)  &&  value.equalsIgnoreCase("S")){
			return true;
		}else{
			return false;
		}
	}
}
