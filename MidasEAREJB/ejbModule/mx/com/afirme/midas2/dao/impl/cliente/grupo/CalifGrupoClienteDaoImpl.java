package mx.com.afirme.midas2.dao.impl.cliente.grupo;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.cliente.grupo.CalifGrupoClienteDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.cliente.CalifGrupoCliente;

@Stateless
public class CalifGrupoClienteDaoImpl extends JpaDao<Long, CalifGrupoCliente> implements CalifGrupoClienteDao {

	
}
