package mx.com.afirme.midas2.dao.impl.zonaCirculacion;

import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.zonaCirculacion.NegocioMunicipioDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;

@Stateless
public class NegocioMunicipioDaoImpl extends EntidadDaoImpl implements
		NegocioMunicipioDao {
	/**
	 * Obtiene los municipios de un estado (El id no es el id del NegocioEstado)
	 * 
	 * @param idToEstado
	 * @return
	 * @autor martin
	 */
	@Override
	public List<NegocioMunicipio> obtenerMunicipiosPorEstadoId(
			Long idToNegocio, String idToEstado) {
		Map<String,Object> params = new LinkedHashMap<String,Object>();
		params.put("negocioEstado.estadoDTO.stateId", idToEstado);
		params.put("negocioEstado.negocio.idToNegocio", idToNegocio);
		return findByPropertiesWithOrder(NegocioMunicipio.class, params, "municipioDTO.municipalityName");
	}
	
	@Override
	public NegocioMunicipio findByNegocioAndEstadoAndMunicipio(Negocio negocio, EstadoDTO estado, MunicipioDTO municipio) {
		final String jpql = "select model from NegocioMunicipio model where model.municipioDTO = :municipio " +
				"and model.negocioEstado.negocio = :negocio " +
				"and model.negocioEstado.estadoDTO = :estado";
		TypedQuery<NegocioMunicipio> query = entityManager.createQuery(jpql, NegocioMunicipio.class);
		query.setParameter("municipio", municipio)
			.setParameter("negocio", negocio)
			.setParameter("estado", estado);
		 List<NegocioMunicipio> resultList = query.getResultList();
		 if (resultList.size() == 1) {
			 return resultList.get(0);
		 }
		 return null;
	}

	@Override
	public NegocioMunicipio findByNegocioAndEstadoAndMunicipio(
			Long idToNegocio, String stateId, String municipalityId) {
		Negocio negocio = entityManager.getReference(Negocio.class, idToNegocio);
		EstadoDTO estado = entityManager.getReference(EstadoDTO.class, stateId);
		MunicipioDTO municipio = entityManager.getReference(MunicipioDTO.class, municipalityId);
		return findByNegocioAndEstadoAndMunicipio(negocio, estado, municipio);
	}

}
