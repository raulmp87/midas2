package mx.com.afirme.midas2.dao.impl.enlace;

import java.util.Arrays;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.enlace.CaseDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dto.enlace.CaseDTO;

import org.apache.commons.lang.StringUtils;

@Stateless
public class CaseDaoImpl extends EntidadDaoImpl implements CaseDao {

	/**
	 * Obtiene los casos que tengan determinados estatus y le pertenezcan a
	 * cierto usuario
	 * 
	 * @param createUser
	 * @param assignedUser
	 * @param status
	 * @return
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<CaseDTO> getByStatus(String createUser, String assignedUser, int... status) {
		
		final StringBuilder script = new StringBuilder(350);
		script.append(" SELECT caso.* FROM midas.tocase caso ");
		script.append(" LEFT JOIN midas.tocasedeviation deviation ON (deviation.createuser = ");
		script.append(" caso.assigneduser and sysdate between deviation.inidate and deviation.enddate) ");
		script.append(" WHERE caso.status in (" + StringUtils.substringBetween(Arrays.toString(status), "[", "]") + " )");
		if (StringUtils.isEmpty(createUser)) {
			script.append(" AND (caso.assigneduser = ?1 or deviation.deviationuser = ?1) ");
		} else {
			script.append(" AND caso.createuser = ?1 ");
		}
		
		final Query query = entityManager.createNativeQuery(script.toString(), CaseDTO.class);
		//query.setParameter(1, status);//; No se por que no jala así!
		query.setParameter(1, StringUtils.isEmpty(createUser) ? assignedUser : createUser);
		return query.getResultList();
		
	}
	
}
