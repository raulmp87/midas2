package mx.com.afirme.midas2.dao.impl.catalogos;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.ResourceBundle;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Parameter;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.jpa.JpaQuery;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ActualizacionAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoActualizacionAgente;
@Stateless
public class EntidadHistoricoDaoImpl extends EntidadDaoImpl implements EntidadHistoricoDao{
	
	@Override
	public List<ActualizacionAgente> getHistory(TipoOperacionHistorial tipoOperacion,Long idRegistro) throws SystemException {
		List<ActualizacionAgente> lista=new ArrayList<ActualizacionAgente>();
		if(tipoOperacion==null){
			throw new SystemException(getMessageFromResource("midas.historico.error.tipoOperacion.null"));
		}
		if(idRegistro==null){
			throw new SystemException(getMessageFromResource("midas.historico.error.idRegistro.null"));
		}
		Long idTipoOperacion=tipoOperacion.getValue();
		Map<String,Object> params=new HashMap<String, Object>();
		params.put("idRegistro", idRegistro);
		params.put("grupoActualizacionAgente.id", idTipoOperacion);
		String[] orderByAttributes={"fechaHoraActualizacion DESC","id DESC"};
		lista=findByPropertiesWithOrder(ActualizacionAgente.class,params, orderByAttributes);
		return lista;
	}

	@Override
	public ActualizacionAgente getLastUpdate(TipoOperacionHistorial tipoOperacion,Long idRegistro) throws SystemException {
		ActualizacionAgente ultimoCambio=null;
		List<ActualizacionAgente> historico=getHistory(tipoOperacion, idRegistro);
		if(historico!=null && !historico.isEmpty()){
			ultimoCambio=historico.get(0);
		}
		return ultimoCambio;
	}
	
	@Override
	public ActualizacionAgente getHistoryRecord(TipoOperacionHistorial operationType,Long idRecord, String historyDate)throws SystemException
	{
		List<ActualizacionAgente> lista=new ArrayList<ActualizacionAgente>();
		if(operationType==null){
			throw new SystemException(getMessageFromResource("midas.historico.error.tipoOperacion.null"));
		}
		if(idRecord==null){
			throw new SystemException(getMessageFromResource("midas.historico.error.idRegistro.null"));
		}
		
		SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy H:mm:ss.SSSSSS a");
	    Date convertedDate = new Date();
		
		try {
			convertedDate = dateFormat.parse(historyDate);
		} catch (ParseException e) {
			
			e.printStackTrace();
		}
		
		Long idTipoOperacion=operationType.getValue();
		Map<String,Object> params=new HashMap<String, Object>();
		params.put("idRegistro", idRecord);
		params.put("grupoActualizacionAgente.id", idTipoOperacion);
        params.put("fechaHoraActualizacion", convertedDate);		
		lista=findByProperties(ActualizacionAgente.class,params);
		
		if(lista.isEmpty())
		{
			throw new SystemException("No se encontro registro historico para la fecha proporcionada: " + historyDate);			
		}
		
		return lista.get(0);
		
	}

	@Override
	public ActualizacionAgente saveHistory(TipoOperacionHistorial tipoOperacion,Long idRegistro, String comentarios, String usuario,String tipoMovimiento) {
		ActualizacionAgente actualizacion=new ActualizacionAgente();
		actualizacion.setIdRegistro(idRegistro);
		actualizacion.setComentarios(comentarios);
		actualizacion.setFechaHoraActualizacion(TimeUtils.now().toDate());
		Long idTipoOperacion=(tipoOperacion!=null)?tipoOperacion.getValue():null;
		GrupoActualizacionAgente grupo=entityManager.find(GrupoActualizacionAgente.class,idTipoOperacion);
		actualizacion.setUsuarioActualizacion(usuario);
		actualizacion.setGrupoActualizacionAgente(grupo);
		actualizacion.setTipoMovimiento(tipoMovimiento);
		entityManager.persist(actualizacion);
		return actualizacion;
	}

	public String getMessageFromResource(String key){
		String recurso= "mx.com.afirme.midas.RecursoDeMensajes";
		return getMensajeRecurso(recurso,key);
	}
	
	@SuppressWarnings("rawtypes")
	private String getMensajeRecurso(String recurso,String clave) {
		ResourceBundle objlResourceBundle =ResourceBundle.getBundle(recurso);
		Enumeration objlEnum = null;
		boolean blExist = false;
		String slMessage = null;
		for (objlEnum = objlResourceBundle.getKeys(); objlEnum!=null && objlEnum.hasMoreElements() && !blExist;){
			if (clave.equalsIgnoreCase((String) objlEnum.nextElement())) {
				blExist = true;
			}
		}
		if (blExist) {
			slMessage = objlResourceBundle.getString(clave);
		}
		return slMessage;
	}
	
	public String getQueryString(StringBuilder queryString){
		return getQueryString(queryString.toString());
	}
	
	private String getQueryString(String queryString){
		if(queryString.endsWith(" and ")){
			queryString = queryString.substring(0,(queryString.length())-(" and ").length());
		}
		if(queryString.endsWith(" or ")){
			queryString = queryString.substring(0,(queryString.length())-(" or ").length());
		}
		return queryString;
	}
	
	public void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	public void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	@Override
	public <E extends Entidad, K> E findById(Class<E> entityClass, String keyPropertyName, K key, Date historyDate) {
		
		E singleResult = null;
	
		List <E> result = this.findByProperty(entityClass, keyPropertyName, key, historyDate);
		
		if (result != null && !result.isEmpty()) {
			
			singleResult = result.get(0);
			
		}
	
		return singleResult;
		
	}

	
	@Override
	public <E extends Entidad> List<E> findAll(Class<E> entityClass, Date historyDate) {
		
		CriteriaQuery<E> criteriaQuery = entityManager.getCriteriaBuilder()
				.createQuery(entityClass);
		criteriaQuery.from(entityClass);
		TypedQuery<E> query = entityManager.createQuery(criteriaQuery);
		query.setHint("org.hibernate.cacheable", "true");
		
		return executeHistoryQuery(historyDate, query);
	}
	
	@Override
	public <E extends Entidad> List<E> findByProperty(Class<E> entityClass,
			String propertyName, Object value, Date historyDate) {
		
		final String queryString = "select model from "+entityClass.getSimpleName()+" model where model."
				+ propertyName + "= :propertyValue";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("propertyValue", value);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		
		return executeHistoryQuery(historyDate, query);
	}
	

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> entityClass,
			Map<String, Object> params, Date historyDate) {
		entityManager.getEntityManagerFactory().getCache().evict(entityClass);
		final StringBuilder queryString = new StringBuilder("select model from "+entityClass.getSimpleName()+" model ");
		boolean setParameters=false;
		if(params!=null && !params.isEmpty()){
			setParameters=true;
			queryString.append(" where ");
			int size=params.size();
			int index=0;
			for(String property:params.keySet()){
				String propertyMap=getValidProperty(property);//checa si tiene "." la propiedad
				
				Object value = params.get(property);
				if (value != null) {
					queryString.append("model."+property+"=:"+propertyMap);
				} else {
					queryString.append("model."+property+" IS NULL");
				}
				
				if(index!=(size-1)){
					queryString.append(" and ");
				}
				index++;
			}
		}
		Query query = entityManager.createQuery(queryString.toString());
		if(setParameters){
			setQueryParametersByPropertiesNotNull(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		
		return executeHistoryQuery(historyDate, query);
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> entityClass,
			Map<String, Object> params, Date historyDate, String... orderByAttributes) {
		final StringBuilder queryString = new StringBuilder("select model from "+entityClass.getSimpleName()+" model ");
		boolean setParameters=false;
		int size=0;
		int index=0;
		if(params!=null && !params.isEmpty()){
			setParameters=true;
			queryString.append(" where ");
			size=params.size();
			for(String property:params.keySet()){
				String propertyMap=getValidProperty(property);//checa si tiene "." la propiedad
				queryString.append("model."+property+"=:"+propertyMap);
				if(index!=(size-1)){
					queryString.append(" and ");
				}
				index++;
			}
		}
		if(orderByAttributes!=null && orderByAttributes.length>0){
			size=orderByAttributes.length;
			index=0;
			queryString.append(" ORDER BY ");
			for(String orderAttribute:orderByAttributes){
				queryString.append("model."+orderAttribute);
				if(index!=(size-1)){
					queryString.append(",");
				}
				index++;
			}
		}
		Query query = entityManager.createQuery(queryString.toString());
		if(setParameters){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return executeHistoryQuery(historyDate, query);
	}

	private void setQueryParametersByPropertiesNotNull(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			if (pairs.getValue() != null && !pairs.getValue().equals("")) {
					String key=getValidProperty(pairs.getKey());
					entityQuery.setParameter(key, pairs.getValue());
				}
		}		
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public <E extends Entidad> List<E> executeHistoryQuery(Date historyDate, Query query) {
		
		String queryString = query.unwrap(JpaQuery.class).getDatabaseQuery().getJPQLString();
		
		for (Parameter<?> parameter : query.getParameters()) {
			queryString = queryString.replace(":" + parameter.getName(), query.getParameterValue(parameter.getName()).toString());
		}
				
		JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
		ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
		AsOfClause asOfClause = new AsOfClause(historyDate);
		Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
		
		ReadAllQuery historicalQuery = new ReadAllQuery();
		historicalQuery.setJPQLString(getQueryString(queryString).toString());
		historicalQuery.refreshIdentityMapResult();
		return (List<E>)historicalSession.executeQuery(historicalQuery);			
		
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public <E extends Entidad> E updateForHistory(E entity) {
		return this.update(entity);
	}
	
	public void addConditionAgnt(int index,StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		if(index>1)
			queryString.append(" and ");
		queryString.append(conditional);		
	}
	
}
