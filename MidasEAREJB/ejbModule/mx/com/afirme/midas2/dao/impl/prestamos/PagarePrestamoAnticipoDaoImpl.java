package mx.com.afirme.midas2.dao.impl.prestamos;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.sql.SQLException;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.prestamos.PagarePrestamoAnticipoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.domain.prestamos.PagarePrestamoAnticipo;
import mx.com.afirme.midas2.dto.impresiones.PagarePrestamoAnticipoImpresion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class PagarePrestamoAnticipoDaoImpl extends EntidadDaoImpl implements PagarePrestamoAnticipoDao{

	private EntidadService entidadService;
	private Integer cont=0;
	private AgenteMidasService agenteMidasService;
	private ValorCatalogoAgentesService catalogoService;

	private static String CLAVE_TRANSACCION_AGENTES="AGT";
	private static String CLAVE_CONCEPTO_TRANSACCION="AGCOB";
	private static String USUARIO_TRANSACCION="MIDAS";
	private static String EMPRESA_DEFAULT="8";
	private static String CLAVE_CLASE_AGENTE="C";
	private static String SITUACION_MOVIMIENTO="PEN";
	private static final String PKGCALCULO_AGENTES="PKGCALCULOS_AGENTES";
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PagarePrestamoAnticipo> loadByIdConfigPrestamo(ConfigPrestamoAnticipo obj) throws Exception {

		List<PagarePrestamoAnticipo> listaPagare = new ArrayList<PagarePrestamoAnticipo>();
//		listaPagare=entidadService.findByProperty(PagarePrestamoAnticipo.class, "configPrestamoAnticipo.id", obj.getId());

		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from PagarePrestamoAnticipo model left join fetch model.configPrestamoAnticipo left join fetch model.estatus ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(obj!=null){
			if(obj.getId()!=null){
				addCondition(queryString, "model.configPrestamoAnticipo.id=:id");
				params.put("id", obj.getId());
			}
		}
		String finalQuery=getQueryString(queryString)+" order by model.fechaCargo ASC ";
		Query query = entityManager.createQuery(finalQuery);
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		listaPagare=query.getResultList();
		return listaPagare;
	}
		
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}

	@Override
	public PagarePrestamoAnticipo save(PagarePrestamoAnticipo obj, Long idConfigPrestamos)throws Exception {

		ConfigPrestamoAnticipo configPrestamoAnticipo = entidadService.findById(ConfigPrestamoAnticipo.class, idConfigPrestamos);
		obj.setConfigPrestamoAnticipo(configPrestamoAnticipo);
		PagarePrestamoAnticipo pagarePrestamoAnticipo = new PagarePrestamoAnticipo();
		List<PagarePrestamoAnticipo> listaPagare = new ArrayList<PagarePrestamoAnticipo>();
		
		ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();
		estatus = entidadService.findById(ValorCatalogoAgentes.class, obj.getEstatus().getId());
		obj.setEstatus(estatus);
	
		listaPagare=loadByIdConfigPrestamo(obj.getConfigPrestamoAnticipo());
		if(!listaPagare.isEmpty()){
			if (cont==0 && !listaPagare.isEmpty()){
				for(PagarePrestamoAnticipo objPagare:listaPagare){
					entidadService.remove(objPagare);
					cont++;
				}
			}
		}
		pagarePrestamoAnticipo=entidadService.save(obj);
		cont=0;
		return pagarePrestamoAnticipo;
	}

	@Override
	public PagarePrestamoAnticipo save(List<PagarePrestamoAnticipo> list, Long idConfigPrestamos)throws Exception {
		if(!list.isEmpty()){
			for(PagarePrestamoAnticipo obj : list){
				save(obj, idConfigPrestamos);
				cont++;
			}
			cont=0;
		}
		return null;
	}

	@Override
	public PagarePrestamoAnticipo updateEstatusPagare(PagarePrestamoAnticipo obj, String elementoCatalogo)throws Exception {
		
		ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();
		/**
		 * FIXME
		 * hay que cambiar el nombre de el catalogo
		 */
		estatus = catalogoService.obtenerElementoEspecifico("Estatus del Pagare de Prestamos", elementoCatalogo);
		
		PagarePrestamoAnticipo	pagarePrestamoAnticipo = entidadService.findById(PagarePrestamoAnticipo.class, obj.getId());
		pagarePrestamoAnticipo.setEstatus(estatus);
		pagarePrestamoAnticipo = entidadService.save(pagarePrestamoAnticipo);
		return pagarePrestamoAnticipo;
	}
	
	@Override
	public List<PagarePrestamoAnticipo> updateEstatusPagare(List<PagarePrestamoAnticipo> list, String elementoCatalogo){
		PagarePrestamoAnticipo	pagarePrestamoAnticipo = new PagarePrestamoAnticipo();
		List<PagarePrestamoAnticipo> lista = new ArrayList <PagarePrestamoAnticipo>();
		try {
			for(PagarePrestamoAnticipo obj:list){
				pagarePrestamoAnticipo = updateEstatusPagare(obj, elementoCatalogo);
				lista.add(pagarePrestamoAnticipo);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return lista;
	}

	@Override
	public boolean llenarYGuardarListaPagares(String pagaresAgregados,ConfigPrestamoAnticipo config) {
		try{
			if(pagaresAgregados!=null && !pagaresAgregados.equals("")){
				List<PagarePrestamoAnticipo> listaPagare = new ArrayList<PagarePrestamoAnticipo>();
				listaPagare=loadByIdConfigPrestamo(config);
				if(!listaPagare.isEmpty()){
					if (cont==0 && !listaPagare.isEmpty()){
						for(PagarePrestamoAnticipo objPagare:listaPagare){
							entidadService.remove(objPagare);
							cont++;
						}
					}
				}
				
				String[]arrayPagares=pagaresAgregados.split(",");
					for(int i=0;i<=arrayPagares.length-1;i+=15){
						PagarePrestamoAnticipo pagareGuardar=new PagarePrestamoAnticipo();
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");						
						Date  fechaCargo = df.parse(arrayPagares[i+3]);
						Double capitalInicial=(isValid(arrayPagares[i+4]))?Double.parseDouble(arrayPagares[i+4]):null;
						Double interesOrdinarioXPagare = (isValid(arrayPagares[i+5])) ? Double.parseDouble(arrayPagares[i+5]) : null;
						Double cargosMoratorios = (isValid(arrayPagares[i+6])) ? Double.parseDouble(arrayPagares[i+6]) : null;
						Double abonoCapital =(isValid(arrayPagares[i+7]))? Double.parseDouble(arrayPagares[i+7]) : null;
						Double iva =(isValid(arrayPagares[i+8]))? Double.parseDouble(arrayPagares[i+8]) : null;
						Double importePago =(isValid(arrayPagares[i+9]))? Double.parseDouble(arrayPagares[i+9]) : null;
						Double capitalFinal =(isValid(arrayPagares[i+10]))? Double.parseDouble(arrayPagares[i+10]) : null;
						Double abonoPorPagare =(isValid(arrayPagares[i+11]))? Double.parseDouble(arrayPagares[i+11]) : null;
						
						ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();
						estatus = catalogoService.obtenerElementoEspecifico("Estatus del Pagare de Prestamos", arrayPagares[i+12]);
												
						pagareGuardar.setConfigPrestamoAnticipo(config);
						pagareGuardar.setFechaCargo(fechaCargo);
						pagareGuardar.setCapitalInicial(capitalInicial);
						pagareGuardar.setInteresOrdinario(interesOrdinarioXPagare);
						pagareGuardar.setInteresMoratorio(cargosMoratorios);
						pagareGuardar.setAbonoCapital(abonoCapital);
						pagareGuardar.setIva(iva);
						pagareGuardar.setImportePago(importePago);
						pagareGuardar.setCapitalfinal(capitalFinal);
						pagareGuardar.setAbonoPorPagare(abonoPorPagare);
						pagareGuardar.setEstatus(estatus);
						
						entidadService.save(pagareGuardar);
						cont=0;
					}			
				}
			return true;
			}catch(Exception e){
				return false;
			}		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public PagarePrestamoAnticipo loadById(PagarePrestamoAnticipo pagare) throws Exception {
		PagarePrestamoAnticipo filtro=new PagarePrestamoAnticipo();
		List<PagarePrestamoAnticipo> listaPagare = new ArrayList<PagarePrestamoAnticipo>();

		
		if(pagare!=null){
			String queryString;
			queryString="SELECT * FROM MIDAS.TOPAGAREPRESTAMOANTICIPO where id =" + pagare.getId();
			
			Query query=entityManager.createNativeQuery(queryString,PagarePrestamoAnticipo.class);
			listaPagare=query.getResultList();
		}
		filtro = listaPagare.get(0);
		return filtro;
	}
	
	@Override
	public PagarePrestamoAnticipoImpresion infoImpresionPagare(PagarePrestamoAnticipo pagare) throws Exception {
		PagarePrestamoAnticipoImpresion filtro=new PagarePrestamoAnticipoImpresion();
		List<PagarePrestamoAnticipoImpresion> listaPagare = new ArrayList<PagarePrestamoAnticipoImpresion>();

		
		if(pagare!=null){
			String queryString;
			queryString= " select pagare.id,pagare.abonoPorPagare as capitalInicial,config.pcteInteresOrdinario as interesOrdinario,UPPER(plazo.valor) as plazoInteresOrdinario,config.FECHAALTAMOVIMIENTO as fechaSuscripcion,pagare.fechaCargo as fechaVencimiento,persona.NOMBRECOMPLETO as nombreCompletoSuscriptor " +
			" ,dom.CALLENUMERO as calleYNumeroSuscriptor,dom.COLONIA as coloniaSuscriptor,dom.CIUDAD as municipioSuscriptor,dom.ESTADO as estadoSuscriptor,dom.CODIGOPOSTAL as codigoPostalSuscriptor,persona.TELEFONOCASA as telefonoSuscriptor" +
			",config.CALLENUMEROAVAL as calleYNumeroAval,config.COLONIAAVAL as coloniaAval,config.NOMBREAVAL as nombreCompletoAval" 
			+ " ,config.MUNICIPIOAVAL as municipioAval,config.ESTADOAVAL as estadoAval,config.CODIGOPOSTALAVAL as codigoPostalAval,config.TELEFONOAVAL as telefonoAval " 
			+ " from MIDAS.topagareprestamoanticipo pagare " 
			+ " left join MIDAS.toconfigprestamoanticipo config on pagare.IDCONFIGPRESTAMOANTICIPO = config.ID " 
			+ " left join MIDAS.toagente agente on config.IDAGENTE = agente.ID " 
			+ " left join MIDAS.vw_persona persona on persona.IDPERSONA = agente.IDPERSONA " 
			+ " left join MIDAS.tovalorcatalogoagentes plazo on plazo.ID = config.IDPLAZOINTERES " 
			+ " left join MIDAS.vw_domicilio dom on persona.IDDOMICILIO = dom.IDDOMICILIO " 
			+ " where pagare.ID =" + pagare.getId();
			
			Query query=entityManager.createNativeQuery(queryString,PagarePrestamoAnticipoImpresion.class);
			listaPagare=query.getResultList();
		}
		filtro = listaPagare.get(0);
		return filtro;
	}
	
	@Override
	public PagarePrestamoAnticipoImpresion infoImpresionPagarePrincipal(ConfigPrestamoAnticipo config) throws Exception {
		PagarePrestamoAnticipoImpresion filtro=new PagarePrestamoAnticipoImpresion();
		List<PagarePrestamoAnticipoImpresion> listaPagare = new ArrayList<PagarePrestamoAnticipoImpresion>();

		
		if(config!=null){
			String queryString;
			queryString= " select config.id,config.importeOtorgado as capitalInicial,config.pcteInteresOrdinario as interesOrdinario,UPPER(plazo.valor) as plazoInteresOrdinario,config.FECHAALTAMOVIMIENTO as fechaSuscripcion, "
					 + " (select max(fechaCargo) from MIDAS.topagareprestamoanticipo where IDCONFIGPRESTAMOANTICIPO = "+config.getId()+") as fechaVencimiento, "
					 + " persona.NOMBRECOMPLETO as nombreCompletoSuscriptor " 
					 + " ,dom.CALLENUMERO as calleYNumeroSuscriptor,dom.COLONIA as coloniaSuscriptor,dom.CIUDAD as municipioSuscriptor,dom.ESTADO as estadoSuscriptor,dom.CODIGOPOSTAL as codigoPostalSuscriptor,persona.TELEFONOCASA as telefonoSuscriptor "
					 + " ,config.CALLENUMEROAVAL as calleYNumeroAval,config.COLONIAAVAL as coloniaAval,config.NOMBREAVAL as nombreCompletoAval "
					 + " ,config.MUNICIPIOAVAL as municipioAval,config.ESTADOAVAL as estadoAval,config.CODIGOPOSTALAVAL as codigoPostalAval,config.TELEFONOAVAL as telefonoAval "  
					 + " from MIDAS.toconfigprestamoanticipo config " 
					 + " left join MIDAS.toagente agente on config.IDAGENTE = agente.ID " 
					 + " left join MIDAS.vw_persona persona on persona.IDPERSONA = agente.IDPERSONA " 
					 + " left join MIDAS.tovalorcatalogoagentes plazo on plazo.ID = config.IDPLAZOINTERES " 
					 + " left join MIDAS.vw_domicilio dom on persona.IDDOMICILIO = dom.IDDOMICILIO  "
					 + " where config.id = " + config.getId();
			
			Query query=entityManager.createNativeQuery(queryString,PagarePrestamoAnticipoImpresion.class);
			listaPagare=query.getResultList();
		}
		filtro = listaPagare.get(0);
		return filtro;
	}
	
		
	@Override
	public PagarePrestamoAnticipo aplicaPagare(String pagaresAgregados,ConfigPrestamoAnticipo config,PagarePrestamoAnticipo pagareActivado) throws Exception {
		if(config==null){
			throw new Exception("No se envio la configuracion que corresponde al pagare");
		}		
		if(config != null){
			PagarePrestamoAnticipo pagareXActivar = new PagarePrestamoAnticipo();
			Double abonoXPagare = pagareActivado.getAbonoPorPagare();
			pagareActivado = entidadService.findById(PagarePrestamoAnticipo.class, pagareActivado.getId());
			pagareXActivar.setAbonoPorPagare(abonoXPagare);
			pagareXActivar.setFechaCargo(pagareActivado.getFechaCargo());
			pagareXActivar.setFechaCargoString(pagareActivado.getFechaCargoString());
			pagareXActivar.setId(pagareActivado.getId());
			pagareXActivar.setIva(pagareActivado.getIva());
			//se guarda el calendario de pagos actualizado
			llenarYActualizarListaPagares(pagaresAgregados, config);
			
			ConfigPrestamoAnticipo configPrestamo = entidadService.findById(ConfigPrestamoAnticipo.class, config.getId());
			//se cancelan los pagares que esten pendientes o en proceso
			cancelaPagareMovAgt(configPrestamo,pagareXActivar);
			//se inserta el nuevo pagare con la cantidad total a pagar (los pagares vencidos mas los activos) en estado de cta
			aplicaPagareEnMovimientosAgt(configPrestamo, pagareXActivar);
			
			
			//FIXME prueba de activacion automatica se ejecutara con un job
//			activacionAutomaticaPagare();
			
		}
		return pagareActivado;
	}
	//metodo para insertar el pagare en la tabla de toagentemovimientos con el concepto de prestamo 
	//y la naturaleza del concepto es 'Cargo'
	private void aplicaPagareEnMovimientosAgt(ConfigPrestamoAnticipo config,PagarePrestamoAnticipo pagare) throws Exception {
		if(pagare!=null && pagare.getId()!=null){
			Long idAgente = config.getAgente().getId();
			Agente agente=null;
			if(isNotNull(idAgente)){
				agente=agenteMidasService.loadById(config.getAgente());
			}
			String nombreAgente=(isNotNull(agente) && isNotNull(agente.getPersona()))?agente.getPersona().getNombreCompleto():null;
			StoredProcedureHelper storedHelper = null;
//			String sp="SEYCOS.pkg_int_midas_E2.stpInserta_Movto_Edocta_midas";
			String sp="MIDAS."+PKGCALCULO_AGENTES+".stp_insertaMovimientoAgente";
			try {
				LogDeMidasInterfaz.log("Entrando a PagarePrestamoAnticipoDaoImpl.aplicarPagare..." + this, Level.INFO, null);
				ValorCatalogoAgentes tipoMoneda=catalogoService.obtenerElementoEspecifico("Tipo Moneda", "NACIONAL");
				Long idTipoAgente=agente.getIdTipoAgente();
				ValorCatalogoAgentes tipoAgente=new ValorCatalogoAgentes();
				if(isNotNull(idTipoAgente)){
					tipoAgente.setId(idTipoAgente);
					tipoAgente=catalogoService.loadById(tipoAgente);
				}
				
				Long idTipoMoneda=(isNotNull(tipoMoneda) && isNotNull(tipoMoneda.getClave()))?Long.parseLong(tipoMoneda.getClave()):null;
				Long claveAgente=(isNotNull(agente))?agente.getIdAgente():null;
				ValorCatalogoAgentes tipoMovimiento=config.getTipoMovimiento();
				String tipoConcepto="PAGARE-PRESTAMO";
				String tipo=(isNotNull(tipoMovimiento) && isValid(tipoMovimiento.getValor()))?tipoMovimiento.getValor():"";
				if("PRESTAMOS".equalsIgnoreCase(tipo)){
					tipoConcepto="PAGARE-PRESTAMO";
				}else if("ANTICIPO DE BONOS".equalsIgnoreCase(tipo)){
					tipoConcepto="PAGARE-ANT_BONO";
				}else if("ANTICIPO DE COMISIONES".equalsIgnoreCase(tipo)){
					tipoConcepto="PAGARE-ANT_COMISION";
				}
				ValorCatalogoAgentes concepto=catalogoService.obtenerElementoEspecifico("Concepto Movimiento Agente", tipoConcepto);//Obtiene el tipo de concepto nuevo para los pagares.
				Long idConcepto = concepto.getIdRegistro();
				ValorCatalogoAgentes estatusMovimiento=catalogoService.obtenerElementoEspecifico("Situacion Movimiento Agente", "PENDIENTE");
				Long idEstatusMovimiento = estatusMovimiento.getId();
				
				
				String fechaCargoPagare = pagare.getFechaCargoString();
				Integer anio = Integer.parseInt(fechaCargoPagare.substring(6,10));
				Integer mes  = Integer.parseInt(fechaCargoPagare.substring(3,5));
	
				String mesCompleto="";
				if(mes<=9){
					mesCompleto = "0"+mes;
				}else{
					mesCompleto = Integer.toString(mes);
				}					
				String anioMes = Integer.toString(anio)+mesCompleto;
				
				
				storedHelper = new StoredProcedureHelper(sp);
				storedHelper.estableceParametro("pId_Empresa",EMPRESA_DEFAULT);
				storedHelper.estableceParametro("pId_Agente",val(claveAgente));
				storedHelper.estableceParametro("pF_Corte_Edocta",new Date());//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
				storedHelper.estableceParametro("pIdConsecMovto",null);
				storedHelper.estableceParametro("pAno_Mes",anioMes);//////////////HJC 23/05/14: No importa lo que se envíe, se calcula el periodo contable abierto en la BD
				storedHelper.estableceParametro("pCve_T_Cpto_A_C","AGMAN");//FIXME que debe de ir aqui
				storedHelper.estableceParametro("pId_Concepto",idConcepto);/////////FIXME De donde viene este concepto, segun esto viene a nivel recibo, pero esto es acomulativo, cual de todos los conceptos de cada recibo pongo???
				storedHelper.estableceParametro("pCve_C_A",CLAVE_CLASE_AGENTE);
				storedHelper.estableceParametro("pId_Moneda",val(idTipoMoneda));//FIXME Revisar si es nacional siempre
				storedHelper.estableceParametro("pId_Moneda_Orig",val(idTipoMoneda));//FIXME Revisar si es nacional siempre
				storedHelper.estableceParametro("pTipo_Cambio",null);//FIXME como obtengo el tipo de cambio al dia?
				storedHelper.estableceParametro("pDesc_Movto",nombreAgente);///////////
				storedHelper.estableceParametro("pId_Ramo_Contable",null);
				storedHelper.estableceParametro("pId_SubR_Contable",null);
				storedHelper.estableceParametro("pId_LIn_Negocio",null);
				storedHelper.estableceParametro("pF_Movto",pagare.getFechaCargo());//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD, se validó con Christian que lo que se inserta sea la fecha actual
				storedHelper.estableceParametro("pId_Centro_Emis",null);
				storedHelper.estableceParametro("pNum_Poliza",null);
				storedHelper.estableceParametro("pNum_Renov_Pol",null);
				storedHelper.estableceParametro("pId_Cotizacion",null);
				storedHelper.estableceParametro("pId_Version_Pol",null);
				storedHelper.estableceParametro("pId_Centro_Emis_E",null);
				storedHelper.estableceParametro("pCve_T_Endoso",null);
				storedHelper.estableceParametro("pNum_Endoso",null);
				storedHelper.estableceParametro("pId_Solicitud",null);
				storedHelper.estableceParametro("pId_Version_End",null);
				storedHelper.estableceParametro("pReferencia","");
				storedHelper.estableceParametro("pId_Centro_Oper",null);
				storedHelper.estableceParametro("pId_Remesa","01");
				storedHelper.estableceParametro("pId_Consec_MovtoR",null);
				storedHelper.estableceParametro("pCve_Origen_Remes",null);
				storedHelper.estableceParametro("pSerie_Folio_Rbo",null);
				storedHelper.estableceParametro("pNum_Folio_Rbo",null);
				storedHelper.estableceParametro("pId_Recibo",null);
				storedHelper.estableceParametro("pId_Version_Rbo",null);
				storedHelper.estableceParametro("pF_Vencto_Recibo",null);
				storedHelper.estableceParametro("pPct_Partic_Agte",null);
				storedHelper.estableceParametro("pImp_Prima_Neta",null);
				storedHelper.estableceParametro("pImp_Rcgos_PagoFr",null);
				storedHelper.estableceParametro("pImp_Comis_Agte",pagare.getAbonoPorPagare());
				storedHelper.estableceParametro("pCve_Origen_Aplic","SU");
				storedHelper.estableceParametro("pCve_Origen_Movto","SUS");
				storedHelper.estableceParametro("pF_Corte_Pagocom",null);
				storedHelper.estableceParametro("pSit_Movto_Edocta",idEstatusMovimiento);
				storedHelper.estableceParametro("pId_Usuario_Integ",USUARIO_TRANSACCION);//FIXME revisar si puede ser midas.
				storedHelper.estableceParametro("pFH_Integracion",new Date());//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
				storedHelper.estableceParametro("pFH_Aplicacion",null);//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
				storedHelper.estableceParametro("pF_Transf_DepV",null);
				storedHelper.estableceParametro("pid_transac_depv",null);
				storedHelper.estableceParametro("pId_Transacc_Orig",pagare.getId());
				storedHelper.estableceParametro("pId_Agente_Orig",val(claveAgente));
				storedHelper.estableceParametro("pCve_T_Cpto_A_C_O","AGAUT");
				storedHelper.estableceParametro("pId_Concepto_O",null);
				storedHelper.estableceParametro("pImp_Prima_Total",null);
				storedHelper.estableceParametro("pB_Facultativo","F");
				storedHelper.estableceParametro("pB_Masivo","F");
				storedHelper.estableceParametro("pId_Cobertura",null);
				storedHelper.estableceParametro("pAnio_Vig_Poliza","1");
				storedHelper.estableceParametro("pImp_Prima_D_CP",null);
				storedHelper.estableceParametro("pCve_Sist_Admon","NA");
				storedHelper.estableceParametro("pCve_Exp_Calc_Div","NA");
				storedHelper.estableceParametro("pImp_Base_Calculo",null);
				storedHelper.estableceParametro("pPctComisAgt",null);
				storedHelper.estableceParametro("pNum_Mov_Man_Agte",null);
				storedHelper.estableceParametro("pImpPagoAntImptos",null);
				storedHelper.estableceParametro("pPct_IVA",null);
				storedHelper.estableceParametro("ppct_iva_ret",null);
				storedHelper.estableceParametro("ppct_isr_ret",null);
				storedHelper.estableceParametro("pimp_iva",pagare.getIva());
				storedHelper.estableceParametro("ppimp_iva_ret",null);
				storedHelper.estableceParametro("pimp_isr",null);
				storedHelper.estableceParametro("pcve_sdo_impto ",null);			
				storedHelper.estableceParametro("pB_Impuesto","F");
				storedHelper.estableceParametro("pAno_Mes_Pago",null);
				storedHelper.ejecutaActualizar();
				
				LogDeMidasInterfaz.log("Saliendo de PagarePrestamoAnticipoDaoImpl.aplicarPagare..." + this, Level.INFO, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,PagarePrestamoAnticipoDaoImpl.class, codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de PagarePrestamoAnticipoDaoImpl.aplicarPagare..." + this, Level.WARNING, e);
				onError(e);
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en PagarePrestamoAnticipoDaoImpl.aplicarPagare..." + this, Level.WARNING, e);
				onError(e);
			}
		}
	}
	
	private void cancelaPagareMovAgt(ConfigPrestamoAnticipo config,PagarePrestamoAnticipo pagare){		
		if(pagare != null){
			StoredProcedureHelper storeHelper = null;
			String sp = "MIDAS."+PKGCALCULO_AGENTES+".stp_cancelaPagareMovAgt";
			try {
				ValorCatalogoAgentes estatusMovimiento=catalogoService.obtenerElementoEspecifico("Situacion Movimiento Agente", "CANCELADA");
				Long idEstatusMovimiento = estatusMovimiento.getId();	
				storeHelper = new  StoredProcedureHelper(sp);
				storeHelper.estableceParametro("p_idempresa",EMPRESA_DEFAULT);
				storeHelper.estableceParametro("p_idAgente",config.getAgente().getIdAgente());
				storeHelper.estableceParametro("p_fCorteEdoCta",pagare.getFechaCargoString());
				storeHelper.estableceParametro("p_estatusMovimiento", idEstatusMovimiento);
				storeHelper.ejecutaActualizar();
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
	}
	
	private boolean llenarYActualizarListaPagares(String pagaresAgregados,ConfigPrestamoAnticipo config) {
		try{
			if(pagaresAgregados!=null && !pagaresAgregados.equals("")){
//				List<PagarePrestamoAnticipo> listaPagare = new ArrayList<PagarePrestamoAnticipo>();
//				listaPagare=loadByIdConfigPrestamo(config);
//				if(!listaPagare.isEmpty()){
//					if (cont==0 && !listaPagare.isEmpty()){
//						for(PagarePrestamoAnticipo objPagare:listaPagare){
//							entidadService.remove(objPagare);
//							cont++;
//						}
//					}
//				}
//				
				String[]arrayPagares=pagaresAgregados.split(",");
					for(int i=0;i<=arrayPagares.length-1;i+=15){
						PagarePrestamoAnticipo pagareGuardar=new PagarePrestamoAnticipo();
						DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
						Long idPagare =(isValid(arrayPagares[i+2]))? Long.parseLong(arrayPagares[i+2]):null;
						Date  fechaCargo = df.parse(arrayPagares[i+3]);
						Double capitalInicial=(isValid(arrayPagares[i+4]))?Double.parseDouble(arrayPagares[i+4]):null;
						Double interesOrdinarioXPagare = (isValid(arrayPagares[i+5])) ? Double.parseDouble(arrayPagares[i+5]) : null;
						Double cargosMoratorios = (isValid(arrayPagares[i+6])) ? Double.parseDouble(arrayPagares[i+6]) : null;
						Double abonoCapital =(isValid(arrayPagares[i+7]))? Double.parseDouble(arrayPagares[i+7]) : null;
						Double iva =(isValid(arrayPagares[i+8]))? Double.parseDouble(arrayPagares[i+8]) : null;
						Double importePago =(isValid(arrayPagares[i+9]))? Double.parseDouble(arrayPagares[i+9]) : null;
						Double capitalFinal =(isValid(arrayPagares[i+10]))? Double.parseDouble(arrayPagares[i+10]) : null;
						Double abonoPorPagare =(isValid(arrayPagares[i+11]))? Double.parseDouble(arrayPagares[i+11]) : null;
						
						ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();;	
						estatus = catalogoService.obtenerElementoEspecifico("Estatus del Pagare de Prestamos", arrayPagares[i+12]);
												
						pagareGuardar.setId(idPagare);
						pagareGuardar.setConfigPrestamoAnticipo(config);
						pagareGuardar.setFechaCargo(fechaCargo);
						pagareGuardar.setCapitalInicial(capitalInicial);
						pagareGuardar.setInteresOrdinario(interesOrdinarioXPagare);
						pagareGuardar.setInteresMoratorio(cargosMoratorios);
						pagareGuardar.setAbonoCapital(abonoCapital);
						pagareGuardar.setIva(iva);
						pagareGuardar.setImportePago(importePago);
						pagareGuardar.setCapitalfinal(capitalFinal);
						pagareGuardar.setAbonoPorPagare(abonoPorPagare);
						pagareGuardar.setEstatus(estatus);
						
						entidadService.save(pagareGuardar);
						cont=0;
					}			
				}
			return true;
			}catch(Exception e){
				return false;
			}		
	}
	
	private void activacionAutomaticaPagare(){
		List<PagarePrestamoAnticipo> listaPagaresPorActivar = new ArrayList<PagarePrestamoAnticipo>();		
		double sumaTotAbonoFecActual=0;
		//se obtiene la lista de los pagares que vencen en la fecha actual (hoy)
		listaPagaresPorActivar = listaPagaresPorActivarHoy();		
		
		if(!listaPagaresPorActivar.isEmpty()){			
			for(PagarePrestamoAnticipo pagarePorActivar:listaPagaresPorActivar){
				Date fechaActual = new Date();						       
				/*se obtiene la configuracion que le corresponde a cada pagare esto para saber que tipo de movimiento es y a que agente pertenece*/
				ConfigPrestamoAnticipo config = entidadService.findById(ConfigPrestamoAnticipo.class, pagarePorActivar.getConfigPrestamoAnticipo().getId());
				List<PagarePrestamoAnticipo> listaPagares;
				try {
					/*se obtienen todos los pagares que tiene cada configuracion esto para hacer el recalculo de cargo moratorio en
					 * los pagares vencidos y despues determinar el monto total a pagar hasta la fecha actual e insertatla en estados de cuenta
					 */
					listaPagares = loadByIdConfigPrestamo(config);
					if(!listaPagares.isEmpty()){
						for(PagarePrestamoAnticipo pagares:listaPagares){
								//se calculan los dias de diferencia entre la fecha de pago y la fecha actual
								Date fechaPagoPagare = pagares.getFechaCargo();
								double diasDiferencia = Utilerias.obtenerDiasEntreFechas(fechaPagoPagare, fechaActual);								   
								//se calcula el interes moratorio a los pagares que estan vencidos o activos
							   if(diasDiferencia > 0 && ("ACTIVO".equals(pagares.getEstatus().getValor()) || "VENCIDO".equals(pagares.getEstatus().getValor()))){
								   //cargosMoratorios = (importePago * diasDiferencia * interesMoratorio) / 360;
								   DecimalFormat doubleFormat = new DecimalFormat("#.##");
								   Double cargosMoratorios =  (pagares.getImportePago() * diasDiferencia * (config.getPcteInteresMoratorio() / 100)) / 360;
								   cargosMoratorios =  new Double(doubleFormat.format(cargosMoratorios));
								   Double abonoXPagare = cargosMoratorios + pagares.getImportePago();
								   abonoXPagare = new Double(doubleFormat.format(abonoXPagare));
//								   Double abono = new Double(doubleFormat.format(abonoXPagare));
								   sumaTotAbonoFecActual += new Double(doubleFormat.format(abonoXPagare));
								   
								   //se obtiene el estatus vencido 
								   ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();;	
								   estatus = catalogoService.obtenerElementoEspecifico("Estatus del Pagare de Prestamos", "VENCIDO");
								   
								   //se actualizan los pagares vencidos cambiando su interes moratorio, abono por pagare y el estatus a vencido
								   PagarePrestamoAnticipo pagareGuardar = new PagarePrestamoAnticipo();								   
								   pagareGuardar.setId(pagares.getId());
								   pagareGuardar.setConfigPrestamoAnticipo(config);
								   pagareGuardar.setFechaCargo(pagares.getFechaCargo());
								   pagareGuardar.setCapitalInicial(pagares.getCapitalInicial());
								   pagareGuardar.setInteresOrdinario(pagares.getInteresOrdinario());
								   pagareGuardar.setInteresMoratorio(cargosMoratorios);
								   pagareGuardar.setAbonoCapital(pagares.getAbonoCapital());
								   pagareGuardar.setIva(pagares.getIva());
								   pagareGuardar.setImportePago(pagares.getImportePago());
								   pagareGuardar.setCapitalfinal(pagares.getCapitalfinal());
								   pagareGuardar.setAbonoPorPagare(abonoXPagare);
								   pagareGuardar.setEstatus(estatus);
									
								   entidadService.save(pagareGuardar);
							   }						  
						}
					}
					
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fechaActual);
					//se actualiza el estatus del pagare que se debe pagar en la fecha actual a ACTIVO
					if(fechaString.equals(pagarePorActivar.getFechaCargoString()) && "PENDIENTE".equals(pagarePorActivar.getEstatus().getValor())){
						ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();;	
						try {
							estatus = catalogoService.obtenerElementoEspecifico("Estatus del Pagare de Prestamos", "ACTIVO");
						} catch (Exception e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}					   
						pagarePorActivar.setEstatus(estatus);
						entidadService.save(pagarePorActivar);
						
						DecimalFormat doubleFormat = new DecimalFormat("#.##");
//						sumaTotAbonoFecActual+=pagarePorActivar.getAbonoPorPagare();
						sumaTotAbonoFecActual += new Double(doubleFormat.format(pagarePorActivar.getAbonoPorPagare()));
						pagarePorActivar.setAbonoPorPagare(sumaTotAbonoFecActual);
						//se cancelan los pagares que esten pendientes o en proceso
						cancelaPagareMovAgt(config,pagarePorActivar);
						//se inserta el nuevo pagare con la cantidad total a pagar (los pagares vencidos mas los activos) en estado de cta
						aplicaPagareEnMovimientosAgt(config, pagarePorActivar);
					}
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
		}
	}
	
	private List<PagarePrestamoAnticipo> listaPagaresPorActivarHoy(){
		List<PagarePrestamoAnticipo> listaPagaresPorActivar = new ArrayList<PagarePrestamoAnticipo>();
		final StringBuilder queryString = new StringBuilder("");		
		
		queryString.append("select model from PagarePrestamoAnticipo model left join fetch model.configPrestamoAnticipo left join fetch model.estatus ");
		Map<String,Object> params=new HashMap<String, Object>();		

		Date fecha=new Date();
		SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
		String fechaString=format.format(fecha);
		Date fecha2 = null;
		 try {
			fecha2 = format.parse(fechaString);
		} catch (ParseException e1) {}
				
		addCondition(queryString, "model.fechaCargo=:fecha");
		params.put("fecha",fecha2);
			
		addCondition(queryString, "model.estatus.valor like UPPER(:estatus)");
		params.put("estatus","%PENDIENTE%");
		
		String finalQuery=getQueryString(queryString);
		Query query = entityManager.createQuery(finalQuery);
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		listaPagaresPorActivar=query.getResultList();
		return listaPagaresPorActivar;
	}
	
	@Override
	public void activacionAutomaticaDePagares() throws Exception {
		activacionAutomaticaPagare();
	}
	
	
	/************************setters & getters*******************************/
	private Object val(Object str){
		return (str!=null)?str:"";
	}		
			
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
}
