package mx.com.afirme.midas2.dao.impl.recepcionDocumentos;

import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.recepcionDocumentos.RecepcionDocumentosDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.recepcionDocumentos.RecepcionDocumentos;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.lang.StringUtils;

@Stateless
public class RecepcionDocumentosDaoImpl extends EntidadDaoImpl implements RecepcionDocumentosDao{

	private ValorCatalogoAgentesService catalogoService;
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	@EJB
	private AgenteMidasService agenteMidasService;
	@EJB
	private MailService mailService;
	
	@Override
	public void actualizarStatusRecibos(DocumentosAgrupados docsAgrupados, Integer mesInicio, Integer mesFin)throws Exception{
		try{		
			ValorCatalogoAgentes pagado=catalogoService.obtenerElementoEspecifico("Estatus Calculo Comisiones", "PAGADO");
			ValorCatalogoAgentes aplicado=catalogoService.obtenerElementoEspecifico("Estatus Calculo Comisiones", "APLICADO");
			final StringBuilder queryString=new StringBuilder("");
			queryString.append(" UPDATE midas.toDetalleCalculoComisiones com SET com.ESTATUSRECIBO = ");
			queryString.append(" (SELECT ID FROM MIDAS.toValorCatalogoAgentes val  ");
			queryString.append(" WHERE val.GRUPOCATALOGOAGENTES_ID = ");
			queryString.append(" (SELECT ID FROM MIDAS.tcGrupoCatalogoAgentes grup ");
			queryString.append(" WHERE grup.DESCRIPCION LIKE 'Estatus Genericos')  "); 
			queryString.append(" AND val.CLAVE like 'ENT') ");
			queryString.append(" WHERE com.IDSOLICITUDCHEQUE IS NOT NULL AND ");
			queryString.append(" com.CLAVEESTATUS IN(");
			queryString.append(pagado.getId()+","+aplicado.getId()+") AND ");
			queryString.append(" com.IDAGENTE = ");
			queryString.append(docsAgrupados.getIdEntidad()+" AND");			
			queryString.append(" com.IDCALCULOCOMISIONES IN(SELECT ID FROM midas.toCalculoComisiones calc ");
			queryString.append(	"WHERE calc.CLAVEESTATUS IN( ");
			queryString.append(pagado.getId()+","+aplicado.getId()+") AND ");		
			queryString.append(" TRUNC(calc.FECHACALCULO) BETWEEN TO_DATE('");
			queryString.append(convertMesAFecha(mesInicio,mesFin,false));
			queryString.append("' ,'dd/MM/yyyy') AND TO_DATE('");
			queryString.append(convertMesAFecha(mesInicio,mesFin,true));
			queryString.append("' ,'dd/MM/yyyy')) ");
			Query query=entityManager.createNativeQuery(queryString.toString());	
			query.executeUpdate();
			}
			catch(Exception e){
				System.out.println(e.toString());
			}
	}
	
	private String convertMesAFecha(Integer mesInicio,Integer mesFin, boolean ultimoDia){
		Date actual= new Date();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(actual); 
		int anho = calendario.get(Calendar.YEAR);
		Calendar calFin = Calendar.getInstance();	
		if(ultimoDia){
			if(mesFin<mesInicio){
				anho++;
			}			
			calFin.set(anho, mesFin, 1);
			calFin.set(anho, mesFin, calFin.getActualMaximum(Calendar.DAY_OF_MONTH));
		}
		else{
			calFin.set(anho, mesInicio, 1);
			calFin.set(anho, mesInicio, calFin.getActualMinimum(Calendar.DAY_OF_MONTH));
		}
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		return sdf.format(calFin.getTime());
	}

	@Override
	public void saveRechazoDocumentos(RecepcionDocumentos recepDoc, Integer mesInicio, Integer mesFin) throws MidasException {
		try {
			SimpleDateFormat sdf= new SimpleDateFormat("dd/MM/yyyy");		
			Date d=sdf.parse(convertMesAFecha(mesInicio,mesFin,false));
			recepDoc.setFechaInicio(d);
			d=sdf.parse(convertMesAFecha(mesInicio,mesFin,true));
			recepDoc.setFechaFin(d);
			entityManager.persist(recepDoc);
//***************************************************************************************
			Agente agente = new Agente(); 
			agente.setId(recepDoc.getIdAgente());
			List<String> address = null;
			address = new ArrayList<String>();
			agente = agenteMidasService.loadById(agente);
			final String correo = agente.getPersona().getEmail();			
			if (StringUtils.isNotBlank(correo)) {
				address.add(correo);
			}
			Map<String,Object> genericParams = new LinkedHashMap<String, Object>();
			genericParams.put("tipoDocumento", "RECIBO DE HONORARIOS");
			genericParams.put("mesInicio",mesInicio);
			genericParams.put("mesFin",mesFin);
			
			genericParams.put("idAgente",agente.getIdAgente());
		} catch (ParseException e) {
			e.printStackTrace();
		} 
	}
	
	private String getAnio(){
		Date fecha = new Date();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		int anio = calendario.get(Calendar.YEAR);
		return  Integer.toString(anio);
		
	}
	
	@Override
	public List<EntregoDocumentosView> listaFacturasMesAnio(Long anioMes, Long idAgente) throws Exception {
		List<EntregoDocumentosView> list = new ArrayList<EntregoDocumentosView>();
		if(anioMes== null && idAgente == null){
			throw new Exception("No se ha especificado el agente y anioMes a consultar");
		}	
		String anioCast = Long.toString(anioMes);
		String anio = anioCast.substring(0,4);
		
		StringBuilder queryString  = new StringBuilder("");
		queryString.append("select  d.id as id,d.nombre as valor,d.existeDocumento as checado from MIDAS.TRDOCUMENTOSAGRUPADOS d ");
		queryString.append(" inner join MIDAS.TCCATALOGODOCUMENTOFORTIMAX c on d.IDDOCUMENTO = c.id and c.NOMBREDOCUMENTO='RECIBO' ");
		queryString.append(" where identidad = " + idAgente + " and anioMes like '"+anio+"%' order by id desc");
		
		Query query = entityManager.createNativeQuery(queryString.toString(),EntregoDocumentosView.class);
		list = query.getResultList();	
		
		return list;
	}
	
	@Override
	public void generaFacturaAgenteComis_job() throws Exception {
		
		Date fecha = new Date();
		Calendar calendario = Calendar.getInstance();
		calendario.setTime(fecha);
		String anio = Integer.toString(calendario.get(Calendar.YEAR));
		int mes = calendario.get(Calendar.MONTH);
		String nuevoMes; 
		if(mes<=9){
			nuevoMes= "0"+mes;
		}else{
			nuevoMes = Integer.toString(mes);
		}
			
		String anioMes = anio+nuevoMes;
		
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS.PKGCALCULOS_AGENTES.GENERAFACTURAAGENTECOMIS";
		try {
			LogDeMidasInterfaz.log("Entrando a generar Facturas..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("p_anioMes",anioMes);
			storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz.log("Saliendo de generar facturas..." + this, Level.INFO, null);
			
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en generar facturas..." + this, Level.WARNING, e);
			onError(e);
		}
	}
}

