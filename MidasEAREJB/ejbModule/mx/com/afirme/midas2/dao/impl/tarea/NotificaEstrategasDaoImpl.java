package mx.com.afirme.midas2.dao.impl.tarea;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.tarea.NotificaEstrategasDao;
import mx.com.afirme.midas2.dto.notificacion.estratega.NotificacionEstrategaDTO;

@Stateless
public class NotificaEstrategasDaoImpl implements NotificaEstrategasDao {
	
	private static final Logger log = LoggerFactory.getLogger(NotificaEstrategasDaoImpl.class);
	
	private static final String PROCESO_EJECUTA = "SEYCOS.PKG_COB_ESTRATEGAS.stp_obtiene_cobestra";
	private static final String[] CAMPOS_NOTIFICACIONDTO = {"poliza", "idSistema", "idOficina", "subGrupo", "idRamo", 
							"idSubrContable", "modulo", "idBanco", "idMoneda", "idTipoCuenta", "numeroCuenta", "numRecibo", 
							"fecCubreDesde", "fecCubreHasta", "impPrimaTotal", "idEmpresa", "idLineaNeg", "idAgente", "idFuncionario", 
							"idSucursal", "tipoMov", "reciboPagado", "numOperacion", "formaPago", "respCobro", "ordenId", "origen", 
							"serieRecibo", "fechaIntento", "numIntento", "descRespCobro"};
	private static final String[] CAMPOS_CURSOR_NOTIFICACION = {"POLIZA", "IDSISTEMA", "IDOFICINA", "SUBGRUPO", "ID_RAMO", 
                			"ID_SUBR_CONTABLE", "MODULO", "IDBANCO", "IDMONEDA", "IDTIPOCUENTA", "NUMEROCUENTA", "NUM_RECIBO", 
                			"F_CUBRE_DESDE", "F_CUBRE_HASTA", "IMP_PRIMA_TOTAL", "IDEMPRESA", "IDLINEANEG", "ID_AGENTE", "IDFUNCIONARIO",
                			"IDSUCURSAL", "TIPOMOV", "RECIBO_PAGADO", "NUMOPERACION", "FORMAPAGO", "RESP_COBRO", "ORDENID", "ORIGEN", 
                			"SERIERECIBO","FECHA_INTENTO", "NUM_INTENTO", "DESC_RESP_COBRO"};
	private static final String TITULO_CORREO_ESTRATEGAS = "Cobranza Seguros Afirme";
	private static final String CUERPO_MENSAJE_CORREO = "Estimados compañeros, se Anexa el Archivo Correspondiente a la fecha: " + new Date().toString() + 
			". \n\rATENTAMENTE\n\r\t" + " Seguros AFIRME, S.A. DE C.V.\n\r\t AFIRME Grupo Financiero";

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<NotificacionEstrategaDTO> obtenerNotificaciones() {
		log.info(">>>>>>>>>>>>>> [  INFO  ] obternerNotificaciones()");
		List<NotificacionEstrategaDTO> notificaciones = new ArrayList<NotificacionEstrategaDTO>();
		try {
			StoredProcedureHelper helper = new StoredProcedureHelper(PROCESO_EJECUTA, StoredProcedureHelper.DATASOURCE_MIDAS);
			helper.estableceMapeoResultados(NotificacionEstrategaDTO.class.getCanonicalName(), CAMPOS_NOTIFICACIONDTO, CAMPOS_CURSOR_NOTIFICACION);
			Calendar calendarioInicio = new GregorianCalendar();
			calendarioInicio.setTime(new Date());
			Calendar calendarioFin = new GregorianCalendar();
			calendarioFin.setTime(new Date());
			calendarioFin.set(GregorianCalendar.DAY_OF_MONTH, (calendarioFin.get(GregorianCalendar.DAY_OF_MONTH) + 1));
			
			helper.estableceParametro("pFIni", calendarioInicio.getTime());
			helper.estableceParametro("pFFin", calendarioFin.getTime());
			
			notificaciones = helper.obtieneListaResultados();
		} catch (Exception e) {
			log.info(":::::::::::::: [  ERROR  ] :::::::::::::: Hubo un problema al ejecutar el proceso STP_OBTIENE_COBESTRA::::", e);
			throw new RuntimeException(e);
		}
		log.info("<<<<<<<<<<<<<< [  INFO  ] Sale del metodo obternerNotificaciones");
		return notificaciones;
	}

	@Override
	public List<String> obtenerEstrategasDestinatarios() {
		log.info(">>>>>>>>>>>>>> [  INFO  ] Entra al metodo obtenerCorreosDestino");
		List<String> correos = new ArrayList<String>();
		
		String seleccionDestinatarios = "SELECT ID_PARAMETRO ,VALOR FROM SEYCOS.AFR_TBL_PARAMETROS WHERE ID_PARAMETRO IN(21,22) ORDER BY ID_PARAMETRO";
		
		Query destinatarios = entityManager.createNativeQuery(seleccionDestinatarios);
		
		@SuppressWarnings("unchecked")
		List<Object[]> correosArr = destinatarios.getResultList();
		
		if(!correosArr.isEmpty() && correosArr.size() > 1){
			for(Object[] mail: correosArr){
				String[] mails =(mail[1] != null ? ((String) mail[1]).split(";") : null);
				
				correos.addAll(Arrays.asList(mails));
			}
		}
		
		log.info("<<<<<<<<<<<<<< [  INFO  ] Sale del metodo obtenerCorreosDestino ");
		return correos;
	}

	@Override
	public List<String> obtenerTituloyCuerpoMensaje(){
		log.info(">>>>>>>>>>>>>> [  INFO  ] Entra al metodo obtenerTituloyCuerpoMensaje");
		
		List<String> tituloCuerpo = new ArrayList<String>();
		String datosCorreo = "SELECT TITULOMENSAJE, REPLACE(INICIO_MENSAJE ,'**FECHA**',TO_CHAR(SYSDATE,'dd/mm/rrrr')), CUERPO_MENSAJE, FINAL_MENSAJE, CORREO_ORIGEN , PERSONA_ORIGEN FROM SEYCOS.AFR_TBL_MENSAJE_CORREO   WHERE ID_NOTIF_DESTINATARIO = 32";
		
		Query datoscorreoQ = entityManager.createNativeQuery(datosCorreo);
		
		@SuppressWarnings("unchecked")
		List<Object[]> mensajeCorreo = datoscorreoQ.getResultList();
		
		String tituloMensaje = mensajeCorreo.get(0)[0] != null ? (String) mensajeCorreo.get(0)[0] : TITULO_CORREO_ESTRATEGAS;
		
		String cuerpoMensaje = mensajeCorreo.get(0)[1] != null ? (String) mensajeCorreo.get(0)[1] : CUERPO_MENSAJE_CORREO;
		
		tituloCuerpo.add(tituloMensaje);
		tituloCuerpo.add(cuerpoMensaje);
		
		log.info("<<<<<<<<<<<<<< [  INFO  ] Sale del metodo obtenerTituloyCuerpoMensaje ");
		return tituloCuerpo;
	}

}
