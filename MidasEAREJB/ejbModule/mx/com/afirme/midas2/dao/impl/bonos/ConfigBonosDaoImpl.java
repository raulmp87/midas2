package mx.com.afirme.midas2.dao.impl.bonos;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.ramo.RamoSeccionFacadeRemote;
import mx.com.afirme.midas2.dao.bonos.ConfigBonosDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAplicaAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAplicaPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCentroOperacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCobertura;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoEjecutivo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoGerencia;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoLineaVenta;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPrioridad;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoProducto;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRamo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoClaveAmis;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSeccion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSituacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSubramo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.bonos.ConfigBonosDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.fuerzaventa.ConfigBonosNegView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.ExcepcionesPolizaView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroOperacionService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ConfigBonosDaoImpl implements ConfigBonosDao{
	private EntidadService entidadService;
	private ValorCatalogoAgentesService catalogoService;
//	private ProductoFacadeRemote productoFacade;
	private TipoPolizaFacadeRemote tipoPolizaFacade;
	private RamoTipoPolizaFacadeRemote ramoTipoPolizaFacade;
	private SubRamoFacadeRemote subRamoFacade;
	private RamoSeccionFacadeRemote ramoSeccionFacade;
	private CoberturaSeccionFacadeRemote coberturaSeccionFacade;
	private CentroOperacionService centroOperacionService;
	private GerenciaJPAService gerenciaService;
	private EjecutivoService ejecutivoService;
	private PromotoriaJPAService promotoriaService;
		
	private EntidadHistoricoDao entidadHistoricoDao;
	private static Logger log = Logger.getLogger("ConfigBonosDaoImpl");
	
	@EJB
	public void setEntidadHistoricoDao(EntidadHistoricoDao entidadHistoricoDao) {
		this.entidadHistoricoDao = entidadHistoricoDao;
	}

	@Override
	public List<ValorCatalogoAgentes> getCatalogoModoEjecucion()throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Modos de Ejecucion de bonos");
	}

	@Override
	public List<ValorCatalogoAgentes> getCatalogoPeriodo() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Periodos de Ejecucion de Bonos");
	}

	@Override
	public List<CentroOperacionView> getCentroOperacionList() throws Exception {
		//return centroOperacionService.findByFiltersView(filtroCentroOperacion);
		return centroOperacionService.getList(true);
	}

	@Override
	public List<EjecutivoView> getEjecutivoList() throws Exception {
		//return entidadService.findAll(Ejecutivo.class);
		return ejecutivoService.getList(true);
	}

	@Override
	public List<GerenciaView> getGerenciaList() throws Exception {
		//return entidadService.findAll(Gerencia.class);
		return gerenciaService.getList(true);
	}

	@Override
	public List<ValorCatalogoAgentes> getPrioridadList() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Prioridades de Agente");
	}

	@Override
	public List<PromotoriaView> getPromotoriaList() throws Exception {
		//return entidadService.findAll(Promotoria.class);
		return promotoriaService.getList(true);
	}

	@Override
	public List<ValorCatalogoAgentes> getSituacionList() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Estatus de Agente (Situacion)");
	}

	@Override
	public List<ValorCatalogoAgentes> getTipoDeAgenteList() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Clasificacion de Agente");
	}

	@Override
	public List<ValorCatalogoAgentes> getTipoPromotoriaList() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Tipos de Promotoria");
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigBonos> findByFilters(ConfigBonos filtro) {
		Map<String,Object> params=new HashMap<String, Object>();
		List<ConfigBonos> lista=new ArrayList<ConfigBonos>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ConfigBonos model ");
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, " model.id=:id");
				params.put("id", filtro.getId());
			}else{
				ValorCatalogoAgentes modoAplicacion=filtro.getModoAplicacion();
				if(modoAplicacion!=null && modoAplicacion.getId()!=null){
					addCondition(queryString, " model.modoAplicacion.id=:idModoAplicacion");
					params.put("idModoAplicacion",  modoAplicacion.getId());
				}
				ValorCatalogoAgentes periodoAjuste=filtro.getPeriodoAjuste();
				if(periodoAjuste!=null && periodoAjuste.getId()!=null){
					addCondition(queryString, " model.periodoEjecucion.id=:idPeriodoEjecucion");
					params.put("idPeriodoEjecucion",  periodoAjuste.getId());
				}
				ValorCatalogoAgentes produccionSobre=filtro.getProduccionSobre();
				if(produccionSobre!=null && produccionSobre.getId()!=null){
					addCondition(queryString, " model.produccionSobre.id=:idProduccionSobre");
					params.put("idProduccionSobre",  produccionSobre.getId());
				}
				ValorCatalogoAgentes siniestralidadSobre=filtro.getSiniestralidadSobre();
				if(siniestralidadSobre!=null && siniestralidadSobre.getId()!=null){
					addCondition(queryString, " model.siniestralidadSobre.id=:idSiniestralidadSobre");
					params.put("idSiniestralidadSobre",  siniestralidadSobre.getId());
				}
			}
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		lista=query.getResultList();
		return lista;
	}

	@Override
	public ConfigBonos loadById(ConfigBonos config) throws Exception{
		if(config==null ||config.getId()==null){
			throw new Exception("Favor de proporcionar la configuracion");
		}
		ConfigBonos filtro=new ConfigBonos();
		filtro.setId(config.getId());
		List<ConfigBonos> list=findByFilters(filtro);
		if(list!=null && !list.isEmpty()){
			filtro=list.get(0);
			if(filtro!=null && filtro.getId()!=null){
				List<ConfigBonoCentroOperacion> centros=getCentrosOperacionPorConfiguracion(filtro);
				List<ConfigBonoGerencia> gerencias=getGerenciasPorConfiguracion(filtro);
				List<ConfigBonoEjecutivo> ejecutivos=getEjecutivosPorConfiguracion(filtro);
				List<ConfigBonoPromotoria> promotorias=getPromotoriasPorConfiguracion(filtro);
				List<ConfigBonoTipoAgente> tiposAgente=getTiposAgentePorConfiguracion(filtro);
				List<ConfigBonoTipoPromotoria> tiposPromotoria=getTiposPromotoriaPorConfiguracion(filtro);
				List<ConfigBonoPrioridad> prioridades=getPrioridadesPorConfiguracion(filtro);
				List<ConfigBonoSituacion> situaciones=getSituacionesPorConfiguracion(filtro);
				List<ConfigBonoLineaVenta> lineaVenta = getLineaVentaPorConfiguracion(filtro);
				filtro.setListaCentroOperaciones(centros);
				filtro.setListaGerencias(gerencias);
				filtro.setListaEjecutivos(ejecutivos);
				filtro.setListaPromotorias(promotorias);
				filtro.setListaTipoAgentes(tiposAgente);
				filtro.setListaTiposPromotoria(tiposPromotoria);
				filtro.setListaPrioridades(prioridades);
				filtro.setListaSituaciones(situaciones);
				filtro.setLineaVenta(lineaVenta);
			}
		}
		return filtro;
	}
	
	/*TODO Servicio Temporal en lo que se hace un refactor para corregir el modelo de configuracion de Bonos (Si estuviera bien esto no seria necesario)*/
	@Override
	public ConfigBonos findById(Long configBonosId, Date asOf) throws Exception {
		
		ConfigBonos config = entidadHistoricoDao.findById(ConfigBonos.class, "id", configBonosId, asOf);
				
		if (config == null) {
			
			throw new Exception("Favor de proporcionar la configuracion");
			
		}
				
		List<ConfigBonoCentroOperacion> centros=getCentrosOperacionPorConfiguracion(config, asOf);
		List<ConfigBonoGerencia> gerencias=getGerenciasPorConfiguracion(config, asOf);
		List<ConfigBonoEjecutivo> ejecutivos=getEjecutivosPorConfiguracion(config, asOf);
		List<ConfigBonoPromotoria> promotorias=getPromotoriasPorConfiguracion(config, asOf);
		List<ConfigBonoTipoAgente> tiposAgente=getTiposAgentePorConfiguracion(config, asOf);
		List<ConfigBonoTipoPromotoria> tiposPromotoria=getTiposPromotoriaPorConfiguracion(config, asOf);
		List<ConfigBonoPrioridad> prioridades=getPrioridadesPorConfiguracion(config, asOf);
		List<ConfigBonoSituacion> situaciones=getSituacionesPorConfiguracion(config, asOf);
		List<ConfigBonoLineaVenta> lineaVenta = getLineaVentaPorConfiguracion(config, asOf);
				
		config.setListaCentroOperaciones(centros);
		config.setListaGerencias(gerencias);
		config.setListaEjecutivos(ejecutivos);
		config.setListaPromotorias(promotorias);
		config.setListaTipoAgentes(tiposAgente);
		config.setListaTiposPromotoria(tiposPromotoria);
		config.setListaPrioridades(prioridades);
		config.setListaSituaciones(situaciones);
		config.setLineaVenta(lineaVenta);
		////////////////////////////////////////////////////////
		config.setProductos(getProductosPorConfiguracion(config, asOf));
		config.setRamos(getRamosPorConfiguracion(config, asOf));
		config.setSubramos(getSubRamosPorConfiguracion(config, asOf));
		config.setSecciones(getSeccionesPorConfiguracion(config, asOf));
		config.setCoberturas(getCoberturasPorConfiguracion(config, asOf));
		
		//TODO implementar metodos publicos restantes
		config.setListaAgentes(getAgentesPorConfiguracion(config, asOf));
		config.setListaConfigPolizas(getPolizasPorConfiguracion(config, asOf));
		config.setListaAplicaAgentes(getAplicaAgentesPorConfiguracion(config, asOf));
		config.setListaAplicaPromotorias(getAplicaPromotoriasByIdConfigBono(config.getId(), asOf));
		
		return config;
		
	}
	
	/**
	 * Desactiva una configuracion de bonos por su id
	 */
	@Override
	public void deleteConfiguration(ConfigBonos config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion que desea eliminar");
		}
		ConfigBonos configuracion=entidadService.findById(ConfigBonos.class,config.getId());
		if(configuracion==null){
			onError("La configuracion con la clave "+config.getId()+" no existe");
		}
		configuracion.setActivo(0);//Se inactiva la configuracion
		entidadService.save(configuracion);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigBonosDTO> findByFiltersView(ConfigBonosDTO filtro) {
//		ConfigBonosDTO obj = entidadService.findById(ConfigBonosDTO.class, filtro.);
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<ConfigBonosDTO> lista=new ArrayList<ConfigBonosDTO>();
		int index=0;
		SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");  
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select /*+INDEX(bon TOCONFIGBONOS_PK)*/ distinct "); 
		queryString.append(" BON.ID as id,");
		queryString.append(" BON.CLAVEACTIVO as activo,");
		queryString.append(" BON.descripcionBono as descripcion,"); 
		queryString.append(" BON.CLAVEPAGOSINFACTURA as pagoSinFactura,");
		queryString.append(" BON.FECHAINICIOVIGENCIA as fechaInicioVigencia,");
		queryString.append(" BON.FECHAFINVIGENCIA as fechaFinVigencia,");
		queryString.append(" BON.IDTOPRODUCTO as idProducto,");
		queryString.append(" BON.IDTCRAMO as idRamo,");
		queryString.append(" BON.IDTCSUBRAMO as idSubRamo,");
		queryString.append(" BON.IDTOSECCION as idSeccion,");
		queryString.append(" BON.IDTOCOBERTURA as idCobertura,");
		queryString.append(" BON.IDTOMONEDA as idMoneda,");
		queryString.append(" BON.CLAVEAJUSTEDEOFICINA as ajusteDeOficina,");
		queryString.append(" BON.IDPERIODODEAJUSTE as periodoAjuste,");
		queryString.append(" BON.IDBONOAJUSTAR as idBonoAjustar,");
		queryString.append(" BON.IDPRODUCCIONSOBRE as produccionSobre,");
		queryString.append(" BON.IDSINIESTRALIDADSOBRE as siniestralidadSobre,");
		queryString.append(" BON.PRODUCCIONMINIMA as produccionMinima,");
		queryString.append(" BON.pctSiniestralidadMaxima AS porcentajeSiniestralidadMaxima,");
		queryString.append(" BON.claveGlobal AS global,");
		queryString.append(" BON.PERIODOINICIAL AS periodoInicial,");
		queryString.append(" BON.PERIODOFINAL AS periodoFinal,");
		queryString.append(" BON.CLAVEMODOAPLICACION AS modoAplicacion,");
		queryString.append(" BON.clavePctImporteDirecto AS porcentajeImporteDirecto,");
		queryString.append(" BON.pctAplicacionDirecto AS porcentajeAplicacionDirecto,");
		queryString.append(" BON.importeAplicacionDirecto AS importeAplicacionDirecto,");
		queryString.append(" BON.clavePctImporteRangos AS porcentajeImporteRangos,");
		queryString.append(" BON.IDPERIODOCOMPARACION as periodoComparacion,");
		queryString.append(" BON.claveTipoBeneficiario as tipoBeneficiario,");
		queryString.append(" BON.claveTodasPromotorias AS todasPromotorias,");
		queryString.append(" BON.claveTodosAgentes AS todosAgentes,");
		queryString.append(" tipoBono.valor as descripcionTipoBono");
		queryString.append(" from MIDAS.TOCONFIGBONOS bon");
		queryString.append(" inner join MIDAS.tcGrupoCatalogoAgentes gTipoBono on(gTipoBono.descripcion='Tipos de Bono')");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoBono on(tipoBono.grupoCatalogoAgentes_id=gTipoBono.id and tipoBono.id=bon.idTipoBono)");
		queryString.append(" LEFT join MIDAS.TRCONFIGBONOAplicaPromotoria aplicaPromotoria on(aplicaPromotoria.configBono_id=bon.id)");
		queryString.append(" LEFT join MIDAS.TRCONFIGBONOLINEAVENTA liineaVenta on(liineaVenta.configBono_id=bon.id)" );
		queryString.append(" WHERE ");
		if(filtro!=null){
			if(filtro.getId()!=null){
				queryString.append(" (bon.id="+filtro.getId()+") ");
				index++;
			}else{
			if(filtro instanceof ConfigBonosDTO){
				ConfigBonosDTO filter=(ConfigBonosDTO)filtro;
				if(filter.getDescripcion()!=null && !StringUtils.isBlank(filter.getDescripcion())){
					queryString.append(" (UPPER(TRIM(bon.descripcionBono)) like '%'||UPPER(TRIM('"+filter.getDescripcion()+"'))||'%' )");
					index++;
				}
				if(filter.getIdCentroOperacion()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append(" exists ( ");
					queryString.append(" 	    select centro.configBono_id,centro.CENTROOPERACION_ID from MIDAS.TRCONFIGBONOCentroOperacion centro where centro.configBono_id = bon.id");
					queryString.append(" 	    and centro.CENTROOPERACION_ID="+filter.getIdCentroOperacion());
					queryString.append(" 	)");
					index++;
				}
				if(filter.getIdGerencia()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append(" exists(");
					queryString.append("		select gerencia.configBono_id from MIDAS.TRCONFIGBONOGerencia gerencia where gerencia.configBono_id = bon.id");
					queryString.append("         and gerencia.gerencia_id = "+filter.getIdGerencia());
					queryString.append("	)");
					index++;
				}
				if(filter.getIdEjecutivo()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append(" exists( ");
					queryString.append("	    select ejecutivo.configBono_id from  MIDAS.TRCONFIGBONOEjecutivo ejecutivo where ejecutivo.configBono_id =bon.id ");
					queryString.append("		and ejecutivo.ejecutivo_id="+filter.getIdEjecutivo());
					queryString.append("	) ");
					index++;
				}
				if(filter.getIdPromotoria()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append(" exists( ");
				    queryString.append("	select promotoria.configBono_id from MIDAS.TRCONFIGBONOPromotoria promotoria where promotoria.configBono_id = bon.id");
				    queryString.append(" 	and promotoria.promotoria_id="+filter.getIdPromotoria());
				    queryString.append(" ) ");
					index++;
				}
				if(filter.getIdTipoAgente()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append(" exists( ");
					queryString.append("	    select tipoAgente.configBono_id from MIDAS.TRCONFIGBONOTipoAgente tipoAgente where tipoAgente.configBono_id = bon.id");
					queryString.append("        and tipoAgente.idTipoAgente="+filter.getIdTipoAgente());
					queryString.append("	) ");
					index++;
				}
				if(filter.getIdTipoPromotoria()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append("     exists(");
					queryString.append("         select tipoPromotoria.configBono_id from MIDAS.TRCONFIGBONOTipoPromotoria tipoPromotoria where tipoPromotoria.configBono_id = bon.id");
					queryString.append("         AND tipoPromotoria.idTipoPromotoria="+filter.getIdTipoPromotoria());
					queryString.append("	) ");
					index++;
				}
				if(filter.getIdProridad()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append("    exists(");
					queryString.append("    select prioridad.configBono_id from MIDAS.TRCONFIGBONOPrioridad prioridad where prioridad.configBono_id = bon.id");
					queryString.append("    and prioridad.idPrioridadAgente="+filter.getIdProridad());
					queryString.append("    )");
					index++;
				}
				if(filter.getIdSituacion()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append("    exists(");
					queryString.append("    select situacion.configBono_id from MIDAS.TRCONFIGBONOSituacion situacion  where situacion.configBono_id = bon.id");
					queryString.append("    and situacion.idSituacionAgente="+filter.getIdSituacion());
					queryString.append("    ) ");
					index++;
				}
				if(filter.getIdRamo()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append("	 exists(");
					queryString.append("        select ramo.idconfigBono from MIDAS.TRCONFIGBONORamo ramo where ramo.idconfigBono = bon.id");
					queryString.append(" 		and ramo.idramo= "+filter.getIdRamo());
					queryString.append("	) ");
					index++;
				}
				if(filter.getIdProducto()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append("	exists( ");
					queryString.append("	select producto.idconfigBono from MIDAS.TRCONFIGBONOPRODUCTO producto where producto.idconfigBono = bon.id");
					queryString.append("	and producto.IDPRODUCTO= "+filter.getIdProducto());
					queryString.append("	)");
					index++;
				}
				ValorCatalogoAgentes modoEjecucion=filter.getModoAplicacion();
				if(modoEjecucion!=null && modoEjecucion.getId()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append(" bon.idModoAplicacion="+modoEjecucion.getId());
					index++;
				}
				if(filter.getFechaAltaInicio()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append( " bon.fechaCreacion>= TO_DATE('"+formato.format(filter.getFechaAltaInicio())+"','dd/MM/YYYY')");
					index++;
				}
				if(filter.getFechaAltaFin()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append("bon.fechaCreacion<= TO_DATE('"+formato.format(filter.getFechaAltaFin())+"','dd/MM/YYYY')");
					index++;
				}
				if(filter.getPeriodoAjuste()!=null && filter.getPeriodoAjuste().getId()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append("bon.idPeriodoDeAjuste= "+filter.getPeriodoAjuste().getId());
					index++;
				}
				if(isNotNull(filter.getActivo())){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append(" bon.claveActivo= "+filter.getActivo());
					index++;
				}
				//revisar ****************************************
				ValorCatalogoAgentes periodoEjecucion=filter.getPeriodoAjuste();
	//			if(periodoEjecucion!=null && periodoEjecucion.getId()!=null){
	//				addCondition(queryString, " bon.idPeriodoAjuste=?");
	//				params.put(index,  periodoEjecucion.getId());
	//				index++;
	//			}
				
				if(filter.getNombreAgente()!=null && !filter.getNombreAgente().isEmpty()){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append("  exists( ");
					queryString.append(" 	    select nombreCompleto from MIDAS.vw_persona p ");
					queryString.append(" 	    inner join MIDAS.TOAGENTE age on  age.IDPERSONA = p.IDPERSONA");
					queryString.append(" 	    inner join MIDAS.TRCONFIGBONOAgente agente on agente.AGENTE_ID =age.IDAGENTE and agente.configBono_id = bon.id");
					queryString.append(" 	    where (UPPER(TRIM(pers.nombreCompleto)) like '%'||UPPER(TRIM("+filter.getNombreAgente()+"))||'%' ) ");
					queryString.append(" 	)");
					index++;
				}
				
				if(filter.getIdPoliza()!=null){
					if(index!=0){
						queryString.append(" and ");
					}
					queryString.append(" exists( ");
					queryString.append("     select poliza.CONFIGBONO_ID from midas.TRCONFIGBONOPOLIZA poliza where poliza.CONFIGBONO_ID = bon.id");
					queryString.append("     poliza.NUMEROPOLIZA= "+filter.getIdPoliza());
					queryString.append(" 	) ");
					index++;
				}
			}
		  }
		}
		if (index==0){
			int lengthWhere=" WHERE ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		String finalQuery = queryString.toString()+" ORDER BY BON.descripcionBono ASC";
		Query query=entityManager.createNativeQuery(finalQuery,ConfigBonosDTO.class);
		lista=query.getResultList();
		return lista;
	}

	@Override
	public List<ConfigBonoCentroOperacion> getCentrosOperacionPorConfiguracion(ConfigBonos config) throws Exception {
		return getCentrosOperacionPorConfiguracion(config, null);
	}

	@Override
	public List<ConfigBonoEjecutivo> getEjecutivosPorConfiguracion(ConfigBonos config) throws Exception {
		return getEjecutivosPorConfiguracion(config, null);
	}
	
	@Override
	public List<ConfigBonoGerencia> getGerenciasPorConfiguracion(ConfigBonos config) throws Exception {
		return getGerenciasPorConfiguracion(config, null);
	}
	
	@Override
	public List<ConfigBonoPrioridad> getPrioridadesPorConfiguracion(ConfigBonos config) throws Exception {
		return getPrioridadesPorConfiguracion(config, null);
	}

	@Override
	public List<ConfigBonoPromotoria> getPromotoriasPorConfiguracion(ConfigBonos config) throws Exception {
		return getPromotoriasPorConfiguracion(config, null);
	}
	
	@Override
	public List<ConfigBonoSituacion> getSituacionesPorConfiguracion(ConfigBonos config) throws Exception {
		return getSituacionesPorConfiguracion(config, null);
	}

	@Override
	public List<ConfigBonoTipoAgente> getTiposAgentePorConfiguracion(ConfigBonos config) throws Exception {
		return getTiposAgentePorConfiguracion(config, null);
	}

	@Override
	public List<ConfigBonoTipoPromotoria> getTiposPromotoriaPorConfiguracion(ConfigBonos config) throws Exception {
		return getTiposPromotoriaPorConfiguracion(config, null);
	}
		
	@Override
	public List<GenericaAgentesView> getCoberturasPorLineasNegocio(List<ConfigBonoSeccion> lineasNegocio) throws Exception{
		List<GenericaAgentesView> list=new ArrayList<GenericaAgentesView>();
		if(lineasNegocio!=null){
			String listaNegocio = obtenerIdNegocios(lineasNegocio);
//				if(isNotNull(idSeccion)){
					List<GenericaAgentesView> listTemp=getCoberturasPorLineasNegocioView(listaNegocio);
					if(!isEmptyList(listTemp)){
						list.addAll(listTemp);
					}
				}
//			}
//		}
		return list;
	}
	public String obtenerIdNegocios(List<ConfigBonoSeccion> lineasNegocio) {
		StringBuilder listaNegocio = new StringBuilder("");
		Collections.reverse(lineasNegocio);
		for(ConfigBonoSeccion lineaNegocio:lineasNegocio){
			listaNegocio.append(lineaNegocio.getIdSeccion().toString()).append(",");
		}		
		return listaNegocio.toString();
	}

	/**
	 * Se obtienen la lista de negocios por ramos
	 */
	@Override
	public List<GenericaAgentesView> getLineasNegocioPorRamos(List<ConfigBonoRamo> ramos) throws Exception {
		List<GenericaAgentesView> list=new ArrayList<GenericaAgentesView>();
		if(ramos!=null){
			String listaRamos = obtenerIdRamos(ramos);
//				if(isNotNull(idRamo)){
					List<GenericaAgentesView> listTemp=getLineaNegocioPorRamoView(listaRamos);
					if(!isEmptyList(listTemp)){
						list.addAll(listTemp);
					}
//				}
//			}
		}
		return list;
	}
	public String obtenerIdRamos(List<ConfigBonoRamo> ramos) {
		StringBuilder listaRamos = new StringBuilder("");
		Collections.reverse(ramos);
		for(ConfigBonoRamo ramo:ramos){
			listaRamos.append(ramo.getIdRamo().toString()).append(",");
		}		
		return listaRamos.toString();
	}

	/**
	 * Se obtienen las lineas de Venta[Autos-A,Vida-V,Danios-D]
	 */
	@Override
	public List<ValorCatalogoAgentes> getLineasVenta() throws Exception {
		return catalogoService.obtenerElementosPorCatalogo("Lineas de Venta");
	}
	/**
	 * Se obtienen la lista de productos de acuerdo a la lista de ventas a encontrar. 
	 */
	@Override
	public List<GenericaAgentesView> getProductosPorLineaVenta(String lineasVenta) throws Exception {
		List<GenericaAgentesView> list=new ArrayList<GenericaAgentesView>();
		if(!lineasVenta.equals("") && !lineasVenta.isEmpty()){
//			for(ValorCatalogoAgentes linea:lineasVenta){
//				String clave=linea.getClave();
				//Se buscan los productos por esa clave de negocio A-Autos,V-Vida,D-Danios
//				List<ProductoDTO> productos=productoFacade.findByProperty("claveNegocio",clave);
				List<GenericaAgentesView> productos=getProductosPorLineaVentaView(lineasVenta);
				
				if(productos!=null && !productos.isEmpty()){
					list.addAll(productos);
				}
//			}
		}
		return list;
	}
	@Override
	public List<GenericaAgentesView> getRamosPorProductos(List<ConfigBonoProducto> productos)throws Exception{
		List<GenericaAgentesView> list=new ArrayList<GenericaAgentesView>();
		if(!isEmptyList(productos)){
			String idProducto = obtenerIdProductos(productos);
				if(isNotNull(idProducto)){
					List<GenericaAgentesView> listTemp=getRamosPorProductosView(idProducto);
					if(!isEmptyList(listTemp)){
						list.addAll(listTemp);
					}
				}
			}
		
		return list;
	} 
	
	public String obtenerIdProductos(List<ConfigBonoProducto> productos) {
		StringBuilder idProducto = new StringBuilder("");
		Collections.reverse(productos);
		for(ConfigBonoProducto producto:productos){
			 idProducto.append(producto.getIdProducto().toString()).append(",");
		}	
		return idProducto.toString();
	}

	@Override
	public List<GenericaAgentesView> getSubramosPorRamos(List<ConfigBonoRamo> ramos)throws Exception{
		List<GenericaAgentesView> list=new ArrayList<GenericaAgentesView>();
		//if(!isEmptyList(ramos)){
			if(ramos!=null || !ramos.isEmpty()){
				List<GenericaAgentesView> subramos= getSubramosPorRamoView(obtenerIdRamos(ramos));
				if(subramos!=null && !subramos.isEmpty()){
					list.addAll(subramos);
				}
//			}
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getProductosPorLineaVentaView(String lineasVenta) throws Exception{
		List<GenericaAgentesView> lista= new ArrayList<GenericaAgentesView>();		
		final StringBuilder queryString = new StringBuilder("");
		if(!lineasVenta.equals("")){
			lineasVenta = lineasVenta.substring(0, lineasVenta.length()-1);
			lineasVenta = lineasVenta.replaceAll(",", "','");
			queryString.append("select p.idToProducto AS id,'P'||TRIM(p.CODIGOPRODUCTO)||', V'||p.VERSIONPRODUCTO||' - '||p.descripcionProducto AS descripcion, p.descripcionProducto from MIDAS.toproducto p where claveNegocio in('" + lineasVenta +"') and CLAVEACTIVO = 1 order by descripcionProducto");
		}
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
		lista= query.getResultList();
		return lista;
	}
	
	/**
	 * Obtiene la lista de ramos por producto
	 * @param idProducto
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getRamosPorProductosView(String idProducto) throws Exception{
		List<GenericaAgentesView> listaRamos = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		idProducto = idProducto.substring(0, idProducto.length()-1);
		if(!idProducto.equals("")){//isNotNull(idProducto)){			
			queryString.append(" select distinct r.idTcRamo AS id,'R'|| TRIM(r.CODIGORAMO)||' - '||descripcionRamo AS descripcion, r.descripcionRamo from MIDAS.TCRAMO r "); 
			queryString.append(" inner join MIDAS.TRRAMOTIPOPOLIZA rtp on (RTP.IDTCRAMO =  R.IDTCRAMO) "); 
			queryString.append(" inner join  MIDAS.TOTIPOPOLIZA tp on (TP.IDTOTIPOPOLIZA = RTP.IDTOTIPOPOLIZA) ");//			
			queryString.append(" where tp.idToProducto in ("+idProducto+") order by r.descripcionRamo");
			Query query = entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
			query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
			listaRamos = query.getResultList();
			return listaRamos;
		}
		return null;
	}

	/**
	 * 
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getSubramosPorRamoView(String ramos) throws Exception {
		List<GenericaAgentesView> listaSubRamos = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		if(!ramos.equals("")){			
			ramos = ramos.substring(0, ramos.length()-1);
			queryString.append(" select idTcSubRamo AS id,'S'|| TRIM(CODIGOSUBRAMO) ||' - '||descripcionSubRamo||' - (R'||TRIM(R.CODIGORAMO)||')' AS descripcion, s.descripcionSubRamo from MIDAS.TCSUBRAMO s ");
     		queryString.append("inner join MIDAS.TCRAMO r on (R.IDTCRAMO = S.IDTCRAMO ) ");
     	    queryString.append("where s.idTcRamo in("+ramos+") order by s.descripcionSubRamo");			
			Query query = entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
			query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
			listaSubRamos = query.getResultList();
			return listaSubRamos;
		}
		return null;
	}
	
	/**
	 * Obtiene las lineas de negocio por ramo
	 * @param idRamo clave del ramo
	 * @return 
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getLineaNegocioPorRamoView(String ramos)throws Exception {
		List<GenericaAgentesView> listaLineaNegocio = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		if(!ramos.equals("")){			
			ramos = ramos.substring(0, ramos.length()-1);			
			queryString.append(" select idtoseccion as id,descripcionseccion as descripcion from MIDAS.toseccion ");
			queryString.append(" where idtoseccion in (select distinct idtoseccion from MIDAS.TRRAMOSECCION where idtcramo in ("+ramos+"))");			
			Query query = entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
			query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
			listaLineaNegocio = query.getResultList();
			return listaLineaNegocio;
		}
		return null;
	}

	/**
	 * Obtiene las coberturas de una linea de negocio, este metodo se utiliza para obtener las coberturas
	 * de cada linea de negocio dentro del servicio.
	 * @param idLineaNegocio linea de negocio para obtener sus coberturas
	 * @return coberturas ligadas a la linea de negocio especificada como argumento de este metodo
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getCoberturasPorLineasNegocioView(String lineaNegocio) throws Exception {
		List<GenericaAgentesView> listaCoberturas = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		if(!lineaNegocio.equals("")){		
			lineaNegocio = lineaNegocio.substring(0, lineaNegocio.length()-1);
			queryString.append(" select c.idToCobertura AS id,  trim(c.idToCobertura)||' - '||c.NOMBRECOMERCIALCOBERTURA ||' - (LN'||MAX(TRIM(S.CODIGOSECCION))||')'  AS descripcion from MIDAS.TOCOBERTURA c ");
			queryString.append(" inner join MIDAS.TOCOBERTURASECCION cs on (CS.IDTOCOBERTURA =  C.IDTOCOBERTURA) ");
			queryString.append(" INNER JOIN MIDAS.TOSECCION s on (CS.IDTOSECCION = S.IDTOSECCION ) ");
			queryString.append(" where cs.idToSeccion in ("+lineaNegocio+") ");			
			queryString.append(" group by c.idToCobertura,c.NOMBRECOMERCIALCOBERTURA ");	
			queryString.append(" order by c.NOMBRECOMERCIALCOBERTURA ");				
		Query query = entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
		listaCoberturas = query.getResultList();
		return listaCoberturas;
		}
		return null;
	}
	
	/**
	 * Obtiene la lista de gerencias que no esten en la lista de centros de operacion a excluir
	 * @param configGerencias
	 * @param configCentrosOperacion
	 * @return
	 */
	public List<GerenciaView> getGerenciasConCentrosOperacionExcluyentes(List<Long> configGerencias,List<Long> configCentrosOperacion){
		return gerenciaService.findGerenciasConCentroOperacionExcluyentes(configGerencias, configCentrosOperacion);
	}
	/**
	 * Obtiene la lista de ejecutivos excluyendo aquellos que pertenezcan a la lista de gerencias a excluir
	 * @param configEjecutivos
	 * @param configGerencias
	 * @return
	 */
	@Override
	public List<EjecutivoView> getEjecutivosConGerenciasExcluyentes(List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros){
		return ejecutivoService.findEjecutivosConGerenciasExcluyentes(configEjecutivos, configGerencias,configCentros);
	}
	/**
	 * Obtiene la lista de promotorias excluyendo aquellas que pertenezcan a la lista de ejecutivos proporcionada.
	 * @param configPromotorias
	 * @param configEjecutivos
	 * @return
	 */
	@Override
	public List<PromotoriaView> getPromotoriasConEjecutivosExcluyentes(List<Long> configPromotorias,List<Long> configEjecutivos,List<Long> configGerencias,List<Long> configCentros){
		return promotoriaService.findPromotoriaConEjecutivosExcluyentes(configPromotorias, configEjecutivos,configGerencias,configCentros);
	}
	/**
	 * Obtiene la lista de negocios que no esta asociado a una excepcion
	 * @param 
	 * @param 
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<ConfigBonosNegView> getListNegociosDesasociado(ConfigBonosNegView filtro){
		final StringBuilder queryString = new StringBuilder();
		List<ConfigBonosNegView> negociosLista = new ArrayList<ConfigBonosNegView>();
		queryString.append(" SELECT distinct ");
		queryString.append(" IDTONEGOCIO as idNegocio, CLAVEESTATUS as claveEstatus, DESCRIPCIONNEGOCIO as descripcionNegocio,pctMaximoDescuento as pcteDescuento");
		queryString.append(" FROM MIDAS.TONEGOCIO ");
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		int index=1;
		if(isNotNull(filtro.getIdNegocio())){
			addOrCondition(queryString, "TONEGOCIO.idToNegocio= ? ");
			params.put(index, filtro.getIdNegocio());
		}
		if(isNotNull(filtro.getClaveEstatus())){
			addOrCondition(queryString, "TONEGOCIO.claveEstatus = ?");
			params.put(index, filtro.getClaveEstatus());
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString),ConfigBonosNegView.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		query.setMaxResults(100);
		negociosLista=query.getResultList();
		return negociosLista;
	}
	
	@Override
	public List <ExcepcionesPolizaView> getPolizaExcepsiones(ExcepcionesPolizaView idPoliza, ConfigBonos configuracion){
		final StringBuilder queryString = new StringBuilder();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<ExcepcionesPolizaView> polizaLista = new ArrayList<ExcepcionesPolizaView>();
		int index=1;
		if (isNotNull(idPoliza)){
			queryString.append(" select DISTINCT  idPoliza, numeroPolizaSeycosString, NombreCompleto, descripcionPromotoria ");
			queryString.append(" from( ");
			queryString.append(" select DISTINCT p.id_cotizacion as idPoliza, lpad(p.id_centro_emis,3,0)||'-'||lpad(num_poliza,10,0)||'-'||lpad(p.num_renov_pol,2,0) as numeroPolizaSeycosString,vef.nombre as NombreCompleto, nom_supervisoria as descripcionPromotoria ");
			queryString.append(" from seycos.pol_poliza p  inner join seycos.vw_est_fv vef on vef.id_agente = p.id_agente ");
			queryString.append(" inner join seycos.conv_midas_seycos cms on cms.ID_SEYCOS = p.ID_COTIZACION ");
			queryString.append(" where p.id_version_pol = 1 ");
			queryString.append(" And p.Sit_Poliza <> 'CP' ");
			if (isNotNull(idPoliza)){
				if (isNotNull(idPoliza.getNumeroPoliza())){
					queryString.append(" and p.ID_COTIZACION in (select ID_SEYCOS from seycos.conv_midas_seycos where tipo = 'POLIZA' and id_midas in((select a.idToPoliza||'|0' from (select idToPoliza from midas.toPoliza where NUMEROPOLIZA ="+idPoliza.getNumeroPoliza()+")a))))");
				}else{
					queryString.append(" AND p.num_poliza = "+idPoliza.getNumeroPolizaSeycos());
//					addCondition(queryString, " AND p.num_poliza = ?");
//					params.put(1, idPoliza.getNumeroPolizaSeycos());
					queryString.append(" UNION ALL select DISTINCT p.id_cotizacion as idPoliza, lpad(p.id_centro_emis,3,0)||'-'||lpad(num_poliza,10,0)||'-'||lpad(p.num_renov_pol,2,0) as numeroPolizaSeycosString,vef.nombre as NombreCompleto, nom_supervisoria as descripcionPromotoria ");
					queryString.append(" from seycos.pol_poliza p  inner join seycos.vw_est_fv vef on vef.id_agente = p.id_agente ");
					queryString.append(" where p.id_version_pol = 1 ");
					queryString.append(" And p.Sit_Poliza <> 'CP' ");
					queryString.append(" AND p.num_poliza = "+idPoliza.getNumeroPolizaSeycos()+")");
				}
			}
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString),ExcepcionesPolizaView.class);
//		if(!params.isEmpty()){
//			for(Integer key:params.keySet()){
//				query.setParameter(key,params.get(key));
//			}
//		}
		polizaLista=query.getResultList();
		return polizaLista;
	}
	/************************************************************************************
	 * ==================================================================================
	 * 	Common methods , sets and gets
	 * ==================================================================================
	 * 
	 */
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private void addOrCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" or ");
	}
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key= EntidadDaoImpl.getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}	
		
	}
	/**
	 * metodo para lanzar errores
	 * @param msg
	 * @throws Exception
	 */
	private void onError(String msg)throws Exception{
		throw new Exception(msg);
	}
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	@EJB
	public void setTipoPolizaFacade(TipoPolizaFacadeRemote tipoPolizaFacade) {
		this.tipoPolizaFacade = tipoPolizaFacade;
	}
	@EJB
	public void setRamoTipoPolizaFacade(
			RamoTipoPolizaFacadeRemote ramoTipoPolizaFacade) {
		this.ramoTipoPolizaFacade = ramoTipoPolizaFacade;
	}
	@EJB
	public void setSubRamoFacade(SubRamoFacadeRemote subRamoFacade) {
		this.subRamoFacade = subRamoFacade;
	}
	@EJB
	public void setRamoSeccionFacade(RamoSeccionFacadeRemote ramoSeccionFacade) {
		this.ramoSeccionFacade = ramoSeccionFacade;
	}
	@EJB
	public void setCoberturaSeccionFacade(
			CoberturaSeccionFacadeRemote coberturaSeccionFacade) {
		this.coberturaSeccionFacade = coberturaSeccionFacade;
	}
	@EJB
	public void setCentroOperacionService(
			CentroOperacionService centroOperacionService) {
		this.centroOperacionService = centroOperacionService;
	}
	@EJB
	public void setGerenciaService(GerenciaJPAService gerenciaService) {
		this.gerenciaService = gerenciaService;
	}
	@EJB
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}
	@EJB
	public void setPromotoriaService(PromotoriaJPAService promotoriaService) {
		this.promotoriaService = promotoriaService;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getProductos() throws Exception {
		List<GenericaAgentesView> lista= new ArrayList<GenericaAgentesView>();		
		final StringBuilder queryString = new StringBuilder("");
			queryString.append("select idToProducto AS id,descripcionProducto AS descripcion from MIDAS.toproducto ");				
	
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
		lista= query.getResultList();
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getRamos() throws Exception {
		List<GenericaAgentesView> listaRamos = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
				
			queryString.append(" select idTcRamo AS id,descripcionRamo AS descripcion from MIDAS.TCRAMO "); 
			queryString.append(" where idTcRamo in (select distinct idTcRamo from MIDAS.TRRAMOTIPOPOLIZA "); 
			queryString.append(" where idToTipoPoliza in (select idToTipoPoliza from MIDAS.TOTIPOPOLIZA ");
			queryString.append(" ))");
		
		Query query = entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
		listaRamos = query.getResultList();
		return listaRamos;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ExcepcionesPolizaView getInfoPoliza(ExcepcionesPolizaView poliza){
		final StringBuilder queryString = new StringBuilder();
		List <ExcepcionesPolizaView>polizaLista = new ArrayList<ExcepcionesPolizaView>();
//		queryString.append(" select distinct IDTOPOLIZA as idPoliza, NUMEROPOLIZA, trim(CODIGOPRODUCTO)||trim(CODIGOTIPOPOLIZA)||'-'||trim(NUMEROPOLIZA)||'-'||trim(NUMERORENOVACION) as numeroPolizaMidasString");
//		queryString.append(" from TOPOLIZA");
//		queryString.append(" where IDTOPOLIZA = " + poliza.getIdPoliza());
		queryString.append(" select distinct p.id_cotizacion as idPoliza, NUM_POLIZA, lpad(p.id_centro_emis,3,0)||'-'||lpad(num_poliza,10,0)||'-'||lpad(p.num_renov_pol,2,0) as numeroPolizaMidasString,p.PCT_DESCTO_GLOB as descuentoPoliza ");
		queryString.append(" from SEYCOS.pol_poliza p ");
        queryString.append(" where p.B_ULT_REG_DIA ='V' and p.ID_COTIZACION = " + poliza.getIdPoliza());

		
		Query query = entityManager.createNativeQuery(getQueryString(queryString),ExcepcionesPolizaView.class);
		
		polizaLista=query.getResultList();
		if(polizaLista!=null){
			poliza=polizaLista.get(0);
		}
		return poliza;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigBonoCobertura> getCoberturasPorConfiguracion(ConfigBonos config) throws Exception {		
		return getCoberturasPorConfiguracion(config, null);
	}

	@Override
	public List<ConfigBonoLineaVenta> getLineaVentaPorConfiguracion(ConfigBonos config) throws Exception {
		return getLineaVentaPorConfiguracion(config, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigBonoProducto> getProductosPorConfiguracion(ConfigBonos config) throws Exception {
		return getProductosPorConfiguracion(config,null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigBonoRamo> getRamosPorConfiguracion(ConfigBonos config) throws Exception {
		return getRamosPorConfiguracion(config,null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigBonoSeccion> getSeccionesPorConfiguracion(ConfigBonos config)	throws Exception {
		return getSeccionesPorConfiguracion(config, null);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfigBonoSubramo> getSubRamosPorConfiguracion(ConfigBonos config) throws Exception {
		return getSubRamosPorConfiguracion (config, null);
	}

	@Override
	public List<GenericaAgentesView> getCoberturasRelacionados(ConfigBonos coberturas) throws Exception {
		
		List<GenericaAgentesView>listCoberturasRelacionados=getLineaNegociosRelacionados(coberturas);
		List<ConfigBonoSeccion> listaCoberturasChecadas = new ArrayList<ConfigBonoSeccion>();
	    
		for(GenericaAgentesView objCoberturasChecado:listCoberturasRelacionados){
			if(objCoberturasChecado.getChecado()!= null && objCoberturasChecado.getChecado()==1){
				
				ConfigBonoSeccion objConfigBonoCobertura = new ConfigBonoSeccion();
				objConfigBonoCobertura.setIdSeccion(objCoberturasChecado.getId());
				listaCoberturasChecadas.add(objConfigBonoCobertura);
			}
		}
		
		List<ConfigBonoCobertura> listCobeturasPorConfiguracion = getCoberturasPorConfiguracion(coberturas);
		List<GenericaAgentesView> listCoberturas = getCoberturasPorLineasNegocio(listaCoberturasChecadas);
		
		for(GenericaAgentesView obj:listCoberturas){
			obj.setChecado(0);
		}
		
		if(listCobeturasPorConfiguracion!=null && !listCobeturasPorConfiguracion.isEmpty()){
			for(GenericaAgentesView objCoberturasPorLieaNegocio:listCoberturas){
				for(ConfigBonoCobertura objCoberturaPorConfiguracion:listCobeturasPorConfiguracion){
					
					if(objCoberturasPorLieaNegocio.getId().equals(objCoberturaPorConfiguracion.getIdCobertura())){
						objCoberturasPorLieaNegocio.setChecado(1);
					}
				}
			}
			
		}
		
		return listCoberturas;
	}

	@Override
	public List<GenericaAgentesView> getLineaNegociosRelacionados(ConfigBonos arg0) throws Exception {
		List<GenericaAgentesView> listLineaNegocios = new ArrayList<GenericaAgentesView>();
//		List <GenericaAgentesView> listramoRelacionados = getRamoRelacionados(arg0);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("configuracionBonos.id", arg0.getId());
		Long existeRamoRelacionados = this.entidadService.findByPropertiesCount(ConfigBonoRamo.class, params);
		if(existeRamoRelacionados>0){
			List <GenericaAgentesView> listProductoRelacionados = getProductoRelacionados(arg0);
			List<ConfigBonoProducto> listConfigBonoProductoChecado = new ArrayList<ConfigBonoProducto>();
			
			for(GenericaAgentesView objChecado:listProductoRelacionados){
				if(objChecado.getChecado()==1){				
					ConfigBonoProducto objConfigBonoProducto = new ConfigBonoProducto();
					objConfigBonoProducto.setIdProducto(objChecado.getId());
					listConfigBonoProductoChecado.add(objConfigBonoProducto);
				}
			}  
			
			listLineaNegocios = getLineasNegocioPorProducto(listConfigBonoProductoChecado);
			List<ConfigBonoSeccion> listLineaNegociosPorConfiguracion= getSeccionesPorConfiguracion(arg0);
			//ConfigBonoSeccion
			if(listLineaNegociosPorConfiguracion!=null || !listLineaNegociosPorConfiguracion.isEmpty()){
					for(GenericaAgentesView objLineaNegocio_Seccion: listLineaNegocios){
						for(ConfigBonoSeccion objLinNeg_SeccionXConfig: listLineaNegociosPorConfiguracion){
							if(objLineaNegocio_Seccion.getChecado()==null){
								if(objLineaNegocio_Seccion.getId().equals(objLinNeg_SeccionXConfig.getIdSeccion())){
									objLineaNegocio_Seccion.setChecado(1);
							}else{
								objLineaNegocio_Seccion.setChecado(0);
							}
					}else if(!objLineaNegocio_Seccion.getChecado().equals(1)){
						if(objLineaNegocio_Seccion.getId().equals(objLinNeg_SeccionXConfig.getIdSeccion())){
							objLineaNegocio_Seccion.setChecado(1);
						}
					}
				}
			}
		}
	}
		return listLineaNegocios;
	}

	@Override
	public List<ValorCatalogoAgentes> getLineasVentasRelacionadas(ConfigBonos arg0) throws Exception {
		
		 List <ValorCatalogoAgentes> listaLineaVenta = getLineasVenta();
		 if(arg0!=null && arg0.getId()!=null){
			 List <ConfigBonoLineaVenta> listaLineaVentaRelacionadas =  getLineaVentaPorConfiguracion(arg0);
			
			 for(ValorCatalogoAgentes  objLineaVenta:listaLineaVenta){
				 objLineaVenta.setChecado(0);
			 }
			 for(ValorCatalogoAgentes  objLineaVenta:listaLineaVenta){
				 for(ConfigBonoLineaVenta objLineaVentaRelacionado :listaLineaVentaRelacionadas ){
					 if(objLineaVenta.getId().equals(objLineaVentaRelacionado.getIdLineaVenta())){
						 objLineaVenta.setChecado(1); 
					 }
				 }
			 }
		 }
		return listaLineaVenta;
	}

	@Override
	public List<GenericaAgentesView> getProductoRelacionados(ConfigBonos arg0)	throws Exception {
		
		List <ValorCatalogoAgentes> listaLineaVenta =  getLineasVentasRelacionadas(arg0);
//		List <ValorCatalogoAgentes> listaLineaVentaRelacionadas = new ArrayList<ValorCatalogoAgentes>();
		List <GenericaAgentesView> listaProductosXlineaVenta = getProductosPorLineaVenta(obtenerIdsSeleccionados(listaLineaVenta));
		
		if(arg0!=null && arg0.getId()!=null){
			List <ConfigBonoProducto> listaProductoChecado = getProductosPorConfiguracion(arg0);
			for(GenericaAgentesView objProducto:listaProductosXlineaVenta){
				 objProducto.setChecado(0);
			}
			
			for(GenericaAgentesView objProducto:listaProductosXlineaVenta){
				for(ConfigBonoProducto objProductoChecado:listaProductoChecado){
					if(objProducto.getId().equals(objProductoChecado.getIdProducto())){
						objProducto.setChecado(1);
				}
			}
		  }
		}
		return listaProductosXlineaVenta;
	}

	public String obtenerIdsSeleccionados(
			List<ValorCatalogoAgentes> listaLineaVenta) {
		 StringBuilder idsSeleccionados = new StringBuilder("");
		 Collections.reverse(listaLineaVenta);
		for(ValorCatalogoAgentes objLineaVentaRelacionadas:listaLineaVenta){
			if(objLineaVentaRelacionadas.getChecado()==1){
					idsSeleccionados.append(objLineaVentaRelacionadas.getClave()).append(",");
			}
		}
		return idsSeleccionados.toString();
	}

	@Override
	public List<GenericaAgentesView> getRamoRelacionados(ConfigBonos arg0) throws Exception {
		
		
		List<ConfigBonoProducto> listaProductosChecados = new ArrayList();
		List<GenericaAgentesView> listProductsRelacionados = getProductoRelacionados(arg0);
		for(GenericaAgentesView objProducto:listProductsRelacionados){
			if(objProducto.getChecado()==1){
				ConfigBonoProducto objProductoChecado = new ConfigBonoProducto();
				objProductoChecado.setIdProducto(objProducto.getId());
				listaProductosChecados.add(objProductoChecado);
			}	
		}
		List<ConfigBonoRamo> listaRamoPorConfiguracion = getRamosPorConfiguracion(arg0);
		List<GenericaAgentesView> listaRamosPorProductos = getRamosPorProductos(listaProductosChecados);
		
		for(GenericaAgentesView obj:listaRamosPorProductos){
			obj.setChecado(0);
		}
		
		if(listaRamoPorConfiguracion!=null && !listaRamoPorConfiguracion.isEmpty()){
			for(GenericaAgentesView objRamosPorProductos:listaRamosPorProductos){
				for(ConfigBonoRamo objRamoPorConfiguracion:listaRamoPorConfiguracion){
					
					if(objRamosPorProductos.getId().equals(objRamoPorConfiguracion.getIdRamo())){
						objRamosPorProductos.setChecado(1);
					}
				}
			}
			
		}
		return listaRamosPorProductos;
	}

	@Override
	public List<GenericaAgentesView> getSubRamoRelacionados(ConfigBonos arg0) throws Exception {
		List <GenericaAgentesView> listramoRelacionados = getRamoRelacionados(arg0);
		List<ConfigBonoRamo> listConfigBonoRamoChecado = new ArrayList<ConfigBonoRamo>();
		
		for(GenericaAgentesView objRamoChecado:listramoRelacionados){
			if(objRamoChecado.getChecado()==1){
				
				ConfigBonoRamo objConfigBonoRamo = new ConfigBonoRamo();
				objConfigBonoRamo.setIdRamo(objRamoChecado.getId());
				listConfigBonoRamoChecado.add(objConfigBonoRamo);
			}
		}  
		
		List<GenericaAgentesView> listSubramosPorRamos = getSubramosPorRamos(listConfigBonoRamoChecado);
		List<ConfigBonoSubramo> listSubRamosPorConfiguracion= getSubRamosPorConfiguracion(arg0);
		
		if(listSubramosPorRamos!=null || !listSubramosPorRamos.isEmpty()){
				for(GenericaAgentesView objSubramosPorRamos: listSubramosPorRamos){
					for(ConfigBonoSubramo objSubRamosPorConfiguracion: listSubRamosPorConfiguracion){
						if(objSubramosPorRamos.getChecado()==null){
							if(objSubramosPorRamos.getId().equals(objSubRamosPorConfiguracion.getIdSubramo())){
							objSubramosPorRamos.setChecado(1);
						}else{
							objSubramosPorRamos.setChecado(0);
						}
				}else if(!objSubramosPorRamos.getChecado().equals(1)){
					if(objSubramosPorRamos.getId().equals(objSubRamosPorConfiguracion.getIdSubramo())){
						objSubramosPorRamos.setChecado(1);
					}
				}
			}
		}
	}else{
		return null;
	}
		
		
		return listSubramosPorRamos;
	}
	
	public List<GenericaAgentesView> getProductosPorClaveLinea(String clave) throws Exception{
//		List<ProductoDTO> listaProductos = new ArrayList<ProductoDTO>();
		List<GenericaAgentesView> lista= new ArrayList<GenericaAgentesView>();		
		final StringBuilder queryString = new StringBuilder("");
		
			if(clave!=null){
				queryString.append("select idToProducto AS id,descripcionProducto AS descripcion from MIDAS.toproducto where claveNegocio = '" +clave+"'");				
				Query query=entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
				query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
				lista= query.getResultList();
				return lista;
			}
		return null;
	}
	
	//
	
	@Override
	public List<ExcepcionesPolizaView> getPolizaByIDConfigBono(Long id) throws Exception{
		List<ConfigBonoPoliza> lista = new ArrayList<ConfigBonoPoliza>();
		Map<String,Object> params = new HashMap<String,Object>();	
		List<ExcepcionesPolizaView> polizaLista = new ArrayList<ExcepcionesPolizaView>();
		ConfigBonos obj = new ConfigBonos();
		if(!id.equals(null)){
			obj.setId(id);
			params.put("configbonos", obj);
			lista = entidadService.findByProperties(ConfigBonoPoliza.class, params);
		}
		String idsPoliza = obtenerIdsCotizacion(lista);
		if(isNotNull(idsPoliza) && !idsPoliza.equals("")){
			StringBuilder queryString = new StringBuilder();
			queryString.append(" select distinct pol_p.id_cotizacion as idPoliza,NUM_POLIZA, lpad(pol_p.id_centro_emis,3,0)||'-'||lpad(pol_p.num_poliza,10,0)||'-'||lpad(pol_p.num_renov_pol,2,0) as numeroPolizaMidasString "); 
			queryString.append(" from SEYCOS.pol_poliza pol_p ");
			queryString.append(" WHERE pol_p.B_ULT_REG_DIA ='V' and pol_p.ID_COTIZACION in("+idsPoliza+")");
			Query query = entityManager.createNativeQuery(getQueryString(queryString),ExcepcionesPolizaView.class);
			polizaLista=query.getResultList();
		}
		return polizaLista;
	}
	
	public String obtenerIdsCotizacion(List<ConfigBonoPoliza> lista) {
		StringBuilder idsPoliza = new StringBuilder("");
		Collections.reverse(lista);
		if (lista.isEmpty() && lista.size()==0) return idsPoliza.toString();
		for(ConfigBonoPoliza objLista :lista){
			idsPoliza.append(objLista.getIdCotizacion()).append(",");
		}	
		return idsPoliza.substring(0, idsPoliza.length()-1);
	}

	@Override
	public List<Agente>getAgentesByIdConfigBono(Long id) throws Exception{
		
		List<ConfigBonoAgente> lista = new ArrayList<ConfigBonoAgente>();
		Map<String,Object> params = new HashMap<String,Object>();		
		ConfigBonos obj = new ConfigBonos();
		List<Persona> persona = new ArrayList<Persona>();
		List<Agente> agente = new ArrayList<Agente>();
		if(!id.equals(null)){
			obj.setId(id);
			params.put("configuracionBonos", obj);
			lista = entidadService.findByProperties(ConfigBonoAgente.class, params);
			for(ConfigBonoAgente objLista : lista){
				Agente objAgente = new Agente();
				final StringBuilder queryString=new StringBuilder("");
				queryString.append(" SELECT VW_PERSONA.IDPERSONA, TOAGENTE.ID, NOMBRECOMPLETO FROM "); 
				queryString.append(" MIDAS.VW_PERSONA INNER JOIN MIDAS.TOAGENTE on VW_PERSONA.IDPERSONA = TOAGENTE.IDPERSONA ");
				queryString.append(" where TOAGENTE.ID = "+objLista.getAgente().getId());
				Query query=entityManager.createNativeQuery(getQueryString(queryString),Persona.class);
				persona = query.getResultList();
				objAgente.setId(objLista.getAgente().getId());
				objAgente.setIdAgente(objLista.getAgente().getIdAgente());
				objAgente.setPersona(persona.get(0));
				agente.add(objAgente);
			}
		}	
		
	
		return agente;
	}
	
	@Override
	public List<ConfigBonoAplicaPromotoria>getAplicaPromotoriasByIdConfigBono(Long id)throws Exception{
		return getAplicaPromotoriasByIdConfigBono(id,null);
	}
	
	@Override
	public List<Agente>getAplicaAgentesByIdConfigBono(Long id)throws Exception{
		List<Agente> agente = new ArrayList<Agente>();
		List<ConfigBonoAplicaAgente> lista = new ArrayList<ConfigBonoAplicaAgente>();
		Map<String,Object> params = new HashMap<String,Object>();		
		ConfigBonos obj = new ConfigBonos();
		List<Persona> persona = new  ArrayList<Persona>();
		if(!id.equals(null)){
			obj.setId(id);
			params.put("configuracionBonos", obj);
			lista = entidadService.findByProperties(ConfigBonoAplicaAgente.class, params);
			for(ConfigBonoAplicaAgente objLista :lista){
				Agente objAgente = new Agente();
				final StringBuilder queryString=new StringBuilder("");
				queryString.append(" SELECT VW_PERSONA.IDPERSONA, TOAGENTE.ID, NOMBRECOMPLETO FROM "); 
				queryString.append(" MIDAS.VW_PERSONA INNER JOIN MIDAS.TOAGENTE on VW_PERSONA.IDPERSONA = TOAGENTE.IDPERSONA ");
				queryString.append(" where TOAGENTE.ID = "+objLista.getAgente().getId());
				Query query=entityManager.createNativeQuery(getQueryString(queryString),Persona.class);
				persona  = query.getResultList();
				objAgente.setId(objLista.getAgente().getId());
				objAgente.setPersona(persona.get(0));
				agente.add(objAgente);
			}
		}
		
		return agente;
	}
	


	@Override
	public ConfigBonos saveExclusiones(ConfigBonos exclusiones) throws Exception{
		ConfigBonos exclu = new ConfigBonos();
		exclu = entidadService.save(exclusiones);
		return exclu;
	}
	
	@Override
	public List<AgenteView> getAgentePorConfiguracion(ConfigBonos configuracion) throws Exception{
		
		List<AgenteView> list=new ArrayList<AgenteView>();		
		List<Long>centroOperPorConfig = new ArrayList<Long>();
		List<Long>gerenciasPorConfig = new ArrayList<Long>();
		List<Long>ejecutivosPorConfig = new ArrayList<Long>();
		List<Long>prioridadesPorConfig = new ArrayList<Long>();
		List<Long>promotoriasPorConfig =new ArrayList<Long>();
		List<Long>agentesPorConfig=new ArrayList<Long>();
		List<Long>situacionesPorConfig = new ArrayList<Long>();
		List<Long>tipoAgentesPorConfig=new ArrayList<Long>();
		List<Long>tipoPromotoriasPorConfig = new ArrayList<Long>();
		String polizasString = obtenerIdsPoliza(configuracion);
			
		if (!configuracion.getListaConfigPolizas().isEmpty()){
			//se obtienen los agentes asociados a las polizas que se agregaron en el aparado de produccion
			StringBuilder  querySreingPolizas = new StringBuilder();
			querySreingPolizas.append(" select DISTINCT ag.ID");
			querySreingPolizas.append(" from seycos.pol_poliza p  ");
			querySreingPolizas.append(" inner join seycos.vw_est_fv vef on vef.id_agente = p.id_agente  ");
			querySreingPolizas.append(" inner join midas.toAgente ag on ag.IDAGENTE = vef.ID_AGENTE  ");
			querySreingPolizas.append(" where p.id_version_pol = 1 ");
			querySreingPolizas.append(" And p.Sit_Poliza <> 'CP' ");
			querySreingPolizas.append("AND p.id_cotizacion in("+polizasString+")");
			List<BigDecimal> agentesPorConfigPoliza = this.entityManager.createNativeQuery(querySreingPolizas.toString()).getResultList();
			for(BigDecimal obj:agentesPorConfigPoliza){
				agentesPorConfig.add(obj.longValue());
			}
		}
		for(ConfigBonoCentroOperacion obj : configuracion.getListaCentroOperaciones()){
			if (obj!=null){
				centroOperPorConfig.add(obj.getCentroOperacion().getId());
			}
		}
		for (ConfigBonoGerencia obj : configuracion.getListaGerencias()){
			if (obj!=null){
				gerenciasPorConfig.add(obj.getGerencia().getId());
			}
		}
		for(ConfigBonoEjecutivo obj : configuracion.getListaEjecutivos()){
			if (obj!=null){
				ejecutivosPorConfig.add(obj.getEjecutivo().getId());
			}
		}
		for(ConfigBonoPrioridad obj:configuracion.getListaPrioridades()){
			if (obj!=null){
				prioridadesPorConfig.add(obj.getPrioridadAgente().getId());
			}
		}
		for(ConfigBonoPromotoria obj:configuracion.getListaPromotorias()){
			if (obj!=null){
				promotoriasPorConfig.add(obj.getPromotoria().getId());
			}
		}
		for(ConfigBonoSituacion obj : configuracion.getListaSituaciones()){
			if (obj!=null){
				situacionesPorConfig.add(obj.getSituacionAgente().getId());
			}
		}
		for(ConfigBonoAgente obj : configuracion.getListaAgentes()){
			if (obj!=null){
				agentesPorConfig.add(obj.getAgente().getId());
			}
		}
		for(ConfigBonoTipoAgente obj : configuracion.getListaTipoAgentes()){
			if (obj!=null){
				tipoAgentesPorConfig.add(obj.getTipoAgente().getId());
			}
		}
		for(ConfigBonoTipoPromotoria obj : configuracion.getListaTiposPromotoria()){
			if (obj!=null){
				tipoPromotoriasPorConfig.add(obj.getTipoPromotoria().getId());		
			}
		}
		///////
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Gerencia filtro=new Gerencia();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select distinct ");
		queryString.append(" agente.id,");
		queryString.append(" agente.idAgente as idAgente,");
		queryString.append(" prioridad.valor as prioridad,");
		queryString.append(" estatus.valor as tipoSituacion,");
		queryString.append(" persona.nombreCompleto as nombreCompleto,");
		queryString.append(" promo.descripcion as promotoria,");
		queryString.append(" ger.descripcion as gerencia ");
		queryString.append(" from MIDAS.toAgente agente ");
		queryString.append(" inner join MIDAS.vw_persona persona on (persona.idpersona=agente.idpersona) ");
		queryString.append(" inner join MIDAS.toPromotoria promo on(agente.idpromotoria=promo.id)  ");
		queryString.append(" inner join MIDAS.toEjecutivo ej on(ej.id=promo.ejecutivo_id)  ");
		queryString.append(" inner join MIDAS.vw_persona personaEj on (personaEj.idpersona=ej.idpersona) ");
		queryString.append(" inner join MIDAS.toGerencia ger on (ej.gerencia_id=ger.id) ");
		queryString.append(" inner join MIDAS.toCentroOperacion co on (ger.centroOperacion_id=co.id) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes clasificacionAgente on(clasificacionAgente.id=agente.clasificacionAgentes) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoPromotoria on (tipoPromotoria.id=promo.idTipoPromotoria) "); 
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes prioridad on (prioridad.id=agente.idPrioridadAgente) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes estatus on (estatus.id=agente.idSituacionAgente) ");
		queryString.append(" where 1=1 and");
		boolean isEmptyParams=true;
		if(isNotNull(configuracion)){			
			int index=1;
			//--- FILTROS AND -----------------------------------
			
			if(!isEmptyList(situacionesPorConfig)){			
				
				queryString.append(" estatus.id in ( ");
				queryString.append(getIdObjectsFromList(situacionesPorConfig)+") and");
				isEmptyParams=false;
			}
			if(!isEmptyList(tipoAgentesPorConfig)){				
				
				queryString.append(" clasificacionAgente.id in ( ");
				queryString.append(getIdObjectsFromList(tipoAgentesPorConfig)+") and");
				isEmptyParams=false;
			}
			
			if(!isEmptyList(prioridadesPorConfig)){
				
				queryString.append(" prioridad.id in ( ");
				queryString.append(getIdObjectsFromList(prioridadesPorConfig)+") and");
				isEmptyParams=false;
			}
			//--- FILTROS OR -------------------------------			
			
			queryString.append(" ( ");
						
			if(!isEmptyList(centroOperPorConfig)){
				queryString.append(" co.id in ( ");
				queryString.append(getIdObjectsFromList(centroOperPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(gerenciasPorConfig)){
				queryString.append(" ger.id in ( ");
				queryString.append(getIdObjectsFromList(gerenciasPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(ejecutivosPorConfig)){
				queryString.append(" ej.id in ( ");
				queryString.append(getIdObjectsFromList(ejecutivosPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(promotoriasPorConfig)){
				queryString.append(" promo.id in ( ");
				queryString.append(getIdObjectsFromList(promotoriasPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(agentesPorConfig)){
				queryString.append(" agente.id in ( ");
				queryString.append(getIdObjectsFromList(agentesPorConfig)+") or");
				isEmptyParams=false;
			}
			
			if(!isEmptyList(tipoPromotoriasPorConfig)){
				queryString.append(" tipoPromotoria.id in ( ");
				queryString.append(getIdObjectsFromList(tipoPromotoriasPorConfig)+") or");
				isEmptyParams=false;
			}
		
		 	if(queryString.toString().endsWith(" or"))
		 	{
		 		queryString.replace(queryString.lastIndexOf(" or"),queryString.length(),"");
		 		queryString.append(" ) ");
		 	}
		 	
		 	if(queryString.toString().endsWith(" and ( "))
		 	{
		 		queryString.replace(queryString.lastIndexOf(" and ( "),queryString.length(),"");
		 	}		 
			
			String finalQuery=getQueryString(queryString)+" order by agente.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,AgenteView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
		}
		return list;
	}
	public String obtenerIdsPoliza(ConfigBonos configuracion) {
		StringBuilder polizasString = new StringBuilder("");
		Collections.reverse(configuracion.getListaConfigPolizas());
		if (configuracion.getListaConfigPolizas().isEmpty() && configuracion.getListaConfigPolizas().size()==0) return polizasString.toString();
		for(ConfigBonoPoliza obj :configuracion.getListaConfigPolizas()){			
				polizasString.append(obj.getIdPoliza().toString()).append(",");
		}
		return polizasString.substring(0, polizasString.length()-1);
	}

	/**
	 * Metodo para obtener el listado de agentes por lista de centros de operacion seleccionados, ejecutivos, promotorias, etc...
	 * @param centroOperacionList
	 * @param ejecutivoList
	 * @param gerenciaList
	 * @param promotoriaList
	 * @param situacionesList
	 * @param tiposAgenteList
	 * @param tiposPromotoriaList
	 * @param agenteList
	 * @param prioridadesList
	 * @return
	 * @throws Exception
	 */
	public List<AgenteView> obtenerAgentesPorConfiguracionCapturada(List<CentroOperacion> centroOperacionList,
			List<Ejecutivo> ejecutivoList,
			List<Gerencia> gerenciaList,
			List<Promotoria> promotoriaList,
			List<ValorCatalogoAgentes> situacionesList,
			List<ValorCatalogoAgentes> tiposAgenteList,
			List<ValorCatalogoAgentes> tiposPromotoriaList,
			List<Agente> agenteList,
			List<ValorCatalogoAgentes> prioridadesList) throws Exception{
		List<AgenteView> list=new ArrayList<AgenteView>();
		List<ConfigBonoCentroOperacion> centros=new ArrayList<ConfigBonoCentroOperacion>();
		if(!isEmptyList(centroOperacionList)){
			for(CentroOperacion bean:centroOperacionList){
				if(isNotNull(bean)){
					ConfigBonoCentroOperacion o=new ConfigBonoCentroOperacion();
					o.setCentroOperacion(bean);
					centros.add(o);
				}
			}
		}
		List<ConfigBonoEjecutivo> ejecutivos=new ArrayList<ConfigBonoEjecutivo>();
		if(!isEmptyList(ejecutivoList)){
			for(Ejecutivo bean:ejecutivoList){
				if(isNotNull(bean)){
					ConfigBonoEjecutivo o=new ConfigBonoEjecutivo();
					o.setEjecutivo(bean);
					ejecutivos.add(o);
				}
			}
		}
		List<ConfigBonoGerencia> gerencias=new ArrayList<ConfigBonoGerencia>();
		if(!isEmptyList(gerenciaList)){
			for(Gerencia bean:gerenciaList){
				if(isNotNull(bean)){
					ConfigBonoGerencia o=new ConfigBonoGerencia();
					o.setGerencia(bean);
					gerencias.add(o);
				}
			}
		}
		List<ConfigBonoPromotoria> promotorias=new ArrayList<ConfigBonoPromotoria>();
		if(!isEmptyList(promotoriaList)){
			for(Promotoria bean:promotoriaList){
				if(isNotNull(bean)){
					ConfigBonoPromotoria o=new ConfigBonoPromotoria();
					o.setPromotoria(bean);
					promotorias.add(o);
				}
			}
		}
		List<ConfigBonoSituacion> situaciones=new ArrayList<ConfigBonoSituacion>();
		if(!isEmptyList(situacionesList)){
			for(ValorCatalogoAgentes bean:situacionesList){
				if(isNotNull(bean)){
					ConfigBonoSituacion o=new ConfigBonoSituacion();
					o.setSituacionAgente(bean);
					situaciones.add(o);
				}
			}
		}
		List<ConfigBonoTipoAgente> tiposAgentes=new ArrayList<ConfigBonoTipoAgente>();
		if(!isEmptyList(tiposAgenteList)){
			for(ValorCatalogoAgentes bean:tiposAgenteList){
				if(isNotNull(bean)){
					ConfigBonoTipoAgente o=new ConfigBonoTipoAgente();
					o.setTipoAgente(bean);
					tiposAgentes.add(o);
				}
			}
		}
		List<ConfigBonoTipoPromotoria> tiposPromotoria=new ArrayList<ConfigBonoTipoPromotoria>();
		if(!isEmptyList(tiposPromotoriaList)){
			for(ValorCatalogoAgentes bean:tiposPromotoriaList){
				if(isNotNull(bean)){
					ConfigBonoTipoPromotoria o=new ConfigBonoTipoPromotoria();
					o.setTipoPromotoria(bean);
					tiposPromotoria.add(o);
				}
			}
		}
		List<ConfigBonoAgente> agentes=new ArrayList<ConfigBonoAgente>();
		if(!isEmptyList(agenteList)){
			for(Agente bean:agenteList){
				if(isNotNull(bean)){
					ConfigBonoAgente o=new ConfigBonoAgente();
					o.setAgente(bean);
					agentes.add(o);
				}
			}
		}
		List<ConfigBonoPrioridad> prioridades=new ArrayList<ConfigBonoPrioridad>();
		if(!isEmptyList(prioridadesList)){
			for(ValorCatalogoAgentes bean:prioridadesList){
				if(isNotNull(bean)){
					ConfigBonoPrioridad o=new ConfigBonoPrioridad();
					o.setPrioridadAgente(bean);
					prioridades.add(o);
				}
			}
		}
		ConfigBonos configuracion=new ConfigBonos();
		configuracion.setListaAgentes(agentes);
		configuracion.setListaCentroOperaciones(centros);
		configuracion.setListaEjecutivos(ejecutivos);
		configuracion.setListaGerencias(gerencias);
		configuracion.setListaPromotorias(promotorias);
		configuracion.setListaSituaciones(situaciones);
		configuracion.setListaTipoAgentes(tiposAgentes);
		configuracion.setListaTiposPromotoria(tiposPromotoria);
		configuracion.setListaPrioridades(prioridades);
		list=obtenerAgentesPorConfiguracionCapturada(configuracion);
		return list;
	}
	/**
	 * Obtiene los agentes resultantes de acuerdo a al configuracion de bonos seleccionada.
	 * @param configuracion
	 * @return
	 * @throws Exception
	 */
	public List<AgenteView> obtenerAgentesPorConfiguracionCapturada(ConfigBonos configuracion) throws Exception{
		List<AgenteView> list=new ArrayList<AgenteView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Gerencia filtro=new Gerencia();
		final StringBuilder queryString=new StringBuilder("");
		List<ConfigBonoCentroOperacion> centros=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaCentroOperaciones()))?configuracion.getListaCentroOperaciones():null;
		List<ConfigBonoEjecutivo> ejecutivos=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaEjecutivos()))?configuracion.getListaEjecutivos():null;
		List<ConfigBonoGerencia> gerencias=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaGerencias()))?configuracion.getListaGerencias():null;
		List<ConfigBonoPromotoria> promotorias=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaPromotorias()))?configuracion.getListaPromotorias():null;
		List<ConfigBonoSituacion> situaciones=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaSituaciones()))?configuracion.getListaSituaciones():null;
		List<ConfigBonoTipoAgente> tiposAgente=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaTipoAgentes()))?configuracion.getListaTipoAgentes():null;
		List<ConfigBonoTipoPromotoria> tiposPromotoria=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaTiposPromotoria()))?configuracion.getListaTiposPromotoria():null;
		List<ConfigBonoAgente> agentes=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaAgentes()))?configuracion.getListaAgentes():null;
		List<ConfigBonoPrioridad> prioridades=(isNotNull(configuracion) && !isEmptyList(configuracion.getListaPrioridades()))?configuracion.getListaPrioridades():null;
		queryString.append(" select distinct ");
		queryString.append(" agente.id,");
		queryString.append(" agente.idAgente as idAgente,");
		queryString.append(" prioridad.valor as prioridad,");
		queryString.append(" estatus.valor as tipoSituacion,");
		queryString.append(" persona.nombreCompleto as nombreCompleto,");
		queryString.append(" promo.descripcion as promotoria,");
		queryString.append(" ger.descripcion as gerencia ");
		queryString.append(" from MIDAS.toAgente agente ");
		queryString.append(" inner join MIDAS.vw_persona persona on (persona.idpersona=agente.idpersona) ");
		queryString.append(" inner join MIDAS.toPromotoria promo on(agente.idpromotoria=promo.id)  ");
		queryString.append(" inner join MIDAS.toEjecutivo ej on(ej.id=promo.ejecutivo_id)  ");
		queryString.append(" inner join MIDAS.vw_persona personaEj on (personaEj.idpersona=ej.idpersona) ");
		queryString.append(" inner join MIDAS.toGerencia ger on (ej.gerencia_id=ger.id) ");
		queryString.append(" inner join MIDAS.toCentroOperacion co on (ger.centroOperacion_id=co.id) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes clasificacionAgente on(clasificacionAgente.id=agente.clasificacionAgentes and agente.clasificacionAgentes  <> (select id from midas.toValorCatalogoAgentes where grupocatalogoagentes_id = (select id from midas.tcgrupocatalogoagentes where descripcion ='Clasificacion de Agente') and valor = 'ALTA POR INTERNET')) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoPromotoria on (tipoPromotoria.id=promo.idTipoPromotoria) "); 
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes prioridad on (prioridad.id=agente.idPrioridadAgente) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes estatus on (estatus.id=agente.idSituacionAgente) ");
		queryString.append(" where ");
		boolean isEmptyParams=true;
		if(isNotNull(configuracion)){
			int index=1;
			if(filtro.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtro.getClaveEstatus());
				index++;
				isEmptyParams=false;
			}
			if(!isEmptyList(agentes)){
				queryString.append(" agente.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoAgente agente:agentes){
					if(isNotNull(agente) && isNotNull(agente.getAgente()) && isNotNull(agente.getAgente().getId())){
						if(i<(agentes.size()-1)){
							lista.append(agente.getAgente().getId()+",");
						}else{
							lista.append(agente.getAgente().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") or ");
				isEmptyParams=false;
			}
			if(!isEmptyList(centros)){
				queryString.append(" co.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoCentroOperacion centro:centros){
					if(isNotNull(centro) && isNotNull(centro.getCentroOperacion()) && isNotNull(centro.getCentroOperacion().getId())){
						if(i<(centros.size()-1)){
							lista.append(centro.getCentroOperacion().getId()+",");
						}else{
							lista.append(centro.getCentroOperacion().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(gerencias)){
				queryString.append(" ger.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoGerencia gerencia:gerencias){
					if(isNotNull(gerencia) && isNotNull(gerencia.getGerencia()) && isNotNull(gerencia.getGerencia().getId())){
						if(i<(gerencias.size()-1)){
							lista.append(gerencia.getGerencia().getId()+",");
						}else{
							lista.append(gerencia.getGerencia().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(ejecutivos)){
				queryString.append(" ej.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoEjecutivo bean:ejecutivos){
					if(isNotNull(bean) && isNotNull(bean.getEjecutivo()) && isNotNull(bean.getEjecutivo().getId())){
						if(i<(ejecutivos.size()-1)){
							lista.append(bean.getEjecutivo().getId()+",");
						}else{
							lista.append(bean.getEjecutivo().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(promotorias)){
				queryString.append(" promo.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoPromotoria bean:promotorias){
					if(isNotNull(bean) && isNotNull(bean.getPromotoria()) && isNotNull(bean.getPromotoria().getId())){
						if(i<(promotorias.size()-1)){
							lista.append(bean.getPromotoria().getId()+",");
						}else{
							lista.append(bean.getPromotoria().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(situaciones)){
				queryString.append("  estatus.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoSituacion situacion:situaciones){
					ValorCatalogoAgentes catalogo=(isNotNull(situacion))?situacion.getSituacionAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(situaciones.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(tiposAgente)){
				queryString.append("  clasificacionAgente.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoTipoAgente tipoAgente:tiposAgente){
					ValorCatalogoAgentes catalogo=(isNotNull(tipoAgente))?tipoAgente.getTipoAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(tiposAgente.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(tiposPromotoria)){
				queryString.append("  tipoPromotoria.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoTipoPromotoria tipoPromotoria:tiposPromotoria){
					ValorCatalogoAgentes catalogo=(isNotNull(tipoPromotoria))?tipoPromotoria.getTipoPromotoria():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(tiposPromotoria.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(prioridades)){
				queryString.append("  prioridad.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoPrioridad prioridad:prioridades){
					ValorCatalogoAgentes catalogo=(isNotNull(prioridad))?prioridad.getPrioridadAgente():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(prioridades.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") ");
				isEmptyParams=false;
			}
			if(isEmptyParams){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			String finalQuery=getQueryString(queryString)+" order by agente.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,AgenteView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
		}
		return list;	
	}
	
	/**
	 * Obtiene las promotorias resultantes de acuerdo a al configuracion de bonos seleccionada.
	 * @param configuracion
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public List<PromotoriaView> obtenerPromotoriasPorConfiguracionCapturada(List<CentroOperacion> centroOperacionList,
			List<Ejecutivo> ejecutivoList,
			List<Gerencia> gerenciaList,
			List<Promotoria> promotoriaList,		
			List<ValorCatalogoAgentes> tiposPromotoriaList) throws Exception{		
		
		List<ConfigBonoCentroOperacion> centros=new ArrayList<ConfigBonoCentroOperacion>();
		if(!isEmptyList(centroOperacionList)){
			for(CentroOperacion bean:centroOperacionList){
				if(isNotNull(bean)){
					ConfigBonoCentroOperacion o=new ConfigBonoCentroOperacion();
					o.setCentroOperacion(bean);
					centros.add(o);
				}
			}
		}
		else{
			centros = null;
		}
		
		List<ConfigBonoEjecutivo> ejecutivos=new ArrayList<ConfigBonoEjecutivo>();
		if(!isEmptyList(ejecutivoList)){
			for(Ejecutivo bean:ejecutivoList){
				if(isNotNull(bean)){
					ConfigBonoEjecutivo o=new ConfigBonoEjecutivo();
					o.setEjecutivo(bean);
					ejecutivos.add(o);
				}
			}
		}else{
			ejecutivos = null;
		}		
		
		List<ConfigBonoGerencia> gerencias=new ArrayList<ConfigBonoGerencia>();
		if(!isEmptyList(gerenciaList)){
			for(Gerencia bean:gerenciaList){
				if(isNotNull(bean)){
					ConfigBonoGerencia o=new ConfigBonoGerencia();
					o.setGerencia(bean);
					gerencias.add(o);
				}
			}
		}else{
			gerencias = null;
		}		
		
		List<ConfigBonoPromotoria> promotorias=new ArrayList<ConfigBonoPromotoria>();
		if(!isEmptyList(promotoriaList)){
			for(Promotoria bean:promotoriaList){
				if(isNotNull(bean)){
					ConfigBonoPromotoria o=new ConfigBonoPromotoria();
					o.setPromotoria(bean);
					promotorias.add(o);
				}
			}
		}else{
			promotorias = null;
		}			
		
		List<ConfigBonoTipoPromotoria> tiposPromotoria=new ArrayList<ConfigBonoTipoPromotoria>();
		if(!isEmptyList(tiposPromotoriaList)){
			for(ValorCatalogoAgentes bean:tiposPromotoriaList){
				if(isNotNull(bean)){
					ConfigBonoTipoPromotoria o=new ConfigBonoTipoPromotoria();
					o.setTipoPromotoria(bean);
					tiposPromotoria.add(o);
				}
			}
		}else{
			tiposPromotoria = null;
		}			
		
		List<PromotoriaView> list=new ArrayList<PromotoriaView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		
		final StringBuilder queryString=new StringBuilder("");		
		
		queryString.append("select distinct  promo.id," +
				"promo.IDTIPOPROMOTORIA, " +
				"promo.idPromotoria as idPromotoria, " +
				"claveEstatus, " +
				"promo.descripcion, " +
				"PROMO.IDPERSONARESPONSABLE  as personaResponsable " +
				"FROM " +    
				"MIDAS.toPromotoria promo " +
				"inner join MIDAS.toEjecutivo ej on(ej.id=promo.ejecutivo_id) " +
				"inner join MIDAS.vw_persona personaEj on (personaEj.idpersona=ej.idpersona) " +
				"inner join MIDAS.toGerencia ger on (ej.gerencia_id=ger.id) " +
				"inner join MIDAS.toCentroOperacion co on (ger.centroOperacion_id=co.id) " +
				"inner join MIDAS.toValorCatalogoAgentes tipoPromotoria on (tipoPromotoria.id=promo.idTipoPromotoria)");
		
		queryString.append(" where ");
		boolean isEmptyParams=true;
	
			if(!isEmptyList(centros)){
				queryString.append(" co.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoCentroOperacion centro:centros){
					if(isNotNull(centro) && isNotNull(centro.getCentroOperacion()) && isNotNull(centro.getCentroOperacion().getId())){
						if(i<(centros.size()-1)){
							lista.append(centro.getCentroOperacion().getId()+",");
						}else{
							lista.append(centro.getCentroOperacion().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(gerencias)){
				queryString.append(" ger.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoGerencia gerencia:gerencias){
					if(isNotNull(gerencia) && isNotNull(gerencia.getGerencia()) && isNotNull(gerencia.getGerencia().getId())){
						if(i<(gerencias.size()-1)){
							lista.append(gerencia.getGerencia().getId()+",");
						}else{
							lista.append(gerencia.getGerencia().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(ejecutivos)){
				queryString.append(" ej.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoEjecutivo bean:ejecutivos){
					if(isNotNull(bean) && isNotNull(bean.getEjecutivo()) && isNotNull(bean.getEjecutivo().getId())){
						if(i<(ejecutivos.size()-1)){
							lista.append(bean.getEjecutivo().getId()+",");
						}else{
							lista.append(bean.getEjecutivo().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			if(!isEmptyList(promotorias)){
				queryString.append(" promo.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoPromotoria bean:promotorias){
					if(isNotNull(bean) && isNotNull(bean.getPromotoria()) && isNotNull(bean.getPromotoria().getId())){
						if(i<(promotorias.size()-1)){
							lista.append(bean.getPromotoria().getId()+",");
						}else{
							lista.append(bean.getPromotoria().getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}			
			
			if(!isEmptyList(tiposPromotoria)){
				queryString.append("  tipoPromotoria.id in ( ");
				StringBuilder lista=new StringBuilder("");
				int i=0;
				for(ConfigBonoTipoPromotoria tipoPromotoria:tiposPromotoria){
					ValorCatalogoAgentes catalogo=(isNotNull(tipoPromotoria))?tipoPromotoria.getTipoPromotoria():null;
					if( isNotNull(catalogo) && isNotNull(catalogo.getId())){
						if(i<(tiposPromotoria.size()-1)){
							lista.append(catalogo.getId()+",");
						}else{
							lista.append(catalogo.getId());
						}
					}
					i++;
				}
				queryString.append(lista.toString()+") and ");
				isEmptyParams=false;
			}
			
			if(isEmptyParams){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			String finalQuery=getQueryString(queryString)+" order by promo.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,PromotoriaView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
		//}
		return list;	
	}
	
	@Override
	public List<ConfigBonoRangoAplica> getRangosPorConfiguracion(ConfigBonos config) throws Exception {
		if(config==null || config.getId()==null){
			onError("Favor de proporcionar la configuracion de bonos");
		}
		
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("select  id,importePrimaInicial,importePrimaFinal,pctSiniestralidadInicial,pctSiniestralidadFinal ");
		queryString.append(",pctcrecimientoinicial,pctcrecimientofinal,pctsupmetainicial,pctsupmetafinal,valoraplicacion,numpolizasemitidasinicial");
		queryString.append(",numpolizasemitidasfinal from MIDAS.TRCONFIGBONORANGOAPLICA where configBono_id="+config.getId());
							 
		Query query = entityManager.createNativeQuery(queryString.toString(),ConfigBonoRangoAplica.class);
		Object list  = query.getResultList();
		return (List<ConfigBonoRangoAplica>) list;
	}
		
	private String getIdObjectsFromList(List<Long>obj){
		StringBuilder ids=new StringBuilder("");
		int i=0;
		for(Long id:obj){
			ids.append(id.toString());
			if(i<(obj.size()-1)){
				ids.append(",");
			}
			i++;
		}
		return ids.toString();
	}
	
	

	@Override
	public List<GenericaAgentesView> getLineasNegocioPorProducto(List<ConfigBonoProducto> productos) throws Exception {
		List<GenericaAgentesView> list=new ArrayList<GenericaAgentesView>();
		if(productos!=null){
			String listaProductos = obtenerIdProductos(productos);
			List<GenericaAgentesView> listTemp=getLineasNegocioPorProductoView(listaProductos);
					if(!isEmptyList(listTemp)){
						list.addAll(listTemp);
					}
		}
		return list;
	}

	@Override
	public List<GenericaAgentesView> getLineasNegocioPorProductoView(String productos) throws Exception {
		List<GenericaAgentesView> listaLineaNegocio = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		if(!productos.equals("")){			
			productos = productos.substring(0, productos.length()-1);
			/*[CAVALOS] se realiza ajuste para obtener las lineas de negocio por producto ya que al obtenerlas por ramo se repetian las lineas de negocio
			*este cambio se consulto con lizandro*/
			queryString.append(" select  distinct sec.idtoseccion as id,'LN'||TRIM(SEC.CODIGOSECCION)||' - '||sec.NOMBRECOMERCIALSECCION||' - (P'||trim(p.CODIGOPRODUCTO)||', R'||trim(R.CODIGORAMO)||')' as descripcion "); 
			queryString.append(" from MIDAS.TOSECCION sec ");
			queryString.append(" inner join MIDAS.TOTIPOPOLIZA tp on(tp.IDTOTIPOPOLIZA=sec.IDTOTIPOPOLIZA) ");
			queryString.append(" inner join MIDAS.TOPRODUCTO p on(P.IDTOPRODUCTO =TP.IDTOPRODUCTO) ");
			queryString.append(" inner join MIDAS.TRRAMOTIPOPOLIZA rtp on (RTP.IDTOTIPOPOLIZA = TP.IDTOTIPOPOLIZA ) ");
			queryString.append(" inner join MIDAS.TRRAMOSECCION rs on (RS.IDTOSECCION = SEC.IDTOSECCION  ) ");
			queryString.append(" inner join MIDAS.TCRAMO r on (R.IDTCRAMO = RS.IDTCRAMO and RTP.IDTCRAMO = R.IDTCRAMO ) ");
			queryString.append(" where tp.idtoproducto in("+productos+") and sec.claveActivo=1 ");
			Query query = entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
			query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
			listaLineaNegocio = query.getResultList();
			return listaLineaNegocio;
		}
		return null;
	}
	
	@Override
	public List<GenericaAgentesView> getLineasNegocioPorRamoProductoView(String productos,String ramos) throws Exception {
		List<GenericaAgentesView> listaLineaNegocio = new ArrayList<GenericaAgentesView>();
		StringBuilder queryString = new StringBuilder("");
		if(!productos.equals("")){			
			productos = productos.substring(0, productos.length()-1);
			/*[HJC] se realiza ajuste para obtener las lineas de negocio en base a los ramos y productos seleccionados por el usuario*/
			queryString.append(" select  distinct sec.idtoseccion as id,'LN'||TRIM(SEC.CODIGOSECCION)||' - '||sec.NOMBRECOMERCIALSECCION||' - (P'||max(trim(p.CODIGOPRODUCTO))||', R'||max(trim(R.CODIGORAMO))||', V'||p.VERSIONPRODUCTO||')' as descripcion,  sec.NOMBRECOMERCIALSECCION ");
			queryString.append(" from MIDAS.TOSECCION sec ");
			queryString.append(" inner join MIDAS.TOTIPOPOLIZA tp on(tp.IDTOTIPOPOLIZA=sec.IDTOTIPOPOLIZA) ");
			queryString.append(" inner join MIDAS.TOPRODUCTO p on(P.IDTOPRODUCTO =TP.IDTOPRODUCTO) ");
			queryString.append(" inner join MIDAS.TRRAMOTIPOPOLIZA rtp on (RTP.IDTOTIPOPOLIZA = TP.IDTOTIPOPOLIZA ) ");
			queryString.append(" inner join MIDAS.TRRAMOSECCION rs on (RS.IDTOSECCION = SEC.IDTOSECCION  ) ");
			queryString.append(" inner join MIDAS.TCRAMO r on (R.IDTCRAMO = RS.IDTCRAMO and RTP.IDTCRAMO = R.IDTCRAMO ) ");
			queryString.append(" where tp.idtoproducto in("+productos+") and sec.claveActivo=1 ");
			if(!ramos.equals("")){
				ramos = ramos.substring(0, ramos.length()-1);
				queryString.append(" and R.IDTCRAMO in ("+ramos+") ");
			}
			queryString.append(" group by sec.idtoseccion, SEC.CODIGOSECCION, sec.NOMBRECOMERCIALSECCION, p.VERSIONPRODUCTO ");			
			queryString.append(" order by  sec.NOMBRECOMERCIALSECCION ");
			Query query = entityManager.createNativeQuery(getQueryString(queryString),"productoXLineaVentaView");
			query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
			listaLineaNegocio = query.getResultList();
			return listaLineaNegocio;
		}
		return null;
	}
	
	@Override
	public List<ConfigBonoRangoClaveAmis> getListaBonoRangosClaveAmis(ConfigBonos config) throws Exception {
		List<ConfigBonoRangoClaveAmis> listaAmis = new ArrayList<ConfigBonoRangoClaveAmis>();
		if(config==null){
			onError("No hay configuracion seleccionada");
		}
		if(config.getId()!=null){
			StringBuilder queryString = new StringBuilder("");
			queryString.append("select id,configBono_id as configBono,cve_amis_ini,cve_amis_fin from MIDAS.TRCONFIGBONORANGOCLAVEAMIS ");
			queryString.append(" where configBono_id="+config.getId()+ " order by id desc ");
			Query query = entityManager.createNativeQuery(queryString.toString(), ConfigBonoRangoClaveAmis.class);
			listaAmis=query.getResultList();
		}
			return listaAmis;
	}

		
	@SuppressWarnings("unchecked")
	private List<ConfigBonoCentroOperacion> getCentrosOperacionPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ConfigBonoCentroOperacion model join fetch model.centroOperacion where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));
		
		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();	
	}
	
	@SuppressWarnings("unchecked")
	private List<ConfigBonoGerencia> getGerenciasPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ConfigBonoGerencia model join fetch model.gerencia where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return  query.getResultList();	
	}
	
	@SuppressWarnings("unchecked")
	private List<ConfigBonoEjecutivo> getEjecutivosPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ConfigBonoEjecutivo model join fetch model.ejecutivo where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	private List<ConfigBonoPromotoria> getPromotoriasPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ConfigBonoPromotoria model join fetch model.promotoria where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));
		
		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();	
	}
	
	@SuppressWarnings("unchecked")
	private List<ConfigBonoTipoAgente> getTiposAgentePorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoTipoAgente model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));
		
		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();	
	}
	
	@SuppressWarnings("unchecked")
	private List<ConfigBonoTipoPromotoria> getTiposPromotoriaPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoTipoPromotoria model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));
		
		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();	
	}

	@SuppressWarnings("unchecked")
	private List<ConfigBonoPrioridad> getPrioridadesPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoPrioridad model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));
		
		if (asOf != null) {
			
			return  entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();	
	}

	@SuppressWarnings("unchecked")
	private List<ConfigBonoSituacion> getSituacionesPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoSituacion model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();		
	}

	@SuppressWarnings("unchecked")
	private List<ConfigBonoLineaVenta> getLineaVentaPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoLineaVenta model where model.configBono.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();	
	}
	
	@SuppressWarnings("unchecked")
	private List<ConfigBonoProducto> getProductosPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoProducto model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
		
	}

	@SuppressWarnings("unchecked")
	private List<ConfigBonoRamo> getRamosPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoRamo model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
	}

	@SuppressWarnings("unchecked")
	private List<ConfigBonoSeccion> getSeccionesPorConfiguracion(ConfigBonos config, Date asOf)	throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoSeccion model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
	}

	@SuppressWarnings("unchecked")
	private List<ConfigBonoSubramo> getSubRamosPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoSubramo model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
	}

	@SuppressWarnings("unchecked")
	private List<ConfigBonoCobertura> getCoberturasPorConfiguracion(ConfigBonos config, Date asOf) throws Exception {		
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoCobertura model where model.configuracionBonos.id="+config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	private List<ConfigBonoAplicaPromotoria>getAplicaPromotoriasByIdConfigBono(Long id, Date asOf)throws Exception{
		
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoAplicaPromotoria model where model.configuracionBonos.id="+ id);
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
	}
	
	
//	config.setListaAgentes(childListClone(ConfigBonoAgente.class, entidadService.findByProperty(ConfigBonoAgente.class, "configuracionBonos", original)));

	@SuppressWarnings("unchecked")
	private List<ConfigBonoAgente>getAgentesPorConfiguracion(ConfigBonos config, Date asOf)throws Exception{
		
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoAgente model where model.configuracionBonos.id="+ config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
	}
	
	//	config.setListaConfigPolizas(childListClone(ConfigBonoPoliza.class, entidadService.findByProperty(ConfigBonoPoliza.class, "configbonos", original)));
	
	@SuppressWarnings("unchecked")
	private List<ConfigBonoPoliza>getPolizasPorConfiguracion(ConfigBonos config, Date asOf)throws Exception{
		
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoPoliza model where model.configbonos.id="+ config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
	}
	
//	config.setListaAplicaAgentes(childListClone(ConfigBonoAplicaAgente.class, entidadService.findByProperty(ConfigBonoAplicaAgente.class, "configuracionBonos", original)));

	@SuppressWarnings("unchecked")
	private List<ConfigBonoAplicaAgente>getAplicaAgentesPorConfiguracion(ConfigBonos config, Date asOf)throws Exception{
		
		StringBuilder queryString = new StringBuilder(""); 
		queryString.append("select model from ConfigBonoAplicaAgente model where model.configuracionBonos.id="+ config.getId());
		Query query = entityManager.createQuery(getQueryString(queryString));

		if (asOf != null) {
			
			return entidadHistoricoDao.executeHistoryQuery(asOf, query);
		
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
		
	}
	
	
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<AgenteView> obtenerAgentesPorFuerzaVenta(Long centroOperacion,Long ejecutivo,Long gerencia,	Long promotoria,Long clasificacionAgente,
			Long agente) throws Exception {
		
		List<AgenteView> listaAgentes = new ArrayList<AgenteView>();
		StringBuilder queryString = new StringBuilder();
		
		queryString.append(" select a.id, a.idAgente as idAgente, p.NOMBRE as nombreCompleto "); 
		queryString.append(" from midas.toAgente a ");
		queryString.append(" inner join seycos.persona p on p.id_persona = a.idPersona ");
		queryString.append(" inner join midas.toPromotoria pro on pro.ID = a.IDPROMOTORIA ");
		queryString.append(" inner join midas.toEjecutivo ej on ej.ID = pro.EJECUTIVO_ID ");
		queryString.append(" inner join midas.toGerencia ge on ge.ID = ej.GERENCIA_ID ");
		queryString.append(" inner join midas.toCentroOperacion cop on cop.ID = ge.CENTROOPERACION_ID ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes vca on vca.ID = a.CLASIFICACIONAGENTES ");
		queryString.append(" where a.IDSITUACIONAGENTE = (select id from MIDAS.toValorCatalogoAgentes where GRUPOCATALOGOAGENTES_ID = (select id from MIDAS.tcgrupocatalogoagentes  where descripcion = 'Estatus de Agente (Situacion)') and valor = 'AUTORIZADO') ");
		
		if(centroOperacion != null){
			queryString.append(" and cop.id = "+centroOperacion);
		}
		if(ejecutivo != null){
			queryString.append(" and ej.id "+ejecutivo);		
		}
		if(gerencia != null){
			queryString.append(" and ge.id "+ gerencia);
		}
		if(promotoria != null){
			queryString.append(" and pro.id = "+promotoria);
		}
		if(clasificacionAgente != null){
			queryString.append(" and vca.id "+clasificacionAgente);
		}
		if(agente != null){
			queryString.append(" and  a.idagente = "+agente);
		}
		
		listaAgentes = this.entityManager.createNativeQuery(queryString.toString(), AgenteView.class).getResultList() ;
		
		
		
		return listaAgentes;
	}
	
	public void actualizarClasificacionConfigBono(Long idConfigBono)throws Exception
	{
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS.PKGREPORTES_AGENTES.STP_ACTUALIZAR_CLASIF_CB";
		int respuesta = 0;
		try {
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS) ;
			storedHelper.estableceParametro("pIdConfigBono",idConfigBono);
			respuesta = storedHelper.ejecutaActualizar();
			if(respuesta!=0){
				log.error(storedHelper.getDescripcionRespuesta());
			}
		} catch (SQLException e) {				
			log.error(e.getMessage() , e);
		}
	}
}
