package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza.seccion.tipouso;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso_;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioTipoUsoDaoImpl extends JpaDao<BigDecimal, NegocioTipoUso> implements NegocioTipoUsoDao{

	@Override
	public NegocioTipoUso buscarDefault(NegocioSeccion negocioSeccion) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<NegocioTipoUso> cq = cb.createQuery(NegocioTipoUso.class);
		Root<NegocioTipoUso> ntu = cq.from(NegocioTipoUso.class);
		Predicate p = cb.equal(ntu.get(NegocioTipoUso_.negocioSeccion), negocioSeccion);
		p = cb.and(p, cb.equal(ntu.get(NegocioTipoUso_.claveDefault), true));
		cq.where(p);
		TypedQuery<NegocioTipoUso> q = entityManager.createQuery(cq);
		return getSingleResult2(q);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TipoUsoVehiculoDTO> getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(
			BigDecimal idToNegSeccion, BigDecimal idTcTipoVehiculo) {
		StringBuffer queryString = new StringBuffer("SELECT model.tipoUsoVehiculoDTO FROM NegocioTipoUso model");
		queryString.append(" WHERE model.tipoUsoVehiculoDTO.idTcTipoVehiculo = :idTcTipoVehiculo");
		queryString.append(" AND model.negocioSeccion.idToNegSeccion = :idToNegSeccion");

		queryString.append(" ORDER BY model.tipoUsoVehiculoDTO.descripcionTipoUsoVehiculo ASC");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("idTcTipoVehiculo", idTcTipoVehiculo);
		query.setParameter("idToNegSeccion", idToNegSeccion);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@Override
	public NegocioTipoUso findByNegocioSeccionAndTipoUsoDescripcion(
			NegocioSeccion negocioSeccion, String tipoUsoDescripcion) {
		final String jpql = "select model from NegocioTipoUso model where model.negocioSeccion = :negocioSeccion " +
			"and upper(model.tipoUsoVehiculoDTO.descripcionTipoUsoVehiculo) = :tipoUsoDescripcion";
		TypedQuery<NegocioTipoUso> query = entityManager.createQuery(jpql, NegocioTipoUso.class);
		query.setParameter("negocioSeccion", negocioSeccion);
		query.setParameter("tipoUsoDescripcion", tipoUsoDescripcion.toUpperCase());
		List<NegocioTipoUso> resultList = query.getResultList();
		//Los tipoUso no deberian repetirse con la descripcion sin embargo si se pueden repetir por eso la condicion checa
		//que sea mayor a 0 en lugar de que sea igual a uno y para estos casos regresaria el primero de la lista. Esto puede generar un
		//comportamiento no deterministico.
		if (resultList.size() > 0){
			return resultList.get(0);
		}
		return null;
	}

	
}
