package mx.com.afirme.midas2.dao.impl.cobranza;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.cobranza.NotificaFacturaReciboDAO;
import mx.com.afirme.midas2.domain.cobranza.FiltroRecibos;
import mx.com.afirme.midas2.domain.cobranza.Recibos;

@Stateless
public class NotificaFacturaReciboDAOImpl implements NotificaFacturaReciboDAO {

	@Override
	public Long obtenerTotalRecibos() {
		// TODO Apéndice de método generado automáticamente
		return null;
	}

	@Override
	public List<Recibos> buscarRecibos( FiltroRecibos filtro) {
		// TODO Apéndice de método generado automáticamente
		
		List<Recibos> ArrayRecibos = null;
		ArrayRecibos = new ArrayList<Recibos>();
		
		String spName="MIDAS.PKG_FACTURA_DOM.SP_FACTURAS";
 
 
		
		String [] atributosDTO = { 
									"idCotizacion" 	, "contratante" 	,"idRecibo"  	, "numRecibo",
									"vencimiento"	, "folioFiscal"		,"serieFiscal"	,
									"importe"		, "llaveComprobante","archivoXml"	,
									"archivoPdf"    , "email"		    , "poliza" 		,"endoso"	 , 
									"finivigencia"	, "ffinvigencia"	, "ramo"
								 };
		String [] columnasCursor = {  "ID_COTIZACION"   , "NOM_SOLICITANTE"  , "ID_RECIBO"  		, "NUM_FOLIO_RBO",
									  "F_VENCTO_RECIBO" , "NUM_FOLIO_FISCAL" , "SERIE_FOLIO_FISCAL"	,
									  "IMP_PRIMA_TOTAL"	, "LLAVE_COMPROBANTE", "ARCHIVOXML"         , 		
									  "ARCHIVOPDF" 	    , "EMAIL"		     , "POLIZA"			    , "NUM_ENDOSO", 
									  "F_INI_VIGENCIA"  , "F_FIN_VIGENCIA"   , "NOM_PRODUCTO" 
                				   };
		try {
			
			
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceMapeoResultados(Recibos.class.getCanonicalName() , atributosDTO, columnasCursor);
			
			storedHelper.estableceParametro("ppoliza"       , filtro.getPoliza());
			storedHelper.estableceParametro("pidcontratante", filtro.getAsegurado() );
			storedHelper.estableceParametro("pidagente"     , filtro.getAgente() );
			storedHelper.estableceParametro("ptodo"         , filtro.getFiltro() );
			
			System.out.println("poliza:"+filtro.getPoliza()+"/Asegurado:"+filtro.getAsegurado()+"/Agente:"+filtro.getAgente()+"/Tipo:"+filtro.getFiltro());

			ArrayRecibos = storedHelper.obtieneListaResultados();
			
			return ArrayRecibos;
			
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName,e);
		}
	}
	
	
	public String registrarproceso(String descripcion){
		StoredProcedureErrorLog.doLog2("PDFADOM",StoredProcedureErrorLog.TipoAccion.GUARDAR, "PKG_FACTURA_DOM", descripcion);
		return "ok";
	}

}
