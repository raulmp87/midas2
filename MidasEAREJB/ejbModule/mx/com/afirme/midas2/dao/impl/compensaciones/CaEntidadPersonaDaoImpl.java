/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaEntidadPersonaDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;

import org.apache.log4j.Logger;

@Stateless
public class CaEntidadPersonaDaoImpl implements CaEntidadPersonaDao {
	
	
	public static final String PARAMETROS_ID = "parametrosId";
	public static final String NOMBRES = "nombres";
	public static final String APATERNO = "apaterno";
	public static final String AMATERNO = "amaterno";
	
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	public static final String COMPENSADICIOID = "caCompensacion.id";
	public static final String TIPOENTIDADCA = "caTipoEntidad.id";
	public static final String TOAGENTEID = "agente.idAgente";
	public static final String PROVEEDORID = "idProveedor";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaEntidadPersonaDaoImpl.class);
	
	public void save(CaEntidadPersona entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaEntidadPersona entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaEntidadPersona.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaEntidadPersona update(CaEntidadPersona entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaEntidadPersona result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaEntidadPersona 	::		CaEntidadPersonaDaoImpl	::	update	::	ERROR	::	",re);
	        throw re;
        }
    }
    
    public CaEntidadPersona findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaEntidadPersonaDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaEntidadPersona instance = entityManager.find(CaEntidadPersona.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaEntidadPersonaDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaEntidadPersonaDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<CaEntidadPersona> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaEntidadPersonaDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaEntidadPersona model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaEntidadPersonaDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaEntidadPersonaDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}
    public CaEntidadPersona findByPropertySingleResult(String propertyName, final Object value){
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaEntidadPersonaDaoImpl	::	findByPropertySingleResult	::	INICIO	::	");
    	try {
			final String queryString = "select model from CaEntidadPersona model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaEntidadPersonaDaoImpl	::	findByPropertySingleResult	::	FIN	::	");
			return  (CaEntidadPersona) query.getSingleResult();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaEntidadPersonaDaoImpl	::	findByPropertySingleResult	::	ERROR	::	",re);
			return null;
		}
    }
    
    public List<CaEntidadPersona> findByNombres(Object nombres
	) {
		return findByProperty(NOMBRES, nombres
		);
	}
    
    public List<CaEntidadPersona> findByParametrosId(Object parametrosId
	) {
		return findByProperty(PARAMETROS_ID, parametrosId
		);
	}
	
	public List<CaEntidadPersona> findByApaterno(Object apaterno
	) {
		return findByProperty(APATERNO, apaterno
		);
	}
	
	public List<CaEntidadPersona> findByAmaterno(Object amaterno
	) {
		return findByProperty(AMATERNO, amaterno
		);
	}
    
	public List<CaEntidadPersona> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaEntidadPersona> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
		
	public List<CaEntidadPersona> findByCompensAdicioId(Long compensAdicioId
	) {
		return findByProperty(COMPENSADICIOID, compensAdicioId
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaEntidadPersona> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaEntidadPersonaDaoImpl	::	findAll	::	INICIO	::	");
			try {
				final String queryString = "select model from CaEntidadPersona model";
				Query query = entityManager.createQuery(queryString);
				LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaEntidadPersonaDaoImpl	::	findAll	::	FIN	::	");
				return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaEntidadPersonaDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}

	public CaEntidadPersona findByCompensacionAndTipoEntidadAndClave(Long compensAdicioId, String clave, Long valorClave,Long tipoEntidad){
		LOGGER.info(">> findByCompensacionAndTipoEntidadAndClave()");		
		CaEntidadPersona caEntidadPersona = null;
		try {
			
			StringBuilder queryBuilder = new StringBuilder("SELECT model FROM CaEntidadPersona model ");
			queryBuilder.append(" WHERE ");
			queryBuilder.append(" model.").append(COMPENSADICIOID).append(" = :compensAdicioId ");
			queryBuilder.append(" AND ");
			queryBuilder.append(" model.").append(clave).append(" = :clave ");
			queryBuilder.append(" AND ");
			queryBuilder.append(" model.").append(TIPOENTIDADCA).append(" = :tipoEntidad ");

			Query query = entityManager.createQuery(queryBuilder.toString(), CaEntidadPersona.class);
			
			query.setParameter("compensAdicioId", compensAdicioId);
			query.setParameter("clave",valorClave);
			query.setParameter("tipoEntidad", tipoEntidad);			
			
			caEntidadPersona = (CaEntidadPersona) query.getSingleResult();
			
		} catch (Exception e) {			
			if(e instanceof NoResultException){
				LOGGER.info(" -- No se encontraron resultados ");
			}else{
				LOGGER.error("Información del Error", e);
			}
		}
		LOGGER.info("<< findByCompensacionAndTipoEntidadAndClave()");
		return caEntidadPersona;		
	}
	
	public CaEntidadPersona findByCompensacionAndTipoEntidadAndClave(Long compensAdicioId, DatosSeycos datosSeycos, Long tipoEntidad){
		LOGGER.info(">> findByCompensacionAndTipoEntidadAndClave()");		
		CaEntidadPersona caEntidadPersona = null;
		try {
			int index = 1;
			StringBuilder queryBuilder = new StringBuilder("SELECT per.* FROM MIDAS.ca_entidadpersona per ");
			queryBuilder.append(" WHERE ").append(" per.compensacion_id").append(" = ? ");
			queryBuilder.append(" AND ").append("per.id_agente").append(" = ? ");
			queryBuilder.append(" AND ").append(" per.id_empresa").append(" = ? ");
			queryBuilder.append(" AND ").append(" per.f_sit").append(" = ? ");
			queryBuilder.append(" AND ").append(" per.tipoentidad_id").append(" = ? ");

			Query query = entityManager.createNativeQuery(queryBuilder.toString(), CaEntidadPersona.class);
			
			query.setParameter(index++, compensAdicioId);
			query.setParameter(index++,datosSeycos.getIdAgente());
			query.setParameter(index++,datosSeycos.getIdEmpresa());
			query.setParameter(index++,datosSeycos.getDateFsit());
			query.setParameter(index++, tipoEntidad);			
			
			caEntidadPersona = (CaEntidadPersona) query.getSingleResult();
			
		} catch (Exception e) {			
			if(e instanceof NoResultException){
				LOGGER.info(" -- No se encontraron resultados ");
			}else{
				LOGGER.error("Información del Error", e);
			}
		}
		LOGGER.info("<< findByCompensacionAndTipoEntidadAndClave()");
		return caEntidadPersona;		
	}
	
	@SuppressWarnings("unchecked")
	public List<CaEntidadPersona> findByAgente(Agente agente){
		LOGGER.info("	::	[INF]	::	Buscando por Agente 	::		CaEntidadPersonaDaoImpl	::	findByAgente	::	INICIO	::	");
		List<CaEntidadPersona> result = null;
		try {
			final String queryString = "select model from CaEntidadPersona model where model.agente.id = :idToAgente ";
			Query query = entityManager.createQuery(queryString);			
			query.setParameter("idToAgente", agente.getId());
			LOGGER.info("	::	[INF]	::	Se busco por Agente 	::		CaEntidadPersonaDaoImpl	::	findByAgente	::	FIN	::	");
				result = query.getResultList();
			if(result != null && !result.isEmpty())
				return result;
			else
				return null;
		} catch (RuntimeException e) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Agente 	::		CaEntidadPersonaDaoImpl	::	findByAgente	::	ERROR	::	",e);
			throw e;
		}
	}

}