/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaRamoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;

import org.apache.log4j.Logger;

@Stateless
public class CaRamoDaoImpl implements CaRamoDao {
	public static final String NOMBRE = "nombre";
	public static final String IDENTIFICADOR = "identificador";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaRamoDaoImpl.class);
	
    public void save(CaRamo entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaRamo 	::		CaRamoDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaRamo 	::		CaRamoDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaRamo 	::		CaRamoDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public void delete(CaRamo entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaRamo 	::		CaRamoDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaRamo.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaRamo 	::		CaRamoDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaRamo 	::		CaRamoDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaRamo update(CaRamo entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaRamo 	::		CaRamoDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaRamo result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaRamo 	::		CaRamoDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaRamo 	::		CaRamoDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaRamo findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaRamoDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaRamo instance = entityManager.find(CaRamo.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaRamoDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaRamoDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<CaRamo> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaRamoDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaRamo model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaRamoDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaRamoDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaRamo> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaRamo> findByIdentificador(Object identificador
	) {
		return findByProperty(IDENTIFICADOR, identificador
		);
	}
	
	public List<CaRamo> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaRamo> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaRamo> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaRamo> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaRamoDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaRamo model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaRamoDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaRamoDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
