/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaRangosDao;
import mx.com.afirme.midas2.domain.compensaciones.CaRangos;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless
public class CaRangosDaoImpl implements CaRangosDao {
	public static final String NOMBRE = "nombre";
	public static final String VALORCOMPENSACION = "valorCompensacion";
	public static final String VALORMINIMO = "valorMinimo";
	public static final String VALORMAXIMO = "valorMaximo";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	public static final String PARAMETROSGRALESID = "caParametros.id";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaRangosDaoImpl.class);
	
	public void save(CaRangos entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaRangos 	::		CaRangosDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaRangos 	::		CaRangosDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaRangos 	::		CaRangosDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaRangos entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaRangos 	::		CaRangosDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaRangos.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaRangos 	::		CaRangosDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaRangos 	::		CaRangosDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }

    public CaRangos update(CaRangos entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaRangos 	::		CaRangosDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaRangos result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaRangos 	::		CaRangosDaoImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaRangos 	::		CaRangosDaoImpl	::	update	::	ERROR	::	",re);
        	throw re;
        }
    }
    
    public CaRangos findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaRangosDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaRangos instance = entityManager.find(CaRangos.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaRangosDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaRangosDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaRangos> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaRangosServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaRangos model where model." 
			 						+ propertyName + "= :propertyValue and model." + BORRADOLOGICO + "= :borradoLogico ORDER BY model.valorMinimo ASC";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setParameter("borradoLogico", ConstantesCompensacionesAdicionales.BORRADOLOGICONO);			
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaRangosServiceImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaRangosServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}		
	public List<CaRangos> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaRangos> findByValorminimo(Object valorminimo
	) {
		return findByProperty(VALORMINIMO, valorminimo
		);
	}
	
	public List<CaRangos> findByValormaximo(Object valormaximo
	) {
		return findByProperty(VALORMAXIMO, valormaximo
		);
	}
	
	public List<CaRangos> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaRangos> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	public List<CaRangos> findByValorcompensacion(Object valorcompensacion
	) {
		return findByProperty(VALORCOMPENSACION, valorcompensacion
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaRangos> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaRangosDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaRangos model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaRangosDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaRangosDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
	public List<CaRangos> findAllbyParametrosGralesId(Long parametrosGralesId){
		return findByProperty(PARAMETROSGRALESID, parametrosGralesId);		
	}
}
