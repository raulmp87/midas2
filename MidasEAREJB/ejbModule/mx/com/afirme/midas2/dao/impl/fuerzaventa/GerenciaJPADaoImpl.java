package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityNotFoundException;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.fuerzaventa.GerenciaJPADao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.dao.impl.domicilio.DomicilioUtils;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote.TipoDomicilio;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote.TipoAccionFuerzaVenta;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.commons.beanutils.BeanUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;
@Stateless
public class GerenciaJPADaoImpl extends EntidadHistoricoDaoImpl implements GerenciaJPADao{
	private DomicilioFacadeRemote domicilioFacade;
	private ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade;
	@EJB
	private EntidadService entidadService;
	@Override
	public List<GerenciaView> findByFiltersView(Gerencia filtroGerencia){
		List<GerenciaView> lista=new ArrayList<GerenciaView>();
		//Si trae datos de fecha entonces busca por medio de sql, sino con jpql
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Domicilio domicilio=(isNotNull(filtroGerencia) && isNotNull(filtroGerencia.getDomicilio()))?filtroGerencia.getDomicilio():null;
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select distinct entidad.id as id, entidad.idGerencia as idGerencia, entidad.descripcion as descripcion, ");
		queryString.append(" entidad.claveestatus as claveestatus,persona.nombreCompleto as nombreGerente,entidad.idPersonaResponsable as idPersonaResponsable, centro.descripcion as descripcionCentroOperacion, centro.id as idCentroOperacion  "); 
		queryString.append(" from MIDAS.toGerencia entidad ");		  
		queryString.append(" inner join MIDAS.VW_PERSONA persona on(persona.idpersona=entidad.idPersonaResponsable) ");
		queryString.append(" inner join MIDAS.TOCENTROOPERACION centro on(centro.id=entidad.CENTROOPERACION_ID) ");
		queryString.append(" inner join MIDAS.TCGRUPOACTUALIZACIONAGENTES grupo on(grupo.id=20)   "); 
		queryString.append(" left join MIDAS.TLACTUALIZACIONAGENTES act on(act.grupoActualizacionAgentes_id=grupo.id and act.idRegistro=entidad.id and (act.tipoMovimiento is null or act.tipoMovimiento like 'ALTA' )) ");
		boolean domicilioEmpty=DomicilioUtils.isEmptyAddress(domicilio);
		if(!domicilioEmpty){
			queryString.append(" LEFT JOIN MIDAS.VW_DOMICILIO dom ON (dom.idDomicilio=entidad.idDomicilio ) ");
		}
		queryString.append(" where ");
		if(isNotNull(filtroGerencia)){
			int index=1;
			if(isNotNull(filtroGerencia.getId())){
				addCondition(queryString, " entidad.id=? ");
				params.put(index, filtroGerencia.getId());
				index++;
			}else if(isNotNull(filtroGerencia.getIdGerencia())){
				addCondition(queryString, " entidad.idGerencia=? ");
				params.put(index, filtroGerencia.getIdGerencia());
				index++;
			}else{
				if(filtroGerencia.getClaveEstatus()!=null){
					addCondition(queryString, " entidad.claveEstatus=? ");
					params.put(index, filtroGerencia.getClaveEstatus());
					index++;					
				}
				if(filtroGerencia.getDescripcion()!=null && !filtroGerencia.getDescripcion().isEmpty()){
					addCondition(queryString, "UPPER(entidad.descripcion) like UPPER(?)");
					params.put(index, "%"+filtroGerencia.getDescripcion()+"%");
					index++;
				}
				
				Persona personaResponsable=filtroGerencia.getPersonaResponsable();
				if(personaResponsable!=null){
					if(personaResponsable.getNombreCompleto()!=null && !personaResponsable.getNombreCompleto().isEmpty()){
						addCondition(queryString, "UPPER(persona.nombreCompleto) like UPPER(?)");
						params.put(index, "%"+personaResponsable.getNombreCompleto()+"%");
						index++;
					}
				}
				
				CentroOperacion centroOperacion=filtroGerencia.getCentroOperacion();
				if(centroOperacion!=null){
					if(centroOperacion.getId()!=null){
						addCondition(queryString, "centro.id=?");
						params.put(index, centroOperacion.getId());
						index++;
					}
				}
				
				if(isNotNull(domicilio)){
					if(isValid(domicilio.getClaveEstado())){
						addCondition(queryString, " dom.claveEstado like UPPER(?)");
						params.put(index,  domicilio.getClaveEstado());
						index++;
					}
					if(isValid(domicilio.getClaveCiudad())){
						addCondition(queryString, " dom.claveCiudad like UPPER(?)");
						params.put(index,  domicilio.getClaveCiudad());
						index++;
					}
					if(isValid(domicilio.getCodigoPostal())){
						addCondition(queryString, " TRIM(dom.codigoPostal) like TRIM(UPPER(?))");
						params.put(index,  domicilio.getCodigoPostal());
						index++;
					}
					if(isValid(domicilio.getNombreColonia())){
						addCondition(queryString, " TRIM(dom.colonia) like TRIM(UPPER(?))||'%'");
						params.put(index,  domicilio.getNombreColonia());
						index++;
					}
				}
				if(isNotNull(filtroGerencia.getFechaInicio())){
					Date fecha=filtroGerencia.getFechaInicio();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroGerencia.getFechaFin())){
					Date fecha=filtroGerencia.getFechaFin();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");						
					params.put(index,  fechaString);
					index++;
				}
			}
			if(params.isEmpty()){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,GerenciaView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			query.setMaxResults(100);
			lista=query.getResultList();
		}
		return lista;
	}
	/**
	 * Obtiene la lista de todas las gerencias
	 * @param onlyActive
	 * @return
	 */
	@Override
	public List<GerenciaView> getList(boolean onlyActive){
		List<GerenciaView> lista=new ArrayList<GerenciaView>();
		//Si trae datos de fecha entonces busca por medio de sql, sino con jpql		
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Gerencia filtro=new Gerencia();
		if(onlyActive){
			filtro.setClaveEstatus(1L);
		}
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT DISTINCT entidad.id as id, entidad.idGerencia as idGerencia, entidad.descripcion as descripcion ");
		queryString.append(" from MIDAS.toGerencia entidad "); 
		queryString.append(" where ");
		if(isNotNull(filtro)){
			int index=1;
			if(filtro.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtro.getClaveEstatus());
				index++;
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.descripcion asc ";
			Query query=entityManager.createNativeQuery(finalQuery,GerenciaView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			lista=query.getResultList();
		}
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Gerencia> findByFilters(Gerencia filtroGerencia, String fechaHistorico) {
		
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;
		
		String entityDomicilio = "domicilio";
		
		if(isHistorico)
		{
			entityDomicilio = "domicilioHistorico";	
		}
		
		List<Gerencia> lista=new ArrayList<Gerencia>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from Gerencia model left join fetch model." + entityDomicilio + " left join fetch model.centroOperacion left join fetch model.personaResponsable ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtroGerencia!=null){
			if(filtroGerencia.getDescripcion()!=null && !filtroGerencia.getDescripcion().isEmpty()){
				addCondition(queryString, "UPPER(model.descripcion) like UPPER(:descripcion)");
				params.put("descripcion", "%"+filtroGerencia.getDescripcion()+"%");
			}
			if(filtroGerencia.getClaveEstatus()!=null){
				addCondition(queryString, "model.claveEstatus=:claveEstatus");
				params.put("claveEstatus", filtroGerencia.getClaveEstatus());
			}
			Domicilio domicilio=filtroGerencia.getDomicilio();
			if(domicilio!=null){
				if(domicilio.getClaveEstado()!=null && !domicilio.getClaveEstado().isEmpty()){
					addCondition(queryString, "model.domicilio.claveEstado=:claveEstado");
					params.put("claveEstado", domicilio.getClaveEstado());
				}
				if(domicilio.getClaveCiudad()!=null && !domicilio.getClaveCiudad().isEmpty()){
					addCondition(queryString, "model.domicilio.claveCiudad=:claveCiudad");
					params.put("claveCiudad", domicilio.getClaveCiudad());
				}
				if(domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().isEmpty()){
					addCondition(queryString, "model.domicilio.nombreColonia like :nombreColonia");
					params.put("nombreColonia", domicilio.getNombreColonia()+"%");
				}
				if(domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().isEmpty()){
					addCondition(queryString, "model.domicilio.codigoPostal like :codigoPostal");
					params.put("codigoPostal", domicilio.getCodigoPostal()+"%");
				}
			}
			CentroOperacion centroOperacion=filtroGerencia.getCentroOperacion();
			if(centroOperacion!=null){
				if(centroOperacion.getId()!=null){
					addCondition(queryString, "model.centroOperacion.id=:idCentroOperacion");
					params.put("idCentroOperacion", centroOperacion.getId());
				}
			}
			Persona personaResponsable=filtroGerencia.getPersonaResponsable();
			if(personaResponsable!=null){
				if(personaResponsable.getNombreCompleto()!=null && !personaResponsable.getNombreCompleto().isEmpty()){
					addCondition(queryString, "UPPER(model.personaResponsable.nombreCompleto) like UPPER(:nombreResponsable)");
					params.put("nombreResponsable", "%"+personaResponsable.getNombreCompleto()+"%");
				}
			}
			if(filtroGerencia.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtroGerencia.getId());
			}
		}
		if(!isHistorico)
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista=query.getResultList();
		}
		else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
			
			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString).toString());
			historicalQuery.addArgument("id");
			historicalQuery.addArgumentValue(filtroGerencia.getId());
			historicalQuery.refreshIdentityMapResult();
			lista = (List<Gerencia>)historicalSession.executeQuery(historicalQuery);			
		}
		
		return lista;
	}

	@Override
	public Gerencia saveFull(Gerencia gerencia) throws Exception {
		if(gerencia==null){
			throw new Exception("Gerencia is null");
		}
		if(gerencia.getCentroOperacion() == null || gerencia.getCentroOperacion().getId() == null){
			throw new Exception("Centro Operacion is null");
		}
		// Valida que la gerencia no tenga datos asociados
		validaRelacion(gerencia);
		CentroOperacion centro=gerencia.getCentroOperacion();
		if(centro!=null && centro.getId()!=null){
			try{
				centro=entityManager.getReference(CentroOperacion.class,centro.getId());
			}catch(EntityNotFoundException e){
				centro=entityManager.find(CentroOperacion.class,centro.getId());
			}
			entityManager.refresh(centro);
			gerencia.setCentroOperacion(centro);
		}else{
			gerencia.setCentroOperacion(centro);
		}	
		
		boolean esUpdate=false;
		Domicilio domicilio=gerencia.getDomicilio();
		//TODO Remove after stored procedure 
		if(domicilio==null){
			domicilio=Domicilio.getDummy();
		}
		if(domicilio!=null){
			DomicilioPk pk=(domicilio!=null && domicilio.getIdDomicilio()!=null)?domicilio.getIdDomicilio():new DomicilioPk();
			Long idDomicilio=(pk!=null)?pk.getIdDomicilio():null;
			//Si no existe el domicilio entonces se inserta
			//if(idDomicilio==null){
				Long idPersona=(gerencia.getPersonaResponsable()!=null && gerencia.getPersonaResponsable().getIdPersona()!=null)?gerencia.getPersonaResponsable().getIdPersona():null;
				domicilio.setIdPersona(idPersona);
				TipoDomicilio tipoDomicilio = domicilioFacade.getTipoDomicilioPorClave("GERE");
				domicilio.setTipoDomicilio(tipoDomicilio.getValue());
				idDomicilio=domicilioFacade.save(domicilio, null);
			//}
			pk.setIdDomicilio(idDomicilio);
			pk.setIdPersona(idPersona);
			Domicilio persistAddress=entityManager.getReference(Domicilio.class, pk);
			
			if(idPersona != null)
			{
				Persona contratante = this.findById(Persona.class, idPersona);
				gerencia.setPersonaResponsable(contratante);			
			}
			
			gerencia.setDomicilio(persistAddress);
		}
		if(gerencia.getId()!=null){
			update(gerencia);
			esUpdate=true;
		}else{
			persist(gerencia);
		}
		//Sección de replicar info a Seycos.
		
		if(!esUpdate){//le setea la clave igual al id 
			gerencia.setIdGerencia(gerencia.getId());//En este caso el idSeycos es el id persona insertada en Seycos.persona que es el id de afianzadora en seycos-midas.
			update(gerencia);//se actualiza la afianzadora persistida con el id de afianzadora de seycos
		}
		update(centro);
		entityManager.refresh(centro);
		replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(gerencia,TipoAccionFuerzaVenta.GUARDAR);//Indica que va a replicar pero va a guardar en seycos
		return gerencia;
	}

	@Override
	public void unsubscribe(Gerencia gerencia) throws Exception {
		if(gerencia==null || gerencia.getId()==null){
			throw new Exception("Gerencia is null");
		}	
			gerencia.setClaveEstatus(0l);
			validaRelacion(gerencia);
			gerencia=loadById(gerencia,null);
			gerencia.setClaveEstatus(0l);
			update(gerencia);//Se actualiza su estatus
			replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(gerencia, TipoAccionFuerzaVenta.INACTIVAR);
	}
	/***Sets && gets******************************************************/
	@EJB
	public void setDomicilioFacade(DomicilioFacadeRemote domicilioFacade) {
		this.domicilioFacade = domicilioFacade;
	}
	@EJB
	public void setReplicarFuerzaVentaSeycosFacade(
			ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade) {
		this.replicarFuerzaVentaSeycosFacade = replicarFuerzaVentaSeycosFacade;
	}

	@Override
	public List<Gerencia> findByCentroOperacion(Long idCentroOperacion) {
		if(idCentroOperacion!=null){
			CentroOperacion centroOperacion=new CentroOperacion();
			centroOperacion.setId(idCentroOperacion);
			Gerencia filtroGerencia=new Gerencia();
			filtroGerencia.setClaveEstatus(1l);//Solo activos
			filtroGerencia.setCentroOperacion(centroOperacion);
			Map<String, Object> param = new HashMap<String, Object>();
			param.put("centroOperacion.id", idCentroOperacion);
			param.put("claveEstatus", 1L);
			return this.entidadService.findByProperties(Gerencia.class, param);
//			return findByFilters(filtroGerencia, null);
		}
		return new ArrayList<Gerencia>();
	}
	/**
	 * Metodo que obtiene la lista de gerencias por centro de operacion de manera ligera. 
	 * @param idCentroOperacion
	 * @return
	 */
	@Override
	public List<GerenciaView> findByCentroOperacionLightWeight(Long idParent) {
		List<GerenciaView> list=new ArrayList<GerenciaView>();
		if(isNotNull(idParent)){
			CentroOperacion parent=new CentroOperacion();
			parent.setId(idParent);
			Gerencia filtro=new Gerencia();
			filtro.setClaveEstatus(1l);//Solo activos
			filtro.setCentroOperacion(parent);
			return findByFiltersView(filtro);
		}
		return list;
	}
	
	@Override
	public Gerencia loadById(Gerencia entidad, String fechaHistorico) {
		if (entidad != null && entidad.getId() != null) {
			Gerencia filtro = new Gerencia();
			filtro.setId(entidad.getId());
			List<Gerencia> list = findByFilters(filtro, fechaHistorico);
			if (list != null && !list.isEmpty()) {
				entidad = list.get(0);
				
				if (fechaHistorico != null && !fechaHistorico.isEmpty()) {
					Domicilio domicilioTemp = new Domicilio();
					try {
						BeanUtils.copyProperties(domicilioTemp,
								entidad.getDomicilioHistorico());
					} catch (Exception e) {
						e.printStackTrace();
					}

					entidad.setDomicilio(domicilioTemp);
				}
			}
		}

		return entidad;
	}
	/**
	 * Obtiene la lista de gerencias segunda las proporcionadas excluyendo los centros de operacion indicados.
	 * @param gerencias
	 * @param centrosOperacionExcluyentes
	 * @return
	 */
	@Override
	public List<GerenciaView> findGerenciasConCentroOperacionExcluyentes(List<Long> gerencias,List<Long> centrosOperacionExcluyentes){
		List<GerenciaView> list=new ArrayList<GerenciaView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Gerencia filtro=new Gerencia();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT DISTINCT entidad.id as id, entidad.idGerencia as idGerencia, entidad.descripcion as descripcion ");
		queryString.append(" from MIDAS.toGerencia entidad ");
		queryString.append(" inner join MIDAS.toCentroOperacion centro on(centro.id=entidad.centroOperacion_Id) ");
		queryString.append(" where ");
		if(isNotNull(filtro)){
			int index=1;
			if(filtro.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtro.getClaveEstatus());
				index++;
			}
			if(!isEmptyList(gerencias)){
				queryString.append(" entidad.id in ( ");
				StringBuilder gerenciaList=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idGerencia:gerencias){
					if(isNotNull(idGerencia)){
						if(i<(gerencias.size()-1)){
							gerenciaList.append(idGerencia+",");
						}else{
							gerenciaList.append(idGerencia);
						}
					}
					i++;
				}
//				params.put(index, gerenciaList.toString());
//				index++;
				queryString.append(gerenciaList.toString()+") or ");
			}
			if(!isEmptyList(centrosOperacionExcluyentes)){
				queryString.append(" centro.id  in ( ");
				StringBuilder listaExcluyente=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idCentroOperacion:centrosOperacionExcluyentes){
					if(isNotNull(idCentroOperacion) && isNotNull(idCentroOperacion)){
						if(i<(centrosOperacionExcluyentes.size()-1)){
							listaExcluyente.append(idCentroOperacion+",");
						}else{
							listaExcluyente.append(idCentroOperacion);
						}
					}
					i++;
				}
//				params.put(index, listaExcluyente.toString());
//				index++;
				queryString.append(listaExcluyente.toString()+") or ");
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.descripcion asc ";
			Query query=entityManager.createNativeQuery(finalQuery,GerenciaView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
		}
		return list;
	}	
	
	/**
	 * Valida que la entidad no tenga asociado
	 * un dato 
	 * @param gerencia
	 * @throws Exception
	 */
	private void validaRelacion(Gerencia gerencia) throws Exception {
		if (gerencia.getClaveEstatus().equals(0l)) {
//			List<Ejecutivo> checaRelcionGerencia = findByProperty(Ejecutivo.class,
//					"gerencia.id", gerencia.getId());
			Map<String,Object> params = new HashMap<String,Object>(); 
			params.put("gerencia.id", gerencia.getId());
			params.put("claveEstatus", 1L);
			List<Ejecutivo> checaRelcionGerencia = findByProperties(Ejecutivo.class, params);
			if (checaRelcionGerencia != null && !checaRelcionGerencia.isEmpty()) {
				throw new Exception(
						"La gerencia est\u00E1 relacionada con un Ejecutivo");
			}
		}
	}
}
