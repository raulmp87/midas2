/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoMonedaDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoMoneda;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoMonedaDaoImpl implements CaTipoMonedaDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoMonedaDaoImpl.class);
	
	public void save(CaTipoMoneda entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }

    public void delete(CaTipoMoneda entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoMoneda.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }

    public CaTipoMoneda update(CaTipoMoneda entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoMoneda result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoMoneda 	::		CaTipoMonedaDaoImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaTipoMoneda findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoMonedaDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoMoneda instance = entityManager.find(CaTipoMoneda.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoMonedaDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoMonedaDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoMoneda> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoMonedaDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoMoneda model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoMonedaDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoMonedaDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoMoneda> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoMoneda> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoMoneda> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoMoneda> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoMoneda entities.
	  	  @return List<CaTipoMoneda> all CaTipoMoneda entities
	 */
	@SuppressWarnings("unchecked")
	public List<CaTipoMoneda> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoMonedaDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoMoneda model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoMonedaDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoMonedaDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
