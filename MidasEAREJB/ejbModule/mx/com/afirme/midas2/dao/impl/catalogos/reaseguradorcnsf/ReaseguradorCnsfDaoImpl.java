package mx.com.afirme.midas2.dao.impl.catalogos.reaseguradorcnsf;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.catalogos.reaseguradorcnsf.ReaseguradorCnsfDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;

@Stateless
public class ReaseguradorCnsfDaoImpl extends EntidadDaoImpl implements ReaseguradorCnsfDao{

	@SuppressWarnings("deprecation")
	public List<ReaseguradorCnsfMov> findByFilters(ReaseguradorCnsfMov filtroColorVehiculo) {

		String queryString = "SELECT DISTINCT MOV.* " +
		"FROM midas.CNSFREASEGURADORMOV MOV " +
		"INNER JOIN midas.CNSFREASEGURADOR REA ON " +
		"REA.ID = MOV.IDREASEGURADOR " +
		"INNER JOIN midas.cnsf_calificaciones_reas CAL ON " +
		"cal.AGENCIA = mov.idagencia " +
		"and cal.id = mov.idcalificacion " +         
		"WHERE MOV.id != 0";
		
		if(filtroColorVehiculo.getFechacorte() != null)
			queryString += "AND TRUNC(FECHACORTE) = TO_DATE(lpad(" + filtroColorVehiculo.getFechacorte().getDate() + ",2,'0')||'/'||lpad(" +  (filtroColorVehiculo.getFechacorte().getMonth()+1) + ",2,'0')||'/'||" +  (filtroColorVehiculo.getFechacorte().getYear() +1900) + ",'dd/mm/yyyy') ";
		if(!filtroColorVehiculo.getNombreReasegurador().equals(""))
			queryString += "AND REA.NOMBREREASEGURADOR LIKE '%" + filtroColorVehiculo.getNombreReasegurador() + "%' ";
		if(!filtroColorVehiculo.getClaveReasegurador().equals(""))
			queryString += "AND REA.CLAVECNSF LIKE '%" + filtroColorVehiculo.getClaveReasegurador() + "%' ";
		if(filtroColorVehiculo.getIdagencia() != null )
			if(filtroColorVehiculo.getIdagencia().intValue()>0)
			queryString += "and cal.AGENCIA = " + filtroColorVehiculo.getIdagencia() + " ";
		if(filtroColorVehiculo.getIdcalificacion()!= null)
			if(filtroColorVehiculo.getIdcalificacion().intValue()>0)
			{	
				queryString += "and cal.ID = " + filtroColorVehiculo.getIdcalificacion() + " " +
						"AND MOV.IDCALIFICACION = " + filtroColorVehiculo.getIdcalificacion();
			}
		
		System.out.println(queryString);
		@SuppressWarnings("unchecked")
		List<ReaseguradorCnsfMov> customers = 
			(List<ReaseguradorCnsfMov>)entityManager.createNativeQuery
        (queryString, ReaseguradorCnsfMov.class)
                          .getResultList(); 
		
		return customers;
	}
	
	public int deleteReaseguradorMovs(BigDecimal idAgencia){
		
		String queryString = "delete from ReaseguradorCnsfMov model" +
		" where model.idreaseguradormov = :parameter";
		System.out.println(queryString + idAgencia);
		Query query = entityManager.createQuery(queryString);
		query.setParameter("parameter", idAgencia);
		return query.executeUpdate();
	}
	
	public void saveReaseguradorMovs(ReaseguradorCnsfMov reaseguradorCnsfMov){
		
		this.entityManager.merge(reaseguradorCnsfMov);
	}
	
	//Obtener fecha de ultimo corte procesado
	public Date getFechaUltimoCorteProcesado() {
		StoredProcedureHelper helper;
		final String sp = "MIDAS.PKG_SOLVENCIA_CNSF.GET_FECHA_ULTIMOCORTEPROCESADO";
		ReaseguradorCnsfMov result;
		Date resultDate = new Date();
		
		try {
			final String atributo = "fechacorte";
			final String columna = "fechacorte";
			
			helper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
			helper.estableceMapeoResultados(ReaseguradorCnsfMov.class.getCanonicalName(), atributo, columna);
			
			result = (ReaseguradorCnsfMov) helper.obtieneResultadoSencillo();
			
			resultDate = result.getFechacorte();
			
		} catch(Exception e) {
			LogDeMidasEJB3.log("Ha ocurrido un error al tratar de obtener la ultima fecha de corte procesada", Level.WARNING, e);
		}
		
		return resultDate;
	}
}
