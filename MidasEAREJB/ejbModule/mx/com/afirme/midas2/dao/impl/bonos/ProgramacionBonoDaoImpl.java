package mx.com.afirme.midas2.dao.impl.bonos;
import static mx.com.afirme.midas2.utils.CommonUtils.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.bonos.ProgramacionBonoDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoCentroOperacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoEjecutivo;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoGerencia;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPrioridad;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoSituacion;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoAgente;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoTipoPromotoria;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class ProgramacionBonoDaoImpl extends EntidadDaoImpl implements ProgramacionBonoDao{


	@EJB
	private EntidadService entidadService;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getNegocioEspeciales() throws Exception {
		List<GenericaAgentesView> lista= new ArrayList<GenericaAgentesView>();		
		final StringBuilder queryString = new StringBuilder("");

		queryString.append("select neg.idToNegocio AS id, neg.descripcionNegocio AS descripcion " +
				"from MIDAS.TONEGOCIO neg, MIDAS.TONEGBONOCOMISION negBono " +
				"where neg.idToNegocio = negBono.negocio_Id " +
				"and negBono.claveBono = 1 " +
				"and neg.claveestatus = 1");				
	
		Query query=entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
		lista= query.getResultList();
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GenericaAgentesView> getConfiguracionBonos(Long idTipoBono) throws Exception {
		List<GenericaAgentesView> lista= new ArrayList<GenericaAgentesView>();		
		final StringBuilder queryString = new StringBuilder("");

		queryString.append("select id AS id, descripcionBono AS descripcion " +
				"from MIDAS.toConfigBonos " +
				"where claveactivo = 1 " +
				"and idTipoBono = " + idTipoBono + " ");				
	
		Query query=entityManager.createNativeQuery(queryString.toString(),"productoXLineaVentaView");
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);
		lista= query.getResultList();
		return lista;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PolizaDTO> getProduccionDePolizas(ConfigBonos config){
		List<PolizaDTO> lista= new ArrayList<PolizaDTO>();
		if(config != null){
			try{
			List<ConfigBonoCentroOperacion> listaCentroOperaciones=config.getListaCentroOperaciones();
			List<ConfigBonoGerencia> listaGerencias=config.getListaGerencias();
			List<ConfigBonoEjecutivo> listaEjecutivos=config.getListaEjecutivos();
			List<ConfigBonoPromotoria> listaPromotorias=config.getListaPromotorias();
			List<ConfigBonoTipoPromotoria> listaTiposPromotoria=config.getListaTiposPromotoria();
			List<ConfigBonoPrioridad> listaPrioridades=config.getListaPrioridades();
			List<ConfigBonoSituacion> listaSituaciones=config.getListaSituaciones();
			List<ConfigBonoTipoAgente> listaTipoAgentes=config.getListaTipoAgentes();
			List<ConfigBonoAgente> listaAgentes=config.getListaAgentes();
			List<ConfigBonoPoliza> listaConfigPolizas = config.getListaConfigPolizas();
			
			final StringBuilder queryString = new StringBuilder("");

			queryString.append("SELECT pol.* FROM MIDAS.TOPOLIZA pol, MIDAS.TOCOTIZACION cot, MIDAS.TOSOLICITUD sol, MIDAS.TOAGENTE agen ");
			queryString.append("LEFT JOIN MIDAS.TOPROMOTORIA pro ON agen.IDPROMOTORIA = pro.ID  ");
			queryString.append("LEFT JOIN MIDAS.TOEJECUTIVO ejec ON pro.EJECUTIVO_ID = ejec.ID  ");
			queryString.append("LEFT JOIN MIDAS.TOGERENCIA geren ON ejec.GERENCIA_ID = geren.ID  ");
			queryString.append("LEFT JOIN MIDAS.TOCENTROOPERACION  cenop ON  geren.CENTROOPERACION_ID = cenop.ID  ");
			queryString.append("WHERE pol.IDTOCOTIZACION = cot.IDTOCOTIZACION  ");
			queryString.append("AND cot.IDTOSOLICITUD = sol.IDTOSOLICITUD  ");
			queryString.append("AND sol.CODIGOAGENTE = agen.ID  ");
			
			if(listaCentroOperaciones != null && !listaCentroOperaciones.isEmpty()){
				queryString.append("AND cenop.ID IN (SELECT cbco.CENTROOPERACION_ID FROM MIDAS.TRCONFIGBONOCENTROOPERACION cbco WHERE cbco.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaGerencias != null && !listaGerencias.isEmpty()){
				queryString.append("AND geren.ID IN (SELECT cbge.GERENCIA_ID FROM MIDAS.TRCONFIGBONOGERENCIA cbge WHERE cbge.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaEjecutivos != null && !listaEjecutivos.isEmpty()){
				queryString.append("AND ejec.ID IN (SELECT cbej.EJECUTIVO_ID FROM MIDAS.TRCONFIGBONOEJECUTIVO cbej WHERE cbej.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaPromotorias != null && !listaPromotorias.isEmpty()){
				queryString.append("AND pro.ID IN (SELECT cbpro.PROMOTORIA_ID FROM MIDAS.TRCONFIGBONOPROMOTORIA cbpro WHERE cbpro.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaTiposPromotoria != null && !listaTiposPromotoria.isEmpty()){
				queryString.append("AND pro.IDTIPOPROMOTORIA IN (SELECT cbtp.IDTIPOPROMOTORIA FROM MIDAS.TRCONFIGBONOTIPOPROMOTORIA cbtp WHERE cbtp.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaConfigPolizas != null && !listaConfigPolizas.isEmpty()){
				queryString.append("AND pol.IDTOPOLIZA IN (SELECT cbpol.POLIZA_ID FROM MIDAS.TRCONFIGBONOPOLIZA cbpol WHERE cbpol.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaAgentes != null && !listaAgentes.isEmpty()){
				queryString.append("AND agen.ID IN (SELECT cbagen.AGENTE_ID FROM MIDAS.TRCONFIGBONOAGENTE cbagen WHERE cbagen.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaTipoAgentes != null && !listaTipoAgentes.isEmpty()){
				queryString.append("AND agen.IDTIPOAGENTE IN (SELECT cbta.IDTIPOAGENTE FROM MIDAS.TRCONFIGBONOTIPOAGENTE cbta WHERE cbta.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaPrioridades != null && !listaPrioridades.isEmpty()){
				queryString.append("AND agen.IDPRIORIDADAGENTE IN (SELECT cbpri.IDPRIORIDADAGENTE FROM MIDAS.TRCONFIGBONOPRIORIDAD cbpri WHERE cbpri.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
			if(listaSituaciones != null && !listaSituaciones.isEmpty()){
				queryString.append("AND agen.IDSITUACIONAGENTE IN (SELECT cbsit.IDSITUACIONAGENTE FROM MIDAS.TRCONFIGBONOSITUACION cbsit WHERE cbsit.CONFIGBONO_ID = " + config.getId() + ")  ");
			}
		
			Query query=entityManager.createNativeQuery(queryString.toString(), PolizaDTO.class);
			
			lista= query.getResultList();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PolizaDTO> getProduccionDePolizasNegocioEspecial(Negocio negocio){
		List<PolizaDTO> lista= new ArrayList<PolizaDTO>();
		if(negocio != null){
		try{
			final StringBuilder queryString = new StringBuilder("");

			queryString.append("SELECT pol.* FROM MIDAS.TOPOLIZA pol, MIDAS.TOCOTIZACION cot, MIDAS.TOSOLICITUD sol, MIDAS.TOAGENTE agen ");
			queryString.append("LEFT JOIN MIDAS.TOPROMOTORIA pro ON agen.IDPROMOTORIA = pro.ID  ");
			queryString.append("LEFT JOIN MIDAS.TOEJECUTIVO ejec ON pro.EJECUTIVO_ID = ejec.ID  ");
			queryString.append("LEFT JOIN MIDAS.TOGERENCIA geren ON ejec.GERENCIA_ID = geren.ID  ");
			queryString.append("LEFT JOIN MIDAS.TOCENTROOPERACION  cenop ON  geren.CENTROOPERACION_ID = cenop.ID  ");
			queryString.append("WHERE pol.IDTOCOTIZACION = cot.IDTOCOTIZACION  ");
			queryString.append("AND cot.IDTOSOLICITUD = sol.IDTOSOLICITUD  ");
			queryString.append("AND sol.CODIGOAGENTE = agen.ID  ");
			queryString.append("AND sol.NEGOCIO_ID = " + negocio.getIdToNegocio() + "");
		
			Query query=entityManager.createNativeQuery(queryString.toString(), PolizaDTO.class);
		
			lista= query.getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		}
		return lista;
	}
	
	/**
	 * Obtiene el bono por id
	 * @param id
	 * @return
	 * @throws MidasException
	 */
	public ProgramacionBono loadById(Long id) throws MidasException{
		ProgramacionBono p=null;
		if(isNull(id)){
			onError("Favor de proporcionar el id de la programacion a cargar");
		}
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ProgramacionBono model where model.id=:idProgramacion");
		params.put("idProgramacion",id);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		List<ProgramacionBono> lista=query.getResultList();
		if(!isEmptyList(lista)){
			p=lista.get(0);
		}
		return p;
	}
	
	public ProgramacionBono loadById(ProgramacionBono programacion) throws MidasException{
		ProgramacionBono p=null;
		Long id=(isNotNull(programacion) && isNotNull(programacion.getId()))?programacion.getId():null;
		p=loadById(id);
		return p;
	}
	@Override
	public String eliminarBonosCero(String ids) throws MidasException {
		String detalleBonos="";
		if(!ids.equals("")){
			String delimiter = ",";
			String[] temp = ids.split(delimiter);
			for(int i=0; i<temp.length; i++){
				String idString = temp[i];
				Long id = (long) Integer.parseInt(idString);
				CalculoBono obj = entidadService.findById(CalculoBono.class, id);
				if(obj.getTotalBeneficiarios()==0){
					detalleBonos = obj.getDescripcionBono()+", ";
					entityManager.remove(obj);
				}
			}
		}
		return detalleBonos;
	}
	@Override 
	public List<ConfigBonos> bonoIsVigente(List<ConfigBonos> configuracionBonoList) throws MidasException{
		List<ConfigBonos> bonosNoVigentes= new ArrayList<ConfigBonos>();
		Map<String, Object> params = new HashMap<String, Object>();
		for (ConfigBonos objConfigBono : configuracionBonoList){
			if(isNotNull(objConfigBono)){
				final StringBuilder queryString=new StringBuilder("");
				queryString.append("select model from ConfigBonos model where model.id=:idConfigBono AND  model.fechaInicioVigencia<=:fechaActual  AND model.fechaFinVigencia>=:fechaActual");
				params.put("idConfigBono",objConfigBono.getId());
				params.put("fechaActual",new Date());
				Query query = entityManager.createQuery(getQueryString(queryString));
				setQueryParametersByProperties(query, params);
				query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
				List<ConfigBonos> lista=query.getResultList();
				if(lista.isEmpty()){
					bonosNoVigentes.add(objConfigBono);
				}
			}
		}
	return bonosNoVigentes;
	}
	@Override
	public  List<ConfigBonos> excluirBonoNoVigentes(List<ConfigBonos> listaBonosAExcluir, List<ConfigBonos> configuracionBonoList) throws MidasException{
		if(!listaBonosAExcluir.isEmpty()){
			configuracionBonoList.removeAll(listaBonosAExcluir);
		}
		return configuracionBonoList;
	}
	
	@Override
	public List<CalculoBonoEjecucionesView> listaCalculoBonosMonitorEjecucion() throws MidasException {
		List<CalculoBonoEjecucionesView> listaBonosEjecutados = new ArrayList<CalculoBonoEjecucionesView>();
		StringBuilder queryString =  new StringBuilder();
		queryString.append(" select m.id,m.nombreConfiguracionBono,modo.valor as modoEjecucion,est.valor as estatusEjecucion,m.idcalculoBono as idPreviewCalculo,m.fechaEjecucion ");
		queryString.append(" ,m.usuario,m.fechafinalizaCalculo ");
		queryString.append(" ,DECODE(est.valor,'En Espera','0%',(select (decode(count(logg.id),10,10,11,10,count(logg.id)) * 100 / 10)||'%'  ");  
		queryString.append(" from midas.calculoBono_log logg where logg.idEjecucion=m.id ");
		queryString.append(" and logg.id>=(select max(id) from  midas.calculoBono_log logg where observaciones ='Inicio de Ejecucion [procedimiento autonomo]' and idEjecucion=m.id))) as avance "); 
		 //se hace la operacion que si supera los 10 pasos ejecutados es porque ya termino de calcular el bono 
		queryString.append(" from midas.tocalculobono_ejecuciones m" );
		queryString.append(" inner join midas.tovalorcatalogoagentes modo on(m.idmodoEjecucion = modo.id) ");
		queryString.append(" inner join midas.tovalorcatalogoagentes est on(m.idEstatusEjecucion = est.id) ");
		queryString.append(" where to_char(fechaEjecucion,'mmyyyy') = to_char(sysdate,'mmyyyy') ");
		queryString.append(" order by DECODE(est.id , '1050','A', '1049','B','1052', 'C', '1051','D','Z') ");
		
		Query query = entityManager.createNativeQuery(queryString.toString(), CalculoBonoEjecucionesView.class);
		listaBonosEjecutados = query.getResultList();
		return listaBonosEjecutados;
	}
}
