package mx.com.afirme.midas2.dao.impl.negocio.cliente;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionGrupoPorNegocioDao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionGrupoPorNegocio;

@Stateless
public class ConfiguracionGrupoPorNegocioDaoImpl extends JpaDao<Long, ConfiguracionGrupoPorNegocio> implements
		ConfiguracionGrupoPorNegocioDao {

}
