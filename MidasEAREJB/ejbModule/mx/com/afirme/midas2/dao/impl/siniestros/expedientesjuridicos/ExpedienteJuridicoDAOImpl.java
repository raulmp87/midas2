package mx.com.afirme.midas2.dao.impl.siniestros.expedientesjuridicos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.expedientesjuridicos.ExpedienteJuridicoDAO;
import mx.com.afirme.midas2.domain.siniestros.expedientejuridico.ExpedienteJuridico;
import mx.com.afirme.midas2.dto.expedientejuridico.ExpedienteJuridicoDTO;

import com.js.util.StringUtil;

/**
* @author Israel
* @version 1.0
* @created 06-ago.-2015 10:30:24 a. m.
*/
@Stateless
public class ExpedienteJuridicoDAOImpl extends EntidadDaoImpl implements ExpedienteJuridicoDAO {

                /**
                * generar el query JPQL para devolver las lista de unmanage objects de acuerdo a
                * los param�tros de b�squeda seleccionados en el DTO
                * 
                 * @param expedienteJuridicoDTO
                */
                @Override
                public List<ExpedienteJuridico> buscarExpedientes(ExpedienteJuridicoDTO filtro){
                               Map<String, Object> parameters = new HashMap<String, Object>();  
                               parameters.put("noSiniestro"        , StringUtil.isEmpty(filtro.getNumeroSiniestro()  ) ? null:filtro.getNumeroSiniestro() );
                               parameters.put("motivoTurno"        , StringUtil.isEmpty(filtro.getMotivoTurno()      ) ? null:filtro.getMotivoTurno() ) ;
                               parameters.put("estatusJuridico"    , StringUtil.isEmpty(filtro.getEstatusJuridico()  ) ? null:filtro.getEstatusJuridico() );
                               parameters.put("expedienteProveedor", StringUtil.isEmpty(filtro.getExpedienteProveedor()) ? null:filtro.getExpedienteProveedor() );
                               parameters.put("vehiculoDetenido"   , filtro.getVehiculoDetenido() );
                               parameters.put("estatusVehiculo"    , StringUtil.isEmpty(filtro.getEstatusVehiculo()    ) ? null:filtro.getEstatusVehiculo() );
                               parameters.put("tipoAbogabo"        , (filtro.getAbogado().getAmbitoPrestadorServicio()));
                               parameters.put("abogabo"            , (filtro.getAbogado().getId()));
                               parameters.put("tipoConclusion"     , StringUtil.isEmpty(filtro.getTipoConclusion()     ) ? null:filtro.getTipoConclusion() );
                               
                               parameters.put("fechaTurnoDe"       , (filtro.getFechaTurnoDe()));
                               parameters.put("fechaTurnoHasta"    , (filtro.getFechaTurnoHasta()));
                               
                               parameters.put("fechaConclusionLegalDe"   , (filtro.getFechaConclusionLegalDe()));
                               parameters.put("fechaConclusionLegalHasta", (filtro.getFechaConclusionLegalHasta()));
                               
                               parameters.put("fechaConclusionAfirmeDe"   , (filtro.getFechaConclusionAfirmeDe()));
                               parameters.put("fechaConclusionAfirmeHasta", (filtro.getFechaConclusionAfirmeHasta()));
                               
                               
                               StringBuilder queryString = new StringBuilder("SELECT model FROM ExpedienteJuridico model  ")
                               .append(" LEFT JOIN model.abogado abogado ")
                               .append(" WHERE  (:noSiniestro IS NULL OR model.numeroSiniestro = :noSiniestro )")
                               .append(" AND  ( :motivoTurno IS NULL OR model.motivoTurno = :motivoTurno  ) ")
                               .append(" AND  ( :estatusJuridico IS NULL OR model.estatusJuridico = :estatusJuridico  ) ")
                               .append(" AND  ( :expedienteProveedor IS NULL OR model.expedienteProveedor = :expedienteProveedor  ) ")
                               .append(" AND  ( :vehiculoDetenido IS NULL OR model.vehiculoDetenido = :vehiculoDetenido  ) ")
                               .append(" AND  ( :estatusVehiculo IS NULL OR model.estatusVehiculo = :estatusVehiculo  ) ")
                               .append(" AND  ( :tipoAbogabo IS NULL OR abogado.ambitoPrestadorServicio = :tipoAbogabo  ) ")
                               .append(" AND  ( :abogabo IS NULL OR abogado.id = :abogabo  ) ")
                               .append(" AND  ( :tipoConclusion IS NULL OR model.tipoConclusion = :tipoConclusion  ) ")
                               
                               .append(" AND  ( :fechaTurnoDe IS NULL OR (  FUNC('trunc', model.fechaTurno )  >= :fechaTurnoDe    ) ) ")
                               .append(" AND  ( :fechaTurnoHasta IS NULL OR (  FUNC('trunc', model.fechaTurno )  <= :fechaTurnoHasta    ) ) ")
                               
                               .append(" AND  ( :fechaConclusionLegalDe IS NULL OR (  FUNC('trunc', model.fechaConclusionLegal )  >= :fechaConclusionLegalDe    ) ) ")
                               .append(" AND  ( :fechaConclusionLegalHasta IS NULL OR (  FUNC('trunc', model.fechaConclusionLegal )  <= :fechaConclusionLegalHasta    ) ) ")
                               
                               .append(" AND  ( :fechaConclusionAfirmeDe IS NULL OR (  FUNC('trunc', model.fechaConclusionAfirme )  >= :fechaConclusionAfirmeDe    ) ) ")
                               .append(" AND  ( :fechaConclusionAfirmeHasta IS NULL OR (  FUNC('trunc', model.fechaConclusionAfirme )  <= :fechaConclusionAfirmeHasta    ) ) ");
                               
                               @SuppressWarnings("unchecked")
                               List<ExpedienteJuridico>  resultados = this.executeQueryMultipleResult(queryString.toString(), parameters);
                               return resultados;
                }

}
