package mx.com.afirme.midas2.dao.impl.sapamis.procesos;

import java.util.Date;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import mx.com.afirme.midas2.dao.sapamis.procesos.SapAmisEjecucionesSTPDao;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;

/*******************************************************************************
 * Nombre Clase: 		SapAmisEjecucionesSTPDaoImpl.
 * 
 * Descripcion: 		Esta clase se utiliza para la ejecución de los Procesos
 * 						de Poblado para los Modulos que son requeridos para su
 * 						envio al SAP-AMIS.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisEjecucionesSTPDaoImpl implements SapAmisEjecucionesSTPDao{
	private static final long serialVersionUID = 1L;
	
	private static final String CATALOG_NAME = "PKG_OPERACIONES_SAP";
	private static final String ESQUEMA = "MIDAS";
	private static final String PARAMETRO_IN_HORA_INICIAL = " 00:00:00";
	private static final String PARAMETRO_IN_HORA_FINAL = " 23:59:59";
	private static final String PARAMETRO_IN_FECHA_INICIAL = "pFechaIni";
	private static final String PARAMETRO_IN_FECHA_FINAL = "pFechaFin";

	@SuppressWarnings("unused")
	private DataSource dataSource;
	@EJB private SapAmisUtilsService sapAmisUtilsService;

	private SimpleJdbcCall emisionPobladoCall;
	private SimpleJdbcCall siniestrosPobladoCall;
	private SimpleJdbcCall rechazosPobladoCall;
	private SimpleJdbcCall prevencionesPobladoCall;
	private SimpleJdbcCall pttPobladoCall;
	private SimpleJdbcCall salvamentoPobladoCall;
	private SimpleJdbcCall roboPobladoREPUVECall;
	private SimpleJdbcCall recuperacionPobladoREPUVECall;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void emisionPoblado(Date fechaIni, Date fechaFin) {
		String fechaIniStr = sapAmisUtilsService.convertToStrDate(fechaIni) + PARAMETRO_IN_HORA_INICIAL;
		String fechaFinStr = sapAmisUtilsService.convertToStrDate(fechaFin) + PARAMETRO_IN_HORA_FINAL;
		SqlParameterSource in = new MapSqlParameterSource().addValue(PARAMETRO_IN_FECHA_INICIAL, fechaIniStr).addValue(PARAMETRO_IN_FECHA_FINAL, fechaFinStr);
		emisionPobladoCall.execute(in);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void prevencionesPoblado(Date fechaIni, Date fechaFin) {
		String fechaIniStr = sapAmisUtilsService.convertToStrDate(fechaIni) + PARAMETRO_IN_HORA_INICIAL;
		String fechaFinStr = sapAmisUtilsService.convertToStrDate(fechaFin) + PARAMETRO_IN_HORA_FINAL;
		SqlParameterSource in = new MapSqlParameterSource().addValue(PARAMETRO_IN_FECHA_INICIAL, fechaIniStr).addValue(PARAMETRO_IN_FECHA_FINAL, fechaFinStr);
		prevencionesPobladoCall.execute(in);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void pttPoblado(Date fechaIni, Date fechaFin) {
		String fechaIniStr = sapAmisUtilsService.convertToStrDate(fechaIni) + PARAMETRO_IN_HORA_INICIAL;
		String fechaFinStr = sapAmisUtilsService.convertToStrDate(fechaFin) + PARAMETRO_IN_HORA_FINAL;
		SqlParameterSource in = new MapSqlParameterSource().addValue(PARAMETRO_IN_FECHA_INICIAL, fechaIniStr).addValue(PARAMETRO_IN_FECHA_FINAL, fechaFinStr);
		pttPobladoCall.execute(in);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void rechazosPoblado(Date fechaIni, Date fechaFin) {
		String fechaIniStr = sapAmisUtilsService.convertToStrDate(fechaIni) + PARAMETRO_IN_HORA_INICIAL;
		String fechaFinStr = sapAmisUtilsService.convertToStrDate(fechaFin) + PARAMETRO_IN_HORA_FINAL;
		SqlParameterSource in = new MapSqlParameterSource().addValue(PARAMETRO_IN_FECHA_INICIAL, fechaIniStr).addValue(PARAMETRO_IN_FECHA_FINAL, fechaFinStr);
		rechazosPobladoCall.execute(in);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void salvamentoPoblado(Date fechaIni, Date fechaFin) {
		String fechaIniStr = sapAmisUtilsService.convertToStrDate(fechaIni) + PARAMETRO_IN_HORA_INICIAL;
		String fechaFinStr = sapAmisUtilsService.convertToStrDate(fechaFin) + PARAMETRO_IN_HORA_FINAL;
		SqlParameterSource in = new MapSqlParameterSource().addValue(PARAMETRO_IN_FECHA_INICIAL, fechaIniStr).addValue(PARAMETRO_IN_FECHA_FINAL, fechaFinStr);
		salvamentoPobladoCall.execute(in);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void siniestrosPoblado(Date fechaIni, Date fechaFin) {
		String fechaIniStr = sapAmisUtilsService.convertToStrDate(fechaIni) + PARAMETRO_IN_HORA_INICIAL;
		String fechaFinStr = sapAmisUtilsService.convertToStrDate(fechaFin) + PARAMETRO_IN_HORA_FINAL;
		SqlParameterSource in = new MapSqlParameterSource().addValue(PARAMETRO_IN_FECHA_INICIAL, fechaIniStr).addValue(PARAMETRO_IN_FECHA_FINAL, fechaFinStr);
		siniestrosPobladoCall.execute(in);
	}	

	@Resource(name = "jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
		jdbcTemplate.setResultsMapCaseInsensitive(true);
		this.emisionPobladoCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(CATALOG_NAME).withProcedureName("EMISIONPOBLADO").withSchemaName(ESQUEMA);
		this.siniestrosPobladoCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(CATALOG_NAME).withProcedureName("SINIESTROSPOBLADO").withSchemaName(ESQUEMA);
		this.rechazosPobladoCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(CATALOG_NAME).withProcedureName("RECHAZOSPOBLADO").withSchemaName(ESQUEMA);
		this.prevencionesPobladoCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(CATALOG_NAME).withProcedureName("PREVENCIONESPOBLADO").withSchemaName(ESQUEMA);
		this.pttPobladoCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(CATALOG_NAME).withProcedureName("PTTPOBLADO").withSchemaName(ESQUEMA);
		this.salvamentoPobladoCall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(CATALOG_NAME).withProcedureName("SALVAMENTOPOBLADO").withSchemaName(ESQUEMA);
		this.recuperacionPobladoREPUVECall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(CATALOG_NAME).withProcedureName("recuperacionREPUVEPoblado").withSchemaName(ESQUEMA);
		this.roboPobladoREPUVECall = new SimpleJdbcCall(jdbcTemplate).withCatalogName(CATALOG_NAME).withProcedureName("roboREPUVEPoblado").withSchemaName(ESQUEMA);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void roboPobladoREPUVE(Date fechaIni, Date fechaFin) {
		String fechaIniStr = sapAmisUtilsService.convertToStrDate(fechaIni) + PARAMETRO_IN_HORA_INICIAL;
		String fechaFinStr = sapAmisUtilsService.convertToStrDate(fechaFin) + PARAMETRO_IN_HORA_FINAL;
		SqlParameterSource in = new MapSqlParameterSource().addValue(PARAMETRO_IN_FECHA_INICIAL, fechaIniStr).addValue(PARAMETRO_IN_FECHA_FINAL, fechaFinStr);
		roboPobladoREPUVECall.execute(in);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void recuperacionPobladoREPUVE(Date fechaIni, Date fechaFin) {
		String fechaIniStr = sapAmisUtilsService.convertToStrDate(fechaIni) + PARAMETRO_IN_HORA_INICIAL;
		String fechaFinStr = sapAmisUtilsService.convertToStrDate(fechaFin) + PARAMETRO_IN_HORA_FINAL;
		SqlParameterSource in = new MapSqlParameterSource().addValue(PARAMETRO_IN_FECHA_INICIAL, fechaIniStr).addValue(PARAMETRO_IN_FECHA_FINAL, fechaFinStr);
		recuperacionPobladoREPUVECall.execute(in);
	}
}