/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoEntidadDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoEntidadDaoImpl implements CaTipoEntidadDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoEntidadDaoImpl.class);
	
	public void save(CaTipoEntidad entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }

    public void delete(CaTipoEntidad entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoEntidad.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }

    public CaTipoEntidad update(CaTipoEntidad entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoEntidad result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoEntidad 	::		CaTipoEntidadDaoImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaTipoEntidad findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoEntidadDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoEntidad instance = entityManager.find(CaTipoEntidad.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoEntidadDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoEntidadDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoEntidad> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoEntidadDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoEntidad model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoEntidadDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoEntidadDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoEntidad> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoEntidad> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoEntidad> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoEntidad> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoEntidad> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoEntidadDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoEntidad model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoEntidadDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoEntidadDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}