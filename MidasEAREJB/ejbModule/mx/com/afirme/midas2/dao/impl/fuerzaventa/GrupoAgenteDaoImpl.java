package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.dao.fuerzaventa.GrupoAgenteDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoAgente;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * 
 * @author Efren Rodriguez
 *
 */

@Stateless
public class GrupoAgenteDaoImpl extends EntidadDaoImpl implements GrupoAgenteDao{
	
	private Map<String, Object> params = new HashMap<String, Object>();
	
	private static final String AND = " and ";
	private static final Logger LOG = Logger.getLogger(GrupoAgenteDaoImpl.class); 
	
	public GrupoAgente loadById(GrupoAgente grupoAgenteDTO) {
		GrupoAgente filtro = new GrupoAgente();
		filtro.setId(grupoAgenteDTO.getId());
		List<GrupoAgente> list = findByFilters(filtro);
		if(list != null && !list.isEmpty()){
			grupoAgenteDTO = list.get(0);
		}
		return grupoAgenteDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<GrupoAgente> findByFilters(GrupoAgente filtro) {
		List<GrupoAgente> list;
		final StringBuilder queryString = new StringBuilder("");
		queryString.append("select model from grupoAgenteDTO model");
		
		if (filtro != null) {
			if (filtro.getId() != null) {
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			} else {
				if(filtro.getNombre() != null && !filtro.getNombre().isEmpty()) {
					addCondition(queryString, "UPPER(model.nombre) like UPPER(:nombre)");
	                params.put("nombre", "%"+filtro.getNombre()+"%");
				}
				complemento(filtro, queryString);
			}
		}
		String q=getQueryString(queryString);
		q+=" order by model.id ASC";
		Query query = entityManager.createQuery(q);
		if(!params.isEmpty()){
			setParametros(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		list = query.getResultList();
		return list;
	}
	
	public boolean deleteGrupo(GrupoAgente filtro) {
		filtro=entityManager.find(GrupoAgente.class,filtro.getId());
		try{
			entityManager.remove(filtro);
		}
		catch(Exception e){
			LOG.error(e.getMessage(), e);
			return false;
		}
		return true;
	}
	
	@Override
	public List<Object[]> obtenerAgentesAsociadosEnGrupo(GrupoAgente tipoGrupoAgenteDTO) {
		List<Object[]> listaGrupoAgenteDetalleDTO = new ArrayList<Object[]>();
		if(tipoGrupoAgenteDTO != null && tipoGrupoAgenteDTO.getId()!= null){			 
			listaGrupoAgenteDetalleDTO = filtrarAgentesDeGrupo(tipoGrupoAgenteDTO.getId(), null, null, null);			
		}
		return listaGrupoAgenteDetalleDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> filtrarAgentesDeGrupo(Long idGrupo, String clave, String nombre, String gerencia){
		String queryString = "select AGD.ID, A.IDAGENTE, P.NOMBRE as NOMBRECOMPLETO, G.IDGERENCIA "
				+ "from MIDAS.TOAGENTE_GRUPO AG "
				+ "INNER JOIN MIDAS.TOAGENTE_GRUPO_DETALLE AGD ON AG.ID = AGD.GRUPO_ID "
				+ "INNER JOIN MIDAS.TOAGENTE A ON A.ID = AGD.AGENTE_ID "
				+ "INNER JOIN SEYCOS.PERSONA P ON P.ID_PERSONA  = A.IDPERSONA "
				+ "INNER JOIN MIDAS.TOPROMOTORIA P ON A.IDPROMOTORIA = P.ID "
				+ "INNER JOIN MIDAS.TOEJECUTIVO EJ ON P.EJECUTIVO_ID = EJ.ID "
				+ "INNER JOIN MIDAS.TOGERENCIA G ON EJ.GERENCIA_ID = G.ID "
				+ "WHERE AG.ID = ?1";
		
		if((clave == null || clave.isEmpty()) && (nombre == null || nombre.isEmpty()) && (gerencia == null || gerencia.isEmpty())) {
			Query query = entityManager.createNativeQuery(queryString);
			query.setParameter(1, idGrupo);
		}
		
		queryString += obtenerQueryAnd(clave, nombre, gerencia);
		Query query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, idGrupo);
		
		if (clave != null && !clave.isEmpty()) {
			query.setParameter(2, clave);
		} else if (nombre != null && !nombre.isEmpty()) {
			query.setParameter(2, "%"+nombre +"%");
		} else if (gerencia != null && !gerencia.isEmpty()) {
			query.setParameter(2, gerencia);
		}
		return (List<Object[]>)query.getResultList();
	}
	
	public void guardarAgentesAsociadosEnGrupo(Long idGrupo, String idsAgentes) {
		String[] agentes = idsAgentes.split(",");
		
		String queryString = "DELETE FROM MIDAS.TOAGENTE_GRUPO_DETALLE WHERE GRUPO_ID = ?1";
		Query query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, idGrupo);
		query.executeUpdate();
		if (!idsAgentes.equals("")) {
			for (String agente : agentes) {
				queryString = "INSERT INTO MIDAS.TOAGENTE_GRUPO_DETALLE(ID, GRUPO_ID, AGENTE_ID) " +
								"SELECT MIDAS.TOAGENTE_GRUPO_DETALLE_SEQ.NEXTVAL, ?1, ID " +
								"FROM MIDAS.TOAGENTE " +
								"WHERE IDAGENTE = ?2";
				query = entityManager.createNativeQuery(queryString);
				query.setParameter(1, idGrupo);
				query.setParameter(2, agente);
				query.executeUpdate();
			}
		}
	
	}
	
	@SuppressWarnings("unchecked")
	public List<AgenteView> filtrarAgente(Agente filtroAgente) {
		
		Long clave = filtroAgente.getIdAgente();
		String nombre = filtroAgente.getPersona().getNombreCompleto();
		String gerencia = filtroAgente.getPromotoria().getEjecutivo().getGerencia().getIdGerencia().toString();
		List<AgenteView> lista = new ArrayList<AgenteView>();
		List<Object[]> tempList;
		
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT "); 
		queryString.append(" A.idAgente as idAgente, ");
		queryString.append(" PE.NOMBRE as nombreCompleto, ");
		queryString.append(" G.idGerencia as gerencia ");
		queryString.append(" from MIDAS.toAgente A ");
		queryString.append(" inner join SEYCOS.PERSONA PE on(PE.id_persona=A.idPersona) ");
		queryString.append(" inner join MIDAS.toPromotoria P on(P.id=A.idPromotoria) ");
		queryString.append(" inner join MIDAS.toEjecutivo EJ on(EJ.id=P.ejecutivo_id) ");
		queryString.append(" inner join MIDAS.toGerencia G on(G.id=EJ.gerencia_id) ");
		queryString.append(" where A.idAgente is not null and ");
		
		if (clave != null && !clave.toString().equals("0")) {
			addCondition(queryString, " A.IDAGENTE = ?1 ");
		} 
		if (nombre != null && !nombre.isEmpty()) {
			addCondition(queryString, " UPPER(PE.NOMBRE) like UPPER(?2) ");
		} 
		if (gerencia != null && !gerencia.isEmpty() && !gerencia.equals("0")) {
			addCondition(queryString, " G.IDGERENCIA = ?3 ");
		}
		
		String finalString = getQueryString(queryString);
		Query query = entityManager.createNativeQuery(finalString+" GROUP BY A.idAgente, PE.NOMBRE, G.idGerencia ORDER BY A.idAgente desc ");
		
		if (clave != null && !clave.toString().equals("0")) {
			query.setParameter(1, clave);
		}
		if (nombre != null && !nombre.isEmpty()) {
			query.setParameter(2, "%"+nombre +"%");
		} 
		if (gerencia != null && !gerencia.isEmpty() && !gerencia.equals("0")) {
			query.setParameter(3, gerencia);
		}
		
		tempList = (List<Object[]>) query.getResultList();
		
		for (Object[] Agetem: tempList){
			AgenteView agente=new AgenteView();
			agente.setIdAgente(Long.parseLong(Agetem[0].toString()));
			agente.setNombreCompleto(String.valueOf(Agetem[1]));
			agente.setGerencia(String.valueOf(Agetem[2]));
			lista.add(agente);
		}
		
		return lista;
	}

	/* --- Common methods --- */
	
	private void addCondition(StringBuilder queryString,String conditional){
		
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(AND);
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(AND)){
			query=query.substring(0,(query.length())-(AND).length());
		}
		return query;
	}
	
	private void setParametros(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	private String obtenerQueryAnd(String clave, String nombre, String gerencia) {
		String str = "";
		if (clave != null && !clave.isEmpty()) {
			str += " and A.IDAGENTE = ?2";
		} else if (nombre != null && !nombre.isEmpty()) {
			str += " and UPPER(P.NOMBRE) like UPPER(?2) ";
		} else if (gerencia != null && !gerencia.isEmpty()) {
			str += " and G.IDGERENCIA = ?2 ";
		}
		return str;
	}
	
	private Map<String, Object> complemento(GrupoAgente filtro,StringBuilder queryString){
		if(filtro.getDescripcion() != null && !filtro.getDescripcion().isEmpty()) {
			addCondition(queryString, "UPPER(model.descripcion) like UPPER(:descripcion)");
            params.put("descripcion", "%"+filtro.getDescripcion()+"%");
		}
		if(filtro.getEstatus() != null && filtro.getEstatus() != 0L) {
			addCondition(queryString, "model.estatus=:estatus");
            params.put("estatus", filtro.getEstatus());
		}
		if(isNotNull(filtro.getFechaCreacion())) {
			Date fechaCreacion = filtro.getFechaCreacion();
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			String fechaCreacionString = format.format(fechaCreacion);
			if(isNull(filtro.getFechaModificacion())) {
				addCondition(queryString,"func('TRUNC', model.fechaCreacion) >= func('TO_DATE', :fechaCreacion, 'DD/MM/YYYY')");
				params.put("fechaCreacion", fechaCreacionString);
			} else {
				Date fechaModificacion=filtro.getFechaModificacion();
				String fechaModificacionString = format.format(fechaModificacion);
				addCondition(queryString, "func('TRUNC', model.fechaCreacion) BETWEEN func('TO_DATE', :fechaCreacion, 'DD/MM/YYYY') AND func('TO_DATE', :fechaModificacion, 'DD/MM/YYYY')");
				params.put("fechaCreacion", fechaCreacionString);
				params.put("fechaModificacion", fechaModificacionString);
			}
		}
		if(isNotNull(filtro.getFechaModificacion()) && isNull(filtro.getFechaCreacion())) {
			Date fechaModificacion = filtro.getFechaModificacion();
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			String fechaModificacionString = format.format(fechaModificacion);
			addCondition(queryString,"func('TRUNC', model.fechaCreacion) <= func('TO_DATE', :fechaModificacion, 'DD/MM/YYYY')");
			params.put("fechaModificacion", fechaModificacionString);
		}
		
		return params;
	}
	
}
