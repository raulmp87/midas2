package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.PromotoriaJPADao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService.TipoContrato;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.MailServiceSupport;
@Stateless
public class PromotoriaJPADaoImpl extends EntidadHistoricoDaoImpl implements PromotoriaJPADao{
//	private DomicilioFacadeRemote domicilioFacade;
	
	@EJB
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private ImpresionesService impresionesService;
	@EJB
	private MailService mailService;
	@SuppressWarnings("unchecked")
	@Override
	public List<PromotoriaView> getList(boolean onlyActive){
		List<PromotoriaView> lista=new ArrayList<PromotoriaView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Promotoria filtro=new Promotoria();
		if(onlyActive){
			filtro.setClaveEstatus(1);//Mostrar unicamnete los activos.
		}
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT DISTINCT entidad.id as id, entidad.idPromotoria as idPromotoria, entidad.descripcion as descripcion ");
		queryString.append(" from MIDAS.toPromotoria entidad "); 
		queryString.append(" where ");
		if(isNotNull(filtro)){
			int index=1;
			if(filtro.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtro.getClaveEstatus());
				index++;
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.descripcion asc ";
			Query query=entityManager.createNativeQuery(finalQuery,PromotoriaView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			lista=query.getResultList();
		}
		return lista;
	}
	
	
	@Override
	public List<PromotoriaView> findByFiltersView(Promotoria filtroPromotoria){
		List<PromotoriaView> lista=new ArrayList<PromotoriaView>();
		//Si trae datos de fecha entonces busca por medio de sql, sino con jpql
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select distinct entidad.id as id, entidad.idPromotoria as idPromotoria, entidad.descripcion as descripcion, ");
		queryString.append(" entidad.idTipoPromotoria as idtipopromotoria,tipoProm.valor as nombreTipoPromotoria,entidad.claveestatus as claveestatus,entidad.idAgrupadorPromotoria,agrupador.valor as nombreAgrupador, ");
		queryString.append(" personaEj.nombre as nombreEjecutivo,entidad.agentePromotor_id,personaAg.nombre as nombreAgente,ejecutivo.id as ejecutivo_id ,act.fechaHoraActualizacion as fechaHoraActualizacion "); 
		queryString.append(" from MIDAS.toPromotoria entidad "); 
		queryString.append(" inner join MIDAS.toEjecutivo ejecutivo on(ejecutivo.id = entidad.ejecutivo_id) ");
		queryString.append(" left join SEYCOS.persona personaEj on(personaEj.id_persona=ejecutivo.idPersona) "); 
		queryString.append(" left join MIDAS.toAgente agente on(entidad.agentePromotor_id = agente.id) ");
		queryString.append(" left join SEYCOS.persona personaAg on(personaAg.id_persona=agente.idPersona) ");
		queryString.append(" left join MIDAS.TCGRUPOACTUALIZACIONAGENTES grupo on(grupo.id=40) ");
		queryString.append(" left join MIDAS.TLACTUALIZACIONAGENTES act on(act.grupoActualizacionAgentes_id=grupo.id and act.idRegistro=entidad.id and (act.tipoMovimiento like 'ALTA' )) ");
		queryString.append(" inner join MIDAS.TCGRUPOCATALOGOAGENTES catAgrupador on(catAgrupador.descripcion='Agrupadores de Promotorias') ");
		queryString.append(" inner join MIDAS.TOVALORCATALOGOAGENTES agrupador on(catAgrupador.id=agrupador.grupoCatalogoAgentes_id and agrupador.id=entidad.idAgrupadorPromotoria) ");
		queryString.append(" inner join MIDAS.TCGRUPOCATALOGOAGENTES catTipoProm on(catTipoProm.descripcion='Tipos de Promotoria') ");
		queryString.append(" inner join MIDAS.TOVALORCATALOGOAGENTES tipoProm on(catTipoProm.id=tipoProm.grupoCatalogoAgentes_id and tipoProm.id=entidad.idTipoPromotoria) ");
		queryString.append(" where ");
		if(isNotNull(filtroPromotoria)){
			int index=1;
			if(isNotNull(filtroPromotoria.getId())){
				addCondition(queryString, " entidad.id=? ");
				params.put(index, filtroPromotoria.getId());
				index++;
			}else if(isNotNull(filtroPromotoria.getIdPromotoria())){
				addCondition(queryString, " entidad.idPromotoria=? ");
				params.put(index, filtroPromotoria.getIdPromotoria());
				index++;
			}else{
				if(isNotNull(filtroPromotoria.getClaveEstatus())){
					addCondition(queryString, " entidad.claveEstatus=? ");
					params.put(index, filtroPromotoria.getClaveEstatus());
					index++;
				}
				ValorCatalogoAgentes agrupador=filtroPromotoria.getAgrupadorPromotoria();
				if(agrupador!=null && agrupador.getId()!=null){
					addCondition(queryString, " entidad.idAgrupadorPromotoria=?");
					params.put(index, agrupador.getId());
					index++;
				}
				ValorCatalogoAgentes tipoPromotoria=filtroPromotoria.getTipoPromotoria();
				if(tipoPromotoria!=null && tipoPromotoria.getId()!=null){
					addCondition(queryString, " entidad.idTipoPromotoria=?");
					params.put(index, tipoPromotoria.getId());
					index++;
				}
				if(filtroPromotoria.getDescripcion()!=null && !filtroPromotoria.getDescripcion().isEmpty()){
					addCondition(queryString, "UPPER(entidad.descripcion) like UPPER(?)");
					params.put(index, "%"+filtroPromotoria.getDescripcion()+"%");
					index++;
				}
				
				Ejecutivo ejecutivo=filtroPromotoria.getEjecutivo();
				if(ejecutivo!=null){
					if(ejecutivo.getId()!=null){
						addCondition(queryString, "entidad.ejecutivo_id=?");
						params.put(index, ejecutivo.getId());
						index++;
					}
				}
				
				if(isNotNull(filtroPromotoria.getFechaInicio())){
					Date fecha=filtroPromotoria.getFechaInicio();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");						
					params.put(index,  fechaString);
					index++;
				}
				if(isNotNull(filtroPromotoria.getFechaFin())){
					Date fecha=filtroPromotoria.getFechaFin();
					SimpleDateFormat format=new SimpleDateFormat("dd/MM/yyyy");
					String fechaString=format.format(fecha);
					addCondition(queryString, " TRUNC(act.fechaHoraActualizacion) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
					params.put(index,  fechaString);
					index++;
				}
			}
			if(params.isEmpty()){
				int lengthWhere="where ".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,"promotoriaView");
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			//query.setMaxResults(100);
			lista=query.getResultList();
		}
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Promotoria> findByFilters(Promotoria filtroPromotoria, String fechaHistorico) {
		
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;
		
		List<Promotoria> lista=new ArrayList<Promotoria>();
		final StringBuilder queryString=new StringBuilder("");
//		queryString.append("select model from Promotoria model left join fetch model.domicilio left join fetch model.ejecutivo left join fetch model.agentePromotor left join fetch model.personaResponsable ");
//		queryString.append("select model from Promotoria model left join fetch model.ejecutivo left join fetch model.agentePromotor left join fetch model.personaResponsable ");
		queryString.append("select model from Promotoria model left join fetch model.ejecutivo left join fetch model.personaResponsable ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtroPromotoria!=null){
			if(filtroPromotoria.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtroPromotoria.getId());
			}else{
				ValorCatalogoAgentes agrupador=filtroPromotoria.getAgrupadorPromotoria();
				if(agrupador!=null && agrupador.getId()!=null){
					addCondition(queryString, "model.agrupadorPromotoria.id=:idAgrupador");
					params.put("idAgrupador", agrupador.getId());
				}
				ValorCatalogoAgentes tipoPromotoria=filtroPromotoria.getTipoPromotoria();
				if(tipoPromotoria!=null && tipoPromotoria.getId()!=null){
					addCondition(queryString, "model.tipoPromotoria.id=:idTipoPromotoria");
					params.put("idTipoPromotoria", tipoPromotoria.getId());
				}
				if(filtroPromotoria.getDescripcion()!=null && !filtroPromotoria.getDescripcion().isEmpty()){
					addCondition(queryString, "UPPER(model.descripcion) like UPPER(:descripcion)");
					params.put("descripcion", "%"+filtroPromotoria.getDescripcion()+"%");
				}
				if(isNotNull(filtroPromotoria.getClaveEstatus())){
					addCondition(queryString, "model.claveEstatus=:claveEstatus");
					params.put("claveEstatus", filtroPromotoria.getClaveEstatus());
				}
//				Domicilio domicilio=filtroPromotoria.getDomicilio();
//				if(domicilio!=null){
//					if(domicilio.getClaveEstado()!=null && !domicilio.getClaveEstado().isEmpty()){
//						addCondition(queryString, "model.domicilio.claveEstado=:claveEstado");
//						params.put("claveEstado", domicilio.getClaveEstado());
//					}
//					if(domicilio.getClaveCiudad()!=null && !domicilio.getClaveCiudad().isEmpty()){
//						addCondition(queryString, "model.domicilio.claveCiudad=:claveCiudad");
//						params.put("claveCiudad", domicilio.getClaveCiudad());
//					}
//					if(domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().isEmpty()){
//						addCondition(queryString, "UPPER(model.domicilio.nombreColonia) like UPPER(:nombreColonia)");
//						params.put("nombreColonia", "%"+domicilio.getNombreColonia()+"%");
//					}
//					if(domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().isEmpty()){
//						addCondition(queryString, "model.domicilio.codigoPostal like :codigoPostal");
//						params.put("codigoPostal", domicilio.getCodigoPostal()+"%");
//					}
//				}
				Ejecutivo ejecutivo=filtroPromotoria.getEjecutivo();
				if(ejecutivo!=null){
					if(ejecutivo.getId()!=null){
						addCondition(queryString, "model.ejecutivo.id=:idEjecutivo");
						params.put("idEjecutivo", ejecutivo.getId());
					}
				}
				Agente agentePromotor=filtroPromotoria.getAgentePromotor();
				if(agentePromotor!=null && agentePromotor.getId()!=null){
					addCondition(queryString, "model.agentePromotor.id=:idAgentePromotor");
					params.put("idAgentePromotor", agentePromotor.getId());
				}
			}
		}
		
		if(!isHistorico)
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista=query.getResultList();			
		}
		else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
			
			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString).toString());
			historicalQuery.addArgument("id");
			historicalQuery.addArgumentValue(filtroPromotoria.getId());
			historicalQuery.refreshIdentityMapResult();
			lista = (List<Promotoria>)historicalSession.executeQuery(historicalQuery);
		}
		
		
		return lista;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * Se cambian los filtros por ejecutivo
	 */
	@Override
	public List<Promotoria> findByEjecutivo(Long idEjecutivo) {
		if(idEjecutivo!=null){
			Ejecutivo ejecutivo=new Ejecutivo();
			ejecutivo.setId(idEjecutivo);
			Promotoria filtroPromotoria=new Promotoria();
			filtroPromotoria.setEjecutivo(ejecutivo);
			filtroPromotoria.setClaveEstatus(1);//Solo activos
			return findByFilters(filtroPromotoria, null);
		}
		return new ArrayList<Promotoria>();
	}
	/**
	 * 
	 * @param idParent
	 * @return
	 */
	public List<PromotoriaView> findByEjecutivoLightWeight(Long idParent){
		List<PromotoriaView> list=new ArrayList<PromotoriaView>();
		if(isNotNull(idParent)){
			Ejecutivo parent=new Ejecutivo();
			parent.setId(idParent);
			Promotoria filtro=new Promotoria();
			filtro.setClaveEstatus(1);//Solo activos
			filtro.setEjecutivo(parent);
			return findByFiltersView(filtro);
		}
		return list;
	}
	
	@Override
	public Promotoria loadById(Promotoria entidad, String fechaHistorico){
		if(entidad!=null && entidad.getId()!=null){
			Promotoria filtro=new Promotoria();
			filtro.setId(entidad.getId());
			List<Promotoria> list=findByFilters(filtro, fechaHistorico);
			if(list!=null && !list.isEmpty()){
				entidad=list.get(0);				
				entidad = getAgtPromByIdProm(entidad);
			}
			if(isNotNull(entidad)){
//				Ejecutivo ejecutivo=entidad.getEjecutivo();
//				if(isNotNull(ejecutivo) && isNotNull(ejecutivo.getId())){
//					ejecutivo=ejecutivoService.loadById(ejecutivo);
//				}
//				entidad.setEjecutivo(ejecutivo);
				/*
				Long idPersona=(entidad.getPersonaResponsable()!=null)?entidad.getPersonaResponsable().getIdPersona():null;
				if(isNotNull(idPersona)){
					Persona persona=null;
					try {
						persona = entidad.getPersonaResponsable();
						List<Domicilio> listDomicilio = persona.getDomicilios();
						Domicilio domicilioPersActual = null;
						Domicilio domicilioFiscActual = null;
						if(listDomicilio != null && !listDomicilio.isEmpty()){
							for(Domicilio domicilio:listDomicilio){
								if(domicilio.getTipoDomicilio()!=null && "PERS".equals(domicilio.getTipoDomicilio().trim())){
									if(domicilioPersActual==null){
										domicilioPersActual=domicilio;
									}
									else if(domicilioPersActual.getIdDomicilio()<domicilio.getIdDomicilio()){
										domicilioPersActual=domicilio;
									}
								}
								else if(domicilio.getTipoDomicilio()!=null && "FISC".equals((domicilio.getTipoDomicilio()!=null)?domicilio.getTipoDomicilio().trim():"")){
									if(domicilioFiscActual==null){
										domicilioFiscActual=domicilio;
									}
									else if(domicilioFiscActual.getIdDomicilio()<domicilio.getIdDomicilio()){
										domicilioFiscActual=domicilio;
									}
								}				
							}
							//listDomicilio.clear();
							domicilioFiscActual=(isNotNull(domicilioFiscActual))?domicilioFiscActual:domicilioPersActual;
							//listDomicilio.add(domicilioFiscActual);
							listDomicilio.set(0,domicilioFiscActual);
						}
						//entidad.getPersonaResponsable().setDomicilios(listDomicilio);
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
				*/
			}
		}
		return entidad;
	}
	
	
	@Override
	public List<PromotoriaView> findPromotoriaConEjecutivosExcluyentes(List<Long> promotorias,List<Long> ejecutivosExcluyentes,List<Long> gerenciasExcluyentes,List<Long> centrosExcluyentes){
		List<PromotoriaView> list=new ArrayList<PromotoriaView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Promotoria filtro=new Promotoria();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT DISTINCT entidad.id as id, entidad.idPromotoria as idPromotoria, entidad.descripcion as descripcion ");
		queryString.append(" from MIDAS.toPromotoria entidad "); 
		queryString.append(" inner join MIDAS.toEjecutivo ejecutivo on(ejecutivo.id = entidad.ejecutivo_id) ");
		queryString.append(" INNER JOIN MIDAS.toGerencia gerencia on ejecutivo.gerencia_id = gerencia.id ");
		queryString.append(" INNER JOIN MIDAS.toCentroOperacion centro on gerencia.centroOperacion_id = centro.id ");
		queryString.append(" where ");
		if(isNotNull(filtro)){
			int index=1;
			if(filtro.getClaveEstatus()!=null){
				addCondition(queryString, " entidad.claveEstatus=? ");
				params.put(index, filtro.getClaveEstatus());
				index++;
			}
			if(!isEmptyList(promotorias)){
				queryString.append(" entidad.id in ( ");
				StringBuilder conditionalList=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idPromotoria:promotorias){
					if(isNotNull(idPromotoria) && isNotNull(idPromotoria)){
						if(i<(promotorias.size()-1)){
							conditionalList.append(idPromotoria+",");
						}else{
							conditionalList.append(idPromotoria);
						}
					}
					i++;
				}
//				params.put(index, conditionalList.toString());
//				index++;
				queryString.append(conditionalList.toString()+") or ");
			}
			if(!isEmptyList(ejecutivosExcluyentes)){
				queryString.append(" ejecutivo.id  in ( ");
				StringBuilder listaExcluyente=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idEjecutivo:ejecutivosExcluyentes){
					if(isNotNull(idEjecutivo) && isNotNull(idEjecutivo)){
						if(i<(ejecutivosExcluyentes.size()-1)){
							listaExcluyente.append(idEjecutivo+",");
						}else{
							listaExcluyente.append(idEjecutivo);
						}
					}
					i++;
				}
//				params.put(index, listaExcluyente.toString());
//				index++;
				queryString.append(listaExcluyente.toString()+") or ");
			}
			if(!isEmptyList(gerenciasExcluyentes)){
				queryString.append(" gerencia.id  in ( ");
				StringBuilder listaExcluyente=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idGerencia:gerenciasExcluyentes){
					if(isNotNull(idGerencia) && isNotNull(idGerencia)){
						if(i<(gerenciasExcluyentes.size()-1)){
							listaExcluyente.append(idGerencia+",");
						}else{
							listaExcluyente.append(idGerencia);
						}
					}
					i++;
				}
//				params.put(index, listaExcluyente.toString());
//				index++;
				queryString.append(listaExcluyente.toString()+") or ");
			}
			if(!isEmptyList(centrosExcluyentes)){
				queryString.append(" centro.id  in ( ");
				StringBuilder listaExcluyente=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(Long idCentro:centrosExcluyentes){
					if(isNotNull(idCentro) && isNotNull(idCentro)){
						if(i<(centrosExcluyentes.size()-1)){
							listaExcluyente.append(idCentro+",");
						}else{
							listaExcluyente.append(idCentro);
						}
					}
					i++;
				}
//				params.put(index, listaExcluyente.toString());
//				index++;
				queryString.append(listaExcluyente.toString()+") or ");
			}
			String finalQuery=getQueryString(queryString)+" order by entidad.descripcion asc ";
			Query query=entityManager.createNativeQuery(finalQuery,PromotoriaView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
		}
		return list;
	}
	
	
	/**
	 * Devuelve el número de agentes asignados en una promotoria
	 * 
	 * @param promotoria La promotoria a evaluar
	 * @return El número de agentes asignados en una promotoria
	 */
	@Override
	public int agentesAsignadosEnPromotoria(Promotoria promotoria) {
				
		TypedQuery<Long> queryCount = entityManager.createQuery("SELECT COUNT(agente) FROM Agente agente WHERE agente.promotoria.id = :promotoriaId", Long.class);
		
		queryCount.setParameter("promotoriaId", promotoria.getId());
				
		return queryCount.getSingleResult().intValue();
				
	}
	
		
	private Promotoria getAgtPromByIdProm(Promotoria obj){
		Agente agtPromotor = new Agente();
		List<AgenteView> lista = new ArrayList<AgenteView>();
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select id as id, idAgente as idAgente, nombrecompleto as nombreCompleto ");
		queryString.append(" from MIDAS.toAgente a ");
		queryString.append(" inner join MIDAS.vw_persona p on (p.idpersona = a.IDPERSONA) ");
		queryString.append(" where idAgente = (select agentepromotor_id from MIDAS.toPromotoria pro where pro.id = "+obj.getId()+" )");
		if(obj!=null && obj.getId()!=null){
			lista = this.entityManager.createNativeQuery(queryString.toString(), AgenteView.class).getResultList();
		}
		if(!lista.isEmpty()){
			if(lista.get(0)!=null){
				System.out.println("id= "+lista.get(0).getId()+" IdAgente = "+lista.get(0).getIdAgente()+" NombreCompleto ="+lista.get(0).getNombreCompleto());
				if(lista.get(0).getId()!=null){
					agtPromotor = this.entidadService.findById(Agente.class, lista.get(0).getId());
					obj.setAgentePromotor(agtPromotor);
				}
			}
		}else{
			obj.setAgentePromotor(null);
		}
		return obj;
	}
	
	/**
	 * Envia correo de contratro de Promotoria con los datos del agente promotor
	 * 
	 * NOTA: Este método sólo permanecerá como referencia en caso de que sea requerido en el nuevo proyecto de Promotorias, en caso contrario se deberá eliminar 
	 * 
	 * @param agentePromotor Agente Promotor
	 */
	@SuppressWarnings("unused")
	private void enviaCorreoContratoPromotoria(Agente agentePromotor) {
				
		List<String> address =  MailServiceSupport.getCorreoAgente(agentePromotor.getPersona());
		
		TransporteImpresionDTO contrato =  impresionesService.imprimirContratoAgente(agentePromotor, TipoContrato.PROMOTORIA, new Locale("es", "MX"));
		
		List<ByteArrayAttachment> attachment = null;
		
		if (contrato != null && contrato.getByteArray() != null) {
			
			attachment =  new ArrayList<ByteArrayAttachment>();
			
			ByteArrayAttachment file = new ByteArrayAttachment();
			
			file.setContenidoArchivo(contrato.getByteArray());
			file.setNombreArchivo("contratoPromotoria.pdf");
			file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
			
			attachment.add(file);
			
		} 
		
		if (address != null && !address.isEmpty()) {
			
			mailService.sendMailAgenteNotificacion(address, null, null,
					"Notificacion", null , attachment,
					"Notificacion", "", GenericMailService.T_GENERAL);
			
		}
				
	}
	
	
}
