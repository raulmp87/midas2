package mx.com.afirme.midas2.dao.impl.tarifa;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaAutoProliberDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoProliber;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoProliberId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;

@Stateless
public class TarifaAutoProliberDaoImpl extends JpaDao<TarifaAutoProliberId, TarifaAutoProliber> implements TarifaAutoProliberDao {

	@Override
	public List<TarifaAutoProliber> findByFilters(
			TarifaAutoProliber tarifaAutoProliber) {
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaAutoProliber> findByTarifaVersionId(
			TarifaVersionId tarifaVersionId) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT tarifa FROM TarifaAutoProliber tarifa")
		   .append(" WHERE tarifa.id.idMoneda = :idMoneda")
		   .append(" AND tarifa.id.idRiesgo = :idRiesgo")
		   .append(" AND tarifa.id.idConcepto = :idConcepto")
		   .append(" AND tarifa.id.version = :version");
		
		Query query =(TypedQuery<TarifaAutoProliber>)entityManager.createQuery(sql.toString());
		query.setParameter("idMoneda", tarifaVersionId.getIdMoneda());
		query.setParameter("idRiesgo", tarifaVersionId.getIdRiesgo());
		query.setParameter("idConcepto", tarifaVersionId.getIdConcepto());
		query.setParameter("version", tarifaVersionId.getVersion());
		
		return query.getResultList();
	}

}
