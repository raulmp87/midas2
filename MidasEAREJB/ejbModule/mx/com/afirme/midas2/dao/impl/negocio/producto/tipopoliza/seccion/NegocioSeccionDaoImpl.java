package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza.seccion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioSeccionDaoImpl extends JpaDao<BigDecimal, NegocioSeccion> implements NegocioSeccionDao {

	@SuppressWarnings("unchecked")
	public List<NegocioSeccion> listarNegSeccionPorIdNegTipoPoliza(BigDecimal idToNegTipoPoliza){
		try {
			String queryString = "select model from NegocioSeccion model where " + 
				" model.negocioTipoPoliza.idToNegTipoPoliza = :idToNegTipoPoliza order by model.idToNegSeccion";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegTipoPoliza", idToNegTipoPoliza);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	
	/**
	 * Metodo que permite obtener un listado de coberturas
	 * de acuerdo a las relacion Negocio-Cobertura-Paquete-Seccion
	 * que no se encuentran asociadas al Negocio-Seccion, pero
	 * estan disponibles en la entidad CobPaquetesSeccion
	 * @param negocioSeccion
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<CobPaquetesSeccion> listarCoberturasPosible(
			NegocioPaqueteSeccion negocioPaqueteSeccion) {
		if(negocioPaqueteSeccion.getNegocioSeccion()!= null){
			String queryString = "select model from CobPaquetesSeccion model where "
				+ " model.id.idToSeccion = :idToSeccion "
				+ " and model.id.idPaquete = :idPaquete ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToSeccion", negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion());
			query.setParameter("idPaquete", negocioPaqueteSeccion.getPaquete().getId());
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();			
		}
		return null;
	}


	@Override
	public List<NegocioSeccion> listarNegSeccionPorCotizacion(
			BigDecimal idToCotizacion) {
		try {
			StringBuffer queryString = new StringBuffer(
					"SELECT model FROM NegocioSeccion model WHERE "
							+ "model.idToNegSeccion IN (SELECT DISTINCT inciso.incisoAutoCot.negocioSeccionId FROM IncisoCotizacionDTO inciso");
			if(idToCotizacion != null){
				queryString.append(" WHERE inciso.id.idToCotizacion = :idToCotizacion)");
			}else{
				queryString.append(" )");
			}
			queryString.append(" ORDER BY model.idToNegSeccion");
			Query query = entityManager.createQuery(queryString.toString());
			if(idToCotizacion != null){
				query.setParameter("idToCotizacion", idToCotizacion);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@Override
	public List<SeccionDTO> listarSeccionNegocioByClaveNegocio(String claveNegocio){
		List<SeccionDTO>  list = new ArrayList<SeccionDTO>(1);
		try {
			StringBuffer queryString = new StringBuffer(
					"SELECT DISTINCT model.seccionDTO FROM NegocioSeccion model " +
					"WHERE model.negocioTipoPoliza.negocioProducto.negocio.claveEstatus = 1 " +
					"AND model.negocioTipoPoliza.negocioProducto.negocio.claveNegocio = :claveNegocio " +
					"");

			queryString.append(" ORDER BY model.seccionDTO.descripcion");
			TypedQuery<SeccionDTO> query = entityManager.createQuery(queryString.toString(), SeccionDTO.class);
			query.setParameter("claveNegocio", claveNegocio);

			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			
			list =  query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}		
		return list;
	}
	
	@Override
	public NegocioSeccion getPorIdNegTipoPolizaIdToSeccion(BigDecimal idToNegTipoPoliza, BigDecimal idToSeccion) {
		LogDeMidasEJB3.log("finding NegocioSeccion instances by idToNegTipoPoliza idToSeccion" + idToNegTipoPoliza, Level.INFO, null);
		try {
			String queryString = "select model from NegocioSeccion model where " + 
				" model.negocioTipoPoliza.idToNegTipoPoliza = :idToNegTipoPoliza " +
				" and model.seccionDTO.idToSeccion = :idToSeccion ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegTipoPoliza", idToNegTipoPoliza);
			query.setParameter("idToSeccion", idToSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return (NegocioSeccion) query.getSingleResult();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@Override
	public List<NegocioSeccion> listarNegocioSeccionPorProductoNegocioTipoPoliza(
			ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza) {
		final String jpql = "select model from NegocioSeccion model where model.negocioTipoPoliza.negocioProducto.productoDTO = :producto " +
				"and model.negocioTipoPoliza.negocioProducto.negocio = :negocio " +
				"and model.negocioTipoPoliza.tipoPolizaDTO = :tipoPoliza";
		TypedQuery<NegocioSeccion> query = entityManager.createQuery(jpql, NegocioSeccion.class);
		query.setParameter("producto", producto);
		query.setParameter("negocio", negocio);
		query.setParameter("tipoPoliza", tipoPoliza);
		return query.getResultList();
	}
	
	@Override
	public List<NegocioSeccion> listarNegocioSeccionPorIdProductoNegocioTipoPoliza(
			BigDecimal idToProducto, Long idToNegocio, BigDecimal idToTipoPoliza) {
		final String jpql = "select model from NegocioSeccion model where model.negocioTipoPoliza.negocioProducto.idToNegProducto = :idToProducto " +
				"and model.negocioTipoPoliza.negocioProducto.negocio.idToNegocio = :idToNegocio " +
				"and model.negocioTipoPoliza.idToNegTipoPoliza = :idToTipoPoliza" +
				" order by model.seccionDTO.descripcion ASC";
		TypedQuery<NegocioSeccion> query = entityManager.createQuery(jpql, NegocioSeccion.class);
		query.setParameter("idToProducto", (idToProducto != null ? idToProducto.longValue() : 0));
		query.setParameter("idToNegocio", idToNegocio);
		query.setParameter("idToTipoPoliza", idToTipoPoliza);
		return query.getResultList();
	}
	
	@Override
	public NegocioSeccion getPorProductoNegocioTipoPolizaSeccionDescripcion(
			ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza, String seccionDescripcion) {
		final String jpql = "select model from NegocioSeccion model where model.negocioTipoPoliza.negocioProducto.productoDTO = :producto " +
				"and model.negocioTipoPoliza.negocioProducto.negocio = :negocio " +
				"and model.negocioTipoPoliza.tipoPolizaDTO = :tipoPoliza " +
				"and model.seccionDTO.descripcion = :seccionDescripcion";
		TypedQuery<NegocioSeccion> query = entityManager.createQuery(jpql, NegocioSeccion.class);
		query.setParameter("producto", producto);
		query.setParameter("negocio", negocio);
		query.setParameter("tipoPoliza", tipoPoliza);
		query.setParameter("seccionDescripcion", seccionDescripcion);
		List<NegocioSeccion> resultList = query.getResultList();
		if (resultList.size() == 1) {
			return resultList.get(0);
		}
		return null;
	}


	@Override
	public List<SeccionDTO> listarSeccionNegocioByIdNegocio(BigDecimal idToNegocio) {		
		StringBuilder queryString = new StringBuilder("SELECT DISTINCT model.seccionDTO FROM NegocioSeccion model ");
				queryString.append("WHERE model.negocioTipoPoliza.negocioProducto.negocio.idToNegocio = :idToNegocio");
		TypedQuery<SeccionDTO> query = entityManager.createQuery(queryString.toString(), SeccionDTO.class);
		query.setParameter("idToNegocio", idToNegocio);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE); 
		//query.setHint(QueryHints.REFRESH, HintValues.TRUE); 			
		return query.getResultList();
	}
	
	@Override
	public NegocioSeccion getPorProductoNegocioTipoPolizaSeccionDescripcion(
			ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza, SeccionDTO seccion) {
		final String jpql = "select model from NegocioSeccion model where model.negocioTipoPoliza.negocioProducto.productoDTO = :producto " +
				"and model.negocioTipoPoliza.negocioProducto.negocio = :negocio " +
				"and model.negocioTipoPoliza.tipoPolizaDTO = :tipoPoliza " +
				"and model.seccionDTO = :seccion";
		TypedQuery<NegocioSeccion> query = entityManager.createQuery(jpql, NegocioSeccion.class);
		query.setParameter("producto", producto);
		query.setParameter("negocio", negocio);
		query.setParameter("tipoPoliza", tipoPoliza);
		query.setParameter("seccion", seccion);
		List<NegocioSeccion> resultList = query.getResultList();
		if (resultList.size() == 1) {
			return resultList.get(0);
		}
		return null;
	}
	
	@Override
	public List<NegocioSeccion> listarNegSeccionByIdNegocio(Long idToNegocio) {		
		StringBuilder queryString = new StringBuilder("SELECT DISTINCT model FROM NegocioSeccion model ");
				queryString.append("WHERE model.negocioTipoPoliza.negocioProducto.negocio.idToNegocio = :idToNegocio ORDER BY model.seccionDTO.descripcion");
		TypedQuery<NegocioSeccion> query = entityManager.createQuery(queryString.toString(), NegocioSeccion.class);
		query.setParameter("idToNegocio", idToNegocio);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 			
		return query.getResultList();
	}
	
}
