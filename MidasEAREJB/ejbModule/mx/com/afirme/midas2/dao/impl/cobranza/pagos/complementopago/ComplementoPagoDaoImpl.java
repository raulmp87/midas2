package mx.com.afirme.midas2.dao.impl.cobranza.pagos.complementopago;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.cobranza.pagos.complementopago.ComplementoPagoDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.cobranza.pagos.complementopago.ComplementoPago;
import mx.com.afirme.midas2.domain.cobranza.pagos.complementopago.IngresosAplicar;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;

@Stateless
public class ComplementoPagoDaoImpl extends EntidadDaoImpl implements ComplementoPagoDao {

	private static final Logger log = Logger.getLogger(ComplementoPagoDaoImpl.class);
	private static final String COMPLEMENTO_PAGO_PROCEDURE_NAME = "SEYCOS.PKG_CFDI_RECEPCION_PAGO.SP_QRY_EXTRACCION_PAGOS";
	private static final String ERRORES_COMPLEMENTO_PAGO_PROCEDURE_NAME = "SEYCOS.PKG_CFDI_RECEPCION_PAGO.SP_QRY_MONITOR_COMP_PAGO";
	private static final String CONSULTA_INGRESOS_APLICAR = "SEYCOS.PKG_CFDI_RECEPCION_PAGO.SP_CONSULTA_ING_X_APLICAR";

	@Autowired
	private JdbcOperations jdbcTemplate;
	private String interfazDataSource= "jdbc/InterfazDataSource";
	private String complementoId="id";

	@SuppressWarnings("unchecked")
	@Override
	public List<ComplementoPago> obtenerComplementosPago(ComplementoPago complementopago){
		log.trace(">> obtenerComplementosPago()");
		List<ComplementoPago> resultList = new ArrayList<ComplementoPago>();
		StoredProcedureHelper storedHelper = null;
		try{
			storedHelper = new StoredProcedureHelper("SEYCOS.PKG_CFDI_RECEPCION_PAGO.FN_CFDI_BUSCAR",interfazDataSource);

			storedHelper.estableceParametro("noRecibo", Integer.valueOf(complementopago.getNoRecibo()));
			storedHelper.estableceParametro("numPoliza", String.valueOf(complementopago.getNumPoliza()));
			storedHelper.estableceParametro("incisoPoliza", String.valueOf(complementopago.getIncisoPoliza()));
			storedHelper.estableceParametro("claveAgente", Integer.valueOf(complementopago.getClaveAgente()));
			storedHelper.estableceParametro("nomCliente", String.valueOf(complementopago.getNomCliente()));
			storedHelper.estableceParametro("clienteRfc", String.valueOf(complementopago.getClienteRfc()));
			storedHelper.estableceParametro("fechaPagostr", String.valueOf(complementopago.getFechaPagostr()));

			String[] propiedades = new String[] {complementoId,
					                             "noRecibo",
					                             "numPoliza",
					                             "incisoPoliza",
					                             "claveAgente",
					                             "nomCliente",
					                             "clienteRfc",
					                             "impSaldoAnt",
					                             "impPagado",
					                             "impSaldoInsoluto",
					                             "numParcialidad"};

			String[] columnas = new String[] {"ID",
					                          "NO_RECIBO",
					                          "NUM_POLIZA",
					                          "INCISO_POLIZA",
					                          "CLAVE_AGENTE",
					                          "NOM_CLIENTE",
					                          "CLIENTE_RFC",
					                          "IMPSALDOANT",
					                          "IMPPAGADO",
					                          "IMPSALDOINSOLUTO",
					                          "NUMPARCIALIDAD"};

			storedHelper.estableceMapeoResultados(ComplementoPago.class.getCanonicalName(), propiedades, columnas);
			resultList = storedHelper.obtieneListaResultados();

		}catch(Exception e){
			log.error("Error: " + e);
			Integer codErr = null;
			String descErr = null;
			log.error("SEYCOS.PKG_CFDI_RECEPCION_PAGO.FN_CFDI_BUSCAR");
			if(storedHelper != null){
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			log.info("metodo obtenerComplementosPago numero de excepcion: " + codErr);
			log.info("metodo obtenerComplementosPago Descripcion de Excepcion: " + descErr);
		}

		if(!resultList.isEmpty()){
			log.debug("Cantidad de complementos encontrados {" + resultList.size() + "}");

			for(int i=0;i<resultList.size();i++){
				ComplementoPago complementoFor = resultList.get(i);
				log.debug("Consolidado = {" + complementoFor.toString()+ "}");
			}
		}

		log.trace("<< obtenerComplementosPago()");
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ComplementoPago> findByFilters(ComplementoPago complementoPago) throws Exception{
		log.trace(">> findByFilters()");
		System.err.println("Entre a dao implement");

		List<ComplementoPago> resultList = new ArrayList<ComplementoPago>();
		StoredProcedureHelper helper = null;

		String [] atributosDTO = {
				"claveAgente",
				"numPoliza",
				"nomCliente",
				"noRecibo",
				"numExhibicion",
				"folioFiscal"};

		String [] columnasResultSet = {
				"agente",
				"poliza",
				"cliente",
				"norecibo",
				"NUM_EXHIBICION",
				"CLAVE_COMPROBANTE"};

		if(complementoPago.getFechaPago()==null){
			complementoPago.setFechaPagostr("");
		}

		try{
			log.trace("Entre al try catch del procedure");
			helper = new StoredProcedureHelper(COMPLEMENTO_PAGO_PROCEDURE_NAME,interfazDataSource);
			helper.estableceParametro("P_CentroEmision", String.valueOf(complementoPago.getEmisionPoliza()));
			helper.estableceParametro("P_NumPoliza", String.valueOf(complementoPago.getNumPoliza()));
			helper.estableceParametro("P_NumRenovPoliz", String.valueOf(complementoPago.getNumRenPoliza()));
			if(complementoPago.getClaveAgente()==null){
				helper.estableceParametro("P_CveAgente","");
			}else{
				helper.estableceParametro("P_CveAgente", String.valueOf(complementoPago.getClaveAgente()));
			}
			helper.estableceParametro("P_RfcCliente", String.valueOf(complementoPago.getClienteRfc()));
			helper.estableceParametro("P_FechaPago",complementoPago.getFechaPagostr());
			helper.estableceParametro("P_NombreCliente", complementoPago.getNomCliente());
			helper.estableceParametro("P_FechaExpedicionInicial", complementoPago.getFechaExpedicionInicialStr());
			helper.estableceParametro("P_FechaExpedicionFinal", complementoPago.getFechaExpedicionFinalStr());
			log.trace("Asigne los parametros al helper correctamente");

			helper.estableceMapeoResultados(
					ComplementoPago.class.getCanonicalName(),
					atributosDTO,
					columnasResultSet);

			log.trace("establecer mapeo de resultados en el helper");

			resultList = helper.obtieneListaResultados();
		}catch(Exception e){
			log.error("Error: " + e);
			Integer codErr = null;
			String descErr = null;
			log.error(COMPLEMENTO_PAGO_PROCEDURE_NAME);
			if(helper != null){
				codErr = helper.getCodigoRespuesta();
				descErr = helper.getDescripcionRespuesta();
			}
			log.info("metodo obtenerComplementosPago numero de excepcion: " + codErr);
			log.info("metodo obtenerComplementosPago Descripcion de Excepcion: " + descErr);
		}

		if(!resultList.isEmpty()){
			log.debug("Cantidad de complementos encontrados {" + resultList.size() + "}");

			for(int i=0;i<resultList.size();i++){
				ComplementoPago complementoFor = resultList.get(i);
				log.debug("Consolidado = {" + complementoFor.toString()+ "}");
			}
		}

		log.trace("<< findByFilters()");
		return resultList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ComplementoPago> listarErrores(ComplementoPago complementoPago) throws Exception{
		System.err.println("Entre a dao listarErrores");
		List<ComplementoPago> resultList = new ArrayList<ComplementoPago>();
		StoredProcedureHelper helper = null;

		String [] atributosDTO = {
			"idRecibo",
			"monitorNumPoliza",
			"monitorNumRenovPoliza",
			"situacionRecibo",
			"fechaCubreDesde",
			"fechaCubreHasta",
			"nomCliente",
			"noRecibo",
			"numExhibicion",
			"remesaDesp",
			"fechaContab",
			"usuarioCreacion",
			"fechaEmision",
			"tipoMovimiento",
			"idFactura",
			"folioFiscal",
			"factura",
			"error"};

		String [] columnasResultSet = {
			"RECIBO",
			"NUM_POLIZA",
			"NUM_RENOV_POL",
			"SIT_RECIBO",
			"F_CUBRE_DESDE",
			"F_CUBRE_HASTA",
			"NOM_SOLICITANTE",
			"NUM_FOLIO_RBO",
			"NUM_EXHIBICION",
			"REMESA_DESP",
			"FCONTAB",
			"USUARIO_CREAC",
			"FECHA_EMISION",
			"TIPO_MOVIMIENTO",
			"IDFACTURA",
			"PAGO_CVE_COMPROBANTE",
			"FACTURA",
			"DESCR_ERROR"};

		try{
			log.debug("Entre al try catch del procedure");
			helper = new StoredProcedureHelper(ERRORES_COMPLEMENTO_PAGO_PROCEDURE_NAME,interfazDataSource);
			helper.estableceParametro("P_NoRecibo",complementoPago.getNoRecibo());
			helper.estableceParametro("P_FechaExpedicionInicial",complementoPago.getFechaPagoInicioStr());
			helper.estableceParametro("P_FechaExpedicionFinal",complementoPago.getFechaPagoFinStr());
			log.debug("Asigne los parametros al helper correctamente");

			helper.estableceMapeoResultados(
					ComplementoPago.class.getCanonicalName(),
					atributosDTO,
					columnasResultSet);

			log.debug("establecer mapeo de resultados en el helper");

			resultList = helper.obtieneListaResultados();
		}catch(Exception e){
			log.error("Error: " + e);
			Integer codErr = null;
			String descErr = null;
			log.error(ERRORES_COMPLEMENTO_PAGO_PROCEDURE_NAME);
			if(helper != null){
				codErr = helper.getCodigoRespuesta();
				descErr = helper.getDescripcionRespuesta();
			}
			log.info("metodo obtenerComplementosPago numero de excepcion: " + codErr);
			log.info("metodo obtenerComplementosPago Descripcion de Excepcion: " + descErr);
		}

		if(!resultList.isEmpty()){
			log.debug("Cantidad de complementos encontrados {" + resultList.size() + "}");

			for(int i=0;i<resultList.size();i++){
				ComplementoPago complementoFor = resultList.get(i);
				log.debug("Consolidado = {" + complementoFor.toString()+ "}");
			}
		}

		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<IngresosAplicar> consultaIngresosAplicar(){
		log.info(">> consultaIngresosAplicar");
		List<IngresosAplicar> resultList = new ArrayList<IngresosAplicar>();
		StoredProcedureHelper helper = null;
		
		String [] atributosDTO = {
				"idVersionSdo",
				"fTerReg",
				"fIniReg",
				"idRefunic",
				"sdoRefunicDep",
				"fIngreso",
				"fCarga",
				"cveUsuario",
				"cuentaBancaria",
				"nomBanco",
				"moneda",
				"referenciaOri",
				"usuCarga",
				"codigo"};

			String [] columnasResultSet = {
				"ID_VERSION_SDO",
				"F_TER_REG",
				"F_INI_REG",
				"ID_REFUNIC",
				"SDO_REFUNIC_DEP",
				"F_INGRESO",
				"F_CARGA",
				"CVE_USUARIO",
				"CUENTA_BANCARIA",
				"NOM_BANCO",
				"ID_MONEDA",
				"REFERENCIA_ORI",
				"USU_CARGA",
				"CODIGO"};
			try{
				log.debug("Entre al try catch del procedure");
				helper = new StoredProcedureHelper(CONSULTA_INGRESOS_APLICAR,interfazDataSource);
				helper.estableceMapeoResultados(IngresosAplicar.class.getCanonicalName(),atributosDTO,columnasResultSet);
				log.debug("establecer mapeo de resultados en el helper");
				resultList = helper.obtieneListaResultados();
				
			}catch(Exception e){
				log.error("Error: " + e);
				Integer codErr = null;
				String descErr = null;
				log.error(CONSULTA_INGRESOS_APLICAR);
				if(helper != null){
					codErr = helper.getCodigoRespuesta();
					descErr = helper.getDescripcionRespuesta();
				}
				
				log.info("metodo consultaIngresosAplicar numero de excepcion: " + codErr);
				log.info("metodo consultaIngresosAplicar Descripcion de Excepcion: " + descErr);
			}

			if(!resultList.isEmpty()){
				log.debug("Cantidad de complementos encontrados {" + resultList.size() + "}");

				for(int i=0;i<resultList.size();i++){
					IngresosAplicar complementoFor = resultList.get(i);
					log.debug("Consolidado = {" + complementoFor.toString()+ "}");
				}
			}
	
		return resultList;
		
	}

}
