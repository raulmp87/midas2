package mx.com.afirme.midas2.dao.impl.cobranza.programapago;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.cobranza.programapago.DesgloseDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToDesgloseRecibo;

@Stateless
public class DesgloseDaoImpl extends JpaDao<Long, ToDesgloseRecibo> implements DesgloseDao{

}
