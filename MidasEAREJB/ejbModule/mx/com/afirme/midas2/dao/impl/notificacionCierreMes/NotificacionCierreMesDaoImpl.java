package mx.com.afirme.midas2.dao.impl.notificacionCierreMes;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.notificacionCierreMes.NotificacionCierreMesDao;
import mx.com.afirme.midas2.domain.movil.cliente.CorreosCosultaMovil;

import org.apache.log4j.Logger;

@Stateless
public class NotificacionCierreMesDaoImpl implements NotificacionCierreMesDao {
	private static final Logger LOG = Logger
			.getLogger(NotificacionCierreMesDaoImpl.class);

	@Override
	public List<CorreosCosultaMovil> getCorreosLista() {
		LOG.info("NotificacionCierreMesDaoImpl");

		List<CorreosCosultaMovil> listAgentes = new ArrayList<CorreosCosultaMovil>();

		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKGREPORTES_AGENTES.getMailAgentes",
					StoredProcedureHelper.DATASOURCE_MIDAS);

			String[] atributosDTO = { "id", "nombre", "correo" };
			String[] columnasResulset = { "ID", "NOMBRE", "CORREO" };
			storedHelper.estableceMapeoResultados(
					CorreosCosultaMovil.class.getCanonicalName(), atributosDTO,
					columnasResulset);
			listAgentes = storedHelper.obtieneListaResultados();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			LOG.info("Error DAOimpl: " + e);
		}
		return listAgentes;
	}
}
