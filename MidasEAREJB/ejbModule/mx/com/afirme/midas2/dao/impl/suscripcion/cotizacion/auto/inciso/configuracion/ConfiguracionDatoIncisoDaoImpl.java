package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoDao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ConfiguracionDatoIncisoDaoImpl extends
		JpaDao<ConfiguracionDatoIncisoId, ConfiguracionDatoInciso> implements
		ConfiguracionDatoIncisoDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfiguracionDatoInciso> getDatosRamoInciso(BigDecimal idTcRamo,Short claveFiltroRIesgo) {
		StringBuffer queryString = new StringBuffer("SELECT model FROM ConfiguracionDatoInciso model");
		queryString.append(" WHERE model.id.idTcRamo = :idTcRamo AND model.id.idTcSubRamo = 0");
		queryString.append(" AND model.id.idToCobertura = 0 AND model.id.claveDetalle = 0");
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.TODOS.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 2");
		}
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 1");
		}
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 0");
		}
		queryString.append(" ORDER BY model.id.idDato");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("idTcRamo", idTcRamo);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfiguracionDatoInciso> getDatosCoberturaInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo, BigDecimal idToCobertura,Short claveFiltroRIesgo) {
		StringBuffer queryString = new StringBuffer("SELECT model FROM ConfiguracionDatoInciso model");
		queryString.append(" WHERE model.id.idTcRamo = :idTcRamo AND model.id.idTcSubRamo = :idTcSubRamo");
		queryString.append(" AND model.id.idToCobertura = :idToCobertura AND model.id.claveDetalle = 0");
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.TODOS.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 2");
		}
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 1");
		}
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 0");
		}
		queryString.append(" ORDER BY model.id.idDato");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("idTcRamo", idTcRamo);
		query.setParameter("idTcSubRamo", idTcSubRamo);
		query.setParameter("idToCobertura", idToCobertura);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfiguracionDatoInciso> getDatosSubRamoInciso(BigDecimal idTcRamo, BigDecimal idTcSubRamo,Short claveFiltroRIesgo) {
		StringBuffer queryString = new StringBuffer("SELECT model FROM ConfiguracionDatoInciso model");
		queryString.append(" WHERE model.id.idTcRamo = :idTcRamo AND model.id.idTcSubRamo = :idTcSubRamo");
		queryString.append(" AND model.id.idToCobertura = 0 AND model.id.claveDetalle = 0");
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.TODOS.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 2");
		}
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 1");
		}
		if (claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus())) {
			queryString.append(" AND model.claveHastaEmision = 0");
		}
		queryString.append(" ORDER BY model.id.idDato");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("idTcRamo", idTcRamo);
		query.setParameter("idTcSubRamo", idTcSubRamo);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ConfiguracionDatoInciso> getConfiguracionDatosIncisoCotAuto(Short claveFiltroRIesgo) {
		StringBuffer queryString = new StringBuffer("SELECT model FROM ConfiguracionDatoInciso model");
		if(claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus())){
			queryString.append(" WHERE model.claveHastaEmision = 1");
		}
		if(claveFiltroRIesgo.equals(ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus())){
			queryString.append(" WHERE model.claveHastaEmision = 0");
		}		
		Query query = entityManager.createQuery(queryString.toString());		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

}
