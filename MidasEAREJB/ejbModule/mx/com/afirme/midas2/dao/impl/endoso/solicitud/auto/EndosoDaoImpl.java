package mx.com.afirme.midas2.dao.impl.endoso.solicitud.auto;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.EndosoDao;
import mx.com.afirme.midas2.dto.RespuestaBoolean_SP;
import mx.com.afirme.midas2.dto.cobranza.programapago.ReciboVO;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;
@Stateless
public class EndosoDaoImpl implements EndosoDao{
	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EndosoDTO> getEndososParaCalculoService(
			BigDecimal idToCotizacion, Date recordFrom) {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT model from EndosoDTO model");
		if (idToCotizacion != null) {
			queryString.append(" WHERE model.idToCotizacion = " + idToCotizacion);
		}
		if (recordFrom != null) {
			queryString.append(" AND model.recordFrom <= :recordFrom");
		}
		
		Query query = entityManager.createQuery(queryString.toString());
		if (recordFrom != null) {
			query.setParameter("recordFrom", recordFrom);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	
	@Override
	public Boolean getExistenCambiosEnCoberturas(Long cotizacionContinuityId) {
		
		Long resultado = null;
		
		String queryString = "SELECT COUNT(cobertura) FROM BitemporalCoberturaSeccion cobertura " +
				" WHERE cobertura.continuity.seccionIncisoContinuity.incisoContinuity.cotizacionContinuity.id = :cotizacionContinuityId " +
				" AND cobertura.recordStatus <> :notInProcess";
		
		Query query = entityManager.createQuery(queryString);
		query.setParameter("cotizacionContinuityId", cotizacionContinuityId);
		query.setParameter("notInProcess", RecordStatus.NOT_IN_PROCESS);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		resultado =  (Long) query.getSingleResult();
		
		return ((resultado != null && resultado.intValue() > 0)?true:false);
		
	}
	
	@Override
	public Boolean getExistenCambiosEnInciso(Long cotizacionContinuityId) {
		
		Long resultado = null;
		
		String queryString = "SELECT COUNT(autoInciso) FROM BitemporalAutoInciso autoInciso " +
				" WHERE autoInciso.continuity.incisoContinuity.cotizacionContinuity.id = :cotizacionContinuityId " +
				" AND autoInciso.recordStatus <> :notInProcess";
		
		Query query = entityManager.createQuery(queryString);
		query.setParameter("cotizacionContinuityId", cotizacionContinuityId);
		query.setParameter("notInProcess", RecordStatus.NOT_IN_PROCESS);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		resultado =  (Long) query.getSingleResult();
		
		return ((resultado != null && resultado.intValue() > 0)?true:false);
	}
	
	@Override
	public Boolean getExistenCambiosEnDatosRiesgo(Long cotizacionContinuityId) {
		
		Long resultado = null;
		
		String queryString = "SELECT COUNT(datoSeccion) FROM BitemporalDatoSeccion datoSeccion " +
				" WHERE datoSeccion.continuity.seccionIncisoContinuity.incisoContinuity.cotizacionContinuity.id = :cotizacionContinuityId " +
				" AND datoSeccion.recordStatus <> :notInProcess";
		
		Query query = entityManager.createQuery(queryString);
		query.setParameter("cotizacionContinuityId", cotizacionContinuityId);
		query.setParameter("notInProcess", RecordStatus.NOT_IN_PROCESS);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		resultado =  (Long) query.getSingleResult();
		
		return ((resultado != null && resultado.intValue() > 0)?true:false);
	}


	@Override
	public Boolean validarRecibosDesagrupados(BigDecimal idToPoliza,
			Integer numeroInciso) {
		
		Boolean resultado = Boolean.FALSE;
	
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS.PKG_INT_MIDAS.STP_VALIDARECIBOSDESAGRUPADOS";
				
		try {
			LogDeMidasEJB3.log("Entrando a validarRecibosDesagrupados..." + this, Level.INFO, null);
								
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pIdToPoliza",idToPoliza);
			storedHelper.estableceParametro("pNumeroInciso",numeroInciso);
			
			storedHelper
			.estableceMapeoResultados(
					RespuestaBoolean_SP.class.getCanonicalName(),
					"resultadoSP", "TIENE_RECIBOS_DESAGRUPADOS");
				
			RespuestaBoolean_SP respuesta = (RespuestaBoolean_SP)storedHelper.obtieneResultadoSencillo();
			resultado = respuesta.getResultadoSPBoolean();
			
			LogDeMidasEJB3.log("Saliendo de validarRecibosDesagrupados, poliza - " + idToPoliza 
					+  " numeroInciso -" + numeroInciso + "..." + this, Level.INFO, null);
			
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			LogDeMidasEJB3.log("Excepcion en BD de validarRecibosDesagrupados..." + this, Level.WARNING, e);
			throw new RuntimeException(e);	
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en validarRecibosDesagrupados..." + this, Level.WARNING, e);	
			throw new RuntimeException(e);	
		}		
		//test
		//resultado = true; //test
		return resultado;
	}


	@Override
	public BigDecimal validarPrimasPendientesPagoInciso(BigDecimal idToPoliza,
			Integer numeroInciso, Date fechaValidacion) { 
		
		ReciboVO resultado = new ReciboVO();
		
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS.PKG_INT_MIDAS.STP_VALIDARECIBOSPAGADOS";
				
		try {
			LogDeMidasEJB3.log("Entrando a validarPrimasPendientesPagoInciso..." + this, Level.INFO, null);			
			
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pIdToPoliza",idToPoliza);
			storedHelper.estableceParametro("pNumeroInciso",numeroInciso);
			storedHelper.estableceParametro("pFechaValidacion",fechaValidacion);
			
			storedHelper
			.estableceMapeoResultados(
					ReciboVO.class.getCanonicalName(),
					"impPrimaTotal", "PRIMA_PENDIENTE_PAGO");
		
			resultado = (ReciboVO) storedHelper.obtieneResultadoSencillo();
			
			LogDeMidasEJB3.log("Saliendo de validarPrimasPendientesPagoInciso, poliza - " + idToPoliza 
					+  " numeroInciso -" + numeroInciso + "..." + this, Level.INFO, null);
			
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			LogDeMidasEJB3.log("Excepcion en BD de validarPrimasPendientesPagoInciso..." + this, Level.WARNING, e);
			
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en validarPrimasPendientesPagoInciso..." + this, Level.WARNING, e);			
		}		

		return resultado.getImpPrimaTotal();		
	}
	

	
	
}
