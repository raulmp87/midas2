package mx.com.afirme.midas2.dao.impl.persona;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.comisiones.ItemsConfigComision;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import org.apache.commons.lang.StringUtils;
@Stateless
public class PersonaSeycosFacade implements PersonaSeycosFacadeRemote{
	private DomicilioFacadeRemote domicilioFacade;
	@PersistenceContext 
	protected EntityManager entityManager;
	@Override
	public List<PersonaSeycosDTO> findAll()throws Exception  {
		return null;
	}
	/**
	 * Metodo para consultar por personas
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<PersonaSeycosDTO> findByFilters(PersonaSeycosDTO filtroPersona,boolean withAddress)throws Exception {
		
		return this.findByFilters(filtroPersona, withAddress, null);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PersonaSeycosDTO> findByFilters(PersonaSeycosDTO filtroPersona,boolean withAddress, String fechaHistorico)throws Exception {
		
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;
		
		List<PersonaSeycosDTO> personas=new ArrayList<PersonaSeycosDTO>();
		if(filtroPersona!=null){
			StoredProcedureHelper storedHelper = null;	
			
			String sp=(withAddress)?"SEYCOS.PKG_INT_MIDAS_E2.cpersonaFiltro":"SEYCOS.PKG_INT_MIDAS_E2.cpersonaSinFiltroDomicilio";			
			
			try {
				LogDeMidasInterfaz.log("Entrando a PersonaSeycosFacade.findByFilters..." + this, Level.INFO, null);
				
				StringBuilder propiedades=new StringBuilder("");
				StringBuilder columnas=new StringBuilder("");
				String[] cols=null;
				String[] props=null;
				//Si desean usar con domicilio o sin domicilio
				if(withAddress){
					String[] colsTemp={
	                    "idPersona","idDomicilio","situacionPersona",
	                    "claveNacionalidad","claveTipoPersona","fechaAlta",
	                    "fechaBaja","claveTipoSituacion","telefonoCasa",
	                    "telefonoFax","telefonoOficina","extension","nombreCompleto",
	                    "email","curp","codigoRFC","telefonoCelular","nombreContacto",
	                    "puestoContacto","facebook","twitter","paginaWeb","telefonosAdicionales",
	                    "correosAdicionales","idRepresentante","razonSocial","fechaConstitucion",
	                    "claveTipoSector","nombre","apellidoPaterno",
	                    "apellidoMaterno","claveEstadoCivil","claveSexo",
	                     "fechaNacimiento","claveEstadoNacimiento","claveMunicipioNacimiento"
			         };
			         String[] propsTemp={
	                    "idPersona","idDomicilio","situacionPersona",
	                    "clavePaisNacimiento","claveTipoPersona","fechaAlta",
	                    "fechaBaja","tipoSituacion","telCasa",
	                    "telFax","telOficina","extension","nombreCompleto",
	                    "email","codigoCURP","codigoRFC","telCelular",
	                    "nombreContacto","puestoContacto","faceBook","twitter",
	                    "paginaWeb","telefonosAdicionales","correosAdicionales",
	                    "idRepresentante","razonSocial","fechaConstitucion",
	                    "claveSectorFinanciero","nombre","apellidoPaterno",
	                    "apellidoMaterno","estadoCivil","sexo","fechaNacimiento",
	                    "claveEstadoNacimiento","claveCiudadNacimiento"
			         };
			         cols=colsTemp;
			         props=propsTemp;
				}else{
					String[] colsTemp={
	                    "idPersona","situacionPersona",
	                    "claveNacionalidad","claveTipoPersona","fechaAlta",
	                    "fechaBaja","claveTipoSituacion","telefonoCasa",
	                    "telefonoFax","telefonoOficina","extension","nombreCompleto",
	                    "email","curp","codigoRFC","telefonoCelular","nombreContacto",
	                    "puestoContacto","facebook","twitter","paginaWeb","telefonosAdicionales",
	                    "correosAdicionales","idRepresentante","razonSocial","fechaConstitucion",
	                    "claveTipoSector","nombre","apellidoPaterno",
	                    "apellidoMaterno","claveEstadoCivil","claveSexo",
	                     "fechaNacimiento","claveEstadoNacimiento","claveMunicipioNacimiento"
			         };
			         String[] propsTemp={
	                    "idPersona","situacionPersona",
	                    "clavePaisNacimiento","claveTipoPersona","fechaAlta",
	                    "fechaBaja","tipoSituacion","telCasa",
	                    "telFax","telOficina","extension","nombreCompleto",
	                    "email","codigoCURP","codigoRFC","telCelular",
	                    "nombreContacto","puestoContacto","faceBook","twitter",
	                    "paginaWeb","telefonosAdicionales","correosAdicionales",
	                    "idRepresentante","razonSocial","fechaConstitucion",
	                    "claveSectorFinanciero","nombre","apellidoPaterno",
	                    "apellidoMaterno","estadoCivil","sexo","fechaNacimiento",
	                    "claveEstadoNacimiento","claveCiudadNacimiento"
			         };
			         cols=colsTemp;
			         props=propsTemp;
				}
				for(int i=0;i<cols.length;i++){
					String columnName=cols[i];
					String propertyName=props[i];
					propiedades.append(propertyName);
					columnas.append(columnName);
					if(i<(cols.length-1)){
						propiedades.append(",");
						columnas.append(",");
					}
				}
				Domicilio domicilio=(filtroPersona.getDomicilios()!=null && !filtroPersona.getDomicilios().isEmpty())?filtroPersona.getDomicilios().get(0):null;
				String claveEstado=null;
				String claveCiudad=null;
				String nombreColonia=null;
				String calleNumero=null;
				String codigoPostal=null;
				//Si trae domicilio se ejecuta otra consulta de filtro de personas con filtros de domicilio pero sin la columna idDomicilio
				if(domicilio!=null){
					claveEstado=domicilio.getClaveEstado();
					claveCiudad=domicilio.getClaveCiudad();
					nombreColonia=domicilio.getNombreColonia();
					calleNumero=domicilio.getCalleNumero();
					codigoPostal=domicilio.getCodigoPostal();
//					sp="SEYCOS.PKG_INT_MIDAS_E2.cpersonaSinFiltroDomicilio";
					sp="SEYCOS.PKG_INT_MIDAS_E2.cpersonaFiltroNoDomicilio";
				}//De lo contrario se ejecuta otro stored 
				String nombre=filtroPersona.getNombre();
				if(nombre==null || nombre.isEmpty()){
					nombre=filtroPersona.getRazonSocial();
				}
				storedHelper = new StoredProcedureHelper(sp);
				Short tipoSituacion_=filtroPersona.getTipoSituacion();
				String tipoSituacion=(tipoSituacion_!=null)?"0"+tipoSituacion_.toString():null;
				String paramTipoPersona="";
				if(filtroPersona.getClaveTipoPersona()!=null){
					if(filtroPersona.getClaveTipoPersona()==1){
						paramTipoPersona ="PF";
					}else{
						paramTipoPersona ="PM";						
					}
					
				}
				String situacionPersona=filtroPersona.getSituacionPersona();
				Date fechaInicioDate=filtroPersona.getFechaAlta();
				Date fechaFinDate=filtroPersona.getFechaBaja();
				SimpleDateFormat formatter=new SimpleDateFormat("dd/MM/yyyy");
				String fechaInicio=(fechaInicioDate!=null)?formatter.format(fechaInicioDate):null;
				String fechaFin=(fechaFinDate!=null)?formatter.format(fechaFinDate):null;
				storedHelper.estableceMapeoResultados(PersonaSeycosDTO.class.getCanonicalName(),propiedades.toString(),columnas.toString());
				storedHelper.estableceParametro("pid_persona", val(filtroPersona.getIdPersona()));
				storedHelper.estableceParametro("pnombre", val(nombre));
				storedHelper.estableceParametro("prfc", val(filtroPersona.getCodigoRFC()));
				storedHelper.estableceParametro("pclaveEstado", val(claveEstado));
				storedHelper.estableceParametro("pclaveCiudad", val(claveCiudad));
				storedHelper.estableceParametro("pnombreColonia", val(nombreColonia));
				storedHelper.estableceParametro("pcalleNumero", val(calleNumero));
				storedHelper.estableceParametro("pcodigoPostal", val(codigoPostal));
				storedHelper.estableceParametro("ptelefono", val(filtroPersona.getTelOficina()));
				storedHelper.estableceParametro("pCve_Actividad_PF", val(tipoSituacion));
				storedHelper.estableceParametro("pFechaInicio", val(fechaInicioDate));
				storedHelper.estableceParametro("pFechaFin", val(fechaFinDate));
				storedHelper.estableceParametro("pSituacion", val(situacionPersona));
				storedHelper.estableceParametro("pTipoPersona", val(paramTipoPersona));
				if(isHistorico)
				{
					SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss.SSSSSS a");
				    Date convertedDate = new Date();
					
					try {
						convertedDate = dateFormat.parse(fechaHistorico);
					} catch (ParseException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					storedHelper.estableceParametro("pFechaHistorico", val(convertedDate));					
				}
				
				personas=storedHelper.obtieneListaResultados();
				LogDeMidasInterfaz.log("Saliendo de PersonaSeycosFacade.findByFilters..." + this, Level.INFO, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.BUSCAR,sp, filtroPersona.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de PersonaSeycosFacade.findByFilters..." + this, Level.WARNING, e);
				throw e;
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en PersonaSeycosFacade.findByFilters..." + this, Level.WARNING, e);
				throw e;
			}
		}
		return personas;
	}

	@Override
	public PersonaSeycosDTO findById(Long idPersona) throws Exception {
		/*if(idPersona==null){
			throw new Exception("Persona.idPersona is null");
		}
		PersonaSeycosDTO persona=new PersonaSeycosDTO();
		persona.setIdPersona(idPersona);
		List<PersonaSeycosDTO> personas=findByFilters(persona,true);
		if(personas!=null && !personas.isEmpty()){
			persona=personas.get(0);	
			if(persona.getClaveTipoPersona()==2){
				persona.setFechaNacimiento(persona.getFechaConstitucion());
			}
			List<Domicilio> domicilios=domicilioFacade.findByPerson(persona.getIdPersona());
			persona.setDomicilios(domicilios);
		}else{
			persona=null;
		}
		return persona;*/
		return this.findById(idPersona, null);
	}
	
	@Override
	public PersonaSeycosDTO findById(Long idPersona, String fechaHistorico) throws Exception {		
		
		if(idPersona==null){
			throw new Exception("Persona.idPersona is null");
		}
		PersonaSeycosDTO persona=new PersonaSeycosDTO();
		persona.setIdPersona(idPersona);
		List<PersonaSeycosDTO> personas=findByFilters(persona,false, fechaHistorico);
		if(personas!=null && !personas.isEmpty()){
			persona=personas.get(0);	
			if(persona.getClaveTipoPersona()==2){
				persona.setFechaNacimiento(persona.getFechaConstitucion());
			}			
			
			List<Domicilio> domicilios=domicilioFacade.findDomiciliosActualesPorPersona(persona.getIdPersona(),null,fechaHistorico);
			List<Domicilio> newDomicilios = new ArrayList<Domicilio>();
			for(Domicilio dom: domicilios){
				if(isNotNull(dom)){
					newDomicilios.add(dom);
				}
			}
			persona.setDomicilios(newDomicilios);
		}else{
			persona=null;
		}
		return persona;
	}
	
	/**
	 * Metodo para guardar personas y si trae mas de un domicilio entonces tambien se guardan sus datos.
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public PersonaSeycosDTO save(PersonaSeycosDTO persona) throws Exception {
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATPERSONAS";
		try {
			LogDeMidasInterfaz.log("Entrando a PersonaSeycosFacade.save..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			int idTipoPersonaMoral=2;
			String situacion=(persona.getSituacionPersona()!=null && !persona.getSituacionPersona().isEmpty())?persona.getSituacionPersona():"IN";
			Short tipoSituacion_=persona.getTipoSituacion();
			String tipoSituacion=null;
//			tipoSituacion=(tipoSituacion_!=null)?"0"+tipoSituacion_.toString():"01";
			tipoSituacion=("AC".equalsIgnoreCase(situacion))?"01":"00";
//			Short situacionInactiva=Short.valueOf("0");
//			String situacionPersona=(tipoSituacion_!=null && situacionInactiva.equals(tipoSituacion_))?"IN":"AC";
			String rama_default="01";
			Date fechaConstitucion=(persona.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?persona.getFechaNacimiento():null;
			String razonSocial=(persona.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?persona.getRazonSocial():null;
			String nombre=(persona.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?persona.getRazonSocial():persona.getNombre();
			List<Domicilio> domicilios=persona.getDomicilios();
			Domicilio domicilio=(domicilios!=null && !domicilios.isEmpty())?domicilios.get(0):null;
			String nuevaColonia=(domicilio.getNuevaColonia()!=null && !domicilio.getNuevaColonia().trim().isEmpty())?domicilio.getNuevaColonia():null;
			String nombreColonia=(domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().trim().isEmpty())?domicilio.getNombreColonia():null;
			nombreColonia=(nuevaColonia!=null)?nuevaColonia:nombreColonia;
			String nombreCalle=(domicilio!=null && domicilio.getCalleNumero()!=null && !domicilio.getCalleNumero().isEmpty())?domicilio.getCalleNumero():null;
			String idMunicipio = (domicilio!=null && domicilio.getClaveCiudad()!=null && !domicilio.getClaveCiudad().isEmpty())?domicilio.getClaveCiudad():null;
			String codigoPostal=(domicilio!=null && domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().isEmpty())?domicilio.getCodigoPostal():null;
			String tipoDomicilio=(domicilio!=null && domicilio.getTipoDomicilio()!=null && !domicilio.getTipoDomicilio().isEmpty())?domicilio.getTipoDomicilio():null;
//			Long idPersonaDefault=(persona.getIdPersona()!=null)?persona.getIdPersona():0l;
			if(idMunicipio!=null){
				idMunicipio = obtenerId(idMunicipio);
			}
			if(persona.getClaveTipoPersona()==2){
				persona.setClavePaisNacimiento((persona.getClavePaisNacimiento()==null)?"PAMEXI":persona.getClavePaisNacimiento());
			}
			//TODO Agregar tipo de domicilio al stored, a la tabla domicilio, a la entidad de domicilio.
			storedHelper.estableceParametro("ptipo_persona", val((Short)persona.getClaveTipoPersona()));
			storedHelper.estableceParametro("pid_persona", val(persona.getIdPersona()));			
			storedHelper.estableceParametro("pnombre", val(nombre));
			storedHelper.estableceParametro("papellido_pat", val(persona.getApellidoPaterno()));
			storedHelper.estableceParametro("papellido_mat", val(persona.getApellidoMaterno()));
			storedHelper.estableceParametro("pgenero", val(persona.getSexo()));
			storedHelper.estableceParametro("pRFC", val(persona.getCodigoRFC()));
			storedHelper.estableceParametro("pe_mail", val(persona.getEmail()));
			storedHelper.estableceParametro("pCURP", val(persona.getCodigoCURP()));
			storedHelper.estableceParametro("pTelefono", val(persona.getTelefono()));
			storedHelper.estableceParametro("pExtension", val(persona.getExtension()));
			storedHelper.estableceParametro("pEstadoCivil", val(persona.getEstadoCivil()));
			storedHelper.estableceParametro("pFechaNac", persona.getFechaNacimiento());
			storedHelper.estableceParametro("pidpaisNac",  val(persona.getClavePaisNacimiento()));
			storedHelper.estableceParametro("pidestadoNac", val(persona.getClaveEstadoNacimiento()));
			storedHelper.estableceParametro("pidCiudadNac", val(persona.getClaveCiudadNacimiento()));
			storedHelper.estableceParametro("pCve_Actividad_PF", val(tipoSituacion));
			storedHelper.estableceParametro("pCve_Tipo_Sector",val(persona.getClaveSectorFinanciero()));
			storedHelper.estableceParametro("pCve_Rama_PF", rama_default);//TODO falta la rama
			storedHelper.estableceParametro("pcalle_numero", val(nombreCalle));
			storedHelper.estableceParametro("pcodigo_postal",val(codigoPostal));
			storedHelper.estableceParametro("pnom_colonia",val(nombreColonia));
			storedHelper.estableceParametro("ptipo_domicilio",val(tipoDomicilio));
			storedHelper.estableceParametro("pNomRazonSocial",val(razonSocial));
			storedHelper.estableceParametro("PFConsitucion",val(persona.getFechaConstitucion()));
			storedHelper.estableceParametro("pCve_Rama_Activ", "");//TODO falta rama activ
			storedHelper.estableceParametro("pCve_sub_rama_ac", "");
			storedHelper.estableceParametro("pidrepresentante", val(persona.getIdRepresentante()));
			storedHelper.estableceParametro("pSituacion", val(situacion));
			int idPersona=storedHelper.ejecutaActualizar();
			persona.setIdPersona(new Long(idPersona));
			LogDeMidasInterfaz.log("Se ha guardado persona con id:"+idPersona +"..."+ this, Level.INFO, null);
			//Si es mayor a 1 entonces hay mas de un domicilio, ya que al inicio ya se guardo un primer domicilio.
			if(persona.getDomicilios()!=null && !persona.getDomicilios().isEmpty() && persona.getDomicilios().size()>1){
				for(int i=0;i<persona.getDomicilios().size();i++){
					Domicilio dom=persona.getDomicilios().get(i);
					dom.setIdPersona(persona.getIdPersona());//se le asigna el domicilio a la misma persona guardada
					LogDeMidasInterfaz.log("Entrnado a guardar domicilios de PersonaSeycos..." + this, Level.INFO, null);
					domicilioFacade.save(dom, null);
					LogDeMidasInterfaz.log("Saliendo de guardar domicilios de PersonaSeycos..." + this, Level.INFO, null);
				}
			}
			LogDeMidasInterfaz.log("Saliendo de PersonaSeycosFacade.save..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, persona.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PersonaSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PersonaSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		}
		return persona;
	}
	
	public String obtenerId(String id) {
		return StringUtils.leftPad(id, 5, '0');	

	}
	@EJB
	public void setDomicilioFacade(DomicilioFacadeRemote domicilioFacade) {
		this.domicilioFacade = domicilioFacade;
	}
	
	private Object val(Object o){
		return (o==null)?"":o;
	}
	/**
	 * GUarda unicamente los datos de contacto de una persona y tambien los datos de oficina
	 */
	@Override
	public PersonaSeycosDTO saveContactData(PersonaSeycosDTO persona)throws Exception {
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_CatContactosPersona";
		try {
			LogDeMidasInterfaz.log("Entrando a PersonaSeycosFacade.saveContactData..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pid_persona", val(persona.getIdPersona()));
			storedHelper.estableceParametro("pnombre_contacto","");
			storedHelper.estableceParametro("ppuesto_contacto", "");
			storedHelper.estableceParametro("ptel_casa", val(persona.getTelCasa()));
			storedHelper.estableceParametro("ptel_oficina", val(persona.getTelOficina()));
			storedHelper.estableceParametro("pExtension", val(persona.getExtension()));
			storedHelper.estableceParametro("ptel_celular", val(persona.getTelCelular()));
			storedHelper.estableceParametro("ptel_fax", val(persona.getTelFax()));
			storedHelper.estableceParametro("pcorreo_electronico", val(persona.getEmail()));
			storedHelper.estableceParametro("pfacebook", val(persona.getFaceBook()));
			storedHelper.estableceParametro("ptwitter", val(persona.getTwitter()));
			storedHelper.estableceParametro("ppagina_web", val(persona.getPaginaWeb()));
			storedHelper.estableceParametro("ptels_adicionales", val(persona.getTelefonosAdicionales()));
			storedHelper.estableceParametro("pcorreos_adicionales", val(persona.getCorreosAdicionales()));
			int idPersona=storedHelper.ejecutaActualizar();
			persona.setIdPersona(new Long(idPersona));
			//TODO Ejecutar lo de domicilio por los tipos de domicilio
			if(persona.getDomicilios()!=null && !persona.getDomicilios().isEmpty() && persona.getDomicilios().size()>0){
				Domicilio domicilio=persona.getDomicilios().get(0);
				domicilio.setTipoDomicilio(TipoDomicilio.OFICINA.getValue());
				domicilio.setIdPersona(persona.getIdPersona());//se le asigna el domicilio a la misma persona guardada
				LogDeMidasInterfaz.log("Entrando a guardar domicilios de PersonaSeycos..." + this, Level.INFO, null);
				domicilioFacade.save(domicilio, null);
				LogDeMidasInterfaz.log("Saliendo de guardar domicilios de PersonaSeycos..." + this, Level.INFO, null);
			}
			LogDeMidasInterfaz.log("Se ha guardado datos de contacto persona con id:"+idPersona +"..."+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de PersonaSeycosFacade.saveContactData..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, persona.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PersonaSeycosFacade.saveContactData..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PersonaSeycosFacade.saveContactData..." + this, Level.WARNING, e);
			throw e;
		}
		return persona;
	}
	@Override
	public PersonaSeycosDTO saveOficeData(PersonaSeycosDTO persona)throws Exception {
		return saveContactData(persona);
	}
	@Override
	public PersonaSeycosDTO unsubscribe(PersonaSeycosDTO persona) throws Exception{
		if(persona==null){
			throw new Exception("PersonaSeycosDTO is null");
		}
		 if(personaIsAsocciate(persona.getIdPersona())==true){
			 throw new Exception(" La Persona Esta Asociada");
		 }
		//TODO El stored de obtener los datos de la persona
		persona=findById(persona.getIdPersona());
		persona.setTipoSituacion((short)0);//se pone con estatus de inhabilitado
		persona.setSituacionPersona("IN");		
		persona.setFechaBaja(new Date());
//		persona=save(persona);//se actualiza su estatus
		persona = saveWithoutAddress(persona);
		return persona;
	}
//******************************************************************************
	public boolean personaIsAsocciate(Long idPersona) throws Exception{
		boolean isAsocciate = false;
		if(personaIsAsocciateCentroOper(idPersona)==true){
			isAsocciate = true;
			return isAsocciate;
		}
		if(personaIsAsocciateGerencia(idPersona)==true){
			isAsocciate = true;
			return isAsocciate;
		}
		if(personaIsAsocciateEjecutivo(idPersona)==true){
			isAsocciate = true;
			return isAsocciate;
		}
		if(personaIsAsocciatePromotoria(idPersona)==true){
			isAsocciate = true;
			return isAsocciate;
		}
		if(personaIsAsocciateAgente(idPersona)==true){
			isAsocciate = true;
			return isAsocciate;
		}
		if(personaIsAsocciatePersona(idPersona)==true){
			isAsocciate = true;
			return isAsocciate;
		}
		return isAsocciate;
	}
	
	public boolean personaIsAsocciatePersona(Long idPersona) throws Exception{
		StringBuilder queryString = new StringBuilder();
		boolean isAsocciate = false;
		queryString.append("select  count(*) as id from MIDAS.vw_persona p where p.CLAVETIPOSITUACION='AC' and p.IDREPRESENTANTE= "+idPersona);
			Query query =  entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
			List<ItemsConfigComision> obj= query.getResultList();
			if(!obj.isEmpty()){
				if(!obj.get(0).getId().equals(0)){
					isAsocciate = true;
				}
			}
		return isAsocciate;
	}
	
	public boolean personaIsAsocciateAgente(Long idPersona) throws Exception{
		StringBuilder queryString = new StringBuilder();
		boolean isAsocciate = false;
		queryString.append("select  count(*) as id  from MIDAS.vw_persona p, MIDAS.toagente agente where agente.IDPERSONA = p.IDPERSONA and p.IDPERSONA = "+idPersona);
			Query query =  entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
			List<ItemsConfigComision> obj= query.getResultList();
			if(!obj.isEmpty()){
				if(!obj.get(0).getId().equals(0)){
					isAsocciate = true;
				}
			}
		return isAsocciate;
	}
	
	public boolean personaIsAsocciatePromotoria(Long idPersona) throws Exception{
		StringBuilder queryString = new StringBuilder();
		boolean isAsocciate = false;
		queryString.append("select  count(*) as id from MIDAS.vw_persona p, MIDAS.toPromotoria promotoria where promotoria.IDPERSONARESPONSABLE = p.IDPERSONA and p.IDPERSONA ="+idPersona);
			Query query =  entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
			List<ItemsConfigComision> obj= query.getResultList();
			if(!obj.isEmpty()){
				if(!obj.get(0).getId().equals(0)){
					isAsocciate = true;
				}
			}
		return isAsocciate;
	}
	
	public boolean personaIsAsocciateEjecutivo(Long idPersona) throws Exception{
		StringBuilder queryString = new StringBuilder();
		boolean isAsocciate = false;
		queryString.append("select  count(*) as id from MIDAS.vw_persona p, MIDAS.toEjecutivo ejecutivo where ejecutivo.IDPERSONA = p.IDPERSONA and p.IDPERSONA ="+idPersona);
			Query query =  entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
			List<ItemsConfigComision> obj= query.getResultList();
			if(!obj.isEmpty()){
				if(!obj.get(0).getId().equals(0)){
					isAsocciate = true;
				}
			}
		return isAsocciate;
	}
	
	public boolean personaIsAsocciateGerencia(Long idPersona) throws Exception{
		StringBuilder queryString = new StringBuilder();
		boolean isAsocciate = false;
		queryString.append("select  count(*) as id from MIDAS.vw_persona p, MIDAS.togerencia gerencia where gerencia.IDPERSONARESPONSABLE = p.IDPERSONA and p.IDPERSONA ="+idPersona);
			Query query =  entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
			List<ItemsConfigComision> obj= query.getResultList();
			if(!obj.isEmpty()){
				if(!obj.get(0).getId().equals(0)){
					isAsocciate = true;
				}
			}
		return isAsocciate;
	}
	public boolean personaIsAsocciateCentroOper(Long idPersona) throws Exception{
		StringBuilder queryString = new StringBuilder();
		boolean isAsocciate = false;
		queryString.append("select  count(*) as id from MIDAS.vw_persona p, MIDAS.toCentroOperacion CentroOper where CentroOper.IDPERSONARESPONSABLE = p.IDPERSONA and p.IDPERSONA ="+idPersona);
			Query query =  entityManager.createNativeQuery(queryString.toString(), ItemsConfigComision.class);
			List<ItemsConfigComision> obj= query.getResultList();
			if(!obj.isEmpty()){
				if(!obj.get(0).getId().equals(0)){
					isAsocciate = true;
				}
			}
		return isAsocciate;
	}
//***********************************************************************************************
	@Override
	public PersonaSeycosDTO saveWithoutAddress(PersonaSeycosDTO persona)
			throws Exception {
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.STP_CATPERSONAS";
		try {
			LogDeMidasInterfaz.log("Entrando a PersonaSeycosFacade.save..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			int idTipoPersonaMoral=2;
			String situacion=(persona.getSituacionPersona()!=null && !persona.getSituacionPersona().isEmpty())?persona.getSituacionPersona():"IN";
//			Short tipoSituacion_=persona.getTipoSituacion();
			String tipoSituacion=null;
//			tipoSituacion=(tipoSituacion_!=null)?"0"+tipoSituacion_.toString():"01";
			tipoSituacion=("AC".equalsIgnoreCase(situacion))?"01":"00";
//			Short situacionInactiva=Short.valueOf("0");
//			String situacionPersona=(tipoSituacion_!=null && situacionInactiva.equals(tipoSituacion_))?"IN":"AC";
			String rama_default="01";
			Date fechaConstitucion=(persona.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?persona.getFechaNacimiento():null;
			String razonSocial=(persona.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?persona.getRazonSocial():null;
			String nombre=(persona.getClaveTipoPersona().intValue()==idTipoPersonaMoral)?persona.getRazonSocial():persona.getNombre();
			List<Domicilio> domicilios=persona.getDomicilios();
			Domicilio domicilio=(domicilios!=null && !domicilios.isEmpty())?domicilios.get(0):null;
			String nombreColonia=(domicilio!=null && domicilio.getNombreColonia()!=null && !domicilio.getNombreColonia().isEmpty())?domicilio.getNombreColonia():null;
			String nombreCalle=(domicilio!=null && domicilio.getCalleNumero()!=null && !domicilio.getCalleNumero().isEmpty())?domicilio.getCalleNumero():null;
			String idMunicipio = (domicilio!=null && domicilio.getClaveCiudad()!=null && !domicilio.getClaveCiudad().isEmpty())?domicilio.getClaveCiudad():null;
			String codigoPostal=(domicilio!=null && domicilio.getCodigoPostal()!=null && !domicilio.getCodigoPostal().isEmpty())?domicilio.getCodigoPostal():null;
			String tipoDomicilio=(domicilio!=null && domicilio.getTipoDomicilio()!=null && !domicilio.getTipoDomicilio().isEmpty())?domicilio.getTipoDomicilio():null;
//			Long idPersonaDefault=(persona.getIdPersona()!=null)?persona.getIdPersona():0l;
			if(idMunicipio!=null){
					idMunicipio= obtenerId(idMunicipio);
			}
			//TODO Agregar tipo de domicilio al stored, a la tabla domicilio, a la entidad de domicilio.
			storedHelper.estableceParametro("ptipo_persona", val((Short)persona.getClaveTipoPersona()));
			storedHelper.estableceParametro("pid_persona", val(persona.getIdPersona()));			
			storedHelper.estableceParametro("pnombre", val(nombre));
			storedHelper.estableceParametro("papellido_pat", val(persona.getApellidoPaterno()));
			storedHelper.estableceParametro("papellido_mat", val(persona.getApellidoMaterno()));
			storedHelper.estableceParametro("pgenero", val(persona.getSexo()));
			storedHelper.estableceParametro("pRFC", val(persona.getCodigoRFC()));
			storedHelper.estableceParametro("pe_mail", val(persona.getEmail()));
			storedHelper.estableceParametro("pCURP", val(persona.getCodigoCURP()));
			storedHelper.estableceParametro("pTelefono", val(persona.getTelefono()));
			storedHelper.estableceParametro("pExtension", val(persona.getExtension()));
			storedHelper.estableceParametro("pEstadoCivil", val(persona.getEstadoCivil()));
			storedHelper.estableceParametro("pFechaNac", persona.getFechaNacimiento());
			storedHelper.estableceParametro("pidpaisNac",  val(persona.getClavePaisNacimiento()));
			storedHelper.estableceParametro("pidestadoNac", val(persona.getClaveEstadoNacimiento()));
			storedHelper.estableceParametro("pidCiudadNac", val(persona.getClaveCiudadNacimiento()));
			storedHelper.estableceParametro("pCve_Actividad_PF", val(tipoSituacion));
			storedHelper.estableceParametro("pCve_Tipo_Sector",val(persona.getClaveSectorFinanciero()));
			storedHelper.estableceParametro("pCve_Rama_PF", rama_default);//TODO falta la rama
			storedHelper.estableceParametro("pcalle_numero", val(nombreCalle));
			storedHelper.estableceParametro("pcodigo_postal",val(codigoPostal));
			storedHelper.estableceParametro("pnom_colonia",val(nombreColonia));
			storedHelper.estableceParametro("ptipo_domicilio",val(tipoDomicilio));
			storedHelper.estableceParametro("pNomRazonSocial",val(razonSocial));
			storedHelper.estableceParametro("PFConsitucion",val(persona.getFechaConstitucion()));
			storedHelper.estableceParametro("pCve_Rama_Activ", "");//TODO falta rama activ
			storedHelper.estableceParametro("pCve_sub_rama_ac", "");
			storedHelper.estableceParametro("pidrepresentante", val(persona.getIdRepresentante()));
			storedHelper.estableceParametro("pSituacion", val(situacion));
			int idPersona=storedHelper.ejecutaActualizar();
			persona.setIdPersona(new Long(idPersona));
			LogDeMidasInterfaz.log("Se ha guardado persona con id:"+idPersona +"..."+ this, Level.INFO, null);			
			LogDeMidasInterfaz.log("Saliendo de PersonaSeycosFacade.save..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, persona.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PersonaSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PersonaSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		}
		return persona;
	}

}
