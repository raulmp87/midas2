package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.compensaciones.CaParametrosDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.dto.impresiones.ReporteNegCompensacionesDTO;
import mx.com.afirme.midas2.util.UtileriasWeb;

@Stateless
public class CaParametrosDaoImpl  extends EntidadHistoricoDaoImpl implements CaParametrosDao {
	public static final String MONTOUTILIDADES = "montoUtilidades";
	public static final String PORCENTAJEPAGO = "porcentajePago";
	public static final String MONTOPAGO = "montoPago";
	public static final String PORCENTAJEAGENTE = "porcentajeAgente";
	public static final String PORCENTAJEPROMOTOR = "porcentajePromotor";
	public static final String EMISIONINTERNA = "emisionInterna";
	public static final String EMISIONEXTERNA = "emisionExterna";
	public static final String PARAMETROACTIVO = "parametroActivo";
	public static final String TOPEMAXIMO = "topeMaximo";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	public static final String COMPENSADICIOID = "caCompensacion.id";
	public static final String TIPOCOMPENSACIONID ="caTipoCompensacion.id";
	public static final String ENTIDADPERSONAID = "caEntidadPersona.id";

	@PersistenceContext
	private EntityManager entityManager;
	

/** Log de CaParametrosDaoImpl */
private static final Logger LOGGER = LoggerFactory
		.getLogger(CaParametrosDaoImpl.class);

	 public void save(CaParametros entity) {
	    	LOGGER.info("	::	[INF]	::	Guardando CaParametros 	::		CaParametrosDaoImpl	::	save	::	INICIO	::	");
		        try {
	            entityManager.persist(entity);
	            LOGGER.info("	::	[INF]	::	Se Guardo CaParametros 	::		CaParametrosDaoImpl	::	save	::	FIN	::	");
		        } catch (RuntimeException re) {
		        	LOGGER.error("	::	[ERR]	::	Error al guardar CaParametros 	::		CaParametrosDaoImpl	::	save	::	ERROR	::	",re);
		            throw re;
	        }
	    }
	    
	    /**
		 Delete a persistent CaParametros entity.
		  @param entity CaParametros entity to delete
		 @throws RuntimeException when the operation fails
		 */
	    public void delete(CaParametros entity) {
	    	LOGGER.info("	::	[INF]	::	Eliminando CaParametros 	::		CaParametrosDaoImpl	::	delete	::	INICIO	::	");
		        try {
	        	entity = entityManager.getReference(CaParametros.class, entity.getId());
	            entityManager.remove(entity);
	            LOGGER.info("	::	[INF]	::	Se Elimino CaParametros 	::		CaParametrosDaoImpl	::	delete	::	FIN	::	");
		        } catch (RuntimeException re) {
		        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaParametros 	::		CaParametrosDaoImpl	::	delete	::	ERROR	::	",re);
		            throw re;
	        }
	    }
	    
	    /**
		 Persist a previously saved CaParametros entity and return it or a copy of it to the sender. 
		 A copy of the CaParametros entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
		  @param entity CaParametros entity to update
		 @return CaParametros the persisted CaParametros entity instance, may not be the same
		 @throws RuntimeException if the operation fails
		 */
	    public CaParametros update(CaParametros entity) {
	    	LOGGER.info("	::	[INF]	::	Actualizando CaParametros 	::		CaParametrosDaoImpl	::	update	::	INICIO	::	");
		        try {
	            CaParametros result = entityManager.merge(entity);
	            LOGGER.info("	::	[INF]	::	Se Actualizo CaParametros 	::		CaParametrosDaoImpl	::	update	::	FIN	::	");
		            return result;
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaParametros 	::		CaParametrosDaoImpl	::	update	::	ERROR	::	",re);
		            throw re;
	        }
	    }
	    
	    public CaParametros findById( Long id) {
	    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaParametrosDaoImpl	::	findById	::	INICIO	::	");
		        try {
	            CaParametros instance = entityManager.find(CaParametros.class, id);
	            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaParametrosDaoImpl	::	findById	::	FIN	::	");
	            return instance;
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaParametrosDaoImpl	::	findById	::	ERROR	::	",re);
	        	return null;
	        }
	    }    
	    

	/**
		 * Find all CaParametros entities with a specific property value.  
		 
		  @param propertyName the name of the CaParametros property to query
		  @param value the property value to match
		  	  @return List<CaParametros> found by query
		 */
	    @SuppressWarnings("unchecked")
	    public List<CaParametros> findByProperty(String propertyName, final Object value
	        ) {
	    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaParametrosDaoImpl	::	findByProperty	::	INICIO	::	");
				try {
				final String queryString = "select model from CaParametros model where model." 
				 						+ propertyName + "= :propertyValue";
				Query query = entityManager.createQuery(queryString, CaParametros.class);
				query.setParameter("propertyValue", value);
				LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaParametrosDaoImpl	::	findByProperty	::	FIN	::	");
				return query.getResultList();
			} catch (RuntimeException re) {
				LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaParametrosDaoImpl	::	findByProperty	::	ERROR	::	",re);
				return null;
			}
		}
	    public CaParametros findByCompensAdicioIdTipoCompensId(Long compensAdicioId,Long tipoCompensId){
	    	LOGGER.info("	::	[INF]	::	Buscando por compensAdicioId & tipoCompensId 	::		CaParametrosDaoImpl	::	findByCompensAdicioIdTipoCompensId	::	INICIO	::	");
	    	CaParametros result = null;
	    	try {	    		
	    		final String queryString = "select model from CaParametros model where model." 
						+ COMPENSADICIOID + "= :compensAdicioId and model." + TIPOCOMPENSACIONID + " = :tipoCompensId";
				Query query = entityManager.createQuery(queryString, CaParametros.class);
				query.setParameter("compensAdicioId", compensAdicioId);
				query.setParameter("tipoCompensId", tipoCompensId);				
				result = (CaParametros) query.getSingleResult();
			} catch (Exception e) {
				LOGGER.error("	::	[ERR]	::	Buscando por compensAdicioId & tipoCompensId 	::		CaParametrosDaoImpl	::	findByCompensAdicioIdTipoCompensId	::	ERROR	::	",e);
				result = null;
			}
			LOGGER.info("	::	[INF]	::	Buscando por compensAdicioId & tipoCompensId 	::		CaParametrosDaoImpl	::	findByCompensAdicioIdTipoCompensId	::	FIN	::	");
			return result;
	    }
		public List<CaParametros> findByMontoutilidades(Object montoutilidades
		) {
			return findByProperty(MONTOUTILIDADES, montoutilidades
			);
		}
		
		public List<CaParametros> findByPorcentajepago(Object porcentajepago
		) {
			return findByProperty(PORCENTAJEPAGO, porcentajepago
			);
		}
		
		public List<CaParametros> findByMontopago(Object montopago
		) {
			return findByProperty(MONTOPAGO, montopago
			);
		}
		
		public List<CaParametros> findByPorcentajeagente(Object porcentajeagente
		) {
			return findByProperty(PORCENTAJEAGENTE, porcentajeagente
			);
		}
		
		public List<CaParametros> findByPorcentajepromotor(Object porcentajepromotor
		) {
			return findByProperty(PORCENTAJEPROMOTOR, porcentajepromotor
			);
		}
		
		public List<CaParametros> findByEmisioninterna(Object emisioninterna
		) {
			return findByProperty(EMISIONINTERNA, emisioninterna
			);
		}
		
		public List<CaParametros> findByEmisionexterna(Object emisionexterna
		) {
			return findByProperty(EMISIONEXTERNA, emisionexterna
			);
		}
		
		public List<CaParametros> findByParametroactivo(Object parametroactivo
		) {
			return findByProperty(PARAMETROACTIVO, parametroactivo
			);
		}
			
		public List<CaParametros> findByTopemaximo(Object topemaximo
		) {
			return findByProperty(TOPEMAXIMO, topemaximo
			);
		}
		
		public List<CaParametros> findByUsuario(Object usuario
		) {
			return findByProperty(USUARIO, usuario
			);
		}
		
		public List<CaParametros> findByBorradologico(Object borradologico
		) {
			return findByProperty(BORRADOLOGICO, borradologico
			);
		}
		
		
		/**
		 * Find all CaParametros entities.
		  	  @return List<CaParametros> all CaParametros entities
		 */
		@SuppressWarnings("unchecked")
		public List<CaParametros> findAll(
			) {
			LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaParametrosDaoImpl	::	findAll	::	INICIO	::	");
				try {
				final String queryString = "select model from CaParametros model";
				Query query = entityManager.createQuery(queryString);
				LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaParametrosDaoImpl	::	findAll	::	FIN	::	");
				return query.getResultList();
			} catch (RuntimeException re) {
				LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaParametrosDaoImpl	::	findAll	::	ERROR	::	",re);
				return null;
			}
		}	
		public List<CaParametros> findAllbyCompensadicioId(BigDecimal compensAdicioId) {
			return findByProperty(COMPENSADICIOID, compensAdicioId);
		}
		@SuppressWarnings("unchecked")
		public CaParametros findByTipocompeidCompeIdPersoId(CaTipoCompensacion tipoCompensacion,CaCompensacion compensacion,CaEntidadPersona entidadPersona){
			
			LOGGER.info(">> findByTipocompeidCompeIdPersoId()");
			
			CaParametros caParametros = null;
			
			try {
				
				StringBuilder queryString = new StringBuilder("SELECT model FROM CaParametros model ");
				queryString.append(" WHERE model.").append(COMPENSADICIOID).append(" = :compensAdicioId");
				queryString.append(" AND model.").append(TIPOCOMPENSACIONID).append("  = :tipoCompensId ");
				queryString.append(" AND model.").append(ENTIDADPERSONAID).append(" = :entidadPersonaId ");
					
				Query query = entityManager.createQuery(queryString.toString(), CaParametros.class);
				query.setParameter("tipoCompensId", tipoCompensacion.getId());
				query.setParameter("compensAdicioId", compensacion.getId());
				query.setParameter("entidadPersonaId", entidadPersona.getId());				
				
				List<CaParametros> result = query.getResultList();
				
				if(!result.isEmpty()){
					return result.get(0);
				}
				
			} catch (RuntimeException re) {
				LOGGER.error("Información del Error", re);
			}
			
			LOGGER.info("<< findByTipocompeidCompeIdPersoId()");
			return caParametros;
		}
		
		public List<CaParametros> findAllbyCompensacionEntidadpersona(CaCompensacion compensacion,CaEntidadPersona entidadPersona){
			LOGGER.info("	::	[INF]	::	Buscando por  Compensacion & EntidadPersona 	::		CaParametrosDaoImpl	::	findAllbyCompensacionEntidadpersona	::	INICIO	::	");
			List<CaParametros> result;
			try {
				final String queryString = "select model from CaParametros model where model." 
					+ COMPENSADICIOID + "= :compensAdicioId and model." + ENTIDADPERSONAID + " = :entidadPersonaId"; 
			Query query = entityManager.createQuery(queryString, CaParametros.class);
			query.setParameter("compensAdicioId", compensacion.getId());
			query.setParameter("entidadPersonaId", entidadPersona.getId());
			LOGGER.info("	::	[INF]	::	Se busco por Compensacion & EntidadPersona 	::		CaParametrosDaoImpl	::	findAllbyCompensacionEntidadpersona	::	FIN	::	");
			result = query.getResultList();
			return result;			
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Compensacion & EntidadPersona 	::		CaParametrosDaoImpl	::	findAllbyCompensacionEntidadpersona	::	ERROR	::	",re);
		}
		return null;
		}
				
		@Override
		public List<ReporteNegCompensacionesDTO> findByIdNegocio(Long idNegocio) {
			LOGGER.info("Obteniendo idNegocio() IdNegocio => {}",idNegocio);
			List<ReporteNegCompensacionesDTO> lista = new ArrayList <ReporteNegCompensacionesDTO>();
			try {
				 final StringBuilder queryString = new StringBuilder(200);
			      queryString.append(" SELECT ");
			      queryString.append(" A.TIPOPERSONA||  '-' || A.ID  AS BENEFICIARIO,PORCENTAJE, NVL(MONTOPAGO,0),NOMBRE  ");
			      queryString.append(" FROM (SELECT tien.NOMBRE AS TIPOPERSONA, ");
				  queryString.append(" CASE WHEN tien.NOMBRE='AGENTE' THEN  EP.TOAGENTE_ID  ");			  
				  queryString.append(" WHEN tien.NOMBRE='PROMOTOR' THEN ( SELECT IDPROMOTORIA FROM MIDAS.TOAGENTE WHERE ID=EP.TOAGENTE_ID ) ");			  
				  queryString.append(" WHEN tien.NOMBRE='PROVEEDOR' THEN  EP.PROVEEDOR_ID ");
				  queryString.append(" END  AS ID, ");	
				  queryString.append(" CASE WHEN tien.NOMBRE='AGENTE' THEN (NVL(caparam.PORCENTAJEAGENTE,0)*0.01)* NVL(caparam.PORCENTAJEPAGO,0) ");	
				  queryString.append(" WHEN tien.NOMBRE='PROMOTOR' THEN (NVL(caparam.PORCENTAJEPROMOTOR,0)*0.01)* NVL(caparam.PORCENTAJEPAGO,0) ");	
				  queryString.append(" WHEN tien.NOMBRE='PROVEEDOR' THEN caparam.PORCENTAJEPAGO ");	
				  queryString.append(" END  AS PORCENTAJE, ");	
				  queryString.append(" caparam.PORCENTAJEAGENTE,caparam.PORCENTAJEPROMOTOR,caparam.PORCENTAJEPAGO, caparam.MONTOPAGO, tipoca.NOMBRE ");	
				  queryString.append(" FROM MIDAS.ca_parametros caparam ");	
				  queryString.append(" INNER JOIN MIDAS.ca_compensacion cacom ON(cacom.id = caparam.compensacion_id) ");	
				  queryString.append(" INNER JOIN MIDAS.tonegocio neg ON(neg.idtonegocio = cacom.negocio_id) ");	
				  queryString.append(" INNER JOIN MIDAS.ca_tipocompensacion tipoca ON(tipoca.id = caparam.tipocompensacion_id) ");	
				  queryString.append(" INNER JOIN MIDAS.ca_entidadpersona ep ON(ep.id = caparam.entidadpersona_id) ");	
				  queryString.append(" INNER JOIN MIDAS.toagente agente ON(agente.id = ep.toagente_id) ");	
				  queryString.append(" INNER JOIN MIDAS.ca_tipoentidad tien ON(tien.id = ep.tipoentidad_id) ");
				  queryString.append(" WHERE cacom.negocio_id = ?1 ");
				  queryString.append(" AND tipoca.valor IN (1,3) ");
				  queryString.append(" UNION ");
				  queryString.append(" SELECT ");
				  queryString.append(" CASE WHEN NVL(caparam.PORCENTAJEPROMOTOR,0)>0 THEN 'PROMOTOR'  END AS TIPOPERSONA, ");
				  queryString.append(" CASE WHEN NVL(caparam.PORCENTAJEPROMOTOR,0)>0 THEN ");
				  queryString.append(" (SELECT IDPROMOTORIA FROM MIDAS.TOAGENTE WHERE ID=EP.TOAGENTE_ID) ");
				  queryString.append(" END  AS ID,");
				  queryString.append(" CASE WHEN NVL(caparam.PORCENTAJEPROMOTOR,0)>0 ");
				  queryString.append(" THEN (NVL(caparam.PORCENTAJEPROMOTOR,0)*0.01)* NVL(caparam.PORCENTAJEPAGO,0) ");
				  queryString.append(" END  AS PORCENTAJE , ");
				  queryString.append(" caparam.PORCENTAJEAGENTE,caparam.PORCENTAJEPROMOTOR,caparam.PORCENTAJEPAGO, caparam.MONTOPAGO, tipoca.NOMBRE ");
				  queryString.append(" FROM MIDAS.ca_parametros caparam ");
				  queryString.append(" INNER JOIN MIDAS.ca_compensacion cacom ON(cacom.id = caparam.compensacion_id) ");
				  queryString.append(" INNER JOIN MIDAS.tonegocio neg ON(neg.idtonegocio = cacom.negocio_id) ");
				  queryString.append(" INNER JOIN MIDAS.ca_tipocompensacion tipoca ON(tipoca.id = caparam.tipocompensacion_id) ");
				  queryString.append(" INNER JOIN MIDAS.ca_entidadpersona ep ON(ep.id = caparam.entidadpersona_id) ");
				  queryString.append(" INNER JOIN MIDAS.toagente agente ON(agente.id = ep.toagente_id) ");
				  queryString.append(" INNER JOIN MIDAS.ca_tipoentidad tien ON (tien.id = ep.tipoentidad_id )");
				  queryString.append(" WHERE  NVL(caparam.PORCENTAJEPROMOTOR,0)>0 ");
				  queryString.append(" AND cacom.negocio_id = ?1");
				  queryString.append(" AND tipoca.valor IN (1,3) ");
				  queryString.append(" ) A ");
				  Query query=entityManager.createNativeQuery(queryString.toString());
				  query.setParameter(1, idNegocio);				  

				  List<Object[]> resultList=query.getResultList();	
				  LOGGER.info("Orden IdNegocio size : ="+resultList.size());
			      LOGGER.info("query="+query.toString());
			      for(Object[] result : resultList){
			    	  			    	  
			    	  LOGGER.info("Orden IdNegocio size result :  ="+result.length);  	  			    	  
			    	  ReporteNegCompensacionesDTO reporteNegCompensacionesDTO = new ReporteNegCompensacionesDTO();
			    	  reporteNegCompensacionesDTO.setTipoBeneficiario(result[0].toString());
			    	  reporteNegCompensacionesDTO.setPorcentajePago(Utilerias.obtenerBigDecimal(result[1]));
			    	  reporteNegCompensacionesDTO.setMontoPago(Utilerias.obtenerBigDecimal(result[2])); 
			    	  reporteNegCompensacionesDTO.setTipoCompensacion(result[3].toString());
			    	  lista.add(reporteNegCompensacionesDTO);
			     	 
			      }
			      LOGGER.info(" LISTA"+ lista.size());			  	
			      
			   	}catch (RuntimeException re) {
				LOGGER.error("-- findByIdNegocio()", re);
				
						throw re;
			}		
			   	return lista;
			}

		@Override
		public List<ReporteNegCompensacionesDTO> findByIdNegocionBS(Long idNegocio) {
			LOGGER.info("Obteniendo idNegocio() IdNegocio => {}",idNegocio);
			List<ReporteNegCompensacionesDTO> lista = new ArrayList <ReporteNegCompensacionesDTO>();
			try {
				final StringBuilder queryString = new StringBuilder(200);
				queryString.append(" SELECT A.TIPOPERSONA  || '-' ||A.ID   AS BENEFICIARIO, NVL(VALORMINIMO,0)  AS VALORMINIMO, NVL(VALORMAXIMO,0) AS VALORMAXIMO, ");
				queryString.append(" NVL(VALORCOMPENSACION,0) AS VALORCOMPENSACION, NOMBRE");
				queryString.append(" FROM ( ");
				queryString.append(" SELECT tien.NOMBRE AS TIPOPERSONA,");
				queryString.append(" CASE WHEN tien.NOMBRE='AGENTE' THEN  EP.TOAGENTE_ID ");
				queryString.append(" WHEN tien.NOMBRE='PROMOTOR' THEN (SELECT IDPROMOTORIA FROM MIDAS.TOAGENTE WHERE ID=EP.TOAGENTE_ID) ");
				queryString.append(" WHEN tien.NOMBRE='PROVEEDOR' THEN  EP.PROVEEDOR_ID ");
				queryString.append(" END  AS ID, ");
				queryString.append(" CASE WHEN tien.NOMBRE='AGENTE' THEN (NVL(caparam.PORCENTAJEAGENTE,0)*0.01)* NVL(caran.VALORCOMPENSACION,0) ");
				queryString.append(" WHEN tien.NOMBRE='PROMOTOR' THEN (NVL(caparam.PORCENTAJEPROMOTOR,0)*0.01)* NVL(caran.VALORCOMPENSACION,0) ");
				queryString.append(" WHEN tien.NOMBRE='PROVEEDOR' THEN caran.VALORCOMPENSACION ");
				queryString.append(" END  AS VALORCOMPENSACION,  ");
				queryString.append(" caran.VALORMINIMO, ");
				queryString.append(" caran.VALORMAXIMO, ");
				queryString.append(" caran.VALORCOMPENSACION AS COMPENSACION, ");
				queryString.append(" caparam.ID as idParametro, ");
				queryString.append(" caparam.PORCENTAJEAGENTE, ");
				queryString.append(" caparam.PORCENTAJEPROMOTOR, ");
				queryString.append(" caparam.PORCENTAJEPAGO, "); 
				queryString.append(" tipoca.NOMBRE ");
				queryString.append(" FROM MIDAS.ca_parametros caparam ");
				queryString.append(" INNER JOIN MIDAS.ca_compensacion cacom ON(cacom.id = caparam.compensacion_id) ");
				queryString.append(" INNER JOIN MIDAS.ca_rangos caran ON (caran.parametros_id = caparam.id) ");
				queryString.append(" INNER JOIN MIDAS.tonegocio neg ON(neg.idtonegocio = cacom.negocio_id) ");
				queryString.append(" INNER JOIN MIDAS.ca_tipocompensacion tipoca ON(tipoca.id = caparam.tipocompensacion_id) ");
				queryString.append(" INNER JOIN MIDAS.ca_entidadpersona ep ON(ep.id = caparam.entidadpersona_id) ");
				queryString.append(" INNER JOIN MIDAS.toagente agente ON(agente.id = ep.toagente_id) ");
				queryString.append(" INNER JOIN MIDAS.ca_tipoentidad tien ON(tien.id = ep.tipoentidad_id) ");
				queryString.append(" WHERE cacom.negocio_id = ?1 ");
				queryString.append(" AND tipoca.valor  = 2 ");
				queryString.append(" UNION ");
				queryString.append(" SELECT ");
				queryString.append(" CASE WHEN NVL(caparam.PORCENTAJEPROMOTOR,0)>0 THEN 'PROMOTOR'  END AS TIPOPERSONA, ");
				queryString.append(" CASE WHEN NVL(caparam.PORCENTAJEPROMOTOR,0)>0 THEN ");
				queryString.append(" (SELECT IDPROMOTORIA FROM MIDAS.TOAGENTE WHERE ID=EP.TOAGENTE_ID) ");
				queryString.append(" END  AS ID, ");
				queryString.append(" CASE WHEN NVL(caparam.PORCENTAJEPROMOTOR,0)>0 ");
				queryString.append(" THEN (NVL(caparam.PORCENTAJEPROMOTOR,0)*0.01)* NVL(caran.VALORCOMPENSACION,0) ");
				queryString.append(" END  AS VALORCOMPENSACION,");
				queryString.append(" NVL(caran.VALORMINIMO,0), ");
				queryString.append(" caran.VALORMAXIMO , caran.VALORCOMPENSACION AS COMPENSACION,caparam.ID,caparam.PORCENTAJEAGENTE,caparam.PORCENTAJEPROMOTOR,caparam.PORCENTAJEPAGO,tipoca.NOMBRE ");
				queryString.append(" FROM MIDAS.ca_parametros caparam ");
				queryString.append(" INNER JOIN MIDAS.ca_compensacion cacom ON(cacom.id = caparam.compensacion_id) ");
				queryString.append(" INNER JOIN MIDAS.ca_rangos caran ON (caran.parametros_id = caparam.id) ");				
				queryString.append(" INNER JOIN MIDAS.tonegocio neg ON(neg.idtonegocio = cacom.negocio_id) ");
				queryString.append(" INNER JOIN MIDAS.ca_tipocompensacion tipoca ON(tipoca.id = caparam.tipocompensacion_id) ");				
				queryString.append(" INNER JOIN MIDAS.ca_entidadpersona ep ON(ep.id = caparam.entidadpersona_id) ");
				queryString.append(" INNER JOIN MIDAS.toagente agente ON(agente.id = ep.toagente_id) ");
				queryString.append(" INNER JOIN MIDAS.ca_tipoentidad tien ON(tien.id = ep.tipoentidad_id) ");
				queryString.append(" AND cacom.negocio_id = ?1 ");
				queryString.append(" AND tipoca.valor =2 ");
				queryString.append(" )A ");
				
				Query query=entityManager.createNativeQuery(queryString.toString());
				query.setParameter(1, idNegocio);
				  //query.setMaxResults(100);

				List<Object[]> resultList=query.getResultList();	
				LOGGER.info("Orden IdNegocio size : ="+resultList.size());
			    LOGGER.info("query="+query.toString());
			    for(Object[] result : resultList){
			    				    	  
			    	LOGGER.info("Orden IdNegocio size result :  ="+result.length);  	  			    	  
			    	ReporteNegCompensacionesDTO reporteNegCompensacionesDTO = new ReporteNegCompensacionesDTO();
			    	reporteNegCompensacionesDTO.setTipoBeneficiario(result[0].toString());
			    	reporteNegCompensacionesDTO.setValorMinimo(Utilerias.obtenerBigDecimal(result[1]));
			    	reporteNegCompensacionesDTO.setValorMaximo(Utilerias.obtenerBigDecimal(result[2])); 
			    	reporteNegCompensacionesDTO.setValorCompensacion(Utilerias.obtenerBigDecimal(result[3]));
			    	reporteNegCompensacionesDTO.setTipoCompensacion(result[4].toString());
			    	
			    	lista.add(reporteNegCompensacionesDTO);
			     	 
			      }
			    LOGGER.info(" LISTA BS = "+ lista.size());
			    
				
			} catch (RuntimeException re) {
				LOGGER.error("-- findByIdNegocionBS()", re);
				// TODO: handle exception
			}
			return lista;
			// TODO Auto-generated method stub
			
		}
		
	}