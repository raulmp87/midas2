package mx.com.afirme.midas2.dao.impl.siniestros.recuperacion.ingresos;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.ingresos.IngresoDevolucionDao;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.IngresoDevolucionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoDevolucionService.IngresoDevolucionFiltro;

@Stateless
public class IngresoDevolucionDaoImpl implements IngresoDevolucionDao {

	@EJB
	EntidadService entidadService;
	@EJB
	EntidadDao entidadDao;
	
	@Override
	public List<IngresoDevolucionDTO> buscar(IngresoDevolucionFiltro filtro){
		List<IngresoDevolucionDTO> lista  = new ArrayList<IngresoDevolucionDTO>();

		String spName = "MIDAS.PKGSIN_RECUPERACIONES.buscarIngresoDevolucion"; 
        StoredProcedureHelper storedHelper 			= null;
        try {
      	  	String propiedades = "id,cuentaAcreedoraStr,tipoRecuperacion,tipoDevolucion,siniestroOrigen,motivoCancelacion,formaPago," +
      	  						 "numSolicitud,solicitadoPor,autorizadoPor,numChequeRef,fechaIngreso,fechaSolicitud," +
      	  						 "fechaAutorizacion,fechaCheque,importe,estatus";
   	  	
      	  	String columnasBaseDatos = "ID,CUENTA_ACREEDORA,TIPO,TIPO_DEVOLUCION,SINIESTRO_ORIGEN,MOTIVO_CANCELACION,FORMA_PAGO," +
      	  							   "SOLICITUDCHEQUE_ID,USUARIO_SOLICITA,USUARIO_AUTORIZARECHAZA,CHEQUE_REF,FECHA_INGRESO,FECHA_SOLICITA," +
      	  							   "FECHA_AUTORIZARECHAZA,FECHA_CHEQUE_REF,IMPORTE_TOTAL,ESTATUS";
      	  	storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(IngresoDevolucionDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
            agregaParametrosDelFiltro(storedHelper, filtro);
            lista = storedHelper.obtieneListaResultados();
        } catch (Exception e) {
             String descErr = null;
             if (storedHelper != null) {
                   descErr = storedHelper.getDescripcionRespuesta();
                   LogDeMidasInterfaz.log(
                               "Excepcion general en busqueda de IngresoDevolucion ..."+descErr
                                           , Level.WARNING, e);    
             }
        }
		return lista;
	}


	public Short solicitarCancelacionCheque(Long solicitudId){
		
		Short resultado = 0;
		
		String spNameSolicitar = "dbo.spCancelaSolicitud";
		//String spNameCancelar = "MIDAS.PKG_LIQUIDACIONES_SINIESTROS.SOLICITARCANCELARDEVINGRESO";

		try {
			StoredProcedureHelper storedHelper = null;
			try {
				LogDeMidasInterfaz.log("Entrando a dbo.spCancelaSolicitud..." + this, Level.INFO, null);
				storedHelper = new StoredProcedureHelper(spNameSolicitar,StoredProcedureHelper.DATASOURCE_MIZAR);
				storedHelper.estableceParametro("Idsolicitud", solicitudId);
				resultado = (short) storedHelper.ejecutaActualizarSQLServer();
				//resultado = RESULTADO_OK;
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en IngresoDevolucionDaoImpl.solicitarCancelacionCheque..." + this, Level.WARNING, e);
				resultado = RESULTADO_ERROR;
			}	
				
			/*if (resultado == RESULTADO_OK) {
				storedHelper = new StoredProcedureHelper(spNameCancelar, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("p_solicitudId", solicitudId);
				resultado = (short) storedHelper.ejecutaActualizar();
			resultado = RESULTADO_OK;
				
			}*/

		} catch (Exception e) {
			resultado = RESULTADO_ERROR;
		}
		
		return resultado;
		
	}

	private void agregaParametrosDelFiltro(StoredProcedureHelper storedHelper, IngresoDevolucionFiltro filtro){
		
		storedHelper.estableceParametro("p_cuentaAcreedora", 		this.convertToNull(filtro.getCuentaAcreedora()));
		storedHelper.estableceParametro("p_siniestroOrigen", 		this.convertToNull(filtro.getSiniestroOrigen()));
		storedHelper.estableceParametro("p_importeDesde", 			this.convertToNull(filtro.getImporteDesde()));
		storedHelper.estableceParametro("p_importeHasta", 			this.convertToNull(filtro.getImporteHasta()));
		storedHelper.estableceParametro("p_fechaSolicitudDesde", 	this.convertToNull(filtro.getFechaSolicitudDesde()));
		storedHelper.estableceParametro("p_fechaSolicitudHasta", 	this.convertToNull(filtro.getFechaSolicitudHasta()));
		storedHelper.estableceParametro("p_fechaAutorizacionDesde", this.convertToNull(filtro.getFechaAutorizacionDesde()));
		storedHelper.estableceParametro("p_fechaAutorizacionHasta", this.convertToNull(filtro.getFechaAutorizacionHasta()));
		storedHelper.estableceParametro("p_fechaChequeDesde", 		this.convertToNull(filtro.getFechaChequeDesde()));
		storedHelper.estableceParametro("p_fechaChequeHasta", 		this.convertToNull(filtro.getFechaChequeHasta()));
		storedHelper.estableceParametro("p_motivoCancelacion", 		this.convertToNull(filtro.getMotivoCancelacion()));
		storedHelper.estableceParametro("p_numChequeReferencia", 	this.convertToNull(filtro.getNumChequeReferencia()));
		storedHelper.estableceParametro("p_tipoDevolucion", 		this.convertToNull(filtro.getTipoDevolucion()));
		storedHelper.estableceParametro("p_estatus", 				this.convertToNull(filtro.getEstatus()));
		storedHelper.estableceParametro("p_tipoRecuperacion", 		this.convertToNull(filtro.getTipoRecuperacion()));
		storedHelper.estableceParametro("p_solicitadoPor", 			this.convertToNull(filtro.getSolicitadoPor()));
		storedHelper.estableceParametro("p_autorizadoPor", 			this.convertToNull(filtro.getAutorizadoPor()));
		storedHelper.estableceParametro("p_formaPago", 				this.convertToNull(filtro.getFormaPago()));
		storedHelper.estableceParametro("p_numSolicitud", 			this.convertToNull(filtro.getNumSolicitud()));
		storedHelper.estableceParametro("p_Tipobusqueda", 			this.convertToNull(filtro.getTipoBusqueda()));

	}
	
	private Object convertToNull(Object obj){
		if( obj instanceof String){
			obj = (obj != null && ((String)obj).equals(""))?null:obj;
		}else if ( obj instanceof Boolean){
			obj = (obj != null && ((Boolean)obj)== Boolean.FALSE)?null:obj;
		}
		return obj;
	}


}