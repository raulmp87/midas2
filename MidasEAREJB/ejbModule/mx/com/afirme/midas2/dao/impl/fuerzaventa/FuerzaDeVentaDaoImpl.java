package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.fuerzaventa.FuerzaDeVentaDao;
import mx.com.afirme.midas2.dao.impl.SPDao;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;

@Stateless
public class FuerzaDeVentaDaoImpl extends SPDao implements
		FuerzaDeVentaDao {
	
	public static final String SP_LISTAR_AGENTES = "SEYCOS.PKG_INT_MIDAS_E2.cListaEstFV";
	
	
	@Override
	public List<RegistroFuerzaDeVentaDTO> listar(
			Object idCentroOperacion, Object idGerencia, Object idOficina,
			Object idPromotoria, String tipo) {
		List<RegistroFuerzaDeVentaDTO> lista = new ArrayList<RegistroFuerzaDeVentaDTO>();
		try {
			StoredProcedureHelper helper = new StoredProcedureHelper(
					SP_LISTAR_AGENTES, StoredProcedureHelper.DATASOURCE_MIDAS);			
			helper.estableceParametro("pId_Centro_oper", idCentroOperacion);
			helper.estableceParametro("pId_Gerencia", idGerencia);
			helper.estableceParametro("pid_oficina", idOficina);
			helper.estableceParametro("pid_promotoria", idPromotoria);
			helper.estableceParametro("pNivelDet", tipo);
			helper.estableceMapeoResultados(
					RegistroFuerzaDeVentaDTO.class.getCanonicalName(),
					new String[] { "id", "description" }, 
					new String[] { "id", "descripcion" });
			lista = helper.obtieneListaResultados();
			//lista = getDummies(tipo);
		} catch (Exception ex) {
			throw new RuntimeException("Imposible obtener la lista de agentes.", ex);
		}
		return lista;
	}
	
	private List<RegistroFuerzaDeVentaDTO> getDummies(String tipo){
		List<RegistroFuerzaDeVentaDTO> lista = new ArrayList<RegistroFuerzaDeVentaDTO>();
		RegistroFuerzaDeVentaDTO dto  =null;
		if(tipo.equals("G")){
			dto = new RegistroFuerzaDeVentaDTO();
			dto.setId(1);
			dto.setDescription("GERENCIA 1");
			lista.add(dto);
			dto = new RegistroFuerzaDeVentaDTO();
			dto.setId(2);
			dto.setDescription("GERENCIA 2");
			lista.add(dto);
		}else if(tipo.equals("O")){
			dto = new RegistroFuerzaDeVentaDTO();
			dto.setId(1);
			dto.setDescription("OFICINA 1");
			lista.add(dto);
		}else if(tipo.equals("P")){
			dto = new RegistroFuerzaDeVentaDTO();
			dto.setId(1);
			dto.setDescription("PROMOTORIA 1");
			lista.add(dto);
		}
		return lista;
	}

}
