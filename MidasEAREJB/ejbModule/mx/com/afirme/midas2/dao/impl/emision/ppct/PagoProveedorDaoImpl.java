package mx.com.afirme.midas2.dao.impl.emision.ppct;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.setQueryParametersByProperties;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.componente.scrollable.ScrollableNavigationDao;
import mx.com.afirme.midas2.dao.emision.ppct.PagoProveedorDao;
import mx.com.afirme.midas2.domain.contabilidad.SolicitudCheque;
import mx.com.afirme.midas2.domain.emision.ppct.CoberturaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.OrdenPago;
import mx.com.afirme.midas2.domain.emision.ppct.Proveedor;
import mx.com.afirme.midas2.domain.emision.ppct.Recibo;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboProveedor;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.emision.ppct.SaldoDTO;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class PagoProveedorDaoImpl implements PagoProveedorDao {

	@SuppressWarnings("rawtypes")
	@Override
	public BigDecimal obtenerImportePreeliminar(String sessionId) {
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String queryString = "SELECT SUM(recibo.monto) FROM ReciboProveedor recibo" +
				" WHERE recibo.sesion = :sessionId" + 
				" AND recibo.seleccionado = :seleccionado";
		
		Utilerias.agregaHashLista(listaParametrosValidos, "sessionId", sessionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", false);
		
		Query query = entityManager.createQuery(queryString);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
				
		return (BigDecimal)query.getSingleResult();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public OrdenPago guardarOrdenPago(OrdenPago ordenPago) {
		
		if (ordenPago.getId() == null) {
			
			if (ordenPago.getFolio() == null) {
				ordenPago.setFolio(obtenerFolioNuevo());
			}
			
			ordenPago.setId((BigDecimal)entidadDao.persistAndReturnId(ordenPago));
			ordenPago = entidadDao.getReference(OrdenPago.class, ordenPago.getId());
			
		} else {
			
			ordenPago = entidadDao.update(ordenPago);
							
		}
				
		return ordenPago;
		
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BigDecimal asignarRecibosPreeliminares(Proveedor proveedor, Date fechaCorte, String sessionId) {
		
		//Se buscan todos los recibos pagados hasta la fecha de corte que no pertenezcan a una Orden de Pago ni esten editandose en este momento
		//Los marca como 'en edicion' usando el sessionId
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String queryString = "UPDATE ReciboProveedor recibo SET recibo.sesion = :sessionId, recibo.seleccionado = :seleccionado" +
				" WHERE recibo.configuracionPago.proveedor.id = :proveedorId" +
				" AND recibo.reciboOriginal.fechaCreacion <= :fechaCorte" +
				" AND recibo.ordenPago IS NULL" +
				" AND recibo.sesion IS NULL";
		
		Utilerias.agregaHashLista(listaParametrosValidos, "sessionId", sessionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", false);
		Utilerias.agregaHashLista(listaParametrosValidos, "proveedorId", proveedor.getId());
		Utilerias.agregaHashLista(listaParametrosValidos, "fechaCorte", fechaCorte);
				
		Query query = entityManager.createQuery(queryString);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
				
		return new BigDecimal(query.executeUpdate());
		
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void desasignarRecibosPreeliminares(String sessionId) {
		
		//Quita el marcado de 'en edicion'
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String queryString = "UPDATE ReciboProveedor recibo SET recibo.sesion = NULL, recibo.seleccionado = :seleccionado" +
				" WHERE recibo.sesion = :sessionId";
		
		Utilerias.agregaHashLista(listaParametrosValidos, "sessionId", sessionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", false);

		Query query = entityManager.createQuery(queryString);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.executeUpdate();
	}
		
	@SuppressWarnings("rawtypes")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void asignarRecibos(OrdenPago ordenPago, String sessionId) {
		
		//Se asignan los recibos a la Orden de Pago (solo los que quedaron sin seleccionar y pertenecen a la sesion en progreso)
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String queryString = "UPDATE ReciboProveedor recibo SET recibo.ordenPago = :ordenPago" +
				" WHERE recibo.sesion = :sessionId" +
				" AND recibo.seleccionado = :seleccionado";
		
		Utilerias.agregaHashLista(listaParametrosValidos, "ordenPago", ordenPago);
		Utilerias.agregaHashLista(listaParametrosValidos, "sessionId", sessionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", false);
		
		Query query = entityManager.createQuery(queryString);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
				
		query.executeUpdate();
	
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SolicitudCheque obtenerSolicitudCheque(OrdenPago ordenPago) {
		
		StoredProcedureHelper storedHelper = null;
		int idSolicitudCheque = 0;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_CONTABILIDAD.SP_GENERA_CHEQUE_PPCT", StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdOrdenPago", ordenPago.getId());
			
			idSolicitudCheque = storedHelper.ejecutaActualizar();
			
			return entidadDao.findById(SolicitudCheque.class, new BigDecimal(idSolicitudCheque));
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public OrdenPago obtenerOrdenPago(OrdenPago filtro) {
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String queryString = "SELECT model FROM OrdenPago model join fetch model.solicitudCheque join fetch model.proveedor " +
				" where model.id = :id";
		
		if(filtro.getId() != null) {
			Utilerias.agregaHashLista(listaParametrosValidos, "id", filtro.getId());
		}
				
		Query query = entityManager.createQuery(queryString);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return (OrdenPago)query.getSingleResult();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CoberturaSeycos> obtenerCoberturasProveedor(Proveedor proveedor, String claveNegocio) {
		
		String queryString = "SELECT " +
		"    COB.ID_COBERTURA, COB.NOM_COBERTURA " +
		"FROM " +
		"    SEYCOS.DP_COBERTURA COB " +
		"    INNER JOIN (MIDAS.PPCT_CONF_COBERTURA_DET CONFDET " +
		"                INNER JOIN MIDAS.PPCT_CONF_COBERTURA CONF " +
		"                ON CONF.ID = CONFDET.PPCT_CONF_COBERTURA_ID " +
		"    ) ON CONFDET.ID_COBERTURA = COB.ID_COBERTURA " +
		"WHERE " +
		"    CONF.PPCT_PROVEEDOR_ID = " + proveedor.getId() +
		"    AND COB.B_COB_TERCEROS = 'V' " +
		"    AND COB.ID_COBERTURA IN ( " +
		"        SELECT DISTINCT ID_COBERTURA FROM SEYCOS.DP_LIN_COB WHERE " + 
		"            ('" + claveNegocio + "' = 'A' AND ID_RAMO_CONTABLE = '04') OR " +
		"            ('" + claveNegocio + "' = 'D' AND ID_RAMO_CONTABLE NOT IN ('04','13','14')) " +
		"    ) " +
		"GROUP BY " +
		"    COB.ID_COBERTURA, COB.NOM_COBERTURA " +    
		"ORDER BY " +
		"    COB.NOM_COBERTURA "; 
		
		
		Query query = entityManager.createNativeQuery(queryString, "CoberturaSeycosResult");
		
		return query.getResultList();
		
	}
	
	@Override
	public List<Recibo> obtenerRecibosPreeliminares(ReciboProveedor filtro) {
		
		return getConsulta("ReciboProveedor", filtro);
		
	}
	
	@Override
	public List<Recibo> obtenerRecibosOrdenPago(ReciboProveedor filtro) {
		
		return getConsulta("ReciboProveedorHistorico", filtro);
		
	}
	
	@Override
	public List<OrdenPago> obtenerOrdenesPago(OrdenPago filtro) {
		
		Query query = null;
		String queryBody = null;
		String queryString = null;
		String sWhere = null;
		Map<String,Object> params = new HashMap<String, Object>();
				
		queryBody = "select model from OrdenPago model join fetch model.solicitudCheque join fetch model.proveedor";
		
		sWhere = generateOPWhereString(filtro, params);
		
		queryString = queryBody + sWhere;
		
		query = entityManager.createQuery(queryString);
		
		setQueryParametersByProperties(query, params);						
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return scrollableNavigationDao.getResultList(query, filtro);

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SaldoDTO> obtenerDetalleReporteSaldos(SaldoDTO filtro) {
		
		StoredProcedureHelper storedHelper = null;
		List<SaldoDTO> respuesta = new ArrayListNullAware<SaldoDTO>();
		
		try {
			String [] atributos = {
					"periodo",
					"proveedor",
					"poliza",
					"endoso",
					"inciso",
					"cobertura",
					"recibo",
					"fechaInicioVigencia",
					"fechaFinVigencia",
					"fechaCorte",
					"saldo",
					"moneda",
					"referenciaContable"
			};
			
			String [] columnasResultSet = {
					"PERIODO",
					"PROVEEDOR",
					"NUMERO_POLIZA",
					"NUMERO_ENDOSO",
					"NUMERO_INCISO",
					"COBERTURA",
					"RECIBO",
					"INICIO_VIGENCIA",
					"FIN_VIGENCIA",
					"FECHA_CORTE",
					"SALDO",
					"ID_MONEDA",
					"REFERENCIA_CONT"
			};
			
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_PPCT.SP_OBTIENE_DETALLE_SALDOS", StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceMapeoResultados(SaldoDTO.class.getCanonicalName(), atributos, columnasResultSet);
			
			storedHelper.estableceParametro("pPeriodo", filtro.getPeriodo());
			storedHelper.estableceParametro("pClaveNegocio", filtro.getClaveNegocio());
			
			respuesta = storedHelper.obtieneListaResultados();
			
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return respuesta;
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<SaldoDTO> obtenerResumenReporteSaldos(SaldoDTO filtro) {
		
		StoredProcedureHelper storedHelper = null;
		List<SaldoDTO> respuesta = new ArrayListNullAware<SaldoDTO>();
		
		try {
			String [] atributos = {
					"periodo",
					"proveedor",
					"cobertura",
					"saldo",
					"moneda"
			};
			
			String [] columnasResultSet = {
					"PERIODO",
					"PROVEEDOR",
					"COBERTURA",
					"SALDO",
					"ID_MONEDA"
			};
			
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKG_PPCT.SP_OBTIENE_RESUMEN_SALDOS", StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceMapeoResultados(SaldoDTO.class.getCanonicalName(), atributos, columnasResultSet);
			
			storedHelper.estableceParametro("pPeriodo", filtro.getPeriodo());
			storedHelper.estableceParametro("pClaveNegocio", filtro.getClaveNegocio());
			
			respuesta = storedHelper.obtieneListaResultados();
			
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		return respuesta;
		
	}
		
	private BigDecimal obtenerFolioNuevo() {
		
		String queryString = "SELECT MIDAS.PPCT_FOLIO_ORDEN_PAGO_SEQ.NEXTVAL FROM DUAL";
		
		Query query = entityManager.createNativeQuery(queryString);
						
		return (BigDecimal) query.getSingleResult();
		
	}
	
	private List<Recibo> getConsulta(String modelo, ReciboProveedor filtro){
		
		Query query = null;
		String queryBody = null;
		String queryString = null;
		String sWhere = null;
		Map<String,Object> params = new HashMap<String, Object>();
				
		queryBody = "select model from " + modelo + " model "
        	+ " join fetch model.reciboOriginal join fetch model.cobertura join fetch model.configuracionPago.proveedor left join fetch model.ordenPago";
					
		sWhere = generateReciboWhereString(filtro, params);
		
		queryString = queryBody + sWhere;
		
		query = entityManager.createQuery(queryString);
		
		setQueryParametersByProperties(query, params);						
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return scrollableNavigationDao.getResultList(query, (Recibo)filtro);

	}
	
	/**
	 * Genera los Filtros para las listas de Recibos
	 * @param filtro
	 * @param params
	 * @return
	 */
	private String generateReciboWhereString(ReciboProveedor filtro, Map<String,Object> params) {
		String sWhere = "";
		String complement = "";
		String clase = "";
		
		if(filtro.getNumeroEndoso()!=null){
			sWhere = (!sWhere.isEmpty()?" and ":" ") + " model.numeroEndoso = :endoso ";
			params.put("endoso", filtro.getNumeroEndoso());
			complement = "";
		}
		
		Date fechaFin = filtro.getRangoFechas().getFechaFin();
		Date fechaInicio = filtro.getRangoFechas().getFechaInicio();
		
		if(fechaFin!=null && fechaInicio!=null){
			complement = (!sWhere.isEmpty()?" and ":" ")+ "(model.reciboOriginal.fechaInicioVigencia BETWEEN :fechaInicio AND :fechaFin) ";
			params.put("fechaFin", fechaFin);
			params.put("fechaInicio", fechaInicio);
			sWhere += complement; 
			complement="";
		}else if(fechaFin!=null ){
			complement = (!sWhere.isEmpty()?" and ":" ")+ "model.reciboOriginal.fechaInicioVigencia <= :fechaFin ";
			params.put("fechaFin", fechaFin);
			sWhere += complement; 
			complement="";
		}else if(fechaInicio!=null){
			complement = (!sWhere.isEmpty()?" and ":" ")+ "model.reciboOriginal.fechaInicioVigencia >= :fechaInicio ";
			params.put("fechaInicio", fechaInicio);
			sWhere += complement;
			complement="";
		}
		
		//poliza
		if(filtro.getNumeroPoliza()!=null && !filtro.getNumeroPoliza().trim().isEmpty()){
			complement = (!sWhere.isEmpty()?" and ":" ")+ "model.numeroPoliza LIKE '%" + filtro.getNumeroPoliza() + "%' ";
			sWhere += complement;
			complement = "";
		}
			
			//Cobertura
			CoberturaSeycos cobertura = filtro.getCobertura();
			if(isNotNull(cobertura)){
				clase = "cobertura";
				if(cobertura.getId()!=null){
					complement = (!sWhere.isEmpty()?" and ":" ")+ "model." + clase + ".id = :idCobertura ";
					params.put("idCobertura", cobertura.getId());
					sWhere += complement; 
					complement="";
				}				
			}
			
			//OrdenPago
			OrdenPago ordenPago = filtro.getOrdenPago();
			if(isNotNull(ordenPago)){
				if(ordenPago.getId()!=null){
					clase = "ordenPago";
					complement = (!sWhere.isEmpty()?" and ":" ")+ "model." + clase + ".id = :idOrdenPago ";
					params.put("idOrdenPago", ordenPago.getId());
					sWhere += complement;
					complement="";
				}
			}
			
			//sesion
			if((filtro.getOrdenPago() == null || filtro.getOrdenPago().getId() == null) && filtro.getSesion() != null && !filtro.getSesion().equals("")){
				sWhere = sWhere + (!sWhere.isEmpty()?" and ":"")+ " model.sesion = :sesion ";
				params.put("sesion", filtro.getSesion());
				complement = "";
			}
			
			//proveedor
			if(filtro.getConfiguracionPago() != null && filtro.getConfiguracionPago().getProveedor() != null && filtro.getConfiguracionPago().getProveedor().getId() != null) {
				sWhere += (!sWhere.isEmpty()?" and ":" ") + "model.configuracionPago.proveedor.id = :idProveedor";
				params.put("idProveedor", filtro.getConfiguracionPago().getProveedor().getId());
			}
			

			if(!sWhere.isEmpty()){
				sWhere =" where "+ sWhere;
			}
							
		return sWhere;
	}
	
	/**
	 * Genera los Filtros para las listas de Orden de Pago
	 * @param filtro
	 * @param params
	 * @return
	 */
	private String generateOPWhereString(OrdenPago filtro, Map<String,Object> params) {
		
		String sWhere = "";
		
		//id
		if(filtro.getId() != null) {
			sWhere += (!sWhere.isEmpty()?" and ":" ") + "model.id = :id";
			params.put("id", filtro.getId());
		}
		
		//folio
		if(filtro.getFolio() != null) {
			sWhere += (!sWhere.isEmpty()?" and ":" ") + "model.folio = :folio";
			params.put("folio", filtro.getFolio());
		}
		
		//fechaCorte
		if(filtro.getRangoFechas().getFechaInicio() != null && filtro.getRangoFechas().getFechaFin() != null) {
			sWhere += (!sWhere.isEmpty()?" and ":" ") + "(model.fechaCorte BETWEEN :fechaInicio AND :fechaFin)";
			params.put("fechaInicio", filtro.getRangoFechas().getFechaInicio());
			params.put("fechaFin", filtro.getRangoFechas().getFechaFin());
			
		} else if (filtro.getRangoFechas().getFechaInicio() != null) {
			
			sWhere += (!sWhere.isEmpty()?" and ":" ") + "model.fechaCorte >= :fechaInicio";
			params.put("fechaInicio", filtro.getRangoFechas().getFechaInicio());
			
		}  else if (filtro.getRangoFechas().getFechaFin() != null) {
			
			sWhere += (!sWhere.isEmpty()?" and ":" ") + "model.fechaCorte <= :fechaFin";
			params.put("fechaFin", filtro.getRangoFechas().getFechaFin());
			
		}
				
		//proveedor.id
		if(filtro.getProveedor() != null && filtro.getProveedor().getId() != null) {
			sWhere += (!sWhere.isEmpty()?" and ":" ") + "model.proveedor.id = :idProveedor";
			params.put("idProveedor", filtro.getProveedor().getId());
		}
				
		//solicitudCheque.status
		if(filtro.getSolicitudCheque() != null && filtro.getSolicitudCheque().getStatus() != null && !filtro.getSolicitudCheque().getStatus().trim().equals("")) {
			sWhere += (!sWhere.isEmpty()?" and ":" ") + "model.solicitudCheque.status = :status";
			params.put("status", filtro.getSolicitudCheque().getStatus());
		}
		
		//claveNegocio
		if(filtro.getClaveNegocio() != null && !filtro.getClaveNegocio().equals("")) {
			sWhere += (!sWhere.isEmpty()?" and ":" ") + "model.claveNegocio = :claveNegocio";
			params.put("claveNegocio", filtro.getClaveNegocio());
		}
		
		if(!sWhere.isEmpty()){
			sWhere =" where "+ sWhere;
		}
							
		return sWhere;
	}
	

	@EJB
	private EntidadDao entidadDao;
	
	@EJB
	private ScrollableNavigationDao scrollableNavigationDao;

	@PersistenceContext
	private EntityManager entityManager;
	
	
}
