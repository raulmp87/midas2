package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.calculo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.calculo.CalculoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccion;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.dto.ContenedorPrimas;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;

@Stateless
public class CalculoDaoImpl implements CalculoDao {
	
	public final static String ERROR_EN = "Error en";
	@PersistenceContext
	protected EntityManager entityManager;
	
	private NegocioEstadoDescuentoService negocioEstadoDescuentoService;

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CoberturaCotizacionDTO executeStoreProcedure(BigDecimal idCotizacion, Short idMoneda, Long IdNegocio, BigDecimal idToProducto,
			Long numeroInciso, BigDecimal idToSeccion, Long idToAgrupadorTarifa, Long idVerAgrupadorTarifa, Long idVertarifa,
			String claveEstilo, Long modeloVehiculo, String claveZonaCircualcion, BigDecimal tipoUso, BigDecimal tipoServicio,
			Long idPaquete,String modificadoresPrima, String modificadoresDescripcion, String claveFuenteSuemaAsegurada, int diasVigencia, 
			CoberturaCotizacionDTO cobertura) {
		 Double deducible = null;
		 String spName="MIDAS.pkgAUT_CalculosRiesgos.spAUT_CalculaRiesgo";
		 String [] atributosDTO = { "valorSumaAsegurada","valorPrimaNeta" };
		 String [] columnasCursor = { "sumaAsegurada", "primaARDV"};
		 try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pIdCotizacion", idCotizacion);
				storedHelper.estableceParametro("pIdMoneda", idMoneda);
				storedHelper.estableceParametro("pIdNegocio", IdNegocio);
				storedHelper.estableceParametro("pIdProducto", idToProducto);
				storedHelper.estableceParametro("pNumeroInciso", numeroInciso);
				storedHelper.estableceParametro("pIdLineaNegocio", idToSeccion);
				storedHelper.estableceParametro("pIdAgrupadorTarifa", idToAgrupadorTarifa);
				storedHelper.estableceParametro("pIdVerAgrupadorTarifa", idVerAgrupadorTarifa);
				storedHelper.estableceParametro("pIdVersionCarga", idVertarifa);
				storedHelper.estableceParametro("pClaveEstiloVehiculo", claveEstilo);
				storedHelper.estableceParametro("pModeloVehiculo", modeloVehiculo);
				storedHelper.estableceParametro("pClaveZonaCirculacion", claveZonaCircualcion);
				storedHelper.estableceParametro("pIdUsoVehiculo", tipoUso);
				storedHelper.estableceParametro("pIdServicioVehiculo", tipoServicio);
				storedHelper.estableceParametro("pIdPaquete", idPaquete);
				storedHelper.estableceParametro("pIdCobertura", cobertura.getId().getIdToCobertura());
				storedHelper.estableceParametro("pVarModifPrima", modificadoresPrima);
				storedHelper.estableceParametro("pVarModifDescripcion", modificadoresDescripcion);
				storedHelper.estableceParametro("pClaveFuenteSumaAsegurada", claveFuenteSuemaAsegurada);
				//TODO multiplicar suma asegurada * num pasajeros para las coberturas de GAstos Medicos y RC Viajero
				storedHelper.estableceParametro("pSumaAsegurada", cobertura.getValorSumaAsegurada());
				storedHelper.estableceParametro("pDiasVigencia", diasVigencia);
				storedHelper.estableceParametro("pClaveTipoDeduciblePP", cobertura.getClaveTipoDeducible());
			    switch(cobertura.getClaveTipoDeducible().intValue()){
				    case 1:
				    	deducible = cobertura.getPorcentajeDeducible();
				    	break;
				    default:
				    	deducible = cobertura.getValorDeducible();
				    	break;
			    }
				storedHelper.estableceParametro("pValorDeduciblePP", deducible);
				storedHelper.estableceParametro("pClaveTipoDeduciblePT", cobertura.getClaveTipoDeducible());
				storedHelper.estableceParametro("pValorDeduciblePT", deducible);				
				storedHelper.estableceMapeoResultados("mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO", atributosDTO, columnasCursor);
				
				CoberturaCotizacionDTO coberturaCotizacionDTOTemp = (CoberturaCotizacionDTO)storedHelper.obtieneResultadoSencillo();
				cobertura.setValorPrimaNeta(coberturaCotizacionDTOTemp.getValorPrimaNeta());
				cobertura.setValorSumaAsegurada(coberturaCotizacionDTOTemp.getValorSumaAsegurada());
			} catch (Exception e) {
				throw new RuntimeException(ERROR_EN + spName,e);
			}
		
		return cobertura;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BitemporalCoberturaSeccion calculaCoberturaBitemporal(BigDecimal idCotizacionB, BigDecimal idIncisoB, BigDecimal idIncisoSeccionB, 
			BigDecimal idCoberturaSeccionB, Integer diasVigencia, Short idMoneda, Long IdNegocio, BigDecimal idToProducto,
			Long numeroInciso, BigDecimal idToSeccion, Long idToAgrupadorTarifa, Long idVerAgrupadorTarifa, Long idVertarifa,
			String claveEstilo, Long modeloVehiculo, String claveZonaCircualcion, BigDecimal tipoUso, BigDecimal tipoServicio,
			Long idPaquete,String modificadoresPrima, String modificadoresDescripcion, String claveFuenteSuemaAsegurada, BitemporalCoberturaSeccion cobertura) {
		
			Double deducible = null;
		 String spName="MIDAS.pkgAUT_CalculosRiesgos.spAUT_CalculaRiesgoBT";
		 String [] atributosDTO = { "valorSumaAsegurada","valorPrimaNeta" };
		 String [] columnasCursor = { "sumaAsegurada", "primaARDV"};
		 try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pIdMCotizacionB", idCotizacionB);
				storedHelper.estableceParametro("pIdMIncisoB", idIncisoB);
				storedHelper.estableceParametro("pIdMIncisoSeccionB", idIncisoSeccionB);
				storedHelper.estableceParametro("pIdMCoberturaSeccionB", idCoberturaSeccionB);
				storedHelper.estableceParametro("pIdMoneda", idMoneda);
				storedHelper.estableceParametro("pIdNegocio", IdNegocio);
				storedHelper.estableceParametro("pIdProducto", idToProducto);
				storedHelper.estableceParametro("pNumeroInciso", numeroInciso);
				storedHelper.estableceParametro("pIdLineaNegocio", idToSeccion);
				storedHelper.estableceParametro("pIdAgrupadorTarifa", idToAgrupadorTarifa);
				storedHelper.estableceParametro("pIdVerAgrupadorTarifa", idVerAgrupadorTarifa);
				storedHelper.estableceParametro("pIdVersionCarga", idVertarifa);
				storedHelper.estableceParametro("pClaveEstiloVehiculo", claveEstilo);
				storedHelper.estableceParametro("pModeloVehiculo", modeloVehiculo);
				storedHelper.estableceParametro("pClaveZonaCirculacion", claveZonaCircualcion);
				storedHelper.estableceParametro("pIdUsoVehiculo", tipoUso);
				storedHelper.estableceParametro("pIdServicioVehiculo", tipoServicio);
				storedHelper.estableceParametro("pIdPaquete", idPaquete);
				storedHelper.estableceParametro("pIdCobertura", cobertura.getEntidadContinuity().getCoberturaDTO().getIdToCobertura());
				storedHelper.estableceParametro("pVarModifPrima", modificadoresPrima);
				storedHelper.estableceParametro("pVarModifDescripcion", modificadoresDescripcion);
				storedHelper.estableceParametro("pClaveFuenteSumaAsegurada", claveFuenteSuemaAsegurada);
				//TODO multiplicar suma asegurada * num pasajeros para las coberturas de GAstos Medicos y RC Viajero
				storedHelper.estableceParametro("pSumaAsegurada", cobertura.getValue().getValorSumaAsegurada());
				storedHelper.estableceParametro("pDiasVigencia", diasVigencia);
				storedHelper.estableceParametro("pClaveTipoDeduciblePP", cobertura.getValue().getClaveTipoDeducible());
			    switch(Integer.valueOf(cobertura.getEntidadContinuity().getCoberturaDTO().getClaveTipoDeducible())){
			    case 1:
			    	deducible = cobertura.getValue().getPorcentajeDeducible();
			    	break;
			    default:
			    	deducible = cobertura.getValue().getValorDeducible();
			    	break;
		    }
				storedHelper.estableceParametro("pValorDeduciblePP", deducible);
				storedHelper.estableceParametro("pClaveTipoDeduciblePT", Integer.valueOf(cobertura.getEntidadContinuity().getCoberturaDTO().getClaveTipoDeducible()));
				storedHelper.estableceParametro("pValorDeduciblePT", deducible);
				
				storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccion", atributosDTO, columnasCursor);
				
				CoberturaSeccion resultado = (CoberturaSeccion)storedHelper.obtieneResultadoSencillo();
				
				if (resultado != null) {
					cobertura.getValue().setValorPrimaNeta(resultado.getValorPrimaNeta());
					cobertura.getValue().setValorSumaAsegurada(resultado.getValorSumaAsegurada());
				}
				
			} catch (Exception e) {
				throw new RuntimeException(ERROR_EN + spName,e);
			}
		
		return cobertura;
	}

	@Override
	public String getZonaCirculacion(BigDecimal idSeccion, Integer idEstado, Integer idMunicipio, Long idToNegocio) {
		
		 String claveZonaCircualcion=null;		 
		
		//Tomar la zona que se tenga configurada por negocio-estado, si no existe 
		//entonces tomar la zona  configurada por estado-ciudad
		 NegocioEstadoDescuento negocioEstadoDescuento = null;
		 if(idToNegocio != null) {
			 negocioEstadoDescuento = negocioEstadoDescuentoService.findByNegocioAndEstado(
						idToNegocio, String.valueOf(idEstado));
		 }
		 
		if(negocioEstadoDescuento != null && negocioEstadoDescuento.getZona() != null){
			claveZonaCircualcion = negocioEstadoDescuento.getZona();
		} else {
			String spName="MIDAS.pkgAUT_Servicios.spAUT_ZonCircPorCiudad";
			 String [] atributosDTO = { "mensaje"};
			 String [] columnasCursor = { "ZONACIRCULACION"};
			 try {
					StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
					storedHelper.estableceParametro("pIdEstado", idEstado);
					storedHelper.estableceParametro("pIdCiudad", idMunicipio);
					storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);
					
					MensajeDTO mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
					claveZonaCircualcion =  mensajeDTO.getMensaje();
				} catch (Exception e) {
					throw new RuntimeException(ERROR_EN + spName,e);
				}
		}

		return claveZonaCircualcion;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<PolizaDTO> getCantidadSiniestro(BigDecimal idToPoliza) {
			List<PolizaDTO> polizaList=null;
		 String spName="SEYCOS.pkg_int_midas_e2.stpobtieneCantSiniestros";
		 String [] atributosDTO = { "idToPoliza","folioPoliza", "numeroRenovacionStr", "totalSiniestro"};
		 String [] columnasCursor = { "idtopoliza", "poliza", "renov", "total_sin"};
		 try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pidtopoliza", idToPoliza);
				storedHelper.estableceMapeoResultados("mx.com.afirme.midas.poliza.PolizaDTO", atributosDTO, columnasCursor);
				
				polizaList = (List<PolizaDTO>)storedHelper.obtieneListaResultados();
				if(polizaList != null && !polizaList.isEmpty()){
					for(PolizaDTO item : polizaList){
						try{
							item.setNumeroRenovacion(Integer.parseInt(item.getNumeroRenovacionStr()));
						}catch(Exception e){
							Double valor = Double.parseDouble(item.getNumeroRenovacionStr());
							item.setNumeroRenovacion(valor.intValue());
						}
						item.setNumeroSiniestro(item.getTotalSiniestro().toPlainString());
					}
				}
			} catch (Exception e) {
				throw new RuntimeException(ERROR_EN + spName,e);
			}
		
		return polizaList;
	}
	
	@Override
	public Double getDescuentoMaximoPorEstado(Integer idEstado) {
		
		 Double descuentoMaximo=0.0;
		 String descuentoMaximoStr=null;
		 String spName="MIDAS.pkgAUT_Servicios.spAUT_DescuentoMaxPorEstado";
		 String [] atributosDTO = { "mensaje"};
		 String [] columnasCursor = { "PCTDESCUENTOMAX"};
		 try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pIdEstado", idEstado);
				storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);
				
				MensajeDTO mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
				descuentoMaximoStr =  mensajeDTO.getMensaje();
				if(descuentoMaximoStr != null) {
					descuentoMaximo = Double.valueOf(descuentoMaximoStr);
				}
			} catch (Exception e) {
				throw new RuntimeException(ERROR_EN + spName,e);
			}
			
		return descuentoMaximo;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ContenedorPrimas getPrimaNetaCoberturas(BigDecimal idToCotizacion, BigDecimal numeroInciso){
		ContenedorPrimas contenedor = new ContenedorPrimas();
		
				Query query;
		String queryString = " SELECT CC.IDTCSUBRAMO,  ";
		queryString = queryString.concat(" SUM(CASE WHEN COB.CLAVECOBERTURAPROPIA = 1 THEN CC.VALORPRIMANETA ELSE 0 END) PROPIAS, ");
		queryString = queryString.concat(" SUM(CASE WHEN COB.CLAVECOBERTURAPROPIA = 0 THEN CC.VALORPRIMANETA ELSE 0 END) NO_PROPIAS  ");
		queryString = queryString.concat(" FROM MIDAS.TOCOBERTURACOT CC, MIDAS.TOCOBERTURA COB ");
		queryString = queryString.concat(" WHERE CC.IDTOCOTIZACION = ? ");
		queryString = queryString.concat(" AND CC.NUMEROINCISO = ? ");
		queryString = queryString.concat(" AND CC.IDTOCOBERTURA = COB.IDTOCOBERTURA ");
		queryString = queryString.concat(" GROUP BY CC.IDTCSUBRAMO ");
		
		query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, idToCotizacion);
		query.setParameter(2, numeroInciso);
		
		List<Object[]> result  = query.getResultList();
		
		if(result instanceof List)  {
			List<Object[]> listaResultados = (List<Object[]>) result;				
			if(listaResultados != null && listaResultados.size() > 0){
				Object[] singleResult = listaResultados.get(0);
				
				contenedor.setIdTcSubramo((BigDecimal)singleResult[0]);
				contenedor.setTotalPrimaPropias((BigDecimal)singleResult[1]);
				contenedor.setTotalPrimaExternas((BigDecimal)singleResult[2]);
			}
		}
		return contenedor;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ResumenCostosDTO resumenCostosEmitidos(BigDecimal idToPoliza, Short numeroEndoso, Integer numeroInciso) {
		ResumenCostosDTO resumenCostosDTO = new ResumenCostosDTO();
				
		String queryString = " select sum(VALORPRIMANETA) totalPrimas,  ";
		queryString = queryString.concat(" sum(VALORBONIFCOMISION) descuentos, ");
		queryString = queryString.concat(" sum(VALORPRIMANETA - VALORBONIFCOMISION) primaNeta, ");
		queryString = queryString.concat(" sum(VALORRECARGOPAGOFRAC - VALORBONIFCOMRECPAGOFRAC) recargo,  ");
		queryString = queryString.concat(" sum(VALORDERECHOS) derechos, ");
		queryString = queryString.concat(" sum(VALORIVA) iva,  ");
		queryString = queryString.concat(" sum(VALORPRIMATOTAL) primaTotal ");
		queryString = queryString.concat(" from midas.TOCOBERTURAEND");
		queryString = queryString.concat(" where idtopoliza = ?1 ");		
		if(numeroEndoso!=null){
			queryString = queryString.concat(" and numeroendoso <= ?2");
		}		
		if(numeroInciso!=null){
			queryString = queryString.concat(" and numeroInciso = ?3 ");
		}
		queryString = queryString.concat(" group by idtopoliza ");
		
		Query query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, idToPoliza);
		query.setParameter(2, numeroEndoso);
		query.setParameter(3, numeroInciso);
		
		List<Object[]> result  = query.getResultList();
		
		if(result instanceof List)  {
			List<Object[]> listaResultados = (List<Object[]>) result;				
			if(listaResultados != null && listaResultados.size() > 0){
				Object[] singleResult = listaResultados.get(0);
				
				resumenCostosDTO.setTotalPrimas(((BigDecimal)singleResult[0]).doubleValue());
				resumenCostosDTO.setDescuentoComisionCedida(((BigDecimal)singleResult[1]).doubleValue());
				resumenCostosDTO.setDescuento(((BigDecimal)singleResult[1]).doubleValue());
				resumenCostosDTO.setPrimaNetaCoberturas(((BigDecimal)singleResult[2]).doubleValue());
				resumenCostosDTO.setRecargo(((BigDecimal)singleResult[3]).doubleValue());
				resumenCostosDTO.setDerechos(((BigDecimal)singleResult[4]).doubleValue());
				resumenCostosDTO.setIva(((BigDecimal)singleResult[5]).doubleValue());
				resumenCostosDTO.setPrimaTotal(((BigDecimal)singleResult[6]).doubleValue());
			}
		}
		
		return resumenCostosDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public void ajusteIgualacionCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso, Double primaTotal) {
		 String spName="MIDAS.pkgAUT_CalculosRiesgos.spAUT_AjusteIgualacion";
			try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(
						spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pIdCotizacion", idToCotizacion);
				storedHelper.estableceParametro("pNumeroInciso", numeroInciso);
				storedHelper.estableceParametro("pPrimaTotal", primaTotal);
				
				storedHelper.ejecutaActualizar();
				
				entityManager.getEntityManagerFactory().getCache().evictAll();

			} catch (Exception e) {
				throw new RuntimeException("Error en :" + spName, e);
			}
	}	

	@EJB
	public void setNegocioEstadoDescuentoService(
			NegocioEstadoDescuentoService negocioEstadoDescuentoService) {
		this.negocioEstadoDescuentoService = negocioEstadoDescuentoService;
	}
}
