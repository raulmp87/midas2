package mx.com.afirme.midas2.dao.impl.cliente.grupo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.cliente.grupo.GrupoClienteDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.cliente.ClienteGrupoCliente;
import mx.com.afirme.midas2.domain.cliente.ClienteGrupoCliente_;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente_;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class GrupoClienteDaoImpl extends JpaDao<Long, GrupoCliente> implements
		GrupoClienteDao {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<GrupoCliente> findByFilters(GrupoCliente entity){
		String queryString = "select model from GrupoCliente model ";
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		String sWhere = "";
		
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere,  "descripcion", entity.getDescripcion());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos, sWhere, "clave", entity.getClave());
		
		if (Utilerias.esAtributoQueryValido(sWhere))
			queryString = queryString.concat(" where ").concat(sWhere);
		
		Query query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
	}
	
	public GrupoCliente findByClave(String clave) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<GrupoCliente> cq = cb.createQuery(GrupoCliente.class);
		Root<GrupoCliente> gc = cq.from(GrupoCliente.class);
		cq.where(cb.equal(gc.get(GrupoCliente_.clave), clave));
		return getSingleResult2(entityManager.createQuery(cq));
	}
	
	public List<GrupoCliente> findByCliente(Long idCliente) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<GrupoCliente> cq = cb.createQuery(GrupoCliente.class);
		Root<GrupoCliente> gc = cq.from(GrupoCliente.class);
		Join<GrupoCliente,ClienteGrupoCliente> cgc = gc.join(GrupoCliente_.clienteGrupoClientes);
		cq.where(cb.equal(cgc.get(ClienteGrupoCliente_.idTcCliente), idCliente));
		return entityManager.createQuery(cq).getResultList();
	}
	
}
