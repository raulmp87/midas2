package mx.com.afirme.midas2.dao.impl.calculos;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.calculos.CalculoBonosDao;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.DetalleCalculoBono;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CalculoBonoEjecuciones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HonorariosAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.bonos.DetalleBonoPolizaView;
import mx.com.afirme.midas2.dto.bonos.DetalleBonoRamoSubramoView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.PreviewCalculoBonoView;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.calculos.AgentesCalculoTemporalService;
import mx.com.afirme.midas2.service.calculos.SolicitudChequesMizarService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class CalculoBonosDaoImpl extends EntidadDaoImpl implements CalculoBonosDao{
	private ConfigBonosService configBonosService;
	private EntidadService entidadService;
	private ValorCatalogoAgentesService catalogoService;
	private AgentesCalculoTemporalService calculoTemporalService;
	private SolicitudChequesMizarService solicitudChequesMizarService;
	private static final String PKG_INT_MIDAS_E2="Pkg_int_midas_E2";
	private static final String PKGCALCULOS_AGENTES="PKGCALCULOS_AGENTES";
	
	private static String CATALOGO_ESTATUS_CALCULO="Estatus Calculo Comisiones";
	
	@Override
	public List<CalculoBono> findByFiltersCalculo(CalculoBono calculoBono) {
		List<CalculoBono>calculoBonoList= new ArrayList<CalculoBono>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from CalculoBono model ");
		Map<String,Object> parametros=new HashMap<String, Object>();
		if(calculoBono!=null){
			if(calculoBono.getId()!=null){
				addCondition(queryString, " model.id=:id");
				parametros.put("id", calculoBono.getId());
			}
			else{
				if(isValid(calculoBono.getDescripcionBono())){
					addCondition(queryString, " model.descripcionBono like :descripcionBono");
					parametros.put("descripcionBono", calculoBono.getDescripcionBono());
				}
				if(isNotNull(calculoBono.getModoEjecucion()) && isNotNull(calculoBono.getModoEjecucion().getId())){
					addCondition(queryString, " model.modoEjecucion.id like :modoEjecucion");
					parametros.put("modoEjecucion", calculoBono.getModoEjecucion().getId().toString());
				}
				if(isNotNull(calculoBono.getTipoBono()) && isNotNull(calculoBono.getTipoBono().getId())){
					addCondition(queryString, " model.tipoBono.id like :tipoBono");
					parametros.put("tipoBono", calculoBono.getTipoBono().getId().toString());
				}
				if(isNotNull(calculoBono.getFechaCorte())){
					addCondition(queryString, " model.fechaCorte =:fechaCorte");
					parametros.put("fechaCorte", calculoBono.getFechaCorte());
				}
			}
		}
		String finalQuery= getQueryString(queryString)+" ORDER BY model.id";
		Query query = entityManager.createQuery(finalQuery);
		if(!parametros.isEmpty()){
			setQueryParametersByProperties(query, parametros);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		calculoBonoList=query.getResultList();
		return calculoBonoList;
	}

	@Override
	public List<PreviewCalculoBonoView> findByFiltersCalculoView(CalculoBono calculoBono) {
		List<PreviewCalculoBonoView> lista= new ArrayList<PreviewCalculoBonoView>();		
		Map<Integer,Object> parametros=new HashMap<Integer, Object>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append("SELECT bono.ID, bono.DESCRIPCIONBONO, bono.FECHACORTE, trunc(bono.IMPORTETOTAL,2) AS IMPORTETOTAL, bono.TOTALBENEFICIARIOS,catEjecucion.valor as modoEjecucion, catTipobono.valor as tipoBono,estatus.valor as descripcionEstatus ");
		queryString.append(" FROM MIDAS.toCalculoBono bono ");
		queryString.append(" INNER JOIN MIDAS.tovalorCatalogoagentes catEjecucion  on  catEjecucion.id=bono.IDMODOEJECUCION ");
		queryString.append(" INNER JOIN MIDAS.tovalorCatalogoagentes catTipobono  on  catTipobono.id=bono.TIPOBONO ");
		queryString.append(" INNER JOIN MIDAS.tovalorCatalogoagentes estatus  on  bono.status=estatus.id ");		 
		queryString.append(" where ");
		if(calculoBono!=null){
			int index=1;
			if(calculoBono.getId()!=null){
				addCondition(queryString, "bono.id=?");
				parametros.put(index, calculoBono.getId());
				index++;
			}
			else{
				if(isValid(calculoBono.getDescripcionBono())){
					addCondition(queryString, " bono.descripcionBono like ?");
					parametros.put(index, calculoBono.getDescripcionBono());
					index++;
				}
				if(isNotNull(calculoBono.getModoEjecucion()) && isNotNull(calculoBono.getModoEjecucion().getId())){
					addCondition(queryString, " bono.IDMODOEJECUCION=?");
					parametros.put(index, calculoBono.getModoEjecucion().getId().toString());
					index++;
				}
				if(isNotNull(calculoBono.getTipoBono()) && isNotNull(calculoBono.getTipoBono().getId())){
					addCondition(queryString, " bono.tipoBono=?");
					parametros.put(index, calculoBono.getTipoBono().getId().toString());
					index++;
				}
				if(isNotNull(calculoBono.getFechaCorteString())){
					addCondition(queryString, " TRUNC(bono.fechaCorte)=to_date(?,'dd/mm/yyyy')");
					parametros.put(index, calculoBono.getFechaCorteString());
					index++;
				}
			}
		}
		if(parametros.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		String finalQuery=getQueryString(queryString)+" order by id desc ";
		Query query=entityManager.createNativeQuery(finalQuery,PreviewCalculoBonoView.class);
		if(!parametros.isEmpty()){
			for(Integer key:parametros.keySet()){
				query.setParameter(key,parametros.get(key));
			}
		}
		lista= query.getResultList();
		return lista;
	}
	
	@Override
	public List<DetalleCalculoBono> findByFiltersDetalleCalculo(DetalleCalculoBono detalleCalculoBono) {
		List<DetalleCalculoBono>listResult = new ArrayList<DetalleCalculoBono>();
		CalculoBono calc=entidadService.findById(CalculoBono.class, detalleCalculoBono.getCalculoBono().getId());
		detalleCalculoBono.setCalculoBono(calc);
		listResult=entidadService.findByProperty(DetalleCalculoBono.class, "calculoBono", detalleCalculoBono.getCalculoBono());
		return listResult;
	}

	@Override
	public CalculoBono loadByIdCalculo(Long idCalculo) {
		CalculoBono obj=null;
		CalculoBono filtro= new CalculoBono();
		filtro.setId(idCalculo);		
		List<CalculoBono>calculoBonoList=findByFiltersCalculo(filtro);
		if(!isEmptyList(calculoBonoList)){
			obj=calculoBonoList.get(0);
			obj.setBanderaExisteAmis(existeClaveAmisConfig(filtro.getId()));
		}
		return obj;
	}

	@Override
	public DetalleCalculoBono loadByIdDetalleCalculo(Long idCalculo) {
		DetalleCalculoBono obj = null;
		DetalleCalculoBono filtro= new DetalleCalculoBono();
		CalculoBono calculoBono= new CalculoBono();
		calculoBono.setId(idCalculo);
		filtro.setCalculoBono(calculoBono);
		List<DetalleCalculoBono>detalleCalculoBonoList=findByFiltersDetalleCalculo(filtro);
		if(!isEmptyList(detalleCalculoBonoList)){
			obj=detalleCalculoBonoList.get(0);
		}
		return obj;
	}

	@Override
	public List<BigDecimal> getCentrosOperacionByConfiguracion(Long idConfig) throws MidasException{
		List<BigDecimal> list=new ArrayList<BigDecimal>();
		if(idConfig==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT CENTROOPERACION_ID as idCentroOperacion FROM MIDAS.TRCONFIGBONOCentroOperacion  ");
		queryString.append(" WHERE  CONFIGBONO_ID = "+idConfig);
		Query query = entityManager.createNativeQuery(queryString.toString());
		list= query.getResultList();
		return list;	
	}

	@Override
	public List<BigDecimal> getEjecutivosByConfiguracion(Long idConfig) throws MidasException{
		List<BigDecimal> list=new ArrayList<BigDecimal>();
		if(idConfig==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT EJECUTIVO_ID as idEjecutivo FROM MIDAS.TRCONFIGBONOEjecutivo  ");
		queryString.append(" WHERE  CONFIGBONO_ID = "+idConfig);
		Query query = entityManager.createNativeQuery(queryString.toString());
		list= query.getResultList();
		return list;	
	}

	@Override
	public List<BigDecimal> getGerenciasByConfiguracion(Long idConfig) throws MidasException{
		List<BigDecimal> list=new ArrayList<BigDecimal>();
		if(idConfig==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
//		return entidadService.findByProperty(ConfigComGerencia.class,"configuracionComisiones.id", config.getId());
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT GERENCIA_ID ");
		queryString.append("FROM MIDAS.TRCONFIGBONOGerencia  ");
		queryString.append(" WHERE  CONFIGBONO_ID = "+idConfig);
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		list  = query.getResultList();
		return list;	
	}

	@Override
	public List<BigDecimal> getPrioridadesByConfiguracion(Long idConfig) throws MidasException{
		List<BigDecimal> list=new ArrayList<BigDecimal>();
		if(idConfig==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT IDPRIORIDADAGENTE as idPrioridad FROM MIDAS.TRCONFIGBONOPrioridad ");
		queryString.append(" WHERE  CONFIGBONO_ID = "+idConfig);
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		list= query.getResultList();
		return list;
	}

	@Override
	public List<BigDecimal> getPromotoriasByConfiguracion(Long idConfig) throws MidasException{
		List<BigDecimal> list=new ArrayList<BigDecimal>();
		if(idConfig==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT PROMOTORIA_ID as idPromotoria FROM MIDAS.TRCONFIGBONOPromotoria ");
		queryString.append(" WHERE  CONFIGBONO_ID = "+idConfig);
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		list= query.getResultList();
		return list;
	}

	@Override
	public List<BigDecimal> getSituacionesByConfiguracion(Long idConfig) throws MidasException{
		List<BigDecimal> list=new ArrayList<BigDecimal>();
		if(idConfig==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT IDSITUACIONAGENTE as idSituacion FROM MIDAS.TRCONFIGBONOSituacion ");
		queryString.append(" WHERE  CONFIGBONO_ID = "+idConfig);
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		list= query.getResultList();
		return list;
	}

	@Override
	public List<BigDecimal> getTiposAgentesByConfiguracion(Long idConfig) throws MidasException{
		List<BigDecimal> list=new ArrayList<BigDecimal>();
		if(idConfig==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(""); 
		queryString.append("SELECT IDTIPOAGENTE as idTipoAgente FROM MIDAS.TRCONFIGBONOTipoAgente  ");
		queryString.append(" WHERE  CONFIGBONO_ID = "+idConfig);
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		list= query.getResultList();
		return list;	
	}

	@Override
	public List<BigDecimal> getTiposPromotoriasByConfiguracion(Long idConfig) throws MidasException{
		List<BigDecimal> list=new ArrayList<BigDecimal>();
		if(idConfig==null){
			onError("Favor de proporcionar la configuracion de comisiones");
		}
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT IDTIPOPROMOTORIA as idTipoPromotoria FROM MIDAS.TRCONFIGBONOTipoPromotoria ");
		queryString.append(" WHERE  CONFIGBONO_ID = "+idConfig);
							 
		Query query = entityManager.createNativeQuery(queryString.toString());
		list= query.getResultList();
		return list;	
	}
	
	@Override
	public List<AgenteView> obtenerAgentesPorConfiguracion(ConfigBonos configuracion)throws Exception {
		List<AgenteView> list=new ArrayList<AgenteView>();
		List<AgenteView> listExcluidos=new ArrayList<AgenteView>();
		Date fechaActual= new Date();		
		configuracion=entidadService.findById(ConfigBonos.class, configuracion.getId());
		List<BigDecimal>gerenciasPorConfig=getGerenciasByConfiguracion(configuracion.getId());
		List<BigDecimal>centroOperPorConfig=getCentrosOperacionByConfiguracion(configuracion.getId());
		List<BigDecimal>ejecutivosPorConfig=getEjecutivosByConfiguracion(configuracion.getId());
		List<BigDecimal>prioridadesPorConfig=getPrioridadesByConfiguracion(configuracion.getId());
		List<BigDecimal>promotoriasPorConfig=getPromotoriasByConfiguracion(configuracion.getId());
		List<BigDecimal>situacionesPorConfig=getSituacionesByConfiguracion(configuracion.getId());
		List<BigDecimal>tipoAgentesPorConfig=getTiposAgentesByConfiguracion(configuracion.getId());
		List<BigDecimal>tipoPromotoriasPorConfig=getTiposPromotoriasByConfiguracion(configuracion.getId());		
		
		///////
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		Gerencia filtro=new Gerencia();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select distinct ");
		queryString.append(" agente.id,");
		queryString.append(" agente.idAgente as idAgente,");
		queryString.append(" prioridad.valor as prioridad,");
		queryString.append(" estatus.valor as tipoSituacion,");
		queryString.append(" persona.nombreCompleto as nombreCompleto,");
		queryString.append(" promo.descripcion as promotoria,");
		queryString.append(" ger.descripcion as gerencia ");
		queryString.append(" from MIDAS.toAgente agente ");
		queryString.append(" inner join MIDAS.vw_persona persona on (persona.idpersona=agente.idpersona) ");
		queryString.append(" inner join MIDAS.toPromotoria promo on(agente.idpromotoria=promo.id)  ");
		queryString.append(" inner join MIDAS.toEjecutivo ej on(ej.id=promo.ejecutivo_id)  ");
		queryString.append(" inner join MIDAS.vw_persona personaEj on (personaEj.idpersona=ej.idpersona) ");
		queryString.append(" inner join MIDAS.toGerencia ger on (ej.gerencia_id=ger.id) ");
		queryString.append(" inner join MIDAS.toCentroOperacion co on (ger.centroOperacion_id=co.id) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoAgente on(tipoAgente.id=agente.idTipoAgente) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoPromotoria on (tipoPromotoria.id=promo.idTipoPromotoria) "); 
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes prioridad on (prioridad.id=agente.idPrioridadAgente) ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes estatus on (estatus.id=agente.idSituacionAgente) ");
		queryString.append(" where ");
		boolean isEmptyParams=true;
		if(isNotNull(configuracion)){
			queryString.append(" agente.IDTIPOCEDULAAGENTE not in(select ced.IDTIPOBONO from MIDAS.trConfigBonoExclusionTipoCed ced where idconfig =");
			queryString.append(configuracion.getId()+") and (");			
			int index=1;
			if(!isEmptyList(centroOperPorConfig)){
				queryString.append(" co.id in ( ");
				queryString.append(getIdObjectsFromList(centroOperPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(gerenciasPorConfig)){
				queryString.append(" ger.id in ( ");
				queryString.append(getIdObjectsFromList(gerenciasPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(ejecutivosPorConfig)){
				queryString.append(" ej.id in ( ");
				queryString.append(getIdObjectsFromList(ejecutivosPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(promotoriasPorConfig)){
				queryString.append(" promo.id in ( ");
				queryString.append(getIdObjectsFromList(promotoriasPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(situacionesPorConfig)){
				queryString.append(" estatus.id in ( ");
				queryString.append(getIdObjectsFromList(situacionesPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(tipoAgentesPorConfig)){
				queryString.append(" tipoAgente.id in ( ");
				queryString.append(getIdObjectsFromList(tipoAgentesPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(tipoPromotoriasPorConfig)){
				queryString.append(" tipoPromotoria.id in ( ");
				queryString.append(getIdObjectsFromList(tipoPromotoriasPorConfig)+") or");
				isEmptyParams=false;
			}
			if(!isEmptyList(prioridadesPorConfig)){
				queryString.append(" prioridad.id in ( ");
				queryString.append(getIdObjectsFromList(prioridadesPorConfig)+") or");
				isEmptyParams=false;
			}
			if(isEmptyParams){
				int lengthWhere="and (".length();
				queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
			}
			else{
				int position=queryString.lastIndexOf(" or");
				if(position>0){
					queryString.replace(queryString.lastIndexOf(" or"),queryString.lastIndexOf(" or")+3,")");
				}
			}
			String finalQuery=getQueryString(queryString)+" order by agente.id desc ";
			Query query=entityManager.createNativeQuery(finalQuery,AgenteView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			list=query.getResultList();
		}
		return list;
	}

	private String getIdObjectsFromList(List<BigDecimal>obj){
		StringBuilder ids=new StringBuilder("");
		int i=0;
		for(BigDecimal id:obj){
			ids.append(id.toString());
			if(i<(obj.size()-1)){
				ids.append(",");
			}
			i++;
		}
		return ids.toString();
	}

	@Override
	public Long generarCalculo(Long idConfigBono,List<AgenteView> agentesDelCalculo) throws MidasException {
		Long idCalculo=null;
		if(isNull(idConfigBono)){
			onError("El id de configuracion no puede ser null");
		}
		ConfigBonos filtro=new ConfigBonos();
		filtro.setId(idConfigBono);
		try {
			ConfigBonos persistentObjectConfigBonos=configBonosService.loadById(filtro);
			if(isNull(persistentObjectConfigBonos)|| isNull(persistentObjectConfigBonos.getId())){
				onError("No existe la configuracion");
			}
			//Por cada elemento de la lista se va a buscar sus agentes, se debe de verificar primero si tiene hijos o no seleccionados, si si, entonces
			//se excluyen sino no.
			if(isEmptyList(agentesDelCalculo)){
				onError("No hay agentes para considerar en el calculo de la configuracion no."+persistentObjectConfigBonos.getId());
			}
			Long idCalculoTemporal=calculoTemporalService.getNextIdCalculo();
			idCalculo=obtenerCalculoPorMovimientosAgentes(idConfigBono, agentesDelCalculo,idCalculoTemporal,null);
		} catch (Exception e) {
			onError(e);
		}
		return idCalculo;
	}

	private Long obtenerCalculoPorMovimientosAgentes(Long idConfigBonos,List<AgenteView> agentes,Long idCalculoTemporal,Long idCalculoBonos) throws MidasException{
		if(isEmptyList(agentes)){
			onError("Favor de proporcionar la lista de agentes a obtener el calculo");
		}
		guardarAgentesEnTemporal(agentes,idCalculoTemporal);
		//Si el idCalculoComisiones no es nulo, entonces se considera ese, y sino, se obtiene como un nuevo calculo de la secuencia.
		idCalculoBonos=(isNotNull(idCalculoBonos))?idCalculoBonos:getNextIdCalculoBonos();
		validarAgentesEnCalculosPendientes(idCalculoTemporal);
		Long resultado=iniciarCalculo(idConfigBonos, idCalculoBonos, idCalculoTemporal);
		return resultado;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private Long guardarAgentesEnTemporal(List<AgenteView> agentes,Long idCalculoTemporal) throws MidasException{
		List<Long> idAgentes=new ArrayList<Long>();
		if(!isEmptyList(agentes)){
			//Se agregan las gerencias separadas por ,
			for(AgenteView agente:agentes){
				if(isNotNull(agente) && isNotNull(agente.getId())){
					idAgentes.add(agente.getId());
				}
			}
		}
		//Long idCalculo=calculoTemporalService.getNextIdCalculo();
		calculoTemporalService.saveAll(idAgentes, "AGENTE", idCalculoTemporal);
		return idCalculoTemporal;
	}
	
	private Long iniciarCalculo(Long idConfiguracionBonos,Long idCalculoBonos,Long idCalculoTemporal) throws MidasException{
		Long idCalculo=null;
		StoredProcedureHelper storedHelper = null;
		String sp="MIDAS."+PKGCALCULOS_AGENTES+".stp_generarCalculoComisiones";
		try {
			LogDeMidasInterfaz.log("Entrando a CalculoComisionesDaoImpl.inciarCalculo..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pIdConfigComisiones",idConfiguracionBonos);
			storedHelper.estableceParametro("pIdCalculoComisiones",idCalculoBonos);
			storedHelper.estableceParametro("pIdCalculoTemporal",idCalculoTemporal);
			int id=storedHelper.ejecutaActualizar();
			idCalculo=Long.valueOf(id);
//			//FIXME Agregar update de calculo en la tabla temporal de calculos-Agentes
//			actualizarTemporalConCalculoGenerado(idCalculo,idCalculoTemporal);
			LogDeMidasInterfaz.log("Saliendo de CalculoBonosDaoImpl.inciarCalculo..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,CalculoComisionesDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de CalculoBonosDaoImpl.inciarCalculo..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en CalculoBonosDaoImpl.inciarCalculo..." + this, Level.WARNING, e);
			onError(e);
		}
		return idCalculo;
	}
	
	@SuppressWarnings("unchecked")
	private Long getNextIdCalculoBonos(){
		Long idCalculo=null;
		StringBuffer queryString = new StringBuffer(); 
		queryString.append("SELECT MIDAS.idToCalculoBono_seq.nextval as idCalculo from dual ");
		Query query = entityManager.createNativeQuery(queryString.toString());
		List<BigDecimal> lista= query.getResultList();
		if(!isEmptyList(lista)){
			idCalculo=(isNotNull(lista.get(0)))?lista.get(0).longValue():null;
		}
		return idCalculo;
	} 
	
	@Override
	public void validarAgentesEnCalculosPendientes(Long idCalculoTemporal)throws MidasException {
		// TODO Auto-generated method stub
		
	}
	

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long ejecutarCalculo(ProgramacionBono progBono, CalculoBono calculoBono, Long modoEjecucion, Integer isNegocio, String conceptoEjecucionAutomatica) throws MidasException {
		
		int idCalculo=0;
		try {
			LogDeMidasInterfaz.log("Entrando a CalculoBonosDaoImpl.ejecutarCalculo..." + this, Level.INFO, null);
			
			Long pIdCalculoIn=(isNotNull(calculoBono) && isNotNull(calculoBono.getId()))?calculoBono.getId():null;
			Long pIdProgramacion=progBono.getId();
			Long pIdConfigBono=progBono.getIdBono();
			Long pIdModoEjec=modoEjecucion;
			Date pFechaAlta=progBono.getFechaAlta();
			Date pFechaEjecucion=progBono.getFechaEjecucion();
			String pHora=progBono.getHora();
			Long pPeriodoEjecucion=progBono.getPeriodoEjecucion()!=null?progBono.getPeriodoEjecucion().getId():null;
			Long pTipoConfiguracion=progBono.getTipoConfiguracion()!=null?progBono.getTipoConfiguracion().getId():null;
			String pUsuarioAlta=progBono.getUsuarioAlta();
			Long pTipoBono=progBono.getTipoBono().getId();
			Long pClaveEstatus=progBono.getClaveEstatus()!=null?progBono.getClaveEstatus().getId():null;
			Integer pActivo=progBono.getActivo();
			Integer pEsNegocio=isNegocio;

			Long id=null;
			if(conceptoEjecucionAutomatica=="BONOS"){	
				id=solicitudChequesMizarService.iniciarCalculoBonos(pIdCalculoIn,pIdProgramacion,pIdConfigBono,pIdModoEjec,pFechaAlta,pFechaEjecucion,pHora,pPeriodoEjecucion,pTipoConfiguracion,pUsuarioAlta,pTipoBono,pClaveEstatus,pActivo,pEsNegocio);
			}else{
				id=solicitudChequesMizarService.iniciarCalculoProvisiones(pIdCalculoIn,pIdProgramacion,pIdConfigBono,pIdModoEjec,pFechaAlta,pFechaEjecucion,pHora,pPeriodoEjecucion,pTipoConfiguracion,pUsuarioAlta,pTipoBono,pClaveEstatus,pActivo,pEsNegocio);
			}
			
			idCalculo=(id!=null)?id.intValue():null;	
		}  catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en el calculo..." + this, Level.WARNING, e);
			//if(e instanceof MidasException){
				onError(e.getMessage());
			//}
		}
		return new Long(idCalculo);
	}
	
	/**
	 * Metodo que genera todas las solicitudes de cheque de cada pago de comisiones que se le hara al agente por medio del id del calculo.
	 * @param cheque
	 * @throws MidasException
	 */
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void generarSolicitudesDeChequePorCalculo(Long idCalculo) {
		
		if (isNull(idCalculo)) {
			
			throw new RuntimeException("Favor de proporcionar el calculo");
			
		}
		
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS." + PKG_INT_MIDAS_E2 + ".stpCrearSolChequesCalculoBono";
		
		try {
			
			logger.info("Generando solicitudes de cheque en SEYCOS a partir del calculo de Bonos " + idCalculo + "...");
			
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pIdCalculoBono",idCalculo);
			storedHelper.ejecutaActualizar();
			
		} catch (SQLException e) {	
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,CalculoComisionesDaoImpl.class, codErr, descErr);
			
			throw new RuntimeException(e);
			
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Long saveCalculoBono(CalculoBono calculo){
		calculo=entidadService.save(calculo);
		return calculo.getId();
	}
	
	private void actualizarEstatusDetallePorCalculo(Long idCalculo,EstatusCalculoBonos estatus) throws MidasException{
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo para actualizar el estatus de todos los detalles del mismo");
		}
		if(isNull(estatus)){
			onError("Favor de proporcionar el estatus del calculo a actualizar");
		}
		actualizarDetallePorCalculo(idCalculo, estatus);
	}
	
	public void actualizarDetallePorCalculo(Long idCalculo,EstatusCalculoBonos estatus) throws MidasException{
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo para actualizar su detalle");
		}
		if(isNull(estatus)){
			onError("Favor de proporcionar el estatus que desea asignar a los detalles del calculo");
		}
		ValorCatalogoAgentes claveEstatus=null;
		try{
			claveEstatus=catalogoService.obtenerElementoEspecifico(CATALOGO_ESTATUS_CALCULO,estatus.getValue());
		}catch(Exception e){
			onError(e);
		}
		if(isNull(claveEstatus)){
			onError("No existe el estatus "+estatus.getValue()+" como estatus del calculo");
		}
		DetalleCalculoBono detalle = new DetalleCalculoBono();
		CalculoBono cal = new CalculoBono();
		cal.setId(idCalculo);
		detalle.setCalculoBono(cal);
		List<DetalleCalculoBono> list=findByFiltersDetalleCalculo(detalle);
		if(!isEmptyList(list)){
			for(DetalleCalculoBono det:list){
				det.setStatus(claveEstatus);
			}
			saveListDetalleCalculoBonos(list);
		}
	}
	
	@Override
	public Long saveListDetalleCalculoBonos(List<DetalleCalculoBono> lista) throws MidasException {
		if(isEmptyList(lista)){
			onError("Favor de proporcionar la lista de detalle de comisiones a pagar");
		}
		for(DetalleCalculoBono detalle:lista){
			if(isNotNull(detalle)){
				saveDetalleCalculoBonos(detalle);
			}
		}
		return null;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)	
	public Long saveDetalleCalculoBonos(DetalleCalculoBono detalleCalculo)throws MidasException {
		if(isNull(detalleCalculo)){
			onError("Favor de proporcionar el detalle del calculo a guardar");
		}
		DetalleCalculoBono jpaObject=entidadService.save(detalleCalculo);
		Long id=(isNotNull(jpaObject))?jpaObject.getId():null;
		return id;
	}
	
	@EJB
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}

	@EJB
	public void setCalculoTemporalService(
			AgentesCalculoTemporalService calculoTemporalService) {
		this.calculoTemporalService = calculoTemporalService;
	}

	@EJB
	public void setSolicitudChequesMizarService(
			SolicitudChequesMizarService solicitudChequesMizarService) {
		this.solicitudChequesMizarService = solicitudChequesMizarService;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * Metodo que obtiene las programaciones de bonos activas automaticas por ejecutar.
	 * conceptoEjecucionAutomatica = parametro para identificar si se ejecuto para Provisiones o Bonos
	 * @return
	 * @throws MidasException
	 */
	@Override
	@SuppressWarnings("unchecked")
	public List<ProgramacionBono> obtenerConfiguracionesAutomaticasActivas(String conceptoEjecucionAutomatica) throws MidasException{
		List<ProgramacionBono> list=new ArrayList<ProgramacionBono>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select ");
		queryString.append(" prog.id as idProgramacion, ");
		queryString.append(" prog.idBono as idConfiguracion, ");
		queryString.append(" prog.fechaEjecucion as fechaOriginal, ");
		//queryString.append(" catPeriodo.valor as periodo,  ");
		queryString.append(" case when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('DIARIO'))) then ");
		queryString.append("     sysdate ");   
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('SEMANAL'))) then "); 
		queryString.append("     DECODE( ");
		queryString.append("         MOD(TO_NUMBER(to_date(sysdate) - to_date(prog.fechaEjecucion)),7) ");
		queryString.append("         ,0, ");
		queryString.append("         sysdate, ");
		queryString.append("         sysdate-1 ");
		queryString.append("     ) ");   
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('QUINCENAL'))) then "); 
		queryString.append("     DECODE( ");
		queryString.append("         MOD(TO_NUMBER(to_date(sysdate) - to_date(prog.fechaEjecucion)),15) ");
		queryString.append("         ,0, ");
		queryString.append("         sysdate, ");
		queryString.append("         sysdate-1 ");
		queryString.append("     ) ");   
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('MENSUAL'))) then "); 
		queryString.append("     DECODE( ");
		queryString.append("         to_date(ADD_MONTHS(prog.fechaEjecucion, floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) )), ");
		queryString.append("         to_date(sysdate), ");
		queryString.append("         sysdate, ");
		queryString.append("         sysdate-1 ");
		queryString.append("     )    ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('BIMESTRAL')) AND MOD(floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) ,2)=0) then ");
		queryString.append("     DECODE( ");
		queryString.append("         to_date(ADD_MONTHS(prog.fechaEjecucion, floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) )), ");
		queryString.append("         to_date(sysdate), ");
		queryString.append("         sysdate, ");
		queryString.append("         sysdate-1 ");
		queryString.append("     )   ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('TRIMESTRAL')) AND MOD(floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) ,3)=0) then ");
		queryString.append("     DECODE( ");
		queryString.append("         to_date(ADD_MONTHS(prog.fechaEjecucion, floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) )), ");
		queryString.append("         to_date(sysdate), ");
		queryString.append("         sysdate, ");
		queryString.append("         sysdate-1 ");
		queryString.append("     )   ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('SEMESTRAL')) AND MOD(floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) ,6)=0) then ");
		queryString.append("     DECODE( ");
		queryString.append("         to_date(ADD_MONTHS(prog.fechaEjecucion, floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) )), ");
		queryString.append("         to_date(sysdate), ");
		queryString.append("         sysdate, ");
		queryString.append("         sysdate-1 ");
		queryString.append("     )   ");
		queryString.append(" when (TRIM(UPPER(catPeriodo.valor))=UPPER(TRIM('ANUAL')) AND MOD(floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) ,12)=0) then ");
		queryString.append("     DECODE( ");
		queryString.append("         to_date(ADD_MONTHS(prog.fechaEjecucion, floor(MONTHS_BETWEEN(to_date(sysdate),prog.fechaEjecucion)) )), ");
		queryString.append("         to_date(sysdate), ");
		queryString.append("         sysdate, ");
		queryString.append("        sysdate-1 ");
		queryString.append("     )   ");
		queryString.append(" else  ");
		queryString.append("     sysdate-1 ");
		queryString.append(" end as fecha     ");
		queryString.append(" from MIDAS.toProgramacionBono prog ");
		queryString.append(" inner join MIDAS.tcGrupoCatalogoAgentes gc on gc.descripcion like 'Estatus Programacion Cliente'   ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes cat on cat.valor='CALCULO PENDIENTE' and cat.grupoCatalogoAgentes_id=gc.id and cat.id=prog.claveEstatus "); 
		queryString.append(" inner join MIDAS.tcGrupoCatalogoAgentes gPeriodo on gPeriodo.descripcion like 'Periodos de Ajuste de Bonos' ");  
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes catPeriodo  on gPeriodo.id=catPeriodo.grupoCatalogoAgentes_id and catPeriodo.id=prog.periodoEjecucion ");
		
		if("BONOS".equals(conceptoEjecucionAutomatica)){
			Calendar calendario = new GregorianCalendar();
			int hora;
			hora =calendario.get(Calendar.HOUR_OF_DAY);
			System.out.println(hora);
			queryString.append(" inner join MIDAS.toConfigBonos config on prog.IDBONO = config.ID and FECHAINICIOVIGENCIA<=sysdate and FECHAFINVIGENCIA>=sysdate ");
			queryString.append(" where prog.fechaEjecucion is not null and prog.periodoEjecucion is not null and prog.activo=1 and prog.fechaEjecucion<=sysdate ");
//			queryString.append(" and 0<(select count(prog.id) from MIDAS.toCalculoBono where idProgramacionBono=prog.id and to_char(fechaCorte,'DD/MM/YYYY')=to_char(sysdate,'DD/MM/YYYY'))");
			queryString.append(" and HORA = "+hora);
		}
		
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"programacionBonosView");
		list=query.getResultList();
		return list;
	}
	
	/**
	 * Permite ejecutar bono automatico
	 * @param progBono
	 * @param calculoBono
	 * @param isNegocio
	 * @return
	 * @throws MidasException
	 */
	@Override
	public void ejecutarCalculoAutomatico(ProgramacionBono progBono, CalculoBono calculoBono,String conceptoEjecucionAutomatica) throws MidasException{
		if(isNull(progBono)){
			onError("Programacion de bono nula");
		}
		ValorCatalogoAgentes tipoConfiguracion=progBono.getTipoConfiguracion();
		Integer esNegocio=null;
		if(isNull(tipoConfiguracion) || !isValid(tipoConfiguracion.getValor())){
			onError("El bono no contiene tipo de configuracion valida");
		}
		if(isNotNull(tipoConfiguracion) && "NEGOCIO ESPECIAL".equalsIgnoreCase(tipoConfiguracion.getValor().trim())){
			esNegocio=1;
		}else if(isNotNull(tipoConfiguracion) && "CONFIGURACION BONO".equalsIgnoreCase(tipoConfiguracion.getValor().trim())){
			esNegocio=0;
		}
		Long idModoEjecucion=null;
		ValorCatalogoAgentes modoEjecucionCalculo=null;
		try {
			modoEjecucionCalculo=catalogoService.obtenerElementoEspecifico("Modos de Ejecucion de Comisiones", "Automatico");
			if(isNull(modoEjecucionCalculo) || isNull(modoEjecucionCalculo.getId())){
				onError("No se encontro el catalogo de [Modos de Ejecucion de Comisiones]-Valor[Automatico]");
			}
			idModoEjecucion=modoEjecucionCalculo.getId();
		} catch (Exception e) {
			onError("No se encontro el catalogo de [Modos de Ejecucion de Comisiones]-Valor[Automatico]");
		}
		ejecutarCalculo(progBono, calculoBono, idModoEjecucion, esNegocio, conceptoEjecucionAutomatica);
	}
	/**
	 * Ejecuta el preview de los calculos de bonos automaticos
	 * @param lista
	 * @return
	 * @throws MidasException
	 */
	@Override
	public void ejecutarCalculosAutomaticos(List<ProgramacionBono> lista,String conceptoEjecucionAutomatica) throws MidasException{
		if(!isEmptyList(lista)){
			for(ProgramacionBono progBono:lista){
				ejecutarCalculoAutomatico(progBono, null,conceptoEjecucionAutomatica);
				LogDeMidasInterfaz.log("progBono.id= "+progBono.getDescripcionBono()+"..." + this, Level.INFO, null);
			}
		}
	}
	
	public String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		if(query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
	public void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	public void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	@Override
	public CalculoBono eliminaCalculoBonos(CalculoBono calculoBono) throws MidasException {
		List<CalculoBono> list = new ArrayList<CalculoBono>();
		if(calculoBono!=null && calculoBono.getId()!=null){
			
			CalculoBono calculo = loadByIdCalculo(calculoBono.getId());
			
			StringBuilder queryString = new StringBuilder("");
			queryString.append(" select  bono.id,cat.valor  from midas.tocalculobono bono ");
			queryString.append(" inner join MIDAS.tovalorcatalogoagentes cat on bono.status = cat.id ");
			queryString.append(" where (upper(cat.valor) like '%AUTORIZADO%' OR upper(cat.valor) like '%APLICADO%') and bono.id="+calculoBono.getId()+"");
			
			Query query=entityManager.createNativeQuery(queryString.toString());			
			list = query.getResultList();
			
			if(list.isEmpty()){
				ValorCatalogoAgentes cat = new ValorCatalogoAgentes();
				try {
					cat = catalogoService.obtenerElementoEspecifico("Estatus Calculo Comisiones", "ELIMINADO");
					calculo.setStatus(cat);			
					calculo=entidadService.save(calculo);
					return calculo;
					
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}				
			}
			
		}
		return null;		
	}
	
	@Override
	public List<DetalleBonoPolizaView> obtenerDetalleBonoPoliza(CalculoBono calculoBono){
		Long idTipoBeneficiario = getTipoBeneficiario();
		StringBuilder queryString= new StringBuilder();
		Map<Integer,Object> parametros=new HashMap<Integer, Object>();
		List<DetalleBonoPolizaView>lista = new ArrayList<DetalleBonoPolizaView>();
		queryString.append(" SELECT /*+INDEX (REC REP_PREV_BONO_IDX) ");
		queryString.append(" INDEX (P PK_POL_POLIZA) ");
		queryString.append(" INDEX (TCR REP_PREV_BONO_IDX2) ");
		queryString.append(" INDEX (TCS TCSUBRAMO_2_UI) ");
		queryString.append(" */ ");
		queryString.append(" max(TRCB.ID) idCalculoBono, ");
		queryString.append(" rec.ID_RECIBO idRecibo, ");
		queryString.append(" REC.NUM_FOLIO_RBO numRecibo, ");
		queryString.append(" TDCB.IDBENEFICIARIO idBeneficiario, ");
		queryString.append(" TDCB.BENEFICIARIO nombreBeneficiario, ");
		queryString.append(" (lpad(TRIM(TO_CHAR(p.id_centro_emis)),3,0)||'-'||lpad(TRIM(TO_CHAR(p.num_poliza)),10,0)||'-'||lpad(TRIM(TO_CHAR(p.num_renov_pol)),2,0)) numeroPoliza, ");
		queryString.append(" sum(TRCB.IMPORTEBONO) importeBono, ");
		queryString.append(" sum(TRCB.PRIMANETA) importePrima, ");
		queryString.append(" TCR.DESCRIPCIONRAMO descripcionRamo, ");
		queryString.append(" TCS.DESCRIPCIONSUBRAMO descripcionSubRamo, ");
		queryString.append(" (case when TRCB.PCTSINIESTRALIDAD is not null then TRCB.PCTSINIESTRALIDAD/100 end) porcentajeSiniestralidad, ");
		queryString.append(" (case when TRCB.PCTCRECIMIENTO is not null then TRCB.PCTCRECIMIENTO/100 end) porcentajeCrecimiento, ");
		queryString.append(" TRCB.PCTSUPERACIONMETA porcentajeSuperacionMeta ");
		queryString.append(" ,TDCB.IMPORTEISR,TDCB.IMPORTEIVA, ");
		queryString.append(" TDCB.IMPORTEIVARET, ");
		queryString.append(" SUM((select sum(importe) primanetaanterior ");
		queryString.append(" from  midas.tocrecimientoCalculoBono ccb where ccb.IDCALCULOBONO = TCB.id and (ccb.idAgente=TDCB.IDBENEFICIARIO or ccb.idpromotoria = TDCB.IDBENEFICIARIO) ");
		queryString.append(" )) AS primaNetaPerodoCoparacion, ");
		queryString.append(" sum((case when  TRCB.tipobeneficiario = "+idTipoBeneficiario+" then ");//--Agente
		queryString.append(" (case when TRCB.SUPERAPRODMINIMAAGTE = 1 AND TRCB.SUPERASINAGTE = 1 AND TRCB.APLICAPAGOXTIPOPRODUCCION = 1 AND TRCB.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND TRCB.EXCL_TIPO_PERSONA = 1 AND TRCB.EXCL_FECHA_VTO_CEDULA = 1 AND TRCB.EXCL_DIASANTES_VTO_CEDULA = 1 AND TRCB.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND TRCB.EXCL_INCISOS_MAY_IGUAL = 1 AND TRCB.EXCL_CLAVE_AMIS = 1 AND TRCB.EXCL_TIPO_MONEDA = 1 AND TRCB.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND TRCB.EXCL_DESCUENTO_MAY_IGUAL = 1 AND TRCB.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" NVL(TRCB.primaneta,0) ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end) ");
		queryString.append(" else ");//--promotoria
		queryString.append(" (case ");
		queryString.append(" when TRCB.SUPERAPRODMINIMAPROM = 1 AND TRCB.SUPERASINPROM = 1 AND TRCB.APLICAPAGOXTIPOPRODUCCION = 1 AND TRCB.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND TRCB.EXCL_TIPO_PERSONA = 1 AND TRCB.EXCL_FECHA_VTO_CEDULA = 1 AND TRCB.EXCL_DIASANTES_VTO_CEDULA = 1 AND TRCB.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND TRCB.EXCL_INCISOS_MAY_IGUAL = 1 AND TRCB.EXCL_CLAVE_AMIS = 1 AND TRCB.EXCL_TIPO_MONEDA = 1 AND TRCB.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND TRCB.EXCL_DESCUENTO_MAY_IGUAL = 1 AND TRCB.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" NVL(TRCB.primaneta,0) ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end) ");
		queryString.append(" end)) as primaNetaBono, ");
		queryString.append(" TRCB.PORCENTAJEBONO as pcteBono , ");
		queryString.append(" (CASE ");
		queryString.append(" when TRIM(rec.CVE_T_RECIBO) = 'REC' then 'Recibo' ");
		queryString.append(" when TRIM(rec.CVE_T_RECIBO) = 'NCR' then 'Nota de Credito' ");
		queryString.append(" when TRIM(rec.CVE_T_RECIBO) = 'RD'  then 'Recibo de Disminución' ");
		queryString.append(" END) AS tipoRecibo ");
		queryString.append(" FROM MIDAS.TOCALCULOBONO TCB ");
		queryString.append(" INNER JOIN MIDAS.TODETALLECALCULOBONO TDCB ");
		queryString.append(" ON TDCB.IDCALCULO = TCB.ID ");
		queryString.append(" AND TDCB.IDBENEFICIARIO > 0 ");
		queryString.append(" INNER JOIN MIDAS.TORECIBOCALCULOBONOS TRCB ");
		queryString.append(" ON TRCB.IDDETALLECALCULOBONOS = TDCB.ID ");
		queryString.append(" LEFT OUTER JOIN MIDAS.TCRAMO TCR ");
		queryString.append(" ON TCR.CODIGORAMO = TRCB.IDRAMO ");
		queryString.append(" LEFT OUTER JOIN MIDAS.TCSUBRAMO TCS ");
		queryString.append(" ON TRIM(TCS.CODIGOSUBRAMO) = TRIM(TO_NUMBER(TRCB.IDRAMO||'0'||TRCB.IDSUBRAMO)) ");
		queryString.append(" AND TCS.IDTCRAMO = TCR.IDTCRAMO ");
		queryString.append(" LEFT OUTER JOIN  SEYCOS.POL_RECIBO rec ");
		queryString.append(" ON REC.ID_RECIBO = TRCB.IDRECIBO ");
		queryString.append(" AND rec.F_TER_REG=to_date('31/12/4712','DD/MM/YYYY') ");
		queryString.append(" LEFT OUTER JOIN SEYCOS.POL_POLIZA p ");
		queryString.append(" ON p.ID_COTIZACION = TRCB.IDCOTIZACION ");
		queryString.append(" AND p.id_version_pol = 1 ");
		queryString.append(" WHERE TCB.ID = ? ");
		queryString.append(" GROUP BY rec.ID_RECIBO, ");
		queryString.append(" REC.NUM_FOLIO_RBO, ");
		queryString.append(" TDCB.IDBENEFICIARIO, ");
		queryString.append(" TDCB.BENEFICIARIO, ");
		queryString.append(" (lpad(TRIM(TO_CHAR(p.id_centro_emis)),3,0)||'-'||lpad(TRIM(TO_CHAR(p.num_poliza)),10,0)||'-'||lpad(TRIM(TO_CHAR(p.num_renov_pol)),2,0)), ");
		queryString.append(" TCR.DESCRIPCIONRAMO, ");
		queryString.append(" TCS.DESCRIPCIONSUBRAMO, ");
		queryString.append(" (case when TRCB.PCTSINIESTRALIDAD is not null then TRCB.PCTSINIESTRALIDAD/100 end), ");
		queryString.append(" (case when TRCB.PCTCRECIMIENTO is not null then TRCB.PCTCRECIMIENTO/100 end), ");
		queryString.append(" TRCB.PCTSUPERACIONMETA, ");
		queryString.append(" TDCB.IMPORTEISR, ");
		queryString.append(" TDCB.IMPORTEIVA, ");
		queryString.append(" TDCB.IMPORTEIVARET, ");
		queryString.append(" TRCB.PORCENTAJEBONO, ");
		queryString.append(" (case ");
		queryString.append(" when TRIM(rec.CVE_T_RECIBO) = 'REC' then 'Recibo' ");
		queryString.append(" when TRIM(rec.CVE_T_RECIBO) = 'NCR' then 'Nota de Credito' ");
		queryString.append(" when TRIM(rec.CVE_T_RECIBO) = 'RD'  then 'Recibo de Disminución' ");
		queryString.append(" end) ");
		parametros.put(1, calculoBono.getId());
				
		Query query = entityManager.createNativeQuery(getQueryString(queryString), DetalleBonoPolizaView.class);
		if(!parametros.isEmpty()){
			for(Integer key:parametros.keySet()){
				query.setParameter(key,parametros.get(key));
			}
		}
		lista = query.getResultList();
		return lista;
	}
	
	@Override
	public List<DetalleBonoPolizaView> obtenerDetalleBonoNivelCobertura(CalculoBono calculoBono) { 
		Long idTipoBeneficiario = getTipoBeneficiario();
		StringBuilder queryString = new StringBuilder();
		Map<Integer,Object> parametros=new HashMap<Integer, Object>();
		List<DetalleBonoPolizaView>lista = new ArrayList<DetalleBonoPolizaView>();
		queryString.append(" SELECT /*+INDEX (REC REP_PREV_BONO_IDX) ");
		queryString.append(" INDEX (P PK_POL_POLIZA) ");
		queryString.append(" INDEX (TCR REP_PREV_BONO_IDX2) ");
		queryString.append(" INDEX (TCS TCSUBRAMO_2_UI) ");
		queryString.append(" */ ");
		queryString.append(" TRCB.ID idCalculoBono, ");
		queryString.append(" REC.NUM_FOLIO_RBO AS numRecibo, ");
		queryString.append(" TRCB.IDCOBERTURA idCobertura, ");
		queryString.append(" COB.DESCRIPCIONCOBERTURA cobertura, ");
		queryString.append(" TDCB.IDBENEFICIARIO idBeneficiario, ");
		queryString.append(" TDCB.BENEFICIARIO nombreBeneficiario, ");
		queryString.append(" (lpad(p.id_centro_emis,3,0)||'-'||lpad(p.num_poliza,10,0)||'-'||lpad(p.num_renov_pol,2,0)) numeroPoliza, ");
		queryString.append(" TRCB.IMPORTEBONO importeBono, ");
		queryString.append(" TRCB.PRIMANETA importePrima, ");
		queryString.append(" TCR.DESCRIPCIONRAMO descripcionRamo, ");
		queryString.append(" TCS.DESCRIPCIONSUBRAMO descripcionSubRamo, ");
		queryString.append(" (case when TRCB.PCTSINIESTRALIDAD is not null then TRCB.PCTSINIESTRALIDAD/100 end) porcentajeSiniestralidad, ");
		queryString.append(" (case when TRCB.PCTCRECIMIENTO IS NOT NULL then TRCB.PCTCRECIMIENTO/100 end) porcentajeCrecimiento, ");
		queryString.append(" (case when TRCB.PCTSUPERACIONMETA IS NOT NULL then TRCB.PCTSUPERACIONMETA/100 end)  porcentajeSuperacionMeta, ");
		queryString.append(" ((case when  TRCB.tipobeneficiario = "+idTipoBeneficiario+" then ");
		queryString.append(" (case when TRCB.SUPERAPRODMINIMAAGTE = 1 AND TRCB.SUPERASINAGTE = 1 AND TRCB.APLICAPAGOXTIPOPRODUCCION = 1 AND TRCB.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND TRCB.EXCL_TIPO_PERSONA = 1 AND TRCB.EXCL_FECHA_VTO_CEDULA = 1 AND TRCB.EXCL_DIASANTES_VTO_CEDULA = 1 AND TRCB.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND TRCB.EXCL_INCISOS_MAY_IGUAL = 1 AND TRCB.EXCL_CLAVE_AMIS = 1 AND TRCB.EXCL_TIPO_MONEDA = 1 AND TRCB.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND TRCB.EXCL_DESCUENTO_MAY_IGUAL = 1 AND TRCB.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" TRCB.primaneta ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end) ");
		queryString.append(" else ");
		queryString.append(" (case when TRCB.SUPERAPRODMINIMAPROM = 1 AND TRCB.SUPERASINPROM = 1 AND TRCB.APLICAPAGOXTIPOPRODUCCION = 1 AND TRCB.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND TRCB.EXCL_TIPO_PERSONA = 1 AND TRCB.EXCL_FECHA_VTO_CEDULA = 1 AND TRCB.EXCL_DIASANTES_VTO_CEDULA = 1 AND TRCB.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND TRCB.EXCL_INCISOS_MAY_IGUAL = 1 AND TRCB.EXCL_CLAVE_AMIS = 1 AND TRCB.EXCL_TIPO_MONEDA = 1 AND TRCB.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND TRCB.EXCL_DESCUENTO_MAY_IGUAL = 1 AND TRCB.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" TRCB.primaneta ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end) ");
		queryString.append(" end)) as primaNetaBono, ");
		queryString.append(" TDCB.IMPORTEISR, ");
		queryString.append(" TDCB.IMPORTEIVA, ");
		queryString.append(" TDCB.IMPORTEIVARET, ");
		queryString.append(" ((CASE WHEN TRCB.EXCL_TIPO_PERSONA = 0 THEN ");
		queryString.append(" '| EXCLUSION TIPO PERSONA ' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_TIPO_MONEDA = 0 THEN ");
		queryString.append(" ' | EXCLUSION TIPO MONEDA' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_TIPO_CEDULA = 0 THEN ");
		queryString.append(" '| EXCLUSION TIPO CEDULA' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_TIPO_BONO = 0 THEN ");
		queryString.append(" ' | EXCLUSION TIPO BONO' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_SOBRECOMISION = 0 THEN ");
		queryString.append(" ' | EXCLUSION SOBRECOMISION' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_FECHA_VTO_CEDULA = 0 THEN ");
		queryString.append(" ' | EXCLUSION FECHA VENCIMIENTO CEDULA' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_INCISOS_MAY_IGUAL = 0 THEN ");
		queryString.append(" ' | EXCLUSION INCISO MAYOR IGUAL' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_ENTREGA_FACTURA = 0 THEN ");
		queryString.append(" ' | EXCLUSION ENTREGA FACTURA' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_DIASANTES_VTO_CEDULA = 0 THEN ");
		queryString.append(" ' | EXCLUSION DIAS ANTES VENCIMIENTO CEDULA' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_DESCUENTO_MAY_IGUAL = 0 THEN ");
		queryString.append(" ' | EXCLUSION DESCUENTO MAYOR IGUAL' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_CLAVE_AMIS = 0 THEN ");
		queryString.append(" ' | EXCLUSION CLAVES AMIS' ");
		queryString.append(" END) || ");
		queryString.append(" (CASE WHEN TRCB.EXCL_PAGADA_NEGESP = 0 THEN ");
		queryString.append(" ' | EXCLUSION PAGADA NEG. ESP.' ");
		queryString.append(" END)) AS exclusion, ");
		queryString.append(" TRCB.PORCENTAJEBONO AS pcteBono, ");
		queryString.append(" (case ");
		queryString.append(" when rec.CVE_T_RECIBO = 'REC' then 'Recibo' ");
		queryString.append(" when rec.CVE_T_RECIBO = 'NCR' then 'Nota de Credito' ");
		queryString.append(" when rec.CVE_T_RECIBO = 'RD'  then 'Recibo de Disminución' ");
		queryString.append(" end) as tipoRecibo ");
		queryString.append(" FROM MIDAS.TOCALCULOBONO TCB ");
		queryString.append(" INNER JOIN MIDAS.TODETALLECALCULOBONO TDCB ");
		queryString.append(" ON  TDCB.IDCALCULO = TCB.ID ");
		queryString.append(" INNER JOIN  MIDAS.TORECIBOCALCULOBONOS TRCB ");
		queryString.append(" ON TRCB.IDDETALLECALCULOBONOS  = TDCB.ID ");
		queryString.append(" LEFT OUTER JOIN MIDAS.TOCOBERTURA COB ");
		queryString.append(" ON COB.IDTOCOBERTURA = TRCB.IDCOBERTURA ");
		queryString.append(" LEFT JOIN MIDAS.TCRAMO TCR ");
		queryString.append(" ON TCR.CODIGORAMO = TRCB.IDRAMO ");
		queryString.append(" LEFT OUTER JOIN MIDAS.TCSUBRAMO TCS ");
		queryString.append(" ON TCS.CODIGOSUBRAMO  =  TO_NUMBER((TRIM(TRCB.IDRAMO)||'0'||TRIM(TRCB.IDSUBRAMO))) ");
		queryString.append("  AND TCS.IDTCRAMO = TCR.IDTCRAMO ");
		queryString.append(" LEFT OUTER JOIN SEYCOS.POL_POLIZA p ");
		queryString.append(" ON p.ID_COTIZACION = TRCB.IDCOTIZACION ");
		queryString.append(" AND p.id_version_pol = 1 ");
		queryString.append(" LEFT OUTER JOIN SEYCOS.POL_RECIBO rec ");
		queryString.append(" ON REC.ID_RECIBO = TRCB.IDRECIBO ");
		queryString.append("  AND rec.F_TER_REG = to_date('31/12/4712','DD/MM/YYYY') ");
		queryString.append(" WHERE TCB.ID = ? ");
		
		parametros.put(1, calculoBono.getId());
		
		Query query = entityManager.createNativeQuery(getQueryString(queryString), DetalleBonoPolizaView.class);
		if(!parametros.isEmpty()){
			for(Integer key:parametros.keySet()){
				query.setParameter(key,parametros.get(key));
			}
		}
		lista = query.getResultList();
		return lista;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void enviarAListadoEjecucionCalculoBonoManual(ProgramacionBono programacionBono, List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonoList,
			Usuario usuario, Boolean isManual) throws MidasException{
			
			if(isNull(configuracionBonoList)){
				onError("No hay calculos para enviar a ejecutar");
			}
			
			try {
				ValorCatalogoAgentes modoEjecucion = catalogoService.obtenerElementoEspecifico("Modos de Ejecucion de Comisiones", "Manual");
				ValorCatalogoAgentes estatusEjecucion = catalogoService.obtenerElementoEspecifico("Estatus de Ejecucion Calculos Bono-Provision", "En Espera");
				
				
				for(ConfigBonos config:configuracionBonoList){		
					if(config!=null && config.getId()!=null){
						CalculoBonoEjecuciones ejecuciones = new CalculoBonoEjecuciones();
						ConfigBonos configuracion = new ConfigBonos();
						configuracion=entidadService.findById(ConfigBonos.class, config.getId());
						ejecuciones.setEsNegocioEspecial((long) 0);
						ejecuciones.setIdProgramacion(null);
						ejecuciones.setIdConfiguracion(config.getId());					
						ejecuciones.setNombreConfiguracionBono(configuracion.getDescripcion());
						ejecuciones.setModoEjecucion(modoEjecucion);
						ejecuciones.setEstatusEjecucion(estatusEjecucion);
						ejecuciones.setFechaEjecucion(new Date());
						ejecuciones.setIdCalculoBono(null);
						ejecuciones.setFechaFinEjecucion(null);
						ejecuciones.setUsuario(usuario.getNombreUsuario());
						entidadService.save(ejecuciones);
					}
				}
				
				
				
				//una vez guardado la lista de bonos a ejecutar se dispara el inicio de los calculos
				StoredProcedureHelper storedHelper = null;
				String sp="MIDAS.PKGCALCULOS_AGENTES.iniciaCalculoBonosManual";				
				LogDeMidasInterfaz.log("Entrando a Inicio de Ejecucion de bonos..." + this, Level.INFO, null);
				storedHelper = new StoredProcedureHelper(sp);
				storedHelper.ejecutaActualizar();
				LogDeMidasInterfaz.log("Saliendo de Ejecucion de bonos..." + this, Level.INFO, null);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
				
	}

	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> arg0, String arg1, Map<String, Object> arg2,
			Map<String, Object> arg3, StringBuilder arg4, StringBuilder arg5) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> arg0, Map<String, Object> arg1, Map<String, Object> arg2,
			StringBuilder arg3, StringBuilder arg4) {
		// TODO Auto-generated method stub
		return null;
	}




	@Override
	public List<DetalleBonoRamoSubramoView> obtenerDetalleBonoRamoSubramo(CalculoBono calculo){
		Long idTipoBeneficiario = getTipoBeneficiario();
		List<DetalleBonoRamoSubramoView> listado = new ArrayList<DetalleBonoRamoSubramoView>();
		
		StringBuilder queryString = new StringBuilder("");
		queryString.append(" select  rownum as id, ");
		queryString.append(" orden, ");
		queryString.append(" nombreBeneficiario, ");
		queryString.append(" idBeneficiario, ");
		queryString.append(" descripcionRamo, ");
		queryString.append(" descripcionSubRamo, ");
		queryString.append(" primaNeta, ");
		queryString.append(" pcteBono, ");
		queryString.append(" bonoPagar, ");
		queryString.append(" primaNetaBono, ");
		queryString.append(" importeisr, ");
		queryString.append(" importeiva, ");
		queryString.append(" importeivaret, ");
		queryString.append(" pctsiniestralidad, ");
		queryString.append(" pctcrecimiento, ");
		queryString.append(" primaNetaPerodoCoparacion ");
		queryString.append(" from( ");
		queryString.append(" select /*+INDEX (REC IDX_TORECIBOCALCULOBONOS_01)");
		queryString.append(" INDEX (CAL REPBONO_X_GER_TOCALCULOBONOIDX)");
		queryString.append(" INDEX (PROG REPBONO_X_GER_PROG_BONO)");
		queryString.append(" */");
		queryString.append(" null as orden, det.beneficiario as nombreBeneficiario, ");
		queryString.append(" det.idbeneficiario as idBeneficiario, ");
		queryString.append(" ram.descripcionRamo as descripcionRamo,");
		queryString.append(" sub.descripcionSubramo as descripcionSubRamo, ");
		queryString.append(" sum(primaneta) as primaNeta, ");
		queryString.append(" case ");
		queryString.append(" when config.claveModoAplicacion=836 then ");//--modo porcentaje
		queryString.append(" case when sum(importebono) <> 0 THEN ");
		queryString.append(" MAX(porcentajebono) ");
		queryString.append(" ELSE ");
		queryString.append(" 0 ");
		queryString.append(" END ");
		queryString.append(" when config.claveModoAplicacion=837 then ");//--modo directo	
		queryString.append(" 0 ");
		queryString.append(" end  as pcteBono,	");
		queryString.append(" sum(rec.importebono) bonoPagar, ");
		queryString.append(" sum(case when  rec.tipobeneficiario = 1428 then ");//--Agente 
		queryString.append(" case when rec.SUPERAPRODMINIMAAGTE = 1 AND rec.SUPERASINAGTE = 1 AND rec.APLICAPAGOXTIPOPRODUCCION = 1 AND rec.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND rec.EXCL_TIPO_PERSONA = 1 AND rec.EXCL_FECHA_VTO_CEDULA = 1 AND rec.EXCL_DIASANTES_VTO_CEDULA = 1 AND rec.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND rec.EXCL_INCISOS_MAY_IGUAL = 1 AND rec.EXCL_CLAVE_AMIS = 1 AND rec.EXCL_TIPO_MONEDA = 1 AND rec.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND rec.EXCL_DESCUENTO_MAY_IGUAL = 1 AND rec.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" primaneta ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end ");
		queryString.append(" else ");//--promotoria
		queryString.append(" case when rec.SUPERAPRODMINIMAPROM = 1 AND rec.SUPERASINPROM = 1 AND rec.APLICAPAGOXTIPOPRODUCCION = 1 AND rec.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND rec.EXCL_TIPO_PERSONA = 1 AND rec.EXCL_FECHA_VTO_CEDULA = 1 AND rec.EXCL_DIASANTES_VTO_CEDULA = 1 AND rec.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND rec.EXCL_INCISOS_MAY_IGUAL = 1 AND rec.EXCL_CLAVE_AMIS = 1 AND rec.EXCL_TIPO_MONEDA = 1 AND rec.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND rec.EXCL_DESCUENTO_MAY_IGUAL = 1 AND rec.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" primaneta ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end ");
		queryString.append(" end) as primaNetaBono, ");
		queryString.append(" NULL AS importeisr, ");
		queryString.append(" NULL AS importeiva, ");
		queryString.append(" NULL AS importeivaret, ");
		queryString.append(" NULL AS pctsiniestralidad, ");
		queryString.append(" NULL AS pctcrecimiento, ");
		queryString.append(" NULL AS primaNetaPerodoCoparacion ");
		queryString.append(" from midas.torecibocalculobonos rec ");
		queryString.append(" inner join midas.todetallecalculobono det on(rec.iddetallecalculobonos = det.id) ");
		queryString.append(" inner join midas.tocalculobono cal on (cal.id=det.idCalculo) ");
		queryString.append(" inner join midas.toprogramacionbono prog on(prog.id=cal.IDPROGRAMACIONBONO) ");
		queryString.append(" inner join midas.toconfigbonos config on(config.id=prog.idBono) ");
		queryString.append(" inner join midas.tcramo ram on(ram.codigoRamo=rec.idramo) ");
		queryString.append(" inner join midas.tcsubramo sub on(sub.codigoSubramo = ram.codigoRamo||lpad(rec.idSubramo,2,0)) ");
		queryString.append(" where det.idcalculo="+calculo.getId());
		queryString.append(" and (ram.descripcionRamo is not null and sub.descripcionSubramo is not null) ");
		queryString.append(" group by det.beneficiario,det.idbeneficiario,ram.descripcionRamo,config.claveModoAplicacion,det.IMPORTEISR,det.IMPORTEIVA,det.IMPORTEIVARET,PCTSINIESTRALIDAD,PCTCRECIMIENTO,REC.IDRAMO, rollup(sub.descripcionSubramo) ");
		queryString.append(" union all ");
		queryString.append(" SELECT  /*+INDEX (REC IDX_TORECIBOCALCULOBONOS_01)");
		queryString.append(" INDEX (CCB REPBONO_X_GER_CRECCALCBONOIDX)");
		queryString.append(" */ ");
		queryString.append(" DISTINCT 1 as orden, det.beneficiario as nombreBeneficiario, ");
		queryString.append(" det.idbeneficiario as idBeneficiario, ");
		queryString.append(" NULL as descripcionRamo, ");
		queryString.append(" NULL AS descripcionSubRamo, ");
		queryString.append(" sum(primaneta) as primaNeta, ");
		queryString.append(" MAX(porcentajebono) AS pcteBono, ");
		queryString.append(" sum(rec.importebono) bonoPagar, ");
		queryString.append(" sum(case when  rec.tipobeneficiario = "+idTipoBeneficiario+" then ");//--Agente
		queryString.append(" case when rec.SUPERAPRODMINIMAAGTE = 1 AND rec.SUPERASINAGTE = 1 AND rec.APLICAPAGOXTIPOPRODUCCION = 1 AND rec.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND rec.EXCL_TIPO_PERSONA = 1 AND rec.EXCL_FECHA_VTO_CEDULA = 1 AND rec.EXCL_DIASANTES_VTO_CEDULA = 1 AND rec.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND rec.EXCL_INCISOS_MAY_IGUAL = 1 AND rec.EXCL_CLAVE_AMIS = 1 AND rec.EXCL_TIPO_MONEDA = 1 AND rec.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND rec.EXCL_DESCUENTO_MAY_IGUAL = 1 AND rec.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" primaneta ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end ");
		queryString.append(" else ");//--promotoria	
		queryString.append(" case when rec.SUPERAPRODMINIMAPROM = 1 AND rec.SUPERASINPROM = 1 AND rec.APLICAPAGOXTIPOPRODUCCION = 1 AND rec.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND rec.EXCL_TIPO_PERSONA = 1 AND rec.EXCL_FECHA_VTO_CEDULA = 1 AND rec.EXCL_DIASANTES_VTO_CEDULA = 1 AND rec.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND rec.EXCL_INCISOS_MAY_IGUAL = 1 AND rec.EXCL_CLAVE_AMIS = 1 AND rec.EXCL_TIPO_MONEDA = 1 AND rec.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND rec.EXCL_DESCUENTO_MAY_IGUAL = 1 AND rec.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" primaneta ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end ");
		queryString.append(" end) as primaNetaBono, ");
		queryString.append(" det.IMPORTEISR as IMPORTEISR, ");
		queryString.append(" det.IMPORTEIVA as IMPORTEIVA, ");
		queryString.append(" det.IMPORTEIVARET as IMPORTEIVARET, ");
		queryString.append(" case when PCTSINIESTRALIDAD is not null then PCTSINIESTRALIDAD/100 end  as PCTSINIESTRALIDAD, ");
		queryString.append(" case when PCTCRECIMIENTO is not null then PCTCRECIMIENTO/100 end  as PCTCRECIMIENTO, ");
		queryString.append(" (select sum(importe) primanetaanterior ");
		queryString.append(" from  midas.tocrecimientoCalculoBono ccb ");
		queryString.append(" where ccb.IDCALCULOBONO = "+calculo.getId()+" and ( ccb.idAgente= idBeneficiario  or ccb.idpromotoria = IDBENEFICIARIO ) ");//EC.IDRAMO= 4
		queryString.append(" ) as primaNetaPerodoCoparacion ");
		queryString.append(" from midas.torecibocalculobonos rec ");
		queryString.append(" inner join midas.todetallecalculobono det on(rec.iddetallecalculobonos = det.id) ");
		queryString.append(" where det.idcalculo="+calculo.getId());
		queryString.append(" group by  DET.idBeneficiario, det.beneficiario, det.IMPORTEISR, det.IMPORTEIVA, det.IMPORTEIVARET, PCTSINIESTRALIDAD, PCTCRECIMIENTO ");
		queryString.append(" ) order by idBeneficiario,orden,descripcionRamo,descripcionSubramo desc ");
		
		Query query = entityManager.createNativeQuery(queryString.toString(), DetalleBonoRamoSubramoView.class);
		listado = query.getResultList();
		return listado;
	}
			
	@Override
	public List<DetalleBonoPolizaView> getDetalleBonosNivelAmisExcel(CalculoBono calculoBono){
		Long idTipoBeneficiario = getTipoBeneficiario();
		StringBuilder queryString = new StringBuilder();
		Map<Integer,Object> parametros=new HashMap<Integer, Object>();
		List<DetalleBonoPolizaView>lista = new ArrayList<DetalleBonoPolizaView>();
		queryString.append(" SELECT /*+ USE_NL_WITH_INDEX(TCS TCSUBRAMO_2_UI) USE_NL_WITH_INDEX(TCR TCRAMO_CODIGO_UK) USE_NL_WITH_INDEX(COB TOCOBERTURA_PK) USE_NL_WITH_INDEX(AMIS IDX_AMIS_CALCULOBONO) */ TRCB.ID idCalculoBono, ");
		queryString.append(" TRCB.IDCOBERTURA idCobertura, ");
		queryString.append(" REC.NUM_FOLIO_RBO AS numRecibo, ");
		queryString.append(" COB.DESCRIPCIONCOBERTURA cobertura, ");
		queryString.append(" TDCB.IDBENEFICIARIO idBeneficiario, ");
		queryString.append(" TDCB.BENEFICIARIO nombreBeneficiario, ");
		queryString.append(" lpad(p.id_centro_emis,3,0)||'-'||lpad(p.num_poliza,10,0)||'-'||lpad(p.num_renov_pol,2,0) numeroPoliza, ");
		queryString.append(" trunc(TRCB.IMPORTEBONO,2) importeBono, ");
		queryString.append(" AMIS.IMPORTEPRIMANETA importePrima, "); //--TRCB.PRIMANETA 
		queryString.append(" TCR.DESCRIPCIONRAMO descripcionRamo, ");
		queryString.append(" TCS.DESCRIPCIONSUBRAMO descripcionSubRamo, ");
		queryString.append(" TRCB.PCTSINIESTRALIDAD porcentajeSiniestralidad, ");
		queryString.append(" TRCB.PCTCRECIMIENTO porcentajeCrecimiento, ");
		queryString.append(" TRCB.PCTSUPERACIONMETA porcentajeSuperacionMeta, ");
		queryString.append(" case when  TRCB.tipobeneficiario = "+idTipoBeneficiario+" then ");//--Agente 
		queryString.append(" case when TRCB.SUPERAPRODMINIMAAGTE = 1 AND TRCB.SUPERASINAGTE = 1 AND TRCB.APLICAPAGOXTIPOPRODUCCION = 1 AND TRCB.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND TRCB.EXCL_TIPO_PERSONA = 1 AND TRCB.EXCL_FECHA_VTO_CEDULA = 1 AND TRCB.EXCL_DIASANTES_VTO_CEDULA = 1 AND TRCB.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND TRCB.EXCL_INCISOS_MAY_IGUAL = 1 AND TRCB.EXCL_CLAVE_AMIS = 1 AND TRCB.EXCL_TIPO_MONEDA = 1 AND TRCB.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND TRCB.EXCL_DESCUENTO_MAY_IGUAL = 1 AND TRCB.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" AMIS.IMPORTEPRIMANETA ");
		queryString.append(" else 	");
		queryString.append(" 0 ");
		queryString.append(" end ");
		queryString.append(" else ");//--promotoria
		queryString.append(" case when TRCB.SUPERAPRODMINIMAPROM = 1 AND TRCB.SUPERASINPROM = 1 AND TRCB.APLICAPAGOXTIPOPRODUCCION = 1 AND TRCB.EXCL_ENTREGA_FACTURA = 1 ");
		queryString.append(" AND TRCB.EXCL_TIPO_PERSONA = 1 AND TRCB.EXCL_FECHA_VTO_CEDULA = 1 AND TRCB.EXCL_DIASANTES_VTO_CEDULA = 1 AND TRCB.EXCL_TIPO_CEDULA = 1 ");
		queryString.append(" AND TRCB.EXCL_INCISOS_MAY_IGUAL = 1 AND TRCB.EXCL_CLAVE_AMIS = 1 AND TRCB.EXCL_TIPO_MONEDA = 1 AND TRCB.EXCL_SOBRECOMISION = 1 ");
		queryString.append(" AND TRCB.EXCL_DESCUENTO_MAY_IGUAL = 1 AND TRCB.EXCL_TIPO_BONO = 1 then ");
		queryString.append(" AMIS.IMPORTEPRIMANETA ");
		queryString.append(" else ");
		queryString.append(" 0 ");
		queryString.append(" end ");
		queryString.append(" end as primaNetaBono, ");
		queryString.append(" TDCB.IMPORTEISR, ");
		queryString.append(" TDCB.IMPORTEIVA, ");
		queryString.append(" TDCB.IMPORTEIVARET, ");
		queryString.append(" CASE WHEN TRCB.EXCL_TIPO_PERSONA = 0 THEN ");
		queryString.append(" '| EXCLUSION TIPO PERSONA ' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_TIPO_MONEDA = 0 THEN ");
		queryString.append(" ' | EXCLUSION TIPO MONEDA' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_TIPO_CEDULA = 0 THEN ");
		queryString.append(" '| EXCLUSION TIPO CEDULA' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_TIPO_BONO = 0 THEN ");
		queryString.append(" ' | EXCLUSION TIPO BONO' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_SOBRECOMISION = 0 THEN ");
		queryString.append(" ' | EXCLUSION SOBRECOMISION' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_FECHA_VTO_CEDULA = 0 THEN ");
		queryString.append(" ' | EXCLUSION FECHA VENCIMIENTO CEDULA' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_INCISOS_MAY_IGUAL = 0 THEN ");
		queryString.append(" ' | EXCLUSION INCISO MAYOR IGUAL' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_ENTREGA_FACTURA = 0 THEN ");
		queryString.append(" ' | EXCLUSION ENTREGA FACTURA' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_DIASANTES_VTO_CEDULA = 0 THEN ");
		queryString.append(" ' | EXCLUSION DIAS ANTES VENCIMIENTO CEDULA' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_DESCUENTO_MAY_IGUAL = 0 THEN ");
		queryString.append(" ' | EXCLUSION DESCUENTO MAYOR IGUAL' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_CLAVE_AMIS = 0 THEN ");
		queryString.append(" ' | EXCLUSION CLAVES AMIS' ");
		queryString.append(" END || ");
		queryString.append(" CASE WHEN TRCB.EXCL_PAGADA_NEGESP = 0 THEN ");
		queryString.append(" ' | EXCLUSION PAGADA NEG. ESP.' ");
		queryString.append(" END AS exclusion ");
		queryString.append(" IDESTILO as idestilo ");
		queryString.append(" FROM MIDAS.TOCALCULOBONO TCB ");
		queryString.append(" JOIN MIDAS.TODETALLECALCULOBONO TDCB ON TCB.ID = TDCB.IDCALCULO ");
		queryString.append(" JOIN MIDAS.TORECIBOCALCULOBONOS TRCB ON TDCB.ID = TRCB.IDDETALLECALCULOBONOS ");
		queryString.append(" LEFT JOIN MIDAS.TOCOBERTURA COB on COB.IDTOCOBERTURA = TRCB.IDCOBERTURA ");
		queryString.append(" LEFT JOIN MIDAS.TCRAMO TCR ON TRCB.IDRAMO = TCR.CODIGORAMO ");
		queryString.append(" LEFT JOIN MIDAS.TCSUBRAMO TCS ON TO_NUMBER(TRCB.IDRAMO||'0'||TRCB.IDSUBRAMO)  = TCS.CODIGOSUBRAMO 	");
		queryString.append(" AND TCR.IDTCRAMO = TCS.IDTCRAMO ");
		queryString.append(" LEFT JOIN SEYCOS.POL_POLIZA p ON p.ID_COTIZACION = TRCB.IDCOTIZACION and p.id_version_pol = 1 ");
		queryString.append(" LEFT JOIN SEYCOS.POL_RECIBO rec on REC.ID_RECIBO = TRCB.IDRECIBO and rec.F_TER_REG=to_date('31/12/4712','DD/MM/YYYY')  AND TCR.IDTCRAMO = TCS.IDTCRAMO ");
		queryString.append(" LEFT JOIN  MIDAS.TOCLAVESAMIS_CALCULOBONO AMIS ON AMIS.IDCALCULO = TCB.ID and AMIS.IDTOAGENTEMOVIMIENTOS = TRCB.IDTOAGENTEMOVIMIENTO ");
		queryString.append(" WHERE TCB.ID =? ");
		
		parametros.put(1, calculoBono.getId());
		
		Query query = entityManager.createNativeQuery(getQueryString(queryString), DetalleBonoPolizaView.class);
		if(!parametros.isEmpty()){
			for(Integer key:parametros.keySet()){
				query.setParameter(key,parametros.get(key));
			}
		}
		lista = query.getResultList();
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<DetalleCalculoBono> listarDetalleDeCalculoPorAutorizar(Long idCalculo) {
		
		List<DetalleCalculoBono> list = new ArrayListNullAware<DetalleCalculoBono>();
		
		List<DetalleCalculoBono> list2 = new ArrayListNullAware<DetalleCalculoBono>();
		
		Map<String,Object> params = new HashMap<String, Object>();
		StringBuilder queryString = new StringBuilder("");
		queryString.append("select model from DetalleCalculoBono model ");
		addCondition(queryString, "model.calculoBono.id =:idCalculo");
		addCondition(queryString, "model.status.valor NOT IN(:claveEstatus,:claveEstatus2)");
		addCondition(queryString, "model.bonoPagar <> 0");
		params.put("idCalculo", idCalculo);
		params.put("claveEstatus", EstatusCalculoBonos.AUTORIZADO.getValue());
		params.put("claveEstatus2", EstatusCalculoBonos.APLICADO.getValue());
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		
		params = new HashMap<String, Object>();
		queryString = new StringBuilder("");
		queryString.append("select model from DetalleCalculoBono model ");
		addCondition(queryString, "model.calculoBono.id =:idCalculo");
		addCondition(queryString, "model.status IS NULL");
		addCondition(queryString, "model.bonoPagar <> 0");
		params.put("idCalculo", idCalculo);
		query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list2=query.getResultList();
		
		list.addAll(list2);
		
		return list;
		
		
	}
	
	@SuppressWarnings("unchecked")
	public List<HonorariosAgente> obtenerHonorariosFacturasPendientes (Long idAgente, Integer periodo) {
		
		List<HonorariosAgente> list = new ArrayListNullAware<HonorariosAgente>();
		
		List<HonorariosAgente> list2 = new ArrayListNullAware<HonorariosAgente>();
		
		String statusRechazada = "Rechazada";
		
		
		Map<String,Object> params = new HashMap<String, Object>();
		StringBuilder queryString = new StringBuilder("");
		
		queryString.append("select model from HonorariosAgente model ");
		addCondition(queryString, "model.idAgente = :idAgente");
		addCondition(queryString, "model.envioFactura.status.valor = :claveEstatus");
		addCondition(queryString, "model.anioMes >= 201501");
		addCondition(queryString, "model.anioMes <= :periodo");
		
		params.put("idAgente", idAgente);
		params.put("claveEstatus", statusRechazada);
		params.put("periodo", periodo);
		
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		
		params = new HashMap<String, Object>();
		queryString = new StringBuilder("");
		
		queryString.append("select model from HonorariosAgente model ");
		addCondition(queryString, "model.idAgente = :idAgente");
		addCondition(queryString, "model.envioFactura IS NULL");
		addCondition(queryString, "model.anioMes >= 201501");
		addCondition(queryString, "model.anioMes <= :periodo");
		
		params.put("idAgente", idAgente);
		params.put("periodo", periodo);
		
		query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list2=query.getResultList();
		
		list.addAll(list2);
		
		return list;
		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public ValorCatalogoAgentes obtenerClaveEstatusCalculo(EstatusCalculoBonos estatus) throws Exception {

		return catalogoService.obtenerElementoEspecifico(CATALOGO_ESTATUS_CALCULO, estatus.getValue());
		
	}
	
	@Override
	public Integer obtenerNumeroCalculosBonoPrevios (Long idCalculo) {
		
		StoredProcedureHelper storedHelper = null;
		
		try {
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS." + PKGCALCULOS_AGENTES + ".stp_conteoCalculosBonoPrevios", StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro("pIdCalculoBono", idCalculo);
			
			return Integer.valueOf(storedHelper.ejecutaActualizar());
			
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
		
	}
	
	private Long getTipoBeneficiario(){
		
		StringBuilder queryString= new StringBuilder();
		List<ValorCatalogoAgentes> lista = new ArrayList<ValorCatalogoAgentes>();
		queryString.append(" select * from MIDAS.toValorCatalogoAgentes where GRUPOCATALOGOAGENTES_ID  = (select id from MIDAS.tcgrupocatalogoagentes where descripcion like '%Tipo de Beneficiario%')");
		queryString.append(" and VALOR = 'Agente'");
		lista = this.entityManager.createNativeQuery(queryString.toString(), ValorCatalogoAgentes.class).getResultList();
		
		for(ValorCatalogoAgentes obj:lista){
			if(obj.getId()!=null){
				return obj.getId();
			}
		}
		
		return null;
	}
	
	private Long existeClaveAmisConfig(Long idCalculo){
		StringBuilder queryString = new StringBuilder();
		queryString.append("select count(*) from MIDAS.TOCLAVESAMIS_CALCULOBONO CACB WHERE CACB.IDCALCULO =  "+ idCalculo);
		BigDecimal count = (BigDecimal) this.entityManager.createNativeQuery(queryString.toString()).getSingleResult();
		return count.longValue();
	}
	
	private static Logger logger = Logger.getLogger(CalculoBonosDaoImpl.class);
	
}
