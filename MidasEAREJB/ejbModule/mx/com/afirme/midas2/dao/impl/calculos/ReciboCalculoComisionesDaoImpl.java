package mx.com.afirme.midas2.dao.impl.calculos;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;
import static mx.com.afirme.midas2.utils.CommonUtils.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.calculos.ReciboCalculoComisionesDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.ReciboCalculoComisiones;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.MidasException;
@Stateless
public class ReciboCalculoComisionesDaoImpl extends EntidadDaoImpl implements ReciboCalculoComisionesDao{
	private EntidadService entidadService;
	@Override
	public void deleteRecibosPorDetalleCalculo(Long idDetalle) throws MidasException {
		if(isNull(idDetalle)){
			onError("Favor de proporcionar la clave de detalle del recibo");
		}
		List<ReciboCalculoComisiones> lista=listarRecibosDeDetalleCalculo(idDetalle);
		if(!isEmptyList(lista)){
			for(ReciboCalculoComisiones recibo:lista){
				if(isNotNull(recibo) && isNotNull(recibo.getId())){
					deleteById(recibo.getId());
				}
			}
		}
	}
	
	private void deleteById(Long idRecibo) throws MidasException{
		if(isNull(idRecibo)){
			onError("Favor de proporcionar el ");
		}
		ReciboCalculoComisiones recibo=loadById(idRecibo);
		if(isNull(recibo)){
			onError("No existe el recibo con la clave "+idRecibo);
		}
		entidadService.remove(recibo);
	}

	@Override
	public void deleteRecibosPorDetalleCalculo(DetalleCalculoComisiones detalleCalculo)throws MidasException {
		if(isNull(detalleCalculo)){
			onError("Favor de proporcionar el detalle del calculo");
		}
		deleteRecibosPorDetalleCalculo(detalleCalculo.getId());
	}
	@Deprecated
	@Override
	public List<ReciboCalculoComisiones> findByFilters(ReciboCalculoComisiones recibo) {
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ReciboCalculoComisiones> listarRecibosDeDetalleCalculo(Long idDetalle)throws MidasException {
		List<ReciboCalculoComisiones> list=new ArrayList<ReciboCalculoComisiones>();
		if(isNull(idDetalle)){
			onError("Favor de proporcionar la clave del detalle del calculo");
		}
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ReciboCalculoComisiones model ");
		addCondition(queryString, "model.detalleCalculoComisiones.id = :idDetalleCalculo");
		params.put("idDetalleCalculo", idDetalle);
		Query query = entityManager.createQuery(getQueryString(queryString));
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}

	@Override
	public List<ReciboCalculoComisiones> listarRecibosDeDetalleCalculo(DetalleCalculoComisiones detalle) throws MidasException {
		if(isNull(detalle)){
			onError("Favor de proporcionar el detalle del calculo");
		}
		return listarRecibosDeDetalleCalculo(detalle.getId());
	}

	@Override
	public ReciboCalculoComisiones loadById(Long id) throws MidasException {
		if(isNull(id)){
			onError("Favor de proporcionar la clave del recibo a consultar");
		}
		ReciboCalculoComisiones recibo=entidadService.findById(ReciboCalculoComisiones.class,id);
		return recibo;
	}

	@Override
	public ReciboCalculoComisiones loadById(ReciboCalculoComisiones recibo)throws MidasException {
		if(isNull(recibo)){
			onError("Favor de proporcionar recibo del detalle del calculo a consultar");
		}
		return loadById(recibo.getId());
	}
	@Deprecated
	@Override
	public void rehabilitarRecibosPorDetalleCalculo(Long arg0)throws MidasException {
		// TODO Auto-generated method stub
	}
	
	@Deprecated
	@Override
	public void rehabilitarRecibosPorDetalleCalculo(DetalleCalculoComisiones arg0) throws MidasException {
		// TODO Auto-generated method stub
	}

	@Override
	public Long saveReciboCalculoComisiones(ReciboCalculoComisiones recibo)throws MidasException {
		if(isNull(recibo)){
			onError("Favor de proporcionar recibo del detalle del calculo a consultar");
		}
		ReciboCalculoComisiones jpaObject=entidadService.save(recibo);
		Long id=(isNotNull(jpaObject))?jpaObject.getId():null;
		return id;
	}
	@SuppressWarnings("unchecked")
	@Override
	public void generarRecibosCalculoComisiones(Long idCalculo) throws MidasException{
		List<ReciboCalculoComisiones> list=new ArrayList<ReciboCalculoComisiones>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		if(isNull(idCalculo)){
			onError("Favor de proporcionar la clave del calculo");
		}
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select    ");
		queryString.append(" MIDAS.idToReciboCalculoCom_seq.nextval as id,");
		queryString.append(" agente.idPromotoria as idPromotoria, ");
		queryString.append(" agente.id as idAgente, ");
		queryString.append(" movs.idrecibo as idRecibo,");
		queryString.append(" (nvl(movs.importeComisionAgente ,0) +nvl(movs.importeIva,0) - nvl(movs.importeIvaRet,0)-nvl(movs.importeIsr,0)) as importeCalculo,");
		queryString.append(" det.id as idDetalleCalculoComisiones ");
		queryString.append(" from MIDAS.toAgenteMovimientos movs  ");
		queryString.append(" inner join MIDAS.toAgente agente on(movs.idAgente=agente.idAgente) ");
		queryString.append(" inner join MIDAS.toDetalleCalculoComisiones det on(det.idAgente=agente.id and det.idCalculoComisiones="+idCalculo+")");
		queryString.append(" inner join ( ");
		queryString.append(" select id,valor from MIDAS.toValorCatalogoAgentes where grupoCatalogoAgentes_id in (select id from MIDAS.tcGrupoCatalogoAgentes where descripcion like 'Situacion Movimiento Agente') ");    
		queryString.append(" ) situacionMov on (upper(trim(situacionMov.valor)) like 'PENDIENTE' and situacionMov.id=movs.estatusMovimiento)    ");
		queryString.append(" where  claveComision=1 "); 
		Query query=entityManager.createNativeQuery(getQueryString(queryString),"detalleCalculoComisionesView");
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		list=query.getResultList();
		if(!isEmptyList(list)){
			for(ReciboCalculoComisiones recibo:list){
				if(isNotNull(recibo)){
					entityManager.detach(recibo);
					entidadService.save(recibo);
				}
			}
		}
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
