package mx.com.afirme.midas2.dao.impl.cobranza.reportes;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.cobranza.reportes.ReportesCobranzaDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.cobranza.reportes.CalendarioGonherDTO;
import mx.com.afirme.midas2.domain.cobranza.reportes.CargoAutoDomiTC;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.log4j.Logger;

@Stateless
public class ReportesCobranzaDaoImpl extends EntidadDaoImpl implements ReportesCobranzaDao{

	public static final Logger LOG = Logger.getLogger(ReportesCobranzaDaoImpl.class);

	@SuppressWarnings("unchecked")
	@Override
	public CalendarioGonherDTO getFechaSiguienteProgramacion(){
		StringBuilder queryString =null;	
		queryString = new StringBuilder(" SELECT calendario from " + CalendarioGonherDTO.class.getSimpleName() + " calendario ");
		queryString.append(" WHERE  calendario.fechaProgramacion = ( ");
		queryString.append(" SELECT Min(calendario2.fechaProgramacion) FROM " + CalendarioGonherDTO.class.getSimpleName() + " calendario2 ");
		queryString.append(" WHERE calendario2.fechaProgramacion >=  :fechaActual ");
		queryString.append(" AND calendario2.activo IS NOT NULL AND calendario2.activo = :activo) ");
		Map<String, Object> param = new HashMap<String, Object>(1);	
		param.put("fechaActual", new Date());
		param.put("activo", 1);
		List<CalendarioGonherDTO>  calendars =  this.entidadDao.executeQueryMultipleResultWithRefresh(queryString.toString(),param);
		CalendarioGonherDTO calendar = new CalendarioGonherDTO(); 
		if(CollectionUtils.isNotEmpty(calendars)){
			calendar = calendars.get(0);
		}
		return calendar;	
	}
	
	public void saveConfrimProgramacion(CalendarioGonherDTO calendarioGonher){
		CalendarioGonherDTO calendarioGonherDTO = entidadDao.findById(CalendarioGonherDTO.class, calendarioGonher.getIdCalendar());
		calendarioGonherDTO.setConfirmUser(calendarioGonher.getConfirmUser());
		calendarioGonherDTO.setIdUsuarioModif(usuarioService.getUsuarioActual().getNombreUsuario());
		calendarioGonherDTO.setFechaModif(new Date());
		entidadDao.update(calendarioGonherDTO);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CargoAutoDomiTC> getReporteCargoAutoDomiTC(){
		LOG.info("Entrando a getReporteCargoAutoDomiTC ");
		List<CargoAutoDomiTC> resultList = new ArrayList<CargoAutoDomiTC>(1);
		
		String sp = "SEYCOS.PKG_GONHER.SP_PREV_PROG_GONHER";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;
		try {
			String[] colsTemp = {"NUMERO_POLIZA",
					"NUMERO_RECIBO",
					"NUM_EXHIBICION",
					"NOMBRE_ASEGURADO",
					"TITULAR",
					"TIPO_COBRO",
					"F_CUBRE_DESDE",
					"F_CUBRE_HASTA",
					"IMP_PRIMA_TOTAL",
					"NOMBRE_BANCO",
					"NUMERO_CUENTA",
					"TYPE_COND_COBRO",
					"INTENTO"};
				String[] propsTemp = {"numeroPoliza",
						"numeroRecibo",
						"numeroExhibicion",
						"nombreAsegurado",
						"titular",
						"tipoCobro",
						"fechaCubreDesde",
						"fechaCubreHasta",
						"importeRecibo",
						"nombreBanco",
						"numeroCuenta",
						"tipoConductoCobro",
						"intento"};
				cols = colsTemp;
				props = propsTemp;
	
				for (int i = 0; i < cols.length; i++) {
					String columnName = cols[i];
					String propertyName = props[i];
					propiedades.append(propertyName);
					columnas.append(columnName);
					if (i < (cols.length - 1)) {
						propiedades.append(",");
						columnas.append(",");
					}
				}
				storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceMapeoResultados(CargoAutoDomiTC.class.getCanonicalName(), propiedades.toString(), columnas.toString());
				resultList = storedHelper.obtieneListaResultados();
			} catch (Exception e) {
				LOG.error("Excepcion general al invocar getReporteCargoAutoDomiTC: ", e);
			}
		return resultList;
	}
	
	@EJB
	private EntidadDao entidadDao;

	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
		
}