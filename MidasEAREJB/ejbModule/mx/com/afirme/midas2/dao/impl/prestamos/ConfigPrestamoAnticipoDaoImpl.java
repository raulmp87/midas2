package mx.com.afirme.midas2.dao.impl.prestamos;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.calculos.SolicitudChequesMizarDao.ClavesTransaccionesContables;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.prestamos.ConfigPrestamoAnticipoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentoEntidadFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.domain.prestamos.PagarePrestamoAnticipo;
import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.prestamos.ConfigPrestamoAnticipoView;
import mx.com.afirme.midas2.dto.prestamos.ReporteIngresosAgente;
import mx.com.afirme.midas2.service.calculos.InterfazMizarService;
import mx.com.afirme.midas2.service.calculos.SolicitudChequesMizarService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoEntidadFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.prestamos.ConfigPrestamoAnticipoService;
import mx.com.afirme.midas2.service.prestamos.PagarePrestamoAnticipoService;
import mx.com.afirme.midas2.util.MidasException;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;


@Stateless
public class ConfigPrestamoAnticipoDaoImpl extends EntidadDaoImpl implements ConfigPrestamoAnticipoDao{

	private EntidadService entidadService;
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	private PagarePrestamoAnticipoService pagarePrestamoAnticipoService;
	private AgenteMidasService agenteMidasService;
	private ValorCatalogoAgentesService catalogoService;
	private SolicitudChequesMizarService solicitudCheques;
	private InterfazMizarService interfazMizarService;
	private SolicitudChequeFacadeRemote solicitudChequeFacade;
	@EJB
	private ConfigPrestamoAnticipoService configPrestamoAnticipoService;
	private static String CLAVE_TRANSACCION_AGENTES="AGT  ";
	private static String CLAVE_CONCEPTO_TRANSACCION="PREAN";
	private static String USUARIO_TRANSACCION="MIDAS";
	private static String EMPRESA_DEFAULT="8";
	private static String CLAVE_CLASE_AGENTE="A";
	private static final String PKGCALCULOS_AGENTES="PKGCALCULOS_AGENTES";
	private static final String PKG_INT_MIDAS_E2="Pkg_int_midas_E2";
	private static final String PKG_INT_MIDAS="PKG_INT_MIDAS";
	private FortimaxService fortimaxService;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	private DocumentoEntidadFortimaxService documentoEntidadService;
	private List<DocumentoEntidadFortimax> listaDocFortimaxGuardados = new ArrayList<DocumentoEntidadFortimax>();
	
	@Override
	public List<ConfigPrestamoAnticipoView> findByFilters(ConfigPrestamoAnticipo filtro) throws Exception {
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		List<ConfigPrestamoAnticipoView> lista=new ArrayList<ConfigPrestamoAnticipoView>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" SELECT  distinct");
		queryString.append(" configPrestamo.ID as id,");
		queryString.append(" agente.IDAGENTE as idAgente,");
		queryString.append(" persona.NOMBRECOMPLETO as nombreCompreto,");
		queryString.append(" tipoMovimiento.VALOR as tipoMovimiento,");
		queryString.append(" configPrestamo.FECHAALTAMOVIMIENTO as fechaAltaMovimiento,");
		queryString.append(" estatus.VALOR as estatus,");
		queryString.append(" configPrestamo.IMPORTEOTORGADO as importeOtorgado,");
		queryString.append(" configPrestamo.IMPORTEPAGO as importePago,");
		queryString.append(" configPrestamo.NUMEROPAGOS as numeroPagos,");
//		queryString.append(" configPrestamo.FECHAINICIOCALCULO as fechaInicioCalculo,");
		queryString.append(" plazos.VALOR as plazos");
		queryString.append(" FROM MIDAS.TOCONFIGPRESTAMOANTICIPO configPrestamo");
		queryString.append(" INNER JOIN MIDAS.TOAGENTE agente ON  (agente.ID=configPrestamo.IDAGENTE)");
		queryString.append(" LEFT JOIN  MIDAS.VW_PERSONA persona ON (persona.IDPERSONA=agente.IDPERSONA)");
		queryString.append(" LEFT JOIN  MIDAS.TOVALORCATALOGOAGENTES estatus ON (estatus.ID = configPrestamo.CLAVEESTATUS)");
		queryString.append(" LEFT JOIN  MIDAS.TOVALORCATALOGOAGENTES plazos ON (plazos.ID = configPrestamo.IDPLAZO)");
		queryString.append(" LEFT JOIN  MIDAS.TOVALORCATALOGOAGENTES tipoMovimiento ON (tipoMovimiento.ID = configPrestamo.IDTIPOMOVIMIENTO)");
		queryString.append(" where ");
		if(filtro!=null){
			int index=1;
			if(filtro.getId()!=null){
				addCondition(queryString, " configPrestamo.id=? ");
				params.put(index, filtro.getId());
				index++;
			}
			if(filtro.getFechaDeAltaMovimiento()!=null){
				addCondition(queryString, " configPrestamo.fechaAltaMovimiento>=? ");
				params.put(index, filtro.getFechaDeAltaMovimiento());
				index++;
			}
			if(filtro.getFechaDeFinMovimiento()!=null){
				addCondition(queryString, " configPrestamo.fechaAltaMovimiento<=? ");
				params.put(index, filtro.getFechaDeFinMovimiento());
				index++;
			}
			if(filtro.getAgente()!=null){
				if(filtro.getAgente().getId()!=null){
					addCondition(queryString, " agente.idAgente=? ");
					params.put(index,  filtro.getAgente().getId());
					index++;
				}
				if(filtro.getAgente().getPersona()!=null){
					if(filtro.getAgente().getPersona().getNombreCompleto()!=" "){
						addCondition(queryString, " persona.nombreCompleto like ? ");
						params.put(index,  "%"+filtro.getAgente().getPersona().getNombreCompleto()+"%");
						index++;
					}
				}
			}
			if(filtro.getFechaInicioCalculo()!=null){
				addCondition(queryString, " configPrestamo.fechaInicioCalculo=? ");
				params.put(index,  filtro.getFechaInicioCalculo());
				index++;
			}
			if(filtro.getEstatus()!=null){
				if(filtro.getEstatus().getId()!=null){
					addCondition(queryString, " configPrestamo.claveEstatus=? ");
					params.put(index,  filtro.getEstatus().getId());
					index++;
				}
			}
			if(filtro.getPlazo()!=null && filtro.getPlazo().getId()!=null){
				addCondition(queryString, " configPrestamo.idPlazo=? ");
				params.put(index,  filtro.getPlazo().getId());
				index++;
			}
			if(filtro.getTipoMovimiento()!=null){
				if(filtro.getTipoMovimiento().getId()!=null){
					addCondition(queryString, " configPrestamo.IDTIPOMOVIMIENTO=? ");
					params.put(index,  filtro.getTipoMovimiento().getId());
					index++;
				}
			}
		}
		if(params.isEmpty()){
			int lengthWhere="where ".length();
			queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
		}
		String finalQuery=getQueryString(queryString)+" order by configPrestamo.id asc ";
		Query query=entityManager.createNativeQuery(finalQuery,ConfigPrestamoAnticipoView.class);
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
	
		lista=query.getResultList();
		return lista;
	}

	@Override
	public ConfigPrestamoAnticipo loadById(ConfigPrestamoAnticipo objConfigPrestamoAnticipo)throws Exception {
		
		ConfigPrestamoAnticipo objRetorno = new ConfigPrestamoAnticipo();
		if(objConfigPrestamoAnticipo!=null && objConfigPrestamoAnticipo.getId()!=null){
			objRetorno = entidadService.findById(ConfigPrestamoAnticipo.class, objConfigPrestamoAnticipo.getId());
		}
		return objRetorno;
	}

	/*Guardar Prestamo*/
	@Override
	public ConfigPrestamoAnticipo save(ConfigPrestamoAnticipo obj) throws Exception {		
		ConfigPrestamoAnticipo objConfigPrestamo = new ConfigPrestamoAnticipo();
		if(obj.getPlazoInteres().getId()==null){
			obj.setPlazoInteres(null); //se setea a null ya que el plazo de interes no es requerido
		}		
		objConfigPrestamo =	entidadService.save(obj);	
		updateEstatus(objConfigPrestamo, "PENDIENTE");
		return objConfigPrestamo;
	}
	
	/*Metodo para cambiar el estatus del prestamo o anticipo*/
	@Override
	public ConfigPrestamoAnticipo updateEstatus(ConfigPrestamoAnticipo configPrestamoAnticipo, String elementoCatalogo){
		
		ValorCatalogoAgentes estatus = new ValorCatalogoAgentes();
		ConfigPrestamoAnticipo objConfigPrestamoAnticipo = new ConfigPrestamoAnticipo();
		try {
			Agente agente = new Agente();
			objConfigPrestamoAnticipo=entidadService.findById(ConfigPrestamoAnticipo.class, configPrestamoAnticipo.getId());
			agente = entidadService.findById(Agente.class, objConfigPrestamoAnticipo.getAgente().getId());
			estatus = catalogoService.obtenerElementoEspecifico("Estatus del Movimiento de Prestamo / Anticipo", elementoCatalogo);						
			objConfigPrestamoAnticipo.setEstatus(estatus);
			objConfigPrestamoAnticipo = entidadService.save(objConfigPrestamoAnticipo);
			if(EstatusMovimientoPrestamoAnticipo.CANCELADO.getValue().equals(elementoCatalogo)){
				Map<String,Map<String,List<String>>> mapCorreos =  configPrestamoAnticipoService
				.obtenerCorreos(
						agente.getId(),
						GenericMailService.P_PRESTAMOS_ANTICIPOS,
						GenericMailService.M_PRESTAMOS_ANTICIPOS_RECHAZO_DEL_MOVIMIENTO);
			configPrestamoAnticipoService.enviarCorreo(configPrestamoAnticipo.getId(),
					mapCorreos, agente.getPersona()
							.getNombreCompleto(), "RECHAZO DEL MOVIMIENTO",
					GenericMailService.T_GENERAL);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return objConfigPrestamoAnticipo;
	}
	
	/*Metodo para aplicar el prestamo o anticipo al agente y cambiar el estatus del movimiento a aplicado */
	@Override
	public ConfigPrestamoAnticipo autorizarMovimiento(ConfigPrestamoAnticipo config) throws Exception {
		if(config!=null && config.getId()!=null){
			ConfigPrestamoAnticipo configPrestamo = entidadService.findById(ConfigPrestamoAnticipo.class, config.getId());
			// TODO: Deshacer este comentario
			guardaEnMovimientosAgentes(configPrestamo);
			updateEstatus(config, "AUTORIZADO");
//			config = updateEstatus(config,EstatusMovimientoPrestamoAnticipo.APLICADO.getValue());
//			if (config.getEstatus().getValor().equals( "APLICADO")) {
				Agente agente = agenteMidasService.findByClaveAgente(config.getAgente());
//				// TODO: Mandar correo de notificacion
				Map<String,Map<String,List<String>>> mapCorreos =  configPrestamoAnticipoService
						.obtenerCorreos(
								agente.getId(),
								GenericMailService.P_PRESTAMOS_ANTICIPOS,
								GenericMailService.M_PRESTAMOS_ANTICIPOS_AUTORIZACION_DEL_MOVIMIENTO);
					configPrestamoAnticipoService.enviarCorreo(config.getId(),
							mapCorreos, agente.getPersona()
									.getNombreCompleto(), "AUTORIZACION",
							GenericMailService.T_GENERAL);
//			}
			//FIXME CONTABILIZAR MOVIMIENTOS
			contabilizarMovimientosPrestamos(config.getId());
		}		
		return config;
	}
	
	private void contabilizarMovimientosPrestamos(Long idPrestamo) throws MidasException{
		solicitudCheques.contabilizarMovimientos(idPrestamo.toString(), ClavesTransaccionesContables.PRESTAMOS);
	}
	
	//metodo que obtiene los honorarios de el agente de la fecha  a 6 meses antes
	public double getHonorariosAgente(Long configPrestamoAnticipo){
		final String queryString;
		Double honorario;
		ConfigPrestamoAnticipo configuracionPrestamoAnticipo = getAgentePorConfiguracion(configPrestamoAnticipo);
	
		SimpleDateFormat sdf1 = new SimpleDateFormat("dd/MM/yyyy"); 
		SimpleDateFormat sdf2 = new SimpleDateFormat("dd/MM/yyyy");

		GregorianCalendar gc = new GregorianCalendar ();
		gc.setTime(configuracionPrestamoAnticipo.getFechaAltaMovimiento());
		gc.add (Calendar.MONTH, -6);
		Date fecha6MesesAntes = gc.getTime();
		
		String fechaAntes6mes = sdf1.format(fecha6MesesAntes);
		String fechaAltaMov = sdf2.format(configuracionPrestamoAnticipo.getFechaAltaMovimiento());
		
		queryString=" SELECT " +
				" sum(vwia.imp_base + vwia.imp_iva + vwia.imp_ret_iva + vwia.imp_ret_isr) honorarios " +
				" FROM seycos.vw_ingresos_agentes  vwia, " +
				" seycos.vw_est_fv vwfv " +
				" WHERE vwia.ID_AGENTE = vwfv.ID_AGENTE"+
				" AND vwia.id_agente   = "+configuracionPrestamoAnticipo.getAgente().getIdAgente()+
				" AND vwia.f_movto BETWEEN to_date('"+fechaAntes6mes+"','dd/mm/yyyy') and to_date('"+fechaAltaMov+"','dd/mm/yyyy') " +
				" ORDER BY vwia.id_agente, vwia.origen,vwia.f_movto ";
		
		Query query=entityManager.createNativeQuery(queryString);
		List<BigDecimal> listaHonorarios = new ArrayList<BigDecimal>();
		
		listaHonorarios = query.getResultList();
		
		if(listaHonorarios.get(0)!=null){
			honorario = (double) listaHonorarios.get(0).longValue();
			return honorario;
		}
		honorario = 0.0;
		return honorario;
	}
	
	public ConfigPrestamoAnticipo getAgentePorConfiguracion(Long idConfig){
		Map<String,Object> params=new HashMap<String, Object>();
		final StringBuilder queryString=new StringBuilder("");
		List<ConfigPrestamoAnticipo> agenteList = new ArrayList<ConfigPrestamoAnticipo>();
		ConfigPrestamoAnticipo obj = new ConfigPrestamoAnticipo();		
		queryString.append(" select model from ConfigPrestamoAnticipo model ");				 
		queryString.append(" left join fetch model.agente ");
		if(idConfig!=null){
			addCondition(queryString, "model.id=:id");
			params.put("id",idConfig);	
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		agenteList=query.getResultList();
	
		if(!agenteList.isEmpty()){
			obj=agenteList.get(0);
		}
		
		final StringBuilder queryStringPer=new StringBuilder("");
		queryStringPer.append(" SELECT * from MIDAS.VW_PERSONA pers");
		queryStringPer.append(" INNER JOIN MIDAS.TOAGENTE agt ON (pers.IDPERSONA = agt.IDPERSONA )");
		queryStringPer.append(" WHERE ID="+obj.getAgente().getId());
		Query query1 = entityManager.createNativeQuery(getQueryString(queryStringPer), Persona.class);
		List <Persona> listPersona = query1.getResultList();
		if(!listPersona.isEmpty()){
			Agente agente = new Agente();
			agente = obj.getAgente();
			agente.setPersona(listPersona.get(0));
			obj.setAgente(agente);
		}
		
		return obj;
	}
	
	@Override
	public ReporteIngresosAgente obtenerDatosReporte(ConfigPrestamoAnticipo configPrestamoAnticipo) throws Exception{
		ReporteIngresosAgente obj= new ReporteIngresosAgente();
		List<PagarePrestamoAnticipo> listPrestamo = new ArrayList<PagarePrestamoAnticipo>();
		PagarePrestamoAnticipo objPagare = new PagarePrestamoAnticipo();
		ConfigPrestamoAnticipo objConfig = new ConfigPrestamoAnticipo();
		objConfig = loadById(configPrestamoAnticipo);
		listPrestamo = pagarePrestamoAnticipoService.loadByIdConfigPrestamo(objConfig);
		
		double impInteresMoratorio=0L;
		double iva=0;
		double total = 0;
		double abonoCApital=0;
		double interesMoratorio=0;
		if(!listPrestamo.isEmpty()){
			objPagare = listPrestamo.get(0);
			for(PagarePrestamoAnticipo prestamo:listPrestamo){
				impInteresMoratorio =impInteresMoratorio+prestamo.getInteresMoratorio();
				iva=iva+prestamo.getIva();
				total =  total+prestamo.getImportePago();
				abonoCApital = abonoCApital+prestamo.getAbonoCapital();
				interesMoratorio = interesMoratorio+prestamo.getInteresMoratorio();
			}
		}
		//***************dar formato con 2 decimales a los valores doubles*********************** 
		impInteresMoratorio = darFormatoDosDecimales(impInteresMoratorio);
		total = darFormatoDosDecimales(total);
		iva = darFormatoDosDecimales(iva);
//*********************************************************************
		obj.setImporteOtorgado(objConfig.getImporteOtorgado());
		obj.setPlazos(objConfig.getNumeroPagos());
		obj.setInteresOrdinario(objConfig.getPcteInteresOrdinario());
		obj.setInteresMoratorio(interesMoratorio);
		obj.setNombreAgente(objConfig.getAgente().getPersona().getNombreCompleto());
		obj.setIdAgente(objConfig.getAgente().getIdAgente());
//***********************************************************************	
		obj.setAbonoCapital(objPagare.getAbonoCapital());
		obj.setImporteTotalIntersOrdinario(objConfig.getImporteInteresOrdinario());
		obj.setImporteInteresMoratorio(impInteresMoratorio);
		obj.setIva(iva);
		obj.setTotal(total);
		obj.setTotalAbonoCapital(abonoCApital);
//*******************************Honorarios Agente*******************************************	 
		obj.setImporteTotalIngresosAgente(getHonorariosAgente(objConfig.getId()));
		double promedioPagoHonorarios =0.0;
		double porcentageRepresentaPagoMensual=0.0;
		if(obj.getImporteTotalIngresosAgente()!=0.0){
			promedioPagoHonorarios =darFormatoDosDecimales(obj.getImporteTotalIngresosAgente()/6);
			porcentageRepresentaPagoMensual=darFormatoDosDecimales((promedioPagoHonorarios/obj.getImporteTotalIngresosAgente())*100);
		}
		obj.setPorcentajeRepresentapagoMensual(porcentageRepresentaPagoMensual);
		obj.setPromedioImporteIngresos(promedioPagoHonorarios);
		return obj;
	}
	private double darFormatoDosDecimales(double numero){
		double numeroDouble = 0;
		try {
			DecimalFormat formateador = new DecimalFormat("########.##");
			DecimalFormatSymbols dfs = formateador.getDecimalFormatSymbols();
			dfs.setDecimalSeparator('.');
			formateador.setDecimalFormatSymbols(dfs);
			String numeroString = formateador.format(numero);
			Number numeroEntero;
			numeroEntero = formateador.parse(numeroString);
			numeroDouble = numeroEntero.doubleValue();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return numeroDouble;
	}
	
	/*metodo para guardar en toagentemovimientos con estatus en PENDIENTE EN SOLICITUD DE CHEQUE*/
	private void guardaEnMovimientosAgentes(ConfigPrestamoAnticipo cheque) throws MidasException{
		Long idAgente=(isNotNull(cheque.getAgente()) && isNotNull(cheque.getAgente().getId()))?cheque.getAgente().getId():null;
		Agente agente=null;
		if(isNotNull(idAgente)){
			agente=agenteMidasService.loadById(cheque.getAgente());
		}
		String nombreAgente=(isNotNull(agente) && isNotNull(agente.getPersona()))?agente.getPersona().getNombreCompleto():null;
		boolean existeChequeEnProceso=existeChequePendientePorAgente(cheque);
		if(existeChequeEnProceso){
			onError("El agente "+nombreAgente+" con la clave "+idAgente+" ya tiene una solicitud de cheque en proceso, por lo tanto no puede continuar el prestamo");
		}
		StoredProcedureHelper storedHelper = null;
//		String sp="SEYCOS.Pkg_int_midas_E2.stpCrear_Sol_Cheque_Midas";
		String sp = "MIDAS."+PKGCALCULOS_AGENTES+".stp_insertaMovimientoAgente";
		try {
			LogDeMidasInterfaz.log("Entrando a ConfigPrestamoAnticipoDaoImpl.utorizarPrestamo..." + this, Level.INFO, null);
			ValorCatalogoAgentes tipoMoneda=catalogoService.obtenerElementoEspecifico("Tipo Moneda", "NACIONAL");
			
			Long idTipoAgente=agente.getIdTipoAgente();
			ValorCatalogoAgentes tipoAgente=new ValorCatalogoAgentes();
			if(isNotNull(idTipoAgente)){
				tipoAgente.setId(idTipoAgente);
				tipoAgente=catalogoService.loadById(tipoAgente);
			}
			ValorCatalogoAgentes tipoMovimiento=cheque.getTipoMovimiento();
			String tipoConcepto="PRESTAMOS";
			tipoConcepto=(isNotNull(tipoMovimiento) && isValid(tipoMovimiento.getValor()))?tipoMovimiento.getValor():tipoConcepto;
			ValorCatalogoAgentes conceptoMovAgt=catalogoService.obtenerElementoEspecifico("Concepto Movimiento Agente",tipoConcepto);
			Long idConcepto = conceptoMovAgt.getIdRegistro();
			ValorCatalogoAgentes estatusMovimiento=catalogoService.obtenerElementoEspecifico("Situacion Movimiento Agente", "PENDIENTE");
			Long idEstatusMovimiento = estatusMovimiento.getId();			
			Long idTipoMoneda=(isNotNull(tipoMoneda) && isNotNull(tipoMoneda.getClave()))?Long.parseLong(tipoMoneda.getClave()):null;
			Long claveAgente=(isNotNull(agente))?agente.getIdAgente():null;
			
			Calendar ahora = Calendar.getInstance();
			Integer anio = ahora.get(Calendar.YEAR);
			Integer mes = ahora.get(Calendar.MONTH) + 1;
			String mesCompleto="";
			if(mes<=9){
				mesCompleto = "0"+mes;
			}else{
				mesCompleto = Integer.toString(mes);
			}					
			String anioMes = Integer.toString(anio)+mesCompleto;
			
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pId_Empresa",EMPRESA_DEFAULT);
			storedHelper.estableceParametro("pId_Agente",val(claveAgente));
			storedHelper.estableceParametro("pF_Corte_Edocta",new Date());//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
			storedHelper.estableceParametro("pIdConsecMovto",null);
			storedHelper.estableceParametro("pAno_Mes",anioMes);////////////HJC 23/05/14: No importa lo que se envíe, se calcula el periodo contable abierto en la BD
			storedHelper.estableceParametro("pCve_T_Cpto_A_C","AGMAN");//FIXME que debe de ir aqui
			storedHelper.estableceParametro("pId_Concepto",idConcepto);/////////FIXME De donde viene este concepto, segun esto viene a nivel recibo, pero esto es acomulativo, cual de todos los conceptos de cada recibo pongo???
			storedHelper.estableceParametro("pCve_C_A",CLAVE_CLASE_AGENTE);
			storedHelper.estableceParametro("pId_Moneda",val(idTipoMoneda));//FIXME Revisar si es nacional siempre
			storedHelper.estableceParametro("pId_Moneda_Orig",val(idTipoMoneda));//FIXME Revisar si es nacional siempre
			storedHelper.estableceParametro("pTipo_Cambio",null);//FIXME como obtengo el tipo de cambio al dia?
			storedHelper.estableceParametro("pDesc_Movto",nombreAgente);///////////
			storedHelper.estableceParametro("pId_Ramo_Contable",null);
			storedHelper.estableceParametro("pId_SubR_Contable",null);
			storedHelper.estableceParametro("pId_LIn_Negocio",null);
			storedHelper.estableceParametro("pF_Movto",new Date());//FIXME que fecha va aqui?//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
			storedHelper.estableceParametro("pId_Centro_Emis",null);
			storedHelper.estableceParametro("pNum_Poliza",null);
			storedHelper.estableceParametro("pNum_Renov_Pol",null);
			storedHelper.estableceParametro("pId_Cotizacion",null);
			storedHelper.estableceParametro("pId_Version_Pol",null);
			storedHelper.estableceParametro("pId_Centro_Emis_E",null);
			storedHelper.estableceParametro("pCve_T_Endoso",null);
			storedHelper.estableceParametro("pNum_Endoso",null);
			storedHelper.estableceParametro("pId_Solicitud",null);
			storedHelper.estableceParametro("pId_Version_End",null);
			storedHelper.estableceParametro("pReferencia","");
			storedHelper.estableceParametro("pId_Centro_Oper",null);
			storedHelper.estableceParametro("pId_Remesa","01");
			storedHelper.estableceParametro("pId_Consec_MovtoR",null);
			storedHelper.estableceParametro("pCve_Origen_Remes",null);
			storedHelper.estableceParametro("pSerie_Folio_Rbo",null);
			storedHelper.estableceParametro("pNum_Folio_Rbo",null);
			storedHelper.estableceParametro("pId_Recibo",null);
			storedHelper.estableceParametro("pId_Version_Rbo",null);
			storedHelper.estableceParametro("pF_Vencto_Recibo",null);
			storedHelper.estableceParametro("pPct_Partic_Agte",null);
			storedHelper.estableceParametro("pImp_Prima_Neta",null);
			storedHelper.estableceParametro("pImp_Rcgos_PagoFr",null);
			storedHelper.estableceParametro("pImp_Comis_Agte",cheque.getImporteOtorgado());
			storedHelper.estableceParametro("pCve_Origen_Aplic","SU");
			storedHelper.estableceParametro("pCve_Origen_Movto","SUS");
			storedHelper.estableceParametro("pF_Corte_Pagocom",null);
			storedHelper.estableceParametro("pSit_Movto_Edocta",idEstatusMovimiento);
			storedHelper.estableceParametro("pId_Usuario_Integ",USUARIO_TRANSACCION);//FIXME revisar si puede ser midas.
			storedHelper.estableceParametro("pFH_Integracion",new Date());//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
			storedHelper.estableceParametro("pFH_Aplicacion",null);//HJC 23/05/14: No importa lo que se envíe, se inserta el valor en la BD
			storedHelper.estableceParametro("pF_Transf_DepV",null);
			storedHelper.estableceParametro("pid_transac_depv",null);
			storedHelper.estableceParametro("pId_Transacc_Orig",cheque.getId());
			storedHelper.estableceParametro("pId_Agente_Orig",val(claveAgente));
			storedHelper.estableceParametro("pCve_T_Cpto_A_C_O","AGAUT");
			storedHelper.estableceParametro("pId_Concepto_O",null);
			storedHelper.estableceParametro("pImp_Prima_Total",null);
			storedHelper.estableceParametro("pB_Facultativo","F");
			storedHelper.estableceParametro("pB_Masivo","F");
			storedHelper.estableceParametro("pId_Cobertura",null);
			storedHelper.estableceParametro("pAnio_Vig_Poliza","1");
			storedHelper.estableceParametro("pImp_Prima_D_CP",null);
			storedHelper.estableceParametro("pCve_Sist_Admon","NA");
			storedHelper.estableceParametro("pCve_Exp_Calc_Div","NA");
			storedHelper.estableceParametro("pImp_Base_Calculo",null);
			storedHelper.estableceParametro("pPctComisAgt",null);
			storedHelper.estableceParametro("pNum_Mov_Man_Agte",null);
			storedHelper.estableceParametro("pImpPagoAntImptos",null);
			storedHelper.estableceParametro("pPct_IVA",null);
			storedHelper.estableceParametro("ppct_iva_ret",null);
			storedHelper.estableceParametro("ppct_isr_ret",null);
			storedHelper.estableceParametro("pimp_iva",null);
			storedHelper.estableceParametro("ppimp_iva_ret",null);
			storedHelper.estableceParametro("pimp_isr",null);
			storedHelper.estableceParametro("pcve_sdo_impto ",null);			
			storedHelper.estableceParametro("pB_Impuesto","F");
			storedHelper.estableceParametro("pAno_Mes_Pago",null);
			storedHelper.ejecutaActualizar();
			//Long idSolicitud=(idSolicitudCheque!=null)?idSolicitudCheque.longValue():null;
			//cheque.setIdSolicitudCheque(idSolicitud);//Se actualiza el id de solicitud de cheque
			LogDeMidasInterfaz.log("Saliendo de ConfigPrestamoAnticipoDaoImpl.utorizarPrestamo..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,ConfigPrestamoAnticipoDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ConfigPrestamoAnticipoDaoImpl.utorizarPrestamo..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ConfigPrestamoAnticipoDaoImpl.utorizarPrestamo..." + this, Level.WARNING, e);
			onError(e);
		}
	}
	
	/**
	 * Metodo que valida si un agente ya tiene un cheque pendiente o en proceso.
	 * @param cheque
	 * @return
	 * @throws MidasException
	 */
	private boolean existeChequePendientePorAgente(ConfigPrestamoAnticipo cheque) throws MidasException{
		boolean existeChequePendiente=false;
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS."+PKG_INT_MIDAS_E2+".stpConsultar_ChequePendiente";
		try {
			LogDeMidasInterfaz.log("Entrando a ConfigPrestamoAnticipoDaoImpl.existeChequePendientePorAgente..." + this, Level.INFO, null);
			Long idAgente=(isNotNull(cheque.getAgente()) && isNotNull(cheque.getAgente().getId()))?cheque.getAgente().getId():null;
			Agente agente=null;
			if(isNotNull(idAgente)){
				agente=agenteMidasService.loadById(cheque.getAgente());
			}
			ValorCatalogoAgentes estatusCheque=catalogoService.obtenerElementoEspecifico("Estatus Solicitud Cheque", "PROCESO");
			String claveEstatusChequeProceso=(isNotNull(estatusCheque) && isValid(estatusCheque.getClave()))?estatusCheque.getClave():null;
			Long claveAgente=(isNotNull(agente))?agente.getIdAgente():null;
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pCve_Trans_Origen",CLAVE_TRANSACCION_AGENTES);
			storedHelper.estableceParametro("pId_Agente",val(claveAgente));
			storedHelper.estableceParametro("pSit_Sol_Cheque",val(claveEstatusChequeProceso));
			storedHelper.estableceParametro("pId_Usuario_Integ",USUARIO_TRANSACCION);//FIXME revisar si puede ser midas.
			storedHelper.estableceParametro("pClavel_cpto_trans", CLAVE_CONCEPTO_TRANSACCION);
			int existe=storedHelper.ejecutaActualizar();
			existeChequePendiente=(existe==1);
			LogDeMidasInterfaz.log("Saliendo de ConfigPrestamoAnticipoDaoImpl.existeChequePendientePorAgente..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,ConfigPrestamoAnticipoDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ConfigPrestamoAnticipoDaoImpl.existeChequePendientePorAgente..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ConfigPrestamoAnticipoDaoImpl.existeChequePendientePorAgente..." + this, Level.WARNING, e);
			onError(e);
		}
		return existeChequePendiente;
	}
	
	/**
	 * Se genera la solicitud de Cheque en Seycos y se envia  a MIZAR tambien.
	 */
	@Override
	public ConfigPrestamoAnticipo solicitarChequeDelMovimiento(ConfigPrestamoAnticipo config) throws Exception {
		if(config!=null && config.getId()!=null){
			ConfigPrestamoAnticipo configPrestamo = entidadService.findById(ConfigPrestamoAnticipo.class, config.getId());
			Long claveAgente=(config!=null && config.getAgente()!=null)?config.getAgente().getIdAgente():null;
			Long idSolicitudCheque=solicitarChequeDeMovimientoMizar(configPrestamo);
			if(idSolicitudCheque==null){
				onError("No se puede autorizar el cheque en Mizar, ya que es nulo.");
			}
			config.setIdSolicitudCheque(idSolicitudCheque);
			//FIXME llamar a mizar SPINSHEAD
			interfazMizarService.actualizarSolitudChequeMizarPrestamo(configPrestamo);
			//FIXME llamar a mizar SPINSDETALLE
			List<DetalleSolicitudCheque> detallesCheque= solicitudChequeFacade.obtenerDetallePorSolicitudCheque(idSolicitudCheque);
			if(detallesCheque==null || detallesCheque.isEmpty()){
				onError("La lista del detalle del cheque ["+configPrestamo.getIdSolicitudCheque()+"] esta vacia");
			}
			for(DetalleSolicitudCheque detalle:detallesCheque){
				interfazMizarService.actualizarDetalleSolitudChequeMizar(detalle, idSolicitudCheque, configPrestamo.getId(), claveAgente);
			}
			updateEstatus(config, "PROCESO");
		}
		return config;
	}
	
	/*metodo para solicitar el cheque del movimiento en mizar*/
	private Long solicitarChequeDeMovimientoMizar(ConfigPrestamoAnticipo cheque) throws MidasException{
		Long idAgente=(isNotNull(cheque.getAgente()) && isNotNull(cheque.getAgente().getId()))?cheque.getAgente().getId():null;
		Long idSolicitud=null;
		Agente agente=null;
		if(isNotNull(idAgente)){
			agente=agenteMidasService.loadById(cheque.getAgente());
		}
		String nombreAgente=(isNotNull(agente) && isNotNull(agente.getPersona()))?agente.getPersona().getNombreCompleto():null;
		
		StoredProcedureHelper storedHelper = null;
		String sp = "SEYCOS."+PKG_INT_MIDAS_E2+".stpSolicitar_Cheque";
		try {
			LogDeMidasInterfaz.log("Entrando a ConfigPrestamoAnticipoDaoImpl.solicitarChequeDelMovimiento..." + this, Level.INFO, null);
			ValorCatalogoAgentes tipoMoneda=catalogoService.obtenerElementoEspecifico("Tipo Moneda", "NACIONAL");			
			Long idTipoMoneda=(isNotNull(tipoMoneda) && isNotNull(tipoMoneda.getClave()))?Long.parseLong(tipoMoneda.getClave()):null;
			Long claveAgente=(isNotNull(agente))?agente.getIdAgente():null;
			
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pId_pago_midas",cheque.getId()); //{ idconcepto|idagente }  ejemplo ‘24|4735’
			storedHelper.estableceParametro("pTrans_cont","PRESTAMOS");          //‘PRESTAMO’
			storedHelper.estableceParametro("pid_moneda",idTipoMoneda);         //{ 484 (nacional) | 840 (dolares) }
			storedHelper.estableceParametro("pTipo_Cambio",1);					//{ 1 (484) | tc dia (840) }
			storedHelper.estableceParametro("pId_prest_serv",null);				//NULL
			storedHelper.estableceParametro("pBeneficiario",nombreAgente);		//nombre Beneficiario del cheque
			storedHelper.estableceParametro("pTipo_pago","SC");				//{ ‘SC’ | ‘TB’ }
			storedHelper.estableceParametro("pConcepto_pol","Prestamo al agente #"+claveAgente);  //‘Prestamo al agente #’
			storedHelper.estableceParametro("pUsuario","MIDAS");
			LogDeMidasInterfaz.log("pId_pago_midas:["+cheque.getId()+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("pTrans_cont:[PRESTAMOS]", Level.INFO, null);
			LogDeMidasInterfaz.log("pid_moneda:["+idTipoMoneda+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("pTipo_Cambio:["+1+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("pId_prest_serv:[null]", Level.INFO, null);
			LogDeMidasInterfaz.log("pBeneficiario:["+nombreAgente+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("pTipo_pago:[SC]", Level.INFO, null);
			LogDeMidasInterfaz.log("pConcepto_pol:[Prestamo al agente #"+claveAgente+"]", Level.INFO, null);
			LogDeMidasInterfaz.log("pUsuario:[MIDAS]", Level.INFO, null);
			Integer idSolicitudCheque=storedHelper.ejecutaActualizar();
			idSolicitud=(idSolicitudCheque!=null)?idSolicitudCheque.longValue():null;
			cheque.setIdSolicitudCheque(idSolicitud);//Se actualiza el id de solicitud de cheque
			LogDeMidasInterfaz.log("Saliendo de ConfigPrestamoAnticipoDaoImpl.solicitarChequeDelMovimiento..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}else{
				descErr=e.getMessage();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,ConfigPrestamoAnticipoDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ConfigPrestamoAnticipoDaoImpl.solicitarChequeDelMovimiento..." + this, Level.WARNING, e);
			onError(descErr);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ConfigPrestamoAnticipoDaoImpl.solicitarChequeDelMovimiento..." + this, Level.WARNING, e);
			onError(e);
		}
		return idSolicitud;
	}
	
	@Override
	public List<EntregoDocumentosView> consultaEstatusDocumentos(Long idPrestamo,Long idAgente) throws Exception {
		List<EntregoDocumentosView> listaDocumentosAgrupadosView = new ArrayList<EntregoDocumentosView>();
		StringBuilder queryString = new StringBuilder("");							
		queryString.append("select id,nombre as valor,existeDocumento as checado from MIDAS.trDocumentosAgrupados where idEntidad="+idAgente+" and (upper(nombre) like upper('PRES"+idPrestamo+"_%') " +
				"or upper(nombre) like upper('ANTI"+idPrestamo+"_%')) order by id asc ");  // and idDocumento in(40,41,42)
		Query query=entityManager.createNativeQuery(queryString.toString(),EntregoDocumentosView.class);
		listaDocumentosAgrupadosView=query.getResultList();
		return listaDocumentosAgrupadosView;
	}
	
	public List<DocumentosAgrupados> documentosFortimaxGuardadosDB(Long idPrestamo,Long idAgente){
		List<DocumentosAgrupados> listaDocumentosAgrupados = new ArrayList<DocumentosAgrupados>();
		StringBuilder queryString = new StringBuilder("");							
		queryString.append("select id,idDocumento,idEntidad,idAgrupador,nombre,existeDocumento from MIDAS.trDocumentosAgrupados where idEntidad="+idAgente+" and (upper(nombre) like upper('PRES"+idPrestamo+"_%') " +
				"or upper(nombre) like upper('ANTI"+idPrestamo+"_%')) order by id asc"); //and idDocumento in(40,41,42)
		Query query=entityManager.createNativeQuery(queryString.toString(),DocumentosAgrupados.class);
		listaDocumentosAgrupados=query.getResultList();
		return listaDocumentosAgrupados;
	}
	
	@Override
	public void auditarDocumentosEntregadosPrestamos(Long idPrestamo,Long idAgente, String nombreAplicacion) throws Exception {
		if(isNull(idAgente)){
			onError("Favor de proporcionar la clave del registro");
		}
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}		
				
		String[] documentosFortimax;
		try {
			/*lista de documentos que estan dados de alta en el portal de fortimax*/
			documentosFortimax = fortimaxService.getDocumentFortimax(idAgente,nombreAplicacion);
			if(isNull(documentosFortimax)|| documentosFortimax.length==0){
				onError("No hay documentos para la aplicacion "+nombreAplicacion+" con el numero de expediente "+idAgente);
			}					
			/*lista de documentos guardados en base de datos midas*/
			List<EntregoDocumentosView> lista=configPrestamoAnticipoService.consultaEstatusDocumentos(idPrestamo,idAgente);
			if(isEmptyList(lista)){
				onError("No hay registros de documentos para la aplicacion "+nombreAplicacion+" con el expediente "+idAgente);
			}
			
			for(EntregoDocumentosView doc:lista){
				String nombreDocumento=doc.getValor();
				Long idDocumento = doc.getId();
				Integer checado = doc.getChecado();
				if(isNotNull(doc) && isValid(nombreDocumento)){
					for(String docFortimax:documentosFortimax){
						//Si coincide el documento de base de datos con lo de fortimax y no ha sido subido, entonces lo marca como subido
						//if(isValid(docFortimax) && !docFortimax.equalsIgnoreCase(nombreDocumento)){
						DocumentosAgrupados rowDocumentos = entityManager.find(DocumentosAgrupados.class, idDocumento);	
						if(nombreDocumento.equalsIgnoreCase(docFortimax)){
							//cif(!docFortimax.equalsIgnoreCase(nombreDocumento)){															
								rowDocumentos.setExisteDocumento(1);
								entidadService.save(rowDocumentos);
								break;
							//}
						}
						
					}						
				}
			}	
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	//********************************************setters and getters

	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	private Object val(Object str){
		return (str!=null)?str:"";
	}
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}
	
	@EJB
	public void setPagarePrestamoAnticipoService(
			PagarePrestamoAnticipoService pagarePrestamoAnticipoService) {
		this.pagarePrestamoAnticipoService = pagarePrestamoAnticipoService;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	@EJB
	public void setSolicitudCheques(SolicitudChequesMizarService solicitudCheques) {
		this.solicitudCheques = solicitudCheques;
	}
	@EJB
	public void setInterfazMizarService(InterfazMizarService interfazMizarService) {
		this.interfazMizarService = interfazMizarService;
	}
	
	@EJB
	public void setSolicitudChequeFacade(
			SolicitudChequeFacadeRemote solicitudChequeFacade) {
		this.solicitudChequeFacade = solicitudChequeFacade;
	}
		
	@EJB
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}
	
	@EJB
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	
	@EJB
	public void setDocumentoEntidadService(DocumentoEntidadFortimaxService documentoEntidadService) {
		this.documentoEntidadService = documentoEntidadService;
	}

	public List<DocumentoEntidadFortimax> getListaDocFortimaxGuardados() {
		return listaDocFortimaxGuardados;
	}

	public void setListaDocFortimaxGuardados(
			List<DocumentoEntidadFortimax> listaDocFortimaxGuardados) {
		this.listaDocFortimaxGuardados = listaDocFortimaxGuardados;
	}

}
