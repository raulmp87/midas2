/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.compensaciones.CaCompensacionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.ProductoVidaDTO;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import oracle.jdbc.driver.OracleTypes;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Stateless
public class CaCompensacionDaoImpl implements CaCompensacionDao {
	public static final String POLIZA_ID = "polizaId";
	public static final String NEGOCIO_ID = "negocioId";
	public static final String NEGOCIOVIDA_ID = "negocioVidaId";
	public static final String CONFIGURACIONBANCA_ID = "configuracionBancaId";
	public static final String COTIZACION_ID = "cotizacionId";
	public static final String RAMO_ID = "caRamo.id";
	public static final String CONTRAPRESTACION = "contraprestacion";
	public static final String CUADERNOCONCURSO = "cuadernoConcurso";
	public static final String CONVENIOESPECIAL = "convenioEspecial";	
	public static final String RETROACTIVO = "retroactivo";
	public static final String MODIFICABLE = "modificable";	
	public static final String EXCEPCIONDOCUMENTOS = "excepcionDocumentos";
	public static final String COMPENSACIONPORSUBRAMOS = "compensacionPorSubramos";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaCompensacionDaoImpl.class);
	
	private SimpleJdbcCall obtenerListaRecibos;
	private SimpleJdbcCall setConfByIdRecibo;
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall setConfByIdCompensacionAut;
    
    @SuppressWarnings("unused")
	private DataSource dataSource;
	
	public CaCompensacion save(CaCompensacion entity) {
    	LOGGER.debug("	::	[INF]	::	Guardando CaCompensacion 	::		CaCompensacionDaoImpl	::	save	::	INICIO	::	");
    	//BigDecimal actual = null;
    	//CaCompensacion ultimaInsertada = null;
    	try {
    		entityManager.persist(entity);
    		//actual = findIdCompensAdicioNext();
    		//ultimaInsertada = findById(actual);
    		LOGGER.debug("	::	[INF]	::	Se Guardo CaCompensacion 	::		CaCompensacionDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaCompensacion 	::		CaCompensacionDaoImpl	::	save	::	ERROR	::	",re);
	        	throw re;
        }
	        return entity;
    }
	
    public void delete(CaCompensacion entity) {
    	LOGGER.debug("	::	[INF]	::	Eliminando CaCompensacion 	::		CaCompensacionDaoImpl	::	delete	::	INICIO	::	");
    	try {
        	entity = entityManager.getReference(CaCompensacion.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.debug("	::	[INF]	::	Se Elimino CaCompensacion 	::		CaCompensacionDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaCompensacion 	::		CaCompensacionDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaCompensacion update(CaCompensacion entity) {
    	LOGGER.debug("	::	[INF]	::	Actualizando CaCompensacion 	::		CaCompensacionDaoImpl	::	update	::	INICIO	::	");
    	try {
            CaCompensacion result = entityManager.merge(entity);
            LOGGER.debug("	::	[INF]	::	Se Actualizo CaCompensacion 	::		CaCompensacionDaoImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaCompensacion 	::		CaCompensacionDaoImpl	::	update	::	ERROR	::	",re);
        	throw re;
        }
    }
    
    @SuppressWarnings("unchecked")
	public CaCompensacion findById( Long id) {
    	LOGGER.debug(">> findById()");
    	CaCompensacion result = null;    	
    	try {
    	final String queryString = "select model from CaCompensacion model where model.id = :id and model." + BORRADOLOGICO + " = :borrado";
		Query query = entityManager.createQuery(queryString,CaCompensacion.class);
		query.setParameter("id", id);
		query.setParameter("borrado", ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
		
		List<CaCompensacion> listResult = query.getResultList();		
		if(listResult != null && !listResult.isEmpty() && listResult.get(0) != null){
			result = listResult.get(0);
		}
		
        } catch (Exception e) {
        	LOGGER.error("Información del Error", e);
        	result = null;
        }        
        LOGGER.debug("<< findById()");
        return result;
    }
	
    @SuppressWarnings("unchecked")
	public List<CaCompensacion> findByProperty(String propertyName, final Object value){
    	LOGGER.debug("	::	[INF]	::	Buscando por Propiedad 	::		CaCompensacionDaoImpl	::	findByProperty	::	INICIO	::	");
    	List<CaCompensacion> result;
    	try {
			final String queryString = "select model from CaCompensacion model where model." 
										+ propertyName + "= :propertyValue and model." + BORRADOLOGICO + " = :borrado";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);					
			query.setParameter("borrado", ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
			result = query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaCompensacionDaoImpl	::	findByProperty	::	ERROR	::	",re);
			result = null;
		}
		LOGGER.debug("	::	[INF]	::	Se busco por Propiedad 	::		CaCompensacionDaoImpl	::	findByProperty	::	FIN	::	");
		return result;
	}			
	public List<CaCompensacion> findByPolizaId(Object polizaId
	) {
		return findByProperty(POLIZA_ID, polizaId
		);
	}
	
	public List<CaCompensacion> findByNegocioId(Object negocioId
	) {
		return findByProperty(NEGOCIO_ID, negocioId
		);
	}
	
	public List<CaCompensacion> findByNegocioVidaId(Object negocioVidaId) {
		return findByProperty(NEGOCIOVIDA_ID, negocioVidaId);
	}
	
	public List<CaCompensacion> findByCotizacionId(Object cotizacionId
	) {
		return findByProperty(COTIZACION_ID, cotizacionId
		);
	}
	
	public List<CaCompensacion> findByContraprestacion(Object contraprestacion
	) {
		return findByProperty(CONTRAPRESTACION, contraprestacion
		);
	}
	
	public List<CaCompensacion> findByCuadernoconcurso(Object cuadernoconcurso
	) {
		return findByProperty(CUADERNOCONCURSO, cuadernoconcurso
		);
	}
	
	public List<CaCompensacion> findByConvenioespecial(Object convenioespecial
	) {
		return findByProperty(CONVENIOESPECIAL, convenioespecial
		);
	}
	
	public List<CaCompensacion> findByRetroactivo(Object retroactivo
	) {
		return findByProperty(RETROACTIVO, retroactivo
		);
	}
	
	public List<CaCompensacion> findByModificable(Object modificable
	) {
		return findByProperty(MODIFICABLE, modificable
		);
	}
	
	public List<CaCompensacion> findByExcepciondocumentos(Object excepciondocumentos
	) {
		return findByProperty(EXCEPCIONDOCUMENTOS, excepciondocumentos
		);
	}
	
	public List<CaCompensacion> findByCompensacionporsubramos(Object compensacionporsubramos
	) {
		return findByProperty(COMPENSACIONPORSUBRAMOS, compensacionporsubramos
		);
	}
	
	public List<CaCompensacion> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaCompensacion> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	public BigDecimal findIdCompensAdicioNext(){
		BigDecimal result;
		LOGGER.debug("	::	[INF]	::	Buscando CaCompensacion siguiente 	::		CaCompensacionDaoImpl	::	findAll	::	INICIO	::	");
		try {			
			result = (BigDecimal)entityManager.createQuery("select max(ca.id) from CaCompensacion ca").getSingleResult();
		} catch (RuntimeException re) {
			LOGGER.debug("	::	[INF]	::	Error al buscar CaCompensacion siguiente 	::		CaCompensacionDaoImpl	::	findAll	::	ERROR	::	",re);
			result = null;
		}
		LOGGER.debug("	::	[INF]	::	Se busco CaCompensacion siguiente 	::		CaCompensacionDaoImpl	::	findAll	::	FIN	::	");
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<CaCompensacion> findAll() {
		LOGGER.debug("	::	[INF]	::	Buscando Todo 	::		CaCompensacionDaoImpl	::	findAll	::	INICIO	::	");
		List<CaCompensacion> result;		
		try {
			final String queryString = "select model from CaCompensacion model where model." + BORRADOLOGICO + " = :borrado";
			Query query = entityManager.createQuery(queryString);			
			query.setParameter("borrado", ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
			result = query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaCompensacionDaoImpl	::	findAll	::	ERROR	::	",re);
			result = null;
		}
		LOGGER.debug("	::	[INF]	::	Se busco todo 	::		CaCompensacionDaoImpl	::	findAll	::	FIN	::	");
		return result;
	}
	
	public CaCompensacion findByNegocioIdandContraprestacion(Long idNegocio, boolean contraprestacion){
		CaCompensacion result = null;
		LOGGER.debug("	::	[INF]	::	Buscando por negocioId & contraprestacion 	::		CaCompensacionDaoImpl	::	findByNegocioIdandContraprestacion	::	INICIO	::	");
		try {
			final String queryString = "select model from CaCompensacion model where model." 
					+ NEGOCIO_ID + "= :negocioId and model." + CONTRAPRESTACION + " = :contraprestacion" 
					+" and model." + BORRADOLOGICO + " = :borrado";
			Query query = entityManager.createQuery(queryString,CaCompensacion.class);
			query.setParameter("negocioId", idNegocio);
			query.setParameter("contraprestacion", contraprestacion);
			query.setParameter("borrado", ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
			result = (CaCompensacion) query.getSingleResult();
		} catch (Exception e) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por negocioId & contraprestacion 	::		CaCompensacionDaoImpl	::	findByNegocioIdandContraprestacion	::	ERROR	::	",e);
			result = null;
		}
		LOGGER.debug("	::	[INF]	::	Se busco por negocioId & contraprestacion 	::		CaCompensacionDaoImpl	::	findByNegocioIdandContraprestacion	::	FIN	::	");
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public CaCompensacion findByRamoAndFieldAndContraprestacion(Long idRamo,  String field, Object valueField, boolean contraprestacion){
		
		LOGGER.debug(">> findByRamoAndFieldAndContraprestacion()");
		
		CaCompensacion result = null;
					
		try {
			
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaCompensacion model ");
			
			queryString.append(" WHERE model.").append(RAMO_ID).append(" = :").append(RAMO_ID.replace(".", ""));
			
			queryString.append(" AND model.").append(field).append(" = :").append(field);
						
			queryString.append(" AND model.").append(CONTRAPRESTACION).append(" = :").append(CONTRAPRESTACION);
			
			queryString.append(" AND model.").append(BORRADOLOGICO).append(" =:").append(BORRADOLOGICO);
			
			Query query = entityManager.createQuery(queryString.toString(),CaCompensacion.class);
			
			query.setParameter(RAMO_ID.replace(".", ""), idRamo);
			query.setParameter(field, valueField);
			query.setParameter(CONTRAPRESTACION, contraprestacion);
			query.setParameter(BORRADOLOGICO, ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
							
			List<CaCompensacion> listResult = query.getResultList();
			
			if(listResult != null && !listResult.isEmpty() && listResult.get(0) != null){
				result = listResult.get(0);
			}
			
		} catch (Exception   e) {
			LOGGER.error("Informacion del error", e);
		}
		
		LOGGER.debug(" << findByRamoAndFieldAndContraprestacion()");
		
		return result;
		
	}
	
	@SuppressWarnings("unchecked")
	public List<CaCompensacion> listCompensaciones(Long idRamo,  String field, Object valueField){
		LOGGER.debug(">> listCompensaciones()");
		List<CaCompensacion> listCompensaciones = null;
		try{
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaCompensacion model ");
			
			queryString.append(" WHERE model.").append(RAMO_ID).append(" = :").append(RAMO_ID.replace(".", ""));
			
			queryString.append(" AND model.").append(field).append(" = :").append(field);					
			
			queryString.append(" AND model.").append(BORRADOLOGICO).append(" =:").append(BORRADOLOGICO);
			
			Query query = entityManager.createQuery(queryString.toString(),CaCompensacion.class);
			
			query.setParameter(RAMO_ID.replace(".", ""), idRamo);
			query.setParameter(field, valueField);
			query.setParameter(BORRADOLOGICO, ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
			
			listCompensaciones = query.getResultList();
		}catch(Exception e){
			LOGGER.error("Información del Error", e);
			listCompensaciones = new ArrayList<CaCompensacion>();
		}
		LOGGER.debug("<< listCompensaciones()");
		return listCompensaciones;
	}
	public List<CompensacionesDTO> getlistRecibosPendientesPagar(Long idPoliza){
		 List<CompensacionesDTO> listaRecibos=null;
		 Map<String,Object> params=null;
		   try{
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pIdToPoliza",idPoliza);
					params= this.obtenerListaRecibos.execute(sqlParameter);	
					listaRecibos = (List<CompensacionesDTO>) params.get("pCursor");
	    		
			}catch (Exception e) {
				LOGGER.error("getlistRecibosPendientesPagar ERROR ", e);
			}
		return listaRecibos;	
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public  void setConfiguracionByIdRecibo(int idRecibo){
		LOGGER.debug(">> setConfiguracionByIdRecibo()");
		Map<String,Object> params=null;
		   try{
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pIdRecibo",idRecibo);
					params= this.setConfByIdRecibo.execute(sqlParameter);	
			}catch (Exception e) {
				LOGGER.error("setConfiguracionByIdRecibo ERROR ", e);
			}
			LOGGER.debug("<< setConfiguracionByIdRecibo()");
	}
	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {						
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.obtenerListaRecibos = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_DAN_COMPENSACIONES")
		.withProcedureName("obtenerRecibosPendientesXPagar")
		.declareParameters(
				new SqlParameter("pIdToPoliza", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Object>() {
					@Override
					public CompensacionesDTO mapRow(ResultSet rs, int index) throws SQLException {
						CompensacionesDTO recibos = new CompensacionesDTO();
						recibos.setIdRecibo(rs.getInt("ID_RECIBO"));
						recibos.setDescripcionRecibo(rs.getInt("DESCRIPCION"));
						return recibos;
					}
		}));
		this.setConfByIdRecibo = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_ORDENPAGO")
		.withProcedureName("cambiarConfiguracionDan")
		.declareParameters(
				new SqlParameter("pIdRecibo", Types.NUMERIC),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR)
			);
		this.setConfByIdCompensacionAut = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("PKG_CA_ORDENPAGO")
		.withProcedureName("cambiarConfiguracionAutos")
		.declareParameters(
				new SqlParameter("pIdCompensacion", Types.NUMERIC),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR)
			);	
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public  void setConfiguracionByIdCompensacionAut(int compensacionId){
		LOGGER.debug(" -->En setConfiguracionByIdCompensacionAut() idCompensacion ="+compensacionId);
		Map<String,Object> params=null;
		   try{
					MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
					sqlParameter.addValue("pIdCompensacion",compensacionId);
					params= this.setConfByIdCompensacionAut.execute(sqlParameter);	
			}catch (Exception e) {
				LOGGER.error("setConfiguracionByIdCompensacionAut ERROR ", e);
			}
			LOGGER.debug(" <--setConfiguracionByIdCompensacionAut() EXITOSO!");
	}
	
	public List<ProductoVidaDTO> getProductosVida(){
		List<ProductoVidaDTO> listaResultado = null;
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "MIDAS.PKG_CA_VIDA_COMPENSACIONES.obtenerProductosVida";
		String[] atributosDTO = {"nombreProducto","nombreLinea","comision" };
		String[] columnasCursor = {"producto","linea","comision" };
		try {
			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(ProductoVidaDTO.class.getCanonicalName(),atributosDTO,columnasCursor);
			listaResultado = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + nombreSP, e);
		}
		return listaResultado;
	}
}
