package mx.com.afirme.midas2.dao.impl.pkg;

import java.sql.SQLException;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.pkg.PkgAutGeneralesDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;

@Stateless
public class PkgAutGeneralesDaoImpl implements PkgAutGeneralesDao {

	
	private static final Logger LOG = Logger.getLogger(PkgAutGeneralesDaoImpl.class);	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public Map<String, String> emitirCotizacion(CotizacionDTO cotizacionDTO) {
		long start = System.currentTimeMillis();
		LOG.info("Emitiendo cotizacion : " + cotizacionDTO.getIdToCotizacion());
		String spName="MIDAS.pkgAUT_Generales.spAUT_EmitePoliza";
		Map<String, String> mensaje = new HashMap<String, String>();
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro("pIdCotizacion", cotizacionDTO.getIdToCotizacion());
			//storedHelper.estableceParametro("pDiasPorDevengar", cotizacionDTO.getDiasPorDevengar());	
			storedHelper.estableceParametro("pGastosExpedicion", cotizacionDTO.getDerechosPoliza());
			storedHelper.estableceParametro("pPorcentajeRPF", cotizacionDTO.getPorcentajePagoFraccionado());
			storedHelper.estableceParametro("pCodigoUsuario", cotizacionDTO.getCodigoUsuarioModificacion());
			storedHelper.estableceParametro("pFolioPolizaAsociada", cotizacionDTO.getFolioPolizaAsociada());
			
			
			
			Integer idPoliza = Integer.valueOf(storedHelper.ejecutaActualizar());
			
			if(idPoliza != null && idPoliza.intValue() > 0){
				mensaje.put("icono", "30");//Exito
				mensaje.put("idpoliza", idPoliza.toString());
				mensaje.put("mensaje", "La p\u00f3liza se emiti\u00f3 correctamente.\n El n\u00famero de  P\u00f3liza es: ");					
			}else{
				String descErr = null;
				mensaje.put("icono", "10");//Error
				if (storedHelper != null) {
					descErr = storedHelper.getDescripcionRespuesta();
					mensaje.put("mensaje", "Error al generar la P\u00f3liza: "+descErr);	
				}					
			}	
			LOG.info("Saliendo de proceso de emision  idCotizacion "  +  cotizacionDTO.getIdToCotizacion() + " : " 
					 + ((System.currentTimeMillis() - start) / 1000.0));
			return mensaje;	
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			
			mensaje.put("icono", "10");//Error
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
				mensaje.put("mensaje", "Error al generar la P\u00f3liza: "+descErr);
			}
			
			StoredProcedureErrorLog.doLog(cotizacionDTO.getCodigoUsuarioModificacion(),
					StoredProcedureErrorLog.TipoAccion.GUARDAR,
					"MIDAS.pkgDAN_Generales.spDAN_EmitePoliza", CotizacionDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de PolizaFacade.emitePoliza..." + this, Level.WARNING, e);
			return mensaje;				
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en PolizaFacade.emitePoliza..." + this, Level.WARNING, e);
			mensaje.put("icono", "10");//Error
			mensaje.put("mensaje", "Error al generar la P\u00f3liza: "+e.getMessage());
			return mensaje;
		}		
	}
	
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)		
	public Map<String, String> migraBitemporalidad(Long polizaId, Date validFrom, Date validTo){
		LogDeMidasInterfaz.log("Entrando a PolizaFacade.migraBitemporalidad..." + this, Level.INFO, null);
		 Map<String, String> mensaje = new HashMap<String, String>();
		 String spName="MIDAS.pkgAUT_Generales.spAUT_EmiteBitemporalidad";
		 StoredProcedureHelper storedHelper = null;
		 try {
				storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pIdPoliza", polizaId);
				storedHelper.estableceParametro("pValidFrom", validFrom);
				storedHelper.estableceParametro("pValidTo", validTo);
				Integer idCotizacionContinuidad = Integer.valueOf(storedHelper.ejecutaActualizar());
				
				if(idCotizacionContinuidad != null && idCotizacionContinuidad.intValue() > 0){
					mensaje.put("exito", "La p\u00f3liza se migro correctamente.");					
				}else{
					String descErr = null;
					mensaje.put("icono", "10");//Error
					if (storedHelper != null) {
						descErr = storedHelper.getDescripcionRespuesta();
						mensaje.put("error", "Error al migrar la P\u00f3liza: "+descErr);	
					}					
				}	
				LogDeMidasInterfaz.log("Saliendo de PolizaFacade.migraBitemporalidad... mensaje:"+mensaje + this, Level.INFO, null);
				return mensaje;	
			} catch (SQLException e) {
				Integer codErr = null;
				String descErr = null;
				
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
					mensaje.put("mensaje", "Error al migrar la P\u00f3liza: "+descErr);
				}
				
				StoredProcedureErrorLog.doLog(polizaId.toString(),
						StoredProcedureErrorLog.TipoAccion.GUARDAR,
						"MIDAS.pkgAUT_Generales.spAUT_EmiteBitemporalidad", BitemporalCotizacion.class, codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de PolizaFacade.migraBitemporalidad..." + this, Level.WARNING, e);
				return mensaje;				
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en PolizaFacade.migraBitemporalidad..." + this, Level.WARNING, e);
				mensaje.put("icono", "10");//Error
				mensaje.put("mensaje", "Error al migrar la P\u00f3liza: "+e.getMessage());
				return mensaje;
			}	
	}



	@SuppressWarnings("unchecked")
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public List<ReciboDTO> emiteRecibos(String polizaEndoso) {
		long start = System.currentTimeMillis();
		LOG.info("Migrando a seycos polizaEndoso : " + polizaEndoso);
		String spName = "SEYCOS.Pkg_int_midas.stpEmiteAuto";
		StoredProcedureHelper storedHelper = null;
		List<ReciboDTO> reciboList = null;
		try {
			storedHelper = new StoredProcedureHelper(spName,
					StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
			.estableceMapeoResultados(
					ReciboDTO.class.getCanonicalName(),
					
					"numeroFolioRecibo," +
					"numeroExhibicion," +
					"llaveFiscal," +
					"importe," +
					"fechaLimitePago",
											
					"num_folio_rbo," +
					"num_exhibicion," +
					"llave_fiscal," +
					"importe," +
					"fecha_limite_pago");
			
			storedHelper.estableceParametro("pid_cot_midas", polizaEndoso);
			reciboList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion generada: " + e.getMessage() + " - " + storedHelper.getCodigoRespuesta() + " - " + storedHelper.getDescripcionRespuesta(), Level.WARNING, null);
			LogDeMidasInterfaz.log("Excepcion general en PkgAutGeneralesDao.emiteRecibos..." + this, Level.WARNING, e);
			
			
		}
		LOG.info("Saliendo de proceso de Migrar a Seycos, polizaEndoso : "  +  polizaEndoso + " : " 
				 + ((System.currentTimeMillis() - start) / 1000.0));
		return reciboList;
	}



	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void cancelarEmision(Double idToPoliza) {
		String spName = "MIDAS.pkgAUT_Generales.spAUT_RollbackEmitePoliza";
		StoredProcedureHelper storedHelper = null;		
		try {
			storedHelper = new StoredProcedureHelper(spName,
					StoredProcedureHelper.DATASOURCE_MIDAS);			
			storedHelper.estableceParametro("pIdPoliza", idToPoliza);
			storedHelper.ejecutaActualizar();
		} catch (Exception e) {
			LogDeMidasInterfaz.log(
					"Excepcion general en MIDAS.pkgAUT_Generales.spAUT_RollbackEmitePoliza..."
							+ this, Level.WARNING, e);
		}				
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public List<ReciboDTO> emiteRecibosEndoso(String polizaEndoso) {
		String spName = "SEYCOS.Pkg_int_midas.stpEmiteEndosoAuto";
		StoredProcedureHelper storedHelper = null;
		List<ReciboDTO> reciboList = null;
		try {
			storedHelper = new StoredProcedureHelper(spName,
					StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
			.estableceMapeoResultados(
					ReciboDTO.class.getCanonicalName(),
					
					"numeroFolioRecibo," +
					"numeroExhibicion," +
					"llaveFiscal," +
					"importe," +
					"fechaLimitePago," +
					"solicitudCheque",
											
					"num_folio_rbo," +
					"num_exhibicion," +
					"llave_fiscal," +
					"importe," +
					"fecha_limite_pago," +
					"numcheque");
			
			storedHelper.estableceParametro("pIdentificador", polizaEndoso);
			reciboList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LogDeMidasInterfaz.log(
					"Excepcion general en PkgAutGeneralesDao.emiteRecibosEndoso..."
							+ this, Level.WARNING, e);
		}
		return reciboList;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateEmisionPendiente(short tipoEmision, boolean esAutomatica){
		
		this.updateEmisionPendiente(tipoEmision, esAutomatica, null);				
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void updateEmisionPendiente(short tipoEmision, boolean esAutomatica, Date fechaValidacion){
		String spName = "MIDAS.pkgAUT_Generales.spaut_updateEmisionPendiente";
		StoredProcedureHelper storedHelper = null;		
		try {
			storedHelper = new StoredProcedureHelper(spName,
					StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pF_Proceso", fechaValidacion != null ? fechaValidacion : TimeUtils.getDateTime(new Date()).toDate());
			storedHelper.estableceParametro("pTipoEmision", tipoEmision);
			storedHelper.estableceParametro("pIsAutomatica", (esAutomatica?1:0));
			
			storedHelper.ejecutaActualizar();
		} catch (Exception e) {
			LogDeMidasInterfaz.log(
					"Excepcion general en MIDAS.pkgAUT_Generales.spaut_updateEmisionPendiente..."
							+ this, Level.WARNING, e);
		}		
	}

}
