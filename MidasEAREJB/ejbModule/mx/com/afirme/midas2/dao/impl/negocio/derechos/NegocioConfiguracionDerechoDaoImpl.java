package mx.com.afirme.midas2.dao.impl.negocio.derechos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.derechos.NegocioConfiguracionDerechoDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.dto.negocio.derechos.NegocioConfiguracionDerechoDTO;
import mx.com.afirme.midas2.util.StringUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioConfiguracionDerechoDaoImpl extends JpaDao<Long, NegocioConfiguracionDerecho>  implements NegocioConfiguracionDerechoDao{

	@PersistenceContext
	protected EntityManager entityManager;
		
	@SuppressWarnings("unchecked")
	@Override
	public List<NegocioConfiguracionDerechoDTO> buscarConfiguracionesDerecho(NegocioConfiguracionDerecho	filtroConfigDerecho){
		List<NegocioConfiguracionDerechoDTO> configuraciones = new ArrayList<NegocioConfiguracionDerechoDTO>(1);		
		Query query = createQuery(filtroConfigDerecho, false);
		if(filtroConfigDerecho.getPosStart() != null){
			query.setFirstResult(filtroConfigDerecho.getPosStart());
		}
		if(filtroConfigDerecho.getCount() != null){
			query.setMaxResults(filtroConfigDerecho.getCount());
		}
		configuraciones = query.getResultList();
	
		return configuraciones;
		
	}
	
	@Override
	public Long conteoBusquedaConfiguracionesDerecho(NegocioConfiguracionDerecho negocioConfiguracionDerecho){
		Query query = createQuery(negocioConfiguracionDerecho, true);
		return ((Long)query.getSingleResult());
	}
	
	@SuppressWarnings("rawtypes")
	private Query createQuery( NegocioConfiguracionDerecho filtroConfigDerecho, boolean count){
			
			Query query 							= null;
			StringBuilder queryString 				= new StringBuilder();
			List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>(1);				
	
			//para obtener el total de registros (paginacion)
			if(count){
				queryString.append(" SELECT COUNT(negocioConfigDerecho.idToNegConfigDerecho) ");
			}else{ //busqueda de los registros
				//se requirio hacer el select de los alias para poder hacer el ordenamiento requerido de la paginacion
				queryString.append(" SELECT new mx.com.afirme.midas2.dto.negocio.derechos.NegocioConfiguracionDerechoDTO(")
				.append(" negocioConfigDerecho.idToNegConfigDerecho, ")
				.append(" negocio.idToNegocio, ")
				.append(" negocio.descripcionNegocio, ")
				.append(" tipoPoliza.idToNegTipoPoliza, ")
				.append(" tipoPoliza.tipoPolizaDTO.descripcion, ")
				.append(" seccion.idToNegSeccion, ")
				.append(" seccion.seccionDTO.descripcion, ")
				.append(" paquete.idToNegPaqueteSeccion, ")
				.append(" paquete.paquete.descripcion, ")
				.append(" moneda.idTcMoneda, ")
				.append(" moneda.descripcion, ")
				.append(" negocioConfigDerecho.tipoDerecho, ")
				.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :tipoDerechoDescripcion, negocioConfigDerecho.tipoDerecho ), ")
				.append(" negocioConfigDerecho.importeDerecho,  ")
				.append(" tuv.idTcTipoUsoVehiculo, ")
				.append(" tuv.descripcionTipoUsoVehiculo, ")
				.append(" est.stateId, ")
				.append(" est.stateName, ")
				.append(" mun.cityId, ")
				.append(" mun.cityName, ")
				.append(" negocioConfigDerecho.importeDefault ")
				.append(" )");
				
				Utilerias.agregaHashLista(listaParametrosValidos, "tipoDerechoDescripcion", CatGrupoFijo.TIPO_CATALOGO.TIPO_CONFIG_DERECHO.toString());
			}
			queryString.append(" FROM NegocioConfiguracionDerecho negocioConfigDerecho ");
			queryString.append(" JOIN negocioConfigDerecho.negocio negocio");
			queryString.append(" JOIN negocioConfigDerecho.tipoPoliza tipoPoliza");
			queryString.append(" JOIN negocioConfigDerecho.seccion seccion");
			queryString.append(" JOIN negocioConfigDerecho.paquete paquete");
			queryString.append(" JOIN negocioConfigDerecho.moneda moneda");
//			queryString.append(" LEFT JOIN negocioConfigDerecho.estado est  ");
//			queryString.append(" LEFT JOIN negocioConfigDerecho.municipio mun");
//			queryString.append(" LEFT JOIN negocioConfigDerecho.tipoUsoVehiculo tuv");
			queryString.append(" LEFT JOIN ").append(EstadoDTO.class.getSimpleName()).append(" est ON negocioConfigDerecho.estado = est");
			queryString.append(" LEFT JOIN ").append(CiudadDTO.class.getSimpleName()).append(" mun ON negocioConfigDerecho.municipio = mun ");
			queryString.append(" LEFT JOIN ").append(TipoUsoVehiculoDTO.class.getSimpleName()).append(" tuv ON negocioConfigDerecho.tipoUsoVehiculo = tuv ");
				
			queryString.append(" WHERE negocio.idToNegocio = :idToNegocio ");
			Utilerias.agregaHashLista(listaParametrosValidos, "idToNegocio", filtroConfigDerecho.getNegocio().getIdToNegocio());
			
			queryString.append(" AND tipoPoliza.idToNegTipoPoliza = :idToNegTipoPoliza ");
			Utilerias.agregaHashLista(listaParametrosValidos, "idToNegTipoPoliza", filtroConfigDerecho.getTipoPoliza().getIdToNegTipoPoliza());
			
			queryString.append(" AND seccion.idToNegSeccion = :idToNegSeccion ");
			Utilerias.agregaHashLista(listaParametrosValidos, "idToNegSeccion", filtroConfigDerecho.getSeccion().getIdToNegSeccion());
			
			queryString.append(" AND paquete.idToNegPaqueteSeccion = :idToNegPaqueteSeccion ");
			Utilerias.agregaHashLista(listaParametrosValidos, "idToNegPaqueteSeccion", filtroConfigDerecho.getPaquete().getIdToNegPaqueteSeccion());
			
			queryString.append(" AND moneda.idTcMoneda = :idTcMoneda ");
			Utilerias.agregaHashLista(listaParametrosValidos, "idTcMoneda", filtroConfigDerecho.getMoneda().getIdTcMoneda());
			
			queryString.append(" AND negocioConfigDerecho.tipoDerecho = :tipoDerecho ");
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoDerecho", filtroConfigDerecho.getTipoDerecho());
			
	
			if( filtroConfigDerecho.getIdToNegDerecho() != null){
				queryString.append(" AND negocioConfigDerecho.idToNegDerecho = :idToNegDerecho");		
				Utilerias.agregaHashLista(listaParametrosValidos, "idToNegDerecho", filtroConfigDerecho.getIdToNegDerecho());
			}
			
			if( filtroConfigDerecho.getEstado() != null
					&& filtroConfigDerecho.getEstado().getStateId() != null
					&& !filtroConfigDerecho.getEstado().getStateId().isEmpty()){
				queryString.append(" AND negocioConfigDerecho.estado.stateId = :stateId");		
				Utilerias.agregaHashLista(listaParametrosValidos, "stateId", filtroConfigDerecho.getEstado().getStateId());
				
				if( filtroConfigDerecho.getMunicipio() != null
						&& filtroConfigDerecho.getMunicipio().getCityId() != null
						&& !filtroConfigDerecho.getMunicipio().getCityId().isEmpty()){
					queryString.append(" AND negocioConfigDerecho.municipio.cityId = :cityId");		
					Utilerias.agregaHashLista(listaParametrosValidos, "cityId", filtroConfigDerecho.getMunicipio().getCityId() );
				}else{
					queryString.append(" AND negocioConfigDerecho.municipio IS NULL");	
				}
			}else{
				queryString.append(" AND negocioConfigDerecho.estado IS NULL");
				queryString.append(" AND negocioConfigDerecho.municipio IS NULL");
			}
		
			if( filtroConfigDerecho.getTipoUsoVehiculo() != null
					&& filtroConfigDerecho.getTipoUsoVehiculo().getIdTcTipoUsoVehiculo() != null){
				queryString.append(" AND negocioConfigDerecho.tipoUsoVehiculo.idTcTipoUsoVehiculo = :idTcTipoUsoVehiculo");		
				Utilerias.agregaHashLista(listaParametrosValidos, "idTcTipoUsoVehiculo", filtroConfigDerecho.getTipoUsoVehiculo().getIdTcTipoUsoVehiculo());
			}else{
				queryString.append(" AND negocioConfigDerecho.tipoUsoVehiculo IS NULL");
			}
			
			//Por motivo de la paginacion y carga dinamica de dhtmlx se requiere hacer un ordenamiento manual
			if(!count){
				if(StringUtil.isEmpty(filtroConfigDerecho.getOrderByAttribute())){
					queryString.append(" ORDER BY negocioConfigDerecho.importeDerecho DESC ");												
				}else{
					queryString.append(" ORDER BY ").append(filtroConfigDerecho.getOrderByAttribute());
				}
			}
			
			query = entityManager.createQuery(queryString.toString());
			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
			return query;
	} 
}
