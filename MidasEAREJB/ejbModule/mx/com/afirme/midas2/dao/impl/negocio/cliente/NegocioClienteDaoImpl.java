package mx.com.afirme.midas2.dao.impl.negocio.cliente;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.cliente.NegocioClienteDao;
import mx.com.afirme.midas2.domain.negocio.Negocio_;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente_;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioClienteDaoImpl extends JpaDao<Long, NegocioCliente> implements NegocioClienteDao {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<NegocioCliente> findByFilters(NegocioCliente entity){
		String queryString = "select model from NegocioCliente model ";
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		String sWhere = "";
		
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere,  NegocioCliente_.idCliente.getName(), entity.getIdCliente());
		
		if(entity.getNegocio() != null){
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere,
					"negocio."+Negocio_.idToNegocio.getName(), entity.getNegocio().getIdToNegocio());
		}
		
		if (Utilerias.esAtributoQueryValido(sWhere))
			queryString = queryString.concat(" where ").concat(sWhere);
		
		Query query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
		
	}

}
