package mx.com.afirme.midas2.dao.impl.custShipmentTracking;

import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.custShipmentTracking.TrackingDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.BitacoraGuia;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.DetalleBitacora;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedor;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedorId;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.ReporteEfectividadEntrega;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.wsClient.tracking.ArrayOfString;
import mx.com.afirme.midas2.wsClient.tracking.DeliveryData;
import mx.com.afirme.midas2.wsClient.tracking.Filter;
import mx.com.afirme.midas2.wsClient.tracking.History;
import mx.com.afirme.midas2.wsClient.tracking.HistoryConfiguration;
import mx.com.afirme.midas2.wsClient.tracking.QueryResult;
import mx.com.afirme.midas2.wsClient.tracking.SearchConfiguration;
import mx.com.afirme.midas2.wsClient.tracking.SearchType;
import mx.com.afirme.midas2.wsClient.tracking.Service;
import mx.com.afirme.midas2.wsClient.tracking.ServiceSoap;
import mx.com.afirme.midas2.wsClient.tracking.TrackingData;
import mx.com.afirme.midas2.wsClient.tracking.WaybillList;

import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class TrackingDaoImpl extends EntidadDaoImpl implements TrackingDao{

	public static final Logger LOG = Logger.getLogger(TrackingDaoImpl.class);
	public static final int CONSULTAS_MAXIMA = 1000;
	public static final int topeFecha = 9;
	
	@EJB
	private EntidadDao entidadDao;
	@EJB
	private SistemaContext sistemaContext;

	@Override
	public RegistroGuiasProveedor findRegistroById(RegistroGuiasProveedorId id){
		return entidadDao.findById(RegistroGuiasProveedor.class, id);
	}
	
	@Override
	public List<BitacoraGuia> getBitacoraByIdToPoliza(BigDecimal idToPoliza){
		Map<String, Object> map = new HashMap<String, Object>(1);
		map.put("idToPoliza", idToPoliza);
		return entidadDao.findByProperties(BitacoraGuia.class, map);
	}
	@Override
	public void saveRegistroGuiasProveedor(RegistroGuiasProveedor registro){
		entidadDao.persist(registro);
	}
	
	@Override
	public void updateRegistroGuia(RegistroGuiasProveedor registro){
		entidadDao.update(registro);
	}

	@Override
	public List<RegistroGuiasProveedor> getRegiostosGuiaSinProcesar(){	
		Map<String, Object> map = new HashMap<String, Object>(1);
		map.put("procesado", false);
		return entidadDao.findByProperties(RegistroGuiasProveedor.class, map);
	}
	
	@Override
	public BitacoraGuia saveBitacora(BitacoraGuia bitacora){
		entidadDao.persist(bitacora);
		return bitacora;
	}
	
	@Override
	public void generaDetallePendienteGuia(BitacoraGuia bitacora){
		DetalleBitacora detalle = new DetalleBitacora();
		detalle.setEstatus(BitacoraGuia.estatusGuia.PROCESADA.name());
		detalle.setFecha(bitacora.getFecha());
		detalle.setIdToBitacora(bitacora.getId());
		entidadDao.persist(detalle);
	}
	
	@Override
	public void getGuias(List<String> guias){
		
		QueryResult result = new QueryResult();
		try{
			//Se configura el tipo de busqueda
			SearchType searchType = getSearchTypeByGuia(guias);
			Filter filter = new Filter();
			filter.setFilterInformation(false);
			
			//Se configura a la busqueda del historico.
			SearchConfiguration searchConfiguration = getSearchConfigurationByLastEvento(
					BitacoraGuia.TipoHistorial.ALL.valor());
			searchConfiguration.setFilterType(filter);
			/*
				https://tracking.estafeta.com/Service.asmx?wsdl			
			*/
			Service sv = new Service(new URL(sistemaContext.getTrackingEndPoint()));
			ServiceSoap trackingLocator = sv.getServiceSoap();
			
			String idSuscriptor = sistemaContext.getTrakingIdSuscripcion();
			String usuario = sistemaContext.getTrakingUser();
			String password = sistemaContext.getTrakingPassword();
			LOG.debug("Consulta Tracking id: " + idSuscriptor + ", user:" + usuario + ", password: "+ password);
			result = trackingLocator.executeQuery(
					idSuscriptor, usuario, password, searchType, searchConfiguration);
			LOG.trace("Termina consulta Traking");
			if(result.getErrorCode().equals("0")){
				saveDatosGuia(result.getTrackingData().getTrackingData());
			}else{
				LOG.error("Ocurrio un error al consultar las guias, descripcion del error: "+result.getErrorCodeDescriptionSPA());
				throw new Exception(result.getErrorCodeDescriptionSPA());
			}
		}catch(Exception e){
			LOG.error("No se pudo acceder a los WSClient", e);
		}
	}
	

	@SuppressWarnings("unchecked")
	@Override
	public List<ReporteEfectividadEntrega> getDatosReporteEfectividadEntrega(
			OrdenRenovacionMasiva orden){
		LOG.trace("Entrando a getDatosReporteEfectividadEntrega ");
		List<ReporteEfectividadEntrega> resultList = new ArrayList<ReporteEfectividadEntrega>(1);
		
		String sp = "MIDAS.PKGREPORTEEFECTIVIDADENTREGA.spOrdenesRenovacion";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;
		try {
			String[] colsTemp = {"IDTOBITACORA",
					"IDTOORDENRENOVACION",
					"IDTOPOLIZA",
					"NUMERO_POLIZA_SEYCOS",
					"NUMERO_POLIZA_MIDAS",
					"NUMERO_POLIZA_SEYCOS_RENOVADA",
					"NUMERO_POLIZA_MIDAS_RENOVADA",
					"CONTRATANTE",
					"TITULAR",
					"TELEFONO",
					"DESCRIPCION_VEHICULO",
					"MODELO",
					"SERIE",
					"PRIMA_TOTAL_POLIZA",
					"FECHA_ENVIO",
					"GUIA",
					"ESTATUS",
					"FECHA_ENTREGA",
					"RECIBE",
					"RECHAZO"};
				String[] propsTemp = {"idToBitacora",
						"idToOrdenRenovacion",
						"idToPoliza",
						"numeroPolizaSeycos",
						"numeroPolizaMidas",
						"numeroPolizaSeycosRenovada",
						"numeroPolizaMidasRenovada",
						"contratante",
						"titular",
						"telefono",
						"vehiculo",
						"modelo",
						"serie",
						"primaTotalPoliza",
						"fecha_Envio",
						"guia",
						"estatus",
						"fecha_Entrega",
						"recibe",
						"rechazo"};
				cols = colsTemp;
				props = propsTemp;

				for (int i = 0; i < cols.length; i++) {
					String columnName = cols[i];
					String propertyName = props[i];
					propiedades.append(propertyName);
					columnas.append(columnName);
					if (i < (cols.length - 1)) {
						propiedades.append(",");
						columnas.append(",");
					}
				}
				storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceMapeoResultados(ReporteEfectividadEntrega.class.getCanonicalName(), propiedades.toString(), columnas.toString());
				storedHelper.estableceParametro("pIniDate", orden.getFechaCreacion());
				storedHelper.estableceParametro("pEndDate", orden.getFechaCreacionHasta());
				storedHelper.estableceParametro("idToOrdenRenovacion",orden.getIdToOrdenRenovacion());
				storedHelper.estableceParametro("nombreUsuario", orden.getNombreUsuarioCreacion());
				storedHelper.estableceParametro("numeroPoliza", orden.getNumeroPoliza());
				storedHelper.estableceParametro("claveEstatus", orden.getClaveEstatus());
				resultList = storedHelper.obtieneListaResultados();
			} catch (Exception e) {
				LOG.error("Excepcion general al invocar getDatosReporteEfectividadEntrega: ", e);
			}
		return resultList;
	}
	
	@Override
	public void procesarTareaEstatusPendientes(){
		List<String> idToBitacorasAProcesar = getBitacorasAProcesar();
		List<String> idToBitacoras = new ArrayList<String>(1);
		
		if(idToBitacorasAProcesar.size()>CONSULTAS_MAXIMA){
			
			for(int i=0; i<idToBitacorasAProcesar.size(); i++){
				
				if(idToBitacoras.size() < CONSULTAS_MAXIMA){
					
					idToBitacoras.add(idToBitacorasAProcesar.get(i));
					
				}else if(idToBitacoras.size()==CONSULTAS_MAXIMA){
					
					getGuias(idToBitacoras);
					
					idToBitacoras.clear();
					
				}				
			}
		}else{
			getGuias(idToBitacorasAProcesar);
		}
		
		if(idToBitacoras.size() > 0 ){			
			getGuias(idToBitacoras);			
		}
		
		LOG.info("salir getguia");
	}
	
	@Override
	public String getEstatusBitacora(Integer estatus) {
		String estatusName = "";
		if(estatus.equals(BitacoraGuia.estatusGuia.CONFIRMADO.valor())){
			estatusName = BitacoraGuia.estatusGuia.CONFIRMADO.name();
		}else if(estatus.equals(BitacoraGuia.estatusGuia.DEVOLUCION.valor())){
			estatusName = BitacoraGuia.estatusGuia.DEVOLUCION.name();
		}else if(estatus.equals(BitacoraGuia.estatusGuia.EN_TRANSITO.valor())){
			estatusName = BitacoraGuia.estatusGuia.EN_TRANSITO.name();
		}else if(estatus.equals(BitacoraGuia.estatusGuia.PROCESADA.valor())){
			estatusName = BitacoraGuia.estatusGuia.PROCESADA.name();
		}
		return estatusName;
	}
	
	private void saveDatosGuia(List<TrackingData> trackingData) {
		for(TrackingData dato : trackingData){
			try{
				Map<String, Object> map = new HashMap<String, Object>(1);
				map.put("idGuia", dato.getWaybill());
				List<BitacoraGuia> bitacoras = entidadDao.findByProperties(BitacoraGuia.class, map);
				if(bitacoras!=null){
					saveBitacora(bitacoras.get(0), dato.getStatusSPA(), dato.getDeliveryData(),
							dato.getHistory().getHistory());
				}
			}catch(Exception e){
				LOG.error("Ocurrio un error al procesar la guia: "+dato.getWaybill());
				LOG.error(e.getMessage(), e);
			}
		}
	}
	
	private void saveBitacora(BitacoraGuia bitacora,String estatusGuia,
			DeliveryData delivery, List<History> historial) {
		BitacoraGuia bitacoraWhitEntrega = setDatosEntrega(bitacora, delivery);
		bitacoraWhitEntrega.setEstatus(getIdStatus(estatusGuia));
		
		for(History evento : historial){			
			if(evento.getExceptionCodeDescriptionSPA()!=null){
				bitacoraWhitEntrega.setMotivoRechazo(evento.getExceptionCodeDescriptionSPA());
			}
			saveDetalleBitacora(evento.getEventDateTime(), estatusGuia, bitacoraWhitEntrega.getId());	
		}

		saveDetalleBitacora(delivery.getDeliveryDateTime(), estatusGuia, bitacoraWhitEntrega.getId());
		entidadDao.update(bitacoraWhitEntrega);
	}

	private List<String> getBitacorasAProcesar(){
		String parametro = "estatus1";
		String parametro2 = "estatus2";
		String where = " WHERE model.estatus IN (:"+parametro+", :"+parametro2+")";
		Object parametroValue = BitacoraGuia.estatusGuia.EN_TRANSITO.valor();
		Object parametroValue2 = BitacoraGuia.estatusGuia.PROCESADA.valor();
		List<BitacoraGuia> registros = getBitacorasByProperty(where, parametro, parametroValue,
				parametro2, parametroValue2);
		
		List<String> idToBitacoras = new ArrayList<String>(1);
		if(!registros.isEmpty()){
			for(BitacoraGuia registro: registros){
				if(registro.getIdGuia()!=null && !registro.getIdGuia().isEmpty()){
					idToBitacoras.add(registro.getIdGuia());
				}					
			}
		}
		
		return idToBitacoras;
	}
	
	@SuppressWarnings("unchecked")
	private List<BitacoraGuia> getBitacorasByProperty(String where, String parametro,
			Object parametroValue, String parametro2, Object parametroValue2 ) {
		
		List<BitacoraGuia> registros = new ArrayList<BitacoraGuia>(1);
		Query query = null;
		String queryString = " SELECT model FROM BitacoraGuia model " + where;
				
		try {			
			LOG.trace("Entrando a obtener Bitacoras a procesar Ejecutando quey ");
			query = entityManager.createQuery(queryString);
			query.setParameter(parametro, parametroValue);
			if(parametroValue2!=null && parametro2!=null){
				query.setParameter(parametro2, parametroValue2);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			return query.getResultList();
			
		} catch (Exception e) {
			LOG.error("Excepcion general al invocar: ", e);
		}
		return registros;
	}

	private BitacoraGuia setDatosEntrega(BitacoraGuia bitacora,
			DeliveryData delivery) {
		if(delivery!=null){					
			if(delivery.getDeliveryDateTime()!=null && !delivery.getDeliveryDateTime().isEmpty()){
				bitacora.setFechaEntrega(Utilerias.obtenerFechaDeCadena(delivery.getDeliveryDateTime()));
			}
			if(delivery.getReceiverName()!=null && !delivery.getReceiverName().isEmpty()){
				bitacora.setRecibe(delivery.getReceiverName());
			}
		}		
		return bitacora;
	}

	private void saveDetalleBitacora(String eventDateTime, String estatusGuia, BigDecimal idBitacoraGuia) {
		Map<String, Object> map = new HashMap<String, Object>(1);
		map.put("estatus", estatusGuia);
		map.put("IdToBitacora", idBitacoraGuia);
		List<DetalleBitacora> detalles = entidadDao.findByProperties(DetalleBitacora.class, map);
		if(eventDateTime!=null && !eventDateTime.isEmpty()){
			DetalleBitacora detalle = new DetalleBitacora();
			detalle.setIdToBitacora(idBitacoraGuia);
			detalle.setEstatus(estatusGuia);	
				detalle.setFecha(Utilerias.obtenerFechaDeCadena(eventDateTime.substring(0, topeFecha)));
			if(detalles.size()<=0){
				entidadDao.persist(detalle);
			}else{
				detalle.setId(detalles.get(0).getId());
				entidadDao.update(detalle);	
			}	
		}
	}
	
	private SearchType getSearchTypeByGuia(List<String> guias) {
		WaybillList waybillList = new WaybillList();
		waybillList.setWaybillType(BitacoraGuia.TIPO_BUSQUEDA_GUIA);
		ArrayOfString guiasArray = new ArrayOfString();
		guiasArray.getString().addAll(guias);
		waybillList.setWaybills(guiasArray);
		
		SearchType searchType = new SearchType();
		searchType.setType(BitacoraGuia.BUSQUEDA_LIST);
		searchType.setWaybillList(waybillList);
		
		return searchType;
	}

	private SearchConfiguration getSearchConfigurationByLastEvento(String evento) {
		HistoryConfiguration history = new HistoryConfiguration();
		history.setIncludeHistory(true);
		history.setHistoryType(evento);
		
		SearchConfiguration searchConfiguration = new SearchConfiguration();
		searchConfiguration.setIncludeDimensions(false);
		searchConfiguration.setIncludeCustomerInfo(true);
		searchConfiguration.setIncludeReturnDocumentData(false);
		searchConfiguration.setIncludeInternationalData(false);
		searchConfiguration.setIncludeMultipleServiceData(false);
		searchConfiguration.setIncludeSignature(false);
		searchConfiguration.setIncludeWaybillReplaceData(false);
		searchConfiguration.setHistoryConfiguration(history);
		return searchConfiguration;
	}
	
	private Integer getIdStatus(String estatusGuia) {
		Integer idEstatus = null;
		if(estatusGuia.equalsIgnoreCase("CONFIRMADO")){
			idEstatus = BitacoraGuia.estatusGuia.CONFIRMADO.valor();
		}else if(estatusGuia.equalsIgnoreCase("EN_TRANSITO")){
			idEstatus = BitacoraGuia.estatusGuia.EN_TRANSITO.valor();
		}else if(estatusGuia.equalsIgnoreCase("DEVOLUCION")){
			idEstatus = BitacoraGuia.estatusGuia.DEVOLUCION.valor();
		}else{
			idEstatus = 0;
		}
		
		return idEstatus;
	}

	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
}