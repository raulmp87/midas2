package mx.com.afirme.midas2.dao.impl.negocio.cliente;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionSeccionDao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccion;

@Stateless
public class ConfiguracionSeccionDaoImpl extends JpaDao<Long, ConfiguracionSeccion> implements ConfiguracionSeccionDao {

}
