package mx.com.afirme.midas2.dao.impl.negocio.cliente;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionCampoDao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionCampo;

@Stateless
public class ConfiguracionCampoDaoImpl extends JpaDao<Long, ConfiguracionCampo> implements ConfiguracionCampoDao {


}
