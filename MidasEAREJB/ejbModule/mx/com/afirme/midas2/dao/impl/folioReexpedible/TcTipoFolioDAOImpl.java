package mx.com.afirme.midas2.dao.impl.folioReexpedible;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.folioReexpedible.TcTipoFolioDAO;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.folioReexpedible.TcTipoFolio;

/*
 * Clase que implementa el acceso a datos
 * para el cat\u00e1logo de tipo folio.
 * 
 * @since 10022016
 * 
 * @author AFIRME
 */
@Stateless
public class TcTipoFolioDAOImpl extends EntidadDaoImpl implements TcTipoFolioDAO{

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	public void eliminarTcTipoFolio(TcTipoFolio entity) {
		entity.setActivo(Boolean.FALSE);
		this.update(entity);
	}

	@Override
	public void eliminarVarios(List<TcTipoFolio> entities) {
		for(TcTipoFolio entity: entities){
			this.eliminarTcTipoFolio(entity);
		}
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcTipoFolio> findAll() {
		final String queryString = "select model from TcTipoFolio model where model.activo = :activo";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("activo", Boolean.TRUE);
		query.setMaxResults(100);
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}

	@Override
	public TcTipoFolio findById(Long id) {
		TcTipoFolio entity = entityManager.find(TcTipoFolio.class, id);
		return entity;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<TcTipoFolio> findByProperty(String propertyName, Object value) {
		final StringBuilder queryString = new StringBuilder("select model from TcTipoFolio model where  model.");
				queryString.append(propertyName)
				.append("= :propertyValue");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("propertyValue", value);
		return query.getResultList();
	}

	@Override
	public void persist(TcTipoFolio entity) {
		entityManager.persist(entity);
	}

	@Override
	public List<TcTipoFolio> saveAll(List<TcTipoFolio> entities) {
		entityManager.persist(entities);
		return entities;
	}

	@Override
	public TcTipoFolio update(TcTipoFolio entity) {
		return entityManager.merge(entity);
	}

	@Override
	public List<TcTipoFolio> updateAll(List<TcTipoFolio> entities) {
		for(TcTipoFolio entity: entities){
			update(entity);
		}
		return entities;
	}

	public EntityManager getEntityManager() {
		return entityManager;
	}

	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
}
