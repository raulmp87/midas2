package mx.com.afirme.midas2.dao.impl.cobranza.recibos.consolidar;

import java.math.BigDecimal;
import java.sql.Types;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.sql.DataSource;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.cobranza.recibos.consolidar.ConsolidarRecibosDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.Consolidado;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RecibosConsolidar;
import mx.com.afirme.midas2.domain.cobranza.recibos.consolidar.RelRecibosConsolidado;
import mx.com.afirme.midas2.exeption.ApplicationException;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcOperations;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Stateless
public class ConsolidarReciboDaoImpl extends EntidadDaoImpl implements ConsolidarRecibosDao {

	private static final String INSERTSQL =
	  "INSERT INTO SEYCOS.REL_RECIBOS_CONSOLIDADO (" +
	  "	REF_CARGO_CONS, " +
	  "	IMP_CARGO_CONS, " +
	  "	ID_RECIBO, " +
	  "	FH_CREACION) " +
	  "VALUES (?, ?, ?, ?)";
	

	private static final Logger log = Logger.getLogger(ConsolidarReciboDaoImpl.class);
	
	@Autowired
	private JdbcOperations  jdbcTemplate;
	private SimpleJdbcCall correrCFDI;
	
	private String interfazDataSource= "jdbc/InterfazDataSource";
	private String referenciacargoconsolidado="referenciacargoconsolidado";
	@SuppressWarnings("unused")
	private DataSource dataSource;
	
	@Resource(name="jdbc/InterfazDataSource")
	public void setDataSource(DataSource dataSource) {
		
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.correrCFDI= new SimpleJdbcCall(dataSource)
		.withSchemaName("SEYCOS")
		.withCatalogName("PKG_COB_RECIBOS_CONSOLIDADOS")
		.withProcedureName("CORR_CFD_CONSOL")
		.declareParameters(
				new SqlParameter("pfolioconsolidado", Types.NUMERIC),
				new SqlParameter("pMedioPago", Types.NUMERIC),
				new SqlParameter("pNumTarjeta", Types.NUMERIC),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Resp", Types.VARCHAR)
				);
	}
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Object correrCFDConsolidado(Consolidado consolidado) {
		log.trace(">> correrCFDConsolidado()");

		Object resultCotizacion = null;
		BigDecimal idCodResp = null;
		String pDescResp = null;
		try {
			Long folio =new Long(consolidado.getFoliofiscal());
			BigDecimal medioPago = consolidado.getIdMedioPago();
			Long numTarjeta = consolidado.getNumTarjeta();
			log.info("Ejecutando StoreProcedure SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.CORR_CFD_CONSOL....");
			log.info("Parametro de busqueda:: [folio ="+ folio+ ", pMedioPago="+  medioPago.toString()+ ", pNumTarjeta="+ numTarjeta.toString()+ "] "
						);
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter
			.addValue("pfolioconsolidado", folio);
			sqlParameter
			.addValue("pMedioPago", medioPago.toString());
			sqlParameter
			.addValue("pNumTarjeta", numTarjeta.toString());
			Map<String, Object> execute = this.correrCFDI
					.execute(sqlParameter);
			log.info("Se ha corrido CORR_CFD_CONSOL consolidado...");
			idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			pDescResp = (String) execute.get("pDesc_Resp");
			log.info("Desc_Resp>>"+pDescResp);
			log.info("idCodResp>>"+idCodResp);
				if (idCodResp !=null){				
					throw new ApplicationException("Ocurrio un error al consolidar los recibos: "+idCodResp+"--"+pDescResp);
				}
		} catch (Exception e) {
			log.error("Excepcion general en StoreProcedure SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.CORR_CFD_CONSOL() "+idCodResp+"--"+pDescResp, e);
			throw new ApplicationException("Ocurrio un error al consolidar los recibos: "+idCodResp+"--"+pDescResp);
		}
		
		log.trace(">> correrCFDConsolidado()");
		return resultCotizacion;
	}	
		
		

	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<Consolidado> cancelarRecibosSeleccionados(RecibosConsolidar reciboConsolidar){
		log.trace(">> cancelarRecibosSeleccionados()");
		log.trace(" Parametros de busqueda :: FechaCorte =  {"
				+ reciboConsolidar.getFechaCorte()+ "...."
		        + "} NumPolizaConsolidar =  {"
		        + reciboConsolidar.getNombreUsuario()+ "} "
				+ " numPolizaConsolidar =  {"
		        + reciboConsolidar.getNumPolizaConsolidar()+ "} "
				+ " lista de ids de recibos a consolidar =  {"
				+ reciboConsolidar.getIdRecibosConsolidar()+ "}"
		        + "ImporteTotal =  {"
				+ reciboConsolidar.getImporteTotal()+ "} ");
			
			List<Consolidado> resultList = new ArrayList<Consolidado>();
			StoredProcedureHelper storedHelper = null;
			
			try {
				storedHelper = new StoredProcedureHelper("SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.SP_CANCELAR_CONSOLIDADOS", interfazDataSource);
				
				storedHelper.estableceParametro("pIdsRecibos", String.valueOf( reciboConsolidar.getIdRecibosConsolidar()));
				log.trace("reciboConsolidar.getIdRecibosConsolidar() "+reciboConsolidar.getIdRecibosConsolidar());
				String[] propiedades = new String[] {
													referenciacargoconsolidado,
													"fechacorte",
													"fechacreacion", 
													"poliza", 
													"usuariocreacion",
													"estatus", 
													"fechaconsolidado", 
													"fechalimite",
												   	"importecargoconsolidado",
													"foliofiscal"					
													  };
				// resultados del cursor
				String[] columnas = new String[] { "REF_CARGO_CONS",
													"FH_CORTE",
													"FH_CREACION", 
													"POLIZA", 
													"USUARIO_CREACION",
													"ESTATUS", 
													"FH_CONSOLIDADO", 
													"FH_LIMITE",
												   	"IMP_CARGO_CONS",
													"FOLIO_FISCAL"
												  };
			
			
				
				storedHelper.estableceMapeoResultados(Consolidado.class.getCanonicalName(), propiedades, columnas);
				resultList = storedHelper.obtieneListaResultados();
			} catch (Exception e) {
				log.error("Error: " +  e);
				Integer codErr = null;
				String descErr = null;
				log.error("SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.SP_CANCELAR_CONSOLIDADOS ");
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				log.error("metodo cancelarRecibosSeleccionados numero de excepcion: " + codErr);
				log.error("metodo cancelarRecibosSeleccionados descripcion de excepcion: " + descErr);
				
			}
			if(!resultList.isEmpty()){
				log.debug("numero de recibos {"
						+ resultList.size()+ "} ");
				for (int i = 0; i < resultList.size(); i++) {
					Consolidado consolidadoFor	=resultList.get(i);
					log.debug("Consolidado =  {"
							+ consolidadoFor.toString()+ "}");
				}
				
			}

			log.trace("<< cancelarRecibosSeleccionados()");
			return resultList;	
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<RelRecibosConsolidado> consolidarRecibosSeleccionados(RecibosConsolidar reciboConsolidar)  {
		log.trace(">> consolidarRecibosSeleccionados()");

		RelRecibosConsolidado relRecibosConsolidado;
		List<RelRecibosConsolidado> lista = new ArrayList<RelRecibosConsolidado>();
		log.info(" Parametros de busqueda :: FechaCorte =  {"
				+ reciboConsolidar.getFechaCorte()+ "...."
		        + "} NumPolizaConsolidar =  {"
		        + reciboConsolidar.getNombreUsuario()+ "} "
				+ " num PolizaConsolidar =  {"
		        + reciboConsolidar.getNumPolizaConsolidar()+ "} "
				+ " Id RecibosConsolidar =  {"
				+ reciboConsolidar.getIdRecibosConsolidar()+ "}"
		        + "ImporteTotal =  {"
				+ reciboConsolidar.getImporteTotal()+ "} ");
		Date fechaCreacionConsolidado= new Date();
		String[] recibosAConsolidar = reciboConsolidar.getIdRecibosConsolidar().split(",");
			Long referenciaReciboConsolidado;			
			Calendar today = Calendar.getInstance();
			
			Consolidado consolidado = new Consolidado();
			consolidado.setFechacorte(reciboConsolidar.getFechaCorte());
			consolidado.setFechacreacion(today.getTime());
			consolidado.setFoliofiscal("");
			consolidado.setPoliza(""+reciboConsolidar.getNumPolizaConsolidar());
			consolidado.setUsuariocreacion(reciboConsolidar.getNombreUsuario());
			consolidado.setEstatus("N");
			consolidado.setFechaconsolidado(reciboConsolidar.getFechaConsolidado());
			consolidado.setFechalimite(null);
			consolidado.setEstatusdesc(null);
			consolidado.setFechaconsolidadostr(null);
			consolidado.setFechalimitestr(null);
			consolidado.setFechacortestr(null);
			consolidado.setFechacreacionstr(null);
			consolidado=save(consolidado);
			referenciaReciboConsolidado=consolidado.getReferenciacargoconsolidado();
			
			log.debug(referenciaReciboConsolidado);
			  int primeraVez=0;
			if(recibosAConsolidar.length >1){
			for(String s:recibosAConsolidar){
					primeraVez=primeraVez+1;
					log.debug(s);
					relRecibosConsolidado= new RelRecibosConsolidado();
					relRecibosConsolidado.setIdrecibo(new Long(recibosAConsolidar[primeraVez-1]));
					relRecibosConsolidado.setImportecargoconsolidado(reciboConsolidar.getImporteTotal());
					relRecibosConsolidado.setFechacreacion(fechaCreacionConsolidado);
					relRecibosConsolidado.setReferenciacargoconsolidado(referenciaReciboConsolidado);
					guardarRegistroReciboConsolidadoConReferencia(relRecibosConsolidado);	
					lista.add(relRecibosConsolidado);
			
				
	  		}
			}
			
			log.trace("<< solidarRecibosSeleccionados()");
		return lista;
	}
	
	private void guardarRegistroReciboConsolidadoConReferencia(	RelRecibosConsolidado relRecibosConsolidado){
		log.trace(">> guardarRegistroReciboConsolidadoConReferencia()");

		try {
    		Object[] params = new Object[] {relRecibosConsolidado.getReferenciacargoconsolidado(),relRecibosConsolidado.getImportecargoconsolidado(), relRecibosConsolidado.getIdrecibo(), relRecibosConsolidado.getFechacreacion() };
    		int[] types = new int[] {Types.NUMERIC, Types.NUMERIC, Types.NUMERIC, Types.TIMESTAMP };
    		int row = jdbcTemplate.update(INSERTSQL, params, types);
    		log.debug("recibo en la posicion :: "
					+ row+ " :: insertado");
    		log.debug("Se Guardo RelRecibosConsolidado 	::		ConsolidarReciboDaoImpl	::	");
	        } catch (RuntimeException re) {
	        	log.error("Error al guardar RelRecibosConsolidado 	::		ConsolidarReciboDaoImpl	::	save	::	ERROR	::	",re);
	        	throw re;
        }
	        
	        log.trace("<< guardarRegistroReciboConsolidadoConReferencia()");
	}
	@Override
	public List<Consolidado> obtenerRecibosConsolidados(Consolidado consolidado){
		log.trace(">> obtenerListadoRecibos()");
		List<Consolidado> resultList = new ArrayList<Consolidado>();
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper("SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.SP_OBTENER_CONSOLIDADOS", interfazDataSource);
			
			storedHelper.estableceParametro("pIdToPoliza", String.valueOf(consolidado.getFoliofiscal()));
			storedHelper.estableceParametro("pTotalCount", String.valueOf(consolidado.getTotalCount()));
			storedHelper.estableceParametro("pPosStart", String.valueOf(consolidado.getPosStart()));
			storedHelper.estableceParametro("pCount", String.valueOf(consolidado.getCount()));
			storedHelper.estableceParametro("pOrderByAttribute", String.valueOf(consolidado.getOrderByAttribute()));
			storedHelper.estableceParametro("pFilterForm", String.valueOf(consolidado.getFilterForm()));
			storedHelper.estableceParametro("pReporteExcel", String.valueOf(consolidado.getReporteExcel()));
			String[] propiedades = new String[] {"totalCount",
												 referenciacargoconsolidado,
												"fechacorte",
												"fechacreacion", 
												"poliza", 
												"usuariocreacion",
												"estatus", 
												"fechaconsolidado", 
												"fechalimite",
											   	"importecargoconsolidado",
												"foliofiscal"					
												  };
			// resultados del cursor
			String[] columnas = new String[] {  "TOTAL_REGISTROS",
												"REF_CARGO_CONS",
												"FH_CORTE",
												"FH_CREACION", 
												"POLIZA", 
												"USUARIO_CREACION",
												"ESTATUS", 
												"FH_CONSOLIDADO", 
												"FH_LIMITE",
											   	"IMP_CARGO_CONS",
												"FOLIO_FISCAL"
											  };
		
			storedHelper.estableceMapeoResultados(Consolidado.class.getCanonicalName(), propiedades, columnas);
			resultList = storedHelper.obtieneListaResultados();
		
		} catch (Exception e) {
			log.error("Error: " +  e);
			Integer codErr = null;
			String descErr = null;
			log.error("SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.SP_OBTENER_CONSOLIDADOS ");
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			log.info("metodo obtenerRecibosConsolidados numero de excepcion: " + codErr);
			log.info("metodo obtenerRecibosConsolidados Descripcion de Excepcion: " + descErr);
			
			
			
		}
		if(!resultList.isEmpty()){
			log.debug("Cantidad de recibos encontrados {"
					+ resultList.size()+ "} ");
			for (int i = 0; i < resultList.size(); i++) {
				Consolidado consolidadoFor	=resultList.get(i);
				log.debug("Consolidado =  {"
						+ consolidadoFor.toString()+ "}");
			}
			
		}
		
		log.trace("<< obtenerListadoRecibos()");
		return resultList;	
	}

	@Override
	public List<RecibosConsolidar> obtenerListadoRecibos(RecibosConsolidar reciboConsolidar ) {
		log.trace(">> obtenerListadoRecibos()");
		
		DateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String fechaConsolidado=sdf.format(reciboConsolidar.getFechaConsolidado());
		
		String fechaCorte=sdf.format(reciboConsolidar.getFechaCorte());		
		
		log.info(" Parametros de busquedad Fecha Consolidado la fecha que se captura en pantalla fecha Consolidado =  {"
				+ fechaCorte+ "} "+ 
				" Parametros de busquedad Fecha Corte la fecha que se captura en pantalla fecha Corte =  {"
				+ fechaConsolidado+ "} "+
				" Centro de Emisión =   {" +			
				+ reciboConsolidar.getCentroEmision()+ "}" +
				" Num Poliza a Consolidar =   {" +
				
				+ reciboConsolidar.getNumPolizaConsolidar()+ "}" +
				"	   Numero de Renovacion de Poliza =   {" +
				+ reciboConsolidar.getNumRenovPol()+ "}" +
						"");
		
		List<RecibosConsolidar> resultList = new ArrayList<RecibosConsolidar>();
		StoredProcedureHelper storedHelper = null;
		
		
		try {
			storedHelper = new StoredProcedureHelper("SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.SP_OBTENER_RECIBOS", interfazDataSource);
			
			storedHelper.estableceParametro("pFechaCorte", String.valueOf(fechaCorte));
			storedHelper.estableceParametro("pFechaConsolidado", String.valueOf(fechaConsolidado));
			storedHelper.estableceParametro("pIdToPoliza", String.valueOf(reciboConsolidar.getNumPolizaConsolidar()));
			storedHelper.estableceParametro("pIdCentroEmision", String.valueOf(reciboConsolidar.getCentroEmision()));
			storedHelper.estableceParametro("pIdNumRenovPol", String.valueOf(reciboConsolidar.getNumRenovPol()));
			String[] propiedades = new String[] {"fechaEmision",
												"endoso",
												"idRecibo", 
												"recibo", 
												"finivigencia",
												"ffinvigencia", 
												"titular", 
												"primaneta",
											   	"primatotal",
												"iva",
											   	"sitrbo"					
												  };
			// resultados del cursor
			String[] columnas = new String[] { "FH_EMISION",
												"ENDOSO",
												"IDRECIBO", 
												"recibo", 
												"finivigencia",
												"ffinvigencia", 
												"TITULAR", 
												"primaneta",
											   	"primatotal",
												"iva",
											   	"sitrbo"
											  };
		
			storedHelper.estableceMapeoResultados(RecibosConsolidar.class.getCanonicalName(), propiedades, columnas);
			resultList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			log.error("Error: " +  e);
			Integer codErr = null;
			String descErr = null;
			log.error("SEYCOS.PKG_COB_RECIBOS_CONSOLIDADOS.SP_OBTENER_RECIBOS ");
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
		
			log.error("metodo obtenerListadoRecibos numero de excepcion: " + codErr);
			log.error("metodo obtenerListadoRecibos descripcion de excepcion: " + descErr);
			
		}
		if(!resultList.isEmpty()){
			log.debug("Cantidad de recibos encontrados {"
					+ resultList.size()+ "} ");
			for (int i = 0; i < resultList.size(); i++) {
				RecibosConsolidar recibosConsolidar	=resultList.get(i);
				log.debug("RecibosConsolidar =  {"
						+ recibosConsolidar.toString()+ "}");
			}
			
		}
		log.trace("<< obtenerListadoRecibos()");
		return resultList;
	}
	
	
	 public static Date getPrimerDiaDelSiguienteMes() {
		 log.trace(">> getPrimerDiaDelSiguienteMes()");
	        Calendar cal = Calendar.getInstance();
	        int numeroDeMes=cal.get(Calendar.MONTH);
	        numeroDeMes=numeroDeMes+1;
	        int numeroDelAnio=cal.get(Calendar.YEAR);
	        if(numeroDeMes==11){/*mes once es diciembre*/
	        	numeroDeMes=0;/*mes CERO es ENERO*/
	        	numeroDelAnio=numeroDelAnio+1;
	        }
	        cal.set(numeroDelAnio, 
	        		numeroDeMes ,
	                cal.getActualMinimum(Calendar.DAY_OF_MONTH),
	                cal.getMinimum(Calendar.HOUR_OF_DAY),
	                cal.getMinimum(Calendar.MINUTE),
	                cal.getMinimum(Calendar.SECOND));
	        log.trace("<< getPrimerDiaDelSiguienteMes()");
	        return cal.getTime();
	    }
	 
	 
	 
	 public Consolidado save(Consolidado entity) {
		 log.trace("Guardando Consolidado 	::		ConsolidarReciboDaoImpl	::	save	::	INICIO	::	");
	    	try {
	    		entityManager.persist(entity);
	    		log.debug("Se Guardo Consolidado 	::		ConsolidarReciboDaoImpl	::	save	::	FIN	::	");
		        } catch (RuntimeException re) {
		        	log.error("Error al guardar Consolidado 	::		ConsolidarReciboDaoImpl	::	save	::	ERROR	::	",re);
		        	throw re;
	        }
		        return entity;
	    }
		
	    public void delete(Consolidado entity) {
	    	log.trace("Eliminando Consolidado 	::		ConsolidarReciboDaoImpl	::	delete	::	INICIO	::	");
	    	Consolidado consolidado= new Consolidado();
	    	try {
	    		consolidado = entityManager.getReference(Consolidado.class, entity.getReferenciacargoconsolidado());
	            entityManager.remove(consolidado);
	            log.trace("Se Elimino Consolidado 	::		ConsolidarReciboDaoImpl	::	delete	::	FIN	::	");
		        } catch (RuntimeException re) {
		        	log.error("Error al eliminar Consolidado 	::		ConsolidarReciboDaoImpl	::	delete	::	ERROR	::	",re);
		            throw re;
	        }
	    }
	    
	    public Consolidado update(Consolidado entity) {
	    	log.trace("Actualizando Consolidado 	::		ConsolidarReciboDaoImpl	::	update	::	INICIO	::	");
	    	try {
	    		Consolidado result = entityManager.merge(entity);
	    		log.debug("Se Actualizo Consolidado 	::		ConsolidarReciboDaoImpl	::	update	::	FIN	::	");
		        return result;
	        } catch (RuntimeException re) {
	        	log.error("Error al actualizar Consolidado 	::		ConsolidarReciboDaoImpl	::	update	::	ERROR	::	",re);
	        	throw re;
	        }
	    }
	    
	    public Consolidado findById( Long referenciacargoconsolidado) {
	    	log.debug("Buscando por Id 	::		ConsolidarReciboDaoImpl	::	findById	::	INICIO	::	");
	    	Consolidado result;    	
	    	try {
	    	final String queryString = "";
			Query query = entityManager.createQuery(queryString,Consolidado.class);
			query.setParameter("referenciacargoconsolidado", referenciacargoconsolidado);
			query.setParameter("borrado", false);
			result = (Consolidado) query.getSingleResult();
	        } catch (RuntimeException re) {
	        	log.error("Error al buscar por Id 	::		ConsolidarReciboDaoImpl	::	findById	::	ERROR	::	",re);
	        	result = null;
	        }        
	        log.trace("Se busco por Id 	::		ConsolidarReciboDaoImpl	::	findById	::	FIN	::	");
	        return result;
	    }
	 
}