package mx.com.afirme.midas2.dao.impl.catalogos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.catalogos.ColorVehiculoDao;
import mx.com.afirme.midas2.domain.catalogos.ColorVehiculo;
import mx.com.afirme.midas2.domain.catalogos.ColorVehiculo_;

@Stateless
public class ColorVehiculoDaoImpl extends EntidadDaoImpl implements
		ColorVehiculoDao {

	public List<ColorVehiculo> findByFilters(ColorVehiculo filtroColorVehiculo) {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			Integer intNull = null;
			Expression<Integer> claveExp = cb.literal(filtroColorVehiculo.getClave());
			Expression<Integer> intNullExp = cb.literal(intNull);
			
			CriteriaQuery<ColorVehiculo> criteriaQuery = cb.createQuery(ColorVehiculo.class);
			Root<ColorVehiculo> root = criteriaQuery.from(ColorVehiculo.class);
			Predicate predicado = cb.and(cb.or(cb.equal(claveExp, intNullExp),cb.equal(root.get(ColorVehiculo_.clave), filtroColorVehiculo.getClave())),
										 cb.like(cb.upper(root.get(ColorVehiculo_.descripcion)),"%" + filtroColorVehiculo.getDescripcion().toUpperCase() + "%"));
			criteriaQuery.where(predicado);
			
			TypedQuery<ColorVehiculo> query = entityManager.createQuery(criteriaQuery);
			return query.getResultList();
		
	}
}
