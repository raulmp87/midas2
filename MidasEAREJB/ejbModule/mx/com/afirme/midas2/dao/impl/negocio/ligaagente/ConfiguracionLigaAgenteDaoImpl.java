package mx.com.afirme.midas2.dao.impl.negocio.ligaagente;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.negocio.ligaagente.ConfiguracionLigaAgenteDao;
import mx.com.afirme.midas2.domain.negocio.ligaagente.ConfiguracionLigaAgente;
import mx.com.afirme.midas2.dto.negocio.ligaagente.ConfigLigaAgenteDTO;
import mx.com.afirme.midas2.dto.negocio.ligaagente.MonitoreoLigaAgenteDTO;

@Stateless
public class ConfiguracionLigaAgenteDaoImpl implements ConfiguracionLigaAgenteDao{

	@PersistenceContext
	private EntityManager entityManager;
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<ConfigLigaAgenteDTO> buscarConfiguracionesLigasAgentes(ConfigLigaAgenteDTO cfgLigaFiltro){
		StringBuilder queryString	= new StringBuilder();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		List<ConfigLigaAgenteDTO> cfgsLigasAgentes = new ArrayList<ConfigLigaAgenteDTO>();
		

		queryString.append(" SELECT new mx.com.afirme.midas2.dto.negocio.ligaagente.ConfigLigaAgenteDTO(" +
				" model.id, model.nombre, model.estatus, model.descripcion, model.correo, model.fechaCreacion, " +
				" model.codigoUsuarioCreacion, model.agente.id, model.agente.persona.nombreCompleto, " +
				" model.negocio.descripcionNegocio, model.liga, model.emitir ) " + 
				" FROM ConfiguracionLigaAgente AS model ");
		queryString.append(" WHERE model.fechaCreacion IS NOT NULL ");
		
		if(cfgLigaFiltro.getId() != null && cfgLigaFiltro.getId() > 0){
			queryString.append(" AND model.id = :id ");
			Utilerias.agregaHashLista(listaParametrosValidos, "id", cfgLigaFiltro.getId());
		}
		
		if(cfgLigaFiltro.getNombre() != null && !cfgLigaFiltro.getNombre().isEmpty() ){
			queryString.append(" AND UPPER(model.nombre) LIKE UPPER('%");
			queryString.append(cfgLigaFiltro.getNombre()); queryString.append("%') ");
		}
		
		if(cfgLigaFiltro.getEstatus() != null && !cfgLigaFiltro.getEstatus().isEmpty() ){
			queryString.append(" AND model.estatus = :estatus ");
			Utilerias.agregaHashLista(listaParametrosValidos, "estatus", cfgLigaFiltro.getEstatus());
		}
		
		if(cfgLigaFiltro.getNombreAgente() != null 
				&& !cfgLigaFiltro.getNombreAgente().isEmpty() ){
			queryString.append(" AND UPPER(model.agente.persona.nombreCompleto) LIKE UPPER('%");
			queryString.append(cfgLigaFiltro.getNombreAgente()); queryString.append("%') ");
		}
		
		if(cfgLigaFiltro.getIdAgente() != null
				&& cfgLigaFiltro.getIdAgente() > 0){
			queryString.append(" AND model.agente.id = :idAgente ");
			Utilerias.agregaHashLista(listaParametrosValidos, "idAgente", cfgLigaFiltro.getIdAgente());
		}
		
		if(cfgLigaFiltro.getIdNegocio() != null
				&& cfgLigaFiltro.getIdNegocio() > 0 ){
			queryString.append(" AND model.negocio.idToNegocio = :idToNegocio ");
			Utilerias.agregaHashLista(listaParametrosValidos, "idToNegocio", cfgLigaFiltro.getIdNegocio());
		}
		
		if(cfgLigaFiltro.getFechaInicio() != null){
			queryString.append(" AND FUNC('trunc', model.fechaCreacion ) >= :fechaInicio ");
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaInicio", cfgLigaFiltro.getFechaInicio());
		}
		
		if(cfgLigaFiltro.getFechaFin() != null){
			queryString.append(" AND FUNC('trunc', model.fechaCreacion ) <= :fechaFin");
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFin", cfgLigaFiltro.getFechaFin());
		}
		
		if(cfgLigaFiltro.getEmitir() != null && cfgLigaFiltro.getEmitir()){
			queryString.append(" AND UPPER(model.emitir) = :emitir" );
			Utilerias.agregaHashLista(listaParametrosValidos, "emitir", ConfiguracionLigaAgente.ESTATUS_ACTIVO.toString());
		}
		
		queryString.append(" ORDER BY model.fechaCreacion DESC " );
		Query query = entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		cfgsLigasAgentes = (List<ConfigLigaAgenteDTO>)query.getResultList();
		
		return cfgsLigasAgentes;
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public List<MonitoreoLigaAgenteDTO> buscarMonitoreoLigasAgentes(MonitoreoLigaAgenteDTO monitorLigaFiltro){
		List<MonitoreoLigaAgenteDTO> monitorLigasAgentes = new ArrayList<MonitoreoLigaAgenteDTO>();
		String spName = "MIDAS.PKGAUT_LIGASAGENTES.monitoreoLigasAgentes"; 
		StoredProcedureHelper storedHelper  = null;
		
		try {
			String propiedades = "id,nombre,idAgente,nombreAgente,descripcionNegocio,idCotizacion,fechaCotizacion," +
					"prospectoCotizacion,telefonoProspecto,correoProspecto,producto,tipoPoliza,lineaNegocio,marca," +
					"modelo,descripcionFinalVehiculo,numeroSerieVehiculo,primaNeta,primaTotal,numeroPoliza,fechaEmision,resultadoContador";

			String columnasBaseDatos = "id,nombre,idAgente,nombreAgente,descripcionNegocio,idCotizacion,fechaCotizacion," +
					"prospectoCotizacion,telefonoProspecto,correoProspecto,producto,tipoPoliza,lineaNegocio,marca," +
					"modelo,descripcionFinalVehiculo,numeroSerieVehiculo,primaNeta,primaTotal,numeroPoliza,fechaEmision,contadorResultados";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(MonitoreoLigaAgenteDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltro(storedHelper, monitorLigaFiltro);
			monitorLigasAgentes = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de monitoreo de Ligas de Agentes ..."+descErr
						, Level.WARNING, e);    
			}
		}
		
		return monitorLigasAgentes;
	}
	
	private Object convertToNull(Object obj){
		if( obj instanceof String){
			obj = (obj != null && ((String)obj).equals(""))?null:obj;
		}else if ( obj instanceof Boolean){
			obj = (obj != null && ((Boolean)obj)== Boolean.FALSE)?null:obj;
		}
		return obj;
	}

	private void agregaParametrosDelFiltro(StoredProcedureHelper storedHelper, MonitoreoLigaAgenteDTO filtro){
		storedHelper.estableceParametro("p_codigo", 			this.convertToNull( filtro.getId() ));
		storedHelper.estableceParametro("p_nombreAgente", 		this.convertToNull( filtro.getNombreAgente() ));
		storedHelper.estableceParametro("p_idNegocio", 			this.convertToNull( filtro.getIdNegocio() ));
		storedHelper.estableceParametro("p_prospectoCotizacion",this.convertToNull( filtro.getProspectoCotizacion() ));
		storedHelper.estableceParametro("p_fechaInicio",		this.convertToNull( filtro.getFechaInicio() ));
		storedHelper.estableceParametro("p_fechaFin", 			this.convertToNull( filtro.getFechaFin() ));
		storedHelper.estableceParametro("p_numeroSerie", 		this.convertToNull( filtro.getNumeroSerieVehiculo() ));
		storedHelper.estableceParametro("p_incluirPoliza", 		this.convertToNull( filtro.isIncluirPolizas() ));
		storedHelper.estableceParametro("p_idAgente", 			this.convertToNull( filtro.getIdAgente() ));
	}
	
	@Override
	public MonitoreoLigaAgenteDTO buscarMonitoreoLigasAgentesContador(MonitoreoLigaAgenteDTO monitorLigaFiltro){
		MonitoreoLigaAgenteDTO monitorLigasAgentes = new MonitoreoLigaAgenteDTO();
		monitorLigasAgentes.setContadorTotal(0L);
		monitorLigasAgentes.setContadorPolizas(0L);
		String spName = "MIDAS.PKGAUT_LIGASAGENTES.monitoreoLigasAgentesConteo"; 
		StoredProcedureHelper storedHelper  = null;
		
		try {
			String propiedades = "contadorTotal,contadorPolizas";

			String columnasBaseDatos = "contadorTotal,contadorPolizas";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(MonitoreoLigaAgenteDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltro(storedHelper, monitorLigaFiltro);
			monitorLigasAgentes = (MonitoreoLigaAgenteDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de monitoreo de Ligas de Agentes ..."+descErr
						, Level.WARNING, e);    
			}
		}
		
		return monitorLigasAgentes;
	}
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public List<MonitoreoLigaAgenteDTO> buscarMonitoreoLigasAgentesPaginado(MonitoreoLigaAgenteDTO monitorLigaFiltro){
		List<MonitoreoLigaAgenteDTO> monitorLigasAgentes = new ArrayList<MonitoreoLigaAgenteDTO>();
		String spName = "MIDAS.PKGAUT_LIGASAGENTES.monitoreoLigasAgentesPaginado"; 
		StoredProcedureHelper storedHelper  = null;
		
		try {
			String propiedades = "id,nombre,idAgente,nombreAgente,descripcionNegocio,idCotizacion,fechaCotizacion," +
					"prospectoCotizacion,telefonoProspecto,correoProspecto,producto,tipoPoliza,lineaNegocio,marca," +
					"modelo,descripcionFinalVehiculo,numeroSerieVehiculo,primaNeta,primaTotal,numeroPoliza,fechaEmision";

			String columnasBaseDatos = "id,nombre,idAgente,nombreAgente,descripcionNegocio,idCotizacion,fechaCotizacion," +
					"prospectoCotizacion,telefonoProspecto,correoProspecto,producto,tipoPoliza,lineaNegocio,marca," +
					"modelo,descripcionFinalVehiculo,numeroSerieVehiculo,primaNeta,primaTotal,numeroPoliza,fechaEmision";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(MonitoreoLigaAgenteDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltroPaginado(storedHelper, monitorLigaFiltro);
			monitorLigasAgentes = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de monitoreo de Ligas de Agentes ..."+descErr
						, Level.WARNING, e);    
			}
		}
		
		return monitorLigasAgentes;
	}
	
	private void agregaParametrosDelFiltroPaginado(StoredProcedureHelper storedHelper, MonitoreoLigaAgenteDTO filtro){
		storedHelper.estableceParametro("p_codigo", 			this.convertToNull( filtro.getId() ));
		storedHelper.estableceParametro("p_nombreAgente", 		this.convertToNull( filtro.getNombreAgente() ));
		storedHelper.estableceParametro("p_idNegocio", 			this.convertToNull( filtro.getIdNegocio() ));
		storedHelper.estableceParametro("p_prospectoCotizacion",this.convertToNull( filtro.getProspectoCotizacion() ));
		storedHelper.estableceParametro("p_fechaInicio",		this.convertToNull( filtro.getFechaInicio() ));
		storedHelper.estableceParametro("p_fechaFin", 			this.convertToNull( filtro.getFechaFin() ));
		storedHelper.estableceParametro("p_numeroSerie", 		this.convertToNull( filtro.getNumeroSerieVehiculo() ));
		storedHelper.estableceParametro("p_incluirPoliza", 		this.convertToNull( filtro.isIncluirPolizas() ));
		storedHelper.estableceParametro("p_idAgente", 			this.convertToNull( filtro.getIdAgente() ));
		storedHelper.estableceParametro("p_startsWith", 		this.convertToNull( filtro.getPosStart() ));
		storedHelper.estableceParametro("p_count", 				this.convertToNull( filtro.getCount() ));
		storedHelper.estableceParametro("p_orderBy", 			this.convertToNull( filtro.getOrderByAttribute() ));
		
	}
	
}
