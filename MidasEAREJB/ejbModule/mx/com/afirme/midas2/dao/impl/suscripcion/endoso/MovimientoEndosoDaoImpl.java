package mx.com.afirme.midas2.dao.impl.suscripcion.endoso;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.dao.suscripcion.endoso.MovimientoEndosoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;

@Stateless
public class MovimientoEndosoDaoImpl implements MovimientoEndosoDao{

	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public void remove(Long controlEndosoCotId){
		String queryString = "Delete from  MovimientoEndoso model where model.controlEndosoCot.id =  "+ controlEndosoCotId;
		Query query = entityManager.createQuery(queryString);

		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoEndoso> movimientosEndosoInciso(
			ControlEndosoCot controlEndosoCot, BitemporalInciso biInciso) {
		
		Query queryMovEndoso = null;
		
		StringBuilder queryString = new StringBuilder(" select mEndoso from MovimientoEndoso mEndoso ");
		queryString.append(" WHERE mEndoso.controlEndosoCot.id = :controlEndosoId AND ");
		queryString.append(" ( ");
		queryString.append(" 	( mEndoso.tipo = :tipoEndosoInciso AND mEndoso.bitemporalInciso = :bitemporal ) ");
		queryString.append("  	OR ");
		queryString.append("       ( ");
		queryString.append("    		mEndoso.tipo = :tipoEndosoPoliza AND mEndoso.id in (  select mEndoso.id from MovimientoEndoso mEndoso WHERE mEndoso.controlEndosoCot.id = :controlEndosoId AND ( mEndoso.tipo = :tipoEndoso AND mEndoso.bitemporalInciso = :bitemporal )  )  ");
		queryString.append("  		) ");
		queryString.append("    OR ");
		queryString.append("       ( ");
		queryString.append("    		mEndoso.tipo = :tipoEndosoCobertura AND mEndoso.movimientoEndoso in  ( select mEndoso2 from MovimientoEndoso mEndoso2 WHERE mEndoso2.controlEndosoCot.id = :controlEndosoId AND ( mEndoso2.tipo = :tipoEndoso AND mEndoso2.bitemporalInciso = :bitemporal )  )  ");
		queryString.append("  		) ");
		queryString.append(" ) ");
		
		queryMovEndoso = this.entityManager.createQuery( queryString.toString() );
		
		queryMovEndoso.setParameter("controlEndosoId"     , controlEndosoCot.getId() );
		queryMovEndoso.setParameter("tipoEndosoInciso"    , TipoMovimientoEndoso.INCISO );
		queryMovEndoso.setParameter("tipoEndosoPoliza"    , TipoMovimientoEndoso.POLIZA);
		queryMovEndoso.setParameter("tipoEndosoCobertura" , TipoMovimientoEndoso.COBERTURA);
		queryMovEndoso.setParameter("bitemporal"          , biInciso );
		queryMovEndoso.setParameter("tipoEndoso"          , TipoMovimientoEndoso.INCISO );
		
		List<MovimientoEndoso> lMovientoEndoso = queryMovEndoso.getResultList(); 
		return lMovientoEndoso;
	}
	
	@Override
	public BigDecimal obtienePrimaDerechosProrrata(BigDecimal idToPoliza, Integer numeroInciso, Date fechaIniVigencia) {
		
		BigDecimal primaDerechos = BigDecimal.ZERO;
		
	    if(idToPoliza==null){
	    	return primaDerechos;
	    }
		LogUtil.log("finding all obtienePrimaDerechosProrrata instances", Level.INFO,null);
		try {

			String queryString = "SELECT MIDAS.FN_AUT_GET_PRIMA_DERECHOS_PRO(?1,?2,?3) FROM DUAL";
			
			Query query = entityManager.createNativeQuery(queryString);
			
			query.setParameter(1,  idToPoliza);
			query.setParameter(2,  numeroInciso);
			query.setParameter(3,  fechaIniVigencia);

			Object object = query.getSingleResult();		
			if(object != null){
				if(object instanceof BigDecimal){
					BigDecimal valor = (BigDecimal) object;
					primaDerechos = valor;
				}else if(object instanceof Double){
					Double valor = (Double) object;
					primaDerechos = new BigDecimal(valor);
				}else if(object instanceof Integer){
					Integer valor = (Integer) object;
					primaDerechos = new BigDecimal(valor);
				} else if(object instanceof Short){
					Short valor = (Short)object;
					primaDerechos = new BigDecimal(valor);
				}
				
			}
			primaDerechos = primaDerechos.negate();
			
			return primaDerechos;
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
		}
		return primaDerechos;
	}	
	
}
