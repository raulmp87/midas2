package mx.com.afirme.midas2.dao.impl.tarea;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.tarea.EmisionPendienteDao;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.EstatusRegistro;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.TipoEmision;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class EmisionPendienteDaoImpl implements EmisionPendienteDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<EmisionPendiente> buscarEmisionesPendientes(EmisionPendiente filtro) {
		
		Query query = getQueryEmisionesPendientesSinProcesar(filtro, false, false);
				
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EmisionPendiente> buscarCancelacionesPendientesAutos(EmisionPendiente filtro) {
		
		Query query = getQueryEmisionesPendientesSinProcesar(filtro, false, true);
		
		if(filtro.getPrimerRegistroACargar() != null && filtro.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(filtro.getPrimerRegistroACargar().intValue());
			query.setMaxResults(filtro.getNumeroMaximoRegistrosACargar().intValue());
		}
				
		return query.getResultList();
	}

	@Override
	public Long obtenerTotalPaginacionCancelacionesPendientesAutos(EmisionPendiente filtro) {		
		Query query = getQueryEmisionesPendientesSinProcesar(filtro, true, true);		
		return Long.valueOf(query.getResultList().size());	
	}
	
	
	@SuppressWarnings("rawtypes")
	private Query getQueryEmisionesPendientesSinProcesar(EmisionPendiente filtro, Boolean esConteo, 
			Boolean esCancelacion) {
		
		if (filtro == null)	{
			return null;				
		}
		
		Query query = null;
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String subject = "DISTINCT NEW mx.com.afirme.midas2.domain.tarea.EmisionPendiente(emisionPendiente.id, emisionPendiente.idToPoliza, " +
				"emisionPendiente.numeroEndoso, emisionPendiente.estatusRegistro, emisionPendiente.claveTipoEndoso, " +
				"emisionPendiente.fechaInicioVigencia, emisionPendiente.valorPrimaNeta, emisionPendiente.valorBonifComision, " +
				"emisionPendiente.valorDerechos, emisionPendiente.valorRPF, emisionPendiente.valorIVA, emisionPendiente.valorPrimaTotal, " +
				"emisionPendiente.valorComision, emisionPendiente.valorComisionRPF, emisionPendiente.valorBonifComisionRPF, " +
				"emisionPendiente.numeroInciso, emisionPendiente.tipoEmision, emisionPendiente.bloqueoEmision, " +
				"emisionPendiente.fechaValidacion, bitemporalCotizacion.value.nombreContratante, " +
				"bitemporalCotizacion.value.solicitud.nombreAgente, emisionPendiente.migrada, emisionPendiente.claveMotivoEndoso, FALSE, \"\", " +
				"poliza.numeroPolizaSeycos, poliza.clavePolizaSeycos, bitemporalCotizacion.value.solicitud.codigoAgente, poliza.codigoProducto, " +
				"poliza.codigoTipoPoliza, poliza.numeroRenovacion, poliza.numeroPoliza) "; 
		
		StringBuilder queryString = new StringBuilder("SELECT " + subject + " FROM " +
				"EmisionPendiente emisionPendiente ");
		
		if (esCancelacion) {
			queryString.append(",PolizaDTO poliza, BitemporalCotizacion bitemporalCotizacion ");
			queryString.append(" WHERE FUNC('trunc', emisionPendiente.fechaValidacion) <= :fechaValidacion ");
			queryString.append(" AND emisionPendiente.id = (SELECT MAX(emisionPendienteInt.id) FROM  EmisionPendiente emisionPendienteInt ");
			queryString.append(" WHERE emisionPendienteInt.id  = emisionPendiente.id)");
			queryString.append(" AND poliza.idToPoliza = emisionPendiente.idToPoliza");
			queryString.append(" AND poliza.cotizacionDTO.idToCotizacion = bitemporalCotizacion.continuity.numero");
			queryString.append(" AND (:validOn >= bitemporalCotizacion.validityInterval.from and :validOn < bitemporalCotizacion.validityInterval.to)");
			queryString.append(" AND (:knownOn >= bitemporalCotizacion.recordInterval.from and :knownOn < bitemporalCotizacion.recordInterval.to)");
			
			queryString.append(" AND bitemporalCotizacion.recordStatus = :notInProcess");
			
			Utilerias.agregaHashLista(listaParametrosValidos, "validOn", filtro.getFechaValidacion());
			Utilerias.agregaHashLista(listaParametrosValidos, "knownOn", TimeUtils.current().toDate());
			Utilerias.agregaHashLista(listaParametrosValidos, "notInProcess", RecordStatus.NOT_IN_PROCESS);
			
			//Filtro Tipo Emision
			if(filtro.getTipoEmision() == null)
			{
				queryString.append(" AND emisionPendiente.tipoEmision IN (:tipoEmisionCancPolAutos, :tipoEmisionCancEndAutos)");
				Utilerias.agregaHashLista(listaParametrosValidos, "tipoEmisionCancPolAutos", TipoEmision.CANC_POL_AUTOS.getTipoEmision());
				Utilerias.agregaHashLista(listaParametrosValidos, "tipoEmisionCancEndAutos", TipoEmision.CANC_END_AUTOS.getTipoEmision());				
			}
			else
			{
				queryString.append(" AND emisionPendiente.tipoEmision = :tipoEmision ");
				Utilerias.agregaHashLista(listaParametrosValidos, "tipoEmision", filtro.getTipoEmision());				
			}
			
			//Filtro Numero Poliza
			if(filtro.getNumeroPoliza() != null && !filtro.getNumeroPoliza().isEmpty()) {
				
				queryString.append(" AND poliza.numeroPoliza = :numeroPoliza");
				
				Utilerias.agregaHashLista(listaParametrosValidos, "numeroPoliza", Integer.valueOf(filtro.getNumeroPoliza()));
			}
			
			//Filtro Nombre Contratante
			if(filtro.getNombreContratante() != null && !filtro.getNombreContratante().isEmpty()) {
				
				queryString.append(" AND bitemporalCotizacion.value.nombreContratante LIKE '%" + filtro.getNombreContratante() + "%'");	
				
			}
			
			//Filtro Numero Poliza Seycos
			if(filtro.getNumeroPolizaSeycos()!= null)
			{
				queryString.append(" AND poliza.numeroPolizaSeycos = :numeroPolizaSeycos");
				
				Utilerias.agregaHashLista(listaParametrosValidos, "numeroPolizaSeycos", filtro.getNumeroPolizaSeycos());				
			}
			
			//Filtro id Agente
			if(filtro.getCodigoAgente() != null)
			{
				queryString.append(" AND bitemporalCotizacion.value.solicitud.codigoAgente = :codigoAgente");
				
				Utilerias.agregaHashLista(listaParametrosValidos, "codigoAgente", filtro.getCodigoAgente());				
			}	
			
			//Filtro bloqueo
			if(filtro.getBloqueoEmision() != null){
				queryString.append(" AND (emisionPendiente.bloqueoEmision IS NULL OR emisionPendiente.bloqueoEmision = :bloqueoEmision) ");				
				Utilerias.agregaHashLista(listaParametrosValidos, "bloqueoEmision", filtro.getBloqueoEmision());
			}
			
			
		} else {
			queryString.append(" WHERE emisionPendiente.fechaValidacion <= :fechaValidacion ");
			queryString.append(" AND emisionPendiente.tipoEmision = :tipoEmision ");
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoEmision", filtro.getTipoEmision());
		
		}
		
		queryString.append(" AND emisionPendiente.estatusRegistro in (:estatusRegistroSinProcesar, :estatusRegistroFallado) ");
		queryString.append(" AND emisionPendiente.migrada = 0 ");
		
		Utilerias.agregaHashLista(listaParametrosValidos, "fechaValidacion", filtro.getFechaValidacion());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistroSinProcesar", EstatusRegistro.SIN_PROCESAR.getEstatusRegistro());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRegistroFallado", EstatusRegistro.EMISION_FALLO.getEstatusRegistro());		

		queryString.append(" AND emisionPendiente.migrada = 0");

		query = entityManager.createQuery(queryString.toString());
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		//query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
		
	}
	
	@Override
	public List<Object[]> monitorearEmisionPendiente(EmisionPendiente filtro) {
		
        Query query = null;
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		StringBuilder queryString = new StringBuilder("");		
		queryString.append(" SELECT pendiente.estatusRegistro, count(pendiente.estatusRegistro) FROM");
		queryString.append(" EmisionPendiente pendiente WHERE EXISTS ( SELECT DISTINCT emisionPendiente FROM");
		queryString.append(" EmisionPendiente emisionPendiente ,PolizaDTO poliza, BitemporalCotizacion bitemporalCotizacion");
		queryString.append(" WHERE FUNC('trunc', emisionPendiente.fechaValidacion) <= :fechaValidacion");
		queryString.append(" AND emisionPendiente.id = (SELECT MAX(emisionPendienteInt.id) FROM  EmisionPendiente emisionPendienteInt ");
		queryString.append(" WHERE emisionPendienteInt.id  = emisionPendiente.id)");
		queryString.append(" AND poliza.idToPoliza = emisionPendiente.idToPoliza");
		queryString.append(" AND emisionPendiente.migrada = 0 AND emisionPendiente.id = pendiente.id ");		
		queryString.append(" AND poliza.cotizacionDTO.idToCotizacion = bitemporalCotizacion.continuity.numero");
		queryString.append(" AND (:validOn >= bitemporalCotizacion.validityInterval.from and :validOn < bitemporalCotizacion.validityInterval.to)");
		queryString.append(" AND (:knownOn >= bitemporalCotizacion.recordInterval.from and :knownOn < bitemporalCotizacion.recordInterval.to)");		
		queryString.append(" AND bitemporalCotizacion.recordStatus = :notInProcess");
		
		Utilerias.agregaHashLista(listaParametrosValidos, "fechaValidacion", filtro.getFechaValidacion());
		Utilerias.agregaHashLista(listaParametrosValidos, "validOn", filtro.getFechaValidacion());
		Utilerias.agregaHashLista(listaParametrosValidos, "knownOn", TimeUtils.current().toDate());
		Utilerias.agregaHashLista(listaParametrosValidos, "notInProcess", RecordStatus.NOT_IN_PROCESS);
		
		//Filtro Tipo Emision
		if(filtro.getTipoEmision() == null)
		{
			queryString.append(" AND emisionPendiente.tipoEmision IN (:tipoEmisionCancPolAutos, :tipoEmisionCancEndAutos)");
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoEmisionCancPolAutos", TipoEmision.CANC_POL_AUTOS.getTipoEmision());
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoEmisionCancEndAutos", TipoEmision.CANC_END_AUTOS.getTipoEmision());				
		}
		else
		{
			queryString.append(" AND emisionPendiente.tipoEmision = :tipoEmision ");
			Utilerias.agregaHashLista(listaParametrosValidos, "tipoEmision", filtro.getTipoEmision());				
		}
		
		queryString.append(") GROUP BY pendiente.estatusRegistro");	
		
        query = entityManager.createQuery(queryString.toString());
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();		
	}
	
	@PersistenceContext
	private EntityManager entityManager;

}
