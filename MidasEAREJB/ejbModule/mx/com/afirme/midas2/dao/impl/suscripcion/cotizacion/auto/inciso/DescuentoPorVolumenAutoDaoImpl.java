package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.inciso;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.DescuentoPorVolumenAutoDao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.DescuentoPorVolumenAuto;

@Stateless
public class DescuentoPorVolumenAutoDaoImpl extends JpaDao<Long, DescuentoPorVolumenAuto> implements DescuentoPorVolumenAutoDao  {

	@Override
	public DescuentoPorVolumenAuto findByTotalIncisoDentroDeMinMaxTotalSubsections(int totalIncisos) {
		final String jpql = "select m from DescuentoPorVolumenAuto m where "
				+ ":totalIncisos >= m.minTotalSubsections and :totalIncisos <= m.maxTotalSubsections";
		TypedQuery<DescuentoPorVolumenAuto> query = entityManager.createQuery(jpql, DescuentoPorVolumenAuto.class);
		query.setParameter("totalIncisos", totalIncisos);
		List<DescuentoPorVolumenAuto> resultList = query.getResultList();
		//Se checa que sea mayor a 1 porque resulta que checando en base de datos encontre rangos que se traslapaban sin embargo
		//esto no es correcto, pero para evitar que cause problema con algo ya desarrollado se retorna el primer elemento en caso
		//de que haya traslapes.
		if (resultList.size() > 1){
			return resultList.get(0);
		}
		return null;
	}

}
