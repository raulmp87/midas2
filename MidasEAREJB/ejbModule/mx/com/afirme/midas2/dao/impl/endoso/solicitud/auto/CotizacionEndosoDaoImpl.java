package mx.com.afirme.midas2.dao.impl.endoso.solicitud.auto;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.CotizacionEndosoDao;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;

@Stateless
public class CotizacionEndosoDaoImpl implements CotizacionEndosoDao{
	
	@PersistenceContext
	private EntityManager entityManager;	
	
	/**
	 * Listado de Cotizaciones de Endoso
	 * Desde la Bandeja de Cotizaciones: Autos/Cotizaciones/Endosos
	 * @param cotizacionEndosoDTO objeto con filtros
	 */
	@Override
	@SuppressWarnings({ "unchecked"})
	public List<Object[]> buscarCotizacionFiltrado(CotizacionEndosoDTO cotizacionEndosoDTO) {		
		Query query = getQueryCotizaciones(cotizacionEndosoDTO, false);
		
		if(cotizacionEndosoDTO.getPrimerRegistroACargar() != null && cotizacionEndosoDTO.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(cotizacionEndosoDTO.getPrimerRegistroACargar().intValue());
			query.setMaxResults(cotizacionEndosoDTO.getNumeroMaximoRegistrosACargar().intValue());
		}
				
		return query.getResultList();
	}

	/**
	 * Total de Cotizaciones de Endoso
	 * Desde la Bandeja de Cotizaciones: Autos/Cotizaciones/Endosos
	 * @param cotizacionEndosoDTO objeto con filtros
	 */
	@Override
	public Long obtenerTotalPaginacionCotizaciones(CotizacionEndosoDTO cotizacionEndosoDTO) {
		
		Query query = getQueryCotizaciones(cotizacionEndosoDTO, true);
		
		return (Long)query.getSingleResult();
	}
	
	/**
	 * Query de Cotizaciones, con datos de bitemporalidad, 
	 * utilizado desde la bandeja de cotizaciones
	 * @param filtro
	 * @param esConteo
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	private Query getQueryCotizaciones(CotizacionEndosoDTO filtro, Boolean esConteo) {
		
		Query query = null;
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String subject = "control, cotizacionb"; 
			
		if (esConteo) {
			subject = "COUNT(DISTINCT control)";
		}
		
		String queryString = "SELECT " + subject + " FROM BitemporalCotizacion cotizacionb, ControlEndosoCot control, " +
				"PolizaDTO poliza";
		queryString += " JOIN cotizacionb.continuity cotizacionc ";
		queryString += " JOIN cotizacionc.incisoContinuities incisoc ";
		queryString += " JOIN incisoc.autoIncisoContinuity autoincisoc ";
		queryString += " JOIN autoincisoc.autoIncisos autoincisob ";
		String sWhere = "" +
					
			" cotizacionb.continuity.numero = control.cotizacionId" +
			
			" AND cotizacionb.continuity.numero = poliza.cotizacionDTO.idToCotizacion" +
			" AND (" +
			" 	(" +
			
			"		(control.validFrom >= autoincisob.validityInterval.from AND control.validFrom < autoincisob.validityInterval.to)" +
			"		AND ((autoincisob.recordStatus = :toBeAdded) " +
			"			OR ((autoincisob.recordStatus = :toBeEnded) AND (control.solicitud.claveTipoEndoso IN (:tipoEndosoCancelacionPoliza, :tipoEndosoBajaInciso)))" +
			"			OR ((autoincisob.recordStatus = :notInProcess) AND (control.solicitud.claveTipoEndoso IN (:tipoEndosoCFP, :tipoEndosoCambioDatos," +
			" :tipoCambioAgente, :tipoCancelEndoso, :tipoInclusionAnexo, :tipoInclusionTexto, :tipoRehabEndosoCancelEndoso, :tipoEndosoMovimiento, :tipoExclusionTexto)))" +
			"			OR ((autoincisob.recordStatus = :notInProcess) AND (autoincisob.recordInterval.to = :endOfTimes) AND (control.solicitud.claveTipoEndoso IN (:tipoEndosoAjustePrima, :tipoCancelEndoso, :tipoRehabEndosoCancelEndoso)))" +
			"		)" +
			" 		AND control.claveEstatusCot IN( " + CotizacionDTO.ESTATUS_COT_EN_PROCESO +","+ CotizacionDTO.ESTATUS_COT_TERMINADA + ")" +
			
			" 	)" +
			" 	OR (" +
			
			"		(control.validFrom >= autoincisob.validityInterval.from AND control.validFrom < autoincisob.validityInterval.to) " +
			"		AND " +
			
			" 		((control.recordFrom = autoincisob.recordInterval.to" +
			"				AND control.solicitud.claveTipoEndoso IN (:tipoEndosoCancelacionPoliza, :tipoEndosoBajaInciso))" +
			
			"			 OR (control.recordFrom >= autoincisob.recordInterval.from AND control.recordFrom < autoincisob.recordInterval.to)" +
			
			
			"		 )" +
			" 	)" +
			" )" +
			" AND (" +
			" 	(" +
			
			"		(control.validFrom >= cotizacionb.validityInterval.from AND control.validFrom < cotizacionb.validityInterval.to)" +
			"		AND ((cotizacionb.recordStatus = :toBeAdded) " +
			"			OR ((cotizacionb.recordStatus = :toBeEnded) AND (control.solicitud.claveTipoEndoso = :tipoEndosoCancelacionPoliza))" +
			"			OR ((cotizacionb.recordStatus = :notInProcess) AND (cotizacionb.recordInterval.to = :endOfTimes) AND (control.solicitud.claveTipoEndoso IN (:tipoEndosoAjustePrima, :tipoCancelEndoso, :tipoRehabEndosoCancelEndoso, :tipoEndosoRehabInciso)) )" +
			"		)" +
			" 		AND control.claveEstatusCot IN( " + CotizacionDTO.ESTATUS_COT_EN_PROCESO +","+ CotizacionDTO.ESTATUS_COT_TERMINADA + ")" +
			
			" 	)" +
			" 	OR (" +
			"		(control.validFrom >= cotizacionb.validityInterval.from AND control.validFrom < cotizacionb.validityInterval.to) " +
			"		AND" +
			" 		((control.recordFrom = cotizacionb.recordInterval.to" +
			"				AND control.solicitud.claveTipoEndoso = :tipoEndosoCancelacionPoliza)" +
			
			"			 OR (control.recordFrom >= cotizacionb.recordInterval.from AND control.recordFrom < cotizacionb.recordInterval.to)" +
			
			"		)" +
			" 	)" +
			" )";
		
		Utilerias.agregaHashLista(listaParametrosValidos, "toBeAdded", RecordStatus.TO_BE_ADDED);
		Utilerias.agregaHashLista(listaParametrosValidos, "toBeEnded", RecordStatus.TO_BE_ENDED);
		Utilerias.agregaHashLista(listaParametrosValidos, "notInProcess", RecordStatus.NOT_IN_PROCESS);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoEndosoCancelacionPoliza", SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoEndosoBajaInciso", SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoEndosoCFP", SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoEndosoCambioDatos", SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoCambioAgente", SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoCancelEndoso", SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoInclusionAnexo", SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoInclusionTexto", SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoRehabEndosoCancelEndoso", SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoEndosoMovimiento", SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoExclusionTexto", SolicitudDTO.CVE_TIPO_ENDOSO_EXCLUSION_TEXTO);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoEndosoAjustePrima", SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA);
		Utilerias.agregaHashLista(listaParametrosValidos, "tipoEndosoRehabInciso", SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS);
		
		GregorianCalendar gcFechaFinTiempos = new GregorianCalendar();
		gcFechaFinTiempos.set(GregorianCalendar.DAY_OF_MONTH, 1);
		gcFechaFinTiempos.set(GregorianCalendar.MONTH, 0);
		gcFechaFinTiempos.set(GregorianCalendar.YEAR, 4000);
		gcFechaFinTiempos.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gcFechaFinTiempos.set(GregorianCalendar.MINUTE, 0);
		gcFechaFinTiempos.set(GregorianCalendar.SECOND, 0);
		gcFechaFinTiempos.set(GregorianCalendar.MILLISECOND, 0);
		
		Utilerias.agregaHashLista(listaParametrosValidos, "endOfTimes", new Timestamp(gcFechaFinTiempos.getTime().getTime()));
		
		
		
		if (filtro == null)	{
			return null;				
		}
				
		/*Seccion de Filtros*/
		
		//Filtro Numero Poliza
		if(filtro.getNumeroPoliza() != null) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " poliza.numeroPoliza = :numeroPoliza";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroPoliza", filtro.getNumeroPoliza());
		}
		
		//Filtro Numero Cotizacion
		if(filtro.getNumeroCotizacionEndoso() != null) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " cotizacionb.continuity.numero = :numeroCotizacion";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroCotizacion", filtro.getNumeroCotizacionEndoso());
		}
		
		if (filtro.getSolicitudId() != null) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " control.solicitud.idToSolicitud = :solicitudId";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "solicitudId", filtro.getSolicitudId());
		}
		
		
		//Filtro Numero Serie Vehiculo
		if(filtro.getNumeroSerieVehiculo() != null && !filtro.getNumeroSerieVehiculo().isEmpty()) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " autoincisob.value.numeroSerie = :numeroSerie";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroSerie", filtro.getNumeroSerieVehiculo().trim());
		}
		
		//Filtro Estatus
		if((filtro.getBusquedaEstatusEmitidas() != null && filtro.getBusquedaEstatusEmitidas()) 
				|| (filtro.getBusquedaEstatusEnProceso() != null && filtro.getBusquedaEstatusEnProceso()))	{
			if(sWhere.equals("")){
				sWhere += "";
			}else{
				sWhere += " AND ";
			}
			sWhere += " control.claveEstatusCot IN( "; 
			
			if (filtro.getBusquedaEstatusEmitidas() != null && filtro.getBusquedaEstatusEmitidas()) {
				sWhere += CotizacionDTO.ESTATUS_COT_EMITIDA;
				if (filtro.getBusquedaEstatusEnProceso() != null && filtro.getBusquedaEstatusEnProceso()) {
					sWhere +=",";
				}
			}
			
			if (filtro.getBusquedaEstatusEnProceso() != null && filtro.getBusquedaEstatusEnProceso()) {
				sWhere += CotizacionDTO.ESTATUS_COT_EN_PROCESO + "," + CotizacionDTO.ESTATUS_COT_TERMINADA;
			}
			
			sWhere += ")";
		}
		
		
		//Filtro Tipo Endoso
		if(filtro.getIdTipoEndoso() != 0) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " control.solicitud.claveTipoEndoso = :claveTipoEndoso";
			
			Utilerias.agregaHashLista(listaParametrosValidos, "claveTipoEndoso", filtro.getIdTipoEndoso());
		}
		
		
		//Filtro Nombre del Asegurado
		if(filtro.getNombreAsegurado() != null && !filtro.getNombreAsegurado().isEmpty()) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " autoincisob.value.nombreAsegurado LIKE '%" + filtro.getNombreAsegurado().trim() + "%'";		
		}
		
		
		//Filtro Conflicto Numero de Serie
		if(filtro.getConflictoNumeroSerie() != null && filtro.getConflictoNumeroSerie()) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " cotizacionb.value.conflictoNumeroSerie = 1";
		}
		
		//Filtro Fecha Cotizacion Desde
		if(filtro.getFechaCotizacionDesde() != null) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " control.validFrom >= :desde";
			
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(filtro.getFechaCotizacionDesde());
			gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
			gcFechaInicio.set(GregorianCalendar.SECOND, 0);
			gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			
			Utilerias.agregaHashLista(listaParametrosValidos, "desde", new Timestamp(gcFechaInicio.getTime().getTime()));
		}
		
		//Filtro Fecha Cotizacion Hasta
		if(filtro.getFechaCotizacionHasta() != null) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " control.validFrom < :hasta";
			
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(filtro.getFechaCotizacionHasta());
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			
			Utilerias.agregaHashLista(listaParametrosValidos, "hasta", new Timestamp(gcFechaFin.getTime().getTime()));
		}
		
		/*Fin de Seccion de Filtros*/
		
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			queryString = queryString.concat(" where ").concat(sWhere);				
		}
		
		//grouping
		if (!esConteo) {
			queryString = queryString.concat(" GROUP BY ").concat(subject);
		}
		
		
		query = entityManager.createQuery(queryString);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
		
	}
	
	/**
	 * Listado de Cotizaciones de Endoso de una poliza
	 * Desde Listado de Cotizaciones de Poliza: Autos/Emision/Consulta Por Poliza
	 * @param cotizacionEndosoDTO objeto con filtro Poliza y Cotizacion
	 */
	@SuppressWarnings("unchecked")
	public List<ControlEndosoCot> buscarCotizacionesEndosoPoliza(CotizacionEndosoDTO cotizacionEndosoDTO){
		Query query = getQueryCotizacionesPorPoliza(cotizacionEndosoDTO, false);
		
		if(cotizacionEndosoDTO.getPrimerRegistroACargar() != null && cotizacionEndosoDTO.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(cotizacionEndosoDTO.getPrimerRegistroACargar().intValue());
			query.setMaxResults(cotizacionEndosoDTO.getNumeroMaximoRegistrosACargar().intValue());
		}
				
		return query.getResultList();
	}
	
	/**
	 * Total de Cotizaciones de Endoso de una poliza
	 * Desde Listado de Cotizaciones de Poliza: Autos/Emision/Consulta Por Poliza
	 * @param cotizacionEndosoDTO objeto con filtro Poliza y Cotizacion
	 */
	public Long obtenerTotalPaginacionCotizacionesEndosoPoliza(CotizacionEndosoDTO cotizacionEndosoDTO){
		Query query = getQueryCotizacionesPorPoliza(cotizacionEndosoDTO, true);		
		return (Long)query.getSingleResult();
	}
	
	/**
	 * Query para obtener las cotizaciones por poliza
	 * @param filtro
	 * @param esConteo
	 * @return
	 */
	private Query getQueryCotizacionesPorPoliza(CotizacionEndosoDTO filtro, Boolean esConteo) {
		Query query = null;
		
		String subject = "control"; 
			
		if (esConteo) {
			subject = "COUNT(DISTINCT control)";
		}
		
		String queryString = "SELECT " + subject + " FROM ControlEndosoCot control " +
				" WHERE control.cotizacionId = :numeroCotizacion " +
				" AND control.claveEstatusCot IN( :cot_en_proceso, :cot_terminada, :cot_emitida ) " +
				" ORDER BY control.id DESC";
		
		query = entityManager.createQuery(queryString);
		query.setParameter("numeroCotizacion", filtro.getNumeroCotizacionEndoso());
		query.setParameter("cot_en_proceso", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		query.setParameter("cot_terminada", CotizacionDTO.ESTATUS_COT_TERMINADA );
		query.setParameter("cot_emitida", CotizacionDTO.ESTATUS_COT_EMITIDA );
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<Object[]> getEndososParaCancelacion(Long cotizacionId) {
		List<Object[]> list = new ArrayList<Object[]>();
		
		Query query = null;
	
		StringBuilder queryString = new StringBuilder("SELECT endoso, controlEndoso FROM EndosoDTO endoso, ControlEndosoCot controlEndoso");

		queryString.append(" WHERE controlEndoso.cotizacionId = :cotizacionId")
			
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :altaInciso")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :cancelacionPoliza")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :cancelacionInciso")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :cancelacionEndoso")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :rehabilitacionPoliza")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :rehabilitacionInciso")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :rehabilitacionEndoso")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :cambioFormaPago")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :inclusionCondiciones")
			.append(" AND controlEndoso.solicitud.claveTipoEndoso <> :bajaCondiciones")
			.append(" AND controlEndoso.claveEstatusCot = :estatusEmitida")
			.append(" AND controlEndoso.id NOT IN (SELECT DISTINCT controlEndosoCE.idCancela")
			.append(" FROM ControlEndosoCot controlEndosoCE")
			.append(" WHERE controlEndoso.cotizacionId = controlEndosoCE.cotizacionId")
			.append(" AND controlEndosoCE.idCancela IS NOT NULL AND controlEndosoCE.recordFrom IS NOT NULL AND controlEndosoCE.id NOT IN")
			.append(" (SELECT DISTINCT controlEndosoRE.idCancela FROM ControlEndosoCot controlEndosoRE")
			.append(" WHERE controlEndosoCE.cotizacionId = controlEndosoRE.cotizacionId AND controlEndosoRE.idCancela IS NOT NULL")
			.append(" AND controlEndosoCE.idCancela IS NOT NULL AND controlEndosoCE.id = controlEndosoRE.idCancela))")
			.append(" AND controlEndoso.idCancela IS NULL")
			.append(" AND endoso.idToCotizacion = controlEndoso.cotizacionId AND endoso.recordFrom = controlEndoso.recordFrom ")
			.append(" AND endoso.id.numeroEndoso <> 0 ")
			.append(" ORDER BY endoso.id.numeroEndoso ");
		
		query = entityManager.createQuery(queryString.toString());
		
		query.setParameter("cotizacionId", cotizacionId);
		query.setParameter("estatusEmitida", CotizacionDTO.ESTATUS_COT_EMITIDA);
		query.setParameter("altaInciso", SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO);
		query.setParameter("cancelacionPoliza", SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA);
		query.setParameter("cancelacionInciso", SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO);
		query.setParameter("cancelacionEndoso", SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO);
		query.setParameter("rehabilitacionPoliza", SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA);
		query.setParameter("rehabilitacionInciso", SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS);
		query.setParameter("rehabilitacionEndoso", SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO);
		query.setParameter("cambioFormaPago", SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO);
		query.setParameter("inclusionCondiciones", SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES);
		query.setParameter("bajaCondiciones", SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		list = query.getResultList();	
		
		return list;		
	}	

	@Override
	@SuppressWarnings("unchecked")
	public List<Object[]> getEndososParaRehabilitacion(Long cotizacionId) {
		
		String queryString = "SELECT endoso, controlEndoso FROM EndosoDTO endoso, ControlEndosoCot controlEndoso, "+
			" ControlEndosoCot controlEndosoCE" +
			" WHERE controlEndosoCE.cotizacionId = :cotizacionId " +
			" AND controlEndosoCE.solicitud.claveTipoEndoso = :cancelacionEndoso" +
			" AND controlEndosoCE.claveEstatusCot = :estatusEmitida" +
			" AND (" +
			"  		SELECT COUNT (controlEndosoRE.idCancela) FROM ControlEndosoCot controlEndosoRE" +
			"		WHERE controlEndosoRE.cotizacionId = :cotizacionId" +
			"		AND controlEndosoRE.idCancela = controlEndosoCE.id" +
			"		AND controlEndosoRE.claveEstatusCot = :estatusEmitida" +
			"	  ) = 0" +
			" AND controlEndoso.id = controlEndosoCE.idCancela" +
			" AND endoso.idToCotizacion = controlEndoso.cotizacionId AND endoso.recordFrom = controlEndoso.recordFrom" +
			" AND endoso.id.numeroEndoso <> 0 " +
			" ORDER BY endoso.id.numeroEndoso";
		
		Query query = entityManager.createQuery(queryString);
		
		query.setParameter("cotizacionId", cotizacionId);
		query.setParameter("cancelacionEndoso", SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO);
		query.setParameter("estatusEmitida", CotizacionDTO.ESTATUS_COT_EMITIDA);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	@Override
	public ControlEndosoCot getControlEndosoCE(Long controlEndosoCanceladoId) {
				
		String queryString = "SELECT controlEndosoCE FROM ControlEndosoCot controlEndosoCE" +
			" WHERE controlEndosoCE.idCancela = :controlEndosoCanceladoId " +
			" AND controlEndosoCE.claveEstatusCot = :estatusEmitida" +
			" AND (" +
			"  		SELECT COUNT (controlEndosoRE.idCancela) FROM ControlEndosoCot controlEndosoRE" +
			"		WHERE controlEndosoRE.idCancela = controlEndosoCE.id" +
			"		AND controlEndosoRE.claveEstatusCot = :estatusEmitida" +
			"	  ) = 0";
		
		Query query = entityManager.createQuery(queryString);
		
		query.setParameter("controlEndosoCanceladoId", controlEndosoCanceladoId);
		query.setParameter("estatusEmitida", CotizacionDTO.ESTATUS_COT_EMITIDA);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return (ControlEndosoCot) query.getSingleResult();
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getSumarizadoMovimientosCoberturaEndososAFuturo(ControlEndosoCot controlEndosoCot, String strIncisosBaja, boolean groupInciso) {
		//Se obtiene un listado de MovimientoEndoso con todos los inversos de los valores de los conceptos multiplicados por sus respectivos 
		//factores de igualacion de todos los movimientos de cobertura de los incisos a dar de baja en todos los controlEndosoCot de la poliza 
		//con fecha de validez mayor a la fecha de inicio de vigencia del endoso
		//que aun no hayan sido cancelados.  Se obtienen los datos de TOCOBERTURAEND.
		
		Query query = null;
		int i = 1;
		StringBuilder queryString = new StringBuilder("");
		queryString.append("")
			
		.append(" SELECT");
		
		if (groupInciso) {
			queryString.append(" COBEND.NUMEROINCISO,");
		} else {
			queryString.append(" COBEND.IDTOCOBERTURA,");
		}
		
		
		queryString.append(" SUM (COBEND.VALORPRIMANETA) * -1 AS VALORPRIMANETA,")
		.append(" SUM (COBEND.VALORBONIFCOMISION) * -1 AS VALORBONIFCOMISION,")
		.append(" SUM (COBEND.VALORBONIFCOMRECPAGOFRAC) * -1 AS VALORBONIFCOMRECPAGOFRAC,")
		.append(" SUM (COBEND.VALORRECARGOPAGOFRAC) * -1 AS VALORRECARGOPAGOFRAC,")
		.append(" SUM (COBEND.VALORCOMISION) * -1 AS VALORCOMISION,")
		.append(" SUM (COBEND.VALORCOMISIONRECPAGOFRAC) * -1 AS VALORCOMISIONRECPAGOFRAC,")
		.append(" SUM (COBEND.VALORDERECHOS) * -1 AS VALORDERECHOS,")
		.append(" SUM (COBEND.VALORIVA) * -1 AS VALORIVA,")
		.append(" MAX (COBEND.NUMEROINCISO) AS NUMEROINCISO,")
		.append(" SUM (COBEND.VALORSOBRECOMISIONAGENTE) * -1 AS VALORSOBRECOMISIONAGENTE,")
		.append(" SUM (COBEND.VALORSOBRECOMISIONPROM) * -1 AS VALORSOBRECOMISIONPROM,")
		.append(" SUM (COBEND.VALORBONOAGENTE) * -1 AS VALORBONOAGENTE,")
		.append(" SUM (COBEND.VALORBONOPROM) * -1 AS VALORBONOPROM,")
		.append(" SUM (COBEND.VALORCESIONDERECHOSAGENTE) * -1 AS VALORCESIONDERECHOSAGENTE,")
		.append(" SUM (COBEND.VALORCESIONDERECHOSPROM) * -1 AS VALORCESIONDERECHOSPROM,")
		.append(" SUM (COBEND.VALORSOBRECOMUDIAGENTE) * -1 AS VALORSOBRECOMUDIAGENTE,")
		.append(" SUM (COBEND.VALORSOBRECOMUDIPROM) * -1 AS VALORSOBRECOMUDIPROM")
		.append(" FROM")
		.append(" MIDAS.TOCOBERTURAEND COBEND")
		.append(" INNER JOIN MIDAS.TOENDOSO ENDOSO")
		.append(" ON ENDOSO.IDTOPOLIZA = COBEND.IDTOPOLIZA") 
		.append(" AND ENDOSO.NUMEROENDOSO = COBEND.NUMEROENDOSO")
		.append(" WHERE")
		.append(" ENDOSO.IDTOCOTIZACION = ?")
		.append(" AND ENDOSO.VALIDFROM > ?");

		
		if (strIncisosBaja != null && !strIncisosBaja.trim().equals("")) {
			queryString.append(" AND COBEND.NUMEROINCISO IN (" + strIncisosBaja +")");
		}
	 
		
		if (groupInciso) {
			queryString.append(" GROUP BY COBEND.NUMEROINCISO");
		} else {
			queryString.append(" GROUP BY COBEND.IDTOCOBERTURA");
		}
		
		query = entityManager.createNativeQuery(queryString.toString());
		
		query.setParameter(i++, controlEndosoCot.getCotizacionId());
		query.setParameter(i++, controlEndosoCot.getValidFrom());	
				
		return query.getResultList();	

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ControlEndosoCot> getControlEndosoCotEndososEAPAnteriores(ControlEndosoCot controlEndosoCot) {
		//Se obtiene un listado de ControlEndosoCot de endosos EAP con fecha de inicio de vigencia menor o igual a la fecha de inicio de vigencia 
		//del endoso de cancelacion que aun no hayan sido cancelados

		Query query = null;
		
		StringBuilder queryString = new StringBuilder("SELECT controlEndoso FROM ControlEndosoCot controlEndoso");
		
		queryString.append(" WHERE controlEndoso.cotizacionId = :cotizacionId")
			
			.append(" AND controlEndoso.solicitud.claveTipoEndoso = :ajustePrima")
			.append(" AND controlEndoso.claveEstatusCot = :estatusEmitida")
			.append(" AND controlEndoso.id NOT IN (SELECT DISTINCT controlEndosoCE.idCancela")
			.append(" FROM ControlEndosoCot controlEndosoCE")
			.append(" WHERE controlEndoso.cotizacionId = controlEndosoCE.cotizacionId")
			.append(" AND controlEndosoCE.idCancela IS NOT NULL AND controlEndosoCE.recordFrom IS NOT NULL AND controlEndosoCE.id NOT IN")
			.append(" (SELECT DISTINCT controlEndosoRE.idCancela FROM ControlEndosoCot controlEndosoRE")
			.append(" WHERE controlEndosoCE.cotizacionId = controlEndosoRE.cotizacionId AND controlEndosoRE.idCancela IS NOT NULL")
			.append(" AND controlEndosoCE.idCancela IS NOT NULL AND controlEndosoCE.id = controlEndosoRE.idCancela))")
			.append(" AND controlEndoso.idCancela IS NULL")
			.append(" AND controlEndoso.validFrom <= :fechaInicioVigencia")
			.append(" ORDER BY controlEndoso.id ");
		
		query = entityManager.createQuery(queryString.toString());
		
		query.setParameter("cotizacionId", controlEndosoCot.getCotizacionId());
		query.setParameter("ajustePrima", SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA);
		query.setParameter("estatusEmitida", CotizacionDTO.ESTATUS_COT_EMITIDA);
		query.setParameter("fechaInicioVigencia", controlEndosoCot.getValidFrom());
		
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();	
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Object[]> getEndososParaAjustar(Long cotizacionId) {
		
		Query query = null;
	
		StringBuilder queryString = new StringBuilder("SELECT endoso, controlEndoso FROM EndosoDTO endoso, ControlEndosoCot controlEndoso");

		queryString.append(" WHERE controlEndoso.cotizacionId = :cotizacionId")
			
			.append(" AND controlEndoso.solicitud.claveTipoEndoso IN (:altaInciso, :movimientos, :ajustePrima)")
			.append(" AND controlEndoso.claveEstatusCot = :estatusEmitida")
			.append(" AND controlEndoso.id NOT IN (SELECT DISTINCT controlEndosoCE.idCancela")
			.append(" FROM ControlEndosoCot controlEndosoCE")
			.append(" WHERE controlEndoso.cotizacionId = controlEndosoCE.cotizacionId")
			.append(" AND controlEndosoCE.idCancela IS NOT NULL AND controlEndosoCE.recordFrom IS NOT NULL AND controlEndosoCE.id NOT IN")
			.append(" (SELECT DISTINCT controlEndosoRE.idCancela FROM ControlEndosoCot controlEndosoRE")
			.append(" WHERE controlEndosoCE.cotizacionId = controlEndosoRE.cotizacionId AND controlEndosoRE.idCancela IS NOT NULL")
			.append(" AND controlEndosoCE.idCancela IS NOT NULL AND controlEndosoCE.id = controlEndosoRE.idCancela))")
			.append(" AND controlEndoso.idCancela IS NULL") 
			.append(" AND endoso.idToCotizacion = controlEndoso.cotizacionId AND endoso.recordFrom = controlEndoso.recordFrom ")
			.append(" AND endoso.id.numeroEndoso <> 0 ")
			.append(" ORDER BY endoso.id.numeroEndoso ");
		
		query = entityManager.createQuery(queryString.toString());
		
		query.setParameter("cotizacionId", cotizacionId);
		query.setParameter("estatusEmitida", CotizacionDTO.ESTATUS_COT_EMITIDA);
		query.setParameter("altaInciso", SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO);
		query.setParameter("movimientos", SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS);
		query.setParameter("ajustePrima", SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();	
				
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ControlEndosoCot> obtenerEndosoInciso(Long idToCotizacion, Long idIncisoContinuity, Date fechaBusquedaInicial,Date fechaBusquedaFinal, List<String> usuarios) {
		
		Query query      = null;
		Boolean sonNulos = true;
		StringBuilder queryString = new StringBuilder();
		
		if( fechaBusquedaInicial!=null || fechaBusquedaFinal!=null || ( usuarios!=null && !usuarios.isEmpty())  ){
			sonNulos = false;
		}
		
		
		queryString.append(" SELECT controlEndoso ");
		queryString.append(" FROM   MovimientoEndoso movimientoEndoso ");
		queryString.append(" JOIN   movimientoEndoso.controlEndosoCot controlEndoso ");
		queryString.append(" JOIN   controlEndoso.solicitud solicitud ");
		
		queryString.append(" WHERE  controlEndoso.cotizacionId = :idToCotizacion ");
		queryString.append(" AND    movimientoEndoso.tipo = :tipo ");
		queryString.append(" AND    movimientoEndoso.bitemporalInciso in  (  ");
		queryString.append(" 							select bitemporalInciso from BitemporalInciso bitemporalInciso where bitemporalInciso.continuity.id = :idIncisoContinuity");
		queryString.append(" ) AND    controlEndoso.claveEstatusCot = :claveEstatusCot ");
		
		if( !sonNulos & ( usuarios!=null && !usuarios.isEmpty() ) ){
			queryString.append(" AND trim(solicitud.codigoUsuarioCreacion) IN :listaUsuarios " );
		}
		
		if ( !sonNulos ){
			
			if( fechaBusquedaInicial != null & fechaBusquedaFinal != null ){
				queryString.append(" AND (solicitud.fechaInicioVigenciaEndoso BETWEEN  :fechaInicial AND :fechaFinal)    " );
			}
			
			if ( fechaBusquedaInicial != null & fechaBusquedaFinal == null ){
				queryString.append(" AND ( solicitud.fechaInicioVigenciaEndoso >= :fechaInicial )    " );
			}else if ( fechaBusquedaInicial == null & fechaBusquedaFinal != null ) {
				queryString.append(" AND ( solicitud.fechaInicioVigenciaEndoso >= :fechaFinal )    " );
			}
		}
		
		query = this.entityManager.createQuery(queryString.toString());
		
		query.setParameter("idToCotizacion"     , idToCotizacion);
		query.setParameter("tipo"               , TipoMovimientoEndoso.INCISO);
		query.setParameter("idIncisoContinuity" , idIncisoContinuity );
		query.setParameter("claveEstatusCot"    , 16 );
		
		if ( !sonNulos ){
			
			if( usuarios!=null && !usuarios.isEmpty() ){
				query.setParameter("listaUsuarios", usuarios  );
			}
			
			if( fechaBusquedaInicial != null & fechaBusquedaFinal != null ){
				query.setParameter("fechaInicial" , fechaBusquedaInicial );
				query.setParameter("fechaFinal"   , fechaBusquedaFinal );
			}
			
			if ( fechaBusquedaInicial != null & fechaBusquedaFinal == null ){
				query.setParameter("fechaInicial" , fechaBusquedaInicial );
			}else if ( fechaBusquedaInicial == null & fechaBusquedaFinal != null ) {
				query.setParameter("fechaFinal"   , fechaBusquedaFinal );
			}
			
			
		}
		
		List<ControlEndosoCot> lControlEndosoCot = query.getResultList(); 
		
		HashSet<ControlEndosoCot> hs = new HashSet<ControlEndosoCot>();
		hs.addAll(lControlEndosoCot);
		lControlEndosoCot.clear();
		lControlEndosoCot.addAll(hs);
		
		return  lControlEndosoCot;
	}
	
	
	
	
}
