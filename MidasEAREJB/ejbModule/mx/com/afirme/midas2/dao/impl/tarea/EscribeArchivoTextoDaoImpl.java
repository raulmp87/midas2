package mx.com.afirme.midas2.dao.impl.tarea;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.tarea.EscribeArchivoTextoDao;
import mx.com.afirme.midas2.util.TextFileUtils;

@Stateless
public class EscribeArchivoTextoDaoImpl implements EscribeArchivoTextoDao {
	
	private static final String PROCEDURE_OBTENER_INFORMACION_SELCOR = "SEYCOS.PKGSIS_ESCRIBE_ARCHIVO.stp_escribe_selcor";
	private static final String PROCEDURE_OBTENER_INFORMACION_CYBER = "SEYCOS.PKGSIS_ESCRIBE_ARCHIVO.stp_escribe_cyber";
	private static final String PROCEDURE_OBTENER_WINSTON_DATA = "SEYCOS.PKGSIS_UTILIDAD.STPEXPORTAWD";
	private static final String PROCEDURE_ESCRIBE_CAJERO = "SEYCOS.PKG_AUTOMATIZACION_RENOV.STP_ESCRIBE_CAJERO";
	private static final String PROCEDURE_RESULTADO_CANC = "SEYCOS.PKGSIS_CANCELA_AUTOM.stpArchivoResultadoCancelacion";
	private static final String PROCEDURE_ESCRIBE_CLIENTE = "SEYCOS.PKGSIS_PROCESO_IDCLIENTE.STP_ESCRIBE_IDCLIENTE";
	private static final String PROCEDURE_ACTUALIZA_CLIENTE_BANCA = "SEYCOS.PKGSIS_PROCESO_IDCLIENTE.STP_ACTUALIZA_CLIENTE_BANCA";
	private static final String PROCEDURE_STPWINSTON_DATA_AUTOS = "SEYCOS.PKG_WINSTON_DATA.STPWINSTON_DATA_AUTOS";
	private static final String PROCEDURE_STPWINSTON_DATA_AUTOS_DIRCEU = "SEYCOS.PKG_WINSTON_DATA.STPWINSTON_DATA_AUTOS_DIRCEU";
	private static final String PROCEDURE_STPWINSTON_DATA_VIDA = "SEYCOS.PKG_WINSTON_DATA.STPWINSTON_DATA_VIDA";
	private static final String PROCEDURE_STPEXPORTAWD = "SEYCOS.PKGSIS_PRIMASDEP_AUTOM.STPEXPORTAWD";
	
	private static final Logger log = LoggerFactory.getLogger(EscribeArchivoTextoDaoImpl.class);
	private static final String DATOS = "DATOS";

	@Override
	public List<String> obtenerInformacionSelcor(){
		return obtenerInformacionListaString(PROCEDURE_OBTENER_INFORMACION_SELCOR, null);
	}
	
	@Override
	public List<String> obtenerInformacionCYBER(){
		return obtenerInformacionListaString(PROCEDURE_OBTENER_INFORMACION_CYBER, null);
	}
	
	@Override
	public StringBuffer obtenerWinstonData(Date fechaProceso){
		Map<String, Object> mapaValores = new HashMap<String, Object>();
		if(fechaProceso == null)
			fechaProceso = new Date();
		mapaValores.put("pf_Proceso", fechaProceso);
		return obtenerResultadoClob(PROCEDURE_OBTENER_WINSTON_DATA, mapaValores);
	}
	
	@Override
	public List<StringBuffer> obtenerWinstonDataAutos(Long idCotizacion){
		Map<String, Object> mapaValores = new HashMap<String, Object>();
		mapaValores.put("pId_Cotizacion", idCotizacion);
		return obtenerResultadoClobList(PROCEDURE_STPWINSTON_DATA_AUTOS, mapaValores);
	}
	
	@Override
	public StringBuffer obtenerWinstonDataVida(Long idCotizacion){
		Map<String, Object> mapaValores = new HashMap<String, Object>();
		mapaValores.put("pId_Cotizacion", idCotizacion);
		return obtenerResultadoClob(PROCEDURE_STPWINSTON_DATA_VIDA, mapaValores);
	}
	
	@Override
	public List<StringBuffer> obtenerWinstonDataDir(Long idCotizacion){
		Map<String, Object> mapaValores = new HashMap<String, Object>();
		mapaValores.put("pId_Cotizacion", idCotizacion);
		return  obtenerResultadoClobList(PROCEDURE_STPWINSTON_DATA_AUTOS_DIRCEU, mapaValores);
	}
	
	@Override
	public StringBuffer obtenerInformacionCajero(){
		return obtenerResultadoClob(PROCEDURE_ESCRIBE_CAJERO, null);
	}
	
	@Override
	public StringBuffer obtenerPrimasDepositoWinstonData(Date fechaProceso){
		Map<String, Object> mapaValores = new HashMap<String, Object>();
		mapaValores.put("pf_Proceso", fechaProceso);
		return obtenerResultadoClob(PROCEDURE_STPEXPORTAWD, mapaValores);
	}
	
	@Override
	public List<StringBuffer> obtenerResultadoCancelacion(Date fechaProceso){
		Map<String, Object> mapaValores = new HashMap<String, Object>();
		mapaValores.put("pF_Cancelacion", fechaProceso);
		return obtenerResultadoClobList(PROCEDURE_RESULTADO_CANC, mapaValores);
	}
	
	@Override
	public List<String> obtenerInformacionCliente(){
		return obtenerInformacionListaString(PROCEDURE_ESCRIBE_CLIENTE, null);
	}
	
	/**
	 * <p>M&eacute;todo que genera una lista de cadenas cuando el resultado
	 * del proceso es un cursor que s&oacute;lo contiene una columna
	 * y en esta solo contenga texto.</p>
	 *  <p>Los multiples resultados de este cursor se combertir&aacute; en
	 *  una lista de Strings</p>
	 *  
	 * @param nombreProceso <p>Nombre del proceso a ejecutar.</p>
	 * 
	 * @param mapaValores <p>Mapa que contiene los valores de 
	 * entrada para la ejecuci&oacute;n del proceso.</p>
	 * 
	 * @return <p>Lista que contiene la informaci&oacute;n obtenida por la
	 * ejecuci&oacute;n del proceso.</p>
	 */
	@SuppressWarnings("unchecked")
	private List<String> obtenerInformacionListaString(String nombreProceso, Map<String, Object> mapaValores) {
		List<String> info = null;
		try {
			StoredProcedureHelper stpHelper = new StoredProcedureHelper(nombreProceso, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			if(mapaValores != null && !mapaValores.isEmpty()){
    			for(Map.Entry<String, Object> entry : mapaValores.entrySet()){
    				stpHelper.estableceParametro(entry.getKey(), entry.getValue());
    			}
			}
			
			info = (List<String>) stpHelper.obtieneListaResultados();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		return info;
	}

	/**
	 * <p>M&eacute;todo que obtiene el <code>StringBuffer</code> correspondiente del resultado
	 * de un proceso que retorna un objeto tipo <code>oracle.sql.CLOB</code></p>
	 * <p>Se realiza la transformaci&oacute;n a un StringBuffer para la manipulaci&oacute;n 
	 * de esta informaci&oacute;n del lado de la aplicaci&oacute;n.</p>
	 * 
	 * @param nombreProceso Nombre del proceso a ejecutar.
	 * 
	 * @param mapaValores Mapa que contiene los valores 
	 * de los parametros de entrada para este proceso.
	 * 
	 * @return StringBuffer que contiene la informaci&oacute;n contenida por el <code>oracle.sql.CLOB<code>
	 */
	private StringBuffer obtenerResultadoClob(String nombreProceso, Map<String, Object> mapaValores) {
		List<StringBuffer> info = obtenerResultadoClobList(nombreProceso, mapaValores);
		return info != null && !info.isEmpty() ? info.get(0): new StringBuffer();
	}
	
	/**
	 * <p>M&eacute;todo que obtiene una lista de <code>StringBuffer</code> correspondiente
	 * con el resultado de un proceso que retorna como resultado un cursor que contiene
	 * m&uacute;ltiples objetos <code>oracle.sql.CLOB</code></p>
	 * 
	 * @param nombreProceso Nombre del proceso a ejecutar;
	 * 
	 * @param mapaValores Mapa que contiene los valores
	 * de los parametros de entrada para ejecutar el proceso.
	 * 
	 * @return Lista de StringBuffer que contienen la informaci&oacute;n contenida en los
	 * resutados del cursor del procedure.
	 */
	private List<StringBuffer> obtenerResultadoClobList(String nombreProceso, Map<String, Object> mapaValores) {
		List<StringBuffer> content = new ArrayList<StringBuffer>();
		try{
			StoredProcedureHelper stpHelper = new StoredProcedureHelper(nombreProceso, StoredProcedureHelper.DATASOURCE_MIDAS);
			stpHelper.estableceMapeoResultados(TextFileUtils.class.getCanonicalName(), "sqlContent", DATOS);
			
			if(mapaValores != null && !mapaValores.isEmpty()){
    			for(Map.Entry<String, Object> entry : mapaValores.entrySet()){
    				stpHelper.estableceParametro(entry.getKey(), entry.getValue());
    			}
			}
			
			@SuppressWarnings("unchecked")
			List<TextFileUtils> archivosTexto = (List<TextFileUtils>) stpHelper.obtieneListaResultados();
			
			for(TextFileUtils archivo : archivosTexto){
				if(archivo.getSqlContent() != null && archivo.getFileContent() == null){
					content.add(archivo.clobToStringBuffer());
				}
			}
			
		} catch (Exception err){
			throw new RuntimeException(err);
		}
		return content;
	}

	@Override
	public void actualizaInformacionClienteBanca(Long idCliente, Long idCotizacion) {
		try{
			StoredProcedureHelper stpHelper = new StoredProcedureHelper(PROCEDURE_ACTUALIZA_CLIENTE_BANCA, StoredProcedureHelper.DATASOURCE_MIDAS);
			stpHelper.estableceParametro("pIdCotizacion", idCotizacion);
			stpHelper.estableceParametro("pIdCliente", idCliente);
			
			stpHelper.ejecutaActualizar();
		}catch(Exception err){
			log.info("Hubo un error al actualizar el idCliente", err);
			throw new RuntimeException(err);
		}
	}
}
