package mx.com.afirme.midas2.dao.impl.componente.incisos;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.componente.incisos.ListadoIncisosDinamicoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.service.componente.incisos.TipoQueryListadoIncisos;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class ListadoIncisosDinamicoDaoImpl implements ListadoIncisosDinamicoDao{
	
	private static final Logger LOG = Logger
	.getLogger(ListadoIncisosDinamicoDaoImpl.class);
		
	@PersistenceContext
	private EntityManager entityManager;
	
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, 
			RecordStatus recordStatusFilter, boolean getCancelados) {
			return buscarIncisosFiltrado(filtros, cotizacionContinuityId, null, recordStatusFilter, getCancelados);
	}

	@Override
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, 
			Date validoEn, RecordStatus recordStatusFilter, boolean getCancelados) {
		
		return buscarIncisosFiltrado(filtros, cotizacionContinuityId, validoEn, null, recordStatusFilter, getCancelados);
		
	}
	
	public Long obtenerTotalPaginacionIncisos(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, RecordStatus recordStatusFilter, boolean getCancelados) {
		return obtenerTotalPaginacionIncisos(filtros, cotizacionContinuityId, null, recordStatusFilter, getCancelados, null);
	}

	@Override
	public Long obtenerTotalPaginacionIncisos(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, Date validoEn,
												RecordStatus recordStatusFilter, boolean getCancelados, TipoQueryListadoIncisos tipoQuery) {
		
		Query query = getQueryIncisos(filtros, cotizacionContinuityId, validoEn, null, recordStatusFilter, getCancelados, true, false, tipoQuery);
		
		return (Long)query.getSingleResult();
		
	}
	
	@Override
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, 
			Date validoEn, Date recordFrom, RecordStatus recordStatusFilter, boolean getCancelados) {
		return buscarIncisosFiltrado(filtros, cotizacionContinuityId, validoEn, recordFrom, recordStatusFilter, getCancelados, false);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, 
			Date validoEn, Date recordFrom, RecordStatus recordStatusFilter, boolean getCancelados, boolean esConsultaPoliza) {
		
		Query query = getQueryIncisos(filtros, cotizacionContinuityId, validoEn, recordFrom, recordStatusFilter, getCancelados, false, esConsultaPoliza, null);
		
		if (filtros.getPrimerRegistroACargar() != null && filtros.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(filtros.getPrimerRegistroACargar().intValue());
			query.setMaxResults(filtros.getNumeroMaximoRegistrosACargar().intValue());
		}
				
		return query.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<BitemporalInciso> buscarIncisosFiltradoPerdidaTotal(IncisoCotizacionDTO filtros, Long cotizacionContinuityId) {
		
		Query query = getQueryIncisos(filtros, cotizacionContinuityId, null, null, null, 
				true, false, false, TipoQueryListadoIncisos.INCISOS_PERDIDA_TOTAL);
		
		if (filtros.getPrimerRegistroACargar() != null && filtros.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(filtros.getPrimerRegistroACargar().intValue());
			query.setMaxResults(filtros.getNumeroMaximoRegistrosACargar().intValue());
		}
				
		return query.getResultList();
		
	}
	
	@SuppressWarnings("unchecked")
	public List<BitemporalInciso> buscarIncisosFiltradoDesagrupacionRecibos(IncisoCotizacionDTO filtros, Date validoEn, 
			Date recordFrom, Long cotizacionContinuityId) {

		Query query = getQueryIncisos(filtros, cotizacionContinuityId, validoEn, recordFrom, null, 
			true, false, false, TipoQueryListadoIncisos.INCISOS_RECIBOS_AGRUPADOS);
	
		if (filtros.getPrimerRegistroACargar() != null && filtros.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(filtros.getPrimerRegistroACargar().intValue());
			query.setMaxResults(filtros.getNumeroMaximoRegistrosACargar().intValue());
		}
				
		return query.getResultList();
	}
	
	@SuppressWarnings("unchecked")
	public List<NegocioSeccion> getSeccionList(Long cotizacionContinuityId, Date validoEn, RecordStatus recordStatus, boolean getCancelados) {

		Query query = null;
		
		List<NegocioSeccion> list = new ArrayList<NegocioSeccion>(1);
		try{
			
			String queryString = " SELECT DISTINCT NS.IDTONEGSECCION, NS.IDTONEGTIPOPOLIZA, NS.IDTOSECCION " +
					" FROM MIDAS.TONEGSECCION NS, MIDAS.MINCISOC IC, MIDAS.MAUTOINCISOC AC, MIDAS.MAUTOINCISOB AB " +
					" WHERE IC.MCOTIZACIONC_ID = " + cotizacionContinuityId +
					" AND IC.ID = AC.MINCISOC_ID " +
					" AND AC.ID = AB.MAUTOINCISOC_ID " +
					" AND AB.NEGOCIOSECCION_ID = NS.IDTONEGSECCION ";
	
			if(recordStatus != null){
				queryString = queryString + " AND AB.RECORDSTATUS = " + recordStatus.ordinal();
			}
			
			if (validoEn != null) {
				String fechIn;
				fechIn = Utilerias.cadenaDeFecha(validoEn);
				queryString = queryString + " AND AB.VALIDFROM <= to_date('"+fechIn+"','dd/MM/YYYY') AND AB.VALIDTO > to_date('"+fechIn+"','dd/MM/YYYY') ";
			}
		
			query = entityManager.createNativeQuery(queryString, NegocioSeccion.class);
		
			return query.getResultList();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	public Set<String> listarIncisosDescripcion(Long cotizacionContinuityId, Date validoEn, RecordStatus recordStatus, boolean getCancelados) {

		Set<String> listDescripcion =  new HashSet<String>();
		Query query = null;
		
		try{
				
			String queryString = "SELECT DISTINCT AB.DESCRIPCION_FINAL " +
					" FROM MIDAS.MINCISOC IC, MIDAS.MAUTOINCISOC AC, MIDAS.MAUTOINCISOB AB " +
					" WHERE IC.MCOTIZACIONC_ID = " + cotizacionContinuityId +
					" AND IC.ID = AC.MINCISOC_ID " +
					" AND AC.ID = AB.MAUTOINCISOC_ID ";
	
			if(recordStatus != null){
				queryString = queryString + " AND AB.RECORDSTATUS = " + recordStatus.ordinal();
			}
			
			if (validoEn != null) {
				String fechIn;
				fechIn = Utilerias.cadenaDeFecha(validoEn);
				queryString = queryString + " AND AB.VALIDFROM <= to_date('"+fechIn+"','dd/MM/YYYY') AND AB.VALIDTO > to_date('"+fechIn+"','dd/MM/YYYY') ";				
			}
					
		
			query = entityManager.createNativeQuery(queryString);
			
			List<String> lista = query.getResultList();
			Iterator<String> iteratorResultsList = lista.iterator();
			while(iteratorResultsList.hasNext())
			{
				String descripcion = iteratorResultsList.next();
				listDescripcion.add(descripcion);
			}	
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
				
		return listDescripcion;
	}
	
	
	@SuppressWarnings("rawtypes")
	private Query getQueryIncisos(IncisoCotizacionDTO filtros, Long cotizacionContinuityId, Date validoEn, Date recordFrom,
			RecordStatus recordStatusFilter, boolean getCancelados, Boolean esConteo, boolean esConsultaPoliza, TipoQueryListadoIncisos tipoQuery) {
		Query query = null;
		
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		String subject = "incisob"; 
		String sWhere = "";
			
		if (esConteo) {
			subject = "COUNT(incisob)";
		}
		
		String queryString = "SELECT " + subject + " FROM BitemporalInciso incisob ";
		
		if(tipoQuery == TipoQueryListadoIncisos.INCISOS_PERDIDA_TOTAL)
		{
			Map<String,String> queryCompleto = getQueryStringIncisosPerdidaTotal(subject);
			queryString = queryCompleto.get("SELECT");
			sWhere = queryCompleto.get("WHERE");
			
		}else if(tipoQuery == TipoQueryListadoIncisos.INCISOS_RECIBOS_AGRUPADOS)
		{
			Map<String,String> queryCompleto = getQueryStringIncisosRecibosAgrupados(subject);
			queryString = queryCompleto.get("SELECT");
			sWhere = queryCompleto.get("WHERE");
			
			if (validoEn != null) {
				Utilerias.agregaHashLista(listaParametrosValidos, "validOn", validoEn);
			}
			
		}else
		{			
			queryString += " JOIN incisob.continuity incisoContinuity ";
			queryString += " JOIN incisoContinuity.autoIncisoContinuity autoIncisoContinuity ";
			queryString += " JOIN autoIncisoContinuity.autoIncisos autoincisob ";
			
			
			if (esConsultaPoliza) {
				sWhere = " incisob.continuity.cotizacionContinuity.id  = :cotizacionContinuityId" +	
				" AND (:knownOn >= incisob.recordInterval.from and :knownOn < incisob.recordInterval.to)" +
				 " AND (:validOn >= incisob.validityInterval.from and :validOn < incisob.validityInterval.to)" +
				 " AND (:knownOn >= autoincisob.recordInterval.from and :knownOn < autoincisob.recordInterval.to)" +
				 " AND (:validOn >= autoincisob.validityInterval.from and :validOn < autoincisob.validityInterval.to)";
				
			} else if (!getCancelados) {
				sWhere = " incisob.id = (SELECT MAX(incisobInt.id) FROM  BitemporalInciso incisobInt " +
				" WHERE incisobInt.continuity.cotizacionContinuity.id  = :cotizacionContinuityId" +
				" AND incisobInt.continuity.id = incisob.continuity.id" +
				
				" AND (:knownOn >= incisobInt.recordInterval.from and :knownOn < incisobInt.recordInterval.to" +
				" OR incisobInt.recordInterval.from is null)" ;
				if (validoEn != null) {
					sWhere += " AND ((:validOn >= incisobInt.validityInterval.from and :validOn < incisobInt.validityInterval.to and incisobInt.recordStatus <> :toBeAdded)"
						+ " OR (:validOn >= incisobInt.validityInterval.from and :validOn <= incisobInt.validityInterval.to and incisobInt.recordStatus = :toBeAdded))";
				}
				sWhere += ")" +
				" AND autoincisob.id = (SELECT MAX(autoincisobInt.id) FROM  BitemporalAutoInciso autoincisobInt WHERE autoincisobInt.continuity.incisoContinuity.id  = incisob.continuity.id" +
				" AND autoincisobInt.continuity.id = autoincisob.continuity.id" +
				
				" AND (:knownOn >= autoincisobInt.recordInterval.from and :knownOn < autoincisobInt.recordInterval.to" +
				" OR autoincisobInt.recordInterval.from is null)" ;
				if (validoEn != null) {
					sWhere += " AND ((:validOn >= autoincisobInt.validityInterval.from and :validOn < autoincisobInt.validityInterval.to  and autoincisobInt.recordStatus <> :toBeAdded)"
						+ " OR (:validOn >= autoincisobInt.validityInterval.from and :validOn <= autoincisobInt.validityInterval.to  and autoincisobInt.recordStatus = :toBeAdded))";
				}
				sWhere += ")";
				
				if (validoEn != null) {
					sWhere += " AND ((:validOn >= incisob.validityInterval.from and :validOn < incisob.validityInterval.to and incisob.recordStatus <> :toBeAdded)"
						+ " OR (:validOn >= incisob.validityInterval.from and :validOn <= incisob.validityInterval.to and incisob.recordStatus = :toBeAdded))"
						+ " AND ((:validOn >= autoincisob.validityInterval.from and :validOn < autoincisob.validityInterval.to  and autoincisob.recordStatus <> :toBeAdded)"
						+ " OR (:validOn >= autoincisob.validityInterval.from and :validOn <= autoincisob.validityInterval.to  and autoincisob.recordStatus = :toBeAdded))";
				}
			} else {   //Obtener Cancelados
				
				subject = "DISTINCT incisob"; 
				
				if (esConteo) {
					subject = "COUNT(DISTINCT incisob)";
				}
				
				queryString = 
					"SELECT " + subject +
					" FROM BitemporalInciso incisob" +
					", CoberturaEndosoDTO cobEnd" +
					", BitemporalAutoInciso autoincisob " +
					", IncisoContinuity incisoContinuity ";
				
				sWhere = " incisob.continuity.cotizacionContinuity.id = :cotizacionContinuityId " +					
					" AND autoincisob.continuity.incisoContinuity.id = incisob.continuity.id " +
					" AND incisoContinuity.id = incisob.continuity.id " +
					" AND incisob.continuity.numero =  cobEnd.id.numeroInciso " +					
					" AND incisob.continuity.cotizacionContinuity.numero = cobEnd.endosoDTO.idToCotizacion " +
					" AND cobEnd.endosoDTO.recordFrom = incisob.recordInterval.to " +
					" AND cobEnd.endosoDTO.claveTipoEndoso IN (4,9) " +
					" AND cobEnd.endosoDTO.id.numeroEndoso = (SELECT MAX(ce.id.numeroEndoso) FROM CoberturaEndosoDTO ce " +
					" WHERE ce.id.idToPoliza  = cobEnd.id.idToPoliza " +  
					" AND ce.id.numeroInciso = cobEnd.id.numeroInciso ) " +
					" ";
			}	
		
		}
		
		Utilerias.agregaHashLista(listaParametrosValidos, "cotizacionContinuityId", cotizacionContinuityId);
		
		if (esConsultaPoliza) {
			Utilerias.agregaHashLista(listaParametrosValidos, "validOn", validoEn);
			Utilerias.agregaHashLista(listaParametrosValidos, "knownOn", recordFrom);
		} else if (!getCancelados) {
			if (validoEn != null) {
				Utilerias.agregaHashLista(listaParametrosValidos, "validOn", validoEn);
				Utilerias.agregaHashLista(listaParametrosValidos, "toBeAdded", RecordStatus.TO_BE_ADDED);
			}
			
			if (recordFrom == null) {
				recordFrom = TimeUtils.current().toDate();
			}
			Utilerias.agregaHashLista(listaParametrosValidos, "knownOn", recordFrom);
		}			
		
		/*Seccion de Filtros*/
		
		//Filtro estatus
		if (recordStatusFilter != null) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += "incisob.recordStatus = :recordEstatus AND autoincisob.recordStatus = :recordEstatus";
			Utilerias.agregaHashLista(listaParametrosValidos, "recordEstatus", recordStatusFilter);
		}
		
		//Filtro solo cotizados
		if (filtros.getSoloCotizados() != null && filtros.getSoloCotizados()) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += "incisob.recordStatus IN (:recordToBeAdded, :recordToBeEnded) AND autoincisob.recordStatus IN (:recordToBeAdded, :recordToBeEnded) ";
			Utilerias.agregaHashLista(listaParametrosValidos, "recordToBeAdded", RecordStatus.TO_BE_ADDED);
			Utilerias.agregaHashLista(listaParametrosValidos, "recordToBeEnded", RecordStatus.TO_BE_ENDED);
		}
		
		//Filtro Numero Inciso
		if (filtros.getId() != null && filtros.getId().getNumeroInciso() != null) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " incisoContinuity.numero = :numeroInciso";
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroInciso", filtros.getId().getNumeroInciso());
		}
		
		//Filtro Descripcion Vehiculo
		if (!StringUtils.isBlank(filtros.getDescripcionGiroAsegurado()) && !filtros.getDescripcionGiroAsegurado().contains("null"))	{
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " UPPER(autoincisob.value.descripcionFinal) LIKE '%" + filtros.getDescripcionGiroAsegurado() + "%'";
		}
		
		//Filtro Linea de Negocio
		if (filtros.getIncisoAutoCot().getNegocioSeccionId() != null) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " autoincisob.value.negocioSeccionId = :negocioSeccionId";
			Utilerias.agregaHashLista(listaParametrosValidos, "negocioSeccionId", filtros.getIncisoAutoCot().getNegocioSeccionId());
		}
		
		// Filtro Paquete
		if (filtros.getIncisoAutoCot().getNegocioPaqueteId() != null) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " autoincisob.value.negocioPaqueteId = :negocioPaqueteId";
			Utilerias.agregaHashLista(listaParametrosValidos, "negocioPaqueteId", filtros.getIncisoAutoCot().getNegocioPaqueteId());
		}
		
		// Filtro Numero Serie
		if (!StringUtils.isBlank(filtros.getIncisoAutoCot().getNumeroSerie())) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " UPPER(autoincisob.value.numeroSerie) LIKE '" + filtros.getIncisoAutoCot().getNumeroSerie() + "%'";				
		}
		
		//Filtro Numero Motor
		if (!StringUtils.isBlank(filtros.getIncisoAutoCot().getNumeroMotor())) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " UPPER(autoincisob.value.numeroMotor) LIKE '" + filtros.getIncisoAutoCot().getNumeroMotor() + "%'";				
		}
		
		//Filtro Placa
		if (!StringUtils.isBlank(filtros.getIncisoAutoCot().getPlaca())) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " UPPER(autoincisob.value.placa) LIKE '" + filtros.getIncisoAutoCot().getPlaca() + "%'";				
		}
		
		//Filtro Nombre Conductor
		if (!StringUtils.isBlank(filtros.getIncisoAutoCot().getNombreConductor())) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
				
			sWhere += " UPPER(CONCAT(autoincisob.value.nombreConductor,' ',autoincisob.value.paternoConductor,' ',autoincisob.value.maternoConductor)) LIKE '%" + filtros.getIncisoAutoCot().getNombreConductor() + "%'";
		}
		
		//Filtro Nombre Asegurado
		if (!StringUtils.isBlank(filtros.getIncisoAutoCot().getNombreAsegurado())) {
			if (sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " UPPER(autoincisob.value.nombreAsegurado) LIKE '" + filtros.getIncisoAutoCot().getNombreAsegurado() + "%'";				
		}
		
		//Filtro Fecha de Cancelacion
		//Nota: Se emplearon propiedades existentes del objeto filtros para transportar los valores de Fecha Cancelacion de y hasta, esto por conveniencia.
		
		//Filtro Fecha Cancelacion Desde
		if(getCancelados && filtros.getCotizacionDTO().getFechaInicioVigencia() != null) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " controlEndosoCot.recordFrom >= :desde";
			
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(filtros.getCotizacionDTO().getFechaInicioVigencia());
			gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
			gcFechaInicio.set(GregorianCalendar.SECOND, 0);
			gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			
			Utilerias.agregaHashLista(listaParametrosValidos, "desde", new Timestamp(gcFechaInicio.getTime().getTime()));
		}
		
		//Filtro Fecha Cancelacion Hasta
		if(getCancelados && filtros.getCotizacionDTO().getFechaFinVigencia() != null) {
			if(sWhere.equals("")) {
				sWhere += "";
			} else {
				sWhere += " AND ";
			}
			sWhere += " controlEndosoCot.recordFrom < :hasta";
			
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(filtros.getCotizacionDTO().getFechaFinVigencia());
			gcFechaFin.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaFin.set(GregorianCalendar.MINUTE, 0);
			gcFechaFin.set(GregorianCalendar.SECOND, 0);
			gcFechaFin.set(GregorianCalendar.MILLISECOND, 0);
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			
			Utilerias.agregaHashLista(listaParametrosValidos, "hasta", new Timestamp(gcFechaFin.getTime().getTime()));
		}
		
		/*Fin de Seccion de Filtros*/
		
		if (Utilerias.esAtributoQueryValido(sWhere)) {
			queryString = queryString.concat(" where ").concat(sWhere);				
		}

		//grouping
		if (!esConteo && !getCancelados) {
			queryString = queryString.concat(" GROUP BY ").concat(subject)
				.concat(" ORDER BY incisob.value.numeroSecuencia ");
		}else if(tipoQuery == TipoQueryListadoIncisos.INCISOS_PERDIDA_TOTAL ||
				tipoQuery == TipoQueryListadoIncisos.INCISOS_RECIBOS_AGRUPADOS)
		{
			queryString = queryString.concat(" ORDER BY incisob.value.numeroSecuencia ");			
		} else {
			queryString = queryString.concat(" ORDER BY incisob.value.numeroSecuencia ");
		}
		
		query = entityManager.createQuery(queryString);
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
		
	}
	
	private Map<String,String> getQueryStringIncisosPerdidaTotal(String subject)
	{
		Map<String,String> queryCompleto = new HashMap<String,String>();
		
		String selectStr = "SELECT " + subject + " FROM " +
				"BitemporalInciso incisob, " +				
				"PolizaDTO poliza, " +
				"SiniestroCabina siniestro " +
				" JOIN incisob.continuity incisoContinuity " +
				" JOIN incisoContinuity.cotizacionContinuity cotizacionContinuity " + 
				" JOIN incisoContinuity.autoIncisoContinuity autoIncisoContinuity " +
				" JOIN autoIncisoContinuity.autoIncisos autoincisob";
		 
		
		String whereStr = " cotizacionContinuity.id = :cotizacionContinuityId " + 
				" AND cotizacionContinuity.numero = poliza.cotizacionDTO.idToCotizacion " +
				" AND poliza.idToPoliza = siniestro.reporteCabina.poliza.idToPoliza " +
				" AND incisoContinuity.numero = siniestro.reporteCabina.seccionReporteCabina.incisoReporteCabina.numeroInciso " +
				" AND siniestro.reporteCabina.estatus IN (1,3)" +       
				" AND (SELECT COUNT(indemnizacion.id) from IndemnizacionSiniestro indemnizacion where " +
				"   indemnizacion.siniestro.id = (siniestro.id) " +     
				" AND indemnizacion.fechaAutIndemnizacion >=  FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI')" +
				" AND indemnizacion.estatusIndemnizacion = 'A' AND indemnizacion.ordenCompra.coberturaReporteCabina.claveTipoCalculo IN ('DM','RT') ) > 0 " + 
				" AND FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI') >= incisob.recordInterval.from" + 
				" AND FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI') < incisob.recordInterval.to" +
				" AND FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI') >= incisob.validityInterval.from " +
				" AND FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI') < incisob.validityInterval.to " +
				" AND FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI') >= autoincisob.recordInterval.from " +
				" AND FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI') < autoincisob.recordInterval.to " +
				" AND FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI') >= autoincisob.validityInterval.from " +
				" AND FUNCTION('TO_DATE',CONCAT(siniestro.reporteCabina.fechaOcurrido,' ',siniestro.reporteCabina.horaOcurrido),'DD/MM/YY HH24:MI') < autoincisob.validityInterval.to" +
				" AND incisoContinuity.id NOT IN (SELECT incisob2.continuity.id FROM ControlEndosoCot controlEndosoCot, SolicitudDTO solicitud, BitemporalInciso incisob2 " +
				" where " +
				" cotizacionContinuity.numero = controlEndosoCot.cotizacionId " +
				" AND controlEndosoCot.solicitud.idToSolicitud = solicitud.idToSolicitud " +
				" AND (( solicitud.claveTipoEndoso IN (24,25) " +				
				" AND incisob2.continuity.id = incisob.continuity.id  " +
				" AND controlEndosoCot.recordFrom = incisob2.recordInterval.from " +
				" AND controlEndosoCot.validFrom = FUNC('TRUNC',siniestro.reporteCabina.fechaOcurrido)) " +
				" OR (solicitud.claveTipoEndoso IN (6) " +
				" AND solicitud.claveMotivoEndoso NOT IN(10,22) " +
				" AND incisob2.continuity.id = incisob.continuity.id " +
				" AND controlEndosoCot.recordFrom = incisob2.recordInterval.from " +
				" AND controlEndosoCot.validFrom <= FUNC('TRUNC',siniestro.reporteCabina.fechaOcurrido))" +
				" ))";          
			
		queryCompleto.put("SELECT", selectStr);
		queryCompleto.put("WHERE", whereStr);		
		
		return queryCompleto;
	}
	
	private Map<String,String> getQueryStringIncisosRecibosAgrupados(String subject)
	{
		Map<String,String> queryCompleto = new HashMap<String,String>();
		
		String selectStr = "SELECT " + subject + " FROM " +
				" BitemporalInciso incisob " +				
				" JOIN incisob.continuity incisoContinuity " +
				" JOIN incisoContinuity.cotizacionContinuity cotizacionContinuity " + 
				" JOIN cotizacionContinuity.cotizaciones cotizacionb " +
				" JOIN incisoContinuity.autoIncisoContinuity autoIncisoContinuity " +				
				" JOIN autoIncisoContinuity.autoIncisos autoincisob";
		
		
		String whereStr = " cotizacionContinuity.id = :cotizacionContinuityId " +
				" AND CURRENT_TIMESTAMP >= cotizacionb.recordInterval.from and CURRENT_TIMESTAMP < cotizacionb.recordInterval.to " +
				" AND :validOn >= cotizacionb.validityInterval.from AND :validOn < cotizacionb.validityInterval.to " +
				" AND CURRENT_TIMESTAMP >= incisob.recordInterval.from and CURRENT_TIMESTAMP < incisob.recordInterval.to " +
				" AND :validOn >= incisob.validityInterval.from AND :validOn < incisob.validityInterval.to " +
				" AND CURRENT_TIMESTAMP >= autoincisob.recordInterval.from and CURRENT_TIMESTAMP < autoincisob.recordInterval.to " +
				" AND :validOn >= autoincisob.validityInterval.from AND :validOn < autoincisob.validityInterval.to " +
				" AND cotizacionb.value.tipoAgrupacionRecibos IS NOT NULL " +
				" AND cotizacionb.value.tipoAgrupacionRecibos != 'UB' " +
				" AND (autoincisob.value.tipoAgrupacionRecibosInciso IS NULL OR autoincisob.value.tipoAgrupacionRecibosInciso != 'UB')";
			         
			
		queryCompleto.put("SELECT", selectStr);
		queryCompleto.put("WHERE", whereStr);		  
		
		return queryCompleto;
	}
	
	public Long getNumeroIncisos(Long cotizacionContinuityId, DateTime validFromDT, DateTime recordFromDT, boolean getCancelados){
		Long numeroIncisos = 0l;
		
		Query query = null;		
		try{
				
			String queryString = "SELECT COUNT(DISTINCT IC.NUMERO)" +
					" FROM MIDAS.MINCISOC IC, MIDAS.MINCISOB IB " +
					" WHERE IC.MCOTIZACIONC_ID = " + cotizacionContinuityId +
					" AND IC.ID = IB.MINCISOC_ID ";
			
			if (validFromDT != null) {
				String fechIn;
				fechIn = Utilerias.cadenaDeFecha(validFromDT.toDate());
				queryString = queryString + " AND IB.VALIDFROM <= to_date('"+fechIn+"','dd/MM/YYYY') AND IB.VALIDTO > to_date('"+fechIn+"','dd/MM/YYYY') ";				
			}
			if (recordFromDT != null) {
				//String fechIn;
				//fechIn = Utilerias.cadenaDeFecha(recordFromDT.toDate());
				if(getCancelados){
					//queryString = queryString + " AND IB.RECORDFROM <= to_date('"+fechIn+"','dd/MM/YYYY') AND IB.RECORDTO = to_date('"+fechIn+"','dd/MM/YYYY') ";
					queryString = queryString + " AND IB.RECORDFROM <= ? AND IB.RECORDTO = ? ";
				}else{
					//queryString = queryString + " AND IB.RECORDFROM <= to_date('"+fechIn+"','dd/MM/YYYY') AND IB.RECORDTO > to_date('"+fechIn+"','dd/MM/YYYY') ";
					queryString = queryString + " AND IB.RECORDFROM <= ? AND IB.RECORDTO > ? ";
				}
			}
			query = entityManager.createNativeQuery(queryString);
			if (recordFromDT != null) {
				int index=1;
				query.setParameter(index, recordFromDT.toDate(), TemporalType.TIMESTAMP);
				index++;
				query.setParameter(index, recordFromDT.toDate(), TemporalType.TIMESTAMP);
			}
			
			BigDecimal numeroIncisosB = (BigDecimal) query.getSingleResult();
			
			numeroIncisos = numeroIncisosB.longValue();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}		
		
		return numeroIncisos;
	}
	/**
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	public List<ReferenciaBancariaDTO> getReferenciasBancarias(BigDecimal idToPoliza){
		
		LOG.trace(">> getReferenciasBancarias()");
		LOG.info(" Parametros de busqueda :: idToPoliza =  {"
				+ idToPoliza+ "...."
		        + "}  ");
			
			List<ReferenciaBancariaDTO> resultList = null;
		
			StoredProcedureHelper storedHelper = null;
			
			try {
				storedHelper = new StoredProcedureHelper("MIDAS.SP_RPT_REFERENCIAS", StoredProcedureHelper.DATASOURCE_MIDAS);
				
				
				storedHelper.estableceParametro("pIdToPoliza", String.valueOf(idToPoliza));
				String[] propiedades = new String[] {
													"fechaDesde",
													"fechaHasta",
													"fechaVencimiento",
													"poliza",
													"recibo",
												   	"estatus",
													"referenciaBancaria"
													  };
				String[] columnas = new String[] { 
													"FECHA_DESDE",
													"FECHA_HASTA",
													"FECHA_VENCIMIENTO",
													"POLIZA",
													"RECIBO", 
													"ESTATUS", 
													"REFERENCIABANCARIA"
												  };
				resultList = new ArrayList<ReferenciaBancariaDTO>();
				storedHelper.estableceMapeoResultados(ReferenciaBancariaDTO.class.getCanonicalName(),propiedades, columnas);
				resultList = storedHelper.obtieneListaResultados();
				} catch (Exception e) {
				e.printStackTrace();
				Integer codErr = null;
				String descErr = null;
				
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				e.printStackTrace();
				LOG.info("e.getMessage(): " + e.getMessage());
				LOG.info("codErr: " + codErr+" DescErr " + descErr);
			}
			if(resultList.size()>0){
				LOG.info("Cantidad de recibos encontrados {"
						+ resultList.size()+ "} ");
				for (int i = 0; i < resultList.size(); i++) {
					ReferenciaBancariaDTO referenciaBancariaDTO	=resultList.get(i);
					resultList.get(i).setReferenciaBancaria("");
					LOG.info("ReferenciaBancariaDTO =  {"
							+ referenciaBancariaDTO.toString()+ "}");
				}
				
			}

			LOG.trace("<< getReferenciasBancarias()");
			return resultList;	
	}
	/**
	 * Obtiene un resumen de las lineas de negocio con datos como Número de
	 * incisos y prima neta de una póliza a cierta fecha. Se creó para optimizar
	 * el tiempo de respuesta al momento de imprimir una póliza con muchos
	 * incisos, antes se calculaba todo en java.
	 * 
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<DatosLineasDeNegocioDTO> getDatosLineaNegocio(BigDecimal idToPoliza, Short numeroEndoso) {
		final StringBuilder sb = new StringBuilder();
		sb.append(" SELECT rownum, PRINCIPAL.* from( ");
		sb.append("		SELECT count(distinct numeroinciso) AS unidadesRiesgo, seccion.DESCRIPCIONSECCION AS lineaNegocio, ");
		sb.append("		sum(VALORPRIMANETA) as impPrimaNeta, seccion.fechaRegistro, seccion.numeroRegistro ");
		sb.append("		from midas.TOCOBERTURAEND ");
		sb.append("		INNER join MIDAS.TOSECCION seccion on (seccion.idtoseccion = TOCOBERTURAEND.idtoseccion) ");
		sb.append("		where idtopoliza = ?1 ");
		if(numeroEndoso!=null){
			sb.append("		and numeroendoso <= ?2");
		}	
		sb.append("		GROUP BY seccion.DESCRIPCIONSECCION, seccion.fechaRegistro, seccion.numeroRegistro  ");
		sb.append("		ORDER BY seccion.DESCRIPCIONSECCION ");
		sb.append("	 ) PRINCIPAL ");
		final Query query = entityManager.createNativeQuery(sb.toString(), DatosLineasDeNegocioDTO.class);
		query.setParameter(1, idToPoliza);
		query.setParameter(2, numeroEndoso);
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);		
		return query.getResultList();
	}
	
}
