package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza.seccion.paquete;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioPaqueteSeccionDaoImpl extends JpaDao<Long, NegocioPaqueteSeccion> implements
		NegocioPaqueteSeccionDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<NegocioCobPaqSeccion> listaCoberturasPorPaqueteSeccion(
			BigDecimal idToSeccion, Long idPaquete) {
		StringBuilder stm = new StringBuilder("");
		stm.append("SELECT model FROM NegocioCobPaqSeccion model where model.negocioPaqueteSeccion.paquete.id =:idPaquete ");
		stm.append("and model.negocioPaqueteSeccion.idToSeccion =:idToSeccion ");

		Query query = entityManager.createQuery(stm.toString());
		query.setParameter("idPaquete", idToSeccion);
		query.setParameter("idToSeccion", idPaquete);

		query.setHint(QueryHints.REFRESH, HintValues.FALSE);
		query.setHint(QueryHints.CACHE_RETRIEVE_MODE, HintValues.TRUE);
		return query.getResultList();
	}

	@Override
	public List<NegocioPaqueteSeccion> listarPaquetesSecccionPorCotizacionInciso(
			BigDecimal idToCotizacion, BigDecimal idNegocioSeccion) {
		try {
			StringBuffer queryString = new StringBuffer(
					"SELECT model FROM NegocioPaqueteSeccion model WHERE "
							+ "model.negocioSeccion.idToNegSeccion =:idNegocioSeccion AND model.idToNegPaqueteSeccion IN (SELECT DISTINCT inciso.incisoAutoCot.negocioPaqueteId FROM IncisoCotizacionDTO inciso WHERE");
			queryString.append(" inciso.id.idToCotizacion =:idToCotizacion)");
			queryString.append("ORDER BY model.idToNegPaqueteSeccion");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("idNegocioSeccion", idNegocioSeccion);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return query.getResultList();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@Override
	public NegocioPaqueteSeccion getPorIdNegSeccionIdPaquete(BigDecimal idToNegSeccion, Long id) {
		LogDeMidasEJB3.log("finding NegocioPaqueteSeccion instances by idToNegSeccion " + idToNegSeccion + " idPaquete "+id, Level.INFO, null);
		try {
			String queryString = "select model from NegocioPaqueteSeccion model where " + 
				" model.negocioSeccion.idToNegSeccion = :idToNegSeccion " +
				" and model.paquete.id = :id ";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToNegSeccion", idToNegSeccion);
			query.setParameter("id", id);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			return (NegocioPaqueteSeccion) query.getSingleResult();
		} catch (RuntimeException re) {
			throw re;
		}
	}
	
	@Override
	public NegocioPaqueteSeccion findByNegocioProductoTipoPolizaSeccionPaquete(
			Long idToNegocio, BigDecimal idToProducto, BigDecimal idToTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete) {
		final String jpql = "select model from NegocioPaqueteSeccion model where " +
				"model.paquete.id = :idPaquete and model.negocioSeccion.seccionDTO.idToSeccion = :idToSeccion " +
				"and model.negocioSeccion.negocioTipoPoliza.tipoPolizaDTO.idToTipoPoliza = :idToTipoPoliza " +
				"and model.negocioSeccion.negocioTipoPoliza.negocioProducto.negocio.idToNegocio = :idToNegocio " +
				"and model.negocioSeccion.negocioTipoPoliza.negocioProducto.productoDTO.idToProducto = :idToProducto";
		TypedQuery<NegocioPaqueteSeccion> query = entityManager.createQuery(jpql, NegocioPaqueteSeccion.class);
		query.setParameter("idPaquete", idPaquete);
		query.setParameter("idToSeccion", idToSeccion);
		query.setParameter("idToTipoPoliza", idToTipoPoliza);
		query.setParameter("idToNegocio", idToNegocio);
		query.setParameter("idToProducto", idToProducto);
		List<NegocioPaqueteSeccion> list = query.getResultList();
		if (list.size() == 1) {
			return list.get(0);
		}		
		return null;
	}

	@Override
	public NegocioPaqueteSeccion findByNegocioSeccionAndPaqueteDescripcion(
			NegocioSeccion negocioSeccion, String paqueteDescripcion) {
		final String jpql = "select model from NegocioPaqueteSeccion model where model.negocioSeccion = :negocioSeccion " +
				"and model.paquete.descripcion = :paqueteDescripcion";
		TypedQuery<NegocioPaqueteSeccion> query = entityManager.createQuery(jpql, NegocioPaqueteSeccion.class);
		query.setParameter("negocioSeccion", negocioSeccion);
		query.setParameter("paqueteDescripcion", paqueteDescripcion);
		List<NegocioPaqueteSeccion> resultList = query.getResultList();
		if (resultList.size() == 1) {
			return resultList.get(0);
		}		
		return null;
	}
	
	


}
