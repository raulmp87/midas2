package mx.com.afirme.midas2.dao.impl.tarifa;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaAutoModifDescDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifDesc;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifDescId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;

@Stateless
public class TarifaAutoModifDescDaoImpl extends JpaDao<TarifaAutoModifDescId, TarifaAutoModifDesc> implements TarifaAutoModifDescDao{

	@Override
	public List<TarifaAutoModifDesc> findByFilters(TarifaAutoModifDesc filter) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<TarifaAutoModifDesc> findByTarifaVersionId(
			TarifaVersionId tarifaVersionId) {
		
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT tarifa FROM TarifaAutoModifDesc tarifa ")
		   .append(" WHERE tarifa.id.idMoneda = :idMoneda")
		   .append(" AND tarifa.id.idRiesgo = :idRiesgo")
		   .append(" AND tarifa.id.idConcepto = :idConcepto")
		   .append(" AND tarifa.id.version = :version");
		
		Query query =(TypedQuery<TarifaAutoModifDesc>)entityManager.createQuery(sql.toString());
		
		query.setParameter("idMoneda", tarifaVersionId.getIdMoneda());
		query.setParameter("idRiesgo", tarifaVersionId.getIdRiesgo());
		query.setParameter("idConcepto", tarifaVersionId.getIdConcepto());
		query.setParameter("version", tarifaVersionId.getVersion());
		
		return query.getResultList();
	}

}
