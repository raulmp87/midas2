package mx.com.afirme.midas2.dao.impl.tarifa;

import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaVersionDao;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId_;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion_;

@Stateless
public class TarifaVersionDaoImpl extends JpaDao<TarifaVersionId, TarifaVersion> implements
		TarifaVersionDao {
	
	public List<Long> cargarVersiones(TarifaVersionId id) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<Long> criteriaQuery = cb.createQuery(Long.class);
		
		Root<TarifaVersion> root = criteriaQuery.from(TarifaVersion.class);
		
		criteriaQuery.select(root.get(TarifaVersion_.id).get(TarifaVersionId_.version));
		
		Predicate predicado = 
			cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idConcepto), id.getIdConcepto()),
					cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idMoneda), id.getIdMoneda()),
							cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idRiesgo), id.getIdRiesgo()),
								cb.notEqual(root.get(TarifaVersion_.claveEstatus), TarifaVersion.ESTATUS_BORRADO))));
		
		criteriaQuery.where(predicado);
		
		criteriaQuery.orderBy(cb.asc(root.get(TarifaVersion_.id).get(TarifaVersionId_.version)));
		
		TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
		
	}
	
	public List<Long> cargarMonedas(TarifaVersionId id) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<Long> criteriaQuery = cb.createQuery(Long.class);
		
		Root<TarifaVersion> root = criteriaQuery.from(TarifaVersion.class);
		
		criteriaQuery.select(root.get(TarifaVersion_.id).get(TarifaVersionId_.idMoneda));
		
		Predicate predicado = 
			cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idConcepto), id.getIdConcepto()),
					cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.version), id.getVersion()),
							cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idRiesgo), id.getIdRiesgo()),
									cb.notEqual(root.get(TarifaVersion_.claveEstatus), TarifaVersion.ESTATUS_BORRADO))));
		
		criteriaQuery.where(predicado);
		
		criteriaQuery.orderBy(cb.asc(root.get(TarifaVersion_.id).get(TarifaVersionId_.idMoneda)));
		
		TypedQuery<Long> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
		
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Long generarNuevaVersion(TarifaVersionId id, String negocio, Short tipoTarifa) {
		// TODO regresar la nueva version generada para esa tarifa
		Long nuevaVersion = null;
		String spName="MIDAS.pkgPYT_Servicios.spPYT_NuevaVersionConceptoTar";
		try {
			
			/**
  PROCEDURE spPYT_NuevaVersionConceptoTar (
  pIdLineaNegocio                 IN        INTEGER,                                      -- (IN)  Id de Linea de Negocio
  pIdMoneda                       IN        INTEGER,                                      -- (IN)  Id de Moneda
  pIdRiesgo                       IN        INTEGER,                                      -- (IN)  Id de Riesgo
  pIdConcepto                     IN        INTEGER,                                      -- (IN)  Id de Concepto
  pIdVersion                      IN        INTEGER,                                      -- (IN)  Id de Version Base
  pClaveNegocio                   IN        CHAR,                                         -- (IN)  Clave de Negocio
  pTipoTarifa                     IN        INTEGER,                                      -- (IN)  Tipo de Concepto Tarifario                       
  pIdVersionNueva                 OUT       NUMBER,                                       -- (IN)  Id de Version Nueva
  pId_Cod_Resp                    OUT       NUMBER,                                       -- (OUT) Codigo de Respuesta
  pDesc_Resp                      OUT       VARCHAR2                                      -- (OUT) Descripcion de Respuesta
);

			 */
			
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro("pIdMoneda", id.getIdMoneda());	
			storedHelper.estableceParametro("pIdRiesgo", id.getIdRiesgo());	
			storedHelper.estableceParametro("pIdConcepto", id.getIdConcepto());
			storedHelper.estableceParametro("pIdVersion", id.getVersion());
			storedHelper.estableceParametro("pClaveNegocio", negocio);
			storedHelper.estableceParametro("pTipoTarifa", tipoTarifa);
						
			
			nuevaVersion = Long.valueOf(storedHelper.ejecutaActualizar());
			
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName);
		}
	
		return nuevaVersion;
	}
	
	
	public TarifaVersion cargarActivo(TarifaVersionId id) {

		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<TarifaVersion> criteriaQuery = cb.createQuery(TarifaVersion.class);
		
		Root<TarifaVersion> root = criteriaQuery.from(TarifaVersion.class);
		
		Predicate predicado = 
			cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idConcepto), id.getIdConcepto()),
					cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idMoneda), id.getIdMoneda()),
							cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idRiesgo), id.getIdRiesgo()),
									cb.equal(root.get(TarifaVersion_.claveEstatus), TarifaVersion.ESTATUS_ACTIVO))));
		
		criteriaQuery.where(predicado);
		
		TypedQuery<TarifaVersion> query = entityManager.createQuery(criteriaQuery);
		
		List<TarifaVersion> tarifaVersiones = query.getResultList();
		
		if (tarifaVersiones.size() > 0) {
			return tarifaVersiones.get(0);
		}
		
		return null;
		
	}
	
	
public List<TarifaVersion> cargarTarifas(TarifaVersionId id) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<TarifaVersion> criteriaQuery = cb.createQuery(TarifaVersion.class);
		
		Root<TarifaVersion> root = criteriaQuery.from(TarifaVersion.class);
		
		
		Predicate predicado = 
			cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idConcepto), id.getIdConcepto()),
					cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idMoneda), id.getIdMoneda()),
							cb.and(cb.equal(root.get(TarifaVersion_.id).get(TarifaVersionId_.idRiesgo), id.getIdRiesgo()),
								cb.notEqual(root.get(TarifaVersion_.claveEstatus), TarifaVersion.ESTATUS_BORRADO))));
		
		criteriaQuery.where(predicado);
		
		criteriaQuery.orderBy(cb.asc(root.get(TarifaVersion_.id).get(TarifaVersionId_.version)));
		
		TypedQuery<TarifaVersion> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
		
	}
	

}
