package mx.com.afirme.midas2.dao.impl.tarifa;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.tarifa.TipoUsoVehiculoPorTipoVehiculoDao;

@Stateless
public class TipoUsoVehiculoPorTipoVehiculoDaoImpl implements
		TipoUsoVehiculoPorTipoVehiculoDao {

	@Override
	public List<TipoUsoVehiculoDTO> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public List<TipoUsoVehiculoDTO> listRelated(Object id) {
		String spName="MIDAS.pkgAUT_Servicios.spAUT_TipUsoVehPorTipoVehiculo";
		String [] atributosDTO = { "codigoTipoUsoVehiculo" , "descripcionTipoUsoVehiculo" };
		String [] columnasCursor = { "CODIGOTIPOUSOVEHICULO", "DESCRIPCIONTIPOUSOVEHICULO"};
		try {
			
			Long idTipoVehiculo = (Long)id;
			
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO", atributosDTO, columnasCursor);
			
			storedHelper.estableceParametro("pIdTipoVehiculo", idTipoVehiculo);
			
			return storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName,e);
		}
	}

	@Override
	public TipoUsoVehiculoDTO findById(BigDecimal id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoUsoVehiculoDTO findById(CatalogoValorFijoId id) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public TipoUsoVehiculoDTO findById(double id) {
		// TODO Auto-generated method stub
		return null;
	}

}
