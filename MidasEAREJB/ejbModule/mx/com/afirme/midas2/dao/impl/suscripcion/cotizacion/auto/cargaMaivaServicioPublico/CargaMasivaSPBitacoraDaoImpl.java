package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.cargaMaivaServicioPublico;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaSPBitacoraDao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPFiltradoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPResultadoDTO;
import mx.com.afirme.midas2.util.StringUtil;

@Stateless
public class CargaMasivaSPBitacoraDaoImpl extends JpaDao<Long, CargaMasivaServicioPublicoDetalle> implements CargaMasivaSPBitacoraDao{
	

	@Override
	public Long contarBitacora(BitacoraCargaMasivaSPFiltradoDTO filtroBitacora) {
		Query query = generarQuery(filtroBitacora, true);
		return ((Long)query.getSingleResult());		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<BitacoraCargaMasivaSPResultadoDTO> buscarBitacora(
			BitacoraCargaMasivaSPFiltradoDTO filtroBitacora) {
		Query query = generarQuery(filtroBitacora, false);
		if(filtroBitacora.getPosStart() != null){
			query.setFirstResult(filtroBitacora.getPosStart());
		}
		if(filtroBitacora.getCount() != null){
			query.setMaxResults(filtroBitacora.getCount());
		}
		return query.getResultList();
	}
	
	
	@SuppressWarnings("rawtypes")
	private Query generarQuery(BitacoraCargaMasivaSPFiltradoDTO filtro, boolean esCuenta){
		Query query 							= null;
		StringBuilder queryString 				= new StringBuilder();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>(1);	
		
		
		if(esCuenta){
			queryString.append("SELECT count(detalle.idDetalleCargaMasiva)");
		}else{
			
			queryString.append("SELECT new mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPResultadoDTO(");
			queryString.append("detalle.idDetalleCargaMasiva, FUNC('to_char',detalle.numToPoliza),  detalle.numeroSerie , ");
			queryString.append("detalle.fechaCreacion, detalle.codigoUsuarioCreacion, detalle.estatusProceso , detalle.descripcionEstatusProceso)");
		}
		queryString.append(" FROM CargaMasivaServicioPublicoDetalle detalle WHERE detalle.idDetalleCargaMasiva = detalle.idDetalleCargaMasiva ");
	
		//TODO agregar filtros
		if(!StringUtil.isEmpty(filtro.getNumSerie())){
			queryString.append(" AND  FUNC('UPPER',detalle.numeroSerie) LIKE '%").append(filtro.getNumSerie().toUpperCase()).append("%'");		
		}
		if(!StringUtil.isEmpty(filtro.getPoliza())){
			queryString.append(" AND detalle.numToPoliza = :pNumToPoliza");		
			Utilerias.agregaHashLista(listaParametrosValidos, "pNumToPoliza", Long.valueOf(filtro.getPoliza()));
		}
		if(!StringUtil.isEmpty(filtro.getEstatus())){
			queryString.append(" AND detalle.estatus = :pEstatus");		
			Utilerias.agregaHashLista(listaParametrosValidos, "pEstatus", filtro.getEstatus());
		}
		if(!StringUtil.isEmpty(filtro.getCodigoError())){
			queryString.append(" AND detalle.descripcionEstatusProceso LIKE '%':pDescripcionEstatusProceso'%'");		
			Utilerias.agregaHashLista(listaParametrosValidos, "pDescripcionEstatusProceso", filtro.getCodigoError());
		}
		if( filtro.getFechaInicioCarga()!=null ){
			queryString.append(" AND FUNC('trunc',detalle.fechaCreacion) >= FUNC('trunc', :fechaInicioCarga) ");		
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaInicioCarga", filtro.getFechaInicioCarga());
		}
		
		if( filtro.getFechaFinCarga() !=null ){
			queryString.append(" AND FUNC('trunc',detalle.fechaCreacion) <= FUNC('trunc',:fechaFinCarga) ");	
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFinCarga", filtro.getFechaFinCarga());
		}
		
		
		
		query = entityManager.createQuery(queryString.toString());
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;	
	}
		
	
}



