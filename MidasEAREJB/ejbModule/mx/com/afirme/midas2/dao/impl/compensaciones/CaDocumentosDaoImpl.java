/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaDocumentosDao;
import mx.com.afirme.midas2.domain.compensaciones.CaDocumentos;

import org.apache.log4j.Logger;

@Stateless
public class CaDocumentosDaoImpl implements CaDocumentosDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaDocumentosDaoImpl.class);
	
	public void save(CaDocumentos entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaDocumentos 	::		CaDocumentosDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaDocumentos 	::		CaDocumentosDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaDocumentos 	::		CaDocumentosDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaDocumentos entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaDocumentos 	::		CaDocumentosDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaDocumentos.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaDocumentos 	::		CaDocumentosDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaDocumentos 	::		CaDocumentosDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaDocumentos update(CaDocumentos entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaDocumentos 	::		CaDocumentosDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaDocumentos result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaDocumentos 	::		CaDocumentosDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaDocumentos 	::		CaDocumentosDaoImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaDocumentos findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaDocumentosDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaDocumentos instance = entityManager.find(CaDocumentos.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaDocumentosDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaDocumentosDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<CaDocumentos> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaDocumentosDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaDocumentos model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaDocumentosDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaDocumentosDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaDocumentos> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaDocumentos> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaDocumentos> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaDocumentos> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaDocumentos> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaDocumentosDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaDocumentos model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaDocumentosDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaDocumentosDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
