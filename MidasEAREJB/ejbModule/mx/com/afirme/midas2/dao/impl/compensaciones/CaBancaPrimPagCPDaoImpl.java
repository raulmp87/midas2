package mx.com.afirme.midas2.dao.impl.compensaciones;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import mx.com.afirme.midas2.dao.compensaciones.CaBancaPrimPagCPDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaPrimPagCP;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless
public class CaBancaPrimPagCPDaoImpl implements CaBancaPrimPagCPDao{
	
	
	public static final String COSTONETOSINAUT = "costonetosinaut";
	public static final String COSTONETOSINDAN = "costonetosindan";
	public static final String PRIMASRETENDEVENAUT = "primasretendevenaut";
	public static final String PRIMASRETENDEVENDAN = "primasretendevendan";
	public static final String ANIO = "anio";
	public static final String MES = "mes";
	public static final String MODIFICACION = "modificacion";
	public static final String USUARIO = "usuario";
	
	
	@PersistenceContext   
	private EntityManager entityManager;
	private static final Logger LOG = LoggerFactory.getLogger(CaBancaPrimPagCPDaoImpl.class);
	
	

	public void save(CaBancaPrimPagCP entity) {
		LOG.info(">> save()");
		try {
		entityManager.persist(entity);
		LOG.info("<< save()");
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
		    throw re;
		}
	}
	
	 public void delete(CaBancaPrimPagCP entity) {
			LOG.info(">> delete()");
			 try {
				entity = entityManager.getReference(CaBancaPrimPagCP.class, entity.getId());
				entityManager.remove(entity);
			 			LOG.info("<< delete()");
			 } catch (RuntimeException re) {
				 LOG.error("Informacion del Error" , re);
			     throw re;
			}
}
	 
	 
	 public CaBancaPrimPagCP update(CaBancaPrimPagCP entity) {
			LOG.info(">> update()");
			 try {
			 CaBancaPrimPagCP result = entityManager.merge(entity);
			 LOG.info("<< update()");
			     return result;
			} catch (RuntimeException re) {
				LOG.error("Informacion del Error" , re);
			     throw re;
			}
}
	 
	 
	  public CaBancaPrimPagCP findById( Long id) {
			LOG.info(">> findById()");
		  try {
		  CaBancaPrimPagCP instance = entityManager.find(CaBancaPrimPagCP.class, id);
		  return instance;
		} catch (RuntimeException re) {
			LOG.error("Informacion del Error" , re);
		      throw re;
		}
}   

}
