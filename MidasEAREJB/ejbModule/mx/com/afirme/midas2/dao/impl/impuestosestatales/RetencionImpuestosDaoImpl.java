package mx.com.afirme.midas2.dao.impl.impuestosestatales;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.impuestosestatales.RetencionImpuestosDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.catalogos.retencionimpuestos.RetencionCedularesView;
import mx.com.afirme.midas2.dto.impuestosestatales.RetencionImpuestosDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosDetPrimas;
import mx.com.afirme.midas2.dto.reportesAgente.RetencionImpuestosReporteView;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.DateUtils;

import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.jpa.jpql.parser.Expression;

@Stateless
public class RetencionImpuestosDaoImpl implements RetencionImpuestosDao {
	private static final Logger LOG = Logger.getLogger(RetencionImpuestosDaoImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	private static final String USUARIO_JOB = "SISTEMA";
	
	private static final String MODEL_FECHAEJECUCION = "fechaEjecucion";
	private static final String MODEL_IDMONITOR = "idRegistroMonitor";
	
	private static final String QUERY_MODEL_MONITOR = "SELECT model FROM RetencionCedularesView model ";
	
	private static final String QUERY_MONITOR_ORDERBY = " ORDER BY model.id DESC, model.fechaEjecucion DESC, model.idConfiguracion DESC, model.tipoEjecucion";
	
	private static final String QUERY_CONDITION_MODEL_FECHAEJECUCION = " model.fechaEjecucion >= :fechaEjecucion ";
	
	private static final String SP_CALCULORETENCIONES_NAME = "MIDAS.PKGCALCULOS_AGENTES.stp_calculoRetencionesAsync";
	private static final String SP_CALCULORETENCIONES_PARAM1 = "p_idConfig";
	private static final String SP_CALCULORETENCIONES_PARAM2 = "p_accion";
	private static final String SP_CALCULORETENCIONES_PARAM3 = "p_modoEjecucion";
	private static final String SP_CALCULORETENCIONES_PARAM4 = "p_username";
	private static final String SP_CALCULORETENCIONES_PARAM5 = "p_idMonitor";
	
	private static final String SP_REGISTROMONITOR_NAME = "MIDAS.PKGCALCULOS_AGENTES.stp_regMonitorRetImpEst";
	private static final String SP_REGISTROMONITOR_PARAM1 = "p_modoEjecucion";
	private static final String SP_REGISTROMONITOR_PARAM2 = "p_idRegistro";
	private static final String SP_REGISTROMONITOR_PARAM3 = "p_tipoEjecucion";
	private static final String SP_REGISTROMONITOR_PARAM4 = "p_estatusEjecucion";
	private static final String SP_REGISTROMONITOR_PARAM5 = "p_avance";
	private static final String SP_REGISTROMONITOR_PARAM6 = "p_username";
	private static final String SP_REGISTROMONITOR_PARAM7 = "p_idConfiguracionRet";
	
	private static final String SP_REPORTEDETALLEPRIMAS_NAME = "MIDAS.PKGREPORTES_AGENTES.stp_repDetPriAgeRetImpuestos";
	private static final String SP_REPORTEDETALLEPRIMAS_PARAM1 = "panio";
	private static final String SP_REPORTEDETALLEPRIMAS_PARAM2 = "pmes";
	private static final String SP_REPORTEDETALLEPRIMAS_PARAM3 = "pestado";
	private static final String SP_REPORTEDETALLEPRIMAS_PARAM4 = "pidagente";
	
	private static final String SP_LIST_CONFIGURACIONES_NAME = "MIDAS.PKGCALCULOS_AGENTES.stp_getListConfiguraciones";
	private static final String SP_LIST_CONFIGURACIONES_PARAM1 = "pid";
	private static final String SP_LIST_CONFIGURACIONES_PARAM2 = "pidEstado";
	private static final String SP_LIST_CONFIGURACIONES_PARAM3 = "pporcentaje";
	private static final String SP_LIST_CONFIGURACIONES_PARAM4 = "pconcepto";
	private static final String SP_LIST_CONFIGURACIONES_PARAM5 = "ptipoComision";
	private static final String SP_LIST_CONFIGURACIONES_PARAM6 = "ppersonalidadJuridica";
	private static final String SP_LIST_CONFIGURACIONES_PARAM7= "pfechaInicioVigencia";
	private static final String SP_LIST_CONFIGURACIONES_PARAM8 = "pfechaFinVigencia";

	private static final String SP_LIST_CONFIGURACIONESHISTORY_NAME = "MIDAS.PKGCALCULOS_AGENTES.stp_getListConfiguracionesHist";
	private static final String SP_LIST_CONFIGURACIONESHISTORY_PARAM1 = "pidConfiguracion";
	
	@SuppressWarnings("unchecked")
	public List<RetencionImpuestosDTO> findByFiltersView(RetencionImpuestosDTO filtro) {
		List<RetencionImpuestosDTO> result = new ArrayList<RetencionImpuestosDTO>();
		StoredProcedureHelper storedHelper = null;
		String[] atributosDTO = {"id", "estadoId", "porcentaje", "concepto", "tipoComisionId", "personalidadJuridicaId",
				"fechaInicioVig", "fechaFinVig", "usuario", "fechaModificacionTimestamp", "estatus"};
		String[] columnasResulset = {"ID", "ESTADO_ID", "PORCENTAJE", "CONCEPTO", "TIPOCOMISION_ID", "PERSONAJURIDICA_ID",
				"FECHA_INICIOVIGENCIA", "FECHA_FINVIGENCIA", "USUARIO", "FECHA_MODIFICACION", "ESTATUS"};
		
		try {
			storedHelper = new StoredProcedureHelper(SP_LIST_CONFIGURACIONES_NAME, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(RetencionImpuestosDTO.class.getCanonicalName(), atributosDTO, columnasResulset);
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONES_PARAM1, filtro.getId());
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONES_PARAM2, filtro.getEstado().getStateId());
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONES_PARAM3, filtro.getPorcentaje());
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONES_PARAM4, filtro.getConcepto());
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONES_PARAM5, filtro.getTipoComision().getId());
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONES_PARAM6, filtro.getPersonalidadJuridica().getId());
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONES_PARAM7, filtro.getFechaInicioVig());
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONES_PARAM8, filtro.getFechaFinVig());
			result = (ArrayList<RetencionImpuestosDTO>) storedHelper.obtieneListaResultados();
		} catch(Exception e) {
			String descErr = storedHelper.getDescripcionRespuesta();
			LOG.error("Ha ocurrido un error al intentar obtener el listado de configuraciones.", e);
			throw new RuntimeException("Ha ocurrido un error al intentar obtener el listado de configuraciones.");
		}
		
		return result;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void ejecutaProcesoRetencion(RetencionImpuestosDTO configuracion, RetencionCedularesView monitor, TipoEjecucionCalculo modoEjecucion) {
		String sp = SP_CALCULORETENCIONES_NAME;
		StoredProcedureHelper storedHelper = null;
		try {
			storedHelper = new StoredProcedureHelper(sp);
			if(modoEjecucion.equals(TipoEjecucionCalculo.USUARIO)) {
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM1, configuracion.getId());
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM2, monitor.getTipoEjecucion().getClave());
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM3, modoEjecucion.getClave());
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM4, configuracion.getUsuario());
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM5, monitor.getId());
			} else if(modoEjecucion.equals(TipoEjecucionCalculo.JOB)) {
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM1, 0);
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM2, "");
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM3, modoEjecucion.getClave());
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM4, USUARIO_JOB);
				storedHelper.estableceParametro(SP_CALCULORETENCIONES_PARAM5, 0);
			}
			
			storedHelper.ejecutaActualizar();
		} catch(SQLException sqle) {
			String descErr = storedHelper.getDescripcionRespuesta();
			LOG.error(descErr.isEmpty() ? "Ha ocurrido un error durante la ejecucion del proceso de calculo de retenciones." : descErr, sqle);
			monitor.setEstatusEjecucion(new ValorCatalogoAgentes());
			monitor.getEstatusEjecucion().setClave(TipoEstatusEjecucionCalculo.ERROR.getClave());
			this.setMonitorAvance(monitor);
			throw new RuntimeException("Ha ocurrido un error durante la ejecucion del proceso de calculo de retenciones.");
		} catch(Exception e) {
			LOG.error("Ha ocurrido un error al intentar ejecutar el proceso de calculo de retenciones.", e);
			monitor.setEstatusEjecucion(new ValorCatalogoAgentes());
			monitor.getEstatusEjecucion().setClave(TipoEstatusEjecucionCalculo.ERROR.getClave());
			this.setMonitorAvance(monitor);
			throw new RuntimeException("Ha ocurrido un error al intentar ejecutar el proceso de calculo de retenciones.");
		}
	}

	public Long setMonitorAvance(RetencionCedularesView monitor) {
		RetencionCedularesView result;
		StoredProcedureHelper storedHelper;
		String sp = SP_REGISTROMONITOR_NAME;
		try {
			
			String[] atributosDTO = { "id" };
			String[] columnasCursor = { MODEL_IDMONITOR };
			
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro(SP_REGISTROMONITOR_PARAM1, TipoEjecucionCalculo.USUARIO.getClave());
			storedHelper.estableceParametro(SP_REGISTROMONITOR_PARAM2, monitor.getId());
			storedHelper.estableceParametro(SP_REGISTROMONITOR_PARAM3, monitor.getTipoEjecucion().getClave());
			storedHelper.estableceParametro(SP_REGISTROMONITOR_PARAM4, monitor.getEstatusEjecucion().getClave());
			storedHelper.estableceParametro(SP_REGISTROMONITOR_PARAM5, monitor.getAvance());
			storedHelper.estableceParametro(SP_REGISTROMONITOR_PARAM6, monitor.getUsuario());
			storedHelper.estableceParametro(SP_REGISTROMONITOR_PARAM7, monitor.getIdConfiguracion());
			
			storedHelper.estableceMapeoResultados(RetencionCedularesView.class.getCanonicalName(), atributosDTO, columnasCursor);
			
			result = (RetencionCedularesView) storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			LOG.error("Ha ocurrido un error al intentar ejecutar setMonitorAvance." , e);
			throw new RuntimeException("Ha ocurrido un error al intentar ejecutar el proceso de calculo de retenciones de Impuestos Estatales.");
		}
		return result.getId();
	}
	
	@SuppressWarnings("unchecked")
	public List<RetencionImpuestosReporteView>llenarDatosDetallePrimaRetencionImpuestos(RetencionImpuestosReporteView filtro) {
		String sp = SP_REPORTEDETALLEPRIMAS_NAME;
		List<RetencionImpuestosReporteView> resultList = new ArrayList<RetencionImpuestosReporteView>();
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		StoredProcedureHelper storedHelper;
		String[] cols;
		String[] props;

		try {
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			String[] atributosDTO = { 
					"idGerencia", "nombreGerencia", "idEjecutivo", "nombreEjecutivo", "oficina", "mes", "idPromotores", "nombrePromotor","idAgente", "nombreAgente", 
					"estado","impBase", "impIva", "impRetIva","impRetIsr", "impEstRet", "tr"
				};
			String[] columnasResulset = { 
					"ID_GERENCIA", "NOM_GERENCIA","ID_EJECUTIVO", "NOM_EJECUTIVO", "OFICINA", "MES", "ID_PROMOTORES", "NOMBRE_PROMOTORIA","ID_AGENTE", "NOMBRE",
					"ESTADO","IMP_BASE", "IMP_IVA", "IMP_RET_IVA","IMP_RET_ISR", "IMP_EST_RET", "TR"
					};
			
			cols = atributosDTO;
			props = columnasResulset;
			
			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			
			storedHelper.estableceMapeoResultados(DatosDetPrimas.class.getCanonicalName(),propiedades.toString(), columnas.toString());
			storedHelper.estableceMapeoResultados(RetencionImpuestosReporteView.class.getCanonicalName(), atributosDTO,columnasResulset);
			storedHelper.estableceParametro(SP_REPORTEDETALLEPRIMAS_PARAM1, filtro.getAnio());
			storedHelper.estableceParametro(SP_REPORTEDETALLEPRIMAS_PARAM2, filtro.getMes());
			storedHelper.estableceParametro(SP_REPORTEDETALLEPRIMAS_PARAM3, filtro.getEstado());
			storedHelper.estableceParametro(SP_REPORTEDETALLEPRIMAS_PARAM4, filtro.getIdAgente());
			
			resultList = storedHelper.obtieneListaResultados();
		} catch(Exception e) {
			LOG.error("Ha ocurrido un error al intentar obtener los datos para generar el Reporte Detalle de Primas de Impuestos Estatales", e);
			throw new RuntimeException("Ha ocurrido un error al intentar obtener los datos para generar el Reporte Detalle de Primas de Impuestos Estatales");
		}
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	public List<RetencionImpuestosDTO> listarHistorico(RetencionImpuestosDTO filtro) {
		List<RetencionImpuestosDTO> result = new ArrayList<RetencionImpuestosDTO>();
		StoredProcedureHelper storedHelper = null;
		String[] atributosDTO = {"id", "usuario", "fechaModificacionTimestamp", "estadoId", "porcentaje", "concepto", "tipoComisionId",
				"personalidadJuridicaId", "estatus", "fechaInicioVig", "fechaFinVig"};
		String[] columnasResulset = {"ID", "USUARIO", "FECHA_MODIFICACION", "ESTADO_ID", "PORCENTAJE", "CONCEPTO", "TIPOCOMISION_ID",
				"PERSONALIDADJURIDICA_ID", "ESTATUS", "FECHA_INICIOVIGENCIA", "FECHA_FINVIGENCIA"};
		
		try {
			storedHelper = new StoredProcedureHelper(SP_LIST_CONFIGURACIONESHISTORY_NAME, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(RetencionImpuestosDTO.class.getCanonicalName(), atributosDTO, columnasResulset);
			storedHelper.estableceParametro(SP_LIST_CONFIGURACIONESHISTORY_PARAM1, filtro.getId());
			result = (ArrayList<RetencionImpuestosDTO>) storedHelper.obtieneListaResultados();
		} catch(Exception e) {
			String descErr = storedHelper.getDescripcionRespuesta();
			LOG.error(descErr.isEmpty() ? "Ha ocurrido un error al intentar obtener el historial de modificaciones de la configuracion." : descErr, e);
			throw new RuntimeException("Ha ocurrido un error al intentar obtener el historial de modificaciones de la configuracion.");
		}
		
		return result;
	}
	
	@SuppressWarnings("unchecked")
	public List<RetencionCedularesView> listarMonitorRetencionImpuestos() {
		List<RetencionCedularesView> result;
		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder queryString = new StringBuilder();
		queryString.append(QUERY_MODEL_MONITOR);
		queryString.append(" " + Expression.WHERE + QUERY_CONDITION_MODEL_FECHAEJECUCION);
		
		Date currentDate = org.apache.commons.lang.time.DateUtils.truncate(new Date(), Calendar.DATE);
		currentDate = DateUtils.add(currentDate, DateUtils.Indicador.Days, -15);
		
		params.put(MODEL_FECHAEJECUCION, currentDate);
		
		String  finalQuery = queryString.toString() + QUERY_MONITOR_ORDERBY;
		Query query = entityManager.createQuery(finalQuery);
		setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		result = query.getResultList();
		return result;
	}
	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key= EntidadDaoImpl.getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}	
	}

}