package mx.com.afirme.midas2.dao.impl.operacionessapamis;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasCesviDao;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasCesvi;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class SapAlertasCesviDaoImpl implements SapAlertasCesviDao {	
	private EntidadService entidadService;

	@Override
	public void guardarAlertasCesvi(List<SapAlertasCesvi> alertasCesvi) {
		entidadService.saveAll(alertasCesvi);		
	}
	
	@EJB
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
