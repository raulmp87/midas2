package mx.com.afirme.midas2.dao.impl.poliza.renovacionmasiva;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.poliza.renovacionmasiva.OrdenRenovacionMasivaDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class OrdenRenovacionMasivaDaoImpl extends EntidadDaoImpl implements OrdenRenovacionMasivaDao {
	
	private UsuarioService usuarioService;
	
	@EJB(beanName = "UsuarioServiceDelegate") 
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Long listByFilterCount(OrdenRenovacionMasiva ordenRenovacion) {
		String queryString = "SELECT COUNT(model) FROM OrdenRenovacionMasiva model ";
		
		String sWhere = "";
		Query query;
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		if(ordenRenovacion.getNumeroPoliza() != null && ordenRenovacion.getNumeroPoliza().intValue() > 0){
			queryString += ", OrdenRenovacionMasivaDet ordDet ";
		}
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToOrdenRenovacion",ordenRenovacion.getIdToOrdenRenovacion());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreUsuarioCreacion",ordenRenovacion.getNombreUsuarioCreacion());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPolizas",ordenRenovacion.getNumeroPolizas());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus",ordenRenovacion.getClaveEstatus());
		
		if(ordenRenovacion.getNumeroPoliza() != null && ordenRenovacion.getNumeroPoliza().intValue() > 0){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.idToOrdenRenovacion = ordDet.id.idToOrdenRenovacion ";
		     sWhere += " and ordDet.polizaAnterior.numeroPoliza = " + ordenRenovacion.getNumeroPoliza() + " ";
		}
		
		if(ordenRenovacion.getFechaCreacion() != null){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.fechaCreacion >= :fechaCreacion ";
		}
		if(ordenRenovacion.getFechaCreacionHasta() != null){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.fechaCreacion <= :fechaCreacionHasta ";
		}
		if(ordenRenovacion.getFechaModificacion() != null){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.fechaModificacion >= :fechaModificacion ";
		}
		if(ordenRenovacion.getFechaModificacionHasta() != null){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.fechaModificacion <= :fechaModificacionHasta ";
		}
		
		//Validacion de agente
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			Promotoria promotoria = agenteUsuarioActual.getPromotoria();
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
			String agenteSubquery = " model.codigoUsuarioCreacion in (select a.codigoUsuario from Agente a where a.promotoria.id = " + promotoria.getId() + ") ";
			sWhere += agenteSubquery;
		}else if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		    }
			sWhere += " trim(model.codigoUsuarioCreacion) = trim('" + usuarioService.getUsuarioActual().getNombreUsuario() + "') ";
		} else if(!StringUtils.isBlank(ordenRenovacion.getCodigoUsuarioCreacion())) {
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		    }
			sWhere += " trim(model.codigoUsuarioCreacion) = trim('" + ordenRenovacion.getCodigoUsuarioCreacion() + "') ";
		}		
		
		if (Utilerias.esAtributoQueryValido(sWhere))
			queryString = queryString.concat(" where ").concat(sWhere);
		
		//Orden
		queryString += " ORDER BY model.fechaCreacion DESC ";
		
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		if(ordenRenovacion.getFechaCreacion() != null){
			GregorianCalendar gcFecha = new GregorianCalendar();
			gcFecha.setTime(ordenRenovacion.getFechaCreacion());
			gcFecha.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFecha.set(GregorianCalendar.MINUTE, 0);
			gcFecha.set(GregorianCalendar.SECOND, 0);
			gcFecha.set(GregorianCalendar.MILLISECOND, 0);
			
			query.setParameter("fechaCreacion", gcFecha.getTime(), TemporalType.TIMESTAMP);
		}
		
		if(ordenRenovacion.getFechaCreacionHasta() != null){
			GregorianCalendar gcFecha = new GregorianCalendar();
			gcFecha.setTime(ordenRenovacion.getFechaCreacionHasta());
			gcFecha.add(GregorianCalendar.DATE, 1);
			gcFecha.add(GregorianCalendar.MILLISECOND, -1);
			
			query.setParameter("fechaCreacionHasta", gcFecha.getTime(), TemporalType.TIMESTAMP);
		}
		
		if(ordenRenovacion.getFechaModificacion() != null){
			GregorianCalendar gcFecha = new GregorianCalendar();
			gcFecha.setTime(ordenRenovacion.getFechaModificacion());
			gcFecha.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFecha.set(GregorianCalendar.MINUTE, 0);
			gcFecha.set(GregorianCalendar.SECOND, 0);
			gcFecha.set(GregorianCalendar.MILLISECOND, 0);
			
			query.setParameter("fechaModificacion", gcFecha.getTime(), TemporalType.TIMESTAMP);
		}
		
		if(ordenRenovacion.getFechaModificacionHasta() != null){
			GregorianCalendar gcFecha = new GregorianCalendar();
			gcFecha.setTime(ordenRenovacion.getFechaModificacionHasta());
			gcFecha.add(GregorianCalendar.DATE, 1);
			gcFecha.add(GregorianCalendar.MILLISECOND, -1);
			
			query.setParameter("fechaModificacionHasta", gcFecha.getTime(), TemporalType.TIMESTAMP);
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		BigDecimal numero = BigDecimal.ZERO;
		try{
			Object object = query.getSingleResult();
			
			if(object instanceof BigDecimal)
				numero = (BigDecimal) object;
			else if (object instanceof Long)
				numero = BigDecimal.valueOf((Long)object);
			else if (object instanceof Double)
				numero = BigDecimal.valueOf((Double)object);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return numero.longValue();	

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenRenovacionMasiva> listByFilter(OrdenRenovacionMasiva ordenRenovacion) {
		String queryString = "SELECT model FROM OrdenRenovacionMasiva model ";
		
		String sWhere = "";
		Query query;
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		
		if(ordenRenovacion.getNumeroPoliza() != null && ordenRenovacion.getNumeroPoliza().intValue() > 0){
			queryString += ", OrdenRenovacionMasivaDet ordDet ";
		}
		
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "idToOrdenRenovacion",ordenRenovacion.getIdToOrdenRenovacion());
		sWhere = Utilerias.agregaParametroQueryLike(listaParametrosValidos,sWhere, "nombreUsuarioCreacion",ordenRenovacion.getNombreUsuarioCreacion());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "numeroPolizas",ordenRenovacion.getNumeroPolizas());
		sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere, "claveEstatus",ordenRenovacion.getClaveEstatus());
		
		if(ordenRenovacion.getNumeroPoliza() != null && ordenRenovacion.getNumeroPoliza().intValue() > 0){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.idToOrdenRenovacion = ordDet.id.idToOrdenRenovacion ";
		     sWhere += " and ordDet.polizaAnterior.numeroPoliza = " + ordenRenovacion.getNumeroPoliza() + " ";
		}
		
		if(ordenRenovacion.getFechaCreacion() != null){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.fechaCreacion >= :fechaCreacion ";
		}
		if(ordenRenovacion.getFechaCreacionHasta() != null){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.fechaCreacion <= :fechaCreacionHasta ";
		}
		if(ordenRenovacion.getFechaModificacion() != null){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.fechaModificacion >= :fechaModificacion ";
		}
		if(ordenRenovacion.getFechaModificacionHasta() != null){
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
		     sWhere += " model.fechaModificacion <= :fechaModificacionHasta ";
		}
			
		//Validacion de agente
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			Promotoria promotoria = agenteUsuarioActual.getPromotoria();
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		     }
			String agenteSubquery = " model.codigoUsuarioCreacion in (select a.codigoUsuario from Agente a where a.promotoria.id = " + promotoria.getId() + ") ";
			sWhere += agenteSubquery;
		}else if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		    }
			sWhere += " trim(model.codigoUsuarioCreacion) = trim('" + usuarioService.getUsuarioActual().getNombreUsuario() + "') ";
		} else if(!StringUtils.isBlank(ordenRenovacion.getCodigoUsuarioCreacion())) {
			if (Utilerias.esAtributoQueryValido(sWhere)) {
		        sWhere = sWhere.concat(" and ");
		    }
			sWhere += " trim(model.codigoUsuarioCreacion) = trim('" + ordenRenovacion.getCodigoUsuarioCreacion() + "') ";
		}
		
		if (Utilerias.esAtributoQueryValido(sWhere))
			queryString = queryString.concat(" where ").concat(sWhere);
		
		//Orden
		queryString += " ORDER BY model.fechaCreacion DESC ";
		
		query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		if(ordenRenovacion.getFechaCreacion() != null){
			GregorianCalendar gcFecha = new GregorianCalendar();
			gcFecha.setTime(ordenRenovacion.getFechaCreacion());
			gcFecha.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFecha.set(GregorianCalendar.MINUTE, 0);
			gcFecha.set(GregorianCalendar.SECOND, 0);
			gcFecha.set(GregorianCalendar.MILLISECOND, 0);
			
			query.setParameter("fechaCreacion", gcFecha.getTime(), TemporalType.TIMESTAMP);
		}
		
		if(ordenRenovacion.getFechaCreacionHasta() != null){
			GregorianCalendar gcFecha = new GregorianCalendar();
			gcFecha.setTime(ordenRenovacion.getFechaCreacionHasta());
			gcFecha.add(GregorianCalendar.DATE, 1);
			gcFecha.add(GregorianCalendar.MILLISECOND, -1);
			
			query.setParameter("fechaCreacionHasta", gcFecha.getTime(), TemporalType.TIMESTAMP);
		}
		
		if(ordenRenovacion.getFechaModificacion() != null){
			GregorianCalendar gcFecha = new GregorianCalendar();
			gcFecha.setTime(ordenRenovacion.getFechaModificacion());
			gcFecha.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFecha.set(GregorianCalendar.MINUTE, 0);
			gcFecha.set(GregorianCalendar.SECOND, 0);
			gcFecha.set(GregorianCalendar.MILLISECOND, 0);
			
			query.setParameter("fechaModificacion", gcFecha.getTime(), TemporalType.TIMESTAMP);
		}
		
		if(ordenRenovacion.getFechaModificacionHasta() != null){
			GregorianCalendar gcFecha = new GregorianCalendar();
			gcFecha.setTime(ordenRenovacion.getFechaModificacionHasta());
			gcFecha.add(GregorianCalendar.DATE, 1);
			gcFecha.add(GregorianCalendar.MILLISECOND, -1);
			
			query.setParameter("fechaModificacionHasta", gcFecha.getTime(), TemporalType.TIMESTAMP);
		}
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		if(ordenRenovacion.getPrimerRegistroACargar() != null && ordenRenovacion.getNumeroMaximoRegistrosACargar() != null){
			query.setFirstResult(ordenRenovacion.getPrimerRegistroACargar());
			query.setMaxResults(ordenRenovacion.getNumeroMaximoRegistrosACargar());
		}
		
		return query.getResultList();

	}

}
