package mx.com.afirme.midas2.dao.impl.siniestros.configuracion.horario.ajustador;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;



import mx.com.afirme.midas.interfaz.StoredProcedureHelper;

import mx.com.afirme.midas2.dao.impl.JpaDao;

import mx.com.afirme.midas2.dao.siniestros.configuracion.horario.ajustador.HorarioAjustadorDAO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina_;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro_;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustadorId;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustadorId_;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador_;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral_;
import mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador.ConfiguracionDeDisponibilidadDTO;

import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.HorarioAjustadorService.HorarioAjustadorFiltro;

import org.apache.log4j.Logger;


@Stateless
public class HorarioAjustadorDAOImpl extends
		JpaDao<HorarioAjustadorId, HorarioAjustador> implements
		HorarioAjustadorDAO {

	@PersistenceContext
	private EntityManager	entityManager;
	
	private static final Logger LOG = Logger.getLogger(HorarioAjustadorDAOImpl.class);
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HorarioAjustador> buscar(HorarioAjustadorFiltro filtro) {
		List<HorarioAjustador> ret = null;
		try{
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<HorarioAjustador> cq = cb.createQuery(HorarioAjustador.class);
		
		Root<HorarioAjustador> fromHorarioAjustador = cq.from(HorarioAjustador.class);

		Join<HorarioAjustador, HorarioAjustadorId> fromHorarioAjustadorId = fromHorarioAjustador.join(HorarioAjustador_.id);
		
		Join<HorarioAjustador, Oficina> fromOficina = fromHorarioAjustador.join(HorarioAjustador_.oficina);
		Join<HorarioAjustador, ServicioSiniestro> fromAjustador = fromHorarioAjustador.join(HorarioAjustador_.ajustador);
		Join<HorarioAjustador, HorarioLaboral> fromHorarioLaboral = fromHorarioAjustador.join(HorarioAjustador_.horarioLaboral);

		List<Predicate> predicates = new ArrayList<Predicate>();

		// variables para agregar los predicados
		Boolean validacion = false;
		Predicate predicado;

		// Filtro de ajustadores ids
		Expression<?> expAjustadores;
		validacion = filtro.validarAjustadoresIds();
		expAjustadores = fromAjustador.get(ServicioSiniestro_.id.getName());
		predicado = expAjustadores.in(filtro.getAjustadoresIds());
		predicateAddIf(validacion, predicates, predicado);

		// Filtro de oficina
		 validacion = filtro.getOficinaId() != null;
		 Expression<?> expOficina = fromOficina.get(Oficina_.id);
		 predicado = cb.equal(expOficina, filtro.getOficinaId());
		 predicateAddIf(validacion, predicates, predicado);

		// Filtro de HorarioEntrada
		Expression<String> expHorarioEntrada;
		validacion = filtro.validarHorarioInicialCode();
		expHorarioEntrada = fromHorarioLaboral
				.get(HorarioLaboral_.horaEntradaCode.getName());
		predicado = cb.equal(expHorarioEntrada, filtro.getHorarioInicialCode());
		predicateAddIf(validacion, predicates, predicado);

		// Filtro de HorarioSalida
		Expression<String> expHorarioFinal;
		validacion = filtro.validarHorarioFinalCode();
		expHorarioFinal = fromHorarioLaboral.get(HorarioLaboral_.horaSalidaCode
				.getName());
		predicado = cb.equal(expHorarioFinal, filtro.getHorarioFinalCode());
		predicateAddIf(validacion, predicates, predicado);

		// Filtro de Tipo Disponibilidad
		Expression<String> expTipoDispon;
		validacion = filtro.validarTipoDisponibilidadCode();
		expTipoDispon = fromHorarioAjustador
				.get(HorarioAjustador_.tipoDisponibilidadCode.getName());
		predicado = cb.equal(expTipoDispon, filtro.getTipoDisponibilidadCode());
		predicateAddIf(validacion, predicates, predicado);

		// Expresion para obtener la fecha de la tabla
		Expression<Date> expFecha;
		expFecha = fromHorarioAjustadorId.get(HorarioAjustadorId_.fecha);

		// Filtro de Fecha Inicial
		validacion = filtro.validarFechaInicial();
		predicado = cb.greaterThanOrEqualTo(expFecha, filtro.getFechaInicial());
		predicateAddIf(validacion, predicates, predicado);

		// Filtro de Fecha Final
		validacion = filtro.validarFechaFinal();
		predicado = cb.lessThanOrEqualTo(expFecha, filtro.getFechaFinal());
		predicateAddIf(validacion, predicates, predicado);
		
		// Filtro de Activo		
		Expression<String> expActivo = fromAjustador.get(ServicioSiniestro_.estatus.getName());
		predicado = cb.equal(expActivo, filtro.getActivo().booleanValue() ? (short)1: (short)0);
		predicateAddIf(true, predicates, predicado);

		cq.where(predicates.toArray(new Predicate[predicates.size()]));
		ret = entityManager.createQuery(cq) .getResultList();

		}catch(Exception e){
			e.printStackTrace();
		}

		return ret;
	}

	private void predicateAddIf(Boolean validacion, List<Predicate> predicates,
			Predicate predicado) {
		if (validacion) {
			predicates.add(predicado);
		}
	}
	
	private String convertDayListToEnglish(String daysListed){
		
		String daysList [] = daysListed.split(",");
		String englishDayList = "";
		for(int x=0; x<daysList.length; x++){
			String itera = daysList[x];
			
			if(itera.trim().equals("domingo")){
				englishDayList = englishDayList.concat("SUNDAY,");
			}else if(itera.trim().equals("lunes")){
				englishDayList = englishDayList.concat("MONDAY,");
			}else if(itera.trim().equals("martes")){
				englishDayList = englishDayList.concat("TUESDAY,");
			}else if(itera.trim().equals("miércoles")){
				englishDayList = englishDayList.concat("WEDNESDAY,");
			}else if(itera.trim().equals("jueves")){
				englishDayList = englishDayList.concat("THURSDAY,");
			}else if(itera.trim().equals("viernes")){
				englishDayList = englishDayList.concat("FRIDAY,");
			}else if(itera.trim().equals("sábado")){
				englishDayList = englishDayList.concat("SATURDAY,");
			}
		}
		
		englishDayList = this.quitLastCommaFromChainString(englishDayList);
		
		return englishDayList;
	}
	
	private String convertListToStringChain(List<String> listString){
		String enchainedString = "";

		//check if the last character is not comma. this for listaAjustadoresId list.
		for(int x = 0; x<listString.size();x++){
			enchainedString = enchainedString.concat(listString.get(x)+",");
		}
		
		enchainedString = this.quitLastCommaFromChainString(enchainedString);
		
		
		return enchainedString;
	}
	
	private String quitLastCommaFromChainString(String stringChained){
		String ultimoCaracter = stringChained.substring(stringChained.length()-1);
		if (ultimoCaracter.equals(",")){
			stringChained = stringChained.substring(0, stringChained.length()-1);
		}
		
		return stringChained;
	}
	
	@Override
	public int spGuardarHorariosAjustadorInt (ConfiguracionDeDisponibilidadDTO configuraciones){
		int codigoRespuesta = 0;
		String oficinaId = configuraciones.getOficinaId();
		String listaAjustadoresId = this.convertListToStringChain(configuraciones.getAjustadoresIds());
		Long horarioLaboralId = configuraciones.getHorarioLaboralId();
		String tipoDisponibilidadCode = configuraciones.getTipoDisponibilidadCode();
		Date fechaInicial = configuraciones.getFechaInicial();
		Date fechaFinal = configuraciones.getFechaFinal();
		String comentarios = configuraciones.getComentarios();
		String selectedDayList = this.convertDayListToEnglish(configuraciones.getDiasSeleccion());
		String userActual = this.usuarioService.getUsuarioActual().getNombreUsuario();
		//start execute process for stored procedure.
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_INCENTIVOS_AJUSTADORES.spGuardarHorariosAjustador";
		
		try{
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			spHelper.estableceParametro("oficinaId", oficinaId);
			spHelper.estableceParametro("listaAjustadoresIds", listaAjustadoresId);
			spHelper.estableceParametro("horarioLaboralId", horarioLaboralId);
			spHelper.estableceParametro("tipoDisponibilidadCode", tipoDisponibilidadCode);
			spHelper.estableceParametro("fechaInicial", fechaInicial);
			spHelper.estableceParametro("fechaFinal", fechaFinal);
			spHelper.estableceParametro("comentarios", comentarios);
			spHelper.estableceParametro("listaDiasSeleccionados", selectedDayList);
			spHelper.estableceParametro("pUserActual", userActual);
			
			codigoRespuesta = spHelper.ejecutaActualizar();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		return codigoRespuesta;
	}


}
