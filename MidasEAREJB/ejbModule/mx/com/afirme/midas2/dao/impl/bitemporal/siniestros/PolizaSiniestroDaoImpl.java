/**
 * 
 */
package mx.com.afirme.midas2.dao.impl.bitemporal.siniestros;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudDataEnTramiteDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.dao.bitemporal.siniestros.PolizaSiniestroDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina.EstatusReporteCabina;
import mx.com.afirme.midas2.dto.siniestros.EstatusCobranzaDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoPolizaDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ProgramaPagoDTO;
import mx.com.afirme.midas2.dto.siniestros.ReciboProgramaPagoDTO;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.DateUtils.Indicador;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;

/**
 * @author admin
 *
 */
@Stateless
public class PolizaSiniestroDaoImpl implements PolizaSiniestroDao {
	
	@EJB
	EntidadDao entidadDao;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private PolizaFacadeRemote polizaInterfazFacade;
	
	private static final Logger LOG = Logger.getLogger(PolizaSiniestroDaoImpl.class);
	
	private enum TIPO_BUSQUEDA{INCISOSINIESTRODTO, INCISOSINIESTROMIDASDTO};
	//joksrc obtener fecha vigencia real inicial del inciso
	@SuppressWarnings("unchecked")
	@Override
	public Date obtenerFechaVigenciaInicioRealPorInciso (Long idIncisoContinuity){
		
		StoredProcedureHelper spHelper = null;
		String fnName = "MIDAS.PKGSIN_POLIZAS.GETFECHAREALVIGENCIA";
		Date fechaRealInicialVigenteInciso = new Date();
		try{
			String atributos = "fechaVigenciaIniRealInciso";
			String columnaBd = "FECHASALIDA";
			spHelper = new StoredProcedureHelper( fnName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(IncisoSiniestroDTO.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("IDCONTINUITY", idIncisoContinuity);
			
			IncisoSiniestroDTO resultadoFecha =(IncisoSiniestroDTO)spHelper.obtieneResultadoSencillo();
			fechaRealInicialVigenteInciso = resultadoFecha.getFechaVigenciaIniRealInciso();
			
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return fechaRealInicialVigenteInciso;
	}
	
	//joksrc obtiene fecha emision por inciso.
	public Date obtenerFechaEmisionPorInciso(Long idIncisoContinuity){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_POLIZAS.GETFECHAEMISIONINCISO";
		Date fechaEmisionDelInciso = new Date();
		
		try{
			String atributos = "fechaRealEmisionPoliza";
			String columnaBd = "VIGENCIAEMISION";
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(IncisoSiniestroDTO.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("IDCONTINUITY", idIncisoContinuity);
			
			IncisoSiniestroDTO resultadoFecha =(IncisoSiniestroDTO)spHelper.obtieneResultadoSencillo();
			fechaEmisionDelInciso = resultadoFecha.getFechaRealEmisionPoliza();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		return fechaEmisionDelInciso;
		
	}
	
	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.dao.bitemporal.siniestros.PolizaSiniestroDao#buscarIncisosPoliza(mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO, java.util.Date)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<IncisoPolizaDTO> buscarIncisosPoliza(String numeroPoliza, String nombreAsegurado, String numeroSerie, 
			Integer numeroInciso, Date fechaOcurridoSiniestro, Short tipoPoliza) {
		
	    /// IMPORTANTE: si se realiza algún cambio en el stored procedure de 
	    /// MIDAS.PKGSIN_POLIZAS.buscarIncisos notificar a Daniel Contreras 0daniel.contreras@afirme.com
	    /// ya que estos impactan en el módulo de SPV. Gracias.
		
		List<IncisoPolizaDTO> listResultado = new ArrayList<IncisoPolizaDTO> (1);
		StoredProcedureHelper storedHelper = null;
		String spName = " MIDAS.PKGSIN_POLIZAS.buscarIncisos";

		try {
			String propiedades = "continuityId,numeroPoliza,numPolizaSeycos,numeroInciso,validoDesde,validoHasta,nombreContratante,numeroSerie,marca,tipoVehiculo,placa,modeloVehiculo,estatusInciso";
		    String columnasBaseDatos = "INCISOCONTINUTY_ID,NUMERO_POLIZA,POLIZA_SEYCOS,INCISO,FECHA_INI_VIGENCIA,FECHA_FIN_VIGENCIA,CONTRATANTE,NUMERO_SERIE,MARCA_VEHICULO,TIPO_VEHICULO,PLACA,MODELO_VEHICULO,ESTATUS_INCISO";
		    
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(IncisoPolizaDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
        	storedHelper.estableceParametro("p_numeroPoliza", numeroPoliza != null &&  !numeroPoliza.equals("") ?  numeroPoliza : null);
        	storedHelper.estableceParametro("p_numeroSerie", numeroSerie != null && !numeroSerie.equals("") ? numeroSerie : null);
        	storedHelper.estableceParametro("p_numeroPlaca", null);
        	storedHelper.estableceParametro("p_numeroMotor", null);
        	storedHelper.estableceParametro("p_nombreContratante", nombreAsegurado != null && !nombreAsegurado.equals("") ? nombreAsegurado : null) ;
        	storedHelper.estableceParametro("p_polizaSeycos", null);
        	storedHelper.estableceParametro("p_esPolizaSeycos", tipoPoliza == 1 ? Boolean.TRUE : null );
        	storedHelper.estableceParametro("p_numeroInciso", numeroInciso);       	
        	storedHelper.estableceParametro("p_clavePolizaMidas", null);
        	storedHelper.estableceParametro("p_esServicioPublico", null);
        	storedHelper.estableceParametro("p_esServicioPrivado", null);
        	storedHelper.estableceParametro("p_validoEn", fechaOcurridoSiniestro);
        	storedHelper.estableceParametro("p_fechaIniVigencia", null);
        	storedHelper.estableceParametro("p_fechaFinVigencia", null);
        	storedHelper.estableceParametro("p_tipoBusqueda", TIPO_BUSQUEDA.INCISOSINIESTROMIDASDTO.ordinal());
        	storedHelper.estableceParametro("p_numeroFolio", null);
        	
        	listResultado = storedHelper.obtieneListaResultados();

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		/*Query query = createQuery(numeroPoliza, numeroSerie, null, null, nombreAsegurado, numeroInciso, tipoPoliza == 0 ? true : false, null, null, false, false,
				null, null,	fechaOcurridoSiniestro, fechaOcurridoSiniestro, TIPO_BUSQUEDA.INCISOSINIESTROMIDASDTO);*/
		
		return listResultado;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<IncisoSiniestroDTO> buscarIncisosPolizaFiltro(IncisoSiniestroDTO filtro,  Date validoEn, Date recordFrom) {
		
	    /// IMPORTANTE: si se realiza algún cambio en el stored procedure de 
	    /// MIDAS.PKGSIN_POLIZAS.buscarIncisos notificar a Daniel Contreras 0daniel.contreras@afirme.com
	    /// ya que estos impactan en el módulo de SPV. Gracias.
		
		List<IncisoSiniestroDTO> listResultado = new ArrayList<IncisoSiniestroDTO> (1);
		StoredProcedureHelper storedHelper = null;
		String spName = " MIDAS.PKGSIN_POLIZAS.buscarIncisos";

		try {
			String propiedades = "incisoContinuityId,numeroPoliza,cvePolizaMidas,numPolizaSeycos,idToPoliza,claveEstatus,fechaIniVigencia,fechaFinVigencia,numeroInciso,validOn,recordFrom,nombreContratante,numeroSerie,marca,tipoVehiculo,placa,numeroMotor,modeloVehiculo,estatus,esSeguroObligatorio";
		    String columnasBaseDatos = "INCISOCONTINUTY_ID,NUMERO_POLIZA,CLAVE_MIDAS,POLIZA_SEYCOS,IDTOPOLIZA,ESTATUS_INCISO,FECHA_INI_VIGENCIA,FECHA_FIN_VIGENCIA,INCISO,VALID_ON,RECORD_FROM,CONTRATANTE,NUMERO_SERIE,MARCA_VEHICULO,TIPO_VEHICULO,PLACA,NUMERO_MOTOR,MODELO_VEHICULO,ESTATUS_INCISO,ES_SEGURO_OBLIGATORIO";
		    	
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(IncisoSiniestroDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
        	storedHelper.estableceParametro("p_numeroPoliza", filtro.getNumeroPoliza() != null &&  !filtro.getNumeroPoliza().equals("") ?  filtro.getNumeroPoliza() : null);
        	storedHelper.estableceParametro("p_numeroSerie", filtro.getAutoInciso() != null && filtro.getAutoInciso().getNumeroSerie() != null && !filtro.getAutoInciso().getNumeroSerie().equals("") ? filtro.getAutoInciso().getNumeroSerie() :null);
        	storedHelper.estableceParametro("p_numeroPlaca", filtro.getAutoInciso() != null && filtro.getAutoInciso().getPlaca() != null && !filtro.getAutoInciso().getPlaca().equals("") ? filtro.getAutoInciso().getPlaca() :null);
        	storedHelper.estableceParametro("p_numeroMotor", filtro.getAutoInciso() != null && filtro.getAutoInciso().getNumeroMotor() != null && !filtro.getAutoInciso().getNumeroMotor().equals("") ? filtro.getAutoInciso().getNumeroMotor() :null);
        	storedHelper.estableceParametro("p_nombreContratante", filtro.getNombreContratante() != null && !filtro.getNombreContratante().equals("") ? filtro.getNombreContratante()  : null) ;
        	storedHelper.estableceParametro("p_polizaSeycos", filtro.getNumPolizaSeycos());
        	storedHelper.estableceParametro("p_esPolizaSeycos", filtro.getBusquedaPolizaSeycos() != null && filtro.getBusquedaPolizaSeycos() == Boolean.TRUE ? filtro.getBusquedaPolizaSeycos() : null);
        	storedHelper.estableceParametro("p_numeroInciso", filtro.getNumeroInciso());       	
        	storedHelper.estableceParametro("p_clavePolizaMidas", filtro.getCvePolizaMidas());
        	storedHelper.estableceParametro("p_esServicioPublico", filtro.getServicioPublico() != null && filtro.getServicioPublico() == Boolean.TRUE ? filtro.getServicioPublico() : null);
        	storedHelper.estableceParametro("p_esServicioPrivado", filtro.getServicioParticular() != null && filtro.getServicioParticular() == Boolean.TRUE ? filtro.getServicioParticular() : null);
        	storedHelper.estableceParametro("p_validoEn", validoEn);
        	storedHelper.estableceParametro("p_fechaIniVigencia", filtro.getFechaIniVigencia());
        	storedHelper.estableceParametro("p_fechaFinVigencia", filtro.getFechaFinVigencia());
        	storedHelper.estableceParametro("p_tipoBusqueda", TIPO_BUSQUEDA.INCISOSINIESTRODTO.ordinal());
        	storedHelper.estableceParametro("p_numeroFolio", filtro.getNumeroFolio());
        	listResultado = storedHelper.obtieneListaResultados();

        	

		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		/*Query query = createQuery(filtro.getNumeroPoliza(), filtro.getAutoInciso() != null ? filtro.getAutoInciso().getNumeroSerie() :null,  
				filtro.getAutoInciso() != null ? filtro.getAutoInciso().getPlaca() :null, 
				filtro.getAutoInciso() != null ? filtro.getAutoInciso().getNumeroMotor() :null, filtro.getNombreContratante(),
				filtro.getNumeroInciso(), filtro.getBusquedaPolizaSeycos(), filtro.getNumPolizaSeycos(), filtro.getCvePolizaMidas(),
				filtro.getServicioPublico(), filtro.getServicioParticular(), filtro.getFechaIniVigencia(), filtro.getFechaFinVigencia(),
				validoEn, recordFrom, TIPO_BUSQUEDA.INCISOSINIESTRODTO );*/

		
		return listResultado;
	}
	
	
	/**
	 * Metodo que busca Incisos de una Poliza que cumplan con cierto criterio de busqueda.
	 * Se retornan incisos de Polizas Vigentes y No Vigentes
	 * @param filtro
	 * @param validoEn
	 * @param recordFrom
	 * @param vigente
	 * @return
	 */
	
	@Deprecated
	@SuppressWarnings("rawtypes")
	private Query createQuery(String numeroPoliza, String numeroSerie, String numeroPlaca, String numeroMotor, String nombreContratante,
			Integer numeroInciso, boolean esPolizaSeycos, String polizaSeycos, Integer clavePolizaMidas,
			boolean esServicioPublico, boolean esServicioPrivado, Date fechaIniVigencia, Date fechaFinVigencia,	 		
			Date validoEn, Date recordFrom, TIPO_BUSQUEDA tipoBusqueda ){
		
		Query query 							= null;
		StringBuilder queryString 				= new StringBuilder();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		
		if (tipoBusqueda.equals(TIPO_BUSQUEDA.INCISOSINIESTRODTO)) {
			queryString.append("SELECT new mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO (")
			.append(" incisob.continuity.id, ")
			.append(" func('CONCAT', TRIM(poliza.codigoProducto), func('CONCAT', TRIM(poliza.codigoTipoPoliza), func('CONCAT', '-', func('CONCAT',  func('LPAD',poliza.numeroPoliza,8,0), func('CONCAT', '-', func('LPAD',poliza.numeroRenovacion,2,0)))))),  ")
			.append(" poliza.numeroPoliza, ")
			.append(" (CASE WHEN ")
			.append(" poliza.numeroPolizaSeycos is not null THEN func('CONCAT', FUNC('LPAD',poliza.idCentroEmisor,3,0), func('CONCAT', '-', func('CONCAT',  func('LPAD',poliza.numeroPolizaSeycos,10,0), func('CONCAT', '-', func('LPAD',poliza.numeroRenovacion,2,0))))) ")
			.append(" ELSE ' ' END),  ")
			.append(" poliza.idToPoliza, poliza.claveEstatus, ")
			.append(" cotizacionb.value.fechaInicioVigencia, cotizacionb.value.fechaFinVigencia, ")
			.append(" incisob.continuity.numero, incisob.validityInterval.from,")
			.append(" incisob.recordInterval.from, cotizacionb.value.nombreContratante, autoincisob.value.numeroSerie, ")
			.append(" marca.descripcionMarcaVehiculo, tipo.descripcionTipoUsoVehiculo, autoincisob.value.placa,")
			.append(" autoincisob.value.numeroMotor, autoincisob.value.modeloVehiculo, ")
			.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerSituacionInciso', incisob.continuity.id, incisob.validityInterval.from, ")
			.append("      incisob.validityInterval.to, incisob.recordInterval.to, cotizacionb.continuity.numero, ")
			.append("      (SELECT negocio.seccionDTO.idToSeccion FROM NegocioSeccion negocio WHERE negocio.idToNegSeccion = autoincisob.value.negocioSeccionId ), :validOn), ")
			.append(" ( CASE WHEN poliza.cotizacionDTO.solicitudDTO.negocio.idToNegocio 	")
			.append(" 	IN (SELECT catalogo.descripcion FROM CatalogoValorFijoDTO catalogo")
			.append(" 		WHERE catalogo.id.idGrupoValores = :catNegocioSeguroObligatorio)")
			.append(" 	THEN true")
			.append(" 	ELSE false")
			.append(" END )")
			.append(" )");
			
		} else if (tipoBusqueda.equals(TIPO_BUSQUEDA.INCISOSINIESTROMIDASDTO)) {
			
			queryString.append("SELECT new mx.com.afirme.midas2.dto.siniestros.IncisoPolizaDTO(")
			.append(" incisob.continuity.id, ")
			.append(" func('CONCAT', TRIM(poliza.codigoProducto), func('CONCAT', TRIM(poliza.codigoTipoPoliza), func('CONCAT', '-', func('CONCAT',  func('LPAD',poliza.numeroPoliza,8,0), func('CONCAT', '-', func('LPAD',poliza.numeroRenovacion,2,0)))))),  ")
			.append(" (CASE WHEN ")
			.append(" poliza.numeroPolizaSeycos is not null THEN func('CONCAT', FUNC('LPAD',poliza.idCentroEmisor,3,0), func('CONCAT', '-', func('CONCAT',  func('LPAD',poliza.numeroPolizaSeycos,10,0), func('CONCAT', '-', func('LPAD',poliza.numeroRenovacion,2,0))))) ")
			.append(" ELSE ' ' END),  ")
			.append(" incisob.continuity.numero, ")
			.append(" incisob.validityInterval.from, ")
			.append(" incisob.validityInterval.to, ")
			.append(" cotizacionb.value.nombreContratante, autoincisob.value.numeroSerie, ")
			.append(" marca.descripcionMarcaVehiculo, tipo.descripcionTipoUsoVehiculo, autoincisob.value.placa,")
			.append(" autoincisob.value.modeloVehiculo, ")
			.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerSituacionInciso', incisob.continuity.id, incisob.validityInterval.from, ")
			.append("      incisob.validityInterval.to, incisob.recordInterval.to, cotizacionb.continuity.numero, ")
			.append("      (SELECT negocio.seccionDTO.idToSeccion FROM NegocioSeccion negocio WHERE negocio.idToNegSeccion = autoincisob.value.negocioSeccionId ), :validOn) ")
			.append(" )");			
		}
		
		queryString.append(" FROM BitemporalInciso incisob")
		.append(" JOIN BitemporalCotizacion cotizacionb ON  cotizacionb.continuity.id =  incisob.continuity.cotizacionContinuity.id")
		.append(" JOIN BitemporalAutoInciso autoincisob ON  autoincisob.continuity.incisoContinuity.id =  incisob.continuity.id")
		.append(" JOIN PolizaDTO poliza on poliza.cotizacionDTO.idToCotizacion = cotizacionb.continuity.numero")
		.append(" JOIN MarcaVehiculoDTO marca ON marca.idTcMarcaVehiculo = autoincisob.value.marcaId")
		.append(" JOIN TipoUsoVehiculoDTO tipo ON tipo.idTcTipoUsoVehiculo =  autoincisob.value.tipoUsoId");
		
		if (esServicioPrivado || esServicioPublico) {
			
			queryString.append(" JOIN NegocioSeccion seccion ON seccion.idToNegSeccion = autoincisob.value.negocioSeccionId ");			
		}
		
		queryString.append(" WHERE incisob.recordStatus = :recordStatus ")
		.append(" AND incisob.id = (SELECT MAX(mbitemporalInciso.id) FROM BitemporalInciso mbitemporalInciso ")
							.append(" WHERE mbitemporalInciso.continuity.id = incisob.continuity.id")
							.append(" AND mbitemporalInciso.recordStatus = :recordStatus ")
							.append(" AND :validOn >= mbitemporalInciso.validityInterval.from  ");
				/* queryString.append(" AND (:knownOn > mbitemporalInciso.recordInterval.from  ")
				 				.append( " OR ( :knownOn <= mbitemporalInciso.recordInterval.from ")
				 					.append(" AND EXISTS(SELECT cotizacion.idToCotizacion FROM CotizacionDTO cotizacion" +
				 										" WHERE cotizacion.solicitudDTO.tieneCartaCobertura = 1 " +
				 										" AND cotizacion.idToCotizacion = cotizacionb.continuity.numero)))");*/
		
		if (fechaIniVigencia != null) {
			queryString.append(" AND :fechaIniVigencia >= mbitemporalInciso.validityInterval.from ");
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaIniVigencia", fechaIniVigencia);
		}
		
		if (fechaFinVigencia!= null ) {
			queryString.append(" AND :fechaFinVigencia <= mbitemporalInciso.validityInterval.to  ");
			Utilerias.agregaHashLista(listaParametrosValidos, "fechaFinVigencia", fechaFinVigencia);
		}
		
		queryString.append(" )");

		
		queryString.append(" AND cotizacionb.id = (SELECT MAX(mbitemporalCotizacion.id) FROM BitemporalCotizacion mbitemporalCotizacion ")
				.append(" WHERE mbitemporalCotizacion.continuity.id = cotizacionb.continuity.id")
				.append(" AND mbitemporalCotizacion.recordStatus = :recordStatus ")
				.append(" AND :validOn >= mbitemporalCotizacion.validityInterval.from  ");
			/*queryString.append(" AND (:knownOn > mbitemporalCotizacion.recordInterval.from  ")
						.append( " OR ( :knownOn <= mbitemporalCotizacion.recordInterval.from ")
								.append(" AND EXISTS(SELECT cotizacion.idToCotizacion FROM CotizacionDTO cotizacion" +
						" WHERE cotizacion.solicitudDTO.tieneCartaCobertura = 1 " +
						" AND cotizacion.idToCotizacion = cotizacionb.continuity.numero)))");	*/	
		queryString.append(" )");

	
		queryString.append(" AND autoincisob.id = (SELECT MAX(mbitemporalAutoInciso.id) FROM BitemporalAutoInciso mbitemporalAutoInciso ")
				.append(" WHERE mbitemporalAutoInciso.continuity.id = autoincisob.continuity.id")
				.append(" AND mbitemporalAutoInciso.recordStatus = :recordStatus ")
				.append(" AND :validOn >= mbitemporalAutoInciso.validityInterval.from  ");
			/*queryString.append(" AND (:knownOn > mbitemporalAutoInciso.recordInterval.from  ")
						.append( " OR ( :knownOn <= mbitemporalAutoInciso.recordInterval.from ")
							.append(" AND EXISTS(SELECT cotizacion.idToCotizacion FROM CotizacionDTO cotizacion" +
												" WHERE cotizacion.solicitudDTO.tieneCartaCobertura = 1 " +
												" AND cotizacion.idToCotizacion = cotizacionb.continuity.numero)))");*/
		queryString.append(" )");
		

		if(!StringUtils.isEmpty(numeroSerie)){
			queryString.append(" AND UPPER(autoincisob.value.numeroSerie) LIKE :numeroSerie");
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroSerie", "%" + numeroSerie + "%");
		}
		
		if(!StringUtils.isEmpty(numeroPlaca)){
			queryString.append(" AND UPPER(autoincisob.value.placa) LIKE :placa");
			Utilerias.agregaHashLista(listaParametrosValidos, "placa", "%" + numeroPlaca+ "%");	
		}
		
		if( !StringUtils.isEmpty(numeroMotor)){				
			queryString.append(" AND UPPER(autoincisob.value.numeroMotor) LIKE :numeroMotor");
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroMotor", "%" + numeroMotor + "%");
		}
			
		
		
		if(!StringUtils.isEmpty(nombreContratante)){
			queryString.append(" AND UPPER(cotizacionb.value.nombreContratante) LIKE :nombreContratante");
			Utilerias.agregaHashLista(listaParametrosValidos, "nombreContratante",  "%" + nombreContratante + "%");
		}
		
		if (!StringUtils.isEmpty(numeroPoliza)) {
			if(!esPolizaSeycos){
				String[] elementosNumPoliza = numeroPoliza.split("-");
				
				if(elementosNumPoliza.length ==3){
					
					String codigoProducto 	=  StringUtils.rightPad(elementosNumPoliza[0].substring(0,2),8);
					String codigoTipoPoliza = StringUtils.rightPad(elementosNumPoliza[0].substring(2),8);
					int numPoliza 		= Integer.parseInt( elementosNumPoliza[1] );
					int numeroRenovacion 	= Integer.parseInt(  elementosNumPoliza[2]);
				
					queryString.append(" AND poliza.codigoProducto = :codigoProducto ")
					.append(" AND poliza.codigoTipoPoliza = :codigoTipoPoliza ")
					.append(" AND poliza.numeroPoliza = :numeroPoliza")
					.append(" AND poliza.numeroRenovacion = :numeroRenovacion ");
					
					Utilerias.agregaHashLista(listaParametrosValidos, "codigoProducto", codigoProducto);
					Utilerias.agregaHashLista(listaParametrosValidos, "codigoTipoPoliza", codigoTipoPoliza);
					Utilerias.agregaHashLista(listaParametrosValidos, "numeroPoliza", numPoliza);
					Utilerias.agregaHashLista(listaParametrosValidos, "numeroRenovacion", numeroRenovacion);					
					
				}else{
					queryString.append(" AND poliza.numeroPoliza = :numeroPoliza ") ;
					Utilerias.agregaHashLista(listaParametrosValidos, "numeroPoliza", Integer.parseInt(numeroPoliza));
					
				}
			}else if(esPolizaSeycos){
				queryString.append(" AND poliza.clavePolizaSeycos LIKE :numeroPolizaSeycos");
				Utilerias.agregaHashLista(listaParametrosValidos, "numeroPolizaSeycos", "%" + numeroPoliza + "%" );
			}
		}
		//Clave Poliza Seycos		
		if(!StringUtils.isEmpty(polizaSeycos)){
			String[] elementosNumPolizaSeycos = polizaSeycos.split("-");
			
			if(elementosNumPolizaSeycos.length ==3){
				
				int idCentroEmisor 	=  Integer.parseInt(elementosNumPolizaSeycos[0]);
				int numeroPolizaSeycos 		= Integer.parseInt(elementosNumPolizaSeycos[1] );
				int numeroRenovacion 	= Integer.parseInt(  elementosNumPolizaSeycos[2]);
			
				queryString.append(" AND poliza.idCentroEmisor = :idCentroEmisor ")
				.append(" AND poliza.numeroPolizaSeycos = :numeroPolizaSeycos")
				.append(" AND poliza.numeroRenovacion = :numeroRenovacion ");
				
				Utilerias.agregaHashLista(listaParametrosValidos, "idCentroEmisor", idCentroEmisor);
				Utilerias.agregaHashLista(listaParametrosValidos, "numeroPolizaSeycos", numeroPolizaSeycos);
				Utilerias.agregaHashLista(listaParametrosValidos, "numeroRenovacion", numeroRenovacion);
				
			}else{
				queryString.append(" AND poliza.numeroPolizaSeycos = :numeroPolizaSeycos ");
				Utilerias.agregaHashLista(listaParametrosValidos, "numeroPolizaSeycos", Integer.parseInt(polizaSeycos));
			}
		}

		//Clave Poliza Midas
		if (clavePolizaMidas != null) {
			queryString.append(" AND poliza.numeroPoliza = :CvePolizaMidas");
			Utilerias.agregaHashLista(listaParametrosValidos,"CvePolizaMidas", clavePolizaMidas);
		}

		
		if (numeroInciso!= null && numeroInciso> 0) {
			queryString.append(" AND incisob.continuity.numero = :numeroInciso ");
			Utilerias.agregaHashLista(listaParametrosValidos, "numeroInciso", numeroInciso);
		}
		
		if (esServicioPrivado) {
			queryString.append(" AND seccion.seccionDTO.servicioPublico = 0 ");
		}
		
		if (esServicioPublico) {
			queryString.append(" AND seccion.seccionDTO.servicioPublico = 1 ");
		}
		

		//Utilerias.agregaHashLista(listaParametrosValidos, "knownOn", recordFrom);
		Utilerias.agregaHashLista(listaParametrosValidos, "validOn", validoEn);
		Utilerias.agregaHashLista(listaParametrosValidos, "recordStatus", RecordStatus.NOT_IN_PROCESS);
		
		if (tipoBusqueda.equals(TIPO_BUSQUEDA.INCISOSINIESTRODTO)) { 
			Utilerias.agregaHashLista(listaParametrosValidos, "catNegocioSeguroObligatorio", CatalogoValorFijoDTO.IDGRUPO_NEGOCIO_SEGURO_OBLIGATORIO);
		}
		
		queryString.append(" ORDER BY incisob.validityInterval.to DESC, incisob.validityInterval.from DESC, incisob.continuity.numero ASC ");

		/** **/
		
		query = entityManager.createQuery(queryString.toString());
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ProgramaPagoDTO> obtenerProgramasPago(Long idToPoliza, Integer numeroInciso, Date fechaReporteSiniestro){
		String spName = "MIDAS.pkgint_siniestros.cprogpagxinciso"; //TODO cambiar por el nombreStoredProc real
		StoredProcedureHelper storedHelper = null;
		List<ProgramaPagoDTO> programasPagoList = null;
		try{
			storedHelper = new StoredProcedureHelper(spName,
					StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
			.estableceMapeoResultados(
					ProgramaPagoDTO.class.getCanonicalName(),
					"numeroPrograma," +
					"documentoOrigen," +
					"tipoEndoso," +
					"numeroRecibos," +
					"idCotizacionSeycos",
					"id_prog_pago," +
					"origen," + 
					"tipoendoso," + 
					"num_recibos," + 
					"idtopoliza"	);
			storedHelper.estableceParametro("pfecha", fechaReporteSiniestro);
			storedHelper.estableceParametro("pidtopoliza", idToPoliza);
			storedHelper.estableceParametro("pnuminciso", numeroInciso);
			programasPagoList = storedHelper.obtieneListaResultados();
		}catch(Exception e){
			LogDeMidasInterfaz.log(
					"Excepcion general en PolizaSiniestroDAO.obtenerProgramasPago..."
							+ this, Level.WARNING, e);
		}
		return programasPagoList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<ReciboProgramaPagoDTO> obtenerRecibosPrograma(Long idCotizacionSeycos, Long numeroPrograma, Date fechaReporteSiniestro){
		final String spName = "MIDAS.pkgint_siniestros.crecxprogpago"; 
		StoredProcedureHelper storedHelper = null;
		List<ReciboProgramaPagoDTO> recibosProgramaPagoList = null;
		try{
			storedHelper = new StoredProcedureHelper(spName,StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper
			.estableceMapeoResultados(
					ReciboProgramaPagoDTO.class.getCanonicalName(),
					"numeroRecibo," +
					"folio," +
					"fechaVencimiento," +
					"fechaFinProrroga," +
					"fechaInicioCobertura," +
					"fechaFinCobertura," +
					"situacion",
					"num_exhibicion," + 
					"num_folio_rbo," + 
					"f_vencimiento," + 
					"f_prorroga," + 
					"f_cubre_desde," + 
					"f_cubre_hasta," + 
					"desc_valor"	); 
			storedHelper.estableceParametro("pfecha", fechaReporteSiniestro);
			storedHelper.estableceParametro("pidtopoliza", idCotizacionSeycos);
			storedHelper.estableceParametro("pidprogpago", numeroPrograma);
			recibosProgramaPagoList = storedHelper.obtieneListaResultados();
		}catch(Exception e){
			LogDeMidasInterfaz.log(
					"Excepcion general en PolizaSiniestroDAO.obtenerProgramasPago..."
							+ this, Level.WARNING, e);
		}
		return recibosProgramaPagoList;
	}
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	@Deprecated
	public String obtenerMotivoCobranza(Long idToPoliza, BigDecimal idToSeccion, Integer numeroInciso, Date fechaReporteSiniestro) {
		
          String spName 								= "MIDAS.pkgint_siniestros.csitpolinciso"; 
          String resultado 								= "";
          StoredProcedureHelper storedHelper 			= null;
          String propiedades 							= "";
          String columnasBaseDatos 						= "";
          List<EstatusCobranzaDTO> listEstatusCobranza 	= null;
           
          try {
        	  
        	  propiedades = "fechaConsulta,idToPoliza,idtoseccion,numInciso,situacion";
        	  columnasBaseDatos = "f_consulta,idtopoliza,idtoseccion,numinciso,situacion";
              storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
              storedHelper.estableceMapeoResultados(EstatusCobranzaDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
              
              storedHelper.estableceParametro("pfecha", fechaReporteSiniestro);
              storedHelper.estableceParametro("pidtopoliza", idToPoliza);
              storedHelper.estableceParametro("pidtoseccion", idToSeccion);
              storedHelper.estableceParametro("pnuminciso", numeroInciso);
              
              listEstatusCobranza = storedHelper.obtieneListaResultados();
              
              if(listEstatusCobranza != null && listEstatusCobranza.size()>0){
            	  resultado = listEstatusCobranza.get(0).getSituacion();
              }
               
              
     
          } catch (Exception e) {
               String descErr = null;
               if (storedHelper != null) {
                     descErr = storedHelper.getDescripcionRespuesta();
                     LogDeMidasInterfaz.log(
                                 "Excepcion general en obtenerMotivoCobranza..."+descErr
                                             , Level.WARNING, e);    
               }
          }
          
          return resultado;
    }
	
	@Override
	public String obtenerDescripcionMotivoCobranza(Long idToPoliza, BigDecimal idToSeccion, Integer numeroInciso, Date fechaReporteSiniestro) {

		Query query = null;		
		StringBuilder queryString 				= new StringBuilder();
		
		// # OBTENER LISTA DE ReporteCabina DONDE IDTOPOLIZA == NULL Y IDTOSOLICITUD != NULL
		queryString.append(" SELECT MIDAS.APP_MOVILES_AJUSTADORES.getDescripcionProcede(?1,?2,?3,?4) FROM DUAL ");
		
		query = entityManager.createNativeQuery(queryString.toString());
		
		query.setParameter(1, polizaInterfazFacade.getConvertion(
				"POLIZA", idToPoliza.toString() + "|0"));
		query.setParameter(2, polizaInterfazFacade.getConvertion(
				PolizaFacadeRemote.CONV_TYPE_LINE, String.valueOf(idToSeccion)));
		query.setParameter(3, numeroInciso);
		query.setParameter(4, fechaReporteSiniestro);
		
		return (String)query.getSingleResult();		
	}
	



	@SuppressWarnings("unchecked")
	@Override
	public List<BitemporalInciso> obtenerIncisoCartaCobertura(Long idToCotizacion, String numeroSerie) {
		
		Query query  = null;
		Date hoy     = new Date();
		
		Date hoyMenos = DateUtils.add(hoy, Indicador.Days , -1);

		// # OTENER BITEMPORAL
		String sQuery = " SELECT mInciso.incisos  " +
						" FROM   IncisoContinuity mInciso" +
						" JOIN   mInciso.autoIncisoContinuity autoc " +
						" JOIN   mInciso.cotizacionContinuity cot" +
						" JOIN   autoc.autoIncisos autob " +
						" where  autob.value.numeroSerie = :numeroSerie " +
						" AND    :hoyMenos >= autob.validityInterval.from AND :hoyMenos < autob.validityInterval.to "+
						" AND    :hoy      >= autob.recordInterval.from   AND :hoy      < autob.recordInterval.to   "+
						" AND    autob.recordStatus = :estatus "+
						" AND    cot.numero = :idToCotizacion ";

		
		query = this.entityManager.createQuery(sQuery);
		
		query.setParameter("idToCotizacion", idToCotizacion);
		query.setParameter("numeroSerie"   , numeroSerie);
		query.setParameter("hoyMenos"      , hoyMenos );
		query.setParameter("estatus"       , RecordStatus.NOT_IN_PROCESS );
		query.setParameter("hoy"           , hoy );
		
		List<BitemporalInciso> lBitemporal =  query.getResultList();
		
		return lBitemporal;
	}



	@SuppressWarnings({"rawtypes","unchecked"})
	@Override
	public List<ReporteCabina> obtenerSiniestrosSinPoliza() {

		Query query = null;		
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		StringBuilder queryString 				= new StringBuilder();
		
		// # OBTENER LISTA DE ReporteCabina DONDE IDTOPOLIZA == NULL Y IDTOSOLICITUD != NULL
		queryString.append(" SELECT reporteCabina FROM ReporteCabina reporteCabina ")
		.append(" LEFT JOIN reporteCabina.poliza poliza ")
		.append(" where reporteCabina.idToSolicitud IS NOT NULL AND poliza.idToPoliza  IS NULL ")
		.append("AND reporteCabina.estatus NOT IN (:estatusRechazado, :estatusCancelado) ");		
		
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusRechazado", EstatusReporteCabina.CANCELADO.getValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusCancelado", EstatusReporteCabina.RECHAZADO.getValue());
		
		query = entityManager.createQuery(queryString.toString());
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return (List<ReporteCabina>) query.getResultList();
	}
	
	
	
	@SuppressWarnings({"rawtypes","unchecked"})
	@Override
	public List<BitemporalInciso> obtenerUltimoBitemporalInciso(Integer idToCotizacion, String numeroSerie) {

		Query query = null;		
		StringBuilder queryString 				= new StringBuilder();		
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		
		queryString.append("SELECT incisob FROM BitemporalInciso incisob ")
		.append(" JOIN incisob.continuity incisoContinuity ")
		.append(" JOIN incisoContinuity.cotizacionContinuity cotizacionContinuity ")
		.append(" WHERE incisob.recordStatus = :recordStatus ")
		.append(" AND cotizacionContinuity.numero = :idToCotizacion")
		.append(" AND incisob.id = (SELECT MAX(mbitemporalInciso.id) FROM BitemporalInciso mbitemporalInciso ")
		.append(" WHERE mbitemporalInciso.continuity.id = incisob.continuity.id)")
		.append(" AND EXISTS(SELECT autoincisob.id FROM BitemporalAutoInciso autoincisob")
		   .append(" JOIN autoincisob.continuity autoIncisoContinuity")
		   .append(" WHERE UPPER(autoincisob.value.numeroSerie) LIKE '%" + numeroSerie + "%'")
		   .append(" AND autoIncisoContinuity.incisoContinuity.id = incisoContinuity.id ")
		   .append(" AND autoincisob.recordStatus = :recordStatus ")
		   .append(" AND autoincisob.id = (SELECT MAX(mbitemporalAutoInciso.id) FROM BitemporalAutoInciso mbitemporalAutoInciso ")
		   						.append(" WHERE mbitemporalAutoInciso.continuity.id  = autoincisob.continuity.id))");		
		
		Utilerias.agregaHashLista(listaParametrosValidos, "recordStatus", RecordStatus.NOT_IN_PROCESS);
		Utilerias.agregaHashLista(listaParametrosValidos, "idToCotizacion", idToCotizacion);		
		
		query = entityManager.createQuery(queryString.toString());
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return (List<BitemporalInciso>) query.getResultList();
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override	
	public List<SolicitudDataEnTramiteDTO> buscarSolicitudesPoliza (String numeroSerie, BigDecimal idSolicitud) {
		
		Query query = null;		
		StringBuilder queryString 				= new StringBuilder();
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		
		queryString.append("SELECT model FROM SolicitudDataEnTramiteDTO model ")
		.append(" JOIN SolicitudDTO solicitud ON solicitud.idToSolicitud = model.idToSolicitud ")
		.append(" WHERE model.serie LIKE :numeroSerie ")
		.append(" AND solicitud.claveEstatus NOT IN (:estatusCancelado)")
		.append(" AND (:folio IS NOT NULL AND solicitud.idToSolicitud = (:folio) OR  :folio IS NULL) ");
		
		Utilerias.agregaHashLista(listaParametrosValidos, "numeroSerie", "%" + numeroSerie);
		Utilerias.agregaHashLista(listaParametrosValidos, "estatusCancelado", SolicitudDTO.Estatus.CANCELADA.getEstatus());
		Utilerias.agregaHashLista(listaParametrosValidos, "folio", idSolicitud);
		
		query = entityManager.createQuery(queryString.toString());
		
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return (List<SolicitudDataEnTramiteDTO>) query.getResultList();
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override	
	public BigDecimal getIdSeccionFromBitemporalTablesSeccion (Long idContinuidad){
		BigDecimal idToSeccion = new BigDecimal(0);
		int resultado = 0;
		//Start procedure execution
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_POLIZAS.getSeccionOnBitemporalidad";
		
		try{
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceParametro("pIdContinuidad", idContinuidad);
			resultado = spHelper.ejecutaActualizar();
			if (resultado != 0) {
				idToSeccion = new BigDecimal(resultado);
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		
		return idToSeccion;
	}
}

