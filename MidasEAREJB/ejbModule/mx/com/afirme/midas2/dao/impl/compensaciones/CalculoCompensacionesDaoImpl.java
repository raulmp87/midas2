package mx.com.afirme.midas2.dao.impl.compensaciones;


import java.math.BigDecimal;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.sql.DataSource;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.compensaciones.CalculoCompensacionesDao;
import mx.com.afirme.midas2.dto.compensaciones.CalculoCompensacionesDTO;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import javax.ejb.Stateless;

@Stateless
public class CalculoCompensacionesDaoImpl implements CalculoCompensacionesDao {

	private static final Logger LOG = LoggerFactory.getLogger(CalculoCompensacionesDaoImpl.class);

	@SuppressWarnings("unused")
	private DataSource dataSource;
	
	private SimpleJdbcCall procesarUtilidad;
	private JdbcTemplate jdbcTemplate;


	private final String CLASS_NAME = this.getClass().getName();
	@SuppressWarnings("unchecked")
	@Override
	public List<CalculoCompensacionesDTO> calcularCompensacion(long idPoliza) {
		// TODO Auto-generated method stub
		String methodName = CLASS_NAME + " :: calcularCompensacion(" + idPoliza + "):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa.", Level.INFO, null);
		List<CalculoCompensacionesDTO> calculo = new ArrayList<CalculoCompensacionesDTO>();
		StoredProcedureHelper storedHelper = null;
		
		try {
			storedHelper = new StoredProcedureHelper("MIDAS.PKG_COMPENSACIONES.calculoCompensaciones", "jdbc/MidasDataSource");
			
			storedHelper.estableceParametro("pIdToPoliza", String.valueOf(idPoliza));
			
			String[] propiedades = new String[] { "idToPoliza", 
												  "idEntidadPersona", 
												  "idCompensacion",
												  "importe"};
			// resultados del cursor
			String[] columnas = new String[] { "IDTOPOLIZA", 
											   "ENTIDADPERSONA_ID", 
											   "COMPENSACION_ID",
											   "IMPORTE"};
			storedHelper.estableceMapeoResultados(CalculoCompensacionesDTO.class.getCanonicalName(), propiedades, columnas);
			calculo = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			e.printStackTrace();
			LogDeMidasEJB3.log(methodName + "e.getMessage(): " + e.getMessage(), Level.INFO, null);
			LogDeMidasEJB3.log(methodName + "codErr: " + codErr + ", descErr: " + descErr, Level.INFO, null);		
		}
		if(calculo.size()>0){
			LogDeMidasEJB3.log(methodName + "   Primer Registro: ", Level.INFO, null);
			LogDeMidasEJB3.log(methodName + "        idToPoliza: " + calculo.get(0).getIdToPoliza(), Level.INFO, null);
			LogDeMidasEJB3.log(methodName + "  idEntidadPersona: " + calculo.get(0).getIdEntidadPersona(), Level.INFO, null);
			LogDeMidasEJB3.log(methodName + "    idCompensacion: " + calculo.get(0).getIdCompensacion(), Level.INFO, null);
			LogDeMidasEJB3.log(methodName + "           importe: " + calculo.get(0).getImporte(), Level.INFO, null);
			LogDeMidasEJB3.log(methodName + "           importe: " + calculo.get(0).getImporte(), Level.INFO, null);
		}
		return calculo;
	}	

	@Override
	public int generarOrdenUtilidad(long idToPoliza, long entidadPersonaId, BigDecimal montoPago) {

		LOG.info(">> generarOrdenUtilidad()");
		
		StoredProcedureHelper storedHelper = null;
		int respuesta = 0;
		try {
			storedHelper = new StoredProcedureHelper("MIDAS.PKG_CA_ORDENPAGO.generarOrdenUtilidad", "jdbc/MidasDataSource");
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pIdEntidadPersona", entidadPersonaId);
			storedHelper.estableceParametro("pMontoPago", montoPago);			
			try {		
			storedHelper.obtieneResultadoSencillo();	
			}catch(Exception ex){				
			}		
			respuesta = storedHelper.getCodigoRespuesta();			
												
		} catch (Exception e) {
			LOG.error("Información del Error", e);
			respuesta = 1;
		}
		LOG.info("<< generarOrdenUtilidad()");
		return respuesta;
	}
	
	@Resource(name ="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {		
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	this.procesarUtilidad = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("MIDAS")
				.withCatalogName("PKG_CA_DAN_COMPENSACIONES")
				.withProcedureName("procesarUtilidad").declareParameters(		
							new SqlParameter("pIdToPoliza", Types.NUMERIC),
							new SqlParameter("pIdEntidadPersona", Types.NUMERIC),
						    new SqlParameter("pMontoPago", Types.NUMERIC),
						    new SqlParameter("pMontoTotal", Types.NUMERIC),
							new SqlOutParameter("pId_Cod_Resp",  Types.NUMERIC),
							new SqlOutParameter("pDesc_Resp", Types.VARCHAR)
							);
	}
	
	@Override
	public int provisionarPorUtilidadCompensacion(long idToPoliza, long entidadPersonaId, BigDecimal montoPago,BigDecimal montoTotal) {
		int respuesta = 0;
		try{
				MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
				sqlParameter.addValue("pIdToPoliza", idToPoliza);
				sqlParameter.addValue("pIdEntidadPersona", entidadPersonaId);
				sqlParameter.addValue("pMontoPago", montoPago);
				sqlParameter.addValue("pMontoTotal", montoTotal);
				Map<String, Object> execute = this.procesarUtilidad.execute(sqlParameter);				
				respuesta = ((BigDecimal) execute.get("pId_Cod_Resp")).intValue();

		}catch (Exception e) {
			LOG.error("Excepcion general en Provisionar", e);
			respuesta = 1;
		}
			
		return respuesta;
	}
}
