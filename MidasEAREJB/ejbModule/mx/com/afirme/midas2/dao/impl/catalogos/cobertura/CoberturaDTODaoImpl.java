package mx.com.afirme.midas2.dao.impl.catalogos.cobertura;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.catalogos.cobertura.CoberturaDTODao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class CoberturaDTODaoImpl  extends JpaDao<BigDecimal,CoberturaDTO> implements CoberturaDTODao  {
	
	protected EntidadService entidadService;

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}


	@Override
	public List<CoberturaDTO> getListarFiltrado(CoberturaDTO coberturaDTO,Boolean mostrarInactivos) {		
		Map<String, Object> map;
		String queryString = "";
		map = new HashMap<String, Object>();
		
		queryString = "select model from CoberturaDTO as model, " +
				"SubRamoDTO as sub  where model.subRamoDTO.idTcSubRamo=sub.idTcSubRamo ";		
		if (coberturaDTO.getIdTcRamo()!=null){
			queryString = queryString + "and sub.ramoDTO.idTcRamo = :idTcRamo ";
			map.put("idTcRamo", coberturaDTO.getIdTcRamo().intValue());
		}
		if (coberturaDTO.getCodigo()!=null && !StringUtils.isBlank(coberturaDTO.getCodigo())){
			queryString = queryString + "and model.codigo like '%" + coberturaDTO.getCodigo() + "%' ";
		}
		if (coberturaDTO.getVersion()!=null){
			queryString = queryString + "and model.version = :version ";
			map.put("version", coberturaDTO.getVersion());
		}
		if (coberturaDTO.getDescripcion()!=null && !StringUtils.isBlank(coberturaDTO.getDescripcion())){
			queryString = queryString + "and upper(model.descripcion) like upper('%" + coberturaDTO.getDescripcion() + "%') ";
		}
		if (coberturaDTO.getNombreComercial()!=null && !StringUtils.isBlank(coberturaDTO.getNombreComercial())){
			queryString = queryString + "and upper(model.nombreComercial) like upper('%" + coberturaDTO.getNombreComercial() + "%') ";
		}
		if (coberturaDTO.getClaveTipoSumaAsegurada()!=null){
			queryString = queryString + "and model.claveTipoSumaAsegurada = :claveTipoSumaAsegurada ";
			map.put("claveTipoSumaAsegurada", coberturaDTO.getClaveTipoSumaAsegurada());
		}
		if (coberturaDTO.getClaveDesglosaRiesgos()!=null){
			queryString = queryString + "and model.claveDesglosaRiesgos = :claveDesglosaRiesgos ";
			map.put("claveDesglosaRiesgos", coberturaDTO.getClaveDesglosaRiesgos());
		}
		if (coberturaDTO.getClaveImporteCero()!=null){
			queryString = queryString + "and model.claveImporteCero = :claveImporteCero ";
			map.put("claveImporteCero", coberturaDTO.getClaveImporteCero());
		}
		if (coberturaDTO.getSubRamoDTO().getIdTcSubRamo()!=null){
			queryString = queryString + "and model.subRamoDTO.idTcSubRamo = :subRamo ";
			map.put("subRamo", coberturaDTO.getSubRamoDTO().getIdTcSubRamo());
		}
		if(!mostrarInactivos) {
			queryString = queryString + " and model.claveEstatus <> :claveEstatus and model.claveActivo <> :claveActivo ";
			map.put("claveEstatus", 0);
			map.put("claveActivo", 0);
		}
		@SuppressWarnings({ "unchecked" })
		List<CoberturaDTO> coberturas = entidadService.executeQueryMultipleResult(queryString, map);
		return coberturas;
	}
	
	public List<CoberturaDTO> listarVigentesAutos(){
		Map<String, Object> map;
		String queryString = "";
		map = new HashMap<String, Object>();
		
		queryString = "select model from CoberturaDTO as model, " +
			"SubRamoDTO as sub  where model.subRamoDTO.idTcSubRamo=sub.idTcSubRamo " +
			"and sub.ramoDTO.idTcRamo = :idTcRamo " +
			"and model.claveEstatus <> 0 and " +
			"model.claveDesglosaRiesgos = 0 and model.claveImporteCero = 0 ";
		map.put("idTcRamo", 4);
		
		@SuppressWarnings({ "unchecked" })
		List<CoberturaDTO> coberturas = entidadService.executeQueryMultipleResult(queryString, map);
		return coberturas;
	}

}
