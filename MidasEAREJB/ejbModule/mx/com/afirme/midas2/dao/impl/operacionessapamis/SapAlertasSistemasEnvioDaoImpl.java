package mx.com.afirme.midas2.dao.impl.operacionessapamis;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.operacionessapamis.SapAlertasSistemasEnvioDao;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class SapAlertasSistemasEnvioDaoImpl implements SapAlertasSistemasEnvioDao {
	private EntidadService entidadService;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public SapAlertasistemasEnvio guardarEncabezadoAlertas(SapAlertasistemasEnvio encabezadoAlertas ) {
		return encabezadoAlertas = entidadService.save(encabezadoAlertas);
	}
	
	public SapAlertasistemasEnvio obtenerAlertas(String alertaBuscar){
		SapAlertasistemasEnvio alertas = entidadService.findById(SapAlertasistemasEnvio.class, Long.valueOf(alertaBuscar)); 
		entidadService.refresh(alertas);
		return alertas;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
}
