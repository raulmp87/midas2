package mx.com.afirme.midas2.dao.impl.bitemporal;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
@SuppressWarnings("rawtypes")
public class EntidadContinuityDaoImpl implements EntidadContinuityDao {
	
	private static final Logger LOG = Logger.getLogger(EntidadContinuityDaoImpl.class);
	
	@Override
	public <E extends Entidad> 
		Object persistAndReturnKey(E entity) {
			persist(entity);
			return entityManager.getEntityManagerFactory().getPersistenceUnitUtil().getIdentifier(entity);
	}

	@Override
	public <E extends Entidad>
		void persist(E entity) {
		
			entityManager.persist(entity);	
	}
	
	@Override
	public <E extends Entidad> void remove(E entity){
		entity = entityManager.merge(entity);
		entityManager.remove(entity);
	}
	
	@Override
	public <C extends EntidadContinuity> C update(C entity) {
		
		return entityManager.merge(entity);
	}
	
	@Override
	public <C extends EntidadContinuity, K> C findByKey(Class<C> continuityEntityClass, K key) {
		
		return (C) entityManager.find(continuityEntityClass, key);
	}
	
	@Override
	public <C extends EntidadContinuity, K> C getReference(Class<C> continuityEntityClass, K key) {
		
		return (C) entityManager.getReference(continuityEntityClass, key);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public <C extends EntidadContinuity> Collection<C> findByProperty(Class<C> continuityEntityClass, String propertyName, final Object value) {
		    LogDeMidasInterfaz.log("Entrando a findByProperty", Level.INFO, null);
		    
		    LogDeMidasInterfaz.log("continuityEntityClass => " + continuityEntityClass, Level.INFO, null);
		    LogDeMidasInterfaz.log("propertyName => " + propertyName, Level.INFO, null);
		    LogDeMidasInterfaz.log("value => " + value, Level.INFO, null);
		    
			final String queryString = "select model from "+continuityEntityClass.getSimpleName()+" model where model."
			+ propertyName + "= :propertyValue";
			LogDeMidasInterfaz.log("queryString => " + queryString, Level.INFO, null);
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			LogDeMidasInterfaz.log("query => " + query, Level.INFO, null);
			Collection<C> resultList = query.getResultList();
			
			LogDeMidasInterfaz.log("resultList => " + resultList, Level.INFO, null);
			
			
			return resultList;
	}
	
	@Override
	public <E extends Entidad> 
		void refresh(E entity) {
			
			entityManager.refresh(entity);	
	}

	/**
	 * Se declara este método pues el original (el que tiene todos los statements) se
	 * ampliara para poder solicitar tambien los cancelados
	 */
	@Override
	@SuppressWarnings("unchecked")
	public <B extends EntidadBitemporal> Collection<B> listarBitemporalsFiltrado(
			Class<B> bitemporalEntityClass, Map<String,Object> params, 
			DateTime validoEn, DateTime conocidoEn, boolean enProceso, String... orderByAttributes) {
		
		boolean obtenerCancelados = false;
		return listarBitemporalsFiltrado(bitemporalEntityClass, params, validoEn, conocidoEn, obtenerCancelados, enProceso, orderByAttributes);
	}

	public <B extends EntidadBitemporal> Collection<B> obtenerCancelados(Class<B> bitemporalEntityClass, 
			Long continuitiId, DateTime validoEn, DateTime fechaDeCancelacion){
		Collection<B> collectionCancelados = null;
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.id", continuitiId);
		
		collectionCancelados = obtenerCancelados(bitemporalEntityClass, validoEn, fechaDeCancelacion, params);
		
		return collectionCancelados;
	}
	
	@Override
	public <B extends EntidadBitemporal> Collection<B> obtenerCancelados(Class<B> bitemporalEntityClass, 
			DateTime validoEn, DateTime fechaDeCancelacion, Map<String, Object> params){
		Collection<B> collectionCancelados = null;
		
		boolean obtenerCancelados = true; 
		
		collectionCancelados = listarBitemporalsFiltrado(
			bitemporalEntityClass, params, validoEn, fechaDeCancelacion, obtenerCancelados, false);
		
		return collectionCancelados;
	}
	
	@SuppressWarnings("unchecked")
	private <B extends EntidadBitemporal> Collection<B> listarBitemporalsFiltrado(
			Class<B> bitemporalEntityClass, Map<String,Object> params, 
			DateTime validoEn, DateTime conocidoEn, boolean obtenerCancelados, boolean enProceso, String... orderByAttributes) {
		
	
		final StringBuilder queryString = new StringBuilder(
				"select model from " + bitemporalEntityClass.getSimpleName() + " model ");
		queryString.append(" where ");
		queryString.append(" :validOn >= model.validityInterval.from  and :validOn < model.validityInterval.to");
		if(enProceso){
			queryString.append(" and model.recordInterval.from is null and model.recordInterval.to is null ");
		}else{
			if(obtenerCancelados){
				queryString.append(" and :knownOn >= model.recordInterval.from and :knownOn = model.recordInterval.to ");
			}else{
				queryString.append(" and :knownOn >= model.recordInterval.from and :knownOn < model.recordInterval.to ");	
			}
		}
		
		boolean setParameters = false;
		int size = 0;
		int index = 0;
		if (params != null && !params.isEmpty()) {
			setParameters = true;
			size = params.size();
			for (String property : params.keySet()) {
				String propertyMap = getValidProperty(property);// checa si
																// tiene "." la
																// propiedad
				queryString.append(" and model." + property + "=:" + propertyMap);
				index++;
			}
		}

		if (orderByAttributes != null && orderByAttributes.length > 0) {
			size = orderByAttributes.length;
			index = 0;
			queryString.append(" ORDER BY ");
			for (String orderAttribute : orderByAttributes) {
				queryString.append("model." + orderAttribute);
				if (index != (size - 1)) {
					queryString.append(",");
				}
				index++;
			}
		}

		Query query = entityManager.createQuery(queryString.toString());
		
		query.setParameter("validOn", validoEn == null ? TimeUtils.current().toDate()
				: validoEn.toDate());
		if(!enProceso){
			query.setParameter("knownOn", conocidoEn == null ? TimeUtils.current().toDate()
				: conocidoEn.toDate());
		}

		if (setParameters) {
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
		
	}
		
	private void setQueryParametersByProperties(Query entityQuery,
			Map<String, Object> parameters) {
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>) it
					.next();
			String key = getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}
	}
	
	public static String getValidProperty(String property) {
		String validProperty = property;
		StringBuilder str = new StringBuilder("");
		if (property.contains(".")) {
			int i = 0;
			for (String word : property.split("\\.")) {
				if (i == 0) {
					word = word.toLowerCase();
				} else {
					word = StringUtils.capitalize(word);
				}
				str.append(word);
				i++;
			}
			validProperty = str.toString();
		}
		return validProperty;
	}

	@PersistenceContext
	protected EntityManager entityManager;

	@Override
	public List executeNativeQueryMultipleResult(String query) {
		Query nativeQuery = entityManager.createNativeQuery(query);
		return nativeQuery.getResultList();
	}

	@Override
	public Object executeNativeQuerySimpleResult(String query) {
		Query nativeQuery = entityManager.createNativeQuery(query);
		return nativeQuery.getSingleResult();
	}

	@Override
	public List executeQueryMultipleResult(String query, Map<String, Object> parameters) {
		Query entityQuery = entityManager.createQuery(query);
		this.setQueryParameters(entityQuery, parameters);
		return entityQuery.getResultList();
	}

	@Override
	public Object executeQuerySimpleResult(String query, Map<String, Object> parameters) {
		try {
			Query entityQuery = entityManager.createQuery(query);
			this.setQueryParameters(entityQuery, parameters);
			return entityQuery.getSingleResult();
		} catch (NoResultException e) {
			e.printStackTrace();
			return null;
		}
	}

	private void setQueryParameters(Query entityQuery,
			Map<String, Object> parameters) {
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while (it.hasNext()) {
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>) it
					.next();
			entityQuery.setParameter(pairs.getKey(), pairs.getValue());
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)	
	public void commitContinuity(Long cotizacionContinuityId, Date recordFrom) {

		String spName = "MIDAS.pkgAUT_Generales.spAUT_CommitBitemporal";
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pRecordTo", new Date(TimeUtils.END_OF_TIME));
			storedHelper.estableceParametro("pRecordFrom", recordFrom);
			storedHelper.estableceParametro("pContinuidadId", cotizacionContinuityId);
			
			storedHelper.ejecutaActualizar();
			
			entityManager.getEntityManagerFactory().getCache().evictAll();

		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)		
	public void rollBackContinuity(Long cotizacionContinuityId) {
		String spName = "MIDAS.pkgAUT_Generales.spAUT_RollBackBitemporal";
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pContinuidadId",
					cotizacionContinuityId);
			storedHelper.ejecutaActualizar();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)		
	public void rollBackCoberturas(Long seccionIncisoContinuityId) {
		String spName = "MIDAS.pkgAUT_Generales.spAUT_RollBackBTCoberturas";
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pContinuidadId",
					seccionIncisoContinuityId);
			storedHelper.ejecutaActualizar();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)		
	public void rollbackCondicionEspecial (Long continuityId , Integer tipo) {
		String spName = "MIDAS.pkgAUT_Generales.spAut_RollBackBTCondEspecial";
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pContinuidadId",continuityId);
			storedHelper.estableceParametro("pTipoCondicion",tipo);
			storedHelper.ejecutaActualizar();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
	}
	
	@Override
	public <B extends EntidadBitemporal> void updateBitemporal(Class<B> bitemporalEntityClass, Map<String,Object> params, Long id) {
		final StringBuilder queryString = new StringBuilder("update " + bitemporalEntityClass.getSimpleName() + " model set ");
		
		try {
			final StringBuilder queryUpdateString = new StringBuilder("");
			int index = 0;
			boolean setParameters = false;
			if (params != null && !params.isEmpty() && id != null) {
				setParameters = true;
				for (String property : params.keySet()) {
					String propertyMap = getValidProperty(property);
					queryUpdateString.append("model.value." + property + "=:" + propertyMap + ",");
					index++;
				}
				queryUpdateString.delete(queryUpdateString.length()-1,queryUpdateString.length());
				queryUpdateString.append(" where model.id= :id");
			}
			
			queryString.append(queryUpdateString);
			if(setParameters) {
				Query entityQuery = entityManager.createQuery(queryString.toString());
				
				params.put("id", id);
				this.setQueryParameters(entityQuery, params);
				
				entityQuery.executeUpdate();
			}
		} catch(NoResultException e){
			LOG.error(e.getMessage(), e);	
		}	
	}
}
