package mx.com.afirme.midas2.dao.impl.siniestros.sipac;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.sipac.ReportesLayoutSipacDAO;
import mx.com.afirme.midas2.domain.siniestros.sipac.LayoutSipac;
import mx.com.afirme.midas2.domain.siniestros.sipac.LoteLayoutSipac;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSubMarcaVehiculo;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSubMarcaVehiculo;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import org.apache.commons.lang.StringUtils;
import javax.persistence.criteria.Predicate;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSubMarcaVehiculo_;
import javax.persistence.criteria.Root;


@Stateless
public class ReportesLayoutSipacDAOImpl extends EntidadDaoImpl implements ReportesLayoutSipacDAO, Serializable{
	
	private static final long serialVersionUID = -140456779387192399L;
	
	private static Logger log = LoggerFactory.getLogger(ReportesLayoutSipacDAOImpl.class);
	private static final String LOTEENPROCESO = "1";
	
	@EJB
	private transient EntidadService entidadService;
	
	private String dateFormat = "dd/MM/yy";
	private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat);
	private String dateFormatTxt = "dd/MM/yyyy";
	private SimpleDateFormat simpleDateFormatTxt = new SimpleDateFormat(dateFormatTxt);
	
	private static final String AND = " and ";
	
	private void addCondition(StringBuilder queryString,String conditional){
		
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(AND);
	}
	
	@Override
	public InputStream generarArchivoTxt(List<LayoutSipac> list){
		InputStream is = null;
		try{	
			byte[] bytes = toArrayByte(list);
			is = new ByteArrayInputStream(bytes);
		}catch(Exception e){
			log.debug("Error al consultar los reportes SIPAC " + e);
		}
			return is;	
	}
	
	@Override
	public InputStream generarTxtByLote(Long idLote){
		List<LayoutSipac> list = this.findLayoutByIdLote(idLote);
		InputStream is = null;
		try{	
			byte[] bytes = toArrayByte(list);
			is = new ByteArrayInputStream(bytes);
		}catch(Exception e){
			log.debug("Error al consultar los reportes SIPAC " + e);
		}
			return is;	
	}
	
	private byte[] toArrayByte(List<LayoutSipac> list){
		byte[] bytes = null;
		try{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		StringBuilder sb = new StringBuilder();
		out.writeBytes("NumeroSecuencial	TipoOperacion	CiaDeudora	FolioOrden	TipoCaptura	SiniestroDeudor	FechaSiniestro	Pais	Estado	Municipio	PolizaDeudor	IncisoDeudor	"
				+ "PersDeudor	NombreResponsable	ApPaternoResponsable	ApMaternoResponsable	TipoTransporteDeudor	MarcaDeudor	TipoVehDeudor	SerieDeudor	ModeloDeudor	ColorVehDeudor	"
				+ "PlacaDeudor	AjustadorDeudora	ClaveAjustDeudora	Cobertura	CiaAcreedora	SiniestroAcreedor	ObsDeudor	TipoOrden	Origen	FechaExpedicion	FechaDictamen	FechaInicioJuicio	"
				+ "PolizaAcreedor	IncisoAcreedor	PersAfectado	NombreAfectado	ApPaternoAfectado	ApMaternoAfectado	TipoTransporteAfectado	MarcaAfectado	TipoVehAfectado	SerieAfectado	"
				+ "ModeloAfectado	ColorVehAfectado	PlacaAfectado	NombreLesionado	ApPaternoLesionado	ApMaternoLesionado	TipoLesionado	TipoPersonaLesionado	DomicilioLesionado	TelefonoLesionado	"
				+ "EdadLesionado	AjustadorAcreedora	ClaveAjustAcreedora	ObsAcreedor	DanosPreexistentes	DescrDPreexistentes	DanosLesiones	FolioLesionado	CircunstanciaDeu	CircunstanciaAcr	"
				+ "UsoVehiculoDeu	UsoVehiculoAcr\n");
		if(list != null)
			for (LayoutSipac item : list) {
				sb.append(validarSiNulo(item.getNumeroSecuencial()) + "\t");
				sb.append(validarSiNulo(item.getTipoOperacion()) + "\t");
				sb.append(validarSiNulo(item.getCiaDeudora()) + "\t");
				sb.append(validarSiNulo(item.getFolioOrden()) + "\t");
				sb.append(validarSiNulo(item.getTipoCaptura()) + "\t");
				sb.append(validarSiNulo(item.getSiniestroDeudor()) + "\t");
				sb.append(validarSiNulo(getFechaFormateada(item.getFechaSiniestro())));
				sb.append(validarSiNulo(item.getPais()) + "\t");
				sb.append(validarSiNulo(item.getEstado()) + "\t");
				sb.append(validarSiNulo(item.getMunicipio()) + "\t");
				sb.append(validarSiNulo(item.getPolizaDeudor()) + "\t");
				sb.append(validarSiNulo(item.getIncisoDeudor()) + "\t");
				sb.append(validarSiNulo(item.getPersDeudor()) + "\t");
				sb.append(validarSiNulo(item.getNombreResponsable()) + "\t");
				sb.append(validarSiNulo(item.getApPaternoResponsable()) + "\t");
				sb.append(validarSiNulo(item.getApMaternoResponsable()) + "\t");
				sb.append(validarSiNulo(item.getTipoTransporteDeudor()) + "\t");
				sb.append(validarSiNulo(item.getMarcaDeudor()) + "\t");
				sb.append(validarSiNulo(item.getTipoVehDeudor()) + "\t");
				sb.append(validarSiNulo(item.getSerieDeudor()) + "\t");
				sb.append(validarSiNulo(item.getModeloDeudor()) + "\t");
				sb.append(validarSiNulo(item.getColorVehDeudor()) + "\t");
				sb.append(validarSiNulo(item.getPlacaDeudor()) + "\t");
				sb.append(validarSiNulo(item.getAjustadorDeudora()) + "\t");
				sb.append(validarSiNulo(item.getClaveAjustDeudora()) + "\t");
				sb.append(validarSiNulo(item.getCobertura()) + "\t");
				sb.append(validarSiNulo(item.getCiaAcreedora()) + "\t");
				sb.append(validarSiNulo(item.getSiniestroAcreedor()) + "\t");
				sb.append(validarSiNulo(item.getObsDeudor()) + "\t");
				sb.append(validarSiNulo(item.getTipoOrden()) + "\t");
				sb.append(validarSiNulo(item.getOrigen()) + "\t");
				sb.append(validarSiNulo(getFechaFormateada(item.getFechaExpedicion())));
				sb.append(validarSiNulo(getFechaFormateada(item.getFechaDictamen())));
				sb.append(validarSiNulo(getFechaFormateada(item.getFechaInicioJuicio())));
				sb.append(validarSiNulo(item.getPolizaAcreedor()) + "\t");
				sb.append(validarSiNulo(item.getIncisoAcreedor()) + "\t");
				sb.append(validarSiNulo(item.getPersAfectado()) + "\t");
				sb.append(validarSiNulo(item.getNombreAfectado()) + "\t");
				sb.append(validarSiNulo(item.getApPaternoAfectado()) + "\t");
				sb.append(validarSiNulo(item.getApMaternoAfectado()) + "\t");
				sb.append(validarSiNulo(item.getTipoTransporteAfectado()) + "\t");
				sb.append(validarSiNulo(item.getMarcaAfectado()) + "\t");
				sb.append(validarSiNulo(item.getTipoVehAfectado()) + "\t");
				sb.append(validarSiNulo(item.getSerieAfectado()) + "\t");
				sb.append(validarSiNulo(item.getModeloAfectado()) + "\t");
				sb.append(validarSiNulo(item.getColorVehAfectado()) + "\t");
				sb.append(validarSiNulo(item.getPlacaAfectado()) + "\t");
				sb.append(validarSiNulo(item.getNombreLesionado()) + "\t");
				sb.append(validarSiNulo(item.getApPaternoLesionado()) + "\t");
				sb.append(validarSiNulo(item.getApMaternoLesionado()) + "\t");
				sb.append(validarSiNulo(item.getTipoLesionado()) + "\t");
				sb.append(validarSiNulo(item.getTipoPersonaLesionado()) + "\t");
				sb.append(validarSiNulo(item.getDomicilioLesionado()) + "\t");
				sb.append(validarConvertirBD(item.getTelefonoLesionado()) + "\t");
				sb.append(validarConvertirBD(item.getEdadLesionado()) + "\t");
				sb.append(validarSiNulo(item.getAjustadorAcreedora()) + "\t");
				sb.append(validarSiNulo(item.getClaveAjustAcreedora()) + "\t");
				sb.append(validarSiNulo(item.getObsAcreedor()) + "\t");
				sb.append(validarSiNulo(item.getDanosPreexistentes()) + "\t");
				sb.append(validarSiNulo(item.getDescrDpreexistentes()) + "\t");
				sb.append(validarSiNulo(item.getDanosLesiones()) + "\t");
				sb.append(validarSiNulo(item.getFolioLesionado()) + "\t");
				sb.append(validarSiNulo(item.getCircunstanciaDeu()) + "\t");
				sb.append(validarSiNulo(item.getCircunstanciaAcr()) + "\t");
				sb.append(validarSiNulo(item.getUsoVehiculoDeu()) + "\t");
				sb.append(validarSiNulo(item.getUsoVehiculoAcr()) + System.getProperty("line.separator"));
			}
		out.writeBytes(sb.toString());
		bytes = baos.toByteArray();
		}catch(Exception e){
			log.error("Ocurrio un error al generar el archivo: " + e);
		}
		return bytes; 
	}
	
	private String getFechaFormateada(Date fecha){
		if(fecha != null){
			return simpleDateFormatTxt.format(fecha) + "\t";
		}else{
			return "" + "\t";
		}
	}
	
	@SuppressWarnings("unchecked")
	public List<LoteLayoutSipac> obtenerLotesSipac(){
		List<LoteLayoutSipac> resultados = new ArrayList<LoteLayoutSipac>();
		final StringBuilder queryString = new StringBuilder("");
		try{
			queryString.append("select model from loteLayoutSipacDTO model");
			String q = getQueryString(queryString);
			q+= " order by model.id DESC";
			Query query = entityManager.createQuery(q);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			resultados = query.getResultList();
		}catch(Exception e){
			log.error("No se pudieron obtener los lotes" + e);
		}
		return resultados;
	}
	
	@SuppressWarnings("unchecked")
	public List<LayoutSipac> buscarReportesPend(Date fechaInicial, Date fechaFinal){
		List<LayoutSipac> resultados = new ArrayList<LayoutSipac>();
		Map<String, Object> params = new HashMap<String, Object>();
		final StringBuilder queryString = new StringBuilder("");
			try{
				
				String fechaI = simpleDateFormat.format(fechaInicial);
				String fechaF = simpleDateFormat.format(fechaFinal);
				
				queryString.append("select model from LayoutSipac model ");
				addCondition(queryString, "model.estatus = :estatus ");
				addCondition(queryString, "model.reporte.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.terminoAjuste in ('SRSIPACYRC','SRSIPAC') "); // se agrega criterio para que solo exporte a txt los que sean se recibio sipac o se recibio sipac con reembolso.
				addCondition(queryString, "model.siniestroAcreedor != null "); // se agrega parametro para que no muestre en el layout cuando esta null este campo
				addCondition(queryString, "func('TRUNC', model.reporte.fechaHoraReporte) between func('TO_DATE', :fechaInicial, 'dd/MM/yy') and func('TO_DATE', :fechaFinal, 'dd/MM/yy') ");
				params.put("fechaInicial", fechaI);
				params.put("fechaFinal", fechaF);
				params.put("estatus", LayoutSipac.Estatus.PENDIENTE.getValue());
				String q= getQueryString(queryString);
				q+= " order by model.id DESC";
				Query query = entityManager.createQuery(q);
				if(!params.isEmpty()){
					setParametros(query, params);
				}
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				resultados = query.getResultList();
			}catch(Exception e){
				log.error("Ocurrió un error al obtener Reportes Pendientes: " + e);
			}
			return resultados;
	}
	
	@SuppressWarnings("unchecked")
	public List<LoteLayoutSipac> obtenerLotesPendientes(){
		List<LoteLayoutSipac> resultados = new ArrayList<LoteLayoutSipac>();
		Map<String, Object> params = new HashMap<String, Object>();
		final StringBuilder queryString = new StringBuilder("");
		try{
			queryString.append("select model from loteLayoutSipacDTO model");
			addCondition(queryString, " model.estatus=:estatus");
			params.put("estatus", LOTEENPROCESO);
			String q= getQueryString(queryString);
			q+= " order by model.id ASC";
			Query query = entityManager.createQuery(q);
			if(!params.isEmpty()){
				setParametros(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			resultados = query.getResultList();
		}catch(Exception e){
			log.error("Ocurrió un error al obtener Lotes Pendientes: " + e);
		}
		return resultados;
	}
	
	private String validarSiNulo (String campo) {
		if (campo == null) {
			return "";
		}
		else if (campo.length() <= 0 ) {
			return "";
		}
		else {
			return campo;
		}
	}
	
	private String validarConvertirBD (BigDecimal bigDec) {
		if (bigDec == null) {
			return "";
		}
		else {
			return bigDec.toString();
		}
	}
	
	public void saveLotes(LoteLayoutSipac entity){
		LogDeMidasEJB3.log("saving LoteLayoutSipac instance", Level.INFO, null);
		try {
			entidadService.save(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void updateLote(LoteLayoutSipac entity) {
		LogDeMidasEJB3.log("updating LoteLayoutSipac instance", Level.INFO,null);
		try {
			entidadService.save(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public LoteLayoutSipac findLoteById(Long idLote){
		LogDeMidasEJB3.log("find LoteLayoutSipac instance", Level.INFO,null);
		try {
			LoteLayoutSipac result = entidadService.findById(LoteLayoutSipac.class, idLote);

			LogDeMidasEJB3.log("find successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public void updateLayout(LayoutSipac entity) {
		LogDeMidasEJB3.log("updating LayoutSipac instance", Level.INFO,null);
		try {
			entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		} 
	}
	
	/**
	 * Obtiene los registros de un lote determinado
	 * @param idLote
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<LayoutSipac> findLayoutByIdLote(Long idLote){
		LoteLayoutSipac lote = this.findLoteById(idLote);
		List<LayoutSipac> resultados = new ArrayList<LayoutSipac>();
		Map<String, Object> params = new HashMap<String, Object>();
		final StringBuilder queryString = new StringBuilder("");
			try{
				queryString.append("select model from LayoutSipac model ");
				addCondition(queryString, "model.lote = :lote");
				params.put("lote", lote);
				String q= getQueryString(queryString);
				q+= " order by model.numeroSecuencial ASC";
				Query query = entityManager.createQuery(q);
				if(!params.isEmpty()){
					setParametros(query, params);
				}
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
				resultados = query.getResultList();
			}catch(Exception e){
				log.error("Ocurrió un error al obtener Layout: " + e);
			}
			return resultados;
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(AND)){
			query=query.substring(0,(query.length())-(AND).length());
		}
		return query;
	}
	
	private void setParametros(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	public List<CatSubMarcaVehiculo> findMarcaTipoTercero(String estiloVehiculo)
	{
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CatSubMarcaVehiculo> cq = cb.createQuery(CatSubMarcaVehiculo.class);
		Root<CatSubMarcaVehiculo> root = cq.from(CatSubMarcaVehiculo.class);
		List<Predicate> predicates=new ArrayList<Predicate>();
		
		if(StringUtils.isNotEmpty(estiloVehiculo)){
			predicates.add(cb.like(cb.upper(root.get(CatSubMarcaVehiculo_.descCatSubMarcaVehiculo)), "%" + estiloVehiculo.toUpperCase() + "%"));
		}
		
		cq.where(predicates.toArray(new Predicate[predicates.size()]));
		List<CatSubMarcaVehiculo> resultado = entityManager.createQuery(cq).getResultList();
		
		return resultado;
		
		
	}
}
