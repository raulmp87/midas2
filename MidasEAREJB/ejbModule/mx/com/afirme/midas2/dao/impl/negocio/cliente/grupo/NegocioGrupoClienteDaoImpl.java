package mx.com.afirme.midas2.dao.impl.negocio.cliente.grupo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.cliente.grupo.NegocioGrupoClienteDao;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente_;
import mx.com.afirme.midas2.domain.negocio.Negocio_;
import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente;
import mx.com.afirme.midas2.domain.negocio.cliente.grupo.NegocioGrupoCliente_;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioGrupoClienteDaoImpl extends JpaDao<Long, NegocioGrupoCliente> implements
		NegocioGrupoClienteDao {

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public List<NegocioGrupoCliente> findByFilters(NegocioGrupoCliente entity){
		String queryString = "select model from NegocioGrupoCliente model ";
		List<HashMap> listaParametrosValidos = new ArrayList<HashMap>();
		String sWhere = "";
		
		if(entity.getGrupoClientes() != null){
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere,
					NegocioGrupoCliente_.grupoClientes.getName()+"."+GrupoCliente_.id.getName(), entity.getGrupoClientes().getId());
		}
		
		if(entity.getNegocio() != null){
			sWhere = Utilerias.agregaParametroQuery(listaParametrosValidos,sWhere,
					"negocio."+Negocio_.idToNegocio.getName(), entity.getNegocio().getIdToNegocio());
		}
		
		if (Utilerias.esAtributoQueryValido(sWhere))
			queryString = queryString.concat(" where ").concat(sWhere);
		
		Query query = entityManager.createQuery(queryString);
		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		return query.getResultList();
		
	}

}
