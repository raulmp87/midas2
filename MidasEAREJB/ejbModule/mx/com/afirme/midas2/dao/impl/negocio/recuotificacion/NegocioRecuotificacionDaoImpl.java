package mx.com.afirme.midas2.dao.impl.negocio.recuotificacion;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.recuotificacion.NegocioRecuotificacionDao;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacion;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionUsuario;

import org.apache.log4j.Logger;

@Stateless
public class NegocioRecuotificacionDaoImpl extends JpaDao<Long, NegocioRecuotificacion> implements NegocioRecuotificacionDao{
	
	private static final long serialVersionUID = 4950177916817396956L;

	public static final Logger LOG = Logger.getLogger(NegocioRecuotificacionDaoImpl.class);
	
	private static final String SPNAME_ASOCIADOS = "MIDAS.PKGAUT_NEGOCIOS.usuariosAsociadosRecuotifica";
	
	private static final String SPNAME_DISPONIBLES = "MIDAS.PKGAUT_NEGOCIOS.usuariosDispoRecuotifica";
	
	@Override
	public List<NegocioRecuotificacionUsuario> obtenerUsuariosDisponibles(
			Long idToNegocio){		
		return obtenerUsuarios(SPNAME_DISPONIBLES, idToNegocio);
	}
	
	@Override	
	public List<NegocioRecuotificacionUsuario> obtenerUsuariosAsociados(
			Long idToNegocio){		
		return obtenerUsuarios(SPNAME_ASOCIADOS, idToNegocio);
	}
	
	@SuppressWarnings("unchecked")
	private List<NegocioRecuotificacionUsuario> obtenerUsuarios(String spName, Long idToNegocio){
		List<NegocioRecuotificacionUsuario> usuarios = new ArrayList<NegocioRecuotificacionUsuario>();		
		StoredProcedureHelper storedHelper  = null;
		try {
			String propiedades = "recuotificacionId,usuarioId,cveUsuario,nombreUsuario";
			String columnasBaseDatos = "recuotificacionId,usuarioId,claveUsuario,nombreUsuario";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(NegocioRecuotificacionUsuario.class.getCanonicalName(), propiedades, columnasBaseDatos);
			storedHelper.estableceParametro("p_idToNegocio", idToNegocio);
			usuarios = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e);
		}
		return usuarios;
	}

	
}
