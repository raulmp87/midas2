package mx.com.afirme.midas2.dao.impl.bonos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.bonos.BonoExclusionTipoCedulaDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionTipoCedula;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class BonoExclusionTipoCedulaDaoImpl extends EntidadDaoImpl implements BonoExclusionTipoCedulaDao {
	
	@Override
	public List<BonoExclusionTipoCedula> findById(Long id) throws MidasException {
		BonoExclusionTipoCedula tipoCedula = new BonoExclusionTipoCedula();
		ConfigBonos configbono= new ConfigBonos();
		configbono.setId(id);
		tipoCedula.setIdConfig(configbono);
		List<BonoExclusionTipoCedula> lista = new ArrayList<BonoExclusionTipoCedula>();
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select model from BonoExclusionTipoCedula model ");
		addCondition(queryString, " model.idConfig=:idConfig");
		
		Query query = entityManager.createQuery(getQueryString(queryString));
		query.setParameter("idConfig",configbono);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		lista=query.getResultList();
		return lista;
	}
		
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}	
		
}
