package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.ValorCatalogoAgentesDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ValorCatalogoAgentesDaoImpl  extends EntidadDaoImpl implements ValorCatalogoAgentesDao{
	
	@Override
	public ValorCatalogoAgentes loadById(ValorCatalogoAgentes valorCatalogoAgentes) throws Exception{
		if(valorCatalogoAgentes==null || valorCatalogoAgentes.getId()==null){
			throw new Exception("ValorCatalogoAgentes is null or id is null");
		}
		ValorCatalogoAgentes filtro=new ValorCatalogoAgentes();
		filtro.setId(valorCatalogoAgentes.getId());
		List<ValorCatalogoAgentes> list=findByFilters(filtro);
		if(list!=null && !list.isEmpty()){
			valorCatalogoAgentes=list.get(0);
		}
		return valorCatalogoAgentes;
	}
	
	@Override
	public ValorCatalogoAgentes obtenerElementoEspecifico(String nombreCatalogo,String nombreElemento) throws Exception {
		if(nombreCatalogo==null || nombreCatalogo.isEmpty()){
			throw new Exception("NombreCatalogo is null");
		}
		if(nombreElemento==null || nombreElemento.isEmpty()){
			throw new Exception("nombreElemento is null");
		}
		ValorCatalogoAgentes filtro=new ValorCatalogoAgentes();
		filtro.setValor(nombreElemento);
		GrupoCatalogoAgente grupo=new GrupoCatalogoAgente();
		grupo.setDescripcion(nombreCatalogo);
		filtro.setGrupoCatalogoAgente(grupo);
		List<ValorCatalogoAgentes> list=findByFilters(filtro);
		if(list!=null && !list.isEmpty()){
			return list.get(0);
		}
		return null;
	}

	@Override
	public List<ValorCatalogoAgentes> obtenerElementosPorCatalogo(String nombreCatalogo)throws Exception {
		if(nombreCatalogo==null || nombreCatalogo.isEmpty()){
			throw new Exception("NombreCatalogo is null");
		}
		ValorCatalogoAgentes filtro=new ValorCatalogoAgentes();
		GrupoCatalogoAgente grupo=new GrupoCatalogoAgente();
		grupo.setDescripcion(nombreCatalogo);
		filtro.setGrupoCatalogoAgente(grupo);
		List<ValorCatalogoAgentes> list=findByFilters(filtro);
		return list;
	}

	@Override
	public Long obtenerIdElementEspecifico(String nombreCatalogo, String nombreElemento)throws Exception {
		if(nombreCatalogo==null || nombreCatalogo.isEmpty()){
			throw new Exception("NombreCatalogo is null");
		}
		if(nombreElemento==null || nombreElemento.isEmpty()){
			throw new Exception("nombreElemento is null");
		}
		ValorCatalogoAgentes filtro=new ValorCatalogoAgentes();
		filtro.setValor(nombreElemento);
		GrupoCatalogoAgente grupo=new GrupoCatalogoAgente();
		grupo.setDescripcion(nombreCatalogo);
		filtro.setGrupoCatalogoAgente(grupo);
		List<ValorCatalogoAgentes> list=findByFilters(filtro);
		if(list!=null && !list.isEmpty()){
			ValorCatalogoAgentes elemento=list.get(0);
			return (elemento!=null)?elemento.getId():null;
		}
		return null;
	}
	
	@Override
	public List<ValorCatalogoAgentes> findByFilters(ValorCatalogoAgentes filtro) throws Exception{
		List<ValorCatalogoAgentes> list=new ArrayList<ValorCatalogoAgentes>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ValorCatalogoAgentes model join fetch model.grupoCatalogoAgente ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){			
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}else{
				if(filtro.getClave()!=null && !filtro.getClave().isEmpty()){
					System.out.println(filtro.getClave());
                    addCondition(queryString, "model.clave=:nombreElementoClave");
                    params.put("nombreElementoClave", filtro.getClave());
               }
				if(filtro.getValor()!=null && !filtro.getValor().isEmpty()){
					addCondition(queryString, "model.valor=:nombreElementoCatalogo");
					params.put("nombreElementoCatalogo", filtro.getValor());
				}
				if(filtro.getIdRegistro() != null && filtro.getIdRegistro() != 0L)
				{
					addCondition(queryString, "model.idRegistro=:idRegistro");
					params.put("idRegistro", filtro.getIdRegistro());										
				}
				
				GrupoCatalogoAgente grupo=filtro.getGrupoCatalogoAgente();
				if(grupo!=null){
					if(grupo.getDescripcion()!=null && !grupo.getDescripcion().isEmpty()){
						addCondition(queryString, "model.grupoCatalogoAgente.descripcion=:nombreCatalogo");
						params.put("nombreCatalogo",grupo.getDescripcion());
					}
				}
			}
		}
		String q=getQueryString(queryString);
		q+=" order by model.id desc ";
		Query query = entityManager.createQuery(q);
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		//query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}
	
	@Override
	public List<ValorCatalogoAgentes> findByFiltersLike(ValorCatalogoAgentes filtro) throws Exception{
		List<ValorCatalogoAgentes> list=new ArrayList<ValorCatalogoAgentes>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ValorCatalogoAgentes model join fetch model.grupoCatalogoAgente ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){			
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}else{
				if(filtro.getClave()!=null && !filtro.getClave().isEmpty()){
					System.out.println(filtro.getClave());
                    addCondition(queryString, "UPPER(model.clave) like UPPER(:nombreElementoClave)");
                    params.put("nombreElementoClave", "%"+filtro.getClave()+"%");
               }
				if(filtro.getValor()!=null && !filtro.getValor().isEmpty()){
					addCondition(queryString, "UPPER(model.valor) like UPPER(:nombreElementoCatalogo)");
					params.put("nombreElementoCatalogo", "%"+filtro.getValor()+"%");
				}
				if(filtro.getIdRegistro() != null && filtro.getIdRegistro() != 0L)
				{
					addCondition(queryString, "model.idRegistro=:idRegistro");
					params.put("idRegistro", filtro.getIdRegistro());										
				}
				
				GrupoCatalogoAgente grupo=filtro.getGrupoCatalogoAgente();
				if(grupo!=null){
					if(grupo.getDescripcion()!=null && !grupo.getDescripcion().isEmpty()){
						addCondition(queryString, "model.grupoCatalogoAgente.descripcion=:nombreCatalogo");
						params.put("nombreCatalogo",grupo.getDescripcion());
					}
				}
			}
		}
		String q=getQueryString(queryString);
		q+=" order by model.id desc ";
		Query query = entityManager.createQuery(q);
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public GrupoCatalogoAgente loadCatalogById(GrupoCatalogoAgente filtro) throws Exception{
		List<GrupoCatalogoAgente> list=new ArrayList<GrupoCatalogoAgente>();
		GrupoCatalogoAgente catalogo=null;
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from GrupoCatalogoAgente model  ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}else{
				if(filtro.getDescripcion()!=null && !filtro.getDescripcion().isEmpty()){
					addCondition(queryString, "model.descripcion=:nombreCatalogo");
					params.put("nombreCatalogo",filtro.getDescripcion());
				}
			}
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		if(!isEmptyList(list)){
			catalogo=list.get(0);
		}
		return catalogo;
	}
	/**
	 * =========================================================================
	 * 								Common methods
	 * =========================================================================
	 */
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	/**
	 * Obtiene la lista de catalogos por filtro
	 */
	@Override
	public List<GrupoCatalogoAgente> catalogFindByFilters(GrupoCatalogoAgente filtro) throws Exception {
		List<GrupoCatalogoAgente> list=new ArrayList<GrupoCatalogoAgente>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from GrupoCatalogoAgente model ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}
			if(filtro.getDescripcion()!=null && !filtro.getDescripcion().isEmpty()){
				addCondition(queryString, "model.descripcion=:nombreCatalogo");
				params.put("nombreCatalogo", filtro.getDescripcion());
			}
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}
	/**
	 * Obtiene el catalogo por su descripcion
	 */
	@Override
	public GrupoCatalogoAgente obtenerCatalogoPorDescripcion(String descripcion)throws Exception {
		GrupoCatalogoAgente filtro=new GrupoCatalogoAgente();
		filtro.setDescripcion(descripcion);
		List<GrupoCatalogoAgente> list=catalogFindByFilters(filtro);
		if(list!=null && !list.isEmpty()){
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * Obtiene el id del catalogo por su descripcion.
	 * @param descripcion
	 * @return
	 * @throws Exception
	 */
	@Override
	public Long obtenerIdCatalogoPorDescripcion(String descripcion)throws Exception{
		GrupoCatalogoAgente catalogo=obtenerCatalogoPorDescripcion(descripcion);
		return (catalogo!=null)?catalogo.getId():null;
	}
	
	@Override
	public List<ValorCatalogoAgentes> findByTipoCedula(ValorCatalogoAgentes filtro) throws Exception {
		List<ValorCatalogoAgentes> list=new ArrayList<ValorCatalogoAgentes>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ValorCatalogoAgentes model join fetch model.grupoCatalogoAgente ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}else{
				if(filtro.getValor()!=null && !filtro.getValor().isEmpty()){
					addCondition(queryString, "UPPER(model.valor) like UPPER(:nombreElementoCatalogo)");
					params.put("nombreElementoCatalogo", "%"+filtro.getValor()+"%");
				}
				GrupoCatalogoAgente grupo=filtro.getGrupoCatalogoAgente();
				if(grupo!=null){
					if(grupo.getDescripcion()!=null && !grupo.getDescripcion().isEmpty()){
						addCondition(queryString, "UPPER(model.grupoCatalogoAgente.descripcion) like UPPER(:nombreCatalogo)");
						params.put("nombreCatalogo","%"+grupo.getDescripcion()+"%");
					}
				}
			}
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public String deleteTipoCedula(ValorCatalogoAgentes tipoCedula) throws Exception {		
		if(isNull(tipoCedula) || isNull(tipoCedula.getId())){
			throw new Exception("Favor de proporcionar el tipo de cedula a eliminar");
		}
		tipoCedula=entityManager.find(ValorCatalogoAgentes.class,tipoCedula.getId());
		try{
			entityManager.remove(tipoCedula);
			eliminarElementoCatalogoEnSeycos(tipoCedula);
			return "La cedula ha sido eliminada satisfactoriamente";
		}
		catch(Exception e){
			throw new Exception("El tipo de cedula ya tiene una relacion");
		}	
	}
	
	public Long guardarElementoCatalogoEnSeycos(ValorCatalogoAgentes elemento) throws Exception{
		Long id=null;
		if(isNull(elemento)){
			throw new Exception("Favor de proporcionar el elemento a replicar");
		}
		if(isNull(elemento.getId())){
			throw new Exception("Favor de proporcionar la clave o id del tipo de cedula");
		}
		GrupoCatalogoAgente catalogo=elemento.getGrupoCatalogoAgente();
		if(isNull(catalogo) || isNull(catalogo.getId())){
			throw new Exception("Favor de indicar el catalogo del elemento");
		}
		catalogo=loadCatalogById(catalogo);
		//FIXME Quitar hasta que se proporcione el stored procedure de Carlos Moreno
		boolean waitingCarlosMorenoProcedure=false;
		if(!waitingCarlosMorenoProcedure){
			StoredProcedureHelper storedHelper = null;
			String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_CatTipoCedula";
			try {
				LogDeMidasInterfaz.log("Entrando a AgenteMidas.replicanEnSeycos..." + this, Level.INFO, null);
				storedHelper = new StoredProcedureHelper(sp);
				storedHelper.estableceParametro("pIdTipoCedula", val(elemento.getId()));
				storedHelper.estableceParametro("pnombre", val(elemento.getValor()));
				storedHelper.ejecutaActualizar();
				LogDeMidasInterfaz.log("Se ha guardado ejecutivo "+ this, Level.INFO, null);
				LogDeMidasInterfaz.log("Saliendo de AgenteMidas.replicanEnSeycos..." + this, Level.INFO, null);
			} catch (SQLException e) {				
				Integer codErr = null;
				String descErr = null;
				if (storedHelper != null) {
					codErr = storedHelper.getCodigoRespuesta();
					descErr = storedHelper.getDescripcionRespuesta();
				}
				StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, elemento.getClass(), codErr, descErr);
				LogDeMidasInterfaz.log("Excepcion en BD de AgenteMidas.replicanEnSeycos..." + this, Level.WARNING, e);
				throw e;
			} catch (Exception e) {
				LogDeMidasInterfaz.log("Excepcion general en AgenteMidas.replicanEnSeycos..." + this, Level.WARNING, e);
				throw e;
			}
		}
		return id;
	}
	
	@Override
	public Long eliminarElementoCatalogoEnSeycos(ValorCatalogoAgentes elemento) throws Exception{
		Long id=null;
		if(isNull(elemento)|| isNull(elemento.getId())){
			throw new Exception("Favor de proporcionar el id del elemento a eliminar");
		}
		GrupoCatalogoAgente catalogo=elemento.getGrupoCatalogoAgente();
		if(isNull(catalogo) || isNull(catalogo.getId())){
			throw new Exception("Favor de indicar el catalogo del elemento");
		}
		catalogo=loadCatalogById(catalogo);
		StoredProcedureHelper storedHelper = null;
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_deleteTipoCedula";
		try {
			LogDeMidasInterfaz.log("Entrando a AgenteMidas.replicanEnSeycos..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceParametro("pIdTipoCedula", val(elemento.getId()));
			storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz.log("Se ha eliminado el tipo de cedula con id:"+elemento.getId()+"."+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de ValorCatalogoAgentesDaoImpl.eliminarElementoCatalogoEnSeycos..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, elemento.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de ValorCatalogoAgentesDaoImpl.eliminarElementoCatalogoEnSeycos..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en ValorCatalogoAgentesDaoImpl.eliminarElementoCatalogoEnSeycos..." + this, Level.WARNING, e);
			throw e;
		}
		return id;
	}
	
	private Object val(Object value){
		return (value!=null)?value:"";
	}
	
	/**
	 * Obtiene la lista de elementos por un catalogo con opcion a excluir elementos
	 * @param nombreCatalogo
	 * @param excluirElementos
	 * @return
	 * @throws Exception
	 */
	public List<ValorCatalogoAgentes> obtenerElementosPorCatalogo(String nombreCatalogo,String... excluirElementos) throws Exception{
		List<ValorCatalogoAgentes> list=new ArrayList<ValorCatalogoAgentes>();
		final StringBuilder queryString=new StringBuilder("");
		if(!isValid(nombreCatalogo)){
			onError("Favor de proporcionar el nombre del catalogo");
		}
		queryString.append("select model from ValorCatalogoAgentes model join fetch model.grupoCatalogoAgente ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(isValid(nombreCatalogo)){
			addCondition(queryString, " model.grupoCatalogoAgente.descripcion=:nombreCatalogo");
			params.put("nombreCatalogo",nombreCatalogo);
		}
		if(!isEmptyArray(excluirElementos)){
			queryString.append(" model.valor not in (");
			int i=0;
			for(String elementos:excluirElementos){
				if(i<(excluirElementos.length-1)){
					queryString.append("'"+elementos+"',");
				}else{
					queryString.append("'"+elementos+"'");
				}
				i++;
			}
			queryString.append(" )");
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}
	
	/**
	 * Obtiene la lista de elementos por un catalogo con opcion a obtener ciertos elementos
	 * @param nombreCatalogo
	 * @param excluirElementos
	 * @return
	 * @throws Exception
	 */
	public List<ValorCatalogoAgentes> obtenerElementosEspecificosPorCatalogo(String nombreCatalogo,String... excluirElementos) throws Exception{
		List<ValorCatalogoAgentes> list=new ArrayList<ValorCatalogoAgentes>();
		final StringBuilder queryString=new StringBuilder("");
		if(!isValid(nombreCatalogo)){
			onError("Favor de proporcionar el nombre del catalogo");
		}
		queryString.append("select model from ValorCatalogoAgentes model join fetch model.grupoCatalogoAgente ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(isValid(nombreCatalogo)){
			addCondition(queryString, " model.grupoCatalogoAgente.descripcion=:nombreCatalogo");
			params.put("nombreCatalogo",nombreCatalogo);
		}
		if(!isEmptyArray(excluirElementos)){
			queryString.append(" model.valor in (");
			int i=0;
			for(String elementos:excluirElementos){
				if(i<(excluirElementos.length-1)){
					queryString.append("'"+elementos+"',");
				}else{
					queryString.append("'"+elementos+"'");
				}
				i++;
			}
			queryString.append(" )");
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list=query.getResultList();
		return list;
	}
}
