package mx.com.afirme.midas2.dao.impl.coberturas.kilometros;

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.coberturas.kilometros.CoberturaAdicionalDao;
import mx.com.afirme.midas2.domain.coberturas.kilometros.SolicitudCoberturaAdicionalDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class CoberturaAdicionalDaoImpl implements CoberturaAdicionalDao {
	
	@EJB
	private EntidadService entidadService;

	@Override
	public void save(SolicitudCoberturaAdicionalDTO entity) {
		LogDeMidasEJB3.log("saving SolicitudCoberturaAdicionalDTO instance", Level.INFO, null);
		try {
			entidadService.save(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public SolicitudCoberturaAdicionalDTO update(SolicitudCoberturaAdicionalDTO entity) {
		LogDeMidasEJB3.log("updating SolicitudCoberturaAdicionalDTO instance", Level.INFO,null);
		try {
			SolicitudCoberturaAdicionalDTO result = entidadService.save(entity);

			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public SolicitudCoberturaAdicionalDTO findBySolicitud(BigDecimal numeroSolicitud) {			
			List<SolicitudCoberturaAdicionalDTO> instance = this.findByProperty("idToSolicitud", numeroSolicitud);
			if (instance!=null && instance.size() > 0){
				return instance.get(0);
			}
			return null;
	}
	
	public List<SolicitudCoberturaAdicionalDTO> findByPoliza(BigDecimal idToPoliza) {
		return this.findByProperty("idToPoliza", idToPoliza);
	}
	
	public List<SolicitudCoberturaAdicionalDTO>  findByProperty(String propertyName, Object propertyValue) {
		return	 entidadService.findByProperty(SolicitudCoberturaAdicionalDTO.class ,propertyName, propertyValue);	
	}
}