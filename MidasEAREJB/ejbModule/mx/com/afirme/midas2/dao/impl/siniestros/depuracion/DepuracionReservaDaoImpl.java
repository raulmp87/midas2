package mx.com.afirme.midas2.dao.impl.siniestros.depuracion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaDTO_;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO_;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO_;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.depuracion.DepuracionReservaDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.Negocio_;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina_;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina_;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina_;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina_;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SeccionReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SeccionReporteCabina_;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina_;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina_;
import mx.com.afirme.midas2.domain.siniestros.depuracion.CoberturaDepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.CoberturaDepuracionReserva_;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.ConfiguracionDepuracionReserva_;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReserva;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReservaDetalle;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReservaDetalle_;
import mx.com.afirme.midas2.domain.siniestros.depuracion.DepuracionReserva_;
import mx.com.afirme.midas2.dto.siniestros.depuracion.DepuracionReservaDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.MovimientoPosteriorAjusteDTO;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import org.apache.commons.lang.StringUtils;

import org.apache.log4j.Logger;

@Stateless
public class DepuracionReservaDaoImpl extends JpaDao<Long, DepuracionReserva> implements DepuracionReservaDao {
	private static final int	LONG_CODIGOTIPOPOLIZA	= 4;


	private static final int	LONG_CODIGOPRODUCTO	= 2;
	private static final Logger LOG = Logger.getLogger(DepuracionReservaDaoImpl.class);

	@EJB
	private EntidadDao entidadDao;
	
	
	private static final String IGUAL = " = ";
	private static final String MAYOR_IGUAL = " >= ";
	private static final String MENOR_IGUAL = " <= ";
	
	private static final String WHERE_CONDITION = " WHERE ";
	private static final String AND_CONDITION = " AND ";
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@Override
	public Long procesarDepuracionPorSP(String listaIdsDepuraciones){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.procesarDepuracion";
		Long depuracionReservaId = null;
		String usuarioEnCurso = this.usuarioService.getUsuarioActual().getNombreUsuario();
		try{
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceParametro("pListaIdsDepurar", listaIdsDepuraciones);
			spHelper.estableceParametro("pCodigoUsuarioEnCurso", usuarioEnCurso);
			Integer resultado = (Integer)spHelper.ejecutaActualizar();
			depuracionReservaId = resultado.longValue();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		return depuracionReservaId;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DepuracionReservaDetalle> getOnlyReservasDepuradasToExcel(String listaIdsDepurados){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.exportarDepuradosExcel";
		List<DepuracionReservaDetalle> reservasDepuradasDet = null;
		
		try{
			String columnaBd = "ID,ESTATUS,NUMERO,NUMEROSINIESTRO,NUMPOLIZA,INCISO,FOLIOPASE,COBERTURA,CONTRATANTE,TERMINOAJUST,NOMCONFIG,RSVACTUAL,RSVDEPURADA,PORCENTDEPURAR";
			String atributos = "id,estatus,numero,numeroSiniestro,numeroPolizaFormateada,numeroInciso,folio,nombreCobertura,nombreAsegurado,terminoAjusteDesc,nombreConfiguracion,reservaActual,reservaDepurada,porcentajeDepurado";
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(DepuracionReservaDetalle.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("pListaIdsDepurados", listaIdsDepurados);
			reservasDepuradasDet = spHelper.obtieneListaResultados();
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return reservasDepuradasDet;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DepuracionReservaDetalle> mostrarListadoReservasParaDepurar(Long configuracionId){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerReservas";
		List<DepuracionReservaDetalle> reservaDetalle = null;

		try{
			String columnaBd = "ID,ESTATUS,NUMERO,NUMEROSINIESTRO,NUMPOLIZA,INCISO,FOLIOPASE,COBERTURA,CONTRATANTE,TERMINOAJUST,NOMCONFIG,RSVACTUAL,RSVDEPURAR,PORCENTDEPURAR";
			String atributos = "id,estatus,numero,numeroSiniestro,numeroPolizaFormateada,numeroInciso,folio,nombreCobertura,nombreAsegurado,terminoAjusteDesc,nombreConfiguracion,reservaActual,reservaPorDepurar,depuratedPercent";
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(DepuracionReservaDetalle.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("pIdConfiguracion", configuracionId);
			reservaDetalle = spHelper.obtieneListaResultados();
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return reservaDetalle;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DepuracionReservaDetalle> obtenerReservasExportacionSP(Long depuracionReservaId){
		StoredProcedureHelper spHelper = null;
		String spName ="MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerReservasExportacion";
		List<DepuracionReservaDetalle> reservaDetalle = null;

		try{
			String columnaBd = "ID,ESTATUS,NUMERO,NUMEROSINIESTRO,NUMPOLIZA,INCISO,FOLIOPASE,COBERTURA,CONTRATANTE,TERMINOAJUST,NOMCONFIG,RSVACTUAL,RSVDEPURAR,PORCENTDEPURAR";
			String atributos = "id,estatus,numero,numeroSiniestro,numeroPolizaFormateada,numeroInciso,folio,nombreCobertura,nombreAsegurado,terminoAjusteDesc,nombreConfiguracion,reservaActual,reservaPorDepurar,depuratedPercent";
			spHelper = new StoredProcedureHelper( spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			spHelper.estableceMapeoResultados(DepuracionReservaDetalle.class.getCanonicalName(), atributos, columnaBd);
			spHelper.estableceParametro("pIdDepuracion", depuracionReservaId);
			reservaDetalle = spHelper.obtieneListaResultados();
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return reservaDetalle;
	}
	
	@Override
	public List<ConfiguracionDepuracionReserva> buscarConfiguraciones(
			DepuracionReservaDTO filtro) {
		List<ConfiguracionDepuracionReserva> listConfiguraciones = new ArrayList<ConfiguracionDepuracionReserva>();
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<ConfiguracionDepuracionReserva> cq = cb.createQuery(ConfiguracionDepuracionReserva.class);
		Root<ConfiguracionDepuracionReserva> root = cq.from(ConfiguracionDepuracionReserva.class);
		List<Predicate> predicates=new ArrayList<Predicate>();
		if(StringUtils.isNotEmpty(filtro.getTipoServicio())){
			predicates.add(cb.equal(root.get(ConfiguracionDepuracionReserva_.tipoServicio), filtro.getTipoServicio()));
		}
		if(filtro.getOficinaId() != null){
			Join<ConfiguracionDepuracionReserva,Oficina> ofi = root.join(ConfiguracionDepuracionReserva_.oficina, JoinType.INNER);
			predicates.add(cb.equal(ofi.get(Oficina_.id), filtro.getOficinaId()));
		}
		if(filtro.getNegocioId() != null){
			Join<ConfiguracionDepuracionReserva,Negocio> neg = root.join(ConfiguracionDepuracionReserva_.negocio, JoinType.INNER);
			predicates.add(cb.equal(neg.get(Negocio_.idToNegocio), filtro.getNegocioId()));
		}
		if(StringUtils.isNotEmpty(filtro.getNombreConfiguracion())){
			predicates.add(cb.like(cb.upper(root.get(ConfiguracionDepuracionReserva_.nombre)), "%" + filtro.getNombreConfiguracion().toUpperCase() + "%"));
		}
		if(StringUtils.isNotEmpty(filtro.getEstatus())){
			predicates.add(cb.equal(root.get(ConfiguracionDepuracionReserva_.estatus), filtro.getEstatus()));
		}
		if(filtro.getFechaIniActivo() != null){
			predicates.add(cb.between(root.get(ConfiguracionDepuracionReserva_.fechaActivo), filtro.getFechaIniActivo(), 
					filtro.getFechaFinActivo() != null ? filtro.getFechaFinActivo() : new Date()));
		}else if(filtro.getFechaFinActivo() != null){
			predicates.add(cb.lessThanOrEqualTo(root.get(ConfiguracionDepuracionReserva_.fechaActivo), filtro.getFechaFinActivo()));
		}
		if(filtro.getFechaIniInactivo() != null){
			predicates.add(cb.between(root.get(ConfiguracionDepuracionReserva_.fechaInactivo), filtro.getFechaIniInactivo(), 
					filtro.getFechaFinInactivo() != null ? filtro.getFechaFinInactivo() : new Date()));
		}else if(filtro.getFechaFinInactivo() != null){
			predicates.add(cb.lessThanOrEqualTo(root.get(ConfiguracionDepuracionReserva_.fechaInactivo), filtro.getFechaFinInactivo()));
		}
		
		if(filtro.getCoberturaId() != null){
			Join<ConfiguracionDepuracionReserva,CoberturaDepuracionReserva> cobDep = root.join(ConfiguracionDepuracionReserva_.coberturas, JoinType.INNER);
			Join<CoberturaDepuracionReserva,CoberturaDTO> cob = cobDep.join(CoberturaDepuracionReserva_.cobertura, JoinType.INNER);
			Join<CoberturaDepuracionReserva,SeccionDTO> sec = cobDep.join(CoberturaDepuracionReserva_.seccion, JoinType.INNER);
			predicates.add(cb.equal(cob.get(CoberturaDTO_.idToCobertura), filtro.getCoberturaId()));
			predicates.add(cb.equal(sec.get(SeccionDTO_.idToSeccion), filtro.getSeccionId()));
			if(StringUtils.isNotEmpty(filtro.getClaveSubCalculo())){
				predicates.add(cb.equal(cobDep.get(CoberturaDepuracionReserva_.claveSubCalculo), filtro.getClaveSubCalculo()));
			}
		}
		if(StringUtils.isNotEmpty(filtro.getNumeroPoliza()) || filtro.getNumeroInciso() != null || StringUtils.isNotEmpty(filtro.getNumeroSiniestro()) ||
				StringUtils.isNotEmpty(filtro.getContratante())){

			Join<ConfiguracionDepuracionReserva,DepuracionReserva> dep = root.join(ConfiguracionDepuracionReserva_.depuracionReservas, JoinType.INNER);
			Join<DepuracionReserva,DepuracionReservaDetalle> depDet = dep.join(DepuracionReserva_.depuracionReservaDetalles, JoinType.INNER);
			Join<DepuracionReservaDetalle,EstimacionCoberturaReporteCabina> est = depDet.join(DepuracionReservaDetalle_.estimacionCobertura, JoinType.INNER);
			Join<EstimacionCoberturaReporteCabina,CoberturaReporteCabina> cobRep = est.join(EstimacionCoberturaReporteCabina_.coberturaReporteCabina, JoinType.INNER);
			Join<CoberturaReporteCabina, IncisoReporteCabina> incRep = cobRep.join(CoberturaReporteCabina_.incisoReporteCabina, JoinType.INNER);
			Join<IncisoReporteCabina, SeccionReporteCabina> secRep = incRep.join(IncisoReporteCabina_.seccionReporteCabina, JoinType.INNER);
			Join<SeccionReporteCabina, ReporteCabina> repCab = secRep.join(SeccionReporteCabina_.reporteCabina, JoinType.INNER);
			Join<ReporteCabina, PolizaDTO> poliza = repCab.join(ReporteCabina_.poliza, JoinType.INNER);
			Join<ReporteCabina, SiniestroCabina> sin = repCab.join(ReporteCabina_.siniestroCabina, JoinType.LEFT);
			
			if(StringUtils.isNotEmpty(filtro.getNumeroPoliza())){
				String[] arrayNumeroPoliza = filtro.getNumeroPoliza().split("-");
				String codigoProducto;
				String codigoTipoPoliza;
				int numeroPoliza;
				int numeroRenovacion;
				try{
					codigoProducto = arrayNumeroPoliza[0].substring(0, LONG_CODIGOPRODUCTO);
					codigoTipoPoliza = arrayNumeroPoliza[0].substring(2, LONG_CODIGOTIPOPOLIZA);
					numeroPoliza = Integer.parseInt(arrayNumeroPoliza[1]);
					numeroRenovacion = Integer.parseInt(arrayNumeroPoliza[2]);
				}catch(Exception e){
					codigoProducto = "0";
					codigoTipoPoliza = "0";
					numeroPoliza = 0;
					numeroRenovacion = 0;
				}
				predicates.add(cb.like(poliza.get(PolizaDTO_.codigoProducto), "%" + codigoProducto + "%"));
				predicates.add(cb.like(poliza.get(PolizaDTO_.codigoTipoPoliza), "%" + codigoTipoPoliza + "%"));
				predicates.add(cb.equal(poliza.get(PolizaDTO_.numeroPoliza), numeroPoliza));
				predicates.add(cb.equal(poliza.get(PolizaDTO_.numeroRenovacion), numeroRenovacion));
			}
			if(filtro.getNumeroInciso() != null){
				predicates.add(cb.equal(incRep.get(IncisoReporteCabina_.numeroInciso), filtro.getNumeroInciso()));
			}
			if(StringUtils.isNotEmpty(filtro.getNumeroSiniestro())){
				String[] arrayNumeroSiniestro = filtro.getNumeroSiniestro().split("-");
				String claveOficina;
				Integer consecutivo;
				Integer anio;
				try{
					claveOficina = arrayNumeroSiniestro[0];
					consecutivo = Integer.parseInt(arrayNumeroSiniestro[1]);
					anio = Integer.parseInt(arrayNumeroSiniestro[2]);
				}catch(Exception e){
					claveOficina = "";
					consecutivo = 0;
					anio = 0;
				}
				predicates.add(cb.equal(cb.upper(sin.get(SiniestroCabina_.claveOficina)), claveOficina));
				predicates.add(cb.equal(sin.get(SiniestroCabina_.consecutivoReporte), consecutivo));
				predicates.add(cb.equal(sin.get(SiniestroCabina_.anioReporte), anio));
			}
			if(StringUtils.isNotEmpty(filtro.getContratante())){
				predicates.add(cb.like(cb.upper(incRep.get(IncisoReporteCabina_.nombreAsegurado)), "%" + filtro.getContratante().toUpperCase() + "%"));
			}
		}
		cq.where(predicates.toArray(new Predicate[predicates.size()]));
		List<ConfiguracionDepuracionReserva> resultado = entityManager.createQuery(cq).getResultList();
		for(ConfiguracionDepuracionReserva conf: resultado){
			if(!listConfiguraciones.contains(conf)){
				listConfiguraciones.add(conf);
			}
		}
		return listConfiguraciones;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfiguracionDepuracionReserva> obtenerConfiguracionesAlDia(
			Date fechaCriterio) {
		int indexParam = 1;
		List<ConfiguracionDepuracionReserva> configuraciones = new ArrayList<ConfiguracionDepuracionReserva>();
		StringBuilder queryString = new StringBuilder("SELECT CONF.ID FROM ");			
		queryString.append(" 	(SELECT ID, TIPO_PROGRAMACION, CASE TIPO_PROGRAMACION  ");
		queryString.append(" 		WHEN 'PD' THEN  TO_DATE(LPAD(DIA_PROGRAMACION, 2, '0') || '-' || TO_CHAR( ?, 'mm-yyyy'), 'dd-mm-yyyy')  ");
		queryString.append(" 		WHEN 'PF' THEN FECHA_PROGRAMACION  ");
		queryString.append(" 		WHEN 'DD' THEN TO_DATE(LPAD(LAST_DAY(?), 2, '0') || '-' || TO_CHAR(?, 'mm-yyyy'), 'dd-mm-yyyy')  - 2  ");
		queryString.append(" 		END AS FEC_PROGRAMACION  ");
		queryString.append(" 	FROM MIDAS.TOCONFIGDEPURACIONRESERVA  ");
		queryString.append(" 	WHERE ESTATUS = 1) CONF  ");
		queryString.append(" 	WHERE TRUNC(CONF.FEC_PROGRAMACION)=TRUNC(?)");
		Query query = entityManager.createNativeQuery(queryString.toString());

		query.setParameter(indexParam++, fechaCriterio);
		query.setParameter(indexParam++, fechaCriterio);
		query.setParameter(indexParam++, fechaCriterio);
		query.setParameter(indexParam, fechaCriterio);
		List<BigDecimal> lista= query.getResultList();
		for(BigDecimal id:lista){
			configuraciones.add(entidadDao.findById(ConfiguracionDepuracionReserva.class, id.longValue()));
		}
		return configuraciones;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Oficina> obtenerOficinasRelacionadas(Long idDepuracion) {
		Map<String, Object> parameters = new HashMap<String, Object>();
		StringBuilder queryString = new StringBuilder(
				"SELECT distinct model.estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.oficina ");
		queryString.append(" from DepuracionReservaDetalle model ");
		queryString.append(" WHERE model.depuracionReserva.id = :idDepuracion");
		queryString.append(" AND model.estatus= :estatus");
		parameters.put("idDepuracion", idDepuracion);
		parameters.put("estatus", 1);
		return (List<Oficina>) entidadDao.executeQueryMultipleResult(queryString.toString(), parameters);
	}

	@SuppressWarnings("unchecked")
	public List<EstimacionCoberturaReporteCabina> obtenerReservas(
			Long idConfiguracion) {
		List<EstimacionCoberturaReporteCabina> idsEstimaciones=new ArrayList<EstimacionCoberturaReporteCabina>();
		List<EstimacionCoberturaReporteCabina> estimacionesReserva=new ArrayList<EstimacionCoberturaReporteCabina>();
		String spName = "MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerReservas"; 
        StoredProcedureHelper storedHelper 			= null;
        String propiedades 							= "";
        String columnasBaseDatos 						= "";
        
        propiedades = "id";
    	columnasBaseDatos = "ID";
    	try{
    		storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(EstimacionCoberturaReporteCabina.class.getCanonicalName(), propiedades, columnasBaseDatos);
            storedHelper.estableceParametro("pIdConfiguracion", idConfiguracion);
            idsEstimaciones=storedHelper.obtieneListaResultados();
            EstimacionCoberturaReporteCabina estimacionReserva;
            for(EstimacionCoberturaReporteCabina idEstimacion: idsEstimaciones ){
            	estimacionReserva = entidadDao.findById(EstimacionCoberturaReporteCabina.class, idEstimacion.getId());
            	estimacionesReserva.add(estimacionReserva);
            }
    	}catch (Exception e) {
            String descErr = null;
            if (storedHelper != null) {
                  descErr = storedHelper.getDescripcionRespuesta();
                  LogDeMidasInterfaz.log(
                              "Excepcion general en MIDAS.PKGSIN_CALCULOSMOVIMIENTOS.obtenerReservas..."+descErr
                                          , Level.WARNING, e);    
            }
       }
		return estimacionesReserva;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoCoberturaSiniestro> buscarMovimientosPosteriores( MovimientoPosteriorAjusteDTO filtro) {
		
		List<MovimientoCoberturaSiniestro> movimientos = new ArrayList<MovimientoCoberturaSiniestro>();
		
		String tipoDeServicio = filtro.getTipoServicio();
		Long oficinaId = filtro.getOficinaId();
		Integer numeroInciso = filtro.getNumeroInciso();
		BigDecimal idToCobertura = filtro.getIdToCobertura();
		String tipoEstimacion = filtro.getTipoEstimacion();
		String terminoSiniestro = filtro.getTerminoSiniestro();
		Long conceptoId = filtro.getConceptoId();
		String numeroReporte = filtro.getNumeroReporte();
		String numeroDeSiniestro = filtro.getNumeroSiniestro();
		String numeroPoliza = filtro.getNumeroPoliza();
		Date fechaInicio = filtro.getFechaInicio();
		Date fechaFin = filtro.getFechaFin();
		
		Map<String,Object> parametersMap = new HashMap<String,Object>();
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT movimiento ");
		queryString.append(" FROM   MovimientoCoberturaSiniestro movimiento ");
		queryString = this.addCondition(queryString, "movimiento.estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.oficina.tipoServicio", "tipoDeServicio",IGUAL, tipoDeServicio, parametersMap);
		queryString = this.addCondition(queryString, "movimiento.estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.oficina.id", "oficinaId",IGUAL, oficinaId, parametersMap);
		queryString = this.addCondition(queryString, "movimiento.estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.numeroInciso", "numeroInciso",IGUAL, numeroInciso, parametersMap);
		queryString = this.addCondition(queryString, "movimiento.estimacionCobertura.coberturaReporteCabina.coberturaDTO.idToCobertura", "coberturaId",IGUAL,  idToCobertura, parametersMap);
		queryString = this.addCondition(queryString, "movimiento.estimacionCobertura.tipoEstimacion", "tipoEstimacion",IGUAL,tipoEstimacion, parametersMap);
		queryString = this.addCondition(queryString, "movimiento.estimacionCobertura.coberturaReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.terminoSiniestro", "terminoSiniestro",IGUAL, terminoSiniestro, parametersMap);
		queryString = this.addCondition(queryString, "movimiento.estimacionCobertura.configuracionDepuracion.id", "conceptoId",IGUAL,conceptoId, parametersMap);
		queryString = this.addCondition(queryString, "movimiento.fechaCreacion", "fechaInicio",MAYOR_IGUAL,fechaInicio, parametersMap);
		queryString = this.addCondition(queryString, "movimiento.fechaCreacion", "fechaFin",MENOR_IGUAL,fechaFin, parametersMap);
		queryString = this.agregaCondicionDespuesDeDepuracionTotal(queryString);
		Query query = this.entityManager.createQuery(queryString.toString());
		this.addParametersToQuery(query, parametersMap);
		movimientos =  query.getResultList();
		movimientos =  this.filtrarPorNumeroDePoliza(movimientos, numeroPoliza);
		movimientos =  this.filtrarPorNumeroDeSiniestro(movimientos, numeroDeSiniestro);
		movimientos =  this.filtrarPorNumeroDeReporte(movimientos, numeroReporte);
		return movimientos;
	}
	
	private List<MovimientoCoberturaSiniestro> filtrarPorNumeroDeReporte(List<MovimientoCoberturaSiniestro> movimientos,String filtroNumeroDeReporte){
		List<MovimientoCoberturaSiniestro> listaFiltrada = new ArrayList<MovimientoCoberturaSiniestro>();
		if(filtroNumeroDeReporte!=null && !filtroNumeroDeReporte.equals("")){
			for(MovimientoCoberturaSiniestro movimiento : movimientos){
				String numeroReporte = movimiento.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getNumeroReporte();
				if(numeroReporte.equals(filtroNumeroDeReporte)){
					listaFiltrada.add(movimiento);
				}
			}
		}else{
			listaFiltrada = movimientos;
		}
		return listaFiltrada;
	}
	
	private List<MovimientoCoberturaSiniestro> filtrarPorNumeroDeSiniestro(List<MovimientoCoberturaSiniestro> movimientos,String filtroNumeroDeSiniestro){
		List<MovimientoCoberturaSiniestro> listaFiltrada = new ArrayList<MovimientoCoberturaSiniestro>();
		if(filtroNumeroDeSiniestro!=null && !filtroNumeroDeSiniestro.equals("")){
			for(MovimientoCoberturaSiniestro movimiento : movimientos){
				SiniestroCabina siniestroCabina = movimiento.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getSiniestroCabina();
				if(siniestroCabina!=null && siniestroCabina.getNumeroSiniestro().equals(filtroNumeroDeSiniestro)){
						listaFiltrada.add(movimiento);
				}
			}
		}else{
			listaFiltrada = movimientos;
		}
		return listaFiltrada;
	}
	
	private List<MovimientoCoberturaSiniestro> filtrarPorNumeroDePoliza(List<MovimientoCoberturaSiniestro> movimientos,String numeroDePoliza){
		List<MovimientoCoberturaSiniestro> listaFiltrada = new ArrayList<MovimientoCoberturaSiniestro>();
		if(numeroDePoliza==null || numeroDePoliza.equals("")){
			listaFiltrada  = movimientos;
		}else{
			for(MovimientoCoberturaSiniestro movimiento: movimientos){
				PolizaDTO poliza = movimiento.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza();
				if(poliza!=null){
					if(poliza.getNumeroPolizaFormateada().equals(numeroDePoliza)){
						listaFiltrada.add(movimiento);
					}
				}
			}
		}
		return listaFiltrada;
	}
	
	private StringBuilder addCondition(StringBuilder query, String propiedad, String identificador, String operador , Object valor,Map<String,Object> parametersMap){
		StringBuilder queryAux = query;
		if(valor!=null){
			if(valor instanceof String && valor.equals("")){
					return queryAux;
			}
			queryAux = this.agregaWhereOrAnd(queryAux);
			if(valor instanceof Date){
				queryAux = queryAux.append(" FUNC('trunc', "+propiedad+" ) "+operador+" :"+identificador);
				
			}else{
				queryAux = queryAux.append(" "+propiedad+" "+operador+" :"+identificador);
			}
			parametersMap.put(identificador, valor);
		}
		return queryAux;
	}
	
	private StringBuilder agregaWhereOrAnd(StringBuilder query){
		StringBuilder queryAux = query;
		if(queryAux.indexOf(WHERE_CONDITION, 0)<0){
			queryAux = queryAux.append(WHERE_CONDITION);
		}else{
			queryAux = queryAux.append(AND_CONDITION);
		}
		return queryAux;
	}
	
	private StringBuilder agregaCondicionDespuesDeDepuracionTotal(StringBuilder query){
		query = this.agregaWhereOrAnd(query);
		//query.append("  FUNC('trunc', movimiento.fechaCreacion )  >= FUNC('trunc', movimiento.estimacionCobertura.fechaDepuracionTotal ) ");
		query.append("  movimiento.fechaCreacion   > movimiento.estimacionCobertura.fechaDepuracionTotal  ");
		return query;
	}
	
	
	private void addParametersToQuery(Query query, Map<String,Object> parametersMap){
		Set<String> keys = parametersMap.keySet();
		for(String key: keys){
			Object value = parametersMap.get(key);
			query.setParameter(key,value);
		}
	}
	
	@Override
	public Long obtenerNumeroDeDepuracion(Long idConfiguracionDepuracion) {
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT MAX(depuracion.numeroDepuracion)	");
		queryString.append(" FROM DepuracionReserva depuracion");
		queryString.append(" WHERE depuracion.configuracion.id = :idConfiguracionDepuracion");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idConfiguracionDepuracion"     , idConfiguracionDepuracion);
		Object obj = query.getSingleResult();
		return ((obj==null)?1L:((Long)obj)+1);
	}

}
