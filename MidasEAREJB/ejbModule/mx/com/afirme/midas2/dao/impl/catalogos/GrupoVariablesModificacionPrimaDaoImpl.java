package mx.com.afirme.midas2.dao.impl.catalogos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.catalogos.GrupoVariablesModificacionPrimaDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima_;

@Stateless
public class GrupoVariablesModificacionPrimaDaoImpl extends JpaDao<Long, GrupoVariablesModificacionPrima> implements
GrupoVariablesModificacionPrimaDao {

	public List<GrupoVariablesModificacionPrima> findByFilters(GrupoVariablesModificacionPrima filtroGpoVarModifPrima) {
			CriteriaBuilder cb = entityManager.getCriteriaBuilder();
			Integer intNull = null;
			Expression<Integer> claveExp = cb.literal(filtroGpoVarModifPrima.getClave());
			Expression<Integer> intNullExp = cb.literal(intNull);
			
			CriteriaQuery<GrupoVariablesModificacionPrima> criteriaQuery = cb.createQuery(GrupoVariablesModificacionPrima.class);
			Root<GrupoVariablesModificacionPrima> root = criteriaQuery.from(GrupoVariablesModificacionPrima.class);
			Predicate predicado = cb.and(cb.or(cb.equal(claveExp, intNullExp),cb.equal(root.get(GrupoVariablesModificacionPrima_.clave), filtroGpoVarModifPrima.getClave())),
					cb.like(cb.upper(root.get(GrupoVariablesModificacionPrima_.descripcion)),"%" + filtroGpoVarModifPrima.getDescripcion() + "%"));		
			criteriaQuery.where(predicado);
			TypedQuery<GrupoVariablesModificacionPrima> query = entityManager.createQuery(criteriaQuery);
			return query.getResultList();
	}

	@Override
	public List<GrupoVariablesModificacionPrima> listRelated(Long arg0) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

}
