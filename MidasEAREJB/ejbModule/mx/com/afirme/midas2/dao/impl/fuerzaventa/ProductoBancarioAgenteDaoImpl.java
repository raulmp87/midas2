package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.fuerzaventa.ProductoBancarioAgenteDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioAgente;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.eclipse.persistence.history.AsOfClause;
import org.eclipse.persistence.jpa.JpaEntityManager;
import org.eclipse.persistence.queries.ReadAllQuery;
import org.eclipse.persistence.sessions.Session;
import org.eclipse.persistence.sessions.server.ClientSession;
@Stateless
public class ProductoBancarioAgenteDaoImpl extends EntidadHistoricoDaoImpl implements ProductoBancarioAgenteDao{

	@Override
	public List<ProductoBancarioAgente> findByAgente(Long idAgente, String fechaHistorico) {
		Agente agente=new Agente();
		agente.setId(idAgente);
		ProductoBancarioAgente productoBancarioAgente=new ProductoBancarioAgente();
		productoBancarioAgente.setAgente(agente);
		return findByFilters(productoBancarioAgente,fechaHistorico);
	}
	
	@Override
	public List<ProductoBancarioAgente> findByFilters(ProductoBancarioAgente filtro) {
		/*List<ProductoBancarioAgente> lista=new ArrayList<ProductoBancarioAgente>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ProductoBancarioAgente model left join fetch model.agente left join fetch model.productoBancario ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}
			Agente agente=filtro.getAgente();
			if(agente!=null){
				if(agente.getId()!=null){
					addCondition(queryString, "model.agente.id=:idAgente");
					params.put("idAgente",agente.getId());
				}
			}
			ProductoBancario productoBancario=filtro.getProductoBancario();
			if(productoBancario!=null){
				if(productoBancario.getId()!=null){
					addCondition(queryString, "model.productoBancario.id=:idProducto");
					params.put("idProducto",agente.getId());
				}
			}
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		lista=query.getResultList();
		return lista;*/
		
		return this.findByFilters(filtro, null);
	}

	@Override
	public List<ProductoBancarioAgente> findByFilters(ProductoBancarioAgente filtro, String fechaHistorico) {
		
		boolean isHistorico = (fechaHistorico != null && !fechaHistorico.isEmpty())?true:false;
		
		List<ProductoBancarioAgente> lista=new ArrayList<ProductoBancarioAgente>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ProductoBancarioAgente model left join fetch model.agente left join fetch model.productoBancario ");
		Map<String,Object> params=new HashMap<String, Object>();
		if(filtro!=null){
			if(filtro.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtro.getId());
			}
			if(filtro.getClaveDefault()!=null){
				addCondition(queryString, "model.claveDefault=:claveDefault");
				params.put("claveDefault", filtro.getClaveDefault());
			}
			Agente agente=filtro.getAgente();
			if(agente!=null){
				if(agente.getId()!=null){
					addCondition(queryString, "model.agente.id=:idAgente");
					params.put("idAgente",agente.getId());
				}
			}
			ProductoBancario productoBancario=filtro.getProductoBancario();
			if(productoBancario!=null){
				if(productoBancario.getId()!=null){
					addCondition(queryString, "model.productoBancario.id=:idProducto");
					params.put("idProducto",agente.getId());
				}
			}
		}
		
		if(!isHistorico)
		{
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista=query.getResultList();			
		}
		else
		{
			SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy h:mm:ss.SSSSSS a");
		    Date convertedDate = new Date();
			
			try {
				convertedDate = dateFormat.parse(fechaHistorico);
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}			
			
			JpaEntityManager jpaEntityManager = entityManager.unwrap(JpaEntityManager.class);
			ClientSession clientSession = jpaEntityManager.getServerSession().acquireClientSession();
			AsOfClause asOfClause = new AsOfClause(convertedDate);
			Session historicalSession = clientSession.acquireHistoricalSession(asOfClause);
			
			ReadAllQuery historicalQuery = new ReadAllQuery();
			historicalQuery.setJPQLString(getQueryString(queryString).toString());
			historicalQuery.addArgument("idAgente");
			historicalQuery.addArgumentValue(filtro.getAgente().getId());
			historicalQuery.refreshIdentityMapResult();
			lista = (List<ProductoBancarioAgente>)historicalSession.executeQuery(historicalQuery);			
		}		
		
		return lista;
	}
	/**
	 * Elimina el producto por su id de producto.
	 * @param idProducto
	 * @throws Exception
	 */
	@Override
	public void delete(Long idProducto) throws Exception{
		if(idProducto==null){
			throw new Exception("Producto es nulo");
		}
		ProductoBancarioAgente producto=new ProductoBancarioAgente();
		producto.setId(idProducto);
		List<ProductoBancarioAgente> list=findByFilters(producto);
		if(list==null || list.isEmpty()){
			throw new Exception("Producto con id:"+idProducto+" no existe");
		}
		producto=list.get(0);
		if(producto!=null){
			remove(producto);
		}
	}
}
