/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoAutorizacionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoAutorizacion;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoAutorizacionDaoImpl implements CaTipoAutorizacionDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoAutorizacionDaoImpl.class);
	
	public void save(CaTipoAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaTipoAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoAutorizacion.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoAutorizacion update(CaTipoAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoAutorizacion result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoAutorizacion 	::		CaTipoAutorizacionDaoImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaTipoAutorizacion findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoAutorizacionDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoAutorizacion instance = entityManager.find(CaTipoAutorizacion.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoAutorizacionDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoAutorizacionDaoImpl	::	findById	::	ERROR	::	",re);
	            throw re;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoAutorizacion> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoAutorizacionDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoAutorizacion model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoAutorizacionDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoAutorizacionDaoImpl	::	findByProperty	::	ERROR	::	",re);
			throw re;
		}
	}			
	public List<CaTipoAutorizacion> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoAutorizacion> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoAutorizacion> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoAutorizacion> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoAutorizacion> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoAutorizacionDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoAutorizacion model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoAutorizacionDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoAutorizacionDaoImpl	::	findAll	::	ERROR	::	",re);
			throw re;
		}
	}
}
