package mx.com.afirme.midas2.dao.impl.calculos;
import static mx.com.afirme.midas2.utils.CommonUtils.addCondition;
import static mx.com.afirme.midas2.utils.CommonUtils.getQueryString;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.setQueryParametersByProperties;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.calculos.MovimientoAgenteDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.MovimientoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.comisiones.ConfigComisiones;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.calculos.DetalleCalculoComisionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.util.MidasException;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class MovimientoAgenteDaoImpl extends EntidadDaoImpl implements MovimientoAgenteDao{
	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoAgente> findByFilters(MovimientoAgente filtro) {
		Map<String,Object> params=new HashMap<String, Object>();
		List<MovimientoAgente> lista=new ArrayList<MovimientoAgente>();
		if(!isNull(filtro)){
			final StringBuilder queryString=new StringBuilder("");
			queryString.append("select model from MovimientoAgente model left join fetch model.agente left join fetch model.centroOperacion left join fetch model.agente.persona ");
			Agente agente=filtro.getAgente();
			if(isNotNull(agente)){
				if(isNotNull(agente.getId())){
					addCondition(queryString, "model.agente.id=:idAgente");
					params.put("idAgente", agente.getId());
				}
				if(isNotNull(agente.getIdAgente())){
					addCondition(queryString, "model.agente.idAgente=:claveAgente");
					params.put("claveAgente", agente.getIdAgente());
				}
				Persona persona=agente.getPersona();
				if(isNotNull(persona) && isValid(persona.getNombreCompleto())){
					addCondition(queryString, "UPPER(model.agente.persona.nombreCompleto) like UPPER(:nombreCompleto)");
					params.put("nombreCompleto", "%"+persona.getNombreCompleto()+"%");
				}
			}
			ValorCatalogoAgentes estatus=filtro.getEstatus();
			if(isNotNull(estatus) && isNotNull(estatus.getId())){
				addCondition(queryString, "model.estatus.id=:idEstatus");
				params.put("idEstatus", estatus.getId());
			}
			
			CentroOperacion centroOperacion=filtro.getCentroOperacion();
			if(isNotNull(centroOperacion) && isNotNull(centroOperacion.getId())){
				addCondition(queryString, "model.centroOperacion.id=:idCentroOperacion ");
				params.put("idCentroOperacion", centroOperacion.getId());
			}
			Query query = entityManager.createQuery(getQueryString(queryString));
			setQueryParametersByProperties(query, params);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista=query.getResultList();
		}
		return lista;	
	}
	@Override
	public List<DetalleCalculoComisionesView> obtenerMovimientosPorAgentes(Long idConfig,List<AgenteView> listadoAgentes) throws MidasException{
		if(isNull(idConfig)){
			throw new MidasException("Favor de proporcionar la clave de la configuracion para realizar obtener los movimientos de los agentes.");
		}
		if(!isEmptyList(listadoAgentes)){
			throw new MidasException("Favor de proporcionar la lista de agentes que desea consultar sus movimientos.");
		}
		List<DetalleCalculoComisionesView> movimientos=new ArrayList<DetalleCalculoComisionesView>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		queryString.append(" select   ");
		queryString.append(" gerencia.id as idGerencia, ");
		queryString.append(" prom.id as idPromotoria, ");
		queryString.append(" idAgente, ");
		queryString.append(" (totalImporteComisionAgente+totalImporteIva - totalImporteIvaRet-totalImporteISR) as importeComision ");  
		queryString.append(" from ( ");
		queryString.append(" select  ");
		queryString.append(" agente.id as idAgente, ");
		queryString.append(" agente.idPromotoria as idPromotoria, ");
		queryString.append(" sum(movs.importeComisionAgente) as totalImporteComisionAgente, ");
		queryString.append(" sum(movs.importeIva) as totalImporteIva, ");
		queryString.append(" sum(movs.importeIvaRet) as totalImporteIvaRet, ");
		queryString.append(" sum(movs.importeIsr) as totalImporteISR  ");     
		queryString.append(" from MIDAS.toAgenteMovimientos movs  ");
		queryString.append(" inner join MIDAS.toAgente agente on(movs.idAgente=agente.idAgente) "); 
		queryString.append(" inner join ( ");
		queryString.append(" select id,valor from MIDAS.toValorCatalogoAgentes where grupoCatalogoAgentes_id in (select id from MIDAS.tcGrupoCatalogoAgentes where descripcion like 'Situacion Movimiento Agente') ");    
		queryString.append(" ) situacionMov on (situacionMov.id=movs.estatusMovimiento)     ");
		queryString.append(" where  claveComision=1 and upper(trim(situacionMov.valor)) like 'PENDIENTE' "); 
		queryString.append(" group by agente.id ,agente.idPromotoria ");
		queryString.append(" ) resumen  ");
		queryString.append(" inner join MIDAS.toConfigComisiones conf on(conf.id="+idConfig+")  ");
		queryString.append(" inner join MIDAS.toPromotoria prom on(prom.id=resumen.idPromotoria)  ");
		queryString.append(" left join MIDAS.toEjecutivo ejecutivo on (ejecutivo.id=prom.ejecutivo_id)  ");
		queryString.append(" left join MIDAS.toGerencia gerencia on(gerencia.id=ejecutivo.gerencia_id) ");
		queryString.append(" where (totalImporteComisionAgente+totalImporteIva - totalImporteIvaRet-totalImporteISR) >=conf.importeMinimo ");
		if(!isEmptyList(listadoAgentes)){
			int index=1;
			if(!isEmptyList(listadoAgentes)){
				queryString.append(" resumen.idAgente  in ( ");
				StringBuilder listaExcluyente=new StringBuilder("");
				//Se agregan las gerencias separadas por ,
				int i=0;
				for(AgenteView agente:listadoAgentes){
					if(isNotNull(agente) && isNotNull(agente.getId())){
						if(i<(listadoAgentes.size()-1)){
							listaExcluyente.append(agente.getId()+",");
						}else{
							listaExcluyente.append(agente.getId());
						}
					}
					i++;
				}
//				params.put(index, listaExcluyente.toString());
//				index++;
				queryString.append(listaExcluyente.toString()+") ");
			}
			String finalQuery=getQueryString(queryString)+" order by resumen.idAgente desc ";
			Query query=entityManager.createNativeQuery(finalQuery,DetalleCalculoComisionesView.class);
			if(!params.isEmpty()){
				for(Integer key:params.keySet()){
					query.setParameter(key,params.get(key));
				}
			}
			movimientos=query.getResultList();
		}
		return movimientos;
	}
}
