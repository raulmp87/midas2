package mx.com.afirme.midas2.dao.impl.enlace;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.enlace.CaseDeviationDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dto.enlace.CaseDeviationDTO;

@Stateless
public class CaseDeviationDaoImpl extends EntidadDaoImpl implements CaseDeviationDao {

	@Override
	@SuppressWarnings("unchecked")
	public List<CaseDeviationDTO> getDeviation(String createUser) {
		final StringBuilder script = new StringBuilder(" SELECT model FROM CaseDeviationDTO model ");
		script.append(" WHERE model.createUser = :createUser ");
		script.append(" AND CURRENT_DATE BETWEEN model.iniDate AND model.endDate ");
		final Query query = entityManager.createQuery(script.toString());
		query.setParameter("createUser", createUser);
		return query.getResultList();
	}

}
