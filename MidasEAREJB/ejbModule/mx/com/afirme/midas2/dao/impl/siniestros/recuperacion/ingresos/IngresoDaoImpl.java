package mx.com.afirme.midas2.dao.impl.siniestros.recuperacion.ingresos;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.ingresos.IngresoDao;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.AplicacionCobranza;
import mx.com.afirme.midas2.dto.siniestros.ListarIngresoDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.DepositoBancarioDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoAcreedorDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.MovimientoManualDTO;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.lang.StringUtils;

import com.js.util.StringUtil;
@Stateless
public class IngresoDaoImpl implements IngresoDao{
	@EJB
	EntidadDao entidadDao;

	@SuppressWarnings("unchecked")
	@Override
	public List<ListarIngresoDTO> buscarIngresos(ListarIngresoDTO filtroIngreso) {
		List<ListarIngresoDTO> lista  = new ArrayList<ListarIngresoDTO>();
		if(!StringUtils.isEmpty(filtroIngreso.getIngresosConcat())){
			String validaIngresosConcat = filtroIngreso.getIngresosConcat();
			validaIngresosConcat= validaIngresosConcat.trim().replace(",", "");
			if(StringUtils.isEmpty(validaIngresosConcat.trim())){
				filtroIngreso.setIngresosConcat(null);
			}else{
				filtroIngreso.setIngresosConcat(filtroIngreso.getIngresosConcat().trim());
			}
		}
		String spName = "MIDAS.PKGSIN_RECUPERACIONES.buscarIngresos"; 
		StoredProcedureHelper storedHelper  = null;
		try {
			String propiedades = "ingresoId,numeroIngreso,numeroRecuperacion,tipoRecuperacionDesc,medioRecuperacionDesc,estatusDesc," +
			"oficinaDesc,claveDeudor,nombreDeudor,referencia,numeroReporte,numeroSiniestro,fechaRegistro,fechaIngreso," +
			"fechaCancelacion,montoFinalRecuperado,estatus,tipoRecuperacion,medioRecuperacion,referenciaIdentificada,estatusFacturado,permitirFacturacion";

			String columnasBaseDatos = "ingresoId,numeroIngreso,numeroRecuperacion,tipoRecuperacionDesc,medioRecuperacionDesc,estatusDesc," +
			"oficinaDesc,claveDeudor,nombreDeudor,referencia,numeroReporte,numeroSiniestro,fechaRegistro,fechaIngreso," +
			"fechaCancelacion,monto,estatus,tipoRecuperacion,medioRecuperacion,referenciaCompania,estatusFacturado,permitirFacturacion";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(ListarIngresoDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltro(storedHelper, filtroIngreso);
			lista = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de INGRESOS ..."+descErr
						, Level.WARNING, e);    
			}
		}
		return lista;
	}

	private Object convertToNull(Object obj){
		if( obj instanceof String){
			obj = (obj != null && ((String)obj).equals(""))?null:obj;
		}else if ( obj instanceof Boolean){
			obj = (obj != null && ((Boolean)obj)== Boolean.FALSE)?null:obj;
		}
		return obj;
	}

	private void agregaParametrosDelFiltro(StoredProcedureHelper storedHelper,ListarIngresoDTO filtro){

		storedHelper.estableceParametro("p_ingresosConcat"     , this.convertToNull( filtro.getIngresosConcat()   ));
		storedHelper.estableceParametro("p_numeroRecuperacion" , this.convertToNull( filtro.getNumeroRecuperacion() ));
		storedHelper.estableceParametro("p_numeroIngreso"      , filtro.getNumeroIngreso());
		storedHelper.estableceParametro("p_tipoRecuperacion"   , this.convertToNull( filtro.getTipoRecuperacion() ));
		storedHelper.estableceParametro("p_medioRecuperacion"  , this.convertToNull( filtro.getMedioRecuperacion() ));
		storedHelper.estableceParametro("p_fechaIniRegistro"   , this.convertToNull( filtro.getFechaIniPendiente() ));
		storedHelper.estableceParametro("p_fechaFinRegistro"   , this.convertToNull( filtro.getFechaFinPendiente() ));
		storedHelper.estableceParametro("p_fechaIniCancelacion", this.convertToNull( filtro.getFechaIniCancelacion() ));
		storedHelper.estableceParametro("p_fechaFinCancelacion", this.convertToNull( filtro.getFechaFinCancelacion() ));
		storedHelper.estableceParametro("p_fechaIniAplicacion" , this.convertToNull( filtro.getFechaIniAplicacion() ));
		storedHelper.estableceParametro("p_fechaFinAplicacion" , this.convertToNull( filtro.getFechaFinAplicacion() ));
		storedHelper.estableceParametro("p_numeroSiniestro"    , this.convertToNull( filtro.getNumeroSiniestro() ));
		storedHelper.estableceParametro("p_numeroReporte"      , this.convertToNull( filtro.getNumeroReporte() ));
		storedHelper.estableceParametro("p_estatusRecuperacion", this.convertToNull( filtro.getEstatus() ));
		storedHelper.estableceParametro("p_claveDeudor"        , this.convertToNull( filtro.getClaveDeudor() ));
		storedHelper.estableceParametro("p_nombreDeudor"       , this.convertToNull( filtro.getNombreDeudor() ));
		storedHelper.estableceParametro("p_oficinaId"          , this.convertToNull( filtro.getOficinaId() ));
		storedHelper.estableceParametro("p_montoIniRecuperado" , this.convertToNull( filtro.getMontoIniRecuperacion() ));
		storedHelper.estableceParametro("p_montoFinRecuperado" , this.convertToNull( filtro.getMontoFinRecuperacion() ));
		storedHelper.estableceParametro("p_montoIniIngreso"    , this.convertToNull( filtro.getMontoIniIngreso() ));
		storedHelper.estableceParametro("p_montoFinIngreso"    , this.convertToNull( filtro.getMontoFinIngreso() ));
		storedHelper.estableceParametro("p_servicio"           , this.convertToNull( ( filtro.getServicio() != null ? ( filtro.getServicio() == 0 ? null : filtro.getServicio() ) : null  )  ));
		storedHelper.estableceParametro("p_referencia"         , this.convertToNull( filtro.getReferencia() ));
		storedHelper.estableceParametro("p_referenciaIdentificada", this.convertToNull( filtro.getReferenciaIdentificada()));
		storedHelper.estableceParametro("p_filtro"                                      , this.convertToNull(filtro.getBanderaBusquedaConFiltro()));
		storedHelper.estableceParametro("p_pantallaOrigen"                  , this.convertToNull(filtro.getPantallaOrigen()));

	}
	
	

	/**
	 * <b>SE AGREGA EN DISEÑO APLICAR INGRESO MANUAL</b>
	 * <b>
	 * </b>Método que actualiza el Saldo del REFUNIC
	 * Invocar a la Interfaz de SEYCOS <b><i>SEYCOS.PKG_INT_MIDAS_SIN.
	 * STPAPLICAREFUNIC</i></b>
	 * <b>
	 * </b>
	 * 
	 * @param refunic
	 * @param monto
	 * @param descripcion
	 */
	@Override
	public void aplicaRefunic(String refunic, BigDecimal monto, String descripcion){
		Integer resultado = null;
		String spName = "SEYCOS.PKG_INT_MIDAS_SIN.STPAPLICAREFUNIC";
		StoredProcedureHelper storedHelper                                         = null;
		try {
			storedHelper = new StoredProcedureHelper(spName);
			storedHelper.estableceParametro("pidrefunic"     , this.convertToNull( refunic ));
			storedHelper.estableceParametro("pmonto"   , this.convertToNull(monto));
			if(descripcion.length()>200){
				descripcion =  descripcion.substring(0, 200);
			}
			storedHelper.estableceParametro("preferencia"   , this.convertToNull(descripcion  ));
			storedHelper.ejecutaActualizar();
		} catch (Exception e) {
			String descErr = null;
			String mj= "Excepcion general en Aplicar Refunic.";
			if (storedHelper != null) {                  
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(mj+descErr, Level.WARNING, e);
			}
			new MidasException(mj+descErr);
		}


	}

	@Override
	public List<DepositoBancarioDTO> buscarDepositosBancarios(
			DepositoBancarioDTO filtro, String depositosConcat,
			Boolean esConsulta) {
		// TODO Auto-generated method stub
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoAcreedorDTO> buscarMovimientosAcreedores(MovimientoAcreedorDTO filtro, String movAcreedoresConcat,Boolean esConsulta) {
		List<MovimientoAcreedorDTO> lista  = new ArrayList<MovimientoAcreedorDTO>();
		String spName = "MIDAS.PKGSIN_RECUPERACIONES.buscarMovimientosAcreedores";
		StoredProcedureHelper storedHelper                                         = null;
		try {
			String propiedades = 
				"movimientoId,cuentaContable,idCuentaContable,refunic,tipoRecuperacionDesc,siniestroOrigen,motivoCancelacionDesc,fechaTraspaso,importe";

			String columnasBaseDatos = 
				"idMovimiento,cuentaContable,idCuentaContable,refunic,tipoRecuperacionDesc,siniestroOrigen,motivoCancelacionDesc,fechaTraspaso,importe";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(MovimientoAcreedorDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltroMovAcreedores(storedHelper, filtro, esConsulta);
			lista = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de INGRESOS ..."+descErr
						, Level.WARNING, e);    
			}
		}
		return lista;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoAcreedorDTO> buscarMovimientosAcreedoresPorCuenta(Long cuentaId){
		List<MovimientoAcreedorDTO> lista  = new ArrayList<MovimientoAcreedorDTO>();
		String spName = "MIDAS.PKGSIN_RECUPERACIONES_.buscarMovtoAcreedorPorCuenta";
		StoredProcedureHelper storedHelper                                         = null;
		try {
			String propiedades = 
				"movimientoId,cuentaContable,idCuentaContable,refunic,tipoRecuperacionDesc,siniestroOrigen,motivoCancelacionDesc,fechaTraspaso,importe";

			String columnasBaseDatos = 
				"idMovimiento,cuentaContable,idCuentaContable,refunic,tipoRecuperacionDesc,siniestroOrigen,motivoCancelacionDesc,fechaTraspaso,importe";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(MovimientoAcreedorDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			storedHelper.estableceParametro("p_cuentaContable"      , this.convertToNull( cuentaId));
			lista = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de INGRESOS ..."+descErr
						, Level.WARNING, e);    
			}
		}
		return lista;
	}
	
	/**
	 * establece los parametros para el stored procedure de busqueda de movimientos acreedores
	 * @param storedHelper
	 * @param filtro
	 */
	private void agregaParametrosDelFiltroMovAcreedores(StoredProcedureHelper storedHelper,MovimientoAcreedorDTO filtro, Boolean esConsulta){
		storedHelper.estableceParametro("p_movAcreedoresConcat" , this.convertToNull( filtro.getMovAcreedoresConcat()  ));
		storedHelper.estableceParametro("p_cuentaContable"      , this.convertToNull( filtro.getCuentaContable()));
		storedHelper.estableceParametro("p_refunic"                              , this.convertToNull( filtro.getRefunic()  ));
		storedHelper.estableceParametro("p_tipoRecuperacion"    , this.convertToNull( filtro.getTipoRecuperacion()  ));
		storedHelper.estableceParametro("p_siniestroOrigen"     , this.convertToNull( filtro.getSiniestroOrigen()  ));
		storedHelper.estableceParametro("p_motivoCancelacion"   , this.convertToNull( filtro.getMotivoCancelacion()  ));
		storedHelper.estableceParametro("p_fechaTraspasoIni"    , this.convertToNull( filtro.getFechaTraspasoIni()  ));
		storedHelper.estableceParametro("p_fechaTraspasoFin"    , this.convertToNull( filtro.getFechaTraspasoFin()  ));
		storedHelper.estableceParametro("p_importeIni"                       , this.convertToNull( filtro.getImporteIni()  ));
		storedHelper.estableceParametro("p_importeFin"                      , this.convertToNull( filtro.getImporteFin()  ));
		storedHelper.estableceParametro("p_filtro"                                                   , this.convertToNull( (esConsulta)? 1 : 0));
	}

	@Override
	public void contabilizaAplicacionIngreso(Long aplicacionId)  throws MidasException {

		String spName = "MIDAS.PKG_CONTABILIDAD.CONTABILIZAAPLICACIONINGRESO";                  
		StoredProcedureHelper storedHelper                                         = null;
		AplicacionCobranza aplicacion = this.entidadDao.findById(AplicacionCobranza.class, aplicacionId);
		AplicacionCobranza resultado =new AplicacionCobranza();
		try { 
			LogDeMidasEJB3.log("Entrando a contabilizaAplicacionIngreso..." + this, Level.INFO, null);                                       
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdAplicacion", aplicacionId);
			storedHelper
			.estableceMapeoResultados(
					AplicacionCobranza.class.getCanonicalName(),
					"idContable", "lote");
			resultado = (AplicacionCobranza) storedHelper.obtieneResultadoSencillo();

			LogDeMidasEJB3.log("Saliendo de contabilizaAplicacionIngreso, aplicacionId - " + aplicacionId 
					+ this, Level.INFO, null);
			if(null!= resultado && null!= resultado.getIdContable()){
				aplicacion.setIdContable(resultado.getIdContable());
				this.entidadDao.update(aplicacion);
			}

		} catch (Exception e) {
			String descErr = null;
			String mj ="Excepcion general al contabilizar Ingreso. ";
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta(); 
				LogDeMidasInterfaz.log(mj+descErr, Level.WARNING, e);    
			}

			throw new MidasException(mj+( (!StringUtil.isEmpty(descErr) ) ? descErr : ""));
		}


	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Boolean contabilizaCancelacion(Long aplicacionCobranzaId) {
		// TODO: SE REQUIERE FUNCIÓN DE ARTURO
		return true;
	}




	@SuppressWarnings("unchecked")
	@Override
	public List<DepositoBancarioDTO> buscarDepositosBancarios(DepositoBancarioDTO filtroDeposito, Boolean esConsulta){
		List<DepositoBancarioDTO> resultado = null;

		String spName = "SEYCOS.PKG_INT_MIDAS_SIN.clistarefunic"; 
		StoredProcedureHelper storedHelper                                         = null;
		try {
			String[] propiedades = new String[]{"referencia","descripcionMovimiento","bancoId","banco","cuentaId","numeroCuenta","refunic","fechaDeposito","monto","identificadorGuia"};

			String[] columnasBaseDatos = new String[]{"referencia","descripcion","banco","nombre","idcuenta","cuenta","refunic","fecdeposito","monto","identificadorGuia"};
			storedHelper = new StoredProcedureHelper(spName);
			storedHelper.estableceMapeoResultados(DepositoBancarioDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			agregaParametrosDelFiltro(storedHelper, filtroDeposito, esConsulta);
			resultado = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de INGRESOS ..."+descErr
						, Level.WARNING, e);    
			}
		}

		return resultado;
	}

	private void agregaParametrosDelFiltro(StoredProcedureHelper storedHelper,DepositoBancarioDTO filtro, Boolean esConsulta){

		storedHelper.estableceParametro("pBConsulta"     , this.convertToNull( esConsulta ));
		storedHelper.estableceParametro("pdescripcion" , this.convertToNull( filtro.getDescripcionMovimiento() ));
		storedHelper.estableceParametro("pfechaini"   , this.convertToNull( filtro.getFechaDepositoIni() ));
		storedHelper.estableceParametro("pfechafin"  , this.convertToNull( filtro.getFechaDepositoFin() ));
		storedHelper.estableceParametro("pmontoini"   , this.convertToNull( filtro.getMontoIni() ));
		storedHelper.estableceParametro("pmontofin"   , this.convertToNull( filtro.getMontoFin() ));
		storedHelper.estableceParametro("preferencia", this.convertToNull( filtro.getReferencia() ));
		storedHelper.estableceParametro("pidrefunic", this.convertToNull( filtro.getRefunic() ));
		storedHelper.estableceParametro("ppreseleccion" , this.convertToNull(filtro.getDepositosConcat() ));
		storedHelper.estableceParametro("pidbanco" , this.convertToNull( filtro.getBancoId() ));
		storedHelper.estableceParametro("pIdcuenta"    , this.convertToNull( filtro.getNumeroCuenta() ));
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<MovimientoManualDTO> buscarMovimientosManuales(MovimientoManualDTO filtro, String movimientosConcat, Boolean esConsulta) {
		List<MovimientoManualDTO> lista  = new ArrayList<MovimientoManualDTO>();
		String spName = "MIDAS.PKGSIN_RECUPERACIONES.buscarMovimientosManuales";
		StoredProcedureHelper storedHelper = null;
		try {
			String propiedades = 
				"idMovimiento,codigoCuentaContable,cuentaContable,idCuentaContable,causaMovimiento,usuario,fechaTraspasoCuenta,importe";

			String columnasBaseDatos = 
				"idMovimiento,codigoCuentaContable,cuentaContable,idCuentaContable,causaMovimiento,usuario,fechaTraspaso,importe";
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(MovimientoManualDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
			storedHelper.estableceParametro("p_movManualesConcat"  , this.convertToNull( filtro.getMovManualConcat()));
			storedHelper.estableceParametro("p_cuentaContable"   , this.convertToNull( filtro.getCuentaContable()));
			storedHelper.estableceParametro("p_causaMovimiento"  , this.convertToNull( filtro.getCausaMovimiento()));
			storedHelper.estableceParametro("p_fechaTraspasoIni" , filtro.getFechaTraspasoCuentaDesde());
			storedHelper.estableceParametro("p_fechaTraspasoFin" , filtro.getFechaTraspasoCuentaHasta());
			storedHelper.estableceParametro("p_importeIni"       , filtro.getImporteDesde());
			storedHelper.estableceParametro("p_importeFin"       , filtro.getImporteHasta());
			storedHelper.estableceParametro("p_filtro"           , esConsulta? 1 : 0);
			lista = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			String descErr = null;
			if (storedHelper != null) {
				descErr = storedHelper.getDescripcionRespuesta();
				LogDeMidasInterfaz.log(
						"Excepcion general en busqueda de INGRESOS ..."+descErr
						, Level.WARNING, e);    
			}
		}
		return lista;
	}
}
