/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaCalculoBancaDao;
import mx.com.afirme.midas2.domain.compensaciones.CaCalculoBanca;

import org.apache.log4j.Logger;

@Stateless
public class CaCalculoBancaDaoImpl implements CaCalculoBancaDao {
	public static final String LINEANEGOCIO_ID = "lineanegocioId";
	public static final String MONTO = "monto";
	public static final String PORCENTAJE = "porcentaje";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaCalculoBancaDaoImpl.class);
	
	public void save(CaCalculoBanca entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaCalculoBanca entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaCalculoBanca.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaCalculoBanca update(CaCalculoBanca entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaCalculoBanca result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaCalculoBanca 	::		CaCalculoBancaDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaCalculoBanca findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaCalculoBancaDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaCalculoBanca instance = entityManager.find(CaCalculoBanca.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaCalculoBancaDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaCalculoBancaDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }
    
    @SuppressWarnings("unchecked")
    public List<CaCalculoBanca> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaCalculoBancaDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaCalculoBanca model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaCalculoBancaDaoImpl	::	findByProperty	::	FIN	::	");
					return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaCalculoBancaDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaCalculoBanca> findByLineanegocioId(Object lineanegocioId
	) {
		return findByProperty(LINEANEGOCIO_ID, lineanegocioId
		);
	}
	
	public List<CaCalculoBanca> findByMonto(Object monto
	) {
		return findByProperty(MONTO, monto
		);
	}
	
	public List<CaCalculoBanca> findByPorcentaje(Object porcentaje
	) {
		return findByProperty(PORCENTAJE, porcentaje
		);
	}
	
	public List<CaCalculoBanca> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaCalculoBanca> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaCalculoBanca> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaCalculoBancaDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaCalculoBanca model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaCalculoBancaDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaCalculoBancaDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
