package mx.com.afirme.midas2.dao.impl.suscripcion.impresion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.suscripcion.impresion.EdicionImpresionDAO;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionEndoso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionInciso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionPoliza;

import org.joda.time.DateTime;

@Stateless
public class EdicionImpresionDAOImpl extends EntidadDaoImpl implements EdicionImpresionDAO{

	@SuppressWarnings("unchecked")
	@Override
	public EdicionPoliza buscarEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual){
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT edicion ");
		queryString.append(" FROM EdicionPoliza  edicion ");
		queryString.append(" WHERE edicion.id.idToPoliza = :idToPoliza ");
		queryString.append(" AND edicion.id.validOn = :validOn ");
		queryString.append(" AND edicion.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicion.id.claveTipoEndoso = :claveTipoEndoso ");
		queryString.append(" AND edicion.id.esSituacionActual = :esSituacionActual ");
		queryString.append(" AND edicion.id.version = ( ");
		queryString.append(" SELECT MAX(edicionSubq.id.version) FROM EdicionPoliza edicionSubq ");
		queryString.append(" WHERE edicionSubq.id.idToPoliza = :idToPoliza ");
		queryString.append(" AND edicionSubq.id.validOn = :validOn ");
		queryString.append(" AND edicionSubq.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicionSubq.id.claveTipoEndoso = :claveTipoEndoso ");
		queryString.append(" AND edicionSubq.id.esSituacionActual = :esSituacionActual ) ");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idToPoliza"     , idToPoliza);
		query.setParameter("validOn"     , validOn.toDate());
		query.setParameter("recordFrom"     , recordFrom.toDate());
		query.setParameter("claveTipoEndoso"     , claveTipoEndoso);
		query.setParameter("esSituacionActual"     , esSituacionActual);
		List<EdicionPoliza> ediciones = query.getResultList();
		if(ediciones != null && !ediciones.isEmpty()){
			return ediciones.get(0);
		}
		return null;
	}
	
	@Override
	public Long obtenerSecuenciaEdicionPoliza(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso, Boolean esSituacionActual){
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT MAX(edicion.id.version) ");
		queryString.append(" FROM EdicionPoliza  edicion ");
		queryString.append(" WHERE edicion.id.idToPoliza = :idToPoliza ");
		queryString.append(" AND edicion.id.validOn = :validOn ");
		queryString.append(" AND edicion.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicion.id.claveTipoEndoso = :claveTipoEndoso ");
		queryString.append(" AND edicion.id.esSituacionActual = :esSituacionActual ");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idToPoliza"     , idToPoliza);
		query.setParameter("validOn"     , validOn.toDate());
		query.setParameter("recordFrom"     , recordFrom.toDate());
		query.setParameter("claveTipoEndoso"     , claveTipoEndoso);
		query.setParameter("esSituacionActual"     , esSituacionActual);
		Object secuencia = query.getSingleResult();
		return ((secuencia==null)?1:((Long)secuencia)+1);
	}

	@SuppressWarnings("unchecked")
	@Override
	public EdicionInciso buscarEdicionInciso(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso){
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT edicion ");
		queryString.append(" FROM EdicionInciso  edicion ");
		queryString.append(" WHERE edicion.id.idToPoliza = :idToPoliza ");
		queryString.append(" AND edicion.id.validOn = :validOn ");
		queryString.append(" AND edicion.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicion.id.claveTipoEndoso = :claveTipoEndoso ");
		queryString.append(" AND edicion.id.version = ( ");
		queryString.append(" SELECT MAX(edicionSubq.id.version) FROM EdicionInciso edicionSubq ");
		queryString.append(" WHERE edicionSubq.id.idToPoliza = :idToPoliza ");
		queryString.append(" AND edicionSubq.id.validOn = :validOn ");
		queryString.append(" AND edicionSubq.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicionSubq.id.claveTipoEndoso = :claveTipoEndoso ) ");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idToPoliza"     , idToPoliza);
		query.setParameter("validOn"     , validOn.toDate());
		query.setParameter("recordFrom"     , recordFrom.toDate());
		query.setParameter("claveTipoEndoso"     , claveTipoEndoso);
		List<EdicionInciso> ediciones = query.getResultList();
		if(ediciones != null && !ediciones.isEmpty()){
			return ediciones.get(0);
		}
		return null;
	}
	
	@Override
	public Long obtenerSecuenciaEdicionInciso(BigDecimal idToPoliza, DateTime validOn, 
			DateTime recordFrom, Short claveTipoEndoso){
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT MAX(edicion.id.version) ");
		queryString.append(" FROM EdicionInciso  edicion ");
		queryString.append(" WHERE edicion.id.idToPoliza = :idToPoliza ");
		queryString.append(" AND edicion.id.validOn = :validOn ");
		queryString.append(" AND edicion.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicion.id.claveTipoEndoso = :claveTipoEndoso ");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idToPoliza"     , idToPoliza);
		query.setParameter("validOn"     , validOn.toDate());
		query.setParameter("recordFrom"     , recordFrom.toDate());
		query.setParameter("claveTipoEndoso"     , claveTipoEndoso);
		Object secuencia = query.getSingleResult();
		return ((secuencia==null)?1:((Long)secuencia)+1);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public EdicionEndoso buscarEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso){
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT edicion ");
		queryString.append(" FROM EdicionEndoso  edicion ");
		queryString.append(" WHERE edicion.id.idToCotizacion = :idToCotizacion ");
		queryString.append(" AND edicion.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicion.id.claveTipoEndoso = :claveTipoEndoso ");
		queryString.append(" AND edicion.id.version = ( ");
		queryString.append(" SELECT MAX(edicionSubq.id.version) FROM EdicionEndoso edicionSubq ");
		queryString.append(" WHERE edicionSubq.id.idToCotizacion = :idToCotizacion ");
		queryString.append(" AND edicionSubq.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicionSubq.id.claveTipoEndoso = :claveTipoEndoso ) ");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idToCotizacion"     , idToCotizacion);
		query.setParameter("recordFrom"     , recordFrom);
		query.setParameter("claveTipoEndoso"     , claveTipoEndoso);
		List<EdicionEndoso> ediciones = query.getResultList();
		if(ediciones != null && !ediciones.isEmpty()){
			return ediciones.get(0);
		}
		return null;
	}
	
	@Override
	public Long obtenerSecuenciaEdicionEndoso(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso){
		StringBuilder queryString = new StringBuilder();
		queryString.append(" SELECT MAX(edicion.id.version) ");
		queryString.append(" FROM EdicionEndoso  edicion ");
		queryString.append(" WHERE edicion.id.idToCotizacion = :idToCotizacion ");
		queryString.append(" AND edicion.id.recordFrom = :recordFrom ");
		queryString.append(" AND edicion.id.claveTipoEndoso = :claveTipoEndoso ");
		Query query = this.entityManager.createQuery(queryString.toString());
		query.setParameter("idToCotizacion"     , idToCotizacion);
		query.setParameter("recordFrom"     , recordFrom);
		query.setParameter("claveTipoEndoso"     , claveTipoEndoso);
		Object secuencia = query.getSingleResult();
		return ((secuencia==null)?1:((Long)secuencia)+1);
	}
	
}
