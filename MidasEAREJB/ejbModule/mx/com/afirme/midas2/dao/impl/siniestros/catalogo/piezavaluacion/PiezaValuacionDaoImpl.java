package mx.com.afirme.midas2.dao.impl.siniestros.catalogo.piezavaluacion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.siniestros.catalogo.piezavaluacion.PiezaValuacionDao;
import mx.com.afirme.midas2.domain.siniestros.catalogo.pieza.PiezaValuacion;
import mx.com.afirme.midas2.service.siniestros.catalogo.pieza.PiezaValuacionService.PiezaValuacionFiltro;
import mx.com.afirme.midas2.util.JpaUtil;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class PiezaValuacionDaoImpl extends JpaDao<Long, PiezaValuacion> implements PiezaValuacionDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<PiezaValuacion> buscar(
			PiezaValuacionFiltro piezaValuacionFiltro){
		Map<String, Object> params = new HashMap<String, Object>();	
		final StringBuilder builder = new StringBuilder("SELECT new mx.com.afirme.midas2.domain.siniestros.catalogo.pieza.PiezaValuacion(");
			builder.append("model.id, model.seccionAutomovil, model.descripcion, model.estatus, ");
			builder.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'SECCION_AUTOMOVIL', model.seccionAutomovil), ");
			builder.append("FUNC('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', 'ESTATUS', model.estatus)) FROM PiezaValuacion model ");
			JpaUtil.addFilterParameter("model", builder, piezaValuacionFiltro, params);
			builder.append("  ORDER BY model.id ");
		Query queryResult = entityManager.createQuery(builder.toString(), PiezaValuacion.class);
		JpaUtil.setQueryParametersByProperties(queryResult, params);
		queryResult.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return queryResult.getResultList();	
		//List<PiezaValuacion> data =  super.catalogoSiniestroDAO.buscar(PiezaValuacion.class, piezaValuacionFiltro, "id");		
		
	}
}
