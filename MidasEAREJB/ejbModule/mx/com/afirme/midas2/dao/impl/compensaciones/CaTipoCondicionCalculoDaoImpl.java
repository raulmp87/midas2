/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoCondicionCalculoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCondicionCalculo;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoCondicionCalculoDaoImpl implements CaTipoCondicionCalculoDao {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoCondicionCalculoDaoImpl.class);
	
	public void save(CaTipoCondicionCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaTipoCondicionCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoCondicionCalculo.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoCondicionCalculo update(CaTipoCondicionCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoCondicionCalculo result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoDaoImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoCondicionCalculo findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoCondicionCalculoDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoCondicionCalculo instance = entityManager.find(CaTipoCondicionCalculo.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoCondicionCalculoDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoCondicionCalculoDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoCondicionCalculo> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoCondicionCalculoDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoCondicionCalculo model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoCondicionCalculoDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoCondicionCalculoDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoCondicionCalculo> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoCondicionCalculo> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoCondicionCalculo> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoCondicionCalculo> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoCondicionCalculo> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoCondicionCalculoDaoImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoCondicionCalculo model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoCondicionCalculoDaoImpl	::	findAll	::	FIN	::	");
//			return query.getResultList();
			return findByProperty(BORRADOLOGICO, ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoCondicionCalculoDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
