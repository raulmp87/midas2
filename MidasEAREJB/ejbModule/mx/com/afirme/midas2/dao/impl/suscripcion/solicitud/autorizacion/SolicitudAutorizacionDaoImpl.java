package mx.com.afirme.midas2.dao.impl.suscripcion.solicitud.autorizacion;

import java.math.BigDecimal;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.solicitud.autorizacion.SolicitudAutorizacionDao;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class SolicitudAutorizacionDaoImpl extends JpaDao<Long, SolicitudExcepcionCotizacion> implements SolicitudAutorizacionDao {

	@Override
	public List<SolicitudExcepcionCotizacion> listarSolicitudes(
			SolicitudExcepcionCotizacion filtro){
		return listarSolicitudes(filtro, true);
	}
	
	@Override
	public Long listarSolicitudesCount(SolicitudExcepcionCotizacion filtro, boolean listarHistorico) {
		StringBuilder queryString = new StringBuilder("select count(model) from SolicitudExcepcionCotizacion model ");		
		queryString.append("where (:cveNegocio is null or model.cveNegocio = :cveNegocio) ");		
		if(filtro.getToIdCotizacion() != null){
			queryString.append(" and model.toIdCotizacion = :idCotizacion");
			if(!listarHistorico){
				queryString.append(" and model.id in (select max(model2.id) from SolicitudExcepcionCotizacion model2 where " +
					"model2.toIdCotizacion = :idCotizacion ) ");
			}
		}
		if(filtro.getEstatus() != null){
			queryString.append(" and model.estatus = :estatus");
		}
		if(filtro.getUsuarioAutorizador() != null){
			queryString.append(" and model.usuarioAutorizador = :usuarioAutorizador");
		}
		if(filtro.getUsuarioSolicitante() != null){
			queryString.append(" and model.usuarioSolicitante = :usuarioSolicitante");
		}
		if(filtro.getId() != null){
			queryString.append(" and model.id = :id");
		}
		if(filtro.getFechaSolicitud() != null){
			queryString.append(" and model.fechaSolicitud >= :fechaSolicitud");
		}
		if(filtro.getFechaHasta() != null){
			queryString.append(" and model.fechaSolicitud <= :fechaHasta");
		}
		queryString.append(" order by model.id DESC ");
		Query query = entityManager.createQuery(queryString.toString());	
		
		query.setParameter("cveNegocio", filtro.getCveNegocio());		
		// Se agregan los parametros
		if(filtro.getToIdCotizacion() != null){
			query.setParameter("idCotizacion", filtro.getToIdCotizacion());
		}
		if(filtro.getUsuarioAutorizador() != null){
			query.setParameter("usuarioAutorizador", filtro.getUsuarioAutorizador());
		}
		if(filtro.getUsuarioSolicitante() != null){
			query.setParameter("usuarioSolicitante", filtro.getUsuarioSolicitante());
		}
		if(filtro.getEstatus() != null){
			query.setParameter("estatus", filtro.getEstatus());
		}
		if(filtro.getId() != null){
			query.setParameter("id", filtro.getId());
		}
		if (filtro.getFechaHasta() != null && filtro.getFechaSolicitud() != null) {
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(filtro.getFechaSolicitud());
			gcFechaInicio.add(GregorianCalendar.DATE, 1);
			gcFechaInicio.add(GregorianCalendar.MILLISECOND, -1);
			query.setParameter("fechaSolicitud", gcFechaInicio.getTime());
			
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(filtro.getFechaHasta());
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			query.setParameter("fechaHasta", gcFechaFin.getTime());
		}else{
			if(filtro.getFechaSolicitud() != null){
				query.setParameter("fechaSolicitud", filtro.getFechaSolicitud());
			}
			if (filtro.getFechaHasta() != null) {
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(filtro.getFechaHasta());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				query.setParameter("fechaHasta", gcFechaFin.getTime());
			}
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		if(filtro.getPrimerRegistroACargar() != null && filtro.getNumeroMaximoRegistrosACargar() != null){
			query.setFirstResult(filtro.getPrimerRegistroACargar());
			query.setMaxResults(filtro.getNumeroMaximoRegistrosACargar());
		}
		
		BigDecimal numero = BigDecimal.ZERO;
		try{
			Object object = query.getSingleResult();
			
			if(object instanceof BigDecimal)
				numero = (BigDecimal) object;
			else if (object instanceof Long)
				numero = BigDecimal.valueOf((Long)object);
			else if (object instanceof Double)
				numero = BigDecimal.valueOf((Double)object);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return numero.longValue();		
	}
	
	
	@Override
	public List<SolicitudExcepcionCotizacion> listarSolicitudes(
			SolicitudExcepcionCotizacion filtro, boolean listarHistorico) {
		StringBuilder queryString = new StringBuilder("select model from SolicitudExcepcionCotizacion model ");		
		queryString.append("where (:cveNegocio is null or model.cveNegocio = :cveNegocio) ");		
		if(filtro.getToIdCotizacion() != null){
			queryString.append(" and model.toIdCotizacion = :idCotizacion");
			if(!listarHistorico){
				queryString.append(" and model.id in (select max(model2.id) from SolicitudExcepcionCotizacion model2 where " +
					"model2.toIdCotizacion = :idCotizacion ) ");
			}
		}
		if(filtro.getEstatus() != null){
			queryString.append(" and model.estatus = :estatus");
		}
		if(filtro.getUsuarioAutorizador() != null){
			queryString.append(" and model.usuarioAutorizador = :usuarioAutorizador");
		}
		if(filtro.getUsuarioSolicitante() != null){
			queryString.append(" and model.usuarioSolicitante = :usuarioSolicitante");
		}
		if(filtro.getId() != null){
			queryString.append(" and model.id = :id");
		}
		if(filtro.getFechaSolicitud() != null){
			queryString.append(" and model.fechaSolicitud >= :fechaSolicitud");
		}
		if(filtro.getFechaHasta() != null){
			queryString.append(" and model.fechaSolicitud <= :fechaHasta");
		}
		queryString.append(" order by model.id DESC ");
		Query query = entityManager.createQuery(queryString.toString());	
		
		query.setParameter("cveNegocio", filtro.getCveNegocio());		
		// Se agregan los parametros
		if(filtro.getToIdCotizacion() != null){
			query.setParameter("idCotizacion", filtro.getToIdCotizacion());
		}
		if(filtro.getUsuarioAutorizador() != null){
			query.setParameter("usuarioAutorizador", filtro.getUsuarioAutorizador());
		}
		if(filtro.getUsuarioSolicitante() != null){
			query.setParameter("usuarioSolicitante", filtro.getUsuarioSolicitante());
		}
		if(filtro.getEstatus() != null){
			query.setParameter("estatus", filtro.getEstatus());
		}
		if(filtro.getId() != null){
			query.setParameter("id", filtro.getId());
		}
		if (filtro.getFechaHasta() != null && filtro.getFechaSolicitud() != null) {
			GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(filtro.getFechaSolicitud());
			gcFechaInicio.add(GregorianCalendar.DATE, 1);
			gcFechaInicio.add(GregorianCalendar.MILLISECOND, -1);
			query.setParameter("fechaSolicitud", gcFechaInicio.getTime());
			
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(filtro.getFechaHasta());
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			query.setParameter("fechaHasta", gcFechaFin.getTime());
		}else{
			if(filtro.getFechaSolicitud() != null){
				query.setParameter("fechaSolicitud", filtro.getFechaSolicitud());
			}
			if (filtro.getFechaHasta() != null) {
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(filtro.getFechaHasta());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				query.setParameter("fechaHasta", gcFechaFin.getTime());
			}
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		if(filtro.getPrimerRegistroACargar() != null && filtro.getNumeroMaximoRegistrosACargar() != null){
			query.setFirstResult(filtro.getPrimerRegistroACargar());
			query.setMaxResults(filtro.getNumeroMaximoRegistrosACargar());
		}
		
		return query.getResultList();		
	}

	
	public List<SolicitudExcepcionDetalle> listarDetalleSolicitudesPorInciso(
			SolicitudExcepcionCotizacion filtro, boolean listarHistorico, BigDecimal idInciso) {
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		int index=1;
		StringBuilder queryString = new StringBuilder("select detalle.* from MIDAS.TOSOLICITUDCOTIZACION model ");
		queryString.append("inner join MIDAS.TOSOLICITUDDETALLE detalle on (detalle.ID_SOLICITUD = model.ID) ");	
		
		queryString.append("where (? is null ");
		params.put(index,filtro.getCveNegocio());
		index++;
		
		queryString.append("or model.cve_Negocio = ?) ");
		params.put(index,filtro.getCveNegocio());
		index++;

		if(filtro.getToIdCotizacion() != null){
			queryString.append(" and model.toIdCotizacion = ?");
			params.put(index,filtro.getToIdCotizacion());
			index++;
			
			if(!listarHistorico){
				queryString.append(" and model.id in (select max(model2.id) from SolicitudExcepcionCotizacion model2 where " +
					"model2.toIdCotizacion = ? ) ");
				params.put(index,filtro.getToIdCotizacion());
				index++;
			}
		}
		if(filtro.getEstatus() != null){
			queryString.append(" and model.estatus = ?");
			params.put(index,filtro.getEstatus());
			index++;
		}
		if(filtro.getUsuarioAutorizador() != null){
			queryString.append(" and model.usuario_Autorizador = ?");
			params.put(index,filtro.getUsuarioAutorizador());
			index++;
		}
		if(filtro.getUsuarioSolicitante() != null){
			queryString.append(" and model.usuario_Solicitante = ?");
			params.put(index,filtro.getUsuarioSolicitante());
			index++;
		}
		if(filtro.getId() != null){
			queryString.append(" and model.id = ?");
			params.put(index,filtro.getId());
			index++;
		}
		if(filtro.getFechaSolicitud() != null){
			queryString.append(" and model.fecha_Solicitud >= ?");
			if (filtro.getFechaHasta() != null) {
				GregorianCalendar gcFechaInicio = new GregorianCalendar();
				gcFechaInicio.setTime(filtro.getFechaSolicitud());
				gcFechaInicio.add(GregorianCalendar.DATE, 1);
				gcFechaInicio.add(GregorianCalendar.MILLISECOND, -1);
				params.put(index,gcFechaInicio.getTime());
				index++;
			} else {
				params.put(index,filtro.getFechaSolicitud());
				index++;
			}
		}
		if(filtro.getFechaHasta() != null){
			queryString.append(" and model.fecha_Solicitud <= ?");
			if (filtro.getFechaSolicitud() != null) {
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(filtro.getFechaHasta());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				params.put(index,gcFechaFin.getTime());
				index++;
			} else {
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(filtro.getFechaHasta());
				gcFechaFin.add(GregorianCalendar.DATE, 1);
				gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
				params.put(index,gcFechaFin.getTime());
				index++;
			}
		}
		if (idInciso != null) {
			queryString.append(" and detalle.id_Inciso = ?");
			params.put(index,idInciso);
			index++;
		} else {
			queryString.append(" and detalle.id_Inciso is null");
		}
		queryString.append(" order by model.id DESC ");
		Query query = entityManager.createNativeQuery(queryString.toString(), SolicitudExcepcionDetalle.class);
		
		// Se agregan los parametros
		if (!params.isEmpty()) {
			for (Integer key:params.keySet()) {
				query.setParameter(key,params.get(key));
			}
		}

		if(filtro.getPrimerRegistroACargar() != null && filtro.getNumeroMaximoRegistrosACargar() != null){
			query.setFirstResult(filtro.getPrimerRegistroACargar());
			query.setMaxResults(filtro.getNumeroMaximoRegistrosACargar());
		}
		
		return query.getResultList();		
	}

	@Override
	public List<SolicitudExcepcionDetalle> listarDetalle(Long id) {
		String queryString = "select model from SolicitudExcepcionDetalle model where " +
				"model.id in (select max(model2.id) from SolicitudExcepcionDetalle model2 where " +
					"model2.solicitudExcepcionCotizacion.id = :id " +
					"group by model2.idSeccion, model2.idInciso, model2.idCobertura, model2.idExcepcion) order by model.id";				
		Query query = entityManager.createQuery(queryString);	
		query.setParameter("id", id);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();		
	}
	

	@Override
	public List<SolicitudExcepcionDetalle> listarDetalle(
			SolicitudExcepcionDetalle filtro) {
		String queryString = "select model from SolicitudExcepcionDetalle model where " +
		"model.id in (select max(model2.id) from SolicitudExcepcionDetalle model2 where " +
			"model2.solicitudExcepcionCotizacion.id = :id " +
			"group by model2.idSeccion, model2.idInciso, model2.idCobertura)";	
		if(filtro.getEstatus() != null){
			queryString = queryString + " and model.estatus = :estatus";
		}
		queryString = queryString + " order by model.id ";
		Query query = entityManager.createQuery(queryString);	
		query.setParameter("id", filtro.getSolicitudExcepcionCotizacion().getId());
		if(filtro.getEstatus() != null){
			query.setParameter("estatus", filtro.getEstatus());
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();		
	}

	@Override
	public List<SolicitudExcepcionCotizacion> listarSolicitudesEstatusInvalido(BigDecimal idToCotizacion) {
		String queryString = "select distinct model.id from SolicitudExcepcionCotizacion model where (model.estatus = 0 " + 
			" or model.id in (select detalle.solicitudExcepcionCotizacion.id from SolicitudExcepcionDetalle detalle where detalle.estatus = 0" +
			" and detalle.id in (select max(model2.id) from SolicitudExcepcionDetalle model2 where " +
			"model2.solicitudExcepcionCotizacion.id = model.id " +
			"group by model2.idSeccion, model2.idInciso, model2.idCobertura)))";			
		if(idToCotizacion != null){
			queryString = queryString + " and model.toIdCotizacion = :idToCotizacion";
			//queryString = queryString + " and model.id = (select max(model2.id) from SolicitudExcepcionCotizacion model2 where model2.toIdCotizacion = :idToCotizacion)";
		}
		queryString = queryString + " order by model.id DESC ";
		Query query = entityManager.createQuery(queryString);	
		if(idToCotizacion != null){
			query.setParameter("idToCotizacion", idToCotizacion);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	
}
