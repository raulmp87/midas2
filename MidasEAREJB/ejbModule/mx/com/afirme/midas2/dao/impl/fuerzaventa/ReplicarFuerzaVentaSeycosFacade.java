package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Afianzadora;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
@Stateless
public class ReplicarFuerzaVentaSeycosFacade implements ReplicarFuerzaVentaSeycosFacadeRemote{
	
	private PersonaSeycosFacadeRemote personaSeycosFacade;
	private CentroOperacionSeycosFacadeRemote centroOperacionSeycosFacade;
	private GerenciaSeycosFacadeRemote gerenciaSeycosFacade;
	private EjecutivoSeycosFacadeRemote ejecutivoSeycosFacade;
	private PromotoriaSeycosFacadeRemote promotoriaSeycosFacade;
	private AgenteMidasService agenteMidasService;
	
	@Override
	public Long replicarFuerzaVenta(Object fuerzaVenta,TipoAccionFuerzaVenta tipoAccion) throws Exception{
		Long idEntidad=null;
		if(fuerzaVenta==null){
			throw new Exception("Fuerza de venta is null");
		}
		if(fuerzaVenta instanceof Afianzadora){
			Afianzadora afianzadora=(Afianzadora)fuerzaVenta;
			idEntidad=guardarAfianzadora(afianzadora,tipoAccion);
		}else if(fuerzaVenta instanceof CentroOperacion){
			CentroOperacion centroOperacion=(CentroOperacion)fuerzaVenta;
			idEntidad=guardarCentroOperacion(centroOperacion,tipoAccion);
		}else if(fuerzaVenta instanceof Gerencia){
			Gerencia gerencia=(Gerencia)fuerzaVenta;
			idEntidad=guardarGerencia(gerencia,tipoAccion);
		}else if(fuerzaVenta instanceof Promotoria){
			Promotoria promotoria=(Promotoria)fuerzaVenta;
			idEntidad=guardarPromotoria(promotoria,tipoAccion);
		}else if(fuerzaVenta instanceof Ejecutivo){
			Ejecutivo ejecutivo=(Ejecutivo)fuerzaVenta;
			idEntidad=guardarEjecutivo(ejecutivo,tipoAccion);
		}else if(fuerzaVenta instanceof Agente){
			Agente agente=(Agente)fuerzaVenta;
			idEntidad=guardarAgente(agente,tipoAccion);
		}
		return idEntidad;
	}
	/**
	 * Metodo que guarda los datos de la afianzadora para el esquema de Seycos como una persona MORAL.
	 * @param afianzadora
	 * @return
	 * @throws Exception
	 */
	private Long guardarAfianzadora(Afianzadora afianzadora,TipoAccionFuerzaVenta tipoAccion) throws Exception{
		PersonaSeycosDTO personaAfianzadora=null;
		switch(tipoAccion){
			case GUARDAR:
				personaAfianzadora=personaSeycosFacade.save(new PersonaSeycosDTO(afianzadora));
			break;
			case INACTIVAR:
				personaAfianzadora=personaSeycosFacade.unsubscribe(personaAfianzadora);
			break;
		}
		return personaAfianzadora.getIdPersona();
	}
	/**
	 * Guarda un centro de operacion en Seycos, lo replica ya sea para insertar, editar o dar de baja a un centro de operacion
	 * @param centroOperacion
	 * @param tipoAccion
	 * @return
	 * @throws Exception
	 */
	private Long guardarCentroOperacion(CentroOperacion centroOperacion,TipoAccionFuerzaVenta tipoAccion) throws Exception{
		Long id=null;
		switch(tipoAccion){
			case GUARDAR:
				id=centroOperacionSeycosFacade.save(centroOperacion);
			break;
			case INACTIVAR:
				centroOperacionSeycosFacade.unsubscribe(centroOperacion);
			break;
		}
		return id;
	}
	/**
	 * Guarda una gerencia en Seycos, lo replica ya sea para insertar, editar o dar de baja a una gerencia
	 * @param gerencia
	 * @param tipoAccion
	 * @return
	 * @throws Exception
	 */
	private Long guardarGerencia(Gerencia gerencia,TipoAccionFuerzaVenta tipoAccion) throws Exception{
		Long id=null;
		switch(tipoAccion){
			case GUARDAR:
				id=gerenciaSeycosFacade.save(gerencia);
			break;
			case INACTIVAR:
				gerenciaSeycosFacade.unsubscribe(gerencia);
			break;
		}
		return id;
	}
	/**
	 * Guarda una promotoria en Seycos, lo replica ya sea para insertar, editar o dar de baja a una promotoria
	 * @param promotoria
	 * @param tipoAccion
	 * @return
	 * @throws Exception
	 */
	private Long guardarPromotoria(Promotoria promotoria,TipoAccionFuerzaVenta tipoAccion) throws Exception{
		Long id=null;
		switch(tipoAccion){
			case GUARDAR:
				id=promotoriaSeycosFacade.save(promotoria);
			break;
			case INACTIVAR:
				promotoriaSeycosFacade.unsubscribe(promotoria);
			break;
		}
		return id;
	}
	
	private Long guardarEjecutivo(Ejecutivo ejecutivo,TipoAccionFuerzaVenta tipoAccion) throws Exception{
		Long id=null;
		switch(tipoAccion){
			case GUARDAR:
				id=ejecutivoSeycosFacade.save(ejecutivo);
			break;
			case INACTIVAR:
				ejecutivoSeycosFacade.unsubscribe(ejecutivo);
			break;
		}
		return id;
	}
	
	private Long guardarAgente(Agente agente,TipoAccionFuerzaVenta tipoAccion)throws Exception{
		Long id=null;
		switch(tipoAccion){
			case GUARDAR:
				agenteMidasService.saveInSeycos(agente);
			break;
			case INACTIVAR:
				agenteMidasService.saveInSeycos(agente);
			break;
		}
		return id;
	}
	/***Sets and Gets************************************************/
	@EJB
	public void setPersonaSeycosFacade(PersonaSeycosFacadeRemote personaSeycosFacade) {
		this.personaSeycosFacade = personaSeycosFacade;
	}
	@EJB
	public void setCentroOperacionSeycosFacade(CentroOperacionSeycosFacadeRemote centroOperacionSeycosFacade) {
		this.centroOperacionSeycosFacade = centroOperacionSeycosFacade;
	}
	@EJB
	public void setGerenciaSeycosFacade(GerenciaSeycosFacadeRemote gerenciaSeycosFacade) {
		this.gerenciaSeycosFacade = gerenciaSeycosFacade;
	}
	@EJB
	public void setEjecutivoSeycosFacade(EjecutivoSeycosFacadeRemote ejecutivoSeycosFacade) {
		this.ejecutivoSeycosFacade = ejecutivoSeycosFacade;
	}
	@EJB
	public void setPromotoriaSeycosFacade(PromotoriaSeycosFacadeRemote promotoriaSeycosFacade) {
		this.promotoriaSeycosFacade = promotoriaSeycosFacade;
	}
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
}
