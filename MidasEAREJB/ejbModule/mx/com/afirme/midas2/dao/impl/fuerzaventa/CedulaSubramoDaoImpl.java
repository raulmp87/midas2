package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.RamoFacadeRemote;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.CedulaSubramoDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CedulaSubramo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
@Stateless
public class CedulaSubramoDaoImpl extends EntidadDaoImpl implements CedulaSubramoDao{
	

	
	private RamoFacadeRemote ramoFacadeRemote;
	
	private SubRamoFacadeRemote subramoFacadeRemote;
	
	@EJB
	public void setRamoFacadeRemote(RamoFacadeRemote ramoFacadeRemote) {
		this.ramoFacadeRemote = ramoFacadeRemote;
	}
	@EJB
	public void setSubramoFacadeRemote(SubRamoFacadeRemote subramoFacadeRemote) {
		this.subramoFacadeRemote = subramoFacadeRemote;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Ap�ndice de m�todo generado autom�ticamente
		return null;
	}	

	@Override
	public List<CedulaSubramo> findByFilters(CedulaSubramo filtroCedulaSubramo) {
		List<CedulaSubramo> lista = new ArrayList<CedulaSubramo>();
		final StringBuilder queryString = new StringBuilder("");
		queryString.append("select model from CedulaSubramo model left join fetch model.tipoCedula left join fetch model.subramoDTO ");
		Map<String, Object> params = new HashMap<String, Object>();
		if(filtroCedulaSubramo != null){
			if(filtroCedulaSubramo.getId()!=null){
				addCondition(queryString, "model.id=:id");
				params.put("id", filtroCedulaSubramo.getId());
			}
			if(filtroCedulaSubramo.getTipoCedula()!=null && filtroCedulaSubramo.getTipoCedula().getId()!=null){
				addCondition(queryString, "model.tipoCedula.id=:idCedula");
				params.put("idCedula", filtroCedulaSubramo.getTipoCedula().getId());
			}
			if(filtroCedulaSubramo.getSubramoDTO()!=null && filtroCedulaSubramo.getSubramoDTO().getId()!=null){
				addCondition(queryString, "model.subramoDTO.id = :idSubramo");
				params.put("idSubramo", filtroCedulaSubramo.getSubramoDTO().getId());
			}
		
		
			Query query = entityManager.createQuery(getQueryString(queryString));
			if(!params.isEmpty()){
				setQueryParametersByProperties(query, params);
			}
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			lista=query.getResultList();
			
		}
		return lista;
	}

	@Override
	public CedulaSubramo loadById(CedulaSubramo entidad) {
		if(entidad!=null && entidad.getId()!=null){
			List<CedulaSubramo> list=findByFilters(entidad);
			if(list!=null && !list.isEmpty()){
				entidad=list.get(0);
			}
		}
		return entidad;
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}

	@Override
	public CedulaSubramo saveFull(CedulaSubramo cedulaSubramo) throws Exception {
		List<CedulaSubramo> lista = new ArrayList<CedulaSubramo>();
		cedulaSubramo.getId();
		
		return null;
	}
	
	@Override
	public List<RamoDTO> obtenerRamos() throws Exception {
		List<RamoDTO> listaRamos = new ArrayList<RamoDTO>();
		listaRamos = ramoFacadeRemote.findAll();
		return listaRamos;
		
	}
	
	@Override
	public List<SubRamoDTO> obtenerSubRamosPorRamo(RamoDTO ramo) throws Exception {
		if(ramo==null ||ramo.getIdTcRamo()==null){
			throw new Exception("Favor de proporcionar el ramo");
		}
		List<SubRamoDTO> listaSubramo = new ArrayList<SubRamoDTO>();
		listaSubramo = subramoFacadeRemote.findByProperty("ramoDTO.idTcRamo", ramo.getIdTcRamo());
		return listaSubramo;
	}
	
	@Override
	public List<CedulaSubramo> obtenerSubRamosAsociadosPorCedula(ValorCatalogoAgentes tipoCedula) throws Exception {
		CedulaSubramo filtro = new CedulaSubramo();
		List<CedulaSubramo> listaSubRamos = new ArrayList<CedulaSubramo>();
		if(tipoCedula != null && tipoCedula.getId()!= null){			 
			filtro.setTipoCedula(tipoCedula);
			listaSubRamos = findByFilters(filtro);			
		}
		return listaSubRamos;
	}
	
	@Override
	public void guardarAsociacion(List<SubRamoDTO> listaSubRamos,ValorCatalogoAgentes tipoCedula) throws Exception {
		if(listaSubRamos==null || listaSubRamos.isEmpty()){
			onError("Favor de elegir subramos a asociar");
		}
		if(tipoCedula==null || tipoCedula.getId()==null){
			onError("Favor de elegir la cedula a la que se asociaran los subramos");
		}
		
		//se eliminar los registros actuales de la cedula
		if(tipoCedula!=null){
			CedulaSubramo filtro = new CedulaSubramo();
			filtro.setTipoCedula(tipoCedula);
			List<CedulaSubramo> lista = findByFilters(filtro); 
			for(CedulaSubramo cedula:lista){
				remove(cedula);
			}
			
		}
				
		//se inserta la lista de subramos para la cedula
		for(SubRamoDTO subRamo:listaSubRamos){			
			CedulaSubramo params = new CedulaSubramo();
			params.setSubramoDTO(subRamo);
			params.setTipoCedula(tipoCedula);
			if(params.getId()==null){			
				persist(params);
			}
		}
	}
	
	private void onError(String msg)throws Exception{
		throw new Exception(msg);
	}
	
}
