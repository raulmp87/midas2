package mx.com.afirme.midas2.dao.impl.siniestros;
import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;



import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteRoboSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento.TipoSalvamento;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.dto.siniestros.DefinicionSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.ocra.EnvioOcraService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestro.RoboService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.provision.SiniestrosProvisionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.EnumUtil;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
/**
 * <font color="#808080">Esta es la definici�n de los m�todos necearios para la
 * b�squeda de sinietros catalogados como robo total.</font>
 * @author usuario
 * @version 1.0
 * @created 28-jul-2014 12:50:44 p.m.
 */
@Stateless 
public class RoboServiceImpl implements RoboService {
	
	private static final int	CIEN	= 100;
	
	private static final String MENSAJE_ERROR_OCRA ="ERROR";
	
	private static final String CODIGO_TIPO_DEDUCIBLE_PORCENTAJE = "1";

	private static final Logger log = Logger.getLogger(RoboServiceImpl.class);

	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ReporteCabinaService reporteCabinaService;
	
	@EJB
	private EnvioOcraService envioOCRAService;
		
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private CatalogoGrupoValorService  catalogoGrupoValorService;
		
	@EJB
	private PolizaSiniestroService polizaSiniestroService;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
    private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	@EJB
	private RecuperacionDeducibleService recuperacionDeducibleService;
	
	@EJB
	private PerdidaTotalService perdidaTotalService;
	
	@EJB
	private SiniestrosProvisionService  siniestrosProvisionService;
	
	@Resource
	private javax.ejb.SessionContext sessionContext;

	/**
	 * Busqueda de siniestros catalogados como robo utilizando el filtro capturado en
	 * pantalla
	 * 
	 * @param Filtro
	 */
	public List<ReporteSiniestroRoboDTO> busquedaRobo(ReporteSiniestroRoboDTO reporteSiniestroDTO){
		return reporteCabinaService.buscarReportesRobo(reporteSiniestroDTO);
	}
	/**
	 * Este m�todo consulta la informacion del DTO
	 * @param reporteCabinaId
	 * @param reporteCabinaId
	 */
	@Override
	public DefinicionSiniestroRoboDTO consultarDefinicionSiniestroRobo(
			Long reporteCabinaId,Long coberturaCabinaId) { 
		DefinicionSiniestroRoboDTO dto = new DefinicionSiniestroRoboDTO();		
		ReporteCabina reporte = reporteCabinaService.buscarReporte(reporteCabinaId);	
		//Datos Principales		 
		dto.setNoPoliza(reporte.getPoliza().getNumeroPolizaFormateada());
		dto.setNoReporte(reporte.getNumeroReporte());
		AutoIncisoReporteCabina auto=reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
		dto.setNumSerie(auto.getNumeroSerie());
		dto.setNumSerieOcra(auto.getNumeroSerieOcra());
		if(null!=reporte.getSeccionReporteCabina().getIncisoReporteCabina() && null!=reporte.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso()){
			dto.setNumeroInciso(reporte.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso());
		}
		
		//Datoa Vehiculo  		
		if(!StringUtil.isEmpty(auto.getColor()) ){		
			CatValorFijo catColor =catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.COLOR, auto.getColor());
			dto.setColor(catColor.getDescripcion()); 
		}
		if (null!=auto.getMarcaId()){
			MarcaVehiculoDTO marcaVehiculoDTO=entidadService.findById(MarcaVehiculoDTO.class, auto.getMarcaId());
			if(null!=marcaVehiculoDTO){
				
				if(null!=marcaVehiculoDTO.getDescripcionMarcaVehiculo()){
					dto.setMarca(marcaVehiculoDTO.getDescripcionMarcaVehiculo());
				}
				if(null!=marcaVehiculoDTO.getTipoBienAutosDTO() && !StringUtil.isEmpty(marcaVehiculoDTO.getTipoBienAutosDTO().getDescripcionTipoBien()) ){
					dto.setTipoVehiculo( marcaVehiculoDTO.getTipoBienAutosDTO().getDescripcionTipoBien() );
				}
			}
		}
		if(null!=auto.getPlaca()){
			dto.setPlacas(auto.getPlaca());			
		}
		if(null!= auto.getNumeroMotor()){
			dto.setNumMotor(auto.getNumeroMotor());
		}
		
		if(null!=auto.getModeloVehiculo()){
			dto.setModelo(auto.getModeloVehiculo().toString());
		}
		//Datos Asegurado
		String asegurado =reporte.getSeccionReporteCabina().getIncisoReporteCabina().getNombreAsegurado();
		String ladaA = reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getLadaTelefono();
		String telA = reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTelefono();
		dto.setLadaAsegurado(ladaA);
		dto.setTelASegurado(telA);
		dto.setNombreAsegurado(asegurado);		
		//Datos contacto
		
		
		 String contacto =parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS  , ParametroGlobalService.PARAMETRO_SEGUIMIENTOROBO_CONTACTO);
			String ladaC = parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS , ParametroGlobalService.PARAMETRO_SEGUIMIENTOROBO_LADA);
			String telC = parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS , ParametroGlobalService.PARAMETRO_SEGUIMIENTOROBO_TEL);
			String mail = parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS , ParametroGlobalService.PARAMETRO_SEGUIMIENTOROBO_MAIL);
			String ext = parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS , ParametroGlobalService.PARAMETRO_SEGUIMIENTOROBO_EXT);
			dto.setMailContacto(mail);
			dto.setExtContacto(ext);
			dto.setNombreContacto(contacto);
			dto.setLadaContacto(ladaC);
			dto.setTelContacto(telC);
		
		
		
		//Localizacion 
		if( null != reporte.getLugarOcurrido() ){
			
			String pais      = ( reporte.getLugarOcurrido().getPais()   != null ) ? reporte.getLugarOcurrido().getPais().getDescripcion()   : "" ;
			String estado    = ( reporte.getLugarOcurrido().getEstado() != null ) ? reporte.getLugarOcurrido().getEstado().getDescripcion() : "" ;
			String municipio = ( reporte.getLugarOcurrido().getCiudad() != null ) ? reporte.getLugarOcurrido().getCiudad().getDescripcion() : "" ;
			String colonia   = ( reporte.getLugarOcurrido().getColonia() != null ) ? reporte.getLugarOcurrido().getColonia().getDescripcion() : "";
			String refrencia = ( reporte.getLugarOcurrido().getReferencia() != null ) ? reporte.getLugarOcurrido().getReferencia() : "";
			String cp        = ( reporte.getLugarOcurrido().getCodigoPostal() != null ) ? reporte.getLugarOcurrido().getCodigoPostal() : "";
			
			dto.setPais(pais);
			dto.setEstado(estado);
			dto.setMunicipio(municipio);
			dto.setColonia(colonia);
			dto.setReferencia(refrencia);
			dto.setCp(cp);
			dto.setCalle(reporte.getLugarOcurrido().getCalleNumero());
			dto.setTipoCarretera(reporte.getLugarOcurrido().getTipoCarretera());
			dto.setNombreCarretera(reporte.getLugarOcurrido().getNombreCarretera());
			dto.setKilometro(reporte.getLugarOcurrido().getKilometro());
									
		}
		//Ajustador /Agente
		if (null!=reporte.getAjustador() && null!=reporte.getAjustador().getPersonaMidas() && null!=reporte.getAjustador().getPersonaMidas().getNombre()){
			String ajustador =reporte.getAjustador().getPersonaMidas().getNombre();
			dto.setAjustador(ajustador);
		}
		//PARA OBTENER EL AGENTE 
		
		dto.setAgente(reporte.getNombreAgente());
		
		//Obtener Fecha vigencia poliza 
		//PARA OBTENER LA FECHA DE VIGENCIA DE LA POLIZA
		
		IncisoReporteCabina  incisoReporte = polizaSiniestroService.obtenerIncisoAsignadoAlReporte(reporteCabinaId);
		
		dto.setVigenciaPolizaIni(incisoReporte.getValidFrom());
		dto.setVigenciaPolizaFin(incisoReporte.getValidTo());		
     
	    dto.setFechaReporte(reporte.getFechaHoraReporte());
	    //datos condicion especiales. 
	    dto.setFechaOcurrido(reporte.getFechaHoraOcurrido() );
	    dto.setFechaOcurridoMillis(reporte.getFechaHoraOcurrido() .getTime());
	    dto.setIncisoContinuityId(reporteCabinaService.obtenerIncisoReporteCabina(reporte.getId()));
	    dto.setIdToPoliza(reporte.getPoliza().getIdToPoliza());
	    //Obtener reserva
	    //obtener cobertura 
	    CoberturaReporteCabina coberturaReporte = entidadService.findById(CoberturaReporteCabina.class, coberturaCabinaId);
	    if (null !=coberturaReporte ){
	    	dto.setSumaAsegurada(new BigDecimal(coberturaReporte.getValorSumaAsegurada()));
	    	if(coberturaReporte.getlEstimacionCoberturaReporteCabina() != null && 
	    			coberturaReporte.getlEstimacionCoberturaReporteCabina().size() > 0 && 
	    			coberturaReporte.getlEstimacionCoberturaReporteCabina().get(0).getAplicaDeducible() != null &&
	    			coberturaReporte.getlEstimacionCoberturaReporteCabina().get(0).getAplicaDeducible().booleanValue()){
	    		dto.setAplicaDeducible(Boolean.TRUE);
	    		BigDecimal deducible = recuperacionDeducibleService.obtenerDeduciblePorCobertura(coberturaReporte.getId(), null);
		    	dto.setMontoDeducible(deducible);
		    	if(coberturaReporte.getCoberturaDTO().getClaveTipoDeducible() != null && coberturaReporte.getCoberturaDTO().getClaveTipoDeducible().equals(CODIGO_TIPO_DEDUCIBLE_PORCENTAJE)){
		    		dto.setPorcentajeDeducible( new BigDecimal (coberturaReporte.getPorcentajeDeducible()));
		    	}
	    	}
	    	if(null!=coberturaReporte.getCoberturaDTO()){
	    		dto.setCoberturaDes(coberturaReporte.getCoberturaDTO().getDescripcion());
	    	}
	    	List<ReporteRoboSiniestro> listRobos =entidadService.findByProperty(ReporteRoboSiniestro.class, "coberturaReporteCabinaId", coberturaReporte.getId());
	    	if(!listRobos.isEmpty()){
	    		ReporteRoboSiniestro robo=listRobos.get(0);
	    		dto.setReporteRoboId(robo.getId());
	    		dto.setTipoRobo(robo.getTipoRobo());
	    		dto.setEstatusVehiculo(robo.getEstatusVehiculo());
	    		dto.setNumActa(robo.getNumeroActa());
	    		dto.setNombreAgenteMinisterioPublico(robo.getNombreAgenteMinisterioPublico());
	    		dto.setAveriguacionDen(robo.getNumeroAveriguacionDen());
	    		dto.setAveriguacionLocal(robo.getNumeroAveriguacionLocal());
	    		dto.setLocalizador(robo.getLocalizador());
	    		dto.setUbicacionVehiculo(robo.getUbicacionVehiculo());
	    		dto.setRecuperador(robo.getRecuperador());
	    		dto.setCausaMovimiento(robo.getCausaMovimiento());
	    		//fechas
	    		dto.setFechaRobo(robo.getFechaRobo());
	    		dto.setFechaCapOCRA(robo.getFechaCapturaOcra());
	    		dto.setFechaAveriguacion(robo.getFechaAveriguacion());
	    		dto.setFechaLocalizacion(robo.getFechaLocalizacion());
	    		dto.setFechaRecuperacion(robo.getFechaRecuperacion());
	    		dto.setObservaciones(robo.getObservaciones());
	    		dto.setPresupuestoDanos(robo.getPresupuestoDanos());
	    		dto.setFaltantes(robo.getFaltantes());
	    		dto.setMontoProvicion(robo.getMontoProvicion());
	    		dto.setImportePagado(robo.getImportePagado() );
	    		dto.setValorComercialMomento(robo.getValorComercialMomento());
	    		dto.setProveedor(robo.getProveedor());
	    		if (StringUtils.isNotBlank(robo.getProveedor())){
	    			dto.setIsTurnarInvetigacion(true);
	    		}else{
	    			dto.setIsTurnarInvetigacion(false);
	    		}
	    	}
	    }
	    //Obtener Siniestro
    	if(!entidadService.findByProperty(SiniestroCabina.class, "reporteCabina.id", reporte.getId()).isEmpty() ){
    		SiniestroCabina   siniestroCabina =entidadService.findByProperty(SiniestroCabina.class, "reporteCabina.id", reporte.getId()).get(0);
    		if ( !StringUtil.isEmpty( siniestroCabina.getNumeroSiniestro() )){
    			 dto.setNoSiniestro(siniestroCabina.getNumeroSiniestro() );
    		}
	    }
	    //obtener estimacion 
	    List<EstimacionCoberturaReporteCabina>   lisEstimacion = entidadService.findByProperty(EstimacionCoberturaReporteCabina.class, "coberturaReporteCabina.id", coberturaReporte.getId());
	    if(!lisEstimacion.isEmpty()){
	    	EstimacionCoberturaReporteCabina   estimacionReporte =lisEstimacion.get(0);
	    	BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(estimacionReporte.getId(), Boolean.TRUE);
	    	
	    	dto.setReserva(reserva);
		    dto.setValorComercial(estimacionReporte.getSumaAseguradaObtenida());
			dto.setEstimacionId(estimacionReporte.getId());
	    }
//	    try {
//	    dto.setMontoDeducible( (dto.getValorComercial().multiply(dto.getPorcentajeDeducible()) ).divide(new BigDecimal(CIEN) )  );
//	    }catch (Exception e){
//	    	dto.setMontoDeducible(null);
//	    }
	    return dto;
	}

	/**
	 * Este m�todo guarda los cambios hechos en la pantalla de seguimiento de robo
	 * 
	 * @param siniestroRoboDTO
	 * @param envioOcra
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String,MovimientoProvisionSiniestros> guardarRoboTotal(DefinicionSiniestroRoboDTO siniestroRoboDTO,boolean envioOcra)throws Exception {
		//Envio a ocra 
	    CoberturaReporteCabina coberturaReporte = entidadService.findById(CoberturaReporteCabina.class, siniestroRoboDTO.getCoberturaId());
	    List<EstimacionCoberturaReporteCabina>   lisEstimacion = entidadService.findByProperty(EstimacionCoberturaReporteCabina.class, "coberturaReporteCabina.id", coberturaReporte.getId());
	    EstimacionCoberturaReporteCabina   estimacionReporte= null;
	    if(!lisEstimacion.isEmpty()){
	    	estimacionReporte =lisEstimacion.get(0);
	    }
		ReporteRoboSiniestro reporteRobo = new ReporteRoboSiniestro();
		ReporteRoboSiniestro informacionPrevia = null;
		if(null!=siniestroRoboDTO.getReporteRoboId()){
			reporteRobo.setId(siniestroRoboDTO.getReporteRoboId());
			informacionPrevia = entidadService.findById(ReporteRoboSiniestro.class, siniestroRoboDTO.getReporteRoboId());
		}
		String estatusVehiculoPrevio = (informacionPrevia != null)? informacionPrevia.getEstatusVehiculo() : null;
		BigDecimal montoProvicionPrevio = (informacionPrevia != null)? informacionPrevia.getMontoProvicion() : null;
		String ubicacionVehiculoPrevio = (informacionPrevia != null)? informacionPrevia.getUbicacionVehiculo() : null;
		reporteRobo.setCoberturaReporteCabinaId(coberturaReporte.getId());
		reporteRobo.setCausaMovimiento(siniestroRoboDTO.getCausaMovimiento());
		reporteRobo.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		reporteRobo.setFechaCreacion(new Date()); 
		reporteRobo.setFechaModificacion(new Date());
		reporteRobo.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		reporteRobo.setEstatusVehiculo(siniestroRoboDTO.getEstatusVehiculo());
		reporteRobo.setFaltantes  (siniestroRoboDTO.getFaltantes());
		reporteRobo.setFechaAveriguacion(siniestroRoboDTO.getFechaAveriguacion());
		reporteRobo.setFechaRecuperacion(siniestroRoboDTO.getFechaRecuperacion());
		reporteRobo.setFechaCapturaOcra(siniestroRoboDTO.getFechaCapOCRA());
		reporteRobo.setFechaLocalizacion(siniestroRoboDTO.getFechaLocalizacion());
		reporteRobo.setFechaRobo(siniestroRoboDTO.getFechaRobo());
		reporteRobo.setImportePagado(siniestroRoboDTO.getImportePagado());
		reporteRobo.setLocalizador(siniestroRoboDTO.getLocalizador());
		reporteRobo.setMontoProvicion(siniestroRoboDTO.getMontoProvicion());
		reporteRobo.setNumeroActa(siniestroRoboDTO.getNumActa());
		reporteRobo.setNumeroAveriguacionLocal(siniestroRoboDTO.getAveriguacionLocal());
		reporteRobo.setNumeroAveriguacionDen(siniestroRoboDTO.getAveriguacionDen());
		reporteRobo.setPresupuestoDanos(siniestroRoboDTO.getPresupuestoDanos());
		reporteRobo.setRecuperador(siniestroRoboDTO.getRecuperador());
		reporteRobo.setTipoRobo(siniestroRoboDTO.getTipoRobo());
		reporteRobo.setUbicacionVehiculo(siniestroRoboDTO.getUbicacionVehiculo());
		reporteRobo.setValorComercialMomento(siniestroRoboDTO.getValorComercialMomento());
		reporteRobo.setObservaciones(siniestroRoboDTO.getObservaciones());
		reporteRobo.setNombreAgenteMinisterioPublico(siniestroRoboDTO.getNombreAgenteMinisterioPublico());
		//Montos Recuperacion 
		reporteRobo.setPresupuestoDanos(siniestroRoboDTO.getPresupuestoDanos());
		reporteRobo.setFaltantes(siniestroRoboDTO.getFaltantes());
		reporteRobo.setMontoProvicion(siniestroRoboDTO.getMontoProvicion());
		reporteRobo.setImportePagado(siniestroRoboDTO.getImportePagado() );	
		reporteRobo.setProveedor(siniestroRoboDTO.getProveedor());
		
		if(estimacionReporte!=null){
			BigDecimal reservaAnterior = movimientoSiniestroService.obtenerReservaAfectacion(estimacionReporte.getId(), Boolean.TRUE);
			
			if((reservaAnterior.compareTo(siniestroRoboDTO.getReserva())!=0) &&   StringUtil.isEmpty(siniestroRoboDTO.getCausaMovimiento())){
				throw new Exception("Debe proporcionar causa de movimiento" );
			 } 
			movimientoSiniestroService.generarAjusteReserva(estimacionReporte.getId(), siniestroRoboDTO.getReserva(), EnumUtil.fromValue(CausaMovimiento.class, siniestroRoboDTO.getCausaMovimiento()), null);
		
		}
		entidadService.save(reporteRobo);
		if(estimacionReporte!=null){			
			this.generarReactivarInactivarRecuperacionSalvamentoEnRobo(reporteRobo, estatusVehiculoPrevio, estimacionReporte);
			this.actualizarRecuperacionSalvamentoRobo(reporteRobo.getMontoProvicion(), montoProvicionPrevio, estimacionReporte, 
					reporteRobo.getUbicacionVehiculo(), ubicacionVehiculoPrevio);
			IndemnizacionSiniestro indemnizacion = perdidaTotalService.obtenerIndemnizacionActivaPorEstimacion(estimacionReporte.getId());
			if(indemnizacion != null){
				recuperacionSalvamentoService.actualizarInformacionRecuperacionSalvamentoEnIndemnizacion(indemnizacion.getId());
			}
		}
		siniestroRoboDTO.setReporteRoboId(reporteRobo.getId());   		
		AutoIncisoReporteCabina autoInciso= coberturaReporte.getIncisoReporteCabina().getAutoIncisoReporteCabina();
		autoInciso.setNumeroSerieOcra(siniestroRoboDTO.getNumSerieOcra());
		entidadService.save(autoInciso);
		return validarMovimientoProvision(siniestroRoboDTO.getEstimacionId(), reporteRobo);
		//llamado al validar
//		TODO agregar la generacion del movimiento contable del monto provision de salvamento (funcionalidad pendiente por definir)
		}
	
	public Map<String,MovimientoProvisionSiniestros> validarMovimientoProvision(Long estimacionId, ReporteRoboSiniestro reporteRoboSiniestro){
		Map<String,MovimientoProvisionSiniestros> movimientos= new HashMap<String, MovimientoProvisionSiniestros>();
		//llamar service que trae el vaor del catalogo de RECUPERADO POR AFIRME
		
		MovimientoProvisionSiniestros nvoMovimiento;
		
		System.out.println("#### OBTENIENDO SALVAMENTO, DE LA ESTIMACION: "+estimacionId);
		RecuperacionSalvamento recuperacionSalvamento=recuperacionSalvamentoService.obtenerRecuperacionSalvamento(estimacionId);
//		System.out.println("#### SALVAMENTO OBTENIDO, ID: "+((recuperacionSalvamento!=null)?recuperacionSalvamento.getId():"NO HAY SALVAMENTO")+", NUMERO: "+((recuperacionSalvamento!=null)?recuperacionSalvamento.getNumero():"0")+", VALOR:"+((recuperacionSalvamento!=null)?recuperacionSalvamento.getValue():"0"));
		if( recuperacionSalvamento != null){
			MovimientoProvisionSiniestros provisionActual=this.siniestrosProvisionService.obtenerMovimientoProvisionado(recuperacionSalvamento.getId(), MovimientoProvisionSiniestros.Origen.RECUPERACION_SALVAMENTO);
//			System.out.println("#### PROVISION ACTUAL OBTENIDA, ID: "+((provisionActual!=null)?provisionActual.getId():"NO HAY PROVISION ACTUALMENTE")+", CAUSA: "+((provisionActual!=null)?provisionActual.getCausa():"0")+", VALOR:"+((provisionActual!=null)?provisionActual.getValue():"0"));

			boolean recuperadoPorAfirme=((reporteRoboSiniestro.getEstatusVehiculo()!=null)?reporteRoboSiniestro.getEstatusVehiculo():"").equalsIgnoreCase(ReporteRoboSiniestro.EstatusVehiculo.RECUPERADO_PARA_AFIRME.toString());
			if(recuperadoPorAfirme){
//				System.out.println("#### EL ESTADO DEL VEHICULO ES RECUPERADO POR AFIRME: "+reporteRoboSiniestro.getEstatusVehiculo());
				
				if(provisionActual!=null)
				{
//					System.out.println("#### EXISTE UNA PROVISIÓN "+provisionActual.getId() +"PARA ESE SINIESTRO ID: "+ reporteRoboSiniestro.getId()+",NUMACTA: "+reporteRoboSiniestro.getNumeroActa()+ ", VALOR:"+reporteRoboSiniestro.getValue());
					if(provisionActual.getMontoMovimiento().compareTo(reporteRoboSiniestro.getMontoProvicion())!=0)
					{
//						System.out.println("#### SE CAMBIÓ EL MONTO DE LA PROVISION POR LO TANTO SE CANCELA EL ANTERIOR Y SE GENERA NUEVO, MONTO ANTERIOR: "+provisionActual.getMontoMovimiento()+", MONTO NUEVO: "+reporteRoboSiniestro.getMontoProvicion());
						movimientos.put("CANCELAR", provisionActual);
//						System.out.println("#### ID DE PROVISION A CANCELAR (ANTERIOR): "+provisionActual.getId());

						nvoMovimiento=this.siniestrosProvisionService.generaMovimientoDeRoboTotal(recuperacionSalvamento.getId(), reporteRoboSiniestro.getMontoProvicion());
//						System.out.println("#### SE CREA UNA NUEVA PROVISIÓN PARA ESTE SINIESTRO ID: "+ reporteRoboSiniestro.getId()+", PROVISION ID: "+nvoMovimiento.getId());
						this.entidadService.save(nvoMovimiento);
						movimientos.put("PROVISIONAR", nvoMovimiento);

						
					}
				}
				else if (provisionActual==null){
//					System.out.println("#### NO EXISTE UNA PROVISIÓN PARA ESE SINIESTRO ID: "+ reporteRoboSiniestro.getId()+", NUMACTA: "+reporteRoboSiniestro.getNumeroActa()+ ", VALOR:"+reporteRoboSiniestro.getValue());
					nvoMovimiento=this.siniestrosProvisionService.generaMovimientoDeRoboTotal(recuperacionSalvamento.getId(), reporteRoboSiniestro.getMontoProvicion());
//					System.out.println("#### SE CREA UNA NUEVA PROVISIÓN PARA ESTE SINIESTRO ID: "+ reporteRoboSiniestro.getId()+", PROVISION ID: "+nvoMovimiento.getId());
					this.entidadService.save(nvoMovimiento);
					movimientos.put("PROVISIONAR", nvoMovimiento);
				}
			}
			else if(!recuperadoPorAfirme){
//				System.out.println("#### EL ESTADO DEL VEHICULO NO ES RECUPERADO POR AFIRME, ES :"+reporteRoboSiniestro.getEstatusVehiculo());
				if(provisionActual!=null){
//					System.out.println("#### EXISTE UNA PROVISIÓN PARA ESE SINIESTRO / VEHICULO: "+ provisionActual.getId()+" PERO SE CANCELA PORQUE SE LE CAMBIO EL ESTADO AL VEHICULO");
					movimientos.put("CANCELAR", provisionActual);
//					System.out.println("#### ID DE PROVISION A CANCELAR (ANTERIOR): "+provisionActual.getId());

				}
			}

		}
				return movimientos;
	}
	
	//private Boolean validarSiTieneRecuperacionSalvamento ()
	
	public void guardarYProvisionar(DefinicionSiniestroRoboDTO siniestroRoboDTO,boolean envioOcra)throws Exception {
		RoboService processorRobo = this.sessionContext.getBusinessObject(RoboService.class);
		Map<String,MovimientoProvisionSiniestros> movimientos=processorRobo.guardarRoboTotal(siniestroRoboDTO,envioOcra);
		if(!movimientos.isEmpty()){
        	if(movimientos.get("CANCELAR")!=null){
        		System.out.println("#### SE VA A CANCELAR PROVISION EN MIZAR");
        		this.siniestrosProvisionService.cancelaProvision(movimientos.get("CANCELAR").getIdRefrencia());
        	}
        	if(movimientos.get("PROVISIONAR")!=null){
        		System.out.println("#### SE VA A GENERAR NUEVA PROVISION EN MIZAR");

        		this.siniestrosProvisionService.creaProvision(movimientos.get("PROVISIONAR").getId());
        	}
		}
	}
	

	/**
	 * Genera, Reactiva o Inactiva la recuperacion de salvamento del robo dependiendo de los cambios del estatus del vehiculo en el reporte de robo
	 * @param robo
	 * @param estatusVehiculoPrevio
	 * @param estimacion
	 */
	private void generarReactivarInactivarRecuperacionSalvamentoEnRobo(ReporteRoboSiniestro robo, 
			String estatusVehiculoPrevio, EstimacionCoberturaReporteCabina estimacion){
		RecuperacionSalvamento recuperacion = 
			recuperacionSalvamentoService.obtenerRecuperacionSalvamento(estimacion.getId());
		
		if(robo.getEstatusVehiculo() != null){
			if((estatusVehiculoPrevio == null 
					|| estatusVehiculoPrevio.compareTo(ReporteRoboSiniestro.EstatusVehiculo.RECUPERADO_PARA_AFIRME.toString()) != 0)
					&& robo.getEstatusVehiculo().compareTo(ReporteRoboSiniestro.EstatusVehiculo.RECUPERADO_PARA_AFIRME.toString()) == 0){
				if(recuperacion == null){
					this.generarRecuperacionSalvamentoRobo(robo,estimacion);
				}else{
					recuperacionSalvamentoService.reactivarRecuperacionSalvamento(recuperacion.getId(), null);
				}
			}else if(estatusVehiculoPrevio != null
					&& estatusVehiculoPrevio.compareTo(ReporteRoboSiniestro.EstatusVehiculo.RECUPERADO_PARA_AFIRME.toString()) == 0
					&& robo.getEstatusVehiculo().compareTo(ReporteRoboSiniestro.EstatusVehiculo.RECUPERADO_PARA_AFIRME.toString()) != 0
					&& recuperacion != null){
				recuperacionSalvamentoService.inactivarRecuperacionSalvamento(recuperacion.getId(), "Inactivo Por Estatus de Vehiculo en RT");
			}
		}
	}
	
	/**
	 * Genera la recuperacion de salvamento para la indemnizacion. 
	 * @param indemnizacionCapturada
	 */
	private void generarRecuperacionSalvamentoRobo(ReporteRoboSiniestro robo, EstimacionCoberturaReporteCabina estimacion){
		Boolean abandonoIncosteable = 
			(robo.getMontoProvicion() != null 
					&& robo.getMontoProvicion().doubleValue() > 0)? 
							Boolean.FALSE : 
								Boolean.TRUE;
		if(abandonoIncosteable){
			recuperacionSalvamentoService.guardarRecuperacionSalvamento(
					estimacion.getId(), (robo.getMontoProvicion() == null)? new BigDecimal(0.00) : robo.getMontoProvicion(), 
					robo.getUbicacionVehiculo(), TipoSalvamento.RT, abandonoIncosteable, "", 
					estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina()
					.getReporteCabina().getLugarOcurrido().getCiudad().getId(),
					"No se ha capturado el monto provicion en el reporte de robo");
		}else{
			recuperacionSalvamentoService.guardarRecuperacionSalvamento(
					estimacion.getId(), (robo.getMontoProvicion() == null)? new BigDecimal(0.00) : robo.getMontoProvicion(), 
					robo.getUbicacionVehiculo(), TipoSalvamento.RT, abandonoIncosteable, "", 
					estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina()
					.getReporteCabina().getLugarOcurrido().getCiudad().getId());
		}
	}
	
	/**
	 * Actualiza la Recuperacion de Salvamento en caso de que haya cambiado
	 * @param montoNuevo
	 * @param montoPrevio
	 * @param estimacion
	 */
	private void actualizarRecuperacionSalvamentoRobo(BigDecimal montoNuevo, BigDecimal montoPrevio, 
			EstimacionCoberturaReporteCabina estimacion, String ubicacionVehiculoNuevo, String ubicacionVehiculoPrevio){
		boolean cambiarMonto = false;
		boolean cambiarUbicacion = false;
		
		if((montoPrevio == null
				&& montoNuevo != null)
				|| (montoPrevio != null
						&& montoNuevo == null)
						|| (montoPrevio != null
								&& montoNuevo != null
								&& montoPrevio.compareTo(montoNuevo) != 0)){
			cambiarMonto = true;
		}
		
		if((ubicacionVehiculoPrevio == null
				&& ubicacionVehiculoNuevo != null)
				|| (ubicacionVehiculoPrevio != null
						&& ubicacionVehiculoNuevo == null)
						|| (ubicacionVehiculoPrevio != null
								&& ubicacionVehiculoNuevo != null
								&& ubicacionVehiculoNuevo.compareTo(ubicacionVehiculoPrevio) != 0)){
			cambiarUbicacion = true;
		}
		
		if(cambiarMonto || cambiarUbicacion){
			RecuperacionSalvamento recuperacion = recuperacionSalvamentoService.obtenerRecuperacionSalvamento(estimacion.getId());
			if(recuperacion != null){
				if(cambiarMonto){
					Boolean abandonoIncosteable = 
						(montoNuevo != null 
								&& montoNuevo.doubleValue() > 0)? 
										Boolean.FALSE : 
											Boolean.TRUE;
					recuperacion.setAbandonoIncosteable(abandonoIncosteable);
					recuperacion.setMotivoAbandonoIncosteable((abandonoIncosteable)? 
						"No se ha capturado el monto provicion en el reporte de robo" : null);
					recuperacion.setValorEstimado((montoNuevo == null)? new BigDecimal(0.00) : montoNuevo);
				}
				if(cambiarUbicacion){
					recuperacion.setUbicacionDeterminacion(ubicacionVehiculoNuevo);
				}
				entidadService.save(recuperacion);
			}
		}
	}

	/**
	 * Este m�todo Realiza el envio a Ocra 
	 * 
	 * @param DefinicionSiniestroRoboDTO
	 */
	@Override
	public void envioOcra(DefinicionSiniestroRoboDTO siniestroRoboDTO)
			throws Exception {
		
			if (siniestroRoboDTO.getEnvioOcra()){
				String msj=	envioOCRAService.enviarAltaRoboOCRAServicio(siniestroRoboDTO.getReporteCabinaid());	
				if (!StringUtil.isEmpty(msj)){				
					String[] reg = null;
					Pattern p = Pattern.compile("\\|");				
					reg = p.split(msj);
					String tipo = reg[0];
					if(tipo.trim().equalsIgnoreCase(MENSAJE_ERROR_OCRA)){
						String mensaje = reg[1];
						throw new Exception( mensaje);
					}
				}
			}
	}
	
	public String getValorParametroGlobal(int appMidas, String parametro ){
		
		try{
			ParametroGlobal parametroGlobal = this.parametroGlobalService.obtenerPorIdParametro( appMidas , parametro );
			
			if( parametroGlobal!=null ){
				return parametroGlobal.getValor();
			}else{
				return "";
			}

		}catch(Exception e){
			log.error("Error en getValorParametroGlobal: "+e);
			// # EN CASO DE ERROR RETORNA SIEMPRE DESARROLLO
			return "";
		}
		
	}
}