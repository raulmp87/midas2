package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.sql.SQLException;
import java.util.logging.Level;

import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioPk;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaSeycosFacadeRemote;

@Stateless
public class GerenciaSeycosFacade implements GerenciaSeycosFacadeRemote{
	/**
	 * Metodo que replica el centro de operacion en Seycos.
	 * Si tiene id el centro de operacion entonces se actualiza, sino, se inserta.
	 */
	@Override
	public Long save(Gerencia gerencia) throws Exception {
		if(gerencia==null){
			throw new Exception("Gerencia is null!");
		}
		StoredProcedureHelper storedHelper = null;	
		String sp="SEYCOS.PKG_INT_MIDAS_E2.stp_catGer";
		Long idSeycos=null;
		try {
			LogDeMidasInterfaz.log("Entrando a GerenciaSeycosFacade.save..." + this, Level.INFO, null);
			storedHelper = new StoredProcedureHelper(sp);
			Long idCentroOperacion=(gerencia.getCentroOperacion()!=null)?gerencia.getCentroOperacion().getIdCentroOperacion():null;
			Long idPersona=(gerencia.getPersonaResponsable()!=null)?gerencia.getPersonaResponsable().getIdPersona():null;
			Domicilio domicilio=gerencia.getDomicilio();
			DomicilioPk pkDomicilio=(domicilio!=null)?domicilio.getIdDomicilio():null;
			Long idDomicilio=(pkDomicilio!=null)?pkDomicilio.getIdDomicilio():null;
			storedHelper.estableceParametro("pid_gerencia", val(gerencia.getIdGerencia()));
			storedHelper.estableceParametro("pdescripcion",val(gerencia.getDescripcion()));
			storedHelper.estableceParametro("pid_centro_oper",val(idCentroOperacion));
			storedHelper.estableceParametro("pid_persona_resp",val(idPersona));
			storedHelper.estableceParametro("pid_domicilio_resp",val(idDomicilio));
			storedHelper.estableceParametro("pcorreo_electronico",val(gerencia.getCorreoElectronico()));
			storedHelper.estableceParametro("psit_gerencia",(gerencia.getClaveEstatus()==1l)?"A":"I");
			storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz.log("Se ha guardado gerencia ..."+ this, Level.INFO, null);
			LogDeMidasInterfaz.log("Saliendo de GerenciaSeycosFacade.save..." + this, Level.INFO, null);
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp, gerencia.getClass(), codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de GerenciaSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en GerenciaSeycosFacade.save..." + this, Level.WARNING, e);
			throw e;
		}
		return idSeycos;
	}
	private Object val(Object value){
		return (value!=null)?value:"";
	}

	@Override
	public void unsubscribe(Gerencia gerencia) throws Exception {
		gerencia.setClaveEstatus(0l);//se inactiva el centro de operacion.(0 inactiva, 1 activa)
		save(gerencia);//Se actualiza el estatus del centro de operacion en Seycos
	}
}
