package mx.com.afirme.midas2.dao.impl.catalogos;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.catalogos.VarModifDescripcionDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion_;


@Stateless
public class VarModifDescripcionDaoImpl extends JpaDao<Long, VarModifDescripcion> implements
		VarModifDescripcionDao {

	@Override
	public List<VarModifDescripcion> findByFilters(VarModifDescripcion varModifDescripcion) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		Integer intNull = null;
		Expression<Integer> claveExp = cb.literal(varModifDescripcion.getIdGrupo());
		Expression<Integer> intNullExp = cb.literal(intNull);
		
		CriteriaQuery<VarModifDescripcion> criteriaQuery = cb.createQuery(entityClass);
		Root<VarModifDescripcion> root = criteriaQuery.from(VarModifDescripcion.class);
		Predicate predicado = cb.or(cb.equal(claveExp, intNullExp),cb.equal(root.get(VarModifDescripcion_.idGrupo), varModifDescripcion.getIdGrupo()));
		criteriaQuery.where(predicado);
		
		TypedQuery<VarModifDescripcion> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}

	@Override
	public List<VarModifDescripcion> findByGroup(Integer group) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<VarModifDescripcion> criteriaQuery = cb.createQuery(VarModifDescripcion.class);
		
		Root<VarModifDescripcion> root = criteriaQuery.from(VarModifDescripcion.class);
				
		Predicate predicado = cb.equal(root.get(VarModifDescripcion_.idGrupo), group);
		
		criteriaQuery.where(predicado);
		
		criteriaQuery.orderBy(cb.asc(root.get(VarModifDescripcion_.valor)));
		
		TypedQuery<VarModifDescripcion> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
		
	}

}
