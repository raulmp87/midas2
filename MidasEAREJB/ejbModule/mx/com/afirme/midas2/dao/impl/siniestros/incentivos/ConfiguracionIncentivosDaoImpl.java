package mx.com.afirme.midas2.dao.impl.siniestros.incentivos;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.siniestros.incentivos.ConfiguracionIncentivosDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.incentivos.ConfiguracionIncentivos;
import mx.com.afirme.midas2.domain.siniestros.incentivos.IncentivoAjustador;
import mx.com.afirme.midas2.domain.siniestros.incentivos.IncentivoAjustador.ConceptoIncentivo;
import mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO;
import mx.com.afirme.midas2.util.CollectionUtils;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class ConfiguracionIncentivosDaoImpl extends EntidadDaoImpl implements ConfiguracionIncentivosDao {
	
	@EJB
	EntidadDao entidadDao;

	@SuppressWarnings("unchecked")
	@Override
	public List<ConfiguracionIncentivos> obtenerConfiguracionesParaBandejaAutorizacion(){
		List<ConfiguracionIncentivos> configuraciones  = null;
		String queryStr = "SELECT model FROM ConfiguracionIncentivos model WHERE   model.estatus IN (:estXAutorizar,:estPreap )";
		Query query = entityManager.createQuery(queryStr);
		query.setParameter("estXAutorizar", ConfiguracionIncentivos.EstatusConfiguracionIncentivo.XAUTORIZAR);
		query.setParameter("estPreap", ConfiguracionIncentivos.EstatusConfiguracionIncentivo.PREAPROBAD);
//		query.setParameter("estReg", ConfiguracionIncentivos.EstatusConfiguracionIncentivo.REGISTRADO);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		configuraciones = query.getResultList();
		return configuraciones;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked", "unused" })
	@Override
	public List<IncentivoAjustadorDTO> obtenerIncentivoPreliminar(Long configuracionId, String codigoUsuario) {
		
		List<IncentivoAjustadorDTO> incentivosDTO = new ArrayList<IncentivoAjustadorDTO>();
		StoredProcedureHelper storedHelper = null;
		String spName = "MIDAS.PKGSIN_INCENTIVOS_AJUSTADORES.busquedaIncentivosPorConfig";
		Query query 							= null;
		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();
		int respuesta = 0;
		
		List<IncentivoAjustador> incentivos = entidadDao.findByProperty(IncentivoAjustador.class, "configuracionIncentivos.id", configuracionId);
		
		System.out.println("INCENTIVOS DAO: PRIMERA BUSQUEDA DE INCENTIVOS PRELIMINARES: " + ((incentivos != null )? incentivos.size() : "null" ));
		
		if (CollectionUtils.isEmpty(incentivos)) {
			try {
				
				System.out.println("EJECUTANDO PROCEDURE PARA CREAR LOS INCENTIVOS Y RETORNAR LISTA DE PRELIMINARES");
				
				String propiedades = "incentivoId,tipoConceptoPago,conceptoPagoDesc,numeroAjustador,nombreAjustador,oficinaAjustador,numeroReporte,oficinaReporte,fechaAsignacionAjustador,montoRecuperacion";
			    String columnasBaseDatos = "incentivoId,tipoConceptoPago,conceptoPagoDesc,numeroAjustador,nombreAjustador,oficinaAjustador,numeroReporte,oficinaReporte,fechaAsignacionAjustador,montoRecuperacion";

				storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
	            storedHelper.estableceMapeoResultados(IncentivoAjustadorDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
	        	storedHelper.estableceParametro("pConfiguracionId", configuracionId);
				storedHelper.estableceParametro("pCodigoUsuario", codigoUsuario);
				incentivosDTO = storedHelper.obtieneListaResultados();
	           
				

			} catch (Exception e) {
				respuesta = 1;
			}
		} else {
			
			System.out.println("EJECUTANDO JPQL PARA OBTENER LA INFORMACION DE LOS INCENTIVOS CREADOS PREVIAMENTE ");
			
			StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO(")
			
			.append(" incentivo.id, incentivo.seleccionado, incentivo.concepto, func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catConcepto, incentivo.concepto), ")
			.append(" incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, incentivo.reporteCabina.ajustador.oficina.nombreOficina, ")
			.append( "func('CONCAT', incentivo.reporteCabina.claveOficina, func('CONCAT', '-', func('CONCAT', incentivo.reporteCabina.consecutivoReporte, func('CONCAT', '-', incentivo.reporteCabina.anioReporte)))), ")
			.append(" incentivo.reporteCabina.oficina.nombreOficina, incentivo.reporteCabina.fechaHoraAsignacion, incentivo.importeRecuperacion) ")
			.append(" FROM  ").append(IncentivoAjustador.class.getSimpleName()).append(" incentivo ")
			.append(" WHERE incentivo.configuracionIncentivos.id = :configuracionId" )
			.append(" ORDER BY FUNC('DECODE', incentivo.concepto, 'INHABIL', '001', 'EFECTIVO', '002', 'COMPANIA', '003', 'SIPAC', '004', 'RECHAZO', '005', incentivo.concepto), ")
			.append(" incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.fechaHoraAsignacion ");
			


			Utilerias.agregaHashLista(listaParametrosValidos, "catConcepto", CatGrupoFijo.TIPO_CATALOGO.CONCEPTO_INCENTIVOS);
			Utilerias.agregaHashLista(listaParametrosValidos, "configuracionId", configuracionId);

			query = entityManager.createQuery(queryString.toString());

			Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			incentivosDTO=  query.getResultList();
		}
		
		System.out.println("RESULTADO FINAL DE LA BUSQUEDA DE LOS INCENTIVOS PRELIMINARES: " + ((incentivosDTO != null )? incentivosDTO.size() : "null" ));
		return incentivosDTO;
	}
	
	

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public List<IncentivoAjustadorDTO> obtenerIncentivoDiaInhabil(Long configuracionId) {

		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();

		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO(")
		
		.append(" incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, ")
		.append(" COUNT(incentivo.id), incentivo.configuracionIncentivos.montoDiaInhabil,  (COUNT(incentivo.id)*incentivo.configuracionIncentivos.montoDiaInhabil)) ")
		.append(" FROM  ").append(IncentivoAjustador.class.getSimpleName()).append(" incentivo ")
		.append(" WHERE incentivo.configuracionIncentivos.id = :configuracionId")
		.append(" AND incentivo.seleccionado = :seleccionado")
		.append(" AND incentivo.concepto = :concepto")
		.append(" GROUP BY incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, incentivo.configuracionIncentivos.montoDiaInhabil ");


		Utilerias.agregaHashLista(listaParametrosValidos, "concepto", ConceptoIncentivo.INHABIL.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "configuracionId", configuracionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", Boolean.TRUE.booleanValue());

		Query query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		return query.getResultList();	
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<IncentivoAjustadorDTO> obtenerIncentivoEfectivo(Long configuracionId) {

		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();

		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO(")
		
		.append(" incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, COUNT(incentivo.id), ")
		.append(" SUM(CASE WHEN incentivo.importeRecuperacion > config.montoRangoEfectivo  ")
		.append("     AND incentivo.importeRecuperacion <= config.montoHastaRangoEfectivo THEN incentivo.importeRecuperacion ELSE 0 END), ")
		.append(" (SUM(CASE WHEN incentivo.importeRecuperacion > config.montoRangoEfectivo  ")
		.append("     AND incentivo.importeRecuperacion <= config.montoHastaRangoEfectivo THEN incentivo.importeRecuperacion ELSE 0 END) * config.porcentajeRangoEfectivo), ")
		.append(" SUM(CASE WHEN config.esExcedenteSiniestro = :seleccionado AND config.condicionExcedente= :mayor ")
		.append("     AND incentivo.importeRecuperacion > config.montoExcedente  THEN incentivo.importeRecuperacion ")
		.append("     WHEN config.esExcedenteSiniestro = :seleccionado AND config.condicionExcedente= :mayorQue ")
		.append("     AND incentivo.importeRecuperacion >= config.montoExcedente  THEN incentivo.importeRecuperacion ELSE 0 END), ")
		.append(" (SUM(CASE WHEN config.esExcedenteSiniestro = :seleccionado AND config.condicionExcedente= :mayor ")
		.append("     AND incentivo.importeRecuperacion > config.montoExcedente  THEN incentivo.importeRecuperacion ")
		.append("     WHEN config.esExcedenteSiniestro = :seleccionado AND config.condicionExcedente= :mayorQue ")
		.append("     AND incentivo.importeRecuperacion >= config.montoExcedente  THEN incentivo.importeRecuperacion ELSE 0 END) * config.porcentajeExcedente), ")
		.append(" ((SUM(CASE WHEN incentivo.importeRecuperacion > config.montoRangoEfectivo  ")
		.append("     AND incentivo.importeRecuperacion <= config.montoHastaRangoEfectivo THEN incentivo.importeRecuperacion ELSE 0 END) * config.porcentajeRangoEfectivo) + ")
		.append(" (SUM(CASE WHEN config.esExcedenteSiniestro = :seleccionado AND config.condicionExcedente= :mayor ")
		.append("     AND incentivo.importeRecuperacion > config.montoExcedente  THEN incentivo.importeRecuperacion ")
		.append("     WHEN config.esExcedenteSiniestro = :seleccionado AND config.condicionExcedente= :mayorQue ")
		.append("     AND incentivo.importeRecuperacion >= config.montoExcedente  THEN incentivo.importeRecuperacion ELSE 0 END) * config.porcentajeExcedente))) ")
		.append(" FROM  ").append(IncentivoAjustador.class.getSimpleName()).append(" incentivo ")
		.append(" JOIN incentivo.configuracionIncentivos config  ")
		.append(" WHERE incentivo.configuracionIncentivos.id = :configuracionId")
		.append(" AND incentivo.seleccionado = :seleccionado")
		.append(" AND incentivo.concepto = :concepto")
		.append(" GROUP BY incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, config.porcentajeRangoEfectivo, config.porcentajeExcedente ");


		Utilerias.agregaHashLista(listaParametrosValidos, "concepto", ConceptoIncentivo.EFECTIVO.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "configuracionId", configuracionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", Boolean.TRUE.booleanValue());
		Utilerias.agregaHashLista(listaParametrosValidos, "mayor", "MAY");
		Utilerias.agregaHashLista(listaParametrosValidos, "mayorQue", "MAQ");

		Query query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		return query.getResultList();	
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<IncentivoAjustadorDTO> obtenerIncentivoCompanias(Long configuracionId) {

		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();

		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO(")
		
		.append(" incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, ")
		.append(" COUNT(incentivo.id), incentivo.configuracionIncentivos.montoRecuperacionCia,  (COUNT(incentivo.id)*incentivo.configuracionIncentivos.montoRecuperacionCia)) ")
		.append(" FROM  ").append(IncentivoAjustador.class.getSimpleName()).append(" incentivo ")
		.append(" WHERE incentivo.configuracionIncentivos.id = :configuracionId")
		.append(" AND incentivo.seleccionado = :seleccionado")
		.append(" AND incentivo.concepto = :concepto")
		.append(" GROUP BY incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, incentivo.configuracionIncentivos.montoRecuperacionCia ");


		Utilerias.agregaHashLista(listaParametrosValidos, "concepto", ConceptoIncentivo.COMPANIA.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "configuracionId", configuracionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", Boolean.TRUE.booleanValue());

		Query query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		return query.getResultList();	
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<IncentivoAjustadorDTO> obtenerIncentivoSIPAC(Long configuracionId) {

		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();

		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO(")
		
		.append(" incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, ")
		.append(" COUNT(incentivo.id), incentivo.configuracionIncentivos.montoRecuperacionSipac,  (COUNT(incentivo.id)*incentivo.configuracionIncentivos.montoRecuperacionSipac)) ")
		.append(" FROM  ").append(IncentivoAjustador.class.getSimpleName()).append(" incentivo ")
		.append(" WHERE incentivo.configuracionIncentivos.id = :configuracionId")
		.append(" AND incentivo.seleccionado = :seleccionado")
		.append(" AND incentivo.concepto = :concepto")
		.append(" GROUP BY incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, incentivo.configuracionIncentivos.montoRecuperacionSipac ");


		Utilerias.agregaHashLista(listaParametrosValidos, "concepto", ConceptoIncentivo.SIPAC.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "configuracionId", configuracionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", Boolean.TRUE.booleanValue());

		Query query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		return query.getResultList();	
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public List<IncentivoAjustadorDTO> obtenerIncentivoRechazos(Long configuracionId) {

		List<HashMap> listaParametrosValidos 	= new ArrayList<HashMap>();

		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.incentivos.IncentivoAjustadorDTO(")
		
		.append(" incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, ")
		.append(" COUNT(incentivo.id), SUM(incentivo.importeRecuperacion), (SUM(incentivo.importeRecuperacion)*incentivo.configuracionIncentivos.porcentajeRechazo)) ")
		.append(" FROM  ").append(IncentivoAjustador.class.getSimpleName()).append(" incentivo ")
		.append(" WHERE incentivo.configuracionIncentivos.id = :configuracionId")
		.append(" AND incentivo.seleccionado = :seleccionado")
		.append(" AND incentivo.concepto = :concepto")
		.append(" GROUP BY incentivo.reporteCabina.ajustador.id, incentivo.reporteCabina.ajustador.personaMidas.nombre, incentivo.configuracionIncentivos.porcentajeRechazo ");


		Utilerias.agregaHashLista(listaParametrosValidos, "concepto", ConceptoIncentivo.RECHAZO.toString());
		Utilerias.agregaHashLista(listaParametrosValidos, "configuracionId", configuracionId);
		Utilerias.agregaHashLista(listaParametrosValidos, "seleccionado", Boolean.TRUE.booleanValue());

		Query query = entityManager.createQuery(queryString.toString());

		Utilerias.estableceParametrosQuery(query, listaParametrosValidos);
		return query.getResultList();	
	}
	
	
	@SuppressWarnings({ "unchecked" })
	@Override
	public List<IncentivoAjustadorDTO> obtenerIncentivosAjustador(Long configuracionId) {
		
		List<IncentivoAjustadorDTO> incentivosDTO = new ArrayList<IncentivoAjustadorDTO>();
		StoredProcedureHelper storedHelper = null;
		String spName = "MIDAS.PKGSIN_INCENTIVOS_AJUSTADORES.obtenerIncentivosPorAjustador";				
		try {
			String propiedades = "numeroAjustador,nombreAjustador,montoTotalInhabiles,montoTotalEfectivo,montoTotalCompanias,montoTotalSIPAC,montoTotalRechazos,montoTotal";
		    String columnasBaseDatos = "numeroAjustador,nombreAjustador,montoTotalInhabiles,montoTotalEfectivo,montoTotalCompanias,montoTotalSIPAC,montoTotalRechazos,montoTotal";

			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
            storedHelper.estableceMapeoResultados(IncentivoAjustadorDTO.class.getCanonicalName(), propiedades, columnasBaseDatos);
        	storedHelper.estableceParametro("pConfiguracionId", configuracionId);
			incentivosDTO = storedHelper.obtieneListaResultados();
		} catch (Exception e) {			
			e.printStackTrace();
		}
		
		
		return incentivosDTO;
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void preRegistroIncentivos(Long configuracionId, String incentivosConcat) {
		StringBuilder queryString = new StringBuilder("UPDATE ").append(IncentivoAjustador.class.getCanonicalName());
		queryString.append(" model SET model.seleccionado = false WHERE model.configuracionIncentivos.id = :configuracionId ");
		Query query = entityManager.createQuery(queryString.toString());
		query.setParameter("configuracionId", configuracionId);		
		query.executeUpdate(); 		
		
		queryString = new StringBuilder("UPDATE ").append(IncentivoAjustador.class.getCanonicalName());
		queryString.append(" model SET model.seleccionado = true WHERE model.id IN (").append(incentivosConcat).append(")");
		query = entityManager.createQuery(queryString.toString());
		query.executeUpdate();
	}
	
}
