package mx.com.afirme.midas2.dao.impl;

import java.lang.reflect.ParameterizedType;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.EntityManager;
import javax.persistence.NonUniqueResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.util.JpaUtil;

import org.apache.commons.lang.StringUtils;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

public abstract class JpaDao<K, E> implements Dao<K, E> {

	@SuppressWarnings("unchecked")
	public JpaDao() {
		ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
		this.entityClass = (Class<E>) genericSuperclass.getActualTypeArguments()[1];
	}
 
	public void persist(E entity) { entityManager.persist(entity); }
	
	public E update(E entity) {
		return entityManager.merge(entity);
	}
 
	public void remove(E entity) { 
		entity = entityManager.merge(entity);
		entityManager.remove(entity); 
	}
 
	public E findById(K id) { return entityManager.find(entityClass, id); }
	
	@PersistenceContext
	protected EntityManager entityManager;

	protected Class<E> entityClass;

	public List<E> findAll() {
		
		CriteriaQuery<E> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(entityClass);
		criteriaQuery.from(entityClass);
		TypedQuery<E> query = entityManager.createQuery(criteriaQuery);
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}
	
	public E getReference(K id) { return entityManager.getReference(entityClass, id); }

	@SuppressWarnings("unchecked")
	public List<E> findByProperty(String propertyName, Object value) {
		final String queryString = "select model from "
				+ entityClass.getSimpleName() + " model where model."
				+ propertyName + "= :propertyValue";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("propertyValue", value);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	public List<E> findByFilters(E entity){
		//TODO: implementar este metodo con reflection
		
		return null;
	}
	
	/**
	 * Convenience method to obtain a single result from a <code>TypedQuery</code>.
	 * @param typedQuery
	 * @return
	 * @throws NonUniqueResultException
	 */
	protected E getSingleResult2(TypedQuery<E> typedQuery) throws NonUniqueResultException {
		return getSingleResult2(typedQuery.getResultList());
	}
	
	/**
	 * Returns a single entity or null.
	 * @param typedQuery
	 * @return the entity or null
	 * @throws NonUniqueResultException if more than one result
	 */
	protected E getSingleResult2(List<E> list) throws NonUniqueResultException {
		if (list.size() > 1) {
			throw new NonUniqueResultException();
		}
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}
	
	/**
	 * Sirve para buscar un listado de una Entidad a través de un objeto filtro.
	 * En caso de que el objeto filtro no pueda mapearse directamente sobre la entidad
	 * se hace uso de <code>FilterPersistanceAnnotation</code> sobre los métodos del filtro
	 * para definir en que forma se mapearán sobre la entidad.
	 * @param <E> entityClass
	 * @param <K> filter
	 * @return
	 */
	@SuppressWarnings({ "hiding" })
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter){
		return findByFilterObject(entityClass, filter, null);
	}
	
	
	/**
	 * Sirve para buscar un listado de una Entidad a través de un objeto filtro.
	 * En caso de que el objeto filtro no pueda mapearse directamente sobre la entidad
	 * se hace uso de <code>FilterPersistanceAnnotation</code> sobre los métodos del filtro
	 * para definir en que forma se mapearán sobre la entidad.
	 * @param <E> entityClass
	 * @param <K> filter
	 * @return
	 */
	@SuppressWarnings({ "unchecked", "hiding" })
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter, String orderBy){
		Map<String, Object> params = new HashMap<String, Object>();		
		final StringBuilder query = new StringBuilder("");		
		JpaUtil.createSimpleFilterQuery(entityClass, query, filter, params, orderBy);	
		Query queryResult = entityManager.createQuery(query.toString(), entityClass);
		JpaUtil.setQueryParametersByProperties(queryResult, params);
		queryResult.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return queryResult.getResultList();	
	}
	
	
	protected void setQueryParameters(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			entityQuery.setParameter(pairs.getKey(), pairs.getValue());
		}		
	}
	
	protected void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	protected static String getValidProperty(String property){
		String validProperty=property;
		StringBuilder str=new StringBuilder("");
		if(property.contains(".")){
			int i=0;
			for(String word:property.split("\\.")){
				if(i==0){
					word=word.toLowerCase();
				}else{
					word=StringUtils.capitalize(word);
				}
				str.append(word);
				i++;
			}
			validProperty=str.toString();
		}
		return validProperty;
	}
}
