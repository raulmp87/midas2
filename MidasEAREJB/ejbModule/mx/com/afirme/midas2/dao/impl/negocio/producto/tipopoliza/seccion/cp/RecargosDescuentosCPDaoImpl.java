package mx.com.afirme.midas2.dao.impl.negocio.producto.tipopoliza.seccion.cp;

import java.math.BigDecimal;

import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.RecargosDescuentosCPDTO;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.cp.RecargosDescuentosCPDao;

@Stateless
public class RecargosDescuentosCPDaoImpl extends JpaDao<BigDecimal, RecargosDescuentosCPDTO> implements RecargosDescuentosCPDao{
	
}
