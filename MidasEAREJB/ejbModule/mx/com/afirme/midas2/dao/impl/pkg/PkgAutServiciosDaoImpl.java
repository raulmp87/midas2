package mx.com.afirme.midas2.dao.impl.pkg;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.pkg.PkgAutServiciosDao;
import mx.com.afirme.midas2.dto.MensajeDTO;

import org.springframework.jdbc.core.JdbcTemplate;

@Stateless
public class PkgAutServiciosDaoImpl implements PkgAutServiciosDao {

	
	private JdbcTemplate jdbcTemplate;
	private DataSource dataSource;
	
	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
		this.dataSource = dataSource;
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	

	
	@Override
	public String getZonaCirculacion(Integer idEstado, Integer idMunicipio) {
		 String claveZonaCircualcion=null;
		 String spName="MIDAS.pkgAUT_Servicios.spAUT_ZonCircPorCiudad";
		 String [] atributosDTO = { "mensaje"};
		 String [] columnasCursor = { "ZONACIRCULACION"};
		 try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pIdEstado", idEstado);
				storedHelper.estableceParametro("pIdCiudad", idMunicipio);
				storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);
				
				MensajeDTO mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
				claveZonaCircualcion =  mensajeDTO.getMensaje();
			} catch (Exception e) {
				throw new RuntimeException("Error en :" + spName,e);
			}
			
		return claveZonaCircualcion;
	}

}
