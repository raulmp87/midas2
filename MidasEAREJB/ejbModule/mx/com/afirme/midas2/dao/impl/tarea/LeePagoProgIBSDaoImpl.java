package mx.com.afirme.midas2.dao.impl.tarea;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dao.tarea.LeePagoProgIBSDao;
import mx.com.afirme.midas2.domain.bitacora.pago.programado.ibs.BitacoraPagoProgIBSTO;

@Stateless
public class LeePagoProgIBSDaoImpl implements LeePagoProgIBSDao {
	
	private static final String PROCEDURE_COMPLEMENTAR_INFORMACION = "MIDAS.PKG_PROCESA_IBS.stp_ComplementaInfoIBS";
	private static final String PROCEDURE_MIGRA_INFO_IBS = "MIDAS.PKG_PROCESA_IBS.stp_Migrar_Informacion_IBS";
	
	@PersistenceContext
	private EntityManager manager;

	@Override
	public void saveBitacoraPagoProgIBS(List<BitacoraPagoProgIBSTO> entities) {
		try{
			for(BitacoraPagoProgIBSTO entity : entities){
				manager.persist(entity);
			}
		} catch (Exception e){
			throw new RuntimeException(e);
		}
	}

	@Override
	public void complementarBitacoraPagoProgIBS() {
		try{
			StoredProcedureHelper stpHelper = new StoredProcedureHelper(PROCEDURE_COMPLEMENTAR_INFORMACION, StoredProcedureHelper.DATASOURCE_MIDAS);
			stpHelper.ejecutaActualizar();
		}catch (Exception err){
			
			throw new RuntimeException(err);
		}
	}

	@Override
	public void migrarInformacionPagoProgIBS() {
		try{
			StoredProcedureHelper stpHelper = new StoredProcedureHelper(PROCEDURE_MIGRA_INFO_IBS, StoredProcedureHelper.DATASOURCE_MIDAS);
			stpHelper.ejecutaActualizar();
		}catch(Exception err){
			throw new RuntimeException(err);
		}
	}
}
