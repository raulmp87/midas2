package mx.com.afirme.midas2.dao.impl.informacionVehicular;

import java.sql.SQLException;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.informacionVehicular.DiferenciasAmisDao;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class DiferenciasAmisDaoImpl extends JpaDao<Long, DiferenciasAmis> implements DiferenciasAmisDao{

	@SuppressWarnings("unchecked")
	@Override
	public List<DiferenciasAmis> findAllIncisos(Long arg0)  throws SQLException, Exception {
		
		String queryString = "select model from DiferenciasAmis model ";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}

	@Override
	public List<DiferenciasAmis> findAllPolizas(Long arg0) throws SQLException,
			Exception {
		String queryString = "select model, p.numeroPoliza " +
				"as numeroPoliza " +
				"from DiferenciasAmis model " +
				"inner join PolizaDTO p " +
				"on model.idToPoliza = p.idToPoliza";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		List<DiferenciasAmis> list = query.getResultList();
		
		for ( DiferenciasAmis item: list ){
			System.out.println( item.getNumeroPoliza() );
			
		}
		
		return null;
	}
}
