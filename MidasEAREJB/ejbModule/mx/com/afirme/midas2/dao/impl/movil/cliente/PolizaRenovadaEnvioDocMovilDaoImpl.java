package mx.com.afirme.midas2.dao.impl.movil.cliente;

import java.util.List;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.movil.cliente.PolizaRenovadaEnvioDocMovilDao;
import mx.com.afirme.midas2.domain.movil.cliente.PolizaRenovadaEnvioDocMovil;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class PolizaRenovadaEnvioDocMovilDaoImpl extends EntidadDaoImpl implements PolizaRenovadaEnvioDocMovilDao {
	
	public static final Logger LOG = Logger.getLogger(PolizaRenovadaEnvioDocMovilDaoImpl.class);
	
	public void save(PolizaRenovadaEnvioDocMovil entity) {
		LogDeMidasEJB3.log("saving MIDAS.TRUSRPOLRENOVADAENVIODOC instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	public void delete(PolizaRenovadaEnvioDocMovil entity) {
		LogDeMidasEJB3.log("deleting MIDAS.TRUSRPOLRENOVADAENVIODOC instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(PolizaRenovadaEnvioDocMovil.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PolizaRenovadaEnvioDocMovil update(PolizaRenovadaEnvioDocMovil entity) {
		LogDeMidasEJB3.log("updating MIDAS.TRUSRPOLRENOVADAENVIODOC instance", Level.INFO, null);
		try {
			PolizaRenovadaEnvioDocMovil result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PolizaRenovadaEnvioDocMovil findById(Long id) {
		LogDeMidasEJB3.log("finding MIDAS.TRUSRPOLRENOVADAENVIODOC instance with id: " + id,
				Level.INFO, null);
		try {
			PolizaRenovadaEnvioDocMovil instance = entityManager.find(
					PolizaRenovadaEnvioDocMovil.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	
	@SuppressWarnings("unchecked")
	public List<PolizaRenovadaEnvioDocMovil> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding MIDAS.TRUSRPOLRENOVADAENVIODOC instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MIDAS.TRUSRPOLRENOVADAENVIODOC model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<PolizaRenovadaEnvioDocMovil> findAll() {
		LogDeMidasEJB3.log("finding all MIDAS.TRUSRPOLRENOVADAENVIODOC instances", Level.INFO, null);
		try {
			final String queryString = "select model from MIDAS.TRUSRPOLRENOVADAENVIODOC model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	@EJB
	private EntidadDao entidadDao;

	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
}