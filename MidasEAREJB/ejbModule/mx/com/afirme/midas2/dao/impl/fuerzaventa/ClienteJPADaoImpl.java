package mx.com.afirme.midas2.dao.impl.fuerzaventa;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.fuerzaventa.ClienteJPADao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ClienteJPA;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
@Stateless
public class ClienteJPADaoImpl extends EntidadHistoricoDaoImpl implements ClienteJPADao{
	
	@Override
	public List<ClienteJPA> findByFilters(ClienteJPA filtroClienteJPA) {
		List<ClienteJPA> lista=new ArrayList<ClienteJPA>();
		
		final StringBuilder queryString=new StringBuilder("");
		queryString.append("select model from ClienteJPA model  ");
		
		Map<String,Object> params=new HashMap<String, Object>();
		
		if(filtroClienteJPA!=null){
			if(filtroClienteJPA.getIdCliente()!=null){
				addCondition(queryString, "model.idCliente=:idCliente");
				params.put("idCliente", filtroClienteJPA.getIdCliente());
			}
			if(filtroClienteJPA.getIdPersona()!=null){
				addCondition(queryString, "model.idPersona=:idPersona");
				params.put("idPersona", filtroClienteJPA.getIdPersona());
			}
		}
		Query query = entityManager.createQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			setQueryParametersByProperties(query, params);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		
		//FIXME: VALORMAXIMO HARCODEADO.
		query.setMaxResults(30);
		
		lista=query.getResultList();
		return lista;
	}

	@Override
	public ClienteJPA loadById(ClienteJPA clienteJPA){
		List<ClienteJPA> list = new ArrayList<ClienteJPA>();
		ClienteJPA entidad = new ClienteJPA();
		
		if(clienteJPA!=null && clienteJPA.getIdCliente()!=null){
			
			entidad.setIdCliente(clienteJPA.getIdCliente());
			list=findByFilters(entidad);
			if(list!=null && !list.isEmpty()){
				entidad=list.get(0);
			}else{
				entidad = null;
			}
		}
		return entidad;
	}
}
