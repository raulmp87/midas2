package mx.com.afirme.midas2.dao.impl.fuerzaventa.mediciones.conf;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.base.Scrollable;
import mx.com.afirme.midas2.dao.componente.scrollable.ScrollableNavigationDao;
import mx.com.afirme.midas2.dao.fuerzaventa.mediciones.conf.ConfiguracionMedicionesDao;

@Stateless
public class ConfiguracionMedicionesDaoImpl implements ConfiguracionMedicionesDao {

		
	@Override
	public <T extends Scrollable> List<T> buscar(T filtro) {
				
		String queryString = "select model from " + filtro.getClass().getSimpleName() + " model ";
		
		Query query = entityManager.createQuery(queryString);
				
		return scrollableNavigationDao.getResultList(query, filtro);

	}
	
	@Override
	public Integer getSize(String className) {
		
		String queryString = "select COUNT(model.id) from " + className + " model ";
		
		Query query = entityManager.createQuery(queryString);
				
		return ((Long)query.getSingleResult()).intValue();
		
	}
	
	
		
	@EJB
	private ScrollableNavigationDao scrollableNavigationDao;

	@PersistenceContext
	private EntityManager entityManager;
	
}
