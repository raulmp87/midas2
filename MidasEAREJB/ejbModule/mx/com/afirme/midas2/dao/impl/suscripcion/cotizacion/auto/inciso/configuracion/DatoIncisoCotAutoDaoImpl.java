package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoDao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;

@Stateless
public class DatoIncisoCotAutoDaoImpl extends JpaDao<Long, DatoIncisoCotAuto>
		implements DatoIncisoCotAutoDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<DatoIncisoCotAuto> getDatosCoberturaInciso(
			BigDecimal idCotizacion, BigDecimal numeroInciso, BigDecimal idTcRamo, BigDecimal idTcSubRamo, BigDecimal idToCobertura) {
		String sql = "select model from DatoIncisoCotAuto model where model.idToCotizacion = :idToCotizacion "
				+ "and model.numeroInciso = :numeroInciso "
				+ "and model.idTcRamo = :idTcRamo "
				+ "and model.idTcSubRamo = :idTcSubRamo "
				+ "and model.idToCobertura = :idToCobertura "
				+ "and model.claveDetalle = 0 "
				+ "order by model.idTcRamo, model.idDato";
		Query query = entityManager.createQuery(sql);
		query.setParameter("idToCotizacion", idCotizacion);
		query.setParameter("numeroInciso", numeroInciso);
		query.setParameter("idTcRamo", idTcRamo);		
		query.setParameter("idTcSubRamo", idTcSubRamo);
		query.setParameter("idToCobertura", idToCobertura);	
		return (List<DatoIncisoCotAuto>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DatoIncisoCotAuto> getDatosRamoInciso(BigDecimal idCotizacion,
			BigDecimal numeroInciso, BigDecimal idTcRamo) {
		String sql = "select model from DatoIncisoCotAuto model where model.idToCotizacion = :idToCotizacion "
				+ "and model.numeroInciso = :numeroInciso "
				+ "and model.idTcRamo = :idTcRamo "
				+ "and model.idTcSubRamo = 0 "
				+ "and model.idToCobertura = 0 "
				+ "and model.claveDetalle = 0 "
				+ "order by model.idTcRamo, model.idDato";
		Query query = entityManager.createQuery(sql);
		query.setParameter("idToCotizacion", idCotizacion);
		query.setParameter("numeroInciso", numeroInciso);
		query.setParameter("idTcRamo", idTcRamo);			
		return (List<DatoIncisoCotAuto>) query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DatoIncisoCotAuto> getDatosSubRamoInciso(
			BigDecimal idCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo, BigDecimal idTcSubRamo) {
		String sql = "select model from DatoIncisoCotAuto model where model.idToCotizacion = :idToCotizacion "
				+ "and model.numeroInciso = :numeroInciso "
				+ "and model.idTcRamo = :idTcRamo "
				+ "and model.idTcSubRamo = :idTcSubRamo "
				+ "and model.idToCobertura = 0 "
				+ "and model.claveDetalle = 0 "
				+ "order by model.idTcRamo, model.idDato";
		Query query = entityManager.createQuery(sql);
		query.setParameter("idToCotizacion", idCotizacion);
		query.setParameter("numeroInciso", numeroInciso);
		query.setParameter("idTcRamo", idTcRamo);			
		query.setParameter("idTcSubRamo", idTcSubRamo);		
		return (List<DatoIncisoCotAuto>) query.getResultList();
	}
	
	@Override
	public DatoIncisoCotAuto getDatoIncisoByConfiguracion(
			BigDecimal idCotizacion, BigDecimal idToSeccion, BigDecimal numeroInciso, 
			BigDecimal idTcRamo, BigDecimal idTcSubRamo, BigDecimal idToCobertura,
			Short claveDetalle, BigDecimal idDato) {
		try {
			String sql = "select model from DatoIncisoCotAuto model where model.idToCotizacion = :idToCotizacion "
					+ "and model.numeroInciso = :numeroInciso "
					+ "and model.idToSeccion = :idToSeccion "
					+ "and model.idTcRamo = :idTcRamo "
					+ "and model.idTcSubRamo = :idTcSubRamo "
					+ "and model.idToCobertura = :idToCobertura "
					+ "and model.claveDetalle = :claveDetalle "
					+ "and model.idDato = :idDato "
					+ "order by model.idTcRamo, model.idDato";
			Query query = entityManager.createQuery(sql);
			query.setParameter("idToCotizacion", idCotizacion);
			query.setParameter("numeroInciso", numeroInciso);
			query.setParameter("idToSeccion", idToSeccion);
			query.setParameter("idTcRamo", idTcRamo);		
			query.setParameter("idTcSubRamo", idTcSubRamo);
			query.setParameter("idToCobertura", idToCobertura);
			query.setParameter("claveDetalle", claveDetalle);	
			query.setParameter("idDato", idDato);	
			
			return (DatoIncisoCotAuto) query.getSingleResult();
		} catch(NoResultException e) {
	        return null;
	    }
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DatoIncisoCotAuto> getDatosIncisoCotAutoPorCotizacion(BigDecimal idCotizacion){
		String sql = "select model from DatoIncisoCotAuto model where model.idToCotizacion = :idToCotizacion";			
		Query query = entityManager.createQuery(sql);
		query.setParameter("idToCotizacion", idCotizacion);
		return (List<DatoIncisoCotAuto>) query.getResultList();
	}

}
