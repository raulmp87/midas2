package mx.com.afirme.midas2.dao.impl.suscripcion.cotizacion.auto;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.CoberturaPrimaCotExpressDao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CoberturaPrimaCotExpress;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CoberturaPrimaCotExpress_;

@Stateless
public class CoberturaPrimaCotExpressDaoImpl extends JpaDao<Long, CoberturaPrimaCotExpress> implements CoberturaPrimaCotExpressDao {

	@Override
	public List<CoberturaPrimaCotExpress> buscarPorCombinacion(
			CoberturaPrimaCotExpress coberturaPrimaCotExpress) {		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<CoberturaPrimaCotExpress> cq = cb.createQuery(CoberturaPrimaCotExpress.class);
		Root<CoberturaPrimaCotExpress> cpce = cq.from(CoberturaPrimaCotExpress.class);
		Predicate p = cb.equal(cpce.get(CoberturaPrimaCotExpress_.idAgrupadorTarifa), coberturaPrimaCotExpress.getIdAgrupadorTarifa());
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.idVerAgrupadorTarifa), coberturaPrimaCotExpress.getIdVerAgrupadorTarifa()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.idMoneda), coberturaPrimaCotExpress.getIdMoneda()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.idVersionCarga), coberturaPrimaCotExpress.getIdVersionCarga()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.claveTipoBien), coberturaPrimaCotExpress.getClaveTipoBien()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.claveEstilo), coberturaPrimaCotExpress.getClaveEstilo()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.modeloVehiculo), coberturaPrimaCotExpress.getModeloVehiculo()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.idTipoVehiculo), coberturaPrimaCotExpress.getIdTipoVehiculo()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.idMarcaVehiculo), coberturaPrimaCotExpress.getIdMarcaVehiculo()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.claveZonaCirculacion), coberturaPrimaCotExpress.getClaveZonaCirculacion()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.claveUsoVehiculo), coberturaPrimaCotExpress.getClaveUsoVehiculo()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.claveServicioVehiculo), coberturaPrimaCotExpress.getClaveServicioVehiculo()));
		p = cb.and(p, cb.equal(cpce.get(CoberturaPrimaCotExpress_.paquete), coberturaPrimaCotExpress.getPaquete()));
		cq.where(p);
		try{
			return entityManager.createQuery(cq).getResultList();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return null;
	}
	
}
