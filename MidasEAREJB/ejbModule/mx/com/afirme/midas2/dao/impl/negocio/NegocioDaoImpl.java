package mx.com.afirme.midas2.dao.impl.negocio;

import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TemporalType;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.NegocioDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.Negocio_;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioDaoImpl extends JpaDao<Long, Negocio> implements
NegocioDao {
	@Override
	public List<Negocio> findByFilters(Negocio filtroNegocio) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Negocio> criteriaQuery = cb.createQuery(Negocio.class);
		Root<Negocio> root = criteriaQuery.from(Negocio.class);
		criteriaQuery.distinct(true);
		Predicate predicado = cb.and(cb.equal(cb.upper(root.get(Negocio_.claveNegocio)), filtroNegocio.getClaveNegocio()),
                cb.equal(root.get(Negocio_.claveEstatus),filtroNegocio.getClaveEstatus()));
	
		Predicate predicado2 = null;
		if(filtroNegocio.getAplicaUsuarioExterno() != null && filtroNegocio.getAplicaUsuarioExterno()){
			predicado2 = cb.equal(root.get(Negocio_.aplicaUsuarioExterno), filtroNegocio.getAplicaUsuarioExterno());
		}
		if(filtroNegocio.getAplicaUsuarioInterno() != null && filtroNegocio.getAplicaUsuarioInterno()){
			predicado2 = cb.equal(root.get(Negocio_.aplicaUsuarioInterno), filtroNegocio.getAplicaUsuarioInterno());
		}
		
		if(filtroNegocio.getAplicaSucursal()){
			predicado2 = cb.equal(root.get(Negocio_.aplicaSucursal), filtroNegocio.getAplicaSucursal());
		}
		
		if(predicado2 != null){
			predicado = cb.and(predicado, predicado2);
		}

		if(filtroNegocio.getClaveNegocio() != null){	
		    criteriaQuery.where(predicado);
		}
		criteriaQuery.orderBy(cb.asc(root.get(Negocio_.descripcionNegocio)));
		TypedQuery<Negocio> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
}
@Override
public List<Negocio> listRelated(Long arg0) {
	// TODO Ap�ndice de m�todo generado autom�ticamente
	return null;
}

	@Override
	public List<NegocioAgente> listarNegocioAgentes(Long idToNegocio) {
		String queryString = "select negocioAgente from NegocioAgente negocioAgente where " + 
		" negocioAgente.negocio.idToNegocio = :idToNegocio order by negocioAgente.idToNegAgente";
		Query query = entityManager.createQuery(queryString);
		query.setParameter("idToNegocio", idToNegocio);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	@Override
	public List<Negocio> listarNegociosNoBorrados(String cveNegocio) {
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		CriteriaQuery<Negocio> criteriaQuery = cb.createQuery(Negocio.class);
		Root<Negocio> root = criteriaQuery.from(Negocio.class);
		criteriaQuery.distinct(true);
		Predicate predicado = cb.and(cb.equal(cb.upper(root.get(Negocio_.claveNegocio)), cveNegocio),
                cb.notEqual(root.get(Negocio_.claveEstatus), Negocio.EstatusNegocio.BORRADO.obtenerEstatus()),
                cb.notEqual(root.get(Negocio_.idToNegocio), 0l));
		criteriaQuery.where(predicado);
		criteriaQuery.orderBy(cb.asc(root.get(Negocio_.descripcionNegocio)));
		TypedQuery<Negocio> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Negocio> listarNegocioSinAgentes(String claveNegocio, Boolean esExterno) {
		//Comentario integracion
		String queryString = "select model from Negocio model where " + 
		" model.idToNegocio not in (select tna.negocio.idToNegocio from NegocioAgente tna)" +
		" and model.claveNegocio = '"+claveNegocio+"' "+
		" and model.claveEstatus = 1"+
		" and model.fechaInicioVigencia < :fechaActual " +
		" and (model.fechaFinVigencia > :fechaActual or model.fechaFinVigencia is null) ";
		
		if(esExterno != null){
			if(esExterno){
				queryString += " and model.aplicaUsuarioExterno = true ";
			}else{
				queryString += " and model.aplicaUsuarioInterno = true ";
			}
		}
		queryString += " order by model.descripcionNegocio ASC ";
		Query query = entityManager.createQuery(queryString);
		
		GregorianCalendar gcFechaInicio = new GregorianCalendar();
		gcFechaInicio.setTime(new Date());
		gcFechaInicio.add(GregorianCalendar.DATE, 1);
		gcFechaInicio.add(GregorianCalendar.MILLISECOND, -1);
		
		System.out.println("Poliza: Rango de Fecha inicio ->" + gcFechaInicio.getTime());
		
		query.setParameter("fechaActual", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
		
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Negocio> listarNegocioAplicaUsuario(String claveNegocio, boolean isOutsider) {
		String queryString = "select model from Negocio model where " + 
		" model.claveNegocio = '"+claveNegocio+"' "+
		" and model.claveEstatus = 1";
		if(isOutsider){
			queryString += " and model.aplicaUsuarioExterno = 1 ";
		}else{
			queryString += " and model.aplicaUsuarioInterno = 1 ";
		}
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}
	@Override
	public List<Negocio> listarNegociosPorDescripcion(String descripcion, Boolean esExterno) {
		String queryString = "select model from Negocio model where " + 
		" UPPER (model.descripcionNegocio) LIKE '%"+descripcion.toUpperCase()+"%'"+
		" and model.claveEstatus = 1";
		if(esExterno != null){
			if(esExterno){
				queryString += " and model.aplicaUsuarioExterno = " + true;
			}else{
				queryString += " and model.aplicaUsuarioInterno = " + true;
			}
		}
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		return query.getResultList();
	}

}
