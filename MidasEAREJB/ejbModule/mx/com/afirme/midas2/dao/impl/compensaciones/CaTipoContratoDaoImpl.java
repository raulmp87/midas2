/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoContratoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoContrato;

import org.apache.log4j.Logger;

@Stateless
public class CaTipoContratoDaoImpl implements CaTipoContratoDao {	
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";	

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaTipoContratoDaoImpl.class);
	
	public void save(CaTipoContrato entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoContrato 	::		CaTipoContratoDaoImpl	::	save	::	INICIO	::	");
	        try {
            entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoContrato 	::		CaTipoContratoDaoImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoContrato 	::		CaTipoContratoDaoImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
	
    public void delete(CaTipoContrato entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoContrato 	::		CaTipoContratoDaoImpl	::	delete	::	INICIO	::	");
	        try {
        	entity = entityManager.getReference(CaTipoContrato.class, entity.getId());
            entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoContrato 	::		CaTipoContratoDaoImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoContrato 	::		CaTipoContratoDaoImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoContrato update(CaTipoContrato entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoContrato 	::		CaTipoContratoDaoImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoContrato result = entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoContrato 	::		CaTipoContratoDaoImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoContrato 	::		CaTipoContratoDaoImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaTipoContrato findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoContratoDaoImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoContrato instance = entityManager.find(CaTipoContrato.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoContratoDaoImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoContratoDaoImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }

    @SuppressWarnings("unchecked")
    public List<CaTipoContrato> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoContratoDaoImpl	::	findByProperty	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoContrato model where model." 
			 						+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoContratoDaoImpl	::	findByProperty	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoContratoDaoImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoContrato> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoContrato> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoContrato> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoContrato> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}

	@SuppressWarnings("unchecked")
	public List<CaTipoContrato> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoContratoDaoImpl	::	findAll	::	INICIO	::	");
			try {
			final String queryString = "select model from CaTipoContrato model";
			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoContratoDaoImpl	::	findAll	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoContratoDaoImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
}
