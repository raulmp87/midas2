package mx.com.afirme.midas2.dao.impl.fortimax;
import static mx.com.afirme.midas2.utils.CommonUtils.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.fortimax.CatalogoAplicacionFortimaxDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ParametroAplicacionFortimax;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.ParametroAplicacionFortimaxService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class CatalogoAplicacionFortimaxDaoImpl extends EntidadDaoImpl implements CatalogoAplicacionFortimaxDao{
	private EntidadService entidadService;
	private ParametroAplicacionFortimaxService parametroAplicacionFortimaxService;
	@Override
	public void delete(Long idAplicacion) throws MidasException {
		if(isNull(idAplicacion)){
			onError("Favor de proporcionar la clave de la aplicacion");
		}
		CatalogoAplicacionFortimax jpaObject=entidadService.findById(CatalogoAplicacionFortimax.class,idAplicacion);
		entidadService.remove(jpaObject);
	}

	@Override
	public Long delete(CatalogoAplicacionFortimax aplicacion) throws MidasException {
		if(isNull(aplicacion) || isNull(aplicacion.getId())){
			onError("Favor de proporcionar la clave de la aplicacion");
		}
		delete(aplicacion.getId());
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CatalogoAplicacionFortimax> findByFilters(CatalogoAplicacionFortimax filtro) {
		List<CatalogoAplicacionFortimax> list=new ArrayList<CatalogoAplicacionFortimax>();
		if(isNotNull(filtro)){
			Map<String,Object> params=new HashMap<String, Object>();
			final StringBuilder queryString=new StringBuilder("");
			queryString.append("select model from CatalogoAplicacionFortimax model ");
			if(isNotNull(filtro.getId())){
				addCondition(queryString, "model.id = :id");
				params.put("id", filtro.getId());
			}else{
				if(isValid(filtro.getNombreAplicacion())){
					addCondition(queryString, "model.nombreAplicacion like UPPER(:nombreAplicacion)");
					params.put("nombreAplicacion", filtro.getNombreAplicacion());
				}
				if(isValid(filtro.getNombreFortimax())){
					addCondition(queryString, "model.nombreFortimax like UPPER(:nombreFortimax)");
					params.put("nombreFortimax", filtro.getNombreFortimax());
				}
			}
			Query query = entityManager.createQuery(getQueryString(queryString));
			setQueryParametersByProperties(query, params);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
			list=query.getResultList();
		}
		return list;
	}

	@Override
	public CatalogoAplicacionFortimax getAplicacionPorNombre(String nombreAplicacion) throws MidasException {
		CatalogoAplicacionFortimax aplicacion=null;
		CatalogoAplicacionFortimax filtro=new CatalogoAplicacionFortimax();
		filtro.setNombreAplicacion(nombreAplicacion);
		List<CatalogoAplicacionFortimax> list=findByFilters(filtro);
		if(!isEmptyList(list)){
			aplicacion=list.get(0);
		}else{
			onError("No se encontro la aplicacion:'"+nombreAplicacion+"'");
		}
		return aplicacion;
	}

	@Override
	public CatalogoAplicacionFortimax loadById(Long idAplicacion) throws MidasException {
		CatalogoAplicacionFortimax aplicacion=null;
		CatalogoAplicacionFortimax filtro=new CatalogoAplicacionFortimax();
		filtro.setId(idAplicacion);
		List<CatalogoAplicacionFortimax> list=findByFilters(filtro);
		if(!isEmptyList(list)){
			aplicacion=list.get(0);
		}else{
			onError("No se encontro la aplicacion con la clave:'"+idAplicacion+"'");
		}
		return aplicacion;
	}

	@Override
	public Long save(CatalogoAplicacionFortimax aplicacion) throws MidasException {
		CatalogoAplicacionFortimax jpaObject=entidadService.save(aplicacion);
		return (jpaObject!=null)?jpaObject.getId():null;
	}
	/**
	 * 
	 * @param nombreAplicacion
	 * @return
	 * @throws MidasException
	 */
	@Override
	public String[] obtenerParametrosAplicacion(String nombreAplicacion) throws MidasException{
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		List<ParametroAplicacionFortimax> parametros=parametroAplicacionFortimaxService.obtenerParametrosPorAplicacion(nombreAplicacion);
		String[] params=null;
		if(!isEmptyList(parametros)){
			params=new String[parametros.size()];
			int count=0;
			for(ParametroAplicacionFortimax parametro:parametros){
				if(isNotNull(parametro)){
					params[count]=parametro.getNombreParametro();
				}
				count++;
			}
		}
		return params;
	}
	/**
	 * Obtiene el atributo o propiedad llave de la serie de parametros establecidos por la aplicacion.
	 * @param nombreAplicacion
	 * @return
	 * @throws MidasException
	 */
	@Override
	public String obtenerParametroLlavePorAplicacion(String nombreAplicacion) throws MidasException{
		if(!isValid(nombreAplicacion)){
			onError("Favor de proporcionar el nombre de la aplicacion");
		}
		ParametroAplicacionFortimax parametroClave=parametroAplicacionFortimaxService.obtenerParametroLlavePorAplicacion(nombreAplicacion);
		String propiedadLlave=(isNotNull(parametroClave) && isValid(parametroClave.getNombreParametro()))?parametroClave.getNombreParametro():null;
		return propiedadLlave; 
	}
	/**
	 * ==========================================================
	 * Setters and getters
	 * ==========================================================
	 */
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@EJB
	public void setParametroAplicacionFortimaxService(ParametroAplicacionFortimaxService parametroAplicacionFortimaxService) {
		this.parametroAplicacionFortimaxService = parametroAplicacionFortimaxService;
	}
}