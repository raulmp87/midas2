package mx.com.afirme.midas2.dao.impl.negocio.cliente;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionSeccionPorNegocioDao;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccionPorNegocio;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccionPorNegocio_;

@Stateless
public class ConfigucacionSeccionPorNegocioDaoImpl extends JpaDao<Long, ConfiguracionSeccionPorNegocio>
		implements ConfiguracionSeccionPorNegocioDao {

	@Override
	public List<ConfiguracionSeccionPorNegocio> findByNegocio(Long idNegocio) {
		
		CriteriaBuilder cb = entityManager.getCriteriaBuilder();
		
		CriteriaQuery<ConfiguracionSeccionPorNegocio> criteriaQuery = cb.createQuery(ConfiguracionSeccionPorNegocio.class);
		Root<ConfiguracionSeccionPorNegocio> root = criteriaQuery.from(ConfiguracionSeccionPorNegocio.class);
		
		Predicate predicado1 = cb.equal(root.get(ConfiguracionSeccionPorNegocio_.idNegocio), idNegocio);
		
		criteriaQuery.where(predicado1);
		
		TypedQuery<ConfiguracionSeccionPorNegocio> query = entityManager.createQuery(criteriaQuery);
		return query.getResultList();
	}

}
