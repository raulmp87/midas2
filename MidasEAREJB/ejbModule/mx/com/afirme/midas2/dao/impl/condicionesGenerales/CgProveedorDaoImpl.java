package mx.com.afirme.midas2.dao.impl.condicionesGenerales;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.condicionesGenerales.CgProveedorDao;
import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgProveedor;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class CgProveedorDaoImpl extends JpaDao<Long, CgProveedor> implements CgProveedorDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<CgProveedor> findByName(String nombre) throws SQLException,
			Exception {
		
		List<CgProveedor> list = new ArrayList<CgProveedor>();
		
		String queryString = "" +
		"" +
		"select prov " +
		"from CgProveedor prov " +
		"where upper(prov.prestadorServicio.persona.nombre) like upper(:nameParam) " +
		"order by prov.prestadorServicio.persona.nombre asc ";

		Query query = entityManager.createQuery(queryString);
		query.setParameter("nameParam", "%"+nombre+"%");
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 
		list = (List<CgProveedor>)query.getResultList();
		
		return list;

	}

}
