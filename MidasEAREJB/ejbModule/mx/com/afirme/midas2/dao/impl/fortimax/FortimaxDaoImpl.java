package mx.com.afirme.midas2.dao.impl.fortimax;

import java.net.URL;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.commons.lang.BooleanUtils;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.fortimax.FortimaxDao;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ParametroAplicacionFortimax;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fortimax.CatalogoAplicacionFortimaxService;
import mx.com.afirme.midas2.service.fortimax.DocumentoCarpetaFortimaxService;
import mx.com.afirme.midas2.service.fortimax.ParametroAplicacionFortimaxService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.wsClient.fortimax.documentos.DocumentoServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax.documentos.DocumentoServiceSoap11BindingStub;
import mx.com.afirme.midas2.wsClient.fortimax.expedientes.ExpedienteServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax.expedientes.ExpedienteServiceSoap11BindingStub;
import mx.com.afirme.midas2.wsClient.fortimax.ligaIfimax.LinkServiceLocator;
import mx.com.afirme.midas2.wsClient.fortimax.ligaIfimax.LinkServiceSoap11BindingStub;

@Stateless
public class FortimaxDaoImpl extends EntidadDaoImpl implements FortimaxDao{
	private final static int TIME_OUT=40000;
	private SistemaContext sistemaContext;
	private CatalogoAplicacionFortimaxService aplicacionFortimaxService;
	private ParametroAplicacionFortimaxService parametroFortimaxService;
	private DocumentoCarpetaFortimaxService documentoCarpetaService;
	//TODO Solo se debe de enviar el titulo de aplicacion y los valores de los parametros, lo de mas se obtiene de los servicios.
	@Override
	public String[] generateExpedient(String tituloAplicacion, String[] fieldValues) {
		String[]respuestas = new String[4];		
		try{
			String endpoint=sistemaContext.getExpedienteFortimaxEndPointPathWSDL();
			CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
			String[] parametros=aplicacionFortimaxService.obtenerParametrosAplicacion(tituloAplicacion);
			String rootFolder=aplicacion.getNombreCarpetaRaiz();			
			ExpedienteServiceLocator expedienteServiceLocator = new ExpedienteServiceLocator();
			ExpedienteServiceSoap11BindingStub serviceExpedientes = (ExpedienteServiceSoap11BindingStub) expedienteServiceLocator.getExpedienteServiceHttpSoap11Endpoint(new URL(endpoint));
			serviceExpedientes.setTimeout(TIME_OUT);
			respuestas=serviceExpedientes.generateExpediente(Utilerias.getMensajeRecurso(SistemaPersistencia.RECURSOS_FORTIMAX, "midas.fortimax.agentes.acces"), 
					aplicacion.getNombreFortimax(), 
					rootFolder, 
					parametros, 
					fieldValues, 
					true);
			System.out.print("Respuesta generateExpedient Fortimax " + respuestas.toString());
			if (!BooleanUtils.toBoolean(respuestas[0]) && respuestas[2].equals("Error. El expediente ya existe.")) {
				respuestas[0] = "true";
			}
		}
		catch(Exception e){			
			System.out.println(e);			
		}
		return respuestas;
	}

	@Override
	public String[] generateDocument(Long id,String tituloAplicacion, String documentName,String folderName) throws Exception {		
		String endpointDocumentos = sistemaContext.getDocumentoFortimaxEndPointPathWSDL(); 
		DocumentoServiceLocator locator=new DocumentoServiceLocator();
		DocumentoServiceSoap11BindingStub service = (DocumentoServiceSoap11BindingStub)locator.getDocumentoServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		ParametroAplicacionFortimax fieldName=parametroFortimaxService.obtenerParametroLlavePorAplicacion(tituloAplicacion);
		return service.generateDocument(Utilerias.getMensajeRecurso(SistemaPersistencia.RECURSOS_FORTIMAX,"midas.fortimax.agentes.acces"), 
				   aplicacion.getNombreFortimax(), 
				   fieldName.getNombreParametro(),
				   id.toString(), 
				   Utilerias.getMensajeRecurso(SistemaPersistencia.RECURSOS_FORTIMAX, "midas.fortimax.agentes.fileType"),
				   folderName,
				   documentName);			
	}

	@Override
	public String[] generateLinkToDocument(Long id, String tituloAplicacion, String nombreDocumento)	throws Exception {
		String endpointLiga = sistemaContext.getLinkFortimaxEndPointPathWSDL(); 
		LinkServiceLocator locator=new LinkServiceLocator();
		LinkServiceSoap11BindingStub service = (LinkServiceSoap11BindingStub)locator.getLinkServiceHttpSoap11Endpoint(new URL(endpointLiga));
		service.setTimeout(TIME_OUT);
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		return service.generateLink(Utilerias.getMensajeRecurso(SistemaPersistencia.RECURSOS_FORTIMAX, "midas.fortimax.agentes.user"), 
				Utilerias.getMensajeRecurso(SistemaPersistencia.RECURSOS_FORTIMAX, "midas.fortimax.agentes.pass"), 
				false, 
				aplicacion.getNombreFortimax(),
				id.toString(), 
				nombreDocumento, 
				3600L, 
				true); 
	}

	@Override
	public String[] getDocumentFortimax(Long id,String tituloAplicacion) throws Exception {
		String endpointDocumentos = sistemaContext.getDocumentoFortimaxEndPointPathWSDL(); 
		DocumentoServiceLocator locator=new DocumentoServiceLocator();
		DocumentoServiceSoap11BindingStub service = (DocumentoServiceSoap11BindingStub)locator.getDocumentoServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		ParametroAplicacionFortimax fieldName=parametroFortimaxService.obtenerParametroLlavePorAplicacion(tituloAplicacion);
		try{
		String[]resultados=service.getDocuments(aplicacion.getNombreFortimax(),
				fieldName.getNombreParametro(), 
				id.toString());
		return resultados;
		}
		catch(Exception e){
			return new String[0];
		}
	}
	
	@Override
	public String[] uploadFile(String fileName, TransporteImpresionDTO transporteImpresionDTO, String tituloAplicacion, String[] expediente, String carpeta){
		String endpointDocumentos = sistemaContext.uploadFileFortimaxEndPointPathWSDL(); 
		DocumentoServiceLocator locator=new DocumentoServiceLocator();
		String[] respuestaWS= new String[4];
		try{
		DocumentoServiceSoap11BindingStub service = (DocumentoServiceSoap11BindingStub)locator.getDocumentoServiceHttpSoap11Endpoint(new URL(endpointDocumentos));
		service.setTimeout(TIME_OUT);		
		CatalogoAplicacionFortimax aplicacion=aplicacionFortimaxService.getAplicacionPorNombre(tituloAplicacion);
		respuestaWS=service.uploadFile(fileName, transporteImpresionDTO.getByteArray(), aplicacion.getNombreFortimax(), expediente, carpeta);
		}
		catch(Exception e){
			respuestaWS[0]="error";
		}
		return respuestaWS;
	}

	@EJB	
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	@EJB
	public void setAplicacionFortimaxService(
			CatalogoAplicacionFortimaxService aplicacionFortimaxService) {
		this.aplicacionFortimaxService = aplicacionFortimaxService;
	}
	@EJB
	public void setParametroFortimaxService(
			ParametroAplicacionFortimaxService parametroFortimaxService) {
		this.parametroFortimaxService = parametroFortimaxService;
	}
	@EJB
	public void setDocumentoCarpetaService(
			DocumentoCarpetaFortimaxService documentoCarpetaService) {
		this.documentoCarpetaService = documentoCarpetaService;
	}
	
}
