package mx.com.afirme.midas2.dao.impl.negocio.condicionespecial;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.condicionespecial.NegocioCondicionEspecialDao;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

@Stateless
public class NegocioCondicionEspecialDaoImpl extends JpaDao<Long, NegocioCondicionEspecial> implements	NegocioCondicionEspecialDao {
	
	private static final int NIVEL_APLICACION_POLIZA 								= 0;
	private static final int NIVEL_APLICACION_INCISO 								= 1;
	private static final int NIVEL_APLICACION_TODAS 								= 2;

	@Override
	public NegocioCondicionEspecial findById(Long arg0) {
		return null;
	}

	@Override
	public NegocioCondicionEspecial getReference(Long arg0) {
		return null;
	}

	@Override
	public List<CondicionEspecial> listarCondicionEspecialByIdNegocio(Long idToNegocio) {
		StringBuilder queryString = new StringBuilder(
				"SELECT model.condicionEspecial FROM NegocioCondicionEspecial model ");
		if (idToNegocio != null && idToNegocio.intValue() != 0) {
			queryString
					.append("WHERE model.negocio.idToNegocio = :idToNegocio");
		}
		TypedQuery<CondicionEspecial> query = entityManager.createQuery(
				queryString.toString(), CondicionEspecial.class);
		query.setParameter("idToNegocio", idToNegocio);
		 query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return query.getResultList();
	}
	
	@Override
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(Long idToNegocio, NivelAplicacion nivelAplicacion, Boolean obligatoriedad){
		//Se quita distinc para por error en nueva bd de cap
		StringBuilder queryString = new StringBuilder("SELECT nce  FROM NegocioCondicionEspecial nce ");
		queryString.append(" WHERE nce.negocio.idToNegocio = :idToNegocio");
		
		if( nivelAplicacion.getNivel()== NIVEL_APLICACION_TODAS){
			queryString.append(" AND (nce.condicionEspecial.nivelAplicacion = :nivelAplicacionPoliza");
			queryString.append(" OR nce.condicionEspecial.nivelAplicacion = :nivelAplicacionIncizo)");
		}else{
			queryString.append(" AND nce.condicionEspecial.nivelAplicacion = :nivelAplicacion");
		}
		
		if(obligatoriedad != null){			
			queryString.append(" AND nce.obligatoria = :obligatoriedad");
		}
		
		TypedQuery<NegocioCondicionEspecial> query = entityManager.createQuery(queryString.toString(), NegocioCondicionEspecial.class);
		query.setParameter("idToNegocio", idToNegocio.longValue());
		
		if( nivelAplicacion.getNivel()==NIVEL_APLICACION_TODAS){
			query.setParameter("nivelAplicacionPoliza", NIVEL_APLICACION_POLIZA);
			query.setParameter("nivelAplicacionIncizo", NIVEL_APLICACION_INCISO);
		}else{
			query.setParameter("nivelAplicacion", nivelAplicacion.getNivel());
		}
		
		if(obligatoriedad != null){
			query.setParameter("obligatoriedad", obligatoriedad.booleanValue() ? 1 : 0);
		}
		
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);

		return query.getResultList();

	}

	@Override
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(Long idToNegocio, NivelAplicacion nivelAplicacion) {
		return obtenerCondicionesNegocio(idToNegocio, nivelAplicacion, null);
	}

	@Override
	public List<CondicionEspecial> obtenerCondicionesEspecialesDisponibles(BigDecimal idToNegocio, BigDecimal idToSeccion) {

		return null;
	}

	@Override
	public List<CondicionEspecial> obtenerCondicionesEspecialesDisponibles(BigDecimal idToNegocio) {
		StringBuilder queryString = new StringBuilder("SELECT ce");
		queryString.append(" FROM CondicionEspecial ce");
		TypedQuery<CondicionEspecial> query = entityManager.createQuery(queryString.toString(), CondicionEspecial.class);
		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);

		List<CondicionEspecial> listaRetorno = query.getResultList();
		List<NegocioCondicionEspecial> condicionesAsociadas = this.obtenerCondicionesNegocio(idToNegocio.longValue(), NivelAplicacion.TODAS);
		
		listaRetorno.removeAll(condicionesAsociadas);
		
		return listaRetorno;
	}
	
	@Override
	public List<CondicionEspecial> buscarCondicionCodigoNombre(Long idToNegocio, String param, NivelAplicacion nivel) {

		StringBuilder queryString = new StringBuilder(
				"SELECT model from " + CondicionEspecial.class.getSimpleName() + " model ");	
		queryString.append(" WHERE UPPER(model.nombre) LIKE :nombre");

		queryString.append(" AND model.id IN (")		
		.append(" SELECT neg.condicionEspecial.id FROM " + NegocioCondicionEspecial.class.getSimpleName() + " neg ")	
		.append(" WHERE  neg.negocio.idToNegocio = :idToNegocio) ") ;
		
		if (nivel != null) {
			queryString.append(" AND model.nivelAplicacion = :nivelAplicacion ");	
		}
		
		queryString.append(" ORDER BY model.codigo DESC");
		
		TypedQuery<CondicionEspecial> query = entityManager.createQuery(queryString.toString(), CondicionEspecial.class);	
		
		query.setParameter("nombre", "%" + param.toUpperCase() + "%");
		
		query.setParameter("idToNegocio", idToNegocio);
		
		if (nivel != null) {
			query.setParameter("nivelAplicacion", nivel.getNivel());
		}

		query.setHint(QueryHints.READ_ONLY, HintValues.TRUE);

		return query.getResultList();
		
		
		
	}

}
