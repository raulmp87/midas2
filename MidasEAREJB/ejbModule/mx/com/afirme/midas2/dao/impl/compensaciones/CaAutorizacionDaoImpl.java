/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dao.compensaciones.CaAutorizacionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaAutorizacion;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless
public class CaAutorizacionDaoImpl implements CaAutorizacionDao {	
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	public static final String COMPENSACIONID = "caCompensacion.id";
	public static final String TIPOAUTOID = "caTipoAutorizacion.id";
	public static final String ESTATUSID = "caEstatus.id";	

	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOGGER = Logger.getLogger(CaAutorizacionDaoImpl.class);
	
	 public void save(CaAutorizacion entity) {
	    	LOGGER.info("	::	[INF]	::	Guardando CaAutorizacion 	::		CaAutorizacionDaoImpl	::	save	::	INICIO	::	");
		        try {
	            entityManager.persist(entity);
	            LOGGER.info("	::	[INF]	::	Se Guardo CaAutorizacion 	::		CaAutorizacionDaoImpl	::	save	::	FIN	::	");
		        } catch (RuntimeException re) {
		        	LOGGER.error("	::	[ERR]	::	Error al guardar CaAutorizacion 	::		CaAutorizacionDaoImpl	::	save	::	ERROR	::	",re);
		            throw re;
	        }
	    }

	    public void delete(CaAutorizacion entity) {
	    	LOGGER.info("	::	[INF]	::	Eliminando CaAutorizacion 	::		CaAutorizacionDaoImpl	::	delete	::	INICIO	::	");
		        try {
	        	entity = entityManager.getReference(CaAutorizacion.class, entity.getId());
	            entityManager.remove(entity);
	            LOGGER.info("	::	[INF]	::	Se Elimino CaAutorizacion 	::		CaAutorizacionDaoImpl	::	delete	::	FIN	::	");
		        } catch (RuntimeException re) {
		        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaAutorizacion 	::		CaAutorizacionDaoImpl	::	delete	::	ERROR	::	",re);
		            throw re;
	        }
	    }    
	    
	    public CaAutorizacion update(CaAutorizacion entity) {
	    	LOGGER.info("	::	[INF]	::	Actualizando CaAutorizacion 	::		CaAutorizacionDaoImpl	::	update	::	INICIO	::	");
		        try {
	            CaAutorizacion result = entityManager.merge(entity);
	            LOGGER.info("	::	[INF]	::	Se Actualizo CaAutorizacion 	::		CaAutorizacionDaoImpl	::	update	::	FIN	::	");
		            return result;
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaAutorizacion 	::		CaAutorizacionDaoImpl	::	update	::	ERROR	::	",re);
		            throw re;
	        }
	    }
	    
	    public CaAutorizacion findById( Long id) {
	    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaAutorizacionDaoImpl	::	findById	::	INICIO	::	");
		        try {
	            CaAutorizacion instance = entityManager.find(CaAutorizacion.class, id);
	            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaAutorizacionDaoImpl	::	findById	::	FIN	::	");
	            return instance;
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaAutorizacionDaoImpl	::	findById	::	ERROR	::	",re);
		            throw re;
	        }
	    }
	    
	    @SuppressWarnings("unchecked")
	    public List<CaAutorizacion> findByProperty(String propertyName, final Object value
	        ) {
	    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaAutorizacionDaoImpl	::	findByProperty	::	INICIO	::	");
				try {
				final String queryString = "select model from CaAutorizacion model where model." 
				 						+ propertyName + "= :propertyValue";
				Query query = entityManager.createQuery(queryString);
				query.setParameter("propertyValue", value);
				LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaAutorizacionDaoImpl	::	findByProperty	::	FIN	::	");
				return query.getResultList();
			} catch (RuntimeException re) {
				LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaAutorizacionDaoImpl	::	findByProperty	::	ERROR	::	",re);
				throw re;
			}
		}			
		public List<CaAutorizacion> findByValor(Object valor
		) {
			return findByProperty(VALOR, valor
			);
		}
		
		public List<CaAutorizacion> findByUsuario(Object usuario
		) {
			return findByProperty(USUARIO, usuario
			);
		}
		
		public List<CaAutorizacion> findByBorradologico(Object borradologico
		) {
			return findByProperty(BORRADOLOGICO, borradologico
			);
		}
		
		@SuppressWarnings("unchecked")
		public List<CaAutorizacion> findAll(
			) {
			LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaAutorizacionDaoImpl	::	findAll	::	INICIO	::	");
				try {
				final String queryString = "select model from CaAutorizacion model";
				Query query = entityManager.createQuery(queryString);
				LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaAutorizacionDaoImpl	::	findAll	::	FIN	::	");
				return query.getResultList();
			} catch (RuntimeException re) {
				LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaAutorizacionDaoImpl	::	findAll	::	ERROR	::	",re);
				throw re;
			}
		}
		@SuppressWarnings("unchecked")
		public List<CaAutorizacion> findByCompensacionid(Long idCompensacion){
			LOGGER.info("	::	[INF]	::	Buscando por compensacionId 	::		CaAutorizacionDaoImpl	::	findByCompensacionid	::	INICIO	::	");
			try {
			final String queryString = "select model from CaAutorizacion model where model." + COMPENSACIONID + " = :idCompensa";			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idCompensa", idCompensacion);
			LOGGER.info("	::	[INF]	::	Se busco por compensacionId 	::		CaAutorizacionDaoImpl	::	findByCompensacionid	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por compensacionId 	::		CaAutorizacionDaoImpl	::	findByCompensacionid	::	ERROR	::	",re);
			throw re;
			}
		}
		@SuppressWarnings("unchecked")
		public List<CaAutorizacion> finByCompensidAndTipoautoid(Long compensId, Long tipoAutoId){
			LOGGER.info("	::	[INF]	::	Buscando por CompensId & TipoAutorizacionId 	::		CaAutorizacionDaoImpl	::	finByCompensidAndTipoautoid	::	INICIO	::	");
			try {
			final String queryString = "select model from CaAutorizacion model where model." + COMPENSACIONID + " = :idCompensa and model." 
			+ TIPOAUTOID + " = :idTipoAuto";			
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idCompensa", compensId);
			query.setParameter("idTipoAuto", tipoAutoId);
			LOGGER.info("	::	[INF]	::	Se busco por CompensId & TipoAutorizacionId 	::		CaAutorizacionDaoImpl	::	finByCompensidAndTipoautoid	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por CompensId & TipoAutorizacionId 	::		CaAutorizacionDaoImpl	::	finByCompensidAndTipoautoid	::	ERROR	::	",re);
			throw re;
			}
		}
		public List<CaAutorizacion> finByCompensidTiposautosid(Long compensId, Long ...tipos){
			LOGGER.info("	::	[INF]	::	Buscando por CompensId & TipoAutorizacionId 	::		CaAutorizacionDaoImpl	::	finByCompensidTiposautosid	::	INICIO	::	");
			try {
			StringBuffer strQuery = new StringBuffer();
			strQuery.append("select * from midas.ca_autorizacion where tipoautorizacion_id in (");
			for (int i = 0; i < tipos.length; i++) {
				if(i != 0)
					strQuery.append(",");
				strQuery.append(tipos[i].intValue());
			}
			strQuery.append(")");
			strQuery.append(" and compensacion_id = " + compensId);
			strQuery.append(" and estatus_id = " + ConstantesCompensacionesAdicionales.ESTATUS_APROBADO);
			Query query = entityManager.createNativeQuery(strQuery.toString(), CaAutorizacion.class);
			LOGGER.info("	::	[INF]	::	Se busco por CompensId & TipoAutorizacionId 	::		CaAutorizacionDaoImpl	::	finByCompensidTiposautosid	::	FIN	::	");
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por CompensId & TipoAutorizacionId 	::		CaAutorizacionDaoImpl	::	finByCompensidTiposautosid	::	ERROR	::	",re);
			throw re;
			}
		}
}