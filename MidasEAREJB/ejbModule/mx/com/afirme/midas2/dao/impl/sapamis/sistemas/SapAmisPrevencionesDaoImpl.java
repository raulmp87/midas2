package mx.com.afirme.midas2.dao.impl.sapamis.sistemas;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas2.dao.sapamis.sistemas.SapAmisPrevencionesDao;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisEmision;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPrevenciones;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatSistemasService;

/*******************************************************************************
 * Nombre Clase: 		SapAmisPrevencionesDaoImpl.
 * 
 * Descripcion: 		Esta clase se utiliza para la ejecución de transacciones
 * 						de Base de Datos para el modulo de Prevenciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisPrevencionesDaoImpl implements SapAmisPrevencionesDao{
	private static final long serialVersionUID = 1L;
	
	@PersistenceContext private EntityManager entityManager;
	@EJB private CatSistemasService catSistemasService;
	private static final int HORAS_24 = 86400000;

	@SuppressWarnings("unchecked")
	@Override
	public List<SapAmisPrevenciones> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		StringBuilder queryString = new StringBuilder(" SELECT sas FROM SapAmisPrevenciones sas where sas.sapAmisBitacoras.catSistemas.id = " + catSistemasService.obtenerIdSistemaPorDescripcion("PREVENCION"));
		if(parametrosConsulta.getEstatus() != null){
			queryString.append(" and sas.sapAmisBitacoras.catEstatusBitacora.id = :estatus ");
		}
		if(parametrosConsulta.getFechaEnvioIni() != null && parametrosConsulta.getFechaEnvioFin() != null){
			queryString.append(" and sas.sapAmisBitacoras.fechaEnvio BETWEEN :fechaEnvioIni and :fechaEnvioFin ");		
		}
		if(parametrosConsulta.getFechaOperacionIni() != null && parametrosConsulta.getFechaOperacionFin() != null){
			queryString.append(" and sas.sapAmisBitacoras.fechaOperacion BETWEEN :fechaOperacionIni and :fechaOperacionFin ");
		}
		if(parametrosConsulta.getFechaRegistroIni() != null && parametrosConsulta.getFechaRegistroFin() != null){
			queryString.append(" and sas.sapAmisBitacoras.fechaRegistro BETWEEN :fechaRegistroIni and :fechaRegistroFin ");
		}
		if(parametrosConsulta.getModulo() != null){
			queryString.append(" and sas.sapAmisBitacoras.modulo = :modulo ");
		}
		if(parametrosConsulta.getObservaciones() != null){
			queryString.append(" and sas.sapAmisBitacoras.observaciones = :observaciones ");
		}
		if(parametrosConsulta.getOperacion() != null){
			queryString.append(" and sas.sapAmisBitacoras.catOperaciones.id = :operacion ");		
		}
		queryString.append(" ORDER BY sas.sapAmisBitacoras.fechaOperacion asc ");
		Query query = entityManager.createQuery(queryString.toString(), SapAmisEmision.class);
		if(parametrosConsulta.getEstatus() != null){
			query.setParameter("estatus", parametrosConsulta.getEstatus());
		}
		if(parametrosConsulta.getFechaEnvioIni() != null && parametrosConsulta.getFechaEnvioFin() != null){
			query.setParameter("fechaEnvioIni", parametrosConsulta.getFechaEnvioIni(), TemporalType.DATE);
			query.setParameter("fechaEnvioFin", new Date(parametrosConsulta.getFechaEnvioFin().getTime() + HORAS_24), TemporalType.DATE);
		}
		if(parametrosConsulta.getFechaOperacionIni() != null && parametrosConsulta.getFechaOperacionFin() != null){
			query.setParameter("fechaOperacionIni", parametrosConsulta.getFechaOperacionIni(), TemporalType.DATE);
			query.setParameter("fechaOperacionFin", new Date(parametrosConsulta.getFechaOperacionFin().getTime() + HORAS_24), TemporalType.DATE);
		}
		if(parametrosConsulta.getFechaRegistroIni() != null && parametrosConsulta.getFechaRegistroFin() != null){
			query.setParameter("fechaRegistroIni", parametrosConsulta.getFechaRegistroIni(), TemporalType.DATE);
			query.setParameter("fechaRegistroFin", new Date(parametrosConsulta.getFechaRegistroFin().getTime() + HORAS_24), TemporalType.DATE);
		}
		if(parametrosConsulta.getModulo() != null){
			query.setParameter("modulo", parametrosConsulta.getModulo());
		}
		if(parametrosConsulta.getObservaciones() != null){
			query.setParameter("observaciones", parametrosConsulta.getObservaciones());
		}
		if(parametrosConsulta.getOperacion() != null){
			query.setParameter("operacion", parametrosConsulta.getOperacion());
		}
		query.setMaxResults((int)numRegXPag);
		query.setFirstResult((int)((numPagina - 1) * numRegXPag));
		return query.getResultList();
	}
}