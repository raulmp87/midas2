package mx.com.afirme.midas2.dao.impl.negocio.cliente;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ClienteAsociadoJPADao;
import mx.com.afirme.midas2.domain.negocio.cliente.ClienteAsociadoJPA;

@Stateless
public class ClienteAsociadoJPADaoImpl extends JpaDao<Long, ClienteAsociadoJPA> implements ClienteAsociadoJPADao{


}
