package mx.com.afirme.midas2.dao.impl.calculos;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.sql.SQLException;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.calculos.SolicitudChequesMizarDao;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.bonos.DetalleCalculoBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.calculos.DetalleSolicitudCheque;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class SolicitudChequesMizarDaoImpl extends EntidadDaoImpl implements SolicitudChequesMizarDao {
	@SuppressWarnings("unused")
	private DataSource dataSource;
	private static final String CATALOG_NAME = "PKGCALCULOS_AGENTES";
	private static final String PACKAGE_CONTABILIZA_MOVS = "PKG_INT_MIDAS_E2";
	private SimpleJdbcCall procCalculoComisiones;
	private SimpleJdbcCall procCalculoBonos;
	private SimpleJdbcCall procContabilizarMovimientos;
	private static ValorCatalogoAgentes tipoMoneda;
	private SimpleJdbcCall procCalculoProvisiones;
	
	private ValorCatalogoAgentes getTipoMoneda(){
		if(tipoMoneda==null){
			try {
				tipoMoneda=catalogoService.obtenerElementoEspecifico("Tipo Moneda", "NACIONAL");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return tipoMoneda;
	}
	
	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
		this.procCalculoComisiones = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("MIDAS")
				.withCatalogName(CATALOG_NAME)
				.withProcedureName("stp_regCalculoComisAsync");
		this.procCalculoBonos = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("stp_generarCalculoBonos");
		this.procContabilizarMovimientos= new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName(PACKAGE_CONTABILIZA_MOVS)
		.withProcedureName("stpContabiliza_Movs");
		this.procCalculoProvisiones = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("stp_generarCalculoProvisiones");		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actSolitudChequeMizarBonos(DetalleCalculoBono cheque)
			throws MidasException {
		StoredProcedureHelper storedHelper = null;
		String sp="spinshead_md";
		Long idCalculo=null;
		try {
			LogDeMidasInterfaz.log("Entrando a SolicitarChequesMizarDaoImpl.actualizarSolitudChequeMizar..." + this, Level.INFO, null);
			CalculoBono calculo=cheque.getCalculoBono();
			idCalculo=(calculo!=null)?calculo.getId():null;
			ValorCatalogoAgentes tipoMoneda=getTipoMoneda();
			String tipoMonedaValor=(isNotNull(tipoMoneda) && isValid(tipoMoneda.getClave()))?tipoMoneda.getClave():null;
			String beneficiario=(isNotNull(cheque.getBeneficiario())?cheque.getBeneficiario():null);
			storedHelper = new StoredProcedureHelper(sp,StoredProcedureHelper.DATASOURCE_MIZAR);
			storedHelper.estableceParametro("id_sesion",idCalculo);
			storedHelper.estableceParametro("id_sol_cheque",cheque.getIdSolicitudCheque());
			storedHelper.estableceParametro("imp_pago",(cheque.getBonoPagar()!=null)?cheque.getBonoPagar():0);
			storedHelper.estableceParametro("fecha",calculo.getFechaCorte());
//			storedHelper.estableceParametro("fecha","2012-08-27");
			storedHelper.estableceParametro("moneda",tipoMonedaValor);
			storedHelper.estableceParametro("tipo_cambio",1);//Como es moneda nacional el multiplo es 1
			storedHelper.estableceParametro("nom_beneficiario",beneficiario);
//			storedHelper.estableceParametro("concepto","PAGO 1ER EXD");
			storedHelper.estableceParametro("concepto","MIDAS PAGO BONO AGTE "+cheque.getIdBeneficiario());			
			storedHelper.estableceParametro("tipo_solicitud","AGT");//Para que aparezca como agente en Mizar
			storedHelper.estableceParametro("idagente",cheque.getIdBeneficiario()); //16-06-2014 se agrega parametro idAgente
			storedHelper.ejecutaActualizarSQLServer();
			
		} catch (SQLException e) {				
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,CalculoComisionesDaoImpl.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de CalculoBonosDaoImpl.actualizarSolitudChequeMizar..." + this, Level.WARNING, e);
			onError(e);
		}catch(NullPointerException e){
			LogDeMidasInterfaz.log("Excepcion general en CalculoBonosDaoImpl.actualizarSolitudChequeMizar [solicitud de cheque:"+cheque.getIdSolicitudCheque()+" |calculo:"+idCalculo+"]..." + this, Level.WARNING, e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en CalculoBonosDaoImpl.actualizarSolitudChequeMizar..." + this, Level.WARNING, e);
			onError(e);
		}		
	}
	
	/**
	 * GENERAR PREVIEW DE CALCULO COMISIONES- TRANSACCIONAL 
	 * 
	 */
	/**
	 * Metodo que hace el calculo de pago de comisiones
	 * @param idConfiguracionComisiones
	 * @param idCalculoComisiones
	 * @param idCalculoTemporal
	 * @return
	 * @throws MidasException
	 */
	public Long iniciarCalculoComisiones(Long idConfiguracionComisiones,Long idCalculoComisiones,Long idCalculoTemporal,String queryAgentes) throws MidasException{
		Long idEjecucion=null;
		
		Usuario usuarioActual = usuarioService.getUsuarioActual();
		
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("pIdConfigComisiones", idConfiguracionComisiones)		
		.addValue("pQueryAgentes", queryAgentes)
		.addValue("pIdUsuario", usuarioActual.getNombreUsuario());
		Map<String,Object> data=procCalculoComisiones.execute(in);
		if(isNotNull(data)){
			Object id=data.get("pIdEjecucion");
			idEjecucion=(id!=null)?Long.parseLong(id.toString()):null;
		}
		return idEjecucion;
	}
	
	public Long iniciarCalculoBonos(Long pIdCalculoIn,Long pIdProgramacion,Long pIdConfigBono,Long pIdModoEjec,Date pFechaAlta,Date pFechaEjecucion,String pHora,Long pPeriodoEjecucion,Long pTipoConfiguracion,String pUsuarioAlta,Long pTipoBono,Long pClaveEstatus,Integer pActivo,Integer pEsNegocio) throws MidasException{
		Long idCalculo=null;
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("pIdCalculoIn", pIdCalculoIn)
		.addValue("pIdProgramacion", pIdProgramacion)
		.addValue("pIdConfigBono",pIdConfigBono)
		.addValue("pIdModoEjec", pIdModoEjec)
		.addValue("pFechaAlta", pFechaAlta)
		.addValue("pFechaEjecucion", pFechaEjecucion)
		.addValue("pHora", pHora)
		.addValue("pPeriodoEjecucion", pPeriodoEjecucion)
		.addValue("pTipoConfiguracion", pTipoConfiguracion)
		.addValue("pUsuarioAlta", pUsuarioAlta)
		.addValue("pTipoBono", pTipoBono)
		.addValue("pClaveEstatus", pClaveEstatus)
		.addValue("pActivo", pActivo)
		.addValue("pEsNegocio", pEsNegocio);
		System.out.println("pIdCalculoIn:"+pIdProgramacion);
		System.out.println("pIdProgramacion:"+ pIdProgramacion);
		System.out.println("pIdConfigBono:"+ pIdConfigBono);
		System.out.println("pIdModoEjec:"+ pIdModoEjec);
		System.out.println("pFechaAlta:"+ pFechaAlta);
		System.out.println("pFechaEjecucion:"+ pFechaEjecucion);
		System.out.println("pHora:"+ pHora);
		System.out.println("pPeriodoEjecucion:"+ pPeriodoEjecucion);
		System.out.println("pTipoConfiguracion:"+ pTipoConfiguracion);
		System.out.println("pUsuarioAlta:"+ pUsuarioAlta);
		System.out.println("pTipoBono:"+ pTipoBono);
		System.out.println("pClaveEstatus:"+ pClaveEstatus);
		System.out.println("pActivo:"+ pActivo);
		System.out.println("pEsNegocio:"+ pEsNegocio);

		Map<String,Object> data=procCalculoBonos.execute(in);
		if(isNotNull(data)){
			Object id=data.get("pIdCalculo");
			Object codigoRespuesta=data.get("PCOD_RESP");
			Integer idCodigo=(codigoRespuesta!=null)?Integer.parseInt(codigoRespuesta.toString()):null;
			if(idCodigo!=0 && idCodigo!=null){
				String msg=(isNotNull(data.get("PDESC_RESP")))?data.get("PDESC_RESP").toString():"";
				onError(msg);
			}
			idCalculo=(id!=null)?Long.parseLong(id.toString()):null;
		}
		return idCalculo;
	}
	
	public Long iniciarCalculoProvisiones(Long pIdCalculoIn,Long pIdProgramacion,Long pIdConfigBono,Long pIdModoEjec,Date pFechaAlta,Date pFechaEjecucion,String pHora,Long pPeriodoEjecucion,Long pTipoConfiguracion,String pUsuarioAlta,Long pTipoBono,Long pClaveEstatus,Integer pActivo,Integer pEsNegocio) throws MidasException{
		Long idCalculo=null;
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("pIdCalculoIn", pIdCalculoIn)
		.addValue("pIdProgramacion", pIdProgramacion)
		.addValue("pIdConfigBono",pIdConfigBono)
		.addValue("pIdModoEjec", pIdModoEjec)
		.addValue("pFechaAlta", pFechaAlta)
		.addValue("pFechaEjecucion", pFechaEjecucion)
		.addValue("pHora", pHora)
		.addValue("pPeriodoEjecucion", pPeriodoEjecucion)
		.addValue("pTipoConfiguracion", pTipoConfiguracion)
		.addValue("pUsuarioAlta", pUsuarioAlta)
		.addValue("pTipoBono", pTipoBono)
		.addValue("pClaveEstatus", pClaveEstatus)
		.addValue("pActivo", pActivo)
		.addValue("pEsNegocio", pEsNegocio);
		System.out.println("pIdCalculoIn:"+pIdProgramacion);
		System.out.println("pIdProgramacion:"+ pIdProgramacion);
		System.out.println("pIdConfigBono:"+ pIdConfigBono);
		System.out.println("pIdModoEjec:"+ pIdModoEjec);
		System.out.println("pFechaAlta:"+ pFechaAlta);
		System.out.println("pFechaEjecucion:"+ pFechaEjecucion);
		System.out.println("pHora:"+ pHora);
		System.out.println("pPeriodoEjecucion:"+ pPeriodoEjecucion);
		System.out.println("pTipoConfiguracion:"+ pTipoConfiguracion);
		System.out.println("pUsuarioAlta:"+ pUsuarioAlta);
		System.out.println("pTipoBono:"+ pTipoBono);
		System.out.println("pClaveEstatus:"+ pClaveEstatus);
		System.out.println("pActivo:"+ pActivo);
		System.out.println("pEsNegocio:"+ pEsNegocio);

		Map<String,Object> data=procCalculoProvisiones.execute(in);
		if(isNotNull(data)){
			Object id=data.get("pIdCalculo");
			Object codigoRespuesta=data.get("PCOD_RESP");
			Integer idCodigo=(codigoRespuesta!=null)?Integer.parseInt(codigoRespuesta.toString()):null;
			if(idCodigo!=0 && idCodigo!=null){
				String msg=(isNotNull(data.get("PDESC_RESP")))?data.get("PDESC_RESP").toString():"";
				onError(msg);
			}
			idCalculo=(id!=null)?Long.parseLong(id.toString()):null;
		}
		return idCalculo;
	}
	
	/**
	 * Metodo para contabilizar movimientos y afectar cuentas contables de Mizar.
	 * @param identificador Es el identificador para obtener el cursor CONTABLE, en caso de COMISIONES y BONOS, se envia idCalculo|idAgente, en caso del prestamo solo se envia el idPrestamo
	 * @param claveTransaccionContable dependiendo de la clave contable
	 * @throws MidasException
	 */
	public void contabilizarMovimientos(String identificador,ClavesTransaccionesContables claveTransaccionContable) throws MidasException{
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("pidentificador", identificador)
		.addValue("pCve_trans_cont", claveTransaccionContable.value);
		System.out.println("pidentificador:"+identificador);
		System.out.println("pCve_trans_cont:"+claveTransaccionContable.value);
		//log.info("Ejecutando procAutMigraEndoso.");
		LogDeMidasInterfaz.log("Entrando a SolicitudChequesMizarDaoImpl.contabilizarMovimientos..." + this, Level.INFO,null);
		Map<String,Object> data=procContabilizarMovimientos.execute(in);
		LogDeMidasInterfaz.log("Saliendo de SolicitudChequesMizarDaoImpl.contabilizarMovimientos..." + this, Level.INFO,null);
		if(isNotNull(data)){
			Object codigoRespuesta=data.get("pid_Cod_Resp");
			Integer idCodigo=(codigoRespuesta!=null)?Integer.parseInt(codigoRespuesta.toString()):null;
			if(idCodigo!=null && idCodigo!=0){
				String msg=(isNotNull(data.get("PDESC_RESP")))?data.get("PDESC_RESP").toString():"";
				onError(msg);
			}
		}
	}
		
	/**
	 * Agrega el encabezado de una solicitud de cheque en MIZAR a partir de una solicitud de cheque de SEYCOS
	 * @param solicitudCheque Solicitud de cheque de SEYCOS
	 */
	public void agregarEncabezadoSolicitudChequeMIZAR(SolicitudChequeDTO solicitudCheque) throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		String sp="spInsHead_md";
		
		try {
			
			logger.info("Agregando en MIZAR encabezado de la solicitud de cheque de SEYCOS...");
			
			logger.info("id_sesion --> " + solicitudCheque.getIdSesion());
			logger.info("id_sol_cheque --> " + solicitudCheque.getIdSolCheque());
			logger.info("imp_pago --> " + solicitudCheque.getImporte());
			logger.info("fecha --> " + solicitudCheque.getFechaMovimiento());
			logger.info("moneda --> " + solicitudCheque.getIdMoneda());
			logger.info("tipo_cambio --> " + solicitudCheque.getTipoCambio());
			logger.info("nom_beneficiario --> " + solicitudCheque.getNombreBeneficiario());
			logger.info("concepto --> " + solicitudCheque.getConceptoMov());
			logger.info("tipo_solicitud --> " + solicitudCheque.getTipoPago());
			logger.info("idagente --> " + solicitudCheque.getIdAgente());
					
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIZAR);

			storedHelper.estableceParametro("id_sesion", solicitudCheque.getIdSesion());
			storedHelper.estableceParametro("id_sol_cheque", solicitudCheque.getIdSolCheque());
			storedHelper.estableceParametro("imp_pago", solicitudCheque.getImporte());
			storedHelper.estableceParametro("fecha", solicitudCheque.getFechaMovimiento());
			storedHelper.estableceParametro("moneda", solicitudCheque.getIdMoneda());
			storedHelper.estableceParametro("tipo_cambio", solicitudCheque.getTipoCambio());
			storedHelper.estableceParametro("nom_beneficiario", solicitudCheque.getNombreBeneficiario());
			storedHelper.estableceParametro("concepto", solicitudCheque.getConceptoMov());
			storedHelper.estableceParametro("tipo_solicitud", solicitudCheque.getTipoPago());
			storedHelper.estableceParametro("idagente", solicitudCheque.getIdAgente());

			storedHelper.ejecutaActualizarSQLServer();
						
		} catch (SQLException e) {				
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
				
			}
			
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			
			throw e;
			
		}
		
	}
	
	/**
	 * Agrega en MIZAR el detalle de una solicitud de cheque de SEYCOS
	 * @param detalleSolicitudCheque Detalle de la solicitud de cheque de SEYCOS
	 * @param solicitudCheque Solicitud de cheque de SEYCOS
	 */
	public void agregarDetalleSolicitudChequeMIZAR(DetalleSolicitudCheque detalleSolicitudCheque, SolicitudChequeDTO solicitudCheque) throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		String sp="spinsDetail";
		
		try {

			logger.info("Agregando en MIZAR detalle de la solicitud de cheque de SEYCOS...");

			logger.info("id_sesion --> " + solicitudCheque.getIdSesion());
			logger.info("id_sol_cheque --> " + solicitudCheque.getIdSolCheque());
			logger.info("auxiliar --> " + solicitudCheque.getAuxiliar());
			logger.info("iva_pct --> " + solicitudCheque.getPctIVAAcreditable());
			logger.info("pct_iva_ret --> " + solicitudCheque.getPctIVARetenido());
			logger.info("pct_isr_ret --> " + solicitudCheque.getPctISRRetenido());
			logger.info("tipo_operacion --> " + StringUtils.leftPad(solicitudCheque.getClaveTipoOperacion().toString(), 2, "0"));
			logger.info("cvel_t_rubro --> " + detalleSolicitudCheque.getRubro());
			logger.info("id_cuenta_cont --> " + detalleSolicitudCheque.getIdCuentaContable());
			logger.info("cve_c_a --> " + detalleSolicitudCheque.getConcepto());
			logger.info("imp_pago --> " + detalleSolicitudCheque.getImporte());
			logger.info("cuenta_contable --> " + detalleSolicitudCheque.getCuentaContable());
						
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIZAR);

			storedHelper.estableceParametro("id_sesion", solicitudCheque.getIdSesion());
			storedHelper.estableceParametro("id_sol_cheque", solicitudCheque.getIdSolCheque());
			storedHelper.estableceParametro("auxiliar", solicitudCheque.getAuxiliar());
			storedHelper.estableceParametro("iva_pct", solicitudCheque.getPctIVAAcreditable());
			storedHelper.estableceParametro("pct_iva_ret", solicitudCheque.getPctIVARetenido());
			storedHelper.estableceParametro("pct_isr_ret", solicitudCheque.getPctISRRetenido());
			storedHelper.estableceParametro("tipo_operacion", StringUtils.leftPad(solicitudCheque.getClaveTipoOperacion().toString(), 2, "0"));
			storedHelper.estableceParametro("cvel_t_rubro", detalleSolicitudCheque.getRubro());
			storedHelper.estableceParametro("id_cuenta_cont", detalleSolicitudCheque.getIdCuentaContable());
			storedHelper.estableceParametro("cve_c_a", detalleSolicitudCheque.getConcepto());
			storedHelper.estableceParametro("imp_pago", detalleSolicitudCheque.getImporte());
			storedHelper.estableceParametro("cuenta_contable", detalleSolicitudCheque.getCuentaContable());
						
			storedHelper.ejecutaActualizarSQLServer();
						
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
				
			}
			
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			
			throw e;
			
		}
		
	}
	
	/**
	 * Procesa en MIZAR las solicitudes de cheque recien importadas desde SEYCOS
	 * @param idSesion Es el identificador de la sesión con la que están relacionadas todas las solicitudes de cheque que serán procesadas
	 */
	@Override
	public void procesarSolicitudesChequeEnMIZAR(Long idSesion) throws Exception {
		
		StoredProcedureHelper storedHelper = null;
		String sp="spinsCheck";
		
		try {
			
			logger.info("Procesando en MIZAR solicitudes de cheque importadas desde SEYCOS...");
			
			logger.info("id_sesion --> " + idSesion);
			
			storedHelper = new StoredProcedureHelper(sp, StoredProcedureHelper.DATASOURCE_MIZAR);
			
			storedHelper.estableceParametro("id_sesion", idSesion);
			
			storedHelper.ejecutaActualizarSQLServer();
			
		} catch (SQLException e) {	
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
				
			}
			
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,sp,SolicitudChequesMizarDaoImpl.class, codErr, descErr);
			
			throw e;
			
		}
		
	}
	
	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> arg0,
			Map<String, Object> arg1) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@EJB
	private ValorCatalogoAgentesService catalogoService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	private static Logger logger = Logger.getLogger(SolicitudChequesMizarDaoImpl.class);
	
}
