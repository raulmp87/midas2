/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Marzo 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.dao.impl.compensaciones;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas2.dao.compensaciones.CaFrecuenciaPagosDao;
import mx.com.afirme.midas2.domain.compensaciones.CaFrecuenciaPagos;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;



@Stateless
public class CaFrecuenciaPagosDaoImpl implements CaFrecuenciaPagosDao {
	
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String DIVISOR = "divisor";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@PersistenceContext
	private EntityManager entityManager;
	private static final Logger LOG = LoggerFactory.getLogger(CaFrecuenciaPagosDaoImpl.class);
	
	public void save(CaFrecuenciaPagos entity) {	
		try {
			LOG.info(">> save()");
			entityManager.persist(entity);
			LOG.info("<< save()");
        } catch (RuntimeException re) {     
        	LOG.error("Información del Error", re);
            throw re;
        }
	}
	
    public void delete(CaFrecuenciaPagos entity) {
    	try {
    		LOG.info(">> delete()");
    		entity = entityManager.getReference(CaFrecuenciaPagos.class, entity.getId());
    		LOG.info("<< delete()");
		    entityManager.remove(entity);
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
		    throw re;
		}
    }
    
    public CaFrecuenciaPagos update(CaFrecuenciaPagos entity) {    
    	try {
    		LOG.info(">> update()");
            CaFrecuenciaPagos result = entityManager.merge(entity);
            LOG.info("<< update()");
            return result;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        	throw re;
        }
    }
    
    public CaFrecuenciaPagos findById( Long id) {
    	CaFrecuenciaPagos instance = null;
    	try {
    		LOG.info(">> findById()");
    		instance = entityManager.find(CaFrecuenciaPagos.class, id);
    		LOG.info("<< findById()");
    	} catch (RuntimeException re) {    		
    		LOG.error("Información del Error", re);
    	}
    	return instance;
    }

    @SuppressWarnings("unchecked")
    public List<CaFrecuenciaPagos> findByProperty(String propertyName, final Object value) {
    	LOG.info(">> findByProperty()");
		try {
			StringBuilder queryString = new StringBuilder("SELECT model FROM CaFrecuenciaPagos model WHERE model.");
			queryString.append(propertyName).append(" = :propertyValue");
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("propertyValue", value);		
			LOG.info("<< findByProperty()");
			return query.getResultList();
		} catch (RuntimeException re) {		
			LOG.error("Información del Error", re);
			return null;
		}
	}			

	public List<CaFrecuenciaPagos> findAll() {		
		try {
			LOG.info(">> findAll()");
			List<CaFrecuenciaPagos> list = findByProperty(BORRADOLOGICO, ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
			LOG.info("<< findAll()");
			return list;
		} catch (RuntimeException re) {
			LOG.error("Información del Error", re);
			return null;
		}
	}
}