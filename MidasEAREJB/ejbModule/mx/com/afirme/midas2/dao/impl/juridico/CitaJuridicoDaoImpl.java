package mx.com.afirme.midas2.dao.impl.juridico;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.Query;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.juridico.CitaJuridicoDao;
import mx.com.afirme.midas2.domain.juridico.CitaJuridico;
import mx.com.afirme.midas2.service.juridico.CitaJuridicoService.CitaJuridicoFiltro;
import mx.com.afirme.midas2.util.JpaUtil;

@Stateless
public class CitaJuridicoDaoImpl extends EntidadDaoImpl implements CitaJuridicoDao {

	@SuppressWarnings("unchecked")
	@Override
	public List<CitaJuridico> filtrar(CitaJuridicoFiltro filtro) {
		
		Integer primerRegistroACargar = filtro.getPrimerRegistroACargar();
		Integer numeroMaximoRegistrosACargar = filtro.getNumeroMaximoRegistrosACargar();
		filtro.setPrimerRegistroACargar(null);
		filtro.setNumeroMaximoRegistrosACargar(null);
		
		Query query = this.getQueryCitasJuridico(filtro, Boolean.FALSE);
		
		if(primerRegistroACargar != null && numeroMaximoRegistrosACargar != null) {
			query.setFirstResult(primerRegistroACargar.intValue());
			query.setMaxResults(numeroMaximoRegistrosACargar.intValue());
		}
				
		return query.getResultList();
	}

	@Override
	public Long filtrarPaginado(CitaJuridicoFiltro filtro) {
		
		Query query = this.getQueryCitasJuridico(filtro, Boolean.TRUE);
		
		return (Long)(query.getSingleResult());
	}	
	
	private Query getQueryCitasJuridico(CitaJuridicoFiltro filtro, Boolean esConteo)
	{
		Query query = null;
		String queryFinal;
		
		Map<String, Object> params = new HashMap<String, Object>();		
		final StringBuilder queryString = new StringBuilder("");		
		JpaUtil.createSimpleFilterQuery(CitaJuridico.class, queryString, 
				filtro, params, "fechaInicio-DESC");
		
		if(esConteo)
		{
			queryFinal = queryString.toString().
				replaceFirst("SELECT e FROM", "SELECT COUNT(e) FROM");
		}
		else
		{
			queryFinal = queryString.toString();			
		}
		
		query  = entityManager.createQuery(queryFinal);
		JpaUtil.setQueryParametersByProperties(query, params);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE); 	
		
		return query;		
	}
	
	

}
