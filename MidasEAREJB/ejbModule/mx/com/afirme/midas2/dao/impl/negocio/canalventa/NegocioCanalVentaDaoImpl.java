package mx.com.afirme.midas2.dao.impl.negocio.canalventa;

import java.util.logging.Level;


import javax.ejb.Stateless;
import javax.persistence.Query;

import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;
import mx.com.afirme.midas2.dao.negocio.canalventa.NegocioCanalVentaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;

@Stateless
public class NegocioCanalVentaDaoImpl extends EntidadDaoImpl implements NegocioCanalVentaDao{
	
	/**
	 * Query que inserta en canalVenta para un negocio dado todos los campos del cotizador que hay en el catalogo
	 * y que no existan en canalventa
	 */
	@Override
	public int sincronizarCamposCatCanalVta(TIPO_CATALOGO tipo, Long idNegocio, String usuarioCreacion, String usuarioModificacion) {
		
		try {
			final String queryString = "insert into MIDAS.TONEGCANALVENTA VAUES(ID,IDTONEGOCIO,IDTCVALORFIJO,STATUS,FECHA_CREACION," +
					"FECHA_MODIFICACION,CODIGO_USUARIO_CREACION,CODIGO_USUARIO_MODIFICACION, REQUERIDO)" +        
					"select MIDAS.TONEGCANALVENTA_ID_SEQ.nextvaL,"+idNegocio+", VF.ID, 1, sysdate, sysdate, '"+usuarioCreacion+
					"', '"+usuarioModificacion+"', 0  "+
					"from MIDAS.tcvalorfijo VF left join MIDAS.TONEGCANALVENTA cvta on vf.id=cvta.IDTCVALORFIJO and idtonegocio = "+ +idNegocio+
					" where vf.grupo_id=(select id from MIDAS.tcgrupofijo gf where gf.codigo='"+tipo.name()+"') and cvta.id is null";
			Query query = entityManager.createNativeQuery(queryString);
			return query.executeUpdate();
		} catch (RuntimeException re) {
						LogUtil.log("###sincronizarCamposCatCanalVta failed", Level.SEVERE, re);
				return -1;
		}
	}
	


	@Override
	public int saveAllCamposNegocio(Boolean visible, Long idNegocio) {
		try {
			final String queryString = "update MIDAS.TONEGCANALVENTA SET MIDAS.TONEGCANALVENTA.STATUS="+(visible?1:0)+" WHERE MIDAS.TONEGCANALVENTA.IDTONEGOCIO="+idNegocio+" and idtcvalorfijo in (select id from midas.tcvalorfijo where activo =1)";
			Query query = entityManager.createNativeQuery(queryString);
			return query.executeUpdate();
		} catch (RuntimeException re) {
						LogUtil.log("###sincronizarCamposCatCanalVta failed", Level.SEVERE, re);
				return -1;
		}
	}
	
	@Override
	public int saveSectionCamposNegocio(Boolean visible, Long idNegocio, String section) {
		try {
			final String queryString = "update MIDAS.TONEGCANALVENTA SET MIDAS.TONEGCANALVENTA.STATUS="+(visible?1:0)+" WHERE MIDAS.TONEGCANALVENTA.IDTONEGOCIO="+idNegocio+
			" and MIDAS.TONEGCANALVENTA.IDTCVALORFIJO IN (SELECT ID FROM MIDAS.TCVALORFIJO WHERE MIDAS.TCVALORFIJO.CODIGO_PADRE='"+section+"' OR MIDAS.TCVALORFIJO.CODIGO='" +
					section+"') and idtcvalorfijo in (select id from midas.tcvalorfijo where activo =1)";
			Query query = entityManager.createNativeQuery(queryString);
			return query.executeUpdate();
		} catch (RuntimeException re) {
						LogUtil.log("###saveSectionCamposNegocio failed", Level.SEVERE, re);
				return -1;
		}
	}
		
}
