package mx.com.afirme.midas2.dao.negocio.bonocomision;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.dao.impl.JpaDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;

@Stateless
public class NegocioBonoComisionDaoImpl extends JpaDao<Long, NegocioBonoComision> implements NegocioBonoComisionDao {

	@Override
	public NegocioBonoComision findByNegocio(Negocio negocio) {
		final String jpql = "select m from NegocioBonoComision m where m.negocio = :negocio";
		TypedQuery<NegocioBonoComision> query = entityManager.createQuery(jpql, NegocioBonoComision.class);
		List<NegocioBonoComision> list = query.setParameter("negocio", negocio).getResultList();
		if (list.isEmpty()) {
			return null;
		}
		return list.get(0);
	}
}
