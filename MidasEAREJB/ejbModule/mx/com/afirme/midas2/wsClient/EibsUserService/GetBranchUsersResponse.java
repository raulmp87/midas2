//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.EibsUserService;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="getBranchUsersReturn" type="{http://webService.eibs.afirme.com}ArrayOf_1831153225_nillable_UserEibsVO"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "getBranchUsersReturn"
})
@XmlRootElement(name = "getBranchUsersResponse")
public class GetBranchUsersResponse {

    @XmlElement(required = true, nillable = true)
    protected ArrayOf1831153225NillableUserEibsVO getBranchUsersReturn;

    /**
     * Gets the value of the getBranchUsersReturn property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOf1831153225NillableUserEibsVO }
     *     
     */
    public ArrayOf1831153225NillableUserEibsVO getGetBranchUsersReturn() {
        return getBranchUsersReturn;
    }

    /**
     * Sets the value of the getBranchUsersReturn property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOf1831153225NillableUserEibsVO }
     *     
     */
    public void setGetBranchUsersReturn(ArrayOf1831153225NillableUserEibsVO value) {
        this.getBranchUsersReturn = value;
    }

}
