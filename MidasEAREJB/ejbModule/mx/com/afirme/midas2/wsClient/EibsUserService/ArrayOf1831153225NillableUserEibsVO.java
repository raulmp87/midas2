//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.EibsUserService;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ArrayOf_1831153225_nillable_UserEibsVO complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ArrayOf_1831153225_nillable_UserEibsVO">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="UserEibsVO" type="{http://eIbsUser.eibs.afirme.com}UserEibsVO" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ArrayOf_1831153225_nillable_UserEibsVO", propOrder = {
    "userEibsVO"
})
public class ArrayOf1831153225NillableUserEibsVO {

    @XmlElement(name = "UserEibsVO", nillable = true)
    protected List< com.afirme.eibs.services.EibsUserService.UserEibsVO> userEibsVO;

    /**
     * Gets the value of the userEibsVO property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the userEibsVO property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getUserEibsVO().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link UserEibsVO }
     * 
     * 
     */
    public List< com.afirme.eibs.services.EibsUserService.UserEibsVO> getUserEibsVO() {
        if (userEibsVO == null) {
            userEibsVO = new ArrayList< com.afirme.eibs.services.EibsUserService.UserEibsVO>();
        }
        return this.userEibsVO;
    }

}
