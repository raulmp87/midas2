package mx.com.afirme.midas2.wsClient.EibsUserService;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

public class EibsUserServiceProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private mx.com.afirme.midas2.wsClient.EibsUserService.EibsUserServiceService _service = null;
        private mx.com.afirme.midas2.wsClient.EibsUserService.EibsUserService _proxy = null;
        private Dispatch<Source> _dispatch = null;
        private boolean _useJNDIOnly = false;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new mx.com.afirme.midas2.wsClient.EibsUserService.EibsUserServiceService(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            try
            {
                InitialContext ctx = new InitialContext();
                _service = (mx.com.afirme.midas2.wsClient.EibsUserService.EibsUserServiceService)ctx.lookup("java:comp/env/service/EibsUserServiceService");
            }
            catch (NamingException e)
            {
                if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
                    System.out.println("JNDI lookup failure: javax.naming.NamingException: " + e.getMessage());
                    e.printStackTrace(System.out);
                }
            }

            if (_service == null && !_useJNDIOnly)
                _service = new mx.com.afirme.midas2.wsClient.EibsUserService.EibsUserServiceService();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getEibsUserService();
        }

        public mx.com.afirme.midas2.wsClient.EibsUserService.EibsUserService getProxy() {
            return _proxy;
        }

        public void useJNDIOnly(boolean useJNDIOnly) {
            _useJNDIOnly = useJNDIOnly;
            init();
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://webService.eibs.afirme.com", "EibsUserService");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

        public void setMTOMEnabled(boolean enable) {
            SOAPBinding binding = (SOAPBinding) ((BindingProvider) _proxy).getBinding();
            binding.setMTOMEnabled(enable);
        }
    }

    public EibsUserServiceProxy() {
        _descriptor = new Descriptor();
        _descriptor.setMTOMEnabled(true);
    }

    public EibsUserServiceProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
        _descriptor.setMTOMEnabled(true);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public ArrayOf1831153225NillableUserEibsVO getBranchUsers(Long branchNumber) {
        return _getDescriptor().getProxy().getBranchUsers(branchNumber);
    }

    public ArrayOf1831153225NillableUserEibsVO getAccountExecutive(Long branchNumber, String accountExecutiveId, String idConcatenated) {
        return _getDescriptor().getProxy().getAccountExecutive(branchNumber,accountExecutiveId,idConcatenated);
    }

    public ArrayOf1831153225NillableUserEibsVO getAccountExecutiveByLoggedUser(String loggedUser, String accountExecutiveId, String idConcatenated) {
        return _getDescriptor().getProxy().getAccountExecutiveByLoggedUser(loggedUser,accountExecutiveId,idConcatenated);
    }

    public UserEibsVO getBranchUser(String eIbsUserName) {
        return _getDescriptor().getProxy().getBranchUser(eIbsUserName);
    }

}