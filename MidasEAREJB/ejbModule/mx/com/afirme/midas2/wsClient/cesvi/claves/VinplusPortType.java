/**
 * VinplusPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.cesvi.claves;

public interface VinplusPortType extends java.rmi.Remote {
    public java.lang.String clavesAfirme(java.lang.String clave_afirme, java.lang.String marca, java.lang.String modelo, java.lang.String version, java.lang.String ano, java.lang.String descripcion) throws java.rmi.RemoteException;
}
