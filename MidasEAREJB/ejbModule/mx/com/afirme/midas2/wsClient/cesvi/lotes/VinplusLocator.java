/**
 * VinplusLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.cesvi.lotes;

public class VinplusLocator extends org.apache.axis.client.Service implements mx.com.afirme.midas2.wsClient.cesvi.lotes.Vinplus {

    public VinplusLocator() {
    }


    public VinplusLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public VinplusLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for vinplusPort
    private java.lang.String vinplusPort_address = "http://172.30.4.172:6036/vinplus/ws_lotes/ws_lotes.php";

    public java.lang.String getvinplusPortAddress() {
        return vinplusPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String vinplusPortWSDDServiceName = "vinplusPort";

    public java.lang.String getvinplusPortWSDDServiceName() {
        return vinplusPortWSDDServiceName;
    }

    public void setvinplusPortWSDDServiceName(java.lang.String name) {
        vinplusPortWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusPortType getvinplusPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(vinplusPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getvinplusPort(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusPortType getvinplusPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusBindingStub _stub = new mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusBindingStub(portAddress, this);
            _stub.setPortName(getvinplusPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setvinplusPortEndpointAddress(java.lang.String address) {
        vinplusPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusPortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusBindingStub _stub = new mx.com.afirme.midas2.wsClient.cesvi.lotes.VinplusBindingStub(new java.net.URL(vinplusPort_address), this);
                _stub.setPortName(getvinplusPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("vinplusPort".equals(inputPortName)) {
            return getvinplusPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("urn:vinplus", "vinplus");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("urn:vinplus", "vinplusPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("vinplusPort".equals(portName)) {
            setvinplusPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
