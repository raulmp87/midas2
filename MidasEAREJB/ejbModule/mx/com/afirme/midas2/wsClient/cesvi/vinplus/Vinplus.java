/**
 * Vinplus.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.cesvi.vinplus;

public interface Vinplus extends javax.xml.rpc.Service {
    public java.lang.String getvinplusPortAddress();

    public mx.com.afirme.midas2.wsClient.cesvi.vinplus.VinplusPortType getvinplusPort() throws javax.xml.rpc.ServiceException;

    public mx.com.afirme.midas2.wsClient.cesvi.vinplus.VinplusPortType getvinplusPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
