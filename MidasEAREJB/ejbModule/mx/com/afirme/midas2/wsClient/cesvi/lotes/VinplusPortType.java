/**
 * VinplusPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.cesvi.lotes;

public interface VinplusPortType extends java.rmi.Remote {
    public java.lang.String consultaLote(java.lang.String nombreLote) throws java.rmi.RemoteException;
    public java.lang.String consultaVin(java.lang.String nombreLote, byte[] archivoTxt) throws java.rmi.RemoteException;
    public byte[] decargaLote(java.lang.String nombreLote) throws java.rmi.RemoteException;
}
