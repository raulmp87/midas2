/**
 * VinplusPortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.cesvi.vinplus;

public interface VinplusPortType extends java.rmi.Remote {
    public byte[] consultaVin(java.lang.String vin) throws java.rmi.RemoteException;
}
