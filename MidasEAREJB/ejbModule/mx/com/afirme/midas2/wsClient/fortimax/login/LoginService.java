/**
 * LoginService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.login;
public interface LoginService extends javax.xml.rpc.Service {
    public java.lang.String getLoginServiceHttpSoap11EndpointAddress();

    public mx.com.afirme.midas2.wsClient.fortimax.login.LoginServicePortType getLoginServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public mx.com.afirme.midas2.wsClient.fortimax.login.LoginServicePortType getLoginServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getLoginServiceHttpSoap12EndpointAddress();

    public mx.com.afirme.midas2.wsClient.fortimax.login.LoginServicePortType getLoginServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException;

    public mx.com.afirme.midas2.wsClient.fortimax.login.LoginServicePortType getLoginServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
