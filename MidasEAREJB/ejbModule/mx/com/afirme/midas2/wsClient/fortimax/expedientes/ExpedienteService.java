/**
 * ExpedienteService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.expedientes;

import java.net.URL;
import javax.xml.rpc.Service;
import javax.xml.rpc.ServiceException;

public interface ExpedienteService extends Service {
    public java.lang.String getExpedienteServiceHttpSoap11EndpointAddress();

    public ExpedienteServicePortType getExpedienteServiceHttpSoap11Endpoint() throws ServiceException;

    public ExpedienteServicePortType getExpedienteServiceHttpSoap11Endpoint(URL portAddress) throws ServiceException;
}
