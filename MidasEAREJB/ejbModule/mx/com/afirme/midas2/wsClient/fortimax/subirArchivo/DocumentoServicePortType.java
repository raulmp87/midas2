/**
 * DocumentoServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.subirArchivo;

import java.rmi.Remote;

public interface DocumentoServicePortType extends Remote {
    public String[] generateDocument(String access, String titulo_aplicacion, String fieldName, String fieldValue, String fileType, String folderName, String documentName) throws java.rmi.RemoteException;
    public String[] getDocuments(String titulo_aplicacion, String campo, String valor) throws java.rmi.RemoteException;
    public String[] uploadFile(String fileName, byte[] fileData, String gaveta, String[] expediente, String carpeta) throws java.rmi.RemoteException;
}
