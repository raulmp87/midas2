/**
 * ExpedienteServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.expedientes;

import java.net.MalformedURLException;
import java.net.URL;
import java.rmi.Remote;
import java.util.HashSet;
import java.util.Iterator;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;

import org.apache.axis.EngineConfiguration;
import org.apache.axis.client.Service;
import org.apache.axis.client.Stub;

public class ExpedienteServiceLocator extends Service implements ExpedienteService {

    /**
	 * 
	 */
	private static final long serialVersionUID = 2690094566793986604L;

	public ExpedienteServiceLocator() {
    }


    public ExpedienteServiceLocator(EngineConfiguration config) {
        super(config);
    }

    public ExpedienteServiceLocator(String wsdlLoc, QName sName) throws ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ExpedienteServiceHttpSoap11Endpoint
    private String ExpedienteServiceHttpSoap11Endpoint_address = "http://172.20.1.202:10082/Fortimax/services/ExpedienteService.ExpedienteServiceHttpSoap11Endpoint/";

    public String getExpedienteServiceHttpSoap11EndpointAddress() {
        return ExpedienteServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private String ExpedienteServiceHttpSoap11EndpointWSDDServiceName = "ExpedienteServiceHttpSoap11Endpoint";

    public String getExpedienteServiceHttpSoap11EndpointWSDDServiceName() {
        return ExpedienteServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setExpedienteServiceHttpSoap11EndpointWSDDServiceName(String name) {
        ExpedienteServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public ExpedienteServicePortType getExpedienteServiceHttpSoap11Endpoint() throws ServiceException {
       URL endpoint;
        try {
            endpoint = new URL(ExpedienteServiceHttpSoap11Endpoint_address);
        }
        catch (MalformedURLException e) {
            throw new ServiceException(e);
        }
        return getExpedienteServiceHttpSoap11Endpoint(endpoint);
    }

    public ExpedienteServicePortType getExpedienteServiceHttpSoap11Endpoint(URL portAddress) throws ServiceException {
        try {
            ExpedienteServiceSoap11BindingStub _stub = new ExpedienteServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getExpedienteServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setExpedienteServiceHttpSoap11EndpointEndpointAddress(String address) {
        ExpedienteServiceHttpSoap11Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public Remote getPort(Class serviceEndpointInterface) throws ServiceException {
        try {
            if (ExpedienteServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                ExpedienteServiceSoap11BindingStub _stub = new ExpedienteServiceSoap11BindingStub(new URL(ExpedienteServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getExpedienteServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new ServiceException(t);
        }
        throw new ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(QName portName, Class serviceEndpointInterface) throws ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        String inputPortName = portName.getLocalPart();
        if ("ExpedienteServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getExpedienteServiceHttpSoap11Endpoint();
        }
        else  {
            Remote _stub = getPort(serviceEndpointInterface);
            ((Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public QName getServiceName() {
        return new QName("http://expedienteService.websevices.fortimax.syc.com", "ExpedienteService");
    }

    private HashSet ports = null;

    public Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new QName("http://expedienteService.websevices.fortimax.syc.com", "ExpedienteServiceHttpSoap11Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(String portName, String address) throws ServiceException {
        
if ("ExpedienteServiceHttpSoap11Endpoint".equals(portName)) {
            setExpedienteServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(QName portName, String address) throws ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
