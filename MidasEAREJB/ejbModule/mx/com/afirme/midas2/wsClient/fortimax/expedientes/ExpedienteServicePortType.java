/**
 * ExpedienteServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.expedientes;

import java.rmi.Remote;
import java.rmi.RemoteException;

public interface ExpedienteServicePortType extends Remote {
    public String[] cloneExpedient(String access, String drawerOriginal, String[] fieldsOriginal, String[] valuesOriginal, String drawerCopy, String[] fieldsCopy, String[] valuesCopy, Boolean totalClone) throws RemoteException;
    public String[] generateExpediente(String access, String titulo_aplicacion, String carpetaRaiz, String[] fieldName, String[] fieldValue, Boolean createEstructura) throws RemoteException;
}
