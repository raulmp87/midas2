/**
 * LinkService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.ligaIfimax;

public interface LinkService extends javax.xml.rpc.Service {
    public java.lang.String getLinkServiceHttpSoap11EndpointAddress();

    public LinkServicePortType getLinkServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public LinkServicePortType getLinkServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
