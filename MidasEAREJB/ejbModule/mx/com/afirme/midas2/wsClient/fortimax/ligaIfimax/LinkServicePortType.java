/**
 * LinkServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.ligaIfimax;

import java.rmi.RemoteException;

public interface LinkServicePortType extends java.rmi.Remote {
    public java.lang.String[] generateLink(String user, String pass, Boolean encrypted, String gaveta, String expediente, String documento, Long segundosVigencia, Boolean lecturaEscritura) throws RemoteException;
}
