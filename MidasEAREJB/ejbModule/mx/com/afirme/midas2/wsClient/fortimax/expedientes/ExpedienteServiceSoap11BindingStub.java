/**
 * ExpedienteServiceSoap11BindingStub.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.expedientes;

import java.net.URL;
import java.rmi.RemoteException;
import java.util.Enumeration;

import javax.xml.namespace.QName;
import javax.xml.rpc.Service;

import org.apache.axis.AxisEngine;
import org.apache.axis.AxisFault;
import org.apache.axis.NoEndPointException;
import org.apache.axis.client.Call;
import org.apache.axis.client.Stub;
import org.apache.axis.constants.Style;
import org.apache.axis.constants.Use;
import org.apache.axis.description.OperationDesc;
import org.apache.axis.description.ParameterDesc;
import org.apache.axis.soap.SOAPConstants;
import org.apache.axis.utils.JavaUtils;

public class ExpedienteServiceSoap11BindingStub extends Stub implements ExpedienteServicePortType {
    private java.util.Vector cachedSerClasses = new java.util.Vector();
    private java.util.Vector cachedSerQNames = new java.util.Vector();
    private java.util.Vector cachedSerFactories = new java.util.Vector();
    private java.util.Vector cachedDeserFactories = new java.util.Vector();

    static OperationDesc [] _operations;

    static {
        _operations = new OperationDesc[2];
        _initOperationDesc1();
    }

    private static void _initOperationDesc1(){
        OperationDesc oper;
        ParameterDesc param;
        oper = new OperationDesc();
        oper.setName("cloneExpedient");
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "access"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "drawerOriginal"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "fieldsOriginal"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "valuesOriginal"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new org.apache.axis.description.ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "drawerCopy"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "fieldsCopy"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "valuesCopy"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "totalClone"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "boolean"), Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(String[].class);
        oper.setReturnQName(new QName("http://expedienteService.websevices.fortimax.syc.com", "return"));
        oper.setStyle(Style.WRAPPED);
        oper.setUse(Use.LITERAL);
        _operations[0] = oper;

        oper = new OperationDesc();
        oper.setName("generateExpediente");
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "access"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), java.lang.String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "titulo_aplicacion"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "carpetaRaiz"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String.class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "fieldName"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "fieldValue"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "string"), String[].class, false, false);
        param.setOmittable(true);
        param.setNillable(true);
        oper.addParameter(param);
        param = new ParameterDesc(new QName("http://expedienteService.websevices.fortimax.syc.com", "createEstructura"), ParameterDesc.IN, new QName("http://www.w3.org/2001/XMLSchema", "boolean"), Boolean.class, false, false);
        param.setOmittable(true);
        oper.addParameter(param);
        oper.setReturnType(new QName("http://www.w3.org/2001/XMLSchema", "string"));
        oper.setReturnClass(String[].class);
        oper.setReturnQName(new QName("http://expedienteService.websevices.fortimax.syc.com", "return"));
        oper.setStyle(Style.WRAPPED);
        oper.setUse(Use.LITERAL);
        _operations[1] = oper;

    }

    public ExpedienteServiceSoap11BindingStub() throws AxisFault {
         this(null);
    }

    public ExpedienteServiceSoap11BindingStub(URL endpointURL, Service service) throws AxisFault {
         this(service);
         super.cachedEndpoint = endpointURL;
    }

    public ExpedienteServiceSoap11BindingStub(Service service) throws AxisFault {
        if (service == null) {
            super.service = new org.apache.axis.client.Service();
        } else {
            super.service = service;
        }
        ((org.apache.axis.client.Service)super.service).setTypeMappingVersion("1.2");
    }

    protected Call createCall() throws RemoteException {
        try {
            Call _call = super._createCall();
            if (super.maintainSessionSet) {
                _call.setMaintainSession(super.maintainSession);
            }
            if (super.cachedUsername != null) {
                _call.setUsername(super.cachedUsername);
            }
            if (super.cachedPassword != null) {
                _call.setPassword(super.cachedPassword);
            }
            if (super.cachedEndpoint != null) {
                _call.setTargetEndpointAddress(super.cachedEndpoint);
            }
            if (super.cachedTimeout != null) {
                _call.setTimeout(super.cachedTimeout);
            }
            if (super.cachedPortName != null) {
                _call.setPortName(super.cachedPortName);
            }
            Enumeration keys = super.cachedProperties.keys();
            while (keys.hasMoreElements()) {
                java.lang.String key = (java.lang.String) keys.nextElement();
                _call.setProperty(key, super.cachedProperties.get(key));
            }
            return _call;
        }
        catch (java.lang.Throwable _t) {
            throw new AxisFault("Failure trying to get the Call object", _t);
        }
    }

    public String[] cloneExpedient(String access, String drawerOriginal, String[] fieldsOriginal, String[] valuesOriginal, String drawerCopy, String[] fieldsCopy, String[] valuesCopy, Boolean totalClone) throws RemoteException {
        if (super.cachedEndpoint == null) {
            throw new NoEndPointException();
        }
        Call _call = createCall();
        _call.setOperation(_operations[0]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:cloneExpedient");
        _call.setEncodingStyle(null);
        _call.setProperty(Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new QName("http://expedienteService.websevices.fortimax.syc.com", "cloneExpedient"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {access, drawerOriginal, fieldsOriginal, valuesOriginal, drawerCopy, fieldsCopy, valuesCopy, totalClone});

        if (_resp instanceof RemoteException) {
            throw (RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (String[]) _resp;
            } catch (Exception _exception) {
                return (String[]) JavaUtils.convert(_resp, String[].class);
            }
        }
  } catch (AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

    public String[] generateExpediente(String access, String titulo_aplicacion, String carpetaRaiz, String[] fieldName, String[] fieldValue, Boolean createEstructura) throws RemoteException {
        if (super.cachedEndpoint == null) {
            throw new NoEndPointException();
        }
        Call _call = createCall();
        _call.setOperation(_operations[1]);
        _call.setUseSOAPAction(true);
        _call.setSOAPActionURI("urn:generateExpediente");
        _call.setEncodingStyle(null);
        _call.setProperty(Call.SEND_TYPE_ATTR, Boolean.FALSE);
        _call.setProperty(AxisEngine.PROP_DOMULTIREFS, Boolean.FALSE);
        _call.setSOAPVersion(SOAPConstants.SOAP11_CONSTANTS);
        _call.setOperationName(new QName("http://expedienteService.websevices.fortimax.syc.com", "generateExpediente"));

        setRequestHeaders(_call);
        setAttachments(_call);
 try {        Object _resp = _call.invoke(new Object[] {access, titulo_aplicacion, carpetaRaiz, fieldName, fieldValue, createEstructura});

        if (_resp instanceof RemoteException) {
            throw (RemoteException)_resp;
        }
        else {
            extractAttachments(_call);
            try {
                return (String[]) _resp;
            } catch (Exception _exception) {
                return (String[]) JavaUtils.convert(_resp, String[].class);
            }
        }
  } catch (AxisFault axisFaultException) {
  throw axisFaultException;
}
    }

}
