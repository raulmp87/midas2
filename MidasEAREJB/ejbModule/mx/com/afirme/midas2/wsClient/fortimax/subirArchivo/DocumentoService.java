/**
 * DocumentoService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.subirArchivo;

public interface DocumentoService extends javax.xml.rpc.Service {
    public java.lang.String getDocumentoServiceHttpSoap11EndpointAddress();

    public DocumentoServicePortType getDocumentoServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public DocumentoServicePortType getDocumentoServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
