/**
 * LinkServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax.ligaIfimax;

public class LinkServiceLocator extends org.apache.axis.client.Service implements LinkService {

    /**
	 * 
	 */
	private static final long serialVersionUID = -7495682680669226288L;

	public LinkServiceLocator() {
    }


    public LinkServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LinkServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LinkServiceHttpSoap11Endpoint
    private String LinkServiceHttpSoap11Endpoint_address = "http://172.20.1.202:10082/Fortimax/services/LinkService.LinkServiceHttpSoap11Endpoint/";

    public String getLinkServiceHttpSoap11EndpointAddress() {
        return LinkServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LinkServiceHttpSoap11EndpointWSDDServiceName = "LinkServiceHttpSoap11Endpoint";

    public java.lang.String getLinkServiceHttpSoap11EndpointWSDDServiceName() {
        return LinkServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setLinkServiceHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        LinkServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public LinkServicePortType getLinkServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LinkServiceHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLinkServiceHttpSoap11Endpoint(endpoint);
    }

    public LinkServicePortType getLinkServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            LinkServiceSoap11BindingStub _stub = new LinkServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getLinkServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLinkServiceHttpSoap11EndpointEndpointAddress(String address) {
        LinkServiceHttpSoap11Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (LinkServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                LinkServiceSoap11BindingStub _stub = new LinkServiceSoap11BindingStub(new java.net.URL(LinkServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getLinkServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LinkServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getLinkServiceHttpSoap11Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://linkService.websevices.fortimax.syc.com", "LinkService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://linkService.websevices.fortimax.syc.com", "LinkServiceHttpSoap11Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LinkServiceHttpSoap11Endpoint".equals(portName)) {
            setLinkServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
