//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-03/09/2011 06:46 PM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.tracking;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TrackingData complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TrackingData">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="waybill" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="shortWaybillId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceDescriptionSPA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="serviceDescriptionENG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="customerNumber" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="packageType" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="additionalInformation" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statusSPA" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="statusENG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="pickupData" type="{http://www.estafeta.com/}PickupData" minOccurs="0"/>
 *         &lt;element name="deliveryData" type="{http://www.estafeta.com/}DeliveryData" minOccurs="0"/>
 *         &lt;element name="dimensions" type="{http://www.estafeta.com/}Dimensions" minOccurs="0"/>
 *         &lt;element name="waybillReplaceData" type="{http://www.estafeta.com/}WaybillReplaceData" minOccurs="0"/>
 *         &lt;element name="returnDocumentData" type="{http://www.estafeta.com/}ReturnDocumentData" minOccurs="0"/>
 *         &lt;element name="multipleServiceData" type="{http://www.estafeta.com/}MultipleServiceData" minOccurs="0"/>
 *         &lt;element name="internationalData" type="{http://www.estafeta.com/}InternationalData" minOccurs="0"/>
 *         &lt;element name="customerInfo" type="{http://www.estafeta.com/}CustomerInfo" minOccurs="0"/>
 *         &lt;element name="history" type="{http://www.estafeta.com/}ArrayOfHistory" minOccurs="0"/>
 *         &lt;element name="signature" type="{http://www.w3.org/2001/XMLSchema}base64Binary" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TrackingData", propOrder = {
    "waybill",
    "shortWaybillId",
    "serviceId",
    "serviceDescriptionSPA",
    "serviceDescriptionENG",
    "customerNumber",
    "packageType",
    "additionalInformation",
    "statusSPA",
    "statusENG",
    "pickupData",
    "deliveryData",
    "dimensions",
    "waybillReplaceData",
    "returnDocumentData",
    "multipleServiceData",
    "internationalData",
    "customerInfo",
    "history",
    "signature"
})
public class TrackingData {

    protected String waybill;
    protected String shortWaybillId;
    protected String serviceId;
    protected String serviceDescriptionSPA;
    protected String serviceDescriptionENG;
    protected String customerNumber;
    protected String packageType;
    protected String additionalInformation;
    protected String statusSPA;
    protected String statusENG;
    protected PickupData pickupData;
    protected DeliveryData deliveryData;
    protected Dimensions dimensions;
    protected WaybillReplaceData waybillReplaceData;
    protected ReturnDocumentData returnDocumentData;
    protected MultipleServiceData multipleServiceData;
    protected InternationalData internationalData;
    protected CustomerInfo customerInfo;
    protected ArrayOfHistory history;
    protected byte[] signature;

    /**
     * Gets the value of the waybill property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getWaybill() {
        return waybill;
    }

    /**
     * Sets the value of the waybill property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setWaybill(String value) {
        this.waybill = value;
    }

    /**
     * Gets the value of the shortWaybillId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getShortWaybillId() {
        return shortWaybillId;
    }

    /**
     * Sets the value of the shortWaybillId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setShortWaybillId(String value) {
        this.shortWaybillId = value;
    }

    /**
     * Gets the value of the serviceId property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceId() {
        return serviceId;
    }

    /**
     * Sets the value of the serviceId property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceId(String value) {
        this.serviceId = value;
    }

    /**
     * Gets the value of the serviceDescriptionSPA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDescriptionSPA() {
        return serviceDescriptionSPA;
    }

    /**
     * Sets the value of the serviceDescriptionSPA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDescriptionSPA(String value) {
        this.serviceDescriptionSPA = value;
    }

    /**
     * Gets the value of the serviceDescriptionENG property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getServiceDescriptionENG() {
        return serviceDescriptionENG;
    }

    /**
     * Sets the value of the serviceDescriptionENG property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setServiceDescriptionENG(String value) {
        this.serviceDescriptionENG = value;
    }

    /**
     * Gets the value of the customerNumber property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCustomerNumber() {
        return customerNumber;
    }

    /**
     * Sets the value of the customerNumber property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCustomerNumber(String value) {
        this.customerNumber = value;
    }

    /**
     * Gets the value of the packageType property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPackageType() {
        return packageType;
    }

    /**
     * Sets the value of the packageType property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPackageType(String value) {
        this.packageType = value;
    }

    /**
     * Gets the value of the additionalInformation property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAdditionalInformation() {
        return additionalInformation;
    }

    /**
     * Sets the value of the additionalInformation property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAdditionalInformation(String value) {
        this.additionalInformation = value;
    }

    /**
     * Gets the value of the statusSPA property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusSPA() {
        return statusSPA;
    }

    /**
     * Sets the value of the statusSPA property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusSPA(String value) {
        this.statusSPA = value;
    }

    /**
     * Gets the value of the statusENG property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getStatusENG() {
        return statusENG;
    }

    /**
     * Sets the value of the statusENG property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setStatusENG(String value) {
        this.statusENG = value;
    }

    /**
     * Gets the value of the pickupData property.
     * 
     * @return
     *     possible object is
     *     {@link PickupData }
     *     
     */
    public PickupData getPickupData() {
        return pickupData;
    }

    /**
     * Sets the value of the pickupData property.
     * 
     * @param value
     *     allowed object is
     *     {@link PickupData }
     *     
     */
    public void setPickupData(PickupData value) {
        this.pickupData = value;
    }

    /**
     * Gets the value of the deliveryData property.
     * 
     * @return
     *     possible object is
     *     {@link DeliveryData }
     *     
     */
    public DeliveryData getDeliveryData() {
        return deliveryData;
    }

    /**
     * Sets the value of the deliveryData property.
     * 
     * @param value
     *     allowed object is
     *     {@link DeliveryData }
     *     
     */
    public void setDeliveryData(DeliveryData value) {
        this.deliveryData = value;
    }

    /**
     * Gets the value of the dimensions property.
     * 
     * @return
     *     possible object is
     *     {@link Dimensions }
     *     
     */
    public Dimensions getDimensions() {
        return dimensions;
    }

    /**
     * Sets the value of the dimensions property.
     * 
     * @param value
     *     allowed object is
     *     {@link Dimensions }
     *     
     */
    public void setDimensions(Dimensions value) {
        this.dimensions = value;
    }

    /**
     * Gets the value of the waybillReplaceData property.
     * 
     * @return
     *     possible object is
     *     {@link WaybillReplaceData }
     *     
     */
    public WaybillReplaceData getWaybillReplaceData() {
        return waybillReplaceData;
    }

    /**
     * Sets the value of the waybillReplaceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link WaybillReplaceData }
     *     
     */
    public void setWaybillReplaceData(WaybillReplaceData value) {
        this.waybillReplaceData = value;
    }

    /**
     * Gets the value of the returnDocumentData property.
     * 
     * @return
     *     possible object is
     *     {@link ReturnDocumentData }
     *     
     */
    public ReturnDocumentData getReturnDocumentData() {
        return returnDocumentData;
    }

    /**
     * Sets the value of the returnDocumentData property.
     * 
     * @param value
     *     allowed object is
     *     {@link ReturnDocumentData }
     *     
     */
    public void setReturnDocumentData(ReturnDocumentData value) {
        this.returnDocumentData = value;
    }

    /**
     * Gets the value of the multipleServiceData property.
     * 
     * @return
     *     possible object is
     *     {@link MultipleServiceData }
     *     
     */
    public MultipleServiceData getMultipleServiceData() {
        return multipleServiceData;
    }

    /**
     * Sets the value of the multipleServiceData property.
     * 
     * @param value
     *     allowed object is
     *     {@link MultipleServiceData }
     *     
     */
    public void setMultipleServiceData(MultipleServiceData value) {
        this.multipleServiceData = value;
    }

    /**
     * Gets the value of the internationalData property.
     * 
     * @return
     *     possible object is
     *     {@link InternationalData }
     *     
     */
    public InternationalData getInternationalData() {
        return internationalData;
    }

    /**
     * Sets the value of the internationalData property.
     * 
     * @param value
     *     allowed object is
     *     {@link InternationalData }
     *     
     */
    public void setInternationalData(InternationalData value) {
        this.internationalData = value;
    }

    /**
     * Gets the value of the customerInfo property.
     * 
     * @return
     *     possible object is
     *     {@link CustomerInfo }
     *     
     */
    public CustomerInfo getCustomerInfo() {
        return customerInfo;
    }

    /**
     * Sets the value of the customerInfo property.
     * 
     * @param value
     *     allowed object is
     *     {@link CustomerInfo }
     *     
     */
    public void setCustomerInfo(CustomerInfo value) {
        this.customerInfo = value;
    }

    /**
     * Gets the value of the history property.
     * 
     * @return
     *     possible object is
     *     {@link ArrayOfHistory }
     *     
     */
    public ArrayOfHistory getHistory() {
        return history;
    }

    /**
     * Sets the value of the history property.
     * 
     * @param value
     *     allowed object is
     *     {@link ArrayOfHistory }
     *     
     */
    public void setHistory(ArrayOfHistory value) {
        this.history = value;
    }

    /**
     * Gets the value of the signature property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getSignature() {
        return signature;
    }

    /**
     * Sets the value of the signature property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setSignature(byte[] value) {
        this.signature = ((byte[]) value);
    }

}
