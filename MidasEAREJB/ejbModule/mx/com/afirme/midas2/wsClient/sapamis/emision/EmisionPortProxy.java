package mx.com.afirme.midas2.wsClient.sapamis.emision;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;

public class EmisionPortProxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private mx.com.afirme.midas2.wsClient.sapamis.emision.Emision _service = null;
        private mx.com.afirme.midas2.wsClient.sapamis.emision.EmisionPortType _proxy = null;
        private Dispatch<Source> _dispatch = null;
        private boolean _useJNDIOnly = false;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new mx.com.afirme.midas2.wsClient.sapamis.emision.Emision(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            try
            {
                InitialContext ctx = new InitialContext();
                _service = (mx.com.afirme.midas2.wsClient.sapamis.emision.Emision)ctx.lookup("java:comp/env/service/Emision");
            }
            catch (NamingException e)
            {
                if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
                    System.out.println("JNDI lookup failure: javax.naming.NamingException: " + e.getMessage());
                    e.printStackTrace(System.out);
                }
            }

            if (_service == null && !_useJNDIOnly)
                _service = new mx.com.afirme.midas2.wsClient.sapamis.emision.Emision();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getEmisionPort();
        }

        public mx.com.afirme.midas2.wsClient.sapamis.emision.EmisionPortType getProxy() {
            return _proxy;
        }

        public void useJNDIOnly(boolean useJNDIOnly) {
            _useJNDIOnly = useJNDIOnly;
            init();
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://ws.sap/", "EmisionPort");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

        public void setMTOMEnabled(boolean enable) {
            SOAPBinding binding = (SOAPBinding) ((BindingProvider) _proxy).getBinding();
            binding.setMTOMEnabled(enable);
        }
    }

    public EmisionPortProxy() {
        _descriptor = new Descriptor();
        _descriptor.setMTOMEnabled(true);
    }

    public EmisionPortProxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
        _descriptor.setMTOMEnabled(true);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public String alta(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5, String arg6, int arg7, int arg8, int arg9, int arg10, int arg11, int arg12, String arg13, String arg14, String arg15, String arg16, String arg17, String arg18, String arg19, String arg20, int arg21, String arg22, String arg23, String arg24, String arg25, String arg26, int arg27, String arg28, int arg29, String arg30, int arg31, int arg32) {
        return _getDescriptor().getProxy().alta(arg0,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9,arg10,arg11,arg12,arg13,arg14,arg15,arg16,arg17,arg18,arg19,arg20,arg21,arg22,arg23,arg24,arg25,arg26,arg27,arg28,arg29,arg30,arg31,arg32);
    }

    public String validaCancelacion(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5) {
        return _getDescriptor().getProxy().validaCancelacion(arg0,arg1,arg2,arg3,arg4,arg5);
    }

    public String validaRehabilitacion(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5) {
        return _getDescriptor().getProxy().validaRehabilitacion(arg0,arg1,arg2,arg3,arg4,arg5);
    }

    public String validaModificacion(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5) {
        return _getDescriptor().getProxy().validaModificacion(arg0,arg1,arg2,arg3,arg4,arg5);
    }

    public String validaAlta(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5) {
        return _getDescriptor().getProxy().validaAlta(arg0,arg1,arg2,arg3,arg4,arg5);
    }

    public String rehabilitacion(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5, String arg6) {
        return _getDescriptor().getProxy().rehabilitacion(arg0,arg1,arg2,arg3,arg4,arg5,arg6);
    }

    public String cancelacion(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5, String arg6) {
        return _getDescriptor().getProxy().cancelacion(arg0,arg1,arg2,arg3,arg4,arg5,arg6);
    }

    public String modificacion(String arg0, String arg1, String arg2, String arg3, String arg4, String arg5, String arg6, int arg7, int arg8, int arg9, int arg10, int arg11, int arg12, String arg13, String arg14, String arg15, String arg16, String arg17, String arg18, String arg19, String arg20, int arg21, String arg22, String arg23, String arg24, String arg25, String arg26, int arg27, String arg28, int arg29, String arg30, int arg31, int arg32) {
        return _getDescriptor().getProxy().modificacion(arg0,arg1,arg2,arg3,arg4,arg5,arg6,arg7,arg8,arg9,arg10,arg11,arg12,arg13,arg14,arg15,arg16,arg17,arg18,arg19,arg20,arg21,arg22,arg23,arg24,arg25,arg26,arg27,arg28,arg29,arg30,arg31,arg32);
    }

}