//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.sapamis.alerta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for respuestaDetalleAlertaEmision complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="respuestaDetalleAlertaEmision">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="agente" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="canal_venta" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cliente1_materno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cliente1_nombre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cliente1_paterno" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fin_vigencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inciso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inicio_vigencia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="marca_desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="modelo" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="persona_c1" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poliza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="tipo_servicio" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ttrans_desc" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaDetalleAlertaEmision", propOrder = {
    "agente",
    "canalVenta",
    "cia",
    "cliente1Materno",
    "cliente1Nombre",
    "cliente1Paterno",
    "finVigencia",
    "inciso",
    "inicioVigencia",
    "marcaDesc",
    "modelo",
    "personaC1",
    "poliza",
    "tipoDesc",
    "tipoServicio",
    "ttransDesc"
})
public class RespuestaDetalleAlertaEmision {

    protected String agente;
    @XmlElement(name = "canal_venta")
    protected String canalVenta;
    protected String cia;
    @XmlElement(name = "cliente1_materno")
    protected String cliente1Materno;
    @XmlElement(name = "cliente1_nombre")
    protected String cliente1Nombre;
    @XmlElement(name = "cliente1_paterno")
    protected String cliente1Paterno;
    @XmlElement(name = "fin_vigencia")
    protected String finVigencia;
    protected String inciso;
    @XmlElement(name = "inicio_vigencia")
    protected String inicioVigencia;
    @XmlElement(name = "marca_desc")
    protected String marcaDesc;
    protected String modelo;
    @XmlElement(name = "persona_c1")
    protected String personaC1;
    protected String poliza;
    @XmlElement(name = "tipo_desc")
    protected String tipoDesc;
    @XmlElement(name = "tipo_servicio")
    protected String tipoServicio;
    @XmlElement(name = "ttrans_desc")
    protected String ttransDesc;

    /**
     * Gets the value of the agente property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAgente() {
        return agente;
    }

    /**
     * Sets the value of the agente property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAgente(String value) {
        this.agente = value;
    }

    /**
     * Gets the value of the canalVenta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCanalVenta() {
        return canalVenta;
    }

    /**
     * Sets the value of the canalVenta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCanalVenta(String value) {
        this.canalVenta = value;
    }

    /**
     * Gets the value of the cia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCia() {
        return cia;
    }

    /**
     * Sets the value of the cia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCia(String value) {
        this.cia = value;
    }

    /**
     * Gets the value of the cliente1Materno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliente1Materno() {
        return cliente1Materno;
    }

    /**
     * Sets the value of the cliente1Materno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliente1Materno(String value) {
        this.cliente1Materno = value;
    }

    /**
     * Gets the value of the cliente1Nombre property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliente1Nombre() {
        return cliente1Nombre;
    }

    /**
     * Sets the value of the cliente1Nombre property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliente1Nombre(String value) {
        this.cliente1Nombre = value;
    }

    /**
     * Gets the value of the cliente1Paterno property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCliente1Paterno() {
        return cliente1Paterno;
    }

    /**
     * Sets the value of the cliente1Paterno property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCliente1Paterno(String value) {
        this.cliente1Paterno = value;
    }

    /**
     * Gets the value of the finVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFinVigencia() {
        return finVigencia;
    }

    /**
     * Sets the value of the finVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFinVigencia(String value) {
        this.finVigencia = value;
    }

    /**
     * Gets the value of the inciso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInciso() {
        return inciso;
    }

    /**
     * Sets the value of the inciso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInciso(String value) {
        this.inciso = value;
    }

    /**
     * Gets the value of the inicioVigencia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInicioVigencia() {
        return inicioVigencia;
    }

    /**
     * Sets the value of the inicioVigencia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInicioVigencia(String value) {
        this.inicioVigencia = value;
    }

    /**
     * Gets the value of the marcaDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMarcaDesc() {
        return marcaDesc;
    }

    /**
     * Sets the value of the marcaDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMarcaDesc(String value) {
        this.marcaDesc = value;
    }

    /**
     * Gets the value of the modelo property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getModelo() {
        return modelo;
    }

    /**
     * Sets the value of the modelo property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setModelo(String value) {
        this.modelo = value;
    }

    /**
     * Gets the value of the personaC1 property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPersonaC1() {
        return personaC1;
    }

    /**
     * Sets the value of the personaC1 property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPersonaC1(String value) {
        this.personaC1 = value;
    }

    /**
     * Gets the value of the poliza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoliza() {
        return poliza;
    }

    /**
     * Sets the value of the poliza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoliza(String value) {
        this.poliza = value;
    }

    /**
     * Gets the value of the tipoDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoDesc() {
        return tipoDesc;
    }

    /**
     * Sets the value of the tipoDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoDesc(String value) {
        this.tipoDesc = value;
    }

    /**
     * Gets the value of the tipoServicio property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTipoServicio() {
        return tipoServicio;
    }

    /**
     * Sets the value of the tipoServicio property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTipoServicio(String value) {
        this.tipoServicio = value;
    }

    /**
     * Gets the value of the ttransDesc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTtransDesc() {
        return ttransDesc;
    }

    /**
     * Sets the value of the ttransDesc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTtransDesc(String value) {
        this.ttransDesc = value;
    }

}
