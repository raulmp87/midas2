//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.sapamis.prevencion;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.com.afirme.midas2.wsClient.sapamis.prevencion package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ValidaAlta_QNAME = new QName("http://ws.sap/", "validaAlta");
    private final static QName _ValidaAltaResponse_QNAME = new QName("http://ws.sap/", "validaAltaResponse");
    private final static QName _AltaResponse_QNAME = new QName("http://ws.sap/", "altaResponse");
    private final static QName _Alta_QNAME = new QName("http://ws.sap/", "alta");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mx.com.afirme.midas2.wsClient.sapamis.prevencion
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Alta }
     * 
     */
    public Alta createAlta() {
        return new Alta();
    }

    /**
     * Create an instance of {@link AltaResponse }
     * 
     */
    public AltaResponse createAltaResponse() {
        return new AltaResponse();
    }

    /**
     * Create an instance of {@link ValidaAlta }
     * 
     */
    public ValidaAlta createValidaAlta() {
        return new ValidaAlta();
    }

    /**
     * Create an instance of {@link ValidaAltaResponse }
     * 
     */
    public ValidaAltaResponse createValidaAltaResponse() {
        return new ValidaAltaResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaAlta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "validaAlta")
    public JAXBElement<ValidaAlta> createValidaAlta(ValidaAlta value) {
        return new JAXBElement<ValidaAlta>(_ValidaAlta_QNAME, ValidaAlta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaAltaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "validaAltaResponse")
    public JAXBElement<ValidaAltaResponse> createValidaAltaResponse(ValidaAltaResponse value) {
        return new JAXBElement<ValidaAltaResponse>(_ValidaAltaResponse_QNAME, ValidaAltaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "altaResponse")
    public JAXBElement<AltaResponse> createAltaResponse(AltaResponse value) {
        return new JAXBElement<AltaResponse>(_AltaResponse_QNAME, AltaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Alta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "alta")
    public JAXBElement<Alta> createAlta(Alta value) {
        return new JAXBElement<Alta>(_Alta_QNAME, Alta.class, null, value);
    }

}
