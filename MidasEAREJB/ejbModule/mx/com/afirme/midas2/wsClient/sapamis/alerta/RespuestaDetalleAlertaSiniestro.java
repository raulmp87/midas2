//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.sapamis.alerta;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for respuestaDetalleAlertaSiniestro complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="respuestaDetalleAlertaSiniestro">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="causa_siniestro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="cia" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="estatus" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="fecha_siniestro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="inciso" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="monto_siniestro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="num_siniestro" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="poliza" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaDetalleAlertaSiniestro", propOrder = {
    "causaSiniestro",
    "cia",
    "estatus",
    "fechaSiniestro",
    "inciso",
    "montoSiniestro",
    "numSiniestro",
    "poliza"
})
public class RespuestaDetalleAlertaSiniestro {

    @XmlElement(name = "causa_siniestro")
    protected String causaSiniestro;
    protected String cia;
    protected String estatus;
    @XmlElement(name = "fecha_siniestro")
    protected String fechaSiniestro;
    protected String inciso;
    @XmlElement(name = "monto_siniestro")
    protected String montoSiniestro;
    @XmlElement(name = "num_siniestro")
    protected String numSiniestro;
    protected String poliza;

    /**
     * Gets the value of the causaSiniestro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCausaSiniestro() {
        return causaSiniestro;
    }

    /**
     * Sets the value of the causaSiniestro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCausaSiniestro(String value) {
        this.causaSiniestro = value;
    }

    /**
     * Gets the value of the cia property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCia() {
        return cia;
    }

    /**
     * Sets the value of the cia property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCia(String value) {
        this.cia = value;
    }

    /**
     * Gets the value of the estatus property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEstatus() {
        return estatus;
    }

    /**
     * Sets the value of the estatus property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEstatus(String value) {
        this.estatus = value;
    }

    /**
     * Gets the value of the fechaSiniestro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFechaSiniestro() {
        return fechaSiniestro;
    }

    /**
     * Sets the value of the fechaSiniestro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFechaSiniestro(String value) {
        this.fechaSiniestro = value;
    }

    /**
     * Gets the value of the inciso property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getInciso() {
        return inciso;
    }

    /**
     * Sets the value of the inciso property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setInciso(String value) {
        this.inciso = value;
    }

    /**
     * Gets the value of the montoSiniestro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMontoSiniestro() {
        return montoSiniestro;
    }

    /**
     * Sets the value of the montoSiniestro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMontoSiniestro(String value) {
        this.montoSiniestro = value;
    }

    /**
     * Gets the value of the numSiniestro property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNumSiniestro() {
        return numSiniestro;
    }

    /**
     * Sets the value of the numSiniestro property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNumSiniestro(String value) {
        this.numSiniestro = value;
    }

    /**
     * Gets the value of the poliza property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPoliza() {
        return poliza;
    }

    /**
     * Sets the value of the poliza property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPoliza(String value) {
        this.poliza = value;
    }

}
