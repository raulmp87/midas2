//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.sapamis.siniestro;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.com.afirme.midas2.wsClient.sapamis.siniestro package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _ValidaAlta_QNAME = new QName("http://ws.sap/", "validaAlta");
    private final static QName _CancelacionResponse_QNAME = new QName("http://ws.sap/", "cancelacionResponse");
    private final static QName _Modificacion_QNAME = new QName("http://ws.sap/", "modificacion");
    private final static QName _Cancelacion_QNAME = new QName("http://ws.sap/", "cancelacion");
    private final static QName _ValidaAltaResponse_QNAME = new QName("http://ws.sap/", "validaAltaResponse");
    private final static QName _ModificacionResponse_QNAME = new QName("http://ws.sap/", "modificacionResponse");
    private final static QName _AltaResponse_QNAME = new QName("http://ws.sap/", "altaResponse");
    private final static QName _Alta_QNAME = new QName("http://ws.sap/", "alta");
    private final static QName _ValidaCancelacionResponse_QNAME = new QName("http://ws.sap/", "validaCancelacionResponse");
    private final static QName _ValidaCancelacion_QNAME = new QName("http://ws.sap/", "validaCancelacion");
    private final static QName _ValidaModificacionResponse_QNAME = new QName("http://ws.sap/", "validaModificacionResponse");
    private final static QName _ValidaModificacion_QNAME = new QName("http://ws.sap/", "validaModificacion");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mx.com.afirme.midas2.wsClient.sapamis.siniestro
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ValidaModificacion }
     * 
     */
    public ValidaModificacion createValidaModificacion() {
        return new ValidaModificacion();
    }

    /**
     * Create an instance of {@link ValidaModificacionResponse }
     * 
     */
    public ValidaModificacionResponse createValidaModificacionResponse() {
        return new ValidaModificacionResponse();
    }

    /**
     * Create an instance of {@link ValidaCancelacion }
     * 
     */
    public ValidaCancelacion createValidaCancelacion() {
        return new ValidaCancelacion();
    }

    /**
     * Create an instance of {@link ValidaCancelacionResponse }
     * 
     */
    public ValidaCancelacionResponse createValidaCancelacionResponse() {
        return new ValidaCancelacionResponse();
    }

    /**
     * Create an instance of {@link Alta }
     * 
     */
    public Alta createAlta() {
        return new Alta();
    }

    /**
     * Create an instance of {@link AltaResponse }
     * 
     */
    public AltaResponse createAltaResponse() {
        return new AltaResponse();
    }

    /**
     * Create an instance of {@link Cancelacion }
     * 
     */
    public Cancelacion createCancelacion() {
        return new Cancelacion();
    }

    /**
     * Create an instance of {@link Modificacion }
     * 
     */
    public Modificacion createModificacion() {
        return new Modificacion();
    }

    /**
     * Create an instance of {@link CancelacionResponse }
     * 
     */
    public CancelacionResponse createCancelacionResponse() {
        return new CancelacionResponse();
    }

    /**
     * Create an instance of {@link ValidaAlta }
     * 
     */
    public ValidaAlta createValidaAlta() {
        return new ValidaAlta();
    }

    /**
     * Create an instance of {@link ModificacionResponse }
     * 
     */
    public ModificacionResponse createModificacionResponse() {
        return new ModificacionResponse();
    }

    /**
     * Create an instance of {@link ValidaAltaResponse }
     * 
     */
    public ValidaAltaResponse createValidaAltaResponse() {
        return new ValidaAltaResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaAlta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "validaAlta")
    public JAXBElement<ValidaAlta> createValidaAlta(ValidaAlta value) {
        return new JAXBElement<ValidaAlta>(_ValidaAlta_QNAME, ValidaAlta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "cancelacionResponse")
    public JAXBElement<CancelacionResponse> createCancelacionResponse(CancelacionResponse value) {
        return new JAXBElement<CancelacionResponse>(_CancelacionResponse_QNAME, CancelacionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Modificacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "modificacion")
    public JAXBElement<Modificacion> createModificacion(Modificacion value) {
        return new JAXBElement<Modificacion>(_Modificacion_QNAME, Modificacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Cancelacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "cancelacion")
    public JAXBElement<Cancelacion> createCancelacion(Cancelacion value) {
        return new JAXBElement<Cancelacion>(_Cancelacion_QNAME, Cancelacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaAltaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "validaAltaResponse")
    public JAXBElement<ValidaAltaResponse> createValidaAltaResponse(ValidaAltaResponse value) {
        return new JAXBElement<ValidaAltaResponse>(_ValidaAltaResponse_QNAME, ValidaAltaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "modificacionResponse")
    public JAXBElement<ModificacionResponse> createModificacionResponse(ModificacionResponse value) {
        return new JAXBElement<ModificacionResponse>(_ModificacionResponse_QNAME, ModificacionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "altaResponse")
    public JAXBElement<AltaResponse> createAltaResponse(AltaResponse value) {
        return new JAXBElement<AltaResponse>(_AltaResponse_QNAME, AltaResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Alta }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "alta")
    public JAXBElement<Alta> createAlta(Alta value) {
        return new JAXBElement<Alta>(_Alta_QNAME, Alta.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaCancelacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "validaCancelacionResponse")
    public JAXBElement<ValidaCancelacionResponse> createValidaCancelacionResponse(ValidaCancelacionResponse value) {
        return new JAXBElement<ValidaCancelacionResponse>(_ValidaCancelacionResponse_QNAME, ValidaCancelacionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaCancelacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "validaCancelacion")
    public JAXBElement<ValidaCancelacion> createValidaCancelacion(ValidaCancelacion value) {
        return new JAXBElement<ValidaCancelacion>(_ValidaCancelacion_QNAME, ValidaCancelacion.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaModificacionResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "validaModificacionResponse")
    public JAXBElement<ValidaModificacionResponse> createValidaModificacionResponse(ValidaModificacionResponse value) {
        return new JAXBElement<ValidaModificacionResponse>(_ValidaModificacionResponse_QNAME, ValidaModificacionResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ValidaModificacion }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.sap/", name = "validaModificacion")
    public JAXBElement<ValidaModificacion> createValidaModificacion(ValidaModificacion value) {
        return new JAXBElement<ValidaModificacion>(_ValidaModificacion_QNAME, ValidaModificacion.class, null, value);
    }

}
