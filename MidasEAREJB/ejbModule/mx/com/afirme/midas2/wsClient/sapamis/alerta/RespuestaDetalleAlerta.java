//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.sapamis.alerta;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for respuestaDetalleAlerta complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="respuestaDetalleAlerta">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="cesvi" type="{http://ws.sap/}respuestaDetalleAlertaCesvi" minOccurs="0"/>
 *         &lt;element name="cii" type="{http://ws.sap/}respuestaDetalleAlertaCii" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="emision" type="{http://ws.sap/}respuestaDetalleAlertaEmision" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="error" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="ocra" type="{http://ws.sap/}respuestaDetalleAlertaOcra" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="prevencion" type="{http://ws.sap/}respuestaDetalleAlertaPrevencion" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="pt" type="{http://ws.sap/}respuestaDetalleAlertaPt" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="scd" type="{http://ws.sap/}respuestaDetalleAlertaScd" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="siniestro" type="{http://ws.sap/}respuestaDetalleAlertaSiniestro" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="sipac" type="{http://ws.sap/}respuestaDetalleAlertaSipac" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="valuacion" type="{http://ws.sap/}respuestaDetalleAlertaValuacion" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "respuestaDetalleAlerta", propOrder = {
    "cesvi",
    "cii",
    "emision",
    "error",
    "ocra",
    "prevencion",
    "pt",
    "scd",
    "siniestro",
    "sipac",
    "valuacion"
})
public class RespuestaDetalleAlerta {

    protected RespuestaDetalleAlertaCesvi cesvi;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaCii> cii;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaEmision> emision;
    protected String error;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaOcra> ocra;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaPrevencion> prevencion;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaPt> pt;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaScd> scd;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaSiniestro> siniestro;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaSipac> sipac;
    @XmlElement(nillable = true)
    protected List<RespuestaDetalleAlertaValuacion> valuacion;

    /**
     * Gets the value of the cesvi property.
     * 
     * @return
     *     possible object is
     *     {@link RespuestaDetalleAlertaCesvi }
     *     
     */
    public RespuestaDetalleAlertaCesvi getCesvi() {
        return cesvi;
    }

    /**
     * Sets the value of the cesvi property.
     * 
     * @param value
     *     allowed object is
     *     {@link RespuestaDetalleAlertaCesvi }
     *     
     */
    public void setCesvi(RespuestaDetalleAlertaCesvi value) {
        this.cesvi = value;
    }

    /**
     * Gets the value of the cii property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the cii property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getCii().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaCii }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaCii> getCii() {
        if (cii == null) {
            cii = new ArrayList<RespuestaDetalleAlertaCii>();
        }
        return this.cii;
    }

    /**
     * Gets the value of the emision property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the emision property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getEmision().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaEmision }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaEmision> getEmision() {
        if (emision == null) {
            emision = new ArrayList<RespuestaDetalleAlertaEmision>();
        }
        return this.emision;
    }

    /**
     * Gets the value of the error property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getError() {
        return error;
    }

    /**
     * Sets the value of the error property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setError(String value) {
        this.error = value;
    }

    /**
     * Gets the value of the ocra property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the ocra property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getOcra().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaOcra }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaOcra> getOcra() {
        if (ocra == null) {
            ocra = new ArrayList<RespuestaDetalleAlertaOcra>();
        }
        return this.ocra;
    }

    /**
     * Gets the value of the prevencion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the prevencion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPrevencion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaPrevencion }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaPrevencion> getPrevencion() {
        if (prevencion == null) {
            prevencion = new ArrayList<RespuestaDetalleAlertaPrevencion>();
        }
        return this.prevencion;
    }

    /**
     * Gets the value of the pt property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the pt property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getPt().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaPt }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaPt> getPt() {
        if (pt == null) {
            pt = new ArrayList<RespuestaDetalleAlertaPt>();
        }
        return this.pt;
    }

    /**
     * Gets the value of the scd property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the scd property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getScd().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaScd }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaScd> getScd() {
        if (scd == null) {
            scd = new ArrayList<RespuestaDetalleAlertaScd>();
        }
        return this.scd;
    }

    /**
     * Gets the value of the siniestro property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the siniestro property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSiniestro().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaSiniestro }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaSiniestro> getSiniestro() {
        if (siniestro == null) {
            siniestro = new ArrayList<RespuestaDetalleAlertaSiniestro>();
        }
        return this.siniestro;
    }

    /**
     * Gets the value of the sipac property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the sipac property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSipac().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaSipac }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaSipac> getSipac() {
        if (sipac == null) {
            sipac = new ArrayList<RespuestaDetalleAlertaSipac>();
        }
        return this.sipac;
    }

    /**
     * Gets the value of the valuacion property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valuacion property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValuacion().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link RespuestaDetalleAlertaValuacion }
     * 
     * 
     */
    public List<RespuestaDetalleAlertaValuacion> getValuacion() {
        if (valuacion == null) {
            valuacion = new ArrayList<RespuestaDetalleAlertaValuacion>();
        }
        return this.valuacion;
    }

}
