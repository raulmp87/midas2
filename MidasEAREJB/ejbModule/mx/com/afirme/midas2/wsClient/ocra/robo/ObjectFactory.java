//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.ocra.robo;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the mx.com.afirme.midas2.wsClient.ocra.robo package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _AltaRoboResponse_QNAME = new QName("http://ws.ocra.amis.com/", "altaRoboResponse");
    private final static QName _AltaRobo_QNAME = new QName("http://ws.ocra.amis.com/", "altaRobo");
    private final static QName _CancelarRoboResponse_QNAME = new QName("http://ws.ocra.amis.com/", "cancelarRoboResponse");
    private final static QName _CancelarRobo_QNAME = new QName("http://ws.ocra.amis.com/", "cancelarRobo");
    private final static QName _ModificarRoboResponse_QNAME = new QName("http://ws.ocra.amis.com/", "modificarRoboResponse");
    private final static QName _ModificarRobo_QNAME = new QName("http://ws.ocra.amis.com/", "modificarRobo");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: mx.com.afirme.midas2.wsClient.ocra.robo
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link AltaRobo }
     * 
     */
    public AltaRobo createAltaRobo() {
        return new AltaRobo();
    }

    /**
     * Create an instance of {@link AltaRoboResponse }
     * 
     */
    public AltaRoboResponse createAltaRoboResponse() {
        return new AltaRoboResponse();
    }

    /**
     * Create an instance of {@link ModificarRobo }
     * 
     */
    public ModificarRobo createModificarRobo() {
        return new ModificarRobo();
    }

    /**
     * Create an instance of {@link ModificarRoboResponse }
     * 
     */
    public ModificarRoboResponse createModificarRoboResponse() {
        return new ModificarRoboResponse();
    }

    /**
     * Create an instance of {@link CancelarRobo }
     * 
     */
    public CancelarRobo createCancelarRobo() {
        return new CancelarRobo();
    }

    /**
     * Create an instance of {@link CancelarRoboResponse }
     * 
     */
    public CancelarRoboResponse createCancelarRoboResponse() {
        return new CancelarRoboResponse();
    }

    /**
     * Create an instance of {@link AltaRoboBean }
     * 
     */
    public AltaRoboBean createAltaRoboBean() {
        return new AltaRoboBean();
    }

    /**
     * Create an instance of {@link CancelacionBean }
     * 
     */
    public CancelacionBean createCancelacionBean() {
        return new CancelacionBean();
    }

    /**
     * Create an instance of {@link MensajeBean }
     * 
     */
    public MensajeBean createMensajeBean() {
        return new MensajeBean();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaRoboResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.ocra.amis.com/", name = "altaRoboResponse")
    public JAXBElement<AltaRoboResponse> createAltaRoboResponse(AltaRoboResponse value) {
        return new JAXBElement<AltaRoboResponse>(_AltaRoboResponse_QNAME, AltaRoboResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link AltaRobo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.ocra.amis.com/", name = "altaRobo")
    public JAXBElement<AltaRobo> createAltaRobo(AltaRobo value) {
        return new JAXBElement<AltaRobo>(_AltaRobo_QNAME, AltaRobo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarRoboResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.ocra.amis.com/", name = "cancelarRoboResponse")
    public JAXBElement<CancelarRoboResponse> createCancelarRoboResponse(CancelarRoboResponse value) {
        return new JAXBElement<CancelarRoboResponse>(_CancelarRoboResponse_QNAME, CancelarRoboResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CancelarRobo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.ocra.amis.com/", name = "cancelarRobo")
    public JAXBElement<CancelarRobo> createCancelarRobo(CancelarRobo value) {
        return new JAXBElement<CancelarRobo>(_CancelarRobo_QNAME, CancelarRobo.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarRoboResponse }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.ocra.amis.com/", name = "modificarRoboResponse")
    public JAXBElement<ModificarRoboResponse> createModificarRoboResponse(ModificarRoboResponse value) {
        return new JAXBElement<ModificarRoboResponse>(_ModificarRoboResponse_QNAME, ModificarRoboResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link ModificarRobo }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://ws.ocra.amis.com/", name = "modificarRobo")
    public JAXBElement<ModificarRobo> createModificarRobo(ModificarRobo value) {
        return new JAXBElement<ModificarRobo>(_ModificarRobo_QNAME, ModificarRobo.class, null, value);
    }

}
