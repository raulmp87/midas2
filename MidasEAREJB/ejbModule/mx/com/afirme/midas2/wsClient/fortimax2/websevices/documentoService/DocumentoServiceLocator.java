/**
 * DocumentoServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService;

public class DocumentoServiceLocator extends org.apache.axis.client.Service implements mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoService {

    public DocumentoServiceLocator() {
    }


    public DocumentoServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public DocumentoServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for DocumentoServiceHttpSoap11Endpoint
    private java.lang.String DocumentoServiceHttpSoap11Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/DocumentoService.DocumentoServiceHttpSoap11Endpoint/";

    public java.lang.String getDocumentoServiceHttpSoap11EndpointAddress() {
        return DocumentoServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DocumentoServiceHttpSoap11EndpointWSDDServiceName = "DocumentoServiceHttpSoap11Endpoint";

    public java.lang.String getDocumentoServiceHttpSoap11EndpointWSDDServiceName() {
        return DocumentoServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setDocumentoServiceHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        DocumentoServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServicePortType getDocumentoServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(DocumentoServiceHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDocumentoServiceHttpSoap11Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServicePortType getDocumentoServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getDocumentoServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDocumentoServiceHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        DocumentoServiceHttpSoap11Endpoint_address = address;
    }


    // Use to get a proxy class for DocumentoServiceHttpSoap12Endpoint
    private java.lang.String DocumentoServiceHttpSoap12Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/DocumentoService.DocumentoServiceHttpSoap12Endpoint/";

    public java.lang.String getDocumentoServiceHttpSoap12EndpointAddress() {
        return DocumentoServiceHttpSoap12Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String DocumentoServiceHttpSoap12EndpointWSDDServiceName = "DocumentoServiceHttpSoap12Endpoint";

    public java.lang.String getDocumentoServiceHttpSoap12EndpointWSDDServiceName() {
        return DocumentoServiceHttpSoap12EndpointWSDDServiceName;
    }

    public void setDocumentoServiceHttpSoap12EndpointWSDDServiceName(java.lang.String name) {
        DocumentoServiceHttpSoap12EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServicePortType getDocumentoServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(DocumentoServiceHttpSoap12Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getDocumentoServiceHttpSoap12Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServicePortType getDocumentoServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap12BindingStub(portAddress, this);
            _stub.setPortName(getDocumentoServiceHttpSoap12EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setDocumentoServiceHttpSoap12EndpointEndpointAddress(java.lang.String address) {
        DocumentoServiceHttpSoap12Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap11BindingStub(new java.net.URL(DocumentoServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getDocumentoServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService.DocumentoServiceSoap12BindingStub(new java.net.URL(DocumentoServiceHttpSoap12Endpoint_address), this);
                _stub.setPortName(getDocumentoServiceHttpSoap12EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("DocumentoServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getDocumentoServiceHttpSoap11Endpoint();
        }
        else if ("DocumentoServiceHttpSoap12Endpoint".equals(inputPortName)) {
            return getDocumentoServiceHttpSoap12Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://documentoService.websevices.fortimax.syc.com", "DocumentoService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://documentoService.websevices.fortimax.syc.com", "DocumentoServiceHttpSoap11Endpoint"));
            ports.add(new javax.xml.namespace.QName("http://documentoService.websevices.fortimax.syc.com", "DocumentoServiceHttpSoap12Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("DocumentoServiceHttpSoap11Endpoint".equals(portName)) {
            setDocumentoServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
if ("DocumentoServiceHttpSoap12Endpoint".equals(portName)) {
            setDocumentoServiceHttpSoap12EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
