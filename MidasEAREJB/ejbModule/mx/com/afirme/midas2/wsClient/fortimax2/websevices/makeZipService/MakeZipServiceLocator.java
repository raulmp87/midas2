/**
 * MakeZipServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService;

public class MakeZipServiceLocator extends org.apache.axis.client.Service implements mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipService {

    public MakeZipServiceLocator() {
    }


    public MakeZipServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public MakeZipServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for MakeZipServiceHttpSoap11Endpoint
    private java.lang.String MakeZipServiceHttpSoap11Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/MakeZipService.MakeZipServiceHttpSoap11Endpoint/";

    public java.lang.String getMakeZipServiceHttpSoap11EndpointAddress() {
        return MakeZipServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MakeZipServiceHttpSoap11EndpointWSDDServiceName = "MakeZipServiceHttpSoap11Endpoint";

    public java.lang.String getMakeZipServiceHttpSoap11EndpointWSDDServiceName() {
        return MakeZipServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setMakeZipServiceHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        MakeZipServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServicePortType getMakeZipServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MakeZipServiceHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMakeZipServiceHttpSoap11Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServicePortType getMakeZipServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getMakeZipServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMakeZipServiceHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        MakeZipServiceHttpSoap11Endpoint_address = address;
    }


    // Use to get a proxy class for MakeZipServiceHttpSoap12Endpoint
    private java.lang.String MakeZipServiceHttpSoap12Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/MakeZipService.MakeZipServiceHttpSoap12Endpoint/";

    public java.lang.String getMakeZipServiceHttpSoap12EndpointAddress() {
        return MakeZipServiceHttpSoap12Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String MakeZipServiceHttpSoap12EndpointWSDDServiceName = "MakeZipServiceHttpSoap12Endpoint";

    public java.lang.String getMakeZipServiceHttpSoap12EndpointWSDDServiceName() {
        return MakeZipServiceHttpSoap12EndpointWSDDServiceName;
    }

    public void setMakeZipServiceHttpSoap12EndpointWSDDServiceName(java.lang.String name) {
        MakeZipServiceHttpSoap12EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServicePortType getMakeZipServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(MakeZipServiceHttpSoap12Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getMakeZipServiceHttpSoap12Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServicePortType getMakeZipServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap12BindingStub(portAddress, this);
            _stub.setPortName(getMakeZipServiceHttpSoap12EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setMakeZipServiceHttpSoap12EndpointEndpointAddress(java.lang.String address) {
        MakeZipServiceHttpSoap12Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap11BindingStub(new java.net.URL(MakeZipServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getMakeZipServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService.MakeZipServiceSoap12BindingStub(new java.net.URL(MakeZipServiceHttpSoap12Endpoint_address), this);
                _stub.setPortName(getMakeZipServiceHttpSoap12EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("MakeZipServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getMakeZipServiceHttpSoap11Endpoint();
        }
        else if ("MakeZipServiceHttpSoap12Endpoint".equals(inputPortName)) {
            return getMakeZipServiceHttpSoap12Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://websevices.fortimax.syc.com", "MakeZipService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://websevices.fortimax.syc.com", "MakeZipServiceHttpSoap11Endpoint"));
            ports.add(new javax.xml.namespace.QName("http://websevices.fortimax.syc.com", "MakeZipServiceHttpSoap12Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("MakeZipServiceHttpSoap11Endpoint".equals(portName)) {
            setMakeZipServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
if ("MakeZipServiceHttpSoap12Endpoint".equals(portName)) {
            setMakeZipServiceHttpSoap12EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
