/**
 * LoginServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService;

public interface LoginServicePortType extends java.rmi.Remote {
    public java.lang.String[] login(java.lang.String user, java.lang.String password, java.lang.Boolean passwordCifrado, java.lang.Long caducidad) throws java.rmi.RemoteException;
}
