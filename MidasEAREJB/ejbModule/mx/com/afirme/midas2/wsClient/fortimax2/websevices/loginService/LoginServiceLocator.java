/**
 * LoginServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService;

public class LoginServiceLocator extends org.apache.axis.client.Service implements mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginService {

    public LoginServiceLocator() {
    }


    public LoginServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LoginServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LoginServiceHttpSoap11Endpoint
    private java.lang.String LoginServiceHttpSoap11Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/LoginService.LoginServiceHttpSoap11Endpoint/";

    public java.lang.String getLoginServiceHttpSoap11EndpointAddress() {
        return LoginServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LoginServiceHttpSoap11EndpointWSDDServiceName = "LoginServiceHttpSoap11Endpoint";

    public java.lang.String getLoginServiceHttpSoap11EndpointWSDDServiceName() {
        return LoginServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setLoginServiceHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        LoginServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServicePortType getLoginServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LoginServiceHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLoginServiceHttpSoap11Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServicePortType getLoginServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getLoginServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLoginServiceHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        LoginServiceHttpSoap11Endpoint_address = address;
    }


    // Use to get a proxy class for LoginServiceHttpSoap12Endpoint
    private java.lang.String LoginServiceHttpSoap12Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/LoginService.LoginServiceHttpSoap12Endpoint/";

    public java.lang.String getLoginServiceHttpSoap12EndpointAddress() {
        return LoginServiceHttpSoap12Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LoginServiceHttpSoap12EndpointWSDDServiceName = "LoginServiceHttpSoap12Endpoint";

    public java.lang.String getLoginServiceHttpSoap12EndpointWSDDServiceName() {
        return LoginServiceHttpSoap12EndpointWSDDServiceName;
    }

    public void setLoginServiceHttpSoap12EndpointWSDDServiceName(java.lang.String name) {
        LoginServiceHttpSoap12EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServicePortType getLoginServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LoginServiceHttpSoap12Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLoginServiceHttpSoap12Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServicePortType getLoginServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap12BindingStub(portAddress, this);
            _stub.setPortName(getLoginServiceHttpSoap12EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLoginServiceHttpSoap12EndpointEndpointAddress(java.lang.String address) {
        LoginServiceHttpSoap12Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap11BindingStub(new java.net.URL(LoginServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getLoginServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.loginService.LoginServiceSoap12BindingStub(new java.net.URL(LoginServiceHttpSoap12Endpoint_address), this);
                _stub.setPortName(getLoginServiceHttpSoap12EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LoginServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getLoginServiceHttpSoap11Endpoint();
        }
        else if ("LoginServiceHttpSoap12Endpoint".equals(inputPortName)) {
            return getLoginServiceHttpSoap12Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://loginService.websevices.fortimax.syc.com", "LoginService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://loginService.websevices.fortimax.syc.com", "LoginServiceHttpSoap11Endpoint"));
            ports.add(new javax.xml.namespace.QName("http://loginService.websevices.fortimax.syc.com", "LoginServiceHttpSoap12Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LoginServiceHttpSoap11Endpoint".equals(portName)) {
            setLoginServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
if ("LoginServiceHttpSoap12Endpoint".equals(portName)) {
            setLoginServiceHttpSoap12EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
