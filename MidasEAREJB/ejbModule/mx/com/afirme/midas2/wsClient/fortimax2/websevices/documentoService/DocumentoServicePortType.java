/**
 * DocumentoServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.documentoService;

public interface DocumentoServicePortType extends java.rmi.Remote {
    public java.lang.String[] generateDocument(java.lang.String access, java.lang.String titulo_aplicacion, java.lang.String fieldName, java.lang.String fieldValue, java.lang.String fileType, java.lang.String folderName, java.lang.String documentName) throws java.rmi.RemoteException;
    public java.lang.String[] uploadFileNodo(java.lang.String fileName, byte[] fileData, java.lang.String gaveta, java.lang.String[] expediente, java.lang.String carpeta) throws java.rmi.RemoteException;
    public java.lang.String[] getDocumentsCompleteInfo(java.lang.String token, java.lang.String application, java.lang.String primaryKeyFieldName, java.lang.String primaryKeyFieldValue, java.lang.Boolean allDocs) throws java.rmi.RemoteException;
    public java.lang.String[] getDocuments(java.lang.String titulo_aplicacion, java.lang.String campo, java.lang.String valor) throws java.rmi.RemoteException;
    public java.lang.String[] uploadFile(java.lang.String fileName, byte[] fileData, java.lang.String gaveta, java.lang.String[] expediente, java.lang.String carpeta) throws java.rmi.RemoteException;
}
