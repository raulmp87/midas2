/**
 * ExpedienteServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService;

public class ExpedienteServiceLocator extends org.apache.axis.client.Service implements mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteService {

    public ExpedienteServiceLocator() {
    }


    public ExpedienteServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public ExpedienteServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for ExpedienteServiceHttpSoap11Endpoint
    private java.lang.String ExpedienteServiceHttpSoap11Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/ExpedienteService.ExpedienteServiceHttpSoap11Endpoint/";

    public java.lang.String getExpedienteServiceHttpSoap11EndpointAddress() {
        return ExpedienteServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ExpedienteServiceHttpSoap11EndpointWSDDServiceName = "ExpedienteServiceHttpSoap11Endpoint";

    public java.lang.String getExpedienteServiceHttpSoap11EndpointWSDDServiceName() {
        return ExpedienteServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setExpedienteServiceHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        ExpedienteServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType getExpedienteServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ExpedienteServiceHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getExpedienteServiceHttpSoap11Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType getExpedienteServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getExpedienteServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setExpedienteServiceHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        ExpedienteServiceHttpSoap11Endpoint_address = address;
    }


    // Use to get a proxy class for ExpedienteServiceHttpSoap12Endpoint
    private java.lang.String ExpedienteServiceHttpSoap12Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/ExpedienteService.ExpedienteServiceHttpSoap12Endpoint/";

    public java.lang.String getExpedienteServiceHttpSoap12EndpointAddress() {
        return ExpedienteServiceHttpSoap12Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String ExpedienteServiceHttpSoap12EndpointWSDDServiceName = "ExpedienteServiceHttpSoap12Endpoint";

    public java.lang.String getExpedienteServiceHttpSoap12EndpointWSDDServiceName() {
        return ExpedienteServiceHttpSoap12EndpointWSDDServiceName;
    }

    public void setExpedienteServiceHttpSoap12EndpointWSDDServiceName(java.lang.String name) {
        ExpedienteServiceHttpSoap12EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType getExpedienteServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(ExpedienteServiceHttpSoap12Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getExpedienteServiceHttpSoap12Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType getExpedienteServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap12BindingStub(portAddress, this);
            _stub.setPortName(getExpedienteServiceHttpSoap12EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setExpedienteServiceHttpSoap12EndpointEndpointAddress(java.lang.String address) {
        ExpedienteServiceHttpSoap12Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap11BindingStub(new java.net.URL(ExpedienteServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getExpedienteServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServiceSoap12BindingStub(new java.net.URL(ExpedienteServiceHttpSoap12Endpoint_address), this);
                _stub.setPortName(getExpedienteServiceHttpSoap12EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("ExpedienteServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getExpedienteServiceHttpSoap11Endpoint();
        }
        else if ("ExpedienteServiceHttpSoap12Endpoint".equals(inputPortName)) {
            return getExpedienteServiceHttpSoap12Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://expedienteService.websevices.fortimax.syc.com", "ExpedienteService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://expedienteService.websevices.fortimax.syc.com", "ExpedienteServiceHttpSoap11Endpoint"));
            ports.add(new javax.xml.namespace.QName("http://expedienteService.websevices.fortimax.syc.com", "ExpedienteServiceHttpSoap12Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("ExpedienteServiceHttpSoap11Endpoint".equals(portName)) {
            setExpedienteServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
if ("ExpedienteServiceHttpSoap12Endpoint".equals(portName)) {
            setExpedienteServiceHttpSoap12EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
