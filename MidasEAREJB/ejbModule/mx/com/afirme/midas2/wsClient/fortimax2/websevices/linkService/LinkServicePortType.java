/**
 * LinkServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService;

public interface LinkServicePortType extends java.rmi.Remote {
    public java.lang.String[] generateLink(java.lang.String user, java.lang.String pass, java.lang.Boolean encrypted, java.lang.String gaveta, java.lang.String expediente, java.lang.String documento, java.lang.Long segundosVigencia, java.lang.Boolean lecturaEscritura, java.lang.String nodo) throws java.rmi.RemoteException;
}
