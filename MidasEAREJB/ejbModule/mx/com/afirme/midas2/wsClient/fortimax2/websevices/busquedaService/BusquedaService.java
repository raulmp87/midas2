package mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService;

/**
 * BusquedaService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */



public interface BusquedaService extends javax.xml.rpc.Service {
    public java.lang.String getBusquedaServiceHttpSoap12EndpointAddress();

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType getBusquedaServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException;

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType getBusquedaServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getBusquedaServiceHttpSoap11EndpointAddress();

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType getBusquedaServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType getBusquedaServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
