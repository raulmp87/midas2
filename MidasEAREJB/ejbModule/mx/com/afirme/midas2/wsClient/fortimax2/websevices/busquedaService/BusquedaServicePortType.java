/**
 * BusquedaServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService;

public interface BusquedaServicePortType extends java.rmi.Remote {
    public java.lang.String[] getNodo(java.lang.String token, java.lang.String gaveta, java.lang.String primaryKeyFieldName, java.lang.String primaryKeyFieldValue, java.lang.String nombreElemento, java.lang.String tipoElemento) throws java.rmi.RemoteException;
}
