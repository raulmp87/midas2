package mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService;

/**
 * LinkServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */


public class LinkServiceLocator extends org.apache.axis.client.Service implements mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkService {

    public LinkServiceLocator() {
    }


    public LinkServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public LinkServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for LinkServiceHttpSoap11Endpoint
    private java.lang.String LinkServiceHttpSoap11Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/LinkService.LinkServiceHttpSoap11Endpoint/";

    public java.lang.String getLinkServiceHttpSoap11EndpointAddress() {
        return LinkServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LinkServiceHttpSoap11EndpointWSDDServiceName = "LinkServiceHttpSoap11Endpoint";

    public java.lang.String getLinkServiceHttpSoap11EndpointWSDDServiceName() {
        return LinkServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setLinkServiceHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        LinkServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServicePortType getLinkServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LinkServiceHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLinkServiceHttpSoap11Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServicePortType getLinkServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getLinkServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLinkServiceHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        LinkServiceHttpSoap11Endpoint_address = address;
    }


    // Use to get a proxy class for LinkServiceHttpSoap12Endpoint
    private java.lang.String LinkServiceHttpSoap12Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/LinkService.LinkServiceHttpSoap12Endpoint/";

    public java.lang.String getLinkServiceHttpSoap12EndpointAddress() {
        return LinkServiceHttpSoap12Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String LinkServiceHttpSoap12EndpointWSDDServiceName = "LinkServiceHttpSoap12Endpoint";

    public java.lang.String getLinkServiceHttpSoap12EndpointWSDDServiceName() {
        return LinkServiceHttpSoap12EndpointWSDDServiceName;
    }

    public void setLinkServiceHttpSoap12EndpointWSDDServiceName(java.lang.String name) {
        LinkServiceHttpSoap12EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServicePortType getLinkServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(LinkServiceHttpSoap12Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getLinkServiceHttpSoap12Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServicePortType getLinkServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap12BindingStub(portAddress, this);
            _stub.setPortName(getLinkServiceHttpSoap12EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setLinkServiceHttpSoap12EndpointEndpointAddress(java.lang.String address) {
        LinkServiceHttpSoap12Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap11BindingStub(new java.net.URL(LinkServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getLinkServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.linkService.LinkServiceSoap12BindingStub(new java.net.URL(LinkServiceHttpSoap12Endpoint_address), this);
                _stub.setPortName(getLinkServiceHttpSoap12EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("LinkServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getLinkServiceHttpSoap11Endpoint();
        }
        else if ("LinkServiceHttpSoap12Endpoint".equals(inputPortName)) {
            return getLinkServiceHttpSoap12Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://linkService.websevices.fortimax.syc.com", "LinkService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://linkService.websevices.fortimax.syc.com", "LinkServiceHttpSoap11Endpoint"));
            ports.add(new javax.xml.namespace.QName("http://linkService.websevices.fortimax.syc.com", "LinkServiceHttpSoap12Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("LinkServiceHttpSoap11Endpoint".equals(portName)) {
            setLinkServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
if ("LinkServiceHttpSoap12Endpoint".equals(portName)) {
            setLinkServiceHttpSoap12EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
