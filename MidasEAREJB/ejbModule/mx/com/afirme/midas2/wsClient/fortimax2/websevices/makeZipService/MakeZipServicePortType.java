/**
 * MakeZipServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.makeZipService;

public interface MakeZipServicePortType extends java.rmi.Remote {
    public java.lang.Object[] generaZip(java.lang.String user, java.lang.String pass, java.lang.Boolean encrypted, java.lang.String gaveta, java.lang.String expediente, java.lang.String documento) throws java.rmi.RemoteException;
    public byte[] descargaBytesPagina(java.lang.String token, java.lang.String nodo, java.lang.Integer numero_pagina) throws java.rmi.RemoteException;
    public byte[] descargaBytesZIP(java.lang.String token, java.lang.String nodo, java.lang.Boolean convertToPDF) throws java.rmi.RemoteException;
}
