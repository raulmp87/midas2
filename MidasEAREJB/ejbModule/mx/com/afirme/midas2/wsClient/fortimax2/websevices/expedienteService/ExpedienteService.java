/**
 * ExpedienteService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService;

public interface ExpedienteService extends javax.xml.rpc.Service {
    public java.lang.String getExpedienteServiceHttpSoap11EndpointAddress();

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType getExpedienteServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException;

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType getExpedienteServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
    public java.lang.String getExpedienteServiceHttpSoap12EndpointAddress();

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType getExpedienteServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException;

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService.ExpedienteServicePortType getExpedienteServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
