/**
 * ExpedienteServicePortType.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.expedienteService;

public interface ExpedienteServicePortType extends java.rmi.Remote {
    public java.lang.String[] cloneExpedient(java.lang.String access, java.lang.String drawerOriginal, java.lang.String[] fieldsOriginal, java.lang.String[] valuesOriginal, java.lang.String drawerCopy, java.lang.String[] fieldsCopy, java.lang.String[] valuesCopy, java.lang.Boolean totalClone) throws java.rmi.RemoteException;
    public java.lang.String[] generateExpediente(java.lang.String access, java.lang.String titulo_aplicacion, java.lang.String carpetaRaiz, java.lang.String[] fieldName, java.lang.String[] fieldValue, java.lang.Boolean createEstructura) throws java.rmi.RemoteException;
    public java.lang.String[] modifyExpediente(java.lang.String access, java.lang.String tituloAplicacion, java.lang.String[] fieldsOriginal, java.lang.String[] valuesOriginal, java.lang.String[] fieldsCopy, java.lang.String[] valuesCopy) throws java.rmi.RemoteException;
}
