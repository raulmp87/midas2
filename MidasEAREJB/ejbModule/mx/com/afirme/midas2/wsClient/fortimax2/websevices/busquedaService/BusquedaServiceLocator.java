/**
 * BusquedaServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService;;

public class BusquedaServiceLocator extends org.apache.axis.client.Service implements mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaService {

    public BusquedaServiceLocator() {
    }


    public BusquedaServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public BusquedaServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for BusquedaServiceHttpSoap12Endpoint
    private java.lang.String BusquedaServiceHttpSoap12Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/BusquedaService.BusquedaServiceHttpSoap12Endpoint/";

    public java.lang.String getBusquedaServiceHttpSoap12EndpointAddress() {
        return BusquedaServiceHttpSoap12Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BusquedaServiceHttpSoap12EndpointWSDDServiceName = "BusquedaServiceHttpSoap12Endpoint";

    public java.lang.String getBusquedaServiceHttpSoap12EndpointWSDDServiceName() {
        return BusquedaServiceHttpSoap12EndpointWSDDServiceName;
    }

    public void setBusquedaServiceHttpSoap12EndpointWSDDServiceName(java.lang.String name) {
        BusquedaServiceHttpSoap12EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType getBusquedaServiceHttpSoap12Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BusquedaServiceHttpSoap12Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBusquedaServiceHttpSoap12Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType getBusquedaServiceHttpSoap12Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap12BindingStub(portAddress, this);
            _stub.setPortName(getBusquedaServiceHttpSoap12EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBusquedaServiceHttpSoap12EndpointEndpointAddress(java.lang.String address) {
        BusquedaServiceHttpSoap12Endpoint_address = address;
    }


    // Use to get a proxy class for BusquedaServiceHttpSoap11Endpoint
    private java.lang.String BusquedaServiceHttpSoap11Endpoint_address = "http://dev.afirme.com.mx:80/FortimaxSeguros/services/BusquedaService.BusquedaServiceHttpSoap11Endpoint/";

    public java.lang.String getBusquedaServiceHttpSoap11EndpointAddress() {
        return BusquedaServiceHttpSoap11Endpoint_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String BusquedaServiceHttpSoap11EndpointWSDDServiceName = "BusquedaServiceHttpSoap11Endpoint";

    public java.lang.String getBusquedaServiceHttpSoap11EndpointWSDDServiceName() {
        return BusquedaServiceHttpSoap11EndpointWSDDServiceName;
    }

    public void setBusquedaServiceHttpSoap11EndpointWSDDServiceName(java.lang.String name) {
        BusquedaServiceHttpSoap11EndpointWSDDServiceName = name;
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType getBusquedaServiceHttpSoap11Endpoint() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(BusquedaServiceHttpSoap11Endpoint_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getBusquedaServiceHttpSoap11Endpoint(endpoint);
    }

    public mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType getBusquedaServiceHttpSoap11Endpoint(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap11BindingStub(portAddress, this);
            _stub.setPortName(getBusquedaServiceHttpSoap11EndpointWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setBusquedaServiceHttpSoap11EndpointEndpointAddress(java.lang.String address) {
        BusquedaServiceHttpSoap11Endpoint_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     * This service has multiple ports for a given interface;
     * the proxy implementation returned may be indeterminate.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap12BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap12BindingStub(new java.net.URL(BusquedaServiceHttpSoap12Endpoint_address), this);
                _stub.setPortName(getBusquedaServiceHttpSoap12EndpointWSDDServiceName());
                return _stub;
            }
            if (mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServicePortType.class.isAssignableFrom(serviceEndpointInterface)) {
                mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap11BindingStub _stub = new mx.com.afirme.midas2.wsClient.fortimax2.websevices.busquedaService.BusquedaServiceSoap11BindingStub(new java.net.URL(BusquedaServiceHttpSoap11Endpoint_address), this);
                _stub.setPortName(getBusquedaServiceHttpSoap11EndpointWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("BusquedaServiceHttpSoap12Endpoint".equals(inputPortName)) {
            return getBusquedaServiceHttpSoap12Endpoint();
        }
        else if ("BusquedaServiceHttpSoap11Endpoint".equals(inputPortName)) {
            return getBusquedaServiceHttpSoap11Endpoint();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://busquedaService.websevices.fortimax.syc.com", "BusquedaService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://busquedaService.websevices.fortimax.syc.com", "BusquedaServiceHttpSoap12Endpoint"));
            ports.add(new javax.xml.namespace.QName("http://busquedaService.websevices.fortimax.syc.com", "BusquedaServiceHttpSoap11Endpoint"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("BusquedaServiceHttpSoap12Endpoint".equals(portName)) {
            setBusquedaServiceHttpSoap12EndpointEndpointAddress(address);
        }
        else 
if ("BusquedaServiceHttpSoap11Endpoint".equals(portName)) {
            setBusquedaServiceHttpSoap11EndpointEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
