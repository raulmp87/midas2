package mx.com.afirme.midas2.wsClient.serviciopublicovehicular;

import javax.naming.InitialContext;
import javax.naming.NamingException;

import java.net.URL;

import javax.xml.namespace.QName;
import javax.xml.transform.Source;
import javax.xml.ws.BindingProvider;
import javax.xml.ws.Dispatch;
import javax.xml.ws.Service;
import javax.xml.ws.soap.SOAPBinding;
import javax.xml.datatype.XMLGregorianCalendar;

public class WSGestionAjustadoresSoap12Proxy{

    protected Descriptor _descriptor;

    public class Descriptor {
        private mx.com.afirme.midas2.wsClient.serviciopublicovehicular.WSGestionAjustadores _service = null;
        private mx.com.afirme.midas2.wsClient.serviciopublicovehicular.WSGestionAjustadoresSoap _proxy = null;
        private Dispatch<Source> _dispatch = null;
        private boolean _useJNDIOnly = false;

        public Descriptor() {
            init();
        }

        public Descriptor(URL wsdlLocation, QName serviceName) {
            _service = new mx.com.afirme.midas2.wsClient.serviciopublicovehicular.WSGestionAjustadores(wsdlLocation, serviceName);
            initCommon();
        }

        public void init() {
            _service = null;
            _proxy = null;
            _dispatch = null;
            try
            {
                InitialContext ctx = new InitialContext();
                _service = (mx.com.afirme.midas2.wsClient.serviciopublicovehicular.WSGestionAjustadores)ctx.lookup("java:comp/env/service/WSGestionAjustadores");
            }
            catch (NamingException e)
            {
                if ("true".equalsIgnoreCase(System.getProperty("DEBUG_PROXY"))) {
                    System.out.println("JNDI lookup failure: javax.naming.NamingException: " + e.getMessage());
                    e.printStackTrace(System.out);
                }
            }

            if (_service == null && !_useJNDIOnly)
                _service = new mx.com.afirme.midas2.wsClient.serviciopublicovehicular.WSGestionAjustadores();
            initCommon();
        }

        private void initCommon() {
            _proxy = _service.getWSGestionAjustadoresSoap12();
        }

        public mx.com.afirme.midas2.wsClient.serviciopublicovehicular.WSGestionAjustadoresSoap getProxy() {
            return _proxy;
        }

        public void useJNDIOnly(boolean useJNDIOnly) {
            _useJNDIOnly = useJNDIOnly;
            init();
        }

        public Dispatch<Source> getDispatch() {
            if (_dispatch == null ) {
                QName portQName = new QName("http://tempuri.org/", "WSGestionAjustadoresSoap12");
                _dispatch = _service.createDispatch(portQName, Source.class, Service.Mode.MESSAGE);

                String proxyEndpointUrl = getEndpoint();
                BindingProvider bp = (BindingProvider) _dispatch;
                String dispatchEndpointUrl = (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
                if (!dispatchEndpointUrl.equals(proxyEndpointUrl))
                    bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, proxyEndpointUrl);
            }
            return _dispatch;
        }

        public String getEndpoint() {
            BindingProvider bp = (BindingProvider) _proxy;
            return (String) bp.getRequestContext().get(BindingProvider.ENDPOINT_ADDRESS_PROPERTY);
        }

        public void setEndpoint(String endpointUrl) {
            BindingProvider bp = (BindingProvider) _proxy;
            bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);

            if (_dispatch != null ) {
                bp = (BindingProvider) _dispatch;
                bp.getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY, endpointUrl);
            }
        }

        public void setMTOMEnabled(boolean enable) {
            SOAPBinding binding = (SOAPBinding) ((BindingProvider) _proxy).getBinding();
            binding.setMTOMEnabled(enable);
        }
    }

    public WSGestionAjustadoresSoap12Proxy() {
        _descriptor = new Descriptor();
        _descriptor.setMTOMEnabled(true);
    }

    public WSGestionAjustadoresSoap12Proxy(URL wsdlLocation, QName serviceName) {
        _descriptor = new Descriptor(wsdlLocation, serviceName);
        _descriptor.setMTOMEnabled(true);
    }

    public Descriptor _getDescriptor() {
        return _descriptor;
    }

    public RespuestaValidaUsuarios validaUsuario(String usuario, String password) {
        return _getDescriptor().getProxy().validaUsuario(usuario,password);
    }

    public RespuestaRecuperaSPVs recuperaSPVs(String usuario, String password, XMLGregorianCalendar fechaInicio, XMLGregorianCalendar fechaFin) {
        return _getDescriptor().getProxy().recuperaSPVs(usuario,password,fechaInicio,fechaFin);
    }

    public RespuestaRecuperaSiniestros recuperaSiniestros(String usuario, String password, int spv, int anio) {
        return _getDescriptor().getProxy().recuperaSiniestros(usuario,password,spv,anio);
    }

    public RespuestaEnviaReporte enviaReporte(String usuario, String password, int idMidasSPV, Cierre datosCierre, ArrayOfTercero datosTerceros, ArrayOfOrdenes datosOrdenes, ArrayOfAsistencias datosAsistencias, ArrayOfEstimaciones datosEstimaciones, ArrayOfTiemposCierre datosTiempos) {
        return _getDescriptor().getProxy().enviaReporte(usuario,password,idMidasSPV,datosCierre,datosTerceros,datosOrdenes,datosAsistencias,datosEstimaciones,datosTiempos);
    }

    public RespuestaEnviaEvidencias enviaEvidencias(String usuario, String password, int spv, int anio, ArrayOfFotoSiniestro evidenciasSiniestro, ArrayOfFotoExpediente evidenciasExpediente) {
        return _getDescriptor().getProxy().enviaEvidencias(usuario,password,spv,anio,evidenciasSiniestro,evidenciasExpediente);
    }

    public RespuestaRecord record(String usuario, String password, XMLGregorianCalendar fechaInicio, XMLGregorianCalendar fechaFin) {
        return _getDescriptor().getProxy().record(usuario,password,fechaInicio,fechaFin);
    }

    public RespuestaAfirme buscarIncisosPoliza(int spv, int anio) {
        return _getDescriptor().getProxy().buscarIncisosPoliza(spv,anio);
    }

    public RespuestaAfirme generarReporteCabina(int spv, int anio) {
        return _getDescriptor().getProxy().generarReporteCabina(spv,anio);
    }

    public RespuestaAfirme sincronizarReporte(int spv, int anio) {
        return _getDescriptor().getProxy().sincronizarReporte(spv,anio);
    }

}