//
// Generated By:JAX-WS RI IBM 2.2.1-11/30/2010 12:42 PM(foreman)- (JAXB RI IBM 2.2.3-11/28/2011 06:21 AM(foreman)-)
//


package mx.com.afirme.midas2.wsClient.serviciopublicovehicular;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for RespuestaRecuperaSPVs complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="RespuestaRecuperaSPVs">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="ClaveError" type="{http://www.w3.org/2001/XMLSchema}int"/>
 *         &lt;element name="DescripcionError" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SPVs" type="{http://tempuri.org/}SPVs" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RespuestaRecuperaSPVs", propOrder = {
    "claveError",
    "descripcionError",
    "spVs"
})
public class RespuestaRecuperaSPVs {

    @XmlElement(name = "ClaveError")
    protected int claveError;
    @XmlElement(name = "DescripcionError")
    protected String descripcionError;
    @XmlElement(name = "SPVs")
    protected SPVs spVs;

    /**
     * Gets the value of the claveError property.
     * 
     */
    public int getClaveError() {
        return claveError;
    }

    /**
     * Sets the value of the claveError property.
     * 
     */
    public void setClaveError(int value) {
        this.claveError = value;
    }

    /**
     * Gets the value of the descripcionError property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDescripcionError() {
        return descripcionError;
    }

    /**
     * Sets the value of the descripcionError property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDescripcionError(String value) {
        this.descripcionError = value;
    }

    /**
     * Gets the value of the spVs property.
     * 
     * @return
     *     possible object is
     *     {@link SPVs }
     *     
     */
    public SPVs getSPVs() {
        return spVs;
    }

    /**
     * Sets the value of the spVs property.
     * 
     * @param value
     *     allowed object is
     *     {@link SPVs }
     *     
     */
    public void setSPVs(SPVs value) {
        this.spVs = value;
    }

}
