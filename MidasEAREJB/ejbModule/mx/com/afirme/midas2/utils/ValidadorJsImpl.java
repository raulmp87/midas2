package mx.com.afirme.midas2.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.util.ValidadorJs;

@Stateless
public class ValidadorJsImpl implements ValidadorJs {

	@Override
	public String generarValidacionJs(String id, String dependenciaId, 
			Integer tipoValidador, Integer tipoDependencia) {
		if ((tipoValidador == null || tipoValidador < 0) && 
				(tipoDependencia == null || tipoDependencia <0)) {
			return "";
		}
		
		String dependenciaJs = null;
		if (tipoValidador == null || tipoValidador < 0) {
			return generaTipoDependenciaJs(id, dependenciaId, tipoDependencia, false);
		}
		
		
		dependenciaJs = generaTipoDependenciaJs(id, dependenciaId, tipoDependencia, true);
		if (dependenciaJs.equals("")) {
			dependenciaJs = null;
		}
		
		String validacionJs = "";
		switch (tipoValidador){
		case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO:
			validacionJs="validarDecimal('" + id + "', 8, 4, " + dependenciaJs + ");";
			break;
		case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_DOS:
			validacionJs="validarDecimal('" + id + "', 8, 2, " + dependenciaJs + ");";
			break;
		case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS: 
			validacionJs="validarDecimal('" + id + "', 16, 2, " + dependenciaJs + ");";
			break;
		case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO:
			validacionJs="validarDecimal('" + id + "', 16, 4, " + dependenciaJs + ");";
			break;
		case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO:
			validacionJs="validarDecimal('" + id + "', 8, 0, " + dependenciaJs + ");";
			break;
		}
		
		if(id.equals("cotizacion.datosRiesgo_\\'1x1x6011x0x100\\'_")){
			validacionJs += "validaPisoEncuentra();";
		}
		
		return validacionJs;
	}

	@Override
	public String generaTipoDependenciaJs(String id, String dependenciaId, Integer tipoDependencia, boolean prepareForEval) {
		if (tipoDependencia == null) {
			return "";
		}
		
		String validacion = "";
		switch (tipoDependencia){
		case ControlDinamicoRiesgoDTO.TIPO_DEPENDENCIA_ES_HORIZONTAL:
			validacion="validarHorizontal('" + id + "', " + "'" + dependenciaId + "'" + ");";
			break;
		}
		
		if (StringUtils.isNotBlank(validacion) && prepareForEval) {
			validacion = prepareForEval(validacion);
		}
		
		return validacion;
	}
	

    private String prepareForEval(String js) {
    	//1. Buscar los caracteres que ya tienen escape y ponerles doble escape.
    	Pattern p = Pattern.compile("\\\\");
    	Matcher m = p.matcher(js);
    	
    	int charsIns = 0;
    	StringBuffer result = new StringBuffer(js);
    	
    	while (m.find()) {
    		result.insert(m.start() + charsIns, "\\\\");
    		charsIns += 2;
    	}
    	
    	//2. Buscar comillas sin escape y ponerles escape.
    	p = Pattern.compile("[^\\\\]'");
    	m = p.matcher(result.toString());
    	
    	charsIns = 0;   	
    	while (m.find()) {
    		result.insert(m.start() +1 + charsIns, "\\");
    		charsIns += 1;
    	}
    	
    	//3. Por ultimo agregarle una comilla simple al principio y al final
    	result.insert(0, "'");
    	result.insert(result.length(), "'");
    	
    	return result.toString();
    }

}
