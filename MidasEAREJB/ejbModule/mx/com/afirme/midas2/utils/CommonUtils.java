package mx.com.afirme.midas2.utils;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;

import javax.persistence.Query;

import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.lang.StringUtils;

public class CommonUtils {
	/**
	 * Valida si una cadena es nula o vacia
	 * @param value
	 * @return
	 */
	public static boolean isValid(String value){
		return (value!=null && !value.trim().isEmpty());
	}
	/**
	 * Valida si un objeto es nulo
	 * @param obj
	 * @return
	 */
	public static boolean isNull(Object obj){
		return (obj==null);
	}
	/**
	 * Valida si un objeto no es nulo
	 * @param obj
	 * @return
	 */
	public static boolean isNotNull(Object obj){
		return (obj!=null);
	}
	public static boolean isNotEmpty(Object obj){
		if(obj!=null){
			if(obj.toString().trim().equals(""))
				return false;
			else
				return true;
		}
		else
			return false;
			
	}
	/**
	 * Valida si una lista es nula o vacia
	 * @param collection
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isEmptyList(Collection collection){
		return (collection==null || collection.isEmpty());
	}
	
	public static boolean isEmptyArray(Object[] collection){
		return (collection==null || collection.length==0);
	}
	
	public static void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	public static void setQueryParametersByProperties(Query entityQuery, Map<String, Object> parameters){
		Iterator<Entry<String, Object>> it = parameters.entrySet().iterator();
		while(it.hasNext()){
			Map.Entry<String, Object> pairs = (Map.Entry<String, Object>)it.next();
			String key=getValidProperty(pairs.getKey());
			entityQuery.setParameter(key, pairs.getValue());
		}		
	}
	
	public static String getValidProperty(String property){
		String validProperty=property;
		StringBuilder str=new StringBuilder("");
		if(property.contains(".")){
			int i=0;
			for(String word:property.split("\\.")){
				if(i==0){
					word=word.toLowerCase();
				}else{
					word=StringUtils.capitalize(word);
				}
				str.append(word);
				i++;
			}
			validProperty=str.toString();
		}
		return validProperty;
	}
	
	public static String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}
		return query;
	}
	/**
	 * Metodo para lanzar excepciones
	 * @param error
	 * @throws Exception
	 */
	public static void onError(String error) throws MidasException{
		throw new MidasException(error);
	}
	/**
	 * Metodo para lanzar excepciones
	 * @param e
	 * @throws Exception
	 */
	public static void onError(Throwable e) throws MidasException{
		throw new MidasException(e);
	}
	/**
	 * Convierte una fecha con hora y fecha a una fecha a las 0:00:00 
	 * @param date
	 * @return
	 */
	public static Date setTimeToMidnight(Date date){
		if(isNotNull(date)){
			Calendar calendar = Calendar.getInstance();
		    calendar.setTime( date );
		    calendar.set(Calendar.HOUR_OF_DAY, 0);
		    calendar.set(Calendar.MINUTE, 0);
		    calendar.set(Calendar.SECOND, 0);
		    calendar.set(Calendar.MILLISECOND, 0);
		    return calendar.getTime();
		}
		return date;
	}

	/**
	 * Genera una fecha con dia de fechaHora y la hora de horaCita con formato
	 * 24 hrs 00:00
	 * @param fechaCita
	 * @param horaCita
	 * @return
	 */
	public static Date getFechaHora(Date fechaCita, String horaCita) {
		Date fechaHoraARetornar = fechaCita;
		Calendar calendario = new GregorianCalendar();
		try {
			// Date nuevaFechaCita = removerHora(fechaCita);
			calendario.setTime(fechaCita);
			String[] hora24 = horaCita.split(":");
			calendario.set(Calendar.HOUR, Integer.valueOf(hora24[0]));
			calendario.set(Calendar.MINUTE, Integer.valueOf(hora24[1]));
		} catch (Exception e) {
		}
		fechaHoraARetornar = calendario.getTime();
		return fechaHoraARetornar;
	}

	/**
	 * Indica si 2 fechas son iguales (sin considerar las horas)
	 * @param date1
	 * @param date2
	 * @return
	 */
	public static boolean compareOnlyDatesWithoutTime(Date date1,Date date2){
		boolean isEqual=false;
		if(isNotNull(date1) && isNotNull(date2)){
			Date d1=setTimeToMidnight(date1);
			Date d2=setTimeToMidnight(date2);
			isEqual=(d1.compareTo(d2)==0);
		}
		return isEqual;
	}
	
	/**
	 * Valida si una cadena es nula o vacia. Retornará true si es vacía.
	 * @param value
	 * @return
	 */
	public static boolean isNullOrEmpty(String value){
		return !isValid(value);
	}

	/**
	 * Valida si una cadena es nula o vacia. Retornará true si es vacía.
	 * @param value
	 * @return
	 */
	public static boolean isNullOrZero(Object value){
		boolean isNull = false;
		int zero = 0;
		isNull = isNull(value);
		if (!isNull){
			String objectType = value.getClass().getSimpleName();
			if(objectType.equals("BigDecimal")){
				isNull = value.equals(BigDecimal.valueOf(zero));
			}else if(objectType.equals("Integer")){
				isNull = value.equals(zero);
			}else if(objectType.equals("Long")){
				isNull = value.equals(Long.valueOf(zero));
			}else if(objectType.equals("Double")){
				isNull = value.equals(Double.valueOf(zero));
			}else if(objectType.equals("Short")){
				isNull = Short.valueOf(value.toString()) == zero;
			}
		}
		return isNull;
	}
	
	/**
	 * Valida si un número no es nulo ni equivalente a cero
	 * Retornará true si el número es válido.
	 * @param o
	 * @return boolean
	 */
	public static boolean isNotNullNorZero(Object o){
		boolean retVal;
		int zero = 0;
		retVal = isNotNull(o);
		if (retVal){
			String objectType = o.getClass().getSimpleName(); 
			if(objectType.equals("BigDecimal")){
				retVal = !o.equals(BigDecimal.valueOf(zero));
			}else if(objectType.equals("Integer")){
				retVal = !o.equals(zero);
			}else if(objectType.equals("Long")){
				retVal = !o.equals(Long.valueOf(zero));
			}else if(objectType.equals("Double")){
				retVal = !o.equals(Double.valueOf(zero));
			}else if(objectType.equals("Short")){
				retVal = !(Short.valueOf(o.toString()) == zero);
			}
		}
		return retVal;
	}
	
	/**
	 * Valida si una lista tiene al menos un elemento
	 * @param collection
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public static boolean isNotEmptyList(Collection collection){
		return (collection!=null && !collection.isEmpty());
	}

}
