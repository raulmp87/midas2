package mx.com.afirme.midas2.utils;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import net.sf.jasperreports.engine.JRDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

import java.io.ByteArrayOutputStream;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

/**
 * Clase encargada de generar el objeto para generar Impresión, realizando los procesos generales.
 * 
 * @author eftregue 
 */
public class GeneradorImpresion {

	
	/**
	 * Metodo encargado de retornar el objeto utilizado para generar la
	 * impresión, por lo general se retornará a la capa de control.
	 * 
	 * @param collectionObj DataSource con los objetos a refefir en el archivo ".jrxml".
	 * @param jrxml URL del archivo ".jrxml".
	 * @param params Map de parametros con la información a settear.
	 * @return transporte
	 */
	public TransporteImpresionDTO getTImpresionDTO( JasperReport jReport, JRDataSource dataSource, Map<String, Object> params ) {
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		byte[] byteReport = null;

		try {
			byteReport = generateReport(jReport, params, dataSource);
			transporte.setByteArray(byteReport);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return transporte;
	}
	
	/**
	 * Metodo encargado de retornar un arreglo de bytes
	 * 
	 * @param jReport Objeto de JasperReport.
	 * @param params Map con la información.
	 * @param collectionObj Coleccion de objetos con la información.
	 * @return
	 */
	public byte[] generateReport(JasperReport jReport, Map<String, Object> params,JRDataSource dataSource) {		
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		try {
			JasperPrint jasperPrint = JasperFillManager.fillReport(jReport,	params, dataSource);
			JasperExportManager.exportReportToPdfStream(jasperPrint, out);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return out.toByteArray();
	}

	/**
	 * Retorna un objeto del tipo "JasperReport"
	 * 
	 * @param jrxml URL del archivo ".jrxml".
	 * @return
	 */
	public JasperReport getOJasperReport(String jrxml) {
		
		InputStream stream = this.getClass().getResourceAsStream(jrxml);
				
		JasperReport report = null;
		try {
			if(jrxml.endsWith(".jrxml")){
				report = JasperCompileManager.compileReport(stream);
			} else if(jrxml.endsWith(".jasper")){
				report = (JasperReport) JRLoader.loadObject(stream);
			}
		} catch (JRException e) {
			e.printStackTrace();
		}

		return report;
	}
	
	public void pdfConcantenate(
			List<byte[]> inputByteArray,
			ByteArrayOutputStream outputStream) {

	      try {
	          int pageOffset = 0;    
	          int f = 0;     
	          ArrayList master = new ArrayList();
	          Document document = null;
	          PdfCopy  writer = null;	          
	          // we create a reader for a certain document	          
	          Iterator iterator = inputByteArray.iterator();	          
	          while(iterator.hasNext()){
	            byte[] data = (byte[])iterator.next();
	            if(data == null || data.length == 0){
	              f++;
	              continue;
	            }
	            PdfReader reader = new PdfReader(data);
	            reader.consolidateNamedDestinations();
	            // we retrieve the total number of pages
	            int n = reader.getNumberOfPages();
	            List bookmarks = SimpleBookmark.getBookmark(reader);
	            if (bookmarks != null) {
	              if (pageOffset != 0)
	                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	                master.addAll(bookmarks);
	            }	            
	            pageOffset += n;	                
	            if (f == 0) {
	              // step 1: creation of a document-object
	              document = new Document(reader.getPageSizeWithRotation(1));
	              // step 2: we create a writer that listens to the document                          
	              writer = new PdfCopy(document, outputStream);
	              // step 3: we open the document
	              document.open();           
	            }	              
	            // step 4: we add content
	            PdfImportedPage page;
	            for (int i = 0; i < n; ) {
	              ++i;
	              page = writer.getImportedPage(reader, i);
	              writer.addPage(page);
	            }	  
	            PRAcroForm form = reader.getAcroForm();
	            if (form != null){
	              writer.copyAcroForm(reader);
	            }
	            f++;
	          }
	          if (!master.isEmpty()){
	            writer.setOutlines(master);
	          }
	          // step 5: we close the document
	          if(document != null){
	            document.close();
	          }
	      }
	      catch(Exception e) {	   
	    	  ;
	      }

	}

}