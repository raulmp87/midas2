package mx.com.afirme.midas2.utils;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.StringTokenizer;

import mx.com.afirme.midas2.dao.impl.catalogos.EntidadDaoImpl;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

public class LeerArchivo extends EntidadDaoImpl {

	private static final Logger LOG = Logger.getLogger(LeerArchivo.class);

	public static  List<Map<String,String>> leer(String pathArchivo){

		final File file = new File(pathArchivo);
		LOG.debug(file.getPath());
		final List <Map<String,String>> result = new ArrayList<Map<String,String>>();
		Map<String, String> mapaElementos = null;
		int cont = 0;
		final List <String> encabezado = new ArrayList<String>();

		try {
			@SuppressWarnings("unchecked")
			final List <String> renglones = FileUtils.readLines(file,"UTF8");

			for (String renglon : renglones) {
				int index = 0;
				mapaElementos = new HashMap<String, String>();
				StringTokenizer tokens = new StringTokenizer(renglon, "|");
				while (tokens.hasMoreElements()) {
					if (cont == 0) {    			 
						encabezado.add(tokens.nextToken().trim());
					} else {  
						mapaElementos.put(encabezado.get(index), tokens.nextToken().trim());
						index++;
					} 
				}
				cont++;
				if (cont > 1 && !mapaElementos.isEmpty()) {
					result.add(mapaElementos);
				}
			}

		} catch (IOException e) {
			LOG.error(e.getMessage(), e);
		}
		return result;
	}
}
