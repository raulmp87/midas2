package mx.com.afirme.midas2.utils;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.transaction.SystemException;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas2.dao.Dao;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoModifPrima;
import mx.com.afirme.midas2.dto.CatalogoAyuda;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.DynamicControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoControl;
import mx.com.afirme.midas2.dto.DynamicControl.TipoValidacion;
import mx.com.afirme.midas2.dto.DynamicControls;
import mx.com.afirme.midas2.dto.IdDynamicRow;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.service.componente.ComponenteService;

import org.apache.commons.beanutils.BeanUtils;
import org.apache.commons.beanutils.NestedNullException;
import org.apache.commons.lang.StringUtils;

/**
 * Clase de casteo de un bean a un objeto tipo RegistroDinamico
 * El bean a castear debe de utilizar la anotaci�n de @DynamicControl
 * @author vmhersil
 *
 */
@Stateless
public class ConvertidorImpl implements Convertidor {
	
	private ComponenteService componenteService;
	
	@EJB
	public void setComponenteService(ComponenteService componenteService) {
		this.componenteService = componenteService;
	}
	
	/**
	 * Convierte una lista de un bean que utiliza anotacion DynamicControl a 
	 * una lista de beans de tipo RegistroDinamicoDTO
	 * @param list
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public  List<RegistroDinamicoDTO> getListaRegistrosDinamicos(List list){
		return  getListaRegistrosDinamicos(list, null);
	}
	
	
	@SuppressWarnings("rawtypes")
	public  List<RegistroDinamicoDTO> getListaRegistrosDinamicos(List list, Class<?> vista){
		List<RegistroDinamicoDTO> registros=new ArrayList<RegistroDinamicoDTO>();
		if(list!=null && !list.isEmpty()){
			loop:
			for(Object o:list){
				RegistroDinamicoDTO registro=getRegistroDinamico(o, vista);
				if(registro==null){
					continue loop;
				}
				registros.add(registro);
			}
		}
		return registros;
	}
		
	/**
	 * Indica si un metodo tiene su atributo en la lista de atributos
	 * @param fields
	 * @param methodName
	 * @return
	 */
	private  boolean existInAttributes(Field[] fields,String methodName){
		String attributeName=methodName.substring("get".length(),methodName.length());
		for(Field field:fields){
			String fieldName=field.getName();
			if(attributeName.equalsIgnoreCase(fieldName)){
				return true;
			}
		}
		return false;
	}
	/**
	 * Indica si un objeto es wrapping object
	 * @param obj
	 * @return
	 */
	private  boolean isWrappingObject(Class obj){
		boolean isWrapping=false;
		if(obj==Long.class || obj==Short.class || obj==Character.class || obj==Double.class || obj==Float.class 
				|| obj==Integer.class || obj==Boolean.class || obj==Byte.class || obj==Class.class || obj==BigDecimal.class){
			isWrapping=true;
		}
		return isWrapping;
	}
	
	
	private boolean isNestedObject(Class type) {
		if(!type.isPrimitive() && !type.isArray() && !type.isInstance(new String()) && !isWrappingObject(type)
				&& !type.isInstance(new ArrayList()) && !(type.equals(java.util.Date.class))) {
			return true;
		}
		
		return false;
		
	}
	
	
	
	/**
	 * Convierte un objeto a un registro dinamico incluyendo si una propiedad tiene como metodo get el retorno de un objeto especial fuera de datos primitivos, string y wrapping
	 * @param object
	 * @return
	 */
	public  RegistroDinamicoDTO getRegistroDinamico(Object object){
		return getRegistroDinamico(object, null);
	}
	
	
	/**
	 * Convierte un objeto a un registro dinamico incluyendo si una propiedad tiene como metodo get el retorno de un objeto especial fuera de datos primitivos, string y wrapping
	 * @param object
	 * @return
	 */
	@SuppressWarnings("rawtypes")
	public  RegistroDinamicoDTO getRegistroDinamico(Object object, Class<?> vista){
		RegistroDinamicoDTO registro=new RegistroDinamicoDTO();
		if(object==null){
			return null;
		}
		Class c=object.getClass();
		Method[] methods= c.getMethods();
		//metodos getter a considerar
		List<Method> getterMethods=new ArrayList<Method>();
		//metodos que regresan objetos simples(datos primitivos o Strings)
		List<Method> metodosObjetosSimples=new ArrayList<Method>();
		if(methods!=null && methods.length>0){
			for(Method m:methods){
				String methodName=m.getName();
				if(isGetterMethod(methodName)){
					getterMethods.add(m);
				}
			}
		}
		if(getterMethods!=null && !getterMethods.isEmpty()){
			List<ControlDinamicoDTO> listaControles=new ArrayList<ControlDinamicoDTO>();
			int index=0;
			for(Method atributo:getterMethods){
				Class type=atributo.getReturnType();
				Object value=null;
				try {
					value=atributo.invoke(object,null);
					value=(value!=null)?value:type.newInstance();
				} catch (Exception e1) {
					System.out.println(e1.getMessage());
				}
				//Si es un objeto entonces obtenemos todos los controles del objeto anidado
				if(isNestedObject(type)){

					RegistroDinamicoDTO registroInterno=getRegistroDinamico(value, vista);
					if(registroInterno!=null){
						List<ControlDinamicoDTO> controles=registroInterno.getControles();
						if(controles!=null && !controles.isEmpty()){
							for(ControlDinamicoDTO control:controles){
								listaControles.add(control);
							}
						}
					}
					
					Annotation[] arrayAnnotations = atributo.getAnnotations();
					
					for(Annotation annotation : arrayAnnotations){
						if(annotation instanceof CatalogoAyuda){
							metodosObjetosSimples.add(getterMethods.get(index));
							break;
						}
					}
					
					
				}else if(type==Class.class){
					index++;
					continue;
				}else{
					metodosObjetosSimples.add(getterMethods.get(index));
				}
				index++;
			}
			if(metodosObjetosSimples!=null && !metodosObjetosSimples.isEmpty()){
				List<ControlDinamicoDTO> campos=obtenerCamposDinamicos(metodosObjetosSimples,object, vista);
				if(campos!=null && !campos.isEmpty()){
					for(ControlDinamicoDTO control:campos){
						listaControles.add(control);
					}
				}
			}
			registro.setControles(listaControles);
			String id=getIdRegistro(getterMethods, object);
			registro.setId(id);
		}
		return registro;
	}
	
	
	public Object getValorEspecifico(String atributoMapeo, Object objeto) {
		
		Class<?> c = objeto.getClass();
		Method m;
		Object value = null;
		try {
			String [] arrayAtributo = atributoMapeo.split("\\.");
			m = c.getMethod(getGetterMethodName(arrayAtributo[0]), null);
			value = m.invoke(objeto,null);
			
			if (arrayAtributo.length > 1) {				
				return getValorEspecifico(obtenerNuevoAtributoMapeo(arrayAtributo), value);
			}
			
		} catch (SecurityException e) {
		} catch (NoSuchMethodException e) {
		} catch (IllegalArgumentException e) {
		} catch (IllegalAccessException e) {
		} catch (InvocationTargetException e) {
		}
				
		return value;
	}

	
	public String obtenerNuevoAtributoMapeo(String[] arrayAtributo) {
		StringBuilder atributo = new StringBuilder("");
		if (arrayAtributo.length > 1){
				for (int i = 1; i < arrayAtributo.length; i++) {
					atributo.append( arrayAtributo[i]).append(".");
		}
		return atributo.substring(0, atributo.lastIndexOf("."));
		}
		return atributo.toString();
	}

	private String getGetterMethodName(String atributo) {
		
		return "get" + StringUtil.capitalizeFirstWord(atributo);
		
	}
	
	/**
	 * Obtiene los controles dinamicos para una anotacion de CatalogoAyuda.
	 * @param dynamicControl
	 * @return
	 */
	private  List<ControlDinamicoDTO> getControlDinamicoDTOs(CatalogoAyuda catalogoAyuda, Object root) {
		DynamicControl[] dynamicControls = getDynamicControls(catalogoAyuda);
		return getControlDinamicoDTOs(dynamicControls, catalogoAyuda, root);
	}
	
	private  List<ControlDinamicoDTO> getControlDinamicoDTOs(
			DynamicControl[] dynamicControls, CatalogoAyuda catalogoAyuda, Object root) {
		List<ControlDinamicoDTO> controlDinamicoDTOs = new ArrayList<ControlDinamicoDTO>();
		List<DynamicControlParentChild> dynamicControlParentChilds = getDynamicControlParentChilds(dynamicControls);
		//Fijo por lo pronto. Tal vez haya casos en los que no se desee y se tendra que desarrollar logica adicional 
		//para que no se considere al momento de setear los atributos mapeos.
		boolean addBase = true; 
		String atributoMapeoBase = "";
		if (addBase) {
			//La propiedad atributoMapeo del catalogoAyuda contiene el atributoMapeo base.
			atributoMapeoBase = addAttributePath(atributoMapeoBase, catalogoAyuda.atributoMapeo());
		}
		
		String atributoMapeoPadre = "";
		//Empieza a iterar de padre a hijo los controles
		for (DynamicControlParentChild dynamicControlParentChild : dynamicControlParentChilds) {
			DynamicControl dynamicControl = dynamicControlParentChild.getDynamicControl();
			//El atributo mapeo para el control actual.
			String atributoMapeoActual = addAttributePath(atributoMapeoBase, dynamicControl.atributoMapeo());
			
			List<CacheableDTO> elementosCombo = getElementosCombo(dynamicControlParentChild, catalogoAyuda, root, atributoMapeoPadre);
			String valor = getValor(root, atributoMapeoActual);
			String cascadaOnChange = getCascadaOnChange(dynamicControlParentChild, atributoMapeoActual, atributoMapeoBase);
			String onChange = appendJavaScript(dynamicControl.javaScriptOnChange(), cascadaOnChange);
			
			ControlDinamicoDTO controlDinamicoDTO = new ControlDinamicoDTO();
			populateControlDinamico(dynamicControl, controlDinamicoDTO);
			controlDinamicoDTO.setAtributoMapeo(atributoMapeoActual);
			controlDinamicoDTO.setValor(valor);
			controlDinamicoDTO.setElementosCombo(elementosCombo);
			controlDinamicoDTO.setJavaScriptOnChange(onChange);
			controlDinamicoDTOs.add(controlDinamicoDTO);

			//Para preservar el valor del atributoMapeoPadre lo asignamos a la variable.
			atributoMapeoPadre = atributoMapeoActual;
		}
		return controlDinamicoDTOs;
	}

	private  String appendJavaScript(String origJavaScript,
			String javaScript) {
		if (StringUtils.isBlank(origJavaScript)) {
			return javaScript;
		}
		int index = origJavaScript.lastIndexOf(";");
		if (!(index == (origJavaScript.length() - 1))) {
			origJavaScript += ";"; //Append the ;
		}
		return origJavaScript + javaScript;
	}


	/**
	 * Genera el JavaScript correspondiente al cascadeo, en caso de necesitarlo.
	 * @param dynamicControlParentChild
	 * @return el codigo JavaScript de cascadeo necesario para este dyamicControlParentChild.
	 */
	private  String getCascadaOnChange(
			DynamicControlParentChild dynamicControlParentChild, String atributoMapeoPadre, String atributoMapeoBase) {
		if (!dynamicControlParentChild.hasChild()) {
			return "";
		}
		String javaScriptTemplate = "cascadeoGenerico(this,'%s','%s');";
		DynamicControl child = dynamicControlParentChild.getChild();
		String atributoMapeo = addAttributePath(atributoMapeoBase, child.atributoMapeo());
		return String.format(javaScriptTemplate, child.claveCascada(), atributoMapeo);
	}
	private  List<DynamicControlParentChild> getDynamicControlParentChilds(DynamicControl[] dynamicControls) {
		List<DynamicControlParentChild> dynamicControlParentChilds = new ArrayList<ConvertidorImpl.DynamicControlParentChild>();
		for (int i = 0; i < dynamicControls.length; i++) {
			DynamicControl dynamicControl = dynamicControls[i];
			DynamicControl parent = getParent(i, dynamicControls);
			DynamicControl child = getChild(i, dynamicControls);
			dynamicControlParentChilds.add(new DynamicControlParentChild(dynamicControl, parent, child));
		}
		return dynamicControlParentChilds;		
	}
	
	private  String getValor(Object root, String atributoMapeo) {
		String valor = null;
		try {
			valor = BeanUtils.getProperty(root, atributoMapeo);
		} catch(NestedNullException e)  {
			//Nada que hacer solo regresara null.
		} catch (IllegalAccessException e) {
			throw new RuntimeException(e);
		} catch (InvocationTargetException e) {
			throw new RuntimeException(e);
		} catch (NoSuchMethodException e) {
			throw new RuntimeException(e);
		}
		return valor;
	}
		
	private  List<CacheableDTO> getElementosCombo(DynamicControlParentChild dynamicControlParentChild, CatalogoAyuda catalogoAyuda,
			Object root, String atributoMapeoPadre) {
		List<CacheableDTO> elementosCombo = new ArrayList<CacheableDTO>();
		DynamicControl dynamicControl = dynamicControlParentChild.getDynamicControl();
		if (dynamicControlParentChild.hasParent()) {
			//Obtener el valor del padre para poder obtener la lista del hijo.
			Object valorPadre = getValor(root, atributoMapeoPadre);
			if (valorPadre != null) {
				//Tiene valor por lo tanto podemos llenar la lista.
				String clave = dynamicControl.claveCascada() + "_" + valorPadre;
				elementosCombo = componenteService.getListadoRegistros(clave);
			}			
		} else {
			String clave = dynamicControl.claveCascada();
			elementosCombo = componenteService.getListadoRegistros(clave);
		}
		return elementosCombo;
	}

	private  DynamicControl getParent(int arrayIndex, DynamicControl[] dynamicControls) {
		if (arrayIndex != 0) {
			return dynamicControls[arrayIndex-1];
		}
		return null;
	}
	
	private  DynamicControl getChild(int arrayIndex, DynamicControl[] dynamicControls) {
		if (arrayIndex != dynamicControls.length - 1) {
			return dynamicControls[arrayIndex+1];
		}
		return null;
	}
	
	private  class DynamicControlParentChild {
		private DynamicControl dynamicControl;
		private DynamicControl parent;
		private DynamicControl child;
	
		public DynamicControlParentChild(DynamicControl dynamicControl, DynamicControl parent, DynamicControl child) {
			this.dynamicControl = dynamicControl;
			this.parent = parent;
			this.child = child;
		}
		
		public DynamicControl getDynamicControl() {
			return dynamicControl;
		}
		
		public DynamicControl getParent() {
			return parent;
		}
		
		public DynamicControl getChild() {
			return child;
		}
		
		public boolean hasChild() {
			return child != null;
		}
		
		public boolean hasParent() {
			return parent != null;
		}

	}
	private  String addAttributePath(String path, String attributeName) {
		if (!StringUtils.isBlank(path)) {
			path += ".";
		}
		return path + attributeName; 
		
	}
	
	
	
	private  DynamicControl[] getDynamicControls(CatalogoAyuda catalogoAyuda) {
		Class<?> nombre = catalogoAyuda.nombre();
		DynamicControls dynamicControlsAnnotation = nombre.getAnnotation(DynamicControls.class);
		if (dynamicControlsAnnotation == null) {
			throw new RuntimeException("El nombre especificado en el Cata\u00e1logo Ayuda " + 
					nombre.getCanonicalName() + " no tiene la anotaci\u00f3n DynamicControls.");
		}
		return dynamicControlsAnnotation.value();		
	}
	
	/**
	 * Obtiene la lista de atributos casteados a ControlDinamicoDTO
	 * @param getterMethods
	 * @return
	 */
	private  List<ControlDinamicoDTO> obtenerCamposDinamicos(List<Method> getterMethods,Object object, Class<?> vista){
		List<ControlDinamicoDTO> controles=new ArrayList<ControlDinamicoDTO>();
		List<ControlDinamicoDTO> controlesAyudaAnidados =new ArrayList<ControlDinamicoDTO>();
		ControlDinamicoDTO control = null;
		
		for(Method m:getterMethods){
			
			for(Annotation a:m.getAnnotations()){
				if(a instanceof DynamicControl){
					
					DynamicControl info = (DynamicControl) a;
					
					if (!contieneVistaSolicitada(info.vistas(), vista)) {
						break;
					}
					
					control=new ControlDinamicoDTO();
					
					populateControlDinamico(info, control);
					String className=info.className();
					Object value=null;
					try {
						value=m.invoke(object, null);
					} catch (Exception e1) {
						System.out.println(e1.getMessage());
					}
					if(value!=null){
						control.setValor(value.toString());
					}
					if(className!=null && !className.trim().isEmpty() && (TipoControl.SELECT.equals(info.tipoControl())||TipoControl.RADIOBUTTON.equals(info.tipoControl()))){
						try {
							Object bean=ServiceLocatorP.getInstance().getEJB(className);
							List<CacheableDTO> lista=null;
							if(bean instanceof MidasInterfaceBase){
								MidasInterfaceBase castingBean=(MidasInterfaceBase)bean;
								lista=castingBean.findAll();
							}else if(bean instanceof Dao){
								Dao castingBean=(Dao)bean;
								lista=castingBean.findAll();
							}
							if(lista!=null && !lista.isEmpty()){
								control.setElementosCombo(lista);
							}
						} catch (SystemException e) {
							System.out.println(e.getMessage());
						} catch (ClassNotFoundException e) {
							System.out.println(e.getMessage());
						}
					}
					controles.add(control);
					break;
				} else if (a instanceof CatalogoAyuda) { //Se asume que cuando esta definida esta anotacion no debe existir una de DynamicControl
					
					CatalogoAyuda info = (CatalogoAyuda) a;
					
					if (!contieneVistaSolicitada(info.vistas(), vista)) {
						break;
					}
					
					controlesAyudaAnidados = getControlDinamicoDTOs(info, object);
					
					for (ControlDinamicoDTO controlAyudaAnidado :  controlesAyudaAnidados) {
						controles.add(controlAyudaAnidado);
					}
					
					break;
				}
			}
			
		}
		return controles;
	}
	
	private  void populateControlDinamico(DynamicControl dynamicControl, ControlDinamicoDTO controlDinamicoDTO) {
		controlDinamicoDTO.setAtributoMapeo(dynamicControl.atributoMapeo());
		controlDinamicoDTO.setDescripcionValidacion(dynamicControl.descripcionValidacion());
		controlDinamicoDTO.setEditable(dynamicControl.editable());
		controlDinamicoDTO.setEsComponenteId(dynamicControl.esComponenteId());
		controlDinamicoDTO.setEsNumerico(dynamicControl.esNumerico());
		controlDinamicoDTO.setEsValido(dynamicControl.esValido());
		controlDinamicoDTO.setEtiqueta(dynamicControl.etiqueta());
		controlDinamicoDTO.setIdControlHijo(dynamicControl.idControlHijo());
		controlDinamicoDTO.setIdControlPadre(dynamicControl.idControlPadre());
		controlDinamicoDTO.setJavaScriptOnChange(dynamicControl.javaScriptOnChange());
		controlDinamicoDTO.setPermiteNulo(dynamicControl.permiteNulo());
		controlDinamicoDTO.setJavaScriptOnFocus(dynamicControl.javaScriptOnFocus());
		String sec=(!StringUtils.isBlank(dynamicControl.secuencia()))?dynamicControl.secuencia():"0";
		Integer secuencia=Integer.parseInt(sec);
		controlDinamicoDTO.setSecuencia(secuencia);
		controlDinamicoDTO.setTipoControl(dynamicControl.tipoControl().getValue());
		controlDinamicoDTO.setJavaScriptOnKeyPress(getJavaScriptOnKeyPressEvent(dynamicControl.tipoValidacion()));
		controlDinamicoDTO.setLongitudMaxima(dynamicControl.longitudMaxima());
	}
	
	/**
	 * Obtiene el codigo JavaScript para validar en el evento OnKeyPress dependiendo del TipoValidacion requerido
	 * @param tipoValidacion
	 * @return
	 */
	private String getJavaScriptOnKeyPressEvent(TipoValidacion tipoValidacion) {
		
		String javaScriptOnKeyPress = "";
		
		
		switch(tipoValidacion){
		case NUM_DECIMAL:
			javaScriptOnKeyPress = "return soloNumeros(this,event,true)";
			break;
		case NUM_ENTERO:
			javaScriptOnKeyPress = "return soloNumerosM2(this,event,false)";
			break;
		case ALFANUMERICO:
			javaScriptOnKeyPress = "return soloAlfanumericos(this, event, false)";
			break;
		}
				
		return javaScriptOnKeyPress;
		
	}
	
	
	/**
	 * Obtiene el id de un registro buscando dentro de sus atributos aquel que tenga la anotacion de IdDynamicRow
	 * @param getterMethods
	 * @param object
	 * @return
	 */
	private  String getIdRegistro(List<Method> getterMethods,Object object){
		String id=null;
		loop1:
		for(Method m:getterMethods){
			Annotation[] annotations=m.getAnnotations();
			if(annotations!=null && annotations.length>0){
				loop2:
				for(Annotation a:annotations){
					if(a instanceof IdDynamicRow){
						Object value=null;
						try {
							value=m.invoke(object, null);
							if(value!=null){
								id=value.toString();
								break loop1;
							}
						} catch (Exception e) {
							System.out.println(e.getMessage());
						}
					}
				}
			}
		}
		return id;
	}
	
	
	private  Boolean contieneVistaSolicitada (Class<?>[] grupos, Class<?> vista) {
		
		if (vista == null) { //Significa que se solicita la vista "Default"
			
			if (grupos.length == 0) {
				return true;
			}
			
			vista =javax.validation.groups.Default.class;
		}
		
		for (Class<?> grupo: grupos) {
			if (grupo.equals(vista)) {
				return true;
			}
		}
		
		return false;
	}
	
	
	private  void p(String msg){System.out.println(msg);}
	/**
	 * Metodo de prueba para imprimir atributos de cada registro
	 * @param rows
	 */
	public void printRows(List<RegistroDinamicoDTO> rows){
		int count=1;
		for(RegistroDinamicoDTO row:rows){
			p("===Registro "+(count++)+"=====================");
			p("<<<<<<<<<<<Campos>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
			List<ControlDinamicoDTO> controles=row.getControles();
			if(controles!=null && !controles.isEmpty()){
				p("Atributo\tValor");
				for(ControlDinamicoDTO control:controles){
					p("---Campo:"+control.getAtributoMapeo()+"-----------------------");
					p("Atributo Mapeo"+"\t"+control.getAtributoMapeo());
					p("DescripcionValidacion"+"\t"+control.getDescripcionValidacion());
					p("Etiqueta"+"\t"+control.getEtiqueta());
					p("IdControlHijo"+"\t"+control.getIdControlHijo());
					p("IdControlPadre"+"\t"+control.getIdControlPadre());
					p("JavaScriptOnChange"+"\t"+control.getJavaScriptOnChange());
					p("Valor"+"\t"+control.getValor());
					p("Secuencia"+"\t"+control.getSecuencia());
					p("TipoControl"+"\t"+control.getTipoControl());
					p("Editable"+"\t"+control.getEditable());
					p("EsComponenteId"+"\t"+control.getEsComponenteId());
					p("EsNumerico"+"\t"+control.getEsNumerico());
					p("EsValido"+"\t"+control.getEsValido());
					p("PermiteNulo"+"\t"+control.getPermiteNulo());
				}
			}
			
		}
	}
	/**
	 * Dada una lista de controles, un atributo a actualizar con su valor, se le manda uno o mas strings que representan el atributo de mapeo a 
	 * actualizar. Ejemplo, supnga que tiene una lista de controles dinamicos donde sus atributos de mapeo sean: nombre,edad,apellido,sexo.
	 * Si deseo actualizar el atributo "editable" a false y sobre nombre y apellido, para ello mandamos lo siguiente:
	 * String[] camposActualizar={"nombre","apellido"};
	 * setAttribute("editable",false,controles,camposActualizar);
	 * 
	 * Y nuestra anotacion ser�a en 2 atributos de nuestra entidad(atributo nombre y apellido) cuyo atributoMapeo="nombre", atributoMapeo="apellido"
	 * @param attribute
	 * @param value
	 * @param controles
	 * @param atributosMapeo
	 */
	public  void setAttribute(String attribute,Object value,List<ControlDinamicoDTO> controles,String... atributosMapeo){
		List<ControlDinamicoDTO> controlsToUpdate=new ArrayList<ControlDinamicoDTO>();
		if((atributosMapeo!=null && atributosMapeo.length>0) && (controles!=null && !controles.isEmpty()) ){
			List<String> atributos=Arrays.asList(atributosMapeo);
			for(ControlDinamicoDTO control:controles){
				String attr=control.getAtributoMapeo();
				if(attr!=null && !attr.isEmpty()){
					if(atributos.contains(attr)){
						controlsToUpdate.add(control);
					}
				}
			}
		}
		setAttribute(attribute, value, controlsToUpdate);
	}
	
	/**
	 * Asigna un atributo a uno o m�s controles
	 * @param attribute
	 * @param value
	 * @param controles
	 */
	public  void setAttribute(String attribute,Object value,ControlDinamicoDTO... controles){
		List<ControlDinamicoDTO> controls=Arrays.asList(controles);
		setAttribute(attribute, value, controls);
	}
	
	/**
	 * Asigna valor a un atributo para uno o mas controles
	 * @param attribute
	 * @param value
	 * @param controles
	 */
	public  void setAttribute(String attribute,Object value,List<ControlDinamicoDTO> controles){
		if(attribute!=null && !attribute.trim().isEmpty() && value!=null && controles!=null && controles.size()>0){ 
			Class<ControlDinamicoDTO> c=ControlDinamicoDTO.class;
			Method[] methods=c.getMethods();
			String setterMethod="set"+StringUtils.capitalize(attribute);
			Method setter=null;
			findingSetterMethod:
			for(Method m:methods){
				String currentMethod=m.getName();
				if(setterMethod.equals(currentMethod)){
					setter=m;
					break findingSetterMethod;
				}
			}
			if(setter!=null){
				for(ControlDinamicoDTO control:controles){
					String campo=control.getAtributoMapeo();
					try {
						setter.invoke(control, value);
					} catch (Exception e) {
						p("No se actualiz\u00f3 el atributo "+attribute+" con el valor "+value+" del campo:"+campo);
					}
				}
			}else{
				p("No se actualiz\u00f3 el atributo "+attribute+" con el valor "+value);
			}
		}
	}
	
	/**
	 * Indica si un metodo es get
	 * @param name
	 * @return
	 */
	private  boolean isGetterMethod(String name){
		if(name.startsWith("get")){
			return true;
		}
		return false;
	}
	/**
	 * How to use
	 * @param args
	 */
	@SuppressWarnings("static-access")
	public static void main(String[] args){
		ConvertidorImpl conv=new ConvertidorImpl();
		List<TarifaAutoModifPrima> lista=new ArrayList<TarifaAutoModifPrima>();
		TarifaAutoModifPrima tarifa1=new TarifaAutoModifPrima();
		tarifa1.setValor(BigDecimal.valueOf(10.5d));
		TarifaAutoModifPrima tarifa2=new TarifaAutoModifPrima();
		tarifa2.setValor(BigDecimal.valueOf(2d));
		tarifa2.setIdExterno(10l);
		lista.add(tarifa1);
		lista.add(tarifa2);
		List<RegistroDinamicoDTO> registros=conv.getListaRegistrosDinamicos(lista);
		RegistroDinamicoDTO reg=registros.get(0);
		List<ControlDinamicoDTO> controles=reg.getControles();
		conv.setAttribute("editable",true, controles);
		conv.printRows(registros);
	}

	
	
	
}
