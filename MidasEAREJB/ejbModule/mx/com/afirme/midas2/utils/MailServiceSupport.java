package mx.com.afirme.midas2.utils;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.domain.notificaciones.DestinatariosNotificaciones;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;

import org.apache.commons.lang.StringUtils;

public class MailServiceSupport {
	private static final String PGO_COMISIONES_AUTORIZACIONMOVIEMIENTO = "<p>Estimado(a) :nombreAgente :<br /> Le notificamos que se ha registrado la instrucci&oacute;n para realizar un dep&oacute;sito a su cuenta desde Afirme por Internet.<br /> Operaci&oacute;n: Dep&oacute;sito a su cuenta<br /> Nombre del Remitente: :nombreAgente <br /> Importe: $ :cantidadDepositada <br /> Operado el d&iacute;a Fecha en que se envi&oacute; :today <br /> Por concepto de Pago de Comisiones<br /> Correspondiente al periodo: :anioMes </p>";
	private static final String MA_PGO_COMISIONES_GENERACION_PREVIEW = "<p>Estimado(a) Administrador de Agentes :<br /> Le notificamos que han sido generado el siguiente Preview de pagos de comisiones</p> <p> Importe: $ :cantidadDepositada <br /> Operado :today<br /> Por concepto de Pago de Comisiones<br /> Correspondiente al periodo: :anioMes</p>";
	private static final String PGO_BONOS_AUTORIZACIONMOVIEMIENTO = "<p>Estimado(a) :nombreAgente :<br /> Le notificamos que se ha registrado la instrucci&oacute;n para realizar un dep&oacute;sito a su cuenta desde Afirme por Internet.<br /> Operaci&oacute;n: Dep&oacute;sito a su cuenta<br /> Nombre del Remitente: :nombreAgente <br /> Importe: $ :cantidadDepositada <br /> Operado el d&iacute;a :today <br /> Por concepto de Pago de Bono correspondiente al mes :mesAnio </p>"; 
	private static final String MA_PGO_BONOS_AUTORIZACIONMOVIEMIENTO = "<p> Estimado(a) Administrador de Agentes :<br /> Le notificamos que han sido registrados los siguientes pagos de bonos</p> <p> Importe: $ :totalImporteDepositada <br /> Operado el d&iacute;a Fecha en que se envi&oacute; :today <br /> Por concepto de Pago de Bonos<br /> Correspondiente al periodo: :anioMes </p>"; 
	private static final String MA_PGO_BONOS_GENERACION_PREVIEW = "<p>Estimado(a) Administrador de Agentes :<br /> Le notificamos que han sido generado el siguiente Preview de pagos de bonos</p> <p> Importe: $ :totalImporteDepositada <br /> Operado el d&iacute;a Fecha en que se envi&oacute; :today <br /> Por concepto de Pago de Bonos<br /> Correspondiente al periodo: :anioMes </p>"; 
	private static final String MA_DOCTO_POR_VENCER = "<p>Estimado(a) :nombreAgente :<br /> Le informamos que su cedula esta pr&oacute;xima a vencer, si ya cuenta con su renovaci&oacute;n es necesario la mande v&iacute;a correo electr&oacute;nico a las siguientes direcciones:<br>Rocio.jasso@afirme.com y Ricardo.alvarado@afirme.com.</p>";
	private static final String MA_DOCTO_VENCIDO = "<p>Estimado(a) :nombreAgente :<br /> Le informamos que su cedula esta vencida, si ya cuenta con su renovaci&oacute;n es necesario la mande v&iacute;a correo electr&oacute;nico a las siguientes direcciones: <br>Rocio.jasso@afirme.com y Ricardo.alvarado@afirme.com</p>";
	private static final String MA_PGO_COMISIONES_AUTORIZACIONMOVIEMIENTO = "Estimado(a) Administrador de Agentes : Le notificamos que han sido registrados los siguientes pagos de comisiones Importe: $ :cantidadDepositada Operado :today Por concepto de Pago de Comisiones Correspondiente al periodo: :anioMes";
	private static final String PA_ALTA_REGISTRO ="<p>Estimado(a) :nombreAgente :<br /> Le informamos que la solicitud de pr&eacute;stamo que present&oacute; a Seguros Afirme, S.A. de .C.V, fue registrada, el importe de $ :importeSolicitado fue el d&iacute;a :today . <br /> En un momento le estaremos notificando si ha sido Autorizada.</p> <p>Cualquier duda o aclaraci&oacute;n</p>";
	private static final String PA_AUTORIZACIONMOVIMIENTO ="<p>Estimado(a):nombreAgente :<br /> Le informamos que la solicitud de pr&eacute;stamo que present&oacute; a Seguros Afirme, S.A. de .C.V, fue autorizada, el importe de $ :importeSolicitado fue el d&iacute;a :today . Le recordamos que el pagar en tiempo evita intereses moratorios.</p> <p>Cualquier duda o aclaraci&oacute;n</p>";
	private static final String PA_RECHAZOMOVIMIENTO ="<p>Estimado(a):nombreAgente :<br /> Le informamos que la solicitud de pr&eacute;stamo que present&oacute; a Seguros Afirme, S.A. de .C.V, fue rechazada, el importe de $ :importeSolicitado fue el d&iacute;a :today . </p>";
	private static final String FIANZA_VENCIDA = "<p>Estimado(a) :nombreAgente :<br /> Le informamos que su fianza esta vencida, la renovaci&oacute;n empezar&aacute; el d&iacute;a 1 de junio y el cobro se le har&aacute; durante el mes de julio.</p>";
	private static final String FIANZA_POR_VENCER = "<p>Estimado(a) :nombreAgente :<br /> <p>Le informamos que su fianza esta pr&oacute;xima a vencer, la renovaci&oacute;n empezar&aacute; el d&iacute;a 1 de junio y el cobro se le har&aacute; durante el mes de julio.</p>";
	private static final String PROVISIONES_GENERACION_PREVIEW = "<p>Estimado(a) Administrador :<br /> Le informamos que el d&iacute;a de hoy se realiz&oacute; la provisi&oacute;n correspondiente a </p> <p>&bull; Producci&oacute;n<br /> &bull; Producci&oacute;n trimestral<br /> &bull; Siniestralidad<br /> &bull; Convenci&oacute;n<br /> &bull; Negociaci&oacute;n Especial<br /> &bull; Emisi&oacute;n Delegada<br /> &bull; Concurso</p> <p>Quedando pendiente la Autorizaci&oacute;n.<br /> </p>";
	private static final String PROVISIONES_AUTORIZACIONMOVIEMIENTO = "<p>PROVISION</p> <p>Estimado(a) Contador :<br /> Le informamos que el d&iacute;a de hoy se Autoriz&oacute; la provisi&oacute;n correspondiente a </p> <p>&bull; Producci&oacute;n<br /> &bull; Producci&oacute;n trimestral<br /> &bull; Siniestralidad<br /> &bull; Convenci&oacute;n<br /> &bull; Negociaci&oacute;n Especial<br /> &bull; Emisi&oacute;n Delegada<br /> &bull; Concurso</p> <p>Del periodo comprendido en :anioMes<br /> Cualquier duda o aclaraci&oacute;n quedamos a sus ordenes.</p> <p>Coordinaci&oacute;n Administraci&oacute;n de Agentes</p>";
	private static final String AGENTE_ESTADO_DE_CUENTA ="<p>ESTADO DE CUENTA</p> <p>Estimado (a) :nombreAgente.-<br /> Por este medio hacemos llegar Estado de Cuenta correspondiente al :anioMes para que pueda realizar los tr&aacute;mites necesarios a ello.</p> <p>Reiteramos nuestro apoyo quedando a sus &oacute;rdenes para cualquier duda o sugerencia al respecto.</p> <p>Seguros Afirme agradece su atenci&oacute;n, saludos cordiales.</p> <p>Para cualquier aclaraci&oacute;n y/o duda favor de enviar correo El ejecutivo correspondiente</p>";
	private static final String AGENTE_DETALLE_DE_PRIMAS ="<p>DETALLE DE PRIMAS</p> <p>Estimado (a) :nombreAgente.-<br /> Por este medio hacemos llegar Detalle de Primas correspondiente al :anioMes para que pueda realizar los tr&aacute;mites necesarios a ello.</p> <p>Reiteramos nuestro apoyo quedando a sus &oacute;rdenes para cualquier duda o sugerencia al respecto.</p> <p>Seguros Afirme agradece su atenci&oacute;n, saludos cordiales.</p> <p>Para cualquier aclaraci&oacute;n y/o duda favor de enviar correo El ejecutivo correspondiente</p>";
	private static final String AGENTE_RECIBO_HONORARIOS ="<p>RECIBO DE HONORARIOS</p> <p>Estimado (a) :nombreAgente.-<br /> Por este medio hacemos llegar el Recibo de Honorarios correspondiente al :anioMes para que pueda realizar los tr&aacute;mites necesarios a ello.</p> <p>Reiteramos nuestro apoyo quedando a sus &oacute;rdenes para cualquier duda o sugerencia al respecto.</p> <p>Seguros Afirme agradece su atenci&oacute;n, saludos cordiales.</p> <p>Para cualquier aclaraci&oacute;n y/o duda favor de enviar correo El ejecutivo correspondiente</p>";
	private static final String MA_RECHAZO_ALTA_AGENTE = "<p>Estimado(a) Agente <br /> Le informamos que ha sido rechazado</p>";
	private static final String MA_AUTORIZACION_ALTA_AGENTE = "<p>Estimado(a) Agente <br /> Le informamos que ha sido autorizado</p>";
	private static final String CAMBIO_ESTATUS_AGENTES="<p>Estimado(a) :nombreAgente <br>Le informamos que su estatus ha sido cambiado.<br>Para cualquier aclaración y/o duda favor de enviar correo El ejecutivo correspondiente</p>";
	private static final String MA_ALTA_AGENTE="<p>Buen día, <br>nos complace comunicarle que el agente con clave :clave de nombre :nombre se ha dado de alta en nuestros sistemas, aun está pendiente de su autorización</p>";
	public static final String[] monthsArray = new DateFormatSymbols(new Locale("es","MX")).getMonths();
	
	/**
	 * Clasifica los correos de la configuracion de pendiendo el tipo de envio
	 * 
	 * @param persona
	 * @param destinatario
	 * @param modoEnvio
	 * @param tipoDestinatario
	 * @param para
	 * @param cc
	 * @param cco
	 * @author martin
	 */
	public static void clasificaCorreo(Persona persona,
			DestinatariosNotificaciones destinatario,
			Map<Long, String> modoEnvio, List<String> para, List<String> cc,
			List<String> cco) {
		if (GenericMailService.PARA.equals(modoEnvio.get(destinatario.getIdModoEnvio().getId()))) {
			if (persona != null && StringUtils.isNotBlank(persona.getEmail())) {
				para.add(persona.getEmail());
			} else if (persona != null
					&& StringUtils.isNotBlank(persona.getCorreosAdicionales())) {
				agregaCorreosAdicionales(para, persona.getCorreosAdicionales()
						.split(";"));
			}
		} else if (GenericMailService.COPIA.equals(modoEnvio.get(destinatario.getIdModoEnvio()
				.getId()))) {
			if (persona != null && StringUtils.isNotBlank(persona.getEmail())) {
				cc.add(persona.getEmail());
			} else if (persona != null
					&& StringUtils.isNotBlank(persona.getCorreosAdicionales())) {
				agregaCorreosAdicionales(cc, persona.getCorreosAdicionales()
						.split(";"));
			}
		} else if (GenericMailService.COPIA_OCULTA.equals(modoEnvio.get(destinatario.getIdModoEnvio()
				.getId()))) {
			if (persona != null && StringUtils.isNotBlank(persona.getEmail())) {
				cco.add(persona.getEmail());
			} else if (persona != null
					&& StringUtils.isNotBlank(persona.getCorreosAdicionales())) {
				agregaCorreosAdicionales(cco, persona.getCorreosAdicionales()
						.split(";"));
			}
		}
	}

	/**
	 * Agrega los correos Adicionales en caso de que no exista un correo en el
	 * objeto persona
	 * 
	 * @param correoList
	 * @param correosAdicionales
	 * @autor martin
	 */
	public static void agregaCorreosAdicionales(List<String> correoList,
			String[] correosAdicionales) {
		for (String correo : correosAdicionales) {
			correoList.add(correo);
		}
	}

	/**
	 * Obtiene el correo de un agente
	 * de no tener regresa una lista con los correos adicionales
	 * @param persona
	 * @return
	 * @autor martin
	 */
	public static List<String> getCorreoAgente(Persona persona) {
		List<String> correoList = null;
		if (persona != null && StringUtils.isNotBlank(persona.getEmail())) {
			correoList = new ArrayList<String>();
			correoList.add(persona.getEmail());
		} else if (persona != null
				&& StringUtils.isNotBlank(persona.getCorreosAdicionales())) {
			correoList = new ArrayList<String>();
			agregaCorreosAdicionales(correoList, persona.getCorreosAdicionales()
					.split(";"));
		}
		return correoList;
	}
	
	/**
	 * Retorna el mensaje personalizado de pago de comisiones
	 * @param nombreAgente
	 * @param cantidadDepositada
	 * @return
	 * @autor martin
	 */
	public static String mensajePagoComisionesAutorizacionMovimiento(
			String nombreAgente, String cantidadDepositada) {
		String mensaje = new String(PGO_COMISIONES_AUTORIZACIONMOVIEMIENTO);
		mensaje = mensaje.replaceAll(":nombreAgente",eliminaAcentos(nombreAgente));
		mensaje = mensaje.replaceAll(":cantidadDepositada", formatoMiles( new BigDecimal(cantidadDepositada)));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":anioMes", Calendar.getInstance().get(Calendar.YEAR) + " " + monthsArray[Calendar.getInstance().get(Calendar.MONTH)]);
		return mensaje;
	}
	
	public static String mensajeAdminAgentePagoComisionesAutorizacion(String cantidadDepositada) {
		String mensaje = new String(MA_PGO_COMISIONES_AUTORIZACIONMOVIEMIENTO);
		mensaje = mensaje.replaceAll(":cantidadDepositada", formatoMiles( new BigDecimal(cantidadDepositada)));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":anioMes", Calendar.getInstance().get(Calendar.YEAR) + " " + monthsArray[Calendar.getInstance().get(Calendar.MONTH)]);
		return mensaje;
	}

	public static String mensajeAdminAgentePagoComisionesPreview(String cantidadDepositada) {
		String mensaje = new String(MA_PGO_COMISIONES_GENERACION_PREVIEW);
		mensaje = mensaje.replaceAll(":cantidadDepositada", formatoMiles( new BigDecimal(cantidadDepositada)));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":anioMes", Calendar.getInstance().get(Calendar.YEAR) + " " +  monthsArray[Calendar.getInstance().get(Calendar.MONTH)]);
		return mensaje;
	}
	/**
	 * Retorna el mensaje personalizado de pago de bonos
	 * @param nombreAgente
	 * @param cantidadDepositada
	 * @return
	 * @autor martin
	 */
	public static String mensajePagoBonosAutorizacionMovimiento(String nombreAgente,String cantidadDepositada,String mes,String anio,List<String> nota){
		String mensaje = new String(PGO_BONOS_AUTORIZACIONMOVIEMIENTO);
		mensaje = mensaje.replaceAll(":nombreAgente",  eliminaAcentos(nombreAgente));
		mensaje = mensaje.replaceAll(":cantidadDepositada", formatoMiles( new BigDecimal(cantidadDepositada)));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":mesAnio",
				monthsArray[Calendar.getInstance().get(Integer.parseInt(mes))] + " "
						+ anio);
		if (nota != null && !nota.isEmpty() && !nota.equals("")) {
			mensaje = mensaje +"<br/>Nota:<br/>"+ nota.get(0);
		}
		return mensaje.replace(" null ", " ");
	}
	/**
	 * Retorna el mensaje personalizado que se envia
	 * al administrador de agentes
	 * cuando se genera un movimiento de autorizacion
	 * @param cantidadDepositada
	 * @return
	 */
	public static String mensajeAdminAgentePagoBono(String cantidadDepositada){
		String mensaje = new String(MA_PGO_BONOS_AUTORIZACIONMOVIEMIENTO);
		mensaje = mensaje.replaceAll(":totalImporteDepositada", formatoMiles( new BigDecimal(cantidadDepositada)));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":anioMes", Calendar.getInstance().get(Calendar.YEAR) + " " + monthsArray[Calendar.getInstance().get(Calendar.MONTH)]);
		return mensaje;
	}

	public static String mensajeAdminAgentePagoBonoGeneraPreview(String cantidadDepositada){
		String mensaje = new String(MA_PGO_BONOS_GENERACION_PREVIEW);
		mensaje = mensaje.replaceAll(":totalImporteDepositada", formatoMiles( new BigDecimal(cantidadDepositada)));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":anioMes", Calendar.getInstance().get(Calendar.YEAR) + " " + monthsArray[Calendar.getInstance().get(Calendar.MONTH)]);
		return mensaje;
	}
	
	public static String mensajeAgenteDocumentoPorVencer(String nombreAgente) {
		String mensaje = new String(MA_DOCTO_POR_VENCER);
		mensaje = mensaje.replace(":nombreAgente", eliminaAcentos(nombreAgente));
		return mensaje;
	}

	public static String mensajeAgenteDocumentoVencido(String nombreAgente) {
		String mensaje = new String(MA_DOCTO_VENCIDO);
		mensaje = mensaje.replace(":nombreAgente",  eliminaAcentos(nombreAgente));
		return mensaje;
	}
	
	public static String mensajePrestamoAnticipoAltaRegistro(String nombreAgente, Double importe){
		String mensaje = new String(PA_ALTA_REGISTRO);
		mensaje = mensaje.replace(":nombreAgente",  eliminaAcentos(nombreAgente));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":importeSolicitado", importe.toString());
		return mensaje;
	}

	public static String mensajePrestamoAnticipoAltaAutorizacionMov(String nombreAgente, Double importe){
		String mensaje = new String(PA_AUTORIZACIONMOVIMIENTO);
		mensaje = mensaje.replace(":nombreAgente",  eliminaAcentos(nombreAgente));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":importeSolicitado", importe.toString());
		return mensaje;
	}
	public static String mensajePrestamoAnticiporechazoMov(String nombreAgente, Double importe){
		String mensaje = new String(PA_RECHAZOMOVIMIENTO);
		mensaje = mensaje.replace(":nombreAgente",  eliminaAcentos(nombreAgente));
		mensaje = mensaje.replaceAll(":today", Utilerias.cadenaDeFecha(Calendar.getInstance().getTime(), "dd/MM/yyyy"));
		mensaje = mensaje.replaceAll(":importeSolicitado", importe.toString());
		return mensaje;
	}
	public static String mensajeFianzaVencida(String nombreAgente) {
		String mensaje = new String(FIANZA_VENCIDA);
		mensaje = mensaje.replace(":nombreAgente", eliminaAcentos(nombreAgente));
		return mensaje;
	}
	public static String mensajeFianzaPorVencer(String nombreAgente) {
		String mensaje = new String(FIANZA_POR_VENCER);
		mensaje = mensaje.replace(":nombreAgente", eliminaAcentos(nombreAgente));
		return mensaje;
	}
	public static String mensajeProvisionesGeneracionPreview(){
		return PROVISIONES_GENERACION_PREVIEW;
	}
	
	public static String mensajeProvisionesAutorizacionMov(){
		String mensaje = new String (PROVISIONES_AUTORIZACIONMOVIEMIENTO);
		mensaje = mensaje.replace(":anioMes",
				"" + Calendar.getInstance().get(Calendar.YEAR) + " "
						+ monthsArray[Calendar.getInstance().get(Calendar.MONTH)]);
		return mensaje;
	}
	
	public static String mensajeAgenteEstadoDeCuenta(String nombreAgente, Integer anio, Integer mes){
//		return AGENTE_ESTADO_DE_CUENTA;
		Calendar calendario = GregorianCalendar.getInstance();
		calendario.set(anio, mes-1, 1);
		String mensaje = new String (AGENTE_ESTADO_DE_CUENTA);
		mensaje = mensaje.replace(":nombreAgente", eliminaAcentos(nombreAgente));
		mensaje = mensaje.replace(":anioMes",calendario.get(calendario.YEAR) + " "
						+ monthsArray[calendario.get(calendario.MONTH)]);
//		System.out.println(mensaje);
		return mensaje;
	}
	
	public static String mensajeAgenteDetallePrimas(String nombreAgente, Integer anio, Integer mes){
//		return AGENTE_DETALLE_DE_PRIMAS;
		Calendar calendario = GregorianCalendar.getInstance();
		calendario.set(anio, mes-1, 1);
		String mensaje = new String (AGENTE_DETALLE_DE_PRIMAS);
		mensaje = mensaje.replace(":nombreAgente", eliminaAcentos(nombreAgente));
		mensaje = mensaje.replace(":anioMes",calendario.get(calendario.YEAR) + " "
						+ monthsArray[calendario.get(calendario.MONTH)]);
//		System.out.println(mensaje);
		return mensaje;
	}
	
	public static String mensajeAgenteReciboHonorarios(String nombreAgente, Integer anio, Integer mes){
//		return AGENTE_RECIBO_HONORARIOS;
		Calendar calendario = GregorianCalendar.getInstance();
		calendario.set(anio, mes-1, 1);
		String mensaje = new String (AGENTE_RECIBO_HONORARIOS);
		mensaje = mensaje.replace(":nombreAgente", eliminaAcentos(nombreAgente));
		mensaje = mensaje.replace(":anioMes",calendario.get(calendario.YEAR) + " "
						+ monthsArray[calendario.get(calendario.MONTH)]);
//		System.out.println(mensaje);
		return mensaje;
	}
	public static String mensajeAltaAgente(String nombreAgente, Long claveAgente){
		
		String mensaje = new String (MA_ALTA_AGENTE);
		mensaje = mensaje.replace(":clave", claveAgente.toString());
		mensaje = mensaje.replace(":nombre", eliminaAcentos(nombreAgente));
		return mensaje;
	}
	
	public static String mensajeAgenteAltaRechazo() {
		return MA_RECHAZO_ALTA_AGENTE;
	}
	
	public static String mensajeAgenteAltaAutorizacion() {
		return MA_AUTORIZACION_ALTA_AGENTE;
	}
	public static String mensajeCambioStatus(String agente){
		String mensaje = new String(CAMBIO_ESTATUS_AGENTES);
		mensaje = mensaje.replace(":nombreAgente", agente);
		System.out.println(mensaje);
		return mensaje;
	}
	/**
	 * Metodo que regresa un string sin acentos
	 * 
	 * @param text
	 * @return string sin acentos
	 * @autor martin
	 */
	public static String eliminaAcentos(String text) {
		text = Normalizer.normalize(text, Normalizer.Form.NFD);
		text = text.replaceAll("[^\\p{ASCII}]", "");
		return text;
	}
	public static String formatoMiles(BigDecimal numero){
		DecimalFormat formateador = new DecimalFormat("###,###.##"); 
		
		return formateador.format(numero);
	}

}