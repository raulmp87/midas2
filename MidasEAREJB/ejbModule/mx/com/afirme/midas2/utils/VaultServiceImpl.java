package mx.com.afirme.midas2.utils;

import java.io.File;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.io.FileUtils;
import org.jfree.util.Log;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.VaultService;

@Stateless
public class VaultServiceImpl implements VaultService {
	private static final Logger LOG = LoggerFactory.getLogger(VaultServiceImpl.class);	
	private SistemaContext sistemaContext;

	public void copyFile(ControlArchivoDTO in, ControlArchivoDTO out) {
		String uploadFolder = sistemaContext.getUploadFolder();
		String fileInName = in.getNombreArchivoOriginal();
		String extension = (fileInName.lastIndexOf(".") == -1) ? ""
				: fileInName.substring(fileInName.lastIndexOf("."),
						fileInName.length());

		File fileIn = new File(uploadFolder + in.getIdToControlArchivo()
				+ extension);
		File fileOut = new File(uploadFolder
				+ out.getIdToControlArchivo().toString() + extension);
		try {
			FileUtils.copyFile(fileIn, fileOut);
		} catch(Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

}
