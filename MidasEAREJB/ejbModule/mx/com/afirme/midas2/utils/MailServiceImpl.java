package mx.com.afirme.midas2.utils;

import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Map.Entry;
import java.util.Set;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.activation.URLDataSource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.mail.BodyPart;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.entorno.Entorno;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayDataSource;
import mx.com.afirme.midas2.domain.cobranza.Recibos;
import mx.com.afirme.midas2.domain.cobranza.pagos.PROSATransactionResponse;
import mx.com.afirme.midas2.domain.notificaciones.ConfiguracionNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.DestinatariosNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.MovimientosProcesos;
import mx.com.afirme.midas2.domain.notificaciones.ProcesosAgentes;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.notificaciones.NotificacionesService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

@Stateless
public class MailServiceImpl implements MailService {
	private SistemaContext sistemaContext;
	private NotificacionesService notificacionesService;
	private ListadoService listadoService;
	
	public static final Logger LOG = Logger	.getLogger(MailServiceImpl.class);

	private static final String emailFromAddress = Entorno.obtenerVariable(
			"email.emailFromAddress", "administrador.midas@afirme.com");
	private static final String SMTP_HOST_NAME = "172.30.4.175"; //AFIRLYDES00 – 172.30.4.176
	private static final String SMTP_PORT = "2525";
	private static final String EMAIL_FROM_USER = Entorno.obtenerVariable(
			"email.smtp.usuario", "midasweb");
	private static final String EMAIL_FROM_PASSWORD = Entorno.obtenerVariable(
			"email.smtp.contrasenia", "AfirmeMidas");
	private static final boolean ENVIO_CORREOS_ACTIVADO = Boolean
			.parseBoolean(Entorno.obtenerVariable(
					"midas.sistema.enviocorreosactivado", "true"));
	public static final boolean FILTRO_ENVIO_CORREOS_ACTIVO = Boolean
			.valueOf(Entorno.obtenerVariable(
					"midas.sistema.filtro.enviocorreos", "false"));
	public static final String[] FILTRO_CORREOS_PERMITIDOS = Entorno
			.obtenerVariable("midas.sistema.filtro.destinatarios",
					"administrador.midas@afirme.com").split(",");
	public static final String AMBIENTE_SISTEMA = Entorno.obtenerVariable(
			"midas.sistema.ambiente", "QA");
		
	private String getDirectorioImagenesMailTemplate() {
		return sistemaContext.getDirectorioImagenes();
	}
	
	private String getDirectorioImagenesMailTemplateNotificaciones() {
		return getDirectorioImagenesMailTemplate() + "/agenteNotificacion/";
	}
	
	
	public static final String EMAIL_ADMINISTRADOR_MIDAS = Entorno
			.obtenerVariable("email.administrador.midas",
					"administrador.midas@afirme.com");
	public static final String EMAIL_ADMINISTRADOR_AFIRME = Entorno
			.obtenerVariable("email.administrador.afirme", "administrador.midas@afirme.com");
	public static final String EMAIL_ADMINISTRADOR_MIDAS_AGENTES = Entorno
	.obtenerVariable("email.administrador.midas.agentes", "0itzel.aguilar@afirme.com");

	@Override
	public void sendMail(List<String> address, String title, String message) {
		sendMessage(address, title, message, emailFromAddress, null, null,null,null);
	}

	@Override
	public void sendMail(List<String> address, String title, String message, List<ByteArrayAttachment> attachment) {
		sendMessage(address, title, message, emailFromAddress, null, attachment,null,null);

	}

	@Override
	public void sendMail(List<String> address, String title, String message, List<ByteArrayAttachment> attachment,String subject) {
		sendMessage(address, title, message, emailFromAddress, null, attachment,null,null);

	}

	@Override
	public void sendMailToAdmin(String title, String message) {
		List<String> destinatarios = new ArrayList<String>();
		destinatarios.add(EMAIL_ADMINISTRADOR_MIDAS);
		destinatarios.add(EMAIL_ADMINISTRADOR_AFIRME);
		
		Date fecha = new Date();
		
		sendMessage(destinatarios, " - Registrado el " + fecha, getTemplate(title, "", message), emailFromAddress, null, null,null,null);

	}
	
	@Override
	public void sendMailToAdminAgentes(String title, String message,
			String subject, List<ByteArrayAttachment> attachment,String tipoCalculo) {
		Map<String,Map<String,List<String>>> mapCorreos = new HashMap<String, Map<String,List<String>>>(1);
		List<String> cc = new ArrayList<String>(1);
		List<String> destinatarios = new ArrayList<String>(1);
		destinatarios.add(EMAIL_ADMINISTRADOR_MIDAS_AGENTES);
		if(subject.equals("GENERACION PREVIEW")&& tipoCalculo!= null && tipoCalculo.equals("PAGO_COMISIONES")){
			mapCorreos =  getListCorreo_Preview(GenericMailService.P_PAGO_COMISIONES , GenericMailService.M_GENERACION_DE_PREVIEW);
			destinatarios.clear();
			if (mapCorreos != null && !mapCorreos.isEmpty()) {
				for (Entry<String, Map<String, List<String>>> map : mapCorreos
						.entrySet()) {		
					if(mapCorreos.get(map.getKey()).get(GenericMailService.PARA)!=null){
						for(String objPara : mapCorreos.get(map.getKey()).get(GenericMailService.PARA)){
							if(objPara!=null && !objPara.isEmpty()){
								destinatarios.add(objPara);
							}
						}
					}
					if(mapCorreos.get(map.getKey()).get(GenericMailService.COPIA)!=null){
						for(String objcc : mapCorreos.get(map.getKey()).get(GenericMailService.COPIA)){
							if(objcc!=null && !objcc.isEmpty()){
								cc.add(objcc);
							}
						}
					}
				}
			}
		}
		sendMessage(destinatarios, subject,
				getTemplate(title, "", message), emailFromAddress, null,
				attachment, cc, null);
	}

	@Override
	public void sendMailToRoleList(List<String> roleList,String title, String message) {
		
	}
	
	@Override
	public void sendMail(List<String> address, String title, String message,
			List<ByteArrayAttachment> attachment, String subject,
			String greeting) {
		
		sendMessage(address, title, getTemplate(title, greeting, message), emailFromAddress, null, attachment,null,null);

	}
	
	@Override
	public void sendMailAgenteNotificacion(List<String> address, List<String> cc,
			List<String> cco, String title, String message,
			List<ByteArrayAttachment> attachment, String subject,
			String greeting,int tipoTemplate) {
		sendMessage(address, subject, getTemplanteNotificacion(tipoTemplate,message),
				emailFromAddress, null, attachment, cc, cco);
	}
	@Override
	public void sendMail(String address, List<String> cc,
			List<String> cco, String title, String message,
			List<ByteArrayAttachment> attachment, String subject,
			String greeting,int tipoTemplate) {
		List<String> correos = new ArrayList<String>();
		correos.add(address);
		sendMessage(correos, subject, getTemplanteNotificacion(tipoTemplate,message),
				emailFromAddress, null, attachment, cc, cco);
	}
	
	/**
	 * @param recipients
	 * @param subject
	 * @param messageContent
	 * @param from
	 * @param attachments
	 * @param cc con copia
	 * @param cco con copia oculta
	 * @throws MessagingException
	 */
	public void sendMessage(List<String> recipients, String subject,
			String messageContent, String from, String[] attachments,
			List<ByteArrayAttachment> fileAttachments,List<String> cc,List<String> cco)
	{
		this.sendMessage(recipients, subject,
				messageContent, from, attachments,
				fileAttachments,cc,cco, null);
	}
	
	/***
	 * {@inheritDoc}
	 */
	@Override
	public void sendMessage(List<String> recipients, String subject,
			String messageContent, String from, String[] attachments,
			List<ByteArrayAttachment> fileAttachments,List<String> cc,List<String> cco, 
			Map<String, String> mapInlineImages) {
		try {
			messageContent = cambiarAcentos(messageContent);
			if (recipients != null && recipients.size()>0) {
				LOG.info("Recipientes:: "+recipients.get(0));
			}
			if (cc != null) {
				cc.toString();
			}
			if (cco != null) {
				cco.toString();
			}
			if (ENVIO_CORREOS_ACTIVADO) {
				boolean debug = false;
				Properties props = new Properties();
				props.put("mail.smtp.host", SMTP_HOST_NAME);
				props.put("mail.smtp.auth", "false");
				props.put("mail.smtp.port", SMTP_PORT);
				props.put("mail.smtp.socketFactory.port", SMTP_PORT);
				props.put("mail.smtp.socketFactory.fallback", "false");
				props.put("mail.smtp.starttls.enable", "true");

				Session session = javax.mail.Session.getInstance(props,
						new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {

								return new PasswordAuthentication(
										EMAIL_FROM_USER, EMAIL_FROM_PASSWORD);
							}
						});
				session.setDebug(debug);
				Message message = new MimeMessage(session);
				InternetAddress addressFrom = new InternetAddress(from);
				message.setFrom(addressFrom);
				// Add Para
				for (Iterator<String> it = recipients.iterator(); it.hasNext();) {
					String destinatario = (String) it.next();
					LOG.info("para destinatario " + destinatario);
					if (FILTRO_ENVIO_CORREOS_ACTIVO) {
						for (String correoPermitido : FILTRO_CORREOS_PERMITIDOS) {
							if (correoPermitido.equalsIgnoreCase(destinatario
									.trim())) {
								message.addRecipient(Message.RecipientType.TO,
										new InternetAddress(destinatario));
							}

						}
					} else {
						for (String correoPermitido : destinatario.split(",")) {
							message.addRecipient(Message.RecipientType.TO,
									new InternetAddress(correoPermitido));
						}
					}
				}
				// Add Copia
				if (cc != null && !cc.isEmpty()) {
					for (Iterator<String> it = cc.iterator(); it.hasNext();) {
						String destinatario = (String) it.next();
						LOG.info("cc destinatario " + destinatario);
						if (FILTRO_ENVIO_CORREOS_ACTIVO) {
							for (String correoPermitido : FILTRO_CORREOS_PERMITIDOS) {
								if (correoPermitido.equalsIgnoreCase(destinatario
										.trim())) {
									message.addRecipient(Message.RecipientType.CC,
											new InternetAddress(destinatario));
								}

							}
						} else {
							for (String correoPermitido : destinatario.split(",")) {
								message.addRecipient(Message.RecipientType.CC,
										new InternetAddress(correoPermitido));
							}
						}
					}
				}
				
				// Add Copia oculta
				if (cco != null && !cco.isEmpty()) {
					for (Iterator<String> it = cco.iterator(); it.hasNext();) {
						String destinatario = (String) it.next();
						LOG.info("cco destinatario " + destinatario);
						if (FILTRO_ENVIO_CORREOS_ACTIVO) {
							for (String correoPermitido : FILTRO_CORREOS_PERMITIDOS) {
								if (correoPermitido.equalsIgnoreCase(destinatario
										.trim())) {
									message.addRecipient(Message.RecipientType.BCC,
											new InternetAddress(destinatario));
								}

							}
						} else {
							for (String correoPermitido : destinatario.split(",")) {
								message.addRecipient(Message.RecipientType.BCC,
										new InternetAddress(correoPermitido));
							}
						}
					}
				}
				// Setting the Subject and Content Type
				LOG.info("antes de ejecutar el metodo message.setSubject(subject)");
				message.setSubject(subject);
				// Create a message part to represent the body text
				BodyPart messageBodyPart = new MimeBodyPart();
				LOG.info("antes de ejecutar el metodo messageBodyPart.setContent(messageContent, text/html)");
				messageBodyPart.setContent(messageContent, "text/html");
				// use a MimeMultipart as we need to handle the file attachments
				Multipart multipart = new MimeMultipart();
				// add the message body to the mime message
				LOG.info("antes de ejecutar el metodo multipart.addBodyPart(messageBodyPart)");
				multipart.addBodyPart(messageBodyPart);

				// Crear la firma
				BodyPart firma = new MimeBodyPart();
				LOG.info("antes de ejecutar el metodo firma.setContent(......)");
				firma.setContent("Sistema MIDAS - " + AMBIENTE_SISTEMA,
						"text/html");
				multipart.addBodyPart(firma);
				// add any file attachments to the message
				if (attachments != null && attachments.length > 0) {
					LOG.info("antes de ejecutar el metodo addAtachments(attachments, multipart)");
					addAtachments(attachments, multipart);
				}
				if (fileAttachments != null && fileAttachments.size() > 0) {
					LOG.info("antes de ejecutar el metodo addAtachments(attachments, multipart)");
					addAtachments(fileAttachments, multipart);
				}
				
				// add inline image attachments
				if(mapInlineImages != null && !mapInlineImages.isEmpty())
				{
					if (mapInlineImages != null && mapInlineImages.size() > 0) {
			            Set<String> setImageID = mapInlineImages.keySet();
			            LOG.info("adding inline image attachments...");
			            for (String contentId : setImageID) {
			                MimeBodyPart imagePart = new MimeBodyPart();
			                
			                String imageFilePath = mapInlineImages.get(contentId);
			                LOG.info("imageFilePath = " + imageFilePath);
			                
			                URL url = new URL(imageFilePath);
			                URLDataSource ds = new URLDataSource(url); 
			                imagePart.setDataHandler(new DataHandler(ds)); 
			                LOG.info("URLDataSource OK");
			                imagePart.setHeader("Content-ID", "<" + contentId + ">");
			                imagePart.setDisposition(MimeBodyPart.INLINE);
			          		 
			                multipart.addBodyPart(imagePart);
			            }
			        }
				}			
				
				// Put all message parts in the message
				LOG.info("antes de ejecutar el metodo message.setContent(multipart)");
				message.setContent(multipart);
				LOG.info("antes de ejecutar el metodo Transport.send(message)");
				Transport.send(message);
				LOG.info("Envio de correo acertado");
			}else{
				LOG.info("No se encuentra habilitado el envío de correos.");
			}
		} catch (Exception mex) {
			LOG.info("Excepcion en el envio de Correo electr�nico "
					+ mex);
			LOG.error(mex,mex);
		}

	}

	@Override
	public void sendMailWithExceptionHandler(List<String> address, String title, String message,
			List<ByteArrayAttachment> attachment, String subject,
			String greeting){

		try {
			sendMessageWithExceptionHandler(address, title, getTemplate(title, greeting, message), emailFromAddress, null, attachment,null,null);
		} catch (Exception e) {
			throw new ApplicationException(e.getMessage());
		}
	}
	
	/**
	 * @param recipients
	 * @param subject
	 * @param messageContent
	 * @param from
	 * @param attachments
	 * @param cc con copia
	 * @param cco con copia oculta
	 * @throws MessagingException
	 */
	public void sendMessageWithExceptionHandler(List<String> recipients, String subject,
			String messageContent, String from, String[] attachments,
			List<ByteArrayAttachment> fileAttachments,List<String> cc,List<String> cco) throws Exception {
		try {
			messageContent = cambiarAcentos(messageContent);
		if (recipients != null) {
			LOG.info(recipients.get(0));
		}
		if (cc != null) {
			cc.toString();
		}
		if (cco != null) {
			cco.toString();
		}
			if (ENVIO_CORREOS_ACTIVADO) {
				boolean debug = false;
				Properties props = new Properties();
				props.put("mail.smtp.host", SMTP_HOST_NAME);
				props.put("mail.smtp.auth", "false");
				props.put("mail.smtp.port", SMTP_PORT);
				props.put("mail.smtp.socketFactory.port", SMTP_PORT);
				props.put("mail.smtp.socketFactory.fallback", "false");
				props.put("mail.smtp.starttls.enable", "true");

				Session session = javax.mail.Session.getInstance(props,
						new javax.mail.Authenticator() {
							protected PasswordAuthentication getPasswordAuthentication() {

								return new PasswordAuthentication(
										EMAIL_FROM_USER, EMAIL_FROM_PASSWORD);
							}
						});
				session.setDebug(debug);
				Message message = new MimeMessage(session);
				InternetAddress addressFrom = new InternetAddress(from);
				message.setFrom(addressFrom);
				// Add Para
				for (Iterator<String> it = recipients.iterator(); it.hasNext();) {
					String destinatario = (String) it.next();
					LOG.info("para destinatario " + destinatario);
					if (FILTRO_ENVIO_CORREOS_ACTIVO) {
						for (String correoPermitido : FILTRO_CORREOS_PERMITIDOS) {
							if (correoPermitido.equalsIgnoreCase(destinatario
									.trim())) {
								message.addRecipient(Message.RecipientType.TO,
										new InternetAddress(destinatario));
							}

						}
					} else {
						for (String correoPermitido : destinatario.split(",")) {
							message.addRecipient(Message.RecipientType.TO,
									new InternetAddress(correoPermitido));
						}
					}
				}
				// Add Copia
				if (cc != null && !cc.isEmpty()) {
					for (Iterator<String> it = cc.iterator(); it.hasNext();) {
						String destinatario = (String) it.next();
						LOG.info("cc destinatario " + destinatario);
						if (FILTRO_ENVIO_CORREOS_ACTIVO) {
							for (String correoPermitido : FILTRO_CORREOS_PERMITIDOS) {
								if (correoPermitido.equalsIgnoreCase(destinatario
										.trim())) {
									message.addRecipient(Message.RecipientType.CC,
											new InternetAddress(destinatario));
								}

							}
						} else {
							for (String correoPermitido : destinatario.split(",")) {
								message.addRecipient(Message.RecipientType.CC,
										new InternetAddress(correoPermitido));
							}
						}
					}
				}
				
				// Add Copia oculta
				if (cco != null && !cco.isEmpty()) {
					for (Iterator<String> it = cco.iterator(); it.hasNext();) {
						String destinatario = (String) it.next();
						LOG.info("cco destinatario " + destinatario);
						if (FILTRO_ENVIO_CORREOS_ACTIVO) {
							for (String correoPermitido : FILTRO_CORREOS_PERMITIDOS) {
								if (correoPermitido.equalsIgnoreCase(destinatario
										.trim())) {
									message.addRecipient(Message.RecipientType.BCC,
											new InternetAddress(destinatario));
								}

							}
						} else {
							for (String correoPermitido : destinatario.split(",")) {
								message.addRecipient(Message.RecipientType.BCC,
										new InternetAddress(correoPermitido));
							}
						}
					}
				}
				// Setting the Subject and Content Type
				message.setSubject(subject);
				// Create a message part to represent the body text
				BodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(messageContent, "text/html");
				// use a MimeMultipart as we need to handle the file attachments
				Multipart multipart = new MimeMultipart();
				// add the message body to the mime message
				multipart.addBodyPart(messageBodyPart);

				// Crear la firma
				BodyPart firma = new MimeBodyPart();
				firma.setContent("Sistema MIDAS - " + AMBIENTE_SISTEMA,	"text/html");
				multipart.addBodyPart(firma);
				// add any file attachments to the message
				if (attachments != null && attachments.length > 0) {
					addAtachments(attachments, multipart);
				}
				if (fileAttachments != null && fileAttachments.size() > 0) {
					addAtachments(fileAttachments, multipart);
				}
				// Put all message parts in the message
				message.setContent(multipart);
				Transport.send(message);
				LOG.info("Envio de correo acertado");
			}else{
				LOG.info("No se encuentra habilitado el envío de correos.");
				throw new Exception("No se encuentra habilitado el envío de correos");
			}
		} catch (Exception mex) {
			LOG.info("Excepcion en el envio de Correo electrónico " + mex);
			throw new Exception("Ocurrió un error en el envío de correos.");
		}

	}
	
	protected static void addAtachments(String[] attachments,
			Multipart multipart) throws MessagingException, AddressException {
		for (int i = 0; i <= attachments.length - 1; i++) {
			String filename = attachments[i];
			MimeBodyPart attachmentBodyPart = new MimeBodyPart();
			// use a JAF FileDataSource as it does MIME type detection
			DataSource source = new FileDataSource(filename);
			attachmentBodyPart.setDataHandler(new DataHandler(source));
			// assume that the filename you want to send is the same as the
			// actual file name - could alter this to remove the file path
			String cleanFileName = filename.substring(filename
					.lastIndexOf("\\") + 1);
			attachmentBodyPart.setFileName(cleanFileName);
			// add the attachment
			multipart.addBodyPart(attachmentBodyPart);
		}
	}

	protected static void addAtachments(List<ByteArrayAttachment> files,
			Multipart multipart) throws MessagingException, AddressException {

		for (ByteArrayAttachment file : files) {
			MimeBodyPart attachmentBodyPart = new MimeBodyPart();
			// ByteArrayDataSource as it does MIME type detection

			String tipoMIME = null;
			switch (file.getTipoArchivo()) {

			case PDF: {
				tipoMIME = "application/pdf";
				break;
			}
			case IMAGEN_JPG: {
				tipoMIME = "image/jpeg";
				break;
			}
			case TEXTO: {
				tipoMIME = "text/plain";
				break;
			}
			case HTML: {
				tipoMIME = "text/html";
				break;
			}
			case DESCONOCIDO: {
				tipoMIME = "application/unknown";
				break;
			}
			default: {
				tipoMIME = "application/unknown";
				break;
			}
			}

			DataSource source = new ByteArrayDataSource(
					file.getContenidoArchivo(), tipoMIME);

			attachmentBodyPart.setDataHandler(new DataHandler(source));
			// assume that the filename you want to send is the same as the
			// actual file name - could alter this to remove the file path
			attachmentBodyPart.setFileName(file.getNombreArchivo());
			// add the attachment
			multipart.addBodyPart(attachmentBodyPart);
		}
	}
	
	public String getTemplate(String title,String greeting, String message){
		StringBuffer buffer = new StringBuffer();
		buffer.append("<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"> <head> <meta http-equiv=\"Content-Type\" content=\"text/html; charset=iso-8859-1\" /> <title>"+title+"</title>");
		buffer.append("<style type=\"text/css\"> .txt_mail { font-family: Verdana, Arial, Helvetica, sans-serif; font-size: 10px; font-weight: normal; color: #666666; text-align: left;}</style> </head>");
		buffer.append("<body><table width=\"615\" border=\"0\" align=\"center\" cellpadding=\"0\" cellspacing=\"0\"><tr><td height=\"18\"></td><td width=\"593\">&nbsp;</td><td height=\"18\"></td></tr>");
		buffer.append("<tr><td width=\"12\" height=\"12\" background=\""
						+ getDirectorioImagenesMailTemplate()
						+ "e_left1.jpg\"></td><td background=\""
						+ getDirectorioImagenesMailTemplate()
						+ "e_line1.jpg\"></td><td width=\"12\" height=\"12\" background=\""
						+ getDirectorioImagenesMailTemplate()
						+ "e_right1.jpg\"></td></tr>");
		buffer.append("<tr><td width=\"10\" height=\"150\" background=\""
						+ getDirectorioImagenesMailTemplate()
						+ "e_line3.jpg\"></td><td width=\"593\" valign=\"top\"><table width=\"600\" border=\"0\" cellpadding=\"0\" cellspacing=\"0\">");
		buffer.append("<tr><td width=\"100%\" valign=\"bottom\"><img src=\""
						+ getDirectorioImagenesMailTemplate()
						+ "Cintillo_Seguros.jpg\" width=\"600\" height=\"50\" /></td></tr><tr><td height=\"10\" colspan=\"2\"></td></tr><tr><td colspan=\"2\"><img src=\""
						+ getDirectorioImagenesMailTemplate()
						+ "line_green.gif\" width=\"600\" height=\"4\" /></td></tr><tr>");
		buffer.append("<td colspan=\"2\"><table width=\"100%\" border=\"0\" cellspacing=\"0\">");
		buffer.append("<tr>");
		buffer.append("<td valign=\"top\" class=\"txt_mail\">");
		buffer.append("<p><br /><span style=\"color: #333333\">"+(StringUtils.isNotBlank(greeting)?greeting:"")+"</span></b></p>");            
		buffer.append("<p>"+(StringUtils.isNotBlank(message)?message:"")+"</p>");                          
		buffer.append("<br />");                                                   
		buffer.append("<br />");
		buffer.append("<p> Atentamente,");
		buffer.append("<br />Seguros Afirme<br />");
		buffer.append("<br />");
		buffer.append("</p></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td width=\"77%\"></td>");
		buffer.append("</tr>");
		buffer.append("</table></td>");
		buffer.append("</tr>");
		buffer.append("</table></td>");
		buffer.append("<td width=\"12\" height=\"150\" valign=\"top\" background=\""+getDirectorioImagenesMailTemplate()+"e_line4.jpg\"></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td width=\"10\"><img src=\""+getDirectorioImagenesMailTemplate()+"e_left2.jpg\" width=\"12\" height=\"12\" /></td>");
		buffer.append("<td background=\""+getDirectorioImagenesMailTemplate()+"e_line2.jpg\"></td>");
		buffer.append("<td width=\"12\"><img src=\""+getDirectorioImagenesMailTemplate()+"e_right2.jpg\" width=\"12\" height=\"12\" /></td>");
		buffer.append("</tr>");
		buffer.append("</table>");
		buffer.append("</body>");
		buffer.append("</html>");
		
		return buffer.toString();
	}
	
	private String getTemplanteNotificacion(int tipoTemplate,String mensaje){
		switch(tipoTemplate) {
			case -1 :
				mensaje =(mensaje==null || mensaje.isEmpty())?"":mensaje.replace("Notas:<br/>null", " ");
				return getTemplate(null,null,mensaje);
			case 0 :
				return getTemplate("Bienvenido","",
						"<img src=\""
						+ getDirectorioImagenesMailTemplateNotificaciones()
						+ "bienvenido.jpg\" width=\"600\" height=\"600\" />");
				
			case 1:
			return getTemplate("Feliz Aniversario","",
					"<img src=\""
					+ getDirectorioImagenesMailTemplateNotificaciones()
					+ "felizAniversario.jpg\" width=\"600\" height=\"600\" />");
			case 2:
				return getTemplate("Feliz Aniversario","",
						"<img src=\""
						+ getDirectorioImagenesMailTemplateNotificaciones()
						+ "felizAniversarioBoda.jpg\" width=\"600\" height=\"600\" />");
			case 3:
				return getTemplate("Feliz Cumplea&amp;ntilde;os","",
						"<img src=\""
						+ getDirectorioImagenesMailTemplateNotificaciones()
						+ "felizCumpleanios.jpg\" width=\"600\" height=\"600\" />");
			case 4:
				return getTemplate("Meta Cumplida","",
						"<img src=\""
						+ getDirectorioImagenesMailTemplateNotificaciones()
						+ "metaCumplida.jpg\" width=\"600\" height=\"600\" />");
		}
		return "";
	}
	/**
	 * @author jmendoza
	 * @param messageContent
	 * @return 
	 */
	public static String cambiarAcentos(String messageContent){
		if(messageContent.contains("á")){
			messageContent = messageContent.replace("á", "&aacute;");
		}
		if(messageContent.contains("é")){
			messageContent = messageContent.replace("é", "&eacute;");
		}
		if(messageContent.contains("í")){
			messageContent = messageContent.replace("í", "&iacute;");
		}
		if(messageContent.contains("ó")){
			messageContent = messageContent.replace("ó", "&oacute;");
		}
		if(messageContent.contains("ú")){
			messageContent = messageContent.replace("ú", "&uacute;");
		}
		if(messageContent.contains("�?")){
			messageContent = messageContent.replace("�?", "&Aacute;");
		}
		if(messageContent.contains("É")){
			messageContent = messageContent.replace("É", "&Eacute;");
		}
		if(messageContent.contains("�?")){
			messageContent = messageContent.replace("�?", "&Iacute;");
		}
		if(messageContent.contains("Ó")){
			messageContent = messageContent.replace("Ó", "&Oacute;");
		}
		if(messageContent.contains("Ú")){
			messageContent = messageContent.replace("Ú", "&Uacute;");
		}
		return messageContent;
	}
	
	public  String getmensajeFacturaRecibos(Recibos reciboaenviar)
	{
		
		String mensaje=null;
		Date fecha = new Date();	
 
		StringBuffer buffer = new StringBuffer();
		
		buffer.append("<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head>");
		buffer.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>");
		buffer.append("<title>Factura del Cliente de Seguros Afirme</title><style type=\"text/css\"></style></head><body>");
		buffer.append("<table align=\"center\" id=\"main_table\">");
		buffer.append("<tr> <td align=\"center\"><span class=\"headerStyle\"><strong>FACTURA TRECIBOT</strong></span></td></tr>");
		buffer.append("<tr> <td align=\"right\" >MONTERREY, N.L. A&nbsp; TFECHAT</td> </tr>");
		buffer.append("<tr> <td align=\"left\" class=\"strongStyle\" ><strong>Estimado Cliente:</strong></td> </tr>");
		buffer.append("<tr> <td align=\"left\" class=\"strongStyle\" ><strong>TCLIENTET</strong></td> </tr>");
		buffer.append("<tr> <td> <div align=\"justify\">");
		buffer.append("El archivo anexo contiene el recibo de pago de primas de SEGUROS AFIRME S.A. DE C.V., en la que podr&aacute; ");
		buffer.append("consultar el detalle de la factura del producto asegurado en nuestra compa&ntilde;&iacute;a.<br><br> ");
		buffer.append("Para efectos fiscales estar&aacute;  recibiendo la factura impresa por correo directo (PDF), y as&iacute; ");
		buffer.append("poder otorgale la facilidad de descargar el Comprobante Fiscal Digital (XML).</div></td></tr><tr> <td >&nbsp;</td>  </tr>");
		buffer.append("<tr><td ><table align=\"center\" id=\"info_table\" style=\"width: inherit;\"border=\"1\">");
		buffer.append("<tr><th width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">RAMO</th>");
		buffer.append("<th width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">POLIZA</th>");
		buffer.append("<th width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">ENDOSO</th>");
		buffer.append("<th width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">INICIO DE VIGENCIA POLIZA</th>");
		buffer.append("<th width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">FIN DE VIGENCIA POLIZA</th>");
		buffer.append("<th width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">RECIBO</th>");
		buffer.append("<th width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">FECHA DE VENCIMIENTO</th>");
		buffer.append("<th width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">MONTO</th></tr>");
		buffer.append("<tr><td colspan=\"7\" nowrap=\"nowrap\" style=\"text-align: center\"></td></tr>");
		buffer.append("<tr><td width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">TRAMOT</td>");
		buffer.append("<td width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">TPOLIZAT</td>");
		buffer.append("<td width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">TENDOSOT</td>");
		buffer.append("<td width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">TINIVIGPOLT</td>");
		buffer.append("<td width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">TFINVIGPOLT</td>");
		buffer.append("<td width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">TRECIBOT</td>");
		buffer.append("<td width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">TFECVENT</td>");
		buffer.append("<td width=\"100\" nowrap=\"nowrap\" style=\"text-align: center\">TMONTOT</td></tr>");
		buffer.append("<tr><td colspan=\"7\"><br /></td></tr></table></td></tr><tr> <td colspan=\"7\"><br/></td></tr>");
		buffer.append("<tr> <td width=\"100\" ><divalign=\"justify\">");
		buffer.append("Este servicio es sin costo.<br><br>");
		buffer.append("Para mayor informaci&oacute;n marque al 01 800 734 8761 (01 800 SEGURO 1) desde su ciudad sin costo.<br><br>");
		buffer.append("Le recordamos que para realizar el pago, es necesario presentar la impresi&oacute;n del documento adjunto, puede ser impreso en papel reciclable, ");
		buffer.append("se recomienda impresi&oacute;n l&aacute;ser.<br><br> El documento adjunto tiene formato pdf para lo cual se requiere de Acrobat Reader.<br>");
		buffer.append("Para obtenerlo, haga click en el siguiente Enlace <a href=\"http://www.latinamerica.adobe.com/products/acrobat/readstep2.html\" target=\"_new\" > Adobe Reader</a><br><br>");			
		buffer.append("El pago lo puede realizar en Banca Afirme o Banorte.<br><br>");
		buffer.append("<strong>Dudas:</strong> Env&iacute;enos sus comentarios al correo <a href=\"mailto:cobranza@afirme.com\">cobranza@afirme.com</a>, donde con gusto lo atenderemos.<br><br></div></td></tr>");
		buffer.append("<tr><td>&nbsp;</td></tr><tr> <td >&nbsp;</td></tr>");
		buffer.append("<tr><td style=\"text-align: center\">ATENTAMENTE</td> </tr>");
		buffer.append("<tr><td >&nbsp;</td> </tr>");
		buffer.append("<tr><td class=\"strongStyle\" style=\"text-align: center\">Seguros AFIRME, S.A. DE C.V.</td> </tr>");
		buffer.append("<tr><td class=\"strongStyle\" style=\"text-align: center\">AFIRME Grupo Financiero</td> </tr></table></body></html>");
		
		mensaje=buffer.toString();		
		
		mensaje = mensaje.replaceAll("TCLIENTET", 		String.valueOf(reciboaenviar.getContratante()) );
		mensaje = mensaje.replaceAll("TFECHAT",   		DateFormat.getDateInstance( DateFormat.LONG ).format(fecha) );		
		mensaje = mensaje.replaceAll("TRAMOT", 			reciboaenviar.getRamo()    );
		mensaje = mensaje.replaceAll("TPOLIZAT", 		reciboaenviar.getPoliza()  );
		mensaje = mensaje.replaceAll("TENDOSOT", 		String.valueOf(reciboaenviar.getEndoso())        );
		mensaje = mensaje.replaceAll("TINIVIGPOLT", 	String.valueOf(reciboaenviar.getFinivigencia())  );
		mensaje = mensaje.replaceAll("TFINVIGPOLT", 	String.valueOf(reciboaenviar.getFfinvigencia())  );		
		mensaje = mensaje.replaceAll("TRECIBOT", 		String.valueOf(reciboaenviar.getNumRecibo())     );
		mensaje = mensaje.replaceAll("TFECVENT", 		String.valueOf(reciboaenviar.getVencimiento())	 ); 
		mensaje = mensaje.replaceAll("TMONTOT", 		String.valueOf( reciboaenviar.getImporte())      );
		mensaje = mensaje.replaceAll("TE_MAILT",     	reciboaenviar.getEmail()   );
		
		return mensaje.toString();
	}
	
	public String generarPlantillaPortal(List<ReciboDTO> recibosDTO, PROSATransactionResponse transactionResponse, PolizaDTO poliza){
		String cuerpoMensaje = null;
		StringBuffer buffer = new StringBuffer();
		buffer.append("<!DOCTYPE html PUBLIC\"-//W3C//DTD XHTML 1.0 Transitional//EN\" \"http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd\">");
		buffer.append("<html xmlns=\"http://www.w3.org/1999/xhtml\"><head>");
		buffer.append("<meta http-equiv=\"Content-Type\" content=\"text/html; charset=utf-8\"/>");
		buffer.append("<title>Comprobante de Pago</title><style type=\"text/css\"></style></head><body>");		
		buffer.append("<table width=\"100%\" cellpadding=\"0\" cellspacing=\"0\" border=\"0\">");
		buffer.append("<tr>");
		buffer.append("<td width=\"50%\">&nbsp;</td>");
		buffer.append("<td align=\"center\">");
		buffer.append("<table style=\"width: 612px; border-collapse: collapse\" cellpadding=\"2\" cellspacing=\"10\" border=\"0\" bgcolor=\"white\">");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td colspan=\"8\"><img src=\" "
					  + getDirectorioImagenesMailTemplate()
					  + "Cintillo_Seguros.jpg \" style=\"width: 624px; height: 84px;\"/></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>");
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td colspan=\"6\" style=\" vertical-align: middle;\"><span style=\"font-family: Arial; color: #000000; font-size: 20px; font-weight: bold;\">CONFIRMACI&Oacute;N DEL PAGO EN L&Iacute;NEA</span></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>");
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td colspan=\"5\"><span style=\"font-family: Arial; color: #000000; font-size: 14px;\">La informaci&oacute;n de su pago se envio al Correo Electr&oacute;nico que registro en la aplicaci&oacute;n. Su pago queda sujeto a validaci&oacute;n de los datos proporcionados de su estado de cuenta ante su banco. &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </span></td>");
		buffer.append("</tr>");
		buffer.append("<br/>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td colspan=\"3\" style=\"vertical-align: middle; text-align: center; background-color: #E7EFFF;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; font-size: 20px; font-weight: bold;\">N&uacute;mero de Referencia:</span></td>"); 
		buffer.append("<td style=\"background-color: #E7EFFF;\"><span style=\"font-family: Arial; color: green; font-size: 18px; font-weight: bold;\">TreferenciaT</span> </td>");
		buffer.append("<td colspan=\"3\" style=\"vertical-align: middle; background-color: #E7EFFF;\">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>");
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td colspan=\"8\"><hr style=\"width: 624px; height: 4px; background-color:green;\"/></hr></td>");
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td style=\"vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; background-color: #FFFFFF; font-size: 16px; font-weight: bold;\">Importe</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle;background-color: #F0F0F0;\"><span style=\"font-family: Arial; color: #000000; background-color: #F0F0F0; font-size: 14px;\">TimporteT</span></td>");  
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td style=\"vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; font-size: 16px; font-weight: bold;\">Comercio</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle;\"><span style=\"font-family: Arial; color: #000000; background-color: #FFFFFF; font-size: 14px;\">TcomercioT</span></td>");
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");  
		buffer.append("<td style=\" vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; background-color: #FFFFFF; font-size: 16px; font-weight: bold;\">Nombre del cliente</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle;background-color: #F0F0F0;\"><span style=\"font-family: Arial; color: #000000; background-color: #F0F0F0; font-size: 14px;\">TnombreClienteT</span></td>");
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");  
		buffer.append("<td style=\" vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; font-size: 16px; font-weight: bold;\">P&oacute;liza del cliente</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle;\"><span style=\"font-family: Arial; color: #000000; background-color: #FFFFFF; font-size: 14px;\">TnumPolizaT</span></td>");  
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");  
		buffer.append("<td style=\" vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; background-color: #FFFFFF; font-size: 16px; font-weight: bold;\">Recibo pagado</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle;background-color: #F0F0F0;\"><span style=\"font-family: Arial; color: #000000; background-color: #F0F0F0; font-size: 14px;\">TnumReciboT</span></td>");  
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");  
		buffer.append("<td style=\" vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; font-size: 16px; font-weight: bold;\">Periodo cubre recibo</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle;\"><span style=\"font-family: Arial; color: #000000; background-color: #FFFFFF; font-size: 14px;\">TperiodoCubreReciboT</span></td>");  
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td style=\" vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; background-color: #FFFFFF; font-size: 16px; font-weight: bold;\">N&uacute;mero de tarjeta</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle; background-color: #F0F0F0;\"><span style=\"font-family: Arial; color: #000000; background-color: #F0F0F0; font-size: 14px;\">TnumeroTarjetaT</span></td>");  
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");  
		buffer.append("<td style=\"vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; font-size: 16px; font-weight: bold;\">Fecha de pago</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("	<td colspan=\"3\" style=\" vertical-align: middle;\"><span style=\"font-family: Arial; color: #000000; background-color: #FFFFFF; font-size: 14px;\">TfechaPagoT</span></td>");  
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td style=\"vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; background-color: #FFFFFF; font-size: 16px; font-weight: bold;\">Hora de pago</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle;background-color: #F0F0F0; \"><span style=\"font-family: Arial; color: #000000; background-color: #F0F0F0; font-size: 14px;\">&nbsp;ThoraPagoT</span></td>");  
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">");
		buffer.append("<td style=\"vertical-align: middle;text-align: right;\"><span style=\"font-family: 'DejaVu Sans', Arial, Helvetica, sans-serif; color: #000000; font-size: 16px; font-weight: bold;\">C&oacute;digo de autorizaci&oacute;n</span></td>");
		buffer.append("<td width=\"4%\"></td>");
		buffer.append("<td colspan=\"3\" style=\" vertical-align: middle;\"><span style=\"font-family: Arial; color: #000000; background-color: #FFFFFF; font-size: 14px;\">TcodigoAutorizacionT</span></td>");
		buffer.append("</tr>");
		buffer.append("<tr>");
		buffer.append("<td>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</td>");
		buffer.append("</tr>");
		buffer.append("<tr valign=\"top\">"); 
		buffer.append("<td colspan=\"4\" style=\" vertical-align: middle;\"><span style=\"font-family: Arial; color: #000000; font-size: 14px;\">El pago efectuado queda a revisi&oacute;n por parte de Seguros Afirme, SA de CV para su aceptaci&oacute;n.</span></td>");  
		buffer.append("</tr>");
		buffer.append("</table>");
		buffer.append("</td>");
		buffer.append("<td width=\"50%\">&nbsp;");
		buffer.append("</td>");
		buffer.append("</tr>");
		buffer.append("</table>");
		buffer.append("</body>");
		buffer.append("</html>");
		
		cuerpoMensaje = buffer.toString();
		
		StringBuilder numRecibo = new StringBuilder();
		String numFolioRecibo = null;
		for(ReciboDTO recibos : recibosDTO){
			numRecibo.append(recibos.getNumeroFolioRecibo().toString());
			numRecibo.append(",");
		}
		numFolioRecibo = numRecibo.substring(0, numRecibo.length()-1);
		LOG.info("numFolioRecibo: "+numFolioRecibo);
		String arrFolioRecibo [] = numFolioRecibo.split(",");
		
		SimpleDateFormat sdft = new SimpleDateFormat("dd/MM/yyyy");
		String iniVigencia = sdft.format(recibosDTO.get(0).getFechaIncioVigencia());
		String finVigencia = sdft.format(recibosDTO.get(0).getFechaFinVigencia());
		String periodoCubreRecibo = iniVigencia +" - "+ finVigencia;
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date fecha =  new Date();
		String fechaPago = sdf.format(fecha).substring(0, 10);
		String horaPago =  sdf.format(fecha).substring(10, 19);
		
		cuerpoMensaje = cuerpoMensaje.replaceAll("TimporteT", transactionResponse.getTotalFormateado() == null ? transactionResponse.getTotal(): transactionResponse.getTotalFormateado());
		cuerpoMensaje = cuerpoMensaje.replaceAll("TcomercioT", transactionResponse.getMerchant()+ " - Afirme Seguros S.A");
		cuerpoMensaje = cuerpoMensaje.replaceAll("TnombreClienteT", StringUtils.isBlank(transactionResponse.getCc_name()) ? poliza.getNombreAsegurado() : transactionResponse.getCc_name());
		cuerpoMensaje = cuerpoMensaje.replaceAll("TnumPolizaT", transactionResponse.getPolicy());
		cuerpoMensaje = cuerpoMensaje.replaceAll("TnumReciboT", StringUtils.join(arrFolioRecibo,","));
		cuerpoMensaje = cuerpoMensaje.replaceAll("TperiodoCubreReciboT", periodoCubreRecibo);
		cuerpoMensaje = cuerpoMensaje.replaceAll("TnumeroTarjetaT", transactionResponse.getCc_number());		
		cuerpoMensaje = cuerpoMensaje.replaceAll("TfechaPagoT", fechaPago);
		cuerpoMensaje = cuerpoMensaje.replaceAll("ThoraPagoT", horaPago);
		cuerpoMensaje = cuerpoMensaje.replaceAll("TcodigoAutorizacionT", transactionResponse.getAuth());
		cuerpoMensaje = cuerpoMensaje.replaceAll("TreferenciaT", transactionResponse.getRefNum());
		
		return cuerpoMensaje;
	}

	public Map<String,Map<String, List<String>>> getListCorreo_Preview(Long proceso, Long movimiento){
		int count = 0;
		Map<String,Map<String, List<String>>> correos = new HashMap<String, Map<String,List<String>>>(1);
		Map<Long, String> modoEnvio = listadoService
				.mapValorCatalogoAgente("Modos de Envio");
		Map<Long, String> tipoDestinatario = listadoService
				.mapValorCatalogoAgente("Tipos de Destinatario");
		
		List<ConfiguracionNotificaciones>configList = new ArrayList<ConfiguracionNotificaciones>();
		// Carga las configuraciones
		ConfiguracionNotificaciones configNotificaciones = new ConfiguracionNotificaciones();
		configNotificaciones.setIdProceso(new ProcesosAgentes(proceso));
		configNotificaciones.setIdMovimiento(new MovimientosProcesos(movimiento));
		configList = notificacionesService.findByFilters(configNotificaciones);
		
		// Obtiene los correos
		for (ConfiguracionNotificaciones configuracion : configList) {
			Map<String,List<String>> mapCorreos = new HashMap<String, List<String>>(1);
			List<String> para = new ArrayList<String>(1);
			List<String> cc = new ArrayList<String>(1);
			List<DestinatariosNotificaciones> destinatariosNotificaciones = configuracion
					.getDestinatariosList();
			for (DestinatariosNotificaciones destinatario : destinatariosNotificaciones) {
				if ("OTROS".equals(tipoDestinatario
						.get(destinatario.getIdTipoDestinatario().getId()))) {
					Persona persona = new Persona();
					persona.setEmail(destinatario.getCorreo());
					MailServiceSupport.clasificaCorreo(persona, destinatario, modoEnvio, para, cc, null);
				}
			}
			if (!para.isEmpty()) {
				mapCorreos.put(GenericMailService.PARA, para);
			}
			if (!cc.isEmpty()) {
				mapCorreos.put(GenericMailService.COPIA, cc);
			}
			if (mapCorreos != null && !mapCorreos.isEmpty()) {
				ArrayList<String> notas = new ArrayList<String>(1);
				notas.add("<br/>Notas:<br/>"+configuracion.getNotas());
				mapCorreos.put(GenericMailService.NOTA,notas);
				correos.put("config"+count, mapCorreos);
				count++;
			}
		}
		
		return correos;
	}
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	@EJB
	public void setNotificacionesService(NotificacionesService notificacionesService) {
		this.notificacionesService = notificacionesService;
	}
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
}
