package mx.com.afirme.midas2.utils;

import java.awt.Font;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.apache.commons.beanutils.PropertyUtilsBean;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;


/**
 * Creador de archivos XLS. 
 * 
 * @author José Luis Arellano
 * @version 3.0.1 DCR Se tuvo que dar downgrade a la libreria para no 
 *                    interferir con la gerencion de reportes existentes. 
 *                    Por  lo tanto se le quita la habilidad de crear archivos XLSX 
 * 3.0.0 Fixes DCR
 * 
 *
 */
public class MidasXLSCreator2 {
	
	public static final Short FORMATO_XLS = new Short("1");
	public static final Short FORMATO_XLSX = new Short("2");
	
	private static final int MAXIMO_FILAS = 65536;
	private static final int MAXIMO_HOJAS = 5;
	protected List<String> nombreColumnas;
	protected List<String> nombreAtributos;
	protected HSSFWorkbook documentoXLS;
	protected HSSFSheet hojaXLS;
	
	protected int contadorFilas=0;
	protected int contadorHojas = 0;
	protected String nombreHoja;
	private Short formatoReporte;
	private HSSFCellStyle headerCellStyle;
	private boolean autoSizeColumns;
	private DateFormat dateFormat;
	
	public MidasXLSCreator2(List<String> nombreColumnas,List<String> nombreAtributos,Short formatoReporte){
		this.nombreColumnas = nombreColumnas;
		setFormatoReporte(formatoReporte);
		if(nombreAtributos != null){
			this.nombreAtributos = nombreAtributos;			
		}
	}
	
	public MidasXLSCreator2(String[] nombreColumnas,String []nombreAtributos,Short formatoReporte){
		this.nombreAtributos = Arrays.asList(nombreAtributos);
		setFormatoReporte(formatoReporte);		
		this.nombreColumnas = new ArrayList<String>();
		for(int i=0;i<nombreColumnas.length;i++)
			this.nombreColumnas.add(nombreColumnas[i]);
	}
	
	public void iniciarProcesamientoArchivoXLS(String nombreHoja)  {
		contadorFilas = contadorHojas = 0;
		crearDocumento();
		crearHoja(this.nombreHoja);
		Object fila = crearFila();
		int contadorColumna = 0;
		for(String nombreColumna : nombreColumnas){
		  HSSFCell celda = crearCelda(fila, contadorColumna);
			celda.setCellStyle(getHeaderCellStyle());
			estableceValorCelda(celda, nombreColumna);
			contadorColumna++;
		}
	}
	
	
	public void insertarFilasArchivoXLS(List<? extends Object> registros){
	  insertarFilasArchivoXLS(registros, false);
	}
	
	/**
   * Inserta las filas a trav�s de la lista de objetos donde cada objeto representa una fila. 
   * Si la bandera silent es <code>true</code> no aventara excepci�n en caso de que no encuentre
   * el nombre del atributo correspondiente en el objeto
   * @param registros
   * @param silent
   */	
	public void insertarFilasArchivoXLS(List<? extends Object> registros, boolean silent){
		if(registros != null && !registros.isEmpty()){
			Object valor = null;
			PropertyUtilsBean propertyUtilsBean = new PropertyUtilsBean();
			for(Object registro : registros){				
				Object fila = crearFila();
				int contadorColumna = 0;				
				for(String nombreAtributo : nombreAtributos){
          try {
            valor = propertyUtilsBean.getProperty(registro, nombreAtributo);
          } catch (Exception e) {
            valor = null;
            if (!silent) {              
              throw new RuntimeException(
                  "Ocurrio un error al intentar obtener el atributo '"
                      + nombreAtributo + "' del objeto: " + registro);
            }
          }					
					Object celda = crearCelda(fila, contadorColumna);
					estableceValorCelda(celda, valor);				
					contadorColumna++;
				}
			}
		}
	}
	
	public void finalizarProcesamientoArchivoXLS(String nombreArchivo) throws IOException{
		if (autoSizeColumns) {
			autoSizeColumns();
		}				
		FileOutputStream fileOut = new FileOutputStream(nombreArchivo);
		if(documentoXLS != null){
			documentoXLS.write(fileOut);
		}		
		fileOut.close();			
	}
	
	private void autoSizeColumns() {
		for (int column = 0; column < nombreColumnas.size(); column++) {
			getSheet().autoSizeColumn((short)column);
		}		
	}

	public byte[] finalizarProcesamientoArchivoXLS() throws IOException{	 
	  if (autoSizeColumns) {
      autoSizeColumns();
    }    
    ByteArrayOutputStream outputByteArray = new ByteArrayOutputStream();
    if(documentoXLS != null){     
      documentoXLS.write(outputByteArray);          
    }   
    return outputByteArray.toByteArray();		
	}
	
	private void crearDocumento(){
		if(formatoReporte.equals(FORMATO_XLS)){
			documentoXLS = new HSSFWorkbook();
		}		
	}
	
	private void crearHoja(String nombreHoja)  {
		if(documentoXLS != null){
			hojaXLS = documentoXLS.createSheet(this.nombreHoja = nombreHoja!=null ? nombreHoja : "Hoja 1");
		}		
		incrementarContadorHojas();
	}
	
	private HSSFSheet getSheet() {
		HSSFSheet sheet = null;
		if(hojaXLS != null){
			sheet = hojaXLS;
		}	
		return sheet;
	}
	
	private HSSFWorkbook getWorkbook() {
	  HSSFWorkbook workbook = null;
		if(documentoXLS != null){
			workbook = documentoXLS;
		}		
		return workbook;
	}
	
	private Object crearFila() {
		Object fila = null;
		if(hojaXLS != null){
			fila = hojaXLS.createRow(contadorFilas);
		}		
		if(fila != null){
			incrementarContadorFilas();
		}
		return fila;
	}
		
	private HSSFCell crearCelda(Object fila,int contadorColumna){
		HSSFCell celda = null;
		if(fila != null){
			if(fila instanceof HSSFRow){
				celda = ((HSSFRow)fila).createCell(contadorColumna);
			}			
		}
		return celda;
	}
	
	private void estableceValorCelda(Object celda, Object valor){
		if(celda != null && valor != null){
			if(celda instanceof HSSFCell){
				HSSFCell celdaParseada = (HSSFCell)celda;
				try{
					celdaParseada.setCellType( obtenerTipoCelda(celda,valor));
					if (valor instanceof String)
						celdaParseada.setCellValue(new HSSFRichTextString((String)valor));
					else
						celdaParseada.setCellValue(Double.valueOf((getStringFromObject(valor))));
				}catch(Exception e){
					celdaParseada.setCellValue(new HSSFRichTextString((getStringFromObject(valor))));
				}
			}			
		}
		else if(celda != null){
			estableceValorCelda(celda, "");
		}
	}
	
	private int obtenerTipoCelda(Object celda,Object object){
		int tipo = 0;
		if(celda instanceof HSSFCell){
			tipo = HSSFCell.CELL_TYPE_STRING;
			if (object != null){
				if(object instanceof BigDecimal || object instanceof Integer || object instanceof Double || object instanceof Short || object instanceof Long)
					tipo = HSSFCell.CELL_TYPE_NUMERIC;
			}
		}		
		return tipo;
	}
	
	private String getStringFromObject(Object object){
		String result = null;
		if (object != null){
			if(object instanceof Date){
				Date fecha = (Date) object;
				result = getDateFormat().format(fecha);
			}
			else{
				if(object instanceof BigDecimal){
					result = ((BigDecimal)(object)).toPlainString();
				}
				else if(object instanceof Integer){
					result = Double.valueOf((Integer)(object)).toString();
				}
				else if(object instanceof Double){
					result = ((Double)(object)).toString();
				}
				else if(object instanceof Short){
					result = ((Short)object).toString();
				}
				else if(object instanceof Long){
					result = ((Long)(object)).toString();
				} else
					result = object.toString();
			}
		}
		return result;
	}
	
	protected void incrementarContadorFilas() {
		contadorFilas++;
		if(contadorFilas >= MAXIMO_FILAS){
			contadorFilas = 0;
			incrementarContadorHojas();
			hojaXLS = documentoXLS.createSheet(nombreHoja = nombreHoja+"_"+contadorHojas);
			HSSFRow fila = hojaXLS.createRow(contadorFilas);
			int contadorColumna = 0;
			for(String nombreColumna : nombreColumnas){
				fila.createCell(contadorColumna).setCellValue(new HSSFRichTextString(nombreColumna));
				contadorColumna++;
			}
			incrementarContadorFilas();
		}
	}
	
	protected void incrementarContadorHojas() {
		contadorHojas ++;
		if(contadorHojas > MAXIMO_HOJAS){
			throw new RuntimeException("Se alcanzó el número máximo de hojas permitidas: "+contadorHojas);
		}
	}

	public Short getFormatoReporte() {
		return formatoReporte;
	}

	public void setFormatoReporte(Short formatoReporte) {
		if(formatoReporte != null && 
				(formatoReporte.compareTo(FORMATO_XLS) ==0 || 
				formatoReporte.compareTo(FORMATO_XLSX) == 0)){
			this.formatoReporte = formatoReporte;
		}
		else throw new IllegalArgumentException("Formato de reporte no válido, utilice las constantes de MidasXLSCreator");
	}
	
	protected HSSFCellStyle getHeaderCellStyle() {
		if (headerCellStyle == null) {
			headerCellStyle = getWorkbook().createCellStyle();
			HSSFFont font = getWorkbook().createFont();
			font.setBoldweight((short)Font.BOLD);
			headerCellStyle.setFont(font);
		}
		return headerCellStyle;
	}
	
	public boolean isAutoSizeColumns() {
		return autoSizeColumns;
	}
	
	public void setAutoSizeColumns(boolean autoSizeColumns) {
		this.autoSizeColumns = autoSizeColumns;
	}
	
	public DateFormat getDateFormat() {
		if (dateFormat == null) {
			dateFormat = new SimpleDateFormat("dd/MM/yyyy");
		}
		return dateFormat;
	}
	
	public void setDateFormat(DateFormat dateFormat) {
		this.dateFormat = dateFormat;
	}
	
	public HSSFWorkbook obtieneHSSFWorkbook() {
		if (autoSizeColumns) {
			autoSizeColumns();
		}
		return documentoXLS;
	}
}
