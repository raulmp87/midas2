package mx.com.afirme.midas2.utils;

import java.io.InputStream;
import java.io.StringReader;

import org.w3c.dom.*;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException; 

public class ReadAndPrintXMLFile{

	Document doc = null;
    
    public ReadAndPrintXMLFile(InputStream is){
    	try {
    		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
    		doc = docBuilder.parse(is);
		} catch (SAXParseException err) {
			System.out.println("** Parsing error" + ", line "
					+ err.getLineNumber() + ", uri " + err.getSystemId());
			System.out.println(" " + err.getMessage());
		} catch (SAXException e) {
			Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();

		} catch (Throwable t) {
			t.printStackTrace();
		}
    }
    
    public ReadAndPrintXMLFile(String str){
    	try {
    		DocumentBuilderFactory docBuilderFactory = DocumentBuilderFactory.newInstance();
    		DocumentBuilder docBuilder = docBuilderFactory.newDocumentBuilder();
    		InputSource is = new InputSource(new StringReader(str));
    		doc = docBuilder.parse(is);
		} catch (SAXParseException err) {
			System.out.println("** Parsing error" + ", line "
					+ err.getLineNumber() + ", uri " + err.getSystemId());
			System.out.println(" " + err.getMessage());
		} catch (SAXException e) {
			Exception x = e.getException();
			((x == null) ? e : x).printStackTrace();

		} catch (Throwable t) {
			t.printStackTrace();
		}
    }
    
	public String getNode(String tagName, String elementName) {
		String value = null;
		if (doc == null) {
			return null;
		}
		try {
			// normalize text representation
			doc.getDocumentElement().normalize();
			System.out.println("Root element of the doc is "+ doc.getDocumentElement().getNodeName());
			NodeList nList = doc.getElementsByTagName(tagName);
			int total = nList.getLength();
			System.out.println("Total of " + tagName +" : " + total);
			if(nList.getLength() > 0) {
				Node nNode = nList.item(0);
				if (nNode.getNodeType() == Node.ELEMENT_NODE) {		 
					Element eElement = (Element) nNode;
					if(eElement != null){
						value = eElement.getAttribute(elementName);
					}
				}
			}
		} catch (Throwable t) {
			t.printStackTrace();
		}
		return value;
	}


}
