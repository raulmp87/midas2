package mx.com.afirme.midas2.utils;

import java.util.List;

import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;

public interface Convertidor {

	@SuppressWarnings("rawtypes")
	public List<RegistroDinamicoDTO> getListaRegistrosDinamicos(List list);
	@SuppressWarnings("rawtypes")
	public List<RegistroDinamicoDTO> getListaRegistrosDinamicos(List list, Class<?> vista);
	public RegistroDinamicoDTO getRegistroDinamico(Object object);
	public RegistroDinamicoDTO getRegistroDinamico(Object object, Class<?> vista);
	public void printRows(List<RegistroDinamicoDTO> rows);
	public  void setAttribute(String attribute,Object value,List<ControlDinamicoDTO> controles,String... atributosMapeo);
	public  void setAttribute(String attribute,Object value,ControlDinamicoDTO... controles);
	public  void setAttribute(String attribute,Object value,List<ControlDinamicoDTO> controles);
	public Object getValorEspecifico(String atributoMapeo, Object objeto);
	
}
