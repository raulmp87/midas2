package mx.com.afirme.midas2.service.impl.bonos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.bonos.BonoExcepcionPolizaDao;
import mx.com.afirme.midas2.domain.bonos.BonoExcepcionPoliza;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.service.bonos.BonoExcepcionPolizaService;
/**
 * 
 * @author sams
 *
 */

@Stateless
public class BonoExcepcionPolizaServiceImpl implements BonoExcepcionPolizaService {

	private BonoExcepcionPolizaDao dao;
		
	@Override
	public List<BonoExcepcionPoliza> loadByConfigBono(ConfigBonos configBono) throws Exception {
		return dao.loadByConfigBono(configBono);
	}
	
	/************************setters & getters*******************************/
	
	@EJB
	public void setDao(BonoExcepcionPolizaDao dao) {
		this.dao = dao;
	}

}
