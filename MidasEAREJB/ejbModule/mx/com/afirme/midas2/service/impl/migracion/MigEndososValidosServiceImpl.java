package mx.com.afirme.midas2.service.impl.migracion;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas2.domain.migracion.MigEndososValidos;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;
import mx.com.afirme.midas2.service.migracion.MigEndososValidosService;

@Stateless
public class MigEndososValidosServiceImpl implements  MigEndososValidosService {

	private EntityManager entityManager;
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizar(MigEndososValidos migEndososValidos) {
		entityManager.merge(migEndososValidos);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizar(MigEndososValidosEd migEndososValidosEd) {
		entityManager.merge(migEndososValidosEd);
	}
	
}
