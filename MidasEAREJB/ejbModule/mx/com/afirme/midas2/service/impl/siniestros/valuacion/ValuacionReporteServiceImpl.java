package mx.com.afirme.midas2.service.impl.siniestros.valuacion;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ValuacionReporte;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ValuacionReportePieza;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ValuacionReporteDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Stateless
public class ValuacionReporteServiceImpl implements ValuacionReporteService{

	@EJB
	private CatalogoSiniestroService catalogoSiniestroService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private SiniestroCabinaService siniestroCabinaService;
	
	@Resource
	private Validator validator;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private static final DecimalFormat formato = new DecimalFormat("#.00");
	
	private static final String IMPRESION_VALUACION_REPORTE =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ValuacionReporte.jrxml";
	private static final String IMPRESION_VALUACION_REPORTE_PIEZAS =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ValuacionReportePiezas.jrxml";

	@Override
	public void agregarPieza(ValuacionReporte reporte, ValuacionReportePieza piezaReporte) {
		Set<ConstraintViolation<ValuacionReportePieza>> constraintViolations 	= validator.validate(piezaReporte);
		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
		}			
		piezaReporte.setValuacionReporte(reporte);
		reporte.getPiezas().add(piezaReporte);
		entidadService.save(reporte);
	}
	
//	private Boolean validarPiezaNueva(ValuacionReportePieza piezaReporte){
//		Boolean piezaCorrecta = false;
//		if(piezaReporte != null
//			&& piezaReporte.getTipoAfectacion() != null && !piezaReporte.getTipoAfectacion().isEmpty()
//			&& piezaReporte.getPieza().getId() != null && piezaReporte.getPieza().getId() != 0
//			&& piezaReporte.getSeccionAutoId() != null && piezaReporte.getSeccionAutoId() != 0){
//				piezaCorrecta = true;
//			}
//		return piezaCorrecta;
//	}

	@Override
	public void eliminarPieza(Long idValRepPieza) {
		ValuacionReportePieza pieza = entidadService.findById(ValuacionReportePieza.class, idValRepPieza);
		ValuacionReporte reporte = pieza.getValuacionReporte();
		reporte.getPiezas().remove(pieza);
		entidadService.remove(pieza);
		actualizarTotales(reporte);
	}

	@Override
	public void guardar(ValuacionReporte valuacionReporte) {
		entidadService.save(valuacionReporte);
		for(ValuacionReportePieza pieza : valuacionReporte.getPiezas()){
			entidadService.save(pieza);
		}
	}
	
	@Override
	public Long guardar(Long idReporteCabina, String codigoValuador, Integer estatusValuacion){
		ValuacionReporte nuevoReporte = new ValuacionReporte();
		nuevoReporte.setEstatus(estatusValuacion);
		nuevoReporte.setCodigoValuador(codigoValuador);
		nuevoReporte.setReporte(entidadService.findById(ReporteCabina.class, idReporteCabina));
		nuevoReporte.setAjustador(nuevoReporte.getReporte().getAjustador());
		return (Long)entidadService.saveAndGetId(nuevoReporte);
	}
	
	@Override
	public List<ValuacionReportePieza> cargarDescripcionCatalogos(List<ValuacionReportePieza> piezas){
		Map<String, String> tiposAfectacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_AFECTACION);
		Map<String, String> seccionesAuto = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.SECCION_AUTOMOVIL);
		
		for(ValuacionReportePieza pieza: piezas){
			pieza.setTipoAfectacionDesc(tiposAfectacion.get(pieza.getTipoAfectacion()));
			pieza.setSeccionAutoDesc(seccionesAuto.get(pieza.getSeccionAutoId().toString()));
		}
		
		return piezas;
	}

	@Override
	public List<ValuacionReporteDTO> listar(ValuacionReporteFiltro filtroBusqueda) {
		List<ValuacionReporteDTO> resultados = new ArrayList<ValuacionReporteDTO>();
		
		resultados = construirListaValuacionReporteDTO(
			catalogoSiniestroService.buscar(ValuacionReporte.class, filtroBusqueda));
		
		if(resultados != null && !resultados.isEmpty()){
			Collections.sort(resultados, new ValuacionReporteDTO());
		}
		
		return resultados;
	}
	
	/**
	 * Transforma la lista de ValuacionReportes a ValuacionReporteDTOs para presentar la informacion en el Grid
	 * @param listaValuaciones
	 * @return
	 */
	private List<ValuacionReporteDTO> construirListaValuacionReporteDTO(List<ValuacionReporte> listaValuaciones){
		List<ValuacionReporteDTO> listaDto = new ArrayList<ValuacionReporteDTO>();
		ValuacionReporteDTO valuacionDTO = null;
		Map<String,String> estatusValidaciones = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION_CRUCERO);
		
		for(ValuacionReporte valuacion: listaValuaciones){
			valuacionDTO = new ValuacionReporteDTO();
			valuacionDTO.setEstatus(valuacion.getEstatus());
			valuacionDTO.setEstatusDesc(estatusValidaciones.get(valuacion.getEstatus().toString()));
			valuacionDTO.setFechaValuacion(valuacion.getFechaValuacion());
			valuacionDTO.setNombreAjustador(valuacion.getAjustador()!= null && valuacion.getAjustador().getPersonaMidas()!= null
										 ? valuacion.getAjustador().getPersonaMidas().getNombre() : null);
			valuacionDTO.setNombreValuador(valuacion.getCodigoValuador());
			valuacionDTO.setNumeroReporte(valuacion.getReporte().getNumeroReporte());
			valuacionDTO.setNumeroSerie(valuacion.getReporte().getSeccionReporteCabina().
					getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie());
			valuacionDTO.setNumeroValuacion(valuacion.getId());
			valuacionDTO.setTotalReparacion(valuacion.getTotal());
			valuacionDTO.setFechaCreacion(valuacion.getFechaCreacion());
			listaDto.add(valuacionDTO);
		}
			
		return listaDto;
	}

	@Override
	public ValuacionReporte obtener(Long idValuacionReporte) {
		
		ValuacionReporte valuacion = entidadService.findById(ValuacionReporte.class, idValuacionReporte);
//		entidadService.refresh(valuacion);
		return valuacion;
	}
	
	@Override
	public ValuacionReporte actualizarTotales(Long idPiezaValuacion, Integer tipoColumna, Double nuevoValor, String origen){
		ValuacionReportePieza pieza = entidadService.findById(ValuacionReportePieza.class, idPiezaValuacion);
		ValuacionReporte reporte = pieza.getValuacionReporte();
		Double sumaTotal = new Double(0.00);
		if(tipoColumna.compareTo(ValuacionReporteService.COLUMNA_HOJALATERIA) == 0){
			pieza.setManoObra(nuevoValor);
			for(ValuacionReportePieza piezaIterar: reporte.getPiezas()){
				if(piezaIterar.getManoObra() != null){
					sumaTotal += piezaIterar.getManoObra();
					sumaTotal = Double.parseDouble(formato.format(sumaTotal));
				}
			}
			reporte.setTotalHojalateria(sumaTotal);
			reporte.setTotal(sumarTotalesValuacionReporte(reporte));
		}else if(tipoColumna.compareTo(ValuacionReporteService.COLUMNA_PINTURA) == 0){
			pieza.setPintura(nuevoValor);
			for(ValuacionReportePieza piezaIterar: reporte.getPiezas()){
				if(piezaIterar.getPintura() != null){
					sumaTotal += piezaIterar.getPintura();
					sumaTotal = Double.parseDouble(formato.format(sumaTotal));
				}
			}
			reporte.setTotalPintura(sumaTotal);
			reporte.setTotal(sumarTotalesValuacionReporte(reporte));
		}else if(tipoColumna.compareTo(ValuacionReporteService.COLUMNA_REFACCIONES) == 0){
			pieza.setCosto(nuevoValor);
			for(ValuacionReportePieza piezaIterar: reporte.getPiezas()){
				if(piezaIterar.getCosto() != null){
					sumaTotal += piezaIterar.getCosto();
					sumaTotal = Double.parseDouble(formato.format(sumaTotal));
				}
			}
			reporte.setTotalRefacciones(sumaTotal);
			reporte.setTotal(sumarTotalesValuacionReporte(reporte));
		}else if(tipoColumna.compareTo(ValuacionReporteService.COLUMNA_ORIGEN) == 0){
			pieza.setOrigen(origen);
		}
		
		entidadService.save(pieza);
		entidadService.save(reporte);
		return reporte;
	}
	
	/**
	 * calcula el total de totales del reporte
	 * @param reporte
	 * @return
	 */
	private Double sumarTotalesValuacionReporte(ValuacionReporte reporte){
		Double total = new Double(0.00);
		if(reporte.getTotalHojalateria() != null){
			total += reporte.getTotalHojalateria();
			total = Double.parseDouble(formato.format(total));
		}
		if(reporte.getTotalPintura() != null){
			total += reporte.getTotalPintura();
			total = Double.parseDouble(formato.format(total));
		}
		if(reporte.getTotalRefacciones() != null){
			total += reporte.getTotalRefacciones();
			total = Double.parseDouble(formato.format(total));
		}
		return total;
	}
	
	/**
	 * Metodo para calcular todos los totales
	 * @return
	 */
	private void actualizarTotales(ValuacionReporte reporte){
			
		Double sumaTotal = new Double(0.00);
		for(ValuacionReportePieza piezaIterar: reporte.getPiezas()){
			if(piezaIterar.getManoObra() != null){
				sumaTotal += piezaIterar.getManoObra();
				sumaTotal = Double.parseDouble(formato.format(sumaTotal));
			}
		}
		reporte.setTotalHojalateria(sumaTotal);

		sumaTotal = new Double(0.00);
		for(ValuacionReportePieza piezaIterar: reporte.getPiezas()){
			if(piezaIterar.getPintura() != null){
				sumaTotal += piezaIterar.getPintura();
				sumaTotal = Double.parseDouble(formato.format(sumaTotal));
			}
		}
		reporte.setTotalPintura(sumaTotal);

		sumaTotal = new Double(0.00);
		for(ValuacionReportePieza piezaIterar: reporte.getPiezas()){
			if(piezaIterar.getCosto() != null){
				sumaTotal += piezaIterar.getCosto();
				sumaTotal = Double.parseDouble(formato.format(sumaTotal));
			}
		}
		reporte.setTotalRefacciones(sumaTotal);
		reporte.setTotal(sumarTotalesValuacionReporte(reporte));

		entidadService.save(reporte);
	}
	
	@Override
	public ValuacionReporte obtenerValuacion(Long idReporteCabina){
		ValuacionReporte valuacionReporte = null;
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, idReporteCabina);
		List<ValuacionReporte> resultado = entidadService.findByProperty(ValuacionReporte.class, "reporte", reporteCabina);
		if(resultado != null && !resultado.isEmpty()){
			valuacionReporte = resultado.get(0);
		}
		return valuacionReporte;
	}
	
	@Override
	public List<String> terminarValuacionReporte(ValuacionReporte valuacion){
		List<String> mensaje = new ArrayList<String>();
		
		if(valuacion.getPiezas() == null || valuacion.getPiezas().isEmpty()){
			mensaje.add("El reporte debe contener al menos una pieza para ser terminado");
		}else if(!validarCostosCompletos(valuacion)){
			mensaje.add("Debe capturar todos los montos para poder terminar el reporte");
		}else if(!validarCostosMayorCero(valuacion)){
			mensaje.add("Debe capturar al menos un monto mayor a cero");
		}else{
			this.guardar(valuacion);
		}
		
		return mensaje;
	}
	
	/**
	 * valida que todos los costos del reporte esten capturados 
	 * @param valuacion
	 * @return
	 */
	private Boolean validarCostosCompletos(ValuacionReporte valuacion){
		for(ValuacionReportePieza pieza: valuacion.getPiezas()){
			if(pieza.getCosto() == null || pieza.getPintura() == null || pieza.getManoObra() == null){
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}
	
	/**
	 * valida que al menos un monto sea mayo a cero 
	 * @param valuacion
	 * @return
	 */
	private Boolean validarCostosMayorCero(ValuacionReporte valuacion){
		for(ValuacionReportePieza pieza: valuacion.getPiezas()){
			Double totalPieza = pieza.getCosto() + pieza.getPintura() + pieza.getManoObra();
			if(totalPieza <= 0){
				return Boolean.FALSE;
			}
		}
		return Boolean.TRUE;
	}
	
	@Override
	public TransporteImpresionDTO imprimirValuacion(Long idValuacionReporte){
		ValuacionReporte valuacion = obtener(idValuacionReporte);
		List<ValuacionReportePieza> piezas = cargarDescripcionCatalogos(valuacion.getPiezas());
			
		AutoIncisoReporteCabina autoIncisoReporteCabina = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(valuacion.getReporte().getId());
		String incisoMarcaTipo = ((autoIncisoReporteCabina.getDescMarca() != null)? autoIncisoReporteCabina.getDescMarca(): "") + 
			" - " + ((autoIncisoReporteCabina.getDescripcionFinal() != null)? autoIncisoReporteCabina.getDescripcionFinal(): "");
		Map<String,String> estatus = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION_CRUCERO);
		Map<String,String> valuadores = listadoService.obtenerValuadores();
			
		
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = gImpresion.getOJasperReport(IMPRESION_VALUACION_REPORTE);
		JasperReport jReportePiezasValuacion = gImpresion.getOJasperReport(IMPRESION_VALUACION_REPORTE_PIEZAS);
		
		List<ValuacionReporte> dataSourceImpresion =new ArrayList<ValuacionReporte>();
		dataSourceImpresion.add(valuacion);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("descEstatus", estatus.get(valuacion.getEstatus()));
		params.put("marcaTipo", incisoMarcaTipo);
		params.put("modelo", autoIncisoReporteCabina.getModeloVehiculo());
		params.put("placas", autoIncisoReporteCabina.getPlaca());
		params.put("color", autoIncisoReporteCabina.getDescColor());
		params.put("numeroSerie", autoIncisoReporteCabina.getNumeroSerie());
		params.put("piezas", piezas);
		params.put("valuador", valuadores.get(valuacion.getCodigoValuador()));
		params.put("jReportePiezasValuacion", jReportePiezasValuacion);
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}
	
	@SuppressWarnings({"unchecked"})
	@Override
	public List<PrestadorServicioRegistro> obtenerListadoValuadores(){
		List<PrestadorServicioRegistro> valuadores = new ArrayList<PrestadorServicioRegistro>();
		
		StringBuilder queryString = new StringBuilder(" SELECT prestador.ID, persona.NOMBRE FROM MIDAS.TCPRESTADORSERVICIO prestador ");
		queryString.append(" INNER JOIN MIDAS.TOPRESTADORSERVTIPOPRESTADOR trtipopres ON prestador.ID = trtipopres.PRESTADOR_SERVICIO_ID ");
		queryString.append(" INNER JOIN MIDAS.TCTIPOPRESTADORSERVICIO tipopres ON trtipopres.TIPO_PRESTADOR_SERVICIO_ID = tipopres.ID ");
		queryString.append(" INNER JOIN MIDAS.PERSONA_MIDAS persona ON prestador.PERSONA_ID = persona.ID");
		queryString.append(" WHERE tipopres.NOMBRE = 'VAL' AND prestador.ESTATUS = 1 ");
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		valuadores.addAll(populateList(query.getResultList()));
		return valuadores;
	}
	
	private List<PrestadorServicioRegistro> populateList(List<Object[]> listQuery) {
		List<PrestadorServicioRegistro> list = new ArrayList<PrestadorServicioRegistro>(1);	
		if(listQuery != null && !listQuery.isEmpty()){
			for(Object[] item: listQuery){
				PrestadorServicioRegistro valuador = new PrestadorServicioRegistro();
				
				Object item0 = item[0];
				valuador.setId(getValue(item0));
				
				item0 = item[1];					
				valuador.setNombrePrestador((String)item0 + '-' + valuador.getId());
				
				list.add(valuador);					
			}
		}
		return list;
	}
	
	private Long getValue(Object item) {
		Long value = 0l;
		if(item instanceof BigDecimal){
			value = ((BigDecimal)item).longValue();
		}else if (item instanceof Long){
			value = ((Long)item);
		}else if (item instanceof Double){
			value = BigDecimal.valueOf((Double)item).longValue();
		}
		return value;
	}
}