package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.recibos;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboSeycosId;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.recibos.MovimientoReciboDTO;
import mx.com.afirme.midas2.service.impl.poliza.renovacionmasiva.RenovacionMasivaSoporteService;
import mx.com.afirme.midas2.service.poliza.EnvioEmailPolizaService;
import mx.com.afirme.midas2.service.poliza.EnvioEmailPolizaService.EnviarEmailPorIncisoParameters;
import mx.com.afirme.midas2.service.poliza.PolizaService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;

import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.jfree.util.Log;
import org.joda.time.DateTime;

import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;
import com.js.util.StringUtil;

@Stateless
public class ImpresionRecibosServiceImpl extends RenovacionMasivaSoporteService implements ImpresionRecibosService{
	private static Logger LOG = Logger.getLogger(ImpresionRecibosServiceImpl.class);
	private boolean endosoConRecibos = false;
	
	@EJB
	protected RenovacionMasivaService renovacionMasivaService;
	
	@EJB
	protected EnvioEmailPolizaService envioEmailPolizaService;
	
	@EJB
	protected PolizaService polizaService;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public Long obtenerTotalPolizasRecibos(PolizaDTO polizaDTO, Boolean filtrar, Boolean isCount){
		if(!filtrar){
			polizaDTO = new PolizaDTO();
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
			
			//Fecha de Vencimiento
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(new Date());
			gcFechaFin.add(GregorianCalendar.DAY_OF_MONTH, 1);

			polizaDTO.getCotizacionDTO().setFechaFinVigencia(new Date());
			polizaDTO.getCotizacionDTO().setFechaFinVigenciaHasta(gcFechaFin.getTime());
		}else{			
			if(polizaDTO.getIdToNegProducto() != null){
				NegocioProducto negocioProducto = entidadService.findById(NegocioProducto.class, polizaDTO.getIdToNegProducto());
				polizaDTO.getCotizacionDTO().getSolicitudDTO().setProductoDTO(negocioProducto.getProductoDTO());
			}
			if(polizaDTO.getIdToNegTipoPoliza() != null){
				NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, polizaDTO.getIdToNegTipoPoliza());
				polizaDTO.getCotizacionDTO().setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
			}
		}
		return polizaService.obtenerTotalPolizaRenovacion(polizaDTO, polizaDTO.getFechaCreacion(), polizaDTO.getFechaCreacionHasta(), isCount, true);
	}

	@Override
	public List<PolizaDTO> listarPolizasRecibos(PolizaDTO polizaDTO, Boolean filtrar, Boolean isCount) {
		if(!filtrar){
			polizaDTO = new PolizaDTO();
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
			
			//Fecha de Vencimiento
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(new Date());
			gcFechaFin.add(GregorianCalendar.DAY_OF_MONTH, 1);

			polizaDTO.getCotizacionDTO().setFechaFinVigencia(new Date());
			polizaDTO.getCotizacionDTO().setFechaFinVigenciaHasta(gcFechaFin.getTime());
		}else{			
			if(polizaDTO.getIdToNegProducto() != null){
				NegocioProducto negocioProducto = entidadService.findById(NegocioProducto.class, polizaDTO.getIdToNegProducto());
				polizaDTO.getCotizacionDTO().getSolicitudDTO().setProductoDTO(negocioProducto.getProductoDTO());
			}
			if(polizaDTO.getIdToNegTipoPoliza() != null){
				NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, polizaDTO.getIdToNegTipoPoliza());
				polizaDTO.getCotizacionDTO().setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
			}
		}
		List<PolizaDTO> polizaDTOList = polizaService.buscarPolizaRenovacion(polizaDTO, polizaDTO.getFechaCreacion(), polizaDTO.getFechaCreacionHasta(), isCount, false, true);
		
		if(polizaDTOList != null && !polizaDTOList.isEmpty()){
			Collections.sort(polizaDTOList, 
					new Comparator<PolizaDTO>() {				
						public int compare(PolizaDTO n1, PolizaDTO n2){
							return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
						}
					});
		}
		
		return polizaDTOList;
	}
	
	@Override
	public InputStream imprimirPoliza(List<PolizaDTO> polizaList, Locale locale, 
        	boolean incluirCaratula, boolean incluirRecibo, boolean incluirIncisos, 
        	boolean incluirCertificadoRC, boolean incluirAnexosIncisos,boolean incluirReferenciasBancarias){
		
		List<ControlDescripcionArchivoDTO> plantillaList = new ArrayList<ControlDescripcionArchivoDTO>(1);
		try{
		for(PolizaDTO poliza : polizaList){
			try{
			//List<EndosoDTO> endosoPolizaList = endosoService.findByPropertyWithDescriptions("id.idToPoliza", poliza.getIdToPoliza(), Boolean.FALSE);
			if(poliza != null && poliza.getCotizacionDTO() == null){
				poliza = entidadService.findById(PolizaDTO.class, poliza.getIdToPoliza());
			}
			double dias = Utilerias.obtenerDiasEntreFechas(new Date(), poliza.getCotizacionDTO().getFechaInicioVigencia());
			Date fechaInicio = new Date();
			if(dias > 0){
				fechaInicio = poliza.getCotizacionDTO().getFechaInicioVigencia();
			}
			EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndosoByValidFrom(poliza.getIdToPoliza().longValue(), fechaInicio);
			DateTime validOnDT = null;
			DateTime recordFromDT = null;
			Short claveTipoEndoso = 2;
			if(ultimoEndosoDTO != null){
		
				validOnDT = new DateTime(ultimoEndosoDTO.getValidFrom().getTime());
				recordFromDT = new DateTime(ultimoEndosoDTO.getRecordFrom().getTime());
				claveTipoEndoso = ultimoEndosoDTO.getClaveTipoEndoso();
			
				TransporteImpresionDTO transporte = generarPlantillaReporte.imprimirPoliza(poliza.getIdToPoliza(), locale, validOnDT,
					recordFromDT,incluirCaratula,incluirRecibo,incluirIncisos,incluirCertificadoRC,incluirAnexosIncisos,incluirReferenciasBancarias, claveTipoEndoso, false,false);
				if(transporte != null && transporte.getByteArray() != null && transporte.getByteArray().length > 0){
					InputStream plantillaInputStream = new ByteArrayInputStream(transporte.getByteArray());
					ControlDescripcionArchivoDTO cda = new ControlDescripcionArchivoDTO();
					cda.setExtension(".pdf");							
					PolizaDTO polizaIni = cargaMasivaService.getPolizaByStringId(poliza.getIdToPoliza().toString());
					cda.setNombreArchivo(polizaIni.getNumeroPolizaFormateada());
					cda.setInputStream(plantillaInputStream);
					plantillaList.add(cda);
				}
			}
			}catch(Exception e){
				
			}
		}
		if(plantillaList.size() > 0){
			return armaZip(plantillaList);
		}
		}catch(Exception e){
		}
		return null;
	}
	
	private InputStream armaZip(List<ControlDescripcionArchivoDTO> plantillaList) throws Exception{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ZipOutputStream os = new ZipOutputStream(baos);
		os.setLevel(Deflater.DEFAULT_COMPRESSION);
		os.setMethod(Deflater.DEFLATED);
		try {
			int i = 1;
			for(ControlDescripcionArchivoDTO transporte: plantillaList){
				ZipEntry entrada = new ZipEntry(transporte.getNombreArchivo()+transporte.getExtension());
				os.putNextEntry(entrada);
				
				InputStream fis = transporte.getInputStream();
				byte [] buffer = new byte[1024];
				int leido=0;
				while (0 < (leido=fis.read(buffer))){
					os.write(buffer,0,leido);
				}
				fis.close();
				os.closeEntry();
				i++;
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			os.close();
		}
		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		baos.close();
		
		return is;
	}
	
	@Override
	public String enviaPolizasProveedor(List<PolizaDTO> polizaList, Locale locale, String nombreArchivo){
		
		boolean incluirReferenciasBancarias= false;
		InputStream zip =  imprimirPoliza(polizaList, locale, true, true, true, true, true,incluirReferenciasBancarias);
		return renovacionMasivaService.enviarProveedorPolizasSeleccionadas(nombreArchivo, zip);
		
	}

	@Override
	public String getLlaveFiscal(BigDecimal idToPoliza, Integer numeroInciso) {
		if (idToPoliza == null || numeroInciso == null) {
			throw new IllegalArgumentException("idToPoliza y numeroInciso requerido");
		}
		Query query = entityManager.createNativeQuery("select midas.fnobtllavefiscal(" + idToPoliza.intValue() + "," + numeroInciso + ") from dual");
		Object result = query.getSingleResult();
		if (result == null) {
			return null;
		}
		return (String) result;
	}
	
	@Override
	public String envioMasivoCorreos(List<PolizaDTO> polizaList, EnviarEmailPorIncisoParameters enviarEmailPorIncisoParameters){
		try {
			for(PolizaDTO poliza : polizaList){
				if(poliza != null && poliza.getCotizacionDTO() == null){
					poliza = entidadService.findById(PolizaDTO.class, poliza.getIdToPoliza());
				}
				enviarEmailPorIncisoParameters.setIdToPoliza(poliza.getIdToPoliza());
				envioEmailPolizaService.enviarEmailPorInciso(enviarEmailPorIncisoParameters);
			}
					
		} catch(Exception ex) {			
//			if (ex.getCause() instanceof ConstraintViolationException) {
//				ConstraintViolationException constraintViolationException = (ConstraintViolationException)ex.getCause();
//				addErrors(constraintViolationException.getConstraintViolations());
//			}else{
//				setTipoMensaje(BaseAction.TIPO_MENSAJE_ERROR);
//				setMensaje(ex.getMessage());				
//			}
//			return INPUT;
		}		
		return "SUCCESS";
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReciboSeycos> listarRecibosPoliza(BigDecimal idToPoliza, BigDecimal idProgPago){
		List<ReciboSeycos> list = new ArrayList<ReciboSeycos>(1);
		
		if(idProgPago != null && idProgPago.intValue() > 0){
			try{
				BigDecimal idCotizacion = polizaService.obtenerNumeroCotizacionSeycos(idToPoliza);
			
				String queryString = "Select model " +
				" FROM ReciboSeycos model  " +
				" WHERE model.idCotizacion = :idCotizacion " +
				" AND model.idProgPago = :idProgPago " +
				" AND model.fechaTerReg = :fechaTerReg " +
				" ORDER BY model.id.idRecibo ";
			
				Query query = entityManager.createQuery(queryString.toString(), ReciboSeycos.class);						
				query.setParameter("idCotizacion", idCotizacion);
				query.setParameter("idProgPago", idProgPago);
				Date fechaTerReg = Utilerias.obtenerFechaDeCadena(ReciboSeycos.FECHA_TERMINO_REGISTRO_SEYCOS);
				query.setParameter("fechaTerReg", fechaTerReg, TemporalType.DATE);
			
				query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			
				list = (List<ReciboSeycos>) query.getResultList();
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		return list;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ReciboSeycos> listarRecibosPolizaInciso(BigDecimal idToPoliza, BigDecimal idProgPago, BigDecimal idInciso) {
		List<ReciboSeycos> list = new ArrayList<ReciboSeycos>(1);
		
		if(idProgPago != null && idProgPago.intValue() > 0){
			try{
				BigDecimal idCotizacion = polizaService.obtenerNumeroCotizacionSeycos(idToPoliza);
			
				String queryString = "select distinct PR.cve_t_recibo, pr.NUM_FOLIO_FISCAL, pr.f_cubre_desde, pr.f_cubre_hasta, "+ 
					"pr.sit_recibo, pr.imp_prima_total, pr.ID_RECIBO " +
					"from seycos.pol_recibo pr " +
					"left join seycos.pol_desg_recibo pdr on pr.ID_recibo = pdr.id_recibo "+
					"where pr.id_cotizacion = " + idCotizacion + " " +
					"and pr.ID_PROG_PAGO = " + idProgPago + " " +
					"and pr.f_ter_reg = to_date('"+ ReciboSeycos.FECHA_TERMINO_REGISTRO_SEYCOS + "', 'dd/MM/yyyy') " +
					"and pr.ID_recibo = pdr.id_recibo and pdr.ID_INCISO = " + idInciso + " " +
					"order by pr.id_recibo";
			
				Query query = entityManager.createNativeQuery(queryString.toString());			
				
				List<Object[]> results = query.getResultList();
				
				if (results != null) {
					Iterator<Object[]> itResult = results.iterator();
					while (itResult.hasNext()) {
						Object[] resultado = itResult.next();
						
						String cveTipoRecibo = (String) resultado[0];
						BigDecimal numFolioFiscal = (BigDecimal) resultado[1];
						java.util.Date fCubreDesde = (java.util.Date) resultado[2];
						java.util.Date fCubreHasta = (java.util.Date) resultado[3];
						String sitRecibo = (String) resultado[4];
						BigDecimal impPrimaTotal = (BigDecimal) resultado[5];
						BigDecimal idRecibo = (BigDecimal) resultado[6];
						
						ReciboSeycos recibo = new ReciboSeycos();
						ReciboSeycosId id = new ReciboSeycosId();
						
						id.setIdRecibo(idRecibo);
						recibo.setId(id);
						recibo.setCveTipoRecibo(cveTipoRecibo);
						recibo.setNumeroFolio(numFolioFiscal);
						recibo.setFechaInicioVigencia(fCubreDesde);
						recibo.setFechaFinVigencia(fCubreHasta);
						recibo.setSitRecibo(sitRecibo);
						recibo.setImpPrimaTotal(impPrimaTotal);
						
						list.add(recibo);
					}
				}
			}catch(Exception e){
				Log.error("Error al intentar obtener la lista de recibos de la poliza:"+idToPoliza+" idProgPago:"+idProgPago+" " +
						"e inciso:"+idInciso+" dados.\n"+e.getMessage());
				throw new RuntimeException("Error al intentar obtener la lista de recibos de la poliza:"+idToPoliza+" " +
						"idProgPago:"+idProgPago+" e inciso:"+idInciso+" dados");
			}
		}
		return list;
	}
	
	@Override
	public String getLlaveFiscalRecibo(BigDecimal idToPoliza, BigDecimal idRecibo){
		String llaveFiscal = null;
		String spName = "MIDAS.PKGAUT_RECIBO.spAut_ObtieneLlaveFiscal";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pIdRecibo", idRecibo);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			MensajeDTO mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
			if(mensajeDTO != null && mensajeDTO.getMensaje() != null){
				llaveFiscal = mensajeDTO.getMensaje();
			}
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return llaveFiscal;
	}
	
	@Override
	public InputStream imprimirLlaveFiscal(String llaveFiscal){
		byte[] recibo = this.getReciboFiscal(llaveFiscal);
		if(recibo != null){
			return new ByteArrayInputStream(recibo);
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<BigDecimal, String> getMapEndososRecibos(BigDecimal idToPoliza){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		try{
			String queryString = "SELECT NUMEROENDOSO, nvl(SS.IDTOSOLICITUD,0) IDTOSOLICITUD" +
					" FROM MIDAS.TOENDOSO EE" +
					" LEFT JOIN MIDAS.CONTROLENDOSOCOT CC ON EE.IDTOCOTIZACION = CC.COTIZACION_ID AND EE.VALIDFROM = CC.VALIDFROM AND EE.RECORDFROM = CC.RECORDFROM" +
					" LEFT JOIN MIDAS.TOSOLICITUD SS ON SS.IDTOSOLICITUD = CC.SOLICITUD_ID" +
					" WHERE EE.IDTOPOLIZA = " + idToPoliza +
					" ORDER BY NUMEROENDOSO";

			Query query = entityManager.createNativeQuery(queryString.toString());			
							
			List<Object[]> results = query.getResultList();
			if (results != null) {
				Iterator<Object[]> itResult = results.iterator();
				while (itResult.hasNext()) {
					Object[] resultado = itResult.next();
					
					BigDecimal numeroEndoso = (BigDecimal) resultado[0];
					BigDecimal idToSolicitud = (BigDecimal) resultado[1];
					
					if(numeroEndoso.equals(BigDecimal.ZERO)){
						map.put(numeroEndoso, numeroEndoso + " - ALTA DE POLIZA");
					}else{
						getMapProgramaPagos(idToPoliza, numeroEndoso);
						
						if (endosoConRecibos) {
							try{
								SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class, idToSolicitud);					
								map.put(numeroEndoso, numeroEndoso + " - " +solicitud.getDescripcionTipoEndoso());
							}catch(Exception e){
								e.printStackTrace();
							}
						}
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<BigDecimal, String> getMapProgramaPagos(BigDecimal idToPoliza, BigDecimal numeroEndoso){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		try{
			
			BigDecimal idCotizacion = polizaService.obtenerNumeroCotizacionSeycos(idToPoliza);
			
			String idMidas = idToPoliza+"|"+numeroEndoso;
			
			String queryString = " SELECT DISTINCT PPP.ID_PROG_PAGO " +
					" FROM SEYCOS.POL_PROG_PAGO PPP, SEYCOS.CONV_MIDAS_SEYCOS cms " +
					" WHERE PPP.ID_COTIZACION = " + idCotizacion +
					" AND CMS.ID_MIDAS = '" + idMidas + "' " +
					" AND CMS.TIPO = 'ENDOSO' " +
					" AND  PPP.ID_DOCTO_VERS = CMS.ID_SEYCOS " +
					" ORDER BY PPP.ID_PROG_PAGO ";
			
			if(numeroEndoso.equals(BigDecimal.ZERO)){
				queryString = " SELECT DISTINCT PPP.ID_PROG_PAGO " +
				" FROM SEYCOS.POL_PROG_PAGO PPP " +
				" WHERE PPP.ID_COTIZACION = " + idCotizacion +
				" AND  PPP.ID_DOCTO_VERS = " + idCotizacion +
				" ORDER BY PPP.ID_PROG_PAGO ";
			}

			Query query = entityManager.createNativeQuery(queryString.toString());			
							
			List<Object> results = query.getResultList();
			if (results != null) {
				if (results.isEmpty()) {
					map.put(BigDecimal.ZERO, "NO SE CREARON PROGRAMAS DE PAGO");
					endosoConRecibos = false;
				} else {
					endosoConRecibos = true;
					Iterator<Object> itResult = results.iterator();
					while (itResult.hasNext()) {
						Object resultado = itResult.next();
					
						BigDecimal proPago = (BigDecimal) resultado;
					
						map.put(proPago, proPago + "");
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<BigDecimal, String> getMapIncisosEndoso(BigDecimal idToPoliza, BigDecimal idProgPago){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		try{
			
			BigDecimal idCotizacion = polizaService.obtenerNumeroCotizacionSeycos(idToPoliza);
			
			String queryString = " SELECT DISTINCT PDR.ID_INCISO " +
					" FROM SEYCOS.POL_RECIBO PR, SEYCOS.POL_DESG_RECIBO PDR " +
					" WHERE PR.ID_COTIZACION = " + idCotizacion +
					" AND PR.ID_RECIBO = PDR.ID_RECIBO " +
					" AND PR.ID_PROG_PAGO = " + idProgPago +
					" ORDER BY PDR.ID_INCISO ";

			Query query = entityManager.createNativeQuery(queryString.toString());			
							
			List<Object> results = query.getResultList();
			if (results != null) {
				Iterator<Object> itResult = results.iterator();
				while (itResult.hasNext()) {
					Object resultado = itResult.next();
					
					BigDecimal numInciso = (BigDecimal) resultado;
					
					map.put(numInciso, numInciso + "");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return map;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<MovimientoReciboDTO> getMovimientosRecibo(BigDecimal idToPoliza, BigDecimal idRecibo){
		List<MovimientoReciboDTO>  list = new ArrayList<MovimientoReciboDTO>(1);
		try{
			
			BigDecimal idCotizacion = polizaService.obtenerNumeroCotizacionSeycos(idToPoliza);
			
			String queryString = " SELECT DISTINCT PR.ID_RECIBO, PR.ID_VERSION_RBO, PR.NUM_FOLIO_RBO, PR.SIT_RECIBO, PR.CVE_T_DOCTO_VERR, PR.ID_DOCTO_VERS, " +
					" CASE WHEN PR.ID_VERSION_RBO = 1 AND PR.CVE_T_DOCTO_VERR = 'P' THEN 'POLIZA' " +
					" WHEN PR.ID_VERSION_RBO > 1 AND PR.SIT_RECIBO = 'PAG' THEN 'PAGO' " +
					" ELSE EE.CVE_MOVTO_END END CVE_MOVTO_END, ET.NUM_ENDOSO " +
					" FROM SEYCOS.POL_RECIBO PR " +
					" LEFT JOIN SEYCOS.END_ENDOSO EE ON EE.ID_SOLICITUD = PR.ID_DOCTO_VERS AND EE.ID_VERSION_END = 1 " +
					" LEFT JOIN SEYCOS.END_ENDOSO_T ET ON EE.ID_SOLICITUD = ET.ID_SOLICITUD " +
					" WHERE PR.ID_COTIZACION = " + idCotizacion +
					" and PR.ID_RECIBO = " + idRecibo +
					" ORDER BY 1, 2 ";

			Query query = entityManager.createNativeQuery(queryString.toString());			
							
			List<Object[]> results = query.getResultList();
			if (results != null) {
				Iterator<Object[]> itResult = results.iterator();
				while (itResult.hasNext()) {
					Object[] resultado = itResult.next();
					
					BigDecimal idReciboQ = (BigDecimal) resultado[0];
					BigDecimal verRecibo = (BigDecimal) resultado[1];
					BigDecimal folioRecibo = (BigDecimal) resultado[2];
					String sitRecibo = (String) resultado[3];
					String cveTDoctoVerr = (String) resultado[4];
					BigDecimal idDoctovers = (BigDecimal) resultado[5];
					String cveMovtoEnd = (String) resultado[6];
					BigDecimal numEndoso = (BigDecimal) resultado[7];

					MovimientoReciboDTO movimiento = new MovimientoReciboDTO();
					movimiento.setIdRecibo(idReciboQ);
					movimiento.setVerRecibo(verRecibo);
					movimiento.setFolioRecibo(folioRecibo);
					movimiento.setSitRecibo(sitRecibo);
					movimiento.setCveTDoctoVerr(cveTDoctoVerr);
					movimiento.setIdDoctovers(idDoctovers);
					movimiento.setCveMovtoEnd(cveMovtoEnd);
					movimiento.setNumEndoso((numEndoso != null)? numEndoso : BigDecimal.ZERO);
					
					list.add(movimiento);
					
				}
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return list;
	}
	
	@Override
	public List<MovimientoReciboDTO> getMovimientosReciboInciso(
			BigDecimal idToPoliza, BigDecimal idRecibo, BigDecimal idInciso) {
		List<MovimientoReciboDTO>  list = new ArrayList<MovimientoReciboDTO>(1);
		try{
			
			BigDecimal idCotizacion = polizaService.obtenerNumeroCotizacionSeycos(idToPoliza);
			
			String queryString = " SELECT DISTINCT PR.ID_RECIBO, PR.ID_VERSION_RBO, PR.NUM_FOLIO_RBO, PR.SIT_RECIBO, PR.CVE_T_DOCTO_VERR, PR.ID_DOCTO_VERS, " +
					" CASE WHEN PR.ID_VERSION_RBO = 1 AND PR.CVE_T_DOCTO_VERR = 'P' THEN 'POLIZA' " +
					" WHEN PR.ID_VERSION_RBO > 1 AND PR.SIT_RECIBO = 'PAG' THEN 'PAGO' " +
					" ELSE EE.CVE_MOVTO_END END CVE_MOVTO_END, ET.NUM_ENDOSO " +
					" FROM SEYCOS.POL_RECIBO PR " +
					" LEFT JOIN SEYCOS.END_ENDOSO EE ON EE.ID_SOLICITUD = PR.ID_DOCTO_VERS AND EE.ID_VERSION_END = 1 " +
					" LEFT JOIN SEYCOS.END_ENDOSO_T ET ON EE.ID_SOLICITUD = ET.ID_SOLICITUD " +
					" left join seycos.pol_desg_recibo pdr on PR.ID_recibo = pdr.id_recibo " +
					" WHERE PR.ID_COTIZACION = " + idCotizacion +
					" and PR.ID_RECIBO = " + idRecibo +
					" and PR.ID_recibo = pdr.id_recibo and pdr.ID_INCISO = " + idInciso +
					" ORDER BY 1, 2 ";

			Query query = entityManager.createNativeQuery(queryString.toString());			
							
			List<Object[]> results = query.getResultList();
			if (results != null) {
				Iterator<Object[]> itResult = results.iterator();
				while (itResult.hasNext()) {
					Object[] resultado = itResult.next();
					
					BigDecimal idReciboQ = (BigDecimal) resultado[0];
					BigDecimal verRecibo = (BigDecimal) resultado[1];
					BigDecimal folioRecibo = (BigDecimal) resultado[2];
					String sitRecibo = (String) resultado[3];
					String cveTDoctoVerr = (String) resultado[4];
					BigDecimal idDoctovers = (BigDecimal) resultado[5];
					String cveMovtoEnd = (String) resultado[6];
					BigDecimal numEndoso = (BigDecimal) resultado[7];

					MovimientoReciboDTO movimiento = new MovimientoReciboDTO();
					movimiento.setIdRecibo(idReciboQ);
					movimiento.setVerRecibo(verRecibo);
					movimiento.setFolioRecibo(folioRecibo);
					movimiento.setSitRecibo(sitRecibo);
					movimiento.setCveTDoctoVerr(cveTDoctoVerr);
					movimiento.setIdDoctovers(idDoctovers);
					movimiento.setCveMovtoEnd(cveMovtoEnd);
					movimiento.setNumEndoso((numEndoso != null)? numEndoso : BigDecimal.ZERO);
					
					list.add(movimiento);
					
				}
			}
		}catch(Exception e){
			Log.error("Error al obtener los movimientos del recibo "+idRecibo+" y el inciso "+idInciso+" dados...\n"+e.getMessage());
			throw new RuntimeException("Error al obtener los movimientos del recibo "+idRecibo+" y el inciso "+idInciso+" dados");
		}
		return list;
	}

	@Override
	public void regenerarFiscalRecibo(BigDecimal idToPoliza, BigDecimal idRecibo){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_RECIBO.spAut_RegeneraRecibo";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pIdRecibo", idRecibo);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}	
	}

	public PrintReport getServicioImpresionED(){
		try {
			PrintReportServiceLocator locator = new PrintReportServiceLocator(
					SistemaPersistencia.URL_WS_IMPRESION, SistemaPersistencia.PUERTO_WS_IMPRESION);						
			return locator.getPrintReport();		
		} catch (Exception e) {
			throw new RuntimeException("WebService de recibos no disponible");
		}
	}
	
	@Override
	public byte[] getReciboFiscal(String llaveFiscal){
		return this.getReciboFiscal(llaveFiscal, "pdf");
	}
	
	public byte[] getReciboFiscal(String llaveFiscal, String type){
		byte[] recibo = null;
		try {
			if (!StringUtil.isEmpty(llaveFiscal)) {
				recibo = this.getServicioImpresionED().getDigitalBill(llaveFiscal, type);
			}
		} catch(Exception e){
			LOG.error(e);
		}
		return recibo;
	}
}
