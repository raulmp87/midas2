package mx.com.afirme.midas2.service.impl.sapamis.procesos;

import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisBitacoras;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisEmision;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPTT;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPrevenciones;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisRechazos;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSalvamento;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSiniestros;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisDistpatcherWSService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisBitacorasService;
import mx.com.afirme.midas2.wsClient.sapamis.emision.EmisionPortProxy;
import mx.com.afirme.midas2.wsClient.sapamis.prevencion.PrevencionPortProxy;
import mx.com.afirme.midas2.wsClient.sapamis.pt.PerdidaTotalPortProxy;
import mx.com.afirme.midas2.wsClient.sapamis.rechazo.RechazoPortProxy;
import mx.com.afirme.midas2.wsClient.sapamis.salvamento.SalvamentoPortProxy;
import mx.com.afirme.midas2.wsClient.sapamis.siniestro.SiniestroPortProxy;

/*******************************************************************************
 * Nombre Interface: 	SapAmisDistpatcherWSServiceImpl.
 * 
 * Descripcion: 		Clase que realiza los Envios de Datos de los Modulos 
 * 						Solicitados por el SAP-AMIS a traves de los Clientes
 * 						que consumen los Servicios Web externos.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisDistpatcherWSServiceImpl implements SapAmisDistpatcherWSService{
	private static final long serialVersionUID = 1L;
	
	@EJB private SapAmisBitacorasService sapAmisBitacorasService;
	@EJB private SapAmisUtilsService sapAmisUtilsService; 

	private static final int OP_ALTA = 1;
	private static final int OP_MODIFICACION = 2;
	private static final int OP_CANCELACION = 3;
	private static final int OP_REHABILITACION = 4;
	
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	private EmisionPortProxy proxyEmision = new EmisionPortProxy();
	private SiniestroPortProxy proxySiniestro = new SiniestroPortProxy();
	private PrevencionPortProxy proxyPrevencion = new PrevencionPortProxy();
	private RechazoPortProxy proxyRechazo = new RechazoPortProxy();
	private PerdidaTotalPortProxy proxyPTT = new PerdidaTotalPortProxy();
	private SalvamentoPortProxy proxysalvamento = new SalvamentoPortProxy();
	
	@Override
	public void enviarEmision(SapAmisEmision emision) {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.enviarEmision(emision, accesos);
	}
	
	@Override
	public void enviarEmision(SapAmisEmision emision, String[] accesos) {
		accesos = (accesos == null ? sapAmisUtilsService.obtenerAccesos() : accesos);
		switch(emision.getSapAmisBitacoras().getCatOperaciones().getId().intValue()){
			case OP_ALTA:
				enviarEmisionAlta(emision, accesos);
				break;
			case OP_MODIFICACION:
				enviarEmisionModificacion(emision, accesos);
				break;
			case OP_CANCELACION:
				enviarEmisionCancelacion(emision, accesos);
				break;
			case OP_REHABILITACION:
				enviarEmisionRehabilitacion(emision, accesos);
				break;
		}
		emision.getSapAmisBitacoras().setFechaEnvio(new Date());
		sapAmisBitacorasService.saveObject(emision.getSapAmisBitacoras());
	}
	
	@Override
	public void enviarSiniestro(SapAmisSiniestros siniestro) {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.enviarSiniestro(siniestro, accesos);
	}
	
	@Override
	public void enviarSiniestro(SapAmisSiniestros siniestro, String[] accesos) {
		accesos = (accesos == null ? sapAmisUtilsService.obtenerAccesos() : accesos); 
		switch(siniestro.getSapAmisBitacoras().getCatOperaciones().getId().intValue()){
			case OP_ALTA:
				enviarSiniestroAlta(siniestro, accesos);
				break;
			case OP_MODIFICACION:	
				enviarSiniestroModificacion(siniestro, accesos);
				break;
			case OP_CANCELACION:
				enviarSiniestroCancelacion(siniestro, accesos);
				break;
		}
		siniestro.getSapAmisBitacoras().setFechaEnvio(new Date());
		sapAmisBitacorasService.saveObject(siniestro.getSapAmisBitacoras());
	}
	
	@Override
	public void enviarPrevencion(SapAmisPrevenciones prevencion) {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.enviarPrevencion(prevencion, accesos);
	}
	
	@Override
	public void enviarPrevencion(SapAmisPrevenciones prevencion, String[] accesos) {
		accesos = (accesos == null ? sapAmisUtilsService.obtenerAccesos() : accesos); 
		enviarPrevencionAlta(prevencion, accesos);
		prevencion.getSapAmisBitacoras().setFechaEnvio(new Date());
		sapAmisBitacorasService.saveObject(prevencion.getSapAmisBitacoras());
	}
	
	@Override
	public void enviarRechazo(SapAmisRechazos rechazo) {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.enviarRechazo(rechazo, accesos);
	}
	
	@Override
	public void enviarRechazo(SapAmisRechazos rechazo, String[] accesos) {
		accesos = (accesos == null ? sapAmisUtilsService.obtenerAccesos() : accesos); 
		enviarRechazoAlta(rechazo, accesos);
		rechazo.getSapAmisBitacoras().setFechaEnvio(new Date());
		sapAmisBitacorasService.saveObject(rechazo.getSapAmisBitacoras());
	}
	
	@Override
	public void enviarPT(SapAmisPTT ptt) {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.enviarPT(ptt, accesos);
	}
	
	@Override
	public void enviarPT(SapAmisPTT ptt, String[] accesos) {
		accesos = (accesos == null ? sapAmisUtilsService.obtenerAccesos() : accesos); 
		switch(ptt.getSapAmisBitacoras().getCatOperaciones().getId().intValue()){
			case OP_ALTA:
				enviarPTAlta(ptt, accesos);
				break;
			case OP_MODIFICACION:
				enviarPTModificacion(ptt, accesos);
				break;
			case OP_CANCELACION:
				enviarPTCancelacion(ptt, accesos);
				break;
		}
		ptt.getSapAmisBitacoras().setFechaEnvio(new Date());
		sapAmisBitacorasService.saveObject(ptt.getSapAmisBitacoras());
	}
	
	@Override
	public void enviarSalvamento(SapAmisSalvamento salvamento) {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.enviarSalvamento(salvamento, accesos);
	}
	
	@Override
	public void enviarSalvamento(SapAmisSalvamento salvamento, String[] accesos) {
		accesos = (accesos == null ? sapAmisUtilsService.obtenerAccesos() : accesos); 
		enviarSalvamentoAlta(salvamento, accesos);
		salvamento.getSapAmisBitacoras().setFechaEnvio(new Date());
		sapAmisBitacorasService.saveObject(salvamento.getSapAmisBitacoras());
	}
	
	private SapAmisBitacoras enviarEmisionAlta(SapAmisEmision emision, String[] accesos) {
		try {
			emision.getSapAmisBitacoras().setObservaciones(proxyEmision.alta(
					accesos[0],
					accesos[1],
					emision.getNoPoliza(),
					emision.getInciso(),
					sdf.format(emision.getFechaVigenciaInicio()),
					sdf.format(emision.getFechaVigenciaFin()),
					emision.getNoSerie(), 
					emision.getVehiculo().getCatMarcaVehiculo().getId().intValue(),
					emision.getVehiculo().getId().intValue(), 
					emision.getModelo(),
					emision.getVehiculo().getCatTipoTransporte().getId().intValue(),
					emision.getTipoPersona().getId().intValue(),
					emision.getCatTipoServicio().getId().intValue(),
					emision.getCliente1ApPaterno(),
					emision.getCliente1ApMaterno(),
					(emision.getCliente1Nombre()==null || emision.getCliente1Nombre().equals(""))? "Sin Nombre" : (emision.getCliente1Nombre().length() <= 100 ? emision.getCliente1Nombre() : emision.getCliente1Nombre().substring(0, 100)),
					emision.getCliente1RFC(),
					emision.getCliente1CURP(),
					emision.getCliente2ApPaterno(),
					emision.getCliente2ApMaterno(),
					(emision.getCliente2Nombre()==null||emision.getCliente2Nombre().equals("")) ? "Sin Nombre" : (emision.getCliente2Nombre().length() <= 50 ? emision.getCliente2Nombre() : emision.getCliente2Nombre().substring(0, 50)),
					emision.getBeneficiarioTipoPersona().getId().intValue(),
					emision.getBeneficiarioApPaterno(),
					emision.getBeneficiarioApMaterno(),
					emision.getBeneficiarioNombre(),
					emision.getBeneficiarioRFC(),
					emision.getBeneficiarioCURP(),
					emision.getCanalVentas().getId().intValue(), 
					emision.getAgente(),
					emision.getObligatorio(),
					sdf.format(emision.getFechaEmision()),
					emision.getUbicacionAsegurado().getId().intValue(),
					emision.getUbicacionEmision().getId().intValue()));
			emision.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long((emision.getSapAmisBitacoras().getObservaciones().equals("0") || emision.getSapAmisBitacoras().getObservaciones().equals("101,DUPLICADO, YA EXISTE LA POLIZA")) ? 0 : -1));
		} catch (Exception e) {
			emision.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			emision.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return emision.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarEmisionModificacion(SapAmisEmision emision, String[] accesos) {
		try {
			emision.getSapAmisBitacoras().setObservaciones(proxyEmision.modificacion(
							accesos[0],
							accesos[1],
							emision.getNoPoliza(), 
							emision.getInciso(),
							sdf.format(emision.getFechaVigenciaInicio()),
							sdf.format(emision.getFechaVigenciaFin()),
							emision.getNoSerie(), 
							emision.getVehiculo().getCatMarcaVehiculo().getId().intValue(),
							emision.getVehiculo().getId().intValue(), 
							emision.getModelo(),
							emision.getVehiculo().getCatTipoTransporte().getId().intValue(),
							emision.getTipoPersona().getId().intValue(),
							emision.getCatTipoServicio().getId().intValue(),
							emision.getCliente1ApPaterno(),
							emision.getCliente1ApMaterno(),
							(emision.getCliente1Nombre()==null || emision.getCliente1Nombre().equals(""))? "Sin Nombre" : (emision.getCliente1Nombre().length() <= 100 ? emision.getCliente1Nombre() : emision.getCliente1Nombre().substring(0, 100)),
							emision.getCliente1RFC(),
							emision.getCliente1CURP(),
							emision.getCliente2ApPaterno(),
							emision.getCliente2ApMaterno(),
							(emision.getCliente2Nombre()==null||emision.getCliente2Nombre().equals("")) ? "Sin Nombre" : (emision.getCliente2Nombre().length() <= 50 ? emision.getCliente2Nombre() : emision.getCliente2Nombre().substring(0, 50)),
							emision.getBeneficiarioTipoPersona().getId().intValue(),
							emision.getBeneficiarioApPaterno(),
							emision.getBeneficiarioApMaterno(),
							emision.getBeneficiarioNombre(),
							emision.getBeneficiarioRFC(),
							emision.getBeneficiarioCURP(),
							emision.getCanalVentas().getId().intValue(), 
							emision.getAgente(),
							emision.getObligatorio(),
							sdf.format(emision.getFechaEmision()),
							emision.getUbicacionAsegurado().getId().intValue(),
							emision.getUbicacionEmision().getId().intValue()));
			if(emision.getSapAmisBitacoras().getObservaciones().equals("1000, No existe la emision especificada.")){
				emision.getSapAmisBitacoras().getCatOperaciones().setId(new Long(1));
				emision.setSapAmisBitacoras(enviarEmisionAlta(emision, accesos));				
			}
			emision.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(emision.getSapAmisBitacoras().getObservaciones().equals("0") ? 0 : -1));
		} catch (Exception e) {
			emision.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			emision.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return emision.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarEmisionCancelacion(SapAmisEmision emision, String[] accesos) {
		try {
			emision.getSapAmisBitacoras().setObservaciones(proxyEmision.cancelacion(accesos[0],
							accesos[1],
							emision.getNoPoliza(),
							emision.getInciso(), 
							sdf.format(emision.getFechaVigenciaInicio()), 
							sdf.format(emision.getFechaVigenciaFin()), 
							sdf.format(emision.getSapAmisBitacoras().getFechaOperacion())));
			emision.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long((emision.getSapAmisBitacoras().getObservaciones().equals("0") || emision.getSapAmisBitacoras().getObservaciones().equals("113,POLIZA CANCELADA"))? 0 : -1));
		} catch (Exception e) {
			emision.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			emision.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return emision.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarEmisionRehabilitacion(SapAmisEmision emision, String[] accesos) {
		try {
			emision.getSapAmisBitacoras().setObservaciones(proxyEmision.rehabilitacion(
							accesos[0],
							accesos[1],
							emision.getNoPoliza(),
							emision.getInciso(), 
							sdf.format(emision.getFechaVigenciaInicio()), 
							sdf.format(emision.getFechaVigenciaFin()), 
							sdf.format(emision.getSapAmisBitacoras().getFechaOperacion())));
			emision.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(emision.getSapAmisBitacoras().getObservaciones().equals("0") ? 0 : -1));
		} catch (Exception e) {
			emision.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			emision.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return emision.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarSiniestroAlta(SapAmisSiniestros siniestro, String[] accesos) {
		try {
			siniestro.getSapAmisBitacoras().setObservaciones(
				proxySiniestro.alta(
					accesos[0],
					accesos[1],
					siniestro.getNumSiniestro(),
					siniestro.getPoliza(), 
					siniestro.getInciso(),
					sdf.format(siniestro.getFechaSiniestro()),
					siniestro.getSerie(),
					siniestro.getCausaSiniestro().getId().intValue(),
					(float)siniestro.getMonto().intValue(), 
					siniestro.getUbicacionSiniestro().getCatUbicacionEstado().getCatUbicacionPais().getId().intValue(),
					siniestro.getUbicacionSiniestro().getCatUbicacionEstado().getId().intValue(),
					siniestro.getUbicacionSiniestro().getId().intValue(),
					1,
					"",
					"",
					"",
					"",
					"",
					siniestro.getConductorApPaterno(),
					siniestro.getConductorApMaterno(),
					siniestro.getConductorNombre(),
					siniestro.getConductorRfc(),
					siniestro.getConductorCurp(),
					siniestro.getInvolucradoSiniestro().getId().intValue(),
					siniestro.getConductorTelefono()
				));
			if (siniestro.getSapAmisBitacoras().getObservaciones().equals("102,NO EXISTE LA POLIZA")){				
				siniestro.getSapAmisBitacoras().setObservaciones(
					proxySiniestro.validaAlta(
						accesos[0],
						accesos[1],
						siniestro.getNumSiniestro(),
						siniestro.getPoliza(), 
						siniestro.getInciso(),
						sdf.format(siniestro.getFechaSiniestro())));
						
			}
			siniestro.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long((siniestro.getSapAmisBitacoras().getObservaciones().equals("0") || siniestro.getSapAmisBitacoras().getObservaciones().equals("105,YA EXISTE EL SINIESTRO") || siniestro.getSapAmisBitacoras().getObservaciones().equals("123,YA EXISTE EL REGISTRO DEL TERCERO EN SINIESTRO") ) ? 0 : -1));
		} catch (Exception e) {
			siniestro.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			siniestro.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return siniestro.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarSiniestroModificacion(SapAmisSiniestros siniestro, String[] accesos) {
		try {
			siniestro.getSapAmisBitacoras().setObservaciones(proxySiniestro.modificacion(
							accesos[0],
							accesos[1],
							siniestro.getNumSiniestro(),
							siniestro.getPoliza(),
							siniestro.getInciso(),
							sdf.format(siniestro.getFechaSiniestro()),
							siniestro.getSerie(),
							siniestro.getCausaSiniestro().getId().intValue(),
							siniestro.getMonto().intValue(),
							siniestro.getUbicacionSiniestro().getCatUbicacionEstado().getCatUbicacionPais().getId().intValue(),
							siniestro.getUbicacionSiniestro().getCatUbicacionEstado().getId().intValue(),
							siniestro.getUbicacionSiniestro().getId().intValue(),
							1,
							"",
							"",
							"",
							"",
							"",
							siniestro.getConductorApPaterno(),
							siniestro.getConductorApMaterno(),
							siniestro.getConductorNombre(),
							siniestro.getConductorRfc(),
							siniestro.getConductorCurp(),
							siniestro.getInvolucradoSiniestro().getId().intValue(),								
							siniestro.getConductorTelefono()));
			if(siniestro.getSapAmisBitacoras().getObservaciones().equals("106,NO EXISTE EL REGISTRO DEL SINIESTRO")){
				siniestro.getSapAmisBitacoras().getCatSistemas().setId(new Long(1));
				siniestro.setSapAmisBitacoras(enviarSiniestroAlta(siniestro, accesos));
			}
			siniestro.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(siniestro.getSapAmisBitacoras().getObservaciones().equals("0") ? 0 : -1));
			
		} catch (Exception e) {
			siniestro.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			siniestro.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return siniestro.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarSiniestroCancelacion(SapAmisSiniestros siniestro, String[] accesos) {
		try {
			siniestro.getSapAmisBitacoras().setObservaciones(proxySiniestro.cancelacion(accesos[0],
							accesos[1],
							siniestro.getNumSiniestro(),
							siniestro.getPoliza(),
							siniestro.getInciso(), 
							sdf.format(siniestro.getFechaSiniestro()), 
							sdf.format(siniestro.getSapAmisBitacoras().getFechaOperacion())));
			siniestro.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(siniestro.getSapAmisBitacoras().getObservaciones().equals("0") ? 0 : -1));
		} catch (Exception e) {
			siniestro.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			siniestro.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return siniestro.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarPrevencionAlta(SapAmisPrevenciones prevencion, String[] accesos) {
		try {
			prevencion.getSapAmisBitacoras().setObservaciones(proxyPrevencion.alta(
					accesos[0],
					accesos[1],
					prevencion.getNumSerie(), 
					 prevencion.getCausaPrevencion().getId().intValue(),
					prevencion.getPlaca().trim(), 
					prevencion.getObservaciones()
			));
			prevencion.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long((prevencion.getSapAmisBitacoras().getObservaciones().equals("0") || prevencion.getSapAmisBitacoras().getObservaciones().equals("1000, Ya existe prevencion para ese numero de serie.")) ? 0 : -1));
		} catch (Exception e) {
			prevencion.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			prevencion.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return prevencion.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarRechazoAlta(SapAmisRechazos rechazo, String[] accesos) {
		try {
			rechazo.getSapAmisBitacoras().setObservaciones(proxyRechazo.alta(
					accesos[0], 
					accesos[1],
					rechazo.getNumeroSiniestro(), 
					 rechazo.getCatBeneficioRelacion().getCatCausaBeneficio().getId().intValue(), 
					rechazo.getDesistimiento(), 
					rechazo.getMontoRechazo(), 
					rechazo.getDetectadoSAP(), 
					rechazo.getObservaciones(), 
					 rechazo.getCatBeneficioRelacion().getCatTipoBeneficio().getId().intValue(),
					rechazo.getCatAlertasRechazos().getId().intValue()==0?2:rechazo.getCatAlertasRechazos().getId().intValue(), 
					sdf.format(rechazo.getFecha())));
			rechazo.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(rechazo.getSapAmisBitacoras().getObservaciones().equals("0") ? 0 : -1));
		} catch (Exception e) {
			rechazo.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			rechazo.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return rechazo.getSapAmisBitacoras();
	}
		
	private SapAmisBitacoras enviarPTAlta(SapAmisPTT ptt, String[] accesos) {
		try { 
			ptt.getSapAmisBitacoras().setObservaciones(proxyPTT.alta(
				  accesos[0], 
				  accesos[1],
				  "0000",
				  ptt.getNoSerie(), 
				  ptt.getNoSiniestro(),
				  sdf.format(ptt.getFechaSiniestro()),
				  (int)ptt.getPorcentajePerdida(),
				  ptt.getCatTipoAfectado().getId().intValue(),
				  ptt.getCatCausaSiniestro().getId().intValue(), 
				  ptt.getNoMotor(),
				  ptt.getCatTipoServicio().getId().intValue(),
				  ptt.getCatDano().getId().intValue(),
				  1,
				  ptt.getCatTipoMonto().getId().intValue(),
				  (int)ptt.getMontoPagado(),
				  ptt.getCatTipoPago().getId().intValue(),
				  sdf.format(ptt.getFechaPago()),
				  ptt.getCatEstVehPago().getId().intValue(),
				  ptt.getCatTipoTransporte().getId().intValue(),
				  ptt.getCatMarcaVehiculo().getId().intValue(),
				  ptt.getCatSubMarcaVehiculo().getId().intValue(),
				  (int)ptt.getAnoModelo()
			  ));
			  ptt.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long((ptt.getSapAmisBitacoras().getObservaciones().equals("0") || ptt.getSapAmisBitacoras().getObservaciones().contains("DUPLICADO, YA EXISTE EL REGISTRO DE PT")) ? 0 : -1));
		} catch (Exception e) {
			ptt.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			ptt.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return ptt.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarPTModificacion(SapAmisPTT ptt, String[] accesos) {
		try {  
			ptt.getSapAmisBitacoras().setObservaciones(proxyPTT.modificacion(
				  accesos[0], 
				  accesos[1],
				  "0000",
				  ptt.getNoSerie(), 
				  ptt.getNoSiniestro(),
				  sdf.format(ptt.getFechaSiniestro()),
				  (int)ptt.getPorcentajePerdida(),
				  ptt.getCatTipoAfectado().getId().intValue(),
				  ptt.getCatCausaSiniestro().getId().intValue(), 
				  ptt.getNoMotor(),
				  ptt.getCatTipoServicio().getId().intValue(),
				  ptt.getCatDano().getId().intValue(),
				  1,
				  ptt.getCatTipoMonto().getId().intValue(),
				  (int)ptt.getMontoPagado(),
				  ptt.getCatTipoPago().getId().intValue(),
				  sdf.format(ptt.getFechaPago()),
				  ptt.getCatEstVehPago().getId().intValue(),
				  ptt.getCatTipoTransporte().getId().intValue(),
				  ptt.getCatMarcaVehiculo().getId().intValue(),
				  ptt.getCatSubMarcaVehiculo().getId().intValue(),
				  (int)ptt.getAnoModelo()
			  ));
			ptt.getSapAmisBitacoras().getCatEstatusBitacora().setId(ptt.getSapAmisBitacoras().getObservaciones().equals("0") ? new Long(0) : new Long(-1));
		} catch (Exception e) {
			ptt.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			ptt.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return ptt.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarPTCancelacion(SapAmisPTT ptt, String[] accesos) {
		try { 
			ptt.getSapAmisBitacoras().setObservaciones(proxyPTT.cancelacion(
				  accesos[0], 
				  accesos[1],
				  ptt.getNoSerie(), 
				  ptt.getCatCausaCancelacion().getId().toString(),
				  sdf.format(ptt.getFechaCancelacion())
			  ));
			ptt.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(ptt.getSapAmisBitacoras().getObservaciones().equals("0") ? 0 : -1));
		} catch (Exception e) {
			ptt.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			ptt.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return ptt.getSapAmisBitacoras();
	}

	private SapAmisBitacoras enviarSalvamentoAlta(SapAmisSalvamento salvamento, String[] accesos) {
		try {
			salvamento.getSapAmisBitacoras().setObservaciones(proxysalvamento.alta(
				  accesos[0], 
				  accesos[1],
				  "0000",
				  salvamento.getNoSerie(), 
				  salvamento.getNoSiniestro(),
				  salvamento.getCatTipoAfectado().getId().intValue(),
				  salvamento.getCatTipoVenta().getId().intValue(),
				  (Double)salvamento.getImporteVenta(),
				  sdf.format(salvamento.getFechaVenta()),					  
				  salvamento.getNoFactura()==null?"":salvamento.getNoFactura(),
				  salvamento.getRfcFactura(),
				  salvamento.getCatTipoPersona().getId().intValue()==0?"":String.valueOf(salvamento.getCatTipoPersona().getId()),
				  salvamento.getCatEstVehPago().getId().intValue(),
				  salvamento.getRemarcado()==null?"":salvamento.getRemarcado(),
				  salvamento.getCatTipoTransporte().getId().intValue(),
				  salvamento.getCatMarcaVehiculo().getId().intValue(),
				  salvamento.getCatSubMarcaVehiculo().getId().intValue(),
				  (int)salvamento.getAnoModelo()));
			salvamento.getSapAmisBitacoras().getCatEstatusBitacora().setId((salvamento.getSapAmisBitacoras().getObservaciones().equals("0") || salvamento.getSapAmisBitacoras().getObservaciones().equals("130,DUPLICADO, YA EXISTE EL REGISTRO EN SALVAMENTO")) ? new Long(0) : new Long(-1));
		} catch (Exception e) {
			salvamento.getSapAmisBitacoras().getCatEstatusBitacora().setId(new Long(1));
			salvamento.getSapAmisBitacoras().setObservaciones("e.getMessage(): " + e.getMessage());
		}
		return salvamento.getSapAmisBitacoras();
	}
}