package mx.com.afirme.midas2.service.impl.siniestros.seguimiento;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.dto.siniestros.DefinicionSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoDatosPersona;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoDatosVehiculo;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoJuridicoDTO;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoRoboTotalDTO;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoSiniestroId;
import mx.com.afirme.midas2.dto.siniestros.seguimiento.SeguimientoValuacionDTO;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestro.RoboService;
import mx.com.afirme.midas2.service.siniestros.seguimiento.SeguimientoSiniestroService;
import mx.com.afirme.midas2.util.StringUtil;
/**
 * <font color="#808080">Esta es la definici�n de los m�todos necearios para la
 * b�squeda de sinietros catalogados como robo total.</font>
 * @author usuario
 * @version 1.0
 * @created 28-jul-2014 12:50:44 p.m.
 */
@Stateless 
public class SeguimientoSiniestroServiceImpl implements SeguimientoSiniestroService {

	
	@EJB
	private RoboService  roboService;
	
	
	@EJB
	private EntidadService entidadService;
	

	@EJB
	private CatalogoGrupoValorService  catalogoGrupoValorService;


	@Override
	public SeguimientoJuridicoDTO obtenerSeguimientoJuridico(
			String numeroSiniestro) {
		// TODO Auto-generated method stub
		return null;
	}
	/**
	 * Se deberá obtener la información del Robo Total de un Reporte. Deberá obtener
	 * el id del Reporte, ya que se recibirá el número de Siniestro.
	 * La información del robo se puede obtener con el método RoboService.
	 * consultarDefinicionSiniestroRobo. La información obtenida se deberá transformar
	 * a un objeto de tipo SeguimientoRoboDTO.
	 * 
	 * @param numeroSiniestro
	 */
	@Override
	public SeguimientoRoboTotalDTO obtenerSeguimientoRoboTotal(
			String numeroSiniestro) {
		String[] nsiniestro = null;
		Pattern p = Pattern.compile("\\-");				
		nsiniestro = p.split(numeroSiniestro);				
		String  nsOficina= nsiniestro[0];
		String nsConsecutivoR= nsiniestro[1];
		String nsAnio= nsiniestro[2];
		ReporteSiniestroRoboDTO reporteSiniestroDTO = new ReporteSiniestroRoboDTO();
		reporteSiniestroDTO.setClaveOficinaSiniestro(nsOficina) ;
		reporteSiniestroDTO.setConsecutivoReporteSiniestro(nsConsecutivoR)  ;
		reporteSiniestroDTO.setAnioReporteSiniestro(nsAnio);
		SeguimientoRoboTotalDTO seguimiento= new SeguimientoRoboTotalDTO();
		List<ReporteSiniestroRoboDTO> busquedaRobos=this.roboService.busquedaRobo(reporteSiniestroDTO);
		if(!busquedaRobos.isEmpty()){
			ReporteSiniestroRoboDTO reporteRobo = busquedaRobos.get(0);				
			DefinicionSiniestroRoboDTO dto= this.roboService.consultarDefinicionSiniestroRobo(reporteRobo.getReporteCabinaId(), reporteRobo.getIdCobertura());
			if(null==dto)
				return null;
			/*Datos SeguimientoSiniestroId ;**/
			/*TODO obtener dato numeroInciso*/
			SeguimientoSiniestroId siniestroId= new SeguimientoSiniestroId(dto.getNumeroInciso(),dto.getNoReporte(),dto.getNoSiniestro(),dto.getNoPoliza());
			seguimiento.setSiniestroId(siniestroId);
			/*Datos Contacto*/
			SeguimientoDatosPersona datosContacto=new SeguimientoDatosPersona();
			datosContacto.setEmail(dto.getMailContacto());
			datosContacto.setExtension(dto.getExtContacto());
			datosContacto.setNombre(dto.getNombreContacto());
			datosContacto.setTelefono(dto.getLadaContacto()+dto.getTelContacto());
			seguimiento.setDatosContacto(datosContacto);
			/*Datos Asegurado*/
			SeguimientoDatosPersona datosAsegurado=new SeguimientoDatosPersona();
			datosAsegurado.setEmail(null);
			datosAsegurado.setExtension(null);
			datosAsegurado.setNombre(dto.getNombreAsegurado());
			datosAsegurado.setTelefono(dto.getLadaAsegurado()+dto.getTelASegurado());
			seguimiento.setDatosAsegurado(datosAsegurado);
			seguimiento.setComentarios(dto.getObservaciones());
			
			/*datosVehiculo*/
			SeguimientoDatosVehiculo datosVehiculo=new SeguimientoDatosVehiculo();
			datosVehiculo.setColor(dto.getColor());
			datosVehiculo.setMarca(dto.getMarca());
			if( !StringUtil.isEmpty(dto.getModelo())){
				try{
					int modelo = new Integer (dto.getModelo());
					datosVehiculo.setModelo(modelo);
				}catch(Exception e){datosVehiculo.setModelo(0);}					
			}
			datosVehiculo.setNumeroSerie(dto.getNumSerie());
			datosVehiculo.setPlacas(dto.getPlacas());
			datosVehiculo.setTipo(dto.getTipoVehiculo());
			seguimiento.setDatosVehiculo(datosVehiculo);
			/*Datos Extras*/
			seguimiento.setCalleNumero(dto.getCalle());
			seguimiento.setCodigoPostal(dto.getCp());
			seguimiento.setColonia(dto.getColonia());
			seguimiento.setEstado(dto.getEstado());
			if(null!=dto.getFechaAveriguacion())
				seguimiento.setFechaAveriguacion(Utilerias.cadenaDeFecha(dto.getFechaAveriguacion(),"dd/MM/yyyy HH:mm"));
			if(null!=dto.getFechaLocalizacion())
				seguimiento.setFechaLocalizacion(Utilerias.cadenaDeFecha(dto.getFechaLocalizacion(),"dd/MM/yyyy HH:mm"));
			if(null!=dto.getFechaRecuperacion())
				seguimiento.setFechaRecuperacion(Utilerias.cadenaDeFecha(dto.getFechaRecuperacion(),"dd/MM/yyyy HH:mm"));
			if(null!=dto.getFechaReporte())
				seguimiento.setFechaReporte(Utilerias.cadenaDeFecha(dto.getFechaReporte(),"dd/MM/yyyy HH:mm"));
			if(!StringUtil.isEmpty( dto.getEstatusVehiculo())){
				CatValorFijo cat= this.catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.ESTATUS_VEHICULO, dto.getEstatusVehiculo());
				if(null!=cat)
					seguimiento.setEstatus(cat.getDescripcion());
			}
			seguimiento.setKilometro(dto.getKilometro());
			seguimiento.setMunicipio(dto.getMunicipio());
			seguimiento.setNombreCarretera(dto.getNombreCarretera());
			seguimiento.setNumAveriguacion(dto.getAveriguacionDen());
			seguimiento.setPais(dto.getPais());
			seguimiento.setReferencia(dto.getReferencia());
			if(!StringUtil.isEmpty( dto.getTipoCarretera())){
				CatValorFijo cat= this.catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_CARRETERA, dto.getTipoCarretera());
				if(null!=cat)
					seguimiento.setTipoCarretera(cat.getDescripcion());
			}
			if(!StringUtil.isEmpty( dto.getTipoRobo())){
				CatValorFijo cat= this.catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ROBO, dto.getTipoRobo());
				if(null!=cat)
					seguimiento.setTipoRobo(cat.getDescripcion());
			}
			seguimiento.setUbicacion(dto.getUbicacionVehiculo());
			
			return seguimiento;
		}else
			return null;

	}
	/**
	 * Se deberá obtener la valuación de HGS. Deberá obtener el id del Reporte y el id
	 * de la Estimación(pase de atención), ya que se recibirá el número de Siniestro y
	 * el folio del pase de atención.
	 * La valuación se puede obtener con el método HGSService.obtenerValuacion. La
	 * información obtenida se deberá transformar a un objeto de tipo
	 * SeguimientoValuacionDTO.
	 * 
	 * @param numeroSiniestro
	 * @param folioPaseAtencion
	 */
	@Override
	public SeguimientoValuacionDTO obtenerSeguimientoValuacion(
			String numeroSiniestro, Long numeroPaseAtencion) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Se deberá obtener el id de la entidad EstimacionCoberturaReporteCabina en base
	 * al numero siniestro capturado.
	 * El número de Siniestro estará formado por:
	 * CveOficina-Consecutivo-AñoReporte
	 * @param numeroSiniestro
	 */
	private Long obtenerIdEstimacion(String folio){
		List<EstimacionCoberturaReporteCabina>  lst= this.entidadService.findByProperty(EstimacionCoberturaReporteCabina.class, "folio", folio);
		if(!lst.isEmpty()){
			EstimacionCoberturaReporteCabina estimacion = lst.get(0);
			return estimacion.getId();
		}else
			return null;
		
	}

	/**
	 * Se deberá implementar una búsqueda del Id del Reporte por medio del Número de
	 * Siniestro.
	 * 
	 * El número de Siniestro estará formado por:
	 * CveOficina-Consecutivo-AñoReporte
	 * 
	 * y deberá obtener el Id del Reporte correspondiente.
	 * 
	 * @param numeroSiniestro
	 */
	private Long obtenerIdReporte(String numeroSiniestro){
		String[] nsiniestro = null;
		Pattern p = Pattern.compile("\\-");				
		nsiniestro = p.split(numeroSiniestro);				
		String  nsOficina= nsiniestro[0];
		String nsConsecutivoR= nsiniestro[1];
		String nsAnio= nsiniestro[2];
		Map<String, Object> param = new HashMap<String, Object>();	
		param.put( "claveOficina", nsOficina);
		param.put( "consecutivoReporte", nsConsecutivoR);
		param.put( "anioReporte", nsAnio);
		List<SiniestroCabina>  lst= this.entidadService.findByProperties(SiniestroCabina.class, param);
		if(!lst.isEmpty()){
			SiniestroCabina siniestroCabina=lst.get(0);
			if(null==siniestroCabina)
				return null;			
			ReporteCabina reporte = siniestroCabina.getReporteCabina();
			if(null== reporte)
				return null;
			
			return reporte.getId();
			
		}else
			return null;
		
		
	}


}