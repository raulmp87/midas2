package mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducible;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;


import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducible.NegocioDeduciblesService;

@Stateless
public class NegocioDeduciblesServiceImpl implements NegocioDeduciblesService {

	@EJB
	protected EntidadService entidadService;
	
	@EJB
	protected EntidadDao entidadDao;

	private String mensajeExito ="30";
	private String mensajeError ="10";

	@Override
	public NegocioDeducibleCob obtenerDeducibleDefault(
			BigDecimal idToNegCobPaqSeccion) {
		List<NegocioDeducibleCob> deducibles = entidadService.findByProperty(
				NegocioDeducibleCob.class,
				"negocioCobPaqSeccion.idToNegCobPaqSeccion",
				idToNegCobPaqSeccion);
		for(NegocioDeducibleCob item: deducibles){
			if(item.getClaveDefault().intValue() == 1){
				return item;
			}
		}
		return null;
	}

	@Override
	public List<NegocioDeducibleCob> obtenerDeduciblesCobertura(
			BigDecimal idToNegCobPaqSeccion) {
		List<NegocioDeducibleCob> deducibles = entidadService.findByProperty(
				NegocioDeducibleCob.class,
				"negocioCobPaqSeccion.idToNegCobPaqSeccion",
				idToNegCobPaqSeccion);
		
		if(deducibles != null && !deducibles.isEmpty()){
			Collections.sort(deducibles, 
					new Comparator<NegocioDeducibleCob>() {				
						public int compare(NegocioDeducibleCob n1, NegocioDeducibleCob n2){
							return n1.getValorDeducible().compareTo(n2.getValorDeducible());
						}
					});
		}
		
		return deducibles;
	}

	@Override
	public RespuestaGridRelacionDTO relacionarDeduciblesCobertura(
			String accion, BigDecimal idToNegCobPaqSeccion, Long id,
			Double valorDeducible, Short claveDefault) {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		NegocioCobPaqSeccion negocioCobPaqSeccion = entidadService.findById(NegocioCobPaqSeccion.class, idToNegCobPaqSeccion);  
		NegocioDeducibleCob negocioDeducibleCob= new NegocioDeducibleCob();
		
		if(id != null && !accion.equals("inserted")){
			negocioDeducibleCob.setId(id);
			
		}

		
		List<NegocioDeducibleCob> deducibles = this.obtenerDeduciblesCobertura(idToNegCobPaqSeccion);
		if(deducibles == null || deducibles.size()==0){
			negocioDeducibleCob.setClaveDefault((short)1);
		}

		String queryValor = "Select model.valorDeducible from NegocioDeducibleCob model where model.negocioCobPaqSeccion.idToNegCobPaqSeccion = :idToNegCobPaqSeccion";
		parameters = new LinkedHashMap<String, Object>();
		parameters.put("idToNegCobPaqSeccion", idToNegCobPaqSeccion);
		@SuppressWarnings("unchecked")
		List<Double> valorDeducibles = entidadDao.executeQueryMultipleResult(queryValor, parameters);
		

		
		if(valorDeducibles!=null & valorDeducibles.size()>0){
			if(valorDeducibles.contains(negocioDeducibleCob.getValorDeducible())){
				respuesta.setTipoMensaje(mensajeError);
				respuesta.setMensaje("Registro duplicado");
				return respuesta;
			}
		}
		
		String query = "Select max(model.numeroSecuencia) from NegocioDeducibleCob model where model.negocioCobPaqSeccion.idToNegCobPaqSeccion = :idToNegCobPaqSeccion";
		Integer numeroSecuencia = ((Integer)entidadDao.executeQuerySimpleResult(query, parameters));
		if(numeroSecuencia==null){
			numeroSecuencia=0;
		}
		negocioDeducibleCob.setNumeroSecuencia(numeroSecuencia+1);
		if (accion.equals("inserted")) {

			  negocioDeducibleCob.setNegocioCobPaqSeccion(negocioCobPaqSeccion);
			  negocioDeducibleCob.setClaveDefault(claveDefault);
			  negocioDeducibleCob.setValorDeducible(valorDeducible != null ? valorDeducible : 0d );

			  entidadService.save(negocioDeducibleCob);
			  obtenerDeduciblesCobertura(idToNegCobPaqSeccion);
			  negocioDeducibleCob = entidadService.getReference(NegocioDeducibleCob.class, negocioDeducibleCob.getId());
			  respuesta.setTipoMensaje(mensajeExito);
			
		} else if (accion.equalsIgnoreCase("updated")) {	
			  negocioDeducibleCob = entidadDao.getReference(NegocioDeducibleCob.class, id);
			  negocioDeducibleCob.setValorDeducible(valorDeducible);

			  negocioDeducibleCob.setNegocioCobPaqSeccion(negocioCobPaqSeccion);
			  negocioDeducibleCob.setClaveDefault(claveDefault);
			  negocioDeducibleCob.setValorDeducible(valorDeducible != null ? valorDeducible : 0d );
            
        if (negocioCobPaqSeccion.getNegocioDeducibleCobList().contains(negocioDeducibleCob)) {
        	negocioCobPaqSeccion.getNegocioDeducibleCobList().get(negocioCobPaqSeccion.getNegocioDeducibleCobList().indexOf(negocioDeducibleCob)).setValorDeducible(valorDeducible);
        	negocioCobPaqSeccion.getNegocioDeducibleCobList().get(negocioCobPaqSeccion.getNegocioDeducibleCobList().indexOf(negocioDeducibleCob)).setClaveDefault(claveDefault);
              
         }else{
        	 negocioCobPaqSeccion.getNegocioDeducibleCobList().add(negocioDeducibleCob);
               
        }
       
        entidadDao.update(negocioDeducibleCob);
        respuesta.setTipoMensaje(mensajeExito);

		} else if (accion.equals("deleted")) {
			entidadService.remove(entidadService.getReference(NegocioDeducibleCob.class, negocioDeducibleCob.getId()));
			respuesta.setTipoMensaje(mensajeExito);
		}		
		
		return respuesta;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

}
