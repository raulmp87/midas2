package mx.com.afirme.midas2.service.impl.subordinados;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas2.domain.subordinados.DerechoEndosoSeccion;
import mx.com.afirme.midas2.domain.subordinados.DerechoPolizaSeccion;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.subordinados.SubordinadosSeccionService;

@Stateless
public class SubordinadosSeccionServiceImpl implements
		SubordinadosSeccionService {

	
	public List<DerechoEndosoSeccion> obtenerDerechosEndoso(BigDecimal idSeccion) {
		
		SeccionDTO seccion = seccionFacade.findById(idSeccion);
		
		return seccion.getDerechosEndoso();
		
	}

	
	public List<DerechoPolizaSeccion> obtenerDerechosPoliza(BigDecimal idSeccion) {
		
		SeccionDTO seccion = seccionFacade.findById(idSeccion);
		
		return seccion.getDerechosPoliza();
	}

	
	public RespuestaGridRelacionDTO relacionarDerechosEndoso(String accion, BigDecimal idSeccion, 
			Integer numeroSecuencia, BigDecimal valor, Short claveDefault) {
		
		Boolean bClaveDefault = (claveDefault==1?true:false);
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		
		SeccionDTO seccion = seccionFacade.findById(idSeccion);
		
		DerechoEndosoSeccion derechoEndoso = new DerechoEndosoSeccion();
		
		derechoEndoso.setNumeroSecuencia(numeroSecuencia);
		derechoEndoso.setSeccion(seccion);
		derechoEndoso.setValor(valor);
		derechoEndoso.setClaveDefault(bClaveDefault);
		
		
		if (accion.equals("inserted")) {
			
			Integer nuevoNumeroSecuencia = 1;
			
			if (!seccion.getDerechosEndoso().isEmpty()) {
				Collections.sort(seccion.getDerechosEndoso());
				nuevoNumeroSecuencia = seccion.getDerechosEndoso().get(seccion.getDerechosEndoso().size()-1).getNumeroSecuencia() + 1;
			} 
			
			derechoEndoso.setNumeroSecuencia(nuevoNumeroSecuencia);
			
			seccion.getDerechosEndoso().add(derechoEndoso);
			
		} else if (accion.equals("updated")) {
			
			if (seccion.getDerechosEndoso().contains(derechoEndoso)) {
				seccion.getDerechosEndoso().get(seccion.getDerechosEndoso().indexOf(derechoEndoso)).setValor(valor);
				seccion.getDerechosEndoso().get(seccion.getDerechosEndoso().indexOf(derechoEndoso)).setClaveDefault(bClaveDefault);
			}
	
		} else if (accion.equals("deleted")) {
			if (seccion.getDerechosEndoso().contains(derechoEndoso)) {
				seccion.getDerechosEndoso().remove(derechoEndoso);
			}
		}
		
		seccionFacade.update(seccion);
				
		//TODO Preparar la respuesta y regresarla		
		return null;
		
		
	}

	
	public RespuestaGridRelacionDTO relacionarDerechosPoliza(String accion, BigDecimal idSeccion, 
			Integer numeroSecuencia, BigDecimal valor, Short claveDefault) {
		

		Boolean bClaveDefault = (claveDefault==1?true:false);
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		
		SeccionDTO seccion = seccionFacade.findById(idSeccion);
		
		DerechoPolizaSeccion derechoPoliza = new DerechoPolizaSeccion();
				
		derechoPoliza.setNumeroSecuencia(numeroSecuencia);
		derechoPoliza.setSeccion(seccion);
		derechoPoliza.setValor(valor);
		derechoPoliza.setClaveDefault(bClaveDefault);
		
		
		if (accion.equals("inserted")) {
			
			Integer nuevoNumeroSecuencia = 1;
			
			if (!seccion.getDerechosPoliza().isEmpty()) {
				Collections.sort(seccion.getDerechosPoliza());
				nuevoNumeroSecuencia = seccion.getDerechosPoliza().get(seccion.getDerechosPoliza().size()-1).getNumeroSecuencia() + 1;
			} 
			
			derechoPoliza.setNumeroSecuencia(nuevoNumeroSecuencia);
			
			seccion.getDerechosPoliza().add(derechoPoliza);
			
		} else if (accion.equals("updated")) {
			
			if (seccion.getDerechosPoliza().contains(derechoPoliza)) {
				seccion.getDerechosPoliza().get(seccion.getDerechosPoliza().indexOf(derechoPoliza)).setValor(valor);
				seccion.getDerechosPoliza().get(seccion.getDerechosPoliza().indexOf(derechoPoliza)).setClaveDefault(bClaveDefault);
			}
	
		} else if (accion.equals("deleted")) {
			if (seccion.getDerechosPoliza().contains(derechoPoliza)) {
				seccion.getDerechosPoliza().remove(derechoPoliza);
			}
		}
		
		seccionFacade.update(seccion);
				
		//TODO Preparar la respuesta y regresarla		
		return null;
		
	}
	
	
	
	private SeccionFacadeRemote seccionFacade;
	
	@EJB
	public void setSeccionFacade(SeccionFacadeRemote seccionFacade) {
		this.seccionFacade = seccionFacade;
	}

}
