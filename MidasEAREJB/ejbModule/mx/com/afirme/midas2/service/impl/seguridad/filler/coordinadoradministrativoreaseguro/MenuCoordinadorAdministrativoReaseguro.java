/**
 * Clase que llena las opciones de Menu para el rol de Cabinero
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadoradministrativoreaseguro;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Menu;

/**
 * @author andres.avalos
 *
 */
public class MenuCoordinadorAdministrativoReaseguro {

	private List<Menu> listaMenu = null;
		
	public MenuCoordinadorAdministrativoReaseguro() {
		listaMenu = new ArrayList<Menu>();
	}
	
public List<Menu> obtieneMenuItems() {
		
		Menu menu;
		
		menu = new Menu(new Integer("1"),"menu","", "Raiz del menu", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("37"),"m5","Reaseguro", "Reportes", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("38"),"m5_3","Reaseguro", "Reportes Reaseguro", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("44"),"m5_3_2","Reaseguro", "Movimientos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("46"),"m5_3_2_2","Reaseguro", "Reporte de Movimientos de Primas por Reasegurador (REAS)", "/MidasWeb/reaseguro/reportes/buscarPoliza.do|contenido|cargarComponentesRptMovtosPorReasegurador()", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("50"),"m5_3_3","Reaseguro", "Siniestros", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("52"),"m5_3_3_2","Reaseguro", "Reporte de Siniestros con Reaseguro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("53"),"m5_3_3_3","Reaseguro", "Reporte de Siniestros de Reservas Pendientes", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("54"),"m5_3_3_4","Reaseguro", "Reporte de Siniestros de Reservas Pendientes Acumulado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("55"),"m5_3_4","Reaseguro", "Contratos No Proporcionales", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("56"),"m5_3_4_1","Reaseguro", "Reporte de Siniestros por Evento Catastr�fico", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("57"),"m5_3_4_2","Reaseguro", "Reporte de Siniestros Tent Plan", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("58"),"m5_3_4_3","Reaseguro", "Reporte de Siniestros Working Cover", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("59"),"m5_3_5","Reaseguro", "Ingresos y Egresos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("60"),"m5_3_5_1","Reaseguro", "Reporte de Ingresos de Reaseguro", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("61"),"m5_3_5_2","Reaseguro", "Reporte Estad�stica Fiscal Facultativos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("62"),"m5_3_5_3","Reaseguro", "Reporte Estad�stica Fiscal Autom�ticos", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("63"),"m7","Ayuda", "Ayuda MIDAS", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("64"),"m7_1","Ayuda Midas", "Submenu Ayuda Midas", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("65"),"m7_2","Acerca de...", "Submenu Acerca de...", null, true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("66"),"m5_3_3_4_1","Reaseguro", "Reporte de Siniestros de Reservas Pendientes Acumulado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("67"),"m5_3_3_4_2","Reaseguro", "Reporte de Siniestros de Reservas Pendientes Acumulado Detallado", "", true);
		listaMenu.add(menu);
		
		menu = new Menu(new Integer("68"),"m5_3_2_7","Reaseguro", "Reportes de Saldos por Reasegurador", "", true);
		listaMenu.add(menu);

		
		return this.listaMenu;
		
	}
	
}
