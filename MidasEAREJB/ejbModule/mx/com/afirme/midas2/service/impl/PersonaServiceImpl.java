package mx.com.afirme.midas2.service.impl;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.InputStream;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidasExt;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidasExt_;
import mx.com.afirme.midas2.service.PersonaService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;

@Stateless
public class PersonaServiceImpl implements PersonaService {
	private static final Logger LOG = Logger.getLogger(PersonaServiceImpl.class);
	
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private EntidadService entidadService;

	@Override
	public boolean guardarFoto(Long idPersona, File foto) {
		try {
			PersonaMidasExt extra = obtenerPersonaExt(idPersona);
			if (extra == null) {
				extra = new PersonaMidasExt();
				final PersonaMidas persona = entityManager.getReference(PersonaMidas.class, idPersona);
				extra.setPersonaMidas(persona);
			} 
			extra.setFoto(FileUtils.readFileToByteArray(foto));
			entityManager.merge(extra);
			return true;
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return false;
		}
	}
	
	public InputStream obtenerFoto(Long idPersona) {
		try {
			final PersonaMidasExt extra = obtenerPersonaExt(idPersona);
			if (extra == null || extra.getFoto() == null) {
				return null;
			} 
			return new ByteArrayInputStream(extra.getFoto());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}
	
	public PersonaMidasExt obtenerPersonaExt(Long idPersona) {
		try {
			List<PersonaMidasExt> lista = entidadService.findByProperty(PersonaMidasExt.class,
					"personaMidas.id", idPersona);
			if (CollectionUtils.isEmpty(lista)) {
				return null;
			} else {
				return lista.get(0);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
			return null;
		}
	}

}
