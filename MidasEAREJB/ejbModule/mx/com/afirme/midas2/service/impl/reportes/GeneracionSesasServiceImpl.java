package mx.com.afirme.midas2.service.impl.reportes;

import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas2.dao.reportes.reporteSesas.GeneracionSesasDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.ReporteSesasArchivosId;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.GeneracionSesasDTO;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.GeneracionSesasArchivosDTO;
import mx.com.afirme.midas2.domain.reportes.reporteSesas.ReporteSesasMatrizDTO;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.reportes.GeneracionSesasService;
import mx.com.afirme.midas2.util.MidasException;



import org.apache.commons.dbutils.DbUtils;
import org.apache.log4j.Logger;


@Stateless    
public class GeneracionSesasServiceImpl implements GeneracionSesasService{
	private static final Logger LOG = Logger.getLogger(GeneracionSesasServiceImpl.class);
	private GeneracionSesasDao generacionSesasDao;
	@EJB
	private FileManagerService fileManagerService;
	
	@EJB
	private ControlArchivoFacadeRemote controlArchivo;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	public void setGeneracionSesasDao(GeneracionSesasDao generacionSesasDao) {
		this.generacionSesasDao = generacionSesasDao;
	}

	@Override
	public List<GeneracionSesasDTO> getTareasSesas(String claveNeg) throws MidasException {
		
		List<GeneracionSesasDTO> listGeneracionSesas = new ArrayList<GeneracionSesasDTO>();
		
		try{			
			listGeneracionSesas = generacionSesasDao.getListTareas(claveNeg);
		}
		catch( SQLException e){
			LogDeMidasInterfaz.log("Excepcion en BD de GeneracionSesasServiceImpl.obtenerLog" + this, Level.WARNING, e);
			onError(e);
			
		}
		catch ( Exception e ){
			LogDeMidasInterfaz.log("Excepcion general en GeneracionSesasServiceImpl.obtenerLog" + this, Level.WARNING, e);
			onError(e);
		}		
		return listGeneracionSesas;
	}

	@Override
	public GeneracionSesasDTO obtenerLog()  throws MidasException{

		GeneracionSesasDTO toGeneracionSesas = new GeneracionSesasDTO();
		
		try{			
			toGeneracionSesas = generacionSesasDao.getLast();
		}
		catch( SQLException e){
			LogDeMidasInterfaz.log("Excepcion en BD de GeneracionSesasServiceImpl.obtenerLog" + this, Level.WARNING, e);
			onError(e);
			
		}
		catch ( Exception e ){
			LogDeMidasInterfaz.log("Excepcion general en GeneracionSesasServiceImpl.obtenerLog" + this, Level.WARNING, e);
			onError(e);
		}		
		return toGeneracionSesas;
	}

	@Override
	public int iniciarJob( Date fInicial, Date fFinal, String usuario, String claveNeg ) throws MidasException {
		String spName = "SEYCOS.PKG_SESAS_DATOS.spRunSesas";
		if (claveNeg.equalsIgnoreCase("V"))
			spName = "SEYCOS.PKG_SESAS_VIDA.spRunSesas";
		LOG.info("###store a ejecutar: "+spName);
		StoredProcedureHelper storedHelper = null;
		int resp = 0;
		String idProgramacion = null;
		try {
			idProgramacion = this.revisarProcesos(claveNeg);
			if  (idProgramacion!=null && idProgramacion.length()!=0) {
				resp = 2;
			}else
			{	
				storedHelper = new StoredProcedureHelper(spName);
				storedHelper.estableceParametro( "pDate1", getStringDate(fInicial) );
				storedHelper.estableceParametro( "pDate2", getStringDate(fFinal) );
				storedHelper.estableceParametro( "pUser", usuario );
				storedHelper.estableceParametro( "pClaveNeg", claveNeg );
				LOG.info("### pDate1" + getStringDate(fInicial) );
				LOG.info("### pDate2" + getStringDate(fFinal) );
				LOG.info("### pUser" + usuario );
				LOG.info("### pClaveNeg" + claveNeg );
				resp = storedHelper.ejecutaActualizar();
			}	
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}

		return resp;
	}

	private String getStringDate( Date d ) throws Exception{
		
		String strDate = null;			
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        strDate = formatter.format(d);
		
		return strDate;
	}
	
	@Override
	public InputStream generarReporte( String listArchivos, String idGeneracion, String claveNeg ) throws MidasException {
		InputStream is = null;
		
		try {
				is = this.descargarArchivosSesas( listArchivos, idGeneracion, claveNeg);
			
		} catch (Exception e) {
			e.printStackTrace();
		}
		return is;
		
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Schedule(minute="0", hour="5,10,13,15,17,19", persistent=false)
	public void generarSesasAut(){
		String idGeneracionStr = null;
			try {
				idGeneracionStr = this.revisarPendientes("A");
				if  (idGeneracionStr!=null && idGeneracionStr.length()!=0) {
				int idGeneracion = Integer.parseInt(idGeneracionStr);
				LOG.info("Entra a la generacion de los reportes Sesas CON ");
				this.getReporte(idGeneracion, "CON");
				}
			} catch (Exception e) {
				LOG.error("Error en la generacion de los reportes Sesas: ", e);
			}
	}
	
	@Deprecated
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Override
	public void generarArchivosSesas(String claveNeg){
		String idGeneracionStr = null;
			try {
				idGeneracionStr = this.obtenerIdGeneracionVida();
				LOG.info("###############generarArchivosSesas- ya obtuvo el idgeneracion: "+idGeneracionStr);
				if  (idGeneracionStr!=null && idGeneracionStr.length()!=0) {
				int idGeneracion = Integer.parseInt(idGeneracionStr);
				this.getReporte(idGeneracion);

				}
			} catch (Exception e) {
				LOG.error("Error en la generacion de los reportes Sesas: ", e);
			}
	}
	
	@Deprecated
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	@Schedule(minute="0", hour="5,10,13,15,17,19", persistent=false)
	public void generarSesasVida(){
		String idGeneracionStr = null;
			try {
				LOG.info("Iniciando tarea generarSesasVida()...");
				idGeneracionStr = this.obtenerIdGeneracionVida();
				if  (idGeneracionStr!=null && idGeneracionStr.length()!=0) {
				int idGeneracion = Integer.parseInt(idGeneracionStr);
				
				this.getReporte(idGeneracion);
				}
			} catch (Exception e) {
				LOG.error("Error en la generacion de los reportes Sesas: ", e);
			}
	}
	
	public String getNombreReporteVida(int idArchivo, int idGeneracion){
		GeneracionSesasArchivosDTO reporteSesasArchivosDTO = new GeneracionSesasArchivosDTO();
		ReporteSesasArchivosId idArchivoSesas = new ReporteSesasArchivosId();
		idArchivoSesas.setidArchivo(idArchivo);
		try {
			idArchivoSesas.setidGeneracion(	Integer.parseInt(obtenerIdGeneracionVida()));
		} catch (NumberFormatException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		reporteSesasArchivosDTO = this.findById(idArchivoSesas);
		String nombreArchivo=reporteSesasArchivosDTO.getNomArchivo();
		LOG.info("### getNombreReporteVidaGrupoEmision  ------------ nombreArchivo obtenido "+nombreArchivo);
		return nombreArchivo;
	}
	
	private String getQueryReporte(int codigo, int grupo){
		Connection con = null;
		PreparedStatement  ps = null;
		String query="";
		
		try {
			Context ctx = new InitialContext();
	        if(ctx == null ) {
				throw new Exception("No Contexto");
	        }
	        
	        DataSource ds = (DataSource)ctx.lookup("jdbc/InterfazDataSource");
	        if (ds != null) {
	        	con = ds.getConnection();
	        }
	        
			 String sqlQuery = "select valor from midas.tcpropiedadesclobs where midas.tcpropiedadesclobs.codigo="+codigo+
			 " and midas.tcpropiedadesclobs.grupo="+grupo;
			 
	        ps = con.prepareStatement(sqlQuery);
	        ResultSet resultSet = ps.executeQuery();
	        
	        while(resultSet.next()){
     			query=resultSet.getString("VALOR");
	        }
	        
			
		} catch (Exception e) {
			LOG.error("Hubo un error al generar el archivo de Vida Grupo Emision " + e.getMessage() + " " + e.getCause());
			 return null;
	    } finally{
	    	DbUtils.closeQuietly(con);
	    	DbUtils.closeQuietly(ps);
	    }
		LOG.info("### getQueryReporte ------------"+" codigo "+codigo+" grupo "+grupo+" ####### query obtenido "+query);

	    return query;
	}
	
	public InputStream generarReporteVida(int idArchivo) throws Exception {
		String sqlQuery=getQueryReporte(idArchivo, catalogoGrupoValorService.obtenerGrupo(TIPO_CATALOGO.PROPS_CLOBS_SESAS_VIDA).getId());
			Connection con = null;
			PreparedStatement  ps = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(baos);
			InputStream is = null;
			try {
				Context ctx = new InitialContext();
		        if(ctx == null ) {
					throw new Exception("No Contexto");
		        }
		        
		        DataSource ds = (DataSource)ctx.lookup("jdbc/InterfazDataSource");

		        if (ds != null) {
		        	con = ds.getConnection();
		        }
		        ps = con.prepareStatement(sqlQuery);
		        ResultSet resultSet = ps.executeQuery();
		        
				LOG.info("### Query de reporte EJECUTADO------------");
		        while(resultSet.next()) {
	        		 out.writeBytes(
		        			resultSet.getString("columna_unica")
		        			);
	        	 
	        	}
				
				is = new ByteArrayInputStream(baos.toByteArray());
				
		        return is;
			} catch (Exception e) {
				LOG.error("Hubo un error al generar el archivo de Vida Grupo Emision " + e.getMessage() + " " + e.getCause());
				 return null;
		    } finally{
		    	DbUtils.closeQuietly(con);
		    	DbUtils.closeQuietly(ps);
		    }
			
		
	}
	
private byte[] toArrayByte( List<MensajeDTO> list ) throws Exception {
		
		byte[] bytes = null;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(baos);
			for (MensajeDTO item : list) {

				String lineSeparator = System.getProperty("line.separator");
				String element = item.getMensaje()+lineSeparator;
			    out.writeBytes(element);

			}
			bytes = baos.toByteArray();
		
		return bytes;
	}
	
	@SuppressWarnings("unchecked")
	private List<MensajeDTO> callSP(String spName,int idGeneracion,String claveNeg){
		
		String [] atributosDTO = { "mensaje" };
		String [] columnasCursor = { "COLUMNA_UNICA"};
		List<MensajeDTO> list = new ArrayList<MensajeDTO>();
		StoredProcedureHelper storedHelper;
		
		try {
			
			storedHelper = new StoredProcedureHelper(spName);			
			storedHelper.estableceParametro( "pidGeneracion", idGeneracion );
			storedHelper.estableceParametro( "pclaveNeg", claveNeg );
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);
			list = (List<MensajeDTO>)storedHelper.obtieneListaResultados();
			
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		
		return list;		
	}
	
	@SuppressWarnings("unchecked")
	private List<MensajeDTO> callSP( String spName, String tipo ){
		
		String [] atributosDTO = { "mensaje" };
		String [] columnasCursor = { "COLUMNA_UNICA"};
		List<MensajeDTO> list = new ArrayList<MensajeDTO>();
		StoredProcedureHelper storedHelper;
		
		try {
			
			storedHelper = new StoredProcedureHelper(spName);			
			storedHelper.estableceParametro( "pTipo", tipo );
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);
			list = (List<MensajeDTO>)storedHelper.obtieneListaResultados();
			
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		
		return list;		
	}
	
	private String getSPName( int spNumber ) throws Exception {
		
		String spName="";
        switch (spNumber) {
            case 1:  spName = "SEYCOS.PKG_SESAS.SP_SESAS_GRAL_IND";
                     break;
            case 2:  spName = "SEYCOS.PKG_SESAS.SP_SESAS_GRAL_FLO";
                     break;
            case 3:  spName = "SEYCOS.PKG_SESAS.SP_SESAS_EMI_IND";
                     break;
            case 4:  spName = "SEYCOS.PKG_SESAS.SP_SESAS_EMI_FLO";
                     break;
            case 5:  spName = "SEYCOS.PKG_SESAS.SP_SESAS_SIN_IND";
                     break;
            case 6:  spName = "SEYCOS.PKG_SESAS.SP_SESAS_SIN_FLO";
                     break;
            case 7:  spName = "SEYCOS.PKG_SESAS.SP_SESAS_SIN_IND_EXC";
            		break;
            case 8:  spName = "SEYCOS.PKG_SESAS.SP_SESAS_SIN_FLO_EXC";
            		break;
            case 101:spName = "SEYCOS.PKG_SESAS_VIDA.SP_SESAS_VIDA_IND_EMI";
					break;
            case 102:spName = "SEYCOS.PKG_SESAS_VIDA.SP_SESAS_VIDA_GPO_EMI";
					break;
            case 103:spName = "SEYCOS.PKG_SESAS_VIDA.SP_SESAS_VIDA_IND_SIN";
					break;
            case 104:spName = "SEYCOS.PKG_SESAS_VIDA.SP_SESAS_VIDA_GPO_SIN";
					break;
        }
        LOG.info(spName);
		return spName;
		
	}
		
	private String getAnioFechaFinal(int idGeneracion) throws MidasException{
		Date fechaFinal = new Date();
		String anio = "";
		try {
			StringBuilder queryString = new StringBuilder();
			queryString.append("SELECT TO_CHAR(FECHAFINAL) AS FECHAFINAL ");
			queryString.append("FROM MIDAS.TOGENERACIONSESAS ");
			queryString.append("WHERE ID="+idGeneracion+"");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			
			anio = (String)query.getSingleResult();
			SimpleDateFormat sdf = new SimpleDateFormat("yy");
			Calendar cal = Calendar.getInstance();
			cal.setTime(fechaFinal);
			anio = sdf.format( cal.getTime() );
		} catch (Exception e) {
			LOG.error("Error en la ejecución de obtencion del año : " + e);
		 
		}
		return anio;
	}
	
	@Deprecated
	private byte[] getReporte (  int idGeneracion )  throws Exception {
		List<MensajeDTO> list = new ArrayList<MensajeDTO>();
		byte[] bytes = null;
		int idArchivo;
		int intModo = 0;
		try {
		ReporteSesasArchivosId idArchivoSesas = new ReporteSesasArchivosId();
		GeneracionSesasArchivosDTO reporteSesasArchivosDTO = new GeneracionSesasArchivosDTO();
		boolean respuesta = false;
		String nomArchivoFrtmx="0";
		for (int i = 0; i < 4; i++) {
			 respuesta = false;
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			controlArchivoDTO.setClaveTipo("0");
			idArchivo = i + 101;
			idArchivoSesas.setidArchivo(idArchivo+intModo);
			idArchivoSesas.setidGeneracion(idGeneracion);
			reporteSesasArchivosDTO = this.findById(idArchivoSesas);
			nomArchivoFrtmx=reporteSesasArchivosDTO.getNomArchivoFrtmx().trim();
			if(existeArchivo(idArchivoSesas)){
				LOG.info("##########getReporte - el archivo: "+reporteSesasArchivosDTO.getIdGeneracion()+", "
						+reporteSesasArchivosDTO.getCveArchivo()+", "+reporteSesasArchivosDTO.getNomArchivo()+", "
						+ reporteSesasArchivosDTO.getNomArchivoFrtmx()+" ya estaba generado.");
				continue;
			}
		
			list = this.callSP(this.getSPName(idArchivo),idGeneracion ,"V");
			LOG.info("###########despues del sp para obtener bytes");
			bytes = this.toArrayByte( list );

			String NombreArchivo=reporteSesasArchivosDTO.getNomArchivo();
			
			LOG.info("##########getReporte - ya trajo el nombre del archivo a generar: "+reporteSesasArchivosDTO.getIdGeneracion()+", "
					+reporteSesasArchivosDTO.getCveArchivo()+", "+reporteSesasArchivosDTO.getNomArchivo()+", "
					+ reporteSesasArchivosDTO.getNomArchivoFrtmx());
			
			controlArchivoDTO.setNombreArchivoOriginal(NombreArchivo);
			
			controlArchivoDTO = controlArchivo.save(controlArchivoDTO);

			String IdControlArchivo = controlArchivoDTO.getIdToControlArchivo().toString();
			
			respuesta =fileManagerService.uploadFile(NombreArchivo,IdControlArchivo,bytes);
			LOG.info("##########getReporte - despues de intentar subir a fortimax");

			if (respuesta ){
				reporteSesasArchivosDTO.setNomArchivoFrtmx(IdControlArchivo);
				reporteSesasArchivosDTO.setNomArchivo(NombreArchivo);
				reporteSesasArchivosDTO.setResultado("CORRECTO");
				
				this.update(reporteSesasArchivosDTO);

			}
		}
		} catch (Exception e) {
			throw new IOException(e);
		}	
		return bytes;
	}
	
private byte[] getReporte (  int idGeneracion, String modo )  throws Exception {
		List<MensajeDTO> list = new ArrayList<MensajeDTO>();
		byte[] bytes = null;
		int idArchivo;
		int intModo = 0;
		String anio;
		try {
		anio = this.getAnioFechaFinal(idGeneracion);
		ReporteSesasArchivosId idArchivoSesas = new ReporteSesasArchivosId();
		GeneracionSesasArchivosDTO reporteSesasArchivosDTO = new GeneracionSesasArchivosDTO();
		boolean respuesta = false;
		for (int i = 0; i < 8; i++) {
			ControlArchivoDTO controlArchivoDTO = new ControlArchivoDTO();
			controlArchivoDTO.setClaveTipo("0");
			idArchivo = i + 1;
			idArchivoSesas.setidArchivo(idArchivo+intModo);
			idArchivoSesas.setidGeneracion(idGeneracion);
			reporteSesasArchivosDTO = this.findById(idArchivoSesas);
			list = this.callSP(this.getSPName(idArchivo), modo);
			bytes = this.toArrayByte( list );	
			String NombreArchivo=reporteSesasArchivosDTO.getNomArchivo();
			controlArchivoDTO.setNombreArchivoOriginal(NombreArchivo);
			controlArchivoDTO = controlArchivo.save(controlArchivoDTO);
			String IdControlArchivo = controlArchivoDTO.getIdToControlArchivo().toString();
			respuesta =fileManagerService.uploadFile(NombreArchivo,IdControlArchivo,bytes);
			if (respuesta ){
				reporteSesasArchivosDTO.setNomArchivoFrtmx(IdControlArchivo);
				reporteSesasArchivosDTO.setResultado("CORRECTO");
				this.update(reporteSesasArchivosDTO);
			}
		}
		} catch (Exception e) {
			throw new IOException(e);
		}	
		return bytes;
	}

public InputStream descargarArchivosSesas( String listArchivos, String idGeneracion, String claveNeg ) throws MidasException {
	InputStream is = null;
	
	try {
		
		is = this.descargarSesasZip(listArchivos, idGeneracion,  claveNeg);
		
	} catch (Exception e) {
		e.printStackTrace();
	}
	return is;
}


public InputStream descargarSesasZip(String listArchivosStr, String idGeneracionStr, String claveNeg) throws IOException {
	LOG.info("########descargarSesasZip - "+listArchivosStr+", "+ idGeneracionStr+", "+ claveNeg);
	String[] idArchivos = listArchivosStr.split(",");
	int idGeneracion = Integer.parseInt(idGeneracionStr);
	ReporteSesasArchivosId idArchivoSesas = new ReporteSesasArchivosId();
	ByteArrayOutputStream baos = new ByteArrayOutputStream();
	ZipOutputStream zos = new ZipOutputStream(baos);
	InputStream is = null;
	byte[] bytes = null;
	String filename = "";
	try {
		
	if (idArchivos.length > 0) {
		for (int i = 0; i < idArchivos.length; i++) {
			idArchivoSesas.setidArchivo(Integer.parseInt(idArchivos[i]));
			idArchivoSesas.setidGeneracion(idGeneracion);
			GeneracionSesasArchivosDTO reporteSesasArchivosDTO = new GeneracionSesasArchivosDTO();
			reporteSesasArchivosDTO = this.findById(idArchivoSesas);
			String IdControlArchivo = reporteSesasArchivosDTO.getNomArchivoFrtmx();
			bytes = fileManagerService.downloadFile("archivo",IdControlArchivo);
			filename = reporteSesasArchivosDTO.getNomArchivo();
			ZipEntry entry = new ZipEntry( filename );
			entry.setSize( bytes.length );
			zos.putNextEntry( entry );
			zos.write( bytes );
		}
	}
		zos.closeEntry();
		zos.close();
		is = new ByteArrayInputStream(baos.toByteArray());
		baos.close();
	} catch (Exception e) {
		throw new IOException(e);
	}	


	return is;

}


private GeneracionSesasArchivosDTO update(GeneracionSesasArchivosDTO entity) {
	try {
		GeneracionSesasArchivosDTO result = entityManager.merge(entity);
		LOG.info("Update Exitoso");
	    return result;
	} catch (RuntimeException e) {
		LOG.error("Error en la ejecución del Update: " + e);
	    throw e;
	}
}



private GeneracionSesasArchivosDTO findById(ReporteSesasArchivosId id) {
	try {
		GeneracionSesasArchivosDTO instance = entityManager.find(GeneracionSesasArchivosDTO.class, id);
		return instance;
		} catch (RuntimeException e) {
			LOG.error("Error en la ejecución del find: " + e);
		    throw e;
		}
}

public GeneracionSesasArchivosDTO obtenerArchivoSesas(ReporteSesasArchivosId id) throws Exception {
    GeneracionSesasArchivosDTO mensajeDTO = new GeneracionSesasArchivosDTO();
    List<GeneracionSesasArchivosDTO> listSesa = new ArrayList<GeneracionSesasArchivosDTO>();
	
    String spName="SEYCOS.PKG_SESAS.stpObtenerArchivoSesas";				
	String[] atributosDTO = { "cveArchivo","nomArchivo","nomArchivoFrtmx","resultado","estatus","idArchivo","idGeneracion", };
	String[] columnasCursor = { "CVEARCHIVO","NOMARCHIVO","NOMARCHIVOFRTMX","RESULTADO","ESTATUS","IDARCHIVO","IDGENERACION"};
	try {
		StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_INTERFAZ );									
		
		storedHelper.estableceParametro("pidarchivo", id.getidArchivo());
		storedHelper.estableceParametro("pidgeneracion", id.getidGeneracion());
		
		storedHelper.estableceMapeoResultados(GeneracionSesasArchivosDTO.class.getCanonicalName(), atributosDTO, columnasCursor);
		
		listSesa = storedHelper.obtieneListaResultados();
		
		for (GeneracionSesasArchivosDTO sesaObject:listSesa)
		{
			if(sesaObject.getCveArchivo()!=null){
			   mensajeDTO.setCveArchivo(sesaObject.getCveArchivo());
			}
			
			if(sesaObject.getEstatus()!=null){
			   mensajeDTO.setEstatus(sesaObject.getEstatus());	
			}
			
			if(sesaObject.getNomArchivo()!=null){
			   mensajeDTO.setNomArchivo(sesaObject.getNomArchivo());
			}
			
			if(sesaObject.getNomArchivoFrtmx()!=null){
			   mensajeDTO.setNomArchivoFrtmx(sesaObject.getNomArchivoFrtmx());
			}

			if(sesaObject.getResultado()!=null){
			   mensajeDTO.setResultado(sesaObject.getResultado());
			}

			if(sesaObject.getIdGeneracion()!=null){
		       mensajeDTO.setIdGeneracion(sesaObject.getIdGeneracion());
			}

		}
		
	} catch (Exception e) {
		throw new RuntimeException("Error en :" + spName, e);
	}	
	return mensajeDTO;
}

private String revisarProcesos(String claveNeg) throws Exception {
	String resultado = null;
	try {
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT  TO_CHAR(MAX(ID)) AS ID ");
		queryString.append("FROM MIDAS.TOGENERACIONSESAS ");
		queryString.append("WHERE RESULTADO='PENDIENTE' ");
		queryString.append("AND CLAVENEGOCIO='" + claveNeg + "'"  );
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		
		resultado = (String)query.getSingleResult();
		
	} catch (Exception e) {
		LOG.error("Error en la ejecución de las generaciones pendientes : " + e);
	 
	}
	return resultado;
}

@Deprecated
private Boolean existeArchivo(ReporteSesasArchivosId archivo) throws Exception {
	String resultado="";
	try {
		StringBuilder queryString = new StringBuilder();
		queryString.append("select nomarchivofrtmx from midas.togeneracionsesasarchivos");
		queryString.append(" where idgeneracion =" +archivo.getidGeneracion());
		queryString.append(" and idarchivo=" +archivo.getidArchivo());
		Query query = entityManager.createNativeQuery(queryString.toString());
		resultado = (String)query.getSingleResult();
		LOG.info("#existeArchivo- resultado:"+resultado);
	} catch (Exception e) {
		LOG.error("Error en la revision del archivo se de sesas:" + archivo.getidGeneracion()+":"+archivo.getidArchivo()+"\n"+e);
	 
	}
	if(resultado.trim().equalsIgnoreCase("0"))
		return false;
	else
		return true;
}

private String revisarPendientes(String claveNeg) throws Exception {
	String resultado = null;
	try {
		StringBuilder queryString = new StringBuilder();	    
		queryString.append("SELECT TO_CHAR(MAX(IDGENERACION)) AS IDGENERACION ");
		queryString.append("FROM MIDAS.TOGENERACIONSESASARCHIVOS");
		if (claveNeg.equals("A")){
			queryString.append(" WHERE NOMARCHIVOFRTMX='0' AND TIPO_REPORTE='AUT'");
		}
		else if(claveNeg.equals("V")) {
			queryString.append(" WHERE NOMARCHIVOFRTMX = '0' AND TIPO_REPORTE='VID'" );
		}
			
		Query query = entityManager.createNativeQuery(queryString.toString());
		
		resultado = (String)query.getSingleResult();
		
	} catch (Exception e) {
		LOG.error("Error en la ejecución de las generaciones pendientes : " + e);
	 
	}
	return resultado;
}




public String obtenerIdGeneracionVida() throws Exception {
	MensajeDTO mensajeDTO = null;
			
	String spName="SEYCOS.PKG_SESAS_VIDA.stpObtenerIdGeneracion";				
	String[] atributosDTO = { "mensaje" };
	String[] columnasCursor = { "IdGeneracion"};
	try {
		StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_INTERFAZ );									
		
		storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);
		
		mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		
	} catch (Exception e) {
		throw new RuntimeException("Error en :" + spName, e);
	}	
	return mensajeDTO.getMensaje() ;
}

public String obtenerIdGeneracion(String claveNeg) throws Exception {
	MensajeDTO mensajeDTO = null;
			
	String spName="SEYCOS.PKG_SESAS.stpObtenerIdGeneracion";				
	String[] atributosDTO = { "mensaje" };
	String[] columnasCursor = { "IdGeneracion"};
	try {
		StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_INTERFAZ );									
		
		storedHelper.estableceParametro("pclaveNeg", claveNeg);
		
		storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);
		
		mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		
	} catch (Exception e) {
		throw new RuntimeException("Error en :" + spName, e);
	}	
	return mensajeDTO.getMensaje() ;
}

@Override
public List<ReporteSesasMatrizDTO> generarMatriz(Integer idGeneracion)
		throws MidasException {
	StoredProcedureHelper storedHelper = null;
	String SP_MATRIZ_VAL="SEYCOS.PKG_SESAS_AUTOS_VAL.SpMatrizValidacion";
	String nombreSP = SP_MATRIZ_VAL;
	LOG.info("Encontrando la Matriz de validacion");
	try {
		storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		 
		String[] nombreParametros = { "pIdGeneracion" };
		Object[] valorParametros = { idGeneracion };
		
		for(int i = 0; i < nombreParametros.length; i ++) {
			storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
		}
		
		String [] atributosDTO = {
				"id_tipo_arch",
				"nombre_campo",
				"id_validacion",
				"descripcion",
				"midas_ind_err",
				"midas_ind_war",
				"midas_flo_err",
				"midas_flo_war",
				"midas_tot_err",
				"midas_tot_war",
				"seycos_ind_err",
				"seycos_ind_war",
				"seycos_flo_err",
				"seycos_flo_war",
				"seycos_tot_err",
				"seycos_tot_war"
		};
		
		String[] campoResultSet = {
				"ID_TIPO_ARCH",
				"NOMBRE_CAMPO",
				"ID_VALIDACION",
				"DESCRIPCION",
				"MIDAS_IND_ERR",
				"MIDAS_IND_WAR",
				"MIDAS_FLO_ERR",
				"MIDAS_FLO_WAR",
				"MIDAS_TOT_ERR",
				"MIDAS_TOT_WAR",
				"SEYCOS_IND_ERR",
				"SEYCOS_IND_WAR",
				"SEYCOS_FLO_ERR",
				"SEYCOS_FLO_WAR",
				"SEYCOS_TOT_ERR",
				"SEYCOS_TOT_WAR"
		};
		storedHelper.estableceMapeoResultados(ReporteSesasMatrizDTO.class.getCanonicalName(),atributosDTO, campoResultSet);
		
		return storedHelper.obtieneListaResultados();
		
	} catch (Exception e) {
		throw new RuntimeException("Error al generar Matriz :", e);
	}
}

}
