package mx.com.afirme.midas2.service.impl.agtSaldoMovto;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.agtSaldoMovto.AgtSaldoDao;
import mx.com.afirme.midas2.domain.agtSaldoMovto.AgtSaldo;
import mx.com.afirme.midas2.dto.agtSaldoMovto.AgtSaldoView;
import mx.com.afirme.midas2.service.agtSaldoMovto.AgtSaldoService;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class AgtSaldoServiceImpl implements AgtSaldoService {

	private AgtSaldoDao agtSaldoDao;
	
	@EJB
	public void setAgtSaldoDao(AgtSaldoDao agtSaldoDao) {
		this.agtSaldoDao = agtSaldoDao;
	}

	
	@Override
	public List<AgtSaldo> findByFilter(AgtSaldo agtSaldo) throws Exception {
		return agtSaldoDao.findByFilter(agtSaldo);
	}
	@Override
	public List<AgtSaldoView> findByFilterview(AgtSaldo arg0) throws Exception {
		return agtSaldoDao.findByFilterview(arg0);
	}
	
	@Override
	public AgtSaldoView obtenerMovimientoSaldoPorAnioMesYAgente(AgtSaldo agtSaldo) throws MidasException {
		return agtSaldoDao.obtenerMovimientoSaldoPorAnioMesYAgente(agtSaldo);
	}
	
	@Override
	public Long actualizarSaldosMesAbierto(Long idAgente, Long anio) throws Exception {
		return agtSaldoDao.actualizarSaldosMesAbierto(idAgente,anio);
	}
}
