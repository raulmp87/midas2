package mx.com.afirme.midas2.service.impl.operacionessapamis;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.amis.alertas.EnvioConsultaAcciones;
import mx.com.afirme.midas2.dao.amis.alertas.EnvioConsultaAlertas;
import mx.com.afirme.midas2.dao.amis.alertas.RespuestaSapAmisAccion;
import mx.com.afirme.midas2.dao.amis.alertas.RespuestaSapAmisAlerta;
import mx.com.afirme.midas2.dto.general.Email;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioAlertas;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioLog;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocio;
import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioConfig;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAcciones;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesRelacion;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasSapAmis;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSistemas;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.operacionessapamis.SapAmisService;
import mx.com.afirme.midas2.util.MailService;

@Stateless
public class SapAmisServiceImpl implements SapAmisService {
	private EntidadService entidadService;
	private String idCotizacion;
	private String noPoliza;

	@EJB
	private MailService mailService;
	private boolean esEmision = false;
	private SapAmisUtilsService sapAmisUtilsService; 
	
	public CatAlertasSapAmis obtenerObjetoCatAlertasSapAmis(String sapAmisAlertas) {
		CatAlertasSapAmis retorno = new CatAlertasSapAmis();
		if (sapAmisAlertas != null && !sapAmisAlertas.equals("")) {
			retorno.setDescCatAlertasSapAmis(sapAmisAlertas);
			List<CatAlertasSapAmis> sapAmisAlertasList = entidadService.findByProperty(CatAlertasSapAmis.class, "descCatAlertasSapAmis",sapAmisAlertas);
			if (sapAmisAlertasList.size() > 0) {
				retorno.setId(sapAmisAlertasList.get(0).getId());
				retorno.setCatSistemas(sapAmisAlertasList.get(0).getCatSistemas());
			} else {
				retorno.setId(new Long(0));
			}
		} else {
			retorno.setId(new Long(0));
		}
		return retorno;
	}

	/**
	  */
	public CatSistemas obtenerObjetoCatSistemas(String catSistemas) {
		CatSistemas retorno = new CatSistemas();
		if (catSistemas != null && !catSistemas.equals("")) {
			// Se valida si ya existe registro para esta alerta
			retorno.setDescCatSistemas(catSistemas);
			List<CatSistemas> catSistemasList = entidadService.findByProperty(CatSistemas.class, "descCatSistemas", catSistemas);
			if (catSistemasList.size() > 0) {
				// Ya existe registro con el ID
				retorno.setId(catSistemasList.get(0).getId());
				retorno.setDescCatSistemas(catSistemasList.get(0).getDescCatSistemas());
			} else {
				retorno.setId(new Long(0));
				// No hay registros
			}
		} else {
			retorno.setId(new Long(0));
			// LogDeMidasEJB3.log(methodName +
			// "Parametro requerido catSistemas", Level.INFO, null);
		}
		return retorno;
	}

	/**
	  */

	public SapAmisAccionesRelacion obtenerObjetoSapAmisAccionesRelacion(String vin, CatAlertasSapAmis alerta, String idCotizacion) {
		SapAmisAccionesRelacion retorno = new SapAmisAccionesRelacion();
		if (!vin.equals("") && alerta.getId() != 0) {
			// Se valida si ya existe registro para esta Relacion
			Map<String, Object> parametros = new HashMap<String, Object>();
			parametros.put("vin", vin);
			parametros.put("catAlertasSapAmis", alerta);
			parametros.put("tipo", 0);

			if (idCotizacion != null && idCotizacion != "") {
				parametros.put("idCotizacion", Long.parseLong(idCotizacion));
				retorno.setIdCotizacion(Long.parseLong(idCotizacion));
			}
			retorno.setCatAlertasSapAmis(alerta);
			retorno.setVin(vin);
			try{
				List<SapAmisAccionesRelacion> sapAmisRelacionList = entidadService.findByProperties(SapAmisAccionesRelacion.class, parametros);
				if (sapAmisRelacionList.size() > 0) {
					// Ya existe registro con el ID
					retorno.setIdSapAmisAccionesRelacion(sapAmisRelacionList.get(0).getIdSapAmisAccionesRelacion());
					retorno.setIdCotizacion(sapAmisRelacionList.get(0).getIdCotizacion());
				} else {
					retorno.setIdCotizacion(new Long(0));
					// No hay registros
				}
			}catch(Exception e){
				retorno.setTipo(0);
				retorno.setIdSapAmisAccionesRelacion((Long)entidadService.saveAndGetId(retorno));
			}
		} else {
			retorno.setIdCotizacion(new Long(0));
			// LogDeMidasEJB3.log(methodName + "Parametro requerido",
			// Level.INFO, null);
		}
//		 LogDeMidasEJB3.log(methodName + "retorno: " + retorno.getValue(), Level.INFO, null);
		return retorno;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public ArrayList<RespuestaSapAmisAlerta> getAlertasSapAmisEmision(RespuestaSapAmisAlerta respuestaAlerta, PolizaDTO poliza) {
		this.esEmision = true;
		noPoliza = poliza.getNumeroPolizaFormateada();
		CatAlertasSapAmis ra = this.obtenerObjetoCatAlertasSapAmis(respuestaAlerta.getAlerta());
		String idNegocio = poliza.getCotizacionDTO().getNegocioDerechoPoliza().getNegocio().getIdToNegocio() + "";
		Map<String, Object> pAlertasEmialNegocio = new LinkedHashMap<String, Object>();
		pAlertasEmialNegocio.put("idNegocio", Long.parseLong(idNegocio));
		pAlertasEmialNegocio.put("estatus", 0);
		pAlertasEmialNegocio.put("catAlertasSapAmis", ra);
		List<EmailNegocioAlertas> lAlertasEmialNegocio = entidadService.findByProperties(EmailNegocioAlertas.class, pAlertasEmialNegocio);

		String corresoNotificar = "";
		List<EmailNegocioConfig> lEmailNegocioConfig = null;
		for (int cAlertasEmailNegocio = 0; cAlertasEmailNegocio < lAlertasEmialNegocio.size(); cAlertasEmailNegocio++) {
			Map<String, Object> pEmailNegocioConfig = new LinkedHashMap<String, Object>();
			pEmailNegocioConfig.put("alertasEmailNegocio", lAlertasEmialNegocio.get(cAlertasEmailNegocio));
			pEmailNegocioConfig.put("estatus", 0);
			lEmailNegocioConfig = entidadService.findByProperties(EmailNegocioConfig.class, pEmailNegocioConfig);
			corresoNotificar = obtenerCorreosNotificar(lEmailNegocioConfig);
		}
		if (!corresoNotificar.equals("")) {
			List<String> address = new ArrayList<String>();
			address.add(corresoNotificar.substring(0,corresoNotificar.length() - 1));
			String cuerpoEmail = crearEmailAlerta(ra);
			EmailNegocioLog logEmail = new EmailNegocioLog();
			logEmail.setFecha(new Date());
			logEmail.setCuerpo(cuerpoEmail);
			logEmail.setAlerta(ra);
			logEmail.setIdNegocio(Long.parseLong(idNegocio));
			logEmail.setIdSapAmisAccionesRelacion(new Long(0));
			for (int cEmailNegocioConfig = 0; cEmailNegocioConfig < lEmailNegocioConfig.size(); cEmailNegocioConfig++) {
				logEmail.setEmail(lEmailNegocioConfig.get(cEmailNegocioConfig).getEmailNegocio().getEmail());
				try {
					entidadService.saveAndGetId(logEmail);
				} catch (Exception ex) {

				}
			}
			mailService.sendMail(address, "Alerta SAP-AMIS", cuerpoEmail);
		}
		return null;
	}

	public String obtenerCorreosNotificar(
			List<EmailNegocioConfig> lEmailNegocioConfig) {
		StringBuilder correosNotificar = new StringBuilder("");
		for (int cEmailNegocioConfig = 0; cEmailNegocioConfig < lEmailNegocioConfig.size(); cEmailNegocioConfig++) {
			EmailNegocio emailNegocio = lEmailNegocioConfig.get(cEmailNegocioConfig).getEmailNegocio();
			if (emailNegocio.getEstatus() == 0) {
				Email email = emailNegocio.getEmail();
				correosNotificar.append(email.getEmail()).append(",");
			}
		}
		return correosNotificar.toString();
	}

	@Override
	public ArrayList<RespuestaSapAmisAlerta> getAlertasSapAmisCotizacion(ArrayList<EnvioConsultaAcciones> respuesConsultaLinea) {
		ArrayList<RespuestaSapAmisAlerta> sapAmisRespuestaAlertas = new ArrayList<RespuestaSapAmisAlerta>();
		for (int x = 0; x < respuesConsultaLinea.size(); x++) {
			List<SapAmisAccionesRelacion> lista = new ArrayList<SapAmisAccionesRelacion>();
			this.idCotizacion = respuesConsultaLinea.get(x).getIdCotizacion();
			if (respuesConsultaLinea == null || respuesConsultaLinea.size() == 0) {
				// //LogDeMidasEJB3.log(methodName +
				// "Sin alertas en el Ws o error", Level.INFO, null);
				List<SapAmisAccionesRelacion> listaProv = new ArrayList<SapAmisAccionesRelacion>();
				Map<String, Object> parametros = new LinkedHashMap<String, Object>();
				parametros.put("vin", respuesConsultaLinea.get(x).getVin());
				if (idCotizacion != null && !idCotizacion.equals("")) {
					parametros.put("idCotizacion", Long.parseLong(idCotizacion));
				}
				listaProv = entidadService.findByProperties(SapAmisAccionesRelacion.class, parametros);
				for (int y = 0; y < listaProv.size(); y++) {
					RespuestaSapAmisAlerta ra = new RespuestaSapAmisAlerta();
					ra.setAlerta(listaProv.get(y).getCatAlertasSapAmis().getDescCatAlertasSapAmis());
					ra.setSistema(listaProv.get(y).getCatAlertasSapAmis().getCatSistemas().getDescCatSistemas());
					lista.add(alimentarCatalogoAlertas(ra, respuesConsultaLinea.get(x).getVin()));
				}
			} else {
				for (int y = 0; y < respuesConsultaLinea.size(); y++) {
					for (int z = 0; z < respuesConsultaLinea.get(y).getAlertas().length; z++) {
						lista.add(alimentarCatalogoAlertas(respuesConsultaLinea.get(y).getAlertas()[z], respuesConsultaLinea.get(x).getVin()));
					}
				}
			}

			for (int i = 0; i < lista.size(); i++) {
				Map<String, Object> parametros = new HashMap<String, Object>();
				parametros.put("sapAmisAccionesRelacion", lista.get(i));
				parametros.put("estatus", 0);
				List<SapAmisAcciones> lista2 = entidadService.findByProperties(SapAmisAcciones.class, parametros);
				RespuestaSapAmisAlerta sara = new RespuestaSapAmisAlerta();
				sara.setIdAlerta(lista.get(i).getCatAlertasSapAmis().getId());
				sara.setAlerta(lista.get(i).getCatAlertasSapAmis().getDescCatAlertasSapAmis());
				sara.setSistema(lista.get(i).getCatAlertasSapAmis().getCatSistemas().getDescCatSistemas());
				sara.setVin(lista.get(i).getVin());
				sara.setIdRelacion(lista.get(i).getIdSapAmisAccionesRelacion());

				ArrayList<RespuestaSapAmisAccion> alsaraa = new ArrayList<RespuestaSapAmisAccion>();

				for (int j = 0; j < lista2.size(); j++) {
					RespuestaSapAmisAccion saraa = new RespuestaSapAmisAccion();
					saraa.setIdAccion(lista2.get(j).getIdSapAmisAcciones());
					saraa.setDescAccion(lista2.get(j).getDescripcionAccion());
					alsaraa.add(saraa);
				}
				sara.setAcciones(alsaraa);
				sapAmisRespuestaAlertas.add(sara);
			}
		}
		return sapAmisRespuestaAlertas;
	}

	/**
	 * @author Eduardosco 2015-09-02 Descripción: Obtiene los datos de Usuario y
	 *         password de la BD para los WS del Sistema SAP
	 */
	private String[] getSapUserPass() {
		String[] login = sapAmisUtilsService.obtenerAccesos();
		return login;
	}

	@Override
	public EnvioConsultaAlertas getInfoEnvioConsultaAlertas(String consultaNuemeroSerie) {
		if (consultaNuemeroSerie != null) {
			String[] login = getSapUserPass();
			String[] numerosDeSerie;
			if (consultaNuemeroSerie.contains("|")) {
				consultaNuemeroSerie = consultaNuemeroSerie.replace("|", "_");
				numerosDeSerie = new String[consultaNuemeroSerie.split("_").length];
				for (int q = 0; q < consultaNuemeroSerie.split("_").length; q++) {
					numerosDeSerie[q] = consultaNuemeroSerie.split("_")[q];
				}
			} else {
				numerosDeSerie = new String[1];
				numerosDeSerie[0] = consultaNuemeroSerie;
			}
			EnvioConsultaAlertas eca = new EnvioConsultaAlertas();
			eca.setUser(login[0]);
			eca.setPass(login[1]);
			eca.setVin(numerosDeSerie);
			return eca;
		}
		return null;
	}

	private SapAmisAccionesRelacion alimentarCatalogoAlertas(RespuestaSapAmisAlerta respuestaAlerta, String vin) {
		CatAlertasSapAmis alerta = this.obtenerObjetoCatAlertasSapAmis(respuestaAlerta.getAlerta());
		if (alerta.getId() == 0) {
			CatSistemas sistema = this.obtenerObjetoCatSistemas(respuestaAlerta.getSistema());
			if (sistema.getId() == 0 && !(sistema.getDescCatSistemas().equals("") || sistema.getDescCatSistemas().equals(null))) {
				sistema.setDescCatSistemas(respuestaAlerta.getSistema());
				sistema.setId((Long) entidadService.saveAndGetId(sistema));
				alerta.setDescCatAlertasSapAmis(respuestaAlerta.getAlerta());
				alerta.setCatSistemas(sistema);
				alerta.setId((Long) entidadService.saveAndGetId(alerta));
			}else{
				alerta.setCatSistemas(sistema);
			}
			alerta.setId((Long) entidadService.saveAndGetId(alerta));
		}
		SapAmisAccionesRelacion relacion = getRelacionCatAlertasSapAmis(vin, alerta);
		return relacion;
	}

	private SapAmisAccionesRelacion getRelacionCatAlertasSapAmis(String vin,CatAlertasSapAmis alerta) {
		SapAmisAccionesRelacion relacion = this.obtenerObjetoSapAmisAccionesRelacion(vin, alerta, this.idCotizacion);
		if (relacion.getIdSapAmisAccionesRelacion() == 0) {
			if (this.idCotizacion != null && this.idCotizacion != "") {
				relacion.setIdCotizacion(Long.parseLong(this.idCotizacion));
			}
//			LogDeMidasEJB3.log(methodName + "relacion1: " + relacion.getValue(), Level.INFO, null);
			relacion.setIdSapAmisAccionesRelacion((Long) entidadService.saveAndGetId(relacion));
//			LogDeMidasEJB3.log(methodName + "relacion2: " + relacion.getValue(), Level.INFO, null);
			try {
				CotizacionDTO cotizacion = (CotizacionDTO) entidadService.findByProperty(CotizacionDTO.class, "idToCotizacion",Long.parseLong(this.idCotizacion)).get(0);
				long idNegocio = cotizacion.getNegocioDerechoPoliza().getNegocio().getIdToNegocio();
				Map<String, Object> pAlertasEmialNegocio = new LinkedHashMap<String, Object>();
				pAlertasEmialNegocio.put("idNegocio", idNegocio);
				pAlertasEmialNegocio.put("estatus", 0);
				pAlertasEmialNegocio.put("catAlertasSapAmis", alerta);
				List<EmailNegocioAlertas> lAlertasEmialNegocio = entidadService.findByProperties(EmailNegocioAlertas.class,pAlertasEmialNegocio);
				String corresoNotificar = "";
				StringBuilder correosNotificar = new StringBuilder("");
				List<EmailNegocioConfig> lEmailNegocioConfig = null;
				for (int cAlertasEmailNegocio = 0; cAlertasEmailNegocio < lAlertasEmialNegocio.size(); cAlertasEmailNegocio++) {
					Map<String, Object> pEmailNegocioConfig = new LinkedHashMap<String, Object>();
					pEmailNegocioConfig.put("alertasEmailNegocio", lAlertasEmialNegocio.get(cAlertasEmailNegocio));
					pEmailNegocioConfig.put("estatus", 0);
					lEmailNegocioConfig = entidadService.findByProperties(EmailNegocioConfig.class, pEmailNegocioConfig);
					for (int cEmailNegocioConfig = 0; cEmailNegocioConfig < lEmailNegocioConfig.size(); cEmailNegocioConfig++) {
						EmailNegocio emailNegocio = lEmailNegocioConfig.get(cEmailNegocioConfig).getEmailNegocio();
						if (emailNegocio.getEstatus() == 0) {
							Email email = emailNegocio.getEmail();
							correosNotificar.append(email.getEmail()).append(",");
						}
					}
				}
				corresoNotificar = correosNotificar.toString();
				Map<String, Object> pCorreoEnviado = new LinkedHashMap<String, Object>();
				pCorreoEnviado.put("idSapAmisAccionesRelacion", idNegocio);
				if (!corresoNotificar.equals("") && entidadService.findByProperties(EmailNegocioLog.class, pCorreoEnviado).size() == 0) {
					List<String> address = new ArrayList<String>();
					address.add(corresoNotificar.substring(0, corresoNotificar.length() - 1));
					// Antes de enviar mail
					String cuerpoEmail = crearEmailAlerta(relacion.getCatAlertasSapAmis());
					EmailNegocioLog logEmail = new EmailNegocioLog();
					logEmail.setFecha(new Date());
					logEmail.setCuerpo(cuerpoEmail);
					logEmail.setAlerta(alerta);
					logEmail.setIdNegocio(idNegocio);
					logEmail.setIdSapAmisAccionesRelacion(relacion.getIdSapAmisAccionesRelacion());
					for (int cEmailNegocioConfig = 0; cEmailNegocioConfig < lEmailNegocioConfig.size(); cEmailNegocioConfig++) {
						logEmail.setEmail(lEmailNegocioConfig.get(cEmailNegocioConfig).getEmailNegocio().getEmail());
						entidadService.saveAndGetId(logEmail);
					}
					mailService.sendMail(address, "Alerta SAP-AMIS", cuerpoEmail);
				}
			} catch (Exception ex) {
				
			}
		}
		return relacion;
	}

	/**
	 * @author Luis Ibarra * 2015-11-18
	 * @return Regresa la cadena html para el email de accion
	 */
	private String crearEmailAlerta(CatAlertasSapAmis alerta) {
		String html = "<html>";
		html += "<head>";
		html += "</head>";
		html += "<body>";
		html += "<h1>";
		if (this.esEmision) {
			html += "Se ha encontrado una alerta durante la emisi\u00F3n ";
			html += "POL-" + this.noPoliza + "";
		} else {
			html += "Se ha encontrado una alerta durante la cotizaci\u00F3n ";
			html += "COT-" + this.idCotizacion + "";
		}
		html += "</h1>";
		html += "<h2>";
		html += "Detalle de la alerta.";
		html += "</h2>";
		html += "<p>";
		html += "Sistema: " + alerta.getCatSistemas().getDescCatSistemas()
				+ "<br>";
		html += "Descripci\u00F3n: " + alerta.getDescCatAlertasSapAmis() + "<br>";
		html += "</p>";
		html += "<p>Favor de analizar el caso y tomar las acciones pertinentes, as\u00ed como recabar mayor informaci\u00F3n con el emisor.</p>";
		html += "</body>";
		html += "</html>";
		return html;
	}

	@EJB
	public void setSapAmisUtilsService(SapAmisUtilsService sapAmisUtilsService) {
		this.sapAmisUtilsService = sapAmisUtilsService;
	}
}