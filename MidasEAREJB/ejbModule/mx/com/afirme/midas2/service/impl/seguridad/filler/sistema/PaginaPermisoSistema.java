package mx.com.afirme.midas2.service.impl.seguridad.filler.sistema;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

public class PaginaPermisoSistema {
	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoSistema(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
		
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		//Permisos  0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 = AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE, 15=TE ,16=RC, 17=EM ,18=IM, 19=ET
		
		PaginaPermiso pp;
	
		//**************SISTEMA
		
		//Paginas default
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("inicio.jsp", "/MidasWeb/sistema/inicio.do"));  //Pagina bienvenida
		pp.getPermisos().add(listaPermiso.get(6)); //VD
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/sistema/cargaCombo/listar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/sistema/cargaCombo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/estado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/ciudad.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/colonia.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/codigoPostal.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/municipio.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subRamo.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subGiro.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subGiroRC.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoEquipoContratista.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoMaquinaria.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoEquipoElectronico.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoRecipientePresion.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/monedaTipoPoliza.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/formaPagoMoneda.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredor.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cuentaBancoPesos.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cuentaBancoDolares.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/contacto.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/download/descargarArchivo.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/gestionPendientes/listarPendientes.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mail/enviarMail.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/mensaje/verMensaje.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesDanios.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesReaseguro.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesSiniestros.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/remover.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("acerca.jsp","/MidasWeb/sistema/mensajePendiente/mostrarAcerca.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/uploadHandler.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/getInfoHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/getIdHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cargaMenu.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/uploadHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getInfoHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getIdHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getFileInformation.do"));  
		listaPaginaPermiso.add(pp); 
		/////////////////////////////////USUARIO//////////////////////////////////////////////////////////

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("inicio.jsp","/MidasWeb/usuario/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/usuario/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("login.jsp","/MidasWeb/sistema/logout.do"));  
		listaPaginaPermiso.add(pp); 

		//*******USUARIO

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("inicio.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("login.jsp","/MidasWeb/sistema/logout.do"));  
		listaPaginaPermiso.add(pp); 
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
	    
		/*FIN PERMISOS TEMPORALES PARA EL SISTEMA*/
		/*****************************************************/
	    
		/*****************************************************/
		
		return this.listaPaginaPermiso;
	}

}
