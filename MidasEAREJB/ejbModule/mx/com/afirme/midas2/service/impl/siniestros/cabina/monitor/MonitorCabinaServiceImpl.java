package mx.com.afirme.midas2.service.impl.siniestros.cabina.monitor;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.siniestros.cabina.monitor.AlertaMonitorCabinaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.monitor.AlertaMonitorCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.monitor.AlertaMonitorCabina.CampoInicioConteo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina_;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.cita.CitaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas.TipoDireccion;
import mx.com.afirme.midas2.dto.siniestros.cabina.monitor.ReporteMonitorDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.monitor.ReporteMonitorDTO.EnumSeccion;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.monitor.MonitorCabinaService;
import mx.com.afirme.midas2.utils.CommonUtils;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Period;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class MonitorCabinaServiceImpl implements MonitorCabinaService {

	public static final Logger									log								= Logger.getLogger(MonitorCabinaServiceImpl.class);

	@EJB
	private AlertaMonitorCabinaDao								alertaDao;

	@EJB
	private EntidadService										entidadService;

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService										usuarioService;

	@EJB
	private ListadoService										listadoService;

	private Map<String, String>									estatusMap						= null;

	// sistemaContext.getRolSubDirectorSiniestrosAutos();
	static final String											rolSubDirectorSiniestrosAutos	= "Rol_M2_SubDirector_Siniestros_Autos";
	// sistemaContext.getRolCoordinadorCabinajustes();
	static final String											rolCoordinadorCabinajustes		= "Rol_M2_Coordinador_Cabina_Ajustes_Siniestros";
	
	static final String rolCabinero = "Rol_M2_Cabinero";

	private Map<EnumSeccion, Map<String, AlertaMonitorCabina>>	alertasMap;

	@Override
	public List<ReporteMonitorDTO> getReporteSiniestrosArribo() {
		return obtenerReporteMonitorDTOs(EnumSeccion.ARRIBO);
	}

	@Override
	public List<ReporteMonitorDTO> getReporteSiniestrosAsignacion() {
		return obtenerReporteMonitorDTOs(EnumSeccion.ASIGNACION);
	}

	@Override
	public List<ReporteMonitorDTO> getReporteSiniestrosTermino() {
		return obtenerReporteMonitorDTOs(EnumSeccion.TERMINO);
	}

	@Override
	public List<ReporteMonitorDTO> getReporteSiniestros() {
		return obtenerReporteMonitorDTOs(EnumSeccion.TODOS);
	}

	private void cargarEstatusMap() {
		if (estatusMap == null) {
			try {
				estatusMap = listadoService
						.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_REPORTE_CABINA);
			} catch (Exception e) {
				log.error("Error al cargar catalogo valor fijo ESTATUS_REPORTE_CABINA");
			}
		}
	}

	private List<ReporteMonitorDTO> obtenerReporteMonitorDTOs(
			EnumSeccion seccion) {
		List<ReporteMonitorDTO> reporteSiniestrosARetornar = new ArrayList<ReporteMonitorDTO>();
		String usuarioRol = usuarioService
				.tieneRolUsuarioActual(rolSubDirectorSiniestrosAutos) ? rolSubDirectorSiniestrosAutos
				: usuarioService
						.tieneRolUsuarioActual(rolCoordinadorCabinajustes) ? rolCoordinadorCabinajustes
						: usuarioService
							.tieneRolUsuarioActual(rolCabinero) ? rolCabinero
								:null;

		// usuarioRol = rolCoordinadorCabinajustes; //Descomentar si todavia no
		// se tienen los roles configurados

		if (usuarioRol == null) {
			log.error("usuario no tiene rol valido para obtener reporteMonitorDTOs");
			return null;
		}
		alertasMap = cargarAlertasMap(usuarioRol);

		List<ReporteCabina> reportes = obtenerReportesEnProcesoDeHoy(seccion);		
		for (ReporteCabina reporteCabina : reportes) {		
			// Generar DTO
			ReporteMonitorDTO reporteMonitorDTOAAgregar = generarReporteMonitorDTO(reporteCabina);
			// Si DTO no es null, agregar a lista de retorno
			if (reporteMonitorDTOAAgregar != null) {
				reporteSiniestrosARetornar.add(reporteMonitorDTOAAgregar);
			}
		}
		return reporteSiniestrosARetornar;
	}

	private String calcularCitaOCruzero(ReporteCabina reporteCabina) {
		AlertaMonitorCabina alerta = new AlertaMonitorCabina();
		alerta.setTieneCita(reporteCabina.getCitaReporteCabina() != null
				&& reporteCabina.getCitaReporteCabina().getId() != null);
		return alerta.getCitaCruzero();
	}

	private Map<String, Date> obtenerTiempoParaAlarma(
			ReporteCabina reporteCabina, AlertaMonitorCabina alertaAAplicar) {
		Date tiempoParaAlarmaARegresar = null;
		Date campoInicioConteo = null;

		try {
			Long tiempoParaAlerta = alertaAAplicar.getTiempoParaAlerta();
			Boolean cuentaRegresiva = alertaAAplicar.getCuentaRegresiva();

			CitaReporteCabina cita = alertaAAplicar.getTieneCita() ? reporteCabina
					.getCitaReporteCabina() : null;

			CampoInicioConteo enumCampoInicioConteo = CampoInicioConteo
					.valueOf(alertaAAplicar.getCampoInicioConteo());

			switch (enumCampoInicioConteo) {
			case FECHA_HORA_REPORTE:
				campoInicioConteo = reporteCabina.getFechaHoraReporte();
				break;
			case FECHA_HORA_CITA:
				if (cita == null) {
					throw new NegocioEJBExeption("MON_CAB_00",
							"Error de Alerta, marca tieneCita=false pero pide datos de la cita");
				}
				campoInicioConteo = CommonUtils.getFechaHora(
						cita.getFechaCita(), cita.getHoraCita());
				break;
			case FECHA_HORA_ASIGNACION:
				campoInicioConteo = reporteCabina.getFechaHoraAsignacion();
				break;
			case FECHA_HORA_CITA_CONTACTO:
				campoInicioConteo = reporteCabina.getFechaHoraContacto();
				break;
			case FECHA_HORA_CONTACTO:
				campoInicioConteo = reporteCabina.getFechaHoraContacto();
				break;
			case FECHA_HORA_CITA_TERMINACION:
				if (cita == null) {
					throw new NegocioEJBExeption("MON_CAB_00",
							"Error de Alerta, marca tieneCita=false pero pide datos de la cita");
				}
				campoInicioConteo = CommonUtils.getFechaHora(
						cita.getFechaTermino(), cita.getHoraTermino());
				break;
			}
			tiempoParaAlarmaARegresar = calculoTiempoParaAlarma(
					campoInicioConteo, tiempoParaAlerta, cuentaRegresiva);
		} catch (Exception e) {
			log.error("Error al obtener tiempo para la alerta");
		}
		Map<String, Date> returnDates = new HashMap<String, Date>();
		returnDates.put("tiempoAlarma", tiempoParaAlarmaARegresar);
		returnDates.put("inicioConteo", campoInicioConteo);
		return returnDates;
	}

	@Override
	public ReporteMonitorDTO generarReporteMonitorDTO(
			ReporteCabina reporteCabina) {
		ReporteMonitorDTO reporteMonitorDTOARetornar = new ReporteMonitorDTO();

		EnumSeccion enumSeccionReporteCabina = obtenerSeccion(reporteCabina);
		if (enumSeccionReporteCabina == null) {
			return null;
		}
		String citaCruzero = calcularCitaOCruzero(reporteCabina);
		Map<String, AlertaMonitorCabina> citaCruceroAlertaMapTemp = alertasMap
				.get(enumSeccionReporteCabina);
		AlertaMonitorCabina alertaAAplicar = citaCruceroAlertaMapTemp
				.get(citaCruzero);
		// si no se encuentra alerta, buscar para la siguiente
		if (alertaAAplicar == null) {
			return null;
		}

		// Empezar seteo de reporteMonitorDTO
		reporteMonitorDTOARetornar.setId(reporteCabina.getId());
		reporteMonitorDTOARetornar.setSeccion(enumSeccionReporteCabina);
		reporteMonitorDTOARetornar.setCitaCruzero(citaCruzero);

		if (citaCruzero.equals(AlertaMonitorCabina.CITA)) {
			CitaReporteCabina cita = reporteCabina.getCitaReporteCabina();
			reporteMonitorDTOARetornar.setFechaHoraCita(CommonUtils
					.getFechaHora(cita.getFechaCita(), cita.getHoraCita()));
		} else {
			// reporteMonitorDTOARetornar.setFechaHoraCita(new Date());
		}
		reporteMonitorDTOARetornar
				.setTipoAlarma(alertaAAplicar.getTipoAlerta());

		Map<String, Date> fechasCalculadas = obtenerTiempoParaAlarma(
				reporteCabina, alertaAAplicar);
		reporteMonitorDTOARetornar.setTiempoParaAlarma(fechasCalculadas
				.get("tiempoAlarma"));

		LugarSiniestroMidas lugarAtencionOcurrido = obtenerLugar(reporteCabina);

		reporteMonitorDTOARetornar.setNumeroReporte(reporteCabina
				.getNumeroReporte());
		reporteMonitorDTOARetornar.setAnioReporte(reporteCabina
				.getAnioReporte());
		reporteMonitorDTOARetornar.setConsecutivo(reporteCabina
				.getConsecutivoReporte());
		reporteMonitorDTOARetornar.setOficina(reporteCabina.getOficina());
		reporteMonitorDTOARetornar.setFechaHoraReporte(reporteCabina
				.getFechaHoraReporte());
		// Este campo se actualiza en la vista
		reporteMonitorDTOARetornar.setTiempoTranscurido(fechasCalculadas
				.get("inicioConteo"));
		reporteMonitorDTOARetornar.setPersonaReporta(reporteCabina
				.getPersonaReporta());
		reporteMonitorDTOARetornar.setTelefono('(' + reporteCabina.getLadaTelefono() + ") " + reporteCabina.getTelefono());
		if (lugarAtencionOcurrido != null) {
			reporteMonitorDTOARetornar.setCiudad(lugarAtencionOcurrido.getCiudad() != null ? 
					lugarAtencionOcurrido.getCiudad().getDescripcion() : "NO INFO");
			reporteMonitorDTOARetornar.setUbicacion(obtenerUbicacionString(lugarAtencionOcurrido));
		}

		if (reporteCabina.getAjustador() != null && reporteCabina.getAjustador().getPersonaMidas() != null) {
			reporteMonitorDTOARetornar.setAjustador(reporteCabina
					.getAjustador().getNombrePersona());
		}
		if (reporteCabina.getFechaHoraContacto() != null) {
			reporteMonitorDTOARetornar.setFechaHoraContacto(reporteCabina
					.getFechaHoraContacto());
		}
		if (reporteCabina.getPoliza() != null) {
			reporteMonitorDTOARetornar.setAsegurado(reporteCabina.getPoliza()
					.getNombreAsegurado());
			reporteMonitorDTOARetornar.setAgente(reporteCabina.getNombreAgente());
		}
		
		if (reporteCabina.getEstatus() != null) {
			reporteMonitorDTOARetornar.setEstatus(estatusMap.get(reporteCabina.getEstatus().toString()));
		}
		
		DateTime startTime = new DateTime(reporteMonitorDTOARetornar.getTiempoTranscurido().getTime());
		DateTime endTime = TimeUtils.now();
		Period period = new Period(startTime, endTime);
		
		reporteMonitorDTOARetornar.setTiempoExcel(period.getHours() + ":" + period.getMinutes() + ":" + period.getSeconds());

		return reporteMonitorDTOARetornar;
	}

	private LugarSiniestroMidas obtenerLugar(ReporteCabina reporteCabina) {
		return reporteCabina.getLugarAtencion();
	}

	private String obtenerUbicacionString(LugarSiniestroMidas lugar) {
		StringBuilder ubicacionARetornar = new StringBuilder();
		String zona = lugar.getZona();
		if (zona.equals(TipoDireccion.CD.toString())) {
			ubicacionARetornar.append(lugar.getCalleNumero());
			if (!lugar.getIdColoniaCheck()) {
				if(lugar.getColonia() != null){
					ubicacionARetornar.append(" "
						+ lugar.getColonia().getDescripcion());
				}
				ubicacionARetornar.append(" C.P. " + lugar.getCodigoPostal());

			} else {				
				ubicacionARetornar.append(" " + lugar.getOtraColonia());
				ubicacionARetornar.append(" C.P. "
						+ lugar.getCodigoPostalOtraColonia());
			}
		} else if (zona.equals(TipoDireccion.CA.toString())) {
			ubicacionARetornar.append(" " + lugar.getTipoCarretera());
		}

		return ubicacionARetornar.toString();
	}

	private EnumSeccion obtenerSeccion(ReporteCabina reporte) {
		EnumSeccion seccionARetornar = null;

		if (reporte.getFechaHoraAsignacion() == null) {
			seccionARetornar = EnumSeccion.ASIGNACION;
		} else if (reporte.getFechaHoraContacto() == null) {
			seccionARetornar = EnumSeccion.ARRIBO;
		} else if (reporte.getFechaHoraTerminacion() == null) {
			seccionARetornar = EnumSeccion.TERMINO;
		}
		return seccionARetornar;
	}

	private Map<EnumSeccion, Map<String, AlertaMonitorCabina>> cargarAlertasMap(
			String usuarioRol) {
		Map<EnumSeccion, Map<String, AlertaMonitorCabina>> enumARetornar = new HashMap<EnumSeccion, Map<String, AlertaMonitorCabina>>();

		try {
			List<AlertaMonitorCabina> alertas = alertaDao
					.obtenerAlertasPorRol(usuarioRol);
			for (AlertaMonitorCabina alerta : alertas) {
				EnumSeccion enumSeccion = EnumSeccion.valueOf(
						EnumSeccion.class, alerta.getSeccionMonitor());
				Map<String, AlertaMonitorCabina> citaCuceroMapa;
				if (enumARetornar.containsKey(enumSeccion)) {
					citaCuceroMapa = enumARetornar.get(enumSeccion);
				} else {
					citaCuceroMapa = new HashMap<String, AlertaMonitorCabina>();
				}
				citaCuceroMapa.put(alerta.getCitaCruzero(), alerta);
				enumARetornar.put(enumSeccion, citaCuceroMapa);
			}
		} catch (Exception e) {
			log.error("Error al obtener alertas");
		}
		return enumARetornar;
	}

	private List<ReporteCabina> obtenerReportesEnProcesoDeHoy(
			EnumSeccion seccion) {
		List<ReporteCabina> reportesARetornar = new ArrayList<ReporteCabina>();
		List<ReporteCabina> reportesTemp = new ArrayList<ReporteCabina>();
		// estatus en listado service es "En Proceso"
		String valorABuscar = "En Proceso";
		// Inicio - Sacar codigo de catalogo valor fijo para estatus En Progreso
		Integer enProcesoEstatusId = 1;
		try {
			cargarEstatusMap();

			for (String key : estatusMap.keySet()) {
				if (estatusMap.get(key).equalsIgnoreCase(valorABuscar)) {
					enProcesoEstatusId = Integer.valueOf(key);
				}
			}
		} catch (Exception e) {
			log.error("Error al buscar en proceso en lista de catalogo");
			return null;
		}
		// Fin - Sacar codigo de catalogo valor fijo para estatus En Progreso

		Map<String, Object> properties = new HashMap<String, Object>();

		properties.put(ReporteCabina_.estatus.getName(), enProcesoEstatusId);
		StringBuilder queryWhere = new StringBuilder();
		switch (seccion) {
		case ASIGNACION:
			properties.put(ReporteCabina_.fechaHoraAsignacion.getName(), null);
			properties.put(ReporteCabina_.fechaHoraContacto.getName(), null);
			properties.put(ReporteCabina_.fechaHoraTerminacion.getName(), null);
			break;
		case ARRIBO:
			properties.put(ReporteCabina_.fechaHoraContacto.getName(), null);
			properties.put(ReporteCabina_.fechaHoraTerminacion.getName(), null);
			queryWhere.append(" and model."
					+ ReporteCabina_.fechaHoraAsignacion.getName()
					+ " IS NOT NULL");
			break;
		case TERMINO:
			properties.put(ReporteCabina_.fechaHoraTerminacion.getName(), null);
			queryWhere.append(" and model."
					+ ReporteCabina_.fechaHoraContacto.getName()
					+ " IS NOT NULL");
			queryWhere.append(" and model."
					+ ReporteCabina_.fechaHoraAsignacion.getName()
					+ " IS NOT NULL");
			break;
		default:
			break;
		}

		reportesARetornar = entidadService.findByProperties(
				ReporteCabina.class, properties);

		// Si el reporte tiene cita, la cita tiene que ser para hoy
		for (ReporteCabina reporteCabina : reportesARetornar) {
			CitaReporteCabina cita = reporteCabina.getCitaReporteCabina();
			if (cita != null && !esFechaHoy(cita.getFechaCita())) {
				reportesTemp.add(reporteCabina);
			}
			if (seccion != EnumSeccion.TODOS
					&& obtenerSeccion(reporteCabina) != seccion) {
				reportesTemp.add(reporteCabina);
			}
		}
		reportesARetornar.removeAll(reportesTemp);

		return reportesARetornar;
	}

	private Boolean esFechaHoy(Date fecha) {
		Date date = CommonUtils.setTimeToMidnight(fecha);
		Date hoyDate = CommonUtils.setTimeToMidnight(new Date());

		return !(hoyDate.getTime() != date.getTime());
	}

	public Date calculoTiempoParaAlarma(Date campoInicioConteo,
			Long tiempoParaAlerta, Boolean cuentaRegresiva) {
		Date tiempoParaAlarma = campoInicioConteo;
		if (cuentaRegresiva) {
			tiempoParaAlarma = new Date(campoInicioConteo.getTime()
					- tiempoParaAlerta);
		} else {
			tiempoParaAlarma = new Date(campoInicioConteo.getTime()
					+ tiempoParaAlerta);
		}
		return tiempoParaAlarma;
	}

}
