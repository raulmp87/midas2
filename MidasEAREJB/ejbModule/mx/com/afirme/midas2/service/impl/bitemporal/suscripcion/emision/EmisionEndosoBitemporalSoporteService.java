package mx.com.afirme.midas2.service.impl.bitemporal.suscripcion.emision;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDTO;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoFacadeRemote;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoId;
import mx.com.afirme.midas.interfaz.cobranza.CobranzaService;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoFacadeRemote;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.BitemporalComision;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

public class EmisionEndosoBitemporalSoporteService {
	@EJB
	protected EntidadContinuityService entidadContinuityService;
	@EJB
	protected EntidadService entidadService;
	@EJB
	protected EndosoService endosoService;
	@EJB
	protected CobranzaService cobranzaService;
	@EJB
	protected FormaPagoFacadeRemote formaPagoFacadeRemote;
	
	@EJB
	protected CoberturaEndosoFacadeRemote coberturaEndosoFacadeRemote;

	private static Map<BigDecimal, CoberturaDTO> coberturasMap;
	
	protected <T extends Entidad> EndosoDTO emiteEndoso(
			CotizacionContinuity cotizacionContinuity,
			BitemporalCotizacion cotizacionBitemporal, PolizaDTO polizaDTO,
			EndosoDTO endoso, List<T> coberturasAfectadas, SolicitudDTO solicitudDTO, Boolean aplicaDerechos) {
		
		coberturasMap = new LinkedHashMap<BigDecimal, CoberturaDTO>();
		
		List<CoberturaEndosoDTO> coberturasGeneradas = new ArrayList<CoberturaEndosoDTO>();

		Double pctIVA = cotizacionBitemporal.getValue().getPorcentajeIva();
		
		Collection<BitemporalComision> comisiones = cotizacionContinuity
				.getComisionContinuities().get(
						cotizacionBitemporal.getValidityInterval()
								.getInterval().getStart());
		// acumuladores del endoso

		Double valorRecargoPagoFrac = 0D;
		Double valorBonifComRecPagoFrac = 0D;
		Double valorBonifComision = 0D;
		Double valorIVA = 0D;
		Double valorDerechos = 0D;
		Double valorPrimaTotal = 0D;
		Double valorPrimaNeta = 0D;
		Double valorComisionFinal = 0D;
		Double valorComisionRecPagoFrac = 0D;
		Double valorComisionFinalRecPagoFrac = 0D;
		Double pctComision = 0D;
		
		//Ordenamiento de la lista coberturasAfectadas de acuerdo al numero de inciso
		MovimientoEndoso movimiento = null;
		BitemporalCoberturaSeccion bitemporalCoberturaSeccion = null;
		Integer numeroInciso = null;
		BigDecimal lineaNegocio = null;
		switch (solicitudDTO.getClaveTipoEndoso().shortValue()) {
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS:   
		case SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS:
			//Copia valores de coberturaEnd anterior del inciso con valor 0	
			for (T coberturaAfectada : coberturasAfectadas) {
			movimiento = (MovimientoEndoso) coberturaAfectada;
			
			BitemporalInciso bitemporalInciso = movimiento.getBitemporalInciso();
			
			numeroInciso = bitemporalInciso.getContinuity().getNumero();
			/*
			try{
			List<SeccionIncisoContinuity> seccionContList = (List<SeccionIncisoContinuity>) entidadContinuityService.findContinuitiesByParentKey(SeccionIncisoContinuity.class, 
					SeccionIncisoContinuity.PARENT_KEY_NAME, bitemporalInciso.getContinuity().getId());
			lineaNegocio = seccionContList.get(0).getSeccion().getIdToSeccion();
			}catch(Exception e){
			}*/
						
			for(CoberturaEndosoDTO coberturaEnd: this.getCoberturasEndInciso(endoso, new BigDecimal(numeroInciso))){
				CoberturaEndosoId coberturaId = new CoberturaEndosoId();
				coberturaId.setIdToPoliza(endoso.getId().getIdToPoliza());
				coberturaId.setNumeroEndoso(endoso.getId().getNumeroEndoso());
				
				coberturaId.setNumeroInciso(coberturaEnd.getId().getNumeroInciso());
				coberturaId.setIdToSeccion(coberturaEnd.getId().getIdToSeccion());
				coberturaId.setIdToCobertura(coberturaEnd.getId().getIdToCobertura());
				
				CoberturaEndosoDTO cobertura = new CoberturaEndosoDTO();
				cobertura.setId(coberturaId);
				cobertura.setEndosoDTO(endoso);
				cobertura.setRiesgoEndosoDTOs(null);
				cobertura.setSubIncisoCoberturaEndosoDTOs(null);
				
				cobertura.setIdTcSubRamo(coberturaEnd.getIdTcSubRamo());
				cobertura.setNumeroAgrupacion((short)0);
				Double ceroDbl = 0.0;
				
				cobertura.setPorcentajeComision(coberturaEnd.getPorcentajeComision());
				cobertura.setValorBonifComision(ceroDbl);
				cobertura.setValorBonifComRecPagoFrac(ceroDbl);
				cobertura.setValorCoaseguro(ceroDbl);
				cobertura.setValorComision(ceroDbl);
				cobertura.setValorComisionFinal(ceroDbl);
				cobertura.setValorComisionFinalRecPagoFrac(ceroDbl);
				cobertura.setValorComisionRecPagoFrac(ceroDbl);
				cobertura.setValorCuota(ceroDbl);
				cobertura.setValorDeducible(coberturaEnd.getValorDeducible());
				cobertura.setValorDerechos(ceroDbl);
				cobertura.setValorIVA(ceroDbl);
				cobertura.setValorSobreComisionAgente(ceroDbl);
				cobertura.setValorSobreComisionProm(ceroDbl);
				cobertura.setValorBonoAgente(ceroDbl);
				cobertura.setValorBonoProm(ceroDbl);
				cobertura.setValorCesionDerechosAgente(ceroDbl);
				cobertura.setValorCesionDerechosProm(ceroDbl);
				cobertura.setValorSobreComUDIAgente(ceroDbl);
				cobertura.setValorSobreComUDIProm(ceroDbl);
				cobertura.setValorPrimaNeta(ceroDbl);
				cobertura.setValorPrimaTotal(ceroDbl);
				cobertura.setValorRecargoPagoFrac(ceroDbl);
				cobertura.setValorSumaAsegurada(coberturaEnd.getValorSumaAsegurada());
				cobertura.setAgrupadorTarifaId(coberturaEnd.getAgrupadorTarifaId());
				cobertura.setVerAgrupadorTarifaId(coberturaEnd.getVerAgrupadorTarifaId());
				cobertura.setVerTarifaId(coberturaEnd.getVerTarifaId());
				cobertura.setVerCargaId(coberturaEnd.getVerCargaId());
				cobertura.setVerTarifaExtId(coberturaEnd.getVerTarifaExtId());
				cobertura.setTarifaExtId(coberturaEnd.getTarifaExtId());
				cobertura.setDiasSalarioMinimo(coberturaEnd.getDiasSalarioMinimo());

				coberturasGeneradas.add(cobertura);
			}
			}
			break;
		default:
		for (T coberturaAfectada : coberturasAfectadas) {
			movimiento = null;
			bitemporalCoberturaSeccion = null;
			// Crear coberturas
			CoberturaEndosoId coberturaId = new CoberturaEndosoId();
			coberturaId.setIdToPoliza(endoso.getId().getIdToPoliza());
			coberturaId.setNumeroEndoso(endoso.getId().getNumeroEndoso());
			
			movimiento = (MovimientoEndoso) coberturaAfectada;
			
			bitemporalCoberturaSeccion = movimiento
					.getBitemporalCoberturaSeccion();
			
			numeroInciso = bitemporalCoberturaSeccion
					.getContinuity().getSeccionIncisoContinuity()
					.getIncisoContinuity().getNumero();
			
			lineaNegocio = bitemporalCoberturaSeccion
					.getContinuity().getSeccionIncisoContinuity()
					.getSeccion().getIdToSeccion();
			
			coberturaId.setNumeroInciso(BigDecimal.valueOf(numeroInciso));
			coberturaId.setIdToSeccion(lineaNegocio);
			coberturaId.setIdToCobertura(bitemporalCoberturaSeccion
					.getContinuity().getCoberturaDTO().getIdToCobertura());
			
			CoberturaEndosoDTO coberturaEnd = new CoberturaEndosoDTO();
			coberturaEnd.setId(coberturaId);
			coberturaEnd.setEndosoDTO(endoso);
			coberturaEnd.setRiesgoEndosoDTOs(null);
			coberturaEnd.setSubIncisoCoberturaEndosoDTOs(null);
			
			CoberturaDTO coberturaDTO = this.findCobertura(coberturaId.getIdToCobertura());
			coberturaEnd.setIdTcSubRamo(coberturaDTO.getSubRamoDTO().getIdTcSubRamo());
			coberturaEnd.setNumeroAgrupacion((short)0);
			coberturaEnd.setDiasSalarioMinimo(bitemporalCoberturaSeccion.getValue().getDiasSalarioMinimo());
			pctComision = this.getPorcentajeComision(comisiones, coberturaDTO);
			
			this.calculaCobertura(movimiento, bitemporalCoberturaSeccion,
					coberturaEnd, pctIVA, pctComision);

			valorPrimaNeta += coberturaEnd.getValorPrimaNeta();
			valorRecargoPagoFrac += coberturaEnd.getValorRecargoPagoFrac();
			valorBonifComRecPagoFrac += coberturaEnd.getValorBonifComRecPagoFrac();
			valorBonifComision += coberturaEnd.getValorBonifComision();
			valorIVA += coberturaEnd.getValorIVA();
			valorPrimaTotal += coberturaEnd.getValorPrimaTotal();
			valorComisionFinal += coberturaEnd.getValorComision();
			valorComisionRecPagoFrac += coberturaEnd.getValorComisionRecPagoFrac();
			valorComisionFinalRecPagoFrac += coberturaEnd.getValorComisionFinalRecPagoFrac();
			valorDerechos += coberturaEnd.getValorDerechos();

			coberturasGeneradas.add(coberturaEnd);
		}
		break;
		}
		endoso.setCoberturaEndosoDTOs(coberturasGeneradas);
		endoso.setValorPrimaNeta(valorPrimaNeta);
		endoso.setValorRecargoPagoFrac(valorRecargoPagoFrac);
		endoso.setValorBonifComision(valorBonifComision);
		endoso.setValorBonifComisionRPF(valorBonifComRecPagoFrac);
		endoso.setValorIVA(valorIVA);
		endoso.setValorDerechos(valorDerechos);
		endoso.setValorPrimaTotal(valorPrimaTotal);
		endoso.setValorComision(valorComisionFinal);
		endoso.setValorComisionRPF(valorComisionRecPagoFrac);
		endoso.setClaveTipoEndoso(solicitudDTO.getClaveTipoEndoso().shortValue());
		

		return endoso;

	}

	protected void calculaCobertura(MovimientoEndoso detalle, BitemporalCoberturaSeccion coberturaSeccion,
			CoberturaEndosoDTO cobertura, Double pctIVA, Double pctComision) {
		//TODO logica para distribuir derechos y descuentos
		
		BigDecimal valorTotalPrimasMovimiento = new BigDecimal (detalle.getValorMovimiento().multiply(detalle.getFactorCobranzaPrimaNeta())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorRPF = new BigDecimal (detalle.getValorRPF().multiply(detalle.getFactorCobranzaRPF())+"").setScale(16, RoundingMode.HALF_EVEN);

		BigDecimal valorComision = new BigDecimal (detalle.getValorComision().multiply(detalle.getFactorCobranzaComision())+"").setScale(16, RoundingMode.HALF_EVEN);
			
		BigDecimal valorComisionRPF = new BigDecimal (detalle.getValorComisionRPF().multiply(detalle.getFactorCobranzaComisionRPF())+"").setScale(16, RoundingMode.HALF_EVEN);

		BigDecimal valorBonificacionComision = new BigDecimal (detalle.getValorBonificacionComision().multiply(detalle.getFactorCobranzaBonificacionComision())+"").setScale(16, RoundingMode.HALF_EVEN);

		BigDecimal valorBonificacionComisionRPF = new BigDecimal (detalle.getValorBonificacionRPF().multiply(detalle.getFactorCobranzaBonificacionRPF())+"").setScale(16, RoundingMode.HALF_EVEN);
			
		BigDecimal valorDerechos = new BigDecimal (detalle.getValorDerechos().multiply(detalle.getFactorCobranzaDerechos())+"").setScale(16, RoundingMode.HALF_EVEN);

		BigDecimal valorIVA = new BigDecimal (detalle.getValorIva().multiply(detalle.getFactorCobranzaIva())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorSobreComisionAgente = new BigDecimal (detalle.getValorSobreComisionAgente().multiply(detalle.getFactorCobranzaSobreComisionAgente())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorSobreComisionProm = new BigDecimal (detalle.getValorSobreComisionProm().multiply(detalle.getFactorCobranzaSobreComisionProm())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorBonoAgente = new BigDecimal (detalle.getValorBonoAgente().multiply(detalle.getFactorCobranzaBonoAgente())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorBonoProm = new BigDecimal (detalle.getValorBonoProm().multiply(detalle.getFactorCobranzaBonoProm())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorCesionDerechosAgente = new BigDecimal (detalle.getValorCesionDerechosAgente().multiply(detalle.getFactorCobranzaCesionDerechosAgente())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorCesionDerechosProm = new BigDecimal (detalle.getValorCesionDerechosProm().multiply(detalle.getFactorCobranzaCesionDerechosProm())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorSobreComisionUDIAgente = new BigDecimal (detalle.getValorSobreComisionUDIAgente().multiply(detalle.getFactorCobranzaSobreComisionUDIAgente())+"").setScale(16, RoundingMode.HALF_EVEN);
		
		BigDecimal valorSobreComisionUDIProm = new BigDecimal (detalle.getValorSobreComisionUDIProm().multiply(detalle.getFactorCobranzaSobreComisionUDIProm())+"").setScale(16, RoundingMode.HALF_EVEN);

		BigDecimal valorPrimaTotal = valorTotalPrimasMovimiento.add(valorDerechos).add(valorRPF)
			.subtract(valorBonificacionComision).subtract(valorBonificacionComisionRPF).add(valorIVA);
		
		cobertura.setPorcentajeComision(pctComision);
		cobertura.setValorBonifComision(valorBonificacionComision.doubleValue());
		cobertura.setValorBonifComRecPagoFrac(valorBonificacionComisionRPF.doubleValue());
		cobertura.setValorCoaseguro(coberturaSeccion.getValue().getValorCoaseguro());
		cobertura.setValorComision(valorComision.doubleValue());
		cobertura.setValorComisionFinal(valorComision.add(valorBonificacionComision.negate()).doubleValue());
		cobertura.setValorComisionFinalRecPagoFrac(valorComisionRPF.add(valorBonificacionComisionRPF.negate()).doubleValue());
		cobertura.setValorComisionRecPagoFrac(valorComisionRPF.doubleValue());
		cobertura.setValorCuota(coberturaSeccion.getValue().getValorCuota());
		
		//cobertura.setValorDeducible(coberturaSeccion.getValue().getValorDeducible());
		//Llena valor deducible dependiendo del tipo de deducible
		try{
			Double deducible = null;
			switch(coberturaSeccion.getValue().getClaveTipoDeducible().intValue()){
			case 1:
				deducible = coberturaSeccion.getValue().getPorcentajeDeducible();
				break;
			default:
				deducible = coberturaSeccion.getValue().getValorDeducible();
				break;
			}
			cobertura.setValorDeducible(deducible);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		cobertura.setValorDerechos(valorDerechos.doubleValue());
		cobertura.setValorIVA(valorIVA.doubleValue());
		cobertura.setValorSobreComisionAgente(valorSobreComisionAgente.doubleValue());
		cobertura.setValorSobreComisionProm(valorSobreComisionProm.doubleValue());
		cobertura.setValorBonoAgente(valorBonoAgente.doubleValue());
		cobertura.setValorBonoProm(valorBonoProm.doubleValue());
		cobertura.setValorCesionDerechosAgente(valorCesionDerechosAgente.doubleValue());
		cobertura.setValorCesionDerechosProm(valorCesionDerechosProm.doubleValue());
		cobertura.setValorSobreComUDIAgente(valorSobreComisionUDIAgente.doubleValue());
		cobertura.setValorSobreComUDIProm(valorSobreComisionUDIProm.doubleValue());
		cobertura.setValorPrimaNeta(valorTotalPrimasMovimiento.doubleValue());
		cobertura.setValorPrimaTotal(valorPrimaTotal.doubleValue());
		cobertura.setValorRecargoPagoFrac(valorRPF.doubleValue());
		cobertura.setValorSumaAsegurada(coberturaSeccion.getValue().getValorSumaAsegurada());
		cobertura.setAgrupadorTarifaId(coberturaSeccion.getValue().getAgrupadorTarifaId()!= null?coberturaSeccion.getValue().getAgrupadorTarifaId():0);
		cobertura.setVerAgrupadorTarifaId(coberturaSeccion.getValue().getVerAgrupadorTarifaId()!=null?coberturaSeccion.getValue().getVerAgrupadorTarifaId():0);
		cobertura.setVerTarifaId(coberturaSeccion.getValue().getVerTarifaId()!=null?coberturaSeccion.getValue().getVerTarifaId():0);
		cobertura.setVerCargaId(coberturaSeccion.getValue().getVerCargaId()!=null?coberturaSeccion.getValue().getVerCargaId():0);
		cobertura.setVerTarifaExtId(coberturaSeccion.getValue().getVerTarifaExtId()!=null?coberturaSeccion.getValue().getVerTarifaExtId():0);
		cobertura.setTarifaExtId(coberturaSeccion.getValue().getTarifaExtId()!=null?coberturaSeccion.getValue().getTarifaExtId():0);
	}

	protected CoberturaDTO findCobertura(BigDecimal coberturaId){
		if(coberturasMap.containsKey(coberturaId)){
			return coberturasMap.get(coberturaId);
		}else{
			CoberturaDTO coberturaEncontrada = entidadService.findById(CoberturaDTO.class, coberturaId);
			coberturasMap.put(coberturaId, coberturaEncontrada);
			return coberturaEncontrada;
		}
	}
	protected Double getPctFormaPago(Integer formaPagoId, Short idMoneda) {
		FormaPagoIDTO formaPagoIDTO = new FormaPagoIDTO();
		formaPagoIDTO.setIdFormaPago(formaPagoId);
		formaPagoIDTO.setIdMoneda(idMoneda);
		try {
			List<FormaPagoIDTO> formaPagoList = formaPagoFacadeRemote
					.findByProperty(formaPagoIDTO, "", "A");
			if (formaPagoList != null)
				return formaPagoList.get(0)
						.getPorcentajeRecargoPagoFraccionado().doubleValue();
		} catch (Exception e) {
		}
		return null;
	}
	
	protected Double getPorcentajeComision(
			Collection<BitemporalComision> comisiones,
			CoberturaDTO coberturaOriginal) {
		Double pctComision = 0D;
		for (BitemporalComision comision : comisiones) {
			if (comision.getContinuity().getSubRamoDTO().getIdTcSubRamo()
					.intValue() == coberturaOriginal.getSubRamoDTO()
					.getIdTcSubRamo().intValue()) {
				pctComision = comision.getValue()
						.getPorcentajeComisionCotizacion().doubleValue();
				break;
			}
		}
		return pctComision;
	}
	
	protected List<CoberturaEndosoDTO> getCoberturasEnd(EndosoDTO endoso){
		List<CoberturaEndosoDTO> list = new ArrayList<CoberturaEndosoDTO>(1);
		int numeroEndoso = getNumeroCoberturaEndAnt(endoso, null);
		while(numeroEndoso >= 0){
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("id.idToPoliza", endoso.getId().getIdToPoliza());
			params.put("id.numeroEndoso", numeroEndoso);
			list = entidadService.findByProperties(CoberturaEndosoDTO.class, params);
			if(!list.isEmpty()){
				break;
			}
			numeroEndoso = numeroEndoso - 1;
		}
		return list;
	}
	
	protected List<CoberturaEndosoDTO> getCoberturasEndInciso(EndosoDTO endoso, BigDecimal numeroInciso){
		List<CoberturaEndosoDTO> list = new ArrayList<CoberturaEndosoDTO>(1);
		int numeroEndoso = getNumeroCoberturaEndAnt(endoso, numeroInciso);
		while(numeroEndoso >= 0){
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("id.idToPoliza", endoso.getId().getIdToPoliza());
			params.put("id.numeroEndoso", numeroEndoso);
			params.put("id.numeroInciso", numeroInciso);
			list = entidadService.findByProperties(CoberturaEndosoDTO.class, params);
			if(!list.isEmpty()){
				break;
			}
			numeroEndoso = numeroEndoso - 1;
		}
		return list;
	}
	
	protected int getNumeroCoberturaEndAnt(EndosoDTO endoso, BigDecimal numeroInciso){
		
		int numeroEndoso = 0;
		try{
			numeroEndoso = coberturaEndosoFacadeRemote.obtenerMaxNumeroCoberturaEnd(endoso.getId().getIdToPoliza(), numeroInciso);
		}catch(Exception e){
			e.printStackTrace();
		}
		return numeroEndoso;
	}
}
