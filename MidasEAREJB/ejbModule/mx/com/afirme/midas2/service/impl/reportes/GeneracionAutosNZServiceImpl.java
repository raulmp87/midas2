package mx.com.afirme.midas2.service.impl.reportes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;


import mx.com.afirme.midas.danios.reportes.reportercs.ReporteRCSDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.service.reportes.GeneracionReporteAutosNZService;
import mx.com.afirme.midas2.util.MidasException;
 
@Stateless    
public class GeneracionAutosNZServiceImpl implements GeneracionReporteAutosNZService{
			
	@Override
	public int generarReporte( Date fInicial, Date fFinal, String usuario ) throws MidasException {
		
		String spName = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_EMISION_REPORTE";
		StoredProcedureHelper storedHelper;
		int resp = 0;
		
		try { 
			storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro( "FINI", getStringDate(fInicial) );
			storedHelper.estableceParametro( "FFIN", getStringDate(fFinal) );
			storedHelper.estableceParametro( "USUARIO", "E" );
			storedHelper.ejecutaActualizar();
			
		}
		catch (Exception e) {
			LogDeMidasEJB3.log("Error en :" + e.getMessage() + " " + e.getCause(), Level.INFO, null);
		}

		return resp;
	}
	
	private String getStringDate( Date d ) throws Exception{
		
		String strDate;			
		DateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
        strDate = formatter.format(d);
		
		return strDate;
	}
	
	@Override
	public InputStream exportarReporte( Date fInicial, Date fFinal) throws MidasException {
		InputStream is;
		StoredProcedureHelper storedHelper;
		StringBuilder descripcionParametros = new StringBuilder();
		String nombreSP = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_SINIESTROS_REPORTE_EXT";
		
		try {
			
			String[] nombreParametros = {"FINI", 
					"FFIN",
					"USUARIO"};
			Object[] valorParametros={fInicial,
				fFinal,
				0};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),					
					"registro",
					"registro");
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append(", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Consultando reporte bases emision. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log("Finaliza consulta reporte bases emision. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros.toString() +
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);

			byte[] bytes = toArrayByte(listaResultado);
			is = new ByteArrayInputStream(bytes);

			return is;
	
		} catch (Exception e) {
			LogDeMidasEJB3.log("Hubo un error exportarReporte EMISION: SELECT * FROM seycos.aux_rep_vigor " + e.getMessage() + " " + e.getCause(), Level.INFO, null);
			 return null;
	    } 
	}
	
	private byte[] toArrayByte( List<ReporteRCSDTO> list ) throws Exception {
		
		byte[] bytes;
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			DataOutputStream out = new DataOutputStream(baos);
			StringBuilder sb = new StringBuilder();
			out.writeBytes("OFI|I_AGENTE|AAMM|ID_RAMO|SUBRAMO|CTACON|I_MONEDA|N_POLIZAX|N_ENDOSO|F_CUBRE_DESDE_END|" +
		       		"F_CUBRE_HASTA_END|FEC_EMISION|PORC_COMI |P_NETA|RECRGOS|DERECHOS|COMISION|PMA_CES|PMA_FAC|TIPO_CAMBIO|COSTO_ADQ|RSVA|" +
		       		"INIPOL|FINPOL|SOLICITANTE|RAMO|LIN_NEGOCIO|PAQUETE|CVE_SUBRAMO|PRIMA_COB_TERCEROS|COM_DIREC_AGT|COSTO_ADQ_ADIC\n");
			
			if(list != null)
				for (ReporteRCSDTO item : list) {
					sb.append(item.getRegistro()+System.getProperty("line.separator"));
				}
			out.writeBytes(sb.toString());
			bytes = baos.toByteArray();
		
		return bytes;
	}
	
	
	@Override
	public String getFechasReporte(int tipoReporte) {
		StoredProcedureHelper storedHelper;
		String nombreSP = "SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_GET_FECHAS_REPORTE";
		StringBuilder descripcionParametros = new StringBuilder();
		try {
			
			String[] nombreParametros = {"FINI", 
					"FFIN",
					"TIPOREPORTE"};
			Object[] valorParametros={"",
				"",
				tipoReporte};

			storedHelper = new StoredProcedureHelper(nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
		
			storedHelper.estableceMapeoResultados(
					ReporteRCSDTO.class.getCanonicalName(),
					"registro",
					"registro");
			
			for(int i=0;i<nombreParametros.length;i++){
				storedHelper.estableceParametro(nombreParametros[i], valorParametros[i]);
				descripcionParametros.append(", ["+nombreParametros[i]+"] : "+valorParametros[i]);
			}
			
			LogDeMidasEJB3.log("Consultando reporte RESERVAS. SP a invocar: "+nombreSP+", Parametros: "+descripcionParametros.toString(), Level.INFO, null);
			
			List<ReporteRCSDTO> listaResultado = storedHelper.obtieneListaResultados();
			
			LogDeMidasEJB3.log("Finaliza consulta reporte RESERVAS. SP invocado: "+nombreSP+", Parametros: "+descripcionParametros.toString() +
					"registros encontrados: "+(listaResultado != null? listaResultado.size() : 0), Level.INFO, null);

			if(listaResultado.size() > 0) {
				return listaResultado.get(0).getRegistro();
			}	        
	        return "";
	
		} catch (Exception e) {
			LogDeMidasEJB3.log("Hubo un error SEYCOS.PKG_ARCHIVOS_RESERVAS.SP_GET_FECHAS_REPORTE " + e.getMessage() + " " + e.getCause(), Level.INFO, null);
			 return "";
	    } 
	}
		
}

