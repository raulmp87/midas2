package mx.com.afirme.midas2.service.impl.complementopago;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas2.dao.cobranza.pagos.complementopago.ComplementoPagoDao;
import mx.com.afirme.midas2.domain.cobranza.pagos.complementopago.ComplementoPago;
import mx.com.afirme.midas2.domain.cobranza.pagos.complementopago.IngresosAplicar;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.complementopago.ComplementoPagoService;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

@Stateless
public class ComplementoPagoServiceImpl implements ComplementoPagoService {

	private static Logger LOG = Logger.getLogger(ComplementoPagoServiceImpl.class);
	public static final String TITULO_CORREO="Aviso de Ingresos de Cobranza pendientes de aplicar"; 
	public static final String SALUDO_CORREO="A quien corresponda.";
	
	private ComplementoPagoDao dao;
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	@EJB
	protected MailService mailService;
	
	private PrintReportClient printReportClient;
	
	@EJB
	public void setDao(ComplementoPagoDao dao) {
		this.dao = dao;
	}
	
	@EJB
	public void setPrintReportClient(PrintReportClient printReportClient) {
		this.printReportClient = printReportClient;
	}
	
	public List<ComplementoPago> findByFilters(ComplementoPago complementoPago) throws Exception{
		System.err.println("Entre a service implement");
		return dao.findByFilters(complementoPago);
	}
	
	public List<ComplementoPago> listarErrores(ComplementoPago complementoPago) throws Exception{
		System.err.println("Entre a listarErroresCP");
		return dao.listarErrores(complementoPago);
	}
	
	@Schedule(minute = "*/30", hour = "*", persistent = false)
	public void timbraComplementoPago() {
		LOG.info("Inicia timbrado de Complemento Pago.");
		
		String switchTimbrado = parametroGeneralFacade.findById(new ParametroGeneralId(ComplementoPagoService.GRUPO_PARAMETRO_COMPLEMENTO_PAGO,ComplementoPagoService.PARAMETRO_SWITCH_COMPLEMENTO_PAGO), true).getValor();		
		
		if(Integer.parseInt(switchTimbrado) == 1){
			try {
				printReportClient.generatePaymentComplementBill();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}else{
			LOG.info("Servicio timbrado de Complemento Pago apagado");
		}
		LOG.info("Termina timbrado de Complemento Pago.");
	}
	
	@Schedule(minute = "*/30", hour = "*", persistent = false)
	public void timbraDespagosComplementoPago() {
		LOG.info("Inicia timbrado de Complemento Despago.");
		
		String switchTimbrado = parametroGeneralFacade.findById(new ParametroGeneralId(ComplementoPagoService.GRUPO_PARAMETRO_COMPLEMENTO_PAGO,ComplementoPagoService.PARAMETRO_SWITCH_COMPLEMENTO_PAGO_DESPAGOS), true).getValor();		
		
		if(Integer.parseInt(switchTimbrado) == 1){
			try {
				printReportClient.generateChargeComplementBill();
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}else{
			LOG.info("Servicio timbrado de Complemento Despago apagado");
		}
		LOG.info("Termina timbrado de Complemento Despago.");
	}
	
	@Schedule(dayOfMonth = "Last", persistent = false)
	public void notificaComplementoPago() {
		LOG.info("Inicia Aviso de Ingresos de Cobranza pendientes de aplicar");

		String switchTimbrado = parametroGeneralFacade.findById(new ParametroGeneralId(ComplementoPagoService.GRUPO_PARAMETRO_COMPLEMENTO_PAGO,ComplementoPagoService.PARAMETRO_SWITCH_NOTIFICACION_APLICAR_INGRESO),true).getValor();

		if (Integer.parseInt(switchTimbrado) == 1) {
			String cadenaCorreos = parametroGeneralFacade.findById(new ParametroGeneralId(ComplementoPagoService.GRUPO_PARAMETRO_COMPLEMENTO_PAGO,ComplementoPagoService.PARAMETRO_CORREOS_NOTIFICACION_APLICAR_INGRESO),true).getValor();
			String msg = "";
			List<ByteArrayAttachment> attachment = getAttachment();

			List<String> correos = new ArrayList<String>();
			for (String correo : cadenaCorreos.split(";")) {
				correos.add(correo);
			}

			mailService.sendMail(correos, TITULO_CORREO, msg, attachment,TITULO_CORREO, SALUDO_CORREO);
		}else{
			LOG.info("Servicio de notificacion apagado");
		}

		LOG.info("Termina Aviso de Ingresos de Cobranza pendientes de aplicar");
	}

	private List<ByteArrayAttachment> getAttachment() {
		List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
		
		try {
			List<IngresosAplicar> resultList = dao.consultaIngresosAplicar();
			ExcelExporter exporter = new ExcelExporter(IngresosAplicar.class);

			TransporteImpresionDTO transporte = exporter.exportXLS(resultList, "Lista_De_Pagos_Sin_Factura");
			
			ByteArrayAttachment arrayAttachment = new ByteArrayAttachment();
			arrayAttachment.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
			arrayAttachment.setContenidoArchivo(IOUtils.toByteArray(transporte.getGenericInputStream()));
			arrayAttachment.setNombreArchivo("Lista_De_Pagos_Sin_Factura.xls");
			attachment.add(arrayAttachment);

		} catch (IOException e) {
			LOG.error("Error al crear Excel" + e);
		}

		return attachment;
	}
}