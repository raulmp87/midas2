package mx.com.afirme.midas2.service.impl.emision.ejecutivoColoca;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;





import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;

import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.service.emision.ejecutivoColoca.EjecutivoColocaService;
import mx.com.afirme.midas2.service.emision.ejecutivoColoca.PolizaEjecutivoColocaDTO;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.wsClient.EibsUserService.ArrayOf1831153225NillableUserEibsVO;
import mx.com.afirme.midas2.wsClient.EibsUserService.EibsUserServiceProxy;
import com.afirme.eibs.services.EibsUserService.UserEibsVO;

@Stateless
public class EjecutivoColocaServiceImpl implements EjecutivoColocaService{
	private static final Logger LOG = Logger.getLogger(EjecutivoColocaServiceImpl.class);
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@EJB
	BitacoraService bitacoraService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	public void setParametroGeneralFacade(
			ParametroGeneralFacadeRemote parametroGeneralFacade) {
		this.parametroGeneralFacade = parametroGeneralFacade;
	}

	
	public List<PolizaEjecutivoColocaDTO> getPolizasEjecutivoColoca(
			String polizaMidas, String polizaSeycos, String endoso,
			String fechaInicio, String fechaFinal, String sucursal, String ramo) {
		LOG.info("-------getPolizasEjecutivoColoca----- polizaMidas "+polizaMidas+" polizaSeycos "+polizaSeycos +" endoso " +endoso
				+" fechaInicio " +fechaInicio+" fechaFinal " +fechaFinal+" sucursal " +sucursal
				+" ramo " +ramo);
		List<PolizaEjecutivoColocaDTO> resList= new ArrayList<PolizaEjecutivoColocaDTO>();
		String codResp="";
		String descResp="";
		StoredProcedureHelper storedHelper = null;
		try {
		 storedHelper = new StoredProcedureHelper("MIDAS.PKG_EJECUTIVO_COLOCA.buscarPolizas","jdbc/MidasDataSource");
		
		 String [] propiedades = new String[]{"idPoliza","claveNeg", "numeroPolizaMidas", "numeroPolizaSeycos","numeroEndoso","sucursal","nombreSucursal", "producto", "ejecutivo", "esBancaSeguros"};
		String [] columnas = new String[]{"IDTOPOLIZA","CLAVENEG","POLIZA_MIDAS", "POLIZA_SEYCOS", "ENDOSO","NUM_SUCURSAL","NOMBRE_SUCURSAL","PRODUCTO","EJECUTIVO", "ESBANCASEGUROS"};
	
		storedHelper.estableceMapeoResultados(PolizaEjecutivoColocaDTO.class.getCanonicalName(),
				propiedades,
			    columnas);
		polizaMidas=polizaMidas.replace(" ", "");
		polizaSeycos=polizaSeycos.replace(" ", "");

		storedHelper.estableceParametro( "p_polizaMidas", polizaMidas );
		storedHelper.estableceParametro( "p_polizaSeycos", polizaSeycos );
		storedHelper.estableceParametro( "p_numeroEndoso", endoso );
		storedHelper.estableceParametro( "p_sucursal",  sucursal);
		storedHelper.estableceParametro( "p_fechaInicio", getDateString(fechaInicio ));
		storedHelper.estableceParametro( "p_fechaFin",getDateString(fechaFinal ));
		storedHelper.estableceParametro( "p_ramo", ramo );
		resList = storedHelper.obtieneListaResultados();
		
		} catch (Exception e) {
			codResp=""+storedHelper.getCodigoRespuesta();
			descResp=storedHelper.getDescripcionRespuesta();
			LOG.error("Error en :" + "MIDAS.PKG_EJECUTIVO_COLOCA.buscarPolizas"+ e+"\n"+codResp+"\n"+descResp);
		}
		return resList;

		
	}
	
	private Date getDateString( String dateInString) throws Exception{
		 Date date=null;
		  SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
	        try {
	           date = formatter.parse(dateInString);
	        } catch (ParseException e) {
	            e.printStackTrace();
	        }
		return date;
	}

	

	public PolizaEjecutivoColocaDTO getPolizaEjecutivoColoca(String polizaMidas, String polizaSeycos, 
			String endoso, String fechaInicio, String fechaFinal, String sucursal, String ramo){
		LOG.info("-------getPolizaEjecutivoColoca----- polizaMidas "+polizaMidas+" polizaSeycos "+polizaSeycos +" endoso " +endoso
				+" fechaInicio " +fechaInicio+" fechaFinal " +fechaFinal+" sucursal " +sucursal
				+" ramo " +ramo);

		PolizaEjecutivoColocaDTO poliza1= new PolizaEjecutivoColocaDTO();
		 
		List<PolizaEjecutivoColocaDTO> resList= new ArrayList<PolizaEjecutivoColocaDTO>();
		try {
		StoredProcedureHelper storedHelper = new StoredProcedureHelper("MIDAS.PKG_EJECUTIVO_COLOCA.buscarPolizas","jdbc/MidasDataSource");		
		
		String [] propiedades = new String[]{"idcotizacion","idPoliza","claveNeg","numeroPolizaMidas", "numeroPolizaSeycos","numeroEndoso", "sucursal","nombreSucursal", "producto", "ejecutivo", "identificador"};
		String [] columnas = new String[]{"IDCOTIZACION", "IDTOPOLIZA","CLAVENEG","POLIZA_MIDAS", "POLIZA_SEYCOS", "ENDOSO","NUM_SUCURSAL","NOMBRE_SUCURSAL","PRODUCTO","EJECUTIVO","IDENTIFICADOR"};
	
		storedHelper.estableceMapeoResultados(PolizaEjecutivoColocaDTO.class.getCanonicalName(),
				propiedades,
			    columnas);
		
		polizaMidas=polizaMidas.replace(" ", "");
		polizaSeycos=polizaSeycos.replace(" ", "");
		endoso=endoso.replace(" ", "");
		ramo=ramo.replace(" ", "");
		storedHelper.estableceParametro( "p_polizaMidas", polizaMidas );
		storedHelper.estableceParametro( "p_polizaSeycos", polizaSeycos );
		storedHelper.estableceParametro( "p_numeroEndoso", endoso );
		storedHelper.estableceParametro( "p_sucursal",  sucursal);
		storedHelper.estableceParametro( "p_fechaInicio", getDateString(fechaInicio));
		storedHelper.estableceParametro( "p_fechaFin",getDateString(fechaFinal ));
		storedHelper.estableceParametro( "p_ramo", ramo );
		resList = storedHelper.obtieneListaResultados();

		} catch (Exception e) {
			LOG.error("Error en :" + "MIDAS.PKG_EJECUTIVO_COLOCA.buscarPolizas", e);
		}
		poliza1=resList.get(0);
		return poliza1;
	}
	
	
	public boolean savePolizaEjecutivoColoca (String numPoliza, String idCotizacion,String idPoliza,
			String claveNeg, String ejecutivo, String endoso, String identificador)
	{
		ejecutivo=ejecutivo.trim();
		LOG.info("-------savePolizaEjecutivoColoca----- numPoliza "+numPoliza+" idCotizacion "+idCotizacion
				+" idPoliza " +idPoliza+" claveNeg " +claveNeg+" ejecutivo " +ejecutivo+" endoso " +endoso);

		String spName = "MIDAS.PKG_EJECUTIVO_COLOCA.asignaAgente";
		int resp = 0;
		try {
		StoredProcedureHelper storedHelper = new StoredProcedureHelper("MIDAS.PKG_EJECUTIVO_COLOCA.asignaAgente","jdbc/MidasDataSource");		
				storedHelper.estableceParametro( "p_claveNeg", claveNeg );
				storedHelper.estableceParametro( "p_idcotizacion", idCotizacion );
				storedHelper.estableceParametro( "p_idtoPoliza", idPoliza );
				storedHelper.estableceParametro( "p_Ejecutivo", ejecutivo );
				storedHelper.estableceParametro( "p_identificador", identificador );
				storedHelper.estableceParametro( "p_endoso", endoso );
				resp = storedHelper.ejecutaActualizar();

		} catch (Exception e) {
			LOG.error("***********Error en :" + spName, e);
		}
		if(resp>0)
			this.bitacoraService.registrar(TIPO_BITACORA.EJECUTIVO_COLOCA, EVENTO.EDICION_EJECUTIVO_COLOCA, numPoliza, "EDICION EJECUTIVO COLOCA POLIZA","Se cambia el ejecutivo a "+ejecutivo+ " de la poliza "+numPoliza+ " endoso "+ endoso, usuarioService.getUsuarioActual().getNombreUsuario() );

		return resp>0?true:false;
	}
	
	public List<UserEibsVO> getEjecutivoColocaList(String userName){
		LOG.info("---------getEjecutivoColocaList--------"+" userName:"+userName);

		EibsUserServiceProxy proxy = new EibsUserServiceProxy();
		ArrayOf1831153225NillableUserEibsVO arr = new ArrayOf1831153225NillableUserEibsVO() ;
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_EJECUTVO_COLOCA, 
				ParametroGeneralDTO.CODIGO_EJECUTVO_COLOCA_CNE);
		ParametroGeneralDTO parametroCNE = parametroGeneralFacade.findById(id);
		userName=parametroCNE.getValor()+userName;
		try {
		arr=proxy.getAccountExecutiveByLoggedUser(userName, null, null);
		LOG.info("**************EIBS WS executive list size: "+arr.getUserEibsVO().size());
		} catch (Exception ex) {
			LOG.error("**************EIBS WS error: " + ex);
			
		}
		Collections.sort(arr.getUserEibsVO(), new EjecutivoColocaSort());
		return arr.getUserEibsVO();
	}


	public  boolean getRolUser(String userName) {
		LOG.info("-------getRolUser---- usuario "+userName);

		String spName = "MIDAS.PKG_EJECUTIVO_COLOCA.validaRol";
		int resp = 0;
		try {
		        StoredProcedureHelper storedHelper = new StoredProcedureHelper("MIDAS.PKG_EJECUTIVO_COLOCA.validaRol","jdbc/MidasDataSource");		
				storedHelper.estableceParametro( "p_userName", userName );
				resp = storedHelper.ejecutaActualizar();

		} catch (Exception e) {
			LOG.error("***********Error en :" + spName, e);
		}
		return resp>0?true:false;
	}
	
	
	

}
