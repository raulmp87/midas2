/**
 * 
 */
package mx.com.afirme.midas2.service.impl.siniestros.catalogo.solicitudautorizacion;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas2.dao.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.autorizacion.ParametroReservaAntiguo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.BitacoraEventoSiniestro.TipoEvento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionAjusteAntiguedad;
import mx.com.afirme.midas2.domain.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionVigencia;
import mx.com.afirme.midas2.dto.siniestros.catalogo.solicitudautorizacion.SolicitudAutorizacionDTO;
import mx.com.afirme.midas2.dto.siniestros.catalogos.solicitudautorizacion.SolicitudAutorizacionAntiguedadDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.solicitudautorizacion.CatSolicitudAutorizacionService;
import mx.com.afirme.midas2.util.EnumUtil;

import org.joda.time.LocalDate;
import org.joda.time.Months;




/**
 * @author admin
 *
 */
@Stateless
public class CatSolicitudAutorizacionServiceImpl implements CatSolicitudAutorizacionService {
	
	private static final int ESTATUS_SOLICITUD_VIGENCIA_PENDIENTE 	= 0;
	public static final int ESTATUS_SOLICITUD_AUTORIZADO 			= 1;
//	private static final int ESTATUS_SOLICITUD_RECHAZADO 			= 2;

	
	private static final int ESTATUS_SOLICITUD_PENDIENTE = 1;
	private static final int ESTATUS_SOLICITUD_TERMINADO = 2;
//	private static final int ESTATUS_SOLICITUD_CANCELADO = 3;
	
	private static final String TIPO_SOLICITUD_AUTORIZACION_VIGENCIA = "SAV";
	private static final String TIPO_SOLICITUD_AJUSTE_RESERVA = "RES";
	public static final String TIPO_SOLICITUD_AUTORIZACION_CANCELACION = "CAN";
	
	private static final Integer ESTATUS_PENDIENTE_INT = 1; 
	private static final String TIPO_SOLICITUD = "RES";
	private static final Integer AUTORIZA = 1;
	private static final Integer NO_AUTORIZA = 0;
	private static final String  ESTATUS_AUTORIZADO = "AUT";
	private static final String  ESTATUS_PENDIENTE = "PEND";
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private CatalogoSiniestroService catalogoSiniestroService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaService;
	
	@EJB
	private CatSolicitudAutorizacionDao catSolicitudAutorizacionDao; 
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
	private ReporteCabinaService reporteCabinaService;


	
	@Override
	public List<SolicitudAutorizacionDTO> buscar(SolicitudAutorizacionFiltro filtro) {
		return  transformSolicitudAutorizacionToSolicitudAutorizacionDTO( catSolicitudAutorizacionDao.buscar(filtro) );
	}
	
	
	
	@Override
	public void autorizacion(Long idSolicitudAutorizacion , Integer estatus , String tipoSolicitud ) {
		
		String codUsuarioAutorizador	= String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() );
		SolicitudAutorizacion entidad 	= null;
		
		entidad = catalogoSiniestroService.obtener(SolicitudAutorizacion.class, idSolicitudAutorizacion);
		
		if( entidad !=null){
			if(! entidad.getEstatus().equals(ESTATUS_SOLICITUD_TERMINADO)){
				
				entidad.setCodUsuarioAutorizador(codUsuarioAutorizador  );
				entidad.setEstatus( ESTATUS_SOLICITUD_TERMINADO );
				entidad.setFechaModEstatus(new Date());
				
				if( TIPO_SOLICITUD_AUTORIZACION_VIGENCIA.equals( tipoSolicitud ) || 
						TIPO_SOLICITUD_AUTORIZACION_CANCELACION.equals(tipoSolicitud)){
					List<SolicitudAutorizacionVigencia> solicitudes = entidadService.findByProperty(
							SolicitudAutorizacionVigencia.class, "solicitudAutorizacion.id", entidad.getId());
					if(solicitudes != null && solicitudes.size() > 0){
						entidad.setSolicitudAutorizacionVigencia(solicitudes.get(0));					
						entidad.getSolicitudAutorizacionVigencia().setEstatus(estatus);
					}
				}else if( TIPO_SOLICITUD_AJUSTE_RESERVA.equals( tipoSolicitud )){	
					
					List<SolicitudAutorizacionAjuste> solicitudesAjuste = entidadService.findByProperty(
							SolicitudAutorizacionAjuste.class, "solicitudAutorizacion.id", entidad.getId());
					
					if(solicitudesAjuste != null && solicitudesAjuste.size() > 0){
						entidad.setSolicitudAutorizacionAjuste(solicitudesAjuste.get(0));					
						entidad.getSolicitudAutorizacionAjuste().setEstatus(estatus);
						
						if(estatus == ESTATUS_SOLICITUD_AUTORIZADO){
							Long idEstimacionCobertura  = entidad.getSolicitudAutorizacionAjuste().getEstimacionCobertura().getId();
							BigDecimal estimacionNueva = entidad.getSolicitudAutorizacionAjuste().getEstimacionNueva();
							String causaMovimiento = entidad.getSolicitudAutorizacionAjuste().getCausaMovimiento();
							
							movimientoSiniestroService.generarAjusteEstimacion(
									idEstimacionCobertura, estimacionNueva, EnumUtil.fromValue(CausaMovimiento.class, causaMovimiento), null);
						}
					}
					
				}
				catalogoSiniestroService.salvar(entidad);
			}
		}
	}
	
	@Override
	public void save( SolicitudAutorizacionDTO solicitudAutorizacionDTO){
		catalogoSiniestroService.salvar( transformSolicitudAutorizacionDTOToSolicitudAutorizacionVigencia(solicitudAutorizacionDTO) );
	}


	@Override
	public Map<String, String> getEstatusMap() {
		return listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_SOLICITUD_AUTORIZACION);
	}

	@Override
	public Map<Long, String> getOficinasMap() {
		return listadoService.obtenerOficinasSiniestros();
	}
	
	@Override
	public Map<String,String> getTipoSolicitudMap(){
		return listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_SOLICITUD_AUTORIZACION);
	}
	
	@Override
	public List<Rol> getRolUsuarioActual(){
		return usuarioService.getUsuarioActual().getRoles();
	}
	
	
	
	
	private List<SolicitudAutorizacionDTO> transformSolicitudAutorizacionToSolicitudAutorizacionDTO( List<SolicitudAutorizacion> informacion){
		
		List<SolicitudAutorizacionDTO> resultado 				= new ArrayList<SolicitudAutorizacionDTO>();
		SolicitudAutorizacionDTO solicitudAutorizacionDTO 		= null;
		Map<String, String> tipoSolicitudMap					= this.getTipoSolicitudMap();
		
		for(SolicitudAutorizacion solicitud :informacion){
			solicitudAutorizacionDTO = new SolicitudAutorizacionDTO();
			solicitudAutorizacionDTO.setIdSolicitud( solicitud.getId() );
			solicitudAutorizacionDTO.setCodUsuarioCreacion( solicitud.getCodigoUsuarioCreacion() );
			solicitudAutorizacionDTO.setCodUsuarioAutorizador( solicitud.getCodUsuarioAutorizador() );
			solicitudAutorizacionDTO.setFechaCreacion( solicitud.getFechaCreacion() );
			solicitudAutorizacionDTO.setEstatusSolicitud( solicitud.getEstatus() );
			solicitudAutorizacionDTO.setTipoSolicitud( tipoSolicitudMap.get(solicitud.getTipoSolicitud()) );
			solicitudAutorizacionDTO.setNombreTipoSolicitud(solicitud.getTipoSolicitud());
			solicitudAutorizacionDTO.setComentarios(solicitud.getComentarios());
			if(solicitud.getTipoSolicitud().equals("RES")){
				this.llenarDatosSolicitudAutorizacionAjuste(solicitudAutorizacionDTO, solicitud);
				if( solicitud.getSolicitudAutorizacionAjuste() != null ){
					solicitudAutorizacionDTO.setEstatusTipoSolicitud(solicitud.getSolicitudAutorizacionAjuste().getEstatus()  );
				}
				
			}else if(solicitud.getTipoSolicitud().equals("SAV") || solicitud.getTipoSolicitud().equals("CAN")){
				this.llenarDatosSolicitudAutorizacionVigencia(solicitudAutorizacionDTO, solicitud);
				if( solicitud.getSolicitudAutorizacionVigencia() != null ){
					solicitudAutorizacionDTO.setEstatusTipoSolicitud(solicitud.getSolicitudAutorizacionVigencia().getEstatus() );
				}
				
			}
			resultado.add(solicitudAutorizacionDTO);
		}
		return resultado;
	}
	
	private void llenarDatosSolicitudAutorizacionVigencia( SolicitudAutorizacionDTO solicitudAutorizacionDTO,SolicitudAutorizacion solicitud ) {
		
		List<SolicitudAutorizacionVigencia> solicitudes = entidadService.findByProperty(SolicitudAutorizacionVigencia.class, "solicitudAutorizacion.id", solicitud.getId());
		if(solicitudes != null && solicitudes.size() > 0){
			SolicitudAutorizacionVigencia vigencia = solicitudes.get(0);		
			solicitudAutorizacionDTO.setIdTipoSolicitud( vigencia.getId() );
			solicitudAutorizacionDTO.setPolizaId(  vigencia.getPoliza().getIdToPoliza().longValue()  );
			solicitudAutorizacionDTO.setNumeroPoliza( vigencia.getPoliza().getNumeroPolizaFormateada() );
			solicitudAutorizacionDTO.setNumeroInciso( vigencia.getNumeroInciso().longValue() );
			solicitudAutorizacionDTO.setEstatusTipoSolicitud( vigencia.getEstatus() );
			solicitudAutorizacionDTO.setOficina( vigencia.getOficina().getNombreOficina()  ); //
			solicitudAutorizacionDTO.setNumReporteCabina(vigencia.getReporte().getNumeroReporte());
		}
	}
	
	
	private void llenarDatosSolicitudAutorizacionAjuste( SolicitudAutorizacionDTO solicitudAutorizacionDTO,SolicitudAutorizacion solicitud ) {
		List<SolicitudAutorizacionAjuste> solicitudes = entidadService.findByProperty(SolicitudAutorizacionAjuste.class, "solicitudAutorizacion.id", solicitud.getId());
		if(solicitudes != null && solicitudes.size()>0){
			SolicitudAutorizacionAjuste solicitudAutorizacionAjuste= solicitudes.get(0);
			EstimacionCoberturaReporteCabina estimacion = solicitudAutorizacionAjuste.getEstimacionCobertura();
			String numeroPoliza = solicitudAutorizacionAjuste.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza().getNumeroPolizaFormateada();
			solicitudAutorizacionDTO.setNumeroPoliza(numeroPoliza);
			Integer numeroInciso = solicitudAutorizacionAjuste.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getNumeroInciso();
			solicitudAutorizacionDTO.setNumeroInciso(Long.valueOf(numeroInciso));
			String oficina = solicitudAutorizacionAjuste.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getOficina().getNombreOficina();
			solicitudAutorizacionDTO.setOficina(oficina);
						
			String nombreCobertura = estimacionCoberturaService.obtenerNombreCobertura(
					estimacion.getCoberturaReporteCabina().getCoberturaDTO(),
					estimacion.getCoberturaReporteCabina().getClaveTipoCalculo(),
					null, estimacion.getTipoEstimacion(),
					estimacion.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());
			
			solicitudAutorizacionDTO.setIdTipoSolicitud( solicitudAutorizacionAjuste.getId() );

			solicitudAutorizacionDTO.setNombreCobertura(nombreCobertura);
			solicitudAutorizacionDTO.setEstatusTipoSolicitud( solicitudAutorizacionAjuste.getEstatus() );
			
			solicitudAutorizacionDTO.setEstimacionNueva(solicitudAutorizacionAjuste.getEstimacionNueva());
			
		    Long idEstimacionCobertura = solicitudAutorizacionAjuste.getEstimacionCobertura().getId();
			BigDecimal estimacionActual = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(idEstimacionCobertura);
			solicitudAutorizacionDTO.setEstimacionActual(estimacionActual);
			BigDecimal reserva =   movimientoSiniestroService.obtenerReservaAfectacion(idEstimacionCobertura, Boolean.TRUE);
			solicitudAutorizacionDTO.setReserva(reserva);		
			solicitudAutorizacionDTO.setNumReporteCabina(solicitudAutorizacionAjuste.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getNumeroReporte() );
			solicitudAutorizacionDTO.setNoPaseAtencion(solicitudAutorizacionAjuste.getEstimacionCobertura().getFolio());
		}
	}

	private SolicitudAutorizacion transformSolicitudAutorizacionDTOToSolicitudAutorizacionVigencia( SolicitudAutorizacionDTO solicitudAutorizacionDTO){
		SolicitudAutorizacion resultado 								= new SolicitudAutorizacion();
		SolicitudAutorizacionVigencia solicitudAutorizacionVigencia 	= new SolicitudAutorizacionVigencia();
		PolizaDTO poliza 												= new PolizaDTO();
		
		poliza.setIdToPoliza( BigDecimal.valueOf( solicitudAutorizacionDTO.getPolizaId() ) );
		solicitudAutorizacionVigencia.setEstatus( ESTATUS_SOLICITUD_VIGENCIA_PENDIENTE );
		solicitudAutorizacionVigencia.setPoliza(poliza);
		resultado.setCodigoUsuarioCreacion( solicitudAutorizacionDTO.getCodUsuarioCreacion());
		resultado.setEstatus( ESTATUS_SOLICITUD_PENDIENTE );
		resultado.setFechaCreacion( solicitudAutorizacionDTO.getFechaCreacion() );
		resultado.setTipoSolicitud(TIPO_SOLICITUD_AUTORIZACION_VIGENCIA);
		resultado.setSolicitudAutorizacionVigencia(solicitudAutorizacionVigencia);
		return resultado;
	}
	
	@Override
	public void solicitarAutorizacionReserva(Long idEstimacionCobertura,BigDecimal estimacionNueva, String causaMovimiento ){
		String usuario = usuarioService.getUsuarioActual().getNombreUsuario();
		SolicitudAutorizacion solicitudAutorizacion = this.createSolicitudAutorizacion();
		SolicitudAutorizacionAjuste solicitudAutorizacionAjuste = new SolicitudAutorizacionAjuste();
		EstimacionCoberturaReporteCabina estimacion = new EstimacionCoberturaReporteCabina();
		estimacion.setId(idEstimacionCobertura);
		solicitudAutorizacionAjuste.setSolicitudAutorizacion(solicitudAutorizacion);
		solicitudAutorizacionAjuste.setEstimacionCobertura(estimacion);
		solicitudAutorizacionAjuste.setEstimacionNueva(estimacionNueva);
		solicitudAutorizacionAjuste.setCausaMovimiento(causaMovimiento);
		solicitudAutorizacionAjuste.setEstatus(ESTATUS_SOLICITUD_VIGENCIA_PENDIENTE);
		solicitudAutorizacionAjuste.setCodigoUsuarioCreacion(usuario);
		solicitudAutorizacionAjuste.setCodigoUsuarioModificacion(usuario);
		solicitudAutorizacionAjuste.setFechaCreacion(new Date());
		solicitudAutorizacionAjuste.setFechaModificacion(new Date());
		this.entidadService.save(solicitudAutorizacionAjuste);
		
	}
	
	private SolicitudAutorizacion createSolicitudAutorizacion(){
		String userId = usuarioService.getUsuarioActual().getNombreUsuario();
		SolicitudAutorizacion solicitudAutorizacion = new SolicitudAutorizacion();
		solicitudAutorizacion.setCodUsuarioAutorizador(userId);
		solicitudAutorizacion.setEstatus(ESTATUS_PENDIENTE_INT);
		solicitudAutorizacion.setFechaCreacion(Calendar.getInstance().getTime());
		solicitudAutorizacion.setFechaModEstatus(Calendar.getInstance().getTime());
		solicitudAutorizacion.setFechaModificacion(Calendar.getInstance().getTime());
		solicitudAutorizacion.setCodigoUsuarioCreacion(userId);
		solicitudAutorizacion.setCodigoUsuarioModificacion(userId);
		solicitudAutorizacion.setTipoSolicitud(TIPO_SOLICITUD);
		this.entidadService.save(solicitudAutorizacion);
		return solicitudAutorizacion;
	}
	
	
	@Override
	public List<SolicitudAutorizacionAjusteAntiguedad> buscarAutorizacionesAntiguedad (SolicitudAutorizacionAntiguedadDTO filtro){
		final Integer PRIMER_ELEMENTO = 0;
		String claveSubCalculo = filtro.getClaveSubCalculo();
		List<SolicitudAutorizacionAjusteAntiguedad> solicitudes = null;
		if(claveSubCalculo!=null){
			List<ConfiguracionCalculoCoberturaSiniestro> confCalList = entidadService.findByProperty(ConfiguracionCalculoCoberturaSiniestro.class, "", claveSubCalculo);
			ConfiguracionCalculoCoberturaSiniestro confCal = confCalList.get(PRIMER_ELEMENTO);
			filtro.setTipoEstimacion(confCal.getTipoEstimacion());
		}
		solicitudes = catSolicitudAutorizacionDao.buscarAutorizacionAntiguedad(filtro);
		this.acompletaPropiedadesTransient(solicitudes);
		return solicitudes;
	}
	
	
	private void acompletaPropiedadesTransient(List<SolicitudAutorizacionAjusteAntiguedad> solicitudes ){
		
		Map<String,String> estatusMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_SOLICITUD_ANTIGUEDAD);
		Map<String,String> tipoAjusteMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_AJUSTE);
		Map<String,String> terminoAjusteMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		Map<Long,String> lstOficina = this.listadoService.obtenerOficinasSiniestros();
		
		for(SolicitudAutorizacionAjusteAntiguedad solicitud : solicitudes){
						
			String nombreCobertura = estimacionCoberturaService.obtenerNombreCobertura(
					solicitud.getEstimacionCobertura().getCoberturaReporteCabina().getCoberturaDTO(),
					solicitud.getEstimacionCobertura().getCoberturaReporteCabina().getClaveTipoCalculo(), 
					null, solicitud.getEstimacionCobertura().getTipoEstimacion(), 
					solicitud.getEstimacionCobertura().getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());			

			solicitud.setNombreCobertura(nombreCobertura);
			Date fechaCreacionSiniestro = solicitud.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getFechaCreacion();
			int monthsBetween = Months.monthsBetween(new LocalDate(fechaCreacionSiniestro), new LocalDate()).getMonths();
			solicitud.setMesesAntiguedad(monthsBetween);
			solicitud.setEstatusDesc(estatusMap.get(solicitud.getEstatus()));
			solicitud.setTipoAjusteDesc(tipoAjusteMap.get(solicitud.getTipoAjuste()));
			String terminoAjusteDesc = solicitud.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoAjuste();
			solicitud.setTerminoAjusteDesc(terminoAjusteMap.get(terminoAjusteDesc));
			Oficina oficina = solicitud.getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getOficina();
			solicitud.setOficinaDesc(lstOficina.get(oficina.getId()));
			this.validaSiPuedeAutorizarRechazar(solicitud);
		}
	}
	
	private void validaSiPuedeAutorizarRechazar(SolicitudAutorizacionAjusteAntiguedad solicitud ){
		 
		 String rol1 = solicitud.getRolPrimerAutorizacion();
		 String rol2 = solicitud.getRolSegundaAutorizacion();
		 String rol3 = solicitud.getRolTerceraAutorizacion();
		 String rol4 = solicitud.getRolCuartaAutorizacion();
		 String estatus1 = solicitud.getEstatusPrimeraAutorizacion();
		 String estatus2 = solicitud.getEstatusSegundaAutorizacion();
		 String estatus3 = solicitud.getEstatusTercerAutorizacion();
		 String estatus4 = solicitud.getEstatusCuartaAutorizacion();
		 
		 solicitud.setAutoriza(NO_AUTORIZA);
		 List<String> rolesAutorizadores = this.catSolicitudAutorizacionDao.obtenerRolesAutorizador();
		 if(rolesAutorizadores.size()>0){
			 if(rolesAutorizadores.contains(rol1) && estatus1.equals(ESTATUS_PENDIENTE)){
				 solicitud.setAutoriza(AUTORIZA);
			 }else if(rol2!= null && rolesAutorizadores.contains(rol2) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_PENDIENTE)){
					 solicitud.setAutoriza(AUTORIZA);
			 }else if(rol3!= null && rolesAutorizadores.contains(rol3) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_AUTORIZADO) && estatus3.equals(ESTATUS_PENDIENTE)){
				 solicitud.setAutoriza(AUTORIZA);
			 }else if(rol4!= null && rolesAutorizadores.contains(rol4) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_AUTORIZADO) && estatus3.equals(ESTATUS_AUTORIZADO)  && estatus4.equals(ESTATUS_PENDIENTE)){
				 solicitud.setAutoriza(AUTORIZA);
			 }
		 }
	}

	@Override
	public List<SolicitudAutorizacionAjusteAntiguedad> obtenerAutorizacionAntiguedadPendientes(){
		List<SolicitudAutorizacionAjusteAntiguedad> solicitudes = new ArrayList<SolicitudAutorizacionAjusteAntiguedad>(); 
		List<SolicitudAutorizacionAjusteAntiguedad> solicitudesPendientesList = this.entidadService.findByProperty(SolicitudAutorizacionAjusteAntiguedad.class, "estatus", "PEND");
		List<SolicitudAutorizacionAjusteAntiguedad> solicitudesPendientesAutorizacionList = this.entidadService.findByProperty(SolicitudAutorizacionAjusteAntiguedad.class, "estatus", "PAUT");
		if(solicitudesPendientesList != null){
			solicitudes.addAll(solicitudesPendientesList);
		}
		if(solicitudesPendientesAutorizacionList != null){
			solicitudes.addAll(solicitudesPendientesAutorizacionList);
		}
		return solicitudes;
	}
	
	
	@Override
	public void autorizarSiniestroAntiguo(String solicitudes){
		final String AUTORIZADO = "AUT";
		final String PENDIENTE_AUTORIZACION = "PAUT";
		if(solicitudes!=null && !solicitudes.equals("") ){
			String[] solicitudesArray = solicitudes.trim().split(",");
			List<String> rolesAutorizadores = this.catSolicitudAutorizacionDao.obtenerRolesAutorizador();
			String codigoUsuario = this.usuarioService.getUsuarioActual().getNombreUsuario();
			for(String solicitudIdStr : solicitudesArray){
				SolicitudAutorizacionAjusteAntiguedad solicitud = this.entidadService.findById(SolicitudAutorizacionAjusteAntiguedad.class, Long.valueOf(solicitudIdStr));
				Long idEstimacionCobertura = solicitud.getEstimacionCobertura().getId();
				BigDecimal montoAjuste = solicitud.getMontoAjuste();
				String causaMovimiento = solicitud.getCausaMovimiento();
				
				this.cambiaEstatusSolicitud(rolesAutorizadores, codigoUsuario, solicitud, AUTORIZADO,PENDIENTE_AUTORIZACION);
				
				if(solicitud.getEstatus().equals(AUTORIZADO)){
					BigDecimal estimacionActual = this.movimientoSiniestroService.obtenerMontoEstimacionAfectacion(solicitud.getEstimacionCobertura().getId());
					BigDecimal estimacionNueva = estimacionActual.add(montoAjuste);
					movimientoSiniestroService.generarAjusteEstimacion(
							idEstimacionCobertura, estimacionNueva, EnumUtil.fromValue(CausaMovimiento.class, causaMovimiento), null);
					
				}
			}
		}
	}
	
	
	@Override
	public void rechazarSiniestroAntiguo(String solicitudes){
		if(solicitudes!=null && !solicitudes.equals("") ){
			String[] solicitudesArray = solicitudes.trim().split(",");
			List<String> rolesAutorizadores = this.catSolicitudAutorizacionDao.obtenerRolesAutorizador();
			String codigoUsuario = this.usuarioService.getUsuarioActual().getNombreUsuario();
			for(String solicitudIdStr : solicitudesArray){
				SolicitudAutorizacionAjusteAntiguedad solicitud = this.entidadService.findById(SolicitudAutorizacionAjusteAntiguedad.class, Long.valueOf(solicitudIdStr));
				this.cambiaEstatusSolicitud(rolesAutorizadores, codigoUsuario, solicitud, "REC","REC");
			}
		}
	}
	
	private void cambiaEstatusSolicitud(List<String> rolesAutorizadores , String codigoUsuario , SolicitudAutorizacionAjusteAntiguedad solicitud,String nuevoEstado,String estatusGeneral){
		
		String rol1 = solicitud.getRolPrimerAutorizacion();
		String rol2 = solicitud.getRolSegundaAutorizacion();
		String rol3 = solicitud.getRolTerceraAutorizacion();
   		String rol4 = solicitud.getRolCuartaAutorizacion();
   		
   		String estatus1 = solicitud.getEstatusPrimeraAutorizacion();
		String estatus2 = solicitud.getEstatusSegundaAutorizacion();
		String estatus3 = solicitud.getEstatusTercerAutorizacion();
		String estatus4 = solicitud.getEstatusCuartaAutorizacion();
		
		/*
		 * 
		 *  if(rolesAutorizadores.contains(rol1) && estatus1.equals(ESTATUS_PENDIENTE)){
				 solicitud.setAutoriza(AUTORIZA);
			 }else if(rol2!= null && rolesAutorizadores.contains(rol2) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_PENDIENTE)){
					 solicitud.setAutoriza(AUTORIZA);
			 }else if(rol3!= null && rolesAutorizadores.contains(rol3) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_AUTORIZADO) && estatus3.equals(ESTATUS_PENDIENTE)){
				 solicitud.setAutoriza(AUTORIZA);
			 }else if(rol4!= null && rolesAutorizadores.contains(rol4) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_AUTORIZADO) && estatus3.equals(ESTATUS_AUTORIZADO)  && estatus4.equals(ESTATUS_PENDIENTE)){
				 solicitud.setAutoriza(AUTORIZA);
			 }
			 
			 
		 * */
   		
   		if(rolesAutorizadores.contains(rol1) && estatus1.equals(ESTATUS_PENDIENTE)){
			 solicitud.setEstatusPrimeraAutorizacion(nuevoEstado);
			 solicitud.setCodigoUsuarioPrimera(codigoUsuario);
			 solicitud.setFechaEstatusPrimera(new Date());
			 if(rol2!= null){
				 solicitud.setEstatus(estatusGeneral);
			 }else{
				 solicitud.setEstatus(nuevoEstado);
			 }
		 }else if(rol2!= null && rolesAutorizadores.contains(rol2) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_PENDIENTE)){
			 solicitud.setEstatusSegundaAutorizacion(nuevoEstado);
			 solicitud.setCodigoUsuarioSegunda(codigoUsuario);
			 solicitud.setFechaEstatusSegunda(new Date());
			 if(rol3== null){
				 solicitud.setEstatus(nuevoEstado);
			 }
		 }else if(rol3!= null && rolesAutorizadores.contains(rol3) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_AUTORIZADO) && estatus3.equals(ESTATUS_PENDIENTE)){
			 solicitud.setEstatusTercerAutorizacion(nuevoEstado);
			 solicitud.setCodigoUsuarioTercera(codigoUsuario);
			 solicitud.setFechaEstatusTercera(new Date());
			 if(rol4== null){
				 solicitud.setEstatus(nuevoEstado);
			 }
		 }else if(rol4!= null && rolesAutorizadores.contains(rol4) && estatus1.equals(ESTATUS_AUTORIZADO) && estatus2.equals(ESTATUS_AUTORIZADO) && estatus3.equals(ESTATUS_AUTORIZADO)  && estatus4.equals(ESTATUS_PENDIENTE)){
			 solicitud.setEstatusCuartaAutorizacion(nuevoEstado);
			 solicitud.setCodigoUsuarioCuarta(codigoUsuario);
			 solicitud.setFechaEstatusCuarta(new Date());
			 solicitud.setEstatus(nuevoEstado);
		 }
   		if(estatusGeneral.equals("REC")){
   			solicitud.setEstatus(estatusGeneral);
   		}
   			
		 this.entidadService.save(solicitud);
	}
	
	@Override
	public void solicitarAutorizacionReservaAntiguedad(Long idEstimacionCobertura,BigDecimal estimacionNueva,Long idParametroReserva,String causaMovimiento){
		SolicitudAutorizacionAjusteAntiguedad solicitud = new SolicitudAutorizacionAjusteAntiguedad();
		EstimacionCoberturaReporteCabina estimacion = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstimacionCobertura);
		solicitud.setEstimacionCobertura(estimacion);
		BigDecimal estimacionActual = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(idEstimacionCobertura);
		
		BigDecimal montoAjuste = estimacionNueva.subtract(estimacionActual);
		String tipoAjuste = this.generaTipoMovimiento(montoAjuste);
		solicitud.setTipoAjuste(tipoAjuste);
		ParametroReservaAntiguo parametro = this.entidadService.findById(ParametroReservaAntiguo.class, idParametroReserva);
		solicitud.setRolPrimerAutorizacion(parametro.getRolPrimeraAutorizacion());
		solicitud.setRolSegundaAutorizacion(parametro.getRolSegundaAutorizacion());
		solicitud.setRolTerceraAutorizacion(parametro.getRolTerceraAutorizacion());
		solicitud.setRolCuartaAutorizacion(parametro.getRolCuartaAutorizacion());
		solicitud.setEstatusPrimeraAutorizacion((solicitud.getRolPrimerAutorizacion()!=null)?"PEND":null);
		solicitud.setEstatusSegundaAutorizacion((solicitud.getRolSegundaAutorizacion()!=null)?"PEND":null);
		solicitud.setEstatusTercerAutorizacion((solicitud.getRolTerceraAutorizacion()!=null)?"PEND":null);
		solicitud.setEstatusCuartaAutorizacion((solicitud.getRolCuartaAutorizacion()!=null)?"PEND":null);
		solicitud.setEstatus("PEND");
		solicitud.setMontoAjuste(montoAjuste);
		solicitud.setCausaMovimiento(causaMovimiento);
		solicitud.setCodigoUsuarioCreacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
		this.entidadService.save(solicitud);
	}
	
	private String generaTipoMovimiento(BigDecimal diferencia){
		String tipoMovimiento = "";
		if(diferencia.compareTo(BigDecimal.ZERO)>0){
			tipoMovimiento = "AA";
		}else if (diferencia.compareTo(BigDecimal.ZERO) < 0){
			tipoMovimiento = "AD";
		}
		return tipoMovimiento;
}



	@Override
	public void autorizaRechazaSolicitud(Long idSolicitudAutorizacion , Integer estatus , String tipoSolicitud, String comentario) {
		
		SolicitudAutorizacion solicitudAutorizacion = entidadService.findById(SolicitudAutorizacion.class, idSolicitudAutorizacion);
		
		if(!StringUtil.isEmpty(comentario) && solicitudAutorizacion!=null){
			
			Long keyReporteCabina           = null;
			TipoEvento tipoEvento           = null;
			
			// AGREGA COMENTARIOS EN SOLICITUD
			solicitudAutorizacion.setComentarios(comentario);
			entidadService.save(solicitudAutorizacion);
			
			// CAMBIA ESTATUS EN VIGENCIA Y RESERVAR
			this.autorizacion(idSolicitudAutorizacion, estatus , tipoSolicitud);
			
			// VALIDA LOS DATOS PARA GUARDAR LOS COMENTARIOS EN LA BITACORA
			if( TIPO_SOLICITUD_AUTORIZACION_VIGENCIA.equals( tipoSolicitud ) || TIPO_SOLICITUD_AUTORIZACION_CANCELACION.equals(tipoSolicitud)){
				List<SolicitudAutorizacionVigencia> lSolicitudVigencia = entidadService.findByProperty(SolicitudAutorizacionVigencia.class, "solicitudAutorizacion.id", solicitudAutorizacion.getId());
				if( !lSolicitudVigencia.isEmpty() ){
					keyReporteCabina = lSolicitudVigencia.get(0).getReporte().getId();
					tipoEvento = ( estatus == ESTATUS_SOLICITUD_AUTORIZADO ? TipoEvento.EVENTO_53:TipoEvento.EVENTO_54 );
				}
			}else if( TIPO_SOLICITUD_AJUSTE_RESERVA.equals( tipoSolicitud )){
				List<SolicitudAutorizacionAjuste> lSolicitudesAjuste = entidadService.findByProperty(SolicitudAutorizacionAjuste.class, "solicitudAutorizacion.id", solicitudAutorizacion.getId());
				if( !lSolicitudesAjuste.isEmpty() ){
					keyReporteCabina = lSolicitudesAjuste.get(0).getEstimacionCobertura().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
					tipoEvento = ( estatus == ESTATUS_SOLICITUD_AUTORIZADO ? TipoEvento.EVENTO_55:TipoEvento.EVENTO_56 );
				}
			}
				
			// GUARDAR EN BTACORA DE REPORTE DE CABINA
			reporteCabinaService.guardarEvento(keyReporteCabina, tipoEvento, comentario);
			
		}
		
	}
}
