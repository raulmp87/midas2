package mx.com.afirme.midas2.service.impl;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.math.BigDecimal;
import java.text.DateFormatSymbols;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.afirme.eibs.services.payments.vo.CreditCardPromotionVO;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.agente.AgenteFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.CiudadFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO_;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO_;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosFacadeRemote;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipovehiculo.TipoVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.formapago.FormaPagoIDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.tipovehiculo.SeccionTipoVehiculoId;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.catalogos.ValorSeccionCobAutosDao;
import mx.com.afirme.midas2.dao.negocio.NegocioDao;
import mx.com.afirme.midas2.dao.negocio.estadodescuento.NegocioEstadoDescuentoDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaAgrupadorTarifaDao;
import mx.com.afirme.midas2.domain.agrupadorPasajeros.AgrupadorPasajeros;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.catalogos.GrupoVariablesModificacionPrima;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVeh;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.catalogos.banco.CuentaBancoMidas;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoMoneda;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.domain.juridico.CatalogoJuridico;
import mx.com.afirme.midas2.domain.juridico.OficinaJuridico;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho.TipoDerecho;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion_;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion_;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.negocio.tipovigencia.NegocioTipoVigencia;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
import mx.com.afirme.midas2.domain.notificaciones.MovimientosProcesos;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDet;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoCobertura;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoTipoPrestador;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.pieza.PiezaValuacion;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro_;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral_;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.ConceptoGuia;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaId;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.dto.juridico.CatalogoJuridicoDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO.ValorTipo;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bonos.ConfigBonosService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.GrupoVariablesModificacionPrimaService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;
import mx.com.afirme.midas2.service.compensaciones.negociovida.NegocioVidaService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroEmisorService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaService;
import mx.com.afirme.midas2.service.fuerzaventa.OficinaService;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.juridico.CatalogoJuridicoService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.agente.NegocioAgenteService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioConfiguracionDerechoService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;
import mx.com.afirme.midas2.service.negocio.motordecision.MotorDecisionAtributosService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicioService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoService;
import mx.com.afirme.midas2.service.negocio.recuotificacion.NegocioRecuotificacionService;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.NegocioEstadoService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.negocioMunicipio.NegocioMunicipioService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.notificaciones.ConfiguracionNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.catalogo.conceptos.ConceptoAjusteService;
import mx.com.afirme.midas2.service.siniestros.catalogo.oficina.CatalogoSiniestroOficinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.oficina.CatalogoSiniestroOficinaService.OficinaFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService.ServicioSiniestroFiltro;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;
import mx.com.afirme.midas2.service.webservice.WebServiceDelegate;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.util.UtileriasWeb;
@Stateless
public class ListadoServiceImpl implements ListadoService {

	private static final Logger LOG = Logger.getLogger(ListadoServiceImpl.class);
	private NegocioEstadoService estadoService;
	private NegocioMunicipioService municipioService;
	private NegocioTipoUsoService negocioTipoUsoService;
	private NegocioTipoServicioService negocioTipoServicioService;
	private TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService;
	private ValorSeccionCobAutosDao valorSeccionCobAutosDao;
	private SeccionFacadeRemote seccionFacadeRemote;
	private MonedaFacadeRemote monedaFacadeRemote;
	private TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote;
	private CoberturaSeccionFacadeRemote coberturaSeccionFacadeRemote;
	protected TarifaAgrupadorTarifaDao tarifaAgrupadorTarifaDao;
	private TipoServicioVehiculoFacadeRemote tipoServicioVehiculoFacadeRemote;
	private TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote;
	protected EntidadDao entidadDao;
	//Added 2011.12.15-vH
	private GrupoVariablesModificacionPrimaService grupoVariablesModificacionPrimaService;
	private AgrupadorTarifaService agrupadorTarifaService;
	private ProductoFacadeRemote productoFacadeRemote;
	private PromotoriaService promotoriaService;
	private GerenciaService gerenciaService;
	private OficinaService oficinaService;
	private AgenteService agenteService;
	private CentroEmisorService centroEmisorService;
	private NegocioSeccionDao negocioSeccionDao;
	private MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;
	private EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	private EstadoFacadeRemote estadoFacadeRemote;
	private MunicipioFacadeRemote municipioFacadeRemote;
	private NegocioService negocioService;
	private TipoPolizaFacadeRemote tipoPolizaFacadeRemote;
	private NegocioProductoDao negocioProductoDao;
	private ModeloVehiculoFacadeRemote modeloVehiculoFacadeRemote;
	private NegocioAgenteService negocioAgenteService;
	private TipoBienAutosFacadeRemote tipoBienAutosService;
	private AgenteFacadeRemote agenteFacadeRemote;
	private NegocioTarifaService negocioTarifaService;
	private CatalogoValorFijoFacadeRemote catalogValorFijoService;
	private PaisFacadeRemote paisFacadeRemote;
	private ColoniaFacadeRemote coloniaFacadeRemote;
	private EntidadService entidadService;
	private GerenciaJPAService gerenciaJpaService;
	private EjecutivoService ejecutivoService;
	private PromotoriaJPAService promotoriaJpaService;
	private CotizacionService cotizacionService;
	private ProductoBancarioService productoBancarioService;
	private NegocioTipoUsoDao negocioTipoUsoDao;
	private AgenteMidasService agenteMidasService;
	private ClienteFacadeRemote clienteFacadeRemote;
	private NegocioDao negocioDao;
	private FormaPagoInterfazServiciosRemote formaPagoInterfazServiciosRemote;
	private UsuarioService usuarioService;
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	private WebServiceDelegate webServiceDelegate;
	private ConfigBonosService configBonosService;
	private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService;
	private IncisoViewService incisoService;
	private EndosoService endosoService;
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	private EntidadBitemporalService entidadBitemporalService;
	private IncisoViewService incisoViewService;
	private NegocioEstadoDescuentoDao negocioEstadoDescuentoDao;
	private ConfiguracionNotificacionesService confNotificacionesService;
	private CotizadorAgentesService cotizadorAgentesService;
	
	private ImpresionRecibosService impresionRecibosService;
	private CiudadFacadeRemote ciudadFacadeRemote;
	//Almacenar vigencia default
	private Integer vigenciaDefault = -1;
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	@EJB
	private CatalogoSiniestroOficinaService catalagoSiniestroOficinaService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private EntidadContinuityService entidadContinuityService;

	@EJB
	private SiniestroCabinaService siniestroCabinaService;
	
	@EJB
	private AutorizacionReservaService autorizacionReservaService;
	
	@EJB
	private CatalogoJuridicoService catalogoJuridicoService;
	
	@EJB
	private BancoMidasService bancoMidasService;

	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@EJB
	private ValorCatalogoAgentesService catalogoAgentesService;
	
	@EJB
	private NegocioDerechosService negocioDerechosService;

	@EJB
	private DireccionMidasService direccionMidasService;

	@EJB 
	EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;

	@EJB
	private PrestadorDeServicioService prestadorDeServicioService;
	
	@EJB
	private ServicioSiniestroService servicioSiniestroService;
	
	private SistemaContext sistemaContext;	
	@EJB
	private BancoEmisorFacadeRemote bancoEmisorFacade;
	
	@EJB
	private NegocioVidaService negocioVidaService;

	@EJB
	private NegocioConfiguracionDerechoService negocioConfiguracionDerechoService;
	
	@EJB
	private MotorDecisionAtributosService motorDecisionAtributosService;

	protected SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote;
	protected IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote;
	
	@EJB
	private NegocioRecuotificacionService negocioRecuotificacionService;

	@Override
	public Map<Long, String> getMapPaquetesPorSeccion(BigDecimal idToSeccion) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		SeccionDTO seccionDTO = entidadService.findById(SeccionDTO.class, idToSeccion);
		for(Paquete paquete: seccionDTO.getPaquetes()){
			map.put(paquete.getId(), paquete.getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapCoberturasSeccionPorSeccion(
			BigDecimal idToSeccion) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<CoberturaSeccionDTO> coberturaSeccionDTOList = coberturaSeccionFacadeRemote.getVigentesPorSeccion(idToSeccion);
		for(CoberturaSeccionDTO coberturaSeccionDTO : coberturaSeccionDTOList){
			map.put(coberturaSeccionDTO.getCoberturaDTO().getIdToCobertura(), coberturaSeccionDTO.getCoberturaDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapTipoUsoVehiculoPorSeccion(BigDecimal idToSeccion) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		@SuppressWarnings("unused")
		SeccionDTO seccion = seccionFacadeRemote.findById(idToSeccion);
		map.put(BigDecimal.ZERO, "Uso general");
		//List<TipoUsoVehiculoDTO> tipoUsoVehiculoDTOList = tipoUsoVehiculoFacadeRemote.findByClaveTipoBien(seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
		List<TipoUsoVehiculoDTO> tipoUsoVehiculoDTOList = tipoUsoVehiculoFacadeRemote.findByClaveTipoBienBySeccion(idToSeccion);
		for(TipoUsoVehiculoDTO tipoUsoVehiculoDTO : tipoUsoVehiculoDTOList){
			map.put(tipoUsoVehiculoDTO.getIdTcTipoUsoVehiculo(), tipoUsoVehiculoDTO.getDescripcionTipoUsoVehiculo());
		}
		return map;
	}
	
	@Override
	public Map<Short, String> getMapTipoLimiteSumaAseg(){
		Map<Short, String>  mapTipoLimiteSumaAseg = new LinkedHashMap<Short, String>();
		
		List<CatalogoValorFijoDTO> tipoSumaAseguradaList = entidadService.findByProperty(CatalogoValorFijoDTO.class, "id.idGrupoValores", 24);
		if(tipoSumaAseguradaList != null && !tipoSumaAseguradaList.isEmpty()){
			
			Collections.sort(tipoSumaAseguradaList, 
					new Comparator<CatalogoValorFijoDTO>() {				
						public int compare(CatalogoValorFijoDTO n1, CatalogoValorFijoDTO n2){
							int c = 0;
							if(n2.getId().getIdDato() > n1.getId().getIdDato()) c = -1;
							if(n2.getId().getIdDato() < n1.getId().getIdDato()) c = 1;
							return c;
						}
			});
			
			for(CatalogoValorFijoDTO item : tipoSumaAseguradaList){
				mapTipoLimiteSumaAseg.put((short)item.getId().getIdDato(), item.getDescripcion());
			}
		}
		return mapTipoLimiteSumaAseg;
	}
	
	@Override
	public List<MonedaDTO> getListarMonedas() {
		return monedaFacadeRemote.findAll();
	}

	@Override
	public List<ValorSeccionCobAutos> getListarValorSeccionCobAutosPorSeccion(
			Long idToSeccion) {
		List<ValorSeccionCobAutos>  tempValorSeccionCobAutosList = valorSeccionCobAutosDao.getPorIdToSeccion(idToSeccion);
		Map<Short, String> tipoLimiteSumaAsegMap = getMapTipoLimiteSumaAseg();
		int index=0;
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO = null;
		MonedaDTO monedaDTO = null;
		CoberturaSeccionDTO coberturaSeccionDTO = null;
		for(ValorSeccionCobAutos item : tempValorSeccionCobAutosList){
			tipoUsoVehiculoDTO = new TipoUsoVehiculoDTO();
			if(item.getId().getIdTcTipoUsoVehiculo().equals(new Long(0))){				
				tipoUsoVehiculoDTO.setDescripcionTipoUsoVehiculo("Uso general");
				tempValorSeccionCobAutosList.get(index).setTipoUsoVehiculoDTO(tipoUsoVehiculoDTO);
			}else{
				tipoUsoVehiculoDTO = tipoUsoVehiculoFacadeRemote.findById(BigDecimal.valueOf(tempValorSeccionCobAutosList.get(index).getId().getIdTcTipoUsoVehiculo()));
				tempValorSeccionCobAutosList.get(index).setTipoUsoVehiculoDTO(tipoUsoVehiculoDTO);
			}
			monedaDTO = monedaFacadeRemote.findById(Short.valueOf(tempValorSeccionCobAutosList.get(index).getId().getIdMoneda().toString()));
			tempValorSeccionCobAutosList.get(index).setMonedaDTO(monedaDTO);
			CoberturaSeccionDTOId coberturaSeccionDTOId = new CoberturaSeccionDTOId(BigDecimal.valueOf(tempValorSeccionCobAutosList.get(index).getId().getIdToSeccion()), BigDecimal.valueOf(tempValorSeccionCobAutosList.get(index).getId().getIdToCobertura()));
			coberturaSeccionDTO = coberturaSeccionFacadeRemote.findById(coberturaSeccionDTOId);
			tempValorSeccionCobAutosList.get(index).setCoberturaSeccionDTO(coberturaSeccionDTO);
			
			if(tipoLimiteSumaAsegMap != null){
				String tipoSumaAseguradaStr = tipoLimiteSumaAsegMap.get(item.getClaveTipoLimiteSumaAseg());
				tempValorSeccionCobAutosList.get(index).setDescripcionTipoSumaAsegurada(tipoSumaAseguradaStr);
			}
			
			index++;
		}
		
		return tempValorSeccionCobAutosList;
	}
	
	@Override
	public List<SeccionDTO> getListarSeccionesVigentesAutos() {
		return seccionFacadeRemote.listarVigentesAutos();
	}
	
	@Override
	public List<SeccionDTO> getListarSeccionesVigentesAutosUsables() {
		List<SeccionDTO>  seccionList = seccionFacadeRemote.listarVigentesAutosUsables();
		Collections.sort(seccionList, new Comparator<SeccionDTO>() {
			@Override
			public int compare(SeccionDTO object1, SeccionDTO object2) {
				return object1.getDescripcion().compareTo(object2.getDescripcion());
			}
		});
		return seccionList;
	}
	
	
	@Override
	public Map<AgrupadorTarifaId, String> getMapAgrupadorTarifaPorNegocio(
			String claveNegocio) {
		Map<AgrupadorTarifaId, String> map = new LinkedHashMap<AgrupadorTarifaId, String>();
		List<AgrupadorTarifa> result = tarifaAgrupadorTarifaDao.getListAgrupadorTarifaPorNegocio(claveNegocio);
		String desEstatus="";
		
		for(AgrupadorTarifa item : result){
//			map.put(item.getId(), item.getDescripcionAgrupador()+" ("+item.getDescripcionVersion()+")");
			switch(item.getClaveEstatus()){ 
				case 0: desEstatus="Creado";
					break;
				case 1: desEstatus="Activo";
					break;
				case 2: desEstatus="No Activo";
					break;
				case 3: desEstatus="Borrado";
					break;
			};
			map.put(item.getId(), item.getDescripcionAgrupador()+" ("+item.getId().getIdVerAgrupadorTarifa()+")"+" "+desEstatus);
		}
		return map;
	}

	@Override
	public Map<BigDecimal, String> getMapLineaNegocioPorAgrupadorTarifa(
			String idToAgrupadorTarifaFromString) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
	
		List<AgrupadorTarifaSeccion> result = tarifaAgrupadorTarifaDao.getListLineaNegocioPorAgrupadorTarifa(idToAgrupadorTarifaFromString);
		for(AgrupadorTarifaSeccion item : result){
			map.put(item.getSeccionDTO().getIdToSeccion(), item.getSeccionDTO().getDescripcion());
		}
		return map;
	}

	@Override
	public Map<Short, String> getMapMomendaPorAgrupadorTarifaLineaNegocio(
			String idToAgrupadorTarifaFromString, BigDecimal idToSeccion) {
		
		Map<Short, String> map = new LinkedHashMap<Short, String>();	
		List<AgrupadorTarifaSeccion> result = tarifaAgrupadorTarifaDao.getListMomendaPorAgrupadorTarifaLineaNegocio(idToAgrupadorTarifaFromString, idToSeccion);
	
		for(AgrupadorTarifaSeccion item : result){
			map.put(item.getMonedaDTO().getIdTcMoneda(), item.getMonedaDTO().getDescripcion());
		}
		return map;
		
	}
	

	@Override
	public Map<BigDecimal, String> getMapTipoServicioVehiculoPorTipoVehiculo(
			BigDecimal idTipoVehiculo) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();	
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO = new TipoServicioVehiculoDTO(null, idTipoVehiculo, null);
		List<TipoServicioVehiculoDTO> tipoServicioVehiculoDTOList = tipoServicioVehiculoFacadeRemote.listarFiltrado(tipoServicioVehiculoDTO);
		for(TipoServicioVehiculoDTO item : tipoServicioVehiculoDTOList){
			map.put(item.getIdTcTipoServicioVehiculo(), item.getDescripcionTipoServVehiculo());
		}
		return map;
	}

	@Override
	public Map<BigDecimal, String> getMapTipoVehiculoPorSeccion(
			BigDecimal idToSeccion) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		SeccionDTO seccionDTO = seccionFacadeRemote.findById(idToSeccion);
		TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
		tipoVehiculoDTO.getTipoBienAutosDTO().setClaveTipoBien(seccionDTO.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
		List<TipoVehiculoDTO> tipoVehiculoDTOList = tipoVehiculoFacadeRemote.listarFiltrado(tipoVehiculoDTO);
		for(TipoVehiculoDTO item :  tipoVehiculoDTOList){
			map.put(item.getIdTcTipoVehiculo(), item.getDescripcionTipoVehiculo());
		}
		return map;
	}
	
	@Override
	public List<ServVehiculoLinNegTipoVeh> getListarServVehiculoLinNegTipoVehPorSeccion(
			Long idToSeccion) {
		
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		List<ServVehiculoLinNegTipoVeh> servVehiculoLinNegTipoVehList = new ArrayList<ServVehiculoLinNegTipoVeh>();
		String query = "Select model from ServVehiculoLinNegTipoVeh model where " +
				"model.id.idToSeccion = :idToSeccion";
		parameters.put("idToSeccion",idToSeccion);
		LOG.info(query +" "+ parameters);
		@SuppressWarnings("unchecked")
		List<ServVehiculoLinNegTipoVeh> servVehiculoLinNegTipoVehListTemp = entidadDao.executeQueryMultipleResult(query, parameters);
		
		ServVehiculoLinNegTipoVeh servVehiculoLinNegTipoVeh;
		for(ServVehiculoLinNegTipoVeh item : servVehiculoLinNegTipoVehListTemp){
			servVehiculoLinNegTipoVeh = new ServVehiculoLinNegTipoVeh();
			servVehiculoLinNegTipoVeh = item;
			
			TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
			tipoVehiculoDTO = tipoVehiculoFacadeRemote.findById(BigDecimal.valueOf(servVehiculoLinNegTipoVeh.getId().getIdTcTipoVehiculo()));
			servVehiculoLinNegTipoVeh.setDescripcionTipoVehiculo(tipoVehiculoDTO.getDescripcionTipoVehiculo());			
			
			TipoServicioVehiculoDTO tipoServicioVehiculoDTO = new TipoServicioVehiculoDTO();
			tipoServicioVehiculoDTO = tipoServicioVehiculoFacadeRemote.findById(BigDecimal.valueOf(servVehiculoLinNegTipoVeh.getIdTcTipoServicioVehiculo()));
			servVehiculoLinNegTipoVeh.setDescripcionTipoServicioVehiculo(tipoServicioVehiculoDTO.getDescripcionTipoServVehiculo());

			servVehiculoLinNegTipoVehList.add(servVehiculoLinNegTipoVeh);
		}
			
		return servVehiculoLinNegTipoVehList;
	}

	/**
	 * Metodo utilizado para las variables modificadoras de prima-Catalogo Generico(Adaptacion)
	 */
	@Override
	public String getClaveTipoDetalleGrupo(Long idGrupo) {
		GrupoVariablesModificacionPrima grupo=grupoVariablesModificacionPrimaService.findById(idGrupo);
		return (grupo!=null && grupo.getClaveTipoDetalle()!=null)?grupo.getClaveTipoDetalle().toString():null;
	}
	
	@Override
	public Map<BigDecimal, String> getMapAgrupadoresPorNegocio(String claveNegocio, Long idMoneda){
		return agrupadorTarifaService.findByNegocio(claveNegocio, idMoneda);
	}
	
	@Override
	public Map<BigDecimal, String> getMapAgrupadoresPorMonedaPorSeccion(String claveNegocio, BigDecimal idSeccion, Long idMoneda){
		return agrupadorTarifaService.findByAgrupadorPorMonedaPorSeccion(claveNegocio, idSeccion, idMoneda);
	}

	@Override
	public List<ProductoDTO> getListarProductos(String claveNegocio, boolean mostrarInactivos) {
		ProductoDTO dto = new ProductoDTO();
		dto.setClaveNegocio(claveNegocio);
		return productoFacadeRemote.listarFiltrado(dto, mostrarInactivos);
	}

	@Override
	public Map<BigDecimal, String> getMapProductos(Long idToNegocio, Integer activo) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		
		List<NegocioProducto> items = negocioProductoDao.listarNegocioProductoPorNegocio(idToNegocio, activo);
		for(NegocioProducto item: items){
			map.put(item.getProductoDTO().getIdToProducto(), item.getProductoDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<Long, String> listarNegociosActivos(String cveNegocio) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		Negocio negocio = new Negocio();
		negocio.setClaveNegocio(cveNegocio);
		negocio.setClaveEstatus(Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		List<Negocio> lista = negocioService.findByFilters(negocio);
		for(Negocio obj : lista){
			map.put(obj.getIdToNegocio(), obj.getDescripcionNegocio());
		}
		return map;
	}
	
	private void convertirListaAMapaFuerzaVenta(Map<Object, String> map, List<RegistroFuerzaDeVentaDTO> list){
		for(RegistroFuerzaDeVentaDTO dto : list){
			map.put(dto.getId(), dto.getDescription());
		}
	}
	
	@Override
	public Map<Object, String> listarGerencias() {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = gerenciaService.listarGerencias();
		convertirListaAMapaFuerzaVenta(map, list);
		return map;
	}

	@Override
	public Map<Object, String> listarOficinasPorGerencia(String id) {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = oficinaService.listarOficinasPorGerencia(id);
		convertirListaAMapaFuerzaVenta(map, list);
		return map;
	}

	@Override
	public Map<Object, String> listarPromotoriasPorOficina(String id) {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = promotoriaService.listarPromotoriasPorOficina(id);
		convertirListaAMapaFuerzaVenta(map, list);
		return map;
	}

	@Override
	public Map<Object, String> listarAgentesPorPromotoria(String id) {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = agenteService.listarAgentesPorPromotoria(id);
		convertirListaAMapaFuerzaVenta(map, list);
		return map;
	}

	@Override
	public Map<Object, String> listarAgentesPorGerencia(String id) {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = agenteService.listarAgentesPorGerencia(id);
		convertirListaAMapaFuerzaVenta(map, list);
		return map;

	}

	@Override
	public Map<Object, String> listarAgentesPorOficina(String id) {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = agenteService.listarAgentesPorOficina(id);
		convertirListaAMapaFuerzaVenta(map, list);
		return map;

	}

	
	@Override
	public Map<BigDecimal, String> getMapNegocioSeccionPorTipoPoliza(
			BigDecimal idToTipoPoliza) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<NegocioSeccion> items = negocioSeccionDao.listarNegSeccionPorIdNegTipoPoliza(idToTipoPoliza);
		for(NegocioSeccion item: items){
			map.put(item.getIdToNegSeccion(), item.getSeccionDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocio(BigDecimal idToNegSeccion) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		
		List<NegocioPaqueteSeccion> items = negocioPaqueteSeccionDao.findByProperty("negocioSeccion.idToNegSeccion", idToNegSeccion);
		//Orden alfabetico
		if(items != null && !items.isEmpty()){
			Collections.sort(items, 
					new Comparator<NegocioPaqueteSeccion>() {				
						public int compare(NegocioPaqueteSeccion n1, NegocioPaqueteSeccion n2){
							return n1.getPaquete().getDescripcion().compareTo(n2.getPaquete().getDescripcion());
						}
					});
		}
		for(NegocioPaqueteSeccion item: items){
			map.put(new Long(item.getIdToNegPaqueteSeccion()), item.getPaquete().getDescription());
		}
		return map;
	}
	
	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocioCotizacionInciso(BigDecimal idToCotizacion,BigDecimal idToNegSeccion) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		
		List<NegocioPaqueteSeccion> items = negocioPaqueteSeccionDao.listarPaquetesSecccionPorCotizacionInciso(idToCotizacion, idToNegSeccion);
		for(NegocioPaqueteSeccion item: items){
			map.put(new Long(item.getIdToNegPaqueteSeccion()), item.getPaquete().getDescription());
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapMonedaPorNegTipoPoliza(
			BigDecimal idToNegTipoPoliza) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		
		NegocioTipoPoliza negocioTipoPoliza = entidadDao.findById(NegocioTipoPoliza.class, idToNegTipoPoliza);
		if(negocioTipoPoliza != null){
			List<MonedaDTO> items = monedaFacadeRemote.listarMonedasAsociadasTipoPoliza(negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza());
			for(MonedaDTO item: items){
				map.put(new BigDecimal(item.getIdTcMoneda()), item.getDescripcion());
			}
		}
		return map;
	}

	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorSeccion(BigDecimal idToNegSeccion) {
		return null;
	}
	
	@Override
	public Map<BigDecimal, String> listarMarcasPorCveTipoBien(String claveTipoBien) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		MarcaVehiculoDTO marca = new MarcaVehiculoDTO();
		TipoBienAutosDTO tipo = new TipoBienAutosDTO();
		tipo.setClaveTipoBien(claveTipoBien);
		marca.setTipoBienAutosDTO(tipo);
		List<MarcaVehiculoDTO> marcas = marcaVehiculoFacadeRemote.listarFiltrado(marca);
		for(MarcaVehiculoDTO item: marcas){
			map.put(item.getIdTcMarcaVehiculo(), item.getDescripcionMarcaVehiculo());
		}
		return map;
	}	
	
	@Override
	public Map<String, String> listarEstilosPorMarcaCveTipoBien(
			BigDecimal idTcMarcaVehiculo,String claveTipoBien) {
		
		EstiloVehiculoDTO estilo = new EstiloVehiculoDTO();
		EstiloVehiculoId id = new EstiloVehiculoId();
		id.setClaveTipoBien(claveTipoBien);
		MarcaVehiculoDTO marca = new MarcaVehiculoDTO();
		marca.setIdTcMarcaVehiculo(idTcMarcaVehiculo);
		estilo.setMarcaVehiculoDTO(marca);
		estilo.setId(id);
		estilo.getTipoBienAutosDTO().setClaveTipoBien(claveTipoBien);
		
		List<EstiloVehiculoDTO> estilos = estiloVehiculoFacadeRemote.listarDistintosFiltrado(estilo);
		Map<String, String> map = new LinkedHashMap<String, String>();
		for(EstiloVehiculoDTO item: estilos){
			map.put(item.getId().getClaveEstilo(), item.getDescripcionEstilo());
		}
		return map;
	}	
	
	@Override
	public Map<BigDecimal, String> listarTiposVehiculo(String claveTipoBien) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		TipoVehiculoDTO tipoVehiculoDTO = new TipoVehiculoDTO();
		tipoVehiculoDTO.getTipoBienAutosDTO().setClaveTipoBien(claveTipoBien);
		List<TipoVehiculoDTO> tipoVehiculoDTOList = tipoVehiculoFacadeRemote.listarFiltrado(tipoVehiculoDTO);
		for(TipoVehiculoDTO item :  tipoVehiculoDTOList){
			map.put(item.getIdTcTipoVehiculo(), item.getDescripcionTipoVehiculo());
		}
		return map;
	
	}
	
	@Override
	public Map<BigDecimal, String> getMapTipoPolizaPorProducto(BigDecimal idToProducto){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		
		List<TipoPolizaDTO> tipoPolizaDTOList = tipoPolizaFacadeRemote.listarVigentesPorIdProducto(idToProducto);
		Collections.sort(tipoPolizaDTOList, new Comparator<TipoPolizaDTO>() {

			@Override
			public int compare(TipoPolizaDTO object1, TipoPolizaDTO object2) {
				return object1.getDescripcion().compareTo(object2.getDescripcion());
			}
		});
		for(TipoPolizaDTO item :  tipoPolizaDTOList){
			map.put(item.getIdToTipoPoliza(), item.getDescripcion());
		}
		return map;
	}
	
	
	@Override
	public Map<BigDecimal, String> getMapSeccionPorTipoPoliza(BigDecimal idToTipoPoliza){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		
		List<SeccionDTO> seccionList = seccionFacadeRemote.listarVigentes(idToTipoPoliza);
		Collections.sort(seccionList, new Comparator<SeccionDTO>() {

			@Override
			public int compare(SeccionDTO object1, SeccionDTO object2) {
				return object1.getDescripcion().compareTo(object2.getDescripcion());
			}
		});
		for(SeccionDTO item : seccionList){
			map.put(item.getIdToSeccion(), item.getDescripcion());
		}
		return map;
	}
	
	public void guardarEstados(List<NegocioEstado> list,Map<String,String> estadosMap){
		for (NegocioEstado estado : list) {
			if (estado.getEstadoDTO() != null) {
				estadosMap.put(estado.getEstadoDTO().getStateId(),
						estado.getEstadoDTO().getDescription());
			}
		}
	}
	/**
	 * Obtiene un listado de los estados
	 * relacionados con un negocio
	 * @param idToNegocio
	 * @return List<NegocioEstado>
	 * @author martin
	 */
	@Override
	public Map<String, String> getEstadosPorNegocioId(Long idToNegocio, Boolean cotizacionExpress){
		Map<String, String> estadosMap = new LinkedHashMap<String, String>();
		List<NegocioEstado> list = null;
		if (!cotizacionExpress) {
			if (idToNegocio != null) {
				list = estadoService.obtenerEstadosPorNegocioId(idToNegocio);

			}
		} else {
			List<Negocio> negocioList = entidadService.findByProperty(Negocio.class, "claveDefault", Boolean.TRUE);
			if (negocioList != null && !negocioList.isEmpty()) {
				list = estadoService.obtenerEstadosPorNegocioId(negocioList.get(0).getIdToNegocio());
			}
		}
		if (!list.isEmpty()) {
			guardarEstados(list, estadosMap);
		}else{
			estadosMap = this.getMapEstadosMX();
		}		
		return estadosMap;
	}
	public void guardarMunicipios(List<NegocioMunicipio> list,Map<String,String> municipiosMap){
		for (NegocioMunicipio municipio : list) {
			municipiosMap.put(municipio.getMunicipioDTO()
					.getMunicipalityId(), municipio.getMunicipioDTO()
					.getDescription());
		}
	}
	@Override
	public Map<String, String> getMunicipiosPorEstadoId(BigDecimal idToCotizacion,String idToEstado){
		Map<String, String> municipiosMap = new LinkedHashMap<String, String>();
		List<NegocioMunicipio> list = null;
		CotizacionDTO cotizacionDTO = null;
		if (idToEstado != null) {
			if(idToCotizacion != null){
				cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
				if (cotizacionDTO != null
						&& cotizacionDTO.getSolicitudDTO() != null
						&& cotizacionDTO.getSolicitudDTO().getNegocio() != null) {
					list = municipioService.obtenerMunicipiosPorEstadoId(
							cotizacionDTO.getSolicitudDTO().getNegocio()
									.getIdToNegocio(), idToEstado);
				}else{
					municipiosMap = this.getMapMunicipiosPorEstado(idToEstado);
				}
			}else{
				List<Negocio> negocioList = entidadService.findByProperty(Negocio.class, "claveDefault", Boolean.TRUE);
				if (negocioList != null && !negocioList.isEmpty()) {
					list = municipioService.obtenerMunicipiosPorEstadoId(negocioList.get(0).getIdToNegocio(), idToEstado);
				}				
			}
			if (!list.isEmpty()) {
				guardarMunicipios(list, municipiosMap);
			}else{
				municipiosMap = this.getMapMunicipiosPorEstado(idToEstado);
			}			
		}
		return municipiosMap;
	}
	
	@Override
	public List<EstadoDTO> listarEstadosMX() {
		List<EstadoDTO> estadoList = estadoFacadeRemote.findByProperty("countryId", UtileriasWeb.KEY_PAIS_MEXICO);
		Collections.sort(estadoList,new Comparator<EstadoDTO>() {
			@Override
			public int compare(EstadoDTO object1, EstadoDTO object2) {
				return object1.getStateName().compareTo(object2.getStateName());
			}
		});
		return estadoList;
	}
	
	@Override
	public Map<String, String> getMapEstadosMX() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<EstadoDTO> estadoList = this.listarEstadosMX();

		for(EstadoDTO item : estadoList){
			map.put(item.getStateId(), item.getStateName());
		}
		return map;
	}
	
	@Override
	public Map<String, String> getMapMunicipiosPorEstado(String stateId) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<MunicipioDTO> municipioList = municipioFacadeRemote.findByProperty("stateId", stateId);
		Collections.sort(municipioList, new Comparator<MunicipioDTO>() {

			@Override
			public int compare(MunicipioDTO object1, MunicipioDTO object2) {
				return object1.getMunicipalityName().compareTo(object2.getMunicipalityName());
			}
		});
		for(MunicipioDTO item : municipioList){
			map.put(item.getMunicipalityId(), item.getMunicipalityName());
		}
		return map;
	}
	@Override
	public Map<Object, String> listarOficinas() {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = oficinaService.listarOficinas();
		convertirListaAMapaFuerzaVenta(map, list);
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapNegocioCoberturaPaqueteSeccionPorPaquete(Long idToNegPaqueteSeccion) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<NegocioCobPaqSeccion> list = entidadDao.findByProperty(NegocioCobPaqSeccion.class, "NegocioPaqueteSeccion.idToNegPaqueteSeccion", idToNegPaqueteSeccion);
		for(NegocioCobPaqSeccion item: list){
			map.put(item.getIdToNegCobPaqSeccion(), item.getCoberturaDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapMarcaVehiculoPorTipoUsoNegocioSeccion(BigDecimal idToNegSeccion) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		if(idToNegSeccion != null){
			NegocioSeccion negocioSeccion = negocioSeccionDao.findById(idToNegSeccion);
			SeccionDTO seccion = seccionFacadeRemote.findById(negocioSeccion.getSeccionDTO().getIdToSeccion());				
			List<MarcaVehiculoDTO> marcaVehiculoDTOList = marcaVehiculoFacadeRemote.findByProperty(MarcaVehiculoDTO_.tipoBienAutosDTO.getName()+"."+TipoBienAutosDTO_.claveTipoBien.getName(), seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
			for(MarcaVehiculoDTO marcaVehiculoDTO: marcaVehiculoDTOList){
				map.put(marcaVehiculoDTO.getIdTcMarcaVehiculo(), marcaVehiculoDTO.getDescripcionMarcaVehiculo());
			}
		}
		return map;
	}
    
	@Override
	public Map<BigDecimal, String> getMapTipoUsoVehiculoPorEstiloVehiculo(
			String estiloVehiculoId, BigDecimal idToNegSeccion) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		EstiloVehiculoId id = new EstiloVehiculoId();
		id.valueOf(estiloVehiculoId);
		if(id.getClaveEstilo() != null){
			EstiloVehiculoDTO estiloVehiculo = entidadService.findById(EstiloVehiculoDTO.class,id);
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
			List<TipoUsoVehiculoDTO> list = negocioTipoUsoDao.getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(idToNegSeccion, negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
			if(list.size() == 0){
				list = tipoUsoVehiculoFacadeRemote.findByProperty("idTcTipoVehiculo", estiloVehiculo.getIdTcTipoVehiculo());
			}
			for(TipoUsoVehiculoDTO item: list){
				map.put(item.getIdTcTipoUsoVehiculo(), item.getDescripcionTipoUsoVehiculo());
			}
		}
		return map;
	}
    
	@Override
	public Map<String, String> getMapTipoUsoVehiculoPorEstiloVehiculoString(
			String estiloVehiculoId, BigDecimal idToNegSeccion) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		EstiloVehiculoId id = new EstiloVehiculoId();
		id.valueOf(estiloVehiculoId);
		if(id.getClaveEstilo() != null){
			EstiloVehiculoDTO estiloVehiculo = entidadService.findById(EstiloVehiculoDTO.class,id);
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
			List<TipoUsoVehiculoDTO> list = negocioTipoUsoDao.getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(idToNegSeccion, negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
			if(list.size() == 0){
				list = tipoUsoVehiculoFacadeRemote.findByProperty("idTcTipoVehiculo", estiloVehiculo.getIdTcTipoVehiculo());
			}
			for(TipoUsoVehiculoDTO item: list){
				map.put(item.getIdTcTipoUsoVehiculo().toString(), item.getDescripcionTipoUsoVehiculo());
			}
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapTipoUsoVehiculoByNegocio (BigDecimal idToNegSeccion) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
		List<TipoUsoVehiculoDTO> list = negocioTipoUsoDao.getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(idToNegSeccion, negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
		for(TipoUsoVehiculoDTO item: list){
			map.put(item.getIdTcTipoUsoVehiculo(), item.getDescripcionTipoUsoVehiculo());
		}
		return map;
	}
	
	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculo(BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda){
		Map<String, String> map = new LinkedHashMap<String, String>();
		if(idTcMarcaVehiculo != null){
			MarcaVehiculoDTO marcaVehiculoDTO = marcaVehiculoFacadeRemote.findById(idTcMarcaVehiculo);
			EstiloVehiculoDTO estiloVehiculo = new EstiloVehiculoDTO();
			estiloVehiculo.setMarcaVehiculoDTO(marcaVehiculoDTO);
			estiloVehiculo.setTipoBienAutosDTO(marcaVehiculoDTO.getTipoBienAutosDTO());
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion  = new NegocioAgrupadorTarifaSeccion ();
			negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, idMoneda);
			List<EstiloVehiculoDTO> estiloVehiculoList = estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupador(estiloVehiculo, negocioAgrupadorTarifaSeccion, idToNegSeccion);
			for(EstiloVehiculoDTO item: estiloVehiculoList){
				map.put(item.getId().toString(), item.getDescription());
			}
		}
		return map;		
	}

    /**
	 * Lista todos los agentes de la bd
	 */
	@Override
	public Map<Object, String> listarAgentes() {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<AgenteDTO> list = agenteService.listarAgentes();
		convertirListaAMapaAgentes(map, list);
		return map;
	}
	

	@Override
	public Map<Object, String> listarCentrosEmisores() {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = centroEmisorService.listarCentrosEmisores();
		convertirListaAMapaFuerzaVenta(map, list);
		return map;
	}

	@Override
	public Map<Object, String> listarGerenciasPorCentroEmisor(String arg0) {
		// GERENCIA JPA SERVICE
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<RegistroFuerzaDeVentaDTO> list = gerenciaService.listarGerenciasPorCentroEmisor(arg0);
		convertirListaAMapaFuerzaVenta(map, list);
		return map;
	}
	@Override
	public List<Gerencia> listarGerenciasPorCentroOperacion(String id) {
		if (StringUtils.isNotBlank(id)) {
			String[] idCentrosOperacion = id.split(",");
			List<GerenciaView> listTemp = new LinkedList<GerenciaView>();
			for (String idCentroOperacion : idCentrosOperacion) {
				listTemp.addAll(gerenciaJpaService
						.findByCentroOperacionLightWeight(Long
								.valueOf(idCentroOperacion)));
			}
			List<Gerencia> gerenciaList = new LinkedList<Gerencia>();
			for(GerenciaView item : listTemp){
				Gerencia gerencia = new Gerencia();
				gerencia.setId(item.getId());
				gerencia.setDescripcion(item.getDescripcion());
				gerenciaList.add(gerencia);
			}
			return gerenciaList;
		}
		return null;

	}
	
	@Override
	public List<Ejecutivo> listarEjecutivoPorGerencia(String id) {
		if (StringUtils.isNotBlank(id)) {
			String[] idGerencias = id.split(",");
			List<EjecutivoView> listTemp = new LinkedList<EjecutivoView>();
			for (String idGerencia : idGerencias) {
				listTemp.addAll(ejecutivoService.findByGerenciaLightWeight(Long
						.valueOf(idGerencia)));
			}
			List<Ejecutivo> ejecutivoList = new LinkedList<Ejecutivo>();
			for(EjecutivoView item : listTemp) {
				Ejecutivo ejecutivo = new Ejecutivo();
				ejecutivo.setId(item.getId());
				ejecutivo.setPersonaResponsable(new Persona());
				ejecutivo.getPersonaResponsable().setNombreCompleto(item.getNombreCompleto());
				ejecutivoList.add(ejecutivo);
			}
			return ejecutivoList;
		}
		return null;
	}
	
	@Override
	public List<Promotoria> listarPromotoriaPorEjecutivo(String id) {
		if (StringUtils.isNotBlank(id)) {
			String[] idEjecutivos = id.split(",");
			List<PromotoriaView> listTemp = new LinkedList<PromotoriaView>();
			for (String idEjecutivo : idEjecutivos) {
				listTemp.addAll(promotoriaJpaService
						.findByEjecutivoLightWeight(Long.valueOf(idEjecutivo)));
			}
			List<Promotoria> promotoriaList = new LinkedList<Promotoria>();
			for (PromotoriaView item : listTemp) {
				Promotoria promotoria = new Promotoria();
				promotoria.setId(item.getId());
				promotoria.setDescripcion(item.getDescripcion());
				promotoriaList.add(promotoria);
			}
			return promotoriaList;
		}
		return null;
	}
	@Override
	public List<ValorCatalogoAgentes> listarTipoPromotoriaByPromotoria(String id) {
		if (StringUtils.isNotBlank(id)) {
			String[] idPromotorias = id.split(",");
			Promotoria filtroPromotoria = null;
			List<Promotoria> listTemp = new LinkedList<Promotoria>();
			for (String idPromotoria : idPromotorias) {
				filtroPromotoria = new Promotoria();
				filtroPromotoria.setId(Long.valueOf(idPromotoria));
				listTemp.addAll(promotoriaJpaService.findByFilters(filtroPromotoria));
			}
			List<ValorCatalogoAgentes> tipoPromotoriaList = new LinkedList<ValorCatalogoAgentes>();
			for (Promotoria promotoria : listTemp) {
				tipoPromotoriaList.add(promotoria.getTipoPromotoria());
			}
			return tipoPromotoriaList;
		}
		return null;
	}
	
	/**
	 *  Mete los elementos de una lista a un Mapa
	 * @param map referencia al objeto que le vamos a meter los elementos
	 * @param list referencia de la lista que le vamos a sacar los elementos
	 */
	private void convertirListaAMapaAgentes(Map<Object, String> map,
			List<AgenteDTO> list) {
		for (AgenteDTO dto : list) {
			map.put(dto.getId(), dto.getDescription());
		}
	}
	
	/////////////////////////////	
	
	@Override
	public Map<Long, String> getMapNegProductoPorNegocio(Long idToNegocio){
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<NegocioProducto> list = negocioProductoDao.listarNegocioProductoUsablesPorNegocio(idToNegocio);
		for(NegocioProducto item: list){
			map.put(item.getIdToNegProducto(), item.getProductoDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<String, String> getMapNegProductoPorNegocioString(Long idToNegocio){
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<NegocioProducto> list = negocioProductoDao.listarNegocioProductoUsablesPorNegocio(idToNegocio);
		for(NegocioProducto item: list){
			map.put(item.getIdToNegProducto().toString(), item.getProductoDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapNegTipoPolizaPorNegProducto(Long idToNegProducto){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<NegocioTipoPoliza> list = getNegocioTipoPoliza(idToNegProducto);
		for(NegocioTipoPoliza item: list){
			map.put(item.getIdToNegTipoPoliza(), item.getTipoPolizaDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapNegTipoPolizaPorNegProductoFlotillaOIndividual(Long idToNegProducto, int aplicaFlotilla){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<NegocioTipoPoliza> list = getNegocioTipoPoliza(idToNegProducto);
		for(NegocioTipoPoliza item: list){
			if(item.getTipoPolizaDTO() != null && item.getTipoPolizaDTO().getClaveAplicaFlotillas() != null){
				//Solo FLotillas
				if(item.getTipoPolizaDTO().getClaveAplicaFlotillas().equals(1) && aplicaFlotilla == 1){
					map.put(item.getIdToNegTipoPoliza(), item.getTipoPolizaDTO().getDescripcion());
				}
				//Solo Individuales
				if(!item.getTipoPolizaDTO().getClaveAplicaFlotillas().equals(1) && aplicaFlotilla == 0){
					map.put(item.getIdToNegTipoPoliza(), item.getTipoPolizaDTO().getDescripcion());
				}
			}
		}
		return map;
	}

	@Override
	public Map<String, String> getMapNegTipoPolizaPorNegProductoFlotillaOIndividualString(Long idToNegProducto, int aplicaFlotilla){
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<NegocioTipoPoliza> list = getNegocioTipoPoliza(idToNegProducto);
		for(NegocioTipoPoliza item: list){
			if(item.getTipoPolizaDTO() != null && item.getTipoPolizaDTO().getClaveAplicaFlotillas() != null){
				//Solo FLotillas
				if(item.getTipoPolizaDTO().getClaveAplicaFlotillas().equals(1) && aplicaFlotilla == 1){
					map.put(item.getIdToNegTipoPoliza().toString(), item.getTipoPolizaDTO().getDescripcion());
				}
				//Solo Individuales
				if(!item.getTipoPolizaDTO().getClaveAplicaFlotillas().equals(1) && aplicaFlotilla == 0){
					map.put(item.getIdToNegTipoPoliza().toString(), item.getTipoPolizaDTO().getDescripcion());
				}
			}
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapNegTipoPolizaAutoExpediblePorNegProducto(Long idToNegProducto){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<NegocioTipoPoliza> list = getNegocioTipoPoliza(idToNegProducto);
		for(NegocioTipoPoliza item: list){
			if(item.getTipoPolizaDTO() != null && item.getTipoPolizaDTO().getClaveAplicaFlotillas() != null){
				//Solo AutoExpedible Individuales
				if(item.getTipoPolizaDTO().getClaveAplicaFlotillas().equals(0) && item.getTipoPolizaDTO().getClaveAplicaAutoexpedible().equals(1)){
					map.put(item.getIdToNegTipoPoliza(), item.getTipoPolizaDTO().getDescripcion());
				}
			}
		}
		return map;
	}
	
	private List<NegocioTipoPoliza> getNegocioTipoPoliza(Long idToNegProducto){
		List<NegocioTipoPoliza> Resp = null;
		if(idToNegProducto!= null && idToNegProducto > 0) {
			Resp = entidadDao.findByProperty(NegocioTipoPoliza.class, "negocioProducto.idToNegProducto", idToNegProducto);
		} else {
			Resp = entidadDao.findAll(NegocioTipoPoliza.class, 1000);
		}
		return Resp;
	}
	
	@Override
	public Map<BigDecimal, String> getMapNegSeccionPorNegTipoPoliza(BigDecimal idToNegTipoPoliza){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<NegocioSeccion> list = null;
		if(idToNegTipoPoliza != null && idToNegTipoPoliza.doubleValue() > 0) {//JFGG
			list = entidadDao.findByProperty(NegocioSeccion.class, "negocioTipoPoliza.idToNegTipoPoliza", idToNegTipoPoliza);
		} else {//JFGG
			list = entidadDao.findAll(NegocioSeccion.class, 1000);
		}//JFGG
		for(NegocioSeccion item: list){
			map.put(item.getIdToNegSeccion(), item.getSeccionDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<Long, String> getMapNegPaqueteSeccionPorNegSeccion(BigDecimal idToNegSeccion){
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		try{
			List<NegocioPaqueteSeccion> list = null;//JFGG
			if(idToNegSeccion != null && idToNegSeccion.doubleValue() > 0) {//JFGG
				list = entidadDao.findByProperty(NegocioPaqueteSeccion.class, "negocioSeccion.idToNegSeccion", idToNegSeccion);
			} else {
				list = entidadDao.findAll(NegocioPaqueteSeccion.class, 1000);
			}//JFGG
			for(NegocioPaqueteSeccion item: list){
				map.put(item.getIdToNegPaqueteSeccion(), item.getPaquete().getDescripcion());
			}
		} catch (Exception e) {
			LOG.error ("getMapNegPaqueteSeccionPorNegSeccion.Excetion:[idToNegSeccion: "+ idToNegSeccion + " ]: " + e, e);
		}
		
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapNegCoberturaPaquetePorNegPaqueteSeccion(Long idToNegPaqueteSeccion){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<NegocioCobPaqSeccion> list = entidadDao.findByProperty(NegocioCobPaqSeccion.class, "negocioPaqueteSeccion.idToNegPaqueteSeccion", idToNegPaqueteSeccion);
		for(NegocioCobPaqSeccion item: list){
			map.put(item.getIdToNegCobPaqSeccion(), item.getCoberturaDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapCoberturaPorNegocioSeccion(Long idToNegSeccion){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		try{
			List<NegocioCobPaqSeccion> list = null;//JFGG
			if(idToNegSeccion != null && idToNegSeccion > 0) {//JFGG
				list = entidadDao.findByProperty(NegocioCobPaqSeccion.class, "negocioPaqueteSeccion.negocioSeccion.idToNegSeccion", idToNegSeccion);
			} else {
				list = entidadDao.findAll(NegocioCobPaqSeccion.class, 1000);
			}//JFGG
			for(NegocioCobPaqSeccion item: list){
				map.put(item.getCoberturaDTO().getIdToCobertura(), item.getCoberturaDTO().getDescripcion());
			}
		} catch (Exception e) {
			LOG.error ("getMapCoberturaPorNegocioSeccion.Exception[idToNegSeccion: "+ idToNegSeccion + " ]: " + e, e);
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapCoberturaPorNegPaqueteSeccion(Long idToNegPaqueteSeccion){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>(1);
		List<NegocioCobPaqSeccion> list = entidadDao.findByProperty(NegocioCobPaqSeccion.class, 
				"negocioPaqueteSeccion.idToNegPaqueteSeccion", idToNegPaqueteSeccion);
		for(NegocioCobPaqSeccion item: list){
			map.put(item.getCoberturaDTO().getIdToCobertura(), item.getCoberturaDTO().getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<Short, Short> getMapModeloVehiculoPorTipoUsoEstilo(
			BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion) {
		Map<Short, Short> map = new LinkedHashMap<Short, Short>(1);
		if(idMoneda != null && claveEstilo != null && idToNegSeccion != null){
			if(!claveEstilo.equals("")){
				NegocioSeccion negocioSeccion = negocioSeccionDao.findById(idToNegSeccion);
				SeccionDTO seccion = seccionFacadeRemote.findById(negocioSeccion.getSeccionDTO().getIdToSeccion());
				
				EstiloVehiculoId id = new EstiloVehiculoId();
				id.valueOf(claveEstilo);
				ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
				modeloVehiculoDTO.getId().setClaveEstilo(id.getClaveEstilo());
				modeloVehiculoDTO.getId().setClaveTipoBien(id.getClaveTipoBien());
				modeloVehiculoDTO.getId().setIdVersionCarga(id.getIdVersionCarga());
				modeloVehiculoDTO.getId().setIdMoneda(idMoneda);
				modeloVehiculoDTO.getId().setClaveTipoBien(seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());

				List<ModeloVehiculoDTO> modeloVehiculoList = modeloVehiculoFacadeRemote.listarFiltrado(modeloVehiculoDTO);
				for(ModeloVehiculoDTO item : modeloVehiculoList){
					map.put(item.getId().getModeloVehiculo(), item.getId().getModeloVehiculo());
				}
			}
		}
		
		return map;
	}
	
	@Override
	public Map<Long, String> listarNegociosPorAgente(Integer idTcAgente) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<Negocio> list = negocioAgenteService.listarNegociosPorAgente(idTcAgente);
		for(Negocio negocio: list){
			map.put(negocio.getIdToNegocio(), negocio.getDescripcionNegocio());
		}
		return map;
	}

	@Override
	public Map<Long, String> getNegociosPorAgente(Integer idAgente, String claveNegocio){
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<Negocio> list = cotizadorAgentesService.getNegociosPorAgente(idAgente, claveNegocio);
		for(Negocio negocio: list){
			map.put(negocio.getIdToNegocio(), negocio.getDescripcionNegocio());
		}
		return map;
	}
	
	@Override
	public Map<Long, String> getNegociosPorAgenteParaAutos(Integer idAgente){
		return this.getNegociosPorAgente(idAgente, Negocio.CLAVE_NEGOCIO_AUTOS);
	}
	
    @Override
    public Map<Long, String> getMapPaquetePorNegocioSeccion(BigDecimal idToNegSeccion) {
    	Map<Long, String>  map = new LinkedHashMap<Long, String>();
    	List<NegocioPaqueteSeccion> list = entidadDao.findByProperty(NegocioPaqueteSeccion.class, NegocioPaqueteSeccion_.negocioSeccion.getName()+"."+NegocioSeccion_.idToNegSeccion.getName(), idToNegSeccion);
    	for(NegocioPaqueteSeccion item : list){
    		map.put(item.getPaquete().getId(), item.getPaquete().getDescripcion());
    	}
    	return map;
    }
    
    @Override
	public Map<Object, String> listarCveTipoBien(){
    	Map<Object, String> map = new LinkedHashMap<Object, String>(1);
		List<TipoBienAutosDTO> tipoBienList =  tipoBienAutosService.listarFiltrado(new TipoBienAutosDTO());
		for(TipoBienAutosDTO bien : tipoBienList){
			map.put(bien.getId(), bien.getDescripcionTipoBien());
		}
		return map;
	}
	
	
	@Override
	public Map<Integer, String> getMapFormasdePago(CotizacionDTO cotizacion) {

		SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class,cotizacion.getSolicitudDTO().getIdToSolicitud());
		return  getMapFormasdePago(solicitud.getNegocio().getIdToNegocio(),solicitud.getProductoDTO().getIdToProducto());
	}	
	
	@Override
	public Map<Integer, String> getMapFormasdePago(SolicitudDTO solicitud){
		return  getMapFormasdePago(solicitud.getNegocio().getIdToNegocio(),solicitud.getProductoDTO().getIdToProducto());
	}
	
	@Override
	public Map<Integer, String> getMapFormasdePagoByNegTipoPoliza(BigDecimal idToNegTipoPoliza){
		NegocioTipoPoliza negTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idToNegTipoPoliza);
		
		return  getMapFormasdePago(negTipoPoliza.getNegocioProducto().getNegocio().getIdToNegocio(),
				negTipoPoliza.getNegocioProducto().getProductoDTO().getIdToProducto());
	}
	
	@Override
	public Map<String, String> getMapFormasdePagoByNegTipoPolizaString(BigDecimal idToNegTipoPoliza){
		NegocioProducto negocioProducto = null;
		Map<String,Object> params = new LinkedHashMap<String,Object>(1);
		Map<String, String> map = new LinkedHashMap<String, String>(1);
		NegocioTipoPoliza negTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, idToNegTipoPoliza);
	
		params.put("negocio.idToNegocio", negTipoPoliza.getNegocioProducto().getNegocio().getIdToNegocio());
		params.put("productoDTO.idToProducto", negTipoPoliza.getNegocioProducto().getProductoDTO().getIdToProducto());
		try{
			negocioProducto = entidadService.findByProperties(NegocioProducto.class,params).get(0);
			List<NegocioFormaPago> negocioFormaPago = getFormasPagoByIdToProducto(negocioProducto.getIdToNegProducto());
			  for(NegocioFormaPago item : negocioFormaPago){
				  map.put(item.getFormaPagoDTO().getIdFormaPago().toString(),item.getFormaPagoDTO().getDescripcion());
			  }
		}catch(Exception e){
			LogDeMidasEJB3.log("producto no encontrado" , Level.WARNING, null);
		}
		return map;
	}
	
	@Override
	public Integer getPrimeraFormasdePagoByNegTipoPoliza(Long idToNegProducto){
		try{
			List<NegocioFormaPago> negocioFormaPago = getFormasPagoByIdToProducto(idToNegProducto);
			return negocioFormaPago.get(0).getFormaPagoDTO().getIdFormaPago();
		}catch(Exception e){
			LogDeMidasEJB3.log("producto no encontrado" , Level.WARNING, null);
		}
		return null;
	}
	
	private List<NegocioFormaPago> getFormasPagoByIdToProducto(Long idToNegProducto){
		return entidadService.findByProperty(NegocioFormaPago.class, 
				"negocioProducto.idToNegProducto",BigDecimal.valueOf(idToNegProducto));
	}
	
	@Override
	public BigDecimal getIdToFormaDePagoByNegocioValid(BigDecimal idToNegTipoPoliza, BigDecimal idToNegTipoPolizaCom){
		Map<String, String> formasPago = getMapFormasdePagoByNegTipoPolizaString(idToNegTipoPoliza);
		 Iterator<Entry<String, String>> it = formasPago.entrySet().iterator();
		 BigDecimal idFormaPago = null;
		 while(it.hasNext()){
			 Map.Entry<String, String> pairs = it.next();
			 idFormaPago = BigDecimal.valueOf(Long.valueOf(pairs.getKey()));
			 if(idToNegTipoPolizaCom.compareTo(idFormaPago)==0) {
				 break;
			 }
		 }		 
		 return idFormaPago;
	}
	
	private Map<Integer, String> getMapFormasdePago(Long idToNegocio, BigDecimal idToProducto){
		NegocioProducto negocioProducto = null;
		Map<String,Object> params = new LinkedHashMap<String,Object>();
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		params.put("negocio.idToNegocio", idToNegocio);
		params.put("productoDTO.idToProducto", idToProducto);
		try{
			negocioProducto = entidadService.findByProperties(NegocioProducto.class,params).get(0);
			List<NegocioFormaPago> formasPagoList = entidadService.findByProperty(
					NegocioFormaPago.class, "negocioProducto.idToNegProducto",
					negocioProducto.getIdToNegProducto());
			  for(NegocioFormaPago item : formasPagoList){
				  map.put(item.getFormaPagoDTO().getIdFormaPago(),item.getFormaPagoDTO().getDescripcion());
			  }
		}catch(Exception e){
			LogDeMidasEJB3.log("producto no encontrado negocio: " + idToNegocio + "idToProducto: " + idToProducto , Level.WARNING, null);
		}
		return map;
	}
	
	@Override
	public Map<Long, Double> getMapDerechosPoliza(CotizacionDTO cotizacion) {

		  Map<Long, Double> map = new LinkedHashMap<Long, Double>();
		  List<NegocioDerechoPoliza> derechosList =  cotizacion.getSolicitudDTO().getNegocio().getNegocioDerechoPolizas();
		  for(NegocioDerechoPoliza item : derechosList){
			  map.put(item.getIdToNegDerechoPoliza(),item.getImporteDerecho());
		  }
		return map;
	}
	
	@Override
	public Map<Long, Double> getMapDerechosPolizaByNegocio(BigDecimal idNegocio) {

		  Map<Long, Double> map = new LinkedHashMap<Long, Double>();
		  //List<NegocioDerechoPoliza> derechosList =  entidadService.findByProperty(NegocioDerechoPoliza.class, "negocio.idToNegocio", idNegocio);
		  List<NegocioDerechoPoliza> derechosList =  negocioDerechosService.obtenerDerechosPoliza(idNegocio.longValue());
		  if(derechosList != null && !derechosList.isEmpty()){
				Collections.sort(derechosList, 
						new Comparator<NegocioDerechoPoliza>() {				
							public int compare(NegocioDerechoPoliza n1, NegocioDerechoPoliza n2){
								return n1.getImporteDerecho().compareTo(n2.getImporteDerecho());
							}
						});
	    	}
		  
		  for(NegocioDerechoPoliza item : derechosList){
			  map.put(item.getIdToNegDerechoPoliza(),item.getImporteDerecho());
		  }
		return map;
	}
	
	@Override
	public Map<Long, Double> getMapDerechosPolizaByNegocio(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		  Map<Long, Double> map = new LinkedHashMap<Long, Double>();
		  MotorDecisionDTO filtroDerechos = negocioConfiguracionDerechoService.obtenerFiltroDerechoParaCotizacion(idToCotizacion, numeroInciso);
		  List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = motorDecisionAtributosService.consultaValoresService(filtroDerechos);
		  List<Object> derechosList = resultadoBusqueda.get(0).getValores();
		  if(derechosList != null && !derechosList.isEmpty()){
				Collections.sort(derechosList, 
						new Comparator<Object>() {				
							public int compare(Object n1, Object n2){
								return Double.compare(Double.parseDouble(((ValorTipo)n1).getValor()),Double.parseDouble(((ValorTipo)n2).getValor()));
							}
						});
	    	}
		  
		  for(Object item : derechosList){
			  map.put(((ValorTipo)item).getIdKey(),Double.parseDouble(((ValorTipo)item).getValor()));
		  }
		return map;
	}
	
	@Override
	public Map<Long, Double> getMapDerechosPolizaByConfiguracion(Long idNegocio, BigDecimal idTipoPoliza, Long idSeccion, 
			Long idPaquete, BigDecimal idMoneda,  String estado, String municipio, Long idTipoUso) {
		  Map<Long, Double> map = new LinkedHashMap<Long, Double>();
		  MotorDecisionDTO filtroDerechos = negocioConfiguracionDerechoService.obtenerFiltroDerechoParaCotizacion(
				  idNegocio,  idTipoPoliza,  idSeccion,	 idPaquete,  idMoneda,   estado,  municipio,  idTipoUso);
		  List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = motorDecisionAtributosService.consultaValoresService(filtroDerechos);
		  List<Object> derechosList = resultadoBusqueda.get(0).getValores();
		  if(derechosList != null && !derechosList.isEmpty()){
				Collections.sort(derechosList, 
						new Comparator<Object>() {				
							public int compare(Object n1, Object n2){
								return Double.compare(Double.parseDouble(((ValorTipo)n1).getValor()),Double.parseDouble(((ValorTipo)n2).getValor()));
							}
						});
	    	}
		  
		  for(Object item : derechosList){
			  map.put(((ValorTipo)item).getIdKey(),Double.parseDouble(((ValorTipo)item).getValor()));
		  }
		return map;
	}
	
	@Override
	public Map<Long, String> getMapDerechosEndoso(SolicitudDTO solicitud) {
		
		  Map<Long, String> map = new LinkedHashMap<Long, String>();
		  
		  Short esAltaInciso = (solicitud.getClaveTipoEndoso().compareTo(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)) == 0?(short)1:(short)0);
		  
		  List<NegocioDerechoEndoso> derechosList = negocioDerechosService.obtenerDerechosEndoso(solicitud.getNegocio().getIdToNegocio(), esAltaInciso);
		  DecimalFormat f = new DecimalFormat("#0.00");
		  for(NegocioDerechoEndoso item : derechosList){
			  map.put(item.getIdToNegDerechoEndoso(),f.format(item.getImporteDerecho()));
		  }
		return map;
	}
	
	@Override
	public Map<Long,Double> listaImportesDeDerechosPorNegocio(Long idToNegocio, String tipoDerecho){
		Map<Long,Double> importes = new LinkedHashMap<Long,Double>();
		
		if(tipoDerecho.equals(TipoDerecho.POLIZA.toString())){
			Map<Long,Double> importesPoliza = this.getMapDerechosPolizaByNegocio(BigDecimal.valueOf(idToNegocio));
			return importesPoliza;
				}
		
		if(tipoDerecho.equals(TipoDerecho.ENDOSO.toString()) 
				|| tipoDerecho.equals(TipoDerecho.INCISO.toString())){
			//Para el caso de los derechos de endoso e inciso hay que preparar un dto para filtrar los derechos a buscar
			SolicitudDTO solicitud = new SolicitudDTO();
			Negocio negocio = new Negocio();
			negocio.setIdToNegocio(idToNegocio);
			solicitud.setNegocio(negocio);
			Map<Long,String> importesEndoso = new LinkedHashMap<Long,String>();
			if(tipoDerecho.equals(TipoDerecho.ENDOSO.toString())){
				solicitud.setClaveTipoEndoso(BigDecimal.valueOf(SolicitudDTO.CVE_TIPO_ENDOSO_INDEFINIDO));
				importesEndoso = getMapDerechosEndoso(solicitud);
			}
			
			if(tipoDerecho.equals(TipoDerecho.INCISO.toString())){
				solicitud.setClaveTipoEndoso(BigDecimal.valueOf(SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO));
				importesEndoso = getMapDerechosEndoso(solicitud);
			}
			//Debido a que el mapa tiene diferentes tipos de datos, hay que transformar el mapa a los valores originales
			if(importesEndoso != null){
				for(Long key : importesEndoso.keySet()){
					importes.put(key, Double.valueOf(importesEndoso.get(key)));
				}
			}
		}
		
		return importes;
	}
	
	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(
			BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, Long autoIncisoContinuityId) {
		
			Map<String, String> map = new LinkedHashMap<String, String>();
			
			if (idTcMarcaVehiculo != null) {
				
				List<EstiloVehiculoDTO> estiloVehiculoList = null;
				EstiloVehiculoDTO estiloVehiculo = new EstiloVehiculoDTO();
				/*
				if (idToNegSeccion != null) {
					NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
					if (negocioSeccion != null) {
						
						
						estiloVehiculo.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());	
					}
				}*/
								
				MarcaVehiculoDTO marcaVehiculoDTO = marcaVehiculoFacadeRemote.findById(idTcMarcaVehiculo);
				estiloVehiculo.setMarcaVehiculoDTO(marcaVehiculoDTO);
				estiloVehiculo.setTipoBienAutosDTO(marcaVehiculoDTO.getTipoBienAutosDTO());
				
				if (autoIncisoContinuityId != null) {
					
					AutoIncisoContinuity autoIncisoContinuity = entidadContinuityService
						.findContinuityByKey(AutoIncisoContinuity.class, autoIncisoContinuityId);
					
					List<BitemporalAutoInciso> autoIncisoHistory = autoIncisoContinuity.getBitemporalProperty().getHistory();
					
					if (autoIncisoHistory != null && !autoIncisoHistory.isEmpty()) {
						
						EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(); 
						estiloVehiculoId.valueOf(autoIncisoHistory.get(0).getValue().getEstiloId());
						estiloVehiculoId.setClaveEstilo(null);
						estiloVehiculoId.setClaveTipoBien(null);
						
						estiloVehiculo.setId(estiloVehiculoId);
						
					}
					
				}
				
				if (estiloVehiculo.getId() != null && estiloVehiculo.getId().getIdVersionCarga() != null) {
					List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList = null;
					if (idToNegSeccion != null) {
						NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
						try{
						seccionTipoVehiculoList = entidadService.findByProperty(SeccionTipoVehiculoDTO.class, "id.idToSeccion", negocioSeccion.getSeccionDTO().getIdToSeccion());
						}catch(Exception e){}
						if(seccionTipoVehiculoList == null || seccionTipoVehiculoList.isEmpty()){
							seccionTipoVehiculoList = new ArrayList<SeccionTipoVehiculoDTO>(1);
							SeccionTipoVehiculoDTO seccionTipoVehiculo = new SeccionTipoVehiculoDTO();
							SeccionTipoVehiculoId id = new SeccionTipoVehiculoId();
							id.setIdToSeccion(negocioSeccion.getSeccionDTO().getIdToSeccion());
							id.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
							seccionTipoVehiculo.setId(id);
							seccionTipoVehiculoList.add(seccionTipoVehiculo);
						}
					}
					estiloVehiculoList = estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupadorTodo(estiloVehiculo, null, seccionTipoVehiculoList);
				} else {
					NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion  = new NegocioAgrupadorTarifaSeccion ();
					negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, idMoneda);
					
					estiloVehiculoList = estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupador(estiloVehiculo, negocioAgrupadorTarifaSeccion, idToNegSeccion);
				}
				
				for(EstiloVehiculoDTO item: estiloVehiculoList){
					map.put(item.getId().toString(), item.getDescription());
				}
			}
			
			return map;	
	}
	
	@Override
	public  Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMonedaByDescription(
			Integer modelo, String descripcionVehiculo, BigDecimal idToNegSeccion){
		Map<String, String> map = new LinkedHashMap<String, String>();
		
		List<EstiloVehiculoDTO> estiloVehiculoList = null;
		
		estiloVehiculoList = estiloVehiculoFacadeRemote.listarDistintosFiltrado( null,modelo, descripcionVehiculo, idToNegSeccion);
	
		for(EstiloVehiculoDTO item: estiloVehiculoList){
			map.put(item.getId().toString(), item.getDescription());
		}
	
		return map;	
		
	}
	
	
	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMoneda(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId) {
		return getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAndDesc(
				idTcMarcaVehiculo, modeloVehiculo, idToNegSeccion, idMoneda, 
				autoIncisoContinuityId, null, null);
	}
	
	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAgente(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId, String descripcion, BigDecimal idAgrupadorPasajeros) {
		return getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAndDesc(
				idTcMarcaVehiculo, modeloVehiculo, idToNegSeccion, idMoneda, 
				autoIncisoContinuityId, descripcion, idAgrupadorPasajeros);
	}
	
	private Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoModeloNegocioSeccionMonedaAndDesc(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId, String descripcion, BigDecimal idAgrupadorPasajeros) {
		
		List<EstiloVehiculoDTO> estiloVehiculoList = getListarEstilo(
				idTcMarcaVehiculo, modeloVehiculo, idToNegSeccion, idMoneda,
				autoIncisoContinuityId, descripcion);
			Map<String, String> map = new LinkedHashMap<String, String>();
			
		if(idAgrupadorPasajeros != null) {
			AgrupadorPasajeros agrupadorPasajeros = entidadService.findById(AgrupadorPasajeros.class, idAgrupadorPasajeros);
			for(EstiloVehiculoDTO item: estiloVehiculoList){
				if(agrupadorPasajeros != null && item.getNumeroAsientos()!= null &&
						item.getNumeroAsientos().compareTo(agrupadorPasajeros.getValorMin().shortValue()) >= 0 &&
						item.getNumeroAsientos().compareTo(agrupadorPasajeros.getValorMax().shortValue()) <= 0) {
					map.put(item.getId().toString(), item.getDescription());
				}
			}
		} else {
			for(EstiloVehiculoDTO item: estiloVehiculoList){
				map.put(item.getId().toString(), item.getDescription());
			}
		}
		return map;
	}
	
	@Override
	public List<EstiloVehiculoDTO> getListarEstilo(
			BigDecimal idTcMarcaVehiculo, Short modeloVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, 
			Long autoIncisoContinuityId, String descripcion) {
				
		List<EstiloVehiculoDTO> estiloVehiculoList = new ArrayList<EstiloVehiculoDTO>();
						
		if (idTcMarcaVehiculo != null) {
						
								
			EstiloVehiculoDTO estiloVehiculo = new EstiloVehiculoDTO();							
				MarcaVehiculoDTO marcaVehiculoDTO = marcaVehiculoFacadeRemote.findById(idTcMarcaVehiculo);
				estiloVehiculo.setMarcaVehiculoDTO(marcaVehiculoDTO);
				estiloVehiculo.setTipoBienAutosDTO(marcaVehiculoDTO.getTipoBienAutosDTO());
				estiloVehiculo.setDescripcionEstilo(descripcion);
				if (autoIncisoContinuityId != null) {
					
					AutoIncisoContinuity autoIncisoContinuity = entidadContinuityService
						.findContinuityByKey(AutoIncisoContinuity.class, autoIncisoContinuityId);
					
					List<BitemporalAutoInciso> autoIncisoHistory = autoIncisoContinuity.getBitemporalProperty().getHistory();
					
					if (autoIncisoHistory != null && !autoIncisoHistory.isEmpty()) {
						
						EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(); 
						estiloVehiculoId.valueOf(autoIncisoHistory.get(0).getValue().getEstiloId());
						estiloVehiculoId.setClaveEstilo(null);
						estiloVehiculoId.setClaveTipoBien(null);
						
						estiloVehiculo.setId(estiloVehiculoId);
						
					}
					
				}
				
				if(modeloVehiculo != null){
					estiloVehiculo.setModeloVehiculo(modeloVehiculo);
				}
				
				if (estiloVehiculo.getId() != null && estiloVehiculo.getId().getIdVersionCarga() != null) {
					List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList = null;
					if (idToNegSeccion != null) {
						NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
						try{
						seccionTipoVehiculoList = entidadService.findByProperty(SeccionTipoVehiculoDTO.class, "id.idToSeccion", negocioSeccion.getSeccionDTO().getIdToSeccion());
						}catch(Exception e){}
						if(seccionTipoVehiculoList == null || seccionTipoVehiculoList.isEmpty()){
							seccionTipoVehiculoList = new ArrayList<SeccionTipoVehiculoDTO>(1);
							SeccionTipoVehiculoDTO seccionTipoVehiculo = new SeccionTipoVehiculoDTO();
							SeccionTipoVehiculoId id = new SeccionTipoVehiculoId();
							id.setIdToSeccion(negocioSeccion.getSeccionDTO().getIdToSeccion());
							id.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
							seccionTipoVehiculo.setId(id);
							seccionTipoVehiculoList.add(seccionTipoVehiculo);
						}
					}
					estiloVehiculoList = estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupadorTodo(estiloVehiculo, null, seccionTipoVehiculoList);
				} else {
					NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion  = new NegocioAgrupadorTarifaSeccion ();
					negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, idMoneda);
					
					estiloVehiculoList = estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupador(estiloVehiculo, negocioAgrupadorTarifaSeccion, idToNegSeccion);
				}


						}
			
		return estiloVehiculoList;	
	}

	@Override
	public Map<String, String> getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(
			BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda) {
			return getMapEstiloVehiculoPorMarcaVehiculoNegocioSeccionMoneda(idTcMarcaVehiculo, idToNegSeccion, idMoneda, null);	
	}

	@Override
	public Map<BigDecimal, String> getMapMarcaVehiculoPorNegocioSeccion(
			BigDecimal idToNegSeccion) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		if(idToNegSeccion != null){
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
			SeccionDTO seccion = entidadService.findById(SeccionDTO.class, negocioSeccion.getSeccionDTO().getIdToSeccion());
			List<MarcaVehiculoDTO> marcaVehiculoDTOList = marcaVehiculoFacadeRemote.findByProperty(MarcaVehiculoDTO_.tipoBienAutosDTO.getName()+"."+TipoBienAutosDTO_.claveTipoBien.getName(), seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
			
			for(MarcaVehiculoDTO marcaVehiculoDTO: marcaVehiculoDTOList){
				map.put(marcaVehiculoDTO.getIdTcMarcaVehiculo(), marcaVehiculoDTO.getDescripcionMarcaVehiculo());
			}
		}
		return map;
	}

	@Override
	public Map<String, String> getMapMarcaVehiculoPorNegocioSeccionString(
			BigDecimal idToNegSeccion) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		if(idToNegSeccion != null){
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
			SeccionDTO seccion = entidadService.findById(SeccionDTO.class, negocioSeccion.getSeccionDTO().getIdToSeccion());
			List<MarcaVehiculoDTO> marcaVehiculoDTOList = marcaVehiculoFacadeRemote.findByProperty(MarcaVehiculoDTO_.tipoBienAutosDTO.getName()+"."+TipoBienAutosDTO_.claveTipoBien.getName(),
					seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
			
			for(MarcaVehiculoDTO marcaVehiculoDTO: marcaVehiculoDTOList){
				map.put(marcaVehiculoDTO.getIdTcMarcaVehiculo().toString(), marcaVehiculoDTO.getDescripcionMarcaVehiculo());
			}
		}
		return map;
	}
	
	@Override
	public Map<Short, Short> getMapModeloVehiculoPorMonedaEstiloNegocioSeccion(
			BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion) {
		Map<Short, Short> map = new LinkedHashMap<Short, Short>();
		if(idMoneda != null && claveEstilo != null && idToNegSeccion != null){
			if(!claveEstilo.equals("")){
				NegocioSeccion negocioSeccion = negocioSeccionDao.findById(idToNegSeccion);
				SeccionDTO seccion = entidadService.findById(SeccionDTO.class, negocioSeccion.getSeccionDTO().getIdToSeccion());
				
				EstiloVehiculoId id = new EstiloVehiculoId();
				id.valueOf(claveEstilo);
				ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
				modeloVehiculoDTO.getId().setClaveEstilo(id.getClaveEstilo());
				modeloVehiculoDTO.getId().setClaveTipoBien(id.getClaveTipoBien());
				modeloVehiculoDTO.getId().setIdVersionCarga(id.getIdVersionCarga());
				modeloVehiculoDTO.getId().setIdMoneda(idMoneda);
				modeloVehiculoDTO.getId().setClaveTipoBien(seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());

				List<ModeloVehiculoDTO> modeloVehiculoList = modeloVehiculoFacadeRemote.listarFiltrado(modeloVehiculoDTO);
				for(ModeloVehiculoDTO item : modeloVehiculoList){
					map.put(item.getId().getModeloVehiculo(), item.getId().getModeloVehiculo());
				}
			}
		}
		
		return map;
	}

	@Override
	public Map<String, String> getMapModeloVehiculoPorMonedaEstiloNegocioSeccionString(BigDecimal idMoneda, 
			String claveEstilo, BigDecimal idToNegSeccion){
		Map<String, String> map = new LinkedHashMap<String, String>();
		if(idMoneda != null && claveEstilo != null && idToNegSeccion != null){
			if(!claveEstilo.trim().equals("")){
				NegocioSeccion negocioSeccion = negocioSeccionDao.findById(idToNegSeccion);
				SeccionDTO seccion = entidadService.findById(SeccionDTO.class, negocioSeccion.getSeccionDTO().getIdToSeccion());
				
				EstiloVehiculoId id = new EstiloVehiculoId();
				id.valueOf(claveEstilo);
				if(id.getClaveEstilo()!=null){
					ModeloVehiculoDTO modeloVehiculoDTO = new ModeloVehiculoDTO();
					modeloVehiculoDTO.getId().setClaveEstilo(id.getClaveEstilo());
					modeloVehiculoDTO.getId().setClaveTipoBien(id.getClaveTipoBien());
					modeloVehiculoDTO.getId().setIdVersionCarga(id.getIdVersionCarga());
					modeloVehiculoDTO.getId().setIdMoneda(idMoneda);
					modeloVehiculoDTO.getId().setClaveTipoBien(seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
	
					List<ModeloVehiculoDTO> modeloVehiculoList = modeloVehiculoFacadeRemote.listarFiltrado(modeloVehiculoDTO);
					for(ModeloVehiculoDTO item : modeloVehiculoList){
						map.put(item.getId().getModeloVehiculo().toString(), item.getId().getModeloVehiculo().toString());
					}
				}
			}
		}
		
		return map;
	}
	
	@Override
	public Map<Short, Short> getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(
			BigDecimal idMoneda, BigDecimal idMarca, BigDecimal idToNegSeccion) {
		Map<Short, Short> map = new LinkedHashMap<Short, Short>();
		if(idMoneda != null && idMarca != null && idToNegSeccion != null){
			try{
				NegocioSeccion negocioSeccion = negocioSeccionDao.findById(idToNegSeccion);
				SeccionDTO seccion = entidadService.findById(SeccionDTO.class, negocioSeccion.getSeccionDTO().getIdToSeccion());
				String claveTipoBien = seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien();

				NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion  = new NegocioAgrupadorTarifaSeccion ();
				negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, idMoneda);
				TarifaAgrupadorTarifa tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService.getPorNegocioAgrupadorTarifaSeccion(negocioAgrupadorTarifaSeccion);
				Long idVersionCarga = tarifaAgrupadorTarifa.getId().getIdVertarifa();
				
				List<SeccionTipoVehiculoDTO> seccionTipoVehiculoList = null;
				if (idToNegSeccion != null) {
					try{
					seccionTipoVehiculoList = entidadService.findByProperty(SeccionTipoVehiculoDTO.class, "id.idToSeccion", negocioSeccion.getSeccionDTO().getIdToSeccion());
					}catch(Exception e){}
					if(seccionTipoVehiculoList == null || seccionTipoVehiculoList.isEmpty()){
						seccionTipoVehiculoList = new ArrayList<SeccionTipoVehiculoDTO>(1);
						SeccionTipoVehiculoDTO seccionTipoVehiculo = new SeccionTipoVehiculoDTO();
						SeccionTipoVehiculoId id = new SeccionTipoVehiculoId();
						id.setIdToSeccion(negocioSeccion.getSeccionDTO().getIdToSeccion());
						id.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
						seccionTipoVehiculo.setId(id);
						seccionTipoVehiculoList.add(seccionTipoVehiculo);
					}
				}

				List<ModeloVehiculoDTO> modeloVehiculoList = modeloVehiculoFacadeRemote.buscarPorMarcaMonedaSeccionVersionCarga(idMoneda,
						idMarca, claveTipoBien, idVersionCarga, seccionTipoVehiculoList);
				for(ModeloVehiculoDTO item : modeloVehiculoList){
					map.put(item.getId().getModeloVehiculo(), item.getId().getModeloVehiculo());
				}
			}catch(Exception e){
				LOG.error ("getMapModeloVehiculoPorMonedaMarcaNegocioSeccion.Excetion: " + e, e);
			}
		}
		
		return map;
	}
	
	@Override
	public Map<String, String> getMapPaises() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<PaisDTO> paisesList = paisFacadeRemote.findAll();
		Collections.sort(paisesList, new Comparator<PaisDTO>() {
			@Override
			public int compare(PaisDTO object1, PaisDTO object2) {
				return object1.getCountryName().compareTo(object2.getCountryName());
			}
		});
		for(PaisDTO item : paisesList){
			map.put(item.getCountryId(), item.getCountryName());
		}
		return map;
	}
	
	@Override
	public Map<String, String> getMapEstados(String idPais) {
		if(idPais==null)
			idPais = "PAMEXI";
		Map<String, String> map = new LinkedHashMap<String, String>();		
		List<EstadoDTO> estadoList = estadoFacadeRemote.findByProperty("countryId", idPais);
		Collections.sort(estadoList,new Comparator<EstadoDTO>() {
			@Override
			public int compare(EstadoDTO object1, EstadoDTO object2) {
				return object1.getStateName().compareTo(object2.getStateName());
			}
		});
		for(EstadoDTO item : estadoList){
			map.put(item.getStateId(), item.getStateName());
		}
		return map;
	}	
	/**
	 * Obtiene el id del estado por medio del codigo postal
	 * @param cp
	 * @return id del estado
	 */
	@Override
	public String getEstadoIdPorCp(String cp){
		if (cp != null) {
			return estadoFacadeRemote.getStateIdByZipCode(cp);
		}
		return null;
	}
	
	@Override
	public Map<String, String> getMapColonias(String idMunicipio) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<ColoniaDTO> coloniaList = coloniaFacadeRemote.findByProperty("cityId", idMunicipio);
		Collections.sort(coloniaList, new Comparator<ColoniaDTO>() {
			@Override
			public int compare(ColoniaDTO object1, ColoniaDTO object2) {
				return object1.getColonyName().compareTo(object2.getColonyName());
			}
		});
		for(ColoniaDTO item : coloniaList){
			map.put(item.getColonyId(), item.getColonyName());
		}
		return map;
	}
	/**
	 * Obtiene la lista de colonias con un mapa de nombre de colonias como id y valor
	 * @param idMunicipio
	 * @return
	 */
	@Override
	public Map<String, String> getMapColoniasSameValue(String idMunicipio) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<ColoniaDTO> coloniaList = coloniaFacadeRemote.findByProperty("cityId", idMunicipio);
		Collections.sort(coloniaList, new Comparator<ColoniaDTO>() {
			@Override
			public int compare(ColoniaDTO object1, ColoniaDTO object2) {
				if(object2==null || object1==null){
					return -1;
				}
				if(object1.getColonyName()==null || object2.getColonyName()==null){
					return -1;
				}
				return object1.getColonyName().compareTo(object2.getColonyName());
			}
		});
		for(ColoniaDTO item : coloniaList){
			if(item.getColonyName()!=null && !item.getColonyName().isEmpty()){
				map.put(item.getColonyName(), item.getColonyName());
			}
		}
		return map;
	}
	

	@Override
	public String getCodigoPostal(String idColonia){
		ColoniaDTO coloniaDTO = coloniaFacadeRemote.findById(idColonia);
		return coloniaDTO.getZipCode();
	}
	/**
	 * Obtiene el codigo postal de una colonia por su nombre dependiendo de su municipio
	 * @param colonia
	 * @param idMunicipio
	 * @return
	 */
	public String getCodigoPostalByColonyNameAndCityId(String colonia,String idMunicipio){
		BigDecimal idCity=(idMunicipio!=null && !idMunicipio.isEmpty())?new BigDecimal(idMunicipio):null;
		List<ColoniaDTO> colonias = coloniaFacadeRemote.findByColonyName(idCity, colonia);
		if(colonias!=null && !colonias.isEmpty()){
			ColoniaDTO coloniaDTO=colonias.get(0);
			return coloniaDTO.getZipCode();
		}
		return null;
	}

	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String arg0) {
		return estiloVehiculoFacadeRemote.listarDistintosFiltrado(null, null, arg0, null);		
	}
	
	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcionClaveTipoBien(String arg0, String cveTipoBien) {
		return estiloVehiculoFacadeRemote.listarDistintosFiltrado(cveTipoBien, null, arg0, null);		
	}
	
	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String tipoBien, String descripcion, BigDecimal idToNegSeccion, BigDecimal idMoneda) {	
		EstiloVehiculoDTO estiloVehiculo = new EstiloVehiculoDTO();
		TipoBienAutosDTO bienAuto = new TipoBienAutosDTO();
		bienAuto.setClaveTipoBien(tipoBien);		
		estiloVehiculo.setTipoBienAutosDTO(bienAuto);
		estiloVehiculo.setDescripcionEstilo(descripcion);
		NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion  = new NegocioAgrupadorTarifaSeccion ();
		negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, idMoneda);		
		return estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupador(estiloVehiculo, negocioAgrupadorTarifaSeccion, idToNegSeccion);		
	}
	
	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String tipoBien, String descripcion, BigDecimal idToNegSeccion, BigDecimal idMarca,
			Short modeloVehiculo, BigDecimal idMoneda) {	
		EstiloVehiculoDTO estiloVehiculo = new EstiloVehiculoDTO();
		TipoBienAutosDTO bienAuto = new TipoBienAutosDTO();
		bienAuto.setClaveTipoBien(tipoBien);		
		estiloVehiculo.setTipoBienAutosDTO(bienAuto);
		estiloVehiculo.setDescripcionEstilo(descripcion);
		
		MarcaVehiculoDTO marcaVehiculoDTO = new MarcaVehiculoDTO();
		marcaVehiculoDTO.setIdTcMarcaVehiculo(idMarca);
		estiloVehiculo.setMarcaVehiculoDTO(marcaVehiculoDTO);
		estiloVehiculo.setModeloVehiculo(modeloVehiculo);
		
		NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion  = new NegocioAgrupadorTarifaSeccion ();
		negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, idMoneda);		
		return estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupador(estiloVehiculo, negocioAgrupadorTarifaSeccion, idToNegSeccion);		
	}
	
	@Override
	public List<EstiloVehiculoDTO> listarEstilosPorDescripcion(String tipoBien, String descripcion, BigDecimal idToNegSeccion) {
		BigDecimal idMonedaDefault = null;
		NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
		try{
			idMonedaDefault = negocioSeccion.getNegocioTipoPoliza().
				getTipoPolizaDTO().getMonedas().get(0).getId().getIdMoneda();
		}catch(Exception ex){
			idMonedaDefault = BigDecimal.valueOf(484);
		}
		return this.listarEstilosPorDescripcion(tipoBien, descripcion, idToNegSeccion, idMonedaDefault);
	}

	@Override
	@Deprecated
	public List<AgenteDTO> listarAgentesPorDescripcion(String arg0) {
		return agenteFacadeRemote.listarAgentesBusqueda(arg0);
	}
	
	@Override
	public List<Agente> listaAgentesPorDescripcion(String descripcion){
		Agente filtro = new Agente();
		filtro.setPersona(new Persona());
		filtro.getPersona().setNombreCompleto(descripcion);
		return agenteMidasService.findByFilters(filtro);
	}
	@Override
	public List<AgenteView> listaAgentesPorDescripcionLightWeight(String descripcion){
		Agente filtro = new Agente();
		filtro.setPersona(new Persona());
		filtro.getPersona().setNombreCompleto(descripcion);
		return agenteMidasService.findByFilterLightWeight(filtro);
	}
	@Override
	public List<AgenteView> listaAgentesPorDescripcionLightWeightAutorizados(String descripcion){
		Agente filtro = new Agente();
		if(UtileriasWeb.isNumeric(descripcion)){
			filtro.setIdAgente(Long.valueOf(descripcion));
		}else{
			filtro.setPersona(new Persona());
			filtro.getPersona().setNombreCompleto(descripcion);
		}
		
		//filtro.setFechaVencimientoCedula(new Date());
		GregorianCalendar gcFechaFin = new GregorianCalendar();
		gcFechaFin.setTime(new Date());
		gcFechaFin.set(GregorianCalendar.HOUR_OF_DAY, 0);
		gcFechaFin.set(GregorianCalendar.MINUTE, 0);
		gcFechaFin.set(GregorianCalendar.SECOND, 0);
		gcFechaFin.set(GregorianCalendar.MILLISECOND, 0);
		filtro.setFechaVencimientoCedula(gcFechaFin.getTime());
		
		try {
			final ValorCatalogoAgentes estatusActivo = catalogoAgentesService.obtenerElementoEspecifico("Estatus de Agente (Situacion)", "AUTORIZADO");
			filtro.setTipoSituacion(estatusActivo);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")) {
			//Limitado a los agentes de la promotoria.
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			filtro.setPromotoria(agenteUsuarioActual.getPromotoria());
		}
		return agenteMidasService.findByFilterLightWeight(filtro);
	}
	
	@Override
	public List<Negocio> listaNegociosPorDescripcion(String descripcion){
		if (usuarioService.tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")) {
			Agente agenteUsuarioActual = usuarioService.getAgenteUsuarioActual();
			List<Negocio> negocios = new ArrayList<Negocio>(1);
			try{
			List<Negocio> list = negocioService.listarNegociosPorAgenteUnionNegociosLibres(agenteUsuarioActual.getId().intValue(), Negocio.CLAVE_NEGOCIO_AUTOS);
			if(list != null && list.size() > 0){
				for(Negocio negocio : list){
					if(negocio.getDescripcionNegocio().toUpperCase().contains(descripcion.toUpperCase())){
						negocios.add(negocio);
					}
				}
			}	
			}catch(Exception e){
			}
			return negocios;
		}else{
			Usuario usuario = usuarioService.getUsuarioActual();
			Boolean esExterno = null;
			if(usuario != null){
				esExterno = usuario.isOutsider();
			}
			return negocioDao.listarNegociosPorDescripcion(descripcion, esExterno);
		}
	}

	@Override
	public Map<Long, String> listarNegociosPorAgente(Integer idTcAgente, String cveTipoNegocio,
			Integer status) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<Negocio> list = negocioService.listarNegociosPorAgenteUnionNegociosLibres(idTcAgente, cveTipoNegocio);
		for(Negocio negocio : list){
			map.put(negocio.getIdToNegocio(), negocio.getDescripcionNegocio());
		}
		return map;
	}

	@Override
	public Map<Object, String> listarCatalogoValorFijo(Integer catalogId) {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<CatalogoValorFijoDTO> lista = 
			catalogValorFijoService.findByProperty("id.idGrupoValores", catalogId);
		for(CatalogoValorFijoDTO catalogo : lista){
			map.put(catalogo.getId().getIdDato(),catalogo.getDescripcion());
		}
		return map;
	}

	/**
	 * Lista Todas las lineas de negocio de una cotizacion
	 * @author Mart�n
	 */
	@Override
	public Map<Object,String> listarLineasDeNegocioPorCotizacionId(
			BigDecimal idToCotizacion) {
		Map<Object, String> map = new LinkedHashMap<Object, String>();
		List<SeccionCotizacionDTO> list = seccionCotizacionFacadeRemote.listarPorCotizacion(idToCotizacion);
		Iterator<SeccionCotizacionDTO> i = list.iterator();
		while(i.hasNext()){
			SeccionCotizacionDTO obj = (SeccionCotizacionDTO) i.next();
			map.put(obj.getSeccionDTO().getIdToSeccion(),obj.getSeccionDTO().getNombreComercial());
		}
		return map;
	}
	/**
	 * Lista Todas las descripciones de los incisos conteneidos en una cotización
	 * @author Martin
	 */
	@Override
	public Set<String> listarIncisosDescripcionByCotId(BigDecimal idToCotizacion){
		Set<String> listDescripcion =  new HashSet<String>();
		List<IncisoCotizacionDTO> listaIncisos = incisoCotizacionFacadeRemote.findByCotizacionId(idToCotizacion);
		Iterator<IncisoCotizacionDTO> i = listaIncisos.iterator();
		while(i.hasNext()){
			IncisoCotizacionDTO obj = (IncisoCotizacionDTO) i.next();
			listDescripcion.add(obj.getIncisoAutoCot().getDescripcionFinal());
		}
		return listDescripcion;
	}
	
	@Override
	public Map<Short, String> getMapMonedaPorAgrupadorTarifa(
			String idToAgrupadorTarifaFromString) {
		
		Map<Short, String> map = new LinkedHashMap<Short, String>();
		if(idToAgrupadorTarifaFromString!=null){
			List<MonedaDTO> result = tarifaAgrupadorTarifaDao.getListMonedaPorAgrupadorTarifa(idToAgrupadorTarifaFromString);
	
			for(MonedaDTO item : result){
				map.put(item.getIdTcMoneda(), item.getDescripcion());
			}
		}
		return map;
	}
	
	@Override
	public Map<Long, String> getMapMonedas(){
		Map<Long, String> map = new LinkedHashMap<Long, String>();	
		List<MonedaDTO> result = monedaFacadeRemote.findAll();
		for(MonedaDTO item : result){
			map.put(item.getIdTcMoneda().longValue(), item.getDescripcion());
		}
		return map;
	}

	/**
	 * Valida si numero de riesgos esta completa
	 * @author maritn
	 */
	@Override
	public boolean validaDatosRiesgosCompletosCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		return validaDatosRiesgoCompletos(idToCotizacion, numeroInciso, ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus());
	}
	
	/**
	 * Valida si numero de riesgos esta completa
	 * @author maritn
	 */
	@Override
	public boolean validaDatosRiesgoCompletos(BigDecimal idToCotizacion, BigDecimal numeroInciso, Short nivelConfiguracionRiesgos) {
		boolean riesgosValidos = true;
		if(idToCotizacion != null && numeroInciso != null){	
			riesgosValidos = cotizacionService.validaDatosRiesgo(idToCotizacion, numeroInciso, nivelConfiguracionRiesgos);					
		}
		return riesgosValidos;
	}
	
	/**
	 * Valida si se requieren datos de riesgo para un inciso al nivel configurado
	 * @author maritn
	 */
	public boolean requiereDatosRiesgoIncisoCotizacion(BigDecimal idToCotizacion, BigDecimal numeroInciso){
		return requiereDatosRiesgoInciso(idToCotizacion, numeroInciso, ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus());
	}
	
	/**
	 * Validar si un inciso requiere datos riesgo
	 * @param idToCotizacion
	 * @param numeroIncisos
	 * @return
	 */
	@Override
	public boolean requiereDatosRiesgoInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso, Short nivelConfiguracionRiesgos){
		boolean requiereDatosRiesgo = false;
		if(idToCotizacion != null && numeroInciso != null){	
			requiereDatosRiesgo = cotizacionService.requiereDatosRiesgo(idToCotizacion, numeroInciso, nivelConfiguracionRiesgos);					
		}
		return requiereDatosRiesgo;
	}
	
	/**
	 * Carga la lista de gerencias por centro de operacion por cascadeo
	 * @param idCentroOperacion
	 * @return
	 */
	public Map<Long,String> getMapGerenciasPorCentroOperacion(Long idCentroOperacion){
		Map<Long,String> map=new LinkedHashMap<Long, String>();
		List<Gerencia> list=gerenciaJpaService.findByCentroOperacion(idCentroOperacion);
		if(list!=null && !list.isEmpty()){
			for(Gerencia gerencia:list){
				map.put(gerencia.getId(),gerencia.getDescripcion());
			}
		}
		return map;
	}
	/**
	 * Carga la lista de ejecutivos por gerencia por cascadeo
	 * @param idGerencia
	 * @return
	 * @throws Exception
	 */
	public Map<Long,String> getMapEjecutivosPorGerencia(Long idGerencia){
		Map<Long,String> map=new LinkedHashMap<Long, String>();
//		List<Ejecutivo> list=ejecutivoService.findByGerencia(idGerencia);
		List<EjecutivoView> list = new ArrayList<EjecutivoView>(1); //ejecutivoService.findByGerenciaLightWeight(idGerencia);
	    StringBuilder queryString = new StringBuilder();
	    queryString.append(" select ID as id, IDEJECUTIVO as idEjecutivo, NOMBRE as nombreCompleto from MIDAS.toEjecutivo ej ");
	    queryString.append(" inner join seycos.persona p on p.ID_PERSONA = ej.IDPERSONA ");
	    queryString.append(" where CLAVEESTATUS = 1  ");
	    queryString.append(" and GERENCIA_ID =  "+idGerencia);
		
		Query query=entityManager.createNativeQuery(queryString.toString(),EjecutivoView.class);
		list = query.getResultList();
		try{
			if(list!=null && !list.isEmpty()){
				for(EjecutivoView ejecutivo:list){
//					Persona responsable=(ejecutivo.getPersonaResponsable()!=null && ejecutivo.getPersonaResponsable().getIdPersona()!=null)?ejecutivo.getPersonaResponsable():null;
					//PersonaSeycosDTO persona=personaSeycosFacadeRemote.findById(idPersona);
//					String nombre=(responsable!=null)?responsable.getNombreCompleto():null;
//					String nombre="";
//					if(persona!=null){
//						nombre=persona.getNombreCompleto();
//					}
//					map.put(ejecutivo.getId(),nombre);
					map.put(ejecutivo.getId(), ejecutivo.getNombreCompleto());
				}
			}
		}catch(Exception ex){
			throw new RuntimeException("Imposible onbtener persona de seycos");
		}
		return map;
	}
	
	/**
	 * Carga la lista de todos los ejecutivos/oficinas
	 * @param idGerencia
	 * @return
	 * @throws Exception
	 */
	public Map<Long,String> getMapEjecutivos(){
		Map<Long,String> map=new LinkedHashMap<Long, String>();
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select ej.id, ej.idEjecutivo, pe.nombre as nombreCompleto, ej.claveEstatus  ");
		queryString.append(" from MIDAS.toejecutivo ej ");
		queryString.append(" inner join seycos.persona pe on pe.id_persona  = ej.idpersona where ej.claveEstatus = 1");
		List<EjecutivoView> list=this.entityManager.createNativeQuery(queryString.toString(), EjecutivoView.class).getResultList();
		try{
			if(list!=null && !list.isEmpty()){
				for(EjecutivoView ejecutivo:list){
//					if(ejecutivo!=null && ejecutivo.getClaveEstatus()==1L){
//						Persona responsable=(ejecutivo.getPersonaResponsable()!=null && ejecutivo.getPersonaResponsable().getIdPersona()!=null)?ejecutivo.getPersonaResponsable():null;
	//					PersonaSeycosDTO persona=personaSeycosFacadeRemote.findById(idPersona);
//						String nombre=(responsable!=null)?responsable.getNombreCompleto():null;
	//					if(persona!=null){
	//						nombre=persona.getNombreCompleto();
	//					}
						map.put(ejecutivo.getId(),ejecutivo.getNombreCompleto());
//					}
				}
			}
		}catch(Exception ex){
			throw new RuntimeException("Imposible onbtener persona de seycos");
		}
		return map;
	}
	
	/**
	 * Carga la lista de todos los ejecutivos/oficinas
	 * @param idGerencia
	 * @return
	 * @throws Exception
	 */
	public Map<Long,String> getMapEjecutivosResponsable(){
		Map<Long,String> map=new LinkedHashMap<Long, String>();
		List<Ejecutivo> list=ejecutivoService.findAllEjecutivoResponsable();
		try{
			if(list!=null && !list.isEmpty()){
				for(Ejecutivo ejecutivo:list){
					if(ejecutivo!=null && ejecutivo.getClaveEstatus()==1L){
						Persona responsable=(ejecutivo.getPersonaResponsable()!=null && ejecutivo.getPersonaResponsable().getIdPersona()!=null)?ejecutivo.getPersonaResponsable():null;
						String nombre=(responsable!=null)?responsable.getNombreCompleto():null;
						map.put(ejecutivo.getId(),nombre);
					}
				}
			}
		}catch(Exception ex){
			throw new RuntimeException("Imposible onbtener persona de seycos");
		}
		return map;
	}
	
	/**
	 * Carga la lista de promotorias por ejecutivo por cascadeo
	 * @param idEjecutivo
	 * @return
	 */
	public Map<Long,String> getMapPromotoriasPorEjecutivo(Long idEjecutivo){
		Map<Long,String> map=new LinkedHashMap<Long, String>();
		List<Promotoria> list= new ArrayList<Promotoria>(1); 
		//promotoriaJpaService.findByEjecutivo(idEjecutivo);
		Map<String,Object> param = new HashMap<String, Object>(1);
		param.put("ejecutivo.id", idEjecutivo);
		list = this.entidadService.findByProperties(Promotoria.class, param);
		if(list!=null && !list.isEmpty()){
			for(Promotoria promotoria:list){
				map.put(promotoria.getId(),promotoria.getDescripcion());
			}
		}
		return map;
	}

	@Override
	public Map<Long, String> getMapGerencias() {		
		Map<Long,String> map=new LinkedHashMap<Long, String>();
//		List<Gerencia> list=entidadService.findAll(Gerenca.class);
		StringBuilder queryString = new StringBuilder();
		queryString.append("select distinct entidad.id as id, entidad.idGerencia as idGerencia, entidad.descripcion as descripcion, entidad.claveestatus as claveestatus from MIDAS.toGerencia entidad where entidad.claveestatus = 1 order by entidad.id desc");
		List<GerenciaView> list=this.entityManager.createNativeQuery(queryString.toString(),GerenciaView.class).getResultList();
//		Gerencia filtro=new Gerencia();
//		List<GerenciaView> list=gerenciaJpaService.findByFiltersView(filtro);
		if(list!=null && !list.isEmpty()){
			for(GerenciaView gerencia:list){
//				if(gerencia!=null && gerencia.getClaveEstatus()==1l){
					map.put(gerencia.getId(),gerencia.getDescripcion());
//				}
			}
		}
		return map;
	}
	
	@Override
	public Map<Long, String> getMapPromotorias() {
		Map<Long,String> map=new LinkedHashMap<Long, String>();
//		List<Gerencia> list=entidadService.findAll(Gerencia.class);
		Promotoria filtro=new Promotoria();
		List<PromotoriaView> list=promotoriaJpaService.findByFiltersView(filtro);
		if(list!=null && !list.isEmpty()){
			for(PromotoriaView promotoria:list){
				if(promotoria!=null && promotoria.getClaveEstatus()==1l){
					map.put(promotoria.getId(),promotoria.getDescripcion());
				}
			}
		}
		return map;
	}
	/**
	 * Obtiene la lista de productos bancarios para los datos contables del agente
	 * @return
	 */
	@Override
	public List<ProductoBancario> getListaProductosBancarios(){
		List<ProductoBancario> productos=entidadService.findAll(ProductoBancario.class);
		return productos;
	}
	@Override
	public List<ProductoBancario> getListaProductosBancariosPorBanco(Long idBanco){
		LogDeMidasInterfaz.log("Entrando a getListaProductosBancariosPorBanco... return productoBancarioService.findByBanco("+idBanco+")" + this, Level.INFO, null);
		return productoBancarioService.findByBanco(idBanco);
	}
	
	@Override
	public Map<BigDecimal,Integer> getListaVersionesPorCodigoProducto(
			String codigo) {
		Map<BigDecimal, Integer> result = new TreeMap<BigDecimal, Integer>();
		if(codigo != null){
			result =  productoFacadeRemote.getListaVersionesPorCodigoProducto(codigo);
		}
		return result;
	}

	@Override
	public Map<Long, String> getMapAgentesPorPromotoria(Long idPromotoria) {
		Map<Long,String> map=new LinkedHashMap<Long, String>();
		List<Agente> list=agenteMidasService.findAgentesByPromotoria(idPromotoria);
		if(list!=null && !list.isEmpty()){
			for(Agente agente:list){
				map.put(agente.getId(), agente.getPersona().getNombreCompleto());
			}
		}
		return map;
	}

	//Solo muestra los autorizados
	@Override
	public Map<Long, String> getMapAgentesPorGerenciaOficinaPromotoria(
			Long gerencia, Long oficina, Long idPromotoria, Short soloAutorizados) {
		Map<Long,String> map=new HashMap<Long, String>(1);
		Agente filtro = new Agente(gerencia, oficina, idPromotoria);
		List<Agente> list=agenteMidasService.findByFilters(filtro);
		if(list!=null && !list.isEmpty()){
			for(Agente agente:list){
				if(soloAutorizados == null || soloAutorizados == 0){
					map.put(agente.getId(), agente.getPersona().getNombreCompleto());
				}else{
					if(agente.getTipoSituacion().getIdRegistro().equals(Agente.ID_REGISTRO_AUTORIZADO) &&
							(agente.getFechaVencimientoCedula() != null && agente.getFechaVencimientoCedula().after(new Date()))){
						map.put(agente.getId(), agente.getPersona().getNombreCompleto());
					}
				}
			}
		}
		return map;
	}

	@Override
	public Map<Long, String> getConductosCobro(Long idCliente,
			Integer idTipoConductoCobro) {
		Map<Long, String> map = new HashMap<Long, String>(1);
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		if(idCliente == null){
			return map;
		}
		cliente.setIdCliente(BigDecimal.valueOf(idCliente));
		try{
			List<ClienteGenericoDTO> list = clienteFacadeRemote.loadMediosPagoPorCliente(cliente, "SYSTEM");
			for(ClienteGenericoDTO dto : list){
				if(dto.getIdTipoConductoCobro()!=null &&  dto.getIdTipoConductoCobro().equals(idTipoConductoCobro)){
					map.put(dto.getIdConductoCobranza(), dto.getNumeroTarjetaCobranza());
				}
			}
			if(list != null && !list.isEmpty()) {
				map.put((long)list.get(list.size()-1).getIdConductoCobranza()+1,"Agregar nuevo...");				
			}else{
				map.put((long)1,"Agregar nuevo...");
			}

		}catch(Exception ex){
			LOG.error(ex.getMessage(), ex);
		}
		return map;
	}

	@Override
	public BigDecimal getTipoUsoDefault(BigDecimal idToNegSeccion) {
		NegocioSeccion negocioSeccion = entidadService.findById(
				NegocioSeccion.class, idToNegSeccion);
		if (negocioSeccion != null) {
			NegocioTipoUso negocioTipoUso = negocioTipoUsoService
					.buscarDefault(negocioSeccion);
			if (negocioTipoUso != null
					&& negocioTipoUso.getTipoUsoVehiculoDTO() != null) {
				return negocioTipoUso.getTipoUsoVehiculoDTO()
						.getIdTcTipoUsoVehiculo();
			}
		}
		return null;
	}

	@Override
	public BigDecimal getTipoServicioDefault(BigDecimal idToNegSeccion) {
		NegocioSeccion negocioSeccion = entidadService.findById(
				NegocioSeccion.class, idToNegSeccion);
		if (negocioSeccion != null) {
			NegocioTipoServicio negocioTipoServicio = negocioTipoServicioService.buscarDefault(negocioSeccion);
			if (negocioTipoServicio != null) {
				return negocioTipoServicio.getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo();
			}
		}
		return null;
	}
	
	@Override
	public Long getDerechoPolizaDefault(BigDecimal idNegocio) {
		
		List<NegocioDerechoPoliza> derechosPolizaList =  entidadService.findByProperty(NegocioDerechoPoliza.class, "negocio.idToNegocio", idNegocio);
		  
		if(derechosPolizaList != null && !derechosPolizaList.isEmpty()){
		  	for(NegocioDerechoPoliza negocioDerechoPoliza: derechosPolizaList ){
		   		if(negocioDerechoPoliza.getClaveDefault()){
		   			return negocioDerechoPoliza.getIdToNegDerechoPoliza();
		   		}
		   	}
		}
		return null;
	}
	
	@Override
	public Long getDerechoPolizaDefaultByConfiguracion(Long idNegocio, BigDecimal idTipoPoliza, Long idSeccion, 
			Long idPaquete, BigDecimal idMoneda,  String estado, String municipio, Long idTipoUso) {
		Long idToNegDerechoPoliza = null;
		boolean defaultEnListaConfigDerecho = false;
		MotorDecisionDTO filtroDerechos = negocioConfiguracionDerechoService.obtenerFiltroDerechoParaCotizacion(
				idNegocio,  idTipoPoliza,  idSeccion,	 idPaquete,  idMoneda,   estado,  municipio,  idTipoUso);
		List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = motorDecisionAtributosService.consultaValoresService(filtroDerechos);
		List<Object> derechosList =  resultadoBusqueda.get(0).getValores();
//		Obteniendo el default de la lista de configuraciones de derecho
		if(derechosList != null && !derechosList.isEmpty()){
			for(Object item : derechosList){
				if(((ValorTipo)item).getTipValor().equals(ResultadoMotorDecisionAtributosDTO.TipoValor.TIPO_VALOR_DEFAULT)){
					idToNegDerechoPoliza = ((ValorTipo)item).getIdKey();
					break;
		}}}
//		En caso de que no se haya configurado default, obtiene el default de los parametros generales
		if(idToNegDerechoPoliza == null){
			idToNegDerechoPoliza = getDerechoPolizaDefault(BigDecimal.valueOf(filtroDerechos.getIdToNegocio()));
		}
//		Revisando si el default configurado en los parametros generales se encuentra en la lista de configuraciones de derecho, 
//		De lo contrario se establece el primer elemento de los resultados como el default
		if(derechosList != null && !derechosList.isEmpty()){
			for(Object item : derechosList){
				if(((ValorTipo)item).getIdKey().equals(idToNegDerechoPoliza)){
					defaultEnListaConfigDerecho = true;
					break;
			}}
//			Si no se encontro el default de parametros generales en la lista, se devuelve el primer elemento de la lista de resultados
			if(!defaultEnListaConfigDerecho){
				idToNegDerechoPoliza = ((ValorTipo)derechosList.get(0)).getIdKey();
			}
		}

		return idToNegDerechoPoliza;
	}
	
	@Override
	public Long getDerechoPolizaDefault(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		Long idToNegDerechoPoliza = null;
		boolean defaultEnListaConfigDerecho = false;
		MotorDecisionDTO filtroDerechos = negocioConfiguracionDerechoService.obtenerFiltroDerechoParaCotizacion(idToCotizacion, numeroInciso);
		List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = motorDecisionAtributosService.consultaValoresService(filtroDerechos);
		List<Object> derechosList =  resultadoBusqueda.get(0).getValores();
//		Obteniendo el default de la lista de configuraciones de derecho
		if(derechosList != null && !derechosList.isEmpty()){
			for(Object item : derechosList){
				if(((ValorTipo)item).getTipValor().equals(ResultadoMotorDecisionAtributosDTO.TipoValor.TIPO_VALOR_DEFAULT)){
					idToNegDerechoPoliza = ((ValorTipo)item).getIdKey();
					break;
		}}}
//		En caso de que no se haya configurado default, obtiene el default de los parametros generales
		if(idToNegDerechoPoliza == null){
			idToNegDerechoPoliza = getDerechoPolizaDefault(BigDecimal.valueOf(filtroDerechos.getIdToNegocio()));
		}
//		Revisando si el default configurado en los parametros generales se encuentra en la lista de configuraciones de derecho, 
//		De lo contrario se establece el primer elemento de los resultados como el default
		if(derechosList != null && !derechosList.isEmpty()){
			for(Object item : derechosList){
				if(((ValorTipo)item).getIdKey().equals(idToNegDerechoPoliza)){
					defaultEnListaConfigDerecho = true;
					break;
			}}
//			Si no se encontro el default de parametros generales en la lista, se devuelve el primer elemento de la lista de resultados
			if(!defaultEnListaConfigDerecho){
				idToNegDerechoPoliza = ((ValorTipo)derechosList.get(0)).getIdKey();
			}
		}

		return idToNegDerechoPoliza;
	}
	
	@Override
	public Long getCountNegocioEstiloVehiculo(BigDecimal idToNegocioSeccion,BigDecimal idMoneda){
		NegocioSeccion negSeccion = entidadService.findById(
				NegocioSeccion.class, idToNegocioSeccion);
		Map<String,Object> params = new HashMap<String, Object>();
		TarifaAgrupadorTarifa tarifaAgrupadorTarifa = null;
		NegocioAgrupadorTarifaSeccion negAgrupadorTarifaSeccion = null;
		negAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegocioSeccion,idMoneda);	
		
		if (negAgrupadorTarifaSeccion != null) {
			tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService.getPorNegocioAgrupadorTarifaSeccion(negAgrupadorTarifaSeccion);
		}
		params.put("negocioSeccion.idToNegSeccion",idToNegocioSeccion);
		params.put("estiloVehiculoDTO.tipoBienAutosDTO.claveTipoBien", negSeccion.getSeccionDTO()
						.getTipoVehiculo().getTipoBienAutosDTO()
						.getClaveTipoBien());
		params.put("estiloVehiculoDTO.id.idVersionCarga", tarifaAgrupadorTarifa.getId().getIdVertarifa());
		try{
			return  entidadService.findByPropertiesCount(NegocioEstiloVehiculo.class, params);
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			return 0L;
		}
		
	}
	
	@Override
	public Double getPctePagoFraccionado(Integer idFormaPago, Short idMoneda) {
		FormaPagoIDTO formaPagoIDTO = null;
		Double porcentajeRecargoPagoFraccionado = null;
		Usuario user = null;
		
		try {
			user = usuarioService.getUsuarioActual();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		if(idFormaPago != null && idMoneda != null){
			try {
        		formaPagoIDTO = formaPagoInterfazServiciosRemote.getPorIdClaveNegocio(idFormaPago.intValue(), 
        				idMoneda, user != null ? user.getNombreUsuario() : "SYSTEM", "A");
				if(formaPagoIDTO != null && formaPagoIDTO.getPorcentajeRecargoPagoFraccionado() != null){
					porcentajeRecargoPagoFraccionado = formaPagoIDTO.getPorcentajeRecargoPagoFraccionado() != null ?
							formaPagoIDTO.getPorcentajeRecargoPagoFraccionado().doubleValue() : null;					
				}					
			} catch (SystemException e) {
				LOG.error(e.getMessage(), e);
			}			
		}				
		return porcentajeRecargoPagoFraccionado;
	}
	
	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocio(
			short claveTipoEndoso, Long cotizacionContinuityId, Date validoEn,
			BigDecimal idToNegSeccion) {
		
		return listadoIncisosDinamicoService.getMapNegocioPaqueteSeccionPorLineaNegocio(claveTipoEndoso, cotizacionContinuityId, validoEn, idToNegSeccion);
	}
	@Override
	public Map<BigDecimal,String> getAgrupadorTarifaById(BigDecimal idToAgrupadorTarifa) {
		Map<BigDecimal,String> mapAgrupadores = new HashMap<BigDecimal, String>();
		List <AgrupadorTarifa> agrupadorTarifas = new ArrayList<AgrupadorTarifa>();
		agrupadorTarifas = entidadService.findByProperty(AgrupadorTarifa.class, "id.idToAgrupadorTarifa", idToAgrupadorTarifa);
		for (AgrupadorTarifa agrupadorTarifa : agrupadorTarifas) {
			mapAgrupadores.put(agrupadorTarifa.getId()
					.getIdVerAgrupadorTarifa(),agrupadorTarifa.getDescripcionVersion() + " "
							+ agrupadorTarifa.getDescripcionEstatus());
		}
			return mapAgrupadores;
	}

	@Override
	public Long getNegocioProducto(Long idToNegocio,
			BigDecimal idToProducto) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("negocio.idToNegocio", idToNegocio);
		params.put("productoDTO.idToProducto", idToProducto);
		try{
			NegocioProducto negocioProducto = entidadService.findByProperties(NegocioProducto.class, params).get(0);
			return negocioProducto.getIdToNegProducto();
		} catch(RuntimeException exception) {
			return 0l;
		}
	}	
	
	@Override
	public Map<String, String> getPromocionesTC(String numTarjeta){
		Map<String, String> map = new HashMap<String, String>();
		
		try{
			CreditCardPromotionVO[] promociones = webServiceDelegate.obtenerPromocionesTC(numTarjeta);
		
			if(promociones != null)
				for(CreditCardPromotionVO promocion: promociones){
					map.put(promocion.getId(), promocion.getValue());
				}
		}catch(Exception e){
			//Only for testing!!
			/*if(numTarjeta!= null && !numTarjeta.trim().equals(""))
				map.put("012", "12 MESES SIN INTERESES");*/
		}
		
		return map;
	}
	
	@Override
	public List<GenericaAgentesView> listaProducto(String listaId) {
		if (StringUtils.isNotBlank(listaId)) {
//			String[] ids = listaId.split(",");
			List<GenericaAgentesView> listaProductos = new LinkedList<GenericaAgentesView>();
//			for(String id : ids) {
//				ValorCatalogoAgentes filtro = new ValorCatalogoAgentes();
//	            GrupoCatalogoAgente catalogoLineaVenta=new GrupoCatalogoAgente();
//	            catalogoLineaVenta.setDescripcion("Lineas de Venta");
//	            filtro.setGrupoCatalogoAgente(catalogoLineaVenta);
//	            if(StringUtils.isNotBlank(id)) {
//	            	filtro.setClave(id);
	            	try {
//	            		listaProductos.addAll(configBonosService.getProductosPorLineaVentaView(filtro));
	            		listaProductos.addAll(configBonosService.getProductosPorLineaVentaView(listaId));
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
					}
//	            }
//			}
			return listaProductos;
		}
		return null;
	}
	
	@Override
	public List<GenericaAgentesView> listaLineaNegocioSeycos(String listaIds,String listaIds2, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaLineaNegocioSeycos(listaIds,listaIds2));
	}
	@Override
	public List<GenericaAgentesView> listaRamoSeycos(String listaIds, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaRamoSeycos(listaIds));
	}
	@Override
	
	public List<GenericaAgentesView> listaSubramoSeycos(String listaIds, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaSubramoSeycos(listaIds));
	}
	@Override
	public List<GenericaAgentesView> listaProductoSeycos(String listaIds, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaProductoSeycos(listaIds));
	}
	public List<GenericaAgentesView> listaLineaNegocioSeycos(String listaId,String listaId2) {
		if (StringUtils.isNotBlank(listaId)) {
			List<GenericaAgentesView> listaLineaNegocio = new LinkedList<GenericaAgentesView>();
				try {
					String[] idParametro = listaId.split(",");
					int i;
					for(i=0;i<idParametro.length;i++){
						Long idListaNeg = Long.valueOf(idParametro[i]);
						listaLineaNegocio.addAll(negocioVidaService.getLineasNegocioPorRamoProductoView(idListaNeg.toString(),listaId2));
					}
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			return listaLineaNegocio;
		}
		return null;
	}
	
	public List<GenericaAgentesView> listaSubramoSeycos(String listaId) {
		if (StringUtils.isNotBlank(listaId)) {
			List<GenericaAgentesView> listaSubRamo = new LinkedList<GenericaAgentesView>();
				try {
					String[] idParametro = listaId.split(",");
					int i;
					for(i=0;i<idParametro.length;i++){
						Long idListaSub = Long.valueOf(idParametro[i]);
						listaSubRamo.addAll(negocioVidaService.getSubramosPorRamoView(idListaSub.toString()));
					}
						
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			return listaSubRamo;
		}
		return null;
	}
	public List<GenericaAgentesView> listaProductoSeycos(String listaId) {
		if (StringUtils.isNotBlank(listaId)) {
			List<GenericaAgentesView> listaProducto = new LinkedList<GenericaAgentesView>();
				try {
					String[] idParametro = listaId.split(",");
					int i;
					for(i=0;i<idParametro.length;i++){
						Long idListaSub = Long.valueOf(idParametro[i]);
						listaProducto.addAll(negocioVidaService.getProductosVidaView(idListaSub));
					}
						
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			return listaProducto;
		}
		return null;
	}
	public List<GenericaAgentesView> listaRamoSeycos(String listaId) {
		if (StringUtils.isNotBlank(listaId)) {
			List<GenericaAgentesView> listaRamo = new LinkedList<GenericaAgentesView>();
				try {
					String[] idParametro = listaId.split(",");
					int i;
					for(i=0;i<idParametro.length;i++){
						Long idLista = Long.valueOf(idParametro[i]);
						listaRamo.addAll(negocioVidaService.getRamosPorProductosView(idLista.toString()));
					}
				} catch (NumberFormatException e) {
					LOG.error("-- listaRamoSeycos()", e);
				} catch (Exception e) {
					LOG.error("-- listaRamoSeycos()", e);
				}
			return listaRamo;
		}
		return null;
	}
	
	@Override
	public List<GenericaAgentesView> listaGerenciaSeycos() {
		List<GenericaAgentesView> listaGerencia = new LinkedList<GenericaAgentesView>();
		try {
			listaGerencia.addAll(negocioVidaService.getGerenciaVidaView());
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return listaGerencia;
	}

	@Override
	public List<DatosSeycos> listarAgenteSeycos(String agente, String idGerencias) {
		if (agente != null) {
			List<DatosSeycos> listaAgenteSeycos = new LinkedList<DatosSeycos>();
				try {
					listaAgenteSeycos = negocioVidaService.getAgenteVidaView(agente, idGerencias);
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			return listaAgenteSeycos;
		}
		return null;
	}
	
	@Override
	public List<GenericaAgentesView> listaProducto(String listaIds, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaProducto(listaIds));
	}
	
	@Override
	public List<GenericaAgentesView> listaLineaNegocio(String listaId,String listaId2) {
		if (StringUtils.isNotBlank(listaId)) {
//			String[] ids = listaId.split(",");
			List<GenericaAgentesView> listaLineaNegocio = new LinkedList<GenericaAgentesView>();
//			for (String id : ids) {
				try {
//					listaLineaNegocio.addAll(configBonosService.getLineaNegocioPorRamoView(listaId));
					listaLineaNegocio.addAll(configBonosService.getLineasNegocioPorRamoProductoView(listaId,listaId2));
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
//			}
			return listaLineaNegocio;
		}
		return null;
	}
	
	@Override
	public List<GenericaAgentesView> listaLineaNegocio(String listaIds,String listaIds2, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaLineaNegocio(listaIds,listaIds2));
	}
	
	@Override
	public List<GenericaAgentesView> listaRamo(String listaId) {
		if (StringUtils.isNotBlank(listaId)) {
//			String[] ids = listaId.split(",");
			List<GenericaAgentesView> listaRamo = new LinkedList<GenericaAgentesView>();
//			for (String id : ids) {
				try {
//					if (StringUtils.isNotBlank(id)) {
//						listaRamo.addAll(configBonosService.getRamosPorProductosView((long) Integer.parseInt(id)));
					listaRamo.addAll(configBonosService.getRamosPorProductosView(listaId));
//					}
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
//			}
			return listaRamo;
		}
		return null;
	}
	
	@Override
	public List<GenericaAgentesView> listaRamo(String listaIds, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaRamo(listaIds));
	}
	
	@Override
	public List<GenericaAgentesView> listaSubramo(String listaId) {
		if (StringUtils.isNotBlank(listaId)) {
//			String[] ids = listaId.split(",");
			List<GenericaAgentesView> listaSubRamo = new LinkedList<GenericaAgentesView>();
//			for (String id : ids) {
				try {
//					if (StringUtils.isNotBlank(listaId)) {
						listaSubRamo.addAll(configBonosService.getSubramosPorRamoView(listaId));
//					}
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
//			}
			return listaSubRamo;
		}
		return null;
	}
	
	@Override
	public List<GenericaAgentesView> listaSubramo(String listaIds, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaSubramo(listaIds));
	}
	
	@Override
	public List<GenericaAgentesView> listaCoberturas(String listaId) {
		if (StringUtils.isNotBlank(listaId)) {
//			String[] ids = listaId.split(",");
			List<GenericaAgentesView> listaCoberturas = new LinkedList<GenericaAgentesView>();
//			for (String id : ids) {
				try {
					listaCoberturas.addAll(configBonosService.getCoberturasPorLineasNegocioView(listaId));
				} catch (NumberFormatException e) {
					LOG.error(e.getMessage(), e);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
//			}
			return listaCoberturas;
		}
		return null;
	}
	
	@Override
	public List<GenericaAgentesView> listaCoberturas(String listaIds, String listaSiguienteID) {
		return getListaChecados(listaSiguienteID,listaCoberturas(listaIds));
	}
	
	/**
	 * Obtiene el numero de riesgos
	 * @author maritn
	 */
	@Override
	public int obtieneNumeroDeRiesgosBitemporal(Long incisoContinuityId, Date validoEn, Short tipoEndoso) {
		if (incisoContinuityId != null) {		
			
			List<ControlDinamicoRiesgoDTO> listaRiesgos = configuracionDatoIncisoBitemporalService.getDatosRiesgo(
									incisoContinuityId, validoEn, new LinkedHashMap<String, String>(), tipoEndoso, null, true);
			if (listaRiesgos != null && !listaRiesgos.isEmpty()) {
				boolean datosRiesgoCapturados = configuracionDatoIncisoBitemporalService.validaDatosRiesgos(
																			incisoContinuityId, validoEn, tipoEndoso);
				if (!datosRiesgoCapturados) {
					return 1;
				} else {
					return 2;
				}	
			}
		}
		return 0;
	}
	
	@Override
	public boolean mostrarDatosConductor(Long incisoContinuityId, Date validoEn) {
		boolean mostrar = false;
		if (incisoContinuityId != null) {		
			
			mostrar = incisoService.infoConductorEsRequerido(incisoContinuityId, validoEn, true);
			
		}
		return mostrar;
	}
	
	@Override
	public boolean mostrarPagoFraccionado(Long idNegocio) {
		boolean mostrar = false;
		if (idNegocio != null) {		
			Negocio negocio = entidadService.findById(Negocio.class, idNegocio);
			try{
				mostrar = negocio.getAplicaPctPagoFraccionado();
			}catch(Exception e){
			}
		}
		return mostrar;
	}
	
	@Override
	public Map<Integer, String> listadoMotivoEndoso(Long tipoEndoso, Long polizaId) {
		Map<Integer, String> map = new HashMap<Integer, String>();		
		
		List<CatalogoValorFijoDTO> listado = endosoService.listadoMotivoEndoso(tipoEndoso, polizaId);
		
		if (listado != null && !listado.isEmpty()) {
			for (CatalogoValorFijoDTO valor: listado) {
				map.put(valor.getId().getIdDato(), valor.getDescripcion());
			}
		}
		
		return map;		
	}
	
	
	private List<GenericaAgentesView> getListaChecados(String listaSiguienteID, List<GenericaAgentesView>lista){
		if (listaSiguienteID != null && lista != null) {
			String []idsChecados = listaSiguienteID.split(",");
			if(!listaSiguienteID.equals(null)&&!listaSiguienteID.isEmpty()){
				for(GenericaAgentesView obj :lista){
					for(int i=0; i<idsChecados.length; i++){
						if(idsChecados[i].equals(obj.getId().toString())){
							obj.setChecado(1);
							break;
						}
					}
				}
			}
		}
		return lista;
	}
	
	@Override
	public boolean validaDatosComplementariosIncisoBorrador(Long polizaId, Long incisoContinuityId, Short claveTipoEndoso, Date validoEn){
		PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
		BitemporalCotizacion bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(), TimeUtils.getDateTime(validoEn));
		BitemporalInciso bitemporalInciso = incisoViewService.getIncisoInProcess(incisoContinuityId, validoEn);
		if(bitemporalInciso == null){
			//FIXME En ciertas pólizas regresa null al hacer un Endoso del tipo Agregar Inciso
			bitemporalInciso = incisoViewService.getInciso(incisoContinuityId, validoEn);
		}
		int tipoValido = 0;
		if(bitemporalInciso != null){
			tipoValido = configuracionDatoIncisoBitemporalService.validaDatosComplementariosIncisoBorrador(bitemporalCotizacion, bitemporalInciso, claveTipoEndoso, validoEn);
		}
		boolean valido = true;
		if(tipoValido != 0){
			valido = false;
		}
		return valido;
	}
	/**
	 * Obtiene el listado de elementos de un catalogo de ValorCatalogoAgentes
	 * @param catalogo
	 * @return
	 */
	public List<ValorCatalogoAgentes> getCatalogoAgentes(String catalogo){
		List<ValorCatalogoAgentes> list=new ArrayList<ValorCatalogoAgentes>();
		if(isValid(catalogo)){
			try {
				list=catalogoAgentesService.obtenerElementosPorCatalogo(catalogo);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return list;
	}
	
	/**
	 * Obtiene un mapa con los datos que pertencen a un talogo. 
	 * Map<Long,String> = Llave: id, Valor: valor
	 * @autor martin 
	 */
	public Map<Long,String> mapValorCatalogoAgente(String nombreCatalogo){
		Map<Long,String> map = null;
		try {
			List<ValorCatalogoAgentes> catalogoAgentes = valorCatalogoAgentesService.obtenerElementosPorCatalogo(nombreCatalogo);
			if (catalogoAgentes != null) {
				map = new HashMap<Long, String>();
				for (ValorCatalogoAgentes item : catalogoAgentes) {
					map.put(item.getId(), item.getValor());
				}
			}
		} catch (Exception e) {
			LOG.error ("mapValorCatalogoAgente.Excetion:[nombreCatalogo: "+ nombreCatalogo + " ]: " + e, e);
		}
		return map;
	}
	/**
	 * Obtiene el id del elemento catalogo
	 * @param catalogo
	 * @param valor
	 * @return
	 */
	public Long getIdElementoValorCatalogoAgentes(String catalogo,String valor){
		Long idElemento=null;
		if(isValid(catalogo) && isValid(valor)){
			try {
				ValorCatalogoAgentes elemento=catalogoAgentesService.obtenerElementoEspecifico(catalogo, valor);
				if(isNotNull(elemento)){
					idElemento=elemento.getId();
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return idElemento;
	}
	
	@EJB
	public void setWebServiceDelegate(WebServiceDelegate webServiceDelegate){
		this.webServiceDelegate = webServiceDelegate;
	}

	@Override
	public Map<Long, String> getMapMovimientosPorProceso(BigDecimal idProceso) {		
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<MovimientosProcesos> movPros=entidadService.findByProperty(MovimientosProcesos.class, "idProceso", idProceso);
		for(MovimientosProcesos movsProcesos: movPros){
			map.put(movsProcesos.getId(), movsProcesos.getDescripcion());
		}
		return map;
	}
	@EJB
	public void setValorCatalogoAgentesService(
			ValorCatalogoAgentesService valorCatalogoAgentesService) {
		this.valorCatalogoAgentesService = valorCatalogoAgentesService;
	}

	@Override
	public Map<Integer, String> getMapMonths() {
		Map<Integer,String> months = new HashMap<Integer, String>();
		String[] monthsArray = new DateFormatSymbols(new Locale("es","MX")).getMonths();
		for (int x = 1; x < monthsArray.length;x++) {
			months.put(x, monthsArray[x-1]);
		}
		return months;
	}

	@Override
	public Map<Integer, Integer> getMapYears(int range) {
		Calendar cal = Calendar.getInstance();
		Map<Integer,Integer> years = new LinkedHashMap<Integer, Integer>();
		try {
			if (range >= 0) {
				for (int y = 0; y <= range; y++) {
					years.put(cal.get(Calendar.YEAR)-y, cal.get(Calendar.YEAR)-y);
				}
			}
		} catch(RuntimeException error) {
			years.put(Calendar.YEAR, Calendar.YEAR);
		}
		return years;
	}

	@Override
	public Map<String, String> obtenerCatalogoValorFijo(
			TIPO_CATALOGO tipo, boolean orderByDescription) {
		return this.obtenerCatalogoValorFijo(tipo, null, orderByDescription);
	}

	@Override
	public Map<String, String> obtenerCatalogoValorFijo(
			TIPO_CATALOGO tipo) {
		return this.obtenerCatalogoValorFijo(tipo, null, Boolean.FALSE);
	}

	@Override
	public Map<String, String> obtenerCatalogoValorFijo(
			TIPO_CATALOGO tipo, String codigoPadreId) {
		return this.obtenerCatalogoValorFijo(tipo, null, Boolean.FALSE);
	}


	@Override
	public Map<String, String> obtenerCatalogoValorFijo(
			TIPO_CATALOGO tipo, String codigoPadreId, boolean orderByDescription) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<CatValorFijo> valores = catalogoGrupoValorService.obtenerCatalogoValores(tipo, codigoPadreId, orderByDescription);
		for(CatValorFijo valor : valores){
			map.put(valor.getCodigo(), valor.getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<String, String> obtenerCatalogoPorCodigo(TIPO_CATALOGO tipo,
			String codigo) {
		Map<String,String> map = new LinkedHashMap<String, String>();
		CatValorFijo valor = catalogoGrupoValorService.obtenerValorPorCodigo(tipo, codigo);
		if(null!= valor){
			map.put(valor.getCodigo(), valor.getDescripcion());
		}
		return map;
	}
	
	@Deprecated
	@Override
	public Map<String, String> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<CatValorFijo> valores = null;
		valores = estimacionCoberturaSiniestroService.obtenerTipoDePasePorTipoEstimacion(codigoTipoEstimacion, codigoTipoResponsabilidad);
		if(valores == null || valores.isEmpty()){
			valores = estimacionCoberturaSiniestroService.obtenerTipoDePaseMenosSoloRegistro();
		}
		for(CatValorFijo valor : valores){
			map.put(valor.getCodigo(), valor.getDescripcion());
		}
		return map;
	}

	@Override
	public Map<String, String> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad, String codigoCausaSiniestro, String codigoTerminoAjuste) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<CatValorFijo> valores = null;
		valores = estimacionCoberturaSiniestroService.obtenerTipoDePasePorTipoEstimacion(codigoTipoEstimacion, codigoTipoResponsabilidad, codigoCausaSiniestro, codigoTerminoAjuste);
		if(valores == null || valores.isEmpty()){
			valores = estimacionCoberturaSiniestroService.obtenerTipoDePaseMenosSoloRegistro();
		}
		for(CatValorFijo valor : valores){
			map.put(valor.getCodigo(), valor.getDescripcion());
		}
		return map;
	}


	@Override
	public Map<Long, String> obtenerOficinasSiniestros() {
		Map<Long, String> values = new LinkedHashMap<Long, String>();
		OficinaFiltro filtro = new OficinaFiltro();
		filtro.setEstatus(Integer.valueOf(1));	
		List<Oficina> oficinas = catalagoSiniestroOficinaService.buscar(Oficina.class, filtro, "nombreOficina");
		for(Oficina oficina : oficinas){
			values.put(oficina.getId(), oficina.getNombreOficina());		
		}
		return values;
	}
	
	@Override
	public Map<Long, String> obtenerOficinasSiniestrosSinFiltro() {
		Map<Long, String> values = new LinkedHashMap<Long, String>();
		List<Oficina> oficinas = catalagoSiniestroOficinaService.buscar(Oficina.class, new OficinaFiltro(), "nombreOficina");
		for(Oficina oficina : oficinas){
			values.put(oficina.getId(), oficina.getNombreOficina());		
		}
		return values;
	}
	
	@Override
	public Map<String, String> getMapColoniasPorCiudadMidas(String idciudad) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		CiudadMidas ciudad = entidadService.findById(CiudadMidas.class, idciudad);
		List<ColoniaMidas> coloniaList = entidadService.findByProperty(ColoniaMidas.class, "ciudad", ciudad);
		Collections.sort(coloniaList, new Comparator<ColoniaMidas>() {
			@Override
			public int compare(ColoniaMidas o1, ColoniaMidas o2) {
				return o1.getDescripcion().compareTo(o2.getDescripcion());
			}
		});
		for(ColoniaMidas item : coloniaList){
			map.put(item.getId(), item.getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<String, String> getMapMunicipiosPorEstadoMidas(String stateId) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		EstadoMidas estado = entidadService.findById(EstadoMidas.class, stateId);
		List<CiudadMidas> ciudadList = entidadService.findByProperty(CiudadMidas.class, "estado", estado);
		Collections.sort(ciudadList, new Comparator<CiudadMidas>() {

			@Override
			public int compare(CiudadMidas o1, CiudadMidas o2) {
				return o1.getDescripcion().compareTo(o2.getDescripcion());
			}
		});
		for(CiudadMidas item : ciudadList){
			map.put(item.getId(), item.getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<String, String> getMapEstadosPorPaisMidas(String idPais) {
		if(idPais==null)
			idPais = "PAMEXI";
		Map<String, String> map = new LinkedHashMap<String, String>();
		PaisMidas pais = entidadService.findById(PaisMidas.class, idPais);
		List<EstadoMidas> estadoList = entidadService.findByProperty(EstadoMidas.class, "pais", pais);
		Collections.sort(estadoList,new Comparator<EstadoMidas>() {
			@Override
			public int compare(EstadoMidas o1, EstadoMidas o2) {
				return o1.getDescripcion().compareTo(o2.getDescripcion());
			}
		});
		for(EstadoMidas item : estadoList){
			map.put(item.getId(), item.getDescripcion());
		}
		return map;
	}
	
	@Override
	public String getCodigoPostalPorColoniaMidas(String idColonia){
		ColoniaMidas colonia = entidadService.findById(ColoniaMidas.class, idColonia);
		return colonia.getCodigoPostal();
	}
	
	
	@Override
	public Map<String, String> getMapPaisesMidas() {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<PaisMidas> paisesList = entidadService.findAll(PaisMidas.class);
		Collections.sort(paisesList, new Comparator<PaisMidas>() {
			@Override
			public int compare(PaisMidas o1, PaisMidas o2) {
				return o1.getDescripcion().compareTo(o2.getDescripcion());
			}
		});
		for(PaisMidas item : paisesList){
			map.put(item.getId(), item.getDescripcion());
		}
		return map;
	}
	
	@Override
	public List<ColoniaMidas> getColoniasPorCPMidas(String cp) {		
		List<ColoniaMidas> coloniaList = entidadService.findByProperty(ColoniaMidas.class, "codigoPostal", cp);
		Collections.sort(coloniaList, new Comparator<ColoniaMidas>() {
			@Override
			public int compare(ColoniaMidas o1, ColoniaMidas o2) {
				return o1.getDescripcion().compareTo(o2.getDescripcion());
			}
		});
		
		return coloniaList;
	}
	
	@Override
	public Map<String, String> getMapColoniasPorCPMidas(String cp) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<ColoniaMidas> coloniaList = this.getColoniasPorCPMidas(cp);
		for(ColoniaMidas item : coloniaList){
			map.put(item.getId(), item.getDescripcion());
		}
		return map;
	}

	@Override
	public Map<String, String> getMapCiudadesPorCPMidas(String cp) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		CiudadMidas ciudadMidas = direccionMidasService.obtenerCiudadPorCP(cp);
		if (ciudadMidas != null){
			List<CiudadMidas> ciudadesMidas = entidadService.findByProperty(CiudadMidas.class, "estado", ciudadMidas.getEstado());
			Collections.sort(ciudadesMidas, new Comparator<CiudadMidas>() {
				@Override
				public int compare(CiudadMidas o1, CiudadMidas o2) {
					return o1.getDescripcion().compareTo(o2.getDescripcion());
				}
			});
			for(CiudadMidas item : ciudadesMidas){
				map.put(item.getId(), item.getDescripcion());
			}
			return map;
		}else{
			return null;
		}
	}
	
	@Override
	public String getEstadoPorIdColoniaMidas(String idColonia){
		ColoniaMidas colonia = entidadService.findById(ColoniaMidas.class, idColonia);
		CiudadMidas ciudad = direccionMidasService.obtenerCiudadPorColonia(colonia.getId());		
		//CiudadMidas ciudad = direccionMidasService.obtenerCiudadPorCP(colonia.getCodigoPostal());
		return ciudad == null ? null : ciudad.getEstado().getId();
	}
	
	@Override
	public String getEstadoPorCPMidas(String cp){
		CiudadMidas ciudad = direccionMidasService.obtenerCiudadPorCP(cp);
		return ciudad == null ? null : ciudad.getEstado().getId();
	}
	
	@Override
	public String getCiudadPorCPMidas(String cp){
		CiudadMidas ciudad = direccionMidasService.obtenerCiudadPorCP(cp);
		return ciudad == null ? null : ciudad.getId();
	}

	@Override		 
	public boolean getAplicaDescuentoNegocioPaqueteSeccion(Long idToNegPaqueteSeccion){
		boolean aplicaDescuento = false;
		if(idToNegPaqueteSeccion != null) {
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("negocioPaqueteSeccion.idToNegSeccion",idToNegPaqueteSeccion);
			try{
				NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao.findById(idToNegPaqueteSeccion);
				if(negocioPaqueteSeccion != null && negocioPaqueteSeccion.getAplicaPctDescuentoEdo() == 1) {
					aplicaDescuento = true;
				} 
			}catch(Exception e){
				LOG.error(e.getMessage(), e);
			}
		}
		return aplicaDescuento;
	}
	
	@Override
	public Double getPcteDescuentoDefaultPorEstado(BigDecimal idToCotizacion, String stateId) {
		CotizacionDTO cotizacionDTO = null;
		Double descuentoDefault=0.0;
		if(idToCotizacion != null && stateId != null){
			try {
				cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
				if(cotizacionDTO != null && cotizacionDTO.getSolicitudDTO() != null &&
						cotizacionDTO.getSolicitudDTO().getNegocio() !=null && 
						cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio() != null){
					NegocioEstadoDescuento negocioEstadoDescuento = negocioEstadoDescuentoDao.findByNegocioAndEstado(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio(), stateId);
					if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuentoDefault() != null){
						descuentoDefault = negocioEstadoDescuento.getPctDescuentoDefault();
					}
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return descuentoDefault;
	}
	
	@Override
	public Double getPcteDescuentoMaximoPorEstado(BigDecimal idToCotizacion, Long idToNegocio, String stateId) {
		CotizacionDTO cotizacionDTO = null;
		Double descuentoMaximo = 0.0;

		try {
			
			if(idToNegocio != null && stateId != null) {
				NegocioEstadoDescuento negocioEstadoDescuento = negocioEstadoDescuentoDao.findByNegocioAndEstado(
						idToNegocio, stateId);
				if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuento() != null) {
					descuentoMaximo = negocioEstadoDescuento.getPctDescuento();
				}
			}
			
			if(idToCotizacion != null){
				cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
				if(cotizacionDTO != null && cotizacionDTO.getSolicitudDTO() != null &&
						cotizacionDTO.getSolicitudDTO().getNegocio() !=null && 
						cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio() != null
						 && stateId != null){
					NegocioEstadoDescuento negocioEstadoDescuento = negocioEstadoDescuentoDao.findByNegocioAndEstado(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio(), stateId);
					if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuento() != null){
						descuentoMaximo = negocioEstadoDescuento.getPctDescuento();
					}
				}
				
				//Renovaciones
				if(cotizacionDTO.getSolicitudDTO() != null && cotizacionDTO.getSolicitudDTO().getEsRenovacion() != null &&
						cotizacionDTO.getSolicitudDTO().getEsRenovacion().intValue() == 1){
					Map<String,Object> params = new HashMap<String, Object>();
					params.put("idToCotizacion", idToCotizacion);
					params.put("claveEstatus", OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION);
					List<OrdenRenovacionMasivaDet> resultList = entidadService.findByProperties(OrdenRenovacionMasivaDet.class, params);
					if(resultList != null) {
						for(OrdenRenovacionMasivaDet element: resultList){
							//Aplica el descuento por estado + descuento por no siniestro
							if(element.getDescuentoNoSiniestro() != null) {
								descuentoMaximo = descuentoMaximo + element.getDescuentoNoSiniestro();
							}
							break;
						}
					}
				}
			}
			
			if(descuentoMaximo > 100) {
				descuentoMaximo = 100.0;
			}
		
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}

		return descuentoMaximo;
	}
	
	public Map<String, String> getMapTipoPrestador(){
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<TipoPrestadorServicio> resultList =entidadService.findAll(TipoPrestadorServicio.class);
		for(TipoPrestadorServicio element: resultList){
			map.put(element.getNombre(), element.getDescripcion());
		}
		return map;
	}
	
	public Map<Long, String> getMapBanco(){
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<TipoPrestadorServicio> resultList =entidadService.findAll(TipoPrestadorServicio.class);
		for(TipoPrestadorServicio element: resultList){
			map.put(element.getId(), element.getNombre());
		}
		return map;
	}
	
	public Map<Long, String> getMapCiaDeSeguros(){
		int ACTIVO = 1;
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		PrestadorServicioFiltro filtro = new PrestadorServicioFiltro ();
		filtro.setTipoPrestadorStr("CIA"); 
		filtro.setEstatus(ACTIVO);
		List<PrestadorServicioRegistro> list = prestadorDeServicioService.buscar(filtro);
		for(PrestadorServicioRegistro prestador : list){
			map.put(prestador.getId(), prestador.getNombrePersona().concat(" - ").concat(prestador.getId().toString()));
		}
		return map;
	}

	@Override
	public Double getPcteDescuentoDefaultPorNegocio(Long idToNegocio, String stateId) {
		Double descuentoDefault = 0.0;
		
		try {
			if ( idToNegocio != null && stateId != null ){
				NegocioEstadoDescuento negocioEstadoDescuento = negocioEstadoDescuentoDao.findByNegocioAndEstado(idToNegocio, stateId);
				if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuentoDefault() != null){
					descuentoDefault = negocioEstadoDescuento.getPctDescuentoDefault();
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return descuentoDefault;
	}
	
	@Override
	public Map<String, String> getMapMovimientosNotificaciones(String idProceso){
		if(idProceso != null){
			return confNotificacionesService.obtenerMovimientos(Long.parseLong(idProceso));
		}else{
			return null;
		}
	}
	
	
	@Override
	public Map<String, String> obtenerCatalogoValorFijoByStr(String tipo,
			String codigoPadreId) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (TIPO_CATALOGO item : TIPO_CATALOGO.values()) {        	
            if (item.name().equals(tipo)) {
            	map =this.obtenerCatalogoValorFijo(item,codigoPadreId, Boolean.TRUE);
            	break;
            }
        }
		return map;
	}

	@Override
	public Map<String, String> obtenerTerminosAjuste(
			String codigoTipoSiniestro, String codigoResponsabilidad) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<CatValorFijo> terminos = siniestroCabinaService.obtenerTerminosAjuste(codigoTipoSiniestro, codigoResponsabilidad);
		for(CatValorFijo valor : terminos){
			map.put(valor.getCodigo(), valor.getDescripcion());
		}
		return map;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<String, String> obtenerAjustadoresValuacionCrucero() {
		Map<String, String> ajustadoresValuacion = new LinkedHashMap<String, String>();
		
		String queryString = "SELECT DISTINCT model.ajustador FROM ValuacionReporte AS model ";
		Query query = entityManager.createQuery(queryString);
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		List<ServicioSiniestro> ajustadores = new ArrayList<ServicioSiniestro>();
		ajustadores = (List<ServicioSiniestro>)query.getResultList();
		
		if(ajustadores!=null && !ajustadores.isEmpty()){
			for(ServicioSiniestro ajustador: ajustadores){
				if (ajustador != null) {
					ajustadoresValuacion.put(ajustador.getId().toString(), 
							ajustador.getPersonaMidas() != null ? ajustador.getPersonaMidas().getNombre() : null);
				}
				
			}
		}
		
		return ajustadoresValuacion;
	}
 
	@Override
	public Map<String, String> obtenerPiezasPorSeccionAutomovil(String codigoSeccion) {
		Map<String, String> piezasPorSeccion = new LinkedHashMap<String, String>();
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("seccionAutomovil", codigoSeccion);
		params.put("estatus"         , 1); // # 1- ACTIVO 0- NO ACTIVO
		
		List<PiezaValuacion> piezas = entidadService.findByProperties(PiezaValuacion.class, params);
		if(piezas != null && !piezas.isEmpty()){
			for(PiezaValuacion pieza: piezas){
				piezasPorSeccion.put(pieza.getId().toString(), pieza.getDescripcion());
			}
		}
		
		return piezasPorSeccion;
	}

	@Override
	public Map<String, String> obtenerValuadores() {
		Map<String, String> valuadores = new LinkedHashMap<String, String>();
		List<Usuario> usuarios = usuarioService.buscarUsuariosPorNombreRol(ValuacionReporteService.NOMBRE_ROL_VALUADORES, ValuacionReporteService.NOMBRE_ROL_M2_VALUADOR);
		for(Usuario usuario: CollectionUtils.emptyIfNull(usuarios)){
			valuadores.put(usuario.getNombreUsuario(), usuario.getNombreCompleto());
		}
		
		return valuadores;
	}	

	@Override
	public Map<String, String> obtenerCoberturaPorSeccion( Long idToSeccion ){
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<CoberturaSeccionSiniestroDTO> coberturas =autorizacionReservaService.obtenerCoberturasPorSeccion( idToSeccion );
		 
		 for( CoberturaSeccionSiniestroDTO valor : coberturas ){
				map.put(valor.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura()+"-"+valor.getClaveSubCalculo()+"-"+valor.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible(), valor.getNombreCobertura());
			}
			return map;
		
	}
	

	@Override
	public Map<String, String> getMapAjustadoresPorOficinaId(Long oficinaId) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		Oficina oficina = entidadService.findById(Oficina.class, oficinaId);

		Map<String, Object> params = new HashMap<String, Object>();
		params.put(ServicioSiniestro_.oficina.getName(), oficina);
		params.put(ServicioSiniestro_.servicioSiniestroId.getName(), 1);
		params.put(ServicioSiniestro_.estatus.getName(),
				Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());

		List<ServicioSiniestro> ajustadores = entidadService.findByPropertiesWithOrder(
				ServicioSiniestro.class, params, "personaMidas.nombre");				

		for (ServicioSiniestro ajustador : ajustadores) {
			map.put("0" + ajustador.getId().toString(), ajustador.getNombrePersona()); //El 0 es para parche para ordenamiento DWR
		}
		return map;
	}
	
	@Override
	public Map<Long, String> getMapAjustadoresPorOficina(Oficina oficina,
			Boolean soloAtivos) {
		Map<Long, String> map = new HashMap<Long, String>(1);

		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put(ServicioSiniestro_.oficina.getName(), oficina);

		if (soloAtivos) {
			params.put(ServicioSiniestro_.estatus.getName(),
					Negocio.EstatusNegocio.ACTIVO.obtenerEstatus());
		}

		List<ServicioSiniestro> ajustadores = entidadService.findByProperties(
				ServicioSiniestro.class, params);

		for (ServicioSiniestro ajustador : ajustadores) {
			map.put(ajustador.getId(), ajustador.getNombrePersona());
		}
		return map;
	}

	@Override
	public Map<Long, String> getMapHorariosLaborales(Boolean soloActivos) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		if (HorarioLaboral.ctgHorarios.size() == 0)
			HorarioLaboral.ctgHorarios = obtenerCatalogoValorFijo(HorarioLaboral.ctgHorariosTipo);
		List<HorarioLaboral> horarios;
		if (soloActivos) {
			Map<String, Object> params = new HashMap<String, Object>();
			params.put(HorarioLaboral_.estatus.getName(),
					HorarioLaboral.Estatus.ACTIVO.getObtenerEstatus());
			horarios = entidadService.findByPropertiesWithOrder(
					HorarioLaboral.class, params,
					HorarioLaboral_.horaEntradaCode.getName(),
					HorarioLaboral_.horaSalidaCode.getName());
		} else {
			horarios = entidadService.findAll(HorarioLaboral.class);
		}
		for (HorarioLaboral horario : horarios) {
			map.put(horario.getId(), horario.getDescripcion());
		}
		return map;
	}
	
	@Override
	public Map<String, String> obtenerTiposPaseAtencion() {
		return obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PASE_ATENCION);
	}
	
	
	@Override
	public Map<Long, String> listarConceptosPorCobertura(
			Long idCoberturaCabina, String cveSubTipoCalculoCobertura,
			short categoria, String tipoConcepto, boolean esValidoHGS) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		Map<String,Object> params = new HashMap<String, Object>();
		if ( categoria == ConceptoAjusteService.GASTO_AJUSTE){
			//Activo 
			params.put(UtileriasWeb.KEY_ESTATUS, OrdenCompraService.ESTATUSACTIVO);
			params.put(UtileriasWeb.KEY_CATEGORIA, categoria);
			map.putAll(getMapConceptosAjuste(params));
		}else if (categoria ==ConceptoAjusteService.AFECTACION_RESERVA && null!=idCoberturaCabina && idCoberturaCabina!=0){
			CoberturaReporteCabina coberturaR= entidadService.findById(CoberturaReporteCabina.class,idCoberturaCabina);
				if (null!= coberturaR.getCoberturaDTO() && null!=coberturaR.getIncisoReporteCabina() && null!=coberturaR.getIncisoReporteCabina().getSeccionReporteCabina() && null!= coberturaR.getIncisoReporteCabina().getSeccionReporteCabina().getSeccionDTO()  ){
					SeccionDTO seccionDto = coberturaR.getIncisoReporteCabina().getSeccionReporteCabina().getSeccionDTO();
					CoberturaDTO coberturaDto = coberturaR.getCoberturaDTO() ;
					StringBuilder queryString = new StringBuilder ("SELECT model FROM ConceptoCobertura AS model ");
					queryString.append("where model.coberturaSeccion.coberturaDTO.idToCobertura = :coberturaId ");
					queryString.append(" and model.coberturaSeccion.seccionDTO.idToSeccion = :idToSeccion ");
					queryString.append(" and model.conceptoAjuste.estatus = :estatus");
					queryString.append(" and model.conceptoAjuste.categoria = :categoria");
					
					
					if(!StringUtil.isEmpty(tipoConcepto)){
						queryString.append(" and model.conceptoAjuste.tipoConcepto in ( :tipoConcepto ) ");
					}
					
					
					if(  !StringUtil.isEmpty(cveSubTipoCalculoCobertura) && !cveSubTipoCalculoCobertura.equalsIgnoreCase(OrdenCompraService.CVENULA) ){
						queryString.append(" and model.claveSubcobertura = :claveSubcobertura");
						
						
					}
					if(esValidoHGS){
						queryString.append(" and model.conceptoAjuste.esValidoHGS = :esValidoHGS ");
					}
					
					Query query = entityManager.createQuery(queryString.toString());
									
					query.setParameter("coberturaId", coberturaDto.getIdToCobertura() );
					query.setParameter("idToSeccion", seccionDto.getIdToSeccion() );
					query.setParameter("estatus", OrdenCompraService.ESTATUSACTIVO );
					query.setParameter("categoria", categoria );
					
					if(!StringUtil.isEmpty(tipoConcepto)){
						query.setParameter("tipoConcepto", tipoConcepto );
					}
					
					
					if(  !StringUtil.isEmpty(cveSubTipoCalculoCobertura) && !cveSubTipoCalculoCobertura.equalsIgnoreCase(OrdenCompraService.CVENULA) ){
						query.setParameter("claveSubcobertura", cveSubTipoCalculoCobertura );
					}
					if(esValidoHGS){
						query.setParameter("esValidoHGS", Boolean.TRUE );
						
					}
					
					List<ConceptoCobertura>  conceptoCoberturaList = (List<ConceptoCobertura>)query.getResultList();
					
					for(ConceptoCobertura concepto : conceptoCoberturaList){
						if (null!=concepto.getConceptoAjuste()){
							map.put(concepto.getConceptoAjuste().getId(), concepto.getConceptoAjuste().getNombre());
						}
					}
				}
			
			
		}
		return map;
	}


	@Override //
	public Map<Long, String> listarConceptosPorCobertura(Long idCoberturaCabina,String cveSubTipoCalculoCobertura, short categoria,String tipoConcepto) {
		
		return this.listarConceptosPorCobertura(idCoberturaCabina, cveSubTipoCalculoCobertura, categoria, tipoConcepto, false);
	}


	
	
	@Override
	public Map<String, String> getCoberturasOrdenCompra(
			Long idReporteCabina, String tipo) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		/*Se valida si el reporte cabina no tiene relacionado una poliza retorna las coberturas en vacio*/
		if(null !=idReporteCabina){
			ReporteCabina reporte=this.entidadService.findById(ReporteCabina.class, idReporteCabina);
			if(null!=reporte){
				if(null==reporte.getPoliza()){
					return map;
				}
			}
		}
		
		boolean soloCoberturasAfectadas=false;
		if(OrdenCompraService.TIPO_AFECTACION_RESERVA.equalsIgnoreCase(tipo)){
			soloCoberturasAfectadas=true;
		}

		return getCoberturasReporte(idReporteCabina, null, null,null, soloCoberturasAfectadas, false);
		
	}
	
	public  Map<String, String> getCoberturasReporte(Long reporteCabinaId, String tipoSiniestro, 
			String terminoAjuste,String tipoResponsabilidad, Boolean soloAfectadas, Boolean soloAfectables) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		
		List<AfectacionCoberturaSiniestroDTO> coberturasAfectacion= 
			estimacionCoberturaSiniestroService.obtenerCoberturasAfectacion(reporteCabinaId, tipoSiniestro, terminoAjuste, 
					tipoResponsabilidad, soloAfectadas, soloAfectables);

		for(AfectacionCoberturaSiniestroDTO cobertura : coberturasAfectacion){
			
			map.put(cobertura.getCoberturaCompuesta(), cobertura.getNombreCobertura());
		}
		
		return map;

	}
	
	@Override
	public Map<Long, String> getMapPrestadorPorTipo(String tipo) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		PrestadorServicioFiltro filtro = new PrestadorServicioFiltro ();
		filtro.setTipoPrestadorStr(tipo); 
		filtro.setEstatus(OrdenCompraService.PRESTADOR_ACTIVO);
		List<PrestadorServicioRegistro> list = prestadorDeServicioService.buscar(filtro);
		for(PrestadorServicioRegistro prestador : list){
			if(prestador.getOficina()!=null && !StringUtil.isEmpty(prestador.getOficina().getClaveOficina()) ){
				map.put( new Long(prestador.getId()),prestador.getId()+" - "+ prestador.getNombrePersona()+ " - "+prestador.getOficina().getClaveOficina() );
			}else{
				map.put( new Long(prestador.getId()),prestador.getId()+" - "+ prestador.getNombrePersona());
			}
		}
		return map;
	}

	@Override
	public Map<Long, String> getListasTercerosAfectadorPorCobertura(
			Long coberturaReporteCabinaId,String cveSubTipoCalculo) {	
		Map<Long, String> map = new HashMap<Long, String>();
		Map<String,Object> params = new HashMap<String, Object>();
		String tipoEstimacion = null;
		String nombreAsegurado="";
		CoberturaReporteCabina coberturaReporteCabina = entidadService.findById(CoberturaReporteCabina.class, coberturaReporteCabinaId);
		if(null!=coberturaReporteCabina.getCoberturaDTO()){
			/*if(coberturaReporteCabina.getCoberturaDTO().getClaveTipoCalculo().equalsIgnoreCase(OrdenCompraService.TERCERO_RESPCIVIL_VEHICULO) ||
					coberturaReporteCabina.getCoberturaDTO().getClaveTipoCalculo().equalsIgnoreCase(OrdenCompraService.TERCERO_RESPCIVIL) ||
					coberturaReporteCabina.getCoberturaDTO().getClaveTipoCalculo().equalsIgnoreCase(OrdenCompraService.TERCERO_GASTOSMEDICO)||
					coberturaReporteCabina.getCoberturaDTO().getClaveTipoCalculo().equalsIgnoreCase(OrdenCompraService.COBERTURA_DANOSMATERIALES)||
					coberturaReporteCabina.getCoberturaDTO().getClaveTipoCalculo().equalsIgnoreCase(OrdenCompraService.TERCERO_ACCIDENTEAUTOMOVILISTICOCONDUCTOR)					
				){*/
				StringBuilder queryWhere = new StringBuilder();
				params.put("cveTipoCalculoCobertura", coberturaReporteCabina.getCoberturaDTO().getClaveTipoCalculo());
				if(null!=cveSubTipoCalculo &&  !StringUtil.isEmpty(cveSubTipoCalculo) && !cveSubTipoCalculo.equalsIgnoreCase(OrdenCompraService.CVENULA)){
					params.put("cveSubTipoCalculoCobertura", cveSubTipoCalculo);
				}else{
					queryWhere.append(" and model.cveSubTipoCalculoCobertura  IS NULL "); 
				}
				List<ConfiguracionCalculoCoberturaSiniestro> list =this.entidadService.findByColumnsAndProperties(ConfiguracionCalculoCoberturaSiniestro.class, "id,tipoEstimacion", params, null, queryWhere, null);
				if(null!=list && !list.isEmpty()){
					ConfiguracionCalculoCoberturaSiniestro conf=	list.get(0);
					tipoEstimacion = conf.getTipoEstimacion();
					if(EnumUtil.equalsValue(tipoEstimacion, TipoEstimacion.DIRECTA) && null!=coberturaReporteCabina.getIncisoReporteCabina() && null!=coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina() &&
							null!=coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina() && null!=coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza() ){
						nombreAsegurado= (!StringUtil.isEmpty(coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza().getNombreAsegurado())) ? coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza().getNombreAsegurado() : "";
					}
				}else {
					return map;
				}
				params.clear();
				params.put("coberturaReporteCabina.id",coberturaReporteCabinaId);
				if ( !StringUtil.isEmpty(tipoEstimacion)){
					params.put("tipoEstimacion",tipoEstimacion);
				}
				List<EstimacionCoberturaReporteCabina> estimaciones= entidadService.findByProperties(EstimacionCoberturaReporteCabina.class,params);
				for(EstimacionCoberturaReporteCabina estimacion : estimaciones){
						map.put( new Long(estimacion.getId()),  ((!StringUtil.isEmpty(estimacion.getFolio())) ? estimacion.getFolio() : "") +"	"+  ((!StringUtil.isEmpty( estimacion.getNombreAfectado())) ? estimacion.getNombreAfectado() : nombreAsegurado));
				}
			//}
		}
		return map;
	}
	
	
	@Override
	public Map<Long,String> obtenerMapSecciones(){
		Map<Long,String> resultado 	= new LinkedHashMap<Long,String>();
		List<SeccionDTO> lSecciones = this.getListarSeccionesVigentesAutosUsables();

		for( SeccionDTO lSeccion : lSecciones ){
			resultado.put( new Long( lSeccion.getIdToSeccion().toString() ), lSeccion.getDescripcion() );
		}
		
		return resultado;
	}
	
	@Override
	public Map<Long, String> listarConceptosPorCoberturaSeccion(Long idCoberturaSeccion, Long idSeccion , String cveSubTipoCalculoCobertura, short tipoConcepto) {
		Map<Long, String> map 					= new LinkedHashMap<Long, String>();
		Map<String,Object> params 				= new HashMap<String, Object>(1);
		List<CoberturaSeccionDTO> listCober 	= null;
		CoberturaSeccionDTO coberturaSeccion 	= null;
		SeccionDTO seccionDto 					= null;
		CoberturaDTO coberturaDto 				= null;
		
		params.put("id.idtocobertura", idCoberturaSeccion);
		params.put("id.idtoseccion", idSeccion);
		
		listCober = entidadService.findByProperties(CoberturaSeccionDTO.class, params);
		params.clear();
		if(listCober != null && listCober.size()>0){
			coberturaSeccion = listCober.get(0);
			
			if(coberturaSeccion.getCoberturaDTO() != null && coberturaSeccion.getSeccionDTO() != null){
				seccionDto = coberturaSeccion.getSeccionDTO();
				coberturaDto = coberturaSeccion.getCoberturaDTO();
				
				params.put("coberturaSeccion.coberturaDTO.idToCobertura", coberturaDto.getIdToCobertura());
				params.put("coberturaSeccion.seccionDTO.idToSeccion", seccionDto.getIdToSeccion());
				params.put("conceptoAjuste.estatus",OrdenCompraService.ESTATUSACTIVO);
				params.put("conceptoAjuste.categoria",tipoConcepto);
				if(  !StringUtil.isEmpty(cveSubTipoCalculoCobertura) && !cveSubTipoCalculoCobertura.equalsIgnoreCase(OrdenCompraService.CVENULA) ){
					params.put("claveSubcobertura",cveSubTipoCalculoCobertura);
				}
				List<ConceptoCobertura>  conceptoCoberturaList = entidadService.findByProperties(ConceptoCobertura.class, params);
				if(CollectionUtils.isNotEmpty(conceptoCoberturaList)){
					Collections.sort(conceptoCoberturaList, new Comparator<ConceptoCobertura>() {
						@Override
						public int compare(ConceptoCobertura object1, ConceptoCobertura object2) {
							return object1.getConceptoAjuste().getNombre().compareTo(object2.getConceptoAjuste().getNombre());
						}
					});
				}
				
				
				
				for(ConceptoCobertura concepto : conceptoCoberturaList){
					if (null!=concepto.getConceptoAjuste()){
						map.put(concepto.getConceptoAjuste().getId(), concepto.getConceptoAjuste().getNombre());
					}
				}
			}
			
		}
		
		return map;
	}
	
	@Override
	public Map<Long, String> obtenerMapConceptosGastoAjuste(){
		Map<String,Object> params 				= new HashMap<String, Object>();
		//Activo 
		params.put(UtileriasWeb.KEY_ESTATUS, OrdenCompraService.ESTATUSACTIVO);
		params.put(UtileriasWeb.KEY_CATEGORIA, ConceptoAjusteService.GASTO_AJUSTE);
		return getMapConceptosAjuste(params);
	}
	
	@Override
	public Map<Long, String> getMapConceptosAjuste() {
		Map<String,Object> params = new HashMap<String, Object>(1);
		//Activo 
		params.put(UtileriasWeb.KEY_ESTATUS, OrdenCompraService.ESTATUSACTIVO);			
		
		return getMapConceptosAjuste(params);		
	}
	
	private Map<Long, String> getMapConceptosAjuste(Map<String,Object> params){
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<ConceptoAjuste>  conceptoAjusteList = entidadService.findByProperties(ConceptoAjuste.class, params);
		if(CollectionUtils.isNotEmpty(conceptoAjusteList)){
			Collections.sort(conceptoAjusteList, new Comparator<ConceptoAjuste>() {
				@Override
				public int compare(ConceptoAjuste object1, ConceptoAjuste object2) {
					return object1.getNombre().compareTo(object2.getNombre());
				}
			});
		}
		for(ConceptoAjuste concepto : conceptoAjusteList){
			map.put(concepto.getId(),  concepto.getNombre());
		}
		return map;
	}

	@Override
	public Map<String, String> obtenerCoberturaPorSeccionConDescripcion( Long idToSeccion ){
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<CoberturaSeccionSiniestroDTO> coberturas =autorizacionReservaService.obtenerCoberturasPorSeccion( idToSeccion );

		for( CoberturaSeccionSiniestroDTO valor : coberturas ){
			map.put(valor.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura()+"-"+valor.getClaveSubCalculo()
					+"-"+valor.getNombreCobertura(), 
					valor.getNombreCobertura());
		}
		return map;

	}

	@Override
	public Map<Long, String> listarConceptosPorCoberturaOrdenCompra(
			Long idCoberturaCabina, String cveSubTipoCalculoCobertura,
			short categoria, String tipoConcepto, boolean aplicaPagoDaños,  Long idTipoPrestador  ) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		Map<String,Object> params = new HashMap<String, Object>(1);
		if ( categoria == ConceptoAjusteService.GASTO_AJUSTE){
			if (null!=idTipoPrestador){
				//Activo 
				params.put("conceptoAjuste.estatus", OrdenCompraService.ESTATUSACTIVO);
				//Activo 
				params.put("conceptoAjuste.categoria", ConceptoAjusteService.GASTO_AJUSTE);
				params.put("tipoPrestadorServicio.id", idTipoPrestador);
				List<ConceptoTipoPrestador>  conceptoCoberturaList = entidadService.findByProperties(ConceptoTipoPrestador.class, params);
				for(ConceptoTipoPrestador concepto : conceptoCoberturaList){
					
					map.put(concepto.getConceptoAjuste().getId(), concepto.getConceptoAjuste().getNombre());
				} 
			}/*else {
				params.clear();
				params.put("estatus", OrdenCompraService.ESTATUSACTIVO);//Activo 
				params.put("categoria", ConceptoAjusteService.GASTO_AJUSTE);//Activo 

				if(!StringUtil.isEmpty(tipoConcepto)){
					params.put("tipoConcepto",tipoConcepto);
				}
				List<ConceptoAjuste>  conceptoList = entidadService.findByProperties(ConceptoAjuste.class, params);
				for(ConceptoAjuste concepto : conceptoList){
						map.put(concepto.getId(), concepto.getNombre());
				}
			}*/
			
			
			
		}else if ( categoria == ConceptoAjusteService.REEMBOLSO_GASTO_AJUSTE){
			params.clear();
			//Activo 
			params.put(UtileriasWeb.KEY_ESTATUS, OrdenCompraService.ESTATUSACTIVO);
			params.put(UtileriasWeb.KEY_CATEGORIA, ConceptoAjusteService.REEMBOLSO_GASTO_AJUSTE); 
			if(!StringUtil.isEmpty(tipoConcepto)){
				params.put("tipoConcepto",tipoConcepto);
			}
			map.putAll(getMapConceptosAjuste(params));
			
		}else if (categoria ==ConceptoAjusteService.AFECTACION_RESERVA && null!=idCoberturaCabina && idCoberturaCabina!=0){
			CoberturaReporteCabina coberturaR= entidadService.findById(CoberturaReporteCabina.class,idCoberturaCabina);
				if (null!= coberturaR.getCoberturaDTO() && null!=coberturaR.getIncisoReporteCabina() && null!=coberturaR.getIncisoReporteCabina().getSeccionReporteCabina() && null!= coberturaR.getIncisoReporteCabina().getSeccionReporteCabina().getSeccionDTO()  ){
					SeccionDTO seccionDto = coberturaR.getIncisoReporteCabina().getSeccionReporteCabina().getSeccionDTO();
					CoberturaDTO coberturaDto = coberturaR.getCoberturaDTO() ;
					params.put("coberturaSeccion.coberturaDTO.idToCobertura", coberturaDto.getIdToCobertura());
					params.put("coberturaSeccion.seccionDTO.idToSeccion", seccionDto.getIdToSeccion());
					params.put("conceptoAjuste.estatus",OrdenCompraService.ESTATUSACTIVO);
					params.put("conceptoAjuste.categoria",categoria);
					if(!StringUtil.isEmpty(tipoConcepto)){
						params.put("conceptoAjuste.tipoConcepto",tipoConcepto);
					}
					
					if(  !StringUtil.isEmpty(cveSubTipoCalculoCobertura) && !cveSubTipoCalculoCobertura.equalsIgnoreCase(OrdenCompraService.CVENULA) ){
						params.put("claveSubcobertura",cveSubTipoCalculoCobertura);
					}
					StringBuilder queryWhere = new StringBuilder();
					queryWhere.append(" and model.conceptoAjuste.tipoConcepto not in ( '"+OrdenCompraService.TIPO_INDEMNIZACION_PERDIDATOTAL+"', '"+OrdenCompraService.TIPO_INDEMNIZACION_ROBO+"') "); 
					List<ConceptoCobertura>  conceptoCoberturaList = entidadService.findByColumnsAndProperties(ConceptoCobertura.class, "id,conceptoAjuste", params, new HashMap<String,Object>(1), queryWhere, null);
					
					//List<ConceptoCobertura>  conceptoCoberturaList = entidadService.findByProperties(ConceptoCobertura.class, params);
					for(ConceptoCobertura concepto : conceptoCoberturaList){
						if (null!=concepto.getConceptoAjuste()){
							map.put(concepto.getConceptoAjuste().getId(), concepto.getConceptoAjuste().getNombre());
						}
					}
				}
		} 
		return map;
	}
	

	@Override
	public Map<BigDecimal, String> getMapVigencias() {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<VigenciaDTO> vigencias = entidadService.findAll(VigenciaDTO.class);
		for(VigenciaDTO vigencia: vigencias){
			map.put(vigencia.getIdTcVigencia(), vigencia.getDescripcion());
		}
		return map;
	}
	
	
	@Override
	public Map<BigDecimal, String> getMapProgramaPagos(BigDecimal idToPoliza, BigDecimal numeroEndoso){
		return impresionRecibosService.getMapProgramaPagos(idToPoliza, numeroEndoso);
	}
	
	@Override
	public String getIncisosProgPago(BigDecimal idToPoliza, BigDecimal idProgPago) {
		String incisos = "";
		if(idToPoliza != null && idProgPago != null){
			try {
				Map<BigDecimal, String> incisosList = impresionRecibosService.getMapIncisosEndoso(idToPoliza, idProgPago);
				if (incisosList != null && incisosList.values() != null){
					incisos = StringUtils.join(incisosList.values().iterator(), ',');
				}
			} catch (Exception e) {	
				LOG.error(e.getMessage(), e);
			}			
		}				
		return incisos;
	}

	@Override
	public Map<Long, String> getMapOficinaJuridico(){
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<OficinaJuridico>  listaOficinaJuridico  =this.entidadService.findByProperty(OficinaJuridico.class, UtileriasWeb.KEY_ESTATUS,""+ OrdenCompraService.ESTATUSACTIVO);
		for (OficinaJuridico oficina : listaOficinaJuridico){
			map.put(oficina.getId(), oficina.getNombreOficina());
		}
		return map;
	}
	
	@Override
	public Map<String, String> obtenerUsuariosJuridico()
	{
		 Map<String, String> usuariosMap = new LinkedHashMap<String, String>();
		Usuario usuarioActual = usuarioService.getUsuarioActual();
		List<Usuario> usuarios = 
			usuarioService.buscarUsuariosSinRolesPorNombreRol(
					usuarioActual.getNombreUsuario(), 
					usuarioActual.getIdSesionUsuario(), 
					sistemaContext.getRolAnalistaJuridico(),
					sistemaContext.getRolDirectorJuridico());
		for (Usuario usuario : usuarios){
			usuariosMap.put(usuario.getNombreUsuario(), usuario.getNombreCompleto());
		}
		
		return usuariosMap;		
	}
	
	/**
	 * Obtiene el listado de delegaciones en juridico activas, odernadas por numero. 
	 * Utilizar el metodo de catalogoJuridicoService.obtener filtrado de acuerdo a lo que se desea retornar.
	 */
	@Override
	public Map<Long, String> obtenerJuridicoDelegaciones() {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		CatalogoJuridico filtro=new CatalogoJuridico();
		filtro.setEstatus(null);
		filtro.setFechaCreacion(null);
		filtro.setFechaModificacion(null);
		filtro.setEstatus(true);
		List<CatalogoJuridicoDTO> lista=this.catalogoJuridicoService.obtenerDelegaciones(filtro);
		for (CatalogoJuridicoDTO dto :lista){
			map.put(dto.getId(), dto.getNombre());
			
		}
		return map;
	}
	/**Obtener el listado de motivos juridico activos ordenado por numero. 
	Utilizar el metodo de catalogoJuridicoService.obtener filtrado de acuerdo a lo que se desea retornar.
	*/ 
	@Override
	public Map<Long, String> obtenerJuridicoMotivos() {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		CatalogoJuridico filtro=new CatalogoJuridico();
		filtro.setEstatus(null);
		filtro.setFechaCreacion(null);
		filtro.setFechaModificacion(null);
		filtro.setEstatus(true);
		List<CatalogoJuridicoDTO> lista=this.catalogoJuridicoService.obtenerMotivosReclamacion(filtro);
		for (CatalogoJuridicoDTO dto :lista){
			map.put(dto.getId(), dto.getNombre());
			
		}
		return map;
	}
	/**
	 * Devuelve el listado de procedimientos juridicos activos ordenados por número. 
	 * Utilizar el metodo de catalogoJuridicoService.obtener filtrado de acuerdo a lo que se desea retornar.
	 * */
	
	@Override
	public Map<Long, String> obtenerJuridicoProcedimientos() {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		CatalogoJuridico filtro=new CatalogoJuridico();
		filtro.setEstatus(null);
		filtro.setFechaCreacion(null);
		filtro.setFechaModificacion(null);
		filtro.setEstatus(true);
		List<CatalogoJuridicoDTO> lista=this.catalogoJuridicoService.obtenerProcedimientos(filtro);
		for (CatalogoJuridicoDTO dto :lista){
			map.put(dto.getId(), dto.getNombre());
			
		}
		
		return map;
	}
	
	@Override
	public Map<Long, String> obtenerTipoPrestadorGastoAjuste() {
		Map<String, String> mapCatalogo = new LinkedHashMap<String, String>();
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		mapCatalogo=this.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PRESTADOR_GASTOAJUSTE);
		if(!mapCatalogo.isEmpty()){
			for(Map.Entry<String,String> mapa : mapCatalogo.entrySet()){
				List<TipoPrestadorServicio> lstTipos =	entidadService.findByProperty(TipoPrestadorServicio.class, "nombre", mapa.getKey());
				for(TipoPrestadorServicio tipo  : lstTipos){
					map.put(tipo.getId(), tipo.getDescripcion());
				} 
			}
			
			
			
		}
		return map;
	}
	
	@Override
	public Map<String, String> getMapTipoPrestadorAfectacionReserva() {
		Map<String, String> mapGastoAjuste = this.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_PRESTADOR_GASTOAJUSTE);
		Map<String, String> map = new LinkedHashMap<String, String>();
		List<TipoPrestadorServicio> resultList =entidadService.findAll(TipoPrestadorServicio.class);
		for(TipoPrestadorServicio element: resultList){
			if (!mapGastoAjuste.containsKey(element.getNombre())){
				map.put(element.getNombre(), element.getDescripcion());
			}
		}
			
		return map;
	}

	@Override
	public Map<Long, String> getMapEjecutivosPorGerencia(Long idGerencia, Long idProvision) {
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>(1);
		StringBuilder queryString = new StringBuilder("");
		queryString.append(" select distinct ejec.id as id,per.nombre as valor");
		queryString.append(" from  midas.toejecutivo ejec  ");
		queryString.append(" inner join midas.topromotoria prom on ejec.id=prom.ejecutivo_id ");
		queryString.append(" inner join seycos.persona per on per.id_persona=ejec.idPersona ");
		queryString.append(" where prom.id in( ");
		queryString.append(" select distinct rec.idPromotoria  ");
		queryString.append(" from midas.tocalculoprovision  prov ");
		queryString.append(" inner join midas.todetallecalculoprovision det on det.idcalculo = prov.id ");
		queryString.append(" inner join midas.torecibocalculoprovision rec on rec.iddetallecalculobonos = det.id ");
		queryString.append(" where prov.idprovision = "+idProvision+" )  ");
		queryString.append(" and ejec.gerencia_id ="+idGerencia);
		queryString.append(" order by per.nombre asc ");
		
		lista = this.entityManager.createNativeQuery(queryString.toString(),GenericaAgentesView.class).getResultList();
		
		Map<Long,String> mapa = new HashMap<Long, String>();
		for(GenericaAgentesView obj:lista){
			mapa.put(obj.getId(), obj.getValor());
		}
		
		return mapa;
	}

	@Override
	public Map<Long, String> getMapLineaNegocioPorProducto(Long idProducto, Long idProvision) {
		StringBuilder queryString = new StringBuilder("");
		queryString.append(" select  distinct sec.idtoseccion as id, ");
		queryString.append(" 'LN'||TRIM(SEC.CODIGOSECCION)||' - '||sec.NOMBRECOMERCIALSECCION||' - (P'||max(trim(p.CODIGOPRODUCTO))||', R'||max(trim(R.CODIGORAMO))||')' as valor, ");  
		queryString.append(" sec.NOMBRECOMERCIALSECCION ");
		queryString.append(" from MIDAS.TOSECCION sec ");
		queryString.append(" inner join MIDAS.TOTIPOPOLIZA tp on(tp.IDTOTIPOPOLIZA=sec.IDTOTIPOPOLIZA) ");  
		queryString.append(" inner join MIDAS.TOPRODUCTO p on(P.IDTOPRODUCTO =TP.IDTOPRODUCTO) ");
		queryString.append(" inner join MIDAS.TRRAMOTIPOPOLIZA rtp on (RTP.IDTOTIPOPOLIZA = TP.IDTOTIPOPOLIZA ) ");  
		queryString.append(" inner join MIDAS.TRRAMOSECCION rs on (RS.IDTOSECCION = SEC.IDTOSECCION  ) ");
		queryString.append(" inner join MIDAS.TCRAMO r on (R.IDTCRAMO = RS.IDTCRAMO and RTP.IDTCRAMO = R.IDTCRAMO ) ");  
		queryString.append(" where  sec.claveActivo=1 ");
		queryString.append(" and sec.idtoseccion in(select distinct rec.idlineanegocio from "); 
		queryString.append(" midas.toProvisionesBonoAgente prov ");
		queryString.append(" inner join midas.tocalculoprovision calc on (prov.id=calc.idProvision) ");  
		queryString.append(" inner join midas.todetallecalculoprovision det on(calc.id = det.idCalculo) ");  
		queryString.append(" inner join midas.torecibocalculoprovision rec on(det.id=rec.iddetallecalculobonos) ");
		queryString.append(" where prov.ID="+idProvision+" and IDLINEANEGOCIO is not null) ");
		queryString.append(" and p.IDTOPRODUCTO = "+idProducto);
		queryString.append(" group by sec.idtoseccion, SEC.CODIGOSECCION, sec.NOMBRECOMERCIALSECCION ");  
		queryString.append(" order by  sec.NOMBRECOMERCIALSECCION ");
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>(1);
		lista = this.entityManager.createNativeQuery(queryString.toString(), GenericaAgentesView.class).getResultList();
		Map<Long,String> mapa = new HashMap<Long, String>();
		for(GenericaAgentesView  obj:lista){
			mapa.put(obj.getId(), obj.getValor());
		}
		return mapa;
	}

	@Override
	public Map<Long, String> getMapProductoPorLineaVenta(Long idLineaVenta, Long idProvision) {
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select idToProducto AS id,'P'||TRIM(CODIGOPRODUCTO)||' - '||descripcionProducto AS valor "); 
		queryString.append(" from MIDAS.toproducto "); 
		queryString.append(" where CLAVEACTIVO = 1 "); 
		queryString.append(" and claveNegocio in( select clave from midas.toValorCatalogoAgentes where grupocatalogoagentes_id = (select id from midas.tcGrupoCatalogoAgentes where descripcion ='Lineas de Venta') and id = "+idLineaVenta+")");
		queryString.append(" and idToProducto in(select distinct rec.idproducto from "); 
		queryString.append(" midas.toProvisionesBonoAgente prov ");  
		queryString.append(" inner join midas.tocalculoprovision calc on (prov.id=calc.idProvision) ");  
		queryString.append(" inner join midas.todetallecalculoprovision det on(calc.id = det.idCalculo) ");  
		queryString.append(" inner join midas.torecibocalculoprovision rec on(det.id=rec.iddetallecalculobonos) ");
		queryString.append(" where prov.ID="+ idProvision +" ) ");
		queryString.append(" order by descripcionProducto ");
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>(1);
		lista = this.entityManager.createNativeQuery(queryString.toString(), GenericaAgentesView.class).getResultList();
		Map<Long,String> mapa = new HashMap<Long, String>(1);
		for(GenericaAgentesView  obj:lista){
			mapa.put(obj.getId(), obj.getValor());
		}
		return mapa;
	}

	@Override
	public Map<Long, String> getMapPromotoriasPorEjecutivo(Long idEjecutivo, Long idProvision) {
		List<GenericaAgentesView> lista = new ArrayList<GenericaAgentesView>(1);
		StringBuilder queryString = new StringBuilder("");

		queryString.append(" select prom.id as id, prom.descripcion as valor "); 
		queryString.append(" from midas.topromotoria prom ");
		queryString.append(" where prom.id in( ");
		queryString.append(" select distinct rec.idPromotoria  ");
		queryString.append(" from midas.tocalculoprovision  prov ");
		queryString.append(" inner join midas.todetallecalculoprovision det on det.idcalculo = prov.id ");
		queryString.append(" inner join midas.torecibocalculoprovision rec on rec.iddetallecalculobonos = det.id ");
		queryString.append(" where prov.idprovision = "+idProvision+" ) ");
		queryString.append(" and prom.ejecutivo_id ="+idEjecutivo );
		queryString.append(" order by prom.descripcion asc ");
		
		lista =this.entityManager.createNativeQuery(queryString.toString(),GenericaAgentesView.class).getResultList() ;
		
		Map<Long,String> mapa = new HashMap<Long, String>(1);
		for(GenericaAgentesView  obj:lista){
			mapa.put(obj.getId(), obj.getValor());
		}
		return mapa;
	}

	@Override
	public Map<Long, String> obtenerMapConceptosReembolsoGastoAjuste() {
		Map<String,Object> params = new HashMap<String, Object>(1);
		//Activo 
		params.put(UtileriasWeb.KEY_ESTATUS, OrdenCompraService.ESTATUSACTIVO);
		params.put(UtileriasWeb.KEY_CATEGORIA, ConceptoAjusteService.REEMBOLSO_GASTO_AJUSTE);
		return getMapConceptosAjuste(params);
	}
	
	@Override
	public Map<Integer, Integer> getMapYears(int year, int range) {
		
		Calendar cal = Calendar.getInstance();
		Map<Integer,Integer> years = new HashMap<Integer, Integer>(1);
		int difYear= cal.get(Calendar.YEAR) - year;
		
		if(difYear>range){
			years.putAll(getMapYears(range));
		}else{
			try {
				if (year >= 0) {
					for (int y = year; y <= cal.get(Calendar.YEAR); y++) 
					{
						years.put(y, y);
					}
				}
			} catch(RuntimeException error) {
				years.put(Calendar.YEAR, Calendar.YEAR);
			}
		}
		return years;
	}

	@Override
	public Map<Long, String> getMapBancosMidas() {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<BancoMidas> bancos = bancoMidasService.obtenerBancos();
		for(BancoMidas banco : CollectionUtils.emptyIfNull(bancos)){
			if(banco.getActivo().booleanValue()){
				map.put(banco.getId(), banco.getNombre());
			}
		}
		return map;
	}

	@Override
	public Map<Long, String> getMapCuentasBancoMidas(Long bancoId) {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<CuentaBancoMidas> cuentas = bancoMidasService.obtenerCuentas(bancoId);
		for(CuentaBancoMidas cuenta : CollectionUtils.emptyIfNull(cuentas)){
			if(cuenta.getActivo().booleanValue()){
				map.put(cuenta.getId(), cuenta.getNumeroCuenta());
			}
		}
		return map;
	}

	@Override
	public Map<Long, String> getMapCuentasAcreedorasSiniestros() {
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("tipo", ConceptoGuia.TIPO_CONCEPTO_GUIA.ACREEDORA);
		params.put("activo", Boolean.TRUE);
		return getMapConceptoGuia(params);
	}
	
	@Override
	public Map<Long, String> getMapCuentasManualesSiniestros() {
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("tipo", ConceptoGuia.TIPO_CONCEPTO_GUIA.MANUAL);
		params.put("activo", Boolean.TRUE);
		return getMapConceptoGuia(params);
	}

	private Map<Long, String> getMapConceptoGuia(Map<String, Object>params){
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<ConceptoGuia> conceptos = entidadService.findByPropertiesWithOrder(ConceptoGuia.class, 
				params, "descripcion");
		for(ConceptoGuia concepto : CollectionUtils.emptyIfNull(conceptos)){
			map.put(concepto.getId(), concepto.getDescripcion());
		}
		return map;
	}

	@Override
	public Map<Long, String> obtenerAbogados() {
		Map<Long,String> abogadosMap = new HashMap<Long, String>(1);
		ServicioSiniestroFiltro filtro = new ServicioSiniestroFiltro();
		filtro.setTipoServicio(Short.valueOf("2"));
		List<ServicioSiniestro> abogados = this.servicioSiniestroService.buscarServicioSiniestro(filtro);
		for(ServicioSiniestro abogado: abogados){
			abogadosMap.put(abogado.getId(), abogado.getNombrePersona());
		}
		return abogadosMap;
	}

	@Override
	public Map<Long, String> getCtgBancosValidosSalvamentos() {
		
		Map<Long,String> ctgBancos = new HashMap<Long,String>(1);
		
		//4208,4209,4210,1588 ID de bancos fijo - no cambian en producción
		ArrayList<Long> lIdBancoPermitidos = new ArrayList<Long>(1){
			private static final long serialVersionUID = 1L;
			{
			    add(Long.valueOf("4208"));
			    add(Long.valueOf("4209"));
			    add(Long.valueOf("4210"));
			    add(Long.valueOf("1588"));
			    add(Long.valueOf("120059"));
			    add(Long.valueOf("88629"));
			}};
		
		Map<Long,String> mBancos = this.getMapBancosMidas();
		int contador = 0, max = 5;
		for (Map.Entry<Long, String> entry : mBancos.entrySet()){
			
		    if( lIdBancoPermitidos.contains(entry.getKey()) && contador < max ){
		    	ctgBancos.put(entry.getKey(), entry.getValue());
		    	contador++;
		    }
		    
		    if( contador > max){
		    	break;
		    }		    
		}	
		return ctgBancos;
	}

	@Override
	public Map<String, String> getMediosPago(Negocio negocio){
		Map<String, String> mediosDePago = new HashMap<String, String>(1); 
		List<MedioPagoDTO> mediosPago = getMediosPagoDTO(negocio);
		for(MedioPagoDTO metodoPago : mediosPago){
			mediosDePago.put(metodoPago.getId(), metodoPago.getDescripcion());
		}
		
		return mediosDePago;
	}
	
	public Map<String, String> getMediosPagoSO(Negocio negocio){
		Map<String, String> mediosDePago = new HashMap<String, String>(1); 
		List<MedioPagoDTO> mediosPago = getMediosPagoDTO(negocio);
		
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_MOVIL_SO, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_MEDIOSPAGO_SO);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		
		if(parametro !=null && parametro.getValor()!=null){
			String[] idMediosPago = parametro.getValor().split(UtileriasWeb.SEPARADOR_COMA);
			for(MedioPagoDTO metodoPago : mediosPago){
				for(String idMedioPago : idMediosPago){
					if(idMedioPago.equals(metodoPago.getId())){
						mediosDePago.put(metodoPago.getId(), metodoPago.getDescripcion());
					}
				}
			}
		}
		
		return mediosDePago;
	}
	
	@Override
	public Map<String, String> getBancos(){
		Map<String, String> mapBancos = new HashMap<String, String>(1); 
		List<BancoEmisorDTO> bancos = bancoEmisorFacade.findAll();
		for(BancoEmisorDTO banco: bancos){
			mapBancos.put(banco.getIdBanco().toString(), banco.getNombreBanco());
		}
		return mapBancos;
	}
	
	public List<BancoEmisorDTO> getBancosList(){
		return bancoEmisorFacade.findAll();
	}
	
	private List<MedioPagoDTO> getMediosPagoDTO(Negocio negocio){
		List<MedioPagoDTO> mediosDePago = new ArrayList<MedioPagoDTO>(1); 
		List<NegocioProducto> negocioProducto = new ArrayList<NegocioProducto>(1);
		try {
			negocioProducto = entidadService.findByProperty(
					NegocioProducto.class, "negocio", negocio);
			for(NegocioProducto negocioProd : negocioProducto){
				ProductoDTO producto = entidadService.findById(ProductoDTO.class,
						negocioProd.getProductoDTO().getId());
				if(producto!=null){
					mediosDePago = producto.getMediosPago();
				}
				break;
			}
		} catch (RuntimeException e) {
			throw new ApplicationException("No es posible obtener los Medios de Pago.");
		}
		return mediosDePago;
	}
	
	@Override
	public Map<String, Object> getSpvData(Long idToNegocio){
		Map<String, Object> DataReturn = new LinkedHashMap<String, Object>(1);
		String numeroTelefonoSPV = "01 800 723 4763";
		String spvNumberDF = "5140 3050";
		Boolean esServicioPublico = isServicioPublico(idToNegocio);
		
		ParametroGeneralId id;
		
		if(esServicioPublico){
			id= new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_TELEFONO_SO_SPV,
					ParametroGeneralDTO.CODIGO_PARAM_GENERAL_TELEFONO_SO_REPUBLICA_SPV);
			ParametroGeneralDTO parametroTelefono = parametroGeneralFacade.findById(id);
			
			if(parametroTelefono !=null && parametroTelefono.getValor()!=null){
				numeroTelefonoSPV = parametroTelefono.getValor();
			}
			
			id= new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_TELEFONO_SO_SPV,
				   ParametroGeneralDTO.CODIGO_PARAM_GENERAL_TELEFONO_SO_DF_SPV);
			ParametroGeneralDTO parametroTelefonoDF = parametroGeneralFacade.findById(id);

			if(parametroTelefonoDF !=null && parametroTelefonoDF.getValor()!=null){
				spvNumberDF = parametroTelefonoDF.getValor();
			}
		}
		
		DataReturn.put("numeroTelefonoSPV", numeroTelefonoSPV);
		DataReturn.put("spvNumberDF", spvNumberDF);
		DataReturn.put("esServicioPublico", esServicioPublico);
		
		return DataReturn;
	}
	
	private boolean isServicioPublico(Long idToNegocio){
		boolean esServicioPublico = false;
		
		ParametroGeneralId id;		
		id= new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_TELEFONO_SO_SPV,
				   				   ParametroGeneralDTO.CODIGO_PARAM_GENERAL_NEGOCIOS_SO_SPV);
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		
		if(parametro !=null && parametro.getValor()!=null){
			String[] idNegocios = parametro.getValor().split(UtileriasWeb.SEPARADOR_COMA);
			for(String idNegocio : idNegocios){
				if(idNegocio.trim().equals(idToNegocio.toString())){
					esServicioPublico = true;
					break;
				}
			}
		}
		return esServicioPublico;
	}
	
	@Override
	public Map<Long, String> getMapCaTipoMoneda() {		
		Map<Long,String> map=new LinkedHashMap<Long, String>(1);
		StringBuilder queryString = new StringBuilder();
		queryString.append(" select distinct tm.id as id, tm.valor as valor, tm.nombre as  nombre from midas.CA_TIPOMONEDA tm order by id asc ");
		List<CaTipoMoneda> list=this.entityManager.createNativeQuery(queryString.toString(),CaTipoMoneda.class).getResultList();
		if(list!=null && !list.isEmpty()){
			for(CaTipoMoneda tipoMoneda:list){
					map.put(new Long(tipoMoneda.getValor().toString()),tipoMoneda.getNombre());
			}
		}
		return map;
	}

	@Override
	public  String getDescripionValorFijo(Integer claveTipoDeducible, int key) {
		String descripcionValorFijo = "";
		CatalogoValorFijoDTO catalogoValorFijo  = null;
		if(claveTipoDeducible != null){
			 catalogoValorFijo = entidadService.findById(CatalogoValorFijoDTO.class, new CatalogoValorFijoId(key, claveTipoDeducible));
			 descripcionValorFijo = catalogoValorFijo != null ? catalogoValorFijo.getDescripcion() : "";
		 }	
		return descripcionValorFijo;
	}
	
	@Override
	public  Map<String, String> listarNegocioSeccionPorProductoNegocioTipoPoliza(
			BigDecimal idToProducto, Long idToNegocio, BigDecimal idToTipoPoliza){
		Map<String,String> map = new HashMap<String, String>(1);
		List<NegocioSeccion> items = negocioSeccionDao.listarNegocioSeccionPorIdProductoNegocioTipoPoliza(
				idToProducto, idToNegocio, idToTipoPoliza);
		for(NegocioSeccion item:items){
			map.put(item.getIdToNegSeccion().toString(), item.getSeccionDTO().getDescripcion().toString());
		}
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getMapTipoServicioVehiculo(BigDecimal idToNegSeccion){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>(); 
		List<NegocioTipoServicio> negocioTipoServicioAsociadasList = entidadDao.findByProperty(NegocioTipoServicio.class, "negocioSeccion.idToNegSeccion", idToNegSeccion);
		for (NegocioTipoServicio negocioTipoServicio:negocioTipoServicioAsociadasList){
			map.put(negocioTipoServicio.getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo(), negocioTipoServicio.getTipoServicioVehiculoDTO().getDescripcionTipoServVehiculo());
		}
		return map;
	}
	
	@Override
	public boolean imprimirLeyendaUMA(Date fechaInicioVigencia){
		boolean imprimirUMA = false;
		Date tmpFecha = null;
		
		ParametroGeneralId id=new ParametroGeneralId(ParametroGeneralDTO.GRUPO_CONFIGURACION_GENERAL_COBERTURAS,
				ParametroGeneralDTO.CODIGO_FECHA_INICIO_UMA);
		
		ParametroGeneralDTO parametro=parametroGeneralFacade.findById(id);
		
		try{
			if(parametro !=null && parametro.getValor()!=null){
				String[] valoresParametros=parametro.getValor().split(UtileriasWeb.SEPARADOR_COMA);
				for(String valoresParametrosTmp : valoresParametros){
					tmpFecha = new SimpleDateFormat("dd/MM/yyyy").parse(valoresParametrosTmp.trim());
					if(fechaInicioVigencia.compareTo(tmpFecha) >= 0){
						imprimirUMA = true;
						break;
					}
				}
			}
		}
		catch (Exception ex){
			LogDeMidasEJB3.log("ListadoServiceImpl:::imprimirLeyendaUMA, No se pudo validar el Parametro General" , Level.WARNING, null);
		}
		
		return imprimirUMA;
	}
	
	@Override
	public String ObtieneMerchantPortalPagosWeb (){
		
		String merchant = null;
		
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL,
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_MERCHANT);
		
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		
		try{
			if(parametro != null && parametro.getValor() != null){
				merchant = parametro.getValor();
			}
		}
		catch (Exception ex){
			LogDeMidasEJB3.log("ListadoServiceImpl:::ObtieneMerchantPortalPagosWeb, No se pudo validar el Parametro General" , Level.WARNING, null);
		}
		
		return merchant;
	}
	
	@Override
	public String ObtieneUrlBackPortalPagosWeb (){
		
		String url_back = null;
		
		ParametroGeneralId id = new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERAL,
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_URL_BACK);
		
		ParametroGeneralDTO parametro = parametroGeneralFacade.findById(id);
		
		try{
			if(parametro != null && parametro.getValor() != null){
				url_back = parametro.getValor();
			}
		}
		catch (Exception ex){
			LogDeMidasEJB3.log("ListadoServiceImpl:::ObtieneMerchantPortalPagosWeb, No se pudo validar el Parametro General" , Level.WARNING, null);
		}
		
		return url_back;
	}
	
	@Override
	public CiudadDTO getMunicipioPorCp(String cp) {
		if (cp != null) {
			return ciudadFacadeRemote.getCityByZipCode(cp);
		}
		
		return null;
	}
	
	@Override
	public Map<Long, Integer> obtenerListadoVersionesRecuotificacionNegocio(
			Long idToNegocio) {
		return negocioRecuotificacionService.obtenerListadoVersiones(idToNegocio);				
	}	
	
	/**
	 * Obtener el listado de Tipos de vigencia asignados a un negocio
	 * @param idToNegocio
	 * @return
	 */
	@Override
	public Map<Integer, String> obtenerTiposVigenciaNegocio(Long idToNegocio){
		Map<Integer, String> map = new LinkedHashMap<Integer, String>(1);
		vigenciaDefault = -1;
		List<NegocioTipoVigencia> list = entidadDao.findByProperty(NegocioTipoVigencia.class, "negocio.idToNegocio", idToNegocio);
		for(NegocioTipoVigencia negocioTipoVigencia : list){
			if(negocioTipoVigencia.getEsDefault() != null && negocioTipoVigencia.getEsDefault() == 1){
				vigenciaDefault = negocioTipoVigencia.getTipoVigencia().getDias();
			}
			map.put(negocioTipoVigencia.getTipoVigencia().getDias(), negocioTipoVigencia.getTipoVigencia().getDescripcion());
		}
		return map;
	}
	
	/**
	 * Obtener vigencia default
	 * @param idToNegocio
	 * @return
	 */
	public Integer obtenerVigenciaDefault(Long idToNegocio){
		return vigenciaDefault;
	}
	
	@EJB
	public void setNegocioTipoUsoService(
			NegocioTipoUsoService negocioTipoUsoService) {
		this.negocioTipoUsoService = negocioTipoUsoService;
	}

	@EJB
	public void setNegocioTipoServicioService(
			NegocioTipoServicioService negocioTipoServicioService) {
		this.negocioTipoServicioService = negocioTipoServicioService;
	}

	@EJB
	public void setTarifaAgrupadorTarifaService(
			TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService) {
		this.tarifaAgrupadorTarifaService = tarifaAgrupadorTarifaService;
	}
	
	@EJB
	public void setEstadoService(NegocioEstadoService estadoService) {
		this.estadoService = estadoService;
	}

	@EJB
	public void setMunicipioService(NegocioMunicipioService municipioService) {
		this.municipioService = municipioService;
	}
	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setNegocioTipoUsoDao(
			NegocioTipoUsoDao negocioTipoUsoDao) {
		this.negocioTipoUsoDao = negocioTipoUsoDao;
	}
	
	@EJB
	public void setProductoBancarioService(
			ProductoBancarioService productoBancarioService) {
		this.productoBancarioService = productoBancarioService;
	}

	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@EJB
	public void setGerenciaJpaService(GerenciaJPAService gerenciaJpaService) {
		this.gerenciaJpaService = gerenciaJpaService;
	}
	@EJB
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}
	@EJB
	public void setPromotoriaJpaService(PromotoriaJPAService promotoriaJpaService) {
		this.promotoriaJpaService = promotoriaJpaService;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCatalogValorFijoService(
			CatalogoValorFijoFacadeRemote catalogValorFijoService) {
		this.catalogValorFijoService = catalogValorFijoService;
	}
	
	@EJB
	public void setNegocioTarifaService(
			NegocioTarifaService negocioTarifaService) {
		this.negocioTarifaService = negocioTarifaService;
	}

	@EJB
	public void setCentroEmisorService(CentroEmisorService centroEmisorService) {
		this.centroEmisorService = centroEmisorService;
	}
	@EJB
	public void setAgenteFacadeRemote(
			AgenteFacadeRemote agenteFacadeRemote) {
		this.agenteFacadeRemote = agenteFacadeRemote;
	}
	
	@EJB
	public void setTipoBienAutosService(
			TipoBienAutosFacadeRemote tipoBienAutosService) {
		this.tipoBienAutosService = tipoBienAutosService;
	}

	@EJB	
	public void setNegocioProductoDao(NegocioProductoDao negocioProductoDao) {
		this.negocioProductoDao = negocioProductoDao;
	} 
	
	@EJB
	public void setTipoPolizaFacadeRemote(TipoPolizaFacadeRemote tipoPolizaFacadeRemote) {
		this.tipoPolizaFacadeRemote = tipoPolizaFacadeRemote;
	}
	
	@EJB
	public void setNegocioService(NegocioService negocioService) {
		this.negocioService = negocioService;
	}

	@EJB	
	public void setNegocioPaqueteSeccionDao(NegocioPaqueteSeccionDao negocioPaqueteSeccionDao) {
		this.negocioPaqueteSeccionDao = negocioPaqueteSeccionDao;
	} 

	
	@EJB	
	public void setEstiloVehiculoFacadeRemote(
			EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote) {
		this.estiloVehiculoFacadeRemote = estiloVehiculoFacadeRemote;
	}

	@EJB	
	public void setMarcaVehiculoFacadeRemote(
			MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote) {
		this.marcaVehiculoFacadeRemote = marcaVehiculoFacadeRemote;
	}

	@EJB	
	public void setNegocioSeccionDao(NegocioSeccionDao negocioSeccionDao) {
		this.negocioSeccionDao = negocioSeccionDao;
	}
	
	@EJB
	public void setSeccionFacadeRemote(SeccionFacadeRemote seccionFacadeRemote){
		this.seccionFacadeRemote = seccionFacadeRemote;
	}
	
	@EJB
	public void setColorVehiculoDao(ValorSeccionCobAutosDao valorSeccionCobAutosDao) {
		this.valorSeccionCobAutosDao = valorSeccionCobAutosDao;
	}
	
	@EJB
	public void setMonedaFacadeRemote(MonedaFacadeRemote monedaFacadeRemote){
		this.monedaFacadeRemote = monedaFacadeRemote;
	}
	
	@EJB
	public void setTipoUsoVehiculoFacadeRemote (TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote){
		this.tipoUsoVehiculoFacadeRemote = tipoUsoVehiculoFacadeRemote;
	}
	
	@EJB
	public void setCoberturaSeccionFacadeRemote (CoberturaSeccionFacadeRemote coberturaSeccionFacadeRemote){
		this.coberturaSeccionFacadeRemote = coberturaSeccionFacadeRemote;
	}

	@EJB
	public void setTarifaAgrupadorTarifaDao(
			TarifaAgrupadorTarifaDao tarifaAgrupadorTarifaDao) {
		this.tarifaAgrupadorTarifaDao = tarifaAgrupadorTarifaDao;
	}
	
	@EJB
	public void setTipoServicioVehiculoFacadeRemote(
			TipoServicioVehiculoFacadeRemote tipoServicioVehiculoFacadeRemote) {
		this.tipoServicioVehiculoFacadeRemote = tipoServicioVehiculoFacadeRemote;
	}
	
    @EJB
	public void setTipoVehiculoFacadeRemote(
			TipoVehiculoFacadeRemote tipoVehiculoFacadeRemote) {
		this.tipoVehiculoFacadeRemote = tipoVehiculoFacadeRemote;
	}

    @EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
 
    
    @EJB
    public void setGrupoVariablesModificacionPrimaService(
			GrupoVariablesModificacionPrimaService grupoVariablesModificacionPrimaService) {
		this.grupoVariablesModificacionPrimaService = grupoVariablesModificacionPrimaService;
	}	
    
    @EJB
	public void setAgrupadorTarifaService(
			AgrupadorTarifaService agrupadorTarifaService) {
		this.agrupadorTarifaService = agrupadorTarifaService;
	}

    @EJB
	public void setProductoFacadeRemote(ProductoFacadeRemote productoFacadeRemote) {
		this.productoFacadeRemote = productoFacadeRemote;
	}

    @EJB
	public void setPromotoriaService(PromotoriaService promotoriaService) {
		this.promotoriaService = promotoriaService;
	}

    @EJB
	public void setGerenciaService(GerenciaService gerenciaService) {
		this.gerenciaService = gerenciaService;
	}

    @EJB
	public void setOficinaService(OficinaService oficinaService) {
		this.oficinaService = oficinaService;
	}

    @EJB
	public void setAgenteService(AgenteService agenteService) {
		this.agenteService = agenteService;
	}
    @EJB
	public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}
    @EJB
	public void setMunicipioFacadeRemote(MunicipioFacadeRemote municipioFacadeRemote) {
		this.municipioFacadeRemote = municipioFacadeRemote;
	}
    
	@EJB
	public void setModeloVehiculoFacadeRemote(
			ModeloVehiculoFacadeRemote modeloVehiculoFacadeRemote) {
		this.modeloVehiculoFacadeRemote = modeloVehiculoFacadeRemote;
	}

	@EJB
	public void setNegocioAgenteService(
			NegocioAgenteService negocioAgenteService) {
		this.negocioAgenteService = negocioAgenteService;
	}
	
	
	@EJB
	 public void setSeccionCotizacionFacadeRemote(
				SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote) {
			this.seccionCotizacionFacadeRemote = seccionCotizacionFacadeRemote;
	 }
			
	@EJB
	public void setIncisoCotizacionFacadeRemote(
				IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote) {
			this.incisoCotizacionFacadeRemote = incisoCotizacionFacadeRemote;
		}
	
	@EJB
	public void setPaisFacadeRemote(PaisFacadeRemote paisFacadeRemote) {
		this.paisFacadeRemote = paisFacadeRemote;
	}
	
	@EJB
	public void setColoniaFacadeRemote(ColoniaFacadeRemote coloniaFacadeRemote) {
		this.coloniaFacadeRemote = coloniaFacadeRemote;
	}
	
	@EJB
	public void setNegocioDao(NegocioDao negocioDao) {
		this.negocioDao = negocioDao;
	}
	
	@EJB
	public void setFormaPagoInterfazServiciosRemote(
			FormaPagoInterfazServiciosRemote formaPagoInterfazServiciosRemote) {
		this.formaPagoInterfazServiciosRemote = formaPagoInterfazServiciosRemote;
	}

	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }
	
	@EJB
	public void setListadoIncisosDinamicoService(
			ListadoIncisosDinamicoService listadoIncisosDinamicoService) {
		this.listadoIncisosDinamicoService = listadoIncisosDinamicoService;
	}
	
	@EJB
	public void setConfigBonosService(ConfigBonosService configBonosService) {
		this.configBonosService = configBonosService;
	}
	
	@EJB
	public void setConfiguracionDatoIncisoBitemporalService(
			ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService) {
		this.configuracionDatoIncisoBitemporalService = configuracionDatoIncisoBitemporalService;
	}	
	
	@EJB
	public void setIncisoService(IncisoViewService incisoService) {
		this.incisoService = incisoService;
	}	
	
	@EJB
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}
	
	@EJB
	public void setNegocioEstadoDescuentoDao(
			NegocioEstadoDescuentoDao negocioEstadoDescuentoDao) {
		this.negocioEstadoDescuentoDao = negocioEstadoDescuentoDao;
	}
	
	@EJB
	public void setConfNotificacionesService(
			ConfiguracionNotificacionesService confNotificacionesService) {
		this.confNotificacionesService = confNotificacionesService;
	}
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	@EJB
	public void setIncisoViewService(
			IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}
	
	@EJB
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}

	public ImpresionRecibosService getImpresionRecibosService() {
		return impresionRecibosService;
	}

	@EJB
	public void setImpresionRecibosService(
			ImpresionRecibosService impresionRecibosService) {
		this.impresionRecibosService = impresionRecibosService;
	}

	@EJB
	public void setCotizadorAgentesService(
			CotizadorAgentesService cotizadorAgentesService) {
		this.cotizadorAgentesService = cotizadorAgentesService;
	}

	@EJB
	public void setCiudadFacadeRemote(CiudadFacadeRemote ciudadFacadeRemote) {
		this.ciudadFacadeRemote = ciudadFacadeRemote;
	}
}