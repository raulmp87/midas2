package mx.com.afirme.midas2.service.impl.datosAmis;

import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas2.domain.poliza.datosamis.DatosAmis;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.datosAmis.DatosAmisService;

@Stateless
public class DatosAmisServiceImpl implements DatosAmisService{
	@EJB
	protected EntidadService entidadService;
	
	
	@Override
	public Map<Long, String> getListarDatos() {
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<DatosAmis> datosAmis =  entidadService.findByProperty(DatosAmis.class, "activo", 1);
		for(DatosAmis item : datosAmis){
			map.put(item.getId(), item.getDescripcion());
		}
		return map;
	}
}
