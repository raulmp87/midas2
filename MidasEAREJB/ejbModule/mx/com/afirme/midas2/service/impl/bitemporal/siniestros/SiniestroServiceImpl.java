package mx.com.afirme.midas2.service.impl.bitemporal.siniestros;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroVehiculoDTO;
import mx.com.afirme.midas2.service.bitemporal.siniestros.SiniestroService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class SiniestroServiceImpl implements SiniestroService {
	
	@EJB
	private EntidadService entidadService;
	
	@SuppressWarnings("unchecked")
	@Override
	/**
	 * Consulta todos los siniestros de un numero de serie
	 * 
	 * 
	 * Método que realiza la búsqueda de los siniestros asociados a un Vehículo.
	 * Buscar en IncisoReporteCabina el numero de Serie, y obtener el Siniestro asociado
	 */
	public List<ReporteSiniestroVehiculoDTO> buscarSiniestro( String numeroSerie ){
		
		
		Map<String, Object>  params = new HashMap<String, Object>();
		StringBuilder query = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroVehiculoDTO( ");
		query.append(" reporteCabina.id, ")
		.append(" func('CONCAT', TRIM(reporteCabina.claveOficina), func('CONCAT', '-', func('CONCAT',  reporteCabina.consecutivoReporte, func('CONCAT', '-', reporteCabina.anioReporte)))),  ")
		.append(" func('CONCAT', TRIM(reporteCabina.siniestroCabina.claveOficina), func('CONCAT', '-', func('CONCAT',  reporteCabina.siniestroCabina.consecutivoReporte, func('CONCAT', '-', reporteCabina.siniestroCabina.anioReporte)))),  ")
		.append(" reporteCabina.seccionReporteCabina.incisoReporteCabina.nombreAsegurado, ")
		.append(" func('CONCAT', TRIM(reporteCabina.poliza.codigoProducto), func('CONCAT', TRIM(reporteCabina.poliza.codigoTipoPoliza), func('CONCAT', '-', func('CONCAT',  func('LPAD',reporteCabina.poliza.numeroPoliza,6,0), func('CONCAT', '-', func('LPAD',reporteCabina.poliza.numeroRenovacion,2,0)))))),  ")
		.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catCausaSiniestro, reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.causaSiniestro), ")	
		.append(" func('MIDAS.PKGSIN_POLIZAS.obtenerDescripcionCatValorFijo', :catTerminoSiniestro, reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.terminoSiniestro), ")	
		.append(" reporteCabina.ajustador.personaMidas.nombre, ")
		.append(" reporteCabina.fechaHoraReporte, ")
		.append(" reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie) ")
		.append(" FROM ReporteCabina reporteCabina")
		.append(" WHERE reporteCabina.seccionReporteCabina.incisoReporteCabina.autoIncisoReporteCabina.numeroSerie = :numeroSerie ");
		
		params.put("catCausaSiniestro", TIPO_CATALOGO.CAUSA_SINIESTRO.toString());
		params.put("catTerminoSiniestro", TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO.toString());
		params.put("numeroSerie", numeroSerie);
		
		return entidadService.executeQueryMultipleResult(query.toString(), params);
	}
	
	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
}
