package mx.com.afirme.midas2.service.impl.cobranza.cliente;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.domain.cobranza.cliente.CotizacionCobranza;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.cliente.CotizacionCobranzaService;

@Stateless
public class CotizacionCobranzaServiceImpl implements CotizacionCobranzaService {
	
	private static final Logger LOG = Logger.getLogger(CotizacionCobranzaServiceImpl.class);
	
	@EJB
	private EntidadService entidadService;

	@Override
	public CotizacionCobranza findFirstByProperty(String propertyName, Object propertyValue) {
		List<CotizacionCobranza> cotizacionCobranzaTmp = findByProperty(propertyName, propertyValue);
		if (cotizacionCobranzaTmp!=null && cotizacionCobranzaTmp.size() > 0){
			return cotizacionCobranzaTmp.get(0);
		}
		
		return null;
	}

	@Override
	public List<CotizacionCobranza> findByProperty(String propertyName, Object propertyValue) {
		return entidadService.findByProperty(CotizacionCobranza.class, propertyName, propertyValue);
	}

	@Override
	public void guardarDatosCotizacionCobranza(CotizacionDTO cotizacionDTO, Integer idConductoCobro, Integer idMedioPago, boolean clienteEsTitular){
		CotizacionCobranza cotizacionCobranza = this.findFirstByProperty("cotizacionDTO.idToCotizacion", cotizacionDTO.getIdToCotizacion());
		
		if (cotizacionCobranza == null){
			cotizacionCobranza = new CotizacionCobranza();
		}
		cotizacionCobranza.setCotizacionDTO(cotizacionDTO);
		cotizacionCobranza.setIdConducto(idConductoCobro);
		cotizacionCobranza.setIdConductoCobro(idMedioPago);
		cotizacionCobranza.setClienteEsTitular(clienteEsTitular);
		
		if (cotizacionCobranza.getId()!=null){
			LOG.info("La informacion se actualizara para la cotizacion: " + cotizacionDTO.getIdToCotizacion());
		}else{
			LOG.info("La informacion no existe para la cotizacion: " + cotizacionDTO.getIdToCotizacion());
		}
		
		entidadService.save(cotizacionCobranza);
	}
}