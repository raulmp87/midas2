package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.calculo;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.RecargosDescuentosCPDTO;
import mx.com.afirme.midas.catalogos.descuento.DescuentoDTO;
import mx.com.afirme.midas.catalogos.recargovario.RecargoVarioDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.endoso.cobertura.CoberturaEndosoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoCoberturaCot;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoCoberturaCotId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoCoberturaCotService;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoCoberturaCot;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoCoberturaCotId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoCoberturaCotService;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.cp.RecargosDescuentosCPDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicioDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.calculo.CalculoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.BitemporalComision;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento.BitemporalCoberturaDescuento;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento.CoberturaDescuentoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo.BitemporalCoberturaRecargo;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo.CoberturaRecargoContinuity;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVeh;
import mx.com.afirme.midas2.domain.catalogos.ServVehiculoLinNegTipoVehId;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDescId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.ContenedorPrimas;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosGeneralDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.CotizacionBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.inciso.IncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.estadodescuento.NegocioEstadoDescuentoService;
import mx.com.afirme.midas2.service.negocio.motordecision.MotorDecisionAtributosService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.extrainfo.CotizacionExtService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.Validate;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;


@Stateless
public class CalculoServiceImpl implements CalculoService {
	private static Logger LOG = Logger.getLogger(CalculoServiceImpl.class);
	private CalculoDao calculoDao;
	private EntidadDao entidadDao;
	private NegocioSeccionService negocioSeccionService;
	private NegocioTarifaService negocioTarifaService;
	private TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService;
	
	private IncisoCotizacionFacadeRemote incisoCotizacionFacade;
	@EJB
	private IncisoService incisoService;
	private EntidadService entidadService;
	private CotizacionService cotizacionService;
	private NegocioTipoServicioDao negocioTipoServicioDao;
	private static ThreadLocal<BigDecimal> idSubRamoC = new ThreadLocal<BigDecimal>();
	private ListadoService listadoService;
	private final MathContext mathCtx = new MathContext(50, RoundingMode.HALF_EVEN);
	private final MathContext mathCtxResumen = new MathContext(2, RoundingMode.HALF_EVEN);
	
	private final BigDecimal CIEN = new BigDecimal(100, mathCtx);
	private final BigDecimal UNO = new BigDecimal(1, mathCtx);
	
	private final String TIPO_DESCUENTO = "DESC";
	private final String TIPO_RECARGO = "RECA";
	
	protected List<MovimientoEndoso> movimientos;
	protected Boolean incisoModificado = false;
	protected EntidadContinuityService entidadContinuityService;
	
	protected CotizacionBitemporalService cotizacionBitemporalService;
	private DescuentoCoberturaCotService descuentoCoberturaCotService;
	private RecargoCoberturaCotService recargoCoberturaCotService;
	private NegocioEstadoDescuentoService negocioEstadoDescuentoService;	
	private IncisoBitemporalService incisoBitemporalService;
	private RecargosDescuentosCPDao recargosDescuentosCPDao;
	private CotizacionExtService cotizacionExtService;//JFGG
	
	@EJB
	protected MotorDecisionAtributosService motorDecisionAtributosService;
	@EJB
	private CotizadorAgentesService cotizadorAgentesService;
	
	@EJB
	private EndosoService endosoService;	
	
	@EJB
	public void setCotizacionBitemporalService(
			CotizacionBitemporalService cotizacionBitemporalService) {
		this.cotizacionBitemporalService = cotizacionBitemporalService;
	}

	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@EJB
	public void setNegocioTipoServicioDao(NegocioTipoServicioDao negocioTipoServicioDao) {
		this.negocioTipoServicioDao = negocioTipoServicioDao;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setIncisoCotizacionFacade(
			IncisoCotizacionFacadeRemote incisoCotizacionFacade) {
		this.incisoCotizacionFacade = incisoCotizacionFacade;
	}

	@EJB
	public void setCalculoDao(CalculoDao calculoDao) {
		this.calculoDao = calculoDao;
	}

	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	@EJB
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}

	@EJB
	public void setNegocioTarifaService(
			NegocioTarifaService negocioTarifaService) {
		this.negocioTarifaService = negocioTarifaService;
	}

	@EJB
	public void setTarifaAgrupadorTarifaService(
			TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService) {
		this.tarifaAgrupadorTarifaService = tarifaAgrupadorTarifaService;
	}

	@EJB
	public void setDescuentoCoberturaCotService(
			DescuentoCoberturaCotService descuentoCoberturaCotService) {
		this.descuentoCoberturaCotService = descuentoCoberturaCotService;
	}
	
	@EJB
	public void setRecargoCoberturaCotService(
			RecargoCoberturaCotService recargoCoberturaCotService) {
		this.recargoCoberturaCotService = recargoCoberturaCotService;
	}	

	@EJB
	public void setNegocioEstadoDescuentoService(
			NegocioEstadoDescuentoService negocioEstadoDescuentoService) {
		this.negocioEstadoDescuentoService = negocioEstadoDescuentoService;
	}
	
	@EJB
	public void setIncisoBitemporalService(
			IncisoBitemporalService incisoBitemporalService) {
		this.incisoBitemporalService = incisoBitemporalService;
	}

	@EJB
	public void setRecargosDescuentosCPDao(
			RecargosDescuentosCPDao recargosDescuentosCPDao) {
		this.recargosDescuentosCPDao = recargosDescuentosCPDao;
	}
	@Autowired
	@Qualifier("cotizacionExtServiceEJB")
	public void setCotizacionExtService(CotizacionExtService cotizacionExtService) {//JFGG
		this.cotizacionExtService = cotizacionExtService;
	}

	@Override
	public CotizacionDTO calcular(CotizacionDTO cotizacionDTO) {
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, cotizacionDTO.getIdToCotizacion());
		
		for(IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()){
			calcular(inciso);
		}
		return cotizacion;
	}
	
	@Override
	public void calcularEndoso(BitemporalCotizacion bitemporalCotizacion, DateTime validoEn, Integer tipoEndoso) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.cotizacionContinuity.id", bitemporalCotizacion.getContinuity().getId());		
		List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class, 
				params, validoEn, null, true);
		
		//for (BitemporalInciso inciso : bitemporalCotizacion.getContinuity().getIncisoContinuities().getInProcess(validoEn)) {
		for (BitemporalInciso inciso : incisos) {
			if(inciso.getRecordStatus().equals(RecordStatus.TO_BE_ADDED)){
				calcular(inciso, validoEn, tipoEndoso, false);
			}
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void igualarPrima(final BigDecimal idToCotizacion, Double primaTotalAIgualar, Boolean restaurarDescuento) {		
		BigDecimal primaIgualada = null;
		ContenedorPrimas contenedorPrimas = null;
		BigDecimal pcteIva = null;
		BigDecimal pcteRecPagoFracc = null;
		BigDecimal pcteCesionComision = null;
		BigDecimal descuentoAplicar = null;
		BigDecimal derechosACobrar = null;
		BigDecimal valorIva = null;
		BigDecimal comisionMenosBonificacion = BigDecimal.ZERO;
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);				
		if (cotizacion.getPorcentajeIva() == null || cotizacion.getPorcentajePagoFraccionado() == null || cotizacion.getPorcentajebonifcomision() == null) {
			throw new RuntimeException("No se tienen definidos los porcentajes necesarios para la igualación");
		}
		if (cotizacion.getValorPrimaTotal() == null) {
			throw new RuntimeException("Primero debe calcularse la cotización con las condiciones actuales antes de intentar la igualación");
		}
		if(cotizacion.getIncisoCotizacionDTOs().size() == 0)
		{
			throw new RuntimeException("No existen incisos asociados a la cotización");						
		}
		
		pcteIva = new BigDecimal(cotizacion.getPorcentajeIva(), mathCtx);
		pcteRecPagoFracc = new BigDecimal(cotizacion.getPorcentajePagoFraccionado(), mathCtx);
		pcteCesionComision = new BigDecimal(cotizacion.getPorcentajebonifcomision(), mathCtx);
		//List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
		derechosACobrar = new BigDecimal(cotizacion.getValorDerechosUsuario() * cotizacion.getIncisoCotizacionDTOs().size(), mathCtx);
		valorIva = pcteIva.divide(CIEN).add(UNO);
		contenedorPrimas = getPrimaNetaCoberturasPropias(idToCotizacion);
		if (primaTotalAIgualar.doubleValue() <= contenedorPrimas.getTotalPrimaExternas().add(derechosACobrar).multiply(valorIva).doubleValue()){
			throw new RuntimeException("Imposible igualar prima. El monto solicitado no cubre al menos las coberturas externas.");
		}
		primaIgualada = (new BigDecimal(primaTotalAIgualar, mathCtx));
		primaIgualada = primaIgualada.divide(valorIva, mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		primaIgualada = primaIgualada.subtract(derechosACobrar);
		primaIgualada = primaIgualada.divide(pcteRecPagoFracc.divide(CIEN).add(UNO), mathCtx).setScale(16, RoundingMode.HALF_EVEN);		
		//calculo comision
		Predicate findId = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				ComisionCotizacionDTO id = (ComisionCotizacionDTO)arg0; 
				return (id.getId().getIdToCotizacion().equals(idToCotizacion) && id.getId().getIdTcSubramo().equals(CalculoServiceImpl.getIdSubRamoC()) 
							&& id.getId().getTipoPorcentajeComision().equals((short) 1)) ;
			}
		};
		ComisionCotizacionDTO comisionCotizacionDTO  = (ComisionCotizacionDTO) CollectionUtils.find(cotizacion.getComisionCotizacionDTOs(), findId);		
		if (comisionCotizacionDTO != null ) {
			if (cotizacion.getPorcentajebonifcomision() != null) {
				comisionMenosBonificacion = pcteCesionComision.divide(CIEN, mathCtx).setScale(16, RoundingMode.HALF_EVEN).multiply(
						comisionCotizacionDTO.getPorcentajeComisionDefault().divide(CIEN, mathCtx).setScale(16, RoundingMode.HALF_EVEN), mathCtx).
						setScale(16, RoundingMode.HALF_EVEN);				
			}
		}
		//<primaIgualada = primaIgualada.multiply(comisionMenosBonificacion.divide(CIEN, mathCtx).add(UNO), mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		primaIgualada = primaIgualada.divide(UNO.subtract(comisionMenosBonificacion), mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		primaIgualada = primaIgualada.subtract(contenedorPrimas.getTotalPrimaExternas());
	    descuentoAplicar = (primaIgualada.multiply(CIEN, mathCtx).divide(contenedorPrimas.getTotalPrimaPropias(), mathCtx)).setScale(16, RoundingMode.HALF_EVEN); 				
		descuentoAplicar = CIEN.subtract(descuentoAplicar);
		//System.out.println("DESCUENTO ::::::::::: " + descuentoAplicar);
		restaurarPrimaCotizacion(idToCotizacion);
		modificarDescuentoCoberturasPropias(idToCotizacion, descuentoAplicar);
		cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.setPorcentajeDescuentoGlobal(0d);
		cotizacion.setDescuentoIgualacionPrimas(descuentoAplicar.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
		cotizacion.setIgualacionNivelCotizacion(Boolean.TRUE);
		cotizacion.setPrimaTotalAntesDeIgualacion(cotizacion.getValorPrimaTotal().setScale(2, RoundingMode.HALF_EVEN).doubleValue());
		entidadService.save(cotizacion);
		
		//Al solicitar manualmente la igualacion de primas al desc por estado se le dara el valor del desc por igualacion
		if(restaurarDescuento) {
			for (IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()) {
				if(inciso.getIncisoAutoCot() != null) {
					inciso.getIncisoAutoCot().setPctDescuentoEstado(cotizacion.getDescuentoIgualacionPrimas());
					entidadService.save(inciso.getIncisoAutoCot());
				}
			}
		}
		
		calcular(cotizacion);
		//Solo aplica para individuales
		if(cotizacion.getTipoPolizaDTO().getClaveAplicaFlotillas().intValue() == 0 ){
			ajusteIgualacion(idToCotizacion, BigDecimal.ZERO,  primaTotalAIgualar);
		}
	}
	
	@Override
	public void ajusteIgualacion(BigDecimal idToCotizacion, BigDecimal numeroInciso, Double primaTotal){
		calculoDao.ajusteIgualacionCotizacion( idToCotizacion,  numeroInciso,  primaTotal);
	}

	/**
	 * Igualacion de primas a nivel de inciso
	 * 
	 * @param idToCotizacion
	 *            El id de la cotizacion, no puede ser null
	 * @param numeroInciso
	 *            El numero de inciso, no puede ser null
	 * @param primaTotalAIgualar
	 *            El monto de la prima total a igualar, no puede ser null
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void igualarPrimaInciso(final BigDecimal idToCotizacion, final BigDecimal numeroInciso,
			final Double primaTotalAIgualar) {
		Validate.notNull(idToCotizacion);
		Validate.notNull(numeroInciso);
		Validate.notNull(primaTotalAIgualar);
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if (cotizacion.getPorcentajeIva() == null || cotizacion.getPorcentajePagoFraccionado() == null
				|| cotizacion.getPorcentajebonifcomision() == null) {
			throw new NegocioEJBExeption("", "No se tienen definidos los porcentajes necesarios para la igualación");
		}
		
		NegocioBonoComision negocioBonoComision = null;
		if(cotizacion != null){
			Long idToNegocio = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
			List<NegocioBonoComision> negocioBonoComisionList = entidadService.findByProperty(NegocioBonoComision.class, "negocio.idToNegocio", idToNegocio);
			if(negocioBonoComisionList != null && !negocioBonoComisionList.isEmpty()){
				negocioBonoComision = negocioBonoComisionList.get(0);
			}
		}

		final BigDecimal pcteIva = new BigDecimal(cotizacion.getPorcentajeIva(), mathCtx);
		final BigDecimal pcteRecPagoFracc = new BigDecimal(cotizacion.getPorcentajePagoFraccionado(), mathCtx);
		final BigDecimal pcteCesionComision = new BigDecimal(cotizacion.getPorcentajebonifcomision(), mathCtx);
		final BigDecimal valorDerechos = new BigDecimal(cotizacion.getValorDerechosUsuario(), mathCtx);
		final BigDecimal primaAIgualar = new BigDecimal(primaTotalAIgualar, mathCtx);

		final IncisoCotizacionId incisoId = new IncisoCotizacionId(idToCotizacion, numeroInciso);
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoId);

		final ContenedorPrimas contenedorPrimas = getPrimaNetaCoberturasPropias(idToCotizacion, numeroInciso);
		final BigDecimal valorPrimaSinIgualacion = contenedorPrimas.getTotalPrimaExternas().add(valorDerechos)
				.multiply(pcteIva.divide(CIEN, mathCtx).add(UNO));

		if (primaAIgualar.compareTo(valorPrimaSinIgualacion) <= 0) {
			throw new NegocioEJBExeption("",
					"Imposible igualar prima. El monto solicitado no cubre al menos las coberturas externas.");
		}
		BigDecimal primaIgualada = primaAIgualar.divide(pcteIva.divide(CIEN, mathCtx).add(UNO), mathCtx);
		primaIgualada = primaIgualada.subtract(valorDerechos);
		primaIgualada = primaIgualada.divide(pcteRecPagoFracc.divide(CIEN, mathCtx).add(UNO), mathCtx);
		BigDecimal comisionMenosBonificacion = BigDecimal.ZERO;
		// calculo comision
		final Predicate findId = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				ComisionCotizacionDTO id = (ComisionCotizacionDTO) arg0;
				return (id.getId().getIdToCotizacion().equals(idToCotizacion)
						&& id.getId().getIdTcSubramo().equals(CalculoServiceImpl.getIdSubRamoC()) && id.getId()
						.getTipoPorcentajeComision().equals((short) 1));
			}
		};
		final ComisionCotizacionDTO comisionCotizacionDTO = (ComisionCotizacionDTO) CollectionUtils.find(
															cotizacion.getComisionCotizacionDTOs(), findId);
		if (comisionCotizacionDTO != null) {
			if (cotizacion.getPorcentajebonifcomision() != null) {
				comisionMenosBonificacion = pcteCesionComision.divide(CIEN, mathCtx).setScale(16, RoundingMode.HALF_EVEN).multiply(
						comisionCotizacionDTO.getPorcentajeComisionDefault().divide(CIEN, mathCtx).setScale(16, RoundingMode.HALF_EVEN), mathCtx).
						setScale(16, RoundingMode.HALF_EVEN);						
			}
		}
		if(negocioBonoComision != null && negocioBonoComision.getPctUDI() != null){
			comisionMenosBonificacion = comisionMenosBonificacion.multiply(BigDecimal.ONE.subtract((negocioBonoComision.getPctUDI().divide(CIEN, mathCtx)))).setScale(16, RoundingMode.HALF_EVEN);
		}	
		
		BigDecimal descuentoSobreCoberturasB = BigDecimal.ZERO;
		if(cotizacion != null && cotizacion.getPorcentajeDescuentoGlobal() != null && !cotizacion.getIgualacionNivelCotizacion()){
			Double descuentoGlobal = 0d;//Dejara de aplicarse el descuento global
			Double descuentoVolumen = 0d;
			try{
				descuentoVolumen = cotizacionService.getDescuentoPorVolumenaNivelPoliza(cotizacion);
			}catch(Exception e){
				e.printStackTrace();
			}
			//if(descuentoGlobal >= 0){
			Double descuentoSobreCoberturas = descuentoGlobal > (descuentoVolumen != null ? descuentoVolumen.doubleValue() : 0d) ? 
					descuentoGlobal : descuentoVolumen;
			descuentoSobreCoberturasB = new BigDecimal(descuentoSobreCoberturas);
			/*}else{
				descuentoSobreCoberturasB = new BigDecimal(descuentoGlobal);	
			}*/
		}
			
		primaIgualada = primaIgualada.divide(UNO.subtract(comisionMenosBonificacion), mathCtx).setScale(16, RoundingMode.HALF_EVEN);		
		primaIgualada = primaIgualada.subtract(contenedorPrimas.getTotalPrimaExternas());
		BigDecimal descuentoAplicar = primaIgualada.divide(contenedorPrimas.getTotalPrimaPropias(), mathCtx);
		descuentoAplicar = ((UNO.subtract(descuentoAplicar)).subtract(descuentoSobreCoberturasB.divide(CIEN))).multiply(CIEN);
		eliminarDescuentos(inciso);
		eliminarRecargos(inciso);
		modificarDescuentoCoberturasPropias(inciso, descuentoAplicar);	
		inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoId);		
		inciso.setDescuentoIgualacionPrimas(descuentoAplicar.setScale(2, RoundingMode.HALF_EVEN).doubleValue());
		inciso.setIgualacionNivelInciso(Boolean.TRUE);
		
		inciso.setPrimaTotalAntesDeIgualacion(incisoService.getValorPrimaTotal(inciso));
		entidadService.save(inciso);
		//Al desc por estado se le dara el valor del desc por igualacion
		if(inciso.getIncisoAutoCot() != null) {
			inciso.getIncisoAutoCot().setPctDescuentoEstado(inciso.getDescuentoIgualacionPrimas());
			entidadService.save(inciso.getIncisoAutoCot());
		}
		calcular(inciso);
		ajusteIgualacion(idToCotizacion, numeroInciso,  primaTotalAIgualar);
	}

	/**
	 * Igualacion de primas, pero con objetos bitemporales
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void igualarPrimaEndoso(Long idToCotizacion, BigDecimal primaTotalAIgualar) {
		
		ControlEndosoCot cec = this.obtenerControlEndosoCotizacion(idToCotizacion.intValue());
		
		if(cec != null){
			cec.setPrimaTotalIgualar(primaTotalAIgualar);			
			entidadService.save(cec);
		}		
	}

	/**
	 * Igualacion de primas a nivel de inciso en un endoso (con bitemporalidad)
	 * 
	 * @param incisoContinuityId
	 *            El id del continuty del inciso
	 * @param numeroInciso
	 *            El numero de inciso, no puede ser null
	 * @param primaTotalAIgualar
	 *            El monto de la prima total a igualar, no puede ser null
	 * @param validoEn
	 *            La fecha valida para el endoso
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void igualarPrimaIncisoEndoso(final Long incisoContinuityId, Double primaTotalAIgualar, DateTime validoEn, Integer tipoEndoso) {
		
		Validate.notNull(incisoContinuityId);
		Validate.notNull(primaTotalAIgualar);
		Validate.notNull(validoEn);
		final BitemporalInciso incisoBitemporal = entidadBitemporalService.getInProcessByKey(IncisoContinuity.class,
				incisoContinuityId, validoEn);
		
		BitemporalSeccionInciso seccionInciso = incisoBitemporal.getEntidadContinuity().getIncisoSeccionContinuities()
		.getInProcess(validoEn).iterator().next();
		
		final Integer numeroInciso = incisoBitemporal.getContinuity().getNumero();

		final BitemporalCotizacion cotizacionBitemporal = incisoBitemporal.getContinuity().getParentContinuity()
				.getBitemporalProperty().getInProcess(validoEn);
		final Cotizacion cotizacion = cotizacionBitemporal.getValue();
		
		NegocioBonoComision negocioBono = new NegocioBonoComision();
		try{
			Long idToNegocio = cotizacion.getSolicitud().getNegocio().getIdToNegocio();
			List<NegocioBonoComision> negocioBonoComisionList = entidadService.findByProperty(NegocioBonoComision.class, "negocio.idToNegocio", idToNegocio);
			if(negocioBonoComisionList != null && !negocioBonoComisionList.isEmpty()){
				negocioBono = negocioBonoComisionList.get(0);
			}
		}catch(Exception e){
		}

		this.restaurarPrimaIncisoEndoso(incisoBitemporal, validoEn, tipoEndoso, true);
		
		if (cotizacion.getPorcentajeIva() == null || cotizacion.getPorcentajePagoFraccionado() == null
				|| cotizacion.getPorcentajebonifcomision() == null) {
			throw new NegocioEJBExeption("", "No se tienen definidos los porcentajes necesarios para la igualación");
		}

		final BigDecimal pcteIva = new BigDecimal(cotizacion.getPorcentajeIva());
		final BigDecimal pcteRecPagoFracc = new BigDecimal(cotizacion.getPorcentajePagoFraccionado());
		final BigDecimal pcteCesionComision = new BigDecimal(cotizacion.getPorcentajebonifcomision());
		double descuentoSobreCoberturas = 0.0d;
		double descuentoGlobal = 0.0d;
		Double descuentoVolumen = null;
		BigDecimal valorDerechos = new BigDecimal(0);
		BigDecimal eliminarDescuentosCoberturasPropias = new BigDecimal(1);
		BigDecimal descuentoSobreCoberturasPropias = new BigDecimal(0);
		
		//BigDecimal valorDerechos = new BigDecimal(cotizacion.getValorDerechosUsuario(), mathCtx);
		if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
		{
			NegocioDerechoEndoso derechosEndoso = movimientoEndosoBitemporalService.getNegocioDerechoEndoso(cotizacionBitemporal.getContinuity().getBitemporalProperty().getInProcess(validoEn));
			valorDerechos = new BigDecimal(derechosEndoso.getImporteDerecho());
		}else{
			valorDerechos =  new BigDecimal(cotizacion.getValorDerechosUsuario());
		}
		final BigDecimal primaAIgualar = new BigDecimal(primaTotalAIgualar,mathCtx);

		final CotizacionContinuity cotizacionContinuty = cotizacionBitemporal.getContinuity();

		final ContenedorPrimas contenedorPrimas = getPrimaNetaCoberturasPropiasEndoso(cotizacionBitemporal,
				numeroInciso, validoEn);
		final BigDecimal valorPrimaSinIgualacion = contenedorPrimas.getTotalPrimaExternas().add(valorDerechos)
				.multiply(pcteIva.divide(CIEN).add(UNO));

		if (primaAIgualar.compareTo(valorPrimaSinIgualacion) <= 0) {
			throw new NegocioEJBExeption("",
					"Imposible igualar prima. El monto solicitado no cubre al menos las coberturas externas.");
		}
		BigDecimal primaIgualada = primaAIgualar.divide(pcteIva.divide(CIEN,mathCtx).add(UNO),mathCtx);
		primaIgualada = primaIgualada.subtract(valorDerechos);
		primaIgualada = primaIgualada.divide(pcteRecPagoFracc.divide(CIEN).add(UNO),mathCtx);
		BigDecimal comisionMenosBonificacion = BigDecimal.ZERO;
		// calculo comision
//		final Predicate findId = new Predicate() {
//			@Override
//			public boolean evaluate(Object arg0) {
//				BitemporalComision bitemporalComision = (BitemporalComision) arg0;
//				return (bitemporalComision.getContinuity().getCotizacionContinuity().getNumero()
//						.equals(cotizacionContinuty.getNumero())
//						&& bitemporalComision.getContinuity().getSubRamoDTO().getIdTcSubRamo()
//								.equals(CalculoServiceImpl.getIdSubRamoC()) && bitemporalComision.getValue()
//						.getClaveTipoPorcentajeComision().equals((short) 1));
//			}
//		};
//		BitemporalComision comisionCotizacionDTO = (BitemporalComision) CollectionUtils.find(cotizacionContinuty
//				.getComisionContinuities().getInProcess(validoEn), findId);
//		
		
//		CalculoServiceImpl.setIdSubRamoC(cobertura
//						.getContinuity().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
				Predicate findId = new Predicate() {
					@Override
					public boolean evaluate(Object arg0) {
						BitemporalComision obj = (BitemporalComision) arg0;

						return (obj.getEntidadContinuity()
										.getSubRamoDTO()
										.getIdTcSubRamo().intValue() ==
										CalculoServiceImpl
												.getIdSubRamoC().intValue());
					}
				};

				BitemporalComision comisionCotizacionDTO = (BitemporalComision) CollectionUtils.find(cotizacionContinuty
								.getComisionContinuities().getInProcess(validoEn), findId);
	
		if (comisionCotizacionDTO != null) {
			if (cotizacion.getPorcentajebonifcomision() != null) {
				comisionMenosBonificacion = pcteCesionComision.divide(CIEN).multiply(
						comisionCotizacionDTO.getValue().getPorcentajeComisionDefault().divide(CIEN)).
						setScale(16, RoundingMode.HALF_EVEN);		
				
//				comisionMenosBonificacion = pcteCesionComision.multiply(comisionCotizacionDTO.getValue()
//						.getPorcentajeComisionDefault().divide(CIEN, mathCtx));
			}
		}
		if(negocioBono != null && negocioBono.getPctUDI() != null){
			comisionMenosBonificacion = comisionMenosBonificacion.multiply(UNO.subtract(negocioBono.getPctUDI().divide(CIEN))).setScale(16, RoundingMode.HALF_EVEN);
		}
		
		primaIgualada = primaIgualada.divide(UNO.subtract(comisionMenosBonificacion),mathCtx).setScale(16,  RoundingMode.HALF_EVEN);		
		//primaIgualada = primaIgualada.multiply(comisionMenosBonificacion.divide(CIEN, mathCtx).add(UNO));
		
		primaIgualada = primaIgualada.subtract(contenedorPrimas.getTotalPrimaExternas()).setScale(2,  RoundingMode.HALF_UP);
		
		//revisar si existe descuento en la cotizacion y crear los descuentos a las coberturas propias
		if (cotizacion.getIgualacionNivelCotizacion() == null
				|| !cotizacion.getIgualacionNivelCotizacion()
						.booleanValue()) {
			if(tipoEndoso != null && tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
			{
				try{
					descuentoGlobal = 0d; //Dejara de aplicarse el descuento global
				}catch(Exception e){
					e.printStackTrace();
				}	
			}else{
				descuentoGlobal = 0d;//Dejara de aplicarse el descuento global
			}
			//descuentoVolumen = cotizacionBitemporalService.getDescuentoPorVolumenaNivelPoliza(cotizacionBitemporal,validoEn);
			descuentoVolumen = obtenerDescuentoBitemporalInciso(seccionInciso,validoEn).doubleValue();
			if(descuentoGlobal >= 0){
				descuentoSobreCoberturas = descuentoGlobal > (descuentoVolumen != null ? descuentoVolumen
					.doubleValue() : 0d) ? descuentoGlobal
					: descuentoVolumen;
			}else{
				descuentoSobreCoberturas = descuentoGlobal;
			}
		}
		if (descuentoSobreCoberturas != 0) {
			eliminarDescuentosCoberturasPropias = new BigDecimal(1/(1-(descuentoSobreCoberturas/100)));
			descuentoSobreCoberturasPropias = new BigDecimal(descuentoSobreCoberturas/100);
		}
		//Calcula la prima Neta de coberturas propias sin descuentos
		BigDecimal totalCoberturasPropiasSinDescuento = contenedorPrimas.getTotalPrimaPropias().multiply(eliminarDescuentosCoberturasPropias).setScale(2,  RoundingMode.HALF_UP);
		//Calcula el factor para la igualacion de la prima
		BigDecimal descuentoAplicar = primaIgualada.divide(totalCoberturasPropiasSinDescuento,mathCtx);
		//Calcula el porcentaje de descuento a aplicar a las primas sin descuentos, eliminando el posible descuento ya existente
		descuentoAplicar = (((UNO.subtract(descuentoAplicar)).subtract(descuentoSobreCoberturasPropias)).multiply(CIEN)).setScale(4,  RoundingMode.HALF_UP);;
		if(descuentoAplicar.doubleValue() != 0.00d){
			modificarDescuentoCoberturasPropiasEndoso(incisoBitemporal, descuentoAplicar, validoEn);
			
			//Actualiza registro de bitemporalidad
			incisoBitemporal.getValue().setIgualacionNivelInciso(true);
			incisoBitemporal.getValue().setDescuentoIgualacionPrimas(descuentoAplicar.doubleValue());

			entidadBitemporalService.saveInProcess(incisoBitemporal,validoEn);
			
			calcular(incisoBitemporal, validoEn, tipoEndoso, true);
		}
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void restaurarPrimaCotizacion(BigDecimal idToCotizacion) {
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		//List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
		for (IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()) {			
			eliminarDescuentos(inciso);
			eliminarRecargos(inciso);
		}
		cotizacion.setIgualacionNivelCotizacion(Boolean.FALSE);
		cotizacion.setPrimaTotalAntesDeIgualacion(null);
		cotizacion.setDescuentoIgualacionPrimas(null);
		entidadService.save(cotizacion);
		calcular(cotizacion);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void restaurarPrimaEndosoMovimiento(BigDecimal idToCotizacion) {
		ControlEndosoCot cec = this.obtenerControlEndosoCotizacion(idToCotizacion.intValue());
		
		if(cec != null){
			cec.setPrimaTotalIgualar(null);			
			entidadService.save(cec);
		}	
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void restaurarPrimaEndoso(BigDecimal idToCotizacion, DateTime validoEn, Integer tipoEndoso) {
		final BitemporalCotizacion cotizacionBitemporal = entidadBitemporalService.getInProcessByBusinessKey(
				CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion, validoEn);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.cotizacionContinuity.id", cotizacionBitemporal.getContinuity().getId());		
		List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class, 
				params, validoEn, null, true);
		
		//for (BitemporalInciso inciso : cotizacionBitemporal.getContinuity().getIncisoContinuities().getInProcess(validoEn)) {
		for (BitemporalInciso inciso : incisos) {
			if(inciso.getRecordStatus().equals(RecordStatus.TO_BE_ADDED)  && !inciso.getValue().getIgualacionNivelInciso() ){
				eliminarDescuentoCoberturaEndoso(-1l, inciso, validoEn);
				eliminarRecargoCoberturaEndoso(-1l, inciso, validoEn);
			}
		}
		// se manda a calcular.
		calcularEndoso(cotizacionBitemporal, validoEn, tipoEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void restaurarPrimaIncisoEndoso(BitemporalInciso inciso, DateTime validoEn, Integer tipoEndoso, boolean esIgualacion){
		inciso = entidadBitemporalService.getInProcessByKey(IncisoContinuity.class, inciso.getContinuity().getId(), validoEn);
		eliminarDescuentoCoberturaEndoso(-1l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-1l, inciso, validoEn);
		//Se eliminan todos los tipos de descuentos
		eliminarDescuentoCoberturaEndoso(-6l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-6l, inciso, validoEn);
		eliminarDescuentoCoberturaEndoso(-5l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-5l, inciso, validoEn);
		eliminarDescuentoCoberturaEndoso(-8l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-8l, inciso, validoEn);
		eliminarDescuentoCoberturaEndoso(-9l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-9l, inciso, validoEn);		
		
		calcular(inciso, validoEn, tipoEndoso, esIgualacion);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void restaurarPrimaInciso(IncisoCotizacionDTO inciso){
		
		eliminarDescuentoCobertura(-1l, inciso);
		eliminarRecargoCobertura(-1l, inciso);
		calcular(inciso);
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void eliminarDescuentoCobertura(Long idTipoDescuento, IncisoCotizacionDTO inciso) {
		eliminarDescuentoCobertura(Arrays.asList(idTipoDescuento), inciso);
	}
	
	private void eliminarDescuentoCobertura(List<Long> idTipoDescuentos, IncisoCotizacionDTO inciso) {
		if(inciso != null){
			inciso = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId());
			SeccionCotizacionDTO seccionCotizacion = inciso.getSeccionCotizacion();
			eliminarDescuentoCobertura(idTipoDescuentos, seccionCotizacion);
		}
	}
	
	public void eliminarDescuentoCobertura(List<Long> idTipoDescuentos, SeccionCotizacionDTO seccionCotizacion) {
		descuentoCoberturaCotService.bulkDelete(seccionCotizacion.getId()
				.getIdToCotizacion().longValue(), seccionCotizacion.getId()
				.getNumeroInciso().longValue(), seccionCotizacion.getId()
				.getIdToSeccion().longValue(), idTipoDescuentos);
	}
	
	public void eliminarRecargoCobertura(List<Long> idTipoDescuentos, SeccionCotizacionDTO seccionCotizacion) {
		recargoCoberturaCotService.bulkDelete(seccionCotizacion.getId()
				.getIdToCotizacion().longValue(), seccionCotizacion.getId()
				.getNumeroInciso().longValue(), seccionCotizacion.getId()
				.getIdToSeccion().longValue(), idTipoDescuentos);
	}
	
	@Override
	public void eliminarDescuentos(IncisoCotizacionDTO inciso){
		eliminarDescuentoCobertura(Arrays.asList(-1l, -5l, -6l, -7l, -8l, -9l), inciso); 
	}
	
	@Override
	public void eliminarRecargos(IncisoCotizacionDTO inciso){
		eliminarRecargoCobertura(Arrays.asList(-1l, -5l, -6l, -8l, -9l), inciso);
	}
	
	private Double obtieneDescuentoPorEstadoAnterior(BitemporalInciso inciso, DateTime validoEn){
		Double pctDescuentoAnterior = 0.0;		
		BitemporalAutoInciso autoIncisoAnt = inciso.getEntidadContinuity().getAutoIncisoContinuity().getBitemporalProperty().get(validoEn);
		if(autoIncisoAnt != null){
			pctDescuentoAnterior = autoIncisoAnt.getValue().getPctDescuentoEstado();
		}
		return pctDescuentoAnterior;
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void eliminarDescuentoCoberturaEndoso(Long idTipoDescuento, BitemporalInciso inciso, DateTime validoEn) {

		final Collection<BitemporalSeccionInciso> lstBitemporalSeccionInciso = inciso.getContinuity()
				.getIncisoSeccionContinuities().getInProcess(validoEn);

		for (BitemporalSeccionInciso bitemporalSeccionInciso : lstBitemporalSeccionInciso) {
			final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = bitemporalSeccionInciso
					.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
			for (BitemporalCoberturaSeccion bitemporalCoberturaSeccion : lstBitemporalCoberturaSeccion) {
				final CoberturaDTO cobertura = bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO();
				if ("1".equals(cobertura.getCoberturaEsPropia())) {					
					List<CoberturaDescuentoContinuity> seccionContList = (List<CoberturaDescuentoContinuity>) entidadContinuityService.findContinuitiesByParentKey(CoberturaDescuentoContinuity.class, 
							CoberturaDescuentoContinuity.PARENT_KEY_NAME, bitemporalCoberturaSeccion.getContinuity().getId());
					for(CoberturaDescuentoContinuity coberturaDescuentoContinuity : seccionContList){
						BitemporalCoberturaDescuento bitemporalCoberturaDescuento = coberturaDescuentoContinuity.getCoberturaDescuentos().getInProcess(validoEn);
						try{
						if(bitemporalCoberturaDescuento != null && bitemporalCoberturaDescuento.getContinuity().getDescuentoDTO().getIdToDescuentoVario() != null && 
								bitemporalCoberturaDescuento.getContinuity().getDescuentoDTO().getIdToDescuentoVario().longValue() == idTipoDescuento){
								entidadBitemporalService.remove(bitemporalCoberturaDescuento, validoEn);
						}
						}catch(Exception e){
							
						}
					}
					/*
					final Collection<BitemporalCoberturaDescuento> lstBitemporalCoberturaDescuento = bitemporalCoberturaSeccion
					 .getContinuity().getCoberturaDescuentoContinuities().getInProcess(validoEn);
					for (BitemporalCoberturaDescuento bitemporalCoberturaDescuento : lstBitemporalCoberturaDescuento) {
						if(bitemporalCoberturaDescuento.getContinuity().getDescuentoDTO().getIdToDescuentoVario() != null && 
								bitemporalCoberturaDescuento.getContinuity().getDescuentoDTO().getIdToDescuentoVario().longValue() == idTipoDescuento 
								//&& bitemporalCoberturaDescuento.getRecordStatus().equals(RecordStatus.TO_BE_ADDED)
								)
						entidadBitemporalService.remove(bitemporalCoberturaDescuento, validoEn);
					}
					*/
				}
			}

		}
	}

	private void eliminarRecargoCobertura(Long idTipoRecargo, IncisoCotizacionDTO inciso) {
		eliminarRecargoCobertura(Arrays.asList(idTipoRecargo), inciso); 
	}
	
	private void eliminarRecargoCobertura(List<Long> idTipoRecargos, IncisoCotizacionDTO inciso) {
		inciso = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId());
		eliminarRecargoCobertura(idTipoRecargos, inciso.getSeccionCotizacion()); 
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void eliminarRecargoCoberturaEndoso(Long idTipoRecargo, BitemporalInciso inciso, DateTime validoEn) {
		final Collection<BitemporalSeccionInciso> lstBitemporalSeccionInciso = inciso.getContinuity()
				.getIncisoSeccionContinuities().getInProcess(validoEn);

		for (BitemporalSeccionInciso bitemporalSeccionInciso : lstBitemporalSeccionInciso) {
			final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = bitemporalSeccionInciso
					.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
			for (BitemporalCoberturaSeccion bitemporalCoberturaSeccion : lstBitemporalCoberturaSeccion) {
				final CoberturaDTO cobertura = bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO();
				if ("1".equals(cobertura.getCoberturaEsPropia())) {
					List<CoberturaRecargoContinuity> seccionContList = (List<CoberturaRecargoContinuity>) entidadContinuityService.findContinuitiesByParentKey(CoberturaRecargoContinuity.class, 
							CoberturaRecargoContinuity.PARENT_KEY_NAME, bitemporalCoberturaSeccion.getContinuity().getId());
					for(CoberturaRecargoContinuity coberturaRecargoContinuity : seccionContList){
						try{							
							BitemporalCoberturaRecargo bitemporalCoberturaRecargo = coberturaRecargoContinuity.getCoberturaRecargos().getInProcess(validoEn);
							if(bitemporalCoberturaRecargo != null && bitemporalCoberturaRecargo.getContinuity().getRecargoVarioDTO().getIdtorecargovario() != null && 
								  bitemporalCoberturaRecargo.getContinuity().getRecargoVarioDTO().getIdtorecargovario().longValue() == idTipoRecargo){							
							  entidadBitemporalService.remove(bitemporalCoberturaRecargo, validoEn);
							}
						}catch(Exception e){
							
						}
					}
					/*
					for (BitemporalCoberturaRecargo bitemporalCoberturaRecargo : bitemporalCoberturaSeccion
							.getContinuity().getCoberturaRecargoContinuities().getInProcess(validoEn)) {
						//if(bitemporalCoberturaRecargo.getRecordStatus().equals(RecordStatus.TO_BE_ADDED)){
							entidadBitemporalService.remove(bitemporalCoberturaRecargo, validoEn);
						//}
					}*/
				}
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void agregarDescuentoCobertura(Long idTipoDescuento, IncisoCotizacionDTO inciso, BigDecimal descuentoAplicar) {
		List<DescuentoCoberturaCot> toBeSaved = new ArrayList<DescuentoCoberturaCot>(); 
		for (CoberturaCotizacionDTO cobertura : inciso.getSeccionCotizacion().getCoberturaCotizacionLista()) {
			if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia().equals("1")) {
				DescuentoCoberturaCot descuentoCobertura = new DescuentoCoberturaCot();
				DescuentoCoberturaCotId descuentoCoberturaId = new DescuentoCoberturaCotId();
				descuentoCoberturaId.setIdToCobertura(cobertura.getId().getIdToCobertura().longValue());
				descuentoCoberturaId.setIdToCotizacion(cobertura.getId().getIdToCotizacion().longValue());
				descuentoCoberturaId.setIdToSeccion(cobertura.getId().getIdToSeccion().longValue());
				descuentoCoberturaId.setNumeroInciso(cobertura.getId().getNumeroInciso().longValue());
				descuentoCoberturaId.setIdToDescuentoVario(idTipoDescuento); // igualacion prima
				descuentoCobertura.setId(descuentoCoberturaId);
				descuentoCobertura.setValorDescuento(descuentoAplicar);
				descuentoCobertura.setClaveAutorizacion(0l);
				descuentoCobertura.setClaveComercialTecnico((short) 2);
				descuentoCobertura.setClaveContrato(cobertura.getClaveContrato());
				descuentoCobertura.setClaveNivel((short) 3);
				descuentoCobertura.setClaveObligatoriedad((short) 0);
				toBeSaved.add(descuentoCobertura);
			}
		}
		entidadService.saveAll(toBeSaved);
	}
	
	//OJOS - REPLICAR METODO ANTERIOR PERO QUE SEA POR COBERTURA EL ALTA DEL DESCUENTO.
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void agregaDescRecCobertura(Long idTipoDesRec, IncisoCotizacionDTO inciso, NegocioCobPaqSeccion negCobPaqSec, String tipo) {
		if(tipo.equals(TIPO_DESCUENTO)){
			for (CoberturaCotizacionDTO cobertura : inciso.getSeccionCotizacion().getCoberturaCotizacionLista()) {
				if(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().toString().equals(negCobPaqSec.getCoberturaDTO().getIdToCobertura().toString())){
					if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia().equals("1")) {
						DescuentoCoberturaCot descuentoCobertura = new DescuentoCoberturaCot();
						DescuentoCoberturaCotId descuentoCoberturaId = new DescuentoCoberturaCotId();
						descuentoCoberturaId.setIdToCobertura(cobertura.getId().getIdToCobertura().longValue());
						descuentoCoberturaId.setIdToCotizacion(cobertura.getId().getIdToCotizacion().longValue());
						descuentoCoberturaId.setIdToSeccion(cobertura.getId().getIdToSeccion().longValue());
						descuentoCoberturaId.setNumeroInciso(cobertura.getId().getNumeroInciso().longValue());
						descuentoCoberturaId.setIdToDescuentoVario(idTipoDesRec); // igualacion prima
						descuentoCobertura.setId(descuentoCoberturaId);
						descuentoCobertura.setValorDescuento(BigDecimal.valueOf(negCobPaqSec.getDescuento()));
						descuentoCobertura.setClaveAutorizacion(0l);
						descuentoCobertura.setClaveComercialTecnico((short) 2);
						descuentoCobertura.setClaveContrato(cobertura.getClaveContrato());
						descuentoCobertura.setClaveNivel((short) 3);
						descuentoCobertura.setClaveObligatoriedad((short) 0);
						entidadService.save(descuentoCobertura);
					}
				}
			}
		}else if(tipo.equals(TIPO_RECARGO)){
			for (CoberturaCotizacionDTO cobertura : inciso.getSeccionCotizacion().getCoberturaCotizacionLista()) {
				if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia().equals("1") && cobertura.getClaveContrato() == 1) {
					RecargoCoberturaCot recargoCobertura = new RecargoCoberturaCot();
					RecargoCoberturaCotId recargoCoberturaId = new RecargoCoberturaCotId();
					recargoCoberturaId.setIdToCobertura(cobertura.getId().getIdToCobertura().longValue());
					recargoCoberturaId.setIdToCotizacion(cobertura.getId().getIdToCotizacion().longValue());
					recargoCoberturaId.setIdToSeccion(cobertura.getId().getIdToSeccion().longValue());
					recargoCoberturaId.setNumeroInciso(cobertura.getId().getNumeroInciso().longValue());
					recargoCoberturaId.setIdToRecargoVario(idTipoDesRec); // igualacion
																			// prima
					recargoCobertura.setId(recargoCoberturaId);
					recargoCobertura.setValorRecargo(BigDecimal.valueOf(negCobPaqSec.getDescuento()));
					recargoCobertura.setClaveAutorizacion(0l);
					recargoCobertura.setClaveComercialTecnico((short) 2);
					recargoCobertura.setClaveContrato(cobertura.getClaveContrato());
					recargoCobertura.setClaveNivel((short) 3);
					recargoCobertura.setClaveObligatoriedad((short) 0);
					entidadService.save(recargoCobertura);
				}
			}
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void agregarDescuentoCoberturaEndoso(Long idTipoDescuento, BitemporalInciso inciso,
			BigDecimal descuentoAplicar, DateTime validoEn) {

		final Collection<BitemporalSeccionInciso> lstBitemporalSeccionInciso = inciso.getContinuity()
				.getIncisoSeccionContinuities().getInProcess(validoEn);
		
		BitemporalSeccionInciso bitemporalSeccionInciso = lstBitemporalSeccionInciso.iterator().next();

		
			final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = bitemporalSeccionInciso
					.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
			
			//DateTime validoEnAnterior = validoEn.minusDays(1);
			
			for (BitemporalCoberturaSeccion bitemporalCoberturaSeccion : lstBitemporalCoberturaSeccion) {
				final CoberturaDTO cobertura = bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO();
				//Solo agrego descuento global a coberturas nuevas, pues las que ya tienen validez ya tienen el descuento global aplicado
				//Solo se calculan las coberturas nuevas
				//BitemporalCoberturaSeccion anterior = bitemporalCoberturaSeccion.getContinuity().getBitemporalProperty().get(validoEnAnterior);
				//if((bitemporalCoberturaSeccion.getContinuity().getBitemporalProperty().get(validoEn)!=null && idTipoDescuento != -5l) || bitemporalCoberturaSeccion.getContinuity().getBitemporalProperty().get(validoEn) == null || (anterior != null && anterior.getValue().getClaveContrato() == 0)){
					//if ("1".equals(cobertura.getCoberturaEsPropia()) && bitemporalCoberturaSeccion.getValue().getClaveContrato() == 1 && bitemporalCoberturaSeccion.getRecordStatus().equals(
					//		RecordStatus.TO_BE_ADDED)) {
					if ("1".equals(cobertura.getCoberturaEsPropia()) && bitemporalCoberturaSeccion.getValue().getClaveContrato() == 1) {
						DescuentoDTO descuentoDTO = new DescuentoDTO();
						descuentoDTO.setIdToDescuentoVario(new BigDecimal(idTipoDescuento));
						final BitemporalCoberturaDescuento descuentoCobertura = new BitemporalCoberturaDescuento();
						descuentoCobertura.getContinuity().setParentContinuity(bitemporalCoberturaSeccion.getContinuity());
						descuentoCobertura.getContinuity().setDescuentoDTO(descuentoDTO);
						descuentoCobertura.getValue().setValorDescuento(descuentoAplicar);
						descuentoCobertura.getValue().setClaveAutorizacion(Long.valueOf("0"));
						descuentoCobertura.getValue().setClaveComercialTecnico((short) 2);
						descuentoCobertura.getValue().setClaveContrato(
								bitemporalCoberturaSeccion.getValue().getClaveContrato());
						descuentoCobertura.getValue().setClaveNivel((short) 3);
						descuentoCobertura.getValue().setClaveObligatoriedad((short) 0);
						entidadBitemporalService.saveInProcess(descuentoCobertura, validoEn);
					}
				//}
			}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void agregarRecargoCobertura(Long idTipoRecargo, IncisoCotizacionDTO inciso, BigDecimal recargoAplicar) {
		for (CoberturaCotizacionDTO cobertura : inciso.getSeccionCotizacion().getCoberturaCotizacionLista()) {
			if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia().equals("1") && cobertura.getClaveContrato() == 1) {
				RecargoCoberturaCot recargoCobertura = new RecargoCoberturaCot();
				RecargoCoberturaCotId recargoCoberturaId = new RecargoCoberturaCotId();
				recargoCoberturaId.setIdToCobertura(cobertura.getId().getIdToCobertura().longValue());
				recargoCoberturaId.setIdToCotizacion(cobertura.getId().getIdToCotizacion().longValue());
				recargoCoberturaId.setIdToSeccion(cobertura.getId().getIdToSeccion().longValue());
				recargoCoberturaId.setNumeroInciso(cobertura.getId().getNumeroInciso().longValue());
				recargoCoberturaId.setIdToRecargoVario(idTipoRecargo); // igualacion
																		// prima
				recargoCobertura.setId(recargoCoberturaId);
				recargoCobertura.setValorRecargo(recargoAplicar);
				recargoCobertura.setClaveAutorizacion(0l);
				recargoCobertura.setClaveComercialTecnico((short) 2);
				recargoCobertura.setClaveContrato(cobertura.getClaveContrato());
				recargoCobertura.setClaveNivel((short) 3);
				recargoCobertura.setClaveObligatoriedad((short) 0);
				entidadService.save(recargoCobertura);
			}
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void agregarRecargoCoberturaEndoso(Long idTipoRecargo, BitemporalInciso inciso, BigDecimal recargoAplicar,
			DateTime validoEn) {
		final Collection<BitemporalSeccionInciso> lstBitemporalSeccionInciso = inciso.getContinuity()
				.getIncisoSeccionContinuities().getInProcess(validoEn);

		for (BitemporalSeccionInciso bitemporalSeccionInciso : lstBitemporalSeccionInciso) {
			final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = bitemporalSeccionInciso
					.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
			for (BitemporalCoberturaSeccion bitemporalCoberturaSeccion : lstBitemporalCoberturaSeccion) {
				final CoberturaDTO cobertura = bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO();
				//if ("1".equals(cobertura.getCoberturaEsPropia())&& bitemporalCoberturaSeccion.getRecordStatus().equals(
				//		RecordStatus.TO_BE_ADDED)) {
				if ("1".equals(cobertura.getCoberturaEsPropia()) && bitemporalCoberturaSeccion.getValue().getClaveContrato() == 1) {
					final BitemporalCoberturaRecargo recargoCobertura = new BitemporalCoberturaRecargo();
					RecargoVarioDTO recargoVarioDTO = new RecargoVarioDTO();
					recargoVarioDTO.setIdtorecargovario(new BigDecimal(idTipoRecargo));
					recargoCobertura.getContinuity().setParentContinuity(bitemporalCoberturaSeccion.getContinuity());
					recargoCobertura.getContinuity().setRecargoVarioDTO(recargoVarioDTO);
					recargoCobertura.getValue().setValorRecargo(recargoAplicar.abs());
					recargoCobertura.getValue().setClaveAutorizacion(0l);
					recargoCobertura.getValue().setClaveComercialTecnico((short) 2);
					recargoCobertura.getValue().setClaveContrato(bitemporalSeccionInciso.getValue().getClaveContrato());
					recargoCobertura.getValue().setClaveNivel((short) 3);
					recargoCobertura.getValue().setClaveObligatoriedad((short) 0);
					entidadBitemporalService.saveInProcess(recargoCobertura, validoEn);
				}
			}
		}
	}

	private void modificarDescuentoCoberturasPropias(BigDecimal idToCotizacion, BigDecimal descuentoAplicar) {
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		//List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
		for (IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()) {
			modificarDescuentoCoberturasPropias(inciso, descuentoAplicar);
		}
	}

	private void modificarDescuentoCoberturasPropias(IncisoCotizacionDTO inciso, BigDecimal descuentoAplicar) {
		boolean descuento = descuentoAplicar.doubleValue() >= 0;
		descuentoAplicar = descuentoAplicar.abs();
		if (descuento) {
			agregarDescuentoCobertura(-1l, inciso, descuentoAplicar);
		} else {
			agregarRecargoCobertura(-1l, inciso, descuentoAplicar);
		}
	}

	private void modificarDescuentoCoberturasPropiasEndoso(BitemporalInciso inciso, BigDecimal descuentoAplicar,
			DateTime validoEn) {
		boolean descuento = descuentoAplicar.doubleValue() >= 0;
		descuentoAplicar = descuentoAplicar.abs();
		if (descuento) {
			agregarDescuentoCoberturaEndoso(-1l, inciso, descuentoAplicar, validoEn);
		} else {
			agregarRecargoCoberturaEndoso(-1l, inciso, descuentoAplicar, validoEn);
		}
	}

	private void modificarDescuentoCoberturasPropiasEndoso(Long idToCotizacion, BigDecimal descuentoAplicar,
			DateTime validoEn) {
		final BitemporalCotizacion cotizacionBitemporal = entidadBitemporalService.getInProcessByBusinessKey(
				CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion, validoEn);
		for (BitemporalInciso inciso : cotizacionBitemporal.getContinuity().getIncisoContinuities()
				.getInProcess(validoEn)) {
			modificarDescuentoCoberturasPropiasEndoso(inciso, descuentoAplicar, validoEn);
		}
	}
	
	private void sumarPrimasDeCoberturas(IncisoCotizacionDTO inciso, ContenedorPrimas contenedor){
		BigDecimal totalPrimasPropias = BigDecimal.ZERO;
		BigDecimal totalPrimasExternas = BigDecimal.ZERO;
		for (CoberturaCotizacionDTO cobertura : inciso.getSeccionCotizacion().getCoberturaCotizacionLista()) {
			if(cobertura.getClaveContrato().intValue() == 1){//Solo se toman en cuenta coberturas contratadas
				if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia().equals("1")) {
					if (!cobertura.getListaDetallePrimacoberturaCotizacion().isEmpty()) {
						BigDecimal r = new BigDecimal(cobertura.getListaDetallePrimacoberturaCotizacion().get(0)
							.getValorPrimaNetaARDT(), mathCtx);
						totalPrimasPropias = totalPrimasPropias.add(r, mathCtx);
					}
				} else {
					BigDecimal r = new BigDecimal(cobertura.getValorPrimaNeta(), mathCtx);
					totalPrimasExternas = totalPrimasExternas.add(r, mathCtx);
				}
				this.setIdSubRamoC(cobertura.getIdTcSubramo());
			}
		}
		contenedor.setTotalPrimaPropias(contenedor.getTotalPrimaPropias().add(totalPrimasPropias));
		contenedor.setTotalPrimaExternas(contenedor.getTotalPrimaExternas().add(totalPrimasExternas));
		contenedor.setTotalPrimaPropias(contenedor.getTotalPrimaPropias().setScale(2, RoundingMode.HALF_EVEN));
		contenedor.setTotalPrimaExternas(contenedor.getTotalPrimaExternas().setScale(2, RoundingMode.HALF_EVEN));
	}

	private ContenedorPrimas getPrimaNetaCoberturasPropias(BigDecimal idToCotizacion) {		
		ContenedorPrimas primas = new ContenedorPrimas();		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);		
		for (IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()) {
			sumarPrimasDeCoberturas(inciso, primas);			
		}		
		return primas;
	}
	
	private ContenedorPrimas getPrimaNetaCoberturasPropias(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		final ContenedorPrimas result = new ContenedorPrimas();
		final IncisoCotizacionId incisoId = new IncisoCotizacionId(idToCotizacion, numeroInciso);
		final IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoId);
		sumarPrimasDeCoberturas(inciso, result);		
		return result;
	}

	private ContenedorPrimas getPrimaNetaCoberturas(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		
		ContenedorPrimas result = new ContenedorPrimas();
		
		result = calculoDao.getPrimaNetaCoberturas(idToCotizacion, numeroInciso);
		
		this.setIdSubRamoC(result.getIdTcSubramo());
		/*
		final IncisoCotizacionDTO inciso = incisoService.getIncisoCoberturaCotizacionesInicializadas(idToCotizacion, numeroInciso);
		List<CoberturaCotizacionDTO> coberturaCotizacionLista = inciso.getSeccionCotizacion().getCoberturaCotizacionLista();
		for (CoberturaCotizacionDTO cobertura : coberturaCotizacionLista) {			
			BigDecimal r = new BigDecimal(cobertura.getValorPrimaNeta(), mathCtx);
			if (cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia().equals("1")) {
				totalPrimasPropias = totalPrimasPropias.add(r, mathCtx);				
			} else {				
				totalPrimasExternas = totalPrimasExternas.add(r, mathCtx);
			}
			this.setIdSubRamoC(cobertura.getIdTcSubramo());
		}
		result.setTotalPrimaPropias(totalPrimasPropias.setScale(2, RoundingMode.HALF_EVEN));
		result.setTotalPrimaExternas(totalPrimasExternas.setScale(2, RoundingMode.HALF_EVEN));	
		*/
		return result;
	}
	
	private ContenedorPrimas getPrimaNetaCoberturasPropiasEndoso(BitemporalCotizacion cotizacionBitemporal,	DateTime validoEn) {
		ContenedorPrimas primas = new ContenedorPrimas();
		BigDecimal totalPrimaPropias = new BigDecimal(0, mathCtx);
		BigDecimal totalPrimaExternas = new BigDecimal(0, mathCtx);
		for (BitemporalInciso inciso : cotizacionBitemporal.getContinuity().getIncisoContinuities()
				.getInProcess(validoEn)) {
			for (BitemporalSeccionInciso seccion : inciso.getContinuity().getIncisoSeccionContinuities()
					.getInProcess(validoEn)) {
				for (BitemporalCoberturaSeccion cobertura : seccion.getContinuity().getCoberturaSeccionContinuities()
						.getInProcess(validoEn)) {
					if (cobertura.getContinuity().getCoberturaDTO().getCoberturaEsPropia().equals("1") && cobertura.getValue().getClaveContrato() == 1) {
//						final Collection<BitemporalCoberturaDetallePrima> lstDetallePrima = cobertura.getContinuity()
//								.getCoberturaDetallePrimaContinuities().getInProcess(validoEn);
//						if (CollectionUtils.isNotEmpty(lstDetallePrima)) {
//							// totalPrimaPropias +=
//							// lstDetallePrima.iterator().next().getValue().getValorPrimaNetaARDT();
							BigDecimal r = new BigDecimal(cobertura.getValue().getValorPrimaNeta(), mathCtx);
							totalPrimaPropias = totalPrimaPropias.add(r, mathCtx);
							this.setIdSubRamoC(cobertura.getContinuity().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
						//}
					} else {
						// totalPrimaExternas +=
						// cobertura.getValue().getValorPrimaNeta();
						if(cobertura.getValue().getClaveContrato() == 1){
							BigDecimal r = new BigDecimal(cobertura.getValue().getValorPrimaNeta(), mathCtx);
							totalPrimaExternas = totalPrimaExternas.add(r, mathCtx);
							this.setIdSubRamoC(cobertura.getContinuity().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
						}
					}
				}
			}
		}
		primas.setTotalPrimaExternas(totalPrimaExternas);
		primas.setTotalPrimaPropias(totalPrimaPropias);
		return primas;
	}

	private ContenedorPrimas getPrimaNetaCoberturasPropiasEndoso(BitemporalCotizacion cotizacionBitemporal,
			Integer numeroInciso, DateTime validoEn) {
		ContenedorPrimas primas = new ContenedorPrimas();
		BigDecimal totalPrimaPropias = new BigDecimal(0, mathCtx);
		BigDecimal totalPrimaExternas = new BigDecimal(0, mathCtx);
		/*
		 * BitemporalInciso inciso =
		 * entidadBitemporalService.getInProcessByBusinessKey
		 * (IncisoContinuity.class, IncisoContinuity.PARENT_KEY_NAME,
		 * cotizacionBitemporal.getContinuity().getId(), validoEn);
		 */		
		final CotizacionContinuity cotizacionContinuty = cotizacionBitemporal.getContinuity();

		BitemporalInciso inciso = incisoBitemporalService.findBitemporalIncisoByNumero(cotizacionContinuty, numeroInciso, validoEn, TimeUtils.now(), true);


		for (BitemporalSeccionInciso seccion : inciso.getContinuity().getIncisoSeccionContinuities()
				.getInProcess(validoEn)) {
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("continuity.seccionIncisoContinuity.id", seccion.getContinuity().getId());		
			List<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = (List<BitemporalCoberturaSeccion>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalCoberturaSeccion.class, 
					params, validoEn, null, true);
			
			if(lstBitemporalCoberturaSeccion == null || lstBitemporalCoberturaSeccion.isEmpty()){
				lstBitemporalCoberturaSeccion = (List<BitemporalCoberturaSeccion>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalCoberturaSeccion.class, 
						params, validoEn, null, false);			
			}
			
			//for (BitemporalCoberturaSeccion cobertura : seccion.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn)) {
			for (BitemporalCoberturaSeccion cobertura : lstBitemporalCoberturaSeccion) {
				if (cobertura.getContinuity().getCoberturaDTO().getCoberturaEsPropia().equals("1") && cobertura.getValue().getClaveContrato() == 1) {
//					final Collection<BitemporalCoberturaDetallePrima> lstDetallePrima = cobertura.getContinuity()
//							.getCoberturaDetallePrimaContinuities().getInProcess(validoEn);
//					if (CollectionUtils.isNotEmpty(lstDetallePrima)) {
//						// totalPrimaPropias +=
//						// lstDetallePrima.iterator().next().getValue().getValorPrimaNetaARDT();
//						BigDecimal r = new BigDecimal(lstDetallePrima.iterator().next().getValue()
//								.getValorPrimaNetaARDT(), mathCtx);
						BigDecimal r = new BigDecimal(cobertura.getValue().getValorPrimaNeta(), mathCtx);
						totalPrimaPropias = totalPrimaPropias.add(r, mathCtx);
						this.setIdSubRamoC(cobertura.getContinuity().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
					//}
				} else {
					// totalPrimaExternas +=
					// cobertura.getValue().getValorPrimaNeta();
					if(cobertura.getValue().getClaveContrato() == 1){
						BigDecimal r = new BigDecimal(cobertura.getValue().getValorPrimaNeta(), mathCtx);
						totalPrimaExternas = totalPrimaExternas.add(r, mathCtx);
						this.setIdSubRamoC(cobertura.getContinuity().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
					}
				}
			}
		}
		primas.setTotalPrimaExternas(totalPrimaExternas);
		primas.setTotalPrimaPropias(totalPrimaPropias);
		return primas;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public IncisoCotizacionDTO calcular(IncisoCotizacionDTO incisoCotizacionDTO) {
		int diasVigencia = 365;
		double descuentoSobreCoberturas = 0.0d;
		double descuentoGlobal = 0.0d;
		double descuentoOriginacion = 0.0d;
		Double descuentoVolumen = null;
		Double descuentoPorEstado = 0.0d;
		if (incisoCotizacionDTO != null && incisoCotizacionDTO.getIncisoAutoCot() != null) {
			SeccionCotizacionDTO seccionCotizacion = incisoCotizacionFacade.getSeccionCotizacionDTO(incisoCotizacionDTO.getId().getIdToCotizacion(), 
					incisoCotizacionDTO.getId().getNumeroInciso());
			CotizacionDTO cotizacion = seccionCotizacion.getIncisoCotizacionDTO().getCotizacionDTO();
			IncisoAutoCot auto = incisoCotizacionDTO.getIncisoAutoCot();

			//Obtiene informacion Zona Circulacion
			String claveZonaCircualcion = calculoDao.getZonaCirculacion(seccionCotizacion.getId().getIdToSeccion(), Integer.valueOf(auto.getEstadoId()), Integer.valueOf(auto.getMunicipioId()), cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			//Obtiene informacion de Tarifas
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion = negocioTarifaService
					.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(
							BigDecimal.valueOf(auto.getNegocioSeccionId()),
							cotizacion.getIdMoneda());

			TarifaAgrupadorTarifa tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService
					.getPorNegocioAgrupadorTarifaSeccion(negocioAgrupadorTarifaSeccion);
			//Informacion del Tipo de Servicio Default
			TipoUsoVehiculoDTO tipoUsoVehiculoDTO = entidadDao.findById(TipoUsoVehiculoDTO.class,BigDecimal.valueOf(auto.getTipoUsoId()));
			NegocioTipoServicio negocioTipoServicio = new NegocioTipoServicio();
			BigDecimal idTcTipoServicioVehiculo = null;
			try {
				negocioTipoServicio = negocioTipoServicioDao.getDefaultByNegSeccionEstiloVehiculo(BigDecimal.valueOf(auto.getNegocioSeccionId()), tipoUsoVehiculoDTO.getIdTcTipoVehiculo());
				if (negocioTipoServicio != null) {
					idTcTipoServicioVehiculo = negocioTipoServicio.getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo();
				}
			} catch(Exception e) {
				LOG.error(e.getMessage(), e);
			}
			if (idTcTipoServicioVehiculo == null) {
				ServVehiculoLinNegTipoVehId servVehiculoLinNegTipoVehId = new ServVehiculoLinNegTipoVehId();
				servVehiculoLinNegTipoVehId.setIdTcTipoVehiculo(tipoUsoVehiculoDTO.getIdTcTipoVehiculo().longValue());
				servVehiculoLinNegTipoVehId.setIdToSeccion(seccionCotizacion.getId().getIdToSeccion().longValue());
				ServVehiculoLinNegTipoVeh servVehiculoLinNegTipoVehDefault = entidadDao.findById(ServVehiculoLinNegTipoVeh.class, servVehiculoLinNegTipoVehId);
				idTcTipoServicioVehiculo = new BigDecimal(servVehiculoLinNegTipoVehDefault.getIdTcTipoServicioVehiculo());
			}

			diasVigencia = Days.daysBetween(new DateTime(cotizacion.getFechaInicioVigencia()), new DateTime(cotizacion.getFechaFinVigencia())).getDays();

			CoberturaDTO coberturaDTO = null;
			//se requiere ordenar para el calculo
			Collections.sort(seccionCotizacion.getCoberturaCotizacionLista(), new Comparator<CoberturaCotizacionDTO>() {
				@Override
				public int compare(CoberturaCotizacionDTO n1, CoberturaCotizacionDTO n2) {
					return n1
							.getCoberturaSeccionDTO()
							.getCoberturaDTO()
							.getNumeroSecuencia()
							.compareTo(n2.getCoberturaSeccionDTO().getCoberturaDTO().getNumeroSecuencia());
				}
			});
			//revisar si existe descuento en la cotizacion y crear los descuentos a las coberturas propias
			if(cotizacion.getIgualacionNivelCotizacion() == null || !cotizacion.getIgualacionNivelCotizacion().booleanValue()){
				descuentoGlobal = 0d;//Dejara de aplicarse el descuento global			
				descuentoVolumen = cotizacionService.getDescuentoPorVolumenaNivelPoliza(cotizacion);	
				if(descuentoGlobal >= 0){
					descuentoSobreCoberturas = descuentoGlobal > (descuentoVolumen != null ? descuentoVolumen.doubleValue() : 0d) ? 
						descuentoGlobal : descuentoVolumen;
				}else{
					descuentoSobreCoberturas = descuentoGlobal;	
				}
			}
			
			eliminarDescuentoCobertura(Arrays.asList(-5l, -7l, -6l, -8l, -9l), seccionCotizacion);
			eliminarRecargoCobertura(Arrays.asList(-5l,-6l, -8l, -9l), seccionCotizacion);
			
			if(descuentoSobreCoberturas > 0){
				agregarDescuentoCobertura(-5l, seccionCotizacion.getIncisoCotizacionDTO(), 
						BigDecimal.valueOf(descuentoSobreCoberturas));
			} else if(descuentoSobreCoberturas < 0){
				agregarRecargoCobertura(-5l, seccionCotizacion.getIncisoCotizacionDTO(), 
						BigDecimal.valueOf(descuentoSobreCoberturas).abs());
			}
			

			descuentoOriginacion = cotizacion.getDescuentoOriginacion() != null ? cotizacion.getDescuentoOriginacion().doubleValue() : 0d;
			
			if(descuentoOriginacion > 0 && (cotizacion.getIgualacionNivelCotizacion() == null || !cotizacion.getIgualacionNivelCotizacion())
				&& 	(incisoCotizacionDTO.getIgualacionNivelInciso() == null || !incisoCotizacionDTO.getIgualacionNivelInciso())){
				agregarDescuentoCobertura(-7l, seccionCotizacion.getIncisoCotizacionDTO(), 
						BigDecimal.valueOf(descuentoOriginacion));
			}			
			
			//OJOS - CONSULTAR COBERTURAS Y DESCUENTOS DE RC RELACIONADAS A LOS FILTROS DE CONFIGURACIÓN 
			MotorDecisionDTO filtrosMotorDecisionDto = this.obtenerFiltrosMotor(incisoCotizacionDTO.getCotizacionDTO().getIdToCotizacion(), incisoCotizacionDTO.getId().getNumeroInciso());
			
			List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = motorDecisionAtributosService.consultaValoresService(filtrosMotorDecisionDto);
			if (!resultadoBusqueda.isEmpty()){
				List<NegocioCobPaqSeccion> resultado = (List<NegocioCobPaqSeccion>)(Object)resultadoBusqueda.get(0).getValores();
				
				if(!resultado.isEmpty()){
					for(NegocioCobPaqSeccion negCobPaqSec : resultado){
						if(negCobPaqSec.getDescuento() > 0){
							agregaDescRecCobertura(-5l, seccionCotizacion.getIncisoCotizacionDTO(), negCobPaqSec, TIPO_DESCUENTO);
						}else if(negCobPaqSec.getDescuento() < 0){
							agregaDescRecCobertura(-5l, seccionCotizacion.getIncisoCotizacionDTO(), negCobPaqSec, TIPO_RECARGO);
						}
					}
				}
			}
			
			//revisar si existe descuento por estado y crear los descuentos a las coberturas propias
			//si se hizo una igualacion entonces no volver a aplicar este descuento ya que se incluye en la igualacion
			if(cotizacion.getIgualacionNivelCotizacion() == null || !cotizacion.getIgualacionNivelCotizacion()){
				if (incisoCotizacionDTO.getIgualacionNivelInciso() == null || !incisoCotizacionDTO.getIgualacionNivelInciso()) {
					descuentoPorEstado = incisoCotizacionDTO.getIncisoAutoCot().getPctDescuentoEstado() == null ? 0.0 :
					incisoCotizacionDTO.getIncisoAutoCot().getPctDescuentoEstado();
					// descuentos recargos x codigo postal, solo cuando no exista una igualacion de primas
					this.agregarDescuentoRecargoCoberturaCodigPostal(seccionCotizacion.getIncisoCotizacionDTO());
				}
			}
			
			if(descuentoPorEstado > 0) {
				agregarDescuentoCobertura(-6l, seccionCotizacion.getIncisoCotizacionDTO(), 
						BigDecimal.valueOf(descuentoPorEstado));
			} else if(descuentoPorEstado < 0){
				agregarRecargoCobertura(-6l, seccionCotizacion.getIncisoCotizacionDTO(), 
						BigDecimal.valueOf(descuentoPorEstado).abs());
			}
			
			for (CoberturaCotizacionDTO coberturaCotizacionDTO: seccionCotizacion.getCoberturaCotizacionLista()) {
				if (coberturaCotizacionDTO.getClaveContrato().intValue() == 1) {
					String[] estilo = auto.getEstiloId().split("_");
					coberturaDTO = coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO();
					//Calculo si es responsabilidad civil viajero
					if(coberturaDTO.getNombreComercial().toUpperCase().equals(CoberturaDTO.NOMBRE_LIMITE_RC_VIAJERO.toUpperCase())){
						Double sumaAsegurada = calculoSumaAseguradaDSMVGDF(coberturaCotizacionDTO.getDiasSalarioMinimo(), auto.getEstiloId());						
						coberturaCotizacionDTO.setValorSumaAsegurada(sumaAsegurada);
					}
					
					coberturaCotizacionDTO = calculoDao.executeStoreProcedure(cotizacion.getIdToCotizacion(), cotizacion.getIdMoneda().shortValue(), cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto(),
							incisoCotizacionDTO.getId().getNumeroInciso().longValue(),seccionCotizacion.getId().getIdToSeccion(), tarifaAgrupadorTarifa.getId().getIdToAgrupadorTarifa(), tarifaAgrupadorTarifa.getId().getIdVerAgrupadorTarifa(),
					tarifaAgrupadorTarifa.getId().getIdVertarifa(), estilo[1], auto.getModeloVehiculo().longValue(), claveZonaCircualcion, BigDecimal.valueOf(auto.getTipoUsoId()),idTcTipoServicioVehiculo,
					auto.getPaquete().getId(),auto.getModificadoresPrima(),auto.getModificadoresDescripcion(),coberturaDTO.getClaveFuenteSumaAsegurada(), 
							diasVigencia, coberturaCotizacionDTO);
				} else {
					coberturaCotizacionDTO.setValorPrimaNeta(new Double(0.0));
					if (!coberturaCotizacionDTO.getListaDetallePrimacoberturaCotizacion().isEmpty()) {
						entidadService.removeAll(coberturaCotizacionDTO.getListaDetallePrimacoberturaCotizacion());
						coberturaCotizacionDTO.getListaDetallePrimacoberturaCotizacion().clear();
					}
				}
			}
			
			entidadService.saveAll(seccionCotizacion.getCoberturaCotizacionLista());
		}
		return incisoCotizacionDTO;
	}
	
	private Double calculoSumaAseguradaDSMVGDF(Integer diasSalarioMinimo, String estiloId){
		
		Double sumaAsegurada = 0.0;
		//Obtiene dias de salario minimo
		try{
			CatalogoValorFijoId idDSMVGDF = new CatalogoValorFijoId();
			idDSMVGDF.setIdDato(1);
			idDSMVGDF.setIdGrupoValores(CatalogoValorFijoDTO.IDGRUPO_CLAVE_TIPO_DSMGVDF);
			CatalogoValorFijoDTO dsmvgdf = entidadService.findById(CatalogoValorFijoDTO.class, idDSMVGDF);
			
			sumaAsegurada = diasSalarioMinimo.doubleValue() * Double.valueOf(dsmvgdf.getDescripcion());
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return sumaAsegurada;
	}

	/**
	 * Metodo que permite calcular  a las condiciones que se encuentran almacenadas
	 * para un grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * el paquet, estado, municipio, cotizacion, moneda, numero de secuencia (inciso),
	 * clave de estilo vehiculo, modelo vehiculo, tipo de uso vehiculo, modificadores de prima y
	 * modificadores de descripcion.
	 * 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCotizacion
	 * @param idMoneda
	 * @param numeroSecuenciaInciso
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUso
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @return List<CoberturaCotizacionDTO>
	 */
	@Override
	public List<CoberturaCotizacionDTO> calculoOnFly(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion, List<CoberturaCotizacionDTO> coberturas) {
		int diasVigencia = 365;
		List<CoberturaCotizacionDTO> coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>();

		//Obtiene informacion Zona Circulacion

		String claveZonaCircualcion = calculoDao.getZonaCirculacion(idToSeccion, Integer.valueOf(idEstado), Integer.valueOf(idMunicipio), IdNegocio);

		//Obtiene informacion de Tarifas
		CotizacionDTO cotizacion = entidadDao.findById(CotizacionDTO.class, idCotizacion);
		List<NegocioSeccion> negocioSeccionList = negocioSeccionService.getSeccionListByCotizacion(cotizacion);
		BigDecimal idToNegSeccion = null;
		for (NegocioSeccion item : negocioSeccionList) {
			if (item.getSeccionDTO().getIdToSeccion().equals(idToSeccion)) {
				idToNegSeccion = item.getIdToNegSeccion();
				break;
			}
		}
		NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, cotizacion.getIdMoneda());
		TarifaAgrupadorTarifa tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService.getPorNegocioAgrupadorTarifaSeccion(negocioAgrupadorTarifaSeccion);
		

		//Informacion del Tipo de Servicio Default
		TipoUsoVehiculoDTO tipoUsoVehiculoDTO = entidadDao.findById(TipoUsoVehiculoDTO.class,tipoUso);
		NegocioTipoServicio negocioTipoServicio = new NegocioTipoServicio();
		BigDecimal idTcTipoServicioVehiculo = null;
		try {
			negocioTipoServicio = negocioTipoServicioDao.getDefaultByNegSeccionEstiloVehiculo(idToNegSeccion, tipoUsoVehiculoDTO.getIdTcTipoVehiculo());
			idTcTipoServicioVehiculo = negocioTipoServicio.getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo();
		} catch(Exception e) {

		}
		if (idTcTipoServicioVehiculo == null) {
			ServVehiculoLinNegTipoVehId servVehiculoLinNegTipoVehId = new ServVehiculoLinNegTipoVehId();
			servVehiculoLinNegTipoVehId.setIdTcTipoVehiculo(tipoUsoVehiculoDTO.getIdTcTipoVehiculo().longValue());
			servVehiculoLinNegTipoVehId.setIdToSeccion(idToSeccion.longValue());
			ServVehiculoLinNegTipoVeh servVehiculoLinNegTipoVehDefault = entidadDao.findById(ServVehiculoLinNegTipoVeh.class, servVehiculoLinNegTipoVehId);
			idTcTipoServicioVehiculo = new BigDecimal(servVehiculoLinNegTipoVehDefault.getIdTcTipoServicioVehiculo());
		}

		diasVigencia = Days.daysBetween(new DateTime(cotizacion.getFechaInicioVigencia()), new DateTime(cotizacion.getFechaFinVigencia())).getDays();

		//se requiere ordenar para el calculo
		Collections.sort(coberturas, new Comparator<CoberturaCotizacionDTO>() {
			@Override
			public int compare(CoberturaCotizacionDTO n1,
					CoberturaCotizacionDTO n2) {
				return n1
						.getCoberturaSeccionDTO()
						.getCoberturaDTO()
						.getNumeroSecuencia()
						.compareTo(
								n2.getCoberturaSeccionDTO().getCoberturaDTO()
										.getNumeroSecuencia());
			}
		});

		for (CoberturaCotizacionDTO coberturaCotizacionDTO: coberturas) {
			if (coberturaCotizacionDTO.getClaveContrato().intValue() == 1) {
				coberturaCotizacionList.add(calculoDao.executeStoreProcedure(idCotizacion, idMoneda, IdNegocio, idToProducto,
				numeroInciso,idToSeccion, tarifaAgrupadorTarifa.getId().getIdToAgrupadorTarifa(), tarifaAgrupadorTarifa.getId().getIdVerAgrupadorTarifa(),
				tarifaAgrupadorTarifa.getId().getIdVertarifa(), claveEstilo, modeloVehiculo, claveZonaCircualcion, tipoUso, idTcTipoServicioVehiculo,
				idPaquete,modificadoresPrima, modificadoresDescripcion,coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada(), 
						diasVigencia, coberturaCotizacionDTO));
			} else {
				coberturaCotizacionDTO.setValorPrimaNeta(new Double(0.0));
				coberturaCotizacionList.add(coberturaCotizacionDTO);
			}
		}

		return coberturaCotizacionList;
	}

	@Override
	public ResumenCostosDTO obtenerResumen(IncisoCotizacionDTO incisoCotizacionDTO) {
		CotizacionDTO cotizacion = entidadDao.findById(CotizacionDTO.class, incisoCotizacionDTO.getCotizacionDTO().getIdToCotizacion());
		BigDecimal porcentajeRecargoPagoFraccionado = getPorcentajeRecargoFraccionado(cotizacion);
		incisoCotizacionDTO.setCotizacionDTO(cotizacion);
		return calcularResumenCostosInciso(incisoCotizacionDTO, porcentajeRecargoPagoFraccionado);
	}

	/**
	 * Obtner porcentaje de recargo fraccionado que corresponde a la cotizacion
	 * @param cotizacion
	 * @return
	 */
	public BigDecimal getPorcentajeRecargoFraccionado(CotizacionDTO cotizacion){
		BigDecimal porcentajeRecargoPagoFraccionado = null;
		if (cotizacion.getSolicitudDTO().getNegocio() == null || cotizacion.getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado() == null || 
				!cotizacion.getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado().booleanValue()) {
			porcentajeRecargoPagoFraccionado = null;
		} else {
			porcentajeRecargoPagoFraccionado = cotizacion.getPorcentajePagoFraccionado() != null ? 
					BigDecimal.valueOf(cotizacion.getPorcentajePagoFraccionado().doubleValue()) : null;
		}
		if (porcentajeRecargoPagoFraccionado == null && cotizacion.getIdFormaPago() != null && cotizacion.getIdMoneda() != null) {
			Double pcte = listadoService.getPctePagoFraccionado(cotizacion.getIdFormaPago().intValue(), 
					cotizacion.getIdMoneda().shortValue());			
			porcentajeRecargoPagoFraccionado = pcte != null ? BigDecimal.valueOf(pcte) : BigDecimal.valueOf(0);

		}
		if (porcentajeRecargoPagoFraccionado != null) {
			porcentajeRecargoPagoFraccionado = porcentajeRecargoPagoFraccionado.divide(BigDecimal.valueOf(100), 4, BigDecimal.ROUND_HALF_UP);
		}
		return porcentajeRecargoPagoFraccionado;
	}

	private ResumenCostosDTO calcularResumenCostosInciso(IncisoCotizacionDTO incisoCotizacionDTO, BigDecimal porcentajeRecargoPagoFraccionado){		
		//List<CoberturaCotizacionDTO> coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>();
		//SeccionCotizacionDTO seccionCotizacionDTO = null;
		NegocioDerechoPoliza negocioDerechoPoliza = null;

		//NegocioBonoComision negocioBonoComision = null;
		/*
		if(incisoCotizacionDTO.getCotizacionDTO() != null){
			//Se obtiene el negocioBonoComision del negocio que tiene la solicitud.
			negocioBonoComision = incisoCotizacionDTO.getCotizacionDTO().getSolicitudDTO().getNegocio().getNegocioBonoComision();
		}*/
		incisoCotizacionDTO = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionDTO.getId());
		//seccionCotizacionDTO = incisoCotizacionDTO.getSeccionCotizacion();
		/*
		if (seccionCotizacionDTO != null) {
			coberturaCotizacionList = seccionCotizacionDTO.getCoberturaCotizacionLista();
		}*/
		
		ResumenCostosDTO resumenCostosDTO = new ResumenCostosDTO();

		BigDecimal sumaValorPrimaNetaCoberturas = BigDecimal.ZERO;
		
		BigDecimal comisionSobreRecargo = BigDecimal.ZERO;
		// Descuento global
		BigDecimal totalPrimasNetasCoberturas = BigDecimal.ZERO;
		BigDecimal comisionCedida = BigDecimal.ZERO;
		Double porcentajeIva = 0d;
		BigDecimal sobreComision = BigDecimal.ZERO;
		BigDecimal primaTotal = BigDecimal.ZERO;
		// Descuento global total
		BigDecimal descuento = BigDecimal.ZERO;
		Double derechos = 0d;
		BigDecimal comision = BigDecimal.ZERO;
		BigDecimal recargo = BigDecimal.ZERO;
		BigDecimal iva = BigDecimal.ZERO;
		ContenedorPrimas contenedorPrimas = null;
		final BigDecimal idToCotizacion = incisoCotizacionDTO.getId().getIdToCotizacion();
		BigDecimal pctBonificacionComision = null;
		BigDecimal pctComision = BigDecimal.ZERO;
		
		CotizacionDTO cotizacionDTO = incisoCotizacionDTO.getCotizacionDTO();
		pctBonificacionComision = new BigDecimal(cotizacionDTO.getPorcentajebonifcomision(), mathCtx);

		if (cotizacionDTO.getPorcentajeIva() != null) {
			porcentajeIva = cotizacionDTO.getPorcentajeIva();
		}
		if (cotizacionDTO.getNegocioDerechoPoliza() != null) {
			negocioDerechoPoliza = cotizacionDTO.getNegocioDerechoPoliza();
			// Metodo para calcular los derechos repartidos entre coberturas propias, como no aplica para guardar en seycos aqui no es relevante
			derechos = negocioDerechoPoliza.getImporteDerecho();
		}

		contenedorPrimas = getPrimaNetaCoberturas(idToCotizacion, incisoCotizacionDTO.getId().getNumeroInciso());		
		totalPrimasNetasCoberturas = contenedorPrimas.getTotalPrimaExternas().add(contenedorPrimas.getTotalPrimaPropias());
		
		//calculo comision
		Predicate findId = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				ComisionCotizacionDTO id = (ComisionCotizacionDTO)arg0; 
				return (id.getId().getIdToCotizacion().equals(idToCotizacion) && id.getId().getIdTcSubramo().equals(CalculoServiceImpl.getIdSubRamoC()) 
							&& id.getId().getTipoPorcentajeComision().equals((short) 1)) ;
			}
		};
		ComisionCotizacionDTO comisionCotizacionDTO  = (ComisionCotizacionDTO) CollectionUtils.find(cotizacionDTO.getComisionCotizacionDTOs(), findId);		
		if (comisionCotizacionDTO != null ) {
			pctComision = comisionCotizacionDTO.getPorcentajeComisionDefault();
		}
		BigDecimal valorComision = totalPrimasNetasCoberturas.multiply(pctComision.divide(CIEN, mathCtx), mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		/*
		if(negocioBonoComision != null && negocioBonoComision.getPctUDI() != null){
			valorComision = valorComision.multiply(BigDecimal.ONE.subtract((negocioBonoComision.getPctUDI().divide(CIEN, mathCtx)))).setScale(16, RoundingMode.HALF_EVEN);
		}*/
		//comisionCedida = totalPrimasNetasCoberturas.multiply(comisionMenosBonificacion.divide(CIEN, mathCtx), mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		//comisionCedida = totalPrimasNetasCoberturas.multiply(comisionMenosBonificacion.divide(CIEN, mathCtx), mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		comisionCedida = valorComision.multiply(pctBonificacionComision.divide(CIEN, mathCtx), mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		sumaValorPrimaNetaCoberturas = totalPrimasNetasCoberturas.subtract(comisionCedida);
		
		
		
		/*1 total de primas = prima neta de coberturas - descuento*/
		// resumenCostosDTO.setTotalPrimas(sumaValorPrimaNetaCoberturas
		// - (comisionCedida + descuento));
		/*1 total de primas = suma de las primas netas de todas la coberturas */
		resumenCostosDTO.setTotalPrimas(totalPrimasNetasCoberturas.setScale(16, RoundingMode.HALF_EVEN).doubleValue());

		/*2 suma de primas netas de las coberturas sean propias*/
		resumenCostosDTO.setPrimaNetaConberturasPropias(contenedorPrimas.getTotalPrimaPropias().setScale(16, RoundingMode.HALF_EVEN).doubleValue());

		/*3 prima neta de coberturas propias * % descto global (cotizacion.porcentajeDescuentoGlobal)*/

		//descuento = sumaValorPrimaNetaCoberturasPropias * (cotizacionDTO.getPorcentajeDescuentoGlobal() /100);
		resumenCostosDTO.setDescuento(descuento.setScale(16, RoundingMode.HALF_EVEN).doubleValue());

		/*Desc /comisi�n cedida = Total Primas * porcentaje de comisi�n cedida (cotizaciondto.porcentajebonifcomision, regla de tres con ComisionCotizacionDTO.PorcentajeComisionDefault)*/
		//resumenCostosDTO.setDescuentoComisionCedida(comisionCedida.add(descuento).setScale(2, RoundingMode.HALF_EVEN).doubleValue());
		resumenCostosDTO.setDescuentoComisionCedida(comisionCedida.add(descuento).setScale(16, RoundingMode.HALF_EVEN).doubleValue());

		/*4 suma de las primas netas de todas la coberturas */ 
		resumenCostosDTO.setPrimaNetaCoberturas(sumaValorPrimaNetaCoberturas.setScale(16, RoundingMode.HALF_EVEN).doubleValue());

		/*recargo = prima neta coberturas * % de recargo de la forma de pago (FormaPagoDN.getPorId)*/
		recargo = sumaValorPrimaNetaCoberturas.multiply(porcentajeRecargoPagoFraccionado, mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		
		/*
		BigDecimal valorComisionRPF = recargo.multiply((pctComision.divide(CIEN, mathCtx)), mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		if(negocioBonoComision != null && negocioBonoComision.getPctUDI() != null){
			valorComisionRPF = valorComisionRPF.multiply(BigDecimal.ONE.subtract(negocioBonoComision.getPctUDI().divide(CIEN, mathCtx))).setScale(16, RoundingMode.HALF_EVEN);
		}
		BigDecimal valorBonificacionComisionRPF = valorComisionRPF.multiply(pctBonificacionComision.divide(CIEN, mathCtx), mathCtx).setScale(16, RoundingMode.HALF_EVEN);
		recargo = recargo.subtract(valorBonificacionComisionRPF).setScale(16, RoundingMode.HALF_EVEN);
		*/
		//recargo = new BigDecimal(sumaValorPrimaNetaCoberturas - comisionCedida).multiply(porcentajeRecargoPagoFraccionado).doubleValue();
		resumenCostosDTO.setRecargo(recargo.setScale(16, RoundingMode.HALF_EVEN).doubleValue());
		resumenCostosDTO.setDerechos(derechos);
		/*Listado de montos de iva validos*/
		resumenCostosDTO.setValorIvaSeleccionado(porcentajeIva.intValue());
		
		/*iva = (prima neta de cotización + recargos + derechos) * % del iva*/
		iva = (sumaValorPrimaNetaCoberturas.add(recargo).add(BigDecimal.valueOf(derechos))).multiply(BigDecimal.valueOf(porcentajeIva).divide(CIEN, mathCtx).setScale(16, RoundingMode.HALF_EVEN), mathCtx);
		//iva = (( resumenCostosDTO.getPrimaNetaCoberturas() + recargo + derechos) * (porcentajeIva / 100));
		resumenCostosDTO.setIva(iva.setScale(16, RoundingMode.HALF_EVEN).doubleValue());

		/*Prima total = prima neta de coberturas + recargos + derechos + iva (cotizacionDTO.porcentajeIva)*/
		primaTotal = sumaValorPrimaNetaCoberturas.add(recargo).add(BigDecimal.valueOf(derechos)).add(iva);
		//primaTotal = resumenCostosDTO.getPrimaNetaCoberturas() + recargo + derechos + iva;
		resumenCostosDTO.setPrimaTotal(primaTotal.setScale(16, RoundingMode.HALF_EVEN).doubleValue());
		resumenCostosDTO.setSobreComision(sobreComision.setScale(16, RoundingMode.HALF_EVEN).doubleValue());
		/*comisi�n = (Prima Neta de Coberturas - sobrecomisi�n ) * % de comisi�n (ComisionCotizacionDTO.PorcentajeComisionDefault)*/
		resumenCostosDTO.setComision(comision.setScale(16, RoundingMode.HALF_EVEN).doubleValue());
		/*comisi�n sobre recargo = comisi�n * % de recargo de la forma de pago (FormaPagoDN.getPorId)*/
		resumenCostosDTO.setComisionSobreRecargo(comisionSobreRecargo.setScale(16, RoundingMode.HALF_EVEN).doubleValue());
		return resumenCostosDTO;
	}
	/**
	 * JFGG
	 * @param cotizacion
	 * @param esComplementar
	 * @return ResumenCostosDTO
	 */
	@Override
	public ResumenCostosDTO obtenerResumenCotizacion(CotizacionDTO cotizacion, Boolean esComplementar) {
		ResumenCostosDTO resp = null;
		boolean statusExcepcion = false;
		resp = obtenerResumenCotizacion(cotizacion, esComplementar, statusExcepcion);
		resp.setStatusExcepcion(false);
		resp.setShowExcepcion(false);
		resp.setMensajeExcepcion("");
		return resp;
	}
	/**
	 * JFGG
	 * @param cotizacion
	 * @param esComplementar
	 * @param statusExcepcion
	 * @return ResumenCostosDTO
	 */
	@Override
	public ResumenCostosDTO obtenerResumenCotizacion(CotizacionDTO cotizacion, Boolean esComplementar, boolean statusExcepcion) {
		ResumenCostosDTO resumenCostosDTO = new ResumenCostosDTO();
		BigDecimal porcentajeRecargoPagoFraccionado = null;
		if (cotizacion != null && cotizacion.getIdToCotizacion() != null) {
			CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
			if(!esComplementar || cotizacionDTO.getValorIva() == null){				
				cotizacion.setPorcentajeIva(cotizacionDTO.getPorcentajeIva());
				entidadService.refresh(cotizacionDTO);
				porcentajeRecargoPagoFraccionado = getPorcentajeRecargoFraccionado(cotizacionDTO);
				for (IncisoCotizacionDTO incisoCotizacionDTOs : cotizacionDTO.getIncisoCotizacionDTOs()) {
					ResumenCostosDTO resumenCostosDTOTemp = calcularResumenCostosInciso(incisoCotizacionDTOs, porcentajeRecargoPagoFraccionado);
					if(statusExcepcion){//JFGG
						resumenCostosDTO.setPrimaNetaCoberturas(0.00);
						resumenCostosDTO.setTotalPrimas(0.00);
						resumenCostosDTO.setDescuentoComisionCedida(0.00);
						resumenCostosDTO.setDescuento(0.00);
						resumenCostosDTO.setRecargo(0.00);
						resumenCostosDTO.setDerechos(0.00);
						resumenCostosDTO.setValorIvaSeleccionado(0);
						resumenCostosDTO.setIva(0.00);
						resumenCostosDTO.setPrimaTotal(0.00);
					} else {//JFGG
						resumenCostosDTO.setPrimaNetaCoberturas(resumenCostosDTO.getPrimaNetaCoberturas() + resumenCostosDTOTemp.getPrimaNetaCoberturas());
						resumenCostosDTO.setTotalPrimas(resumenCostosDTO.getTotalPrimas() + resumenCostosDTOTemp.getTotalPrimas());
						resumenCostosDTO.setDescuentoComisionCedida(resumenCostosDTO.getDescuentoComisionCedida() + resumenCostosDTOTemp.getDescuentoComisionCedida());
						resumenCostosDTO.setDescuento(resumenCostosDTO.getDescuento() + resumenCostosDTOTemp.getDescuento());
						resumenCostosDTO.setRecargo(resumenCostosDTO.getRecargo() + resumenCostosDTOTemp.getRecargo());
						resumenCostosDTO.setDerechos(resumenCostosDTO.getDerechos() + resumenCostosDTOTemp.getDerechos());
						resumenCostosDTO.setValorIvaSeleccionado(resumenCostosDTOTemp.getValorIvaSeleccionado());
						resumenCostosDTO.setIva(resumenCostosDTO.getIva()+resumenCostosDTOTemp.getIva());
						resumenCostosDTO.setPrimaTotal(resumenCostosDTO.getPrimaTotal() + resumenCostosDTOTemp.getPrimaTotal());
					}//JFGG
				//*******************se recupera el valor de PrimaNetaConberturasPropias lo debe de revisar Lizandro******************************************
					resumenCostosDTO.setPrimaNetaConberturasPropias(resumenCostosDTOTemp.getPrimaNetaConberturasPropias());
				//*********************************************************************************************************
				}
				//Guarda prima total en cotizacion
				cotizacionDTO.setValorPrimaTotal(new BigDecimal(resumenCostosDTO.getPrimaTotal()));
				cotizacionDTO.setValorTotalPrimas(new BigDecimal(resumenCostosDTO.getTotalPrimas()));
				cotizacionDTO.setValorComisionCedida(new BigDecimal(resumenCostosDTO.getDescuentoComisionCedida()));
				cotizacionDTO.setValorRecargo(new BigDecimal(resumenCostosDTO.getRecargo()));
				cotizacionDTO.setValorDerechos(new BigDecimal(resumenCostosDTO.getDerechos()));
				cotizacionDTO.setValorIva(new BigDecimal(resumenCostosDTO.getIva()));
			
				if(CotizacionDTO.TIPO_COTIZACION_AUTOPLAZO.equals(cotizacionDTO.getTipoCotizacion())){
					cotizacionDTO.setPrimaTarifa(cotizacionDTO.getValorPrimaTotal().doubleValue());	
				}
				
				if (resumenCostosDTO.getIvaList() != null && resumenCostosDTO.getValorIvaSeleccionado() != null) {
					for (Integer valor  : resumenCostosDTO.getIvaList()) {
						if (resumenCostosDTO.getValorIvaSeleccionado().intValue() == 0) {
							cotizacionDTO.setPorcentajeIva(Double.valueOf(valor));
							break;
						}
						if (valor.intValue() == resumenCostosDTO.getValorIvaSeleccionado().intValue()) {
							cotizacionDTO.setPorcentajeIva(Double.valueOf(valor));
							break;
						}
					}
				}
				entidadService.save(cotizacionDTO);
			}else{
				if(statusExcepcion){//JFGG
					resumenCostosDTO.setPrimaNetaCoberturas(0.00);
					resumenCostosDTO.setTotalPrimas(0.00);
					resumenCostosDTO.setDescuentoComisionCedida(0.00);
					resumenCostosDTO.setDescuento(0.00);
					resumenCostosDTO.setRecargo(0.00);
					resumenCostosDTO.setDerechos(0.00);
					resumenCostosDTO.setValorIvaSeleccionado(0);
					resumenCostosDTO.setIva(0.00);
					resumenCostosDTO.setPrimaTotal(0.00);
				} else {//JFGG
					resumenCostosDTO.setPrimaNetaCoberturas(cotizacionDTO.getValorTotalPrimas().doubleValue() - cotizacionDTO.getValorComisionCedida().doubleValue());
					resumenCostosDTO.setTotalPrimas(cotizacionDTO.getValorTotalPrimas().doubleValue());
					resumenCostosDTO.setDescuentoComisionCedida(cotizacionDTO.getValorComisionCedida().doubleValue());
					//resumenCostosDTO.setDescuento(0.0);
					resumenCostosDTO.setRecargo(cotizacionDTO.getValorRecargo().doubleValue());
					resumenCostosDTO.setDerechos(cotizacionDTO.getValorDerechos().doubleValue());
					resumenCostosDTO.setValorIvaSeleccionado(cotizacionDTO.getPorcentajeIva().intValue());
					resumenCostosDTO.setIva(cotizacionDTO.getValorIva().doubleValue());
					resumenCostosDTO.setPrimaTotal(cotizacionDTO.getValorPrimaTotal().doubleValue());
				}//JFGG
			}
		} else {
			resumenCostosDTO.setPrimaNetaCoberturas(resumenCostosDTO.getPrimaNetaCoberturas());
			resumenCostosDTO.setTotalPrimas(resumenCostosDTO.getTotalPrimas());
			resumenCostosDTO.setDescuentoComisionCedida(resumenCostosDTO.getDescuentoComisionCedida());
			resumenCostosDTO.setRecargo(resumenCostosDTO.getRecargo());
			resumenCostosDTO.setDerechos(resumenCostosDTO.getDerechos());
			resumenCostosDTO.setValorIvaSeleccionado(resumenCostosDTO.getValorIvaSeleccionado());
			resumenCostosDTO.setPrimaTotal(resumenCostosDTO.getPrimaTotal());
		}
		if (resumenCostosDTO.getValorIvaSeleccionado() == null && cotizacion != null && cotizacion.getPorcentajeIva() != null) {
			if(statusExcepcion){//JFGG
				resumenCostosDTO.setValorIvaSeleccionado(0);
			} else {
				resumenCostosDTO.setValorIvaSeleccionado(cotizacion.getPorcentajeIva().intValue());
			}
		}

		return resumenCostosDTO;
	}


	@Override
	public ResumenCostosGeneralDTO obtenerResumenGeneral(
			CotizacionDTO cotizacion) {
		ResumenCostosGeneralDTO resumen = new ResumenCostosGeneralDTO();
		ResumenCostosDTO caratula = new ResumenCostosDTO();
		Map<BigDecimal, ResumenCostosDTO> incisos = new HashMap<BigDecimal, ResumenCostosDTO>(1);
		cotizacion = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
		BigDecimal porcentajeRecargoPagoFraccionado = getPorcentajeRecargoFraccionado(cotizacion);
		for (IncisoCotizacionDTO incisoCotizacionDTOs : cotizacion.getIncisoCotizacionDTOs()) {
			ResumenCostosDTO resumenInciso = calcularResumenCostosInciso(incisoCotizacionDTOs, porcentajeRecargoPagoFraccionado);
			incisos.put(incisoCotizacionDTOs.getId().getNumeroInciso(), resumenInciso);
			caratula.setPrimaNetaCoberturas(caratula.getPrimaNetaCoberturas() + resumenInciso.getPrimaNetaCoberturas());
			caratula.setTotalPrimas(caratula.getTotalPrimas() + resumenInciso.getTotalPrimas());
			caratula.setDescuentoComisionCedida(caratula.getDescuentoComisionCedida() + resumenInciso.getDescuentoComisionCedida());
			caratula.setRecargo(caratula.getRecargo() + resumenInciso.getRecargo());
			caratula.setDerechos(caratula.getDerechos() + resumenInciso.getDerechos());
			caratula.setValorIvaSeleccionado(resumenInciso.getValorIvaSeleccionado());
			caratula.setIva(caratula.getIva()+resumenInciso.getIva());
			caratula.setPrimaTotal(caratula.getPrimaTotal() + resumenInciso.getPrimaTotal());
		}
		resumen.setCaratula(caratula);
		resumen.setIncisos(incisos);
		return resumen;
	}
	
	@Override
	public ResumenCostosGeneralDTO obtenerResumenGeneral(
			CotizacionDTO cotizacion, Boolean esComplementar) {
		ResumenCostosGeneralDTO resumen = new ResumenCostosGeneralDTO();
		ResumenCostosDTO resumenCostos = this.obtenerResumenCotizacion(cotizacion, esComplementar);
		resumen.setCaratula(resumenCostos);
		return resumen;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BitemporalInciso calcular(BitemporalInciso inciso, DateTime validoEn, Integer tipoEndoso, boolean esIgualacion) {
		
		int diasVigencia;
		if (inciso != null && inciso.getEntidadContinuity().getAutoIncisoContinuity() != null) {

			//Se establece el ultimo inciso en proceso (como un refresh) para que ahi se acumule la prima de las coberturas calculadas
			inciso = inciso.getEntidadContinuity().getBitemporalProperty().getInProcess(validoEn);
			BitemporalCotizacion cotizacion = inciso.getEntidadContinuity().getParentContinuity().getBitemporalProperty().getInProcess(validoEn);

			BitemporalSeccionInciso seccionInciso = inciso.getEntidadContinuity().getIncisoSeccionContinuities()
					.getInProcess(validoEn).iterator().next();

			BitemporalAutoInciso autoInciso = inciso.getEntidadContinuity().getAutoIncisoContinuity().getBitemporalProperty().getInProcess(validoEn);

			//Obtiene informacion Zona Circulacion
			String claveZonaCircualcion = calculoDao.getZonaCirculacion(seccionInciso.getEntidadContinuity().getSeccion().getIdToSeccion(), Integer.valueOf(autoInciso.getValue().getEstadoId()), Integer.valueOf(autoInciso.getValue().getMunicipioId()), cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());

			//Obtiene informacion de Tarifas
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion = negocioTarifaService
					.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(
							BigDecimal.valueOf(autoInciso.getValue().getNegocioSeccionId()),
							new BigDecimal(cotizacion.getValue().getMoneda().getIdTcMoneda()));

			TarifaAgrupadorTarifa tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService
					.getPorNegocioAgrupadorTarifaSeccion(negocioAgrupadorTarifaSeccion);

			//Informacion del Tipo de Servicio Default
			TipoUsoVehiculoDTO tipoUsoVehiculoDTO = entidadDao.findById(TipoUsoVehiculoDTO.class,BigDecimal.valueOf(autoInciso.getValue().getTipoUsoId()));
			NegocioTipoServicio negocioTipoServicio = new NegocioTipoServicio();
			BigDecimal idTcTipoServicioVehiculo = null;
			try {
				negocioTipoServicio = negocioTipoServicioDao.getDefaultByNegSeccionEstiloVehiculo(BigDecimal.valueOf(autoInciso.getValue().getNegocioSeccionId()), tipoUsoVehiculoDTO.getIdTcTipoVehiculo());
				idTcTipoServicioVehiculo = negocioTipoServicio.getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo();
			} catch(Exception e) {
				LOG.error(e.getMessage(), e);
			}
			if (idTcTipoServicioVehiculo == null) {
				ServVehiculoLinNegTipoVehId servVehiculoLinNegTipoVehId = new ServVehiculoLinNegTipoVehId();
				servVehiculoLinNegTipoVehId.setIdTcTipoVehiculo(tipoUsoVehiculoDTO.getIdTcTipoVehiculo().longValue());
				servVehiculoLinNegTipoVehId.setIdToSeccion(seccionInciso.getEntidadContinuity().getSeccion().getIdToSeccion().longValue());			
				ServVehiculoLinNegTipoVeh servVehiculoLinNegTipoVehDefault = entidadDao.findById(ServVehiculoLinNegTipoVeh.class, servVehiculoLinNegTipoVehId);
				idTcTipoServicioVehiculo = new BigDecimal(servVehiculoLinNegTipoVehDefault.getIdTcTipoServicioVehiculo());
			}

			List<BitemporalCoberturaSeccion> coberturas = (List<BitemporalCoberturaSeccion>)seccionInciso.getEntidadContinuity()
				.getCoberturaSeccionContinuities().getInProcess(validoEn);

			//Se ordenan
			Collections.sort(coberturas, new Comparator<BitemporalCoberturaSeccion>() {
				@Override
		            public int compare(BitemporalCoberturaSeccion n1,
		                       BitemporalCoberturaSeccion n2) {
					return n1.getContinuity().getCoberturaDTO().getNumeroSecuencia()
							.compareTo(n2.getContinuity().getCoberturaDTO().getNumeroSecuencia());
				}
			});
			
			//El manejos de descuento en endosos se manejara de la siguiente manera -- LPV 20150601
			/*
			 * Debido a la entrada del descuento por estado; este sera el unico que se aplicara en los endosos
			 * Alta de inciso: se manejara el descuento asignado en el alta
			 * Movimientos: no se eliminara ni se asignara ningun descuento nuevo en movimientos a menos que el descuento por estado haya cambiado
			 * del que tenia anteriormente y como resultado se eliminarán todos los descuentos previos en el inciso para evitar descuentos dobles en el calculo
			 * 
			 * En caso de que el movimiento sea de igualacion se presume que el metodo de igualacion hizo los ajustes correspondientes a los descuentos
			 */			
			//revisar si existe descuento por estado
			if ((inciso.getValue().getIgualacionNivelInciso() == null || !inciso.getValue().getIgualacionNivelInciso()) && !esIgualacion) {

				Double descuentoPorEstado = autoInciso.getValue().getPctDescuentoEstado() != null ? autoInciso.getValue().getPctDescuentoEstado() : 0.0d;

				Double descuentoPorEstadoAnt = 0.0;
				if(tipoEndoso != null && tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS){
					descuentoPorEstadoAnt = obtieneDescuentoPorEstadoAnterior(inciso, validoEn);
				}


				if(!descuentoPorEstado.equals(descuentoPorEstadoAnt)){
					eliminarDescuentoCoberturaEndoso(-5l, inciso, validoEn); //Borra el descuento existente para prevenir que se acumulen registros repetidos.
					eliminarRecargoCoberturaEndoso(-5l, inciso, validoEn);

					eliminarDescuentoCoberturaEndoso(-6l, inciso, validoEn);
					eliminarRecargoCoberturaEndoso(-6l, inciso, validoEn);

					eliminarDescuentoCoberturaEndoso(-1l, inciso, validoEn);
					eliminarRecargoCoberturaEndoso(-1l, inciso, validoEn);

					if(descuentoPorEstado > 0) {
						agregarDescuentoCoberturaEndoso(-6l, inciso, BigDecimal.valueOf(descuentoPorEstado), validoEn);
					} else if(descuentoPorEstado < 0){
						agregarRecargoCoberturaEndoso(-6l, inciso, BigDecimal.valueOf(descuentoPorEstado), validoEn);
					}
				}


				try {
					this.agregarDescuentoRecargoCoberturaCodigPostal(inciso,
							validoEn,
							autoInciso.getValue().getCodigoPostal(),
							seccionInciso.getEntidadContinuity().getSeccion().getIdToSeccion()
							);
				} catch (Exception e) {
					LOG.info("Ocurrio  error en agregarDescuentoRecargoCoberturaEndosoCodigoPostal", e);
				}
			}
			CoberturaDTO coberturaDTO = null;
			Long idAgrupadorTarifas = null;
			Long versionAgrupadorTarifas = null;
			List<BitemporalCoberturaSeccion> coberturaHistory = null;
			List<BitemporalCoberturaSeccion> coberturaEvolution = null;
			boolean obtenerAgrupadorDeCobertura = false;

			//Busca version agrupador de coberturas originales
			if(tipoEndoso != null && tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS){
				List<BitemporalCoberturaSeccion> coberturasOriginales = new ArrayList(entidadBitemporalService.findInProcessByParentKey(CoberturaSeccionContinuity.class, 
						CoberturaSeccionContinuity.PARENT_KEY_NAME, seccionInciso.getEntidadContinuity().getId(), validoEn));

				for (BitemporalCoberturaSeccion coberturaOri: coberturasOriginales) {
					if (coberturaOri.getValue().getAgrupadorTarifaId() != null
							&& coberturaOri.getValue().getAgrupadorTarifaId().intValue() != 0) {
						idAgrupadorTarifas = coberturaOri.getValue().getAgrupadorTarifaId().longValue();
						versionAgrupadorTarifas = coberturaOri.getValue().getVerAgrupadorTarifaId().longValue();
						break;
					}
				}

			}


			for (BitemporalCoberturaSeccion cobertura: coberturas) {
				obtenerAgrupadorDeCobertura = false;
				if (cobertura.getValue().getClaveContrato().intValue() == 1) {
					if(cobertura.getRecordInterval()==null){

						String[] estilo = autoInciso.getValue().getEstiloId().split("_");
						coberturaDTO = cobertura.getEntidadContinuity().getCoberturaDTO();

						coberturaHistory = cobertura.getEntidadContinuity().getBitemporalProperty().getHistory();

						if (coberturaHistory != null && !coberturaHistory.isEmpty()) {
							for (BitemporalCoberturaSeccion cobHistory : coberturaHistory) {
								if (cobHistory.getValue().getAgrupadorTarifaId() != null
										&& cobHistory.getValue().getAgrupadorTarifaId().intValue() != 0) {
									idAgrupadorTarifas = cobHistory.getValue().getAgrupadorTarifaId().longValue();
									versionAgrupadorTarifas = cobHistory.getValue().getVerAgrupadorTarifaId().longValue();
									obtenerAgrupadorDeCobertura = true;
									break;
								}
							}
						}

						if (!obtenerAgrupadorDeCobertura) {
							diasVigencia = Days.daysBetween(cobertura.getValidityInterval().getInterval().getStart(),
									TimeUtils.getDateTime(cotizacion.getValue().getFechaFinVigencia())).getDays();
						} else {

							coberturaEvolution = cobertura.getEntidadContinuity().getBitemporalProperty().getEvolution(validoEn);
							if(coberturaEvolution.isEmpty()){	
								coberturaEvolution = cobertura.getEntidadContinuity().getBitemporalProperty().getHistory(validoEn);
							}

							Collections.sort(coberturaEvolution, new Comparator(){
								public int compare(Object o1, Object o2) {
									return ((BitemporalCoberturaSeccion)o1).getId().compareTo(((BitemporalCoberturaSeccion)o2).getId());
								}
							});

							diasVigencia = Days.daysBetween(coberturaEvolution.get(0).getValidityInterval().getInterval().getStart(),
									TimeUtils.getDateTime(cotizacion.getValue().getFechaFinVigencia())).getDays();

						}

						if (!obtenerAgrupadorDeCobertura && (idAgrupadorTarifas == null || versionAgrupadorTarifas == null)) {
							idAgrupadorTarifas = tarifaAgrupadorTarifa.getId().getIdToAgrupadorTarifa();
							versionAgrupadorTarifas = tarifaAgrupadorTarifa.getId().getIdVerAgrupadorTarifa();
						}

						//Calculo si es responsabilidad civil viajero
						if(coberturaDTO.getNombreComercial().toUpperCase().equals(CoberturaDTO.NOMBRE_LIMITE_RC_VIAJERO.toUpperCase())){
							Double sumaAsegurada = calculoSumaAseguradaDSMVGDF(cobertura.getValue().getDiasSalarioMinimo(), autoInciso.getValue().getEstiloId());						
							cobertura.getValue().setValorSumaAsegurada(sumaAsegurada);
						}

						BitemporalCoberturaSeccion coberturaRespuesta = calculoDao.calculaCoberturaBitemporal(new BigDecimal(cotizacion.getId()), 
								new BigDecimal(inciso.getId()), new BigDecimal(seccionInciso.getId()), new BigDecimal(cobertura.getId()), diasVigencia, 
								cotizacion.getValue().getMoneda().getIdTcMoneda(), cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), 
								cotizacion.getValue().getSolicitud().getProductoDTO().getIdToProducto(), inciso.getEntidadContinuity().getNumero().longValue(), 
								seccionInciso.getEntidadContinuity().getSeccion().getIdToSeccion(), 
								idAgrupadorTarifas,	versionAgrupadorTarifas, Long.parseLong(estilo[2]), estilo[1], 
								autoInciso.getValue().getModeloVehiculo().longValue(), claveZonaCircualcion, BigDecimal.valueOf(autoInciso.getValue().getTipoUsoId()),
								idTcTipoServicioVehiculo, autoInciso.getValue().getPaquete().getId(),autoInciso.getValue().getModificadoresPrima(),
								autoInciso.getValue().getModificadoresDescripcion(),coberturaDTO.getClaveFuenteSumaAsegurada(), cobertura);

						cobertura.getValue().setValorPrimaNeta(coberturaRespuesta.getValue().getValorPrimaNeta());
					}
				} else {
					cobertura.getValue().setValorPrimaNeta(new Double(0.0));
				}
			}
		}

		return inciso;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public ControlEndosoCot obtenerControlEndosoCotizacion(Integer numeroCotizacion){
		ControlEndosoCot controlEndosoCot = null;

		String query = "SELECT model FROM ControlEndosoCot model WHERE model.cotizacionId ="
			+ numeroCotizacion
			+ " AND model.claveEstatusCot IN("+ CotizacionDTO.ESTATUS_COT_EN_PROCESO +","+ CotizacionDTO.ESTATUS_COT_TERMINADA +")";

		List<ControlEndosoCot> controlEndosoCotList = entidadService.executeQueryMultipleResult(query, null);

		if (!controlEndosoCotList.isEmpty() && controlEndosoCotList.get(0) != null) {
			controlEndosoCot = controlEndosoCotList.get(0);
		}

		return controlEndosoCot;
	}
	
	 /**
	   * @deprecated No usar para desarrollos futuros.
	   */
	@Deprecated
	public static BigDecimal getIdSubRamoC() {
		return idSubRamoC.get();
	}
	
	/**
	   * @deprecated No usar para desarrollos futuros.
	   */
	@Deprecated
	public void setIdSubRamoC(BigDecimal idSubRamoC) {
		CalculoServiceImpl.idSubRamoC.set(idSubRamoC);
	}
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;
	 
	private EntidadBitemporalService entidadBitemporalService;	

	@EJB
	 public void setEntidadBitemporalService(
			 EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}

	 
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;

	@EJB
	 public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	private MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;

	@EJB
	public void setMovimientoEndosoBitemporalService(
			MovimientoEndosoBitemporalService movimientoEndosoBitemporalService) {
		this.movimientoEndosoBitemporalService = movimientoEndosoBitemporalService;
	}

	@EJB
    public void setEntidadContinuityService(EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}	
	
	@Override
	public Double obtenerPrimaTotalAltaInciso(BitemporalInciso inciso,
			CotizacionContinuity cotizacionContinuity, Date validoEn, short tipoEndoso) {

		Double valorPrimaTotal = 0.0;
		NegocioDerechoEndoso derechosEndoso = null;
		double comisionCedida = 0.0;
		double totalPrimasNetas = 0.0;
		Double porcentajeIva = new Double(0.0);		
		Double porcentajeBonificacionComision = new Double(0.0);
		Double bonificacionComisionRPF = new Double(0.0);
		
		Double comisionCedidaRPF = new Double(0.0);
		Double derechos = new Double(0.0);
		Double iva = new Double(0.0);
		Double recargo = new Double(0.0);
		Double primaNetaDeCotizacion = new Double(0.0);


		Collection<BitemporalCoberturaSeccion> coberturasSeccion = new ArrayList<BitemporalCoberturaSeccion>();
		Collection<BitemporalComision> comisiones = new ArrayList<BitemporalComision>();

		BitemporalCotizacion cotizacion = entidadBitemporalService.getByKey(
				CotizacionContinuity.class, cotizacionContinuity.getId());
		
		if(cotizacion == null && validoEn != null){
			cotizacion = entidadBitemporalService.getByKey(CotizacionContinuity.class, cotizacionContinuity.getId(), TimeUtils.getDateTime(validoEn));
		}		

		if (cotizacion != null) {
			
			ControlEndosoCot controlEndosoCot = obtenerControlEndosoCotizacion(cotizacion
					.getContinuity().getNumero());

			List<MovimientoEndoso> movimientos = emisionEndosoBitemporalService
					.getMovimientos(controlEndosoCot);
			
			NegocioBonoComision negocioBono = new NegocioBonoComision();
			try{
				Long idToNegocio = cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio();
				List<NegocioBonoComision> negocioBonoComisionList = entidadService.findByProperty(NegocioBonoComision.class, "negocio.idToNegocio", idToNegocio);
				if(negocioBonoComisionList != null && !negocioBonoComisionList.isEmpty()){
					negocioBono = negocioBonoComisionList.get(0);
				}
			}catch(Exception e){
			}

			BitemporalSeccionInciso seccionInciso = inciso
					.getEntidadContinuity().getIncisoSeccionContinuities()
					.getInProcess(TimeUtils.getDateTime(validoEn))
					.iterator().next();

			if (seccionInciso != null) {
				coberturasSeccion = seccionInciso
						.getEntidadContinuity()
						.getCoberturaSeccionContinuities()
						.getInProcess(TimeUtils.getDateTime(validoEn));
			}
			
			comisiones = cotizacion.getEntidadContinuity().getComisionContinuities().getInProcessOrGet(TimeUtils.getDateTime(validoEn), true);
			
			if (cotizacion.getValue().getPorcentajeIva() != null) {
				porcentajeIva = cotizacion.getValue().getPorcentajeIva();
			}
			
			if (cotizacion.getValue().getPorcentajebonifcomision() != null) {
				porcentajeBonificacionComision = cotizacion.getValue().getPorcentajebonifcomision();
			}
			incisoModificado = false;
			for (BitemporalCoberturaSeccion cobertura : coberturasSeccion) {
				if(cobertura.getValue().getClaveContrato()==1){
					double comisionCedidaParcial = 0.0;
					double porcentajeComision = 0.0;
					double recargoParcial = 0.0;
					double comisionRPFParcial = 0.0;
				
					Double primaNetaCoberturaSeccion = 0.0;
						
					
					if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
					{
						if (movimientos != null && movimientos.contains(cobertura)) {
							primaNetaCoberturaSeccion = movimientos.get(
									movimientos.indexOf(cobertura))
									.getValorMovimiento().doubleValue();
							derechosEndoso = movimientoEndosoBitemporalService.getNegocioDerechoEndoso(cotizacion.getContinuity().getBitemporalProperty().getInProcess(TimeUtils.getDateTime(validoEn)));
							derechos = derechosEndoso.getImporteDerecho();
						}else{
							if(movimientos == null)
							return valorPrimaTotal;
						}	
						
					}
					else
					{
						primaNetaCoberturaSeccion = cobertura.getValue().getValorPrimaNeta();		
						derechos =  cotizacion.getValue().getValorDerechosUsuario();
					}				
			
					totalPrimasNetas = totalPrimasNetas + primaNetaCoberturaSeccion;
	
				
					this.setIdSubRamoC(cobertura
							.getContinuity().getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
					Predicate findId = new Predicate() {
						@Override
						public boolean evaluate(Object arg0) {
							BitemporalComision obj = (BitemporalComision) arg0;
	
							return (obj.getEntidadContinuity()
											.getSubRamoDTO()
											.getIdTcSubRamo().intValue() ==
											CalculoServiceImpl
													.getIdSubRamoC().intValue());
						}
					};
	
					BitemporalComision comisionCotizacionDTO = (BitemporalComision) CollectionUtils
							.find(comisiones, findId);
					if (comisionCotizacionDTO != null) {
						porcentajeComision = comisionCotizacionDTO
						.getValue().getPorcentajeComisionDefault()
						.doubleValue();
					}
					comisionCedidaParcial = primaNetaCoberturaSeccion * (porcentajeComision / 100);
					if(negocioBono != null && negocioBono.getPctUDI() != null){
						comisionCedidaParcial = comisionCedidaParcial* (1 - (negocioBono.getPctUDI().doubleValue() / 100));
					}
					comisionCedida = comisionCedida + comisionCedidaParcial;	
					recargoParcial = primaNetaCoberturaSeccion *(cotizacion.getValue().getPorcentajePagoFraccionado()/100);
					recargo = recargo + recargoParcial;
					comisionRPFParcial = recargoParcial * (porcentajeComision / 100);
					if(negocioBono != null && negocioBono.getPctUDI() != null){
						comisionRPFParcial = comisionRPFParcial * (1 - (negocioBono.getPctUDI().doubleValue() / 100));
					}
					comisionCedidaRPF = comisionCedidaRPF + comisionRPFParcial;
				}
				
				
			}
			comisionCedida = comisionCedida * (porcentajeBonificacionComision/100);
			
			bonificacionComisionRPF = comisionCedidaRPF * (porcentajeBonificacionComision/100);
			primaNetaDeCotizacion = totalPrimasNetas - comisionCedida;
			
			recargo = recargo - bonificacionComisionRPF;
						
			iva = (( primaNetaDeCotizacion  + derechos + recargo) * (porcentajeIva / 100));
	
			valorPrimaTotal = primaNetaDeCotizacion + derechos + recargo  + iva;
		}

		return valorPrimaTotal;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void generarCoberturasInProcessParaIgualacionPrimas(Long incisoContinuityId, DateTime validoEn)
	{		
		BitemporalInciso incisoBitemporal = entidadBitemporalService
				.getInProcessByKey(IncisoContinuity.class, incisoContinuityId,
						validoEn);	
		
        // Se crean los in process en caso de no existir
		if (incisoBitemporal.getRecordStatus().equals(
				RecordStatus.NOT_IN_PROCESS)) 
		{
			incisoBitemporal.getValue().setIgualacionNivelInciso(true);
			incisoBitemporal = entidadBitemporalService.saveInProcess(incisoBitemporal,validoEn);
		}	
		/*
		final Collection<BitemporalSeccionInciso> lstBitemporalSeccionInciso = incisoBitemporal
				.getContinuity().getIncisoSeccionContinuities()
				.getInProcess(validoEn);
				*/
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.incisoContinuity.id", incisoBitemporal.getContinuity().getId());		
		List<BitemporalSeccionInciso> lstBitemporalSeccionInciso = (List<BitemporalSeccionInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalSeccionInciso.class, 
				params, validoEn, null, true);
		
		if(lstBitemporalSeccionInciso == null || lstBitemporalSeccionInciso.isEmpty()){
			lstBitemporalSeccionInciso = (List<BitemporalSeccionInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalSeccionInciso.class, 
					params, validoEn, null, false);			
		}
			
		BitemporalSeccionInciso bitemporalSeccionInciso = (BitemporalSeccionInciso)lstBitemporalSeccionInciso.iterator().next();
		// Se crean los in process en caso de no existir
		if (bitemporalSeccionInciso.getRecordStatus().equals(RecordStatus.NOT_IN_PROCESS)) {
			bitemporalSeccionInciso = (BitemporalSeccionInciso) entidadBitemporalService.saveInProcess(entidadBitemporalService
					.getInProcessByKey(SeccionIncisoContinuity.class, bitemporalSeccionInciso.getEntidadContinuity().getId(),
							validoEn), validoEn);			
		}
		
		final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = bitemporalSeccionInciso
				.getContinuity().getCoberturaSeccionContinuities()
				.getInProcess(validoEn);
		/*		
		Map<String, Object> params2 = new HashMap<String, Object>();
		params2.put("continuity.seccionIncisoContinuity.id", bitemporalSeccionInciso.getContinuity().getId());		
		List<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = (List<BitemporalCoberturaSeccion>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalCoberturaSeccion.class, 
				params2, validoEn, null, true);		
		*/
		Iterator<BitemporalCoberturaSeccion> itBitemporalCoberturaSeccion = lstBitemporalCoberturaSeccion.iterator();
		while (itBitemporalCoberturaSeccion.hasNext()) 
		{
			BitemporalCoberturaSeccion bitemporalCoberturaSeccion = itBitemporalCoberturaSeccion.next();
			
			if (bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO().getCoberturaEsPropia().equalsIgnoreCase("1") 
					&& bitemporalCoberturaSeccion.getValue().getClaveContrato() == 1 && bitemporalCoberturaSeccion.getRecordStatus().equals(
							RecordStatus.NOT_IN_PROCESS)) 
			{
				entidadBitemporalService.saveInProcess(entidadBitemporalService.getInProcessByKey(CoberturaSeccionContinuity.class,
						bitemporalCoberturaSeccion.getContinuity().getId(), validoEn), validoEn);						
			}					
		}

	}
	
	@Override
	public ResumenCostosDTO resumenCostosEmitidos(BigDecimal idToPoliza, Short numeroEndoso, Integer numeroInciso){
		return this.calculoDao.resumenCostosEmitidos(idToPoliza, numeroEndoso, numeroInciso);
	}
	
	
	@Override
	public ResumenCostosDTO resumenCostosCaratulaPoliza(BigDecimal idToPoliza){
		return this.resumenCostosEmitidos(idToPoliza, null, null, true);
	}
	
	@Override
	public ResumenCostosDTO resumenCostosIncisoPoliza(BigDecimal idToPoliza, BigDecimal numeroInciso){
		return this.resumenCostosEmitidos(idToPoliza, null, numeroInciso, false);
	}
	
	@Override
	public ResumenCostosDTO resumenCostosIncisoPoliza(BigDecimal idToPoliza, BigDecimal numeroInciso, Short numeroEndoso) {
		return this.resumenCostosEmitidos(idToPoliza, numeroEndoso, numeroInciso, true);
	}
	
	@Override
	public ResumenCostosDTO resumenCostosEndoso(BigDecimal idToPoliza, Short numeroEndoso){
		return this.resumenCostosEmitidos(idToPoliza, numeroEndoso, null, false); 
	}
	
	@Override
	public ResumenCostosDTO resumenCostosEndoso(BigDecimal idToPoliza, Short numeroEndoso, boolean esConsultaPoliza) {
		return this.resumenCostosEmitidos(idToPoliza, numeroEndoso, null, esConsultaPoliza); 
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public ResumenCostosDTO resumenCotizacionEndoso(Integer numeroCotizacion, DateTime validoEn){
		ResumenCostosDTO resumenCostos = new ResumenCostosDTO();
		BigDecimal primaNeta = new BigDecimal(0, mathCtxResumen);
		BigDecimal totalPrimas = new BigDecimal(0, mathCtxResumen);
		BigDecimal descuentos = new BigDecimal(0, mathCtxResumen);
		BigDecimal recargo = new BigDecimal(0, mathCtxResumen);
		BigDecimal derechos = new BigDecimal(0, mathCtxResumen);
		BigDecimal iva = new BigDecimal(0, mathCtxResumen);
		BigDecimal primaTotal = new BigDecimal(0, mathCtxResumen);

		BitemporalCotizacion cotizacion = null;
		ControlEndosoCot controlEndosoCot = null;

		if (numeroCotizacion != null && validoEn != null) {
			Map<String, Object> values = new LinkedHashMap<String, Object>();
			
			//Devuelve la cotizacion en proceso
			controlEndosoCot = obtenerControlEndosoCotizacion(numeroCotizacion);
			if(controlEndosoCot != null){
				values.put("controlEndosoCot.id", controlEndosoCot.getId());
				values.put("tipo", TipoMovimientoEndoso.COBERTURA);
				cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,
						CotizacionContinuity.BUSINESS_KEY_NAME, numeroCotizacion, validoEn);

				if (cotizacion == null){
					cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,
							CotizacionContinuity.BUSINESS_KEY_NAME, numeroCotizacion, validoEn);
				}

				//se calculan los movimientos.
				movimientoEndosoBitemporalService.calcular(controlEndosoCot);
				endosoService.igualarPrimaMovimientosEndoso(controlEndosoCot);
				endosoService.ajustaMovimientosEndoso(controlEndosoCot);
				//Carga los movimientos a nivel inciso para acumular los costos del endoso
				movimientos = entidadService.findByProperties(MovimientoEndoso.class, values);	
			}else{
				movimientos=null;
			}
		}
		
		if ( movimientos == null || movimientos.isEmpty()) {
			return resumenCostos;
		} else {
			
			for(MovimientoEndoso detalle: movimientos){
				if(detalle.getValorMovimiento() != null){
					totalPrimas = totalPrimas.add((detalle.getValorMovimiento().multiply(detalle.getFactorCobranzaPrimaNeta())).setScale(16, BigDecimal.ROUND_HALF_UP));
					descuentos = descuentos.add((detalle.getValorBonificacionComision().multiply(detalle.getFactorCobranzaBonificacionComision())).setScale(16, BigDecimal.ROUND_HALF_UP));
					primaNeta = totalPrimas.subtract(descuentos).setScale(16, BigDecimal.ROUND_HALF_UP);
					recargo = recargo.add((detalle.getValorRPF().multiply(detalle.getFactorCobranzaRPF())).setScale(16, BigDecimal.ROUND_HALF_UP)).subtract((detalle.getValorBonificacionRPF().multiply(detalle.getFactorCobranzaBonificacionRPF())).setScale(16, BigDecimal.ROUND_HALF_UP));
					derechos = derechos.add((detalle.getValorDerechos().multiply(detalle.getFactorCobranzaDerechos())).setScale(16, BigDecimal.ROUND_HALF_UP));
					iva  = iva.add((detalle.getValorIva().multiply(detalle.getFactorCobranzaIva())).setScale(16, BigDecimal.ROUND_HALF_UP));
					primaTotal = primaNeta.add(recargo).add(derechos).add(iva);
				}
			}
			
			resumenCostos.setTotalPrimas(totalPrimas.doubleValue());
			resumenCostos.setDescuentoComisionCedida(descuentos.doubleValue());
			resumenCostos.setPrimaNetaCoberturas(primaNeta.doubleValue());
			resumenCostos.setRecargo(recargo.doubleValue());
			resumenCostos.setDerechos(derechos.doubleValue());
			resumenCostos.setIva(iva.doubleValue());
			resumenCostos.setPrimaTotal(primaTotal.doubleValue());
			resumenCostos.setPorcentajeIva(cotizacion.getValue().getPorcentajeIva());

			if (cotizacion.getEntidadContinuity().getBitemporalProperty().hasRecordsInProcess()) {
				BigDecimal primaTotalOriginal = new BigDecimal(0);
				BigDecimal totalPrimasOriginal = new BigDecimal(0);

				BitemporalCotizacion cotizacionOriginal = 
					cotizacion.getEntidadContinuity().getBitemporalProperty().get(validoEn);

				if (cotizacionOriginal != null) {
					primaTotalOriginal = cotizacionOriginal.getValue().getValorPrimaTotal();
					totalPrimasOriginal= cotizacionOriginal.getValue().getValorTotalPrimas();
				}
				
				BigDecimal nuevaPrimaTotal = primaTotalOriginal.add(primaTotal);

				BigDecimal nuevaTotalPrimas = totalPrimasOriginal.add(totalPrimas);

				//Guarda totales en cotizacion
				cotizacion.getValue().setValorPrimaTotal(nuevaPrimaTotal);
				cotizacion.getValue().setValorTotalPrimas(nuevaTotalPrimas);
				cotizacion.getValue().setValorPrimaTotalEndoso(primaTotal);


				entidadService.save(cotizacion.getEntidadContinuity());
			}
			
			
		}
		return resumenCostos;
	}
	
	@SuppressWarnings("unchecked")
	private ResumenCostosDTO resumenCostosEmitidos(BigDecimal idToPoliza, final Short numeroEndoso, 
			BigDecimal numeroInciso, boolean esConsultaPoliza) {
		ResumenCostosDTO resumenCostosDTO = new ResumenCostosDTO();
		Map<String, Object> values = new LinkedHashMap<String, Object>();
		List<CoberturaEndosoDTO> coberturaEndosoList = null;
		
		BigDecimal primaNeta = new BigDecimal(0, mathCtxResumen);
		BigDecimal totalPrimas = new BigDecimal(0, mathCtxResumen);
		BigDecimal descuentos = new BigDecimal(0, mathCtxResumen);
		BigDecimal recargo = new BigDecimal(0, mathCtxResumen);
		BigDecimal derechos = new BigDecimal(0, mathCtxResumen);
		BigDecimal iva = new BigDecimal(0, mathCtxResumen);
		BigDecimal primaTotal = new BigDecimal(0, mathCtxResumen);
		
		if (idToPoliza != null) {
			values.put("id.idToPoliza", idToPoliza);
			//if (numeroEndoso == null && numeroInciso == null){
			if (numeroInciso == null && esConsultaPoliza) {
				coberturaEndosoList =  entidadService.findByProperties(CoberturaEndosoDTO.class, values);
			}
			if (numeroInciso != null) {
				values.put("id.numeroInciso", numeroInciso);
				coberturaEndosoList =  entidadService.findByProperties(CoberturaEndosoDTO.class, values);
			}
			if (numeroEndoso != null && !esConsultaPoliza) {
				values.put("id.numeroEndoso", numeroEndoso);
				coberturaEndosoList =  entidadService.findByProperties(CoberturaEndosoDTO.class, values);
			}
		}
		
		if (coberturaEndosoList != null) {
			
			if (numeroEndoso != null && esConsultaPoliza)  {
				Predicate predicate = new Predicate() {		
					@Override
					public boolean evaluate(Object arg0) {
						CoberturaEndosoDTO cobertura = (CoberturaEndosoDTO)arg0;
						if (cobertura.getId().getNumeroEndoso() <= numeroEndoso) {
							return true;
						}
						return false;
					}
				};		
				coberturaEndosoList = (List<CoberturaEndosoDTO>) CollectionUtils.select(coberturaEndosoList, predicate);
			}
			
			for (CoberturaEndosoDTO coberturaEndoso : coberturaEndosoList) {
				totalPrimas = totalPrimas.add(new BigDecimal(coberturaEndoso.getValorPrimaNeta()));
				double valorComisionRPF = coberturaEndoso.getValorRecargoPagoFrac();
				double valorBonifComision = coberturaEndoso.getValorBonifComision();
				//Formula incorrecta
				/*
				if(negocioBono != null && negocioBono.getPctUDI() != null){
					valorComisionRPF = coberturaEndoso.getValorRecargoPagoFrac() * (1 - (negocioBono.getPctUDI().doubleValue() / 100));
					valorBonifComision = valorBonifComision * (1 - (negocioBono.getPctUDI().doubleValue() / 100));
				}*/
				descuentos = descuentos.add(new BigDecimal(valorBonifComision));
				primaNeta = totalPrimas.subtract(descuentos);
				recargo = recargo.add(new BigDecimal(valorComisionRPF)).subtract(new BigDecimal(coberturaEndoso.getValorBonifComRecPagoFrac()));				
				derechos = derechos.add(new BigDecimal(coberturaEndoso.getValorDerechos()));
				iva  = iva.add(new BigDecimal(coberturaEndoso.getValorIVA()));
				primaTotal = primaTotal.add(new BigDecimal(coberturaEndoso.getValorPrimaTotal()));				
			}			
			
			resumenCostosDTO.setTotalPrimas(totalPrimas.doubleValue());
			resumenCostosDTO.setDescuentoComisionCedida(descuentos.doubleValue());
			resumenCostosDTO.setDescuento(descuentos.doubleValue());
			resumenCostosDTO.setPrimaNetaCoberturas(primaNeta.doubleValue());
			resumenCostosDTO.setRecargo(recargo.doubleValue());
			resumenCostosDTO.setDerechos(derechos.doubleValue());
			resumenCostosDTO.setIva(iva.doubleValue());
			resumenCostosDTO.setPrimaTotal(primaTotal.doubleValue());
		}
		
		return resumenCostosDTO;
	}
	
	private BigDecimal obtenerDescuentoBitemporalInciso(BitemporalSeccionInciso seccionInciso, DateTime validoEn){
		BigDecimal descuento = BigDecimal.ZERO;
		try{
			final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = seccionInciso.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
			for (BitemporalCoberturaSeccion bitemporalCoberturaSeccion : lstBitemporalCoberturaSeccion) {
				final CoberturaDTO cobertura = bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO();
				if ("1".equals(cobertura.getCoberturaEsPropia())) {
					final Collection<BitemporalCoberturaDescuento> lstBitemporalCoberturaDescuento = bitemporalCoberturaSeccion
					.getContinuity().getCoberturaDescuentoContinuities().getInProcess(validoEn);
					for (BitemporalCoberturaDescuento bitemporalCoberturaDescuento : lstBitemporalCoberturaDescuento) {
						if(bitemporalCoberturaDescuento.getContinuity().getDescuentoDTO().getIdToDescuentoVario() != null && 
								bitemporalCoberturaDescuento.getContinuity().getDescuentoDTO().getIdToDescuentoVario().longValue() == -5 ){
							descuento = bitemporalCoberturaDescuento.getValue().getValorDescuento();
							break;
						}
					}
				}
			}
		}catch(Exception e){
		}
		return descuento;
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void restaurarDescuentoPorEstado(BigDecimal idToCotizacion, Double descuentoPorNoSiniestro, NegocioRenovacionDescId id, boolean renovarEmitir) {
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		for (IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()) {
			Double descuentoPorEstado = 0.0, descuento = 0.0;
			if(inciso.getIncisoAutoCot() != null && inciso.getIncisoAutoCot().getPctDescuentoEstado() != null) {
				descuentoPorEstado = inciso.getIncisoAutoCot().getPctDescuentoEstado();
			}
			
		    descuento = descuentoPorEstado + descuentoPorNoSiniestro;
			
			
			if(descuento > 99 && renovarEmitir) {
				descuento = 99.0;
			}
			
			inciso.getIncisoAutoCot().setPctDescuentoEstado(descuento);
			entidadService.save(inciso.getIncisoAutoCot());
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Double calculaPorcentajeDescuentoGlobal(BigDecimal idToCotizacion) {
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		Double porcentajeDescuentoGlobal = 0.0;

		//Tomar el máximo descuento por estado del primer inciso
		if(cotizacion.getIncisoCotizacionDTOs() != null && cotizacion.getIncisoCotizacionDTOs().size() > 0 
				&& cotizacion.getIncisoCotizacionDTOs().get(0).getIncisoAutoCot() != null) {
			NegocioEstadoDescuento negocioEstadoDescuento = negocioEstadoDescuentoService.findByNegocioAndEstado(
					cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), 
					cotizacion.getIncisoCotizacionDTOs().get(0).getIncisoAutoCot().getEstadoId());
			if(negocioEstadoDescuento != null && negocioEstadoDescuento.getPctDescuento() != null) {
				porcentajeDescuentoGlobal = negocioEstadoDescuento.getPctDescuento();
			}
		}
		
		return porcentajeDescuentoGlobal;
	}
	
	public MotorDecisionDTO obtenerFiltrosMotor(BigDecimal idToCotizacion, BigDecimal numeroInciso){
		MotorDecisionDTO filtrosMotorDecisionDto = null;
		
		if(idToCotizacion != null
				&& numeroInciso != null){
			filtrosMotorDecisionDto = new MotorDecisionDTO();
			
			//recuperando la informacion de la cotizacion
			IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
			incisoCotizacionId.setIdToCotizacion(idToCotizacion);
			incisoCotizacionId.setNumeroInciso(numeroInciso);
			IncisoCotizacionDTO incisoCotizacion =  entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionId);
			CotizacionDTO cotizacion = incisoCotizacion.getCotizacionDTO();
			
			if(cotizacion.getNegocioTipoPoliza() == null
					|| cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza() == null){
				NegocioTipoPoliza negocioTipoPoliza = cotizadorAgentesService.getNegocioTipoPoliza(cotizacion);
				cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
			}
			
			//inicia la carga de los filtros para la busqueda
			filtrosMotorDecisionDto.setTipoConsulta(MotorDecisionDTO.TipoConsulta.TIPO_CONSULTA_SUMASASEGURADAS);
			filtrosMotorDecisionDto.setIdToNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			
			if( incisoCotizacion.getIncisoAutoCot().getNegocioSeccionId() != null
					&& incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId() != null
					&& cotizacion.getIdMoneda() != null){
				
				List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosMotor = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
				
				//filtros requeridos para la busqueda
				filtrosMotor.add(filtrosMotorDecisionDto.new FiltrosMotorDecisionDTO(
					Boolean.TRUE,"negocioPaqueteSeccion.idToNegPaqueteSeccion", incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId()));
				filtrosMotor.add(filtrosMotorDecisionDto.new FiltrosMotorDecisionDTO(
					Boolean.TRUE,"monedaDTO.idTcMoneda",cotizacion.getIdMoneda()));
				
				EstadoDTO estado = null;
				CiudadDTO municipio = null;
				TipoUsoVehiculoDTO tipoUsoVehiculo = null;
				Agente agente = null;
				
				if(incisoCotizacion.getIncisoAutoCot().getEstadoId() != null){
					estado = entidadService.findById(EstadoDTO.class, incisoCotizacion.getIncisoAutoCot().getEstadoId());
				}
				if(incisoCotizacion.getIncisoAutoCot().getMunicipioId() != null){
					municipio = entidadService.findById(CiudadDTO.class, incisoCotizacion.getIncisoAutoCot().getMunicipioId());
				}
				if(incisoCotizacion.getIncisoAutoCot().getTipoUsoId() != null){
					tipoUsoVehiculo = entidadService.findById(TipoUsoVehiculoDTO.class, BigDecimal.valueOf(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()));
				}
				if(cotizacion.getSolicitudDTO().getCodigoAgente() != null){
					List<Agente> agenteList = entidadService.findByProperty(Agente.class, "id", cotizacion.getSolicitudDTO().getCodigoAgente());
					agente = agenteList.get(0);
				}
				
				filtrosMotor.add(filtrosMotorDecisionDto.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"estadoDTO",estado));
				filtrosMotor.add(filtrosMotorDecisionDto.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"ciudadDTO",municipio));
				filtrosMotor.add(filtrosMotorDecisionDto.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"tipoUsoVehiculo",tipoUsoVehiculo));
				filtrosMotor.add(filtrosMotorDecisionDto.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"agente",agente));
				filtrosMotor.add(filtrosMotorDecisionDto.new FiltrosMotorDecisionDTO(
						Boolean.FALSE,"renovacion",false));
				
				filtrosMotorDecisionDto.setFiltrosDTO(filtrosMotor);
			}
		}
		return filtrosMotorDecisionDto;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void agregarDescuentoRecargoCoberturaCodigPostal(IncisoCotizacionDTO inciso) {
		
		LOG.info("Entro agregarDescuentoRecargoCoberturaCodigPostal...");
		if (inciso.getIncisoAutoCot().getCodigoPostal() != null	
				&& inciso.getIncisoAutoCot().getCodigoPostal() != 0L){
			for (CoberturaCotizacionDTO cobertura : inciso.getSeccionCotizacion().getCoberturaCotizacionLista()) {
				
				if (cobertura.getCoberturaSeccionDTO()!=null &&
						cobertura.getCoberturaSeccionDTO().getCoberturaDTO() !=null &&
								cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia()!=null &&
										cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getCoberturaEsPropia().equals("1")  && 
										cobertura.getClaveContrato() == 1) {
					
					List<RecargosDescuentosCPDTO> list = this.obtenerListaRecargosDescuentos(
							inciso.getIncisoAutoCot().getCodigoPostal().intValue(),
							cobertura.getId().getIdToSeccion().intValue(),
							cobertura.getId().getIdToCobertura().intValue());
					
					if (list!=null && !list.isEmpty() && list.get(0) != null 
							&& this.validarFechaCodigoPostal(list.get(0).getFechaVencimiento())){
						String tipoValor = list.get(0).getTipoValor();
						BigDecimal valor = list.get(0).getValor();
					
						if (valor.compareTo(new BigDecimal(0)) > 0){
							RecargoCoberturaCot recargoCobertura = new RecargoCoberturaCot();
							RecargoCoberturaCotId recargoCoberturaId = new RecargoCoberturaCotId();
							recargoCoberturaId.setIdToCobertura(cobertura.getId().getIdToCobertura().longValue());
							recargoCoberturaId.setIdToCotizacion(cobertura.getId().getIdToCotizacion().longValue());
							recargoCoberturaId.setIdToSeccion(cobertura.getId().getIdToSeccion().longValue());
							recargoCoberturaId.setNumeroInciso(cobertura.getId().getNumeroInciso().longValue());
							recargoCoberturaId.setIdToRecargoVario(tipoValor.equalsIgnoreCase(RecargosDescuentosCPDTO.TIPO_VALOR_PORCENTAJE_STR) ? -8l : -9l);
							recargoCobertura.setId(recargoCoberturaId);
							recargoCobertura.setValorRecargo(valor);
							recargoCobertura.setClaveAutorizacion(0l);
							recargoCobertura.setClaveComercialTecnico((short) 2);
							recargoCobertura.setClaveContrato(cobertura.getClaveContrato());
							recargoCobertura.setClaveNivel((short) 3);
							recargoCobertura.setClaveObligatoriedad((short) 0);
							entidadService.save(recargoCobertura);
						} else {
							DescuentoCoberturaCot descuentoCobertura = new DescuentoCoberturaCot();
							DescuentoCoberturaCotId descuentoCoberturaId = new DescuentoCoberturaCotId();
							descuentoCoberturaId.setIdToCobertura(cobertura.getId().getIdToCobertura().longValue());
							descuentoCoberturaId.setIdToCotizacion(cobertura.getId().getIdToCotizacion().longValue());
							descuentoCoberturaId.setIdToSeccion(cobertura.getId().getIdToSeccion().longValue());
							descuentoCoberturaId.setNumeroInciso(cobertura.getId().getNumeroInciso().longValue());
							descuentoCoberturaId.setIdToDescuentoVario(tipoValor.equalsIgnoreCase(RecargosDescuentosCPDTO.TIPO_VALOR_PORCENTAJE_STR) ? -8l : -9l);
							descuentoCobertura.setId(descuentoCoberturaId);
							descuentoCobertura.setValorDescuento(valor.abs());
							descuentoCobertura.setClaveAutorizacion(0l);
							descuentoCobertura.setClaveComercialTecnico((short) 2);
							descuentoCobertura.setClaveContrato(cobertura.getClaveContrato());
							descuentoCobertura.setClaveNivel((short) 3);
							descuentoCobertura.setClaveObligatoriedad((short) 0);
							entidadService.save(descuentoCobertura);
						}
					}
				}
			}
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void agregarDescuentoRecargoCoberturaCodigPostal(BitemporalInciso inciso,
		DateTime validoEn, Long cp,	BigDecimal idToSeccion) {
		LOG.info("Entro agregarDescuentoRecargoCoberturaEndosoCodigoPostal...");

		eliminarDescuentoCoberturaEndoso(-8l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-8l, inciso, validoEn);
		eliminarDescuentoCoberturaEndoso(-9l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-9l, inciso, validoEn);

		if (cp != null	&& cp != 0l) {

			final Collection<BitemporalSeccionInciso> lstBitemporalSeccionInciso = inciso.getContinuity()
				.getIncisoSeccionContinuities().getInProcess(validoEn);

			for (BitemporalSeccionInciso bitemporalSeccionInciso : lstBitemporalSeccionInciso) {
				final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = bitemporalSeccionInciso
						.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
				for (BitemporalCoberturaSeccion bitemporalCoberturaSeccion : lstBitemporalCoberturaSeccion) {

					final CoberturaDTO cobertura = bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO();

					// consultar factor descuento
					List<RecargosDescuentosCPDTO> list = this.obtenerListaRecargosDescuentos(cp.intValue(),
						idToSeccion.intValueExact(),
						cobertura.getIdToCobertura().intValueExact());

					if (list != null && !list.isEmpty()	&& list.get(0) != null && this.validarFechaCodigoPostal(list.get(0).getFechaVencimiento())) {

						String tipoValor = list.get(0).getTipoValor();
						BigDecimal valor = list.get(0).getValor();

						if ("1".equals(cobertura.getCoberturaEsPropia()) && bitemporalCoberturaSeccion.getValue().getClaveContrato() == 1) {
							if (valor.compareTo(new BigDecimal(0)) > 0) {
								final BitemporalCoberturaRecargo recargoCobertura = new BitemporalCoberturaRecargo();
								RecargoVarioDTO recargoVarioDTO = new RecargoVarioDTO();
								recargoVarioDTO.setIdtorecargovario(new BigDecimal(tipoValor.equalsIgnoreCase(RecargosDescuentosCPDTO.TIPO_VALOR_PORCENTAJE_STR) ? -8l : -9l));
								recargoCobertura.getContinuity().setParentContinuity(bitemporalCoberturaSeccion.getContinuity());
								recargoCobertura.getContinuity().setRecargoVarioDTO(recargoVarioDTO);
								recargoCobertura.getValue().setValorRecargo(valor);
								recargoCobertura.getValue().setClaveAutorizacion(0l);
								recargoCobertura.getValue().setClaveComercialTecnico((short) 2);
								recargoCobertura.getValue().setClaveContrato(bitemporalSeccionInciso.getValue().getClaveContrato());
								recargoCobertura.getValue().setClaveNivel((short) 3);
								recargoCobertura.getValue().setClaveObligatoriedad((short) 0);
								entidadBitemporalService.saveInProcess(recargoCobertura, validoEn);
							} else {
								DescuentoDTO descuentoDTO = new DescuentoDTO();
								descuentoDTO.setIdToDescuentoVario(new BigDecimal(tipoValor.equalsIgnoreCase(RecargosDescuentosCPDTO.TIPO_VALOR_PORCENTAJE_STR) ? -8l : -9l));
								final BitemporalCoberturaDescuento descuentoCobertura = new BitemporalCoberturaDescuento();
								descuentoCobertura.getContinuity().setParentContinuity(bitemporalCoberturaSeccion.getContinuity());
								descuentoCobertura.getContinuity().setDescuentoDTO(descuentoDTO);
								descuentoCobertura.getValue().setValorDescuento(valor.abs());
								descuentoCobertura.getValue().setClaveAutorizacion(Long.valueOf("0"));
								descuentoCobertura.getValue().setClaveComercialTecnico((short) 2);
								descuentoCobertura.getValue().setClaveContrato(
								bitemporalCoberturaSeccion.getValue().getClaveContrato());
								descuentoCobertura.getValue().setClaveNivel((short) 3);
								descuentoCobertura.getValue().setClaveObligatoriedad((short) 0);
								entidadBitemporalService.saveInProcess(descuentoCobertura, validoEn);
							}
						}
					}
				}
			}
		}
	}
	
	private List<RecargosDescuentosCPDTO> obtenerListaRecargosDescuentos(
			Integer cp,
			Integer seccionId,
			Integer coberturaId){
		
		RecargosDescuentosCPDTO filtro = new RecargosDescuentosCPDTO();
		filtro.setCodigoPostal(cp);
		filtro.setIdToSeccion(seccionId);
		filtro.setIdToCobertura(coberturaId);
		
		List<RecargosDescuentosCPDTO> list = null;
		try {
			list = this.recargosDescuentosCPDao.findByFilterObject(RecargosDescuentosCPDTO.class, filtro, null);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return list;
	}
	
	//Devuelve true si la fecha es null o si aun es válida
	private boolean validarFechaCodigoPostal(Date fechaVencimiento){
		
		boolean isValida = true; 
		Date hoy = new Date();
		
		if (fechaVencimiento != null && fechaVencimiento.before(hoy)){
			isValida = false;
		}
		return isValida;
	}
}

