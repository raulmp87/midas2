package mx.com.afirme.midas2.service.impl.suscripcion.solicitud.impresiones;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.DeudorPorPrimaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.reportes.ReporteService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.impresiones.GeneraPlantillaReporteSolicitudService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.lowagie.text.Chunk;
import com.lowagie.text.Document;
import com.lowagie.text.Font;
import com.lowagie.text.Image;
import com.lowagie.text.Paragraph;
import com.lowagie.text.pdf.BaseFont;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfContentByte;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.PdfStamper;
import com.lowagie.text.pdf.PdfWriter;
import com.lowagie.text.pdf.SimpleBookmark;


@Stateless
public class GeneraPlantillaReporteSolicitudServiceImpl implements GeneraPlantillaReporteSolicitudService {

	private static final Logger LOG = Logger.getLogger(GeneraPlantillaReporteSolicitudServiceImpl.class);
	
	
private EntidadService entidadService;

private ReporteService reporteService;

@EJB(beanName = "UsuarioServiceDelegate")
private UsuarioService usuarioService;

@EJB
private FileManagerService fileManagerService;

@EJB
public void setEntidadService(EntidadService entidadService) {
	this.entidadService = entidadService;
}

@Autowired
@Qualifier("reporteServiceEJB")
public void setReporteService(ReporteService reporteService) {
	this.reporteService = reporteService;
}

/*
@EJB(beanName="UsuarioServiceDelegate")
public void setUsuarioService(UsuarioService usuarioService) {
	this.usuarioService = usuarioService;
}
*/

public byte[] imprimirSolicitud(
	SolicitudDTO datosCaratulaSolicitud, Locale locale) {
		
	List<SolicitudDTO> datasourceSolicitud = new ArrayList<SolicitudDTO>();
	List<Comentario>        datasourceComentario = entidadService.findByProperty(Comentario.class, "solicitudDTO.idToSolicitud",datosCaratulaSolicitud.getIdToSolicitud());
	List<DocumentoDigitalSolicitudDTO>  documentoDigitalSolicitudList = entidadService.findByProperty(DocumentoDigitalSolicitudDTO.class, "solicitudDTO.idToSolicitud",datosCaratulaSolicitud.getIdToSolicitud());
	for(Comentario comentario : datasourceComentario){
		if(comentario.getDocumentoDigitalSolicitudDTO() != null){
			for(int i = 0; i < documentoDigitalSolicitudList.size(); i++){
				DocumentoDigitalSolicitudDTO documento = documentoDigitalSolicitudList.get(i);
				if(comentario.getDocumentoDigitalSolicitudDTO().getIdToDocumentoDigitalSolicitud().equals(documento.getIdToDocumentoDigitalSolicitud())){
					documentoDigitalSolicitudList.remove(i);
					break;
				}
			}
		}
	}
	List<ControlArchivoDTO> datasourceArchivo = new ArrayList<ControlArchivoDTO>(1);
	for(int i = 0; i < documentoDigitalSolicitudList.size(); i++){
		DocumentoDigitalSolicitudDTO documento = documentoDigitalSolicitudList.get(i);
		datasourceArchivo.add(documento.getControlArchivo());
	}
	for(Comentario item: datasourceComentario){
	  if(item.getDocumentoDigitalSolicitudDTO() != null && item.getDocumentoDigitalSolicitudDTO().getControlArchivo()!=null){	 
		  item.setNombreArchivoOriginal(item.getDocumentoDigitalSolicitudDTO().getControlArchivo().getNombreArchivoOriginal());
			String extension=null;
			String fileName =item.getDocumentoDigitalSolicitudDTO().getControlArchivo().getNombreArchivoOriginal();
			extension =   (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());
			item.setExtension(extension);
	  }
	}
	
		
	for(ControlArchivoDTO item: datasourceArchivo){
		  
		if(item.getNombreArchivoOriginal()!=null){
			String extension=null;
			String fileName =item.getNombreArchivoOriginal();
			extension =   (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());
			item.setExtension(extension!=null?extension:"");
		}
	}
	byte[] pdfSolicitud = null;

	try {
		InputStream caratulaSolicitudStream = this
				.getClass()
				.getResourceAsStream(
						"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CaratulaSolicitud.jrxml");
		JasperReport reporteSolicitud = JasperCompileManager
				.compileReport(caratulaSolicitudStream);
		
		InputStream desgloseComentariosStream = this.getClass().getResourceAsStream(
				"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/DesgloseComentariosSolicitud.jrxml");
        JasperReport reporteComentario = JasperCompileManager.compileReport(desgloseComentariosStream);		

		InputStream desgloseArchivoStream = this.getClass().getResourceAsStream(
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/DesgloseArchivosSolicitud.jrxml");
        JasperReport reporteArchivo = JasperCompileManager.compileReport(desgloseArchivoStream);	

		Map<String, Object> parameters = new HashMap<String, Object>();

		if (locale != null)
    	parameters.put(JRParameter.REPORT_LOCALE, locale);
		parameters.put("LOGO_AFIRME", SistemaPersistencia.LOGO_AFIRME);
		parameters.put("datasourceComentarios", datasourceComentario);
		parameters.put("reporteComentarios", reporteComentario);	
		
		parameters.put("datasourceArchivos", datasourceArchivo);
		parameters.put("reporteArchivos", reporteArchivo);
		
		parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(2));
		datasourceSolicitud.add(datosCaratulaSolicitud);
		JRBeanCollectionDataSource dsSolicitud = new JRBeanCollectionDataSource(
				datasourceSolicitud);

		pdfSolicitud = JasperRunManager.runReportToPdf(reporteSolicitud,
				parameters, dsSolicitud);
	} catch (JRException ex) {
		throw new RuntimeException(ex);
	}
	return pdfSolicitud;
}

@Override
public InputStream impresionDeudorPrima(String nivel, BigDecimal rango){
	
	//TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
	InputStream inputStream = null;
	
	String uploadFolder = this.getPath();
	
    String user = "ADMIN";
    //File file = File.createTempFile("DXP", ".csv", new File(uploadFolder));
    try {
    	user = usuarioService.getUsuarioActual().getNombreUsuario();
    }catch (Exception e) {
    	LOG.info("Log GeneraPlantillaReporteSolicitudServiceImpl: No se encontro Usuario.");
	}
    //Date fechaSP = reporteService.getUltimaFechaGeneracionDporP();
	//try {
		//String fileName = creaDXPD2(nivel, rango);		
		//transporteImpresionDTO.setByteArray(IOUtils.toByteArray(obtieneDXPD2(nivel, rango, fileName)));
		inputStream = obtieneDXPD2(nivel, rango, uploadFolder+"DXP"+nivel+".txt");
		//return transporteImpresionDTO;
		
	/*} catch (IOException e1) {
		e1.printStackTrace();
	}*/
	/*
	ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	try{
	
	JRProperties.setProperty(JRXPathExecuterUtils.PROPERTY_XPATH_EXECUTER_FACTORY, "net.sf.jasperreports.engine.util.xml.JaxenXPathExecuterFactory");
	JRProperties.setProperty(JRQueryExecuterFactory.QUERY_EXECUTER_FACTORY_PREFIX+"plsql","com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
	
	String jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/DporP_D3.jrxml";
	if(nivel.equals("R")){
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/DporP_R.jrxml";
	}
	InputStream caratulaSolicitudStream = this.getClass().getResourceAsStream(jrxml);
	JasperReport jasperReport = JasperCompileManager.compileReport(caratulaSolicitudStream);

	//Para llamado a StoreProcedure
	jasperReport.setProperty( "net.sf.jasperreports.query.executer.factory.plsql","com.jaspersoft.jrx.query.PlSqlQueryExecuterFactory");
	//Quita fondo blanco en export a excel
	jasperReport.setProperty( "net.sf.jasperreports.export.xls.white.page.background","false");
	//Texto completo en celdas de excel
	jasperReport.setProperty( "net.sf.jasperreports.print.keep.full.text","true");
	//Autoajustar celdas de Excel No funciona
	//jasperReport.setProperty( "net.sf.jasperreports.export.xls.auto.fit.column","true");
	
	Map<String, Object> parameters = new HashMap<String, Object>();	
	//JRFileVirtualizer virtualizer = new JRFileVirtualizer (100, System.getProperty("java.io.tmpdir"));
	JRSwapFileVirtualizer virtualizer = new JRSwapFileVirtualizer(1000, new JRSwapFile(System.getProperty("java.io.tmpdir"), 254, 512), true);
	JRVirtualizationHelper.setThreadVirtualizer(virtualizer);
	virtualizer.setReadOnly(false);
	parameters.put(JRParameter.REPORT_VIRTUALIZER, virtualizer);	
	parameters.put("NIVEL", nivel);
	parameters.put("RANGO", rango);
	
	
	Context ctx = new InitialContext();
	DataSource ds = (DataSource)ctx.lookup("jdbc/MidasDataSource");
	Connection connection = ds.getConnection();
	
    JasperPrint jasperPrint = JasperFillManager.fillReport(jasperReport, parameters, connection);

    //JRXlsxExporter exporter = new JRXlsxExporter();
    JRCsvExporter  exporter = new JRCsvExporter();
    exporter.setParameter(JRXlsExporterParameter.JASPER_PRINT, jasperPrint);
    exporter.setParameter(JRXlsExporterParameter.OUTPUT_STREAM, outputStream);
    //exporter.setParameter(JRXlsExporterParameter.MAXIMUM_ROWS_PER_SHEET, 10);
    exporter.setParameter(JRXlsExporterParameter.IS_ONE_PAGE_PER_SHEET, Boolean.FALSE);
    exporter.setParameter(JRXlsExporterParameter.IS_REMOVE_EMPTY_SPACE_BETWEEN_ROWS, Boolean.TRUE);
    
    exporter.exportReport();
	}catch(Exception e){
		e.printStackTrace();
	}
	
	transporteImpresionDTO.setByteArray(outputStream.toByteArray());
	*/
	return inputStream;

}


public void generaRepDeudorPrima(String nivel, BigDecimal rango){
	
	try {
		creaDXPD2(nivel, rango);		
		
	} catch (IOException e1) {
		e1.printStackTrace();
	}

}

public String creaDXPD2(String nivel, BigDecimal rango) throws IOException {
	
	String csvFilePath;
	String uploadFolder = this.getPath();
	
    long start = System.currentTimeMillis();
    String user = "ADMIN";
    //File file = File.createTempFile("DXP", ".csv", new File(uploadFolder));
    try {
    	user = usuarioService.getUsuarioActual().getNombreUsuario();
    }catch (Exception e) {
    	LOG.info("Log GeneraPlantillaReporteSolicitudServiceImpl: No se encontro Usuario.");
	}
    //Date fechaSP = reporteService.getUltimaFechaGeneracionDporP();
    //File file = new File(uploadFolder+"DXP_"+user+".csv");
    try {
    	//FileWriter writer = new FileWriter(file);
    	// List<DeudorPorPrimaDTO> lista =  getDXP(nivel, rango);
    	//StringBuilder message = new StringBuilder();
    	
    	File file = new File(uploadFolder+"DXP"+nivel+".txt");
    	
    	if(!file.exists())
    	{
    		csvFilePath = getDXP(nivel, rango, uploadFolder+"DXP"+nivel+".txt");
    		LOG.info("Log GeneraPlantillaReporteSolicitudServiceImpl: Generando Archivo.");
    	}else
    	{
    		csvFilePath = "";
    		LOG.info("Log GeneraPlantillaReporteSolicitudServiceImpl: El archivo ya existe.");
    	}
    	
    	/*Iterator<DeudorPorPrimaDTO> it = lista.iterator();
    	boolean isFirst = true;
    	while(it.hasNext()){
    		DeudorPorPrimaDTO dxp = it.next();

    		if(isFirst){
    			String encabezado = creaEncabezadoDXP(nivel);
    			writer.write(encabezado);
    			isFirst = false;
    		}
    		
    		StringBuilder message = new StringBuilder();
    		message.append(dxp.getRango()+",");
    		message.append(dxp.getDesc_moneda()+",");
    		message.append(dxp.getUen()+",");
    		message.append(dxp.getId_ramo()+",");
    		message.append(dxp.getNom_ramo()+",");
    		if(nivel.equals("D")){
    			message.append(dxp.getNum_poliza()+",");
    			message.append("'"+dxp.getNom_solicitante()+"',");
    			message.append(dxp.getCve_t_recibo()+",");
    			message.append(dxp.getF_vencimiento()+",");
    			message.append(dxp.getNum_folio()+",");
    			message.append("'"+dxp.getNom_producto()+"',");
    			message.append("'"+dxp.getNom_linea_neg()+"',");
    			message.append(dxp.getEndoso()+",");
    		}
    		message.append(dxp.getPrima_neta()+",");
    		message.append(dxp.getBonif_comis()+",");
    		message.append(dxp.getRecargos()+",");
    		message.append(dxp.getDerechos()+",");
    		message.append(dxp.getIva()+",");
    		message.append(dxp.getPrima_total()+",");
    		message.append(dxp.getCom_pn_pf()+",");
    		message.append(dxp.getCom_pn_pm()+",");
    		message.append(dxp.getCom_rpf_pf()+",");
    		message.append(dxp.getCom_rpf_pm()+",");
    		if(nivel.equals("D")){
    			message.append(dxp.getId_agente()+",");
    			message.append("'"+dxp.getNombre()+"',");
    			message.append("'"+dxp.getNom_supervisoria()+"',");
    			message.append("'"+dxp.getNom_gerencia()+"',");
    			message.append("'"+dxp.getNom_oficina()+"',");
    			message.append(dxp.getId_cotizacion()+",");
    			message.append(dxp.getId_recibo()+",");
    			message.append(dxp.getF_prorroga()+",");
    			message.append(dxp.getDias_vigencia()+",");
    			message.append("'"+dxp.getSit_poliza()+"',");    			
    			message.append(dxp.getF_ini_vigencia()+",");
    			message.append(dxp.getF_fin_vigencia()+",");    			
    		}
    		message.append(System.getProperty("line.separator"));
    		writer.write(message.toString());
    	}*/
    //	writer.write(message.toString());
    //	writer.flush();
    //	writer.close();
    } finally {
        // comment this out if you want to inspect the files afterward
        //file.delete();
    }    	
    long end = System.currentTimeMillis();
    System.out.println((end - start) / 1000f + " seconds");
    
    return csvFilePath;
}

private String creaEncabezadoDXP(String nivel){
	
	StringBuilder message = new StringBuilder();
	message.append("RANGO,");
	message.append("DESC_MONEDA,");
	message.append("UEN,");
	message.append("ID_RAMO,");
	message.append("NOM_RAMO,");
	if(nivel.equals("D")){
		message.append("NUM_POLIZA,");
		message.append("NOM_SOLICITANTE,");
		message.append("CVE_T_RECIBO,");
		message.append("F_VENCIMIENTO,");
		message.append("NUM_FOLIO,");
		message.append("NOM_PRODUCTO,");
		message.append("NOM_LINEA_NEG,");
		message.append("ENDOSO,");
	}
	message.append("PRIMA_NETA,");
	message.append("BONIF_COMIS,");
	message.append("RECARGOS,");
	message.append("DERECHOS,");
	message.append("IVA,");
	message.append("PRIMA_TOTAL,");
	message.append("COM_PN_PF,");
	message.append("COM_PN_PM,");
	message.append("COM_RPF_PF,");
	message.append("COM_RPF_PM,");
	if(nivel.equals("D")){
		message.append("ID_AGENTE,");
		message.append("NOMBRE,");
		message.append("NOM_SUPERVISORIA,");
		message.append("NOM_GERENCIA,");
		message.append("NOM_OFICINA,");
		message.append("ID_COTIZACION,");
		message.append("ID_RECIBO,");
		message.append("FECHA_PRORROGA,");
		message.append("DIAS_VENCIMIENTO,");
		message.append("SIT_POLIZA,");
		message.append("F_INI_VIGENCIA,");
		message.append("F_FIN_VIGENCIA,");
	}
	message.append(System.getProperty("line.separator"));
	
	return message.toString();
}

public InputStream obtieneDXPD2(String nivel, BigDecimal rango, String FileName){
	InputStream is = null;
	
	//String uploadFolder = this.getPath();	
    try {
       
        File file = new File(FileName);    	
    	//File file = new File(uploadFolder+"DXP.csv");
		return new FileInputStream(file);
        /*
        URL url = new URL("http://localhost:9080");

        URLConnection urlCon = url.openConnection();

        System.out.println(urlCon.getContentType());

        is = urlCon.getInputStream();
        FileOutputStream fos = new FileOutputStream(uploadFolder+"DXP.csv");

        byte[] buffer = new byte[1000];        
        int bytesRead = is.read(buffer);
         
        while (bytesRead > 0) {
             
            fos.write(buffer, 0, bytesRead);
            bytesRead = is.read(buffer);
             
        }

        is.close();
        fos.close();
         */
    } catch (Exception e) {
         
        e.printStackTrace();
         
    }	
	
	return is;
}

public String getDXP(String nivel, BigDecimal rango, String fullPath) {

	List<DeudorPorPrimaDTO>  lista = new ArrayList<DeudorPorPrimaDTO>(1);
	String spName = "MIDAS.PKG_DXP.stp_reporte_dxp";
	
	String[] atributosDTO = { "rango","desc_moneda","uen","id_ramo","nom_ramo","num_poliza","nom_solicitante",
			"cve_t_recibo","f_vencimiento","num_folio","nom_producto","nom_linea_neg","endoso","prima_neta",
			"bonif_comis","recargos","derechos","iva","prima_total","com_pn_pf","com_pn_pm","com_rpf_pf","com_rpf_pm",
			"id_agente","nombre","nom_supervisoria","nom_gerencia","nom_oficina","id_cotizacion","id_recibo",
			"f_prorroga","dias_vigencia","sit_poliza","f_ini_vigencia","f_fin_vigencia"};
	
	String[] columnasCursor = { "rango","desc_moneda","uen","id_ramo","nom_ramo","num_poliza","nom_solicitante",
			"cve_t_recibo","f_vencimiento","num_folio","nom_producto","nom_linea_neg","endoso","prima_neta",
			"bonif_comis","recargos","derechos","iva","prima_total","com_pn_pf","com_pn_pm","com_rpf_pf","com_rpf_pm",
			"id_agente","nombre","nom_supervisoria","nom_gerencia","nom_oficina","id_cotizacion","id_recibo",
			"f_prorroga","dias","sit_poliza","f_ini_vigencia","f_fin_vigencia"};
	

	String[]	atributosDTOR = { "rango","desc_moneda","uen","id_ramo","nom_ramo","prima_neta",
				"bonif_comis","recargos","derechos","iva","total","com_pn_pf","com_pn_pm","com_rpf_pf","com_rpf_pm"};
		
	String[]	columnasCursorR = { "rango","desc_moneda","uen","id_ramo","nom_ramo","prima_neta",
				"bonif_comis","recargos","derechos","iva","total","com_pn_pf","com_pn_pm","com_rpf_pf","com_rpf_pm"};
		
	try {
		StoredProcedureHelper storedHelper = new StoredProcedureHelper(
				spName, StoredProcedureHelper.DATASOURCE_MIDAS);
		
		if(nivel.equals("D")){
			storedHelper.estableceMapeoResultados(DeudorPorPrimaDTO.class.getCanonicalName(), atributosDTO, columnasCursor);
		}else{
			storedHelper.estableceMapeoResultados(DeudorPorPrimaDTO.class.getCanonicalName(), atributosDTOR, columnasCursorR);
		}
			
			
		storedHelper.estableceParametro("pnivel", nivel);
		storedHelper.estableceParametro("prango", rango);
		

		fullPath = storedHelper.obtieneArchivoCSV(fullPath);	
		  
		
	} catch (Exception e) {
		throw new RuntimeException("Error en :" + spName, e);
	}
	return fullPath;
}

private String getPath(){
	String OSName = System.getProperty("os.name");
	String uploadFolder = "";
	if (OSName.toLowerCase().indexOf("windows")!=-1)
		uploadFolder = SistemaPersistencia.UPLOAD_FOLDER;
	else
		uploadFolder = SistemaPersistencia.LINUX_UPLOAD_FOLDER;
	
	return uploadFolder;
}



public TransporteImpresionDTO imprimirDocumento(Comentario comentario,Locale locale,String extension) {
	List<byte[]> inputByteArray = new ArrayList<byte[]>();
	Map<String, Object> parametros = new HashMap<String, Object>();	
	List<Comentario> listacomentario = new ArrayList<Comentario>(1);
	List<Comentario> datosComentario = entidadService.findByProperty(Comentario.class, "solicitudDTO.idToSolicitud",comentario.getSolicitudDTO().getIdToSolicitud());
	TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
	if(locale != null){
		parametros.put(JRParameter.REPORT_LOCALE, locale);
	}
	try{
		 String path=getPath();
	 for (Comentario item : datosComentario) {
		 if(item.getDocumentoDigitalSolicitudDTO() != null && item.getDocumentoDigitalSolicitudDTO().getControlArchivo() != null){
		   String archivoPdf=null;
		          archivoPdf=item.getDocumentoDigitalSolicitudDTO().getControlArchivo()
                                 .getNombreArchivoOriginal().substring(0, item.getDocumentoDigitalSolicitudDTO()
                                 .getControlArchivo().getNombreArchivoOriginal().indexOf("."))+"_stamper.pdf";

	  if(item.getDocumentoDigitalSolicitudDTO().getControlArchivo()
				.getNombreArchivoOriginal().trim().equalsIgnoreCase(comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo()	
			    .getNombreArchivoOriginal().trim())){  
	     if(extension.equalsIgnoreCase(".pdf")) {
		    	PdfReader reader = new PdfReader(fileManagerService.downloadFile(
		    			comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo().obtenerNombreArchivoFisico(), 
		    			comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo().getIdToControlArchivo().toString()));
	            PdfStamper stamp = new PdfStamper(reader, 
	            new FileOutputStream(path+archivoPdf));
	
	            PdfContentByte over = null;
	            over = stamp.getOverContent(1);
	            over.beginText();
	            BaseFont bf_times = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
	            over.setFontAndSize(bf_times, 13);
	            over.showTextAligned(PdfContentByte.ALIGN_CENTER,"Documento Adjunto corresponde al comentario # "+ comentario.getId().toString(), 300, 60, 0);
                over.endText();
	            stamp.close();

 	    }else if (extension.equalsIgnoreCase(".png")||extension.equalsIgnoreCase(".jpg")||extension.equalsIgnoreCase(".jpeg")||extension.equalsIgnoreCase(".bmp")){
	    	Document documento = new Document();
	    	FileOutputStream ficheroPdf = new FileOutputStream(path+archivoPdf);

	    	PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(1);
	    	documento.open();
			Font font = new Font(Font.COURIER, 13, Font.BOLD);
	    	documento.add(new Paragraph("Documento Adjunto corresponde al comentario # " +comentario.getId().toString(), font));

	    	Image foto = Image.getInstance(fileManagerService.downloadFile(
	    			comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo().obtenerNombreArchivoFisico(), 
	    			comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo().getIdToControlArchivo().toString()));
	    	foto.scaleToFit(400, 400);
	    	foto.setAlignment(Chunk.ALIGN_MIDDLE);
	    	documento.add(foto);
	    	documento.close();
	   }
     }
		 }//if
    }//for
	 
	   if(comentario.getId()!=null && comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo()!=null){  
		    InputStream reporteDocumento = this.getClass().getResourceAsStream(
			"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CaratulaDocumento.jrxml");
			JasperReport reporteDatosCompile = JasperCompileManager.compileReport(reporteDocumento);					
			JRBeanCollectionDataSource datosDocumentoDS = new JRBeanCollectionDataSource(listacomentario);
			inputByteArray.add(JasperRunManager.runReportToPdf(reporteDatosCompile, parametros, datosDocumentoDS)); 
			transporteImpresionDTO=getFileFromDisk(path+comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo()
                                                       .getNombreArchivoOriginal().substring(0, comentario.getDocumentoDigitalSolicitudDTO()
                                                       .getControlArchivo().getNombreArchivoOriginal().indexOf("."))+"_stamper.pdf");

			inputByteArray.add(transporteImpresionDTO.getByteArray());
		  }
		return transporteImpresionDTO;
	}catch(Exception ex){
		throw new RuntimeException(ex);
	}
}

public TransporteImpresionDTO imprimirDocumento(ControlArchivoDTO controlArchivo,Locale locale,String extension) {
	List<byte[]> inputByteArray = new ArrayList<byte[]>();
	Map<String, Object> parametros = new HashMap<String, Object>();	
	TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
	if(locale != null){
		parametros.put(JRParameter.REPORT_LOCALE, locale);
	}
	try{
		 String path=getPath();
		   String archivoPdf=null;
		          archivoPdf=controlArchivo.getNombreArchivoOriginal().substring(0, controlArchivo.getNombreArchivoOriginal().indexOf("."))+"_stamper.pdf";

	     if(extension.equalsIgnoreCase(".pdf")) {
	    	 
	    	 	PdfReader reader = new PdfReader(fileManagerService.downloadFile(controlArchivo.obtenerNombreArchivoFisico(), controlArchivo.getIdToControlArchivo().toString()));
	            PdfStamper stamp = new PdfStamper(reader, 
	            new FileOutputStream(path+archivoPdf));
	
	            PdfContentByte over = null;
	            over = stamp.getOverContent(1);
	            over.beginText();
	            BaseFont bf_times = BaseFont.createFont(BaseFont.COURIER, "Cp1252", false);
	            over.setFontAndSize(bf_times, 13);
	            over.showTextAligned(PdfContentByte.ALIGN_CENTER,"Documento Adjunto corresponde al Documento # "+ controlArchivo.getIdToControlArchivo().toString(), 300, 60, 0);
                over.endText();
	            stamp.close();

 	    }else if (extension.equalsIgnoreCase(".png")||extension.equalsIgnoreCase(".jpg")||extension.equalsIgnoreCase(".jpeg")||extension.equalsIgnoreCase(".bmp")){
	    	Document documento = new Document();
	    	FileOutputStream ficheroPdf = new FileOutputStream(path+archivoPdf);

	    	PdfWriter.getInstance(documento,ficheroPdf).setInitialLeading(1);
	    	
	    	documento.open();
	    	documento.add(new Paragraph("Documento Adjunto corresponde al Documento # " +controlArchivo.getIdToControlArchivo().toString() ));

	    	Image foto = Image.getInstance(fileManagerService.downloadFile(controlArchivo.obtenerNombreArchivoFisico(), controlArchivo.getIdToControlArchivo().toString()));
	    	foto.scaleToFit(400, 400);
	    	foto.setAlignment(Chunk.ALIGN_MIDDLE);
	    	documento.add(foto);
	    	documento.close();
	   }
     
    
	 
	   if(controlArchivo!=null){  
		    InputStream reporteDocumento = this.getClass().getResourceAsStream(
			"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CaratulaDocumento.jrxml");
			JasperReport reporteDatosCompile = JasperCompileManager.compileReport(reporteDocumento);					
			JRBeanCollectionDataSource datosDocumentoDS = new JRBeanCollectionDataSource(new ArrayList<Comentario>(1));
			inputByteArray.add(JasperRunManager.runReportToPdf(reporteDatosCompile, parametros, datosDocumentoDS)); 
			transporteImpresionDTO=getFileFromDisk(path+archivoPdf);

			inputByteArray.add(transporteImpresionDTO.getByteArray());
		  }
		return transporteImpresionDTO;
	}catch(Exception ex){
		throw new RuntimeException(ex);
	}
}

	@Override
	public TransporteImpresionDTO llenarSolicitudDTO(BigDecimal idToSolicitud,Locale locale){
		
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		List<Comentario> comentariosDocumentosList= new ArrayList<Comentario>();
	    comentariosDocumentosList = entidadService.findByProperty(Comentario.class, "solicitudDTO.idToSolicitud",idToSolicitud);
	    List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudList = entidadService.findByProperty(DocumentoDigitalSolicitudDTO.class, "solicitudDTO.idToSolicitud", idToSolicitud);
	    if(documentoDigitalSolicitudList == null){
	    	documentoDigitalSolicitudList = new ArrayList<DocumentoDigitalSolicitudDTO>(1);
	    }
		List<byte[]> inputByteArray = new ArrayList<byte[]>();		
		ByteArrayOutputStream output = new ByteArrayOutputStream();	
		SolicitudDTO datosCaratulaSolicitud = new SolicitudDTO();
		String extension=null;

		try{  
		SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class, idToSolicitud);

		datosCaratulaSolicitud.setIdToSolicitud(idToSolicitud);
		datosCaratulaSolicitud.setFolio(idToSolicitud);
		datosCaratulaSolicitud.setNombrePersona(solicitud.getNombrePersona());
		datosCaratulaSolicitud.setApellidoPaterno(solicitud.getApellidoPaterno());
		datosCaratulaSolicitud.setApellidoMaterno(solicitud.getApellidoMaterno());
		datosCaratulaSolicitud.setNombreAgente(solicitud.getNombreAgente());
		datosCaratulaSolicitud.setNombreNegocio(solicitud.getNegocio().getDescripcionNegocio());
		datosCaratulaSolicitud.setTipodeMovimiento(solicitud.getDescripcionTipoSolicitud());
		datosCaratulaSolicitud.setFechaCreacion(solicitud.getFechaCreacion());
		datosCaratulaSolicitud.setNombreEjecutivo(solicitud.getNombreEjecutivo());
		if(solicitud.getCodigoAgente() != null){
			try{
				Agente agente = entidadService.findById(Agente.class, solicitud.getCodigoAgente().longValue());
				datosCaratulaSolicitud.setCodigoAgente(new BigDecimal(agente.getIdAgente()));
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		datosCaratulaSolicitud.setNombreProducto(solicitud.getProductoDTO().getNombreComercial());
		datosCaratulaSolicitud.setEmailContactos(solicitud.getEmailContactos());
		datosCaratulaSolicitud.setNombreDescripcionEstatus(solicitud.getDescripcionEstatus());		
		String nombreCompleto = "";
		if(solicitud.getNombrePersona() != null){
			nombreCompleto += solicitud.getNombrePersona();
		}
		if(solicitud.getApellidoPaterno() != null){
			nombreCompleto += " "  + solicitud.getApellidoPaterno();
		}
		if(solicitud.getApellidoMaterno() != null){
			nombreCompleto += " " + solicitud.getApellidoMaterno();
		}
		datosCaratulaSolicitud.setNombreCompleto(nombreCompleto.toUpperCase());
		if(solicitud.getClaveTipoPersona().intValue()==1){
		 datosCaratulaSolicitud.setTituloCliente("Nombre del Cliente");
		}
		
		if(solicitud.getClaveTipoPersona().intValue()==2){
			 datosCaratulaSolicitud.setTituloCliente("Razon Social");
		}
		
		if(solicitud.getCodigoUsuarioCreacion() != null){
			Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(solicitud.getCodigoUsuarioCreacion());
			if(usuario != null && usuario.getNombreCompleto() != null){
				datosCaratulaSolicitud.setNombreUsuarioSolicitante(usuario.getNombreCompleto().toUpperCase());
			}
		}
		
		inputByteArray.add(imprimirSolicitud(datosCaratulaSolicitud, locale));
		
		if(!comentariosDocumentosList.isEmpty()){			
			for(Comentario comentario : comentariosDocumentosList){
			 if(comentario.getDocumentoDigitalSolicitudDTO() != null && comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo()!=null)	{
				String fileName=comentario.getDocumentoDigitalSolicitudDTO().getControlArchivo().getNombreArchivoOriginal();
				extension =   (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());				 
				if (extension.equalsIgnoreCase(".jpg")||
					extension.equalsIgnoreCase(".bmp")||
			        extension.equalsIgnoreCase(".gif")||
					extension.equalsIgnoreCase(".jpeg")||
					extension.equalsIgnoreCase(".png")||
					extension.equalsIgnoreCase(".pdf")){
				    if(comentario.getId()!=null){
			        inputByteArray.add(this.imprimirDocumento(comentario, locale,extension).getByteArray());
				 }else{
				  break; 
				 }
			  }
				
				if(comentario.getDocumentoDigitalSolicitudDTO() != null){
					for(int i = 0; i < documentoDigitalSolicitudList.size(); i++){
						DocumentoDigitalSolicitudDTO documento = documentoDigitalSolicitudList.get(i);
						if(comentario.getDocumentoDigitalSolicitudDTO().getIdToDocumentoDigitalSolicitud().equals(documento.getIdToDocumentoDigitalSolicitud())){
							documentoDigitalSolicitudList.remove(i);
							break;
						}
					}
				}
		    }
		  }
		}
		
		for(int i = 0; i < documentoDigitalSolicitudList.size(); i++){
				DocumentoDigitalSolicitudDTO documento = documentoDigitalSolicitudList.get(i);
				
				String fileName=documento.getControlArchivo().getNombreArchivoOriginal();
				extension =   (fileName.lastIndexOf(".") == -1) ? "" : fileName.substring(fileName.lastIndexOf("."), fileName.length());				 
				if (extension.equalsIgnoreCase(".jpg")||
					extension.equalsIgnoreCase(".bmp")||
			        extension.equalsIgnoreCase(".gif")||
					extension.equalsIgnoreCase(".jpeg")||
					extension.equalsIgnoreCase(".png")||
					extension.equalsIgnoreCase(".pdf")){
			        inputByteArray.add(this.imprimirDocumento(documento.getControlArchivo(), locale,extension).getByteArray());
				 }
		}
		 pdfConcantenate(inputByteArray, output);
		 transporte.setByteArray(output.toByteArray());
	   }catch(Exception ex){
		throw new RuntimeException(ex);
	   }
	   
	   return transporte;	
	}

	public TransporteImpresionDTO getFileFromDisk(String path){
		  File file = null;
		  TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO(); 
		  byte[] byteArray = null;
		  try{		 
		    file = new File(path);		    
		    byteArray = this.getBytesFromFile(file);		    
		  }catch(IOException ex){		    
		  }
		  transporteImpresionDTO.setByteArray(byteArray);
		  return transporteImpresionDTO;
	}
	
	
	private byte[] getBytesFromFile(File file) throws IOException {
		InputStream is = null; 
		try {
			is = new FileInputStream(file);  
			long length = file.length();	  
			if (length > Integer.MAX_VALUE) {    
				throw new IOException("File \"" + file + 
						"\" is too large to process.");
			}
			return IOUtils.toByteArray(is);
		} finally {
			IOUtils.closeQuietly(is);
		}
	}	

	public void  pdfConcantenate(List inputByteArray, OutputStream outputStream){  
	      try {
	          int pageOffset = 0;    
	          int f = 0;     
	          ArrayList master = new ArrayList();
	          Document document = null;
	          PdfCopy  writer = null;
	          Iterator iterator = inputByteArray.iterator();	          
	          while(iterator.hasNext()){   
	            byte[] data = (byte[])iterator.next();
	            if(data == null || data.length == 0){
	              f++;
	              continue;
	            }
	            PdfReader reader = new PdfReader(data);
	            reader.consolidateNamedDestinations();
	            int n = reader.getNumberOfPages();
	            List bookmarks = SimpleBookmark.getBookmark(reader);
	            if (bookmarks != null) {
	              if (pageOffset != 0)
	                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	                master.addAll(bookmarks);
	            }	            
	            pageOffset += n;	                
	            if (f == 0) {
	              document = new Document(reader.getPageSizeWithRotation(1));
	                                  
	              writer = new PdfCopy(document, outputStream);
	             
	              document.open();           
	            }
	            PdfImportedPage page;
	            for (int i = 0; i < n; ) {
	              ++i;
	              page = writer.getImportedPage(reader, i);
	              writer.addPage(page);
	            }	  
	            PRAcroForm form = reader.getAcroForm();
	            if (form != null){
	              writer.copyAcroForm(reader);
	            }
	            f++;
	          }
	          if (!master.isEmpty()){
	            writer.setOutlines(master);
	          }
	          if(document != null){
	            document.close();
	          }
	      }
	      catch(Exception e) {	          
	      }	      
	  }	
	

	
	
	
}
