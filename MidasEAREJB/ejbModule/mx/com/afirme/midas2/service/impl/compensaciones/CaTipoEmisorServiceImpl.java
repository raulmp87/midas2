/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoEmisorDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEmisor;
import mx.com.afirme.midas2.service.compensaciones.CaTipoEmisorService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoEmisorServiceImpl  implements CaTipoEmisorService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaTipoEmisorDao tipoEmisorcaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoEmisorServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoEmisor entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoEmisor entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoEmisor entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoEmisorcaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoEmisor entity.
	  @param entity CaTipoEmisor entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoEmisor entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	delete	::	INICIO	::	");
	        try {
        	//entity = entityManager.getReference(CaTipoEmisor.class, entity.getId());
	        	tipoEmisorcaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoEmisor entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoEmisor entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoEmisor entity to update
	 @return CaTipoEmisor the persisted CaTipoEmisor entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoEmisor update(CaTipoEmisor entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoEmisor result = tipoEmisorcaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoEmisor 	::		CaTipoEmisorServiceImpl	::	update	::	ERROR	::	",re);
	        throw re;
        }
    }
    
    public CaTipoEmisor findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoEmisorServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoEmisor instance = tipoEmisorcaDao.findById(id);//entityManager.find(CaTipoEmisor.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoEmisorServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoEmisorServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaTipoEmisor entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoEmisor property to query
	  @param value the property value to match
	  	  @return List<CaTipoEmisor> found by query
	 */
    public List<CaTipoEmisor> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoEmisorServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoEmisor model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoEmisorServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoEmisorcaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoEmisorServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoEmisor> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoEmisor> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoEmisor> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoEmisor> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoEmisor entities.
	  	  @return List<CaTipoEmisor> all CaTipoEmisor entities
	 */
	public List<CaTipoEmisor> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoEmisorServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoEmisor model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoEmisorServiceImpl	::	findAll	::	FIN	::	");
			return tipoEmisorcaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoEmisorServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}