package mx.com.afirme.midas2.service.impl.negocio.zonacirculacion;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.zonaCirculacion.NegocioEstadoDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
import mx.com.afirme.midas2.dto.negocio.zonacirculacion.RelacionesNegocioZonaCirculacion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.NegocioEstadoService;


@Stateless 
public class NegocioEstadoServiceImpl implements NegocioEstadoService {
	private EntidadService entidadService;
	protected EstadoFacadeRemote estadoFacadeRemote;
	private EntidadDao catalogoDao;
	private NegocioEstadoDao negocioEstadoDao;
	

	@Override
	public RelacionesNegocioZonaCirculacion getRelationLists(Long idToNegocio) {
		Negocio negocio=new Negocio();
		negocio = entidadService.findById(Negocio.class, idToNegocio);
           
		List<NegocioEstado> negocioEstadoAsociadas = new ArrayList<NegocioEstado>(1);
		
	      negocioEstadoAsociadas =  this.getPoridToNegocio(idToNegocio);


	    String country_id = "PAMEXI";
        List<EstadoDTO> posibles = estadoFacadeRemote.findByProperty("countryId", country_id);
        
		List<NegocioEstado> negocioEstadoDisponibles = new ArrayList<NegocioEstado>(1);

		RelacionesNegocioZonaCirculacion relacionesNegocioZonaCirculacion = new RelacionesNegocioZonaCirculacion();

		List<EstadoDTO> tipoestadosAsociados = new ArrayList<EstadoDTO>(1);

		for (NegocioEstado item : negocioEstadoAsociadas) {
			tipoestadosAsociados.add(item.getEstadoDTO());
		}

		for (EstadoDTO item : posibles) {
			if (!tipoestadosAsociados.contains(item)) {
				NegocioEstado negocioestado = new NegocioEstado();	
				negocioestado.setEstadoDTO(item);
				negocio.setIdToNegocio(idToNegocio);
			if(!item.getStateName().equalsIgnoreCase("DESCONOCIDO"))	{
				negocioEstadoDisponibles.add(negocioestado);
			 }
		    }
		}
		
		relacionesNegocioZonaCirculacion.setAsociadas(negocioEstadoAsociadas);
		relacionesNegocioZonaCirculacion.setDisponibles(negocioEstadoDisponibles);
		return relacionesNegocioZonaCirculacion;
	 }

  	List<NegocioEstado> getPoridToNegocio(Long idToNegocio) {
		List<NegocioEstado> negocioEstado = new ArrayList<NegocioEstado>();
		negocioEstado = entidadService.findByProperty(
				NegocioEstado.class, "negocio.idToNegocio", idToNegocio);

		return negocioEstado;

	}
  	
  	
  	
	@Override
	public Map<Long, String> getNegocioEstadosPorNegocioId(Long idToNegocio){
		Map<Long, String>  map = new LinkedHashMap<Long, String>();
		
		List<NegocioEstado> negocioEstado = new ArrayList<NegocioEstado>();
		negocioEstado = entidadService.findByProperty(
				NegocioEstado.class, "negocio.idToNegocio", idToNegocio);
		
        for(NegocioEstado item :negocioEstado ){
            map.put(item.getId(), item.getEstadoDTO().getStateName());
        }
        return map;
}	
  	
	@Override
	public Long relacionarNegocio(String accion, NegocioEstado negocioestado) {
		
		EstadoDTO estadoDTO =null;
		estadoDTO = entidadService.findById(EstadoDTO.class, negocioestado.getEstadoDTO().getStateId());
		if(negocioestado.getId()!=null){
			List<NegocioMunicipio> negocioMunicipioList  = entidadService.findByProperty(NegocioMunicipio.class, "negocioEstado.id", negocioestado.getId());
			negocioestado.setNegocioMunicipioList(negocioMunicipioList);
		}
		if(estadoDTO!=null){
	       negocioestado.setEstadoDTO(estadoDTO);
		}
		try{
			catalogoDao.executeActionGrid(accion, negocioestado);
		}catch(RuntimeException e){
			return negocioestado.getId();
		}
		return null;
	}
	
	/**
	 * Obtiene un listado de los estados
	 * relacionados con un negocio
	 * @param idToNegocio
	 * @return List<NegocioEstado>
	 * @author martin
	 */
	@Override
	public List<NegocioEstado> obtenerEstadosPorNegocioId(Long idToNegocio) {
		HashMap<String,Object> properties = new HashMap<String,Object>();
		properties.put("negocio.idToNegocio",idToNegocio);
			return negocioEstadoDao.findByPropertiesWithOrder(NegocioEstado.class, properties, "estadoDTO.stateName ASC");
	}
	
	@EJB
	public void setCatalogoService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}


	

	@EJB
	public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}




	@EJB
	public void setCatalogoDao(EntidadDao catalogoDao) {
		this.catalogoDao = catalogoDao;
	}
	
	@EJB
	public void setNegocioEstadoDao(NegocioEstadoDao negocioEstadoDao) {
		this.negocioEstadoDao = negocioEstadoDao;
	}
	
	
}
