package mx.com.afirme.midas2.service.impl.pago;

import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dto.pago.ConfiguracionPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoTarjetaCreditoDTO;
import mx.com.afirme.midas2.dto.pago.PagosAfirmeDefinicion;
import mx.com.afirme.midas2.dto.pago.ReciboDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaIbsDTO;
import mx.com.afirme.midas2.service.pago.IbsService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

import com.afirme.commons.exception.CommonException;
import com.afirme.commons.model.dao.AS400DAO;
import com.afirme.commons.model.vo.ResultVO;
import com.afirme.commons.model.vo.SourceVO;
import com.afirme.commons.sources.SourceHandler;
import com.afirme.commons.util.Utils;
import com.js.util.StringUtil;

import datapro.eibs.beans.ELEERRMessage;
import datapro.eibs.beans.ESEGPA6001Message;

@Stateless
public class IbsServiceImpl extends AS400DAO implements IbsService {
	
	private static final Logger LOG = Logger.getLogger("java.util.logging.Logger");
	
	protected UsuarioService usuarioService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public RespuestaIbsDTO consultarSaldoAplicarCargoTarjetaCredito(ConfiguracionPagoDTO pago, ReciboDTO recibo){
		RespuestaIbsDTO respuestaIbs = null;
		SourceVO objlSource = null;
		ResultVO objlResult = null;
		ESEGPA6001Message objlESEGPA6001Message = null;
		CuentaPagoDTO tarjetaCredito = null;
		try {
			cargarFuentesSocket();
			tarjetaCredito = pago.getInformacionCuenta();
			objlSource = getSource("appCreditCharges_executeCrediCardCharge", PagosAfirmeDefinicion.USER);
			objlSource.addField("P6TARJETA", tarjetaCredito.getCuentaNoEncriptada());
			objlSource.addField("P6IMPORTE", recibo.getCantidad().toString());
			objlSource.addField("P6FECEXP", tarjetaCredito.getFechaVencimiento());
			objlSource.addField("P6RECIBO", recibo.getFolioRecibo().getNumRecibo().toString());
			objlSource.addField("P6DIGITO", recibo.getFolioRecibo().getDigitoVerificador().toString());
			objlSource.addField("P6CVV", tarjetaCredito.getCodigoSeguridad());
			objlSource.addField("P6FORMPAGO", tarjetaCredito.getTipoPromocion());
			LOG.setLevel(org.apache.log4j.Level.DEBUG);			
			objlResult = getResult(objlSource, LOG);
			respuestaIbs = setBussinesErrors(objlResult);
			// Si no hay error obtener el numero de authorizacion.
			if (!Utils.isNull(objlResult) && !objlResult.hasErrorBean()) {
				objlESEGPA6001Message = (ESEGPA6001Message) objlResult.getData();
				if (objlESEGPA6001Message != null && objlESEGPA6001Message.getP6AUTOR() != null) {
					respuestaIbs.setIbsNumAutorizacion(objlESEGPA6001Message.getP6AUTOR());				
					LogDeMidasEJB3.getLogger().log(Level.INFO, "Numero de autorizacion cobro tarjeta " + 
							tarjetaCredito.getCuenta() + ": " + respuestaIbs.getIbsNumAutorizacion());
				}else{
					LogDeMidasEJB3.getLogger().log(Level.SEVERE, "Numero de autorizacion no obtenedio para cobro en tarjeta " + 
							tarjetaCredito.getCuenta());
	        		respuestaIbs.setIbsNumAutorizacion("12345"); //PARA EFECTOS DE DESARROLLO
	        	}
			}
		} catch (Exception e) {
			respuestaIbs = new RespuestaIbsDTO();
			respuestaIbs.setCodigoErrorIBS("9999");
			respuestaIbs.setDescripcionErrorIBS("No fue posible hacer el cargo en la tarjeta de cr\u00E9dito " + 
					(StringUtil.isEmpty(e.getMessage()) ? "." : ": " + e.getMessage()));
		}
		return respuestaIbs;
	}

	@Override
	@Deprecated
	public RespuestaIbsDTO consultarSaldoTarjetaCredito(
			ConfiguracionPagoDTO pago,  ReciboDTO recibo) {
		RespuestaIbsDTO respuestaIbs = null;
		SourceVO objlSource = null;
		ResultVO objlResult = null;
		ESEGPA6001Message objlESEGPA6001Message = null;
		CuentaPagoDTO tarjetaCredito = null;
	    try {
	    	cargarFuentesSocket();
	    	tarjetaCredito = pago.getInformacionCuenta();
	    	objlSource = getSource("creditCardBalanceEnquiry", PagosAfirmeDefinicion.USER);
	    	objlSource.addField("P6TARJETA", tarjetaCredito.getCuentaNoEncriptada());
	    	objlSource.addField("P6IMPORTE", recibo.getCantidad().toString());
		    objlSource.addField("P6FECEXP", tarjetaCredito.getFechaVencimiento());
		    objlSource.addField("P6RECIBO", recibo.getFolioRecibo().getNumRecibo().toString());
		    objlSource.addField("P6CVV", tarjetaCredito.getCodigoSeguridad());
		    LOG.setLevel(org.apache.log4j.Level.DEBUG);			
			objlResult = getResult(objlSource, LOG);
		    respuestaIbs = setBussinesErrors(objlResult);
		     
		    //Gets authorization number if the transaction was successful 
		    if (!Utils.isNull(objlResult) && !objlResult.hasErrorBean()) {
		    	objlESEGPA6001Message = (ESEGPA6001Message) objlResult.getData();
		        if (objlESEGPA6001Message != null && objlESEGPA6001Message.getP6AUTOR() != null) {		          
		        	respuestaIbs.setIbsNumAutorizacion(objlESEGPA6001Message.getP6AUTOR());	
		        	LogDeMidasEJB3.getLogger().log(Level.INFO, "Numero de autorizacion cobro tarjeta " + 
							tarjetaCredito.getCuenta() + ": " + respuestaIbs.getIbsNumAutorizacion());
		        }else{
		        	LogDeMidasEJB3.getLogger().log(Level.SEVERE, "Numero de autorizacion no obtenedio para cobro en tarjeta " + 
							tarjetaCredito.getCuenta());
	        		respuestaIbs.setIbsNumAutorizacion("12345"); //PARA EFECTOS DE DESARROLLO
	        	}	
		    }
	    } catch (Exception e) {
	    	respuestaIbs.setCodigoErrorIBS("9999");
	    	respuestaIbs.setDescripcionErrorIBS("No fue posible consultar el saldo de la tarjeta de cr\u00E9dito "+ 
					(StringUtil.isEmpty(e.getMessage()) ? "." : ": " + e.getMessage()));
	    }
	    return respuestaIbs;
	}

	@Override
	@Deprecated
	public RespuestaIbsDTO confirmarCargoTarjetaCredito(
			ConfiguracionPagoDTO pago, ReciboDTO recibo) {
		RespuestaIbsDTO respuestaIbs = null;
	    SourceVO objlSource = null;
	    ResultVO objlResult = null;
	    ESEGPA6001Message objlESEGPA6001Message = null;
	    CuentaPagoTarjetaCreditoDTO tarjetaCredito = null;
	    try {
	    	cargarFuentesSocket();
	    	tarjetaCredito = (CuentaPagoTarjetaCreditoDTO)pago.getInformacionCuenta();
	    	objlSource = getSource("creditCardApplyCharge", PagosAfirmeDefinicion.USER);
	    	objlSource.addField("P6TARJETA", tarjetaCredito.getCuentaNoEncriptada());
	    	objlSource.addField("P6IMPORTE", recibo.getCantidad().toString());
	    	objlSource.addField("P6FECEXP", tarjetaCredito.getFechaVencimiento());      
	    	objlSource.addField("P6CVV", tarjetaCredito.getCodigoSeguridad());
	    	objlSource.addField("P6AUTOR", pago.getReferenciaPago()); //num autorizacion
	    	objlSource.addField("P6RECIBO", recibo.getFolioRecibo().getNumRecibo().toString());  
	    	objlSource.addField("P6DIGITO", recibo.getFolioRecibo().getDigitoVerificador().toString());
	    	objlSource.addField("P6FORMPAGO", tarjetaCredito.getTipoPromocion());
	    	LOG.setLevel(org.apache.log4j.Level.DEBUG);			
			objlResult = getResult(objlSource, LOG);
	    	respuestaIbs = setBussinesErrors(objlResult);	      
	    	//Gets authorization number if the transaction was successful
	    	if (!Utils.isNull(objlResult) && !objlResult.hasErrorBean()) {
	    		objlESEGPA6001Message = (ESEGPA6001Message) objlResult.getData();
	    		if (objlESEGPA6001Message != null && objlESEGPA6001Message.getP6AUTOR() != null) {
	    			respuestaIbs.setIbsNumAutorizacion(objlESEGPA6001Message.getP6AUTOR());	    
	    			LogDeMidasEJB3.getLogger().log(Level.INFO, "Numero de autorizacion cobro tarjeta " + 
							tarjetaCredito.getCuenta() + ": " + respuestaIbs.getIbsNumAutorizacion());
	    		}else{
	    			LogDeMidasEJB3.getLogger().log(Level.SEVERE, "Numero de autorizacion no obtenedio para cobro en tarjeta " + 
							tarjetaCredito.getCuenta());
	        		respuestaIbs.setIbsNumAutorizacion("12345"); //PARA EFECTOS DE DESARROLLO
	        	}
	    	}	        
	    } catch (Exception e) {
	      respuestaIbs.setCodigoErrorIBS("9999");
	      respuestaIbs.setDescripcionErrorIBS("No fue posible confirmar el cargo sobre la tarjeta de cr\u00E9dito " + 
					(StringUtil.isEmpty(e.getMessage()) ? "." : ": " + e.getMessage())); 	
	    }
	    return respuestaIbs;
	}

	@Override
	@Deprecated
	public RespuestaIbsDTO cancelarCargoTarjetaCredito(ConfiguracionPagoDTO pago, ReciboDTO recibo) {
		RespuestaIbsDTO respuestaIbs = new RespuestaIbsDTO();
		SourceVO objlSource = null;
	    ResultVO objlResult = null;
	    ESEGPA6001Message objlESEGPA6001Message = null;
	    CuentaPagoTarjetaCreditoDTO tarjetaCredito = null;
	    try {
	    	cargarFuentesSocket();
	    	tarjetaCredito = (CuentaPagoTarjetaCreditoDTO)pago.getInformacionCuenta();
	    	objlSource = getSource("creditCardCancellCharge", PagosAfirmeDefinicion.USER);  
		    objlSource.addField("P6TARJETA", tarjetaCredito.getCuentaNoEncriptada());
		    objlSource.addField("P6IMPORTE", recibo.getCantidad().toString());
		    objlSource.addField("P6FECEXP", tarjetaCredito.getFechaVencimiento());      
		    objlSource.addField("P6CVV", tarjetaCredito.getCodigoSeguridad());
		    objlSource.addField("P6AUTOR", pago.getReferenciaPago()); //se carga num autorizacion
		    objlSource.addField("P6RECIBO", recibo.getFolioRecibo().getNumRecibo().toString());  
		    objlSource.addField("P6DIGITO", recibo.getFolioRecibo().getDigitoVerificador().toString());      
		    LOG.setLevel(org.apache.log4j.Level.DEBUG);			
			objlResult = getResult(objlSource, LOG);
		    respuestaIbs = setBussinesErrors(objlResult);
	     
		    //Gets authorization number if the transaction was successful 
		    if (!Utils.isNull(objlResult) && !objlResult.hasErrorBean()) {
	        	objlESEGPA6001Message = (ESEGPA6001Message) objlResult.getData();
	        	if (objlESEGPA6001Message != null && objlESEGPA6001Message.getP6AUTOR() != null) {
	        		respuestaIbs.setIbsNumAutorizacion(objlESEGPA6001Message.getP6AUTOR());	 
	        		LogDeMidasEJB3.getLogger().log(Level.INFO, "Numero de autorizacion cobro tarjeta " + 
							tarjetaCredito.getCuenta() + ": " + respuestaIbs.getIbsNumAutorizacion());
	        	}else{
	        		LogDeMidasEJB3.getLogger().log(Level.SEVERE, "Numero de autorizacion no obtenedio para cobro en tarjeta " + 
							tarjetaCredito.getCuenta());
	        		respuestaIbs.setIbsNumAutorizacion("12345"); //PARA EFECTOS DE DESARROLLO
	        	}
		    }	      
	    } catch (Exception e) {
	      respuestaIbs.setCodigoErrorIBS("9999");
	      respuestaIbs.setDescripcionErrorIBS("No fue posible cancelar la petici\u00F3n a PROSA: " + 
					(StringUtil.isEmpty(e.getMessage()) ? "." : ": " + e.getMessage())); 	   
	    }
	    return respuestaIbs;	 
	}

	@Override
	public RespuestaIbsDTO consultarSaldoCuentaAfirme(ConfiguracionPagoDTO pago,  ReciboDTO recibo) {
		RespuestaIbsDTO respuestaIbs = new RespuestaIbsDTO();
		SourceVO objlSource = null;
		ResultVO objlResult = null;
		try {
			cargarFuentesSocket();			
			objlSource = getSource("cargoAutomatico", PagosAfirmeDefinicion.USER);
			objlSource.addField("H01OPECOD", "4");// codigo de Operacion Consulta de saldo
			objlSource.addField("PGSERVICIO", "6");// "6" //Servicio para seguros
			objlSource.addField("PGCONVENIO", "601");// "601" //Convenio para seguros
			objlSource.addField("PGREF", "1");// "1" //Referencia para seguros
			objlSource.addField("PGCUENTA", pago.getInformacionCuenta().getCuentaNoEncriptada());
			objlSource.addField("PGIMPORTE", recibo.getCantidad().toString());
			LOG.setLevel(org.apache.log4j.Level.DEBUG);			
			objlResult = getResult(objlSource, LOG);
			respuestaIbs = setBussinesErrors(objlResult);				
		} catch (Exception e) {				
			LogDeMidasEJB3.log("Error al consultar el saldo de la cuenta afirme", Level.SEVERE, e);
			respuestaIbs.setCodigoErrorIBS("9999");
			respuestaIbs.setDescripcionErrorIBS("No fue posible consultar el saldo para la cuenta " + 
					pago.getInformacionCuenta().getCuenta() + 
					(StringUtil.isEmpty(e.getMessage()) ? "." : ": " + e.getMessage()) + " Importe del primer Recibo [" + recibo.getCantidad().toString() + "]");
		}
		return respuestaIbs;
	}

	@Override
	public RespuestaIbsDTO aplicarCargoCuentaAfirme(ConfiguracionPagoDTO pago, ReciboDTO recibo) {
		RespuestaIbsDTO respuestaIbs = new RespuestaIbsDTO();
		SourceVO objlSource = null;
		ResultVO objlResult = null;
		try{
			cargarFuentesSocket();		
			objlSource = getSource("cargoAutomatico", PagosAfirmeDefinicion.USER);
			objlSource.addField("H01OPECOD", "0001");// codigo de Operacion cargo a cuenta
			objlSource.addField("PGSERVICIO", "6");// "6" //Servicio para seguros
			objlSource.addField("PGCONVENIO", "601");// "601" //Convenio para seguros
			objlSource.addField("PGREF", recibo.getFolioRecibo().getFolioConDigito()); //Referencia para seguros
			objlSource.addField("PGMONEDA", "MXP");
			objlSource.addField("PGCUENTA", pago.getInformacionCuenta().getCuentaNoEncriptada());
			objlSource.addField("PGIMPORTE", recibo.getCantidad().toString());
			objlSource.addField("PGPOL", pago.getReferenciaPago()); //num poliza
			objlSource.addField("PGFECHA", new SimpleDateFormat("yyyyMMdd")
				.format(new Date()));// Es la fecha de realizacion de la  operacion
			// falta poner los campos de anio, mes y codigo de seguridad si es
			// tarjeta de debito.
			LOG.setLevel(org.apache.log4j.Level.DEBUG);			
			objlResult = getResult(objlSource, LOG);			
			respuestaIbs = setBussinesErrors(objlResult);
			if (!Utils.isNull(objlResult) && !objlResult.hasErrorBean()) {				
				respuestaIbs.setIbsNumAutorizacion(pago.getInformacionCuenta().getCuentaNoEncriptada());												
			}
		} catch (Exception e) {
			respuestaIbs.setCodigoErrorIBS("9999");
			respuestaIbs.setDescripcionErrorIBS("Ocurri\u00F3 un error al tratar de aplicar el cargo para el recibo " + 
					recibo.getFolioRecibo().getNumRecibo() + 
					(StringUtil.isEmpty(e.getMessage()) ? "." : ": " + e.getMessage()) + " Importe del primer Recibo [" + recibo.getCantidad().toString() + "]");
		}
		return respuestaIbs;
	}

	@Override
	@Deprecated
	public RespuestaIbsDTO programarCargoCuentaAfirme(ConfiguracionPagoDTO pago, ReciboDTO recibo) {
		RespuestaIbsDTO respuestaIbs = new RespuestaIbsDTO();
		SourceVO objlSource = null;
		ResultVO objlResult = null;	
		try{
			cargarFuentesSocket();								
			objlSource = getSource("pagoProgramado", PagosAfirmeDefinicion.USER);
			objlSource.addField("H01OPECOD", "0001");// codigo de Operacion
			objlSource.addField("EGBANCO", "01"); // 01 Afirme
			objlSource.addField("EGSUCURSAL", recibo.getRamo());
			objlSource.addField("EGMONEDA", "MXP");
			objlSource.addField("EGPOLIZA", pago.getReferenciaPago());
			objlSource.addField("EGRECIBO", recibo.getFolioRecibo().getNumRecibo().toString());
			objlSource.addField("EGDIGITO", recibo.getFolioRecibo().getDigitoVerificador().toString());
			objlSource.addField("EGCTACGO", pago.getInformacionCuenta().getCuentaNoEncriptada());
			objlSource.addField("EGFECCUOTA", recibo.getFechaVencimiento());
			objlSource.addField("EGNUMCUOTA", recibo.getNumConsecutivo().toString());
			objlSource.addField("EGIMPCUOTA", recibo.getCantidad().toString());
			objlSource.addField("EGUSUARIO", usuarioService.getUsuarioActual().getNombreUsuario());
			objlSource.addField("EGFECOPER", new SimpleDateFormat("yyyyMMdd").format(new Date()));
			objlSource.addField("EGHOROPER", this.getTime());
			objlSource.addField("EGORIG", "I");// I- internet default
			objlSource.addField("EGTIPO", String.valueOf(pago.getOrigenCobro().valor()));
			objlSource.addField("EGDSCPAGO", pago.getDescripcionPago());
			LOG.setLevel(org.apache.log4j.Level.DEBUG);			
			objlResult = getResult(objlSource, LOG);
			respuestaIbs = setBussinesErrors(objlResult);					
		}catch (Exception e) {
			respuestaIbs.setCodigoErrorIBS("9999");
			respuestaIbs.setDescripcionErrorIBS("No fue posible hacer la programaci\u00F3n del recibo " + 
					recibo.getFolioRecibo().getNumRecibo() + 
					(StringUtil.isEmpty(e.getMessage()) ? "." : ": " + e.getMessage()));							
		}
		return respuestaIbs;
	}

	/**
	 * Cargar archivos fuentes para comunicacion a trav�s del socket
	 */
	protected void cargarFuentesSocket(){		
		//String sources = "mx/com/afirme/midas2/service/impl/pago/resources/sources.xml";
		String sources = "D:/apps/profile3/sources.xml";
		InputStream objlInputStream = null;
		objlInputStream = this.getClass().getClassLoader().getResourceAsStream(sources);
		SourceHandler.getInstance().reLoad(objlInputStream);
		SourceHandler.getInstance().reLoad(sources);
		IOUtils.closeQuietly(objlInputStream);
	}
	
	/**
	  * Obtener errores prvocados por el socket
	  * @param result
	  * @param paymentDTO
	  * @throws CommonException
	  */
	protected RespuestaIbsDTO setBussinesErrors(ResultVO result)
			throws CommonException {
		RespuestaIbsDTO respuestaIbsDTO = new RespuestaIbsDTO();			
		ELEERRMessage businessErrors = null;
		String errorMessage = null;
		String errorCode = null;
		LogDeMidasEJB3.getLogger().log(Level.INFO, "hasErrorBean : " + result.hasErrorBean());		
		if (!Utils.isNull(result) && result.hasErrorBean()) {
			businessErrors = (ELEERRMessage) result.getData();
			if (!businessErrors.getERRNUM().equals("0")) {
				if (!Utils.isNull(businessErrors.getERNU01())) {
					errorCode = businessErrors.getERNU01();
					errorMessage = businessErrors.getERDS01();
				} else if (!Utils.isNull(businessErrors.getERNU02())) {
					errorCode = businessErrors.getERNU02();
					errorMessage = businessErrors.getERDS02();
				} else if (!Utils.isNull(businessErrors.getERNU03())) {
					errorCode = businessErrors.getERNU03();
					errorMessage = businessErrors.getERDS03();
				} else if (!Utils.isNull(businessErrors.getERNU04())) {
					errorCode = businessErrors.getERNU04();
					errorMessage = businessErrors.getERDS04();
				} else if (!Utils.isNull(businessErrors.getERNU05())) {
					errorCode = businessErrors.getERNU05();
					errorMessage = businessErrors.getERDS05();
				} else if (!Utils.isNull(businessErrors.getERNU06())) {
					errorCode = businessErrors.getERNU06();
					errorMessage = businessErrors.getERDS06();
				} else if (!Utils.isNull(businessErrors.getERNU07())) {
					errorCode = businessErrors.getERNU07();
					errorMessage = businessErrors.getERDS07();
				} else if (!Utils.isNull(businessErrors.getERNU08())) {
					errorCode = businessErrors.getERNU08();
					errorMessage = businessErrors.getERDS08();
				} else if (!Utils.isNull(businessErrors.getERNU09())) {
					errorCode = businessErrors.getERNU09();
					errorMessage = businessErrors.getERDS09();
				} else if (!Utils.isNull(businessErrors.getERNU10())) {
					errorCode = businessErrors.getERNU10();
					errorMessage = businessErrors.getERDS10();
				}
			}
			LogDeMidasEJB3.getLogger().log(Level.INFO, "IBS ERROR : (" + errorCode + ") " + errorMessage);		
		} else {
			errorCode = String.valueOf(0);
		}
		respuestaIbsDTO.setCodigoErrorIBS(errorCode);
		respuestaIbsDTO.setDescripcionErrorIBS(errorMessage);	
		return respuestaIbsDTO;
	}
		
	protected String getTime() {
		Calendar calendario = new GregorianCalendar();
		int hora, minutos, segundos;
		hora = calendario.get(Calendar.HOUR_OF_DAY);
		minutos = calendario.get(Calendar.MINUTE);
		segundos = calendario.get(Calendar.SECOND);
		return String.valueOf(hora) + String.valueOf(minutos)
				+ String.valueOf(segundos);
	}

}
