package mx.com.afirme.midas2.service.impl.excepcion;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.excepcion.ExcepcionSuscripcionDao;
import mx.com.afirme.midas2.domain.excepcion.CondicionExcepcion;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.excepcion.ValorExcepcion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

@Stateless
public class ExcepcionSuscripcionServiceImpl implements ExcepcionSuscripcionService {

	EntidadService entidad;
	
	ExcepcionSuscripcionDao excepcionDao;
		
	@EJB
	public void setExcepcionDao(ExcepcionSuscripcionDao excepcionDao) {
		this.excepcionDao = excepcionDao;
	}

	@EJB
	public void setEntidad(EntidadService entidad) {
		this.entidad = entidad;
	}
	
	@Override
	public void agregarCondicion(Long idToExcepcion, TipoCondicion tipoCondicion, TipoValor tipoValor, List<String> valores) {
		final Integer accesor = tipoCondicion.valor();	
		CondicionExcepcion condicion = null;
		ValorExcepcion valorExcepcion = null;
		boolean salvarCondicion = false;	
		//remover condicion si existe
		ExcepcionSuscripcion excepcion = entidad.findById(ExcepcionSuscripcion.class, idToExcepcion);		
		excepcion.setCondicionExcepcion(excepcionDao.obtenerCondiciones(idToExcepcion));
		removerCondicion(idToExcepcion, tipoCondicion);		
		Predicate remove = new Predicate() {					
			@Override
			public boolean evaluate(Object arg0) {
				CondicionExcepcion condicionExcepcion = (CondicionExcepcion)arg0;				
				return !(condicionExcepcion.getTipoCondicion().intValue() == accesor.intValue());
			}
		};
		CollectionUtils.filter(excepcion.getCondicionExcepcion(), remove);
		
		//revisar si existen valores para insertar
		for(String str : valores){
			if(str != null && !str.equals("")){
				salvarCondicion = true;
			}
		}		
		if(salvarCondicion){		
			//guardar nuevos valores
			//ExcepcionSuscripcion excepcion = entidad.findById(ExcepcionSuscripcion.class, idToExcepcion);
			condicion = new CondicionExcepcion();
			condicion.setTipoCondicion(tipoCondicion.valor());
			condicion.setTipoValor(tipoValor.valor());			
 			for(String valor : valores){
				if(valor != null && !valor.equals("")){
					valorExcepcion = new ValorExcepcion();
					valorExcepcion.setValor(valor);
					valorExcepcion.setCondicionExcepcion(condicion);
					condicion.addValor(valorExcepcion);				
				}
			} 	
 			//System.out.println(condicion.getValorExcepcions().size());
 			excepcion.addCondicion(condicion);
 			excepcion = entidad.save(excepcion);
 			//System.out.println(excepcion.getCondicionExcepcion().size());
 			
		}
	}
	
	@Override
	public Long guardarExcepcion(ExcepcionSuscripcion excepcion) {
		if(excepcion.getNivelEvaluacion() == null){
			excepcion.setNivelEvaluacion(0);
		}
		return (Long)entidad.saveAndGetId(excepcion);		
	}

	@Override
	public ExcepcionSuscripcion listarExcepcion(Long idToExcepion) {
		return entidad.findById(ExcepcionSuscripcion.class, idToExcepion);
	}

	@Override
	public List<ExcepcionSuscripcion> listarExcepciones(String cveNegocio) {
		return this.listarExcepciones(cveNegocio, null);
	}
	
	@Override
	public List<ExcepcionSuscripcion> listarExcepciones(String cveNegocio, NivelEvaluacion nivelEvaluacion) {
		return excepcionDao.listarExcepciones(cveNegocio, nivelEvaluacion != null ? nivelEvaluacion.valor() : null);
	}
	
	@Override
	public List<ExcepcionSuscripcion> listarExcepciones(ExcepcionSuscripcion filtro) {
		return excepcionDao.listarExcepciones(filtro);
	}

	@Override
	public CondicionExcepcion obtenerCondicion(Long idToExcepcion, TipoCondicion tipoCondicion) {
		CondicionExcepcion condicion = excepcionDao.obtenerCondicion(idToExcepcion, tipoCondicion.valor());
		if(condicion != null){
			condicion.setValorExcepcions(excepcionDao.obtenerValoresDeCondicion(condicion.getId()));
		}
		return condicion;
	}
	
	@Override
	public void removerValoresDeCondicion(Long idToExcepcion, TipoCondicion tipoCondicion) {
		CondicionExcepcion condicion = null;
		try{
			condicion = excepcionDao.obtenerCondicion(idToExcepcion, tipoCondicion.valor());			
			for(ValorExcepcion valor : condicion.getValorExcepcions()){
				entidad.remove(valor);
			}
		}catch(Exception ex){
		}		
	}
	
	@Override
	public void removerCondicion(Long idToExcepcion, TipoCondicion tipoCondicion){
		CondicionExcepcion condicion = excepcionDao.obtenerCondicion(idToExcepcion, tipoCondicion.valor());
		if(condicion != null){
			entidad.remove(condicion);
		}
	}

	@Override
	public void removerCondicion(Long idToExcepcion, Long idToCondicion) {
		CondicionExcepcion condicion = entidad.findById(CondicionExcepcion.class, idToCondicion);
		for(ValorExcepcion valor : condicion.getValorExcepcions()){
			entidad.remove(valor);
		}
		entidad.remove(condicion);
	}

	@Override
	public void removerExcepcion(Long idToExcepcion) {
		ExcepcionSuscripcion excepcion = entidad.findById(ExcepcionSuscripcion.class, idToExcepcion);
		entidad.remove(excepcion);
	}

	@Override
	public List<CondicionExcepcion> obtenerCondiciones(Long idExcepcion) {
		return excepcionDao.obtenerCondiciones(idExcepcion);
	}

}
