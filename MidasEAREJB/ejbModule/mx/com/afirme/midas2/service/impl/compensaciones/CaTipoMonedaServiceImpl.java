/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;


import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoMonedaDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoMoneda;
import mx.com.afirme.midas2.service.compensaciones.CaTipoMonedaService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoMonedaServiceImpl  implements CaTipoMonedaService{
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@EJB
	private CaTipoMonedaDao tipoMonedacaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoMonedaServiceImpl.class);    
	
		/**
	 Perform an initial save of a previously unsaved CaTipoMoneda entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoMoneda entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoMoneda entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoMonedacaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoMoneda entity.
	  @param entity CaTipoMoneda entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoMoneda entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoMoneda.class, entity.getId());
	        	tipoMonedacaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoMoneda entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoMoneda entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoMoneda entity to update
	 @return CaTipoMoneda the persisted CaTipoMoneda entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoMoneda update(CaTipoMoneda entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoMoneda result = tipoMonedacaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoMoneda 	::		CaTipoMonedaServiceImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaTipoMoneda findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoMonedaServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoMoneda instance = tipoMonedacaDao.findById(id);//entityManager.find(CaTipoMoneda.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoMonedaServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoMonedaServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaTipoMoneda entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoMoneda property to query
	  @param value the property value to match
	  	  @return List<CaTipoMoneda> found by query
	 */
    public List<CaTipoMoneda> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoMonedaServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoMoneda model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoMonedaServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoMonedacaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoMonedaServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoMoneda> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoMoneda> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoMoneda> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoMoneda> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoMoneda entities.
	  	  @return List<CaTipoMoneda> all CaTipoMoneda entities
	 */
	public List<CaTipoMoneda> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoMonedaServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoMoneda model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoMonedaServiceImpl	::	findAll	::	FIN	::	");
			return tipoMonedacaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoMonedaServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}