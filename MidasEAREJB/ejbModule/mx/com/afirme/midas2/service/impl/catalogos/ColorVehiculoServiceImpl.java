package mx.com.afirme.midas2.service.impl.catalogos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.com.afirme.midas2.dao.catalogos.ColorVehiculoDao;
import mx.com.afirme.midas2.domain.catalogos.ColorVehiculo;
import mx.com.afirme.midas2.service.catalogos.ColorVehiculoService;


@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ColorVehiculoServiceImpl extends EntidadServiceImpl implements ColorVehiculoService {


	public ColorVehiculo findByClave(Integer clave) {
		
		ColorVehiculo filtroColorVehiculo = new ColorVehiculo();
		
		filtroColorVehiculo.setClave(clave);
		
		List<ColorVehiculo> colorVehiculoList = colorVehiculoDao.findByFilters(filtroColorVehiculo);
		
		if (colorVehiculoList != null && colorVehiculoList.size() > 0) {
			return colorVehiculoList.get(0);
		}
		return null;
	}

	public List<ColorVehiculo> findByFilters(ColorVehiculo filtroColorVehiculo) {
		if(filtroColorVehiculo==null)filtroColorVehiculo = new ColorVehiculo();
		return colorVehiculoDao.findByFilters(filtroColorVehiculo);
	}



	public ColorVehiculo save(ColorVehiculo colorVehiculo) {
		
		if (colorVehiculo.getId() != null) {
			return colorVehiculoDao.update(colorVehiculo);
		} else {
			colorVehiculoDao.persist(colorVehiculo);
			return colorVehiculo;
		}
	}

	
	private ColorVehiculoDao colorVehiculoDao;
	
	@EJB
	public void setColorVehiculoDao(ColorVehiculoDao colorVehiculoDao) {
		this.colorVehiculoDao = colorVehiculoDao;
	}
	

}
