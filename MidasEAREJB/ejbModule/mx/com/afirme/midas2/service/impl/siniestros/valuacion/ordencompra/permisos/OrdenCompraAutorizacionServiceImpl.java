/**
 * 
 */
package mx.com.afirme.midas2.service.impl.siniestros.valuacion.ordencompra.permisos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.ASMUserDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.pagos.PagosSiniestroDao;
import mx.com.afirme.midas2.dao.siniestros.valuacion.ordencompra.OrdenCompraDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacionId;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraPermiso;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.permisos.OrdenCompraPermisoUsuario;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.AutorizacionesUsuarioDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.OrdenesCompraAutorizarDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.UsuarioPermisoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacionService;
import mx.com.afirme.midas2.util.CollectionUtils;

import mx.com.afirme.midas2.dao.siniestros.valuacion.ordencompra.OrdenCompraDao;

import org.apache.commons.lang.StringUtils;

import com.asm.service.ASMConstants;

/**
 * @author admin
 *
 */
@Stateless
public class OrdenCompraAutorizacionServiceImpl implements OrdenCompraAutorizacionService {
	@EJB
	private OrdenCompraDao ordenCompraDao;	
	
	public OrdenCompraDao getOrdenCompraDao() {
		return ordenCompraDao;
	}



	public void setOrdenCompraDao(OrdenCompraDao ordenCompraDao) {
		this.ordenCompraDao = ordenCompraDao;
	}

	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	private ListadoService listadoService;
	
	@EJB
	private OrdenCompraService ordenCompraService;
		
	@EJB
	private PerdidaTotalService perdidaTotalService;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
	private RecuperacionDeducibleService recuperacionDeducibleService;
	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	
	
	@Override
	public String autorizarOrdenCompra(Long idOrdenCompra) {
		String mensaje = null;
		
		Map<String, Object> parameters						= new HashMap<String, Object>();
		List<OrdenCompraPermiso> listaOrdenCompraPermiso 	= null;
		OrdenCompra ordenCompra 							= null;
		OrdenCompraAutorizacion ordenCompraAutorizacion 	= null;
		OrdenCompraAutorizacionId ordenCompraAutorizacionId = null;
		String usuarioActual 								= "";
		String tipoOrdenCompra 								= "";
		Long idOficina 										= 0l;
		Long idConceptoAjuste 								= 0l;
		Long idtoseccion									= 0l;
		Long idtocobertura	 								= 0l;
		boolean usuarioConPermiso 							= Boolean.FALSE;
		
		ordenCompra = entidadService.findById(OrdenCompra.class, idOrdenCompra);
		List<DetalleOrdenCompra> lstdetalleOrden = this.entidadService.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", idOrdenCompra);
		if(lstdetalleOrden.isEmpty()){
			mensaje="Autorizacion denegada, La orden de compra no tiene conceptos para autorizar";
			return mensaje;
			
		}
		if (!validateCantidadesOrdenCompraDetalle(ordenCompra)){
			mensaje="Autorizacion denegada, los conceptos no deben tener montos en ceros.";
			return mensaje;
		}
		
		mensaje=validateRangoMontosOrdenCompraDetalle(idOrdenCompra);
		if(!StringUtils.isEmpty(mensaje)){
			return mensaje;
		}
		
		
		usuarioActual	= String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() );
		
		
		for(DetalleOrdenCompra detalle : lstdetalleOrden ){
			
			if(validateDetallePorAutorizar(idOrdenCompra, detalle.getId())){
			
				ordenCompraAutorizacion = new OrdenCompraAutorizacion();
				ordenCompraAutorizacionId = new OrdenCompraAutorizacionId();
				parameters.clear();
				
				idOficina 			= detalle.getOrdenCompra().getReporteCabina().getOficina().getId();
				idConceptoAjuste 	= detalle.getConceptoAjuste().getId();
				tipoOrdenCompra 	= detalle.getOrdenCompra().getTipo();
				
				parameters.put("oficina.id", idOficina);
				parameters.put("tipo", tipoOrdenCompra);
				parameters.put("concepto.id", idConceptoAjuste);
				parameters.put("estatus", 1);
				
				if ( OrdenCompraAutorizacionService.TIPO_ORDEN_COMPRA_AFECTACION_RESERVA.equals( tipoOrdenCompra ) ){
					idtoseccion = detalle.getOrdenCompra().getReporteCabina().getSeccionReporteCabina().getSeccionDTO().getIdToSeccion().longValue();
					idtocobertura = detalle.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getIdToCobertura().longValue();
					parameters.put("coberturaSeccion.id.idtoseccion", idtoseccion);
					parameters.put("coberturaSeccion.id.idtocobertura", idtocobertura);
				}
					
				listaOrdenCompraPermiso = entidadService.findByProperties(OrdenCompraPermiso.class, parameters);
				
				if(listaOrdenCompraPermiso !=null && listaOrdenCompraPermiso.size()>0){
					for(OrdenCompraPermiso ordenCompraPermiso : listaOrdenCompraPermiso ){
						
						usuarioConPermiso =  usuarioConPermiso(usuarioActual, ordenCompraPermiso.getId());
						if(usuarioConPermiso){
							ordenCompraAutorizacionId.setNivel( ordenCompraPermiso.getNivel());
							
							if(ordenCompraPermiso.isValidarMontos()){
								if( validateImporteIsInRange(detalle.getImporte(), ordenCompraPermiso.getMontoMax(), ordenCompraPermiso.getMontoMin()) ){
									ordenCompraAutorizacion.setEstatus( OrdenCompraAutorizacionService.ESTATUS_AUTORIZADO);
									break;
								}else{
									mensaje="Autorizacion denegada, Concepto: "+detalle.getConceptoAjuste().getNombre()+" fuera del Rango autorizado";
									usuarioConPermiso=false;
									ordenCompraAutorizacion.setEstatus(OrdenCompraAutorizacionService.ESTATUS_PENDIENTE);
									break;
								}
							}else{
								ordenCompraAutorizacion.setEstatus( OrdenCompraAutorizacionService.ESTATUS_AUTORIZADO);
								break;
							}
							
						}else{
							mensaje="El usuario no tiene permisos para autorizar todos los conceptos de la lista.";
						}
						
					}
					
					if( usuarioConPermiso   ){
						ordenCompraAutorizacionId.setIdDetalleOrdenCOmpra( detalle.getId());
						ordenCompraAutorizacionId.setIdOrdenCompra( detalle.getOrdenCompra().getId());
						ordenCompraAutorizacion.setId(ordenCompraAutorizacionId);
						ordenCompraAutorizacion.setOrderCompra(detalle.getOrdenCompra());
						ordenCompraAutorizacion.setDetalleOrdenCompra(detalle);
						ordenCompraAutorizacion.setCodigoUsuario(usuarioActual);
						
						
						ordenCompraAutorizacion.setFechaAutorizacion( new Date());
						entidadService.save(ordenCompraAutorizacion);						
					}
					
				}else{
					mensaje="El usuario no tiene permisos para autorizar todos los conceptos de la lista.";
				}
				
				if( validateOrdenCompraAceptada(ordenCompra) ){
					ordenCompra.setEstatus(OrdenCompraService.ESTATUS_AUTORIZADA);
					ordenCompraService.guardarOrdenCompra( ordenCompra);				
					ImportesOrdenCompraDTO importes= this.ordenCompraService.calcularImportes(ordenCompra.getId(),null,false);
					
					//Se modifica para no generar el movimiento de gasto de ajuste
					/*	
					if(ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_GASTOAJUSTE)){
						
						
						movimientoSiniestroService.generarMovimientoGA(ordenCompra.getReporteCabina().getId(),
								ordenCompra.getId(), importes.getTotal(), TipoDocumentoMovimiento.PAGO, TipoMovimiento.PAGO_GASTOAJUSTE, 
								 CausaMovimiento.GASTO_AJUSTE, null, null);
					}else if (ordenCompra.getTipo().equalsIgnoreCase(OrdenCompraService.TIPO_REMBOLSO_GASTOAJUSTE)){
						
						movimientoSiniestroService.generarMovimientoGA(ordenCompra.getReporteCabina().getId(),
								ordenCompra.getId(), importes.getTotal(), TipoDocumentoMovimiento.PAGO, TipoMovimiento.PAGO_REEMBOLSOGASTOAJUSTE, 
								CausaMovimiento.REEMBOLSO_GASTO_AJUSTE, null, null);

					}
					*/
					mensaje="La orden de compra ha sido autorizada.";
					if ( OrdenCompraAutorizacionService.TIPO_ORDEN_COMPRA_AFECTACION_RESERVA.equals( tipoOrdenCompra ) ){
//						perdidaTotalService.generarIndemnizacion(ordenCompra);
						//Identificar que la orden de compra sea de tipo OC y revisar si existe deducible en status REGISTRADA para generar su ingreso
							RecuperacionDeducible recuperacion = recuperacionDeducibleService.obtenerRecuperacionPorEstimacionId(
									ordenCompra.getIdTercero());
							if(recuperacion != null 
									&& recuperacion.getEstatus().equals(Recuperacion.EstatusRecuperacion.REGISTRADO.toString())
									&& ordenCompra.getEstatus().equalsIgnoreCase(OrdenCompraService.ESTATUS_AUTORIZADA)){
								recuperacionDeducibleService.generarIngreso(recuperacion);
							}
					}
				}	
			
			}
		} // Fin For detalles 
		return mensaje;
		
		
	}
	
	@SuppressWarnings("static-access")
	@Override
	public void rechazarOrdenCompra(Long idOrdenCompra) {
		OrdenCompraAutorizacion ordenCompraAutorizacion 	= null;
		OrdenCompraAutorizacionId ordenCompraAutorizacionId = null;
		OrdenCompra ordenCompra 							= null;
		String usuarioActual 								= "";
		
		ordenCompra = entidadService.findById(OrdenCompra.class, idOrdenCompra);
		usuarioActual	= String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() );
		List<DetalleOrdenCompra> lstdetalleOrden = this.entidadService.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", ordenCompra.getId());

		for(DetalleOrdenCompra detalle : lstdetalleOrden ){
			
			ordenCompraAutorizacion = new OrdenCompraAutorizacion();
			ordenCompraAutorizacionId = new OrdenCompraAutorizacionId();
			
			ordenCompraAutorizacionId.setNivel(0);
			ordenCompraAutorizacionId.setIdDetalleOrdenCOmpra( detalle.getId());
			ordenCompraAutorizacionId.setIdOrdenCompra( detalle.getOrdenCompra().getId());
			ordenCompraAutorizacion.setId(ordenCompraAutorizacionId);
			ordenCompraAutorizacion.setOrderCompra(detalle.getOrdenCompra());
			ordenCompraAutorizacion.setDetalleOrdenCompra(detalle);
			ordenCompraAutorizacion.setCodigoUsuario(usuarioActual);
			ordenCompraAutorizacion.setEstatus( OrdenCompraAutorizacionService.ESTATUS_RECHAZADO);
			ordenCompraAutorizacion.setFechaAutorizacion( new Date());
			entidadService.save(ordenCompraAutorizacion);		
			
		}
		ordenCompra.setEstatus(OrdenCompraService.ESTATUS_RECHAZADA);
		ordenCompraService.guardarOrdenCompra( ordenCompra);
	
		
	}
	
	private boolean validateDetallePorAutorizar(Long idOrdenCompra, Long idDetalleCompra ){
		String estatusDetalle		= "";
		boolean result 				= Boolean.FALSE;
		
		estatusDetalle = obtenerEstatusDetalleCompra(idOrdenCompra, idDetalleCompra);
		
		if( OrdenCompraAutorizacionService.ESTATUS_PENDIENTE.equals( estatusDetalle) ){
			result = Boolean.TRUE;
		}
		
		return result;
	}
	
	private boolean validateOrdenCompraAceptada( OrdenCompra ordenCompra ){
		
		Map<String, Object> parameters							= new HashMap<String, Object>();
		List<OrdenCompraAutorizacion> listaDetallesAutorizados 	= null;
		boolean resultado 										= Boolean.FALSE;
		
		parameters.put("orderCompra.id", ordenCompra.getId());
		parameters.put("estatus", OrdenCompraAutorizacionService.ESTATUS_AUTORIZADO);
		
		listaDetallesAutorizados = entidadService.findByProperties(OrdenCompraAutorizacion.class, parameters);
		List<DetalleOrdenCompra> lstdetalleOrden = this.entidadService.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", ordenCompra.getId());

		if(lstdetalleOrden!= null && !lstdetalleOrden.isEmpty() && 
			 listaDetallesAutorizados.size() == lstdetalleOrden.size() ){
			resultado = Boolean.TRUE;
		}
		
		return resultado;
	}
	
	private boolean validateCantidadesOrdenCompraDetalle( OrdenCompra ordenCompra 	 ){
		boolean resultado=true;
		List<DetalleOrdenCompra> lstdetalleOrden = this.entidadService.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", ordenCompra.getId());
		for (DetalleOrdenCompra detalle :lstdetalleOrden){
			BigDecimal zero = new BigDecimal (0);
			if (null==detalle.getImporte()|| detalle.getImporte().compareTo(zero)!=1){
				
				return false;
				
			}
			if (null==detalle.getCostoUnitario()|| detalle.getCostoUnitario().compareTo(zero)!=1){
				return false;
				
			}
		}
		
		return resultado;
	}
	
	
	private boolean usuarioConPermiso(String usuarioActual, Long idOrdenCompraPermiso){
		Map<String, Object> parameters						= new HashMap<String, Object>();
		List<OrdenCompraPermisoUsuario> permisoUsuarios 	= null;
		boolean result 										= Boolean.FALSE;
		
		parameters.put("codigoUsuario", usuarioActual);
		parameters.put("permiso.id", idOrdenCompraPermiso);
		
		permisoUsuarios = entidadService.findByProperties(OrdenCompraPermisoUsuario.class, parameters);
		if(permisoUsuarios != null && permisoUsuarios.size() >0){
			result = Boolean.TRUE;
		}
		
		return result;
	}
	
	
	private boolean validateImporteIsInRange(BigDecimal importe , BigDecimal montoMax , BigDecimal montoMin){
		Boolean result = Boolean.FALSE;
		
		if(  importe.compareTo(montoMin) == 0 || importe.compareTo(montoMin) == 1){
			if( montoMax.compareTo(importe) == 0 || montoMax.compareTo(importe)== 1 ){
				result = Boolean.TRUE;
			}
		}
		
		
		
		return result;
	}

	
	@Override
	public void autorizarOrdenCompra(String usuario, OrdenCompra ordenCompra) {
		// TODO Auto-generated method stub

	}


	@Override
	public void eliminarPermiso(Long idPermiso) throws Exception {
		OrdenCompraPermiso entidad = entidadService.findById(OrdenCompraPermiso.class, idPermiso);
		
		if(entidad.getPermisoUsuarios().size() == 0){
			entidadService.remove(entidad);
		}else{
			throw new Exception();
		}
		

	}


	@Override
	public boolean estaAutorizadoConcepto(DetalleOrdenCompra concepto) {
		// TODO Auto-generated method stub
		return false;
	}

	
	@Override
	public void guardarPermiso(OrdenCompraPermiso permiso) {
		String codigoUsuarioCreacion		= String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() );
		
		if(permiso.getId()!= null && permiso.getId()>0){
			editarPermiso(permiso);
		}else{
			permiso.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
			entidadService.save(permiso);
		}		
	}
	
	
	@Override
	public OrdenCompraPermiso obtenerPermisoById(Long idPermiso) {
		return entidadService.findById(OrdenCompraPermiso.class, idPermiso);	
	}
	
	
	@Override
	public void editarPermiso(OrdenCompraPermiso permiso) {
		
		OrdenCompraPermiso entidadGuardada 		= entidadService.findById(OrdenCompraPermiso.class, permiso.getId());
		String codigoUsuarioModificacion		= String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() );
		
		entidadGuardada.setEstatus(permiso.getEstatus());
		entidadGuardada.setNivel(permiso.getNivel());
		entidadGuardada.setValidarMontos(permiso.isValidarMontos());
		entidadGuardada.setMontoMax(permiso.getMontoMax());
		entidadGuardada.setMontoMin(permiso.getMontoMin());
		entidadGuardada.setCodigoUsuarioModificacion(codigoUsuarioModificacion);
		entidadGuardada.setFechaModificacion( new Date());
		
		entidadService.save(entidadGuardada);
	}
	
	
	@Override
	public void eliminarAutorizacionesDetalle(Long idOrdenCompra, Long idDetalleOrdenCompra){
		
	List<OrdenCompraAutorizacion> listaDetallesAutorizados 	= null;
	Map<String, Object> parameters							= new HashMap<String, Object>();
	
	parameters.put("orderCompra.id", idOrdenCompra);
	parameters.put("detalleOrdenCompra.id", idDetalleOrdenCompra);
	
	listaDetallesAutorizados = entidadService.findByProperties(OrdenCompraAutorizacion.class, parameters);
	
	if(listaDetallesAutorizados != null && listaDetallesAutorizados.size()>0){
		entidadService.removeAll(listaDetallesAutorizados);
	}
	
	
	}
	

	
	@Override
	public Integer obtenerNivelAutorizacion(DetalleOrdenCompra concepto) {
		// TODO Auto-generated method stub
		return null;
	}

	
	@SuppressWarnings("unchecked")
	@Override
	public List<UsuarioPermisoDTO> obtenerUsuariosDisponibles( Long ordenCompraPermisoId) {
		List<UsuarioPermisoDTO> resultado = new ArrayList<UsuarioPermisoDTO>();	
		try {
			resultado = obtenerUsuariosASM("1,2,3");
		} catch (Exception e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try {
			List<String> roles = new ArrayList<String>();
			List<CatValorFijo> valores = catalogoGrupoValorService.obtenerCatalogoValores(TIPO_CATALOGO.ROLES_USUARIOS_PERMISOS_ORDENESCOMPRA, null);			
			for(CatValorFijo cat : valores){
				roles.add(cat.getDescripcion());							
			}
			List<Usuario> usuarios = usuarioService
					.buscarUsuariosSinRolesPorNombreRol(
							ASMConstants.USERNAME_MIDAS_APP, "1",
							roles.toArray(new String[roles.size()]));
			for(Usuario usuario : usuarios){
				Map<String, Object> parameters = new HashMap<String, Object>();
				parameters.put("usuario", usuario.getNombreUsuario());
				parameters.put("idPermiso", ordenCompraPermisoId);
				List<OrdenCompraPermisoUsuario> permisos = entidadService.executeQueryMultipleResult(
						"SELECT model FROM OrdenCompraPermisoUsuario model WHERE model.codigoUsuario = :usuario AND model.permiso.id = :idPermiso ", 
						parameters);
				if(permisos == null || permisos.size() == 0){
					UsuarioPermisoDTO usuarioPermisoDTO = new UsuarioPermisoDTO();
					usuarioPermisoDTO.setIdUsuario(usuario.getId().longValue());
					usuarioPermisoDTO.setNombre( usuario.getNombreCompleto());
					usuarioPermisoDTO.setNombreUsuario( usuario.getNombreUsuario());
					usuarioPermisoDTO.setIdOrdenCompraPermiso(ordenCompraPermisoId);					
					if(!resultado.contains(usuarioPermisoDTO)){
						resultado.add(usuarioPermisoDTO);
					}
				}
			}
					
		}catch (Exception e){
			e.printStackTrace();
		}
		return resultado;
	}
	
	@SuppressWarnings("null")
	private List<UsuarioPermisoDTO> obtenerUsuariosASM(String roles) throws Exception
	{
		String[] atributosDTO = { "userId", "username", "fullname" };
		String[] columnasCursor = { "USER_ID", "USERNAME", "NAME"};
		
		StoredProcedureHelper storedHelper = null;
		String nombreSP = "MIDAS.SP_GET_USERS_ASM";
		LogDeMidasEJB3.log("Entrando a obtenerUsuariosASM..." + this, Level.INFO, null);
		
		try {
			storedHelper = new StoredProcedureHelper(
					nombreSP, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceMapeoResultados(
					"mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.permisos.ASMUserDTO",
					atributosDTO, columnasCursor);			
			storedHelper.estableceParametro("pRoles", roles);
			List<ASMUserDTO> listaResultado = storedHelper.obtieneListaResultados();
			List<UsuarioPermisoDTO> listUsuarioPermisoDTO = new ArrayList<UsuarioPermisoDTO>();
			UsuarioPermisoDTO usuarioPermisoDTO;
			for (ASMUserDTO user : listaResultado)
			{
				usuarioPermisoDTO = new UsuarioPermisoDTO();
				usuarioPermisoDTO.setIdUsuario(user.getUserId());
				usuarioPermisoDTO.setNombreUsuario(user.getUsername());
				usuarioPermisoDTO.setNombre(user.getFullname());
				listUsuarioPermisoDTO.add(usuarioPermisoDTO);
			}
			return listUsuarioPermisoDTO;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Ocurri� un error al consultar los usuarios. SP invocado: "+nombreSP, Level.SEVERE, e); 
			throw e;
		}	
		
	}
	
	private List<Usuario>  eliminarRepetidos( List<Usuario> usuarios ){
		Map<String, Object> mapUsuario	= new HashMap<String, Object>();
		List<Usuario> resultado 		= new ArrayList<Usuario>();
		
		for(Usuario usuario: usuarios){
			if(!mapUsuario.containsKey(usuario.getNombreUsuario())){
				mapUsuario.put(usuario.getNombreUsuario(), usuario);
				resultado.add(usuario);
			}
			
		}
		return resultado;
	}
	
	
	@Override
	public List<UsuarioPermisoDTO> obtenerUsuariosAsociados( Long ordenCompraPermisoId ) {
		OrdenCompraPermiso  ordenCompraPermiso	= new OrdenCompraPermiso();
		
		ordenCompraPermiso = this.obtenerPermisoById(ordenCompraPermisoId);
		ordenCompraPermiso.getPermisoUsuarios().size();
		entidadService.refresh(ordenCompraPermiso);
		return transformOrdenCompraPermisoUsuarioToUsuarioPermisoDTO( ordenCompraPermiso.getPermisoUsuarios() );
	}
	
	
	@Override
	public List<AutorizacionesUsuarioDTO> obtenerListaAutorizaciones(){
		List<OrdenCompraPermiso> listOrdenCompra = null;
		
		listOrdenCompra = entidadService.findByPropertiesWithOrder(OrdenCompraPermiso.class, null, "fechaCreacion desc");
		
		return transformToAutorizacionesUsuarioDTO(listOrdenCompra);
	}
	
	
	@Override
	public void guardarRelacionUsuario(UsuarioPermisoDTO usuario){
		OrdenCompraPermisoUsuario ordenCompraPermisoUsuario 	= new OrdenCompraPermisoUsuario();
		OrdenCompraPermiso  ordenCompraPermiso 					= new OrdenCompraPermiso();
		
		ordenCompraPermiso.setId( usuario.getIdOrdenCompraPermiso());
		
		ordenCompraPermisoUsuario.setPermiso(ordenCompraPermiso);
		ordenCompraPermisoUsuario.setCodigoUsuario(usuario.getNombreUsuario());
		ordenCompraPermisoUsuario.setFechaCreacion( new Date());
		entidadService.save(ordenCompraPermisoUsuario);
	}
	
	@Override
	public void eliminarRelacionUsuario( Long ordenCompraPermisoUsuarioId){
		OrdenCompraPermisoUsuario ordenCompraPermisoUsuario 	= new OrdenCompraPermisoUsuario();
		
		ordenCompraPermisoUsuario = entidadService.findById(OrdenCompraPermisoUsuario.class, ordenCompraPermisoUsuarioId);
		
		entidadService.remove(ordenCompraPermisoUsuario);
	}
	
	@Override
	public List<OrdenesCompraAutorizarDTO> obtenerListaOrdenesCompraPorAutorizarSP(){
		List<OrdenesCompraAutorizarDTO> lista = this.ordenCompraDao.getOrdenesCompraPorAuthViaSP();
		
		return lista;
	}
	
	@Override
	public List<OrdenesCompraAutorizarDTO> obtenerListaOrdenesCompraPorAutorizar(){
		Map<String, Object> parameters 							= new HashMap<String, Object>();	
		List<OrdenCompra>  listaOrdenCompras 					= new ArrayList<OrdenCompra>();
		List<OrdenesCompraAutorizarDTO>  listaDto 				= new ArrayList<OrdenesCompraAutorizarDTO>();
		OrdenesCompraAutorizarDTO ordenesCompraAutorizarDTO 	= null; 
		ImportesOrdenCompraDTO importes 						= null;
		
		parameters.put("estatus","Tramite");
		listaOrdenCompras.addAll( entidadService.findByPropertiesWithOrder(OrdenCompra.class, parameters,"id DESC") );
		
		
		for(OrdenCompra ordenCompra :listaOrdenCompras){
			ordenesCompraAutorizarDTO = new OrdenesCompraAutorizarDTO();
			
			ordenesCompraAutorizarDTO.setNoOrdenCompra( ordenCompra.getId().toString());
			
			if (null!=ordenCompra.getReporteCabina().getSiniestroCabina()){
				ordenesCompraAutorizarDTO.setNoSiniestro( ordenCompra.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
			}
			
			if( ordenCompra.getCoberturaReporteCabina() != null && ordenCompra.getCoberturaReporteCabina().getCoberturaDTO() != null){
				ordenesCompraAutorizarDTO.setCobertura( ordenCompra.getCoberturaReporteCabina().getCoberturaDTO().getNombreComercial());
			}
			
			//Se agrega consulta del tipo
			if(!StringUtil.isEmpty(ordenCompra.getTipo())){
				CatValorFijo cat=catalogoGrupoValorService.obtenerValorPorCodigo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA, ordenCompra.getTipo());
				if(null!=cat){
					ordenesCompraAutorizarDTO.setTipoOrdenCompra(cat.getDescripcion());
				} 
				
			}
			
			ordenesCompraAutorizarDTO.setConceptoPago(  ordenCompra.getDetalleConceptos());
			ordenesCompraAutorizarDTO.setFechaElaboracion( ordenCompra.getFechaCreacion());
			
			importes = ordenCompraService.calcularImportes(ordenCompra.getId(),null,false);
			ordenesCompraAutorizarDTO.setSubtotal( importes.getSubtotal());
			ordenesCompraAutorizarDTO.setIva( importes.getIva());
			ordenesCompraAutorizarDTO.setImporte( importes.getTotal());
			ordenesCompraAutorizarDTO.setTotalPagado( importes.getTotalPagado());
			ordenesCompraAutorizarDTO.setTotalPendiente( importes.getTotalPendiente());
			ordenesCompraAutorizarDTO.setEstatus( ordenCompra.getEstatus());
			
			listaDto.add( ordenesCompraAutorizarDTO );
		}
		
		return listaDto;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	@Override
	public String obtenerEstatusDetalleCompra(Long idOrdenCompra, Long idDetalleCompra){
		
		Map<String, Object> parameters 					= new HashMap<String, Object>();
		List<OrdenCompraAutorizacion> listaDetalles 	= null;
		String queryString 								= "";
		String resultado 								= "";
		
		queryString = "SELECT model FROM OrdenCompraAutorizacion AS model  "
				+ " WHERE model.orderCompra.id=:idOrdenCompra  "
				+ " AND model.detalleOrdenCompra.id = :idDetalleCompra  "
				+ " ORDER BY model.fechaAutorizacion DESC ";	
		parameters.put("idDetalleCompra", idDetalleCompra);
		parameters.put("idOrdenCompra", idOrdenCompra);

		listaDetalles = entidadService.executeQueryMultipleResult( queryString , parameters);
		
		if(listaDetalles != null && listaDetalles.size()>0){
			resultado = listaDetalles.get(0).getEstatus();
		}else{
			resultado = OrdenCompraAutorizacionService.ESTATUS_PENDIENTE;
		}
		return resultado;
	}
	
	
	
	private List<AutorizacionesUsuarioDTO> transformToAutorizacionesUsuarioDTO( List<OrdenCompraPermiso> listOrdenCompra ){
		List<AutorizacionesUsuarioDTO> listAutorizaccionesUsuario 	= new ArrayList<AutorizacionesUsuarioDTO>();
		AutorizacionesUsuarioDTO autorizacionesUsuarioDTO 			= null;
		Map<String,String> estautsMap 								= new HashMap<String,String>();
		Map<String,String> nivelMap 								= new HashMap<String,String>();
		Map<String,String> tipoOrdenCompraMap						= new HashMap<String,String>();
		
		estautsMap= listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS);
		nivelMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.NIVEL_DE_AUTORIZACION);
		tipoOrdenCompraMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		
		for(OrdenCompraPermiso ordenCompraPermiso :listOrdenCompra){
			autorizacionesUsuarioDTO = new AutorizacionesUsuarioDTO();
			autorizacionesUsuarioDTO.setOficina( ordenCompraPermiso.getOficina().getNombreOficina());
			autorizacionesUsuarioDTO.setEstatus( estautsMap.get(ordenCompraPermiso.getEstatus().toString() ) );
			autorizacionesUsuarioDTO.setFechaCreacion( ordenCompraPermiso.getFechaCreacion());
			autorizacionesUsuarioDTO.setNivelAutorizacion( nivelMap.get( ordenCompraPermiso.getNivel().toString() ));
			autorizacionesUsuarioDTO.setId( ordenCompraPermiso.getId());
			autorizacionesUsuarioDTO.setConcepto( ordenCompraPermiso.getConcepto().getNombre());
			autorizacionesUsuarioDTO.setTipo( tipoOrdenCompraMap.get( ordenCompraPermiso.getTipo()) );
			
			if(ordenCompraPermiso.getCoberturaSeccion() != null){
				if(ordenCompraPermiso.getCoberturaSeccion().getCoberturaDTO() != null){
					autorizacionesUsuarioDTO.setCobertura( ordenCompraPermiso.getCoberturaSeccion().getCoberturaDTO().getDescripcion());
				}
				if(ordenCompraPermiso.getCoberturaSeccion().getSeccionDTO() != null){
					autorizacionesUsuarioDTO.setSeccion( ordenCompraPermiso.getCoberturaSeccion().getSeccionDTO().getDescripcion());
				}
			}
			listAutorizaccionesUsuario.add(autorizacionesUsuarioDTO);
		}
		
		return listAutorizaccionesUsuario;
	}
	
	private List<UsuarioPermisoDTO> transformUsuarioToUsuarioPermisoDTO( List<Usuario> listaUsuarios, Long idOrdenCompraPermiso){
		
		List<UsuarioPermisoDTO> resultado 			= new ArrayList<UsuarioPermisoDTO>();
		UsuarioPermisoDTO usuarioPermisoDTO 		= null;
		OrdenCompraPermiso ordenCompraPermiso 		= null;
		boolean usuarioYaRelacionado 				= Boolean.FALSE;
		
		ordenCompraPermiso = obtenerPermisoById(idOrdenCompraPermiso);
		
		for(Usuario usuario : listaUsuarios){
			usuarioPermisoDTO = new UsuarioPermisoDTO();
			usuarioPermisoDTO.setIdUsuario(usuario.getId().longValue());
			usuarioPermisoDTO.setNombre( usuario.getNombreCompleto());
			usuarioPermisoDTO.setNombreUsuario( usuario.getNombreUsuario());
			usuarioPermisoDTO.setIdOrdenCompraPermiso(idOrdenCompraPermiso);
			
			if(!ordenCompraPermiso.getPermisoUsuarios().isEmpty()){
				for (OrdenCompraPermisoUsuario permisoUsuario :ordenCompraPermiso.getPermisoUsuarios()){
						if(permisoUsuario.getCodigoUsuario().equals(usuario.getNombreUsuario() )){
							usuarioYaRelacionado = Boolean.TRUE;
							break;
						}else{
							usuarioYaRelacionado = Boolean.FALSE;
						}
				}
			}
			
			if(!usuarioYaRelacionado){
				resultado.add(usuarioPermisoDTO);
			}
			
			
		}
		
		return resultado;
	}
	
	
	
	private List<UsuarioPermisoDTO> transformOrdenCompraPermisoUsuarioToUsuarioPermisoDTO( List<OrdenCompraPermisoUsuario> listaPermisos){
		List<UsuarioPermisoDTO> resultado 		= new ArrayList<UsuarioPermisoDTO>();
		UsuarioPermisoDTO usuarioPermisoDTO 	= null;
		
		for(OrdenCompraPermisoUsuario permiso : listaPermisos){
			usuarioPermisoDTO = new UsuarioPermisoDTO();
			usuarioPermisoDTO.setNombreUsuario( permiso.getCodigoUsuario());
			usuarioPermisoDTO.setNombre( usuarioService.buscarUsuarioPorNombreUsuario(permiso.getCodigoUsuario()).getNombreCompleto() );
			usuarioPermisoDTO.setIdUsuarioPermiso( permiso.getId());
			usuarioPermisoDTO.setIdOrdenCompraPermiso(permiso.getPermiso().getId());
			
			resultado.add(usuarioPermisoDTO);
		}
		
		return resultado;
	}
	

	private String validateRangoMontosOrdenCompraDetalle(Long idOrdenCompra){
		String mensaje = null;			
		Map<String, Object> parameters						= new HashMap<String, Object>();
		List<OrdenCompraPermiso> listaOrdenCompraPermiso 	= null;
		OrdenCompraAutorizacion ordenCompraAutorizacion 	= null;
		OrdenCompraAutorizacionId ordenCompraAutorizacionId = null;
		String tipoOrdenCompra 								= "";
		Long idOficina 										= 0l;
		Long idConceptoAjuste 								= 0l;
		Long idtoseccion									= 0l;
		Long idtocobertura	 								= 0l;
		List<DetalleOrdenCompra> lstdetalleOrden = this.entidadService.findByProperty(DetalleOrdenCompra.class, "ordenCompra.id", idOrdenCompra);
		
		for(DetalleOrdenCompra detalle : lstdetalleOrden ){	
			parameters.clear();
			parameters.put("orderCompra.id", idOrdenCompra);
			parameters.put("detalleOrdenCompra.id", detalle.getId());
			parameters.put("estatus", OrdenCompraAutorizacionService.ESTATUS_AUTORIZADO);
			List<OrdenCompraAutorizacion> listaOrdenCompraAutorizacion= entidadService.findByProperties(OrdenCompraAutorizacion.class, parameters);
			if(null!=listaOrdenCompraAutorizacion && !listaOrdenCompraAutorizacion.isEmpty()){
				continue;
			}	
			parameters.clear();
			
				idOficina 			= detalle.getOrdenCompra().getReporteCabina().getOficina().getId();
				idConceptoAjuste 	= detalle.getConceptoAjuste().getId();
				tipoOrdenCompra 	= detalle.getOrdenCompra().getTipo();				
				parameters.put("oficina.id", idOficina);
				parameters.put("tipo", tipoOrdenCompra);
				parameters.put("concepto.id", idConceptoAjuste);
				parameters.put("estatus", 1);
				if ( OrdenCompraAutorizacionService.TIPO_ORDEN_COMPRA_AFECTACION_RESERVA.equals( tipoOrdenCompra ) ){
					idtoseccion = detalle.getOrdenCompra().getReporteCabina().getSeccionReporteCabina().getSeccionDTO().getIdToSeccion().longValue();
					idtocobertura = detalle.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getIdToCobertura().longValue();
					parameters.put("coberturaSeccion.id.idtoseccion", idtoseccion);
					parameters.put("coberturaSeccion.id.idtocobertura", idtocobertura);
				}	
				listaOrdenCompraPermiso = entidadService.findByProperties(OrdenCompraPermiso.class, parameters);
				if(listaOrdenCompraPermiso !=null && listaOrdenCompraPermiso.size()>0){
					for(OrdenCompraPermiso ordenCompraPermiso : listaOrdenCompraPermiso ){						
							if(ordenCompraPermiso.isValidarMontos()){
								if( !validateImporteIsInRange(detalle.getImporte(), ordenCompraPermiso.getMontoMax(), ordenCompraPermiso.getMontoMin()) ){
									String max="N/A";
									String min = "N/A";
									if(null!=ordenCompraPermiso.getMontoMax() && ordenCompraPermiso.getMontoMax().compareTo(BigDecimal.ZERO)==1){
										 max="$"+ordenCompraPermiso.getMontoMax().toString();
									}
									if(null!=ordenCompraPermiso.getMontoMin() && ordenCompraPermiso.getMontoMin().compareTo(BigDecimal.ZERO)==1){
										 min="$"+ordenCompraPermiso.getMontoMin().toString();
									}
									mensaje="Autorizacion denegada, Concepto: "+detalle.getConceptoAjuste().getNombre()+" fuera del Rango autorizado.  Min: "+min+" Max: "+max;
									return mensaje;
								}
							}
					}
			
				} 
		}
		return mensaje;
	}
	
	/**
	 * Retornar las autorizaciones para una orden de compra
	 * @param ordenCompraId
	 * @return
	 */
	public List<OrdenCompraAutorizacion> obtenerAutorizacionesOrdenCompra(Long ordenCompraId){
		return entidadService.findByProperty(OrdenCompraAutorizacion.class, "orderCompra.id", ordenCompraId);
	}
	
	
	/**
	 * Retornar las autorizaciones para las ordenes de compra de una estimacion
	 * @param estimacionId
	 * @return
	 */
	public List<OrdenCompraAutorizacion> obtenerAutorizacionesEstimacion(Long estimacionId){
		List<OrdenCompraAutorizacion> autorizaciones = new ArrayList<OrdenCompraAutorizacion>();
		List<OrdenCompra> ordenes = entidadService.findByProperty(OrdenCompra.class, "idTercero", estimacionId);
		for(OrdenCompra orden : CollectionUtils.emptyIfNull(ordenes)){
			autorizaciones.addAll(this.obtenerAutorizacionesOrdenCompra(orden.getId()));
		}
		return autorizaciones;
	}
	
	
	/**
	 * Revisar si una orden de compra cuenta con autorizaciones
	 * @param ordenCompraId
	 * @return
	 */
	public Boolean tieneAutorizacionesOrdenCompra(Long ordenCompraId){		
		List<OrdenCompraAutorizacion> autorizaciones = obtenerAutorizacionesOrdenCompra(ordenCompraId);
		for(OrdenCompraAutorizacion autorizacion : CollectionUtils.emptyIfNull(autorizaciones)){
			if(!autorizacion.getOrderCompra().getEstatus().equals(OrdenCompraService.ESTATUS_CANCELADA)  && 
					autorizacion.getEstatus().equals(OrdenCompraAutorizacionService.ESTATUS_AUTORIZADO)){
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}
	
	/**
	 * Revisar si una estimacion tiene autorizaciones para cualquiera de sus ordenes de compra
	 * @param estimacionId
	 * @return
	 */
	public Boolean tieneAutorizacionesEstimacion(Long estimacionId){
		Boolean tiene = Boolean.FALSE;
		List<OrdenCompra> ordenes = entidadService.findByProperty(OrdenCompra.class, "idTercero", estimacionId);
		for(OrdenCompra orden : CollectionUtils.emptyIfNull(ordenes)){
			if(!orden.getEstatus().equals(OrdenCompraService.ESTATUS_CANCELADA)){
				tiene = tieneAutorizacionesOrdenCompra(orden.getId());
			}
		}
		return tiene;		
	}
}
