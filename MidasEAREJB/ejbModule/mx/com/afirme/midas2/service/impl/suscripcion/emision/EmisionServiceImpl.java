package mx.com.afirme.midas2.service.impl.suscripcion.emision;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

import mx.com.afirme.midas.consultas.formapago.FormaPagoInterfazServiciosRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO.TIPO_AGRUPACION_RECIBOS;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.recibo.ReciboDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.compensaciones.CalculoCompensacionesDao;
import mx.com.afirme.midas2.dao.pkg.PkgAutGeneralesDao;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaAmexDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaIbsDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.programapago.RecuotificacionService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularService;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;

@Stateless
public class EmisionServiceImpl implements EmisionService {
	
	private static final Logger LOG = Logger.getLogger(EmisionServiceImpl.class);
	public static final String ERROR_CODIGO_IBS = "9999";
	public static final String ERROR_GENERAL_IBS_SALDO = "Error al intentar consultar el saldo";
	public static final String ERROR_GENERAL_IBS_CARGO = "Error al intentar realizar el cargo";

	private PkgAutGeneralesDao pkgAutGeneralesDao;
	private FormaPagoInterfazServiciosRemote formaPagoInterfazServiciosRemote;
	private UsuarioService usuarioService;
	private IncisoCotizacionFacadeRemote incisoService;
	private EntidadService entidadService;
	private PolizaPagoService polizaPagoService;
	private CotizacionService cotizacionService;
	private RenovacionMasivaService renovacionMasivaService;
	private InformacionVehicularService informacionVehicularService;
	private EndosoService endosoService;
	private FronterizosService fronterizosService;
	
	private RecuotificacionService recuotificacionService;
	private CalculoCompensacionesDao calculoCompensacionesDao;
	private ClientePolizasService clientePolizasService;

	
	public final String CLASS_NAME = this.getClass().getName();
	private String fechaVencimiento;
	private String codigoSeguridad;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> emitir(CotizacionDTO cotizacionDTO, boolean esRenovacion) {
		String methodName = CLASS_NAME + " :: emitir(CotizacionDTO, boolean):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		Map<String, String> mensajeEmitir = new HashMap<String, String>(1);
		String mensajeInformacionVehicular = "";
		boolean pagoNoProgramado = false;	
		boolean aplicaPago = true;
		CotizacionDTO cotizacion = actualizaDatosCotizacion(cotizacionDTO);
		aplicaPago = aplicaPagosAPolizaConVigenciaIniciada(cotizacion);
		BigDecimal polizaId = null;
		
		//Valida si es renovacion
		if(cotizacion.getSolicitudDTO() != null && cotizacion.getSolicitudDTO().getEsRenovacion() != null &&
				cotizacion.getSolicitudDTO().getEsRenovacion().intValue() == 1){
			esRenovacion = true;
		}
		//Si es flotilla y los recibos no son agrupados se busca recibo para aplicar pago a cada Inciso
		boolean aplicaGlobal = false;
		if((cotizacion.getTipoAgrupacionRecibos() != null && 
						cotizacion.getTipoAgrupacionRecibos().equals(TIPO_AGRUPACION_RECIBOS.AGRUPADO.getValue()))
				|| cotizacion.getTipoPolizaDTO().getClaveAplicaFlotillas().intValue() == 0){
			aplicaGlobal = true;
		}
		
		//consulta de saldo - cuenta afirme, se toma como base RespuestaIBS  para no perder  la logica implementada, los servicios de pago son independientes.
		RespuestaIbsDTO respuestaIbs = consultaRespuestaIbs(cotizacion, esRenovacion, aplicaPago, aplicaGlobal);
		if(respuestaIbs.esValido()){
			mensajeEmitir = pkgAutGeneralesDao.emitirCotizacion(cotizacion);					
			
			try{				
				polizaId = BigDecimal.valueOf(Long.valueOf(mensajeEmitir.get(PolizaDTO.IDPOLIZA_EMISON)));
				respuestaIbs = aplicaPagosRespuestaIbs(cotizacion, esRenovacion, aplicaPago, aplicaGlobal);
				
				if(!respuestaIbs.esValido()){
					//Error
					mensajeEmitir.put(PolizaDTO.MENSAJE_EMISON, "No puede emitirse la p\u00F3liza. [Cobranza]: " +  respuestaIbs.getDescripcionErrorIBS());
					mensajeEmitir.put(PolizaDTO.ICONO_EMISION, PolizaDTO.ICONO_ERROR);
					if(polizaId != null){
						pkgAutGeneralesDao.cancelarEmision(polizaId.doubleValue());
					}
				}else{								
					try{						
						//cambia estatus de la cotizacion		
						try{
							cotizacionService.actualizarEstatusCotizacionEmitida(cotizacion.getIdToCotizacion());
						}catch(Exception e){
							LOG.error(methodName + e.getMessage(), e);
						}
						
						boolean isNotFlotilla = (cotizacion.getTipoPolizaDTO().getClaveAplicaFlotillas() != null &&
								cotizacion.getTipoPolizaDTO().getClaveAplicaFlotillas() == 0);
						//Valida estilo contra la informacion vehicular de CESVI, en cotizacion individual
						try {
							if (isNotFlotilla) {
								mensajeInformacionVehicular = informacionVehicularService.
									validaInfoVehicularByEstiloCotInd(cotizacion.getIdToCotizacion(), polizaId);
							} 
						} catch(Exception e){
							LOG.error(methodName + e.getMessage(), e);
						}
						
						try{
							if(isNotFlotilla){
								fronterizosService.migrarClavesCotizacion(cotizacion);
							}
						}catch(Exception e){
							LOG.error(methodName + "Ocurrio una excepcion en FronterizosServiceImpl.migrarClavesCotizacion() "+e);
						}

						//Valida CoberturaEnd de emision inicial
						endosoService.actualizaAgrupadoresEndoso(polizaId, BigDecimal.ZERO);
						
						//Migra a bitemporalidad
						pkgAutGeneralesDao.migraBitemporalidad(polizaId.longValue(), 
								TimeUtils.getDateTime(cotizacion.getFechaInicioVigencia()).toDate(), 
								TimeUtils.getDateTime(cotizacion.getFechaFinVigencia()).toDate());
						

						//Generar Recibos
						emitirRecibosPoliza(polizaId);
						
						if(esRenovacion){
							MensajeDTO mensaje = renovacionMasivaService.programaPago(polizaId);
							if(mensaje != null && mensaje.getMensaje() != null && !mensaje.getMensaje().isEmpty()){
								mensajeEmitir.put(PolizaDTO.ICONO_EMISION, PolizaDTO.ICONO_CONFIRM);
								mensajeEmitir.put(PolizaDTO.MENSAJE_EMISON, "La p\u00f3liza se emiti\u00f3 correctamente. \nPago no programado\n" + mensajeInformacionVehicular + "\nEl n\u00famero de  P\u00f3liza es: ");
								pagoNoProgramado = true;
							}
							clientePolizasService.enviarNotificacionCorreoRenovacion(cotizacionDTO.getSolicitudDTO().getIdToPolizaAnterior().longValue());
							
						}
					}catch(Exception e){
						LOG.error(methodName + e.getMessage(), e);
						mensajeEmitir.put(PolizaDTO.MENSAJE_EMISON, e.getMessage());
					}										
				}				
			}catch(Exception ex){
				LogDeMidasEJB3.log(methodName + "No se pudo emitir la poliza", java.util.logging.Level.SEVERE, ex);
			}
		}else{
			mensajeEmitir.put(PolizaDTO.ICONO_EMISION, PolizaDTO.ICONO_ERROR);
			mensajeEmitir.put(PolizaDTO.MENSAJE_EMISON, respuestaIbs.getDescripcionErrorIBS() != null ? "Cuenta Afirme: " +
					respuestaIbs.getDescripcionErrorIBS() : "Imposible consultar el saldo en este momento");			
		}
		
		if(mensajeEmitir.get(PolizaDTO.ICONO_EMISION).equals(PolizaDTO.ICONO_CONFIRM) && !pagoNoProgramado && !mensajeInformacionVehicular.isEmpty()) {
			mensajeEmitir.put(PolizaDTO.MENSAJE_EMISON, "La p\u00f3liza se emiti\u00f3 correctamente.\n" + mensajeInformacionVehicular 
					+ "\nEl n\u00famero de  P\u00f3liza es: ");
		}
			
		return mensajeEmitir;
	}
	
	private CotizacionDTO actualizaDatosCotizacion(CotizacionDTO cotizacionDTO) {
		String methodName = CLASS_NAME + " :: actualizaDatosCotizacion(CotizacionDTO):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		double dias = 0;
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, cotizacionDTO.getIdToCotizacion());
		if(cotizacion.getFechaInicioVigencia() != null && cotizacion.getFechaFinVigencia() != null){
			long fechaInicialMs = cotizacion.getFechaInicioVigencia().getTime();
			long fechaFinalMs = cotizacion.getFechaFinVigencia().getTime();
			long diferencia = fechaFinalMs - fechaInicialMs;
			dias = Math.floor(diferencia / (1000 * 60 * 60 * 24));
			
		}
		cotizacion.setDiasPorDevengar(dias);
		
		double derechosPoliza = 0;
		int numIncisos = incisoService.contarIncisosPorCotizacion(cotizacion.getIdToCotizacion());
		if(cotizacion.getValorDerechosUsuario() != null &&  cotizacion.getIncisoCotizacionDTOs() != null){
			derechosPoliza = cotizacion.getValorDerechosUsuario() * numIncisos;
		}
		cotizacion.setDerechosPoliza(derechosPoliza);
		
		return cotizacion;
	}
	
	private boolean aplicaPagosAPolizaConVigenciaIniciada(CotizacionDTO cotizacion){
		String methodName = CLASS_NAME + " :: aplicaPagosAPolizaConVigenciaIniciada(CotizacionDTO):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		boolean aplicaPago = true;
		if(cotizacion.getFechaInicioVigencia() != null && cotizacion.getFechaFinVigencia() != null){
			long fechaInicialMs = cotizacion.getFechaInicioVigencia().getTime();
			
			//Aplica pago solo si la poliza ya empezó vigencia
			GregorianCalendar cal = new GregorianCalendar();
			cal.setTime(new Date());
			long diferenciaVigencia = fechaInicialMs - cal.getTimeInMillis();
			if(diferenciaVigencia > 0){
				aplicaPago = false;
			}
		}
		return aplicaPago;
	}

	private RespuestaIbsDTO consultaRespuestaIbs(CotizacionDTO cotizacion, 
			boolean esRenovacion, boolean aplicaPago, boolean aplicaGlobal) {
		String methodName = CLASS_NAME + " :: consultaRespuestaIbs(CotizacionDTO, boolean, boolean, boolean):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		RespuestaIbsDTO respuestaIbs =  new RespuestaIbsDTO();		
		if(!esRenovacion && aplicaPago){
			if(aplicaGlobal){
				respuestaIbs = saldoParaPagoValido(cotizacion.getIdMedioPago(), cotizacion.getIdToCotizacion());
			}else{
				respuestaIbs = saldoParaPagoValidoIncisos(cotizacion.getIdToCotizacion());
			}
		}
		return respuestaIbs;
	}
	
	private RespuestaIbsDTO aplicaPagosRespuestaIbs(CotizacionDTO cotizacion, 
			boolean esRenovacion, boolean aplicaPago, boolean aplicaGlobal) {
		String methodName = CLASS_NAME + " :: aplicaPagosRespuestaIbs(CotizacionDTO, boolean, boolean, boolean):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		RespuestaIbsDTO respuestaIbs =  new RespuestaIbsDTO();		
		if(!esRenovacion && aplicaPago){
			if(aplicaGlobal){
				respuestaIbs = aplicaPago(cotizacion.getIdMedioPago(), cotizacion.getIdToCotizacion());
			}else{
				respuestaIbs = aplicaPagoInciso(cotizacion.getIdToCotizacion());
			}
		}
		return respuestaIbs;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void emitirRecibosPoliza(BigDecimal idToPoliza) {	
		String methodName = CLASS_NAME + " :: emitirRecibosPoliza(BigDecimal):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		LOG.info(methodName + "Entrando a emitirRecibo ==> emitirRecibosPoliza");	
		try {
			//Generar Recibos
			List<ReciboDTO> recibos = pkgAutGeneralesDao.emiteRecibos(idToPoliza.toString());
			
			if(recibos != null && recibos.size() > 0){
				ReciboDTO recibo = recibos.get(0);
				EndosoId id  = new EndosoId(idToPoliza, (short)0);
				EndosoDTO endoso = entidadService.findById(EndosoDTO.class, id);
				if(endoso != null){
					endoso.setLlaveFiscal(recibo.getLlaveFiscal());
					entidadService.save(endoso);
				}
			}
		} catch(Exception e){
			LOG.error(methodName + "Error al emitir recibo", e);
		}
		LOG.info(methodName + "Después de emitirRecibo ...");			
	}

		
	private RespuestaIbsDTO saldoParaPagoValido(BigDecimal idMedioPago, BigDecimal idToCotizacion){			
		String methodName = CLASS_NAME + " :: saldoParaPagoValido(BigDecimal, BigDecimal):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		RespuestaIbsDTO respuestaIbsDTO = null;
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);

		if(cotizacion.getIdMedioPago().shortValue() == CuentaPagoDTO.TipoCobro.CUENTA_AFIRME.valor()){
			try{
				respuestaIbsDTO = polizaPagoService.consultarSaldo(idToCotizacion);
			}catch(Exception ex){
				LOG.error(methodName + ex.getMessage(), ex);
				respuestaIbsDTO = new RespuestaIbsDTO(ERROR_CODIGO_IBS, ERROR_GENERAL_IBS_SALDO);
			}
		}else{
			// solo consulta saldo mediante conducot de cobro cuenta AFIRME.
			respuestaIbsDTO = new RespuestaIbsDTO();
		}
		return respuestaIbsDTO;
	}
	
	private RespuestaIbsDTO saldoParaPagoValidoIncisos(BigDecimal idToCotizacion){			
		String methodName = CLASS_NAME + " :: saldoParaPagoValidoIncisos(BigDecimal):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		RespuestaIbsDTO respuestaIbsDTO = new RespuestaIbsDTO();
		try{
			respuestaIbsDTO = polizaPagoService.consultarSaldoIncisos(idToCotizacion);
		}catch(Exception ex){
			LOG.error(methodName + ex.getMessage(), ex);
			respuestaIbsDTO = new RespuestaIbsDTO(ERROR_CODIGO_IBS, ERROR_GENERAL_IBS_SALDO);
		}
		return respuestaIbsDTO;
	}
	
	private RespuestaIbsDTO aplicaPago(BigDecimal idMedioPago, BigDecimal idToCotizacion){	
		String methodName = CLASS_NAME + " :: aplicaPago(BigDecimal, BigDecimal):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		RespuestaIbsDTO respuestaIbsDTO = null;
		if(idMedioPago.shortValue() == CuentaPagoDTO.TipoCobro.CUENTA_AFIRME.valor() || 
				idMedioPago.shortValue() == CuentaPagoDTO.TipoCobro.TARJETA_CREDITO.valor()){
			try{
				respuestaIbsDTO = polizaPagoService.aplicarPago(idToCotizacion);
			}catch(Exception ex){
				LOG.error(methodName + ex.getMessage(), ex);
				respuestaIbsDTO = new RespuestaIbsDTO(ERROR_CODIGO_IBS, ERROR_GENERAL_IBS_CARGO);
			}
		}else if(idMedioPago.shortValue() == CuentaPagoDTO.TipoCobro.AMERICAN_EXPRESS.valor()){
			respuestaIbsDTO = new RespuestaIbsDTO();
			try{
				
				RespuestaAmexDTO respuestaAmexDTO = null;
				respuestaAmexDTO = polizaPagoService.aplicarPagoAmex(idToCotizacion,fechaVencimiento,codigoSeguridad);
				if (respuestaAmexDTO.esValido()) {
					respuestaIbsDTO.setIbsNumAutorizacion(respuestaAmexDTO.getAmexNumAutorizacion());
				}else{
					respuestaIbsDTO.setCodigoErrorIBS(respuestaAmexDTO.getCodigoErrorAmex());
					respuestaIbsDTO.setDescripcionErrorIBS(respuestaAmexDTO.getDescripcionErrorAmex());
				}
			}catch(Exception ex){
				LOG.error(ex);
				respuestaIbsDTO.setCodigoErrorIBS("9999");
				respuestaIbsDTO.setDescripcionErrorIBS(ex.getMessage());
			}
			
		}else{
			respuestaIbsDTO = new RespuestaIbsDTO();
		}
		return respuestaIbsDTO;
	}
	
	private RespuestaIbsDTO aplicaPagoInciso(BigDecimal idToCotizacion){	
		String methodName = CLASS_NAME + " :: aplicaPagoInciso():: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		RespuestaIbsDTO respuestaIbsDTO = null;
		try{
			polizaPagoService.setDatosTarjetaEmision(fechaVencimiento, codigoSeguridad);
			respuestaIbsDTO = polizaPagoService.aplicarPagoInciso(idToCotizacion);
		}catch(Exception ex){
			LOG.error(methodName + ex.getMessage(), ex);
			respuestaIbsDTO = new RespuestaIbsDTO(ERROR_CODIGO_IBS, ERROR_GENERAL_IBS_CARGO);
		}
		return respuestaIbsDTO;
	}
	
	public void validacionPreviaRecibos(BigDecimal idToCotizacion) throws RuntimeException{	
		String methodName = CLASS_NAME + " :: validacionPreviaRecibos(BigDecimal):: ";
		LogDeMidasEJB3.log(methodName + "Inicializa. " , Level.INFO, null);
		
		BigDecimal grupoParametro = ParametroGeneralDTO.GRUPO_PARAM_COMPONENETE_PROGRAMA_PAGO;
		LOG.info(methodName + "Obteniendo valores de parametros de configuracion de recuotificacion");
		Integer banderaRecuotificacion = recuotificacionService.validarEstatusRecuotificacion(grupoParametro, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_MODO_CALCULO_RECIBOS_AUTOS);
		Integer banderaPrevioRecibos = recuotificacionService.validarEstatusRecuotificacion(grupoParametro, 
				ParametroGeneralDTO.CODIGO_PARAM_GENERAL_VALIDAR_PREVIO_RECIBOS_AUTOS);
		
		if (banderaRecuotificacion.intValue() == 1){
					
			LOG.info(methodName + "Validando Previo de recibos para cotizacion: " + idToCotizacion.toString());
			//SE AGREGA VALIDACION DE PREVIO DE RECIBOS AL EMITIR
			//ParametroGeneralDN parametroGeneralDN = ParametroGeneralDN.getINSTANCIA();
			//ParametroGeneralId id = new ParametroGeneralId();
			//id.setIdToGrupoParametroGeneral(new BigDecimal("22"));
			//id.setCodigoParametroGeneral(new BigDecimal("22010"));
			//ParametroGeneralDTO parametroGeneralDTO = parametroGeneralDN.getPorId(id);
		
			boolean previoDeRecibos = recuotificacionService.validarPrevioDeRecibos(idToCotizacion);
			if (previoDeRecibos){
				LOG.info(methodName + "Existe Previo de recibos, validando saldos de Recuotificacion");
				Map<String, Object> res = recuotificacionService.validaTotalesDeCotizacionSalir(idToCotizacion);
				BigDecimal resultado = (BigDecimal) res.get("outIdEjecucion");
				if (resultado.toString().equals("0")){
					LOG.info(methodName + "No existen saldos, continuando con emision");
				}else{
					LOG.info(methodName + "Existen saldos por asignar a PP, pausando emision");
					throw new RuntimeException("Existen saldos de Recuotificacion. Verifique.");
				}
			}else{
				LOG.info(methodName + "No existe previo de recuotificacion, validando previo de recibos");
				if(banderaPrevioRecibos.intValue() == 1){
					LOG.info(methodName + "Bandera de Recuotificacion encendida, Pausando emision");
					throw new RuntimeException("No Existe Previo de Recibos. Seleccione la opcion Previo de Recibos antes de continuar con la emision.");
				}else{
					LOG.info(methodName + "Bandera de Recuotificacion apagada, continuando con la emision.");
				}
			}
		}else{
			LOG.info(methodName + "Bandera de recoutificacion apagada, continuando con la operacion normal");
		}
	}

	@Override	
	public String setFunctionPrintPoliza(BigDecimal idToPoliza) {
		String functionPrintPoliza = "";
		List<EndosoDTO> endosoPolizaList = endosoService.findByPropertyWithDescriptions("id.idToPoliza", idToPoliza, Boolean.TRUE);
		if(endosoPolizaList != null && endosoPolizaList.size()>0){
			EndosoDTO ultimoEndosoDTO = endosoPolizaList.get(endosoPolizaList.size() - 1);
			functionPrintPoliza = "$_imprimirDetallePoliza(" + idToPoliza + ","
					+ ultimoEndosoDTO.getClaveTipoEndoso() + ","
					+ ultimoEndosoDTO.getValidFrom().getTime()
					+ ","
					+ ultimoEndosoDTO.getRecordFrom().getTime()+")";
		}
		return functionPrintPoliza;
	}
	
		
	@EJB
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}

	@EJB
	public void setRenovacionMasivaService(RenovacionMasivaService renovacionMasivaService) {
		this.renovacionMasivaService = renovacionMasivaService;
	}
	
	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@EJB
	public void setPolizaPagoService(PolizaPagoService polizaPagoService) {
		this.polizaPagoService = polizaPagoService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setIncisoService(IncisoCotizacionFacadeRemote incisoService) {
		this.incisoService = incisoService;
	}

	@EJB
	public void setPkgAutGeneralesDao(PkgAutGeneralesDao pkgAutGeneralesDao) {
		this.pkgAutGeneralesDao = pkgAutGeneralesDao;
	}
	
    @EJB
	public void setFormaPagoInterfazServiciosRemote(
			FormaPagoInterfazServiciosRemote formaPagoInterfazServiciosRemote) {
		this.formaPagoInterfazServiciosRemote = formaPagoInterfazServiciosRemote;
	}
	
    @EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }
    
    @EJB
	public void setInformacionVehicularService(
			InformacionVehicularService informacionVehicularService) {
		this.informacionVehicularService = informacionVehicularService;
	}
	
    @EJB
	public void setFronterizosService(FronterizosService fronterizosService) {
		this.fronterizosService = fronterizosService;
	}

    @EJB
	public void setRecuotificacionService(RecuotificacionService recuotificacionService) {
		this.recuotificacionService = recuotificacionService;
	}

    @EJB
	public void setCalculoCompensacionesDao(
			CalculoCompensacionesDao calculoCompensacionesDao) {
		this.calculoCompensacionesDao = calculoCompensacionesDao;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	@Override
	public void setDatosTarjetaEmision(String fechaVencimiento, String codigoSeguridad) {
		this.setCodigoSeguridad(codigoSeguridad);
		this.setFechaVencimiento(fechaVencimiento);
	}
}
