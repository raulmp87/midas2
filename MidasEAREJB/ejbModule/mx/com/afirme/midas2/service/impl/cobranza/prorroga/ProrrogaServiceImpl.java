package mx.com.afirme.midas2.service.impl.cobranza.prorroga;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.cobranza.prorroga.ProrrogaDao;
import mx.com.afirme.midas2.domain.cobranza.prorroga.ToProrroga;
import mx.com.afirme.midas2.service.cobranza.prorroga.ProrrogaService;

@Stateless
public class ProrrogaServiceImpl implements ProrrogaService{
	
	private ProrrogaDao prorrogaDao;

	@EJB
	public void setProrrogaDao(ProrrogaDao prorrogaDao) {
		this.prorrogaDao = prorrogaDao;
	}

	@Override
	public List<ToProrroga> listarPorIdCotizacion(Long idCotizacion, Long inciso) {
		ToProrroga toProrroga = new ToProrroga();
		
		if ( idCotizacion != null && idCotizacion != 0 ){
			toProrroga.setIdToCotizacion(idCotizacion);
		}
		if ( inciso != null && inciso != 0 ){
			toProrroga.setNumeroInciso(inciso);
		}
		
		return prorrogaDao.findByFilters(toProrroga);
	}

}
