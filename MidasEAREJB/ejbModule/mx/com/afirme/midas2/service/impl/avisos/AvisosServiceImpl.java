package mx.com.afirme.midas2.service.impl.avisos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.service.avisos.AvisosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.log4j.Logger;

@Stateless
public class AvisosServiceImpl implements AvisosService {
	
	private static final Logger log = Logger.getLogger(AvisosServiceImpl.class);

	public static final BigDecimal PARAM_IDGROUP = new BigDecimal(13);
	private static final String AVISO_EMISION_WS_TITLE = "AVISO DE EMISION DE POLIZA POR WEB SERVICE";
	public static final BigDecimal AVISO_EMISION_WS_NEGOCIO = new BigDecimal(133003);
	public static final BigDecimal AVISO_EMISION_WS_DESTINATARIO = new BigDecimal(133007);
	public static final BigDecimal AVISO_EMISION_WS_ACTIVO = new BigDecimal(133011);
	private static final String AVISO_CANCELACION_TITLE = "AVISO DE CANCELACION";
	public static final BigDecimal AVISO_CANCELACION_NEGOCIO = new BigDecimal(133000);
	public static final BigDecimal AVISO_CANCELACION_DESTINATARIO = new BigDecimal(133004);
	public static final BigDecimal AVISO_CANCELACION_ACTIVO = new BigDecimal(133008);
	
	private static final String AVISO_ALTA_NOTA_TITLE = "AVISO DE ALTA DE NOTA DE CREDITO";
	public static final BigDecimal AVISO_ALTA_NOTA_NEGOCIO = new BigDecimal(133001);
	public static final BigDecimal AVISO_ALTA_NOTA_DESTINATARIO = new BigDecimal(133005);
	public static final BigDecimal AVISO_ALTA_NOTA_ACTIVO = new BigDecimal(133009);
	
	public static final int AVISO_CANCELACION = 0;
	public static final int AVISO_ALTA_NOTA = 1;
	public static final int AVISO_PROXIMA_CANCELACION = 2;
	public static final int AVISO_EMISION_WS = 3;
	
	@EJB
	private EntidadService entidadService;
	@EJB
	private MailService mailService;
	@EJB
	protected ParametroGeneralService parametroGeneralService;

	@Override
	public void enviarAviso(BigDecimal idToPoliza,
			int tipoAviso,
			Short tipoEndoso){
		
		BigDecimal tipoActivo = null;
		BigDecimal tipoNegocio = null;
		BigDecimal tipoDestinatario = null;
		String title = "";
		String body = "";
		List<String> destinatarios = null;
		
		try{
			log.info("enviarAviso. IdToPoliza: "+idToPoliza);
			switch (tipoAviso){
			case AVISO_CANCELACION: 
				tipoActivo = AVISO_CANCELACION_ACTIVO;
				tipoNegocio = AVISO_CANCELACION_NEGOCIO;
				tipoDestinatario = AVISO_CANCELACION_DESTINATARIO;
				title = AVISO_CANCELACION_TITLE;
				break;
			case AVISO_EMISION_WS: 
				tipoActivo = AVISO_EMISION_WS_ACTIVO;
				tipoNegocio = AVISO_EMISION_WS_NEGOCIO;
				tipoDestinatario = AVISO_EMISION_WS_DESTINATARIO;
				title = AVISO_EMISION_WS_TITLE;
				break;
			case AVISO_ALTA_NOTA: 
				tipoActivo = AVISO_ALTA_NOTA_ACTIVO;
				tipoNegocio = AVISO_ALTA_NOTA_NEGOCIO;
				tipoDestinatario = AVISO_ALTA_NOTA_DESTINATARIO;
				title = AVISO_ALTA_NOTA_TITLE;
				break;
			default:
				log.info("No se envia aviso. tipoAviso incorrecto. IdToPoliza: "+idToPoliza.toString());
				return;
			}
			
			if (this.isActiva(tipoActivo)){
				PolizaDTO poliza = entidadService.findById(PolizaDTO.class,idToPoliza);
				Long idToNegocio = poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio();
				body = this.getBody(poliza,tipoNegocio,tipoAviso,tipoEndoso);
				if (!body.isEmpty()){
					destinatarios = this.getDestinatarios(tipoDestinatario,idToNegocio);
					mailService.sendMail(destinatarios, title, body);			
					log.info("Se envio aviso a Cobranza. IdToPoliza: " + idToPoliza.toString());				
				}
			}
			else{
				log.info("No se envia aviso. El parametro para el tipo de aviso no esta activo: "+tipoActivo.toString());
			}
			
		}catch(Exception e){
			log.error("Error al enviar aviso. IdToPoliza: " + idToPoliza.toString() +" "+ e.getMessage());
		}
	}
	
	private String getBody(PolizaDTO poliza,
			BigDecimal tipoNegocio,
			int tipoAviso,
			Short tipoEndoso) throws Exception{
		
		String result = "";
		switch (tipoAviso){
			case AVISO_CANCELACION:
				if (this.isNegocio(poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio(), tipoNegocio)){
					String descripcion = "";
					CatalogoValorFijoId catalogoValorFijoId = new CatalogoValorFijoId(331,tipoEndoso.intValue());
					List<CatalogoValorFijoDTO> list = entidadService.findByProperty(CatalogoValorFijoDTO.class, "id", catalogoValorFijoId);
					if (list!=null && list.size()>0){
						descripcion = list.get(0).getDescripcion();
					}
					StringBuilder body = new StringBuilder();
					body.append("Se realizo la cancelacion de la poliza: ");
					body.append(poliza.getNumeroPolizaFormateada());
					body.append("<br>Del negocio: ");
					body.append(poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getDescripcionNegocio());
					body.append("<br>Tipo de cancelacion: ");
					body.append(descripcion);
					body.append("<br><br>Notificacion automatica generada por el sistema.");
					result = body.toString();
				}
				break;
			case AVISO_ALTA_NOTA:
				if (this.isNegocio(poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio(), tipoNegocio)){
					StringBuilder body = new StringBuilder();
					body.append("Se dio de alta nota de credito de la poliza: ");
					body.append(poliza.getNumeroPolizaFormateada());
					body.append("<br>Del negocio: ");
					body.append(poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getDescripcionNegocio());
					body.append("<br><br>Notificacion automatica generada por el sistema.");
					result = body.toString();
				}
				break;
			case AVISO_EMISION_WS:
				if (this.isNegocio(poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio(), tipoNegocio)){

					FormaPagoDTO formaPagoDTO =  entidadService.findById(FormaPagoDTO.class, poliza.getCotizacionDTO().getIdFormaPago().intValue());
					MedioPagoDTO medioPagoDTO =  entidadService.findById(MedioPagoDTO.class, poliza.getCotizacionDTO().getIdMedioPago().intValue());
					
					StringBuilder body = new StringBuilder();
					body.append("Se emitio mediante Web Service la poliza: ");
					body.append(poliza.getNumeroPolizaFormateada());
					body.append("<br>Del negocio: ");
					body.append(poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getDescripcionNegocio());
					body.append("<br>Forma de Pago: ");
					body.append(formaPagoDTO.getDescripcion());
					body.append("<br>Tipo de Pago: ");
					body.append(medioPagoDTO.getDescripcion());
					body.append("<br><br>Notificacion automatica generada por el sistema.");
					result = body.toString();
				}
				break;
			default: break;		
		}
		return result;
	}
	
	private boolean isActiva(BigDecimal tipoActivo) {
			
		boolean activa = false;
		ParametroGeneralId id = new ParametroGeneralId();
		id.setIdToGrupoParametroGeneral(PARAM_IDGROUP);
		id.setCodigoParametroGeneral(tipoActivo);
		ParametroGeneralDTO parametro = parametroGeneralService.findById(id );
		if(parametro != null && parametro.getValor() != null && parametro.getValor().equals("1")) {
			activa = true;
		}
		return activa;
	}
	
	/**
	 * 
	 * @param idNegocio
	 * @return Devuelve true si el idNegocio se encuentra en la tabla toparametrogeneral
	 */
	private boolean isNegocio(Long idNegocio, BigDecimal tipoNegocio){
		ParametroGeneralId id = new ParametroGeneralId();
		id.setIdToGrupoParametroGeneral(PARAM_IDGROUP);
		id.setCodigoParametroGeneral(tipoNegocio);
		ParametroGeneralDTO parametro = parametroGeneralService.findById(id );
		if(parametro != null && parametro.getValor() != null) {
			String[] valores = parametro.getValor().split(",");
			for (int i=0; i<valores.length;i++){
				if (idNegocio == Long.parseLong(valores[i])){
					return true;
				}
			}
		}
		return false;
	}
	
	/**
	 * Devuelve la lista de correos de los destinatarios
	 * dependiendo el idNegocio y en caso que el idNegocio 
	 * no este configurado,entonces devuelve la lista por default
	 * en la tabla toparametro debera guardarse asi:
	 * mail1@company.com,mail2@company.com|20,mail3@company.com,mail4@company.com|87,mail5@company.com,mail6@company.com
	 * cada grupo de destinatarios inicia con el idNegocio
	 * cada grupo de destinatarios esta separado por |
	 * cada destinatario esta separado por ,
	 * el primer grupo es el default y no inicia con idNegocio.
	 * @param tipoDestinatario
	 * @param idNegocio
	 * @return
	 */
	private List<String> getDestinatarios(BigDecimal tipoDestinatario,Long idNegocio){
		List<String> dest = new ArrayList<String>(1);
		ParametroGeneralId id = new ParametroGeneralId();
		id.setIdToGrupoParametroGeneral(PARAM_IDGROUP);
		id.setCodigoParametroGeneral(tipoDestinatario);
		ParametroGeneralDTO parametro = parametroGeneralService.findById(id );
		if(parametro != null && parametro.getValor() != null) {
			String[] grupo = parametro.getValor().split("\\|");
			for (int i=0; i<grupo.length;i++){
				String[] parametros = grupo[i].split(",");
				if (i>0){
					try{
						if (Long.parseLong(parametros[0]) == idNegocio){						
							for (int j=1; j<parametros.length;j++){
								dest.add(parametros[j]);
							}
							break;
						}
					}
					catch(NumberFormatException e){
						log.info("El idNegocio de destinatario es incorrecto. "+e.getMessage());
					}
				}
			}
			if (dest != null && dest.size() == 0){
				String[] parametros = grupo[0].split(",");
				for (int j=0; j<parametros.length;j++){
					dest.add(parametros[j]);
				}
			}
		}
		return dest;
	}
}
