package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import mx.com.afirme.midas2.dao.fuerzaventa.ProductoBancarioAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioAgente;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioAgenteService;
import mx.com.afirme.midas2.service.impl.catalogos.EntidadHistoricoServiceImpl;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ProductoBancarioAgenteServiceImpl extends EntidadHistoricoServiceImpl implements ProductoBancarioAgenteService{
	private ProductoBancarioAgenteDao productoBancarioAgenteDao;
	@Override
	public List<ProductoBancarioAgente> findByAgente(Long arg0) {
		return productoBancarioAgenteDao.findByAgente(arg0, null);
	}

	@Override
	public List<ProductoBancarioAgente> findByFilters(ProductoBancarioAgente arg0) {
		return productoBancarioAgenteDao.findByFilters(arg0);
	}
	
	@Override
	public void delete(Long idProducto)throws Exception{
		productoBancarioAgenteDao.delete(idProducto);
	}
	
	@Override
	public List<ProductoBancarioAgente> findByAgente(Long idAgente, String fechaHistorico)
	{
		return productoBancarioAgenteDao.findByAgente(idAgente,fechaHistorico);		
	}
	
	/***Sets and gets******************************/
	@EJB
	public void setProductoBancarioAgenteDao(
			ProductoBancarioAgenteDao productoBancarioAgenteDao) {
		this.productoBancarioAgenteDao = productoBancarioAgenteDao;
	}
	
}
