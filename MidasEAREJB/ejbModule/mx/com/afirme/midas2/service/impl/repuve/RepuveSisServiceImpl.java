package mx.com.afirme.midas2.service.impl.repuve;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.repuve.RepuveSisDao;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisEmision;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisPTT;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRecuperacion;
import mx.com.afirme.midas2.dto.repuve.sistemas.RepuveSisRobo;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.repuve.RepuveSisService;

/*******************************************************************************
 * Nombre Interface: 	RepuveSisServiceImpl.
 * 
 * Descripcion: 		Se utiliza para el manejo del Modulo de REPUVE.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class RepuveSisServiceImpl implements RepuveSisService{
	private static final long serialVersionUID = 1L;
	
	@EJB private RepuveSisDao repuveSisDao;

	@Override
	public List<RepuveSisEmision> obtenerEmisionPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return repuveSisDao.obtenerEmisionPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}

	@Override
	public List<RepuveSisRobo> obtenerRoboPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return repuveSisDao.obtenerRoboPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}

	@Override
	public List<RepuveSisRecuperacion> obtenerRecuperacionPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return repuveSisDao.obtenerRecuperacionPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}

	@Override
	public List<RepuveSisPTT> obtenerPTTPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return repuveSisDao.obtenerPTTPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}
}