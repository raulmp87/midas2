package mx.com.afirme.midas2.service.impl.compensaciones;
// default package

import java.math.BigDecimal;
import java.util.List;
import java.util.logging.Level;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.usuario.LogUtil;
import mx.com.afirme.midas2.domain.compensaciones.CaAutorizarConf;
import mx.com.afirme.midas2.service.compensaciones.CaAutorizarConfService;

import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

/**
 * Facade for entity CaAutorizarConf.
 * 
 * @see .CaAutorizarConf
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CaAutorizarConfServiceImpl implements CaAutorizarConfService {
	// property constants
	public static final String ENDOSO = "endoso";
	public static final String MODIFICABLE = "modificable";

	@PersistenceContext
	private EntityManager entityManager;

	public void save(CaAutorizarConf entity) {
		LogUtil.log("saving CaAutorizarConf instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogUtil.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	public void delete(CaAutorizarConf entity) {
		LogUtil.log("deleting CaAutorizarConf instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(CaAutorizarConf.class, entity
					.getId());
			entityManager.remove(entity);
			LogUtil.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogUtil.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CaAutorizarConf update(CaAutorizarConf entity) {
		LogUtil.log("updating CaAutorizarConf instance", Level.INFO, null);
		try {
			CaAutorizarConf result = entityManager.merge(entity);
			LogUtil.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogUtil.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<CaAutorizarConf> findByProperty(String propertyName,
			final Object value) {
		LogUtil.log("finding CaAutorizarConf instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from CaAutorizarConf model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	public List<CaAutorizarConf> findByEndoso(Object endoso) {
		return findByProperty(ENDOSO, endoso);
	}

	public List<CaAutorizarConf> findByModificable(Object modificable) {
		return findByProperty(MODIFICABLE, modificable);
	}

	@SuppressWarnings("unchecked")
	public List<CaAutorizarConf> findAll() {
		LogUtil.log("finding all CaAutorizarConf instances", Level.INFO, null);
		try {
			final String queryString = "select model from CaAutorizarConf model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogUtil.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@Override
	public boolean esModificable(String ramo, Long tipoEndoso) {
       boolean retorno = false;
       try{
			StringBuilder queryString = new StringBuilder();
			queryString.append(" SELECT AUT.MODIFICABLE AS CODMODIFICACION FROM  MIDAS.CA_AUTORIZARCONF AUT");
			queryString.append(" INNER JOIN MIDAS.CA_RAMO R ON(AUT.RAMO_ID = R.ID) ");
			queryString.append(" WHERE R.IDENTIFICADOR = ?1 ");
			if(tipoEndoso != null){
				queryString.append(" AND AUT.ENDOSO =?2 " );
			}
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, ramo);
			if(tipoEndoso != null){
				query.setParameter(2, tipoEndoso);
			}	    	    
			BigDecimal codMod =  (BigDecimal) query.getSingleResult();
			Long codModificable = codMod.longValue();
			if(codModificable == 1){
				retorno = true;	    	
			}else if(codModificable == 0){
				retorno  = false;
			}
		}catch(Exception ex){
		
		}
		return  retorno;	
	}

	@SuppressWarnings("unchecked")
	public Short obtenerTipoEndoso(BigDecimal idToCotizacion,Short numeroEndoso) {
		LogUtil.log("buscando Endoso "+numeroEndoso+"de la poliza: "+idToCotizacion, Level.INFO, null);
		EndosoDTO  endosoDTO = null;
		try {
			if(numeroEndoso == null)
				numeroEndoso = new Short((short)0);
			String queryString = "select model from EndosoDTO model where model.idToCotizacion = :idToCotizacion and model.id.numeroEndoso = :numeroEndoso";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("idToCotizacion", idToCotizacion);
			query.setParameter("numeroEndoso", numeroEndoso);
			query.setHint(QueryHints.REFRESH, HintValues.TRUE);
			List<EndosoDTO> result = query.getResultList();
			if(!result.isEmpty()) {
				endosoDTO = (EndosoDTO) result.get(0);
			} else {
				return null;
			}
		} catch (RuntimeException re) {
			LogUtil.log("getPenultimoEndoso failed", Level.SEVERE, re);
			throw re;
		}
		return endosoDTO.getClaveTipoEndoso();
	}
	
	@Override
	public CaAutorizarConf findById(Long arg0) {
		// TODO Auto-generated method stub
		return null;
	}


}