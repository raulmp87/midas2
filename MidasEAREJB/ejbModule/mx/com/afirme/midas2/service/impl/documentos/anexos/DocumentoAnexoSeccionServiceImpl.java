package mx.com.afirme.midas2.service.impl.documentos.anexos;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.documentos.anexos.DocumentoAnexoSeccion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.documentos.anexos.DocumentoAnexoSeccionService;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;

@Stateless
public class DocumentoAnexoSeccionServiceImpl implements DocumentoAnexoSeccionService{
	
	@EJB
	private EntidadService entidadService;
	@EJB
	private FortimaxV2Service fortimaxV2Service;
	@PersistenceContext
	protected EntityManager entityManager;

	@Override
	public List<DocumentoAnexoSeccion> findDocumentoAnexoSeccionByFmxDocName(
			String fmxDocName) throws SQLException, Exception {
		
		 	List<DocumentoAnexoSeccion> documentoAnexoSeccionList = new ArrayList<DocumentoAnexoSeccion>();
			documentoAnexoSeccionList = entidadService.findByProperty(DocumentoAnexoSeccion.class, "fortimaxDocNombre", fmxDocName);
			return documentoAnexoSeccionList;
	}

	@Override
	public DocumentoAnexoSeccion findDocumentoAnexoSeccionById(Long id)
			throws SQLException, Exception {
		DocumentoAnexoSeccion documentoAnexoSeccion = new DocumentoAnexoSeccion();
		documentoAnexoSeccion = entidadService.findById(DocumentoAnexoSeccion.class, id);
		return documentoAnexoSeccion;
	}

	@Override
	public List<DocumentoAnexoSeccion> findDocumentoAnexoSeccionByIdToSeccion(
			Long idToSeccion) throws SQLException, Exception {
		List<DocumentoAnexoSeccion> documentoAnexoSeccionList = new ArrayList<DocumentoAnexoSeccion>();
		documentoAnexoSeccionList = entidadService.findByProperty(DocumentoAnexoSeccion.class, "idToSeccion", idToSeccion);
		return documentoAnexoSeccionList;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DocumentoAnexoSeccion> obtenerDocumentoAnexoSeccion(Long idToSeccion) {
		List<DocumentoAnexoSeccion> documentoAnexoSeccionList = new ArrayList<DocumentoAnexoSeccion>();
		Map<String, Object> parameters = new HashMap<String, Object>();	
		StringBuilder queryString = new StringBuilder("SELECT model FROM " + DocumentoAnexoSeccion.class.getSimpleName() + " model ");
		queryString.append(" WHERE idToSeccion = :idToSeccion");
		parameters.put("idToSeccion", idToSeccion);
		queryString.append(" ORDER BY model.id");
		documentoAnexoSeccionList = (List<DocumentoAnexoSeccion>) entidadService.executeQueryMultipleResult(queryString.toString(), parameters);
		return documentoAnexoSeccionList;
	}

	@Override
	public void saveDocumentoAnexoSeccion(DocumentoAnexoSeccion documentoAnexoSeccion) {	
		entidadService.save(documentoAnexoSeccion);
	}

	@Override
	public Long updateDocumentoAnexoSeccion(String accion,
			DocumentoAnexoSeccion documentoAnexoSeccion) throws Exception {
		
		DocumentoAnexoSeccion documentoAnexoSeccionUpdate = new DocumentoAnexoSeccion();
		documentoAnexoSeccionUpdate = this.findDocumentoAnexoSeccionById(documentoAnexoSeccion.getId());
		documentoAnexoSeccionUpdate.setDescripcion(documentoAnexoSeccion.getDescripcion());
		documentoAnexoSeccionUpdate.setNumeroSecuencia(documentoAnexoSeccion.getNumeroSecuencia());
		documentoAnexoSeccionUpdate.setClaveObligatoriedad(documentoAnexoSeccion.getClaveObligatoriedad());
		entidadService.executeActionGrid(accion, documentoAnexoSeccionUpdate);
		return documentoAnexoSeccionUpdate.getId();
	}

	@Override
	public List<byte[]> getAnexosSeccion(Long idToSeccion) throws Exception {
		
		List<byte[]> anexosSeccionList = new ArrayList<byte[]>();	
		List<DocumentoAnexoSeccion> documentoAnexoSeccionList = this.findDocumentoAnexoSeccionByIdToSeccion(idToSeccion);
		anexosSeccionList = this.getBytes(documentoAnexoSeccionList);		
		return anexosSeccionList;
	}

	@Override
	public List<byte[]> getAnexosSeccion(PolizaDTO polizaDTO) throws Exception {
		
		List<BigDecimal> idSeccionList = new ArrayList<BigDecimal>();
		List<byte[]> bytesList = new ArrayList<byte[]>();
		idSeccionList = this.obtenerIdSecciones( polizaDTO );
		List<DocumentoAnexoSeccion> documentoAnexoSeccionList1 = new ArrayList<DocumentoAnexoSeccion>();
		
		for ( BigDecimal item: idSeccionList ){
			List<DocumentoAnexoSeccion> documentoAnexoSeccionList = this.findDocumentoAnexoSeccionByIdToSeccion(item.longValue());
			documentoAnexoSeccionList1.addAll(documentoAnexoSeccionList);			
		}
		
		bytesList = this.getBytes( documentoAnexoSeccionList1 );
		return bytesList;
	}
	
	private List<byte[]> getBytes( List<DocumentoAnexoSeccion> documentoAnexoSeccionList ) throws Exception{
		
		List<byte[]> bytesList = new ArrayList<byte[]>();
		
		for ( DocumentoAnexoSeccion item : documentoAnexoSeccionList ){
			byte byteArray[] = fortimaxV2Service.downloadFileBP(item.getFmxNombre(), "D", item.getFmxGaveta(), Long.valueOf(item.getFmxId()));
			bytesList.add(byteArray);
		}
		
		return bytesList;		
	}
	
	@SuppressWarnings("unchecked")
	private List<BigDecimal> obtenerIdSecciones(PolizaDTO polizaDTO) throws Exception {
		
		List<BigDecimal> idSeccionList = new ArrayList<BigDecimal>();
		
		StringBuilder queryString = new StringBuilder("select distinct(a4.seccion_id) ");		
		queryString.append("from MIDAS.topoliza a0, MIDAS.mcotizacionc a1, MIDAS.mincisoc a2, MIDAS.mseccionincisoc a4 ");
		queryString.append("where a0.codigoproducto = ");
		queryString.append(polizaDTO.getCodigoProducto());
		queryString.append(" and a0.codigotipopoliza = ");
		queryString.append(polizaDTO.getCodigoTipoPoliza());
		queryString.append(" and a0.numeroPoliza = ");
		queryString.append(polizaDTO.getNumeroPoliza());
		queryString.append(" and a0.numerorenovacion = ");
		queryString.append(polizaDTO.getNumeroRenovacion());
		queryString.append(" and a0.idtocotizacion = a1.numero ");
		queryString.append("and a2.mcotizacionc_id = a1.id ");
		queryString.append("and a2.id = a4.mincisoc_id ");
		queryString.append("order by seccion_id desc ");		

		Query query = entityManager.createNativeQuery(queryString.toString());
		idSeccionList = (List<BigDecimal>)query.getResultList();		
		
		return idSeccionList;
	}

}
