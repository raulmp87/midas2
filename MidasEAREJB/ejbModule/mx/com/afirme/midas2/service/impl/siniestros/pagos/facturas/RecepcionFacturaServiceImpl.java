package mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.PersistenceException;

import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.siniestros.pagos.facturas.RecepcionFacturaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.compensaciones.LiquidacionCompensaciones;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal.OrigenDocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.OrdenCompraProveedorDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.facturas.RelacionFacturaOrdenCompraDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.gob.sat.cfd.x3.ComprobanteDocument;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.TipoDeComprobante;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;



import org.apache.commons.lang.StringUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlException;


@Stateless
public class RecepcionFacturaServiceImpl implements RecepcionFacturaService {
	
	private static final Logger log = Logger.getLogger(RecepcionFacturaServiceImpl.class);
	
	public static final String FACTURA_PROCESADA = "PR";
	public static final String FACTURA_DEVUELTA = "D";
	public static final String FACTURA_REGISTRADA = "R";
	public static final String FACTURA_PAGADA = "P";
	public static final String FACTURA_ERROR = "E";
	
	
	
	public static final String ENVIO_VALIDACION_APROBADA = "A";
	public static final String ENVIO_VALIDACION_RECHAZADA = "R";
	public static final String ENVIO_VALIDACION_PENDIENTE = "P";
	
	public static final Long NO_TIPO_MENSAJE_INTERNO = 70l;
	
	public static final String MENSAJE_ERROR_TOTAL = "El TOTAL de la factura y la suma del TOTAL de las ordenes de compra no es el mismo";
	public static final String MENSAJE_ERROR_SUBTOTAL = "El SUBTOTAL de la factura y la suma del SUBTOTAL de las ordenes de compra no es el mismo";
	public static final String MENSAJE_ERROR_IVA = "El IVA de la factura y la suma del IVA de las ordenes de compra no es el mismo";
	public static final String MENSAJE_ERROR_IVA_RETENIDO = "El IVA RETENIDO de la factura y la suma del IVA RETENIDO de las ordenes de compra no es el mismo";
	public static final String MENSAJE_ERROR_ISR = "El ISR de la factura y la suma del ISR de las ordenes de compra no es el mismo";
	
	public static final int MIDAS_APP_ID = 5;
	public static final String FACTURA_VALIDAR_PDF = "FACTURA_VALIDAR_PDF";
	public static final String FACTURA_DIFF_IMPUESTO = "FACTURA_DIFF_IMPUESTO";
	public static final String FACTURA_DIFF_TOTAL = "FACTURA_DIFF_TOTAL";
	public static final String VALIDA_AXOSNET = "SE_VALIDA_CON_AXOSNET";
	public static final String RFC_AFIRME = "RFC_AFIRME";
	
	public static final String NUMERO_FACTURA_FICTICIA = "xxxxxxxx";
	
	
	public static final String ORIGEN_ENVIO = "ZIPFACT";
	

	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private RecepcionFacturaDao recepcionFacturaDao;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private static ListadoService listadoService;
	
	
	@EJB
	private OrdenCompraService ordenCompraService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService ; 
	
	@EJB
	private MailService mailService;
	
	@EJB
	private EnvioFacturaService envioFacturaService;
	
	@EJB
	private static PagosSiniestroService pagosSiniestroService;
	
	@EJB
	private BitacoraService bitacoraService;
	
	
	




	@Override
	public List<EnvioValidacionFactura> obtenerFacturasProcesadas(Long idBatch) {
		return agregaEstatusDescripcion(this.recepcionFacturaDao.obtenerFacturasProcesadas(idBatch));
	}
	
	@Override
	public List<FacturaDTO> obtenerFacturasPendientesComplementoProveedor(Integer idProveedor, String origenEnvio) throws Exception{
		return recepcionFacturaDao.obtenerFacturasPendientesComplementoProveedor(idProveedor,origenEnvio);
	}

	@Override
	public List<FacturaDTO> obtenerFacturasPendientesComplementoAgente(Integer idAgente) throws Exception{
		return recepcionFacturaDao.obtenerFacturasPendientesComplementoAgente(idAgente);
	}

	@Override
	public List<MensajeValidacionFactura> obtenerResultadosValidacion(Long idEnvioValidacion) {
		return this.entidadService.findByProperty(MensajeValidacionFactura.class, "envioValidacionFactura.id",idEnvioValidacion);
	}



	@SuppressWarnings("unchecked")
	@Override
	public List<EnvioValidacionFactura> procesarFacturasZip(Integer proveedorId, File zipFile, String extensionArchivo) {
		
		List<DocumentoFiscal> facturas = new ArrayList<DocumentoFiscal>();
		List<String> validFileNameList = this.findValidFileNames(zipFile); // ???
		
		log.info("FileName :"+extensionArchivo);
		FileInputStream fis = null;
		try {
			if(extensionArchivo.endsWith("zip")){
				ZipInputStream dis = new ZipInputStream(new FileInputStream(zipFile));
				ZipEntry ze = dis.getNextEntry();
				byte[] buffer = new byte[2048];
				while (ze != null) {
					String fileName = ze.getName();
					
					if( (ze.getName().endsWith(".xml") || ze.getName().endsWith(".XML")) && validFileNameList.contains(fileName.substring(0, fileName.lastIndexOf(".")))){
						int size;
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						while ((size = dis.read(buffer, 0, buffer.length)) != -1) {
							bos.write(buffer, 0, size);
						}
						DocumentoFiscal factura = this.procesaXML(bos,proveedorId,DocumentoFiscal.TipoDocumentoFiscal.FACTURA);
						this.entidadService.save(factura);
						facturas.add(factura);
					}
					dis.closeEntry();
					ze = dis.getNextEntry();
				}
				dis.closeEntry();
				dis.close();
			}else if(extensionArchivo.endsWith("xml") || extensionArchivo.endsWith("XML")){
				fis = new FileInputStream(zipFile);
				byte[] bytes = new byte[(int) zipFile.length()];
				fis.read(bytes);
				ByteArrayOutputStream bos = new ByteArrayOutputStream( );
				bos.write(bytes);
				DocumentoFiscal factura = this.procesaXML(bos,proveedorId,DocumentoFiscal.TipoDocumentoFiscal.FACTURA);
				this.entidadService.save(factura);
				facturas.add(factura);
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(),e);
		} catch (IOException e) {
			log.error(e.getMessage(),e);
		} finally {
			IOUtils.closeQuietly(fis);
		}
		
		List<EnvioValidacionFactura> envioValidacionList = creaEnvioValidacion(facturas);
		this.validaProcesamientoFactura(envioValidacionList,proveedorId);
		envioValidacionList = (List<EnvioValidacionFactura>) this.entidadService.saveAll(envioValidacionList);
		return agregaEstatusDescripcion(envioValidacionList);

	}	
	
	
	
	/**
	 * 
	 *  * Validar que el RFC Corresponda al del proveedor
	 * recepcionFacturaService.esValidoRFCPorProveedor
	 * En caso de que no lo sea agregar a la lista de errores
	 * "EL RFC NO CORRESPONDE AL PROVEEDOR"
	 * 
	 * Si la factura se encuentra Registrada, agregar a la lista de errores "LA NOTA
	 * DE CREDITO SE ENCUENTRA REGISTRADA"
	 * 
	 * Si la factura se encuentra Registrada, agregar a la lista de errores "LA NOTA
	 * DE CREDITO SE ENCUENTRA PAGADA"
	 * 
	 * Si el documentoFiscal no es NOTA DE CREDITO, agregar a la lista de errores
	 * "ESTE DOCUMENTO NO ES UNA NOTA DE CREDITO"
	 */
	
	@Override
	public void validaProcesamientoDeArchivos(List<EnvioValidacionFactura> envioValidacionList, Integer idProveedor){
		final String MENSAJE_ERROR_PROCESAMIENTO = "EL ARCHIVO NO SE PUDO PROCESAR";
		final String MENSAJE_ERROR_RFC_PROVEEDOR = "EL RFC NO CORRESPONDE AL PROVEEDOR";
		final String MENSAJE_ERROR_NOTA_REGISTRADA = "LA NOTA DE CREDITO SE ENCUENTRA REGISTRADA";
		final String MENSAJE_ERROR_NOTA_PAGADA = "LA NOTA DE CREDITO SE ENCUENTRA PAGADA";
		final String MENSAJE_NO_ES_NOTA_CREDITO = "ESTE DOCUMENTO NO ES UNA NOTA DE CREDITO";
		final String MENSAJE_FOLIO_EXISTENTE = "El folio se encuentra duplicado";
		
		List<String> foliosEnArchivo = new ArrayList<String>();
		
		final Long NO_TIPO_MENSAJE_INTERNO = 0L;
		for(EnvioValidacionFactura envioValidacion: envioValidacionList){
			List<MensajeValidacionFactura> mensajes = new ArrayList<MensajeValidacionFactura>();
			if(!envioValidacion.getFactura().getTipo().equals(DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue())){
				mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_NO_ES_NOTA_CREDITO ));
				envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
			}else{
				if(!envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue())){
					if(envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue())){
						mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_NOTA_REGISTRADA ));
					}else if(envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.PAGADA.getValue())){
						mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_NOTA_PAGADA ));
					} else if(foliosEnArchivo.contains( envioValidacion.getFactura().getNumeroFactura())){
						mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_FOLIO_EXISTENTE ));
						envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
					}else{
						foliosEnArchivo.add(envioValidacion.getFactura().getNumeroFactura());
					}
			}else{
					if(envioValidacion.getFactura().getNumeroFactura().equals(NUMERO_FACTURA_FICTICIA)){
						mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_PROCESAMIENTO ));
					}else{
						if(!this.esValidoRFCPorProveedor(envioValidacion.getFactura().getRfcEmisor(), idProveedor)){
							mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_RFC_PROVEEDOR ));
						}
					}
				}
			}
			envioValidacion.setMensajes(mensajes);
		}
	}
	
	
	@Override
	public void validaProcesamientoFactura(List<EnvioValidacionFactura> envioValidacionList, Integer idProveedor){
		final String MENSAJE_ERROR_PROCESAMIENTO = "EL ARCHIVO NO SE PUDO PROCESAR";
		final String MENSAJE_ERROR_RFC_PROVEEDOR = "EL RFC NO CORRESPONDE AL PROVEEDOR";
		final String MENSAJE_ERROR_FACTURA_REGISTRADA = "LA FACTURA SE ENCUENTRA REGISTRADA";
		final String MENSAJE_ERROR_FACTURA_PAGADA = "LA FACTURA SE ENCUENTRA PAGADA";
		final String MENSAJE_NO_ES_FACTURA = "ESTE DOCUMENTO NO ES UNA FACTURA";
		final String MENSAJE_FOLIO_EXISTENTE = "El folio se encuentra duplicado";
		
		List<String> foliosEnArchivo = new ArrayList<String>();
		
		final Long NO_TIPO_MENSAJE_INTERNO = 0L;
		
		for(EnvioValidacionFactura envioValidacion: envioValidacionList){
			List<MensajeValidacionFactura> mensajes = new ArrayList<MensajeValidacionFactura>();
			if(!envioValidacion.getFactura().getTipo().equals(DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue())){
				envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
				mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_NO_ES_FACTURA ));
			}else{
				if(!envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue())){
					if(envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue())){
						mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_FACTURA_REGISTRADA ));
					}else if(envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.PAGADA.getValue())){
						mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_FACTURA_PAGADA ));
					}else if(foliosEnArchivo.contains( envioValidacion.getFactura().getNumeroFactura())){
						envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
						mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_FOLIO_EXISTENTE ));
					}else{
						foliosEnArchivo.add(envioValidacion.getFactura().getNumeroFactura());
					}
				}else{
					if(envioValidacion.getFactura().getNumeroFactura().equals(NUMERO_FACTURA_FICTICIA)){
						mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_PROCESAMIENTO ));
					}else{
						if(!this.esValidoRFCPorProveedor(envioValidacion.getFactura().getRfcEmisor(), idProveedor)){
							mensajes.add(this.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_RFC_PROVEEDOR ));
						}
					}
				}
			}
			envioValidacion.setMensajes(mensajes);
		}
	}
	
	
	
	
	
	@Override
	public DocumentoFiscal procesaXML(ByteArrayOutputStream bos,Integer idProveedor,DocumentoFiscal.TipoDocumentoFiscal tipo){
		DocumentoFiscal factura = new DocumentoFiscal();
		DocumentoFiscal facturaSiniestro = new DocumentoFiscal();
		String facturaXML;
		try {
			facturaXML = bos.toString("UTF-8");
			facturaXML.indexOf(0);
			if(facturaXML.indexOf("?") == 0){
				facturaXML = (String)facturaXML.subSequence(1, facturaXML.length());
			}
			
			
			String  version = new String();
			version = facturaXML.substring(facturaXML.indexOf("version=\"3.")+9, facturaXML.indexOf("version=\"3.")+12);
		    
		   
		    if(version.equals("3.2")){
		    		
		    	mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante comprobante = this.parsearCFD(bos);
			     facturaSiniestro = convierteComprobanteAFacturaSiniestro(comprobante,facturaXML);
		    
		    }
		    else
		    {    	
		    	String  Version3 = new String();
				Version3 = facturaXML.substring(facturaXML.indexOf("Version=\"3.3")+9, facturaXML.indexOf("Version=\"3.3")+12);
				if(Version3.equals("3.3")){
		
		    	com.cfdi.model.Comprobante comprobante  = this.parsearCFD33(bos);
		    	 facturaSiniestro = convierteComprobanteAFacturaSiniestro33(comprobante,facturaXML);
		    	
		    	
				}
		    }	
			
		
			if(facturaSiniestro.getTipo().equals(tipo.getValue())){
				if(this.esValidoRFCPorProveedor(facturaSiniestro.getRfcEmisor(), idProveedor)){
					DocumentoFiscal facturaExistente = this.recepcionFacturaDao.validaSiExiste(idProveedor,facturaSiniestro);
					if(facturaExistente!= null){
						if(!facturaExistente.getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue()) && !facturaExistente.getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.PAGADA.getValue())){
							this.actualizaInformacionDeLaFacturaExistente(facturaSiniestro, facturaExistente);
							factura = facturaSiniestro;
						}else{
							factura = facturaExistente;
						}
					}else{
						PrestadorServicio proveedor = entidadService.findById(PrestadorServicio.class, Integer.valueOf(idProveedor.toString()));
						facturaSiniestro.setProveedor(proveedor);
						factura = facturaSiniestro;
					}
				}else{
					log.error("El RFC NO COINCIDE CON EL PROVEEDOR");
					factura = facturaSiniestro;
					factura.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
				}
			}else{
				log.error("NO ES DEL TIPO DE DOCUMENTO FISCAL ESPERADO");
				factura = facturaSiniestro;
				factura.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
			}
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
			factura = generarFacturaFicticia();
		} catch (XmlException e) {
			log.error(e.getMessage(), e);
			factura = generarFacturaFicticia();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			factura = generarFacturaFicticia();
		} catch (Exception e) {
			log.error(e.getMessage(), e);
			factura = generarFacturaFicticia();
		}
		return factura;
	}
	
	/**
	 * Metodo para generar una factura dummy para cuando falla el parseo del XML
	 * @return
	 */
	private DocumentoFiscal generarFacturaFicticia(){
		DocumentoFiscal facturaFicticia = new DocumentoFiscal();
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		
		facturaFicticia.setNumeroFactura(NUMERO_FACTURA_FICTICIA);
		facturaFicticia.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
		facturaFicticia.setCodigoUsuarioCreacion(codigoUsuario);
		facturaFicticia.setCodigoUsuarioModificacion(codigoUsuario);
		
		return facturaFicticia;
	}
	
	@Override
	public List<EnvioValidacionFactura> agregaEstatusDescripcion(List<EnvioValidacionFactura> envioValidacionList){
		Map<String,String> estatusFacturas = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_FACTURA);
		for(EnvioValidacionFactura envio : envioValidacionList){
			String estatusDesc = estatusFacturas.get(envio.getFactura().getEstatus());
			envio.getFactura().setEstatusStr(estatusDesc);
		}
		return envioValidacionList;
	}
	
	@Override
	public Boolean esValidoRFCPorProveedor(String rfc,Integer proveedorId){
		
		Boolean esValido = Boolean.FALSE;
		PrestadorServicio proveedor = this.entidadService.findById(PrestadorServicio.class, proveedorId);
		String rfcPreestador = proveedor.getPersonaMidas().getRfc();
		if(rfcPreestador.equals(rfc.trim())){
			esValido = Boolean.TRUE;
		}
		return esValido;
	}
	
	
	private void actualizaInformacionDeLaFacturaExistente(DocumentoFiscal facturaNueva, DocumentoFiscal facturaExistente){
		facturaNueva.setId(facturaExistente.getId());
		facturaNueva.setFechaCreacion(facturaExistente.getFechaCreacion());
		facturaNueva.setCodigoUsuarioCreacion(facturaExistente.getCodigoUsuarioCreacion());
		facturaNueva.setFechaDevolucion(facturaExistente.getFechaModificacion());
	}
	

	
	@Override
	public List<EnvioValidacionFactura> creaEnvioValidacion(List<DocumentoFiscal> facturas){
		final String ORIGEN = "SIN";
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		List<EnvioValidacionFactura> envioValidacionList = new ArrayList<EnvioValidacionFactura>();
		Long batchId = this.recepcionFacturaDao.obtenerBatchId();
		for(DocumentoFiscal factura : facturas){
			EnvioValidacionFactura envioValidacionFactura = new EnvioValidacionFactura();
			envioValidacionFactura.setFactura(factura);
			envioValidacionFactura.setIdBatch(batchId);
			envioValidacionFactura.setEstatus(RecepcionFacturaServiceImpl.ENVIO_VALIDACION_PENDIENTE);
			envioValidacionFactura.setOrigen(ORIGEN);
			envioValidacionFactura.setFechaEnvio(new Date());
			envioValidacionFactura.setCodigoUsuarioCreacion(codigoUsuario);
			envioValidacionFactura.setCodigoUsuarioModificacion(codigoUsuario);
			envioValidacionList.add(envioValidacionFactura);
		}
		return envioValidacionList;
	}
	
	private List<String> findValidFileNames(File zipFile){
		List<String> validFileNameList = new ArrayList<String>();
		List<String> xmlFileNameList = new ArrayList<String>();
		List<String> pdfFileNameList = new ArrayList<String>();
		String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , FACTURA_VALIDAR_PDF);
		Boolean validaPdf = Boolean.FALSE;
		if(value != null && Integer.valueOf(value).intValue() == 1){
			validaPdf = Boolean.TRUE;
		}
		try {
			ZipInputStream dis = new ZipInputStream(new FileInputStream(zipFile));
			ZipEntry ze = dis.getNextEntry();
			while (ze != null) {
				String fileName = ze.getName();
				log.info("ZIP FileName :"+fileName);
				if(fileName.endsWith(".xml") || fileName.endsWith(".XML")){
					xmlFileNameList.add(fileName.substring(0, fileName.lastIndexOf(".")));
				}
				if(validaPdf && fileName.endsWith(".pdf")){
					pdfFileNameList.add(fileName.substring(0, fileName.lastIndexOf(".")));
				}
				dis.closeEntry();
				ze = dis.getNextEntry();
			}
			dis.closeEntry();
			dis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(validaPdf){
			for(String xmlFile : xmlFileNameList){
				if(pdfFileNameList.contains(xmlFile)){
					validFileNameList.add(xmlFile);
				}
			}
		}else{
			validFileNameList = xmlFileNameList;
		}
		return validFileNameList;
	}
	
	@Override
	public boolean isValidoXmlSolo(){
		
		String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , FACTURA_VALIDAR_PDF);
		if(value != null && Integer.valueOf(value).intValue() == 1){
			return true;
		}else{
			return false;
		}
		
	}
	
	private DocumentoFiscal convierteComprobanteAFacturaSiniestro(
			mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante comprobante, String xmlFactura ){
		DocumentoFiscal factura = new DocumentoFiscal();
		String numeroFactura = comprobante.getFolio();
		String nombreEmisor = comprobante.getEmisor().getNombre();
		String rfcEmisor = comprobante.getEmisor().getRfc();
		String nombreReceptor = comprobante.getReceptor().getNombre();
		String rfcReceptor = comprobante.getReceptor().getRfc();
		String monedaPago = comprobante.getMoneda();
		String tipoDocumentoFiscal = this.obtenerTipoDeDocumentoFiscal(comprobante);
		BigDecimal montoTotal = comprobante.getTotal();
		BigDecimal subTotal = comprobante.getSubTotal();
		mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos impuestos = comprobante.getImpuestos();
		BigDecimal iva = obtenerImpuestoTraslados(
				impuestos, mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado.Impuesto.IVA);
		BigDecimal ivaRetenido = obtenerImpuestoRetenciones(
				impuestos, mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion.Impuesto.IVA );
		BigDecimal isr = obtenerImpuestoRetenciones(
				impuestos, mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion.Impuesto.ISR);
		Date fechaFactura = comprobante.getFecha().getTime();
		
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		
		factura.setXmlFactura(xmlFactura);
		factura.setNumeroFactura(numeroFactura);
		factura.setEstatus(RecepcionFacturaServiceImpl.FACTURA_PROCESADA);
		factura.setNombreEmisor(nombreEmisor);
		factura.setRfcEmisor(rfcEmisor);
		factura.setNombreReceptor(nombreReceptor);
		factura.setRfcReceptor(rfcReceptor);
		factura.setMonedaPago(monedaPago);
		factura.setTipo(tipoDocumentoFiscal);
		factura.setIva(iva);
		factura.setIvaRetenido(ivaRetenido);
		factura.setIsr(isr);
		factura.setSubTotal(subTotal);
		factura.setMontoTotal(montoTotal);
		factura.setCodigoUsuarioCreacion(codigoUsuario);
		factura.setCodigoUsuarioModificacion(codigoUsuario);
		factura.setOrigen(OrigenDocumentoFiscal.LSN);
		factura.setFechaFactura(fechaFactura);
		return factura;
	}
	
	
	public DocumentoFiscal convierteComprobanteAFacturaSiniestro33(com.cfdi.model.Comprobante comprobante, String xmlFactura ){
		DocumentoFiscal factura = new DocumentoFiscal();
		String serie = ""; // se agrega validacion ya que no todas las facturas traen serie.
		if ( comprobante.getSerie() != null && !comprobante.getSerie().isEmpty())
		{
			serie = comprobante.getSerie();
		}
		String numeroFactura = serie + comprobante.getFolio();
		String nombreEmisor = comprobante.getEmisor().getNombre();
		String rfcEmisor = comprobante.getEmisor().getRfc();
		String nombreReceptor = comprobante.getReceptor().getNombre();
		String rfcReceptor = comprobante.getReceptor().getRfc();
		String monedaPago = comprobante.getMoneda().value();
		
		String tipoDocumentoFiscal = this.obtenerTipoDeDocumentoFiscal33(comprobante);
		BigDecimal montoTotal = comprobante.getTotal();
		BigDecimal subTotal = comprobante.getSubTotal();
		com.cfdi.model.Comprobante.Impuestos impuestos = comprobante.getImpuestos();
		com.cfdi.model.Comprobante.Impuestos.Traslados traslados = (impuestos != null ? impuestos.getTraslados(): null);
		com.cfdi.model.Comprobante.Impuestos.Retenciones retenciones = ( impuestos != null ? impuestos.getRetenciones() :null);
		
		// se agrega validacion cuando el campo de impuestos no se encuentre en la factura aun asi se pueda procesar.
		BigDecimal iva = BigDecimal.ZERO;
		if(impuestos != null || traslados != null)
		{ 
			iva = obtenerImpuestoTraslados33(impuestos, traslados);
		}
		
		BigDecimal ivaRetenido = BigDecimal.ZERO;
		if(impuestos != null || retenciones != null )
		{
			ivaRetenido = obtenerImpuestoRetencionesIVA33(impuestos, retenciones);
		}
		BigDecimal isr = BigDecimal.ZERO;
		if(impuestos != null || retenciones != null)
		{
			isr = obtenerImpuestoRetencionesISR33(impuestos, retenciones);
		}
		
		Date fechaFactura = comprobante.getFecha().toGregorianCalendar().getTime();
		
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		
		factura.setXmlFactura(xmlFactura);
		factura.setNumeroFactura(numeroFactura);
		factura.setEstatus(RecepcionFacturaServiceImpl.FACTURA_PROCESADA);
		factura.setNombreEmisor(nombreEmisor);
		factura.setRfcEmisor(rfcEmisor);
		factura.setNombreReceptor(nombreReceptor);
		factura.setRfcReceptor(rfcReceptor);
		factura.setMonedaPago(monedaPago);
		factura.setTipo(tipoDocumentoFiscal);
		factura.setIva(iva);
		factura.setIvaRetenido(ivaRetenido);
		factura.setIsr(isr);
		factura.setSubTotal(subTotal);
		factura.setMontoTotal(montoTotal);
		factura.setCodigoUsuarioCreacion(codigoUsuario);
		factura.setCodigoUsuarioModificacion(codigoUsuario);
		factura.setOrigen(OrigenDocumentoFiscal.LSN);
		factura.setFechaFactura(fechaFactura);
		return factura;
	}
	

	public String obtenerTipoDeDocumentoFiscal(
			mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante comprobante){
		String tipo = "OTRO";
		if(comprobante.getTipoDeComprobante()==mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.TipoDeComprobante.INGRESO){
			tipo = DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue();
		}else if (comprobante.getTipoDeComprobante()==mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.TipoDeComprobante.EGRESO){
			tipo = DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue();
		}
		return tipo;
	}
	
	public String obtenerTipoDeDocumentoFiscal33(
			com.cfdi.model.Comprobante  comprobante){
		String tipo = "OTRO";
		if(comprobante.getTipoDeComprobante()== com.cfdi.model.CTipoDeComprobante.I){
			tipo = DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue();
		}else if (comprobante.getTipoDeComprobante()== com.cfdi.model.CTipoDeComprobante.E){
			tipo = DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue();
		}
		return tipo;
	}
	
	public BigDecimal obtenerImpuestoTraslados(
			mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos impuestos, 
			mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado.Impuesto.Enum impuesto){
		BigDecimal importeImpuesto = BigDecimal.ZERO;
		mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados traslados = impuestos.getTraslados();
		if(traslados != null){
			mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado[] trasladosArray = traslados.getTrasladoArray();
			for(mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado traslado : trasladosArray){
				if(impuesto == traslado.getImpuesto()){
					importeImpuesto = importeImpuesto.add(traslado.getImporte());
				}
			}
		}
		return importeImpuesto;
	}
	
	
	public BigDecimal obtenerImpuestoTraslados33(
			com.cfdi.model.Comprobante.Impuestos impuestos, 
			com.cfdi.model.Comprobante.Impuestos.Traslados traslados){
		BigDecimal importeImpuesto = BigDecimal.ZERO;

		if(traslados != null){
			List<com.cfdi.model.Comprobante.Impuestos.Traslados.Traslado> trasladosArray = traslados.getTraslado();
			for(com.cfdi.model.Comprobante.Impuestos.Traslados.Traslado traslado : trasladosArray){
				if(traslado.getImpuesto().equals("002") ){
					importeImpuesto = importeImpuesto.add(traslado.getImporte());
				}
			}
		}
		return importeImpuesto;
	}
	
	public BigDecimal obtenerImpuestoRetenciones(
			mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos impuestos, mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion.Impuesto.Enum impuesto){
		BigDecimal importeImpuesto = BigDecimal.ZERO;
		mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones retenciones = impuestos.getRetenciones();
		if(retenciones!=null){
			mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion[] retencionesArray = retenciones.getRetencionArray();
			for(mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion retencion : retencionesArray){
				if(impuesto == retencion.getImpuesto()){
					importeImpuesto = importeImpuesto.add(retencion.getImporte());
				}
			}
		}
		return importeImpuesto;
	}

	public BigDecimal obtenerImpuestoRetencionesIVA33(
			com.cfdi.model.Comprobante.Impuestos impuestos, 
			com.cfdi.model.Comprobante.Impuestos.Retenciones retenciones){
		BigDecimal importeImpuesto = BigDecimal.ZERO;
		if(retenciones != null){
			List<com.cfdi.model.Comprobante.Impuestos.Retenciones.Retencion> retencionArray = retenciones.getRetencion();
			for(com.cfdi.model.Comprobante.Impuestos.Retenciones.Retencion retencion : retencionArray){
				if(retencion.getImpuesto().equals("002")){
					importeImpuesto = importeImpuesto.add(retencion.getImporte());
				}
			}
		}
		return importeImpuesto;
	}
	
	public BigDecimal obtenerImpuestoRetencionesISR33(
			com.cfdi.model.Comprobante.Impuestos impuestos, 
			com.cfdi.model.Comprobante.Impuestos.Retenciones retenciones){
		BigDecimal importeImpuesto = BigDecimal.ZERO;
		if(retenciones != null){
			List<com.cfdi.model.Comprobante.Impuestos.Retenciones.Retencion> retencionArray = retenciones.getRetencion();
			for(com.cfdi.model.Comprobante.Impuestos.Retenciones.Retencion retencion : retencionArray){
				if(retencion.getImpuesto().equals("001")){
					importeImpuesto = importeImpuesto.add(retencion.getImporte());
				}
			}
		}
		return importeImpuesto;
	}
	
	@Override
	public mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante parsearCFD(ByteArrayOutputStream bos) throws XmlException, IOException {
		mx.gob.sat.cfd.x3.ComprobanteDocument comprobanteDocument = mx.gob.sat.cfd.x3.ComprobanteDocument.Factory
				.parse(new ByteArrayInputStream(bos.toByteArray()));
		
	
		mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante comprobante = comprobanteDocument.getComprobante();
				return comprobante;
	}
	
	
	
	
public  com.cfdi.model.Comprobante  parsearCFD33(ByteArrayOutputStream bos) throws XmlException, IOException, JAXBException {
	
	
	JAXBContext jaxbContext = JAXBContext.newInstance(com.cfdi.model.Comprobante.class);
	Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
	
	//com.cfdi.model.Comprobante comp2 = new com.cfdi.model.Comprobante();
	
//    com.cfdi.model.ObjectFactory variable = new com.cfdi.model.ObjectFactory();	
//    variable.createComprobante();
//    com.cfdi.model.Comprobante comp = variable.createComprobante();
	return   (com.cfdi.model.Comprobante) jaxbUnmarshaller.unmarshal(new ByteArrayInputStream(bos.toByteArray()));
		
	}
	
	


	@SuppressWarnings("unchecked")
	public List<EnvioValidacionFactura> validarFacturasSiniestroAction(List<RelacionFacturaOrdenCompraDTO> listaFacturaOrdenCompra, 
				Long batchId, Integer idProveedor)
	{
		List<DocumentoFiscal> facturas = new ArrayList<DocumentoFiscal>();
		
		Map<String,Map<String, Object>> listaFacturasRelacionadas = new HashMap<String,Map<String, Object>>();
		
		//Organizar ordenes por factura
		for(RelacionFacturaOrdenCompraDTO registro :listaFacturaOrdenCompra)
		{
			if(!listaFacturasRelacionadas.containsKey(registro.getIdFactura()))
			{
				List<OrdenCompra> idsOrdenesCompra = new ArrayList<OrdenCompra>();
				idsOrdenesCompra.add(entidadService.findById(OrdenCompra.class, Long.valueOf(registro.getIdOrdenCompra())));
				
				Map<String, Object> relacion = new HashMap<String, Object>();
				relacion.put("factura", entidadService.findById(DocumentoFiscal.class, Long.valueOf(registro.getIdFactura())));
				relacion.put("ordenesCompra", idsOrdenesCompra );
				listaFacturasRelacionadas.put(registro.getIdFactura(),relacion);				
			}
			else
			{
				List<OrdenCompra> idsOrdenesCompra = (List<OrdenCompra>)listaFacturasRelacionadas.get(registro.getIdFactura()).get("ordenesCompra");
				idsOrdenesCompra.add(entidadService.findById(OrdenCompra.class, Long.valueOf(registro.getIdOrdenCompra())));				
			}		
		}		
		
		for(Map<String, Object> elemento : listaFacturasRelacionadas.values())
		{
			DocumentoFiscal factura = (DocumentoFiscal)elemento.get("factura");
			List<OrdenCompra> listaOrdenesCompra = (List<OrdenCompra>)elemento.get("ordenesCompra");
			
			factura.setOrdenesCompra(listaOrdenesCompra);
			
			facturas.add(factura);
		}		
		
		return this.validarFacturasSiniestro(facturas,batchId,idProveedor);		
	}
	
	@Override
	public List<EnvioValidacionFactura> validarFacturasSiniestro(List<DocumentoFiscal> facturas,Long batchId,Integer idProveedor){
		List<EnvioValidacionFactura> envioValidacionFacturaList = new ArrayList<EnvioValidacionFactura>();
		List<String> facturasAprobadas = new ArrayList<String>();
		List<EnvioValidacionFactura> facturasRechazadas = new ArrayList<EnvioValidacionFactura>();
		
		
		
		List<Long> idEnvioValidados = new ArrayList<Long>();
		
		for(DocumentoFiscal factura: facturas){
			EnvioValidacionFactura envioValidacionFactura = obtenerEnvioValidacionFactura(batchId, factura.getId());
			
			idEnvioValidados.add(envioValidacionFactura.getId());
			
			List<MensajeValidacionFactura> mensajes = validaAsociarLaFacturaConLasOrdenesAsignadas(factura,envioValidacionFactura);
			if(mensajes.size() == 0){
				try{
//					OBTENIENDO LOS RFCS DE AFIRME CONFIGURADOS EN LA TABLA DE PARAMETROS GLOBALES
					String stringRfcsAfirme = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , RFC_AFIRME);
					String rfcAfirme = "";
//					SI ALGUNO COINCIDE CON EL DE LA FACTURA, SE ENVIA A AXOSNET, SI NO SE ENVIA EL PRIMERO PARA QUE DESPLIEGUE EL ERROR
					String[] rfcsAfirme = stringRfcsAfirme.split(",");
					for(String rfc : rfcsAfirme){
						if(rfc.equalsIgnoreCase(factura.getRfcReceptor())){
							rfcAfirme = rfc;
							break;
						}
					}
					if(rfcAfirme.isEmpty()){
						rfcAfirme = rfcsAfirme[0];
					}
					
					envioValidacionFactura.setFactura(factura);
					EnvioFactura envioFactura = new EnvioFactura();
					envioFactura.setComprobante(factura.getXmlFactura());
					envioFactura.setOrigenEnvio(ORIGEN_ENVIO);
					envioFactura.setIdOrigenEnvio(factura.getId());
					envioFactura.setIdSociedad(rfcAfirme);
					envioFactura.setIdDepartamento("0003");
					RecepcionFacturaService processor = this.sessionContext.getBusinessObject(
							RecepcionFacturaService.class);
					
					String seValidaConAxosNet = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , VALIDA_AXOSNET);
					if(Boolean.valueOf(seValidaConAxosNet)){
						envioFactura = processor.enviaAAxosNET(envioFactura);
						mensajes = this.convierteAMensajesValidacionFactura(envioFactura.getRespuestas(),envioValidacionFactura);
						envioValidacionFactura.setMensajes(mensajes);
					}
					if(Boolean.valueOf(seValidaConAxosNet)){
						if(envioFactura.getEstatusEnvio().intValue()== 1
								|| reenviarAxosnet(envioFactura)){
							factura.setEstatus(FACTURA_REGISTRADA);
							facturasAprobadas.add(factura.getNumeroFactura());
							
							// BITACORA CREAR FACTURA
							bitacoraService.registrar(TIPO_BITACORA.FACTURACION, EVENTO.CREACION_FACTURA, factura.getId().toString(), "Crear Factura","DocumentoFiscal.id="+factura.getId().toString(), usuarioService.getUsuarioActual().getNombreUsuario());
							// CREAR ORDENES DE PAGO
							this.crearOrdenesPago(factura.getOrdenesCompra());
						}else{
							factura.setOrdenesCompra(null);
							factura.setEstatus(FACTURA_ERROR);
							facturasRechazadas.add(envioValidacionFactura);
						}
					}else{
						factura.setEstatus(FACTURA_REGISTRADA);
						facturasAprobadas.add(factura.getNumeroFactura());
						
						// BITACORA CREAR FACTURA
						bitacoraService.registrar(TIPO_BITACORA.FACTURACION, EVENTO.CREACION_FACTURA, factura.getId().toString(), "Crear Factura","DocumentoFiscal.id="+factura.getId().toString(), usuarioService.getUsuarioActual().getNombreUsuario());
						// CREAR ORDENES DE PAGO
						this.crearOrdenesPago(factura.getOrdenesCompra());
					}
					
				}catch (Exception e) {
					factura.setEstatus(FACTURA_ERROR);
					MensajeValidacionFactura mensajeError = this.creaMensajeValidacionFactura(envioValidacionFactura, 666L, e.getMessage());
					ArrayList<MensajeValidacionFactura> mensajesErrorList = new ArrayList<MensajeValidacionFactura>();
					mensajesErrorList.add(mensajeError);
					envioValidacionFactura.setMensajes(mensajesErrorList);
					factura.setOrdenesCompra(null);
					log.error(e.getMessage(), e);
				}
				this.entidadService.save(factura);
			}else{
				factura.setEstatus(FACTURA_ERROR);
				factura.setOrdenesCompra(null);
				envioValidacionFactura.setMensajes(mensajes);
				envioValidacionFactura.setFactura(factura);
				facturasRechazadas.add(envioValidacionFactura);
			}
			this.entidadService.save(envioValidacionFactura);
			envioValidacionFacturaList.add(envioValidacionFactura);
		}
		
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("idBatch", batchId);
		
		List<EnvioValidacionFactura> envioValidaciones = this.entidadService.findByProperties(EnvioValidacionFactura.class, params);
		List<EnvioValidacionFactura> envioValidacionesAEliminar = new ArrayList<EnvioValidacionFactura>();
		for(EnvioValidacionFactura envioValidacion : envioValidaciones){
			if(!idEnvioValidados.contains(envioValidacion.getId())){
				envioValidacionesAEliminar.add(envioValidacion);
			}
		}
		this.entidadService.removeAll(envioValidacionesAEliminar);
		PrestadorServicio proveedor = this.entidadService.findById(PrestadorServicio.class, idProveedor);
		this.enviarNotificacion(proveedor, facturasAprobadas, facturasRechazadas);
		return envioValidacionFacturaList;
	}
	
	/**
	 * Metodo que valida si ya ha sido enviada previamente la factura y permite que se vuelva a enviar  
	 * @param envioFactura
	 * @return
	 */
	private boolean reenviarAxosnet(EnvioFactura envioFactura){
		boolean reenviar = false;
		if(envioFactura.getEstatusEnvio() == 0
				&& envioFactura.getNumeroErrores() == 1){
			for(EnvioFacturaDet det : envioFactura.getRespuestas()){
				if(det.getIdTipoRespuesta() == 20){
					reenviar = true;
					break;
				}
			}
		}
		
		return reenviar;
	}
	
	private void crearOrdenesPago(List<OrdenCompra> lOrdenesCompra ){
		try{
			if( !lOrdenesCompra.isEmpty() ){
				for(OrdenCompra ordenCompra:lOrdenesCompra){
					OrdenPagoSiniestro ordenPagoSiniestro = pagosSiniestroService.generacionAutomaticaOrdenPago(ordenCompra.getId());
					if( ordenPagoSiniestro != null ){
						pagosSiniestroService.autorizarOrdenPago(ordenPagoSiniestro.getId());
					}
				}
			}
		}catch(Exception e){
			log.error(e.getMessage(), e);
		}
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EnvioFactura enviaAAxosNET(EnvioFactura envioFactura) throws FileNotFoundException, PersistenceException, Exception{
		return this.envioFacturaService.saveEnvio(envioFactura);
	}
	
	@Override
	public List<MensajeValidacionFactura> convierteAMensajesValidacionFactura( List<EnvioFacturaDet> axosNetRespuestas, EnvioValidacionFactura envioValidacionFactura ){
		List<MensajeValidacionFactura> mensajes = new ArrayList<MensajeValidacionFactura>();
		for(EnvioFacturaDet respuesta  : axosNetRespuestas){
			MensajeValidacionFactura mensaje = creaMensajeValidacionFactura(envioValidacionFactura, Long.valueOf(respuesta.getIdTipoRespuesta()), respuesta.getDescripcionRespuesta());
			mensajes.add(mensaje);
		}
		return mensajes;
	}
	
	private EnvioValidacionFactura obtenerEnvioValidacionFactura(Long batchId, Long facturaId){
		final int PRIMER_ELEMENTO = 0;
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("idBatch", batchId);
		params.put("factura.id", facturaId);
		List<EnvioValidacionFactura> envioValidacion = this.entidadService.findByProperties(EnvioValidacionFactura.class, params);
		return envioValidacion.get(PRIMER_ELEMENTO);
	}
	
	private List<MensajeValidacionFactura> validaAsociarLaFacturaConLasOrdenesAsignadas(DocumentoFiscal factura,EnvioValidacionFactura envioValidacionFactura  ){
		String strDiffImpuesto = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , FACTURA_DIFF_IMPUESTO);
		String strDiffTotal = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , FACTURA_DIFF_TOTAL);
		final BigDecimal DIFERENCIA_UN_PESOS = new BigDecimal(strDiffImpuesto);
		final BigDecimal DIFERENCIA_CINCO_PESOS = new BigDecimal(strDiffTotal);
		
		List<MensajeValidacionFactura> mensajes = new ArrayList<MensajeValidacionFactura>(); 
		BigDecimal iva = new BigDecimal(0);
		BigDecimal ivaRetenido = new BigDecimal(0);
		BigDecimal isr = new BigDecimal(0);
		BigDecimal subTotal = new BigDecimal(0);
		BigDecimal total = new BigDecimal(0);
		for(OrdenCompra orden : factura.getOrdenesCompra()){
			ImportesOrdenCompraDTO importesDTO = ordenCompraService.calcularImportes(orden.getId(), null,false);
			subTotal = subTotal.add(importesDTO.getSubtotal());
			total = total.add(importesDTO.getTotal());
			iva = iva.add(importesDTO.getIva());
			ivaRetenido = ivaRetenido.add(importesDTO.getIvaRetenido());
			isr = isr.add(importesDTO.getIsr());
		}
		if(!this.esElMontoIgualConDiferencia(factura.getMontoTotal(), total, DIFERENCIA_CINCO_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_TOTAL ));
		}
		if(!this.esElMontoIgualConDiferencia(factura.getSubTotal(), subTotal, DIFERENCIA_UN_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_SUBTOTAL));
		}
		if(!this.esElMontoIgualConDiferencia(factura.getIva(), iva, DIFERENCIA_UN_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_IVA));
		}
		if(!this.esElMontoIgualConDiferencia(factura.getIvaRetenido(), ivaRetenido, DIFERENCIA_UN_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_IVA_RETENIDO));
		}
		if(!this.esElMontoIgualConDiferencia(factura.getIsr(), isr, DIFERENCIA_UN_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_ISR));
		}
		return mensajes;
	}
	
	public EnvioValidacionFactura validaAsociarLaFacturaLiquidacion(DocumentoFiscal factura,EnvioValidacionFactura envioValidacionFactura){
		String strDiffImpuesto = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , FACTURA_DIFF_IMPUESTO);
		String strDiffTotal = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , FACTURA_DIFF_TOTAL);
		final BigDecimal DIFERENCIA_UN_PESOS = new BigDecimal(strDiffImpuesto);
		final BigDecimal DIFERENCIA_CINCO_PESOS = new BigDecimal(strDiffTotal);
		
		List<MensajeValidacionFactura> mensajes = new ArrayList<MensajeValidacionFactura>(); 
		LiquidacionCompensaciones liquidacion = factura.getLiquidacionCompensaciones();
		
		if(!this.esElMontoIgualConDiferencia(factura.getMontoTotal(), new BigDecimal(liquidacion.getImporteTotal()), DIFERENCIA_CINCO_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_TOTAL ));
		}
		/*if(!this.esElMontoIgualConDiferencia(factura.getSubTotal(), liquidacion.getNetoPorPagar(), DIFERENCIA_UN_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_SUBTOTAL));
		}*/
		if(!this.esElMontoIgualConDiferencia(factura.getIva(), new BigDecimal(liquidacion.getIva()), DIFERENCIA_UN_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_IVA));
		}
		if(!this.esElMontoIgualConDiferencia(factura.getIvaRetenido(), new BigDecimal(liquidacion.getIvaRetenido()), DIFERENCIA_UN_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_IVA_RETENIDO));
		}
		if(!this.esElMontoIgualConDiferencia(factura.getIsr(), new BigDecimal(liquidacion.getIsr()), DIFERENCIA_UN_PESOS)){
			mensajes.add(this.creaMensajeValidacionFactura(envioValidacionFactura,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_ISR));
		}
		
		if(mensajes.size() == 0){
			try{
				envioValidacionFactura.setFactura(factura);
				EnvioFactura envioFactura = new EnvioFactura();
				envioFactura.setComprobante(factura.getXmlFactura());
				envioFactura.setOrigenEnvio(ORIGEN_ENVIO);
				envioFactura.setIdOrigenEnvio(factura.getId());
				RecepcionFacturaService processor = this.sessionContext.getBusinessObject(
						RecepcionFacturaService.class);
				
				String seValidaConAxosNet = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , VALIDA_AXOSNET);
				if(Boolean.valueOf(seValidaConAxosNet)){
					envioFactura = processor.enviaAAxosNET(envioFactura);
					mensajes = this.convierteAMensajesValidacionFactura(envioFactura.getRespuestas(),envioValidacionFactura);
					envioValidacionFactura.setMensajes(mensajes);
				}
				if(Boolean.valueOf(seValidaConAxosNet)){
					if(envioFactura.getEstatusEnvio().intValue()== 1){
						factura.setEstatus(FACTURA_REGISTRADA);
						
						// BITACORA CREAR FACTURA
						bitacoraService.registrar(TIPO_BITACORA.FACTURACION, EVENTO.CREACION_FACTURA, factura.getId().toString(), "Crear Factura","DocumentoFiscal.id="+factura.getId().toString(), usuarioService.getUsuarioActual().getNombreUsuario());
					
					}else{
						factura.setLiquidacionCompensaciones(null);
						factura.setEstatus(FACTURA_ERROR);
					}
				}else{
					factura.setEstatus(FACTURA_REGISTRADA);
					// BITACORA CREAR FACTURA
					bitacoraService.registrar(TIPO_BITACORA.FACTURACION, EVENTO.CREACION_FACTURA, factura.getId().toString(), "Crear Factura","DocumentoFiscal.id="+factura.getId().toString(), usuarioService.getUsuarioActual().getNombreUsuario());
				}
				
			}catch (Exception e) {
				factura.setEstatus(FACTURA_ERROR);
				MensajeValidacionFactura mensajeError = this.creaMensajeValidacionFactura(envioValidacionFactura, 666L, e.getMessage());
				ArrayList<MensajeValidacionFactura> mensajesErrorList = new ArrayList<MensajeValidacionFactura>();
				mensajesErrorList.add(mensajeError);
				envioValidacionFactura.setMensajes(mensajesErrorList);
				factura.setOrdenesCompra(null);
				log.error(e.getMessage(), e);
			}
			this.entidadService.save(factura);
		}else{
			factura.setEstatus(FACTURA_ERROR);
			factura.setLiquidacionCompensaciones(null);
			envioValidacionFactura.setMensajes(mensajes);
			envioValidacionFactura.setFactura(factura);
			//facturasRechazadas.add(envioValidacionFactura);
		}
		this.entidadService.save(envioValidacionFactura);
		
		return envioValidacionFactura;
	}
	
	
	
	@Override
	public MensajeValidacionFactura creaMensajeValidacionFactura(EnvioValidacionFactura envioValidacionFactura  ,Long tipoMensaje, String texto){
		MensajeValidacionFactura mensaje = new MensajeValidacionFactura();
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		mensaje.setMensaje(texto);
		mensaje.setTipoMensaje(tipoMensaje);
		mensaje.setCodigoUsuarioCreacion(codigoUsuario);
		mensaje.setCodigoUsuarioModificacion(codigoUsuario);
		mensaje.setEnvioValidacionFactura(envioValidacionFactura);
		return mensaje;
	}
	
	private Boolean esElMontoIgualConDiferencia(BigDecimal primerMonto, BigDecimal segundoMonto, BigDecimal diferenciaValida){
		if(primerMonto==null){
			return Boolean.TRUE;
		}
		Boolean esValido = Boolean.FALSE;
		BigDecimal diferencia = primerMonto.subtract(segundoMonto).abs();
		if(diferencia.compareTo(diferenciaValida) <= 0){
			esValido = Boolean.TRUE;
		}
		return esValido;
	}
	
	@Override
	public Map<Long,String> obtenerFacturasPorBatchId(Long batchId){
		List<EnvioValidacionFactura> envioValidacionList = this.recepcionFacturaDao.obtenerFacturasAsignables(batchId);
		Map<Long,String> facturasMap = new HashMap<Long, String>();
		for(EnvioValidacionFactura envioValidacion : envioValidacionList){
			facturasMap.put(envioValidacion.getFactura().getId(), envioValidacion.getFactura().getNumeroFactura() 
					+ " - (" +  UtileriasWeb.formatoMoneda(envioValidacion.getFactura().getMontoTotal())+ ")");
		}
		return facturasMap;
	}
	
	
	private void enviarNotificacion(PrestadorServicio proveedor,List<String> facturasAprobadas,List<EnvioValidacionFactura> enviosRechazados){
		List<String> direccionesDeCorreo = new ArrayList<String>();
		String nombreProveedor = proveedor.getPersonaMidas().getNombre();
		direccionesDeCorreo.add(proveedor.getPersonaMidas().getContacto().getCorreoPrincipal());
		String titulo = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas2.siniestros.pagos.facturas.titulo");
		String mensaje = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas2.siniestros.pagos.facturas.cuerpo");
		String saludo = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas2.siniestros.pagos.facturas.saludo",nombreProveedor);
		
		if(facturasAprobadas.size()>0){
			String textoFacturasAprobadas = this.creaMensajeDeFacturasAprobadas(facturasAprobadas); 
			mensaje += textoFacturasAprobadas;
		}
		if(enviosRechazados.size()>0){
			String textoFacturasRechazadas = this.creaMensajeDeFacturasRechazadas(enviosRechazados);
			mensaje += textoFacturasRechazadas;
		}
		this.mailService.sendMail(direccionesDeCorreo,titulo,mensaje,null,titulo,saludo);
	}
	
	private String creaMensajeDeFacturasAprobadas(List<String> facturas){
		String listaFacturas = this.convierteAListaHtml(facturas);
		String mensaje = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas2.siniestros.pagos.facturas.aprobadas",listaFacturas);
		return mensaje;
	}
	
	private String creaMensajeDeFacturasRechazadas(List<EnvioValidacionFactura> enviosRechazados){
		String listaFacturasRechazadas = this.convierteAEnviosHtml(enviosRechazados);
		String mensaje = Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas2.siniestros.pagos.facturas.rechazadas",listaFacturasRechazadas);
		return mensaje;
	}
	
	private String convierteAListaHtml(List<String> facturas){
		StringBuilder html = new StringBuilder("");
		if(facturas.size()>0){
			html.append("<ul>");
			for(String factura : facturas){
				html.append("<li>"+factura+"</li>");
			}
			html.append("</ul>");
		}
		return html.toString();
	}
	
	private String convierteAEnviosHtml(List<EnvioValidacionFactura> envios){
		StringBuilder html = new StringBuilder("");
		if(envios.size()>0){
			html.append("<ul>");
			for(EnvioValidacionFactura envio : envios){
				html.append("<li>"+envio.getFactura().getNumeroFactura()+"</li>");
				if(envio.getMensajes()!=null && envio.getMensajes().size()>0){
					html.append("<ul>");
					for(MensajeValidacionFactura mensaje : envio.getMensajes()){
						html.append("<li>"+mensaje.getMensaje()+"</li>");
					}
					html.append("</ul>");
				}
				html.append("</li>");
			}
			html.append("</ul>");
		}
		return html.toString();
	}


	@Override
	public List<DocumentoFiscal> obtenerFacturasRegistradaByOrdenCompra(
			Long idOrdenCompra, String estatusFactura) {
	
		return (List<DocumentoFiscal>) this.recepcionFacturaDao.obtenerFacturasRegistradaByOrdenCompra(idOrdenCompra, estatusFactura);
	}

	@Override
	public EnvioValidacionFactura creaEnvioValidacion(DocumentoFiscal factura,Long batchId) {
		final String ORIGEN = "SIN";
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		EnvioValidacionFactura envioValidacionFactura = new EnvioValidacionFactura();
		envioValidacionFactura.setFactura(factura);
		envioValidacionFactura.setIdBatch(batchId);
		envioValidacionFactura.setEstatus(RecepcionFacturaServiceImpl.ENVIO_VALIDACION_PENDIENTE);
		envioValidacionFactura.setOrigen(ORIGEN);
		envioValidacionFactura.setFechaEnvio(new Date());
		envioValidacionFactura.setCodigoUsuarioCreacion(codigoUsuario);
		envioValidacionFactura.setCodigoUsuarioModificacion(codigoUsuario);
		return envioValidacionFactura;
	}

	@Override
	public DocumentoFiscal guardaAgrupador(Integer idPrestador, String ordenesCompraConcat){		
		PrestadorServicio compania = this.entidadService.findById(PrestadorServicio.class, idPrestador);
		String[] ordenCompraIdStrArray = ordenesCompraConcat.split(",");
		List<Long> ordenesCompraId = new ArrayList<Long>();
		for(String ordenCompraIdStr : ordenCompraIdStrArray){
			ordenesCompraId.add(Long.valueOf(ordenCompraIdStr));
		}
		DocumentoFiscal factura = this.creaFacturaParaAgrupar(compania, ordenesCompraId);
		crearOrdenesPago(factura.getOrdenesCompra());
		// PERSISTIR LA INFORMACION
		factura = this.entidadService.save(factura);
		// RECUPERAR EL ID CREADO, AGREGARLO AL NOMBRE DEL AGRUPADOR Y VOLVER A PERSISTIR
		factura.setNombreAgrupador( factura.getNombreAgrupador().replace("*", factura.getId().toString()) );
		this.entidadService.save(factura);
		return factura;
		
	}
	
	@Override
	public DocumentoFiscal eliminaAgrupador(Long idAgrupador){
		DocumentoFiscal entidad = this.entidadService.findById(DocumentoFiscal.class, idAgrupador);
		List<LiquidacionSiniestro> liquidaciones = this.entidadService.findByProperty(LiquidacionSiniestro.class, "facturas",entidad);
		if(liquidaciones.isEmpty()) {
			this.entidadService.remove(entidad);
		}else {
			throw new NegocioEJBExeption("ASOC_A_LIQUIDACION","El agrupador ("+entidad.getNombreAgrupador()+") NO SE PUEDE CANCELAR POR ESTAR ASOCIADO A UNA LIQUIDACION") ;
		}
		return entidad;
	}
	
	private DocumentoFiscal creaFacturaParaAgrupar(PrestadorServicio compania, List<Long> idOrdenesCompra){
		final String AGRUPADOR = "Agrupador";
		final String MONEDA = "MXN";
		final String TIPO_FACTURA = "FACT";
		
		List<OrdenCompra> ordenesCompra = new ArrayList<OrdenCompra>();
		List<String> lOficinas = new ArrayList<String>();
		
		DocumentoFiscal factura = new DocumentoFiscal();
		String numeroFactura = "0";
		String nombreEmisor = compania.getPersonaMidas().getNombre();
		String rfcEmisor = compania.getPersonaMidas().getRfc();
		String nombreReceptor = AGRUPADOR;
		String rfcReceptor = AGRUPADOR;
		String monedaPago = MONEDA;
		String tipoDocumentoFiscal = TIPO_FACTURA;
		
		BigDecimal montoTotal = BigDecimal.ZERO;
		BigDecimal subTotal = BigDecimal.ZERO;
		BigDecimal iva = BigDecimal.ZERO;
		BigDecimal ivaRetenido = BigDecimal.ZERO;
		BigDecimal isr = BigDecimal.ZERO;
		
		String tipoAgrupador = DocumentoFiscal.TipoAgrupadorDocumentoFiscal.CIA.toString();
		
		for(Long ordenCompraId: idOrdenesCompra){
			OrdenCompra ordenCompra = this.entidadService.findById(OrdenCompra.class, ordenCompraId);
			if(StringUtils.isNotEmpty(ordenCompra.getOrigen()) && OrdenCompraService.ORIGENSIPAC_AUTOMATICO.equals(ordenCompra.getOrigen())){
				tipoAgrupador = DocumentoFiscal.TipoAgrupadorDocumentoFiscal.SIPAC.toString();
			}
			ordenesCompra.add(ordenCompra);
			ImportesOrdenCompraDTO importesDTO = this.ordenCompraService.calcularImportes(ordenCompraId, null, false);
			montoTotal = importesDTO.getTotal();
			subTotal = importesDTO.getSubtotal();
			iva = importesDTO.getIva();
			ivaRetenido = importesDTO.getIvaRetenido();
			isr = importesDTO.getIsr();
			lOficinas.add(ordenCompra.getReporteCabina().getOficina().getClaveOficina());
		}
		
		// PROCESA OFICINAS SELECCIONADAS PARA GENERAR NOMBRE PARA AGRUPADOR
		String nombreAgrupador = this.generaNombreAgrupador(lOficinas, ordenesCompra)+"/*/"+idOrdenesCompra.size();
		
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		
		factura.setNumeroFactura(numeroFactura);
		factura.setEstatus(RecepcionFacturaServiceImpl.FACTURA_REGISTRADA);
		factura.setNombreEmisor(nombreEmisor);
		factura.setRfcEmisor(rfcEmisor);
		factura.setNombreReceptor(nombreReceptor);
		factura.setRfcReceptor(rfcReceptor);
		factura.setMonedaPago(monedaPago);
		factura.setTipo(tipoDocumentoFiscal);
		factura.setIva(iva);
		factura.setIvaRetenido(ivaRetenido);
		factura.setIsr(isr);
		factura.setSubTotal(subTotal);
		factura.setMontoTotal(montoTotal);
		factura.setCodigoUsuarioCreacion(codigoUsuario);
		factura.setCodigoUsuarioModificacion(codigoUsuario);
		factura.setOrigen(OrigenDocumentoFiscal.LSN);
		factura.setOrdenesCompra(ordenesCompra);
		factura.setProveedor(compania);
		factura.setNombreAgrupador(nombreAgrupador);
		factura.setTipoAgrupador(tipoAgrupador);
		return factura;
	}
	
	
	// TODO: GENERAR NOMBRE DE AGRUPADOR
	private String generaNombreAgrupador(List<String> lOficinas, List<OrdenCompra> ordenesCompra) {
		
		String   codigoOficina = "";
		
		Collections.sort(ordenesCompra,
			new Comparator<OrdenCompra>() {				
				public int compare(OrdenCompra o1, OrdenCompra o2 ){
					return o1.getId().compareTo(o2.getId());
				}
		});
		
		if( lOficinas.size() == 1 ) {
			codigoOficina =  lOficinas.get(0);
		}else if ( lOficinas.size() == 2 ) {
						
			codigoOficina = ordenesCompra.get(0).getReporteCabina().getClaveOficina();
			
		}else {
		
			if( lOficinas.size() > 1 ) {
				
				// ORDENAR LAS OFICINAS
				Collections.sort(lOficinas);
				
				String[] nombres  = new String[lOficinas.size()];
				int[]    contador = new int[lOficinas.size()];
				int      i        = 0;
				
				// CONTAR OFICINAS
				Set<String> oficinasUnicas = new HashSet<String>(lOficinas);
		        for (String lstCodigoOficina : oficinasUnicas) {
		            if( !lstCodigoOficina.isEmpty() ) {
		            	nombres[i]  = lstCodigoOficina;
		            	contador[i] = Collections.frequency(lOficinas, lstCodigoOficina);
		            }
		            i++;
		        }
		        
		        //ORDENAR DATOS
		        int aux = 0;
		        String auxNombre = "";
		        
		        for( int a = 0; a < contador.length-1; a++) {
		        	for( int c = 0; c < contador.length-a-1; c++ ) {
		        		if( contador[c] < contador[c+1] ) {
		        			aux = contador[c+1];
		        			auxNombre = nombres[c+1];
		        			
		        			contador[c+1] = contador[c];
		        			nombres[c+1] = nombres[c];
		        			
		        			contador[c] = aux;
		        			nombres[c] = auxNombre;
		        		}
		        	}
		        }

		        
		        // VALIDAR QUE EL ARRAY TENGA MAS DE 2 ELEMENTOS O MAS
		        if( contador.length >= 1 ) {
		        	// TOMAR LAS 2 PRIMERAS POSICIONES Y COMPARAR,
		        	// SI LA PRIMERA ES MAYOR TOMAR EL CODIGO DE LA OFICINA
		        	if( contador[0] > contador[1]  ) {
		        		codigoOficina = nombres[0];
		        	}else {
		        		codigoOficina = ordenesCompra.get(0).getReporteCabina().getClaveOficina();
		        	}
		        }else {
		        	codigoOficina = nombres[0];
		        }
			
			}
		
		}
		
		return codigoOficina;
	}	
	
	
	
	@Override
	public List<DocumentoFiscal> buscarAgrupadores(OrdenCompraProveedorDTO filtroAgrupador){
		List<DocumentoFiscal> facturas = this.recepcionFacturaDao.recepcionFacturaService(filtroAgrupador);
		return this.agregarSiniestroYOficina(facturas);
	}
	
	private List<DocumentoFiscal> agregarSiniestroYOficina (List<DocumentoFiscal> facturas){
		final String VARIOS = "VARIOS";
		
		for(DocumentoFiscal factura : facturas){
			String siniestro = null;
			String oficina = null;
			String siniestroTercero = null;
			boolean validaSiniestro = true;
			boolean validaOficina = true;
			for(OrdenCompra orden : factura.getOrdenesCompra()){
				if(orden.getCartaPago()!=null){
					String siniestroTerceroOC = orden.getCartaPago().getSiniestroTercero();
					if(siniestroTercero == null ){
						siniestroTercero = siniestroTerceroOC;
					}else if(!siniestroTercero.equals(siniestroTerceroOC)){
							siniestroTercero = VARIOS;
					}
				}
				
				Long estimacionId = orden.getIdTercero();
				if(estimacionId!=null){
					EstimacionCoberturaReporteCabina estimacion = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
					ReporteCabina reporte = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina();
					if(validaSiniestro &&  reporte.getSiniestroCabina()!=null){
						if(siniestro == null){
							siniestro = reporte.getSiniestroCabina().getNumeroSiniestro();
						}else{
							if(!siniestro.equals(reporte.getSiniestroCabina().getNumeroSiniestro())){
								siniestro = VARIOS;
								validaSiniestro = false;
							}
						}
					}
					if(validaOficina){
						if(oficina == null){
							oficina = reporte.getOficina().getNombreOficina();
						}else{
							if(!oficina.equals(reporte.getOficina().getNombreOficina())){
								oficina = VARIOS;
								validaOficina = false;
							}
						}
					}
					if(!validaOficina && !validaSiniestro){
						break;
					}
				}
			}
			factura.setNumeroSiniestro(siniestro);
			factura.setOficina(oficina);
			factura.setSiniestroTercero(siniestroTercero);
		}
		return facturas;
	}
	
	@Override
	public Boolean validaNombreDelAgrupador(String nombreAgrupador){
		return this.recepcionFacturaDao.validaNombreDelAgrupador(nombreAgrupador);
	}


}
