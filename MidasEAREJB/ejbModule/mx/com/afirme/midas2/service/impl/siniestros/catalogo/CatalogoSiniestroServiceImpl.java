package mx.com.afirme.midas2.service.impl.siniestros.catalogo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.siniestros.catalogo.CatalogoSiniestroDao;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;

@Stateless
public class CatalogoSiniestroServiceImpl implements CatalogoSiniestroService {


	@EJB
	protected EntidadService entidadService;
	@EJB
	protected CatalogoSiniestroDao catalogoSiniestroDAO;


	@Override
	public <K extends CatalogoFiltro, E extends Entidad> List<E> buscar(
			Class<E> entidad, K filtro) {
		return catalogoSiniestroDAO.buscar(entidad, filtro);
	}
	
	@Override
	public <K extends CatalogoFiltro, E extends Entidad, T extends Object> List<E> buscar(
			 Class<E> entidad, K filtro, String ordenadoPor){
		return catalogoSiniestroDAO.buscar(entidad, filtro, ordenadoPor);	
	}

	@Override
	public <E extends Entidad, K> E obtener(Class<E> entityClass, K id) {
		return entidadService.findById(entityClass, id);
	}

	@Override
	public <E extends Entidad> void salvar(E entidad) {
		entidadService.save(entidad);		
	}

	@Override
	public <E extends Entidad> Long salvarObtenerId(E entidad) {
		Long id = (Long)entidadService.saveAndGetId(entidad);
		return id;
	}
}
