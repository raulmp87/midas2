package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatTipoBeneficio;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatTipoBeneficioService;

@Stateless
public class CatTipoBeneficioServiceImpl implements CatTipoBeneficioService{
	
	private static final long serialVersionUID = 1L;

	@EJB private EntidadService entidadService;
	
	@Override
	public CatTipoBeneficio completeObject(CatTipoBeneficio catTipoBeneficio) {
		CatTipoBeneficio retorno = new CatTipoBeneficio(); 
		if(catTipoBeneficio != null){
			if(catTipoBeneficio.getId().intValue() < 1){
				retorno = this.findIdByAttributes(catTipoBeneficio);
			}else{
				if(!validateAttributes(catTipoBeneficio)){
					retorno = this.findById(catTipoBeneficio.getId());
				}
			}
		}else{
			retorno.setId(new Long(-1));
		}
		return retorno;
	}

	private CatTipoBeneficio findById(long id) {
		CatTipoBeneficio retorno = new CatTipoBeneficio();
		if(id >= 0){
			retorno = entidadService.findById(CatTipoBeneficio.class, id);
		}
		return retorno;
	}

	@Override
	public List<CatTipoBeneficio> findByStatus(boolean status) {
		return entidadService.findByProperty(CatTipoBeneficio.class, "estatus", status?0:1);
	}
	
	private boolean validateAttributes(CatTipoBeneficio catTipoBeneficio){
		return catTipoBeneficio.getDescCatTipoBeneficio() != null && !catTipoBeneficio.getDescCatTipoBeneficio().equals("");
	}

	private CatTipoBeneficio findIdByAttributes(CatTipoBeneficio catTipoBeneficio) {
		if(validateAttributes(catTipoBeneficio)){
			Map<String,Object> parametros = new HashMap<String,Object>();
			parametros.put("descCatTipoBeneficio", catTipoBeneficio.getDescCatTipoBeneficio());
			List<CatTipoBeneficio> catTipoBeneficioList = entidadService.findByProperties(CatTipoBeneficio.class, parametros);
			if(!catTipoBeneficioList.isEmpty()){
				catTipoBeneficio.setId(catTipoBeneficioList.get(0).getId());
			}else{
				catTipoBeneficio.setId(new Long(0));
			}
		}else{
			catTipoBeneficio.setId(new Long(-1));
		}
		return catTipoBeneficio;
	}
}