/**
 * 
 */
package mx.com.afirme.midas2.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.ExcelRowValidationMapper;
import mx.com.afirme.midas2.util.WriterUtil;

import org.apache.log4j.Logger;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * @author aavacor
 * Extension de ExcelConverter para manejar validaciones, registro y entrega de bitacora de errores en cargas masivas desde plantillas en Excel
 */
public class ExcelLoader extends ExcelConverter {
	
	private Map<String, List<Error>> errorMap = new HashMap<String, List<Error>>();
			
	public ExcelLoader() {
		super();
	}
	
	public ExcelLoader(InputStream is) {
		super(is);
	}
	
	public ExcelLoader(InputStream is, ExcelConfig config) {
		super(is, config);
	}
	
	
	private class Error implements Comparable<Error>{
		
		private Integer rowNumber;
		
		private String columnName;
		
		private String description;
		
		public Error(Integer rowNumber, String columnName, String description) {
			this.rowNumber = rowNumber;
			this.columnName = columnName;
			this.description = description;
		}
				
		public Integer getRowNumber() {
			return rowNumber;
		}
		
		public String getColumnName() {
			return columnName;
		}

		public String getDescription() {
			return description;
		}
		
		@Override
		public int compareTo(Error o) {
			
			if (this.rowNumber != null && o != null && o.getRowNumber() != null) {
				return this.rowNumber - o.getRowNumber();
			}
			
			return 0;
		}
				
	}
	
	private class ExcelRowHashMap<K,V> extends HashMap<K,V> implements Map<K,V> {
				
		private static final long serialVersionUID = 884373326090514446L;
		
		private String columnInProcess;
		
		public V get(Object key) {
						
			this.columnInProcess = (String)key;
						
			return super.get(key);
			
		}
		
		public String getColumnInProcess() {
			
			return this.columnInProcess;
			
		}
		
		public ExcelRowHashMap(Map<? extends K,? extends V> m) {
			
			super(m);
			
		}
		
	}
	
	public void setError (Integer rowNumber, String columnName, String description){
		List<Error> errors = new ArrayListNullAware<ExcelLoader.Error>();
		errors.add(new Error(rowNumber, columnName, description));
		this.errorMap.put(this.getSheetNames().get(0), errors);		
	}
	
	public boolean hasErrors() {
		
		return hasErrors(this.getSheetNames().get(0));
			
	}
	
	public boolean hasErrors(String sheetName) {
		
		return this.errorMap.get(sheetName) != null && this.errorMap.get(sheetName).size() > 0;
			
	}
	
	public InputStream getErrorLog(File originalFile) {
		
		return getErrorLog(originalFile, this.getSheetNames().get(0));
		
	}
		
	public InputStream getErrorLog(File originalFile, String sheetName) {
		
		if (!this.hasErrors(sheetName)) return null;
		
		Workbook errorWorkBook = null;
		Sheet sheet = null;
		List<Error> errors = null;
		Row errorRow = null;
		Cell cell = null;
		CellStyle markedStyle = null;
		int errorColumnIndex;
		int errorRowIndex;
		boolean firstIteration = true;
		int shiftRowIndex = 0;
		InputStream is = null;
		RuntimeException rtEx = null;
		
		try {
			
			is = new FileInputStream(originalFile);
			
			//Se crea una copia del archivo procesado original para hacer el log de errores y se deja solo la hoja en proceso
			
			errorWorkBook = WorkbookFactory.create(is);
			
			for (String name : this.getSheetNames()) {
				
				if (!name.equalsIgnoreCase(sheetName)) {
					
					errorWorkBook.removeSheetAt(errorWorkBook.getSheetIndex(name));
					
				}
							
			}
			
			sheet = errorWorkBook.getSheet(sheetName);
			
			errorColumnIndex = getErrorColumnIndex(sheet);
			
			errors = this.errorMap.get(sheetName);
			
			markedStyle = getMarkedStyle(sheet);
			
			//Los errores se ordenan en reversa (para el shifting)
			
			Collections.sort(errors, Collections.reverseOrder());
			
			//Se agrega en una celda al final de cada renglon los datos del error de cada fila
			
			for (Error error : errors) {
				
				errorRowIndex = error.getRowNumber() -1;
				
				errorRow = sheet.getRow(errorRowIndex);
				
				cell = errorRow.createCell(errorColumnIndex);
				
				cell.setCellValue(getMessage(error));
				
				cell.setCellStyle(markedStyle);
								
				if (errorRowIndex < sheet.getLastRowNum()) {
					
					if (!firstIteration) {
						
						sheet.shiftRows(shiftRowIndex, sheet.getLastRowNum(), (shiftRowIndex - errorRowIndex - 1) * -1);
					
					} else {
						
						sheet.shiftRows(sheet.getLastRowNum(), sheet.getLastRowNum(), (sheet.getLastRowNum() - errorRowIndex - 1) * -1);
						
						sheet.removeRow(sheet.getRow(sheet.getLastRowNum()));
					
					}
					
				}
				
				shiftRowIndex = errorRowIndex;
				
				firstIteration = false;
						
			}
						
			//Se hace un ultimo shift para quitar los registros que no son error que estan entre el encabezado y el primer error
			
			sheet.shiftRows(shiftRowIndex, sheet.getLastRowNum(), (shiftRowIndex - this.getConfig().getColumnNamesRowNumber()) * -1);
			
		} catch (Exception e) {
			
			rtEx = new RuntimeException(e);
			
		} finally {
			
            if (is != null) {
                    try {
                    	
                            is.close();
                            
                    } catch (IOException e) {
                    	
                    		logger.error(e.getMessage());
                            
                    }
                    
            }
            
            if (rtEx != null) throw rtEx;
                      
		}
		
		return WriterUtil.writeWorkBook(errorWorkBook);
		
	}
	
	public InputStream getErrorLogAsText() {
		
		return getErrorLogAsText(this.getSheetNames().get(0));
		
	}
	
	public InputStream getErrorLogAsText(String sheetName) {
		
		if (!this.hasErrors(sheetName)) return null;
		
		List<String> records = new ArrayListNullAware<String>();
		List<Error> errors = this.errorMap.get(sheetName);
		
		for (Error error : errors) {
			
			records.add(getMessage(error));
			
		}
			
		return WriterUtil.writeStringRecords(records);
		
	}
	
	public <T> List<T> parseWithRowMapper(ExcelRowMapper<T> mapper) {
		
		return parseWithRowMapper(this.getSheetNames().get(0), mapper);
		
	}
	
	public <T> List<T> parseWithRowMapper(String sheetName, ExcelRowMapper<T> mapper) {
		
		return this.getMapperEntries(parse(sheetName), sheetName, mapper);
		
	}
	
	public <T> List<T> parseWithRowMapper(InputStream is, ExcelRowMapper<T> mapper) {
		
		return this.getMapperEntries(parse(is), null, mapper);
		
	}
	
	public <T> List<T> parseWithRowValidationMapper(ExcelRowValidationMapper<T> mapper) {
		
		return parseWithRowValidationMapper(this.getSheetNames().get(0), mapper);
		
	}
	
	public <T> List<T> parseWithRowValidationMapper(String sheetName, ExcelRowValidationMapper<T> mapper) {
		
		return this.getMapperEntries(parse(sheetName), sheetName, mapper);
		
	}
	
	public <T> List<T> parseWithRowValidationMapper(InputStream is, ExcelRowValidationMapper<T> mapper) {
		
		return this.getMapperEntries(parse(is), null, mapper);
		
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	private  <T> List<T> getMapperEntries(List<Map<String,String>> entries, String sheetName, ExcelRowMapper<T> mapper) {
		List<T> mapperEntries = new ArrayList<T>();
		List<Error> errors = new ArrayListNullAware<ExcelLoader.Error>();
		ExcelRowHashMap<String, String> validationEntry = null;
		int rowNum = 0;
		
		for (Map<String, String> entry : entries) {
			
			validationEntry = new ExcelRowHashMap(entry);
			
			try {
			
				T mapperEntry = mapper.mapRow(validationEntry, rowNum++);
				
				if (mapper instanceof ExcelRowValidationMapper) {
					
					((ExcelRowValidationMapper)mapper).validateMapperEntry(mapperEntry, mapperEntries);
					
				}
			
				mapperEntries.add(mapperEntry);
			
			} catch (Exception ex) {
				
				errors.add(new Error(rowNum + this.getConfig().getColumnNamesRowNumber(), validationEntry.getColumnInProcess(), ex.getMessage()));
				
			}
			
		}
		
		this.errorMap.put(sheetName, errors);
		
		return mapperEntries;
	}
	
	private int getErrorColumnIndex(Sheet sheet) {
		
		int rowNum = 0;
		int columnIndex = 0;
				
		for (Row row : sheet) {
			
			rowNum++;
			
			if (rowNum == this.getConfig().getColumnNamesRowNumber()) {
				
				columnIndex = row.getLastCellNum();
				break;
			}
			
		}
		
		return columnIndex;
		
		
	}
	
	private String getMessage(Error error) {
		
		return Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, 
				"midas.excel.carga.masiva.error", 
				error.getColumnName(), 
				error.getRowNumber(), 
				error.getDescription());
		
	}
	
	
	private CellStyle getMarkedStyle(Sheet sheet) {
		
		
		CellStyle style = sheet.getWorkbook().createCellStyle();
		
		style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		
		style.setFillForegroundColor(HSSFColor.YELLOW.index);
		
		return style;
		
		
	}
	
	
	private static Logger logger = Logger.getLogger(ExcelLoader.class);
	
}
