package mx.com.afirme.midas2.service.impl.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SeccionReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CoberturaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.PaseRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSipac;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.MovimientoSiniestroDTO;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSipacService;
import mx.com.afirme.midas2.util.StringUtil;


@Stateless
public class RecuperacionSipacServiceImpl extends RecuperacionServiceImpl implements RecuperacionSipacService{
	
	@EJB
	private MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;	
	
	@EJB
	protected EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	
	@EJB
	private PrestadorDeServicioService prestadorDeServicioService;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;	
	
	
	private RecuperacionSipac find(Long estimacionId) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("estimacionId", estimacionId);
		param.put("estatus", EstatusRecuperacion.CANCELADO.toString());
		@SuppressWarnings("unchecked")
		List<RecuperacionSipac> result = 
			entidadService.executeQueryMultipleResult(
					"SELECT model FROM RecuperacionSipac model WHERE model.estimacion.id = :estimacionId AND model.estatus <> :estatus", 
					param);			
		if(result != null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}
	
	@Override
	public RecuperacionSipac generar(BigDecimal valorEstimado,
			EstimacionCoberturaReporteCabina estimacion, 
			String codigoUsuario){
		RecuperacionSipac sipac = this.find(estimacion.getId());
		
		if(sipac==null){
			sipac = new RecuperacionSipac(valorEstimado, estimacion, codigoUsuario);
			sipac.setNumero(entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO));
			
			entidadService.save(sipac);
			
			CoberturaRecuperacion coberturaRecuperacion = new CoberturaRecuperacion();
			coberturaRecuperacion.setCoberturaReporte(estimacion.getCoberturaReporteCabina());
			coberturaRecuperacion.setClaveSubCalculo(estimacion.getCoberturaReporteCabina().getClaveTipoCalculo());     	    
		    coberturaRecuperacion.setRecuperacion(sipac);
		    entidadService.save(coberturaRecuperacion);		
		    
		    PaseRecuperacion paseRecuperacion = new PaseRecuperacion(coberturaRecuperacion, estimacion);	
		    entidadService.save(paseRecuperacion);
		    
		} else if(valorEstimado.compareTo(sipac.getValorEstimado())!=0 && 
				(sipac.getEstatus().equals(Recuperacion.EstatusRecuperacion.PENDIENTE.toString()) 
				|| sipac.getEstatus().equals(Recuperacion.EstatusRecuperacion.REGISTRADO.toString()))){
			sipac.setFechaModificacion(new Date());	
			sipac.setCodigoUsuarioModificacion(codigoUsuario);
			sipac.setValorEstimado(valorEstimado);
			
			entidadService.save(sipac);
		}
		
		Ingreso ingreso = generarIngreso(sipac);
		
		if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.getValue()) 
				&& ingreso.getMonto().compareTo(sipac.getValorEstimado())!=0){
			ingreso.setMonto(sipac.getValorEstimado());
			entidadService.save(ingreso);
		}
		
		return sipac;
		
	}
	
	@Override
	public RecuperacionSipac obtener(Long recuperacionId)
	{
		RecuperacionSipac recuperacion = entidadService.findByProperty(
				RecuperacionSipac.class, "id",recuperacionId).get(0);
		EstimacionCoberturaReporteCabina estimacion = recuperacion.getEstimacion();	
		
		CoberturaReporteCabina coberturaReporte =  estimacion.getCoberturaReporteCabina();
		IncisoReporteCabina incisoAfectado = coberturaReporte.getIncisoReporteCabina();
		AutoIncisoReporteCabina autoIncisoAfectado = incisoAfectado.getAutoIncisoReporteCabina();
		SeccionReporteCabina seccionReporte = incisoAfectado.getSeccionReporteCabina();
		ReporteCabina reporte = seccionReporte.getReporteCabina();
		
	    recuperacion.setMarcaId(autoIncisoAfectado.getMarcaId());
	    recuperacion.setEstiloId(autoIncisoAfectado.getEstiloId());
	    recuperacion.setPolizaCia(autoIncisoAfectado.getPolizaCia());
	    recuperacion.setIncisoCia(autoIncisoAfectado.getIncisoCia());
	    recuperacion.setModeloVehiculo(autoIncisoAfectado.getModeloVehiculo());
	    recuperacion.setSiniestroCia(autoIncisoAfectado.getSiniestroCia());
	    	    
	    PrestadorServicio ciaSeguros = prestadorDeServicioService.buscarPrestador(
	    		Integer.parseInt(autoIncisoAfectado.getCiaSeguros()));
	    
	    recuperacion.setCiaSeguros(ciaSeguros.getNombrePersona());
	    
	    String poliza = reporte.getPoliza().getNumeroPolizaFormateada();
	    
	    recuperacion.setNumPoliza(poliza);
	    

		if(recuperacion.getMarcaId() != null){
			recuperacion.setDescripcionMarca(
			marcaVehiculoFacadeRemote.findById(recuperacion.getMarcaId().doubleValue())
			.getDescripcionMarcaVehiculo());			
		}
		   
		if(!StringUtil.isEmpty(recuperacion.getEstiloId())) {
			String[] estiloId = null;
			estiloId = recuperacion.getEstiloId().split("_");
			EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(estiloId[0], estiloId[1],
						BigDecimal.valueOf(Long.valueOf(estiloId[2]))); 
			EstiloVehiculoDTO estilo = estiloVehiculoFacadeRemote.findById(estiloVehiculoId);

			recuperacion.setDescripcionEstilo(estilo.getDescripcionEstilo());
		}
		
		LugarSiniestroMidas lugarOcurrido = reporte.getLugarOcurrido();
		if(lugarOcurrido!=null){
			recuperacion.setLugarSiniestro(lugarOcurrido.toString());
		}
		
		recuperacion.setFechaSiniestro(reporte.getFechaOcurrido());
		
		TerceroRCVehiculo vehTercero = this.getDatosTercero(incisoAfectado.getId());
		if(vehTercero!=null){			
			recuperacion.setVehiculoTercero(vehTercero.getDescripcionVehiculo());
		}
		
		MovimientoSiniestroDTO filtroMovimientos = new MovimientoSiniestroDTO();
		filtroMovimientos.setIdToReporte(reporte.getId());
		filtroMovimientos.setIdCoberturaReporte(coberturaReporte.getId());		
		filtroMovimientos.setTipoDocumento(MovimientoCoberturaSiniestro.TipoDocumentoMovimiento.ESTIMACION.toString());
		recuperacion.setTotalEstimado(movimientoSiniestroService.obtenerImporteMovimientos(filtroMovimientos));
				
		
		return recuperacion;
	}
	
	/**
	 * Obtiene el Vehiculo Tercero Relacionado a la Recuperacion Sipac
	 * @param incisoReporteCabinaId
	 * @return
	 */
	private TerceroRCVehiculo getDatosTercero(Long incisoReporteCabinaId) {
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("incisoReporteCabinaId", incisoReporteCabinaId);
		param.put("claveTipoCalculo", CoberturaReporteCabina.ClaveTipoCalculo.RESPONSABILIDAD_CIVIL.toString());
		param.put("claveSubTipoCalculo", EstimacionCoberturaReporteCabina.TipoEstimacion.RC_VEHICULO.toString());
		param.put("tipoPase", EstimacionCoberturaReporteCabina.TipoPaseAtencion.SOLO_REG_SIPAC.toString());
		@SuppressWarnings("unchecked")
		List<TerceroRCVehiculo> result = 
			entidadService.executeQueryMultipleResult(
					"SELECT pase FROM CoberturaReporteCabina coberturas"
					+ " JOIN EstimacionCoberturaReporteCabina estimacion ON (estimacion.coberturaReporteCabina.id = coberturas.id) "
					+ " JOIN TerceroRCVehiculo pase ON (pase.id = estimacion.id) "
					+ " WHERE coberturas.incisoReporteCabina.id = :incisoReporteCabinaId "
					+ " AND coberturas.claveTipoCalculo = :claveTipoCalculo "
					+ " AND estimacion.cveSubCalculo = :claveSubTipoCalculo "
					+ " AND estimacion.tipoPaseAtencion = :tipoPase ", 
					param);			
		if(result != null && !result.isEmpty()){
			return result.get(0);
		}
		return null;
	}
	
}
