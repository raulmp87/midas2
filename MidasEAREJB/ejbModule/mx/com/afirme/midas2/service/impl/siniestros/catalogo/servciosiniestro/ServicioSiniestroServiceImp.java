package mx.com.afirme.midas2.service.impl.siniestros.catalogo.servciosiniestro;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.catalogo.ServicioSiniestroDao;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.DatoContactoMidas;
import mx.com.afirme.midas2.domain.personadireccion.DireccionMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas.TIPO_DOMICILIO;
import mx.com.afirme.midas2.domain.personadireccion.PersonaFisicaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMoralMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestroUnidad;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador.TipoDisponibilidad;
import mx.com.afirme.midas2.dto.siniestros.catalogos.ServicioSiniestrosDTO;
import mx.com.afirme.midas2.service.impl.siniestros.catalogo.CatalogoSiniestroServiceImpl;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.AjustadorEstatusService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService;
import mx.com.afirme.midas2.util.StringUtil;

@Stateless
public class ServicioSiniestroServiceImp extends CatalogoSiniestroServiceImpl implements ServicioSiniestroService {

	private  static final int PRIMER_ELEMENTO = 0;
	private static final String PERSONA_FISICA = "PF";
	private static final String PERSONA_MORAL = "PM";
	private static final String DEFAULT_CURP = "CURP";
	
	private  Usuario 			       usuario;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private  UsuarioService           usuarioService;
	
	@EJB
	private  DireccionMidasService    direccionMidasService;
	
	@EJB
	private  ServicioSiniestroDao 	  servicioSiniestroDao;
	
	@EJB
	private   AjustadorEstatusService  ajustadorEstatusService;
	
	@Resource
	private Validator validator;


	@Override
	public void salvarServicioSiniestro(ServicioSiniestrosDTO servicioSiniestroDTO, Short tipoServicio) {
		
		Set<ConstraintViolation<ServicioSiniestrosDTO>> constraintViolations = validator.validate( servicioSiniestroDTO );
		int prestadorServicioId = 0;
		
		if (constraintViolations.size() > 0) {
			
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
			
		}else{

			usuario							  	  = usuarioService.getUsuarioActual();
			DatoContactoMidas datosContacto       = new DatoContactoMidas();
			ServicioSiniestro servicioSiniestro   = new ServicioSiniestro();
			Oficina oficina                       = new Oficina();
			PrestadorServicio prestadorServicio   = new PrestadorServicio();
			
			PersonaMidas personaMidas = new PersonaMidas();
			if(servicioSiniestroDTO.getTipoPersona().equals(PERSONA_FISICA)){
				// # LLENAR OBJETO PARA PERSONA_FISICA
				PersonaFisicaMidas pf = new PersonaFisicaMidas(
						servicioSiniestroDTO.getNombrePersona(),
						servicioSiniestroDTO.getApellidoPaterno(),
						servicioSiniestroDTO.getApellidoMaterno(),
						DEFAULT_CURP,
						new Date(),
						servicioSiniestroDTO.getRfc(),
						"HOMO",
						null,
						usuario.getNombreUsuario()
				);
				
				// Modificar para llenar objeto persona moral
				
				pf.setNombre(pf.getNombreCompleto());
				personaMidas = pf;
			}else{
				if(servicioSiniestroDTO.getTipoPersona().equals(PERSONA_MORAL)){
					PersonaMoralMidas pm = new PersonaMoralMidas();
					pm.setNombre(servicioSiniestroDTO.getNombreComercial());
					pm.setNombreRazonSocial(servicioSiniestroDTO.getNombreDeLaEmpresa());
					pm.setRepresentanteLegal(servicioSiniestroDTO.getAdministrador());
					pm.setFechaConstruccion(Calendar.getInstance().getTime());
					personaMidas = pm;
				}
			}
			
			// # LLENAR OBJETO DATOS CONTACTO
			datosContacto.setCodigoUsuarioCreacion( usuario.getNombreUsuario() );
			datosContacto.setCodigoUsuarioModificacion( usuario.getNombreUsuario() );
			datosContacto.setCorreoPrincipal      ( servicioSiniestroDTO.getCorreoPrincipal());
			datosContacto.setTelCasaLada          ( servicioSiniestroDTO.getTelCasaLada() );
			datosContacto.setTelCasa              ( servicioSiniestroDTO.getTelCasa());
			datosContacto.setTelOficinaLada       ( servicioSiniestroDTO.getTelCasaLada2() );
			datosContacto.setTelOficina           ( servicioSiniestroDTO.getTelCasa2() );
			datosContacto.setTelOtro1Lada		  ( servicioSiniestroDTO.getTelOtro1Lada());
			datosContacto.setTelOtro1		      ( servicioSiniestroDTO.getTelOtro1() );
			datosContacto.setTelOtro2Lada		  ( servicioSiniestroDTO.getTelOtro2Lada() );
			datosContacto.setTelOtro2			  ( servicioSiniestroDTO.getTelOtro2() );
			datosContacto.setTelCelularLada		  ( servicioSiniestroDTO.getTelCelularLada() );
			datosContacto.setTelCelular			  ( servicioSiniestroDTO.getTelCelular() );

			// # CREAR OBJETO PARA DIRECCION
			ColoniaMidas colMidas = this.entidadService.findById(ColoniaMidas.class, servicioSiniestroDTO.getIdColoniaName() );
			DireccionMidas direccionMidas = 
										new DireccionMidas( 
															servicioSiniestroDTO.getCalleName(), 
															servicioSiniestroDTO.getNumeroName(),
															colMidas, 
															servicioSiniestroDTO.getNuevaColoniaName(),
															Integer.parseInt( servicioSiniestroDTO.getCpName() ) ,
															usuario.getNombreUsuario() 
										);
			
			direccionMidas.setNumInterior( servicioSiniestroDTO.getNumeroIntName() );
			direccionMidas.setReferencia ( servicioSiniestroDTO.getReferencia() );
		
			personaMidas.setCveNacionalidad(servicioSiniestroDTO.getIdPaisName());
			personaMidas.setRfc(servicioSiniestroDTO.getRfc());
			
			String[] rfc = getRFC(servicioSiniestroDTO.getRfc());
			
			if(servicioSiniestroDTO.getRfc() != null){
				personaMidas.setCodigoRfc(rfc[0]);
				personaMidas.setFechaRfc(rfc[1]);
				personaMidas.setHomoclaveRfc(rfc[2]);
			}
			
			personaMidas.agregarDireccion(direccionMidas,TIPO_DOMICILIO.PERSONAL, usuario.getNombreUsuario());
			
			// # SET VALORES A PERSONA FISICA, DESPUES SE HACER UPPER CASTING
			personaMidas.setContacto		         ( datosContacto );
			personaMidas.setTipoPersona			 ( servicioSiniestroDTO.getTipoPersona() );
			personaMidas.setFechaCreacion          ( new Date() );
			personaMidas.setCodigoUsuarioCreacion  ( usuario.getNombreUsuario() );
			personaMidas.setFechaModificacion		 (new Date());
			personaMidas.setCodigoUsuarioModificacion( usuario.getNombreUsuario() );
			
			// # SET OFICINA
			oficina.setId( Long.valueOf(servicioSiniestroDTO.getOficinaId()) );
			
			// # SET PRESTADOR SERVICIO
			if ( servicioSiniestroDTO.getPrestadorId() != null ){
				prestadorServicioId = Integer.parseInt( servicioSiniestroDTO.getPrestadorId().toString() );
				prestadorServicio.setId                ( prestadorServicioId );
				servicioSiniestro.setPrestadorServicio ( prestadorServicio );
			}
			
			// # SET SERVICIO SINIESTRO
			servicioSiniestro.setPersonaMidas		     ( personaMidas );
			servicioSiniestro.setOficina			     ( oficina );
			servicioSiniestro.setServicioSiniestroId     ( Long.valueOf( tipoServicio.toString()) );
			servicioSiniestro.setAmbitoPrestadorServicio ( Short.valueOf( servicioSiniestroDTO.getAmbitoPrestadorServicio() ) );
			servicioSiniestro.setEstatus			     ( servicioSiniestroDTO.getEstatus() );
			servicioSiniestro.setCertificacion		     ( servicioSiniestroDTO.getCertificacion());
			servicioSiniestro.setClaveUsuario			(servicioSiniestroDTO.getClaveUsuario());
			servicioSiniestro.setFechaCreacion           ( new Date() );
			servicioSiniestro.setCodigoUsuarioCreacion   ( usuario.getNombreUsuario());
			
			if (debeActualizarUnidad(servicioSiniestroDTO)) {
				final ServicioSiniestroUnidad unidad = new ServicioSiniestroUnidad();
				unidad.setCodigo(servicioSiniestroDTO.getCodigoUnidad());
				unidad.setPlaca(servicioSiniestroDTO.getPlacas());
				unidad.setDescripcion(servicioSiniestroDTO.getDescripcionUnidad());
				servicioSiniestro.setServicioSiniestroUnidad(unidad);
			}
				
			// # PERSISTIR PERSONA MIDAS
			Long id = this.salvarObtenerId(servicioSiniestro);
			servicioSiniestroDTO.setId(id);
		}
	
	}
	
	
	@Override
	public void actualizaServicioSiniestro(ServicioSiniestrosDTO servicioSiniestroDTO, Short tipoServicio) {
		Set<ConstraintViolation<ServicioSiniestrosDTO>> constraintViolations = validator.validate( servicioSiniestroDTO );
		
		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
			
		}else{
			usuario = usuarioService.getUsuarioActual();
			
			// # OBTENER OBJETO SERVICIOSINIESTRO, DE ESTE SE OBTENDRAN LOS ID PARA SACAR LA INFORMACION DE PERSONA,DATOSCONTACTO,COLONIA Y DIRECCION
			ServicioSiniestro servicioSiniestro = this.entidadService.findById(ServicioSiniestro.class, servicioSiniestroDTO.getId());
			
			servicioSiniestro.setFechaModificacion		  ( new Date() );
			servicioSiniestro.setCodigoUsuarioModificacion( usuario.getNombreUsuario() );
			
			if ( servicioSiniestro!= null ){
				DireccionMidas     direccionMidas    = null;
				DatoContactoMidas  datosContactoMidas  = null;
				PersonaMidas personaMidas = servicioSiniestro.getPersonaMidas();
				servicioSiniestroDTO.setTipoPersona(servicioSiniestro.getPersonaMidas().getTipoPersona());
				if(servicioSiniestroDTO.getTipoPersona().equals(PERSONA_FISICA)){
					personaMidas = this.entidadService.findById(PersonaFisicaMidas.class, servicioSiniestro.getPersonaMidas().getId() );			
					((PersonaFisicaMidas)personaMidas).setNombrePersona( servicioSiniestroDTO.getNombrePersona() );
					((PersonaFisicaMidas)personaMidas).setApellidoPaterno			( servicioSiniestroDTO.getApellidoPaterno() );
					((PersonaFisicaMidas)personaMidas).setApellidoMaterno			( servicioSiniestroDTO.getApellidoMaterno() );
					personaMidas.setNombre(((PersonaFisicaMidas)personaMidas).getNombreCompleto());
				}else{
					personaMidas = this.entidadService.findById(PersonaMoralMidas.class, servicioSiniestro.getPersonaMidas().getId() );
					((PersonaMoralMidas)personaMidas).setNombre(servicioSiniestroDTO.getNombreComercial());
					((PersonaMoralMidas)personaMidas).setRepresentanteLegal(servicioSiniestroDTO.getAdministrador());
					((PersonaMoralMidas)personaMidas).setNombreRazonSocial(servicioSiniestroDTO.getNombreDeLaEmpresa());
					personaMidas.setNombre(servicioSiniestroDTO.getNombreDeLaEmpresa());
				}
				personaMidas.setTipoPersona(servicioSiniestroDTO.getTipoPersona());
				personaMidas.setRfc(servicioSiniestroDTO.getRfc());
				String[] rfc = getRFC(servicioSiniestroDTO.getRfc());
				
				if(servicioSiniestroDTO.getRfc() != null){
					personaMidas.setCodigoRfc(rfc[0]);
					personaMidas.setFechaRfc(rfc[1]);
					personaMidas.setHomoclaveRfc(rfc[2]);
				}
						
			
				if(personaMidas.getPersonaDireccion() != null && personaMidas.getPersonaDireccion().size() > 0){
					direccionMidas = personaMidas.getPersonaDireccion().get(0).getDireccion();
				}else{
					direccionMidas = new DireccionMidas();
				}
				
				if(personaMidas.getContacto() != null){
					datosContactoMidas = this.entidadService.findById(DatoContactoMidas.class, personaMidas.getContacto().getId() );
				}else{
					datosContactoMidas = new DatoContactoMidas();
				}
				ColoniaMidas       colMidas           = this.entidadService.findById(ColoniaMidas.class, servicioSiniestroDTO.getIdColoniaName() );
				PrestadorServicio prestadorServicio   = new PrestadorServicio();
				
				
				// # LLENAR OBJETO DATOS CONTACTO
				datosContactoMidas.setCorreoPrincipal          ( servicioSiniestroDTO.getCorreoPrincipal());
				datosContactoMidas.setTelCasaLada              ( servicioSiniestroDTO.getTelCasaLada() );
				datosContactoMidas.setTelCasa                  ( servicioSiniestroDTO.getTelCasa());
				datosContactoMidas.setTelOficinaLada           ( servicioSiniestroDTO.getTelCasaLada2() );
				datosContactoMidas.setTelOficina               ( servicioSiniestroDTO.getTelCasa2() );
				datosContactoMidas.setTelOtro1Lada		       ( servicioSiniestroDTO.getTelOtro1Lada());
				datosContactoMidas.setTelOtro1		           ( servicioSiniestroDTO.getTelOtro1() );
				datosContactoMidas.setTelOtro2Lada		       ( servicioSiniestroDTO.getTelOtro2Lada() );
				datosContactoMidas.setTelOtro2			       ( servicioSiniestroDTO.getTelOtro2() );
				datosContactoMidas.setTelCelularLada	       ( servicioSiniestroDTO.getTelCelularLada() );
				datosContactoMidas.setTelCelular		       ( servicioSiniestroDTO.getTelCelular() );
				datosContactoMidas.setFechaModificacion        ( new Date() );				
				datosContactoMidas.setCodigoUsuarioModificacion( usuario.getNombreUsuario());
				if(StringUtil.isEmpty(datosContactoMidas.getCodigoUsuarioCreacion())){
					datosContactoMidas.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
					datosContactoMidas.setFechaCreacion(new Date());
				}
					
				
				// # LLENAR DATOS DIRECCION
			
				direccionMidas.setCalle					    ( servicioSiniestroDTO.getCalleName());
				direccionMidas.setColonia				    ( colMidas);
				direccionMidas.setNumExterior			    ( servicioSiniestroDTO.getNumeroName() );
				direccionMidas.setNumInterior			    ( servicioSiniestroDTO.getNumeroIntName() );
				direccionMidas.setReferencia				( servicioSiniestroDTO.getReferencia() );
				direccionMidas.setCodigoUsuarioModificacion ( usuario.getNombreUsuario());
				direccionMidas.setFechaModificacion		    ( new Date());
				if(StringUtil.isEmpty(direccionMidas.getCodigoUsuarioCreacion())){
					direccionMidas.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
					direccionMidas.setFechaCreacion(new Date());
				}
				
				if( servicioSiniestroDTO.getIdColoniaCheckName() != null ){
					direccionMidas.setOtraColonia( servicioSiniestroDTO.getNuevaColoniaName() );
				}
				personaMidas.agregarDireccion( direccionMidas,TIPO_DOMICILIO.PERSONAL, usuario.getNombreUsuario());
					
				// # VALIDA CAMBIO DE ESTATUS
				if( servicioSiniestroDTO.getEstatusBase() != servicioSiniestroDTO.getEstatus() &  servicioSiniestroDTO.getEstatusBase()!=null ){
					if(servicioSiniestroDTO.getEstatus() == 1){
						servicioSiniestro.setFechaCambioEstatus( null );
					}else{
						servicioSiniestro.setFechaCambioEstatus( new Date() );
					}
				}
				
				// # AGREGA NUEVA OFICINA
				Oficina oficina = new Oficina();
				oficina.setId( Long.valueOf(servicioSiniestroDTO.getOficinaId()) );
				
				// # SERVICIO SINIESTRO
				servicioSiniestro.setEstatus         		 ( servicioSiniestroDTO.getEstatus() );
				servicioSiniestro.setCertificacion   		 ( servicioSiniestroDTO.getCertificacion());
				servicioSiniestro.setAmbitoPrestadorServicio ( servicioSiniestroDTO.getAmbitoPrestadorServicio() );
				servicioSiniestro.setClaveUsuario			 (servicioSiniestroDTO.getClaveUsuario());
				// # ACTUALIZA CERTIFICACION SI ES AJUSTADOR
				if ( tipoServicio == 1 ){
					servicioSiniestro.setCertificacion( servicioSiniestroDTO.getCertificacion() );
				}
				
				if ( servicioSiniestroDTO.getAmbitoPrestadorServicio() == 2 ){
					// # AJUSTADOR DE SERVICIO
					if ( servicioSiniestroDTO.getPrestadorId() != null ){
						prestadorServicio.setId( Integer.parseInt( servicioSiniestroDTO.getPrestadorId().toString() ) );
						
						servicioSiniestro.setPrestadorServicio ( prestadorServicio );
					}
				}else{
					servicioSiniestro.setPrestadorServicio ( null );
				}
				
				
				// # VINCULAR ENTIDADES A SERVICIO SINIESTRO
				personaMidas.setContacto    ( datosContactoMidas );
				servicioSiniestro.setPersonaMidas ( personaMidas );
				servicioSiniestro.setOficina	  ( oficina );
				Long id = this.salvarObtenerId(servicioSiniestro);
				servicioSiniestroDTO.setId(id);
				if (debeActualizarUnidad(servicioSiniestroDTO)) {
					final ServicioSiniestroUnidad unidad = servicioSiniestro
							.getServicioSiniestroUnidad() == null ? new ServicioSiniestroUnidad()
							: servicioSiniestro.getServicioSiniestroUnidad();
					unidad.setCodigo(servicioSiniestroDTO.getCodigoUnidad());
					unidad.setPlaca(servicioSiniestroDTO.getPlacas());
					unidad.setTipo(ServicioSiniestroUnidad.TIPO_DEFAULT);
					unidad.setDescripcion(servicioSiniestroDTO.getDescripcionUnidad());
					servicioSiniestro.setServicioSiniestroUnidad(unidad);
					unidad.setServicioSiniestro(servicioSiniestro);
					this.salvarObtenerId(servicioSiniestro);
				}
				

			}
		
		}
	
	}
	
	/**
	 * Indica si se debe actualizar la unidad de un servicio siniestro
	 * @return
	 */
	private boolean debeActualizarUnidad(ServicioSiniestrosDTO servicio) {
		return StringUtils.isNotBlank(servicio.getPlacas()) ||
				StringUtils.isNotBlank(servicio.getCodigoUnidad()) || StringUtils.isNotBlank(servicio.getDescripcionUnidad());
	}


	@Override
	public ServicioSiniestrosDTO obtenerDatosSiniestro(Long idSiniestro, Long idPersonaMidas, Short tipoOperacion ) {

		 // # OBJETO AL CUAL SE MAPEAN LOS DATOS DE LA INTERFAZ
		ServicioSiniestrosDTO servicioSiniestroDTO = new ServicioSiniestrosDTO();
		

		// # OBTENER DATOS DE SERVICIO SINIESTRO
		ServicioSiniestro servicioSiniestro = this.entidadService.findById(ServicioSiniestro.class, idSiniestro);
		if( servicioSiniestro != null ){
			DireccionMidas     direccionMidas   = null;
			DatoContactoMidas  datosContactoMidas = null;
			PersonaMidas personaMidas = this.entidadService.findById(PersonaMidas.class, idPersonaMidas );
			if(personaMidas.getPersonaDireccion() != null && personaMidas.getPersonaDireccion().size() > 0){
				if(personaMidas.getPersonaDireccion().size() > 0){
					direccionMidas = this.entidadService.findById(DireccionMidas.class, personaMidas.getPersonaDireccion().get(0).getDireccion().getId() );
				}
			}
	
			if(personaMidas.getContacto() != null){
				datosContactoMidas = this.entidadService.findById(DatoContactoMidas.class, personaMidas.getContacto().getId() );
			}
			
			servicioSiniestroDTO.setId              ( servicioSiniestro.getId() );
			servicioSiniestroDTO.setIdPersonaMidas  ( servicioSiniestro.getPersonaMidas().getId() );
			servicioSiniestroDTO.setEstatus         ( servicioSiniestro.getEstatus() );
			servicioSiniestroDTO.setOficinaId       ( servicioSiniestro.getOficina().getId() );
			servicioSiniestroDTO.setEstatusBase	    ( servicioSiniestro.getEstatus() );
			if( servicioSiniestro.getPrestadorServicio() != null ){
				servicioSiniestroDTO.setPrestadorId     ( Long.valueOf( servicioSiniestro.getPrestadorServicio().getId().toString() ) );
			}
			servicioSiniestroDTO.setTipoOperacion   ( tipoOperacion );
			servicioSiniestroDTO.setAmbitoPrestadorServicio(servicioSiniestro.getAmbitoPrestadorServicio());

			servicioSiniestroDTO.setFechaActivo     ( servicioSiniestro.getFechaCreacion() );
			servicioSiniestroDTO.setFechaInactivo   ( servicioSiniestro.getFechaModificacion());
			servicioSiniestroDTO.setFechaInactivo   ( servicioSiniestro.getFechaCambioEstatus() );
			if(personaMidas.getTipoPersona().equals(PERSONA_FISICA)){
				servicioSiniestroDTO.setTipoPersona(PERSONA_FISICA);
				servicioSiniestroDTO.setNombrePersona   ( ((PersonaFisicaMidas)personaMidas).getNombrePersona() );
				servicioSiniestroDTO.setApellidoPaterno ( ((PersonaFisicaMidas)personaMidas).getApellidoPaterno() );
				servicioSiniestroDTO.setApellidoMaterno ( ((PersonaFisicaMidas)personaMidas).getApellidoMaterno() );
				servicioSiniestroDTO.setNombreRazon     ( ((PersonaFisicaMidas)personaMidas).getNombrePersona());
			}else{
				servicioSiniestroDTO.setTipoPersona(PERSONA_MORAL);
				servicioSiniestroDTO.setNombreDeLaEmpresa( ((PersonaMoralMidas)personaMidas).getNombreRazonSocial());
				servicioSiniestroDTO.setNombreComercial ( ((PersonaMoralMidas)personaMidas).getNombre() );
				servicioSiniestroDTO.setAdministrador( ((PersonaMoralMidas)personaMidas).getRepresentanteLegal() );
				servicioSiniestroDTO.setNombreRazon(((PersonaMoralMidas)personaMidas).getNombre());
			}
			servicioSiniestroDTO.setCertificacion   ( servicioSiniestro.getCertificacion() );
			servicioSiniestroDTO.setRfc(personaMidas.getRfc());
			servicioSiniestroDTO.setClaveUsuario	( servicioSiniestro.getClaveUsuario()  );
			
			if(datosContactoMidas != null){
				servicioSiniestroDTO.setCorreoPrincipal ( datosContactoMidas.getCorreoPrincipal() );
				servicioSiniestroDTO.setTelCasaLada     ( datosContactoMidas.getTelCasaLada() );
				servicioSiniestroDTO.setTelCasa         ( datosContactoMidas.getTelCasa() );
				servicioSiniestroDTO.setTelCasaLada2    ( datosContactoMidas.getTelOficinaLada() );
				servicioSiniestroDTO.setTelCasa2        ( datosContactoMidas.getTelOficina() );
				servicioSiniestroDTO.setTelCelularLada  ( datosContactoMidas.getTelCelularLada() );
				servicioSiniestroDTO.setTelCelular      ( datosContactoMidas.getTelCelular() );
				servicioSiniestroDTO.setTelOtro1Lada    ( datosContactoMidas.getTelOtro1Lada() );
				servicioSiniestroDTO.setTelOtro1        ( datosContactoMidas.getTelOtro1() );
				servicioSiniestroDTO.setTelOtro2Lada    ( datosContactoMidas.getTelOtro2Lada() );
				servicioSiniestroDTO.setTelOtro2        ( datosContactoMidas.getTelOtro2() );
				servicioSiniestroDTO.setIdDatoContacto  ( datosContactoMidas.getId() );
			}
			
			if(direccionMidas != null){
				// # OBTENER DATOS DE DIRECCION
				Map<String,String> ubicacionPersona = this.procesaDatosDireccion(personaMidas);
				servicioSiniestroDTO.setIdDatosDireccion( direccionMidas.getId() );

				servicioSiniestroDTO.setIdPaisName      ( ubicacionPersona.get("pais") );
				servicioSiniestroDTO.setIdEstadoName    ( ubicacionPersona.get("estado") );
				servicioSiniestroDTO.setIdCiudadName	( ubicacionPersona.get("ciudad") );
				servicioSiniestroDTO.setIdColoniaName   ( ubicacionPersona.get("colonia") );
				servicioSiniestroDTO.setCpName			( ubicacionPersona.get("cp") );
				
				servicioSiniestroDTO.setNuevaColoniaName( direccionMidas.getOtraColonia());
				servicioSiniestroDTO.setCalleName		( direccionMidas.getCalle() );
				servicioSiniestroDTO.setNumeroName		( direccionMidas.getNumExterior() );
				servicioSiniestroDTO.setNumeroIntName   ( direccionMidas.getNumInterior() );
				servicioSiniestroDTO.setReferencia      ( direccionMidas.getReferencia()  );
			}
			if (servicioSiniestro.getServicioSiniestroUnidad() != null ) {
				servicioSiniestroDTO.setPlacas(servicioSiniestro.getServicioSiniestroUnidad().getPlaca());
				servicioSiniestroDTO.setCodigoUnidad(servicioSiniestro.getServicioSiniestroUnidad().getCodigo());
				servicioSiniestroDTO.setDescripcionUnidad(servicioSiniestro.getServicioSiniestroUnidad().getDescripcion());
			}
		}
		
		return servicioSiniestroDTO;
	}
	
	private Map<String,String> procesaDatosDireccion(PersonaMidas personaMidas){
		
		String cp = "", paisId = "", estadoId = "", ciudadId = "", coloniaId = "";
		CiudadMidas ciudad = null;
		Map<String,String> datosUbicacion = new HashMap<String,String>();
		
		if( personaMidas.getPersonaDireccion().get(0).getDireccion().getColonia() != null ){
			ColoniaMidas colonia = entidadService.findById(ColoniaMidas.class, personaMidas.getPersonaDireccion().get(0).getDireccion().getColonia().getId());
			
			if ( colonia != null ){
				
				//ciudad  = direccionMidasService.obtenerCiudadPorCP(colonia.getCodigoPostal());
				ciudad  = direccionMidasService.obtenerCiudadPorColonia(colonia.getId());
				cp        = colonia.getCodigoPostal();
				coloniaId = colonia.getId();
		
			}
			
		}else{
			cp = String.valueOf( personaMidas.getPersonaDireccion().get(0).getDireccion().getIdCodigoPostalOtraColonia() );
			ciudad  = direccionMidasService.obtenerCiudadPorCP( cp );

		}
		
		if ( ciudad != null){
			paisId   = ciudad.getEstado().getPais().getId();
			estadoId = ciudad.getEstado().getId();
			ciudadId = ciudad.getId();
		}
		
		datosUbicacion.put("pais",   paisId );
		datosUbicacion.put("estado", estadoId );
		datosUbicacion.put("ciudad", ciudadId );
		datosUbicacion.put("colonia", coloniaId );
		datosUbicacion.put("cp", 	 cp);
		
		return datosUbicacion;
	}
	
	
	@Override
	public List<ServicioSiniestro> buscarServicioSiniestro(ServicioSiniestroFiltro servicioSiniestroFiltro) {
		
		return servicioSiniestroDao.buscar(servicioSiniestroFiltro);
		
		/*List<ServicioSiniestro> lServicioSiniestro = this.buscar(ServicioSiniestro.class, servicioSiniestroFiltro);
		this.agregaEstaYMunicipioAServicioSiniestro(lServicioSiniestro);
		
		lServicioSiniestro = this.filtraPorEstadoYMunicipio(lServicioSiniestro,servicioSiniestroFiltro);
		return lServicioSiniestro;
		*/
	}
	
	@Override
	public ServicioSiniestro findById(Long id) {
		final ServicioSiniestro result = servicioSiniestroDao.findById(id);
		result.getPersonaMidas().getContacto();
		result.getServicioSiniestroUnidad();
		return result;
	}
	
	
	private List<ServicioSiniestro> filtraPorEstadoYMunicipio(List<ServicioSiniestro> listaAjustadores,ServicioSiniestroFiltro filtro) {
		List<ServicioSiniestro> listaAjustadoresFiltrada = new ArrayList<ServicioSiniestro>(); 
		if(filtro.getEstado()!=null && !filtro.getEstado().equals("")){
			for(ServicioSiniestro ajustador : listaAjustadores){
				CiudadMidas ciudad = findCiudad(ajustador.getPersonaMidas().getPersonaDireccion().get(0).getDireccion());
				if(ciudad != null && filtro.getEstado().equals(ciudad.getEstado().getId())){
					if(filtro.getMunicipio()!=null && !filtro.getMunicipio().equals("")){
						if(ciudad.getId().equals(filtro.getMunicipio())){
							listaAjustadoresFiltrada.add(ajustador);
						}
					}else{
						listaAjustadoresFiltrada.add(ajustador);
					}
				}
				
			}
		}else{
			return listaAjustadores;
		}
		return listaAjustadoresFiltrada;
	}
	
	private void agregaEstaYMunicipioAServicioSiniestro(List<ServicioSiniestro> ajustadores){
		for(ServicioSiniestro ajustador : ajustadores){
			DireccionMidas direccion = this.getPrimeraDireccionMidas(ajustador);
			CiudadMidas ciudad = findCiudad(direccion);
			ajustador.setEstado((ciudad!=null)?ciudad.getEstado().getDescripcion():"");
			ajustador.setMunicipio((ciudad!=null)?ciudad.getDescripcion():"");
		}
	}
	
	private CiudadMidas findCiudad(DireccionMidas direccion){
		String cp = "";
		String cpColonia = direccion != null ? direccion.getCodigoPostalColonia() : null;
		Integer cpOtraColonia = direccion != null ? direccion.getIdCodigoPostalOtraColonia() : null;
		if(cpColonia!=null){
			cp = cpColonia;
		}else if(cpOtraColonia!=null){
			cp = cpOtraColonia.toString();
		}else{
			return null;
		}
		return direccionMidasService.obtenerCiudadPorCP(cp);
	}

	private DireccionMidas getPrimeraDireccionMidas(ServicioSiniestro ajustador) {
		DireccionMidas direccionMidas = null;
		if(ajustador.getPersonaMidas() != null && ajustador.getPersonaMidas().getPersonaDireccion() != null && 
				ajustador.getPersonaMidas().getPersonaDireccion().size() > 0){
			List<PersonaDireccionMidas> personaDireccionList = ajustador.getPersonaMidas()
					.getPersonaDireccion();
			direccionMidas = personaDireccionList.get(PRIMER_ELEMENTO)
					.getDireccion();
		}
		return direccionMidas;
	}
	
	@Override
	public List<ServicioSiniestro> buscarAjustadoresDisponibles(ServicioSiniestroFiltro servicioSiniestroFiltro){
		List<ServicioSiniestro> listaAjustadores = new ArrayList<ServicioSiniestro>();
		if(!servicioSiniestroFiltro.getTipoDisponibilidad().equals("") && !(servicioSiniestroFiltro.getTipoDisponibilidad() == null)){
			if(servicioSiniestroFiltro.getTipoDisponibilidad().equals(TipoDisponibilidad.Normal.getCodigo())){
				listaAjustadores.addAll(ajustadorEstatusService.obtenerDisponibilidadAjustador( servicioSiniestroFiltro.getOficinaId() , TipoDisponibilidad.Normal));
			}else if(servicioSiniestroFiltro.getTipoDisponibilidad().equals(TipoDisponibilidad.Guardia.getCodigo())){
				listaAjustadores.addAll(ajustadorEstatusService.obtenerDisponibilidadAjustador( servicioSiniestroFiltro.getOficinaId() , TipoDisponibilidad.Guardia));
			}else if(servicioSiniestroFiltro.getTipoDisponibilidad().equals(TipoDisponibilidad.Apoyo.getCodigo())){
				listaAjustadores.addAll(ajustadorEstatusService.obtenerDisponibilidadAjustador( servicioSiniestroFiltro.getOficinaId() , TipoDisponibilidad.Apoyo));

			}
		}else{
			listaAjustadores.addAll(ajustadorEstatusService.obtenerDisponibilidadAjustador( servicioSiniestroFiltro.getOficinaId() , TipoDisponibilidad.Normal));
			listaAjustadores.addAll(ajustadorEstatusService.obtenerDisponibilidadAjustador( servicioSiniestroFiltro.getOficinaId() , TipoDisponibilidad.Guardia));
			listaAjustadores.addAll(ajustadorEstatusService.obtenerDisponibilidadAjustador( servicioSiniestroFiltro.getOficinaId() , TipoDisponibilidad.Apoyo));
		}

		//List<ServicioSiniestro> listaFiltrada = buscarServicioSiniestro( servicioSiniestroFiltro );

		//filtro de estatus de Asignación
		if(!servicioSiniestroFiltro.getEstatusAsignacion().equals("")){
			for(ServicioSiniestro element : listaAjustadores){
				if(!element.getEstatusAsignacion().equals(servicioSiniestroFiltro.getEstatusAsignacion())){
					listaAjustadores.remove(element);	
				}
			}
		}
			
		//listaFiltrada.retainAll(listaAjustadores);
		
		return listaAjustadores;
	}
		
	
	private String[] getRFC(String str){
		String[] rfc = null;
		if (str.length() >= 12){
			int longFecha = 6;
			int longHomoclave = 3;
			int longCodigo = str.length() - longFecha - longHomoclave;
			rfc = new String[3];;
			rfc[0] = str.substring(0, longCodigo);
			rfc[1] = str.substring(longCodigo, longCodigo + longFecha);
			rfc[2] = str.substring(longCodigo + longFecha, longCodigo + longFecha + longHomoclave);
		}

		return rfc;
	}
	

}