package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.ws;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.afirme.commons.exception.CommonException;
import com.afirme.commons.util.BeanUtils;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaResponse;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.Inciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.banco.BancoMidasService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.ws.EmitirEndosoFlotillaService;

@Stateless
public class EmitirEndosoFlotillaServiceImpl implements EmitirEndosoFlotillaService {
	
	private static final Logger LOG = Logger.getLogger(EndosoFlotillaServiceImpl.class);
	
	public static final String VALOR_VISA = "VISA";
	public static final String VALOR_MASTERCARD = "MASTERD CARD";
	public static final String VALOR_TC= "TARJETA DE CREDITO";
	public static final String VALOR_DOMICILIACION= "COBRANZA DOMICILIADA";
	public static final String VALOR_EFECTIVO= "EFECTIVO";
	public static final String VALOR_CUENTA_AFIRME= "CUENTA AFIRME (CREDITO O DEBITO)";
	
	private static final String MEDIO_PAGO_DOMICILIACION = "8";
	private static final String MEDIO_PAGO_EFECTIVO = "15";
	private static final String MEDIO_CUENTA_AFIRME= "1386";
		
	private static final String TIPO_CUENTA_DEBITO = "DEBITO";
	private static final String TIPO_CUENTA_CUENTA_CLABE = "CUENTA CLABE";
	private static final String TIPO_CUENTA_CUENTA_BANCARIA = "CUENTA BANCARIA";

	
	final SimpleDateFormat creditCardFormat = new SimpleDateFormat("MM/yy");
	
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private EndosoService endosoService;
	@EJB
	private IncisoViewService incisoService;
	@EJB
	private BancoMidasService bancoMidasService;
	@EJB
	private ClienteFacadeRemote clienteFacadeRemote;
	@EJB
	private ListadoService listadoService;
	@EJB
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	@EJB
	private SeguroObligatorioService seguroObligatorioService;
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EmitirEndosoFlotillaResponse terminarCotizacion(EmitirEndosoFlotillaRequest request) {
		final EmitirEndosoFlotillaResponse response = new EmitirEndosoFlotillaResponse();		
		BitemporalCotizacion cotizacion = endosoService.prepareCotizacionEndoso(request.getIdCotizacion(), TimeUtils.getDateTime(request.getFechaInicioVigencia()));
		TerminarCotizacionDTO terminarCotizacionDTO = endosoService.terminarCotizacionEndoso(cotizacion,  TimeUtils.getDateTime(request.getFechaInicioVigencia()));
		if (terminarCotizacionDTO != null) {
			response.setExepcionesList(terminarCotizacionDTO.getExcepcionesList());
			response.setMensaje(terminarCotizacionDTO.getMensajeError());				
			response.setEstatus(ERROR);			
		}		
		response.setEstatus(SUCCESS);
		return response;
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EmitirEndosoFlotillaResponse complementarIncisos(EmitirEndosoFlotillaRequest request) {
		final EmitirEndosoFlotillaResponse response = new EmitirEndosoFlotillaResponse();
		BitemporalCotizacion cotizacion = endosoService.prepareCotizacionEndoso(request.getIdCotizacion(), TimeUtils.getDateTime(request.getFechaInicioVigencia()));
		List<BitemporalAutoInciso> bitemporalAutoIncisos = obtenerAutoIncisosEndoso(cotizacion.getContinuity().getBusinessKey().longValue(), TimeUtils.getDateTime(request.getFechaInicioVigencia()));
		for (final AutoInciso autoInciso : request.getLstInciso()) {
			BitemporalAutoInciso bitAutoInciso = (BitemporalAutoInciso)CollectionUtils.find(bitemporalAutoIncisos, new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					BitemporalAutoInciso elemento = (BitemporalAutoInciso) arg0;
					if (autoInciso.getNumeroInciso().equals(elemento.getContinuity().getParentContinuity().getNumero())) {
						return true;
					}
					return false;
				}
			});
			actualizarDatosComplementarios(bitAutoInciso.getContinuity().getId(), autoInciso, BigDecimal.valueOf(cotizacion.getContinuity().getBusinessKey()));
		}
		response.setEstatus(SUCCESS);
		return response;
	}


	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EmitirEndosoFlotillaResponse emitir(EmitirEndosoFlotillaRequest request) {
		final EmitirEndosoFlotillaResponse response = new EmitirEndosoFlotillaResponse();		
		BitemporalCotizacion cotizacion = endosoService.prepareCotizacionEndoso(request.getIdCotizacion(), TimeUtils.getDateTime(request.getFechaInicioVigencia()));
		TerminarCotizacionDTO terminarCotizacionDTO = endosoService.validarEmisionCotizacion(cotizacion, TimeUtils.getDateTime(request.getFechaInicioVigencia()));
		if (terminarCotizacionDTO != null) {
			response.setExepcionesList(terminarCotizacionDTO.getExcepcionesList());
			response.setMensaje(terminarCotizacionDTO.getMensajeError());				
			response.setEstatus(ERROR);
			return response;
		}
		
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(), 
				TimeUtils.getDateTime(request.getFechaInicioVigencia()),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		if (endoso == null) {
			response.setEstatus(ERROR);
		}
		if (endoso != null) {
			if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_ALTA_INCISO)){
				creaPolizaAnexaEndosoAltaInciso(endoso.getPolizaDTO(), endoso.getId().getNumeroEndoso().shortValue());
			}
			response.setEstatus(SUCCESS);
			response.setNumeroEndoso(endoso.getId().getNumeroEndoso());
		}
		return response;
	}
	
	public void creaPolizaAnexaEndosoAltaInciso(PolizaDTO poliza, Short numeroEndoso){
		try{
			if(poliza.getClaveAnexa() == null || !poliza.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
				PolizaDTO polizaAnexa = seguroObligatorioService.obtienePolizaAnexa(poliza.getIdToPoliza());
				if(polizaAnexa != null){						
					seguroObligatorioService.emiteEndosoAltaSO(poliza.getIdToPoliza(), numeroEndoso);	
				}
			}
		} catch(Exception e){
			LOG.error("-- creaPolizaAnexaEndosoAltaInciso()", e);
		}				
	}

	public List<LogErroresCargaMasivaDTO> actualizarDatosComplementarios(Long autoIncisoContinuityId, AutoInciso autoInciso, BigDecimal idCotizacion) {

		List<LogErroresCargaMasivaDTO> result = new ArrayList<LogErroresCargaMasivaDTO>();
		DateTime fechaInicioVigencia = obtenerFechaInicioVigencia(idCotizacion);
		BitemporalAutoInciso bitAutoInciso = entidadBitemporalService.getInProcessByKey(AutoIncisoContinuity.class, autoIncisoContinuityId, fechaInicioVigencia);
		bitAutoInciso.getEmbedded().setNombreAsegurado(autoInciso.getNombreAsegurado());
		bitAutoInciso.getEmbedded().setObservacionesinciso(autoInciso.getObservacionesinciso());
		bitAutoInciso.getEmbedded().setNumeroSerie(autoInciso.getNumeroSerie());
		bitAutoInciso.getEmbedded().setNumeroMotor(autoInciso.getNumeroMotor());
		bitAutoInciso.getEmbedded().setPlaca(autoInciso.getPlaca());
		bitAutoInciso.getEmbedded().setNombreConductor(autoInciso.getNombreConductor());
		bitAutoInciso.getEmbedded().setPaternoConductor(autoInciso.getPaternoConductor());
		bitAutoInciso.getEmbedded().setMaternoConductor(autoInciso.getMaternoConductor());
		bitAutoInciso.getEmbedded().setNumeroLicencia(autoInciso.getNumeroLicencia());
		bitAutoInciso.getEmbedded().setFechaNacConductor(autoInciso.getFechaNacConductor());
		bitAutoInciso.getEmbedded().setOcupacionConductor(autoInciso.getOcupacionConductor());
		bitAutoInciso.getEmbedded().setDescripcionFinal(autoInciso.getDescripcionFinal());

		BitemporalInciso bitemporalInciso = incisoService.getIncisoInProcess(bitAutoInciso.getContinuity().getParentContinuity().getId(), fechaInicioVigencia.toDate());

		try {
			//Para guardar el idClienteCob y el idConductoCobro
			BitemporalCotizacion bitemporalCotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME,	idCotizacion, fechaInicioVigencia);	;
			BigDecimal idPersonaContratante = new BigDecimal(bitemporalCotizacion.getValue().getPersonaContratanteId());
			ClienteDTO cliente = null;
			ClienteGenericoDTO cuenta = null;

			if (idPersonaContratante != null) {
				cliente = clienteFacadeRemote.findById(idPersonaContratante,"nombreUsuario");
			}
			if (cliente != null) {

				bitemporalInciso.getEmbedded().setIdClienteCob(cliente.getIdCliente());
				int conductoCobro = listadoService.getConductosCobro(cliente.getIdCliente().longValue(),Integer.parseInt(autoInciso.getIdMedioPago().toString())).size() - 1;
				bitemporalInciso.getEmbedded().setIdConductoCobroCliente(new Long((conductoCobro > 0) ? (conductoCobro) : 1));
			}

			bitemporalInciso.getEmbedded().setIdMedioPago(new BigDecimal(autoInciso.getIdMedioPago()));
			bitemporalInciso.getEmbedded().setConductoCobro(autoInciso.getConductoCobroValue());

			String tipoCuenta = autoInciso.getTipoTarjetaValue();
			if (!autoInciso.getIdMedioPago().equals(MEDIO_PAGO_EFECTIVO)) {
				if(autoInciso.getIdMedioPago().equals(MEDIO_PAGO_DOMICILIACION)||autoInciso.getIdMedioPago().equals(MEDIO_CUENTA_AFIRME)){
					if (autoInciso.getNumeroTarjetaClave().length()==16){
						tipoCuenta = TIPO_CUENTA_DEBITO;
					}else if(autoInciso.getNumeroTarjetaClave().length()==18){
						tipoCuenta = TIPO_CUENTA_CUENTA_CLABE;
					}else{
						tipoCuenta = TIPO_CUENTA_CUENTA_BANCARIA;
					}
				}

				bitemporalInciso.getEmbedded().setTipoTarjeta(tipoCuenta);			

				//Informacion de la tarjeta de Credito
				bitemporalInciso.getEmbedded().setInstitucionBancaria(autoInciso.getInstitucionBancariaValue());
				bitemporalInciso.getEmbedded().setTipoTarjeta(tipoCuenta);
				bitemporalInciso.getEmbedded().setNumeroTarjetaClave(bancoMidasService.encriptaInformacionBancaria(autoInciso.getNumeroTarjetaClave()));

				if (autoInciso.getCodigoSeguridad() != null&& autoInciso.getFechaVencimiento() != null) {
					bitemporalInciso.getEmbedded().setCodigoSeguridad(bancoMidasService.encriptaInformacionBancaria(autoInciso.getCodigoSeguridad()));
					bitemporalInciso.getEmbedded().setFechaVencimiento(bancoMidasService.encriptaInformacionBancaria(creditCardFormat.format(autoInciso.getFechaVencimiento())));
				}

				//Gaurdar en ClienteCob
				if (bitemporalInciso.getEmbedded().getIdMedioPago() != null&& bitemporalInciso.getEmbedded().getConductoCobro() != null&& bitemporalInciso.getEmbedded().getInstitucionBancaria() != null) {
					cuenta = clienteMigracion(bitemporalCotizacion,bitemporalInciso.getEmbedded(), cliente);
					clienteFacadeRemote.guardarDatosCobranza(cuenta, "MIDAS", true);
				}
			}

		} catch (Exception e) {
			throw new RuntimeException(e.getMessage());

		}
		return result;
	}
	
	
	private DateTime obtenerFechaInicioVigencia(BigDecimal idCotizacion) {
		//Se obtiene la fecha de inicio de vigencia del endoso a partir de la solicitud del controlendosocot
		ControlEndosoCot controlEndoso = endosoService.obtenerControlEndosoCotCotizacion(idCotizacion);
		SolicitudDTO solicitud = controlEndoso.getSolicitud();
		return new DateTime(solicitud.getFechaInicioVigenciaEndoso());
	}
	
	private ClienteGenericoDTO clienteMigracion(BitemporalCotizacion biCotizacion,Inciso inciso, ClienteDTO clienteDto) {
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		cliente.setIdCliente(inciso.getIdClienteCob());
		cliente.setIdConductoCobranza(inciso.getIdConductoCobroCliente());
		cliente.setNombreTarjetaHabienteCobranza(biCotizacion.getValue().getNombreContratante());
		cliente.setEmailCobranza("");
		cliente.setTelefonoCobranza("");
		cliente.setNombreCalleCobranza(clienteDto.getNombreCalle());
		cliente.setCodigoPostalCobranza(clienteDto.getCodigoPostal());
		cliente.setNombreColoniaCobranza(clienteDto.getNombreColonia());
		cliente.setIdTipoConductoCobro(inciso.getIdMedioPago().intValue());
		
		List<BancoEmisorDTO> bancos = bancoMidasService.bancosSeycos();
		Integer idBancoCobranza = null;
		for(BancoEmisorDTO banco: bancos){
			if(inciso.getInstitucionBancaria().equals(banco.getNombreBanco())){
				idBancoCobranza =  banco.getIdBanco();
			}
		}
		cliente.setIdBancoCobranza(new BigDecimal(idBancoCobranza));
		cliente.setIdTipoTarjetaCobranza(inciso.getTipoTarjeta());
		cliente.setNumeroTarjetaCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getNumeroTarjetaClave()).trim());
		//Concidicon para cuando el conducto de cobro es del tipo tarjeta de credito
		if (inciso.getCodigoSeguridad() != null&&inciso.getFechaVencimiento()!=null) {
			cliente.setCodigoSeguridadCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getCodigoSeguridad()).trim());
			cliente.setFechaVencimientoTarjetaCobranza(bancoMidasService.desencriptaInformacionBancaria(inciso.getFechaVencimiento()).replace("/", ""));
		}		
		cliente.setTipoPromocion("");
		cliente.setDiaPagoTarjetaCobranza(new BigDecimal(0));				
		cliente.setRfcCobranza(clienteDto.getCodigoRFC());
		
		return cliente;
	}
	
	
	
	private List<BitemporalAutoInciso> obtenerAutoIncisosEndoso(Long idCotizacion, DateTime fechaIniVigenciaEndoso) {
		
		//Se obtienen los autoincisos inprocess con sus correspondientes incisos 
		List<BitemporalAutoInciso> bitemporalAutoIncisos = new ArrayListNullAware<BitemporalAutoInciso>();	
		BitemporalCotizacion cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,
				CotizacionContinuity.BUSINESS_KEY_NAME,	idCotizacion, fechaIniVigenciaEndoso);	
		Collection<BitemporalInciso> incisos = cotizacion.getContinuity().getIncisoContinuities().getOnlyInProcess(fechaIniVigenciaEndoso);
		for (BitemporalInciso inciso : incisos) {
			bitemporalAutoIncisos.add(inciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getOnlyInProcess(fechaIniVigenciaEndoso));
		}
		Collections.sort(bitemporalAutoIncisos, new Comparator<BitemporalAutoInciso>() {
			@Override
			public int compare(BitemporalAutoInciso obj1,
					BitemporalAutoInciso obj2) {
				
				return obj1.getContinuity().getParentContinuity().getNumero()
					.compareTo(obj2.getContinuity().getParentContinuity().getNumero());
			}
		});
		
		return bitemporalAutoIncisos;
	}



	
}
