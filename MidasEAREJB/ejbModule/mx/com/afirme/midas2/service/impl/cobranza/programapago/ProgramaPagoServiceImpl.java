package mx.com.afirme.midas2.service.impl.cobranza.programapago;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas2.dao.cobranza.programapago.ProgramaPagoDao;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToDesgloseRecibo;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToProgPago;
import mx.com.afirme.midas2.domain.cobranza.programapago.ToRecibo;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.programapago.ProgramaPagoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Stateless
public class ProgramaPagoServiceImpl implements ProgramaPagoService {
	
	@EJB
	private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	private DomicilioFacadeRemote domicilioFacadeRemote;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ProgramaPagoDao programaPagoDao;
	
	@EJB
	private IncisoService incisoService;
	
	@EJB 
	private CalculoService calculoService;
	@EJB 
	private CotizacionService cotizacionService;
	
	@Override
	public ToProgPago getProgramaPago(Long id) {
		return entidadService.findById(ToProgPago.class, id);		
	}

	@Override
	public ToProgPago getProgramaPago(BigDecimal idToCotizacion,Integer numProgPago) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cotizacion.idToCotizacion", idToCotizacion);
		params.put("numProgPago", numProgPago);
		List<ToProgPago> list = entidadService.findByProperties(ToProgPago.class, params);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}

    @Override
    public List<ToProgPago> getProgramaPago(BigDecimal idToCotizacion) {
          List<ToProgPago> programas =  entidadService.findByProperty(ToProgPago.class, "cotizacion.idToCotizacion", idToCotizacion);
          ClienteGenericoDTO contratante = new ClienteGenericoDTO();
          
          
          for(ToProgPago programa : programas){
                programa = getPrima(programa);
                
                List<ToRecibo> recibos = programa.getRecibos();
    			List<Date> fechaDesde = new ArrayList<Date>();
    			List<Date> fechaHasta = new ArrayList<Date>();
    			
    			
    			// Si solo tiene un recibo, obtenemos sus valores desde y hasta para
    			// totales de programa de pago
    			if (recibos.size() == 1) {
    				programa.setInicioPrograma(recibos.get(0).getFechaCubreDesde());
    				programa.setFinPrograma(recibos.get(0).getFechaCubreHasta());

    				// Si tiene mas de uno

    			} else {
    				for (ToRecibo r : recibos) {
    					fechaDesde.add(r.getFechaCubreDesde());
    					fechaHasta.add(r.getFechaCubreHasta());
    				}
    				Collections.sort(fechaDesde);
    				Collections.sort(fechaHasta);

    				programa.setInicioPrograma(fechaDesde.get(0));
    				programa.setFinPrograma(fechaHasta.get(fechaHasta.size() - 1));
    			}
    			
                
    			ClienteGenericoDTO filtro = new ClienteGenericoDTO();
    			programa.setNombreContratante(" ");
    			if (programa.getIdContratante() == null || programa.getIdContratante().equals("") ){
    				LogDeMidasEJB3.log("No fue posible obtener id de contratante de programa de pago", Level.INFO, null);
    			}else{
    				LogDeMidasEJB3.log("Id de contratante: " + programa.getIdContratante(), Level.INFO, null);
    				filtro.setIdCliente(new BigDecimal(programa.getIdContratante()));
        			try{
        				contratante = clienteFacadeRemote.loadByIdNoAddress(filtro);
        				programa.setNombreContratante(contratante.getNombreCompleto());			
        			}catch(Exception e){	
        				LogDeMidasEJB3.log("Excepcion al obtener el nombre del contratante: " + e.getMessage(), Level.INFO, null);
        			}
    			}
          }
          return programas;
    }
    
    @Override
	public ToProgPago getPrima(ToProgPago progPago) {
    	  for(ToRecibo recibo : progPago.getRecibos()){
              if(!recibo.getSitRecibo().equals("CAN")){
                  //progPago.setImpPrimaNeta(progPago.getImpPrimaNeta().add(recibo.getImpPrimaNeta()));
            	  //progPago.setImpRcgosPagoFR(progPago.getImpRcgosPagoFR().add(recibo.getImpRcgosPagoFR()));
            	  //progPago.setImpDerechos(progPago.getImpDerechos().add(recibo.getImpDerechos())  );
            	  //progPago.setImpIVA(progPago.getImpIVA().add(recibo.getImpIVA()));
            	  //progPago.setImpPrimaTotal(progPago.getImpPrimaTotal().add(recibo.getImpPrimaTotal()));         
                    progPago.setImpBomoProm(progPago.getImpBomoProm().add(recibo.getImpBomoProm()));
                    progPago.setImpBonComis(progPago.getImpBonComis().add(recibo.getImpBonComis()));
                    progPago.setImpBonComPN(progPago.getImpBonComPN().add(recibo.getImpBonComPN()));
                    progPago.setImpBonComRPF(progPago.getImpBonComRPF().add(recibo.getImpBonComRPF()));
                    progPago.setImpBonoAgt(progPago.getImpBonoAgt().add(recibo.getImpBonoAgt()));
                    progPago.setImpCesionDerechosAgt(progPago.getImpCesionDerechosAgt().add(recibo.getImpCesionDerechosAgt()));
                    progPago.setImpCesionDerechosProm(progPago.getImpCesionDerechosProm().add(recibo.getImpCesionDerechosProm()));
                    progPago.setImpComAgt(progPago.getImpComAgt().add(recibo.getImpComAgt()));
                    progPago.setImpComAgtPN(progPago.getImpComAgtPN().add(recibo.getImpComAgtPN()));
                    progPago.setImpComAgtRPF(progPago.getImpComAgtRPF().add(recibo.getImpComAgtRPF()));
                    progPago.setImpComSup(progPago.getImpComSup().add(recibo.getImpComSup()));
                    progPago.setImpComSupPN(progPago.getImpComSupPN().add(recibo.getImpComSupPN()));
                    progPago.setImpComSupRPF(progPago.getImpComSupRPF().add(recibo.getImpComSupRPF()));
                    progPago.setImpDerechos(progPago.getImpDerechos().add(recibo.getImpDerechos()));
                    progPago.setImpIVA(progPago.getImpIVA().add(recibo.getImpIVA()));
                    progPago.setImpOtrosImptos(progPago.getImpOtrosImptos().add(recibo.getImpOtrosImptos()));
                    progPago.setImpPrimaNeta(progPago.getImpPrimaNeta().add(recibo.getImpPrimaNeta()));
                    progPago.setImpPrimaTotal(progPago.getImpPrimaTotal().add(recibo.getImpPrimaTotal()));
                    progPago.setImpRcgosPagoFR(progPago.getImpRcgosPagoFR().add(recibo.getImpRcgosPagoFR()));
                    progPago.setImpSobreComAgt(progPago.getImpSobreComAgt().add(recibo.getImpSobreComAgt()));
                    progPago.setImpSobreComProm(progPago.getImpSobreComProm().add(recibo.getImpSobreComProm()));
                    progPago.setImpSobreComUdiAgt(progPago.getImpSobreComUdiAgt().add(recibo.getImpSobreComUdiAgt()));
                    progPago.setImpSobreComUdiProm(progPago.getImpSobreComUdiProm().add(recibo.getImpSobreComUdiProm()));
                   
              }
        }
    	progPago.setNumRecibos(progPago.getRecibos().size());
    	
        return progPago;
	}

	@Override
	public ToRecibo getRecibo(Long id) {
		return entidadService.findById(ToRecibo.class, id);
	}

	@Override
	public ToRecibo getRecibo(Long idProgPago, Integer numeroExhibicion) {
		Map<String,Object> params = new HashMap<String, Object>();
		
		params.put("progPago.id", idProgPago);
		params.put("numExhibicion", numeroExhibicion);
		List<ToRecibo> list = entidadService.findByProperties(ToRecibo.class, params);
		if(list != null && list.size() > 0){
			return list.get(0);
		}
		return null;
	}
	
	@Override
	public List<ToRecibo> getReciboDePrograma(Long idProgPago) {
		return entidadService.findByProperty(ToRecibo.class, "progPago.id", idProgPago);		
	}

	@Override
	public List<ToRecibo> getRecibo(BigDecimal idToCotizacion) {		
		return entidadService.findByProperty(ToRecibo.class, "progPago.cotizacion.idToCotizacion", idToCotizacion);		
	}

	@Override
	public List<ToRecibo> getRecibo(BigDecimal idToCotizacion, Long idProgPago) {
      
		//Traemos un Objeto Programa de pago por su ID
	   ToProgPago programaPago = this.getProgramaPago(idProgPago);
		   
		//   this.getProgramaPago(idToCotizacion, this.getProgramaPago(idProgPago).getNumProgPago())  ;
		
	   List<ToRecibo> listRecibos = programaPago.getRecibos();
	   List<ToRecibo> listRecibosPagados = new ArrayList<ToRecibo>();
	    //Traer toda la lista de recibos de un Programa de Pagos
	   //listRecibos = getReciboDePrograma(programaPago.getId()) ;
	   
	   for(ToRecibo recibo : listRecibos){
           if(!recibo.getSitRecibo().equals("CAN")){
        	   listRecibosPagados.add(recibo);
           }
	   }
	 
	   return listRecibosPagados;
	}
	
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> agregarProgramaPago(ToProgPago dto, BigDecimal formaPago) {

		StoredProcedureHelper storedHelper = null;
		Map<String, String> mensaje = new HashMap<String, String>();
		try {
			LogDeMidasEJB3.log("Datos de Emision: pIdCotizacion ="
					+ dto.getCotizacion().getIdToCotizacion() + " idProgPago="
					+ dto.getId() + " primaNeta=" + dto.getImpPrimaNetaDiferencia()
					+ " derechos=" + dto.getImpDerechos() + " recargos="
					+ dto.getImpRcgosPagoFR() + " iva=" + dto.getImpIVA()
					+ " primaTotal=" + dto.getImpPrimaTotal(), Level.INFO, null);

			storedHelper = new StoredProcedureHelper("Midas.PKGAUT_RECUOTIFICAII.GeneraProgramaPago", "jdbc/MidasDataSource");
			
			storedHelper.estableceParametro("PidToCotizacion", dto.getCotizacion().getIdToCotizacion());
			storedHelper.estableceParametro("pidProgPago", new BigDecimal(dto.getId()));
			storedHelper.estableceParametro("pFormaPago", formaPago);
			storedHelper.estableceParametro("pimpPrimaNeta", dto.getImpPrimaNetaDiferencia());
			storedHelper.estableceParametro("pimpRcgosPagoFR", dto.getImpRcgosPagoFR());
			storedHelper.estableceParametro("pimpDerechos", dto.getImpDerechos());
			storedHelper.estableceParametro("pimpIVA", dto.getImpIVA());
			storedHelper.estableceParametro("pimpPrimaTotal", dto.getImpPrimaTotal());
			
			CotizacionDTO cotizacionDTO = cotizacionService.obtenerCotizacionPorId(dto.getCotizacion().getIdToCotizacion());
			//Actualizar estatus
			actualizarBanderaRecuotificacionManual(dto.getCotizacion().getIdToCotizacion(), Boolean.TRUE);
			
			ResumenCostosDTO resumenCostosDTO= calculoService.obtenerResumenCotizacion(cotizacionDTO, false);
			
			//System.out.println("--- RESUMEN DE COSTOS ---");
			//System.out.println("pImpPrimaNeta" + resumenCostosDTO.getPrimaNetaCoberturas());
			//System.out.println("pImpDerechos" +  resumenCostosDTO.getDerechos());
			//System.out.println("pImpRcgoPagoFr" + resumenCostosDTO.getRecargo());
			//System.out.println("pImpIva" + resumenCostosDTO.getIva());
			//System.out.println("pImpPrimaTotal" + resumenCostosDTO.getPrimaTotal());
			
			storedHelper.estableceParametro("pImpPrimaNetaCot", resumenCostosDTO.getPrimaNetaCoberturas());
			storedHelper.estableceParametro("pImpDerechosCot", resumenCostosDTO.getDerechos());
			storedHelper.estableceParametro("pImpRcgoPagoFrCot", resumenCostosDTO.getRecargo());
			storedHelper.estableceParametro("pImpIvaCot", resumenCostosDTO.getIva());
			storedHelper.estableceParametro("pImpPrimaTotalCot", resumenCostosDTO.getPrimaTotal());
			
			
			/*
			storedHelper.estableceParametro("pOrigen", "D"); // D de daños			
			storedHelper.estableceParametro("pCodigoUsuario", dto.getCodigoUsuarioCreacion());
			*/
			
			Integer res = Integer.valueOf(storedHelper.ejecutaActualizar());
			LogDeMidasEJB3.log("Valor de resultado de operacion agregarPP: " + res, Level.INFO, null);

			if (res != null) {
				mensaje.put("res", "30");// Exito
				mensaje.put("codigoRes", res.toString());
				mensaje.put("resDesc",
						"Programa de pago agregado exitosamente...");
			} else {
				String descErr = null;
				mensaje.put("res", "10");// Error
				if (storedHelper != null) {
					descErr = storedHelper.getDescripcionRespuesta();
					mensaje.put("resDesc",
							"Error al agregar Programa de pago: " + descErr);
				}
			}
			return mensaje;
		} catch (SQLException e) {
			mensaje.put("res", "10");// Error
			if (storedHelper != null) {
				//Integer codErr = storedHelper.getCodigoRespuesta();
				String descErr = storedHelper.getDescripcionRespuesta();
				mensaje.put("resDesc", "Error al agregar programa de pago: "
						+ descErr);
			}

			LogDeMidasEJB3.log(
					"Excepcion en BD de Midas.PKGAUT_RECUOTIFICAII.GeneraProgramaPago..." + e.getMessage(), Level.WARNING, null);
			return mensaje;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion general en stpGenerarRecibosOriginales..."
					+ e.getMessage(), Level.WARNING, null);
			mensaje.put("res", "10");// Error
			mensaje.put("resDesc", "stpGenerarRecibosOriginales: "
					+ e.getMessage());
			return mensaje;
		}
	}

	@Override
	public ToDesgloseRecibo getDesglose(Long id) {
		return entidadService.findById(ToDesgloseRecibo.class, id);	
	}

	@Override
	public List<ToDesgloseRecibo> getDesgloseRecibo(Long idRecibo) {
		return entidadService.findByProperty(ToDesgloseRecibo.class, "recibo.id", idRecibo);		
	}

	@Override
	public void guardar(ToProgPago progPago) {
		entidadService.save(progPago);
		actualizarBanderaRecuotificacionManual(progPago.getCotizacion().getIdToCotizacion(), Boolean.TRUE);
	}

	@Override
	public void guardar(ToRecibo recibo) {
        entidadService.save(recibo);
        actualizarBanderaRecuotificacionManual(recibo.getProgPago().getCotizacion().getIdToCotizacion(), Boolean.TRUE);
	}

	@Override
	public void guardar(List<ToRecibo> recibos, BigDecimal idToCotizacion) {
		// TODO Auto-generated method stub
		List<ToRecibo> listEditada = recibos;
		//Obtener el ID Cotizacion
	    List<ToRecibo> listOriginal = this.getRecibo(idToCotizacion);
	    
	    ToProgPago programaPago = new ToProgPago();
	    String codigoUsuarioCreacion = null; 
	    Date fechaRegistro = null; 
	    String cveTipoDocumento = null; 
	    String cveRecibo = null; 
	    String sitRecibo = null; 
		
	    for(ToRecibo l1 :listEditada)
        {
            int flag = 0;
            for(ToRecibo l2 :listOriginal)
            {
            	
               programaPago = l2.getProgPago();
       	       codigoUsuarioCreacion = l2.getCodigoUsuarioCreacion();
    	       fechaRegistro = l2.getFechaRegistro();
    	       cveTipoDocumento = l2.getCveTipoDocumento();
    	       cveRecibo = l2.getCveRecibo();
    	       sitRecibo = l2.getSitRecibo();
            	
               if(l2.getId().equals(l1.getId()))
               {
                  if(l1.equals(l2))
                  {  
                     flag = 2; //
                     LogDeMidasEJB3.log("no hacemos nada ya que el id es igual " + l1.getId(), Level.INFO, null);
                     break;
                  }
                  else
                  {
                     flag = 1; //
                     LogDeMidasEJB3.log("vamos a hacer el update del ids " + l1.getId(), Level.INFO, null);
                     l1.setProgPago(programaPago);
                     l1.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
                     l1.setFechaRegistro(fechaRegistro);
                     l1.setCveTipoDocumento(cveTipoDocumento);
                     l1.setCveRecibo(cveRecibo);
                     l1.setSitRecibo(sitRecibo);
                     
                     l1.setImpBomoProm( BigDecimal.ZERO);
                     l1.setImpBonComis( BigDecimal.ZERO);
                     l1.setImpBonComPN( BigDecimal.ZERO);
                     l1.setImpBonComRPF( BigDecimal.ZERO);
                     l1.setImpBonoAgt( BigDecimal.ZERO);
                     l1.setImpCesionDerechosAgt( BigDecimal.ZERO);
                     l1.setImpCesionDerechosProm( BigDecimal.ZERO);
                     l1.setImpComAgt( BigDecimal.ZERO);
                     l1.setImpComAgtPN( BigDecimal.ZERO);
                     l1.setImpComAgtRPF( BigDecimal.ZERO);
                     l1.setImpComSup( BigDecimal.ZERO);
                     l1.setImpComSupPN( BigDecimal.ZERO);
                     l1.setImpComSupRPF( BigDecimal.ZERO);
                     l1.setImpOtrosImptos( BigDecimal.ZERO);
                     l1.setImpSobreComAgt( BigDecimal.ZERO);
                     l1.setImpSobreComProm( BigDecimal.ZERO);
                     l1.setImpSobreComUdiAgt( BigDecimal.ZERO);
                     l1.setImpSobreComUdiProm( BigDecimal.ZERO);
                     
                     this.guardar(l1);
                    // dao.update(l1);
                    
                     break;
                  }
               }
            } 
            
            
            if(flag==0)
            {
            	LogDeMidasEJB3.log("vamos a insertar el id "+ l1.getId(), Level.INFO, null);
              
               l1.setProgPago(programaPago);
               l1.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
               l1.setFechaRegistro(fechaRegistro);
               l1.setCveTipoDocumento(cveTipoDocumento);
               l1.setCveRecibo(cveRecibo);
               l1.setSitRecibo(sitRecibo);
               l1.setId(null);
               
               l1.setImpBomoProm( BigDecimal.ZERO);
               l1.setImpBonComis( BigDecimal.ZERO);
               l1.setImpBonComPN( BigDecimal.ZERO);
               l1.setImpBonComRPF( BigDecimal.ZERO);
               l1.setImpBonoAgt( BigDecimal.ZERO);
               l1.setImpCesionDerechosAgt( BigDecimal.ZERO);
               l1.setImpCesionDerechosProm( BigDecimal.ZERO);
               l1.setImpComAgt( BigDecimal.ZERO);
               l1.setImpComAgtPN( BigDecimal.ZERO);
               l1.setImpComAgtRPF( BigDecimal.ZERO);
               l1.setImpComSup( BigDecimal.ZERO);
               l1.setImpComSupPN( BigDecimal.ZERO);
               l1.setImpComSupRPF( BigDecimal.ZERO);
               l1.setImpOtrosImptos( BigDecimal.ZERO);
               l1.setImpSobreComAgt( BigDecimal.ZERO);
               l1.setImpSobreComProm( BigDecimal.ZERO);
               l1.setImpSobreComUdiAgt( BigDecimal.ZERO);
               l1.setImpSobreComUdiProm( BigDecimal.ZERO);

              // progPago.setImpDerechos(progPago.getImpDerechos().add(recibo.getImpDerechos()));
              // progPago.setImpIVA(progPago.getImpIVA().add(recibo.getImpIVA()));
               // progPago.setImpOtrosImptos(progPago.getImpOtrosImptos().add(recibo.getImpOtrosImptos()));
               // progPago.setImpPrimaNeta(progPago.getImpPrimaNeta().add(recibo.getImpPrimaNeta()));
               // progPago.setImpPrimaTotal(progPago.getImpPrimaTotal().add(recibo.getImpPrimaTotal()));
               //progPago.setImpRcgosPagoFR(progPago.getImpRcgosPagoFR().add(recibo.getImpRcgosPagoFR()));
               // progPago.setImpSobreComAgt(progPago.getImpSobreComAgt().add(recibo.getImpSobreComAgt()));
               // progPago.setImpSobreComProm(progPago.getImpSobreComProm().add(recibo.getImpSobreComProm()));
               // progPago.setImpSobreComUdiAgt(progPago.getImpSobreComUdiAgt().add(recibo.getImpSobreComUdiAgt()));
               // progPago.setImpSobreComUdiProm(progPago.getImpSobreComUdiProm().add(recibo.getImpSobreComUdiProm()));

               this.guardar(l1);
              // ORA-01400: cannot insert NULL into ("MIDAS"."TORECIBO"."PROGPAGO_ID")
               
            }
        }
	    
	       //barremos la lista dos que es la que tenemos en la base de datos
        for(ToRecibo l2 :listOriginal)
        {
            int flag = 0;
            for(ToRecibo l1 :listEditada)
            {
                if(l2.getId().equals(l1.getId()))
                {
                   flag = 2;
                   break;
                }
            } 
            
            if(flag==0)
            {
            	LogDeMidasEJB3.log("vamos a eliminar el id "+ l2.getId(), Level.INFO, null);
             
               //dao.delete(l2);
               this.eliminarRecibo(l2.getId());    
            }          
        }
	    
        //hay que ir a buscar el programa de pago relacionado y actualizarlo
		
	}
	
	
	

	@Override
	public void eliminarProgPago(Long idProgPago) {
       entidadService.remove(entidadService.findById(ToProgPago.class, idProgPago));
	}

	@Override
	public void eliminarRecibo(Long idToRecibo) {
       entidadService.remove(entidadService.findById(ToRecibo.class, idToRecibo));
	}

	@Override
	public List<ToProgPago> inicializarProgramaPago(Long idToCotizacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<ToRecibo> inicializaRecibos(Long idProgPago) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Saldo> obtenerSaldoOriginal(Long idCotizacion) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Saldo obtenerSaldoOriginal(CotizacionDTO cotizacion, BigDecimal idProgPago, Long idReferencia) {
		return programaPagoDao.obtenerSaldoOriginal(cotizacion, null, idReferencia);
	}

	@Override
	public Saldo obtenerSaldoOriginalPorInciso(CotizacionDTO cotizacion, BigDecimal idProgPago, Long numeroInciso) {
		return programaPagoDao.obtenerSaldoOriginalPorInciso(cotizacion, null, numeroInciso);
	}
	
	public Saldo obtenerSaldoOriginalDeInciso(CotizacionDTO cotizacion, BigDecimal idProgPago, Long numeroInciso) {
		return programaPagoDao.obtenerSaldoOriginalPorInciso(cotizacion, idProgPago, numeroInciso);
	}

	@Override
	public List<Saldo> obtenerSaldoProgramaPago(Long idCotizacion) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Saldo> obtenerSaldoRecibos(Long idRecibo) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Saldo obtenerSaldoActual(BigDecimal idToCotizacion) {
		// TODO Auto-generated method stub
		Saldo saldoActual = new Saldo();
		
		List<ToProgPago> listProgPago = this.getProgramaPago(idToCotizacion);
		//List<ToProgPago> listProgPagoPrima = new ArrayList<ToProgPago>();
		
		//for(ToProgPago p : listProgPago){
		//	listProgPagoPrima.add(this.getPrima(p));
		//}
		
		for(ToProgPago progPagoPrima : listProgPago){
			saldoActual.setImpBomoProm(saldoActual.getImpBomoProm().add(progPagoPrima.getImpBomoProm())) ; 
			saldoActual.setImpBonComis(saldoActual.getImpBonComis().add(progPagoPrima.getImpBonComis())) ; 
			saldoActual.setImpBonComPN(saldoActual.getImpBonComPN().add(progPagoPrima.getImpBonComPN())) ; 
			saldoActual.setImpBonComRPF(saldoActual.getImpBonComRPF().add(progPagoPrima.getImpBonComRPF())) ; 
			saldoActual.setImpBonoAgt(saldoActual.getImpBonoAgt().add(progPagoPrima.getImpBonoAgt())) ; 
			saldoActual.setImpCesionDerechosAgt(saldoActual.getImpCesionDerechosAgt().add(progPagoPrima.getImpCesionDerechosAgt())) ; 
			saldoActual.setImpCesionDerechosProm(saldoActual.getImpCesionDerechosProm().add(progPagoPrima.getImpCesionDerechosProm())) ; 
			saldoActual.setImpComAgt(saldoActual.getImpComAgt().add(progPagoPrima.getImpComAgt())) ; 
			saldoActual.setImpComAgtPN(saldoActual.getImpComAgtPN().add(progPagoPrima.getImpComAgtPN())) ; 
			saldoActual.setImpComAgtRPF(saldoActual.getImpComAgtRPF().add(progPagoPrima.getImpComAgtRPF())) ; 
			saldoActual.setImpComSup(saldoActual.getImpComSup().add(progPagoPrima.getImpComSup())) ; 
			saldoActual.setImpComSupPN(saldoActual.getImpComSupPN().add(progPagoPrima.getImpComSupPN())) ; 
			saldoActual.setImpComSupRPF(saldoActual.getImpComSupRPF().add(progPagoPrima.getImpComSupRPF())) ; 
			saldoActual.setImpDerechos(saldoActual.getImpDerechos().add(progPagoPrima.getImpDerechos())) ; 
			saldoActual.setImpIVA(saldoActual.getImpIVA().add(progPagoPrima.getImpOtrosImptos())) ; 
			saldoActual.setImpOtrosImptos(saldoActual.getImpOtrosImptos().add(progPagoPrima.getImpOtrosImptos())) ; 
			saldoActual.setImpPrimaNeta(saldoActual.getImpPrimaNeta().add(progPagoPrima.getImpPrimaNeta())) ; 
			saldoActual.setImpPrimaTotal(saldoActual.getImpPrimaTotal().add(progPagoPrima.getImpPrimaTotal())) ; 
			saldoActual.setImpRcgosPagoFR(saldoActual.getImpRcgosPagoFR().add(progPagoPrima.getImpRcgosPagoFR())) ; 
			saldoActual.setImpSobreComAgt(saldoActual.getImpSobreComAgt().add(progPagoPrima.getImpSobreComAgt())) ; 
			saldoActual.setImpSobreComProm(saldoActual.getImpSobreComProm().add(progPagoPrima.getImpSobreComProm())) ; 
			saldoActual.setImpSobreComUdiAgt(saldoActual.getImpSobreComUdiAgt().add(progPagoPrima.getImpSobreComUdiAgt())) ; 
			saldoActual.setImpSobreComUdiProm(saldoActual.getImpSobreComUdiProm().add(progPagoPrima.getImpSobreComUdiProm())) ; 
		}
		//Traer una Lista de Todos los Programas de Pago
		return saldoActual;
	}

	@Override
	public Saldo obtenerSaldoOriginal(BigDecimal idToCotizacion) {
		Saldo saldoOriginal = new Saldo();
		
		saldoOriginal.setImpBomoProm(saldoOriginal.getImpBomoProm().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpBonComis(saldoOriginal.getImpBonComis().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpBonComPN(saldoOriginal.getImpBonComPN().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpBonComRPF(saldoOriginal.getImpBonComRPF().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpBonoAgt(saldoOriginal.getImpBonoAgt().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpCesionDerechosAgt(saldoOriginal.getImpCesionDerechosAgt().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpCesionDerechosProm(saldoOriginal.getImpCesionDerechosProm().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpComAgt(saldoOriginal.getImpComAgt().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpComAgtPN(saldoOriginal.getImpComAgtPN().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpComAgtRPF(saldoOriginal.getImpComAgtRPF().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpComSup(saldoOriginal.getImpComSup().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpComSupPN(saldoOriginal.getImpComSupPN().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpComSupRPF(saldoOriginal.getImpComSupRPF().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpDerechos(saldoOriginal.getImpDerechos().add(new BigDecimal("20"))) ; 
		saldoOriginal.setImpIVA(saldoOriginal.getImpIVA().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpOtrosImptos(saldoOriginal.getImpOtrosImptos().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpPrimaNeta(saldoOriginal.getImpPrimaNeta().add(new BigDecimal("138591.19"))) ; 
		saldoOriginal.setImpPrimaTotal(saldoOriginal.getImpPrimaTotal().add(new BigDecimal("144336"))) ; 
		saldoOriginal.setImpRcgosPagoFR(saldoOriginal.getImpRcgosPagoFR().add(new BigDecimal("5744.81"))) ; 
		saldoOriginal.setImpSobreComAgt(saldoOriginal.getImpSobreComAgt().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpSobreComProm(saldoOriginal.getImpSobreComProm().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpSobreComUdiAgt(saldoOriginal.getImpSobreComUdiAgt().add(new BigDecimal("0"))) ; 
		saldoOriginal.setImpSobreComUdiProm(saldoOriginal.getImpSobreComUdiProm().add(new BigDecimal("0"))) ; 

		return saldoOriginal;
	}

	@Override
	public Saldo obtenerSaldoPeriodoPago(Long idProgPago) {
		Saldo saldo = new Saldo();

		// Traemos un Objeto Programa de pago por su ID
		ToProgPago programaPago = this.getProgramaPago(idProgPago);
		List<ToRecibo> listRecibos = programaPago.getRecibos();
		
		for (ToRecibo e : listRecibos) {
			saldo.setImpPrimaNeta(saldo.getImpPrimaNeta().add(
					e.getImpPrimaNeta()));
			saldo.setImpRcgosPagoFR(saldo.getImpRcgosPagoFR().add(
					e.getImpRcgosPagoFR()));
			saldo
					.setImpDerechos(saldo.getImpDerechos().add(
							e.getImpDerechos()));
			saldo.setImpIVA(saldo.getImpIVA().add(e.getImpIVA()));
			saldo.setImpPrimaTotal(saldo.getImpPrimaTotal().add(
					e.getImpPrimaTotal()));
		} 

		return saldo;
	}

	@Override
	public Saldo obtenerSaldoDiferencia(BigDecimal idToCotizacion){
		// TODO Auto-generated method stub
		Saldo saldoDiferencia = new Saldo();
		Saldo saldoActual = this.obtenerSaldoActual(idToCotizacion);
		Saldo saldoOriginal  = this.obtenerSaldoOriginal (idToCotizacion);
	
		saldoDiferencia.setImpBomoProm( saldoOriginal.getImpBomoProm().subtract(saldoActual.getImpBomoProm()) );
		saldoDiferencia.setImpBonComis( saldoOriginal.getImpBonComis().subtract(saldoActual.getImpBonComis()) );
		saldoDiferencia.setImpBonComPN( saldoOriginal.getImpBonComPN().subtract(saldoActual.getImpBonComPN()) );
		saldoDiferencia.setImpBonComRPF( saldoOriginal.getImpBonComRPF().subtract(saldoActual.getImpBonComRPF()) );
		saldoDiferencia.setImpBonoAgt( saldoOriginal.getImpBonoAgt().subtract(saldoActual.getImpBonoAgt()) );
		saldoDiferencia.setImpCesionDerechosAgt( saldoOriginal.getImpCesionDerechosAgt().subtract(saldoActual.getImpCesionDerechosAgt()) );
		saldoDiferencia.setImpCesionDerechosProm( saldoOriginal.getImpCesionDerechosProm().subtract(saldoActual.getImpCesionDerechosProm()) );
		saldoDiferencia.setImpComAgt( saldoOriginal.getImpComAgt().subtract(saldoActual.getImpComAgt()) );
		saldoDiferencia.setImpComAgtPN( saldoOriginal.getImpComAgtPN().subtract(saldoActual.getImpComAgtPN()) );
		saldoDiferencia.setImpComAgtRPF( saldoOriginal.getImpComAgtRPF().subtract(saldoActual.getImpComAgtRPF()) );
		saldoDiferencia.setImpComSup( saldoOriginal.getImpComSup().subtract(saldoActual.getImpComSup()) );
		saldoDiferencia.setImpComSupPN( saldoOriginal.getImpComSupPN().subtract(saldoActual.getImpComSupPN()) );
		saldoDiferencia.setImpComSupRPF( saldoOriginal.getImpComSupRPF().subtract(saldoActual.getImpComSupRPF()) );
		saldoDiferencia.setImpDerechos( saldoOriginal.getImpDerechos().subtract(saldoActual.getImpDerechos()) );
		saldoDiferencia.setImpIVA( saldoOriginal.getImpIVA().subtract(saldoActual.getImpIVA()) );
		saldoDiferencia.setImpOtrosImptos( saldoOriginal.getImpOtrosImptos().subtract(saldoActual.getImpOtrosImptos()) );
		saldoDiferencia.setImpPrimaNeta( saldoOriginal.getImpPrimaNeta().subtract(saldoActual.getImpPrimaNeta()) );
		saldoDiferencia.setImpPrimaTotal( saldoOriginal.getImpPrimaTotal().subtract(saldoActual.getImpPrimaTotal()) );
		saldoDiferencia.setImpRcgosPagoFR( saldoOriginal.getImpRcgosPagoFR().subtract(saldoActual.getImpRcgosPagoFR()) );
		saldoDiferencia.setImpSobreComAgt( saldoOriginal.getImpSobreComAgt().subtract(saldoActual.getImpSobreComAgt()) );
		saldoDiferencia.setImpSobreComProm( saldoOriginal.getImpSobreComProm().subtract(saldoActual.getImpSobreComProm()) );
		saldoDiferencia.setImpSobreComUdiAgt( saldoOriginal.getImpSobreComUdiAgt().subtract(saldoActual.getImpSobreComUdiAgt()) );
		saldoDiferencia.setImpSobreComUdiProm( saldoOriginal.getImpSobreComUdiProm().subtract(saldoActual.getImpSobreComUdiProm()) );
		
		return saldoDiferencia;
	}
	
	@Override
	public ContenedorDatosProgramaPago obtenerDatosCliente(Long idProgPago, BigDecimal idToCotizacion){
		ClienteGenericoDTO contratante = new ClienteGenericoDTO();
		CotizacionDTO cotizacion = new CotizacionDTO();
		ContenedorDatosProgramaPago datos = new ContenedorDatosProgramaPago();
		ToProgPago progPago = entidadService.findById(ToProgPago.class, idProgPago);
		BigDecimal idContratante =  new BigDecimal(progPago.getIdContratante());
		BigDecimal idDomicilioContratante = cotizacion.getIdDomicilioContratante();
	    ToProgPago programaPago = new ToProgPago();
	    programaPago =  this.getProgramaPago(idProgPago);
		
	    datos.setNumProgPago(programaPago.getNumProgPago());
		datos.setRecibos(this.getReciboDePrograma(idProgPago));
		datos.setSaldo(this.obtenerSaldoPeriodoPago(idProgPago));
		datos.setIdToCotizacion(idToCotizacion);
		
	    
		
		if(datos.getCliente() == null && idContratante != null){
			ClienteGenericoDTO filtro = new ClienteGenericoDTO();
			filtro.setIdCliente(idContratante);
			try{
				contratante = clienteFacadeRemote.loadByIdNoAddress(filtro);
				datos.setCliente(contratante);			
			}catch(Exception e){			
			}
		}
		
		
		
		if(datos.getDomicilioCliente() == null){
			if(idDomicilioContratante != null){
				try{
					datos.setDomicilioCliente(domicilioFacadeRemote.findDomicilioImpresionById(idDomicilioContratante.longValue()));
				}catch(Exception ex){			
				}
			}else{
				if(datos.getCliente() != null){
					ClienteGenericoDTO filtro = new ClienteGenericoDTO();
					filtro.setIdCliente(idContratante);
					try {
						ClienteGenericoDTO cliente = clienteFacadeRemote.loadById(filtro);
						if(cliente != null){
							if(cliente.getIdDomicilioConsulta() != null){
								datos.setDomicilioCliente(domicilioFacadeRemote.findDomicilioImpresionById(cliente.getIdDomicilioConsulta()));
							}
						}
					} catch (Exception e) {
					}
				}
			}
		}
		datos.setNombreContratante(datos.getCliente().getNombreCompleto());
		return datos;
	}
	
	@Override
	public TransporteImpresionDTO imprimirProgramaPagos( BigDecimal idToCotizacion ){

		GeneradorImpresion gImpresion = new GeneradorImpresion();
		
		List<ContenedorDatosProgramaPago> dataSourceProgramaPago = new ArrayList<ContenedorDatosProgramaPago>();
		
        //Traer la Lista de Programa de Pago de la Cotizacion
		List<ToProgPago> listProgramaPago = new ArrayList<ToProgPago>();
		listProgramaPago = this.getProgramaPago(idToCotizacion);
		
		for (ToProgPago p : listProgramaPago)
		{
			dataSourceProgramaPago.add(this.obtenerDatosCliente(p.getId(), idToCotizacion));
		}
		
		// Compilado de jReports y generación del .jasper
		String jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionProgramaPagos.jrxml";
		JasperReport jReport = gImpresion.getOJasperReport(jrxml);
		
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionRecibos.jrxml";
		JasperReport jReportRecibos = gImpresion.getOJasperReport(jrxml);
		
		// Creación de los datasources que contienen la información a mapear en el jrxml
		//List<ToProgPago> dataSourceCondicionEspecial = new ArrayList<ToProgPago>();
		//dataSourceCondicionEspecial = this.getProgramaPago(idToCotizacion);
		
		//List<ToRecibo> dataSourceAreasImpacto = this.getLstAreasAsociadas(idCondicionEspecial, true);
		//	List<AreaCondicionDTO> dataSourceAreasImpacto = this.obtenerAreaCondicion(idCondicionEspecial);
		//	List<CoberturasCondicionDTO> dataSourceNegocioCoberturas = this.getLstCoberturasAsociadas(idCondicionEspecial);
		//	List<VarAjusteCondicionDTO> dataSourceVarAjuste = this.getLstVariablesAsociadas(idCondicionEspecial);
		
		// Se definen los parámetros a mapear en el jrxml
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		params.put("dataSourceProgramaPago", dataSourceProgramaPago);	
		params.put("jReportRecibos", jReportRecibos);
		//params.put("dataSourceAreasImpacto", dataSourceAreasImpacto);
	    //	params.put("jReportFactorAreaCondEsp", jReportFactorAreaCondEsp);
		//	params.put("dataSourceNegocioCoberturas", dataSourceNegocioCoberturas);
		//	params.put("jReportNegocioCoberturas", jReportNegocioCoberturas);
		//  params.put("jReportNegocio", jReportNegocio);
		//	params.put("dataSourceVarAjuste", dataSourceVarAjuste);
		//	params.put("jReportVariablesAjuste", jReportVariablesAjuste);
		
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceProgramaPago);
		
		// Rellena el reporte con los datos y exporta el PDF
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}

	@Override
	public Map<String, String> obtenerPersonasAseguradasCot(BigDecimal arg0) {
		
		Map<String,String> mapaPersonasAseguradasCot = new HashMap<String, String>();
		
		/*
		List<IncisoAutoCot> incisoAutoCot = programaPagoDao.obtenerAseguradosCot(arg0);
		System.out.println("Personas resultantes: " + incisoAutoCot.size());
		for(IncisoAutoCot elemento:incisoAutoCot){
			int contador = 0;
			
			//if(elemento.getPersonaAseguradoId() != null){
			//	mapaPersonasAseguradasCot.put(elemento.getPersonaAseguradoId().toString(), elemento.getNombreAsegurado());
			//}else{
			//	mapaPersonasAseguradasCot.put( "c" + elemento.getId(), elemento.getNombreAsegurado());
			//}
			
			System.out.println("PERSONA: " + elemento.getId()+ " : " + elemento.getNombreAsegurado());
			if (elemento.getId() == null){
				mapaPersonasAseguradasCot.put("c", elemento.getNombreAsegurado());
			}else{
				mapaPersonasAseguradasCot.put(elemento.getId().toString(), elemento.getNombreAsegurado());
			}
		}
		*/
		
		//List<ClienteGenericoDTO> incisoAutoCot = programaPagoDao.obtenerTodosLosAsegurados(arg0);
		//System.out.println("Personas resultantes: " + incisoAutoCot.size());
		//for(ClienteGenericoDTO elemento : incisoAutoCot){
		//	System.out.println("PERSONA: " + elemento.getIdClienteString()+ " : " + elemento.getNombreCompleto());
		//		mapaPersonasAseguradasCot.put(elemento.getIdClienteString(), elemento.getNombreCompleto());
		//}
		
		//System.out.println("REGISTROS EN MAPA: " + mapaPersonasAseguradasCot.size());
		return mapaPersonasAseguradasCot;
	}
	
	public Boolean actualizarAutoIncisoCot(String idAutoIncisoCot, ClienteGenericoDTO Cliente){
		
		Boolean regreso = false;
		try{
			
			IncisoAutoCot incisoAutoCot  = incisoService.obtenerIncisoAutoCotPorId(Long.valueOf(idAutoIncisoCot));
			incisoAutoCot.setPersonaAseguradoId(Cliente.getIdCliente().longValue());
			incisoAutoCot.setNombreAsegurado(Cliente.getNombre());
			incisoService.guardarIncisoAutoCot(incisoAutoCot);
			regreso = true;
		}catch(Exception ex){
			LogDeMidasEJB3.log("Excepcion generada al actualizar autoIncisoCot: " + ex.getMessage(), Level.WARNING, null);
		}
		
		return regreso;
	}
	
	//Si son distintos, regresa true
	public Boolean validaClineteOriginalCot(BigDecimal idToCotizacion, ClienteGenericoDTO cliente){
		Boolean regreso = false;
		CotizacionDTO cotizacionPoliza = new CotizacionDTO();
		cotizacionPoliza = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if (cliente == null){
			return true;
		}
		if(cotizacionPoliza != null && cliente.getIdCliente() != null){
			if(!(cotizacionPoliza.getIdToPersonaContratante().longValue() == cliente.getIdCliente().longValue())){
				regreso = true;
			}
		}else{
			regreso = true;
		}
		return regreso; 
	}
	
	public Map<String, String> simulaEmitePoliza(CotizacionDTO cotizacionDTO, String canalVenta, Integer recuotificaFlag){
		return programaPagoDao.simulaEmitePoliza(cotizacionDTO, canalVenta, recuotificaFlag);
	}
	
	public Saldo obtenerSaldoPeriodoPago(BigDecimal idToCotizacion) {
		Saldo saldo = new Saldo();

		List<ToRecibo> recibosList = obtenerRecibosDeCotizacion(idToCotizacion);

		for (ToRecibo recibo : recibosList) {
			saldo.setImpPrimaNeta(saldo.getImpPrimaNeta().add(
					recibo.getImpPrimaNeta()));
			saldo.setImpRcgosPagoFR(saldo.getImpRcgosPagoFR().add(
					recibo.getImpRcgosPagoFR()));
			saldo.setImpDerechos(saldo.getImpDerechos().add(
					recibo.getImpDerechos()));
			saldo.setImpIVA(saldo.getImpIVA().add(recibo.getImpIVA()));
			saldo.setImpPrimaTotal(saldo.getImpPrimaTotal().add(
					recibo.getImpPrimaTotal()));
		}

		return saldo;
	}
	
	public Saldo obtenerSaldoProgoPagoIn(BigDecimal idProgPago) {
		Saldo saldo = new Saldo();

		List<ToRecibo> recibosList = getReciboDePrograma(idProgPago.longValue());

		for (ToRecibo recibo : recibosList) {
			saldo.setImpPrimaNeta(saldo.getImpPrimaNeta().add(recibo.getImpPrimaNeta()));
			saldo.setImpRcgosPagoFR(saldo.getImpRcgosPagoFR().add(recibo.getImpRcgosPagoFR()));
			saldo.setImpDerechos(saldo.getImpDerechos().add(recibo.getImpDerechos()));
			saldo.setImpIVA(saldo.getImpIVA().add(recibo.getImpIVA()));
			saldo.setImpPrimaTotal(saldo.getImpPrimaTotal().add(recibo.getImpPrimaTotal()));
		}

		return saldo;
	}
	
	public List<ToRecibo> obtenerRecibosDeCotizacion(BigDecimal idToCotizacion) {
		List<ToRecibo> resList = new ArrayList<ToRecibo>();

		List<ToProgPago> pagoList = getProgramaPago(idToCotizacion);


		for (ToProgPago programa : pagoList) {
			List<ToRecibo> reciboList = getRecibo(idToCotizacion, programa
					.getId());
			resList.addAll(reciboList);
		}

		return resList;
	}
	
	
	public Saldo obtenerSaldoDiferencia(CotizacionDTO cotizacionDTO) {

		Saldo saldoDiferencia = new Saldo();

		// Obtenemos el saldo de periodo de pago
		Saldo saldoActual = this.obtenerSaldoPeriodoPago(cotizacionDTO
				.getIdToCotizacion());
		// Obtenemos el saldo original, primera linea
		Saldo saldoOriginal = this.obtenerSaldoOriginal(cotizacionDTO, null, null);

		saldoDiferencia.setImpBomoProm(saldoOriginal.getImpBomoProm().subtract(
				saldoActual.getImpBomoProm()));
		saldoDiferencia.setImpBonComis(saldoOriginal.getImpBonComis().subtract(
				saldoActual.getImpBonComis()));
		saldoDiferencia.setImpBonComPN(saldoOriginal.getImpBonComPN().subtract(
				saldoActual.getImpBonComPN()));
		saldoDiferencia.setImpBonComRPF(saldoOriginal.getImpBonComRPF()
				.subtract(saldoActual.getImpBonComRPF()));
		saldoDiferencia.setImpBonoAgt(saldoOriginal.getImpBonoAgt().subtract(
				saldoActual.getImpBonoAgt()));
		saldoDiferencia.setImpCesionDerechosAgt(saldoOriginal
				.getImpCesionDerechosAgt().subtract(
						saldoActual.getImpCesionDerechosAgt()));
		saldoDiferencia.setImpCesionDerechosProm(saldoOriginal
				.getImpCesionDerechosProm().subtract(
						saldoActual.getImpCesionDerechosProm()));
		saldoDiferencia.setImpComAgt(saldoOriginal.getImpComAgt().subtract(
				saldoActual.getImpComAgt()));
		saldoDiferencia.setImpComAgtPN(saldoOriginal.getImpComAgtPN().subtract(
				saldoActual.getImpComAgtPN()));
		saldoDiferencia.setImpComAgtRPF(saldoOriginal.getImpComAgtRPF()
				.subtract(saldoActual.getImpComAgtRPF()));
		saldoDiferencia.setImpComSup(saldoOriginal.getImpComSup().subtract(
				saldoActual.getImpComSup()));
		saldoDiferencia.setImpComSupPN(saldoOriginal.getImpComSupPN().subtract(
				saldoActual.getImpComSupPN()));
		saldoDiferencia.setImpComSupRPF(saldoOriginal.getImpComSupRPF()
				.subtract(saldoActual.getImpComSupRPF()));
		saldoDiferencia.setImpDerechos(saldoOriginal.getImpDerechos().subtract(
				saldoActual.getImpDerechos()));
		saldoDiferencia.setImpIVA(saldoOriginal.getImpIVA().subtract(
				saldoActual.getImpIVA()));
		saldoDiferencia.setImpOtrosImptos(saldoOriginal.getImpOtrosImptos()
				.subtract(saldoActual.getImpOtrosImptos()));
		saldoDiferencia.setImpPrimaNeta(saldoOriginal.getImpPrimaNeta()
				.subtract(saldoActual.getImpPrimaNeta()));
		saldoDiferencia.setImpPrimaTotal(saldoOriginal.getImpPrimaTotal()
				.subtract(saldoActual.getImpPrimaTotal()));
		saldoDiferencia.setImpRcgosPagoFR(saldoOriginal.getImpRcgosPagoFR()
				.subtract(saldoActual.getImpRcgosPagoFR()));
		saldoDiferencia.setImpSobreComAgt(saldoOriginal.getImpSobreComAgt()
				.subtract(saldoActual.getImpSobreComAgt()));
		saldoDiferencia.setImpSobreComProm(saldoOriginal.getImpSobreComProm()
				.subtract(saldoActual.getImpSobreComProm()));
		saldoDiferencia.setImpSobreComUdiAgt(saldoOriginal
				.getImpSobreComUdiAgt().subtract(
						saldoActual.getImpSobreComUdiAgt()));
		saldoDiferencia.setImpSobreComUdiProm(saldoOriginal
				.getImpSobreComUdiProm().subtract(
						saldoActual.getImpSobreComUdiProm()));

		return saldoDiferencia;
	}
	
	public Saldo obtenerSaldoDiferenciaInciso(BigDecimal idToprogPago) {

		Saldo saldoDiferencia = new Saldo();
		ToProgPago programaPago = entidadService.findById(ToProgPago.class, idToprogPago.longValue());
				
		// Obtenemos el saldo del de los recibos del inciso
		Saldo saldoActual = this.obtenerSaldoRecibosIn(idToprogPago);

		// Obtenemos el saldo original del inciso
		//Saldo saldoOriginal = this.obtenerSaldoOriginal(programaPago.getCotizacion(),null, programaPago.getIdReferencia());
		Saldo saldoOriginal = this.obtenerSaldoOriginalPorInciso(programaPago.getCotizacion(),null, programaPago.getNumInciso().longValue());
		
		
		saldoDiferencia.setImpBomoProm(saldoOriginal.getImpBomoProm().subtract(saldoActual.getImpBomoProm()));
		saldoDiferencia.setImpBonComis(saldoOriginal.getImpBonComis().subtract(saldoActual.getImpBonComis()));
		saldoDiferencia.setImpBonComPN(saldoOriginal.getImpBonComPN().subtract(saldoActual.getImpBonComPN()));
		saldoDiferencia.setImpBonComRPF(saldoOriginal.getImpBonComRPF().subtract(saldoActual.getImpBonComRPF()));
		saldoDiferencia.setImpBonoAgt(saldoOriginal.getImpBonoAgt().subtract(saldoActual.getImpBonoAgt()));
		saldoDiferencia.setImpCesionDerechosAgt(saldoOriginal.getImpCesionDerechosAgt().subtract(saldoActual.getImpCesionDerechosAgt()));
		saldoDiferencia.setImpCesionDerechosProm(saldoOriginal.getImpCesionDerechosProm().subtract(saldoActual.getImpCesionDerechosProm()));
		saldoDiferencia.setImpComAgt(saldoOriginal.getImpComAgt().subtract(saldoActual.getImpComAgt()));
		saldoDiferencia.setImpComAgtPN(saldoOriginal.getImpComAgtPN().subtract(saldoActual.getImpComAgtPN()));
		saldoDiferencia.setImpComAgtRPF(saldoOriginal.getImpComAgtRPF().subtract(saldoActual.getImpComAgtRPF()));
		saldoDiferencia.setImpComSup(saldoOriginal.getImpComSup().subtract(saldoActual.getImpComSup()));
		saldoDiferencia.setImpComSupPN(saldoOriginal.getImpComSupPN().subtract(saldoActual.getImpComSupPN()));
		saldoDiferencia.setImpComSupRPF(saldoOriginal.getImpComSupRPF().subtract(saldoActual.getImpComSupRPF()));
		saldoDiferencia.setImpDerechos(saldoOriginal.getImpDerechos().subtract(saldoActual.getImpDerechos()));
		saldoDiferencia.setImpIVA(saldoOriginal.getImpIVA().subtract(saldoActual.getImpIVA()));
		saldoDiferencia.setImpOtrosImptos(saldoOriginal.getImpOtrosImptos().subtract(saldoActual.getImpOtrosImptos()));
		saldoDiferencia.setImpPrimaNeta(saldoOriginal.getImpPrimaNeta().subtract(saldoActual.getImpPrimaNeta()));
		saldoDiferencia.setImpPrimaTotal(saldoOriginal.getImpPrimaTotal().subtract(saldoActual.getImpPrimaTotal()));
		saldoDiferencia.setImpRcgosPagoFR(saldoOriginal.getImpRcgosPagoFR().subtract(saldoActual.getImpRcgosPagoFR()));
		saldoDiferencia.setImpSobreComAgt(saldoOriginal.getImpSobreComAgt().subtract(saldoActual.getImpSobreComAgt()));
		saldoDiferencia.setImpSobreComProm(saldoOriginal.getImpSobreComProm().subtract(saldoActual.getImpSobreComProm()));
		saldoDiferencia.setImpSobreComUdiAgt(saldoOriginal.getImpSobreComUdiAgt().subtract(saldoActual.getImpSobreComUdiAgt()));
		saldoDiferencia.setImpSobreComUdiProm(saldoOriginal.getImpSobreComUdiProm().subtract(saldoActual.getImpSobreComUdiProm()));

		return saldoDiferencia;
	}
	
	
	
	public void guardar(List<ToRecibo> recibos, Long progPago) {
		// TODO Auto-generated method stub
		List<ToRecibo> listEditada = recibos;
		// Obtener el ID Cotizacion

		ToProgPago programaPagoOr = this.getProgramaPago(progPago);
		List<ToRecibo> listOriginal = programaPagoOr.getRecibos();

		ToProgPago programaPago = new ToProgPago();
		String codigoUsuarioCreacion = null;
		Date fechaRegistro = null;
		String cveTipoDocumento = null;
		String cveRecibo = null;
		String sitRecibo = null;

		for (ToRecibo l1 : listEditada) {
			int flag = 0;
			for (ToRecibo l2 : listOriginal) {

				programaPago = l2.getProgPago();
				codigoUsuarioCreacion = l2.getCodigoUsuarioCreacion();
				fechaRegistro = l2.getFechaRegistro();
				cveTipoDocumento = l2.getCveTipoDocumento();
				cveRecibo = l2.getCveRecibo();
				sitRecibo = l2.getSitRecibo();

				if (l2.getId().equals(l1.getId())) {
					if (l1.equals(l2)) {
						flag = 2; //
						LogDeMidasEJB3.log("no hacemos nada ya que el id es igual " + l1.getId(), Level.INFO, null);
						break;
					} else {
						flag = 1; //
						LogDeMidasEJB3.log("vamos a hacer el update del ids "
								+ l1.getId(), Level.INFO, null);
						l1.setProgPago(programaPago);
						l1.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
						l1.setFechaRegistro(fechaRegistro);
						l1.setCveTipoDocumento(cveTipoDocumento);
						l1.setCveRecibo(cveRecibo);
						l1.setSitRecibo(sitRecibo);

						l1.setImpBomoProm(BigDecimal.ZERO);
						l1.setImpBonComis(BigDecimal.ZERO);
						l1.setImpBonComPN(BigDecimal.ZERO);
						l1.setImpBonComRPF(BigDecimal.ZERO);
						l1.setImpBonoAgt(BigDecimal.ZERO);
						l1.setImpCesionDerechosAgt(BigDecimal.ZERO);
						l1.setImpCesionDerechosProm(BigDecimal.ZERO);
						l1.setImpComAgt(BigDecimal.ZERO);
						l1.setImpComAgtPN(BigDecimal.ZERO);
						l1.setImpComAgtRPF(BigDecimal.ZERO);
						l1.setImpComSup(BigDecimal.ZERO);
						l1.setImpComSupPN(BigDecimal.ZERO);
						l1.setImpComSupRPF(BigDecimal.ZERO);
						l1.setImpOtrosImptos(BigDecimal.ZERO);
						l1.setImpSobreComAgt(BigDecimal.ZERO);
						l1.setImpSobreComProm(BigDecimal.ZERO);
						l1.setImpSobreComUdiAgt(BigDecimal.ZERO);
						l1.setImpSobreComUdiProm(BigDecimal.ZERO);

						this.guardar(l1);
						// dao.update(l1);
						break;
					}
				}
				//si no son iguales, pero tienen el mismo Numero exhibicion
				else{
					if (l1.getNumExhibicion().intValue() == l2.getNumExhibicion().intValue()){
						
						LogDeMidasEJB3.log("vamos a actualizar el id"+ l2.getId() + "con: " + l1.getId(), Level.INFO, null);
						//Actualizamos ese registro
						l1.setProgPago(programaPago);
						l1.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
						l1.setFechaRegistro(fechaRegistro);
						l1.setCveTipoDocumento(cveTipoDocumento);
						l1.setCveRecibo(cveRecibo);
						l1.setSitRecibo(sitRecibo);
						l1.setId(l2.getId());
						l1.setNumExhibicion(l2.getNumExhibicion());

						l1.setImpBomoProm(BigDecimal.ZERO);
						l1.setImpBonComis(BigDecimal.ZERO);
						l1.setImpBonComPN(BigDecimal.ZERO);
						l1.setImpBonComRPF(BigDecimal.ZERO);
						l1.setImpBonoAgt(BigDecimal.ZERO);
						l1.setImpCesionDerechosAgt(BigDecimal.ZERO);
						l1.setImpCesionDerechosProm(BigDecimal.ZERO);
						l1.setImpComAgt(BigDecimal.ZERO);
						l1.setImpComAgtPN(BigDecimal.ZERO);
						l1.setImpComAgtRPF(BigDecimal.ZERO);
						l1.setImpComSup(BigDecimal.ZERO);
						l1.setImpComSupPN(BigDecimal.ZERO);
						l1.setImpComSupRPF(BigDecimal.ZERO);
						l1.setImpOtrosImptos(BigDecimal.ZERO);
						l1.setImpSobreComAgt(BigDecimal.ZERO);
						l1.setImpSobreComProm(BigDecimal.ZERO);
						l1.setImpSobreComUdiAgt(BigDecimal.ZERO);
						l1.setImpSobreComUdiProm(BigDecimal.ZERO);
						
						this.guardar(l1);
					}	
				}
				
			}
			/*
			if (flag == 0) {

				l1.setProgPago(programaPago);
				l1.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
				l1.setFechaRegistro(fechaRegistro);
				l1.setCveTipoDocumento(cveTipoDocumento);
				l1.setCveRecibo(cveRecibo);
				l1.setSitRecibo(sitRecibo);
				l1.setId(null);

				l1.setImpBomoProm(BigDecimal.ZERO);
				l1.setImpBonComis(BigDecimal.ZERO);
				l1.setImpBonComPN(BigDecimal.ZERO);
				l1.setImpBonComRPF(BigDecimal.ZERO);
				l1.setImpBonoAgt(BigDecimal.ZERO);
				l1.setImpCesionDerechosAgt(BigDecimal.ZERO);
				l1.setImpCesionDerechosProm(BigDecimal.ZERO);
				l1.setImpComAgt(BigDecimal.ZERO);
				l1.setImpComAgtPN(BigDecimal.ZERO);
				l1.setImpComAgtRPF(BigDecimal.ZERO);
				l1.setImpComSup(BigDecimal.ZERO);
				l1.setImpComSupPN(BigDecimal.ZERO);
				l1.setImpComSupRPF(BigDecimal.ZERO);
				l1.setImpOtrosImptos(BigDecimal.ZERO);
				l1.setImpSobreComAgt(BigDecimal.ZERO);
				l1.setImpSobreComProm(BigDecimal.ZERO);
				l1.setImpSobreComUdiAgt(BigDecimal.ZERO);
				l1.setImpSobreComUdiProm(BigDecimal.ZERO);

				this.guardar(l1);
				// ORA-01400: cannot insert NULL into
				// ("MIDAS"."TORECIBO"."PROGPAGO_ID")
			}
			*/
		}

		for (ToRecibo l2 : listOriginal) {
			int flag = 0;
			for (ToRecibo l1 : listEditada) {
				if (l2.getId().equals(l1.getId()) || l2.getNumExhibicion() == l1.getNumExhibicion()) {
					flag = 2;
					break;
				}
			}

			if (flag == 0) {
				LogDeMidasEJB3.log("vamos a eliminar el id " + l2.getId(), Level.INFO, null);
				// dao.delete(l2);
				this.eliminarRecibo(l2.getId());
			}
		}
		
		for (ToRecibo reciboEditado : listEditada){
			boolean existeRegistroEditado = false;
			for (ToRecibo reciboOriginal : listOriginal){
				
				if (reciboEditado.getId().equals(reciboOriginal.getId())){
					existeRegistroEditado = true;
					break;
				}

			}
			if (!existeRegistroEditado){
				LogDeMidasEJB3.log("vamos a insertar el id " + reciboEditado.getId(), Level.INFO, null);
				reciboEditado.setProgPago(programaPago);
				reciboEditado.setCodigoUsuarioCreacion(codigoUsuarioCreacion);
				reciboEditado.setFechaRegistro(fechaRegistro);
				reciboEditado.setCveTipoDocumento(cveTipoDocumento);
				reciboEditado.setCveRecibo(cveRecibo);
				reciboEditado.setSitRecibo(sitRecibo);
				reciboEditado.setId(null);

				reciboEditado.setImpBomoProm(BigDecimal.ZERO);
				reciboEditado.setImpBonComis(BigDecimal.ZERO);
				reciboEditado.setImpBonComPN(BigDecimal.ZERO);
				reciboEditado.setImpBonComRPF(BigDecimal.ZERO);
				reciboEditado.setImpBonoAgt(BigDecimal.ZERO);
				reciboEditado.setImpCesionDerechosAgt(BigDecimal.ZERO);
				reciboEditado.setImpCesionDerechosProm(BigDecimal.ZERO);
				reciboEditado.setImpComAgt(BigDecimal.ZERO);
				reciboEditado.setImpComAgtPN(BigDecimal.ZERO);
				reciboEditado.setImpComAgtRPF(BigDecimal.ZERO);
				reciboEditado.setImpComSup(BigDecimal.ZERO);
				reciboEditado.setImpComSupPN(BigDecimal.ZERO);
				reciboEditado.setImpComSupRPF(BigDecimal.ZERO);
				reciboEditado.setImpOtrosImptos(BigDecimal.ZERO);
				reciboEditado.setImpSobreComAgt(BigDecimal.ZERO);
				reciboEditado.setImpSobreComProm(BigDecimal.ZERO);
				reciboEditado.setImpSobreComUdiAgt(BigDecimal.ZERO);
				reciboEditado.setImpSobreComUdiProm(BigDecimal.ZERO);

				this.guardar(reciboEditado);
			}
		}
		
	}

	//Metodo me devuelve el saldo del programa de pago seleccionado
	public Saldo obtenerSaldoRecibosIn(BigDecimal idProgPago){
		
		Saldo saldo = new Saldo();
		ToProgPago progPago = entidadService.findById(ToProgPago.class, idProgPago.longValue());
		
		//List<ToRecibo> recibosList = obtenerRecibosDeInciso(progPago.getCotizacion().getIdToCotizacion(),progPago.getIdReferencia());
		List<ToRecibo> recibosList = obtenerRecibosDeInciso(progPago.getCotizacion().getIdToCotizacion(),progPago.getNumInciso().longValue());

		
		for (ToRecibo recibo : recibosList) {
			saldo.setImpPrimaNeta(saldo.getImpPrimaNeta().add(
					recibo.getImpPrimaNeta()));
			saldo.setImpRcgosPagoFR(saldo.getImpRcgosPagoFR().add(
					recibo.getImpRcgosPagoFR()));
			saldo.setImpDerechos(saldo.getImpDerechos().add(
					recibo.getImpDerechos()));
			saldo.setImpIVA(saldo.getImpIVA().add(recibo.getImpIVA()));
			saldo.setImpPrimaTotal(saldo.getImpPrimaTotal().add(
					recibo.getImpPrimaTotal()));
		}

		return saldo;
	}
	
	public List<ToRecibo> obtenerRecibosDeInciso(BigDecimal idToCotizacion, Long idReferencia) {
		List<ToRecibo> resList = new ArrayList<ToRecibo>();

		List<ToProgPago> pagoList = getProgramaPago(idToCotizacion);

		for (ToProgPago programa : pagoList) {
			if (programa.getNumInciso().compareTo(new BigDecimal(idReferencia)) == 0){
			//if(programa.getIdReferencia().equals(idReferencia)){
				List<ToRecibo> reciboList = getRecibo(idToCotizacion, programa.getId());
				resList.addAll(reciboList);
			}
		}
		return resList;
	}
	

//	@Override
//	public Saldo obtenerSaldoTotal(BigDecimal idCotizacion) {
//		// TODO Auto-generated method stub
//		return null;
//	}
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean validarPrevioDeRecibos(BigDecimal idToCotizacion) {		
		boolean result = false;
		List<ToProgPago> res = entidadService.findByProperty(ToProgPago.class, "cotizacion.idToCotizacion", idToCotizacion);
		if (res != null){
			if (res.size() > 0){
				result = true;
			}
		}
		return result;
	}
	
	@Override
	public boolean validaRolRecuotificacion(String userName, String rol){
		boolean tieneRol = false;
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper("MIDAS.PKG_RECUOTIFICACION.validaUsuarioRol");
			storedHelper.estableceParametro("nombreUsuario", userName);
			storedHelper.estableceParametro("nombreRol", rol);
			Integer datosModificados = Integer.valueOf(storedHelper.ejecutaActualizar());
			if (datosModificados.intValue() > 0){
				tieneRol = true;
			}
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion al validar el rol del usuario: " + e.getMessage(), Level.WARNING, null);
			return false;
		}
		return tieneRol;
	}
	
	@Override
	public Integer actualizarClienteInciso(String identificador, BigDecimal idInciso, BigDecimal idCliente, String nombreCliente){
		Integer res = -1;
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper("MIDAS.PKGAUT_RECUOTIFICAII.actualizarClienteInciso");
			storedHelper.estableceParametro("inIdentificador", identificador);
			storedHelper.estableceParametro("inIdInciso", idInciso);
			storedHelper.estableceParametro("inIdCliente", idCliente);
			storedHelper.estableceParametro("inNombreCliente", nombreCliente);			
			
			res = Integer.valueOf(storedHelper.ejecutaActualizar());
			return res;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion al actualizar inciso con datos del cliente: " + e.getMessage(), Level.WARNING, null);
			return -1;
		}
	}
	
	@Override
	public Integer obtenerParametroGeneral(BigDecimal idGrupoParametro, BigDecimal codigoParametroGeneral){
		Integer res = -1;
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper("MIDAS.PKG_RECUOTIFICACION.obtenerParametroGeneral");
			storedHelper.estableceParametro("inIdGrupoParametroGeneral", idGrupoParametro);
			storedHelper.estableceParametro("inCodigoParametroGeneral", codigoParametroGeneral);		
			
			res = Integer.valueOf(storedHelper.ejecutaActualizar());
			return res;
		} catch (Exception e) {
			LogDeMidasEJB3.log("Excepcion al obtener el valor del parametro general: " + e.getMessage(), Level.INFO, null);
			return -1;
		}
	}
	
	/************************* DCR ***************************/
	
	private void actualizarBanderaRecuotificacionManual(BigDecimal idToCotizacion, boolean estatus){
		CotizacionDTO cotizacion  = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.setRecuotificadaManual(estatus);
		entidadService.save(cotizacion);
	}
	

}
