package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cobranzainciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.complementar.cobranzaInciso.CobranzaIncisoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;

@Stateless
public class CobranzaIncisoServiceImpl implements CobranzaIncisoService{

	private EntidadService entidadService;
	private IncisoService incisoService;
	private ListadoService listadoService;
	private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@Override
	public Long getCountCobranzaIncisos(IncisoCotizacionDTO inciso) {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Boolean habilitaCobranzaInciso(BigDecimal cotizacionId){
		Boolean habilita = false;
		
		if(cotizacionId != null){
			CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, cotizacionId);			
			if(cotizacionDTO != null 
					&& cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas().intValue() == 1
					&& (cotizacionDTO.getTipoAgrupacionRecibos() == null || 
							cotizacionDTO.getTipoAgrupacionRecibos().equals(CotizacionDTO.TIPO_AGRUPACION_RECIBOS.UBICACION.getValue()))){				
				try{
					if(cotizacionDTO.getSolicitudDTO().getNegocio().getAplicaReciboGlobal() != null && cotizacionDTO.getSolicitudDTO().getNegocio().getAplicaReciboGlobal()){
						habilita = true;
					}	
				}catch(Exception e){					
				}
			}
		}
		
		return habilita;
	}

	@Override
	public List<CobranzaIncisoDTO> getCobranzaIncisos(IncisoCotizacionDTO incisoCotizacion) {
		List<CobranzaIncisoDTO> incisosCob = new ArrayList<CobranzaIncisoDTO>(1); 
		
		List<IncisoCotizacionDTO> listado = incisoService.listarIncisosConFiltro(incisoCotizacion);
		boolean isFirst = true;
		BigDecimal idContratante = null;
		for(IncisoCotizacionDTO inciso : listado){
			
			if(isFirst){
				CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, inciso.getId().getIdToCotizacion());
				idContratante = cotizacion.getIdToPersonaContratante();
				isFirst = false;
			}
			
			List<MedioPagoDTO> medioPagoList = new ArrayList<MedioPagoDTO>(1);
			Map<Long, String> conductosDeCobro = new HashMap<Long, String>();

			CobranzaIncisoDTO conInciso = new CobranzaIncisoDTO();
			conInciso.setInciso(inciso);
			if(inciso.getIdClienteCob() == null && inciso.getIncisoAutoCot().getPersonaAseguradoId() == null){
				inciso.setIdClienteCob(idContratante);
			}else if(inciso.getIdClienteCob() == null && inciso.getIncisoAutoCot().getPersonaAseguradoId() != null){
				inciso.setIdClienteCob(new BigDecimal(inciso.getIncisoAutoCot().getPersonaAseguradoId()));
			}
			if(inciso.getIdClienteCob() != null){
				medioPagoList = getMedioPagosView();				
				if(inciso.getIdMedioPago() != null){
					conductosDeCobro = listadoService.getConductosCobro(
							inciso.getIdClienteCob().longValue(),
							inciso.getIdMedioPago().intValue());
				}
			}else{
				MedioPagoDTO medioPago = new MedioPagoDTO();
				medioPago.setDescripcion("NO DISPONIBLE");
				medioPagoList.add(medioPago);
			}
			conInciso.setMediosPago(medioPagoList);
			conInciso.setConductosCobro(conductosDeCobro);
			BigDecimal idAsegurado = null;
			if(inciso.getIncisoAutoCot().getPersonaAseguradoId() != null){
				idAsegurado = new BigDecimal(inciso.getIncisoAutoCot().getPersonaAseguradoId());
			}
			conInciso.setClientesCobro(getClientesCobro(idContratante, idAsegurado));
			
			incisosCob.add(conInciso);
		}
		return incisosCob;
	}
	
	public Map<BigDecimal, String> getClientesCobro(BigDecimal idContrantante, BigDecimal idAsegurado){
		Map<BigDecimal, String> clientesCobro = new HashMap<BigDecimal, String>();
		
		if(idContrantante != null){
			ClienteGenericoDTO cliente = new ClienteGenericoDTO();
			cliente.setIdCliente(idContrantante);
			try {
				cliente = clienteFacadeRemote.loadByIdNoAddress(cliente);
				if(cliente != null){
					clientesCobro.put(cliente.getIdCliente(), cliente.getNombreCompleto());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		}
		if(idAsegurado != null){
			ClienteGenericoDTO cliente = new ClienteGenericoDTO();
			cliente.setIdCliente(idAsegurado);
			try {
				cliente = clienteFacadeRemote.loadByIdNoAddress(cliente);
				if(cliente != null){
					clientesCobro.put(cliente.getIdCliente(), cliente.getNombreCompleto());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return clientesCobro;
	}
	
	@Override
	public Map<BigDecimal, String> getClientesCobroCotizacion(BigDecimal idCotizacion){
		Map<BigDecimal, String> clientesCobro = new HashMap<BigDecimal, String>();
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
		BigDecimal idContratante = cotizacion.getIdToPersonaContratante();
		
		if(idContratante != null){
			ClienteGenericoDTO cliente = new ClienteGenericoDTO();
			cliente.setIdCliente(idContratante);
			try {
				cliente = clienteFacadeRemote.loadByIdNoAddress(cliente);
				if(cliente != null){
					clientesCobro.put(cliente.getIdCliente(), cliente.getNombreCompleto());
				}
			} catch (Exception e) {
				e.printStackTrace();
			}		
		}		
		IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
		IncisoCotizacionId id = new IncisoCotizacionId();
		id.setIdToCotizacion(idCotizacion);
		incisoCotizacion.setId(id);
		List<IncisoCotizacionDTO> listado = incisoService.listarIncisosConFiltro(incisoCotizacion);
		for(IncisoCotizacionDTO inciso : listado){
			Long idAseguradol = inciso.getIncisoAutoCot().getPersonaAseguradoId();
			if(idAseguradol != null){
				BigDecimal idAsegurado = new BigDecimal(idAseguradol);
				if(!clientesCobro.containsKey(idAsegurado)){
					ClienteGenericoDTO cliente = new ClienteGenericoDTO();
					cliente.setIdCliente(idAsegurado);
					try {
						cliente = clienteFacadeRemote.loadByIdNoAddress(cliente);
						if(cliente != null){
							clientesCobro.put(cliente.getIdCliente(), cliente.getNombreCompleto());
						}
					} catch (Exception e) {
						e.printStackTrace();
					}
				}
			}
		}
		return clientesCobro;
	}	
	
	public List<MedioPagoDTO> getMedioPagosView(){
		List<MedioPagoDTO> list = new ArrayList<MedioPagoDTO>(1);
		//list="#{'8':'COBRANZA DOMICILIADA','4':'TARJETA DE CREDITO','1386':'CUENTA AFIRME (CREDITO O DEBITO)','15':'EFECTIVO'}"
		list.add(new MedioPagoDTO(8, "COBRANZA DOMICILIADA"));
		list.add(new MedioPagoDTO(4, "TARJETA DE CREDITO"));
		list.add(new MedioPagoDTO(1386, "CUENTA AFIRME (CREDITO O DEBITO)"));
		list.add(new MedioPagoDTO(15, "EFECTIVO"));		
		
		return list;
	}

	@Override
	public void aplicarCobranzaIncisos(List<IncisoCotizacionDTO> incisos,
			BigDecimal idToCotizacion, BigDecimal idMedioPago, Long idConductoCobro, Boolean todos) {
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if(todos != null && todos){
			incisos = entidadService.findByProperty(IncisoCotizacionDTO.class, "id.idToCotizacion", idToCotizacion);
		}
		if(cotizacion.getIdMedioPago() == null){
			try{			
				cotizacion.setIdMedioPago(new BigDecimal(CuentaPagoDTO.TipoCobro.EFECTIVO.valor()));
				entidadService.save(cotizacion);
			}catch(Exception e){
			}
		}
		
		for(IncisoCotizacionDTO inciso: incisos){
			try{
				if(todos == null || !todos){
					IncisoCotizacionId id = new IncisoCotizacionId();
					id.setIdToCotizacion(idToCotizacion);
					id.setNumeroInciso(inciso.getId().getNumeroInciso());
					inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
				}
				inciso.setIdMedioPago(idMedioPago);
				inciso.setIdConductoCobroCliente(idConductoCobro);
				inciso.setIdClienteCob(cotizacion.getIdToPersonaContratante());
			
				entidadService.save(inciso);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	
	@Override
	public void aplicarCobranzaAseguradoIncisos(List<IncisoCotizacionDTO> incisos) {
		
		if(incisos != null && incisos.size() > 0){
			try{
				CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, incisos.get(0).getId().getIdToCotizacion());
				if(cotizacion.getIdMedioPago() == null){			
					cotizacion.setIdMedioPago(new BigDecimal(CuentaPagoDTO.TipoCobro.EFECTIVO.valor()));
					entidadService.save(cotizacion);
				}
			}catch(Exception e){
			}
		}
		for(IncisoCotizacionDTO inciso: incisos){
			try{
				IncisoCotizacionId id = new IncisoCotizacionId();
				id.setIdToCotizacion(inciso.getId().getIdToCotizacion());
				id.setNumeroInciso(inciso.getId().getNumeroInciso());
				IncisoCotizacionDTO incisoCot = entidadService.findById(IncisoCotizacionDTO.class, id);
					
				incisoCot.setIdMedioPago(inciso.getIdMedioPago());
				incisoCot.setIdConductoCobroCliente(inciso.getIdConductoCobroCliente());
				incisoCot.setIdClienteCob(inciso.getIdClienteCob());
			
				entidadService.save(incisoCot);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	
	@Override
	public void limpiarCobranzaIncisos(BigDecimal idToCotizacion){		
		List<IncisoCotizacionDTO> incisos = entidadService.findByProperty(IncisoCotizacionDTO.class, "id.idToCotizacion", idToCotizacion);
		
		for(IncisoCotizacionDTO inciso: incisos){
			try{
				inciso.setIdMedioPago(null);
				inciso.setIdConductoCobroCliente(null);
				inciso.setIdClienteCob(null);			
				entidadService.save(inciso);
			}catch(Exception e){
				e.printStackTrace();
			}
		}		
	}
	
	@Override
	public void validarCambioContratanteCobranzaInciso(BigDecimal idToCotizacion){			
		try{
			List<IncisoCotizacionDTO> incisos = entidadService.findByProperty(IncisoCotizacionDTO.class, "id.idToCotizacion", idToCotizacion);			
			for(IncisoCotizacionDTO inciso: incisos){
				validarCambioAseguradoCobranzaInciso(inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso());
			}
		}catch(Exception e){
			e.printStackTrace();
		}			
	}
	
	@Override
	public void validarCambioAseguradoCobranzaInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso){	
		
		IncisoCotizacionId id = new IncisoCotizacionId();
		id.setIdToCotizacion(idToCotizacion);
		id.setNumeroInciso(numeroInciso);
		try{
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);			
			IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
			
			if(inciso.getIdClienteCob() != null){
				if(!cotizacion.getIdToPersonaContratante().equals(inciso.getIdClienteCob())){
					if(inciso.getIncisoAutoCot().getPersonaAseguradoId() == null || 
							!inciso.getIdClienteCob().equals(inciso.getIncisoAutoCot().getPersonaAseguradoId())){
						inciso.setIdMedioPago(null);
						inciso.setIdConductoCobroCliente(null);
						inciso.setIdClienteCob(null);			
						entidadService.save(inciso);
					}						
				}		
			}
		}catch(Exception e){
			e.printStackTrace();
		}			
	}

}
