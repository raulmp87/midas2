package mx.com.afirme.midas2.service.impl.impresiones;

import java.io.InputStream;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import mx.com.afirme.midas2.domain.reportes.ReporteEmisionView;
import mx.com.afirme.midas2.dto.reportes.emision.BienAseguradoDTO;
import mx.com.afirme.midas2.dto.reportes.emision.DocumentoDTO;
import mx.com.afirme.midas2.dto.reportes.emision.ReciboDTO;
import mx.com.afirme.midas2.service.reportes.ReporteEmisionRestService;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.util.WriterUtil;

@Stateless
public class ReporteEmisionRestServiceImpl implements ReporteEmisionRestService {
	
	private JdbcTemplate jdbcTemplate;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
	
	@SuppressWarnings("unused")
	private DataSource dataSource;
	
	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) { 
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}
	
	
	public InputStream imprimirReporteEmision(ReporteEmisionView filtro) {
		try {

			final ExcelExporter documentoExcelExporter = 
					new ExcelExporter(DocumentoDTO.class);
			documentoExcelExporter.setSheetName("Documento");
			final List<DocumentoDTO> documentoDS = getDocumentos(filtro);
			final Workbook documentoWorkbook = documentoExcelExporter.getWorkbook(documentoDS);

			final ExcelExporter bienAseguradoExcelExporter = new ExcelExporter(BienAseguradoDTO.class);
			final List<BienAseguradoDTO> bienAseguradoDS = getBienAsegurado(filtro);
			final Workbook bienAseguradoWorkbook = bienAseguradoExcelExporter.getWorkbook(bienAseguradoDS);

			final ExcelExporter recibosExcelExporter = new ExcelExporter(ReciboDTO.class);
			final List<ReciboDTO> recibosDS = getRecibo(filtro);
			final Workbook recibosWorkbook = recibosExcelExporter.getWorkbook(recibosDS);
			
			final Sheet recibosSheet = documentoWorkbook.createSheet("Recibos");
			ExcelExporter.copySheets(recibosSheet, recibosWorkbook.getSheetAt(0));
			
			final Sheet bienAseguradoSheet = documentoWorkbook.createSheet("Bien Asegurado");
			ExcelExporter.copySheets(bienAseguradoSheet, bienAseguradoWorkbook.getSheetAt(0));
			
			return WriterUtil.writeWorkBook(documentoWorkbook);

		} catch (Exception e) {
			throw new RuntimeException(e);
		} 
	}
	

	@SuppressWarnings("unchecked")
	public List<DocumentoDTO> getDocumentos(ReporteEmisionView filtro) {
		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("MIDAS")
				.withCatalogName("PKGAUT_REPORTE_EMISION_REST")
				.withProcedureName("get_documentos")
				.declareParameters(
					new SqlParameter("pFechaInicial", Types.DATE),
					new SqlParameter("pFechaFinal", Types.DATE),
					new SqlParameter("pClaveAgente", Types.NUMERIC))
                .returningResultSet("pCursor",
                        new BeanPropertyRowMapper<DocumentoDTO>(DocumentoDTO.class));
		
		SqlParameterSource parameter = null;
		try {
			parameter = new MapSqlParameterSource("pFechaInicial",
					new java.sql.Date(sdf.parse(filtro.getFechaInicial()).getTime()))
							.addValue("pFechaFinal", new java.sql.Date(sdf.parse(filtro.getFechaFinal()).getTime()))
							.addValue("pClaveAgente", filtro.getClaveAgente());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return (List<DocumentoDTO>) simpleJdbcCall.execute(parameter).get("pCursor");

	}
	
	
	public List<BienAseguradoDTO> getBienAsegurado(ReporteEmisionView filtro) {

		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("MIDAS")
				.withCatalogName("PKGAUT_REPORTE_EMISION_REST")
				.withProcedureName("get_bien_asegurado")
				.declareParameters(
					new SqlParameter("pFechaInicial", Types.DATE),
					new SqlParameter("pFechaFinal", Types.DATE),
					new SqlParameter("pClaveAgente", Types.NUMERIC))
                .returningResultSet("pCursor",
                        new BeanPropertyRowMapper<BienAseguradoDTO>(BienAseguradoDTO.class));
		
		SqlParameterSource parameter = null;
		try {
			parameter = new MapSqlParameterSource("pFechaInicial",
					new java.sql.Date(sdf.parse(filtro.getFechaInicial()).getTime()))
							.addValue("pFechaFinal", new java.sql.Date(sdf.parse(filtro.getFechaFinal()).getTime()))
							.addValue("pClaveAgente", filtro.getClaveAgente());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return (List<BienAseguradoDTO>) simpleJdbcCall.execute(parameter).get("pCursor");

	
	}
	
	
	public List<ReciboDTO> getRecibo(ReporteEmisionView filtro) {


		SimpleJdbcCall simpleJdbcCall = new SimpleJdbcCall(jdbcTemplate)
                .withSchemaName("MIDAS")
				.withCatalogName("PKGAUT_REPORTE_EMISION_REST")
				.withProcedureName("get_recibo")
				.declareParameters(
					new SqlParameter("pFechaInicial", Types.DATE),
					new SqlParameter("pFechaFinal", Types.DATE),
					new SqlParameter("pClaveAgente", Types.NUMERIC))
                .returningResultSet("pCursor",
                        new BeanPropertyRowMapper<ReciboDTO>(ReciboDTO.class));
		
		SqlParameterSource parameter = null;
		try {
			parameter = new MapSqlParameterSource("pFechaInicial",
					new java.sql.Date(sdf.parse(filtro.getFechaInicial()).getTime()))
							.addValue("pFechaFinal", new java.sql.Date(sdf.parse(filtro.getFechaFinal()).getTime()))
							.addValue("pClaveAgente", filtro.getClaveAgente());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}

		return (List<ReciboDTO>) simpleJdbcCall.execute(parameter).get("pCursor");

	
	
	}

}
