package mx.com.afirme.midas2.service.impl.reaseguro.reporteRR6;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.danios.reportes.reporterr6.CargaRR6DTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dao.reaseguro.reporteRR6.ComplementoRR6Dao;
import mx.com.afirme.midas2.domain.reaseguro.reporteRR6.ReporteRARN;
import mx.com.afirme.midas2.domain.reaseguro.reporteRR6.ReporteRTRC;
import mx.com.afirme.midas2.domain.reaseguro.reporteRR6.ReporteRTRE;
import mx.com.afirme.midas2.domain.reaseguro.reporteRR6.ReporteRTRF;
import mx.com.afirme.midas2.domain.reaseguro.reporteRR6.ReporteRTRR;
import mx.com.afirme.midas2.domain.reaseguro.reporteRR6.ReporteRTRS;
import mx.com.afirme.midas2.domain.reaseguro.reporteRR6.ReporteRTRI;
import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.impl.ExcelConverter;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;
import mx.com.afirme.midas2.service.reaseguro.reporteRR6.ComplementoRR6Service;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class ComplementoRR6ServiceImpl implements ComplementoRR6Service {
	
	private InputStream is;
	
	private String resultado;
	private String extension;
	public static final String EXITOSO = "exitoso";
	public static final String ERROR = "error";
	public static final String EXISTENTE = "existente";
	public static final String ESTATUS = "CARGADO";
	public static final String ESTATCARGA = "bloqueado";
	public static final int ORDEN = 10;
	public static final int RTRE = 1;
	public static final int RTRC = 2;	
	public static final int RTRF = 3;
	public static final int RTRR = 4;
	public static final int RTRS = 5;
	public static final int RARN = 6;
	public static final int RTRI = 7;
	public static final int CPV = 1;
	public static final int CPF = 2;	
	public static final int CXL = 3;
	public static final int SIV = 4;
	public static final int CFD = 5;
	public static final int estatusCarga = 0;
	
	private List<ReporteRTRE> listaRTRE = new ArrayList<ReporteRTRE>();
	private List<ReporteRTRC> listaRTRC = new ArrayList<ReporteRTRC>();
	private List<ReporteRTRF> listaRTRF = new ArrayList<ReporteRTRF>();
	private List<ReporteRTRR> listaRTRR = new ArrayList<ReporteRTRR>();
	private List<ReporteRTRS> listaRTRS = new ArrayList<ReporteRTRS>();
	private List<ReporteRARN> listaRARN = new ArrayList<ReporteRARN>();
	private List<ReporteRTRI> listaRTRI = new ArrayList<ReporteRTRI>();
	private List<String> listaErrores = new ArrayList<String>();
	CargaRR6DTO cargaDTO;
	java.util.Date fCorte;
	
	private Date fechaCorte;	
			
	@EJB
	private EntidadHistoricoDao entidadService;

	@EJB
	private SistemaContext sistemaContext;

	@EJB
	private ComplementoRR6Dao complementoRR6Dao;

	/**
	 * Procesa la informacion de los reportes.
	 * @param idToControlArchivo
	 * @param tipoArchivo
	 * @param tipoContrato
	 * @param fechaCorte
	 * @param usuario
	 * @param accion
	 * @return resultado.
	 */		
	@Override
	public String procesarInfo(BigDecimal idToControlArchivo, String tipoArchivo, String tipoContrato, String fechaCorte, String usuario, String accion) {
		   
		   String reporte;
		   String negocio = "";
		   resultado = EXITOSO;
		   
			try {
				this.fechaCorte=getFechaFromString(fechaCorte);
				fCorte = Utilerias.obtenerFechaDeCadena(fechaCorte);
			} catch (ParseException ex) {
				LogDeMidasEJB3.log("Falla al parsear la fecha de corte", Level.SEVERE, ex);
			}
			
			
		switch(Integer.valueOf(tipoContrato)) {
			case (CPV):{
				negocio="PROPORCIONAL";
				break;
			}case (CPF):{
				negocio="XL";
				break;
			}case (CXL):{
				negocio="FACULTATIVO";
				break;
			}case (SIV):{
				negocio="SINIESTRO VIDA";
				break;
			}case (CFD):{
				negocio="FAC_DANOS";
				break;
			}
			
		}
			
		switch(Integer.valueOf(tipoArchivo)) {
			case (RTRE):{
				
			  int registros = complementoRR6Dao.getRegistros(fCorte, "MIDAS.CNSF_RR6_RTRE_CARGA", "fcorte", negocio);
			  int candado = complementoRR6Dao.estatusCarga(fCorte, "RTRE "+negocio);
			  
			  if (candado == estatusCarga){
				
				if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
					resultado = cargarRTRE(idToControlArchivo, negocio);
				}else if(registros > 0 && accion.equalsIgnoreCase("CARGA")){
					resultado = EXISTENTE;
				}else if(registros > 0 && accion.equalsIgnoreCase("RECARGA")){
					complementoRR6Dao.borraRegistros(fCorte, "MIDAS.CNSF_RR6_RTRE_CARGA", "fcorte", negocio);
					resultado = cargarRTRE(idToControlArchivo, negocio);
				}
				
				
				if(resultado.equalsIgnoreCase(EXITOSO)){
					
					if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
						reporte = "RTRE "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.insertaEstatusCarga(cargaDTO);
					}else{
						reporte = "RTRE "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.actualizaEstatusCarga(cargaDTO);
					}
				  }
			  }else{
					resultado = ESTATCARGA;
			  }
			  break;
		  }case (RTRC):{	
				
			int registros = complementoRR6Dao.getRegistros(fCorte, "MIDAS.CNSF_RR6_RTRC_CARGA", "fcorte", negocio);
		    int candado = complementoRR6Dao.estatusCarga(fCorte, "RTRC "+negocio);
				  
			if (candado == estatusCarga){
				
				if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
					resultado = cargarRTRC(idToControlArchivo, negocio);
				}else if(registros > 0 && accion.equalsIgnoreCase("CARGA")){
					resultado = EXISTENTE;
				}else if(registros > 0 && accion.equalsIgnoreCase("RECARGA")){
					complementoRR6Dao.borraRegistros(fCorte, "MIDAS.CNSF_RR6_RTRC_CARGA", "fcorte", negocio);
					resultado = cargarRTRC(idToControlArchivo, negocio);
				}
				
				if(resultado.equalsIgnoreCase(EXITOSO)){
					
					if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
						reporte = "RTRC "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.insertaEstatusCarga(cargaDTO);
					}else{
						reporte = "RTRC "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.actualizaEstatusCarga(cargaDTO);
					}
				}
			}else{
				resultado = ESTATCARGA;
			}
			break;
		}case (RTRF):{
				
			int registros = complementoRR6Dao.getRegistros(fCorte, "MIDAS.CNSF_RR6_RTRF_CARGA", "fechafincorte", negocio);
			int candado = complementoRR6Dao.estatusCarga(fCorte, "RTRF "+negocio);
				  
			if (candado == estatusCarga){
				if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
					resultado = cargarRTRF(idToControlArchivo, negocio);
				}else if(registros > 0 && accion.equalsIgnoreCase("CARGA")){
					resultado = EXISTENTE;
				}else if(registros > 0 && accion.equalsIgnoreCase("RECARGA")){
					complementoRR6Dao.borraRegistros(fCorte, "MIDAS.CNSF_RR6_RTRF_CARGA", "fechafincorte", negocio);
					resultado = cargarRTRF(idToControlArchivo, negocio);
				}
				
				if(resultado.equalsIgnoreCase(EXITOSO)){
					
					if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
						reporte = "RTRF "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.insertaEstatusCarga(cargaDTO);
					}else{
						reporte = "RTRF "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.actualizaEstatusCarga(cargaDTO);
					}
				}
			}else{
				resultado = ESTATCARGA;
			}
			break;
		}case (RTRR):{
				
			int registros = complementoRR6Dao.getRegistros(fCorte, "MIDAS.CNSF_RR6_RTRR_CARGA", "fechacorte", negocio);
			int candado = complementoRR6Dao.estatusCarga(fCorte, "RTRR "+negocio);
			  
			if (candado == estatusCarga){
			
				if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
					resultado = cargarRTRR(idToControlArchivo, negocio);
				}else if(registros > 0 && accion.equalsIgnoreCase("CARGA")){
					resultado = EXISTENTE;
				}else if(registros > 0 && accion.equalsIgnoreCase("RECARGA")){
					complementoRR6Dao.borraRegistros(fCorte, "MIDAS.CNSF_RR6_RTRR_CARGA", "fechacorte", negocio);
					resultado = cargarRTRR(idToControlArchivo, negocio);
				}
				
				if(resultado.equalsIgnoreCase(EXITOSO)){
					
					if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
						reporte = "RTRR "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.insertaEstatusCarga(cargaDTO);
					}else{
						reporte = "RTRR "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.actualizaEstatusCarga(cargaDTO);
					}
				}
			}else{
				resultado = ESTATCARGA;
			}
			break;
		}case (RTRS):{
				
			int registros = complementoRR6Dao.getRegistros(fCorte, "MIDAS.CNSF_RR6_RTRS_CARGA", "fcorte", negocio);
			int candado = complementoRR6Dao.estatusCarga(fCorte, "RTRS "+negocio);
			  
			if (candado == estatusCarga){
				
				if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
					resultado = cargarRTRS(idToControlArchivo, negocio);
				}else if(registros > 0 && accion.equalsIgnoreCase("CARGA")){
					resultado = EXISTENTE;
				}else if(registros > 0 && accion.equalsIgnoreCase("RECARGA")){
					complementoRR6Dao.borraRegistros(fCorte, "MIDAS.CNSF_RR6_RTRS_CARGA", "fcorte", negocio);
					resultado = cargarRTRS(idToControlArchivo, negocio);
				}
				
				if(resultado.equalsIgnoreCase(EXITOSO)){
					
					if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
						reporte = "RTRS "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.insertaEstatusCarga(cargaDTO);
					}else{
						reporte = "RTRS "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.actualizaEstatusCarga(cargaDTO);
					}
				}
			}else{
				resultado = ESTATCARGA;
			}
			break;
		}case (RARN):{
				
			int registros = complementoRR6Dao.getRegistros(fCorte, "MIDAS.CNSF_RR6_RARN_CARGA", "fechafincorte", negocio);
			int candado = complementoRR6Dao.estatusCarga(fCorte, "RARN "+negocio);
			  
			if (candado == estatusCarga){
				
				if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
					resultado = cargarRARN(idToControlArchivo, negocio);
				}else if(registros > 0 && accion.equalsIgnoreCase("CARGA")){
					resultado = EXISTENTE;
				}else if(registros > 0 && accion.equalsIgnoreCase("RECARGA")){
					complementoRR6Dao.borraRegistros(fCorte, "MIDAS.CNSF_RR6_RARN_CARGA", "fechafincorte", negocio);
					resultado = cargarRARN(idToControlArchivo, negocio);
				}
				
				if(resultado.equalsIgnoreCase(EXITOSO)){
					
					if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
						reporte = "RARN "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.insertaEstatusCarga(cargaDTO);
					}else{
						reporte = "RARN "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.actualizaEstatusCarga(cargaDTO);
					}
				}
			}else{
				resultado = ESTATCARGA;
			}
			break;
		}case (RTRI):{
			
			int registros = complementoRR6Dao.getRegistros(fCorte, "MIDAS.CNSF_RR6_RTRI_CARGA", "fechafincorte", negocio);
			int candado = complementoRR6Dao.estatusCarga(fCorte, "RTRI "+negocio);
			  
			if (candado == estatusCarga){
				
				if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
					resultado = cargarRTRI(idToControlArchivo, negocio);
				}else if(registros > 0 && accion.equalsIgnoreCase("CARGA")){
					resultado = EXISTENTE;
				}else if(registros > 0 && accion.equalsIgnoreCase("RECARGA")){
					complementoRR6Dao.borraRegistros(fCorte, "MIDAS.CNSF_RR6_RTRI_CARGA", "fechafincorte", negocio);
					resultado = cargarRTRI(idToControlArchivo, negocio);
				}
				
				if(resultado.equalsIgnoreCase(EXITOSO)){
					
					if (registros == 0 && accion.equalsIgnoreCase("CARGA")) {
						reporte = "RTRI "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.insertaEstatusCarga(cargaDTO);
					}else{
						reporte = "RTRI "+negocio;
						llenarCargaRR6DTO(fCorte,ESTATUS,reporte,usuario, idToControlArchivo);
						complementoRR6Dao.actualizaEstatusCarga(cargaDTO);
					}
				}
			}else{
				resultado = ESTATCARGA;
			}
			break;
		 }
			
	  }	
			// Regresa el resultado
			
			if(resultado.equalsIgnoreCase(ERROR) && listaErrores.size()>0){
					resultado = obtenerResultado(listaErrores);
								
				}	
						
			
			return resultado;
	}
	
	
	public String obtenerResultado(List<String> listaErrores) {
				StringBuilder result = new StringBuilder("");
				for(String errores : listaErrores) {
					result.append(errores).append("<br>");
				}		
				return result.toString();
		}
	
		/**
		 * Procesa la informacion del reporte RTRE.
		 * @param idToControlArchivo
		 * @param negocio
		 * @return resultado.
		 */		
		public String cargarRTRE(BigDecimal idToControlArchivo, String negocio){
				
			try {
			// Cargar el archivo
			is = getArchivoDatos(idToControlArchivo);
				
				
			listaRTRE = listarReporteRTRE();
			if(resultado.equals(EXITOSO)){			
					
					for (ReporteRTRE reporte : listaRTRE) {
							complementoRR6Dao.insertaReporteRTRE(
									reporte.getClave(), 
									reporte.getNegCubiertos(),
									reporte.getClaveEstrategia(), 
									reporte.getOrdenCobertura(),
									reporte.getIdentificador(), 
									reporte.getfCorte(),
									negocio);
							
						  }
					}
				
				
				} catch (RuntimeException ex) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
				}
				
			return resultado;
						
		}

			
		/**
		 * Procesa la informacion del reporte RTRC.
		 * @param idToControlArchivo
		 * @param negocio
		 * @return resultado.
		 */		
		public String cargarRTRC(BigDecimal idToControlArchivo, String negocio){
			
			try {
				// Cargar el archivo
			is = getArchivoDatos(idToControlArchivo);
				
				
				
			listaRTRC = listarReporteRTRC();
			if(resultado.equals(EXITOSO)){
				
				for (ReporteRTRC reporte : listaRTRC) {
							complementoRR6Dao.insertaReporteRTRC(
									reporte.getIdentificador(),
									reporte.getClave(), 
									reporte.getNegCubiertos(),
									reporte.getMoneda(), 
									reporte.getCaptura(),
									reporte.getModificados(), 
									reporte.getFechainicial(),
									reporte.getFechafinal(), 
									reporte.getTipocontrato(),
									reporte.getCapas(),
									reporte.getPorcionCedida(),
									reporte.getRetencionPrioridad(),
									reporte.getRetencionFianzas(),
									reporte.getCapacidadMaxima(),
									reporte.getCapMaxFianzas(),
									reporte.getImporte(),
									reporte.getComision(),
									reporte.getUtilidades(),
									reporte.getReasInscrito(),
									reporte.getTipoReas(),
									reporte.getReasNacional(),
									reporte.getNoInscrito(),
									reporte.getParticipacion(),
									reporte.getTipoIntermediario(),
									reporte.getId_Intermediario(),
									reporte.getIntermNoAutorizado(),
									reporte.getSuscriptor(),
									reporte.getAclaracion(),
									reporte.getfCorte(),
									negocio,
									ORDEN);
							
						  }
					}
				
				
				} catch (RuntimeException ex) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
				}
			
				return resultado;
						
		}
			
		/**
		 * Procesa la informacion del reporte RTRF.
		 * @param idToControlArchivo
		 * @param negocio
		 * @return resultado.
		 */		
		public String cargarRTRF(BigDecimal idToControlArchivo,String negocio){
				
			try {
			// Cargar el archivo
			is = getArchivoDatos(idToControlArchivo);
				
				
			listaRTRF = listarReporteRTRF();
			if(resultado.equals(EXITOSO)){
					
			for (ReporteRTRF reporte : listaRTRF) {
							complementoRR6Dao.insertaReporteRTRF(								
									reporte.getIdentificador(),
									reporte.getClave(), 
									reporte.getNegCubiertos(),
									reporte.getAsegurado(),
									reporte.getSumaAsegurada(),
									reporte.getMoneda(), 
									reporte.getPrimaEmitida(),
									reporte.getPrimaFacultada(), 
									reporte.getPrimaCedProporc(),
									reporte.getPrimaRetenida(), 
									reporte.getFechaInicial(),
									reporte.getFechaFinal(),
									reporte.getTipoContrato(),
									reporte.getCapas(),
									reporte.getRetPrioridad(),
									reporte.getRetpriorfian(),
									reporte.getCapacidadMaxReas(),
									reporte.getCapacidadMaxFian(),
									reporte.getComision(),
									reporte.getPartUtilidades(),
									reporte.getReasInscrito(),
									reporte.getTipoReas(),
									reporte.getClvReasNac(),
									reporte.getReasNoInscrito(),
									reporte.getParticipacionReas(),
									reporte.getTipoIntermediario(),
									reporte.getClvIntermediario(),
									reporte.getIntNoAutorizado(),
									reporte.getSuscriptor(),
									reporte.getAclaraciones(),
									reporte.getEntidades(),
									reporte.getMunicipio(),
									reporte.getSector(),
									reporte.getFechaFinCorte(),
									negocio);
							
						  }
					}
				
				
				} catch (RuntimeException ex) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
				}
			
				return resultado;
						
		}
			
		/**
		 * Procesa la informacion del reporte RTRR.
		 * @param idToControlArchivo
		 * @param negocio
		 * @return resultado.
		 */	
			public String cargarRTRR(BigDecimal idToControlArchivo, String negocio){
				
							
					try {
					// Cargar el archivo
					is = getArchivoDatos(idToControlArchivo);
							
							
						listaRTRR = listarReporteRTRR();
						if(resultado.equals(EXITOSO)){
															
						for (ReporteRTRR reporte : listaRTRR) {
										complementoRR6Dao.insertaReporteRTRR(								
												reporte.getIdentificador(),
												reporte.getTipoContrato(),
												reporte.getAnio(),
												reporte.getClave(),
												reporte.getIniciovigencia(),
												reporte.getComisiones(),
												reporte.getUtilidades(),
												reporte.getSinReclamaFac(),
												reporte.getSinReclamaNoProp(),
												reporte.getIngresos(),
												reporte.getOtrosIngresos(),
												reporte.getMontoPrima(),
												reporte.getCostoCobertura(),
												reporte.getPartSalvamento(),
												reporte.getRecursosRetenidos(),
												reporte.getReasFinanciero(),
												reporte.getCastigo(),
												reporte.getEgresos(),
												reporte.getGastosReas(),
												reporte.getAclaraciones(),
												reporte.getFechaCorte(),
												negocio);
										
									  }
								}
							
							
							} catch (RuntimeException ex) {
								resultado = ERROR;
								LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
							}
							
							
				return resultado;
									
			}
		
		
		/**
		* Procesa la informacion del reporte RTRS.
		* @param idToControlArchivo
		* @param negocio
		* @return resultado.
		*/	
		public String cargarRTRS(BigDecimal idToControlArchivo, String negocio){
			
				
			try {
			// Cargar el archivo
			is = getArchivoDatos(idToControlArchivo);
				
				
			listaRTRS = listarReporteRTRS();
			if(resultado.equals(EXITOSO)){
					
			for (ReporteRTRS reporte : listaRTRS) {
							complementoRR6Dao.insertaReporteRTRS(								
									reporte.getConsecutivo(),
									reporte.getSiniestro(),
									reporte.getClaveNegocio(),
									reporte.getSiniestroReclamacion(),
									reporte.getAsegurado(),
									reporte.getFechaReclamacion(),
									reporte.getImporteReclamacion(),
									reporte.getImporteRecSiniestro(),
									reporte.getReasInscrito(),
									reporte.getTipoReasNac(),
									reporte.getClaveReasNac(),
									reporte.getReasNoInscrito(),
									reporte.getImporteProporcional(),
									reporte.getImporteNoProporcional(),
									reporte.getImporteFacultativo(),
									reporte.getEntidades(),
									reporte.getMunicipio(),
									reporte.getSector(),
									reporte.getFechaCorte(),
									negocio);
							
						  }
					}
				
				
				} catch (RuntimeException ex) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
				}
				
			
		 return resultado;
						
		}
			
		/**
		 * Procesa la informacion del reporte RARN.
		 * @param idToControlArchivo
		 * @param negocio
		 * @return resultado.
		 */		
		public String cargarRARN(BigDecimal idToControlArchivo, String negocio){
						
			try {
			// Cargar el archivo
			is = getArchivoDatos(idToControlArchivo);
						
						
					listaRARN = listarReporteRARN();
					if(resultado.equals(EXITOSO)){
							
						
					for (ReporteRARN reporte : listaRARN) {
									complementoRR6Dao.insertaReporteRARN(								
											reporte.getMes(),
											reporte.getConcepto(),
											reporte.getRamo(),
											reporte.getRgre(),
											reporte.getTipo(),
											reporte.getReasNacional(),	
											reporte.getNoInscrito(),
											reporte.getPrimaCedida(),
											reporte.getCoberturaXL(),
											reporte.getSumaAsegurada(),
											reporte.getSumaAseguradaRetenida(),
											reporte.getFechaIniCorte(),
											reporte.getFechaFinCorte(),
											negocio);
									
								  }
							}
						
						
						} catch (RuntimeException ex) {
							resultado = ERROR;
							LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
						}
						
					return resultado;
								
				}
		
		/**
		 * Procesa la informacion del reporte RTRI.
		 * @param idToControlArchivo
		 * @param negocio
		 * @return resultado.
		 */		
		public String cargarRTRI(BigDecimal idToControlArchivo, String negocio){
						
			try {
			// Cargar el archivo
			is = getArchivoDatos(idToControlArchivo);
						
						
					listaRTRI = listarReporteRTRI();
					if(resultado.equals(EXITOSO)){
							
						
					for (ReporteRTRI reporte : listaRTRI) {
									complementoRR6Dao.insertaReporteRTRI(								
											reporte.getIdContrato(),
											reporte.getCorretajeContratos(),
											reporte.getCorretajeFacultativos(),
											reporte.getFechaFinCorte(),
											negocio);									
								  }
							}
						
						
						} catch (RuntimeException ex) {
							resultado = ERROR;
							LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
						}
						
					return resultado;
								
				}	
				
			

	private InputStream getArchivoDatos(BigDecimal idToControlArchivo) {

		InputStream archivo = null;
		ControlArchivoDTO controlArchivoDTO = entidadService.findById(ControlArchivoDTO.class, idToControlArchivo);
		String nombreCompletoArchivo = null;
		String nombreOriginalArchivo = controlArchivoDTO.getNombreArchivoOriginal();
		extension = (nombreOriginalArchivo.lastIndexOf('.') == -1) ? ""	: nombreOriginalArchivo.substring(
						nombreOriginalArchivo.lastIndexOf('.'),
						nombreOriginalArchivo.length());

		LogDeMidasEJB3.log("Buscando el archivo", Level.INFO, null);
		nombreCompletoArchivo = controlArchivoDTO.getIdToControlArchivo().toString() + extension;
		try {
			archivo = new FileInputStream(sistemaContext.getUploadFolder()
					+ nombreCompletoArchivo);
		} catch (FileNotFoundException re) {
			LogDeMidasEJB3.log("Archivo no encontrado", Level.SEVERE, re);
		}

		return archivo;
	}
	
	/**
	 * Convierte informacion del archivo excel RTRE.
	 * @return listaReporteRTRE.
	 */	
	public List<ReporteRTRE> listarReporteRTRE() {
		List<ReporteRTRE> listaReporteRTRE = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaReporteRTRE = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ReporteRTRE>() {
							public ReporteRTRE mapRow(Map<String, String> row,
									int rowNum) {
								ReporteRTRE reporteRTRE = new ReporteRTRE(fechaCorte);
								try {
									reporteRTRE.setClave(row.get("clave del negocio"));
									reporteRTRE.setNegCubiertos(row.get("negocios cubiertos"));
									reporteRTRE.setClaveEstrategia(Integer.parseInt(row.get("clave de la estrategia reas.")));
									reporteRTRE.setOrdenCobertura(Integer.parseInt(row.get("orden de cobertura")));
									reporteRTRE.setIdentificador(row.get("id. contrato"));


								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide.", Level.SEVERE, e);
								}
								return reporteRTRE;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteRTRE;
	}
	
	/**
	 * Convierte informacion del archivo excel RTRC.
	 * @return listaReporteRTRE.
	 */	
	public List<ReporteRTRC> listarReporteRTRC() {
		List<ReporteRTRC> listaReporteRTRC = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaReporteRTRC = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ReporteRTRC>() {
							public ReporteRTRC mapRow(Map<String, String> row,
									int rowNum) {
								ReporteRTRC reporteRTRC = new ReporteRTRC(fechaCorte);
								try {
									reporteRTRC.setIdentificador(row.get("id. contrato"));
									reporteRTRC.setClave(row.get("clave negocio"));
									reporteRTRC.setNegCubiertos(row.get("negocios cubiertos"));
									reporteRTRC.setMoneda(row.get("moneda"));
									reporteRTRC.setCaptura(row.get("tipo de captura"));
									reporteRTRC.setModificados(row.get("campos modificados"));
									reporteRTRC.setFechainicial(new BigDecimal(row.get("inicio vigencia")).toPlainString());
									reporteRTRC.setFechafinal(new BigDecimal(row.get("fin vigencia")).toPlainString());
									reporteRTRC.setTipocontrato(Integer.parseInt(row.get("tipo contrato")));
									reporteRTRC.setCapas(row.get("capas-lineas"));
									reporteRTRC.setPorcionCedida(new BigDecimal(row.get("porcion cedida")));
									reporteRTRC.setRetencionPrioridad(new BigDecimal(row.get("retencion-prioridad")));
									reporteRTRC.setRetencionFianzas(row.get("retencion fianzas"));
									reporteRTRC.setCapacidadMaxima(new BigDecimal(row.get("cap. max lim. resp.")));
									reporteRTRC.setCapMaxFianzas(row.get("cap. max. fianzas"));
									reporteRTRC.setImporte(new BigDecimal(row.get("imp. reinstal")));
									reporteRTRC.setComision(row.get("comision rate"));
									reporteRTRC.setUtilidades(row.get("participacion utilidades"));
									reporteRTRC.setReasInscrito(row.get("reas. inscrito"));
									reporteRTRC.setTipoReas(row.get("tipo reas. nac."));
									reporteRTRC.setReasNacional(row.get("clave reas. nac."));
									reporteRTRC.setNoInscrito(row.get("reas. no inscrito"));
									reporteRTRC.setParticipacion(row.get("participacion reas"));
									reporteRTRC.setTipoIntermediario(row.get("tipo intermediario"));
									reporteRTRC.setId_Intermediario(row.get("clave intermed."));
									reporteRTRC.setSuscriptor(row.get("suscriptor facultado"));
									reporteRTRC.setAclaracion(row.get("aclaraciones"));
									reporteRTRC.setTipoIntermediario(row.get("tipo intermediario"));
									

								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide.", Level.SEVERE, e);
								}
								return reporteRTRC;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteRTRC;
	}
	
	/**
	 * Convierte informacion del archivo excel RTRF.
	 * @return listaReporteRTRE.
	 */	
	public List<ReporteRTRF> listarReporteRTRF() {
		List<ReporteRTRF> listaReporteRTRF = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());
			listaErrores.clear();
			for (String sheetName : converter.getSheetNames()) {
				 listaReporteRTRF = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ReporteRTRF>() {
							public ReporteRTRF mapRow(Map<String, String> row,
									int rowNum) {
								ReporteRTRF reporteRTRF = new ReporteRTRF(fechaCorte);
														
								try {
									reporteRTRF.setIdentificador(row.get("id. contrato"));
									validaCampos(1, reporteRTRF.getIdentificador(), rowNum);
									reporteRTRF.setClave(row.get("clave negocio"));
									reporteRTRF.setNegCubiertos(row.get("negocios cubiertos"));
									validaCampos(3, reporteRTRF.getNegCubiertos(), rowNum);
									reporteRTRF.setAsegurado(row.get("asegurado"));
									validaCampos(4, reporteRTRF.getAsegurado(), rowNum);
									validaCampos(5, row.get("sa. afianzada"), rowNum);
									reporteRTRF.setSumaAsegurada(row.get("sa. afianzada"));
									reporteRTRF.setMoneda(Integer.parseInt(row.get("moneda")));
									validaCampos(7, row.get("prima emitida"), rowNum);
									reporteRTRF.setPrimaEmitida(new BigDecimal(row.get("prima emitida")));
									validaCampos(8, row.get("prima cedida fac."), rowNum);
									reporteRTRF.setPrimaFacultada(new BigDecimal(row.get("prima cedida fac.")));
									validaCampos(9, row.get("prima ced. prop."), rowNum);
									reporteRTRF.setPrimaCedProporc(new BigDecimal(row.get("prima ced. prop.")));
									validaCampos(10, row.get("prima retenida"), rowNum);
									reporteRTRF.setPrimaRetenida(new BigDecimal(row.get("prima retenida")));
									reporteRTRF.setFechaInicial(new BigDecimal(row.get("inicio vigencia")).toPlainString());
									reporteRTRF.setFechaFinal(new BigDecimal(row.get("fin vigencia")).toPlainString());
									reporteRTRF.setTipoContrato(row.get("tipo contrato"));
									reporteRTRF.setCapas(row.get("capas-lineas"));
									validaCampos(15, row.get("retencion-prioridad"), rowNum);
									reporteRTRF.setRetPrioridad(new BigDecimal(row.get("retencion-prioridad")));
									reporteRTRF.setRetpriorfian(row.get("retencion fianzas"));
									validaCampos(17, row.get("cap. max. reas."), rowNum);
									reporteRTRF.setCapacidadMaxReas(new BigDecimal(row.get("cap. max. reas.")));
									reporteRTRF.setCapacidadMaxFian(row.get("cap. max. fianzas"));
									reporteRTRF.setComision(new BigDecimal(row.get("comision rate")));
									reporteRTRF.setPartUtilidades(row.get("participacion utilidades"));
									reporteRTRF.setReasInscrito(row.get("reas. inscritos"));
									reporteRTRF.setTipoReas(row.get("tipo reas. nac."));
									reporteRTRF.setClvReasNac(row.get("clave reas. nac."));									
									reporteRTRF.setReasNoInscrito(row.get("reas. no inscrito"));
									reporteRTRF.setParticipacionReas(new BigDecimal(row.get("participacion reas.")));
									reporteRTRF.setTipoIntermediario(row.get("tipo intermediario"));
									reporteRTRF.setClvIntermediario(row.get("clave intermediario"));
									reporteRTRF.setIntNoAutorizado(row.get("intermediario no autorizado"));
									reporteRTRF.setSuscriptor(row.get("suscriptor fac."));
									reporteRTRF.setAclaraciones(row.get("aclaraciones"));
									reporteRTRF.setEntidades(row.get("entidades"));
									reporteRTRF.setMunicipio(row.get("municipio"));
									reporteRTRF.setSector(row.get("sector"));
									
								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide.", Level.SEVERE, e);
								}
								return reporteRTRF;
							}
						});
			    
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteRTRF;
	}
	
	/**
	 * Convierte informacion del archivo excel RTRR.
	 * @return listaReporteRTRE.
	 */	
	public List<ReporteRTRR> listarReporteRTRR() {
		List<ReporteRTRR> listaReporteRTRR = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());
			listaErrores.clear();
			for (String sheetName : converter.getSheetNames()) {
				 listaReporteRTRR = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ReporteRTRR>() {
							public ReporteRTRR mapRow(Map<String, String> row,
									int rowNum) {
								ReporteRTRR reporteRTRR = new ReporteRTRR(fechaCorte);
														
								try {
									
									reporteRTRR.setIdentificador(row.get("id. contrato"));
									reporteRTRR.setTipoContrato(Integer.parseInt(row.get("tipo contrato")));
									reporteRTRR.setAnio(row.get("anio reporte"));
									reporteRTRR.setClave(row.get("clave negocio"));
									if(StringUtils.substring(row.get("id. contrato"), 0,3).equalsIgnoreCase("FAC")){
										if(new BigDecimal(row.get("ini. vigencia")).toPlainString().equals("19000101")){
											reporteRTRR.setIniciovigencia(new BigDecimal(row.get("ini. vigencia")).toPlainString());
										}else{
											listaErrores.add("Campo INICIO DE VIGENCIA. Fila: "+rowNum+1+" Columna: "+5+". Debe ser igual a \"19000101\" para contrato FAC.");
											resultado = ERROR;
										}				
									}else{
										reporteRTRR.setIniciovigencia(new BigDecimal(row.get("ini. vigencia")).toPlainString());
									}									
									reporteRTRR.setComisiones(new BigDecimal(row.get("comisiones reas.")));
									reporteRTRR.setUtilidades(new BigDecimal(row.get("particip. utilidades reas.")));
									reporteRTRR.setSinReclamaFac(new BigDecimal(row.get("reclamaciones proporcionales y fac.")));
									reporteRTRR.setSinReclamaNoProp(new BigDecimal(row.get("reclamaciones no proporcionales")));
									reporteRTRR.setIngresos(new BigDecimal(row.get("ingresos oper. reas. financiero")));
									reporteRTRR.setOtrosIngresos(new BigDecimal(row.get("otros ingresos")));
									reporteRTRR.setMontoPrima(new BigDecimal(row.get("prima cedida")));
									reporteRTRR.setCostoCobertura(new BigDecimal(row.get("cobert. no prop.")));
									reporteRTRR.setPartSalvamento(new BigDecimal(row.get("particip. salvamentos")));
									reporteRTRR.setRecursosRetenidos(new BigDecimal(row.get("intereses recur. retenidos")));
									reporteRTRR.setReasFinanciero(new BigDecimal(row.get("intereses reas. financiero")));
									reporteRTRR.setCastigo(new BigDecimal(row.get("castigo")));
									reporteRTRR.setEgresos(new BigDecimal(row.get("otros egresos")));
									reporteRTRR.setGastosReas(new BigDecimal(row.get("gastos reas.")));
									reporteRTRR.setAclaraciones(row.get("aclaraciones"));
									
									 
									
								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide.", Level.SEVERE, e);
								}
								return reporteRTRR;
							}
						});
			    
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteRTRR;
	}
	
	/**
	 * Convierte informacion del archivo excel RTRS.
	 * @return listaReporteRTRE.
	 */	
	public List<ReporteRTRS> listarReporteRTRS() {
		List<ReporteRTRS> listaReporteRTRS = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());
			listaErrores.clear();
			for (String sheetName : converter.getSheetNames()) {
				 listaReporteRTRS = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ReporteRTRS>() {
							public ReporteRTRS mapRow(Map<String, String> row,
									int rowNum) {
								ReporteRTRS reporteRTRS = new ReporteRTRS(fechaCorte);
														
								try {
																		
									reporteRTRS.setConsecutivo(Integer.parseInt(row.get("num. consecutivo")));
									reporteRTRS.setSiniestro(new BigDecimal(row.get("siniestro")).intValue());
									reporteRTRS.setClaveNegocio(row.get("clave negocio"));
									reporteRTRS.setSiniestroReclamacion(row.get("siniestro reclamacion"));
									reporteRTRS.setAsegurado(row.get("asegurado afianzado"));
									reporteRTRS.setFechaReclamacion(new BigDecimal(row.get("fecha siniestro reclamacion")).toPlainString());									
									reporteRTRS.setImporteReclamacion(new BigDecimal(row.get("importe siniestro reclamacion")));
									reporteRTRS.setImporteRecSiniestro(new BigDecimal(row.get("importe recuperado")));
									reporteRTRS.setReasInscrito(row.get("reas. inscritos"));									
									reporteRTRS.setTipoReasNac(row.get("tipo reas. nac."));									
									reporteRTRS.setClaveReasNac(row.get("clave reas. nac."));									
									reporteRTRS.setReasNoInscrito(row.get("reas. no inscrito"));
									validaRTRS(13, row.get("importe proporcional"), rowNum);
									reporteRTRS.setImporteProporcional(new BigDecimal(row.get("importe proporcional")));
									validaRTRS(14, row.get("importe no proporcional"), rowNum);
									reporteRTRS.setImporteNoProporcional(new BigDecimal(row.get("importe no proporcional")));
									validaRTRS(15, row.get("importe facultativo"), rowNum);
									reporteRTRS.setImporteFacultativo(new BigDecimal(row.get("importe facultativo")));
									reporteRTRS.setEntidades(row.get("entidades"));
									reporteRTRS.setMunicipio(row.get("municipio"));
									reporteRTRS.setSector(row.get("sector publico o privado"));
									
									BigDecimal importes = BigDecimal.ZERO;
									importes = importes.add(reporteRTRS.getImporteProporcional());
									importes = importes.add(reporteRTRS.getImporteNoProporcional());
									importes = importes.add(reporteRTRS.getImporteFacultativo());
											
									if (reporteRTRS.getImporteRecSiniestro().compareTo(importes) != 0){
										rowNum = rowNum+1;
										listaErrores.add("El Importe Recuperado de la fila "+rowNum+" debe ser igual a la suma de los demás importes.");
										resultado = ERROR;
										
									}									
																		
								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide.", Level.SEVERE, e);
								}
								return reporteRTRS;
							}
						});
			    
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteRTRS;
	}
	
	/**
	 * Convierte informacion del archivo excel RARN.
	 * @return listaReporteRTRE.
	 */	
	public List<ReporteRARN> listarReporteRARN() {
		List<ReporteRARN> listaReporteRARN = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());
			listaErrores.clear();
			for (String sheetName : converter.getSheetNames()) {
				 listaReporteRARN = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ReporteRARN>() {
							public ReporteRARN mapRow(Map<String, String> row,
									int rowNum) {
								ReporteRARN reporteRARN = new ReporteRARN(fechaCorte);
														
								try {
									
									validaRARN(1, new BigDecimal(row.get("mes")).toPlainString(), rowNum);									
									reporteRARN.setMes(new BigDecimal(row.get("mes")).toPlainString());
									validaRARN(2, row.get("concepto"), rowNum);
									reporteRARN.setConcepto(row.get("concepto"));
									validaRARN(3, row.get("clave ramo"), rowNum);
									reporteRARN.setRamo(row.get("clave ramo"));
									reporteRARN.setRgre(row.get("reas. inscritos"));
									reporteRARN.setTipo(row.get("tipo reas. nac."));
									reporteRARN.setReasNacional(row.get("clave reas. nac."));	
									reporteRARN.setNoInscrito(row.get("reas. no inscrito"));
									reporteRARN.setPrimaCedida(validaNulos(row.get("prima cedida")));
									reporteRARN.setCoberturaXL(validaNulos(row.get("cobertura xl")));
									reporteRARN.setSumaAsegurada(validaNulos(row.get("suma asegurada")));									
									reporteRARN.setSumaAseguradaRetenida(validaNulos(row.get("sa retenida")));
									
																											
								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide.", Level.SEVERE, e);
								}
								return reporteRARN;
							}
						});
			    
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteRARN;
	}
	
	/**
	 * Convierte informacion del archivo excel RTRI.
	 * @return listaReporteRTRI.
	 */	
	public List<ReporteRTRI> listarReporteRTRI() {
		List<ReporteRTRI> listaReporteRTRI = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());
			listaErrores.clear();
			for (String sheetName : converter.getSheetNames()) {
				 listaReporteRTRI = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ReporteRTRI>() {
							public ReporteRTRI mapRow(Map<String, String> row,
									int rowNum) {
								ReporteRTRI reporteRTRI = new ReporteRTRI(fechaCorte);
														
								try {
									
									if(row.get("id. contrato") == null || row.get("id. contrato").equals("") || row.get("id. contrato").isEmpty()){
										listaErrores.add("Campo RAMO. Fila: "+row+" Columna: 1 - Valor inválido.");
										resultado = ERROR;
									}
									reporteRTRI.setIdContrato(row.get("id. contrato"));
									reporteRTRI.setCorretajeContratos(validaNulos(row.get("corretaje cont.")));
									reporteRTRI.setCorretajeFacultativos(validaNulos(row.get("corretaje fac.")));
									
																											
								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide.", Level.SEVERE, e);
								}
								return reporteRTRI;
							}
						});
			    
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteRTRI;
	}
	
	
	/**
	 * Valida campos del archivo excel.
	 * @param col
	 * @param reporte
	 * @param row
	 * @return resultado.
	 */	
	public String validaCampos(int col, String reporte, int row){
		switch(col) {
		case (1):{
			if(reporte.length()>16){
			row = row+1;
			listaErrores.add("Campo IDENTIFICADOR. Fila: "+row+" Columna: "+col);
			resultado = ERROR;
			}
			break;
		}
		case (3):{
			if(reporte.length()>100){
			row = row+1;
			listaErrores.add("Campo NEGOCIOS CUBIERTOS. Fila: "+row+" Columna: "+col);
			resultado = ERROR;
			}
			break;
		}
		case (4):{
			if(reporte.length()>100){
			row = row+1;
			listaErrores.add("Campo ASEGURADO. Fila: "+row+" Columna: "+col+ ". - Debe ser menor o igual a 100 caracteres.");
			resultado = ERROR;
			}
			break;
		 }
		case (5):
		{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){ 
			row = row+1;
		    listaErrores.add("Campo SA. AFIANZADA. Fila: "+row+" Columna: "+col+ " Introduzca un número válido.");
		    resultado = ERROR;
		    }
			break;
		 }
		case (7):
		{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){ 
			row = row+1;
		    listaErrores.add("Campo PRIMA EMITIDA. Fila: "+row+" Columna: "+col+ " Introduzca un número válido.");
		    resultado = ERROR;
		    }
			break;
		 }
		case (8):
		{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){ 
			row = row+1;
		    listaErrores.add("Campo PRIMA CEDIDA FAC. Fila: "+row+" Columna: "+col+ " Introduzca un número válido.");
		    resultado = ERROR;
		    }
			break;
		 }
		case (9):
		{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){ 
			row = row+1;
		    listaErrores.add("Campo PRIMA CED. PROP. Fila: "+row+" Columna: "+col+ " Introduzca un número válido.");
		    resultado = ERROR;
		    }
			break;
		 }
		case (10):
		{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){ 
			row = row+1;
		    listaErrores.add("Campo PRIMA RETENIDA. Fila: "+row+" Columna: "+col+ " Introduzca un número válido.");
		    resultado = ERROR;
		    }
			break;
		 }
		case (15):
		{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){ 
			row = row+1;
		    listaErrores.add("Campo RETENCION-PRIORIDAD. Fila: "+row+" Columna: "+col+ " Introduzca un número válido.");
		    resultado = ERROR;
		    }
			break;
		 }
		case (17):{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){ 
			row = row+1;
		    listaErrores.add("Campo CAP. MAX. REAS. Fila: "+row+" Columna: "+col+ " Introduzca un número válido.");
		    resultado = ERROR;
		    }
			break;
		 }
		}
		
		return resultado;
	}
	
	/**
	 * Valida campos del archivo excel.
	 * @param col
	 * @param reporte
	 * @param row
	 * @return resultado.
	 */	
	public String validaRARN(int col, String reporte, int row){
		switch(col) {
		case (1):{
			if(reporte.length()>0 || !reporte.equals("") || !reporte.isEmpty()){
				String fechaFormatada = Utilerias.cadenaDeFecha(fCorte, "yyyyMMdd");
				
				if(!fechaFormatada.equalsIgnoreCase(reporte)){
					row = row+1;
					listaErrores.add("Campo MES. Fila: "+row+" Columna: "+col+" No coincide con la fecha de corte.");
					resultado = ERROR;
				}					
			}else{
				row = row+1;
				listaErrores.add("Campo MES. Fila: "+row+" Columna: "+col+" Valor inválido.");
				resultado = ERROR;
			}
			break;
		}
		case (2):{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){
			row = row+1;
			listaErrores.add("Campo CONCEPTO. Fila: "+row+" Columna: "+col+" Valor inválido.");
			resultado = ERROR;
			}
			break;
		}	
		case (3):{
			if(reporte == null || reporte.equals("") || reporte.isEmpty()){
			row = row+1;
			listaErrores.add("Campo RAMO. Fila: "+row+" Columna: "+col+" Valor inválido.");
			resultado = ERROR;
			}
			break;
		}		
	  }
		
	  return resultado;
	
	}
	
	/**
	 * Valida campos del archivo excel.
	 * @param col
	 * @param reporte
	 * @param row
	 * @return resultado.
	 */	
	
	public String validaRTRS(int col, String reporte, int row){
		switch(col) {
		case (13):{
			if(reporte==null || reporte.equals("") || reporte.isEmpty()){
				
				row = row+1;
				listaErrores.add("Campo IMPORTE PROPORCIONAL. Fila: "+row+" Columna: "+col+" Valor inválido.");
				resultado = ERROR;					
			}
			break;		   
		}
		case (14):{
			if(reporte==null || reporte.equals("") || reporte.isEmpty()){
				
				row = row+1;
				listaErrores.add("Campo IMPORTE NO PROPORCIONAL. Fila: "+row+" Columna: "+col+" Valor inválido.");
				resultado = ERROR;					
			}
			break;
		}
		case (15):{
			if(reporte==null || reporte.equals("") || reporte.isEmpty()){
				
				row = row+1;
				listaErrores.add("Campo IMPORTE FACULTATIVO. Fila: "+row+" Columna: "+col+" Valor inválido.");
				resultado = ERROR;					
			}
			break;
		}
	  }
		
	  return resultado;
	
	}
	
	
	/**
	 * Valida campos nulos del archivo excel.
	 * @param valor
	 * @return result.
	 */	
	public BigDecimal validaNulos(String valor){
		BigDecimal result;
		if(valor == null){
			 result = BigDecimal.ZERO;
		}else{
			result = new BigDecimal(valor);
		}
		
		return result;
	}
	
	/**
	 * Llena CargaRR6DTO.
	 * @param fCorte
	 * @param estatus
	 * @param reporte
	 * @return resultado.
	 */	
	public void llenarCargaRR6DTO(java.util.Date fCorte,String estatus, String reporte, String usuario, BigDecimal idToControlArchivo){
		java.util.Date fechaActual = new java.util.Date();
		cargaDTO =  new CargaRR6DTO();
		cargaDTO.setFechaFin(fCorte);
		cargaDTO.setEstatus(estatus);
		cargaDTO.setReporte(reporte);
		cargaDTO.setUsuario(usuario);
		cargaDTO.setFechaCarga(fechaActual);
		cargaDTO.setIdArchivo(idToControlArchivo.intValueExact());
		cargaDTO.setCandado(0);
	}
		

	
	public static Date getFechaFromString(String fecha) throws ParseException {
		java.util.Date fechaCorte = null;
		Date sqlDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
		fechaCorte = sdf.parse((fecha));
		sqlDate = new java.sql.Date(fechaCorte.getTime());
		} catch (ParseException e) {
			LogDeMidasEJB3.log("Falla al obtnener la fecha", Level.SEVERE, e);
		}
		return sqlDate;
	}
}
