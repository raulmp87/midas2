package mx.com.afirme.midas2.service.impl;

import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

/**
 * Used to convert an Excel to a <code>Map</code> or a custom object using a <code>ExcelRowMapper</code>.
 * @author amosomar
 */
public class ExcelConverter {
	
	private static final Log log = LogFactory.getLog(ExcelConverter.class);
	
	private Workbook workbook;
	private ExcelConfig config;
			
	public static class ExcelConfig {	
		
		public static class ExcelConfigBuilder {
			private boolean columnNamesToLowerCase;
			private boolean trimColumnNames;
			private int columnNamesRowNumber = 1;
			private boolean skipIfRowEmpty = true;
			private boolean trimColumnValues = true;
			private String dateFormat = "dd/MM/yyyy";

			public ExcelConfigBuilder withColumnNamesToLowerCase(boolean columnNamesToLowerCase) {
				this.columnNamesToLowerCase = columnNamesToLowerCase;
				return this;
			}
			
			public ExcelConfigBuilder withTrimColumnNames(boolean trimColumnNames) {
				this.trimColumnNames = trimColumnNames;
				return this;
			}
			
			public ExcelConfigBuilder withColumnNamesRowNumber(int columnNamesRowNumber) {
				this.columnNamesRowNumber = columnNamesRowNumber;
				return this;
			}	
			
			public ExcelConfigBuilder withSkipIfRowEmpty(boolean skipIfRowEmpty) {
				this.skipIfRowEmpty = skipIfRowEmpty;
				return this;
			}

			public ExcelConfigBuilder withTrimColumnValues(boolean trimColumnValues) {
				this.trimColumnValues = trimColumnValues;
				return this;
			}
			
			public ExcelConfigBuilder withDateFormat(String dateFormat) {
				this.dateFormat = dateFormat;
				return this;
			}

			public ExcelConfig build() {
				return new ExcelConfig(this);
			}
		}
		
		private boolean columnNamesToLowerCase;
		private boolean trimColumnNames;
		private int columnNamesRowNumber;
		private boolean skipIfRowEmpty;
		private boolean trimColumnValues;
		private SimpleDateFormat dateFormatter;
		
		private ExcelConfig(ExcelConfigBuilder builder) {
			this.columnNamesToLowerCase = builder.columnNamesToLowerCase;
			this.trimColumnNames = builder.trimColumnNames;
			this.columnNamesRowNumber = builder.columnNamesRowNumber;
			this.skipIfRowEmpty = builder.skipIfRowEmpty;
			this.trimColumnValues = builder.trimColumnValues;
			this.dateFormatter = new SimpleDateFormat(builder.dateFormat);
		}			
		
		public boolean isColumnNamesToLowerCase() {
			return columnNamesToLowerCase;
		}
		public boolean isTrimColumnNames() {
			return trimColumnNames;
		}
		
		public int getColumnNamesRowNumber() {
			return columnNamesRowNumber;
		}

		public boolean isSkipIfRowEmpty() {
			return skipIfRowEmpty;
		}
		
		public boolean isTrimColumnValues() {
			return trimColumnValues;
		}
		
		public SimpleDateFormat getDateFormatter() {
			return dateFormatter;
		}
	}
	
	public ExcelConverter() {
		this.config = new ExcelConfigBuilder().build();
	}
	
	public ExcelConverter(InputStream is) {
		this(is, new ExcelConfigBuilder().build());
	}
	
	public ExcelConverter(InputStream is, ExcelConfig config) {
		try {
			this.workbook = WorkbookFactory.create(is);
			this.config = config;
		} catch (InvalidFormatException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
	
	public List<String> getSheetNames() {
		List<String> sheetNames = new ArrayList<String>();
		for (int i = 0; i < workbook.getNumberOfSheets(); i++) {
			sheetNames.add(workbook.getSheetName(i));
		}
		return sheetNames;
	}
	
	public ExcelConfig getConfig() {
		
		return this.config;
		
	}
	
	public List<Map<String, String>> parse(String sheetName) {
		return parse(workbook.getSheet(sheetName));
	}
	
	public <T> List<T> parseWithRowMapper(String sheetName, ExcelRowMapper<T> mapper) {
		return getMapperEntries(parse(workbook.getSheet(sheetName)), mapper);
	}
	
	public <T> List<T> parseWithRowMapper(InputStream is, ExcelRowMapper<T> mapper) {
		return getMapperEntries(parse(is), mapper);
	}
	
	private  <T> List<T> getMapperEntries(List<Map<String,String>> entries, ExcelRowMapper<T> mapper) {
		List<T> mapperEntries = new ArrayList<T>();
		int rowNum = 0;
		for (Map<String, String> entry : entries) {
			T mapperEntry = mapper.mapRow(entry, rowNum++);
			mapperEntries.add(mapperEntry);
		}
		return mapperEntries;
	}
	
	/**
	 * <p>Parses the Excel
	 * @param is the Excel stream.
	 * @return
	 */
	public List<Map<String, String>> parse(InputStream is) {
		if (is == null) {
			throw new IllegalArgumentException("InputStream is null.");
		}
		
		Workbook workBook = null;
		try {
			workBook = WorkbookFactory.create(is);
		} catch (InvalidFormatException e) {
			throw new RuntimeException(e);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}		
		
		return parse(workBook.getSheetAt(0));		
		
	}
	
	private List<Map<String, String>> parse(Sheet sheet) {
		List<Map<String, String>> entries = new ArrayList<Map<String,String>>();
		Map<String, Integer> columnNameColumnIndexMap = getColumnNameColumnIndexMap(sheet);		
		if (columnNameColumnIndexMap.size() == 0) {
			return entries;
		}
		
		int currentRow = 0;
		for (Row row : sheet) {
			currentRow++; //Not necessarily the row index.
			if (currentRow <= config.columnNamesRowNumber) {
				continue; //Skip all the rows before the column name row number.
			}
			addEntry(row, entries, columnNameColumnIndexMap);
		}
		return entries;
	}
	
	private void addEntry(Row row, List<Map<String, String>> entries,
			Map<String, Integer> columnNameColumnIndexMap) {
		//Check if the given row should be processed.
		boolean empty = true;
		Map<String, String> entry = new HashMap<String, String>();
		for (Entry<String, Integer> columnNameColumnIndexEntry : columnNameColumnIndexMap.entrySet()) {
			String columnName = columnNameColumnIndexEntry.getKey();
			int columnIndex = columnNameColumnIndexEntry.getValue();
			Cell cell = row.getCell(columnIndex);
			if (!isEmpty(cell)) {
				empty = false;
				//cell.setCellType(Cell.CELL_TYPE_STRING);
				String value = getStringCellValue(cell);
				if (config.trimColumnValues) {
					value = value.trim();
				}
				entry.put(columnName, value);
			} else {
				entry.put(columnName, null);
			}
		}
		if (config.skipIfRowEmpty && empty) {
			return;
		}
		entries.add(entry);
	}

	private Map<String, Integer> getColumnNameColumnIndexMap(Sheet sheet) {
		Map<String, Integer> columnNameColumnIndexMap = new HashMap<String, Integer>();
		for (Row row : sheet) {
			for (Cell  cell : row) {
				cell.setCellType(Cell.CELL_TYPE_STRING);
				String columnName = getStringCellValue(cell);
				if (config.columnNamesToLowerCase) {
					columnName = columnName.toLowerCase();
				}
				if (config.trimColumnNames) {
					columnName = columnName.trim();
				}
				if (StringUtils.isBlank(columnName)) {
					log.debug("Ignoring header cell " + cell.getColumnIndex()
							+ " because is empty.");
					continue;
				}
				if (columnNameColumnIndexMap.containsKey(columnName)) {
					throw new IllegalStateException("Duplicated column name: " + columnName + ".");
				}
				columnNameColumnIndexMap.put(columnName, cell.getColumnIndex());
			}
			break; //We only iterate one time because the first row is the column names row.
		}
		return columnNameColumnIndexMap;
	}
	
	private boolean isEmpty(Cell cell) {
		if (cell == null) {
			return true;
		}
		
		//cell.setCellType(Cell.CELL_TYPE_STRING);
		if (StringUtils.isBlank(getStringCellValue(cell))) {
			return true;
		}
		return false;
	}
		
	private String getStringCellValue(Cell cell) {
		String value;
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			value = cell.getRichStringCellValue().getString();
			break;
		case Cell.CELL_TYPE_NUMERIC:
			if (DateUtil.isCellDateFormatted(cell)) {
				value = config.dateFormatter.format(cell.getDateCellValue());
			} else {
				value = String.valueOf(cell.getNumericCellValue());
				
				if (value.endsWith(".0")) {
					value = value.substring(0, value.length() -2);
				}
				
			}
			break;
		case Cell.CELL_TYPE_BOOLEAN:
			value = String.valueOf(cell.getBooleanCellValue());
			break;
		case Cell.CELL_TYPE_FORMULA:
			value = cell.getCellFormula();
			break;
		default:
			value = "";
		}
		return value;
	}
	
}
