/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaBaseCalculoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBaseCalculo;
import mx.com.afirme.midas2.service.compensaciones.CaBaseCalculoService;

import org.apache.log4j.Logger;

@Stateless

public class CaBaseCalculoServiceImpl  implements CaBaseCalculoService {

	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaBaseCalculoDao baseCalculocaDao;

//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaBaseCalculoServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaBaseCalculo entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaBaseCalculo entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaBaseCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	save	::	INICIO	::	");
	        try {
//            entityManager.persist(entity);
	        	baseCalculocaDao.save(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaBaseCalculo entity.
	  @param entity CaBaseCalculo entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaBaseCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaBaseCalculo.class, entity.getId());
//            entityManager.remove(entity);
	        	baseCalculocaDao.delete(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaBaseCalculo entity and return it or a copy of it to the sender. 
	 A copy of the CaBaseCalculo entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaBaseCalculo entity to update
	 @return CaBaseCalculo the persisted CaBaseCalculo entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaBaseCalculo update(CaBaseCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaBaseCalculo result = baseCalculocaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaBaseCalculo 	::		CaBaseCalculoServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaBaseCalculo findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaBaseCalculoServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaBaseCalculo instance = baseCalculocaDao.findById(id);//entityManager.find(CaBaseCalculo.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaBaseCalculoServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaBaseCalculoServiceImpl	::	findById	::	ERROR	::	",re);
	            throw re;
        }
    }   

    
    public List<CaBaseCalculo> findByProperties(String propertyName, int ...isValues){
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaBaseCalculoServiceImpl	::	findByProperty	::	INICIO	::	");
    	try {
		LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaBaseCalculoServiceImpl	::	findByProperty	::	FIN	::	");
		return baseCalculocaDao.findByProperties(propertyName, isValues);
		}catch (RuntimeException re){
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaBaseCalculoServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
    }
/**
	 * Find all CaBaseCalculo entities with a specific property value.  
	 
	  @param propertyName the name of the CaBaseCalculo property to query
	  @param value the property value to match
	  	  @return List<CaBaseCalculo> found by query
	 */
    public List<CaBaseCalculo> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaBaseCalculoServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaBaseCalculo model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaBaseCalculoServiceImpl	::	findByProperty	::	FIN	::	");
			return baseCalculocaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaBaseCalculoServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaBaseCalculo> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaBaseCalculo> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaBaseCalculo> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaBaseCalculo> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaBaseCalculo entities.
	  	  @return List<CaBaseCalculo> all CaBaseCalculo entities
	 */
	public List<CaBaseCalculo> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaBaseCalculoServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaBaseCalculo model";
//			Query query = entityManager.createQuery(queryString, CaBaseCalculo.class);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaBaseCalculoServiceImpl	::	findAll	::	FIN	::	");
			return baseCalculocaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaBaseCalculoServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}