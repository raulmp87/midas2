package mx.com.afirme.midas2.service.impl.siniestros.catalogo.pieza;

import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas2.dao.siniestros.catalogo.piezavaluacion.PiezaValuacionDao;
import mx.com.afirme.midas2.domain.siniestros.catalogo.pieza.PiezaValuacion;
import mx.com.afirme.midas2.service.impl.siniestros.catalogo.CatalogoSiniestroServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.pieza.PiezaValuacionService;

@Stateless
public class PiezaValuacionServiceImpl extends CatalogoSiniestroServiceImpl implements PiezaValuacionService {
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService       usuarioService;
	
	@EJB
	private PiezaValuacionDao piezaValuacionDao;
	
	private Date hoy = new Date();
	
	
	@Resource
	private Validator validator;

	@Override
	public void guardar(PiezaValuacion piezaValuacion) {
		
		Set<ConstraintViolation<PiezaValuacion>> constraintViolations = validator.validate( piezaValuacion );
		
		if (constraintViolations.size() > 0) {
			
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
			
		}else{
		
			// # SI EL ID NO ES NULO SE SETEA DATOS: FECHA MODIFICACION Y USUARIO MODIFICACION
			
			if( piezaValuacion.getId() != null ){
				piezaValuacion.setFechaModificacion(this.hoy);
				piezaValuacion.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
			}else{
				piezaValuacion.setCodigoUsuarioCreacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
			}
			
			this.salvar(piezaValuacion);
		
		}
	}

	@Override
	public PiezaValuacion obtener(Long idPiezaValuacion) {
		return this.obtener(PiezaValuacion.class, idPiezaValuacion);
	}

	@Override
	public List<PiezaValuacion> listar(
			PiezaValuacionFiltro piezaValuacionFiltro) {
		return piezaValuacionDao.buscar(piezaValuacionFiltro);
	}
	
}
