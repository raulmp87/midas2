package mx.com.afirme.midas2.service.impl.movil.cliente;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.math.BigDecimal;
import java.net.URL;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;
import mx.com.afirme.midas.poliza.NumeroPolizaCompletoMidas;
import mx.com.afirme.midas.poliza.NumeroPolizaCompletoSeycos;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas.ws.cliente.PrintReportClientImpl;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.dao.movil.cliente.PolizaRenovadaEnvioDocMovilDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.movil.MensajeValidacion;
import mx.com.afirme.midas2.domain.movil.ajustador.Recibo;
import mx.com.afirme.midas2.domain.movil.cliente.ClientePolizas;
import mx.com.afirme.midas2.domain.movil.cliente.CorreosCosultaMovil;
import mx.com.afirme.midas2.domain.movil.cliente.DatosPolizaSeycos;
import mx.com.afirme.midas2.domain.movil.cliente.EnvioCaratulaParameter;
import mx.com.afirme.midas2.domain.movil.cliente.InfoDescargaPoliza;
import mx.com.afirme.midas2.domain.movil.cliente.InfoPolizaParameter;
import mx.com.afirme.midas2.domain.movil.cliente.NotificacionCliente;
import mx.com.afirme.midas2.domain.movil.cliente.NotificacionClienteMovil;
import mx.com.afirme.midas2.domain.movil.cliente.PolizaDescargaMovil;
import mx.com.afirme.midas2.domain.movil.cliente.PolizaRenovadaEnvioDocMovil;
import mx.com.afirme.midas2.domain.movil.cliente.PolizasClienteMovil;
import mx.com.afirme.midas2.domain.movil.cotizador.ValidacionEstatusUsuarioActual;
import mx.com.afirme.midas2.domain.siniestro.TipoSiniestroMovil;
import mx.com.afirme.midas2.dto.fuerzaventa.CentroOperacionView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.movil.cliente.NotificacionClienteMovilService;
import mx.com.afirme.midas2.service.movil.cliente.PolizaDescargaMovilService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.apn.SendAPNs;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;
import com.google.android.gcm.server.Message.Builder;
import com.google.android.gcm.server.Result;
import com.google.android.gcm.server.Sender;
@Stateless
public class ClientePolizasServiceImpl extends EntidadHistoricoDaoImpl implements ClientePolizasService{
	
	private final Logger LOG = Logger.getLogger(ClientePolizasServiceImpl.class);
	
	public static final String SEPARADOR = "-";
	public static final String TIPOPOLIZAMIDAS = "M";
	public static final String TIPOPOLIASEYCOS = "S";
	public static final String RAMOAUTOS = "A";
	public static final String RAMOVIDA= "V";
	public static final String RAMOCASA = "C";
	public static final String RAMODANIO = "D";
	public static final String RAMOPYME = "P";
	public static final String CODIGO_PRODUCTO_PYME = "07";
	public static final String IDPOL = "IDTOPOLIZA";
	//Notificacion de renovacion APP Afirme Seguros
	public static final BigDecimal ID_GRUPO_PARAMETRO_GENERAL_CONFIGURACION = new BigDecimal("13");
	public static final BigDecimal CDG_NOTIFICACION_RENOVACION = new BigDecimal("136090");
	
	
	@PersistenceContext
	private EntityManager em;	
	private JdbcTemplate jdbcTemplate;
	private SimpleJdbcCall existePolizaMidas;
	private SimpleJdbcCall existePolizaSeycos;
	private SimpleJdbcCall eliminarPoliza;
	private SimpleJdbcCall recibosMidas;
	private SimpleJdbcCall recibosSeycos;
	private SimpleJdbcCall registroDuplicado;
	private SimpleJdbcCall correosNotificacion;
	private SimpleJdbcCall guardarCorreosNotificacion;
	private SimpleJdbcCall eliminarCorreoNotificacion;
	private SimpleJdbcCall registroDescarga;
	private SimpleJdbcCall consultaTipoNotificaciones;

	private String  fileName;
	
	@EJB
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	
	@EJB
	protected EntidadService entidadService;
	private Long validOnMillis;
	private Long recordFromMillis;
	private Date validOn;
	private Date recordFrom;
	private List<Integer> datosPoliza;
	
	private List<EndosoDTO> endosoPolizaList = new ArrayList<EndosoDTO>(1);
	private String polizaNo;
	private String correoGerencia;
	private String URL;
	private Date fechaVigenciaSey;
	@EJB
	public IncisoViewService incisoViewService;
	@EJB
	public MailService mailService;
	//private String CORREOPRUEBAS;
	private UsuarioService usuarioService;
	
	@EJB
	private PrintReportClient printReportClient;
	
	@EJB
	private NotificacionClienteMovilService notificacionClienteService;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	EndosoService endosoService;
	
	@EJB
	public void setEndosoService(EndosoService endosoService) {
		this.endosoService = endosoService;
	}	
	private ImpresionRecibosService impresionRecibosService;

	@EJB
	public void setImpresionRecibosService(
			ImpresionRecibosService impresionRecibosService) {
		this.impresionRecibosService = impresionRecibosService;
	}	

	@EJB
	private PolizaRenovadaEnvioDocMovilDao polizaRenovadaEnvioDocMovilDao;
	
	@EJB
	EntidadContinuityService entidadContinuityService;
	
	@EJB
	protected ParametroGeneralService parametroGeneralService;
	
	@SuppressWarnings("unused")
	private DataSource dataSource;	
	
	public Long getRecordFromMillis() {
		return recordFromMillis;
	}

	public void setRecordFromMillis(Long recordFromMillis) {
		this.recordFromMillis = recordFromMillis;
	}

	public Long getValidOnMillis() {
		return validOnMillis;
	}

	public void setValidOnMillis(Long validOnMillis) {
		this.validOnMillis = validOnMillis;
	}
	
	@Resource	
	private TimerService timerService;
	
	//private AgenteMidasService agenteMidasService;
	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
				
		//Consulta de polizas en sistema central en MIDAS
		this.existePolizaMidas = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("existePolizaMidas")
		.declareParameters(new SqlParameter("pPolizaNo", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<ClientePolizas>() {
					@Override
					public ClientePolizas mapRow(ResultSet rs, int index) throws SQLException {
						ClientePolizas polizas = new ClientePolizas();
						polizas.setPolizaNo(rs.getString("poliza"));
						return polizas;
					}
		}));

		//Consulta de polizas en sistema central en SEYCOS
		this.existePolizaSeycos = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("existePolizaSeycos")
		.declareParameters(
				new SqlParameter("pPolizaNo", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<ClientePolizas>() {
					@Override
					public ClientePolizas mapRow(ResultSet rs, int index) throws SQLException {
						ClientePolizas polizas = new ClientePolizas();
						polizas.setPolizaNo(rs.getString("poliza"));
						return polizas;
					}
		}));
		
					
		//elimina informacion en BD
		this.eliminarPoliza = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("eliminarPoliza")
		.declareParameters(new SqlParameter("pID", Types.INTEGER)
		);
		
		
		this.recibosMidas = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("getRecibosMidas")
		.declareParameters(
				new SqlParameter("pID", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Recibo>() {
					@Override
					public Recibo mapRow(ResultSet rs, int index) throws SQLException {
						Recibo recibo = new Recibo();
						recibo.setId(rs.getLong("ID_RECIBO"));
						recibo.setFolio(rs.getLong("NUM_FOLIO_RBO"));
						recibo.setFechaInicio(rs.getDate("F_CUBRE_DESDE"));
						recibo.setFechaFin(rs.getDate("F_CUBRE_HASTA"));
						recibo.setEstatus(rs.getString("SIT_RECIBO"));
						recibo.setPrimaTotal(rs.getString("IMP_PRIMA_TOTAL"));
						recibo.setLlaveFiscal(rs.getString("LLAVEFISCAL"));
						return recibo;
					}
		}));
		
		this.recibosSeycos = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("getRecibosSeycos")
		.declareParameters(
				new SqlParameter("pID", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Recibo>() {
					@Override
					public Recibo mapRow(ResultSet rs, int index) throws SQLException {
						Recibo recibo = new Recibo();
						recibo.setId(rs.getLong("ID_RECIBO"));
						recibo.setFolio(rs.getLong("NUM_FOLIO_RBO"));
						recibo.setFechaInicio(rs.getDate("F_CUBRE_DESDE"));
						recibo.setFechaFin(rs.getDate("F_CUBRE_HASTA"));
						recibo.setEstatus(rs.getString("SIT_RECIBO"));
						recibo.setPrimaTotal(rs.getString("IMP_PRIMA_TOTAL"));
						recibo.setLlaveFiscal(rs.getString("LLAVEFISCAL"));
						return recibo;
					}
		}));
				
		//Consulta duplicidad de registros
		this.registroDuplicado = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("registroDuplicado")
		.declareParameters(
				new SqlParameter("pPolizaNo", Types.VARCHAR),
				new SqlParameter("pUsuario", Types.VARCHAR),
				new SqlParameter("pInciso", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<ClientePolizas>() {
					@Override
					public ClientePolizas mapRow(ResultSet rs, int index) throws SQLException {
						ClientePolizas polizas = new ClientePolizas();
						polizas.setPolizaNo(rs.getString("poliza"));
						return polizas;
					}
		}));
		
		//Consulta de registros de correos almacenados
		this.correosNotificacion = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("getCorreosAviso")
		.declareParameters(
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<CorreosCosultaMovil>() {
					@Override
					public CorreosCosultaMovil mapRow(ResultSet rs, int index) throws SQLException {
						CorreosCosultaMovil correos = new CorreosCosultaMovil();
						correos.setId( rs.getString("id"));
						correos.setCorreo(rs.getString("correo"));
						correos.setNombre(rs.getString("nombre"));
						return correos;
					}
		}));
		
		//guardado de informacion de correos en BD
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.guardarCorreosNotificacion = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("setCorreosAviso")
		.declareParameters(
				new SqlParameter("pCorreo", Types.VARCHAR),
				new SqlParameter("pNombre", Types.VARCHAR),
				new SqlParameter("pGerenciaID", Types.VARCHAR)
		);
		
		//elimina el correo de notificacion
		this.eliminarCorreoNotificacion = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("eliminarCorreo")
		.declareParameters(new SqlParameter("pID", Types.INTEGER)
		);
		
		
		//Obtiene los registros de descarga de caratulas
		this.registroDescarga = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("getRegistrosDescarga")
		.declareParameters(
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<InfoDescargaPoliza>() {
					@Override
					public InfoDescargaPoliza mapRow(ResultSet rs, int index) throws SQLException {
						InfoDescargaPoliza infoDescarga = new InfoDescargaPoliza();
						infoDescarga.setUsuario( rs.getString("usuario"));
						infoDescarga.setEstatus(rs.getString("estatus"));
						infoDescarga.setDescargas(rs.getString("descargas"));
						
						return infoDescarga;
					}
		}));
		
			
		this.consultaTipoNotificaciones = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_cliente_consulta")
		.withProcedureName("getTipoNotificacion")
		.declareParameters(
				new SqlParameter("pUsuario", Types.VARCHAR),
				new SqlParameter("pTipoNotificacion", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<NotificacionCliente>() {
					@Override
					public NotificacionCliente mapRow(ResultSet rs, int index) throws SQLException {
						NotificacionCliente notificacion = new NotificacionCliente();
						
						//notificacion.setFechaCreacion("fechacreacion");
						notificacion.setMensaje("mensaje");
						notificacion.setTipoNotificacion("tiponotificacion");
						return notificacion;
					}
		}));
		
	}
	
	/**
	 * Mostrar las pólizas relacionadas al cliente por Ramo
	 */
	public List<ClientePolizas> buscarPolizas(InfoPolizaParameter param){
		List<ClientePolizas> polizasCliente = new ArrayList<ClientePolizas>();
		ErrorBuilder eb = new ErrorBuilder();
		if(param.getRamo().isEmpty()||param.getRamo()==null){
			throw new ApplicationException(eb.addFieldError("Ramo",
			"Requerido"));
		}
		else{
			List<PolizasClienteMovil> lista=findByUsuarioRamo(usuarioService.getUsuarioActual().getNombreUsuario(), param.getRamo());
			for(PolizasClienteMovil object : lista){
				ClientePolizas entidad = new ClientePolizas();
				entidad = transformarPolizaclienteModel(object);
				polizasCliente.add(entidad);
			}
		}
		return polizasCliente;
	}
	
	/*
	 * Obtiene todas las pólizas de un usuario
	 */
	public List<ClientePolizas> buscarPolizasPorUsuario(){
		List<ClientePolizas> polizasCliente = new ArrayList<ClientePolizas>();
		
		List<PolizasClienteMovil> lista=findByProperty("usuario", usuarioService.getUsuarioActual().getNombreUsuario());
		for(PolizasClienteMovil object : lista){
			ClientePolizas entidad = new ClientePolizas();
			entidad = transformarPolizaclienteModel(object);
			polizasCliente.add(entidad);
		}
		
		return polizasCliente;
	}
	
	/**
	 * Método que obtiene las pólizas de un usuario.
	 * @param userName
	 * @return
	 */
	public List<ClientePolizas> buscarPolizasPorUsuario(String userName){
		List<ClientePolizas> polizasCliente = new ArrayList<ClientePolizas>();
		
		List<PolizasClienteMovil> lista=findByProperty("usuario", userName);
		ParametroGeneralDTO parametro = new ParametroGeneralDTO();
		try{
			parametro = parametroGeneralService.findById(new ParametroGeneralId(
					ID_GRUPO_PARAMETRO_GENERAL_CONFIGURACION,
					CDG_NOTIFICACION_RENOVACION) );
		}
		catch(Exception e){
			LOG.error("Ocurrio un error al obtener parametro general CDG_NOTIFICACION_RENOVACION", e);
		}
		for(PolizasClienteMovil object : lista){
			ClientePolizas entidad = new ClientePolizas();			
			if( object.getPolizaDTO() != null 
					&& ( (object.getPolizaDTO().getCotizacionDTO().getFechaFinVigencia().compareTo(new Date())>0)
					|| !(parametro != null && parametro.getValor() != null && parametro.getValor().equals("1")) )){
				entidad = transformarPolizaclienteModel(object);
				polizasCliente.add(entidad);
			} else if (object.getIdCotizacionSeycos()!=null) {
			    getDatosSeycos(object.getId().toString());
			    if( fechaVigenciaSey != null 
			    		&& ( (fechaVigenciaSey.compareTo(new Date ())>0) 
			    		|| !(parametro != null && parametro.getValor() != null && parametro.getValor().equals("1"))	)) {
			    	entidad = transformarPolizaclienteModel(object);
					polizasCliente.add(entidad);
			    }
			}			
		}
		
		return polizasCliente;
	}
	
	/**
	 * Metodo para guardar la relacion de polizas
	 * y usuarios
	 **/
	@EJB
	private PolizaFacadeRemote polizaFacadeRemote;
	
	public void guardarPoliza(InfoPolizaParameter params){
		
		ErrorBuilder eb = new ErrorBuilder();
		if(params.getPolizaNo()!=null||StringUtils.isNotBlank(params.getPolizaNo())){
			PolizaDTO poliza;
			poliza = polizaFacadeRemote.find(params.getNumeroPoliza());
			String tipoPoliza = getTipoPoliza(params.getPolizaNo());
			
			if(poliza!=null){
				PolizasClienteMovil polizasClienteMovil= new PolizasClienteMovil();
				polizasClienteMovil.setUsuario( usuarioService.getUsuarioActual().getNombreUsuario());
				polizasClienteMovil.setDescripcion(params.getDescripcion());
				polizasClienteMovil.setInciso(new Short(params.getInciso()));
				polizasClienteMovil.setOrigenTipoPoliza(tipoPoliza);
				polizasClienteMovil.setPoliza(params.getPolizaNo());
				polizasClienteMovil.setTipo(tipoPoliza);
				polizasClienteMovil.setPolizaDTO(poliza);
				save(polizasClienteMovil);
			}
			else{
				throw new ApplicationException(eb.addFieldError("numeroPoliza",
				"Invalida o Desconocida"));
			}
			
		}
		else{
			throw new ApplicationException(eb.addFieldError("numeroPoliza",
			"Requerido"));
		}
		
		
		
	}
			
	/**
	 * Metodo para verificar en MIDAS /SEYCOS, si existen las polizas a ingresar
	 * 
	 * @param polizaNo
	 *            numero de poliza
	 * @return
	 **/
	@SuppressWarnings({ "unchecked", " null" })
	public boolean existePoliza(String polizaNo) {
		ErrorBuilder eb = new ErrorBuilder();
		List<ClientePolizas> lista = null;
		if ((polizaNo == null) || (StringUtils.isBlank(polizaNo))) {
			throw new ApplicationException(eb.addFieldError("numeroPoliza",
					"Requerido"));
		} else {
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("pPolizaNo", polizaNo);
			Map<String, Object> execute = null;
			String tipoPoliza = getTipoPoliza(polizaNo);
			if(tipoPoliza==null){
				throw new ApplicationException(eb.addFieldError("numeroPoliza",
				"Desconocido"));
			}
			// / Se debe de cambiar y solo mandar el parametro de tipopoliza al
			// procedimiento invocado
			else if (tipoPoliza.trim().equals(TIPOPOLIZAMIDAS)) {
				execute = existePolizaMidas.execute(sqlParameter);
			} else if (tipoPoliza.trim().equals(TIPOPOLIASEYCOS)) {
				execute = existePolizaSeycos.execute(sqlParameter);
			} 
			lista = (List<ClientePolizas>) execute.get("pCursor");
		}

		return lista.size() > 0;
	}
	
	/**
	 * Actualiza los datos alacenados
	 * 
	 * @param id
	 *            el ID de la poliza a modificar
	 */
	public MensajeValidacion actualizarPolizasClienteMovil(InfoPolizaParameter param){
		
		LogDeMidasEJB3.log(this.getClass().getName()
				+ "mis parametros " + param, Level.INFO, null);
		MensajeValidacion mensaje = new  MensajeValidacion();
		
		PolizasClienteMovil polizasClienteMovil = this.findById(new Long(param.getPolizaID()));		
		polizasClienteMovil.setDescripcion(param.getDescripcion());
		polizasClienteMovil.setInciso(new Short(param.getInciso()));
		update(polizasClienteMovil);
		
		mensaje.setEsValido(true);
		mensaje.setMensaje("Se ha Actualizado la Poliza");
		return mensaje;
		
	}

	/**
	 * Metodo para el envio de caratula de la poliza Se debe notificar al agente
	 */
	@EJB
	CotizacionEndosoService cotizacionEndosoService;
	public void enviarCaratula(EnvioCaratulaParameter param){
		ErrorBuilder eb = new ErrorBuilder();
		this.mailService = param.getMailService();
		this.entidadService = param.getEntidadService();
		this.generaPlantillaReporteBitemporalService = param.getGeneraPlantillaReporteBitemporalService();
		//CORREOPRUEBAS = param.getCorreo();
		this.URL = param.getURL();
		//this.userName = usuarioService.getUsuarioActual().getNombreUsuario();
		//para pruebas con usuario
		if(StringUtils.isNotBlank(param.getPolizaID())||param.getPolizaID()!=null){
			
			PolizasClienteMovil polizasClienteMovil= this.findById(new Long(param.getPolizaID()));			
			/////////
			/*PolizaDTO poliza=polizasClienteMovil.getPolizaDTO();
			
			double dias = Utilerias.obtenerDiasEntreFechas(new Date(), poliza.getCotizacionDTO().getFechaInicioVigencia());
			Date fechaInicio = new Date();
			if(dias > 0){
				fechaInicio = poliza.getCotizacionDTO().getFechaInicioVigencia();
			}
			EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndosoByValidFrom(poliza.getIdToPoliza().longValue(), fechaInicio);
			/////////
			ControlEndosoCot controlEndosoCot=endosoService.getControlEndosoCotEAP(poliza.getCotizacionDTO().getIdToCotizacion(),  ultimoEndosoDTO.getId().getNumeroEndoso());
			//ControlEndosoCot controlEndosoCot=this.getControlFromEndoso(poliza, ultimoEndosoDTO.getId().getNumeroEndoso());
			/*try{
				mailAgente=controlEndosoCot.getSolicitud().getAgente().getPersona().getEmail();
			}
			catch(Exception e){
				mailAgente="0david.hernandezr@afirme.com";
			}
			*/
			
			if (polizasClienteMovil.getOrigenTipoPoliza().equals(TIPOPOLIZAMIDAS)) {
				getPDFMidas(param.getPolizaID());
			} else if (polizasClienteMovil.getOrigenTipoPoliza().equals(TIPOPOLIASEYCOS)) {
				getPDFSeycos(param.getPolizaID(), param.getTipoPoliza());
			}
			else{
				throw new ApplicationException(eb.addFieldError("numeroPoliza",
						"Invalido"));
			}
		}
		else{
			throw new ApplicationException(eb.addFieldError("numeroPoliza",
			"Requerido"));
		}
		
		
		guardarRegistroDescarga(param);
	}
	
	private void getPDFSeycos(String polizaID, String tipoPoliza) {
		PrintReportClientImpl print = new PrintReportClientImpl();
		byte[] byteArray = null;
		getDatosSeycos(polizaID);
		String cotizacionID = null;
		if (datosPoliza.size() > 0) {
			cotizacionID =  datosPoliza.get(0).toString();
		}
		
		
		if ("A".equals(tipoPoliza)) {
			byteArray = print.getAutoInsurancePolicy(cotizacionID);			
		} else if ("V".equals(tipoPoliza)) {
			byteArray = print.getLifeInsurancePolicy(cotizacionID);
		} else {
			byteArray = print.getHousePolicy(cotizacionID);
		}
		fileName = "poliza_" + polizaNo + ".pdf";
		sendMailUser(byteArray);
	}

	private List<Integer> getDatosSeycos(String polizaID) {
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT pp.id_cotizacion as cotizacion_ID, tpol.poliza, pp.f_fin_vigencia as vigencia \r\n")
		.append("FROM midas.TRUSRPOLIZA tPol \r\n")
		
		.append("INNER JOIN seycos.pol_poliza pp  \r\n")
		.append("on cast(SUBSTR(tPol.poliza, 1,3) as number) = pp.id_centro_emis  \r\n")
		.append("AND cast(SUBSTR(tPol.poliza, 5, 10)as number) = pp.num_poliza \r\n")
		.append("AND cast(SUBSTR(tPol.poliza, 16, 2)as number) = pp.num_renov_pol  \r\n")
		.append(" AND pp.f_ter_reg = date '4712-12-31' \r\n")
        .append("WHERE  tPol.ID = ? \r\n");
		
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {polizaID}, new RowMapper<List<Integer>>() {

			@Override
			public List<Integer> mapRow(ResultSet rs, int rowNum) throws SQLException {
				datosPoliza =  new ArrayList<Integer>();
				datosPoliza.add(rs.getInt("cotizacion_ID"));
				polizaNo = rs.getString("poliza");
				fechaVigenciaSey = rs.getDate("vigencia");
				
				return datosPoliza;
			}
		});
		
	}

	private void sendMailAgente(String correoAgente) {
		try{		
			List<String> destinatarios = new ArrayList<String>();
			StringBuilder message = new StringBuilder();
			
		 message.append("le informamos que mediante la app movil de consultas se ha solicitado \r\n")
				.append("el envio de caratula de la póliza \r\n")
				.append(polizaNo)
				.append("\r\n")
				.append("Si desea bloquear al usuario presione la siguiente liga o copiela en su navegador\r\n")
				//direccion para pruebas
				.append("http://cap2.afirme.com.mx/MidasWeb/rest/cliente/polizas/clientePolizas-movil/bloqueaUsuario.action?username=")
			.append(usuarioService.getUsuarioActual().getNombreUsuario());
			
			if (StringUtils.isNotBlank(correoAgente)) {
				destinatarios.add(correoAgente);
				mailService.sendMail(destinatarios, "Aviso descarga de Caratula", 
						message.toString(), null, "descarga de Caratula", "Estimado(a): "+usuarioService.getUsuarioActual().getNombreCompleto());
			}
			
					
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

	/**
	 * Metodo para el envio de la caratula al correo registrado del usuario
	 * 
	 * @param filePoliza
	 *            arreglo de bytes para la caratula de la poliza
	 */
	private void sendMailUser(byte[] filePoliza) {
		ByteArrayAttachment attachment = null;
		List<String> destinatarios = new ArrayList<String>();
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		try{			
			attachment = new ByteArrayAttachment(fileName + ".pdf", TipoArchivo.PDF, filePoliza);		
			if(attachment != null){	
				//if(StringUtils.isNotBlank(CORREOPRUEBAS))
					//destinatarios.add(CORREOPRUEBAS);
				destinatarios.add(usuarioService.getUsuarioActual().getEmail());
				/*if (StringUtils.isNotBlank(mailAgente)) {
					destinatarios.add(mailAgente);
				}*/
				adjuntos.add(attachment);
				mailService.sendMail(destinatarios, "Caratula_" + fileName, 
						"Ha solicitado la caratula de su póliza, favor de verificar los documentos anexos", adjuntos, "Estimado(a): "+usuarioService.getUsuarioActual().getNombreCompleto().trim());	
			}		 
		}catch(Exception ex){
			ex.printStackTrace();
		}
		
	}

	/**
	 * Obtiene un arreglo de bytes para el envio de la caratula por medio de
	 * correo electronico
	 * 
	 * @param polizaID
	 *            el id de la poliza a consultar
	 */
	private void getPDFMidas(String polizaID) { 
		int toPolizaID = 0, inciso = 0, idAgente = 0;
		short claveTipoEndoso = 0;
		
		Date today = new Date();
		if(getValidOnMillis() == null || getValidOnMillis() == 0){
			setValidOn(today);
			setValidOnMillis(today.getTime());
		}else{
			Date accurateDate = new Date(getValidOnMillis().longValue());
			setValidOn(accurateDate);
		}
		
		if(getRecordFromMillis() == null || getRecordFromMillis() == 0){
			setRecordFrom(today);
			setRecordFromMillis(today.getTime());
		}else{
			Date accurateRecordFromDate = new Date(getRecordFromMillis());
			setRecordFrom(accurateRecordFromDate);
		}
		//////////////
		getDatosMidas(polizaID);
		//Date today = new Date();
		//setValidOnMillis(today.getTime());
		//setRecordFromMillis(today.getTime());//// verficar fecha de obtencion
		if (datosPoliza.size() > 0) {
			
			inciso = datosPoliza.get(1);
			toPolizaID = datosPoliza.get(0);
			claveTipoEndoso = datosPoliza.get(2).shortValue();
			idAgente = datosPoliza.get(3);
		}
		Date accurateDate = new Date(getValidOnMillis());
		setValidOn(accurateDate);
		try{
			DateTime validOnDT = new DateTime(getValidOnMillis());
			DateTime recordFromDT = new DateTime(getRecordFromMillis());
			
			endosoPolizaList = endosoService.findByPropertyWithDescriptions("id.idToPoliza", toPolizaID, Boolean.TRUE);
			validOnDT = new DateTime(endosoPolizaList.get(0).getValidFrom());
			
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(toPolizaID));	
			double dias = Utilerias.obtenerDiasEntreFechas(new Date(), polizaDTO.getCotizacionDTO().getFechaInicioVigencia());
			Date fechaInicio = new Date();
			LogDeMidasEJB3.log(this.getClass().getName()
					+ ".getPDFMidas... dias: " + dias, Level.INFO, null);
			if(dias > 0){
				fechaInicio = polizaDTO.getCotizacionDTO().getFechaInicioVigencia();
				//recordFromDT = polizaDTO.getCotizacionDTO().getFechaInicioVigencia();
			}
			LogDeMidasEJB3.log(this.getClass().getName()
					+ ".getPDFMidas... fechaInicio: " + fechaInicio, Level.INFO, null);
			EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndosoByValidFrom(new Long(toPolizaID), fechaInicio);
			//ultimoEndosoDTO.get
			if (ultimoEndosoDTO != null) {
				recordFromDT= new DateTime(ultimoEndosoDTO.getRecordFrom());
				LogDeMidasEJB3.log(this.getClass().getName()
						+ ".getPDFMidas...ultimoEndosoDTO!=null recordFromDT: " + recordFromDT, Level.INFO, null);
			} else {
				LogDeMidasEJB3.log(this.getClass().getName()
						+ ".getPDFMidas...ultimoEndosoDTO==null recordFromDT: " + recordFromDT, Level.INFO, null);
				recordFromDT = new DateTime(fechaInicio.getTime());					
			}
			LogDeMidasEJB3.log(this.getClass().getName()
					+ ".getPDFMidas...: recordFromDT:" + recordFromDT, Level.INFO, null);
			TransporteImpresionDTO transporte = 
					generaPlantillaReporteBitemporalService.imprimirPoliza(
							new BigDecimal(toPolizaID), new Locale("es","MX"), validOnDT, recordFromDT, true, 
							false, false, false, inciso, inciso, claveTipoEndoso, true, false);
			LogDeMidasEJB3.log(this.getClass().getName()
					+ "TransporteImpresionDTO:=>" + transporte, Level.INFO, null);

			fileName = "poliza_" + polizaDTO.getNumeroPolizaFormateada() + ".pdf";							
							
			sendMailUser(transporte.getByteArray());
			sendMailAgente(getMailAgenteMidas(idAgente));

		}catch(Exception e){
			e.printStackTrace();
		}

	}

	/**
	 * Metodo para obtener la direccion de correo del agente
	 * 
	 * @param idAgente
	 *            el id del Agente a consultar
	 * @return
	 */
	private String getMailAgenteMidas(int idAgente) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT DISTINCT persona.email  ") 
		.append("FROM MIDAS.toAgente agente ") 
		.append("INNER JOIN MIDAS.VW_PERSONA persona ON persona.idpersona = agente.idPersona ") 
		.append("WHERE agente.id = ? "); 
		
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {idAgente}, new RowMapper<String>() {

			@Override
			public String mapRow(ResultSet rs, int arg1) throws SQLException {
				String correo = null;
				correo = rs.getString("email");
				return correo;
			}			
		});
	}

	/**
	 * Obtiene los datos necesarios para la impresion de la caratula
	 * 
	 * @param polizaID
	 *            el id de la poliza a consultar
	 */
	private List<Integer> getDatosMidas(String polizaID) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT * FROM ( ")
		.append("SELECT tPol.inciso, pol.idtopoliza, tEnd.claveTipoEndoso, tPol.poliza, sol.codigoagente \r\n")
		.append("FROM midas.TRUSRPOLIZA tPol \r\n")
		.append("INNER JOIN midas.TOPOLIZA pol on pol.IDTOPOLIZA = tPol.IDTOPOLIZA \r\n")		   
        .append("INNER JOIN MIDAS.toendoso tEnd on tEnd.idtopoliza = pol.idtopoliza AND tEnd.idtocotizacion = pol.idtoCotizacion \r\n")
        .append("INNER JOIN MIDAS.tosolicitud sol on sol.idtosolicitud = pol.idtosolicitud \r\n")
        .append("WHERE  tPol.ID = ? \r\n")
        .append(" ) WHERE rownum = 1 ");
		
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {polizaID}, new RowMapper<List<Integer>>() {

			@Override
			public List<Integer> mapRow(ResultSet rs, int rowNum) throws SQLException {
				datosPoliza =  new ArrayList<Integer>();
				datosPoliza.add(rs.getInt("idtopoliza"));
				datosPoliza.add(rs.getInt("inciso"));
				datosPoliza.add(rs.getInt("claveTipoEndoso"));
				datosPoliza.add(rs.getInt("codigoagente"));
				polizaNo = rs.getString("poliza");
				
				return datosPoliza;
			}
		});
	}

	/**
	 * Metodo para buscar y mostrar los recios que estan relacionados a una
	 * poliza
	 * 
	 * @param id
	 *            el ID de la poliza a consultar
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Recibo> mostrarRecibos(String polizaID) {

		List<Recibo> lstRecibos = null;
		Map<String, Object> execute = null;
		ErrorBuilder eb = new ErrorBuilder();
		if (polizaID != null||StringUtils.isNotBlank(polizaID)) {
			PolizasClienteMovil polizasClienteMovil = this.findById(new Long(
					polizaID));

			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter.addValue("pID", polizaID);

			if (polizasClienteMovil.getOrigenTipoPoliza().equals(TIPOPOLIZAMIDAS)) {
				execute = recibosMidas.execute(sqlParameter);
			} else if (polizasClienteMovil.getOrigenTipoPoliza().equals(TIPOPOLIASEYCOS)) {
				execute = recibosSeycos.execute(sqlParameter);
			}
			else{
				throw new ApplicationException(eb.addFieldError("numeroPoliza",
				"Invalido o Desconocido"));
			}
			lstRecibos = (List<Recibo>) execute.get("pCursor");
			// return listaPolizas;
		} else {
			throw new ApplicationException(eb.addFieldError("numeroPoliza",
					"Requerido"));
		}
		return lstRecibos;
	}


	/**
	 * Metodo para eliminar la poliza relacionada al cliente
	 * 
	 * @param id
	 *            ID de la poliza a eliminar
	 */
	public void eliminarPoliza(String id){
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pID", id);
		
		this.eliminarPoliza.execute(sqlParameter);
	}
	
	/**
	 * Obtiene la informacion de una poliza especifica
	 * 
	 * @param polizaID
	 *            el ID de la poliza a consultar
	 * @return
	 */
	public ClientePolizas infoPoliza(String polizaID){		
		ClientePolizas polizaDet = gePolizaDet(polizaID);
		return polizaDet;
	}

	private ClientePolizas gePolizaDet(String polizaID) {
		 ErrorBuilder eb = new ErrorBuilder();
		ClientePolizas entidad = new ClientePolizas();
		 if ((polizaID == null) || (StringUtils.isBlank(polizaID))) {
				throw new ApplicationException(eb.addFieldError("numeroPoliza",
						"Requerido"));
		} else {
			PolizasClienteMovil polizasClienteMovil = this.findById(new Long(
					polizaID));
			if (polizasClienteMovil != null) {
				entidad = transformarPolizaclienteModel(polizasClienteMovil);
			} else {
					throw new ApplicationException(eb.addFieldError("numeroPoliza",
						"Poliza no Encontrada"));
				}
		 }
		return entidad;
		
		}

	//@Override
	public MensajeValidacion bloqueaUsuario(String username) {
		LogDeMidasEJB3.log(this.getClass().getName()+"==>mi usuario desBloqueado"+username, Level.INFO,
				null);
		/*MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pUsuario", usuarioService.getUsuarioActual().getNombreUsuario());
		
		this.bloqueoUsuario.execute(sqlParameter);*/
		List<PolizasClienteMovil> polizasClienteMovilList =this.findByProperty("usuario",username);
		PolizasClienteMovil polizasClienteMovil=null;
		try{
			polizasClienteMovil=polizasClienteMovilList.get(0);
		}
		catch(Exception e){
			polizasClienteMovil=null;
		}
		MensajeValidacion mensaje = new  MensajeValidacion();
		if(polizasClienteMovil!=null){					
			polizasClienteMovil.setBloqueado("Y");
			update(polizasClienteMovil);
			
			mensaje.setEsValido(true);
			mensaje.setMensaje("Se ha Bloqueado el Usuario");
			LogDeMidasEJB3.log(mensaje+"==> Metodo bloqueaUsuario", Level.INFO,
					null);
			//return mensaje;
		}		
		return mensaje;
	}

	//@Override
	public MensajeValidacion desbloqueaUsuario(String username) {
		LogDeMidasEJB3.log(this.getClass().getName()+"==>mi usuario desBloqueado"+username, Level.INFO,
				null);
		List<PolizasClienteMovil> polizasClienteMovilList =this.findByProperty("usuario",username);
		PolizasClienteMovil polizasClienteMovil=null;
		try{
			polizasClienteMovil=polizasClienteMovilList.get(0);
		}
		catch(Exception e){
			polizasClienteMovil=null;
		}
		MensajeValidacion mensaje = new  MensajeValidacion();
		if(polizasClienteMovil!=null){
			polizasClienteMovil.setBloqueado("N");
			update(polizasClienteMovil);
			mensaje.setEsValido(true);
			mensaje.setMensaje("Se ha Desbloqueado el Usuario");
			LogDeMidasEJB3.log(mensaje+"==>Metodo desbloqueaUsuario", Level.INFO,
					null);
		}		
		
		return mensaje;
		
	}

	@Override
	public void solicitudDesbloqueo(EnvioCaratulaParameter param) {
		 PolizasClienteMovil polizasClienteMovil= this.findById(new Long(param.getPolizaID()));
		/* if ((polizaNo == null) || (StringUtils.isBlank(polizaNo))) {
				throw new ApplicationException(eb.addFieldError("numeroPoliza",
						"Requerido"));
		}*/ 
		if (mailService == null) {
			this.mailService = param.getMailService();
		} 
		
		if (entidadService == null) {
			this.entidadService = param.getEntidadService();
		}
			
		if (generaPlantillaReporteBitemporalService == null) {
			this.generaPlantillaReporteBitemporalService = param.getGeneraPlantillaReporteBitemporalService();
		}
		
		if (polizasClienteMovil.getOrigenTipoPoliza().equals((TIPOPOLIZAMIDAS))) {
			getMailfromMidas(param.getPolizaID());
		} else if (polizasClienteMovil.getOrigenTipoPoliza().equals((TIPOPOLIASEYCOS))) {
			getMailFromSeycos(param.getPolizaID());
		}
		
		/*int idAgente = 0;
		getDatosMidas(param.getPolizaID());
		if (datosPoliza.size() > 0) {
			idAgente = datosPoliza.get(3);
		} else {
			getDatosSeycos(param.getPolizaID());
			if (condition) {
				
			}
		}*/
		//para pruebas con usuario
		
		try{		
			List<String> destinatarios = new ArrayList<String>();
			StringBuilder message = new StringBuilder();
			
			message.append("le informamos que el usuario ").append(param.getUsername()).append(" \r\n")
				.append("con la póliza \r\n")
				.append(polizaNo).append("\r\n")
				.append(", está solicitando su desbloqueo en la app móvil. \r\n")
				/*.append("Si desea bloquear al usuario, presione la siguiente liga  \r\n")
				//direccion para pruebas
			.append("http://cap2.afirme.com.mx/MidasWeb/rest/cliente/polizas/clientePolizas-movil/desbloqueaUsuario.action?username=")
			.append(param.getUsername())*/;
			
			destinatarios.add(correoGerencia);
			//if(StringUtils.isNotBlank(CORREOPRUEBAS))
				//destinatarios.add(CORREOPRUEBAS);
			//para pruebas con usuario
			/*if (StringUtils.isNotBlank(mailAgente)) {
				destinatarios.add(mailAgente);
			}// Fin*/
			if (destinatarios.size() > 0) {				
				mailService.sendMail(destinatarios, "Solicitud Desbloqueo", message.toString(), null, "Desbloqueo", "Hola");
			}			
					
		}catch(Exception ex){
			ex.printStackTrace();
		}		
	}
	
	private String getMailFromSeycos(String polizaID) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT pp.id_cotizacion as cotizacion_ID, tpol.poliza ,pp.id_gerencia, mail.correo, tPol.ID \r\n")
		.append("FROM midas.TRUSRPOLIZA tPol \r\n")		
		.append("INNER JOIN SEYCOS.pol_poliza pp \r\n")
		.append("on cast(SUBSTR(tPol.poliza, 1,3) as number) = pp.id_centro_emis \r\n")
		.append("AND cast(SUBSTR(tPol.poliza, 5, 10)as number) = pp.num_poliza \r\n") 
		.append("AND cast(SUBSTR(tPol.poliza, 16, 2)as number) = pp.num_renov_pol \r\n")
		.append("AND pp.f_ter_reg = date '4712-12-31' \r\n")
		.append("inner join  midas.trusrcorreo mail on pp.id_gerencia = mail.id_gerencia \r\n")
		.append("WHERE  tPol.ID = ?  \r\n");

		try {
			return jdbcTemplate.queryForObject(sql.toString(), new Object[] {polizaID}, new RowMapper<String>() {				
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					polizaNo = rs.getString("poliza");
					correoGerencia = rs.getString("correo");
					return StringUtils.EMPTY;
				}
			});
		} catch (Exception e) {	
			return correoGerencia = StringUtils.EMPTY;
		}
	}

	private void getMailfromMidas(String polizaID) {
		StringBuilder sql = new StringBuilder();
		sql.append("SELECT tPol.poliza, mail.correo \r\n")
		.append("FROM midas.TRUSRPOLIZA tPol \r\n")
		.append("INNER JOIN midas.TOPOLIZA pol on cast(SUBSTR(tPol.poliza, 1,2) as number) = cast(pol.codigoproducto as number) \r\n")
		.append("AND cast(SUBSTR(tPol.poliza, 3, 2)as number) = cast(pol.codigotipopoliza as number) \r\n") 
		.append("AND cast(SUBSTR(tPol.poliza, 6, 6)as number) = cast(pol.numeropoliza  as number) \r\n")
		.append("AND cast(SUBSTR(tPol.poliza, 14, 2)as number)  = cast(pol.numerorenovacion as number) \r\n")
		.append("INNER JOIN SEYCOS.conv_midas_seycos cms on cms.id_midas = pol.idtopoliza||'|0' AND cms.tipo = 'POLIZA' \r\n")
		.append("inner join SEYCOS.pol_poliza pp on cms.id_seycos = pp.id_cotizacion AND pp.f_ter_reg = date '4712-12-31' \r\n")
		.append("inner join  midas.trusrcorreo mail on pp.id_gerencia = mail.id_gerencia \r\n")
		.append("WHERE  tPol.ID = ? \r\n");

		try {
			jdbcTemplate.queryForObject(sql.toString(), new Object[] {polizaID}, new RowMapper<String>() {				
				@Override
				public String mapRow(ResultSet rs, int rowNum) throws SQLException {
					polizaNo = rs.getString("poliza");
					correoGerencia = rs.getString("correo");
					return correoGerencia;
				}
			});
		} catch (Exception e) {	
			correoGerencia  = StringUtils.EMPTY;
		}		
	}

	/**
	 * Metodo para bucar registros duplicados
	 * 
	 **/
	public boolean existeRegistro(InfoPolizaParameter params){
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pUsuario", usuarioService.getUsuarioActual().getNombreUsuario())
			.addValue("pPolizaNo", params.getPolizaNo())
			.addValue("pInciso", params.getInciso());
		
		Map<String, Object> execute = registroDuplicado.execute(sqlParameter);	
		@SuppressWarnings("unchecked")
		List<ClientePolizas> lista = (List<ClientePolizas>) execute.get("pCursor");		
		
		return lista.size() > 0;
	}
	
	/**
	 * Metodo para bucar correos para notificacion
	 * 
	 **/
	public List<CorreosCosultaMovil> getCorreosNotificacion(){
		
		Map<String, Object> execute = correosNotificacion.execute();	
		@SuppressWarnings("unchecked")
		List<CorreosCosultaMovil> lista = (List<CorreosCosultaMovil>) execute.get("pCursor");		
		
		return lista;
	}
	
	/**
	 * Metodo para guardar los correos a los que se le envia notificacion de
	 * desbloqueo
	 * 
	 * @param pCorreo
	 *            el correo a registrar
	 */
	public void guardarCorreo(String pCorreo, String pNombre, String gerenciaID){
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pCorreo", pCorreo)
					.addValue("pGerenciaID", gerenciaID)
					.addValue("pNombre", pNombre);
		//UUID idOne = UUID.randomUUID();
		this.guardarCorreosNotificacion.execute(sqlParameter);
	}
	
	/**
	 * Metodo para eliminar el registro del correo para notificacion
	 * 
	 * @param id
	 *            el identificador del registro
	 */
	public void eliminarCorreo(String id){
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pID", id);

		this.eliminarCorreoNotificacion.execute(sqlParameter);
	}
	
	@EJB
	PolizaDescargaMovilService polizaDescargaMovilService;
	private void guardarRegistroDescarga(EnvioCaratulaParameter params){
		
		List<PolizaDescargaMovil> polizaDescargaMovilList =polizaDescargaMovilService.findByProperty("poliza",params.getPolizaID());
		PolizaDescargaMovil polizaDescargaMovil=null;
		try{
			polizaDescargaMovil=polizaDescargaMovilList.get(0);
		}
		catch(Exception e){
			polizaDescargaMovil=null;
		}
		
		if(polizaDescargaMovil==null){
			polizaDescargaMovil= new PolizaDescargaMovil();
			polizaDescargaMovil.setFecha(new Date());
			polizaDescargaMovil.setPoliza(params.getPolizaID());
			polizaDescargaMovil.setDescargas(new Long("1"));
			polizaDescargaMovil.setUsuario(usuarioService.getUsuarioActual().getNombre());
			polizaDescargaMovil.setNombreUsuarioLog(usuarioService.getUsuarioActual().getNombreUsuario());
			polizaDescargaMovilService.save(polizaDescargaMovil);
		}
		else{
			polizaDescargaMovil.setDescargas(polizaDescargaMovil.getDescargas()+1);
			polizaDescargaMovilService.update(polizaDescargaMovil);			
		}	
		
	}

	/**
	 * Metodo para el envio de comprobantes de los recibos
	 */
	
	@EJB
	private GeneraPlantillaReporteBitemporalService GeneraPlantillaReporteBitemporalService;
	
	
	public void envioRecibos(EnvioCaratulaParameter params) {
		LogDeMidasEJB3.log(this.getClass().getName() + "--> envioRecibos",
				Level.INFO, null);

		StringBuilder message = new StringBuilder();
		message.append(
				"Ha solicitado el envio de documentos del Recibo de su poliza \r\n")
				.append("favor de revisar los archivos adjuntos \r\n");

		StringBuilder title = new StringBuilder();
		title.append("Recibos de Póliza");

		StringBuilder subject = new StringBuilder();
		subject.append("caratula poliza");

		StringBuilder greeting = new StringBuilder();
		greeting.append("Estimado(a): "
				+ usuarioService.getUsuarioActual().getNombreCompleto().trim());

		params.setMessage(message.toString());
		params.setTitle(title.toString());
		params.setSubject(subject.toString());
		params.setGreeting(greeting.toString());

		envioPDFyXML(params);
		LogDeMidasEJB3.log(this.getClass().getName() + "<-- envioRecibos",
				Level.INFO, null);

	}
	
	public void envioPDFyXML(EnvioCaratulaParameter params){
		LogDeMidasEJB3.log(this.getClass().getName()+  "--> envioPDFyXML" , Level.INFO, null);
		this.mailService = params.getMailService();
		this.entidadService = params.getEntidadService();
		this.generaPlantillaReporteBitemporalService = params.getGeneraPlantillaReporteBitemporalService();
				
		List<String> destinatarios = new ArrayList<String>();
		try {
			//Lista de Archivos para el atach. Será uno solo 
			List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment pdf = new ByteArrayAttachment();
			//Byte de xml
			ByteArrayAttachment xml = new ByteArrayAttachment();
			byte[] reciboFiscalPDF   = null;
			byte[] reciboFiscalXML   = null;
			
			reciboFiscalPDF = impresionRecibosService.getReciboFiscal(params.getLlaveFiscal());
			if (reciboFiscalPDF!= null && reciboFiscalPDF.length>0) {
				pdf.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
				pdf.setNombreArchivo("Factura.pdf");
				LogDeMidasEJB3.log(this.getClass().getName()+  ": Preparando PDF" , Level.INFO, null);							
				pdf.setContenidoArchivo( reciboFiscalPDF);								
				attachment.add(pdf);
			} else{
				LogDeMidasEJB3.log(this.getClass().getName()+  ": No se Extrajo el archivo PDF" , Level.INFO, null);		
			}
			
			reciboFiscalXML = getReciboFiscalXML(params.getLlaveFiscal());
			if (reciboFiscalXML!= null && reciboFiscalXML.length>0) {
				xml.setTipoArchivo(ByteArrayAttachment.TipoArchivo.DESCONOCIDO );
			  	xml.setNombreArchivo("Factura.xml");
			  	LogDeMidasEJB3.log(this.getClass().getName()+  " : Preparando XML" , Level.INFO, null);
				xml.setContenidoArchivo( reciboFiscalXML);
				attachment.add( xml); 
			} else{
				LogDeMidasEJB3.log(this.getClass().getName()+  ": No se Extrajo el archivo XML" , Level.INFO, null);		
			}
			
			LogDeMidasEJB3.log("--> attachment.size:"+attachment.size(), Level.INFO, null);
			if(params.getCorreo()!=null &&params.getCorreo()!=""){
				destinatarios.add(usuarioService.getUsuarioActual().getEmail()+"," +params.getCorreo());
			}else{
				destinatarios.add(usuarioService.getUsuarioActual().getEmail());
			}
			
			
			mailService.sendMail(destinatarios, params.getTitle(),params.getMessage(), attachment, 
					params.getSubject(),params.getGreeting());
			
		} catch (Exception e) {
			e.printStackTrace();
		}		
		
		LogDeMidasEJB3.log(this.getClass().getName()+  "<-- envioPDFyXML" , Level.INFO, null);
		
	}
	
	public byte[] getReciboFiscalXML(String llaveFiscal){
		return printReportClient.getDigitalBill(llaveFiscal, "xml");
	}
	
	@Override
	public void enviarCaratulaPolizaPDF(TransporteImpresionDTO param, String destinatario, EnvioCaratulaParameter params){
		try{
			List<String> destinatarios = new ArrayList<String>();
			List<ByteArrayAttachment> attach = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment pdf = new ByteArrayAttachment();
			if(param != null && param.getByteArray().length > 0){
				param.getByteArray();
    			pdf.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
    			pdf.setNombreArchivo("Factura.pdf");
    			pdf.setContenidoArchivo(param.getByteArray());
    			attach.add(pdf);
			}
			if(destinatario != null && destinatario.trim().length() > 0){
				destinatarios.add(destinatario);
				mailService.sendMail(destinatarios, params.getTitle(), params.getMessage(), attach, params.getSubject(), params.getGreeting());
			}
		}catch (Exception e){
			LOG.trace(e);
		}
	}
	
	@EJB
	private SistemaContext sistemaContext;
	//de printreport
	private PrintReport initPrintReportService(){				
		
		try{
			PrintReportServiceLocator locator = new PrintReportServiceLocator();			
			
			String endpoint = sistemaContext.getPrintReportEndpoint();
			
			return locator.getPrintReport(new URL(endpoint));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}
	@Override
	public List<InfoDescargaPoliza> getRegistroDescargas() {		
		Map<String, Object> execute = registroDescarga.execute();	
		@SuppressWarnings("unchecked")
		List<InfoDescargaPoliza> lista = (List<InfoDescargaPoliza>) execute.get("pCursor");		
		
		return lista;
	}
	
	public List<InfoDescargaPoliza> getDetalleDescargas(String pUsuario){//try catch
		StringBuilder sql = new StringBuilder();
		
		sql.append("SELECT * FROM MIDAS.trusrpolizadescarga WHERE usuario = ?");

		try {
			return jdbcTemplate.queryForObject(sql.toString(), new Object[] {pUsuario}, new RowMapper<List<InfoDescargaPoliza>>() {
				List<InfoDescargaPoliza> lstDatos =  new ArrayList<InfoDescargaPoliza>();
				@Override
				public List<InfoDescargaPoliza> mapRow(ResultSet rs, int rowNum) throws SQLException {
					InfoDescargaPoliza datos = new InfoDescargaPoliza();
					lstDatos.add(datos);

					return lstDatos;
				}
			});
		} catch (Exception e) {
			List<InfoDescargaPoliza> lstDatos =  new ArrayList<InfoDescargaPoliza>();
			return lstDatos;
		}

	}

	@Override
	public List<NotificacionCliente> getReciboVencido(String tipoNotificacion) {
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pTipoNotificacion", tipoNotificacion);
		
		Map<String, Object> execute = consultaTipoNotificaciones.execute(sqlParameter);	
		@SuppressWarnings("unchecked")
		List<NotificacionCliente> lista = (List<NotificacionCliente>) execute.get("pCursor");		
		
		return lista;
	}

	@Override
	public List<NotificacionCliente> getReciboPagado(String tipoNotificacion) {
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pTipoNotificacion", tipoNotificacion);
		
		Map<String, Object> execute = consultaTipoNotificaciones.execute(sqlParameter);	
		@SuppressWarnings("unchecked")
		List<NotificacionCliente> lista = (List<NotificacionCliente>) execute.get("pCursor");		
		
		return lista;
	}

	@Override
	public List<NotificacionCliente> getReciboVencimiento(String tipoNotificacion) {
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pTipoNotificacion", tipoNotificacion);
		
		Map<String, Object> execute = consultaTipoNotificaciones.execute(sqlParameter);	
		@SuppressWarnings("unchecked")
		List<NotificacionCliente> lista = (List<NotificacionCliente>) execute.get("pCursor");		
		
		return lista;
	}

	@Override
	public List<NotificacionCliente> getCancelacionPoliza(String tipoNotificacion) {
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pTipoNotificacion", tipoNotificacion);
		
		Map<String, Object> execute = consultaTipoNotificaciones.execute(sqlParameter);	
		@SuppressWarnings("unchecked")
		List<NotificacionCliente> lista = (List<NotificacionCliente>) execute.get("pCursor");		
		
		return lista;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NotificacionCliente> getNotification() {
		LogDeMidasEJB3.log(this.getClass().getName()+"finding all getNotificaciones instances", Level.INFO,
				null);
		List<NotificacionCliente> notificacionList = new ArrayList<NotificacionCliente>();
		Query query =null;
		try{
			String usuarioMidas=usuarioService.getUsuarioActual().getNombreUsuario();
			final String queryString ="select tNot.*, tpol.id as idClientePoliza , tpol.tipo "+ 
					"from MIDAS.trusrnotificacion tNot " +
					"inner join MIDAS.trusrpoliza tPol " +
					"on (tnot.idtopoliza = tpol.idtopoliza or "+
					"tnot.id_cotizacion_seycos = tPol.id_cotizacion_seycos) and " +
					"tpol.usuario='"+usuarioMidas+"'";
			query = entityManager.createNativeQuery(queryString);
			List<Object[]> listaObj = query.getResultList();
			
			for(Object[] object : listaObj){
				NotificacionCliente notificacionesCliente = new NotificacionCliente();
				notificacionesCliente.setMensaje(object[1].toString());
				notificacionesCliente.setFechaCreacion(object[2].toString());
				notificacionesCliente.setTipoNotificacion(object[3].toString());
				notificacionesCliente.setPoliza(object[4].toString());
				notificacionesCliente.setIdClientePoliza(object[7].toString());
				notificacionesCliente.setTipoPoliza(object[8].toString());
				notificacionesCliente.setIdNotificacion(object[0].toString()); 
				notificacionList.add(notificacionesCliente);
			}
		}catch(RuntimeException re){
			LogDeMidasEJB3.log(this.getClass().getName()+"find all failed", Level.SEVERE, re);
			throw re;
		}
		LogDeMidasEJB3.log(this.getClass().getName()
				+"notificacionList.get(1)=>>"+notificacionList, Level.SEVERE, null);
		return notificacionList;
	}
	
	@Override
	public List<NotificacionCliente> getEnviarNotificacion() {
		LogDeMidasEJB3.log(this.getClass().getName()+"finding all getEnviarNotificacion instances", Level.INFO,null);
		List<NotificacionCliente> envioNotificacionList = new ArrayList<NotificacionCliente>();
		Query query =null;
		try{
			final String queryString =
				    "SELECT pol.usuario,  nvl(tNot.idtopoliza,0), tNot.mensaje, tNot.tiponotificacion,pol.ID, pol.TIPO,tNot.id " +
					"FROM MIDAS.trusrpoliza pol " +
					"INNER JOIN MIDAS.trusrnotificacion tNot " +
					"ON pol.idtopoliza=tNot.idtopoliza or " +
					"pol.id_cotizacion_seycos = tNot.id_cotizacion_seycos";
			query = entityManager.createNativeQuery(queryString);
			@SuppressWarnings("unchecked")
			List<Object[]> listaObj = query.getResultList();
			
			for(Object[] object : listaObj){
				NotificacionCliente notificacionesCliente = new NotificacionCliente();
				notificacionesCliente.setUsuario(object[0].toString());
				notificacionesCliente.setIdtopoliza(object[1].toString());
				notificacionesCliente.setMensaje(object[2].toString());
				notificacionesCliente.setTipoNotificacion(object[3].toString());
				notificacionesCliente.setIdClientePoliza(object[4].toString()); 
				notificacionesCliente.setTipoPoliza(object[5].toString()); 
				notificacionesCliente.setIdNotificacion(object[6].toString()); 
				envioNotificacionList.add(notificacionesCliente);
			}
		}
		catch(RuntimeException re){
			LogDeMidasEJB3.log(this.getClass().getName()+"find all failed", Level.SEVERE, re);
			throw re;
		}
		return envioNotificacionList;
	}

	/**
	 * Metodo para obtener las gerencias de la organizacion
	 */
	@Override
	public List<CentroOperacionView> getLstGerencias() {
		final List<CentroOperacionView> lstDatos =  new ArrayList<CentroOperacionView>();
		StringBuilder sql = new StringBuilder();
		//final List<InfoDescargaPoliza> lstDatos =  new ArrayList<InfoDescargaPoliza>();
		sql.append("select distinct(id_gerencia), nom_gerencia from SEYCOS.VW_GERENCIA_OFICINAS order by id_gerencia");

		try {
			return jdbcTemplate.queryForObject(sql.toString(), null, new RowMapper<List<CentroOperacionView>>() {				
				@Override
				public List<CentroOperacionView> mapRow(ResultSet rs, int rowNum) throws SQLException {
					CentroOperacionView datos = new CentroOperacionView();					
					datos.setId(rs.getLong("id_gerencia"));
					datos.setDescripcion(rs.getString("nom_gerencia"));
					lstDatos.add(datos);

					return lstDatos;
				}
			});
		} catch (Exception e) {			
			return lstDatos;
		}
	}
	public void save(PolizasClienteMovil entity) {
		LogDeMidasEJB3.log("saving PolizasClienteMovil instance", Level.INFO, null);
		try {
			entity.setPolizasPermitidos(new Long (3));
			entity.setBloqueado("N");
			em.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Trusrpoliza entity.
	 * 
	 * @param entity
	 *            Trusrpoliza entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PolizasClienteMovil entity) {
		LogDeMidasEJB3.log("deleting PolizasClienteMovil instance", Level.INFO, null);
		try {
			entity = em.getReference(PolizasClienteMovil.class,
					entity.getId());
			em.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Trusrpoliza entity and return it or a copy of
	 * it to the sender. A copy of the Trusrpoliza entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            Trusrpoliza entity to update
	 * @return Trusrpoliza the persisted Trusrpoliza entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PolizasClienteMovil update(PolizasClienteMovil entity) {
		LogDeMidasEJB3.log("updating PolizasClienteMovil instance", Level.INFO, null);
		try {
			PolizasClienteMovil result = em.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public PolizasClienteMovil findById(Long id) {
		LogDeMidasEJB3.log("finding PolizasClienteMovil instance with id: " + id, Level.INFO,
				null);
		try {
			PolizasClienteMovil instance = em.find(PolizasClienteMovil.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Trusrpoliza entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Trusrpoliza property to query
	 * @param value
	 *            the property value to match
	 * @return List<Trusrpoliza> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<PolizasClienteMovil> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding PolizasClienteMovil instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from PolizasClienteMovil model where model."
					+ propertyName + "= :propertyValue";
			Query query = em.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	@SuppressWarnings("unchecked")
	public List<PolizasClienteMovil> findByUsuarioRamo(String usuario,
			String ramo) {
		LogDeMidasEJB3.log("finding PolizasClienteMovil instance with usuario: "
				+ usuario + ", ramo: " + ramo, Level.INFO, null);
		try {
			final String queryString = "select model from PolizasClienteMovil model where "
					+ " model.tipo " + " = :propertyRamo "
					+ " and model.usuario " + " = :propertyUsuario ";
			Query query = em.createQuery(queryString);
			query.setParameter("propertyRamo", ramo.trim());
			query.setParameter("propertyUsuario", usuario.trim());
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	public List<TipoSiniestroMovil> getSiniestrosByTipoProducto(String clave, Long nivel) {
		LogDeMidasEJB3.log(this.getClass().getName()+" finding all tipos siniestros de clave:"+clave+" y nivel:"+nivel, Level.INFO,null);
		
		return (List<TipoSiniestroMovil>) jdbcTemplate.query("select a.id, a.prodreportesinmov_id, a.nombre, a.nivel, a.padre, a.imagen, "+
				"( select " +
				"(case   count(1) when 0 then '0' else '1' end) " +
				"from midas.TCTIPOSINIESTROMOVIL c "+
				"where c.padre = a.id "+ 
				") as hijos "+
				"from midas.TCTIPOSINIESTROMOVIL a "+
				"left join midas.tcprodreportesinmov b on a.prodreportesinmov_id = b.id "+
				"where b.clave = ? and a.nivel = ? order by a.id", new Object[]{clave, nivel},
			new RowMapper<TipoSiniestroMovil>(){
				public TipoSiniestroMovil mapRow(ResultSet rs, int rowNum) throws SQLException{
					TipoSiniestroMovil dto = new TipoSiniestroMovil();
					dto.setId(rs.getLong("id"));
					dto.setImagen(rs.getString("imagen"));
					dto.setNombre(rs.getString("nombre"));
					dto.setTieneHijos(rs.getBoolean("hijos"));
					LogDeMidasEJB3.log("Hijos:"+ dto.isTieneHijos()+"\nimagen:"+dto.getImagen()+"\nNombre:"+dto.getNombre(), Level.INFO,null);
					return dto;
				}
			}
		);
	}
	
	public List<TipoSiniestroMovil> getSiniestrosByPadre(Long idPadre) {
		LogDeMidasEJB3.log(this.getClass().getName()+" finding all tipos siniestros Hijos de:"+idPadre, Level.INFO,null);
		
		return (List<TipoSiniestroMovil>) jdbcTemplate.query("select " +
				"a.id, a.prodreportesinmov_id, a.nombre, a.nivel, a.padre, a.imagen, "+ 
				" (" +
				"	select "+
				"	(case   count(1) when 0 then '0' else '1' end) "+
				"	from midas.TCTIPOSINIESTROMOVIL c "+
				"	where c.padre = a.id "+ 
				" ) as hijos "+
				" from midas.TCTIPOSINIESTROMOVIL a "+
				" where a.padre = ?", new Object[]{idPadre},
			new RowMapper<TipoSiniestroMovil>(){
				public TipoSiniestroMovil mapRow(ResultSet rs, int rowNum) throws SQLException{
					TipoSiniestroMovil dto = new TipoSiniestroMovil();
					dto.setId(rs.getLong("id"));
					dto.setImagen(rs.getString("imagen"));
					dto.setNombre(rs.getString("nombre"));
					dto.setTieneHijos(rs.getBoolean("hijos"));
					LogDeMidasEJB3.log("Hijos:"+ dto.isTieneHijos()+"\nimagen:"+dto.getImagen()+"\nNombre:"+dto.getNombre(), Level.INFO,null);
					return dto;
				}
			}
		);
	}
	
	public boolean existePolizasClienteMovil(InfoPolizaParameter param) {
		LogDeMidasEJB3.log("finding PolizasClienteMovil instance with poliza: "
				+ param.getNumeroPoliza().toString()+ "inciso:" +param.getInciso() , Level.INFO, null);
		boolean existe=false;
		try {
			final String queryString = "select model from PolizasClienteMovil model where "
					+ " model.poliza " + " = :propertyPoliza "
					+ " and model.inciso " + " = :propertyInciso ";
			Query query = em.createQuery(queryString);
			query.setParameter("propertyPoliza", param.getNumeroPoliza().toString().trim());
			query.setParameter("propertyInciso", new Short(param.getInciso().trim()));
			if(query.getResultList().size()>0)
				existe =true;
			else
				existe=false;
		} catch (RuntimeException re) {
			existe=false;
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
		return existe;
	}

	public boolean existePolizasClienteMovilWithUser(InfoPolizaParameter param) {
		LogDeMidasEJB3.log("finding PolizasClienteMovilWithUser instance with poliza: "
				+ param.getNumeroPoliza().toString()+ "inciso:" +param.getInciso()+" user:"+usuarioService.getUsuarioActual().getNombreUsuario() , Level.INFO, null);
		boolean existe=false;
		try {
			final String queryString = "select model from PolizasClienteMovil model where "
					+ " model.poliza " + " = :propertyPoliza "
					+ " and model.inciso " + " = :propertyInciso "
					+ " and model.usuario " + " = :propertyUsuario ";
			Query query = em.createQuery(queryString);
			query.setParameter("propertyPoliza", param.getNumeroPoliza().toString().trim());
			query.setParameter("propertyInciso", new Short(param.getInciso().trim()));
			query.setParameter("propertyUsuario", usuarioService.getUsuarioActual().getNombreUsuario());
			if(query.getResultList().size()>0)
				existe =true;
			else
				existe=false;
		} catch (RuntimeException re) {
			existe=false;
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
		return existe;
	}
	
	public boolean existePolizasMigradaClienteMovilSinImei(InfoPolizaParameter param) {
		LogDeMidasEJB3.log("finding existePolizasMigradaClienteMovilSinImei instance with poliza: "
				+ param.getNumeroPoliza().toString()+ " inciso:" +param.getInciso(), Level.INFO, null);
		ErrorBuilder eb = new ErrorBuilder();
		boolean existe = false;

		String sql = "SELECT * FROM MIDAS.TRUSRPOLIZA " +
				"where imei is null and poliza = ? and inciso = ?";
		List<PolizasClienteMovil> list = new ArrayList<PolizasClienteMovil>();
		
		try {
	   		   list = (List<PolizasClienteMovil>) jdbcTemplate.query(sql.toString(), 
	   				   new Object[] {param.getNumeroPoliza().toString(), param.getInciso()}, 
	   				   new RowMapper<PolizasClienteMovil>() {
		   			@Override
		   			public PolizasClienteMovil mapRow(ResultSet rs, int rowNum) throws SQLException {	
		   				PolizasClienteMovil dto = new PolizasClienteMovil();
		   				dto.setId(rs.getLong("id"));
		   				dto.setUsuario(rs.getString("usuario"));
		   				return dto;			
		   			}
	   		   }); 
	   	} catch(EmptyResultDataAccessException e){
	   		   throw new ApplicationException(eb.addFieldError("numeroPoliza",
	   				   "El noPoliza "+param.getNumeroPoliza().toString()+" con inciso "+ param.getInciso()+" no existe"));
	   	} catch (RuntimeException re) {
	   		throw new ApplicationException(eb.addFieldError("Error",
	   				   "Ocurrió un error interno en tiempo de ejecución"));
	   	}
	   	
	   	if (list.size() > 0)
	   		existe = true;
	   	
		return existe;
	}
	
	/**
	 * Find all Trusrpoliza entities.
	 * 
	 * @return List<Trusrpoliza> all Trusrpoliza entities
	 */
	@SuppressWarnings("unchecked")
	public List<PolizasClienteMovil> findAll() {
		LogDeMidasEJB3.log("finding all PolizasClienteMovil instances", Level.INFO, null);
		try {
			final String queryString = "select model from PolizasClienteMovil model";
			Query query = entityManager.createQuery(queryString);
			query.setMaxResults(100);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	public  String getTipoPoliza(String strNumeroPolizaCompleto) {
		String tipoPoliza=null;
		if (strNumeroPolizaCompleto == null || StringUtils.isBlank(strNumeroPolizaCompleto)) {
			throw new IllegalArgumentException("Nulo o vacio.");
		}
		strNumeroPolizaCompleto = StringUtils.trim(strNumeroPolizaCompleto).replace(SEPARADOR, "");
		if (!strNumeroPolizaCompleto.matches("\\d+")) {
			throw new IllegalArgumentException("Contiene caracteres invalidos.");			
		}
		if (strNumeroPolizaCompleto.length() == 12) {
			tipoPoliza= TIPOPOLIZAMIDAS;
		} else if (strNumeroPolizaCompleto.length() == 15) {
			tipoPoliza= TIPOPOLIASEYCOS;
		} else {
			throw new RuntimeException("Formato incorrecto.");
		}	
		return tipoPoliza;
	}
	
	public MensajeValidacion validateGuardar(InfoPolizaParameter param){
		LogDeMidasEJB3.log("validateGuardar isConsultaPoliza="+param.isConsultaPoliza(), Level.INFO, null);
		MensajeValidacion mensaje= new MensajeValidacion();
		ErrorBuilder eb = new ErrorBuilder();
		PolizaDTO poliza=null;
		Long idCotizacionSeycos=null;
	    ValidacionPolizaRenovada valPolRenovada = new ValidacionPolizaRenovada();
		if (param.getNumeroPoliza() == null) {
			throw new ApplicationException(eb.addFieldError("numeroPoliza",
			"Requerido"));
		}
		
		else if (param.getInciso() == null) {
			throw new ApplicationException(eb.addFieldError("numeroInciso", "requerido"));
		}
			
		else if (param.getNumeroPoliza() != null) {
			if (!param.getRamo().trim().equals(RAMOVIDA)){
				poliza = polizaFacadeRemote.find(param.getNumeroPoliza());
			} else {
				boolean espolizaVidaValida = esNoPolizaVida(param.getNumeroPoliza().toString());
				LogDeMidasEJB3.log(this.getClass().getName()+". find : "+param.getNumeroPoliza().toString()+ "; espolizaVidaValida="+espolizaVidaValida, Level.INFO, null);
				if (espolizaVidaValida == false) {
					throw new ApplicationException(eb.addFieldError("numeroPoliza", "No es una pÃ³liza de vida."));
				} else {
					int idCentroEmis = Integer.parseInt(param.getNumeroPoliza().toString().substring(0, 3));
					Long noPoliza = Long.parseLong(param.getNumeroPoliza().toString().substring(4, 14));
					int numRenovPol = Integer.parseInt(param.getNumeroPoliza().toString().substring(15, 17));
					LogDeMidasEJB3.log(this.getClass().getName()+".validateGuardar poliza : "+param.getNumeroPoliza().toString()+" \nidCentroEmis:"+idCentroEmis+" noPoliza:"+noPoliza+" numRenovPol:"+numRenovPol, Level.INFO, null);

					idCotizacionSeycos = new Long(getNoPolizaVida(noPoliza, idCentroEmis, numRenovPol).toString());
				}
				
			}
			valPolRenovada = existeRenovacionPosteriorVigente(param);	
			if (poliza == null && !param.getRamo().trim().equals(RAMOVIDA)) {
				throw new ApplicationException(eb.addFieldError("numeroPoliza", "No se encontrÃ³ la pÃ³liza."));
			} else if (param.isConsultaPoliza() && existePolizasClienteMovil(param)){ //si se está dando de alta una póliza desde la consulta
				throw new ApplicationException(eb.addFieldError("numeroPoliza", "La pÃ³liza: "+param.getNumeroPoliza().toString() +" con el inciso: "+ param.getInciso() +" ya fue registrada"));
			} else if (!param.isConsultaPoliza() && existePolizasClienteMovilWithUser(param)){ //si se está dando de alta una póliza desde Alerta Siniestros
				throw new ApplicationException(eb.addFieldError("numeroPoliza", "La pÃ³liza: "+param.getNumeroPoliza().toString() +" con el inciso: "+ param.getInciso() +" y el usuario "+usuarioService.getUsuarioActual().getNombreUsuario()+" ya fue registrada"));
			} else if (param.isConsultaPoliza() && valPolRenovada.getRenovacionPosteriorVigente() != null && valPolRenovada.getRenovacionPosteriorVigente()){
				throw new ApplicationException(eb.addFieldError("", "Póliza vencida, su póliza vigente es: "+valPolRenovada.getNumeroPoliza() +"\nFavor de ingresarla." ));
			} else if (param.isConsultaPoliza() && valPolRenovada.getPolizaVencida()!= null && valPolRenovada.getPolizaVencida()){
				throw new ApplicationException(eb.addFieldError("", "Póliza vencida. Favor de ingresar póliza vigente."));
			}
			else {
				if (poliza != null)
					LogDeMidasEJB3.log("find polizaFacadeRemote find poliza="+poliza.getIdToPoliza(), Level.INFO, null);
				else if( idCotizacionSeycos != null)
					LogDeMidasEJB3.log("find cotizacion vida found idCotizacionSeycos="+idCotizacionSeycos, Level.INFO, null);
				if (param.getInciso() != null) {// valida inciso autos y danios
					if (!existeInciso(poliza,idCotizacionSeycos, new Integer(param.getInciso()))) {
						throw new ApplicationException(eb.addFieldError("numeroInciso", "No se encontrÃ³ el inciso."));
					} else {
						//Validar que el tipoSiniestro y la poliza sean apropiados.
						if (param.getRamo() != null) {
							if (param.getRamo().trim().equals(RAMOAUTOS)) {
								String claveNegocio = poliza.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getClaveNegocio();
								LogDeMidasEJB3.log(this.getClass().getName()+". find claveNegocio="+claveNegocio, Level.INFO, null);
								if (!claveNegocio.equals(NegocioSeguros.CLAVE_AUTOS)) {
									throw new ApplicationException(eb.addFieldError("numeroPoliza", "No es una pÃ³liza de autos."));
								}
								else{
									mensaje = guardarPolizasClienteMovil(param,poliza,null);									
								}
							} else if (param.getRamo().trim().equals(RAMOCASA)) {
								if(!polizaFacadeRemote.esPolizaCasa(poliza.getIdToPoliza())) {
									throw new ApplicationException(eb.addFieldError("numeroPoliza", "No es una pÃ³liza de casa."));
								}
								else{
									mensaje = guardarPolizasClienteMovil(param,poliza,null);									
								}
							} else if (param.getRamo().trim().equals(RAMOVIDA)) {
								mensaje = guardarPolizasClienteMovil(param,null,idCotizacionSeycos);																	
							} else if (param.getRamo().trim().equals(RAMODANIO) || param.getRamo().trim().equals(RAMOPYME)) {
								mensaje = guardarPolizasClienteMovil(param,poliza,idCotizacionSeycos);
							}
						}
					}
				} 
			}
		}
		return mensaje;
	}

	/**
	 * Método para guardar las pólizas migradas desde la App de Reporte de Siniestros.
	 * Este método podrá ser eliminado cuando termine el periodo de migración.
	 * @param param
	 * @return MensajeValidacion
	 */
	public MensajeValidacion validateGuardarPolizaMigrada(InfoPolizaParameter param){
		MensajeValidacion mensaje= new MensajeValidacion();
		ErrorBuilder eb = new ErrorBuilder();
		PolizaDTO poliza=null;
		Long idCotizacionSeycos=null;
		LogDeMidasEJB3.log(this.getClass().getName()+".validateGuardarPolizaMigrada: -->"+param.getNumeroPoliza().toString(), Level.INFO, null);
		if (param.getNumeroPoliza() == null) {
			throw new ApplicationException(eb.addFieldError("numeroPoliza", "Requerido"));
		} else if (param.getInciso() == null) {
			throw new ApplicationException(eb.addFieldError("numeroInciso", "requerido"));
		} else if (param.getImei() == null) {
			throw new ApplicationException(eb.addFieldError("imei", "requerido"));
		} else if (param.getNumeroPoliza() != null) {
			poliza = polizaFacadeRemote.find(param.getNumeroPoliza());
			
			if (poliza == null) {
				throw new ApplicationException(eb.addFieldError("numeroPoliza", "No se encontrÃ³ la pÃ³liza."));
			} else if (existePolizasMigradaClienteMovil(param)){
				throw new ApplicationException(eb.addFieldError("numeroPoliza", "La pÃ³liza: "+param.getNumeroPoliza().toString() +" con el inciso: "+ param.getInciso() +" y con el IMEI:"+ param.getImei() +" ya fue registrada"));
			} else {
				boolean updateRegistro = false;
				if (poliza != null)
					LogDeMidasEJB3.log("find polizaFacadeRemote find poliza="+poliza.getIdToPoliza(), Level.INFO, null);
			
				if (existePolizasMigradaClienteMovilSinImei(param)){ //si existe porque se registró con Cliente Suite antes de ser migrada. No grabó IMEI
					updateRegistro = true;
					LogDeMidasEJB3.log(this.getClass().getName()+".validateGuardarPolizaMigrada: se actualizará el IMEI de la póliza", Level.INFO, null);
				}
				
				if (param.getInciso() != null) {// valida inciso autos y danios
					if (!existeInciso(poliza, idCotizacionSeycos, new Integer(param.getInciso()))) {
						throw new ApplicationException(eb.addFieldError("numeroInciso", "No se encontrÃ³ el inciso."));
					} else {
						if (param.getRamo() != null) {
							if (param.getRamo().trim().equals(RAMOAUTOS)) {
								String claveNegocio = poliza.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getClaveNegocio();
								LogDeMidasEJB3.log(this.getClass().getName()+". find claveNegocio="+claveNegocio, Level.INFO, null);
								if (!claveNegocio.equals(NegocioSeguros.CLAVE_AUTOS)) {
									throw new ApplicationException(eb.addFieldError("numeroPoliza", "No es una pÃ³liza de autos."));
								}
								else{
									if (updateRegistro)
										mensaje = actualizarPolizasMigradasClienteMovil(param);
									else
										mensaje = guardarPolizaMigradaClienteMovil(param,poliza,null);									
								}
							} else if (param.getRamo().trim().equals(RAMOCASA)) {
								if(!polizaFacadeRemote.esPolizaCasa(poliza.getIdToPoliza())) {
									throw new ApplicationException(eb.addFieldError("numeroPoliza", "No es una pÃ³liza de casa."));
								}
								else{
									if (updateRegistro)
										mensaje = actualizarPolizasMigradasClienteMovil(param);
									else
										mensaje = guardarPolizaMigradaClienteMovil(param,poliza,null);									
								}
							}
						}
					}
				} 
			}
		}
		return mensaje;
	}
	
	public MensajeValidacion actualizarPolizasMigradasClienteMovil(InfoPolizaParameter param){
		LogDeMidasEJB3.log(this.getClass().getName()+".actualizarPolizasMigradasClienteMovil()  poliza="+param.getNumeroPoliza().toString()+" imei="+param.getImei()+" inciso:"+param.getInciso(), Level.INFO, null);
		MensajeValidacion mensaje = new  MensajeValidacion();
		
		String sql = "update midas.trusrpoliza set imei = ? " +
				"where poliza = ? and inciso = ?";
		
		jdbcTemplate.update(sql, new Object[]{param.getImei(), param.getNumeroPoliza().toString(), param.getInciso()});
		
		mensaje.setEsValido(true);
		mensaje.setMensaje("Se ha Actualizado la Poliza");
		return mensaje;
	}
	
	public boolean existeInciso(PolizaDTO poliza,Long idCotizacionSeycos, Integer numeroInciso) {
		LogDeMidasEJB3.log(this.getClass().getName()+".existeInciso()  poliza="+poliza+" ;idCotizacionSeycos="+idCotizacionSeycos, Level.INFO, null);
		if(poliza!=null){
			//Checar si en alguno de los endosos existe el inciso.
			String countIncisosQuery = "select count(*) from MIDAS.tocotizacion \n" + 
					"    inner join (select distinct idtocotizacion from MIDAS.toendoso where idtopoliza = ?) e on e.idtocotizacion = tocotizacion.idtocotizacion\n" + 
					"    inner join MIDAS.toincisocot on toincisocot.idtocotizacion = tocotizacion.idtocotizacion\n" + 
					"    where numeroinciso = ?";
			Query query = em.createNativeQuery(countIncisosQuery);
			query.setParameter(1, poliza.getIdToPoliza());
			query.setParameter(2, numeroInciso);
			int total = ((BigDecimal) query.getSingleResult()).intValue();
			
			if (total > 0) {
				return true;
			} 
			
			//Buscar en estructura bitemporal.
			if (getIncisoContinuity(poliza.getCotizacionDTO().getIdToCotizacion().intValue(), numeroInciso) != null) {
				return true;
			}
		}
		else if(idCotizacionSeycos!=null){
			return existeIncisoPolizaVida(idCotizacionSeycos,new Long (numeroInciso.toString()));
		}		

		return false;
	}
	private IncisoContinuity getIncisoContinuity(Integer numeroCotizacion, Integer numeroInciso) {
		String jpql = "select m from IncisoContinuity m where m.numero = :numeroInciso and m.cotizacionContinuity.numero = :numeroCotizacion";
		TypedQuery<IncisoContinuity> query = em.createQuery(jpql, IncisoContinuity.class);
		query.setParameter("numeroCotizacion", numeroCotizacion);
		query.setParameter("numeroInciso", numeroInciso);
		List<IncisoContinuity> list = query.getResultList();
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}
	public ClientePolizas transformarPolizaclienteModel(PolizasClienteMovil polizasClienteMovil ){
		 ClientePolizas entidad = new ClientePolizas();
		 BitemporalAutoInciso autoIncisoBitemporal= null;
		 List<EndosoDTO> endosoPol = new ArrayList<EndosoDTO>();
		 if(polizasClienteMovil.getPolizaDTO()!=null){
			 Map<String,Object> params = new HashMap<String, Object>();
			 params.put("polizaDTO.idToPoliza", polizasClienteMovil.getPolizaDTO().getIdToPoliza());
			 endosoPol = entidadService.findByProperties(EndosoDTO.class, params);
			 EndosoDTO ultimoEndoso = new EndosoDTO();
			 ultimoEndoso = endosoPol.get(0);
			 Date validOnEnd = ultimoEndoso.getValidFrom();
			 if(polizasClienteMovil.getTipo().equals(RAMOAUTOS)){
				 IncisoContinuity incisoContinuity = this.getIncisoContinuity(polizasClienteMovil.getPolizaDTO().getCotizacionDTO().getIdToCotizacion().intValue(), polizasClienteMovil.getInciso().intValue());
				 if (incisoContinuity != null) {
					 autoIncisoBitemporal = incisoViewService.getAutoInciso(incisoContinuity.getId(), validOnEnd);
					 entidad.setAsegurado(autoIncisoBitemporal.getValue().getNombreAsegurado());
				 }
			 }else if(endosoPol != null && !endosoPol.isEmpty()){
				CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class,  ultimoEndoso.getIdToCotizacion());
				entidad.setAsegurado(cotizacion.getNombreAsegurado());
			}else{
				 entidad.setAsegurado(polizasClienteMovil.getPolizaDTO().getCotizacionDTO().getNombreAsegurado());
			}
			if(polizasClienteMovil.getPolizaDTO().getClaveEstatus().equals(new Short("1"))|| polizasClienteMovil.getPolizaDTO().getClaveEstatus()== 1)
					entidad.setEstatus("VIGENTE");
			 else
					entidad.setEstatus("POLIZA NO VIGENTE");
				
		 }
		 else{
			 DatosPolizaSeycos datosPolizaSeycos= getIncisoPolizaVida(polizasClienteMovil.getIdCotizacionSeycos(),new Long(polizasClienteMovil.getInciso().toString()));
			 entidad.setEstatus(datosPolizaSeycos.getSituacionPoliza());
			 entidad.setAsegurado(datosPolizaSeycos.getNombreAsegurado());
		}
		if(polizasClienteMovil.getBloqueado().trim().equals("S"))
			entidad.setBloqueado(true);
		else
			entidad.setBloqueado(false);
		entidad.setDescripcion(polizasClienteMovil.getDescripcion());
		entidad.setInciso(polizasClienteMovil.getInciso().toString());
		entidad.setPolizaID(new Integer(polizasClienteMovil.getId().toString()));
		entidad.setPolizaNo(polizasClienteMovil.getPoliza());
		entidad.setTipo(polizasClienteMovil.getTipo());
		return entidad;
	}
	public MensajeValidacion guardarPolizasClienteMovil(InfoPolizaParameter param,PolizaDTO poliza, Long idCotizacionSeycos){
		MensajeValidacion mensaje = new  MensajeValidacion();
		PolizasClienteMovil polizasClienteMovil= new PolizasClienteMovil();
		
			polizasClienteMovil.setUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
			polizasClienteMovil.setNombreUsuario(usuarioService.getUsuarioActual().getNombreCompleto());
			polizasClienteMovil.setEmail(usuarioService.getUsuarioActual().getEmail());
			polizasClienteMovil.setDescripcion(param.getDescripcion());
			polizasClienteMovil.setInciso(new Short(param.getInciso()));
			if (param.getNumeroPoliza() instanceof  NumeroPolizaCompletoMidas) {
				polizasClienteMovil.setOrigenTipoPoliza(TIPOPOLIZAMIDAS);
				polizasClienteMovil.setPoliza(param.getNumeroPoliza().toString());
			} else if (param.getNumeroPoliza() instanceof NumeroPolizaCompletoSeycos) {
				polizasClienteMovil.setOrigenTipoPoliza(TIPOPOLIASEYCOS);
				polizasClienteMovil.setPoliza(param.getNumeroPoliza().toString());
			} 
			
			polizasClienteMovil.setTipo(param.getRamo().trim());
			if(poliza!=null)
				polizasClienteMovil.setPolizaDTO(poliza);						
			if(idCotizacionSeycos!=null)
				polizasClienteMovil.setIdCotizacionSeycos(idCotizacionSeycos);
			save(polizasClienteMovil);
			mensaje.setEsValido( true);
			mensaje.setMensaje("Se ha guardado la Poliza");
		return mensaje;
	}
	
	public MensajeValidacion guardarPolizaMigradaClienteMovil(InfoPolizaParameter param,PolizaDTO poliza, Long idCotizacionSeycos){
		MensajeValidacion mensaje = new  MensajeValidacion();
		PolizasClienteMovil polizasClienteMovil= new PolizasClienteMovil();
		
			polizasClienteMovil.setUsuario(null);
			polizasClienteMovil.setNombreUsuario(null);
			polizasClienteMovil.setEmail(null);
			polizasClienteMovil.setDescripcion(param.getDescripcion());
			polizasClienteMovil.setInciso(new Short(param.getInciso()));
			polizasClienteMovil.setImei(param.getImei());
			
			if (param.getNumeroPoliza() instanceof  NumeroPolizaCompletoMidas) {
				polizasClienteMovil.setOrigenTipoPoliza(TIPOPOLIZAMIDAS);
				polizasClienteMovil.setPoliza(param.getNumeroPoliza().toString());
			} else if (param.getNumeroPoliza() instanceof NumeroPolizaCompletoSeycos) {
				polizasClienteMovil.setOrigenTipoPoliza(TIPOPOLIASEYCOS);
				polizasClienteMovil.setPoliza(param.getNumeroPoliza().toString());
			} 
			
			polizasClienteMovil.setTipo(param.getRamo().trim());
			
			if(poliza!=null)
				polizasClienteMovil.setPolizaDTO(poliza);						
			if(idCotizacionSeycos!=null)
				polizasClienteMovil.setIdCotizacionSeycos(idCotizacionSeycos);
			
			save(polizasClienteMovil);
			mensaje.setEsValido( true);
			mensaje.setMensaje("Se ha guardado la Poliza");
		return mensaje;
	}
	
	 List<String> polizas;
   private Integer getNumeroPolizaVida(String Poliza) {
	    StringBuilder sql = new StringBuilder();	 
		sql.append(" SELECT pp.ID_COTIZACION \r\n")
		.append(" FROM seycos.pol_poliza pp \r\n")		
		.append(" WHERE  CAST(SUBSTR( ? , 1,3) as number) = CAST(pp.id_centro_emis as number)  \r\n")
		.append(" AND CAST(SUBSTR( ? , 5, 10)as number) = CAST(pp.num_poliza as number)   \r\n")
		.append(" AND CAST(SUBSTR( ? , 16, 2)as number) = CAST(pp.num_renov_pol  as number) \r\n")
		.append(" AND ID_PRODUCTO NOT IN (1,2)  \r\n")
		.append(" AND CVE_T_POLIZA IN ('INML','GPO') \r\n")
		.append(" AND F_TER_REG = TO_DATE('12/31/4712 00:00:00', 'MM/DD/YYYY HH24:MI:SS')  \r\n")
		.append(" AND B_ULT_REG_DIA = 'V'  \r\n")
		.append(" AND CVE_T_DOCTO_VERP <> 'SE'  \r\n");
		
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {Poliza,Poliza,Poliza}, new RowMapper<Integer>() {
			@Override
			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {				
				Integer poliza = rs.getInt("ID_COTIZACION");
				LogDeMidasEJB3.log(this.getClass().getName()+". find ID_COTIZACION : "+poliza, Level.INFO, null);
				return poliza;			
			}
		});       
   }
   
   public DatosPolizaSeycos getIncisoPolizaVida(Long  poliza,Long inciso) {
	   LogDeMidasEJB3.log(this.getClass().getName()+".getIncisoPolizaVida() find ID_COTIZACION : "+poliza +"inciso:"+inciso, Level.INFO, null);
	    StringBuilder sql = new StringBuilder();	 
	    
		sql.append(" SELECT distinct pp.ID_COTIZACION,pp.SIT_POLIZA,per.NOMBRE,pp.F_INI_VIGENCIA,pp.F_FIN_VIGENCIA \r\n")
		.append(" FROM seycos.pol_poliza pp    \r\n")
		.append(" inner join seycos.pol_inciso poli on (poli.id_cotizacion=pp.id_cotizacion    \r\n")
		.append(" AND poli.F_TER_REG = TO_DATE('12/31/4712 00:00:00', 'MM/DD/YYYY HH24:MI:SS')     \r\n")
		.append(" AND poli.B_ULT_REG_DIA = 'V'     \r\n")
		.append(" AND poli.CVE_T_DOCTO_VERP <> 'SE'    \r\n")
		.append("  )    \r\n")
		.append(" inner join seycos.pol_persona per on (poli.id_cotizacion=per.id_cotizacion   \r\n")
		.append(" AND poli.id_lin_negocio=per.id_lin_negocio   \r\n")
		.append(" and poli.id_inciso=per.id_inciso   \r\n")
		.append(" and poli.id_version_pol=per.id_version_pol   \r\n")
		.append(" )    \r\n")
		.append(" WHERE  pp.ID_COTIZACION= ?     \r\n")
		.append(" AND pp.ID_PRODUCTO NOT IN (1,2)     \r\n")
		.append(" AND pp.CVE_T_POLIZA IN ('INML','GPO')    \r\n")
		.append(" AND pp.F_TER_REG = TO_DATE('12/31/4712 00:00:00', 'MM/DD/YYYY HH24:MI:SS')     \r\n")
		.append(" AND pp.B_ULT_REG_DIA = 'V'     \r\n")
		.append(" AND pp.CVE_T_DOCTO_VERP <> 'SE'     \r\n")
		.append(" AND poli.id_inciso = ?    \r\n");

		
		return jdbcTemplate.queryForObject(sql.toString(), new Object[] {poliza,inciso}, new RowMapper<DatosPolizaSeycos>() {
			@Override
			public DatosPolizaSeycos mapRow(ResultSet rs, int rowNum) throws SQLException {				
				DatosPolizaSeycos datosPolizaSeycos = new DatosPolizaSeycos(); 
				datosPolizaSeycos.setIdCotizacionSeycos(new Long(rs.getInt("ID_COTIZACION")));
				datosPolizaSeycos.setNombreAsegurado(rs.getString("NOMBRE"));
				String sit=rs.getString("SIT_POLIZA");
				if(sit.equals("NV"))
					datosPolizaSeycos.setSituacionPoliza("POLIZA NO VIGENTE");
				else if(sit.equals("PV"))
					datosPolizaSeycos.setSituacionPoliza("VIGENTE");
				else if(sit.equals("CP"))
					datosPolizaSeycos.setSituacionPoliza("COTIZACION POLIZA");
				datosPolizaSeycos.setIniVigencia(rs.getDate("F_INI_VIGENCIA"));
				datosPolizaSeycos.setFinVigencia(rs.getDate("F_FIN_VIGENCIA"));
				LogDeMidasEJB3.log(this.getClass().getName()+". find ID_COTIZACION : "+datosPolizaSeycos.getIdCotizacionSeycos(), Level.INFO, null);
				return datosPolizaSeycos;			
			}
		});       
  }
   private boolean existeIncisoPolizaVida(Long  poliza,Long inciso){
	   if(getIncisoPolizaVida(poliza,inciso).getIdCotizacionSeycos()!=null)
		   return true;
	   else
		   return false;
   }
   private boolean esPolizaVida(String poliza){
	   if(getNumeroPolizaVida(poliza)!=null)
		   return true;
	   else
		   return false;
   }

	public ValidacionEstatusUsuarioActual validarEstatusUsuarioActual(){
		ValidacionEstatusUsuarioActual validacionUsuario= new ValidacionEstatusUsuarioActual();
		List<PolizasClienteMovil> polizasClienteMovilList =this.findByProperty("usuario",usuarioService.getUsuarioActual().getNombreUsuario());
		PolizasClienteMovil polizasClienteMovil=null;
		try{
			polizasClienteMovil=polizasClienteMovilList.get(0);
		}
		catch(Exception e){
			polizasClienteMovil=null;
		}
		if(polizasClienteMovil.getBloqueado()=="Y"){					
			validacionUsuario.setActivo(false);
			validacionUsuario.setMensaje("Usuario bloqueado para impresión");
			//return mensaje;
		}
		else{
			validacionUsuario.setActivo(true);
			validacionUsuario.setMensaje("Usuario Activo");
		}
		return validacionUsuario;
	}
	
	@SuppressWarnings("unchecked")
	public List<PolizasClienteMovil> findByFilters(PolizasClienteMovil filter) {
		LogDeMidasEJB3.log("findByFilters  PolizasClienteMovil instances",
				Level.INFO, null);
		List<PolizasClienteMovil> lista = new ArrayList<PolizasClienteMovil>();
		Map<Integer, Object> params = new HashMap<Integer, Object>();
		final StringBuilder queryString = new StringBuilder("");
		try {
			queryString
					.append("select distinct" +
							"entidad.usuario as usuario, entidad.poliza as poliza," +
							"entidad.descripcion as descripcion,entidad.inciso as inciso," +
							"entidad.tipo as tipo,entidad.bloqueado as bloqueado,"+
							"entidad.origenTipoPoliza as origenTipoPoliza,"+
							"entidad.polizasPermitidos as polizasPermitidos," +
							"entidad.nombreUsuario as nombreUsuario, entidad.email as email");
			
			queryString.append(" from MIDAS.TRUSRPOLIZA entidad ");
			queryString.append(" where ");
			if (isNotNull(filter)) {
				int index = 1;
				if (isNotNull(filter.getUsuario())) {
					addCondition(queryString,
							" entidad.usuario =? ");
					params.put(index, filter.getUsuario());
					index++;
				}
				if (filter.getPoliza() != null) {
					addCondition(queryString, " entidad.poliza=? ");
					params.put(index, filter.getPoliza());
					index++;
				}
				if (filter.getDescripcion() != null) {
					addCondition(queryString, " entidad.descripcion=? ");
					params.put(index, filter.getDescripcion());
					index++;
				}
				if (filter.getInciso() != null) {
					addCondition(queryString, " entidad.inciso=? ");
					params.put(index, filter.getInciso());
					index++;
				}
				if (isNotNull(filter.getTipo())) {
					addCondition(queryString, " entidad.tipo=? ");
					params.put(index, filter.getTipo());
					index++;
				}
				if (isNotNull(filter.getBloqueado())) {
					addCondition(queryString, " entidad.bloqueado=? ");
					params.put(index, filter.getBloqueado());
					index++;
				}
				if (isNotNull(filter.getOrigenTipoPoliza())) {
					addCondition(queryString, " entidad.origenTipoPoliza=? ");
					params.put(index, filter.getOrigenTipoPoliza());
					index++;
				}
				if (isNotNull(filter.getPolizasPermitidos())) {
					addCondition(queryString, " entidad.polizasPermitidos=? ");
					params.put(index, filter.getPolizasPermitidos());
					index++;
				}
				if (isNotNull(filter.getNombreUsuario())) {
					addCondition(queryString, " entidad.nombreUsuario=? ");
					params.put(index, filter.getNombreUsuario());
					index++;
				}
				if (isNotNull(filter.getEmail())) {
					addCondition(queryString, " entidad.email=? ");
					params.put(index, filter.getEmail());
					index++;
				}
				if (params.isEmpty()) {
					int lengthWhere = "where ".length();
					queryString.replace(queryString.length() - lengthWhere,
							queryString.length(), " ");
				}
				String finalQuery = getQueryString(queryString);
				Query query = entityManager.createNativeQuery(finalQuery,
						PolizasClienteMovil.class);
				if (!params.isEmpty()) {
					for (Integer key : params.keySet()) {
						query.setParameter(key, params.get(key));
					}
				}
				query.setMaxResults(100);
				lista = query.getResultList();
			}

		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("findByFilters all failed", Level.SEVERE, re);
			throw re;
		}
		return lista;

	}
	
	public boolean validaAsegurado(InfoPolizaParameter param,PolizaDTO poliza, Long idCotizacionSeycos){
		boolean result=false;
		if(poliza!=null){
		////asegurado autos y danios
		     if(quitarCaracteres(poliza.getNombreAsegurado()).equals(quitarCaracteres(param.getNombreUsuario()))){
		    	 result=true;
		     }
		     else{
		    	 result= false;
		     }
		}
		else if(idCotizacionSeycos!=null){
			///asegurado vida 
			DatosPolizaSeycos datosPolizaSeycos= getIncisoPolizaVida(idCotizacionSeycos,new Long(param.getInciso().toString()));
			if (quitarCaracteres(datosPolizaSeycos.getNombreAsegurado()).equals(quitarCaracteres(param.getNombreUsuario()))){
				result=true;
			}
			else{
				result= false;
			}
		}
		return result;
	}
	
    private String reemplazar(String cadena, String busqueda, String reemplazo) {
        return cadena.replaceAll(busqueda, reemplazo);
      }
    
    private String quitarCaracteres(String cadena) {
    	cadena=cadena.trim();
    	String caracteres="&;(;!;%;/;);=;?;';¿;[;];{;};-;+;.|;°;,:,#;¡";
    	String nombre="";
    	String [] caracteresList  = caracteres.split(";");
		for(int i=0;i<=caracteresList.length-1;i++){
			nombre=reemplazar(cadena,caracteresList[i],"");
		}
		nombre=reemplazar(cadena,";","");
		return nombre.toUpperCase();
    	
      }
    
    @Override
	public void envioNotificacionesCliente(){
    	LOG.info(">>Entrando a envioNotificacionesCliente()");
    	String certificado = sistemaContext.getRutaCertificadoClienteMovil()+ sistemaContext.getNombreCertificadoClienteSuite();
    	LOG.info(">>>Certificado>>>"+certificado);
    	String evento ="";
    	//ErrorBuilder eb = new ErrorBuilder();
    	try{
			List <NotificacionCliente> envioNotificacionesList = this.getEnviarNotificacion();			
			for (NotificacionCliente notificacionTmp : envioNotificacionesList) {
				LOG.info(">>>Datos0>>>"+notificacionTmp.getMensaje());
				LOG.info(">>>Datos1>>>"+notificacionTmp.getTipoNotificacion());
				LOG.info(">>>Datos2>>>"+notificacionTmp.getUsuario());				
				if(notificacionTmp.getTipoNotificacion().equals("renovacionPoliza")){
					evento = "mensajeRenovacion";
				} else{
					evento = "MensajeCobranza";
				}
				//nuevo metodo porque el de abajo esta depreciado
				//final List<String> registrationIdList = usuarioService.getRegistrationIds(notificacionTmp.getUsuario());
				final List<String> registrationIdList = usuarioService.getRegistrationIds(notificacionTmp.getUsuario(), "ClienteMovil");
				if(!registrationIdList.isEmpty()){
					for(String registroAndroidIos : registrationIdList){
						String idRegistration = registroAndroidIos.toString();
						String registrationIdAndroid = idRegistration.substring(2);
						String so = String.valueOf(idRegistration.charAt(0)) ;
						LOG.info(">>>Datos3>>>"+idRegistration);
						LOG.info(">>>Datos4>>>"+registrationIdAndroid);
						LOG.info(">>>Datos5>>>"+so);
						LOG.info(">>>Datos6>>>"+evento);
						LOG.info(">>>Datos7>>>"+sistemaContext.getClienteMovilSenderKey());
						if(so.equals("A")){
							LOG.info(">>> Datos 8 ES DISPOSITIVO ANDROID>>>");
							final Builder builder = new Builder();
							builder.addData("message",  notificacionTmp.getMensaje());
							builder.addData("evento",evento);
							if(notificacionTmp.getTipoNotificacion().equals("renovacionPoliza")){
								builder.addData("title", "Renovacion de Poliza");
							}else {
								builder.addData("title", notificacionTmp.getTipoNotificacion());
							}
							builder.addData("idClientePoliza", notificacionTmp.getIdClientePoliza());
							builder.addData("tipoPoliza", notificacionTmp.getTipoPoliza());
							builder.addData("idNotificacion", notificacionTmp.getIdNotificacion());
							LOG.info(">>> Datos 9 se termina de generar el builder para enviar notificacion ClienteMovil>>>");	
							final Sender sender = new Sender(sistemaContext.getClienteMovilSenderKey());//Se obtiene despues de registrar la app en la consola de desarrolladores de google
							LOG.info(">>> Datos 10 despues de crear el Sender ClienteMovil>>>");							
							Result result = sender.send(builder.build(), registrationIdAndroid, 0);
							LOG.info(">>> Datos 11 despues de enviar la notificacion>>>");
							LOG.info(">>>Datos12>>>"+ result.getMessageId());
							LOG.info(">>>Datos13>>>"+ result.getCanonicalRegistrationId());
							LOG.info(">>>Datos14>>>"+ result.getErrorCodeName());
						} else if(so.equals("I")){
							final Map<String, String> data = new HashMap<String, String>();
							data.put("idClientePoliza",  notificacionTmp.getIdClientePoliza());
							data.put("tipoPoliza", notificacionTmp.getTipoPoliza());
							this.advertiseClienteSuite(notificacionTmp.getMensaje(), "SISTEMA", notificacionTmp.getUsuario(), notificacionTmp.getTipoNotificacion(), data, idRegistration, evento);					
						}
					}
				}else{
					LogDeMidasEJB3.log(this.getClass().getName()+"RegistrationId No encontrado", 
			    			Level.INFO, null);
				}				
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
	}

	public Date getValidOn() {
		return validOn;
	}

	public void setValidOn(Date validOn) {
		this.validOn = validOn;
	}

	public Date getRecordFrom() {
		return recordFrom;
	}

	public void setRecordFrom(Date recordFrom) {
		this.recordFrom = recordFrom;
	}
 
	@SuppressWarnings("unused")
	public String getTipoProductoPoliza(NumeroPolizaCompleto numeroPoliza){
		ErrorBuilder eb = new ErrorBuilder();
		String tipoPoliza = "";
		LogDeMidasEJB3.log(this.getClass().getName()+".getTipoProductoPoliza numeroPoliza: "+numeroPoliza.toString(), Level.INFO, null);
		
		if (numeroPoliza != null) {
			PolizaDTO poliza = polizaFacadeRemote.find(numeroPoliza);
			if (poliza == null) {
				if (numeroPoliza.toString().length() == 17){
					boolean espolizaVidaValida = esNoPolizaVida(numeroPoliza.toString());
					LogDeMidasEJB3.log(this.getClass().getName()+".getTipoProductoPoliza: espolizaVidaValida="+espolizaVidaValida, Level.INFO, null);
					if (espolizaVidaValida == true) {
						tipoPoliza = RAMOVIDA;
					} else {
						throw new ApplicationException(eb.addFieldError("numeroPoliza", "No se encuentra la pÃ³liza"));
					}
				} else{
					throw new ApplicationException(eb.addFieldError("numeroPoliza", "No se encuentra la pÃ³liza."));
				}
			} else{
				String claveNegocio = poliza.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getClaveNegocio();
				LogDeMidasEJB3.log(this.getClass().getName()+".getTipoProductoPoliza claveNegocio: "+claveNegocio, Level.INFO, null);
				if (claveNegocio.equals(RAMOAUTOS)) {
					tipoPoliza = RAMOAUTOS;
				} else if (polizaFacadeRemote.esPolizaCasa(poliza.getIdToPoliza())){
					tipoPoliza = RAMOCASA;
				} else {
					String codigoProducto = poliza.getCotizacionDTO().getTipoPolizaDTO().getProductoDTO().getCodigo();
					LogDeMidasEJB3.log(this.getClass().getName()+".getTipoProductoPoliza codigoProducto:"+codigoProducto+" codigoproducto2:"+poliza.getCodigoProducto(), Level.INFO, null);
					
					if (codigoProducto.equals(CODIGO_PRODUCTO_PYME)){
						tipoPoliza = RAMOPYME;
					} else{
						tipoPoliza = RAMODANIO;						
					}
				}
			}
		} else{
			throw new ApplicationException(eb.addFieldError("numeroPoliza",
			"numeroPoliza es requerido"));
		}
		
		return tipoPoliza;
	}
	
   private Integer getNoPolizaVida(Long noPoliza, int idCentroEmis, int numRenovPol) {
	   ErrorBuilder eb = new ErrorBuilder();
	   
   	   StringBuilder sql = new StringBuilder();	 
   	   sql.append(" SELECT pp.ID_COTIZACION \r\n")
   		.append(" FROM seycos.pol_poliza pp \r\n")		
   		.append(" WHERE pp.id_centro_emis = ?  \r\n")
   		.append(" AND pp.num_poliza = ?   \r\n") 
   		.append(" AND pp.num_renov_pol = ? \r\n")
   		.append(" AND ID_PRODUCTO NOT IN (1,2)  \r\n")
   		.append(" AND CVE_T_POLIZA IN ('INML','GPO') \r\n")
   		.append(" AND F_TER_REG = TO_DATE('12/31/4712 00:00:00', 'MM/DD/YYYY HH24:MI:SS')  \r\n")
   		.append(" AND B_ULT_REG_DIA = 'V'  \r\n")
   		.append(" AND CVE_T_DOCTO_VERP <> 'SE'  \r\n");
   		
   	   Integer poliza = null;
   		
   	   try {
   		   poliza = (Integer) jdbcTemplate.queryForObject(sql.toString(), new Object[] {idCentroEmis, noPoliza, numRenovPol}, new RowMapper<Integer>() {
	   			@Override
	   			public Integer mapRow(ResultSet rs, int rowNum) throws SQLException {				
	   				Integer poliza_ = rs.getInt("ID_COTIZACION");
	   				LogDeMidasEJB3.log(this.getClass().getName()+".getNoPolizaVida ID_COTIZACION : "+poliza_, Level.INFO, null);
	   				return poliza_;			
	   			}
   		   }); 
   	   } catch(EmptyResultDataAccessException e){
   		   throw new ApplicationException(eb.addFieldError("numeroPoliza",
   				   "El noPoliza "+noPoliza+" con centroEmis:"+idCentroEmis+" y numRenovPol:"+numRenovPol+" no existe"));
   	   }
   		
   		return poliza;
   }
   
   private boolean esNoPolizaVida(String poliza){
	   int idCentroEmis = Integer.parseInt(poliza.substring(0, 3));
	   Long noPoliza = Long.parseLong(poliza.substring(4, 14));
	   int numRenovPol = Integer.parseInt(poliza.substring(15, 17));
	   LogDeMidasEJB3.log(this.getClass().getName()+".esNoPolizaVida poliza : "+poliza+" \nidCentroEmis:"+idCentroEmis+" noPoliza:"+noPoliza+" numRenovPol:"+numRenovPol, Level.INFO, null);
	
	   if (getNoPolizaVida(noPoliza, idCentroEmis, numRenovPol) != null)
		   return true;
	   else
		   return false;
   }
   
	public boolean existePolizasMigradaClienteMovil(InfoPolizaParameter param) {
		LogDeMidasEJB3.log("finding PolizaMigradaClienteMovil instance with poliza: "
				+ param.getNumeroPoliza().toString()+ "inciso:" +param.getInciso() + " IMEI:"+ param.getImei() , Level.INFO, null);
		boolean existe=false;
		try {
			final String queryString = "select model from PolizasClienteMovil model where "
					+ " model.poliza " + " = :propertyPoliza "
					+ " and model.inciso " + " = :propertyInciso "
					+ " and model.imei " + " = :propertyImei ";
			Query query = em.createQuery(queryString);
			query.setParameter("propertyPoliza", param.getNumeroPoliza().toString().trim());
			query.setParameter("propertyInciso", new Short(param.getInciso().trim()));
			query.setParameter("propertyImei", param.getImei().toString().trim());
			if(query.getResultList().size()>0)
				existe =true;
			else
				existe=false;
		} catch (RuntimeException re) {
			existe=false;
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
		return existe;
	}

	@SuppressWarnings("unchecked")
	public List<PolizasClienteMovil> getPolizasMigradaClienteMovilByImei(String imei) {
		LogDeMidasEJB3.log("finding getPolizasMigradaClienteMovilByImei IMEI: "	+ imei, Level.INFO, null);
		try {
			final String queryString = "select model from PolizasClienteMovil model where "
					+ " model.imei " + " = :propertyImei ";
			Query query = em.createQuery(queryString);
			query.setParameter("propertyImei", imei);
			
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find getPolizasMigradaClienteMovilByImei name failed", Level.SEVERE, re);
			throw re;
		}
	}

	@Override
	public void addPolizasMigradasToUser(String imei, String userName) {
		LogDeMidasEJB3.log("addPolizasMigradasToUser IMEI: "+ imei+" userName:"+userName, Level.INFO, null);
		
		if (userName != null && !userName.equals("")){
			jdbcTemplate.update("UPDATE MIDAS.trusrpoliza SET usuario = ? " +
					"WHERE imei = ? ", new Object[]{userName, imei});
		} 
	}
	
	private void advertiseClienteSuite(String message, String senderUserName, String receiptUserName, String evento, 
			Map<String, String> data, String registrationId, String title){
		LOG.info(">>>message>>>"+message);
		LOG.info(">>>senderUserName>>>"+senderUserName);
		LOG.info(">>>receiptUserName>>>"+receiptUserName);
		LOG.info(">>>evento>>>"+evento);
		SendAPNs apn = new SendAPNs();
		try {
			String pathFile = "";			
			if (System.getProperty("os.name").toLowerCase().indexOf("windows") != -1){
				pathFile = SistemaPersistencia.UPLOAD_FOLDER + sistemaContext.getNombreCertificadoClienteSuite();//"SuiteClienteMovil.p12";
			}else{
				pathFile = sistemaContext.getRutaCertificadoClienteMovil()+ sistemaContext.getNombreCertificadoClienteSuite();
			}
			String pw = sistemaContext.getPasswordCertificadoClienteSuite();//"Af1rm3$2k";//
			LOG.info(">>print");
			data.put("message",  message);
			data.put("sender", senderUserName);
			data.put("senderName", receiptUserName);
			data.put("evento", evento);
			data.put("title", title);
			LOG.info("<<<final data");			
			apn.sendAPN(data, registrationId, pathFile, pw, message, sistemaContext.getClienteSuiteEnvironment());
		}catch (Exception e){
			LOG.error(e.getMessage(), e);
			e.getMessage();
		}
	}
	
	public void initialize() {
		String timerInfo = "TimerClientePolizas";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {				
				expression.minute(5);
				expression.hour("*");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerClientePolizas", false));
				
				LOG.info("Tarea TimerClientePolizas configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerClientePolizas");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerClientePolizas:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		//envioNotificacionesCliente();
	}
	
	@SuppressWarnings("unchecked")
	public List<String> getUsuariosPorPoliza(Long idPoliza, Long inciso) {
		final String queryString = " SELECT pol.usuario FROM MIDAS.TRUSRPOLIZA pol "
				+ " WHERE pol.IDTOPOLIZA = ?1 AND inciso = ?2 "
				+ " AND pol.usuario is not null ";
		final Query query = entityManager.createNativeQuery(queryString);
		query.setParameter(1, idPoliza);
		query.setParameter(2, inciso);
		return query.getResultList();
	}

	public ValidacionPolizaRenovada existeRenovacionPosteriorVigente(InfoPolizaParameter param){
				
		Integer numeroRenovacion ;
		ValidacionPolizaRenovada  validacion = new ValidacionPolizaRenovada();
		Date fechaActual = new Date();
		try {
			if (param.getNumeroPoliza() instanceof NumeroPolizaCompletoMidas) {
				NumeroPolizaCompletoMidas numeroPolizaCompletoMidas = (NumeroPolizaCompletoMidas) param.getNumeroPoliza();			
				PolizaDTO polizaDTO = new PolizaDTO();
				polizaDTO.setCodigoProducto(numeroPolizaCompletoMidas.getCodigoProducto());
				polizaDTO.setCodigoTipoPoliza(numeroPolizaCompletoMidas.getCodigoTipoPoliza());
				polizaDTO.setNumeroPoliza(numeroPolizaCompletoMidas.getNumeroPoliza());
				numeroRenovacion = numeroPolizaCompletoMidas.getNumeroRenovacion();
				
				List<PolizaDTO> listaPoliza = polizaFacadeRemote.buscarFiltrado(polizaDTO);
				if (listaPoliza != null && listaPoliza.size() >= 1) {
					for(PolizaDTO dto : listaPoliza ){
						if(numeroRenovacion < dto.getNumeroRenovacion()
								&& dto.getCotizacionDTO().getFechaInicioVigencia().compareTo(fechaActual)<0 
								&& dto.getCotizacionDTO().getFechaFinVigencia().compareTo(fechaActual)>0){
							validacion.setRenovacionPosteriorVigente(true);
							validacion.setNumeroPoliza(dto.getNumeroPolizaFormateada());
							validacion.setPolizaVencida(false);
						}else if(numeroRenovacion == dto.getNumeroRenovacion() 
								&& dto.getCotizacionDTO().getFechaFinVigencia().compareTo(fechaActual)<0 ){
							validacion.setRenovacionPosteriorVigente(false);
							validacion.setNumeroPoliza(dto.getNumeroPolizaFormateada());
							validacion.setPolizaVencida(true);
						}
						
					}
				}
		} else if (param.getNumeroPoliza() instanceof NumeroPolizaCompletoSeycos) {
				NumeroPolizaCompletoSeycos numeroPolizaCompletoSeycos = (NumeroPolizaCompletoSeycos) param.getNumeroPoliza();
				Integer centroEmisor = numeroPolizaCompletoSeycos.getIdCentroEmisor();
				Long numeroPoliza = numeroPolizaCompletoSeycos.getNumeroPoliza();
				numeroRenovacion = numeroPolizaCompletoSeycos.getNumeroRenovacion();
				List<DatosPolizaSeycos> listaPolizaSey = new ArrayList<DatosPolizaSeycos>();
				listaPolizaSey = this.busquedaPolizaSinRenovacion(centroEmisor, numeroPoliza);
				if (listaPolizaSey != null && listaPolizaSey.size() >= 1) {
					for(DatosPolizaSeycos dto : listaPolizaSey ){
						if(numeroRenovacion < dto.getNumRenovacion() && dto.getIniVigencia().compareTo(fechaActual)<0 &&
								dto.getFinVigencia().compareTo(fechaActual)>0){
							validacion.setRenovacionPosteriorVigente(true);
							validacion.setNumeroPoliza(String.format("%03d",centroEmisor) + "-" + String.format("%010d",numeroPoliza) 
									+ "-" + String.format("%02d",dto.getNumRenovacion()) );
							validacion.setPolizaVencida(false);
						}else if(numeroRenovacion == dto.getNumRenovacion() 
								&& dto.getFinVigencia().compareTo(new Date ())<0 ){
							validacion.setRenovacionPosteriorVigente(false);
							validacion.setNumeroPoliza(String.format("%03d",centroEmisor) + "-" + String.format("%010d",numeroPoliza)
									+ "-" + String.format("%02d",numeroRenovacion));
							validacion.setPolizaVencida(true);
						}
						
					}
				}
			} else {
				throw new IllegalArgumentException("Tipo de NumeroPolizaCompleto desconocido");
			}
		
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
		return validacion;
	}
	
	/** Realiza la busqueda de la póliza por centro emisor y numero de poliza en Seycos **/
	private List<DatosPolizaSeycos> busquedaPolizaSinRenovacion(Integer centroEmisor,Long numeroPoliza){
		StringBuilder sql = new StringBuilder();
				
		sql.append("SELECT pp.id_cotizacion as cotizacion_ID, pp.num_renov_pol as renovacion , pp.sit_poliza as estatus, pp.f_fin_vigencia as fechaFinVigencia, pp.f_ini_vigencia as fechaIniVigencia\r\n")
		.append("FROM seycos.pol_poliza pp  \r\n")
		.append("WHERE pp.id_centro_emis = ? \r\n")
		.append("AND pp.num_poliza = ? \r\n")
		.append("AND pp.f_ter_reg = date '4712-12-31' \r\n")
		.append("AND TRIM(pp.b_ult_reg_dia) = 'V' \r\n")
		.append("AND TRIM(pp.cve_t_docto_verp) <> 'SE' \r\n")
		.append("ORDER BY renovacion asc");
		
        
		return (List<DatosPolizaSeycos>)jdbcTemplate.query(sql.toString(), new Object[] {centroEmisor,numeroPoliza},new RowMapper<DatosPolizaSeycos>() {
			@Override
			public DatosPolizaSeycos mapRow(ResultSet rs, int rowNum) throws SQLException {				
						//DatosPolizaSeycos polSeycos = new DatosPolizaSeycos();	
						DatosPolizaSeycos datosPolizaSeycos = new DatosPolizaSeycos(); 
						datosPolizaSeycos.setIdCotizacionSeycos(new Long(rs.getInt("cotizacion_ID")));
						datosPolizaSeycos.setNumRenovacion(rs.getInt("renovacion"));
						datosPolizaSeycos.setSituacionPoliza(rs.getString("estatus"));
						datosPolizaSeycos.setFinVigencia(rs.getDate("fechaFinVigencia"));
						datosPolizaSeycos.setIniVigencia(rs.getDate("fechaIniVigencia"));
						//polSeycos.add(datosPolizaSeycos);
						
						return datosPolizaSeycos;			
					}
				});  
	}
	class ValidacionPolizaRenovada {
		private Boolean renovacionPosteriorVigente;
		private String numeroPoliza;
		private Boolean polizaVencida;
				
		public Boolean getRenovacionPosteriorVigente() {
			return renovacionPosteriorVigente;
		}

		public void setRenovacionPosteriorVigente(Boolean renovacionPosteriorVigente) {
			this.renovacionPosteriorVigente = renovacionPosteriorVigente;
		}

		public String getNumeroPoliza() {
			return numeroPoliza;
		}

		public void setNumeroPoliza(String numeroPoliza) {
			this.numeroPoliza = numeroPoliza;
		}
		public Boolean getPolizaVencida() {
			return polizaVencida;
		}

		public void setPolizaVencida(Boolean polizaVencida) {
			this.polizaVencida = polizaVencida;
		}
	}

	public byte[] generarPdf(PolizasClienteMovil polizasClienteMovil){
		byte[] byteArray = null;
		try {
			if (polizasClienteMovil.getOrigenTipoPoliza().equals(TIPOPOLIZAMIDAS)
						&&  polizasClienteMovil.getTipo().equals(NegocioSeguros.CLAVE_AUTOS)) {
					LOG.info("Es de Autos");		
					//this.validateDate();
					TransporteImpresionDTO transporte = null;
					DateTime validOnDT = new DateTime(getValidOnMillis());
					DateTime recordFromDT = new DateTime(getRecordFromMillis());
					double dias = Utilerias.obtenerDiasEntreFechas(new Date(), polizasClienteMovil.getPolizaDTO().getCotizacionDTO().getFechaInicioVigencia());
					Date fechaInicio = new Date();
					if(dias > 0){
						fechaInicio = polizasClienteMovil.getPolizaDTO().getCotizacionDTO().getFechaInicioVigencia();
						validOnDT = new DateTime(fechaInicio.getTime());
					}
					EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndosoByValidFrom(polizasClienteMovil.getPolizaDTO().getIdToPoliza().longValue(), fechaInicio);
					if (ultimoEndosoDTO != null) {
						recordFromDT= new DateTime(ultimoEndosoDTO.getRecordFrom());
					} else {
						recordFromDT = new DateTime(fechaInicio.getTime());					
					}
					transporte= generaPlantillaReporteBitemporalService.imprimirPoliza(
								 polizasClienteMovil.getPolizaDTO().getIdToPoliza(), new Locale("es","MX"), validOnDT, recordFromDT, 
								 true, true, true, true, true, 
								 new Short("2"), false, false);
					fileName = "poliza_" +polizasClienteMovil.getPolizaDTO().getNumeroPolizaFormateada() + ".pdf";
					byteArray = transporte.getByteArray();
				}else if (polizasClienteMovil.getOrigenTipoPoliza().equals(TIPOPOLIASEYCOS)) {	
					LOG.info("Es de Vida");
					PrintReportClientImpl print = new PrintReportClientImpl();
					
					getDatosSeycos(polizasClienteMovil.getId().toString());
					String cotizacionID = null;
					if (datosPoliza.size() > 0) {
						cotizacionID =  datosPoliza.get(0).toString();
					}
					if ("A".equals(polizasClienteMovil.getTipo())) {
						byteArray = print.getAutoInsurancePolicy(cotizacionID);			
					} else if ("V".equals(polizasClienteMovil.getTipo())) {
						byteArray = print.getLifeInsurancePolicy(cotizacionID);
					} else {
						byteArray = print.getHousePolicy(cotizacionID);
					}
					fileName = "poliza_" + polizaNo + ".pdf";
					
				}		
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al geberar pdf de renovacion poliza ", Level.SEVERE, e);
		}
		
		return byteArray;
	}

	
	public void enviarDocumentosRenovacion(EnvioCaratulaParameter params){
		LOG.info("Entra a enviarDocumentosRenovacionServiceImpl : " + params.getPolizaID());		
		this.mailService = params.getMailService();
		this.entidadService = params.getEntidadService();
		try {
			if(StringUtils.isNotBlank(params.getPolizaID())||params.getPolizaID()!=null){
				PolizasClienteMovil polizasClienteMovil= this.findById(new Long(params.getPolizaID()));
				byte[] pdfCaratula = null;
				pdfCaratula = this.generarPdf(polizasClienteMovil);
				sendMailUserRenovacion(pdfCaratula, polizasClienteMovil.getEmail());
				guardarEnvioMailRenovacion(polizasClienteMovil);
				if(params.getIdNotificacionRenov()!=null){
					NotificacionClienteMovil notificacion = notificacionClienteService.findById(params.getIdNotificacionRenov());
					LOG.info("notificacion : " + notificacion.getId());	
					notificacionClienteService.delete(notificacion);
				}
			}	
			
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al geberar pdf de renovacion poliza ", Level.SEVERE, e);
		}
		
		
	}
	private void sendMailUserRenovacion(byte[] filePoliza, String email) {
		ByteArrayAttachment attachment = null;
		List<String> destinatarios = new ArrayList<String>();
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		try{	
			attachment = new ByteArrayAttachment(fileName, TipoArchivo.PDF, filePoliza);		
			if(attachment != null){	
				destinatarios.add(email);
				destinatarios.add("0leonor.arreola@afirme.com");
				attachment = new ByteArrayAttachment(fileName, TipoArchivo.PDF, filePoliza);		
				adjuntos.add(attachment);
				mailService.sendMail(destinatarios, "Caratula Poliza_"+fileName+ "_Renovacion ", 
						"Estimado Asegurado:\n\n"
						+"Le notificamos que su póliza ha sido renovada, por lo cual adjuntamos la documentación contractual correspondiente.\n"
						+ "Cualquier duda o aclaración favor de consultarlo con su Agente de Seguros o Sucursal Bancaria dónde adquirió su póliza.",
						adjuntos, "Estimado(a): Leonor Itzel Arreola Cruz");	
			}		 
		}catch(Exception ex){
			LogDeMidasEJB3.log("Error al enviar correo de renovacion poliza ", Level.SEVERE, ex);
		}
		
	}
	private void guardarEnvioMailRenovacion(PolizasClienteMovil polizasClienteMovil) {
		PolizaRenovadaEnvioDocMovil polizaRenovada = new PolizaRenovadaEnvioDocMovil();
		polizaRenovada.setDestinatarios(polizasClienteMovil.getEmail());
		polizaRenovada.setPoliza(polizasClienteMovil.getPoliza());
		polizaRenovada.setAsunto("Renovacion Póliza");
		polizaRenovada.setCc("");
		polizaRenovada.setEnviado(true);
		polizaRenovada.setFechaEnvio(new Date());
		polizaRenovada.setMensaje("Su poliza ha sido renovada");
		polizaRenovada.setUsuario(usuarioService.getUsuarioActual().getNombre());
		polizaRenovada.setNombreUsuarioLog(usuarioService.getUsuarioActual().getNombreUsuario());
		polizaRenovada.setIdToPoliza(polizasClienteMovil.getPolizaDTO() == null ? 0l: polizasClienteMovil.getPolizaDTO().getIdToPoliza().longValue());
		polizaRenovada.setIdCotizacionSey(polizasClienteMovil.getIdCotizacionSeycos()==null?0l: polizasClienteMovil.getIdCotizacionSeycos());
		polizaRenovadaEnvioDocMovilDao.save(polizaRenovada);
	}
	
	
	public void enviarNotificacionCorreoRenovacion(Long idPoliza){
		List<PolizasClienteMovil> polClienteList = new ArrayList<PolizasClienteMovil>();
		EnvioCaratulaParameter param = new EnvioCaratulaParameter();
		polClienteList = this.findByProperty("idToPoliza", idPoliza);
		
		
		if(polClienteList!=null && !polClienteList.isEmpty()){
			for(PolizasClienteMovil polCliente : polClienteList){
				EnvioCaratulaParameter params = new EnvioCaratulaParameter();
				params.setPolizaID(polCliente.getId().toString());
				param.setMailService(mailService);
				param.setEntidadService(entidadService);
				param.setGeneraPlantillaReporteBitemporalService(generaPlantillaReporteBitemporalService);
				this.enviarDocumentosRenovacion(params);
				
			}
		}
	}
	
	
}
