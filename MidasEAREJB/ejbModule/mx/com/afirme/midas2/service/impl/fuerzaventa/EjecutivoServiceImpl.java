package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.EjecutivoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.dto.fuerzaventa.EjecutivoView;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
@Stateless
public class EjecutivoServiceImpl implements EjecutivoService{
	private EjecutivoDao ejecutivoDao;
	@Override
	public List<Ejecutivo> findByFilters(Ejecutivo arg0) {
		return ejecutivoDao.findByFilters(arg0, null);
	}
	
	@Override
	public List<Ejecutivo> findAllEjecutivoResponsable() {
		return ejecutivoDao.findAllEjecutivoResponsable();
	}

	@Override
	public List<Ejecutivo> findByGerencia(Long arg0) {
		return ejecutivoDao.findByGerencia(arg0);
	}
	@EJB
	public void setEjecutivoDao(EjecutivoDao ejecutivoDao) {
		this.ejecutivoDao = ejecutivoDao;
	}
	@Override
	public void unsubscribe(Ejecutivo arg0) throws Exception {
		ejecutivoDao.unsubscribe(arg0);
	}

	@Override
	public Ejecutivo saveFull(Ejecutivo arg0) throws Exception {
		return ejecutivoDao.saveFull(arg0);
	}
	@Override
	public Ejecutivo loadById(Ejecutivo ejecutivo){
		return ejecutivoDao.loadById(ejecutivo, null);
	}

	@Override
	public List<EjecutivoView> findByFiltersView(Ejecutivo arg0) {
		return ejecutivoDao.findByFiltersView(arg0);
	}
	@Override
	public List<EjecutivoView> getList(boolean onlyActive){
		return ejecutivoDao.getList(onlyActive);
	}
	@Override
	public List<EjecutivoView> findByGerenciaLightWeight(Long idParent){
		return ejecutivoDao.findByGerenciaLightWeight(idParent);
	}
	@Override
	public List<EjecutivoView> findEjecutivosConGerenciasExcluyentes(List<Long> ejecutivos,List<Long> gerenciasExcluyentes,List<Long> centrosExcluyentes){
		return ejecutivoDao.findEjecutivosConGerenciasExcluyentes(ejecutivos,gerenciasExcluyentes,centrosExcluyentes);
	}	
	
	@Override
	public Ejecutivo loadById(Ejecutivo ejecutivo, String fechaHistorico){
		return ejecutivoDao.loadById(ejecutivo,fechaHistorico );
	}
}
