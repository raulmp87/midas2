package mx.com.afirme.midas2.service.impl.negocio.condicionespecial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.condicionespecial.NegocioCondicionEspecialDao;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.StringUtil;

@Stateless
public class NegocioCondicionEspecialServiceImpl implements	NegocioCondicionEspecialService {

	@EJB
	private NegocioCondicionEspecialDao negocioCondicionEspecialDao;

	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;

	@EJB
	public CondicionEspecialService condicionEspecialService;

	@EJB
	public EntidadService entidadService;

	@EJB
	public EntidadDao entidadDao;
	private Negocio negocio = new Negocio();

	@Override
	public void aplicarExternos(BigDecimal idToNegocio, Integer aplicaExterno) {
		List<NegocioCondicionEspecial> listaAsociadas = negocioCondicionEspecialDao
				.obtenerCondicionesNegocio(idToNegocio.longValue(),
						NivelAplicacion.TODAS);

		for (NegocioCondicionEspecial negocioCondicion : listaAsociadas) {
			negocioCondicion.setAplicaExterno(aplicaExterno);
		}
		entidadService.saveAll(listaAsociadas);
	}

	@Override
	public boolean contieneCondicionEspecial(Long idToNegocio, String codigo) {

		List<String> validarCodigo = new ArrayList<String>();
		validarCodigo.add(codigo);

		// Consume el método contieneCondicionEspecial(Long idToNegocio,List<String> codigos)
		List<String> codigos = this.contieneCondicionEspecial(idToNegocio,
				validarCodigo);

		// Si el contador es 0 indica que la condicion si es parte de
		if (codigos.size() == 0) {
			return true;
		} else {
			return false;
		}
	}

	@Override
	public List<String> contieneCondicionEspecial(Long idToNegocio,
			List<String> codigos) {

		List<String> lNoValidos = new LinkedList<String>();
		List<String> condicionesEspeciales = new ArrayList<String>();

		// Obtiene todas las condiciones especiales de un negocio
		List<NegocioCondicionEspecial> condiciones = negocioCondicionEspecialDao
				.obtenerCondicionesNegocio(idToNegocio, NivelAplicacion.TODAS);

		// Almacena en un List<String> los ID de condiciones especiales
		for (NegocioCondicionEspecial nce : condiciones) {
			condicionesEspeciales.add(nce.getCondicionEspecial().getCodigo()
					.toString());
		}

		// Itera List de condiciones especiales
		for (String cod : codigos) {
			if (!condicionesEspeciales.contains(cod)) {
				lNoValidos.add(cod);
			}
		}

		return lNoValidos;
	}
	
	
	private List<String> contieneCondicionEspecial(List<NegocioCondicionEspecial> lNegocioCondicioneEspecial,
			List<String> codigos) {

		List<String> lNoValidos = new LinkedList<String>();
		List<String> condicionesEspeciales = new ArrayList<String>();

		// Almacena en un List<String> los ID de condiciones especiales
		for (NegocioCondicionEspecial nce : lNegocioCondicioneEspecial) {
			condicionesEspeciales.add(nce.getCondicionEspecial().getCodigo()
					.toString());
		}

		// Itera List de condiciones especiales
		for (String cod : codigos) {
			if (!condicionesEspeciales.contains(cod)) {
				lNoValidos.add(cod);
			}
		}

		return lNoValidos;
	}	

	@Override
	public void guardar(NegocioCondicionEspecial negocioCondicionEspecial) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<NegocioCondicionEspecial> obtenerCondicionesBase(Long idToNegocio,
			NivelAplicacion nivelAplicacion) {
		return negocioCondicionEspecialDao.obtenerCondicionesNegocio(idToNegocio, nivelAplicacion, Boolean.TRUE);
	}

	@Override
		public List<CondicionEspecial> obtenerCondicionesEspeciales(Long idToNegocio){
			return negocioCondicionEspecialDao.listarCondicionEspecialByIdNegocio(idToNegocio);
		}
		
		@Override
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(
			NegocioCondicionEspecial negocioCondicionEspecial) {

		return null;
	}

	@Override
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(
			Long idToNegocio) {
		
		// Obtiene todas las condiciones especiales de un negocio
		List<NegocioCondicionEspecial> condiciones = negocioCondicionEspecialDao
				.obtenerCondicionesNegocio(idToNegocio, NivelAplicacion.TODAS);
		
		return condiciones;
	}

	@Override
	public List<NegocioCondicionEspecial> obtenerCondicionesNegocio(Long idToNegocio, NivelAplicacion nivelAplicacion) {
		return negocioCondicionEspecialDao.obtenerCondicionesNegocio(
				idToNegocio, nivelAplicacion);
	}

	@Override
	public void relacionar(String accion,
			NegocioCondicionEspecial negocioCondicionEspecial) {
		entidadService.executeActionGrid(accion, negocioCondicionEspecial);
	}

	@Override
	public void relacionar(String accion,
			List<NegocioCondicionEspecial> condiciones) {
		// TODO Auto-generated method stub

	}

	@Override
	public List<NegocioCondicionEspecial> obtenerCondicionesEspecialesDisponibles(	BigDecimal idToNegocio, String codigoNombre) {
		List<NegocioCondicionEspecial> listaDisponibles = new ArrayList<NegocioCondicionEspecial>();
		List<CondicionEspecial> listaPorCodigoNombre = condicionEspecialService
				.obtenerCondicionEspecialCodNombre(codigoNombre);
		List<NegocioCondicionEspecial> listaAsociadas = this
				.obtenerCondicionesNegocio(idToNegocio.longValue(),
						NivelAplicacion.TODAS);

		for (CondicionEspecial condicion : listaPorCodigoNombre) {
			if (!contains(condicion, listaAsociadas)) {
				NegocioCondicionEspecial negocioCondEspecial = new NegocioCondicionEspecial();
				negocioCondEspecial.setCondicionEspecial(condicion);
				listaDisponibles.add(negocioCondEspecial);
			}
		}

		return listaDisponibles;
	}

	private boolean contains(CondicionEspecial condicion,
			List<NegocioCondicionEspecial> listaNegocio) {
		for (NegocioCondicionEspecial negocio : listaNegocio) {
			if (negocio.getCondicionEspecial().equals(condicion)) {
				return true;
			}
		}

		return false;
	}

	@Override
	public List<NegocioCondicionEspecial> obtenerCondicionesEspecialesDisponibles(
			BigDecimal idToNegocio, BigDecimal idToSeccion) {

		List<NegocioCondicionEspecial> listaDisponibles = new ArrayList<NegocioCondicionEspecial>();
		List<NegocioCondicionEspecial> listaAsociadas = this
				.obtenerCondicionesNegocio(idToNegocio.longValue(),
						NivelAplicacion.TODAS);
		List<CondicionEspecial> listaPorSeccion = condicionEspecialService
				.obtenerCondicionEspecialPorLinea(idToSeccion);

		for (CondicionEspecial condicion : listaPorSeccion) {
			if (!contains(condicion, listaAsociadas)) {
				if (!contains(condicion, listaDisponibles)) {
					NegocioCondicionEspecial negocioCondicion = new NegocioCondicionEspecial();
					negocioCondicion.setCondicionEspecial(condicion);
					listaDisponibles.add(negocioCondicion);
				}
			}
		}

		return listaDisponibles;
	}

	@Override
	public void relacionarTodas(BigDecimal idToNegocio, BigDecimal idToSeccion,
			String codigoNombre, Integer aplicaExterno) {
		List<NegocioCondicionEspecial> listaDisponibles = new ArrayList<NegocioCondicionEspecial>();
		Integer idUsuario = usuarioService.getUsuarioActual().getId();
		negocio.setIdToNegocio(idToNegocio.longValue());

		if (StringUtil.isEmpty(codigoNombre)) {
			listaDisponibles = this.obtenerCondicionesEspecialesDisponibles(
					idToNegocio, idToSeccion);
		} else {
			listaDisponibles = this.obtenerCondicionesEspecialesDisponibles(
					idToNegocio, codigoNombre);
		}

		for (NegocioCondicionEspecial condicion : listaDisponibles) {
			condicion.setFechaCreacion(new Date());
			condicion.setNegocio(negocio);
			condicion.setCodigoUsuarioCreacion(idUsuario.toString());
			condicion.setAplicaExterno(aplicaExterno);
			condicion.setObligatoria(0);
			condicion.setProduceExcepcion(0);
		}

		entidadService.saveAll(listaDisponibles);
	}
	

	@Override
	public void desligarTodas(BigDecimal idToNegocio) {
		List<NegocioCondicionEspecial> listaAsociadas = new ArrayList<NegocioCondicionEspecial>();
		negocio.setIdToNegocio(idToNegocio.longValue());
		listaAsociadas = this.obtenerCondicionesNegocio(idToNegocio.longValue(),NivelAplicacion.TODAS);

		entidadService.removeAll(listaAsociadas);
	}

	@Override
	public List<String> validaCondicionEspecialAlta(Long idToNegocio,
			List<String> codigos) {

		List<String> noValidos = this.contieneCondicionEspecial(idToNegocio,
				codigos);

		return noValidos;
	}
	
	@Override
	public List<String> validaCondicionEspecialAlta(List<NegocioCondicionEspecial> lNegocioCondicioneEspecial,
			List<String> codigos) {

		List<String> noValidos = this.contieneCondicionEspecial(lNegocioCondicioneEspecial,
				codigos);

		return noValidos;
	}

	@Override
	public List<String> validaCondicionEspecialBaja(Long idToNegocio, List<String> listCodigocondicion) {

		List<String> listCodigoCondicionErroneosList = new ArrayList<String>();
		Map<String, NegocioCondicionEspecial> mapCondiciones = null;
		NegocioCondicionEspecial negocioCondicionEspecial = null;


		mapCondiciones = transformMap(negocioCondicionEspecialDao.obtenerCondicionesNegocio(idToNegocio, NivelAplicacion.TODAS));

		for (String codigoCondicion : listCodigocondicion) {
			if (mapCondiciones.containsKey(codigoCondicion)) {
				negocioCondicionEspecial = mapCondiciones.get(codigoCondicion);
				if (negocioCondicionEspecial.getObligatoria() == 1) {
					listCodigoCondicionErroneosList.add(codigoCondicion);
				}
			}
		}

		return listCodigoCondicionErroneosList;
	}

	private Map<String, NegocioCondicionEspecial> transformMap(List<NegocioCondicionEspecial> listaCondiciones) {
		Map<String, NegocioCondicionEspecial> mapCondiciones = new HashMap<String, NegocioCondicionEspecial>();

		for (NegocioCondicionEspecial negocioCondicion : listaCondiciones) {
			mapCondiciones.put(negocioCondicion.getCondicionEspecial().getCodigo().toString(), negocioCondicion);
		}

		return mapCondiciones;
	}
	
	@Override
	public void saveIds( String ids ){
		ids.toString();
	}




}