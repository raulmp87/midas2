package mx.com.afirme.midas2.service.impl.bitemporal.excepcion;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import org.apache.log4j.Logger;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cambiosglobales.IncisoAutoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.excepcion.CondicionExcepcion;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.excepcion.ValorExcepcion;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;
import mx.com.afirme.midas2.dto.condicionespecial.CondicionEspecialBitemporalDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.excepcion.auto.ExcepcionCotizacionEvaluacionAutoDTO;
import mx.com.afirme.midas2.dto.excepcion.auto.ExcepcionSuscripcionReporteAutoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.IncisoNumeroSerieDTO;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.excepcion.ExcepcionSuscripNegAutoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.NivelEvaluacion;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoValor;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.StringUtil;

import org.joda.time.DateTime;

@Stateless
public class ExcepcionSuscripNegAutoBitemporalServiceImpl  implements ExcepcionSuscripNegAutoBitemporalService {

	public static final Logger LOG = Logger.getLogger(ExcepcionSuscripNegAutoBitemporalServiceImpl.class);
	
	public static final String TITULO_CORREO_EXCEPCION = "Se ha detectado una excepci\u00F3n en la cotizaci\u00F3n"; 
	
	public static final String SALUDO_CORREO_EXCEPCION = "A quien corresponda.";
	
	private DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	private static final double MAX_PRIMA_TOTAL_PF_DLLS = 100000;
	
	private ExcepcionSuscripcionService excepcionService = null;
	private IncisoViewService incisoViewService = null;
	private EntidadDao entidad = null;
	private MailService mailService = null;
	private SolicitudAutorizacionService solicitudService = null;
	private IncisoAutoDao incisoAutoDao = null;
	private NegocioCondicionEspecialService negocioCondEspecialService;
	private CondicionEspecialBitemporalService condicionEspBitService;
	private CondicionEspecialService condicionEspecialService;
	
	@EJB
	public void setExcepcionService(ExcepcionSuscripcionService excepcionService) {
		this.excepcionService = excepcionService;
	}	
	
	@EJB
	public void setIncisoViewService(IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}
	
	@EJB
	public void setEntidad(EntidadDao entidad) {
		this.entidad = entidad;
	}
	
	@EJB
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	@EJB
	public void setSolicitudService(SolicitudAutorizacionService solicitudService) {
		this.solicitudService = solicitudService;
	}	
	
	@EJB
	public void setIncisoAutoDao(IncisoAutoDao incisoAutoDao) {
		this.incisoAutoDao = incisoAutoDao;
	}

	public NegocioCondicionEspecialService getNegocioCondEspecialService() {
		return negocioCondEspecialService;
	}
	@EJB
	public void setNegocioCondEspecialService(
			NegocioCondicionEspecialService negocioCondEspecialService) {
		this.negocioCondEspecialService = negocioCondEspecialService;
	}

	public CondicionEspecialBitemporalService getCondicionEspBitService() {
		return condicionEspBitService;
	}
	@EJB
	public void setCondicionEspBitService(
			CondicionEspecialBitemporalService condicionEspBitService) {
		this.condicionEspBitService = condicionEspBitService;
	}

	public CondicionEspecialService getCondicionEspecialService() {
		return condicionEspecialService;
	}
	@EJB
	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BitemporalCotizacion cotizacion, 
															NivelEvaluacion nivelEvaluacion, DateTime validoEn) {
		List<ExcepcionSuscripcionReporteAutoDTO> reporte = new ArrayList<ExcepcionSuscripcionReporteAutoDTO>();
		List<ExcepcionSuscripcion> excepciones = excepcionService.listarExcepciones("A", nivelEvaluacion);
		List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelCotizacion = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();
		if(cotizacion.getValue().getSolicitud().getClaveTipoEndoso().longValue() == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES){
			registrosNivelCotizacion = prepararCotizacionParaEvaluacionNivelCotizacion(cotizacion, validoEn);
		}
		List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelCoberturas = prepararCotizacionParaEvaluacionNivelCobertura(cotizacion, validoEn);
		List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelInciso = prepararCotizacionParaEvaluacionNivelInciso(cotizacion, validoEn);
		
		for (ExcepcionSuscripcion excepcion : excepciones) {
			evaluarExcepcion(cotizacion, reporte, registrosNivelCoberturas, registrosNivelInciso, registrosNivelCotizacion, excepcion.getId());
		}		
		return reporte;
	}
	
	public List<ExcepcionCotizacionEvaluacionAutoDTO> prepararCotizacionParaEvaluacionNivelCobertura(BitemporalCotizacion cotizacion,
																													DateTime validoEn) {
		List<ExcepcionCotizacionEvaluacionAutoDTO> lista = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion =  null;		
		
		//Collection<BitemporalInciso> incisos = cotizacion.getContinuity().getIncisoContinuities().getInProcessOrGet(validoEn,true);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.cotizacionContinuity.id",cotizacion.getContinuity().getId());		
		List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class, 
				params, validoEn, null, true);
		for (BitemporalInciso inciso : incisos) {
			if(inciso.getRecordInterval()==null){
				Collection<BitemporalCoberturaSeccion> coberturas = incisoViewService.getLstCoberturasByInciso(
																				inciso.getContinuity().getId(), validoEn.toDate(), true);
				
				for (BitemporalCoberturaSeccion cobertura: coberturas) {
					evaluacion = llenarRegistroAEvaluar(cotizacion, inciso, validoEn);				
					evaluacion.setCobertura(cobertura.getContinuity().getCoberturaDTO().getIdToCobertura());					
					evaluacion.setDeducible(cobertura.getValue().getValorDeducible());										
					evaluacion.setSumaAsegurada(cobertura.getValue().getValorSumaAsegurada());					
					lista.add(evaluacion);
				}
			}
		}		
		
		return lista;
	}
	
	public List<ExcepcionCotizacionEvaluacionAutoDTO> prepararCotizacionParaEvaluacionNivelCotizacion(BitemporalCotizacion cotizacion,
			DateTime validoEn) {
		List<ExcepcionCotizacionEvaluacionAutoDTO> lista = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion =  llenarRegistroAEvaluar(cotizacion, validoEn);	
		lista.add(evaluacion);
		return lista;
	}
	
	
	private ExcepcionCotizacionEvaluacionAutoDTO llenarRegistroAEvaluar(BitemporalCotizacion cotizacion, DateTime validoEn) {
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion =  new ExcepcionCotizacionEvaluacionAutoDTO();								
		evaluacion.setCotizacion(new BigDecimal(cotizacion.getContinuity().getNumero()));			
		evaluacion.setNegocio(cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());					
		evaluacion.setProducto(cotizacion.getValue().getSolicitud().getProductoDTO().getIdToProducto());
		evaluacion.setTipoPoliza(cotizacion.getValue().getTipoPoliza().getIdToTipoPoliza());
		evaluacion.setUsuario(cotizacion.getValue().getCodigoUsuarioCotizacion());
		evaluacion.setIdCliente(new BigDecimal(cotizacion.getValue().getPersonaContratanteId()));
		evaluacion.setDescripcionEstilo("");
		evaluacion.setEstado("");
		evaluacion.setModelo(Integer.valueOf(0));
		evaluacion.setMunicipio("");
		evaluacion.setPaquete(new Long(0));
		evaluacion.setLinea(BigDecimal.ZERO);
		evaluacion.setAmis("");
		evaluacion.setNumSerie("");
		List<NegocioCondicionEspecial> negCondicionEspecial = negocioCondEspecialService.obtenerCondicionesNegocio(cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), CondicionEspecial.NivelAplicacion.TODAS);
		List<CondicionEspecialBitemporalDTO> condicionesEspBitCot = condicionEspBitService.obtenerCondicionesAsociadasCotizacionInProcess(cotizacion.getContinuity().getId(), validoEn.toDate());
		List<CondicionEspecial> condicionesCot = new ArrayList<CondicionEspecial>();
		if(condicionesEspBitCot != null){
			for(CondicionEspecialBitemporalDTO bitCondEsp: condicionesEspBitCot){
				condicionesCot.add(bitCondEsp.getCondicionEspecial());
			}
		}
		if(negCondicionEspecial != null && condicionesCot.size() > 0){
			for(NegocioCondicionEspecial negocio: negCondicionEspecial){
				if(negocio.getProduceExcepcion() == 1){
					if(condicionesCot.contains(negocio.getCondicionEspecial())){
						evaluacion.getCondicionesCotizacion().add(negocio.getCondicionEspecial());
					}
				}
			}
		}
		return evaluacion;
	}
	
	private ExcepcionCotizacionEvaluacionAutoDTO llenarRegistroAEvaluar(BitemporalCotizacion cotizacion, BitemporalInciso inciso,
			DateTime validoEn) {
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion =  new ExcepcionCotizacionEvaluacionAutoDTO();
		BitemporalAutoInciso auto = inciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(validoEn);
		if(auto.getRecordInterval()== null){
			evaluacion.setAgente(cotizacion.getValue().getSolicitud().getCodigoAgente().intValue());									
			evaluacion.setCotizacion(new BigDecimal(cotizacion.getContinuity().getNumero()));			
			evaluacion.setInciso(new BigDecimal(inciso.getContinuity().getNumero()));					
			evaluacion.setNegocio(cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());					
			evaluacion.setProducto(cotizacion.getValue().getSolicitud().getProductoDTO().getIdToProducto());
			evaluacion.setSecuenciaInciso(inciso.getValue().getNumeroSecuencia().longValue());			
			evaluacion.setTipoPoliza(cotizacion.getValue().getTipoPoliza().getIdToTipoPoliza());
			evaluacion.setUsuario(cotizacion.getValue().getCodigoUsuarioCotizacion());
			evaluacion.setVigenciaIncial(cotizacion.getValue().getFechaInicioVigencia());
			evaluacion.setVigenciaFinal(cotizacion.getValue().getFechaFinVigencia());
			evaluacion.setDescuentoGlobal(cotizacion.getValue().getPorcentajeDescuentoGlobal());
			evaluacion.setIdCliente(new BigDecimal(cotizacion.getValue().getPersonaContratanteId()));
			evaluacion.setCveTipoPersona(cotizacion.getValue().getSolicitud().getClaveTipoPersona());
			evaluacion.setPrimaTotal(cotizacion.getValue().getValorPrimaTotal().doubleValue());
			List<NegocioCondicionEspecial> negCondicionEspecial = negocioCondEspecialService.obtenerCondicionesNegocio(cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), CondicionEspecial.NivelAplicacion.TODAS);
			List<CondicionEspecialBitemporalDTO> condicionesEspBitCot = condicionEspBitService.obtenerCondicionesAsociadasCotizacionInProcess(cotizacion.getContinuity().getId(), validoEn.toDate());
			List<CondicionEspecialBitemporalDTO> condicionesEspBitIns = condicionEspBitService.obtenerCondicionesIncisoAsociadasInProcess(inciso.getContinuity().getId(), validoEn.toDate());
			List<CondicionEspecial> condicionesCot = new ArrayList<CondicionEspecial>();
			List<CondicionEspecial> condicionesIns = new ArrayList<CondicionEspecial>();
			if(condicionesEspBitCot != null){
				for(CondicionEspecialBitemporalDTO bitCondEsp: condicionesEspBitCot){
					condicionesCot.add(bitCondEsp.getCondicionEspecial());
				}
			}
			if(negCondicionEspecial != null && condicionesCot.size() > 0){
				for(NegocioCondicionEspecial negocio: negCondicionEspecial){
					if(negocio.getProduceExcepcion() == 1){
						if(condicionesCot.contains(negocio.getCondicionEspecial())){
							evaluacion.getCondicionesCotizacion().add(negocio.getCondicionEspecial());
						}
					}
				}
			}
			if(condicionesEspBitIns != null){
				for(CondicionEspecialBitemporalDTO bitCondEsp: condicionesEspBitIns){
					condicionesIns.add(bitCondEsp.getCondicionEspecial());
				}
			}
			if(negCondicionEspecial != null && condicionesIns.size() > 0){
				for(NegocioCondicionEspecial negocio: negCondicionEspecial){
					if(negocio.getProduceExcepcion() == 1){
						if(condicionesIns.contains(negocio.getCondicionEspecial())){
							evaluacion.getCondicionesInciso().add(negocio.getCondicionEspecial());
						}
					}
				}
			}

			if (auto != null) {			
				evaluacion.setDescripcionEstilo(auto.getValue().getDescripcionFinal());
				evaluacion.setEstado(auto.getValue().getEstadoId());
				evaluacion.setModelo(Integer.valueOf(auto.getValue().getModeloVehiculo()));
				evaluacion.setMunicipio(auto.getValue().getMunicipioId());
				evaluacion.setPaquete(auto.getValue().getNegocioPaqueteId());
				evaluacion.setLinea(BigDecimal.valueOf(auto.getValue().getNegocioSeccionId()));
				evaluacion.setAmis(auto.getValue().getEstiloId() != null ?
						auto.getValue().getEstiloId().split("_") [1] : null);
				evaluacion.setNumSerie(auto.getValue().getNumeroSerie());
			}
		}else{
			if(cotizacion.getValue().getSolicitud().getClaveTipoEndoso().longValue() == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES){
				evaluacion.setCotizacion(new BigDecimal(cotizacion.getContinuity().getNumero()));			
				evaluacion.setInciso(new BigDecimal(inciso.getContinuity().getNumero()));					
				evaluacion.setNegocio(cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());					
				evaluacion.setProducto(cotizacion.getValue().getSolicitud().getProductoDTO().getIdToProducto());
				evaluacion.setSecuenciaInciso(inciso.getValue().getNumeroSecuencia().longValue());	
				evaluacion.setTipoPoliza(cotizacion.getValue().getTipoPoliza().getIdToTipoPoliza());
				evaluacion.setUsuario(cotizacion.getValue().getCodigoUsuarioCotizacion());
				evaluacion.setIdCliente(new BigDecimal(cotizacion.getValue().getPersonaContratanteId()));
				evaluacion.setDescripcionEstilo("");
				evaluacion.setEstado("");
				evaluacion.setModelo(Integer.valueOf(0));
				evaluacion.setMunicipio("");
				evaluacion.setPaquete(new Long(0));
				evaluacion.setLinea(BigDecimal.ZERO);
				evaluacion.setAmis("");
				evaluacion.setNumSerie("");
				List<NegocioCondicionEspecial> negCondicionEspecial = negocioCondEspecialService.obtenerCondicionesNegocio(cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio(), CondicionEspecial.NivelAplicacion.TODAS);
				List<CondicionEspecialBitemporalDTO> condicionesEspBitCot = condicionEspBitService.obtenerCondicionesAsociadasCotizacionInProcess(cotizacion.getContinuity().getId(), validoEn.toDate());
				List<CondicionEspecialBitemporalDTO> condicionesEspBitIns = condicionEspBitService.obtenerCondicionesIncisoAsociadasInProcess(inciso.getContinuity().getId(), validoEn.toDate());
				List<CondicionEspecial> condicionesCot = new ArrayList<CondicionEspecial>();
				List<CondicionEspecial> condicionesIns = new ArrayList<CondicionEspecial>();
				if(condicionesEspBitCot != null){
					for(CondicionEspecialBitemporalDTO bitCondEsp: condicionesEspBitCot){
						condicionesCot.add(bitCondEsp.getCondicionEspecial());
					}
				}
				if(negCondicionEspecial != null && condicionesCot.size() > 0){
					for(NegocioCondicionEspecial negocio: negCondicionEspecial){
						if(negocio.getProduceExcepcion() == 1){
							if(condicionesCot.contains(negocio.getCondicionEspecial())){
								evaluacion.getCondicionesCotizacion().add(negocio.getCondicionEspecial());
							}
						}
					}
				}
				if(condicionesEspBitIns != null){
					for(CondicionEspecialBitemporalDTO bitCondEsp: condicionesEspBitIns){
						condicionesIns.add(bitCondEsp.getCondicionEspecial());
					}
				}
				if(negCondicionEspecial != null && condicionesIns.size() > 0){
					for(NegocioCondicionEspecial negocio: negCondicionEspecial){
						if(negocio.getProduceExcepcion() == 1){
							if(condicionesIns.contains(negocio.getCondicionEspecial())){
								evaluacion.getCondicionesInciso().add(negocio.getCondicionEspecial());
							}
						}
					}
				}
			}
		}
		return evaluacion;
}
	
	private List<ExcepcionCotizacionEvaluacionAutoDTO> prepararCotizacionParaEvaluacionNivelInciso(BitemporalCotizacion cotizacion,
			DateTime validoEn) {
		List<ExcepcionCotizacionEvaluacionAutoDTO> lista = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();

		//Collection<BitemporalInciso> incisos = cotizacion.getContinuity().getIncisoContinuities().getInProcess(validoEn);		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.cotizacionContinuity.id",cotizacion.getContinuity().getId());		
		List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class, 
				params, validoEn, null, true);
		
		for (BitemporalInciso inciso : incisos){
			if(inciso.getRecordInterval()==null || 
					cotizacion.getValue().getSolicitud().getClaveTipoEndoso().longValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS ||
					cotizacion.getValue().getSolicitud().getClaveTipoEndoso().longValue() == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES){
				lista.add(llenarRegistroAEvaluar(cotizacion, inciso, validoEn));
			}
		}	
		return lista;
	}
	
	public void evaluarExcepcion(BitemporalCotizacion cotizacion, List<ExcepcionSuscripcionReporteAutoDTO> reporte, 
												List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelCoberturas, 
												List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelInciso,  
												List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelPoliza,
												Long excepcionId) {
		
		ConvertidorEstatus convertidor = new ConvertidorEstatus();
		//Integer status = null;
		String descripcionRegistroConExcepcion = "";
		ExcepcionSuscripcionReporteAutoDTO reporteDTO = null;	
		List<ExcepcionCotizacionEvaluacionAutoDTO> registrosParaEvaluar;
		ExcepcionSuscripcion excepcion = entidad.findById(ExcepcionSuscripcion.class, excepcionId);		
		//entidad.findById(ExcepcionSuscripcion.class, excepcionId);
		List<CondicionExcepcion> condiciones = excepcion.getCondicionExcepcion();
		//si la excepcion tiene condicion de cobertura evaluar todas las coberturas de todos los incisos
		if (contieneCondicion(condiciones, TipoCondicion.COBERTURA)) {
			//usar todos los registros
			registrosParaEvaluar = registrosNivelCoberturas;
		} else if (contieneCondicion(condiciones, TipoCondicion.CONDICION_ESPECIAL_COTIZACION)){
			registrosParaEvaluar = registrosNivelPoliza;
		}else{
			//filtrar por registros de tipo inciso
			registrosParaEvaluar = registrosNivelInciso;
		}
		
		for (ExcepcionCotizacionEvaluacionAutoDTO evaluador : registrosParaEvaluar) {
			for (CondicionExcepcion condicion : condiciones) {
				List<ValorExcepcion> valores = condicion.getValorExcepcions();
				Collections.sort(valores, 
						new Comparator<ValorExcepcion>() {				
							public int compare(ValorExcepcion n1, ValorExcepcion n2) {
								return n1.getId().compareTo(n2.getId());
							}
						});				
				convertidor = evaluarCondicion(cotizacion, convertidor, evaluador, 
						condicion.getTipoValor(), condicion.getTipoCondicion(), valores);
				if (convertidor.getStatus() != null && convertidor.getStatus() == 0) {					
					break;
				}				
			}			
			//cumple con excepcion
			if (convertidor.getStatus() != null && convertidor.getStatus() == 1) {								
				//realizar operaciones para reportar excepcion para la cotizacion
				convertidor.setDescripcion(convertidor.getDescripcion().trim());
				descripcionRegistroConExcepcion = convertidor.getDescripcion().substring(0, convertidor.getDescripcion().length() -1);
				reporteDTO = generarRegistroReporte(evaluador, excepcion, descripcionRegistroConExcepcion);
				if(excepcion.getRequiereNotificacion()){
					//notificar
					String recipientes = excepcion.getCorreoNotificacion();
					String titulo = TITULO_CORREO_EXCEPCION + " " + evaluador.getCotizacion();
					String saludo = SALUDO_CORREO_EXCEPCION;
					String cuerpo = generarCuerpoMensajeNotificacion(reporteDTO);
					enviarNotificacion(recipientes, titulo, titulo, saludo, cuerpo);					
				}				
				if (excepcion.getRequiereAutorizacion() && 
						!revisarValorAutorizado(excepcionId, evaluador)) {
					reporte.add(reporteDTO);
				}
			}
			convertidor.setStatus(null);
			convertidor.setDescripcion("");
		}	
	}
	
	/**
	 * Rvisa si la lista de condiciones tiene alguna condicion de tipo <code>tipoCondicion</code>
	 * @param condiciones
	 * @param tipoCondicion
	 * @return
	 */
	public boolean contieneCondicion(List<CondicionExcepcion> condiciones, TipoCondicion tipoCondicion){
		boolean contiene = false;
		for(CondicionExcepcion condicion : condiciones){
			if(condicion.getTipoCondicion() == tipoCondicion.valor()){
				contiene = true;
			}
		}
		return contiene;
	}
	
	public ConvertidorEstatus evaluarCondicion(BitemporalCotizacion biCotizacion, ConvertidorEstatus status, 
																			ExcepcionCotizacionEvaluacionAutoDTO cotizacion, 
															short tipoValor, Integer tipoCondicion, List<ValorExcepcion> valores) {
		
		Integer match = 1;
		Integer noMatch = 0;
		String valor = null;
		String valorRango = null;
		if (!valores.isEmpty()) {
			switch (tipoValor) {
				case 0:
				case 2:
				case 3:				
					valor = valores.get(0).getValor();
					break;
				case 1:
					valor = valores.get(0).getValor();
					valorRango = valores.get(1).getValor();
					break;
				default:
					break;					
			}			
		}
		switch (TipoCondicion.tipoCondicion(tipoCondicion)) {
			case AGENTE:
				if(cotizacion.getAgente() != null && cotizacion.getAgente().intValue() == Integer.valueOf(valor)){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + " AGENTE: " + cotizacion.getAgente() + ", "); 
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}			
				break;
			
			case AMIS:			
				if (cotizacion.getAmis() != null && cotizacion.getAmis().equals(valor)) {
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "AMIS: " + cotizacion.getAmis() + ", ");
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case COBERTURA:
				if (cotizacion.getCobertura() != null && cotizacion.getCobertura().longValue() == Long.valueOf(valor)) {
					String descripcion = "";
					status.setStatus(match);
					CoberturaDTO cobertura = entidad.findById(CoberturaDTO.class, cotizacion.getCobertura());
					if(cobertura != null){
						descripcion = "COBERTURA: " + cobertura.getDescripcion() + ", "; 
					}			
					status.setDescripcion(status.getDescripcion() + descripcion);					
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}				
				break;
			
			case DEDUCIBLE:				
				if (cotizacion.getDeducible() != null && 
						(cotizacion.getDeducible() >= Double.valueOf(valor) && 
								cotizacion.getDeducible() <= Double.valueOf(valorRango))){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "DEDUCIBLE: " + cotizacion.getDeducible() + ", ");				
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case DESCRIPCION_ESTILO:
				if(cotizacion.getDescripcionEstilo() != null && cotizacion.getDescripcionEstilo().trim().toUpperCase().contains(valor.toUpperCase())){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "DESC ESTILO: " + cotizacion.getDescripcionEstilo().replace(",","") + ", ");		
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case ESTADO:
				if (cotizacion.getEstado() != null && cotizacion.getEstado().equals(valor)) {
					String descripcion = "";
					status.setStatus(match);
					EstadoDTO obj = entidad.findById(EstadoDTO.class, cotizacion.getEstado());
					if (obj != null) {
						descripcion = "ESTADO: " + obj.getDescription()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);						
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case MODELO:
				if (cotizacion.getModelo() != null && (cotizacion.getModelo() >= Integer.valueOf(valor) && 
						cotizacion.getModelo() <=  Integer.valueOf(valorRango))) {
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "MODELO: " + cotizacion.getModelo() + ", ");				
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case MUNICIPIO:
				if (cotizacion.getMunicipio() != null && cotizacion.getMunicipio().equals(valor)) {
					String descripcion = "";
					status.setStatus(match);
					MunicipioDTO obj = entidad.findById(MunicipioDTO.class, cotizacion.getMunicipio());
					if (obj != null) {
						descripcion = "MUNICIPIO: " + obj.getDescription()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);	
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case NEGOCIO:
				if (cotizacion.getNegocio() != null && cotizacion.getNegocio().longValue() == Long.valueOf(valor)) {
					String descripcion = "";
					status.setStatus(match);
					Negocio obj = entidad.findById(Negocio.class, cotizacion.getNegocio());
					if (obj != null) {
						descripcion = "NEGOCIO: " + obj.getDescripcionNegocio()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);				
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			
			case PAQUETE:
				if(cotizacion.getPaquete() != null && cotizacion.getPaquete().longValue() == Long.valueOf(valor)){
					String descripcion = "";
					status.setStatus(match);
					NegocioPaqueteSeccion obj = entidad.findById(NegocioPaqueteSeccion.class, cotizacion.getPaquete());
					if(obj != null){
						descripcion = "PAQUETE: " + obj.getPaquete().getDescripcion()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);	
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case PRODUCTO:
				BigDecimal idProducto = null;
				NegocioProducto negProducto = entidad.findById(NegocioProducto.class, Long.valueOf(valor));
				if (negProducto != null){					
					idProducto = negProducto.getProductoDTO().getIdToProducto();					
					if(cotizacion.getProducto() != null && cotizacion.getProducto().longValue() == idProducto.longValue()){
						String descripcion = "";
						status.setStatus(match);
						ProductoDTO obj = entidad.findById(ProductoDTO.class, cotizacion.getProducto());
						if(obj != null){
							descripcion = "PRODUCTO: " + obj.getDescripcion()+ ", "; 
						}		
						status.setDescripcion(status.getDescripcion() + descripcion);					
					}else{
						status.setStatus(noMatch);
						status.setDescripcion("");
					}
				}
				break;
			
			case SECCION:
				if (cotizacion.getLinea() != null && cotizacion.getLinea().longValue() == Long.valueOf(valor)) { 					
					String descripcion = "";
					status.setStatus(match);
					NegocioSeccion obj = entidad.findById(NegocioSeccion.class, cotizacion.getLinea());
					if(obj != null){
						descripcion = "SECCION: " + obj.getSeccionDTO().getDescripcion()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);			
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case SUMA_ASEGURADA:
				if (cotizacion.getSumaAsegurada() != null && 
						((tipoValor == TipoValor.VALOR_DIRECTO.valor() && cotizacion.getSumaAsegurada() == Double.valueOf(valor)) ||
					     (tipoValor == TipoValor.MENOR_IGUAL.valor() && cotizacion.getSumaAsegurada() <= Double.valueOf(valor)) ||
					     (tipoValor == TipoValor.MAYOR_IGUAL.valor() && cotizacion.getSumaAsegurada() >= Double.valueOf(valor))
						)) {
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "SUMA ASEGURADA: " + cotizacion.getSumaAsegurada() + ", ");			
				} else {
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case TIPO_POLIZA:	
				BigDecimal idTipoPoliza = null;
				NegocioTipoPoliza negTipoPoliza = entidad.findById(NegocioTipoPoliza.class, new BigDecimal(valor));
				if (negTipoPoliza != null) {					
					idTipoPoliza = negTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza();				
					if (cotizacion.getTipoPoliza() != null && cotizacion.getTipoPoliza().longValue() == idTipoPoliza.longValue()) {
						String descripcion = "";
						status.setStatus(match);
						TipoPolizaDTO obj = entidad.findById(TipoPolizaDTO.class, cotizacion.getTipoPoliza());
						if(obj != null){
							descripcion = "TIPO POLIZA: " + obj.getDescripcion()+ ", "; 
						}		
						status.setDescripcion(status.getDescripcion() + descripcion);								
					} else {
						status.setStatus(noMatch);
						status.setDescripcion("");
					}
				}
				break;
			
			case USUARIO:
				valor = obtenerCondicionValor(null, TipoCondicion.USUARIO);
				//TODO obtener tipo usuario de la cotizacion
				break;
			
			
			case MODELO_ANTIGUEDAD:
				status.setStatus(noMatch);
				status.setDescripcion("");
				NegocioPaqueteSeccion negocioPaquete = entidad.findById(NegocioPaqueteSeccion.class, cotizacion.getPaquete());
				BigDecimal maxAntiguedad = negocioPaquete.getModeloAntiguedadMax();
				if (maxAntiguedad != null && maxAntiguedad.intValue() > 0) {
					GregorianCalendar fechaSeguimiento = new GregorianCalendar();
					fechaSeguimiento.setTime(new Date());
					int anioActual = fechaSeguimiento.get(GregorianCalendar.YEAR);
					int antiguedad = anioActual - cotizacion.getModelo();
					if (antiguedad > maxAntiguedad.intValue()) {
						status.setStatus(match);
						status.setDescripcion("ANTIG MODELO MAX: " + maxAntiguedad.intValue()+ ", ");
					} 	
				}
				break;
				
			
			case RANGO_VIGENCIA:				
				status.setStatus(noMatch);
				status.setDescripcion("");
				// LPV 20170117 Excepcion de validacion de vigencia no aplica en endosos, se valida antes de iniciar el endoso
				break;
				
				
			case NUM_SERIE:				
				status.setStatus(noMatch);
				status.setDescripcion("");
				if(cotizacion.getNegocio() != null){
				Negocio negocioNumSerie = entidad.findById(Negocio.class, cotizacion.getNegocio());
				if(negocioNumSerie.getAplicaValidacionNumeroSerie()){
					if (!StringUtil.isEmpty(cotizacion.getNumSerie()) && cotizacion.getVigenciaIncial() != null &&
						cotizacion.getVigenciaFinal() != null) {
						List<IncisoNumeroSerieDTO> listRepetidos;
						try {
						listRepetidos = incisoAutoDao.numerosSerieRepetidosPoliza(cotizacion.getCotizacion(), cotizacion.getInciso(), cotizacion.getNumSerie());
						
						//boolean repetidos = incisoService.existenNumeroSerieRepetidos(cotizacion.getCotizacion(), cotizacion.getInciso(), cotizacion.getNumSerie(), 
								//cotizacion.getVigenciaIncial(), cotizacion.getVigenciaFinal());
						if (listRepetidos != null && !listRepetidos.isEmpty()) {
							status.setStatus(match);
							status.setDescripcion("NUM SERIE REPETIDO:" + cotizacion.getNumSerie() + ", ");
						}
						} catch (Exception e) {
						// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}
				}
				break;
			
			
			case DESCUENTO_GLOBAL:			
				status.setStatus(noMatch);
				status.setDescripcion("");
				Map<Boolean,Double> resultDescuento = validaDescuentoGlobal(biCotizacion, cotizacion.getDescuentoGlobal());
				if (resultDescuento != null) {
					if (resultDescuento.containsKey(true)) {
						status.setStatus(match);
						status.setDescripcion("DESCUENTO GLOBAL MAX: " + resultDescuento.get(true) + ", ");
					}
				}
				break;

			case ARTICULO_140:
				status.setStatus(noMatch);
				status.setDescripcion("");
				//TODO REVISAR BANDERA DE SI YA SE VALIDARON LOS MENSAJES
				if(cotizacion.getNegocio() != null){
				Negocio negocioArt140 = entidad.findById(Negocio.class, cotizacion.getNegocio());
				if(negocioArt140.getAplicaValidacionArticulo140()){				
					if (cotizacion.getIdCliente() != null && cotizacion.getCveTipoPersona() != null) {
						if (cotizacion.getCveTipoPersona().shortValue() == 2 || 
							(cotizacion.getCveTipoPersona().shortValue() == 1 && 
									obtenerValorPrimaTotalDolares(cotizacion.getPrimaTotal()) > MAX_PRIMA_TOTAL_PF_DLLS)) {
							status.setStatus(match);
							status.setDescripcion("ART 140: CLIENTE (" + cotizacion.getIdCliente() + "), ");
						}
					}
				}
				}
				break;
			case CONDICION_ESPECIAL_COTIZACION:
				if(cotizacion.getCondicionesCotizacion() != null && cotizacion.getCondicionesCotizacion().size() > 0){
					String descripcion = "";
					StringBuilder descripcionStrBuilder = new StringBuilder();
					status.setStatus(match);
					descripcionStrBuilder.append("CONDICION ESPECIAL A NIVEL COTIZACION: ");
					for(CondicionEspecial condicion: cotizacion.getCondicionesCotizacion()){
						descripcionStrBuilder.append(condicion.getNombre());
						descripcionStrBuilder.append("-");
					}
					descripcion = descripcionStrBuilder.toString();
					status.setDescripcion(status.getDescripcion() + descripcion);				
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			case CONDICION_ESPECIAL_INCISO:
				if(cotizacion.getCondicionesInciso() != null && cotizacion.getCondicionesInciso().size() > 0){
					String descripcion = "";
					StringBuilder descripcionStrBuilder = new StringBuilder();
					status.setStatus(match);
					descripcionStrBuilder.append("CONDICION ESPECIAL A NIVEL INCISO: ");
					for(CondicionEspecial condicion: cotizacion.getCondicionesInciso()){
						descripcionStrBuilder.append(condicion.getNombre());
						descripcionStrBuilder.append("-");
					}
					descripcion = descripcionStrBuilder.toString();
					status.setDescripcion(status.getDescripcion() + descripcion);				
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;	
			default:
				break;
		}
		
		return status;
	
	}
	
	protected String generarCuerpoMensajeNotificacion(ExcepcionSuscripcionReporteDTO reporte){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:MM");
		StringBuilder cuerpo = new StringBuilder("");
		cuerpo.append("Excepci\u00F3n encontrada: " + reporte.getDescripcionExcepcion() + "<br/>");
		cuerpo.append("<ul><li>Cotizaci\u00F3n: " + reporte.getIdToCotizacion() + "</li></ul>");
		cuerpo.append("<ul><li>Secci\u00F3n (L\u00EDnea): " + reporte.getLineaNegocio() + "</li></ul>");
		cuerpo.append("<ul><li>Inciso: " + reporte.getSequenciaInciso() + "</li></ul>");
		if(reporte.getCobertura() != null){
			cuerpo.append("<ul><li>Cobertura: " + reporte.getCobertura() != null ? reporte.getCobertura():"NA" + "</li></ul>");
		}
		cuerpo.append("<ul><li>Error encontrado: " + reporte.getDescripcionRegistroConExcepcion() + "</li></ul>");
		cuerpo.append("<ul><li>Fecha: " + format.format(new Date()) + "</li></ul>");		
		return cuerpo.toString();
	}
	
	public String obtenerCondicionValor(Long idToExcepcion, TipoCondicion tipoCondicion) {
		List<String> valores = this.obtenerCondicionValores(idToExcepcion, tipoCondicion);
		return valores.isEmpty()? null : valores.get(0);
	}

	public List<String> obtenerCondicionValores(Long idToExcepcion, TipoCondicion tipoCondicion) {
		List<String> valores = new ArrayList<String>();
		CondicionExcepcion condicion = excepcionService.obtenerCondicion(idToExcepcion, tipoCondicion);
		if(condicion != null){
			for(ValorExcepcion valor : condicion.getValorExcepcions()){
				valores.add(valor.getValor());
			}
		}
		return valores;
	}
	
	private double obtenerValorPrimaTotalDolares(Double primaTotal){
		//TODO OBTENER CAMBIO DE SEYCOS
		double cambio = 14.3f;
		return primaTotal / cambio;
	} 
	
	public Map<Boolean,Double> validaDescuentoGlobal(BitemporalCotizacion cotizacion, Double porcentaje) {
		
		Map<Boolean,Double> map = new HashMap<Boolean, Double>(1);
		if (porcentaje != null) {
			if (cotizacion.getValue().getPorcentajeDescuentoGlobal() > cotizacion.getValue()
											.getSolicitud().getNegocio().getPctMaximoDescuento()) {
				map.put(true, cotizacion.getValue().getSolicitud().getNegocio().getPctMaximoDescuento());
				
			} else {
				map.put(false, porcentaje);
			}
		}
		return map;
	}
	
	public ExcepcionSuscripcionReporteAutoDTO generarRegistroReporte(ExcepcionCotizacionEvaluacionAutoDTO evaluacion, ExcepcionSuscripcion excepcion, 
			String descripcionRegistroConExcepcion){
		ExcepcionSuscripcionReporteAutoDTO reporte = new ExcepcionSuscripcionReporteAutoDTO();
		reporte.setIdToCotizacion(evaluacion.getCotizacion());		
		reporte.setIdLineaNegocio(evaluacion.getLinea());	
		reporte.setIdInciso(evaluacion.getInciso());		
		reporte.setIdCobertura(evaluacion.getCobertura());
		reporte.setSequenciaInciso(evaluacion.getSecuenciaInciso());
		reporte.setIdExcepcion(excepcion.getId());
		reporte.setDescripcionExcepcion(excepcion.getDescripcion().toUpperCase());
		reporte.setNumExcepcion(excepcion.getNumero());
		reporte.setDescripcionRegistroConExcepcion(descripcionRegistroConExcepcion);
		NegocioSeccion seccion = entidad.findById(NegocioSeccion.class, evaluacion.getLinea());
		//NegocioSeccion seccion = entidad.findById(NegocioSeccion.class, evaluacion.getLinea());
		if (seccion != null) {
			reporte.setLineaNegocio(seccion.getSeccionDTO().getDescripcion());
		}
		if (evaluacion.getCobertura() != null) {
			CoberturaDTO cobertura = entidad.findById(CoberturaDTO.class, evaluacion.getCobertura()); 
				//entidad.findById(CoberturaDTO.class, evaluacion.getCobertura());
			if (cobertura != null) {
				reporte.setCobertura(cobertura.getDescripcion());
			}
		}		
		return reporte;
	}
	
	protected void enviarNotificacion(String recipientes, String titulo, String encabezado, String saludo, String cuerpo ) {
		if (recipientes != null && recipientes.length() > 0) {
			String[] correos = recipientes.split(";");
			List<String> direcciones = Arrays.asList(correos);	
			mailService.sendMail(direcciones, encabezado, cuerpo, null, titulo, saludo);
		}
	}
	
	/**
	 * Metodo para revisar que el valor de la suma asergurada y deducibles sea el mismo que se autorizo (temporal en lo que se hace dinamico)
	 * @param descripcionRegistroConExcepcion
	 * @param evaluacion
	 */
	private boolean revisarValorAutorizado(Long excepcionId, ExcepcionCotizacionEvaluacionAutoDTO evaluacion){		
		boolean esValida = false;
		if(solicitudService.obtenerEstatusParaExcepcion(evaluacion.getCotizacion(), evaluacion.getLinea(), 
				evaluacion.getInciso(), evaluacion.getCobertura(), excepcionId) == 
							SolicitudAutorizacionService.EstatusDetalleSolicitud.AUTORIZADA.valor()){
			esValida = true;
			SolicitudExcepcionDetalle detalle = solicitudService.obtenerDetalleParaExcepcion(evaluacion.getCotizacion(), 
					excepcionId, evaluacion.getLinea(), evaluacion.getInciso(), evaluacion.getCobertura());			
			if(detalle != null){					
				//obtener cadena de la evaluacion autorizada y sacar valores de suma y deducible			
				String[] condiciones = detalle.getProvocadorExcepcion().split(",");
				for(String condicion : condiciones){
					String[] valores = condicion.split(":");
					String llave = valores[0].trim();
					String valor = null;
					if(valores.length == 2) {
						valor = valores[1].trim();
					} else {
						esValida = false;
						//Contiene comas extras
						LOG.error("El provocador de excepciones no es valido, cotizacion --> " 
						+ evaluacion.getCotizacion() + ", provocador de excepcion --> " +  detalle.getProvocadorExcepcion());
					}
					if(llave.equals("DEDUCIBLE")){
						Double deducible = Double.valueOf(valor);
						if(evaluacion.getDeducible().doubleValue() != deducible.doubleValue()){
							esValida = false;
						}
					}else if(llave.equals("SUMA ASEGURADA")){
						Double sumaAsegurada = Double.valueOf(valor);
						if(evaluacion.getSumaAsegurada().doubleValue() != sumaAsegurada.doubleValue()){
							esValida = false;
						}
					}else if(llave.equals("MODELO")){
						Integer modelo = Integer.valueOf(valor);
						if(evaluacion.getModelo().intValue() != modelo.intValue()){
							esValida = false;
						}
					}else if (llave.equals("DESC ESTILO")){
						if(!(evaluacion.getDescripcionEstilo().replace(",", "")).contains(valor)){
							esValida = false;
						}
					}else if (llave.equals("NUM SERIE REPETIDO")){
						if(!evaluacion.getNumSerie().equals(valor)){
							esValida = false;
						}
					}else if (llave.equals("ART 140")){
						if(!valor.equals("CLIENTE (" + evaluacion.getIdCliente() + ")")){
							esValida = false;
						}
					}else if (llave.equals("CONDICION ESPECIAL A NIVEL COTIZACION")){
						String condicionesEspeciales = obtenerCondicionesEspecialesCotizacion(evaluacion);
						if(!condicionesEspeciales.equals(valor)){
							esValida = false;
						}
					}else if (llave.equals("CONDICION ESPECIAL A NIVEL INCISO")){
						String condicionesEspeciales = obtenerCondicionesEspecialesInciso(evaluacion);
						if(!condicionesEspeciales.equals(valor)){
							esValida = false;
						}
					}					
				}
			}
			
		}
		return esValida;
	}
	
	public String obtenerCondicionesEspecialesInciso(
			ExcepcionCotizacionEvaluacionAutoDTO evaluacion) {
		StringBuilder condicionesEspeciales = new StringBuilder("");
		boolean isFirst = true;
		for(CondicionEspecial item: evaluacion.getCondicionesInciso()){
			if(isFirst){
				isFirst = false;
			}else{
				condicionesEspeciales.append("-");
			}
			condicionesEspeciales.append(item.getNombre());
		}	
		return condicionesEspeciales.toString();
	}

	public String obtenerCondicionesEspecialesCotizacion(
			ExcepcionCotizacionEvaluacionAutoDTO evaluacion) {
		StringBuilder condicionesEspeciales = new StringBuilder("");
		boolean isFirst = true;
		for(CondicionEspecial item: evaluacion.getCondicionesCotizacion()){
			if(isFirst){
				isFirst = false;
			}else{
				condicionesEspeciales.append("-");
			}
			condicionesEspeciales.append(item.getNombre());
		}		
		return condicionesEspeciales.toString();
	}

	public class ConvertidorEstatus {
		
		private Integer status = null;
		
		private String descripcion = "";
		
		public Integer getStatus() {
			return status;
		}
		public void setStatus(Integer status) {
			this.status = status;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		
	}


}