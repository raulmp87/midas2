package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatAlertasRechazos;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatAlertasRechazosService;

@Stateless
public class CatAlertasRechazosServiceImpl implements CatAlertasRechazosService{
	private static final long serialVersionUID = 1L;
	
	private EntidadService entidadService;

	@Override
	public List<CatAlertasRechazos> findAll() {
		return entidadService.findAll(CatAlertasRechazos.class);
	}

	@Override
	public CatAlertasRechazos findById(long id) {
		CatAlertasRechazos retorno = new CatAlertasRechazos();
		if(id >= 0){
			retorno = entidadService.findById(CatAlertasRechazos.class, id);
		}
		return retorno;
	}

	@Override
	public List<CatAlertasRechazos> findByStatus(boolean status) {
		return entidadService.findByProperty(CatAlertasRechazos.class, "estatus", status?0:1);
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}