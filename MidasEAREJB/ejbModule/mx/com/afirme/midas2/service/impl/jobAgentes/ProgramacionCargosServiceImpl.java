package mx.com.afirme.midas2.service.impl.jobAgentes;

import java.util.List;
import java.util.logging.Level;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.service.cargos.DetalleCargosService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCargosService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class ProgramacionCargosServiceImpl implements ProgramacionCargosService{
	private DetalleCargosService detalleCargoService;
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProgramacionCargosServiceImpl.class);
	
	@Override
	public void executeTasks() {
	LOG.info("Ejecutando ProgramacionCargosService.executeTasks()...");
		try {
			LogDeMidasInterfaz.log("entrando a ProgramacionCargosServiceImpl.executeTasks... "+ this, Level.INFO, null);
			detalleCargoService.ejecutarCargo();
		} catch (Exception e) {
			e.printStackTrace();
			LogDeMidasInterfaz.log("error general en ProgramacionCargosServiceImpl.executeTasks... "+ this, Level.INFO, e);
		}
	}

	@Override
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		return null;
	}
	
	public void initialize() {
		String timerInfo = "TimerProgramacionCargos";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0/5 * * * ?
				expression.minute("0/5");
				expression.hour("*");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerProgramacionCargos", false));
				
				LOG.info("Tarea TimerProgramacionCargos configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerProgramacionCargos");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerProgramacionCargos:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		executeTasks();
	}
	
	/**
	 * ==================================================
	 * Sets and Gets
	 * ==================================================
	 */
	@EJB
	public void setDetalleCargoService(DetalleCargosService detalleCargoService) {
		this.detalleCargoService = detalleCargoService;
	}
}
