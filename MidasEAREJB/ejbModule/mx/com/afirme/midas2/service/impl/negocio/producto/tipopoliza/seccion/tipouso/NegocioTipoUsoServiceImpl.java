package mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion.tipouso;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.dto.negocio.seccion.tipouso.RelacionesNegocioTipoUsoDTO;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoService;

@Stateless
public class NegocioTipoUsoServiceImpl implements NegocioTipoUsoService {

	@Override
	public RelacionesNegocioTipoUsoDTO getRelationLists(NegocioSeccion negocioSeccion) {
		RelacionesNegocioTipoUsoDTO relacionesNegocioTipoUsoDTO = new RelacionesNegocioTipoUsoDTO();
		//Listado asociadas
		List<NegocioTipoUso> negocioTipoUsoAsociadasList = entidadDao.findByProperty(NegocioTipoUso.class, "negocioSeccion.idToNegSeccion", negocioSeccion.getIdToNegSeccion());
		//Listado posibles
//		List<TipoUsoVehiculoDTO> posibles =  tipoUsoVehiculoFacadeRemote.findByClaveTipoBien(negocioSeccion.getSeccionDTO().getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
		List<TipoUsoVehiculoDTO> posibles = tipoUsoVehiculoFacadeRemote.findByProperty("idTcTipoVehiculo", negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
		//Listado disponibles
		List<NegocioTipoUso> negocioTipoUsoDisponiblesList = new ArrayList<NegocioTipoUso>();
		
		List<TipoUsoVehiculoDTO> tipoUsoVehiculoDTOAsociadasList = new ArrayList<TipoUsoVehiculoDTO>();
		
		for(NegocioTipoUso item : negocioTipoUsoAsociadasList){
			tipoUsoVehiculoDTOAsociadasList.add(item.getTipoUsoVehiculoDTO());
		}
		NegocioTipoUso negocioTipoUso = null;
		for(TipoUsoVehiculoDTO item : posibles){
			if(!tipoUsoVehiculoDTOAsociadasList.contains(item)){
				negocioTipoUso = new NegocioTipoUso();
				negocioTipoUso.setTipoUsoVehiculoDTO(item);
				negocioTipoUso.setNegocioSeccion(negocioSeccion);
				negocioTipoUso.setClaveDefault(false);
				negocioTipoUsoDisponiblesList.add(negocioTipoUso);
			}
		}
		
		relacionesNegocioTipoUsoDTO.setAsociadas(negocioTipoUsoAsociadasList);
		relacionesNegocioTipoUsoDTO.setDisponibles(negocioTipoUsoDisponiblesList);
		return relacionesNegocioTipoUsoDTO;
	}

	@Override
	public void relacionarNegocioTipoUso(String accion, NegocioTipoUso negocioTipoUso) {
		
		//Persite negocio seccion
		if(accion.equals("deleted")){
			negocioTipoUso = entidadDao.findById(NegocioTipoUso.class, negocioTipoUso.getIdToNegTipoUso());
		}else{
			NegocioSeccion negocioSeccion = entidadDao.findById(NegocioSeccion.class, negocioTipoUso.getNegocioSeccion().getIdToNegSeccion());
			negocioTipoUso.setNegocioSeccion(negocioSeccion);
			TipoUsoVehiculoDTO tipoUsoVehiculoDTO = tipoUsoVehiculoFacadeRemote.findById(negocioTipoUso.getTipoUsoVehiculoDTO().getIdTcTipoUsoVehiculo());
			negocioTipoUso.setTipoUsoVehiculoDTO(tipoUsoVehiculoDTO);
			negocioTipoUso.setClaveDefault(negocioTipoUso.getClaveDefault());
		//Si no existen mas negocioTipoUsoAsociedados es default
		List<NegocioTipoUso> negocioTipoUsoList = entidadDao.findByProperty(NegocioTipoUso.class, "negocioSeccion.idToNegSeccion", negocioTipoUso.getNegocioSeccion().getIdToNegSeccion());
		if(negocioTipoUsoList.size()==0)negocioTipoUso.setClaveDefault(true);
		
		}
		entidadDao.executeActionGrid(accion, negocioTipoUso);

	}
	
	private EntidadDao entidadDao;
	private TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote;
	private NegocioTipoUsoDao negocioTipoUsoDao;
	
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	
	@EJB
	public void setTipoUsoVehiculoFacadeRemote(TipoUsoVehiculoFacadeRemote tipoUsoVehiculoFacadeRemote) {
		this.tipoUsoVehiculoFacadeRemote = tipoUsoVehiculoFacadeRemote;
	}
	
	@EJB
	public void setNegocioTipoUsoDao(NegocioTipoUsoDao negocioTipoUsoDao) {
		this.negocioTipoUsoDao = negocioTipoUsoDao;
	}

	@Override
	public NegocioTipoUso buscarDefault(
			NegocioSeccion negocioSeccion) {
		return negocioTipoUsoDao.buscarDefault(negocioSeccion);
	}

}
