package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.RepSaldoVSContDao;
import mx.com.afirme.midas2.dto.reportesAgente.RepMizarSelectMizar;
import mx.com.afirme.midas2.dto.reportesAgente.SaldosVsContabilidadView;
import mx.com.afirme.midas2.service.fuerzaventa.RepSaldoVSContService;

@Stateless
public class RepSaldoVSContServiceImpl implements RepSaldoVSContService {

	private RepSaldoVSContDao dao;
	@EJB
	public void setDao(RepSaldoVSContDao dao) {
		this.dao = dao;
	}

	@Override
	public void deleteTPSaldosVsContabilidad() throws Exception {
		dao.deleteTPSaldosVsContabilidad();
	}

	@Override
	public void insertTPSaldosVsContabilidad(List<RepMizarSelectMizar> arg0)
			throws Exception {
		dao.insertTPSaldosVsContabilidad(arg0);
	}

	@Override
	public List<RepMizarSelectMizar> consultaMizar(String anioMes, String FechaString)
			throws Exception {
		return dao.consultaMizar(anioMes, FechaString);
	}

	@Override
	public List<SaldosVsContabilidadView> executeSpSaldosVSContabilidad(String pfechaCorte) throws Exception {
		return dao.executeSpSaldosVSContabilidad(pfechaCorte);
		
	}

}
