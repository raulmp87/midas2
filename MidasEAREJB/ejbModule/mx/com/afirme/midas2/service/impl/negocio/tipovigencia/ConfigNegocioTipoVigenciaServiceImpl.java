package mx.com.afirme.midas2.service.impl.negocio.tipovigencia;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Local;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.tipovigencia.NegocioTipoVigencia;
import mx.com.afirme.midas2.domain.negocio.tipovigencia.TipoVigencia;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.tipovigencia.ConfigNegocioTipoVigenciaService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

/**
 * Session Bean implementation class ConfigNegocioTipoVigenciaServiceImpl
 */
@Stateless
public class ConfigNegocioTipoVigenciaServiceImpl implements ConfigNegocioTipoVigenciaService {

	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
    /**
     * Default constructor. 
     */
    public ConfigNegocioTipoVigenciaServiceImpl() {
    }

	/**
     * @see ConfigNegocioTipoVigenciaService#agregarTipoVigencia(TipoVigencia)
     */
    public void agregarTipoVigencia(TipoVigencia tipoVigencia) {    	
    	tipoVigencia.setUsuarioCreacionId(usuarioService.getUsuarioActual().getId());    	
    	entidadService.save(tipoVigencia);
    }

	/**
     * @see ConfigNegocioTipoVigenciaService#validarContraVigenciaProductosNegocio(TipoVigencia, Long)
     */
    public Boolean validarContraVigenciaProductosNegocio(TipoVigencia tipoVigencia, Long idNegocio) {
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("idNegocio", idNegocio);
    	params.put("tipoVigenciaDias", BigDecimal.valueOf(tipoVigencia.getDias()));
    	List<ProductoDTO> productosNegocio = entidadService.executeQueryMultipleResult("SELECT neg.productoDTO FROM NegocioProducto neg WHERE neg.negocio.idToNegocio =:idNegocio AND neg.productoDTO.valorMinimoUnidadVigencia > :tipoVigenciaDias OR neg.productoDTO.valorMaximoUnidadVigencia < :tipoVigenciaDias", params);
    	
    	if(productosNegocio.size() > 0){
    		return false;
    	} else {
    		return true;
    	}
    }

	/**
     * @see ConfigNegocioTipoVigenciaService#obtenerTiposVigenciaDisponibles(Long)
     */
    public List<NegocioTipoVigencia> obtenerTiposVigenciaDisponibles(Long idNegocio) {
    	
    	List<NegocioTipoVigencia> listaTipoVigencia = new ArrayList<NegocioTipoVigencia>();
    	
    	@SuppressWarnings("unchecked")
		List<TipoVigencia> listadoTipoVigencias = entidadService.executeQueryMultipleResult("SELECT tv FROM TipoVigencia tv ORDER BY tv.dias ASC", null);
    	
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("negocio.idToNegocio", idNegocio);
    	
    	List<NegocioTipoVigencia> listadoVigenciasNegocio = entidadService.findByProperties(NegocioTipoVigencia.class, params);
    	Negocio negocio = entidadService.findById(Negocio.class, idNegocio);
    	obtenerTiposVigenciaDisponibles(listadoTipoVigencias, listadoVigenciasNegocio, listaTipoVigencia, negocio);
    	
    	return listaTipoVigencia;
    }

    private final void obtenerTiposVigenciaDisponibles(List<TipoVigencia> tiposVigencia, List<NegocioTipoVigencia> negocioVigencias, List<NegocioTipoVigencia> vigenciasDisponibles, Negocio negocio){
    	
    	int indiceVigenciaEncontrada = 0;
    	boolean vigenciaIncluida = false;
    	
    	for(TipoVigencia tipoVigencia: tiposVigencia){
    		vigenciaIncluida = false;
    		
    		for(int i = 0; i < negocioVigencias.size(); i++){
    			NegocioTipoVigencia negocioTipoVigencia = negocioVigencias.get(i);
    			
    			if(negocioTipoVigencia.getTipoVigencia().equals(tipoVigencia)){
    				vigenciaIncluida = true;
    				indiceVigenciaEncontrada = i;
    				break;
    			}
    		}
    		
    		if(!vigenciaIncluida){
    			NegocioTipoVigencia ntv = new NegocioTipoVigencia();
    			ntv.setNegocio(negocio);
    			ntv.setTipoVigencia(tipoVigencia);
    			vigenciasDisponibles.add(ntv);
    		} else {
    			negocioVigencias.remove(indiceVigenciaEncontrada);
    		}
    	}
    }
    
	/**
     * @see ConfigNegocioTipoVigenciaService#eliminarTipoVigencia(Long)
     */
    public Boolean eliminarTipoVigencia(Long id) {
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("tipoVigencia.id", id);
    	if(entidadService.findByProperties(NegocioTipoVigencia.class, params).size() == 0){
    		TipoVigencia vigencia = entidadService.findById(TipoVigencia.class, id);    	
        	entidadService.remove(vigencia);
        	return true;
    	} else {
    		return false;
    	}    
    }

	/**
     * @see ConfigNegocioTipoVigenciaService#validarParametroFechaFinVigenciaFija(Long)
     */
    public Boolean validarParametroFechaFinVigenciaFija(Long idNegocio) {
    	Negocio negocio = entidadService.findById(Negocio.class, idNegocio);
		if(negocio.getFechaFinVigenciaFija() == null){
			return true;
		} else {
			return false;
		}
    }
    
    

	/**
     * @see ConfigNegocioTipoVigenciaService#setTipoVigenciaDefaultNegocio(Long, Long)
     */
    public void setTipoVigenciaDefaultNegocio(Long idNegocioTipoVigencia, Long idNegocio) {	
    	NegocioTipoVigencia negocioTipoVigenciaNuevaDefault = entidadService.findById(NegocioTipoVigencia.class, idNegocioTipoVigencia);
    	negocioTipoVigenciaNuevaDefault.setEsDefault(1);
    
    	entidadService.save(negocioTipoVigenciaNuevaDefault);
    	
    }

	/**
     * @see ConfigNegocioTipoVigenciaService#obtenerTiposVigenciaAsociados(Long)
     */
    public List<NegocioTipoVigencia> obtenerTiposVigenciaAsociados(Long idNegocio) {
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("idNegocio", idNegocio);
    	@SuppressWarnings("unchecked")
		List<NegocioTipoVigencia> vigenciasAsociadas = entidadService.executeQueryMultipleResult("SELECT neg FROM NegocioTipoVigencia neg WHERE neg.negocio.idToNegocio =:idNegocio ORDER BY neg.tipoVigencia.id ASC", params);
    	
    	return vigenciasAsociadas;			
    }

	/**
     * @see ConfigNegocioTipoVigenciaService#asignarANegocio(Long, Long)
     */
    public void asignarANegocio(Long idNegocio, Long idTipoVigencia) {
    	TipoVigencia vigencia = entidadService.findById(TipoVigencia.class, idTipoVigencia);
    	
    	asignarVigenciaANegocio(vigencia, idNegocio);
    }

    private void asignarVigenciaANegocio(TipoVigencia vigencia, Long idNegocio){
    	Negocio negocio = entidadService.findById(Negocio.class, idNegocio);
    	
    	NegocioTipoVigencia negocioTipoVigencia = new NegocioTipoVigencia();
    	
    	negocioTipoVigencia.setNegocio(negocio);
    	negocioTipoVigencia.setTipoVigencia(vigencia);
    	negocioTipoVigencia.setEsDefault(0);
    	entidadService.save(negocioTipoVigencia);
    }
    
	/**
     * @see ConfigNegocioTipoVigenciaService#quitarANegocio(Long, Long)
     */
    public void quitarANegocio(Long idNegocioTipoVigencia, Long idNegocio) {
    	Map<String, Object> params = new HashMap<String, Object>();
    	params.put("negocio.idToNegocio", idNegocio);
    	params.put("id", idNegocioTipoVigencia);
    	
    	NegocioTipoVigencia negocioTipovigencia = entidadService.findByProperties(NegocioTipoVigencia.class, params).get(0);
    	
    	entidadService.remove(negocioTipovigencia);
    }

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.service.negocio.tipovigencia.ConfigNegocioTipoVigenciaService#obtenerTiposVigencia()
	 */
	@Override
	public List<TipoVigencia> obtenerTiposVigencia() {
		return entidadService.findAll(TipoVigencia.class);
	}
	
	@Override
	public boolean asociarTodos(Long idNegocio){
		boolean vigenciasFueraDeRango = false;
		List<NegocioTipoVigencia> vigenciasDisponibles = obtenerTiposVigenciaDisponibles(idNegocio);
		
		for(int i = 0; i < vigenciasDisponibles.size(); i++){
			NegocioTipoVigencia negocioTipoVigencia = vigenciasDisponibles.get(i);
			if(!validarContraVigenciaProductosNegocio(negocioTipoVigencia.getTipoVigencia(), idNegocio)){
				vigenciasDisponibles.remove(i);
				i--;
				vigenciasFueraDeRango = true;
			}
		}
		
		entidadService.saveAll(vigenciasDisponibles);
		
		if(vigenciasDisponibles.size() > 0 && existeVigenciaDefaultNegocio(idNegocio) == null){			
			List<NegocioTipoVigencia> vigenciasAsociadas = obtenerTiposVigenciaAsociados(idNegocio);
			if(vigenciasAsociadas.size() > 0){
				setTipoVigenciaDefaultNegocio(vigenciasAsociadas.get(0).getId(), idNegocio);
			}			
		}
		return vigenciasFueraDeRango;
	}
	
	@Override
	public void desasociarTodos(Long idNegocio){
		Map<String, Object> params = new HashMap<String, Object>();
    	params.put("negocio.idToNegocio", idNegocio);
		List<NegocioTipoVigencia> vigenciasAsociadas= entidadService.findByProperties(NegocioTipoVigencia.class, params);
				
		entidadService.removeAll(vigenciasAsociadas);
	}
	
	@Override
	public boolean existeRegistroDiasVigencia(Integer dias){
		if(entidadService.findByProperty(TipoVigencia.class, "dias", dias).isEmpty()){
			return false;
		} else { 
			return true;
		}
	}
	
	
	public NegocioTipoVigencia existeVigenciaDefaultNegocio(Long idNegocio){
		NegocioTipoVigencia vigenciaDefault = null;
		Map<String, Object> params = new HashMap<String, Object>();
    	params.put("negocio.idToNegocio", idNegocio);
    	params.put("esDefault", 1);
    	
    	List<NegocioTipoVigencia> negocioTipoVigenciaDefault = entidadService.findByProperties(NegocioTipoVigencia.class, params);
    	if(negocioTipoVigenciaDefault.size() > 0){
    		vigenciaDefault = negocioTipoVigenciaDefault.get(0);
    	}    	
    	
		return vigenciaDefault;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.service.negocio.tipovigencia.ConfigNegocioTipoVigenciaService#cambiarVigenciaDefault(java.lang.Long, java.lang.Long)
	 */
	@Override
	public void cambiarVigenciaDefault(Long idNegocioTipoVigencia, Long idNegocio) {
		NegocioTipoVigencia negocioVigenciaDefault = existeVigenciaDefaultNegocio(idNegocio);
		if(negocioVigenciaDefault != null){
			negocioVigenciaDefault.setEsDefault(0);
			entidadService.save(negocioVigenciaDefault);
			setTipoVigenciaDefaultNegocio(idNegocioTipoVigencia, idNegocio);
		}		
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.service.negocio.tipovigencia.ConfigNegocioTipoVigenciaService#relacionarTipoVigencia(java.lang.String, mx.com.afirme.midas2.domain.negocio.tipovigencia.NegocioTipoVigencia)
	 */
	@Override
	public void relacionarTipoVigencia(String accion, NegocioTipoVigencia negocioTipoVigencia) {	
		if(accion.equals("inserted") && existeVigenciaDefaultNegocio(negocioTipoVigencia.getNegocio().getIdToNegocio()) == null){
			negocioTipoVigencia.setEsDefault(1);	
		}
		entidadService.executeActionGrid(accion, negocioTipoVigencia);
	}

}
