package mx.com.afirme.midas2.service.impl.emisionFacturas;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.rmi.RemoteException;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.emisionFacturas.EmisionFacturaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.emisionFacturas.EmisionFacturaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFactura;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFacturaHistorico;
import mx.com.afirme.midas2.domain.emisionFacturas.EmisionFacturaHistorico.EstatusEmisionFactCancelacion;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaDireccionMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumModoEnvio;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCruceroJuridica;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.emisionFactura.ConceptoFacturaDTO;
import mx.com.afirme.midas2.dto.emisionFactura.FacturaResultadoDTO;
import mx.com.afirme.midas2.dto.emisionFactura.FiltroFacturaDTO;
import mx.com.afirme.midas2.dto.emisionFactura.IngresoFacturableDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosContratanteDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import com.afirme.insurance.services.PrintReport.PrintReport;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EmailDestinaratios;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import mx.com.afirme.midas2.utils.NumeroALetra;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Conceptos.Concepto;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperReport;

import org.apache.log4j.Logger;
import org.w3c.dom.Document;

@Stateless
public class EmisionFacturaServiceImpl implements EmisionFacturaService{
	
	public  static final Logger log = Logger.getLogger(EmisionFacturaServiceImpl.class);
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private PolizaSiniestroService polizaSiniestroService;
	
	@EJB
    private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	BitacoraService bitacoraService;
	
	@EJB 
	private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	@EJB
    private EmisionFacturaDao emisionFacturaDao;
	
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	
	@EJB
	EmisionFacturaService emisionFacturaService;
	
	@Resource
	protected javax.ejb.SessionContext sessionContext;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private EnvioNotificacionesService envioNotificacionesService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private RecepcionFacturaService recepcionFacturaService;
	
	@EJB
	private ImpresionRecibosService impresionRecibosService;
	
	@EJB
	private DireccionMidasService direccionService;
	
	@EJB
	private ColoniaFacadeRemote coloniaFacadeRemote;
	
	@Resource	
	private TimerService timerService;
	
	private static String OTRA_PERSONA = "OTRAPER";
	
	private Map<String,String> listaTipoRecuperacion = null; 
	
	private static final String PLANTILLA_FACTURA_ELECTRONICA = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/FacturaElectronica.jrxml";
	
	private static final String PLANTILLA_CONCEPTO_FACTURA_ELECTRONICA = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ConceptoFactura.jrxml";
	
	public String actualizaDatosFiscales(){
		return null;
	}
	
	@Override
	public void actualizarDatosEmisionFactura(EmisionFactura emisionFactura){
		
		String tipoRecuperacion = "";
		EmisionFactura emisionFacturaUpdate = this.entidadService.findById(EmisionFactura.class, emisionFactura.getId());
		
		if( emisionFacturaUpdate != null ){
			
			tipoRecuperacion = emisionFacturaUpdate.getIngreso().getRecuperacion().getTipo();
			
			// DATOS GENERICOS A ACTUALIZAR PARA TODAS LAS RECUPERACIONES
			emisionFacturaUpdate.setMetodoPago(emisionFactura.getMetodoPago());
			emisionFacturaUpdate.setDigitos(emisionFactura.getDigitos());
			emisionFacturaUpdate.setObservacionesFacturacion(emisionFactura.getObservacionesFacturacion());
			
			if( tipoRecuperacion.equals("DED") || tipoRecuperacion.equals("CRU")) {
				emisionFacturaUpdate.setNombreRazonSocial(emisionFactura.getNombreRazonSocial());
				emisionFacturaUpdate.setRfc   (emisionFactura.getRfc());
				emisionFacturaUpdate.setCalle (emisionFactura.getCalle());
				emisionFacturaUpdate.setNumero(emisionFactura.getNumero());
				emisionFacturaUpdate.setEstadoMidas(this.entidadService.findById(EstadoMidas.class, emisionFactura.getEstadoMidas().getId() ));
				emisionFacturaUpdate.setCiudadMidas(this.entidadService.findById(CiudadMidas.class, emisionFactura.getCiudadMidas().getId() ));
				emisionFacturaUpdate.setColoniaMidas(this.entidadService.findById(ColoniaMidas.class, emisionFactura.getColoniaMidas().getId()));
				emisionFacturaUpdate.setCp      (emisionFactura.getCp());
				emisionFacturaUpdate.setTelefono(emisionFactura.getTelefono());
				emisionFacturaUpdate.setEmail   (emisionFactura.getEmail());
				emisionFacturaUpdate.setTipoPersona(emisionFactura.getTipoPersona());
			}
			
			this.entidadService.save(emisionFacturaUpdate);
			
		}
	}
	
	private String getConceptoFactura(Recuperacion recuperacion){
		Map<String,String> listaConceptoFactura = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.EMISION_FACTURA_CONCEPTO);
		String concepto = listaConceptoFactura.get(recuperacion.getTipo());

		String numeroSiniestro = recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro();
		String numeroPoliza = String.valueOf(recuperacion.getReporteCabina().getPoliza().getNumeroPoliza());
		concepto = concepto.replace("[SINIESTRO]", numeroSiniestro);
		concepto = concepto.replace("[POLIZA]", numeroPoliza);
		
		AutoIncisoReporteCabina autoIncisoReporteCabina = recuperacion.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
		if(autoIncisoReporteCabina != null){
			if(autoIncisoReporteCabina.getDescMarca() != null)
				concepto = concepto.replace("[MARCA]", autoIncisoReporteCabina.getDescMarca());
			if(autoIncisoReporteCabina.getDescTipoUso() != null)
				concepto = concepto.replace("[TIPO]", autoIncisoReporteCabina.getDescTipoUso());
			if(autoIncisoReporteCabina.getModeloVehiculo() != null)
				concepto = concepto.replace("[MODELO]", String.valueOf(autoIncisoReporteCabina.getModeloVehiculo()));
			if(autoIncisoReporteCabina.getNumeroSerie() != null)
				concepto = concepto.replace("[SERIE]",autoIncisoReporteCabina.getNumeroSerie());
			if(autoIncisoReporteCabina.getNumeroMotor() != null)
				concepto = concepto.replace("[MOTOR]",autoIncisoReporteCabina.getNumeroMotor());
			if(autoIncisoReporteCabina.getColor() != null)
				concepto = concepto.replace("[COLOR]",autoIncisoReporteCabina.getColor());
		}
		return concepto;
	}
	
	private Map<String,Object> obtenerDatosImportes(Long recuperacionId){
		Map<String, Object> resultado = new HashMap<String, Object>();
		Recuperacion recuperacion = entidadService.findById(Recuperacion.class, recuperacionId);
		resultado.put("CONCEPTO",getConceptoFactura(recuperacion));
		if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.COMPANIA.getValue())){
			RecuperacionCompania recuperacionCompania = entidadService.findById(RecuperacionCompania.class, recuperacionId);
			resultado.put("IMPORTE", recuperacionCompania.getImporte());
			resultado.put("TOTAL", recuperacionCompania.getImporte());
			return resultado;
		}
		if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.getValue())){
			RecuperacionCruceroJuridica recuperacionCrucero = entidadService.findById(RecuperacionCruceroJuridica.class, recuperacionId);
			resultado.put("IMPORTE", recuperacionCrucero.getMontoFinal());
			resultado.put("TOTAL", recuperacionCrucero.getMontoFinal());
			return resultado;
		}
		if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.DEDUCIBLE.getValue())){
			RecuperacionDeducible recuperacionDeducible = entidadService.findById(RecuperacionDeducible.class, recuperacionId);
			resultado.put("IMPORTE", recuperacionDeducible.getMontoFinalDeducible());
			resultado.put("TOTAL", recuperacionDeducible.getMontoFinalDeducible());
			return resultado;
		}
		if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.PROVEEDOR.getValue())){
			RecuperacionProveedor recuperacionProveedor = entidadService.findById(RecuperacionProveedor.class, recuperacionId);
			if(recuperacionProveedor.getMedio().equals(Recuperacion.MedioRecuperacion.VENTA.toString())){
				resultado.put("IMPORTE", recuperacionProveedor.getSubTotalVenta());
				resultado.put("IVA", recuperacionProveedor.getIvaVenta());
				resultado.put("TOTAL", recuperacionProveedor.getMontoVenta());
			}else{
				resultado.put("IMPORTE", recuperacionProveedor.getSubTotal());
				resultado.put("IVA", recuperacionProveedor.getIva());
				resultado.put("TOTAL", recuperacionProveedor.getMontoTotal());
			}
			return resultado;
		}
		if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue())){
			RecuperacionSalvamento recuperacionSalvamento = entidadService.findById(RecuperacionSalvamento.class, recuperacionId);
			resultado.put("IMPORTE", recuperacionSalvamento.obtenerVentaActiva().getSubtotal());
			resultado.put("IVA", recuperacionSalvamento.obtenerVentaActiva().getIva());
			resultado.put("TOTAL", recuperacionSalvamento.obtenerVentaActiva().getTotalVenta());
			return resultado;
			
		}
		return null;
	}
	
	@Override
	public EmisionFactura obtenerEmisionFactura(Long emisionFacturaId){
		
		EmisionFactura emisionFactura = entidadService.findById(EmisionFactura.class, emisionFacturaId);

		if( emisionFactura != null && emisionFactura.getIngreso().getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue()) ){
			this.llenarDatosVehiculoSalvamento(emisionFactura);
		}
		
		return emisionFactura;
	}
	
	
	public EmisionFactura obtenerDatosImportesPorIngreso(Long ingresoId){
		EmisionFactura emisionFactura = new EmisionFactura();
		Ingreso ingreso = entidadService.findById(Ingreso.class, ingresoId);
		Map<String,Object> importe = obtenerDatosImportes(ingreso.getRecuperacion().getId());
		emisionFactura.setImporte((BigDecimal) importe.get("IMPORTE"));
		emisionFactura.setIva((BigDecimal) importe.get("IVA"));
		emisionFactura.setTotalFactura((BigDecimal) importe.get("TOTAL"));
		emisionFactura.setDescripcionFacturacion(importe.get("CONCEPTO").toString());
		return emisionFactura;
	}
	
	public EmisionFactura obtenerDatosImportesPorIngreso(Long ingresoId, EmisionFactura emisionFactura){
		Ingreso ingreso = entidadService.findById(Ingreso.class, ingresoId);
		Map<String,Object> importe = obtenerDatosImportes(ingreso.getRecuperacion().getId());
		emisionFactura.setImporte((BigDecimal) importe.get("IMPORTE"));
		emisionFactura.setIva((BigDecimal) importe.get("IVA"));
		emisionFactura.setTotalFactura((BigDecimal) importe.get("TOTAL"));
		emisionFactura.setDescripcionFacturacion(importe.get("CONCEPTO").toString());
		return emisionFactura;
	}
	
	public EmisionFactura obtenerEmisionFacturaByIngresoId(Long ingresoId){
		List<EmisionFactura> lista = entidadService.findByProperty(EmisionFactura.class, "ingreso.id", ingresoId);
		if(lista != null && lista.size() > 0){
			
			EmisionFactura emisionFactura = lista.get(0);
			
			if( emisionFactura != null && emisionFactura.getIngreso().getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue()) ){
				this.llenarDatosVehiculoSalvamento(emisionFactura);
			}
			return emisionFactura;
		}
		return null;
	}
	
	public Ingreso getIngresoById(Long ingresoId){
		return entidadService.findById(Ingreso.class, ingresoId);
	}
	
	public EmisionFactura obtenerInformacionAsegurado(Long ingresoId){
		
		BigDecimal personaContratanteId = new BigDecimal(0);
		
		listaTipoRecuperacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_RECUPERACION);
		EmisionFactura emisionFactura = new EmisionFactura();
		Ingreso ingreso = entidadService.findById(Ingreso.class, ingresoId);
		Recuperacion recuperacion = ingreso.getRecuperacion();
		if( recuperacion.getReporteCabina().getPersonaContratanteId() != null ){
			personaContratanteId = new BigDecimal(recuperacion.getReporteCabina().getPersonaContratanteId());
		}
		DatosContratanteDTO datosContratanteDTO = polizaSiniestroService.obtenerDatosContratante(personaContratanteId);
		emisionFactura.setNombreRazonSocial(datosContratanteDTO.getNombreContratante());
		emisionFactura.setRfc(datosContratanteDTO.getRfcFiscal());
		emisionFactura.setTipoPersona(EmisionFactura.TipoPersona.ASEGURADO.getValue());
		emisionFactura.setCalle(datosContratanteDTO.getCalleNumeroFiscal());		
		emisionFactura.setConcepto(listaTipoRecuperacion.get(ingreso.getRecuperacion().getTipo()));
		ClienteGenericoDTO clienteGenericoDTO = new ClienteGenericoDTO();   
        clienteGenericoDTO.setIdCliente(personaContratanteId);
        
        try {
			clienteGenericoDTO = clienteFacadeRemote.loadById(clienteGenericoDTO);
			emisionFactura.setTelefono(clienteGenericoDTO.getTelefono());
			emisionFactura.setEmail(clienteGenericoDTO.getCorreoElectronicoAviso());
			
			if( clienteGenericoDTO.getIdEstadoFiscal() != null) {
				emisionFactura.setEstado(clienteGenericoDTO.getIdEstadoFiscal());
				emisionFactura.setEstadoMidas( this.entidadService.findById(EstadoMidas.class,clienteGenericoDTO.getIdEstadoFiscal() ));
			}
			
			if( clienteGenericoDTO.getIdMunicipioFiscal() != null ){
				emisionFactura.setCiudad(clienteGenericoDTO.getIdMunicipioFiscal());
				emisionFactura.setCiudadMidas(this.entidadService.findById(CiudadMidas.class, clienteGenericoDTO.getIdMunicipioFiscal()));
			}
			
			if( clienteGenericoDTO.getNombreColoniaFiscal() != null ) {
				
				List<ColoniaDTO> lColonia = coloniaFacadeRemote.findByColonyName( new BigDecimal(clienteGenericoDTO.getIdMunicipioFiscal()), clienteGenericoDTO.getNombreColoniaFiscal());
				if( !lColonia.isEmpty() ) {
					emisionFactura.setColonia( lColonia.get(0).getColonyId() );
					emisionFactura.setColoniaMidas(this.entidadService.findById(ColoniaMidas.class, lColonia.get(0).getColonyId() ));
				}
				
			}
			
			emisionFactura.setCp(clienteGenericoDTO.getCodigoPostalFiscal());		
		} catch (Exception e) {
			e.printStackTrace();
		}
		Map<String,Object> importe = obtenerDatosImportes(recuperacion.getId());
		emisionFactura.setImporte((BigDecimal) importe.get("IMPORTE"));
		emisionFactura.setIva((BigDecimal) importe.get("IVA"));
		emisionFactura.setTotalFactura((BigDecimal) importe.get("TOTAL"));
		emisionFactura.setDescripcionFacturacion(importe.get("CONCEPTO").toString());
		return emisionFactura;
	}
	
	public EmisionFactura obtenerInformacionProveedor(Long ingresoId){
		EmisionFactura emisionFactura = new EmisionFactura();
		Ingreso ingreso = entidadService.findById(Ingreso.class, ingresoId);
		PrestadorServicio prestadorServicio = null;
		Recuperacion recuperacion = ingreso.getRecuperacion();
		if(ingreso.getRecuperacion() instanceof  RecuperacionCompania){
			prestadorServicio = ((RecuperacionCompania)ingreso.getRecuperacion()).getCompania();
			emisionFactura.setConcepto(recuperacion.getTipo());
		}
		if(ingreso.getRecuperacion() instanceof  RecuperacionSalvamento){
			prestadorServicio = ((RecuperacionSalvamento)ingreso.getRecuperacion()).obtenerVentaActiva().getComprador();
			emisionFactura.setConcepto(recuperacion.getTipo());
		}
		if(ingreso.getRecuperacion() instanceof  RecuperacionProveedor){
			prestadorServicio = ((RecuperacionProveedor)ingreso.getRecuperacion()).getComprador();
			emisionFactura.setConcepto(recuperacion.getTipo());
			
		}
		if(prestadorServicio != null){
			emisionFactura.setNombreRazonSocial(prestadorServicio.getNombrePersona());
			emisionFactura.setRfc(prestadorServicio.getPersonaMidas().getRfc());
			PersonaDireccionMidas personaDireccionMidas = null;
			if( prestadorServicio.getPersonaMidas().getPersonaDireccion() != null && !prestadorServicio.getPersonaMidas().getPersonaDireccion().isEmpty() ){
				personaDireccionMidas = prestadorServicio.getPersonaMidas().getPersonaDireccion().get(0);
				
				emisionFactura.setColonia(personaDireccionMidas.getDireccion().getColonia().getId() );
				emisionFactura.setColoniaMidas(personaDireccionMidas.getDireccion().getColonia());
				
				String cp = personaDireccionMidas.getDireccion().getCodigoPostalColonia();
				CiudadMidas ciudadMidas = direccionService.obtenerCiudadPorCP(cp);
				emisionFactura.setCiudad(ciudadMidas.getId());
				emisionFactura.setCiudadMidas(ciudadMidas);
				
				emisionFactura.setEstado(ciudadMidas.getEstado().getId() );
				emisionFactura.setEstadoMidas(ciudadMidas.getEstado());
				
				emisionFactura.setCalle( personaDireccionMidas.getDireccion().getCalle() );
				emisionFactura.setCp( personaDireccionMidas.getDireccion().getCodigoPostalColonia() );
			}
			emisionFactura.setTelefono(prestadorServicio.getPersonaMidas().getContacto().getTelOficina());
			emisionFactura.setEmail(prestadorServicio.getPersonaMidas().getContacto().getCorreoPrincipal());
			
		}
		Map<String,Object> importe = obtenerDatosImportes(recuperacion.getId());
		emisionFactura.setImporte((BigDecimal) importe.get("IMPORTE"));
		emisionFactura.setIva((BigDecimal) importe.get("IVA"));
		emisionFactura.setTotalFactura((BigDecimal) importe.get("TOTAL"));
		emisionFactura.setDescripcionFacturacion(importe.get("CONCEPTO").toString());
		return emisionFactura;
	}
	
	@Override
	public EmisionFactura obtenerInformacionFactura(Long emisionFacturaId){
		EmisionFactura emisionFactura = entidadService.findById(EmisionFactura.class, emisionFacturaId);
		
		if( emisionFactura != null ){
			
			if( emisionFactura != null && emisionFactura.getIngreso().getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue()) ){
				this.llenarDatosVehiculoSalvamento(emisionFactura);
			}
		}
		
		return emisionFactura;
	}
	
	public List<String> guardarInformacionFactura(EmisionFactura emisionFactura){
		List<String> errores = validarInfomacionAGuardar(emisionFactura);
		if(errores != null && errores.size() == 0){
			EVENTO evento;
			String mensajeBitacora = "";
			if(emisionFactura.getId() == null || emisionFactura.getId().equals("")){
				evento = EVENTO.CREAR_EMISION_FACTURA;
				mensajeBitacora = "Se crea la emisión de la factura con id: [ID] en estatus TRAMITE";
			}else{
				evento = EVENTO.ACTUALIZAR_EMISION_FACTURA;
				mensajeBitacora = "Se actualiza la emisión de la factura con id: [ID]";
			}
			emisionFactura.setSiniestroCabina((entidadService.findById(Ingreso.class, emisionFactura.getIngreso().getId())).getRecuperacion().getReporteCabina().getSiniestroCabina());
			Long id = (Long) entidadService.saveAndGetId(emisionFactura);
			errores.add("ID "+String.valueOf(id));
			mensajeBitacora = mensajeBitacora.replace("[ID]", String.valueOf(id));
			bitacoraService.registrar(TIPO_BITACORA.EMISION_FACTURA, evento, String.valueOf(id), mensajeBitacora, 
					"SINIESTRO: "+emisionFactura.getSiniestroCabina().getId(), emisionFactura.getCodigoUsuarioCreacion() != null ? emisionFactura.getCodigoUsuarioCreacion() : "ADMIN");
			return errores;
		}
		return errores;
	}
	
	public List<IngresoFacturableDTO> obtenerIngresosFacturables(String filtro){
		List<IngresoFacturableDTO> resultado = new ArrayList<IngresoFacturableDTO>();
		listaTipoRecuperacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_RECUPERACION);
		Map<String,String> listaEstatus = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS_INGRESO);
		Map<String,String> listaEstatusFactura = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.EMISION_FACTURA_ESTATUS);
		Map<String, Object> params = new HashMap<String, Object>();
		for (String id : filtro.split(",")){
			Ingreso ingreso = entidadService.findById(Ingreso.class, Long.valueOf(id.trim()));
			IngresoFacturableDTO dto = new IngresoFacturableDTO();
			dto.setIngresoId(ingreso.getId());
			dto.setNumeroIngreso(ingreso.getNumero());
			dto.setTipoRecuperacion(ingreso.getRecuperacion().getTipo());
			dto.setTipoRecuperacionDesc(listaTipoRecuperacion.get(ingreso.getRecuperacion().getTipo()));
			dto.setEstatusIngreso(ingreso.getEstatus());
			dto.setEstatusIngresoDesc(listaEstatus.get(ingreso.getEstatus()));
			params.clear();
			params.put("ingreso.id", ingreso.getId());
			List<EmisionFactura> emisionFacturas = entidadService.findByProperties(EmisionFactura.class, params);
			if(emisionFacturas.size() > 0){
				EmisionFactura factura = emisionFacturas.get(0);
				dto.setEstatusFactura(listaEstatusFactura.get(factura.getEstatus()));
				dto.setDetalleRechazo(factura.getObservacionesFacturacion());
				dto.setEmisionFacturaId(factura.getId());
				dto.setFolioFactura(factura.getFolioFactura());
			}
			resultado.add(dto);
		}
		return resultado;
	}
	
	public Map<String,String> getListaTiposPersona(){
		return listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.EMISION_FACTURA_PERSONA);
	}
	
	public Map<String,String> getListaMetodosPago(){
		return listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.EMISION_FACTURA_PAGO);
	}

	public List<String> validarInfomacionAGuardar(EmisionFactura emisionFactura){
		List<String> resultado = new ArrayList<String>();
		if(emisionFactura == null){
			resultado.add("No se cuenta con la información necesaria para generar la(s) factura(s)");
		}else{
			if(emisionFactura.getTipoPersona() != null && 
					emisionFactura.getTipoPersona().equals("OTRAPER")){
				if(emisionFactura.getNombreRazonSocial() == null || emisionFactura.getNombreRazonSocial().trim().equals("")){
					resultado.add("No se proporcionó el nombre de la persona");
				}
				if(emisionFactura.getRfc() == null || emisionFactura.getRfc().trim().equals("")){
					resultado.add("No se proporcionó el rfc de la persona");
				}
				if(emisionFactura.getCalle() == null || emisionFactura.getCalle().trim().equals("")){
					resultado.add("No se proporcionó la calle de la persona");
				}
				if(emisionFactura.getColonia() == null ){
					resultado.add("No se proporcionó la colonia de la persona");
				}
				if(emisionFactura.getEstado() == null || emisionFactura.getEstado().trim().equals("")){
					resultado.add("No se proporcionó el estado de la persona");
				}
				if(emisionFactura.getCiudadMidas() == null ){
					resultado.add("No se proporcionó la ciudad de la persona");
				}
				if(emisionFactura.getCp() == null || emisionFactura.getCp().trim().equals("")){
					resultado.add("No se proporcionó el C.P. de la persona");
				}
				if(emisionFactura.getTelefono() == null || emisionFactura.getTelefono().trim().equals("")){
					resultado.add("No se proporcionó el teléfono de la persona");
				}
				if(emisionFactura.getEmail() == null || emisionFactura.getEmail().trim().equals("")){
					resultado.add("No se proporcionó el email de la persona");
				}
			}else{
				if(emisionFactura.getMetodoPago() == null || emisionFactura.getMetodoPago().trim().equals("")){
					resultado.add("No se proporcionó el metodo de pago");
				}
				if(!emisionFactura.getMetodoPago().equals("EFECT") && !emisionFactura.getMetodoPago().equals("NOIDENT"))
					if(emisionFactura.getDigitos() == null){
						resultado.add("No se proporcionó la cuenta");
					}
				if(emisionFactura.getObservacionesFacturacion() == null || emisionFactura.getObservacionesFacturacion().trim().equals("")){
					resultado.add("No se proporcionaron las observaciones de facturación");
				}
			}
			
		}
		return resultado;
	}
	
	@Override
	public List<FacturaResultadoDTO> buscarFacturas(FiltroFacturaDTO filtroFacturaDTO){
		List<FacturaResultadoDTO> resultado = new ArrayList<FacturaResultadoDTO>();
		resultado = emisionFacturaDao.buscarFacturas(filtroFacturaDTO);
		return resultado;
	}

	public void agregarDatosExtras(){
		
	}
	
	@Override
	public List<String> obtenerDetalleError(String folioFactura)
	{
		 List<String> listaDescripcionesErrores;
		
		 listaDescripcionesErrores = emisionFacturaDao.seEncuentraEnTablaDeError(folioFactura);
		 
		 return listaDescripcionesErrores;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EmisionFactura generarFactura(EmisionFactura emisionFactura)
	{	    
		emisionFacturaDao.insertarInformacion(emisionFactura);

		//refresh		
		emisionFactura = (entidadService.findByProperty(EmisionFactura.class,"id", emisionFactura.getId())).get(0);
		
		emisionFactura.setEstatus("PROCESADA");
		entidadService.save(emisionFactura);
		
		try{  
			bitacoraService.registrar(TIPO_BITACORA.EMISION_FACTURA, EVENTO.GENERAR_FACTURA, emisionFactura.getId().toString(), 
					 String.format("Se envia a facturar el ingreso No. [ %1$s ]", emisionFactura.getId().toString()), 
					 emisionFactura.toString(), usuarioService.getUsuarioActual().getNombreUsuario());
			
			emisionFacturaService.invocarProcesoWFactura(emisionFactura);	
			emisionFactura.setEstatus("FACT");
			this.enviarNotificacionEmisionFactura(emisionFactura);
			emisionFactura.setFechaGeneracion(new Date());
			emisionFactura.setCodigoUsuarioGeneracion(usuarioService.getUsuarioActual().getNombreUsuario());
			this.entidadService.save(emisionFactura);
			
		}catch(Exception e)
		{
			emisionFactura.setEstatus(EmisionFactura.EstatusFactura.RECHAZADA.getValue());
			entidadService.save(emisionFactura);

		}
		
		// GUARDA EN EL HISTORICO EL ESTATUS CON SE CREA LA FACTURA
		this.generaHistorico(emisionFactura, "", "");
		
		return emisionFactura;
	}
		
	@Override
	public String generarFacturaElectronica(String idsEmisionFactura)
	{        
		String[] listaIds = idsEmisionFactura.split(",");
		
		for(String idString:listaIds)
		{
			List<EmisionFactura> resultado = entidadService.findByProperty(EmisionFactura.class, "ingreso.id", Long.valueOf(idString));
			
			EmisionFactura emisionFactura = emisionFacturaService.generarFactura(resultado.get(0));//Se asume un solo registro
			if(emisionFactura.getEstatus().equalsIgnoreCase(EmisionFactura.EstatusFactura.RECHAZADA.getValue()))
			{
				throw new NegocioEJBExeption("", "Ha ocurrido un error al procesar el ingreso No." + emisionFactura.getIngreso().getNumero()
						+ " para mas detalle puede ir a la opci\u00F3n Detalle por Rechazo en el Listado de Ingresos a Facturar");
			}			
		}
		
		return null;
	}	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public String invocarProcesoWFactura(EmisionFactura emisionFactura)
	{
		PrintReport printReport = impresionRecibosService.getServicioImpresionED();		
		byte[] recibo;
		String reciboString = "";
				
			try {
				recibo = printReport.getDigitalBill(emisionFactura.getFolioFactura(),"xml");
						
				if(emisionFacturaDao.seGeneraFacturaConExito(emisionFactura))
				{
					if(recibo != null)
					{
						reciboString = new String(recibo);			
					}	
					
				}else
				{
					throw new NegocioEJBExeption("", " Ha ocurrido un error al procesar el ingreso No.  " 
							+ emisionFactura.getIngreso().getNumero());		
				}				
					
			} catch (RemoteException e) {
				e.printStackTrace();
				throw new NegocioEJBExeption("", " Ha ocurrido un error al intentar invocar el servicio de facturación para el ingreso No. " 
						+ emisionFactura.getIngreso().getNumero());	
			}		
		
		return reciboString; 
	}
	
	@Override
	public void enviarNotificacionEmisionFactura(EmisionFactura emisionFactura)
	{
		try {
			enviarNotificacionEmisionFactura(emisionFactura, null, Boolean.FALSE);
		}catch(Exception e) {
			log.error("Error EmisionFacturaService enviarNotificacionEmisionFactura: ",e);
		}
	}	
	
	public void enviarNotificacionEmisionFactura(EmisionFactura emisionFactura, EmailDestinaratios destinatariosAdicionales, Boolean esReenvio)
	{	
		if(esReenvio == null)
		{
			esReenvio = Boolean.FALSE;
		}
		
		Usuario usuarioActual = usuarioService.getUsuarioActual();
		   
		List<Usuario> usuarios = 
			usuarioService.buscarUsuariosSinRolesPorNombreRol(
					sistemaContext.getRolCoordinadorOperaciones(),
					usuarioActual.getNombreUsuario(), 
					usuarioActual.getIdSesionUsuario());
		
		EmailDestinaratios destinatarios;
		
		if(esReenvio && destinatariosAdicionales != null)
		{
			destinatarios = destinatariosAdicionales;
			
		}else
		{
			destinatarios = new EmailDestinaratios();
		}
				
		if(!esReenvio && emisionFactura.getEmail() != null)
		{
			destinatarios.agregar(EnumModoEnvio.PARA, emisionFactura.getEmail(), emisionFactura.getNombreRazonSocial());
		}
		else if(destinatarios.getPara().isEmpty())
		{
			throw new NegocioEJBExeption("", "No se encontró direccion del correo para" 
					+ emisionFactura.getIngreso().getNumero());	
		}	
		
		for(Usuario gerenteOperacionIndemnizaciones:usuarios)
		{
			destinatarios.agregar(EnumModoEnvio.CC, gerenteOperacionIndemnizaciones.getEmail(), gerenteOperacionIndemnizaciones.getNombreCompleto()); 
		}
		
		//Obtener adjuntos
		
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		
		ByteArrayAttachment adjunto1 = new ByteArrayAttachment();
		ByteArrayAttachment adjunto2 = new ByteArrayAttachment();
		PrintReport printReport = impresionRecibosService.getServicioImpresionED();	
		try {
			byte[] recibo = printReport.getDigitalBill(emisionFactura.getFolioFactura(),"xml");
			adjunto1 = new ByteArrayAttachment();
			adjunto1.setContenidoArchivo(recibo);
			adjunto1.setNombreArchivo("Factura_" + emisionFactura.getFolioFactura() + ".xml");
			adjunto1.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XML);
			adjuntos.add(adjunto1);
			
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		TransporteImpresionDTO pdf = this.obtenerPdf(emisionFactura.getId());		
		adjunto2 = new ByteArrayAttachment();
		adjunto2.setContenidoArchivo(pdf.getByteArray());
		
		adjunto2.setNombreArchivo("Factura_" + emisionFactura.getFolioFactura()+".pdf");
		adjunto2.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
		adjuntos.add(adjunto2);
		
		Map<String, String> mapInlineImages = new HashMap<String, String>();
		mapInlineImages.put("image1", sistemaContext.getDirectorioImagenesParametroGlobal() +"Cintillo_Seguros.jpg");
		
		envioNotificacionesService.enviarNotificacion(esReenvio?EnumCodigo.NOTIF_EMISION_FACTURA.toString()
				:EnumCodigo.NOTIF_EMISION_FACTURA.toString(), new HashMap<String, Serializable>(),null, 
				destinatarios, adjuntos,mapInlineImages);		
	}
	
	
	@Override
	public void enviarNotificacionCancelacionFactura(EmisionFactura emisionFactura, Boolean exitosa)
	{	
		NumberFormat currencyFormatter = NumberFormat.getCurrencyInstance();
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd/MM/yyyy");
		String codigoNotificacion;
		EmailDestinaratios destinatarios = new EmailDestinaratios();
		
		exitosa = exitosa != null ? exitosa:Boolean.FALSE; //Default cancelacion no exitosa.
		
		if(exitosa)
		{
			codigoNotificacion = EnumCodigo.NOTIF_CANCELA_FACTURA.toString();
			
			Usuario usuarioActual = usuarioService.getUsuarioActual();
			   
			List<Usuario> coordinadoresOperaciones = 
				usuarioService.buscarUsuariosSinRolesPorNombreRol(
						sistemaContext.getRolCoordinadorOperaciones(),
						usuarioActual.getNombreUsuario(), 
						usuarioActual.getIdSesionUsuario());
			
			List<Usuario> gerentesOpIndemnizaciones = 
				usuarioService.buscarUsuariosSinRolesPorNombreRol(
						sistemaContext.getRolGerenteOperacionIndemnizacionesAutos(),
						usuarioActual.getNombreUsuario(), 
						usuarioActual.getIdSesionUsuario());			
					
			for(Usuario coordinador:coordinadoresOperaciones)
			{
				destinatarios.agregar(EnumModoEnvio.PARA, coordinador.getEmail(), coordinador.getNombreCompleto()); 
			}
			
			for(Usuario gerenteOperacionIndemnizaciones:gerentesOpIndemnizaciones)
			{
				destinatarios.agregar(EnumModoEnvio.PARA, gerenteOperacionIndemnizaciones.getEmail(), gerenteOperacionIndemnizaciones.getNombreCompleto()); 
			}	
			
		}else
		{
			codigoNotificacion = EnumCodigo.NOTIF_CANCELA_FACTURA_NO_EXITOSA.toString();
			String username = emisionFactura.getCodigoUsuarioModificacion();
			Usuario usuarioCancelacion = usuarioService.buscarUsuarioPorNombreUsuario(username);
			
			destinatarios.agregar(EnumModoEnvio.PARA, usuarioCancelacion.getEmail(), usuarioCancelacion.getNombreCompleto());
		}				
		
		HashMap<String, Serializable> parametros = new HashMap<String, Serializable>();		
		parametros.put("folioFactura", emisionFactura.getFolioFactura());
		parametros.put("fechaFactura", simpleDateFormat.format(emisionFactura.getFechaGeneracion()));
		parametros.put("razonSocial", emisionFactura.getNombreRazonSocial());
		parametros.put("importeTotal", "\\" + currencyFormatter.format(emisionFactura.getImporte()));
		// OBTENER DATOS DE FECHA Y MOTIVO SOLICITUD DE CANCELACION DEL HISTORICO
		Map<String,String> mMotivoHistorico = this.obtenerMotivoCancelacion(emisionFactura);
		parametros.put("motivoCancelacion", mMotivoHistorico.get("motivo"));
		parametros.put("fechaCancelacion",  mMotivoHistorico.get("fecha") );
		parametros.put("directorioImagenes", sistemaContext.getDirectorioImagenes());		
		
		envioNotificacionesService.enviarNotificacion(codigoNotificacion, parametros, null, destinatarios);
	}
	
	
	private Map<String,String> obtenerMotivoCancelacion(EmisionFactura emisionFactura){
		
		Map<String,String> datos = new HashMap<String,String>();
		datos.put("motivo","");
		datos.put("fecha" ,"");
		SimpleDateFormat sdf =  new SimpleDateFormat("dd MMMM yyyy",new Locale("es","MX"));
		
		List<EmisionFacturaHistorico> lHistorico = emisionFactura.getLstEmisionFacturaHistorico();
		
		// ORDENAR POR ID DEL MAS ALTO AL MAS BAJO
		Collections.sort(lHistorico, new Comparator<EmisionFacturaHistorico>(){
			public int compare(EmisionFacturaHistorico vs1, EmisionFacturaHistorico vs2){
				return vs2.getId().compareTo(vs1.getId());
			}
		});
		
		for(EmisionFacturaHistorico historico : CollectionUtils.emptyIfNull(lHistorico)){
			if( historico.getEstatus().equals(EmisionFactura.EstatusFactura.CANCELACION_EN_TRAMITE.getValue()) ){
				datos.put("motivo", historico.getEstatusHistoricoDesc()  );
				datos.put("fecha" , sdf.format(historico.getFechaCreacion()) );
				break;
			}
		}
		
		return datos;
	}


	@Override
	public void cancelaFactura(Long idEmisionFactura, String motivoCancelacion, String observacion) {

		EmisionFactura emisionFactura = this.entidadService.findById(EmisionFactura.class, idEmisionFactura);
		emisionFactura.setEstatus(EmisionFactura.EstatusFactura.CANCELACION_EN_TRAMITE.getValue());
		emisionFactura.setFechaModificacion(new Date());
		emisionFactura.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());

		// ACTUALIZAR ESTATUS DE LA EMISION
		this.entidadService.save(emisionFactura);

		// GUARDAR HISTORICO DE LA CANCELACION
		this.generaHistorico(emisionFactura, motivoCancelacion, observacion);
		
		// SOLICITAR CANCELACION DE FACTURA
		this.emisionFacturaDao.insertarRegistroCancelacion(emisionFactura.getFolioFactura());

	}


	@Override
	public void generaHistorico(EmisionFactura emisionFactura, String motivoCancelacion, String observacion) {
		
		EmisionFacturaHistorico historico = new EmisionFacturaHistorico();
		
		historico.setEstatus           (emisionFactura.getEstatus());
		historico.setMotivoCancelacion (motivoCancelacion);
		historico.setObservaciones     (observacion);
		historico.setFolioFactura      (emisionFactura.getFolioFactura());
		historico.setEmisionFactura    (emisionFactura);
		historico.setCodigoUsuarioCreacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
		historico.setFechaCreacion     (new Date());
		
		this.entidadService.save(historico);
	}

	@Override
	public List<EmisionFacturaHistorico> obtenerHistoricoEmisionFactura(Long idEmisionFactura) {
		
		HashMap<String,Object> params = new HashMap<String,Object>();
		params.put("emisionFactura.id", idEmisionFactura);
		
		List<EmisionFacturaHistorico> lEmisiones = this.entidadService.findByPropertiesWithOrder(EmisionFacturaHistorico.class, params, "fechaCreacion desc");
		
		return lEmisiones;
	}

	@Override
	public void reFacturar(Long idEmisionFactura) {
		
		EmisionFactura emision = this.entidadService.findById(EmisionFactura.class, idEmisionFactura);
		this.generarFactura(emision);
		
	}

	@Override
	public void enviarFactura(Long idEmisionFactura, String destinatariosStr, String observaciones)
	{
		EmisionFactura emisionFactura = entidadService.findById(EmisionFactura.class, idEmisionFactura);		
		
		String[] listaDirecciones = destinatariosStr.split(";");
		
		EmailDestinaratios destinatariosAdicionales = new EmailDestinaratios();
		
		for(String direccionCorreo:listaDirecciones)
		{			
			destinatariosAdicionales.agregar(EnumModoEnvio.PARA, direccionCorreo, ""); 
		}		
		
		enviarNotificacionEmisionFactura(emisionFactura, destinatariosAdicionales, true);		
		
		bitacoraService.registrar(TIPO_BITACORA.EMISION_FACTURA, EVENTO.REENVIAR_FACTURA, emisionFactura.getId().toString(), 
				 String.format("Se reenvia la factura con el Folio [ %1$s ] a los siguientes destinatarios [ %2$s ]", 
						 emisionFactura.getFolioFactura(),destinatariosStr), 
				 "Observaciones: " + observaciones, usuarioService.getUsuarioActual().getNombreUsuario());
	}

	@Override
	public TransporteImpresionDTO obtenerPdf(Long idEmisionFactura)
	{   
		byte[] recibo = null;
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		Comprobante comprobante = null;
			
		EmisionFactura emisionFactura = entidadService.findById(EmisionFactura.class, idEmisionFactura);
		
		PrintReport printReport = impresionRecibosService.getServicioImpresionED();	
		
		try {
			
			recibo = printReport.getCFDIWithAddenda(emisionFactura.getFolioFactura());
			log.info("Factura: \n " + new String(recibo));
		
			ByteArrayOutputStream baos = new ByteArrayOutputStream(recibo.length);
			baos.write(recibo, 0, recibo.length);
			
			comprobante = recepcionFacturaService.parsearCFD(baos);
			
		} catch (Exception e) {
			
			log.error("Obtiene Factura XML y obtiene objeto SAT > " + e);
		} 		
		
		//------------------------------------------
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		
		String folioFiscal = "";
		String certificadoDigitalSat = "";
		String fechaCertificacion = "";
		String selloDigitalSAT = "";
		String cadenaOriginalTimbre = "";
		String iva = "";
		String porcentajeIva = "";
		String moneda = "";

		try {
			
	    factory.setNamespaceAware(true);
	    DocumentBuilder builder = factory.newDocumentBuilder();	    
		Document d = builder.parse(new ByteArrayInputStream(recibo));
		XPath xPath =  XPathFactory.newInstance().newXPath();
		String expression = "";
		
		//FOLIO FISCAL 
		expression = "//*[local-name()='TimbreFiscalDigital']/@UUID";
		log.info(expression);
		folioFiscal = xPath.compile(expression).evaluate(d);
		log.info(folioFiscal);
		
		//CERTIFICADO DIGITAL SAT 
		expression = "//*[local-name()='TimbreFiscalDigital']/@noCertificadoSAT";
		log.info(expression);
		certificadoDigitalSat = xPath.compile(expression).evaluate(d);
		log.info(certificadoDigitalSat);
		
		
		//FECHA DE CERTIFICACION 
		expression = "//*[local-name()='TimbreFiscalDigital']/@FechaTimbrado";
		log.info(expression);
		fechaCertificacion = xPath.compile(expression).evaluate(d);
		log.info(fechaCertificacion);
		
		//MONEDA
		expression = "//*[local-name()='Addenda']/*[local-name()='wfactura']/@Extra29";
		log.info(expression);
		moneda = xPath.compile(expression).evaluate(d);
		log.info(moneda);
		
		//SELLO SAT		
		expression = "//*[local-name()='TimbreFiscalDigital']/@selloSAT";
		log.info(expression);
		selloDigitalSAT = xPath.compile(expression).evaluate(d);
		log.info(selloDigitalSAT);
		
		//CADENA ORIGINAL		
		expression = "//*[local-name()='Addenda']/*[local-name()='wfactura']/@CadenaOriginal";
		log.info(expression);
		cadenaOriginalTimbre = xPath.compile(expression).evaluate(d);
		log.info(cadenaOriginalTimbre);
		
		//IVA		
		expression = "//*[local-name()='Traslado'][@impuesto='IVA']/@importe";
		log.info(expression);
		iva = xPath.compile(expression).evaluate(d);
		log.info(iva);
		
		//PORCENTAJE IVA		
		expression = "//*[local-name()='Traslado'][@impuesto='IVA']/@tasa";
		log.info(expression);
		porcentajeIva = xPath.compile(expression).evaluate(d);
		log.info(porcentajeIva);		
		
		} catch (Exception e) {
			
			log.error("Xpath parsing > " + e);
		} 
		
		//------------------------------------------
		
		JasperReport jReport = gImpresion.getOJasperReport(PLANTILLA_FACTURA_ELECTRONICA);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("NUMERO_FACTURA",comprobante.getSerie() + " " + comprobante.getFolio());
		params.put("CERTIFICADO_DIGITAL", comprobante.getCertificado()); 
		params.put("FOLIO_FISCAL", folioFiscal);
		params.put("FECHA_EMISION", comprobante.getFecha().toString());
		params.put("RAZON_SOCIAL_EMISOR", comprobante.getEmisor().getNombre());
		params.put("RFC_EMISOR", comprobante.getEmisor().getRfc());
		params.put("REGIMEN_EMISOR", comprobante.getEmisor().getRegimenFiscalArray(0).getRegimen());		
		params.put("CALLE_EMISOR", comprobante.getEmisor().getDomicilioFiscal().getCalle());
		params.put("NUMERO_EMISOR", comprobante.getEmisor().getDomicilioFiscal().getNoExterior());
		params.put("COLONIA_EMISOR", comprobante.getEmisor().getDomicilioFiscal().getColonia());
		params.put("MUNICIPIO_EMISOR", comprobante.getEmisor().getDomicilioFiscal().getMunicipio());
		params.put("ESTADO_EMISOR", comprobante.getEmisor().getDomicilioFiscal().getEstado());
		params.put("CODIGO_POSTAL_EMISOR", comprobante.getEmisor().getDomicilioFiscal().getCodigoPostal());
		params.put("PAIS_EMISOR", comprobante.getEmisor().getDomicilioFiscal().getPais());
		params.put("LUGAR_EXPEDICION", comprobante.getLugarExpedicion());
		params.put("RFC", comprobante.getReceptor().getRfc());
		params.put("NOMBRE_RAZON_SOCIAL", comprobante.getReceptor().getNombre());
		params.put("CALLE", comprobante.getReceptor().getDomicilio().getCalle());
		params.put("COLONIA", comprobante.getReceptor().getDomicilio().getColonia());
		params.put("MUNICIPIO", comprobante.getReceptor().getDomicilio().getMunicipio());
		params.put("ESTADO", comprobante.getReceptor().getDomicilio().getEstado());
		params.put("CODIGO_POSTAL", comprobante.getReceptor().getDomicilio().getCodigoPostal());
		params.put("REFERENCIA", "");
		params.put("PAIS", comprobante.getReceptor().getDomicilio().getPais());
		params.put("TELEFONO", emisionFactura.getTelefono());
		params.put("EMAIL", emisionFactura.getEmail());
		params.put("NUMERO_ORDEN","");
		params.put("FORMA_PAGO", comprobante.getFormaDePago());
		params.put("METODO_PAGO", comprobante.getMetodoDePago());		
		
		String numeroCuentaStr = emisionFactura.getDigitos() != null ? emisionFactura.getDigitos().toString():"";
		params.put("NUMERO_CUENTA", numeroCuentaStr.length() > 0
				? numeroCuentaStr.substring(numeroCuentaStr.length()-4, numeroCuentaStr.length()):"");
		
		params.put("MONEDA", moneda);
		params.put("TIPO_CAMBIO", "1");
		params.put("SUBTOTAL", comprobante.getSubTotal());
		params.put("DESCUENTO", comprobante.getDescuento());
		params.put("IVA", iva != null? new BigDecimal(iva):BigDecimal.ZERO);	
		params.put("PORCENTAJE_IVA", porcentajeIva);
		params.put("TOTAL", comprobante.getTotal());
		params.put("IMPORTE_LETRA",NumeroALetra.convertirFormatoPesos(comprobante.getTotal().doubleValue()).trim());
		params.put("OBSERVACIONES", emisionFactura.getObservacionesFacturacion());
		params.put("CERTIFICADO_DIGITAL_SAT", comprobante.getNoCertificado());
		params.put("FECHA_CERTIFICACION", fechaCertificacion);
		params.put("CADENA_ORIGINAL_TIMBRE", cadenaOriginalTimbre);
		params.put("SELLO_DIGITAL_EMISOR", comprobante.getSello());
		params.put("SELLO_DIGITAL_SAT", selloDigitalSAT);
		String cadenaQR = "re=" + comprobante.getEmisor().getRfc() 
						+ "&rr=" + comprobante.getReceptor().getRfc()
						+ "&tt=" + comprobante.getTotal().toPlainString() 
						+ "&id=" + folioFiscal;
		params.put("CADENA_CODIGO_QR", cadenaQR);
		
		List<ConceptoFacturaDTO> dataSourceConceptos = new ArrayList<ConceptoFacturaDTO>();
		
		for(Concepto c :comprobante.getConceptos().getConceptoArray())
		{
			ConceptoFacturaDTO concepto = new ConceptoFacturaDTO();
			concepto.setClave("");
			concepto.setCantidad(c.getCantidad().intValue());
			concepto.setUnidadMedida(c.getUnidad());
			concepto.setDescripcion(c.getDescripcion());
			concepto.setPrecioUnitario(c.getValorUnitario());
			concepto.setImporte(c.getImporte());	
			
			dataSourceConceptos.add(concepto);			
		}		
		
		JasperReport jsubReporte = gImpresion.getOJasperReport(PLANTILLA_CONCEPTO_FACTURA_ELECTRONICA);
		
		params.put("dataSourceConceptos", dataSourceConceptos);
		params.put("jReportConceptosFactura", jsubReporte);
				
		TransporteImpresionDTO contenedorReporte = gImpresion.getTImpresionDTO(jReport, new JREmptyDataSource(), params);
		
		return contenedorReporte;
	}

	@Override
	public AutoIncisoReporteCabina obtenerDatosVehiculoSalvamentoByIngreso(
			Long ingresoId) {
		
		AutoIncisoReporteCabina autoInciso = null;
		Ingreso ingreso = this.entidadService.findById(Ingreso.class, ingresoId);
		
		System.out.println(ingreso.getRecuperacion().getTipo()+" "+Recuperacion.TipoRecuperacion.SALVAMENTO.getValue());
		if( ingreso != null && ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue()) ){
			
			autoInciso = 
					this.recuperacionSalvamentoService.obtenerDatosAutoByRecuperacionSalvamento( (RecuperacionSalvamento) ingreso.getRecuperacion() );
		}
		
		return autoInciso;
	}
	
	/***
	 * En caso de ser salvamento busco en el autoinciso los detalles del vehiculo asociado a esa recuperación
	 * @param emisionFactura
	 */
	private void llenarDatosVehiculoSalvamento(EmisionFactura emisionFactura){
		
		if( emisionFactura != null ){
			
			AutoIncisoReporteCabina autoInciso = this.recuperacionSalvamentoService.obtenerDatosAutoByRecuperacionSalvamento( (RecuperacionSalvamento) emisionFactura.getIngreso().getRecuperacion() );
			
			emisionFactura.setMarca  (autoInciso.getDescMarca());
			emisionFactura.setModelo (autoInciso.getModeloVehiculo().toString());
			emisionFactura.setColor  (autoInciso.getDescColor());
			emisionFactura.setNoSerie(autoInciso.getNumeroSerie());
			emisionFactura.setNoMotor(autoInciso.getNumeroMotor());
			emisionFactura.setModelo (autoInciso.getModeloVehiculo().toString());
			emisionFactura.setTipo   (autoInciso.getDescTipoUso());
		}
	}
	
	@Override
	public void validaCancelacionesEnWFactura(){
		System.out.println(" =========! INVOCACION A CANCELACION DE FACTURA !!!!!!!!!!! ");
		
		
		Usuario usuario = new Usuario();
		usuario.setNombreUsuario("M2ADMINI");
		this.usuarioService.setUsuarioActual(usuario);
		
		final int CODIGO_EXITO = 201;
		final int CODIGO_ENTREGA_PROVEEDOR = 0;
		final int CODIGO_RECIBE_PROVEEDOR = 1;
		List<EmisionFactura> emisionesRespuesta = this.emisionFacturaDao.validaCancelacionesEnWFactura();
		for(EmisionFactura emisionRespuesta : emisionesRespuesta){
			EmisionFactura emisionFactura = this.entidadService.findById(EmisionFactura.class, emisionRespuesta.getId());
			int estatusCancelacion = emisionRespuesta.getEstatusCancelacion().intValue(); 
			if(estatusCancelacion == CODIGO_EXITO){
				emisionFactura.setEstatus(EmisionFactura.EstatusFactura.CANCELADA.getValue());
				emisionFactura.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
				emisionFactura.setFechaModificacion(new Date());
				this.enviarNotificacionCancelacionFactura(emisionFactura, Boolean.TRUE);
			}else if(estatusCancelacion != CODIGO_ENTREGA_PROVEEDOR && estatusCancelacion != CODIGO_RECIBE_PROVEEDOR){
				emisionFactura.setEstatus(EmisionFactura.EstatusFactura.FACTURADA.getValue());
				emisionFactura.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
				emisionFactura.setFechaModificacion(new Date());
				this.enviarNotificacionCancelacionFactura(emisionFactura, Boolean.FALSE);
			}
			this.entidadService.save(emisionFactura);

			// AGREGAR EL HISTORICO
			this.generaHistorico(emisionFactura, EstatusEmisionFactCancelacion.OTRO_MOTIVO.getValue() , "CANCELACION POR WS");
		}
	}
	
	public void initialize() {
		String timerInfo = "TimerEmisionFactura";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(0);
				expression.hour(5);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerEmisionFactura", false));
				
				log.info("Tarea TimerEmisionFactura configurado");
			} catch (Exception e) {
				log.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		log.info("Cancelar Tarea TimerEmisionFactura");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			log.error("Error al detener TimerEmisionFactura:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		validaCancelacionesEnWFactura();
	}
}
