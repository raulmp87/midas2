package mx.com.afirme.midas2.service.impl.compensaciones;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Types;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.sql.DataSource;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.ramo.RamoProductoDTO;
import mx.com.afirme.midas2.dao.compensaciones.CompensacionesAdicionalesDao;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionCompensacionLista;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.dto.compensaciones.FiltroCompensacion;
import mx.com.afirme.midas2.dto.compensaciones.Respuesta;
import mx.com.afirme.midas2.service.compensaciones.CompensacionesAdicionalesService;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.jdbc.datasource.DataSourceUtils;



@Stateless
public class CompensacionesAdicionalesServiceImpl implements CompensacionesAdicionalesService{
	
	private JdbcTemplate jdbcTemplate;
	
	private SimpleJdbcCall procesarAmortizacionJob;
	private static final Logger LOG = Logger.getLogger(CompensacionesAdicionalesServiceImpl.class);

	@EJB
	private CompensacionesAdicionalesDao compensacionesAdicionalesDao;
	
	public Respuesta guardarCompensacionAdicional(CaCompensacion compensacionAdicional){
		return null;
	}
	
	public Long totalCompensaciones(FiltroCompensacion filtro){
		return compensacionesAdicionalesDao.totalCompensaciones(filtro);
	}
	
	public List<CaCompensacion> buscarCompensaciones(FiltroCompensacion filtro){
		return compensacionesAdicionalesDao.buscarCompensaciones(filtro);
	}
	
	public List<CaConfiguracionCompensacionLista> buscarEntidadesPersona(FiltroCompensacion filtro){
		return compensacionesAdicionalesDao.buscarEntidadesPersona(filtro);
	}
	
	public List<CaParametros> buscarParametrosGenerales(FiltroCompensacion filtro){
		return compensacionesAdicionalesDao.buscarParametrosGenerales(filtro);
	}
	
	
	public Respuesta calcularCompensacion(CotizacionDTO cotizacion){
		Respuesta respuesta = new Respuesta();
		List<RamoProductoDTO> listaRamoProducto = cotizacion.getSolicitudDTO().getProductoDTO().getRamoProductoDTOList();
		if (listaRamoProducto != null && listaRamoProducto.size() > 0 ) {
			RamoProductoDTO ramoProducto = listaRamoProducto.get(0);
			RamoDTO ramo = ramoProducto.getRamoDTO();
			if (ramo.getClaveNegocio().trim().compareTo("A")==0){
				if (cotizacion.getSolicitudDTO().getAgente() != null && cotizacion.getSolicitudDTO().getNegocio()!=null){
					
				}
			}
		}
		return respuesta;
	}
	
	private Connection con ;
	@SuppressWarnings("unused")
	private DataSource dataSource;
	
	
	@Resource(name = "jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {		
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.setCon(DataSourceUtils.getConnection(dataSource));
		this.procesarAmortizacionJob = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("MIDAS")
				.withCatalogName("PKG_CA_AMORTIZACION")
				.withProcedureName("procesarAmortizacionJob")
				.declareParameters(
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));
	}
	
	//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	
	@Override
	public String procesarAmortizacionJob(){
		LOG.info(">>Ejecutando procesarAmortizacionJob()");
		Map<String, Object> execute = this.procesarAmortizacionJob.execute();
		BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
		String pDesc_Resp  = (String) execute.get("pDesc_Resp");
		LOG.info("pId_Cod_Resp"+idCodResp);
		LOG.info("pDesc_Resp"+pDesc_Resp);
		return pDesc_Resp;
	}

	public void setCon(Connection con) {
		this.con = con;
	}

	public Connection getCon() {
		return con;
	}
}
