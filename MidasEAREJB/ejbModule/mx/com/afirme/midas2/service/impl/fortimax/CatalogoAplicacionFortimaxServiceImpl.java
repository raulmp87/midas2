package mx.com.afirme.midas2.service.impl.fortimax;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fortimax.CatalogoAplicacionFortimaxDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.service.fortimax.CatalogoAplicacionFortimaxService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class CatalogoAplicacionFortimaxServiceImpl implements CatalogoAplicacionFortimaxService{
	private CatalogoAplicacionFortimaxDao dao;
	@Override
	public void delete(Long arg0) throws MidasException {
		dao.delete(arg0);
	}

	@Override
	public Long delete(CatalogoAplicacionFortimax arg0) throws MidasException {
		return dao.delete(arg0);
	}

	@Override
	public List<CatalogoAplicacionFortimax> findByFilters(CatalogoAplicacionFortimax arg0) {
		return dao.findByFilters(arg0);
	}

	@Override
	public CatalogoAplicacionFortimax getAplicacionPorNombre(String arg0)throws MidasException {
		return dao.getAplicacionPorNombre(arg0);
	}

	@Override
	public CatalogoAplicacionFortimax loadById(Long arg0) throws MidasException {
		return dao.loadById(arg0);
	}

	@Override
	public Long save(CatalogoAplicacionFortimax arg0) throws MidasException {
		return dao.save(arg0);
	}
	@Override
	public String[] obtenerParametrosAplicacion(String nombreAplicacion) throws MidasException{
		return dao.obtenerParametrosAplicacion(nombreAplicacion);
	}
	@Override
	public String obtenerParametroLlavePorAplicacion(String nombreAplicacion) throws MidasException{
		return dao.obtenerParametroLlavePorAplicacion(nombreAplicacion);
	}
	/**
	 * Setters and getters
	 */
	@EJB
	public void setDao(CatalogoAplicacionFortimaxDao dao) {
		this.dao = dao;
	}
}
