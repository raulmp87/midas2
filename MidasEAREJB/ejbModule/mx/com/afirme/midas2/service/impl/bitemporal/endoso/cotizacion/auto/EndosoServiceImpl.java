package mx.com.afirme.midas2.service.impl.bitemporal.endoso.cotizacion.auto;

import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO;
import static mx.com.afirme.midas.solicitud.SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.EJBException;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.swing.text.Document;
import javax.swing.text.rtf.RTFEditorKit;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.IOUtils;
import org.joda.time.DateTime;
import org.joda.time.Interval;
import org.springframework.beans.BeanUtils;

import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.js.util.StringUtil;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADTO;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAFacadeRemote;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAId;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.cobranza.CobranzaService;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.CotizacionEndosoDao;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.EndosoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.comision.BitemporalComision;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.BitemporalDocAnexoCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.aumento.BitemporalCoberturaAumento;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento.BitemporalCoberturaDescuento;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.detalle.BitemporalCoberturaDetallePrima;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo.BitemporalCoberturaRecargo;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.BitemporalDatoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.subinciso.BitemporalSubIncisoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.BitemporalTexAdicionalCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCotContinuity;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.EstatusRegistro;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.TipoEmision;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoCobro;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoService;
import mx.com.afirme.midas2.service.bitemporal.excepcion.ExcepcionSuscripNegAutoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.NivelEvaluacion;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.service.tarea.EmisionPendienteService;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.DateUtils.Indicador;

@Stateless
public class EndosoServiceImpl implements EndosoService {
	
	private final MathContext mathCtx = new MathContext(2, RoundingMode.HALF_UP);
	
	public enum EstatusValidacionRecibosPT {
		RECIBOSAGRUPADOS,
		RECIBOSPENDIENTESDEPAGO,
		RECIBOSOK
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoCFP(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO,SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO,accionEndoso);
	}

	@Override
	public BitemporalCotizacion getCotizacionEndosoEV(BigDecimal polizaId,
			String accionEndoso) {
		
		return getCotizacionEndosoEV(polizaId, accionEndoso, null);
	}
	public BitemporalCotizacion getCotizacionEndosoEV(BigDecimal polizaId,
			String accionEndoso, Date fechaExtension) {
		
		if (polizaId == null) {
			throw new IllegalArgumentException("[initCotizacionEndoso] El id de la póliza es nula.");
		}
		//se obtiene la poliza
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
				polizaId);
		if (poliza == null) {
			return null;
		}
		// se obtiene la fecha de creación de la póliza para obtener la cotización
//		ControlEndosoCot conEndCot = null;
		Object[] res = obtenerFechaBusquedaCotizacion(poliza,accionEndoso);
		Date fechaBusquedaCot = (Date)res[0];
//		if(res[1] != null)
//			conEndCot = (ControlEndosoCot)res[1];
		
		BitemporalCotizacion bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
				CotizacionContinuity.BUSINESS_KEY_NAME, poliza.getCotizacionDTO().getIdToCotizacion(),
				TimeUtils.getDateTime(fechaBusquedaCot));
		
	   	if(bitemporalCotizacion == null){
	   		bitemporalCotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME,  poliza.getCotizacionDTO().getIdToCotizacion(), 
					TimeUtils.getDateTime(Utilerias.agregaDiasAFecha(fechaBusquedaCot, -1)));	   		
	   	}
	   	
		//Se valida que la cotización no sea nula.s
		validaCotizacionNoNula(bitemporalCotizacion, poliza);
		
		//se obtiene la fecha fin de vigencia para la póliza.
		//si existe endoso significa que ya fue cotizada, asi que se obtiene de la bitemporal la 
		//fecha validFrom
		Date fechaInicioEndoso = null;
		if (accionEndoso.equals(TipoAccionDTO.getNuevoEndosoCot())) {
//			if(conEndCot != null)
//				fechaInicioEndoso = bitemporalCotizacion.getValidityInterval().getInterval().getStart().toDate();
//			elses
			fechaInicioEndoso = bitemporalCotizacion.getValidityInterval().getInterval().getEnd().toDate();;
		}else{
			fechaInicioEndoso = bitemporalCotizacion.getValidityInterval().getInterval().getStart().toDate();
		}
		
		//si se especifica la fecha es para cuestión de validación.
		if(fechaExtension != null) {
			validaFechaExtension(bitemporalCotizacion, fechaExtension,poliza);
		}
		//la fecha de inicio de vigencia para la extensión de la póliza es la fecha fin de 
		//vigencia que tiene actualmente la póliza.
		return initCotizacionEndoso(poliza, fechaInicioEndoso, 
				SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA, 
				SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	/**
	 * Valida la fecha de Extensión con respecto a la fecha de la cotización.
	 * @param bitemporalCotizacion
	 * @param fechaExtension
	 * @param poliza, Poliza solamente es requerida para mandar el mensaje d excepción.
	 */
private void validaFechaExtension(BitemporalCotizacion bitemporalCotizacion, Date fechaExtension,
		PolizaDTO poliza){
	//si se especifica la fecha es para cuestión de validación.
	if(fechaExtension != null){
		//se obtiene la fecha fin de vigencia para la póliza.
		Date fechaFinVigencia = bitemporalCotizacion.getValue().getFechaFinVigencia();
		if(DateUtils.dateDiff(fechaExtension, fechaFinVigencia, Indicador.Days)<=0){
			Object[] pars = new Object[1];
			if(poliza != null){
				pars[0] = poliza.getNumeroPolizaFormateada()+" ("+poliza.getIdToPoliza()+")" ;
			}else{
				pars[0] = "(BitemporalCotizacion :"+bitemporalCotizacion.getId()+")";
			}
			throw new NegocioEJBExeption("NE_9",pars, "La fecha de extensión no puede ser menor o igual  a la fecha fin de vigencia para la p\u00F3liza "+pars[0]);
		}
	} 
		
	}
	@Override
	public BitemporalCotizacion getCotizacionEndosoCEAPDummy(BigDecimal polizaId,
			Date fechaInicioVigencia){
		
		Calendar cal = Calendar.getInstance();
		cal.add(Calendar.DATE, 2);
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		
		throw new NegocioEJBExeption("NE_5",new Object[]{polizaId,cal.getTime()}, "El endoso puede ser cancelado solo para la fecha "+format.format(cal.getTime())+" para la póliza "+polizaId);
	}
	
	//TODO: Aqui falta agregar la firma a EndosoService.Por el momento falta por definir bien la firma.
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoCEAP(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso) {
		short tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO;
//		if (polizaId == null) {
//			throw new IllegalArgumentException("[initCotizacionEndoso] El id de la póliza es nula.");
//		}
//		//se obtiene la poliza
//		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
//				polizaId);
//		if (poliza == null) {
//			return null;
//		}
//		// se realiza la validación de si es cancelable.
//		validaEsCancelablePoliza(poliza,fechaInicioVigencia, tipoEndoso);
		
		//se realiza la cotización
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, tipoEndoso,SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO,accionEndoso);
	}
 
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoMovimientos(BigDecimal polizaId,String accionEndoso) {
		return this.getCotizacionEndosoMovimientos(polizaId, TimeUtils.now().toDate(), accionEndoso);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoMovimientos(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS,SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO,accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoIT(BigDecimal polizaId, String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, TimeUtils.now().toDate(), SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO,SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoIT(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO,SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoExclusionTexto(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_EXCLUSION_TEXTO,SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoIA(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO,SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoBajaInciso(BigDecimal polizaId,String accionEndoso, Short motivoEndoso) {
		return this.getCotizacionEndosoBajaInciso(polizaId, TimeUtils.now().toDate(),accionEndoso, motivoEndoso);
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoBajaInciso(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso, Short motivoEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO,  motivoEndoso,  accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BitemporalCotizacion getCotizacionEndosoBajaIncisoTrxNew(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso, Short motivoEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO,  motivoEndoso,  accionEndoso);
	}
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoAltaInciso(
			BigDecimal polizaId, String accionEndoso) {		
		return this.getCotizacionEndosoAltaInciso(polizaId, TimeUtils.now().toDate(), accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NEVER)
	public BitemporalCotizacion getCotizacionEndosoAltaInciso(
			BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {		
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BitemporalCotizacion getCotizacionEndosoAltaIncisoTrxNew(
			BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {		
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BitemporalCotizacion getCotizacionEndosoBajaIncisoPerdidaTotal(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso) {
		
		return this.getCotizacionEndosoBajaIncisoPerdidaTotal(polizaId,
				fechaInicioVigencia, accionEndoso,SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL,
				EmisionPendiente.POLIZA_NO_MIGRADA);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BitemporalCotizacion getCotizacionEndosoBajaIncisoPerdidaTotal(BigDecimal polizaId,
			Date fechaInicioVigencia,String accionEndoso, short claveMotivoEndoso, short migrada) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, 
				SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO, claveMotivoEndoso, 
				accionEndoso, migrada);
	}
	
	@Override
	public void getCotizacionEndosoCancelacionEndoso(BigDecimal polizaId) {
		initCotizacionEndosoCancelacion(polizaId);
	}

	@Override
	public BitemporalCotizacion getCotizacionEndosoCambioDatos(
			BigDecimal polizaId, String accionEndoso) {
		return getCotizacionEndosoCambioDatos(polizaId, TimeUtils.now().toDate(), accionEndoso);
	}
	
	@Override
	public BitemporalCotizacion getCotizacionEndosoCambioDatos(
			BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BitemporalCotizacion getCotizacionEndosoCambioAgente(BigDecimal polizaId, Date fechaInicioVigencia,
			String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE,
				SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	
	@Override
	public BitemporalCotizacion getCotizacionEndosoRehabilitacionInciso(
			BigDecimal polizaId, String accionEndoso) {
		
		return this.getCotizacionEndosoRehabilitacionInciso(polizaId, TimeUtils.now().toDate(), accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BitemporalCotizacion getCotizacionEndosoRehabilitacionInciso(
			BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso, short motivoEndoso){
		
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS, motivoEndoso, accionEndoso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public BitemporalCotizacion getCotizacionEndosoRehabilitacionInciso(
			BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {
		
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	
	@Override
	public BitemporalCotizacion getCotizacionEndosoRehabilitacionEndoso(
			BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void guardaCotizacionEndoso(BitemporalCotizacion cotizacion) {
		
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		
		PolizaDTO poliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", new BigDecimal(cotizacion.getContinuity().getNumero())).get(0);
		
		if (solicitudDTO.getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO) {
			cotizacion.getValue().setSolicitud(solicitudDTO);		
			try {
				cobranzaService.consultaCoberturasConSaldoPendiente(poliza.getIdToPoliza());
			}
			catch (Exception e) {
				
				if(e.getCause() instanceof NegocioEJBExeption) // Ajuste propagar error a GUI
				{
					throw (NegocioEJBExeption)e.getCause();
				}else				
				{
					String mensaje="";
					if(e.getCause() instanceof EJBException)
					{
						EJBException ejbException = (EJBException)e.getCause();
						mensaje = ejbException.getCause().getMessage();					
					}
					
					throw new NegocioEJBExeption("_", "No es posible realizar la operaci\u00F3n. " + mensaje);				
				}			
			}
		}
		
		if (cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
			//entidadContinuityDao.refresh(cotizacion.getContinuity());
		} else {
			cotizacion.getValue().setSolicitud(solicitudDTO);
			cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
			//Guardo registro historico en tabla control endosos
			saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
		}
		if (solicitudDTO.getClaveTipoEndoso().shortValue() != SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS) {
			if(solicitudDTO.getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO){
				generaBitEndosoCFP(poliza.getIdToPoliza(), cotizacion.getValue().getSolicitud().getIdToSolicitud(),
						cotizacion.getValue().getFormaPago().getIdFormaPago(), cotizacion.getValue().getPorcentajePagoFraccionado());
			}else{
				entidadBitemporalService.saveInProcess(cotizacion,TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
			}
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void guardaCotizacionEndosoCambioAgente(BitemporalCotizacion cotizacion) {
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		solicitudDTO.setCodigoAgente(cotizacion.getValue().getSolicitud().getCodigoAgente());
		solicitudDTO.setComentarioList(cotizacion.getValue().getSolicitud().getComentarioList());
		solicitudDTO.setNombreAgente(cotizacion.getValue().getSolicitud().getNombreAgente());
		solicitudDTO.getComentarioList().get(0).getSolicitudDTO().setIdToSolicitud(solicitudDTO.getIdToSolicitud());
		solicitudDTO.getComentarioList().get(0).setFechaCreacion(new Date());
		solicitudDTO.getComentarioList().get(0).setCodigoUsuarioCreacion(solicitudDTO.getCodigoUsuarioCreacion());		
		solicitudDTO.setNombreOficinaAgente(cotizacion.getValue().getSolicitud().getNombreOficinaAgente());
		solicitudDTO.setCodigoEjecutivo(cotizacion.getValue().getSolicitud().getCodigoEjecutivo());
		solicitudDTO.setNombreEjecutivo(cotizacion.getValue().getSolicitud().getNombreEjecutivo());
		solicitudDTO.setIdOficina(cotizacion.getValue().getSolicitud().getIdOficina());
		solicitudDTO.setNombreOficina(cotizacion.getValue().getSolicitud().getNombreOficina());	
		
		if (cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
			//entidadContinuityDao.refresh(cotizacion.getContinuity());
		} else {
			cotizacion.getValue().setSolicitud(solicitudDTO);
			cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
			//Guardo registro historico en tabla control endosos
			saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
		}
		entidadService.save(solicitudDTO);
		entidadBitemporalService.saveInProcess(cotizacion,TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
	}

	private Object[] obtenerFechaBusquedaCotizacion(PolizaDTO poliza,String accionEndoso){
		Date fecha = poliza.getCotizacionDTO().getFechaInicioVigencia();
		short  cveStatusCot = CotizacionDTO.ESTATUS_COT_EN_PROCESO;
		if (accionEndoso.equals(TipoAccionDTO.getNuevoEndosoCot())) {
			cveStatusCot = CotizacionDTO.ESTATUS_COT_EMITIDA;
		}
		ControlEndosoCot  controlEndosoCot =   obtenerControlEndosoCotiz(poliza.getIdToPoliza(),cveStatusCot);
		if(controlEndosoCot != null){
			fecha = controlEndosoCot.getValidFrom();
		}
		return new Object[]{fecha,controlEndosoCot};
	}
	/**
	 * Obtiene la fecha la última cotización y dependiendo de la accionEndoso.
	 * @param poliza
	 * @return
	 */
//	private Date obtenerFechaBusquedaCotizacion(PolizaDTO poliza){
//		Date fecha = poliza.getFechaCreacion();
//		ControlEndosoCot conEndCot =   obtenerControlEndosoCot(poliza.getIdToPoliza());
//		if(conEndCot != null){
//			
//			fecha = conEndCot.getValidFrom();
//		}
//		return fecha;
//	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void guardaCotizacionEndosoEV(BitemporalCotizacion cotizacion, Date validoHasta) {
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		
		//si se especifica la fecha es para cuestión de validación.
		if(validoHasta != null){
			validaFechaExtension(cotizacion, validoHasta,null);
		}
		
		if(cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()){
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
		}else{
			cotizacion.getValue().setSolicitud(solicitudDTO);
			cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
			saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),cotizacion.getValue().getFechaFinVigencia());
		}
		
		//entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, poliza.getCotizacionDTO().getIdToCotizacion());
		cotizacion.setContinuity(entidadContinuityDao.findByKey(CotizacionContinuity.class, cotizacion.getContinuity().getId()));
		//se realiza el guardado para todo el arbol jerarquíco partiendo desde la cotización.
		generaCotizacionEndosoExtensionVigencia(cotizacion, validoHasta);
		
		//Se modifica a que siempre traiga el default del negocio
		//if (cotizacion.getValue().getNegocioDerechoEndosoId() == null) {
			for (NegocioDerechoEndoso negocioDerechoEndoso : cotizacion.getValue().getSolicitud().getNegocio().getNegocioDerechoEndosos()) {
				if (negocioDerechoEndoso.getClaveDefault()) {
					cotizacion.getValue().setNegocioDerechoEndosoId(negocioDerechoEndoso.getIdToNegDerechoEndoso());
					cotizacion.getValue().setValorDerechosUsuario(negocioDerechoEndoso.getImporteDerecho());
					break;
				}
			}
		//}else{
		//	NegocioDerechoEndoso negocioDerechoEndoso = entidadService.findById(NegocioDerechoEndoso.class, cotizacion.getValue().getNegocioDerechoEndosoId());
		//	cotizacion.getValue().setValorDerechosUsuario(negocioDerechoEndoso.getImporteDerecho());
		//}
		
		//se manda actualizar la cotización continuity. esto realiza el guardado anido de los
		//objeto que contiene.
		entidadContinuityDao.update(cotizacion.getContinuity());
		
	}
	
	/**
	 * Guarda en proceso para la contización todo el arbol jerarquico contenido. 
	 * @param cotizacion
	 * @param validoHasta
	 */
	@Override	
	public void generaCotizacionEndosoExtensionVigencia(BitemporalCotizacion cotizacion, Date validoHasta) {
		generaCotizacionEndosoExtensionVigencia(cotizacion, null, validoHasta);
	}
	
	public void generaCotizacionEndosoExtensionVigencia(BitemporalCotizacion cotizacion,  Date validoEn, Date validoHasta) {
		// se valida que la fecha de extensión.
		validaFechaExtension(cotizacion, validoHasta, null);
		
		//Obtiene poliza
		try{
			List<PolizaDTO> list = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", new BigDecimal(cotizacion.getContinuity().getNumero()));
			if(list != null && !list.isEmpty()){
				generaBitEndosoExtensionVigencia(list.get(0).getIdToPoliza(), validoHasta);
			}
		}catch(Exception e){
			e.printStackTrace();
			throw new NegocioEJBExeption("_", e.getMessage());
		}
		

	}
	
	private MensajeDTO generaBitEndosoExtensionVigencia(BigDecimal idToPoliza, Date fechaExtension) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Gen_End_Ext_Vigencia";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pFechaExtension", fechaExtension);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	private MensajeDTO ajusteBitEndosoExtensionVigenciaRehab(BigDecimal idToPoliza) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Rehab_End_Ext_Vigencia";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	@Override
	public void guardaCotizacionEndosoMovimientos(
			BitemporalCotizacion cotizacion) {
		
		validaCotizacionConPrima(cotizacion, null);
		
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		if(!cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess())
		{			
			saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
		}	
		entidadBitemporalService.saveInProcess(cotizacion,TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
	}
	
	@Override
	public void validaPrimaNetaCeroEndosoCotizacion(Short claveTipoEndoso, Long idToPoliza, Date validoEn){
		IncisoCotizacionDTO filtros = new IncisoCotizacionDTO();
		filtros.setSoloCotizados(true);
		int idTipoVista = 0;
		String elementosSeleccionados = "";
		List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>(1);
		listaIncisosCotizacion = listadoIncisosDinamicoService.seleccionarIncisos(listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros, claveTipoEndoso, idToPoliza, validoEn, true, idTipoVista), elementosSeleccionados);
		for(BitemporalInciso item : listaIncisosCotizacion){
			validaPrimaNetaCeroEndoso(item, validoEn, claveTipoEndoso);
		}
	}
	
	@Override
	public void validaPrimaNetaCeroEndoso(BitemporalInciso bitemporalInciso, Date validoEn, Short claveTipoEndoso){
    	Collection<BitemporalCoberturaSeccion> biCoberturaSeccionList = new ArrayList<BitemporalCoberturaSeccion>(1);
    	try{
    		if(claveTipoEndoso.equals(SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS)){
    			biCoberturaSeccionList = incisoViewService.getLstCoberturasByInciso(bitemporalInciso.getEntidadContinuity().getId(), validoEn, false);
    		}else if(claveTipoEndoso.equals(SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)){
    			biCoberturaSeccionList = incisoViewService.getLstCoberturasByInciso(bitemporalInciso.getEntidadContinuity().getId(), validoEn, true);
    		}
    	}catch(Exception e){
    		e.printStackTrace();
    	}
    	String mensaje = "";
    	StringBuilder mensajes = new StringBuilder("");
    	
    	for(BitemporalCoberturaSeccion item : biCoberturaSeccionList){
    		if(item.getValue().getValorPrimaNeta().doubleValue() == 0 && item.getValue().getClaveContrato().doubleValue() == 1){
    			if(item.getContinuity().getCoberturaDTO().getClaveImporteCero().equals("0")){
    				if(item.getContinuity().getCoberturaDTO().getIdToCobertura().equals(CoberturaDTO.IDTOCOBERTURA_DANIOSOCASIONADOSPORLACARGA)){
    					Collection<BitemporalDatoSeccion> riesgoCotizacion = configuracionDatoIncisoService.getDatosSeccionByInciso(bitemporalInciso.getContinuity().getId(), 
        						validoEn, TimeUtils.now().toDate(), claveTipoEndoso, true);
    					
    					for(BitemporalDatoSeccion dato: riesgoCotizacion){
    						if(dato.getContinuity().getCoberturaId().equals(CoberturaDTO.IDTOCOBERTURA_DANIOSOCASIONADOSPORLACARGA.longValue())
    								&& dato.getValue().getValor() != null && !dato.getValue().getValor().equals("10")){
    							mensajes.append(item.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity().getNumero()).append(" .- La Cobertura ").append(item.getContinuity().getCoberturaDTO().getNombreComercial()).append(" no permite Prima Neta en Cero \n");    							
    						}
    					}
    				}else{
    					mensajes.append("La Cobertura ").append(item.getContinuity().getCoberturaDTO().getNombreComercial()).append(" no permite Prima Neta en Cero \n");
    				}
    			}
    		}
    	}
    	mensaje = mensajes.toString();
    	if(!mensaje.isEmpty()){
    		throw new NegocioEJBExeption("_", mensaje);
    	}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void guardaCotizacionRehabilitacionEndoso(BitemporalCotizacion cotizacion, BigDecimal idToPoliza, DateTime validoEn, Short numeroEndoso, String accionEndoso, short motivoEndoso) {
				
		//Se obtiene el ControlEndosoCot del endoso a cancelar/rehabilitar
		ControlEndosoCot controlEndosoARehabilitar = getControlFromEndoso(cotizacion.getEntidadContinuity().getNumero().longValue(), numeroEndoso);
		
		//Se obtiene el ControlEndosoCot del endoso que cancelo al endoso a rehabilitar
		ControlEndosoCot controlEndosoCE = cotizacionEndosoDao.getControlEndosoCE(controlEndosoARehabilitar.getId());
		SolicitudDTO solicitud = prepareSolicitudDTO(cotizacion, idToPoliza, 
					new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO), validoEn, motivoEndoso);
		//Se cancela el endoso de CE (Rehabilitacion)
		cancelaEndoso(cotizacion, controlEndosoCE, solicitud, validoEn, accionEndoso);
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void ajustaCotizacionRehabilitacionEndoso(BitemporalCotizacion cotizacion, BigDecimal idToPoliza, Short numeroEndoso) {
				
		//Se obtiene el ControlEndosoCot del endoso a cancelar/rehabilitar
		ControlEndosoCot controlEndosoARehabilitar = getControlFromEndoso(cotizacion.getEntidadContinuity().getNumero().longValue(), numeroEndoso);
		
		//Se obtiene el ControlEndosoCot del endoso que cancelo al endoso a rehabilitar
		if (controlEndosoARehabilitar.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA) {
			ajusteBitEndosoExtensionVigenciaRehab(idToPoliza);
		}
				
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	//Permite guardar cada una de las entidades bitemporales asociadas a la cotizacion, previa invocacion del guardarCotizacionEndoso
	public <B extends EntidadBitemporal> void guardarEntidadBitemporalEndoso(B entidadBitemporal, DateTime validoEn) {
		entidadBitemporalService.saveEndorsementBitemporalEntity(entidadBitemporal, validoEn);
	}

	
	/*public void guardarEntidadBitemporalEndoso(@SuppressWarnings("rawtypes") EntidadBitemporal entidadBitemporal, DateTime validoEn){
		entidadBitemporalService.saveEndorsementBitemporalEntity(entidadBitemporal, validoEn);

	}*/
	
	@Override
	
	public  ControlEndosoCot guardaControlEndosoCotizacion(BitemporalCotizacion cotizacion, Short claveEstatus,Date fechaInicioVigencia){
		return guardaControlEndosoCotizacion(cotizacion, claveEstatus, fechaInicioVigencia, null);
	}
	
	protected  ControlEndosoCot guardaControlEndosoCotizacion(BitemporalCotizacion cotizacion, Short claveEstatus,
																Date fechaInicioVigencia, Long idCancela) {
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		//Giuardo registro historico en tabla control endosos
		return saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(claveEstatus),fechaInicioVigencia, idCancela, null);
	}
	
	protected ControlEndosoCot saveControlEndosoCot(SolicitudDTO solicitudDTO, BitemporalCotizacion cotizacion, 
			Long claveEstatus, Date fechaInicioVigencia) {
		return saveControlEndosoCot(solicitudDTO, cotizacion, claveEstatus, fechaInicioVigencia, null, null);
	}
	
	protected ControlEndosoCot saveControlEndosoCot(SolicitudDTO solicitudDTO, BitemporalCotizacion cotizacion, 
			Long claveEstatus, Date fechaInicioVigencia, Long idCancela, BigDecimal primaTotalIgualar) {
		LogDeMidasInterfaz.log("Entrando a saveControlEndosoCot", Level.INFO, null);
		
		ControlEndosoCot cecSaved = null;
		
		//Valida que no exista un controlendoso en proceso
    	Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", cotizacion.getContinuity().getNumero().longValue());
		properties.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByPropertiesWithOrder(ControlEndosoCot.class, properties, "id DESC");
		
		if(controlEndosoCotList == null || controlEndosoCotList.isEmpty()){
	    	ControlEndosoCot cecToSave = new ControlEndosoCot();
			cecToSave.setCotizacionId(cotizacion.getContinuity().getNumero().longValue());
			cecToSave.setClaveEstatusCot(claveEstatus);
			cecToSave.setSolicitud(solicitudDTO);
			cecToSave.setValidFrom(fechaInicioVigencia);
			cecToSave.setIdCancela(idCancela);
			cecToSave.setPrimaTotalIgualar(primaTotalIgualar);
			LogDeMidasInterfaz.log("cecToSave => " + cecToSave, Level.INFO, null);
			
			cecSaved = entidadService.save(cecToSave);
		}else{
			cecSaved = controlEndosoCotList.get(0);
		}
		
		LogDeMidasInterfaz.log("cecSaved => " + cecSaved, Level.INFO, null);
		LogDeMidasInterfaz.log("Saliendo de saveControlEndosoCot", Level.INFO, null);
		return   cecSaved;
}
   
    
    @Override
	public void guardaCotizacionInclusionTexto(String textos, BitemporalCotizacion cotizacion, DateTime validoEn) {
    	
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		if (!cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {			
			ControlEndosoCot controlEndosoCot = saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO), cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
			entidadBitemporalService.saveInProcess(cotizacion, TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
			if(controlEndosoCot!=null){
				movimientoEndosoBitemporalService.calcular(controlEndosoCot);
			}
		}	
		Collection<BitemporalTexAdicionalCot> biTextosCotizacionInProcess = getLstBiTexAdicionalCot(cotizacion.getContinuity().getId(), validoEn);
		for (BitemporalTexAdicionalCot biTextoAdicional : biTextosCotizacionInProcess) {
			entidadBitemporalService.remove(biTextoAdicional, validoEn);
		}
		
		List<BitemporalTexAdicionalCot> biTextosCotizacion = prepareEntidadBitemporalEndosoTexto(textos, validoEn);
			
		for (BitemporalTexAdicionalCot biTexAdicional : biTextosCotizacion) {
			BitemporalTexAdicionalCot biTemp = prepareEntidadBitemporalEndoso(BitemporalTexAdicionalCot.class, 
					biTexAdicional.getContinuity().getId(), 
					cotizacion.getContinuity().getId(), validoEn);
			
			biTexAdicional.getContinuity().setParentContinuity(biTemp.getContinuity().getParentContinuity());
			guardarEntidadBitemporalEndoso(biTexAdicional, validoEn);
		}
	}
    
    @Override
	public void guardaCotizacionInclusionAnexo(BitemporalCotizacion cotizacion, DateTime validoEn) {
    	
    	SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		if (!cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {			
			ControlEndosoCot controlEndosoCot = saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO), cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
			entidadBitemporalService.saveInProcess(cotizacion, validoEn);
			if(controlEndosoCot!=null){
				movimientoEndosoBitemporalService.calcular(controlEndosoCot);
			}
		}
	}
    
    
   
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardaCotizacionEndosoBajaInciso(
			String[] continuitiesIds, BitemporalCotizacion cotizacion) 
    {    	
    	validaCotizacionConPrima(cotizacion, continuitiesIds);
    	
    	validaBajaIncisos(cotizacion, continuitiesIds);
    	  
    	//Se agrega validacion para Siniestros, endosos PT
    	if(cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue() != SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL)
    	{
    		guardaCotizacionEndoso(cotizacion);      		
    	}    	  	
    	
    	if(continuitiesIds != null)
    	{
    		List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>();
        	for(String idString:continuitiesIds)
    		{
    			BitemporalInciso bitemporalInciso = new BitemporalInciso();			
    			bitemporalInciso = incisoService.getInciso(cotizacion.getContinuity().getId(), Long.valueOf(idString), TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
    			listaIncisosCotizacion.add(bitemporalInciso);			
    		}
        	
        	for(BitemporalInciso bitemporalInciso : listaIncisosCotizacion)
        	{
        		entidadBitemporalService.remove(bitemporalInciso, TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));    		
        	}    		
    	}
	}
    
    @Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardaCotizacionEndosoExclusionTexto(String[] continuitiesIds, BitemporalCotizacion cotizacion) 
    {    	 	
    	
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
    	if(continuitiesIds != null)
    	{
    		List<BitemporalTexAdicionalCot> listaTextosAdicionales = new ArrayList<BitemporalTexAdicionalCot>();
        	for(String idString:continuitiesIds)
    		{
        		BitemporalTexAdicionalCot bitemporalTexAdicional = new BitemporalTexAdicionalCot();			
        		bitemporalTexAdicional = entidadBitemporalService.getInProcessByKey(TexAdicionalCotContinuity.class, Long.valueOf(idString), TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
    			listaTextosAdicionales.add(bitemporalTexAdicional);			
    		}
        	
        	for(BitemporalTexAdicionalCot bitemporalTexAdicional : listaTextosAdicionales)
        	{
        		entidadBitemporalService.remove(bitemporalTexAdicional, TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));    		
        	}    		
    	}
    	
		if (!cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {			
			ControlEndosoCot controlEndosoCot = saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO), cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
			entidadBitemporalService.saveInProcess(cotizacion, TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
			if(controlEndosoCot!=null){
				movimientoEndosoBitemporalService.calcular(controlEndosoCot);
			}
		}else{
			movimientoEndosoBitemporalService.calcular(cotizacion);
		}
	}
    
    
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public ControlEndosoCot getControlEndosoCotEAP(BigDecimal cotizacionId, Short numeroEndoso) {
    	
    	Map<String,Object> properties = new HashMap<String,Object>();
		
		properties.put("cotizacionId", cotizacionId);
		properties.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		properties.put("solicitud.claveTipoEndoso", SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA);
		
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByPropertiesWithOrder(ControlEndosoCot.class, properties, "id DESC");		
		
		if(controlEndosoCotList != null && !controlEndosoCotList.isEmpty()) {
			return controlEndosoCotList.get(0);
		}
    	
    	return null;
    	
    }
    
    
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void guardaCotizacionAjustePrima(BigDecimal cotizacionId, BigDecimal idToPoliza, ControlEndosoCot controlEndosoCotEAP, Short numeroEndoso, 
    		Integer tipoFormaPago) {
    	
    	DateTime validoEn = null;
    	
    	if (numeroEndoso > 0) {
    		ControlEndosoCot controlEndosoCot = getControlFromEndoso(cotizacionId.longValue(), numeroEndoso);
    		validoEn = TimeUtils.getDateTime(controlEndosoCot.getValidFrom());
    	} else {
    		//Fecha inicio vigencia de la poliza
    		EndosoId endosoId = new EndosoId(idToPoliza, numeroEndoso);
    		EndosoDTO endosoCero = entidadService.findById(EndosoDTO.class, endosoId);
    		validoEn = TimeUtils.getDateTime(endosoCero.getValidFrom());
    	}
    	
    	BitemporalCotizacion cotizacion = entidadBitemporalService.getByBusinessKey
    	 	(CotizacionContinuity.class, "numero", cotizacionId, validoEn);
    	 
    	SolicitudDTO solicitud = prepareSolicitudDTO(cotizacion, idToPoliza, 
    			 										new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA), validoEn, null);
    	
    	//Cotizacion en su estatus actual
    	cotizacion = entidadBitemporalService.getByBusinessKey
 			(CotizacionContinuity.class, "numero", cotizacionId);
    	if(cotizacion == null){
    		//Si la poliza es a futuro tomar inicio de vigencia
    		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, cotizacionId);
        	cotizacion = entidadBitemporalService.getByBusinessKey
 			(CotizacionContinuity.class, "numero", cotizacionId, TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia()));    		
    	}
    	
    	//1: Igual poliza, 2: Anual
    	if (tipoFormaPago != null && tipoFormaPago == 2) {
    		controlEndosoCotEAP.setPorcentajePagoFraccionado(movimientoEndosoBitemporalService.getPctFormaPago(
    				1, 
					cotizacion.getValue().getMoneda().getIdTcMoneda()));
    	} else {
    		controlEndosoCotEAP.setPorcentajePagoFraccionado(movimientoEndosoBitemporalService.getPctFormaPago(
    				cotizacion.getValue().getFormaPago().getIdFormaPago(), 
					cotizacion.getValue().getMoneda().getIdTcMoneda()));
    	}
    	
    	controlEndosoCotEAP.setSolicitud(solicitud);
    	controlEndosoCotEAP.setNumeroEndosoAjusta(numeroEndoso);
    	controlEndosoCotEAP.setCotizacionId(cotizacionId.longValue());
    	controlEndosoCotEAP.setValidFrom(validoEn.toDate());
    	controlEndosoCotEAP.setClaveEstatusCot(Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO));
    	
    	controlEndosoCotEAP = entidadService.save(controlEndosoCotEAP);
   	
	}
    
    
    @Override
    @TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void guardaCotizacionCancelacionEndoso(BitemporalCotizacion cotizacion, DateTime validoEn, BigDecimal idToPoliza,
    														Short numeroEndoso, String accionEndoso, Short motivoEndoso) {
    	
    	 ControlEndosoCot controlEndosoCot = getControlFromEndoso(cotizacion.getEntidadContinuity().getNumero().longValue(), numeroEndoso);
    	 SolicitudDTO solicitud = prepareSolicitudDTO(cotizacion, idToPoliza, 
    			 										new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO), validoEn, motivoEndoso);
    	 cancelaEndoso(cotizacion, controlEndosoCot, solicitud, validoEn, accionEndoso);
	}
    
    private void cancelaEndoso(BitemporalCotizacion biCotizacion, ControlEndosoCot controlEndosoCot, SolicitudDTO solicitud,
    											DateTime validoEn, String accionEndoso) {
    	
    	
    	//Si es una cancelacion de EAP o rehabilitacion de EAP no se revierte la bitemporalidad
    	boolean cancelaRehabilitaEAP = false;
    	if (controlEndosoCot.getIdCancela() != null) {
    		ControlEndosoCot controlEndosoCotCancelado = entidadService.findById(ControlEndosoCot.class, controlEndosoCot.getIdCancela());
    		if (controlEndosoCotCancelado.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA) {
    			cancelaRehabilitaEAP = true;
    		}
    	} else if (controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA) {
    		cancelaRehabilitaEAP = true;
    	}
    	
    	if (!cancelaRehabilitaEAP) {
    		
	    	biCotizacion.getEntidadContinuity().getBitemporalProperty().revert(new DateTime(controlEndosoCot.getRecordFrom()), true);
	
	    	if (validoEn.toDate().compareTo(controlEndosoCot.getValidFrom()) > 0) {
	
	    		
	    		setBitemporalCancelacionEndoso(biCotizacion, controlEndosoCot.getValidFrom(), validoEn.toDate());
	    	}
	    	    	
	    	entidadContinuityDao.update(biCotizacion.getEntidadContinuity());   
    	}
    	
		if (accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot())) {
			actualizaControlEndosoCot(new BigDecimal(biCotizacion.getEntidadContinuity().getNumero()),
    										(long) CotizacionDTO.ESTATUS_COT_EN_PROCESO, validoEn.toDate(), null, controlEndosoCot.getId());
    		
    	} else {
    		
    		saveControlEndosoCot(solicitud, biCotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO), 
    																		validoEn.toDate(), controlEndosoCot.getId(), null);    		
    	}		
			
    }
	
    
    @SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardaCotizacionEndosoRehabilitacionInciso(
			String[] continuitiesIds, List<BitemporalInciso> listaIncisosCancelados, BitemporalCotizacion cotizacion) {
    	DateTime validoHasta = null;
    	DateTime recordFrom = null;
    	BitemporalInciso bitemporalARehabilitar = null;
    	List<BitemporalInciso> incisos = null;
    	
        guardaCotizacionEndoso(cotizacion);    	
    	
    	if(continuitiesIds != null)
    	{
    		List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>();
        	for(String idString:continuitiesIds)
    		{    			
    			Iterator itListaIncisosCancelados = listaIncisosCancelados.iterator();
    			while(itListaIncisosCancelados.hasNext())
    			{
    				BitemporalInciso bitemporalInciso = (BitemporalInciso) itListaIncisosCancelados.next();
    				if(bitemporalInciso.getEntidadContinuity().getId().compareTo(Long.valueOf(idString)) == 0)
    				{
    					listaIncisosCotizacion.add(bitemporalInciso);
    					break;
    				}    				
    			}    						
    		}
        	
        	for(BitemporalInciso bitemporalInciso : listaIncisosCotizacion) {        		
        		
        		List <IntervalWrapper> intervalos = bitemporalInciso.getEntidadContinuity().getBitemporalProperty().getMergedValidityIntervals();
        		
        		if (intervalos != null && !intervalos.isEmpty()) {
        			validoHasta = intervalos.get(0).getInterval().getEnd();
        		} else {
        			validoHasta = bitemporalInciso.getEntidadContinuity().getBitemporalProperty().getData()
        				.iterator().next().getValidityInterval().getInterval().getStart();
        		}
        		
        		incisos = bitemporalInciso.getEntidadContinuity().getBitemporalProperty().getEvolution(validoHasta); 
        		
        		Collections.sort(incisos, new Comparator(){
        			public int compare(Object o1, Object o2) {
        				return ((BitemporalInciso)o1).getId().compareTo(((BitemporalInciso)o2).getId());
        			}
        		});
        		
        		bitemporalARehabilitar = incisos.get(incisos.size()-1);
        		
        		if (bitemporalARehabilitar.getRecordInterval().getInterval().getEnd().equals(TimeUtils.endOfTime())) {
        			recordFrom = bitemporalARehabilitar.getRecordInterval().getInterval().getStart();
        		} else {
        			recordFrom = bitemporalARehabilitar.getRecordInterval().getInterval().getEnd();
        		}
        		
        		
        		Collection<BitemporalInciso> bitemporalesCancelados = entidadBitemporalService.obtenerCancelados(BitemporalInciso.class, bitemporalInciso.getContinuity().getId(),
        				TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()),
        				recordFrom);
        		
        		Boolean found = false;
        		for (BitemporalInciso bitemporalCancelado : bitemporalesCancelados) {
        			
        			if (bitemporalCancelado.getId().equals(bitemporalARehabilitar.getId())) {
        				bitemporalARehabilitar = bitemporalCancelado;
        				found = true;
        				break;
        			}
        		}
        		if (!found) {
        			 bitemporalARehabilitar = entidadBitemporalService.getByKey(IncisoContinuity.class, bitemporalARehabilitar.getContinuity().getId(), bitemporalARehabilitar.getValidityInterval().getInterval().getStart());	
        		}
        		
        		
        		bitemporalARehabilitar.getEntidadContinuity().getBitemporalProperty().revert(recordFrom, true);
        		
        		//En caso de que la fecha de Baja de inciso sea diferente a la de su rehabilitacion (Se deja el codigo pero por negocio 
        		//esto NUNCA deberia pasar)
        		if(validoHasta.toDate().compareTo(org.apache.commons.lang.time.DateUtils.truncate(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()
    					,Calendar.DAY_OF_MONTH)) != 0) {
    				// Remover espacio entre baja del inciso y fecha Inicio Vigencia de Rehabilitacion del Endoso
    				entidadBitemporalService.remove(bitemporalARehabilitar, validoHasta,
    						TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
  
    			}
        		
        		entidadContinuityDao.update(bitemporalARehabilitar.getEntidadContinuity());
        	}    		
    	}		
	}	
    @Override
	public void guardaCotizacionEndosoCambioDatos(BitemporalCotizacion cotizacion) {    	
    	
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
		if(!cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess())
		{
			entidadBitemporalService.saveInProcess(cotizacion,TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
		}
		else
		{			
			entidadContinuityDao.update(cotizacion.getContinuity());
		}
		ControlEndosoCot controlEndoso = obtenerControlEndosoCotCotizacion(new BigDecimal(cotizacion.getContinuity().getNumero()));
		if(controlEndoso == null || !controlEndoso.getClaveEstatusCot().equals(new Long(CotizacionDTO.ESTATUS_COT_EN_PROCESO))){
			controlEndoso = saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
		}
		movimientoEndosoBitemporalService.calcular(controlEndoso);
	}
	
	@Override
	public BitemporalCotizacion prepareCotizacionEndoso(Long idCotizacion, DateTime validoEn) {
		
		BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, idCotizacion, 
				validoEn);
		BitemporalCotizacion cotizacionCopiaEditable = (BitemporalCotizacion) cotizacion.copyWith(new IntervalWrapper(TimeUtils.from(validoEn)), true);
		cotizacionCopiaEditable.setId(null);
		
		return cotizacionCopiaEditable;
	}
	
	
	public BitemporalCotizacion prepareCotizacionEndosoEV(BigDecimal polizaId,
			String accionEndoso) {
		
		if (polizaId == null) {
			throw new IllegalArgumentException("[initCotizacionEndoso] El id de la póliza es nula.");
		}
		//se obtiene la poliza
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
				polizaId);
		if (poliza == null) {
			return null;
		}
		
		// se obtiene la fecha de creación de la póliza para obtener la cotización
		Date fechaBusquedaCot = (Date)obtenerFechaBusquedaCotizacion(poliza,accionEndoso)[0];
		
		BitemporalCotizacion bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
				CotizacionContinuity.BUSINESS_KEY_NAME, poliza.getCotizacionDTO().getIdToCotizacion(),
				TimeUtils.getDateTime(fechaBusquedaCot));

		
//		BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, idCotizacion, 
//				validoEn);
//		BitemporalCotizacion cotizacionCopiaEditable = (BitemporalCotizacion) cotizacion.copyWith(new IntervalWrapper(TimeUtils.from(validoEn)), true);
//		cotizacionCopiaEditable.setId(null);
		
		return bitemporalCotizacion;
	}
	
	@Override
	public BitemporalCotizacion prepareCotizacionEndosoCancelacion(PolizaDTO poliza, Short tipoEndoso, 
			DateTime validoEn, String accionEndoso, Long numeroEndoso, short motivoEndoso, short migrada)
	{
		
		EndosoDTO endoso = getEndoso(poliza, numeroEndoso);
		
	   	aplicarValidaciones(endoso, validoEn.toDate(), tipoEndoso, motivoEndoso, migrada);
	   	
	   	BitemporalCotizacion cotizacion = null;
	   	
	   	if (accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot())) {
		   	cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
		   											CotizacionContinuity.BUSINESS_KEY_NAME, poliza.getCotizacionDTO().getIdToCotizacion(),
		   											validoEn);
	   	} else {
	   		cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
													CotizacionContinuity.BUSINESS_KEY_NAME,  poliza.getCotizacionDTO().getIdToCotizacion(), 
													validoEn);
	   	}
	   	
	   	if(cotizacion == null && tipoEndoso.equals(SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO)){
	   		cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME,  poliza.getCotizacionDTO().getIdToCotizacion(), 
					TimeUtils.getDateTime(Utilerias.agregaDiasAFecha(validoEn.toDate(), -1)));	   		
	   	}

		//Se valida que la cotización no sea nula
		validaCotizacionNoNula(cotizacion, poliza);
			
		//Si no tiene registros de cotizacion en proceso se procede a limpiar cualquier otro registro en proceso 
		//dentro de la estructura de la bitemporalidad
		if (cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
		}
					
		SolicitudDTO nuevaSolicitud = new SolicitudDTO();
		BeanUtils.copyProperties(cotizacion.getValue().getSolicitud(), nuevaSolicitud);
		
//		nuevaSolicitud.setIdToSolicitud(null);
//		nuevaSolicitud.setClaveTipoSolicitud(TIPO_SOLICITUD);
//		nuevaSolicitud.setFechaInicioVigenciaEndoso(validoEn.toDate());
//		nuevaSolicitud.setIdToPolizaEndosada(poliza.getIdToPoliza());
//		nuevaSolicitud.setClaveTipoEndoso(new BigDecimal(tipoEndoso));
//		nuevaSolicitud.setClaveMotivoEndoso(SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO);
//		entidadService.save(nuevaSolicitud);
//		cotizacion.getValue().setSolicitud(nuevaSolicitud);
//		cotizacion.getValue().setNumeroEndoso(this.getNumeroEndoso(poliza.getIdToPoliza(), "%06d"));
		
		BitemporalCotizacion cotizacionCopiaEditable = (BitemporalCotizacion) cotizacion.copyWith(new IntervalWrapper(TimeUtils.from(validoEn)), true);
		cotizacionCopiaEditable.setId(null);
		return cotizacionCopiaEditable;
	}
	
	@Override
	public BitemporalCotizacion prepareCotizacionEndosoCancelacion(PolizaDTO poliza, Short tipoEndoso, 
																	DateTime validoEn, String accionEndoso, Long numeroEndoso, short motivoEndoso) {
		return prepareCotizacionEndosoCancelacion( poliza, tipoEndoso, 
				validoEn,  accionEndoso,  numeroEndoso, motivoEndoso, EmisionPendiente.POLIZA_NO_MIGRADA);
		
	}
	
	
	private SolicitudDTO prepareSolicitudDTO(BitemporalCotizacion cotizacion, BigDecimal idToPoliza, 
																BigDecimal tipoEndoso, DateTime validoEn, Short motivoEndoso) {
		
		SolicitudDTO nuevaSolicitud = new SolicitudDTO();
		BeanUtils.copyProperties(cotizacion.getValue().getSolicitud(), nuevaSolicitud);
		
		nuevaSolicitud.setIdToSolicitud(null);
		nuevaSolicitud.setComentarioList(null);
		nuevaSolicitud.setClaveTipoSolicitud(TIPO_SOLICITUD);
		nuevaSolicitud.setFechaInicioVigenciaEndoso(validoEn.toDate());
		nuevaSolicitud.setIdToPolizaEndosada(idToPoliza);
		nuevaSolicitud.setClaveTipoEndoso(tipoEndoso);
		nuevaSolicitud.setClaveMotivoEndoso(motivoEndoso);
		
		try{
			Usuario usuario = usuarioService.getUsuarioActual();
			nuevaSolicitud.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
			nuevaSolicitud.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
			nuevaSolicitud.setFechaCreacion(new Date());
			nuevaSolicitud.setFechaModificacion(new Date());
		}catch(Exception e){
			
		}
		
		entidadService.save(nuevaSolicitud);
		
		return nuevaSolicitud;
	}
	
	
	
	@Override
	@SuppressWarnings("rawtypes")
	public <B extends EntidadBitemporal> B prepareEntidadBitemporalEndoso(
						Class<B> bitemporalEntityClass, Long entidadContinuityId, 
						Long parentContinuityId, DateTime validoEn) {
		
		return  entidadBitemporalService.prepareEndorsementBitemporalEntity(bitemporalEntityClass, entidadContinuityId, parentContinuityId, validoEn);
	}
	/*public EntidadBitemporal prepareEntidadBitemporalEndoso(Class<EntidadBitemporal> bitemporalEntityClass, Long entidadContinuityId, 
			Long parentContinuityId, DateTime validoEn) {
		return entidadBitemporalService.prepareEndorsementBitemporalEntity(bitemporalEntityClass, entidadContinuityId, parentContinuityId, validoEn);
	}*/

	@Override
	public BitemporalCotizacion getInitCotizacionEndoso(BigDecimal polizaId,
			Date fechaInicioVigencia, short tipoEndoso, short motivoEndoso,String accionEndoso){
		return initCotizacionEndoso(polizaId, fechaInicioVigencia, tipoEndoso,motivoEndoso, accionEndoso);
	}
	
	@Override
	public BitemporalCotizacion getInitCotizacionEndoso(BigDecimal polizaId,
			Date fechaInicioVigencia, short tipoEndoso, short motivoEndoso,String accionEndoso, short migrada){
		return initCotizacionEndoso(polizaId, fechaInicioVigencia, tipoEndoso,motivoEndoso, accionEndoso, migrada);
	}
	
	protected BitemporalCotizacion initCotizacionEndoso(BigDecimal polizaId,
			Date fechaInicioVigencia, short tipoEndoso,short motivoEndoso, String accionEndoso) {
		
		return initCotizacionEndoso(polizaId,
				fechaInicioVigencia, tipoEndoso, motivoEndoso, accionEndoso, EmisionPendiente.POLIZA_NO_MIGRADA);		
	}
	
	protected BitemporalCotizacion initCotizacionEndoso(BigDecimal polizaId,
			Date fechaInicioVigencia, short tipoEndoso,short motivoEndoso, String accionEndoso, short migrada) {
		if (polizaId == null) {
			throw new IllegalArgumentException("[initCotizacionEndoso] El id de la póliza es nula.");
		}
		
		//se obtiene la poliza
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
				polizaId);
		if (poliza == null) {
			return null;
		}
		return initCotizacionEndoso(poliza, fechaInicioVigencia, tipoEndoso, motivoEndoso, accionEndoso, migrada);
	}
	
	protected BitemporalCotizacion initCotizacionEndoso(PolizaDTO poliza,
			Date fechaInicioVigencia, short tipoEndoso,short motivoEndoso, String accionEndoso) {
		
		return initCotizacionEndoso(poliza,
				fechaInicioVigencia, tipoEndoso, motivoEndoso, accionEndoso, EmisionPendiente.POLIZA_NO_MIGRADA);
		/*BitemporalCotizacion cotizacion=null;
		if (poliza == null) {
			return null;
		}
		
		DateTime dateTimeFechaIniVigencia = TimeUtils.getDateTime(fechaInicioVigencia);
		Object obj = new Object();
		
		BigDecimal polizaId = poliza.getIdToPoliza();
		EndosoDTO endoso = getEndoso(poliza, (short)0);
		//Si existe una cotizacion en proceso la retorno para edicion
		if(accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot()) || accionEndoso.equals(TipoAccionDTO.getConsultarEndosoCot())){
			
			if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA) {
				cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, poliza
						.getCotizacionDTO().getIdToCotizacion(), dateTimeFechaIniVigencia);
			} else {
				cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, poliza
						.getCotizacionDTO().getIdToCotizacion(), dateTimeFechaIniVigencia);
			}
			
			
			if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA && motivoEndoso != SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO) {
				obj = validaPagosRealizadosFront(endoso.getId().getIdToPoliza(), endoso.getId().getNumeroEndoso(), null, fechaInicioVigencia, 
						TipoEmision.CANC_POL_AUTOS, motivoEndoso);
			} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO && motivoEndoso != SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO) {
				obj = validaEndosoCancelacionEndoso(endoso, fechaInicioVigencia, motivoEndoso);
			}
			
			cotizacion.getValue().setNumeroEndoso(this.getNumeroEndoso(polizaId, "%06d"));
		}else{
			//aplica las validaciones para 
			if(TipoAccionDTO.getAltaIncisoEndosoCot().equals(accionEndoso)){
				
				cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, poliza
						.getCotizacionDTO().getIdToCotizacion(), dateTimeFechaIniVigencia);
				if(cotizacion!=null && cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()){
					cotizacion.getValue().setNumeroEndoso(this.getNumeroEndoso(polizaId, "%06d"));
					return cotizacion;
				}else{
					aplicarValidaciones(endoso, fechaInicioVigencia, tipoEndoso, motivoEndoso);
				}
			}else{
				obj = aplicarValidaciones(endoso, fechaInicioVigencia, tipoEndoso, motivoEndoso);
				cotizacion = encuentraBitemporalCotizacion(poliza.getCotizacionDTO().getIdToCotizacion(), dateTimeFechaIniVigencia, tipoEndoso, accionEndoso);
				//Se valida que la cotización no sea nula.s
				validaCotizacionNoNula(cotizacion, poliza);
				//Si la accion Endoso es diferente a altaInciso procede a borrar toda informacion de la cotizacion en proceso
				entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
			}
		
			SolicitudDTO nuevaSolicitud = new SolicitudDTO();
			BeanUtils.copyProperties(cotizacion.getValue().getSolicitud(), nuevaSolicitud);

			nuevaSolicitud.setIdToSolicitud(null);
			nuevaSolicitud.setClaveTipoSolicitud(TIPO_SOLICITUD);
			nuevaSolicitud.setFechaInicioVigenciaEndoso(fechaInicioVigencia);
			nuevaSolicitud.setIdToPolizaEndosada(poliza.getIdToPoliza());
			nuevaSolicitud.setClaveTipoEndoso(new BigDecimal(tipoEndoso));
			nuevaSolicitud.setClaveMotivoEndoso(motivoEndoso);
			nuevaSolicitud.setComentarioList(null);
			entidadService.save(nuevaSolicitud);
			cotizacion.getValue().setSolicitud(nuevaSolicitud);
			cotizacion.getValue().setNumeroEndoso(this.getNumeroEndoso(polizaId, "%06d"));
		}
		if(obj != null && obj instanceof BigDecimal){
			cotizacion.getValue().setImporteNotaCredito((BigDecimal)obj);
		}
		if (cotizacion.getValue().getNegocioDerechoEndosoId() == null) {
			for (NegocioDerechoEndoso negocioDerechoEndoso : cotizacion.getValue().getSolicitud().getNegocio().getNegocioDerechoEndosos()) {
				if (negocioDerechoEndoso.getClaveDefault()) {
					cotizacion.getValue().setNegocioDerechoEndosoId(negocioDerechoEndoso.getIdToNegDerechoEndoso());
					cotizacion.getValue().setValorDerechosUsuario(negocioDerechoEndoso.getImporteDerecho());
					break;
				}
			}
		}else{
			NegocioDerechoEndoso negocioDerechoEndoso = entidadService.findById(NegocioDerechoEndoso.class, cotizacion.getValue().getNegocioDerechoEndosoId());
			cotizacion.getValue().setValorDerechosUsuario(negocioDerechoEndoso.getImporteDerecho());
		}
				
		return cotizacion;*/
	}
	
	protected BitemporalCotizacion initCotizacionEndoso(PolizaDTO poliza,
			Date fechaInicioVigencia, short tipoEndoso,short motivoEndoso, String accionEndoso, short migrada) {
		BitemporalCotizacion cotizacion=null;
		if (poliza == null) {
			return null;
		}
		
		DateTime dateTimeFechaIniVigencia = TimeUtils.getDateTime(fechaInicioVigencia);
		Object obj = new Object();
		
		BigDecimal polizaId = poliza.getIdToPoliza();
		EndosoDTO endoso = getEndoso(poliza, new Long(0));
		//Si existe una cotizacion en proceso la retorno para edicion
		if(accionEndoso.equals(TipoAccionDTO.getEditarEndosoCot()) || accionEndoso.equals(TipoAccionDTO.getConsultarEndosoCot())){
			
			if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA) {
				cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, poliza
						.getCotizacionDTO().getIdToCotizacion(), dateTimeFechaIniVigencia);
			} else {
				cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, poliza
						.getCotizacionDTO().getIdToCotizacion(), dateTimeFechaIniVigencia);
			}
			if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA && 
					convierteMotivoEndosoSeycos(motivoEndoso) != SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO) {
				obj = validaPagosRealizadosFront(endoso.getId().getIdToPoliza(), endoso.getId().getNumeroEndoso().longValue(), null, fechaInicioVigencia, 
						TipoEmision.CANC_POL_AUTOS, motivoEndoso);
			} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO && 
					convierteMotivoEndosoSeycos(motivoEndoso)  != SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO) {
				obj = validaEndosoCancelacionEndoso(endoso, fechaInicioVigencia, motivoEndoso);
			}
			cotizacion.getValue().setNumeroEndoso(this.getNumeroEndoso(polizaId, "%06d"));
		}else{
			
			//aplica las validaciones para 
			if(TipoAccionDTO.getAltaIncisoEndosoCot().equals(accionEndoso)){
				
				cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, poliza
						.getCotizacionDTO().getIdToCotizacion(), dateTimeFechaIniVigencia);
				if(cotizacion!=null && cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()){
					cotizacion.getValue().setNumeroEndoso(this.getNumeroEndoso(polizaId, "%06d"));
					return cotizacion;
				}else{
					aplicarValidaciones(endoso, fechaInicioVigencia, tipoEndoso, motivoEndoso, migrada);
				}
			}else{
				obj = aplicarValidaciones(endoso, fechaInicioVigencia, tipoEndoso, motivoEndoso, migrada);
				cotizacion = encuentraBitemporalCotizacion(poliza.getCotizacionDTO().getIdToCotizacion(), dateTimeFechaIniVigencia, tipoEndoso, accionEndoso);
				//Se valida que la cotización no sea nula.s
				validaCotizacionNoNula(cotizacion, poliza);
				//Si la accion Endoso es diferente a altaInciso procede a borrar toda informacion de la cotizacion en proceso
				entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
			}
		
			SolicitudDTO nuevaSolicitud = new SolicitudDTO();
			BeanUtils.copyProperties(cotizacion.getValue().getSolicitud(), nuevaSolicitud);

			nuevaSolicitud.setIdToSolicitud(null);
			nuevaSolicitud.setClaveTipoSolicitud(TIPO_SOLICITUD);
			nuevaSolicitud.setFechaInicioVigenciaEndoso(fechaInicioVigencia);
			nuevaSolicitud.setIdToPolizaEndosada(poliza.getIdToPoliza());
			nuevaSolicitud.setClaveTipoEndoso(new BigDecimal(tipoEndoso));
			nuevaSolicitud.setClaveMotivoEndoso(motivoEndoso);
			nuevaSolicitud.setComentarioList(null);
			nuevaSolicitud.setDocumentoDigitalSolicitudDTOs(null);
			try{
				
				if(usuarioService.getUsuarioActual() != null)
				{
					Usuario usuario = usuarioService.getUsuarioActual();
					nuevaSolicitud.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
					nuevaSolicitud.setCodigoUsuarioModificacion(usuario.getNombreUsuario());					
				}else
				{
					nuevaSolicitud.setCodigoUsuarioCreacion("M2ADMINI");
					nuevaSolicitud.setCodigoUsuarioModificacion("M2ADMINI");					
				}				
				
				nuevaSolicitud.setFechaCreacion(new Date());
				nuevaSolicitud.setFechaModificacion(new Date());
			}catch(Exception e){
				
				LogDeMidasEJB3.log("Error al intentar asignar usuario y fecha creacion/modificacion del registro", Level.INFO,e);
				
			}
			
			entidadService.save(nuevaSolicitud);
			cotizacion.getValue().setSolicitud(nuevaSolicitud);
			cotizacion.getValue().setNumeroEndoso(this.getNumeroEndoso(polizaId, "%06d"));
		}
		
		if(obj != null && obj instanceof BigDecimal){
			cotizacion.getValue().setImporteNotaCredito((BigDecimal)obj);
		}
		
		if (cotizacion.getValue().getNegocioDerechoEndosoId() == null) {
			try{
				NegocioDerechoEndoso negocioDerechoEndoso = negocioDerechosService.obtenerDerechosEndosoDefault(cotizacion.getValue().getSolicitud());
				if(negocioDerechoEndoso != null){
					cotizacion.getValue().setNegocioDerechoEndosoId(negocioDerechoEndoso.getIdToNegDerechoEndoso());
					cotizacion.getValue().setValorDerechosUsuario(negocioDerechoEndoso.getImporteDerecho());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			NegocioDerechoEndoso negocioDerechoEndoso = entidadService.findById(NegocioDerechoEndoso.class, cotizacion.getValue().getNegocioDerechoEndosoId());
			cotizacion.getValue().setValorDerechosUsuario(negocioDerechoEndoso.getImporteDerecho());
		}
				
		return cotizacion;
	}
	

	protected void initCotizacionEndosoCancelacion(BigDecimal polizaId) {
		if (polizaId == null) {
			throw new IllegalArgumentException("[initCotizacionEndoso] El id de la póliza es nula.");
		}		
		//se obtiene la poliza
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,	polizaId);
		if (poliza == null) {
			throw new IllegalArgumentException("[initCotizacionEndoso] El id de la póliza es nula.");
		}
		
		validaStatusPoliza(polizaId);
			
		validaNoTieneRecordsEnProceso(polizaId);		
	
	}
	
	private void validaCotizacionNoNula(BitemporalCotizacion cotizacion, PolizaDTO poliza){
		if(cotizacion == null) {
			Object[] values = new Object[]{poliza.getCotizacionDTO().getIdToCotizacion()};
			throw new NegocioEJBExeption("NE_10", values);
		}
	}
	
	@Override
	public BitemporalCotizacion encuentraBitemporalCotizacion(BigDecimal cotizacionId, DateTime dateTimeFechaIniVigencia, 
			short tipoEndoso, String accionEndoso) {
		
		BitemporalCotizacion cotizacion= null;
		
		if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA) {
			ControlEndosoCot controlEndoso = getControlEndosoCancelacion(cotizacionId, accionEndoso);
			
			CotizacionContinuity cotizacionContinuity = entidadContinuityDao
			.findByProperty(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, 
					cotizacionId).iterator().next();
			
			cotizacion = entidadBitemporalService.getNew(BitemporalCotizacion.class, null);
			cotizacion.setEntidadContinuity(cotizacionContinuity);
			cotizacion.getValue().setSolicitud(controlEndoso.getSolicitud());
		} else {
			//Para el caso de Endoso Extension que tiene que restar 1 dia.
			if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA) {
				dateTimeFechaIniVigencia = dateTimeFechaIniVigencia.minusDays(1);
			
			}
			
			cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, 
					cotizacionId, dateTimeFechaIniVigencia);
		}
		
		return cotizacion;
	}
	/**
	 * Realiza las validaciones generales: Estatus de la póliza, dias retroctivo-diferimiento, valida fecha este entre inicio-fin y si es cancelable
	 * o no la póliza/endoso.
	 * @param endoso
	 * @param fechaInicioVigencia
	 * @param tipoEndoso
	 */
	protected Object aplicarValidaciones(EndosoDTO endoso, Date fechaInicioVigencia, short tipoEndoso, short motivoEndoso){
		return aplicarValidaciones(endoso, fechaInicioVigencia, tipoEndoso, motivoEndoso, EmisionPendiente.POLIZA_NO_MIGRADA);		
	}
	
	protected Object aplicarValidaciones(EndosoDTO endoso, Date fechaInicioVigencia, short tipoEndoso, short motivoEndoso, short migrada){
		Object obj = new Object();
		
		// valida el estatus de la poliza de acuerdo al endoso solicitado
		if(tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA){
			validaStatusPoliza(endoso.getId().getIdToPoliza());
		}else{
			validaEsPolizaCancelada(endoso.getId().getIdToPoliza());
		}
		
		//Valida que no tenga records en proceso la cotización
		validaNoTieneRecordsEnProceso(endoso.getId().getIdToPoliza());
		
		// Valida los dias de retroactivadad y diferimiento
		if(tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA &&
				tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL &&
				tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL && 
				tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS &&
				convierteMotivoEndosoSeycos(motivoEndoso) != SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO && 
				migrada != EmisionPendiente.POLIZA_MIGRADA){
			
			boolean validarDiasRD = true;
			if(tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA ||
					tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO){
				validarDiasRD = validarDiasRetroactividadCANCFP(endoso.getId().getIdToPoliza());
			}
			if(validarDiasRD){
				validaDiasRetroactividadDiferimiento(endoso, fechaInicioVigencia);
			}
		}
		
		//valida que la fecha este entre inicio y fin de la vigencia de la poliza
		validaFechaEntreInicioFin(endoso, fechaInicioVigencia);
		
		//valida rehabilitacion de la poliza
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_POLIZA){
			validaFechaRehabilitacion(endoso, fechaInicioVigencia);
			validaRehabilitacionCambioIVA(endoso);
		}

		// se realiza la validación de si es cancelable.
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA && 
				convierteMotivoEndosoSeycos(motivoEndoso) != SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO) {
			obj = validaPagosRealizadosFront(endoso.getId().getIdToPoliza(), new Long(endoso.getId().getNumeroEndoso()), null, fechaInicioVigencia, 
					TipoEmision.CANC_POL_AUTOS, motivoEndoso);
		} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO && 
				convierteMotivoEndosoSeycos(motivoEndoso) != SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO) {
			obj = validaEndosoCancelacionEndoso(endoso, fechaInicioVigencia, motivoEndoso);
		} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO ) {
			MensajeDTO mensaje = cobranzaService.stpValidaPrimerRecNoPagado(endoso.getId().getIdToPoliza(), fechaInicioVigencia);
			if(mensaje != null && mensaje.getMensaje() != null && !mensaje.getMensaje().isEmpty()){
				throw new NegocioEJBExeption("_", mensaje.getMensaje());
			}
		}
		
		//se valida el atributo aplica flotilla para determinar si es posible o no un alta de inciso
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO || tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO 
				|| tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS) {
			validaAplicaFlotilla(endoso.getId().getIdToPoliza(), tipoEndoso);
		}
		
		//Si el tipo de endoso afecta prima, se valida que no sea Poliza Anexa
		if (tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS && 
				tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE &&
				tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_EXCLUSION_TEXTO &&
				tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO &&
				tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO) {
			validaPolizaAnexa(endoso);
		}
		
		//TODO si es extension de vigencia la fecha de solicitud debe ser un dia posterior a la fecha de fin de vigencia de poliza
		return obj;
	}

	/**
	 * Valida que no tenga records en proceso la cotización. En caso de tenerla manda una excepción NegocioEJBExeption
	 * @param polizaId
	 */
	protected void validaNoTieneRecordsEnProceso(BigDecimal polizaId) throws NegocioEJBExeption {
		
		ControlEndosoCot controlEndosoCot = obtenerControlEndosoCot(polizaId); 
		if(controlEndosoCot != null){
			// valida que no tenga records en proceso la cotización.
			if (controlEndosoCot.getClaveEstatusCot().shortValue() == 
				CotizacionDTO.ESTATUS_COT_EN_PROCESO ||
				controlEndosoCot.getClaveEstatusCot().shortValue() == 
					CotizacionDTO.ESTATUS_COT_TERMINADA) {
				Object[] values = new Object[]{polizaId};
				throw new NegocioEJBExeption("NE_6", values);
			}
		}
	}

	/**
	 * Valida el estatus de la poliza. Si es cancelada manda una exepción.
	 * @param endoso
	 */
	private void validaStatusPoliza(BigDecimal polizaId) {
		boolean lanzarMsg= false;
		
		PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, polizaId);	
		
		//se obtiene el último control endoso cot.
		ControlEndosoCot controlEndCot = obtenerControlEndosoCotiz(polizaId, CotizacionDTO.ESTATUS_COT_EMITIDA);
		
		//if (poliza.getEstatus().equals(PolizaDTO.Status.Cancelada.ordinal()+ "")) {
		if (controlEndCot != null) {			
			lanzarMsg = controlEndCot.getSolicitud().getClaveTipoEndoso().shortValue() == 
												SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA;			
		}
		// se evalua si se lanza el mensaje.
		if (lanzarMsg){ 
			Object[] pars = new Object[]{polizaDTO.getNumeroPoliza()};
			throw new NegocioEJBExeption("NE_1",pars);
		}
		
	}
	
	/**
	 * Valida los dias de Retroactividad y Diferimiento.  Diferimiento > abs((fechaInicioVigencia - Time)) < Retroactivo
	 * @param endoso
	 * @param fechaInicioVigencia
	 */
	private void validaDiasRetroactividadDiferimiento(EndosoDTO endoso, Date fechaInicioVigencia){
		
		if (endoso.getCotizacionDTO() == null) {
			throw new RuntimeException("La cotización de la póliza " + endoso.getId().getIdToPoliza() + " es nula.");
		}

		if (endoso.getCotizacionDTO().getSolicitudDTO() == null) {
			throw new RuntimeException("La solicitud de la cotización " + endoso.getCotizacionDTO().getIdToCotizacion() + 
					" de la póliza " + endoso.getId().getIdToPoliza() + " es nula.");
		}

		if (endoso.getCotizacionDTO().getSolicitudDTO().getNegocio() == null) {
			throw new RuntimeException("El negocio de la solicitud " + endoso.getCotizacionDTO().getSolicitudDTO().getIdToSolicitud() +
					" solicitud de la cotización " + endoso.getCotizacionDTO().getIdToCotizacion() + " de la póliza " + endoso.getId().getIdToPoliza() +
					" es nula.");
		}
		
		//se realiza la diferencia en dias
		 Date today = Calendar.getInstance().getTime();
	     double dias = DateUtils.dateDiff(fechaInicioVigencia, today, DateUtils.Indicador.Days);
	     LogDeMidasInterfaz.log("dias => " + dias, Level.INFO, null);
	     
		//si la fecha fechaInicioVigencia es mayor a la fecha actual
		if (dias > 0) {
			BigDecimal val  =  endoso.getCotizacionDTO().getSolicitudDTO().getNegocio().getDiasDiferimiento();
			//si es mayor no es valido, por lo tanto se manda la exepción
			if (val == null || dias > val.doubleValue()) {
				Object[] pars = new Object[3];
			    pars[0]= endoso.getCotizacionDTO().getSolicitudDTO().getIdToSolicitud();
			    pars[1]= endoso.getCotizacionDTO().getIdToCotizacion();
			    pars[2]= endoso.getId().getIdToPoliza();
				throw new NegocioEJBExeption("NE_3",pars);
			}
		} else if (dias < 0) {
			BigDecimal val  =  endoso.getCotizacionDTO().getSolicitudDTO().getNegocio().getDiasRetroactividad();
			//si es mayor no es valido, por lo tanto se manda la exepción
			if (val == null || Math.abs( dias) > val.doubleValue()) {
				Object[] pars = new Object[1];
				pars[0]= endoso.getCotizacionDTO().getSolicitudDTO().getIdToSolicitud();
			    //pars[1]= endoso.getCotizacionDTO().getIdToCotizacion();
			    //pars[2]= endoso.getId().getIdToPoliza();
				throw new NegocioEJBExeption("NE_4",pars);
			}
		}
		
	}
	
	/**
	 * valida que la fecha fechaInicioVigencia este dentro de la fecha Inicio/Fin de vigencia de la poliza.
	 * @param endoso
	 * @param fechaInicioVigenciaEndoso
	 */
	private void validaFechaEntreInicioFin(EndosoDTO endoso, Date fechaInicioVigenciaEndoso){
		
		LogDeMidasInterfaz.log("Entrando a validaFechaEntreInicioFin" , Level.INFO, null);
		
		if(endoso.getCotizacionDTO() == null ) {
			throw new RuntimeException("La cotización de la póliza " + endoso.getId().getIdToPoliza() + " es nula.");
		}

		Date fechaInicioVigenciaPoliza  = null;
		Date fechaFinVigenciaPoliza = null;
		
		//Obtiene la fecha fin dependiendo del inicio del endoso
		try{
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class, endoso.getId().getIdToPoliza());
			BitemporalCotizacion cotizacion = obtenerCotizacionCancelacion(poliza, TimeUtils.getDateTime(fechaInicioVigenciaEndoso));
			fechaFinVigenciaPoliza = cotizacion.getValidityInterval().getTo();
			if(fechaFinVigenciaPoliza.compareTo(cotizacion.getValue().getFechaFinVigencia()) < 0){
				fechaFinVigenciaPoliza = cotizacion.getValue().getFechaFinVigencia();
			}
		}catch(Exception e){
			EndosoDTO ultEndoso = this.endosoFacadeRemote.getUltimoEndoso(endoso.getId().getIdToPoliza());
			fechaFinVigenciaPoliza = ultEndoso.getFechaFinVigencia();
		}
		fechaInicioVigenciaPoliza  = endoso.getCotizacionDTO().getFechaInicioVigencia();
		
		LogDeMidasInterfaz.log("fechaInicioVigenciaEndoso => "+fechaInicioVigenciaEndoso, Level.INFO, null);
		LogDeMidasInterfaz.log("fechaInicioVigenciaPoliza => "+fechaInicioVigenciaPoliza, Level.INFO, null);
		LogDeMidasInterfaz.log("fechaFinVigenciaPoliza => "+fechaFinVigenciaPoliza, Level.INFO, null);
		if (DateUtils.dateDiff(fechaInicioVigenciaEndoso, fechaInicioVigenciaPoliza, Indicador.Days) < 0  
				|| DateUtils.dateDiff(fechaInicioVigenciaEndoso, fechaFinVigenciaPoliza, Indicador.Days) > 0) {
			throw new NegocioEJBExeption("NE_2",new Object[]{endoso.getId().getIdToPoliza().longValue()});
		}
		LogDeMidasInterfaz.log("Saliendo a validaFechaEntreInicioFin" , Level.INFO, null);
	}
	
	/**
	 * valida que la fecha fechaInicioVigencia para rehabilitacion de poliza.
	 * @param endoso
	 * @param fechaInicioVigenciaEndoso
	 */
	private void validaFechaRehabilitacion(EndosoDTO endoso, Date fechaInicioVigenciaEndoso){
		
		if(endoso.getCotizacionDTO() == null ) {
			throw new RuntimeException("La cotización de la póliza " + endoso.getId().getIdToPoliza() + " es nula.");
		}

		Date fechaFinVigenciaPoliza = null;
		
		ControlEndosoCot conEndCot = obtenerControlEndosoCot(endoso.getId().getIdToPoliza());
		if (conEndCot != null) {
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class, endoso.getId().getIdToPoliza());
			if(poliza.getClaveEstatus().intValue() == 0 && 
					(conEndCot.getSolicitud().getClaveTipoEndoso().shortValue()==SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA || 
					conEndCot.getSolicitud().getClaveTipoEndoso().shortValue()==SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL )){
				fechaFinVigenciaPoliza = conEndCot.getValidFrom();
			}else{
				throw new NegocioEJBExeption("_","Ultimo endoso realizado no es cancelacion de poliza ");
			}
		}else{
			throw new NegocioEJBExeption("_","No posee endoso de cancelacion de poliza ");
		}
		
		if (DateUtils.dateDiff(fechaInicioVigenciaEndoso, fechaFinVigenciaPoliza, Indicador.Days) < 0  
				|| DateUtils.dateDiff(fechaInicioVigenciaEndoso, fechaFinVigenciaPoliza, Indicador.Days) > 0) {
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			throw new NegocioEJBExeption("_","La fecha de inicio de la rehabilitacion debe ser ("+format.format(fechaFinVigenciaPoliza)+")");
		}
		
	}
	
	/**
	 * Valida si es poliza de 11% que tenga endoso de cambio de iva
	 * @param endoso
	 * @param fechaInicioVigenciaEndoso
	 */
	private void validaRehabilitacionCambioIVA(EndosoDTO endoso){
		
		if(endoso.getCotizacionDTO() == null ) {
			throw new RuntimeException("La cotización de la póliza " + endoso.getId().getIdToPoliza() + " es nula.");
		}		
		if(polizaFacadeRemote.esPolizaCambioIVA(endoso.getId().getIdToPoliza())){
			Map<String, Object> values = new LinkedHashMap<String, Object>();
	        values.put("id.idToPoliza", endoso.getId().getIdToPoliza());
	        values.put("claveTipoEndoso", EndosoDTO.TIPO_ENDOSO_CAMBIO_IVA);
			Long total = entidadService.findByPropertiesCount(EndosoDTO.class, values);
			if(total == 0){
				throw new NegocioEJBExeption("_","Las polizas fronterizas canceladas sin cambio de iva no pueden rehabilitarse");
			}
		}
		
	}
	
	/**
	 * Valida si es poliza anexa no permita realizar endosos que puedan afectar la prima
	 * @param endoso
	 * @param fechaInicioVigenciaEndoso
	 */
	private void validaPolizaAnexa(EndosoDTO endoso){
		if (endoso.getCotizacionDTO() == null ) {
			throw new RuntimeException("La cotización de la póliza " + endoso.getId().getIdToPoliza() + " es nula.");
		}
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, endoso.getId().getIdToPoliza());
		
		if (poliza.getClaveAnexa() != null && poliza.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)) {
			int result = this.validaSiPolAnexaEsCancelable(poliza.getIdToPoliza());
			
			if (result == 0) {
				throw new NegocioEJBExeption("_","No es posible realizar este tipo de Endoso en la poliza");
			}
		}
		
	}	
	
	@Override
	public BitemporalCotizacion obtenerCotizacionCancelacion(PolizaDTO poliza, DateTime validFrom) {		
		
		
		BitemporalCotizacion cotizacion = null;		
		
		if (validFrom != null) {
			cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, 
																			poliza.getCotizacionDTO().getIdToCotizacion(), validFrom);   

		} else {
			cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, 
																						poliza.getCotizacionDTO().getIdToCotizacion());
		}		
		
		if (cotizacion == null) {
			
			ControlEndosoCot conEndCot = obtenerControlEndosoCot(poliza.getIdToPoliza());			
			
			if (conEndCot != null) {
				Collection<CotizacionContinuity> cotizacionContinuities = entidadContinuityDao.findByProperty(CotizacionContinuity.class, 
																CotizacionContinuity.BUSINESS_KEY_NAME, poliza.getCotizacionDTO().getIdToCotizacion());
				
				if (!cotizacionContinuities.isEmpty()) {
					CotizacionContinuity cotizacionContinuity = cotizacionContinuities.iterator().next();
					Collection<BitemporalCotizacion>  cotizaciones = entidadBitemporalService.obtenerCancelados(BitemporalCotizacion.class, cotizacionContinuity.getId(), 
									TimeUtils.getDateTime(conEndCot.getValidFrom()), new DateTime(conEndCot.getRecordFrom()));
					
					if (!cotizaciones.isEmpty()) {
						cotizacion = cotizaciones.iterator().next();
					}				
				}
			}
		}		
		return cotizacion;
	}

	/**
	 * Obtiene el último endoso Cot para la poliza.
	 * @param polizaId
	 * @returns
	 */
	
	private ControlEndosoCot obtenerControlEndosoCot(BigDecimal polizaId) {
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, polizaId);
		Map<String, Object> values = new LinkedHashMap<String, Object>();
        values.put("cotizacionId", poliza.getCotizacionDTO().getIdToCotizacion());
        //values.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EMITIDA);
    
        List<ControlEndosoCot> controlEndosos = entidadService.findByProperties(ControlEndosoCot.class,values);
		ControlEndosoCot controlEndoso = null;
		if(controlEndosos !=null && !controlEndosos.isEmpty()){
			controlEndoso = Collections.max(controlEndosos, new Comparator<ControlEndosoCot>() {
				public int compare(ControlEndosoCot e1, ControlEndosoCot e2){
					return e1.getId().compareTo(e2.getId());
				} 
			});
		}
		return controlEndoso;
	}
	/**
	 * Obtiene el último endoso Cot para la poliza.
	 * @param polizaId
	 * @return
	 */
	private ControlEndosoCot obtenerControlEndosoCotiz(BigDecimal polizaId, Short cveStatusCot){
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, polizaId);
		Map<String, Object> values = new LinkedHashMap<String, Object>();
        values.put("cotizacionId", poliza.getCotizacionDTO().getIdToCotizacion());
        if(cveStatusCot != null) {
			values.put("claveEstatusCot", cveStatusCot);
		}
    
        List<ControlEndosoCot> controlEndosos = entidadService.findByProperties(ControlEndosoCot.class,values);
		ControlEndosoCot controlEndoso = null;
		if(controlEndosos !=null && !controlEndosos.isEmpty()){
			controlEndoso = Collections.max(controlEndosos, new Comparator<ControlEndosoCot>() {
				public int compare(ControlEndosoCot e1, ControlEndosoCot e2){
					return e1.getId().compareTo(e2.getId());
				} 
			});
		}
		return controlEndoso;
	}
	
	@Override
	public ControlEndosoCot obtenerControlEndosoCotCotizacion(BigDecimal idToCotizacion) {
		List<ControlEndosoCot> controlEndosos = entidadService.findByProperty(ControlEndosoCot.class, "cotizacionId", idToCotizacion);
		ControlEndosoCot controlEndoso = null;
		if(controlEndosos !=null && !controlEndosos.isEmpty()){
			controlEndoso = Collections.max(controlEndosos, new Comparator<ControlEndosoCot>() {
				public int compare(ControlEndosoCot e1, ControlEndosoCot e2){
					return e1.getId().compareTo(e2.getId());
				} 
			});
		}
		return controlEndoso;
	}
	
	protected void validaEsPolizaCancelada(BigDecimal polizaId){
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, polizaId);
		List<ControlEndosoCot> controlEndosos = entidadService.findByProperty(ControlEndosoCot.class,"cotizacionId",poliza.getCotizacionDTO().getIdToCotizacion());
		ControlEndosoCot controlEndoso = null;
		if(controlEndosos !=null && !controlEndosos.isEmpty()){
			controlEndoso = Collections.max(controlEndosos, new Comparator<ControlEndosoCot>() {
				public int compare(ControlEndosoCot e1, ControlEndosoCot e2){
					return e1.getId().compareTo(e2.getId());
				} 
			});
		}
		if(controlEndoso!=null){
			if ((controlEndoso.getSolicitud().getClaveTipoEndoso().shortValue() != SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA &&
					controlEndoso.getSolicitud().getClaveTipoEndoso().shortValue() != SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL)
						|| controlEndoso.getClaveEstatusCot().shortValue() != CotizacionDTO.ESTATUS_COT_EMITIDA) {
				String statusCotizacion="";
				String tipoEndoso = "";
				if(controlEndoso.getClaveEstatusCot().shortValue() == CotizacionDTO.ESTATUS_COT_EMITIDA){
					statusCotizacion = "Emitida";
				}else{
					statusCotizacion = "en Proceso";
				}
				tipoEndoso = getDescripcionClaveTipoEndoso(controlEndoso.getSolicitud());
				
				Object[] values = new Object[]{poliza.getIdToPoliza(),statusCotizacion,tipoEndoso};
				throw new NegocioEJBExeption("NE_8", values, "La p\u00F3liza "+ poliza.getNumeroPoliza() +" no puede ser rehabilitada. El estatus actual es "+ statusCotizacion +". Corresponde a un endoso de "+tipoEndoso);
			}
		}
		
	}
	
	private String getDescripcionClaveTipoEndoso(SolicitudDTO solicitudDTO){
		String result ="";
		if(solicitudDTO.getClaveTipoEndoso()!=null){
			int claveTipoEndoso = solicitudDTO.getClaveTipoSolicitud();
			switch(claveTipoEndoso){
				case CVE_TIPO_ENDOSO_ALTA_INCISO: 
					result = "Alta de Inciso";
		            break;
		            
			    case CVE_TIPO_ENDOSO_BAJA_INCISO:
		    		result = "Baja de Inciso";
		            break;	    	
			   
			    case CVE_TIPO_ENDOSO_CAMBIO_AGENTE: 
		    		result = "Cambio de Agente";
		            break;
			            
			    case CVE_TIPO_ENDOSO_CAMBIO_DATOS: 
		    		result = "Cambio de Datos";
		            break;
			    
			    case CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO: 
		    		result = "Cambio de Forma de Pago";
		            break;
			            
			    case CVE_TIPO_ENDOSO_CANCELACION_POLIZA: 
		    		result = "Cancelaci\u00F3n de P\u00F3liza";
		            break;
			            
			    case CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA: 
		    		result = "Extensi\u00F3n de Vigencia";
		            break;
			            
			    case CVE_TIPO_ENDOSO_REHABILITACION_POLIZA: 
			    	result = "Rehabilitaci\u00F3n de P\u00F3liza";
		            break;
		            
			    case CVE_TIPO_ENDOSO_INCLUSION_ANEXO: 
			    	result = "Inclusi\u00F3n de Anexo";
		            break;
			     
			    case CVE_TIPO_ENDOSO_INCLUSION_TEXTO: 
			    	result = "Inclusi\u00F3n de Texto";
			    	break;
		
		        case CVE_TIPO_ENDOSO_DE_MOVIMIENTOS: 
		        	result = "Movimientos";
			     	break;
			     	
		        case CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL:
		        	result = "Baja de inciso PT";
					break;
					
		        case CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL:
		        	result = "Cancelacion de Poliza PT";
					break;	
					
		        case CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS:
		        	result = "Desagrupacion de Recibos";
					break;	
					
		    	 default:
		    		 break;
				
			}
		}
		return result;
	}
	
	@Override
	public int getNumeroEndoso(BigDecimal polizaId) {
		EndosoDTO ultimoEndoso = endosoFacadeRemote.getUltimoEndoso(polizaId);
		if (ultimoEndoso != null && ultimoEndoso.getId() != null
				&& ultimoEndoso.getId().getNumeroEndoso() != null) {
			return ultimoEndoso.getId().getNumeroEndoso()+1;
		}else{
			return 1;
		}			
	}

	@Override
	public String getNumeroEndoso(BigDecimal polizaId, String format) {

		int numeroEndoso = this.getNumeroEndoso(polizaId);
		return String.format(format, numeroEndoso);
	}
	
	public List<BitemporalTexAdicionalCot> prepareEntidadBitemporalEndosoTexto(String textos, DateTime validoEn) {
		List<BitemporalTexAdicionalCot> lstBiEndosoTexto = new ArrayList<BitemporalTexAdicionalCot>();
		if (!mx.com.afirme.midas2.util.StringUtil.isEmpty(textos)) {
			String[] arrayTextos = textos.split("\\|");
			
			BitemporalTexAdicionalCot biTexAdicional = null;
			Usuario usuario = usuarioService.getUsuarioActual();
			int secuencia = 0;
			for (int i = 0; i < arrayTextos.length - 1; i+= 6) {
				
				//if ("".equals(arrayTextos [i + 1]) ) {
				biTexAdicional = new BitemporalTexAdicionalCot();
				biTexAdicional.getValue().setNombreUsuarioCreacion(usuario.getNombreUsuario());
				biTexAdicional.getValue().setFechaCreacion(new Date());
				biTexAdicional.getValue().setCodigoUsuarioCreacion(usuario.getNombreUsuario());
					/*} else {
					biTexAdicional = entidadBitemporalService.getInProcessByKey(TexAdicionalCotContinuity.class, 
							Long.parseLong(arrayTextos [i + 1]), validoEn);
				}*/
				
				biTexAdicional.getValue().setClaveAutorizacion(TexAdicionalCot.CLAVEAUTORIZACION_EN_PROCESO);				
				biTexAdicional.getValue().setDescripcionTexto(arrayTextos [i + 2]);
				biTexAdicional.getValue().setNombreUsuarioModificacion(usuario.getNombreUsuario());				
				biTexAdicional.getValue().setFechaModificacion(new Date());
				biTexAdicional.getValue().setCodigoUsuarioModificacion(usuario.getNombreUsuario());
				biTexAdicional.getValue().setNumeroSecuencia(+secuencia);
				lstBiEndosoTexto.add(biTexAdicional);
			}
		}
		
		return lstBiEndosoTexto;
	}
	
	
	@Override
	public Collection<BitemporalTexAdicionalCot> getLstBiTexAdicionalCot(Long cotizacionContinuityId, DateTime validoEn) {
		Collection<BitemporalTexAdicionalCot> lstBiEndosoTextoInProcess = new ArrayList<BitemporalTexAdicionalCot>();
		
		Collection<TexAdicionalCotContinuity> lstTexAdicionalCotContinuity = new ArrayList<TexAdicionalCotContinuity>();
		
		BitemporalCotizacion biCotizacion = entidadBitemporalService.getByKey(CotizacionContinuity.class, cotizacionContinuityId, validoEn);

		lstTexAdicionalCotContinuity = biCotizacion.getContinuity().getTexAdicionalCotContinuities().getValue();
		
		for (TexAdicionalCotContinuity texAdicionalCotContinuity : lstTexAdicionalCotContinuity) {
			if (texAdicionalCotContinuity.getTexAdicionales().hasRecordsInProcess()) {
				lstBiEndosoTextoInProcess.add(texAdicionalCotContinuity.getTexAdicionales().
						getInProcess(validoEn));
			}
		}		

		return lstBiEndosoTextoInProcess;
	}
	
	@Override
	public Collection<BitemporalTexAdicionalCot> getLstBiTexAdicionalCotNotInProcess(Long cotizacionContinuityId, DateTime validoEn) {
		Collection<BitemporalTexAdicionalCot> lstBiEndosoTextoInProcess = new ArrayList<BitemporalTexAdicionalCot>();
		
		Collection<TexAdicionalCotContinuity> lstTexAdicionalCotContinuity = new ArrayList<TexAdicionalCotContinuity>();
		
		 lstTexAdicionalCotContinuity = entidadContinuityService.findContinuitiesByParentBusinessKey(
				 TexAdicionalCotContinuity.class, TexAdicionalCotContinuity.PARENT_KEY_NAME, cotizacionContinuityId);
		
		for (TexAdicionalCotContinuity texAdicionalCotContinuity : lstTexAdicionalCotContinuity) {
			if (!texAdicionalCotContinuity.getTexAdicionales().hasRecordsInProcess()) {
				BitemporalTexAdicionalCot bitemporalTexAdicionalCot = texAdicionalCotContinuity.getTexAdicionales().get(validoEn);
				if(bitemporalTexAdicionalCot != null){
					lstBiEndosoTextoInProcess.add(bitemporalTexAdicionalCot);
				}
			}
		}
		if(lstBiEndosoTextoInProcess != null && !lstBiEndosoTextoInProcess.isEmpty()){
			for(BitemporalTexAdicionalCot item : lstBiEndosoTextoInProcess){
				try{
					if(item.getValue().getDescripcionTexto() != null && IsRichText(item.getValue().getDescripcionTexto())){
						RTFEditorKit rtfParser = new RTFEditorKit();
						Document document = rtfParser.createDefaultDocument();
						rtfParser.read(IOUtils.toInputStream(item.getValue().getDescripcionTexto()), document, 0);
						String text = document.getText(0, document.getLength());
						item.getValue().setDescripcionTexto(text);
					}
				}catch(Exception e){			
				}				
			}
		}

		return lstBiEndosoTextoInProcess;
	}
	
	public static boolean IsRichText(String testString)
    {
        if ((testString != null) &&
            (testString.trim().startsWith("{\\rtf")))
        {
            return true;
        }
        else
        {
            return false;
        }
    }
	
	@Override
	public Collection<BitemporalTexAdicionalCot> getLstBiTexAdicionalCotProcess(Long cotizacionContinuityId, DateTime validoEn) {
		Collection<BitemporalTexAdicionalCot> lstBiEndosoTextoInProcess = new ArrayList<BitemporalTexAdicionalCot>();
		
		Collection<TexAdicionalCotContinuity> lstTexAdicionalCotContinuity = new ArrayList<TexAdicionalCotContinuity>();
		
		 lstTexAdicionalCotContinuity = entidadContinuityService.findContinuitiesByParentBusinessKey(
				 TexAdicionalCotContinuity.class, TexAdicionalCotContinuity.PARENT_KEY_NAME, cotizacionContinuityId);
		
		for (TexAdicionalCotContinuity texAdicionalCotContinuity : lstTexAdicionalCotContinuity) {
			if (texAdicionalCotContinuity.getTexAdicionales().hasRecordsInProcess()) {
				BitemporalTexAdicionalCot bitemporalTexAdicionalCot = texAdicionalCotContinuity.getTexAdicionales().get(validoEn);
				if(bitemporalTexAdicionalCot != null){
					lstBiEndosoTextoInProcess.add(bitemporalTexAdicionalCot);
				}
			}
		}
		
		if(lstBiEndosoTextoInProcess != null && !lstBiEndosoTextoInProcess.isEmpty()){
			for(BitemporalTexAdicionalCot item : lstBiEndosoTextoInProcess){
				try{
					if(item.getValue().getDescripcionTexto() != null && IsRichText(item.getValue().getDescripcionTexto())){
						RTFEditorKit rtfParser = new RTFEditorKit();
						Document document = rtfParser.createDefaultDocument();
						rtfParser.read(IOUtils.toInputStream(item.getValue().getDescripcionTexto()), document, 0);
						String text = document.getText(0, document.getLength());
						item.getValue().setDescripcionTexto(text);
					}
				}catch(Exception e){			
				}				
			}
		}

		return lstBiEndosoTextoInProcess;
	}
	
	@Override
	public Boolean isBiTextAdicionalCotAutorizado(Long cotizacionContinuityId, DateTime validoEn) {
		Boolean autorizados = true;
		
		Collection<BitemporalTexAdicionalCot> lstBiEndosoTexto = getLstBiTexAdicionalCot(cotizacionContinuityId, validoEn);
		
		for (BitemporalTexAdicionalCot biTextAdicionalCot : lstBiEndosoTexto) {
			if (TexAdicionalCot.CLAVEAUTORIZACION_AUTORIZADA != 
				biTextAdicionalCot.getValue().getClaveAutorizacion().shortValue()) {
				autorizados = false;
				break;
			}
		}
		
		return autorizados;
	}

	
	public void actualizaControlEndosoCot(BigDecimal cotizacionId, Long claveEstatusCot,Date validFrom, Date recordFrom){
		actualizaControlEndosoCot(cotizacionId, claveEstatusCot, validFrom, recordFrom, null);
	}
	
	@SuppressWarnings("unchecked")
	protected void actualizaControlEndosoCot(BigDecimal cotizacionId, Long claveEstatusCot, Date validFrom,
												Date recordFrom, Long idCancela) {
		
		String query = "SELECT model FROM ControlEndosoCot model WHERE model.cotizacionId =" + cotizacionId
			+ " AND model.claveEstatusCot IN("+ CotizacionDTO.ESTATUS_COT_EN_PROCESO +","+ CotizacionDTO.ESTATUS_COT_TERMINADA +")";
		
		List<ControlEndosoCot> controlEndosoCotList = entidadService.executeQueryMultipleResult(query, null);		
	
		if(!controlEndosoCotList.isEmpty()&&controlEndosoCotList.get(0)!=null){
			if(idCancela == null){
				idCancela = controlEndosoCotList.get(0).getIdCancela();
			}
			ControlEndosoCot controlEndosoCot = controlEndosoCotList.get(0);
			controlEndosoCot.setClaveEstatusCot(claveEstatusCot);
			controlEndosoCot.setValidFrom(validFrom);
			controlEndosoCot.setIdCancela(idCancela);
			if(claveEstatusCot==CotizacionDTO.ESTATUS_COT_EMITIDA){
				controlEndosoCot.setRecordFrom(recordFrom);
			}
			
			entidadService.save(controlEndosoCot);
		}
	}
	
	public void deleteCotizacionEndoso(Long cotizacionContinuityId){
		CotizacionContinuity cotizacionContinuity = entidadContinuityDao.findByKey(CotizacionContinuity.class, cotizacionContinuityId);
		entidadContinuityDao.rollBackContinuity(cotizacionContinuity.getId());
		entidadContinuityDao.refresh(cotizacionContinuity);
	}
	
	@Override
	public TerminarCotizacionDTO terminarCotizacionEndoso(BitemporalCotizacion cotizacion, DateTime validoEn) {
		
		TerminarCotizacionDTO terminarCotizacion = validateTerminarCotizacion(cotizacion, validoEn, NivelEvaluacion.TERMINAR_COTIZACION);
		
		if (terminarCotizacion == null) {
			cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_TERMINADA);
			entidadBitemporalService.saveInProcess(cotizacion, TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
			
			this.actualizaControlEndosoCot(new BigDecimal(cotizacion.getEntidadContinuity().getNumero()),
					(long) CotizacionDTO.ESTATUS_COT_TERMINADA,cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso(),null);
		
			//GENERAR LAS CONDICIONES ESPECIALES BASE DE LOS INCISOS EN EL ALTA DE INCISO
			if (cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
				condicionEspecialBitemporalService.generarCondicionesBaseInciso(cotizacion, validoEn);
			}
		
		}
		
		return terminarCotizacion;
	}
	
	@Override
	public void regresarAProcesoCotizacionEndoso(BitemporalCotizacion cotizacion, DateTime validoEn) {

		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		entidadBitemporalService.saveInProcess(cotizacion, TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
		
		this.actualizaControlEndosoCot(new BigDecimal(cotizacion.getEntidadContinuity().getNumero()),
				(long) CotizacionDTO.ESTATUS_COT_EN_PROCESO,cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso(),null);

	}
	
	
	@Override
	public boolean actualizarDatosContratanteCotizacion(BitemporalCotizacion cotizacion, ClienteGenericoDTO cliente, 
			String nombreUsuario, Date validoEn) {
		boolean saved = true;
			
			if (cliente != null) {

				if (cliente.getIdDomicilioConsulta() == null || cliente.getCodigoPostalFiscal() == null
						|| cliente.getCodigoPostalFiscal().equals("")) {
					return  false;
				}

				Double iva = obtieneIvaPorPersonaDomicilio(cliente.getIdToPersona().longValue(), cliente.getIdDomicilioConsulta());
				if(iva != null && !cotizacion.getValue().getPorcentajeIva().equals(iva)){
					return false;
				}
				
				cotizacion.getValue().setDomicilioContratanteId(BigDecimal.valueOf(cliente.getIdDomicilioConsulta()));		
				cotizacion.getValue().setPersonaContratanteId(cliente.getIdCliente().longValue());
				cotizacion.getValue().setNombreContratante(cliente.getNombreCompleto());
				cotizacion.getValue().getSolicitud().setClaveTipoPersona(cliente.getClaveTipoPersona());
				
				if(cotizacion.getRecordStatus().equals(RecordStatus.NOT_IN_PROCESS))
				{
					entidadBitemporalService.saveInProcess(cotizacion, TimeUtils.getDateTime(validoEn));					
				}
				else
				{
					entidadContinuityDao.update(cotizacion.getContinuity());
				}			
			}else{
				return false;
			}
			
		return saved;
	}
	
	private Double obtieneIvaPorPersonaDomicilio(Long idToPersona, Long idToDomicilio){
		
		try{
			CodigoPostalIVAId codigoPostalIVAId= new CodigoPostalIVAId();
			Domicilio domicilio = domicilioFacadeRemote.findById(idToDomicilio,idToPersona);	
			String claveColonia = domicilio.getIdColonia();
			codigoPostalIVAId.setNombreColonia(claveColonia);
			codigoPostalIVAId.setCodigoPostal(new BigDecimal(domicilio.getCodigoPostal().trim()));
			CodigoPostalIVADTO  codigoPostalIVADTO = codigoPostalIVAFacadeRemote.findById(codigoPostalIVAId);				
			if (codigoPostalIVADTO != null) {
				return codigoPostalIVADTO.getValorIVA();
			}else{
				LogDeMidasEJB3.log("No se pudo obtener el valor del IVA, para la colonia: "+claveColonia+" y codigo postal: "+ domicilio.getCodigoPostal()+"...", Level.WARNING,null);
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public ClienteGenericoDTO getClienteGenerico(BigDecimal idCliente) {
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		ClienteGenericoDTO cliente = null;
		filtro.setIdCliente(idCliente);
		try{
			cliente = clienteFacadeRemote.loadById(filtro);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return cliente;
	}
	
	@Override
	public List<EndosoDTO> findByPropertyWithDescriptions(String propertyName,
			final Object value, boolean ascendingOrder) {

		List<EndosoDTO> list = endosoFacadeRemote.findByProperty(propertyName, value, ascendingOrder);
			
			int index=0;
			for(EndosoDTO item : list){
				boolean existeEdicionEndoso;
				list.get(index).setDescripcionNumeroEndoso(endosoFacadeRemote.getNumeroEndoso(item.getId().getNumeroEndoso().intValue()));
				if(item.getId().getNumeroEndoso()>0){
					Map<String, Object> values = new LinkedHashMap<String, Object>();
					values.put("cotizacionId", item.getIdToCotizacion().longValue());
					values.put("recordFrom", item.getRecordFrom());
					List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,values);
					if(!controlEndosoCotList.isEmpty()&&controlEndosoCotList.get(0)!=null){
						ControlEndosoCot controlEndosoCot = controlEndosoCotList.get(0);
						list.get(index).setDescripcionTipoEndoso(endosoFacadeRemote.getDescripcionClaveTipoEndoso(controlEndosoCot.getSolicitud()));
						list.get(index).setDescripcionMotivoEndoso(endosoFacadeRemote.getDescripcionClaveMotivoEndoso(controlEndosoCot.getSolicitud()));
					}
					existeEdicionEndoso =
						generaPlantillaReporteBitemporalService.existeEdicionEndoso(
								item.getIdToCotizacion(), item.getRecordFrom(), item.getClaveTipoEndoso());
					item.setExisteEdicionEndoso(existeEdicionEndoso);
				}else{
					list.get(index).setDescripcionTipoEndoso("");
					list.get(index).setDescripcionMotivoEndoso("");
				}
				index++;
			}
			return list;
	}
	
	@Override
	public List<EndosoDTO> findForCancel(Long cotizacionId) {
		List<Object[]> list = cotizacionEndosoDao.getEndososParaCancelacion(cotizacionId);		
		
		return getEndosoDTOfromControlCot(list, cotizacionId, false);
	}
	
	@Override
	public List<EndosoIDTO> validaPagosRealizados (BigDecimal polizaId, Long numeroEndoso, String[] incisoContinuityIds, Date fechaInicioVigencia, 
			TipoEmision tipoEmision) {
		
		//Se busca el motivo del endoso
		Short motivoEndoso = SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO;
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, polizaId);
		
		HashMap<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", poliza.getCotizacionDTO().getIdToCotizacion().longValue());
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByPropertiesWithOrder(ControlEndosoCot.class, properties, "id DESC");
		
		if (controlEndosoCotList != null && !controlEndosoCotList.isEmpty()) {
			ControlEndosoCot controlEndosoCot = controlEndosoCotList.get(0);
			SolicitudDTO solicitud = controlEndosoCot.getSolicitud();
			motivoEndoso =  convierteMotivoEndosoSeycos(solicitud.getClaveMotivoEndoso());
		}
		
		
		return validaPagosRealizados (polizaId, numeroEndoso, incisoContinuityIds, fechaInicioVigencia, 
				tipoEmision, motivoEndoso);
	}
	
	/**
	 * Valida en Seycos la situacion de los recibos del endoso, poliza o inciso(s) que se desea cancelar a peticion, validando 
	 * si se puede continuar con la cancelación o no, y la fecha limite en la que se debe cancelar en caso de que aplique.
	 * Si la fecha de inicio de vigencia segun la situacion de los recibos no coincide con la fecha de inicio de vigencia
	 * ingresada por el usuario, se manda una excepcion sugiriendo la fecha valida de inicio de vigencia.
	 * @param polizaId Id de la poliza a validar
	 * @param numeroEndoso Numero de endoso a validar (en caso de que aplique)
	 * @param incisoContinuityIds Arreglo con los ids de las continuidades de incisos a validar (en caso de que aplique)
	 * @param fechaInicioVigencia Fecha de inicio de vigencia del endoso a peticion del usuario
	 * @param tipoEmision Tipo de Emision con la cual se va a validar
	 * @param motivoEndoso Motivo del Endoso (A Peticion o Automatica)
	 * @return Lista con los incisos validados de manera individual
	 */
	@Override
	public List<EndosoIDTO> validaPagosRealizados (BigDecimal polizaId, Long numeroEndoso, String[] incisoContinuityIds, Date fechaInicioVigencia, 
			TipoEmision tipoEmision, Short motivoEndoso) {
			
		if (polizaId == null) throw new RuntimeException("polizaId es nulo");
		String numeroEndosoString = "0";
		Boolean invocaValidaEndoso = true;
		Map<String, Object> values = new LinkedHashMap<String, Object>();
		values.put("idToPoliza", polizaId);
		values.put("estatusRegistro", EstatusRegistro.EN_PROCESO.getEstatusRegistro());
		values.put("tipoEmision", (tipoEmision.equals(TipoEmision.CANC_INCISO_AUTOS)?TipoEmision.BAJA_INCISO_PERDIDA_TOTAL_AUTOS.getTipoEmision():tipoEmision.getTipoEmision()));
		values.put("migrada", EmisionPendiente.POLIZA_MIGRADA);
		List<EmisionPendiente> emisionesPendientes = entidadService.findByProperties(EmisionPendiente.class,values);
		
		if(emisionesPendientes!=null && !emisionesPendientes.isEmpty()) {
			
			if (emisionesPendientes.get(0).getTipoEmision() == EmisionPendiente.TipoEmision.CANC_END_AUTOS.getTipoEmision()) {
				int numeroEndosoInt = (int)(emisionesPendientes.get(0).getNumeroEndoso()/1000);
				numeroEndoso = new Long(numeroEndosoInt);
			} else {
				numeroEndosoString = emisionesPendientes.get(0).getNumeroEndoso().toString();
			}
		}
		if(numeroEndoso!=null){
			values = new LinkedHashMap<String, Object>();
			values.put("id.idToPoliza", polizaId);
			values.put("id.numeroEndoso", numeroEndoso.shortValue());
			
			List<EndosoDTO> endosoAcancelar = entidadService.findByProperties(EndosoDTO.class, values);
			
			if(!endosoAcancelar.isEmpty() && endosoAcancelar.get(0)!=null){
				values = new LinkedHashMap<String, Object>();
				values.put("recordFrom",  endosoAcancelar.get(0).getRecordFrom());
				
				List<ControlEndosoCot> controlEndosoCotACancelar = entidadService.findByProperties(ControlEndosoCot.class, values);
				if(!controlEndosoCotACancelar.isEmpty() && controlEndosoCotACancelar.get(0)!=null){
					if(controlEndosoCotACancelar.get(0).getSolicitud().getClaveTipoEndoso().shortValue()==SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS || 
							controlEndosoCotACancelar.get(0).getSolicitud().getClaveTipoEndoso().shortValue()==SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE ||
							controlEndosoCotACancelar.get(0).getSolicitud().getClaveTipoEndoso().shortValue()==SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO ||
							controlEndosoCotACancelar.get(0).getSolicitud().getClaveTipoEndoso().shortValue()==SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO ||
							controlEndosoCotACancelar.get(0).getSolicitud().getClaveTipoEndoso().shortValue()==SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES ||
							controlEndosoCotACancelar.get(0).getSolicitud().getClaveTipoEndoso().shortValue()==SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES){
						invocaValidaEndoso = false;
					}
				}
			}
		}
		String identificador = polizaId + "|" + convierteMotivoEndosoSeycos(motivoEndoso) + "|";
		String identificadorEndoso = numeroEndosoString;
		StringBuilder identificadorIncisos = new StringBuilder("");
		Usuario  usuario  = usuarioService.getUsuarioActual();
		IncisoContinuity incisoContinuity = null;
		SeccionIncisoContinuity seccionIncisoContinuity = null;
		List<EndosoIDTO> resultadosValidacion = null;
		
		if (tipoEmision.equals(TipoEmision.CANC_INCISO_AUTOS)) {
			
			if (incisoContinuityIds == null || incisoContinuityIds.length == 0) throw new NegocioEJBExeption("NE_20",new Object[]{}); 
			
			identificadorIncisos.append("|");
			
			for (String stringIncisoContinuityId : incisoContinuityIds) {
				
				incisoContinuity = entidadService.findById(IncisoContinuity.class, Long.valueOf(stringIncisoContinuityId));
				seccionIncisoContinuity = incisoContinuity.getIncisoSeccionContinuities().getValue().iterator().next();
				
				identificadorIncisos.append(seccionIncisoContinuity.getSeccion().getIdToSeccion()).append("-").append(incisoContinuity.getNumero()).append(",");
				
			}
			
		} else if (tipoEmision.equals(TipoEmision.CANC_END_AUTOS)) {
			
			if (numeroEndoso == null) throw new RuntimeException("numeroEndoso es nulo");
			
			identificadorEndoso = "" + numeroEndoso;
		}
		
		identificador  = identificador + identificadorEndoso + identificadorIncisos.toString();
		
		System.out.println(
				"IDENTIFICADOR "+ identificador + " IDENTIFICADORENDOSO " +identificadorEndoso+ " IDENTIFICADORINCISOS " +identificadorIncisos.toString());
		
		if(invocaValidaEndoso){
		
			resultadosValidacion = endosoFacadeInterfaz.validaPagosRealizados(identificador,usuario!=null?usuario.getNombreUsuario():"SISTEMA", 
					fechaInicioVigencia, tipoEmision);
			
			
			if (resultadosValidacion != null && !resultadosValidacion.isEmpty()) {
				
				EndosoIDTO resultadoValidacion = resultadosValidacion.get(0);
				
				if (resultadoValidacion.getEsValido() == 0) {
					
					if (resultadoValidacion.getMotivoNoCancelable() != null 
							&& !resultadoValidacion.getMotivoNoCancelable().trim().isEmpty()) {
						throw new NegocioEJBExeption("_", resultadoValidacion.getMotivoNoCancelable());
					} else if (!fechaInicioVigencia.equals(resultadoValidacion.getFechaInicioVigencia())) {
						switch (tipoEmision) {
							case CANC_POL_AUTOS:
								throw new NegocioEJBExeption("NE_12",new Object[]{resultadoValidacion.getFechaInicioVigencia()});
							case CANC_END_AUTOS:
								throw new NegocioEJBExeption("NE_13",new Object[]{resultadoValidacion.getFechaInicioVigencia()});
						
						}
					}
					
				}
				
			}else{
				throw new NegocioEJBExeption("_", "No es posible realizar la operaci\u00F3n.");
			}
		}
		return resultadosValidacion;
				
	}
	
	
	
	@Override
	public Object validaPagosRealizadosFront (BigDecimal polizaId, Long numeroEndoso, String[] incisoContinuityIds, Date fechaInicioVigencia, 
			TipoEmision tipoEmision, Short motivoEndoso) {
		
		
		Object obj = new Object();
	
		List<EndosoIDTO> resultadosValidacion = this.validaPagosRealizados(polizaId, numeroEndoso, incisoContinuityIds, fechaInicioVigencia, tipoEmision, motivoEndoso);
		
		BigDecimal impNCR = new BigDecimal(0,mathCtx);
		if(resultadosValidacion != null && !resultadosValidacion.isEmpty()){
			for(EndosoIDTO resultadoEndoso : resultadosValidacion){
				if (resultadoEndoso.getEsValido() == 0) {
					String mensaje = resultadoEndoso.getMotivoNoCancelable();
					if(mensaje == null || mensaje.trim().isEmpty()){
						mensaje = "No es posible realizar la operaci\u00F3n.";
					}
					throw new NegocioEJBExeption("_", mensaje);
				} else if (!fechaInicioVigencia.equals(resultadoEndoso.getFechaInicioVigencia())){
					switch (tipoEmision) {
						case CANC_POL_AUTOS:
							throw new NegocioEJBExeption("NE_12",new Object[]{resultadoEndoso.getFechaInicioVigencia()});
						case CANC_END_AUTOS:
							throw new NegocioEJBExeption("NE_13",new Object[]{resultadoEndoso.getFechaInicioVigencia()});
					}
				}else{
					impNCR = impNCR.add(resultadoEndoso.getImporteNotaCredito(),mathCtx);
				}
			}
		}
		
		obj = impNCR;
		
		return obj;
	}
	
	@Override
	public List<EndosoDTO> findForAdjust(Long cotizacionId) {
		List<Object[]> list = cotizacionEndosoDao.getEndososParaAjustar(cotizacionId);		
		
		List<EndosoDTO> endososPosiblesParaAjustar = new ArrayListNullAware<EndosoDTO>();
		
		//Se agrega el Endoso 0 (Poliza Original)
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("idToCotizacion", cotizacionId);
		properties.put("id.numeroEndoso", 0);
		
		List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, properties);
		
		if (endosos != null && endosos.size() > 0) {
			list.add(0, new Object[] {endosos.get(0), null});
		}
		
		for (EndosoDTO endoso : getEndosoDTOfromControlCot(list, cotizacionId, true)) {
			endososPosiblesParaAjustar.add(endoso);
		}
		
		return endososPosiblesParaAjustar;
	
	}
	
	private Object validaEndosoCancelacionEndoso(EndosoDTO endoso, Date fechaInicioVigencia, Short motivoEndoso) {
		Object obj = new Object();
		if (fechaInicioVigencia.compareTo(endoso.getFechaInicioVigencia()) < 0) {
			throw new NegocioEJBExeption("NE_14",new Object[]{endoso.getFechaInicioVigencia()});
		}
		
		obj = validaPagosRealizadosFront(endoso.getId().getIdToPoliza(), new Long(endoso.getId().getNumeroEndoso()), null, fechaInicioVigencia, 
				TipoEmision.CANC_END_AUTOS, motivoEndoso);
		return obj;
	}
	
		
	private List<EndosoDTO> getEndosoDTOfromControlCot(List<Object[]> list, Long cotizacionId, boolean paraAjustePrima) {
		List<EndosoDTO> listEndoso = new ArrayList<EndosoDTO>();
			
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cotizacionId", cotizacionId);
		params.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
		List<ControlEndosoCot> controlesEnProceso = entidadService.findByProperties(ControlEndosoCot.class, params);		

		boolean checked = false;
		if (list != null) {
			
			Iterator<Object[]> itLst = list.iterator();
			while (itLst.hasNext()) {
				Object[] resultado = itLst.next();
				EndosoDTO endoso = (EndosoDTO) resultado[0];
				ControlEndosoCot control = (ControlEndosoCot) resultado[1];
				
				if (controlesEnProceso != null  && !controlesEnProceso.isEmpty()) {
					ControlEndosoCot controlEnProceso = controlesEnProceso.get(0);
					
					if (paraAjustePrima && endoso.getId().getNumeroEndoso().equals(controlEnProceso.getNumeroEndosoAjusta())) {
						endoso.setChecked(true);
						checked = true;
					} else if (control != null && control.getId().equals(controlEnProceso.getIdCancela())) {
						endoso.setChecked(true);
						checked = true;
					}					
				}
				if (endoso.getId().getNumeroEndoso().shortValue() == 0) {
					endoso.setDescripcionTipoEndoso("POLIZA ORIGINAL");
				} else {
					endoso.setDescripcionTipoEndoso(endosoFacadeRemote.getDescripcionClaveTipoEndoso(control.getSolicitud()));
				}
				
				listEndoso.add(endoso);
			}			
		}
		
		if (list != null && !list.isEmpty() && !checked) {
			listEndoso.get(0).setChecked(true);
		}
		return listEndoso;
	}
	
	private ControlEndosoCot getControlFromEndoso(Long cotizacionId, Short numeroEndoso) {
        
        ControlEndosoCot controlEndoso = null;
        Map<String, Object> filtros = new HashMap<String, Object>();
        
        filtros.put("id.numeroEndoso", numeroEndoso);
        filtros.put("idToCotizacion", cotizacionId);
        List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, filtros);
        
        if (endosos != null && endosos.size() > 0) {
              filtros = new HashMap<String, Object>();
              filtros.put("cotizacionId", cotizacionId);
              filtros.put("recordFrom", endosos.get(0).getRecordFrom());
              
              List<ControlEndosoCot> controles = entidadService.findByProperties(ControlEndosoCot.class, filtros);
              
              if (controles != null && controles.size() > 0) {
                    controlEndoso = controles.get(0);
              }
        }        
        return controlEndoso;        
	}
	
	protected EndosoDTO getEndoso(PolizaDTO poliza, Long numeroEndoso) {
        
		EndosoDTO endoso = null;
         Map<String, Object> filtros = new HashMap<String, Object>();
        
        filtros.put("id.numeroEndoso", numeroEndoso.shortValue());
        filtros.put("idToCotizacion", poliza.getCotizacionDTO().getIdToCotizacion());
        List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, filtros);
        
        if (endosos != null && !endosos.isEmpty()) {
        	endoso = endosos.get(0);
        	
        	CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, endoso.getIdToCotizacion());
        	endoso.setCotizacionDTO(cotizacion);
        	
        }        
     
        return endoso;        
	}

	
	@Override
	public List<EndosoDTO> findForRehab(Long cotizacionId) {

		List<Object[]> list = cotizacionEndosoDao.getEndososParaRehabilitacion(cotizacionId);
		return getEndosoDTOfromControlCot(list, cotizacionId, false);
	}
	
	protected ControlEndosoCot getControlEndosoCancelacion(BigDecimal cotizacionId, String accionEndoso) {
		
		ControlEndosoCot controlEndoso = null;
		List<ControlEndosoCot> controlEndosos = null;
		
		controlEndosos = entidadService.findByProperty(ControlEndosoCot.class,"cotizacionId", cotizacionId);
		
		if(controlEndosos != null && !controlEndosos.isEmpty()) {

			Collections.sort(controlEndosos, new Comparator<ControlEndosoCot>() {
				public int compare(ControlEndosoCot e1, ControlEndosoCot e2){
					return e1.getId().compareTo(e2.getId());
				} 
			});
			
			Collections.reverse(controlEndosos);
			
			/**
			 Si es nuevo Endoso el ultimo control Endoso es el de la cancelacion,
			 Si es edicion de Endoso de Rehabilitacion el control Endoso de la cancelacion es el penultimo
			 */
			if (accionEndoso.equals(TipoAccionDTO.getNuevoEndosoCot())) {
				controlEndoso = controlEndosos.get(0);
			} else {
				controlEndoso = controlEndosos.get(1);
			}
		
		} else {
			throw new RuntimeException("No se encontraron controles de Endoso para la cotizacion con id=" + cotizacionId);
		}
		
		return controlEndoso;
	}
	
	@Override
	public TerminarCotizacionDTO validateTerminarCotizacion(BitemporalCotizacion cotizacion, DateTime validoEn, NivelEvaluacion nivelEvaluacion) {		
	
		TerminarCotizacionDTO terminarCotizacionDTO = null;

		/*
		terminarCotizacionDTO = validarIncisosFlotilla(cotizacion, validoEn);
		if(terminarCotizacionDTO != null){
			return terminarCotizacionDTO;
		}*/
		
		terminarCotizacionDTO = validarMonto(cotizacion, validoEn);
		if(terminarCotizacionDTO != null){
			return terminarCotizacionDTO;
		}
		
		terminarCotizacionDTO = validarSolicitudes(cotizacion, validoEn);
		if(terminarCotizacionDTO != null){
			return terminarCotizacionDTO;
		}
		
		terminarCotizacionDTO = validarExcepciones(cotizacion, validoEn, nivelEvaluacion);
		if(terminarCotizacionDTO != null){
			return terminarCotizacionDTO;
		}
		
		
		return terminarCotizacionDTO;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TerminarCotizacionDTO validarEmisionCotizacion(BitemporalCotizacion cotizacion, DateTime validoEn) {				

		Short tipoEndoso = null;
		try{
			tipoEndoso = cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		TerminarCotizacionDTO terminarCotizacionDTO = null;	
		
		terminarCotizacionDTO = validarMonto(cotizacion, validoEn);
		if (terminarCotizacionDTO != null) {
			return terminarCotizacionDTO;
		}
		
		//Validar solo si no son flotillas y no son cambio de datos
		if(tipoEndoso != null && !tipoEndoso.equals(SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS) &&
				cotizacion.getValue().getTipoPoliza().getClaveAplicaFlotillas().intValue() == 0){
			terminarCotizacionDTO = validarConductoCobro(cotizacion, validoEn);
			if (terminarCotizacionDTO != null) {
				return terminarCotizacionDTO;
			}
		}
		
		terminarCotizacionDTO = validarSolicitudes(cotizacion, validoEn);
		if (terminarCotizacionDTO != null) {
			return terminarCotizacionDTO;
		}
		
		terminarCotizacionDTO = validarExcepciones(cotizacion, validoEn, NivelEvaluacion.EMITIR);
		if (terminarCotizacionDTO != null) {
			revisarConflictoNumeroSerie(terminarCotizacionDTO.getExcepcionesList(), cotizacion.getContinuity().getId(), validoEn);
			return terminarCotizacionDTO;
		}		
		return terminarCotizacionDTO;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TerminarCotizacionDTO validarEmisionCotizacionCondicionesGenerales(BitemporalCotizacion cotizacion, DateTime validoEn) {	
		TerminarCotizacionDTO terminarCotizacionDTO = null;		
		terminarCotizacionDTO = validarExcepciones(cotizacion, validoEn, NivelEvaluacion.EMITIR);
		return terminarCotizacionDTO;
	}
	
	private TerminarCotizacionDTO validarMonto(BitemporalCotizacion cotizacion, DateTime validoEn) {
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		if(cotizacion.getValue().getSolicitud().getClaveTipoEndoso().longValue() != SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS){
			ResumenCostosDTO resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getContinuity().getNumero(),
																													validoEn);
			if(resumenCostosDTO.getPrimaNetaCoberturas() == null || resumenCostosDTO.getPrimaNetaCoberturas() == 0){
				terminarCotizacionDTO = new TerminarCotizacionDTO();
				terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_MONTO.getEstatus());
				terminarCotizacionDTO.setMensajeError("Esta cotizaci\u00F3n no puede ser terminada debido a que el monto de prima " +
						"no esta debidamente calculado, por favor recalcula la cotizaci\u00F3n y vuelva a intentarlo.");
			}
		}
		return terminarCotizacionDTO;		
	}
	
	
	@SuppressWarnings("unchecked")
	private TerminarCotizacionDTO validarExcepciones(BitemporalCotizacion cotizacion, DateTime validoEn,
																				NivelEvaluacion nivelEvaluacion){
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		List<ExcepcionSuscripcionReporteDTO> lista = 
			(List<ExcepcionSuscripcionReporteDTO>) excepcionSuscripNegAutoBitemporalService.evaluarCotizacion(
																					cotizacion, nivelEvaluacion, validoEn);
		if (!lista.isEmpty()) {
			for (ExcepcionSuscripcionReporteDTO item : lista) {
				if (item.getIdLineaNegocio() != null) {
					NegocioSeccion seccion = entidadService.findById(NegocioSeccion.class, item.getIdLineaNegocio());
					if (seccion != null) {
						item.setDescripcionSeccion(seccion.getSeccionDTO().getDescripcion());
					}
				}
				if (item.getIdCobertura() != null) {					
					CoberturaCotizacionDTO cobertura = entidadService.findById(CoberturaCotizacionDTO.class, 
							new CoberturaCotizacionId(item.getIdToCotizacion(), item.getIdInciso(), 
									item.getIdLineaNegocio(), item.getIdCobertura()));
					if (cobertura != null) {
						item.setDescripcionCobertura(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion());
					}
				} else {
					item.setDescripcionCobertura("NA");
				}
				if (item.getIdInciso() != null) {
					IncisoCotizacionId id = new IncisoCotizacionId();
					id.setIdToCotizacion(item.getIdToCotizacion());
					id.setNumeroInciso(item.getIdInciso());
					IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
					if (inciso != null) {
						item.setDescripcionInciso(inciso.getIncisoAutoCot().getDescripcionFinal());
					}
				}
			}
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus());
			terminarCotizacionDTO.setExcepcionesList(lista);
			terminarCotizacionDTO.setMensajeError("Algunos datos de la cotizaci\u00f3n provocaron excepciones que " +
					"requieren ser revisadas");
		}
		return terminarCotizacionDTO;
	}
		
	private TerminarCotizacionDTO validarSolicitudes(BitemporalCotizacion cotizacion, DateTime validoEn) {
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		List<SolicitudExcepcionCotizacion> lista = solicitudAutorizacionService.listarSolicitudesInvalidas(
																			new BigDecimal(cotizacion.getContinuity().getNumero()));
		if(!lista.isEmpty()){
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_SOLICITUDES.getEstatus());
			terminarCotizacionDTO.setMensajeError("Esta cotizaci\u00F3n no puede ser terminada debido a que existen solicitudes de autorizacion " +
					"pendientes. Favor de revisar la bandeja de solicitudes.");
		}
		return terminarCotizacionDTO;
	}
	
	//TODO AJUSTAR A PROCESO INTERNO DE CONTADOR DE INCISOS
	private TerminarCotizacionDTO validarIncisosFlotilla(BitemporalCotizacion cotizacion, DateTime validoEn) {
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		//Collection<BitemporalInciso> incisos = cotizacion.getContinuity().getIncisoContinuities().getInProcess(validoEn);
		int totalIncisos = incisoViewService.getNumeroIncisos(cotizacion.getContinuity().getNumero().longValue(),
				new DateTime(validoEn), new DateTime(new Date())).intValue();
		//if (cotizacion.getValue().getTipoPoliza().getClaveAplicaFlotillas().intValue() == 1 && incisos.size() < 2) {
		if (cotizacion.getValue().getTipoPoliza().getClaveAplicaFlotillas().intValue() == 1 && totalIncisos < 2) {
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_INCISOS_INSUFICIENTES.getEstatus());
			terminarCotizacionDTO.setMensajeError("Para poder terminar una flotilla se necesitan al menos dos incisos.");
		}
		return terminarCotizacionDTO;
	}
	
	private TerminarCotizacionDTO validarConductoCobro(BitemporalCotizacion cotizacion, DateTime validoEn) {
		CuentaPagoDTO cuenta = null;
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		if (cotizacion.getValue().getMedioPago() == null || cotizacion.getValue().getMedioPago().getIdMedioPago() == null) {
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_COBRANZA.getEstatus());
			terminarCotizacionDTO.setMensajeError("No se ha asignado un medio de pago para la cotizaci\u00F3n.");
		} else if (cotizacion.getValue().getMedioPago().getIdMedioPago().shortValue() != TipoCobro.EFECTIVO.valor()) {
			try{
				cuenta = polizaPagoService.obtenerConductoCobro(cotizacion.getContinuity().getId(), validoEn);				
			}catch(Exception ex){
				ex.printStackTrace();
			}		
			if(cuenta == null || StringUtil.isEmpty(cuenta.getCuenta())){
				terminarCotizacionDTO = new TerminarCotizacionDTO();
				terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_DATOS_COBRANZA.getEstatus());
				terminarCotizacionDTO.setMensajeError("No se ha complementado la informaci\u00F3n del medio de pago. " +
						"Complem\u00E9ntela en la secci\u00F3n de Cobranza.");
			}
		}		
		return terminarCotizacionDTO;
	}
	
	private void revisarConflictoNumeroSerie(List<? extends ExcepcionSuscripcionReporteDTO> excepcionesList, 
																				Long cotizacionContinuity, DateTime validoEn) {
		boolean conflicto = false;

		for (ExcepcionSuscripcionReporteDTO reporte : excepcionesList) {
			if (reporte.getIdExcepcion().shortValue() == 
				ExcepcionSuscripcionService.TipoExcepcionFija.NUMERO_SERIE_REPETIDO.valor()) {
				conflicto = true;				
				break;
			}
		}		
		BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, 
																					cotizacionContinuity, validoEn);
		if (conflicto) {
			cotizacion.getValue().setConflictoNumeroSerie(true);
			entidadBitemporalService.saveInProcess(cotizacion, validoEn);
		}
		
	}

	
	private void validaAplicaFlotilla(BigDecimal idToPoliza, short tipoEndoso) {
		PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, polizaDTO.getCotizacionDTO().getIdToCotizacion());
		if (cotizacionDTO != null && cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas()==0) {
			Object[] values = new Object[]{polizaDTO.getNumeroPoliza()};
			if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
				throw new NegocioEJBExeption("NE_15",values,"La p\u00F3liza "+ polizaDTO.getNumeroPoliza() +" es individual. No es posible realizar un endoso de alta de inciso");
				
			} else 	if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO) {
				throw new NegocioEJBExeption("NE_17",values,"La p\u00F3liza "+ polizaDTO.getNumeroPoliza() +" es individual. No es posible realizar un endoso de baja de inciso");
			}
			else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS)
			{
				throw new NegocioEJBExeption("NE_28",values,"La p\u00F3liza " + polizaDTO.getNumeroPoliza() 
						+ " es individual. No es posible realizar un endoso de Desagrupaci\u00F3n de recibos");
				
			}
			
		}
	}
	
	private void validaNoTieneEndososAFuturo(BitemporalCotizacion cotizacion) {
		
		DateTime validoEn = TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
		
		DateTime validoHasta1 = cotizacion.getEntidadContinuity().getBitemporalProperty()
			.get(validoEn).getValidityInterval().getInterval().getEnd();
				
		List<IntervalWrapper> intervalos = cotizacion.getEntidadContinuity().getBitemporalProperty()
			.getMergedValidityIntervals();
	
		DateTime validoHasta2 = intervalos.get(0).getInterval().getEnd();
		
		if (!validoHasta1.equals(validoHasta2)) {
			throw new NegocioEJBExeption("NE_21",new Object[]{});
		}
	}
	
	
	@Override
	public List<CatalogoValorFijoDTO> listadoMotivoEndoso(Long tipoEndoso, Long polizaId) {
		List<CatalogoValorFijoDTO> listado = entidadService.findByProperty(CatalogoValorFijoDTO.class, "id.idGrupoValores", 332);
		
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO) {
			listado = listadoMotivoEndosoFiltrado(listado, SolicitudDTO.CVE_MOTIVO_ENDOSO_ALTA_SINIESTRALIDAD,
					SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL, SolicitudDTO.CVE_MOTIVO_ENDOSO_ROBO_TOTAL,
					SolicitudDTO.CVE_MOTIVO_ENDOSO_FALTA_PAGO);
		}
		
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA) {
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, new BigDecimal(polizaId));
			
			if (polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas() ==  0) {
				listado = listadoMotivoEndosoFiltrado(listado, SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL, 
						SolicitudDTO.CVE_MOTIVO_ENDOSO_ROBO_TOTAL, SolicitudDTO.CVE_MOTIVO_ENDOSO_FALTA_PAGO);
			} else {
				listado = listadoMotivoEndosoFiltrado(listado, SolicitudDTO.CVE_MOTIVO_ENDOSO_VENTA_UNIDAD,
						SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL, SolicitudDTO.CVE_MOTIVO_ENDOSO_ROBO_TOTAL,
						SolicitudDTO.CVE_MOTIVO_ENDOSO_FALTA_PAGO);
			}
		}
		
		return listado;
	}
	
	@SuppressWarnings("unchecked")
	private List<CatalogoValorFijoDTO> listadoMotivoEndosoFiltrado(List<CatalogoValorFijoDTO> listado, final Short... motivosARemover) {
		Predicate predicate = new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				CatalogoValorFijoDTO valor = (CatalogoValorFijoDTO) arg0;
				
				for (Short motivo : motivosARemover) {
					if (valor.getId().getIdDato() == motivo.intValue()) {
						return false;
					}
				}
				
				return true;
			}
		};		
		listado = (List<CatalogoValorFijoDTO>) CollectionUtils.select(listado, predicate);
		return listado;
	}
	
	@Override
	public Short convierteMotivoEndosoSeycos(Short motivoEndoso)  {
		Short motivoEndosoSeycos = SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO;
		
		if (motivoEndoso == SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL 
				|| motivoEndoso == SolicitudDTO.CVE_MOTIVO_ENDOSO_ROBO_TOTAL
				|| motivoEndoso == SolicitudDTO.CVE_MOTIVO_ENDOSO_FALTA_PAGO
				|| motivoEndoso == SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO)  {
			 motivoEndosoSeycos = SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO;
		}
		
		return motivoEndosoSeycos;		
	}
	
	protected static final Short TIPO_SOLICITUD = (short) 2;

	@EJB
	protected CalculoService calculoService;
	
	
	@EJB
	protected EntidadService entidadService;

	@EJB
	protected EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	protected EntidadContinuityService entidadContinuityService;

	@EJB(name="ejb/endosoInterfaz")
	protected mx.com.afirme.midas.interfaz.endoso.EndosoFacadeRemote endosoFacadeInterfaz;
	
	@EJB
	protected EndosoFacadeRemote endosoFacadeRemote; 

	@EJB(beanName="UsuarioServiceDelegate")
	protected UsuarioService usuarioService;	
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;	

	@EJB
	protected IncisoService incisoService;

	@EJB
	private MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;
	
	@EJB
	private ClienteFacadeRemote clienteFacadeRemote;
	
	
	@EJB
	private CotizacionEndosoDao cotizacionEndosoDao;
	
	@EJB
	private ExcepcionSuscripNegAutoBitemporalService excepcionSuscripNegAutoBitemporalService;
	
	@EJB
	private SolicitudAutorizacionService solicitudAutorizacionService;
	
	@EJB
	private PolizaPagoService polizaPagoService;
	
	@EJB
	private SolicitudAutorizacionService autorizacionService;
	
    @EJB
    private DomicilioFacadeRemote domicilioFacadeRemote;
    
    @EJB
    private CodigoPostalIVAFacadeRemote codigoPostalIVAFacadeRemote;
    
    @EJB
	private CobranzaService cobranzaService;
	
    @EJB
	private EndosoDao endosoDao;
    
    @EJB
	private NegocioDerechosService negocioDerechosService;
    
    @EJB
    private IncisoViewService incisoViewService;
    
    @EJB
    private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoService;
    
    @EJB
    private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
    
    @EJB
    private ParametroGeneralService parametroGeneralService;
    
    @EJB
    private PolizaFacadeRemote polizaFacadeRemote;
    
    @EJB
    private CondicionEspecialBitemporalService condicionEspecialBitemporalService;
    
    @EJB
    private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
    
    @EJB
    private EmisionPendienteService emisionPendienteService;
    
    @EJB
    private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}

	public void setEndosoFacadeInterfaz(
			mx.com.afirme.midas.interfaz.endoso.EndosoFacadeRemote endosoFacadeInterfaz) {
		this.endosoFacadeInterfaz = endosoFacadeInterfaz;
	}
	
	public void setEntidadContinuityDao(
			EntidadContinuityDao entidadContinuityDao) {
		this.entidadContinuityDao = entidadContinuityDao;
	}
	
	public void setMovimientoEndosoBitemporalService(
			MovimientoEndosoBitemporalService movimientoEndosoBitemporalService) {
		this.movimientoEndosoBitemporalService = movimientoEndosoBitemporalService;
	}
	
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	public void setCotizacionEndosoDao(CotizacionEndosoDao cotizacionEndosoDao) {
		this.cotizacionEndosoDao = cotizacionEndosoDao;
	}
	
	public void setExcepcionSuscripNegAutoBitemporalService(
			ExcepcionSuscripNegAutoBitemporalService excepcionSuscripNegAutoBitemporalService) {
		this.excepcionSuscripNegAutoBitemporalService = excepcionSuscripNegAutoBitemporalService;
	}
	
	public void setSolicitudAutorizacionService(
			SolicitudAutorizacionService solicitudAutorizacionService) {
		this.solicitudAutorizacionService = solicitudAutorizacionService;
	}
	
	public void setCobranzaService(CobranzaService cobranzaService) {
		this.cobranzaService = cobranzaService;
	}
	
	public EndosoDao getEndosoDao() {
		return endosoDao;
	}

	public void setEndosoDao(EndosoDao endosoDao) {
		this.endosoDao = endosoDao;
	}

	public void setPolizaPagoService(PolizaPagoService polizaPagoService) {
		this.polizaPagoService = polizaPagoService;
	}
	
	public void setPolizaFacadeRemote(PolizaFacadeRemote polizaFacadeRemote) {
		this.polizaFacadeRemote = polizaFacadeRemote;
	}

	@Override
	public void setBitemporalCancelacionEndoso(BitemporalCotizacion cotizacion,
			Date validoDesde, Date validoHasta) {		
		
		Cotizacion cotValue = new Cotizacion(cotizacion.getEmbedded());
		
		BitemporalCotizacion c = new BitemporalCotizacion();
		c.setValue(cotValue);
		c.getContinuity().setId(cotizacion.getContinuity().getId());
		c.getContinuity().setParentContinuity(cotizacion.getContinuity().getParentContinuity());
		//se setea la cotización a la continuidad
		cotizacion.getContinuity().getCotizaciones().set(c.getValue(), 
				new IntervalWrapper(new Interval(TimeUtils.getDateTime(validoDesde), TimeUtils.getDateTime(validoHasta))), true);
		
		//Se realiza la actualización de BitemporalTexAdicionalCot
		if(cotizacion.getContinuity().getTexAdicionalCotContinuities() != null){
			Collection<BitemporalTexAdicionalCot> list = cotizacion.getContinuity().getTexAdicionalCotContinuities().get(TimeUtils.getDateTime(validoDesde));
			if(list != null){
				for (BitemporalTexAdicionalCot bitTexAdiCot : list) {
					entidadBitemporalService.attachToParentAndSet(bitTexAdiCot, cotizacion, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
					cotizacion.getContinuity().getTexAdicionalCotContinuities().getValue().add(bitTexAdiCot.getContinuity());
					//entidadBitemporalService.saveInProcess(bitTexAdiCot,validOn,validoHastaF);
				}
			}
		}
		//Se realiza la actualización de BitemporalDocAnexoCot
		if(cotizacion.getContinuity().getDocAnexoContinuities() != null){
			Collection<BitemporalDocAnexoCot> list = cotizacion.getContinuity().getDocAnexoContinuities().get(TimeUtils.getDateTime(validoDesde));
			if(list != null){
				for (BitemporalDocAnexoCot bitDocAnex : list) {
					entidadBitemporalService.attachToParentAndSet(bitDocAnex, cotizacion, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
					cotizacion.getContinuity().getDocAnexoContinuities().getValue().add(bitDocAnex.getContinuity());
					//entidadBitemporalService.saveInProcess(bitDocAnex,validOn,validoHastaF);
				}
			}
		}
		//Se realiza la actualización de BitemporalComision
		if(cotizacion.getContinuity().getComisionContinuities() != null){
			Collection<BitemporalComision> list = cotizacion.getContinuity().getComisionContinuities().get(TimeUtils.getDateTime(validoDesde));
			if(list != null){
				for (BitemporalComision bitComision : list) {
					entidadBitemporalService.attachToParentAndSet(bitComision, cotizacion, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
					cotizacion.getContinuity().getComisionContinuities().getValue().add(bitComision.getContinuity());
					//entidadBitemporalService.saveInProcess(bitComision,validOn,validoHastaF);
				}
			}
		}
		//Se realiza la actualización de BitemporalInciso
		if(cotizacion.getContinuity().getIncisoContinuities() != null){
			Collection<BitemporalInciso> list = cotizacion.getContinuity().getIncisoContinuities().get(TimeUtils.getDateTime(validoDesde));
			if(list != null){
				for (BitemporalInciso bitInciso : list) {
					
					//FIXME Esto es una solucion temporal y 'a medias' considerando la urgrncia con la que se quiere evitar el error de los bitemporales incorrectos.
					//Se tiene que ver una solucion completa y de raiz que implica quitar por completo este metodo
					if (!bitInciso.getRecordStatus().equals(RecordStatus.TO_BE_ENDED)) continue;
					
					
					entidadBitemporalService.attachToParentAndSet(bitInciso, cotizacion, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
					cotizacion.getContinuity().getIncisoContinuities().getValue().add(bitInciso.getContinuity());
					//entidadBitemporalService.saveInProcess(bitInciso,validOn,validoHastaF);
				
					//Se realiza la actualización de BitemporalAutoInciso
					if(bitInciso.getContinuity().getAutoIncisoContinuity() != null){
						BitemporalAutoInciso bitAutoInc =  bitInciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().get(TimeUtils.getDateTime(validoDesde));
						if(bitAutoInc != null){
							entidadBitemporalService.attachToParentAndSet(bitAutoInc, bitInciso, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
							bitInciso.getContinuity().setAutoIncisoContinuity(bitAutoInc.getContinuity());
							//entidadBitemporalService.saveInProcess(bitAutoInc,validOn,validoHastaF);
						}
					}
					
					//Se realiza la actualización de BitemporalSeccionInciso
					if(bitInciso.getContinuity().getIncisoSeccionContinuities() != null){
						Collection<BitemporalSeccionInciso> listAnida = bitInciso.getContinuity().getIncisoSeccionContinuities().get(TimeUtils.getDateTime(validoDesde));
						if(list != null){
							for (BitemporalSeccionInciso bitSeccionInc : listAnida) {
								entidadBitemporalService.attachToParentAndSet(bitSeccionInc, bitInciso, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
								bitInciso.getContinuity().getIncisoSeccionContinuities().getValue().add(bitSeccionInc.getContinuity());
								//entidadBitemporalService.saveInProcess(bitSeccionInc,validOn,validoHastaF);
							
							
								//Se realiza la actualización de BitemporalSubIncisoSeccion
								if(bitSeccionInc.getContinuity().getSubIncisoSeccionContiniuties() != null){
									Collection<BitemporalSubIncisoSeccion> listSubIncisoSecc =  bitSeccionInc.getContinuity().getSubIncisoSeccionContiniuties2().get(TimeUtils.getDateTime(validoDesde));
									if(listSubIncisoSecc != null){
										for (BitemporalSubIncisoSeccion bitSubIncSec : listSubIncisoSecc) {
											entidadBitemporalService.attachToParentAndSet(bitSubIncSec, bitSeccionInc, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
											bitSeccionInc.getContinuity().getSubIncisoSeccionContiniuties().add(bitSubIncSec.getContinuity());
											//entidadBitemporalService.saveInProcess(bitSubIncSec,validOn,validoHastaF);
										}
									}
								}
							
								//Se realiza la actualización de BitemporalDatoSeccion
								if(bitSeccionInc.getContinuity().getDatoSeccionContinuities() != null){
									Collection<BitemporalDatoSeccion> listDatoSecc =   bitSeccionInc.getContinuity().getDatoSeccionContinuities2().get(TimeUtils.getDateTime(validoDesde));
									if(listDatoSecc != null){
										for (BitemporalDatoSeccion bitDatoSec : listDatoSecc) {
											entidadBitemporalService.attachToParentAndSet(bitDatoSec, bitSeccionInc, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
											bitSeccionInc.getContinuity().getDatoSeccionContinuities().add(bitDatoSec.getContinuity());
											//entidadBitemporalService.saveInProcess(bitDatoSec,validOn,validoHastaF);
										}
									}
								}
								
								//Se realiza la actualización de BitemporalCoberturaSeccion
								if(bitSeccionInc.getContinuity().getCoberturaSeccionContinuities()!= null){
									Collection<BitemporalCoberturaSeccion> listCoberturaSecc =   bitSeccionInc.getContinuity().getCoberturaSeccionContinuities().get(TimeUtils.getDateTime(validoDesde));
									if(listCoberturaSecc != null){
										for (BitemporalCoberturaSeccion bitCoberturaSec : listCoberturaSecc) {
											entidadBitemporalService.attachToParentAndSet(bitCoberturaSec, bitSeccionInc, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
											bitSeccionInc.getContinuity().getCoberturaSeccionContinuities().getValue() .add(bitCoberturaSec.getContinuity());
											//entidadBitemporalService.saveInProcess(bitCoberturaSec,validOn,validoHastaF);
										
											//Se realiza la actualización de BitemporalCoberturaRecargo
											if(bitCoberturaSec.getContinuity().getCoberturaRecargoContinuities() != null){
												Collection<BitemporalCoberturaRecargo> listCoberturaRecargo =   bitCoberturaSec.getContinuity().getCoberturaRecargoContinuities().get(TimeUtils.getDateTime(validoDesde));
												if(listCoberturaRecargo != null){
													for (BitemporalCoberturaRecargo bitCoberturaRecargo : listCoberturaRecargo) {
														entidadBitemporalService.attachToParentAndSet(bitCoberturaRecargo, bitCoberturaSec, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
														bitCoberturaSec.getContinuity().getCoberturaRecargoContinuities().getValue().add(bitCoberturaRecargo.getContinuity());
														//entidadBitemporalService.saveInProcess(bitCoberturaRecargo,validOn,validoHastaF);
													}
												}
											}
											
											//Se realiza la actualización de BitemporalCoberturaAumento
											if(bitCoberturaSec.getContinuity().getCoberturaAumentoContinuities() != null){
												Collection<BitemporalCoberturaAumento> listCoberturaAumento =   bitCoberturaSec.getContinuity().getCoberturaAumentoContinuities().get(TimeUtils.getDateTime(validoDesde));
												if(listCoberturaAumento != null){
													for (BitemporalCoberturaAumento bitCoberturaAumento : listCoberturaAumento) {
														entidadBitemporalService.attachToParentAndSet(bitCoberturaAumento, bitCoberturaSec, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
														bitCoberturaSec.getContinuity().getCoberturaAumentoContinuities().getValue().add(bitCoberturaAumento.getContinuity());
														//entidadBitemporalService.saveInProcess(bitCoberturaAumento,validOn,validoHastaF);
													}
												}
											}
										
											//Se realiza la actualización de BitemporalCoberturaDescuento
											if(bitCoberturaSec.getContinuity().getCoberturaDescuentoContinuities() != null){
												Collection<BitemporalCoberturaDescuento> listCoberturaDescuento =   bitCoberturaSec.getContinuity().getCoberturaDescuentoContinuities().get(TimeUtils.getDateTime(validoDesde));
												if(listCoberturaDescuento != null){
													for (BitemporalCoberturaDescuento bitCoberturaDescuento : listCoberturaDescuento) {
														entidadBitemporalService.attachToParentAndSet(bitCoberturaDescuento, bitCoberturaSec, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
														bitCoberturaSec.getContinuity().getCoberturaDescuentoContinuities().getValue().add(bitCoberturaDescuento.getContinuity());
														//entidadBitemporalService.saveInProcess(bitCoberturaDescuento,validOn,validoHastaF);
													}
												}
											}
										
											//Se realiza la actualización de BitemporalCoberturaDetallePrima
											if(bitCoberturaSec.getContinuity().getCoberturaDetallePrimaContinuities() != null){
												Collection<BitemporalCoberturaDetallePrima> listCoberturaDetallePrima =   bitCoberturaSec.getContinuity().getCoberturaDetallePrimaContinuities().get(TimeUtils.getDateTime(validoDesde));
												if(listCoberturaDetallePrima != null){
													for (BitemporalCoberturaDetallePrima bitCoberturaDetallePrima : listCoberturaDetallePrima) {
														entidadBitemporalService.attachToParentAndSet(bitCoberturaDetallePrima, bitCoberturaSec, TimeUtils.getDateTime(validoDesde),TimeUtils.getDateTime(validoHasta));
														bitCoberturaSec.getContinuity().getCoberturaDetallePrimaContinuities().getValue().add(bitCoberturaDetallePrima.getContinuity());
														//entidadBitemporalService.saveInProcess(bitCoberturaDetallePrima,validOn,validoHastaF);
													}
												}
											}										
										}
									}
								}							
							}
						}
					}				
				}
			}
		}		
	}
	
	@Override
	public Long generarSolicitudAutorizacion(BitemporalCotizacion cotizacion, Long usuarioSolicitante, Long usuarioAutorizador) {		
		
		List<ExcepcionSuscripcionReporteDTO> excepcionesList = new ArrayList<ExcepcionSuscripcionReporteDTO>();
		ExcepcionSuscripcionReporteDTO excepcionSuscripcionReporteDTO = new ExcepcionSuscripcionReporteDTO();
		ExcepcionSuscripcion excepcionSuscripcion = entidadService.findById(ExcepcionSuscripcion.class, new Short(ExcepcionSuscripcionService.TipoExcepcionFija.ENDOSO_INCLUSION_TEXTO.valor()).longValue()); //Endoso de Inclusion de Texto
		
		excepcionSuscripcionReporteDTO.setIdExcepcion(excepcionSuscripcion.getId());
		excepcionSuscripcionReporteDTO.setDescripcionExcepcion(excepcionSuscripcion.getDescripcion());
		excepcionSuscripcionReporteDTO.setIdToCotizacion(new BigDecimal(cotizacion.getEntidadContinuity().getNumero()));
		excepcionSuscripcionReporteDTO.setDescripcionRegistroConExcepcion("ENDOSO DE INCLUSI\u00D3N DE TEXTO");
		excepcionesList.add(excepcionSuscripcionReporteDTO);
		
		Long idSolicitud = null;
		if (!cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess())
		{
			idSolicitud = autorizacionService.guardarSolicitudDeAutorizacion(new BigDecimal(cotizacion.getEntidadContinuity().getNumero()), "", 
					usuarioSolicitante, usuarioAutorizador, 
					excepcionSuscripcion.getCveNegocio(), excepcionesList);			
		}		
		
		return idSolicitud;
	}

	@SuppressWarnings("rawtypes")
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void cancelarSolicitudesAutorizacion(BitemporalCotizacion cotizacion) {
		
		List<SolicitudExcepcionCotizacion> listaSolicitudes = autorizacionService.listarSolicitudesInvalidas(new BigDecimal(cotizacion.getEntidadContinuity().getNumero()));
		if(listaSolicitudes!=null && !listaSolicitudes.isEmpty()){
			Iterator itListaSolicitudes = listaSolicitudes.iterator();
			while(itListaSolicitudes.hasNext())
			{
				Long id = (Long)itListaSolicitudes.next();
				SolicitudExcepcionCotizacion solicitudExcepcionCotizacion =entidadService.findById(SolicitudExcepcionCotizacion.class, id);
				List<SolicitudExcepcionDetalle> listadoDetalle = entidadService.findByProperty(SolicitudExcepcionDetalle.class, "solicitudExcepcionCotizacion.id", id);
				Iterator<SolicitudExcepcionDetalle> itListadoDetalle  =  listadoDetalle.iterator();
				while(itListadoDetalle.hasNext())
				{
					SolicitudExcepcionDetalle solicitudExcepcionDetalle = itListadoDetalle.next();
					
					autorizacionService.cancelarDetalle(solicitudExcepcionCotizacion.getId(), solicitudExcepcionDetalle.getId(), "CANCELACION COTIZACION ENDOSO");
				}		
				Long usuarioAutorizacion = solicitudExcepcionCotizacion.getUsuarioAutorizador();
				try{
					Usuario usuario = usuarioService.getUsuarioActual();
					if(usuario != null){
						usuarioAutorizacion = usuario.getId().longValue();
					}
				}catch(Exception e){
					
				}
				
				autorizacionService.terminarSolicitud(solicitudExcepcionCotizacion.getId(), usuarioAutorizacion);
			}	
		}
	}	
	
	@Override
	public void guardarCobranzaCotizacion(Long cotizacionContinuityId, Long idToPoliza, DateTime validoEn, Short claveAfectacionPrimas, Short tipoEndoso) {
		
		BitemporalCotizacion cotizacion = null;
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA) {
			cotizacion = entidadBitemporalService.getByKey(CotizacionContinuity.class, cotizacionContinuityId, validoEn);
		} else {
			cotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, cotizacionContinuityId, validoEn);
		}
		
		cotizacion.getValue().setClaveAfectacionPrimas(claveAfectacionPrimas);
		cotizacion.getValue().setPolizaReferencia(idToPoliza);
		
		entidadService.save(cotizacion.getEntidadContinuity());
	}
	
	@Override
	public void validaCotizacionConPrima(BitemporalCotizacion cotizacion, String[] continuitiesIds) {
		
		SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		
		if (solicitud.getClaveTipoEndoso().compareTo(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS)) == 0) {
			if (!endosoDao.getExistenCambiosEnCoberturas(cotizacion.getEntidadContinuity().getId()) &&
					!endosoDao.getExistenCambiosEnInciso(cotizacion.getEntidadContinuity().getId()) &&
					!endosoDao.getExistenCambiosEnDatosRiesgo(cotizacion.getEntidadContinuity().getId())) {
				//No hubo movimientos
				throw new NegocioEJBExeption("NE_18",new Object[]{});
			}
		} else if (solicitud.getClaveTipoEndoso().compareTo(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO)) == 0 || 
				solicitud.getClaveTipoEndoso().compareTo(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL)) == 0) {
			
			DateTime validoEn = TimeUtils.getDateTime(solicitud.getFechaInicioVigenciaEndoso());
			
			if (continuitiesIds != null && continuitiesIds.length > 0) {

				Long totalIncisos = listadoIncisosDinamicoService.getNumeroIncisos(cotizacion.getContinuity().getId(), validoEn, TimeUtils.now(), false);
				//if (cotizacion.getEntidadContinuity().getIncisoContinuities().get(validoEn).size() == continuitiesIds.length) {
				if (totalIncisos.intValue() == continuitiesIds.length) {
					//No se deben cancelar todos los incisos
					throw new NegocioEJBExeption("NE_19",new Object[]{});
				}
				
			} else {
				//Debe haber al menos un inciso para dar de baja
				throw new NegocioEJBExeption("NE_20",new Object[]{});
			}
		}
	}
	
	@Override
	public void validaBajaIncisos(BitemporalCotizacion cotizacion, String[] continuitiesIds) {
		
		SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());		
		if (solicitud.getClaveTipoEndoso().compareTo(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO)) == 0 || 
				solicitud.getClaveTipoEndoso().compareTo(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL)) == 0) {
			//Solo se puede dar de baja el inciso si esta vivo en la fecha fin de vigencia 
			//GregorianCalendar gcFechaValidez = new GregorianCalendar();
			//gcFechaValidez.setTime(cotizacion.getValue().getFechaFinVigencia());
			//gcFechaValidez.add(GregorianCalendar.SECOND, -1);
			
			if (continuitiesIds != null && continuitiesIds.length > 0) {
				
				//Busca Poliza
				List<PolizaDTO> polizaList = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", cotizacion.getContinuity().getNumero());
				BigDecimal idToPoliza = null;
				if(polizaList != null && !polizaList.isEmpty()){
					idToPoliza = polizaList.get(0).getIdToPoliza();
				}
				
	        	for(String idString:continuitiesIds)
	    		{
	    			//BitemporalInciso bitemporalInciso = new BitemporalInciso();			
	    			//bitemporalInciso = incisoService.getInciso(cotizacion.getContinuity().getId(), Long.valueOf(idString), TimeUtils.getDateTime(gcFechaValidez.getTime()));
	        		EndosoDTO endoso = incisoService.getUltimoEndosoInciso(idToPoliza, Long.valueOf(idString));
					//if (bitemporalInciso == null) {
					if (endoso != null && endoso.getClaveTipoEndoso().equals(EndosoDTO.TIPO_ENDOSO_CANCELACION_INCISO)) {
						//No se deben dar de baja incisos ya cancelados
						throw new NegocioEJBExeption("NE_22",new Object[]{});	
					}
			
	    		}
			} 
		}
	}	
	
	@SuppressWarnings("unchecked")
	@Override
	/**
	 * Obtener el ultimo endoso a la situacion actual
	 */
	public EndosoDTO getUltimoEndoso(Long idToPoliza, Date validFrom) {
		
		EndosoDTO endoso = null;
		
		String query = "SELECT endoso FROM EndosoDTO endoso WHERE endoso.id.idToPoliza = :idToPoliza"
		 + " AND endoso.validFrom <= :validFrom ORDER BY endoso.id.numeroEndoso desc" ;
		
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("idToPoliza", idToPoliza);
		properties.put("validFrom", validFrom);
		

		List<EndosoDTO> lstEndoso = entidadService.executeQueryMultipleResult(query, properties);		

		if (!lstEndoso.isEmpty()) {
			endoso = lstEndoso.get(0);		
		}
		return endoso;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	/**
	 * Obtener el endoso con maximo validFrom a la situacion actual
	 */
	public EndosoDTO getUltimoEndosoByValidFrom(Long idToPoliza, Date validFrom) {
		
		EndosoDTO endoso = null;
		
		String query = "SELECT endoso FROM EndosoDTO endoso WHERE endoso.id.idToPoliza = :idToPoliza"
		 + " AND endoso.validFrom <= :validFrom " +
		 "ORDER BY endoso.validFrom desc, endoso.recordFrom desc" ;
		
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("idToPoliza", idToPoliza);
		properties.put("validFrom", validFrom);
		

		List<EndosoDTO> lstEndoso = entidadService.executeQueryMultipleResult(query, properties);		

		if (!lstEndoso.isEmpty()) {
			endoso = lstEndoso.get(0);		
		}
		return endoso;
	}
	
	@Override
	public void deleteBitemporalTexAdicionalCot(BitemporalTexAdicionalCot biCotizacion){
		biCotizacion = entidadBitemporalService.getInProcessByKey(TexAdicionalCotContinuity.class, biCotizacion.getContinuity().getId(), new DateTime(biCotizacion.getValue().getFechaCreacion()));
		if(biCotizacion != null){
			entidadBitemporalService.remove(biCotizacion, biCotizacion.getValidityInterval().getInterval().getStart());
		}
	}
	/**
	 * Valida si se trata de una poliza Flotilla Migrada, y determina si puede o no proceder el endoso
	 * @param idTopoliza id de la poliza a validar
	 * @return Si el endoso puede proceder true, de lo contrario false
	 */
	@Override
	public boolean validarRestriccionEndososPolizaFlotillaMigrada(BigDecimal idTopoliza)
	{
		boolean procedeEndoso = true;
		
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId();
		parametroGeneralId.setIdToGrupoParametroGeneral(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES);
		parametroGeneralId.setCodigoParametroGeneral(ParametroGeneralDTO.CODIGO_PARAM_GENERAL_RESTRICCION_ENDOSOS_FLOTILLAS_MIGRADAS);
		
		ParametroGeneralDTO parametroGeneral = null;
		
		try {
			parametroGeneral = parametroGeneralService
					.findById(parametroGeneralId);
		} catch (Exception e) {
			LogDeMidasEJB3
					.log("Error al consultar parametro general  GRUPO: "
							+ ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES
									.longValue()
							+ " CODIGO: "
							+ ParametroGeneralDTO.CODIGO_PARAM_GENERAL_RESTRICCION_ENDOSOS_FLOTILLAS_MIGRADAS
									.longValue(), Level.SEVERE, e);
		}
		
		if(parametroGeneral != null && (parametroGeneral.getValor().trim()).equalsIgnoreCase("1")) 
		{
			//Valida Restriccion
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class,idTopoliza);
			
			if((polizaDTO.getCodigoProducto().trim()).equalsIgnoreCase(ProductoDTO.AUTOMOVILES_FLOTILLA) && 
					(polizaDTO.getClavePolizaSeycos() != null && !polizaDTO.getClavePolizaSeycos().isEmpty()))
			{
				procedeEndoso = false;			
			}			
		}		
		
		return procedeEndoso;
	}

	@Override
	public BitemporalCotizacion getCotizacionEndosoBajaCondiciones(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}

	@Override
	public BitemporalCotizacion getCotizacionEndosoInclusionCondiciones(BigDecimal polizaId, Date fechaInicioVigencia, String accionEndoso) {
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, accionEndoso);
	}
	


	@Override
	public void guardaCotizacionBajaCondiciones(BitemporalCotizacion cotizacion, DateTime validoEn) {
		
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
		if (!cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {			
			ControlEndosoCot controlEndosoCot = saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO), cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
			entidadBitemporalService.saveInProcess(cotizacion, validoEn);
			if(controlEndosoCot!=null){
				movimientoEndosoBitemporalService.calcular(controlEndosoCot);
			}
		}
		
	}

	@Override
	public void guardaCotizacionInclusionCondiciones(BitemporalCotizacion cotizacion, DateTime validoEn) {
		
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
		if (!cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {			
			ControlEndosoCot controlEndosoCot = saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO), cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
			entidadBitemporalService.saveInProcess(cotizacion, validoEn);
			if(controlEndosoCot!=null){
				movimientoEndosoBitemporalService.calcular(controlEndosoCot);
			}
		}
		
	}
	
	@Override
	public Short getCotizacionEndosoPerdidaTotal(BigDecimal idToPoliza,
			Date validoEn,  Short tipoEndoso) {
		
		validaStatusPoliza(idToPoliza);
		
		validaNoTieneRecordsEnProceso(idToPoliza);
		
		//se obtiene la poliza
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,	idToPoliza);
		if (poliza == null) {
			throw new IllegalArgumentException("[initCotizacionEndoso] El id de la póliza es nula.");
		}
		
		return this.validarTipoEndosoPerdidaTotal(poliza,validoEn,tipoEndoso);			
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BitemporalCotizacion prepareCotizacionEndosoPerdidaTotal(BigDecimal idToPoliza,
			Date fechaInicioVigencia, Short tipoEndoso,  String tipoIndemnizacion, String accionEndoso, IncisoContinuity incisoContinuity)
	{
		return initCotizacionEndosoPerdidaTotal(idToPoliza,
				fechaInicioVigencia, tipoEndoso, tipoIndemnizacion, accionEndoso, incisoContinuity);
	}
	
	/**
	 * Genera la solicitud del endoso de perdida total y la agrega a la cotizacion.
	 * 
	 * @param idToPoliza id de la poliza sobre la cual se realiza el endoso.
	 * @param fechaInicioVigencia fecha de inicio de vigencia del endoso.
	 * @param tipoEndoso tipo de endoso de Perdida Total. Null para dejar que el metodo determine el tipo adecuado.
	 * @param tipoIndemnizacion el tipo de indemnizacion del siniestro que declara la perdida total.
	 * @param accionEndoso tipo de accion que se esta realizando
	 * @return BitemporalCotizacion La cotizacion con la nueva solicitud de endoso. 
	 */
	protected BitemporalCotizacion initCotizacionEndosoPerdidaTotal(BigDecimal idToPoliza,
			Date fechaInicioVigencia, Short tipoEndoso, String tipoIndemnizacion, String accionEndoso,IncisoContinuity incisoContinuity)
	{
		BitemporalCotizacion bitemporalCotizacion = null;
		Short tipoEndosoParametro = tipoEndoso;  
		
		//se obtiene la poliza
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class,
				idToPoliza);
		if (poliza == null) {
			return null;
		}
		
		// Se obtiene motivoEndoso
		short motivoEndoso = SolicitudDTO.CVE_MOTIVO_ENDOSO_INDEFINIDO;
		
		if(tipoIndemnizacion.equalsIgnoreCase(OrdenCompraService.TIPO_INDEMNIZACION_PERDIDATOTAL))
		{
			motivoEndoso = SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL;
			
		}else if(tipoIndemnizacion.equalsIgnoreCase(OrdenCompraService.TIPO_INDEMNIZACION_PAGO_DANIOS))   
		{
			motivoEndoso = SolicitudDTO.CVE_MOTIVO_ENDOSO_PAGO_DANIOS_SUSTITUCION_PERDIDA_TOTAL;			
		}
		
		//Determinar automaticamente el tipo de endoso que debe aplicar.
		tipoEndoso = this.validarTipoEndosoPerdidaTotal(poliza,fechaInicioVigencia,tipoEndoso);	
				
		String[] continuitiesStringIds ={String.valueOf(incisoContinuity.getId())};
		
		Boolean bloquearFlujo = tipoEndosoParametro != null ? Boolean.TRUE:Boolean.FALSE;
		EstatusValidacionRecibosPT estatusValidacion = this.validarRecibosEndosoPT(poliza, incisoContinuity, 
				fechaInicioVigencia, motivoEndoso, bloquearFlujo);
		
		if(estatusValidacion == EstatusValidacionRecibosPT.RECIBOSAGRUPADOS)
		{
			// Generar un endoso de Desagrupacion de recibos automaticamente.
			emisionPendienteService.generarEndosoDesagrupacionRecibosAutomatico(idToPoliza, fechaInicioVigencia, 
					incisoContinuity);
			
			try {
				LogDeMidasEJB3.log("En espera para procesar el endoso de PT con un RecordFrom distinto al endoso de agrupacion. ", Level.INFO,null);
			    TimeUnit.SECONDS.sleep(1);
			} catch (Exception e) {
			   e.printStackTrace();
			}							  
		}	
		
		//Debe generarse hasta este punto ya que potencialmente se pudo haber generado un endoso de Desagrupacion de Recibos
		bitemporalCotizacion = this.initCotizacionEndoso(poliza,   
				 fechaInicioVigencia, tipoEndoso, motivoEndoso, accionEndoso,  EmisionPendiente.POLIZA_NO_MIGRADA);		
		
		this.validaBajaIncisos(bitemporalCotizacion, continuitiesStringIds);
		
		
		return bitemporalCotizacion;
	}
	
	/**
	 * Determina cual es el tipo de endoso de pérdida total que debe aplicar con respecto al tipo de poliza y numero de incisos vigentes.
	 * Si se proporciona un valor en el parametro tipoEndosoPTValidar, entonces se valida que el endoso de Perdida Total determinado por el 
	 * método sea igual al proporcionado, en caso de ser distinto se arroja una excepcion indicando el tipo de endoso que se debe aplicar. 
	 * 
	 * @param poliza poliza a validar
	 * @param validoEn fecha en que se desea validar la situacion de los incisos de la poliza
	 * @param tipoEndoso tipo de endoso de Perdida Total que se desea validar. Null si no se desea validar un tipo en especifico.
	 * @return tipo de endoso de perdida total adecuado para el tipo de poliza y numero de incisos vigentes.
	 */
	private Short validarTipoEndosoPerdidaTotal(PolizaDTO poliza, Date validoEn, Short tipoEndosoPTValidar)
	{
		DateTime validoEnDateTime = TimeUtils.getDateTime(validoEn);
		int numeroIncisosVigentes = 0;
		Short tipoEndoso = 0;		
		
		BitemporalCotizacion cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, 
				poliza.getCotizacionDTO().getIdToCotizacion(), validoEnDateTime);	
		
		Collection<BitemporalInciso> incisos = cotizacion.getContinuity().getIncisoContinuities().getInProcess(validoEnDateTime);
		if(incisos != null)
		{
			numeroIncisosVigentes = incisos.size();
		}
		
		
		if (poliza.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas() == 0 ||
				numeroIncisosVigentes == 1)//Poliza individual o flotilla con un solo inciso vigente.
		{
			tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL;
			
			if(tipoEndosoPTValidar != null && tipoEndosoPTValidar > 0 && tipoEndosoPTValidar != tipoEndoso)
			{
				throw new NegocioEJBExeption("NE_24",new Object[]{}); //Debe manejarse como endoso Baja Inciso por PT	
								
			}
			
		}else if (numeroIncisosVigentes > 1 )//Poliza flotilla con mas de un inciso vigente.
		{
			tipoEndoso = SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL;
			
			if(tipoEndosoPTValidar != null && tipoEndosoPTValidar > 0 && tipoEndosoPTValidar != tipoEndoso)
			{
				throw new NegocioEJBExeption("NE_23",new Object[]{}); // Debe manejarse como endoso Cancelacion Poliza por PT									
			}
		}
		
		return tipoEndoso;
	}
	
	private EstatusValidacionRecibosPT validarRecibosEndosoPT(PolizaDTO poliza, IncisoContinuity incisoContinuity, Date validoEn, 
			Short motivoEndoso, Boolean bloquearFlujo)
	{
		EstatusValidacionRecibosPT estatus = EstatusValidacionRecibosPT.RECIBOSOK;
		
		if(incisoContinuity.getNumero()== null)
		{
			incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuity.getId());			
		}        
		
		//	Invocar servicio validacion recibos agrupados del inciso.		
		if(poliza.getCotizacionDTO() != null && poliza.getCotizacionDTO().getTipoPolizaDTO().getClaveAplicaFlotillas() == 1)
		{
			if(!endosoDao.validarRecibosDesagrupados(poliza.getIdToPoliza(), incisoContinuity.getNumero()))
			{
				estatus = EstatusValidacionRecibosPT.RECIBOSAGRUPADOS;					
			}			
		}	
		
		//	Invocar servicio validacion pagos realizados.		
		Date fechaValidacion = null;
		String complementoMsjExcepcion = "Deben estar pagados los recibos correspondientes a toda la vigencia de la póliza";
		
		if(motivoEndoso.compareTo(SolicitudDTO.CVE_MOTIVO_ENDOSO_PAGO_DANIOS_SUSTITUCION_PERDIDA_TOTAL) == 0)
		{
			fechaValidacion = validoEn;
			complementoMsjExcepcion = "Deben estar pagados los recibos hasta " +
					"la fecha de ocurrencia del siniestro. " + Utilerias.cadenaDeFecha(fechaValidacion);
		}
		
		if(this.validarPrimasPendientesPagoInciso(poliza.getIdToPoliza(), 
				incisoContinuity.getNumero(), fechaValidacion).compareTo(BigDecimal.ZERO) > 0)
		{
			estatus = (estatus == EstatusValidacionRecibosPT.RECIBOSAGRUPADOS)?
					EstatusValidacionRecibosPT.RECIBOSAGRUPADOS:EstatusValidacionRecibosPT.RECIBOSPENDIENTESDEPAGO;				
		}else
		{
			estatus = EstatusValidacionRecibosPT.RECIBOSOK; 
		}		
		
		if(bloquearFlujo && estatus != EstatusValidacionRecibosPT.RECIBOSOK)
		{
			List<String> params = new ArrayList<String>();
			params.add(String.valueOf(incisoContinuity.getNumero()));
			params.add(poliza.getNumeroPolizaFormateada());
			
			if(estatus == EstatusValidacionRecibosPT.RECIBOSAGRUPADOS)
			{
				throw new NegocioEJBExeption("NE_25",params.toArray());	
				
			}else //Estatus RECIBOSPENDIENTESDEPAGO
			{				
				params.add(complementoMsjExcepcion);				
				throw new NegocioEJBExeption("NE_26",params.toArray());				
			}
		}
		
		return estatus;
	}
	
	public void getCotizacionEndosoDesagrupacionRecibos(BigDecimal idToPoliza,Date validoEn, Short tipoEndoso)
	{			
			validaStatusPoliza(idToPoliza);
			
			validaNoTieneRecordsEnProceso(idToPoliza);
			
			validaAplicaFlotilla(idToPoliza, tipoEndoso);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BitemporalCotizacion prepareCotizacionEndosoDesagrupacionRecibos(BigDecimal polizaId,Date fechaInicioVigencia, String accionEndoso)
	{
		return this.initCotizacionEndoso(polizaId, fechaInicioVigencia, 
				SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS, SolicitudDTO.CVE_MOTIVO_ENDOSO_INDEFINIDO, accionEndoso);		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardaCotizacionEndosoDesagrupacionRecibosAutomatica(PolizaDTO poliza, BitemporalCotizacion cotizacion, IncisoContinuity incisoContinuity,
			BitemporalAutoInciso bitemporalAutoInciso)
	{
		this.guardaCotizacionEndosoDesagrupacionRecibos(poliza, cotizacion, incisoContinuity, bitemporalAutoInciso);
	}
		
	public void guardaCotizacionEndosoDesagrupacionRecibos(PolizaDTO poliza, BitemporalCotizacion cotizacion, IncisoContinuity incisoContinuity,
			BitemporalAutoInciso bitemporalAutoInciso)
	{
		if(incisoContinuity.getNumero()== null)
		{
			incisoContinuity = entidadContinuityService.findContinuityByKey(IncisoContinuity.class, incisoContinuity.getId());			
		}
	
		//se valida que los recibos no se encuentren ya desagrupados.
		if(endosoDao.validarRecibosDesagrupados(poliza.getIdToPoliza(), incisoContinuity.getNumero()) 
				|| (bitemporalAutoInciso.getValue().getTipoAgrupacionRecibosInciso() != null &&  
				bitemporalAutoInciso.getValue().getTipoAgrupacionRecibosInciso()
				.equals(CotizacionDTO.TIPO_AGRUPACION_RECIBOS.UBICACION.getValue())))
		{
			throw new NegocioEJBExeption("NE_27",new ArrayList<Object>().toArray());										
		}
					
		//Establece el tipo de de agrupacion recibos por inciso.
		bitemporalAutoInciso.getValue().setTipoAgrupacionRecibosInciso(CotizacionDTO.TIPO_AGRUPACION_RECIBOS.UBICACION.getValue());
				
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, cotizacion.getValue().getSolicitud().getIdToSolicitud());
		
		cotizacion.getValue().setSolicitud(solicitudDTO);
		cotizacion.getValue().setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
		entidadBitemporalService.saveInProcess(cotizacion,TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
				
		entidadBitemporalService.saveInProcess(bitemporalAutoInciso, TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()));
		
		ControlEndosoCot controlEndoso = obtenerControlEndosoCotCotizacion(new BigDecimal(cotizacion.getContinuity().getNumero()));
		if(controlEndoso == null || !controlEndoso.getClaveEstatusCot().equals(new Long(CotizacionDTO.ESTATUS_COT_EN_PROCESO))){
			saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO),cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso());
		}		
	}
	
	public List<EndosoIDTO> validaEndosoPT (BigDecimal idToPoliza, Integer numeroInciso, Date fechaInicioVigencia, Short motivoEndoso) {
	
		return endosoFacadeInterfaz.validaEndosoPT(idToPoliza, numeroInciso, fechaInicioVigencia, motivoEndoso);		
	}	
	
	public BigDecimal validarPrimasPendientesPagoInciso(BigDecimal idToPoliza,
			Integer numeroInciso, Date fechaValidacion)
	{
		return endosoDao.validarPrimasPendientesPagoInciso(idToPoliza, 
				numeroInciso, fechaValidacion);		
	}
	
	@Override
	public void actualizaAgrupadoresEndoso(BigDecimal idToPoliza, BigDecimal numeroEndoso){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Act_Agrup_Tarifa";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pNumeroEndoso", numeroEndoso);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}	
	
	
	
	@Override
	public boolean validarEndososPorMigrarSeycos(BigDecimal idTopoliza)
	{
		boolean procedeEndoso = false;
		
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId();
		parametroGeneralId.setIdToGrupoParametroGeneral(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES);
		parametroGeneralId.setCodigoParametroGeneral(ParametroGeneralDTO.CODIGO_PARAM_GENERAL_RESTRICCION_ENDOSOS_PEND_MIGRACION_SEYCOS);
		
		ParametroGeneralDTO parametroGeneral = null;
		
		try {
			parametroGeneral = parametroGeneralService.findById(parametroGeneralId);
		} catch (Exception e) {
			LogDeMidasEJB3
					.log("Error al consultar parametro general  GRUPO: "
							+ ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES.longValue()
							+ " CODIGO: "
							+ ParametroGeneralDTO.CODIGO_PARAM_GENERAL_RESTRICCION_ENDOSOS_PEND_MIGRACION_SEYCOS.longValue(), Level.SEVERE, e);
		}
		
		if(parametroGeneral != null && (parametroGeneral.getValor().trim()).equalsIgnoreCase("1")) 
		{
			//Valida Restriccion
			BigDecimal idSeycos = polizaFacadeRemote.obtenerNumeroCotizacionSeycos(idTopoliza);
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("id.idCotizacion", idSeycos);
			params.put("claveProceso", MigEndososValidosEd.CLAVE_PROCESO_NO_PROCESADO);
			
			Long total = entidadService.findByPropertiesCount(MigEndososValidosEd.class, params);
			
			if(total != null && total.intValue() > 0)
			{
				procedeEndoso = true;			
			}			
		}		
		
		return procedeEndoso;
	}
	
	@Override
	public void bloquearMigracionSeycos(String identificador){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Bloqueo_Migracion";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdentificador", identificador);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	@Override
	public void igualarEndosoMidas(BigDecimal pnum_poliza, BigDecimal pnum_renov, BigDecimal pnum_endoso, 
			BigDecimal varPrimaNeta, BigDecimal varBonifComis, BigDecimal varRecargos, BigDecimal varBonifComisRPF,
			BigDecimal varDerechos, BigDecimal varIva, BigDecimal varPrimaTotal, BigDecimal varComisPN,
			BigDecimal varComisRPF){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Igualar_Endoso";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pnum_poliza", pnum_poliza);
			storedHelper.estableceParametro("pnum_renov", pnum_renov);
			storedHelper.estableceParametro("pnum_endoso", pnum_endoso);
			storedHelper.estableceParametro("pPrimaNeta", varPrimaNeta);
			storedHelper.estableceParametro("pBonifComis", varBonifComis);
			storedHelper.estableceParametro("PRecargos", varRecargos);
			storedHelper.estableceParametro("pBonifComisRPF", varBonifComisRPF);
			storedHelper.estableceParametro("pDerechos", varDerechos);
			storedHelper.estableceParametro("pIva", varIva);
			storedHelper.estableceParametro("pPrimaTotal", varPrimaTotal);
			storedHelper.estableceParametro("pComisPN", varComisPN);
			storedHelper.estableceParametro("pComisRPF", varComisRPF);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	@Override
	public void migrarEndosoMidas(String identificador){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Migrar_Endoso";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdentificador", identificador);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	@Override
	public void regeneraPrimaRecibo(BigDecimal pId_Recibo,  
			BigDecimal varPrimaNeta, BigDecimal varBonifComis, BigDecimal varRecargos, BigDecimal varBonifComisRPF,
			BigDecimal varDerechos, BigDecimal varIva, BigDecimal varPrimaTotal, BigDecimal varComisPN,
			BigDecimal varComisRPF){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Regenera_Prima_Recibo";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			
			storedHelper.estableceParametro("pId_Recibo", pId_Recibo);
			storedHelper.estableceParametro("pPrimaNeta", varPrimaNeta);
			storedHelper.estableceParametro("pBonifComis", varBonifComis);
			storedHelper.estableceParametro("PRecargos", varRecargos);
			storedHelper.estableceParametro("pBonifComisRPF", varBonifComisRPF);
			storedHelper.estableceParametro("pDerechos", varDerechos);
			storedHelper.estableceParametro("pIva", varIva);
			storedHelper.estableceParametro("pPrimaTotal", varPrimaTotal);
			storedHelper.estableceParametro("pComisPN", varComisPN);
			storedHelper.estableceParametro("pComisRPF", varComisRPF);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	@Override
	public void regeneraRecibo(BigDecimal pId_Recibo){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Regenera_Recibo";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pId_Recibo", pId_Recibo);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	@Override
	public void setLlaveFiscal(BigDecimal pIdToPoliza, BigDecimal pNumeroEndoso, String pllaveFiscal){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Set_Llave_Fiscal";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", pIdToPoliza);
			storedHelper.estableceParametro("pNumeroEndoso", pNumeroEndoso);
			storedHelper.estableceParametro("pllaveFiscal", pllaveFiscal);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	@Override
	public void reprocesaEndosoSeycos(BigDecimal pId_Cotizacion, BigDecimal pId_Solicitud_Canc){
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Reprocesa_Endoso_Seycos";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pId_Cotizacion", pId_Cotizacion);
			storedHelper.estableceParametro("pId_Solicitud_Canc", pId_Solicitud_Canc);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	
	private MensajeDTO generaBitEndosoCFP(BigDecimal idToPoliza, BigDecimal idToSolicitud, Integer idFormaPago, Double pctRecargoPagoFraccionado) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Gen_Cot_Endoso_CFP";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pIdSolicitud", idToSolicitud);
			storedHelper.estableceParametro("pIdFormaPago", idFormaPago);
			storedHelper.estableceParametro("pPctFormaPago", pctRecargoPagoFraccionado);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	@Override
	public boolean validaRehabilitacionCAP(BigDecimal idToPoliza){
		//Obtiene ultimo endoso
		ControlEndosoCot controlEndoso = obtenerControlEndosoCot(idToPoliza);	
		return controlEndoso.getSolicitud().getClaveMotivoEndoso().equals(SolicitudDTO.CVE_MOTIVO_ENDOSO_CANCELACION_AUTOMATICA_FALTA_PAGO);
	}
	
	
	@Override
	public void migrarEndosoCFP(String identificador){
		String spName = "SEYCOS.PKG_INT_MIDAS_E2.spAut_Migrar_Endoso_CFP";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdentificador", identificador);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	@Override
	public void generaReciboEndoso(BigDecimal pIdCotizacion, BigDecimal pIdSolicitud, BigDecimal pIdVersionPol, BigDecimal pIdVersionPolCanc,
			String pCveTEndoso, String pCveLTFolioCE){
		String spName = "SEYCOS.PKG_INT_MIDAS_E2.spAut_Genera_Recibo_Endoso";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pId_Cotizacion", pIdCotizacion);
			storedHelper.estableceParametro("pId_Solicitud", pIdSolicitud);
			storedHelper.estableceParametro("pid_version_pol", pIdVersionPol);
			storedHelper.estableceParametro("pid_version_polCanc", pIdVersionPolCanc);
			storedHelper.estableceParametro("pcve_t_endoso", pCveTEndoso);
			storedHelper.estableceParametro("pCveL_T_Folio_CE", pCveLTFolioCE);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}		
	}
	
	@Override
	public void igualarPrimaMovimientosEndoso(ControlEndosoCot controlEndosoCot){
		
		short tipoEndoso = controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue();
		if ((tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA ||
				tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS ||
				tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO ||
				tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_EXTENSION_VIGENCIA)
			&& controlEndosoCot.getPrimaTotalIgualar() != null) {		
			String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_IgualarMovimientosEndoso";
			String[] atributosDTO = { "mensaje" };
			String[] columnasCursor = { "valido"};
			try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(
						spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("pIdControlEndoso", controlEndosoCot.getId());
				
				storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);
	
				storedHelper.obtieneResultadoSencillo();
			} catch (Exception e) {
				throw new RuntimeException("Error en :" + spName, e);
			}	
		}
		
	}
	
	@Override
	public void ajustaMovimientosEndoso(ControlEndosoCot controlEndosoCot){
			
			String spName = "MIDAS.PKGAUT_GENERALES.stp_ajustamovPOLEND";
			try {
				StoredProcedureHelper storedHelper = new StoredProcedureHelper(
						spName, StoredProcedureHelper.DATASOURCE_MIDAS);
				storedHelper.estableceParametro("porigen", "E");
				storedHelper.estableceParametro("pidentificador", controlEndosoCot.getId());
				int result = storedHelper.ejecutaActualizar();

				if(result == 1){
					throw new Exception("Error al borrar cotizacion");
				}
			} catch (Exception e) {
				throw new RuntimeException("Error en :" + spName, e);
			}	
		
	}
	
	private int validaSiPolAnexaEsCancelable(BigDecimal idToPolizaObl){
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_validaPolOblCancelable";
		int result = 0;
		
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper (
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPolizaObl);
			
			result = storedHelper.ejecutaActualizar();

			return result;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Error en :" + spName, Level.INFO, null);
			return result;
		}
	
	}
	
	/**
	 * Guarda el No. de orden del brocker
	 * @param polizaId
	 * @param orderNo
	 */
	@Override
	public void guardaNoOrderBroker(BigDecimal polizaId, String orderNo){
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, polizaId);
		EndosoDTO endoso = getEndoso(poliza, new Long(0));
		
		if(endoso!=null) {
			endoso.setFolioCorredor(orderNo);
			entidadService.save(endoso);
		}
	}
	
	/**
	 * Obtiene el No. de orden del brocker
	 * @param polizaId
	 * @return
	 */
    @Override
	public String obtenerNoOrderBroker(BigDecimal polizaId){
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, polizaId);
		EndosoDTO endoso = getEndoso(poliza, new Long(0));
		
		return endoso.getFolioCorredor();
	}
    
	private boolean validarDiasRetroactividadCANCFP(BigDecimal idToPoliza){
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_validarDiasRetroCANCFP";
		boolean result = true;		
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper (
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			
			int resultInt = storedHelper.ejecutaActualizar();
			if(resultInt == 0){
				result = false;
			}
		} catch (Exception e) {
			e.printStackTrace();
			LogDeMidasInterfaz.log("Error en :" + spName, Level.INFO, null);
		}
		return result;
	}    
}
