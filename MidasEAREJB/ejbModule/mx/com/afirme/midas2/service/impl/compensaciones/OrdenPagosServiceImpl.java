package mx.com.afirme.midas2.service.impl.compensaciones;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;

import java.math.BigDecimal;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.CaRamo;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoEntidad;
import mx.com.afirme.midas2.domain.compensaciones.LiquidacionCompensaciones;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagos;
import mx.com.afirme.midas2.domain.compensaciones.OrdenPagosDetalle;
import mx.com.afirme.midas2.domain.compensaciones.OrdenesPagoDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosFacturacionDetalle;
import mx.com.afirme.midas2.dto.reportesAgente.DatosHonorariosAgentes;
import mx.com.afirme.midas2.service.compensaciones.LiquidacionCompensacionesService;
import mx.com.afirme.midas2.service.compensaciones.OrdenPagosDetalleService;
import mx.com.afirme.midas2.service.compensaciones.OrdenPagosService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.MailService;

@Stateless
public class OrdenPagosServiceImpl extends EntidadHistoricoDaoImpl implements OrdenPagosService{
	// property constants
		
	/** Log de OrdenPagosServiceImpl */
	private static final Logger LOG = LoggerFactory
			.getLogger(OrdenPagosServiceImpl.class);
	
	public static final String CLAVE_NOMBRE = "claveNombre";
	public static final String NOMBRE_CONTRATANTE = "nombreContratante";
	public static final String IMPORTE_PRIMA = "importePrima";
	public static final String IMPORTE_BS = "importeBs";
	public static final String IMPORTE_DERPOL = "importeDerpol";
	public static final String IMPORTE_CUM_META = "importeCumMeta";
	public static final String IMPORTE_UTILIDAD = "importeUtilidad";
	public static final String SUBTOTAL = "subtotal";
	public static final String IVA = "iva";
	public static final String IVA_RETENIDO = "ivaRetenido";
	public static final String ISR = "isr";
	public static final String ESTATUS_FACTURA = "estatusFactura";
	public static final String ESTATUS_ORDENPAGO = "estatusOrdenpago";
	public static final String IMPORTE_TOTAL = "importeTotal";

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;
	
	@EJB
	private LiquidacionCompensacionesService liquidacionCompensacionesService;
	
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	@EJB
	private OrdenPagosDetalleService ordenPagosDetalleService;
	
	@EJB
	MailService mailService;
	public void save(OrdenPagos entity) {
		LOG.trace(">>save()--OrdenPagosServiceImpl");
		try {
			entityManager.persist(entity);
			LOG.info("Se Guardo OrdenPagos");
		} catch (RuntimeException re) {
			LOG.error("Error al guardar OrdenPagos ",re);
			throw re;
		}

	}

	public void delete(OrdenPagos entity) {
		LOG.trace(">>::	[INF]	::	Eliminando OrdenPagos  	::		OrdenPagosServiceImpl	::	delete	::	INICIO	::	");
		try {
			entity = entityManager.getReference(OrdenPagos.class,
					entity.getId());
			entityManager.remove(entity);
			LOG.info("	::	[INF]	::	Se Elimino OrdenPagos 	::		OrdenPagosServiceImpl	::	delete	::	FIN	::	");
		} catch (RuntimeException re) {
			LOG.error(
					"	::	[ERR]	::	Error al eliminar OrdenPagos 	::		OrdenPagosServiceImpl	::	delete	::	ERROR	::	",
					re);
			throw re;
		}
	}

	public OrdenPagos update(OrdenPagos entity) {
		LOG.trace(">>::	[INF]	::	Actualizando OrdenPagos 	::		OrdenPagosServiceImpl	::	update	::	INICIO	::	");
		try {
			OrdenPagos result = entityManager.merge(entity);
			LOG.info("	::	[INF]	::	Se Actualizo OrdenPagos 	::		OrdenPagosServiceImpl	::	update	::	FIN	::	");
			return result;
		} catch (RuntimeException re) {
			LOG.error(
					"	::	[ERR]	::	Error al actualizar OrdenPagos 	::		OrdenPagosServiceImpl	::	update	::	ERROR	::	",
					re);
			throw re;
		}
	}

	public OrdenPagos findById(Long id) {
		LOG.trace("	>>::	[INF]	::	Buscando por Id 	::		OrdenPagosServiceImpl	::	findById	::	INICIO	::	");
		try {
			OrdenPagos instance = entityManager.find(OrdenPagos.class, id);
			LOG.info("	::	[INF]	::	Se busco por Id 	::		OrdenPagosServiceImpl	::	findById	::	FIN	::	");
			return instance;
		} catch (RuntimeException re) {
			LOG.error(
					"	::	[ERR]	::	Error al buscar por Id 	::		OrdenPagosServiceImpl	::	findById	::	ERROR	::	",
					re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<OrdenPagos> findByProperty(String propertyName,
			final Object value) {
		LOG.info(">>::	[INF]	::	Buscando por Propiedad 	::		OrdenPagosServiceImpl	::	findByProperty	::	INICIO	::	");
		try {
			final String queryString = "select model from OrdenPagos model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();

		} catch (RuntimeException re) {
			LOG.error(
					"	::	[ERR]	::	Error al buscar por Propiedad 	::		OrdenPagosServiceImpl	::	findByProperty	::	ERROR	::	",
					re);
			throw re;
		}
	}

	public List<OrdenPagos> findByClaveNombre(Object claveNombre) {
		return findByProperty(CLAVE_NOMBRE, claveNombre);
	}

	public List<OrdenPagos> findByNombreContratante(Object nombreContratante) {
		return findByProperty(NOMBRE_CONTRATANTE, nombreContratante);
	}

	public List<OrdenPagos> findByImportePrima(Object importePrima) {
		return findByProperty(IMPORTE_PRIMA, importePrima);
	}

	public List<OrdenPagos> findByImporteBs(Object importeBs) {
		return findByProperty(IMPORTE_BS, importeBs);
	}

	public List<OrdenPagos> findByImporteDerpol(Object importeDerpol) {
		return findByProperty(IMPORTE_DERPOL, importeDerpol);
	}

	public List<OrdenPagos> findByImporteCumMeta(Object importeCumMeta) {
		return findByProperty(IMPORTE_CUM_META, importeCumMeta);
	}

	public List<OrdenPagos> findByImporteUtilidad(Object importeUtilidad) {
		return findByProperty(IMPORTE_UTILIDAD, importeUtilidad);
	}

	public List<OrdenPagos> findBySubtotal(Object subtotal) {
		return findByProperty(SUBTOTAL, subtotal);
	}

	public List<OrdenPagos> findByIva(Object iva) {
		return findByProperty(IVA, iva);
	}

	public List<OrdenPagos> findByIvaRetenido(Object ivaRetenido) {
		return findByProperty(IVA_RETENIDO, ivaRetenido);
	}

	public List<OrdenPagos> findByIsr(Object isr) {
		return findByProperty(ISR, isr);
	}

	public List<OrdenPagos> findByEstatusFactura(Object estatusFactura) {
		return findByProperty(ESTATUS_FACTURA, estatusFactura);
	}

	public List<OrdenPagos> findByEstatusOrdenpago(Object estatusOrdenpago) {
		return findByProperty(ESTATUS_ORDENPAGO, estatusOrdenpago);
	}

	public List<OrdenPagos> findByImporteTotal(Object importeTotal) {
		return findByProperty(IMPORTE_TOTAL, importeTotal);
	}

	@SuppressWarnings("unchecked")
	public List<OrdenPagos> findAll() {

		LOG.info(">>  :: [INF]	::	buscar todo	::		OrdenPagosServiceImpl	::	findByProperty	::	INICIO	::	");
		try {
			final String queryString = "select model from OrdenPagos model";
			Query query = entityManager.createQuery(queryString);
//			List<OrdenPagos> list = query.getResultList();
//			LOG.info("Salida Lista>>>"+list.size());
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error(
					"	::	[ERR]	::	Error al buscar todo 	::		OrdenPagosServiceImpl	::	findAll	::	ERROR	::	",
					re);
			throw re;
		}
	}
	
	public  void guardarOrdenesPago(List<OrdenPagos> ordenPagosList){
		LOG.info(">> guardarOrdenesPago()");
		try {
			for(OrdenPagos orden:ordenPagosList){
				if(orden!=null){
					if(orden.getIdentificador_ben_id()!=null){
						List<OrdenPagos> list =this.findByProperty("identificador_ben_id",orden.getIdentificador_ben_id());
						for(OrdenPagos object:list){
							ordenPagosDetalleService.actualizarEstatusOrdenPagosDetalleAGenerado(object.getFolio());
						}						
					}
					
				}
				
			}
		} 
		 catch (Exception e) {
			LOG.error("-- guardarOrdenesPago()", e);
		}
		
		
	}
	private void cambiarEstatusOrdenPagosDetalle(List<OrdenPagos> ordenPagosList){
		for(OrdenPagos orden:ordenPagosList){
			if(orden!=null){
				if(orden.getFolio()!=null){
					List<OrdenPagos> ordenesFolio=this.findByProperty("folio", orden.getFolio());
					for(OrdenPagos objectFolio:ordenesFolio){
						ordenPagosDetalleService.actualizarEstatusOrdenPagosDetalle(objectFolio.getFolio());
					}
					
				}
				
			}
			
		}
	}	
	public List<OrdenPagos> findOrdenPagosByFilters(OrdenesPagoDTO filter){
		return findOrdenPagosByFilters(filter,false);
	}
	@SuppressWarnings("unchecked")
	private  List<OrdenPagos> findOrdenPagosByFilters(OrdenesPagoDTO filter,boolean soloOrdenGenerados){
	      LOG.info("findOrdenPagosByFilters  OrdenPagos instances soloOrdenGenerados=>"+soloOrdenGenerados);
	      List<OrdenPagos>  lista=new ArrayList<OrdenPagos>();  
	      Map<Integer,Object> params=new HashMap<Integer, Object>();
	      final StringBuilder queryString=new StringBuilder("");
	      final StringBuilder queryStringGoup=new StringBuilder("");
	      try{
	        queryString.append(" SELECT 0 as  ID,  " );
	        queryString.append(" ORD.IDENTIFICADOR_BEN_ID, " +
	        		"ORD.FOLIO," +
	        		"RM.NOMBRE," +
	        		"ORD.IDTOPOLIZA," +
	        		"ORD.TIPOENTIDAD_ID," +
	        		"ORD.COMPENSACION_ID," +
	        		"ORD.ENTIDADPERSONA_ID," +
	        		" 0 AS TIPOCOMPENSACION_ID," +
	        		"ORD.NOMBRE_BENEFICIARIO," +
	        		"ORD.CLAVE_NOMBRE," +
	        		"ORD.NOMBRE_CONTRATANTE, " +
	        		"SUM(ORD.IMPORTE_PRIMA) AS IMPORTE_PRIMA," +
	        		"SUM(ORD.IMPORTE_BS) AS IMPORTE_BS," +
	        		"SUM(ORD.IMPORTE_DERPOL) AS IMPORTE_DERPOL," +
	        		"SUM(ORD.IMPORTE_CUM_META) AS IMPORTE_CUM_META," +
	        		"SUM(ORD.IMPORTE_UTILIDAD) AS IMPORTE_UTILIDAD," +
	        		"SUM(ORD.SUBTOTAL) AS SUBTOTAL," +
	        		"SUM(ORD.IVA) AS IVA," +
	        		"SUM(ORD.IVA_RETENIDO) AS IVA_RETENIDO," +
	        		"SUM(ORD.ISR) AS ISR, " );
	        queryString.append(" ORD.ESTATUS_FACTURA,ORD.ESTATUS_ORDENPAGO,SUM(ORD.IMPORTE_TOTAL) IMPORTE_TOTAL,ORD.FECHA_CORTE,ORD.FECHA_CREACION   " );
	        queryString.append(" FROM MIDAS.CA_ORDENPAGOS ORD  ");
	        queryString.append(" INNER JOIN MIDAS.CA_ORDENPAGOS_DETALLE ORDD ON(ORDD.FOLIO=ORD.FOLIO) ");
	        if(soloOrdenGenerados==true){
	        	queryString.append(" and ORDD.ESTATUS_ORDPAG_GEN = 3 ");
	        }else{
	        	queryString.append(" and ORDD.ESTATUS_ORDPAG_GEN in (1,2,3) ");
	        }
	        queryString.append(" INNER JOIN MIDAS.ca_compensacion CO  ON (ORD.COMPENSACION_ID=CO.ID ) ");
	        queryString.append(" INNER JOIN MIDAS.ca_ramo RM ON (CO.ramo_id = rm.id) ");
	        queryString.append(" INNER JOIN MIDAS.ca_entidadpersona EP ON (EP.compensacion_id = CO.id AND ORD.ENTIDADPERSONA_ID=EP.ID) ");
	        queryString.append(" INNER JOIN MIDAS.ca_tipoentidad TE ON (EP.tipoentidad_id = TE.id AND TE.ID=ORD.TIPOENTIDAD_ID) ");
	        queryString.append(" INNER JOIN MIDAS.CA_TIPOCOMPENSACION TIPOC ON( TIPOC.ID = ORD.TIPOCOMPENSACION_ID AND TIPOC.ID = ORDD.TIPOCOMPENSACION_ID)        ");
	        //queryString.append(" INNER JOIN seycos.pol_recibo POLREC ON( POLREC.ID_RECIBO=ORDD.ID_RECIBO AND  POLREC.SIT_RECIBO='PAG') ");
	        queryString.append(" INNER JOIN seycos.pol_recibo POLREC ON( POLREC.ID_RECIBO=ORDD.ID_RECIBO) ");
	        if(isNotNull(filter)){
	          int index=1;
				if (filter.getId() != null) {
					addCondition(queryString, " ord.id = ?" + index);
					params.put(index, filter.getId());
					index++;
				}				
				if (filter.getRamo() != null && !filter.getRamo().isEmpty()) {
					addCondition(queryString, " rm.identificador IN ?" + index);
					params.put(index, filter.getRamo());
					index++;
				}				
				if (filter.getEstatus_op_gen() != null) {
					addCondition(queryString, " ordd.estatus_ordpag_gen = ?" + index);
					params.put(index, filter.getEstatus_op_gen());
					index++;
				}				
				if (filter.getContratante() != null 
						&& !filter.getContratante().isEmpty()&& !filter.getContratante().equals("0")) {
                    addCondition(queryString, " ord.nombre_contratante = ?" + index);
                    params.put(index, filter.getContratante());
                    index++;
				}
				if (filter.getAgente() != null && !filter.getAgente().isEmpty()) {
					if (new Long(filter.getAgente()) > 0) {
						addCondition(queryString, " ep.ID = ?" + index);
						params.put(index, filter.getAgente());
						index++;
					} 
					else if (new Long(filter.getAgente()) == 0) {
						addCondition(queryString, " TE.valor = ?" + index);
						params.put(index, CaTipoEntidad.TIPO_AGENTE);
						index++;
					}
				}				
				if (filter.getPromotor() != null
						&& !filter.getPromotor().isEmpty()) {
					if (new Long(filter.getPromotor()) > 0) {
						addCondition(queryString, " ep.ID = ?" + index);
						params.put(index, filter.getPromotor());
						index++;
					}					
					else if (new Long(filter.getPromotor()) == 0) {
						addCondition(queryString, " TE.valor = ?" + index);
						params.put(index, CaTipoEntidad.TIPO_PROMOTOR);
						index++;
					}
				}
				if (filter.getProveedor() != null
						&& !filter.getProveedor().isEmpty()) {
					if (new Long(filter.getProveedor()) > 0) {
						addCondition(queryString, " ep.ID = ?" + index);
						params.put(index, filter.getProveedor());
						index++;
					} 
					else if (new Long(filter.getProveedor()) == 0) {
						addCondition(queryString, " TE.valor = ?" + index);
						params.put(index, CaTipoEntidad.TIPO_PROVEEDOR);
						index++;
					}
				}				
				if (filter.getNegocio() != null	&& !filter.getNegocio().isEmpty()) {
					if (new Long(filter.getNegocio()) > 0) {
						addCondition(queryString, " CO.NEGOCIO_ID = ?" + index);
						params.put(index, filter.getNegocio());
						index++;
					} 
				}
				if (filter.getTipobeneficiario() != null) {
					addCondition(queryString, " te.valor IN ?" + index);
					params.put(index, filter.getTipobeneficiario());
					index++;
				}							
				if (filter.getConcepto() != null && !filter.getConcepto().isEmpty()) {
			        addCondition(queryString, " TIPOC.VALOR IN ( ? " + index + " ) ");
			        params.put(index, filter.getConcepto());
			        index++;          
			    }				
				if(filter.getFechaInicio() !=null && filter.getFechaFin()!=null){
                    addCondition(queryString, " ( ORD.fecha_creacion >= ?"+index);
                    params.put(index, filter.getFechaInicio());
                    index++;             
                    addCondition(queryString, " ORD.fecha_creacion <= ?"+index +")");
                    params.put(index, filter.getFechaFin());
                    index++;
				}				
		        if(params.isEmpty()){
	               int lengthWhere=" where".length();
	               queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
	             }
	          queryStringGoup.append(" GROUP BY ORD.IDENTIFICADOR_BEN_ID,ORD.FOLIO,RM.NOMBRE,ORD.IDTOPOLIZA,ORD.TIPOENTIDAD_ID,ORD.COMPENSACION_ID,ORD.ENTIDADPERSONA_ID,ORD.NOMBRE_BENEFICIARIO,ORD.CLAVE_NOMBRE,ORD.NOMBRE_CONTRATANTE,ORD.ESTATUS_FACTURA,ORD.ESTATUS_ORDENPAGO,ORD. FECHA_CORTE,ORD.FECHA_CREACION  ");
	          queryStringGoup.append(" ORDER BY ORD.IDENTIFICADOR_BEN_ID DESC ");
	          String finalQuery=getQueryString(queryString) +queryStringGoup.toString();  
	          LOG.info("finalQuery==>"+finalQuery);
				Query query=entityManager.createNativeQuery(finalQuery);
	          LOG.info(" LISTANDO POR FILTROS"+ finalQuery);
	          if(!params.isEmpty()){
	            for(Integer key:params.keySet()){
	              query.setParameter(key,params.get(key));
	            }
	          }
	          LOG.info("query.toString()>"+ query.toString());
	          
	          query.setMaxResults(100);        
				List<Object[]> resultList=query.getResultList();
	            for (Object[] result : resultList){ 
	                OrdenPagos ordenTmp= new OrdenPagos();
	                ordenTmp.setId(new Long(((BigDecimal)result[0]).toString()));
	                ordenTmp.setFolio(new Long(((BigDecimal)result[1]).toString()));
	                ordenTmp.setCaRamo(new CaRamo(result[2].toString(),null,null,null,null,null,null));
	              	ordenTmp.setIdToPoliza(new Long(((BigDecimal)result[3]).toString()));
	              	ordenTmp.setCaTipoentidad(new CaTipoEntidad(new Long(((BigDecimal)result[4]).toString()),null,null,null,null,null,null));
	              	ordenTmp.setCaCompensacion(new CaCompensacion(new Long(((BigDecimal)result[5]).toString())));
	              	ordenTmp.setCaEntidadPersona(new CaEntidadPersona(new Long(((BigDecimal)result[6]).toString())));
	                ordenTmp.setClaveNombre((result[9]).toString());
	                ordenTmp.setNombreContratante((result[10]).toString());
	                ordenTmp.setImportePrima(new Double(((BigDecimal)result[11]).toString()));
	                ordenTmp.setImporteBs(new Double(((BigDecimal)result[12]).toString()));
	                ordenTmp.setImporteDerpol(new Double(((BigDecimal)result[13]).toString()));
	                ordenTmp.setImporteCumMeta(new Double(((BigDecimal)result[14]).toString()));
	                ordenTmp.setImporteUtilidad(new Double(((BigDecimal)result[15]).toString()));
	                ordenTmp.setImporteTotal(new Double(((BigDecimal)result[22]).toString()));
	                ordenTmp.setSubtotal(new Double(((BigDecimal)result[16]).toString()));
	                ordenTmp.setIva(new Double(((BigDecimal)result[17]).toString()));
	                ordenTmp.setIvaRetenido(new Double(((BigDecimal)result[18]).toString()));
	                ordenTmp.setIsr(new Double(((BigDecimal)result[20]).toString()));
	                ordenTmp.setImporteTotal(new Double(((BigDecimal)result[23]).toString()));
	                  lista.add(ordenTmp);
	                }
	          LOG.info(" LISTA"+ lista.size());
	        }	      
	      } catch (RuntimeException re) {
	        LOG.error("findOrdenPagosByFilters all failed",re);
	        throw re;
	      }
	      LOG.info("lista>"+ lista.size());
	      /*actualiza las ordenes de pago a estatus en proceso*/
	       cambiarEstatusOrdenPagosDetalle(lista);
	      return lista;     
	    }
	
	public String enviarReportePorCorreo(Long idLiquidacion){
		ByteArrayAttachment attachment = null;
//		ByteArrayAttachment attachment2 = null;
		List<String> destinatarios = new ArrayList<String>();
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		String nombre = ""; 	 	 	
		String email = "";
		try{
			LiquidacionCompensaciones ordenLiquidacion = liquidacionCompensacionesService.findById(idLiquidacion); 
			List<OrdenPagosDetalle> listaOrdenPagosDetalle = liquidacionCompensacionesService.findByLiquidacionId(idLiquidacion);
			nombre = ordenLiquidacion.getNombreBeneficiario();
			email  = ordenLiquidacion.getCorreo();
			
			
			attachment = new ByteArrayAttachment("Recibo de Honorarios.pdf",TipoArchivo.PDF,
					generarPlantillaReporte.imprimirReciboHonorariosCompensaciones(llenarDatosHonorariosCompensaciones(ordenLiquidacion)));
			
//			attachment2 = new ByteArrayAttachment("Reporte Agente Facturacion Detalle.pdf",TipoArchivo.PDF,
//					generarPlantillaReporte.imprimirReporteAgenteDetalle(listaOrdenPagosDetalle));
		    
			if(attachment != null){
				destinatarios.add(email);
				adjuntos.add(attachment);
//				adjuntos.add(attachment2);
				/////*correo a destinatario*/////
						mailService.sendMail(destinatarios,
						"Recibo Honorario Compensaciones Adicionales",
						"Se ha generado una orden de pago. Enviamos Recibo de Honorarios para elaboración de factura", 
						 adjuntos,
						"Recibo de Honorarios",
						"Estimado(s): " + nombre);
						liquidacionCompensacionesService.enviarLiquidacionFactura(idLiquidacion);
				}   
		}catch (RuntimeException re) {
	    	LOG.error("--error enviarReportePorCorreo()", re);
	        throw re; 
	    } 
		return null;
	}
	
	public  List<OrdenPagos> obtenerOrdenPagosProcesoPorId(Long ordenPagoId){ 
		LOG.info(">>obtenerOrdenPagosProcesoPorId  OrdenPagos");
	    List<OrdenPagos> lista = new ArrayList<OrdenPagos>();
	    try {
	    	lista=obtenerOrdenPagosPorEstatus(ordenPagoId, OrdenPagos.ESTATUS_ORDPAG_GEN_INCLUIDO);
	    } catch (RuntimeException re) {
	    	LOG.error("-- obtenerOrdenPagosProcesoPorId()", re);
	        throw re; 
	    }    
	    return lista;
	}
			
	public  List<OrdenPagos> obtenerOrdenPagosGeneradoPorId(Long ordenPagoId){ 
		LOG.info(">>Entrando obtenerOrdenPagosGeneradoPorId  OrdenPagos");
	    List<OrdenPagos > lista = new ArrayList<OrdenPagos>();
	    try {
	    	lista=obtenerOrdenPagosGenerado(OrdenPagos.ESTATUS_ORDPAG_GEN_GENERADO,false, null);
	    } catch (RuntimeException re) {
	    	LOG.error("-->obtenerOrdenPagosGeneradoPorId()", re);
	        throw re; 
		}
	    return lista;
		}
	@SuppressWarnings("unchecked")
	private  List<OrdenPagos> obtenerOrdenPagosPorEstatus(Long ordenPagoId, Long estatusOrdenPago){  
		LOG.debug("obtenerOrdenPagosPorEstatus() OrdenPagoId => {} estatusOrdenGenerado => {} " ,ordenPagoId,estatusOrdenPago);
	    List<OrdenPagos > lista = new ArrayList<OrdenPagos>();
	    try {
	      final StringBuilder queryString = new StringBuilder(200);
	      queryString.append(" SELECT distinct ORD.* FROM MIDAS.CA_ORDENPAGOS ORD ");
		  queryString.append(" INNER JOIN MIDAS.CA_ORDENPAGOS_DETALLE ORDD ON(ORDD.FOLIO=ORD.FOLIO) ");
		  queryString.append(" INNER JOIN MIDAS.ca_compensacion CO  ON (ORD.COMPENSACION_ID=CO.ID) ");
		  queryString.append(" INNER JOIN MIDAS.ca_ramo RM ON (CO.ramo_id = rm.id) ");
		  queryString.append(" INNER JOIN MIDAS.ca_entidadpersona EP ON (EP.compensacion_id = CO.id AND ORD.ENTIDADPERSONA_ID=EP.ID) ");
		  queryString.append(" INNER JOIN MIDAS.ca_tipoentidad TE ON (EP.tipoentidad_id = TE.id AND TE.ID=ORD.TIPOENTIDAD_ID) ");
		  queryString.append(" and ordd.estatus_ordpag_gen = ?1 ");
		  queryString.append(" order by ORD.folio desc ");
	      Query query = entityManager.createNativeQuery(queryString.toString() ,OrdenPagosDetalle.class);
	      //query.setParameter(1, ordenPagoId);
	      query.setParameter(1, estatusOrdenPago);	      
	      
	      lista=query.getResultList();
	      LOG.info("size="+lista.size());
	      LOG.info("query="+query.toString());                
	    } catch (RuntimeException re) {
	    	LOG.error("--error obtenerOrdenPagosPorEstatus()", re);
	        throw re; 
		}

	    return lista;
	}
	
	@SuppressWarnings("unchecked")
	private  List<OrdenPagos> obtenerOrdenPagosGenerado(Long estatusOrdenPago, boolean esGenerado, Long idParamtro){  
		LOG.debug("obtenerOrdenPagosGenerado() estatusOrdenPago => {} idParamtro => {} " ,estatusOrdenPago,idParamtro);
	    List<OrdenPagos > lista = new ArrayList<OrdenPagos>();
	    try {
	      final StringBuilder queryString = new StringBuilder(200);
	      queryString.append(" SELECT ORDD.LIQUIDACION_ID AS ID,ORDD.FOLIO,TE.valor,ORD.IDTOPOLIZA, " +
	      		"ORD.TIPOENTIDAD_ID,ORD.COMPENSACION_ID,ORD.ENTIDADPERSONA_ID,0 AS TIPOCOMPENSACION_ID," +
	      		"ORD.NOMBRE_BENEFICIARIO,ORD.CLAVE_NOMBRE,ORD.NOMBRE_CONTRATANTE," +
	      		"SUM(ORD.IMPORTE_PRIMA)AS IMPORTE_PRIMA,SUM(ORD.IMPORTE_BS) AS IMPORTE_BS," +
	      		"SUM(ORD.IMPORTE_DERPOL)AS IMPORTE_DERPOL,SUM(ORD.IMPORTE_CUM_META) AS IMPORTE_CUM_META," +
	      		"SUM(ORD.IMPORTE_UTILIDAD) AS IMPORTE_UTILIDAD," +
	      		"SUM(ORD.SUBTOTAL)AS SUBTOTAL,SUM(ORD.IVA)AS IVA," +
	      		"SUM(ORD.IVA_RETENIDO)AS IVA_RETENIDO,SUM(ORD.ISR)AS ISR, ORDD.ESTATUS_FACTURA," +
	      		"ORDD.ESTATUS_ORDENPAGO,ORDD.ESTATUS_ORDPAG_GEN,SUM(ORD.IMPORTE_TOTAL) IMPORTE_TOTAL," +
	      		"ORDD.FECHA_CORTE,ORDD.FECHA_CREACION ");
		  queryString.append(" FROM MIDAS.CA_ORDENPAGOS ORD ");
		  queryString.append(" INNER JOIN MIDAS.CA_ORDENPAGOS_DETALLE ORDD ON(ORDD.FOLIO=ORD.FOLIO) ");
		  queryString.append(" INNER JOIN MIDAS.ca_compensacion CO  ON (ORD.COMPENSACION_ID=CO.ID) ");
		  queryString.append(" INNER JOIN MIDAS.ca_ramo RM ON (CO.ramo_id = rm.id) ");
		  queryString.append(" INNER JOIN MIDAS.ca_entidadpersona EP ON (EP.compensacion_id = CO.id AND ORD.ENTIDADPERSONA_ID=EP.ID) ");
		  queryString.append(" INNER JOIN MIDAS.ca_tipoentidad TE ON (EP.tipoentidad_id = TE.id AND TE.ID=ORD.TIPOENTIDAD_ID) ");
		  queryString.append(" and ordd.estatus_ordpag_gen = ?1 ");
		  if(esGenerado==true){
	        	queryString.append(" and ORDD.folio = ");
	        	queryString.append(idParamtro);
	        }
		  queryString.append(" GROUP BY ORDD.LIQUIDACION_ID, ORDD.FOLIO,TE.valor,RM.NOMBRE,ORD.IDTOPOLIZA, ORD.TIPOENTIDAD_ID, ORD.COMPENSACION_ID, " +
		  		"ORD.ENTIDADPERSONA_ID, ORD.NOMBRE_BENEFICIARIO, ORD.CLAVE_NOMBRE, ORD.NOMBRE_CONTRATANTE, ORDD.ESTATUS_FACTURA, " +
		  		"ORDD.ESTATUS_ORDENPAGO, ORDD.ESTATUS_ORDPAG_GEN, ORDD.FECHA_CORTE, ORDD.FECHA_CREACION");
		  queryString.append(" order by ORDD.folio desc ");
		  String finalQuery=getQueryString(queryString) ;
		  Query query=entityManager.createNativeQuery(finalQuery);
	      //Query query = entityManager.createNativeQuery(queryString.toString() ,OrdenPagosDetalle.class);
	      query.setParameter(1, estatusOrdenPago);     
          LOG.info("finalQuery==>"+finalQuery);	      
	      LOG.info("size="+lista.size());
	      LOG.info("query="+query.toString());
	      query.setMaxResults(100);       
		  List<Object[]> resultList=query.getResultList();
          for (Object[] result : resultList){ 
              OrdenPagos ordenTmp= new OrdenPagos();
              ordenTmp.setId(new Long(((BigDecimal)result[0]).toString()));
              ordenTmp.setFolio(new Long(((BigDecimal)result[1]).toString()));
              ordenTmp.setCaRamo(new CaRamo(result[2].toString(),null,null,null,null,null,null));
              ordenTmp.setIdToPoliza(new Long(((BigDecimal)result[3]).toString()));
              ordenTmp.setCaTipoentidad(new CaTipoEntidad(new Long(((BigDecimal)result[4]).toString()),null,null,null,null,null,null));
              ordenTmp.setCaCompensacion(new CaCompensacion(new Long(((BigDecimal)result[5]).toString())));
              ordenTmp.setCaEntidadPersona(new CaEntidadPersona(new Long(((BigDecimal)result[6]).toString())));
              ordenTmp.setClaveNombre((result[9]).toString());
              ordenTmp.setNombreContratante((result[10]).toString());
              ordenTmp.setImportePrima(new Double(((BigDecimal)result[11]).toString()));
              ordenTmp.setImporteBs(new Double(((BigDecimal)result[12]).toString()));
              ordenTmp.setImporteDerpol(new Double(((BigDecimal)result[13]).toString()));
              ordenTmp.setImporteCumMeta(new Double(((BigDecimal)result[14]).toString()));
              ordenTmp.setImporteUtilidad(new Double(((BigDecimal)result[15]).toString()));
              ordenTmp.setImporteTotal(new Double(((BigDecimal)result[23]).toString()));
              ordenTmp.setSubtotal(new Double(((BigDecimal)result[16]).toString()));
              ordenTmp.setIva(new Double(((BigDecimal)result[17]).toString()));
              ordenTmp.setIvaRetenido(new Double(((BigDecimal)result[18]).toString()));
              ordenTmp.setIsr(new Double(((BigDecimal)result[19]).toString()));
              ordenTmp.setFechaCorte( Timestamp.valueOf(result[24].toString()));
                lista.add(ordenTmp);
              }
	    } catch (RuntimeException re) {
	    	LOG.error("--error obtenerOrdenPagosGenerado()", re);
	        throw re; 
		}

	    return lista;
	}
	
	
	
	///LISTA ADJUNTADO REPORTE AGENTES PDF
	public DatosFacturacionDetalle llenarDatosReporteAgenteDetalle(OrdenPagosDetalle ordenPagosDetalle) {
		
		List<DatosFacturacionDetalle> lista = new ArrayList<DatosFacturacionDetalle>();
		DatosFacturacionDetalle datosFacturacionDetalle = new DatosFacturacionDetalle();
		try {	
			    datosFacturacionDetalle.setNumero_poliza(ordenPagosDetalle.getPoliza());
				datosFacturacionDetalle.setContratante(ordenPagosDetalle.getNombreContratante());
			    datosFacturacionDetalle.setEndoso(ordenPagosDetalle.getNumeroEndoso());
			    datosFacturacionDetalle.setPrima_base(ordenPagosDetalle.getPrimaBase());
			    datosFacturacionDetalle.setImporte_prima_base(ordenPagosDetalle.getImportePrimaBase());
			    datosFacturacionDetalle.setPorcentaje_comp_prima(ordenPagosDetalle.getPorcentajeCompPrima());
			    datosFacturacionDetalle.setImporte_prima(ordenPagosDetalle.getImportePrima());
			    datosFacturacionDetalle.setComp_derecho_poliza(ordenPagosDetalle.getImporteDerpol());
			    datosFacturacionDetalle.setComp_baja_siniestralidad(ordenPagosDetalle.getImporteBs());
			    datosFacturacionDetalle.setTotal_compensacion(ordenPagosDetalle.getImporteTotal());
			    lista.add(datosFacturacionDetalle);
		}catch (RuntimeException re) {
	    	LOG.error("--error al llenarDatosHonorariosCompensaciones()", re);
	        throw re; 
		 }
		return datosFacturacionDetalle;
	}

/////LLENAR LISTA PARA PDF//
	
public DatosHonorariosAgentes llenarDatosHonorariosCompensaciones(LiquidacionCompensaciones ordenLiquidacion) {		
		List<DatosHonorariosAgentes> lista = new ArrayList<DatosHonorariosAgentes>();
		DatosHonorariosAgentes datosHonorariosAgentes = new DatosHonorariosAgentes();
		try {			
				if(ordenLiquidacion.getCveTipoEntidad().equals("AGENTE")){
					datosHonorariosAgentes.setNombreGerencia("Agente:");
				}else if(ordenLiquidacion.getCveTipoEntidad().equals("PROMOTOR")){
					datosHonorariosAgentes.setNombreGerencia("Promotor:");
				}else if(ordenLiquidacion.getCveTipoEntidad().equals("PROVEEDOR")){
					datosHonorariosAgentes.setNombreGerencia("Proveedor:");
				}
					datosHonorariosAgentes.setImporteIva(new BigDecimal(ordenLiquidacion.getIva()));
					datosHonorariosAgentes.setImporteSubtotal(new BigDecimal(ordenLiquidacion.getSubtotal()));
					datosHonorariosAgentes.setImporteIsrRetenido(new BigDecimal(ordenLiquidacion.getIsr()));
					datosHonorariosAgentes.setImporteIvaRetenido(new BigDecimal(ordenLiquidacion.getIvaRetenido()));
					datosHonorariosAgentes.setImporteExento(new BigDecimal(ordenLiquidacion.getImporteBs()));
					datosHonorariosAgentes.setImporteGravable(new BigDecimal(ordenLiquidacion.getSubtotal()));
					datosHonorariosAgentes.setOrigen(ordenLiquidacion.getId().toString());
					datosHonorariosAgentes.setIdAgente(ordenLiquidacion.getIdentificadorBenId());
				Double total = new Double(ordenLiquidacion.getImporteTotal());
				datosHonorariosAgentes.setImporteTotalCLetra(Utilerias.convertNumberToLetter(total));
				LOG.info("total>>"+total);
				LOG.info("total_Letra>>"+Utilerias.convertNumberToLetter(total));		
				datosHonorariosAgentes.setImporteTotal(new BigDecimal(total));
				datosHonorariosAgentes.setNombre(ordenLiquidacion.getNombreBeneficiario());				
				lista.add(datosHonorariosAgentes);				
		}catch (RuntimeException re) {
	    	LOG.error("--error al llenarDatosHonorariosCompensaciones()", re);
	        throw re; 
		 }
		return datosHonorariosAgentes;
	}
}