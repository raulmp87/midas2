package mx.com.afirme.midas2.service.impl.seguridad.filler.consultasiniestros;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

public class PaginaPermisoConsultaSiniestros {
    
    private List<Permiso> listaPermiso = new ArrayList<Permiso>();
    private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();

    public PaginaPermisoConsultaSiniestros(List<Permiso> listaPermiso) {
	this.listaPermiso = listaPermiso;
    }

    private Pagina nuevaPagina(String nombrePaginaJSP, String nombreAccionDo) {
	return new Pagina(new Integer("1"), nombrePaginaJSP.trim(),
		nombreAccionDo.trim(), "Descripcion pagina");
    }
    
    public List<PaginaPermiso> obtienePaginaPermisos() {
	PaginaPermiso pp;
	//***** ACERCA DE **********//
	pp = new PaginaPermiso(); 
	pp.setPagina(nuevaPagina("acerca.jsp","/MidasWeb/sistema/mensajePendiente/mostrarAcerca.do"));  
	listaPaginaPermiso.add(pp); 

		//Listar reporte
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/listarFiltrado.do")); 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/listar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp", "/MidasWeb/siniestro/cabina/reporteSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/listarTodos.do"));
		listaPaginaPermiso.add(pp);
		
		//Caratula
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("caratulaSiniestro.jsp", "/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestroPDF.do")); 
		listaPaginaPermiso.add(pp);
		
		//Vista previa del reporte
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosReporteSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosPolizaRS.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosPoliza.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosEstadoSiniestroRS.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosEstadoSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarAfectarCoberturaRiesgo.jsp","/MidasWeb/siniestro/poliza/desplegarAfectarCoberturaRiesgo.do")); 
		listaPaginaPermiso.add(pp);
		
		//Listar reserva
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarHistorialReserva.jsp","/MidasWeb/siniestro/finanzas/listarReserva.do")); 
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarHistorialReservaDetalle.jsp","/MidasWeb/siniestro/finanzas/listarReservaDetalle.do")); 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSPFechas.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);		
		
		return this.listaPaginaPermiso;
    }

}
