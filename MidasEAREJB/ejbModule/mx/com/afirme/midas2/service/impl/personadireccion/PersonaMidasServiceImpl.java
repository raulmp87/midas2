package mx.com.afirme.midas2.service.impl.personadireccion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMoralMidas;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.personadireccion.PersonaMidasService;

@Stateless
public class PersonaMidasServiceImpl implements PersonaMidasService<PersonaMidas> {

	@EJB
	EntidadService entidadService;
	
	@Override
	public void guardar(PersonaMidas persona) {
		entidadService.save(persona);		
	}

	@Override
	public PersonaMidas obtenerPersona(Long id) {
		return entidadService.findById(PersonaMidas.class, id);		
	}

	@Override
	public List<PersonaMoralMidas> obtenerPersonas(String sector, String rama,
			String subRama){
		Map<String, Object> parameters = new HashMap<String, Object>();		
		parameters.put("cveSector", sector);
		parameters.put("cveRama", rama);
		parameters.put("cveSubRama", subRama);		
		return entidadService.findByProperties(PersonaMoralMidas.class, parameters);
	}

	@Override
	public List<PersonaMidas> buscarPersonas(PersonaMidas filtro) {		
		// TODO Auto-generated method stub
		return null;
	}
	

}
