package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatEstatusBitacora;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatEstatusBitacoraService;

/*******************************************************************************
 * Nombre Interface: 	CatEstatusBitacoraServiceImpl.
 * 
 * Descripcion: 		Contiene la logica del manejo de los Servicios para el 
 * 						objeto CatEstatusBitacora.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class CatEstatusBitacoraServiceImpl implements CatEstatusBitacoraService{
	private static final long serialVersionUID = 1L;
	
	@EJB private EntidadService entidadService;

	@Override
	public List<CatEstatusBitacora> findByStatusReg(boolean estatusReg) {
		return entidadService.findByProperty(CatEstatusBitacora.class, "estatusRegistro", estatusReg?0:1);
	}
}