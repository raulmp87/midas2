package mx.com.afirme.midas2.service.impl.poliza;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.poliza.EnvioEmailPolizaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

@Stateless
public class EnvioEmailPolizaServiceImpl implements EnvioEmailPolizaService {

	
	@PersistenceContext
	private EntityManager entityManager;
		
	@Resource
	private Validator validator;
	
	@EJB
	private EndosoFacadeRemote endosoFacadeRemote;
	
	@EJB
	private EntidadContinuityService entidadContinuityService;
	
	@EJB
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private SeguroObligatorioService seguroObligatorioService;

	private static final Logger log = Logger.getLogger(EnvioEmailPolizaServiceImpl.class);
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void enviarEmailPorInciso(EnviarEmailPorIncisoParameters parameters) {
		Set<ConstraintViolation<EnviarEmailPorIncisoParameters>> constraintViolations = validator.validate(parameters);
		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
		}
		PolizaDTO poliza = entityManager.find(PolizaDTO.class, parameters.getIdToPoliza());
		if (poliza == null) {
			throw new RuntimeException("La poliza " + parameters.getIdToPoliza() + " no existe.");
		}
		//Validacion Impresion de Seguro Obligatorio
	    if(poliza.getClaveAnexa() != null && poliza.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
			//Valida Seguro Obligatorio Pagado
		 	MensajeDTO mensaje = seguroObligatorioService.validaPagoSO(parameters.getIdToPoliza());
		 	if(mensaje != null && mensaje.getMensaje() != null && !mensaje.getMensaje().isEmpty()){
		 		throw new RuntimeException("No es posible imprimir la Poliza de Seguro Obligatorio hasta el pago de la Voluntaria.");
			}		
		}
		
		DateTime validOn;
		DateTime knownOn;
		Integer numero = poliza.getCotizacionDTO().getIdToCotizacion().intValue();
		if (parameters.getValidOn() != null && parameters.getKnownOn() != null) {
			validOn = new DateTime(parameters.getValidOn().getTime());
			knownOn = new DateTime(parameters.getKnownOn().getTime());
		} else {
			EndosoDTO endoso = endosoFacadeRemote.findById(new EndosoId(parameters.getIdToPoliza(), (short)0));
			validOn = new DateTime(endoso.getValidFrom().getTime());
			knownOn = new DateTime(endoso.getRecordFrom().getTime());
		}
				
		CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class, "numero", numero);
		for (BitemporalInciso inciso : cotizacionContinuity.getIncisoContinuities().get(validOn, knownOn)) {
			try {
				String emailContacto = inciso.getValue().getEmailContacto();
				if (StringUtils.isBlank(emailContacto)) {
					continue;
				}
				
				Locale locale = new Locale("es", "MX");
				TransporteImpresionDTO transporteImpresion = generaPlantillaReporteBitemporalService.imprimirPoliza(poliza.getIdToPoliza(), 
						locale, validOn, knownOn, false, parameters.getIncluirRecibo(), parameters.getIncluirNu(), parameters.getIncluirAnexos(), 
						inciso.getContinuity().getNumero(), inciso.getContinuity().getNumero(), null, false, false);
				byte[] impresionPolizaByteArray = transporteImpresion.getByteArray();
	
				//Enviar
				String fileName = "poliza " + poliza.getNumeroPolizaFormateada() + "-" + inciso.getContinuity().getNumero()  + ".pdf";
				ByteArrayAttachment attachment = new ByteArrayAttachment(fileName, TipoArchivo.PDF, impresionPolizaByteArray);
				List<ByteArrayAttachment> attachments = new ArrayList<ByteArrayAttachment>();
				attachments.add(attachment);
				List<String> recipients = new ArrayList<String>();
				recipients.add(emailContacto);

				String subject;
				String messageContent;
				if (parameters.getFrom().equals("emisionautos@scottasesores.com.mx")) {
					//Desarrollo especial para scottasesores para que incluya el texto como lo solicitaron
					subject = "Renovación de Póliza";
					messageContent  = "<html xmlns:v=\"urn:schemas-microsoft-com:vml\" xmlns:o=\"urn:schemas-microsoft-com:office:office\" xmlns:w=\"urn:schemas-microsoft-com:office:word\" xmlns:m=\"http://schemas.microsoft.com/office/2004/12/omml\" xmlns=\"http://www.w3.org/TR/REC-html40\"><head><meta http-equiv=Content-Type content=\"text/html; charset=iso-8859-1\"><meta name=Generator content=\"Microsoft Word 12 (filtered medium)\"><!--[if !mso]><style>v\\:* {behavior:url(#default#VML);}\r\n" + 
							"o\\:* {behavior:url(#default#VML);}\r\n" + 
							"w\\:* {behavior:url(#default#VML);}\r\n" + 
							".shape {behavior:url(#default#VML);}\r\n" + 
							"</style><![endif]--><style><!--\r\n" + 
							"/* Font Definitions */\r\n" + 
							"@font-face\r\n" + 
							"	{font-family:\"MS Mincho\";\r\n" + 
							"	panose-1:2 2 6 9 4 2 5 8 3 4;}\r\n" + 
							"@font-face\r\n" + 
							"	{font-family:\"Cambria Math\";\r\n" + 
							"	panose-1:2 4 5 3 5 4 6 3 2 4;}\r\n" + 
							"@font-face\r\n" + 
							"	{font-family:Calibri;\r\n" + 
							"	panose-1:2 15 5 2 2 2 4 3 2 4;}\r\n" + 
							"@font-face\r\n" + 
							"	{font-family:Tahoma;\r\n" + 
							"	panose-1:2 11 6 4 3 5 4 4 2 4;}\r\n" + 
							"@font-face\r\n" + 
							"	{font-family:\"\\@MS Mincho\";\r\n" + 
							"	panose-1:2 2 6 9 4 2 5 8 3 4;}\r\n" + 
							"@font-face\r\n" + 
							"	{font-family:Verdana;\r\n" + 
							"	panose-1:2 11 6 4 3 5 4 4 2 4;}\r\n" + 
							"@font-face\r\n" + 
							"	{font-family:Webdings;\r\n" + 
							"	panose-1:5 3 1 2 1 5 9 6 7 3;}\r\n" + 
							"/* Style Definitions */\r\n" + 
							"p.MsoNormal, li.MsoNormal, div.MsoNormal\r\n" + 
							"	{margin:0in;\r\n" + 
							"	margin-bottom:.0001pt;\r\n" + 
							"	font-size:11.0pt;\r\n" + 
							"	font-family:\"Calibri\",\"sans-serif\";}\r\n" + 
							"a:link, span.MsoHyperlink\r\n" + 
							"	{mso-style-priority:99;\r\n" + 
							"	color:blue;\r\n" + 
							"	text-decoration:underline;}\r\n" + 
							"a:visited, span.MsoHyperlinkFollowed\r\n" + 
							"	{mso-style-priority:99;\r\n" + 
							"	color:purple;\r\n" + 
							"	text-decoration:underline;}\r\n" + 
							"p.MsoAcetate, li.MsoAcetate, div.MsoAcetate\r\n" + 
							"	{mso-style-priority:99;\r\n" + 
							"	mso-style-link:\"Texto de globo Car\";\r\n" + 
							"	margin:0in;\r\n" + 
							"	margin-bottom:.0001pt;\r\n" + 
							"	font-size:8.0pt;\r\n" + 
							"	font-family:\"Tahoma\",\"sans-serif\";}\r\n" + 
							"span.EstiloCorreo17\r\n" + 
							"	{mso-style-type:personal-compose;\r\n" + 
							"	font-family:\"Calibri\",\"sans-serif\";\r\n" + 
							"	color:windowtext;}\r\n" + 
							"span.TextodegloboCar\r\n" + 
							"	{mso-style-name:\"Texto de globo Car\";\r\n" + 
							"	mso-style-priority:99;\r\n" + 
							"	mso-style-link:\"Texto de globo\";\r\n" + 
							"	font-family:\"Tahoma\",\"sans-serif\";}\r\n" + 
							".MsoChpDefault\r\n" + 
							"	{mso-style-type:export-only;}\r\n" + 
							"@page WordSection1\r\n" + 
							"	{size:8.5in 11.0in;\r\n" + 
							"	margin:70.85pt 85.05pt 70.85pt 85.05pt;}\r\n" + 
							"div.WordSection1\r\n" + 
							"	{page:WordSection1;}\r\n" + 
							"--></style><!--[if gte mso 9]><xml>\r\n" + 
							"<o:shapedefaults v:ext=\"edit\" spidmax=\"2050\" />\r\n" + 
							"</xml><![endif]--><!--[if gte mso 9]><xml>\r\n" + 
							"<o:shapelayout v:ext=\"edit\">\r\n" + 
							"<o:idmap v:ext=\"edit\" data=\"1\" />\r\n" + 
							"</o:shapelayout></xml><![endif]--></head><body lang=ES link=blue vlink=purple><div class=WordSection1><p class=MsoNormal><span style='font-size:14.0pt;color:#1F497D'><img width=70 height=74 id=\"Imagen_x0020_6\" src=\"http://www.segurosafirme.com.mx/MidasWeb/img/scottLogo.png\" alt=scottlogo></span><span lang=ES-MX style='font-size:14.0pt;color:#1F497D'><o:p></o:p></span></p><p class=MsoNormal><span lang=ES-MX style='color:#1F497D'>Scott Asesores</span><span lang=ES-MX style='font-size:14.0pt;color:#1F497D'><o:p></o:p></span></p><p class=MsoNormal><span lang=ES-MX style='color:#1F497D'>Padre Mier 1482 Pte</span><span lang=ES-MX style='font-size:14.0pt;color:#1F497D'><o:p></o:p></span></p><p class=MsoNormal><span lang=ES-MX style='color:#1F497D'>Col. Obispado</span><span lang=ES-MX style='font-size:14.0pt;color:#1F497D'><o:p></o:p></span></p><p class=MsoNormal><span lang=ES-MX style='color:#1F497D'>Monterrey, N.L. México</span><span lang=ES-MX style='font-size:14.0pt;color:#1F497D'><o:p></o:p></span></p><p class=MsoNormal><span lang=EN-US style='color:#1F497D'>Tel: (52) + 81-1365-6060<o:p></o:p></span></p><p class=MsoNormal><span lang=EN-US style='color:#1F497D'>01 800 087 SCOTT (72688)</span><span lang=EN-US style='font-size:14.0pt;color:#1F497D'><o:p></o:p></span></p><p class=MsoNormal><span lang=ES-MX style='color:#1F497D'><a href=\"http://www.scottaasesores.com.mx/\"><span lang=EN-US>www.scottaasesores.com.mx</span></a></span><span lang=EN-US style='font-size:14.0pt;color:#1F497D'><o:p></o:p></span></p><p class=MsoNormal><span lang=EN-US><o:p>&nbsp;</o:p></span></p><p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><b><span style='font-size:24.0pt;font-family:Webdings;color:green'>P</span></b><b><span style='font-size:9.0pt;color:green'> </span></b><b><span style='font-size:9.0pt;font-family:\"Tahoma\",\"sans-serif\";color:green'>Antes de imprimir este e-mail piense bien si es necesario hacerlo</span></b><b><span style='font-size:24.0pt;font-family:Webdings;color:green'>q</span></b><o:p></o:p></p><p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span style='color:navy'>&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;&#8230;</span><o:p></o:p></p><p class=MsoNormal style='mso-margin-top-alt:auto;mso-margin-bottom-alt:auto'><span style='font-size:9.0pt;font-family:\"Verdana\",\"sans-serif\";color:#663300'>::::::::&nbsp; Email Disclaimer.&nbsp; Queda usted informado que todo el contenido de esta comunicación electrónica es de carácter confidencial y para uso exclusivo del destinatario.&nbsp; Si usted la ha recibido por error, por favor notifíquele al remitente.&nbsp; Queda estrictamente prohibido copiar, distribuir o divulgar el contenido total o parcial de este mensaje electrónico.&nbsp; SCOTT ASESORES se deslinda de toda responsabilidad que resulte en daños causados por virus electrónicos transmitidos con este mensaje.&nbsp; Es responsabilidad del destinatario revisar este mensaje y sus archivos adjuntos para detectar y eliminar la presencia de cualquier virus electrónico.&nbsp; ::::::::::::&nbsp; You are hereby notified that all the contents of this electronic communication is confidential and intended for the exclusive use of the addressed person and/or entity.&nbsp; </span><span lang=EN-US style='font-size:9.0pt;font-family:\"Verdana\",\"sans-serif\";color:#663300'>If you are not the intended recipient, please notify us immediately.&nbsp; It is strictly prohibited to copy, distribute or disseminate all or part of the contents of this message.&nbsp; SCOTT ASESORES is not responsible for any damages caused by electronic virus transmitted with this message.&nbsp; It is the responsibility of the recipient to check this message and its attached files for the presence of any electronic virus and to eliminate them.&nbsp; :::::::::::::::</span><span lang=EN-US><o:p></o:p></span></p></div></body></html>";
				} else {
					subject = "Póliza Seguros Afirme";
					messageContent = "Se le ha enviado una póliza a través de este medio.";
				}
				
				mailService.sendMessage(recipients, subject, messageContent, parameters.getFrom(), null, attachments, null, null);			
			}catch(Exception e) {
				log.error("No se pudo enviar el correo masivo para el bitemporalInciso: " + inciso.getId());
			}
		}
	}
}
