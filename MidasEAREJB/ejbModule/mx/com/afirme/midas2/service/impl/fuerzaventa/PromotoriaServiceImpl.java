package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaService;

@Stateless
public class PromotoriaServiceImpl extends FuerzaDeVentaServiceImpl implements PromotoriaService{

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarPromotorias() {
		return super.listar(null, null, 
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.PROMOTORIA.obtenerTipo());
	}

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarPromotoriasPorOficina(Object id) {
		return super.listar(id,  
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.OFICINA.obtenerTipo(), 
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.PROMOTORIA.obtenerTipo());
	}
	
	@Override
	public Map<String, String> obtenerDireccionesCorreoSuperioresPromotoria(Promotoria promotoria)
	{
		Map<String, String> correosSuperiores = new HashMap<String, String>();
		
		Ejecutivo ejecutivo = promotoria.getEjecutivo();
		Gerencia gerencia = ejecutivo.getGerencia();
		CentroOperacion centroOperacion = gerencia.getCentroOperacion();		
		
		//Ejecutivo
		if(ejecutivo != null && ejecutivo.getPersonaResponsable() != null && 
				ejecutivo.getPersonaResponsable().getEmail() != null)
		{
			correosSuperiores.put("EJECUTIVO", ejecutivo.getPersonaResponsable().getEmail());
		}
				
		//Gerencia
		if(gerencia != null && gerencia.getCorreoElectronico() != null)
		{
			correosSuperiores.put("GERENCIA", gerencia.getCorreoElectronico());
		}
				
		//Centro Operacion
		if(centroOperacion != null && centroOperacion.getCorreoElectronico() != null)
		{
			correosSuperiores.put("CENTRO_OPERACION", centroOperacion.getCorreoElectronico());			
		}		
		
		return correosSuperiores;
	}

}
