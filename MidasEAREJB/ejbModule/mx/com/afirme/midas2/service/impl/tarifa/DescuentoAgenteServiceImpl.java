package mx.com.afirme.midas2.service.impl.tarifa;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotEmpty;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.tarifa.DescuentoAgenteService;
import mx.com.afirme.midas2.utils.LeerArchivo;
import mx.com.afirme.midas2.exeption.ErrorBuilder;

/**
 * Facade for entity DescuentosAgenteDTO.
 * 
 * 
 * 
 */
@Stateless
public class DescuentoAgenteServiceImpl extends EntidadHistoricoDaoImpl  implements DescuentoAgenteService {
	// property constants
	private static final Logger LOG = Logger.getLogger(DescuentoAgenteServiceImpl.class);
	public static final String CLAVEAGENTE = "claveagente";
	public static final String NOMBRE = "nombre";
	public static final String PORCENTAJE = "porcentaje";
	public String respuestaMensaje="";
	@PersistenceContext
	private EntityManager entityManager;
	@EJB
	private AgenteMidasService agenteMidasService;
	ErrorBuilder eb = new ErrorBuilder();
	public DescuentosAgenteDTO save(DescuentosAgenteDTO entity) {
		LOG.trace("saving DescuentosAgenteDTO instance");
		LOG.info("Result Negocio>>"+entity.getClaveNegocio());
		LOG.info("Result ClaveAgente>>"+entity.getClaveagente());
		LOG.info("Result getPorcentaje>>"+entity.getPorcentaje());
		LOG.info("Result getNumerotelefono>>"+entity.getNumerotelefono());
		LOG.info("Result getesNuevoAgente>>"+entity.getEsNuevoAgente());
		try {
			if(entity.getClaveNegocio() == null || entity.getClaveNegocio()==""){
				entity.setClaveNegocio("A");
			}
			if(entity.getPorcentaje().equals("0") || entity.getPorcentaje()== null){
				entity.setPorcentaje("0.000001");
			}
			if(entity.getNumerotelefono() == null){
				entity.setNumerotelefono("0");
			}
			entity.setFecharegistro(new Date());
			entity.setFechamodificacion(new Date());			
			entity.setBajalogica(new Short("1"));
			entity.setClaveNegocio("A");
			DescuentosAgenteDTO descuentoAutos= poblarDescuentosAgenteDTO(entity);
			if(existeDescuentoAgente(descuentoAutos) && !entity.getEsNuevoAgente()){
					throw new ApplicationException(eb.addFieldError("Error",
					"La Clave Promo ya Existe"));
			}
			else{
				if(entity.getClaveagente()!= null && entity.getClavepromo() != null){
					if(!entity.getClaveagente().trim().equals("") || !entity.getClavepromo().trim().equals("")){
						if(!existeDescuentoAgente(descuentoAutos)){
							entityManager.persist(descuentoAutos);
							LOG.info("save successful descuentoAutos");
						}
						//if(entity.getPorcentaje().equals("0.000001")){
							entity.setPorcentaje("0");
						//}
						entity.setClaveNegocio("V");
						DescuentosAgenteDTO descuentoVida= poblarDescuentosAgenteDTO(entity);
						if(!existeDescuentoAgente(descuentoVida)){
							entityManager.persist(descuentoVida);
							LOG.info("save successful descuentoVida");
						}
						//if(entity.getPorcentaje().equals("0.000001")){
							entity.setPorcentaje("0");
						//}
						entity.setClaveNegocio("D");
						DescuentosAgenteDTO descuentoDanios= poblarDescuentosAgenteDTO(entity);
						if(!existeDescuentoAgente(descuentoDanios)){
							entityManager.persist(descuentoDanios);
							LOG.info("save successful descuentoDanios");
						}
					}
				}
			}
			
			LOG.info("save all successful");
		}catch (RuntimeException re) {
			LOG.error("save failed", re);
			throw re;
		}
		return entity;
	}
	
	
	private DescuentosAgenteDTO poblarDescuentosAgenteDTO(DescuentosAgenteDTO param) {
        DescuentosAgenteDTO descuentoAgenteMovil= new DescuentosAgenteDTO();
        descuentoAgenteMovil.setBajalogica(param.getBajalogica());
        descuentoAgenteMovil.setClaveagente(param.getClaveagente());
        descuentoAgenteMovil.setClavepromo(param.getClavepromo());
        descuentoAgenteMovil.setEmail(param.getEmail());
        descuentoAgenteMovil.setClaveNegocio(param.getClaveNegocio());
        descuentoAgenteMovil.setNombre(param.getNombre());
        descuentoAgenteMovil.setPorcentaje(param.getPorcentaje());
        descuentoAgenteMovil.setNumerotelefono(param.getNumerotelefono());
        descuentoAgenteMovil.setFechamodificacion(param.getFechamodificacion());
        descuentoAgenteMovil.setFecharegistro(param.getFecharegistro());
        return descuentoAgenteMovil;
  }
	
	public boolean setDescuentoAgenteExcelList(List<DescuentosAgenteDTO> descuentosAgenteDTOExcelList){
		LOG.info("Saving all descuentosAgenteDTOExcelList instances");
		try {
			for (DescuentosAgenteDTO descuentosAgenteDTO : descuentosAgenteDTOExcelList) {
				if(descuentosAgenteDTO!= null){
					DescuentosAgenteDTO tmpDescuentoAgente=null;
					List<DescuentosAgenteDTO> tmp= new ArrayList<DescuentosAgenteDTO>();	
					tmp=findByClaveagente(descuentosAgenteDTO.getClaveagente());
					for(DescuentosAgenteDTO tmpDescAgente:tmp){
						tmpDescuentoAgente=tmpDescAgente;
					}
					if(tmpDescuentoAgente!=null){
						descuentosAgenteDTO.setFecharegistro(new Date());
						descuentosAgenteDTO.setFechamodificacion(new Date());
						save(descuentosAgenteDTO);
					}
				}
			}
			return true;
		} catch (RuntimeException re) {
			LOG.error("Saving descuentosAgenteDTOExcelList failed", re);
			return false;
		}		
	}
	public void delete(DescuentosAgenteDTO entity) {
		LOG.info("deleting DescuentosAgenteDTO instance");
		try {
			entity = entityManager.getReference(DescuentosAgenteDTO.class,
					entity.getIdtcdescuentosagente());
			entityManager.remove(entity);
			LOG.info("delete successful");
		} catch (RuntimeException re) {
			LOG.error("delete failed", re);
			throw re;
		}
	}
	public String update(DescuentosAgenteDTO entity) {
		LOG.info("updating DescuentosAgenteDTO instance");
		try {
			//ErrorBuilder eb = new ErrorBuilder();
			DescuentosAgenteDTO validarExisteduplicado=new DescuentosAgenteDTO();
			validarExisteduplicado.setIdtcdescuentosagente(entity.getIdtcdescuentosagente());
			validarExisteduplicado.setClaveNegocio(entity.getClaveNegocio());
			validarExisteduplicado.setClavepromo(entity.getClavepromo());			
			List<DescuentosAgenteDTO> lista=findByFiltersDiferentID(validarExisteduplicado);
			if(lista.isEmpty()){
				entity.setFechamodificacion(new Date());			
				validaActualizaClavePromo(entity.getIdtcdescuentosagente(), entity.getClavepromo());
				DescuentosAgenteDTO result = entityManager.merge(entity);
				LOG.info("update successful");
				respuestaMensaje= "Actualizacion Exitosa";
			}
			else{
				respuestaMensaje= "duplicado";
			}
		} catch (RuntimeException re) {
			LOG.error("update failed", re);
			throw re;
		}
		return respuestaMensaje;
	}

	public DescuentosAgenteDTO findById(Long id) {
		LOG.info("finding DescuentosAgenteDTO instance with id: " + id);
		try {
			DescuentosAgenteDTO instance = entityManager.find(
					DescuentosAgenteDTO.class, id);
			return instance;
		} catch (RuntimeException re) {
			LOG.error("find failed", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<DescuentosAgenteDTO> findByProperty(String propertyName,
			final Object value) {
		LOG.info("finding DescuentosAgenteDTO instance with property: "
				+ propertyName + ", value: " + value);
		try {
			final String queryString = "select model from DescuentosAgenteDTO model where  model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("find by property name failed", re);
			throw re;
		}
	}
	public List<DescuentosAgenteDTO> findByPropertyActivos(String claveNegocio,String propertyName,
			final Object value) {
		LOG.info("finding DescuentosAgenteDTO instance with property: "
				+ propertyName + ", value: " + value);
		try {
			final String queryString = "select model from DescuentosAgenteDTO model where  model.bajalogica=1 and model."
					+ propertyName + "= :propertyValue" 
					+ " and model.claveNegocio" + "= :claveNegocioValue" ;
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			query.setParameter("claveNegocioValue", claveNegocio);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("find by property name failed", re);
			throw re;
		}
	}
	public List<DescuentosAgenteDTO> findByClaveagente(Object claveagente) {
		return findByProperty(CLAVEAGENTE, claveagente);
	}

	public List<DescuentosAgenteDTO> findByNombre(Object nombre) {
		return findByProperty(NOMBRE, nombre);
	}

	public List<DescuentosAgenteDTO> findByPorcentaje(Object porcentaje) {
		return findByProperty(PORCENTAJE, porcentaje);
	}

	@SuppressWarnings("unchecked")
	public List<DescuentosAgenteDTO> findAll() {
		LOG.info(">>findByFilters  DescuentosAgenteDTO instances");
		LOG.info("finding all DescuentosAgenteDTO instances");
		try {
			final String queryString = "select model from DescuentosAgenteDTO model";
			Query query = entityManager.createQuery(queryString);
			query.setMaxResults(100);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOG.error("find all failed", re);
			throw re;
		}
	}

	public List<DescuentosAgenteDTO> findByFilters(DescuentosAgenteDTO filter) {
		LOG.info("findByFilters  DescuentosAgenteDTO instances");
		LOG.info("filter.getClaveNegocio()>>"+filter.getClaveNegocio());
		LOG.info("filter.getClavepromo()>>"+filter.getClavepromo());
		List<DescuentosAgenteDTO> lista = new ArrayList<DescuentosAgenteDTO>();
		Map<Integer, Object> params = new HashMap<Integer, Object>();
		final StringBuilder queryString = new StringBuilder("");
		try {
			queryString
					.append(" select distinct entidad.idtcdescuentosagente as idtcdescuentosagente ," +
							"entidad.claveagente as claveagente,entidad.nombre as nombre," +
							"entidad.porcentaje as porcentaje,entidad.fecharegistro as fecharegistro ," +
							"entidad.fechamodificacion as fechamodificacion, entidad.bajalogica as bajalogica," +
							"entidad.claveNegocio as claveNegocio,entidad.clavepromo as clavepromo");
			queryString.append(" from MIDAS.TCDESCUENTOSAGENTE entidad ");
			queryString.append(" where ");
			if (isNotNull(filter)) {
				int index = 1;
				if (isNotEmpty(filter.getIdtcdescuentosagente())) {
					addConditionAgnt(index,queryString,
							" entidad.idtcdescuentosagente =? ");
					params.put(index, filter.getIdtcdescuentosagente());
					index++;
				}
				if (isNotEmpty(filter.getClaveagente())) {
					addConditionAgnt(index,queryString, " entidad.claveagente=? ");
					params.put(index, filter.getClaveagente());
					index++;
				}

				if (filter.getNombre() != null) {
					addConditionAgnt(index,queryString, " entidad.nombre=? ");
					params.put(index, filter.getNombre());
					index++;
				}
				if (filter.getBajalogica() != null) {
					addConditionAgnt(index,queryString, " entidad.bajalogica=? ");
					params.put(index, filter.getBajalogica());
					index++;
				}
				if (isNotNull(filter.getFechamodificacion())) {
					Date fecha = filter.getFechamodificacion();
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					String fechaString = format.format(fecha);
					addCondition(queryString,
							" TRUNC(entidad.fechamodificacion) >= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
					params.put(index, fechaString);
					index++;
				}
				if (isNotNull(filter.getFecharegistro())) {
					Date fecha = filter.getFecharegistro();
					SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
					String fechaString = format.format(fecha);
					addCondition(queryString,
							" TRUNC(entidad.fecharegistro) <= TRUNC(TO_DATE(?,'DD/MM/YYYY')) ");
					params.put(index, fechaString);
					index++;
				}
				if (isNotEmpty(filter.getClavepromo())) {
					addConditionAgnt(index,queryString, " entidad.clavepromo=? ");
					params.put(index, filter.getClavepromo());
					index++;
				}
				if (isNotEmpty(filter.getClaveNegocio())) {
					addConditionAgnt(index,queryString, " entidad.claveNegocio=? ");
					params.put(index, filter.getClaveNegocio());
					index++;
				}				
				if (params.isEmpty()) {
					int lengthWhere = "where ".length();
					queryString.replace(queryString.length() - lengthWhere,
							queryString.length(), " ");
				}
				String finalQuery = queryString.toString()
						+ " order by entidad.idtcdescuentosagente desc ";
				Query query = entityManager.createNativeQuery(finalQuery,
						DescuentosAgenteDTO.class);
				if (!params.isEmpty()) {
					for (Integer key : params.keySet()) {
						query.setParameter(key, params.get(key));
					}
				}
				query.setMaxResults(100);
				lista = query.getResultList();
				LOG.info("Query="+query.toString());
			}

		} catch (RuntimeException re) {
			LOG.error("findByFilters all failed", re);
			throw re;
		}
		return lista;

	}
	/*
	 * Carga Masiva 
	 */
	private List<String> registrosAfectados = new ArrayList<String>();
	private List<Map<String,String>>cargaMasiva = new ArrayList<Map<String,String>>();
	private Map<String, String> obtenerDato;
	
	public boolean existeDescuentoAgente(DescuentosAgenteDTO param){
		boolean result;
		try{
			List<DescuentosAgenteDTO> descuentosAgenteTmpList=null;
			DescuentosAgenteDTO descuentosAgenteDTO= new DescuentosAgenteDTO();
			descuentosAgenteDTO.setClavepromo(param.getClavepromo());
	        descuentosAgenteDTO.setClaveNegocio(param.getClaveNegocio());
			descuentosAgenteTmpList = findByFilters(descuentosAgenteDTO);
			LOG.info("existeDescuentoAgente>>"+param.getClaveNegocio());
			if(descuentosAgenteTmpList.isEmpty()){
				result = false;
			}else{
				result = true;
			}
		}catch (RuntimeException re) {
			LOG.error("error existeDescuentoAgente", re);
			throw re;
		}
		
		return result;
	}
	

	public boolean existeClavePromoClaveNegocio(DescuentosAgenteDTO filter){
		List<DescuentosAgenteDTO> clavePromoClaveNegocioList=null;
		clavePromoClaveNegocioList= findByFiltersDiferentID(filter);
		if(clavePromoClaveNegocioList.isEmpty()){
			return false;
		}
		else{
			return true;
		}
		
	}
	public List<String> cargaMasiva(String archivo) throws Exception{
		int cont = 0;
		int registrosNoInsertados=0;
		int registrosInsertados=0;
		cargaMasiva.addAll(LeerArchivo.leer(archivo));
		registrosAfectados.clear();
		String claveAgente;
		Agente agente;
		for(Map<String, String> iterador:cargaMasiva){
			try {
				DescuentosAgenteDTO descuentosAgenteDTO = new DescuentosAgenteDTO();
				obtenerDato = new HashMap<String, String>();
				obtenerDato.putAll(cargaMasiva.get(cont));
				agente=new Agente();
				boolean agenteValido=false;
				claveAgente=obtenerDato.get("CLAVEAGENTE");
				if(existeDescuentoAgente(descuentosAgenteDTO)==true){
					registrosAfectados.add("Registro "+String.valueOf(cont)+" "+ "Clave Promo Ya existe");
					registrosNoInsertados++;
					cont++;		
				}
				else{
					if(claveAgente!=null){
						agente.setIdAgente(new Long(claveAgente.trim()));
						List<Agente> agenteList;
						agenteList= agenteMidasService.findByFilters(agente);
						for(Agente agenteTmp:agenteList){
							if(agenteTmp!=null)
								agenteValido=true;
						}
						if(agenteValido==true){
							descuentosAgenteDTO.setClaveagente(obtenerDato.get("CLAVEAGENTE").trim());
							descuentosAgenteDTO.setNombre(obtenerDato.get("NOMBRE").trim());
							descuentosAgenteDTO.setPorcentaje(obtenerDato.get("PORCENTAJE").trim());
							String activo=obtenerDato.get("ACTIVO").trim();
							descuentosAgenteDTO.setBajalogica(Short.parseShort(activo));
							descuentosAgenteDTO.setClavepromo(obtenerDato.get("CLAVEPROMO").trim());
							descuentosAgenteDTO.setNumerotelefono(obtenerDato.get("NUMEROTELEFONO").trim());
							descuentosAgenteDTO.setEmail(obtenerDato.get("EMAIL").trim());
							descuentosAgenteDTO.setFechamodificacion(new Date());
							descuentosAgenteDTO.setFecharegistro(new Date());
							descuentosAgenteDTO.setClaveNegocio(obtenerDato.get("CLAVENEGOCIO").trim());
							save(descuentosAgenteDTO);
							registrosInsertados++;
							cont++;
						}
						else{
							registrosAfectados.add("Registro "+String.valueOf(cont)+" "+ "Clave Agente  no existe");
							registrosNoInsertados++;
							cont++;	
						}
		
					}
					else{
						registrosAfectados.add("Registro "+String.valueOf(cont)+" "+ "No tiene clave Agente");
						registrosNoInsertados++;
						cont++;
					}
				}
				
			/**/

			} catch (Exception e) {
				e.toString();
				e.printStackTrace();
				String error = e.toString().replace("java.lang.Exception:", "");
				registrosNoInsertados++;
				cont++;
				
				registrosAfectados.add("Registro "+String.valueOf(cont)+" "+error);
			}
		}
		registrosAfectados.add(String.valueOf(registrosInsertados));
		registrosAfectados.add(String.valueOf(registrosNoInsertados));
		cargaMasiva.clear();
		return registrosAfectados;
	}
	public List<DescuentosAgenteDTO> findByFiltersDiferentID(DescuentosAgenteDTO filter){
		LOG.info(">>>findByFilters  DescuentosAgenteDTO instances");
		List<DescuentosAgenteDTO>  lista=new ArrayList<DescuentosAgenteDTO>();
		Map<Integer,Object> params=new HashMap<Integer, Object>();
		final StringBuilder queryString=new StringBuilder("");
		try{
			queryString.append(" select distinct entidad.idtcdescuentosagente as idtcdescuentosagente ,entidad.claveagente as claveagente,entidad.nombre as nombre,entidad.porcentaje as porcentaje,entidad.fecharegistro as fecharegistro ,entidad.fechamodificacion as fechamodificacion, entidad.bajalogica as bajalogica, entidad.clavepromo as clavepromo, entidad.claveNegocio as claveNegocio ");
			queryString.append(" from MIDAS.TCDESCUENTOSAGENTE entidad ");
			queryString.append(" where ");
			if(isNotNull(filter)){
				int index=1;
				if(isNotNull(filter.getIdtcdescuentosagente())){
					addCondition(queryString, " entidad.idtcdescuentosagente <>? ");
					params.put(index, filter.getIdtcdescuentosagente());
					index++;
				}
				if(isNotNull(filter.getClaveagente())){
					addCondition(queryString, " entidad.claveagente=? ");
					params.put(index, filter.getClaveagente());
					index++;
				}
				if(filter.getEmail()!=null){
					addCondition(queryString, " entidad.email=? ");
					params.put(index, filter.getEmail());
					index++;					
				}
				if(filter.getNumerotelefono()!=null){
					addCondition(queryString, " entidad.numerotelefono=? ");
					params.put(index, filter.getNumerotelefono());
					index++;					
				}
				if(filter.getClavepromo()!=null){
					addCondition(queryString, " entidad.clavepromo=? ");
					params.put(index, filter.getClavepromo());
					index++;					
				}
				if(filter.getClaveNegocio()!=null){
					addCondition(queryString, " entidad.claveNegocio=? ");
					params.put(index, filter.getClaveNegocio());
					index++;					
				}

				if(params.isEmpty()){
					int lengthWhere="where ".length();
					queryString.replace(queryString.length()-lengthWhere,queryString.length()," ");
				}
				String finalQuery=getQueryString(queryString)+" order by entidad.idtcdescuentosagente desc ";
				finalQuery.replaceAll("and", "or");
				Query query=entityManager.createNativeQuery(finalQuery,DescuentosAgenteDTO.class);
				if(!params.isEmpty()){
					for(Integer key:params.keySet()){
						query.setParameter(key,params.get(key));
					}
				}
				query.setMaxResults(100);
				LOG.info("query>>"+query.toString());
				lista=query.getResultList();
			}
		
		} catch (RuntimeException re) {
			LOG.error("findByFilters all failed", re);
			throw re;
		}
		return lista;
		
	}
	
	public List<RamoDTO> obtenerRamos(){
		return null;
	}
	
	private boolean actualizarClavePromo(String clavePromoActual, String clavePromoNuevo) {
	    LOG.info("»>.actualizarClavePromo ...  clavePromoActual:" + clavePromoActual
	        + ";clavePromoNuevo:" + clavePromoNuevo);	    
	    boolean actualizado = false;
	    try{
	    	List<DescuentosAgenteDTO> lista=this.findByProperty("clavepromo", clavePromoActual);
		    for(DescuentosAgenteDTO object :lista){
		    	if(object!=null){
		    		object.setClavepromo(clavePromoNuevo);
			    	entityManager.merge(object);	
		    	}	    	
		    }	 
		    actualizado=true;
	    }
	    catch(RuntimeException re){
	    	LOG.error("error",re);
	    	throw re;
	    }
	    
	    return actualizado;
	  }
	
	private boolean validaActualizaClavePromo(Long id, String clavePromoNuevo) {
		LOG.info("»>.validaActualizaClavePromo>>>clavePromoNuevo:" + clavePromoNuevo);
		    boolean actualizado = false;
		    try{
			    DescuentosAgenteDTO descuentoAgenteValida = new DescuentosAgenteDTO();
			    descuentoAgenteValida = findById(id);
				if(!descuentoAgenteValida.getClavepromo().equals(clavePromoNuevo)){
					actualizado = actualizarClavePromo(descuentoAgenteValida.getClavepromo(),clavePromoNuevo);
				}
		    }catch (RuntimeException re) {
			      actualizado = false;
			      LOG.error("validaActualizaClavePromo fallida  ", re);
			      throw re;
			}
		    return actualizado;
	}
	
	   private static boolean isNumeric(String cadena){
	    	try {
	    		Double.parseDouble(cadena);
	    		return true;
	    	} catch (NumberFormatException nfe){
	    		return false;
	    	}
	    }
}