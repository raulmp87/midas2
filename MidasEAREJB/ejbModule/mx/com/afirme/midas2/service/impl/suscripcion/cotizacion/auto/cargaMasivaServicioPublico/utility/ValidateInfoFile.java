package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.utility;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.interfaz.centroemisor.CentroEmisorDTO;
import mx.com.afirme.midas.interfaz.centroemisor.CentroEmisorFacadeRemote;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioConfiguracionDerecho;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle.EstatusDetalle;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalleTradSeyMid;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO.EstatusDescriDetalle;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoJobImpl;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.motordecision.MotorDecisionAtributosService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.util.VINValidator;
import mx.com.afirme.midas2.validators.NumSerieValidador;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

public class ValidateInfoFile {
	
	private static final Logger LOG = Logger.getLogger(ValidateInfoFile.class);
	
	protected CentroEmisorFacadeRemote centroEmisorFacade;
	protected TipoUsoVehiculoFacadeRemote tipoUsoVehiculo;
	
	protected NegocioService negocioService;
	protected EntidadService entidadService;
	protected DireccionMidasService direccionMidasService;
	protected ParametroGeneralService parametroGeneralService;
	protected MotorDecisionAtributosService motorDecisionAtributosService;
	protected AgenteMidasService agenteMidasService;
	protected BitacoraService bitacoraService;
	
	private static int BUS_SERVICE_BUSINESS_LINE_ID = 1252;
	private static int TIPO_VALIDA_DEDUCIBLE = 1;
	private static int TIPO_VALIDA_SUMAS = 2;
	private static int TIPO_VALIDA_DECISION = 3;
	
	CargaMasivaServicioPublicoEstatusDescriDetalleDTO infoItem;
	
	public enum ClaveCoberturas{
		COB_ACCIDEN_AUTO_CONDUCTOR("AAC"),	//MUERTE
		COB_ASIS_JURIDICA("AJ"),
		COB_ASIS_VIAJES_VIAL("AV"),
		COB_DA_MATERIALES("DM"),
		COB_EQUIPO_ESPECIAL("EQE"),
		COB_GASTOS_MEDICOS("GMO"),
		COB_RES_CIVIL("RC"),				//RESPONSABILIDAD CIVIL TERCEROS
		COB_RES_CIV_VIAJERO("RCV"),
		COB_ROBO_TOTAL("RT");
		
		private final String clave;

		private ClaveCoberturas(String clave) {
			this.clave = clave;
		}
		
		public String getClave() {
			return clave;
		}
	}
	
	public ValidateInfoFile(CargaMasivaServicioPublicoEstatusDescriDetalleDTO infoItem, NegocioService negocioService, EntidadService entidadService,
							DireccionMidasService direccionMidasService, ParametroGeneralService parametroGeneralService,
							MotorDecisionAtributosService motorDecisionAtributosService, AgenteMidasService agenteMidasService, BitacoraService bitacoraService
							) {
		this.infoItem = infoItem;
		this.negocioService = negocioService;
		this.entidadService = entidadService;
		this.direccionMidasService = direccionMidasService;
		this.parametroGeneralService = parametroGeneralService;
		this.motorDecisionAtributosService = motorDecisionAtributosService;
		this.agenteMidasService = agenteMidasService;
		this.bitacoraService = bitacoraService;
	}
	
	public EstatusDescriDetalle validaEstatus(String estatus){
		LOG.info("VAL-INI ESTATUS");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(estatus != null && estatus != ""){
			List validValues = Arrays.asList(new Object[] {"EMI", "ENI"});
			if(!validValues.contains(estatus.trim().toUpperCase())){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El estatus de la poliza debe de ser uno de los siguientes: " + validValues + ".";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El estatus de la poliza es requerido.";
		}
		
		LOG.info("VAL-FIN ESTATUS");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaIdAgente(Long idAgente, Long idNegocio){
		LOG.info("VAL-INI AGENTE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(idAgente != null && idAgente > 0){	
			if(idNegocio != null && idNegocio > 0){				
				Agente filtroAgente = new Agente();
				filtroAgente.setIdAgente(Long.valueOf(idAgente));
				List<AgenteView> listAgentes = agenteMidasService.findByFilterLightWeight(filtroAgente);
				if(listAgentes.isEmpty()){
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El agente indicad no existe.";
				}else{
					Integer idAge = listAgentes.get(0).getId().intValue();
					List<NegocioAgente> agentesNegocio = entidadService.findByProperty(NegocioAgente.class, "idTcAgente", idAge);
					if(agentesNegocio.isEmpty()){
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "El agente no tiene un negocio asignado.";
					}else{
						for(NegocioAgente negocioAgente : agentesNegocio){
							if(negocioAgente.getNegocio().getIdToNegocio().toString().equals(idNegocio.toString())){
								estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
								descriEstatus = "";
								break;
							}else{
								estatusDetalle = EstatusDetalle.ERROR.toString();
								descriEstatus = "El agente no pertenece al negocio capturado.";
							}
						}
					}
				}			
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio es requerido para la valdacion del agente.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El identificador del agente es requerido.";
		}
		
		LOG.info("VAL-FIN AGENTE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaIdPaquete(Long idPaquete, Long idDetalleCargaMasiva, Long idNegocio, Long idTipoPoliza){
		LOG.info("VAL-INI PAQUETE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(idPaquete != null){
			if(idPaquete > 0){
				List<CargaMasivaServicioPublicoDetalleTradSeyMid> listTradSeyMidasCMSP = entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", idDetalleCargaMasiva);
				CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = listTradSeyMidasCMSP.get(0);
				if(idNegocio != null && idNegocio > 0){
					Negocio negocio = negocioService.findById(idNegocio);
					for(NegocioProducto negocioProducto : negocio.getNegocioProductos()){
						if(negocioProducto.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){
							
							if(idTipoPoliza == null || idTipoPoliza == 0){
								idTipoPoliza = Long.parseLong(negocioProducto.getNegocioTipoPolizaList().get(0).getTipoPolizaDTO().getIdToTipoPoliza().toString());
							}
							
							if(!negocioProducto.getNegocioTipoPolizaList().isEmpty()){
								for(NegocioTipoPoliza negocioTipoPoliza : negocioProducto.getNegocioTipoPolizaList()){
									if(idTipoPoliza != null && idTipoPoliza > 0){
										if(!negocioTipoPoliza.getNegocioSeccionList().isEmpty()){
											for(NegocioSeccion negocioSeccion : negocioTipoPoliza.getNegocioSeccionList()){
												if(negocioSeccion.getSeccionDTO().getIdToSeccion().toString().equals(tradSeyMidasCMSP.getLineaNegIdMidas().toString())){
													if(!negocioSeccion.getNegocioPaqueteSeccionList().isEmpty()){
														for(NegocioPaqueteSeccion negocioPaqueteSeccion : negocioSeccion.getNegocioPaqueteSeccionList()){
															if(negocioPaqueteSeccion.getPaquete().getId().toString().equals(idPaquete.toString())){
																estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
																descriEstatus = "";
																break;
															}else{
																estatusDetalle = EstatusDetalle.ERROR.toString();
																descriEstatus = "El paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio";
															}
														}
														break;
													}else{
														estatusDetalle = EstatusDetalle.ERROR.toString();
														descriEstatus = "Na existen secciones (Lineas de Negocio) configuradas para el producto y negocio indicados";
													}
													break;
												}else{
													estatusDetalle = EstatusDetalle.ERROR.toString();
													descriEstatus = "La seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
												}
											}
											break;
										}else{
											estatusDetalle = EstatusDetalle.ERROR.toString();
											descriEstatus = "No existen secciones (Lineas de Negocio) configuradas para el producto y negocio indicados.";
										}
										break;
									}else{
										estatusDetalle = EstatusDetalle.ERROR.toString();
										descriEstatus = "El tipo de poliza es requerido para realizar validacion, por lo que no se puede consultar el paquete.";
									}
								}
								break;
							}else{
								estatusDetalle = EstatusDetalle.ERROR.toString();
								descriEstatus = "No existen tipos de polizas configurados para el negocio capturado.";
							}
							break;
						}else{
							estatusDetalle = EstatusDetalle.ERROR.toString();
							descriEstatus = "El producto capturado no coincide con el negocio indicado";
						}
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El negocio es requerido para la validacion del paquete";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El producto es invaido.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El producto es requerido";
		}
		
		LOG.info("VAL-FIN PAQUETE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaIdCentroEmisor(Long idCentroEmisor, String usuarioCarga){
		LOG.info("VAL-INI CENRO EMISOR");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";

		List<String> validValues = new ArrayList<String>();
		try {
			if(idCentroEmisor != null){
				boolean existe = false;
				if(idCentroEmisor > 0){
					ServiceLocatorP serviceLocator = ServiceLocatorP.getInstance();
					centroEmisorFacade = serviceLocator.getEJB(CentroEmisorFacadeRemote.class);
					
					List<CentroEmisorDTO> listCentroEmisor = centroEmisorFacade.listarCentrosEmisores(usuarioCarga);
//					List<CentroEmisorDTO> listCentroEmisor = centroEmisorFacade.listarCentrosEmisores("MFERPER");
					if(listCentroEmisor.isEmpty()){
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "No existen centros emisores relacionados con el usuario que intenta cargar el archivo.";
					}else{
						for(CentroEmisorDTO centroEmi : listCentroEmisor){
							validValues.add(centroEmi.getClave().toString());
							if(centroEmi.getClave().toString().trim().equals(idCentroEmisor.toString().trim())){
								existe = true;
								break;
							}
						}						
						if(!existe){
							estatusDetalle = EstatusDetalle.ERROR.toString();
							descriEstatus = "El centro emisor para el agente proporcionado debe de ser \""+ validValues + "\".";
						}
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El centro emisor proporcionado es invalido.";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El centro emisor del agente es requerido.";
			}
		} catch (Exception e) {
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El centro emisor no puede ser validado error inesperado.";
			e.printStackTrace();
		}
		
		LOG.info("VAL-FIN CENTRO EMISOR");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaIdOficina(Long idOficina, Long idAgente){
		LOG.info("VAL-INI OFICINA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		try {
			if(idOficina != null){
				if(idOficina > 0){
//					if(idAgente != null && idAgente > 0){
//						
//						Agente filtroAgente = new Agente();
//						filtroAgente.setIdAgente(idAgente);
//						List<AgenteView> listAgentes = agenteMidasService.findByFilterLightWeight(filtroAgente);
//						filtroAgente.setId(listAgentes.get(0).getId());
//						Agente agente = agenteMidasService.loadById(filtroAgente);
//						if(!Long.valueOf(agente.getIdOficina()).toString().equals(idOficina.toString().trim())){
//							estatusDetalle = EstatusDetalle.ERROR.toString();
//							descriEstatus = "La oficina para el agente proporcionado debe de ser \"" + agente.getIdOficina() + "\".";
//						}
//					}else{
//						estatusDetalle = EstatusDetalle.ERROR.toString();
//						descriEstatus = "El agent es requerido para la validacion de la oficina";
//					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "La oficina proporcionado es invalida.";
				}
				
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La oficina del agente es requerida.";
			}
		} catch (Exception e) {
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "la clave de uso no puede ser validado error inesperado.";
			e.printStackTrace();
		}
		
		LOG.info("VAL-FIN OFICINA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaNumeroPoliza(Long numToPoliza, Boolean existePoliza){
		LOG.info("VAL-INI NUMERO DE POLIZA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";

		if(numToPoliza != null){
			if(numToPoliza > 0){
				if(!existePoliza.equals(null)){
					if(existePoliza){
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "El numero de poliza ya esta emitido.";
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "No se pudo validar a existencia del numero de poliza.";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El numero de poliza es invalido.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El numero de poliza es requerido.";
		}
		
		LOG.info("VAL-FIN NUMEROD DE POLIZA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaNumeroLiquidacion(Long numLiquidacion){
		LOG.info("VAL-INI NUMERO LIQUIDACION");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(numLiquidacion != null && numLiquidacion > 0){
			if(numLiquidacion.toString().length() > 40){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El numero de liquidacion no puede tener mas de 40 caracteres.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El numero de liquidacion es requerido.";
		}
		
		LOG.info("VAL-FIN NUMERO DE LIQUIDACION");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaAutorizacionProsa(String autorizacionProsa){
		LOG.info("VAL-INI AUTORIZACION PROSA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(autorizacionProsa != null && autorizacionProsa != ""){
			if(autorizacionProsa.toString().length() > 10){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La autorizacion prosa no puede tener mas de 10 caracteres.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El numero de autorizacion prosa es requerido.";
		}
		
		LOG.info("VAL-FIN AUTORIZACION PROSA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaFechaVigencia(Date fechaVigencia, Long idNegocio){
		LOG.info("VAL-INI FECHA VIGENCIA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(fechaVigencia != null && fechaVigencia.toString() != "" ){
			if(idNegocio != null && idNegocio > 0 ){
				Negocio negocio = negocioService.findById(idNegocio);
				DateTime fechaVig = new DateTime(fechaVigencia);
				DateTime fechaDiferimiento = (new DateTime(new Date())).plusDays(negocio.getDiasDiferimiento().intValue()); //DIFERIMIENTO
				DateTime fechaRetroactividad = (new DateTime(new Date())).minusDays(negocio.getDiasRetroactividad().intValue()); //RETROACTIVIDAD
				
				if(fechaVig.isBefore(fechaDiferimiento)){
					if(fechaVig.isAfter(fechaRetroactividad)){
						estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
						descriEstatus = "";
					}else{
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "La fecha de vigencia es menor al plazo de retroactividad configurado para el negocio.";
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "La fecha de vigencia es mayor al plazo de diferimiento configurado para el negocio.";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio es requerido para la validacion de la fecha de vigencia.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "La fecha de vigencia es requerida.";
		}
		
		LOG.info("VAL-FIN FECHA VIGENCIA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaFechaEmision(Date fechaEmision, Long idNegocio){
		LOG.info("VAL-INI FECHA EMISION");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(fechaEmision != null && fechaEmision.toString() != ""){
			if(idNegocio != null && idNegocio > 0 ){
				Negocio negocio = negocioService.findById(idNegocio);
				DateTime fechaEmi = new DateTime(fechaEmision);
				DateTime fechaDiferimiento = (new DateTime(new Date())).plusDays(negocio.getDiasDiferimiento().intValue()); //DIFERIMIENTO
				DateTime fechaRetroactividad = (new DateTime(new Date())).minusDays(negocio.getDiasRetroactividad().intValue()); //RETROACTIVIDAD
				
				if(fechaEmi.isBefore(fechaDiferimiento)){
					if(fechaEmi.isAfter(fechaRetroactividad)){
						estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
						descriEstatus = "";
					}else{
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "La fecha de emision es menor al plazo de retroactividad configurado para el negocio.";
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "La fecha de emision es mayor al plazo de diferimiento configurado para el negocio.";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio es requerido para la validacion de la fecha de emision.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "La fecha de emision es requerida.";
		}
		
		LOG.info("VAL-FIN FECHA EMISION");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaVigencia(Long vigencia, Long idPaquete){
		LOG.info("VAL-INI VIGENCIA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(vigencia != null && vigencia > 0){
			List validValues = Arrays.asList(new Long[]{new Long(1), new Long(2), new Long(3)});
			if(!validValues.contains(vigencia)){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La vigencia debe de ser: 1 (ANUAL), 2 (SEMESTRAL) o 3 (TRIMESTRAL).";
			}else{
				if(vigencia.equals(3)){
					if(!idPaquete.equals(1) && !idPaquete.equals(3)){
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "La vigencia 3 (TRIMESTRAL) solo aplica para los paquetes AMPLIA y LIMITADA.";
					}
				}
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "La vigencia es requerida.";
		}
		
		LOG.info("VAL-FIN VIGENCIA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaTipoPersonaCliente(String tipoPersonaCliente){
		LOG.info("VAL-INI TIPO PERSONA CLIENTE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(tipoPersonaCliente != null && tipoPersonaCliente != ""){
			List validValues = Arrays.asList(new Object[] { "PF", "PM" });
			if(!validValues.contains(tipoPersonaCliente)){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El tipo de persona debe de ser uno de: " + validValues + ".";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El tipo de persona es requerido.";
		}
		
		LOG.info("VAL-FIN TIPO PERSONA CLIENTE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaRfcCliente(String rFCCliente){
		LOG.info("VAL-INI RFC CLIENTE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(rFCCliente != null && rFCCliente != ""){
			rFCCliente = rFCCliente.replaceAll(" ", "");
			rFCCliente = rFCCliente.replaceAll("-", "");
			if (rFCCliente.trim().length() > 14){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El rfc no puede tener mas de 14 caracteres";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El rfc es requerido.";
		}
		
		LOG.info("VAL-FIN RFC CLIENTE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaNombreRazonSocial(String tipoPersonaCliente, String nombreRazonSocial){
		LOG.info("VAL-INI NOMBRE / RAZON SOCIAL");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(tipoPersonaCliente != null
				&& !tipoPersonaCliente.isEmpty()){
			int maxLength = tipoPersonaCliente.equals("PF") ? 40 : 80;
			
			if(nombreRazonSocial != null && nombreRazonSocial != ""){
				if(nombreRazonSocial.length() > maxLength){
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El nombre del cliente no puede tener mas de " + Integer.toString(maxLength) + " caracteres de longitud.";
				}
			}else{ 
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El nombre del cliente es requerido.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El tipo de persona es requerido para la validacion del nombre del cliente.";
		}
		
		LOG.info("VAL-FIN NOMBRE / RAZON SOCIAL");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaApellidoPaternoCLiente(String apellidoPaterno){
		LOG.info("VAL-INI APE PATERNO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(apellidoPaterno != null && apellidoPaterno != ""){
			if(apellidoPaterno.length() > 20){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El apellido paterno del cliente no puede tener mas de 20 caracteres de longitud.";
			}
		}
		
		LOG.info("VAL-FIN APE PATERNO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaApellidoMAternoCLiente(String apellidoMaterno){
		LOG.info("VAL-INI APE MATERNO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(apellidoMaterno != null && apellidoMaterno != ""){
			if(apellidoMaterno.length() > 20){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El apellido materno del cliente no puede tener mas de 20 caracteres de longitud.";
			}
		}
		
		LOG.info("VAL-FIN APE MATERNO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaCodigoPostal(String codigoPostalCliente, Long vigencia, Long idNegocio){
		LOG.info("VAL-INI CODIGO POSTAL");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		boolean existe = false;
		String estado = "";
		
		if (codigoPostalCliente != null && codigoPostalCliente != ""){
			DecimalFormat decFormat = new DecimalFormat("00000");
			String postalCode = decFormat.format(Integer.parseInt(codigoPostalCliente.trim()));
			
			List<ColoniaMidas> listColonias = direccionMidasService.obtenerColoniasPorCP(postalCode);
			if(!listColonias.isEmpty()){
				estado = listColonias.get(0).getCiudad().getEstado().getId();
				if(estado == null){
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El codigo postal \"" + codigoPostalCliente + "\" es invalido.";
				}else{
					if(vigencia == 3){
						List validaState = Arrays.asList(new String[]{"19000", "09000", "15000"});
						if(!validaState.contains(estado)){
							estatusDetalle = EstatusDetalle.ERROR.toString();
							descriEstatus = "La vigencia 3 (TRIMESTRAL) solo aplica para los estados: DISTRITO FEDERAL, ESTADO DE MEXICO y NUEVO LEON.";
						}
					}
				}
				if(idNegocio != null && idNegocio >0 ){
					Negocio negocio = negocioService.findById(idNegocio);
					if(!negocio.getNegocioEstadoList().isEmpty()){
						for(NegocioEstado negocioEstado : negocio.getNegocioEstadoList()){
							if(negocioEstado.getEstadoDTO().getId().toString().equals(estado.toString())){
								existe = true;
								break;
							}
						}
					}else{
						existe = false;
					}
					if(!existe){
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "El estado al que pertenece el codigo postal no pertenece al negocio capturado.";
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El negocio es requerido para la validacion del codigo postal.";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El codigo postal \"" + codigoPostalCliente + "\" es invalido.";
			}	
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El codigo postal es requerido.";
		}
		
		LOG.info("VAL-FIN CODIGO POSTAL");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaCalle(String calleCliente){
		LOG.info("VAL-INI CALLE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(calleCliente != null && calleCliente != ""){
			if(calleCliente.length() > 100){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La calle y numero no pueden tener mas de 100 caracteres.";
			}
		}else{ 
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "La calle y numero son requeridos.";
		}
		
		LOG.info("VAL-FIN CALLE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaColonia(String coloniaCliente){
		LOG.info("VAL-INI COLONIA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(coloniaCliente != null && coloniaCliente != ""){
			if(coloniaCliente.length() > 100){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La colonia no puede tener mas de 100 caracters.";
			}
		}else{ 
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "La colonia es requerida.";
		}
		
		LOG.info("VAL-FIN COLONIA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaTelefono(String telefonoCliente){
		LOG.info("VAL-INI TELEFONO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(telefonoCliente != null && telefonoCliente != ""){
			if(telefonoCliente.length() > 12){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El telefono no puede tener mas de 12 caracters.";
			}
		}else{ 
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El telefono es requerido.";
		}
		
		LOG.info("VAL-FIN TELEFONO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
}
	public EstatusDescriDetalle validaCveAMIS(String claveAMIS){
		LOG.info("VAL-INI CLAVE AMIS");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		DecimalFormat decFormat = new DecimalFormat("00000   ");
		
		if(claveAMIS != null && claveAMIS != ""){
			try {
				decFormat.format(Integer.parseInt(claveAMIS));
			} catch (NumberFormatException nfException) {
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La clave AMIS proporcionada es invalida.";
			}
		}else{ 
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "La clave AMIS es requerida.";
		}
		
		LOG.info("VAL-FIN CLAVE AMIS");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	@SuppressWarnings("deprecation")
	public EstatusDescriDetalle validaModelo(Long modelo, Long idDetalleCargaMasiva, Long idPaquete, Long idNegocio, Long idTipoPoliza){
		LOG.info("VAL-INI MODELO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";

		if(modelo != null){
			if(modelo > 0){
				//Obtiene el id de midas de la tabla de traducciones.
				CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = (entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", idDetalleCargaMasiva)).get(0);
				if(idNegocio != null && idNegocio > 0){
					Negocio negocio = negocioService.findById(idNegocio);
					for(NegocioProducto negocioProducto : negocio.getNegocioProductos()){
						if(negocioProducto.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){

							if(idTipoPoliza == null || idTipoPoliza == 0){
								idTipoPoliza = Long.parseLong(negocioProducto.getNegocioTipoPolizaList().get(0).getTipoPolizaDTO().getIdToTipoPoliza().toString());
							}
							
							if(!negocioProducto.getNegocioTipoPolizaList().isEmpty()){
								for(NegocioTipoPoliza negocioTipoPoliza : negocioProducto.getNegocioTipoPolizaList()){
									if(idTipoPoliza != null && idTipoPoliza > 0){
										for(NegocioSeccion negocioSeccion : negocioTipoPoliza.getNegocioSeccionList()){
											if(negocioSeccion.getSeccionDTO().getIdToSeccion().toString().equals(tradSeyMidasCMSP.getLineaNegIdMidas().toString())){
												for(NegocioPaqueteSeccion negocioPaqueteSeccion : negocioSeccion.getNegocioPaqueteSeccionList()){
													if(idPaquete != null && idPaquete > 0){
														if(negocioPaqueteSeccion.getPaquete().getId().toString().equals(idPaquete.toString())){
															int modeloMinimo = new DateTime(new Date()).getYear() - negocioPaqueteSeccion.getModeloAntiguedadMax().intValue();
															if(modeloMinimo >= modelo){
																estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
																descriEstatus = "";
															}else{
																estatusDetalle = EstatusDetalle.ERROR.toString();
																descriEstatus = "El modelo no corresponde a la antiguedad permitida";
															}
															break;
														}else{
															estatusDetalle = EstatusDetalle.ERROR.toString();
															descriEstatus = "El paquete capturado no coincide con la seccion (Linea de Negocio), producto, negocio y modelo indicado";
														}
													}else{
														estatusDetalle = EstatusDetalle.ERROR.toString();
														descriEstatus = "El paquete es requerido para la validacion del modelo indicado";
													}
												}
												break;
											}else{
												estatusDetalle = EstatusDetalle.ERROR.toString();
												descriEstatus = "La seccion (Linea de Negocio) capturada no coincide con el negocio y modelo indicados";
											}
										}
										break;
									}else{
										estatusDetalle = EstatusDetalle.ERROR.toString();
										descriEstatus = "El tipo de poliza es requerido para realizar validacion, por lo que no se puede consultar el modelo.";
									}
								}
								break;
							}else{
								estatusDetalle = EstatusDetalle.ERROR.toString();
								descriEstatus = "No existen tipos de polizas configuraas para el negocio capturado.";
							}
							break;
						}else{
							estatusDetalle = EstatusDetalle.ERROR.toString();
							descriEstatus = "El producto capturado no coincide con el negocio y modelo indicados";
						}
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El negocio es requerido para la validacion de modelo";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El modelo proporcionado es invalido.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El modelo es requerido.";
		}
		
		LOG.info("VAL-FIN MODELO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaNcRepube(String ncRepuve){
		LOG.info("VAL-INI REPUBE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(ncRepuve != null && ncRepuve != ""){
			if(ncRepuve.length() > 8){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El repuve no puede tener mas de 8 caracteres.";
			}
		}else{ 
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El repuve es requerido.";
		}
		 
		LOG.info("VAL-FIN REPUBE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaCveUso(String claveUso, String lineaNegocio, Long idNegocio, Long idDetalleCargaMasiva, Long idProducto, Long idTipoPoliza){
		LOG.info("VAL-INI CLAVE USO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";

		try {
			if(claveUso != null && claveUso != ""){
				List validValuesLinNegBus = Arrays.asList(new Object[] {"PU", "ES", "EM"});
				List validValuesServPub = Arrays.asList(new Object[] {"RT", "TA", "UB"});
				
				ServiceLocatorP serviceLocator = ServiceLocatorP.getInstance();
				tipoUsoVehiculo = serviceLocator.getEJB(TipoUsoVehiculoFacadeRemote.class);
				
				List<TipoUsoVehiculoDTO> listTipoUso = tipoUsoVehiculo.findByProperty("codigoTipoUsoVehiculo", claveUso.trim());
				if(listTipoUso.isEmpty()){
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "La clave de uso no existe";
				}else{
					if(lineaNegocio != null && lineaNegocio != ""){
						if(Integer.parseInt(lineaNegocio.trim()) == BUS_SERVICE_BUSINESS_LINE_ID){
							if(!validValuesLinNegBus.contains(claveUso.trim())){
								estatusDetalle = EstatusDetalle.ERROR.toString();
								descriEstatus = "La clave de uso debe de ser \"PU\" - Publico, \"ES\" - Escolar o \"EM\" - Empleados.";
							}
						}else{
							if(!validValuesServPub.contains(claveUso.trim())){
								estatusDetalle = EstatusDetalle.ERROR.toString();
								descriEstatus = "La clave de uso debe de ser \"RT\" - Ruletero Taxi \"TA\" - Taxi Aeropuerto o \"UB\" - Uber.";
							}
						}
					}else{
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "La seccion es requerida para la validacion de la clave de uso";
					}
					
					if(idNegocio != null && idNegocio > 0){
						Negocio negocio = negocioService.findById(idNegocio);
						CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = (entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", idDetalleCargaMasiva)).get(0);
						
						for(NegocioProducto negocioProducto : negocio.getNegocioProductos()){
							if(negocioProducto.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){

								if(idTipoPoliza == null || idTipoPoliza == 0){
									idTipoPoliza = Long.parseLong(negocioProducto.getNegocioTipoPolizaList().get(0).getTipoPolizaDTO().getIdToTipoPoliza().toString());
								}

								for(NegocioTipoPoliza negTipoPoliza : negocioProducto.getNegocioTipoPolizaList()){
									if(idTipoPoliza != null && idTipoPoliza > 0){
										if(negTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza().toString().equals(idTipoPoliza.toString())){
											for(NegocioSeccion negocioSeccion : negTipoPoliza.getNegocioSeccionList()){
												
												TipoUsoVehiculoDTO tipoUsoVehiculoDto = new TipoUsoVehiculoDTO();
												tipoUsoVehiculoDto.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
												tipoUsoVehiculoDto.setCodigoTipoUsoVehiculo(claveUso);
												tipoUsoVehiculoDto.setDescripcionTipoUsoVehiculo("");
												List <TipoUsoVehiculoDTO> listTipoUsoVehiculoDto = tipoUsoVehiculo.listarFiltrado(tipoUsoVehiculoDto);
												
												if(listTipoUsoVehiculoDto != null
														&& !listTipoUsoVehiculoDto.isEmpty()){
													if(negocioSeccion.getSeccionDTO().getIdToSeccion().toString().equals(tradSeyMidasCMSP.getLineaNegIdMidas().toString())){
														estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
														descriEstatus = "";
														break;
													}else{
														estatusDetalle = EstatusDetalle.ERROR.toString();
														descriEstatus = "La seccion (Linea de Negocio) capturada no coincide con el producto, negocio y clave de uso indicados.";
													}
												}else{
													estatusDetalle = EstatusDetalle.ERROR.toString();
													descriEstatus = "La clave de uso no esta configurado para la linea de negocio seleccionada.";
												}
											}
											break;
										}else{
											estatusDetalle = EstatusDetalle.ERROR.toString();
											descriEstatus = "El tipo de poliza capturado no coincide con el producto ni negocio y clave de uso indicados.";
										}
									}else{
										estatusDetalle = EstatusDetalle.ERROR.toString();
										descriEstatus = "El tipo de poliza es requerido para realizar validacion.";
									}
								}
								break;
							}else{
								estatusDetalle = EstatusDetalle.ERROR.toString();
								descriEstatus = "El producto capturado no pertenece al negocio y clave de uso indicado.";
							}
						}	
					}else{
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "El negocio es requerido para la validacion de la clave de uso";
					}
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La clave de uso es requerida.";
			}
		} catch (Exception e) {
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "la clave de uso no puede ser validado error inesperado.";
			e.printStackTrace();
		}
		
		LOG.info("VAL-FIN CLAVE USO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaPlacas(String placas){
		LOG.info("VAL-INI PLACAS");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(placas != null && placas != ""){
			if(placas.trim().length() > 8){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El numero de placas no puede tener mas de 8 caracteres.";
			}
		}else{ 
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El numero de placas es requerido.";
		}
		 
		LOG.info("VAL-FIN PLACAS");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaNumeroMotor(String numeroMotor){
		LOG.info("VAL-ININUMERO MOTOR");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(numeroMotor != null && numeroMotor != ""){
			if(numeroMotor.trim().length() > 30){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El numero de motor no puede tener mas de 30 caracteres.";
			}
		}else{ 
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El numero de motor es requerido.";
		}
		 
		LOG.info("VAL-FIN NUMERO MOTOR");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaNumeroSerie(String numeroSerie, String numeroPoliza, String nombreUsuario){
		LOG.info("VAL-INI NUMERO SERIE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		NumSerieValidador validaNumSerie = NumSerieValidador.getInstance();
		List<String> listErrorValida = validaNumSerie.validateCargaMasivaSP(numeroSerie);
		
		numeroSerie = numeroSerie.trim().toUpperCase();  
		if(!VINValidator.getInstance().isValid(numeroSerie)){
			bitacoraService.registrar(TIPO_BITACORA.CARGA_MASIVA_SP, EVENTO.ERROR_VALIDACION_VIN, numeroPoliza, 
					"El n\u00famero de serie es inv\u00e1lido. El noveno d\u00edgito deber\u00eda ser un " + 
		    		   VINValidator.getInstance().getVerificationDigit(numeroSerie) +
		    		   " para conformar uno v\u00e1lido. Num. Serie registrado: " + numeroSerie, 
		    		   "Numero de poliza de seycos (Cotizacion.getNumPolizaServicioPublico) ", nombreUsuario);
		}
		
		if(!listErrorValida.isEmpty()){
			String errorValida = "";
			for(String error : listErrorValida){
				errorValida += error.toString() + " ";
			}
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = errorValida;
		}
		
		LOG.info("VAL-FIN NUMERO SERIE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaValorComercial(Double valorComercial, Double limiteEquipoEspecial){
		LOG.info("VAL-INI VALOR COMERCIAL");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(valorComercial != null && valorComercial > 0.00){
			if(valorComercial.isNaN()){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El valor comercial no es valido.";
			}
		}else if(limiteEquipoEspecial != null && limiteEquipoEspecial > 0){
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El valor comercial es requerido cuando se tiene valor de eqipo especial";
		}
		
		LOG.info("VAL-FIN VALOR COMERCIAL");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaTotalPasajeros(Long totalPasajeros){
		LOG.info("VAL-INI TOTAL PASAJEROS");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(totalPasajeros != null && totalPasajeros > 0){
			try {
				Long.valueOf(totalPasajeros.toString());
			} catch (NumberFormatException nfException) {
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El numero de pasajeros proporcionado no es valido.";
			}
		}
		 
		LOG.info("VAL-FIN TOTAL PASAJEROS");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaDescriVehiculo(String descripcionVehiculo, String claveAmis){
		LOG.info("VAL-INI DESCRI VEHICULO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(descripcionVehiculo != null && descripcionVehiculo != ""){
			if(descripcionVehiculo.trim().length() > 80){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La descripcion del vehiculo no puede tener mas de 80 caracteres.";
			}
		}else{
			if(claveAmis != null 
					&& !claveAmis.isEmpty()){
				ParametroGeneralId parametroGeneralId = new ParametroGeneralId(BigDecimal.valueOf(16d), BigDecimal.valueOf(162080d));
				ParametroGeneralDTO parametroGeneral = parametroGeneralService.findById(parametroGeneralId);
				List clavesAmisGenericas = Arrays.asList(parametroGeneral.getValor().split(","));
				
				DecimalFormat decFormat = new DecimalFormat("00000   ");
				claveAmis = decFormat.format(Integer.parseInt(claveAmis));
				
				if(!clavesAmisGenericas.contains(claveAmis.trim())){
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "La clave AMIS no coincide con las configuradas, la descripcion del vehiculo no se puede validar.";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La clave AMIS es requerida para validar la descripcion del vehiculo.";
			}
		}
		 
		LOG.info("VAL-FIN DESCRI VEHICULO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaDerrotero(String derrotero){
		LOG.info("VAL-INI DERROTERO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(derrotero != null && derrotero != ""){
			if(derrotero.trim().length() > 2050){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El derrotero no puede tener mas de 2050 caracteres.";
			}
		}
		 
		LOG.info("VAL-FIN DERROTERO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaRamal(String ramal){
		LOG.info("VAL-INI RAMAL");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(ramal != null && ramal != ""){
			if(ramal.trim().length() > 2050){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El ramal no puede tener mas de 2050 caracteres.";
			}
		}
		 
		LOG.info("VAL-FIN RAMAL");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaRuta(String ruta){
		LOG.info("VAL-INI RUTA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(ruta != null && ruta != ""){
			if(ruta.trim().length() > 2050){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La ruta no puede tener mas de 2050 caracteres.";
			}
		}
		 
		LOG.info("VAL-FIN RUTA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaNumeroEconomico(String numeroEconomico){
		LOG.info("VAL-INI NUM ECONOMICO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(numeroEconomico != null && numeroEconomico != ""){
			if(numeroEconomico.trim().length() > 8){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El numero economico no puede tener mas de 8 caracteres.";
			}
		}
		 
		LOG.info("VAL-FIN NUM ECONOMICO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaPrimaTotal(Double primaTotal){
		LOG.info("VAL-INI PRIMA TOTAL");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(primaTotal != null && primaTotal > 0){
			if(primaTotal.isNaN()){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La prima neta no es correcta";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "La prima neta es requerida.";
		}
		 
		LOG.info("VAL-FIN PRIMA TOTAL");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaDeduDanosMateriales(Double deducibleDanosMateriales, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI DANOS MATERIALES");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
		
		if(deducibleDanosMateriales != null && deducibleDanosMateriales >= 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_DA_MATERIALES.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DEDUCIBLE, claveUso, deducibleDanosMateriales, idAgente, codigoPostalCliente);
		}else if (deducibleDanosMateriales == null){
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_DA_MATERIALES.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DEDUCIBLE, claveUso, deducibleDanosMateriales, idAgente, codigoPostalCliente);
		}
		
		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - El negocio capturado no contiene coberturas danos materiales disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - El tipo de cobertura danos materiales no esta configurado para el negocio, producto, seccion indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - La cobertura danos materiales configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, no existen valores configurados para realizar las validaciones.";
				break;
			case 12:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, el monto no coincide con los deducibles configurados para el negocio.";
				break;
			case 13:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, no existen deducbles configurados para el negocio.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura danos materiales, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN DANOS MATERIALES");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaDeduRobTotal(Double deducibleRoboTotal, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI DEDU ROBO TOTAL");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
		
		if(deducibleRoboTotal != null && deducibleRoboTotal >= 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_ROBO_TOTAL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DEDUCIBLE, claveUso, deducibleRoboTotal, idAgente, codigoPostalCliente);
		}else if (deducibleRoboTotal == null){
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_ROBO_TOTAL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DEDUCIBLE, claveUso, deducibleRoboTotal, idAgente, codigoPostalCliente);
		}
		
		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - El negocio capturado no contiene coberturas robo total disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, el producto capturado no coincide con el negocio indicado.";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados.";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - El tipo de cobertura robo total no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - La cobertura robo total configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, no existen valores configurados para realizar las validaciones.";
				break;
			case 12:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, el monto no coincide con los deducibles configurados para el negocio.";
				break;
			case 13:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, no existen deducbles configurados para el negocio.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura robo total, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}

		LOG.info("VAL-FIN DEDU ROBO TOTAL");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaLimiteRcTerceros(Double limiteRcTerceros, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI LIMITE RC TERCEROS");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
		
		if(limiteRcTerceros != null && limiteRcTerceros> 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_RES_CIVIL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteRcTerceros, idAgente, codigoPostalCliente);
		}else if (limiteRcTerceros == null || limiteRcTerceros == 0){
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_RES_CIVIL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteRcTerceros, idAgente, codigoPostalCliente);
		}

		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El tipo de cobertura responsabilidad civil por danos a terceros no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el producto capturado no coincide con el negocio indicado";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio capturado no contiene coberturas responsabilidad civil por danos a terceros disponibles";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La cobertura responsabilidad civil por danos a terceros configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, no existen valores configurados para realizar las validaciones.";
				break;
			case 14:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el valor a considerar es el cofigurado al valor comercial y no al capturado";
				break;
			case 15:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el valor a considerar es el configurado al valor convenido y no al capturado.";
				break;
			case 16:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el monto no coincide con las sumas aseguradas configuradas.";
				break;
			case 17:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el monto capturado esta fuera del rango configurado.";
				break;
			case 18:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el monto capturado debe ser mayor a cero para poder ser validado.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil por danos a terceros, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN LIMITE RC TERCEROS");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaDeducibleRcTerceros(Double deducibleRcTerceros, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI DEDU RC TERCEROS");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
//		if(deducibleRcTerceros != null && deducibleRcTerceros>= 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_RES_CIVIL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DEDUCIBLE, claveUso, deducibleRcTerceros, idAgente, codigoPostalCliente);
//		}else if (deducibleRcTerceros == null ){
//			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_RES_CIVIL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DEDUCIBLE, claveUso, deducibleRcTerceros, idAgente, codigoPostalCliente);
//		}

		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - El negocio capturado no contiene coberturas responsabilidad civil por danos a terceros disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - El tipo de cobertura responsabilidad civil por danos a terceros no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - La cobertura responsabilidad civil por danos a terceros configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, no existen valores configurados para realizar las validaciones.";
				break;
			case 12:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, el monto no coincide con los deducibles configurados para el negocio.";
				break;
			case 13:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, no existen deducbles configurados para el negocio.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil por danos a terceros, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN DEDU RC TERCEROS");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaLimiteGastosMedicos(Double limiteGastosMedicos, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI LIMITE GASTOS MEDICOS");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
		
		if(limiteGastosMedicos != null && limiteGastosMedicos> 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_GASTOS_MEDICOS.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteGastosMedicos, idAgente, codigoPostalCliente);
		}else if (limiteGastosMedicos == null || limiteGastosMedicos == 0){
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_GASTOS_MEDICOS.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteGastosMedicos, idAgente, codigoPostalCliente);
		}

		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio capturado no contiene coberturas gastos medicos disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El tipo de cobertura gastos medicos no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La cobertura gastos medicos configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, no existen valores configurados para realizar las validaciones.";
				break;
			case 14:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura gastos medicos, el valor a considerar es el cofigurado al valor comercial y no al capturado";
				break;
			case 15:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura gastos medicos, el valor a considerar es el configurado al valor convenido y no al capturado.";
				break;
			case 16:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, el monto no coincide con las sumas aseguradas configuradas.";
				break;
			case 17:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, el monto capturado esta fuera del rango configurado.";
				break;
			case 18:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura gastos medicos, el monto capturado debe ser mayor a cero para poder ser validado.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura gastos medicos, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN LIMITE GASTOS MEDICOS");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaLimiteMuerte(Double limiteMuerte, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI LIMITE MUERTE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
		
		if(limiteMuerte != null && limiteMuerte> 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_ACCIDEN_AUTO_CONDUCTOR.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteMuerte, idAgente, codigoPostalCliente);
		}else if (limiteMuerte == null || limiteMuerte == 0){
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_ACCIDEN_AUTO_CONDUCTOR.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteMuerte, idAgente, codigoPostalCliente);
		}

		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio capturado no contiene coberturas limite por muerte disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El tipo de cobertura limite por muerte no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La cobertura limite por muerte configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, no existen valores configurados para realizar las validaciones.";
				break;
			case 14:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura limite por muerte, el valor a considerar es el cofigurado al valor comercial y no al capturado";
				break;
			case 15:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura limite por muerte, el valor a considerar es el configurado al valor convenido y no al capturado.";
				break;
			case 16:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, el monto no coincide con las sumas aseguradas configuradas.";
				break;
			case 17:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, el monto capturado esta fuera del rango configurado.";
				break;
			case 18:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura limite por muerte, el monto capturado debe ser mayor a cero para poder ser validado.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura limite por muerte, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN LIMITE MUERTE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaLimiteRcViajero(Double limiteRcViajero, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI LIM RC VIAJERO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
		
		if(limiteRcViajero != null && limiteRcViajero> 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_RES_CIV_VIAJERO.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteRcViajero, idAgente, codigoPostalCliente);
		}else if (limiteRcViajero == null || limiteRcViajero == 0){
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_RES_CIV_VIAJERO.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteRcViajero, idAgente, codigoPostalCliente);
		}

		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio capturado no contiene coberturas responsabilidad civil del viajero disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El tipo de cobertura responsabilidad civil del viajero no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La cobertura responsabilidad civil del viajero configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, no existen valores configurados para realizar las validaciones.";
				break;
			case 14:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el valor a considerar es el cofigurado al valor comercial y no al capturado";
				break;
			case 15:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el valor a considerar es el configurado al valor convenido y no al capturado.";
				break;
			case 16:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el monto no coincide con las sumas aseguradas configuradas.";
				break;
			case 17:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el monto capturado esta fuera del rango configurado.";
				break;
			case 18:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el monto capturado debe ser mayor a cero para poder ser validado.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura responsabilidad civil del viajero, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN LIM RC VIAJERO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaDedRCV(Double deducibleRCV, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI DEDU RC VIAJERO");
		String estatusDetalle = "";
		String descriEstatus = "";
		
		int valRetorno = 0;
		
//		if(deducibleRCV != null && deducibleRCV >= 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_RES_CIV_VIAJERO.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DEDUCIBLE, claveUso, deducibleRCV, idAgente, codigoPostalCliente);
//		}else if (deducibleRCV == null ){
//			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_RES_CIV_VIAJERO.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DEDUCIBLE, claveUso, deducibleRCV, idAgente, codigoPostalCliente);
//		}

		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - El negocio capturado no contiene coberturas responsabilidad civil del viajero disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - El tipo de cobertura responsabilidad civil del viajero no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - La cobertura responsabilidad civil del viajero configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, no existen valores configurados para realizar las validaciones.";
				break;
			case 12:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, el monto no coincide con los deducibles configurados para el negocio.";
				break;
			case 13:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, no existen deducbles configurados para el negocio.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Deducible - Para la cobertura responsabilidad civil del viajero, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN DEDU RC VIAJERO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaAsistenciaJuridica(String asistenciaJuridica, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI ASISTENCIA JURIDICA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;

		if(asistenciaJuridica != null && asistenciaJuridica != ""){
			if(!asistenciaJuridica.trim().equals("NO") && !asistenciaJuridica.trim().equals("SI")){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Asistencia juridica " +
        						"invalida. Debe introducir NO si no requiere " +
        						"y SI si la requiere.";
			}else if (asistenciaJuridica.trim().equals("SI")){
				valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_ASIS_JURIDICA.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DECISION, claveUso, null, idAgente, codigoPostalCliente);
			}else if (asistenciaJuridica.trim().equals("NO")){
				valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_ASIS_JURIDICA.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DECISION, claveUso, null, idAgente, codigoPostalCliente);
			}
		}else{
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_ASIS_JURIDICA.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DECISION, claveUso, null, idAgente, codigoPostalCliente);
		}
		
		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio capturado no contiene coberturas asistencia juridica disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El tipo de cobertura asistencia juridica no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La cobertura asistencia juridica configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, no existen valores configurados para realizar las validaciones.";
				break;
			case 19:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, la cobertura no esta configurada como amparada.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia juridica, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN ASISTENCIA JURIDICA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaLimiteEquipoEspecial(Double limiteEquipoEspecial, Long idDetalleCargaMasiva, Long idPaquete, Long idNegocio, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI LIM EQUIPO ESPECIAL");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
		
		if(limiteEquipoEspecial != null && limiteEquipoEspecial > 0){
			valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_EQUIPO_ESPECIAL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteEquipoEspecial, idAgente, codigoPostalCliente);
		}else if (limiteEquipoEspecial == null || limiteEquipoEspecial == 0){
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_EQUIPO_ESPECIAL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_SUMAS, claveUso, limiteEquipoEspecial, idAgente, codigoPostalCliente);
		}

		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio capturado no contiene coberturas equipo especial disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El tipo de cobertura equipo especial no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La cobertura equipo especial configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, no existen valores configurados para realizar las validaciones.";
				break;
			case 14:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura equipo especial, el valor a considerar es el cofigurado al valor comercial y no al capturado";
				break;
			case 15:
				estatusDetalle = EstatusDetalle.AVISO.toString();
				descriEstatus = "Para la cobertura equipo especial, el valor a considerar es el configurado al valor convenido y no al capturado.";
				break;
			case 16:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el monto no coincide con las sumas aseguradas configuradas.";
				break;
			case 17:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el monto capturado esta fuera del rango configurado.";
				break;
			case 18:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el monto capturado debe ser mayor a cero para poder ser validado.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura equipo especial, el negocio es requrido.";
				break;
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}
		
		LOG.info("VAL-FIN LIM EQUIPO ESPECIAL");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaAsistenciaVial(String asistenciaVial, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza, String claveUso, Long idAgente, String codigoPostalCliente){
		LOG.info("VAL-INI ASISTENCIA VIAL");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		int valRetorno = 0;
		
		if(asistenciaVial != null && asistenciaVial != ""){
			if(!asistenciaVial.trim().equals("NO") && !asistenciaVial.trim().equals("SI")){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Asistencia en Viajes y Vial Km \"0\" " +
        						"invalida. Debe introducir NO si no requiere " +
        						"y SI si la requiere.";
			}else if (asistenciaVial.trim().equals("SI")){
				valRetorno = validaCorrespondenciaCobertura(false, ClaveCoberturas.COB_ASIS_VIAJES_VIAL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DECISION, claveUso, null, idAgente, codigoPostalCliente);
			}else if (asistenciaVial.trim().equals("NO")){
				valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_ASIS_VIAJES_VIAL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DECISION, claveUso, null, idAgente, codigoPostalCliente);
			}
		}else{
			valRetorno = validaCorrespondenciaCobertura(true, ClaveCoberturas.COB_ASIS_VIAJES_VIAL.getClave(), idDetalleCargaMasiva, idNegocio, idPaquete, idTipoPoliza, TIPO_VALIDA_DECISION, claveUso, null, idAgente, codigoPostalCliente);
		}

		switch (valRetorno) {
			case 1:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio capturado no contiene coberturas asistencia vial disponibles";
				break;
			case 2:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, el producto capturado no coincide con el negocio indicado";
				break;
			case 3:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, la seccion (Linea de Negocio) capturada no coincide con el producto y negocio indicados";
				break;
			case 4:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, el paquete capturado no coincide con la seccion (Linea de Negocio), producto y negocio indicados.";
				break;
			case 5:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El tipo de cobertura asistencia vial no esta configurado para el negocio, producto, seccion, producto indicados.";
				break;
			case 6:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "La cobertura asistencia vial configurada para el negocio, producto, seccion, paquete es obligatoria.";
				break;
			case 7:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, el tipo de poliza capturada no coincide con el producto y el negocio indicado.";
				break;
			case 8:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, el tipo de poliza es requerido.";
				break;
			case 9:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, el paquete es requerido.";
				break;
			case 10:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, la cave de cobertura es requerido.";
				break;
			case 11:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, no existen valores configurados para realizar las validaciones.";
				break;
			case 19:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, la cobertura no esta configurada como amparada.";
				break;
			case 20:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Para la cobertura asistencia vial, el negocio es requerido.";
				break;	
			case 21:
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "No existen coberturas coincidentes con los filtros proporcionados.";
				break;
		}

		LOG.info("VAL-FIN ASISTENCA VIAL");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaIgualacion(Long igualacion){
		LOG.info("VAL-INI IGUALACION");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(igualacion != null && igualacion != 1){
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "Valor invalido para igualacion de primas, elegir 1 o dejar la celda vacia";
		}
		
		LOG.info("VAL-FIN IGUALACION");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	@SuppressWarnings("null")
	public EstatusDescriDetalle validaDerechos(Double drechos, String codigoPostalCliente, Long idNegocio, Long idProducto, Long idTipoPoliza, Long idDetalleCargaMasiva, Long idPaquete, String claveUso){
		LOG.info("VAL-INI DERECHOS");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		MotorDecisionDTO filtroDerecho = new MotorDecisionDTO();
		CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = (entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", idDetalleCargaMasiva)).get(0);
		
		if(idNegocio != null && idNegocio > 0){
			Negocio negocio = negocioService.findById(idNegocio);
			if(negocio == null){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio capturado no existe por lo que no se puede consultar la suma de derechos indicada";
			}else{
				if(!negocio.getNegocioProductos().isEmpty()){
					for(NegocioProducto negProd : negocio.getNegocioProductos()){
						if(negProd.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){
							
							if(idTipoPoliza == null || idTipoPoliza == 0){
								idTipoPoliza = Long.parseLong(negProd.getNegocioTipoPolizaList().get(0).getTipoPolizaDTO().getIdToTipoPoliza().toString());
							}
							
							if(!negProd.getNegocioTipoPolizaList().isEmpty()){
								for(NegocioTipoPoliza negTipoPoliza : negProd.getNegocioTipoPolizaList()){
									if(idTipoPoliza != null && idTipoPoliza > 0){
										if(negTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza().equals(BigDecimal.valueOf(idTipoPoliza))){
											for(NegocioSeccion negocioSeccion : negTipoPoliza.getNegocioSeccionList()){
												if(negocioSeccion.getSeccionDTO().getIdToSeccion().toString().equals(tradSeyMidasCMSP.getLineaNegIdMidas().toString())){
													for(NegocioPaqueteSeccion negocioPaqueteSeccion : negocioSeccion.getNegocioPaqueteSeccionList()){
														if(idPaquete != null && idPaquete > 0){
															if(negocioPaqueteSeccion.getPaquete().getId().toString().equals(idPaquete.toString())){
																String estado = null;
																String ciudad = null;
																if(codigoPostalCliente != null && codigoPostalCliente != ""){
																	DecimalFormat decFormat = new DecimalFormat("00000");
																	String postalCode = decFormat.format(Integer.parseInt(codigoPostalCliente.trim()));
					
																	List<ColoniaMidas> listColonias = direccionMidasService.obtenerColoniasPorCP(postalCode);
																	if(!listColonias.isEmpty()){
																		estado = listColonias.get(0).getCiudad().getEstado().getId();
																		ciudad = listColonias.get(0).getCiudad().getId();
																	}
																}
					
																//inicia la carga de los filtros para la busqueda
																filtroDerecho.setTipoConsulta(MotorDecisionDTO.TipoConsulta.TIPO_CONSULTA_DERECHOS);
																filtroDerecho.setIdToNegocio(negocio.getIdToNegocio());
				
																List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosMotor = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
				
																//filtros requeridos para la busqueda
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.TRUE,"negocio.idToNegocio",negocio.getIdToNegocio()));
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.TRUE,"tipoPoliza.idToNegTipoPoliza",negTipoPoliza.getIdToNegTipoPoliza()));
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.TRUE,"seccion.idToNegSeccion",negocioSeccion.getIdToNegSeccion()));
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.TRUE,"paquete.idToNegPaqueteSeccion",negocioPaqueteSeccion.getIdToNegPaqueteSeccion()));
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.TRUE,"moneda.idTcMoneda",CargaMasivaServicioPublicoJobImpl.ID_MONEDA_NACIONAL_PESOS));
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.TRUE,"tipoDerecho",NegocioConfiguracionDerecho.TipoDerecho.POLIZA.toString()));
																
																EstadoDTO estadoDto = null;
																CiudadDTO ciudadDto = null;
																TipoUsoVehiculoDTO tipoUsoVehiculoDto = new TipoUsoVehiculoDTO();
																
																if(estado != null){
																	estadoDto = entidadService.findById(EstadoDTO.class, estado);
																}
																if(ciudad != null){
																	ciudadDto = entidadService.findById(CiudadDTO.class, ciudad);
																}
																
																tipoUsoVehiculoDto.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
																tipoUsoVehiculoDto.setCodigoTipoUsoVehiculo(claveUso);
																tipoUsoVehiculoDto.setDescripcionTipoUsoVehiculo("");
																
																List <TipoUsoVehiculoDTO> listTipoUsoVehiculoDto = tipoUsoVehiculo.listarFiltrado(tipoUsoVehiculoDto);
																
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.FALSE,"estado",estadoDto));
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.FALSE,"municipio",ciudadDto));
																filtrosMotor.add(filtroDerecho.new FiltrosMotorDecisionDTO(
																		Boolean.FALSE,"tipoUsoVehiculo", (listTipoUsoVehiculoDto == null || listTipoUsoVehiculoDto.isEmpty()) ? null : listTipoUsoVehiculoDto.get(0)));
																
																filtroDerecho.setFiltrosDTO(filtrosMotor);
																
																EstatusDescriDetalle estatusDescriDetalle = infoItem.new EstatusDescriDetalle();
																estatusDescriDetalle = motorDecisionAtributosService.validaDerechos(filtroDerecho, drechos);
																estatusDetalle = estatusDescriDetalle.getEstatusDetalle();
																descriEstatus = estatusDescriDetalle.getDescriEstatus();
																break;
															}else{
																estatusDetalle = EstatusDetalle.ERROR.toString();
																descriEstatus = "El paquete capturado no coincide con los configurados para realizar validacion, por lo que no se puede consultar los derechos.";
															}
														}else{
															estatusDetalle = EstatusDetalle.ERROR.toString();
															descriEstatus = "El paquete es requerido para realizar validacion, por lo que no se puede consultar los derechos.";
														}
													}
													break;
												}else{
													estatusDetalle = EstatusDetalle.ERROR.toString();
													descriEstatus = "La seccion capturada no coincide con el tipo de poliza, el producto ni negocio capturados por lo que no se puede consultar los derechos.";
												}
											}
											break;
										}else{
											estatusDetalle = EstatusDetalle.ERROR.toString();
											descriEstatus = "El tipo de poliza capturado no coincide con el producto ni negocio capturados por lo que no se puede consultar los derechos.";
										}
										break;
									}else{
										estatusDetalle = EstatusDetalle.ERROR.toString();
										descriEstatus = "El tipo de poliza es requerido para realizar validacion, por lo que no se puede consultar los derechos.";
									}
								}
							}else{
								estatusDetalle = EstatusDetalle.ERROR.toString();
								descriEstatus = "No existen tipos de polizas configuradas para el negocio capturado.";
							}
							break;
						}else{
							estatusDetalle = EstatusDetalle.ERROR.toString();
							descriEstatus = "El producto capturado no coincide con el negocio capturado por lo que no se puede consultar los derechos.";
						}
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "No existen productos configurados para el negocio capturado.";
				}
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El negocio es requerido para realizar validacion, por lo que no se puede consultar los derechos.";
		}
		
		LOG.info("VAL-FIN DERECHOS");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaSolicitarAutorizacion(Long solicitarAutorizacion){
		LOG.info("VAL-INI SOLICI AUTORIZACION");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(solicitarAutorizacion != null){
			if(solicitarAutorizacion.equals(0L) || solicitarAutorizacion.equals(1L)){
				estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
				descriEstatus = "";
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Bandera de autorizacion invalida."
							  + " Debe introducir 0 (cero) si no requiere autorizacion"
							  + " y 1 (uno) si la requiere.";
			}
		}
		
		LOG.info("VAL-FIN SOLICI AUTORIZACION");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaCausaAutorizacion(String causaAutorizacion){
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		//OJOS
		
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaLineaNegocio(String lineaNegocio, Long idDetalleCargaMasiva, Long idNegocio, Long idTipoPoliza){
		LOG.info("VAL-INI LINEA NEGOCIO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = (entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", idDetalleCargaMasiva)).get(0);
		if(lineaNegocio != null && lineaNegocio != ""){
			if(idNegocio != null && idNegocio > 0){
				Negocio negocio = negocioService.findById(idNegocio);
				if(negocio != null){
					for(NegocioProducto negocioProducto : negocio.getNegocioProductos()){
						if(negocioProducto.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){
							
							if(idTipoPoliza == null || idTipoPoliza == 0){
								idTipoPoliza = Long.parseLong(negocioProducto.getNegocioTipoPolizaList().get(0).getTipoPolizaDTO().getIdToTipoPoliza().toString());
							}
							
							for(NegocioTipoPoliza negocioTipoPoliza : negocioProducto.getNegocioTipoPolizaList()){
								if(idTipoPoliza != null && idTipoPoliza > 0){
									for(NegocioSeccion negocioSeccion : negocioTipoPoliza.getNegocioSeccionList()){
										if(negocioSeccion.getSeccionDTO().getIdToSeccion().toString().equals(tradSeyMidasCMSP.getLineaNegIdMidas().toString())){
											estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
											descriEstatus = "";
											break;
										}else{
											estatusDetalle = EstatusDetalle.ERROR.toString();
											descriEstatus = "La seccion (linea de negocio) capturada no coincide con el producto y negocio indicados";
										}
									}
									break;
								}else{
									estatusDetalle = EstatusDetalle.ERROR.toString();
									descriEstatus = "El tipo de poliza es requerido para realizar validacion, por lo que no se puede consultar el paquete.";
								}
							}
							break;
						}else{
							estatusDetalle = EstatusDetalle.ERROR.toString();
							descriEstatus = "el producto capturado no coincide con el negocio y la seccion (linea de negocio) indicado";
						}
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El negocio capturado no coincide con la seccion (linea de negocio) capturada";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio es requerido para validar la seccion (linea de negocio) capturada";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "La linea de negocio es requerida.";
		}
		
		LOG.info("VAL-FIN LINEA NEGOCIO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaAnualMesesSinIntereses(Long anualMesesSinIntereses){
		LOG.info("VAL-INI MESES SIN INTE");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(anualMesesSinIntereses != null && anualMesesSinIntereses > 0){
			if(!anualMesesSinIntereses.equals(0L) && !anualMesesSinIntereses.equals(1L)){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "Bandera de meses sin intereses invalida. Debe introducir 0 (cero) 0 1 (uno).";
			}
		}
		
		LOG.info("VAL-FIN MESES SIN INTE");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaIdNegocio(Long idNegocio){
		LOG.info("VAL-INI NEGOCIO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(idNegocio != null && idNegocio > 0){
			Negocio negocio = negocioService.findById(idNegocio);
			if(negocio == null){
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio no existe";
			}else{
				if(!negocio.isNegocioActivoVigente()){
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El negocio no esta activo o no es vigente.";
				}
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El negocio es requerido.";
		}
		 
		LOG.info("VAL-FIN NEGOCIO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
}
	public EstatusDescriDetalle validaIdProducto(Long idProducto, Long idNegocio, Long idDetalleCargaMasiva){
		LOG.info("VAL-INI PRODUCTO");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(idProducto != null && idProducto > 0){
			boolean existe = false;
			if(idNegocio != null && idNegocio > 0 ){
				Negocio negocio = negocioService.findById(idNegocio);
				if(negocio != null){
					CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = (entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", idDetalleCargaMasiva)).get(0);
					for(NegocioProducto negProd : negocio.getNegocioProductos()){
						if(negProd.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){
							existe = true;
							break;
						}
					}
					if(!existe){
						estatusDetalle = EstatusDetalle.ERROR.toString();
						descriEstatus = "El producto no pertenece al negocio capturado.";
					}
				}else{
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El negocio no existe y es requerido para la validacion de producto.";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio es requerido para la validacion del producto.";
			}
		} 
		
		LOG.info("VAL-FIN PRODUCTO");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	public EstatusDescriDetalle validaIdTipoPoliza(Long idTipoPoliza, Long idProducto, Long idNegocio, Long idDetalleCargaMasiva){
		LOG.info("VAL-INI TIPO POLIZA");
		String estatusDetalle = EstatusDetalle.POR_EMITIR.toString();
		String descriEstatus = "";
		
		if(idNegocio != null && idNegocio > 0){
			boolean existe = false;
			CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = (entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", idDetalleCargaMasiva)).get(0);
			Negocio negocio = negocioService.findById(idNegocio);
			if(negocio != null){
				for(NegocioProducto negProd : negocio.getNegocioProductos()){
					if(negProd.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){
						if(idTipoPoliza == null || idTipoPoliza == 0){
							idTipoPoliza = Long.parseLong(negProd.getNegocioTipoPolizaList().get(0).getTipoPolizaDTO().getIdToTipoPoliza().toString());
						}
						if(idTipoPoliza != null && idTipoPoliza > 0){
							for(NegocioTipoPoliza negTipoPoliza : negProd.getNegocioTipoPolizaList()){
								if(negTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza().toString().equals(idTipoPoliza.toString())){
									existe = true;
									break;
								}
							}
						}else{
							estatusDetalle = EstatusDetalle.ERROR.toString();
							descriEstatus = "El tipo poliza se requiere para ser validado.";
						}
					}
				}
				if(!existe){
					estatusDetalle = EstatusDetalle.ERROR.toString();
					descriEstatus = "El tipo poliza no pertenece al producto capturado.";
				}
			}else{
				estatusDetalle = EstatusDetalle.ERROR.toString();
				descriEstatus = "El negocio no existe y es requerido para la validacion de tipo de poliza.";
			}
		}else{
			estatusDetalle = EstatusDetalle.ERROR.toString();
			descriEstatus = "El negocio es requerido para la validacion de tipo de poliza.";
		}
		
		LOG.info("VAL-FIN TIPO POLIZA");
		return infoItem.new EstatusDescriDetalle(estatusDetalle, descriEstatus);
	}
	
	private int validaCorrespondenciaCobertura(boolean valObligatoriedad, String claveCobertura, Long idDetalleCargaMasiva, Long idNegocio, Long idPaquete, Long idTipoPoliza,int tipoValida, String claveUso, Double monto, Long idAgente, String codigoPostalCliente){
		int valRetorno = 0;
		CargaMasivaServicioPublicoDetalleTradSeyMid tradSeyMidasCMSP = (entidadService.findByProperty(CargaMasivaServicioPublicoDetalleTradSeyMid.class, "detalle.idDetalleCargaMasiva", idDetalleCargaMasiva)).get(0);
		if(idNegocio != null && idNegocio > 0){
			Negocio negocio = negocioService.findById(idNegocio);
			if(negocio != null){
				for(NegocioProducto negocioProducto : negocio.getNegocioProductos()){
					if(negocioProducto.getProductoDTO().getIdToProducto().toString().equals(tradSeyMidasCMSP.getProductoIdMidas().toString())){
						if(idTipoPoliza == null || idTipoPoliza == 0){
							idTipoPoliza = Long.parseLong(negocioProducto.getNegocioTipoPolizaList().get(0).getTipoPolizaDTO().getIdToTipoPoliza().toString());
						}
						if(idTipoPoliza != null && idTipoPoliza > 0){
							for(NegocioTipoPoliza negocioTipoPoliza : negocioProducto.getNegocioTipoPolizaList()){
								if(negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza().toString().equals(idTipoPoliza.toString())){
									for(NegocioSeccion negocioSeccion : negocioTipoPoliza.getNegocioSeccionList()){
										if(negocioSeccion.getSeccionDTO().getIdToSeccion().toString().equals(tradSeyMidasCMSP.getLineaNegIdMidas().toString())){
											if(idPaquete != null && idPaquete > 0 ){	
												for(NegocioPaqueteSeccion negocioPaqueteSeccion : negocioSeccion.getNegocioPaqueteSeccionList()){
													if(negocioPaqueteSeccion.getPaquete().getId().toString().equals(idPaquete.toString())){
														if(claveCobertura != null & claveCobertura != ""){
															for(NegocioCobPaqSeccion negocioCobPaqSeccion : negocioPaqueteSeccion.getNegocioCobPaqSeccionList()){
																if(negocioCobPaqSeccion.getCoberturaDTO().getClaveTipoCalculo().equals(claveCobertura)){
																	BigDecimal idToCobertura = negocioCobPaqSeccion.getCoberturaDTO().getIdToCobertura();
																	if(valObligatoriedad){
																		CobPaquetesSeccionId id = new CobPaquetesSeccionId(tradSeyMidasCMSP.getLineaNegIdMidas(), idPaquete, negocioCobPaqSeccion.getCoberturaDTO().getIdToCobertura().longValue());
																		CobPaquetesSeccion cobPaqueteSeccion = entidadService.findById(CobPaquetesSeccion.class, id);
																		if(cobPaqueteSeccion.getClaveObligatoriedad().toString().equals("0")){
																			valRetorno = 6;
																			break;
																		}else if(monto == null || monto <= 0){
																			valRetorno = 0;
																			break;
																		}
																	}

																	String estado = null;
																	String ciudad = null;
																	if(codigoPostalCliente != null && codigoPostalCliente != ""){
																		DecimalFormat decFormat = new DecimalFormat("00000");
																		String postalCode = decFormat.format(Integer.parseInt(codigoPostalCliente.trim()));
						
																		List<ColoniaMidas> listColonias = direccionMidasService.obtenerColoniasPorCP(postalCode);
																		if(!listColonias.isEmpty()){
																			estado = listColonias.get(0).getCiudad().getEstado().getId();
																			ciudad = listColonias.get(0).getCiudad().getId();
																		}
																	}
																	
																	MotorDecisionDTO filtroCoberturas = new MotorDecisionDTO();
																	
																	filtroCoberturas.setTipoConsulta(MotorDecisionDTO.TipoConsulta.TIPO_CONSULTA_SUMASASEGURADAS);
																	filtroCoberturas.setIdToNegocio(negocio.getIdToNegocio());
																	
																	List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosMotor = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
																	
																	//filtros requeridos para la busqueda
																	filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
																			Boolean.TRUE,"negocioPaqueteSeccion",negocioPaqueteSeccion));
																	filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
																			Boolean.TRUE,"coberturaDTO.idToCobertura",idToCobertura));
																	filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
																			Boolean.TRUE,"monedaDTO.idTcMoneda",CargaMasivaServicioPublicoJobImpl.ID_MONEDA_NACIONAL_PESOS));
																	
																	EstadoDTO estadoDto = null;
																	CiudadDTO ciudadDto = null;
																	TipoUsoVehiculoDTO tipoUsoVehiculoDto = new TipoUsoVehiculoDTO();
																	Agente agente = null;
																	
																	if(estado != null){
																		estadoDto = entidadService.findById(EstadoDTO.class, estado);
																	}
																	if(ciudad != null){
																		ciudadDto = entidadService.findById(CiudadDTO.class, ciudad);
																	}
																	
																	if(idAgente != null){
																		List<Agente> agenteList = entidadService.findByProperty(Agente.class, "idAgente", idAgente);
																		agente = agenteList.get(0);
																	}
																	
																	tipoUsoVehiculoDto.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
																	tipoUsoVehiculoDto.setCodigoTipoUsoVehiculo(claveUso);
																	tipoUsoVehiculoDto.setDescripcionTipoUsoVehiculo("");
																	
																	List <TipoUsoVehiculoDTO> listTipoUsoVehiculoDto = tipoUsoVehiculo.listarFiltrado(tipoUsoVehiculoDto);
																	
																	//filtros opcionales
																	filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
																			Boolean.FALSE,"estadoDTO",estadoDto));
																	filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
																			Boolean.FALSE,"ciudadDTO",ciudadDto));
																	filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
																			Boolean.FALSE,"tipoUsoVehiculo",(listTipoUsoVehiculoDto == null || listTipoUsoVehiculoDto.isEmpty()) ? null : listTipoUsoVehiculoDto.get(0)));
																	filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
																			Boolean.FALSE,"agente",agente));
																	filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
																			Boolean.FALSE,"renovacion",false));
																	
																	filtroCoberturas.setFiltrosDTO(filtrosMotor);
																	
																	/*
																	 * valResultado = 11 - No existen valores configurados para realizar las validaciones.
																	 * valResultado = 12 - El monto no coincide con los deducibles configurados para el negocio.
																	 * valResultado = 13 - No existen deducbles configurados para el negocio.
																	 * valResultado = 14 - NO ERROR - El valor a considerar es el cofigurado al valor comercial y no al capturado.
																	 * valResultado = 15 - NO ERROR - El valor a considerar es el configurado al valor convenido y no al capturado.
																	 * valResultado = 16 - El monto no coincide con las sumas aseguradas configuradas.
																	 * valResultado = 17 - El monto capturado esta fuera del rango configurado.
																	 * valResultado = 18 - El monto capturado debe ser mayor a cero para poder ser validado.
																	 * valResultado = 19 - La cobertura no esta configurada como amparada.
																	 * valResultado = 21 - No existen coberturas coincidentes con los filtros proporcionados.
																	 */
																	valRetorno = motorDecisionAtributosService.validaSumasDeducibles(filtroCoberturas, monto, tipoValida);
																	break;
																}else{
																	if(monto == null || monto <= 0){
																		valRetorno = 0;
																	}else{
																		valRetorno = 5;
																	}
																}
															}
															break;
														}else{
															valRetorno = 10;
														}
														break;
													}else{
														valRetorno = 4;
													}
												}
												break;
											}else{
												valRetorno = 9;
											}
											break;
										}else{
											valRetorno = 3;
										}
									}
									break;
								}else{
									valRetorno = 7;
								}
							}
							break;
						}else{
							valRetorno = 8;
						}
						break;
					}else{
						valRetorno = 2;
					}
				}
			}else{
				valRetorno = 1;
			}
		}else{
			valRetorno = 20;
		}
		return valRetorno;
	}
}