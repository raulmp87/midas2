package mx.com.afirme.midas2.service.impl.siniestro;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.interfaz.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.siniestro.Cabina;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil.Estatus;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas.TipoLugar;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestro.ReporteSiniestroMovilService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.PagingList;
import mx.com.multiasistencia.servicing.qbit.remoting.rest.api.qbit.EnvioAutoAlertaRestInDto;
import mx.com.multiasistencia.servicing.qbit.remoting.rest.api.qbit.EnvioAutoAlertaRestOutDto;
import mx.com.multiasistencia.servicing.qbit.remoting.rest.api.qbit.QbitServicingSoap;
import mx.com.multiasistencia.servicing.qbit.remoting.rest.impl.qbit.QbitServicingSoapImpl;

import org.apache.commons.lang.StringUtils;
import org.joda.time.Interval;

@Stateless
public class ReporteSiniestroMovilServiceImpl implements ReporteSiniestroMovilService {

	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private PolizaFacadeRemote polizaInterfazFacade;
	
	@EJB
	private mx.com.afirme.midas.poliza.PolizaFacadeRemote polizaFacade;
	
	
	@EJB
	private ParametroGeneralService parametroGeneralService;
	private List<IncisoSiniestroDTO> incisos;
	
	@EJB
	private ReporteCabinaService reporteCabinaService;
	
	private IncisoSiniestroDTO filtroBusqueda;
	
	@EJB
	private PolizaSiniestroService polizaSiniestroService;
	
	private LugarSiniestroMidas lugarAtencionOcurrido;

	
	/**
	 * Por medio de reglas muy basicas esta clase trata de obtener el nombre, apellido paterno y apellido materno a partir de un nombre completo.
	 * No siempre es posible obtener todos los valores, esto consistira en el valor que se proporcione. No considera casos especiales como apellidos compuestos
	 * con mas de una palabra como "Cabeza de Vaca".
	 * @author amosomar
	 */
	private class NombreCompleto {
		
		private String value;		
		private String nombre;
		private String apellidoPaterno;
		private String apellidoMaterno;
		
		public NombreCompleto(String value) {
			this.value = value;
			
			String[] tokens = value.split(" ");
			if (tokens.length == 1) {
				nombre = tokens[0];
			} else if (tokens.length == 2) {
				nombre = tokens[0];
				apellidoPaterno = tokens[1];				
			} else if (tokens.length == 3) {
				nombre = tokens[0];
				apellidoPaterno = tokens[1];
				apellidoMaterno = tokens[2];
			} else {
				String nombres = "";
				for (int i = 0; i < tokens.length - 2; i++) {
					nombres += tokens[i] + " ";
				}
				nombre = nombres.substring(0, nombres.length() - 1); //quita el ultimo espacio
				apellidoPaterno = tokens[tokens.length - 2];
				apellidoMaterno = tokens[tokens.length - 1];				
			}
		}
		
		@SuppressWarnings("unused")
		public String getValue() {
			return value;
		}
		
		public String getNombre() {
			return nombre;
		}
		
		public String getApellidoPaterno() {
			return apellidoPaterno;
		}
		
		public String getApellidoMaterno() {
			return apellidoMaterno;
		}
	}
	
	
	@Override
	public ReporteSiniestroMovil reportarSiniestro(
			ReporteSiniestroMovil reporteSiniestroMovil) {
		reporteSiniestroMovil.setCabina(getCabinaCorrespondiente(reporteSiniestroMovil));
		
		if (reporteSiniestroMovil.getCabina().getNombre().equals(Cabina.NOMBRE_MULTIASISTENCIA)) {
			notificarCabinaMultiasistencia(reporteSiniestroMovil);
		}
		return guardar(reporteSiniestroMovil);
	}	

	private void notificarCabinaMultiasistencia(
			ReporteSiniestroMovil reporteSiniestroMovil) {	

		EnvioAutoAlertaRestInDto envioAutoAlertaRestInDto = new EnvioAutoAlertaRestInDto();
		envioAutoAlertaRestInDto.setEmpresa("SEGUROS AFIRME");
		envioAutoAlertaRestInDto.setArea("SINIESTROS");
		envioAutoAlertaRestInDto.setTelefono(reporteSiniestroMovil.getNumeroCelular());
		
		BigDecimal idCotizacion = polizaFacade.obtenerNumeroCotizacionSeycos(reporteSiniestroMovil.getPolizaDTO().getIdToPoliza());
		String polizaFormateadaSeycos = polizaInterfazFacade.obtenerFolioPolizaSeycos(idCotizacion);		
		String poliza = polizaFormateadaSeycos
				+ StringUtils.leftPad(polizaInterfazFacade.getConvertion(
						PolizaFacadeRemote.CONV_TYPE_LINE,
						getSeccionDTO(reporteSiniestroMovil).getIdToSeccion()
								.toString()).toBigInteger().toString(), 4, '0');
		envioAutoAlertaRestInDto.setPoliza(poliza.replace("-", ""));
		envioAutoAlertaRestInDto.setInciso(reporteSiniestroMovil.getNumeroInciso().intValue());
		NombreCompleto nombreCompleto = new NombreCompleto(reporteSiniestroMovil.getNombrePersonaReporta());
		envioAutoAlertaRestInDto.setNombre(nombreCompleto.getNombre());
		envioAutoAlertaRestInDto.setPaterno(nombreCompleto.getApellidoPaterno());
		envioAutoAlertaRestInDto.setMaterno(nombreCompleto.getApellidoMaterno());
		if (reporteSiniestroMovil.getCoordenadas() != null) {
			String urlGps = String.format("http://maps.google.com/?q=%s,%s", 
					reporteSiniestroMovil.getCoordenadas().getLatitud(), reporteSiniestroMovil.getCoordenadas().getLongitud());
			envioAutoAlertaRestInDto.setUrlUbicacion(urlGps);
		}
		
		QbitServicingSoapImpl qbitServicingSoapImpl = new QbitServicingSoapImpl(sistemaContext.getMultiasistenciaSendAlertPortWsdlUrl());
		QbitServicingSoap qbitServicingSoap = qbitServicingSoapImpl.getQbitServicingSoapImplPort();
		EnvioAutoAlertaRestOutDto envioAutoAlertaRestOutDto = qbitServicingSoap.envioAutoAlerta(envioAutoAlertaRestInDto);		
		reporteSiniestroMovil.setNumeroReporteSiniestro(envioAutoAlertaRestOutDto.getNumSolicitud().toString());
		//actualizar estatus: se considera terminada puesto que se logro notificar exitosamente.
		reporteSiniestroMovil.setEstatus(Estatus.TERMINADA);
	}

	/**
	 * Obtiene le cabina que le corresponde a un reporte de siniestro.
	 * @param reporteSiniestroMovil
	 * @return
	 */
	private Cabina getCabinaCorrespondiente(ReporteSiniestroMovil reporteSiniestroMovil) {
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId(BigDecimal.valueOf(13), BigDecimal.valueOf(131010));
		final ParametroGeneralDTO parametro = parametroGeneralService.findById(parametroGeneralId);
		parametroGeneralId.setCodigoParametroGeneral(BigDecimal.valueOf(131020));
		final ParametroGeneralDTO parametroSPV = parametroGeneralService.findById(parametroGeneralId);
		Negocio negocio = getNegocio( reporteSiniestroMovil );
		
		final Cabina result;
		ProductoDTO productoDTO = reporteSiniestroMovil.getPolizaDTO().getCotizacionDTO().getSolicitudDTO().getProductoDTO();
		// ¿Todos los reportes a cabina monterrey o el producto es de Daños?
		if (parametro.getValor().equals("1") || productoDTO.getClaveNegocio().equals(NegocioSeguros.CLAVE_DANOS)) {
			result = findCabina(Cabina.NOMBRE_MONTERREY);
		} else {
			// ¿El negocio es Autos y es servicio público?
			if(productoDTO.getClaveNegocio().equals(NegocioSeguros.CLAVE_AUTOS)
					&& getSeccionDTO(reporteSiniestroMovil)
						.isServicioPublico() || negocio.getAplicaEnlace() ) {
				// ¿Está activa la cabina SPV?
				if (parametroSPV.getValor().equals("1")) { 
					result = findCabina(Cabina.NOMBRE_SPV);
				} else {
					result = findCabina(Cabina.NOMBRE_MONTERREY);
				}
			} else {
				// ¿El telfono tiene lada soportada por cabina monterrey?
				if (isTelefonoSoportado(reporteSiniestroMovil.getNumeroCelular())) {
					result = findCabina(Cabina.NOMBRE_MONTERREY);
				} else {
					result =  findCabina(Cabina.NOMBRE_MULTIASISTENCIA);
				}
			}
		}
		return result;		
	}
	
	@Override
	public boolean existeInciso(PolizaDTO poliza, Integer numeroInciso) {
		if (poliza == null) {
			throw new IllegalArgumentException("poliza requerido.");
		}
		//Checar si en alguno de los endosos existe el inciso.
		String countIncisosQuery = "select count(*) from MIDAS.tocotizacion \n" + 
				"    inner join (select distinct idtocotizacion from MIDAS.toendoso where idtopoliza = ?) e on e.idtocotizacion = tocotizacion.idtocotizacion\n" + 
				"    inner join MIDAS.toincisocot on toincisocot.idtocotizacion = tocotizacion.idtocotizacion\n" + 
				"    where numeroinciso = ?";
		Query query = entityManager.createNativeQuery(countIncisosQuery);
		query.setParameter(1, poliza.getIdToPoliza());
		query.setParameter(2, numeroInciso);
		int total = ((BigDecimal) query.getSingleResult()).intValue();
		
		if (total > 0) {
			return true;
		} 
		
		//Buscar en estructura bitemporal.
		if (getIncisoContinuity(poliza.getCotizacionDTO().getIdToCotizacion().intValue(), numeroInciso) != null) {
			return true;
		}

		return false;
	}
	
	/**
	 * Obtiene la seccion del reporte de siniestro movil de las entidades bitemporales.
	 * @param reporteSiniestroMovil
	 * @return
	 */
	private SeccionDTO getSeccionDTO(ReporteSiniestroMovil reporteSiniestroMovil) {
		Integer numeroCotizacion = reporteSiniestroMovil.getPolizaDTO().getCotizacionDTO().getIdToCotizacion().intValue(); 
		Integer numeroInciso = reporteSiniestroMovil.getNumeroInciso().intValue();
		IncisoContinuity incisoContinuity = getIncisoContinuity(numeroCotizacion, numeroInciso);
		Collection<SeccionIncisoContinuity> seccionIncisoContinuities = incisoContinuity.getIncisoSeccionContinuities().getValue();
		SeccionIncisoContinuity seccionIncisoContinuity = seccionIncisoContinuities.iterator().next();
		return seccionIncisoContinuity.getSeccion();
	}
	
	private IncisoContinuity getIncisoContinuity(Integer numeroCotizacion, Integer numeroInciso) {
		String jpql = "select m from IncisoContinuity m where m.numero = :numeroInciso and m.cotizacionContinuity.numero = :numeroCotizacion";
		TypedQuery<IncisoContinuity> query = entityManager.createQuery(jpql, IncisoContinuity.class);
		query.setParameter("numeroCotizacion", numeroCotizacion);
		query.setParameter("numeroInciso", numeroInciso);
		List<IncisoContinuity> list = query.getResultList();
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}
	
	private Cabina findCabina(String nombre) {
		String jpql = "select m from Cabina m where m.nombre = :nombre";
		TypedQuery<Cabina> query = entityManager.createQuery(jpql, Cabina.class);
		query.setParameter("nombre", nombre);
		List<Cabina> list = query.getResultList();
		if (list.size() == 0) {
			return null;
		} else if (list.size() == 1) {
			return list.get(0);
		} else {
			throw new RuntimeException("Mas de un resultado.");
		}
	}
	
	@Override
	public ReporteSiniestroMovil findById(Long id) {
		ReporteSiniestroMovil reporteSiniestroMovil = entityManager.find(ReporteSiniestroMovil.class, id);
		if (reporteSiniestroMovil != null && isRestringirCabina() && 
				!reporteSiniestroMovil.getCabina().getId().equals(usuarioService.getUsuarioActual().getCabina().getId())) {
			//Regresa nulo porque no tiene permiso para acceder a reportes de siniestros que no sean de la misma cabina.
			//Probablemente sería mejor manejar una excepcion de seguridad.
			return null;
		}
		return reporteSiniestroMovil;
	}
	
	@Override
	public ReporteSiniestroMovil findByIdWithCabin(Long id) {
		ReporteSiniestroMovil reporteSiniestroMovil = entityManager.find(ReporteSiniestroMovil.class, id);
		reporteSiniestroMovil.getReporteCabina();
		return reporteSiniestroMovil;
	}


	@Override
	public List<ReporteSiniestroMovil> find() {
		String jpql = "select m from ReporteSiniestroMovil m where 1 = 1";
		
		boolean restringirCabina = isRestringirCabina();
		if (restringirCabina) {
			jpql += " and m.cabina = :cabina";
		}
		
		TypedQuery<ReporteSiniestroMovil> query = entityManager.createQuery(jpql, ReporteSiniestroMovil.class);
		if (restringirCabina) {
			query.setParameter("cabina", usuarioService.getUsuarioActual().getCabina());
		}
		
		return query.getResultList();
	}
	
	public boolean isRestringirCabina() {
		return !usuarioService.tieneRolUsuarioActual("Rol_M2_Administrador");
	}
	
	/**
	 * Indica si  <code>telefono</code> pertenece a un telefono soportado por la cabina de Afirme.
	 * @param telefono
	 * @return
	 */
	private boolean isTelefonoSoportado(String telefono) {
		final ParametroGeneralDTO parametro = parametroGeneralService
				.findById(new ParametroGeneralId(new BigDecimal(13), new BigDecimal(131030)));
		final String[] codigosLadaSoportados = parametro.getValor().split(",");
		for (String codigoLadaSoportado : codigosLadaSoportados) {
			if (telefono.startsWith(codigoLadaSoportado)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public ReporteSiniestroMovil guardar(
			ReporteSiniestroMovil reporteSiniestroMovil) {
		if (reporteSiniestroMovil.getId() == null) {
			entityManager.persist(reporteSiniestroMovil);
		} else {
			reporteSiniestroMovil = entityManager.merge(reporteSiniestroMovil);
		}
		return reporteSiniestroMovil;
	}
	
	public List<ReporteSiniestroMovil> findSinAsignar() {
		String jpql = "select m from ReporteSiniestroMovil m where m.estatusId = :estatusId";
		
		boolean restringirCabina = isRestringirCabina();
		if (restringirCabina) {
			jpql += " and m.cabina = :cabina";
		}
		jpql += " order by m.fechaCreacion";		
		TypedQuery<ReporteSiniestroMovil> query = entityManager.createQuery(jpql, ReporteSiniestroMovil.class);
		query.setParameter("estatusId", Estatus.SIN_ASIGNAR.getId());
		if (restringirCabina) {
			query.setParameter("cabina", usuarioService.getUsuarioActual().getCabina());
		}
		return query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	public PagingList<ReporteSiniestroMovil> findByExamplePage(
			ReporteSiniestroMovil reporteSiniestroMovil, int pageSize, int page) {
		String jpql = "select m from ReporteSiniestroMovil m";
		
		
		Map<String, Object> andConditions = new HashMap<String, Object>();
		
		if (reporteSiniestroMovil.getEstatus() != null) {
			andConditions.put("m.estatusId",reporteSiniestroMovil.getEstatus().getId());
		}
		
		if (!StringUtils.isBlank(reporteSiniestroMovil.getNombreUsuarioAsignado())) {
			andConditions.put("m.nombreUsuarioAsignado", reporteSiniestroMovil.getNombreUsuarioAsignado());
		}
		
		
		String orderBy = "";
		if (reporteSiniestroMovil.getEstatus() != null && (reporteSiniestroMovil.getEstatus().equals(Estatus.SIN_ASIGNAR) || reporteSiniestroMovil.getEstatus().equals(Estatus.ASIGNADA))) {
			orderBy = "m.fechaCreacion ASC";
		} else {
			orderBy = "m.fechaCreacion DESC";			
		}
		
		if (isRestringirCabina()) {
			andConditions.put("m.cabina",  usuarioService.getUsuarioActual().getCabina());
		}
		
		TypedQuery<ReporteSiniestroMovil> query = createQuery(jpql, ReporteSiniestroMovil.class, andConditions, orderBy);		
		setQueryParameters(query, andConditions);
						
		PagingList<ReporteSiniestroMovil> pagingList = new PagingList<ReporteSiniestroMovil>(query, pageSize);
		pagingList.getPage(page);
		return pagingList;
	}
	
	
	private <T> TypedQuery<T> createQuery(String jpql, Class<T> clazz, Map<String, Object> andConditions, String orderBy) {
		List<String> andConditionsList = new ArrayList<String>();
		for (Map.Entry<String, Object> entry : andConditions.entrySet()) {
			andConditionsList.add(entry.getKey() + " = " + ":" + entry.getKey().replace(".", ""));
		}
				
		if (!andConditionsList.isEmpty()) {
			
			String andClauses = StringUtils.join(andConditionsList.iterator(), " AND ");
			
			if (!jpql.contains("where")) {
				jpql += " where ";
			}
			
			jpql += andClauses;
						
		}
		
		if (!StringUtils.isBlank(orderBy)) {
			jpql += " ORDER BY " + orderBy;	
		}
		
		return entityManager.createQuery(jpql, clazz);
	}
	
	
	private <T> void setQueryParameters(TypedQuery<T> query, Map<String, Object> ... parametersMaps) {
		for (Map<String, Object> parametersMap : parametersMaps) {
			for (Map.Entry<String, Object> entry : parametersMap.entrySet()) {
				query.setParameter(entry.getKey().replace(".", ""), entry.getValue());
			}
		}
	}
	
	@Override
	public ReporteSiniestroMovil findReporteReciente(String numeroCelular) {
		//Especifica el maximo numero de milisegundos para considerar un Reporte existente como reciente desde el momento de su creacion.
		final long maxMilliseconds = 5 * 60 * 1000;
		String jpql = "select m from ReporteSiniestroMovil m where m.numeroCelular = :numeroCelular";
		TypedQuery<ReporteSiniestroMovil> query = entityManager.createQuery(jpql, ReporteSiniestroMovil.class);
		query.setParameter("numeroCelular", numeroCelular);
		List<ReporteSiniestroMovil> list = query.getResultList();
		for (ReporteSiniestroMovil reporteSiniestroMovil : list) {
			Interval interval = new Interval(reporteSiniestroMovil.getFechaCreacion().getTime(), reporteSiniestroMovil.getFechaCreacion().getTime() + maxMilliseconds);
			if (interval.containsNow()) {
				return reporteSiniestroMovil;
			}
		}		
		return null;
	}	

	private Negocio getNegocio( ReporteSiniestroMovil reporteSiniestroMovil ){
		Negocio negocio = new Negocio();
		PolizaDTO poliza = reporteSiniestroMovil.getPolizaDTO();
		CotizacionDTO cot = poliza.getCotizacionDTO()!=null?poliza.getCotizacionDTO():new CotizacionDTO();
		SolicitudDTO solicitud = cot.getSolicitudDTO()!=null?cot.getSolicitudDTO():new SolicitudDTO();
		negocio = solicitud.getNegocio();
		return negocio;
	}
	
	@Override
	public Long crearReporteCabina(ReporteSiniestroMovil reporteSiniestroMovil) {
		DateFormat fechaOcurrido = new SimpleDateFormat("dd/MM/yy");
		final ReporteCabina reporteCabina = new ReporteCabina();
		reporteSiniestroMovil.setNombreUsuarioAsignado("M2ADMINI");
		reporteCabina.setCodigoUsuarioCreacion(reporteSiniestroMovil.getNombreUsuarioAsignado());
		reporteCabina.setOficina(reporteSiniestroMovil.getOficina());
		reporteCabina.setPersonaReporta(reporteSiniestroMovil.getNombrePersonaReporta());
		final String splittedPhone[] = splitPhoneNumber(reporteSiniestroMovil.getNumeroCelular());
		reporteCabina.setLadaTelefono(splittedPhone[0]);
		reporteCabina.setTelefono(splittedPhone[1]);
		filtroBusqueda = new IncisoSiniestroDTO();
		String polizaFormateada = reporteSiniestroMovil.getPolizaDTO().getNumeroPolizaFormateada();
		filtroBusqueda.setAutoInciso(new AutoInciso());
		filtroBusqueda.getAutoInciso().setNumeroSerie(filtroBusqueda.getNumeroSerie()!=null ? filtroBusqueda.getNumeroSerie():"");
		filtroBusqueda.setNumeroPoliza(polizaFormateada);
		incisos = polizaSiniestroService.buscarPolizaFiltro(filtroBusqueda, reporteSiniestroMovil.getFechaCreacion(), reporteSiniestroMovil.getFechaCreacion());
		IncisoSiniestroDTO inc = new IncisoSiniestroDTO();
		try {
			reporteCabina.setFechaOcurrido(fechaOcurrido.parse(fechaOcurrido.format(reporteSiniestroMovil.getFechaCreacion())));
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		for (IncisoSiniestroDTO i : incisos){
			if (i.getNumeroInciso().toString().equals(reporteSiniestroMovil.getNumeroInciso().toString())){
					inc = i;
					break;
				}
		}
		reporteCabina.setHoraOcurrido(new SimpleDateFormat("HH:mm").format(reporteSiniestroMovil.getFechaCreacion()));
		reporteCabinaService.salvarReporte(reporteCabina);
		
		reporteSiniestroMovil.setReporteCabina(reporteCabina);
		reporteSiniestroMovil.setNumeroReporteSiniestro(reporteCabina.getNumeroReporte());
		try {
			polizaSiniestroService.asignarInciso(reporteCabina.getId(), reporteSiniestroMovil.getPolizaDTO().getIdToPoliza().longValue(), inc.getIncisoContinuityId(), 
					new Date(inc.getValidOnMillis()), new Date(inc.getRecordFromMillis()), reporteSiniestroMovil.getFechaCreacion());
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		if(reporteSiniestroMovil.getCoordenadas() != null ){
			lugarAtencionOcurrido = new LugarSiniestroMidas();
			lugarAtencionOcurrido.setCoordenadas(reporteSiniestroMovil.getCoordenadas());
			reporteCabinaService.guardarLugarSiniestro(
				reporteCabina.getId(), reporteCabina.getPersonaReporta(), reporteCabina.getLadaTelefono(), reporteCabina.getTelefono(), lugarAtencionOcurrido, TipoLugar.AT );
		}
		
		guardar(reporteSiniestroMovil);
		return reporteCabina.getId();
	}
	
	private final int phoneLength = 8;
	
	public String[] splitPhoneNumber(String thePhone) {

		if (thePhone == null) {
			return new String[] { "0", "0" };
		}
		thePhone = thePhone.replaceAll("\\s", "");
		if (thePhone.length() > 11) {
			return new String[] { "0", thePhone.substring(thePhone.length() - phoneLength) };
		} else if (thePhone.length() <= phoneLength) {
			return new String[] { "0", thePhone };
		} else if (thePhone.length() <= 11) {
			return new String[] { thePhone.substring(0, thePhone.length() - phoneLength),
					thePhone.substring(thePhone.length() - phoneLength) };
		}

		return new String[] { "0", "0" };
	}	
}
