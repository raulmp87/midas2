package mx.com.afirme.midas2.service.impl.poliza;

import java.math.BigDecimal;
import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.BitacoraGuia;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.DetalleBitacora;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.custShipmentTracking.TrackingService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.poliza.PolizaService;
import mx.com.afirme.midas2.utils.Convertidor;
import mx.com.afirme.midas2.service.ListadoService;

import org.apache.commons.lang.StringUtils;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

/**
 * 
 * @author jose luis arellano
 */
@Stateless
public class PolizaServiceImpl implements PolizaService {
	
	private Convertidor convertidor;
	
	private PolizaFacadeRemote polizaFacade;
	private ClienteFacadeRemote clienteFacadeRemote;
	private EndosoFacadeRemote endosoFacadeRemote;
	private TrackingService trackingService;
	
	private EntidadService entidadService;
	private ListadoService listadoService;
	
	private EntidadContinuityService entidadContinuityService;
	
	private EntidadBitemporalService entidadBitemporalService;

	@EJB
	public void setEntidadBitemporalService(EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}	
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setEntidadContinuityService(EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@EJB
	public void setTrackingService(TrackingService trackingService) {
		this.trackingService = trackingService;
	}
	
	@Override
	public RegistroDinamicoDTO verDetalle(String id, String accion,Class<?> vista) {
		RegistroDinamicoDTO registro=null;
		if(TipoAccionDTO.getFiltrar().equals(accion)){
			PolizaDTO polizaDTO = new PolizaDTO();
			registro = convertidor.getRegistroDinamico(polizaDTO,vista);
			registro.setJavascriptOnLoad("inicializarComponentesfiltroPoliza()");
		}
		
		else if(TipoAccionDTO.getVer().equals(accion)){
			PolizaDTO polizaDTO = new PolizaDTO();
			registro = convertidor.getRegistroDinamico(polizaDTO);
		}
		else{
			throw new RuntimeException("la accion no aplica para esta entidad.");
		}
		return registro;
	}

	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicos() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicos(Object filtro) {
		PolizaDTO polizaFiltro = (PolizaDTO)filtro;

		List<PolizaDTO> listaPolizas = null;
		//Para búsquedas de pólizas de autos ó de danios, se utiliza este servicio, 
		//si no se reciben parámetros de bśuqeda, se regresa la lista vacía
		if(polizaFiltro == null || 
				(polizaFiltro.getFechaCreacion() == null && 
						StringUtil.isEmpty(polizaFiltro.getFolioPoliza()) &&
						StringUtil.isEmpty(polizaFiltro.getCodigoAgente()) &&
						StringUtil.isEmpty(polizaFiltro.getNombreAsegurado()) &&
						StringUtil.isEmpty(polizaFiltro.getNumeroSerie())
						) ){
			listaPolizas = new ArrayList<PolizaDTO>(1);
		}
		else{
			//En este servicio falta incluir el filtro por número de serie
			listaPolizas = polizaFacade.buscarFiltrado(polizaFiltro);
		}

		List<RegistroDinamicoDTO> listaResultado = new ArrayList<RegistroDinamicoDTO>(1);
		for(PolizaDTO poliza : listaPolizas){
			listaResultado.add(convertidor.getRegistroDinamico(poliza));
		}
		return listaResultado;
	}


	@Override
	public List<PolizaDTO> buscarPoliza(PolizaDTO filtro, Date fechaIncial,
			Date fechaFinal) {
		
		if (filtro == null) return null;
		
		List<PolizaDTO> polizas = new ArrayList<PolizaDTO>(1);
		
		if(filtro.getCotizacionDTO() == null)
			filtro.setCotizacionDTO(new CotizacionDTO());
		if(filtro.getCotizacionDTO().getSolicitudDTO()== null)
			filtro.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		if(filtro.getCotizacionDTO().getSolicitudDTO().getNegocio()==null)
			filtro.getCotizacionDTO().getSolicitudDTO().setNegocio(new Negocio());
		
		filtro.getCotizacionDTO().getSolicitudDTO().getNegocio().setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);		
		
		polizas.addAll(convertResultsToPolizaDTOList(polizaFacade.buscarFiltrado(filtro, fechaIncial, fechaFinal)));
		
		return polizas; 
		
	}
	
	@Override
	public Long obtenerTotalPaginacion(PolizaDTO filtro, Date fechaIncial,
			Date fechaFinal) {
		
		if (filtro == null) return 0L;
		
		if(filtro.getCotizacionDTO() == null)
			filtro.setCotizacionDTO(new CotizacionDTO());
		if(filtro.getCotizacionDTO().getSolicitudDTO()== null)
			filtro.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		if(filtro.getCotizacionDTO().getSolicitudDTO().getNegocio()==null)
			filtro.getCotizacionDTO().getSolicitudDTO().setNegocio(new Negocio());
		
		filtro.getCotizacionDTO().getSolicitudDTO().getNegocio().setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);		
		
		return polizaFacade.obtenerTotalPaginacion(filtro, fechaIncial, fechaFinal); 
		
	}
	
	public void setNombreAsegurado(List<PolizaDTO> polizas){
		ClienteDTO asegurado = null;
		CotizacionDTO cotizacion = null;
		String cliente = "No disponible";
		
		for(PolizaDTO polizaDTO:polizas){
			cotizacion = polizaDTO.getCotizacionDTO();
			if (cotizacion.getIdToPersonaAsegurado() != null){
				
				try{
					if(cotizacion.getIdDomicilioAsegurado() != null && cotizacion.getIdDomicilioAsegurado().intValue() > 0){
						asegurado = new ClienteDTO();
						asegurado.setIdCliente(cotizacion.getIdToPersonaAsegurado());
						asegurado.setIdDomicilio(cotizacion.getIdDomicilioAsegurado());  
						asegurado = clienteFacadeRemote.buscarPorCURP(asegurado, polizaDTO.getCodigoUsuarioCreacion()).get(0);
					}else{
						asegurado = clienteFacadeRemote.findById(cotizacion.getIdToPersonaAsegurado(), cotizacion.getCodigoUsuarioCotizacion());
					}	
					if(asegurado != null){
						cliente = asegurado.obtenerNombreCliente();
					}					
				}catch(Exception exc){}
				if (asegurado != null){
					    
					if (asegurado.getClaveTipoPersona() == 1)
						cliente = asegurado.getNombre() + " " + asegurado.getApellidoPaterno() + " " + asegurado.getApellidoMaterno();
				      	
					else
						cliente = asegurado.getNombre();
				}
			}
			if (cotizacion.getIdToPersonaContratante() != null){
				cliente = cotizacion.getNombreContratante();
				
			}
			
			 polizaDTO.setNombreAsegurado(cliente);
		  }
		}
	
	@Override
	public ResumenCostosDTO obtenerResumenCostos(PolizaDTO poliza) {
		ResumenCostosDTO resumenCostosDTO = new ResumenCostosDTO();
		if (poliza != null) {
			PolizaDTO polizaDTO = entidadService.findById(PolizaDTO.class, poliza.getIdToPoliza());
			entidadService.refresh(polizaDTO);
			EndosoDTO endoso = endosoFacadeRemote.getEndosoByPolizaCotizacion(polizaDTO.getIdToPoliza(), polizaDTO.getCotizacionDTO().getIdToCotizacion());

				//resumenCostosDTO.setPrimaNetaCoberturas(resumenCostosDTO.getPrimaNetaCoberturas() + endoso.getPrimaNetaCoberturas());
				//resumenCostosDTO.setTotalPrimas(resumenCostosDTO.getTotalPrimas() + endoso.get);
				//resumenCostosDTO.setDescuentoComisionCedida(resumenCostosDTO.getDescuentoComisionCedida() + endoso.getDescuentoComisionCedida());
				resumenCostosDTO.setRecargo(resumenCostosDTO.getRecargo() + endoso.getValorRecargoPagoFrac());
				resumenCostosDTO.setDerechos(resumenCostosDTO.getDerechos() + endoso.getValorDerechos());
				resumenCostosDTO.setIva(resumenCostosDTO.getIva() + endoso.getValorIVA());
				resumenCostosDTO.setPrimaTotal(resumenCostosDTO.getPrimaTotal() + endoso.getValorPrimaTotal());
		
		} 
		return resumenCostosDTO;
	}
	
	@Override
	public List<PolizaDTO> buscarPolizaRenovacion(PolizaDTO filtro, Date fechaIncial, Date fechaFinal, Boolean isCount, Boolean isFixEndoso,
			Boolean exclusionRenovacion) {
		List<PolizaDTO> polizas = new ArrayList<PolizaDTO>(1);
		if(filtro.getCotizacionDTO() == null)
			filtro.setCotizacionDTO(new CotizacionDTO());
		if(filtro.getCotizacionDTO().getSolicitudDTO()== null)
			filtro.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		if(filtro.getCotizacionDTO().getSolicitudDTO().getNegocio()==null)
			filtro.getCotizacionDTO().getSolicitudDTO().setNegocio(new Negocio());
		
		filtro.getCotizacionDTO().getSolicitudDTO().getNegocio().setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);		
		
		polizas = polizaFacade.buscarFiltradoRenovacion(filtro, fechaIncial, fechaFinal, isCount, isFixEndoso, exclusionRenovacion);
		
		this.setNombreAsegurado(polizas);
		
		return polizas; 
		
	}
	
	@Override
	public Long obtenerTotalPolizaRenovacion(PolizaDTO filtro, Date fechaIncial, Date fechaFinal, Boolean isCount,
			Boolean exclusionRenovacion) {
		if(filtro.getCotizacionDTO() == null)
			filtro.setCotizacionDTO(new CotizacionDTO());
		if(filtro.getCotizacionDTO().getSolicitudDTO()== null)
			filtro.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
		if(filtro.getCotizacionDTO().getSolicitudDTO().getNegocio()==null)
			filtro.getCotizacionDTO().getSolicitudDTO().setNegocio(new Negocio());
		
		filtro.getCotizacionDTO().getSolicitudDTO().getNegocio().setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);		
		
		return polizaFacade.obtieneTotalFiltradoRenovacion(filtro, fechaIncial, fechaFinal, isCount, exclusionRenovacion);
		
	}
	
	@Override
	public PolizaDTO find(String codigoProducto, String codigoTipoPoliza,
			Integer numeroPoliza, Integer numeroRenovacion){
		return polizaFacade.find(codigoProducto, codigoTipoPoliza, numeroPoliza,  numeroRenovacion);
	}
	
	
	private List<PolizaDTO> convertResultsToPolizaDTOList(List<Object[]> results) {
	
		Object[] resultado = null;		
		CotizacionDTO cotizacion = null;
		Integer numeroIncisos = null;
		String cliente = "No disponible";
		List<PolizaDTO> polizas = new ArrayList<PolizaDTO>(1);

		
		if (results != null) {
			Iterator<Object[]> itResult = results.iterator();
			while (itResult.hasNext()) {
				resultado = itResult.next();
				
				PolizaDTO poliza = new PolizaDTO();
				
				poliza.setIdToPoliza(Utilerias.obtenerBigDecimal(resultado[0]));
				poliza.setCodigoProducto((String) resultado[1]);
				poliza.setCodigoTipoPoliza((String) resultado[2]);
				poliza.setNumeroPoliza(Utilerias.obtenerInteger(resultado[3]));
				poliza.setNumeroRenovacion(Utilerias.obtenerInteger(resultado[4]));
				poliza.setClavePolizaSeycos((String) resultado[5]);
				
				CotizacionDTO cotizacionDTO =new CotizacionDTO();
				cotizacionDTO.setIdToCotizacion(Utilerias.obtenerBigDecimal(resultado[6]));
				
				poliza.setFechaCreacion(Utilerias.obtenerDate(resultado[7]));
				poliza.setClaveEstatus(Utilerias.obtenerBigDecimal(resultado[8]).shortValue());
				
				TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
				tipoPolizaDTO.setClaveAplicaFlotillas(Utilerias.obtenerInteger(resultado[9]));
				
				cotizacionDTO.setTipoPolizaDTO(tipoPolizaDTO);				
				poliza.setCotizacionDTO(cotizacionDTO);
				
				numeroIncisos = Utilerias.obtenerInteger(resultado[10]);
				//Setea numero Incisos
				poliza.setNumeroIncisos(numeroIncisos);
				//Setea nombre asegurado
				cotizacion = poliza.getCotizacionDTO();
				cotizacion = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
				poliza.setCotizacionDTO(cotizacion);
				BitemporalCotizacion bitempCotizacion = null; 
				try{
				CotizacionContinuity cotizacionContinuity2 = entidadContinuityService.findContinuityByBusinessKey(
					CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, cotizacion.getIdToCotizacion());
				
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(cotizacion.getFechaFinVigencia());
				gcFechaFin.add(GregorianCalendar.DAY_OF_YEAR, -1);
				gcFechaFin.set(GregorianCalendar.HOUR_OF_DAY, 23);
				gcFechaFin.set(GregorianCalendar.MINUTE, 59);
				gcFechaFin.set(GregorianCalendar.SECOND, 59);
				gcFechaFin.set(GregorianCalendar.MILLISECOND, 59);
				
				//bitempCotizacion = cotizacionContinuity2.getCotizaciones().get(TimeUtils.getDateTime(gcFechaFin.getTime()));
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("continuity.id", cotizacionContinuity2.getId());
				List<BitemporalCotizacion> bitempCotizacionList =  (List<BitemporalCotizacion>) entidadBitemporalService.listarFiltrado(
						BitemporalCotizacion.class, params, TimeUtils.getDateTime(gcFechaFin.getTime()),  TimeUtils.current(), false);
				if(bitempCotizacionList != null && !bitempCotizacionList.isEmpty()){
					bitempCotizacion = bitempCotizacionList.get(0);
				}
				
				if (bitempCotizacion == null && poliza.getClaveEstatus() == 0) {
					Collection<BitemporalCotizacion> collectionBitemporalCotizacion = entidadBitemporalService.obtenerCancelados(
							BitemporalCotizacion.class, cotizacionContinuity2.getId(),TimeUtils.getDateTime(gcFechaFin.getTime()), null);
					if (collectionBitemporalCotizacion.size() > 0) {
						bitempCotizacion = collectionBitemporalCotizacion.iterator().next();
					}
				}				
				}catch(Exception e){
				}

				if (bitempCotizacion != null && bitempCotizacion.getValue().getPersonaContratanteId() != null){
					cliente = bitempCotizacion.getValue().getNombreContratante();
				}else if (cotizacion.getIdToPersonaContratante() != null){
					cliente = cotizacion.getNombreContratante();
				}
				
				poliza.setNombreAsegurado(cliente);
				//Setea tipo de poliza Individual o Flotilla
				poliza.setDescripcionTipo(cotizacion.getTipoPolizaDTO().getClaveAplicaFlotillas().equals(1)?"FLOTILLA":"INDIVIDUAL");
				
				//
				polizas.add(poliza);
			}
		}
		
		return polizas;
	}

	@Override
	public boolean equalsPolizaNombre(PolizaDTO poliza, String nombre) {
		if (poliza == null) {
			throw new IllegalArgumentException("poliza es nulo.");
		}
		//Cuanto es la maxima tolerancia de error al comparar el nombre.
		final double maximumErrorRate = .15; 
		
		CotizacionDTO cotizacion = poliza.getCotizacionDTO();
		String claveNegocio = cotizacion.getTipoPolizaDTO().getProductoDTO().getClaveNegocio();

		if (claveNegocio.equals(NegocioSeguros.CLAVE_DANOS)) {
			ClienteDTO contratante;
			try {
				contratante = clienteFacadeRemote.findById(cotizacion.getIdToPersonaContratante(), "");
			} catch (Exception e) {
				throw new RuntimeException(e);
			}

			if (looselyEquals(contratante.obtenerNombreCliente(), nombre, maximumErrorRate)) {
				return true;
			}
			
			ClienteDTO asegurado;
			try {
				asegurado = clienteFacadeRemote.findById(cotizacion.getIdToPersonaAsegurado(), "");
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
						
			return looselyEquals(asegurado.obtenerNombreCliente(), nombre, maximumErrorRate);
			
		
		} else {			
			if (looselyEquals(cotizacion.getNombreContratante(), nombre, maximumErrorRate)) {
				return true;
			}
						
			return looselyEquals(cotizacion.getNombreAsegurado(), nombre, maximumErrorRate);
		}
	}
	
	private boolean looselyEquals(String str1, String str2, double maximumErrorRate) {
		str1 = removeWhitespaces(str1).toLowerCase();
		str2 = removeWhitespaces(str2).toLowerCase();
		
		str1 = removeAccents(str1);
		str2 = removeAccents(str2);
		
		str1 = str1.replace("ñ", "n");
		str2 = str2.replace("ñ", "n");
		
		int levenshteinDistance = StringUtils.getLevenshteinDistance(str1, str2);	
		double errorRate = (double) levenshteinDistance / str1.length();
		return errorRate <= maximumErrorRate;
	}
	
	private String removeWhitespaces(String str) {
		return str.replace(" ", "");
	}
	
	private String removeAccents(String str) {
		str = Normalizer.normalize(str, Normalizer.Form.NFD);
		return str.replaceAll("[^\\p{ASCII}]", "");
	}
	
	@Override
	public boolean esPolizaAutoplazo(BigDecimal idToPoliza, List<SeccionDTO> seccionList){
		return polizaFacade.esPolizaAutoplazo(idToPoliza, seccionList);		
	}
	
	@Override
	public BigDecimal obtenerNumeroCotizacionSeycos(BigDecimal idToPoliza){
		return polizaFacade.obtenerNumeroCotizacionSeycos(idToPoliza);
	}
	
	@Override
	public String obtenerXMLPrimerReciboPoliza(BigDecimal idToPoliza){
		return polizaFacade.obtenerXMLPrimerReciboPoliza(idToPoliza);
	}
	
	@Override
	public BitacoraGuia getBitacoraEnvioPoliza(BigDecimal idToPoliza){
		BitacoraGuia bitacora = new BitacoraGuia();
		List<BitacoraGuia> bitacoras = trackingService.getBitacoraByIdToPoliza(idToPoliza);
		if(bitacoras!=null && bitacoras.size()>0){
			bitacora = bitacoras.get(0);
			Map<String, Object> map = new HashMap<String, Object>(1);
			map.put("IdToBitacora", bitacora.getId());
			List<DetalleBitacora> detalles  = entidadService.findByProperties(DetalleBitacora.class, map);
			bitacora.setDetalleBitacora(detalles);
		}
		return bitacora;
	}
	
	@Override
	public BigDecimal obtenerIdDoctoVersSeycos(BigDecimal idToPoliza, BigDecimal numeroEndoso){
		return polizaFacade.obtenerIdDoctoVersSeycos(idToPoliza, numeroEndoso);
	}
	
	@EJB
	public void setConvertidor(Convertidor convertidor) {
		this.convertidor = convertidor;
	}
	
	@EJB
	public void setPolizaFacade(PolizaFacadeRemote polizaFacade) {
		this.polizaFacade = polizaFacade;
	}
	
	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@EJB
	public void setEndosoFacadeRemote(EndosoFacadeRemote endosoFacadeRemote) {
		this.endosoFacadeRemote = endosoFacadeRemote;
	}
	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
}