package mx.com.afirme.midas2.service.impl.siniestro.movil.cuestionario;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;







import org.apache.commons.lang.NullArgumentException;
import org.apache.log4j.Logger;
import org.jfree.util.Log;

import mx.com.afirme.midas2.domain.siniestro.movil.cuestionario.PreguntaSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestro.movil.cuestionario.RespuestaNumericaSiniestroMovil;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestro.movil.cuestionario.CuestionarioSiniestroMovilService;

@Stateless
public class CuestionarioSiniestroMovilServiceImpl implements CuestionarioSiniestroMovilService {
	
	private static final Logger LOG = Logger.getLogger(CuestionarioSiniestroMovilServiceImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private EntidadService entidadService;
	
	@Override
	public List<PreguntaSiniestroMovil> obtenerPreguntas() {
		final String jpql = "SELECT p FROM PreguntaSiniestroMovil p WHERE p.activo = TRUE";		
		final TypedQuery<PreguntaSiniestroMovil> query = entityManager.createQuery(jpql, PreguntaSiniestroMovil.class);
		return query.getResultList();
	}

	@Override
	public boolean responder(List<RespuestaNumericaSiniestroMovil> respuestas) {
		for (RespuestaNumericaSiniestroMovil respuesta : respuestas) {
			validarRespuesta(respuesta);
			entidadService.save(respuesta);
		}
		return true;
	}
	
	private void validarRespuesta(RespuestaNumericaSiniestroMovil respuesta) {
		if (respuesta == null || respuesta.getRespuesta() == null) {
			throw new NullArgumentException(
					"No se capturó la información de la respuesta");
		}
		if (respuesta.getPregunta() == null
				|| respuesta.getPregunta().getId() == null) {
			throw new NullArgumentException(
					"No se capturó la información de la pregunta");
		}

	}

}
