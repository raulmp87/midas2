package mx.com.afirme.midas2.service.impl.compensaciones;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.domain.compensaciones.CaCompensacion;
import mx.com.afirme.midas2.domain.compensaciones.CaCorreos;
import mx.com.afirme.midas2.service.compensaciones.CaCorreosService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales.RAMO;
import mx.com.afirme.midas2.util.MailService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
 
/**
 * Facade for entity CaCorreos.
 * 
 * @see .CaCorreos
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CaCorreosServiceImpl implements CaCorreosService {
	// property constants
	public static final String AREA = "area";
	public static final String OFICINA = "oficina";
	public static final String USERNAME = "username";

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private MailService mailService;
	 
	private static final Logger LOGGER = LoggerFactory
	.getLogger(CaCorreosServiceImpl.class);
	
	public void save(CaCorreos entity) {
		
		try {
			entityManager.persist(entity);
		
		} catch (RuntimeException re) {
			LOGGER.error("-- save()", re);
			throw re;
		}
	}

	public void delete(CaCorreos entity) {
	
		try {
			entity = entityManager
					.getReference(CaCorreos.class, entity.getId());
			entityManager.remove(entity);
					} catch (RuntimeException re) {
			LOGGER.error("-- delete()", re);
			throw re;
		}
	}

	public CaCorreos update(CaCorreos entity) {
		
		try {
			CaCorreos result = entityManager.merge(entity);
			
			return result;
		} catch (RuntimeException re) {
			LOGGER.error("-- update()", re);;
			throw re;
		}
	}

	public CaCorreos findById(BigDecimal id) {		
				try {
			CaCorreos instance = entityManager.find(CaCorreos.class, id);
			return instance;
		} catch (RuntimeException re) {
			LOGGER.error("-- findById()", re);
			throw re;
		}
	}

	@SuppressWarnings("unchecked")
	public List<CaCorreos> findByProperty(String propertyName,
			final Object value) {

		try {
			final String queryString = "select model from CaCorreos model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("-- findByProperty()", re);
			throw re;
		}
	}

	public List<CaCorreos> findByArea(Object area) {
		return findByProperty(AREA, area);
	}

	public List<CaCorreos> findByOficina(Object oficina) {
		return findByProperty(OFICINA, oficina);
	}

	public List<CaCorreos> findByUsername(Object username) {
		return findByProperty(USERNAME, username);
	}
	
	@SuppressWarnings("unchecked")
	public List<CaCorreos> findAll() {
		
		try {
			
			final String queryString = "select model from CaCorreos model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("-- findAll()", re);
			throw re;
		}
	}
	
	/**
	 * 
	 * @param ramo
	 * @param tipoAutorizacionId
	 * @param caCompensacion
	 */
	@SuppressWarnings("unchecked")
	public void notificacionAutorizacionesCorreo(String movimiento, CaCompensacion caCompensacion){	
		LOGGER.info(">> notificacionAutorizacionesCorreo()");
		List<String> destinatarios = new ArrayList<String>();
		final StringBuilder queryString = new StringBuilder();
		
		try {			
			
			int index = 1;
			queryString.append(" SELECT tuse.email ");
			queryString.append(" FROM ASM.tbl_user UDAT ");
			queryString.append(" INNER JOIN ASM.tbl_user_data tuse ON (udat.user_id=tuse.user_id) ");
			queryString.append(" WHERE TRIM(UDAT.username) ");
			queryString.append(" IN(  ");
			queryString.append(" 	SELECT TRIM(username) ");
			queryString.append("    FROM MIDAS.ca_correos ");
			queryString.append("    WHERE ramo_id = ?  ");			
			
			
			if(RAMO.BANCA.getValor().equals(caCompensacion.getCaRamo().getValor().longValue())){
				queryString.append("  	AND tipoautorizacion_id = ? ");
			}else if(ConstantesCompensacionesAdicionales.CONFIGURACION.equals(movimiento) 
					|| (ConstantesCompensacionesAdicionales.MODIFICACION.equals(movimiento) 
								&& RAMO.VIDA.getValor().equals(caCompensacion.getCaRamo().getValor().longValue()))){
				queryString.append("  	AND tipoautorizacion_id IN (? , ?) ");
			}else if(ConstantesCompensacionesAdicionales.AUTORIZACION_AREA_TECNICA.equals(movimiento)
				|| ConstantesCompensacionesAdicionales.AUTORIZACION_ADMINISTRADOR_AGENTE.equals(movimiento)){				
				queryString.append("  	AND tipoautorizacion_id = ? ");
			}else{
				return;
			}
			queryString.append(" ) ");
			
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(index++, caCompensacion.getCaRamo().getValor());
			
			if(RAMO.BANCA.getValor().equals(caCompensacion.getCaRamo().getValor().longValue())){
				query.setParameter(index++, ConstantesCompensacionesAdicionales.AUTORIZACION_JURIDICO);
			}else if(ConstantesCompensacionesAdicionales.CONFIGURACION.equals(movimiento) 
					|| (ConstantesCompensacionesAdicionales.MODIFICACION.equals(movimiento) 
							&& RAMO.VIDA.getValor().equals(caCompensacion.getCaRamo().getValor().longValue()))){
				query.setParameter(index++, ConstantesCompensacionesAdicionales.AUTORIZACION_DIRECTORTECNICO);
				query.setParameter(index++, ConstantesCompensacionesAdicionales.AUTORIZACION_ADMONAGENTES);
			}else if(ConstantesCompensacionesAdicionales.AUTORIZACION_AREA_TECNICA.equals(movimiento)
				|| ConstantesCompensacionesAdicionales.AUTORIZACION_ADMINISTRADOR_AGENTE.equals(movimiento)){				
				query.setParameter(index++, ConstantesCompensacionesAdicionales.AUTORIZACION_JURIDICO);
			}
			
			List<String> resultList = query.getResultList();
			
			if(resultList != null){
				LOGGER.info("Total Emails Encontrados : " + resultList.size());
				for(String row : resultList){
					destinatarios.add(row.toString());				
				}
			}
			
		  if(!destinatarios.isEmpty()){			  
			  this.envioCorreo(destinatarios, movimiento, caCompensacion);
		  }	
			
		} catch (RuntimeException re) {
			LOGGER.error("-- notificacionAutorizacionesCorreo()", re);				
		}
		LOGGER.info("<< notificacionAutorizacionesCorreo()");
		
	}
	
	/**
	 * 
	 * @param destinatarios
	 * @param tipoAutorizacionId
	 * @param ramo
	 * @param caCompensacion
	 */
	private void envioCorreo(List<String> destinatarios, String movimiento, CaCompensacion caCompensacion){
		LOGGER.info(">> envioCorreo()");
		StringBuilder correo = new StringBuilder();
		
		String personaAutoriza = null;
		String accion = null;
		String value = null;
				
		if(RAMO.BANCA.getValor().equals(caCompensacion.getCaRamo().getValor().longValue())){
			accion = "creado";
			personaAutoriza = "Jurídico";			
		}else if(ConstantesCompensacionesAdicionales.CONFIGURACION.equals(movimiento)){			
			accion = "creado";
			personaAutoriza = "Director Técnico o Administrador de Agentes";
		}else if(ConstantesCompensacionesAdicionales.MODIFICACION.equals(movimiento) 
				&& RAMO.VIDA.getValor().equals(caCompensacion.getCaRamo().getValor().longValue()) ){
			accion = "modificado";
			personaAutoriza = "Director Técnico o Administrador de Agentes"
								.concat(" para aplicar el cambio")
								.concat(" apartir de la fecha ")
								.concat(new SimpleDateFormat("dd-MM-yyyy").format(caCompensacion.getFechaModificacionVida()))
								.concat(" ,");
		}else if(ConstantesCompensacionesAdicionales.MODIFICACION.equals(movimiento) 
				&& RAMO.DANOS.getValor().equals(caCompensacion.getCaRamo().getValor().longValue())
				&& caCompensacion.getIdReciboModificacion() > 0){	
						accion = "modificado";
						personaAutoriza = "Director Técnico o Administrador de Agentes"
								.concat(" para aplicar el cambio")
								.concat(" apartir del Recibo ")
								.concat(""+caCompensacion.getIdReciboModificacion())
								.concat(" ,");
			
		} else if(ConstantesCompensacionesAdicionales.AUTORIZACION_AREA_TECNICA.equals(movimiento)){
			accion = "validado por el Área de Técnica";
			personaAutoriza = "Jurídico";					
		}else if(ConstantesCompensacionesAdicionales.AUTORIZACION_ADMINISTRADOR_AGENTE.equals(movimiento)){
			accion = "validado por el Área de Administración de Agentes";
			personaAutoriza = "Jurídico";	
		}
		
		if(RAMO.AUTOS.getValor().equals(caCompensacion.getCaRamo().getValor().longValue())){
			value = " para el negocio id ".concat(caCompensacion.getNegocioId().toString());					
		}else if(RAMO.DANOS.getValor().equals(caCompensacion.getCaRamo().getValor().longValue())){
			value = " para la cotización id ".concat(caCompensacion.getCotizacionId().toString());			
		}else if(RAMO.VIDA.getValor().equals(caCompensacion.getCaRamo().getValor().longValue())){
			value = " ";
		}else if(RAMO.BANCA.getValor().equals(caCompensacion.getCaRamo().getValor().longValue())){
			value = " ";
		}
				
		correo.append(" Se ha ").append(accion);
		correo.append(" la Compensación Adicional id ").append(caCompensacion.getId());
		correo.append(value);
		correo.append(", es necesario la validación del ");
		correo.append(personaAutoriza);
		correo.append(" para procesar el pago. ");
		
		LOGGER.info(correo.toString());
		
		try{
			mailService.sendMail(destinatarios,
					"Notificacion de Autorización Compensaciones Adicionales",
					correo.toString(), 
					null,
					"", 
					"Estimado(s): ".concat("Usuario"));
		}catch(Exception e){
			LOGGER.error("Información del Error", e);
		}
		
		LOGGER.info("<< envioCorreo()");
	}
}
