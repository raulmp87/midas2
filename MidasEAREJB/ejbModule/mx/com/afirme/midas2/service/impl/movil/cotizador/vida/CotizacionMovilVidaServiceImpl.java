package mx.com.afirme.midas2.service.impl.movil.cotizador.vida;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.impl.catalogos.EntidadHistoricoDaoImpl;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.cotizador.CotizacionMovilDTO;
import mx.com.afirme.midas2.domain.movil.cotizador.DescuentosAgenteDTO;
import mx.com.afirme.midas2.dto.impresiones.ResumenCotMovilVidaDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas.sistema.seguridad.Usuario;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.Query;
import javax.sql.DataSource;

import oracle.jdbc.driver.OracleTypes;


import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.movil.cotizador.CotizacionMovilService;
import mx.com.afirme.midas2.service.movil.cotizador.vida.CotizacionMovilVidaService;
import mx.com.afirme.midas2.service.movil.cotizador.vida.ImpresionCotizacionMovilVidaService;
import mx.com.afirme.midas2.service.portal.cotizador.CotizadorPortalService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.tarifa.DescuentoAgenteService;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.vida.domain.movil.cotizador.CrearCotizacionVidaParameterDTO;
import mx.com.afirme.vida.domain.movil.cotizador.QuotationVO;
import mx.com.afirme.vida.domain.movil.cotizador.SumaAseguradaDTO;
import mx.com.afirme.vida.domain.movil.cotizador.TarifasMovilVidaDTO;
import mx.com.afirme.vida.service.tarifa.TarifaMovilVidaService;
import mx.com.afirme.vida.domain.movil.cotizador.OccupationDTO;

/**
 *
 */
@Stateless
public class CotizacionMovilVidaServiceImpl extends EntidadHistoricoDaoImpl
		implements CotizacionMovilVidaService {
	
	private static final Logger LOG = Logger.getLogger(CotizacionMovilVidaServiceImpl.class);
	
	private String ALTAASEGURADO = "SEYCOS.PKG_COTIZADOR_VIDA_ED.pAlta_Asegurado";
	private String ALTACOTIZACION = "SEYCOS.PKG_COTIZADOR_VIDA_ED.pAlta_Cotiza";
	private String BUSCACOTIZACION = "SEYCOS.PKG_COTIZADOR_VIDA_ED.pBuscaCotizacion";
	private String CAMBIOFORMAPAGO = "SEYCOS.PKG_COTIZADOR_VIDA_ED.pCambioFormaDePago";
	private String ESTADO_CIVIL = "C";
	private Integer ID_PRODUCTO = 1405;
	private Integer CLIENTE_AFIRME = 0;
	private String CLAVE_ASEGURADO = "T";
	private String USUARIO_MIDAS = "SISTEMA";
	public static final String USUARIO_MIDAS_MOVIL = "SISTEMA";
	private Integer ID_USR_COD_POS = 1;
	private Integer ID_COBERTURA = 974;
	private String VALOR_DEFAULT = "0";
	private String VALOR_DEFAULT_SA = "000000.0";
	private String VALOR_DEFAULT_DOUBLE = "0.0";
	private String VALOR_DEFAULT_SPACE=" ";
	private String VALOR_RFC = "880728";
	private String ESTATUS_DEFAULT = "1";
	public static final Double DERECHOPOLIZA = 150.00;
	private String CODIGOAGENTE;
	public static final int ID_MOSTRAR_SUMA_ASEGURADA_MOVIL = 502;
	
	public static final String CLAVENEGOCIO="V";
	
	public static final int FORMA_PAGO_PERIODO_SEMESTRAL = 2;
	public static final int FORMA_PAGO_PERIODO_TRIMESTRAL = 4;
	public static final int FORMA_PAGO_PERIODO_MENSUAL = 12;
	public static final Integer FORMA_PAGO_DEFAULT = 1;
	
	public static final Double CALCULA_PAGO_SEMESTRAL = 1.0455;
	public static final Double CALCULA_PAGO_TRIMESTRAL = 1.065;
	public static final Double CALCULA_PAGO_MENSUAL = 1.0802;
	
	
	private UsuarioService usuarioService;

	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	private SimpleJdbcCall cotizarVida;
	private SimpleJdbcCall altaAsegurado;
	private SimpleJdbcCall buscaCotizacion;
	private SimpleJdbcCall cambioFormaPago;
	private SimpleJdbcCall validarIMC;
	private JdbcTemplate jdbcTemplate;

	@EJB
	TarifaMovilVidaService tarifaMovilVidaService;
	@EJB
	private EntidadService entidadService;
	@EJB
	ImpresionCotizacionMovilVidaService impresionCotizacionMovilVidaService;
	@EJB
	GenerarPlantillaReporte generarPlantillaReporte;
	@EJB
	MailService mailService;
	@EJB
	private CatalogoValorFijoFacadeRemote beanRemoto;
	@EJB
	private AgenteMidasService agenteMidasService;
	@EJB
	private DescuentoAgenteService descuentoAgenteService;
	@EJB
	private CotizadorPortalService cotizacionPortalService;
	@EJB
	private CotizacionMovilService cotizacionMovilService;

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Object cotizar(CrearCotizacionVidaParameterDTO param) {
		ErrorBuilder eb = new ErrorBuilder();
		final Integer quotationId;
		Agente agente = new Agente();
		List<Agente> agenteList;
		Usuario usuario = null;
		String nombreProspecto = "";
		String telefonoProspecto = "";
		String emailProspecto = "";
		String apellidoPaterno = "";
		String apellidoMaterno = "";
		boolean usuarioValido = false;
		
		TarifasMovilVidaDTO tarifasMovilVidaDTO;
		CotizacionDTO cotizacion = new CotizacionDTO();
		ResumenCotMovilVidaDTO resumenCotMovilVidaDTO = new ResumenCotMovilVidaDTO();
		CotizacionMovilDTO cotizacionMovil = new CotizacionMovilDTO();
		
		DescuentosAgenteDTO descuentoAgenteDTO = new DescuentosAgenteDTO();
		List<DescuentosAgenteDTO> descuentoAgenteDTOList = null;
		BigDecimal idCotizacion = null;
		Agente filtro = new Agente();
		if(param.getTipoCotizacion() == null) {
			param.setTipoCotizacion(CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_MOVIL);
		}
		usuario = usuarioService.getUsuarioActual();
		if(!isNotNull(usuario)) {
			LOG.info(">>>>Entrando-->!isNotNull");
			LOG.info(">>>>param.getNombre()-->"+param.getNombre());
			LOG.info(">>>>param.getTelefono()-->"+param.getTelefono());
			LOG.info(">>>>param.getEmail()-->"+param.getEmail());
			LOG.info("param.getApellidoPaterno()-->"+param.getApellidoPaterno());
			LOG.info("param.getApellidoMaterno()-->"+param.getApellidoMaterno());
			nombreProspecto = param.getNombre();	
			telefonoProspecto = param.getTelefono();
			emailProspecto = param.getEmail();
			apellidoPaterno = param.getApellidoPaterno();
			apellidoMaterno = param.getApellidoMaterno();
			
			usuario = usuarioService.buscarUsuarioPorNombreUsuario(USUARIO_MIDAS);
			usuario.setNombreUsuario(USUARIO_MIDAS);
			usuario.setNombre(nombreProspecto);
			usuario.setApellidoPaterno(apellidoPaterno);
			usuario.setApellidoMaterno(apellidoMaterno);
			usuario.setEmail(emailProspecto);
			usuario.setTelefonoCelular(telefonoProspecto);
			usuario.setPromoCode("0");
			
		}else {
			LOG.info(">>>>Entrando-->isNotNull(else)");
			nombreProspecto = (regresaObjectEsNuloParametro(usuario.getNombreCompleto(), " ").toString());
			telefonoProspecto = (regresaObjectEsNuloParametro(usuario.getTelefonoCelular(), "").toString());
			emailProspecto = (regresaObjectEsNuloParametro(usuario.getEmail(), " ").toString());			
			//usuario = usuarioService.buscarUsuarioPorNombreUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
		}
		usuarioValido = cotizacionMovilService.isUserValid(usuario,
				CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_PORTAL
						.equals(param.getTipoCotizacion()));
		LOG.info("valor UsuarioValido==> " + usuarioValido  + "<=====");
		if (usuario != null) {
			LOG.info("usuario.getPromoCode>>>"+usuario.getPromoCode());
			if (usuario.getPromoCode() == null) {				
				usuario.setPromoCode("0");
			}
			LOG.info("getPromoCode==>>>"+usuario.getPromoCode());
			descuentoAgenteDTOList = descuentoAgenteService
			.findByPropertyActivos(CLAVENEGOCIO,"clavepromo",usuario.getPromoCode());
			param.setClaveagente(null);
		}
		
		if (descuentoAgenteDTOList != null) {
			for (DescuentosAgenteDTO descuentoAgenteDTOTmp : descuentoAgenteDTOList) {
				if (descuentoAgenteDTOTmp != null) {
					descuentoAgenteDTO = descuentoAgenteDTOTmp;
					param.setClaveagente(descuentoAgenteDTO
							.getClaveagente());
					filtro.setIdAgente(new Long(descuentoAgenteDTO
							.getClaveagente()));
				}
			}
		}
		
		agente.setIdAgente(new Long(descuentoAgenteDTO.getClaveagente()));
		agenteList = agenteMidasService.findByFilters(agente);
		for (Agente agenteTmp : agenteList) {
			if (agenteTmp != null)
				agente = agenteTmp;
		}
		CODIGOAGENTE = agente.getCodigoUsuario();
		LOG.info("getAgente==> " + CODIGOAGENTE  + "-----");
		
		if (CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_MOVIL.equals(param.getTipoCotizacion())) {
			if (param.getCveSexo() == null || param.getCveSexo().isEmpty()
					|| param.getCveSexo().equals("")) {
				throw new ApplicationException(eb.addFieldError("claveSexo",
						"Requerido"));
			} else if (param.getFNacimiento() == null) {
				throw new ApplicationException(eb.addFieldError("fechaNacimiento",
						"requerido"));
			} else if (UtileriasWeb
					.isFechaValida(param.getFNacimiento().toString())) {
				throw new ApplicationException(eb.addFieldError("fechaNacimiento",
						"Invalido"));
			} else if (param.getEstatura() == null
					|| param.getEstatura().equals("")) {
				throw new ApplicationException(eb.addFieldError("estatura",
						"Requerido"));
			} else if (param.getPeso() == null
					|| param.getPeso().equals("")) {
				throw new ApplicationException(eb.addFieldError("peso",
						"Requerido"));
			} else if (param.getSumaAsegurada() == null
					|| param.getSumaAsegurada().equals("")) {
				throw new ApplicationException(eb.addFieldError("sumaAsegurada",
						"Requerido"));
			} else if (param.getIdGiroOcup() == null
					|| param.getIdGiroOcup().equals("")) {
				throw new ApplicationException(eb.addFieldError("idGiroOcup",
						"Requerido"));
			} else {
				//Para que a las cot movil siempre les asigne nuevo quotationId
				param.setProcesada(true);
				param.setContratar(true);
				param.setIdToCotizacionMovil(null);
				tarifasMovilVidaDTO = tarifaMovilVidaService.findFechaNacimientoTarifaMovilVida(param.getFNacimiento());
			}
		} else {
			tarifasMovilVidaDTO = tarifaMovilVidaService.findFechaNacimientoTarifaMovilVida(param.getFNacimiento());
		}
		
		if(validaIMC(param).trim().equals("APROBADO")){
			if (tarifasMovilVidaDTO!=null) {				
				String quotationIdTmp="0";
				
					if(usuarioValido && param.getContratar() && !param.getProcesada() && param.getIdCotizacion() != null) {
						quotationId = param.getIdCotizacion();
						LOG.info("<<quotationId:"+quotationId);
					} else {
						if(usuarioValido){
							quotationIdTmp = (String) saveCotizacion();
						}
						quotationId = Integer.parseInt(quotationIdTmp);
						LOG.info("<<quotationId2:"+quotationId);
					}
				
				//if (quotationId > 0) {
					LOG.info(">>>Entrando a setear CotizacionMovil");
					/*if(!usuarioValido){
						cotizacionMovil.setUsuario(null);
					}*/
					
					idCotizacion = cotizacion.getIdToCotizacion();
					cotizacionMovil.setDescuentoAgente(descuentoAgenteDTO);
					cotizacionMovil.setFecharegistro(new Date());
					cotizacionMovil.setIdtocotizacionmovil(param.getIdToCotizacionMovil() != null ? 
							param.getIdToCotizacionMovil() : Long.valueOf(VALOR_DEFAULT));
					cotizacionMovil.setIdtocotizacionseycos(new BigDecimal(quotationId.toString()));
					cotizacionMovil.setTarifaPlatino(tarifasMovilVidaDTO.getTarifaPlatino());
					cotizacionMovil.setTarifaBasica(tarifasMovilVidaDTO.getTarifaBasica());
					cotizacionMovil.setUserId(Long.valueOf(usuario.getId()));
					cotizacionMovil.setUsuario(usuario);
					cotizacionMovil.setFechanacimiento(param.getFNacimiento());
					cotizacionMovil.setIdtocotizacionmidas(idCotizacion);
					cotizacionMovil.setOcupacionId(new Long(param.getIdGiroOcup()));
					cotizacionMovil.setSexo(param.getCveSexo());
					cotizacionMovil.setPeso(BigDecimal.valueOf(param.getPeso()));
					cotizacionMovil.setEstatura(BigDecimal.valueOf(param.getEstatura()));
					cotizacionMovil.setSumaasegurada(BigDecimal.valueOf(param.getSumaAsegurada()));
					cotizacionMovil.setFecharegistro(new Date());
					cotizacionMovil.setClavepromo(descuentoAgenteDTO.getClavepromo());
					cotizacionMovil.setClaveagente(regresaObjectEsNuloParametro(descuentoAgenteDTO.getClaveagente(),VALOR_DEFAULT_SPACE).toString());
					cotizacionMovil.setPorcentajeDescuento(descuentoAgenteDTO.getPorcentaje());
					cotizacionMovil.setNombreProspecto(regresaObjectEsNuloParametro(
									nombreProspecto,VALOR_DEFAULT_SPACE).toString());
					cotizacionMovil.setTelefonoProspecto(regresaObjectEsNuloParametro(
									telefonoProspecto,VALOR_DEFAULT_SPACE)
									.toString());
					cotizacionMovil.setEmailProspecto(regresaObjectEsNuloParametro(emailProspecto,VALOR_DEFAULT_SPACE).toString());
					cotizacionMovil.setClaveNegocio(CLAVENEGOCIO);
					cotizacionMovil.setEstatusProspecto(new Short(ESTATUS_DEFAULT));
					cotizacionMovil.setUuId(regresaObjectEsNuloParametro(param.getUuid(),VALOR_DEFAULT_SPACE).toString());
					cotizacionMovil.setOsMovil(regresaObjectEsNuloParametro(param.getOs(),VALOR_DEFAULT_SPACE).toString());
					cotizacionMovil.setTipoCotizacion(param.getTipoCotizacion());
					cotizacionMovil.setProcesada(param.getProcesada());
					entidadService.save(cotizacionMovil);
					
				/*} else {
					throw new ApplicationException(
							eb.addFieldError("Error",
									"Ocurrio un error durante la cotizacion, Intente nuevamente mas tarde"));
				}*/				
				param.setIdCotizacion(quotationId);
				if(param.getContratar()) {
					if(usuarioValido){
						LOG.info("Usuario antes de Guardar Asegurado"+cotizacionMovil.getUsuario());
						saveAsegurado(param , cotizacionMovil);
					}
					if (CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_PORTAL.equals(param.getTipoCotizacion()) &&
							!param.getProcesada()) {
						param.setProcesada(true);
						Map<String, Object> params = new HashMap<String, Object>();
						params.put("idtocotizacionmovil", cotizacionMovil.getIdtocotizacionmovil());
						cotizacionMovil = cotizacionPortalService.buscaCotizacion(params);
						cotizacionMovil.setProcesada(param.getProcesada());
						entidadService.save(cotizacionMovil);
						
						if(FORMA_PAGO_DEFAULT.compareTo(param.getIdFormaPago()) != 0) {
							this.cambioFormaPago(param);
						}
					}
				}				

				String mensualBasico;
				String trimestralBasico;
				String semestralBasico;
				String mensualPlatino;
				String trimestralPlatino;
				String semestralPlatino;
				
				MathContext mc = new MathContext(2, RoundingMode.HALF_EVEN);
				BigDecimal tB = tarifasMovilVidaDTO.getTarifaBasica();
				
			    BigDecimal totalMB=tB.divide(new BigDecimal(FORMA_PAGO_PERIODO_MENSUAL), MathContext.DECIMAL32).round(mc);
			    mensualBasico=UtileriasWeb.formatoMoneda(totalMB.multiply(new BigDecimal(CALCULA_PAGO_MENSUAL)));
			    
			    BigDecimal totalTB=tB.divide(new BigDecimal(FORMA_PAGO_PERIODO_TRIMESTRAL), MathContext.DECIMAL32).round(mc);
				trimestralBasico=UtileriasWeb.formatoMoneda(totalTB.multiply(new BigDecimal(CALCULA_PAGO_TRIMESTRAL)));
				
				BigDecimal totalSB=tB.divide(new BigDecimal(FORMA_PAGO_PERIODO_SEMESTRAL), MathContext.DECIMAL32).round(mc);
				semestralBasico=UtileriasWeb.formatoMoneda(totalSB.multiply(new BigDecimal(CALCULA_PAGO_SEMESTRAL)));
				
				BigDecimal tP=tarifasMovilVidaDTO.getTarifaPlatino();
				
				BigDecimal totalMP=tP.divide(new BigDecimal(FORMA_PAGO_PERIODO_MENSUAL), MathContext.DECIMAL32).round(mc);
				mensualPlatino=UtileriasWeb.formatoMoneda(totalMP.multiply(new BigDecimal(CALCULA_PAGO_MENSUAL)));
				
				BigDecimal totalTP=tP.divide(new BigDecimal(FORMA_PAGO_PERIODO_TRIMESTRAL), MathContext.DECIMAL32).round(mc);
				trimestralPlatino=UtileriasWeb.formatoMoneda(totalTP.multiply(new BigDecimal(CALCULA_PAGO_TRIMESTRAL)));
				
				BigDecimal totalSP=tP.divide(new BigDecimal(FORMA_PAGO_PERIODO_SEMESTRAL), MathContext.DECIMAL32).round(mc);
				semestralPlatino=UtileriasWeb.formatoMoneda(totalSP.multiply(new BigDecimal(CALCULA_PAGO_SEMESTRAL)));
				
				String derechoPoliza=UtileriasWeb.formatoMoneda(DERECHOPOLIZA);
				
				resumenCotMovilVidaDTO.setCotizacionMovil(cotizacionMovil);
				resumenCotMovilVidaDTO.setMensualBasico(mensualBasico);
				resumenCotMovilVidaDTO.setTrimestralBasico(trimestralBasico);
				resumenCotMovilVidaDTO.setSemestralBasico(semestralBasico);
				resumenCotMovilVidaDTO.setMensualPlatino(mensualPlatino);
				resumenCotMovilVidaDTO.setTrimestralPlatino(trimestralPlatino);
				resumenCotMovilVidaDTO.setSemestralPlatino(semestralPlatino);
				resumenCotMovilVidaDTO.setEdad(calcularEdad(param).toString());
				resumenCotMovilVidaDTO.setDerechoPoiza(derechoPoliza);
				
				if(param.getProcesada()) {
					try {
						/**/
						/* correo a cliente */
						ByteArrayAttachment attachment = null;
						List<String> destinatarios = new ArrayList<String>();
						List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
						List<String> destinatariosAgente = new ArrayList<String>();
						String [] correosDestinatarios = null;	
						String nombreArchivo = null;
						String aplicacion = null;
						if (CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_MOVIL.equals(param.getTipoCotizacion())) {
							nombreArchivo = "Cotizacion Movil Vida.pdf";
							aplicacion = "nuestra aplicaci\u00F3n M\u00F3vil";
						} else {
							nombreArchivo = CrearCotizacionVidaParameterDTO.PORTALPDF;
							aplicacion = "nuestro Portal";
						}
						attachment = new ByteArrayAttachment(
								nombreArchivo, TipoArchivo.PDF,
								generarPlantillaReporte.imprimirCotizacionMovilVida(resumenCotMovilVidaDTO));
						adjuntos.add(attachment);
						usuario   = cotizacionMovil.getUsuario();
						LOG.info("cotizacionMovil.getUsuario()==> " + usuario.getNombreCompleto()  + "<=====");
						if (attachment != null) {						
							String MensajeAgente = "Nos complace informarle que el prospecto: "+VALOR_DEFAULT_SPACE+ 
									regresaObjectEsNuloParametro(usuario.getNombreCompleto().toLowerCase().toString(),VALOR_DEFAULT_SPACE).toString()+VALOR_DEFAULT_SPACE
									+ "a utilizado " + aplicacion + " de Cotizaci\u00F3n y deseamos "
									+ "le de seguimiento para el cierre de su venta, la informaci\u00F3n correspondiente se encuentra en el documento Anexo "
									+ "Favor de comunicarse con su prospecto lo mas r\u00E1pido posible.";
							
							correosDestinatarios = cotizacionMovil.getDescuentoAgente().getEmail().toString().split(";");
							LOG.info("cotizacionMovil.getUsuario()==> " + correosDestinatarios  + "<=====");
							for(int i=0;i<=correosDestinatarios.length-1;i++){
								destinatariosAgente.add(correosDestinatarios[i]);
							}
							destinatarios.add(usuario.getEmail().toString());
							/* correo a cliente */
							mailService.sendMail(destinatarios,
									usuario.getNombre().toLowerCase().toString()
									.concat("-").concat(getNumeroCotizacion(cotizacionMovil.getIdtocotizacionseycos()).toString()),
									"Se le ha enviado una cotización a través de este medio.", 
									adjuntos,
									"Cotización Vida", 
									"Estimado(s): " + usuario.getNombreCompleto().toLowerCase().toString());
							/* correo a Agente */
							mailService.sendMail(
									destinatariosAgente,
									regresaObjectEsNuloParametro(cotizacionMovil.getDescuentoAgente().getNombre(),VALOR_DEFAULT_SPACE).toString()
									.concat("-").concat(getNumeroCotizacion(cotizacionMovil.getIdtocotizacionseycos()).toString()),
									MensajeAgente,
									adjuntos,
									"Cotización Vida",
									"Estimado(s):"+regresaObjectEsNuloParametro(cotizacionMovil.getDescuentoAgente().getNombre(),VALOR_DEFAULT_SPACE).toString());
						}
					} catch (Exception e) {
						LOG.error("Ocurri\u00F3 un error al enviar la cotizaci\u00F3n por correo...", e);
					}
				} else {
					if(CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_PORTAL.equals(param.getTipoCotizacion())) {
						resumenCotMovilVidaDTO.setCotizacionMovil(null);
						cotizacionMovil.setResumenCotMovilVidaDTO(resumenCotMovilVidaDTO);
					}
				}
			} else {
				throw new ApplicationException(eb.addFieldError("fechaNacimiento",
						"Invalido o verifica los datos Capturados"));
			}
		}
		else if(validaIMC(param).trim().equals("NO APROBADO")){
			if (usuario != null) {
				LOG.info("usuario.getPromoCode>>>"+usuario.getPromoCode());
				if (usuario.getPromoCode() == null) {				
					usuario.setPromoCode("0");
				}
				LOG.info("getPromoCode>>>"+usuario.getPromoCode());
				  descuentoAgenteDTOList = descuentoAgenteService
					.findByPropertyActivos(CLAVENEGOCIO,"clavepromo",usuario.getPromoCode());
				  param.setClaveagente(null);
				
			}
			if (descuentoAgenteDTOList != null) {
				for (DescuentosAgenteDTO descuentoAgenteDTOTmp : descuentoAgenteDTOList) {
					if (descuentoAgenteDTOTmp != null) {
						descuentoAgenteDTO = descuentoAgenteDTOTmp;
						param.setClaveagente(descuentoAgenteDTO
								.getClaveagente());
						filtro.setIdAgente(new Long(descuentoAgenteDTO
								.getClaveagente()));
					}

				}
			}
			
			cotizacionMovil.setDescuentoAgente(descuentoAgenteDTO);
			
			List<String> destinatarios = new ArrayList<String>();
			List<String> destinatariosAgente = new ArrayList<String>();
			String [] correosDestinatarios = null;
				
				String MensajeAgente = "Nos complace informarle que el prospecto: "+VALOR_DEFAULT_SPACE+ usuario.getNombreCompleto().toString().toString()
						+ VALOR_DEFAULT_SPACE+"(correo: "+ usuario.getEmail().toString() +VALOR_DEFAULT_SPACE+" y numero de telefono:"
						+ VALOR_DEFAULT_SPACE+ usuario.getTelefonoCelular().toString()+")"
						+ "a utilizado nuestra aplicaci\u00F3n M\u00F3vil de Cotizaci\u00F3n,"
						+ "Sin embargo no se pudo proceder con la cotizacion debido que el IMC esta fuera de de los parámetros establecidos,"
						+ "le pedimos le de seguimiento";
				
				String MensajeCliente=" EL IMC ESTA FUERA DE LOS PARAMETROS ESTABLECIDOS" + VALOR_DEFAULT_SPACE
					+"FAVOR DE COMUNICARSE CON SU AGENTE AL"+VALOR_DEFAULT_SPACE+ cotizacionMovil.getDescuentoAgente().getNumerotelefono().toString() +VALOR_DEFAULT_SPACE+"O A SEGUROS AFIRME AL TELEFONO 01 800 AFIRME";
				correosDestinatarios = cotizacionMovil.getDescuentoAgente().getEmail().toString().split(";");
				for(int i=0;i<=correosDestinatarios.length-1;i++){
					destinatariosAgente.add(correosDestinatarios[i]);
				}
				destinatarios.add(usuario.getEmail().toString());
				/* correo a cliente */
				mailService.sendMail(destinatarios,
						usuario.getNombreUsuario(),
						MensajeCliente, 
						null,
						"Cotización Vida", 
						"Estimado(s): " + usuario.getNombre());
				/* correo a Agente */
				mailService.sendMail(
						destinatariosAgente,
						cotizacionMovil.getDescuentoAgente().getNombre().toString(),
						MensajeAgente,
						null,
						"Cotización Vida",
						"Estimado(s): "+cotizacionMovil.getDescuentoAgente().getNombre().toString());
				
				if (CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_PORTAL.equals(param.getTipoCotizacion())) {
					throw new ApplicationException(eb.addFieldError("IMC", MensajeCliente));
				} else {
					return true;		
				}		
			}		
		return cotizacionMovil;
	}
	
	@SuppressWarnings("unused")
	private DataSource dataSource;

	@Resource(name = "jdbc/InterfazDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.cotizarVida = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("SEYCOS")
				.withCatalogName("PKG_COTIZADOR_VIDA_ED")
				.withProcedureName("pAlta_Cotiza")
				.declareParameters(
						new SqlParameter("pcve_usuario", Types.VARCHAR),
						new SqlParameter("pid_producto", Types.NUMERIC),
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Codigo", Types.VARCHAR),
						new SqlOutParameter("pResult_Cursor",
								OracleTypes.CURSOR, new RowMapper<Object>() {
									@Override
									public Object mapRow(ResultSet rs, int index)
											throws SQLException {
										Object item = new Object();
										item = rs.getString("id_cotizacion");
										return item;
									}
								}));

		this.altaAsegurado = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("SEYCOS")
				.withCatalogName("PKG_COTIZADOR_VIDA_ED")
				.withProcedureName("pAlta_Asegurado")
				.declareParameters(

						new SqlParameter("pId_Cotizacion", Types.NUMERIC),
						new SqlParameter("pNombre", Types.VARCHAR),
						new SqlParameter("pApellido_Paterno", Types.VARCHAR),
						new SqlParameter("pApellido_Materno", Types.VARCHAR),
						new SqlParameter("pid_estado", Types.VARCHAR),
						new SqlParameter("pid_municipio", Types.VARCHAR),
						new SqlParameter("pCalle_numero", Types.VARCHAR),
						new SqlParameter("pCodigo_Postal", OracleTypes.CHAR),
						new SqlParameter("pid_usr_cod_post", Types.NUMERIC),
						new SqlParameter("pColonia", Types.VARCHAR),
						new SqlParameter("pTelef_Oficina", Types.VARCHAR),
						new SqlParameter("pTelef_Fax", Types.VARCHAR),
						new SqlParameter("pTelef_Casa", Types.VARCHAR),
						new SqlParameter("pE_Mail", Types.VARCHAR),
						new SqlParameter("pCURP", Types.VARCHAR),
						new SqlParameter("pCve_Sexo", OracleTypes.CHAR),
						new SqlParameter("pF_Nacimiento", OracleTypes.DATE),
						new SqlParameter("pCve_Edo_Civil", OracleTypes.CHAR),
						new SqlParameter("pSiglas_RFC", Types.VARCHAR),
						new SqlParameter("pF_RFC", OracleTypes.CHAR),
						new SqlParameter("pHomoclave_RFC", Types.VARCHAR),
						new SqlParameter("pcve_t_asegura", OracleTypes.CHAR),
						new SqlParameter("pid_cobertura", Types.NUMERIC),
						new SqlParameter("psuma_amparada", Types.NUMERIC),
						new SqlParameter("pIdLugar_nac", Types.NUMERIC),
						new SqlParameter("pEstatura", Types.NUMERIC),
						new SqlParameter("pPeso", Types.NUMERIC),
						new SqlParameter("pId_Giro_ocup", OracleTypes.CHAR),
						new SqlParameter("pdet_ocupa", Types.VARCHAR),
						new SqlParameter("pcliente_afirme", Types.NUMERIC),
						new SqlParameter("pid_domicilio", Types.NUMERIC),
						new SqlParameter("pid_producto", Types.NUMERIC),
						new SqlParameter("p_preferredbeneficiary",
								Types.VARCHAR),
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Codigo", Types.VARCHAR),
						new SqlOutParameter("pResult_Cursor",
								OracleTypes.CURSOR, new RowMapper<Object>() {
									@Override
									public Object mapRow(ResultSet rs, int index)
											throws SQLException {
										Object item = new Object();
										item = rs.getString("id_Asegurado");
										return item;
									}
								}));

		this.buscaCotizacion = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("SEYCOS")
				.withCatalogName("PKG_COTIZADOR_VIDA_ED")
				.withProcedureName("pBuscaCotizacion")
				.declareParameters(
						new SqlParameter("pId_cotizacion", Types.NUMERIC),
						new SqlParameter("pRfc", Types.VARCHAR),
						new SqlParameter("pNombre", Types.VARCHAR),
						new SqlParameter("pap_pat", Types.VARCHAR),
						new SqlParameter("pap_mat", Types.VARCHAR),
						new SqlParameter("pUsuario", Types.VARCHAR),
						new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
						new SqlOutParameter("pDesc_Codigo", Types.VARCHAR),
						new SqlOutParameter("pResult_Cursor",
								OracleTypes.CURSOR,
								new RowMapper<QuotationVO>() {
									@Override
									public QuotationVO mapRow(ResultSet rs,
											int index) throws SQLException {
										QuotationVO item = new QuotationVO();
										item.setQuotationId(Integer
												.parseInt(regresaObjectEsNuloParametro(
														rs.getString("quotation_Id"),
														VALOR_DEFAULT).toString()));
										item.setQuotationNumber(Integer
												.parseInt(regresaObjectEsNuloParametro(
														rs.getString("quotation_Number"),
														VALOR_DEFAULT).toString()));
										item.setHomePhoneNumber(regresaObjectEsNuloParametro(
												rs.getString("home_Phone_Number"),
												VALOR_DEFAULT_SPACE).toString());
										item.setProductId(Integer
												.parseInt(regresaObjectEsNuloParametro(
														rs.getString("product_Id"),
														VALOR_DEFAULT).toString()));
										item.setQuotationStatus(regresaObjectEsNuloParametro(
												rs.getString("quotation_Status"),
												VALOR_DEFAULT_SPACE).toString());
										item.setUserId(regresaObjectEsNuloParametro(
												rs.getString("user_id"), VALOR_DEFAULT_SPACE)
												.toString());
										item.setMethodOfPaymentId(Integer
												.parseInt(regresaObjectEsNuloParametro(
														rs.getString("method_Of_Payment_Id"),
														VALOR_DEFAULT).toString()));
										item.setPaymentTypeId(Integer
												.parseInt(regresaObjectEsNuloParametro(
														rs.getString("payment_Type_Id"),
														VALOR_DEFAULT).toString()));
										item.setAmountNetPremium(Double
												.parseDouble(regresaObjectEsNuloParametro(
														rs.getString("amount_net_premium"),
														VALOR_DEFAULT_DOUBLE).toString()));
										item.setAmountDiscount(Double
												.parseDouble(regresaObjectEsNuloParametro(
														rs.getString("amount_discount"),
														VALOR_DEFAULT_DOUBLE).toString()));
										item.setAmountIva(Double
												.parseDouble(regresaObjectEsNuloParametro(
														rs.getString("amount_iva"),
														VALOR_DEFAULT_DOUBLE).toString()));
										item.setAmountRights(Double
												.parseDouble(regresaObjectEsNuloParametro(
														rs.getString("amount_rights"),
														VALOR_DEFAULT_DOUBLE).toString()));
										item.setAmountPaymentSurcharge(Double
												.parseDouble(regresaObjectEsNuloParametro(
														rs.getString("amount_Payment_Surcharge"),
														VALOR_DEFAULT_DOUBLE).toString()));
										item.setAmountTotalPremium(Double
												.parseDouble(regresaObjectEsNuloParametro(
														rs.getString("amount_total_premium"),
														VALOR_DEFAULT_DOUBLE).toString()));
										item.setAgentId(Integer
												.parseInt(regresaObjectEsNuloParametro(
														rs.getString("agent_id"),
														VALOR_DEFAULT).toString()));
										item.setProductId(Integer
												.parseInt(regresaObjectEsNuloParametro(
														rs.getString("product_id"),
														VALOR_DEFAULT).toString()));
										return item;
									}
								}));
		this.validarIMC = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("PKG_COTIZADOR_VIDA").withFunctionName("fnValidaIMC")
		.declareParameters(
				new SqlParameter("pid_cotizacion", Types.NUMERIC),
				new SqlParameter("pSumaAsegurada", Types.NUMERIC),
				new SqlParameter("pPeso", Types.NUMERIC),
				new SqlParameter("pEstatura", Types.NUMERIC),
				new SqlParameter("pEdad",Types.VARCHAR),
				new SqlParameter("pCve_Sexo",Types.CHAR),
				new SqlParameter("pGiro_Ocup",Types.NUMERIC));
		
		this.cambioFormaPago = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("PKG_COTIZADOR_VIDA_ED")
		.withProcedureName("pCambioFormaDePago")
		.declareParameters(
				new SqlParameter("pid_cotizacion", Types.NUMERIC),
				new SqlParameter("pid_forma_pago", Types.NUMERIC),
				new SqlOutParameter("pId_Cod_Resp", Types.NUMERIC),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Object saveCotizacion() {
		List<Object> obj = null;
		Object resultCotizacion = null;
		try {
			LOG.trace("Entrando a  " + ALTACOTIZACION + ".save...");
			LOG.info("pcve_usuario =  "
					+ CODIGOAGENTE+ "....");
			LOG.info("pid_producto =  " + this.ID_PRODUCTO);
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter
			.addValue("pcve_usuario", CODIGOAGENTE)
			.addValue("pid_producto", this.ID_PRODUCTO);
			Map<String, Object> execute = this.cotizarVida
					.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Codigo = (String) execute.get("pDesc_Codigo");
			obj = (List<Object>) execute.get("pResult_Cursor");
			resultCotizacion = obj.get(0);
			if (idCodResp.compareTo(BigDecimal.ONE) != 0) {
				throw new ApplicationException(
						(String) execute.get("pDesc_Codigo"));
			}
			/**/
			LOG.info("Se ha guardado cotizacion...");
			LOG.info("pid_Cod_Resp..=>" + idCodResp);
			LOG.info("pDesc_Codigo..=>" + pDesc_Codigo);
			LOG.info("resultCotizacion..=>" + resultCotizacion);
		} catch (Exception e) {
			LOG.error("Excepcion general en " + ALTACOTIZACION
					+ ".save...", e);
		}
		return resultCotizacion;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	 public Integer calcularEdad(CrearCotizacionVidaParameterDTO param){
		    Date fechaNac=null;
		        try {
		            fechaNac = param.getFNacimiento();
		        } catch (Exception ex) {
		        	LOG.error("CalculaEdad=>" +".Resumen"+ex);
		        }
		        Calendar fechaNacimiento = Calendar.getInstance();
		        Calendar fechaActual = Calendar.getInstance();
		        fechaNacimiento.setTime(fechaNac);
		        int year = fechaActual.get(Calendar.YEAR)- fechaNacimiento.get(Calendar.YEAR);
		        int month =fechaActual.get(Calendar.MONTH)- fechaNacimiento.get(Calendar.MONTH);
		        int day = fechaActual.get(Calendar.DATE)- fechaNacimiento.get(Calendar.DATE);
		        if(month<0 || (month==0 && day<0)){
		        	year--;
		        }
		        return year;
		    }
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Object saveAsegurado(CrearCotizacionVidaParameterDTO param, CotizacionMovilDTO cotizacionMovil) {
		Usuario usuario = null;
		LOG.info("<<entrando a saveAsegurado::::con getIdtocotizacionseycos-->>>>>"+cotizacionMovil.getIdtocotizacionseycos());
		usuario = cotizacionMovil.getUsuario();
		Object result = null;
		try {
				if(CrearCotizacionVidaParameterDTO.TIPO_COTIZACION_PORTAL.equals(param.getTipoCotizacion()) && usuario == null) {
					usuario = usuarioService.buscarUsuarioPorNombreUsuario(USUARIO_MIDAS);
					usuario.setNombre(regresaObjectEsNuloParametro(param.getNombre(), " ").toString());
					usuario.setApellidoPaterno(regresaObjectEsNuloParametro(param.getApellidoPaterno(), " ").toString());
					usuario.setApellidoMaterno(regresaObjectEsNuloParametro(param.getApellidoMaterno(), " ").toString());
					usuario.setNombreUsuario(usuario.getNombreCompleto());
					usuario.setEmail(regresaObjectEsNuloParametro(param.getMail(), " ").toString());
					usuario.setTelefonoCelular(regresaObjectEsNuloParametro(param.getTelCasa(), " ").toString());
				}
			
			LOG.info(">>>Entrando a  " + ALTAASEGURADO + ".save...");
		    LOG.info("usuario=>"+usuario);
		    LOG.info("nombre=>"+usuario.getNombre());
		    LOG.info("APaterno=>"+usuario.getApellidoPaterno());
		    LOG.info("AMaterno=>"+usuario.getApellidoMaterno());
			LOG.info("pId_Cotizacion="+ Integer.parseInt(String.valueOf(cotizacionMovil.getIdtocotizacionseycos())));
			LOG.info("pNombre=" + regresaObjectEsNuloParametro(usuario.getNombre(), VALOR_DEFAULT_SPACE).toString());
			LOG.info("pApellido_Paterno="+ regresaObjectEsNuloParametro(usuario.getApellidoPaterno(), VALOR_DEFAULT_SPACE));
			LOG.info("pApellido_Materno="+ regresaObjectEsNuloParametro(usuario.getApellidoMaterno(), VALOR_DEFAULT_SPACE).toString());
			LOG.info("pid_estado="+ regresaObjectEsNuloParametro(param.getIdEstado(), VALOR_DEFAULT).toString());
			LOG.info("pid_municipio="+ regresaObjectEsNuloParametro(param.getIdMunicipio(), VALOR_DEFAULT).toString());
			LOG.info("pCalle_numero="+ regresaObjectEsNuloParametro(param.getCalleNumero(), VALOR_DEFAULT_SPACE).toString());
			LOG.info("pCodigo_Postal="+ regresaObjectEsNuloParametro(param.getCodigoPostal(), VALOR_DEFAULT).toString());
			LOG.info("pid_usr_cod_post="+ Integer.parseInt(regresaObjectEsNuloParametro(this.ID_USR_COD_POS, VALOR_DEFAULT).toString()));
			LOG.info("pColonia="+ regresaObjectEsNuloParametro(param.getColonia(),VALOR_DEFAULT_SPACE).toString());
			LOG.info("pTelef_Oficina="+ regresaObjectEsNuloParametro(param.getTelefOficina(), VALOR_DEFAULT).toString());
			LOG.info("pTelef_Fax="+ regresaObjectEsNuloParametro(param.getTeleFax(), VALOR_DEFAULT).toString());
			LOG.info("pTelef_Casa="+ regresaObjectEsNuloParametro(usuario.getTelefonoCelular(), VALOR_DEFAULT).toString());
			LOG.info("pE_Mail="+ regresaObjectEsNuloParametro(param.getEmail(), VALOR_DEFAULT_SPACE).toString());
			LOG.info("pCURP="+ regresaObjectEsNuloParametro(param.getCurp(), VALOR_DEFAULT).toString());
			LOG.info("pCve_Sexo="+ regresaObjectEsNuloParametro(param.getCveSexo(),VALOR_DEFAULT_SPACE).toString());
			LOG.info("pF_Nacimiento=" + cotizacionMovil.getFechanacimiento());
			LOG.info("pCve_Edo_Civil="+ regresaObjectEsNuloParametro(this.ESTADO_CIVIL, VALOR_DEFAULT_SPACE).toString());
			LOG.info("pSiglas_RFC="+ regresaObjectEsNuloParametro(param.getSiglasRFC(), VALOR_DEFAULT_SPACE).toString());
			LOG.info("pF_RFC="+ regresaObjectEsNuloParametro(param.getfRFC(),VALOR_RFC).toString());
			LOG.info("pHomoclave_RFC="+ regresaObjectEsNuloParametro(param.getHomoclaveRFC(), VALOR_DEFAULT).toString());
			LOG.info("pcve_t_asegura="+ regresaObjectEsNuloParametro(this.CLAVE_ASEGURADO, VALOR_DEFAULT_SPACE).toString());
			LOG.info("pid_cobertura="+ Integer.parseInt(regresaObjectEsNuloParametro(param.getIdCobertura(), this.ID_COBERTURA).toString()));
			LOG.info("psuma_amparada="+ Double.parseDouble(regresaObjectEsNuloParametro(param.getsA_amparada(), VALOR_DEFAULT_SA).toString()));
			LOG.info("pIdLugar_nac="+ Integer.parseInt(regresaObjectEsNuloParametro(param.getIdLugarNac(), VALOR_DEFAULT).toString()));
			LOG.info("pEstatura="+ Double.parseDouble(regresaObjectEsNuloParametro(cotizacionMovil.getEstatura(), VALOR_DEFAULT_DOUBLE).toString()));
			LOG.info("pPeso="+ Double.parseDouble(regresaObjectEsNuloParametro(cotizacionMovil.getPeso(), VALOR_DEFAULT_DOUBLE).toString()));
			LOG.info("pId_Giro_ocup="+ regresaObjectEsNuloParametro(cotizacionMovil.getOcupacionId(), VALOR_DEFAULT).toString());
			LOG.info("pdet_ocupa="+ regresaObjectEsNuloParametro(param.getDetOcupa(), VALOR_DEFAULT_SPACE).toString());
			LOG.info("pcliente_afirme="+ Integer.parseInt(regresaObjectEsNuloParametro(this.CLIENTE_AFIRME, VALOR_DEFAULT).toString()));
			LOG.info("pid_domicilio="+ Integer.parseInt(regresaObjectEsNuloParametro(param.getIdDomicilio(), VALOR_DEFAULT).toString()));
			LOG.info("pid_producto="+ Integer.parseInt(regresaObjectEsNuloParametro(this.ID_PRODUCTO, VALOR_DEFAULT).toString()));
			LOG.info("p_preferredbeneficiary="+ regresaObjectEsNuloParametro(param.getPreferredBeneficiary(), VALOR_DEFAULT_SPACE).toString());

			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter
					.addValue(
							"pId_Cotizacion",
							Integer.parseInt(String.valueOf(cotizacionMovil.getIdtocotizacionseycos())))
					.addValue(
							"pNombre",
							regresaObjectEsNuloParametro(usuario.getNombre(),
									VALOR_DEFAULT_SPACE).toString())
					.addValue(
							"pApellido_Paterno",
							regresaObjectEsNuloParametro(
									usuario.getApellidoPaterno(), VALOR_DEFAULT_SPACE)
									.toString())
					.addValue(
							"pApellido_Materno",
							regresaObjectEsNuloParametro(
									usuario.getApellidoMaterno(), VALOR_DEFAULT_SPACE)
									.toString())
					.addValue(
							"pid_estado",
							regresaObjectEsNuloParametro(param.getIdEstado(),
									VALOR_DEFAULT).toString())
					.addValue(
							"pid_municipio",
							regresaObjectEsNuloParametro(
									param.getIdMunicipio(), VALOR_DEFAULT).toString())
					.addValue(
							"pCalle_numero",
							regresaObjectEsNuloParametro(
									param.getCalleNumero(), VALOR_DEFAULT_SPACE).toString())
					.addValue(
							"pCodigo_Postal",
							regresaObjectEsNuloParametro(
									param.getCodigoPostal(), VALOR_DEFAULT).toString())
					.addValue(
							"pid_usr_cod_post",
							Integer.parseInt(regresaObjectEsNuloParametro(
									this.ID_USR_COD_POS, VALOR_DEFAULT).toString()))
					.addValue(
							"pColonia",
							regresaObjectEsNuloParametro(param.getColonia(),
									VALOR_DEFAULT_SPACE).toString())
					.addValue(
							"pTelef_Oficina",
							regresaObjectEsNuloParametro(
									param.getTelefOficina(), VALOR_DEFAULT)
									.toString())
					.addValue(
							"pTelef_Fax",
							regresaObjectEsNuloParametro(param.getTeleFax(),
									VALOR_DEFAULT).toString())
					.addValue(
							"pTelef_Casa",
							regresaObjectEsNuloParametro(
									usuario.getTelefonoCelular(), VALOR_DEFAULT)
									.toString())
					.addValue(
							"pE_Mail",
							regresaObjectEsNuloParametro(usuario.getEmail(),
									VALOR_DEFAULT_SPACE).toString())
					.addValue(
							"pCURP",
							regresaObjectEsNuloParametro(param.getCurp(), VALOR_DEFAULT)
									.toString())
					.addValue(
							"pCve_Sexo",
							regresaObjectEsNuloParametro(param.getCveSexo(),
									VALOR_DEFAULT_SPACE).toString())
					.addValue("pF_Nacimiento", cotizacionMovil.getFechanacimiento())
					.addValue(
							"pCve_Edo_Civil",
							regresaObjectEsNuloParametro(this.ESTADO_CIVIL, VALOR_DEFAULT_SPACE)
									.toString())
					.addValue(
							"pSiglas_RFC",
							regresaObjectEsNuloParametro(param.getSiglasRFC(),
									VALOR_DEFAULT_SPACE).toString())
					.addValue(
							"pF_RFC",
							regresaObjectEsNuloParametro(param.getfRFC(),
									VALOR_RFC).toString())
					.addValue(
							"pHomoclave_RFC",
							regresaObjectEsNuloParametro(
									param.getHomoclaveRFC(), VALOR_DEFAULT).toString())
					.addValue(
							"pcve_t_asegura",
							regresaObjectEsNuloParametro(this.CLAVE_ASEGURADO,
									VALOR_DEFAULT_SPACE).toString())
					.addValue(
							"pid_cobertura",
							Integer.parseInt(regresaObjectEsNuloParametro(
									param.getIdCobertura(), this.ID_COBERTURA).toString()))
					.addValue(
							"psuma_amparada",
							Double.parseDouble(regresaObjectEsNuloParametro(
									param.getsA_amparada(), VALOR_DEFAULT_SA)
									.toString()))
					.addValue(
							"pIdLugar_nac",
							Integer.parseInt(regresaObjectEsNuloParametro(
									param.getIdLugarNac(), VALOR_DEFAULT).toString()))
					.addValue(
							"pEstatura",
							Double.parseDouble(regresaObjectEsNuloParametro(
									cotizacionMovil.getEstatura(), VALOR_DEFAULT_DOUBLE).toString()))
					.addValue(
							"pPeso",
							Double.parseDouble(regresaObjectEsNuloParametro(
									cotizacionMovil.getPeso(), VALOR_DEFAULT_DOUBLE).toString()))
					.addValue(
							"pId_Giro_ocup",
							regresaObjectEsNuloParametro(
									cotizacionMovil.getOcupacionId(), VALOR_DEFAULT).toString())
					.addValue(
							"pdet_ocupa",
							regresaObjectEsNuloParametro(param.getDetOcupa(),
									VALOR_DEFAULT_SPACE).toString())
					.addValue(
							"pcliente_afirme",
							Integer.parseInt(regresaObjectEsNuloParametro(
									CLIENTE_AFIRME, VALOR_DEFAULT).toString()))
					.addValue(
							"pid_domicilio",
							Integer.parseInt(regresaObjectEsNuloParametro(
									param.getIdDomicilio(), VALOR_DEFAULT).toString()))
					.addValue(
							"pid_producto",
							Integer.parseInt(regresaObjectEsNuloParametro(
									this.ID_PRODUCTO, VALOR_DEFAULT).toString()))				
					.addValue(
							"p_preferredbeneficiary",
							regresaObjectEsNuloParametro(
									param.getPreferredBeneficiary(), VALOR_DEFAULT_SPACE)
									.toString());
			Map<String, Object> execute = this.altaAsegurado
					.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Codigo = (String) execute.get("pDesc_Codigo");
			result = execute.get("pResult_Cursor");
			if (idCodResp.compareTo(BigDecimal.ONE) != 0) {
				throw new ApplicationException(
						(String) execute.get("pDesc_Codigo"));
			}
			/**/
			LOG.info("Se ha guardado asegurado..." + pDesc_Codigo);
			LOG.info("Se ha guardado asegurado..." + result);
		} catch (Exception e) {
			LOG.error("Excepcion general en " + ALTAASEGURADO
					+ ".save...", e);
		}
		return result;
	}
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String validaIMC(CrearCotizacionVidaParameterDTO param) {
		String result = null;
		LOG.info(">>>Entrando a  "+this.getClass().getName());
		try {
			LOG.info("pId_Cotizacion="+ Integer.parseInt(String.valueOf(VALOR_DEFAULT)));
			LOG.info("pCve_Sexo="+ regresaObjectEsNuloParametro(param.getCveSexo(),VALOR_DEFAULT_SPACE).toString());
			LOG.info("pEdad=" + calcularEdad(param));
			LOG.info("pSumaAsegurada="+ Double.parseDouble(regresaObjectEsNuloParametro(param.getSumaAsegurada(), VALOR_DEFAULT_SA).toString()));
			LOG.info("pEstatura="+ Double.parseDouble(regresaObjectEsNuloParametro(param.getEstatura(), VALOR_DEFAULT_DOUBLE).toString()));			
			LOG.info("pPeso="+ Double.parseDouble(regresaObjectEsNuloParametro(param.getPeso(), VALOR_DEFAULT_DOUBLE).toString()));
			LOG.info("pGiro_Ocup="+ regresaObjectEsNuloParametro(param.getIdGiroOcup(), VALOR_DEFAULT).toString());

			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter
					.addValue(
							"pId_Cotizacion",Integer.parseInt(String.valueOf(VALOR_DEFAULT)))
					.addValue(
							"pCve_Sexo",regresaObjectEsNuloParametro(param.getCveSexo(),VALOR_DEFAULT_SPACE).toString())
					.addValue(
							"pF_Nacimiento", param.getFNacimiento())
					.addValue(
							"pEdad", calcularEdad(param))
					.addValue(
							"pSumaAsegurada",Double.parseDouble(regresaObjectEsNuloParametro(param.getSumaAsegurada(), VALOR_DEFAULT_SA).toString()))
					.addValue(
							"pEstatura",Double.parseDouble(regresaObjectEsNuloParametro(param.getEstatura(), VALOR_DEFAULT_DOUBLE).toString()))
					.addValue(
							"pPeso",Double.parseDouble(regresaObjectEsNuloParametro(param.getPeso(), VALOR_DEFAULT_DOUBLE).toString()))
					.addValue(
							"pGiro_Ocup",regresaObjectEsNuloParametro(param.getIdGiroOcup(), VALOR_DEFAULT).toString());
				
			String resultExecute = validarIMC.execute(sqlParameter).get("return").toString();
			result=resultExecute;
			
		} catch (Exception e) {
			LOG.error("Excepcion general en validado asegurado..." , e);
		}
		return result;
	}

	public QuotationVO buscaCotizacion(int quotationId) {
		QuotationVO quotationVO = null;
		LOG.trace("Entrando a  " + BUSCACOTIZACION + ".... ID="
				+ quotationId);
		try {
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter
					.addValue("pId_cotizacion", quotationId)
					.addValue("pRfc", null)
					.addValue("pNombre", null)
					.addValue("pap_pat", null)
					.addValue("pap_mat", null)
					.addValue("pFecNac", null)
					.addValue("pUsuario", null);
			Map<String, Object> execute = this.buscaCotizacion
					.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Codigo = (String) execute.get("pDesc_Codigo");
			@SuppressWarnings("unchecked")
			List<QuotationVO> result = (List<QuotationVO>) execute
					.get("pResult_Cursor");
			quotationVO = result.get(0);
			if (idCodResp.compareTo(BigDecimal.ONE) != 0) {
				throw new ApplicationException(
						(String) execute.get("pDesc_Codigo"));
			}
			LOG.info("Se ha encontrado la cotizacion asegurado..."+ pDesc_Codigo);
		} catch (Exception e) {
			LOG.error("Excepcion general en " + BUSCACOTIZACION
					+ ".finding...", e);
		}
		return quotationVO;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void cambioFormaPago(CrearCotizacionVidaParameterDTO param) {
		LOG.trace("Entrando a  " + CAMBIOFORMAPAGO + ".... ID="
				+ param.getIdCotizacion());

		try {
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter
					.addValue("pId_cotizacion", param.getIdCotizacion())
					.addValue("pId_forma_pago", param.getIdFormaPago());
			Map<String, Object> execute = this.cambioFormaPago
					.execute(sqlParameter);
			BigDecimal idCodResp = (BigDecimal) execute.get("pId_Cod_Resp");
			String pDesc_Codigo = (String) execute.get("pDesc_Codigo");
			
			if (idCodResp.compareTo(BigDecimal.ONE) != 0) {
				throw new ApplicationException(
						(String) execute.get("pDesc_Codigo"));
			}
			
			LOG.trace("Se ha cambiado la forma de pago..."
							+ pDesc_Codigo );
		} catch (Exception e) {
			LOG.error("Excepcion general en " + CAMBIOFORMAPAGO
					+ ".finding...", e);
		}
	}

	/***** MOSTRAR GIRO OCUPACION *****/
	private static final RowMapper<OccupationDTO> giroOcupacionMapper = new RowMapper<OccupationDTO>() {

		@Override
		public OccupationDTO mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			OccupationDTO response = new OccupationDTO();
			response.setIdGiroOcupacion(rs.getLong("ID_GIRO_OCUP"));
			response.setGiroOcupacion(rs.getString("DESC_GIRO_OCUP"));
			return response;
		}
	};

	public OccupationDTO getOcupacionPorId(Long id) {
		String sql = "SELECT GIROOCUPACION.ID_GIRO_OCUP IDGIROOCUPACION, "
				+ "GIROOCUPACION.DESC_GIRO_OCUP OCUPACION "
				+ "FROM SEYCOS.ING_GIRO_OCUP GIROOCUPACION "
				+ "WHERE GIROOCUPACION.ID_GIRO_OCUP = ?";

		return jdbcTemplate.queryForObject(sql, new Object[] { id },
				new RowMapper<OccupationDTO>() {

					@Override
					public OccupationDTO mapRow(ResultSet rs, int rowNum)
							throws SQLException {
						OccupationDTO ocupacionDTO = new OccupationDTO();
						ocupacionDTO.setIdGiroOcupacion(rs
								.getLong("IDGIROOCUPACION"));
						ocupacionDTO.setGiroOcupacion(rs.getString("OCUPACION"));

						return ocupacionDTO;
					}
				});
	}

	public List<OccupationDTO> getGiroOcupacion(OccupationDTO filter) {
		ErrorBuilder eb = new ErrorBuilder();
		String sql = "SELECT GIROOCUPACION.ID_GIRO_OCUP IDGIROOCUPACION, "
				+ " GIROOCUPACION.DESC_GIRO_OCUP OCUPACION "
				+ " FROM SEYCOS.ING_GIRO_OCUP GIROOCUPACION  "
				+ " WHERE GIROOCUPACION.DESC_GIRO_OCUP LIKE ? ";
		String ocupacion = filter.getGiroOcupacion().trim().toUpperCase();
		List<OccupationDTO> Occupation = null;
		if (ocupacion != null) {
			if (ocupacion.length() >= 3) {
				Occupation = jdbcTemplate.query(sql, new Object[] { "%"
						+ ocupacion + "%" }, new RowMapper<OccupationDTO>() {

					public OccupationDTO mapRow(ResultSet rs, int i)
							throws SQLException {

						OccupationDTO occupationDTO = new OccupationDTO();
						occupationDTO.setIdGiroOcupacion(rs
								.getLong("IDGIROOCUPACION"));
						occupationDTO.setGiroOcupacion(rs
								.getString("OCUPACION"));

						return occupationDTO;
					}
				});
			}
			else{
				throw new ApplicationException(eb.addFieldError("ocupacion",
				"Es necesario capturar mas de tres letras."));
			}
		}

		return Occupation;
	}

	public List<SumaAseguradaDTO> obtenerSumaAseguradaList() {
		List<SumaAseguradaDTO> listaSumaAseguradaDTO = new ArrayList<SumaAseguradaDTO>();
		List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = null;
		try {
			catalogoValorFijoDTOs = beanRemoto.findByProperty(
					"id.idGrupoValores",
					Integer.valueOf(ID_MOSTRAR_SUMA_ASEGURADA_MOVIL));
			if (!catalogoValorFijoDTOs.isEmpty()
					&& catalogoValorFijoDTOs != null) {
				for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
					SumaAseguradaDTO sumaAseguradaDTO = new SumaAseguradaDTO();
					sumaAseguradaDTO.setSumaAsegurada(estatus.getDescripcion());
					sumaAseguradaDTO.setIdSumaAsegurada(new Long(estatus
							.getId().getIdDato()));
					listaSumaAseguradaDTO.add(sumaAseguradaDTO);
				}
			}
		} catch (Exception e) {
			LOG.error("Excepcion general en obtenerSumaAseguradaList...", e);
		}
		return listaSumaAseguradaDTO;
	}
	
	private BigDecimal getNumeroCotizacion(BigDecimal id) {
        BigDecimal numeroCotizacion;
        LOG.trace(">>>finding getNumeroCotizacion =>"+id);
        try {
              final StringBuilder sb = new StringBuilder(200);
              sb.append(" SELECT ppol.num_cotizacion ");
              sb.append(" FROM seycos.pol_poliza ppol ");
              sb.append(" WHERE ppol.id_cotizacion = ?1 ");
     
              Query query = entityManager.createNativeQuery(sb.toString());
              query.setParameter(1, id);
     
              numeroCotizacion = (BigDecimal) query.getSingleResult();
              LOG.info("getNumeroCotizacion . numeroCotizacion =>" + numeroCotizacion);
              return numeroCotizacion;
        } catch (RuntimeException re) {
              LOG.error("find getNumeroCotizacion failed", re);
              return null;            
        }
  }


	public static Object regresaObjectEsNuloParametro(Object parametro,
			Object retorno) {
		if (parametro == null)
			return retorno;
		else
			return parametro;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1, Date arg2) {
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, Date arg2, String... arg3) {
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> arg0, String arg1, Map<String, Object> arg2,
			Map<String, Object> arg3, StringBuilder arg4, StringBuilder arg5) {
		return null;
	}

	@Override
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> arg0, Map<String, Object> arg1, Map<String, Object> arg2,
			StringBuilder arg3, StringBuilder arg4) {
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		return null;
	}

	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> arg0,
			Map<String, Object> arg1) {
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		return null;
	}
}