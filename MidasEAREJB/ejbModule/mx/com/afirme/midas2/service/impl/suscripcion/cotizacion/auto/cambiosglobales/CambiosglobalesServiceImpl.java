package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cambiosglobales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cambiosglobales.IncisoAutoDao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cambiosglobales.ConfiguracionPlantillaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cambiosglobales.CambiosglobalesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;

@Stateless
public class CambiosglobalesServiceImpl implements CambiosglobalesService  {
	public EntidadDao entidadDao;
	protected IncisoAutoDao incisoAutoDao;
	protected ConfiguracionPlantillaService configuracionPlantillaService;
	protected EntidadService entidadService;
	protected CoberturaService coberturaService;
	protected CotizacionFacadeRemote cotizacionFacadeRemote;
	protected IncisoService incisoService;	
	protected CalculoService calculoService;
	
	//Temp para poder solucionar problema de lazy list y session null.
	@PersistenceContext
	protected EntityManager entityManager;

	@Override
	public CoberturaCotizacionDTO saveCoberturtas(CoberturaCotizacionDTO coberturas) {
		CoberturaSeccionDTO coberturaSeccionDTO = new CoberturaSeccionDTO();
		CoberturaCotizacionId coberturaCotizacionId = new CoberturaCotizacionId();
		coberturas.setId(coberturaCotizacionId);
		coberturas.setCoberturaSeccionDTO(coberturaSeccionDTO);
		entidadDao.update(coberturas);
		return coberturas;
	}

	@Override
	public List<Paquete> getPaquete(BigDecimal idToCotizacion) {
		return incisoAutoDao.getPaquete(idToCotizacion);
	}

	@Override
	public List<CoberturaCotizacionDTO> obtenerCoberturasPlantilla(Long idToNegPaqueteSeccion,BigDecimal idToCotizacion) {
		NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);
		CotizacionDTO cotizacion = cotizacionFacadeRemote.findById(idToCotizacion);
		List<CoberturaCotizacionDTO> coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>(1);
		
		coberturaCotizacionList= coberturaService.getCoberturas(negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
				negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(), 
				negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
				negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
				negocioPaqueteSeccion.getPaquete().getId(),idToCotizacion,cotizacion.getIdMoneda().shortValue());
		
		
		return coberturaCotizacionList;
	}
	
	@Override
	public void getCoberturas(List<CoberturaCotizacionDTO> coberturaCotizacionList,Long plantillaId,CoberturaCotizacionDTO coberturaCotizacionDTO){
		
	    for(CoberturaCotizacionDTO item : coberturaCotizacionList){
	        if(item.getClaveCobertura()==true){
	           configuracionPlantillaService.agregarCobertura(plantillaId,item.getId().getIdToCobertura(),item.getClaveContratoBoolean(),
	        		   item.getValorSumaAsegurada(),(item.getClaveTipoDeducible() != null  && item.getClaveTipoDeducible().intValue() == CoberturaDTO.CLAVE_TIPO_DEDUCIBLE_PCTE) ? 
	        				   item.getPorcentajeDeducible() : item.getValorDeducible());
	      	}
	    }
	}
	
	@Override	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void reestablecerValoresDelNegocio(BigDecimal idCotizacion,
			BigDecimal negocioSeccionId, Long negocioPaqueteId) {
		List<CoberturaCotizacionDTO> nuevasCoberturas = null;
		configuracionPlantillaService.reestablecerValoresDelNegocio(idCotizacion, negocioSeccionId, negocioPaqueteId);			
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);		
		for(IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()){	
			if(negocioSeccionId.longValue() == inciso.getIncisoAutoCot().getNegocioSeccionId().longValue() && 
					negocioPaqueteId.longValue() == inciso.getIncisoAutoCot().getNegocioPaqueteId().longValue()){			
				nuevasCoberturas = incisoService.obtenerCoberturasParaLaConfiguracion(idCotizacion, 
						inciso.getId().getNumeroInciso(), negocioPaqueteId);				 				
				incisoService.prepareGuardarInciso(idCotizacion, inciso.getIncisoAutoCot(), inciso, nuevasCoberturas);				
				calculoService.calcular(inciso);
			}
		}		
	}
	

	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@EJB
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}	
	
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}


	@EJB
	public void setIncisoAutoDao(IncisoAutoDao incisoAutoDao) {
		this.incisoAutoDao = incisoAutoDao;
	}

	@EJB
	public void setConfiguracionPlantillaService(
			ConfiguracionPlantillaService configuracionPlantillaService) {
		this.configuracionPlantillaService = configuracionPlantillaService;
	}


	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}

	@EJB
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}

	

}
