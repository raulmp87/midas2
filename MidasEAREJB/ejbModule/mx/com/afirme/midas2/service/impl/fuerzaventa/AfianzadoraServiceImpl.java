package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.AfianzadoraDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Afianzadora;
import mx.com.afirme.midas2.service.fuerzaventa.AfianzadoraService;

@Stateless
public class AfianzadoraServiceImpl implements AfianzadoraService{
	private AfianzadoraDao afianzadoraDAO;

	@EJB
	public void setAfianzadoraDAO(AfianzadoraDao afianzadoraDAO) {
		this.afianzadoraDAO = afianzadoraDAO;
	}

	@Override
	public Afianzadora saveFull(Afianzadora arg0) throws Exception {
		return afianzadoraDAO.saveFull(arg0);
	}

	@Override
	public void unsubscribe(Afianzadora arg0) throws Exception {
		afianzadoraDAO.unsubscribe(arg0);
	}

	@Override
	public List<Afianzadora> findByFilters(Afianzadora arg0) {
		return afianzadoraDAO.findByFilters(arg0, null);
	}
	@Override
	public List<Afianzadora> findByFiltersView(Afianzadora filtroAfianzadora){
		return afianzadoraDAO.findByFiltersView(filtroAfianzadora);
	}
	@Override
	public Afianzadora loadById(Afianzadora entidad){
		return afianzadoraDAO.loadById(entidad, null);
	}
	
	@Override
	public Afianzadora loadById(Afianzadora afianzadora, String fechaHistorico)
	{
		return afianzadoraDAO.loadById(afianzadora, fechaHistorico);
	}
}
