package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.agente.AgenteDTO;
import mx.com.afirme.midas.interfaz.agente.AgenteFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.negocio.NegocioDao;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;
import mx.com.afirme.midas2.service.negocio.agente.NegocioAgenteService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class AgenteServiceImpl extends FuerzaDeVentaServiceImpl implements
		AgenteService {
	
	private static String[] CEDULA_DUMMY = {"A", "A2", "B", "B1", "C"};
	
	AgenteFacadeRemote agenteFacadeRemote;
		
	NegocioDao negocioDAO;
	
	NegocioAgenteService negocioAgenteService;
	
	UsuarioService usuarioService;
	
	
	@EJB(beanName = "UsuarioServiceDelegate") 
	public void setUsuarioService(
			UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}	
	@EJB	
	public void setNegocioAgenteService(NegocioAgenteService negocioAgenteService) {
		this.negocioAgenteService = negocioAgenteService;
	}

	@EJB
	public void setNegocioDAO(NegocioDao negocioDAO) {
		this.negocioDAO = negocioDAO;
	}

	@EJB
	public void setAgenteFacadeRemote(AgenteFacadeRemote agenteFacadeRemote) {
		this.agenteFacadeRemote = agenteFacadeRemote;
	}

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarAgentesPorPromotoria(Object id) {
		return super.listar(id, 
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.PROMOTORIA.obtenerTipo(),
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.AGENTE.obtenerTipo());
	}

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarAgentesPorGerencia(Object id) {
		return super.listar(id,
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.GERENCIA.obtenerTipo(),
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.AGENTE.obtenerTipo());
	}

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarAgentesPorOficina(Object id) {
		return super.listar(id, 
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.OFICINA.obtenerTipo(),
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.AGENTE.obtenerTipo());
	}

	@Override
	public List<AgenteDTO> listarAgentes() {		
		List<AgenteDTO> agentes;
		try {
			Usuario usuario = usuarioService.getUsuarioActual();
			if(usuario == null){
				agentes = agenteFacadeRemote.listarAgentes("", CEDULA_DUMMY[0]);
			}else{
				agentes = agenteFacadeRemote.listarAgentes(usuario.getNombreUsuario(), CEDULA_DUMMY[0]);
			}
			
		} catch (Exception e) {
			throw new RuntimeException("No se pudo listar los agentes");
		}		
		return agentes;
	}		

	@Override
	public List<AgenteDTO> listarAgentes(Object centroOperacionId, Object gerenciaId, Object oficinaId,
			Object promotoriaId) {
		List<AgenteDTO> agentes = new ArrayList<AgenteDTO>();
		AgenteDTO agente = null;
		List<RegistroFuerzaDeVentaDTO> fuerzaDeVenta = 
			super.listar(centroOperacionId, gerenciaId , oficinaId, promotoriaId, 
					FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.AGENTE.obtenerTipo());
		for(RegistroFuerzaDeVentaDTO registro : fuerzaDeVenta){
			agente = new AgenteDTO();
			agente.setIdTcAgente(Integer.valueOf(registro.getId().toString()));
			agente.setNombre(registro.getDescription());
			agentes.add(agente);
		}
		return agentes;
	}

	@Override
	public AgenteDTO obtenerAgente(Integer idTcAgente) {
		AgenteDTO agente = null;
		try{
			Usuario usuario = usuarioService.getUsuarioActual();
			if(usuario == null){
				agente = agenteFacadeRemote.findById(idTcAgente,"");
			}else{
				agente = agenteFacadeRemote.findById(idTcAgente,usuario.getNombreUsuario());	
			}
			
		}catch(Exception ex){
			throw new RuntimeException("Imposible obtener agente", ex);
		}
		return agente;
	}

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarAgentesPorCentroEmisor(
			Object id) {
		return super.listar(id, 
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.CENTRO_EMISOR.obtenerTipo(),
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.AGENTE.obtenerTipo());
	}
	
	public List<AgenteDTO> listarAgentes(String nombreUsuario){
		List<AgenteDTO> agentes;
		try {
			agentes = agenteFacadeRemote.listarAgentes(nombreUsuario, CEDULA_DUMMY[0]);
		} catch (Exception e) {
			throw new RuntimeException("No se pudo listar los agentes");
		}		
		return agentes;
	}
}
