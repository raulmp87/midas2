package mx.com.afirme.midas2.service.impl.siniestros.cabina.notificaciones;


import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.siniestros.cabina.notificaciones.ConfiguracionNotificacionesDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.MovimientoProcesoCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ProcesoCabina;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.AdjuntoConfigNotificacionCabinaDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.notificaciones.ConfiguracionNotificacionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.notificaciones.ConfiguracionNotificacionesService;

@Stateless
public class ConfiguracionNotificacionesServiceImpl implements ConfiguracionNotificacionesService{
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private ConfiguracionNotificacionesDao notifDao;
	
	@Override
	public void agregarAdjunto(ConfiguracionNotificacionCabina configuracion,
			AdjuntoConfigNotificacionCabina adjunto) {
		entidadService.save(adjunto);
		configuracion.getAdjuntosNotificacion().add(adjunto);
	}

	@Override
	public void borrarAdjunto(ConfiguracionNotificacionCabina configuracion,
			AdjuntoConfigNotificacionCabina adjunto) {
		entidadService.remove(adjunto);
		
	}

	@Override
	public void eliminarConfiguracion(ConfiguracionNotificacionCabina configuracion) {
		ConfiguracionNotificacionCabina config = obtenerConfiguracion(configuracion.getId());
		//se cambia a un borrado logico, por si existen envios previos en el log
		config.setEsActivo(Integer.valueOf(0));
		config.setInvisible(Boolean.TRUE);
		entidadService.save(config);		
	}

	@Override
	public void guardarActualizarConfiguracion(
			ConfiguracionNotificacionCabina configuracion) {
		if(configuracion.getId() == null || configuracion.getId() == 0){
			configuracion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			configuracion.setFechaCreacion(new Date());
		}
		notifDao.guardarConfiguracion(configuracion);
	}
	
	@Override
	public void actualizarEstatusConfiguracion(ConfiguracionNotificacionCabina configuracion){
		Integer estatus = configuracion.getEsActivo();
		ConfiguracionNotificacionCabina config = obtenerConfiguracion(configuracion.getId());
		config.setEsActivo(estatus);
		entidadService.save(config);
	}

	@Override
	public ConfiguracionNotificacionCabina obtenerConfiguracion(Long configId) {
		return notifDao.obtenerConfiguracion(configId);
	}

	@Override
	public List<ConfiguracionNotificacionCabina> obtenerConfiguraciones() {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("invisible", Boolean.FALSE);		
		return entidadService.findByPropertiesWithOrder(ConfiguracionNotificacionCabina.class, params, "oficina.claveOficina, model.fechaCreacion");
	}

	@Override
	public List<ConfiguracionNotificacionCabina> obtenerConfiguracionesPorFiltros(
			ConfiguracionNotificacionDTO configuracionDTO) {
		return notifDao.obtenerConfiguracionesPorFiltros(configuracionDTO);
	}
	
	

	@Override
	public Map<String, String> obtenerMovimientos(
			Long procesoId) {
		Map<String, Object> params = new HashMap<String, Object>();
		Map<String, String> map = new LinkedHashMap<String, String>();
		params.put("procesoCabina.id", procesoId);
		params.put("activo", Boolean.TRUE);
		List<MovimientoProcesoCabina> movs = entidadService.findByPropertiesWithOrder(MovimientoProcesoCabina.class, params, "descripcion");
		for(MovimientoProcesoCabina mov : movs){
			map.put(String.format("%04d", mov.getId()), mov.getDescripcion());//El 0 es para parche para ordenamiento DWR
		}
		return map;
	}
	
	@Override
	public MovimientoProcesoCabina obtenerMovimiento(String codigo){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("codigo", codigo);
		List<MovimientoProcesoCabina> movs = entidadService.findByProperties(MovimientoProcesoCabina.class, params);
		if(movs != null && movs.size() > 0){
			return movs.get(0);
		}
		return null;
	}
	
	public Boolean movimientoSinOficina(String codigo){
		MovimientoProcesoCabina mov = obtenerMovimiento(codigo);
		if(mov != null && mov.getSinOficina() != null){
			return mov.getSinOficina().booleanValue();
		}
		return Boolean.FALSE;
	}

	@Override
	@SuppressWarnings("unchecked")
	public Map<Long, String> obtenerProcesos() {		
		List<ProcesoCabina> procesos = entidadService.executeQueryMultipleResult(
				"select model from ProcesoCabina model where model.visible = true order by model.descripcion asc", null);
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		for(ProcesoCabina proc : procesos){
			map.put(proc.getId(), proc.getDescripcion());
		}
		return map;
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.notificaciones.ConfiguracionNotificacionesService#obtenerAdjuntos(mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina)
	 */
	@Override
	public List<AdjuntoConfigNotificacionCabina> obtenerAdjuntos(
			ConfiguracionNotificacionCabina confNotificacionCabina) {
		return entidadService.findByProperty(AdjuntoConfigNotificacionCabina.class, "confNotificacionCabina.id", confNotificacionCabina.getId()); 
	}
	
	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.notificaciones.ConfiguracionNotificacionesService#obtenerAdjunto(java.lang.Long)
	 */
	@Override
	public AdjuntoConfigNotificacionCabina obtenerAdjunto (Long id){
		return entidadService.findById(AdjuntoConfigNotificacionCabina.class, id);
	}
	
	public List<AdjuntoConfigNotificacionCabinaDTO> obtenerAdjuntosSimpleList(ConfiguracionNotificacionCabina config){
		return notifDao.obtenerAdjuntosSimpleList(config);
	}

}
