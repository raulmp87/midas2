package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.emision;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.sql.Connection;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisFacadeRemote;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilDTO;
import mx.com.afirme.midas.catalogos.estadocivil.EstadoCivilFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.sector.SectorDTO;
import mx.com.afirme.midas.catalogos.sector.SectorFacadeRemote;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.moneda.MonedaTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.entorno.Entorno;
import mx.com.afirme.midas.sistema.reporte.MidasBaseReporte;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.compensaciones.CaParametrosDao;
import mx.com.afirme.midas2.dao.compensaciones.reporteBaseCompensacionesDao.ReporteBaseCompensacionesDao;
import mx.com.afirme.midas2.dao.fuerzaventa.AgenteMidasDao;
import mx.com.afirme.midas2.dao.impuestosestatales.RetencionImpuestosDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.emision.BasesEmisionDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.domain.cargos.DetalleCargos;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoActualizacionAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.dto.reportesAgente.RetencionImpuestosReporteView;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaSegurosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaPagosSaldosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesCompDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesCompDTO.DatosReporteCompParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO.DatosReporteEstadoCuentaParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDerPolDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDerPolDTO.DatosRepDerPolParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagDevengarDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagDevengarDTO.DatosReportePagDevDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagoCompContra;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesPagoCompContra.DatosReportePagoCompContraParametrosDTO;
import mx.com.afirme.midas2.domain.compensaciones.CaReportesDTO.DatosReporteEstadoCuentaParametrosDTO;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.bonocomision.NegocioBonoComision;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacion;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutProgPago;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutRecibo;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica.Status;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica.TipoVigencia;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDesc;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.domain.prestamos.PagarePrestamoAnticipo;
import mx.com.afirme.midas2.domain.provisiones.ProvisionImportesRamo;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReporteDetalleProvisionView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReporteProvisionView;
import mx.com.afirme.midas2.dto.impresiones.ContenedorDatosImpresion;
import mx.com.afirme.midas2.dto.impresiones.DatosAgrupadorTarifaNegocioDetalleDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosBasesEmisionDTO.DatosBasesEmisionParametrosDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaCotizacion;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaPoliza;
import mx.com.afirme.midas2.dto.impresiones.DatosCobPaqueteNegocioDetalleDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDeNegocioDetalleDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosContrato;
import mx.com.afirme.midas2.dto.impresiones.DatosCotizacionDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosDesglosePagosDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosIncisoDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasNegocioDetalleDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPaquetesNegocioDetalleDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosPolizaDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosProductosDeNegocioDetalleDTO;
import mx.com.afirme.midas2.dto.impresiones.PagarePrestamoAnticipoImpresion;
import mx.com.afirme.midas2.dto.impresiones.ReporteNegCompensacionesDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.prestamos.ReporteIngresosAgente;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteBonosGerenciaView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteContabilidadMMView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteDatosView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteEstatusEntregaFactura;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteIngresosView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentePrimaEmitidaVsPriamaPagadaDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentePrimaPagadaPorRamoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteTopAgenteView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentesDetalleBonoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosAgentesReporteProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.DatosDetPrimas;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteAcumuladoView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteMovimientosView;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.DatosEdoctaAgenteSiniestralidad;
import mx.com.afirme.midas2.dto.reportesAgente.DatosHonorariosAgentes;
import mx.com.afirme.midas2.dto.reportesAgente.DatosReporteCalculoBonoMensualDTO;
import mx.com.afirme.midas2.dto.reportesAgente.DatosReportePreviewBonos;
import mx.com.afirme.midas2.dto.reportesAgente.DatosVentasProduccion;
import mx.com.afirme.midas2.dto.reportesAgente.ReporteGlobalBonoYComisionesDTO;
import mx.com.afirme.midas2.dto.reportesAgente.SaldosVsContabilidadView;
import mx.com.afirme.midas2.dto.sapamis.alertas.SapAlertasistemasEnvio;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraEmision;
import mx.com.afirme.midas2.dto.sapamis.otros.SapAmisBitacoraSiniestros;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosGeneralDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.cargos.ConfigCargosService;
import mx.com.afirme.midas2.service.cargos.DetalleCargosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.RepSaldoVSContService;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.calculo.CalculoServiceImpl;
import mx.com.afirme.midas2.service.impuestosestatales.RetencionImpuestosService;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.agente.NegocioAgenteService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducible.NegocioDeduciblesService;
import mx.com.afirme.midas2.service.operacionessapamis.sapamisbitacoras.SapAmisBitacoraService;
import mx.com.afirme.midas2.service.prestamos.ConfigPrestamoAnticipoService;
import mx.com.afirme.midas2.service.prestamos.PagarePrestamoAnticipoService;
import mx.com.afirme.midas2.service.reportes.MidasBaseReporteService;
import mx.com.afirme.midas2.service.reportes.MidasBaseReporte.MidasBaseReporteExcel;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoService;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaSeccionService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;
import net.sf.jasperreports.engine.xml.JRXmlLoader;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.dbutils.DbUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;

import com.js.util.StringUtil;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;
@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class ImpresionesServiceImpl implements ImpresionesService {
	
	
	public static final Logger LOG = Logger.getLogger(ImpresionesServiceImpl.class);
	public final static String LOGIC_EQUAL = "=";
	public final static String LOGIC_GREATER_THAN = ">";
	public final static String LOGIC_LESS_THAN = "<";
	public final static String LOGIC_ASHIGHER = ">=";
	public final static String LOGIC_ASMINOR = "<=";
	private static final long serialVersionUID = -1L;
	public final static String TIPO_VALOR_SA_DSMGVDF = "DSMGVDF";
	public final static String TIPO_VALOR_SA_UMA = "UMA";
	
	protected EstadoFacadeRemote estadoFacadeRemote;
	protected MunicipioFacadeRemote municipioFacadeRemote;
	protected PaisFacadeRemote paisFacadeRemote;
	protected EstadoCivilFacadeRemote estadoCivilFacadeRemote;
	protected ClienteFacadeRemote clienteFacadeRemote;
	protected DomicilioFacadeRemote domicilioFacadeRemote;
	
	private CoberturaService coberturaService;	
	private CotizacionService cotizacionService;	
	private CalculoService calculoService;	
	private ConfiguracionDatoIncisoService configuracionDatoIncisoService;	
	private EntidadService entidadService;
	private PersonaSeycosFacadeRemote personaSeycosFacadeRemote;
	private AgenteMidasService agenteMidasService;
	private ListadoService listadoService;
	private UsuarioService usuarioService;
	private PagarePrestamoAnticipoService pagarePrestamoAnticipoService;
	private ConfigPrestamoAnticipoService configPrestamoAnticipoService;
	private DetalleCargosService detalleCargosService;
	private ConfigCargosService configCargosService;
	private List<CatalogoValorFijoDTO> posiblesDeducibles;
	private RepSaldoVSContService repSaldoVSContService; 
	private SapAmisBitacoraService sapAmisBitacoraService;
	private CaParametrosDao caParametrosDao;
	private List<ReporteNegCompensacionesDTO> lista_caParametros;
	private List<ReporteNegCompensacionesDTO> lista_caParametrosBS;
	public static final String IMGHEADER = "imgHeader";
	private AgenteMidasDao agenteMidasDao;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	private void getContenedorDatosImpresion(ContenedorDatosImpresion datosImpresion, 
			CotizacionDTO cotizacion, boolean esCaratula, int incisoInicial, int incisoFinal){
		Integer idFormaPago = cotizacion.getIdFormaPago().intValue();
		Short idMoneda = cotizacion.getIdMoneda().shortValue();		
		BigDecimal idDomicilioContratante = cotizacion.getIdDomicilioContratante();
		BigDecimal idContratante = cotizacion.getIdToPersonaContratante();
		if(datosImpresion.getFormaPago() == null){
			datosImpresion.setFormaPago(entidadService.findById(FormaPagoDTO.class, idFormaPago));
		}
		if(datosImpresion.getMoneda() == null){
			datosImpresion.setMoneda(entidadService.findById(MonedaDTO.class, idMoneda));
		}

		datosImpresion.setCliente(getClienteSinDireccion(datosImpresion.getCliente(), idContratante));
		
		datosImpresion.setDomicilioCliente(getClienteImpresion(datosImpresion.getCliente(), datosImpresion.getDomicilioCliente(),
				idDomicilioContratante,idContratante));
		if(datosImpresion.getResumenCostosDTO() == null && esCaratula){
			datosImpresion.setResumenCostosDTO(calculoService.obtenerResumenGeneral(cotizacion, true));
		}
		//if(datosImpresion.getIncisosCotizacionList().isEmpty()){
			/*datosImpresion.setIncisosCotizacionList(entidadService.findById(
					CotizacionDTO.class, cotizacion.getIdToCotizacion()).getIncisoCotizacionDTOs());	//lazy en qa
					*/
			if(incisoInicial > 0 && incisoFinal > 0){
				datosImpresion.setIncisosCotizacionList(incisoService.getIncisosRango(cotizacion.getIdToCotizacion(), new BigDecimal(incisoInicial), new BigDecimal(incisoFinal)));
			}else{
				datosImpresion.setIncisosCotizacionList(incisoService.getIncisosRango(cotizacion.getIdToCotizacion(), null, null));
			}			
			//populateIncisos(datosImpresion.getIncisosCotizacionList());
		//}
	}	
	
	@Override
	public DatosCaratulaPoliza llenarPolizaDTO(BigDecimal idToPoliza, int incisoInicial, int incisoFinal) {
		DatosPolizaDTO datosPolizaDTO = new DatosPolizaDTO();	
		ResumenCostosGeneralDTO resumen = null;
		PolizaDTO polizaDTO = new PolizaDTO();					
		DatosCaratulaPoliza datosCaratulaPoliza = new DatosCaratulaPoliza();
		datosCaratulaPoliza.setContenedorDatosImpresion(new ContenedorDatosImpresion()); 
		polizaDTO = entidadService.findById(PolizaDTO.class, idToPoliza);	
		getContenedorDatosImpresion(datosCaratulaPoliza.getContenedorDatosImpresion(), polizaDTO.getCotizacionDTO(), true, incisoInicial, incisoFinal);
		resumen = datosCaratulaPoliza.getContenedorDatosImpresion().getResumenCostosDTO();
		datosPolizaDTO.setNumeroPoliza(polizaDTO.getNumeroPolizaFormateada());		
		datosPolizaDTO.setFechaInicioVigencia(polizaDTO.getCotizacionDTO().getFechaInicioVigencia());
		datosPolizaDTO.setFechaFinVigencia(polizaDTO.getCotizacionDTO().getFechaFinVigencia());
		datosPolizaDTO.setPrimaNetaCotizacion(resumen.getCaratula().getPrimaNetaCoberturas());
		datosPolizaDTO.setDerechosPoliza(resumen.getCaratula().getDerechos());
		datosPolizaDTO.setPrimaNetaTotal(resumen.getCaratula().getPrimaTotal());
		datosPolizaDTO.setMontoRecargoPagoFraccionado(resumen.getCaratula().getRecargo());
		datosPolizaDTO.setMontoIVA(resumen.getCaratula().getIva());
		datosPolizaDTO.setIdToPersonaContratante(polizaDTO.getCotizacionDTO().getIdToPersonaContratante());		
		datosPolizaDTO.setDescripcionMoneda(datosCaratulaPoliza.getContenedorDatosImpresion().getMoneda().getDescripcion());
		datosPolizaDTO.setDescripcionFormaPago(datosCaratulaPoliza.getContenedorDatosImpresion().getFormaPago().getDescripcion());
		datosPolizaDTO.setNombreContratante(polizaDTO.getCotizacionDTO().getNombreContratante().toUpperCase());
		datosPolizaDTO.setIdToPersonaContratante(polizaDTO.getCotizacionDTO().getIdToPersonaContratante());
		StringBuilder domicilio = new StringBuilder("");
		if(datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente()!= null){
			domicilio.append(!StringUtil.isEmpty(datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente().getCalleNumero())? datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente().getCalleNumero() : "");
			domicilio.append(!StringUtil.isEmpty(datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente().getNombreColonia()) ? " " + datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente().getNombreColonia() : "");
			domicilio.append(!StringUtil.isEmpty(datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente().getEstado()) ? ", " + datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente().getEstado() : "");
			domicilio.append(!StringUtil.isEmpty(datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente().getCiudad()) ? ", " + datosCaratulaPoliza.getContenedorDatosImpresion().getDomicilioCliente().getCiudad() : "");			
		}
		if(datosCaratulaPoliza.getContenedorDatosImpresion().getCliente() != null){			
			datosPolizaDTO.setRfcContratante(!StringUtil.isEmpty(datosCaratulaPoliza.getContenedorDatosImpresion().getCliente().getCodigoRFC())?datosCaratulaPoliza.getContenedorDatosImpresion().getCliente().getCodigoRFC():"");
		}
		datosPolizaDTO.setTxDomicilio(domicilio.toString());
		datosPolizaDTO.setTitulo("P\u00D3LIZA DE SEGUROS PARA "+ polizaDTO.getCotizacionDTO().getTipoPolizaDTO().getDescripcion());
		datosPolizaDTO.setObservaciones(!StringUtil.isEmpty(datosPolizaDTO.getObservaciones())? datosPolizaDTO.getObservaciones() :"");
		//Identificador
		//Centro Emisor - Gerencia - Oficina - Promotoria - Clave Agente
		String identificador = "";
		if(polizaDTO.getCotizacionDTO().getSolicitudDTO().getCodigoAgente() != null){
			identificador = getIdentificador(polizaDTO.getCotizacionDTO());			
		}
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("ddMMyyyyHHmmss");
		identificador += "-"+formatoDelTexto.format(new Date());
		
		datosPolizaDTO.setIdentificador(identificador);
		datosCaratulaPoliza.setDatosPolizaDTO(datosPolizaDTO);		
		datosCaratulaPoliza.setDatasourceLineasNegocio(llenarDatosLineaNegocio(polizaDTO.getCotizacionDTO().getIdToCotizacion()));
		datosCaratulaPoliza.setPolizaDTO(polizaDTO);
		return datosCaratulaPoliza;
	}

	@SuppressWarnings("unchecked")
	private List<DatosLineasDeNegocioDTO> llenarDatosLineaNegocio(BigDecimal idToCotizacion){
		
		String queryString = "select ROWNUM, resumenlineas.* from (";
		queryString = queryString.concat(" select sum(toseccioncot.valorprimaneta) as impPrimaNeta,");
		queryString = queryString.concat(" COUNT(*) AS unidadesRiesgo, toseccion.descripcionseccion AS lineaNegocio");
		queryString = queryString.concat(" from MIDAS.toincisoautocot");
		queryString = queryString.concat(" LEFT JOIN MIDAS.toseccioncot ON (toseccioncot.IDTOCOTIZACION = toincisoautocot.cotizacion_id");
		queryString = queryString.concat(" AND toseccioncot.numeroinciso =  toincisoautocot.numeroinciso)");
		queryString = queryString.concat(" LEFT JOIN MIDAS.TOSECCION ON (toseccioncot.IDTOSECCION = TOSECCION.IDTOSECCION)");
		queryString = queryString.concat(" where cotizacion_id = ?");
		queryString = queryString.concat(" group by toseccion.descripcionseccion");
		queryString = queryString.concat(" order by toseccion.descripcionseccion)");
		queryString = queryString.concat(" resumenlineas");
		
		Query query =  entityManager.createNativeQuery(queryString, DatosLineasDeNegocioDTO.class);
		query.setParameter(1, idToCotizacion);
		query.setHint(QueryHints.MAINTAIN_CACHE, HintValues.FALSE);	

		return query.getResultList();
	}
	
	@Override
	public List<DatosCaratulaInciso> llenarIncisoDTO(CotizacionDTO cotizacionDTO, String numeroPoliza, ContenedorDatosImpresion contenedorDatosImpresion) {
		return llenarIncisoDTO(cotizacionDTO, numeroPoliza, 0, 0, contenedorDatosImpresion);
	}
	
	@Override
	public List<DatosCaratulaInciso> llenarIncisoDTO(CotizacionDTO cotizacionDTO, String numeroPoliza, int incisoInicial, int incisoFinal, ContenedorDatosImpresion contenedorDatosImpresion){
		String detalleTitulo = "";
		String identificador = "";
		ResumenCostosDTO resumenInciso = null;		
		List<DatosCaratulaInciso> datosCaratulaIncisoList = new ArrayList<DatosCaratulaInciso>(1);
		List<IncisoCotizacionDTO> incisosCotizacionList = null;
		List<CoberturaCotizacionDTO> coberturasInciso = null;
		List<CoberturaCotizacionDTO> coberturasCotizacionList = null;	
		TipoServicioVehiculoDTO tipoServicio = null;
		if(contenedorDatosImpresion == null){
			contenedorDatosImpresion = new ContenedorDatosImpresion();			
		}		
		getContenedorDatosImpresion(contenedorDatosImpresion, cotizacionDTO, false, incisoInicial, incisoFinal);
		
		boolean esServicioPublico = false;
		
		if( cotizacionDTO != null
				&& cotizacionDTO.getSolicitudDTO() != null
				&& cotizacionDTO.getSolicitudDTO().getNegocio() != null
				&& cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio() != null){
			Map<String, Object> spvData = listadoService.getSpvData(cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
			esServicioPublico = (Boolean)spvData.get("esServicioPublico");
		}
		
		//
		// Lista de incisos
		//		
		//if(incisoInicial != 0 && incisoFinal != 0){
		//	incisosCotizacionList = new ArrayList<IncisoCotizacionDTO>(contenedorDatosImpresion.getIncisosCotizacionList().subList(incisoInicial -1, incisoFinal));
		//}else{
			incisosCotizacionList = contenedorDatosImpresion.getIncisosCotizacionList();
		//}
		String subTitulo = "";
		String tipoInciso = "";
		String numeroImpresion = "";
		
		detalleTitulo = "COTIZACI\u00D3N DE SEGURO PARA";
		subTitulo = "INFORMACI\u00D3N GENERAL DE LA COTIZACI\u00D3N";
		tipoInciso = "Cotizaci\u00F3n:";
		numeroImpresion = cotizacionDTO.getNumeroCotizacion();
		
		
		boolean obtenerSoloCoberturasContratadas = true;
		coberturasCotizacionList = 
			coberturaService.getCoberturas(cotizacionDTO.getIdToCotizacion(), obtenerSoloCoberturasContratadas);
		getPosiblesDeduciblesConfig();		
		if(cotizacionDTO.getSolicitudDTO().getCodigoAgente() != null){
			identificador = getIdentificador(cotizacionDTO);
		}
		SimpleDateFormat formatoDelTexto = new SimpleDateFormat("ddMMyyyyHHmmss");
		
		for(IncisoCotizacionDTO incisoCotizacionDTO:incisosCotizacionList){		
			SeccionCotizacionDTO seccionCotizacion = incisoCotizacionDTO.getSeccionCotizacion();
			coberturasInciso = filtrarCoberturasPorSeccionInciso(coberturasCotizacionList, seccionCotizacion.getSeccionDTO().getIdToSeccion(), 
					incisoCotizacionDTO.getId().getNumeroInciso());
			String nombreContratante = null;
			String nombreContratanteInciso = null;	
			boolean imprimirUMA = listadoService.imprimirLeyendaUMA(cotizacionDTO.getFechaInicioVigencia());
			if(incisoCotizacionDTO.getIncisoAutoCot()!=null){
				DatosIncisoDTO dataSourceInciso = new DatosIncisoDTO();
				DatosCaratulaInciso datosCaratulaInciso = new DatosCaratulaInciso();
				dataSourceInciso.setIdInciso(incisoCotizacionDTO.getNumeroSecuencia().toString());
				dataSourceInciso.setNumeroCotizacion(cotizacionDTO.getNumeroCotizacion());
				dataSourceInciso.setIdSeccion(seccionCotizacion.getId().getIdToSeccion().toString());
				dataSourceInciso.setFechaInicioVigencia(cotizacionDTO.getFechaInicioVigencia());
				dataSourceInciso.setFechaFinVigencia(cotizacionDTO.getFechaFinVigencia());
				if(!StringUtil.isEmpty(incisoCotizacionDTO.getIncisoAutoCot().getNombreAsegurado())){
					dataSourceInciso.setNationalUnity(true);
				}
				
				resumenInciso = calculoService.obtenerResumen(incisoCotizacionDTO);
				if(resumenInciso != null){
					dataSourceInciso.setPrimaNetaCotizacion(UtileriasWeb.getDoubleValue(resumenInciso.getPrimaNetaCoberturas()));
					dataSourceInciso.setDerechosPoliza(UtileriasWeb.getDoubleValue(resumenInciso.getDerechos()));
					dataSourceInciso.setIdToPersonaContratante(cotizacionDTO.getIdToPersonaContratante());
					dataSourceInciso.setPrimaNetaTotal(UtileriasWeb.getDoubleValue(resumenInciso.getPrimaTotal()));
					dataSourceInciso.setTotalPrimas(UtileriasWeb.getDoubleValue(resumenInciso.getTotalPrimas()));
					dataSourceInciso.setMontoRecargoPagoFraccionado(UtileriasWeb.getDoubleValue(resumenInciso.getRecargo()));
					dataSourceInciso.setDescuentoComisionCedida(UtileriasWeb.getDoubleValue(resumenInciso.getDescuentoComisionCedida()));
					dataSourceInciso.setMontoIVA(UtileriasWeb.getDoubleValue(resumenInciso.getIva()));
				}

				nombreContratante = getNombreContratante(cotizacionDTO.getNombreContratante());
				nombreContratanteInciso = getNombreContratanteInciso(incisoCotizacionDTO.getIncisoAutoCot().getNombreAsegurado(),
						cotizacionDTO.getNombreContratante(), cotizacionDTO.getIdToPersonaContratante(), 
						incisoCotizacionDTO.getIncisoAutoCot().getPersonaAseguradoId());
				dataSourceInciso.setNombreContratanteInciso(nombreContratanteInciso);
				dataSourceInciso.setNombreContratante(nombreContratante);
				dataSourceInciso.setTitulo(detalleTitulo +" "+seccionCotizacion.getSeccionDTO().getDescripcion().toUpperCase());
				dataSourceInciso.setSubTitulo(subTitulo);
				dataSourceInciso.setTipoInciso(tipoInciso);
				dataSourceInciso.setNumeroPoliza(numeroImpresion);
				dataSourceInciso.setTipoPoliza(cotizacionDTO.getTipoPolizaDTO().getDescripcion());
				
				dataSourceInciso.setIdToPersonaContratante(cotizacionDTO.getIdToPersonaContratante());
				if (contenedorDatosImpresion.getCliente() != null){
					dataSourceInciso.setRfcContratante(contenedorDatosImpresion.getCliente().getCodigoRFC());
				}
				
				if(contenedorDatosImpresion.getDomicilioCliente() != null){
					dataSourceInciso.setCalleYNumero(contenedorDatosImpresion.getDomicilioCliente().getCalleNumero());
					dataSourceInciso.setColonia(contenedorDatosImpresion.getDomicilioCliente().getNombreColonia());
					dataSourceInciso.setCiudad(contenedorDatosImpresion.getDomicilioCliente().getCiudad());
					dataSourceInciso.setEstado(contenedorDatosImpresion.getDomicilioCliente().getEstado());
					dataSourceInciso.setCodigoPostal(contenedorDatosImpresion.getDomicilioCliente().getCodigoPostal());				
				}
				
				dataSourceInciso.setIdClase(incisoCotizacionDTO.getIncisoAutoCot().getClaveTipoBien());			
				dataSourceInciso.setModelo(incisoCotizacionDTO.getIncisoAutoCot().getModeloVehiculo().toString());
				
				dataSourceInciso.setNumeroPlacas(UtileriasWeb.toUpperCase(incisoCotizacionDTO.getIncisoAutoCot().getPlaca()));
				dataSourceInciso.setNumeroMotor(UtileriasWeb.toUpperCase(incisoCotizacionDTO.getIncisoAutoCot().getNumeroMotor()));
				dataSourceInciso.setNumeroSerie(UtileriasWeb.toUpperCase(incisoCotizacionDTO.getIncisoAutoCot().getNumeroSerie()));
				
				EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();			
				estiloVehiculoId.setStrId(incisoCotizacionDTO.getIncisoAutoCot().getEstiloId());
				EstiloVehiculoDTO estiloVehiculoDTO =  entidadService.findById(EstiloVehiculoDTO.class, estiloVehiculoId);			
				dataSourceInciso.setNumeroAsientos(estiloVehiculoDTO.getNumeroAsientos().toString());
				dataSourceInciso.setIdClase(estiloVehiculoId.getClaveEstilo());
				dataSourceInciso.setDescripcionClase(incisoCotizacionDTO.getIncisoAutoCot().getDescripcionFinal());
				
				dataSourceInciso.setTipoServicio(getTipoServicioConfig(incisoCotizacionDTO.getIncisoAutoCot().getTipoServicioId()));
				
				dataSourceInciso.setTipoUso(incisoCotizacionDTO.getIncisoAutoCot().getClaveTipoBien());			
				dataSourceInciso.setTextoCopia("COPIA");
				if(incisoCotizacionDTO.getIncisoAutoCot().getMarcaId()!=null){
					MarcaVehiculoDTO marcaVehiculoDTO= entidadService.findById(MarcaVehiculoDTO.class, incisoCotizacionDTO.getIncisoAutoCot().getMarcaId());
					dataSourceInciso.setDescripcionFormaPago(contenedorDatosImpresion.getFormaPago().getDescripcion());			
					dataSourceInciso.setMarca(marcaVehiculoDTO.getDescription());				
					dataSourceInciso.setDescripcionMoneda(contenedorDatosImpresion.getMoneda().getDescripcion());
				}
				setObservacionesIniciso(incisoCotizacionDTO, dataSourceInciso);
				if(incisoCotizacionDTO.getIncisoAutoCot().getTipoUsoId()!=null){
					TipoUsoVehiculoDTO tipoUsoVehiculoDTO = entidadService.findById(TipoUsoVehiculoDTO.class,BigDecimal.valueOf(incisoCotizacionDTO.getIncisoAutoCot().getTipoUsoId()));
					dataSourceInciso.setTipoUso(tipoUsoVehiculoDTO.getDescription().toUpperCase());	
				}
				dataSourceInciso.setTextoAgregado("");
		
				dataSourceInciso.setIdentificador(identificador + "-"+formatoDelTexto.format(new Date()));
				
				if(incisoCotizacionDTO.getIncisoAutoCot().getCodigoPostal()!=null){				
				dataSourceInciso.setCodigoPostalCirculacion(incisoCotizacionDTO.getIncisoAutoCot().getCodigoPostal().toString());
				}
				
				if (incisoCotizacionDTO.getIncisoAutoCot().getEstadoId()!=null){
					EstadoDTO estado = entidadService.findById(EstadoDTO.class,incisoCotizacionDTO.getIncisoAutoCot().getEstadoId());
					dataSourceInciso.setEdoCirculacion(estado.getStateName());
				
					
					
					//
				}

				setTipoCargaInciso(incisoCotizacionDTO, dataSourceInciso);		
				
				datosCaratulaInciso.setDataSourceIncisos(dataSourceInciso);
				
				//
				ordenaEnSecuencia(coberturasInciso);
				datosCaratulaInciso.setDataSourceCoberturas(llenarCoberturasList(coberturasInciso, estiloVehiculoDTO.getNumeroAsientos(), incisoCotizacionDTO, imprimirUMA, esServicioPublico));
				
				datosCaratulaIncisoList.add(datosCaratulaInciso);
				
			}
		}
		if(!incisosCotizacionList.isEmpty()){
			datosCaratulaIncisoList.get(0).setListadoIncisos(incisosCotizacionList);
		}
		return datosCaratulaIncisoList;	
	}
	
	private void getPosiblesDeduciblesConfig() {
		if(posiblesDeducibles == null){
			posiblesDeducibles = entidadService.findByProperty(CatalogoValorFijoDTO.class, "id.idGrupoValores", 
					CatalogoValorFijoDTO.IDGRUPO_POSIBLES_DEDUCIBLES);
		}
	}

	private String getNombreContratante(String nombreContratanteCot) {
		String nombreContratante = "";
		if (nombreContratanteCot != null) {
				nombreContratante = nombreContratanteCot.toUpperCase();
		}
		return nombreContratante;
	}
	
	private String getNombreContratanteInciso(String nombreAsegurado,
			String nombreContratanteCot, BigDecimal idToPersonaContratante, Long personaAseguradoId) {
		String nombreContratante = getNombreContratante(nombreContratanteCot);
		String nombreContratanteInciso = null;
		if (nombreAsegurado != null) {
			nombreContratanteInciso = "Y/O "+ nombreAsegurado.toUpperCase();
		}

		if (idToPersonaContratante != null && personaAseguradoId != null
				&& personaAseguradoId.equals(idToPersonaContratante.longValue())) {
			nombreContratanteInciso = "";
		} else {
			if (nombreContratanteInciso != null && nombreContratante != null) {
				nombreContratanteInciso = nombreContratanteInciso
								.substring(4).trim()
								.equals(nombreContratante.trim()) ? ""
								: nombreContratanteInciso;
			} else {
				nombreContratanteInciso = "";
			}
		}
		return nombreContratanteInciso;
	}
	
	private void ordenaEnSecuencia(List<CoberturaCotizacionDTO> coberturasInciso) {
		Collections.sort(coberturasInciso, new Comparator<CoberturaCotizacionDTO>(){
			public int compare(CoberturaCotizacionDTO cc1, CoberturaCotizacionDTO cc2){
				return cc1.getId().getIdToCobertura().compareTo(cc2.getId().getIdToCobertura());
			}
		});
	}

	@SuppressWarnings("unchecked")
	private List<CoberturaCotizacionDTO> filtrarCoberturasPorSeccionInciso(List<CoberturaCotizacionDTO> coberturasCotizacionList, 
			final BigDecimal idSeccion, final BigDecimal numInciso){		
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				CoberturaCotizacionDTO cobertura = (CoberturaCotizacionDTO)arg0;				
				return cobertura.getId().getIdToSeccion().longValue() == idSeccion.longValue() && 
					cobertura.getId().getNumeroInciso().longValue() == numInciso.longValue();
			}
		};		
		return (List<CoberturaCotizacionDTO>)CollectionUtils.select(coberturasCotizacionList, predicate);
	}
	
	
	private void setObservacionesIniciso(IncisoCotizacionDTO incisoCotizacionDTO, DatosIncisoDTO datosIncisoDTO){
		 Map<String, String> valores = new LinkedHashMap<String, String>();
		 List<ControlDinamicoRiesgoDTO> controles = configuracionDatoIncisoService.getDatosRiesgo(valores, ConfiguracionDatoInciso.Estatus.TODOS.getEstatus(),incisoCotizacionDTO);
		 StringBuilder stringBuilder = new StringBuilder();
		 for(ControlDinamicoRiesgoDTO controlDinamicoRiesgoDTO: controles){
			 stringBuilder.append(controlDinamicoRiesgoDTO.getEtiqueta().toUpperCase() + " " +controlDinamicoRiesgoDTO.getDescripcionNivel());
			 stringBuilder.append(": ");			 
			 if(controlDinamicoRiesgoDTO.getValor() != null){
				 String valor = controlDinamicoRiesgoDTO.getValor();
				 if(controlDinamicoRiesgoDTO.getLista() != null && !controlDinamicoRiesgoDTO.getLista().isEmpty()){
					 valor = controlDinamicoRiesgoDTO.getLista().get(controlDinamicoRiesgoDTO.getValor());
				 }
				 if(valor == null){
					 valor = "";
				 }
				 if(valor.length() > 300){
					 stringBuilder.append(valor.substring(0,300));
					 stringBuilder = new StringBuilder(stringBuilder.substring(0,stringBuilder.lastIndexOf(" ")));
					 stringBuilder.append("...");
				 }else{
					 stringBuilder.append(valor);
				 }
			 }		
			 stringBuilder.append(System.getProperty("line.separator"));
		 }
		 try{
			 if(StringUtils.isNotBlank( incisoCotizacionDTO.getIncisoAutoCot().getObservacionesinciso())) {
				 stringBuilder.append("OBSERVACIONES: " + incisoCotizacionDTO.getIncisoAutoCot().getObservacionesinciso());
			 }
		 }catch(Exception e){}
		 if(stringBuilder.toString() != null && !stringBuilder.toString().equals("")){
			 datosIncisoDTO.setObservaciones(stringBuilder.toString());
		 }else{
			 datosIncisoDTO.setObservaciones("");
		 }
		 
	}
	private List<DatosCoberturasDTO> llenarCoberturasList(List<CoberturaCotizacionDTO> coberturasList, Short numeroAsientos,  IncisoCotizacionDTO incisoCotizacionDTO, boolean imprimirUMA, boolean esServicioPublico){
		List<DatosCoberturasDTO> dataSourceCoberturas = new ArrayList<DatosCoberturasDTO>(1);
		for(CoberturaCotizacionDTO coberturaCotizacionDTO:coberturasList){
			coberturaCotizacionDTO.setDescripcionDeducible(getDescripcionDeducible(coberturaCotizacionDTO.getClaveTipoDeducible()!=null?
					coberturaCotizacionDTO.getClaveTipoDeducible().toString():null));
			DatosCoberturasDTO datosCoberturasDTO = new DatosCoberturasDTO();
			datosCoberturasDTO.setSumaAsegurada(getSumaAsegurada(coberturaCotizacionDTO, incisoCotizacionDTO, numeroAsientos, esServicioPublico)); 
			datosCoberturasDTO.setDeducible(getDeducibleCobertura(coberturaCotizacionDTO, incisoCotizacionDTO));
			
			datosCoberturasDTO.setRiesgo(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getNumeroSecuencia().intValue());
			datosCoberturasDTO.setNombreCobertura(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
			datosCoberturasDTO.setPrimaCobertura(coberturaCotizacionDTO.getValorPrimaNeta());
			
			//Valida datos a mostrar
			 if(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveMostrarPolDescripcion() != null && 
					 coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveMostrarPolDescripcion().equals("0")){
				 datosCoberturasDTO.setNombreCobertura(" ");
				 datosCoberturasDTO.setDeducible(" ");
			 }
			 if(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveMostrarPolSumaAsegurada() != null && 
					 coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveMostrarPolSumaAsegurada().equals("0")){
				 datosCoberturasDTO.setSumaAsegurada(" ");
			 }
			 if(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveMostrarPolPrimaNeta() != null && 
					 coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveMostrarPolPrimaNeta().equals("0")){
				 datosCoberturasDTO.setPrimaCobertura(0.0);
			 }
			 
			 if (imprimirUMA && (datosCoberturasDTO.getDeducible()!=null && datosCoberturasDTO.getDeducible().indexOf(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF) >=0)){
				 datosCoberturasDTO.setDeducible(datosCoberturasDTO.getDeducible().replace(ImpresionesServiceImpl.TIPO_VALOR_SA_DSMGVDF,
							ImpresionesServiceImpl.TIPO_VALOR_SA_UMA));
			 }
			
			dataSourceCoberturas.add(datosCoberturasDTO);
		}		
		return dataSourceCoberturas;		
	}
	
	@SuppressWarnings("unused")
	private String getSumaAsegurada(CoberturaCotizacionDTO coberturaCotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO, Short numeroAsientos) {
		return getSumaAsegurada(coberturaCotizacionDTO, incisoCotizacionDTO, numeroAsientos, false);
	}
	
	private String getSumaAsegurada(CoberturaCotizacionDTO coberturaCotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO, Short numeroAsientos, boolean esServicioPublico) {
		String sumaAsegurada = null;
		switch(Integer.parseInt(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada())){
		 case CoberturaDTO.VALOR_PROPORCIONADO:
		 case CoberturaDTO.VALOR_FACTURA:
		 case CoberturaDTO.VALOR_CONVENIDO:
			 if(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().equals(CoberturaDTO.GASTOS_MEDICOS_NOMBRE_COMERCIAL)){
					 Double valorSumaAsegurada_local = null;
					 valorSumaAsegurada_local = (esServicioPublico)? coberturaCotizacionDTO.getValorSumaAsegurada() :coberturaCotizacionDTO.getValorSumaAsegurada() * numeroAsientos;
				 sumaAsegurada = NumberFormat.getCurrencyInstance().format(valorSumaAsegurada_local);
				 }
			 else if(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().equals(CoberturaDTO.NOMBRE_LIMITE_RC_VIAJERO)){
				 sumaAsegurada = coberturaCotizacionDTO.getDiasSalarioMinimo()+" UMA";
				 }
				 else{
				 sumaAsegurada = UtileriasWeb.formatoMoneda(coberturaCotizacionDTO.getValorSumaAsegurada());	 
				 }
				 break;
		 case CoberturaDTO.VALOR_COMERCIAL:
			 sumaAsegurada = CoberturaDTO.DESCRIPCION_TIPO_SA_VALOR_COMERCIAL;
			 break;
		 case CoberturaDTO.VALOR_AMPARADA:
			 sumaAsegurada = CoberturaDTO.DESCRIPCION_TIPO_SUMA_ASEGURADA_AMPARADA;
				 break;
		 case CoberturaDTO.VALOR_CARATULA:
			 String claveEstilo = incisoCotizacionDTO.getIncisoAutoCot().getEstiloId();
			 EstiloVehiculoId idEstilo = new EstiloVehiculoId();
			 idEstilo.valueOf(claveEstilo);
			 ModeloVehiculoId id = new ModeloVehiculoId();
			 id.setClaveEstilo(idEstilo.getClaveEstilo());
			 id.setClaveTipoBien(idEstilo.getClaveTipoBien());
			 id.setIdVersionCarga(idEstilo.getIdVersionCarga());
			 id.setIdMoneda(incisoCotizacionDTO.getCotizacionDTO().getIdMoneda());
			 id.setModeloVehiculo(incisoCotizacionDTO.getIncisoAutoCot().getModeloVehiculo());
			 ModeloVehiculoDTO modelo = entityManager.find(ModeloVehiculoDTO.class, id);
			 if(modelo!=null && modelo.getValorCaratula()!=null){
				 sumaAsegurada = UtileriasWeb.formatoMoneda(modelo.getValorCaratula());
			 }else{
				  sumaAsegurada = "";
			 }
				 break;
			  default:
			  sumaAsegurada = "";
					break;
			 }			 
		return sumaAsegurada;
	}

	private String getDeducibleCobertura(CoberturaCotizacionDTO coberturaCotizacionDTO, IncisoCotizacionDTO incisoCotizacionDTO) {
		String deducibleString = "";
		Double deducible = null;
		
		if(CoberturaDTO.VALOR_CARATULA == Integer.parseInt(coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada())){
			deducibleString = getDeducibleValorCaratula(coberturaCotizacionDTO, incisoCotizacionDTO);
		}else{
			 if(coberturaCotizacionDTO.getClaveTipoDeducible() == null || coberturaCotizacionDTO.getClaveTipoDeducible().intValue() == 0){
				 deducibleString = getDescripionValorFijo(coberturaCotizacionDTO.getClaveTipoDeducible(),
						 coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible());
			 }else if(coberturaCotizacionDTO.getClaveTipoDeducible().intValue() == CoberturaDTO.CLAVE_TIPO_DEDUCIBLE_PCTE){
				 deducible = coberturaCotizacionDTO.getPorcentajeDeducible();
				 deducibleString = (deducible != null ? deducible.intValue() + " %": "");
			 }else{
				 deducible = coberturaCotizacionDTO.getValorDeducible();
				 deducibleString = getDescripionValorFijo(coberturaCotizacionDTO.getClaveTipoDeducible(), 
						 coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible());
				 deducibleString =  deducible!= null ? deducible.intValue() + deducibleString : "";				 
			 }
			 
			 deducibleString = getDeducibleCotSP(incisoCotizacionDTO, deducible, coberturaCotizacionDTO.getId().getIdToCobertura(), deducibleString);
		}
		
		return deducibleString;
	}
			
	private String getDeducibleValorCaratula(CoberturaCotizacionDTO coberturaCotizacionDTO, 
			IncisoCotizacionDTO incisoCotizacionDTO) {
		Double deducible = null;
		List<NegocioCobPaqSeccion> negocioCobPaqSeccionList = negocioCobPaqSeccionService.getCoberturaPorNegSeccion(
				 coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura()
				 ,null, null, incisoCotizacionDTO.getCotizacionDTO().getIdMoneda().shortValue(), incisoCotizacionDTO.getIncisoAutoCot().getNegocioPaqueteId(), null, null, null);
		NegocioCobPaqSeccion negocioCobPaqSeccion = negocioCobPaqSeccionList.get(0);
		if(negocioCobPaqSeccion!=null){
			deducible = Double.valueOf("0.00");
			String claveEstilo = incisoCotizacionDTO.getIncisoAutoCot().getEstiloId();
			EstiloVehiculoId idEstilo = new EstiloVehiculoId();
			idEstilo.valueOf(claveEstilo);
			ModeloVehiculoId id = new ModeloVehiculoId(idEstilo.getClaveTipoBien(), idEstilo.getClaveEstilo(),
					incisoCotizacionDTO.getIncisoAutoCot().getModeloVehiculo(), incisoCotizacionDTO.getCotizacionDTO().getIdMoneda(),
					idEstilo.getIdVersionCarga());
			ModeloVehiculoDTO modelo = entityManager.find(ModeloVehiculoDTO.class, id);
			if(modelo!=null && modelo.getValorCaratula()!=null){
				if(negocioCobPaqSeccion.getClaveTipoDeduciblePP()!=null && negocioCobPaqSeccion.getClaveTipoDeduciblePP().intValue()>0){
					deducible =  modelo.getValorCaratula().doubleValue()*(Double.valueOf(negocioCobPaqSeccion.getClaveTipoDeduciblePP())/100);
				}
			}
						
			return "PT: "+negocioCobPaqSeccion.getClaveTipoDeduciblePT()+"% \n PP: "+UtileriasWeb.formatoMoneda(deducible).toString();
		}
		 
		return "PT: 0.00% PP: $0.00";
	}

	private String getDescripionValorFijo(Short claveTipoDeducibleCobCot, String claveTipoDeducibleCob) {
		String descripionValorFijo = "";
		 if(claveTipoDeducibleCobCot != null){
				descripionValorFijo = listadoService.getDescripionValorFijo(Integer.valueOf(claveTipoDeducibleCob), 
						CatalogoValorFijoDTO.IDGRUPO_POSIBLES_DEDUCIBLES); 
			 }
		return descripionValorFijo;
			 }

	private String getDeducibleCotSP(IncisoCotizacionDTO incisoCotizacionDTO,
			Double deducible, BigDecimal idToCobertura, String deducibleString) {
		String deducibleCotSP = deducibleString;
		if(CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO.equals(incisoCotizacionDTO.getCotizacionDTO().getTipoCotizacion()) && deducible!= null && deducible == 0.00) {
			 List<NegocioDeducibleCob> deducibles = this.getDeducibles(incisoCotizacionDTO, idToCobertura);
			 if(deducibles==null || deducibles.isEmpty()){
				 deducibleCotSP = StringUtils.EMPTY;
			 }
		}		
		return deducibleCotSP;
	}
	
	@Override
	public DatosCaratulaCotizacion llenarCotizacionDTO(BigDecimal idToCotizacion) {	
		final BigDecimal idToCotizacionFinal = idToCotizacion;
		BigDecimal comisionMenosBonificacion = BigDecimal.ZERO;
		BigDecimal pcteCesionComision = null;
		DatosCaratulaCotizacion datosCaratulaCotizacion = new DatosCaratulaCotizacion();
		datosCaratulaCotizacion.setContenedorDatosImpresion(new ContenedorDatosImpresion());
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		getContenedorDatosImpresion(datosCaratulaCotizacion.getContenedorDatosImpresion(), cotizacionDTO, true, 0, 0);		
		datosCaratulaCotizacion.setDatasourceDesglosePagos(llenarDatosEsquemaPagos(cotizacionDTO, 
				datosCaratulaCotizacion.getContenedorDatosImpresion().getResumenCostosDTO()));
		datosCaratulaCotizacion.setDatosCotizacionDTO(llenarDatosCotizacion(cotizacionDTO, 
				datosCaratulaCotizacion.getContenedorDatosImpresion().getCliente(), 
				datosCaratulaCotizacion.getContenedorDatosImpresion().getDomicilioCliente()));
		PolizaDTO polizaDTO = new PolizaDTO();
		polizaDTO.setCotizacionDTO(cotizacionDTO);				
		datosCaratulaCotizacion.setCotizacionDTO(cotizacionDTO);
		datosCaratulaCotizacion.setDatasourceLineasNegocio(llenarDatosLineaNegocio(idToCotizacion));
		pcteCesionComision = BigDecimal.valueOf(datosCaratulaCotizacion.getCotizacionDTO().getPorcentajebonifcomision());
		
		/*Se cambia la prima neta desplegada en la cotizacion a que sea prima neta - comision */
		// calculo comision
		final Predicate findId = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				ComisionCotizacionDTO id = (ComisionCotizacionDTO) arg0;
				return (id.getId().getIdToCotizacion().equals(idToCotizacionFinal)
						&& id.getId().getIdTcSubramo().equals(CalculoServiceImpl.getIdSubRamoC()) && id.getId()
						.getTipoPorcentajeComision().equals((short) 1));
			}
		};
		final ComisionCotizacionDTO comisionCotizacionDTO = (ComisionCotizacionDTO) CollectionUtils.find(
															datosCaratulaCotizacion.getCotizacionDTO().getComisionCotizacionDTOs(), findId);
		if (comisionCotizacionDTO != null) {
			if (datosCaratulaCotizacion.getCotizacionDTO().getPorcentajebonifcomision() != null) {
				comisionMenosBonificacion = pcteCesionComision.multiply(
						comisionCotizacionDTO.getPorcentajeComisionDefault()).divide(BigDecimal.valueOf(100));
			}
		}		
		for(DatosLineasDeNegocioDTO detalleLinea : datosCaratulaCotizacion.getDatasourceLineasNegocio()){
			detalleLinea.setImpPrimaNeta(detalleLinea.getImpPrimaNeta().doubleValue() -  
					detalleLinea.getImpPrimaNeta().doubleValue() * comisionMenosBonificacion.doubleValue() / 100);
		}				
		datosCaratulaCotizacion.getDatosCotizacionDTO().setPrimaNeta(
				datosCaratulaCotizacion.getDatosCotizacionDTO().getPrimaNeta().doubleValue() - 
				datosCaratulaCotizacion.getDatosCotizacionDTO().getPrimaNeta().doubleValue() *  comisionMenosBonificacion.doubleValue() / 100);		
		return datosCaratulaCotizacion;
	}	
	
	private void setObservacionesCaratula(CotizacionDTO cotizacionDTO, DatosCotizacionDTO datosCotizacion){
		List<TexAdicionalCotDTO> textosAdicionales = entidadService.findByProperty(TexAdicionalCotDTO.class, "cotizacion.idToCotizacion", cotizacionDTO.getIdToCotizacion());
		//Sort
		if(textosAdicionales != null && !textosAdicionales.isEmpty()){
			Collections.sort(textosAdicionales, 
					new Comparator<TexAdicionalCotDTO>() {				
						public int compare(TexAdicionalCotDTO n1, TexAdicionalCotDTO n2){
							return n1.getIdToTexAdicionalCot().compareTo(n2.getIdToTexAdicionalCot());
						}
					});
		}
		
		StringBuilder stringBuilder = new StringBuilder();
		int lineas = 0;
		if(textosAdicionales != null && !textosAdicionales.isEmpty()){
			for(TexAdicionalCotDTO texto : textosAdicionales){
				if(texto != null && texto.getDescripcionTexto() != null && !texto.getDescripcionTexto().isEmpty()){
					 if(lineas == 7){
						 stringBuilder.append("..."); 
						 break;
					 }
					 if(lineas > 0){
						 stringBuilder.append(System.getProperty("line.separator"));
					 }
					 if(texto.getDescripcionTexto().length() > 300){
						 stringBuilder.append(texto.getDescripcionTexto().substring(0,300));
						 stringBuilder = new StringBuilder(stringBuilder.substring(0,stringBuilder.lastIndexOf(" ")));
						 stringBuilder.append("...");
					 }else{
						 stringBuilder.append(texto.getDescripcionTexto());
					 }
					 int extras = (texto.getDescripcionTexto().length() / 92) + 1;
					 lineas = lineas + extras;
				 }
			}
		}
		
		 if(stringBuilder.toString() != null && !stringBuilder.toString().equals("")){
			 datosCotizacion.setObservaciones(stringBuilder.toString());
		 }else{
			 datosCotizacion.setObservaciones("");
		 }
	}

	private DatosCotizacionDTO llenarDatosCotizacion(CotizacionDTO cotizacionDTO, ClienteDTO cliente, DomicilioImpresion domicilioCliente){
		DatosCotizacionDTO datosCotizacionDTO = new DatosCotizacionDTO();
		datosCotizacionDTO.setNumeroCotizacion(cotizacionDTO.getNumeroCotizacion());
		datosCotizacionDTO.setFechaInicioVigencia((cotizacionDTO.getFechaInicioVigencia()!=null)?cotizacionDTO.getFechaInicioVigencia():new Date());
		datosCotizacionDTO.setFechaFinVigencia((cotizacionDTO.getFechaFinVigencia()!=null)?cotizacionDTO.getFechaFinVigencia():new Date());
		datosCotizacionDTO.setNombreContratante(cotizacionDTO.getNombreContratante() != null ? cotizacionDTO.getNombreContratante().toUpperCase(): "");
		if (domicilioCliente != null) {
			datosCotizacionDTO.setCalleYNumero(domicilioCliente.getCalleNumero());
			datosCotizacionDTO.setColonia(domicilioCliente.getNombreColonia());
			datosCotizacionDTO.setCiudad(domicilioCliente.getCiudad());
			datosCotizacionDTO.setEstado(domicilioCliente.getEstado());
			datosCotizacionDTO.setCodigoPostal(domicilioCliente.getCodigoPostal());			
		}
		if(cliente != null){
			datosCotizacionDTO.setRfcContratante(cliente.getCodigoRFC());
		}
		datosCotizacionDTO.setIdToPersonaContratante(cotizacionDTO.getIdToPersonaContratante());
		
		DatosCaratulaCotizacion datosCaratulaCotizacion = new DatosCaratulaCotizacion();
		datosCaratulaCotizacion.setContenedorDatosImpresion(new ContenedorDatosImpresion());
		datosCaratulaCotizacion.setCotizacionDTO(entidadService.findById(CotizacionDTO.class, cotizacionDTO.getIdToCotizacion()));
		getContenedorDatosImpresion(datosCaratulaCotizacion.getContenedorDatosImpresion(), cotizacionDTO, true, 0, 0);
		datosCotizacionDTO.setDerechos(datosCaratulaCotizacion.getContenedorDatosImpresion().getResumenCostosDTO().getCaratula().getDerechos()!= null? 
				datosCaratulaCotizacion.getContenedorDatosImpresion().getResumenCostosDTO().getCaratula().getDerechos():0.00d);
		datosCotizacionDTO.setPrimaNeta((cotizacionDTO.getValorTotalPrimas()!= null ? cotizacionDTO.getValorTotalPrimas().doubleValue():0.00d));
		datosCotizacionDTO.setTitulo("COTIZACI\u00D3N DE SEGUROS PARA "+cotizacionDTO.getTipoPolizaDTO().getDescripcion());
		//Identificador
		//Centro Emisor - Gerencia - Oficina - Promotoria - Clave Agente
		String identificador = getIdentificador(cotizacionDTO);		
		datosCotizacionDTO.setIdentificador(identificador);
		
		//Observaciones
		setObservacionesCaratula(cotizacionDTO, datosCotizacionDTO);
		
		return datosCotizacionDTO;
	}
	
	private List<DatosDesglosePagosDTO> llenarDatosEsquemaPagos(CotizacionDTO cotizacionDTO, ResumenCostosGeneralDTO resumenCostosDTO){
		double primaTotal = 0;
		double pctRecargo = 0;
		boolean isServicioPublico = false;
		Double impRecargosFraccionados = null;
		DatosDesglosePagosDTO datosDesglosePagosDTO = null;
		List<EsquemaPagoCotizacionDTO> esquemaPagoList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
		List<DatosDesglosePagosDTO> dataSourceEsquemaPago = new ArrayList<DatosDesglosePagosDTO>(1);		
		if(cotizacionDTO.getTipoCotizacion()!=null){
			isServicioPublico = CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO.equals(cotizacionDTO.getTipoCotizacion());
		}
		if(!isServicioPublico) {
		if(cotizacionDTO.getValorPrimaTotal()!=null){
		for(int i=1;i<=4;i++){
			if(cotizacionDTO.getPorcentajePagoFraccionado() != null && 
				 i == cotizacionDTO.getIdFormaPago().intValue()){
				impRecargosFraccionados = BigDecimal.valueOf(resumenCostosDTO.getCaratula().getPrimaNetaCoberturas() * 
				(cotizacionDTO.getPorcentajePagoFraccionado() / 100)).setScale(2, RoundingMode.HALF_EVEN).doubleValue();
			}
			EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO = cotizacionService.findEsquemaPagoCotizacion(
					cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaInicioVigencia(), cotizacionDTO.getFechaFinVigencia(), 
					i, BigDecimal.valueOf(resumenCostosDTO.getCaratula().getTotalPrimas() - 
							(resumenCostosDTO.getCaratula().getDescuentoComisionCedida() != null ? 
									resumenCostosDTO.getCaratula().getDescuentoComisionCedida().doubleValue() : 0d)), 
					BigDecimal.valueOf(resumenCostosDTO.getCaratula().getDerechos()), 
					BigDecimal.valueOf(cotizacionDTO.getPorcentajeIva()), 
					cotizacionDTO.getIdMoneda().intValue(), impRecargosFraccionados,
					true);			         
			esquemaPagoList.add(esquemaPagoCotizacionDTO);
			impRecargosFraccionados = null;
		}
		
		for(EsquemaPagoCotizacionDTO esquemaPagoCotizacionDTO:esquemaPagoList){
			primaTotal = 0;
			pctRecargo = 0;
			datosDesglosePagosDTO = new DatosDesglosePagosDTO();
			datosDesglosePagosDTO.setFormaPago(esquemaPagoCotizacionDTO.getFormaPagoDTO().getDescription());			
			datosDesglosePagosDTO.setPrimerPago(esquemaPagoCotizacionDTO.getPagoInicial().getImporte().doubleValue());
			if(esquemaPagoCotizacionDTO.getPagoSubsecuente()!=null)
				datosDesglosePagosDTO.setPagosSubsecuentes(esquemaPagoCotizacionDTO.getPagoSubsecuente().getImporte().doubleValue());
			else{
				datosDesglosePagosDTO.setPagosSubsecuentes(0.0);
			}			
			pctRecargo = listadoService.getPctePagoFraccionado(esquemaPagoCotizacionDTO.getFormaPagoDTO().getIdFormaPago(), cotizacionDTO.getIdMoneda().shortValue());			
			primaTotal = esquemaPagoCotizacionDTO.getPagoInicial().getImporte().doubleValue() + (esquemaPagoCotizacionDTO.getPagoSubsecuente() != null ?  
					esquemaPagoCotizacionDTO.getPagoSubsecuente().getImporte().doubleValue() * esquemaPagoCotizacionDTO.getPagoSubsecuente().getNumExhibicion().doubleValue() : 0d); 
			datosDesglosePagosDTO.setMontoRecargoPagoFraccionado(resumenCostosDTO.getCaratula().getPrimaNetaCoberturas().doubleValue() * (pctRecargo / 100));
			datosDesglosePagosDTO.setMontoIVA(primaTotal - (primaTotal / (1 + cotizacionDTO.getPorcentajeIva() / 100)));			
			datosDesglosePagosDTO.setPrimaNetaTotal(primaTotal);			
			dataSourceEsquemaPago.add(datosDesglosePagosDTO);
		}
		}		
		}
		return dataSourceEsquemaPago;
	} 
	
	@Override
	public TransporteImpresionDTO imprimirCartaBienvenidaAgente() {
		String path=Entorno.obtenerVariable(SistemaPersistencia.LINUX_UPLOAD_FOLDER, SistemaPersistencia.UPLOAD_FOLDER);
		return getFileFromDisk(path+"/carta bienvenida.pdf");
	}	
	
	@Override
	public TransporteImpresionDTO imprimirContratoAgente(Agente agente, TipoContrato tipoContrato, Locale locale) {
		List<byte[]> inputByteArray = new ArrayList<byte[]>(1);
		Map<String, Object> parametros = new HashMap<String, Object>(1);		
		ByteArrayOutputStream output = new ByteArrayOutputStream();	
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		if(locale != null){
			parametros.put(JRParameter.REPORT_LOCALE, locale);
			parametros.put("P_IMAGE_LOGO", SistemaPersistencia.AFIRME_SEGUROS);				
		}		
		try{			
			List<DatosContrato> datosAgente = llenarDatosContratoAgente(agente);
			String path=Entorno.obtenerVariable(SistemaPersistencia.LINUX_UPLOAD_FOLDER, SistemaPersistencia.UPLOAD_FOLDER);
			InputStream reporteContratoAgente=null;
			GrupoActualizacionAgente tipoC=entidadService.findById(GrupoActualizacionAgente.class, tipoContrato.getValue());
			if(tipoC.getDescripcion().equals("Agente")){
				if(datosAgente.get(0).getAgente().getPersona().getClaveTipoPersona()==1){
					reporteContratoAgente = this.getClass().getResourceAsStream(
						"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/contratoAgentePersFisica.jrxml");
					transporteImpresionDTO=getFileFromDisk(path+"/contratoAgentePersFisica.pdf");
				}
				else{
					reporteContratoAgente = this.getClass().getResourceAsStream(
					"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/contratoAgentePersMoral.jrxml");
					transporteImpresionDTO=getFileFromDisk(path+"/contratoAgentePersMoral.pdf");
				}
			}
			else{
				if(datosAgente.get(0).getAgente().getPersona().getClaveTipoPersona()==1){
					reporteContratoAgente = this.getClass().getResourceAsStream(
						"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/contratoPromotorPersFisica.jrxml");
					transporteImpresionDTO=getFileFromDisk(path+"/contratoPromotorPersFisica.pdf");
				}
				else{
					reporteContratoAgente = this.getClass().getResourceAsStream(
					"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/contratoPromotorPersMoral.jrxml");
					transporteImpresionDTO=getFileFromDisk(path+"/contratoPromotorPersMoral.pdf");
				}
			}
			JasperReport reporteDatosContrato = JasperCompileManager.compileReport(reporteContratoAgente);					
			JRBeanCollectionDataSource datosAgenteDS = new JRBeanCollectionDataSource(datosAgente);
			inputByteArray.add(JasperRunManager.runReportToPdf(reporteDatosContrato, parametros, datosAgenteDS));
			inputByteArray.add(transporteImpresionDTO.getByteArray());				
			
			pdfConcantenate(inputByteArray, output);
			transporteImpresionDTO.setByteArray(output.toByteArray());			
		 return transporteImpresionDTO;		 	
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
	}
	
	public void  pdfConcantenate(List inputByteArray, OutputStream outputStream){  
	      try {
	          int pageOffset = 0;    
	          int f = 0;     
	          ArrayList master = new ArrayList();
	          Document document = null;
	          PdfCopy  writer = null;	          
	          // we create a reader for a certain document	          
	          Iterator iterator = inputByteArray.iterator();	          
	          while(iterator.hasNext()){   
	            byte[] data = (byte[])iterator.next();
	            if(data == null || data.length == 0){
	              f++;
	              continue;
	            }
	            PdfReader reader = new PdfReader(data);
	            reader.consolidateNamedDestinations();
	            // we retrieve the total number of pages
	            int n = reader.getNumberOfPages();
	            List bookmarks = SimpleBookmark.getBookmark(reader);
	            if (bookmarks != null) {
	              if (pageOffset != 0)
	                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	                master.addAll(bookmarks);
	            }	            
	            pageOffset += n;	                
	            if (f == 0) {
	              // step 1: creation of a document-object
	              document = new Document(reader.getPageSizeWithRotation(1));
	              // step 2: we create a writer that listens to the document                          
	              writer = new PdfCopy(document, outputStream);
	              // step 3: we open the document
	              document.open();           
	            }	              
	            // step 4: we add content
	            PdfImportedPage page;
	            for (int i = 0; i < n; ) {
	              ++i;
	              page = writer.getImportedPage(reader, i);
	              writer.addPage(page);
	            }	  
	            PRAcroForm form = reader.getAcroForm();
	            if (form != null){
	              writer.copyAcroForm(reader);
	            }
	            f++;
	          }
	          if (!master.isEmpty()){
	            writer.setOutlines(master);
	          }
	          // step 5: we close the document
	          if(document != null){
	            document.close();
	          }
	      }
	      catch(Exception e) {	          
	      }	      
	  }
	
	private List<DatosContrato> llenarDatosContratoAgente(Agente agente){		
		PersonaSeycosDTO persona = new PersonaSeycosDTO();
		DatosContrato datosContratoAgente = new DatosContrato();
		try {
			persona = personaSeycosFacadeRemote.findById(agente.getPersona().getIdPersona());
//		List<Domicilio> domicilio=domicilioFacadeRemote.findDomiciliosActualesPorPersona(persona.getIdPersona(), "PERS", null);
		List<Domicilio> domicilio=domicilioFacadeRemote.findDomiciliosActualesPorPersona(persona.getIdPersona(), "FISC", null);
		if(domicilio.isEmpty() || domicilio.get(0) == null){
			domicilio=domicilioFacadeRemote.findDomiciliosActualesPorPersona(persona.getIdPersona(), null, null);
		}
		EstadoDTO estadoDTO = estadoFacadeRemote.findById(domicilio.get(0).getClaveEstado());
		MunicipioDTO municipioDTO = municipioFacadeRemote.findById(domicilio.get(0).getClaveCiudad());
		PaisDTO paisDTO=paisFacadeRemote.findById(agente.getPersona().getClaveNacionalidad());
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");				
		
		//se agrega el sector financiero para persona moral
		if(persona!=null && persona.getClaveSectorFinanciero()!=null){
			SectorDTO objSector = new SectorDTO();
			objSector = sectorFacadeRemote.findById(agente.getPersona().getClaveTipoSectorMoral());
			agente.getPersona().setClaveSectorFinanciero((objSector!=null)?objSector.getNombreSector():"");
		}else{
			agente.getPersona().setClaveSectorFinanciero("");
		}
		//************
		datosContratoAgente.setAgente(agente);
		if (persona.getEstadoCivil() != null) {
			EstadoCivilDTO estadoCivilDTO=estadoCivilFacadeRemote.findById(persona.getEstadoCivil());
			datosContratoAgente.setEstadoCivil(estadoCivilDTO);
		}	
		datosContratoAgente.setPais(paisDTO);
		datosContratoAgente.setDomicilio(domicilio.get(0));
		datosContratoAgente.setEstadoDTO(estadoDTO);
		datosContratoAgente.setMunicipioDTO(municipioDTO);
		datosContratoAgente.setVigenciaAutorizacion(agente.getFechaAutorizacionCedula()!=null?sdf.format(agente.getFechaAutorizacionCedula()):""+"-"+agente.getVencimientoCedulaString()!=null?agente.getVencimientoCedulaString():"");
		datosContratoAgente.setVigenciaFianza(agente.getFechaAutorizacionFianza()!=null?sdf.format(agente.getFechaAutorizacionFianza()):""+"-"+agente.getVencimientoFianzaString()!=null?agente.getVencimientoFianzaString():"");
		datosContratoAgente.setDomicilioParticular(domicilio.get(0).getCalleNumero()+" "+domicilio.get(0).getNombreColonia()+" "+domicilio.get(0).getCodigoPostal()+" "+municipioDTO.getMunicipalityName()+","+estadoDTO.getDescription());
		if(agente.getMontoFianza()!=null){
			datosContratoAgente.setMontoFianzaLetra(Utilerias.convertNumberToLetter(agente.getMontoFianza().doubleValue()));
		}
		if(persona.getClaveTipoPersona()==2){
			if(agente.getPersona().getIdRepresentante()!=null){
				PersonaSeycosDTO repLegal = new PersonaSeycosDTO();
				repLegal = personaSeycosFacadeRemote.findById(agente.getPersona().getIdPersona());
				datosContratoAgente.setRepLegal(repLegal.getNombreCompleto());
			}
			if(agente.getPersona().getFechaConstitucion()!=null){
				datosContratoAgente.setFechaConstString(sdf.format(agente.getPersona().getFechaConstitucion()));
			}
		}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		List<DatosContrato> datosAgente = new ArrayList<DatosContrato>();
		datosAgente.add(datosContratoAgente);
		
		return datosAgente;
	}
	
	private String getDescripcionDeducible(String tipoDeducible){
		String descripcion = "";
		int valorTipoDeducible;
		if(tipoDeducible != null){
			valorTipoDeducible = Integer.valueOf(tipoDeducible);
			for(CatalogoValorFijoDTO valor: posiblesDeducibles){
				if(valor.getId().getIdDato() == valorTipoDeducible){
					descripcion = valor.getDescripcion();					
					break;
				}
			}
		}
		return descripcion;
	}
	
	@SuppressWarnings("unchecked")
	private void populateIncisos(List<IncisoCotizacionDTO> incisos){		
		for(IncisoCotizacionDTO inciso : incisos){
			inciso = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId());
		}
		Collections.sort(incisos, new Comparator(){
			public int compare(Object o1, Object o2) {
				return ((IncisoCotizacionDTO)o1).getId().getNumeroInciso().compareTo(((IncisoCotizacionDTO)o2).getId().getNumeroInciso());
			}
		});						
	}
	
	public  NegocioRecuotificacion llenarRecuotificacion(Long idToNegocio){
		List<NegocioRecuotificacion> negocioRecuotificacionList=entidadService.findByProperty(NegocioRecuotificacion.class, "negocio.idToNegocio", idToNegocio);
		NegocioRecuotificacion negocioRecuotificacion=new NegocioRecuotificacion();
		if(negocioRecuotificacionList!=null && !negocioRecuotificacionList.isEmpty()){
			negocioRecuotificacion=negocioRecuotificacionList.get(0);
			//System.out.println("##llenarRecuotificacion|idNegocioRecuotif:"+negocioRecuotificacion.getId());
			negocioRecuotificacion.setVersiones(new LinkedList<NegocioRecuotificacionAutomatica>());
			negocioRecuotificacion.getVersiones().addAll(entidadService.findByProperty(NegocioRecuotificacionAutomatica.class,
					"negRecuotificacion.id", negocioRecuotificacion.getId()));	
		//System.out.println("##llenarRecuotificacion|versiones:"+negocioRecuotificacion.getVersiones().size());
		for(NegocioRecuotificacionAutomatica version: negocioRecuotificacion.getVersiones()){
			version.setProgramas( new LinkedList<NegocioRecuotificacionAutProgPago>());
			version.getProgramas().addAll(entidadService.findByProperty(NegocioRecuotificacionAutProgPago.class,
					"automatica.id", version.getId()));
			//System.out.println("##llenarRecuotificacion|version:"+version.getVersion()+"-"+version.getStatus()+"|programas:"+version.getProgramas().size());
			for(NegocioRecuotificacionAutProgPago programa : version.getProgramas()){
				programa.setRecibos(new LinkedList<NegocioRecuotificacionAutRecibo>());
				programa.getRecibos().addAll(entidadService.findByProperty(NegocioRecuotificacionAutRecibo.class,
						"progPago.id",programa.getId()));
				//System.out.println("##llenarRecuotificacion|programa:"+programa.getNumProgPago()+"|recibos:"+(programa.getRecibos().isEmpty()?"0":programa.getRecibos().size())+"|1reciboId:"+(programa.getRecibos().isEmpty()?"":programa.getRecibos().get(0).getId()));
			}
		}
		}
		return negocioRecuotificacion;
	}
	

	@Override
	public DatosNegocioDTO llenarNegocioDTO(Long idToNegocio) {
		DatosNegocioDTO negocioDatos = new DatosNegocioDTO();
		List<NegocioProducto> negocioProductos = new LinkedList<NegocioProducto>();
		negocioDatos.setDatosProductosDeNegocioDetalleDTOs(new LinkedList<DatosProductosDeNegocioDetalleDTO>());
		List<NegocioSeccion> negocioSeccions = null;
		negocioProductos = entidadService.findByProperty(NegocioProducto.class, "negocio.idToNegocio", idToNegocio);
		Negocio negocio = entidadService.findById(Negocio.class, idToNegocio);
		Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(negocio.getCodigoUsuarioActivacion());
		if (usuario != null && usuario.getNombreCompleto() != null) {
			negocioDatos.setUsuarioCreador(usuario.getNombreCompleto());
		}else if(usuario != null){
			negocioDatos.setUsuarioCreador(usuario.getNombre());
		}else{
			negocioDatos.setUsuarioCreador("MIDAS");
		}
		if (negocioProductos != null) {
			for (NegocioProducto negocioProducto : negocioProductos) {
				DatosProductosDeNegocioDetalleDTO datosProductosDeNegocioDetalleDTO = new DatosProductosDeNegocioDetalleDTO();
				datosProductosDeNegocioDetalleDTO.setNegocioProducto(negocioProducto);
				datosProductosDeNegocioDetalleDTO.setDatosAgrupadorTarifaNegocioDetalleDTOs(new LinkedList<DatosAgrupadorTarifaNegocioDetalleDTO>());
				datosProductosDeNegocioDetalleDTO.setDatosCoberturasDeNegocioDetalleDTOs(new LinkedList<DatosCoberturasDeNegocioDetalleDTO>());
				datosProductosDeNegocioDetalleDTO.setDatosLineasNegocioDetalleDTOs(new LinkedList<DatosLineasNegocioDetalleDTO>());
				datosProductosDeNegocioDetalleDTO.setDatosPaquetesNegocioDetalleDTOs(new LinkedList<DatosPaquetesNegocioDetalleDTO>());
				datosProductosDeNegocioDetalleDTO.setNegocioTipoPolizas(new LinkedList<NegocioTipoPoliza>());
				negocioDatos.getDatosProductosDeNegocioDetalleDTOs().add(datosProductosDeNegocioDetalleDTO);
			}
		}
		// Obtiene Negocio Tipo de Poliza
		for (DatosProductosDeNegocioDetalleDTO detalleDTO : negocioDatos
				.getDatosProductosDeNegocioDetalleDTOs()) {
			detalleDTO.setNegocioTipoPolizas(entidadService.findByProperty(
					NegocioTipoPoliza.class, "negocioProducto.idToNegProducto",
					detalleDTO.getNegocioProducto().getIdToNegProducto()));
		}
		// Obtiene los negocio seccion
		for (DatosProductosDeNegocioDetalleDTO detalleDTO : negocioDatos
				.getDatosProductosDeNegocioDetalleDTOs()) {
			// Negocio seccion para cada Producto
			negocioSeccions = new LinkedList<NegocioSeccion>();
			for (NegocioTipoPoliza negocioTipoPoliza : detalleDTO.getNegocioTipoPolizas()) {
				List<NegocioSeccion> listNegocioSeccionTemp = entidadService.findByProperty(NegocioSeccion.class,
						"negocioTipoPoliza.idToNegTipoPoliza",
						negocioTipoPoliza.getIdToNegTipoPoliza());
				if (listNegocioSeccionTemp != null && !listNegocioSeccionTemp.isEmpty()) {
					negocioSeccions.addAll(listNegocioSeccionTemp);
				}
				String descripcionPoliza = negocioTipoPoliza.getTipoPolizaDTO().getDescripcion().toUpperCase();
				detalleDTO.setTipoPolizaDesc(descripcionPoliza);
				
			// Obtiene todo el Flow 
			for (NegocioSeccion negocioSeccion : negocioSeccions) {
				String descripcionSeccion = negocioSeccion.getSeccionDTO().getDescripcion().toUpperCase(); 
				detalleDTO.setSeccionDesc(descripcionSeccion);
				/**
				 * Data Source de Lineas de Negocio
				 */
					DatosLineasNegocioDetalleDTO lineasNegocioDetalleDTO = new DatosLineasNegocioDetalleDTO();
					// Tipo uso
					lineasNegocioDetalleDTO.setNegocioTipoUso(new LinkedList<NegocioTipoUso>());
						lineasNegocioDetalleDTO.getNegocioTipoUso().addAll(negocioSeccion.getNegocioTipoUsoList());
						
					lineasNegocioDetalleDTO.setSeccionDesc(descripcionSeccion);
					lineasNegocioDetalleDTO.setTipoPolizaDesc(descripcionPoliza);
					// Valida el tipo de uso
					if (lineasNegocioDetalleDTO.getNegocioTipoUso().isEmpty()) {
						List<TipoUsoVehiculoDTO> listTipoUsoTemp = entidadService
						.findAll(TipoUsoVehiculoDTO.class);
						for (TipoUsoVehiculoDTO usoVehiculoDTO : listTipoUsoTemp) {
							NegocioTipoUso negocioTipoUso = new NegocioTipoUso();
							negocioTipoUso.setTipoUsoVehiculoDTO(usoVehiculoDTO);
							lineasNegocioDetalleDTO.getNegocioTipoUso().add(negocioTipoUso);
						}
					}
					// Tipo servicio
					lineasNegocioDetalleDTO.setNegocioTipoServicio(new LinkedList<NegocioTipoServicio>());
						lineasNegocioDetalleDTO.getNegocioTipoServicio().addAll(negocioSeccion.getNegocioTipoServicioList());
						
					// Estilo vehiculo
					lineasNegocioDetalleDTO.setNegocioEstiloVehiculo(new LinkedList<NegocioEstiloVehiculo>());
						lineasNegocioDetalleDTO.getNegocioEstiloVehiculo().addAll(negocioSeccion.getNegocioEstiloVehiculoList());
					
					/**
					 * Data Source de Paquetes
					 */
					DatosPaquetesNegocioDetalleDTO paquetesNegocioDetalleDTO = new DatosPaquetesNegocioDetalleDTO();
					// Paquetes
					paquetesNegocioDetalleDTO.setPaqueteSeccion(new LinkedList<NegocioPaqueteSeccion>());
					paquetesNegocioDetalleDTO.setSeccionDesc(descripcionSeccion);
					paquetesNegocioDetalleDTO.setTipoPolizaDesc(descripcionPoliza);
					List<NegocioPaqueteSeccion> negocioPaqueteSeccionTemp = negocioSeccion.getNegocioPaqueteSeccionList();
					if (negocioPaqueteSeccionTemp == null) {
						negocioPaqueteSeccionTemp = new LinkedList<NegocioPaqueteSeccion>();
					}
					/**
					 * Data Soruce de Coberturas
					 */
					// Coberturas
					for (NegocioPaqueteSeccion paqueteSeccion : negocioPaqueteSeccionTemp) {
						DatosCoberturasDeNegocioDetalleDTO coberturasDeNegocioDetalleDTO = new DatosCoberturasDeNegocioDetalleDTO();
						List<NegocioCobPaqSeccion> cobPaqSeccionsTemp = paqueteSeccion.getNegocioCobPaqSeccionList();
						if (paqueteSeccion.getNegocioCobPaqSeccionList() != null) {
							for (NegocioCobPaqSeccion cobPaqSeccion : cobPaqSeccionsTemp) {
								DatosCobPaqueteNegocioDetalleDTO cobPaqueteNegocioDetalleDTO = new DatosCobPaqueteNegocioDetalleDTO();
								cobPaqueteNegocioDetalleDTO.setSeccionDesc(descripcionSeccion);
								cobPaqueteNegocioDetalleDTO.setTipoPolizaDesc(descripcionPoliza);
								StringBuilder deducibles = new StringBuilder();
								cobPaqueteNegocioDetalleDTO.setCiudadDTO(cobPaqSeccion
										.getCiudadDTO());
								cobPaqueteNegocioDetalleDTO.setEstadoDTO(cobPaqSeccion
										.getEstadoDTO());
								cobPaqueteNegocioDetalleDTO.setIdToNegCobPaqSeccion(cobPaqSeccion.getIdToNegCobPaqSeccion());
								cobPaqueteNegocioDetalleDTO.setMonedaDTO(cobPaqSeccion.getMonedaDTO());
								cobPaqueteNegocioDetalleDTO.setPaquete(cobPaqSeccion.getNegocioPaqueteSeccion().getPaquete());
								cobPaqueteNegocioDetalleDTO.setValorSumaAseguradaDefault(cobPaqSeccion.getValorSumaAseguradaDefault());
								cobPaqueteNegocioDetalleDTO.setValorSumaAseguradaMax(cobPaqSeccion.getValorSumaAseguradaMax());
								cobPaqueteNegocioDetalleDTO.setValorSumaAseguradaMin(cobPaqSeccion.getValorSumaAseguradaMin());
								cobPaqueteNegocioDetalleDTO.setCoberturaDesc(cobPaqSeccion.getCoberturaDTO().getDescripcion());
								List<NegocioDeducibleCob> cobsTemp = cobPaqSeccion.getNegocioDeducibleCobList();
								if (cobsTemp != null) {
									for(NegocioDeducibleCob deducibleCoberturaDTO : cobsTemp) {
										if(deducibleCoberturaDTO
												.getClaveDefault() != 0) {
											deducibles.append(deducibleCoberturaDTO
													.getValorDeducible().toString()
													+ " Default ,");
										}else{
											deducibles.append(deducibleCoberturaDTO
													.getValorDeducible().toString()
													+",");
										}
									}
									if (!deducibles.equals("") && deducibles.length() > 1) {
										cobPaqueteNegocioDetalleDTO.setCoberturaDeducibles(deducibles.substring(0, deducibles.length()-1).toString());
									}
								}
								coberturasDeNegocioDetalleDTO.getDatosCobPaqueteNegocioDetalleDTO().add(cobPaqueteNegocioDetalleDTO);
							}
							detalleDTO.getDatosCoberturasDeNegocioDetalleDTOs().add(coberturasDeNegocioDetalleDTO);
						}
					}
						paquetesNegocioDetalleDTO.setPaqueteSeccion(negocioPaqueteSeccionTemp);
					/**
					 * Data Source Tarifas
					 */
					DatosAgrupadorTarifaNegocioDetalleDTO agrupadorTarifaNegocioDetalleDTO = new DatosAgrupadorTarifaNegocioDetalleDTO();
					agrupadorTarifaNegocioDetalleDTO.setAgrupadorTarifaSeccion(new LinkedList<AgrupadorTarifaSeccion>());
					agrupadorTarifaNegocioDetalleDTO.setSeccionDesc(descripcionSeccion);
					List<MonedaTipoPolizaDTO> monedaTipoPolizaDTOs = negocioTipoPoliza.getTipoPolizaDTO().getMonedas(); 
					final StringBuilder idMonedas = new StringBuilder();
					for(MonedaTipoPolizaDTO moneda : monedaTipoPolizaDTOs) {
						if(StringUtils.isBlank(idMonedas.toString())) {
							idMonedas.append(moneda.getId().getIdMoneda());
						}else {
							idMonedas.append("," + moneda.getId().getIdMoneda());
						} 
					}
					// Obtiene las tarifas
					agrupadorTarifaNegocioDetalleDTO.setTipoPolizaDesc(descripcionPoliza);
								agrupadorTarifaNegocioDetalleDTO
										.getAgrupadorTarifaSeccion().addAll(agrupadorTarifaSeccionService
												.consultaAgrupadoresPosibles(
														negocioSeccion,
														idMonedas.toString()));
				 // Guarda todo el flow
					detalleDTO.getDatosLineasNegocioDetalleDTOs().add(lineasNegocioDetalleDTO);
					detalleDTO.getDatosPaquetesNegocioDetalleDTOs().add(paquetesNegocioDetalleDTO);
					detalleDTO.getDatosAgrupadorTarifaNegocioDetalleDTOs().add(agrupadorTarifaNegocioDetalleDTO);
				}
			}
			// Obtiene las formas de pago
			 detalleDTO.setFormaPagos(entidadService.findByProperty(NegocioFormaPago.class,
						"negocioProducto.idToNegProducto", detalleDTO
						.getNegocioProducto().getIdToNegProducto()));
			 // Obtiene los medios de pago
			 detalleDTO.setMedioPagos(
					 entidadService.findByProperty(NegocioMedioPago.class,"negocioProducto.idToNegProducto",
							 detalleDTO.getNegocioProducto().getIdToNegProducto()));
		}
		// Informacion general del negocio
		if (negocio != null) {
			negocioDatos.setNegocio(negocio);
			// Obtiene Clientes del negocio
			negocioDatos.setNegocioClientes(negocioService
					.listarClientesAsociados(negocio.getIdToNegocio()));
			// Obtiene Agentes del negocio
			negocioDatos.setNegocioAgentes(negocioAgenteService.listarNegocioAgentesAsociadosLight(negocio
					.getIdToNegocio()));
			// Obtiene Bonos y sobreComisiones
				negocioDatos.setNegocioBonoComisions(entidadService.findByProperty(
						NegocioBonoComision.class, "negocio.idToNegocio",
						idToNegocio));
				// Obtiene derechos de endoso
				negocioDatos.setNegocioDerechoEndosos(entidadService.findByProperty(NegocioDerechoEndoso.class,
				"negocio.idToNegocio", negocio.getIdToNegocio()));
		
				// Obtiene derechos de poliza
			negocioDatos.setNegocioDerechoPolizas(entidadService
					.findByProperty(NegocioDerechoPoliza.class,
							"negocio.idToNegocio", negocio.getIdToNegocio()));
			// Obtiene los estados
			negocioDatos.setEstados(entidadService.findByProperty(NegocioEstado.class,
					"negocio.idToNegocio", negocio.getIdToNegocio()));
		}

		// Obtiene condiciones de renovacion
		Map<String, Object> params = new HashMap<String, Object>(1);
		params.put("id.idToNegocio", negocio.getIdToNegocio());
		params.put("id.tipoRiesgo",1);
		negocioDatos.setNegocioRenovacions(entidadService
				.findByProperties(NegocioRenovacion.class, params));
		// Obtiene los descuentos de renovacion
		negocioDatos.setRenovacionDescuentosList(entidadService.findByProperty(
				NegocioRenovacionDesc.class, "id.idToNegocio",
				negocio.getIdToNegocio()));
			
		//Obtiene la informacion del las compensaciones
		lista_caParametros = caParametrosDao.findByIdNegocio(idToNegocio);
		negocioDatos.setReportePrima_poliza(lista_caParametros);
		LOG.info("Mostrando registros Prima y Polizas" + lista_caParametros.size());
		LOG.info("idNegocio"+idToNegocio);
		lista_caParametrosBS = caParametrosDao.findByIdNegocionBS(new Long(idToNegocio));
		negocioDatos.setReporteBs(lista_caParametrosBS);
		LOG.info("Mostrando registros Baja Sinestralidad : " + lista_caParametrosBS.size());
		
		//se llena la informacion de recuotificacion
		negocioDatos.setNegocioRecuotificacion(llenarRecuotificacion(idToNegocio));
		return negocioDatos;
	}
	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@Override
	public TransporteImpresionDTO imprimirBasesEmision(DatosBasesEmisionParametrosDTO datosParametros, Locale locales){
		TransporteImpresionDTO transporteImpresionDTO = null;
		try {
			String p = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
			
			if (datosParametros.getNivelAgrupamiento().intValue() == 0) {
				//contrato reaseguro
				p += "";
			}else if (datosParametros.getNivelAgrupamiento().intValue() == 1) {
				//Endoso
				p += "RepBasesEmisionAutoEndoso.jrxml";
			}else if (datosParametros.getNivelAgrupamiento().intValue() == 2) {
				//Poliza
				p += "RepBasesEmisionAutoPoliza.jrxml";
			}else if (datosParametros.getNivelAgrupamiento().intValue() == 3) {
				//SubRamo
				p += "RepBasesEmisionAutoSubramo.jrxml";
			}else if (datosParametros.getNivelAgrupamiento().intValue() == 4) {
				//Ramo
				p += "RepBasesEmisionAutoRamo.jrxml";
			}else if (datosParametros.getNivelAgrupamiento().intValue() == 5) {
				//Oficina emisión
				p += "RepBasesEmisionAutoOficinaEmi.jrxml";
			}
			
			InputStream reporteBasesEmision = this.getClass().getResourceAsStream(p);
			//se obtiene la lista.
			List<DatosBasesEmisionDTO> basesEmisionDTOLista =  basesEmisionDao.obtenerBasesEmision(datosParametros);
			//
			JasperReport reporteDatosContrato = JasperCompileManager.compileReport(reporteBasesEmision);					
			JRBeanCollectionDataSource datosAgenteDS = new JRBeanCollectionDataSource(basesEmisionDTOLista);
			
			GestorReportes ges = new GestorReportes();
			transporteImpresionDTO = new TransporteImpresionDTO();
			byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS, reporteDatosContrato, null, datosAgenteDS);
			transporteImpresionDTO.setByteArray(d);
		} catch (Exception e) {
			throw new RuntimeException("[imprimirBasesEmision]",e);
		}
		return transporteImpresionDTO;
	}
	
	/**/
	public TransporteImpresionDTO imprimirReportesPagoCompContra(DatosReportePagoCompContraParametrosDTO datosParametros, Locale locales){
		TransporteImpresionDTO transporteImpresionDTO = null;
		try{
			String p ="/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
			p  +="RepPagoCompContra.jrxml";
			
			InputStream reportePagoCompensacionesContraprestaciones = this.getClass().getResourceAsStream(p);
			
			
			List<CaReportesPagoCompContra> caReportesPCCLista = reporteBaseCompensacionesDao.obtenerRepPagoCompContra(datosParametros);
			JasperReport reporteDatos = JasperCompileManager.compileReport(reportePagoCompensacionesContraprestaciones);
			JRBeanCollectionDataSource datosDS = new JRBeanCollectionDataSource(caReportesPCCLista);
			
			GestorReportes ges = new GestorReportes();
			transporteImpresionDTO = new TransporteImpresionDTO();
			byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS,reporteDatos,null,datosDS);
			transporteImpresionDTO.setByteArray(d);
			
		}catch(Exception e){
			throw new RuntimeException("[imprimirReportesPagoCompContra]",e);
		}
		return transporteImpresionDTO;
	}
	
	public TransporteImpresionDTO imprimirReporteBancaSeguros(Map<String,Object> parametros){
		TransporteImpresionDTO transporteImpresionDTO= null;
		try{
			String p = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ReporteBancaSeguros.jrxml";
			InputStream reporteBancaSeguros = this.getClass().getResourceAsStream(p);
			
			List<CaBancaSegurosDTO> bancaSegurosList = reporteBaseCompensacionesDao.obtenerRepBancaSeguros(parametros);
			LOG.info("Total Registros == " +bancaSegurosList.size());
			JasperReport reporteDatos = JasperCompileManager.compileReport(reporteBancaSeguros);
			JRBeanCollectionDataSource datosDS = new JRBeanCollectionDataSource(bancaSegurosList);
			
		    GestorReportes ges = new GestorReportes();
		    transporteImpresionDTO = new TransporteImpresionDTO();
		    byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS,reporteDatos,null,datosDS);
		    transporteImpresionDTO.setByteArray(d);
		}catch(Exception e){
			throw new RuntimeException("[imprimirReporteBancaSeguros]", e);
		}   
		return transporteImpresionDTO;
	}
	
	public TransporteImpresionDTO imprimirReportePagosSaldos(Map<String,Object> parametros){
		TransporteImpresionDTO transporteImpresionDTO= null;
		try{
			String p = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ReportePagosSaldos.jrxml";
		    InputStream reportePagosSaldos =  this.getClass().getResourceAsStream(p);
		    
		    List<CaPagosSaldosDTO> pagosSaldosList = reporteBaseCompensacionesDao.obtenerRepPagosSaldos(parametros);
		    LOG.info("Total Registros == " +pagosSaldosList.size());
		    JasperReport reporteDatos = JasperCompileManager.compileReport(reportePagosSaldos);
		    JRBeanCollectionDataSource datosDS = new JRBeanCollectionDataSource(pagosSaldosList);
		    
		    GestorReportes ges = new GestorReportes();
		    transporteImpresionDTO = new TransporteImpresionDTO();
		    byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS,reporteDatos,null,datosDS);
		    transporteImpresionDTO.setByteArray(d);
		}catch(Exception e){
			throw new RuntimeException("[imprimirReportePagosSaldos]", e);
		}
		return transporteImpresionDTO;
	}
	
	
	public TransporteImpresionDTO imprimirReportesEstadoCuenta(DatosReporteEstadoCuentaParametrosDTO datosParametros, Locale locales){
		TransporteImpresionDTO transporteImpresionDTO= null;
		try{
			String p ="/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
			p +="edoctaN.jrxml";
		

		InputStream reporteEstadoCuenta = this.getClass().getResourceAsStream(p);
		Map<String, Object> parameters = new HashMap<String, Object>(1);
		parameters.put(IMGHEADER, SistemaPersistencia.LOGO_AFIRME);

		List<CaReportesDTO> caReportesECDTOLista = reporteBaseCompensacionesDao.obtenerRepEstadoCuenta(datosParametros);

		JasperReport reporteDatos = JasperCompileManager.compileReport(reporteEstadoCuenta);
		JRBeanCollectionDataSource datosDS = new JRBeanCollectionDataSource(caReportesECDTOLista);

		GestorReportes ges = new GestorReportes();
		transporteImpresionDTO = new TransporteImpresionDTO();
		byte[] d = ges.generaReporte(ConstantesReporte.TIPO_PDF,reporteDatos,parameters,datosDS);
		//byte[] d = ges.generaReporte(ConstantesReporte.TIPO_PDF,reporteDatos,null,datosDS);
		transporteImpresionDTO.setByteArray(d);
} catch(Exception e){
		throw new RuntimeException("[imprimirReportesEstadoCuenta]",e);
}
	return  transporteImpresionDTO;
}
	/**/
	
	
	public TransporteImpresionDTO imprimirReportesCompensacionesAdicionales(DatosReporteCompParametrosDTO datosParametros, Locale locales){
		TransporteImpresionDTO transporteImpresionDTO = null;
		try {
			String p = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
			if (datosParametros.getTipoReporte().intValue() == 1) {
				p += "RepCompensaciones.jrxml";
			}
			
			InputStream reporteCompensaciones = this.getClass().getResourceAsStream(p);
			List<CaReportesCompDTO> caReportesDTOLista =  reporteBaseCompensacionesDao.obtenerRepCompensaciones(datosParametros);
			JasperReport reporteDatosContrato = JasperCompileManager.compileReport(reporteCompensaciones);					
			JRBeanCollectionDataSource datosAgenteDS = new JRBeanCollectionDataSource(caReportesDTOLista);
			
			GestorReportes ges = new GestorReportes();
			transporteImpresionDTO = new TransporteImpresionDTO();
			byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS, reporteDatosContrato, null, datosAgenteDS);
			transporteImpresionDTO.setByteArray(d);
		} catch (Exception e) {
			throw new RuntimeException("[imprimirReporteCompensaciones]",e);
		}
		return transporteImpresionDTO;
	}
	
	public TransporteImpresionDTO imprimirReportesDerechoPoliza(DatosRepDerPolParametrosDTO datosParametros, Locale locales){
		TransporteImpresionDTO transporteImpresionDTO = null;
		try {
			String p = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
			//if (datosParametros.getTipoReporte().intValue() == 2) {
				p += "RepDerechoPolizaCa.jrxml";
			//}
			InputStream reporteDerPol = this.getClass().getResourceAsStream(p);
				List<CaReportesDerPolDTO> caDerechoPolizaDTOLista =  reporteBaseCompensacionesDao.obtenerRepDerechoPoliza(datosParametros);
				JasperReport reporteDatosContrato = JasperCompileManager.compileReport(reporteDerPol);					
				JRBeanCollectionDataSource datosAgenteDS = new JRBeanCollectionDataSource(caDerechoPolizaDTOLista);
		
			GestorReportes ges = new GestorReportes();
			transporteImpresionDTO = new TransporteImpresionDTO();
			byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS, reporteDatosContrato, null, datosAgenteDS);
			transporteImpresionDTO.setByteArray(d);
		} catch (Exception e) {
			throw new RuntimeException("[imprimirReportesDerechoPoliza]",e);
		}
		return transporteImpresionDTO;
	}
	
	public TransporteImpresionDTO imprimirReportesPagadasDevengar(DatosReportePagDevDTO datosParametros, Locale locales){
		TransporteImpresionDTO transporteImpresionDTO = null;
		try {
			String p = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
			if (datosParametros.getTipoReporte().intValue() == 3 && datosParametros.getTipoArchivo().intValue()==1) {
				//Compensaciones Adicionales
				p += "RepPagadasDevengarCaResumen.jrxml";
			}
			if (datosParametros.getTipoReporte().intValue() ==3 && datosParametros.getTipoArchivo().intValue() ==2){
				p +="RepPagadasDevengarCaDetalle.jrxml";
			}
			InputStream reportePagDev = this.getClass().getResourceAsStream(p);
				List<CaReportesPagDevengarDTO> caPagadasDevengarDTOLista =  reporteBaseCompensacionesDao.obtenerRepPagadasDevengar(datosParametros);
				JasperReport reporteDatosContrato = JasperCompileManager.compileReport(reportePagDev);					
				JRBeanCollectionDataSource datosAgenteDS = new JRBeanCollectionDataSource(caPagadasDevengarDTOLista);
		
			GestorReportes ges = new GestorReportes();
			transporteImpresionDTO = new TransporteImpresionDTO();
			byte[] d = ges.generaReporte(ConstantesReporte.TIPO_XLS, reporteDatosContrato, null, datosAgenteDS);
			transporteImpresionDTO.setByteArray(d);
		} catch (Exception e) {
			throw new RuntimeException("[imprimirReportesPagadasDevengar]",e);
		}
		return transporteImpresionDTO;
	}
	/*
	public TransporteImpresionDTO imprimirReportesPagoSaldoCompensaciones(DatosCompensacionPagoSaldosDTO datosParametros, Locale locales){
		TransporteImpresionDTO transporteImpresionDTO = null;
		try{
			String p = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/";
			p +="repPagoSaldo.jrxml";
			
			List<CompensacionPagoSaldosDTO> ListaPagoSaldos = ReporteBaseCompensacionesDao.obtenerPagoSaldoCompensacion(datosParametros);
			
			
		}
	}
	*/
	@SuppressWarnings("rawtypes")
	@Override
	public TransporteImpresionDTO getExcel(List dataSourceList, String fileNameResource,String formatoSalida) {
		return getExcel(dataSourceList, fileNameResource, null, formatoSalida);
	}
	
	/**
	* Este metodo genera un reporte con multiples hojas apartir de una platilla JRXML y lo regresa
	 * cada hoja necesita un JRBeanCollectionDataSource el cual se le pasa en el mapa de los parametros con el nombre
	 * de "dataSource" si no lo ponen asi, no va funcionar
	 * tambien necesita que los reportes que van a generar tenga activada la opcion 
	 * ignore pagitation
	 * @martin
	 */
	@SuppressWarnings("rawtypes")
	@Override
	public TransporteImpresionDTO getExcel( List dataSourceList,
			List<Map<String, Object>> params, String... fileNameResource) {

		TransporteImpresionDTO respuesta = new TransporteImpresionDTO();
		try {
			JasperReport[] reportes = new JasperReport[fileNameResource.length];
			JRBeanCollectionDataSource collectionDS = new JRBeanCollectionDataSource(
					dataSourceList);
			for (int x = 0; x<fileNameResource.length; x++){
				String plantilla = Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, fileNameResource[x]);
				InputStream reportInputStream = this.getClass()
				.getResourceAsStream(plantilla);
				JasperReport reporte = JasperCompileManager
				.compileReport(reportInputStream);
				reportes[x] = reporte;
			}
			GestorReportes gestorReportes = new GestorReportes();
			byte[] reporteByteArray = gestorReportes.generaReporte(
					ConstantesReporte.TIPO_XLS, collectionDS, params, reportes);
			respuesta.setByteArray(reporteByteArray);

		} catch (Exception e) {
			throw new RuntimeException("[getExcel]", e);
		}

		return respuesta;
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public TransporteImpresionDTO getExcel(List dataSourceList,
			String fileNameResource, Map<String, Object> params, String formatoSalida) {
		TransporteImpresionDTO respuesta = new TransporteImpresionDTO();
		try {
			if (dataSourceList == null || dataSourceList.isEmpty()) {
				throw new RuntimeException("DataSource empty");
			}
			JRBeanCollectionDataSource collectionDS = new JRBeanCollectionDataSource(
					dataSourceList);

			String plantilla = Utilerias.getMensajeRecurso(
					SistemaPersistencia.ARCHIVO_RECURSOS, fileNameResource);
			InputStream reportInputStream = this.getClass()
					.getResourceAsStream(plantilla);
			JasperReport reporte = JasperCompileManager
					.compileReport(reportInputStream);

			GestorReportes gestorReportes = new GestorReportes();
			
			if (params != null) {
				params.put(JRParameter.REPORT_VIRTUALIZER,  new JRGzipVirtualizer(1));
			}else {
				params = new HashMap<String, Object>();
				params.put(JRParameter.REPORT_VIRTUALIZER,  new JRGzipVirtualizer(1));
			}
			byte[] reporteByteArray = null;
			if(ConstantesReporte.TIPO_TXT.equals(formatoSalida)){
				LOG.info("[ImpresionesServiceImpl.getExcel] formato del reporte "+formatoSalida);
				 reporteByteArray = gestorReportes.generaReporte(
						ConstantesReporte.TIPO_TXT, reporte, params, collectionDS);
			}else if (ConstantesReporte.TIPO_XLS.equals(formatoSalida)){
				LOG.info("[ImpresionesServiceImpl.getExcel] formato del reporte "+formatoSalida);
				 reporteByteArray = gestorReportes.generaReporte(
							ConstantesReporte.TIPO_XLS, reporte, params, collectionDS);
			}else if (ConstantesReporte.TIPO_XLSX.equals(formatoSalida)){
				LOG.info("[ImpresionesServiceImpl.getExcel] formato del reporte "+formatoSalida);
				 reporteByteArray = gestorReportes.generaReporte(
							ConstantesReporte.TIPO_XLSX, reporte, params, collectionDS);	 
			}else if (ConstantesReporte.TIPO_CSV.equals(formatoSalida)){
				LOG.info("[ImpresionesServiceImpl.getExcel] formato del reporte "+formatoSalida);
				 reporteByteArray = gestorReportes.generaReporte(
							ConstantesReporte.TIPO_CSV, reporte, params, collectionDS);
			}else if (ConstantesReporte.TIPO_PDF.equals(formatoSalida)){
				LOG.info("[ImpresionesServiceImpl.getExcel] formato del reporte "+formatoSalida);
				 reporteByteArray = gestorReportes.generaReporte(
							ConstantesReporte.TIPO_PDF, reporte, params, collectionDS);
			}

			respuesta.setByteArray(reporteByteArray);

		} catch (Exception e) {
			throw new RuntimeException("[getExcel]", e);
		}

		return respuesta;
	}
	
	@Override
	@SuppressWarnings("rawtypes")
	public TransporteImpresionDTO getReporteEfectividad( List dataSourceList,
			Map<String, Object> params) {
		TransporteImpresionDTO respuesta = new TransporteImpresionDTO();
		GestorReportes gestorReportes = new GestorReportes();

		try {
			String plantilla1 = Utilerias.getMensajeRecurso(
					SistemaPersistencia.ARCHIVO_RECURSOS, 
					"midas.agente.reporte.efectividad.plantilla1.archivo.nombre");
			String plantilla2 = Utilerias.getMensajeRecurso(
					SistemaPersistencia.ARCHIVO_RECURSOS, 
					"midas.agente.reporte.efectividad.plantilla2.archivo.nombre");
			//Se genera la primera hoja de excel
			Connection connection = null;
			JasperPrint jasperPrint = null;
			JasperPrint jasperPrint2 = null;
	
			InputStream caratulaExcepcionesStream = null;
			JasperReport reporteExcepciones = null;
			try{			
				Context ctx = new InitialContext();
				DataSource ds = (DataSource) ctx.lookup(StoredProcedureHelper.DATASOURCE_MIDAS);
				connection = ds.getConnection();
				caratulaExcepcionesStream = this.getClass().getResourceAsStream(plantilla1);
				JasperDesign design = JRXmlLoader.load(caratulaExcepcionesStream);
				reporteExcepciones = JasperCompileManager.compileReport(design);	
				jasperPrint = JasperFillManager.fillReport(reporteExcepciones,
						params, connection);
			}catch(Exception e){
				LOG.error("Error al generar la primer pestania del archivo", e);
			}finally{
		    	DbUtils.closeQuietly(connection);
			}

			try {
	
				JRBeanCollectionDataSource collectionDS = new JRBeanCollectionDataSource(
						dataSourceList);
				
				//Se genera la segunda hoja de excel
				caratulaExcepcionesStream = this.getClass().getResourceAsStream(plantilla2);	
				JasperDesign design2 = JRXmlLoader.load(caratulaExcepcionesStream);
				reporteExcepciones = JasperCompileManager.compileReport(design2);
				jasperPrint2 = JasperFillManager.fillReport(reporteExcepciones,
						params, collectionDS);
			}catch(Exception e){
				LOG.error("Error al generar la primer pestania del archivo", e);
			}
	
				List<JasperPrint> pages = new ArrayList<JasperPrint>(1);
				pages.add(jasperPrint);
				pages.add(jasperPrint2);
				
				respuesta.setByteArray(gestorReportes.generaReporte(ConstantesReporte.TIPO_XLS, pages));
				respuesta.setGenericInputStream(new ByteArrayInputStream(respuesta.getByteArray()));
				respuesta.setContentType("application/vnd.ms-excel");
				respuesta.setFileName("Reporte_Efectividad_Entrega.xls");
			} catch (Exception e) {
				LOG.error("Error al generar el archivo excel", e);
			}

		return respuesta;
	}
	
	/**
	 * Genera un excel austero con apache poi
	 */
	@Override
	public InputStream getExcel(List dataSourceList) throws IllegalArgumentException, FileNotFoundException, InvalidFormatException, IllegalAccessException, InvocationTargetException, IOException {
		if (dataSourceList == null || dataSourceList.isEmpty()) {
			throw new RuntimeException("DataSource empty");
		}
		MidasBaseReporteExcel sheet = new MidasBaseReporteExcel();
		sheet.setBookName("reporteExcel_" + Calendar.getInstance().getTimeInMillis());
		sheet.setDataSource(dataSourceList);
		InputStream file =  baseReporteService.generateExcelVeryLong(sheet);
		if (file != null) {
			return file;
		}
		return null;
	}
	
	@Override
	public TransporteImpresionDTO imprimirPagarePrestamoAnticipo(PagarePrestamoAnticipo pagare,Locale locale) {
		byte[] pdfPagare = null;
		Map<String, Object> parametros = new HashMap<String, Object>(1);		
		List<PagarePrestamoAnticipoImpresion> datosPagare = llenarDatosPagare(pagare);
		
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		if(locale != null){
			parametros.put(JRParameter.REPORT_LOCALE, locale);
			parametros.put("P_IMAGE_LOGO", SistemaPersistencia.AFIRME_SEGUROS);				
		}		
		try{					
			InputStream reportePagarePrestamoAnticipo = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/impresiones/jrxml/pagarePrestamoAnticipo.jrxml");			
			JasperReport reportePagare = JasperCompileManager.compileReport(reportePagarePrestamoAnticipo);					
			JRBeanCollectionDataSource datosPagareDS = new JRBeanCollectionDataSource(datosPagare);
			pdfPagare = JasperRunManager.runReportToPdf(reportePagare, parametros, datosPagareDS);
			transporteImpresionDTO.setByteArray(pdfPagare);
			
		 return transporteImpresionDTO;
		 	
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
	}
	
	private List<PagarePrestamoAnticipoImpresion> llenarDatosPagare(PagarePrestamoAnticipo pagare){
	
		PagarePrestamoAnticipoImpresion pagarePrestamoAnticipo = new PagarePrestamoAnticipoImpresion();
		try {
			pagarePrestamoAnticipo = pagarePrestamoAnticipoService.infoImpresionPagare(pagare);
			
			pagarePrestamoAnticipo.setCapitalInicialLetra(Utilerias.convertNumberToLetter(pagare.getAbonoPorPagare()));
			pagarePrestamoAnticipo.setInteresOrdinarioLetra(Utilerias.convertNumberToLetterPorciento(pagarePrestamoAnticipo.getInteresOrdinario()));
			pagarePrestamoAnticipo.setLugarSuscripcion("MONTERREY, NUEVO LEÓN");
			
		} catch (Exception e) {
			LOG.error(e);
		}		
		
		List<PagarePrestamoAnticipoImpresion> datosPagarePrestamoAnticipo = new ArrayList<PagarePrestamoAnticipoImpresion>();
		datosPagarePrestamoAnticipo.add(pagarePrestamoAnticipo);
		return datosPagarePrestamoAnticipo;
		
	}
	
	@Override
	public TransporteImpresionDTO imprimirPagarePrincipalPrestamoAnticipo(ConfigPrestamoAnticipo config,Locale locale) {
		byte[] pdfPagare = null;
		Map<String, Object> parametros = new HashMap<String, Object>();		
		List<PagarePrestamoAnticipoImpresion> datosPagare = llenarDatosPagarePrincipal(config);
		
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		if(locale != null){
			parametros.put(JRParameter.REPORT_LOCALE, locale);
			parametros.put("P_IMAGE_LOGO", SistemaPersistencia.AFIRME_SEGUROS);				
		}		
		try{					
			InputStream reportePagarePrestamoAnticipo = this.getClass().getResourceAsStream(
					"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/pagarePrestamoAnticipo.jrxml");			
			JasperReport reportePagare = JasperCompileManager.compileReport(reportePagarePrestamoAnticipo);					
			JRBeanCollectionDataSource datosPagareDS = new JRBeanCollectionDataSource(datosPagare);
			pdfPagare = JasperRunManager.runReportToPdf(reportePagare, parametros, datosPagareDS);
			transporteImpresionDTO.setByteArray(pdfPagare);
			
		 return transporteImpresionDTO;
		 	
		}catch(Exception ex){
			throw new RuntimeException(ex);
		}
	}
	
	private List<PagarePrestamoAnticipoImpresion> llenarDatosPagarePrincipal(ConfigPrestamoAnticipo config){
	
		PagarePrestamoAnticipoImpresion pagarePrestamoAnticipo = new PagarePrestamoAnticipoImpresion();
		try {
			pagarePrestamoAnticipo = pagarePrestamoAnticipoService.infoImpresionPagarePrincipal(config);
			
			pagarePrestamoAnticipo.setCapitalInicialLetra(Utilerias.convertNumberToLetter(config.getImporteOtorgado()));
			pagarePrestamoAnticipo.setInteresOrdinarioLetra(Utilerias.convertNumberToLetterPorciento(config.getPcteInteresOrdinario()));
			pagarePrestamoAnticipo.setLugarSuscripcion("MONTERREY, NUEVO LEÓN");
			
		} catch (Exception e) {
			LOG.error(e);
		}		
		
		List<PagarePrestamoAnticipoImpresion> datosPagarePrestamoAnticipo = new ArrayList<PagarePrestamoAnticipoImpresion>();
		datosPagarePrestamoAnticipo.add(pagarePrestamoAnticipo);
		return datosPagarePrestamoAnticipo;
		
	}
	
	public static class  GestorReportes extends MidasBaseReporte{
		
	}
	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }
	
	@Override
	public TransporteImpresionDTO imprimirTablaAmortizacion(ConfigPrestamoAnticipo configPrestamoAnticipo, Locale locale){
		byte[] pdfTablaAmortizacion = null;
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		try {
			InputStream  gridTablaAmortizacion= this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/impresiones/jrxml/gridTablaAmortizacion.jrxml");
			JasperReport tablaAmortizacion = JasperCompileManager.compileReport(gridTablaAmortizacion);
			
			InputStream detalleTablaAmortizacion = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/impresiones/jrxml/tablaAmortizacion.jrxml");
			JasperReport principalTablaAmortizacion = JasperCompileManager.compileReport(detalleTablaAmortizacion);

			Map<String, Object> parameters = new HashMap<String, Object>();
			List<PagarePrestamoAnticipo> reporteDataSourceGrid = new ArrayList<PagarePrestamoAnticipo>();
			List <ReporteIngresosAgente> listaReporete = new ArrayList<ReporteIngresosAgente>();
			ReporteIngresosAgente reporte = new ReporteIngresosAgente();
			reporteDataSourceGrid = pagarePrestamoAnticipoService.loadByIdConfigPrestamo(configPrestamoAnticipo);
			//***************se llenan los datos del reporte
			double objImporteTotal=0;
			for(PagarePrestamoAnticipo obj: reporteDataSourceGrid){
				objImporteTotal = obj.getImportePago()+objImporteTotal;
			}
			reporte=configPrestamoAnticipoService.obtenerDatosReporte(configPrestamoAnticipo);
			listaReporete.add(reporte);
			//***********************
			if (locale != null){
				JRBeanCollectionDataSource dsInciso = new JRBeanCollectionDataSource(listaReporete);
				parameters.put(JRParameter.REPORT_LOCALE, locale);
				parameters.put("gridDataSource", reporteDataSourceGrid);
				parameters.put("gridReport", tablaAmortizacion);
				parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(2));
				pdfTablaAmortizacion = JasperRunManager.runReportToPdf(principalTablaAmortizacion,parameters,dsInciso);
				transporteImpresionDTO.setByteArray(pdfTablaAmortizacion);
			}
		}catch (JRException ex) {
			throw new RuntimeException(ex);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return transporteImpresionDTO;
	}
	@Override
	public TransporteImpresionDTO imprimirComprobanteCargo(ConfigCargos configCargos, Locale locale) {

		byte[] pdfTablaAmortizacion = null;
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		try {
			JasperReport detalleProgramacionCargos = getJasperReport(
					"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/detalleProgramacionCargos.jrxml");
			
			JasperReport principalDetalleCargo = getJasperReport(
					"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/comprobanteAcuerdosCargos.jrxml");

			Map<String, Object> parameters = new HashMap<String, Object>();
			
			List<DetalleCargos> reporteDataSourceGrid = new ArrayList<DetalleCargos>();
			List <ConfigCargos> listaReporte = new ArrayList<ConfigCargos>();
			ConfigCargos reporte = new ConfigCargos();
			reporteDataSourceGrid = detalleCargosService.loadByIdConfigCargos(configCargos); 
			
			Agente agente = new Agente();
//			agente = agenteMidasService.loadById(agente);
			reporte=configCargosService.loadById(configCargos);
			agente = entidadService.findById(Agente.class, reporte.getAgente().getId());
			reporte.setAgente(null);
			if(!agente.getPersona().getDomicilios().isEmpty()){
				if(agente.getPersona().getDomicilios().get(0)!=null){
					Domicilio dom = agente.getPersona().getDomicilios().get(0);
					String domic = dom.getCalleNumero()+" "+dom.getNombreColonia()+" "+" "+dom.getCodigoPostal()+" "+dom.getCiudad()+" "+dom.getEstado();
					agente.setDireccionAgente(domic);
				}
			}
			reporte.setAgente(agente);
			ValorCatalogoAgentes tipAgent = entidadService.findById(ValorCatalogoAgentes.class, reporte.getAgente().getIdTipoAgente());
			reporte.setTipoAgente(tipAgent);

			listaReporte.add(reporte);
			//***********************
			if (locale != null){
				JRBeanCollectionDataSource dsCargo = new JRBeanCollectionDataSource(listaReporte);
				parameters.put(JRParameter.REPORT_LOCALE, locale);
				parameters.put("gridDataSource", reporteDataSourceGrid);
				parameters.put("gridReport", detalleProgramacionCargos);
				parameters.put(JRParameter.REPORT_VIRTUALIZER, new JRGzipVirtualizer(2));
				pdfTablaAmortizacion = JasperRunManager.runReportToPdf(principalDetalleCargo,parameters,dsCargo);
				transporteImpresionDTO.setByteArray(pdfTablaAmortizacion);
			}
		}catch (JRException ex) {
			throw new RuntimeException(ex);
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return transporteImpresionDTO;
	}
	
	private void insertIntoAgenteSupport(List<AgenteView> listaAgentes){
		StringBuilder queryInsertString = new StringBuilder();
		for (AgenteView agente : listaAgentes) {
			queryInsertString.append("INSERT INTO MIDAS.AGENTEIDSUPPORT (IDAGENTE) VALUES("+agente.getId()+") ");
			Query queryInsert = entityManager.createNativeQuery(queryInsertString.toString());
			queryInsert.executeUpdate();
			queryInsertString.setLength(0);
		}
		entityManager.flush();
	}
	
	@Override
	public List<DatosAgenteIngresosView> llenarDatosAgenteIngresos(
			String anio, String mes, Long rangoInicio, Long rangoFin,
			Integer tipoReporte) {
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_repIngresosAgente";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;
		List<DatosAgenteIngresosView> listaAgentesIngresos = new ArrayList<DatosAgenteIngresosView>();

		try {
			String[] colsTemp = {"id","idGerencia","gerencia",
								 "idEjecutivo","ejecutivo","idPromotoria",
								 "promotoria","claveAgente","nombreCompleto",
								 "origen","anioMes","fechaMovimiento",
								 "importeBaseCalculo","importeIva","importeIvaRetenido",
								 "importeISR","importeEstatalRetenido","email","tr","importeTotal"};
			String[] propsTemp = {"id","idGerencia","gerencia",
					  			  "idEjecutivo","ejecutivo","idPromotoria",
								  "promotoria","claveAgente","nombreCompleto",
								  "origen","anioMes","fechaMovimiento",
								 "importeBaseCalculo","importeIva","importeIvaRetenido",
								  "importeISR","importeEstatalRetenido","email","tr","importeTotal"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(
					DatosAgenteIngresosView.class.getCanonicalName(),
					propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("anio", val(anio));
			storedHelper.estableceParametro("mes", val(mes));
			storedHelper.estableceParametro("agenteInicio", val(rangoInicio));
			storedHelper.estableceParametro("agenteFin", val(rangoFin));
			listaAgentesIngresos = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (!listaAgentesIngresos.isEmpty()) {
			return listaAgentesIngresos;
		}
		return null;
	}

	@Override
	public List<DatosAgentePrimaPagadaPorRamoView> llenarDatosAgentePrimaPagadaPorRamo(
			List<Agente> listaAgentes, Date fechaCorteInicio,
			Date fechaCorteFin, List<CentroOperacion> centroOperacionesSeleccionados,
			List<Ejecutivo> ejecutivosSeleccionados, List<Gerencia> gerenciasSeleccionadas,
			List<Promotoria> promotoriasSeleccionadas, Integer tipoReporte) {
		String id_Agentes="";
		if(listaAgentes!=null){
			id_Agentes = obtenerIdAgentes(listaAgentes);
		}
		List<DatosAgentePrimaPagadaPorRamoView> agentePrimaPagadaPorRamoViews = new ArrayList<DatosAgentePrimaPagadaPorRamoView>();
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_reportePrimaParXramo";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;

		try {
			String[] colsTemp = {"idAgente","nombreCompleto","idPromotoria","promotoria","idGerencia","gerencia",
					 "idCop","cop","idEjecutivo","ejecutivo","primaPagadaTotal","acumuladodanios",
					 "acumuladoautos","acumuladovida","sin_total"};
			String[] propsTemp = {"idAgente","nombreCompleto","idPromotoria","promotoria","idGerencia","gerencia",
					 "idCop","cop","idEjecutivo","ejecutivo","primaPagadaTotal","acumuladodanios",
					 "acumuladoautos","acumuladovida","sin_total"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(
					DatosAgentePrimaPagadaPorRamoView.class.getCanonicalName(),
					propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("pfechaInicio", val(f.format(fechaCorteInicio)));
			storedHelper.estableceParametro("pfechaFin", val(f.format(fechaCorteFin)));
			storedHelper.estableceParametro("pid_centroOperacion", val((centroOperacionesSeleccionados==null || centroOperacionesSeleccionados.isEmpty())?null:centroOperacionesSeleccionados.get(0).getId()));
			storedHelper.estableceParametro("pid_gerencia", val((gerenciasSeleccionadas==null || gerenciasSeleccionadas.isEmpty())?null:gerenciasSeleccionadas.get(0).getId()));
			storedHelper.estableceParametro("pid_ejecutivo", val((ejecutivosSeleccionados == null || ejecutivosSeleccionados.isEmpty())?null:ejecutivosSeleccionados.get(0).getId()));
			storedHelper.estableceParametro("pid_promotoria", val((promotoriasSeleccionadas == null || promotoriasSeleccionadas.isEmpty())?null:promotoriasSeleccionadas.get(0).getId()));
			storedHelper.estableceParametro("pid_agente_inicio", val(id_Agentes));
			agentePrimaPagadaPorRamoViews = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (!agentePrimaPagadaPorRamoViews.isEmpty()) {
			return agentePrimaPagadaPorRamoViews;
		}
		return null;
	}

	public String obtenerIdAgentes(List<Agente> listaAgentes) {
		StringBuilder idAgentes = new StringBuilder("");
		int contador=0;
		for(Agente objAgente:listaAgentes){
			if(contador ==0){
				idAgentes.append(Long.toString(objAgente.getId()));
				contador++;
			}else{
				idAgentes.append(",").append(Long.toString(objAgente.getId()));
			}
		}
		return idAgentes.toString();
	}

	@Override
	public List<DatosAgenteTopAgenteView> llenarDatosAgenteTopAgente(
			List<Agente> listaAgentes,
			List<CentroOperacion> centroOperacionesSeleccionados, List<Ejecutivo> ejecutivosSeleccionados,
			List<Gerencia> gerenciasSeleccionadas,List<Promotoria> promotoriasSeleccionadas
			,String anio,String mes, Integer tipoReporte,Integer topAgente,Date fechaInicio, Date fechaFin) {
		
		String idsAgentes = null;
	
		if(listaAgentes!=null && !listaAgentes.isEmpty()){
			idsAgentes = obtenerIdAgentes(listaAgentes);
		}
		if(mes!=null){
			if(Integer.parseInt(mes)<=9){
				mes = "0"+mes;
			}
		}
		String sp="MIDAS.PKGREPORTES_AGENTES.stp_reporteTopAgente";
		StringBuilder propiedades=new StringBuilder("");
		StringBuilder columnas=new StringBuilder("");
		String[] cols=null;
		String[] props=null;
		StoredProcedureHelper storedHelper = null;
		List<DatosAgenteTopAgenteView> resultList = new ArrayList<DatosAgenteTopAgenteView>();
		try {
			
			String[] colsTemp={
				"id",	
				"idAgente",
				"gerencia",
				"nombreCompleto",
				"idTipoAgente",
				"acumuladothisYear",
				"acumuladoBeforeYear",
				"acumuladoThisMonthThisYear",
				"acumuladoThisMonthLastYear",
				"totalCiaMesAct",
				"totalCiaAcumAnioAct",
				"totalCiaMesAnt",
				"totalCiaAcumAnioAnt",
				"pctCiaAcumMesAct",
				"pctCiaAcumAnioAct",
				"totalAcumAgentes",
				"pctPromAcumAnioActual"
//				"fechaMovimiento"
			};
			String[] propsTemp={
					"id",
					"idAgente",
					"gerencia",
					"nombreCompleto",
					"idTipoAgente",
					"acumuladothisYear",
					"acumuladoBeforeYear",
					"acumuladoThisMonthThisYear",
					"acumuladoThisMonthLastYear",
					"totalCiaMesAct"
					,"totalCiaAcumAnioAct",
					"totalCiaMesAnt",
					"totalCiaAcumAnioAnt",
					"pctCiaAcumMesAct",
					"pctCiaAcumAnioAct",
					"totalAcumAgentes",
					"pctPromAcumAnioActual"
//					"fechaMovimiento"
			};
			cols=colsTemp;
	        props=propsTemp;
	        
	        for(int i=0;i<cols.length;i++){
				String columnName=cols[i];
				String propertyName=props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if(i<(cols.length-1)){
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(DatosAgenteTopAgenteView.class.getCanonicalName(),propiedades.toString(),columnas.toString());
			storedHelper.estableceParametro("topAgente", val(topAgente));
			storedHelper.estableceParametro("anio", val(anio));
			storedHelper.estableceParametro("mes", val(mes));
			storedHelper.estableceParametro("id_centroOperacion", val((centroOperacionesSeleccionados==null || centroOperacionesSeleccionados.isEmpty())?null:centroOperacionesSeleccionados.get(0).getId()));
			storedHelper.estableceParametro("id_gerencia", val((gerenciasSeleccionadas==null || gerenciasSeleccionadas.isEmpty())?null:gerenciasSeleccionadas.get(0).getId()));
			storedHelper.estableceParametro("id_ejecutivo", val((ejecutivosSeleccionados == null || ejecutivosSeleccionados.isEmpty())?null:ejecutivosSeleccionados.get(0).getId()));
			storedHelper.estableceParametro("id_promotoria", val((promotoriasSeleccionadas == null || promotoriasSeleccionadas.isEmpty())?null:promotoriasSeleccionadas.get(0).getId()));
			storedHelper.estableceParametro("id_agente", val(idsAgentes));
			resultList = storedHelper.obtieneListaResultados();
		
			
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (!resultList.isEmpty()) {
			return resultList;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DatosDetPrimas> llenarDatosDetPrimas(CentroOperacion centroOperacion, Gerencia gerencia, Ejecutivo ejecutivo, Promotoria promotoria, ValorCatalogoAgentes tipoAgentes, String fechaInicio, String fechafin, Agente agente) {
		List<DatosDetPrimas> result = new ArrayList<DatosDetPrimas>();
		result = new ArrayList<DatosDetPrimas>();
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_DetallePrimaSupport";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;
		String parametrosTrace;

		try {
			String[] colsTemp = { "fechaMovimiento", "id_supervisoria",
					"nom_supervisoria", "id_agente", "nom_agente",
					"num_poliza", "num_endoso", "num_folio_rbo",
					"id_ramo_contable", "id_subr_contable", "desc_moneda",
					"imp_prima_neta", "imp_rcgos_pagofr", "imp_prima_total",
					"pct_comis_agte", "imp_comis_agte", "imp_iva_acred",
					"imp_iva_ret", "imp_isr", "imp_ret_est", "desc_movto", "dia", "contratante",
					"forma_pago", "totalporpoliza", "tipo_cambio",
					"num_fianza", "f_fin_vig_fianza", "descripcionramo","sdo_inicial"};

			String[] propsTemp = { "fechaMovimiento", "idSupervisoria",
					"nombreSupervisoria", "idAgente", "nombreAgente",
					"numeroPoliza", "numeroEndoso", "numeroFolioRubro",
					"idRamo", "idSubramo", "moneda",
					"importePrimaNeta", "importeRecargosPagoFraccionado", "importePrimaTotal",
					"porcentajeComisionAgente", "importeComisionAgente", "importeIvaAcreditado",
					"importeIvaRetenido", "importeIsr", "importeEstatalRetenido", "descripcionMovimiento", "dia", "contratante",
					"formaPago", "importeTotalPorPoliza", "importeTipoCambio",
					"numeroFianza", "fechaFinFianza", "ramo", "importeSaldoInicialPeriodo"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(DatosDetPrimas.class.getCanonicalName(),propiedades.toString(), columnas.toString());
//			storedHelper.estableceParametro("pAnioMes", val(anioMes));
			storedHelper.estableceParametro("pfechainicio", val(fechaInicio));
			storedHelper.estableceParametro("pfechafinal", val(fechafin));
			storedHelper.estableceParametro("pidAgente", val(agente.getId()));
			storedHelper.estableceParametro("pIdcentroOperacion", val(centroOperacion.getId()));
			storedHelper.estableceParametro("pIdgerencia", val(gerencia.getId()));
			storedHelper.estableceParametro("pIdejecutivo", val(ejecutivo.getId()));
			storedHelper.estableceParametro("pIdpromotoria", val(promotoria.getId()));
			storedHelper.estableceParametro("pIdtipoAgentes", val(tipoAgentes.getId()));
			
			result = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (!result.isEmpty()) {
			return result;
		}
		return null;
	}

	@Override
	public List<DatosReportePreviewBonos> llenarDatosPreviewBonos(Long centroOperacion, Long gerencia, Long ejecutivo,
			Long promotoria, ValorCatalogoAgentes tipoAgentes, String anio, String mes, Agente agente, ConfigBonos bono, Long idCalculo) {
		List<DatosReportePreviewBonos> resultList = new ArrayList<DatosReportePreviewBonos>();
		
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_repPreviewBonos";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;

		try {
			String[] colsTemp = {"promotor","clasificacion","acumulada","mes","mesActual"};
			String[] propsTemp = {"promotor","clasificacion","acumulada","mes","mesActual"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(DatosReportePreviewBonos.class.getCanonicalName(),propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("anio", val(anio));
			storedHelper.estableceParametro("mes", val(mes));
//			storedHelper.estableceParametro("pCentroOperacion", val(centroOperacion));
//			storedHelper.estableceParametro("pGerencia", val(gerencia));
//			storedHelper.estableceParametro("pEjecutivo", val(ejecutivo));
//			storedHelper.estableceParametro("pPromotoria", val(promotoria));
//			storedHelper.estableceParametro("pTipoAgentes", val((tipoAgentes!=null && tipoAgentes.getId() != null)?tipoAgentes.getId():null));
//			storedHelper.estableceParametro("pIdAgente", val((agente!=null && agente.getId()!=null)?agente.getId():null));
//			storedHelper.estableceParametro("pidBono", val((bono!=null && bono.getId()!=null)?bono.getId():null));
//			storedHelper.estableceParametro("pIdCalculo", val(idCalculo));
			resultList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if(resultList!=null && !resultList.isEmpty()){
			return resultList;
		}
		return null;
	}
	
	private void addCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" and ");
	}
	
	private void addOrCondition(StringBuilder queryString,String conditional){
		if(!queryString.toString().contains("where")){
			queryString.append(" where ");
		}
		queryString.append(conditional);
		queryString.append(" or ");
	}
	
	private String getQueryString(StringBuilder queryString){
		String query=queryString.toString();
		if(query.endsWith(" and ")){
			query=query.substring(0,(query.length())-(" and ").length());
		}else if (query.endsWith(" or ")){
			query=query.substring(0,(query.length())-(" or ").length());
		}
		return query;
	}
	@Override
	public List<DatosAgenteDatosView> llenarDatosAgenteDatosAgente(
			Date fechaInicial, Date fechaFinal,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long rangoInicio, Long rangoFin) {
		StringBuilder queryString = new StringBuilder();

		String fecha1 = null;
		String fecha2 = null;
		DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
		if(fechaInicial!=null){	
			fecha1 = df.format(fechaInicial);
		}
		if(fechaFinal!= null){	
			fecha2 = df.format(fechaFinal);
		}
		
		queryString.append(" select ");
		queryString.append(" a.id as id, ");
		queryString.append(" cop.id as idCentroOperacion, ");
		queryString.append(" cop.DESCRIPCION as nom_centroOperacion, ");
		queryString.append(" ger.ID as idGenrencia, ");
		queryString.append(" ger.DESCRIPCION as nomGenrencia, ");
		queryString.append(" eje.ID as idEjecutivo, ");
		queryString.append(" perEje.NOMBRECOMPLETO as nomEjecutivo, ");
		queryString.append(" pro.ID as idPromotoria, ");
		queryString.append(" pro.DESCRIPCION as nomPromotoria, ");
		queryString.append(" agePro.IDAGENTE as idAgentePromotoria, ");
		queryString.append(" perAgePro.NOMBRECOMPLETO as nomAgentePromotoria, ");
		queryString.append(" a.IDAGENTE as idAgente, ");
		queryString.append(" perAge.NOMBRECOMPLETO as nombre, ");
		queryString.append(" to_char(perAge.FECHANACIMIENTO,'dd/mon/yyyy','NLS_DATE_LANGUAGE=Spanish') as fechaNacimiento, ");
		queryString.append(" dom.CALLENUMERO as calleAgente, ");
		queryString.append(" dom.COLONIA as coloniaAgente, ");
		queryString.append(" dom.CIUDAD as ciudadAgente, ");
		queryString.append(" dom.ESTADO as estadoAgente, ");
		queryString.append(" dom.CODIGOPOSTAL as codigoPostalAgente, ");
		queryString.append(" perAge.EMAIL as e_mail, ");
		queryString.append(" to_char(tl1.FECHAHORAACTUALIZACION,'dd/mon/yyyy hh24:MI:SS','NLS_DATE_LANGUAGE=Spanish') as f_ultima_actualizacion, ");
		queryString.append(" tl1.USUARIOACTUALIZACION as id_usuario, ");
		queryString.append(" perAge.CODIGORFC as rfc, ");
		queryString.append(" perAge.IDPERSONA as id_Persona, ");
		queryString.append(" case when perAge.CLAVETIPOPERSONA = 1 ");
		queryString.append(" then 'PF' ");
		queryString.append(" else 'PM' ");
		queryString.append(" end as personalidadJuridica, ");
		queryString.append(" tipoAgente.valor as cve_t_agente, ");
		queryString.append(" to_char(a.FECHAAUTORIZACIONCEDULA,'dd/mon/yyyy','NLS_DATE_LANGUAGE=Spanish') as f_autorizacion, ");
		queryString.append(" to_char(a.FECHAVENCIMIENTOCEDULA,'dd/mon/yyyy','NLS_DATE_LANGUAGE=Spanish') as f_vencto_autor, ");
		queryString.append(" trim(tipoCedula.CLAVE)||' - '||tipoCedula.VALOR as cedula, ");
		queryString.append(" a.PORCENTAJEDIVIDENDOS as pct_dividendos, ");
		queryString.append(" to_char(a.FECHAALTA,'dd/mon/yyyy','NLS_DATE_LANGUAGE=Spanish') as f_alta, ");
		queryString.append(" to_char(tlsusp1.FECHAHORAACTUALIZACION,'dd/mon/yyyy hh24:MI:SS','NLS_DATE_LANGUAGE=Spanish') as f_baja, ");
		queryString.append(" situacionAgente.VALOR as sit_agente, ");
		queryString.append(" motsituacionAgente.VALOR as mot_sit_agte, ");
		queryString.append(" banco.NOM_BANCO as banco, ");
		queryString.append(" cpb.DESCRIPCION as tipoProdBancario, ");
		queryString.append(" pb.NUMEROCUENTA as numCuenta, ");
		queryString.append(" pb.NUMEROCLABE numCuentaClave, ");
		queryString.append(" case when a.CLAVEGENERACHEQUE = 1 then 'V' else 'F' end as b_genera_cheque, ");
		queryString.append(" case when a.CLAVECONTABILIZACOMISION =1 then 'V' else 'F' end as b_contabiliza_comision, ");
		queryString.append(" case when a.CLAVECOMISION = 1 then 'V' else 'F' end as b_comision, ");
		queryString.append(" case when a.CLAVEIMPRIMEESTADOCTA = 1 then 'V' else 'F' end as b_impr_edocta, ");
		queryString.append(" agentereclutador.IDAGENTE as id_agte_reclutad, ");
		queryString.append(" perAgtRec.NOMBRECOMPLETO as nom_agte_reclutad, ");
		queryString.append(" perAge.CURP as curp, ");
		queryString.append(" ent.entretenimiento as entretenimiento ");
		queryString.append(" from MIDAS.toAgente a ");
		queryString.append(" LEFT join MIDAS.toAgente agenteReclutador on agentereclutador.ID = a.id ");
		queryString.append(" left join MIDAS.vw_persona perAgtRec on perAgtRec.IDPERSONA = agenteReclutador.IDPERSONA ");
		queryString.append(" left join midas.toProductoBancarioAgente pb on pb.AGENTE_ID = a.id and CLAVEDEFAULT = 1 ");
		queryString.append(" left join midas.TCPRODUCTOBANCARIO cpb on cpb.ID = PRODUCTOBANCARIO_ID ");
		queryString.append(" left join seycos.ING_BANCO banco on banco.ID_BANCO = cpb.IDBANCO ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes motsituacionAgente on motsituacionAgente.ID = a.IDMOTIVOESTATUSAGENTE ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes situacionAgente on situacionAgente.ID = a.IDSITUACIONAGENTE ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoCedula on tipoCedula.ID = a.IDTIPOCEDULAAGENTE ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes tipoAgente on tipoAgente.ID = a.IDTIPOAGENTE ");
		queryString.append(" inner join MIDAS.vw_persona perAge on perAge.IDPERSONA = a.IDPERSONA ");
		queryString.append(" left join MIDAS.toPromotoria pro on pro.ID = a.IDPROMOTORIA ");
		queryString.append(" left join MIDAS.toAgente agePro on agePro.id = pro.AGENTEPROMOTOR_ID ");
		queryString.append(" left join MIDAS.vw_persona perAgePro on perAgePro.IDPERSONA = agePro.IDPERSONA ");
		queryString.append(" left join MIDAS.toEjecutivo eje on eje.ID = pro.EJECUTIVO_ID ");
		queryString.append(" left join MIDAS.vw_persona perEje on perEje.IDPERSONA = eje.IDPERSONA ");
		queryString.append(" left join MIDAS.toGerencia ger on ger.ID = eje.GERENCIA_ID ");
		queryString.append(" left join MIDAS.toCentroOperacion cop on cop.ID = ger.CENTROOPERACION_ID ");
		queryString.append(" left join MIDAS.vw_persona perRespCop on perRespCop.IDPERSONA = cop.IDPERSONARESPONSABLE ");
		queryString.append(" left join (SELECT agente_id,TRANSLATE(Wm_Concat(' ||'||clave||'.- '||comentarios||' '),',',' ') as entretenimiento ");
		queryString.append(" FROM MIDAS.TOENTRETENIMIENTOAGENTE e ");
		queryString.append(" inner join MIDAS.toValorCatalogoAgentes ce on e.idtipoentretenimiento = ce.id ");
		queryString.append(" group by agente_id) ent on a.ID = ent.agente_id ");
		queryString.append(" left join MIDAS.VW_DOMICILIO dom on dom.IDPERSONA = perAge.IDPERSONA and dom.TIPODOMICILIO = 'PERS' ");
		queryString.append(" left join (select max(t.id) as id, t.idregistro from MIDAS.tlactualizacionagentes t where GRUPOACTUALIZACIONAGENTES_ID = 50 group by t.IDREGISTRO) tl on tl.IDREGISTRO = a.id ");
		queryString.append(" left join MIDAS.tlactualizacionagentes tl1 on tl1.ID = tl.id ");
		queryString.append(" left join (select max(t.id) as id, t.idregistro from MIDAS.tlactualizacionagentes t where GRUPOACTUALIZACIONAGENTES_ID = 50 AND COMENTARIOS LIKE'%Se ha suspendido%' group by t.IDREGISTRO ) tlsusp on tlsusp.IDREGISTRO = a.id ");
		queryString.append(" left join MIDAS.tlactualizacionagentes tlsusp1 on tlsusp1.ID = tlsusp.id ");
		queryString.append(" WHERE a.IDAGENTE is not null ");
		if(fechaInicial!=null){
			queryString.append(" AND a.FECHAALTA >= TO_DATE ('"+fecha1+"','dd/mm/yyyy') ");
		}
		if(fechaFinal!=null){
			queryString.append(" AND a.FECHAALTA <= TO_DATE ('"+fecha2+"','dd/mm/yyyy') ");
		}
		if(idCentroOperacion!=null){
				queryString.append(" AND cop.id="+idCentroOperacion);
		}
		if(idGerencia!=null){
			queryString.append(" AND ger.id="+idGerencia);
		}
		if(idEjecutivo!=null){
			queryString.append(" AND eje.id="+idEjecutivo);
		}
		if(idPromotoria!=null){
			queryString.append(" AND pro.id="+idPromotoria);
		}
		if(rangoInicio!=null){
			queryString.append(" AND a.idagente>="+rangoInicio);
		}
		if(rangoFin!=null){
			queryString.append(" AND a.idagente<="+rangoFin);
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString),
				DatosAgenteDatosView.class);
		LOG.debug(query.toString());
		List<DatosAgenteDatosView> result = query.getResultList(); 
		if (result != null && !result.isEmpty()) {
			return result;
		}
		return null;
	}

	@Override
	public List<DatosAgenteBonosGerenciaView> llenarDatosAgenteBonosGerencia(
			String anio, String mes, String mesFin, Long idCentroOperacion, Long idGerencia,
			Long idEjecutivo, Long idPromotoria, Long idAgente, Long clasificacionAgente) {

		List<DatosAgenteBonosGerenciaView> resultList = new ArrayList<DatosAgenteBonosGerenciaView>();
		
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_repDetalleBonosXgerencia";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;

		try {
			String[] colsTemp = {"id",
					"claveAgente",
					"nombreAgente",
					"primaPagadaTotal",
					"primaNetaPagSP",
					"primaNetaPagBonoSP",
					"bonoSPAL",
					"bonoPagadoSp",
					"crecimientoSp",
					"bonoPagadoCrecimientoSP",
					"primaNetaPagAutos",
					"primaNetaPagBonoAutos",
					"bonosAutosAl",
					"bonosPagAutos",
					"primaNetaPagDanios",
					"primaNetaPagBonoDanios",
					"bonoDaniosAl",
					"bonoPagadoDanios",
					"primaNetaPagTC",
					"primaNetaPagBonoTC",
					"bonoDaniosAlTC",
					"bonoPagadoTC",
					"primaNetaPagCasa",
					"primaNetaPagBonoCasa",
					"bonoDaniosALCasa",
					"bonoPagadoCasa",
					"primaNetaPagVida",
					"primaNetaPagBonoVida",
					"bonoVidaAL",
					"bonoPagadoVida"};
			String[] propsTemp = {"id",
					"claveAgente",
					"nombreAgente",
					"primaPagadaTotal",
					"primaNetaPagSP",
					"primaNetaPagBonoSP",
					"bonoSPAL",
					"bonoPagadoSp",
					"crecimientoSp",
					"bonoPagadoCrecimientoSP",
					"primaNetaPagAutos",
					"primaNetaPagBonoAutos",
					"bonosAutosAl",
					"bonosPagAutos",
					"primaNetaPagDanios",
					"primaNetaPagBonoDanios",
					"bonoDaniosAl",
					"bonoPagadoDanios",
					"primaNetaPagTC",
					"primaNetaPagBonoTC",
					"bonoDaniosAlTC",
					"bonoPagadoTC",
					"primaNetaPagCasa",
					"primaNetaPagBonoCasa",
					"bonoDaniosALCasa",
					"bonoPagadoCasa",
					"primaNetaPagVida",
					"primaNetaPagBonoVida",
					"bonoVidaAL",
					"bonoPagadoVida"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(DatosAgenteBonosGerenciaView.class.getCanonicalName(),propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("anio", val(Integer.parseInt(anio)));
			storedHelper.estableceParametro("mes", val(Integer.parseInt(mes)));
			storedHelper.estableceParametro("mesFin", val(Integer.parseInt(mesFin)));
			storedHelper.estableceParametro("id_centroOperacion", val(idCentroOperacion));
			storedHelper.estableceParametro("id_gerencia", val(idGerencia));
			storedHelper.estableceParametro("id_ejecutivo", val(idEjecutivo));
			storedHelper.estableceParametro("id_promotoria", val(idPromotoria));
			storedHelper.estableceParametro("id_agente", val(idAgente));
			storedHelper.estableceParametro("clasificacionAgente", val(clasificacionAgente));
			resultList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (resultList != null && !resultList.isEmpty()) {
			return resultList;
		}
		return null;
	}

	private void addParameter(StringBuilder queryString,String parameter,String logicOperator,Object value){
		if (value != null) {
			if (!StringUtils.contains(queryString.toString(), "WHERE")) { 
				queryString.append(" WHERE "+ parameter + logicOperator + value.toString());
			}else {
				queryString.append(" AND  "+ parameter + logicOperator + value.toString());
			}
		}
	}
	
	@Override
	public List<DatosAgenteContabilidadMMView> llenarDatosAgenteContabilidadMM(
			String anioInicio, String mesInicio, String anioFin, String mesFin,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long idAgente) {
		int index=1;
		Map<Integer,Object>params = new HashMap<Integer, Object>();
		StringBuilder queryString = new StringBuilder();
		queryString.append("SELECT /*+INDEX (valores idx_RepContaMizar_anoMes)*/  "+
								" valores.ID, "+
								" valores.FECHAMOVIMIENTO, "+
								" valores.NOM_SUPERVISORIA, "+
								" valores.IDAGENTE, "+
								" valores.NOM_AGENTE, "+
								" valores.ID_RAMO_CONTABLE, "+
								" valores.ID_SUBR_CONTABLE, "+
								" valores.RAMO1, "+
								" valores.RAMO2, "+
								" valores.CONTRATANTE, "+
								" valores.IMP_COMIS_AGTE, "+
								" valores.IMP_IVA_ACRED, "+
								" valores.IMP_IVA_RET, "+
								" valores.IMP_ISR, "+
								" valores.DESC_MOVTO, "+
								" valores.CENTROOPERACION, "+
								" valores.PROMOTORIA, "+
								" valores.GERENCIA, "+
								" valores.EJECUTIVO, "+
								" valores.DESC_MONEDA, "+
								" valores.IDCENTROOPERACION, "+
								" valores.IDPROMOTORIA, "+
								" valores.IDGERENCIA, "+
								" valores.IDEJECUTIVO, "+
								" valores.ANO_MES "+
			"FROM MIDAS.REPORTECONTABILIDADMMSUPPORT valores");
		
		if(idCentroOperacion!=null){
			addCondition(queryString, "valores.IDCENTROOPERACION=?");
			params.put(index, idCentroOperacion);
			index++;
		}
		if(idPromotoria!=null){
			addCondition(queryString, "valores.IDPROMOTORIA=?");
			params.put(index, idPromotoria);
			index++;
		}
		if(idGerencia!=null){
			addCondition(queryString, "valores.IDGERENCIA=?");
			params.put(index, idGerencia);
			index++;
		}
		if(idEjecutivo!=null){
			addCondition(queryString,"valores.IDEJECUTIVO=?");
			params.put(index, idEjecutivo);
			index++;
		}
		if(idAgente!=null){
			addCondition(queryString, "valores.IDAGENTE=?");
			params.put(index, idAgente);
			index++;
		}
		if(Integer.parseInt(mesInicio)-1 < 10) {
			addCondition(queryString, "valores.ANO_MES=?");
			params.put(index, anioInicio+"0"+(Integer.parseInt(mesInicio)));
			index++;
		}else {
			addCondition(queryString, "valores.ANO_MES=?");
			params.put(index, anioInicio+(Integer.parseInt(mesInicio)));
			index++;
		}
		
//		if (StringUtils.isNotBlank(mesFin)) {
//			if(Integer.parseInt(mesFin)-1 < 10) {
//				addCondition(queryString, "valores.ANO_MES<=?");
//				params.put(index, anioFin+"0"+(Integer.parseInt(mesFin)));
//				index++;
//			}else {
//				addCondition(queryString, "valores.ANO_MES<=?");
//				params.put(index, anioFin+(Integer.parseInt(mesFin)));
//				index++;
//			}
//		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString),
				DatosAgenteContabilidadMMView.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key, params.get(key));
			}
		}
		
		List<DatosAgenteContabilidadMMView> result = query.getResultList();
		
		if (result != null && !result.isEmpty()) {
			return result;
		}
		return null;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List <DatosAgentesDetalleBonoView> llenarDatosDetBonos(Long idAgente, String fechaInicial, String fechaFinal) { 
		List<DatosAgentesDetalleBonoView> resultList = new ArrayList<DatosAgentesDetalleBonoView>();
		
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_repDetalleBonosSupport";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;

		try {
			String[] colsTemp = {"id",
					"fechaMovimiento",
					"nom_agente",
					"gerencia",
					"tipoAgente",
					"promotoria",
					"claveAgente",
					"num_poliza",
					"num_endoso",
					"num_folio_rbo",
					"forma_pago",
					"id_ramo_contable",
					"id_subr_contable",
					"desc_moneda",
					"contratante",
					"imp_prima_neta",
					"imp_rcgos_pagofr",
					"imp_prima_total",
					"pct_comis_agte",
					"sdo_inicial",
					"imp_comis_agte",
					"imp_iva_acred",
					"imp_iva_ret",
					"imp_isr",
					"desc_movto",
					"ramo"};
			String[] propsTemp = {"id",
					"fechaMovimiento",
					"nom_agente",
					"gerencia",
					"tipoAgente",
					"promotoria",
					"claveAgente",
					"num_poliza",
					"num_endoso",
					"num_folio_rbo",
					"forma_pago",
					"id_ramo_contable",
					"id_subr_contable",
					"desc_moneda",
					"contratante",
					"imp_prima_neta",
					"imp_rcgos_pagofr",
					"imp_prima_total",
					"pct_comis_agte",
					"sdo_inicial",
					"imp_comis_agte",
					"imp_iva_acred",
					"imp_iva_ret",
					"imp_isr",
					"desc_movto",
					"ramo"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(DatosAgentesDetalleBonoView.class.getCanonicalName(),propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("pFechaInicial", val(fechaInicial));
			storedHelper.estableceParametro("pFechaFinal", val(fechaFinal));
			storedHelper.estableceParametro("pAgenteInicio", val(idAgente));
			resultList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (resultList != null && !resultList.isEmpty()) {
			return resultList;
		}
		return null;
	}

	public List <DatosReporteCalculoBonoMensualDTO> llenarDatosReporteCalculoBonoMensual(Long idBeneficiario, Long idCalculoBonos, 
			String anio, String mes, Long tipoBeneficiario)
	{
		StoredProcedureHelper storedHelper = null;
		List <DatosReporteCalculoBonoMensualDTO> listaBonos = new ArrayList<DatosReporteCalculoBonoMensualDTO>();
		
		try {
			String [] atributos = {
					"idBeneficiario",
					"nombreBeneficiario",
					"periodoFechaPago",
					"idCalculoBono",
					"clasificacionBono",
					"primaPagada",
					"primaPagadaBono",
					"bonoPagar",
					"iva",
					"ivaRetenido",
					"isr",
					"porcentajeBono",
					"porcentajeSiniestralidad",
					"periodoProduccion",
					"estatus"
			};
			
			String [] columnasResultSet = {
					"ID_BENEFICIARIO",
					"NOMBRE_BENEFICIARIO",
					"PERIODO_FECHAPAGO",
					"ID_CALCULO_BONO",
					"CLASIFICACION_BONO",
					"PRIMA_PAGADA",
					"PRIMA_PAGADA_BONO",
					"BONO_PAGAR",
					"IVA",
					"IVA_RETENIDO",
					"ISR",
					"PCT_BONO",
					"PCT_SINIESTRALIDAD",
					"PERIODO_PRODUCCION",
					"ESTATUS"
			};			
			
			storedHelper = new StoredProcedureHelper(
					"MIDAS.PKGREPORTES_AGENTES.STP_REPCALCBONOMENSUAL", StoredProcedureHelper.DATASOURCE_MIDAS);  
			
			storedHelper.estableceMapeoResultados(DatosReporteCalculoBonoMensualDTO.class.getCanonicalName(), atributos, columnasResultSet);
			
			storedHelper.estableceParametro("pIdAgente",idBeneficiario);
			storedHelper.estableceParametro("PIdCalculoBono", idCalculoBonos);
			storedHelper.estableceParametro("pAnio", anio);
			storedHelper.estableceParametro("pMes", mes);
			storedHelper.estableceParametro("pTipoBeneficiario", tipoBeneficiario);
			 
			
			listaBonos = (List <DatosReporteCalculoBonoMensualDTO>)storedHelper.obtieneListaResultados();
			
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
			
		return listaBonos;		
	}

	@Override
	public List<DatosAgentesReporteProduccion> llenarDatosReporteProd(Date fechaInicio, Date fechaFin) {
		List<DatosAgentesReporteProduccion> resultList = new ArrayList<DatosAgentesReporteProduccion>();
		StringBuilder queryString = new StringBuilder();
		String fechIn;
		String fechFin;
		fechIn = Utilerias.cadenaDeFecha(fechaInicio);
		fechFin = Utilerias.cadenaDeFecha(fechaFin);
		queryString.append(" select /*+INDEX (valores idx_rep_produccion_FCONTAB)*/ VALORES.ID,VALORES.ID_MONEDA , VALORES.DESC_MONEDA , VALORES.ID_RAMO_CONTABLE , VALORES.NOM_RAMO , VALORES.ID_SUBR_CONTABLE , VALORES.NOM_SUBRAMO , VALORES.ID_CENTRO_EMIS , VALORES.NUM_POLIZA , VALORES.NUM_RENOV_POL , VALORES.NUM_POLIZAX , VALORES.ENDOSO , VALORES.F_CONTAB, VALORES.ID_EMPRESA , VALORES.ID_AGENTE , VALORES.D_NOMBRE , VALORES.I_SUPERVISORIA , VALORES.N_SUPERVISORIA , VALORES.I_OFICINA , VALORES.N_OFICINA , VALORES.I_GERENCIA ,VALORES.N_GERENCIA , VALORES.SIT_RECIBO , VALORES.F_CUBRE_DESDE , VALORES.F_CUBRE_HASTA , VALORES.NOM_SOLICITANTE , VALORES.PRIMA_NETA , VALORES.BONIF_COMIS , VALORES.DERECHOS , VALORES.RECARGOS , VALORES.IVA , VALORES.PRIMATOTAL , VALORES.COM_AGT_PF as com_agt_pf, VALORES.COM_AGT_PM as com_agt_pm, VALORES.COM_SUP_PF , VALORES.COM_SUP_PM , VALORES.COM_REC_PF , VALORES.COM_REC_PM , VALORES.COMPESACIONESAGTPF as compesacionesagtpf, VALORES.COMPESACIONESAGTPM as compesacionesagtpm, compesacionesProm as compesacionesProm, VALORES.ID_CONTRATANTE , VALORES.ID_REMESA , VALORES.F_CUBRE_DESDE_RECIBO , VALORES.F_CUBRE_HASTA_RECIBO , VALORES.TIPO_MOV , VALORES.NUM_CUENTA , VALORES.TAR_CRE , VALORES.LIN_NEGOCIO , VALORES.USU_EMI , VALORES.NUM_FOLIO_RBO , VALORES.RECIBO_PAGADO , VALORES.PAQUETE , VALORES.MEDIO_PAGO , VALORES.SISTEMA_EMISION , VALORES.POL_RC_EXT , VALORES.DUSO , VALORES.DSERVICIO , VALORES.FPCT_DESCTO_GLOB , VALORES.TIPO_ENDOSO , VALORES.TIPO_CAMBIO from midas.reporteproduccionsupport valores");
		addParameter(queryString, "to_date(F_CONTAB,'dd/MM/YYYY') ", LOGIC_ASHIGHER, "to_date('"+fechIn+"','dd/MM/YYYY')");
		addParameter(queryString, "to_date(F_CONTAB,'dd/MM/YYYY') ", LOGIC_ASMINOR, "to_date('"+fechFin+"','dd/MM/YYYY')");
		Query query = entityManager.createNativeQuery(queryString.toString(),
				DatosAgentesReporteProduccion.class);
		resultList = query.getResultList();
		if (resultList != null && !resultList.isEmpty()) {
			return resultList;
		}
		return null;
	}
	
	@Override
	public List<DatosAgentePrimaEmitidaVsPriamaPagadaDTO> llenarDatosRepPrimaEmitidaVsPrmaPagada(
			String fechaCoreteInicio, String fechaCorteFin, Long centroOperacion,
			Long gerencia, Long ejecutivo, Long promotoria, Long agente) {
		List<DatosAgentePrimaEmitidaVsPriamaPagadaDTO> resultList = new ArrayList<DatosAgentePrimaEmitidaVsPriamaPagadaDTO>();
		DateFormat f = new SimpleDateFormat("dd/MM/yyyy");
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_reportePrimaEmitVsPrimaPag";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;

		try {
			String[] colsTemp = {"ejecutivo", "tipoAgente", "anio", "claveAgente", 
								 "primaEmitida", "primaPagada", "agente", "id",
								 "siniestralidad_mes","siniestralidad_ejercicio","siniestralidad_ult12Meses,estatusAgente"};
			String[] propsTemp = {"ejecutivo", "tipoAgente", "anio", "claveAgente", 
								  "primaEmitida", "primaPagada", "agente", "id",
								  "siniestralidad_mes","siniestralidad_ejercicio","siniestralidad_ult12Meses,estatusAgente"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(
					DatosAgentePrimaEmitidaVsPriamaPagadaDTO.class.getCanonicalName(),
					propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("pfechaInicio", val(fechaCoreteInicio));
			storedHelper.estableceParametro("pfechaFin", val(fechaCorteFin));
			storedHelper.estableceParametro("pid_centroOperacion", val(centroOperacion));
			storedHelper.estableceParametro("pid_gerencia", val(gerencia));
			storedHelper.estableceParametro("pid_ejecutivo", val(ejecutivo));
			storedHelper.estableceParametro("pid_promotoria", val(promotoria));
			storedHelper.estableceParametro("pid_agente_inicio", val(agente));
			resultList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (resultList != null && !resultList.isEmpty()) {
			return resultList;
		}
		return null;
	}

	@Override
	public List<ReporteGlobalBonoYComisionesDTO> llenarDatosReporteGlobalComisionesY_Bonos(
			Long idAgente, Date fecha, Long idBono, Boolean bandera) {
		String pFechaInicio ="";
		if (fecha!=null){
			SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy"); 
			pFechaInicio = formato.format(fecha); 
		}
		String sp="MIDAS.PKGREPORTES_AGENTES.stp_repGlobalPagoCom";
		if (bandera) {
			sp = "MIDAS.PKGREPORTES_AGENTES.stp_repGlobalPagoBono";
		}
		StringBuilder propiedades=new StringBuilder("");
		StringBuilder columnas=new StringBuilder("");
		String[] cols=null;
		String[] props=null;
		StoredProcedureHelper storedHelper = null;
		List<ReporteGlobalBonoYComisionesDTO> resultList = new ArrayList<ReporteGlobalBonoYComisionesDTO>();
		try {
			String[] colsTemp={
				"f_corte_pagocom",
				"nom_gerencia",
				"ejecutivo",
				"id_agente",
				"nombre",
				"imp_comis_agte",
				"banco",
				"descripcionbono"
			};
			String[] propsTemp={
				"f_corte_pagocom",
				"nom_gerencia",
				"ejecutivo",
				"id_agente",
				"nombre",
				"imp_comis_agte",
				"banco",
				"descripcionbono"
			};
			cols=colsTemp;
	        props=propsTemp;
	        
	        for(int i=0;i<cols.length;i++){
				String columnName=cols[i];
				String propertyName=props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if(i<(cols.length-1)){
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(ReporteGlobalBonoYComisionesDTO.class.getCanonicalName(),propiedades.toString(),columnas.toString());
			storedHelper.estableceParametro("pIdAgente", val(idAgente));
			storedHelper.estableceParametro("pIdBono", val(idBono));
			storedHelper.estableceParametro("pFecha", val(pFechaInicio));
			resultList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (!resultList.isEmpty()) {
			return resultList;
		}
		return null;
	}
	
	private Object val(Object o){
		return (o==null)?"":o;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<DatosEdoctaAgenteAcumuladoView> llenarDatosEdoctaAgente(Long idEmpresa, Long idAgente, Long anioMes) {
		List<DatosEdoctaAgenteAcumuladoView> datosEdoctaAgenteAcumuladoView = new ArrayList<DatosEdoctaAgenteAcumuladoView>(); 

		try{
			String sp1="MIDAS.PKGREPORTES_AGENTES.stp_EdoctaAgenteGralResumen";
			StringBuilder propierties=new StringBuilder("");
			StringBuilder columns=new StringBuilder("");
			String[] cols1=null;
			String[] props1=null;
			StoredProcedureHelper storedHelper1 = null;
			
			String[] colsTemp1={
					"idAgente","nombreAgnete","domicilio","colonia",
					"ciudad","cp","rfc","cedula","correo",
					"correo_cc","Promotor","Ejecutivo","centrooperacion",
					"promotoria","gerencia","sdo_inicial","imp_com_grav_iva_a",
					"imp_com_exen_iva_a","imp_liquidado","imp_cargos","imp_bono_vida",
					"imp_bono_danos","imp_iva_acred","imp_iva_ret","imp_isr","imp_ret_est","sdo_final",
					"acum_sdo_inicial","acum_com_iva","acum_com_exe","acum_liq","acum_cargos",
					"acum_bono_vida","acum_bono_danos","acum_iva_acr","acum_iva_ret","acum_isr","acum_ret_est",
					"acum_sdo_final","nvo_sdo_inicial","nvo_com_iva","nvo_com_exe",	"nvo_liq","nvo_cargos",
					"nvo_bono_vida","nvo_bono_danos","nvo_iva_acr","nvo_iva_ret","nvo_isr","nvo_ret_est","nvo_sdo_final",
					"acum_otrosAbonos","nvo_otrosAbonos","imp_otrosAbonos"
			};

			String[] propsTemp1={
					"idAgente","nombreAgente","domicilio","colonia",
					"ciudad","cp","rfc","cedula","correo",
					"correoCc","promotor","ejecutivo","centroOperacion",
					"promotoria","gerencia","saldoInicial","importeComisionGravadaIvaA",
					"importeComisionExentaIvaA","importeLiquidado","importeCargos","importeBonoVida",
					"importeBonoDanos","importeIvaAcreditado","importeIvaRetenido","importeIsr","importeRetenidoEstatal","saldoFinal",
					"acumuladoSaldoInicial","acumuladoComisionIva","acumuladoComisionExenta","acumuladoLiquidado","acumuladoCargos",
					"acumuladoBonoVida","acumuladoBonoDanos","acumuladoIvaAcreditado","acumuladoIvaRetenido","acumuladoIsr","acumuladoEstatalRetenido",
					"acumuladoSaldoFinal","nuevoSaldoInicial","nuevoComisionIva","nuevoComisionExenta",	"nuevoLiquidado","nuevoCargos",
					"nuevoBonoVida","nuevoBonoDanos","nuevoIvaAcreditado","nuevoIvaRetenido","nuevoIsr","nuevoRetenidoEstatal","nuevoSaldoFinal",
					"acumuladoOtrosAbonos","nuevoOtrosAbonos","importeOtrosAbonos"
			};
			cols1=colsTemp1;
	        props1=propsTemp1;
	        
	        for(int i=0;i<cols1.length;i++){
				String columnName=cols1[i];
				String propertyName=props1[i];
				propierties.append(propertyName);
				columns.append(columnName);
				if(i<(cols1.length-1)){
					propierties.append(",");
					columns.append(",");
				}
			}
	        storedHelper1 = new StoredProcedureHelper(sp1);
			storedHelper1.estableceMapeoResultados(DatosEdoctaAgenteAcumuladoView.class.getCanonicalName(),propierties.toString(),columns.toString());
			storedHelper1.estableceParametro("anioMes", anioMes);
			storedHelper1.estableceParametro("idAgente", idAgente);
			storedHelper1.estableceParametro("idEmpresa", idEmpresa);
			datosEdoctaAgenteAcumuladoView = storedHelper1.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		if (datosEdoctaAgenteAcumuladoView != null) {
			for (DatosEdoctaAgenteAcumuladoView view : datosEdoctaAgenteAcumuladoView) {
				 List<DatosEdoctaAgenteMovimientosView> movimientos = new ArrayList<DatosEdoctaAgenteMovimientosView>();

				 try{		
					String sp="MIDAS.PKGREPORTES_AGENTES.stp_DatosEdoctaAgtDetalle";
					StringBuilder propiedades=new StringBuilder("");
					StringBuilder columnas=new StringBuilder("");
					String[] cols=null;
					String[] props=null;
					StoredProcedureHelper storedHelper = null;
					
					String[] colsTemp={
								"periodo","idAgente","id","dia","concepto","referencia",
					"ramo","subramo","lineaVenta","cargo","abono","saldos","totalDia"
					};
					
					String[] propsTemp={
							"periodo","idAgente","id","dia","concepto","referencia",
					"ramo","subramo","lineaVenta","cargo","abono","saldos","totalDia"
					};
					
					cols=colsTemp;
					props=propsTemp;
					
					for(int i=0;i<cols.length;i++){
						String columnName=cols[i];
						String propertyName=props[i];
						propiedades.append(propertyName);
						columnas.append(columnName);
						if(i<(cols.length-1)){
							propiedades.append(",");
							columnas.append(",");
						}
					}
					
					storedHelper = new StoredProcedureHelper(sp);
					storedHelper.estableceMapeoResultados(DatosEdoctaAgenteMovimientosView.class.getCanonicalName(),propiedades.toString(),columnas.toString());
					storedHelper.estableceParametro("pIdEmpresa", idEmpresa);
					storedHelper.estableceParametro("pIdAgente", idAgente);
					storedHelper.estableceParametro("pAnioMes", anioMes);
					movimientos = storedHelper.obtieneListaResultados();
					view.setMovimientos(movimientos);
				} catch (Exception e) {
					LOG.error(e.getMessage(), e);
				}
			}
		}
		
		if (datosEdoctaAgenteAcumuladoView != null && !datosEdoctaAgenteAcumuladoView.isEmpty()) {
			return datosEdoctaAgenteAcumuladoView;
		}
		return null;
	}
	/**
	 * Obtiene la produccion mensual de un agente por mes
	 * y la produccion al dia actual
	 * @autor martin
	 */
	@Override
	public DatosEdoctaAgenteProduccion llenarDatosEdoctaAgenteProduccion(
			Long idAgente, String anio, String mes) {
		List<DatosEdoctaAgenteProduccion> produccionList =  new ArrayList<DatosEdoctaAgenteProduccion>();
	try{		
		String sp="MIDAS.PKGREPORTES_AGENTES.stp_DatosEdoctaAgtProd";
		StringBuilder propiedades=new StringBuilder("");
		StringBuilder columnas=new StringBuilder("");
		String[] cols=null;
		String[] props=null;
		StoredProcedureHelper storedHelper = null;
		
			String[] colsTemp={
					"idAgente",
					"mensual",
					"anual"
			};
			String[] propsTemp={
					"idAgente",
					"mensual",
					"anual"
			};
			cols=colsTemp;
	        props=propsTemp;
	        
	        for(int i=0;i<cols.length;i++){
				String columnName=cols[i];
				String propertyName=props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if(i<(cols.length-1)){
					propiedades.append(",");
					columnas.append(",");
				}
			}
	       // String anioInicio ="01/01/"+Calendar.getInstance().get(Calendar.YEAR);
	        //String anioFin =Utilerias.cadenaDeFecha(Calendar.getInstance().getTime());
	        storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(DatosEdoctaAgenteProduccion.class.getCanonicalName(),propiedades.toString(),columnas.toString());
			storedHelper.estableceParametro("pAnio", anio);
			storedHelper.estableceParametro("pMes", mes);
//			storedHelper.estableceParametro("pFecha1", anioInicio);
//		    storedHelper.estableceParametro("pFecha2", anioFin);
		    storedHelper.estableceParametro("pIdAgente", idAgente);
			produccionList = storedHelper.obtieneListaResultados();
	} catch (Exception e) {
		LOG.error(e.getMessage(), e);
	}
			//*****************************************************
//		String querStringProduccion=""; 
//			querStringProduccion="SELECT rownum as idagente, ejercicioactual.importeanual as anual,mesactual.importemensual as mensual FROM dual , (SELECT SUM (importeprimaneta) AS importemensual FROM MIDAS.VW_DetRecibosPolizasPagadas WHERE idagente = "
//			+ idAgente
//			+ " AND EXTRACT (MONTH FROM fechaemision) = "
//			+ mes
//			+ " AND EXTRACT (YEAR FROM fechaemision) = "
//			+ anio
//			+ ") mesactual, (SELECT SUM (importeprimaneta) AS importeanual FROM MIDAS.VW_DetRecibosPolizasPagadas WHERE idagente = "
//			+ idAgente
//			+ " AND fechaemision BETWEEN TO_DATE ('"
//			+ "01/01/"+Calendar.getInstance().get(Calendar.YEAR)
//			+ "', 'DD/MM/YYYY') AND TO_DATE ('"+Utilerias.cadenaDeFecha(Calendar.getInstance().getTime())+"', 'DD/MM/YYYY')) ejercicioactual";
//		Query queryProduccion = entityManager.createNativeQuery(querStringProduccion, DatosEdoctaAgenteProduccion.class);//vw_detrecibospolizasemitidas
//		produccionList = queryProduccion.getResultList();
		if (produccionList != null && !produccionList.isEmpty()) {
			return produccionList.get(0);
		}
		return null;
	}
	/**
	 * Obtiene la siniestralidad mensual
	 * y la siniestralidad al dia actual
	 * @autor martin
	 */
	@Override
	public DatosEdoctaAgenteSiniestralidad llenarDatosEdocataAgenteSiniestralidad(
			Long idAgente, String anio, String mes,
			Long idProduccionSobre, String rango, Date fechaInicioAnio,
			Date fechaFinAnio,Date fechaUltimoAnioInicio ,Date fechaUltimoAnioFin) {
//		String queryStringSiniestralidad = ""; 
//		queryStringSiniestralidad = "SELECT rownum as id, ejercicioactual.actual, mesactual.mensual,ultimoanio.anual FROM DUAL, (SELECT pkgcalculos_agentes.fn_buscaporcentajerangosagt ("
//				+"to_date('"+ Utilerias.cadenaDeFecha(fechaInicioMes, "dd/MM/yyyy") +"','DD/MM/YYYY')"
//				+ ", "
//				+"to_date('" + Utilerias.cadenaDeFecha(fechaFinMes, "dd/MM/yyyy") + "','DD/MM/YYYY')"
//				+ ", "
//				+ idAgente
//				+ ", "
//				+ idProduccionSobre
//				+ ", null, "
//				+"'" +rango +"'"
//				+ ", null ) AS mensual FROM DUAL) mesactual, (SELECT pkgcalculos_agentes.fn_buscaporcentajerangosagt ("
//				+"to_date('" + Utilerias.cadenaDeFecha(fechaInicioAnio, "dd/MM/yyyy") + "','DD/MM/YYYY')"
//				+ ", "
//				+"to_date('" +Utilerias.cadenaDeFecha(fechaFinAnio, "dd/MM/yyyy") +"','DD/MM/YYYY')"
//				+ ", "
//				+ idAgente
//				+ ", "
//				+ idProduccionSobre
//				+ ", null, "
//				+"'" + rango + "'"
//				+ ", null ) AS actual FROM DUAL) ejercicioactual,(SELECT pkgcalculos_agentes.fn_buscaporcentajerangosagt"
//				+ "("
//				+ "to_date('"+Utilerias.cadenaDeFecha(fechaUltimoAnioInicio, "dd/MM/yyyy") +"','DD/MM/YYYY')"
//				+ ","
//				+"to_date('" +Utilerias.cadenaDeFecha(fechaUltimoAnioFin, "dd/MM/yyyy") +"','DD/MM/YYYY')"
//				+ ","
//				+ idAgente
//				+ ","
//				+ idProduccionSobre
//				+ ",null,"
//				+ "'" +rango + "'"
//				+ ",null) AS anual from dual) ultimoanio";
//			
//		Query querySiniestralidad = entityManager.createNativeQuery(
//				queryStringSiniestralidad,
//				DatosEdoctaAgenteSiniestralidad.class);
		List<DatosEdoctaAgenteSiniestralidad> siniestralidadList = new ArrayList<DatosEdoctaAgenteSiniestralidad>(); 
//		 siniestralidadList  = querySiniestralidad
//				.getResultList();
		 
		 try{		
				String sp="MIDAS.PKGREPORTES_AGENTES.stp_DatosEdoctaAgtSiniest";
				StringBuilder propiedades=new StringBuilder("");
				StringBuilder columnas=new StringBuilder("");
				String[] cols=null;
				String[] props=null;
				StoredProcedureHelper storedHelper = null;
				
					String[] colsTemp={
							"id",
							"anual",
							"mensual",
							"actual"
					};
					String[] propsTemp={
							"id",
							"anual",
							"mensual",
							"actual"
					};
					cols=colsTemp;
			        props=propsTemp;
			        
			        for(int i=0;i<cols.length;i++){
						String columnName=cols[i];
						String propertyName=props[i];
						propiedades.append(propertyName);
						columnas.append(columnName);
						if(i<(cols.length-1)){
							propiedades.append(",");
							columnas.append(",");
						}
					}
			        storedHelper = new StoredProcedureHelper(sp);
					storedHelper.estableceMapeoResultados(DatosEdoctaAgenteSiniestralidad.class.getCanonicalName(),propiedades.toString(),columnas.toString());
//					storedHelper.estableceParametro("pFechaInicioMes", Utilerias.cadenaDeFecha(fechaInicioMes, "dd/MM/yyyy"));
//					storedHelper.estableceParametro("pFechaFinMes", Utilerias.cadenaDeFecha(fechaFinMes, "dd/MM/yyyy"));
//					storedHelper.estableceParametro("pFechaInicioAnio", Utilerias.cadenaDeFecha(fechaInicioAnio, "dd/MM/yyyy"));
//				    storedHelper.estableceParametro("pFechaFinAnio", Utilerias.cadenaDeFecha(fechaFinAnio, "dd/MM/yyyy"));
//				    storedHelper.estableceParametro("pFechaUltimoAnioInicio", Utilerias.cadenaDeFecha(fechaUltimoAnioInicio, "dd/MM/yyyy"));
//					storedHelper.estableceParametro("pFechaUltimoAnioFin", Utilerias.cadenaDeFecha(fechaUltimoAnioFin, "dd/MM/yyyy"));
					storedHelper.estableceParametro("pAnio", anio);
					storedHelper.estableceParametro("pMes", mes);
					storedHelper.estableceParametro("pIdAgente", idAgente);
				    storedHelper.estableceParametro("pIdProduccionSobre", idProduccionSobre);
				    storedHelper.estableceParametro("pRango", rango);
				    siniestralidadList = storedHelper.obtieneListaResultados();
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		if (siniestralidadList != null && !siniestralidadList.isEmpty()) {
			DatosEdoctaAgenteSiniestralidad siniestralidad = siniestralidadList.get(0);
			if(siniestralidad.getActual() != null) {
				siniestralidad.setActualstr(siniestralidad.getActual().toString()+" %");
			} else {
				LOG.info("No se encontro un importe Actual de Siniestralidad");
			}
			if(siniestralidad.getAnual() != null) {
				siniestralidad.setAnualstr(siniestralidad.getAnual().toString()+" %");
			} else {
				LOG.info("No se encontro un importe Anual de Siniestralidad");
			}
			if(siniestralidad.getMensual() != null) {
				siniestralidad.setMensualstr(siniestralidad.getMensual().toString()+" %");
			} else {
				LOG.info("No se encontro un importe Mensual de Siniestralidad");
			}
			return siniestralidad;
		}
		return null;
	}

	@Override
	public List<ReporteDetalleProvisionView> reporteDetalleProvisiones(String anio,String mes,ProvisionImportesRamo filtro) throws Exception {
		
		Map<Integer,Object> params = new HashMap<Integer, Object>();
		StringBuilder queryString = new StringBuilder("");
		List<Object> idsAgentes = agentesPorFiltroReporteProvision(anio,mes,filtro);
		int index=1;
		if(idsAgentes!=null){				

			queryString
					.append("SELECT /*+INDEX (valores idx_Det_provision_IDAGENTE)*/ valores.AUTOMOVILES as automoviles, "+
					      " valores.CAMIONES_RESIDENTES as camiones_residentes, "+
					      " valores.DIVERSOS_MISCELANEOS_Y_TECNICO as diversos_miscelaneos_y_tecnico, "+
					      " valores.FECHAPROVISION, "+
					      " valores.HURACAN_Y_GRANIZO as huracan_y_granizo, "+
					      " valores.ID as id, "+
					      " valores.IDAGENTE as idAgente, "+
					      " valores.INCENDIO_PURO as incendio_puro, "+
					      " valores.INUNDACION as inundacion, "+
					      " valores.ISR as isr, "+
					      " valores.IVA as iva, "+
					      " valores.IVARET as ivaRet, "+
					      " valores.MARITIMO_Y_TRANSPORTES as maritimo_y_transportes, "+
					      " valores.MOTOCICLETAS_RESIDENTES as motocicletas_residentes, "+
					      " valores.NOMBREAGENTE as nombreAgente, "+
					      " valores.RESP_CIVIL_DE_AVION as resp_civil_de_avion, "+
					      " valores.RESP_CIVIL_GENERAL as resp_civil_general, "+
					      " valores.RESP_CIVIL_VIAJERO as resp_civil_viajero, "+
					      " valores.TOTAL as total, "+
					      " valores.VIDA_GRUPO_Y_COLECTIVO as vida_grupo_y_colectivo, "+
					      " valores.VIDA_INDIVIDUAL as vida_individual "+
					"FROM MIDAS.REPORTEDETALLEPROVISIONSUPPORT valores ");
					StringBuilder idsAgentesString = new  StringBuilder("");
					int cont=0;
					if(idsAgentes!=null){
						for(Object agt:idsAgentes){
							BigDecimal id= (BigDecimal)agt;
							if(cont==0){
								idsAgentesString.append(id.toString());
							}else{
								idsAgentesString.append(",").append(id.toString());
								cont++;
							}
						}
					}
				addCondition(queryString, "valores.IDAGENTE in(?)");
				params.put(1, idsAgentesString.toString());
					Query query=entityManager.createNativeQuery(getQueryString(queryString),ReporteDetalleProvisionView.class);
					if(!params.isEmpty()){
						for(Integer key:params.keySet()){
							query.setParameter(key,params.get(key));
						}
					}
					List<ReporteDetalleProvisionView> resultList = query.getResultList();
					if (!resultList.isEmpty()) {
						return resultList;
					}
					return null;
		}			
		return null;
	}
	
	private List<Object> agentesPorFiltroReporteProvision(String anio,String mes,ProvisionImportesRamo filtro){
		Map<Integer,Object> params = new HashMap<Integer, Object>();
		StringBuilder queryString =  new StringBuilder("");
		queryString.append(" select  distinct agt.IDAGENTE as id from midas.VW_AGENTEINFO agt ");
		queryString.append(" inner join MIDAS.TODETALLEPROVISIONBONO det on(det.idbeneficiario = agt.claveAgente) ");
		queryString.append(" inner join MIDAS.TOPROVISIONESBONOAGENTE prov on(prov.id = det.idProvision) ");
		queryString.append(" where ");

		
		if(filtro!=null){
			int index=1;			
			if(filtro.getProvisionAgentes()!=null && filtro.getProvisionAgentes().getId()!=null){
				addCondition(queryString, " det.idprovision=? ");
				params.put(index,filtro.getProvisionAgentes().getId());
				index++;
			}
			   
			if(anio!=null ){
				addCondition(queryString, " EXTRACT(year FROM prov.fechaprovision)=? ");
				params.put(index,anio);
				index++;
			}
			
			if(mes!=null){
				addCondition(queryString, " EXTRACT(month FROM prov.fechaprovision)=? ");
				params.put(index,mes);
				index++;
			}			
			if(filtro.getConfigBono()!=null && filtro.getConfigBono().getId()!=null){
				addCondition(queryString, " det.idBono=? ");
				params.put(index,filtro.getConfigBono().getId());
				index++;
			}			
			if(filtro.getPromotoria()!=null && filtro.getPromotoria().getId()!=null){
				addCondition(queryString, " agt.idPromotoria=? ");
				params.put(index,filtro.getPromotoria().getId());
				index++;
			}
			if(filtro.getIdCentroOperacion()!=null){
				addCondition(queryString, " agt.idCentroOperacion=? ");
				params.put(index,filtro.getIdCentroOperacion());
				index++;
			}
			if(filtro.getEjecutivo()!=null && filtro.getEjecutivo().getId()!=null){
				addCondition(queryString, " agt.idEjecutivo=? ");
				params.put(index,filtro.getEjecutivo().getId());
				index++;
			}
			if(filtro.getIdGerencia()!=null){
				addCondition(queryString, " agt.idGerencia=? ");
				params.put(index,filtro.getIdGerencia());
				index++;
			}
			if(filtro.getAgenteOrigen()!=null){
				if(filtro.getAgenteOrigen().getId()!=null){
					addCondition(queryString, " agt.idAgente=? ");
					params.put(index,filtro.getAgenteOrigen().getId());
					index++;
				}
				if(filtro.getAgenteOrigen().getPersona()!=null && !filtro.getAgenteOrigen().getPersona().getNombreCompleto().equals("")){
					addCondition(queryString, " agt.agente like ? ");
					params.put(index,"%"+filtro.getAgenteOrigen().getPersona().getNombreCompleto()+"%");
					index++;
				}
			}

		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString));
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key,params.get(key));
			}
		}
		List<Object> resultList = query.getResultList();
		if (!resultList.isEmpty()) {
			return resultList;
		}
		return null;
	}
		
	@SuppressWarnings("unchecked")
	@Override
	public List<DatosHonorariosAgentes> llenarDatosHonorarios(BigDecimal solicitudChequeId) {
				
		List<DatosHonorariosAgentes> resultList = new ArrayList<DatosHonorariosAgentes>();
		
		try{		
			String sp="MIDAS.PKGREPORTES_AGENTES.stp_HonorariosAgentes";
			StringBuilder propiedades=new StringBuilder("");
			StringBuilder columnas=new StringBuilder("");
			String[] cols=null;
			String[] props=null;
			StoredProcedureHelper storedHelper = null;
			
			String[] colsTemp={
					"id_agente","nombre","imp_base","imp_gravable","imp_exento","imp_iva",
					"imp_subtotal","imp_ret_iva","imp_ret_isr","imp_ret_est","imp_total",
					"num_cheque", "f_aplicacion", "claveprodserv", "descripcionprod", "importe_concepto"
			};
			String[] propsTemp={
					"idAgente","nombre","importeBase","importeGravable","importeExento","importeIva",
					"importeSubtotal","importeIvaRetenido","importeIsrRetenido","importeRetenidoEstatal","importeTotal",
					"numeroCheque", "fechaAplicacion", "claveServicio", "descripcionServicio", "importeConcepto"
			};
			cols=colsTemp;
	        props=propsTemp;
		        
	        for(int i=0;i<cols.length;i++){
				String columnName=cols[i];
				String propertyName=props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if(i<(cols.length-1)){
					propiedades.append(",");
					columnas.append(",");
				}
			}
		        
	        storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(DatosHonorariosAgentes.class.getCanonicalName(),propiedades.toString(),columnas.toString());				
			storedHelper.estableceParametro("ChequeId", solicitudChequeId);    
			    
			resultList = storedHelper.obtieneListaResultados();
				
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		if (resultList != null && !resultList.isEmpty()) {
			resultList.get(0).setImporteTotalCLetra(Utilerias.convertNumberToLetter(resultList.get(0).getImporteTotal().toString()));
			
			
			return resultList;
		}
		return null;
	}
	
	@Override
	public List<ReporteProvisionView> reporteProvisiones(String anio,String mes,ProvisionImportesRamo filtro) throws Exception {
		
		Map<Integer,Object> params = new HashMap<Integer, Object>();
		StringBuilder queryString = new StringBuilder("");
		List<Object> idsAgentes = agentesPorFiltroReporteProvision(anio,mes,filtro);
		
		if(idsAgentes!=null){				
			
			StringBuilder idsAgentesString = new StringBuilder();
			for(Object agt:idsAgentes){
				BigDecimal id= (BigDecimal)agt;
				idsAgentesString.append(id+",");
			}
			idsAgentesString.replace(idsAgentesString.length()-1,idsAgentesString.length(),"");


			
			queryString.append("select  valores.AJUSTEMAS, "+
								      " valores.AJUSTEMENOS, "+
								      " valores.CLAVEAGENTE, "+
								      " valores.DEDUCIBLE, "+
								      " valores.ESTIMACIONINICIAL, "+
								      " valores.GASTOS, "+
								      " valores.ID, "+
								      " valores.IDAGENTE, "+
								      " valores.IDGERENCIA, "+
								      " valores.MONTO, "+
								      " valores.NOMBREAGENTE, "+
								      " valores.NOMBREEJECUTIVO, "+
								      " valores.NOMBREGERENCIA, "+
								      " valores.NOMBREGRUPO, "+
								      " valores.PRIMADEVENGADA, "+
								      " valores.PRIMAPAGADA, "+
								      " valores.RECUPERACIONES, "+
								      " valores.SALV_RECUP, "+
								      " valores.SEC, "+
								      " valores.TIPOAGENTE "+
								 "from MIDAS.REPORTEPROVISIONESSUPPORT valores");
							
			queryString.append(" where ");
			
			addCondition(queryString, " valores.CLAVEAGENTE in(?)");
			params.put(1, idsAgentesString.toString());
			String finalQuery=getQueryString(queryString);
			Query query=entityManager.createNativeQuery(finalQuery,ReporteProvisionView.class);

			List<ReporteProvisionView> resultList = query.getResultList();
			if (!resultList.isEmpty()) {
				return resultList;
			}
			return null;
		}			
		return null;
	}
	
	@SuppressWarnings("unchecked")
	public List<DatosDetPrimas> llenarDatosDetallePrimasReporte(
			String anio, String mes, Agente agente) {

		List<DatosDetPrimas> resultList = new ArrayList<DatosDetPrimas>();

		String anioMes = anio + mes;	

		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_repDetallePrimasAgente";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;

		try {
			String[] colsTemp = { "fechaMovimiento", "id_supervisoria",
					"nom_supervisoria", "id_agente", "nom_agente",
					"num_poliza", "num_endoso", "num_folio_rbo",
					"id_ramo_contable", "id_subr_contable", "desc_moneda",
					"imp_prima_neta", "imp_rcgos_pagofr", "imp_prima_total",
					"pct_comis_agte", "imp_comis_agte", "imp_iva_acred",
					"imp_iva_ret", "imp_isr", "imp_ret_est", "desc_movto", "dia", "contratante",
					"forma_pago", "totalporpoliza", "tipo_cambio",
					"num_fianza", "f_fin_vig_fianza", "descripcionramo","ini_iva","ini_isr","ini_iva_ret","ini_ret_est","imp_comis_agte","F_CORTE_EDOCTA","cve_t_cpto_a_c_o","id_concepto","cve_c_a"};

			String[] propsTemp = { "fechaMovimiento", "idSupervisoria",
					"nombreSupervisoria", "idAgente", "nombreAgente",
					"numeroPoliza", "numeroEndoso", "numeroFolioRubro",
					"idRamo", "idSubramo", "moneda",
					"importePrimaNeta", "importeRecargosPagoFraccionado", "importePrimaTotal",
					"porcentajeComisionAgente", "importeComisionAgente", "importeIvaAcreditado",
					"importeIvaRetenido", "importeIsr", "importeEstatalRetenido", "descripcionMovimiento", "dia", "contratante",
					"formaPago", "importeTotalPorPoliza", "importeTipoCambio",
					"numeroFianza", "fechaFinFianza", "ramo","importeInicialIva","importeInicialIsr","importeInicialIvaRetenido","importeInicialEstatalRetenido","importeComisionAgente","fechaCorte","tipoConceptoOrigen","idConcepto","naturalezaConcepto"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(
					DatosDetPrimas.class.getCanonicalName(),
					propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("pAnioMes", val(anioMes));
			storedHelper.estableceParametro("pidAgente", val(agente.getIdAgente()));			
			resultList = storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return resultList;
	}
	
	@Override
	public int generarDatosReporteRecibosProv(String mesAnio) throws Exception
	{
		int result = 0;
		
		if(mesAnio.length() == 5)
		{
			mesAnio = "0" + mesAnio;
		}
		
		String sp = "MIDAS.PKGAUT_REPORTES.stp_generarDatosRecibosProv";		
		StoredProcedureHelper storedHelper = null;
		storedHelper = new StoredProcedureHelper(sp,StoredProcedureHelper.DATASOURCE_MIDAS);
		storedHelper.estableceParametro("pMesAnio", val(mesAnio));
		result = storedHelper.ejecutaActualizar();
		
		return result;
	}
	
	public StringBuilder idAgenteSupport(StringBuilder strb, List <CentroOperacion> centroOperacionesSeleccionados,
										 List<Agente> ageteList, List<Gerencia> gerenciaList, List <Ejecutivo> ejecutivoList,
								 List <Promotoria> promotoriaList){
		int contador =0;
		String centroOpStr = obtenerCentroOpstr(centroOperacionesSeleccionados,contador);
		String agenteStr = obtenerIdAgentes(ageteList);
		String gerenciaStr = obtenerGerenciaStr(gerenciaList,contador);
		String ejecutivoStr = obtenerEjecutivos(ejecutivoList,contador);
		String promotoriaStr = obtenerPromotoria(promotoriaList,contador);
		return idAgenteSupport(strb,centroOpStr,agenteStr,gerenciaStr,ejecutivoStr,promotoriaStr);
	}
	
	public String obtenerPromotoria(List<Promotoria> promotoriaList,int contador) {
		StringBuilder promotoriaStr = new  StringBuilder("");
		if(promotoriaList!=null){
			for(Promotoria objprom:promotoriaList){
				if(contador ==0){
					promotoriaStr.append(Long.toString(objprom.getId()));
					contador++;
				}else{
					promotoriaStr.append(",").append(Long.toString(objprom.getId()));
				}
			}
		}		
		return promotoriaStr.toString();
	}

	public String obtenerEjecutivos(List<Ejecutivo> ejecutivoList,int contador) {
		StringBuilder ejecutivoStr = new StringBuilder("");
		if(ejecutivoList!=null){
			for(Ejecutivo objejec:ejecutivoList){
				if(contador ==0){
					ejecutivoStr.append(Long.toString(objejec.getId()));
					contador++;
				}else{
					ejecutivoStr.append(",").append(Long.toString(objejec.getId()));
				}
			}
		}
		return ejecutivoStr.toString();
	}

	public String obtenerGerenciaStr(List<Gerencia> gerenciaList,int contador) {
		StringBuilder gerenciaStr = new StringBuilder("");
		if(gerenciaList!=null){
			for(Gerencia objger:gerenciaList){
				if(contador ==0){
					gerenciaStr.append(Long.toString(objger.getId()));
					contador++;
				}else{
					gerenciaStr.append(",").append(Long.toString(objger.getId()));
				}
			}
		}
		return gerenciaStr.toString();
	}

	public String obtenerCentroOpstr(
			List<CentroOperacion> centroOperacionesSeleccionados, int contador) {
		StringBuilder centroOpStr = new StringBuilder("");
		if(centroOperacionesSeleccionados!=null){
			for(CentroOperacion objcentOp:centroOperacionesSeleccionados){
				if(contador ==0){
					centroOpStr.append(Long.toString(objcentOp.getId()));
					contador++;
				}else{
					centroOpStr.append(",").append(Long.toString(objcentOp.getId()));
				}
			}
		}
		return centroOpStr.toString();
	}

	public StringBuilder idAgenteSupport(StringBuilder strb, String centroOpStr, String agenteStr, String gerenciaStr, String ejecutivoStr, 
										String promotoriaStr){
		int cont=0;
		strb.append("select /*+INDEX (ej TOEJECUTIVO_PK) INDEX(promo TOPROMOTORIA_PK) INDEX(agente IDX_TOAGT_CALCBONOEXCLUSION)*/ distinct " +
						  "agente.id, " +
						  "agente.idAgente as idAgente, " +
						  "prioridad.valor as prioridad, " +
						  "estatus.valor as tipoSituacion, " +
						  "persona.nombreCompleto as nombreCompleto, " +
						  "promo.descripcion as promotoria, " +
						  "ger.descripcion as gerencia " +
						  "FROM MIDAS.toAgente agente " +
						  "INNER JOIN MIDAS.vw_persona persona on (persona.idpersona=agente.idpersona) " +
						  "INNER JOIN MIDAS.toPromotoria promo on(agente.idpromotoria=promo.id) " +
						  "INNER JOIN MIDAS.toEjecutivo ej on(ej.id=promo.ejecutivo_id) " +
						  "INNER JOIN MIDAS.vw_persona personaEj on (personaEj.idpersona=ej.idpersona) " +
						  "INNER JOIN MIDAS.toGerencia ger on (ej.gerencia_id=ger.id) " +
						  "INNER JOIN MIDAS.toCentroOperacion co on (ger.centroOperacion_id=co.id) " +
						  "INNER JOIN MIDAS.toValorCatalogoAgentes tipoAgente on(tipoAgente.id=agente.idTipoAgente) " +
						  "INNER JOIN MIDAS.toValorCatalogoAgentes tipoPromotoria on (tipoPromotoria.id=promo.idTipoPromotoria) " +
						  "INNER JOIN MIDAS.toValorCatalogoAgentes prioridad on (prioridad.id=agente.idPrioridadAgente) " +
						  "INNER JOIN MIDAS.toValorCatalogoAgentes estatus on (estatus.id=agente.idSituacionAgente) ");
							if(!agenteStr.equals("")){
								if(cont==0){
									strb.append("WHERE agente.id in ("+agenteStr+") ");
									cont++;
								}
							}
								if(!centroOpStr.equals("")){
									if(cont==0){
										strb.append(" WHERE ");
										cont++;
									}else{
										strb.append(" OR ");
									}
									//se cambio IDCENTROOPERACION por ID
										strb.append("co.ID in ("+centroOpStr+") "); 
							}
							if(!gerenciaStr.equals("")){
								if(cont==0){	
									strb.append(" WHERE ");
									cont++;
								}else{
									strb.append(" AND ");
								}
									strb.append(" ger.id in ("+gerenciaStr+") " );
							}
							if(!ejecutivoStr.equals("")){
								if(cont==0){
									strb.append(" WHERE ");
									cont++;
								}else{
									strb.append(" AND ");
								}
								strb.append(" ej.id in ("+ejecutivoStr+") " );
							}
							if(!promotoriaStr.equals("")){
								if(cont==0){
									strb.append(" WHERE ");
									cont++;
								}else{
									strb.append(" AND ");
								}
								strb.append(" promo.id in ("+promotoriaStr+") ");
							}
		return strb;
	}

	@Override
	public List<DatosAgenteContabilidadMMView> llenarDatosAgenteContabilidadMizar(
			String anioInicio, String mesInicio, String anioFin, String mesFin,
			Long idCentroOperacion, Long idGerencia, Long idEjecutivo,
			Long idPromotoria, Long idAgente) {
		int index=1;
		Map<Integer,Object>params = new HashMap<Integer, Object>();
		StringBuilder queryString = new StringBuilder();
		queryString.append("");
		
		if(idCentroOperacion!=null){
			addCondition(queryString, "valores.IDCENTROOPERACION=?");
			params.put(index, idCentroOperacion);
			index++;
		}
		if(idPromotoria!=null){
			addCondition(queryString, "valores.IDPROMOTORIA=?");
			params.put(index, idPromotoria);
			index++;
		}
		if(idGerencia!=null){
			addCondition(queryString, "valores.IDGERENCIA=?");
			params.put(index, idGerencia);
			index++;
		}
		if(idEjecutivo!=null){
			addCondition(queryString,"valores.IDEJECUTIVO=?");
			params.put(index, idEjecutivo);
			index++;
		}
		if(idAgente!=null){
			addCondition(queryString, "valores.IDAGENTE=?");
			params.put(index, idAgente);
			index++;
		}
		if(Integer.parseInt(mesInicio)-1 < 10) {
			addCondition(queryString, "valores.ANO_MES>=?");
			params.put(index, anioInicio+"0"+(Integer.parseInt(mesInicio)));
			index++;
		}else {
			addCondition(queryString, "valores.ANO_MES>=?");
			params.put(index, anioInicio+(Integer.parseInt(mesInicio)));
			index++;
		}
		
		if (StringUtils.isNotBlank(mesFin)) {
			if(Integer.parseInt(mesFin)-1 < 10) {
				addCondition(queryString, "valores.ANO_MES<=?");
				params.put(index, anioFin+"0"+(Integer.parseInt(mesFin)));
				index++;
			}else {
				addCondition(queryString, "valores.ANO_MES<=?");
				params.put(index, anioFin+(Integer.parseInt(mesFin)));
				index++;
			}
		}
		Query query = entityManager.createNativeQuery(getQueryString(queryString),
				DatosAgenteContabilidadMMView.class);
		
		if(!params.isEmpty()){
			for(Integer key:params.keySet()){
				query.setParameter(key, params.get(key));
			}
		}

		List<DatosAgenteContabilidadMMView> result = query.getResultList();
		
		if (result != null && !result.isEmpty()) {
			return result;
		}
		return null;
	}
	
	private void setTipoCargaInciso(IncisoCotizacionDTO incisoCotizacionDTO, DatosIncisoDTO datosIncisoDTO){
		//Obtiene Tipo de Carga 
		CoberturaDTO cobertura = entidadService.findById(CoberturaDTO.class, CoberturaDTO.IDTOCOBERTURA_DANIOSOCASIONADOSPORLACARGA);
		SeccionCotizacionDTO seccionCotizacion = incisoCotizacionDTO.getSeccionCotizacion();
		Map<String,Object> params = new LinkedHashMap<String,Object>();
		params.put("idToCobertura", CoberturaDTO.IDTOCOBERTURA_DANIOSOCASIONADOSPORLACARGA);
		params.put("idToCotizacion", incisoCotizacionDTO.getId().getIdToCotizacion());
		params.put("numeroInciso", incisoCotizacionDTO.getId().getNumeroInciso());
		params.put("idToSeccion", seccionCotizacion.getSeccionDTO().getIdToSeccion());
		params.put("idTcSubRamo", cobertura.getSubRamoDTO().getIdTcSubRamo());
		List<DatoIncisoCotAuto> datoRiesgoList = entidadService.findByProperties(DatoIncisoCotAuto.class, params);
		if(datoRiesgoList != null && !datoRiesgoList.isEmpty()){
			DatoIncisoCotAuto item = datoRiesgoList.get(0);
			if(item.getValor() != null && !item.getValor().isEmpty()){
				CatalogoValorFijoId id = new CatalogoValorFijoId(CatalogoValorFijoDTO.IDGRUPO_TIPO_CARGA, Integer.parseInt(item.getValor()));
				CatalogoValorFijoDTO cat = entidadService.findById(CatalogoValorFijoDTO.class, id);
				datosIncisoDTO.setTipoCarga(cat.getDescripcion());
			}
		}
	}

	@Override
	public List<SaldosVsContabilidadView> llenarDatosReporteSaldosVsContabilidad(Date fechaCorte) {
		try {
			
			SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
			String formatoFech = formatoFecha.format(fechaCorte);
			//*************se eliminan los registros de la tabla de paso *************
//			repSaldoVSContService.deleteTPSaldosVsContabilidad();
			//************* se hace la consulta a mizar *************
//			List<RepMizarSelectMizar> lista = new ArrayList<RepMizarSelectMizar>();
//			lista = repSaldoVSContService.consultaMizar(anioMes, fechaString);
			//************* se inserta los datos obtenidos de la consulta en laa tabla de paso  *************
//			repSaldoVSContService.insertTPSaldosVsContabilidad(lista);
			//************* se manda a llamar el sp que devuelve la consulta con la cual se llena el reporte *************
			List<SaldosVsContabilidadView> resultado = new ArrayList<SaldosVsContabilidadView>();
			
			StringBuilder queryString = new StringBuilder();
			queryString.append(" select rownum as id, " );
			queryString.append(" a.id_agente, "); 
			queryString.append(" b.nombre, "); 
			queryString.append(" b.b_contab_comis, "); 
			queryString.append(" a.saldo_inicial, "); 
			queryString.append(" a.cobranza,"); 
			queryString.append(" a.manuales,"); 
			queryString.append(" a.pagos,"); 
			queryString.append(" a.saldo_final, "); 
			queryString.append(" a.cob_mizar, "); 
			queryString.append(" a.agt_mizar, "); 
			queryString.append(" a.gg_mizar "); 
			queryString.append(" from midas.g_saldos_1622 a ");
			queryString.append(" left join seycos.vw_est_fv b on b.id_agente = a.id_agente ");
			queryString.append(" order by a.id_agente ");
			
			Query query = entityManager.createNativeQuery(queryString.toString(), SaldosVsContabilidadView.class);
			resultado = query.getResultList();
//			resultado = repSaldoVSContService.executeSpSaldosVSContabilidad(formatoFech);
				if( resultado !=null && !resultado.isEmpty()){
					LogDeMidasInterfaz.log("resultado OK... llenarDatosReporteSaldosVsContabilidad..." + this, Level.WARNING, null);
					return resultado;
				}
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
				LogDeMidasInterfaz.log("Excepcion en BD de ImpresionesServiceImpl.llenarDatosReporteSaldosVsContabilidad..." + this, Level.WARNING, e);
			}
			LogDeMidasInterfaz.log("resultado NULL...Excepcion en BD de ImpresionesServiceImpl.llenarDatosReporteSaldosVsContabilidad..." + this, Level.WARNING, null);
		return null;
	}
	
	public List<SapAlertasistemasEnvio> obtenerDetalleAlertsBitacora(String idEncabezadoAlertas){
		List<SapAlertasistemasEnvio> listaDetAlertSistemas = new ArrayList<SapAlertasistemasEnvio>();
		listaDetAlertSistemas.add(entidadService.findById(SapAlertasistemasEnvio.class, Long.valueOf(idEncabezadoAlertas) ));
		return listaDetAlertSistemas;	
	}
	
	public List<SapAmisBitacoraEmision> obtenerBitacoraEmision (String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
			String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
			String siniestro, String sipac , String valuacion){
		
		return sapAmisBitacoraService.obtenerBitacoraEmisionFiltrada(bitacoraPoliza, bitacoraVin, bitacoraFechaEnvio, estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd, siniestro, sipac, valuacion);
	}
	
	public List<SapAmisBitacoraSiniestros> obtenerBitacoraSiniestro (String bitacoraPoliza ,String bitacoraVin ,String bitacoraFechaEnvio,
			String estatusEnvio,String cesvi,String cii,String emision,String ocra,String prevencion,String pt,String csd,
			String siniestro, String sipac , String valuacion){
		
		return sapAmisBitacoraService.obtenerBitacoraSiniestrosFiltrada(bitacoraPoliza, bitacoraVin, bitacoraFechaEnvio, estatusEnvio, cesvi, cii, emision, ocra, prevencion, pt, csd, siniestro, sipac, valuacion);
	}
	
	private List<NegocioDeducibleCob> getDeducibles(IncisoCotizacionDTO incisoCotizacionDTO, BigDecimal idToCobertura) {
		List<NegocioDeducibleCob> deducibles = null;
		NegocioCobPaqSeccion negocioCobPaqSeccion = null;
		
		negocioCobPaqSeccion = negocioCobPaqSeccionService.getLimitesSumaAseguradaPorCobertura(idToCobertura, incisoCotizacionDTO.getIncisoAutoCot().getEstadoId(), 
				incisoCotizacionDTO.getIncisoAutoCot().getMunicipioId(), incisoCotizacionDTO.getCotizacionDTO().getIdMoneda(), incisoCotizacionDTO.getIncisoAutoCot().getNegocioPaqueteId());
		deducibles = negocioDeduciblesService.obtenerDeduciblesCobertura(negocioCobPaqSeccion.getIdToNegCobPaqSeccion());

		 return deducibles;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DatosAgenteEstatusEntregaFactura> llenarDatosEstatusEntregaFactura(
			DatosAgenteEstatusEntregaFactura datos){
		List<DatosAgenteEstatusEntregaFactura> resultList = new ArrayList<DatosAgenteEstatusEntregaFactura>(1);

		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_repEstatusEntregaFactura";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;

		try {
			String[] colsTemp = { "idAgente","gerenciaNombre", "promotoriaNombre",
					"nombreAgente", "periodo", "estatus"};
			String[] propsTemp = { "idAgente","gerenciaNombre", "promotoriaNombre",
					"nombreAgente", "periodo", "estatus"};
			cols = colsTemp;
			props = propsTemp;

			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(
					"mx.com.afirme.midas2.dto.reportesAgente.DatosAgenteEstatusEntregaFactura",
					propiedades.toString(), columnas.toString());
			storedHelper.estableceParametro("pfechainicio", datos.getFechaInicio());
			storedHelper.estableceParametro("pfechafinal", datos.getFechaFin());
			storedHelper.estableceParametro("pIdAgente", datos.getIdAgente());
			storedHelper.estableceParametro("pIdcentroOperacion", datos.getCentroOperacionId());
			storedHelper.estableceParametro("pIdgerencia", datos.getGerenciaId());
			storedHelper.estableceParametro("pIdejecutivo", datos.getEjecutivoId());
			storedHelper.estableceParametro("pIdpromotoria", datos.getPromotoriaId());
			storedHelper.estableceParametro("pEstatus", datos.getEstatus());
			resultList = (List<DatosAgenteEstatusEntregaFactura>)storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return resultList;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<DatosVentasProduccion> llenarDatosVentasProduccion(String anio, String mesInicio, String mesFin, Long idCentroOperacion, Long idGerencia,
			Long idEjecutivo, Long idPromotoria, Long idAgente){
		
		List<DatosVentasProduccion> resultList = new ArrayList<DatosVentasProduccion>();
		String sp = "MIDAS.PKGREPORTES_AGENTES.stp_RepVentasProduccion";
		StringBuilder propiedades = new StringBuilder("");
		StringBuilder columnas = new StringBuilder("");
		String[] cols = null;
		String[] props = null;
		StoredProcedureHelper storedHelper = null;
		try {
			String[] colsTemp = {"producto",
					"prima_pagada_Ene","exclusiones_Ene","prima_pagada_bono_Ene","bono_produccion_Ene","bono_siniestralidad_Ene","bono_crecimiento_Ene",
					"prima_pagada_Feb","exclusiones_Feb","prima_pagada_bono_Feb","bono_produccion_Feb","bono_siniestralidad_Feb","bono_crecimiento_Feb",
					"prima_pagada_Mar","exclusiones_Mar","prima_pagada_bono_Mar","bono_produccion_Mar","bono_siniestralidad_Mar","bono_crecimiento_Mar",
					"prima_pagada_Abr","exclusiones_Abr","prima_pagada_bono_Abr","bono_produccion_Abr","bono_siniestralidad_Abr","bono_crecimiento_Abr",
					"prima_pagada_May","exclusiones_May","prima_pagada_bono_May","bono_produccion_May","bono_siniestralidad_May","bono_crecimiento_May",
					"prima_pagada_Jun","exclusiones_Jun","prima_pagada_bono_Jun","bono_produccion_Jun","bono_siniestralidad_Jun","bono_crecimiento_Jun",
					"prima_pagada_Jul","exclusiones_Jul","prima_pagada_bono_Jul","bono_produccion_Jul","bono_siniestralidad_Jul","bono_crecimiento_Jul",
					"prima_pagada_Ago","exclusiones_Ago","prima_pagada_bono_Ago","bono_produccion_Ago","bono_siniestralidad_Ago","bono_crecimiento_Ago",
					"prima_pagada_Sep","exclusiones_Sep","prima_pagada_bono_Sep","bono_produccion_Sep","bono_siniestralidad_Sep","bono_crecimiento_Sep",
					"prima_pagada_Oct","exclusiones_Oct","prima_pagada_bono_Oct","bono_produccion_Oct","bono_siniestralidad_Oct","bono_crecimiento_Oct",
					"prima_pagada_Nov","exclusiones_Nov","prima_pagada_bono_Nov","bono_produccion_Nov","bono_siniestralidad_Nov","bono_crecimiento_Nov",
					"prima_pagada_Dic","exclusiones_Dic","prima_pagada_bono_Dic","bono_produccion_Dic","bono_siniestralidad_Dic","bono_crecimiento_Dic",
					"pa_prima_pagada_Ene","pa_exclusiones_Ene","pa_prima_pagada_bono_Ene","pa_bono_produccion_Ene","pa_bono_siniestralidad_Ene","pa_bono_crecimiento_Ene",
					"pa_prima_pagada_Feb","pa_exclusiones_Feb","pa_prima_pagada_bono_Feb","pa_bono_produccion_Feb","pa_bono_siniestralidad_Feb","pa_bono_crecimiento_Feb",
					"pa_prima_pagada_Mar","pa_exclusiones_Mar","pa_prima_pagada_bono_Mar","pa_bono_produccion_Mar","pa_bono_siniestralidad_Mar","pa_bono_crecimiento_Mar",
					"pa_prima_pagada_Abr","pa_exclusiones_Abr","pa_prima_pagada_bono_Abr","pa_bono_produccion_Abr","pa_bono_siniestralidad_Abr","pa_bono_crecimiento_Abr",
					"pa_prima_pagada_May","pa_exclusiones_May","pa_prima_pagada_bono_May","pa_bono_produccion_May","pa_bono_siniestralidad_May","pa_bono_crecimiento_May",
					"pa_prima_pagada_Jun","pa_exclusiones_Jun","pa_prima_pagada_bono_Jun","pa_bono_produccion_Jun","pa_bono_siniestralidad_Jun","pa_bono_crecimiento_Jun",
					"pa_prima_pagada_Jul","pa_exclusiones_Jul","pa_prima_pagada_bono_Jul","pa_bono_produccion_Jul","pa_bono_siniestralidad_Jul","pa_bono_crecimiento_Jul",
					"pa_prima_pagada_Ago","pa_exclusiones_Ago","pa_prima_pagada_bono_Ago","pa_bono_produccion_Ago","pa_bono_siniestralidad_Ago","pa_bono_crecimiento_Ago",
					"pa_prima_pagada_Sep","pa_exclusiones_Sep","pa_prima_pagada_bono_Sep","pa_bono_produccion_Sep","pa_bono_siniestralidad_Sep","pa_bono_crecimiento_Sep",
					"pa_prima_pagada_Oct","pa_exclusiones_Oct","pa_prima_pagada_bono_Oct","pa_bono_produccion_Oct","pa_bono_siniestralidad_Oct","pa_bono_crecimiento_Oct",
					"pa_prima_pagada_Nov","pa_exclusiones_Nov",	"pa_prima_pagada_bono_Nov","pa_bono_produccion_Nov","pa_bono_siniestralidad_Nov","pa_bono_crecimiento_Nov",
					"pa_prima_pagada_Dic","pa_exclusiones_Dic","pa_prima_pagada_bono_Dic","pa_bono_produccion_Dic","pa_bono_siniestralidad_Dic","pa_bono_crecimiento_Dic"
					};
			
			String[] propsTemp = {"producto",
					"primaPagadaEne","exclusionesEne","primaPagadabonoEne","bonoProduccionEne","bonoSiniestralidadEne","bonoCrecimientoEne",
					"primaPagadaFeb","exclusionesFeb","primaPagadabonoFeb","bonoProduccionFeb","bonoSiniestralidadFeb","bonoCrecimientoFeb",
					"primaPagadaMar","exclusionesMar","primaPagadabonoMar","bonoProduccionMar","bonoSiniestralidadMar","bonoCrecimientoMar",
					"primaPagadaAbr","exclusionesAbr","primaPagadabonoAbr","bonoProduccionAbr","bonoSiniestralidadAbr","bonoCrecimientoAbr",
					"primaPagadaMay","exclusionesMay","primaPagadabonoMay","bonoProduccionMay","bonoSiniestralidadMay","bonoCrecimientoMay",
					"primaPagadaJun","exclusionesJun","primaPagadabonoJun","bonoProduccionJun","bonoSiniestralidadJun","bonoCrecimientoJun",
					"primaPagadaJul","exclusionesJul","primaPagadabonoJul","bonoProduccionJul","bonoSiniestralidadJul","bonoCrecimientoJul",
					"primaPagadaAgo","exclusionesAgo","primaPagadabonoAgo","bonoProduccionAgo","bonoSiniestralidadAgo","bonoCrecimientoAgo",
					"primaPagadaSep","exclusionesSep","primaPagadabonoSep","bonoProduccionSep","bonoSiniestralidadSep","bonoCrecimientoSep",
					"primaPagadaOct","exclusionesOct","primaPagadabonoOct","bonoProduccionOct","bonoSiniestralidadOct","bonoCrecimientoOct",
					"primaPagadaNov","exclusionesNov","primaPagadabonoNov","bonoProduccionNov","bonoSiniestralidadNov","bonoCrecimientoNov",
					"primaPagadaDic","exclusionesDic","primaPagadabonoDic","bonoProduccionDic","bonoSiniestralidadDic","bonoCrecimientoDic",
					"paPrimaPagadaEne","paExclusionesEne","paPrimaPagadabonoEne","paBonoProduccionEne","paBonoSiniestralidadEne","paBonoCrecimientoEne",
					"paPrimaPagadaFeb","paExclusionesFeb","paPrimaPagadabonoFeb","paBonoProduccionFeb","paBonoSiniestralidadFeb","paBonoCrecimientoFeb",
					"paPrimaPagadaMar","paExclusionesMar","paPrimaPagadabonoMar","paBonoProduccionMar","paBonoSiniestralidadMar","paBonoCrecimientoMar"
					,"paPrimaPagadaAbr","paExclusionesAbr","paPrimaPagadabonoAbr","paBonoProduccionAbr","paBonoSiniestralidadAbr","paBonoCrecimientoAbr",
					"paPrimaPagadaMay","paExclusionesMay","paPrimaPagadabonoMay","paBonoProduccionMay","paBonoSiniestralidadMay","paBonoCrecimientoMay",
					"paPrimaPagadaJun","paExclusionesJun","paPrimaPagadabonoJun","paBonoProduccionJun","paBonoSiniestralidadJun","paBonoCrecimientoJun",
					"paPrimaPagadaJul","paExclusionesJul","paPrimaPagadabonoJul","paBonoProduccionJul","paBonoSiniestralidadJul","paBonoCrecimientoJul",
					"paPrimaPagadaAgo","paExclusionesAgo","paPrimaPagadabonoAgo","paBonoProduccionAgo","paBonoSiniestralidadAgo","paBonoCrecimientoAgo",
					"paPrimaPagadaSep","paExclusionesSep","paPrimaPagadabonoSep","paBonoProduccionSep","paBonoSiniestralidadSep","paBonoCrecimientoSep",
					"paPrimaPagadaOct","paExclusionesOct","paPrimaPagadabonoOct","paBonoProduccionOct","paBonoSiniestralidadOct","paBonoCrecimientoOct",
					"paPrimaPagadaNov","paExclusionesNov","paPrimaPagadabonoNov","paBonoProduccionNov","paBonoSiniestralidadNov","paBonoCrecimientoNov",
					"paPrimaPagadaDic","paExclusionesDic","paPrimaPagadabonoDic","paBonoProduccionDic","paBonoSiniestralidadDic","paBonoCrecimientoDic"
					};
			
			cols = colsTemp;
			props = propsTemp;
			
			for (int i = 0; i < cols.length; i++) {
				String columnName = cols[i];
				String propertyName = props[i];
				propiedades.append(propertyName);
				columnas.append(columnName);
				if (i < (cols.length - 1)) {
					propiedades.append(",");
					columnas.append(",");
				}
			}
			storedHelper = new StoredProcedureHelper(sp);
			storedHelper.estableceMapeoResultados(
					"mx.com.afirme.midas2.dto.reportesAgente.DatosVentasProduccion",
					propiedades.toString(), columnas.toString());
			
			storedHelper.estableceParametro("anio",anio);
			storedHelper.estableceParametro("mes",(Integer.parseInt(mesInicio) <10)?"0"+mesInicio:mesInicio);
			storedHelper.estableceParametro("mesFin",(Integer.parseInt(mesFin)<10)?"0"+mesFin:mesFin);
			storedHelper.estableceParametro("id_centrooperacion",idCentroOperacion);
			storedHelper.estableceParametro("id_gerencia",idGerencia);
			storedHelper.estableceParametro("id_ejecutivo",idEjecutivo);
			storedHelper.estableceParametro("id_promotoria",idPromotoria);
			storedHelper.estableceParametro(" id_agente",idAgente);
			
			resultList = (List<DatosVentasProduccion>)storedHelper.obtieneListaResultados();
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
			return resultList;
		}
		return resultList;
	}
			
	public TransporteImpresionDTO getFileFromDisk(String path){
		  File file = null;
		  TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO(); 
		  byte[] byteArray = null;
		  try{		 
		    file = new File(path);		    
		    byteArray = this.getBytesFromFile(file);		    
		  }catch(IOException ex){		    
		  }
		  transporteImpresionDTO.setByteArray(byteArray);
		  return transporteImpresionDTO;
		}
	
	private byte[] getBytesFromFile(File file) throws IOException {
		InputStream is  = null;
		try {
			is = new FileInputStream(file);  
			long length = file.length();	  
			if (length > Integer.MAX_VALUE) {          
				throw new IOException("File \"" + file + 
						"\" is too large to process.");          
			}
			return IOUtils.toByteArray(is);
		} finally {
			IOUtils.closeQuietly(is);
		}
	}
	private String getIdentificador(CotizacionDTO cotizacion) {
		String identificador = "";
		String idAgente ="000";
		try{
			Agente agente = new Agente();
			agente.setId(cotizacion.getSolicitudDTO().getCodigoAgente().longValue());
			if(agente.getIdAgente() == null) {
				agente = agenteMidasService.loadByIdImpresiones(agente);
			}
			idAgente = agente.getIdTipoAgente().toString();
			if(agente != null){
				if (agente.getPromotoria().getEjecutivo().getGerencia()
						.getCentroOperacion().getIdCentroOperacion() != null) {
					identificador += agente.getPromotoria().getEjecutivo()
							.getGerencia().getCentroOperacion()
							.getIdCentroOperacion()
							+ "-";
				} else {
					identificador += "000-";
				}
				
				if(cotizacion.getSolicitudDTO().getCodigoEjecutivo() != null){
					identificador += cotizacion.getSolicitudDTO().getCodigoEjecutivo() + "-";
				}else{
					identificador += "000-";
				}
				
				if(cotizacion.getSolicitudDTO().getIdOficina() != null){
					identificador += cotizacion.getSolicitudDTO().getIdOficina() + "-";
				}else{
					identificador += "000-";
				}
				
				if(agente.getPromotoria().getIdPromotoria() != null){
					identificador += agente.getPromotoria().getIdPromotoria() + "-";
				}else{
					identificador += "0000-";
				}
				
				if(cotizacion.getSolicitudDTO().getCodigoAgente() != null){
					identificador += idAgente + "";
				}else{
					identificador += "0";
				}
			} 
			if(identificador.isEmpty()){
				identificador = "000-000-000-0000-" + idAgente;
			}
		}catch(Exception e){
			identificador = "000-000-000-0000-" + idAgente;
		}
		return identificador;
	}
	
	private JasperReport getJasperReport(String plantilla){
		InputStream reportInputStream = this.getClass()
		.getResourceAsStream(plantilla);
		JasperReport jasperReport = null;
		try {
			jasperReport = JasperCompileManager
			.compileReport(reportInputStream);
		} catch (JRException e) {
			LOG.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		
		return jasperReport;
	}
	
	private String getTipoServicioConfig(BigDecimal tipoServicioId) {
		TipoServicioVehiculoDTO tipoServicio = new TipoServicioVehiculoDTO();
		String tipoServicioConfig = "No disponible";
		if(tipoServicioId != null){
			tipoServicio = entidadService.findById(TipoServicioVehiculoDTO.class, 
					tipoServicioId);
			if(tipoServicio != null){
				tipoServicioConfig = tipoServicio.getDescripcionTipoServVehiculo();
			}
		}
		return tipoServicioConfig;
	}
	
	private DomicilioImpresion getClienteImpresion(ClienteGenericoDTO clienteImpresion,
			DomicilioImpresion domicilioCliente,
			BigDecimal idDomicilioContratante, BigDecimal idContratante) {
		DomicilioImpresion domicilio = new DomicilioImpresion();
		if(domicilioCliente == null){
			if(idDomicilioContratante != null){
				try{
					domicilio = domicilioFacadeRemote.findDomicilioImpresionById(idDomicilioContratante.longValue());
				}catch(Exception e){
					LOG.error(e.getMessage(), e);
				}
				}else{
				if(clienteImpresion != null){
					ClienteGenericoDTO filtro = new ClienteGenericoDTO();
					filtro.setIdCliente(idContratante);
					try {
						ClienteGenericoDTO cliente = clienteFacadeRemote.loadById(filtro);
						if(cliente != null && cliente.getIdDomicilioConsulta() != null){
							domicilio =  domicilioFacadeRemote.findDomicilioImpresionById(cliente.getIdDomicilioConsulta());
				}
					} catch (Exception e) {
						LOG.error(e.getMessage(), e);
				}
			} 
			}
		}
		
		return domicilio;
	}
	
	private ClienteGenericoDTO getClienteSinDireccion(
			ClienteGenericoDTO cliente, BigDecimal idContratante) {
		
		if(cliente == null && idContratante != null){
			ClienteGenericoDTO filtro = new ClienteGenericoDTO();
			filtro.setIdCliente(idContratante);
		try {
				 return clienteFacadeRemote.loadByIdNoAddress(filtro);
		}catch (Exception e) {
				LOG.error(e.getMessage(), e);
			}
		}
		return null;
	}
	
	public void actualizarEstatusReporteDetPrimas(String estatus) {
		agenteMidasDao.updateEstatusReporteDetPrimas(estatus);
	}
	
	public List<RetencionImpuestosReporteView>llenarDatosDetallePrimaRetencionImpuestos(RetencionImpuestosReporteView filtro){
		return retencionImpuestosDao.llenarDatosDetallePrimaRetencionImpuestos(filtro); 
	}
	
	@EJB 
	private AgrupadorTarifaSeccionService agrupadorTarifaSeccionService;
	@EJB 
	private NegocioService negocioService;
	@EJB 
	private NegocioAgenteService negocioAgenteService;
	@EJB 
	private BasesEmisionDao basesEmisionDao;
	@EJB
	private IncisoService incisoService;
	
	@EJB 
	private ReporteBaseCompensacionesDao reporteBaseCompensacionesDao;
	
	@EJB private NegocioDeduciblesService negocioDeduciblesService;
	
	@EJB
	private SectorFacadeRemote sectorFacadeRemote;
	
	@EJB
	private MidasBaseReporteService baseReporteService;
	@PersistenceContext 
	private EntityManager entityManager;
	
	@EJB
	private NegocioCobPaqSeccionService negocioCobPaqSeccionService;
	
	@EJB
	private RetencionImpuestosDao retencionImpuestosDao;
	
	@EJB
	public void setRepSaldoVSContService(RepSaldoVSContService repSaldoVSContService) {
		this.repSaldoVSContService = repSaldoVSContService;
	}
	
	@EJB
	public void setSapAmisBitacoraService(
			SapAmisBitacoraService sapAmisBitacoraService) {
		this.sapAmisBitacoraService = sapAmisBitacoraService;
	}

	@EJB
	public void setPagarePrestamoAnticipoService(PagarePrestamoAnticipoService pagarePrestamoAnticipoService) {
		this.pagarePrestamoAnticipoService = pagarePrestamoAnticipoService;
	}

	@EJB
	public void setConfigPrestamoAnticipoService(
			ConfigPrestamoAnticipoService configPrestamoAnticipoService) {
		this.configPrestamoAnticipoService = configPrestamoAnticipoService;
	}

	@EJB
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}

	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	
	@EJB
	public void setConfiguracionDatoIncisoService(
			ConfiguracionDatoIncisoService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setPersonaSeycosFacadeRemote(
			PersonaSeycosFacadeRemote personaSeycosFacadeRemote) {
		this.personaSeycosFacadeRemote = personaSeycosFacadeRemote;
	}

	@EJB
	public void setEstadoFacadeRemote(EstadoFacadeRemote estadoFacadeRemote) {
		this.estadoFacadeRemote = estadoFacadeRemote;
	}

	@EJB
	public void setMunicipioFacadeRemote(MunicipioFacadeRemote municipioFacadeRemote) {
		this.municipioFacadeRemote = municipioFacadeRemote;
	}

	@EJB
	public void setPaisFacadeRemote(PaisFacadeRemote paisFacadeRemote) {
		this.paisFacadeRemote = paisFacadeRemote;
	}

	@EJB
	public void setEstadoCivilFacadeRemote(
			EstadoCivilFacadeRemote estadoCivilFacadeRemote) {
		this.estadoCivilFacadeRemote = estadoCivilFacadeRemote;
	}

	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}

	@EJB
	public void setDomicilioFacadeRemote(DomicilioFacadeRemote domicilioFacadeRemote) {
		this.domicilioFacadeRemote = domicilioFacadeRemote;
	}

	public List<CatalogoValorFijoDTO> getPosiblesDeducibles() {
		return posiblesDeducibles;
	}

	public void setPosiblesDeducibles(List<CatalogoValorFijoDTO> posiblesDeducibles) {
		this.posiblesDeducibles = posiblesDeducibles;
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setDetalleCargosService(DetalleCargosService detalleCargosService) {
		this.detalleCargosService = detalleCargosService;
	}

	@EJB
	public void setConfigCargosService(ConfigCargosService configCargosService) {
		this.configCargosService = configCargosService;
	}

    @EJB
	public void setCaParametrosDao(CaParametrosDao caParametrosDao) {
		this.caParametrosDao = caParametrosDao;
	}

	public void setNegocioCobPaqSeccionService(
			NegocioCobPaqSeccionService negocioCobPaqSeccionService) {
		this.negocioCobPaqSeccionService = negocioCobPaqSeccionService;
	}
	
	@EJB
	public void setAgenteMidasDao(AgenteMidasDao agenteMidasDao) {
		this.agenteMidasDao = agenteMidasDao;
	}


}