package mx.com.afirme.midas2.service.impl.movil.ajustador;

import static mx.com.afirme.midas.sistema.Utilerias.convertToUTF8;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.movil.ajustador.Acciones;
import mx.com.afirme.midas2.domain.movil.ajustador.Ajustador;
import mx.com.afirme.midas2.domain.movil.ajustador.AjustadorMovil;
import mx.com.afirme.midas2.domain.movil.ajustador.Alertas;
import mx.com.afirme.midas2.domain.movil.ajustador.AtencionATallerAsegurado;
import mx.com.afirme.midas2.domain.movil.ajustador.AtencionATercero;
import mx.com.afirme.midas2.domain.movil.ajustador.AtencionParaPagosDeDanios;
import mx.com.afirme.midas2.domain.movil.ajustador.Bien;
import mx.com.afirme.midas2.domain.movil.ajustador.Catalogo;
import mx.com.afirme.midas2.domain.movil.ajustador.Cobertura;
import mx.com.afirme.midas2.domain.movil.ajustador.CoberturaIncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.DeclaracionSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.DireccionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Domicilio;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaResponse;
import mx.com.afirme.midas2.domain.movil.ajustador.IncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarContactoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarTerminacionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.PaseServicioMedico;
import mx.com.afirme.midas2.domain.movil.ajustador.PaseServicioObraCivil;
import mx.com.afirme.midas2.domain.movil.ajustador.Persona;
import mx.com.afirme.midas2.domain.movil.ajustador.Poliza;
import mx.com.afirme.midas2.domain.movil.ajustador.ProcedeSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.Recibo;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestrosAsignados;
import mx.com.afirme.midas2.domain.movil.ajustador.SaveDocumentoSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Tercero;
import mx.com.afirme.midas2.domain.movil.ajustador.TerceroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.TipoDocumentoSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.ValeGrua;
import mx.com.afirme.midas2.domain.movil.ajustador.Vehiculo;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAcciones;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesRelacion;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.service.OracleCurrentDateService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlInOutParameter;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Stateless
public class AjustadorMovilServiceImpl implements AjustadorMovilService {

	@PersistenceContext
	private EntityManager em;
	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate nJdbcTemplate;
	private SimpleJdbcCall siniestrosEnAtencionFunc;
	private SimpleJdbcCall getDatosPolizaProc;
	private SimpleJdbcCall asignarInciso;
	private SimpleJdbcCall getCoberturasAutoProc;
	private SimpleJdbcCall getRecibosAutoProc;
	private SimpleJdbcCall isProcedeProc;
	private SimpleJdbcCall getTiposSiniestro;
	private SimpleJdbcCall getTerminosAjuste;
	private SimpleJdbcCall getResponsabilidad;
	private SimpleJdbcCall guardaInfoGralReporte;
	private SimpleJdbcCall getCoberturasAfecta;
	private SimpleJdbcCall getTerceros;
	private SimpleJdbcCall getTercero;
	private SimpleJdbcCall getTerceroVehiculo;
	private SimpleJdbcCall getTerceroPersona;
	private SimpleJdbcCall getTerceroBien;
	private SimpleJdbcCall getTiposPases;
	private SimpleJdbcCall afectaReservaDM;
	private SimpleJdbcCall afectaReservaRCV;
	private SimpleJdbcCall afectaReservaRCB;
	private SimpleJdbcCall afectaReservaRCP;
	private SimpleJdbcCall afectaReservaGM;
	private SimpleJdbcCall afectaReservaEXM;
	private SimpleJdbcCall afectaReservaRT;
	private SimpleJdbcCall afectaReservaCyAoEE;
	private SimpleJdbcCall getCatalogoSimple;
	private SimpleJdbcCall getMunicipios;
	private SimpleJdbcCall getInfoCP;
	private SimpleJdbcCall convierteReclamacion;
	private SimpleJdbcCall notificarHgs;
	private SimpleJdbcCall enviaMail;
	
	@EJB
	public EntidadService entidadService;
		
	@EJB
	private OracleCurrentDateService oracleCurrentDateService;
	
	@EJB
	private SistemaContext sistemaContext;

	private static Logger log = Logger.getLogger(AjustadorMovilServiceImpl.class);	

	@Resource
	private Validator validator;
	public enum ConfigCobertura {
        DM(1), GM(7), RCB(1564), RCV(1563), RCP(1562), EXM(1822), RT(2), AYC(11), EE(12);
        private int value;
 
        private ConfigCobertura(int value) {
            this.value = value;
        }
    }
	
	@EJB
	private FortimaxV2Service fortimaxV2Service;	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	private static final String GAVETA_DOC_SINIESTROS_FORTIMAX = "SEG_SIN_AUTOS";
	private static final String SEPARADOR = "-";
	private static final RowMapper<FindPolizaResponse> findPolizaResponseMapper = new RowMapper<FindPolizaResponse>() {
		@Override
		public FindPolizaResponse mapRow(ResultSet rs, int rowNum)
				throws SQLException {
			FindPolizaResponse response = new FindPolizaResponse();
			response.setFechaInicioVigencia(rs.getDate("f_ini_vigencia"));
			response.setFechaFinVigencia(rs.getDate("f_fin_vigencia"));
			String numeroPoliza = String.format("%03d", rs.getInt("id_centro_emis")) + SEPARADOR + String.format("%012d", rs.getLong("num_poliza")) + SEPARADOR + String.format("%02d", rs.getInt("num_renov_pol")); 
			response.setNumeroPoliza(numeroPoliza);
			int numeroInciso = rs.getInt("id_inciso");
			response.setNumeroInciso(numeroInciso);
			response.setNumeroSerie(rs.getString("num_serie"));
			response.setVehiculo(rs.getString("desc_vehic") + rs.getString("id_modelo_auto"));
			response.setId(rs.getLong("id_cotizacion")  + SEPARADOR + rs.getLong("id_lin_negocio") + SEPARADOR + numeroInciso);			
			response.setEstatus(rs.getString("sit_poliza_desc") + "-" + rs.getString("mot_sit_cotiz_desc"));
			response.setPlaca(rs.getString("placa"));
			response.setNombreContratante(rs.getString("nombreContratante"));
			return response;
		}		
	};
	


	
	//Lo tuve que poner porque marca un error en el IDE:
	//CommonAnnotations for Java, 2.3: @Resource methods must be setters that follow 
	//the standard JavaBeans convention. i.e. void "setProperty(<Type> value)" for "<Type> property".
	@SuppressWarnings("unused")
	private DataSource dataSource;
	
	@Resource(name="jdbc/InterfazDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.nJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		this.siniestrosEnAtencionFunc = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withFunctionName("get_Siniestros_En_Atencion")
		.declareParameters(new SqlParameter("pidAjustador", Types.INTEGER))
        .declareParameters(new SqlOutParameter("return", OracleTypes.CURSOR, new RowMapper<ReporteSiniestro>(){ 
        	@Override
        	public ReporteSiniestro mapRow(ResultSet rs, int rowNum) throws SQLException {
        		return getReporteSiniestro(rs.getLong("ID_REPORTE_SIN"));
        	}
        }));		
		
		this.getDatosPolizaProc = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getDatosPoliza")
		.declareParameters(
				new SqlParameter("pid_Cotizacion", Types.INTEGER), 
				new SqlParameter("pid_Lin_Negocio", Types.INTEGER),
				new SqlParameter("pid_Inciso", Types.INTEGER), 
				new SqlParameter("pFecha_Ocurrido", Types.DATE),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Poliza>() {
					@Override
					public Poliza mapRow(ResultSet rs, int index)
							throws SQLException {
						Poliza poliza = new Poliza();
						poliza.setNumeroPoliza(rs.getString("POLIZA"));
						poliza.setFechaInicioVigencia(rs.getDate("F_INI_VIGENCIA"));
						poliza.setFechaFinVigencia(rs.getDate("F_FIN_VIGENCIA"));
						poliza.setEstatus(rs.getString("SITUACION_POLIZA"));
						ProcedeSiniestro procedeSiniestro = new ProcedeSiniestro();			
						procedeSiniestro.setProcede(rs.getBoolean("PROCEDE"));
						procedeSiniestro.setDescripcion(rs.getString("PROCEDE_DESCRIPCION"));
						poliza.setProcedeSiniestro(procedeSiniestro);
						poliza.setNombreContratante(rs.getString("CONTRATANTE"));
						poliza.setNombreConductor(rs.getString("CONDUCTOR"));
						poliza.setVehiculo(rs.getString("VEHICULO_ASEGURADO"));
						poliza.setNumeroSerie(rs.getString("NUMERO_SERIE"));
						poliza.setCapacidad(rs.getString("CAPACIDAD"));
						poliza.setServicio(rs.getString("SERVICIO"));
						poliza.setUso(rs.getString("USO"));
						poliza.setNumeroMotor(rs.getString("MOTOR"));
						poliza.setPlaca(rs.getString("PLACA"));
						poliza.setNumeroInciso(rs.getInt("ID_INCISO"));
						poliza.setTipoCarga(rs.getString("TIPO_CARGA"));
						poliza.setLineaNegocio(rs.getString("LIN_NEGOCIO_DESC"));
						poliza.setPaquete(rs.getString("PAQUETE"));
						return poliza;
					}
				}));
		
		this.getCoberturasAutoProc = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getCoberturasAuto")
		.declareParameters(
				new SqlParameter("pid_Cotizacion", Types.INTEGER), 
				new SqlParameter("pid_Lin_Negocio", Types.INTEGER),
				new SqlParameter("pid_Inciso", Types.INTEGER), 
				new SqlParameter("pFecha_Ocurrido", Types.DATE),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Cobertura>() {
					@Override
					public Cobertura mapRow(ResultSet rs, int index)
							throws SQLException {
						Cobertura cobertura = new Cobertura();
						cobertura.setId(rs.getLong("ID_COBERTURA"));
						cobertura.setNombre(rs.getString("COBERTURAS_AMPARADAS"));
						cobertura.setLimiteMaxResponsabilidad(rs.getString("LIMITE_RESPONSABILIDAD"));
						BigDecimal deducible = rs.getBigDecimal("DEDUCIBLE");
						deducible = !deducible.equals(BigDecimal.ZERO) ? deducible : null; 
						cobertura.setDeducible(deducible);
						cobertura.setPrima(rs.getBigDecimal("PRIMA"));
						return cobertura;
					}
				}));
		
		this.getRecibosAutoProc = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getRecibosAuto")
		.declareParameters(
				new SqlParameter("pid_Cotizacion", Types.INTEGER), 
				new SqlParameter("pid_Lin_Negocio", Types.INTEGER),
				new SqlParameter("pid_Inciso", Types.INTEGER), 
				new SqlParameter("pFecha_Ocurrido", Types.DATE),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Recibo>() {
					@Override
					public Recibo mapRow(ResultSet rs, int index)
							throws SQLException {
						Recibo recibo = new Recibo();
						recibo.setId(rs.getLong("ID_RECIBO"));
						recibo.setFolio(rs.getLong("NUM_FOLIO_RBO"));
						recibo.setFechaInicio(rs.getDate("F_CUBRE_DESDE"));
						recibo.setFechaFin(rs.getDate("F_CUBRE_HASTA"));
						recibo.setEstatus(rs.getString("SIT_RECIBO"));
						return recibo;
					}
				}));
		this.isProcedeProc = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("isProcede")
		.declareParameters(
				new SqlParameter("pid_Cotizacion", Types.INTEGER), 
				new SqlParameter("pid_Lin_Negocio", Types.INTEGER),
				new SqlParameter("pid_Inciso", Types.INTEGER), 
				new SqlParameter("pFecha_Ocurrido", Types.DATE),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<ProcedeSiniestro>() {
					@Override
					public ProcedeSiniestro mapRow(ResultSet rs, int index)
							throws SQLException {
						ProcedeSiniestro procedeSiniestro = new ProcedeSiniestro();
						procedeSiniestro.setProcede(rs.getBoolean("procede"));
						procedeSiniestro.setDescripcion(rs.getString("procede_descripcion"));
						return procedeSiniestro;
					}
				}));
		

		this.asignarInciso = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AsignaPolizaSin")
		.declareParameters(
				new SqlParameter("pidReporteSin", Types.NUMERIC),
				new SqlParameter("pidCotizacion", Types.NUMERIC),
				new SqlParameter("pLinea", Types.NUMERIC),
				new SqlParameter("pNumInciso", Types.NUMERIC),
				new SqlParameter("pUsuario", Types.VARCHAR),
				new SqlOutParameter("pid_Cod_Resp",Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo",Types.VARCHAR));
		
		this.getTiposSiniestro = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getTiposSiniestro")
		.declareParameters(
				new SqlParameter("pcve_t_bien", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Catalogo>() {
					@Override
					public Catalogo mapRow(ResultSet rs, int index)
							throws SQLException {
						Catalogo item = new Catalogo();
						item.setLabel(rs.getString("desc_causa_sin"));
						item.setValue(rs.getString("Id_causa_sin"));
						return item;
					}
				}));
		
		this.getTerminosAjuste = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getTerminosAjuste")
		.declareParameters(
				new SqlParameter("pid_causa_sin", Types.VARCHAR),
				new SqlParameter("pCvel_T_Resp", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Catalogo>() {
					@Override
					public Catalogo mapRow(ResultSet rs, int index)
							throws SQLException {
						Catalogo item = new Catalogo();
						item.setLabel(rs.getString("desc_valor"));
						item.setValue(rs.getString("cve_term_ajust"));
						return item;
					}
				}));
		
		this.getResponsabilidad = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getResponsabilidad")
		.declareParameters(
				new SqlParameter("pid_causa_sin", Types.VARCHAR),
				new SqlParameter("pCve_Term_Ajust", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Catalogo>() {
					@Override
					public Catalogo mapRow(ResultSet rs, int index)
							throws SQLException {
						Catalogo item = new Catalogo();
						item.setLabel(rs.getString("Desc_Valor"));
						item.setValue(rs.getString("CVEL_T_RESP"));
						return item;
					}
				}));
		
		this.guardaInfoGralReporte = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("GuardaInfoGralReporte")
		.declareParameters(
				new SqlParameter("pidReporteSin", Types.NUMERIC),
				new SqlParameter("pidCausaSin", Types.NUMERIC),
				new SqlParameter("pCveTermAjte", Types.VARCHAR),
				new SqlParameter("pCveTResp", Types.VARCHAR),
				new SqlParameter("pDeclaracion", Types.VARCHAR),
				new SqlParameter("pBMismoAsegu", Types.NUMERIC),
				new SqlParameter("pNomConductor", Types.VARCHAR),
				new SqlParameter("pCveSexoCond", Types.VARCHAR),
				new SqlParameter("pEdadCond", Types.NUMERIC),
				new SqlParameter("pCveCurp", Types.VARCHAR),
				new SqlParameter("pCveEstado", Types.VARCHAR),
				new SqlParameter("pCveRfc", Types.VARCHAR),
				new SqlParameter("pTelefono", Types.VARCHAR),
				new SqlParameter("pBEquipoPesado", Types.NUMERIC),
				new SqlParameter("pCvelLugarAjust", Types.VARCHAR),
				new SqlParameter("pIdCiaAsegResp", Types.NUMERIC),
				new SqlOutParameter("pid_Cod_Resp",Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo",Types.VARCHAR));

		this.getCoberturasAfecta = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getCoberturas_Afecta")
		.declareParameters(
				new SqlParameter("pid_reporte_sin", Types.NUMERIC),
				new SqlParameter("pid_Cotizacion", Types.INTEGER), 
				new SqlParameter("pid_lin_negocio", Types.INTEGER),
				new SqlParameter("pid_inciso", Types.INTEGER), 
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Cobertura>() {
					@Override
					public Cobertura mapRow(ResultSet rs, int index)
							throws SQLException {
						Cobertura cobertura = new Cobertura();
						cobertura.setId(rs.getLong("id_cobertura"));
						cobertura.setNombre(rs.getString("nom_cobertura"));
						cobertura.setAfectable(rs.getBoolean("afectable"));
						cobertura.setRegistro(rs.getString("Origen").equalsIgnoreCase("SIN"));
						return cobertura;
					}
				}));
		this.getTerceros = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("pListaTercero")
		.declareParameters(
				new SqlParameter("pid_reporte_sin", Types.NUMERIC),
				new SqlParameter("pid_lin_negocio", Types.INTEGER), 
				new SqlParameter("pid_inciso", Types.INTEGER),
				new SqlParameter("pid_cobertura", Types.INTEGER), 
				new SqlOutParameter("pId_Cod_Resp",Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo",Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Tercero>() {
					@Override
					public Tercero mapRow(ResultSet rs, int index)
							throws SQLException {
						Tercero tercero = new Tercero();
						tercero.setId(rs.getLong("id_tercero"));
						tercero.setNombre(rs.getString("nom_tercero"));
						tercero.setDescripcion(rs.getString("descripcion"));
						tercero.setTipoPase(rs.getString("tipoPase"));
						tercero.setEstimacion(rs.getBigDecimal("importe_estimado"));
						return tercero;
					}
				}));
		
		this.getTercero = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("pConsTercero")
		.declareParameters(
				new SqlParameter("pid_reporte_sin", Types.NUMERIC),
				new SqlParameter("pid_tercero", Types.NUMERIC),
				new SqlParameter("pidCotizacion", Types.NUMERIC),
				new SqlParameter("pid_lin_negocio", Types.INTEGER),
				new SqlParameter("pid_inciso", Types.INTEGER), 
				new SqlParameter("pid_cobertura", Types.INTEGER), 
				new SqlOutParameter("pId_Cod_Resp",Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo",Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Tercero>() {
					@Override
					public Tercero mapRow(ResultSet rs, int index)
							throws SQLException {
						Tercero t = new Tercero();
						if(rs.getLong("id_tercero")>0){
							t.setId(rs.getLong("id_tercero"));
						}
						t.setReporteSiniestroId(rs.getLong("id_reporte_sin"));
						t.setCoberturaId(rs.getInt("id_cobertura"));
						t.setEstimacion(rs.getBigDecimal("importe_estimado"));
						t.setAplicaDeducible(rs.getBoolean("b_deducible"));
						t.setSumaAmparada(rs.getBigDecimal("suma_amparada"));
						return t;
					}
				}));
		
		this.getTerceroVehiculo = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("pConsTercero")
		.declareParameters(
				new SqlParameter("pid_reporte_sin", Types.NUMERIC),
				new SqlParameter("pid_tercero", Types.NUMERIC),
				new SqlParameter("pidCotizacion", Types.NUMERIC),
				new SqlParameter("pid_lin_negocio", Types.INTEGER),
				new SqlParameter("pid_inciso", Types.INTEGER), 
				new SqlParameter("pid_cobertura", Types.INTEGER), 
				new SqlOutParameter("pId_Cod_Resp",Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo",Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Vehiculo>() {
					@Override
					public Vehiculo mapRow(ResultSet rs, int index)
							throws SQLException {
						Vehiculo t = new Vehiculo();
						if(rs.getLong("id_tercero")>0){
							t.setId(rs.getLong("id_tercero"));
						}
						t.setReporteSiniestroId(rs.getLong("id_reporte_sin"));
						t.setNombre(rs.getString("nom_tercero"));
						t.setContacto(rs.getString("nom_pers_cont"));
						t.setTelefono(rs.getString("telef_1"));
						t.setCorreo(rs.getString("correo"));
						t.setCoberturaId((rs.getInt("id_cobertura") != ConfigCobertura.DM.value && 
								rs.getInt("id_cobertura") != ConfigCobertura.AYC.value && 
								rs.getInt("id_cobertura") != ConfigCobertura.EE.value)?ConfigCobertura.RCV.value:rs.getInt("id_cobertura"));
						t.setEstimacion(rs.getBigDecimal("importe_estimado"));
						t.setEstimacionAnterior(rs.getBigDecimal("importe_estimado_anterior"));
						if(rs.getObject("b_deducible")!=null){
							t.setAplicaDeducible(rs.getBoolean("b_deducible"));
						}
						t.setObservaciones(rs.getString("tx_siniestro"));
						t.setDescripcion(rs.getString("descripcion"));
						t.setSumaAmparada(rs.getBigDecimal("suma_amparada"));
						
						if(rs.getString("cve_terc_afect")!=null){
							t.setTipo(rs.getString("cve_terc_afect"));
						}
						
						t.setTipoPase(rs.getString("cve_tipo_pase"));
						
						t.setMarcaId(rs.getString("id_marca"));
						
						if(rs.getInt("id_modelo_auto")>0){
							t.setModelo(rs.getInt("id_modelo_auto"));
						}						
						t.setDescripcionTipoEstilo(rs.getString("desc_tipo_estilo"));
						t.setSerie(rs.getString("num_serie"));
						t.setPlaca(rs.getString("placa"));
						t.setColor(rs.getString("desc_color"));
						
						t.setTallerId(rs.getString("id_taller_asig"));
						t.setAreasDanadas(rs.getString("areas_dan"));
						t.setDanosPreexistentes(rs.getString("dan_preexistentes"));
						t.setPerdidaTotal(rs.getBoolean("perdida_total"));
						t.setPctDeducible(rs.getBigDecimal("pct_deducible"));
						if(rs.getObject("b_tiene_cia_aseg")!=null){
							t.setTieneCia(rs.getBoolean("b_tiene_cia_aseg"));
						}
						if(rs.getInt("id_cia_aseg")>0){
							t.setCiaId(rs.getInt("id_cia_aseg"));
						}
						
						t.setSiniestroCia(rs.getString("num_sin_cia_ter"));
						if(rs.getObject("b_reci_pase_aten")!=null){
							t.setRecibeOrdenCia(rs.getBoolean("b_reci_pase_aten"));
						}
						if(rs.getObject("cve_llego_cia")!=null){
							t.setLlegoCia(rs.getBoolean("cve_llego_cia"));
						}
						
						return t;
					}
				}));
		
		this.getTerceroPersona = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("pConsTercero")
		.declareParameters(
				new SqlParameter("pid_reporte_sin", Types.NUMERIC),
				new SqlParameter("pid_tercero", Types.NUMERIC),
				new SqlParameter("pidCotizacion", Types.NUMERIC),
				new SqlParameter("pid_lin_negocio", Types.INTEGER),
				new SqlParameter("pid_inciso", Types.INTEGER), 
				new SqlParameter("pid_cobertura", Types.INTEGER), 
				new SqlOutParameter("pId_Cod_Resp",Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo",Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Persona>() {
					@Override
					public Persona mapRow(ResultSet rs, int index)
							throws SQLException {
						Persona t = new Persona();
						if(rs.getLong("id_tercero")>0){
							t.setId(rs.getLong("id_tercero"));
						}
						t.setReporteSiniestroId(rs.getLong("id_reporte_sin"));
						t.setNombre(rs.getString("nom_tercero"));
						t.setContacto(rs.getString("nom_pers_cont"));
						t.setTelefono(rs.getString("telef_1"));
						t.setCorreo(rs.getString("correo"));
						t.setCoberturaId(rs.getInt("id_cobertura") == ConfigCobertura.GM.value?ConfigCobertura.GM.value:ConfigCobertura.RCP.value);
						t.setEstimacion(rs.getBigDecimal("importe_estimado"));
						t.setEstimacionAnterior(rs.getBigDecimal("importe_estimado_anterior"));
						if(rs.getObject("b_deducible")!=null){
							t.setAplicaDeducible(rs.getBoolean("b_deducible"));
						}
						t.setObservaciones(rs.getString("tx_siniestro"));
						t.setDescripcion(rs.getString("descripcion"));
						t.setSumaAmparada(rs.getBigDecimal("suma_amparada"));
						
						if(rs.getString("cve_terc_afect")!=null){
							t.setTipo(rs.getString("cve_terc_afect"));
						}
						t.setTipoPase(rs.getString("cve_tipo_pase"));
											
						t.setHomicidio(rs.getBoolean("homicidio"));
						t.setHospitalId(rs.getString("id_hospital"));
						t.setMedicoId(rs.getString("id_medico"));
						t.setNumOrdenAtencion(rs.getString("num_orden_atenm"));
						t.setLesiones(rs.getString("lesiones"));
						if(rs.getInt("edad")>0){
							t.setEdad(rs.getInt("edad"));
						}
						t.setLimiteAtencion(rs.getBigDecimal("limiteAtencion"));
						if(rs.getObject("b_tiene_cia_aseg")!=null){
							t.setTieneCia(rs.getBoolean("b_tiene_cia_aseg"));
						}
						if(rs.getInt("id_cia_aseg")>0){
							t.setCiaId(rs.getInt("id_cia_aseg"));
						}
						t.setSiniestroCia(rs.getString("num_sin_cia_ter"));
						if(rs.getObject("b_reci_pase_aten")!=null){
							t.setRecibeOrdenCia(rs.getBoolean("b_reci_pase_aten"));
						}
						if(rs.getObject("cve_llego_cia")!=null){
							t.setLlegoCia(rs.getBoolean("cve_llego_cia"));	
						}
						
						return t;
					}
				}));
		
		this.getTerceroBien = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("pConsTercero")
		.declareParameters(
				new SqlParameter("pid_reporte_sin", Types.NUMERIC),
				new SqlParameter("pid_tercero", Types.NUMERIC),
				new SqlParameter("pidCotizacion", Types.NUMERIC),
				new SqlParameter("pid_lin_negocio", Types.INTEGER),
				new SqlParameter("pid_inciso", Types.INTEGER), 
				new SqlParameter("pid_cobertura", Types.INTEGER), 
				new SqlOutParameter("pId_Cod_Resp",Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo",Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Bien>() {
					@Override
					public Bien mapRow(ResultSet rs, int index)
							throws SQLException {
						Bien t = new Bien();
						if(rs.getLong("id_tercero")>0){
							t.setId(rs.getLong("id_tercero"));
						}
						t.setReporteSiniestroId(rs.getLong("id_reporte_sin"));
						t.setNombre(rs.getString("nom_tercero"));
						t.setContacto(rs.getString("nom_pers_cont"));
						t.setTelefono(rs.getString("telef_1"));
						t.setCorreo(rs.getString("correo"));
						t.setCoberturaId(ConfigCobertura.RCB.value);
						t.setEstimacion(rs.getBigDecimal("importe_estimado"));
						t.setEstimacionAnterior(rs.getBigDecimal("importe_estimado_anterior"));
						if(rs.getObject("b_deducible")!=null){
							t.setAplicaDeducible(rs.getBoolean("b_deducible"));
						}
						t.setObservaciones(rs.getString("tx_siniestro"));
						t.setSumaAmparada(rs.getBigDecimal("suma_amparada"));
						
						t.setTipoPase(rs.getString("cve_tipo_pase"));
						
						t.setAreasDanadas(rs.getString("areas_dan"));
						t.setTipoBien(rs.getString("cvel_bien_afect"));
						t.setIngeniero(rs.getString("nom_ing_asig"));
						t.setPerdidaTotal(rs.getBoolean("perdida_total"));
						
						t.setPaisId(rs.getString("cve_pais"));
						t.setEstadoId(rs.getString("cve_estado"));
						t.setCiudadId(rs.getString("cve_ciudad"));
						
						t.setCalleNumero(rs.getString("calle_numero"));
						t.setCodigoPostal(rs.getString("codigo_postal"));
						t.setColonia(rs.getString("colonia"));
						t.setReferencia(rs.getString("tx_ref_domicilio"));
						t.setKm(rs.getString("num_km"));
						
						return t;
					}
				}));
		
		this.getTiposPases = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getTiposPases")
		.declareParameters(
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Catalogo>() {
					@Override
					public Catalogo mapRow(ResultSet rs, int index)
							throws SQLException {
						Catalogo item = new Catalogo();
						item.setLabel(rs.getString("Desc_Valor"));
						item.setValue(rs.getString("Val_Campo"));
						return item;
					}
				}));
		
		this.afectaReservaDM = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AfectaReservaDM")
		.useInParameterNames("pId_Tercero")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlInOutParameter("pId_Tercero", Types.NUMERIC),
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlParameter("pnomInteresado", Types.VARCHAR),
				new SqlParameter("ptelef1", Types.VARCHAR),
				new SqlParameter("pCorreo", Types.VARCHAR),
				new SqlParameter("pIdCiaAsegTer", Types.NUMERIC),
				new SqlParameter("pIdSinCiaTer", Types.VARCHAR),
				new SqlParameter("precibePaseCiaTerc", Types.NUMERIC),
				new SqlParameter("pCausaMovimiento", Types.VARCHAR),
				new SqlParameter("pCveTipoPase", Types.VARCHAR),
				new SqlParameter("pTipoPerdida", Types.NUMERIC),
				new SqlParameter("pAplicaDeducible", Types.NUMERIC),
				new SqlParameter("pidTaller", Types.NUMERIC),
				new SqlParameter("pAreasDan", Types.VARCHAR),
				new SqlParameter("pDanPreexistentes", Types.VARCHAR),
				new SqlParameter("pValorComercial", Types.NUMERIC),
				new SqlParameter("pimp_Estimado", Types.NUMERIC),
				new SqlParameter("pcve_usuario", Types.VARCHAR),
				new SqlParameter("pObservaciones", Types.VARCHAR),
				new SqlParameter("pPlaca", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));
		
		this.afectaReservaCyAoEE = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AfectaReservaAyCoEqE")
		.useInParameterNames("pId_Tercero")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlInOutParameter("pId_Tercero", Types.NUMERIC),
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlParameter("pnomInteresado", Types.VARCHAR),
				new SqlParameter("ptelef1", Types.VARCHAR),
				new SqlParameter("pCorreo", Types.VARCHAR),
				new SqlParameter("pIdCiaAsegTer", Types.NUMERIC),
				new SqlParameter("pIdSinCiaTer", Types.VARCHAR),
				new SqlParameter("precibePaseCiaTerc", Types.NUMERIC),
				new SqlParameter("pCausaMovimiento", Types.VARCHAR),
				new SqlParameter("pCveTipoPase", Types.VARCHAR),
				new SqlParameter("pTipoPerdida", Types.NUMERIC),
				new SqlParameter("pAplicaDeducible", Types.NUMERIC),
				new SqlParameter("pidTaller", Types.NUMERIC),
				new SqlParameter("pAreasDan", Types.VARCHAR),
				new SqlParameter("pDanPreexistentes", Types.VARCHAR),
				new SqlParameter("pValorComercial", Types.NUMERIC),
				new SqlParameter("pimp_Estimado", Types.NUMERIC),
				new SqlParameter("pcve_usuario", Types.VARCHAR),
				new SqlParameter("pDescripcionTercero", Types.VARCHAR),
				new SqlParameter("pObservaciones", Types.VARCHAR),
				new SqlParameter("pPlaca", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));

		this.afectaReservaRCV = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AfectaReservaRCV")
		.useInParameterNames("pId_Tercero")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlInOutParameter("pId_Tercero", Types.NUMERIC),
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlParameter("pCveTipoPase", Types.VARCHAR),
				new SqlParameter("pCausaMovimiento", Types.VARCHAR),
				new SqlParameter("pTipoPerdida", Types.NUMERIC),
				new SqlParameter("pNomTercero", Types.VARCHAR),
				new SqlParameter("pNomContacto", Types.VARCHAR),
				new SqlParameter("ptelef1", Types.VARCHAR),
				new SqlParameter("pCorreo", Types.VARCHAR),
				new SqlParameter("pcveLlegoCiaTer", Types.NUMERIC),
				new SqlParameter("pIdCiaAsegTer", Types.NUMERIC),
				new SqlParameter("pIdSinCiaTer", Types.VARCHAR),
				new SqlParameter("pidTaller", Types.NUMERIC),
				new SqlParameter("pimp_Estimado", Types.NUMERIC),
				new SqlParameter("pidMarcaVehTer", Types.NUMERIC),
				new SqlParameter("pModeloVehTer", Types.NUMERIC),
				new SqlParameter("pDescTipoEstiloVehTer", Types.VARCHAR),
				new SqlParameter("pNumSerieVehTer", Types.VARCHAR),
				new SqlParameter("pPlacaVehTer", Types.VARCHAR),
				new SqlParameter("pColorVehTer", Types.VARCHAR),
				new SqlParameter("pAreasDan", Types.VARCHAR),
				new SqlParameter("pDanPreex", Types.VARCHAR),
				new SqlParameter("pcve_usuario", Types.VARCHAR),
				new SqlParameter("pObservaciones", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));

		this.afectaReservaRCB = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AfectaReservaRCBI")
		.useInParameterNames("pId_Tercero")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlInOutParameter("pId_Tercero", Types.NUMERIC),
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlParameter("pCveTipoPase", Types.VARCHAR),
				new SqlParameter("pCausaMovimiento", Types.VARCHAR),
				new SqlParameter("pTipoPerdida", Types.NUMERIC),
				new SqlParameter("pTipoBien", Types.VARCHAR),
				new SqlParameter("pNomTercero", Types.VARCHAR),
				new SqlParameter("pNomContacto", Types.VARCHAR),
				new SqlParameter("ptelef1", Types.VARCHAR),
				new SqlParameter("pCorreo", Types.VARCHAR),
				new SqlParameter("pimp_Estimado", Types.NUMERIC),
				new SqlParameter("pcve_pais", Types.VARCHAR),
				new SqlParameter("pcve_estado", Types.VARCHAR),
				new SqlParameter("pcve_ciudad", Types.VARCHAR),
				new SqlParameter("pcalleynum", Types.VARCHAR),
				new SqlParameter("pcodigoPostal", Types.VARCHAR),
				new SqlParameter("pColonia", Types.VARCHAR),
				new SqlParameter("pReferencias", Types.VARCHAR),
				new SqlParameter("pkm", Types.VARCHAR),
				new SqlParameter("pNomIngeniero", Types.VARCHAR),
				new SqlParameter("pAreasDan", Types.VARCHAR),
				new SqlParameter("pcve_usuario", Types.VARCHAR),
				new SqlParameter("pObservaciones", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));
		
		this.afectaReservaRCP = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AfectaReservaRCPER")
		.useInParameterNames("pId_Tercero")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlInOutParameter("pId_Tercero", Types.NUMERIC),
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlParameter("pCveTipoPase", Types.VARCHAR),
				new SqlParameter("pCausaMovimiento", Types.VARCHAR),
				new SqlParameter("pIdCiaAsegTer", Types.NUMERIC),
				new SqlParameter("pIdSinCiaTer", Types.VARCHAR),
				new SqlParameter("pNomTercero", Types.VARCHAR),
				new SqlParameter("pNomContacto", Types.VARCHAR),
				new SqlParameter("ptelef1", Types.VARCHAR),
				new SqlParameter("pCorreo", Types.VARCHAR),
				new SqlParameter("pimp_Estimado", Types.NUMERIC),
				new SqlParameter("pTipoDano", Types.NUMERIC),
				new SqlParameter("pidHospital", Types.NUMERIC),
				new SqlParameter("pidMedico", Types.NUMERIC),
				new SqlParameter("pEdad", Types.NUMERIC),
				new SqlParameter("pLesiones", Types.VARCHAR),
				new SqlParameter("pLimiteAtencion", Types.NUMERIC),
				new SqlParameter("pcve_usuario", Types.VARCHAR),
				new SqlParameter("pObservaciones", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));

		this.afectaReservaGM = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AfectaReservaGM")
		.useInParameterNames("pId_Tercero")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlInOutParameter("pId_Tercero", Types.NUMERIC),
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlParameter("pCausaMovimiento", Types.VARCHAR),
				new SqlParameter("pIdCiaAsegTer", Types.NUMERIC),
				new SqlParameter("pIdSinCiaTer", Types.VARCHAR),
				new SqlParameter("precibePaseCiaTerc", Types.NUMERIC),
				new SqlParameter("pNomTercero", Types.VARCHAR),
				new SqlParameter("pNomContacto", Types.VARCHAR),
				new SqlParameter("ptelef1", Types.VARCHAR),
				new SqlParameter("pCorreo", Types.VARCHAR),
				new SqlParameter("pimp_Estimado", Types.NUMERIC),
				new SqlParameter("pTipoDano", Types.NUMERIC),
				new SqlParameter("pidHospital", Types.NUMERIC),
				new SqlParameter("pidMedico", Types.NUMERIC),
				new SqlParameter("pEdad", Types.NUMERIC),
				new SqlParameter("pLesiones", Types.VARCHAR),
				new SqlParameter("pLimiteAtencion", Types.NUMERIC),
				new SqlParameter("pcve_usuario", Types.VARCHAR),
				new SqlParameter("pObservaciones", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));

		this.afectaReservaEXM = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AfectaReservaRCEXCM")
		.useInParameterNames("pId_Tercero")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlInOutParameter("pId_Tercero", Types.NUMERIC),
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlParameter("pCausaMovimiento", Types.VARCHAR),
				new SqlParameter("pimp_Estimado", Types.NUMERIC),
				new SqlParameter("pcve_usuario", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));
		
		this.afectaReservaRT = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("AfectaReservaRT")
		.useInParameterNames("pId_Tercero")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlInOutParameter("pId_Tercero", Types.NUMERIC),
				new SqlParameter("pid_cobertura", Types.NUMERIC),
				new SqlParameter("pCausaMovimiento", Types.VARCHAR),
				new SqlParameter("pimp_Estimado", Types.NUMERIC),
				new SqlParameter("pimp_SumaAsegurada", Types.NUMERIC),
				new SqlParameter("pcve_usuario", Types.VARCHAR),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR));

		this.getCatalogoSimple = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getCatalogo")
		.declareParameters(
				new SqlParameter("P_CATALOG_TYPE", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Catalogo>() {
					@Override
					public Catalogo mapRow(ResultSet rs, int index)
							throws SQLException {
						Catalogo item = new Catalogo();
						item.setLabel(rs.getString("descripcion"));
						item.setValue(rs.getString("valor"));
						return item;
					}
				}));
		
		this.getMunicipios = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getMunicipios")
		.declareParameters(
				new SqlParameter("pid_estado", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Catalogo>() {
					@Override
					public Catalogo mapRow(ResultSet rs, int index)
							throws SQLException {
						Catalogo item = new Catalogo();
						item.setLabel(rs.getString("municipio"));
						item.setValue(rs.getString("id_municipio"));
						return item;
					}
				}));
		
		this.getInfoCP = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("getInfoCP")
		.declareParameters(
				new SqlParameter("pcodigo_p", Types.VARCHAR),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR, new RowMapper<Catalogo>() {
					@Override
					public Catalogo mapRow(ResultSet rs, int index)
							throws SQLException {
						Catalogo item = new Catalogo();
						item.setLabel(rs.getString("id_estado"));
						item.setValue(rs.getString("colonia"));
						item.setExtra(rs.getString("id_municipio"));
						return item;
					}
				}));
		
		this.convierteReclamacion = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("ConvierteReclamacion")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlParameter("pCve_Usuario",Types.VARCHAR),
				new SqlOutParameter("pNum_Folio_Rep", Types.NUMERIC),
				new SqlOutParameter("pAnio_Rep", Types.NUMERIC),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR)
		);
		this.notificarHgs =  new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("notificaHGS")
		.declareParameters(
				new SqlParameter("pId_Reporte_sin", Types.NUMERIC),
				new SqlOutParameter("pId_Cod_Resp", Types.VARCHAR),
				new SqlOutParameter("pDesc_Codigo", Types.VARCHAR)
		);
		this.enviaMail = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("SEYCOS")
		.withCatalogName("pkg_not_sin_sms")
		.withProcedureName("SP_PREPEXEC_MAIL")
		.declareParameters(
				new SqlParameter("pid_reporte", Types.NUMERIC),
				new SqlParameter("pid_cotizacion", Types.NUMERIC),
				new SqlParameter("pid_inciso", Types.NUMERIC),
				new SqlParameter("pid_lin_negocio", Types.NUMERIC),
				new SqlParameter("pDescripcionInc", Types.VARCHAR),
				new SqlParameter("pDescripcionSin", Types.VARCHAR),
				new SqlOutParameter("pcode",Types.VARCHAR),
				new SqlOutParameter("pmssg",Types.VARCHAR));

	}
	
	@Override
	public ReporteSiniestro getReporteSiniestro(Long id) {
		String sql = "select SIN_RECLAMACION.ID_REPORTE_SIN ID, NUM_REP_CABINA NUMEROREPORTE, \r\n" + 
				"    CASE WHEN NUM_POLIZA IS NULL THEN NULL \r\n" + 
				"        ELSE LPAD(ID_CENTRO_EMISPO, 3, '0') || '-' || LPAD(NUM_POLIZA, 12, '0') || '-' || LPAD(NUM_RENOV_POL, 2, '0') END POLIZA,\r\n" + 
				"    CASE WHEN NUM_POLIZA IS NULL THEN NULL \r\n" + 
				"	 	 ELSE SIN_RECLAMACION.id_cotizacion || '-' || SIN_INCISO_RECL.id_Lin_Negocio || '-' || SIN_INCISO_RECL.ID_INCISO END IDPOLIZA,\r\n" +
				"    SIN_INCISO_RECL.ID_INCISO NUMEROINCISO,\r\n" + 
				"    pob_pais.desc_poblacion PAIS,\r\n" + 
				"    pob_estado.desc_poblacion ESTADO,\r\n" + 
				"    pob_ciudad.desc_poblacion CIUDAD,\r\n" + 
				"    CALLE_NUMERO CALLENUMERO,\r\n" + 
				"    CODIGO_POSTAL CODIGOPOSTAL,\r\n" + 
				"    TX_REF_DOMICILIO REFERENCIA,\r\n" + 
				"    COLONIA,\r\n" + 
				"    NUM_KM KILOMETRO, \r\n" + 
				"    NULL UBICACIONLAT,\r\n" + 
				"    NULL UBICACIONLON,\r\n" + 
				"    TXT_VEH_PARTICIP VEHICULOSPARTICIPANTES,\r\n" + 
				"    NOM_REPORTA NOMBREREPORTA,\r\n" + 
				"    TXT_CONDUCTOR NOMBRECONDUCTOR,\r\n" + 
				"    INS_CAUSA_SIN.DESC_CAUSA_SIN CAUSASINIESTRO,\r\n" + 
				"    TELEFS_REPORTA TELEFONO,\r\n" + 
				"    FH_REPORTADO FECHAREPORTE,\r\n" + 
				"    FH_OCURRIDO FECHAOCURRIDO,\r\n" + 
				"    FH_ENTERADO_ASIGN FECHAENTERADOASIGNACION,\r\n" + 
				"    FH_CONTACTO FECHACONTACTO,\r\n" + 
				"    FH_TER_SERV FECHATERMINACION,\r\n" + 
				"    FH_ASIGNA_SERV FECHAASIGNACION,\r\n" +
				"    FH_CITA_SERV FECHACITA,\r\n" + 
				"    ID_PREST_SERV AJUSTADORID,\r\n" + 
				"    NOM_PREST_SERV AJUSTADORNOMBRE,\r\n" +
				"    SIN_INCISO_RECL.DESC_INCISO,\r\n" +
				"    SIN_INCISO_RECL.CVE_T_BIEN TIPOINCISO,\r\n" + 
				"    SIN_INCISO_RECL.ID_CAUSA_SIN TIPOSINIESTRO,\r\n" +
				"    TRIM(SIN_AUTO.CVE_TERM_AJUST) TERMINOAJUSTE,\r\n" +
				"    TRIM(SIN_AUTO.CVEL_T_RESP) RESPONSABILIDAD,\r\n" +
				"	 tx_siniestro DECLARACION,\r\n" +
				"	 ID_EXPEDIENTE_FORTIMAX EXPEDIENTEFORTIMAXID\r\n," +
				"	 NUM_RECLAMACION NUMEROSINIESTRO\r\n," + 
				"	 DECODE(TRIM(SIN_AUTO.B_EQUIPO_PESADO),'V',1,0) PARTICIPOEQPESADO\r\n," +
				"	 SIN_AUTO.NOM_CONDUCTOR CONDUCTOR\r\n," +
				"	 SIN_AUTO.CVE_SEXO_COND SEXOCOND\r\n," + 
				"	 SIN_AUTO.EDAD_COND EDADCOND\r\n," +
				"	 SIN_AUTO.Cvel_Lugar_Ajust LUGARAJUST\r\n," +
				"	 DECODE(TRIM(Sin_Det_Asegurado.b_mismo_asegu),'V',1,0) CONDUCTORMISMOASEG\r\n," +
				"    Sin_Det_Asegurado.cve_curp CURPASEG\r\n," +
				"    Sin_Det_Asegurado.cve_estado ESTADOASEG\r\n," +
				"    Sin_Det_Asegurado.cve_rfc RFCASEG\r\n," +
				"    Sin_Det_Asegurado.telefono TELEFONOASEG\r\n," +
				"	 sin_reclamacion.ID_CIA_ASEG_RESP\r\n" +
				"    from SEYCOS.SIN_RECLAMACION\r\n" +  
				"    left join SEYCOS.INS_CAUSA_SIN ON SIN_RECLAMACION.ID_CAUSA_SIN = INS_CAUSA_SIN.ID_CAUSA_SIN \r\n" + 
				"    LEFT join SEYCOS.SIN_INCISO_RECL \r\n" + 
				"        on SIN_RECLAMACION.ID_REPORTE_SIN = SIN_INCISO_RECL.ID_REPORTE_SIN AND SIN_RECLAMACION.ID_COTIZACION=SIN_INCISO_RECL.ID_COTIZACION AND SIN_INCISO_RECL.F_TER_REG = DATE '4712-12-31' AND SIN_INCISO_RECL.b_ult_reg_dia = 'V' \r\n" +
				"        and SIN_INCISO_RECL.ID_VERSION_RECL = (SELECT   MAX (Id_Version_Recl) FROM   SEYCOS.SIN_INCISO_RECL WHERE ID_REPORTE_SIN = ? )\r\n" +
				"    LEFT join SEYCOS.SIN_AUTO \r\n" + 
				"        on SIN_AUTO.ID_REPORTE_SIN = SIN_RECLAMACION.ID_REPORTE_SIN AND SIN_AUTO.ID_VERSION_RECL = SIN_INCISO_RECL.id_version_recl     \r\n" +
				"    LEFT join SEYCOS.Sin_Det_Asegurado \r\n" + 
				"        on Sin_Det_Asegurado.ID_REPORTE_SIN = SIN_RECLAMACION.ID_REPORTE_SIN AND Sin_Det_Asegurado.ID_VERSION_RECL = SIN_INCISO_RECL.id_version_recl     \r\n" +
				"	 LEFT JOIN Seycos.Sin_Texto ON SIN_TEXTO.id_texto_sin = SIN_RECLAMACION.id_texto_sin \r\n" +
				"    inner join SEYCOS.sin_domicilio on sin_domicilio.id_domicilio_sin = sin_reclamacion.id_domicilio_sin\r\n" + 
				"    inner join SEYCOS.poblacion pob_pais on sin_domicilio.cve_pais = pob_pais.cve_poblacion\r\n" + 
				"    inner join SEYCOS.poblacion pob_estado on sin_domicilio.cve_estado = pob_estado.cve_poblacion\r\n" + 
				"    inner join SEYCOS.poblacion pob_ciudad on sin_domicilio.cve_ciudad = pob_ciudad.cve_poblacion\r\n" + 
				"    LEFT join SEYCOS.SIN_SERVICIO on SIN_RECLAMACION.ID_REPORTE_SIN = SIN_SERVICIO.ID_REPORTE_SIN and SIN_SERVICIO.ID_SERVICIO_SIN = SEYCOS.APP_MOVILES_AJUSTADORES.getServicioVigente(SIN_RECLAMACION.ID_REPORTE_SIN) \r\n" + 
				"    where SIN_RECLAMACION.id_reporte_sin = ? and SIN_RECLAMACION.F_TER_REG = DATE '4712-12-31'\r\n"+
				"    and SIN_RECLAMACION.ID_VERSION_RECL = (SELECT   MAX (Id_Version_Recl) FROM   SEYCOS.SIN_RECLAMACION  WHERE ID_REPORTE_SIN = ? )";
		
		return jdbcTemplate.queryForObject(sql, new Object[] {id, id, id}, new RowMapper<ReporteSiniestro>() {

			@Override
			public ReporteSiniestro mapRow(ResultSet rs, int rowNum) throws SQLException {
				ReporteSiniestro reporteSiniestro = new ReporteSiniestro();
				reporteSiniestro.setId(rs.getLong("ID"));
				reporteSiniestro.setNumeroReporte(rs.getLong("NUMEROREPORTE"));
				reporteSiniestro.setPoliza(rs.getString("POLIZA"));
				reporteSiniestro.setPolizaId(rs.getString("IDPOLIZA"));
				String strNumeroInciso = rs.getString("NUMEROINCISO");
				if (StringUtils.isNotBlank(strNumeroInciso)) {
					reporteSiniestro.setNumeroInciso(Integer.parseInt(strNumeroInciso));
					reporteSiniestro.setTipoInciso(rs.getString("TIPOINCISO"));
					reporteSiniestro.setDescripcionInciso(rs.getString("DESC_INCISO"));
					reporteSiniestro.setProcedeSiniestro(getProcedeSiniestro(rs.getString("IDPOLIZA"), rs.getTimestamp("FECHAOCURRIDO")));
				}
				reporteSiniestro.setPais(rs.getString("PAIS"));
				reporteSiniestro.setEstado(rs.getString("ESTADO"));
				reporteSiniestro.setCiudad(rs.getString("CIUDAD"));
				reporteSiniestro.setCalleNumero(rs.getString("CALLENUMERO"));
				reporteSiniestro.setColonia(rs.getString("COLONIA"));
				reporteSiniestro.setCodigoPostal(rs.getString("CODIGOPOSTAL"));
				reporteSiniestro.setReferencia(rs.getString("REFERENCIA"));
				reporteSiniestro.setKilometro(rs.getString("KILOMETRO"));
				String ubicacionLatitud = rs.getString("UBICACIONLAT");
				String ubicacionLongitud = rs.getString("UBICACIONLON");
				if (StringUtils.isNotBlank(ubicacionLatitud) && StringUtils.isNotBlank(ubicacionLongitud)) {
					reporteSiniestro.setUbicacionCoordenadas(new Coordenadas(Double.parseDouble(ubicacionLatitud), Double.parseDouble(ubicacionLongitud)));					
				}
				reporteSiniestro.setCiaRespId(rs.getInt("ID_CIA_ASEG_RESP"));
				reporteSiniestro.setVehiculosParticipantes(rs.getString("VEHICULOSPARTICIPANTES"));
				reporteSiniestro.setNombreReporta(rs.getString("NOMBREREPORTA"));
				reporteSiniestro.setNombreConductor(rs.getString("nombreConductor"));
				reporteSiniestro.setCausaSiniestro(rs.getString("CAUSASINIESTRO"));
				reporteSiniestro.setTelefono(rs.getString("TELEFONO"));
				reporteSiniestro.setFechaReporte(rs.getTimestamp("FECHAREPORTE"));
				reporteSiniestro.setFechaOcurrido(rs.getTimestamp("FECHAOCURRIDO"));
				reporteSiniestro.setFechaEnteradoAsignacion(rs.getTimestamp("FECHAENTERADOASIGNACION"));
				reporteSiniestro.setFechaContacto(rs.getTimestamp("FECHACONTACTO"));
				reporteSiniestro.setFechaCita(rs.getTimestamp("FECHACITA"));
				reporteSiniestro.setFechaAsignacion(rs.getTimestamp("FECHAASIGNACION"));
				if (reporteSiniestro.getFechaCita() != null) {
					reporteSiniestro.setTipo("CITA");
				} else {
					reporteSiniestro.setTipo("CRUCERO");
				}
				reporteSiniestro.setFechaTerminacion(rs.getTimestamp("FECHATERMINACION"));
				if (rs.getObject("AJUSTADORID") != null) {
					reporteSiniestro.setAjustador(getAjustador(rs.getLong("AJUSTADORID")));
				}
				reporteSiniestro.setTipoSiniestro(rs.getLong("TIPOSINIESTRO"));
				reporteSiniestro.setTerminoAjuste(rs.getString("TERMINOAJUSTE"));
				reporteSiniestro.setResponsabilidad(rs.getString("RESPONSABILIDAD"));
				reporteSiniestro.setDeclaracion(rs.getString("DECLARACION"));
				reporteSiniestro.setExpedienteFortimaxId(rs.getString("EXPEDIENTEFORTIMAXID"));
				BigDecimal numeroSiniestro = rs.getBigDecimal("NUMEROSINIESTRO");
				if (numeroSiniestro != null) {
					reporteSiniestro.setNumeroSiniestro(numeroSiniestro.longValue());					
				}
				
				reporteSiniestro.setLugarAjuste(rs.getString("LUGARAJUST"));
				if(rs.getObject("PARTICIPOEQPESADO") != null){
					reporteSiniestro.setParticipoUnidadEqPesado(rs.getBoolean("PARTICIPOEQPESADO"));
				}
				
				reporteSiniestro.setConductorMismoAsegurado(rs.getBoolean("CONDUCTORMISMOASEG"));
				reporteSiniestro.setConductor(rs.getString("CONDUCTOR"));
				reporteSiniestro.setEdad(rs.getInt("EDADCOND"));
				reporteSiniestro.setSexo(rs.getString("SEXOCOND"));
				reporteSiniestro.setCurp(rs.getString("CURPASEG"));
				reporteSiniestro.setRfc(rs.getString("RFCASEG"));
				reporteSiniestro.setTelefonoAseg(rs.getString("TELEFONOASEG"));
				reporteSiniestro.setEstadoNacimiento(rs.getString("ESTADOASEG"));
				
				return reporteSiniestro;
			}
		});
	}
	
	@Override
	public ReporteSiniestro getReporteSiniestroAsignado(Long id, String tipo) {
		Long reporteSiniestroId = null;
		if(tipo!=null && "N".equals(tipo)){
			reporteSiniestroId = getReporteSiniestroId(id);
		} else {
			reporteSiniestroId = id;
		}		
		
		ErrorBuilder eb = new ErrorBuilder();
		if (reporteSiniestroId == null) {
			throw new ApplicationException(eb.addFieldError("reporteSiniestroId", "requerido"));
		}		
		ReporteSiniestro reporteSiniestro = getReporteSiniestro(reporteSiniestroId);
		if (reporteSiniestro == null) {
			throw new ApplicationException(eb.addFieldError("reporteSiniestroId", "no existe"));			
		}
		validarReporteAsignadoAjustadorUsuarioActual(reporteSiniestro);
		confirmarEnteradoAsignacion(reporteSiniestro);
		return reporteSiniestro;
	}
	
	private Long getReporteSiniestroId(Long numeroReporte) {		
		AjustadorMovil ajustadorMovilUsuarioActual = usuarioService.getAjustadorMovilUsuarioActual();
		
		final String sql = "SELECT SEYCOS.APP_MOVILES_AJUSTADORES.getIdReporteSin(:numeroReporte,:ajustador) FROM DUAL";
		Map<String, Object> p = new HashMap<String, Object>();
		p.put("numeroReporte", numeroReporte);
		p.put("ajustador", ajustadorMovilUsuarioActual.getIdSeycos());
		return nJdbcTemplate.queryForObject(sql, p, Long.class);
	}

	
	@Override
	public void asignarInciso(IncisoParameter parameter, String usuario) {
		
		String[] split = parameter.getId().split(SEPARADOR, -1);
		Long idCotizacion = Long.parseLong(split[0]);
		Integer lineaNegocio = Integer.parseInt(split[1]);
		Integer inciso = Integer.parseInt(split[2]);
		
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pidReporteSin", parameter.getReporteSiniestroId())
			.addValue("pidCotizacion", idCotizacion)
			.addValue("pLinea", lineaNegocio)
			.addValue("pNumInciso", inciso)
			.addValue("pUsuario", usuario);
		
		Map<String, Object> execute = this.asignarInciso.execute(sqlParameter);
		String idCodResp = (String)execute.get("pid_Cod_Resp");
		if(!idCodResp.equals("0")){
			throw new ApplicationException((String)execute.get("pDesc_Codigo"));
			
		} 
					
	}
	public void enviarNotificacion(IncisoParameter parameter, String usuario){
		String[] split = parameter.getId().split(SEPARADOR, -1);
		Long idCotizacion = Long.parseLong(split[0]);
		Integer lineaNegocio = Integer.parseInt(split[1]);
		Integer inciso = Integer.parseInt(split[2]);
		try{
			MapSqlParameterSource sqlParameterMail = new MapSqlParameterSource();
			sqlParameterMail.addValue("pid_reporte", parameter.getReporteSiniestroId())
			.addValue("pid_cotizacion", idCotizacion)
			.addValue("pid_inciso", inciso)
			.addValue("pid_lin_negocio", lineaNegocio)
			.addValue("pDescripcionInc", null)
			.addValue("pDescripcionSin", null);
						
			Map<String, Object> executeMail = this.enviaMail.execute(sqlParameterMail);
			String idCodRespMail = (String)executeMail.get("pcode");
			if(!idCodRespMail.equals("0")){
				log.error("Error al notificar por email: "+(String)executeMail.get("pmssg"));
			}
		} catch(Exception e) {
			log.error("Falla notifica por correo: ", e);
		}

	}
	
	@Override
	public void guardaInfoGralReporte(ReporteSiniestroParameter parameter) {				
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pidReporteSin", parameter.getId())
			.addValue("pidCausaSin", convertToUTF8(parameter.getTipoSiniestro()))
			.addValue("pCveTermAjte", convertToUTF8(parameter.getTerminoAjuste()))
			.addValue("pCveTResp", convertToUTF8(parameter.getResponsabilidad()))
			.addValue("pDeclaracion", convertToUTF8(parameter.getDeclaracion()))	
			.addValue("pBMismoAsegu", parameter.isConductorMismoAsegurado()?1:0)
			.addValue("pNomConductor", convertToUTF8(parameter.getConductor()))
			.addValue("pCveSexoCond", convertToUTF8(parameter.getSexo()))
			.addValue("pEdadCond", parameter.getEdad())
			.addValue("pCveCurp", convertToUTF8(parameter.getCurp()))
			.addValue("pCveEstado", convertToUTF8(parameter.getEstadoNacimiento()))
			.addValue("pCveRfc", convertToUTF8(parameter.getRfc()))
			.addValue("pTelefono", convertToUTF8(parameter.getTelefonoAseg()))
			.addValue("pBEquipoPesado", parameter.isParticipoUnidadEqPesado()?1:0)
			.addValue("pCvelLugarAjust", convertToUTF8(parameter.getLugarAjuste()))
			.addValue("pIdCiaAsegResp", parameter.getCiaRespId());		
		Map<String, Object> execute = this.guardaInfoGralReporte.execute(sqlParameter);
		String idCodResp = (String)execute.get("pid_Cod_Resp");
		if(!idCodResp.equals("0")){
			throw new ApplicationException((String)execute.get("pDesc_Codigo"));
		}
	}

	@Override
	public void marcarContacto(MarcarContactoParameter parameter) {
		Map<String, Object> p = new HashMap<String, Object>();
		p.put("reporteSiniestroId", parameter.getReporteSiniestroId());
		Double latitud = null;
		Double longitud = null;
		if (parameter.getCoordenadas() != null) {
			latitud = parameter.getCoordenadas().getLatitud();
			longitud = parameter.getCoordenadas().getLongitud();
		}
		p.put("latitud", latitud);
		p.put("longitud", longitud);
		Integer servicioVigente = getServicioVigente(parameter.getReporteSiniestroId());
		p.put("idServicioSin", servicioVigente);
		nJdbcTemplate.update("update seycos.sin_servicio set fh_contacto = sysdate, contacto_latitud = :latitud, contacto_longitud = :longitud " +
				" where id_reporte_sin = :reporteSiniestroId " + 
				" and id_servicio_sin = :idServicioSin", p);		
	}
	
	@Override
	public void marcarTerminacion(MarcarTerminacionParameter parameter) {
		Map<String, Object> p = new HashMap<String, Object>();
		p.put("reporteSiniestroId", parameter.getReporteSiniestroId());
		Integer servicioVigente = getServicioVigente(parameter.getReporteSiniestroId());
		p.put("idServicioSin", servicioVigente);
		nJdbcTemplate.update("update seycos.sin_servicio set FH_TER_SERV = sysdate " +
				" where id_reporte_sin = :reporteSiniestroId " + 
				" and id_servicio_sin = :idServicioSin", p);			
	}
	
	private Integer getServicioVigente(Long reporteSiniestroId) {
		final String sql = "SELECT SEYCOS.APP_MOVILES_AJUSTADORES.getServicioVigente(:reporteSiniestroId) FROM DUAL";
		Map<String, Object> p = new HashMap<String, Object>();
		p.put("reporteSiniestroId", reporteSiniestroId);
		return nJdbcTemplate.queryForObject(sql, p, Integer.class);
	}

	@Override
	public Poliza getPoliza(String id, Date fechaOcurrido) {
		
		System.out.println("getPoliza:::inicializa");
		
		ErrorBuilder eb = new ErrorBuilder();
		if (fechaOcurrido == null) {			
			throw new ApplicationException(eb.addFieldError("fechaOcurrido", "requerido"));
		}
		System.out.println("getPoliza:::paso 2");
		String[] split = id.split(SEPARADOR, -1);
		Long idCotizacion = Long.parseLong(split[0]);
		Integer lineaNegocio = Integer.parseInt(split[1]);
		Integer inciso = Integer.parseInt(split[2]);		

		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter
			.addValue("pid_Cotizacion", idCotizacion)
			.addValue("pid_Lin_Negocio", lineaNegocio)
			.addValue("pid_Inciso", inciso)
			.addValue("pFecha_Ocurrido", fechaOcurrido);
		
		Map<String, Object> execute = getDatosPolizaProc.execute(parameter);
		System.out.println("getPoliza:::paso 3");
		@SuppressWarnings("unchecked")
		List<Poliza> polizas = (List<Poliza>) execute.get("pCursor");
		
		if (polizas.size() == 0) {
			return null;
		}
		
		if (polizas.size() > 1) {
			throw new RuntimeException("Se encontro más de una poliza");
		}
		System.out.println("getPoliza:::paso 4");
		Poliza poliza = polizas.get(0);
		poliza.setId(id);

		execute = getCoberturasAutoProc.execute(parameter);
		@SuppressWarnings("unchecked")
		List<Cobertura> coberturas = (List<Cobertura>) execute.get("pCursor");
		poliza.setCoberturas(coberturas);
		
		execute = getRecibosAutoProc.execute(parameter);
		@SuppressWarnings("unchecked")
		List<Recibo> recibos = (List<Recibo>) execute.get("pCursor");	
		poliza.setRecibos(recibos);	
		System.out.println("getPoliza:::paso 5");
		poliza.setAlertas(getAlertas(idCotizacion, poliza.getNumeroSerie()));
		System.out.println("getPoliza:::paso 6");
		return poliza;
	}	
	
	
	/**
	 * Obtiene las alertas de un numero de serie y las acciones por numero de cotizacion
	 * @param noCotizacion Numero de cotizacion
	 * @param noSerie 
	 * @author Luis Ibarra
	 * @since 03/Dic/2015
	 * @return Lista de alertas para el numero de serie
	 */
	private List<Alertas> getAlertas(long noCotizacion, String noSerie){
		
		System.out.println("ObteniendoAcciones getAlertas Movil:::");

		List<Alertas> alertas = new ArrayList<Alertas>();
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("vin", noSerie);
		param.put("idCotizacion", noCotizacion);
		param.put("tipo",1);
		List<SapAmisAccionesRelacion> relaciones = entidadService.findByProperties(SapAmisAccionesRelacion.class, param);
		System.out.println("ObteniendoAcciones getAlertas relaciones:::" + relaciones.size());
		for(int i = 0; i<relaciones.size(); i++){
			
			System.out.println("ObteniendoAcciones getAlertas for relaciones:::" + i);
			String sistema = relaciones.get(i).getCatAlertasSapAmis().getCatSistemas().getDescCatSistemas();
			
			System.out.println("ObteniendoAcciones getAlertas for relaciones:::" + sistema);
			if(sistema != null && (sistema.equals("PPT") || sistema.equals("OCRA"))) {
				Alertas alerta = new Alertas();
				
				System.out.println("ObteniendoAcciones getAlertas for relaciones:::" + relaciones.get(i).getCatAlertasSapAmis().getDescCatAlertasSapAmis());
				System.out.println("ObteniendoAcciones getAlertas for relaciones:::" + relaciones.get(i).getCatAlertasSapAmis().getCatSistemas().getDescCatSistemas());
				alerta.setAlerta(relaciones.get(i).getCatAlertasSapAmis().getDescCatAlertasSapAmis());
				alerta.setSistema(relaciones.get(i).getCatAlertasSapAmis().getCatSistemas().getDescCatSistemas());
				
				
				List<SapAmisAcciones> acciones = entidadService.findByProperty(SapAmisAcciones.class, "sapAmisAccionesRelacion", relaciones.get(i));
				
				List<Acciones> accionesList = new ArrayList<Acciones>();
				System.out.println("ObteniendoAcciones getAlertas  esta alerta tiene acciones :::" +acciones.size());
				for(int j=0; j<acciones.size();j++){
					System.out.println("ObteniendoAcciones getAlertas for acciones :::" +j);
					System.out.println("ObteniendoAcciones getAlertas for acciones :::" +acciones.get(0).getDescripcionAccion());
					Acciones accion = new Acciones();
					accion.setAccion(acciones.get(0).getDescripcionAccion());
					accionesList.add(accion);
				}
				alerta.setAcciones(accionesList);
				alertas.add(alerta);
			}
		}
		return alertas;
	}
	
	/**
	 * De acuerdo a los datos del reporte, obtiene el listado de coberturas, indicando cuales son afectables
	 * @param reporteSiniestroId
	 * @param id
	 * @param fechaOcurrido
	 * @return listado coberturas del reporte
	 */
	public List<Cobertura> getCoberturasAfecta(IncisoParameter parameter){
		
		String[] split = parameter.getId().split(SEPARADOR, -1);
		Long idCotizacion = Long.parseLong(split[0]);
		Integer lineaNegocio = Integer.parseInt(split[1]);
		Integer inciso = Integer.parseInt(split[2]);		
		
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter
			.addValue("pid_reporte_sin", parameter.getReporteSiniestroId())
			.addValue("pid_Cotizacion", idCotizacion)
			.addValue("pid_lin_negocio", lineaNegocio)
			.addValue("pid_inciso", inciso);
		
		Map<String, Object> execute = getCoberturasAfecta.execute(sqlParameter);
		@SuppressWarnings("unchecked")
		List<Cobertura> coberturas = (List<Cobertura>) execute.get("pCursor");
		
		return coberturas;
	}

	@Override
	public List<FindPolizaResponse> findPoliza(FindPolizaParameter parameter) {
		if (StringUtils.isBlank(parameter.getPoliza()) && StringUtils.isBlank(parameter.getNumeroSerie()) && StringUtils.isBlank(parameter.getPlaca()) & StringUtils.isBlank(parameter.getContratante())) {
			throw new ApplicationException("Se requiere al menos un dato.");
		}
		
		if (parameter.getFechaOcurrido() == null) {
			throw new ApplicationException("fechaOcurrido requerido.");
		}
		
		List<FindPolizaResponse> findPolizaResponseList = new ArrayList<FindPolizaResponse>();
		final String baseSql = "SELECT ${hint}\n" + 
				"           pi.id_lin_negocio,\n" + 
				"           pa.id_inciso,\n" + 
				"           pp.id_centro_emis,\n" +
				"			pp.id_cotizacion,\n" + 
				"           pp.num_poliza,\n" + 
				"           pp.num_renov_pol,\n" + 
				"           v1121.desc_valor sit_poliza_desc,\n" + 
				"           pp.mot_sit_cotiz,\n" +
				"			v1142.desc_valor mot_sit_cotiz_desc,\n" + 
				"           pi.f_ini_vigencia,\n" + 
				"           pi.f_fin_vigencia,\n" + 
				"           pp.nom_solicitante || DECODE(pp.and_or_name,NULL,' ','y/o'||TRIM(pp.and_or_name)) AS nombreContratante,\n" + 
				"           pa.id_modelo_auto,\n" + 
				"           pa.desc_vehic,\n" + 
				"           pa.num_serie,\n" + 
				"           pa.placa\n" + 
				"    FROM   SEYCOS.pol_poliza pp,\n" +
				"			SEYCOS.Pol_Auto pa,\n" + 
				"           SEYCOS.Pol_Inciso pi,\n" + 
				"           SEYCOS.ing_valor_campo v1121,\n" + 
				"           SEYCOS.ing_valor_campo v1142,\n" + 
				"           SEYCOS.ing_valor_campo v1617,\n" + 
				"           SEYCOS.ing_valor_campo v1618,\n" + 
				"           SEYCOS.dp_linea_neg ln\n" + 
				"   WHERE   v1121.id_campo = 1121\n" + 
				"           AND TRIM(v1121.val_campo) != 'CP'\n" + 
				"           AND v1142.id_campo = 1142\n" + 
				"           AND v1617.id_campo = 1617\n" + 
				"           AND v1618.id_campo = 1618\n" + 
				"           AND TRIM(pp.cve_t_docto_verp) <> 'SE'\n" + 
				"           AND TRIM(pp.mot_sit_cotiz) NOT IN ('COT', 'AUT')\n" + 
				"           AND pp.f_ter_reg = TO_DATE('31/12/4712','DD/MM/YYYY')\n" + 
				"           AND TRIM(pp.b_ult_reg_dia) = 'V'\n" + 
				"           AND pp.num_poliza >= 1\n" + 
				"           AND pp.sit_poliza = v1121.val_campo\n" + 
				"           AND pp.mot_sit_cotiz = v1142.val_campo\n" + 
				"           AND pi.sit_inciso_pol = v1617.val_campo\n" + 
				"           AND pi.mot_sit_inc_pol = v1618.val_campo\n" + 
				"           AND pa.id_cotizacion = pp.id_cotizacion\n" +
				"			AND pi.id_cotizacion = pa.id_cotizacion\n" + 
				"           AND pi.id_version_pol = pa.id_version_pol\n" +
				"			AND pi.id_inciso = pa.id_inciso\n" +
				"			AND pi.id_lin_negocio = pa.id_lin_negocio\n" +
				"           AND  pi.id_version_pol = (" +
				"					SELECT MIN(pinc.id_version_pol)\n" +
				"					FROM SEYCOS.pol_inciso pinc\n" +
				"					WHERE pinc.id_cotizacion = pi.id_cotizacion\n" +
                "                   AND pinc.id_inciso = pi.id_inciso\n" +
                "                   AND pinc.id_lin_negocio = pi.id_lin_negocio\n" +
                "                   AND pinc.f_ter_reg >=?\n" +
                "                   AND TRIM(pinc.cve_t_docto_verp) <> 'SE')\n" +
                "			AND ln.id_lin_negocio = pa.id_lin_negocio";
        	
		String orderBy = " ORDER BY pa.desc_vehic, pp.num_poliza, pp.num_renov_pol";
		StringBuilder sql = new StringBuilder(baseSql);
		ErrorBuilder errorBuilder = new ErrorBuilder();
		//Busqueda por numero de serie
		if (!StringUtils.isBlank(parameter.getNumeroSerie())) {
			final int minLength = 7;
			String numSerie = parameter.getNumeroSerie().trim().toUpperCase();
			if (numSerie.length() < minLength) {
				errorBuilder.addFieldError("numeroSerie", "Se requiere al menos " + minLength + " caracteres para el número de serie.");
				throw new ApplicationException(errorBuilder);
			}
			
			if (numSerie.length() == 17) {
				sql.append(" AND pa.num_serie = ?");
				sql = new StringBuilder(sql.toString().replaceAll(Pattern.quote("${hint}"), ""));
			} else {
				sql.append(" AND pa.num_serie like ?");
				sql = new StringBuilder(sql.toString().replaceAll(Pattern.quote("${hint}"), "/*+ LEADING(pa pi) USE_NL(pa pi) */"));
				numSerie = "%" + numSerie + "%";
			}
			sql.append(orderBy);
			findPolizaResponseList = jdbcTemplate.query(sql.toString(), new Object[] {parameter.getFechaOcurrido(),numSerie},  findPolizaResponseMapper);
		}else if (!StringUtils.isBlank(parameter.getPoliza())) { 	
			//Busqueda por poliza e inciso
			String[] polizaBloques = parameter.getPoliza().split(SEPARADOR, -1);
			if (polizaBloques.length != 3) {
				errorBuilder.addFieldError("poliza", "Formato Inválido");
				throw new ApplicationException(errorBuilder);
			}
			
			String numeroPolizaStr = polizaBloques[1];
			
			if (StringUtils.isBlank(numeroPolizaStr)) {
				errorBuilder.addFieldError("numeroPoliza", "requerido");
				throw new ApplicationException(errorBuilder);
			}
			
			try {
				Long numeroPoliza = Long.parseLong(numeroPolizaStr);
				if (numeroPoliza > 999999999999l) {
					errorBuilder.addFieldError("poliza", "Formato Inválido");
					throw new ApplicationException(errorBuilder);
				}
				sql.append(" AND PP.NUM_POLIZA = " + numeroPoliza);
				
			}catch(NumberFormatException e){
				errorBuilder.addFieldError("poliza", "Formato Inválido");
				throw new ApplicationException(errorBuilder);
			}
															
			String centroEmisorStr = polizaBloques[0];
			if (!StringUtils.isBlank(centroEmisorStr)) {
				try {
					Integer centroEmisor = Integer.parseInt(centroEmisorStr);
					if (centroEmisor > 999) {
						errorBuilder.addFieldError("poliza", "Formato Inválido");
						throw new ApplicationException(errorBuilder);
					}
					sql.append(" AND PP.ID_CENTRO_EMIS = " + centroEmisor);					
				} catch(NumberFormatException e){
					errorBuilder.addFieldError("poliza", "Formato Inválido");
					throw new ApplicationException(errorBuilder);
				}
			}
			
			String numeroRenovStr = polizaBloques[2];
			if (!StringUtils.isBlank(numeroRenovStr)) {
				try {
					Integer numeroRenov = Integer.parseInt(numeroRenovStr);
					if (numeroRenov > 99) {
						errorBuilder.addFieldError("poliza", "Formato Inválido");
						throw new ApplicationException(errorBuilder);
					}
					sql.append(" AND PP.NUM_RENOV_POL = " + numeroRenov);	
				}catch(NumberFormatException e){
					errorBuilder.addFieldError("poliza", "Formato Inválido");
					throw new ApplicationException(errorBuilder);
				}
			}
			
			if (parameter.getInciso() != null) {
				sql.append(" AND pi.id_inciso = " + parameter.getInciso());	
			}
			sql.append(orderBy);
			sql = new StringBuilder(sql.toString().replaceAll(Pattern.quote("${hint}"), ""));
			findPolizaResponseList = jdbcTemplate.query(sql.toString(), new Object[] {parameter.getFechaOcurrido()},  findPolizaResponseMapper);
		} else if (!StringUtils.isBlank(parameter.getPlaca())) {
			sql.append(" AND pa.placa like ?");
			sql.append(orderBy);
			sql = new StringBuilder(sql.toString().replaceAll(Pattern.quote("${hint}"), ""));
			findPolizaResponseList = jdbcTemplate.query(sql.toString(), new Object[] {parameter.getFechaOcurrido(), parameter.getPlaca().toUpperCase().trim() + "%" },  findPolizaResponseMapper);
		} else if (!StringUtils.isBlank(parameter.getContratante())) {
			sql.append(" AND pp.nom_solicitante || ' ' || pp.AND_OR_NAME like ?");
			sql.append(orderBy);
			sql = new StringBuilder(sql.toString().replaceAll(Pattern.quote("${hint}"), ""));
			findPolizaResponseList = jdbcTemplate.query(sql.toString(), new Object[] {parameter.getFechaOcurrido(),"%" + StringUtils.trim(parameter.getContratante()).toUpperCase() + "%"} ,  findPolizaResponseMapper);
		}
		
		for (FindPolizaResponse findPolizaResponse : findPolizaResponseList) {
			ProcedeSiniestro procedeSiniestro = getProcedeSiniestro(findPolizaResponse.getId(), parameter.getFechaOcurrido());
			findPolizaResponse.setProcedeSiniestro(procedeSiniestro);
		}
		
		return findPolizaResponseList;
	}

	@Override
	public ReporteSiniestrosAsignados getReporteSiniestrosAsignados(String ajustadorId) {
		SqlParameterSource parameter = new MapSqlParameterSource("pidAjustador", Long.parseLong(ajustadorId));
		Map<String, Object> execute = siniestrosEnAtencionFunc.execute(parameter);
		ReporteSiniestrosAsignados reporteSiniestrosAsignados = new ReporteSiniestrosAsignados();		
		@SuppressWarnings("unchecked")
		List<ReporteSiniestro> enAtencion = (List<ReporteSiniestro>) execute.get("return");
		reporteSiniestrosAsignados.setEnAtencion(enAtencion);
		reporteSiniestrosAsignados.setSinTerminoAjuste(new ArrayList<ReporteSiniestro>());
		return reporteSiniestrosAsignados;
	}
	
	@Override
	public List<ReporteSiniestro> getReporteSiniestrosAsignadosSinConfirmarEnterado(String ajustadorId) {
		List<ReporteSiniestro> reporteSiniestros = new ArrayList<ReporteSiniestro>();
		ReporteSiniestrosAsignados reporteSiniestrosAsignados = getReporteSiniestrosAsignados(ajustadorId);
		for (ReporteSiniestro reporteSiniestro : reporteSiniestrosAsignados.getEnAtencion()) {
			if (reporteSiniestro.getFechaEnteradoAsignacion() == null) {
				reporteSiniestros.add(reporteSiniestro);
			}
		}
		return reporteSiniestros;
	}
	

	@Override
	public Ajustador getAjustador(Long id) {
		String sql = "select ID_PREST_SERV ID, NOMBRE from SEYCOS.INS_PREST_SERV\r\n" + 
				"    inner join SEYCOS.PERSONA ON INS_PREST_SERV.ID_PERSONA = PERSONA.ID_PERSONA\r\n" + 
				"    WHERE ID_PREST_SERV = ?";
		return jdbcTemplate.queryForObject(sql, new Object[] {id}, new RowMapper<Ajustador>() {
			@Override
			public Ajustador mapRow(ResultSet rs, int rowNum) throws SQLException {
				Ajustador ajustador = new Ajustador();
				ajustador.setId(rs.getLong("ID"));
				ajustador.setNombre(rs.getString("NOMBRE"));
				return ajustador;
			}
		});
	}
	
	@Override
	public Ajustador getAjustador(Usuario usuario) {
		AjustadorMovil ajustadorMovilUsuario = usuarioService.getAjustadorMovilUsuario(usuario);
		if (ajustadorMovilUsuario == null) {
			return null;
		}
		return getAjustador(ajustadorMovilUsuario.getIdSeycos());
	}
	
	@Override
	public ProcedeSiniestro getProcedeSiniestro(String id, Date fechaOcurrido) {
		ErrorBuilder eb = new ErrorBuilder();
		if (fechaOcurrido == null) {			
			throw new ApplicationException(eb.addFieldError("fechaOcurrido", "requerido"));
		}
		String[] split = id.split(SEPARADOR, -1);
		ProcedeSiniestro procedeSiniestro = null;
		if(split[0].length()>0 && split[1].length()>0 && split[2].length()>0){
			Long idCotizacion = Long.parseLong(split[0]);
			Integer lineaNegocio = Integer.parseInt(split[1]);
			Integer inciso = Integer.parseInt(split[2]);		

			MapSqlParameterSource parameter = new MapSqlParameterSource();
			parameter
				.addValue("pid_Cotizacion", idCotizacion)
				.addValue("pid_Lin_Negocio", lineaNegocio)
				.addValue("pid_Inciso", inciso)
				.addValue("pFecha_Ocurrido", fechaOcurrido);
			
			Map<String, Object> execute = isProcedeProc.execute(parameter);
			@SuppressWarnings("unchecked")
			List<ProcedeSiniestro> procedeSiniestros = (List<ProcedeSiniestro>) execute.get("pCursor");
			
			if (procedeSiniestros.size() == 0) {
				return null;
			}
			
			if (procedeSiniestros.size() > 1) {
				throw new RuntimeException("Se encontro más de una poliza");
			}	
			
			procedeSiniestro = procedeSiniestros.get(0);
		}
		return procedeSiniestro;
	}

	@Override
	public AjustadorMovil getAjustadorMovil(Long idSeycos) {
		String jpql = "select m from AjustadorMovil m where m.idSeycos = :idSeycos";
		TypedQuery<AjustadorMovil> q = em.createQuery(jpql, AjustadorMovil.class);
		q.setParameter("idSeycos", idSeycos);
		List<AjustadorMovil> list = q.getResultList();
		if (list.size() == 1) {
			return list.get(0);
		}
		if (list.size() == 0) {
			return null;
		}
		throw new RuntimeException("Más de un resultado");
	}
	
	@Override
	public List<AjustadorMovil> getAjustadoresMoviles() {
		String jpql = "select m from AjustadorMovil m";
		TypedQuery<AjustadorMovil> q = em.createQuery(jpql, AjustadorMovil.class);
		return q.getResultList();
	}
	
	
	public List<String> getRegistrationIds(Long idSeycos) {
		AjustadorMovil ajustadorMovil = getAjustadorMovil(idSeycos);
		if (ajustadorMovil == null) {
			return new ArrayList<String>();
		}
		return usuarioService.getRegistrationIds(ajustadorMovil.getCodigoUsuario(), sistemaContext.getAjustadorMovilDeviceApplicationId());
	}

	@Override
	public Date confirmarEnteradoAsignacion(Long reporteSiniestroId) {
		ErrorBuilder eb = new ErrorBuilder();
		if (reporteSiniestroId == null) {
			throw new ApplicationException(eb.addFieldError("reporteSiniestroId", "requerido"));
		}
		
		ReporteSiniestro reporteSiniestro = getReporteSiniestro(reporteSiniestroId);
		
		if (reporteSiniestro == null) {
			throw new ApplicationException(eb.addFieldError("reporteSiniestroId", "no existe"));			
		}

		
		validarReporteAsignadoAjustadorUsuarioActual(reporteSiniestro);
		
		return confirmarEnteradoAsignacion(reporteSiniestro);		
	}
	
	/**
	 * Valida que el reporte este asignado al ajustador del usuario actual.
	 * @param reporteSiniestro
	 * @throws ApplicationException cuando el ajustador del usuario actual es diferente al ajustador asignado al reporte.
	 */
	private void validarReporteAsignadoAjustadorUsuarioActual(ReporteSiniestro reporteSiniestro) {
		if (!isReporteAsignadoAjustadorUsuarioActual(reporteSiniestro)) {
			throw new ApplicationException("No se pudo confirmar de enterado porque el reporte esta asignado a otro ajustador.");			
		}		
	}
	
	private boolean isReporteAsignadoAjustadorUsuarioActual(ReporteSiniestro reporteSiniestro) {
		AjustadorMovil ajustadorMovilUsuarioActual = usuarioService.getAjustadorMovilUsuarioActual();
		return reporteSiniestro.getAjustador().getId().equals(ajustadorMovilUsuarioActual.getIdSeycos());
	}
	
	private Date confirmarEnteradoAsignacion(ReporteSiniestro reporteSiniestro) {
		if (reporteSiniestro.getFechaEnteradoAsignacion() != null) {
			//Ya se había marcado la fecha, no se lanza excepcion sin embargo no se actualiza puesto que es inmutable.
			return reporteSiniestro.getFechaEnteradoAsignacion();
		}
		
		Date fechaEnteradoAsignacion = new Date(oracleCurrentDateService.getCurrentDate().getMillis());
		Map<String, Object> p = new HashMap<String, Object>();
		p.put("reporteSiniestroId", reporteSiniestro.getId());
		p.put("fechaEnteradoAsignacion", fechaEnteradoAsignacion);
		Integer servicioVigente = getServicioVigente(reporteSiniestro.getId());
		p.put("idServicioSin", servicioVigente);
		nJdbcTemplate.update("update seycos.sin_servicio set FH_ENTERADO_ASIGN = :fechaEnteradoAsignacion " +
				" where id_reporte_sin = :reporteSiniestroId and id_servicio_sin = :idServicioSin", p);
		return fechaEnteradoAsignacion;		
	}
	
	/**
	 * Obtiene el catalogo de tipo de siniestro (causa del siniestro), aplicable de acuerdo al tipo de inciso
	 * @param tipoInciso MOTOS o AUTOS o CAMN etc...
	 * @return catalogo de tipos de siniestro validos
	 * @author ncorrea
	 */
	@SuppressWarnings("unchecked")
	public List<Catalogo> getTiposSiniestro(String tipoInciso){		
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("pcve_t_bien", tipoInciso);		
		Map<String, Object> execute = getTiposSiniestro.execute(parameter);		
		return (List<Catalogo>) execute.get("pCursor");
	}
	
	/**
	 * Obtiene el catalogo de terminos de ajuste aplicable al tipo de siniestro indicado
	 * @param tipoSiniestro
	 * @param responsabilidad
	 * @return catalogo de terminos de ajuste validos
	 * @author ncorrea
	 */
	@SuppressWarnings("unchecked")
	public List<Catalogo> getTerminosAjuste(String tipoSiniestro, String responsabilidad){		
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("pid_causa_sin", tipoSiniestro);	
		parameter.addValue("pCvel_T_Resp", responsabilidad);
		Map<String, Object> execute = getTerminosAjuste.execute(parameter);
		return (List<Catalogo>) execute.get("pCursor");
	}
	
	/**
	 * Obtiene el catalogo de terminos de ajuste aplicable al tipo de siniestro indicado
	 * @param tipoSiniestro
	 * @param terminoAjuste
	 * @return catalogo de terminos de ajuste validos
	 * @author ncorrea
	 */
	@SuppressWarnings("unchecked")
	public List<Catalogo> getResponsabilidad(String tipoSiniestro, String terminoAjuste){		
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("pid_causa_sin", tipoSiniestro);		
		parameter.addValue("pCve_Term_Ajust", terminoAjuste);
		Map<String, Object> execute = getResponsabilidad.execute(parameter);
		return (List<Catalogo>) execute.get("pCursor");
	}
	
	/**
	 * Catalogos Simples 
	 * @param tipo
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Catalogo> getCatalogoSimple(String tipo){
		MapSqlParameterSource parameter = new MapSqlParameterSource();
		parameter.addValue("P_CATALOG_TYPE", tipo);
		Map<String, Object> execute = getCatalogoSimple.execute(parameter);
		return (List<Catalogo>) execute.get("pCursor");
	}
	
	/**
	 * Municipios
	 * @param idEstado
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Catalogo> getMunicipios(DireccionParameter parameter){
		MapSqlParameterSource mapSqlParam = new MapSqlParameterSource();
		mapSqlParam.addValue("pid_estado", parameter.getEstadoId());
		Map<String, Object> execute = getMunicipios.execute(mapSqlParam);
		return (List<Catalogo>) execute.get("pCursor");
	}
	
	/**
	 * Informacion en Base al CP
	 * @param idEstado
	 * @param idMunicipio
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Catalogo> getInfoCP(DireccionParameter parameter){
		MapSqlParameterSource mapSqlParam = new MapSqlParameterSource();
		mapSqlParam.addValue("pcodigo_p", parameter.getCodigoPostal());
		Map<String, Object> execute = getInfoCP.execute(mapSqlParam);
		return (List<Catalogo>) execute.get("pCursor");
	}
	
	/**
	 * Consulta el listado de terceros de una cobertura
	 * @param parameter
	 * @return
	 * @author ncorrea
	 */
	@SuppressWarnings("unchecked")
	public List<Tercero> getTerceros(CoberturaIncisoParameter parameter){
		
		String[] split = parameter.getPolizaId().split(SEPARADOR, -1);
		Integer lineaNegocio = Integer.parseInt(split[1]);
		Integer inciso = Integer.parseInt(split[2]);		

		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter
			.addValue("pid_reporte_sin", parameter.getReporteSiniestroId())
			.addValue("pid_lin_negocio", lineaNegocio)
			.addValue("pid_inciso", inciso)
			.addValue("pid_cobertura", parameter.getCoberturaId());
		
		Map<String, Object> execute = this.getTerceros.execute(sqlParameter);
		
		String idCodResp = (String)execute.get("pId_Cod_Resp");
		if(!idCodResp.equals("0")){
			throw new ApplicationException((String)execute.get("pDesc_Codigo"));
		}
		return (List<Tercero>) execute.get("pCursor");
	}
	
	/**
	 * Consulta la informacion de un tercero
	 * @param parameter
	 * @return
	 * @author ncorrea
	 */
	public Object getTercero(TerceroParameter parameter){
		Object result = null;
		String[] split = parameter.getPolizaId().split(SEPARADOR, -1);
		Long idCotizacion = Long.parseLong(split[0]);
		Integer lineaNegocio = Integer.parseInt(split[1]);
		Integer inciso = Integer.parseInt(split[2]);
		
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter
			.addValue("pidCotizacion", idCotizacion)
			.addValue("pid_reporte_sin", parameter.getReporteSiniestroId())
			.addValue("pid_tercero", parameter.getId())
			.addValue("pid_lin_negocio", lineaNegocio)
			.addValue("pid_inciso", inciso)
			.addValue("pid_cobertura", parameter.getCoberturaId());
		
		Map<String, Object> execute;
		if(parameter.getCoberturaId().equals(ConfigCobertura.DM.value) || 
				parameter.getCoberturaId().equals(ConfigCobertura.RCV.value) ||
				parameter.getCoberturaId().equals(ConfigCobertura.EE.value) ||
				parameter.getCoberturaId().equals(ConfigCobertura.AYC.value)){
			execute = this.getTerceroVehiculo.execute(sqlParameter);
		} else if(parameter.getCoberturaId().equals(ConfigCobertura.GM.value) 
				|| parameter.getCoberturaId().equals(ConfigCobertura.RCP.value)){
			execute = this.getTerceroPersona.execute(sqlParameter);
		} else if(parameter.getCoberturaId().equals(ConfigCobertura.RCB.value)){
			execute = this.getTerceroBien.execute(sqlParameter);
		} else {
			execute = this.getTercero.execute(sqlParameter);
		}
		
		String idCodResp = (String)execute.get("pId_Cod_Resp");
		if(!idCodResp.equals("0")){
			throw new ApplicationException((String)execute.get("pDesc_Codigo"));
		}
		result = (Object) execute.get("pCursor");
	
		return result;
		
	}
	
	/**
	 * Guarda Estimacion de Reserva
	 * @param tercero
	 * @param claveUsuario
	 */
	public BigDecimal afectaReserva(Object tercero, String claveUsuario){
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		Map<String, Object> execute = null;
		if(tercero instanceof Persona){
			Persona t = (Persona)tercero;
			sqlParameter
			.addValue("pId_Reporte_sin", t.getReporteSiniestroId())
			.addValue("pId_Tercero", t.getId())
			.addValue("pCveTipoPase", convertToUTF8(t.getTipoPase()))
			.addValue("pCausaMovimiento", convertToUTF8(t.getCausaMovimiento()))
			.addValue("pIdCiaAsegTer",t.getCiaId())
			.addValue("pIdSinCiaTer", convertToUTF8(t.getSiniestroCia()))
			.addValue("pNomTercero", convertToUTF8(t.getNombre()))
			.addValue("pNomContacto", convertToUTF8(t.getContacto()))
			.addValue("ptelef1",convertToUTF8( t.getTelefono()))
			.addValue("pCorreo", convertToUTF8(t.getCorreo()))
			.addValue("pimp_Estimado", t.getEstimacion())
			.addValue("pTipoDano", t.isHomicidio()?1:0)
			.addValue("pidHospital", convertToUTF8(t.getHospitalId()))
			.addValue("pidMedico", convertToUTF8(t.getMedicoId()))
			.addValue("pEdad", t.getEdad())
			.addValue("pLesiones", convertToUTF8(t.getLesiones()))
			.addValue("pLimiteAtencion", t.getLimiteAtencion())
			.addValue("pcve_usuario", convertToUTF8(claveUsuario))
			.addValue("pObservaciones", convertToUTF8(t.getObservaciones()));
			if(t.getCoberturaId().equals(ConfigCobertura.GM.value)){
				sqlParameter.addValue("pid_cobertura", t.getCoberturaId())
				.addValue("precibePaseCiaTerc", t.getRecibeOrdenCia());
				execute = this.afectaReservaGM.execute(sqlParameter);
			} else {
				sqlParameter.addValue("pid_cobertura", ConfigCobertura.RCP.value);
				execute = this.afectaReservaRCP.execute(sqlParameter);
			}			
		} else if(tercero instanceof Vehiculo){
			Vehiculo t = (Vehiculo)tercero;
			if(t.getCoberturaId().equals(ConfigCobertura.DM.value)){
				sqlParameter
				.addValue("pId_Reporte_sin", t.getReporteSiniestroId())
				.addValue("pId_Tercero", t.getId())
				.addValue("pid_cobertura", t.getCoberturaId())
				.addValue("pnomInteresado", convertToUTF8(t.getNombre()))
				.addValue("ptelef1", convertToUTF8(t.getTelefono()))
				.addValue("pCorreo", convertToUTF8(t.getCorreo()))
				.addValue("pIdCiaAsegTer", t.getCiaId())
				.addValue("pIdSinCiaTer", convertToUTF8(t.getSiniestroCia()))
				.addValue("precibePaseCiaTerc", t.getRecibeOrdenCia())
				.addValue("pCausaMovimiento", convertToUTF8(t.getCausaMovimiento()))
				.addValue("pCveTipoPase", convertToUTF8(t.getTipoPase()))
				.addValue("pTipoPerdida", t.isPerdidaTotal()?1:0)
				.addValue("pAplicaDeducible", t.getAplicaDeducible())
				.addValue("pidTaller", convertToUTF8(t.getTallerId()))
				.addValue("pAreasDan", convertToUTF8(t.getAreasDanadas()))
				.addValue("pDanPreexistentes", convertToUTF8(t.getDanosPreexistentes()))
				.addValue("pValorComercial", t.getSumaAmparada())
				.addValue("pimp_Estimado", t.getEstimacion())
				.addValue("pcve_usuario", convertToUTF8(claveUsuario))
				.addValue("pObservaciones", convertToUTF8(t.getObservaciones()))
				.addValue("pPlaca", convertToUTF8(t.getPlaca()));
				execute = this.afectaReservaDM.execute(sqlParameter);
			}else if(t.getCoberturaId().equals(ConfigCobertura.EE.value) 
					|| t.getCoberturaId().equals(ConfigCobertura.AYC.value)){
				sqlParameter
				.addValue("pId_Reporte_sin", t.getReporteSiniestroId())
				.addValue("pId_Tercero", t.getId())
				.addValue("pid_cobertura", t.getCoberturaId())
				.addValue("pnomInteresado", convertToUTF8(t.getNombre()))
				.addValue("ptelef1", convertToUTF8(t.getTelefono()))
				.addValue("pCorreo", convertToUTF8(t.getCorreo()))
				.addValue("pIdCiaAsegTer", t.getCiaId())
				.addValue("pIdSinCiaTer", convertToUTF8(t.getSiniestroCia()))
				.addValue("precibePaseCiaTerc", t.getRecibeOrdenCia())
				.addValue("pCausaMovimiento", convertToUTF8(t.getCausaMovimiento()))
				.addValue("pCveTipoPase", convertToUTF8(t.getTipoPase()))
				.addValue("pTipoPerdida", t.isPerdidaTotal()?1:0)
				.addValue("pAplicaDeducible", t.getAplicaDeducible())
				.addValue("pidTaller", convertToUTF8(t.getTallerId()))
				.addValue("pAreasDan", convertToUTF8(t.getAreasDanadas()))
				.addValue("pDanPreexistentes", convertToUTF8(t.getDanosPreexistentes()))
				.addValue("pValorComercial", t.getSumaAmparada())
				.addValue("pimp_Estimado", t.getEstimacion())
				.addValue("pcve_usuario", convertToUTF8(claveUsuario))
				.addValue("pDescripcionTercero", convertToUTF8(t.getDescripcion()))
				.addValue("pObservaciones", convertToUTF8(t.getObservaciones()))
				.addValue("pPlaca", convertToUTF8(t.getPlaca()));
				execute = this.afectaReservaCyAoEE.execute(sqlParameter);
			} else {
				sqlParameter
				.addValue("pId_Reporte_sin", t.getReporteSiniestroId())
				.addValue("pId_Tercero", t.getId())
				.addValue("pid_cobertura", ConfigCobertura.RCV.value)
				.addValue("pCveTipoPase", convertToUTF8(t.getTipoPase()))
				.addValue("pCausaMovimiento", convertToUTF8(t.getCausaMovimiento()))
				.addValue("pTipoPerdida", t.isPerdidaTotal()?1:0)
				.addValue("pNomTercero", convertToUTF8(t.getNombre()))
				.addValue("pNomContacto", convertToUTF8(t.getContacto()))
				.addValue("ptelef1", convertToUTF8(t.getTelefono()))
				.addValue("pCorreo", convertToUTF8(t.getCorreo()))
				.addValue("pcveLlegoCiaTer", t.getLlegoCia())
				.addValue("pIdCiaAsegTer", t.getCiaId())
				.addValue("pIdSinCiaTer", convertToUTF8(t.getSiniestroCia()))
				.addValue("pidTaller",convertToUTF8( t.getTallerId()))
				.addValue("pimp_Estimado", t.getEstimacion())
				.addValue("pidMarcaVehTer", convertToUTF8(t.getMarcaId()))
				.addValue("pModeloVehTer", t.getModelo())
				.addValue("pDescTipoEstiloVehTer", convertToUTF8(t.getDescripcionTipoEstilo()))
				.addValue("pNumSerieVehTer", convertToUTF8(t.getSerie()))
				.addValue("pPlacaVehTer", convertToUTF8(t.getPlaca()))
				.addValue("pColorVehTer", convertToUTF8(t.getColor()))
				.addValue("pAreasDan", convertToUTF8(t.getAreasDanadas()))
				.addValue("pDanPreex", convertToUTF8(t.getDanosPreexistentes()))
				.addValue("pcve_usuario", convertToUTF8(claveUsuario))
				.addValue("pObservaciones", convertToUTF8(t.getObservaciones()));
				execute = this.afectaReservaRCV.execute(sqlParameter);
			}
		} else if(tercero instanceof Bien){
			Bien t = (Bien)tercero;
			sqlParameter
				.addValue("pId_Reporte_sin", t.getReporteSiniestroId())
				.addValue("pId_Tercero", t.getId())
				.addValue("pid_cobertura", ConfigCobertura.RCB.value)
				.addValue("pCveTipoPase", t.getTipoPase())
				.addValue("pCausaMovimiento", t.getCausaMovimiento())
				.addValue("pTipoPerdida", t.isPerdidaTotal()?1:0)
				.addValue("pTipoBien", convertToUTF8(t.getTipoBien()))
				.addValue("pNomTercero", convertToUTF8(t.getNombre()))
				.addValue("pNomContacto", convertToUTF8(t.getContacto()))
				.addValue("ptelef1", convertToUTF8(t.getTelefono()))
				.addValue("pCorreo", convertToUTF8(t.getCorreo()))
				.addValue("pimp_Estimado", t.getEstimacion())
				.addValue("pcve_pais", convertToUTF8(t.getPaisId()))
				.addValue("pcve_estado", convertToUTF8(t.getEstadoId()))
				.addValue("pcve_ciudad", convertToUTF8(t.getCiudadId()))
				.addValue("pcalleynum", convertToUTF8(t.getCalleNumero()))
				.addValue("pcodigoPostal", convertToUTF8(t.getCodigoPostal()))
				.addValue("pColonia", convertToUTF8(t.getColonia()))
				.addValue("pReferencias", convertToUTF8(t.getReferencia()))
				.addValue("pkm", convertToUTF8(t.getKm()))
				.addValue("pNomIngeniero", convertToUTF8(t.getIngeniero()))
				.addValue("pAreasDan", convertToUTF8(t.getAreasDanadas()))
				.addValue("pcve_usuario", convertToUTF8(claveUsuario))
				.addValue("pObservaciones", convertToUTF8(t.getObservaciones()));
			execute = this.afectaReservaRCB.execute(sqlParameter);
		} else {
			Tercero t = (Tercero)tercero;
			sqlParameter
			.addValue("pId_Reporte_sin", t.getReporteSiniestroId())
			.addValue("pId_Tercero", t.getId())
			.addValue("pid_cobertura", t.getCoberturaId())
			.addValue("pCausaMovimiento", convertToUTF8(t.getCausaMovimiento()))
			.addValue("pimp_Estimado", t.getEstimacion())
			.addValue("pcve_usuario", convertToUTF8(claveUsuario));
			if(t.getCoberturaId().equals(ConfigCobertura.RT.value)){
				sqlParameter.addValue("pimp_SumaAsegurada", t.getSumaAmparada());
				execute = this.afectaReservaRT.execute(sqlParameter);
			} else if (t.getCoberturaId().equals(ConfigCobertura.EXM.value)){
				execute = this.afectaReservaEXM.execute(sqlParameter);
			} else {
				throw new ApplicationException("Datos Invalidos!");
			}
			
		}
		
		String idCodResp = (String)execute.get("pId_Cod_Resp");
		if(!idCodResp.equals("0")){
			throw new ApplicationException((String)execute.get("pDesc_Codigo"));
		}
		
		return (BigDecimal)execute.get("pId_Tercero");
	}
	
	/**
	 * Lista de Tipos de Pases Medicos que aplican por cobertura
	 * @param parameter
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public List<Catalogo> getTiposPases(CoberturaIncisoParameter parameter){
		
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter.addValue("pid_cobertura", parameter.getCoberturaId());
		Map<String, Object> execute = this.getTiposPases.execute(sqlParameter);
		
		return (List<Catalogo>) execute.get("pCursor");
	}
	
	private List<Tercero> getTerceros(Long id){
		String sql = " Select Upper(St.Nom_Tercero) nombre, St.telef_1, C2930.Desc_Valor tipopase, \r\n"+ 
				" Nvl(Scrd.Imp_Reserva,Scr.Imp_Reserva) estimacion, \r\n"+ 
				"upper(nvl(categoria.nom_cobertura, cobertura.nom_cobertura)) as cobertura, \r\n"+
				"upper (marca.nom_marca || '' || St.id_estilo)  marcaVehiculoTercero, \r\n" +
				"upper (St.desc_tipo_estilo) tipoVehiculoTercero"+ 
      " From Seycos.Sin_Reclamacion Reclamacion  \r\n"+ 
      " Inner Join Seycos.Sin_Inciso_Recl On (Reclamacion.Id_Reporte_Sin = Sin_Inciso_Recl.Id_Reporte_Sin AND Reclamacion.ID_COTIZACION=Sin_Inciso_Recl.ID_COTIZACION) \r\n"+  
      " Inner Join SEYCOS.Sin_Cobert_Recl Scr  On (Scr.Id_Reporte_Sin = Reclamacion.Id_Reporte_Sin And Scr.Id_Lin_Negocio = SIN_INCISO_RECL.ID_LIN_NEGOCIO And Scr.Id_Inciso = SIN_INCISO_RECL.ID_INCISO ) \r\n"+ 
      " Inner Join Seycos.Sin_Cobert_Recl_D Scrd On (Scrd.Id_Reporte_Sin=Scr.Id_Reporte_Sin And Scrd.Id_Version_Recl=Scr.Id_Version_Recl  \r\n"+ 
      " And Scrd.Id_Lin_Negocio=Scr.Id_Lin_Negocio And Scrd.Id_Inciso=Scr.Id_Inciso And Scrd.Id_Cobertura=Scr.Id_Cobertura )   \r\n"+ 
      " Inner Join Seycos.Sin_Tercero St On (St.Id_Reporte_Sin = Reclamacion.Id_Reporte_Sin  And St.Id_Tercero = Scrd.Id_Tercero) \r\n"+ 
      " Inner Join Seycos.Ing_Valor_Campo C2930 On (C2930.Id_Campo = 2930  And St.Cve_Tipo_Pase = C2930.Val_Campo  ) \r\n"+ 
      " Inner Join Seycos.Dp_Cobertura Cobertura On (Cobertura.Id_Cobertura = St.Id_Cobertura) \r\n"+ 
      " left join seycos.DP_SIN_COBERTURA categoria on (categoria.id_cobertura=Scrd.Id_Categoria) \r\n"+
      " left join seycos.ing_marca marca on (marca.id_marca = St.id_marca) \r\n"+
      " Where Reclamacion.Id_Reporte_Sin = ?  \r\n"+ 
      " And Reclamacion.F_Ter_Reg = Date '4712-12-31' \r\n"+ 
      " And Scrd.F_Ter_Reg = Date '4712-12-31' \r\n"+ 
      " And Sin_Inciso_Recl.F_Ter_Reg = Date '4712-12-31' \r\n"+ 
      " And st.F_Ter_Reg = Date '4712-12-31'";
		
		return (List<Tercero>)jdbcTemplate.query(sql, new Object[] {id}, new RowMapper<Tercero>() {
			
			@Override
			public Tercero mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				Tercero tercero = new Tercero();
				tercero.setNombre(rs.getString("nombre"));
				tercero.setTipoPase(rs.getString("tipopase"));
				tercero.setCobertura(rs.getString("cobertura"));
				tercero.setEstimacion(rs.getBigDecimal("estimacion"));
				tercero.setTelefono(rs.getString("telef_1"));
				tercero.setMarca(rs.getString("marcaVehiculoTercero"));
				tercero.setTipo(rs.getString("tipoVehiculoTercero"));
				return tercero;
			}
		});
	}

	private List<DeclaracionSiniestro> getPaseDeclaracionSiniestro(Long id) {
		
		String sql = "SELECT RECLAMACION.ID_REPORTE_SIN, RECLAMACION.FH_OCURRIDO FSINIESTRO, \r\n"+ 
			" RECLAMACION.FH_REPORTADO FATENCION,  \r\n"+
			" RECLAMACION.NUM_REP_CABINA NUMEROREPORTE, \r\n"+
			" RECLAMACION.NUM_RECLAMACION NUMEROSINIESTRO, \r\n"+
			" case when RECLAMACION.ID_COTIZACION is not null then LPAD(TO_CHAR(POLIZA.ID_CENTRO_EMIS),3,0) || '-' ||LPAD(TO_CHAR(POLIZA.NUM_POLIZA),12,0) || '-' || LPAD(TO_CHAR(POLIZA.NUM_RENOV_POL),2,0) end POLIZA, \r\n"+
			" AUTO.ID_INCISO INCISO, \r\n"+ 
			" UPPER(PORTAL.GET_CLIENT_NAME (RECLAMACION.ID_COTIZACION,RECLAMACION.ID_VERSION_POL,AUTO.ID_LIN_NEGOCIO,AUTO.ID_INCISO)) NOMBREASEGURADO, \r\n"+
			" NVL(UPPER(AUTO.NOM_CONDUCTOR), RECLAMACION.TXT_CONDUCTOR) NOMBRECONDUCTOR, \r\n"+ 
			" UPPER(MARCA.NOM_MARCA || ' ' || DECODE(TRIM(MARCA.CVEL_T_BIEN), 'CAMN', 'CAMIONES', MARCA.CVEL_T_BIEN)) MARCAVEHICULOASEGURADO, \r\n"+
			" UPPER(NVL(ASEG.desc_tipo_estilo,VEHICLE.desc_tipo_estilo)) TIPOVEHICULOASEGURADO, \r\n"+
			" NVL(ASEG.ID_MODELO_AUTO,VEHICLE.ID_MODELO_AUTO) MODELOVEHICULOASEGURADO, \r\n"+
			" UPPER(NVL(ASEG.PLACA,VEHICLE.PLACA)) PLACASVEHICULOASEGURADO, \r\n"+
			" UPPER(NVL(ASEG.NUM_SERIE,VEHICLE.NUM_SERIE)) NUMSERIEVEHICULOASEGURADO, \r\n"+
			" UPPER(DECLARACION.TX_SINIESTRO) DECLARACIONSINIESTRO, \r\n"+
			" TERMINOAJUSTE.DESC_VALOR TERMINOAJUSTE, \r\n"+
			" RESPONSABILIDAD.DESC_VALOR RESPONSABILIDAD, \r\n"+
			" DECODE(POLIZA.SIT_POLIZA, 'NV', 'POLIZA NO VIGENTE', 'POLIZA VIGENTE') ESTATUSPOLIZA \r\n"+
			" FROM SEYCOS.SIN_RECLAMACION RECLAMACION \r\n"+
			" LEFT JOIN SEYCOS.POL_POLIZA POLIZA ON (POLIZA.ID_COTIZACION = RECLAMACION.ID_COTIZACION AND POLIZA.ID_VERSION_POL = RECLAMACION.ID_VERSION_POL) \r\n"+
			" LEFT JOIN SEYCOS.SIN_INCISO_RECL ON (RECLAMACION.ID_REPORTE_SIN = SIN_INCISO_RECL.ID_REPORTE_SIN AND RECLAMACION.ID_COTIZACION=SIN_INCISO_RECL.ID_COTIZACION AND SIN_INCISO_RECL.F_TER_REG = DATE '4712-12-31') \r\n"+
			" LEFT JOIN SEYCOS.SIN_AUTO AUTO ON (AUTO.ID_REPORTE_SIN = RECLAMACION.ID_REPORTE_SIN AND AUTO.ID_LIN_NEGOCIO = SIN_INCISO_RECL.ID_LIN_NEGOCIO AND AUTO.ID_INCISO=SIN_INCISO_RECL.ID_INCISO) \r\n"+
			" LEFT JOIN SEYCOS.POL_AUTO VEHICLE ON (RECLAMACION.ID_COTIZACION = VEHICLE.ID_COTIZACION AND VEHICLE.ID_LIN_NEGOCIO = SIN_INCISO_RECL.ID_LIN_NEGOCIO AND VEHICLE.ID_INCISO = SIN_INCISO_RECL.ID_INCISO) \r\n"+
			" LEFT JOIN SEYCOS.ING_MARCA MARCA ON (MARCA.ID_MARCA = VEHICLE.ID_MARCA) \r\n"+
			" LEFT JOIN SEYCOS.SIN_TEXTO DECLARACION ON (RECLAMACION.ID_TEXTO_SIN = DECLARACION.ID_TEXTO_SIN) \r\n"+
			" LEFT JOIN SEYCOS.ING_VALOR_CAMPO TERMINOAJUSTE on (TERMINOAJUSTE.val_campo=auto.CVE_TERM_AJUST and TERMINOAJUSTE.id_campo=2832) \r\n"+
			" LEFT JOIN SEYCOS.ING_VALOR_CAMPO RESPONSABILIDAD on (RESPONSABILIDAD.val_campo=auto.CVEL_T_RESP and RESPONSABILIDAD.id_campo=2834) \r\n"+   
			" LEFT JOIN SEYCOS.SIN_TERCERO ASEG ON (ASEG.ID_REPORTE_SIN=RECLAMACION.ID_REPORTE_SIN AND ASEG.ID_COBERTURA=1 AND ASEG.F_TER_REG = DATE '4712-12-31') \r\n"+
			" WHERE RECLAMACION.ID_REPORTE_SIN = ? \r\n"+
			" AND RECLAMACION.F_TER_REG = DATE '4712-12-31' \r\n"+
			" AND (AUTO.ID_VERSION_RECL is null or AUTO.ID_VERSION_RECL = (SELECT MAX(ID_VERSION_RECL) FROM SEYCOS.SIN_AUTO WHERE ID_REPORTE_SIN = RECLAMACION.ID_REPORTE_SIN AND ID_LIN_NEGOCIO = SIN_INCISO_RECL.ID_LIN_NEGOCIO AND ID_INCISO = SIN_INCISO_RECL.ID_INCISO)) \r\n"+
			" and (VEHICLE.id_version_pol is null or VEHICLE.id_version_pol = (select max(id_version_pol) from SEYCOS.pol_auto where id_cotizacion = RECLAMACION.ID_COTIZACION and id_lin_negocio = SIN_INCISO_RECL.ID_LIN_NEGOCIO and id_inciso = SIN_INCISO_RECL.ID_INCISO)) \r\n";
		
		return (List<DeclaracionSiniestro>)jdbcTemplate.query(sql, new Object[] {id}, new RowMapper<DeclaracionSiniestro>() {
			
			@Override
			public DeclaracionSiniestro mapRow(ResultSet rs, int rowNum) throws SQLException {
				
				DeclaracionSiniestro declaracionSiniestro = new DeclaracionSiniestro();
				declaracionSiniestro.setfSiniestro(rs.getTimestamp("FSINIESTRO"));
				declaracionSiniestro.setfAtencion(rs.getTimestamp("FATENCION"));
				declaracionSiniestro.setNumeroReporte(rs.getLong("NUMEROREPORTE"));
				declaracionSiniestro.setNumeroSiniestro(rs.getLong("NUMEROSINIESTRO")>0?rs.getLong("NUMEROSINIESTRO"):null);
				declaracionSiniestro.setPoliza(rs.getString("POLIZA"));
				declaracionSiniestro.setInciso(rs.getInt("INCISO")>0?rs.getInt("INCISO"):null);
				declaracionSiniestro.setNombreAsegurado(rs.getString("NOMBREASEGURADO"));
				declaracionSiniestro.setNombreConductor(rs.getString("NOMBRECONDUCTOR"));
				declaracionSiniestro.setMarcaVehiculoAsegurado(rs.getString("MARCAVEHICULOASEGURADO"));
				declaracionSiniestro.setTipoVehiculoAsegurado(rs.getString("TIPOVEHICULOASEGURADO"));
				declaracionSiniestro.setModeloVehiculoAsegurado(rs.getInt("MODELOVEHICULOASEGURADO")>0?rs.getInt("MODELOVEHICULOASEGURADO"):null);
				declaracionSiniestro.setPlacasVehiculoAsegurado(rs.getString("PLACASVEHICULOASEGURADO"));
				declaracionSiniestro.setNumSerieVehiculoAsegurado(rs.getString("NUMSERIEVEHICULOASEGURADO"));
				declaracionSiniestro.setDeclaracionSiniestro(rs.getString("DECLARACIONSINIESTRO")); 
				declaracionSiniestro.setTerceros(getTerceros(rs.getLong("ID_REPORTE_SIN")));
				declaracionSiniestro.setTerminoAjuste(rs.getString("TERMINOAJUSTE"));
				declaracionSiniestro.setResponsabilidad(rs.getString("RESPONSABILIDAD"));
				declaracionSiniestro.setEstatusPoliza(rs.getString("ESTATUSPOLIZA"));
				
				return declaracionSiniestro;
			}
		});
	}

	private List<AtencionATallerAsegurado> getPaseAtencionATallerAsegurado(Long id, Long idTercero) {
		
		String sql = "select sin_tercero.folio_tercero NUMFOLIO, \r\n" +
				   "sin_tercero.f_ini_reg FPASEATENCION, \r\n" +
				   "RECLAMACION.NUM_REP_CABINA NUMEROREPORTE, \r\n" + 
				   "RECLAMACION.NUM_RECLAMACION NUMEROSINIESTRO, \r\n" + 
				   "LPAD(TO_CHAR(POLIZA.ID_CENTRO_EMIS),3,0) || '-' ||LPAD(TO_CHAR(POLIZA.NUM_POLIZA),12,0) || '-' || LPAD(TO_CHAR(POLIZA.NUM_RENOV_POL),2,0) POLIZA, \r\n" + 
				   "SIN_INCISO_RECL.id_inciso INCISO, \r\n" + 
				   "UPPER(NVL(SIN_TERCERO.NOM_TALLER_ASIG, PERSONA.NOMBRE)) NOMBRETALLERASIGNADO, \r\n" +
				   "UPPER(POB_PAIS.DESC_POBLACION) PAIS, \r\n" +
				   "UPPER(POB_ESTADO.DESC_POBLACION) ESTADO, \r\n" +
				   "UPPER(POB_CIUDAD.DESC_POBLACION) CIUDAD, \r\n" +
				   "UPPER(DOM.CALLE_NUMERO) CALLENUMERO, \r\n" +
				   "DOM.CODIGO_POSTAL CODIGOPOSTAL, \r\n" +
				   "UPPER(DOM.COLONIA) COLONIA, \r\n" +
				   "UPPER(NVL(PERSONA.TELEF_OFICINA, PERSONA.TELEF_CASA)) TELEFONOTALLERASIGNADO, \r\n" +            
				   "UPPER(PORTAL.GET_CLIENT_NAME (RECLAMACION.ID_COTIZACION,RECLAMACION.ID_VERSION_POL,SIN_INCISO_RECL.ID_LIN_NEGOCIO,SIN_INCISO_RECL.id_inciso)) NOMBREASEGURADO, \r\n" + 
				   "UPPER(SIN_TERCERO.NOM_TERCERO) NOMBREINTERESADO, \r\n" +
				   "UPPER(trim(nvl(TELEF_1, '')||' '||nvl(TELEF_2, ''))) TELEFONOS, \r\n" +
				   "UPPER(MARCA.NOM_MARCA || ' ' || VEHICLE.ID_ESTILO) MARCAVEHICULOASEGURADO, \r\n" +
				   "UPPER(NVL(SIN_TERCERO.desc_tipo_estilo,VEHICLE.DESC_VEHIC)) TIPOVEHICULOASEGURADO, \r\n" +
				   "NVL(SIN_TERCERO.ID_MODELO_AUTO,VEHICLE.ID_MODELO_AUTO) MODELOVEHICULOASEGURADO, \r\n" +
				   "UPPER(NVL(SIN_TERCERO.PLACA,VEHICLE.PLACA)) PLACASVEHICULOASEGURADO, \r\n" +
				   "UPPER(NVL(SIN_TERCERO.NUM_SERIE,VEHICLE.NUM_SERIE)) NUMSERIEVEHICULOASEGURADO, \r\n" +
				   "nvl(DECODE(sin_tercero.b_deducible,'V','SI','NO'),'NO') APL_DEDUCIBLE, \r\n" +
				   "UPPER(sin_tercero.correo) EMAIL, \r\n" +
				   "UPPER(sin_tercero.areas_dan) AREASDANIADAS, \r\n" +
				   "UPPER(sin_tercero.dan_preexistentes) DANIOSPREEXISTENTES, \r\n" +
				   " POL_COBERTURA.DEDUCIBLE_DFT/100 PCT_DEDUCIBLE, \r\n" +
				   " NVL(SIN_COBERT_RECL.imp_deducible, nvl(POL_COBERTURA.sa_amparada * (POL_COBERTURA.deducible_dft/100) ,0)) DEDUCIBLE, \r\n" +
				   " UPPER(POB_PAIS_TER.DESC_POBLACION) PAISI,  \r\n" +
				   " UPPER(POB_ESTADO_TER.DESC_POBLACION) ESTADOI, \r\n" +
				   " UPPER(POB_CIUDAD_TER.DESC_POBLACION) CIUDADI, \r\n" +
				   " UPPER(DOM_TER.CALLE_NUMERO) CALLENUMEROI, \r\n" + 
				   " DOM_TER.CODIGO_POSTAL CODIGOPOSTALI, \r\n" + 
				   " UPPER(DOM_TER.COLONIA) COLONIAI \r\n" +				   
				"FROM SEYCOS.SIN_RECLAMACION RECLAMACION \r\n" +
				"INNER join SEYCOS.sin_tercero on (reclamacion.ID_REPORTE_SIN = sin_tercero.ID_REPORTE_SIN and sin_tercero.id_tercero = ?) \r\n" +
				"Inner Join Seycos.Pol_Poliza Poliza On (Poliza.Id_Cotizacion = Reclamacion.Id_Cotizacion And Poliza.Id_Version_Pol = Reclamacion.Id_Version_Pol) \r\n" +
				"Inner Join Seycos.Sin_Inciso_Recl On (Reclamacion.Id_Reporte_Sin = Sin_Inciso_Recl.Id_Reporte_Sin AND RECLAMACION.ID_COTIZACION=Sin_Inciso_Recl.ID_COTIZACION)  \r\n" +
				"INNER join SEYCOS.INS_PREST_SERV on (INS_PREST_SERV.id_prest_serv = sin_tercero.id_taller_asig) \r\n" +
				"INNER JOIN SEYCOS.PERSONA ON PERSONA.ID_PERSONA = INS_PREST_SERV.ID_PERSONA \r\n" +
				"INNER JOIN SEYCOS.DOMICILIO DOM ON DOM.ID_DOMICILIO = PERSONA.ID_DOMICILIO \r\n" +
				"INNER JOIN SEYCOS.POBLACION PO on po.cve_poblacion = dom.cve_estado \r\n" +
				"INNER JOIN SEYCOS.poblacion pob_pais on DOM.cve_pais = pob_pais.cve_poblacion \r\n" +
				"INNER JOIN SEYCOS.poblacion pob_estado on DOM.cve_estado = pob_estado.cve_poblacion \r\n" +
				"INNER JOIN SEYCOS.poblacion pob_ciudad on DOM.cve_ciudad = pob_ciudad.cve_poblacion \r\n" +
				"LEFT JOIN SEYCOS.POL_AUTO VEHICLE ON (RECLAMACION.ID_COTIZACION = VEHICLE.ID_COTIZACION AND VEHICLE.ID_LIN_NEGOCIO = SIN_INCISO_RECL.ID_LIN_NEGOCIO AND VEHICLE.ID_INCISO = SIN_INCISO_RECL.id_inciso) \r\n" +
				"LEFT JOIN SEYCOS.ING_MARCA MARCA on (MARCA.ID_MARCA = VEHICLE.ID_MARCA) \r\n" +
				"LEFT JOIN SEYCOS.SIN_COBERT_RECL ON (RECLAMACION.ID_REPORTE_SIN = SIN_COBERT_RECL.ID_REPORTE_SIN AND SIN_COBERT_RECL.ID_LIN_NEGOCIO=SIN_INCISO_RECL.ID_LIN_NEGOCIO AND SIN_COBERT_RECL.ID_INCISO=SIN_INCISO_RECL.ID_INCISO AND SIN_COBERT_RECL.ID_COBERTURA=SIN_TERCERO.ID_COBERTURA) \r\n" +
				"LEFT JOIN SEYCOS.POL_COBERTURA on (POL_COBERTURA.ID_COTIZACION = RECLAMACION.ID_COTIZACION \r\n" +
				" AND POL_COBERTURA.ID_VERSION_POL = RECLAMACION.ID_VERSION_POL \r\n" +
				" AND POL_COBERTURA.ID_LIN_NEGOCIO = SIN_INCISO_RECL.ID_LIN_NEGOCIO \r\n" +
				" AND POL_COBERTURA.ID_INCISO = SIN_INCISO_RECL.ID_INCISO \r\n" +
				" AND POL_COBERTURA.ID_COBERTURA = SIN_TERCERO.ID_COBERTURA) \r\n" +
				"LEFT JOIN SEYCOS.SIN_DOMICILIO DOM_TER ON (SIN_TERCERO.ID_DOMICIL_TERC = DOM_TER.ID_DOMICILIO_SIN) \r\n" + 
				"LEFT JOIN SEYCOS.POBLACION POB_PAIS_TER ON (DOM_TER.CVE_PAIS = POB_PAIS_TER.CVE_POBLACION) \r\n" +
				"LEFT JOIN SEYCOS.POBLACION POB_ESTADO_TER ON (DOM_TER.CVE_ESTADO = POB_ESTADO_TER.CVE_POBLACION) \r\n" +
				"LEFT JOIN SEYCOS.POBLACION POB_CIUDAD_TER ON (DOM_TER.CVE_CIUDAD = POB_CIUDAD_TER.CVE_POBLACION) \r\n" +
				"WHERE RECLAMACION.ID_REPORTE_SIN = ? \r\n" + 
				"AND RECLAMACION.F_TER_REG = DATE '4712-12-31' \r\n" +
				"AND sin_tercero.F_TER_REG = DATE '4712-12-31' \r\n" +
				"AND SIN_INCISO_RECL.f_ter_reg = DATE '4712-12-31' \r\n" +
				"AND SIN_COBERT_RECL.F_TER_REG  = DATE '4712-12-31' \r\n" +
				"and (VEHICLE.id_version_pol is null or VEHICLE.id_version_pol = (select max(id_version_pol) from SEYCOS.pol_auto where id_cotizacion = RECLAMACION.ID_COTIZACION and id_lin_negocio = SIN_INCISO_RECL.ID_LIN_NEGOCIO and id_inciso = SIN_INCISO_RECL.ID_INCISO)) "; 
		
		return (List<AtencionATallerAsegurado>)jdbcTemplate.query(sql, new Object[] {idTercero, id}, new RowMapper<AtencionATallerAsegurado>() {

			@Override
			public AtencionATallerAsegurado mapRow(ResultSet rs, int rowNum) throws SQLException {
				AtencionATallerAsegurado atencionATallerAsegurado = new AtencionATallerAsegurado();
				atencionATallerAsegurado.setNumFolio(rs.getString("NUMFOLIO"));
				atencionATallerAsegurado.setfPaseAtencion(rs.getDate("FPASEATENCION"));
				atencionATallerAsegurado.setNumeroReporte(rs.getLong("NUMEROREPORTE"));
				atencionATallerAsegurado.setNumeroSiniestro(rs.getLong("NUMEROSINIESTRO")>0?rs.getLong("NUMEROSINIESTRO"):null);
				atencionATallerAsegurado.setPoliza(rs.getString("POLIZA"));
				atencionATallerAsegurado.setNumeroInciso(rs.getInt("INCISO"));
				atencionATallerAsegurado.setNombreTallerAsignado(rs.getString("NOMBRETALLERASIGNADO"));
				atencionATallerAsegurado.setTelefonoTallerAsignado(rs.getString("TELEFONOTALLERASIGNADO"));
				
				//Direccion del taller
				atencionATallerAsegurado.setDomicilioTaller(new Domicilio(rs.getString("PAIS"), rs.getString("ESTADO"), rs.getString("CIUDAD"), rs.getString("COLONIA"), rs.getString("CODIGOPOSTAL"), rs.getString("CALLENUMERO")));
								
				atencionATallerAsegurado.setNombreAsegurado(rs.getString("NOMBREASEGURADO"));
				atencionATallerAsegurado.setNombreInteresado(rs.getString("NOMBREINTERESADO"));
				//Direccion del interesado
				atencionATallerAsegurado.setDomicilioInteresado(new Domicilio(rs.getString("PAISI"), rs.getString("ESTADOI"), rs.getString("CIUDADI"), rs.getString("COLONIAI"), rs.getString("CODIGOPOSTALI"), rs.getString("CALLENUMEROI")));
				
				atencionATallerAsegurado.setTelefonos(rs.getString("TELEFONOS"));
				atencionATallerAsegurado.setEmail(rs.getString("EMAIL"));
				atencionATallerAsegurado.setMarcaVehiculoAsegurado(rs.getString("MARCAVEHICULOASEGURADO"));
				atencionATallerAsegurado.setTipoVehiculoAsegurado(rs.getString("TIPOVEHICULOASEGURADO"));
				atencionATallerAsegurado.setModeloVehiculoAsegurado(rs.getInt("MODELOVEHICULOASEGURADO"));
				atencionATallerAsegurado.setPlacasVehiculoAsegurado(rs.getString("PLACASVEHICULOASEGURADO"));
				atencionATallerAsegurado.setNumSerieVehiculoAsegurado(rs.getString("NUMSERIEVEHICULOASEGURADO"));
				atencionATallerAsegurado.setAreasDaniadas(rs.getString("AREASDANIADAS"));
				atencionATallerAsegurado.setDaniosPreexistentes(rs.getString("DANIOSPREEXISTENTES"));
				atencionATallerAsegurado.setAplicaDeducible(rs.getString("APL_DEDUCIBLE"));
				atencionATallerAsegurado.setDeducible(rs.getDouble("DEDUCIBLE"));
				atencionATallerAsegurado.setPct_deducible(rs.getDouble("PCT_DEDUCIBLE"));
				return atencionATallerAsegurado;
			}
		});
	}
	
	private List<AtencionATercero> getPaseAtencionATercero(Long idReporteSiniestro, Long idTercero) {
		//Pase de atencion a tercero y reembolso a compañia del tercero
		
		String sql="select sin_tercero.folio_tercero NUMFOLIO, \r\n" +
   " sin_tercero.f_ini_reg FPASEATENCION, \r\n" +
   " RECLAMACION.NUM_REP_CABINA NUMEROREPORTE, \r\n" + 
   " RECLAMACION.NUM_RECLAMACION NUMEROSINIESTRO,  \r\n" +
   " LPAD(TO_CHAR(POLIZA.ID_CENTRO_EMIS),3,0) || '-' ||LPAD(TO_CHAR(POLIZA.NUM_POLIZA),12,0) || '-' || LPAD(TO_CHAR(POLIZA.NUM_RENOV_POL),2,0) POLIZA, \r\n" + 
   " SIN_INCISO_RECL.id_inciso INCISO, \r\n" + 
   " UPPER(NVL(SIN_TERCERO.NOM_TALLER_ASIG, PERSONA.NOMBRE)) NOMBRETALLERASIGNADO, \r\n" + 
   " UPPER(POB_PAIS.DESC_POBLACION) PAIS, \r\n" +
   " UPPER(POB_ESTADO.DESC_POBLACION) ESTADO, \r\n" +
   " UPPER(POB_CIUDAD.DESC_POBLACION) CIUDAD, \r\n" +
   " UPPER(DOM.CALLE_NUMERO) CALLENUMERO, \r\n" +
   " DOM.CODIGO_POSTAL CODIGOPOSTAL, \r\n" +
   " UPPER(DOM.COLONIA) COLONIA, \r\n" +
   " UPPER(NVL(PERSONA.TELEF_OFICINA, PERSONA.TELEF_CASA)) TELEFONOTALLERASIGNADO, \r\n" +  
   " UPPER(SIN_TERCERO.nom_Tercero) NOMBREINTERESADO, \r\n" +
   " UPPER(trim(nvl(TELEF_1, '')||' '||nvl(TELEF_2, ''))) TELEFONOS, \r\n" +
   " UPPER(MARCA.NOM_MARCA || ' - ' || DECODE(TRIM(MARCA.CVEL_T_BIEN), 'CAMN', 'CAMIONES', MARCA.CVEL_T_BIEN)) AS  MARCAVEHICULOTERCERO, \r\n" +
   " UPPER(sin_tercero.desc_tipo_estilo) TIPOVEHICULOTERCERO, \r\n" +
   " sin_tercero.ID_MODELO_AUTO MODELOVEHICULOTERCERO, \r\n" + 
   " UPPER(sin_tercero.PLACA) PLACASVEHICULOTERCERO, \r\n" +
   " UPPER(sin_tercero.num_serie) NUMSERIEVEHICULOTERCERO, \r\n" +
   " UPPER(sin_tercero.areas_dan) AREASDANIADAS, \r\n" +
   " UPPER(sin_tercero.dan_preexistentes) DANIOSPREEXISTENTES, \r\n" +
   " UPPER(sin_tercero.correo) EMAIL, \r\n" +
   " UPPER(SIN_TERCERO.NOM_CIA_ASEG) NOM_CIA_ASEG, \r\n" +
   " UPPER(POB_PAIS_TER.DESC_POBLACION) PAISI,  \r\n" +
   " UPPER(POB_ESTADO_TER.DESC_POBLACION) ESTADOI, \r\n" +
   " UPPER(POB_CIUDAD_TER.DESC_POBLACION) CIUDADI, \r\n" +
   " UPPER(DOM_TER.CALLE_NUMERO) CALLENUMEROI, \r\n" + 
   " DOM_TER.CODIGO_POSTAL CODIGOPOSTALI, \r\n" + 
   " UPPER(DOM_TER.COLONIA) COLONIAI \r\n" +				   
   " FROM SEYCOS.SIN_RECLAMACION RECLAMACION \r\n" +
   " INNER JOIN SEYCOS.sin_tercero on (reclamacion.ID_REPORTE_SIN = sin_tercero.ID_REPORTE_SIN and sin_tercero.id_tercero = ?) \r\n" +
   " INNER JOIN SEYCOS.POL_POLIZA POLIZA ON (POLIZA.ID_COTIZACION = RECLAMACION.ID_COTIZACION AND POLIZA.ID_VERSION_POL = RECLAMACION.ID_VERSION_POL) \r\n" +
   " INNER JOIN SEYCOS.SIN_INCISO_RECL on (RECLAMACION.ID_REPORTE_SIN = SIN_INCISO_RECL.ID_REPORTE_SIN AND RECLAMACION.ID_COTIZACION=SIN_INCISO_RECL.ID_COTIZACION) \r\n" +
   " LEFT JOIN SEYCOS.INS_PREST_SERV on (INS_PREST_SERV.id_prest_serv = sin_tercero.id_taller_asig) \r\n" +
   " LEFT JOIN SEYCOS.PERSONA ON PERSONA.ID_PERSONA = INS_PREST_SERV.ID_PERSONA \r\n" +
   " LEFT JOIN SEYCOS.DOMICILIO DOM ON DOM.ID_DOMICILIO = PERSONA.ID_DOMICILIO \r\n" +
   " LEFT JOIN SEYCOS.POBLACION PO on po.cve_poblacion = dom.cve_estado \r\n" +
   " LEFT JOIN SEYCOS.poblacion pob_pais on DOM.cve_pais = pob_pais.cve_poblacion \r\n" +
   " LEFT JOIN SEYCOS.poblacion pob_estado on DOM.cve_estado = pob_estado.cve_poblacion \r\n" +
   " LEFT JOIN SEYCOS.poblacion pob_ciudad on DOM.cve_ciudad = pob_ciudad.cve_poblacion \r\n" +
   " LEFT JOIN SEYCOS.ING_MARCA MARCA on (MARCA.ID_MARCA = sin_tercero.ID_MARCA) \r\n" +
   " LEFT JOIN SEYCOS.SIN_DOMICILIO DOM_TER ON (SIN_TERCERO.ID_DOMICIL_TERC = DOM_TER.ID_DOMICILIO_SIN) \r\n" + 
   " LEFT JOIN SEYCOS.POBLACION POB_PAIS_TER ON (DOM_TER.CVE_PAIS = POB_PAIS_TER.CVE_POBLACION) \r\n" +
   " LEFT JOIN SEYCOS.POBLACION POB_ESTADO_TER ON (DOM_TER.CVE_ESTADO = POB_ESTADO_TER.CVE_POBLACION) \r\n" +
   " LEFT JOIN SEYCOS.POBLACION POB_CIUDAD_TER ON (DOM_TER.CVE_CIUDAD = POB_CIUDAD_TER.CVE_POBLACION) \r\n" +
   " WHERE RECLAMACION.ID_REPORTE_SIN = ? \r\n" +
   " AND RECLAMACION.F_TER_REG = DATE '4712-12-31' \r\n" +
   " AND sin_tercero.F_TER_REG = DATE '4712-12-31' \r\n" +
   " AND SIN_INCISO_RECL.f_ter_reg = DATE '4712-12-31'";
		
		return (List<AtencionATercero>)jdbcTemplate.query(sql, new Object[] {idTercero, idReporteSiniestro}, new RowMapper<AtencionATercero>() {

			@Override
			public AtencionATercero mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				AtencionATercero aTercero = new AtencionATercero();
				aTercero.setNumFolio(rs.getString("numFolio"));
				aTercero.setfPaseAtencion(rs.getDate("fPaseAtencion"));
				aTercero.setNumeroReporte(rs.getLong("numeroReporte"));
				aTercero.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
				aTercero.setPoliza(rs.getString("poliza"));
				aTercero.setNumeroInciso(rs.getInt("INCISO"));
				aTercero.setNombreTallerAsignado(rs.getString("NOMBRETALLERASIGNADO"));
				aTercero.setTelefonoTallerAsignado(rs.getString("TELEFONOTALLERASIGNADO"));
				aTercero.setCia(rs.getString("NOM_CIA_ASEG"));
				aTercero.setDomicilioTaller(new Domicilio(rs.getString("PAIS"), rs.getString("ESTADO"),
						rs.getString("CIUDAD"), rs.getString("COLONIA"), rs.getString("CODIGOPOSTAL"), rs.getString("CALLENUMERO")));				
				aTercero.setNombreInteresado(rs.getString("NOMBREINTERESADO"));	
				aTercero.setDomicilioInteresado(new Domicilio(rs.getString("PAISI"), rs.getString("ESTADOI"),
						rs.getString("CIUDADI"), rs.getString("COLONIAI"), rs.getString("CODIGOPOSTALI"), rs.getString("CALLENUMEROI")));				
				aTercero.setTelefonos(rs.getString("TELEFONOS"));
				aTercero.setEmail(rs.getString("EMAIL"));	
				aTercero.setMarcaVehiculo(rs.getString("MARCAVEHICULOTERCERO"));
				aTercero.setTipoVehiculo(rs.getString("TIPOVEHICULOTERCERO"));
				aTercero.setModeloVehiculo(rs.getInt("MODELOVEHICULOTERCERO"));
				aTercero.setPlacasVehiculo(rs.getString("PLACASVEHICULOTERCERO"));
				aTercero.setNumSerieVehiculo(rs.getString("NUMSERIEVEHICULOTERCERO"));
				aTercero.setAreasDaniadas(rs.getString("AREASDANIADAS"));
				aTercero.setDaniosPreexistentes(rs.getString("DANIOSPREEXISTENTES"));
				return aTercero;
			}
			
		});
	}

	private List<AtencionParaPagosDeDanios> getAtencionParaPagosDeDanios(Long id, Long idTercero) {
		String sql="SELECT SIN_TERCERO.FOLIO_TERCERO NUMFOLIO, \r\n" +
		   " sin_tercero.f_ini_reg FPASEATENCION, \r\n" +
		   " RECLAMACION.NUM_REP_CABINA NUMEROREPORTE, \r\n" + 
		   " RECLAMACION.NUM_RECLAMACION NUMEROSINIESTRO, \r\n" + 
		   " LPAD(TO_CHAR(POLIZA.ID_CENTRO_EMIS),3,0) || '-' ||LPAD(TO_CHAR(POLIZA.NUM_POLIZA),12,0) || '-' || LPAD(TO_CHAR(POLIZA.NUM_RENOV_POL),2,0) POLIZA, \r\n" + 
		   " SIN_INCISO_RECL.ID_INCISO INCISO, \r\n" + 
		   " UPPER(PORTAL.GET_CLIENT_NAME (RECLAMACION.ID_COTIZACION,RECLAMACION.ID_VERSION_POL,SIN_INCISO_RECL.ID_LIN_NEGOCIO,SIN_INCISO_RECL.ID_INCISO)) NOMBREASEGURADO, \r\n" + 
		   " UPPER(SIN_TERCERO.NOM_TERCERO) NOMBREINTERESADO, \r\n" +
		   " UPPER(TRIM(NVL(TELEF_1, '')||' '||NVL(TELEF_2, ''))) TELEFONOS, \r\n" +
		   " UPPER(MARCA.NOM_MARCA || ' - ' || DECODE(TRIM(MARCA.CVEL_T_BIEN), 'CAMN', 'CAMIONES', MARCA.CVEL_T_BIEN)) AS  MARCAVEHICULOASEGURADO, \r\n" +
		   " UPPER(SIN_TERCERO.DESC_TIPO_ESTILO) TIPOVEHICULOASEGURADO, \r\n" +
		   " SIN_TERCERO.ID_MODELO_AUTO MODELOVEHICULOASEGURADO, \r\n" +
		   " UPPER(SIN_TERCERO.PLACA) PLACASVEHICULOASEGURADO, \r\n" +
		   " UPPER(SIN_TERCERO.NUM_SERIE) NUMSERIEVEHICULOASEGURADO, \r\n" +
		   " UPPER(SIN_TERCERO.areas_dan) AREASDANIADAS, \r\n" +
		   " UPPER(SIN_TERCERO.dan_preexistentes) DANIOSPREEXISTENTES, \r\n" +
		   " UPPER(SIN_TERCERO.CORREO) EMAIL, \r\n" +
		   " NVL(DECODE(SIN_TERCERO.B_DEDUCIBLE,'V','SI','NO'),'NO') APL_DEDUCIBLE, \r\n" +
		   " POL_COBERTURA.DEDUCIBLE_DFT/100 PCT_DEDUCIBLE, \r\n" +
		   " NVL(SIN_COBERT_RECL.SA_AMPARADA, POL_COBERTURA.SA_AMPARADA) VALORCOMERCIAL, \r\n" +
		   " NVL(SIN_COBERT_RECL.imp_deducible, nvl(POL_COBERTURA.sa_amparada * (POL_COBERTURA.deducible_dft/100) ,0)) DEDUCIBLE, \r\n" +
		   " UPPER(POB_PAIS_TER.DESC_POBLACION) PAISI,  \r\n" +
		   " UPPER(POB_ESTADO_TER.DESC_POBLACION) ESTADOI, \r\n" +
		   " UPPER(POB_CIUDAD_TER.DESC_POBLACION) CIUDADI, \r\n" +
		   " UPPER(DOM_TER.CALLE_NUMERO) CALLENUMEROI, \r\n" + 
		   " DOM_TER.CODIGO_POSTAL CODIGOPOSTALI, \r\n" + 
		   " UPPER(DOM_TER.COLONIA) COLONIAI, \r\n" +
		   " UPPER(DP_COBERTURA.nom_cobertura) COBERTURAAFECTADA \r\n" +				   
		   " FROM SEYCOS.SIN_RECLAMACION RECLAMACION \r\n" +
		   " INNER JOIN SEYCOS.SIN_TERCERO ON (RECLAMACION.ID_REPORTE_SIN = SIN_TERCERO.ID_REPORTE_SIN AND SIN_TERCERO.ID_TERCERO = ?) \r\n" +
		   " Inner Join Seycos.Pol_Poliza Poliza On (Poliza.Id_Cotizacion = Reclamacion.Id_Cotizacion And Poliza.Id_Version_Pol = Reclamacion.Id_Version_Pol)  \r\n" +
		   " Inner Join Seycos.Sin_Inciso_Recl On (Reclamacion.Id_Reporte_Sin = Sin_Inciso_Recl.Id_Reporte_Sin AND RECLAMACION.ID_COTIZACION=SIN_INCISO_RECL.ID_COTIZACION) \r\n" +
		   " INNER JOIN SEYCOS.DP_COBERTURA ON (DP_COBERTURA.ID_COBERTURA = SIN_TERCERO.ID_COBERTURA) \r\n" +
		   " INNER JOIN SEYCOS.INS_PREST_SERV ON (INS_PREST_SERV.ID_PREST_SERV = SIN_TERCERO.ID_TALLER_ASIG) \r\n" +
		   " INNER JOIN SEYCOS.PERSONA ON PERSONA.ID_PERSONA = INS_PREST_SERV.ID_PERSONA \r\n" +
		   " INNER JOIN SEYCOS.DOMICILIO DOM ON DOM.ID_DOMICILIO = PERSONA.ID_DOMICILIO \r\n" +
		   " INNER JOIN SEYCOS.POBLACION PO ON PO.CVE_POBLACION = DOM.CVE_ESTADO \r\n" +
		   " INNER JOIN SEYCOS.POBLACION POB_PAIS ON DOM.CVE_PAIS = POB_PAIS.CVE_POBLACION \r\n" +
		   " INNER JOIN SEYCOS.POBLACION POB_ESTADO ON DOM.CVE_ESTADO = POB_ESTADO.CVE_POBLACION \r\n" +
		   " LEFT JOIN SEYCOS.POBLACION POB_CIUDAD ON DOM.CVE_CIUDAD = POB_CIUDAD.CVE_POBLACION \r\n" +
		   " LEFT JOIN SEYCOS.ING_MARCA MARCA ON (MARCA.ID_MARCA = SIN_TERCERO.ID_MARCA) \r\n" +
		   " LEFT JOIN SEYCOS.SIN_COBERT_RECL ON (RECLAMACION.ID_REPORTE_SIN = SIN_COBERT_RECL.ID_REPORTE_SIN AND SIN_COBERT_RECL.ID_LIN_NEGOCIO=SIN_INCISO_RECL.ID_LIN_NEGOCIO AND SIN_COBERT_RECL.ID_INCISO=SIN_INCISO_RECL.ID_INCISO AND SIN_COBERT_RECL.ID_COBERTURA=SIN_TERCERO.ID_COBERTURA) \r\n" +
		   " LEFT JOIN seycos.POL_COBERTURA on (POL_COBERTURA.ID_COTIZACION = RECLAMACION.ID_COTIZACION \r\n" +
		   " AND POL_COBERTURA.ID_VERSION_POL = RECLAMACION.ID_VERSION_POL \r\n" +
		   " AND POL_COBERTURA.ID_LIN_NEGOCIO = SIN_INCISO_RECL.ID_LIN_NEGOCIO \r\n" +
		   " AND POL_COBERTURA.ID_INCISO = SIN_INCISO_RECL.ID_INCISO \r\n" +
		   " AND POL_COBERTURA.ID_COBERTURA = SIN_TERCERO.ID_COBERTURA) \r\n" +
		   " LEFT JOIN SEYCOS.SIN_DOMICILIO DOM_TER ON (SIN_TERCERO.ID_DOMICIL_TERC = DOM_TER.ID_DOMICILIO_SIN) \r\n" + 
		   " LEFT JOIN SEYCOS.POBLACION POB_PAIS_TER ON (DOM_TER.CVE_PAIS = POB_PAIS_TER.CVE_POBLACION) \r\n" +
		   " LEFT JOIN SEYCOS.POBLACION POB_ESTADO_TER ON (DOM_TER.CVE_ESTADO = POB_ESTADO_TER.CVE_POBLACION) \r\n" +
		   " LEFT JOIN SEYCOS.POBLACION POB_CIUDAD_TER ON (DOM_TER.CVE_CIUDAD = POB_CIUDAD_TER.CVE_POBLACION) \r\n" +
		   " WHERE RECLAMACION.ID_REPORTE_SIN = ? \r\n" + 
		   " AND RECLAMACION.F_TER_REG = DATE '4712-12-31' \r\n" +
		   " AND SIN_TERCERO.F_TER_REG = DATE '4712-12-31' \r\n" +
		   " AND SIN_INCISO_RECL.F_TER_REG = DATE '4712-12-31' \r\n" +
		   " AND SIN_COBERT_RECL.F_TER_REG  = DATE '4712-12-31'"; 
		
		return (List<AtencionParaPagosDeDanios>)jdbcTemplate.query(sql, new Object[]{idTercero, id}, new RowMapper<AtencionParaPagosDeDanios>(){

			@Override
			public AtencionParaPagosDeDanios mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				AtencionParaPagosDeDanios atencionParaPagosDeDanios = new AtencionParaPagosDeDanios();
				atencionParaPagosDeDanios.setNumFolio(rs.getString("NUMFOLIO"));
				atencionParaPagosDeDanios.setfPaseAtencion(rs.getDate("FPASEATENCION"));
				atencionParaPagosDeDanios.setNumeroReporte(rs.getLong("NUMEROREPORTE"));
				atencionParaPagosDeDanios.setNumeroSiniestro(rs.getLong("NUMEROSINIESTRO")>0?rs.getLong("NUMEROSINIESTRO"):null);
				atencionParaPagosDeDanios.setPoliza(rs.getString("POLIZA"));
				atencionParaPagosDeDanios.setNumeroInciso(rs.getInt("INCISO"));
				atencionParaPagosDeDanios.setNombreAsegurado(rs.getString("NOMBREASEGURADO"));
				atencionParaPagosDeDanios.setNombreInteresado(rs.getString("NOMBREINTERESADO"));
				atencionParaPagosDeDanios.setCobertura(rs.getString("COBERTURAAFECTADA"));
				atencionParaPagosDeDanios.setDomicilioInteresado(new Domicilio(rs.getString("PAISI"), rs.getString("ESTADOI"), rs.getString("CIUDADI"), rs.getString("COLONIAI"), rs.getString("CODIGOPOSTALI"), rs.getString("CALLENUMEROI")));
				
				atencionParaPagosDeDanios.setTelefonos(rs.getString("TELEFONOS"));
				atencionParaPagosDeDanios.setEmail(rs.getString("EMAIL"));	
				atencionParaPagosDeDanios.setMarcaVehiculoAsegurado(rs.getString("MARCAVEHICULOASEGURADO"));
				atencionParaPagosDeDanios.setTipoVehiculoAsegurado(rs.getString("TIPOVEHICULOASEGURADO"));
				atencionParaPagosDeDanios.setModeloVehiculoAsegurado(rs.getInt("MODELOVEHICULOASEGURADO")>0?rs.getInt("MODELOVEHICULOASEGURADO"):null);
				atencionParaPagosDeDanios.setPlacasVehiculoAsegurado(rs.getString("PLACASVEHICULOASEGURADO"));
				atencionParaPagosDeDanios.setNumSerieVehiculoAsegurado(rs.getString("NUMSERIEVEHICULOASEGURADO"));
				atencionParaPagosDeDanios.setAreasDaniadas(rs.getString("AREASDANIADAS"));
				atencionParaPagosDeDanios.setDaniosPreexistentes(rs.getString("DANIOSPREEXISTENTES"));
				atencionParaPagosDeDanios.setAplicaDeducible(rs.getString("APL_DEDUCIBLE"));
				atencionParaPagosDeDanios.setDeducible(rs.getDouble("DEDUCIBLE")); 
				atencionParaPagosDeDanios.setPct_deducible(rs.getDouble("PCT_DEDUCIBLE"));
				atencionParaPagosDeDanios.setValorComercial(rs.getDouble("VALORCOMERCIAL"));
				return atencionParaPagosDeDanios;
			}
			
		});
	}
	
	private List<PaseServicioMedico> getPaseServicioMedico(Long id, Long idTercero) {
		String sql="select sin_tercero.folio_tercero NUMFOLIO, \r\n" +
   " sin_tercero.f_ini_reg FPASEATENCION, \r\n" +
   " RECLAMACION.NUM_REP_CABINA NUMEROREPORTE, \r\n" + 
   " RECLAMACION.NUM_RECLAMACION NUMEROSINIESTRO, \r\n" + 
   " RECLAMACION.FH_OCURRIDO FSINIESTRO, \r\n" +
   " LPAD(TO_CHAR(POLIZA.ID_CENTRO_EMIS),3,0) || '-' ||LPAD(TO_CHAR(POLIZA.NUM_POLIZA),12,0) || '-' || LPAD(TO_CHAR(POLIZA.NUM_RENOV_POL),2,0) POLIZA, \r\n" + 
   " SIN_INCISO_RECL.id_inciso INCISO, \r\n" + 
   " UPPER(NVL(SIN_TERCERO.NOM_hospital, PERSONA.NOMBRE)) INSTITUCIONHOSPITALARIA, \r\n" + 
   " UPPER(POB_PAIS.DESC_POBLACION) PAIS, \r\n" +
   " UPPER(POB_ESTADO.DESC_POBLACION) ESTADO, \r\n" +
   " 	 UPPER(POB_CIUDAD.DESC_POBLACION) CIUDAD, \r\n" +
   " 	 UPPER(CALLE_NUMERO) CALLENUMERO, \r\n" +
   " 	 CODIGO_POSTAL CODIGOPOSTAL, \r\n" +
   " 	 UPPER(COLONIA) COLONIA, \r\n" +
   " UPPER(NVL(PERSONA.TELEF_OFICINA, PERSONA.TELEF_CASA)) TELEFONOHOSPITAL, \r\n" +  
   " UPPER(SIN_TERCERO.nom_Tercero) NOMBRELESIONADO, \r\n" +
   " UPPER(trim(nvl(TELEF_1, '')||' '||nvl(TELEF_2, ''))) TELEFONOS, \r\n" +
   " SIN_TERCERO.EDAD, \r\n" +
   " UPPER(SIN_TERCERO.LESIONES) DESCRIPCIONLESIONES, \r\n" +
   " SIN_TERCERO.LIMITE_ATENCION ATENCIONHASTACANTIDAD, \r\n" +
   " UPPER(DP_COBERTURA.nom_cobertura) COBERTURAAFECTADA, \r\n" +
   " UPPER(SIN_TERCERO.NOM_CIA_ASEG) NOM_CIA_ASEG \r\n" +
   " FROM SEYCOS.SIN_RECLAMACION RECLAMACION \r\n" +
   " INNER JOIN SEYCOS.SIN_TERCERO ON (RECLAMACION.ID_REPORTE_SIN = SIN_TERCERO.ID_REPORTE_SIN AND SIN_TERCERO.ID_TERCERO = ?) \r\n" +
   " INNER JOIN SEYCOS.POL_POLIZA POLIZA ON (POLIZA.ID_COTIZACION = RECLAMACION.ID_COTIZACION AND POLIZA.ID_VERSION_POL = RECLAMACION.ID_VERSION_POL) \r\n" +
   " INNER JOIN SEYCOS.SIN_INCISO_RECL ON (RECLAMACION.ID_REPORTE_SIN = SIN_INCISO_RECL.ID_REPORTE_SIN AND RECLAMACION.ID_COTIZACION=SIN_INCISO_RECL.ID_COTIZACION) \r\n" +
   " LEFT JOIN SEYCOS.INS_PREST_SERV on (INS_PREST_SERV.id_prest_serv = sin_tercero.id_hospital) \r\n" +
   " LEFT JOIN SEYCOS.PERSONA ON PERSONA.ID_PERSONA = INS_PREST_SERV.ID_PERSONA \r\n" +
   " LEFT JOIN SEYCOS.DOMICILIO DOM ON DOM.ID_DOMICILIO = PERSONA.ID_DOMICILIO \r\n" +
   " LEFT JOIN SEYCOS.POBLACION PO on po.cve_poblacion = dom.cve_estado \r\n" +
   " LEFT JOIN SEYCOS.poblacion pob_pais on DOM.cve_pais = pob_pais.cve_poblacion \r\n" +
   " LEFT JOIN SEYCOS.poblacion pob_estado on DOM.cve_estado = pob_estado.cve_poblacion \r\n" +
   " LEFT JOIN SEYCOS.poblacion pob_ciudad on DOM.cve_ciudad = pob_ciudad.cve_poblacion \r\n" +
   " INNER JOIN SEYCOS.DP_COBERTURA on (DP_COBERTURA.id_cobertura = sin_tercero.id_cobertura) \r\n" +
   " WHERE RECLAMACION.ID_REPORTE_SIN = ? \r\n" +
   " AND RECLAMACION.F_TER_REG = DATE '4712-12-31' \r\n" +
   " AND sin_tercero.F_TER_REG = DATE '4712-12-31' \r\n" +
   " AND SIN_INCISO_RECL.f_ter_reg = DATE '4712-12-31'";
		
		return (List<PaseServicioMedico>)jdbcTemplate.query(sql, new Object[]{idTercero, id}, new RowMapper<PaseServicioMedico>(){

			@Override
			public PaseServicioMedico mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				PaseServicioMedico paseServicioMedico = new PaseServicioMedico();
				paseServicioMedico.setNumFolio(rs.getString("NUMFOLIO"));
				paseServicioMedico.setfPaseAtencion(rs.getDate("FPASEATENCION"));
				paseServicioMedico.setNumeroReporte(rs.getLong("NUMEROREPORTE"));
				paseServicioMedico.setNumeroSiniestro(rs.getLong("NUMEROSINIESTRO")>0?rs.getLong("NUMEROSINIESTRO"):null);
				paseServicioMedico.setPoliza(rs.getString("POLIZA"));
				paseServicioMedico.setNumeroInciso(rs.getInt("INCISO"));
				paseServicioMedico.setCoberturaAfectada(rs.getString("COBERTURAAFECTADA"));
				paseServicioMedico.setInstitucionHospitalaria(rs.getString("INSTITUCIONHOSPITALARIA"));
				paseServicioMedico.setTelefonoHospital(rs.getString("TELEFONOHOSPITAL"));
				paseServicioMedico.setDomicilioHospital(new Domicilio(rs.getString("PAIS"), rs.getString("ESTADO"), rs.getString("CIUDAD"), rs.getString("COLONIA"), rs.getString("CODIGOPOSTAL"), rs.getString("CALLENUMERO")));
				paseServicioMedico.setCia(rs.getString("NOM_CIA_ASEG"));
				paseServicioMedico.setEdad(rs.getInt("EDAD"));
				paseServicioMedico.setTelefonos(rs.getString("TELEFONOS"));
				paseServicioMedico.setfOcurrio(rs.getDate("FSINIESTRO"));
				paseServicioMedico.setDescripcionLesiones(rs.getString("DESCRIPCIONLESIONES"));
				paseServicioMedico.setAtencionHastaCantidad(rs.getDouble("ATENCIONHASTACANTIDAD"));
				paseServicioMedico.setNombreLesionado(rs.getString("NOMBRELESIONADO"));
				return paseServicioMedico;
			}
			
		});
	}

	private List<ValeGrua> getValeGrua(Long id, Long idTercero) {
		String sql="SELECT SIN_TERCERO.FOLIO_TERCERO NUMFOLIO, \r\n" +
   " sin_tercero.f_ini_reg FPASEATENCION, \r\n" +
   " RECLAMACION.NUM_REP_CABINA NUMEROREPORTE, \r\n" + 
   " RECLAMACION.NUM_RECLAMACION NUMEROSINIESTRO, \r\n" + 
   " RECLAMACION.FH_OCURRIDO FSINIESTRO, \r\n" +
   " LPAD(TO_CHAR(POLIZA.ID_CENTRO_EMIS),3,0) || '-' ||LPAD(TO_CHAR(POLIZA.NUM_POLIZA),12,0) || '-' || LPAD(TO_CHAR(POLIZA.NUM_RENOV_POL),2,0) POLIZA, \r\n" + 
   " SIN_INCISO_RECL.ID_INCISO INCISO, \r\n" + 
   " UPPER(SIN_TERCERO.NOM_TERCERO) NOMBREINTERESADO, \r\n" +
   " UPPER(MARCA.NOM_MARCA || ' - ' || DECODE(TRIM(MARCA.CVEL_T_BIEN), 'CAMN', 'CAMIONES', MARCA.CVEL_T_BIEN)) AS  MARCAVEHICULO, \r\n" +
   " UPPER(sin_tercero.desc_tipo_estilo) TIPOVEHICULO, \r\n" +
   " SIN_TERCERO.ID_MODELO_AUTO MODELOVEHICULO, \r\n" + 
   " UPPER(SIN_TERCERO.PLACA) PLACASVEHICULO, \r\n" +
   " UPPER(SIN_TERCERO.NUM_SERIE) NUMSERIEVEHICULO, \r\n" +
   " UPPER(SIN_TERCERO.DESC_COLOR) COLOR, \r\n" +
   " '' VEHICULOASEGURADOOTERCERO, \r\n" +
   " '' SERVICIOMANIOBRAOARRASTRE, \r\n" +
   " '' RECOGEREN, \r\n" +
   " '' ENTREGAREN \r\n" +
   " FROM SEYCOS.SIN_RECLAMACION RECLAMACION \r\n" +
   " INNER JOIN SEYCOS.SIN_TERCERO ON (RECLAMACION.ID_REPORTE_SIN = SIN_TERCERO.ID_REPORTE_SIN AND SIN_TERCERO.ID_TERCERO = ?) \r\n" +
   " INNER JOIN SEYCOS.POL_POLIZA POLIZA ON (POLIZA.ID_COTIZACION = RECLAMACION.ID_COTIZACION AND POLIZA.ID_VERSION_POL = RECLAMACION.ID_VERSION_POL) \r\n" +
   " INNER JOIN SEYCOS.SIN_INCISO_RECL ON (RECLAMACION.ID_REPORTE_SIN = SIN_INCISO_RECL.ID_REPORTE_SIN AND RECLAMACION.ID_COTIZACION=SIN_INCISO_RECL.ID_COTIZACION) \r\n" +
   " LEFT JOIN SEYCOS.ING_MARCA MARCA ON (MARCA.ID_MARCA = SIN_TERCERO.ID_MARCA) \r\n" + 
   " WHERE RECLAMACION.ID_REPORTE_SIN = ? \r\n" +
   " AND RECLAMACION.F_TER_REG = DATE '4712-12-31' \r\n" +
   " AND SIN_TERCERO.F_TER_REG = DATE '4712-12-31' \r\n" +
   " AND SIN_INCISO_RECL.F_TER_REG = DATE '4712-12-31'";
		
				return (List<ValeGrua>)jdbcTemplate.query(sql, new Object[]{idTercero, id}, new RowMapper<ValeGrua>(){

			@Override
			public ValeGrua mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				ValeGrua valeGrua = new ValeGrua();
				valeGrua.setNumFolio(rs.getString("numFolio"));
				valeGrua.setfVale(rs.getDate("FPASEATENCION"));
				valeGrua.setNumeroReporte(rs.getLong("numeroReporte"));
				valeGrua.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
				valeGrua.setfSiniestro(rs.getDate("fSiniestro"));
				valeGrua.setPoliza(rs.getString("poliza"));
				valeGrua.setNumeroInciso(rs.getInt("INCISO"));
				valeGrua.setMarcaVehiculo(rs.getString("MARCAVEHICULO"));
				valeGrua.setTipoVehiculo(rs.getString("TIPOVEHICULO"));
				valeGrua.setModeloVehiculo(rs.getInt("MODELOVEHICULO"));
				valeGrua.setPlacasVehiculo(rs.getString("PLACASVEHICULO"));
				valeGrua.setNumSerieVehiculo(rs.getString("NUMSERIEVEHICULO"));
				valeGrua.setColor(rs.getString("COLOR"));
				valeGrua.setVehiculoAseguradoOTercero(rs.getString("VEHICULOASEGURADOOTERCERO"));
				valeGrua.setNombreInteresado(rs.getString("NOMBREINTERESADO"));
				valeGrua.setServicioManiobraOArrastre(rs.getString("SERVICIOMANIOBRAOARRASTRE"));
				valeGrua.setRecogerEn(rs.getString("RECOGEREN"));
				valeGrua.setEntregarEn(rs.getString("ENTREGAREN"));
				return valeGrua;
			}
			
		});
	}
	
	private List<PaseServicioObraCivil> getPaseServicioObraCivil(Long id, Long idTercero) {
		String sql="select sin_tercero.folio_tercero NUMFOLIO, \r\n" +
		   " sin_tercero.f_ini_reg FPASEATENCION, \r\n" +
		   " RECLAMACION.FH_OCURRIDO FSINIESTRO, \r\n" +
		   " RECLAMACION.NUM_REP_CABINA NUMEROREPORTE, \r\n" + 
		   " RECLAMACION.NUM_RECLAMACION NUMEROSINIESTRO, \r\n" + 
		   " LPAD(TO_CHAR(POLIZA.ID_CENTRO_EMIS),3,0) || '-' ||LPAD(TO_CHAR(POLIZA.NUM_POLIZA),12,0) || '-' || LPAD(TO_CHAR(POLIZA.NUM_RENOV_POL),2,0) POLIZA, \r\n" + 
		   " SIN_INCISO_RECL.id_inciso INCISO, \r\n" + 
		   " \r\n" +   
		   " UPPER(POB_PAIS.DESC_POBLACION) PAIS, \r\n" +
		   " UPPER(POB_ESTADO.DESC_POBLACION) ESTADO, \r\n" +
		   " 	 UPPER(POB_CIUDAD.DESC_POBLACION) CIUDAD, \r\n" +
		   " 	 UPPER(CALLE_NUMERO) CALLENUMERO, \r\n" +
		   " 	 CODIGO_POSTAL CODIGOPOSTAL, \r\n" +
		   " 	 UPPER(COLONIA) COLONIA, \r\n" + 
		   " UPPER(SIN_TERCERO.nom_Tercero) nombreInteresado, \r\n" +
		   " UPPER(trim(nvl(TELEF_1, '')||' '||nvl(TELEF_2, ''))) TELEFONOS, \r\n" +   
		   " UPPER(SIN_TERCERO.AREAS_DAN) descripcionBienDaniado, \r\n" +
		   " Sin_Tercero.Nom_Ing_Asig nombreIngenieroHacerReparacion, \r\n" +
		   " (Select (Case When Cli.Telef_Oficina Is Not Null Then 'Oficina: ' || Cli.Telef_Oficina End ) \r\n" +
		   " || (Case When Cli.Telef_Fax Is Not Null Then ' Fax: ' || Cli.Telef_Fax End ) \r\n" +
		   " || (Case when Cli.Telef_Casa Is Not Null Then ' Casa: ' || Cli.Telef_Casa End ) as telefonos \r\n" +
		   " From SEYCOS.Ins_Prest_Serv P, Persona Cli, SEYCOS.Ing_Valor_Campo Ivc Where Ivc.Id_Campo = 2558 And Ivc.Val_Campo='ING' \r\n" + 
		   " And Cli.Id_Persona = P.Id_Persona And P.Sit_Prest_Serv = 'A' And P.Cve_T_Prest_Serv = Ivc.Val_Campo \r\n" +
		   " And Cli.Nombre = Sin_Tercero.Nom_Ing_Asig \r\n" +
		   " ) as telefonoIngeniero \r\n" +
		   " FROM SEYCOS.SIN_RECLAMACION RECLAMACION \r\n" +
		   " INNER JOIN SEYCOS.sin_tercero on (reclamacion.ID_REPORTE_SIN = sin_tercero.ID_REPORTE_SIN and sin_tercero.id_tercero = ?) \r\n" +
		   " INNER JOIN SEYCOS.POL_POLIZA POLIZA ON (POLIZA.ID_COTIZACION = RECLAMACION.ID_COTIZACION AND POLIZA.ID_VERSION_POL = RECLAMACION.ID_VERSION_POL) \r\n" +
		   " INNER JOIN SEYCOS.SIN_INCISO_RECL ON (RECLAMACION.ID_REPORTE_SIN = SIN_INCISO_RECL.ID_REPORTE_SIN AND RECLAMACION.ID_COTIZACION=SIN_INCISO_RECL.ID_COTIZACION) \r\n" +
		   " INNER JOIN SEYCOS.SIN_DOMICILIO DOM ON (SIN_TERCERO.ID_DOMICILIO_BIEN = DOM.ID_DOMICILIO_SIN) \r\n" +
		   " INNER JOIN SEYCOS.POBLACION PO on po.cve_poblacion = dom.cve_estado \r\n" +
		   " INNER JOIN SEYCOS.POBLACION POB_PAIS ON DOM.CVE_PAIS = POB_PAIS.CVE_POBLACION \r\n" +
		   " INNER JOIN SEYCOS.POBLACION POB_ESTADO ON DOM.CVE_ESTADO = POB_ESTADO.CVE_POBLACION \r\n" +
		   " LEFT JOIN SEYCOS.POBLACION POB_CIUDAD ON DOM.CVE_CIUDAD = POB_CIUDAD.CVE_POBLACION \r\n" +
		   " WHERE RECLAMACION.ID_REPORTE_SIN = ? \r\n" +
		   " AND RECLAMACION.F_TER_REG = DATE '4712-12-31' \r\n" +
		   " AND sin_tercero.F_TER_REG = DATE '4712-12-31' \r\n" +
		   " AND SIN_INCISO_RECL.f_ter_reg = DATE '4712-12-31'";
		
		return (List<PaseServicioObraCivil>)jdbcTemplate.query(sql, new Object[]{idTercero, id}, new RowMapper<PaseServicioObraCivil>(){

			@Override
			public PaseServicioObraCivil mapRow(ResultSet rs, int rowNum)
					throws SQLException {
				PaseServicioObraCivil paseServicioObraCivil = new PaseServicioObraCivil();
				paseServicioObraCivil.setNumFolio(rs.getString("numFolio"));
				paseServicioObraCivil.setfSiniestro(rs.getDate("fSiniestro"));
				paseServicioObraCivil.setfPaseDeAtencion(rs.getDate("FPASEATENCION"));
				paseServicioObraCivil.setNumeroReporte(rs.getLong("numeroReporte"));
				paseServicioObraCivil.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
				paseServicioObraCivil.setPoliza(rs.getString("poliza"));
				paseServicioObraCivil.setInciso(rs.getInt("inciso"));
				paseServicioObraCivil.setNombreIngenieroAsignadoHacerReparacion(rs.getString("nombreIngenieroHacerReparacion"));
				paseServicioObraCivil.setTelefonoIngeniero(rs.getString("telefonoIngeniero"));
				paseServicioObraCivil.setDescripcionBienDaniado(rs.getString("descripcionBienDaniado"));
				paseServicioObraCivil.setDomicilioBienDaniado(new Domicilio(rs.getString("PAIS"), rs.getString("ESTADO"), 
						rs.getString("CIUDAD"), rs.getString("COLONIA"), rs.getString("CODIGOPOSTAL"), rs.getString("CALLENUMERO")));				
				paseServicioObraCivil.setNombreInteresado(rs.getString("nombreInteresado"));	
				paseServicioObraCivil.setTelefono(rs.getString("TELEFONOS"));
				return paseServicioObraCivil;
			}
			
		});
	}

	public byte[] getBytesPase(TerceroParameter tercero, String tipoPase, String usuario) {
		byte[] pdfPase = null;

		try {
			InputStream caratula = null;
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("usuario", usuario);
			parameters.put("tipo", tipoPase);
			parameters.put("idCobertura", tercero.getCoberturaId());
			JasperReport report = null;
			
			JRBeanCollectionDataSource jrDataSource = null;
			
			if(tipoPase.equals("DDS")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeclaracionDeSiniestro.jrxml");
				
				InputStream terceros =  this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/Terceros.jrxml");
				JasperReport subreport = JasperCompileManager.compileReport(terceros);
				parameters.put("SUBREPORT_DIR", subreport);
				
				jrDataSource = new JRBeanCollectionDataSource(this.getPaseDeclaracionSiniestro(tercero.getReporteSiniestroId()));
			}else if(tipoPase.equals("PAT")){
				if(tercero.getCoberturaId().intValue() == ConfigCobertura.DM.value){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATallerAsegurado.jrxml");
					jrDataSource = new JRBeanCollectionDataSource(this.getPaseAtencionATallerAsegurado(tercero.getReporteSiniestroId(), tercero.getId()));
				}else {
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATercero.jrxml");
					jrDataSource = new JRBeanCollectionDataSource(this.getPaseAtencionATercero(tercero.getReporteSiniestroId(), tercero.getId()));
				}
			}else if(tipoPase.equals("RAC")){
				if(tercero.getCoberturaId().intValue() == ConfigCobertura.RCV.value){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATercero.jrxml");
					jrDataSource = new JRBeanCollectionDataSource(this.getPaseAtencionATercero(tercero.getReporteSiniestroId(), tercero.getId()));
				} else {
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					jrDataSource = new JRBeanCollectionDataSource(this.getPaseServicioMedico(tercero.getReporteSiniestroId(), tercero.getId()));
				}
			}else if(tipoPase.equals("PDD")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionParaPagoDeDanios.jrxml");
				jrDataSource = new JRBeanCollectionDataSource(this.getAtencionParaPagosDeDanios(tercero.getReporteSiniestroId(), tercero.getId()));
			}else if(tipoPase.equals("GMO") || tipoPase.equals("GMT")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
				jrDataSource = new JRBeanCollectionDataSource(this.getPaseServicioMedico(tercero.getReporteSiniestroId(), tercero.getId()));
			}else if(tipoPase.equals("OCI")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioObraCivil.jrxml");
				jrDataSource = new JRBeanCollectionDataSource(this.getPaseServicioObraCivil(tercero.getReporteSiniestroId(), tercero.getId()));
			}else{
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseValeDeGrua.jrxml");
				jrDataSource = new JRBeanCollectionDataSource(this.getValeGrua(tercero.getReporteSiniestroId(), tercero.getId()));
			}
			
			report = JasperCompileManager.compileReport(caratula);
			pdfPase = JasperRunManager.runReportToPdf(report, parameters, jrDataSource);
			
		} catch (JRException ex) {
			log.error("Falla jasperreport opcion: " + tipoPase , ex);
			throw new RuntimeException(ex);
		}
		return pdfPase;
	}

	@Override
	public List<TipoDocumentoSiniestro> getTiposDocumentosSiniestro() {
		TypedQuery<TipoDocumentoSiniestro> query = em.createQuery("select m from TipoDocumentoSiniestro m order by m.id", TipoDocumentoSiniestro.class);
		return query.getResultList();
	}

	@Override
	public void saveDocumentoSiniestro(SaveDocumentoSiniestroParameter parameter) {
		Set<ConstraintViolation<SaveDocumentoSiniestroParameter>> constraintViolations = validator.validate(parameter);
		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
		}
		
		ErrorBuilder eb = new ErrorBuilder();
		ReporteSiniestro reporteSiniestro = getReporteSiniestro(parameter.getReporteSiniestroId());
		if (reporteSiniestro == null) {
			eb.addFieldError("reporteSiniestroId", "No existe el reporte de siniestro.");
		}
		
		TipoDocumentoSiniestro tipoDocumentoSiniestro = em.find(TipoDocumentoSiniestro.class, parameter.getTipoDocumentoSiniestroId());
		
		if (tipoDocumentoSiniestro == null) {
			eb.addFieldError("idTipoDocumentoSiniestro", "No existe el tipo de documento siniestro.");
		}
		
		if (eb.hasErrors()) {
			throw new ApplicationException(eb);
		}
		
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		byte[] fileBytes;
		try {
			fileBytes = FileUtils.readFileToByteArray(parameter.getFile());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		transporteImpresionDTO.setByteArray(fileBytes);
		
		String expediente = crearExpedienteFortimax(reporteSiniestro);
		String[] documentosNombre = tipoDocumentoSiniestro.getFortimaxDocumentoNombre().split(SEPARADOR);
		String documentoNombre = documentosNombre[0] + "." + FilenameUtils.getExtension(parameter.getFileFileName());
		String[] rutaDocumento = tipoDocumentoSiniestro.getFortimaxDocumentoDirectorio().split(SEPARADOR);
		log.info("ExpedienteId:" + expediente);
		log.info("documentoNombre:" + documentoNombre);
		log.info("Nombre de la gaveta:" + GAVETA_DOC_SINIESTROS_FORTIMAX);
		log.info("idReporteSiniestro:" + parameter.getReporteSiniestroId());
		log.info("ruta del documento:" + rutaDocumento[0]);
		try {
			fortimaxV2Service.uploadFile(documentoNombre, transporteImpresionDTO, GAVETA_DOC_SINIESTROS_FORTIMAX, new String[] {expediente}, 
					rutaDocumento[0]);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}		
	}

	/**
	 * Crea un expediente para el <code>ReporteSiniestro</code>. Se crea el expediente aunque
	 * ya pudiera existir esto para evitar que se llegue a subir un documento en una gaveta no existente
	 * lo cual ocasionaria un error.
	 * @param reporteSiniestro
	 * @return el id del expediente creado.
	 */
	private String crearExpedienteFortimax(ReporteSiniestro reporteSiniestro) {
		String id = reporteSiniestro.getExpedienteFortimaxId();
		boolean newId = false;
		if (id == null) {
			newId = true;
			Calendar cal = Calendar.getInstance();
			cal.setTime(reporteSiniestro.getFechaOcurrido());
			int año = cal.get(Calendar.YEAR);	
			if (reporteSiniestro.getNumeroSiniestro() == null) {
				id = "R" + año + "-" + reporteSiniestro.getNumeroReporte();
			} else {
				id = año + "-" + reporteSiniestro.getNumeroSiniestro();
			}			
		}
		String[] strReturns;
		try {
			strReturns = fortimaxV2Service.generateExpedient(GAVETA_DOC_SINIESTROS_FORTIMAX, new String[]{id});
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		
		if (strReturns[0].equals("false")) {
			throw new RuntimeException("No se pudo crear el expediente en Fortimax para el reporte de siniestro.");
		}
		//Si se genero un nuevo id entonces almacenarlo.
		if (newId) {
			reporteSiniestro.setExpedienteFortimaxId(id);
			guardarExpedienteFortimaxId(reporteSiniestro);
		}
		return id;
	}
	
	/**
	 * Guarda el campo expedienteFortimaxId del reporteSiniestro.
	 * @param reporteSiniestro
	 */
	private void guardarExpedienteFortimaxId(ReporteSiniestro reporteSiniestro) {
		Map<String, Object> p = new HashMap<String, Object>();
		p.put("reporteSiniestroId", reporteSiniestro.getId());
		p.put("expedienteFortimaxId", reporteSiniestro.getExpedienteFortimaxId());
		nJdbcTemplate.update("update seycos.sin_reclamacion set ID_EXPEDIENTE_FORTIMAX = :expedienteFortimaxId " +
				" where id_reporte_sin = :reporteSiniestroId and f_ter_reg = DATE '4712-12-31'", p);
	}
	
	public String convertirReclamacion(ReporteSiniestroParameter reporteSiniestro, String claveUsuario){
		Map<String, Object> sqlParameter = new HashMap<String, Object>();
		sqlParameter.put("pId_Reporte_sin", reporteSiniestro.getId());
		sqlParameter.put("pCve_Usuario", claveUsuario);

		Map<String, Object> execute = this.convierteReclamacion.execute(sqlParameter);
		
		String idCodResp = (String)execute.get("pId_Cod_Resp");
		if(!idCodResp.equals("0")){
			throw new ApplicationException((String)execute.get("pDesc_Codigo"));
		}else {
			try{
				Map<String, Object> sqlParameterNHgs = new HashMap<String, Object>();
				sqlParameterNHgs.put("pId_Reporte_sin", reporteSiniestro.getId());
			
				Map<String, Object> executeNHgs = this.notificarHgs.execute(sqlParameterNHgs);
				String idCodRespNHgs = (String)executeNHgs.get("pId_Cod_Resp");
				if(!idCodRespNHgs.equals("0")){
					log.error("Errar al notificar a Hgs: "+(String)execute.get("pDesc_Codigo"));
				}
			}catch(Exception e){
				log.error("Falla notifica hgs: ", e);
			}
		}
		return execute.get("pNum_Folio_Rep") + "/" + execute.get("pAnio_Rep");
	}
	
}
