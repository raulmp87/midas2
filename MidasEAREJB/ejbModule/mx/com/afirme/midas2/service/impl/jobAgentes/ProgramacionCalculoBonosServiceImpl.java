package mx.com.afirme.midas2.service.impl.jobAgentes;
import static mx.com.afirme.midas2.utils.CommonUtils.compareOnlyDatesWithoutTime;
import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.service.bonos.ProgramacionBonoService;
import mx.com.afirme.midas2.service.calculos.CalculoBonosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCalculoBonosService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MidasException;
/**
 * Servicio para obtener las tareas programadas y ejecutarlas con Quartz para generar
 * previews automaticos de bonos
 * @author vmhersil
 *
 */
@Stateless
public class ProgramacionCalculoBonosServiceImpl implements ProgramacionCalculoBonosService{
	private CalculoBonosService calculoBonoService;
	private EntidadService entidadService;
	private ProgramacionBonoService programacionBonoService;
	@Resource	
	private TimerService timerService;
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProgramacionCalculoBonosServiceImpl.class);
	
	public static enum conceptoEjecucionAutomatica{
		BONOS("BONOS"),PROVISION("PROVISION");
		private String value;
		private conceptoEjecucionAutomatica(String value){
			this.value=value;
		}
		public String getValue(){
			return value;
		}
	}
	
	@Override
	public void executeTasks() {
		LOG.info("Ejecutando ProgramacionCalculoBonosService.executeTasks()...");
		List<TareaProgramada> tasks=getTaskToDo(conceptoEjecucionAutomatica.BONOS.getValue());
		List<ProgramacionBono> bonosProgramados=new ArrayList<ProgramacionBono>();
		Date currentDate=new Date();
		if(!isEmptyList(tasks)){
			for(TareaProgramada task:tasks){
				if(compareOnlyDatesWithoutTime(currentDate, task.getFechaEjecucion())){
					try{
						Long idProgramacion=task.getId();
						ProgramacionBono programacion=programacionBonoService.loadById(idProgramacion);
						
						if(isNull(programacion)){
							onError("No existe la programacion con clave ["+idProgramacion+"]");
						}
						bonosProgramados.add(programacion);
//						ValorCatalogoAgentes tipoConfiguracion=programacion.getTipoConfiguracion();
//						Integer esNegocio=null;
//						if(isNotNull(tipoConfiguracion) && "NEGOCIO ESPECIAL".equalsIgnoreCase(tipoConfiguracion.getValor().trim())){
//							esNegocio=1;
//						}else if(isNotNull(tipoConfiguracion) && "CONFIGURACION BONO".equalsIgnoreCase(tipoConfiguracion.getValor().trim())){
//							esNegocio=0;
//						}
//						ValorCatalogoAgentes modoEjecucion=
//						calculoBonoService.ejecutarCalculoA
//						calculoBonoService.generarCalculo(idProgramacion,null);
						
					}catch(Exception e){
						e.printStackTrace();
					}
				}
			}
			if(!isEmptyList(bonosProgramados)){
				try {
					calculoBonoService.ejecutarCalculosAutomaticos(bonosProgramados,conceptoEjecucionAutomatica.BONOS.getValue());
				} catch (MidasException e) {
					e.printStackTrace();
				}
			}
		}
	}

	@Override
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		List<TareaProgramada> tasks=new ArrayList<TareaProgramada>();
		try{
			List<ProgramacionBono> list=calculoBonoService.obtenerConfiguracionesAutomaticasActivas(conceptoEjecucionAutomatica);
			if(!isEmptyList(list)){
				for(ProgramacionBono programacion:list){
					TareaProgramada tarea=new TareaProgramada();
					tarea.setId(programacion.getId());
					tarea.setFechaEjecucion(programacion.getFechaEjecucion());
					tasks.add(tarea);
				}
			}
		}catch(MidasException e){
			e.printStackTrace();
		}
		return tasks;
	}
	
	public void initialize() {
		String timerInfo = "TimerProgramacionCalculoBonos";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 0/1 1/1 * ?
				expression.minute(0);
				expression.hour("0/1");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerProgramacionCalculoBonos", false));
				
				LOG.info("Tarea TimerProgramacionCalculoBonos configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerProgramacionCalculoBonos");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerProgramacionCalculoBonos:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		executeTasks();
	}
	
	/**
	 * ===================================================
	 * Sets and gets
	 * ===================================================
	 */
	@EJB
	public void setCalculoBonoService(CalculoBonosService calculoBonoService) {
		this.calculoBonoService = calculoBonoService;
	}
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@EJB
	public void setProgramacionBonoService(ProgramacionBonoService programacionBonoService) {
		this.programacionBonoService = programacionBonoService;
	}
}
