package mx.com.afirme.midas2.service.impl.negocio.motordecision;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas2.dao.negocio.motordecision.MotorDecisionAtributosDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle.EstatusDetalle;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO.ValorTipo;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO.EstatusDescriDetalle;
import mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionServiceImpl;
import mx.com.afirme.midas2.service.negocio.motordecision.MotorDecisionAtributosService;

import org.apache.log4j.Logger;

@Stateless
public class MotorDecisionAtributosServiceImpl implements MotorDecisionAtributosService{

	public static final Logger LOG = Logger.getLogger(NegocioCobPaqSeccionServiceImpl.class);
	
	@EJB
	private MotorDecisionAtributosDao motorDecisionAtributosDao;
	
	/**
	 * Método implementado genérico para la consulta de valores de configuración
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link MotorDecisionDTO} motorDecisionDto
	 * @return {@link ResultadoMotorDecisionAtributosDTO}
	 */
	@Override
	public List<ResultadoMotorDecisionAtributosDTO> consultaValoresService(MotorDecisionDTO motorDecisionDto) {
		
		List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosReqList = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
		List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosNoReqList = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
				
		this.separaFiltros(motorDecisionDto.getFiltrosDTO(), filtrosReqList, filtrosNoReqList);
		List<ResultadoMotorDecisionAtributosDTO> resultadoValores  = motorDecisionAtributosDao.consultaValoresDao(motorDecisionDto.getTipoConsulta(), motorDecisionDto.getIdToNegocio(), filtrosReqList, filtrosNoReqList);
		
		return resultadoValores;
	}
	
	private void separaFiltros(List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosDto, List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosReqList, List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosNoReqList){
		for(MotorDecisionDTO.FiltrosMotorDecisionDTO filtroMotor : filtrosDto){
			if(filtroMotor.getRequerido()){
				filtrosReqList.add(filtroMotor);
			}else{
				filtrosNoReqList.add(filtroMotor);
			}
		}
	}

	/**
	 * Método implementado genérico para la validación de sumas aseguradas
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link MotorDecisionDTO} motorDecisionDto
	 * @return int - Rgresa el entero depediendo del tipo de error. 
	 */
	@Override
	public int validaSumasDeducibles(MotorDecisionDTO filtrosMotorDecisionDto, Double monto, int tipoValida) {
		int valResultado = 0;
		List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = this.consultaValoresService(filtrosMotorDecisionDto);
		
		if(!resultadoBusqueda.isEmpty()){
			List<NegocioCobPaqSeccion> resultado = (List<NegocioCobPaqSeccion>)(Object)resultadoBusqueda.get(0).getValores();
			
			/*
			 * TIPO_VALIDA_DEDUCIBLE = 1
			 * TIPO_VALIDA_SUMAS = 2
			 * TIPO_VALIDA_DECISION = 3
			 */
			if(resultado != null && !resultado.isEmpty()){
				switch (tipoValida) {
					case 1:
						//Si es Responsabilidad Civil o Responsabilidad Civil Viajero, no valida los deducibles
						if(resultado.get(0).getCoberturaDTO().getClaveTipoCalculo() == null
								|| resultado.get(0).getCoberturaDTO().getClaveTipoCalculo().isEmpty()
								|| (!resultado.get(0).getCoberturaDTO().getClaveTipoCalculo().equals("RC")
								&& !resultado.get(0).getCoberturaDTO().getClaveTipoCalculo().equals("RCV"))){
							boolean existeDeducible = false;
							if(!resultado.get(0).getNegocioDeducibleCobList().isEmpty()){
								for(NegocioDeducibleCob deducible : resultado.get(0).getNegocioDeducibleCobList()){
									if(deducible.getValorDeducible().toString().equals(monto.toString())){
										existeDeducible = true;
										break;
									}
								}
								if(!existeDeducible){
									valResultado = 12; //El monto no coincide con los deducibles configurados para el negocio.
								}
							}else{
								valResultado = 13; //No existen deducbles configurados para el negocio.
							}
						}
						break;
					case 2:
						boolean existeSuma = false;
						if(resultado.get(0).getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_COMERCIAL)){
							if(monto != null && monto > 0){
								valResultado = 14; //NO ERROR - El valor a considerar es el cofigurado al valor comercial y no al capturado.
							}
						}
						if(resultado.get(0).getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CONVENIDO)){
							if(monto != null && monto > 0){
								valResultado = 15; //NO ERROR - El valor a considerar es el configurado al valor convenido y no al capturado.  
							}
						}
						
						if(	resultado.get(0).getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO) ||
							resultado.get(0).getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA) ||
							resultado.get(0).getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CARATULA) ){
							
							if(monto != null && monto > 0){
								if(!resultado.get(0).getNegocioCobSumAseList().isEmpty()){
									for(NegocioCobSumAse sumaAse : resultado.get(0).getNegocioCobSumAseList()){
										if(sumaAse.getValorSumaAsegurada().toString().equals(monto.toString())){
											existeSuma = true;
											break;
										}
									}
									if(!existeSuma){ 
										valResultado = 16; //El monto no coincide con las sumas aseguradas configuradas.
									}
								}else{
									if( (resultado.get(0).getValorSumaAseguradaMin() != null && resultado.get(0).getValorSumaAseguradaMin() > 0) &&
										(resultado.get(0).getValorSumaAseguradaMax() != null && resultado.get(0).getValorSumaAseguradaMin() > 0)){
										if(monto < resultado.get(0).getValorSumaAseguradaMin() || monto > resultado.get(0).getValorSumaAseguradaMax()){
											valResultado = 17; // El monto capturado esta fuera del rango configurado.
										}
									}
								}
							}else{
								valResultado = 18; //El monto capturado debe ser mayor a cero para poder ser validado.
							}
						}
						break;
					case 3:
						if(!resultado.get(0).getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_AMPARADA)){
							valResultado = 19; // La cobertura no esta configurada como amparada.
						}
						break;
					default:
						break;
				}
			}else{
				valResultado = 11; //No existen valores configurados para realizar las validaciones.
			}
		}else{
			valResultado = 21; //No existen coberturas coincidentes con los filtros proporcionados.
		}
		
		return valResultado;
	}
	
	/**
	 * Método implementado genérico para la validación de derechos
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param {@link MotorDecisionDTO} motorDecisionDto
	 * @return EstatusDescriDetalle - Objeto que encapsula el codigo de estatus y la descripción en caso de error 
	 */
	
	@Override
	public EstatusDescriDetalle validaDerechos(MotorDecisionDTO filtroDerechos, Double montoDerecho) {
		CargaMasivaServicioPublicoEstatusDescriDetalleDTO infoItem = new CargaMasivaServicioPublicoEstatusDescriDetalleDTO();
		EstatusDescriDetalle estatusDescriDetalle = infoItem.new EstatusDescriDetalle();
		
		List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = this.consultaValoresService(filtroDerechos);
		List<Object> derechosList = resultadoBusqueda.get(0).getValores();
		
		boolean existe = false;
		
		if(derechosList != null && !derechosList.isEmpty()){
			for(Object item : derechosList){
				if(Double.parseDouble(((ValorTipo)item).getValor()) == montoDerecho){
					estatusDescriDetalle.setEstatusDetalle(EstatusDetalle.POR_EMITIR.toString());
					estatusDescriDetalle.setDescriEstatus("");
					existe = true;
					break;
				}
			}
			if(!existe){
				estatusDescriDetalle.setEstatusDetalle(EstatusDetalle.ERROR.toString());
				estatusDescriDetalle.setDescriEstatus("El valor de derecho capturado no coincide con los capturados en la configuraci\u00F3n.");
			}
		}
		
		return estatusDescriDetalle;
	}
}
