package mx.com.afirme.midas2.service.impl.bitemporal.endoso.cotizacion.auto.tipoEndoso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.documentoanexo.DocumentoAnexoTipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaFacadeRemote;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoDTO;
import mx.com.afirme.midas.producto.documentoanexo.DocumentoAnexoProductoFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.BitemporalDocAnexoCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.DocAnexoCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.DocAnexoCotContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.tipoEndoso.EndosoInclusionAnexoCotService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.joda.time.DateTime;

@Stateless
public class EndosoInclusionAnexoCotServiceImpl implements EndosoInclusionAnexoCotService{

	
    
    
   @Override
	public void accionInclusionAnexo(BitemporalDocAnexoCot docAnexo, BitemporalCotizacion cotizacion, DateTime validoEn, String accion) {    	
	   
	   if (accion.equals("inserted")) {
		   	Usuario usuario = usuarioService.getUsuarioActual();
		   	docAnexo.getValue().setNombreUsuarioCreacion(usuario.getNombreUsuario());
		   	docAnexo.getValue().setFechaCreacion(new Date());
		   	docAnexo.getValue().setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		   	docAnexo.getValue().setNombreUsuarioModificacion(usuario.getNombreUsuario());				
		   	docAnexo.getValue().setFechaModificacion(new Date());
		   	docAnexo.getValue().setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		   	docAnexo.getValue().setClaveSeleccion((short) 1);

		   cotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, cotizacion.getContinuity().getId(), validoEn);
		   docAnexo.getContinuity().setParentContinuity(cotizacion.getContinuity());
		   entidadBitemporalService.saveInProcess(docAnexo, validoEn);
	   } else if (accion.equals("deleted")) {
		   docAnexo = entidadBitemporalService.getInProcessByKey(DocAnexoCotContinuity.class, 
				   docAnexo.getContinuity().getId(), validoEn);
		   entidadBitemporalService.remove(docAnexo, validoEn);
	   }
	   
	   //entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalDocAnexoCot.class, cotizacion.getContinuity().getId(), 
			  // cotizacion.getContinuity().getId(), validoEn);
	}		
	
	
	public List<BitemporalDocAnexoCot> prepareEntidadBitemporalEndosoDocAnexo(String anexos, DateTime validoEn) {
		List<BitemporalDocAnexoCot> lstBiEndosoAnexo = new ArrayList<BitemporalDocAnexoCot>();
		if (anexos != null && !"".equals(anexos)) {
			String[] arrayAnexos = anexos.split("-");
			
			BitemporalDocAnexoCot biDocAnexos = null;
			Usuario usuario = usuarioService.getUsuarioActual();
		
			for (int i = 0; i <= arrayAnexos.length - 1; i+= 8) {
				
				if ("".equals(arrayAnexos [i + 1]) ) {
					biDocAnexos = new BitemporalDocAnexoCot();
					biDocAnexos.getValue().setNombreUsuarioCreacion(usuario.getNombreUsuario());
					biDocAnexos.getValue().setFechaCreacion(new Date());
					biDocAnexos.getValue().setCodigoUsuarioCreacion(usuario.getNombreUsuario());
				} else {
					biDocAnexos = entidadBitemporalService.getByKey(DocAnexoCotContinuity.class, 
							Long.parseLong(arrayAnexos [i + 1]), validoEn);
				}
			
				biDocAnexos.getValue().setDescripcionDocumentoAnexo(arrayAnexos [i + 3]);
				biDocAnexos.getValue().setNombreUsuarioModificacion(usuario.getNombreUsuario());				
				biDocAnexos.getValue().setFechaModificacion(new Date());
				biDocAnexos.getValue().setCodigoUsuarioModificacion(usuario.getNombreUsuario());
				biDocAnexos.getValue().setClaveObligatoriedad(Short.valueOf(arrayAnexos [i + 4]));
				biDocAnexos.getValue().setClaveSeleccion((short) 1);
				biDocAnexos.getValue().setOrden(Integer.valueOf(arrayAnexos [i + 5]));
				biDocAnexos.getValue().setClaveTipo(Integer.valueOf(arrayAnexos [i + 6]));
				biDocAnexos.getValue().setCoberturaId(Integer.valueOf(arrayAnexos [i + 7]));
				
				lstBiEndosoAnexo.add(biDocAnexos);
			}
		}
		
		return lstBiEndosoAnexo;
	}
	
	
	@Override
	public Collection<BitemporalDocAnexoCot> getLstBiDocAnexosAsociados(Long cotizacionContinuityId, DateTime validoEn) {
		Collection<BitemporalDocAnexoCot> lstBiEndosoDocAnexo = new ArrayList<BitemporalDocAnexoCot>();
		Collection<BitemporalDocAnexoCot> lstBiEndosoDocAnexoInProcess = new ArrayList<BitemporalDocAnexoCot>();
		
		BitemporalCotizacion biCotizacion = entidadBitemporalService.getByKey(CotizacionContinuity.class, cotizacionContinuityId, validoEn);
				
		lstBiEndosoDocAnexo = biCotizacion.getContinuity().getDocAnexoContinuities().getInProcess(validoEn);
		
		for (BitemporalDocAnexoCot biAnexoCot : lstBiEndosoDocAnexo) {
			if (biAnexoCot.getContinuity().getDocAnexos().hasRecordsInProcess()) {				
				lstBiEndosoDocAnexoInProcess.add(biAnexoCot.getContinuity().getDocAnexos().getInProcess(validoEn));
			}
		}
		return lstBiEndosoDocAnexoInProcess;
	}
	
	@Override
	public Collection<BitemporalDocAnexoCot> getLstBiDocAnexosDisponibles(Long cotizacionContinuityId, DateTime validoEn,
																			Collection<BitemporalDocAnexoCot> lstBiEndososAsociados) {
		Collection<BitemporalDocAnexoCot> lstBiEndosoDocAnexos = new ArrayList<BitemporalDocAnexoCot>();
		
		Collection<BitemporalDocAnexoCot> lstBiEndosoDocAnexosDisponibles = new ArrayList<BitemporalDocAnexoCot>();
		
		BitemporalCotizacion biCotizacion = entidadBitemporalService.getByKey(CotizacionContinuity.class, cotizacionContinuityId, validoEn);
		
		Collection<BitemporalDocAnexoCot> lstBiAnexosPrevios = getDocAnexosTotales(biCotizacion.getContinuity().getNumero());

		List<DocAnexoCotDTO> lstDocPoliza = docAnexoCotFacadeRemote.findByProperty("cotizacionDTO.idToCotizacion", biCotizacion.getContinuity().getNumero());
		
		List<DocumentoAnexoProductoDTO> listaAnexosProducto = documentoAnexoProductoFacadeRemote.findByProperty(
																		"productoDTO.idToProducto", 
																		biCotizacion.getValue().getSolicitud().getProductoDTO().getIdToProducto());
		
		for (DocumentoAnexoProductoDTO docAnexoProductoDTO: listaAnexosProducto) {
			BitemporalDocAnexoCot biDocAnexoCot = new BitemporalDocAnexoCot();
			biDocAnexoCot.getContinuity().setControlArchivoId(docAnexoProductoDTO.getIdToControlArchivo().intValue());
			biDocAnexoCot.getValue().setClaveObligatoriedad(docAnexoProductoDTO.getClaveObligatoriedad());
			biDocAnexoCot.getValue().setDescripcionDocumentoAnexo(docAnexoProductoDTO.getDescripcion());
			biDocAnexoCot.getValue().setNivel(DocAnexoCot.NIVEL_POLIZA);
			biDocAnexoCot.getValue().setOrden(docAnexoProductoDTO.getNumeroSecuencia().intValue());
			biDocAnexoCot.getValue().setClaveTipo(DocAnexoCot.CLAVETIPO_POLIZA);
			biDocAnexoCot.getValue().setCoberturaId(0);
			lstBiEndosoDocAnexos.add(biDocAnexoCot);								
		}

		List<DocumentoAnexoTipoPolizaDTO> listaAnexosTipoPoliza = documentoAnexoTipoPolizaFacadeRemote.findByProperty(
																			"tipoPolizaDTO.idToTipoPoliza", 
																			biCotizacion.getValue().getTipoPoliza().getIdToTipoPoliza());

		
		for (DocumentoAnexoTipoPolizaDTO docAnexoTipoPolizaDTO: listaAnexosTipoPoliza) {
			BitemporalDocAnexoCot biDocAnexoCot = new BitemporalDocAnexoCot();
			biDocAnexoCot.getContinuity().setControlArchivoId(docAnexoTipoPolizaDTO.getIdToControlArchivo().intValue());
			biDocAnexoCot.getValue().setClaveObligatoriedad(docAnexoTipoPolizaDTO.getClaveObligatoriedad());
			biDocAnexoCot.getValue().setDescripcionDocumentoAnexo(docAnexoTipoPolizaDTO.getDescripcion());
			biDocAnexoCot.getValue().setNivel(DocAnexoCot.NIVEL_COTIZACION);
			biDocAnexoCot.getValue().setOrden(docAnexoTipoPolizaDTO.getNumeroSecuencia().intValue());
			biDocAnexoCot.getValue().setClaveTipo(DocAnexoCot.CLAVETIPO_COTIZACION);
			biDocAnexoCot.getValue().setCoberturaId(0);			
			lstBiEndosoDocAnexos.add(biDocAnexoCot);			
		}

		List<DocumentoAnexoCoberturaDTO> listaAnexosCobertura = getDocAnexoCoberturasDto(biCotizacion, validoEn);
		
		for (DocumentoAnexoCoberturaDTO docAnexoCoberturaDTO: listaAnexosCobertura) {
			BitemporalDocAnexoCot biDocAnexoCot = new BitemporalDocAnexoCot();
			biDocAnexoCot.getContinuity().setControlArchivoId(docAnexoCoberturaDTO.getIdToControlArchivo().intValue());
			biDocAnexoCot.getValue().setClaveObligatoriedad(docAnexoCoberturaDTO.getClaveObligatoriedad());
			biDocAnexoCot.getValue().setDescripcionDocumentoAnexo(docAnexoCoberturaDTO.getDescripcion());
			biDocAnexoCot.getValue().setCoberturaId(docAnexoCoberturaDTO.getCoberturaDTO().getIdToCobertura().intValue());
			biDocAnexoCot.getValue().setNivel(DocAnexoCot.NIVEL_COBERTURA);
			biDocAnexoCot.getValue().setOrden(docAnexoCoberturaDTO.getNumeroSecuencia().intValue());
			biDocAnexoCot.getValue().setClaveTipo(DocAnexoCot.CLAVETIPO_COBERTURA);
			lstBiEndosoDocAnexos.add(biDocAnexoCot);			
		}
		for (BitemporalDocAnexoCot biDocAnexoCot: lstBiEndosoDocAnexos) {
			boolean add = true;
			for (BitemporalDocAnexoCot biDocAnexoCotAs : lstBiEndososAsociados) {
				if (biDocAnexoCot.getValue().equals(biDocAnexoCotAs.getValue())) {
					add = false;
					break;
				}
			}
			
			for (DocAnexoCotDTO docPoliza: lstDocPoliza) {
				if (docPoliza.getClaveSeleccion() == 1  && equalsAnexo(biDocAnexoCot.getValue(), docPoliza)) {
					add = false;
					break;
				}				
			}
			
			for (BitemporalDocAnexoCot biDocAnexoCotAs : lstBiAnexosPrevios) {
				if (biDocAnexoCotAs.getValue().getClaveSeleccion() == 1 &&  biDocAnexoCot.getValue().equals(biDocAnexoCotAs.getValue())) {
					add = false;
					break;
				}
			}
			
			if (add) {
				lstBiEndosoDocAnexosDisponibles.add(biDocAnexoCot);
			}
		}
		
		return lstBiEndosoDocAnexosDisponibles;
	}
	

	private List<DocumentoAnexoCoberturaDTO> getDocAnexoCoberturasDto(BitemporalCotizacion biCotizacion, DateTime validoEn) {
		List<DocumentoAnexoCoberturaDTO> lstDocAnexoCoberturasDto = new ArrayList<DocumentoAnexoCoberturaDTO>();
		List<BigDecimal> lstCoberturasDtoId = new ArrayList<BigDecimal>();
		
		Collection<BitemporalInciso> lstIncisos = biCotizacion.getContinuity().getIncisoContinuities().get(validoEn);
		for (BitemporalInciso biInciso : lstIncisos) {
			Collection<BitemporalSeccionInciso> lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().get(validoEn);
			
			for (BitemporalSeccionInciso seccionInciso : lstSeccion) {
				Collection<BitemporalCoberturaSeccion> lstCoberturaSeccion = seccionInciso.getContinuity().getCoberturaSeccionContinuities().get(validoEn);
				
				for (BitemporalCoberturaSeccion coberturaSeccion : lstCoberturaSeccion) {
					lstCoberturasDtoId.add(coberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura());					
				}
			}
		}
		lstDocAnexoCoberturasDto = documentoAnexoCoberturaFacadeRemote.listarAnexosPorCoberturas(lstCoberturasDtoId);
		return lstDocAnexoCoberturasDto;
	}
	
	private Collection<BitemporalDocAnexoCot> getDocAnexosTotales(Integer idToCotizacion) {
		Collection<BitemporalDocAnexoCot> lstAnexos = new ArrayList<BitemporalDocAnexoCot>();
		
		Collection<CotizacionContinuity>  cotContinuities = entidadContinuityDao.findByProperty(CotizacionContinuity.class, 
																																												CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion);
		
		for (CotizacionContinuity continuity: cotContinuities) {
			Collection<BitemporalDocAnexoCot> lst = continuity.getDocAnexoContinuities().get();
			if (lst != null && !lst.isEmpty()) {
				lstAnexos.addAll(lst);
			}
		}

		return lstAnexos;
	}
	
	private boolean equalsAnexo(DocAnexoCot docDisponible, DocAnexoCotDTO docPoliza) {
		
		if (docDisponible.getClaveTipo() == null) {
			if (docPoliza.getClaveTipo() != null)
				return false;
		} else if (docDisponible.getClaveTipo() != docPoliza.getClaveTipo().intValue()) 
			return false;
		if (docDisponible.getDescripcionDocumentoAnexo() == null) {
			if (docPoliza.getDescripcionDocumentoAnexo() != null)
				return false;
		} else if (!docDisponible.getDescripcionDocumentoAnexo().equals(docPoliza.getDescripcionDocumentoAnexo()))
			return false;
		if (docDisponible.getClaveObligatoriedad() == null) {
			if (docPoliza.getClaveObligatoriedad() != null)
				return false;
		} else if (docDisponible.getClaveObligatoriedad() != docPoliza.getClaveObligatoriedad())
			return false;
		if (docDisponible.getOrden() == null) {
			if (docPoliza.getOrden() != null)
				return false;
		} else if (docPoliza.getOrden() != null && (docDisponible.getOrden() != docPoliza.getOrden().intValue()))
			return false;
				
		return true;
	}	

	@EJB
	private EntidadBitemporalService entidadBitemporalService;

	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private DocumentoAnexoProductoFacadeRemote documentoAnexoProductoFacadeRemote;
	
	@EJB
	private DocumentoAnexoTipoPolizaFacadeRemote documentoAnexoTipoPolizaFacadeRemote;
	
	@EJB
	private DocumentoAnexoCoberturaFacadeRemote documentoAnexoCoberturaFacadeRemote;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;	
	
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	public void setEntidadContinuityDao(
			EntidadContinuityDao entidadContinuityDao) {
		this.entidadContinuityDao = entidadContinuityDao;
	}
	
	private DocAnexoCotFacadeRemote docAnexoCotFacadeRemote;
	
	@EJB
	public void setDocAnexoCotFacadeRemote(DocAnexoCotFacadeRemote docAnexoCotFacadeRemote) {
		this.docAnexoCotFacadeRemote = docAnexoCotFacadeRemote;
	}	

}
