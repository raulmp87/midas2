package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.digitalComplementarios;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionFacadeRemote;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoFacadeRemote;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.digitalComplementarios.DocDigitalComplementariosService;

@Stateless
public class DocDigitalComplementariosServiceImpl implements DocDigitalComplementariosService {

	private DocumentoDigitalCotizacionFacadeRemote documentoDigitalCotizacionFacadeRemote;
	private ControlArchivoFacadeRemote controlArchivoFacadeRemote;

	@EJB
	public void setControlArchivoFacadeRemote(
			ControlArchivoFacadeRemote controlArchivoFacadeRemote) {
		this.controlArchivoFacadeRemote = controlArchivoFacadeRemote;
	}
	
	@EJB
	public void setDocumentoDigitalCotizacionFacadeRemote(
			DocumentoDigitalCotizacionFacadeRemote documentoDigitalCotizacionFacadeRemote) {
		this.documentoDigitalCotizacionFacadeRemote = documentoDigitalCotizacionFacadeRemote;
	}

	@Override
	public List<DocumentoDigitalCotizacionDTO> listarDocDigitalesCotizacion(
			BigDecimal idToCotizacion) {
		List<DocumentoDigitalCotizacionDTO> docDigitalesList = new ArrayList<DocumentoDigitalCotizacionDTO>(1);
		docDigitalesList = documentoDigitalCotizacionFacadeRemote.findByProperty("cotizacionDTO.idToCotizacion", idToCotizacion);
		return docDigitalesList;
	}

	@Override
	public void eliminaDocumentoDigitalCotizacionDTO(
			DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO) {
		ControlArchivoDTO controlArchivo = controlArchivoFacadeRemote.findById(documentoDigitalCotizacionDTO.getControlArchivo().getIdToControlArchivo());
		
		//Elimina Documento Digital Cotizacion
		documentoDigitalCotizacionFacadeRemote.delete(documentoDigitalCotizacionDTO);
		
		//Elimina ControlarArchivo
		controlArchivoFacadeRemote.delete(controlArchivo);
	}

	@Override
	public DocumentoDigitalCotizacionDTO findById(BigDecimal idDocumentoDigitalCotizacion) {
		DocumentoDigitalCotizacionDTO documentoDigitalCotizacionDTO = documentoDigitalCotizacionFacadeRemote.findById(idDocumentoDigitalCotizacion);
		return documentoDigitalCotizacionDTO;
	}

}
