package mx.com.afirme.midas2.service.impl.siniestros.spv;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.soap.SOAPFaultException;

import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora.STATUS_BITACORA;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_PROCESO;
import mx.com.afirme.midas2.dto.TransporteCommDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.exeption.CommException;
import mx.com.afirme.midas2.exeption.CommException.CODE;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.sistema.comm.CommServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.oficina.CatalogoSiniestroOficinaService;
import mx.com.afirme.midas2.service.siniestros.spv.ServicioPublicoService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.ArrayOfAsistencias;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.ArrayOfEstimaciones;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.ArrayOfOrdenes;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.ArrayOfTercero;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.ArrayOfTiemposCierre;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.Cierre;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.EnviaReporte;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.Estimaciones;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.RespuestaEnviaReporte;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.TiemposCierre;
import mx.com.afirme.midas2.wsClient.serviciopublicovehicular.WSGestionAjustadoresSoapProxy;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;

@Stateless
public class ServicioPublicoServiceImpl extends CommServiceImpl implements ServicioPublicoService {
	
	private static String PREFIJO_SPV   = "SPV_ENVIA_REPORTE_";
	private String endPoint             = "";
	private static String FECHA_BASE    = "1999-01-01 01:01:01";
	private static final Logger log     = Logger.getLogger(ServicioPublicoServiceImpl.class);
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;  
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	@EJB
	private ParametroGlobalService parametroGlobalService;
	@EJB
	private BitacoraService bitacoraService;
	@EJB
	CatalogoSiniestroOficinaService catalogoSiniestroOficinaService;
	
	@SuppressWarnings("unchecked")
	@Override
	public void notificarReporteServicioPublico(Long keyReporteCabina) {
		
		try {
			ReporteCabina reporteCabina = this.entidadService.findById(ReporteCabina.class, keyReporteCabina);
			
			if( reporteCabina.getOficina().getClaveOficina().equals(parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.SPV_CODIGO_OFICINA  )) ) {
				log.info("CONVERTIR SINIESTRO. (notificarReporteServicioPublico) : Inicia el proceso para notificar a SPV");
				// OBTENER ENDPOINT
				this.endPoint = this.catalogoSiniestroOficinaService.obtenerWsEnpoint(reporteCabina.getOficina()).getEndpoint();
				//this.endPoint = "http://172.30.4.172:6092/WSGestionAjustadores.asmx";
				
				// OBJETO SPV PARA COLOCAR REPORTE
				EnviaReporte enviarReporte = new EnviaReporte();
				
				// ID DEL REPORTE CABINA
				enviarReporte.setIdMidasSPV(Integer.parseInt(keyReporteCabina.toString()));
				// CREDENCIALES WS
				enviarReporte.setUsuario(parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS  , ParametroGlobalService.CODIGO_USUARIO));
				enviarReporte.setPassword(parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS , ParametroGlobalService.CODIGO_PASSWORD));
				
				// OBTENER DATOS DEL REPORTE PARA LLENAR LA SECCION DE DATOS GENERALES DEL WS
				Cierre cierre = this.obtenedarDatosCierre(reporteCabina);
				
				// OBTENER DATOS DEL REPORTE PARA LLENAR LA SECCION DE TIEMPO CIERRE
				ArrayOfTiemposCierre aTiempoCierre = new ArrayOfTiemposCierre();
				aTiempoCierre.getTiemposCierre().add(this.obtenerDatosTiempoCierre(reporteCabina));
				
	            enviarReporte.setDatosCierre(cierre);
	            enviarReporte.setDatosTiempos(aTiempoCierre);
	            enviarReporte.setDatosEstimaciones(this.obtenerDatosEstimacion(keyReporteCabina));
				
	            // ENVIAR WS
	            log.info("CONVERTIR SINIESTRO. (notificarReporteServicioPublico) : Inicia el envio del WS");
	            realizarEnvio(enviarReporte);
	            log.info("CONVERTIR SINIESTRO. (notificarReporteServicioPublico) : Termina el envio del WS");
			}
			
		}catch(Exception e) {
			log.error("Error - ServicioPublicoService.notificarReporteServicioPublico ",e);
		}
		log.info("CONVERTIR SINIESTRO. (notificarReporteServicioPublico) : Finaliza el proceso para notificar a SPV");
		
		
	}
	
	private ArrayOfEstimaciones obtenerDatosEstimacion(Long keyReporteCabina){
		
		ArrayOfEstimaciones aEstimaciones = new ArrayOfEstimaciones();
		
		try {
			
			// OBTENER LISTADO DE PASE DE ATENCION ASOCIADOS AL REPORTE
			PaseAtencionSiniestroDTO filtro = new PaseAtencionSiniestroDTO();
			filtro.setIdReporteCabina(keyReporteCabina);
			List<PaseAtencionSiniestroDTO> pases = this.estimacionCoberturaSiniestroService.buscarPasesAtencion(filtro);
			
	        for(PaseAtencionSiniestroDTO listadoPasesAtencion : CollectionUtils.emptyIfNull(pases)) {
	        	
	        	Estimaciones estimaciones = new Estimaciones();
	        	GregorianCalendar c = new GregorianCalendar();
				c.setTime(listadoPasesAtencion.getEstimacionCoberturaReporte().getFechaCreacion());
				XMLGregorianCalendar fechaReporte = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
	        	
	        	estimaciones.setFolio      ( listadoPasesAtencion.getEstimacionCoberturaReporte().getFolio());
	        	estimaciones.setDescripcion( listadoPasesAtencion.getNombreCobertura() );
	        	estimaciones.setCantidad   ( this.obtenerMontoReservaPase( listadoPasesAtencion.getEstimacionCoberturaReporte().getId() ) );
	        	estimaciones.setFecha      (fechaReporte);

	        	aEstimaciones.getEstimaciones().add(estimaciones);
	        }
	        
	        return aEstimaciones;
	        
		}catch(Exception e) {
			log.error("Error - ServicioPublicoService.obtenerDatosEstimacion ",e);
			return null;
		}
	}
	
	private Double obtenerMontoReservaPase(Long keyEstimacion) {
		
		Double cantidad = 0.0;
		
		try {
			cantidad = Double.valueOf( this.movimientoSiniestroService.obtenerReservaAfectacion(keyEstimacion, true).toString() );
		}catch(Exception e) {
			log.error("Error - ServicioPublicoService.obtenerMontoReservaPase ",e);
		}
		return cantidad;
	}
	
	private TiemposCierre obtenerDatosTiempoCierre(ReporteCabina reporteCabina){
		
		try {
			
			TiemposCierre tiemposCierre = new TiemposCierre();
			Date fechaTerminacionReporte = reporteCabina.getFechaHoraTerminacion();
			
			if( fechaTerminacionReporte == null ) {
				SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				fechaTerminacionReporte = sdf.parse(FECHA_BASE);
			}
			
			GregorianCalendar c = new GregorianCalendar();
			c.setTime(fechaTerminacionReporte);
			XMLGregorianCalendar fechaTerminacion = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);
			
			tiemposCierre.setTipo    ( 5 );
			tiemposCierre.setLatitud ( (reporteCabina.getLugarOcurrido() != null ? reporteCabina.getLugarOcurrido().getCoordenadas().getLatitud().toString() : "" )  );
			tiemposCierre.setLongitud( (reporteCabina.getLugarOcurrido() != null ? reporteCabina.getLugarOcurrido().getCoordenadas().getLongitud().toString(): "" )  );
			tiemposCierre.setTiempo  ( "" );
			tiemposCierre.setFecha   ( fechaTerminacion );
			
			return tiemposCierre;
			
		}catch(Exception e) {
			log.error("Error - ServicioPublicoService.obtenerDatosTiempoCierre ",e);
			return null;
		}
		
	}
	
	private Cierre obtenedarDatosCierre(ReporteCabina reporteCabina) {
		
		try {
			
			Cierre cierre = new Cierre();
			
			AutoIncisoReporteCabina autoInciso = reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
			
			cierre.setCausaSiniestro        (autoInciso.getCausaSiniestro());
			cierre.setTipoResponsabilidad   (autoInciso.getTipoResponsabilidad());
			cierre.setTerminoAjuste         (autoInciso.getTerminoAjuste());
			cierre.setTerminoSiniestro      (autoInciso.getTerminoSiniestro());
			cierre.setFugaTerceroResponsable(autoInciso.getFugoTerceroResponsable() == 1 ? "SI":"NO" );
			cierre.setCompaniaSeguros       (autoInciso.getCiaSeguros());
			cierre.setOrdenesOtraCia        ("NO");
			
			return cierre;
		
		}catch(Exception e) {
			log.error("Error - ServicioPublicoService.obtenedarDatosCierre ",e);
			return null;
		}
	}
	

	@Override
	public TransporteCommDTO enviar(TransporteCommDTO comm)
			throws CommException {
		
		try {
			
			EnviaReporte enviarReporte = (EnviaReporte) comm.getObjetoASerializar();
			
			this.registrarBitacora(EVENTO.SPV_ENVIO_REPORTE_WS, String.valueOf(enviarReporte.getIdMidasSPV()),"PREPARANDO ENVIO DE REPORTE","");
			
			WSGestionAjustadoresSoapProxy envioSpv = new WSGestionAjustadoresSoapProxy();
			
			envioSpv._getDescriptor().setEndpoint("http://172.30.4.172:6092/WSGestionAjustadores.asmx");
			
			RespuestaEnviaReporte respuestaEnvioReporte = envioSpv.enviaReporte(
					enviarReporte.getUsuario(),
					enviarReporte.getPassword(),
					enviarReporte.getIdMidasSPV(), 
					enviarReporte.getDatosCierre(),
					new ArrayOfTercero(), 
					new ArrayOfOrdenes(), 
					new ArrayOfAsistencias(), 
					enviarReporte.getDatosEstimaciones(),
					enviarReporte.getDatosTiempos()
			);
			
			this.registrarBitacora(
						EVENTO.SPV_ENVIO_REPORTE_WS, 
						String.valueOf(enviarReporte.getIdMidasSPV()),
						"ENVIO EXITOSO",
						"Clave: "+respuestaEnvioReporte.getClaveError()+" Respuesta Servicio:"+respuestaEnvioReporte.getDescripcionError()
					);
			
			//# VALIDA MENSAJE EXITOSO
			//# MENSAJES MOSTRADOS POR EL WS
			//# 0	La informacion se ha recibido y almacenado correctamente
			//# 1	Usuario incorrecto
			//# 2	Password incorrecto
			//# 3	No se puede procesar la informacion
			//# 4	No existe el SPV:  ClaveSPV

			if( respuestaEnvioReporte.getClaveError() != 0 ) {
				// SI LA COMUNICACIÓN FUE EXITOSA PERO LA RESPUESTA NEGATIVA SE ACTUALICE EL ESTATUS DE LA BITACORA A ERROR
				this.registrarErrorPrevioEnvio(this.init(enviarReporte), respuestaEnvioReporte.getDescripcionError());
				// SE TRUENA EL SERVICIO PARA QUE ENTREA AL CATCH GENERAL Y GUARDE LOS PARÁMETROS QUE VIAJABAN
				throw new Exception();
			}
			
			}catch(SOAPFaultException e){
				log.error("Error - ServicioPublicoService.enviar - SOAPFaultException fallo ",e);
				throw new CommException(CODE.COMM_ERROR, "SOAPFaultException falló: "+imprimirTrazaError(e));
			}catch(Exception  e){
				log.error("Error - ServicioPublicoService.enviar - ErrorGeneral ",e);
				throw new CommException(CODE.GENERAL_ERROR, "Socket|Datos falló: "+ imprimirTrazaError(e) );
			}
		
		return comm;
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Override
	public TransporteCommDTO init(Object e) {

		TransporteCommDTO comm = new TransporteCommDTO<T>();
		
		EnviaReporte enviarReporte = (EnviaReporte) e;
		comm.setFolio            (PREFIJO_SPV+enviarReporte.getIdMidasSPV() );
		comm.setTipoProceso		 (TIPO_PROCESO.SPV_ENVIAR_REPORTE);
		comm.setTipoComm		 (TIPO_COMM.WS);
		comm.setUsuario			 (usuarioService.getUsuarioActual());
		comm.setObjetoASerializar(e);
		
		processor = super.sessionContext.getBusinessObject(ServicioPublicoService.class);
		
		return comm;
	}

	@Override
	public String imprimirTrazaError(Exception e) {
		
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		//e.printStackTrace(printWriter);
		return writer.toString();
		
	}

	private void registrarBitacora(Bitacora.EVENTO evento,String identificador,String mensaje, String detalle ){
		try{
			
			this.bitacoraService.registrar(TIPO_BITACORA.SPV, evento, identificador , mensaje,detalle, this.usuarioService.getUsuarioActual().getNombreUsuario() );
			
		}catch(Exception e){
			log.error("Error - ServicioPublicoService.registrarBitacora ",e);
		}
	}
	
	@Override
	public boolean isPendientesSpv(Long keyReporteCabina){
		
		boolean bandera = false;
		
		try{
			
			ReporteCabina reporteCabina = this.entidadService.findById(ReporteCabina.class, keyReporteCabina);
			
			if( reporteCabina.getOficina().getClaveOficina().equals(parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, ParametroGlobalService.SPV_CODIGO_OFICINA )) ) {
				
				CommBitacora bitacora = commBitacoraService.obtenerBitacora(PREFIJO_SPV+keyReporteCabina);
				if ( bitacora != null ){
					if(bitacora.getStatus().equals(STATUS_BITACORA.ERROR.name() ) || bitacora.getStatus().equals(STATUS_BITACORA.CANCELADO.name() )){
						return true;
					}
				}
			}
			
		}catch(Exception e){
			log.error("Error - ServicioPublicoService.isPendientesSpv",e);
		}
		
		return bandera;
	}

}
