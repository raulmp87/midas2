package mx.com.afirme.midas2.service.impl.fortimax;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fortimax.FortimaxDao;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;

@Stateless
public class FortimaxServiceImpl implements FortimaxService{

	private FortimaxDao fortimaxDao;
	
	@Override
	public String[] generateDocument(Long id, String tituloAplicacion, String documentName,String folderName)throws Exception {
		return fortimaxDao.generateDocument(id,tituloAplicacion,documentName,folderName);		
	}
	
	@Override
	public String[] generateExpedient(String tituloAplicacion, String[] fieldValues) throws Exception {
		return fortimaxDao.generateExpedient(tituloAplicacion, fieldValues);
	}

	@Override
	public String[] generateLinkToDocument(Long id, String tituloAplicacion, String nombreDocumento)
			throws Exception {
		return fortimaxDao.generateLinkToDocument(id,tituloAplicacion, nombreDocumento);
	}
	

	@Override
	public String[] uploadFile(String fileName,  TransporteImpresionDTO transporteImpresionDTO, String tituloAplicacion, String[] expediente, String carpeta) throws Exception {
		return fortimaxDao.uploadFile(fileName, transporteImpresionDTO, tituloAplicacion, expediente, carpeta);
	}
	
	@Override
	public String[] getDocumentFortimax(Long id,String tituloAplicacion) throws Exception {
		return fortimaxDao.getDocumentFortimax(id, tituloAplicacion);
	}
	
	@EJB
	public void setFortimaxDao(FortimaxDao fortimaxDao) {
		this.fortimaxDao = fortimaxDao;
	}

}
