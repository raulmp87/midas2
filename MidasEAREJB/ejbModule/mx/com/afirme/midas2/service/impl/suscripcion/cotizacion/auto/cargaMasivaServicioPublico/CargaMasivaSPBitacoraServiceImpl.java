package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaSPBitacoraDao;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPFiltradoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.BitacoraCargaMasivaSPResultadoDTO;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaSPBitacoraService;


@Stateless
public class CargaMasivaSPBitacoraServiceImpl implements CargaMasivaSPBitacoraService{

	@EJB
	private CargaMasivaSPBitacoraDao cargaMasivaSPBitacoraDao;
	
	
	public List<BitacoraCargaMasivaSPResultadoDTO> buscarBitacora(BitacoraCargaMasivaSPFiltradoDTO filtroBitacora) {
		return cargaMasivaSPBitacoraDao.buscarBitacora(filtroBitacora);
	}


	@Override
	public Long conteoBitacoraCargaMasivaService(BitacoraCargaMasivaSPFiltradoDTO filtroBitacora) {
		return  cargaMasivaSPBitacoraDao.contarBitacora(filtroBitacora);
	}
	
  
	
}



