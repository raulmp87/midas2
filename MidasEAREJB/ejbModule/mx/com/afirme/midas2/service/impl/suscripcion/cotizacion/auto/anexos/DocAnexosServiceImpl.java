package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.anexos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.dao.fuerzaventa.AgenteMidasDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.documentosFortimax.DocumentoEntity;
import mx.com.afirme.midas2.domain.documentosFortimax.TipoDocumentoEntity;
import mx.com.afirme.midas2.dto.fuerzaventa.DocumentoFortimax;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.anexos.DocAnexosService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

@Stateless
public class DocAnexosServiceImpl implements DocAnexosService {
	
	private DocAnexoCotFacadeRemote docAnexoCotFacadeRemote;
	private EntidadService entidadService;
	private FortimaxService fortimaxService;
	private AgenteMidasDao agenteMidasDao;
	private ClienteFacadeRemote clienteFacade;
	private ValorCatalogoAgentesService catalogoService;
	
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}

	@EJB
	public void setClienteFacade(ClienteFacadeRemote clienteFacade) {
		this.clienteFacade = clienteFacade;
	}

	@EJB
	public void setAgenteMidasDao(AgenteMidasDao agenteMidasDao) {
		this.agenteMidasDao = agenteMidasDao;
	}

	@EJB	
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setFortimaxService(FortimaxService fortimaxService) {
		this.fortimaxService = fortimaxService;
	}

	@EJB
	public void setDocAnexoCotFacadeRemote(DocAnexoCotFacadeRemote docAnexoCotFacadeRemote) {
		this.docAnexoCotFacadeRemote = docAnexoCotFacadeRemote;
	}		

	@Override
	public List<DocAnexoCotDTO> cargaAnexosPorCotizacion(BigDecimal idToCotizacion) {
		List<DocAnexoCotDTO> anexos = new ArrayList<DocAnexoCotDTO>();
		
		anexos = docAnexoCotFacadeRemote.findByProperty("cotizacionDTO.idToCotizacion", idToCotizacion);
		
		return anexos;
	}
	
	@Override
	public DocAnexoCotDTO findById(DocAnexoCotId id) {
		DocAnexoCotDTO anexo = docAnexoCotFacadeRemote.findById(id);
		return anexo;
	}	
	
	@Override
	public void modificarDocAnexo(DocAnexoCotDTO docAnexoCotDTO){
		docAnexoCotFacadeRemote.update(docAnexoCotDTO);
	}

	@Override
	public List<DocumentoFortimax> cargarListaDocsFortimax(Long id, String tipoDocumento) throws Exception {		
		List<TipoDocumentoEntity> tipoDocumentosList=findTypeDocuments(id,tipoDocumento);		
		List<DocumentoEntity> documentoList=findAllDocumentsEntity(id,tipoDocumento);
		return llenarListaDocumentos(tipoDocumentosList,documentoList);
	}

	private List<DocumentoFortimax> llenarListaDocumentos(List<TipoDocumentoEntity>tipoDocumentoEntity,List<DocumentoEntity>documentoEntity){
		List<DocumentoFortimax> listaDocumentosFortimax = new ArrayList<DocumentoFortimax>();
		for(TipoDocumentoEntity tipoDocumentosEntity:tipoDocumentoEntity){
			boolean x=false;
			DocumentoFortimax documentoFortimax = new DocumentoFortimax();
			documentoFortimax.setClaveAplicaPersonaFisicaString(tipoDocumentosEntity.getClaveAplicaPersonaFisica().toString());
			documentoFortimax.setClaveAplicaPersonaMoralString(tipoDocumentosEntity.getClaveAplicaPersonaMoral().toString());
			documentoFortimax.setDescripcion(tipoDocumentosEntity.getDescripcion());
			documentoFortimax.setIdTipoDocumentoString(tipoDocumentosEntity.getId().toString());
			for(DocumentoEntity docEntity:documentoEntity){
				if(docEntity.getTipoDocumentoEntity().getId().equals(tipoDocumentosEntity.getId())){
					documentoFortimax.setIdArchivoString(docEntity.getIdArchivo().toString());
					documentoFortimax.setIdDocumentoString(docEntity.getId().toString());
					documentoFortimax.setExiste("true");
					x=true;
				}
			}
			if(!x){
				documentoFortimax.setIdArchivoString("0");
				documentoFortimax.setIdDocumentoString("0");
				documentoFortimax.setExiste("false");
			}
			listaDocumentosFortimax.add(documentoFortimax);
		}
		return listaDocumentosFortimax;		
	}
	
	@Override
	public String[] generateExpedientClient(Long id)throws Exception {
		
			ClienteGenericoDTO cliente = new ClienteGenericoDTO();
			cliente.setIdCliente(new BigDecimal(id));
			cliente=clienteFacade.loadById(cliente);				
					
			String[] fieldValues = new String[6];
			
			fieldValues[0]=id.toString();
			if(cliente.getClaveTipoPersona()==1){
			fieldValues[1]=cliente.getNombre();		
			fieldValues[2]=cliente.getApellidoPaterno();
			fieldValues[3]=cliente.getApellidoMaterno();
			}
			else{
				fieldValues[1]=cliente.getRazonSocial();		
				fieldValues[2]=cliente.getRazonSocial();
				fieldValues[3]=cliente.getRazonSocial();
			}
			fieldValues[4]=cliente.getClaveTipoPersona().toString();
			fieldValues[5]="1";	
			return fortimaxService.generateExpedient("CLIENTES", fieldValues);
	}

	@Override
	public List<String> getDocumentosFaltantes(Long id,Long claveTipoPersona,String tipoDocumento, String grupoDocumentos)
			throws Exception {
		getAndSaveDocument(id,tipoDocumento,grupoDocumentos);
		List<String> documentosFaltantesList = new ArrayList<String>();
		List<TipoDocumentoEntity> tipoDocumentosList=findTypeDocuments(id, tipoDocumento);		
		List<DocumentoEntity>documentoList=findAllDocumentsEntity(id,grupoDocumentos);		
		for(TipoDocumentoEntity tipoDocEntity:tipoDocumentosList){
			String documentoNoEncontrado = tipoDocEntity.getDescripcion().trim();
			for(DocumentoEntity docEntity :documentoList){			
				if(claveTipoPersona==1&&tipoDocEntity.getClaveAplicaPersonaFisica().equals(1)&&
						tipoDocEntity.getDescripcion().trim().equals(docEntity.getTipoDocumentoEntity().getDescripcion().trim())){
					documentoNoEncontrado=null;
				}			
				else if(claveTipoPersona==2&&tipoDocEntity.getClaveAplicaPersonaMoral().equals(1)&&
						tipoDocEntity.getDescripcion().trim().equals(docEntity.getTipoDocumentoEntity().getDescripcion().trim())){
					documentoNoEncontrado=null;
				}
			}
			if(documentoNoEncontrado!=null){
				documentosFaltantesList.add(documentoNoEncontrado);
			}
		}
		return documentosFaltantesList;
	}

	@Override
	public List<String> validarDocumentos(Long id, List<String> documentosPorValidar)
			throws Exception {
		return null;
	}

	@Override
	public String[] getAndSaveDocument(Long id,String tipoDocumento, String grupoDocumentos) throws Exception {				
		try{
		String[]resultados=agenteMidasDao.getDocumentFortimax(id,"");
		Long count=1L;
		for(int i=0;i<=resultados.length-1;i++){			
			String[]ar=resultados[i].split("_"+id);
			final String respuesta=ar[0];					
			Predicate docFortimax = new Predicate() {				
				@Override
				public boolean evaluate(Object arg0) {
					DocumentoFortimax dto = (DocumentoFortimax)arg0;
					return dto.getDescripcion().equals(respuesta);
				}
			};					
			DocumentoFortimax documentoFortimax = (DocumentoFortimax)CollectionUtils.find(cargarListaDocsFortimax(id,tipoDocumento), docFortimax);
			if(documentoFortimax.getIdArchivoString().equals("0")){
				DocumentoEntity documentoEntity= new DocumentoEntity();	
				TipoDocumentoEntity tipoDocEntity = new TipoDocumentoEntity();
				tipoDocEntity.setClaveAplicaPersonaFisica(Integer.parseInt(documentoFortimax.getClaveAplicaPersonaFisicaString()));
				tipoDocEntity.setClaveAplicaPersonaMoral(Integer.parseInt(documentoFortimax.getClaveAplicaPersonaMoralString()));
				tipoDocEntity.setDescripcion(documentoFortimax.getDescripcion());
				tipoDocEntity.setId(Long.parseLong(documentoFortimax.getIdTipoDocumentoString()));
				documentoEntity.setTipoDocumentoEntity(tipoDocEntity);
				documentoEntity.setId(null);
				documentoEntity.setIdArchivo(count);
				documentoEntity.setEntityId(id);				
				
				entidadService.save(documentoEntity);
			}
			count++;
		}
		return resultados;
		}catch(Exception e){
			return new String[0];
		}
	}

	@Override
	public List<TipoDocumentoEntity> findTypeDocuments(Long id, String tipoDocumento)
			throws Exception {
		try {			
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();			
			ValorCatalogoAgentes moduloDocumento = catalogoService.obtenerElementoEspecifico("Tipo Seccion Documentos",tipoDocumento);		
			parametros.put("idTipoSeccion", moduloDocumento);
				if(tipoDocumento.equals("Articulo 140")){
					ClienteGenericoDTO cliente= new ClienteGenericoDTO();
					cliente.setIdCliente(new BigDecimal(id));
					cliente=clienteFacade.loadById(cliente);					
					if(cliente.getClaveTipoPersona()==1){
						parametros.put("claveAplicaPersonaFisica", 1);		
					}
					else{
						parametros.put("claveAplicaPersonaMoral", 1);			
					}	
				}			 
			 return entidadService.findByProperties(TipoDocumentoEntity.class, parametros);
			} catch (Exception e) {
				e.printStackTrace();
				return new ArrayList<TipoDocumentoEntity>(); 
			}
	}
	
	@Override
	public List<DocumentoEntity> findAllDocumentsEntity(Long id, String tipoDocumento) {
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("descripcion", tipoDocumento);
		
		parametros = new LinkedHashMap<String, Object>();
		
		parametros.put("entityId", id);
		List<DocumentoEntity> doc = new ArrayList<DocumentoEntity>();
		try{
			doc=entidadService.findByProperties(DocumentoEntity.class, parametros);
		}catch(Exception e){
			System.out.println(e.toString());
		}
		return doc;
	}
	
	@Override
	public void generateCobDocumentsByCotizacion(BigDecimal idToCotizacion) {
		docAnexoCotFacadeRemote.generateCobDocumentsByCotizacion(idToCotizacion);
	}
}
