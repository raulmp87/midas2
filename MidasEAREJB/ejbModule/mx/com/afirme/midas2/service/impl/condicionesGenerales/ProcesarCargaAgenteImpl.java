package mx.com.afirme.midas2.service.impl.condicionesGenerales;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.condicionesGenerales.CgAgenteDao;
import mx.com.afirme.midas2.domain.condicionesGenerales.CgAgente;
import mx.com.afirme.midas2.service.condicionesGenerales.ProcesarCargaAgente;

@Stateless
public class ProcesarCargaAgenteImpl implements ProcesarCargaAgente{
	
	private CgAgenteDao cgAgenteDao;
	@EJB
	public void setCgAgenteDao(CgAgenteDao cgAgenteDao) {
		this.cgAgenteDao = cgAgenteDao;
	}

	@Override
	public void guardarCgAgente( CgAgente cgAgente ) throws Exception {
		
		cgAgenteDao.update(cgAgente);
		
	}


}