package mx.com.afirme.midas2.service.impl.movil.cliente;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import com.google.android.gcm.server.Sender;
import com.google.android.gcm.server.Message.Builder;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestro;
import mx.com.afirme.midas2.domain.movil.cliente.NotificacionClienteMovil;
import mx.com.afirme.midas2.domain.siniestro.ReporteSiniestroMovil;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.movil.cliente.NotificacionClienteMovilService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.apn.SendAPNs;

/**
 * Facade for entity Trusrnotificacion.
 * 
 * @see com.mx.NotificacionClienteMovil
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class NotificacionClienteMovilServiceImpl implements NotificacionClienteMovilService {

	private static final Logger LOG = Logger.getLogger(NotificacionClienteMovilServiceImpl.class);

	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	protected SistemaContext sistemaContext;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private ClientePolizasService clientePolizasService;

	/**
	 * Perform an initial save of a previously unsaved Trusrnotificacion entity.
	 * All subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Trusrnotificacion entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(NotificacionClienteMovil entity) {
		LogDeMidasEJB3.log("saving Trusrnotificacion instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Trusrnotificacion entity.
	 * 
	 * @param entity
	 *            Trusrnotificacion entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(NotificacionClienteMovil entity) {
		LogDeMidasEJB3.log("deleting Trusrnotificacion instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(NotificacionClienteMovil.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Trusrnotificacion entity and return it or a
	 * copy of it to the sender. A copy of the Trusrnotificacion entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            Trusrnotificacion entity to update
	 * @return Trusrnotificacion the persisted Trusrnotificacion entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public NotificacionClienteMovil update(NotificacionClienteMovil entity) {
		LogDeMidasEJB3.log("updating Trusrnotificacion instance", Level.INFO, null);
		try {
			NotificacionClienteMovil result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public NotificacionClienteMovil findById(Long id) {
		LogDeMidasEJB3.log("finding Trusrnotificacion instance with id: " + id,
				Level.INFO, null);
		try {
			NotificacionClienteMovil instance = entityManager.find(
					NotificacionClienteMovil.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Trusrnotificacion entities with a specific property value.
	 *
	 * @param propertyName
	 *            the name of the Trusrnotificacion property to query
	 * @param value
	 *            the property value to match
	 * @return List<Trusrnotificacion> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<NotificacionClienteMovil> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding Trusrnotificacion instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MIDAS.Trusrnotificacion model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Trusrnotificacion entities.
	 *
	 * @return List<Trusrnotificacion> all Trusrnotificacion entities
	 */
	@SuppressWarnings("unchecked")
	public List<NotificacionClienteMovil> findAll() {
		LogDeMidasEJB3.log("finding all Trusrnotificacion instances", Level.INFO, null);
		try {
			final String queryString = "select model from MIDAS.Trusrnotificacion model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}
	
	@Override
	public void notificarArriboAjustador(Long idReporte) {
		try {
			final ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporte);
			final ReporteSiniestroMovil reporteMovil = reporte.getReporteMovil();
			if (reporteMovil != null) {
				List<String> destinatarios = clientePolizasService
						.getUsuariosPorPoliza(reporteMovil.getPolizaDTO()
								.getIdToPoliza().longValue(), reporteMovil
								.getNumeroInciso().longValue());
				for (String destinatario : destinatarios) {
					notificar("Confirmar Arribo",
							"Confirma el arribo de tu ajustador", destinatario,
							"confirmAdjusterArrival", null);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	@Override
	public void notificarComienzoServicioSiniestro(Long idReporte) {
		try {
			LOG.debug("notificarComienzoServicioSiniestro()-- " + idReporte);
			final ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporte);
			final ReporteSiniestroMovil reporteMovil = reporte.getReporteMovil();
			if (reporteMovil != null) {
				List<String> destinatarios = clientePolizasService
						.getUsuariosPorPoliza(reporteMovil.getPolizaDTO()
								.getIdToPoliza().longValue(), reporteMovil
								.getNumeroInciso().longValue());
				Map<String, String> data = new HashMap<String, String>();
				data.put("sinisterId", reporteMovil.getId().toString());
				for (String destinatario : destinatarios) {
					notificar("Ajustador asignado",
							"Revisa y sigue a tu ajustador en tiempo real", destinatario,
							"startSinisterService", data);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
		
	@Override
	public void notificarTerminoServicioSiniestro(Long idReporte) { 
		try {
			final ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporte);
			final ReporteSiniestroMovil reporteMovil = reporte.getReporteMovil();
			if (reporteMovil != null) {
				List<String> destinatarios = clientePolizasService
						.getUsuariosPorPoliza(reporteMovil.getPolizaDTO()
								.getIdToPoliza().longValue(), reporteMovil
								.getNumeroInciso().longValue());
				for (String destinatario : destinatarios) {
					notificar("Encuesta de Satisfacci\u00f3n",
							"Ayudanos a mejorar llenando la encuesta de satisfacci\u00f3n", destinatario,
							"endSinisterService", null);
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}
	
	
	public void notificar(String title, String message, String receiptUserName, String evento, Map<String, String> data){
		try {
			final List<String> registrationId = usuarioService.getRegistrationIds(receiptUserName, "ClienteMovil");
			if(CollectionUtils.isNotEmpty(registrationId)) {
				
				List<String> registrationIdAndroid = new ArrayList<String>();
				List<String> registrationIdIphone = new ArrayList<String>();
				for (String registration: registrationId) {
					String so = String.valueOf(registration.charAt(0));
					if ("A".equals(so)) {
						registrationIdAndroid.add(registration.substring(2));
					} else if ("I".equals(so)) {
						registrationIdIphone.add(registration.substring(2));
					}
				}
				if (CollectionUtils.isNotEmpty(registrationIdAndroid)) {
					final Builder builder = new Builder();
					builder.addData("message",  message);
					builder.addData("evento", evento);
					builder.addData("title", title);
					if (data != null) {
						for (Map.Entry<String, String> entry : data.entrySet()) {
							builder.addData(entry.getKey(), entry.getValue());
						}
					}
					final Sender sender = new Sender(sistemaContext.getClienteMovilSenderKey());
					Object gcmResult = sender.send(builder.build(), registrationIdAndroid, 2);
                    LOG.info("--notificar() result: " +  gcmResult);
				}
				if (CollectionUtils.isNotEmpty(registrationIdIphone)) {
					String pathFile = "";			
					if (System.getProperty("os.name").toLowerCase().indexOf("windows") != -1){
						pathFile = SistemaPersistencia.UPLOAD_FOLDER + sistemaContext.getNombreCertificadoClienteSuite();//"SuiteClienteMovil.p12";
					}else{
						pathFile = sistemaContext.getRutaCertificadoClienteMovil()+ sistemaContext.getNombreCertificadoClienteSuite();
					}
					String pw = sistemaContext.getPasswordCertificadoClienteSuite();//"Af1rm3$2k";//
					if (data == null ){
						data = new HashMap<String, String>();
					}
					data.put("message",  message);
					data.put("sender", "Afirme Seguros");
					data.put("senderName", receiptUserName);
					data.put("evento", evento);
					data.put("title", title);	
					SendAPNs apn = new SendAPNs();
					for (String registration: registrationIdIphone) {
						apn.sendAPN(data, registration, pathFile, pw, message, sistemaContext.getClienteSuiteEnvironment());
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
	}

}