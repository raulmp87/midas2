package mx.com.afirme.midas2.service.impl.sapamis.accionesalertas;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesLog;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.accionesalertas.SapAmisAccionesLogService;

@Stateless
public class SapAmisAccionesLogServiceImpl implements SapAmisAccionesLogService{
	private EntidadService entidadService;

	@Override
	public List<SapAmisAccionesLog> findAll() {
		return entidadService.findAll(SapAmisAccionesLog.class);
	}

	@Override
	public SapAmisAccionesLog findById(long id) {
		SapAmisAccionesLog retorno = new SapAmisAccionesLog();
		if(id >= 0){
			retorno = entidadService.findById(SapAmisAccionesLog.class, id);
		}
		return retorno;
	}

	@Override
	public List<SapAmisAccionesLog> findByProperty(String propertyName, Object property) {
		List<SapAmisAccionesLog> emailNegocioAlertasList = new ArrayList<SapAmisAccionesLog>();
		if(propertyName != null && !"".equals(propertyName) && property != null){
			emailNegocioAlertasList = entidadService.findByProperty(SapAmisAccionesLog.class, propertyName, property);
		}
		return emailNegocioAlertasList;
	}

	@Override
	public List<SapAmisAccionesLog> findByStatus(boolean arg0) {
		List<SapAmisAccionesLog> emailNegocioAlertas = entidadService.findByProperty(SapAmisAccionesLog.class, "estatus", arg0?0:1);
		return emailNegocioAlertas;
	}

	@Override
	public SapAmisAccionesLog saveObject(SapAmisAccionesLog arg0) {
		if(arg0 != null && arg0.getIdSapAmisAccionesLog() >= 0){
			arg0.setIdSapAmisAccionesLog((Long)entidadService.saveAndGetId(arg0));
		}
		return arg0;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}