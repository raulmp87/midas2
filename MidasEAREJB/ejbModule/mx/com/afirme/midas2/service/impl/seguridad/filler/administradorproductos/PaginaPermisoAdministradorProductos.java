/**
 * Clase que llena Paginas y Permisos para el rol de Administador de Productos
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.administradorproductos;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoAdministradorProductos {

	@SuppressWarnings("unused")
	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoAdministradorProductos(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
		
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		//Permisos  0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 = AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE
		
		PaginaPermiso pp;
		
		//////////////////////// AdministradordeProductos ////////////////////////////////////////////////
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/producto/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("configuracion.jsp","/MidasWeb/configuracion/producto/listarSimple.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/producto/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/producto/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/producto/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/producto/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/producto/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/producto/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/producto/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarcatalogo.jsp","/MidasWeb/catalogos/producto/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/configuracion/producto/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/listarPorPradre.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/poblarTreeView.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarDescuentoAProducto.jsp","/MidasWeb/configuracion/producto/asociarDescuento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarDescuentosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarDescuentosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/guardarDescuentoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRecargoAProducto.jsp","/MidasWeb/configuracion/producto/asociarRecargo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarRecargosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarRecargosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/guardarRecargoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRamoAProducto.jsp","/MidasWeb/configuracion/producto/mostrarAsociarRamo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarRamosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/guardarRamoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarRamosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarAumentoAProducto.jsp","/MidasWeb/configuracion/producto/asociarAumento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarAumentosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarAumentosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/guardarAumentoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirRecargoProducto.jsp","/MidasWeb/configuracion/producto/mostrarAsociarExclusionRecargo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarExcRecargoProductoAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/guardarExcRecargoProductoAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarExcRecargoProductoPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirAumentoProducto.jsp","/MidasWeb/configuracion/producto/mostrarAsociarExclusionAumento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarExcAumentoProductoAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/guardarExcAumentoProductoAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarExcAumentoProductoPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirDescuentoProducto.jsp","/MidasWeb/configuracion/producto/mostrarAsociarExclusionDescuento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarExcDescuentoProductoAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/guardarExcDescuentoProductoAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarExcDescuentoProductoPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarMonedaAProducto.jsp","/MidasWeb/configuracion/producto/asociarMoneda.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarMonedasAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/mostrarMonedasPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/producto/guardarMonedaAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("documentosAnexos.jsp","/MidasWeb/catalogos/producto/listarDocumentosAnexos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/producto/mostrarDocumentosAnexos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/producto/guardarDocumentoAnexo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/configuracion/producto/mostrarTipoPolizaInactivas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/configuracion/tipopoliza/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/configuracion/tipopoliza/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/configuracion/tipopoliza/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/configuracion/tipopoliza/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/configuracion/tipopoliza/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/configuracion/tipopoliza/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegardetalle.jsp","/MidasWeb/configuracion/tipopoliza/mostrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/configuracion/tipopoliza/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/listarPorPradre.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/listarJsonPorPradre.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRamoATipoPoliza.jsp","/MidasWeb/configuracion/tipopoliza/mostrarAsociarRamo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarRamosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarRamoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarRamosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarDescuentoATipoPoliza.jsp","/MidasWeb/configuracion/tipopoliza/mostrarAsociarDescuento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarDescuentosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarDescuentoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarDescuentosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRecargoATipoPoliza.jsp","/MidasWeb/configuracion/tipopoliza/mostrarAsociarRecargo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarRecargosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarRecargoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarRecargosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarAumentoATipoPoliza.jsp","/MidasWeb/configuracion/tipopoliza/mostrarAsociarAumento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarAumentosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarAumentoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarAumentosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirAumentoTipoPoliza.jsp","/MidasWeb/configuracion/tipopoliza/mostrarExclusionAumento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarExcAumentoTipoPolizaAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarExcAumentoTipoPolizaAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarExcAumentoTipoPolizaPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirDescuentoTipoPoliza.jsp","/MidasWeb/configuracion/tipopoliza/mostrarExclusionDescuento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarExcDescuentoTipoPolizaAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarExcDescuentoTipoPolizaAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarExcDescuentoTipoPolizaPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirRecargoTipoPoliza.jsp","/MidasWeb/configuracion/tipopoliza/mostrarExclusionRecargo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarExcRecargoTipoPolizaAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarExcRecargoTipoPolizaAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarExcRecargoTipoPolizaPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarMonedaATipoPoliza.jsp","/MidasWeb/configuracion/tipopoliza/mostrarAsociarMoneda.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarMonedasAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarMonedaAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarMonedasPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("documentosAnexos.jsp","/MidasWeb/configuracion/tipopoliza/listarDocumentosAnexos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/mostrarDocumentosAnexos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/tipopoliza/guardarDocumentoAnexo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/configuracion/seccion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/configuracion/seccion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/configuracion/seccion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/configuracion/seccion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/configuracion/seccion/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/configuracion/seccion/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/configuracion/seccion/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/configuracion/seccion/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/configuracion/seccion/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/configuracion/seccion/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/listarPorPradre.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/listarJsonPorPradre.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarCoberturaASeccion.jsp","/MidasWeb/configuracion/seccion/mostrarAsociarCobertura.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/mostrarCoberturasAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarCoberturaASeccion.jsp","/MidasWeb/configuracion/seccion/guardarCoberturaAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/mostrarCoberturasPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRamoASeccion.jsp","/MidasWeb/configuracion/seccion/mostrarAsociarRamo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/mostrarRamosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/guardarRamoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/mostrarRamosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarSeccionRequerida.jsp","/MidasWeb/configuracion/seccion/mostrarAsociarSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/mostrarSeccionesRequeridas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/guardarSeccionRequerida.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/seccion/mostrarSeccionesPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/cobertura/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cobertura/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/cobertura/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cobertura/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/cobertura/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cobertura/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/cobertura/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/cobertura/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cobertura/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/cobertura/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cobertura/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/cobertura/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/cobertura/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/cobertura/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarcatalogo.jsp","/MidasWeb/catalogos/cobertura/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/configuracion/cobertura/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/listarPorPradre.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/listarJsonPorPradre.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRiesgoACobertura.jsp","/MidasWeb/configuracion/cobertura/mostrarAsociarRiesgo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarRiesgosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarRiesgoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarRiesgosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("editarRiesgoCobertura.jsp","/MidasWeb/configuracion/cobertura/mostrarEditarRelacionRiesgoCobertura.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/editarRelacionRiesgoCobertura.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarDescuentoACobertura.jsp","/MidasWeb/configuracion/cobertura/asociarDescuento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarDescuentosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarDescuentosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarDescuentoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarAumentoACobertura.jsp","/MidasWeb/configuracion/cobertura/asociarAumento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarAumentosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarAumentosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarAumentoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarRecargoACobertura.jsp","/MidasWeb/configuracion/cobertura/asociarRecargo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarRecargosAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarRecargosPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarRecargoAsociado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirRecargoCobertura.jsp","/MidasWeb/configuracion/cobertura/excluirRecargoCobertura.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarRecargosCoberturaExcluidos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarRecargosCoberturaPorExcluir.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarRecargoCoberturaExcluido.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirAumentoCobertura.jsp","/MidasWeb/configuracion/cobertura/mostrarExclusionAumento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarExcAumentoCoberturaAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarExcAumentoCoberturaAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWebconfiguracion/cobertura/mostrarExcAumentoCoberturaPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("excluirDescuentoCobertura.jsp","/MidasWeb/configuracion/cobertura/mostrarExclusionDescuento.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarExcDescuentoCoberturaAsociadas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarExcDescuentoCoberturaAsociada.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarExcDescuentoCoberturaPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarCoberturasExcluidas.jsp","/MidasWeb/configuracion/cobertura/asociarCoberturasExcluidas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarCoberturasExcluidas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarCoberturasExcluidas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarCoberturasPorExcluir.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("asociarCoberturasRequeridas.jsp","/MidasWeb/configuracion/cobertura/asociarCoberturasRequeridas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarCoberturasRequeridas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarCoberturasRequeridas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarCoberturasNoRequeridas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("deducibleCobertura.jsp","/MidasWeb/configuracion/cobertura/mostrarDeducibleCobertura.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarDeducibles.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarDeducibles.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("coaseguroCobertura.jsp","/MidasWeb/configuracion/cobertura/mostrarCoaseguroCobertura.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/mostrarCoaseguros.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/cobertura/guardarCoaseguros.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("documentosAnexos.jsp","/MidasWeb/catalogos/cobertura/listarDocumentosAnexos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/cobertura/mostrarDocumentosAnexos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/cobertura/guardarDocumentoAnexo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/riesgo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/riesgo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/riesgo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/riesgo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/riesgo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/riesgo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/riesgo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/riesgo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/riesgo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/riesgo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/riesgo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/riesgo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/riesgo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/riesgo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/riesgo/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/riesgo/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/riesgo/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarcatalogo.jsp","/MidasWeb/catalogos/riesgo/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/configuracion/riesgo/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/riesgo/listarJsonPorPradre.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarCoaseguros.jsp","/MidasWeb/configuracion/riesgo/mostrarRegistroCoaseguro.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/riesgo/mostrarCoasegurosRegistrados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/riesgo/guardarCoaseguro.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registrarDeducibles.jsp","/MidasWeb/configuracion/riesgo/mostrarRegistroDeducible.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/riesgo/mostrarDeduciblesRegistrados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/configuracion/riesgo/guardarDeducible.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/tarifa/configuracion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/tarifa/configuracion/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/tarifa/configuracion/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/tarifa/configuracion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/tarifa/configuracion/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/tarifa/configuracion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/tarifa/configuracion/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/tarifa/configuracion/modificar.do"));
		listaPaginaPermiso.add(pp);


////////////////////SISTEMA///////////////////////////////////////////////////////////////////////

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/sistema/cargaCombo/listar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/sistema/cargaCombo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/estado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/ciudad.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/colonia.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/codigoPostal.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/municipio.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subRamo.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subGiro.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subGiroRC.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoEquipoContratista.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoMaquinaria.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoEquipoElectronico.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/subTipoRecipientePresion.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/monedaTipoPoliza.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/formaPagoMoneda.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/reaseguradorCorredor.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cuentaBancoPesos.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cuentaBancoDolares.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/contacto.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/download/descargarArchivo.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/gestionPendientes/listarPendientes.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mail/enviarMail.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/mensaje/verMensaje.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesDanios.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesReaseguro.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/listarPendientesSiniestros.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/remover.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("acerca.jsp","/MidasWeb/sistema/mensajePendiente/mostrarAcerca.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/uploadHandler.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/getInfoHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/getIdHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/cargaMenu.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/uploadHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getInfoHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getIdHandler.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/sistema/vault/getFileInformation.do"));  
		listaPaginaPermiso.add(pp); 



		/////////////////////////////////USUARIO//////////////////////////////////////////////////////////

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("inicio.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/validarUsuario.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("login.jsp","/MidasWeb/sistema/logout.do"));  
		listaPaginaPermiso.add(pp); 


		//////////////////////////////CAT�LOGOS/////////////////////////////////////////////////////////

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ajustador/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/ajustador/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/ajustador/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/ajustador/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/aumentovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/aumentovario/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/aumentovario/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/aumentovario/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/aumentovario/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/aumentovario/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/aumentovario/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/clasificacionembarcacion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/clasificacionembarcacion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/clasificacionembarcacion/getPorId.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ajustador/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/ajustador/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/ajustador/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/ajustador/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listarFiltradoPaginado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonahidro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonahidro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/codigopostalzonahidro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listarFiltradoPaginado.do"));  
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/codigopostalzonasismo/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/contacto/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/contacto/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/contacto/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/contacto/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/contacto/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/contacto/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/contacto/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/cuentabanco/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/listarFiltrado.do")); 
		 listaPaginaPermiso.add(pp); 
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/cuentabanco/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/cuentabanco/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/cuentabanco/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/cuentabanco/mostrarBorrar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/cuentabanco/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/cuentabanco/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/descuento/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/descuento/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/descuento/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/descuento/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/descuento/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/descuento/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/descuento/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/giro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/giro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/giro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/giro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/giro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/giro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/giro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/giro/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/giro/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/giro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/giro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/girorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/girorc/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/girorc/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/girorc/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/girorc/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/girotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/girotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/girotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/girotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/girotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/girotransporte/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/girotransporte/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/girotransporte/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/girotransporte/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/materialcombustible/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/materialcombustible/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/materialcombustible/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/materialcombustible/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/materialcombustible/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/materialcombustible/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/materialcombustible/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/materialcombustible/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/materialcombustible/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/materialcombustible/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/mediotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/mediotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/mediotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/mediotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/mediotransporte/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/mediotransporte/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/mediotransporte/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/mediotransporte/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/descuento/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/descuento/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/descuento/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/descuento/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/descuento/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/descuento/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/distanciaciudad/listar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/paistipodestinotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregarborrar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/agregarborrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/paistipodestinotransporte/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/paistipodestinotransporte/mostrarPaisesAsociados.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/paistipodestinotransporte/mostrarPaisesPorAsociar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ramo/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/ramo/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/ramo/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/ramo/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/recargovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/recargovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/recargovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/recargovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/recargovario/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/recargovario/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/recargovario/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/recargovario/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/recargovario/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/recargovario/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/recargovario/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/recargovario/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/listar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgiro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgiro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subgiro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgiro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgiro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgiro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgiro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subgiro/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subgiro/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subgiro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subgiro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgirorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subgirorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgirorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subgirorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subgirorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subgirorc/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subgirorc/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subgirorc/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subgirorc/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subgirorc/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		
		 pp = new PaginaPermiso(); 
		 pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/listarFiltradoPaginado.do"));  
		 listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subramo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subramo/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subramo/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subramo/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subramo/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subtipoequipocontratista/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subtipomontajemaquina/mostrarDetalle.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/subtiporecipientepresion/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipobandera/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipobandera/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipobandera/getPorId.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipodestinotransporte/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipodestinotransporte/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipodestinotransporte/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipodestinotransporte/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipoembarcacion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipoembarcacion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tipoembarcacion/getPorId.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoempaque/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/agregar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoempaque/agregar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/modificar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoempaque/modificar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoempaque/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoempaque/borrar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoempaque/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoempaque/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipoempaque/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoempaque/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipoempaque/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoequipocontratista/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipoequipocontratista/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoequipocontratista/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipoequipocontratista/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipomontajemaquina/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipomontajemaquina/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipomontajemaquina/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipomontajemaquina/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomuro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipomuro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipomuro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipomuro/borrar.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipomuro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipomuro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipomuro/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipomuro/mostrarBorrar.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipomuro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipomuro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tiponavegacion/listar.do"));
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tiponavegacion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/tiponavegacion/getPorId.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoobracivil/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoobracivil/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoobracivil/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipoobracivil/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipoobracivil/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipoobracivil/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipoobracivil/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipoobracivil/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipoobracivil/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipoobracivil/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tiporecipientepresion/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tiporecipientepresion/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tiporecipientepresion/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tiporecipientepresion/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipotecho/listar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipotecho/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipotecho/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/tipotecho/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipotecho/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/tipotecho/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/tipotecho/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/tipotecho/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/listar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 


		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/zonacresta/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/zonacresta/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/zonacresta/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/viejo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listarFiltrado","/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do")); 
		 listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/zonacresta/viejo/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonacresta/agregarTr.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonacresta/agregarTr.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonacresta/agregarTr.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/listarFiltrado.do")); 
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/agregar.do"));  
		listaPaginaPermiso.add(pp);
			
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/zonahidro/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonahidro/borrar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonahidro/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/zonahidro/mostrarAgrega.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/zonahidro/mostrarBorrar.do")); 
		 listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/zonahidro/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonahidro/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/listar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		 pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));  
		 listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		 pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/agregar.do")); 
		 listaPaginaPermiso.add(pp); 

		 pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/agregar.do")); 
		 listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/modificar.do"));  
		listaPaginaPermiso.add(pp);
		 
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/zonasismo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/zonasismo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/zonasismo/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/zonasismo/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/zonasismo/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/zonasismo/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/zonasismo/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/datosriesgo/listarFiltrado.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/datosriesgo/modificar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/datosriesgo/modificarCodigoFormato.do"));
	    listaPaginaPermiso.add(pp);

/////////////////////////////CONSULTAS//////////////////////////////////////////////////////////////////////////////
		//OBSOLETOS !!!!!! ??????

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		//OBSOLETOS !!!!!! ??????







		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tipoaeronave/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tipoaeronave/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tipolicenciapiloto/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tipolicenciapiloto/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tipopropietariocamion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tipopropietariocamion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tiposerviciotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tiposerviciotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tipotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/consultas/tipotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);
		
		//Listar configuraci�n de productos
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("configuracion.jsp","/MidasWeb/sistema/configuracion/listar.do"));
		listaPaginaPermiso.add(pp);
		
		
		//Cat�logo de colonias
	       
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarColonias.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarFiltrado.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarFiltradoPaginado.do"));
	    listaPaginaPermiso.add(pp); 
	       
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/colonia/mostrarModificar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/modificar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/colonia/mostrarAgregar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/validaCodigoPostal.do"));
	    listaPaginaPermiso.add(pp);
	   
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/agregar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/colonia/mostrarDetalle.do"));
	    listaPaginaPermiso.add(pp);	    
	    
	    //Listar filtrado colonias
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarFiltradoPaginado.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);

		/******** Fin de permisos para listado de Cotizaciones ***************/

	    /******** Inicio de permisos para m�dulo de configuraci�n de paquetes ***************/
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("principalConfiguracionPaquetes.jsp","/MidasWeb/configuracion/tipopoliza/mostrarPaquetes.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listadoPaquetes.jsp","/MidasWeb/configuracion/tipopoliza/obtenerPaquetesPorTipoPoliza.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("desplegarPaquete.jsp","/MidasWeb/configuracion/tipopoliza/paquete/desplegarPaquete.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listaCoberturasRegistradas.jsp","/MidasWeb/configuracion/tipopoliza/paquete/obtenerCoberturasRegistradas.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listaCoberturasDisponibles.jsp","/MidasWeb/configuracion/tipopoliza/paquete/obtenerCoberturasDisponibles.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("respuestaRegistroCobertura.jsp","/MidasWeb/configuracion/tipopoliza/paquete/guardarCoberturaPaquete.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("desplegarPaquete.jsp","/MidasWeb/configuracion/tipopoliza/paquete/agregar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("desplegarPaquete.jsp","/MidasWeb/configuracion/tipopoliza/paquete/modificar.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("respuestaRegistroCobertura.jsp","/MidasWeb/configuracion/tipopoliza/paquete/liberarPaquetePoliza.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("respuestaRegistroCobertura.jsp","/MidasWeb/configuracion/tipopoliza/paquete/eliminarPaquetePoliza.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("respuestaRegistroCobertura.jsp","/MidasWeb/configuracion/tipopoliza/paquete/definirPaquetePoliza.do"));
	    listaPaginaPermiso.add(pp);
	    
	    /******** Fin de permisos para m�dulo de configuraci�n de paquetes ***************/
	    
		return this.listaPaginaPermiso;
	}
	
}