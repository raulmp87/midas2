package mx.com.afirme.midas2.service.impl.tarifa;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifaId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.RelacionesTarifaAgrupadorTarifaDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.impl.ListadoServiceImpl;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;

@Stateless
public class TarifaAgrupadorTarifaServiceImpl extends ListadoServiceImpl implements
		TarifaAgrupadorTarifaService {

	private final String insertedAction = "inserted";
	private final String updatedAction = "updated";
	private final String deletedAction = "deleted";

	@Override
	public RelacionesTarifaAgrupadorTarifaDTO getRelationList(
		TarifaAgrupadorTarifa tarifaAgrupadorTarifa, String claveNegocio) {
		RelacionesTarifaAgrupadorTarifaDTO relacionesTarifasAgrupadorTarifasDTO = new RelacionesTarifaAgrupadorTarifaDTO();
		
		List<TarifaVersion> asociadas = new ArrayList<TarifaVersion>();
		List<TarifaVersion> disponibles = new ArrayList<TarifaVersion>();
		
		
		if(claveNegocio.equals("D")){
			TarifaAgrupadorTarifaId tarifaAgrupadorTarifaId = new TarifaAgrupadorTarifaId();
			tarifaAgrupadorTarifa.setId(tarifaAgrupadorTarifaId);
			tarifaAgrupadorTarifa.getId().setIdLineanegocio(new Long(-1));
			tarifaAgrupadorTarifa.getId().setIdMoneda(new Long(0));						
		}
			
		String[] idToAgrupadorTarifaFromString = tarifaAgrupadorTarifa.getIdToAgrupadorTarifaFromString().split("_");

		tarifaAgrupadorTarifa.getId().setIdToAgrupadorTarifa(Long.valueOf(idToAgrupadorTarifaFromString[0]));
		tarifaAgrupadorTarifa.getId().setIdVerAgrupadorTarifa(Long.valueOf(idToAgrupadorTarifaFromString[1]));
		
	
		List<TarifaVersion> tarifaVersionList = tarifaAgrupadorTarifaDao.getTarifaVersionPorLineaNegocio(claveNegocio);
		List<TarifaAgrupadorTarifa> tarifaAgrupadorTarifaList = tarifaAgrupadorTarifaDao.getTarifaAgrupadorTarifaPorAgrupadorTarifaLineaNegocio(tarifaAgrupadorTarifa, claveNegocio);
		List<TarifaVersionId> tarifaVersionIdList = new ArrayList<TarifaVersionId>();
		
		for(TarifaAgrupadorTarifa item : tarifaAgrupadorTarifaList){
			TarifaVersionId tarifaVersionId = new TarifaVersionId();
			
			tarifaVersionId.setIdConcepto(Long.valueOf(item.getId().getIdConcepto()));
			tarifaVersionId.setIdMoneda(item.getId().getIdMoneda());
			tarifaVersionId.setIdRiesgo(item.getId().getIdToRiesgo());
			tarifaVersionId.setVersion(item.getId().getIdVertarifa());
			tarifaVersionIdList.add(tarifaVersionId);
		}
		
		for(Object object: tarifaVersionList){
			TarifaVersion tempTarifaVersion = (TarifaVersion)object;
			
			if(tarifaVersionIdList.contains(tempTarifaVersion.getId())){
				asociadas.add(tempTarifaVersion);
			}else{
				disponibles.add(tempTarifaVersion);
			}
		}
		
	
		relacionesTarifasAgrupadorTarifasDTO.setAsociadas(asociadas);
		relacionesTarifasAgrupadorTarifasDTO.setDisponibles(disponibles);
		return relacionesTarifasAgrupadorTarifasDTO;
	}
	
	@Override
	public RespuestaGridRelacionDTO relacionarTarifaAgrupadorTarifa(
			String accion, TarifaAgrupadorTarifa tarifaAgrupadorTarifa) {
		
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		String[] idToAgrupadorTarifaFromString = tarifaAgrupadorTarifa.getIdToAgrupadorTarifaFromString().split("_");
		tarifaAgrupadorTarifa.getId().setIdToAgrupadorTarifa(Long.valueOf(idToAgrupadorTarifaFromString[0]));
		tarifaAgrupadorTarifa.getId().setIdVerAgrupadorTarifa(Long.valueOf(idToAgrupadorTarifaFromString[1]));
		
		if (accion.equals(insertedAction)) {
			tarifaAgrupadorTarifaDao.persist(tarifaAgrupadorTarifa);
		}else if ( accion.equals(updatedAction)){
			tarifaAgrupadorTarifaDao.update(tarifaAgrupadorTarifa);
		}else if (accion.equals(deletedAction)) {
			tarifaAgrupadorTarifaDao.remove(tarifaAgrupadorTarifa);
		}
		//TODO Preparar la respuesta y regresarla		
		return null;
	}

	@Override
	public TarifaAgrupadorTarifa getPorNegocioAgrupadorTarifaSeccion(
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion) {
		
		String query = "SELECT model FROM TarifaAgrupadorTarifa model WHERE "+
		"model.id.idToAgrupadorTarifa = :idToAgrupadorTarifa "+
		"AND model.id.idVerAgrupadorTarifa = :idVerAgrupadorTarifa "+
		"AND model.id.idMoneda = :idMoneda "+
		"AND model.id.idToRiesgo = :idToRiesgo "+
		"AND model.id.idConcepto = :idConcepto";

		Map<String, Object>  parameters = new LinkedHashMap<String, Object>();
		parameters.put("idToAgrupadorTarifa", negocioAgrupadorTarifaSeccion.getIdToAgrupadorTarifa());
		parameters.put("idVerAgrupadorTarifa", negocioAgrupadorTarifaSeccion.getIdVerAgrupadorTarifa());
		parameters.put("idMoneda", negocioAgrupadorTarifaSeccion.getIdMoneda());
		parameters.put("idToRiesgo", new Long(501));
		parameters.put("idConcepto", new Short((short) 10));
		
		@SuppressWarnings("unchecked")
		List<TarifaAgrupadorTarifa> tarifaAgrupadorTarifaList = entidadDao.executeQueryMultipleResult(query, parameters);
		TarifaAgrupadorTarifa  tarifaAgrupadorTarifa = null;
		if(tarifaAgrupadorTarifaList.size() > 0){
			tarifaAgrupadorTarifa = tarifaAgrupadorTarifaList.get(0);
		}
		return tarifaAgrupadorTarifa;
	}
	

}
