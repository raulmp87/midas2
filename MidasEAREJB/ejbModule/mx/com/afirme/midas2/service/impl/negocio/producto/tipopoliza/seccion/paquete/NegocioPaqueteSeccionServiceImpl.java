package mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion.paquete;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.RelacionesNegocioPaqueteDTO;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionService;

@Stateless
public class NegocioPaqueteSeccionServiceImpl implements
		NegocioPaqueteSeccionService {
	
	private EntidadDao entidadDao;
	private SeccionFacadeRemote seccionFacadeRemote;
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
		
	@EJB
	public void setSeccionFacadeRemote(SeccionFacadeRemote seccionFacadeRemote) {
		this.seccionFacadeRemote = seccionFacadeRemote;
	}

	@EJB	
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	
	@EJB
	public void setNegocioPaqueteSeccionDao(
			NegocioPaqueteSeccionDao negocioPaqueteSeccionDao) {
		this.negocioPaqueteSeccionDao = negocioPaqueteSeccionDao;
	}



	@Override
	public RelacionesNegocioPaqueteDTO getRelationLists(NegocioSeccion negocioSeccion) {
		//NegocioPaqueteSecccion
		List<NegocioPaqueteSeccion> negocioPaqueteSeccionAsociadas = entidadDao.findByProperty(NegocioPaqueteSeccion.class, "negocioSeccion.idToNegSeccion", negocioSeccion.getIdToNegSeccion());
		//Paquetes Posibles
		SeccionDTO seccionDTO = seccionFacadeRemote.findById(negocioSeccion.getSeccionDTO().getIdToSeccion());
		List<Paquete> posibles = seccionDTO.getPaquetes();
		//NegocioPaqueteSecccion Disponibles
		List<NegocioPaqueteSeccion> negocioPaqueteSeccionDisponibles = new ArrayList<NegocioPaqueteSeccion>();

		//Objeto de trasporte RelacionesNegocioPaqueteDTO
		RelacionesNegocioPaqueteDTO relacionesNegocioPaqueteDTO = new RelacionesNegocioPaqueteDTO();

		//Listado Usado para asociar las ya asociadas con las posibles
		List<Paquete> paquetesAsociados = new ArrayList<Paquete>();

		//llenado del listado paquetesAsociados
		for(NegocioPaqueteSeccion item :negocioPaqueteSeccionAsociadas){
			paquetesAsociados.add(item.getPaquete());
		}

		//llenado del listado negocioPaqueteSeccionDisponibles
		for(Paquete item:posibles){
			if(!paquetesAsociados.contains(item)){
				NegocioPaqueteSeccion negocioPaqueteSeccion = new NegocioPaqueteSeccion();
				negocioPaqueteSeccion.setPaquete(item);
				negocioPaqueteSeccion.setNegocioSeccion(negocioSeccion);
				negocioPaqueteSeccionDisponibles.add(negocioPaqueteSeccion);
				negocioPaqueteSeccion.setModeloAntiguedadMax(new BigDecimal(0));
			}
		}

		//llenado del Objeto RelacionesNegocioPaqueteDTO
		relacionesNegocioPaqueteDTO.setAsociadas(negocioPaqueteSeccionAsociadas);
		relacionesNegocioPaqueteDTO.setDisponibles(negocioPaqueteSeccionDisponibles);

		return relacionesNegocioPaqueteDTO;
	}

	@Override
	public void relacionarNegocioPaquete(String accion, NegocioPaqueteSeccion negocioPaqueteSeccion) {
		//Accion del grid asociar / des asociar
		entidadDao.executeActionGrid(accion, negocioPaqueteSeccion);
		
	}

	@Override
	public NegocioPaqueteSeccion findByNegocioSeccionAndPaqueteDescripcion(
			NegocioSeccion negocioSeccion, String paqueteDescripcion) {
		return negocioPaqueteSeccionDao.findByNegocioSeccionAndPaqueteDescripcion(negocioSeccion, paqueteDescripcion);
	}

}
