package mx.com.afirme.midas2.service.impl.ReporteAgentes;

import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;
import static mx.com.afirme.midas2.utils.CommonUtils.onError;

import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.domain.reporteAgentes.ParamReporteAgenteDTO;
import mx.com.afirme.midas2.domain.reporteAgentes.ReporteAgenteDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.MidasException;

public class ProgramacionReporteAgenteDelegateQuartz {
	
	private EntidadService entidadService;

	/**
	 * Recibe un reporte, del cual se obtiene el nombre del procedure a ejecutar
	 * y debe de cargar los parametros de dicho reporte para enviarlos al stored
	 * procedure. Cada procedure debe de realizar los siguientes pasos: 1.-
	 * Insertar en la tabla correspondiente del reporte 2.- Insertar en el log
	 * de reportes que ya fue ejecutado. 3.- Actualizar el proximo valor de los
	 * parametros del reporte.
	 * 
	 * @param reporte
	 */
	public void cargarDatosInicialesPorReporte(ReporteAgenteDTO reporte)
			throws MidasException {
		if (isNull(reporte) || isNull(reporte.getId())) {
			onError("Favor de proporcionar el reporte a ejecutar");
		}
		if (!isValid(reporte.getSpname())) {
			onError("El reporte " + reporte.getSpname()
					+ " no contiene nombre de procedimiento a ejecutar.");
		}
		List<ParamReporteAgenteDTO> parametros = obtenerParametrosPorReporte(reporte
				.getId());
		StoredProcedureHelper storedHelper = null;
		String sp = reporte.getSpname();
		try {
			LogDeMidasInterfaz
					.log("Entrando a ProgramacionReporteAgenteDelegateImpl.cargarDatosInicialesPorReporte...procedure["
							+ reporte.getSpname() + "]" + this, Level.INFO,
							null);
			storedHelper = new StoredProcedureHelper(sp,StoredProcedureHelper.DATASOURCE_MIDAS);
			for (ParamReporteAgenteDTO parametro : parametros) {
				if (isNotNull(parametro) && isValid(parametro.getNombre())) {
					storedHelper.estableceParametro(parametro.getNombre(),
							str(parametro.getValor()));
				}
			}
			storedHelper.ejecutaActualizar();
			LogDeMidasInterfaz
					.log("Saliendo de ProgramacionReporteAgenteDelegateImpl.cargarDatosInicialesPorReporte...procedure["
							+ reporte.getSpname() + "]" + this, Level.INFO,
							null);
		} catch (SQLException e) {
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			StoredProcedureErrorLog.doLog(null,
					StoredProcedureErrorLog.TipoAccion.GUARDAR, sp,
					ProgramacionReporteAgenteDelegateImpl.class, codErr,
					descErr);
			LogDeMidasInterfaz
					.log("Excepcion en BD de ProgramacionReporteAgenteDelegateImpl.cargarDatosInicialesPorReporte...procedure["
							+ reporte.getSpname() + "]" + this, Level.WARNING,
							e);
			onError(e);
		} catch (Exception e) {
			LogDeMidasInterfaz
					.log("Excepcion general en ProgramacionReporteAgenteDelegateImpl.cargarDatosInicialesPorReporte...procedure["
							+ reporte.getSpname() + "]" + this, Level.WARNING,
							e);
			onError(e);
		}
	}

	private Object str(Object str) {
		return (str != null) ? str : "";
	}

	/**
	 * Obtiene la lista de parametros
	 * 
	 * @param idReporte
	 * @return
	 */
	private List<ParamReporteAgenteDTO> obtenerParametrosPorReporte(
			Long idReporte) {
		List<ParamReporteAgenteDTO> list = entidadService.findByProperty(
				ParamReporteAgenteDTO.class, "reporteAgenteDTO.id", idReporte);
		return list;
	}

	/**
	 * Sobrecarga del metodo cargarDatosInicialesPorReporte con la misma
	 * funcionalidad
	 * 
	 * @param idReporte
	 */
	public void cargarDatosInicialesPorReporte(Long idReporte)
			throws MidasException {
		if (isNull(idReporte)) {
			onError("Favor de proporcionar el reporte a ejecutar");
		}
		ReporteAgenteDTO reporte = entidadService.findById(
				ReporteAgenteDTO.class, idReporte);
		cargarDatosInicialesPorReporte(reporte);
	}

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidad(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
}
