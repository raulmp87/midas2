package mx.com.afirme.midas2.service.impl.sapamis.sistemas;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.sapamis.sistemas.SapAmisRechazosDao;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisRechazos;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisDistpatcherWSService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisRechazosService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisRechazosServiceImpl.
 * 
 * Descripcion: 		Se utiliza para el manejo del Modulo de Rechazos.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisRechazosServiceImpl implements SapAmisRechazosService{
	private static final long serialVersionUID = 1L;
	
	@EJB private SapAmisRechazosDao sapAmisRechazosDao;
	@EJB private SapAmisUtilsService sapAmisUtilsService;
	@EJB private SapAmisDistpatcherWSService sapAmisDistpatcherWSService;
	
	private static final Long ESTATUS_PENDIENTE = new Long(1);
    private static final int CANTIDAD_DE_REGISTROS_PROCESAR = 1000;

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis() {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.sendRegSapAmis(accesos);
    }

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(String[] accesos) {
    	ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    	parametrosConsulta.setEstatus(ESTATUS_PENDIENTE);
    	this.sendRegSapAmis(accesos, parametrosConsulta);
	}

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(ParametrosConsulta parametrosConsulta){
		this.sendRegSapAmis(sapAmisUtilsService.obtenerAccesos(), parametrosConsulta);
	}

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(String[] accesos, ParametrosConsulta parametrosConsulta){
    	List<SapAmisRechazos> sapAmisRechazoList = this.obtenerPorFiltros(parametrosConsulta, CANTIDAD_DE_REGISTROS_PROCESAR, 1);
		for(SapAmisRechazos sapAmisRechazo: sapAmisRechazoList){
			sapAmisDistpatcherWSService.enviarRechazo(sapAmisRechazo, accesos);
		}
    }

	@Override
	public List<SapAmisRechazos> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return sapAmisRechazosDao.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}
}