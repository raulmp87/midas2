package mx.com.afirme.midas2.service.impl.componente.condicionespecial;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.log4j.Logger;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService.FiltroCondicionEspecialCotizacion.Asociacion;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;

@Stateless
public class CondicionEspecialCotizacionServiceImpl implements CondicionEspecialCotizacionService {

	@EJB
	private EntidadService entidadService;
	
	@EJB
	private NegocioCondicionEspecialService negocioCondicionEspecialService;
	
	@EJB
	private IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote;
	
	private static final Logger LOG = Logger.getLogger(CondicionEspecialCotizacionServiceImpl.class);
	


	/*Metodo que permite agregar una condicion especial a nivel inciso o cotizacion. 
	 * El método valida que la condición se encuentre dentro del negocio, obteniendo el negocio de la cotizacion 
	 * para despues llamar al metodo de contieneCondicionEspecial de NegocioCondicionEspecialService. 
	 * Si el negocio la contiene entonces se crea un CondicionEspecialInciso si el numero de inciso es diferente de nulo, 
	 * y una CondicionEspecialCotizacion en caso de ser nulo. 
	 * Se persiste la entidad creada a través de entidadService.save. 
	 * En caso contrario retorna excepcion de Negocio
	 * */
	@Override
	public void agregarCondicionEspecial(BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idCondicionEspecial) {
		CondicionEspecial condicionEspecial = entidadService.findById(CondicionEspecial.class, idCondicionEspecial);
		if(negocioCondicionEspecialService.contieneCondicionEspecial(getIdNegocio(idToCotizacion), condicionEspecial.getCodigo().toString())){
			if (numeroInciso != null){
				IncisoCotizacionDTO inciso = entidadService.findById(
						IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, numeroInciso));
				asignarInciso(condicionEspecial, inciso);
			}else{
				CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
				asignarCotizacion(condicionEspecial, cotizacion);
			}
		}else{
			throw new NegocioEJBExeption("_","No es posible agregar la condición especial.");
		}
	}

	@Override
	public void generaCondicionesBaseCompletas(BigDecimal idToCotizacion) {
		long start = System.currentTimeMillis();
		LOG.info("Entrando a generaCondicionesBaseCompletas idCotizacion : " + idToCotizacion);
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		
		//obtiene cotizacion y busca listado de obligatorias		
		List<NegocioCondicionEspecial> condicionesObligatorias = negocioCondicionEspecialService.obtenerCondicionesBase(
				cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), NivelAplicacion.TODAS);		

		//agrega las condiciones donde correspondan
		for(NegocioCondicionEspecial negCondicion : condicionesObligatorias){
			if(negCondicion.getCondicionEspecial().getNivelAplicacion().shortValue() == NivelAplicacion.POLIZA.getNivel().shortValue()){
				cotizacion.getCondicionesEspeciales().size();
				if(!cotizacion.getCondicionesEspeciales().contains(negCondicion.getCondicionEspecial())){
					agregarCondicionEspecial(idToCotizacion, null, negCondicion.getCondicionEspecial().getId());
				}
			}else{
				for(IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()){
					if(!inciso.getCondicionesEspeciales().contains(negCondicion.getCondicionEspecial())){
						//agregarCondicionEspecial(idToCotizacion, inciso.getId().getNumeroInciso(), 
							//negCondicion.getCondicionEspecial().getId());
						inciso.getCondicionesEspeciales().add(negCondicion.getCondicionEspecial());
						entidadService.save(inciso);
					}
				}
			}			
		}
		LOG.info("Saliendo de generaCondicionesBaseCompletas  idCotizacion "  + idToCotizacion + " : " 
				 + ((System.currentTimeMillis() - start) / 1000.0));
	}

	/*Liga las condiciones especiales obligatorias a nivel póliza que tiene configurado el negocio.
	 * Primero busca las condiciones obligatorias a nivel negocio y luego las relaciona con la cotizacion.
	 * */
	@Override
	public void generarCondicionesBaseCotizacion(BigDecimal idToCotizacion) {
		List<CondicionEspecial> list = negocioCondicionEspecialService.obtenerCondicionesEspeciales(getIdNegocio(idToCotizacion));
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.getCondicionesEspeciales().isEmpty();
		cotizacion.getCondicionesEspeciales().addAll(list);
		entidadService.save(cotizacion);
	}

	/*Liga las condiciones especiales obligatorias a nivel inciso que tiene configurado el negocio.
	 *Primero busca las condiciones obligatorias a nivel negocio y luego las relaciona con el  inciso  o todos los incisos en caso de que venga nulo el parametro de entrada.
	 * */
	@Override
	public void generarCondicionesBaseInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso) {
		List<CondicionEspecial> list = negocioCondicionEspecialService.obtenerCondicionesEspeciales(getIdNegocio(idToCotizacion));
		for(CondicionEspecial element : list){
			if(numeroInciso != null){
				IncisoCotizacionDTO inciso = entidadService.findById(
						IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, numeroInciso));
				asignarInciso(element, inciso);
			}else{
				
				List<IncisoCotizacionDTO> incisos = incisoCotizacionFacadeRemote.findByCotizacionId(idToCotizacion);
				for(IncisoCotizacionDTO inciso : incisos){
					asignarInciso(element, inciso);
				}
			}
		}
	}

	/*Crea un FiltroCotizacionCondicionEspecialDTO e invoca el DAO de obtenerCondicionEspecial.
	 * */
	@Override
	public List<CondicionEspecial> obtenerCondicionEspecial(BigDecimal idToNegocio, BigDecimal idToCotizacion,
			BigDecimal numeroInciso, NivelAplicacion nivelAplicacion, Long idCondicionEspecial, FiltroCondicionEspecialCotizacion.Asociacion asociacion) {
		FiltroCondicionEspecialCotizacion filtro = new FiltroCondicionEspecialCotizacion(idToCotizacion, idCondicionEspecial, numeroInciso, nivelAplicacion, asociacion);
		List<CondicionEspecial> asociadas = new ArrayList<CondicionEspecial>();
		asociadas = obtenerCondicionEspecial(filtro);
		if (asociacion == Asociacion.DISPONIBLE){
			List<CondicionEspecial> list = negocioCondicionEspecialService.obtenerCondicionesEspeciales(idToNegocio.longValue());
			list = filtrarCondicionesPorNivelAplicacion(nivelAplicacion, list);
			list.removeAll(asociadas);
			return list;
		}
		return asociadas;
	}

	/* Si el numero de inciso es nulo, busca la condicion especial a nivel cotizacion en la persistencia y la remueve 
	 * a traves de entidadService.remove. Si el numero de inciso es diferente de nulo busca la condicion  especial a nivel 
	 * inciso y la remueve de la persistencia a traves de entidadService.remove
	 * */
	@Override
	public void removerCondicionEspecial(BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idCondicionEspecial) {
		if(!contieneObligatorias(idToCotizacion, numeroInciso, idCondicionEspecial)){
			if (numeroInciso != null){
				desasignarInciso(idToCotizacion, numeroInciso, idCondicionEspecial);
			}else{
				desasignarCotizacion(idToCotizacion, idCondicionEspecial);
			}			
		}else{
			throw new NegocioEJBExeption("_","No es posible remover la condición especial. Es obligatoria.");
		}
		

	}
	
	@Override
	public List<CondicionEspecial> obtenerCondicionEspecial(FiltroCondicionEspecialCotizacion filtro){
		List<CondicionEspecial> list = new ArrayList<CondicionEspecial>();
		switch(filtro.getNivelAplicacion()){
		case TODAS:
			list = obtenerCondicionEspecialCotizacion(filtro);
			list.addAll(obtenerCondicionEspecialInciso(filtro));			
			break;
		case POLIZA:
			list = obtenerCondicionEspecialCotizacion(filtro);
			break;
		case INCISO:
			list = obtenerCondicionEspecialInciso(filtro);
			break;
		default:
			break;
		}

		return list;
	}
	
	@Override
	public List<CondicionEspecial> obtenerCondicionEspecialCotizacion(FiltroCondicionEspecialCotizacion filtro){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, filtro.getIdToCotizacion());
		cotizacion.getCondicionesEspeciales().isEmpty(); //Obtiene la lista
		return cotizacion.getCondicionesEspeciales();
	}

	@Override
	public List<CondicionEspecial> obtenerCondicionEspecialInciso(FiltroCondicionEspecialCotizacion filtro){
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, new IncisoCotizacionId(filtro.getIdToCotizacion(), filtro.getNumeroInciso()));	
		inciso.getCondicionesEspeciales().isEmpty();
		return inciso.getCondicionesEspeciales();
	}
	
	@Override
	public List<CondicionEspecial> buscarCondicionesEspeciales(BigDecimal idToNegocio, BigDecimal idToCotizacion,
			BigDecimal numeroInciso, NivelAplicacion nivelAplicacion, Long idCondicionEspecial, String token) {
		List<CondicionEspecial> list = obtenerCondicionEspecial(idToNegocio, idToCotizacion, numeroInciso, nivelAplicacion, idCondicionEspecial, Asociacion.DISPONIBLE);
		List<CondicionEspecial> resultadoBusqueda = new ArrayList<CondicionEspecial>();
		for(CondicionEspecial element : list){
			if(element.getNombre().toLowerCase().contains(token.toLowerCase()) || element.getCodigo().toString().contains(token)){
				resultadoBusqueda.add(element);
			}
		}
		return resultadoBusqueda;
	}

	@Override
	public void quitarTodas(BigDecimal idToCotizacion, BigDecimal numeroInciso, NivelAplicacion nivelAplicacion) {
		switch(nivelAplicacion){
			case TODAS:
				desasignarInciso(idToCotizacion, numeroInciso, null);
				desasignarCotizacion(idToCotizacion, null);
				break;
			case POLIZA:
				desasignarCotizacion(idToCotizacion, null);
				break;
			case INCISO:
				desasignarInciso(idToCotizacion, numeroInciso, null);
				break;
			default:
				break;
		}
	}

	@Override
	public void relacionarTodas(BigDecimal idToNegocio, BigDecimal idToCotizacion, BigDecimal numeroInciso, NivelAplicacion nivelAplicacion, String codigoNombre) {
		List<CondicionEspecial> list;
		if(codigoNombre != null){
			list = buscarCondicionesEspeciales(idToNegocio, idToCotizacion, numeroInciso, nivelAplicacion, null, codigoNombre);
		}else{
			list = obtenerCondicionEspecial(idToNegocio, idToCotizacion, numeroInciso, nivelAplicacion, null, Asociacion.DISPONIBLE);	
		}
		
		switch(nivelAplicacion){	
			case TODAS:
				asignarTodasCotizacion(idToCotizacion, list);
				asignarTodasInciso(idToCotizacion, numeroInciso, list);
				break;
			case POLIZA:
				asignarTodasCotizacion(idToCotizacion, list);
				break;
			case INCISO:
				asignarTodasInciso(idToCotizacion, numeroInciso, list);
				break;
			default:
				break;
		}		
	}
	
	@Override
	public Long getIdNegocio(BigDecimal idToCotizacion){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		return cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
	}

	private void asignarCotizacion(CondicionEspecial condicionEspecial, CotizacionDTO cotizacion) {
		cotizacion.getCondicionesEspeciales().isEmpty();//Obtener la lista
		cotizacion.getCondicionesEspeciales().add(condicionEspecial);
		entidadService.save(cotizacion);
	}


	private void asignarInciso(CondicionEspecial condicionEspecial,  IncisoCotizacionDTO inciso) {
		inciso.getCondicionesEspeciales().isEmpty();
		inciso.getCondicionesEspeciales().add(condicionEspecial);
		entidadService.save(inciso);
	}
	
	public void asignarTodasCotizacion(BigDecimal idToCotizacion, List<CondicionEspecial> list){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.getCondicionesEspeciales().isEmpty();
		cotizacion.getCondicionesEspeciales().addAll(list);
		entidadService.save(cotizacion);
	}
	
	public void asignarTodasInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso, List<CondicionEspecial> list){
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, numeroInciso));
		inciso.getCondicionesEspeciales().isEmpty();
		inciso.getCondicionesEspeciales().addAll(list);
		entidadService.save(inciso);
	}

	private void desasignarCotizacion(BigDecimal idToCotizacion, Long idCondicionEspecial) {
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.getCondicionesEspeciales().isEmpty();

		if(idCondicionEspecial == null){
			List<CondicionEspecial> eliminadas = new ArrayList<CondicionEspecial>();
			for(CondicionEspecial element : cotizacion.getCondicionesEspeciales()){
				if(!contieneObligatorias(idToCotizacion, null, element.getId())){
					eliminadas.add(element);
				}
			}
			cotizacion.getCondicionesEspeciales().removeAll(eliminadas);
		}else{
			if(!contieneObligatorias(idToCotizacion, null, idCondicionEspecial)){
				CondicionEspecial element = entidadService.findById(CondicionEspecial.class, idCondicionEspecial);
				cotizacion.getCondicionesEspeciales().remove(element);
			}
		}

		entidadService.save(cotizacion);
	}

	private void desasignarInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idCondicionEspecial) {
		IncisoCotizacionDTO inciso = entidadService.findById(
				IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, numeroInciso));
		inciso.getCondicionesEspeciales().isEmpty();
		
		if(idCondicionEspecial == null){
			List<CondicionEspecial> eliminadas = new ArrayList<CondicionEspecial>();
			for(CondicionEspecial element : inciso.getCondicionesEspeciales()){
				if(!contieneObligatorias(idToCotizacion, numeroInciso, element.getId())){
					eliminadas.add(element);
				}
			}
			inciso.getCondicionesEspeciales().removeAll(eliminadas);
		}else{
			if(!contieneObligatorias(idToCotizacion, numeroInciso, idCondicionEspecial)){
				CondicionEspecial element = entidadService.findById(CondicionEspecial.class, idCondicionEspecial);
				inciso.getCondicionesEspeciales().remove(element);
			}		
		}

		entidadService.save(inciso);
	}
	
	private List<CondicionEspecial> filtrarCondicionesPorNivelAplicacion(NivelAplicacion nivelAplicacion, List<CondicionEspecial> list){
		List<CondicionEspecial> listAux = new ArrayList<CondicionEspecial>();
		for(CondicionEspecial element : list){
			if (element.getNivelAplicacion().equals(nivelAplicacion.getNivel()) || element.getNivelAplicacion().equals(NivelAplicacion.TODAS.getNivel())){
				listAux.add(element);
			}
		}
		return listAux;
	}
	
	private boolean contieneObligatorias(BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idCondicionEspecial){
		boolean retVal = false;
		NivelAplicacion nivelAplicacion = numeroInciso == null ? NivelAplicacion.POLIZA : NivelAplicacion.INCISO;
		List<NegocioCondicionEspecial> listNegocio = negocioCondicionEspecialService.obtenerCondicionesNegocio(getIdNegocio(idToCotizacion), nivelAplicacion);
		List<CondicionEspecial> listObligatorias = new ArrayList<CondicionEspecial>();
		for(NegocioCondicionEspecial element: listNegocio){
			if(element.getObligatoria() == 1){
				listObligatorias.add(element.getCondicionEspecial());
			}
		}
		
		List<CondicionEspecial> list = new ArrayList<CondicionEspecial>();
		if(idCondicionEspecial == null){
			list = obtenerCondicionEspecial(new BigDecimal(getIdNegocio(idToCotizacion)), idToCotizacion, numeroInciso, nivelAplicacion, idCondicionEspecial, FiltroCondicionEspecialCotizacion.Asociacion.ASOCIADA);
		}else{
			list.add(entidadService.findById(CondicionEspecial.class, idCondicionEspecial));
		}
		
		for(CondicionEspecial element : list){
			if(listObligatorias.contains(element)){
				retVal = true;
			}	
		}

		return retVal;
	}
}
