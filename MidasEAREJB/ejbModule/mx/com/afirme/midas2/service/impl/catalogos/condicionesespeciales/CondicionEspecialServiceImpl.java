package mx.com.afirme.midas2.service.impl.catalogos.condicionesespeciales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionFacadeRemote;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.condicionesespeciales.CondicionEspecialDao;
import mx.com.afirme.midas2.domain.condicionesespeciales.ArchivoAdjuntoCondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.AreaCondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.AreaImpacto;
import mx.com.afirme.midas2.domain.condicionesespeciales.CoberturaCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.EstatusCondicion;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial.NivelAplicacion;
import mx.com.afirme.midas2.domain.condicionesespeciales.Factor;
import mx.com.afirme.midas2.domain.condicionesespeciales.FactorAreaCondEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorFactor;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorVarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.ValorVariableAjuste;
import mx.com.afirme.midas2.domain.condicionesespeciales.VarAjusteCondicionEsp;
import mx.com.afirme.midas2.domain.condicionesespeciales.VariableAjuste;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;
import mx.com.afirme.midas2.dto.condicionespecial.AreaCondicionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.CoberturasCondicionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.FactorCondicionDTO;
import mx.com.afirme.midas2.dto.condicionespecial.VarAjusteCondicionDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import com.afirme.commons.exception.CommonException;
import com.afirme.commons.util.BeanUtils;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;
/**
 * 
 * @author Lizeth De La Garza
 *
 */


@Stateless
public class CondicionEspecialServiceImpl  implements CondicionEspecialService {

	@EJB
	private EntidadService entidadService;

	@EJB
	private CondicionEspecialDao condicionEspecialDao;

	@EJB
	private CoberturaSeccionFacadeRemote coberturaSeccionFacade;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;

	@Override
	public List<CondicionEspecial> obtenerCondiciones(CondicionEspecialDTO condicionFiltro) {
		return condicionEspecialDao.buscarCondicionFiltro(condicionFiltro);
	}
	
	@Override
	public List<CondicionEspecial> obtenerCondicionesNombreCodigo(String param) {
		return condicionEspecialDao.buscarCondicionCodigoNombre(param);
	}

	@Override
	public CondicionEspecial obtenerCondicion(Long idCondicionEspecial) {
		return entidadService.findById(CondicionEspecial.class, idCondicionEspecial);
	}

	@Override
	public CondicionEspecial obtenerCondicionCodigo(Long codigo) {
		List<CondicionEspecial> lst = entidadService.findByProperty(CondicionEspecial.class, "codigo", codigo);

		if (!lst.isEmpty()) {
			return lst.get(0);
		}
		return null;
	}	

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	public void setCondicionEspecialDao(CondicionEspecialDao condicionEspecialDao) {
		this.condicionEspecialDao = condicionEspecialDao;
	}

	@Override
	public boolean modificarEstatus(Long idCondicionEspecial, Short estatus) {	

		if (EnumUtil.getLabel(EstatusCondicion.class, estatus) == null) {
			throw new NegocioEJBExeption("000", "Invalid estatus");
		}

		CondicionEspecial condicion = entidadService.findById(CondicionEspecial.class, idCondicionEspecial);

		if (condicion != null) {
			condicion.setEstatus(estatus);			

			if (estatus == EstatusCondicion.ALTA.getValue()) {
				condicion.setFechaAlta(TimeUtils.now().toDate());
				condicion.setFechaBaja(null);
			}

			if (estatus == EstatusCondicion.BAJA.getValue()) {
				condicion.setFechaBaja(TimeUtils.now().toDate());
			}

		} else {
			return false;
		}

		entidadService.save(condicion);

		return true;
	}

	@Override
	public List<EstatusCondicion> obtenerEstatusDisponible(Long idCondicionEspecial) {
		List<EstatusCondicion> list = new ArrayList<EstatusCondicion>();

		if (idCondicionEspecial != null) {

			CondicionEspecial condicion = entidadService.findById(CondicionEspecial.class, idCondicionEspecial);

			if (condicion != null) {
				if (condicion.getEstatus() == null) {
					list.add(EstatusCondicion.PROCESO);
				} else if (condicion.getEstatus() == EstatusCondicion.PROCESO.getValue()) {
					list.add(EstatusCondicion.PROCESO);
					list.add(EstatusCondicion.ALTA);

				} else if (condicion.getEstatus() == EstatusCondicion.ALTA.getValue()) {
					list.add(EstatusCondicion.ALTA);

				} else if (condicion.getEstatus() == EstatusCondicion.BAJA.getValue()) {
					list.add(EstatusCondicion.BAJA);
				}
			}
		} else {
			list.add(EstatusCondicion.PROCESO);
		}

		return list;
	}

	@Override
	public CondicionEspecial guardarCondicionEnProceso(CondicionEspecial condicion) {
		
		CondicionEspecial condicionEntity = null;
		
		if (condicion.getId() != null) {
			condicionEntity = entidadService.findById(CondicionEspecial.class, condicion.getId());
			condicionEntity.setNombre(condicion.getNombre());
			condicionEntity.setDescripcion(condicion.getDescripcion());
		} else {
			
			try {
				condicionEntity = new CondicionEspecial();
				BeanUtils.copyProperties(condicionEntity, condicion);
				Long codigo = condicionEspecialDao.generarCodigo();

				condicionEntity.setCodigo(codigo);
				condicionEntity.setFechaRegistro(TimeUtils.now().toDate());
				condicionEntity.setNivelAplicacion(NivelAplicacion.POLIZA.getNivel());
				condicionEntity.setEstatus(EstatusCondicion.PROCESO.getValue());
				condicionEntity.setCodigoUsuario(usuarioService.getUsuarioActual().getNombreUsuario());
			} catch (CommonException e) {
			}			
		}		

		Long idCondicion = (Long) entidadService.saveAndGetId(condicionEntity);
		condicionEntity = entidadService.findById(CondicionEspecial.class, idCondicion);

		return condicionEntity;
	}

	@Override
	public void guardarCondicionActivacion(CondicionEspecial condicion) {
		condicion.setFechaAlta(TimeUtils.now().toDate());

		entidadService.save(condicion);

	}

	@Override
	public CoberturaCondicionEsp obtenerCoberturaCondicion(Long idCoberturaCondEsp) {
		CoberturaCondicionEsp coberturaCondicion = (CoberturaCondicionEsp) entidadService.findById(CoberturaCondicionEsp.class, idCoberturaCondEsp);

		SeccionDTO seccion = entidadService.findById(SeccionDTO.class, coberturaCondicion.getSeccion().getIdToSeccion());

		CoberturaDTO cobertura = entidadService.findById(CoberturaDTO.class, coberturaCondicion.getCobertura().getIdToCobertura());

		coberturaCondicion.setSeccion(seccion);
		coberturaCondicion.setCobertura(cobertura);

		return coberturaCondicion;
	}


	@Override
	public List<CoberturasCondicionDTO> getLstCoberturasAsociadas(Long idCondicionEspecial) {
		List<CoberturaCondicionEsp> list = new ArrayList<CoberturaCondicionEsp>();
		List<CoberturasCondicionDTO> listCoberturas = new ArrayList<CoberturasCondicionDTO>();
		CoberturasCondicionDTO coberturaCondicion = new CoberturasCondicionDTO();
		BigDecimal idSeccion = new BigDecimal(0);	

		list = condicionEspecialDao.obtenerCoberturasCondicionEspecial(idCondicionEspecial);
		
		for (CoberturaCondicionEsp cobertura : list) {
			
			cobertura = obtenerCoberturaCondicion(cobertura.getId());
			if (cobertura.getSeccion().getIdToSeccion() == idSeccion) {
				coberturaCondicion.getListCoberturas().add(cobertura.getCobertura());
			} else {
				coberturaCondicion = new CoberturasCondicionDTO(idCondicionEspecial, cobertura.getSeccion(), new ArrayList<CoberturaDTO>());
				coberturaCondicion.getListCoberturas().add(cobertura.getCobertura());
				idSeccion = cobertura.getSeccion().getIdToSeccion();
				listCoberturas.add(coberturaCondicion);
			}

		}

		return listCoberturas;
	}

	@Override
	public CoberturasCondicionDTO getCoberturaCondicionAsociada(Long idCondicionEspecial, Long idSeccion) {
		List<CoberturaCondicionEsp> list = new ArrayList<CoberturaCondicionEsp>();

		CoberturasCondicionDTO	coberturaCondicion = new CoberturasCondicionDTO(idCondicionEspecial, null, new ArrayList<CoberturaDTO>());

		Map<String, Object> params = new HashMap<String, Object>();

		params.put("condicionEspecial.id", idCondicionEspecial);
		params.put("seccion.idToSeccion", idSeccion);

		list = entidadService.findByProperties(CoberturaCondicionEsp.class, params);

		for (CoberturaCondicionEsp cobertura : list) {
			cobertura = obtenerCoberturaCondicion(cobertura.getId());

			coberturaCondicion.setSeccion(cobertura.getSeccion());
			coberturaCondicion.getListCoberturas().add(cobertura.getCobertura());
		}

		return coberturaCondicion;
	}

	@Override
	public List<CoberturasCondicionDTO> getLstCoberturasDisponibles(Long idCondicionEspecial, Long idSeccion) {

		List<CoberturasCondicionDTO> coberturasDisponibles = new ArrayList<CoberturasCondicionDTO>();
		CoberturasCondicionDTO coberturaDisponible = new CoberturasCondicionDTO();
		
		if (idCondicionEspecial != null && idSeccion != null) {			
		
			CoberturasCondicionDTO coberturasAsociadas = getCoberturaCondicionAsociada(idCondicionEspecial, idSeccion);			

			boolean agregar = true;

			List<CoberturaSeccionDTO> listCoberturasSeccion = coberturaSeccionFacade.findByProperty("seccionDTO.idToSeccion", idSeccion);

			coberturaDisponible = new CoberturasCondicionDTO(idCondicionEspecial, listCoberturasSeccion.get(0).getSeccionDTO(), 
															 new ArrayList<CoberturaDTO>());
			
			for (CoberturaSeccionDTO coberturaSeccion: listCoberturasSeccion) {
				agregar = true;

				for (CoberturaDTO coberturaDTO : coberturasAsociadas.getListCoberturas()) {

					if (coberturaDTO.getIdToCobertura() == coberturaSeccion.getCoberturaDTO().getIdToCobertura()) {
						agregar = false;
						break;
					}
				}
				
				if (agregar) {
					coberturaDisponible.getListCoberturas().add(coberturaSeccion.getCoberturaDTO());
				}
			}
			if (!coberturaDisponible.getListCoberturas().isEmpty()) {
				coberturasDisponibles.add(coberturaDisponible);
			}
		}
		
		
		
		

		return coberturasDisponibles;
	}

	@Override
	public void asociarCoberturaCondicion(BigDecimal idSeccion, BigDecimal idCobertura, Long idCondicionEspecial) {		
		
		CoberturaCondicionEsp coberturaCondicion = new CoberturaCondicionEsp();
		
		SeccionDTO seccion = entidadService.findById(SeccionDTO.class, idSeccion);
		
		CoberturaDTO cobertura = entidadService.findById(CoberturaDTO.class, idCobertura);
		
		CondicionEspecial condicion = obtenerCondicion(idCondicionEspecial);
		
		coberturaCondicion.setSeccion(seccion);
		
		coberturaCondicion.setCobertura(cobertura);
		
		coberturaCondicion.setCondicionEspecial(condicion);
		
		entidadService.save(coberturaCondicion);

	}

	@Override
	public void removerCoberturaCondicion(BigDecimal idSeccion, BigDecimal idCobertura, Long idCondicionEspecial) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		params.put("seccion.idToSeccion", idSeccion);
		params.put("cobertura.idToCobertura", idCobertura);
		
		List<CoberturaCondicionEsp> coberturaCondicionlst = entidadService.findByProperties(CoberturaCondicionEsp.class, params);
		
		CoberturaCondicionEsp coberturaCondicion = coberturaCondicionlst.get(0);
		
		entidadService.remove(coberturaCondicion);		
	}
	
	@Override
	public void asociarSeccionCondicion(BigDecimal idSeccion, Long idCondicionEspecial) {
		List<CoberturasCondicionDTO> coberturasDisponibles = getLstCoberturasDisponibles(idCondicionEspecial, idSeccion.longValue());
		
		for (CoberturasCondicionDTO coberturaCondicionDisp : coberturasDisponibles) {	
			
			for (CoberturaDTO cobertura : coberturaCondicionDisp.getListCoberturas()) {
				
				CoberturaCondicionEsp coberturaCondicion = new CoberturaCondicionEsp();
				
				CondicionEspecial condicion = obtenerCondicion(idCondicionEspecial);
				
				coberturaCondicion.setCondicionEspecial(condicion);
				coberturaCondicion.setSeccion(coberturaCondicionDisp.getSeccion());				
				coberturaCondicion.setCobertura(cobertura);
				
				entidadService.save(coberturaCondicion);
				//Long id = (Long) entidadService.saveAndGetId(coberturaCondicion);
				//System.out.println(id);
			}
		}

	}

	@Override
	public void removerSeccionCondicion(BigDecimal idSeccion, Long idCondicionEspecial) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		params.put("seccion.idToSeccion", idSeccion);
		
		List<CoberturaCondicionEsp> coberturaCondicionlst = entidadService.findByProperties(CoberturaCondicionEsp.class, params);
		
		for(CoberturaCondicionEsp coberturaCondicion : coberturaCondicionlst) {
			entidadService.remove(coberturaCondicion);	
		}
			
	}

	@Override
	public List<CondicionEspecial> obtenerCondicionEspecialCodNombre(String token) {
		CondicionEspecial cond = new CondicionEspecial();
		CondicionEspecialDTO cDTO = new CondicionEspecialDTO();
		cond.setEstatus(EstatusCondicion.ALTA.getValue());
		cDTO.setCondicion(cond);
		
		List<CondicionEspecial> condiciones = this.obtenerCondiciones( cDTO );
		List<CondicionEspecial> listaRetorno = new ArrayList<CondicionEspecial>();
		for(CondicionEspecial condicion : condiciones){
			if(condicion.getNombre().toLowerCase().contains(token.toLowerCase()) || 
					condicion.getCodigo().toString().contains(token)){
				listaRetorno.add(condicion);
			}
		}		
		return listaRetorno;
	}

	@Override
	public List<CondicionEspecial> obtenerCondicionEspecialPorLinea(BigDecimal idSeccion) {
		return condicionEspecialDao.obtenerCondicionEspecialPorLinea(idSeccion);
	}

	@Override
	public void guardarArchivoAdjunto(ControlArchivoDTO control, Long idCondicionEspecial, String usuario) {
		ArchivoAdjuntoCondicionEspecial archivo = new ArchivoAdjuntoCondicionEspecial();
		archivo.setCodigoUsuarioCreacion(usuario);
		archivo.setControlArchivo(control);	
		archivo.setFechaCreacion(new Date());
		archivo.setCondicionEspecial(entidadService.findById(CondicionEspecial.class, idCondicionEspecial));
		entidadService.save(archivo);	
	}
	
	@Override
	public List<ArchivoAdjuntoCondicionEspecial> listarArchivosAdjuntos(Long idCondicionEspecial){
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("condicionEspecial.id", idCondicionEspecial);
		return entidadService.findByPropertiesWithOrder(ArchivoAdjuntoCondicionEspecial.class, parameters, "fechaCreacion");		
	}

	@Override
	public void eliminarArchivoAdjunto(BigDecimal idToControlArchivo) {
		List<ArchivoAdjuntoCondicionEspecial> archivos = 
			entidadService.findByProperty(ArchivoAdjuntoCondicionEspecial.class, 
					"controlArchivo.idToControlArchivo", idToControlArchivo);
		for(ArchivoAdjuntoCondicionEspecial archivo : archivos){
			entidadService.remove(archivo);
		}	
		entidadService.remove(entidadService.findById(ControlArchivoDTO.class, idToControlArchivo));
	}
	
	@Override
	public VariableAjuste obtenerVariableAjuste(Long id) {
		
		return entidadService.findById(VariableAjuste.class, id);
	}
	
	@Override
	public VarAjusteCondicionEsp obtenerVariableAjusteCondicion(Long idVariableAjuste, Long idCondicionEspecial) {
	
		Map<String, Object> params = new HashMap<String, Object>();
		VarAjusteCondicionEsp variable = null;
		
		params.put("variableAjuste.id", idVariableAjuste);
		params.put("condicionEspecial.id", idCondicionEspecial);
		
		List<VarAjusteCondicionEsp> variables = entidadService.findByProperties(VarAjusteCondicionEsp.class, params);
		
		if (!variables.isEmpty()) {
			variable = variables.get(0);
		}
		
		return variable;
	}
	
	@Override
	public List<ValorVariableAjuste> obtenerValoresVariable(Long idVariableAjuste) {

		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("variableAjuste.id", idVariableAjuste);
		
		List<ValorVariableAjuste> valores = entidadService.findByPropertiesWithOrder(ValorVariableAjuste.class, params, "orden");
		
		return valores;
	}
	
	@Override
	public List<ValorVarAjusteCondicionEsp> obtenerValoresVariableCondicion(Long idVariableAjusteCond) {

		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("varAjusteCondicion.id", idVariableAjusteCond);
		
		List<ValorVarAjusteCondicionEsp> valores = entidadService.findByProperties(ValorVarAjusteCondicionEsp.class, params);
		
		return valores;
	}

	@Override
	public List<VarAjusteCondicionDTO> getLstVariablesAsociadas(Long idCondicionEspecial) {
		List<VarAjusteCondicionEsp> listAsociadas = new ArrayList<VarAjusteCondicionEsp>();
		List<VarAjusteCondicionDTO> listAsociadasDTO = new ArrayList<VarAjusteCondicionDTO>();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		
		listAsociadas = entidadService.findByProperties(VarAjusteCondicionEsp.class, params);
		
		for(VarAjusteCondicionEsp variableAsociada : listAsociadas) {
			VarAjusteCondicionDTO variableDTO = new VarAjusteCondicionDTO();
			VariableAjuste variable = entidadService.findById(VariableAjuste.class, variableAsociada.getVariableAjuste().getId());
			variableDTO.setVariableAjuste(variable);
			listAsociadasDTO.add(variableDTO);
		}
		
		return listAsociadasDTO;
	}

	@Override
	public List<VarAjusteCondicionDTO> getLstVariablesDisponibles(Long idCondicionEspecial) {
		List<VarAjusteCondicionDTO> listDisponibles = new ArrayList<VarAjusteCondicionDTO>();
		List<VariableAjuste> listVariables = new ArrayList<VariableAjuste>();
		
		listVariables = condicionEspecialDao.obtenerVariablesDisponibles(idCondicionEspecial, null);
		
		for(VariableAjuste varAjuste: listVariables) {
			VarAjusteCondicionDTO varAjusteDTO = new VarAjusteCondicionDTO();
			varAjusteDTO.setIdCondicionEspecial(idCondicionEspecial);
			varAjusteDTO.setVariableAjuste(varAjuste);
			listDisponibles.add(varAjusteDTO);
		}
		
		return listDisponibles;
	}
	
	@Override
	public void guardarVariableAjuste(Long idVariableAjuste, Long idCondicionEspecial) {
		VarAjusteCondicionEsp varAjusteCondicion = new VarAjusteCondicionEsp();
		
		VariableAjuste variableAjuste = entidadService.getReference(VariableAjuste.class, idVariableAjuste);
		CondicionEspecial condicion = entidadService.getReference(CondicionEspecial.class, idCondicionEspecial);
		
		varAjusteCondicion.setVariableAjuste(variableAjuste);
		varAjusteCondicion.setCondicionEspecial(condicion);
		
		entidadService.save(varAjusteCondicion);
	}
	
	@Override
	public void eliminarVariableAjuste(Long idVariableAjuste, Long idCondicionEspecial) {
		VarAjusteCondicionEsp varAjusteCondicion = null;
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("variableAjuste.id", idVariableAjuste);
		params.put("condicionEspecial.id", idCondicionEspecial);
		
		List<VarAjusteCondicionEsp> lstVarAjusteCondicion = entidadService.findByProperties(VarAjusteCondicionEsp.class, params);
		varAjusteCondicion = lstVarAjusteCondicion.get(0);
		
		entidadService.remove(varAjusteCondicion);
	}
	
	@Override
	public void guardarValorVariable(Long idCondicionEspecial, Long idVariableAjuste, Long idValorVariableAjuste,String valor) {
		
		VariableAjuste varAjuste = entidadService.findById(VariableAjuste.class, idVariableAjuste);
		
		ValorVariableAjuste valorVariable = null;
		
		ValorVarAjusteCondicionEsp valorCondicion = null;
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("variableAjuste.id", varAjuste.getId());
		params.put("condicionEspecial.id", idCondicionEspecial);
		
		List<VarAjusteCondicionEsp> varAjusteCondicion = entidadService.findByProperties(VarAjusteCondicionEsp.class, params);
		
		if (varAjusteCondicion != null && !varAjusteCondicion.isEmpty()) {
			params.clear();
			
			params.put("varAjusteCondicion.id", varAjusteCondicion.get(0).getId());
			
			if (varAjuste.getTipo() == VariableAjuste.COMPONENT_TYPE.TIME_RANGE.getValue() 
					|| varAjuste.getTipo() == VariableAjuste.COMPONENT_TYPE.DATE_RANGE.getValue() 
					|| varAjuste.getTipo() == VariableAjuste.COMPONENT_TYPE.NUMERIC_RANGE.getValue() ) {
				valorVariable = entidadService.findById(ValorVariableAjuste.class, idValorVariableAjuste);
				params.put("orden", valorVariable.getOrden());
			}
			
			
			List<ValorVarAjusteCondicionEsp> valoresCondicion = entidadService.findByProperties(ValorVarAjusteCondicionEsp.class, params);
			
			if (valoresCondicion != null && !valoresCondicion.isEmpty()) {
				valorCondicion = valoresCondicion.get(0);
				valorCondicion.setValor(valor);
			} else {
				valorCondicion = new ValorVarAjusteCondicionEsp();
				valorCondicion.setValorVariable(valorVariable);
				valorCondicion.setValor(valor);
				valorCondicion.setOrden(valorVariable != null ? valorVariable.getOrden() : 1);
				valorCondicion.setVarAjusteCondicion(varAjusteCondicion.get(0));
			}
			
			entidadService.save(valorCondicion);
		}
	}
	
	@Override
	public Long obtenerIdVariablePorValor(Long idValorVariableAjuste) {
		ValorVariableAjuste valorVariable = entidadService.getReference(ValorVariableAjuste.class, idValorVariableAjuste);
		
		return valorVariable.getVariableAjuste().getId();
	}
	
	@Override	
	public List<VarAjusteCondicionDTO> obtenerVariablePorNombre(Long idCondicionEspecial, String nombreVariable) {		
		List<VarAjusteCondicionDTO> lst = new ArrayList<VarAjusteCondicionDTO>();
		List<VariableAjuste> variables = condicionEspecialDao.obtenerVariablesDisponibles(idCondicionEspecial, nombreVariable);
		 
		 
		 for (VariableAjuste variable : variables) {
			VarAjusteCondicionDTO variableDTO = new VarAjusteCondicionDTO();				
			variableDTO.setVariableAjuste(variable);
			lst.add(variableDTO);
		}
		 return lst;
		
	}
	
	@Override
	public void asociarVariablesAjuste(Long idCondicionEspecial, String nombreVariable) {
		
		List<VariableAjuste> variables = condicionEspecialDao.obtenerVariablesDisponibles(idCondicionEspecial, nombreVariable);
		
		CondicionEspecial condicion = entidadService.getReference(CondicionEspecial.class, idCondicionEspecial);
		
		for(VariableAjuste variable : variables) {
			VarAjusteCondicionEsp varAjusteCondicion = new VarAjusteCondicionEsp();
			varAjusteCondicion.setVariableAjuste(variable);
			varAjusteCondicion.setCondicionEspecial(condicion);
			
			entidadService.save(varAjusteCondicion);
			
		}
		
	}
	
	@Override
	public void eliminarVariablesAjuste(Long idCondicionEspecial) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		
		List<VarAjusteCondicionEsp> variables = entidadService.findByProperties(VarAjusteCondicionEsp.class, params);
		
		entidadService.removeAll(variables);
	}
	

	@Override
	public List<AreaImpacto> getCatalogoAreas() {
		return entidadService.findAll(AreaImpacto.class);
	}

	@Override
	public List<Factor> getCatalogoFactor() {
		return entidadService.findAll(Factor.class);
	}

	@Override
	public List<AreaCondicionDTO> obtenerAreaCondicion(Long idCondicionEspecial) {
		List<AreaCondicionDTO> listAreaDTO = new ArrayList<AreaCondicionDTO>();
		List<AreaCondicionEspecial> listAreaCondicion = new ArrayList<AreaCondicionEspecial>();
		List<FactorCondicionDTO> listFactoresCondicion = null;
		
		List<AreaImpacto> listArea = getCatalogoAreas();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		
		listAreaCondicion = entidadService.findByProperties(AreaCondicionEspecial.class, params);
		
		for (AreaImpacto area : listArea) {
			AreaCondicionDTO areaDTO = new AreaCondicionDTO();
			areaDTO.setIdCondicionEspecial(idCondicionEspecial);
			listFactoresCondicion = this.obtenerFactorCondicion(idCondicionEspecial, area.getId());
			areaDTO.setFactores(listFactoresCondicion);
			areaDTO.setArea(area);
			for (AreaCondicionEspecial areaCondicion : listAreaCondicion) {
				if (areaCondicion.getAreaImpacto().getId() == area.getId()) {
					areaDTO.setSelected(true);
					break;
				}
			}
			listAreaDTO.add(areaDTO);
		}
		
		return listAreaDTO;
	}

	@Override
	public List<FactorCondicionDTO> obtenerFactorCondicion(Long idCondicionEspecial, Long idAreaImpacto) {			
		List<FactorCondicionDTO> listFactorDTO = new ArrayList<FactorCondicionDTO>();
		List<FactorAreaCondEsp> listFactorCondicion = new ArrayList<FactorAreaCondEsp>();
		
		List<Factor> listFactor = getCatalogoFactor();
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("area.condicionEspecial.id", idCondicionEspecial);
		params.put("area.areaImpacto.id", idAreaImpacto);
		
		listFactorCondicion = entidadService.findByProperties(FactorAreaCondEsp.class, params);
		
		for (Factor factor : listFactor) {
			FactorCondicionDTO factorDTO = new FactorCondicionDTO();
			factorDTO.setIdCondicionEspecial(idCondicionEspecial);
			factorDTO.setIdAreaImpacto(idAreaImpacto);
			factor.setValoresFactor(this.obtenerValoresFactor(factor.getId()));
			factorDTO.setFactor(factor);
			
			for (FactorAreaCondEsp factorCondicion: listFactorCondicion) {
				if (factorCondicion.getFactor().getId() == factor.getId()) {
					factorDTO.setSelected(true);
					break;
				}
			}
			listFactorDTO.add(factorDTO);
		}
		
		return listFactorDTO;
	}

	@Override
	public void guardarAreaImpacto(Long idCondicionEspecial, Long idAreaImpacto) {
		AreaCondicionEspecial areaCondicion = new AreaCondicionEspecial();
		
		areaCondicion.setAreaImpacto(entidadService.getReference(AreaImpacto.class, idAreaImpacto));
		
		areaCondicion.setCondicionEspecial(entidadService.getReference(CondicionEspecial.class, idCondicionEspecial));
		
		entidadService.save(areaCondicion);
		
	}

	@Override
	public void eliminarAreaImpacto(Long idCondicionEspecial, Long idAreaImpacto) {
		List<AreaCondicionEspecial> listAreaCondicion = new ArrayList<AreaCondicionEspecial>();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		params.put("areaImpacto.id", idAreaImpacto);
		
		listAreaCondicion = entidadService.findByProperties(AreaCondicionEspecial.class, params);
		
		entidadService.removeAll(listAreaCondicion);		
	}

	@Override
	public void guardarFactor(Long idCondicionEspecial, Long idAreaImpacto, Long idFactor) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		Factor factor = entidadService.getReference(Factor.class, idFactor);			
			
		FactorAreaCondEsp factorCondicion = new FactorAreaCondEsp();
		
		params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		params.put("areaImpacto.id", idAreaImpacto);
		
		List<AreaCondicionEspecial> listAreaCondicion = entidadService.findByProperties(AreaCondicionEspecial.class, params);
		
		if (!listAreaCondicion.isEmpty()) {
			factorCondicion.setArea(listAreaCondicion.get(0));
			
			factorCondicion.setFactor(entidadService.getReference(Factor.class, factor.getKey()));
			
			entidadService.save(factorCondicion);
		}
	
	}
	
	@Override
	public Factor obtenerFactor(Long idFactor) {
		return entidadService.findById(Factor.class, idFactor);
	}

	@Override
	public List<ValorFactor> obtenerValoresFactor(Long idFactor) {
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("factor.id", idFactor);
		
		return entidadService.findByPropertiesWithOrder(ValorFactor.class, params, "orden");
	}

	@Override
	public FactorAreaCondEsp obtenerFactorCondicion(Long idCondicionEspecial, Long idAreaImpacto, Long idFactor) {
		
		FactorAreaCondEsp factor = null;
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("factor.id", idFactor);
		params.put("area.condicionEspecial.id", idCondicionEspecial);
		params.put("area.areaImpacto.id", idAreaImpacto);
		
		List<FactorAreaCondEsp> factores = entidadService.findByProperties(FactorAreaCondEsp.class, params);
		
		if (factores != null && !factores.isEmpty()) {
			factor = factores.get(0);
		}
		
		return factor;
	}


	@Override
	public void eliminarFactor(Long idCondicionEspecial, Long idAreaImpacto, Long idFactor) {
		
		FactorAreaCondEsp factor = obtenerFactorCondicion(idCondicionEspecial, idAreaImpacto, idFactor);
		
		entidadService.remove(factor);		
	}


	/**
	 * Manda a exportar el documento PDF
	 */


	@Override
	public TransporteImpresionDTO imprimirCondicionEspecial( Long idCondicionEspecial ){

		GeneradorImpresion gImpresion = new GeneradorImpresion();

		// Compilado de jReports y generación del .jasper
		String jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionCondicionesEspeciales.jrxml";
		JasperReport jReport = gImpresion.getOJasperReport(jrxml);
		
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionCondicionesEspecialesAreasImpacto.jrxml";
		JasperReport jReportAreasImpacto = gImpresion.getOJasperReport(jrxml);
		
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionCondicionesEspecialesFactores.jrxml";
		JasperReport jReportFactorAreaCondEsp = gImpresion.getOJasperReport(jrxml);
		
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionCondicionesEspecialesFactoresDetalle.jrxml";
		JasperReport jReportFactorDetalle = gImpresion.getOJasperReport(jrxml);

		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionCondicionesEspecialesNegocio.jrxml";
		JasperReport jReportNegocio = gImpresion.getOJasperReport(jrxml);
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionCondicionesEspecialesNegocioCoberturas.jrxml";
		JasperReport jReportNegocioCoberturas = gImpresion.getOJasperReport(jrxml);

		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionCondicionesEspecialesVariablesAjuste.jrxml";
		JasperReport jReportVariablesAjuste = gImpresion.getOJasperReport(jrxml);
		jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/impresionCondicionesEspecialesVariablesAjusteDetalle.jrxml";
		JasperReport jReportVariablesAjusteDetalle = gImpresion.getOJasperReport(jrxml);
		
		// Creación de los datasources que contienen la información a mapear en el jrxml
		List<CondicionEspecial> dataSourceCondicionEspecial = new ArrayList<CondicionEspecial>();
		dataSourceCondicionEspecial.add(this.obtenerCondicion(idCondicionEspecial));
		String nivelAplica = "";
		String esVip = "";
		String status = "";
		
		if( dataSourceCondicionEspecial.get(0).getNivelAplicacion() == CondicionEspecial.NivelAplicacion.POLIZA.getNivel() ){
			nivelAplica = "Póliza";
		} else if( dataSourceCondicionEspecial.get(0).getNivelAplicacion() == CondicionEspecial.NivelAplicacion.INCISO.getNivel() ){
			nivelAplica = "Inciso";
		} else if( dataSourceCondicionEspecial.get(0).getNivelAplicacion() == CondicionEspecial.NivelAplicacion.TODAS.getNivel() ){
			nivelAplica = "Póliza e Inciso";
		}
		
		if( dataSourceCondicionEspecial.get(0).getVip() != null && dataSourceCondicionEspecial.get(0).getVip() ){
			esVip = "Si";
		} else {
			esVip = "No";
		}
		
		if( dataSourceCondicionEspecial.get(0).getEstatus() == 0 ){
			status = "En Proceso";
		} else if( dataSourceCondicionEspecial.get(0).getEstatus() == 1 ){
			status = "Activo";
		} else if( dataSourceCondicionEspecial.get(0).getEstatus() == 2 ){
			status = "Inactivo";
		}
			
		List<AreaCondicionEspecial> dataSourceAreasImpacto = this.obtenerAreasDeCondicion(idCondicionEspecial);
		
		List<CoberturasCondicionDTO> dataSourceNegocioCoberturas = this.getLstCoberturasAsociadas(idCondicionEspecial);
		
		List<VarAjusteCondicionEsp> dataSourceVarAjuste = this.obtenerVarAjusteDeCondicion(idCondicionEspecial);
		
		// Se definen los parámetros a mapear en el jrxml
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		params.put("dataSourceCondicionEspecial", dataSourceCondicionEspecial);
		
		params.put("jReportAreasImpacto", jReportAreasImpacto);
		params.put("dataSourceAreasImpacto", dataSourceAreasImpacto);
		
		params.put("jReportFactorAreaCondEsp", jReportFactorAreaCondEsp);
		params.put("jReportFactorDetalle", jReportFactorDetalle);
		
		params.put("dataSourceNegocioCoberturas", dataSourceNegocioCoberturas);
		params.put("jReportNegocioCoberturas", jReportNegocioCoberturas);
		params.put("jReportNegocio", jReportNegocio);

		params.put("dataSourceVarAjuste", dataSourceVarAjuste);
		params.put("jReportVariablesAjuste", jReportVariablesAjuste);
		params.put("jReportVariablesAjusteDetalle", jReportVariablesAjusteDetalle);
		params.put("nivelAplica", nivelAplica);
		params.put("esVip", esVip);
		params.put("status", status);
		
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceCondicionEspecial);
		
		// Rellena el reporte con los datos y exporta el PDF
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}
	
	@Override
	public List<CoberturaCondicionEsp> getLstCobCondEspAsociadas(Long idCondicionEspecial){
		return condicionEspecialDao.obtenerCoberturasCondicionEspecial(idCondicionEspecial);
	}
	
	public List<AreaCondicionEspecial> getLstAreasAsociadas(Long idCondicionEspecial, boolean readOnly){
		return condicionEspecialDao.obtenerAreasImpactoLigadas(idCondicionEspecial, readOnly);
	}

	@Override
	public void guardarValorFactor(String valor, Long idCondicionEspecial,
			Long idAreaImpacto, Long idFactor) {

		FactorAreaCondEsp factorCondicion = obtenerFactorCondicion(idCondicionEspecial, idAreaImpacto, idFactor);
		
		factorCondicion.setValor(valor);
		
		entidadService.save(factorCondicion);
		
	}

	@Override
	public void guardarNivelImpacto(Long idCondicionEspecial, Short nivelImpacto) {
		CondicionEspecial condicionEspecial = entidadService.findById(CondicionEspecial.class, idCondicionEspecial);		
		condicionEspecial.setNivelAplicacion(nivelImpacto);
		entidadService.save(condicionEspecial);
		
	}

	@Override
	public void guardarVIP(Long idCondicionEspecial, Boolean esVIP) {
		CondicionEspecial condicionEspecial = entidadService.findById(CondicionEspecial.class, idCondicionEspecial);		
		condicionEspecial.setVip(esVIP);
		entidadService.save(condicionEspecial);		
	}
	
	@Override
	public List<SeccionDTO> obtenerNegociosLigados( Long idCondicionEspecial , Boolean readOnly ){
		return condicionEspecialDao.obtenerNegociosLigados(idCondicionEspecial , readOnly );
	}
	
	@Override
	public CondicionEspecial copiarCondicionEspecial(Long idCondicionEspecialBase, String nombre){
		CondicionEspecial condicionEspecialBase = entidadService.findById(CondicionEspecial.class, idCondicionEspecialBase);
		List<CoberturaCondicionEsp> cobConEspBase = this.getLstCobCondEspAsociadas(condicionEspecialBase.getId());
		List<AreaCondicionEspecial> areaCondEspBase = condicionEspecialDao.obtenerAreasImpactoLigadas(condicionEspecialBase.getId(), false);
		List<VarAjusteCondicionEsp> varAjusteCondEspBase = condicionEspecialDao.obtenerVarAjusteCondicionEspecial(condicionEspecialBase.getId());
		CondicionEspecial condEspNuevo = new CondicionEspecial();
		Long codigo = condicionEspecialDao.generarCodigo();
		condEspNuevo.setId(null);
		condEspNuevo.setNombre(nombre);
		condEspNuevo.setCodigo(codigo);
		condEspNuevo.setEstatus(CondicionEspecial.EstatusCondicion.PROCESO.getValue());
		condEspNuevo.setFechaRegistro(new Date());
		condEspNuevo.setCodigoUsuario(condicionEspecialBase.getCodigoUsuario());
		condEspNuevo.setDescripcion(condicionEspecialBase.getDescripcion());
		condEspNuevo.setNivelAplicacion(condicionEspecialBase.getNivelAplicacion());
		condEspNuevo.setNivelImportancia(condicionEspecialBase.getNivelImportancia());
		condEspNuevo.setVip(condicionEspecialBase.getVip());
		entidadService.save(condEspNuevo);
		CoberturaCondicionEsp cobCondEspNuevo;
		AreaCondicionEspecial areaCondEspNuevo;
		VarAjusteCondicionEsp varAjusteCondEspNuevo;
		FactorAreaCondEsp factorAreaCondNuevo;
		ValorVarAjusteCondicionEsp valVarAjusCondEspNuevo;
		for(CoberturaCondicionEsp item: cobConEspBase){
			cobCondEspNuevo = new CoberturaCondicionEsp();
			cobCondEspNuevo.setId(null);
			cobCondEspNuevo.setCobertura(item.getCobertura());
			cobCondEspNuevo.setSeccion(item.getSeccion());
			cobCondEspNuevo.setCondicionEspecial(condEspNuevo);
			entidadService.save(cobCondEspNuevo);
		}
		for(AreaCondicionEspecial item: areaCondEspBase){
			areaCondEspNuevo = new AreaCondicionEspecial();
			areaCondEspNuevo.setId(null);
			areaCondEspNuevo.setCondicionEspecial(condEspNuevo);
			areaCondEspNuevo.setAreaImpacto(item.getAreaImpacto());
			entidadService.save(areaCondEspNuevo);
			for(FactorAreaCondEsp factor: item.getFactores()){
				factorAreaCondNuevo = new FactorAreaCondEsp();
				factorAreaCondNuevo.setId(null);
				factorAreaCondNuevo.setArea(areaCondEspNuevo);
				factorAreaCondNuevo.setFactor(factor.getFactor());
				factorAreaCondNuevo.setValor(factor.getValor());
				entidadService.save(factorAreaCondNuevo);
				areaCondEspNuevo.getFactores().add(factorAreaCondNuevo);
			}
			entidadService.save(areaCondEspNuevo);
		}
		for(VarAjusteCondicionEsp item: varAjusteCondEspBase){
			varAjusteCondEspNuevo = new VarAjusteCondicionEsp();
			varAjusteCondEspNuevo.setId(null);
			varAjusteCondEspNuevo.setCondicionEspecial(condEspNuevo);
			varAjusteCondEspNuevo.setVariableAjuste(item.getVariableAjuste());
			entidadService.save(varAjusteCondEspNuevo);
			//varAjusteCondEspNuevo.getValores().clear();
			for(ValorVarAjusteCondicionEsp valor: item.getValores()){
				valVarAjusCondEspNuevo = new ValorVarAjusteCondicionEsp();
				valVarAjusCondEspNuevo.setId(null);
				valVarAjusCondEspNuevo.setVarAjusteCondicion(varAjusteCondEspNuevo);
				valVarAjusCondEspNuevo.setOrden(valor.getOrden());
				valVarAjusCondEspNuevo.setValor(valor.getValor());
				valVarAjusCondEspNuevo.setValorVariable(valor.getValorVariable());
				entidadService.save(valVarAjusCondEspNuevo);
				varAjusteCondEspNuevo.getValores().add(valVarAjusCondEspNuevo);
			}
			entidadService.save(varAjusteCondEspNuevo);
		}
		return condEspNuevo;
	}
	
	public List<FactorAreaCondEsp> obtenerFactoresAreaCondEsp( AreaCondicionEspecial areaCond ){
		Long idCondicionEspecial = areaCond.getCondicionEspecial().getId();
		Long idAreaImpacto = areaCond.getAreaImpacto().getId();

		List<FactorAreaCondEsp> lstFactorArea = new ArrayList<FactorAreaCondEsp>();

		List<FactorCondicionDTO> lstFactorCond = this.obtenerFactoresDeCondicion(idCondicionEspecial, idAreaImpacto);
		for( FactorCondicionDTO factorDTO : lstFactorCond ){
			Long idFactor = factorDTO.getFactor().getId();
			FactorAreaCondEsp factorArea = this.obtenerFactorCondicion(idCondicionEspecial, idAreaImpacto, idFactor);
			
			FactorAreaCondEsp factorACE = new FactorAreaCondEsp();
			factorACE.setArea(factorArea.getArea());
			factorACE.setFactor(factorArea.getFactor());
			factorACE.setId(factorArea.getId());
			
			String valor = obtenerValorFactor(factorArea);
			factorACE.setValor(valor);
			
			lstFactorArea.add(factorACE);
		}
		
		return lstFactorArea;
	}
	
	public String obtenerValorFactor( FactorAreaCondEsp factorAreaCond ){

		String valor = "";
		
		if( factorAreaCond.getFactor().getTipo() == 7 ){
			
			Map<String, Object> params = new HashMap<String, Object>();
			
			params.put("factor.id", factorAreaCond.getFactor().getId());
			params.put("id", new Long(factorAreaCond.getValor()));
			
			List<ValorFactor> valoresFactor = entidadService.findByProperties(ValorFactor.class, params);
			
			valor = valoresFactor.get(0).getNombre();
		} else {
			valor = factorAreaCond.getValor();
		}

		return valor;
	}
	
	public List<AreaCondicionEspecial> obtenerAreasDeCondicion(Long idCondicionEspecial) {
		List<AreaCondicionEspecial> listAreaCondicion = new ArrayList<AreaCondicionEspecial>();
		List<AreaCondicionEspecial> listAreaCondiciones = new ArrayList<AreaCondicionEspecial>();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		
		listAreaCondiciones = entidadService.findByProperties(AreaCondicionEspecial.class, params);
		
		for (AreaCondicionEspecial areaCondicion : listAreaCondiciones) {
			AreaCondicionEspecial areaCond = new AreaCondicionEspecial();
			
			areaCond.setAreaImpacto(areaCondicion.getAreaImpacto());
			areaCond.setCondicionEspecial(areaCondicion.getCondicionEspecial());
			areaCond.setId(areaCondicion.getId());
			areaCond.setFactores(this.obtenerFactoresAreaCondEsp(areaCondicion));
			listAreaCondicion.add(areaCond);
		}
		
		return listAreaCondicion;
	}
	
	public List<FactorCondicionDTO> obtenerFactoresDeCondicion(Long idCondicionEspecial, Long idAreaImpacto) {			
		List<FactorCondicionDTO> listFactorDTO = new ArrayList<FactorCondicionDTO>();
		List<FactorAreaCondEsp> listFactorCondicion = new ArrayList<FactorAreaCondEsp>();
		
		List<Factor> listFactor = getCatalogoFactor();
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("area.condicionEspecial.id", idCondicionEspecial);
		params.put("area.areaImpacto.id", idAreaImpacto);
		
		listFactorCondicion = entidadService.findByProperties(FactorAreaCondEsp.class, params);
		
		for (Factor factor : listFactor) {
			FactorCondicionDTO factorDTO = new FactorCondicionDTO();
			factorDTO.setIdCondicionEspecial(idCondicionEspecial);
			factorDTO.setIdAreaImpacto(idAreaImpacto);
			factor.setValoresFactor(this.obtenerValoresFactor(factor.getId()));
			factorDTO.setFactor(factor);
			
			for (FactorAreaCondEsp factorCondicion: listFactorCondicion) {
				if (factorCondicion.getFactor().getId() == factor.getId()) {
					factorDTO.setSelected(true);
					listFactorDTO.add(factorDTO);
					break;
				}
			}
		}
		
		return listFactorDTO;
	}

	public List<VarAjusteCondicionEsp> obtenerVarAjusteDeCondicion( Long idCondicionEspecial ){
		List<VarAjusteCondicionEsp> listVariablePreCondiciones = new ArrayList<VarAjusteCondicionEsp>();
		List<VarAjusteCondicionEsp> listVariableCondiciones = new ArrayList<VarAjusteCondicionEsp>();
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("condicionEspecial.id", idCondicionEspecial);
		
		listVariablePreCondiciones = entidadService.findByProperties(VarAjusteCondicionEsp.class, params);
		
		for (VarAjusteCondicionEsp variableCondicion : listVariablePreCondiciones) {
			VarAjusteCondicionEsp variableCondicionDTO = new VarAjusteCondicionEsp();
			variableCondicionDTO.setCondicionEspecial(variableCondicion.getCondicionEspecial());
			variableCondicionDTO.setId(variableCondicion.getId());
			variableCondicionDTO.setValores(this.obtenerValoresDeVariable(variableCondicion));
			variableCondicionDTO.setVariableAjuste(variableCondicion.getVariableAjuste());
			listVariableCondiciones.add(variableCondicionDTO);
		}
		
		return listVariableCondiciones;
	}
	
	public List<ValorVarAjusteCondicionEsp> obtenerValoresDeVariable(VarAjusteCondicionEsp variableCondicion) {
		
		List<ValorVarAjusteCondicionEsp> listValorPreVariableCondiciones = new ArrayList<ValorVarAjusteCondicionEsp>();
		List<ValorVarAjusteCondicionEsp> listValorVariableCondiciones = new ArrayList<ValorVarAjusteCondicionEsp>();
		
		Map<String, Object> param = new HashMap<String, Object>();
		
		Long varajustecondesp_id = variableCondicion.getId();
		
		param.put("varAjusteCondicion.id", varajustecondesp_id);
		
		listValorPreVariableCondiciones = entidadService.findByProperties(ValorVarAjusteCondicionEsp.class, param);
		
		for( ValorVarAjusteCondicionEsp valorVarAjusteCondEsp : listValorPreVariableCondiciones ){
			ValorVarAjusteCondicionEsp valorVarAjusteCondEspDTO = new ValorVarAjusteCondicionEsp();
			valorVarAjusteCondEspDTO.setId( valorVarAjusteCondEsp.getId() );
			valorVarAjusteCondEspDTO.setOrden(valorVarAjusteCondEsp.getOrden());
			valorVarAjusteCondEspDTO.setValor(obtenerValorVariableAjuste( valorVarAjusteCondEsp ));
			valorVarAjusteCondEspDTO.setValorVariable(valorVarAjusteCondEsp.getValorVariable());
			valorVarAjusteCondEspDTO.setVarAjusteCondicion(valorVarAjusteCondEsp.getVarAjusteCondicion());
			listValorVariableCondiciones.add(valorVarAjusteCondEspDTO);
		}
		
		return listValorVariableCondiciones;
	}
	
	public String obtenerValorVariableAjuste( ValorVarAjusteCondicionEsp valVarAjusteCond ){
		
		String valor = "";
		
		if( valVarAjusteCond.getVarAjusteCondicion().getVariableAjuste().getTipo() == 7 ){
			valor = valVarAjusteCond.getValorVariable().getValor();			
		} else {
			valor = valVarAjusteCond.getValorVariable().getValor() + ": " + valVarAjusteCond.getValor();
		}

		return valor;
	}
	
}