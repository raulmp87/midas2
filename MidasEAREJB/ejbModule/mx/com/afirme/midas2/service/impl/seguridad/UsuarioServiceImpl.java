package mx.com.afirme.midas2.service.impl.seguridad;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.asm.dto.ApplicationDTO;
import com.asm.dto.ChangePasswordDTO;
import com.asm.dto.CreatePasswordUserParameterDTO;
import com.asm.dto.CreatePortalUserParameterDTO;
import com.asm.dto.LoginParametersDTO;
import com.asm.dto.PageConsentDTO;
import com.asm.dto.ResendConfirmationEmailDTO;
import com.asm.dto.RoleDTO;
import com.asm.dto.SearchUserParametersDTO;
import com.asm.dto.SendResetPasswordTokenDTO;
import com.asm.dto.UserDTO;
import com.asm.dto.UserInboxEntryDTO;
import com.asm.dto.UserUpdateDTO;
import com.asm.dto.UserView;
import com.asm.ejb.ASMRemote;
import com.asm.ejb.RoleRemote;
import com.asm.ejb.ValidationImplementationRemote;
import com.asm.service.ASMConstants;
import com.asm.web.webservice.ASMWSC;
import com.asm.web.webservice.RoleWSC;
import com.asm.web.webservice.ValidationImplementationWSC;
import com.js.service.SystemException;

import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.usuario.UsuarioDTO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.ajustador.AjustadorMovil;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.sistema.seguridad.SeguridadContext;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.seguridad.MidasASMTranslator;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class UsuarioServiceImpl implements UsuarioService {

	private ASMRemote asmRemote;
	private RoleRemote roleRemote;
	@SuppressWarnings("unused")
	private ValidationImplementationRemote validationImplementationRemote;
	private MidasASMTranslator midasASMTranslator;
	private AgenteMidasService agenteMidasService;
	private SistemaContext sistemaContext;
	private EntityManager entityManager;
	private Validator validator;
	private static final Logger log = Logger.getLogger(UsuarioServiceImpl.class);
	
	@PostConstruct
	private void init() {
		this.asmRemote = new ASMWSC(sistemaContext.getAsmWscContext());
		this.roleRemote = new RoleWSC(sistemaContext.getAsmWscContext());
		this.validationImplementationRemote = new ValidationImplementationWSC(sistemaContext.getAsmWscContext());
	}
	
	public void setAsmRemote(ASMRemote asmRemote) {
		this.asmRemote = asmRemote;
	}
	
	public void setRoleRemote(RoleRemote roleRemote) {
		this.roleRemote = roleRemote;
	}
	
	public void setValidationImplementationRemote(
			ValidationImplementationRemote validationImplementationRemote) {
		this.validationImplementationRemote = validationImplementationRemote;
	}
		
	@EJB
	public void setMidasASMTranslator(MidasASMTranslator midasASMTranslator) {
		this.midasASMTranslator = midasASMTranslator;
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	@PersistenceContext
	public void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	@Resource
	public void setValidator(Validator validator) {
		this.validator = validator;
	}
	
	@Override
	public Usuario getUsuarioActual() {
		return SeguridadContext.getContext().getUsuario();
	}
	
	@Override
	public void setUsuarioActual(Usuario usuario){
		SeguridadContext.getContext().setUsuario(usuario);
	}

	@Override
	public List<Usuario> buscarUsuariosSinRolesPorNombreRol(String nombreRol,
			String nombreUsuario, String idSesionUsuario) {
		log.info("idSesionUsuario :" + idSesionUsuario);
		int idAccesoUsuario = Integer.parseInt(idSesionUsuario);
		List<UserDTO> usuariosASM;
		try {
			usuariosASM = asmRemote.getUsersWithOutRolesByRoleName(nombreRol,
					nombreUsuario, idAccesoUsuario);
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
		return midasASMTranslator.obtieneListaUsuariosMidasSinRoles(usuariosASM);
	}

	@Override
	public List<Usuario> buscarUsuariosSinRolesPorNombreRol(
			String nombreUsuario, String idSesionUsuario, String... nombresRol) {
		List<Usuario> usuarios = new ArrayList<Usuario>();
		for (String nombreRol : nombresRol) {
			usuarios.addAll(buscarUsuariosSinRolesPorNombreRol(nombreRol,nombreUsuario, idSesionUsuario));
		}
		return usuarios;		
	}

	@Override
	public Usuario buscarUsuarioRegistrado(String nombreUsuario,
			String idSesionUsuario) {
		return buscarUsuarioRegistrado(nombreUsuario, idSesionUsuario, null);
	}
	
	@Override
	public Usuario buscarUsuarioRegistrado(String token) {
		return buscarUsuarioRegistrado(null, null, token);
	}
	
	private Usuario buscarUsuarioRegistrado(String nombreUsuario, String idSesionUsuario, String token) {			
		UserView usuarioASM;
		UsuarioDTO usuarioDTO;
		try {
			if (token != null) {
				usuarioASM = asmRemote.getUserViewByToken(token);
			} else {
				int idAccesoUsuario = Integer.parseInt(idSesionUsuario);				
				log.info("idAccesoUsuario ==> "+idAccesoUsuario);
				usuarioASM = asmRemote.getLoggedUserView(nombreUsuario, idAccesoUsuario);
			}			
			usuarioDTO = findUsuarioDTO(usuarioASM.getUsername());
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
		return midasASMTranslator.obtieneUsuarioMidas(usuarioASM, usuarioDTO);		
	}
	
	private UsuarioDTO findUsuarioDTO(String usuarionombre) {
		String jpql = "select m from UsuarioDTO m where m.usuarionombre = :usuarionombre";		
		TypedQuery<UsuarioDTO> query = entityManager.createQuery(jpql, UsuarioDTO.class);
		query.setParameter("usuarionombre", usuarionombre);
		log.info("usuarioNombre ==> "+ usuarionombre);
		List<UsuarioDTO> list = query.getResultList();
		if (list.size() == 0) {
			return null;
		}
		
		if (list.size() == 1) {
			return list.get(0);
		}
		
		throw new RuntimeException("Más de un resultado.");
	}

	@Override
	public boolean logOutUsuario(String nombreUsuario, String idSesionUsuario) {
		int idAccesoUsuario = Integer.parseInt(idSesionUsuario);
		try {
			return asmRemote.setLogoutUser(nombreUsuario, idAccesoUsuario);
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public List<PageConsentDTO> buscarConsentimientosPorRol(int idRole) {
		try {
			return roleRemote.getPageConsentList(idRole);
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	public Usuario buscarUsuarioPorId(int idUsuario) {
		UserDTO usuarioASM;
		UsuarioDTO usuarioDTO = null;
		try {
			usuarioASM = asmRemote.getUser(idUsuario, UserDTO.class);
			if (usuarioASM != null) {
				usuarioDTO = findUsuarioDTO(usuarioASM.getUsername());
			}
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
		return midasASMTranslator.obtieneUsuarioMidas(usuarioASM, usuarioDTO);
	}

	@Override
	public Usuario buscarUsuarioPorNombreUsuario(String nombreUsuario) {
		UserDTO usuarioASM;
		UsuarioDTO usuarioDTO;
		try {
			usuarioASM = asmRemote.getUser(nombreUsuario, UserDTO.class);
			usuarioDTO = findUsuarioDTO(nombreUsuario);
		} catch (SystemException e) {
			throw new RuntimeException(e);
		}
		return midasASMTranslator.obtieneUsuarioMidas(usuarioASM, usuarioDTO);
	}

	@Override
	public List<Usuario> buscarUsuariosPorNombreRol(String... nombresRol) {
		// TODO Auto-generated method stub
		List<UserDTO> usuarios = new ArrayList<UserDTO>();
		List<UserDTO> usuariosTmp = new ArrayList<UserDTO>();
		for (String nombreRol : nombresRol) {
			try {
				usuariosTmp = asmRemote.getUsersByRoleName(5, nombreRol);
				usuarios.addAll(usuariosTmp);
			} catch (SystemException e) {
				throw new RuntimeException(e);
			}
		}
		
		return midasASMTranslator.obtieneListaUsuariosMidasSinRoles(usuarios);
	}
	
	/*
	 * Metodo que solo devuelve username y nombre para los roles dados
	 * 
	 */
	@Override
	public List<Usuario> buscarUsuariosPorNombreRolSimple(String... nombresRol) {
		// TODO Auto-generated method stub
		List<UserDTO> usuarios = new ArrayList<UserDTO>();
		List<UserDTO> usuariosTmp = new ArrayList<UserDTO>();
		for (String nombreRol : nombresRol) {
			try {
				usuariosTmp = asmRemote.getUsersByRoleNameWithOutRoles(5, nombreRol);
				usuarios.addAll(usuariosTmp);
			} catch (SystemException e) {
				throw new RuntimeException(e);
			}
		}
		
		return midasASMTranslator.obtieneListaUsuariosMidasSinRoles(usuarios);
	}	

	@Override
	public boolean tieneRol(String nombreRol, Usuario usuario) {
		final String nombre = nombreRol;
		Predicate predicado = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				Rol rol = (Rol)arg0;
				return rol.getDescripcion().equals(nombre);
			}
		};
		return usuario != null && CollectionUtils.exists(usuario.getRoles(), predicado);
	}
	
	@Override
	public boolean tieneRol(String[] nombreRoles, Usuario usuario) {
		for (String nombreRol : nombreRoles) {
			if (tieneRol(nombreRol, usuario)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean tieneRolUsuarioActual(String nombreRol) {
		return this.tieneRol(nombreRol, this.getUsuarioActual());
	}
	
	@Override
	public boolean tieneRolUsuarioActual(String[] nombreRoles) {
		for (String nombreRol : nombreRoles) {
			if (tieneRolUsuarioActual(nombreRol)) {
				return true;
			}
		}
		return false;
	}

	@Override
	public boolean tienePermisoUsuario(String nombrePermiso, Usuario usuario) {
		if(usuario != null){
			log.info("Iniciando tienePermisoUsuario en UsuarioServiceImpl para el permiso: "+ nombrePermiso+ "\n y el usuario: "+usuario.getNombreUsuario());
			for (Rol rol : usuario.getRoles()) {
				if (rol.getDescripcion().equals(nombrePermiso)) {
					log.info("El usuario cuenta con el permiso: "+nombrePermiso);
					return true;
				}
			}
		}		
		log.info("El usuario no cuenta con el permiso: "+nombrePermiso);
		return false;
	}
	
	@Override
	public boolean tienePermisoUsuario(String[] nombrePermisos, Usuario usuario) {
		for (String nombrePermiso : nombrePermisos) {
			if (tienePermisoUsuario(nombrePermiso, usuario)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean tienePermisoUsuarioActual(String nombrePermiso) {
		log.info("Iniciando tienePermisoUsuarioActual en UsuarioServiceImpl para el permiso: "+nombrePermiso);
		return tienePermisoUsuario(nombrePermiso, getUsuarioActual());
	}

	
	@Override
	public boolean tienePermisoUsuarioActual(String[] nombrePermisos) {
		for (String nombrePermiso : nombrePermisos) {
			if (tienePermisoUsuarioActual(nombrePermiso)) {
				return true;
			}
		}
		return false;
	}
	
	@Override
	public Agente getAgenteUsuarioActual() {
		UserDTO padre = null;
		Agente agente = null;
		Usuario usuarioActual = getUsuarioActual();
		
		//si el rol es ejectivo
		if(tieneRol(new String[]{"Rol_M2_Conducto","Rol_M2_Ejecutivo_Sucursales", "Rol_M2_Empresa",}, usuarioActual)){
			try{
				padre = asmRemote.getParent(usuarioActual.getId());
				agente = agenteMidasService.findByCodigoUsuario(padre.getUsername());
			}catch(Exception ex){
				throw new NegocioEJBExeption("_","El usuario no tiene un agente autorizado asignado, o la c\u00E9dula del agente est\u00E1 vencida.");
			}							
		}else{
			agente = agenteMidasService.findByCodigoUsuario(usuarioActual.getNombreUsuario());
		}
		boolean obligatorio = tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")
				|| tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")
				|| tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Conducto_Agentes_Promotoria_Usuario_Actual");
		if (agente == null && obligatorio)  {
			throw new NegocioEJBExeption("_","El usuario no tiene un agente autorizado asignado, o la c\u00E9dula del agente est\u00E1 vencida.");
		}
		return agente;
	}
	
	@Override
	public Agente getAgenteUsuarioActual(Usuario usuarioActual) {
		UserDTO padre = null;
		Agente agente = null;
		
		//si el rol es ejectivo
		if(tieneRol(new String[]{"Rol_M2_Conducto","Rol_M2_Ejecutivo_Sucursales", "Rol_M2_Empresa",}, usuarioActual)){
			try{
				padre = asmRemote.getParent(usuarioActual.getId());
				agente = agenteMidasService.findByCodigoUsuario(padre.getUsername());
			}catch(Exception ex){
				throw new NegocioEJBExeption("_","El usuario no tiene un agente autorizado asignado, o la c\u00E9dula del agente est\u00E1 vencida.");
			}							
		}else{
			agente = agenteMidasService.findByCodigoUsuario(usuarioActual.getNombreUsuario());
		}
		boolean obligatorio = tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agente_Usuario_Actual")
				|| tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Agente_Agentes_Promotoria_Usuario_Actual")
				|| tienePermisoUsuarioActual("FN_M2_General_Visibilidad_Conducto_Agentes_Promotoria_Usuario_Actual");
		if (agente == null && obligatorio)  {
			throw new NegocioEJBExeption("_","El usuario no tiene un agente autorizado asignado, o la c\u00E9dula del agente est\u00E1 vencida.");
		}
		return agente;
	}
	
	@Override
	public List<Usuario> buscarUsuarioPorNombreCompleto(String likeCompleteName) {
		List<UserDTO> usuarios = new ArrayList<UserDTO>();
		List<UserDTO> usuariosTmp = new ArrayList<UserDTO>();
		try {
			usuariosTmp = asmRemote.getUserList(5, null, likeCompleteName);
			usuarios.addAll(usuariosTmp);
		} catch (Exception e) {
		}
		return midasASMTranslator.obtieneListaUsuariosMidasSinRoles(usuarios);
	}

	@Override
	public Usuario login(LoginParameter loginParameter) {
		Set<ConstraintViolation<LoginParameter>> constraintViolations = validator.validate(loginParameter);
		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
		}
		LoginParametersDTO loginParametersDTO = new LoginParametersDTO();
		if (loginParameter.getApplicationId() != null) {
			loginParametersDTO.setApplicationId(loginParameter.getApplicationId());
		}
		loginParametersDTO.setUsername(loginParameter.getUsuario());
		loginParametersDTO.setPassword(loginParameter.getPassword());
		loginParametersDTO.setUserIPAddress(loginParameter.getIpAddress());
		loginParametersDTO.setJSessionId("-1");
		loginParametersDTO.setRegistrationId(loginParameter.getRegistrationId());
		loginParametersDTO.setDeviceApplicationId(loginParameter.getDeviceApplicationId());
		loginParametersDTO.setDeviceUuid(loginParameter.getDeviceUuid());
		
		try {
			UserDTO userASM = asmRemote.getUser(loginParametersDTO, UserDTO.class);
			UsuarioDTO usuarioDTO = findUsuarioDTO(loginParameter.getUsuario());
			return midasASMTranslator.obtieneUsuarioMidas(userASM, usuarioDTO);
		} catch (SystemException e) {
			if(!"TESTUSER".equals(loginParameter.getUsuario())){
				log.error("Error al autenticar: ",  e);
			}
			String errorMessage;
			if (e.getErrorCode() == ASMConstants.USER_EMAIL_NOT_CONFIRMED) {
				errorMessage = "El email no ha sido confirmado. Revise su correo y de click en la liga para confirmar.";
			} else if (e.getErrorCode() == ASMConstants.GIVEN_PASSWORD_NOT_VALID) {
				errorMessage = "Password incorrecto.";				
			} else if (e.getErrorCode() == ASMConstants.USER_IS_NOT_ACTIVE) {
				errorMessage = "El usuario no esta activo.";
			} else if (e.getErrorCode() == ASMConstants.USER_PASSWORD_IS_EXPIRED) {				
				errorMessage = "El password ha expirado.";
			} else if (e.getErrorCode() == ASMConstants.USER_IS_NOT_PRESENT) {
				errorMessage = "El usuario no existe.";				 
			} else if (e.getErrorCode() == ASMConstants.USER_IS_NOT_A_PORTAL_USER) { 
				errorMessage = "El usuario no es un usuario portal.";				
			} else if (e.getErrorCode() == ASMConstants.USER_HAS_NO_ACCESS_TO_APPLICATION) { 
				errorMessage = "El usuario no tiene acceso a la aplicación.";				
			} else {
				errorMessage = "Ocurrio un error al autenticar.";				
			}
			throw new ApplicationException(errorMessage);
		}
	}
	
	
	@Override
	public AjustadorMovil getAjustadorMovilUsuarioActual() {
		return getAjustadorMovilUsuario(getUsuarioActual());
	}
	
	@Override
	public AjustadorMovil getAjustadorMovilUsuario(Usuario usuario) {
		String jpql = "select m from AjustadorMovil m where m.codigoUsuario = :codigoUsuario";
		TypedQuery<AjustadorMovil> query = entityManager.createQuery(jpql, AjustadorMovil.class);
		query.setParameter("codigoUsuario", usuario.getNombreUsuario());
		List<AjustadorMovil> list = query.getResultList();
		if (list.size() == 0) {
			return null;
		} else if (list.size() == 1) {
			return list.get(0);
		} else {
			throw new RuntimeException("Más de un resultado.");
		}
	}
	
	public AjustadorMovil getAjustadorMovilMidasUsuarioActual(){
		return getAjustadorMovilMidasUsuario(getUsuarioActual());
	}
	
	@Override
	public AjustadorMovil getAjustadorMovilMidasUsuario(Usuario usuario) {
		ServicioSiniestro ajustador = null;
		String jpql = "select m from ServicioSiniestro m where m.claveUsuario = :codigoUsuario";
		TypedQuery<ServicioSiniestro> query = entityManager.createQuery(jpql, ServicioSiniestro.class);
		query.setParameter("codigoUsuario", usuario.getNombreUsuario());
		List<ServicioSiniestro> list = query.getResultList();
		if (list.size() == 0) {
			ajustador = null;
		} else if (list.size() == 1) {
			ajustador = list.get(0);
		} else {
			throw new RuntimeException("Más de un resultado.");
		}
		
		AjustadorMovil ajustadorMovil = null;
		if(ajustador!=null){
			ajustadorMovil = new AjustadorMovil();
			ajustadorMovil.setCodigoUsuario(ajustador.getClaveUsuario());
			ajustadorMovil.setId(ajustador.getId());
			ajustadorMovil.setIdSeycos(ajustador.getIdSeycos());
			ajustadorMovil.setNombre(ajustador.getNombrePersona());
		}
		return ajustadorMovil;
	}

	@Override
	public AjustadorMovil getAjustadorMovilMidasUsuario(Long ajustadorId) {
		ServicioSiniestro ajustador = null;
		String jpql = "select m from ServicioSiniestro m where m.id = :ajustadorId";
		TypedQuery<ServicioSiniestro> query = entityManager.createQuery(jpql, ServicioSiniestro.class);
		query.setParameter("ajustadorId", ajustadorId);
		List<ServicioSiniestro> list = query.getResultList();
		if (list.size() == 0) {
			ajustador = null;
		} else if (list.size() == 1) {
			ajustador = list.get(0);
		} else {
			throw new RuntimeException("Más de un resultado.");
		}
		
		AjustadorMovil ajustadorMovil = null;
		if(ajustador!=null){
			ajustadorMovil = new AjustadorMovil();
			ajustadorMovil.setCodigoUsuario(ajustador.getClaveUsuario());
			ajustadorMovil.setId(ajustador.getId());
			ajustadorMovil.setIdSeycos(ajustador.getIdSeycos());
			ajustadorMovil.setNombre(ajustador.getNombrePersona());
		}
		return ajustadorMovil;
	}

	@Override
	public void deshabilitarToken(String token) {
		asmRemote.disableToken(token);
	}
	
	@Override
	@Deprecated
	public List<String> getRegistrationIds(String nombreUsuario) {
		try {
			return asmRemote.getRegistrationIds(nombreUsuario);
		} catch (com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}
	
	@Override
	public List<String> getRegistrationIds(String nombreUsuario, String deviceApplicationId) {
		try {
			return asmRemote.getRegistrationIds(nombreUsuario, deviceApplicationId);
		} catch (com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}
	
	@Override
	public String getRegistrationId(String nombreUsuario, String deviceApplicationId, String deviceUuid) {
		try {
			return asmRemote.getRegistrationId(nombreUsuario, deviceApplicationId, deviceUuid);
		} catch (com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}
	
	@Override
	public Usuario createPortalUser(CreatePortalUserParameterDTO parameter) {
		UserDTO usuarioASM;
		try {
			usuarioASM = asmRemote.createPortalUser(parameter);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
		UsuarioDTO usuarioDTO = findUsuarioDTO(usuarioASM.getUsername());
		return midasASMTranslator.obtieneUsuarioMidas(usuarioASM, usuarioDTO);
	}
	
	
	@Override
	public void resendConfirmationEmail(Integer userId) {
		try {
			asmRemote.resendConfirmationEmail(userId);
		}catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public void resendConfirmationEmail(ResendConfirmationEmailDTO resendConfirmationEmail) {
		try {
			asmRemote.resendConfirmationEmail(resendConfirmationEmail);
		}catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public int confirmEmail(String confirmationCode) {
		try {
			return asmRemote.confirmEmail(confirmationCode);
		}catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public void changePassword(String oldPassword, String newPassword) {
		try {
			ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO();
			changePasswordDTO.setUsername(getUsuarioActual().getNombreUsuario());
			changePasswordDTO.setOldPassword(oldPassword);
			changePasswordDTO.setNewPassword(newPassword);
			changePasswordDTO.setCheckIfPasswordRepeats(false);
			changePasswordDTO.setCheckLength(true);
			changePasswordDTO.setPasswordExpires(false);
			asmRemote.changePassword(changePasswordDTO);
		}catch(SystemException e) {
			if (e.getErrorCode() == ASMConstants.GIVEN_PASSWORD_NOT_VALID) {
				throw new ApplicationException("Password actual incorrecto.");
			} else if(e.getErrorCode() == ASMConstants.NEW_PASSWORD_INVALID) {
				throw new ApplicationException(e.getMessage());
			} else {
				throw new ApplicationException(e.getMessage());
			}
		}
	}
	
	public void changePassword2(String newPassword) {
		try {
			ChangePasswordDTO changePasswordDTO = new ChangePasswordDTO();
			changePasswordDTO.setUsername(getUsuarioActual().getNombreUsuario());
			changePasswordDTO.setNewPassword(newPassword);
			changePasswordDTO.setCheckIfPasswordRepeats(false);
			changePasswordDTO.setCheckLength(true);
			changePasswordDTO.setPasswordExpires(false);
			changePasswordDTO.setCheckOldPassword(false);
			asmRemote.changePassword(changePasswordDTO);
		}catch(SystemException e) {
			if(e.getErrorCode() == ASMConstants.NEW_PASSWORD_INVALID) {
				throw new ApplicationException(e.getMessage());
			} else {
				throw new ApplicationException(e.getMessage());
			}
		}
	}
	
	@Override
	public void resetPassword(String token, String newPassword) {
		try {
			asmRemote.resetPassword(token, newPassword);
		}catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}
	
	@Override
	public void sendResetPasswordToken(Integer userId) {
		try {
			asmRemote.sendResetPasswordToken(userId);
		}catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public void sendResetPasswordToken(SendResetPasswordTokenDTO sendResetPasswordToken) {
		try {
			asmRemote.sendResetPasswordToken(sendResetPasswordToken);
		}catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}		
	}
		
	@Override
	public void updateUser(UserUpdateDTO userUpdate) {
		try {
			asmRemote.updateUser(userUpdate);
		}catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public void activateDeactivateUser(Integer userId, Boolean active,
			Integer administratorUserId) {
		try {
	//		asmRemote.activateDeactivateUser(userId, active, administratorUserId);
		}catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public List<UserInboxEntryDTO> getUserList(
			SearchUserParametersDTO searchParameters) {
		try {
			return asmRemote.getUserList(searchParameters);
		} catch (SystemException e) {
			throw new ApplicationException(e.getMessage());
		}
	}


	@Override
	public Usuario createPasswordUser(CreatePasswordUserParameterDTO parameter) {
		UserDTO usuarioASM;
		try {
			usuarioASM = asmRemote.createPasswordUser(parameter);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
		UsuarioDTO usuarioDTO = findUsuarioDTO(usuarioASM.getUsername());
		return midasASMTranslator.obtieneUsuarioMidas(usuarioASM, usuarioDTO);		
	}

	@Override
	public Integer findUserIdByUsername(String username) {
		try {
			
			if (username == null) {
				
				return null;
				
			}
			
			return asmRemote.findUserIdByUsername(username);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}
	
	@Override
	public Integer findRoleId(Integer applicationId, String roleName) {
		try {
			return asmRemote.findRoleId(applicationId, roleName);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}
	
	@Override
	public boolean isUserNeedsToSetPassword(Integer userId) {
		try {
			return asmRemote.isUserNeedsToSetPassword(userId);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public String getResetPasswordUrl(Integer userId) {
		try {
			return asmRemote.getResetPasswordUrl(userId);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}
	
	@Override
	public boolean isUserHasAccessToApplication(Integer userId,
			Integer applicationId) {
		try {
			return asmRemote.isUserHasAccessToApplication(userId, applicationId);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public boolean isUserPortalUser(Integer userId) {
		try {
			return asmRemote.isUserPortalUser(userId);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}

	@Override
	public List<ApplicationDTO> getUserApplications(Integer userId) {
		try {
			return asmRemote.getUserApplications(userId);
		} catch(com.asm.dto.ApplicationException e) {
			throw new ApplicationException(e.getErrors(), e.getFieldErrors());
		}
	}
	
	@Override
	public boolean isTokenActive(String token){
		return asmRemote.isTokenActive(token);
	}
	
	@Override
	public UserDTO getUserDTOById(Integer userId) {
		
		try {
			
			if (userId == null) {
				
				return null;
				
			}
			
			return asmRemote.getUser(userId, UserDTO.class);
		} catch (SystemException e) {
			throw new ApplicationException(e.toString());
		}
		
	}
	
	
	@Override
	public void validateToken( String token ) throws SystemException{
		if (StringUtils.isBlank(token)){
			throw new SystemException("El token de sesión es requerido");
		} else{
			if(!asmRemote.isTokenActive(token))
				throw new SystemException("El token "+token+" es un token inactivo.");
		}
	}
	
	
	@Override
	public void updateMidasUser(UserDTO updateUser, UserDTO administrator,
			List<RoleDTO> roleList) {
		
		try {
			asmRemote.updateMidasUser(updateUser, administrator, roleList);
		} catch (SystemException e) {
			throw new ApplicationException(e.toString());
		}
		
	}
	
	@Override
	public RoleDTO getRoleDTOById(int id) {
		
		try {
			return roleRemote.getRole(id);
		} catch (SystemException e) {
			throw new ApplicationException(e.toString());
		}
		
	}
	

}
