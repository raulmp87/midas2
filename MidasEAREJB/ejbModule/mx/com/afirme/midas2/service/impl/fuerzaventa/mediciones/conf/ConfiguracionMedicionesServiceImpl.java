package mx.com.afirme.midas2.service.impl.fuerzaventa.mediciones.conf;

import java.math.BigDecimal;
import java.util.List;

import javax.crypto.Cipher;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.xml.bind.DatatypeConverter;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.fuerzaventa.mediciones.conf.ConfiguracionMedicionesDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf.ConfiguracionMedicionesAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.mediciones.conf.ConfiguracionMedicionesPromotoria;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.service.fuerzaventa.mediciones.conf.ConfiguracionMedicionesService;

@Stateless
public class ConfiguracionMedicionesServiceImpl implements ConfiguracionMedicionesService {
	
	private static final BigDecimal LIMITE_CORREOS_AGENTES = new BigDecimal("86001");
	
	private static final BigDecimal LIMITE_CORREOS_PROMOTORES = new BigDecimal("86002");
	
	private static final BigDecimal CORREOS_ADICIONALES = new BigDecimal("86003");
	
	private static final BigDecimal RUTA_BASE_REPORTES = new BigDecimal("86004");
	
	private static final BigDecimal USUARIO_RUTA_BASE_REPORTES = new BigDecimal("86005");
	
	private static final BigDecimal PASSWORD_RUTA_BASE_REPORTES = new BigDecimal("86006");
	
	private static final String KEY = "12345678";
	
	private static final String ENCODING = "UTF8";
	
	/*
	
	    Configuracion Grupo 'MEDICIONES AGENTES Y PROMOTORES'
	 	
	 	86001 'LIMITE DE CORREOS A ENVIAR A AGENTES'
	
	   	86002 'LIMITE DE CORREOS A ENVIAR A PROMOTORES'
	        
	    86003 'CORREOS ADICIONALES (EN CASO DE QUE EXISTAN)'
	    
	    86004 'RUTA BASE REPORTES'
		 
	*/
	
	@Override
	public void actualizarAgente (Long claveAgente, String correo) {
	
		if (claveAgente == null || correo == null || correo.trim().equals("")) {
			
			throw new RuntimeException("Parametros invalidos");
			
		}	
		
		ConfiguracionMedicionesAgente configAgente = getConfigAgente(claveAgente);
		
		configAgente.setEmail(correo);
		
		entidadDao.update(configAgente);
		
	}
	
	@Override
	public void actualizarPromotoria (Long clavePromotoria, String correo) {
		
		if (clavePromotoria == null || correo == null || correo.trim().equals("")) {
			
			throw new RuntimeException("Parametros invalidos");
			
		}
				
		ConfiguracionMedicionesPromotoria configPromotoria = getConfigPromotoria(clavePromotoria);
		
		configPromotoria.setEmail(correo);
		
		entidadDao.update(configPromotoria);
				
	}
		
	@Override
	public void agregarAgente(ConfiguracionMedicionesAgente configAgente) {
		
		if (configAgente == null || configAgente.getAgente() == null || configAgente.getAgente().getIdAgente() == null 
				|| configAgente.getEmail() == null || configAgente.getEmail().trim().equals("")) {
		
			throw new RuntimeException("Parametros invalidos");
		
		}
		Integer size = confDao.getSize(configAgente.getClass().getSimpleName());
		
		Integer limiteCorreos = getLimiteCorreosAgentes();
		
		if (size >= limiteCorreos) {
		
			throw new RuntimeException("Ha alcanzado el limite de Agentes a configurar");
			
		}
		
		ConfiguracionMedicionesAgente originalConfigAgente = getConfigAgente(configAgente.getAgente().getIdAgente());
		
		if (originalConfigAgente != null) {
			
			throw new RuntimeException("El agente ya se encuentra configurado");
						
		}
		
		configAgente.setAgente(doGetAgente(configAgente.getAgente().getIdAgente()));
		
		if (configAgente.getAgente() == null) {
			
			throw new RuntimeException("El agente no existe");
			
		}
		
		configAgente.setHabilitado(true);
				
		entidadDao.persist(configAgente);
		
	}

	@Override
	public void agregarCorreosAdicionales(String correosAdicionales) {
		
		ParametroGeneralDTO parametro = getParametroGeneral(CORREOS_ADICIONALES);
		
		parametro.setValor((correosAdicionales!=null?correosAdicionales.trim():""));
		
		parametroGeneralService.update(parametro);
				
	}

	@Override
	public void agregarPromotoria(ConfiguracionMedicionesPromotoria configPromotoria) {
		
		if (configPromotoria == null || configPromotoria.getPromotoria() == null || configPromotoria.getPromotoria().getIdPromotoria() == null 
				|| configPromotoria.getEmail() == null || configPromotoria.getEmail().trim().equals("")) {
		
			throw new RuntimeException("Parametros invalidos");
			
		}
		
		Integer size = confDao.getSize(configPromotoria.getClass().getSimpleName());
		
		Integer limiteCorreos = getLimiteCorreosPromotores();
		
		if (size >= limiteCorreos) {
		
			throw new RuntimeException("Ha alcanzado el limite de Promotorias a configurar");
			
		}
		
		ConfiguracionMedicionesPromotoria originalConfigPromotoria = getConfigPromotoria(configPromotoria.getPromotoria().getIdPromotoria());
		
		if (originalConfigPromotoria != null) {
			
			throw new RuntimeException("La promotoria ya se encuentra configurada");
						
		}
			
		configPromotoria.setPromotoria(doGetPromotoria(configPromotoria.getPromotoria().getIdPromotoria()));
		
		if (configPromotoria.getPromotoria() == null) {
			
			throw new RuntimeException("La promotoria no existe");
			
		}
		
		configPromotoria.setHabilitado(true);
				
		entidadDao.persist(configPromotoria);
		
	}

	@Override
	public void eliminarAgente(Long claveAgente) {
		
		ConfiguracionMedicionesAgente configAgente = getConfigAgente(claveAgente);
		
		entidadDao.remove(configAgente);
		
	}

	@Override
	public void eliminarPromotoria(Long clavePromotoria) {
		
		ConfiguracionMedicionesPromotoria configPromotoria = getConfigPromotoria(clavePromotoria);
		
		entidadDao.remove(configPromotoria);
		
	}

	@Override
	public void habilitarAgente(Long claveAgente) {
		
		ConfiguracionMedicionesAgente configAgente = getConfigAgente(claveAgente);
		
		configAgente.setHabilitado((configAgente.getHabilitado() != null ? !configAgente.getHabilitado() : true));
		
		entidadDao.update(configAgente);
		
	}

	@Override
	public void habilitarPromotoria(Long clavePromotoria) {
		
		ConfiguracionMedicionesPromotoria configPromotoria = getConfigPromotoria(clavePromotoria);
		
		configPromotoria.setHabilitado((configPromotoria.getHabilitado() != null ? !configPromotoria.getHabilitado() : true));
		
		entidadDao.update(configPromotoria);
		
	}
	
	@Override
	public String obtenerCorreosAdicionales() {
		
		return parametroGeneralService.getValueByCode(CORREOS_ADICIONALES);
		
	}
	
	@Override
	public String obtenerRutaBaseReportes() {
		
		return parametroGeneralService.getValueByCode(RUTA_BASE_REPORTES);
		
	}
	
	@Override
	public String obtenerUsuarioRutaBaseReportes() {
		
		return parametroGeneralService.getValueByCode(USUARIO_RUTA_BASE_REPORTES);
		
	}
	
	@Override
	public String obtenerPasswordRutaBaseReportes() {
		
		try {
			
			return desencriptar(parametroGeneralService.getValueByCode(PASSWORD_RUTA_BASE_REPORTES));
						
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
		
	}
	
	
	private Integer getLimiteCorreosAgentes() {
		
		return Integer.parseInt(parametroGeneralService.getValueByCode(LIMITE_CORREOS_AGENTES));
		
	}
	
	private Integer getLimiteCorreosPromotores() {
		
		return Integer.parseInt(parametroGeneralService.getValueByCode(LIMITE_CORREOS_PROMOTORES));
		
	}
	
	public ConfiguracionMedicionesAgente getConfigAgente(Long claveAgente) {
		
		ConfiguracionMedicionesAgente configAgente = null;
		
		List<ConfiguracionMedicionesAgente> configAgenteList = new ArrayListNullAware<ConfiguracionMedicionesAgente>();
		
		configAgenteList = entidadDao.findByProperty(ConfiguracionMedicionesAgente.class, "agente.idAgente", claveAgente);
		
		if (configAgenteList != null && !configAgenteList.isEmpty()) {
			
			configAgente = configAgenteList.get(0);
						
		}
		
		return configAgente;
		
	}
	
	private Agente doGetAgente(Long claveAgente) {
		
		Agente agente = null;
		
		List<Agente> agenteList = new ArrayListNullAware<Agente>();
		
		agenteList = entidadDao.findByProperty(Agente.class, "idAgente", claveAgente);
		
		if (agenteList != null && !agenteList.isEmpty()) {
			
			agente = agenteList.get(0);
						
		}
		
		return agente;
		
	}
	
	public ConfiguracionMedicionesPromotoria getConfigPromotoria(Long clavePromotoria) {
		
		ConfiguracionMedicionesPromotoria configPromotoria = null;
		
		List<ConfiguracionMedicionesPromotoria> configPromotoriaList = new ArrayListNullAware<ConfiguracionMedicionesPromotoria>();
		
		configPromotoriaList = entidadDao.findByProperty(ConfiguracionMedicionesPromotoria.class, "promotoria.idPromotoria", clavePromotoria);
		
		if (configPromotoriaList != null && !configPromotoriaList.isEmpty()) {
			
			configPromotoria = configPromotoriaList.get(0);
						
		}
		
		return configPromotoria;
		
	}
	
	private Promotoria doGetPromotoria(Long clavePromotoria) {
		
		Promotoria promotoria = null;
		
		List<Promotoria> promotoriaList = new ArrayListNullAware<Promotoria>();
		
		promotoriaList = entidadDao.findByProperty(Promotoria.class, "idPromotoria", clavePromotoria);
		
		if (promotoriaList != null && !promotoriaList.isEmpty()) {
			
			promotoria = promotoriaList.get(0);
						
		}
		
		return promotoria;
		
	}
	
	private ParametroGeneralDTO getParametroGeneral(BigDecimal codigo) {
				
		List<ParametroGeneralDTO> parametros =  parametroGeneralService.findByProperty("id.codigoParametroGeneral", codigo);
		
		if (parametros != null && parametros.size() > 0) {
			
			return parametros.get(0);
			
		}
		
		return null;
		
	}
	
	  
	@SuppressWarnings("unused")
	private String encriptar(String textoAEncriptar) throws Exception {
		
		byte[] encodedBytes = encryptDecrypt (DatatypeConverter.printHexBinary(textoAEncriptar.getBytes(ENCODING)), 
				Cipher.ENCRYPT_MODE);
		
		return DatatypeConverter.printHexBinary(encodedBytes);
		
	}
	
	private String desencriptar (String textoEncriptado) throws Exception {
		
		byte[] decodedBytes = encryptDecrypt (textoEncriptado, Cipher.DECRYPT_MODE);
		
		return new String(decodedBytes, ENCODING);
		
	}
	
	
	private byte[] encryptDecrypt (String hexBinaryText, int mode) throws Exception {
		
		String algorithm = "DES/ECB/PKCS5Padding";
		
	    SecretKeyFactory keyFactory = SecretKeyFactory.getInstance("DES");

	    byte[] keyBytes = KEY.getBytes(ENCODING);
	   
	    DESKeySpec keySpec = new DESKeySpec(keyBytes);
	   
	    SecretKey key = keyFactory.generateSecret(keySpec);
	    
	    Cipher cipher = Cipher.getInstance(algorithm);
	   	    
	    cipher.init(mode, key);
	   	    
	    return cipher.doFinal(DatatypeConverter.parseHexBinary(hexBinaryText));
	    		
	}
		
		
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralService;
	
	@EJB
	private EntidadDao entidadDao;
	
	@EJB
	private ConfiguracionMedicionesDao confDao;

}
