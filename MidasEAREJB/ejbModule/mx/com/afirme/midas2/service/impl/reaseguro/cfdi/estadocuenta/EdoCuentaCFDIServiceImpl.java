package mx.com.afirme.midas2.service.impl.reaseguro.cfdi.estadocuenta;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.poi.hssf.model.InternalWorkbook;
import org.apache.poi.hssf.usermodel.DVConstraint;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFCellStyle;
import org.apache.poi.hssf.usermodel.HSSFComment;
import org.apache.poi.hssf.usermodel.HSSFDataValidation;
import org.apache.poi.hssf.usermodel.HSSFName;
import org.apache.poi.hssf.usermodel.HSSFPatriarch;
import org.apache.poi.hssf.usermodel.HSSFRichTextString;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.DataValidationConstraint.ValidationType;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.ss.util.CellRangeAddressList;
import org.apache.poi.ss.util.CellReference;
import org.joda.time.DateTime;
import org.joda.time.Interval;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

import mx.com.afirme.midas.catalogos.coasegurador.Coasegurador;
import mx.com.afirme.midas.catalogos.coasegurador.CoaseguradorHistorico;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcorredor.ReaseguradorCorredorDTOHistorico;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dao.reaseguro.cfdi.estadocuenta.EdoCuentaCFDIDao;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.Concepto;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.ConceptoHistorico;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.Contrato;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.ContratoConceptoHistorico;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.ContratoHistorico;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.Folio;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.SubRamo;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta.ConceptoEstadoCuenta;
import mx.com.afirme.midas2.domain.reaseguro.cfdi.estadocuenta.EstadoCuenta;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.RegistroEC;
import mx.com.afirme.midas2.service.OracleCurrentDateService;
import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;
import mx.com.afirme.midas2.service.reaseguro.cfdi.estadocuenta.EdoCuentaCFDIService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class EdoCuentaCFDIServiceImpl implements EdoCuentaCFDIService {

	@Override
	public ControlDescripcionArchivoDTO procesarInfo(BigDecimal idToControlArchivo, String claveNegocio) {
		
		HashMap<String,Object> properties;
		Date fechaRegistroPlantilla;
		HSSFCell cell;
		Plantilla plantilla;
		HSSFSheet sheet;
		HSSFWorkbook workbook;
		ContratoHistorico contratoHist;
		List<RegistroEC> registros;
		List<ControlDescripcionArchivoDTO> archivosGenerados = new ArrayListNullAware<ControlDescripcionArchivoDTO>();
		List<ConceptoHistorico> conceptosHeader = new ArrayListNullAware<ConceptoHistorico>();
		List<ConceptoHistorico> conceptos = new ArrayListNullAware<ConceptoHistorico>();
		List<ContratoConceptoHistorico> contratoConceptoHistoricoList = new ArrayListNullAware<ContratoConceptoHistorico>();
		ConceptoHistorico conceptoHistorico;
		ControlDescripcionArchivoDTO archivoErrores;
		boolean esEstandar = false;
		
		try {
		
			//Cargar el archivo
			workbook = getArchivoDatos(idToControlArchivo);
							
			sheet = workbook.getSheet(CANCELACIONES);
			
			if (sheet != null) return procesaInfoCancelaciones(sheet); //Es de cancelaciones (No necesita validaciones especiales)
		
			//Validaciones de datos de sistema
			//(como el tipo de negocio, tipo de plantilla, que se encuentren los datos de ayuda, que exista al menos un indicador de registro, etc.)
			
			sheet = workbook.getSheet(DATOS_SISTEMA);
			
			cell = getCellAt(sheet, 0, 0); //Plantilla
			plantilla = getPlantilla(getCellValue(cell, true, Integer.class));
			
			cell = getDownCell(cell);
			
			properties = new HashMap<String,Object>();
			properties.put("idOrigen", getCellValue(cell, true, BigDecimal.class));
			
			cell = getDownCell(cell);
			
			try {
				fechaRegistroPlantilla = new SimpleDateFormat(FORMATO_FECHA_REGISTRO_DET).parse(getCellValue(cell, true, String.class));
			} catch (ParseException e) {
				throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.campo.fecha"));
			} 
					
			contratoHist = entidadService.findByProperties(ContratoHistorico.class, properties, fechaRegistroPlantilla).get(0);
			
			if (!contratoHist.getClaveNegocio().equals(claveNegocio)) {
				throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.negocio.diferente"));
			}
					
			//Datos usuario
			sheet = workbook.getSheetAt(1);
			
			initSheet(sheet);
			
			conceptosHeader = new ArrayListNullAware<ConceptoHistorico>();
			
			conceptos = new ArrayListNullAware<ConceptoHistorico>();
			
			esEstandar = (plantilla.equals(Plantilla.EC_ESTANDAR)?true:false);
			
			properties = new HashMap<String,Object>();
			properties.put("id.contratoId", contratoHist.getIdOrigen());
			properties.put("concepto.esBase", false);
			
			if (!esEstandar) {
				properties.put("concepto.esEstandar", false);
			}
			
			contratoConceptoHistoricoList = entidadService.findByProperties(ContratoConceptoHistorico.class, properties, fechaRegistroPlantilla);
			
			for (ContratoConceptoHistorico contratoConceptoHistorico : contratoConceptoHistoricoList) {
				
				properties = new HashMap<String,Object>();
				properties.put("idOrigen", contratoConceptoHistorico.getConcepto().getId());
				
				conceptoHistorico = entidadService.findByProperties(ConceptoHistorico.class, properties, fechaRegistroPlantilla).get(0);
							
				if (conceptoHistorico.getEsHeader().equals(true)) {
					conceptosHeader.add(conceptoHistorico);
				} else {
					conceptos.add(conceptoHistorico);
				}
				
			}
			
			Collections.sort(conceptosHeader);
			
			Collections.sort(conceptos);
			
			registros = iniciaRegistros(conceptosHeader.get(0).getClave(), sheet);
					
			if (registros.size() == 0) {
				throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.plantilla.vacia"));
			}
					
			for (RegistroEC registro : registros) {
				//Si pasa la validacion el XML y el PDF con la rep. impresa generados se agregan en un listado de documentos procesados
				procesaRegistro (registro, sheet, contratoHist, conceptosHeader, conceptos, esEstandar, fechaRegistroPlantilla);
				
				if (registro.getProcesado()) {
					archivosGenerados.addAll(registro.getRepresentacionesImpresas());
					archivosGenerados.addAll(registro.getCfdiGenerados());
				}
			}
					
			//El worksheet de errores se agrega al listado de documentos procesados
			archivoErrores = generaArchivoErroresEC (registros, sheet);
					
			if (archivoErrores != null) {
				archivosGenerados.add(archivoErrores);
			}
		
		} catch (RuntimeException ex) {
			archivosGenerados.add(errorGeneral(ex.getMessage()));
		}
		
		//Regresa el zip con los documentos procesados
		return generaZip(archivosGenerados);
	}
	
	@Override
	public ControlDescripcionArchivoDTO obtenerPlantilla(Integer tipoPlantilla, Contrato contrato) {
		
		ControlDescripcionArchivoDTO archivo = new ControlDescripcionArchivoDTO();
		
		Date fechaCreacion = oracleCurrentDateService.getCurrentDate().toDate();
		
		archivo.setExtension(CONTENT_TYPE_EXCEL); // contentType
				
		Plantilla plantilla = getPlantilla(tipoPlantilla);
				
		switch (plantilla) {
			case EC_ABREVIADA: 
			case EC_ESTANDAR: {
				
				contrato = entidadService.getReference(Contrato.class, contrato.getId());
				
				//Proceso para actualizar los conceptos en caso de una renovacion de contrato
				fechaCreacion = revisaRenovacionContrato(contrato, fechaCreacion);
				
				archivo.setInputStream(getInputStreamEC(contrato, plantilla, fechaCreacion));
				
				break;
			}
			case CANCELACIONES: {
				
				archivo.setInputStream(getInputStreamCAN());
				
				contrato = null;
				
				break;
			}
		}
		
		archivo.setNombreArchivo(formaNombreArchivo(plantilla, contrato, EXTENSION_EXCEL, fechaCreacion));
						
		return archivo;
	}
	
	@Override
	public List<EstadoCuenta> filtrar(Contrato contrato, EstadoCuenta estadoCuenta, BigDecimal folio) {
		
		ReaseguradorCorredorDTOHistorico reasegurador = null;
		CoaseguradorHistorico coasegurador = null;
		List<EstadoCuenta> estadosCuentaFinal = new ArrayListNullAware<EstadoCuenta>();
		Folio folioFiltrado = null;
		Integer resultadoEncontrado;
		List<Folio> foliosAux = new ArrayListNullAware<Folio>();
		List<EstadoCuenta> estadosCuenta = edoCuentaCFDIDao.filtrar(contrato, estadoCuenta, folio);
				
		for (EstadoCuenta edoCuenta : estadosCuenta) {
						
			reasegurador = entidadService.findById(ReaseguradorCorredorDTOHistorico.class, edoCuenta.getIdReaseguradorCoaseguradorHist());
			
			if (reasegurador != null) {
				
				edoCuenta.setNombreReasCorr(reasegurador.getNombre());
				
			} else {
				
				coasegurador = entidadService.findById(CoaseguradorHistorico.class, edoCuenta.getIdReaseguradorCoaseguradorHist());
				
				if (coasegurador != null) {
					
					edoCuenta.setNombreReasCorr(coasegurador.getNombre());
					
				}
				
			}
			
			foliosAux = new ArrayListNullAware<Folio>();
			edoCuenta.getFoliosD().clear();
			
			if(folio != null) {
				
				folioFiltrado = new Folio();
				
				folioFiltrado.setFolio(folio);
				
				foliosAux.add(folioFiltrado);
				
			} else {

				foliosAux.addAll(edoCuenta.getFolios());
			}
			
			//FIXME: En una oportunidad cambiar todo esto por un native query
			for (Folio folioD : foliosAux) {
				
				resultadoEncontrado = edoCuentaCFDIDao.validaFolio (null, null, folioD.getFolio());
				
				if (resultadoEncontrado != null && resultadoEncontrado.intValue() > 0) {
					//El CFDI esta vigente
					edoCuenta.getFoliosD().add(folioD);
				}
			}
			
			if (!edoCuenta.getFoliosD().isEmpty()) {
				estadosCuentaFinal.add(edoCuenta);
			}
			
		}
				
		return estadosCuentaFinal;
	}

	@Override
	public List<String> findReasCoas() {
				
		List<String> respuesta = new ArrayListNullAware<String>();
		List<ReaseguradorCorredorDTO> reaseguradores = entidadService.findAll(ReaseguradorCorredorDTO.class);
		List<Coasegurador> coaseguradores = entidadService.findAll(Coasegurador.class);
		
		for (ReaseguradorCorredorDTO reasegurador : reaseguradores) {
			respuesta.add(reasegurador.getNombre());
		}
		
		for (Coasegurador coasegurador : coaseguradores) {
			respuesta.add(coasegurador.getNombre());
		}
		
		Collections.sort(respuesta);
		
		return respuesta;
	}

	@Override
	public ControlDescripcionArchivoDTO imprimir(BigDecimal folio, Integer esPDF) {
		
		return getComprobante(EstadoCuenta.SERIE + " " + folio, (esPDF.intValue()==1?true:false));
	
	}
	
	private Date revisaRenovacionContrato(Contrato contrato, Date fechaCreacion) {
		
		DateTime fechaActual = TimeUtils.getDateTime(fechaCreacion);
		DateTime fechaSiguienteRenovacion = TimeUtils.getDateTime(contrato.getUltimaRenovacion()).plusYears(1);
		GregorianCalendar cal = null;
		
		if (fechaActual.isAfter(fechaSiguienteRenovacion)) {
			
			contrato.setUltimaRenovacion(fechaSiguienteRenovacion.toDate());
			
			ejecutaRenovacionConceptosContrato(contrato);
			
			entidadService.update(contrato);
			
			cal = new GregorianCalendar();
			
			cal.setTime(fechaCreacion);
			cal.add(GregorianCalendar.MINUTE, 5);
			fechaCreacion = cal.getTime();
		}
		
		return fechaCreacion;
			
	}
	
	@SuppressWarnings("unchecked")
	private void ejecutaRenovacionConceptosContrato(Contrato contrato) {
		
		List<Concepto> conceptosBase = new ArrayListNullAware<Concepto>();
		
		Predicate predicado = new Predicate() {
			public boolean evaluate(Object arg0) {
				return ((Concepto) arg0).getEsBase();
			}};
				
		conceptosBase = (List<Concepto>) CollectionUtils.select(contrato.getConceptos(), predicado);
		
		if (!conceptosBase.isEmpty()) {
			
			Collections.sort(contrato.getConceptos());
						
			ejecutaRenovacionConceptosContrato(conceptosBase, contrato, Concepto.BASE_PRIMAS);
			
			ejecutaRenovacionConceptosContrato(conceptosBase, contrato, Concepto.BASE_OTROS);
			
		}
		
	}
	
	
	@SuppressWarnings("unchecked")
	private void ejecutaRenovacionConceptosContrato (List<Concepto> conceptosBase, Contrato contrato, String baseConcepto) {
		
		List<Concepto> conceptosBaseGrupo = new ArrayListNullAware<Concepto>();
		List<Concepto> conceptosFabricados = new ArrayListNullAware<Concepto>();
		List<Concepto>  nuevoConceptoList = new ArrayListNullAware<Concepto>();
		HashMap<String,Object> properties = null;
		Predicate predicado = null;
		Concepto nuevoConcepto = null;
		String filtro = "";
		int secuencia = 100;
		DateTime fecha = null;
		
		Predicate predicadoFabricadosGenerales = new Predicate() {
			public boolean evaluate(Object arg0) {
				
				Concepto evaluado = (Concepto) arg0;
				
				return (evaluado.equals(REGEX_PREFIJO + Concepto.BASE_PRIMAS  + REGEX_SUFIJO) 
					|| evaluado .equals(REGEX_PREFIJO + Concepto.BASE_OTROS  + REGEX_SUFIJO))
					&& !evaluado.getEsBase();
			}};
		
		if (Concepto.BASE_PRIMAS.equals(baseConcepto)) {
			
			//Primas (Primas, Comisiones)
			predicado = new Predicate() {
				public boolean evaluate(Object arg0) {
					return ((Concepto) arg0).equals(REGEX_PREFIJO + Concepto.BASE_PRIMAS  + REGEX_SUFIJO);
				}};
				
		} else {
			
			//Otros (Siniestros, Ajustes de Prima)
			predicado = new Predicate() {
				public boolean evaluate(Object arg0) {
					return ((Concepto) arg0).equals(REGEX_PREFIJO + Concepto.BASE_OTROS  + REGEX_SUFIJO);
				}};
				
		}
		
		conceptosBaseGrupo = (List<Concepto>) CollectionUtils.select(conceptosBase, predicado);
						
		for (Concepto conceptoBase : conceptosBaseGrupo) {
			
			conceptosFabricados = new ArrayListNullAware<Concepto>();
			
			filtro = REGEX_PREFIJO + conceptoBase.getClave().substring(0, 3) + REGEX_SUFIJO;
			
			for (Concepto concepto : contrato.getConceptos()) {
				
				if (!concepto.getEsBase() && concepto.equals(filtro)) {
					conceptosFabricados.add(concepto);
				}
				
			}
			
			if (!conceptosFabricados.isEmpty()) {
				
				fecha =  TimeUtils.getDateTime(contrato.getUltimaRenovacion());
				
				Collections.sort(conceptosFabricados);
				
				if (Concepto.BASE_PRIMAS.equals(baseConcepto)) {
					contrato.getConceptos().remove(contrato.getConceptos().indexOf(conceptosFabricados.get(0)));
				}
				
				filtro = conceptosFabricados.get(0).getClave().substring(0, 3) 
				+ new SimpleDateFormat(FORMATO_CVE_CPTO_FABRICADO).format(fecha.toDate());
				
				properties = new HashMap<String,Object>();
				properties.put("clave", filtro);
				
				nuevoConceptoList = entidadService.findByProperties(Concepto.class, properties);
				
				if (nuevoConceptoList != null && !nuevoConceptoList.isEmpty()) {
					
					nuevoConcepto = nuevoConceptoList.get(0);
					
				} else  {
					
					nuevoConcepto = new Concepto();
					nuevoConcepto.setClave(filtro);
									
					nuevoConcepto.setDescripcion(conceptoBase.getDescripcion() + " " + new SimpleDateFormat(FORMATO_SUSCRIPCION).format(fecha.toDate())
							+ "-" + new SimpleDateFormat(FORMATO_SUSCRIPCION).format(fecha.plusYears(1).toDate()));
					
					nuevoConcepto.setEsBase(false);
					nuevoConcepto.setEsHeader(false);
					nuevoConcepto.setEsEstandar(false);
					nuevoConcepto.setEsObligatorio(conceptoBase.getEsObligatorio());
					nuevoConcepto.setEsTimbre(conceptoBase.getEsTimbre());
					nuevoConcepto.setSecuencia(0);
					nuevoConcepto.setTipoValidacion(VALIDACION_NUMERO);
					
				}
								
				contrato.getConceptos().add(contrato.getConceptos().indexOf(conceptosFabricados.get(conceptosFabricados.size() - 1)) + 1, nuevoConcepto);
				
			}
		
		}
		
		//Se reordenan las secuencias
		conceptosFabricados = (List<Concepto>) CollectionUtils.select(contrato.getConceptos(), predicadoFabricadosGenerales);
		
		for (Concepto concepto : contrato.getConceptos()) {
			
			if (conceptosFabricados.contains(concepto)) {
				concepto.setSecuencia(secuencia);
				secuencia++;
			}
			
		}
				
	}
	
	
	private String formaNombreArchivo(Plantilla plantilla, Contrato contrato, String extension, Date fechaCreacion) {
		
		String nombreArchivo = plantilla.getNombre();
		
		if (contrato != null) {
			nombreArchivo += contrato.getClaveNegocio() + contrato.getClave();
		}
		nombreArchivo +="_" +  new SimpleDateFormat(FORMATO_FECHA_REGISTRO).format(fechaCreacion) + "." + extension;
		
		return nombreArchivo;
	
	}
	
	@SuppressWarnings("rawtypes")
	private void initSheet(HSSFSheet sheet) {
	   
		try {
			Field field;
			
			Class c = org.apache.poi.hssf.usermodel.HSSFSheet.class;
		    field = c.getDeclaredField("_patriarch");
			field.setAccessible(true);
		    field.set(sheet, null);
			
			Class wbc = org.apache.poi.hssf.usermodel.HSSFWorkbook.class;
			field = wbc.getDeclaredField("workbook");
		    field.setAccessible(true);
		    Object internalWorkbookObj = field.get(sheet.getWorkbook());
		    InternalWorkbook internalWorkbook = (InternalWorkbook) internalWorkbookObj;
		    internalWorkbook.createDrawingGroup();
		 	
		} catch (Exception e) {
			LOG.error(e);
			throw new RuntimeException (e);
		}
	    
	}
	
	private List<RegistroEC> iniciaRegistros(String claveInicioRegistro, HSSFSheet sheet) {
		
		CellRangeAddress rango;
		HSSFCell cell;
		int rowNumber = 0;
		int secuencia = 1;
		Integer rowNumberInicial = null;
		RegistroEC registro;
		List<RegistroEC> registros = new ArrayListNullAware<RegistroEC>();
		
		do {
			
			rango = new CellRangeAddress(rowNumber, sheet.getLastRowNum(), 0, 0);
			
			cell = findCell(claveInicioRegistro, rango, sheet);
			
			rowNumber = (cell != null? cell.getRowIndex():sheet.getLastRowNum());
			
			if (rowNumberInicial != null) {
				registro = new RegistroEC();
				registro.setRango(new CellRangeAddress(rowNumberInicial, rowNumber - (rowNumber == sheet.getLastRowNum()?0:1), 0, MAX_COLUMNAS - 1));
				registro.setSecuencia(secuencia);
				registros.add(registro);
				secuencia++;
			}
			
			rowNumberInicial = rowNumber;
			
			rowNumber++;
			
		} while (cell != null);
				
		return registros;
	}
	
	private HSSFWorkbook getArchivoDatos(BigDecimal idToControlArchivo) {
		
		HSSFWorkbook workbook = null;
		ControlArchivoDTO controlArchivoDTO = entidadService.findById(ControlArchivoDTO.class, idToControlArchivo);
		String nombreCompletoArchivo = null; 
		String nombreOriginalArchivo = controlArchivoDTO.getNombreArchivoOriginal();
		String extension = (nombreOriginalArchivo.lastIndexOf(".") == -1) ? "" : nombreOriginalArchivo
				.substring(nombreOriginalArchivo.lastIndexOf("."), nombreOriginalArchivo.length());
		
		if (!extension.equalsIgnoreCase(".XLS") && !extension.equalsIgnoreCase(".XLSX"))
			throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.extension.diferente"));
		
		nombreCompletoArchivo = controlArchivoDTO.getIdToControlArchivo().toString() + extension;
			
		File file = new File(sistemaContext.getUploadFolder() + nombreCompletoArchivo);
		try {
			workbook = new HSSFWorkbook(new FileInputStream(file));
		} catch (Exception e) {
			LOG.error(e);
			throw new RuntimeException(e.getMessage());
		}
		
		return workbook;
	}
	
	
	private ControlDescripcionArchivoDTO generaArchivoErroresEC (List<RegistroEC> registros, HSSFSheet sheet) {
		
		ControlDescripcionArchivoDTO archivo = null;
		boolean conErrores = false;
						
		Collections.sort(registros, Collections.reverseOrder());
		
		for (RegistroEC registro : registros) {
			
			if (registro.getProcesado() == false || registro.getErroresGenerales() != null) {
				
				conErrores = true;
			
			} else if (registro.getProcesado() == true) { 
				
				if (registro.getRango().getLastRow() < sheet.getLastRowNum()) {
				
					sheet.shiftRows(registro.getRango().getLastRow() + 1, sheet.getLastRowNum(), 
							((registro.getRango().getLastRow() - registro.getRango().getFirstRow()) + 1) * -1);
				
				} else if (registro.getRango().getLastRow() == sheet.getLastRowNum()) {
					
					for (int i = registro.getRango().getLastRow(); i >= registro.getRango().getFirstRow(); i--) {
						if (sheet.getRow(i) != null) sheet.removeRow(sheet.getRow(i));
					}
					
				}
				
			}
			
		}
		
		if (conErrores) {
			
			archivo = new ControlDescripcionArchivoDTO();
			archivo.setExtension(CONTENT_TYPE_EXCEL); // contentType
			archivo.setInputStream(escribeWorkBook(sheet.getWorkbook()));
			archivo.setNombreArchivo(ERRORES + "." + EXTENSION_EXCEL);
							
			return archivo;
			
		}
		
		return null;
		
	}
	
	private ControlDescripcionArchivoDTO generaZip (List<ControlDescripcionArchivoDTO> archivos) {
		
		ControlDescripcionArchivoDTO archivoZip = null;
		ByteArrayOutputStream bos = null;
		ZipEntry ze = null;
		ZipOutputStream zos = null;
		byte[] buffer = new byte[1024];
		
		try{
			
			//Se escribe el ZIP
			
    		bos = new ByteArrayOutputStream();
    		
    		zos = new ZipOutputStream(bos);
    		
    		for (ControlDescripcionArchivoDTO archivo : archivos) {  
    			
    			//Posicionar inputstream en el inicio para leer bytes completos del archivo
    			archivo.getInputStream().reset();    			
				//Obtener bytes del archivo
    			buffer = IOUtils.toByteArray(archivo.getInputStream());
    			
    			ze = new ZipEntry(archivo.getNombreArchivo());
    			ze.setSize(buffer.length);
        		zos.putNextEntry(ze);
        		
        		zos.write(buffer);

        		zos.closeEntry();
        		
    		}
    						
			
		} catch (Exception ex) {
			LOG.error(ex);
	    	   
	    } finally {
				
			try {
				
				zos.close();
	    		bos.close();
	    		
	    		//Se genera el archivo
				
				archivoZip = new ControlDescripcionArchivoDTO();
				archivoZip.setExtension(CONTENT_TYPE_ZIP); // contentType
				archivoZip.setInputStream(new ByteArrayInputStream(bos.toByteArray()));
				archivoZip.setNombreArchivo(PROCESADO + "_" +  new SimpleDateFormat(FORMATO_FECHA_REGISTRO).format(new Date()) + "." + EXTENSION_ZIP);
			
				
			} catch (IOException e) {
				LOG.error(e);
				
			}
				
		}
		
		return archivoZip;
		
	}
	
	private ControlDescripcionArchivoDTO procesaInfoCancelaciones(HSSFSheet sheet) {
		
		HSSFCell cell = null;
		String llaveComprobante = null;
		String mensajeError = null;
		ControlDescripcionArchivoDTO archivo = null;
		
		if (sheet.getLastRowNum() < 1) {
			throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.plantilla.vacia"));
		}
		
		for (int i = 1; i <= sheet.getLastRowNum(); i++) {
			
			try {
			
				cell = readCellAt(sheet, i, 0);
				
				llaveComprobante = getCellValue(cell, true, String.class);
				
				edoCuentaCFDIDao.generaSolicitudCancelacionCFDI(llaveComprobante);
				
				//Procesado
				setCellStyle(cell, true, COLOR_TITULO_CONCEPTO);
				
				
			} catch (ClassCastException ex) {
				mensajeError = getRecurso("reaseguro.edocuentacfdi.validacion.campo.tipodato");
			} catch (RuntimeException ex) {
				mensajeError = ex.getMessage();
			}
			
			if (mensajeError != null) {
				
				setCellStyle(cell, true, COLOR_ERROR);
				setCellComment(cell, mensajeError);
				
			}
			
		}
		
		archivo = new ControlDescripcionArchivoDTO();
		archivo.setExtension(CONTENT_TYPE_EXCEL); // contentType
		archivo.setInputStream(escribeWorkBook(sheet.getWorkbook()));
		archivo.setNombreArchivo(PROCESADO + "." + EXTENSION_EXCEL);
						
		return archivo;

	}
	
	private ControlDescripcionArchivoDTO errorGeneral(String mensajeError) {
		
		HSSFCell cell = null;
		ControlDescripcionArchivoDTO archivo = null;
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet sheet = workbook.createSheet();
		
		cell = getCellAt(sheet, 0, 0);
		cell.setCellValue(mensajeError);
				
		archivo = new ControlDescripcionArchivoDTO();
		archivo.setExtension(CONTENT_TYPE_EXCEL); // contentType
		archivo.setInputStream(escribeWorkBook(sheet.getWorkbook()));
		archivo.setNombreArchivo(ERRORES + "." + EXTENSION_EXCEL);
						
		return archivo;

	}
	
	
	private void procesaRegistro (RegistroEC registro, HSSFSheet sheet, ContratoHistorico contrato, List<ConceptoHistorico> conceptosHeader
			, List<ConceptoHistorico> conceptos, boolean esEstandar, Date fechaRegistroPlantilla) {
		
		SubRamo subRamo;
		BigDecimal idOrigen;
		BigDecimal idReaseguradorCorredor;
		HashMap<String,Object> properties;
		List<ReaseguradorCorredorDTOHistorico> reaseguradores;
		List<CoaseguradorHistorico> coaseguradores;
		List<ConceptoEstadoCuenta> conceptosECBorrar = new ArrayListNullAware<ConceptoEstadoCuenta>();
		List<BigDecimal> foliosOriginales = new ArrayListNullAware<BigDecimal>();
		Contrato contratoOriginal;
		Coasegurador coasegurador;
		ReaseguradorCorredorDTO reasegurador;
		EstadoCuenta estadoCuenta;
		EstadoCuenta estadoCuentaSet;
		ConceptoEstadoCuenta conceptoEstadoCuenta = null;
		DateTime fechaInicial;
		DateTime fechaFinal;
		DateTime fechaInicialAnt;
		DateTime fechaFinalAnt;
		Date fechaModificacion = null;
		boolean existeIngreso = false;
		boolean existeEgreso = false;
		boolean existePrimaCedida = false;
		Folio folio;
		HSSFCell cell = null;
		BigDecimal debe = null;
		BigDecimal haber = null;
		Integer numero = null;
		boolean folioValido = false;
		Integer monedaIngresada = null;
		Integer remesa = null;
		Integer cheque = null;
		
		try {
			estadoCuentaSet = new EstadoCuenta();
			
			contratoOriginal = entidadService.findById(Contrato.class, contrato.getIdOrigen());
			
			//SubRamo
			subRamo = new SubRamo();
			subRamo.setId(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_RAMO, BigDecimal.class));
			
			if (subRamo.getId() == null && contratoOriginal.getSubRamos().size() == 1) {
				subRamo = contratoOriginal.getSubRamos().get(0);
			}
			
			if (subRamo.getId() != null) {
				subRamo = entidadService.getReference(SubRamo.class, subRamo.getId());
			}
			
			estadoCuentaSet.setSubRamo(subRamo);
			
			//Reasegurador / Coasegurador
			idOrigen = procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_NOMBRE, BigDecimal.class);
			
			properties = new HashMap<String,Object>();
			properties.put("idOrigen", idOrigen);
			
			if (contrato.getClave().equals(Contrato.CLAVE_COASEGURO_VIDA) || contrato.getClave().equals(Contrato.CLAVE_COASEGURO_DANIOS)) { 
				
				if (esEstandar) {
					
					coasegurador = entidadService.findById(Coasegurador.class, idOrigen);
					
					coasegurador.setCalle(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_CALLE, String.class));
					
					numero = procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_NUMERO_EXT, Integer.class);
					
					coasegurador.setNumeroExterior(numero + "");
					coasegurador.setNumeroInterior(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_NUMERO_INT, String.class, false));
					coasegurador.setColonia(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_COLONIA, String.class, false));
					coasegurador.setDescripcionCiudad(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_CIUDAD, String.class, false));
					coasegurador.setDescripcionEstado(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_ESTADO, String.class, false));
					coasegurador.setDescripcionPais(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_PAIS, String.class));
					coasegurador.setCodigoPostal(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_CODIGO_POSTAL, String.class, false));
					
					if (registro.getProcesado() == null) {
						coasegurador = entidadService.updateForHistory(coasegurador);
					}
					
					fechaModificacion = new Date(TimeUtils.END_OF_TIME);
				} else {
					fechaModificacion = fechaRegistroPlantilla;
				}
				
				coaseguradores = entidadService.findByProperties(CoaseguradorHistorico.class, properties, fechaModificacion);
				idReaseguradorCorredor = coaseguradores.get(0).getId();
				
			} else {
				
				if (esEstandar) {
					
					reasegurador = entidadService.findById(ReaseguradorCorredorDTO.class, idOrigen);
					
					reasegurador.setCalle(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_CALLE, String.class));
					
					numero = procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_NUMERO_EXT, Integer.class);
					
					reasegurador.setNumeroExterior(numero + "");
					reasegurador.setNumeroInterior(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_NUMERO_INT, String.class, false));
					reasegurador.setColonia(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_COLONIA, String.class, false));
					reasegurador.setDescripcionCiudad(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_CIUDAD, String.class, false));
					reasegurador.setDescripcionEstado(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_ESTADO, String.class, false));
					reasegurador.setDescripcionPais(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_PAIS, String.class));
					reasegurador.setCodigoPostal(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_CODIGO_POSTAL, String.class, false));
					
					if (registro.getProcesado() == null) {
						reasegurador = entidadService.updateForHistory(reasegurador);
					}
					
					fechaModificacion = new Date(TimeUtils.END_OF_TIME);
				} else {
					fechaModificacion = fechaRegistroPlantilla;
				}
				
				reaseguradores = entidadService.findByProperties(ReaseguradorCorredorDTOHistorico.class, properties, fechaModificacion);
				idReaseguradorCorredor = reaseguradores.get(0).getId();
			}
			
			estadoCuentaSet.setIdReaseguradorCoaseguradorHist(idReaseguradorCorredor);
			
			//Periodo actual
			estadoCuentaSet.setFechaInicioPeriodo(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_FECHA_INICIO_PERIODO, Date.class));
			estadoCuentaSet.setFechaFinPeriodo(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_FECHA_FIN_PERIODO, Date.class));
			
			//Remesa/Cheque
			remesa=procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_REMESA, Integer.class, false);
			cheque=procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_CHEQUE, Integer.class, false);
					
			//Periodo anterior
			if (estadoCuentaSet.getFechaInicioPeriodo() != null && estadoCuentaSet.getFechaFinPeriodo() != null) {
				
				fechaInicial = TimeUtils.getDateTime(estadoCuentaSet.getFechaInicioPeriodo());
				fechaFinal = TimeUtils.getDateTime(estadoCuentaSet.getFechaFinPeriodo());
				fechaFinalAnt = fechaInicial.minusDays(1);
				fechaInicialAnt = fechaInicial.minusMonths(getRealMonths(fechaInicial, fechaFinal.plusDays(1)));
				
				estadoCuentaSet.setFechaInicioPeriodoAnterior(fechaInicialAnt.toDate());
				estadoCuentaSet.setFechaFinPeriodoAnterior(fechaFinalAnt.toDate());
				
			}
			
			//Otros
			estadoCuentaSet.setContratoHistorico(contrato);
			estadoCuentaSet.setFechaCreacion(new Date());
			estadoCuentaSet.setDescripcionExtra(procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_DESCRIPCION_EXTRA, String.class, false));
			
			
			//Remesa/Cheque
			if(remesa != null && cheque != null){
				throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.campo.envio.unico"));
			} else if (remesa != null){
				estadoCuentaSet.setEntidad("REMESA");
				estadoCuentaSet.setIdentificador(remesa);
			} else if (cheque != null){
				estadoCuentaSet.setEntidad("CHEQUE");
				estadoCuentaSet.setIdentificador(cheque);
			}
			
			if (estadoCuentaSet.getDescripcionExtra() != null) {
			
				if(estadoCuentaSet.getDescripcionExtra().trim().length() > 60) {
					throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.descripcionextra.longitud"));
				}
				
				estadoCuentaSet.setDescripcionExtra(estadoCuentaSet.getDescripcionExtra().trim());
				
			}
			
			if (contratoOriginal.getMonedas().size() == 1) {
				monedaIngresada = contratoOriginal.getMonedas().get(0).getIdTcMoneda().intValue();
			} else {
				monedaIngresada = procesaConceptoHeader(registro, sheet, conceptosHeader, Concepto.HD_MONEDA, Integer.class);
			}
			
			if (subRamo.getMonedaUnica() != null && subRamo.getMonedaUnica().getIdTcMoneda().intValue() != monedaIngresada.intValue()) {
				throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.ramo.moneda"));
			}
			
			estadoCuentaSet.setMoneda(monedaIngresada);
			
			//Se revisa si el estado de cuenta ya habia sido procesado anteriormente
			
			if (subRamo.getId() == null
					|| contrato.getIdOrigen() == null
					|| idReaseguradorCorredor == null
					|| estadoCuentaSet.getFechaInicioPeriodo() == null
					|| estadoCuentaSet.getFechaFinPeriodo() == null
					|| estadoCuentaSet.getMoneda() == null) {
				throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.campo.vacio"));
			}
			
			properties = new HashMap<String,Object>();
			properties.put("subRamo", subRamo);
			properties.put("contratoHistorico.idOrigen", contrato.getIdOrigen());
			properties.put("idReaseguradorCoaseguradorHist", idReaseguradorCorredor);
			properties.put("fechaInicioPeriodo", estadoCuentaSet.getFechaInicioPeriodo());
			properties.put("fechaFinPeriodo", estadoCuentaSet.getFechaFinPeriodo());
			properties.put("moneda", estadoCuentaSet.getMoneda());
						
			List<EstadoCuenta> estadosCuenta  = entidadService.findByProperties(EstadoCuenta.class, properties);
			
			if (estadosCuenta != null && !estadosCuenta.isEmpty() && estadosCuenta.size() > 0) {
								
				estadoCuenta = estadosCuenta.get(0);
								
				estadoCuenta.setContratoHistorico(contrato);
				
				estadoCuenta.setSubRamo(subRamo);
				
				for (Folio folioEC: estadoCuenta.getFolios()) {
					
					foliosOriginales.add(folioEC.getFolio());
					folioEC.setEsValido(false);
				}
				
				
			} else {
				estadoCuenta = estadoCuentaSet;
			}
							
			
			for (ConceptoHistorico concepto : conceptos) {
				
				conceptoEstadoCuenta = null;
				
				if (estadoCuenta.getId() != null) {
					
					for (ConceptoEstadoCuenta cEC : estadoCuenta.getConceptosEstadoCuenta()) {
						
						if (cEC.getConceptoHistorico().equals(concepto)) {
							conceptoEstadoCuenta = cEC;
							break;
						}
						
					}
				
				} 
				
				if (conceptoEstadoCuenta == null) {
					conceptoEstadoCuenta = new ConceptoEstadoCuenta();
				}
				
				
				conceptoEstadoCuenta.setConceptoHistorico(concepto);
				conceptoEstadoCuenta.setEstadoCuenta(estadoCuenta);
				
				debe = procesaConcepto(registro, sheet, concepto, BigDecimal.class, false, false);
				haber = procesaConcepto(registro, sheet, concepto, BigDecimal.class, true, false);
				
				conceptoEstadoCuenta.setDebe(debe!=null?debe:BigDecimal.ZERO);
				conceptoEstadoCuenta.setHaber(haber!=null?haber:BigDecimal.ZERO);
				
				if (conceptoEstadoCuenta.getDebe().compareTo(BigDecimal.ZERO) > 0 
						&& conceptoEstadoCuenta.getHaber().compareTo(BigDecimal.ZERO) > 0) {
					
					throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.detalle.dobleingreso"));
					
				}
				
				if (conceptoEstadoCuenta.getId() == null) {
					estadoCuenta.getConceptosEstadoCuenta().add(conceptoEstadoCuenta);
				}
								
				if (concepto.getEsTimbre()) {
					
					if (conceptoEstadoCuenta.getDebe().compareTo(BigDecimal.ZERO) > 0) {
						existeIngreso = true;
					} else if (conceptoEstadoCuenta.getHaber().compareTo(BigDecimal.ZERO) > 0 && !concepto.getClave().equals(CLAVE_PRIMA_CEDIDA)) {
						existeEgreso = true;
					} else if (conceptoEstadoCuenta.getHaber().compareTo(BigDecimal.ZERO) > 0 && concepto.getClave().equals(CLAVE_PRIMA_CEDIDA)) {
						existePrimaCedida = true;
					}
					
				}
				
			}
			
			if (!existeIngreso && !existeEgreso) {
				//Para que se genere el CFDI y la Rep. Impresa del EC aunque no exista un comprobante de ingresos como tal
				existeIngreso = true;
			}
			
			//Se quitan los conceptos antiguos que ya no se usaran
			for (ConceptoEstadoCuenta cEC : estadoCuenta.getConceptosEstadoCuenta()) {
				if (!conceptos.contains(cEC.getConceptoHistorico())) {
					conceptosECBorrar.add(cEC);
				}
			}
			
			estadoCuenta.getConceptosEstadoCuenta().removeAll(conceptosECBorrar);
			
			//Si pasa la validacion se guarda la info, se generan el XML y el PDF con la rep. impresa
			if (registro.getProcesado() == null) {
				
				if (existeIngreso) {
					
					folioValido = validaFolio(estadoCuenta, 0, registro, sheet);
					
					if (folioValido) {
						folio = new Folio();
						folio.setEsEgreso(0); 
						folio.setEstadoCuenta(estadoCuenta);
						estadoCuenta.getFolios().add(folio);
					}
					
				}
				
				if (existeEgreso) {
					
					folioValido = validaFolio(estadoCuenta, 1, registro, sheet);
					
					if (folioValido) {
						folio = new Folio();
						folio.setEsEgreso(1);
						folio.setEstadoCuenta(estadoCuenta);
						estadoCuenta.getFolios().add(folio);
					}
					
				}
				
				//se agrega un folio para el comprobante de prima cedida
				if (existePrimaCedida) {
					folioValido = validaFolio(estadoCuenta, 2, registro, sheet);
					
					if (folioValido) {
						folio = new Folio();
						folio.setEsEgreso(2);
						folio.setEstadoCuenta(estadoCuenta);
						estadoCuenta.getFolios().add(folio);
					}
				}
				
				estadoCuenta = edoCuentaCFDIDao.guardaEstadoCuenta(estadoCuenta);
				
				
				for (Folio folioEC: estadoCuenta.getFolios()) {
					
					if (!foliosOriginales.contains(folioEC.getFolio())) {
						folioEC.setEsValido(true);
					}
					
				}
				
				generarCFDIyRepImpresa(estadoCuenta);
				
				registro.setCfdiGenerados(getCFDIs(estadoCuenta));
				registro.setRepresentacionesImpresas(getRepresentacionesImpresas(estadoCuenta));
				registro.setProcesado(true);
				
				cell = readCellAt(sheet, registro.getRango().getFirstRow(), registro.getRango().getFirstColumn());
				setCellStyle(cell, false, COLOR_DIVISION_REGISTRO);
				
			}
		
		} catch (RuntimeException ex) {
			
			LOG.error(ex);
			
			cell = getCellAt(sheet, registro.getRango().getFirstRow(), registro.getRango().getFirstColumn() + 1);
						
			setCellStyle(cell, false, COLOR_ERROR);
			cell.setCellValue(ex.getMessage());
			
			registro.setProcesado(false);
		}
		
	}
	
	private boolean validaFolio(EstadoCuenta estadoCuenta, Integer esEgreso, RegistroEC registro, HSSFSheet sheet) {
		
		boolean resultado = true;
		HSSFCell cell = null;
		String tipoComprobante = null;
		
		if (estadoCuenta.getId() != null) {
		
			Integer resultadoEncontrado = edoCuentaCFDIDao.validaFolio (estadoCuenta.getId(), esEgreso, null);
			
			if (resultadoEncontrado != null && resultadoEncontrado.intValue() > 0) {
				//No es valido porque ya existe un CFDI para este EC de este tipo de comprobante
				resultado = false;
				
				//Prepara el registro para que muestre el problema
				cell = getCellAt(sheet, registro.getRango().getFirstRow(), registro.getRango().getFirstColumn() + 1);
				
				setCellStyle(cell, false, COLOR_ERROR);
				
				tipoComprobante = ((esEgreso!= null && esEgreso.intValue() == 1)
						?getRecurso("reaseguro.edocuentacfdi.validacion.tipocomprobante.egreso")
								:getRecurso("reaseguro.edocuentacfdi.validacion.tipocomprobante.ingreso"));
				
				cell.setCellValue(cell.getStringCellValue() + ", " + Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "reaseguro.edocuentacfdi.validacion.ec.existente", tipoComprobante));
				
				registro.setErroresGenerales("SI");
			} 
			
		}
		
		return resultado;
		
	}
	
	
	private <C> C procesaConceptoHeader(RegistroEC registro, HSSFSheet sheet, List<ConceptoHistorico> conceptosHeader, String claveConcepto, 
			Class<C> claseRespuesta) {
		return procesaConceptoHeader(registro, sheet, conceptosHeader, claveConcepto, claseRespuesta, true);
	}
	
	private <C> C procesaConceptoHeader(RegistroEC registro, HSSFSheet sheet, List<ConceptoHistorico> conceptosHeader, String claveConcepto, 
			Class<C> claseRespuesta, boolean obligatorio) {
		
		ConceptoHistorico conceptoHeader = null;
		ConceptoHistorico filtro = new ConceptoHistorico();
		
		filtro.setClave(claveConcepto);
		
		if (conceptosHeader.contains(filtro)) {
			conceptoHeader = conceptosHeader.get(conceptosHeader.indexOf(filtro));
		}
		
		return (conceptoHeader==null?null:procesaConcepto(registro, sheet, conceptoHeader, claseRespuesta, false, obligatorio));
		
	}
	
	private <C> C procesaConcepto(RegistroEC registro, HSSFSheet sheet, ConceptoHistorico concepto, Class<C> claseRespuesta, boolean esHaber, 
			boolean obligatorio) {
		
		HSSFCell cell = null;
		HSSFName name = null;
		HSSFSheet sheetSistema = null;
		C valor = null;
		String mensajeError = null;
		String formula = null;
		CellRangeAddress rangoAyuda = null;
		CellReference cellRefInicio = null;
		CellReference cellRefFin = null;
		HSSFCell cellResumenError = null;
		
		try {
			
			cell = findCell(concepto.getDescripcion(), registro.getRango(), sheet);
			
			if (concepto.getEsHeader()) {
				
				cell = readDownCell(cell);
				
				setCellStyle(cell, true, COLOR_CONTENIDO_CONCEPTO);
			
				name = sheet.getWorkbook().getName(DATOS_SISTEMA + concepto.getClave());
				
				if (name != null) {
					
					sheetSistema = sheet.getWorkbook().getSheet(name.getSheetName());
					
					formula = name.getRefersToFormula(); //Ej.  'My Sheet'!$A$5:$B$9
					
					formula = formula.substring(formula.indexOf("!") + 1);
					
					formula = formula.replace("$", "");
					
					cellRefInicio = new CellReference(formula.substring(0, formula.indexOf(":")));
					
					cellRefFin = new CellReference(formula.substring(formula.indexOf(":") + 1));
					
					rangoAyuda = new CellRangeAddress(cellRefInicio.getRow(), cellRefFin.getRow(), cellRefInicio.getCol(), cellRefFin.getCol());
					
					cell = findCell(getCellValue(cell, true, String.class), rangoAyuda, sheetSistema);
					
					cell = readCellAt(sheetSistema, cell.getRowIndex(), cell.getColumnIndex() -1);
					
				}
				
			} else {
				
				cell = readNextRightCell(cell);
				
				if (esHaber) {
					
					cell = readNextRightCell(cell);
					
				}
				
				setCellStyle(cell, true, COLOR_CONTENIDO_CONCEPTO);
				
			}
			
			valor = (C) getCellValue(cell, obligatorio, claseRespuesta);
			
		} catch (ClassCastException ex) {
			mensajeError = getRecurso("reaseguro.edocuentacfdi.validacion.campo.tipodato");
		} catch (RuntimeException ex) {
			mensajeError = ex.getMessage();
		}
		
		if (mensajeError != null) {

			setCellStyle(cell, true, COLOR_ERROR);
			setCellComment(cell, mensajeError);
						
			cellRefInicio = new CellReference(cell.getRowIndex(), cell.getColumnIndex());
						
			cellResumenError = getCellAt(sheet, registro.getRango().getFirstRow() + 1, registro.getRango().getFirstColumn());
			
			cellResumenError.setCellValue(cellResumenError.getStringCellValue() + " - " 
					+ cellRefInicio.getCellRefParts()[2] + cellRefInicio.getCellRefParts()[1] + ":" + mensajeError);
			
			registro.setProcesado(false);
			
			return null;
			
		}
		
		return valor;
		
	}
	
	private int getRealMonths(DateTime fechaInicial, DateTime fechaFinal) {
		
		int respuesta = 0;
		
		Interval intervalo = TimeUtils.interval(fechaInicial, fechaFinal.plusDays(1));
		
		respuesta = (intervalo.toPeriod().getYears() * 12) + intervalo.toPeriod().getMonths();
		
		return respuesta;
		
	}
	
	
	private void generarCFDIyRepImpresa(EstadoCuenta estadoCuenta) {
		
		BigDecimal saldoAcumulado;
		BigDecimal saldoAcumuladoCFDI;
		BigDecimal debeAcumulado;
		BigDecimal haberAcumulado;
		BigDecimal debe;
		BigDecimal haber;
		BigDecimal valor;
		DateTime fechaInicial;
		DateTime fechaFinal;
		DateTime fechaInicialAnt;
		DateTime fechaFinalAnt;
		DateTime fechaUltimaRenovacion;
		MonedaDTO moneda;
		ConceptoHistorico conceptoTimbreDefault = null;
		String textoPeriodoAnterior = null;
		String descripcionSuscripcion = null;
		int trimestre = 0;
		boolean agregarTextoPeriodoAnterior = false;
				
		fechaInicial = TimeUtils.getDateTime(estadoCuenta.getFechaInicioPeriodo());
		fechaFinal = TimeUtils.getDateTime(estadoCuenta.getFechaFinPeriodo()).plusDays(1);
		
		fechaUltimaRenovacion = TimeUtils.getDateTime(estadoCuenta.getContratoHistorico().getUltimaRenovacion());
		
		if (fechaInicial.isAfter(fechaFinal)
				|| fechaFinal.isBefore(fechaUltimaRenovacion) 
				|| fechaFinal.isEqual(fechaUltimaRenovacion)
				|| fechaUltimaRenovacion.plusYears(1).isBefore(fechaInicial) 
				|| fechaUltimaRenovacion.plusYears(1).isEqual(fechaInicial)) {
			throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.periodo.fecha"));
		}
		
		if (estadoCuenta.getContratoHistorico().getEsAutomatico() && getRealMonths(fechaInicial, fechaFinal) == 3) {
		
			if (fechaUltimaRenovacion.isEqual(fechaInicial)
					|| (fechaUltimaRenovacion.isBefore(fechaInicial) && getRealMonths(fechaUltimaRenovacion, fechaInicial)%3 == 0)) {
				
				descripcionSuscripcion = "";
				
				trimestre = getRealMonths(fechaUltimaRenovacion, fechaFinal.plusDays(1)) / 3;
				
				if (trimestre == 1) {
					descripcionSuscripcion = getRecurso("reaseguro.edocuentacfdi.plantillaec.suscripcion.trimestre.primero");
				} else if (trimestre == 2) {
					descripcionSuscripcion = getRecurso("reaseguro.edocuentacfdi.plantillaec.suscripcion.trimestre.segundo");
				} else if (trimestre == 3) {
					descripcionSuscripcion = getRecurso("reaseguro.edocuentacfdi.plantillaec.suscripcion.trimestre.tercero");
				} else if (trimestre == 4) {
					descripcionSuscripcion = getRecurso("reaseguro.edocuentacfdi.plantillaec.suscripcion.trimestre.cuarto");
				}
				
				descripcionSuscripcion += " " + getRecurso("reaseguro.edocuentacfdi.plantillaec.suscripcion.trimestre.contratos") 
					+ " " + new SimpleDateFormat(FORMATO_SUSCRIPCION).format(fechaUltimaRenovacion.toDate());
				
			}
			
		}
		
		fechaInicialAnt = TimeUtils.getDateTime(estadoCuenta.getFechaInicioPeriodoAnterior());
		fechaFinalAnt = TimeUtils.getDateTime(estadoCuenta.getFechaFinPeriodoAnterior());
		
		if (getRealMonths(fechaInicialAnt, fechaFinalAnt) == 1) {
			
			textoPeriodoAnterior = new SimpleDateFormat(FORMATO_FIN_PERIODO_ANT, new Locale("es", "MX")).format(fechaFinalAnt.toDate());
			textoPeriodoAnterior = textoPeriodoAnterior.replace(" ", "-");
		
		} else {
			
			textoPeriodoAnterior = new SimpleDateFormat(FORMATO_FIN_PERIODO_ANT, new Locale("es", "MX")).format(fechaInicialAnt.toDate())
				+ "-" + new SimpleDateFormat(FORMATO_FIN_PERIODO_ANT, new Locale("es", "MX")).format(fechaFinalAnt.toDate());
		
		}

		Collections.sort(estadoCuenta.getConceptosEstadoCuenta());
		
		for (Folio folio : estadoCuenta.getFolios()) {
			
			if (!folio.getEsValido()) {
				continue;
			}
			
			debeAcumulado = BigDecimal.ZERO;
			haberAcumulado = BigDecimal.ZERO;
			saldoAcumulado = BigDecimal.ZERO;
			saldoAcumuladoCFDI = BigDecimal.ZERO;
			agregarTextoPeriodoAnterior = true;
			conceptoTimbreDefault = null;
			
			for (ConceptoEstadoCuenta conceptoEstadoCuenta : estadoCuenta.getConceptosEstadoCuenta()) {
								
				debe = (conceptoEstadoCuenta.getDebe() != null?conceptoEstadoCuenta.getDebe():BigDecimal.ZERO);
				haber = (conceptoEstadoCuenta.getHaber() != null?conceptoEstadoCuenta.getHaber():BigDecimal.ZERO);
				valor = BigDecimal.ZERO;
				
				debeAcumulado = debeAcumulado.add(debe);
				haberAcumulado = haberAcumulado.add(haber);
				saldoAcumulado = saldoAcumulado.add(haber.subtract(debe));
				
				if (conceptoEstadoCuenta.getConceptoHistorico().getEsTimbre()) {
					
					if (conceptoTimbreDefault == null) {
						conceptoTimbreDefault = conceptoEstadoCuenta.getConceptoHistorico();
					}
					
					if (folio.getEsEgreso() == 0 && debe.compareTo(BigDecimal.ZERO) > 0) {
						valor = debe;
					} else if (folio.getEsEgreso() == 1 && haber.compareTo(BigDecimal.ZERO) > 0 &&
							!conceptoEstadoCuenta.getConceptoHistorico().getClave().equals(CLAVE_PRIMA_CEDIDA)) {
						valor = haber;
					}else if (folio.getEsEgreso() == 2 && haber.compareTo(BigDecimal.ZERO) > 0 && 
							conceptoEstadoCuenta.getConceptoHistorico().getClave().equals(CLAVE_PRIMA_CEDIDA)) {
						valor = haber;
					}
					
					if (valor.compareTo(BigDecimal.ZERO) > 0) {
						
						saldoAcumuladoCFDI = saldoAcumuladoCFDI.add(valor);
						
						edoCuentaCFDIDao.insertaConceptoFiscal(EstadoCuenta.SERIE, folio, 
								conceptoEstadoCuenta.getConceptoHistorico().getDescripcion(), valor);
						
					}
					
				}
								
				if (conceptoEstadoCuenta.getConceptoHistorico().getEsObligatorio() 
						|| debe.compareTo(BigDecimal.ZERO) > 0 || haber.compareTo(BigDecimal.ZERO) > 0) {
					
					edoCuentaCFDIDao.insertaConceptoEC(EstadoCuenta.SERIE, folio, conceptoEstadoCuenta.getConceptoHistorico().getSecuencia(), 
							(agregarTextoPeriodoAnterior?textoPeriodoAnterior:null), conceptoEstadoCuenta.getConceptoHistorico().getDescripcion(), 
							debe, haber, saldoAcumulado);
					
				}
								
				agregarTextoPeriodoAnterior = false;
			} //Fin del for que recorre los conceptos
			
			//Si no hay ingresos se agrega un concepto de ingreso por default (Para que se genere el CFDI)
			if (saldoAcumuladoCFDI.compareTo(BigDecimal.ZERO) == 0 && conceptoTimbreDefault != null) {
				
				edoCuentaCFDIDao.insertaConceptoFiscal(EstadoCuenta.SERIE, folio, 
						conceptoTimbreDefault.getDescripcion(), BigDecimal.ZERO);
				
			}
						
			edoCuentaCFDIDao.insertaConceptoEC(EstadoCuenta.SERIE, folio, SIN_LIMITE_DE_COLUMNAS, 
					null, getRecurso("reaseguro.edocuentacfdi.plantillaec.concepto.subtotal"), debeAcumulado, haberAcumulado, saldoAcumulado);
			
			moneda = entidadService.findById(MonedaDTO.class, estadoCuenta.getMoneda().shortValue());
			
			if (folio.getEsEgreso() == 1 || folio.getEsEgreso() == 2) {
				saldoAcumuladoCFDI = saldoAcumuladoCFDI.multiply(new BigDecimal(-1)); //Para que el software de WFACTURA entienda si es ingreso o egreso
			}
			
			edoCuentaCFDIDao.insertaComprobanteEC(EstadoCuenta.SERIE, folio.getFolio(), 
					estadoCuenta.getContratoHistorico().getDescripcion() 
						+ (estadoCuenta.getDescripcionExtra()!=null?" " + SEPARADOR_DESC_CONTRATO_FAC + " " + estadoCuenta.getDescripcionExtra():"") , 
					descripcionSuscripcion, estadoCuenta.getSubRamo().getDescripcion(), estadoCuenta.getFechaInicioPeriodo(), 
					estadoCuenta.getFechaFinPeriodo(), moneda.getDescripcion(), estadoCuenta.getIdReaseguradorCoaseguradorHist(), saldoAcumuladoCFDI);
			
		} //Fin del for que recorre los folios
		
	}
	
	
	private List<ControlDescripcionArchivoDTO> getRepresentacionesImpresas(EstadoCuenta estadoCuenta) {
		return getComprobantes(estadoCuenta, true);
	}
		
	private List<ControlDescripcionArchivoDTO> getCFDIs(EstadoCuenta estadoCuenta) {
		return getComprobantes(estadoCuenta, false);
	}
	
	private List<ControlDescripcionArchivoDTO> getComprobantes(EstadoCuenta estadoCuenta, Boolean esPDF) {
		
		List<ControlDescripcionArchivoDTO> archivos = new ArrayListNullAware<ControlDescripcionArchivoDTO>();
		
		ControlDescripcionArchivoDTO archivo = new ControlDescripcionArchivoDTO();
		
		for (Folio folio : estadoCuenta.getFolios()) {
			
			if (!folio.getEsValido()) {
				continue;
			}
			
			archivo = getComprobante(EstadoCuenta.SERIE + " " + folio.getFolio(), esPDF);
			archivos.add(archivo);
			
			if (!esPDF && !"".equals(estadoCuenta.getEntidad()) && estadoCuenta.getIdentificador()!=null){
				EnvioFactura envioFactura = new EnvioFactura();
				byte[] buffer = new byte[1024];
				File filePro = null;
				try {
					
					buffer = IOUtils.toByteArray(archivo.getInputStream());
					filePro = new File(sistemaContext.getUploadFolder() + archivo.getNombreArchivo());
					
					FileUtils.writeByteArrayToFile(filePro, buffer);
				    
				    if(filePro.exists()){
						envioFactura.setOrigenEnvio(estadoCuenta.getEntidad());
						envioFactura.setIdOrigenEnvio(Long.valueOf(estadoCuenta.getIdentificador().toString()));
						envioFactura.setFacturaXml(filePro);
						envioFactura.setIdDepartamento(D_REASEGURO);
						envioFacturaService.saveEnvio(envioFactura);
						filePro.delete();	
					}
				} catch (IOException e1) {
					LOG.error(e1);
					throw new RuntimeException("Error al intentar crear el archivo XML: " + e1.getMessage());
				} catch (Exception e) {
					LOG.error(e);
					throw new RuntimeException("Error al intentar enviar la Factura a AXOSNET: " + e.getMessage());
				}
			}			
		}
		
		return archivos;
	}
	
	private ControlDescripcionArchivoDTO getComprobante(String llaveComprobante, Boolean esPDF) {
		
		ControlDescripcionArchivoDTO archivo = new ControlDescripcionArchivoDTO();
		String contentType = (esPDF?CONTENT_TYPE_PDF:CONTENT_TYPE_XML);
		String extension = (esPDF?EXTENSION_PDF:EXTENSION_XML);
		
		try {
			
			LOG.info("Consumiendo PrintReportWS para llave " + llaveComprobante + " extension " + extension);
			
			if (esPDF) {
				
				archivo.setInputStream(new ByteArrayInputStream(printReportClient.getDigitalBill(llaveComprobante)));
				
			} else {
				
				archivo.setInputStream(new ByteArrayInputStream(printReportClient.getCFDIWithAddenda(llaveComprobante)));
				
			}
			
			archivo.setExtension(contentType); // contentType
						
			archivo.setNombreArchivo(llaveComprobante.replace(" ", "_") + "." + extension);
			
		} catch (Exception e) {
			LOG.error(e);
			throw new RuntimeException("Error al intentar obtener/generar el comprobante fiscal : " + e.getMessage());
		}
						
		return archivo;
		
	}
		
	
	/**
	 XXX Excel / POI
	 */
	
		
	private InputStream getInputStreamEC(Contrato contrato, Plantilla plantilla, Date fechaCreacion) {
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		
		buildDatosSistemaSheet(workbook, contrato, plantilla, fechaCreacion);
		
		buildDatosUsuarioSheet(workbook, contrato, plantilla);

		workbook.setSheetHidden(0, true);
		
		workbook.setActiveSheet(1);
		
		return escribeWorkBook(workbook);
		
	}
		
	private InputStream getInputStreamCAN() {
		
		HSSFWorkbook workbook = new HSSFWorkbook();
		HSSFSheet datosUsuario = workbook.createSheet(CANCELACIONES);
				
		HSSFCell cell = getCell(datosUsuario, null, 1);
		cell.setCellValue(getRecurso("reaseguro.edocuentacfdi.plantillacan.titulo"));   
		setCellStyle(cell, true, COLOR_HABER_DEBE);
		
		datosUsuario.autoSizeColumn(0);
		
		workbook.setActiveSheet(0);
		
		datosUsuario.showInPane((short)0, (short)0);
		
		return escribeWorkBook(workbook);
		
	}
	
	
	private void buildDatosSistemaSheet(HSSFWorkbook workbook, Contrato contrato, Plantilla plantilla, Date fechaCreacion) {
		
		HSSFCell cell = null;
		HSSFCell rightCell = null;
		HSSFSheet datosSistema = workbook.createSheet(DATOS_SISTEMA);
		
		//Encabezado
		
		cell = getCell(datosSistema, null, 1);
		cell.setCellValue(plantilla.getValor());
		
		rightCell = getNextRightCell(cell);
		rightCell.setCellValue(getRecurso("reaseguro.edocuentacfdi.plantillaec.titulo.plantilla"));
		
		cell = getDownCell(cell);
		cell.setCellValue(contrato.getId().intValue());
		setCellComment(cell, getRecurso("reaseguro.edocuentacfdi.plantillaec.titulo.contrato"));
		
		rightCell = getDownCell(rightCell);
		rightCell.setCellValue(getRecurso("reaseguro.edocuentacfdi.plantillaec.titulo.contrato"));
		
		cell = getDownCell(cell);
		cell.setCellValue(new SimpleDateFormat(FORMATO_FECHA_REGISTRO_DET).format(fechaCreacion));
		
		rightCell = getDownCell(rightCell);
		rightCell.setCellValue(getRecurso("reaseguro.edocuentacfdi.plantillaec.titulo.fechacreacion"));
				
		//Ayudas para los combos
		
		//NOMBRE reasegurador/coasegurador
		
		if (contrato.getClave().equals(Contrato.CLAVE_COASEGURO_VIDA) || contrato.getClave().equals(Contrato.CLAVE_COASEGURO_DANIOS)) { 
			List<Coasegurador> coaseguradores = entidadService.findAll(Coasegurador.class);
			Collections.sort(coaseguradores);
			setAyuda(datosSistema, Concepto.HD_NOMBRE, coaseguradores);
		} else {
			List<ReaseguradorCorredorDTO> reaseguradores = entidadService.findAll(ReaseguradorCorredorDTO.class);
			Collections.sort(reaseguradores);
			setAyuda(datosSistema, Concepto.HD_NOMBRE, reaseguradores);
		}
		
		//RAMO
		setAyuda(datosSistema, Concepto.HD_RAMO, contrato.getSubRamos());
		
		//MONEDA
		setAyuda(datosSistema, Concepto.HD_MONEDA, contrato.getMonedas());
				
		//Se protege la hoja con contraseña
		datosSistema.protectSheet("midascfdi");
		
	}
	
	
	private void buildDatosUsuarioSheet(HSSFWorkbook workbook, Contrato contrato, Plantilla plantilla) {
		
				
		HSSFSheet datosUsuario = workbook.createSheet("EC " + contrato.getClave());
		
		List<Concepto> conceptosHeader = new ArrayListNullAware<Concepto>();
		
		List<Concepto> conceptos = new ArrayListNullAware<Concepto>();
		
		int espaciado = 3; //renglones entre cada titulo de concepto (tiene que ser minimo 2 por el tipo de presentacion diseñado)
		
		boolean esEstandar = (plantilla.equals(Plantilla.EC_ESTANDAR)?true:false);

		for (Concepto concepto : contrato.getConceptos()) {
			if (!concepto.getEsBase()) {
				if (concepto.getEsHeader()) {
					if (esEstandar || !concepto.getEsEstandar()) {
						conceptosHeader.add(concepto);
					}
				} else {
					conceptos.add(concepto);
				}
			}
		}
		
		Collections.sort(conceptosHeader);
		
		Collections.sort(conceptos);
				
				
		for (Concepto conceptoHeader : conceptosHeader) {
			
			if (!conceptoHeader.getClave().equals(Concepto.HD_MONEDA) || contrato.getMonedas().size() > 1) {
				
				setDatoUsuarioHeader(datosUsuario, conceptoHeader, espaciado);
				
			}
			
		}
		
		setTitulosDatoUsuarioConcepto(datosUsuario, espaciado);
		
		for (Concepto concepto : conceptos) {
			setDatoUsuarioConcepto(datosUsuario, concepto);
		}
		
		for (int i = 0; i < MAX_COLUMNAS; i++) {
			datosUsuario.setColumnWidth(i, ANCHO_COLUMNA);
		}
		
		datosUsuario.showInPane((short)0, (short)0);
	}
	
	
	private void setDatoUsuarioHeader(HSSFSheet sheet, Concepto concepto, int espaciado) {
		
		HSSFCell cell = getCell(sheet, MAX_COLUMNAS, espaciado);
		
		if (concepto.getSecuencia().intValue() == 0) { //Si se trata del separador de registro
			
			if (cell.getRowIndex() != 0 || cell.getColumnIndex() != 0) 
				throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.campo.separador.secuencia"));
			
			cell.setCellValue(concepto.getClave());
			setCellStyle(cell, false, COLOR_DIVISION_REGISTRO);
			
			for (int i = 1; i < MAX_COLUMNAS; i++) {
				cell = getNextRightCell(cell);
				setCellStyle(cell, false, COLOR_DIVISION_REGISTRO);
			}
			
		} else {
			
			cell.setCellValue(concepto.getDescripcion());
			setCellStyle(cell, true, COLOR_TITULO_CONCEPTO);
			
			cell = getDownCell(cell);
			validacionyEstiloCaptura(cell, concepto);

		}
		
	}

	private void setDatoUsuarioConcepto(HSSFSheet sheet, Concepto concepto) {
		
		HSSFCell cell = getCell(sheet, null, 1, sheet.getLastRowNum(), null, false);
		cell.setCellValue(concepto.getDescripcion());
		setCellStyle(cell, true, COLOR_TITULO_CONCEPTO);
		
		//DEBE
		cell = getNextRightCell(cell);
		validacionyEstiloCaptura(cell, concepto);
		
		//HABER
		cell = getNextRightCell(cell);
		validacionyEstiloCaptura(cell, concepto);
		
		
	}
	
	private void setTitulosDatoUsuarioConcepto(HSSFSheet sheet, int espaciado) {
		
		HSSFCell cell = null;
		
		cell = getCell(sheet, null, espaciado);
		cell.setCellValue(getRecurso("reaseguro.edocuentacfdi.plantillaec.titulo.concepto"));
		setCellStyle(cell, true, COLOR_HABER_DEBE);
		
		cell = getNextRightCell(cell);
		cell.setCellValue(getRecurso("reaseguro.edocuentacfdi.plantillaec.titulo.debe"));
		setCellStyle(cell, true, COLOR_HABER_DEBE);
		
		cell = getNextRightCell(cell);
		cell.setCellValue(getRecurso("reaseguro.edocuentacfdi.plantillaec.titulo.haber"));
		setCellStyle(cell, true, COLOR_HABER_DEBE);
		
	}
	
	private HSSFCell getNextRightCell(HSSFCell cell) {
		return getCell(cell.getSheet(), SIN_LIMITE_DE_COLUMNAS, 1, cell.getRowIndex(), null, false);
	}
		
	private HSSFCell getDownCell(HSSFCell cell) {
		return getCell(cell.getSheet(), SIN_LIMITE_DE_COLUMNAS, 1, cell.getRowIndex() + 1, cell.getColumnIndex(), false);
	}
	
	private HSSFCell getCellAt(HSSFSheet sheet, int rowNumber, Integer sColumn) {
		return getCell(sheet, SIN_LIMITE_DE_COLUMNAS, 1, rowNumber, sColumn, false);
	}
	
	private HSSFCell readNextRightCell(HSSFCell cell) {
		return getCell(cell.getSheet(), SIN_LIMITE_DE_COLUMNAS, 1, cell.getRowIndex(), cell.getColumnIndex() + 1, true);
	}
		
	private HSSFCell readDownCell(HSSFCell cell) {
		return getCell(cell.getSheet(), SIN_LIMITE_DE_COLUMNAS, 1, cell.getRowIndex() + 1, cell.getColumnIndex(), true);
	}
	
	private HSSFCell readCellAt(HSSFSheet sheet, int rowNumber, Integer sColumn) {
		return getCell(sheet, SIN_LIMITE_DE_COLUMNAS, 1, rowNumber, sColumn, true);
	}
	
	private HSSFCell getCell(HSSFSheet sheet, Integer limiteColumnas, Integer espaciadoRenglones) {
		return getCell(sheet, limiteColumnas, espaciadoRenglones, 0, null, false);
	}
	
	private HSSFCell getCell(HSSFSheet sheet, Integer limiteColumnas, Integer espaciadoRenglones, int rowNumber, Integer sColumn, boolean readOnly) {
		
		HSSFRow row = null;
		int column = 0;
		
		while (sheet.getRow(rowNumber) != null 
				&& (limiteColumnas == null || sheet.getRow(rowNumber).getCell(limiteColumnas.intValue() -1) != null)) {
			rowNumber += espaciadoRenglones;
		}
				
		row = sheet.getRow(rowNumber);
		
		if (row == null) {
			if (readOnly) return null;
			row = sheet.createRow(rowNumber);
		}
		
		if (sColumn != null) {
			if (row.getCell(sColumn.intValue()) != null) {
				return row.getCell(sColumn.intValue());
			}
			if (readOnly) return null;
			return row.createCell(sColumn.intValue());
		}
		
		while (row.getCell(column) != null) {
			column++;
		}
		
		return row.createCell(column);
		
	}
	
	@SuppressWarnings("unchecked")
	private <C> C getCellValue(HSSFCell cell, boolean validate, Class<C> claseRespuesta) {
		
		C respuesta = null;
		
		if (cell != null && !cell.toString().isEmpty()) {
				
			switch(cell.getCellType()) {
				case HSSFCell.CELL_TYPE_NUMERIC: {
					if (claseRespuesta.equals(Date.class)) {
		                 respuesta = (C)cell.getDateCellValue();
		                 cell.getCellStyle().setDataFormat(cell.getSheet().getWorkbook().createDataFormat().getFormat(FORMATO_PERIODO));
		            } else if (claseRespuesta.equals(BigDecimal.class)) {
		                 respuesta = (C)new BigDecimal(cell.getNumericCellValue());
		            } else if (claseRespuesta.equals(String.class)) {
		                 respuesta = (C)new Integer(new Double(cell.getNumericCellValue()).intValue() + "").toString();
		            } else {
		            	respuesta = (C) new Integer(new Double(cell.getNumericCellValue()).intValue() + "");
		            }
					break;
				}
				case HSSFCell.CELL_TYPE_STRING: {
					if (!claseRespuesta.equals(String.class)) throw new ClassCastException();
					respuesta = (C)cell.getStringCellValue().toUpperCase();
					break;
				}
				default: {
					if (!claseRespuesta.equals(String.class)) throw new ClassCastException();
					respuesta = (C)cell.toString();
					break;
				}
			}

		} else {
			
			if (validate) {
				throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.validacion.campo.vacio"));
			}
			
		}
		
		return respuesta;
		
	}
	
	private HSSFCell findCell(String cellValue, CellRangeAddress range, HSSFSheet sheet) {
		
		HSSFCell cell;
		
		for (int rowNumber = range.getFirstRow(); rowNumber <= range.getLastRow(); rowNumber ++) {
			for (int columnNumber = range.getFirstColumn(); columnNumber <= range.getLastColumn(); columnNumber ++) {
				cell = readCellAt (sheet, rowNumber, columnNumber);
				
				if (cellValue.equals(getCellValue(cell, false, String.class))) {
					return cell;
				}
				
			}
		}
		
		return null;
	}
		
	private void setCellComment(HSSFCell cell, String comment) {
		
		HSSFPatriarch patr;
		HSSFComment commentObj;
		
		if (comment != null) {

			patr = cell.getSheet().createDrawingPatriarch();
				
			commentObj = patr.createComment(patr.createAnchor(0, 0, 10, 10, (short)cell.getColumnIndex(), cell.getRowIndex(), (short) (cell.getColumnIndex() + 1), cell.getRowIndex() + 3));
			commentObj.setString(new HSSFRichTextString(comment));
		    
			cell.setCellComment(commentObj);
			
		}
			
	}
	
	private void setCellStyle(HSSFCell cell, boolean bordered, HSSFColor color) {
		
		short anchoBorde = 1;
		
		cell.setCellStyle(cell.getSheet().getWorkbook().createCellStyle()); 
		
		if (bordered) {
			cell.getCellStyle().setBorderTop(anchoBorde);
			cell.getCellStyle().setBorderLeft(anchoBorde);
			cell.getCellStyle().setBorderBottom(anchoBorde);
			cell.getCellStyle().setBorderRight(anchoBorde);
		}
		cell.getCellStyle().setFillPattern(HSSFCellStyle.SOLID_FOREGROUND); 
		cell.getCellStyle().setFillForegroundColor(color.getIndex());
		
	}
	
	private void validacionyEstiloCaptura(HSSFCell cell, Concepto concepto) {
		
		DVConstraint dvConstraint = null;
		HSSFDataValidation data_validation = null;
		
		setCellStyle(cell, true, COLOR_CONTENIDO_CONCEPTO);
		
		if (!concepto.getTipoValidacion().equals(SIN_VALIDACION)) {
			
			if (concepto.getTipoValidacion().equals(VALIDACION_LISTA)) {
				
				dvConstraint = DVConstraint.createFormulaListConstraint(DATOS_SISTEMA + concepto.getClave());
				
			} else if (concepto.getTipoValidacion().equals(VALIDACION_NUMERO)) {
				
				dvConstraint = DVConstraint.createNumericConstraint(ValidationType.DECIMAL,
						DVConstraint.OperatorType.GREATER_THAN, "0", "0");
				
			}
			
			data_validation = new HSSFDataValidation(new CellRangeAddressList(cell.getRowIndex(), cell.getRowIndex(), cell.getColumnIndex(), cell.getColumnIndex()), dvConstraint);
			
			if (data_validation.getConstraint().isListValidationType()) {
				data_validation.setSuppressDropDownArrow(false);
			}
			
			cell.getSheet().addValidationData(data_validation);
			
		}
		
	}
		
		
	private <E extends Entidad> void setAyuda(HSSFSheet sheet, String nombreAyuda, List<E> entidades) {
		
		HSSFCell cell = null;
		HSSFCell rightCell = null;
		HSSFName name = null;
		int initContentRow = 0;
		String rangoAyuda;
		
		if (!entidades.isEmpty()) {
			
			//Descripcion del nombre de la ayuda
			
			cell = getCellAt(sheet, ROW_AYUDA_SISTEMA, null);
			cell.setCellValue(nombreAyuda);
			
			rightCell = getNextRightCell(cell);
			rightCell.setCellValue("DESC");
						
			initContentRow = ROW_AYUDA_SISTEMA + 1;

			for (Entidad entidad : entidades) {
				
				cell = getDownCell(cell);
				
				if (entidad.getKey() instanceof BigDecimal) {
					cell.setCellValue(((BigDecimal)entidad.getKey()).intValueExact());
				} else {
					cell.setCellValue(Integer.parseInt(entidad.getKey() + ""));
				}
				
				rightCell = getDownCell(rightCell);
				rightCell.setCellValue(entidad.getValue());
				
			}
			
			CellReference cellRefInicio = new CellReference(initContentRow, cell.getColumnIndex());
			
			CellReference cellRefFin = new CellReference(initContentRow + (entidades.size() - 1), rightCell.getColumnIndex());

			rangoAyuda = "$" + cellRefFin.getCellRefParts()[2] + "$" + cellRefInicio.getCellRefParts()[1] + ":$" 
				+ cellRefFin.getCellRefParts()[2] + "$" + cellRefFin.getCellRefParts()[1];			                                                                                              
			                                                                                              
			name = sheet.getWorkbook().createName();
			name.setNameName(DATOS_SISTEMA + nombreAyuda);
			name.setRefersToFormula("'" + sheet.getSheetName() + "'!" + rangoAyuda);
			
		}
		
	}
	
	
	
	private InputStream escribeWorkBook(HSSFWorkbook workbook) {
		
		ByteArrayOutputStream bos = null;
		boolean exito = false;
		
		try {
			
			bos = new ByteArrayOutputStream();

			workbook.write(bos);
			
			exito = true;
			
		} catch (Exception e) {
			LOG.error(e);
			
		} finally {
			
			if (bos != null) {
				try {
					
					bos.close();
					
				} catch (IOException e) {
					LOG.error(e);
					
				}
			}
			
		}
		
		if(!exito){
			throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.plantilla.error"));
		}
		
		return new ByteArrayInputStream(bos.toByteArray());
	}
	
	private Plantilla getPlantilla(Integer tipoPlantilla) {
		
		if (tipoPlantilla.intValue() == Plantilla.EC_ABREVIADA.getValor()) {
			return Plantilla.EC_ABREVIADA;
		} else if (tipoPlantilla.intValue() == Plantilla.EC_ESTANDAR.getValor()) {
			return Plantilla.EC_ESTANDAR;
		} else if (tipoPlantilla.intValue() == Plantilla.CANCELACIONES.getValor()) {
			return Plantilla.CANCELACIONES;
		} else {
			throw new RuntimeException(getRecurso("reaseguro.edocuentacfdi.plantilla.noencontrada"));
		}
		
	}
	
	
	private String getRecurso (String entradaRecurso) {
		return Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, entradaRecurso);
	}
	
	/**
	 XXX Definiciones
	 */
	
	private static final String CONTENT_TYPE_EXCEL = "application/vnd.ms-excel";
	
	private static final String CONTENT_TYPE_ZIP = "application/zip";
	
	private static final String CONTENT_TYPE_XML = "application/xml";
	
	private static final String CONTENT_TYPE_PDF = "application/pdf";
	
	private static final String EXTENSION_EXCEL = "xls";
	
	private static final String EXTENSION_ZIP = "zip";
	
	private static final String EXTENSION_XML = "xml";
	
	private static final String EXTENSION_PDF = "pdf";
	
	private static final String FORMATO_FECHA_REGISTRO = "yyyyMMdd_HHmm";
	
	private static final String FORMATO_FECHA_REGISTRO_DET = "yyyyMMdd_HHmmss";
	
	//private static final String FORMATO_INICIO_PERIODO_ANT = "MMM";
	
	private static final String FORMATO_FIN_PERIODO_ANT = "MMM yy";
	
	private static final String FORMATO_SUSCRIPCION = "yyyy";
	
	private static final String FORMATO_CVE_CPTO_FABRICADO = "yy";
	
	private static final String FORMATO_PERIODO = "dd/MM/yyyy";
	
	private static final String SEPARADOR_DESC_CONTRATO_FAC = "//";
	
	private static final String DATOS_SISTEMA = "DatosSistema";
	
	private static final String CANCELACIONES = "Cancelaciones";
	
	private static final String ERRORES = "Errores";
	
	private static final String PROCESADO = "Procesado";
	
	private static final Integer SIN_VALIDACION = 0;
	
	private static final Integer VALIDACION_LISTA = 1;
	
	private static final Integer VALIDACION_NUMERO = 2;
	
	private static final int SIN_LIMITE_DE_COLUMNAS = 10000;
	
	private static final int ROW_AYUDA_SISTEMA = 4;
	
	private static final int MAX_COLUMNAS = 4;
	
	private static final int ANCHO_COLUMNA = 12326; // (Equivale a 49 del tamaño en excel)
	
	private static final String REGEX_PREFIJO = "^";
	
	private static final String REGEX_SUFIJO = "(.)*$";
	
	private static final HSSFColor COLOR_CONTENIDO_CONCEPTO = new HSSFColor.LIGHT_TURQUOISE();
	
	private static final HSSFColor COLOR_ERROR = new HSSFColor.YELLOW();
	
	private static final HSSFColor COLOR_DIVISION_REGISTRO = new HSSFColor.GREEN();
	
	private static final HSSFColor COLOR_TITULO_CONCEPTO = new HSSFColor.GREY_25_PERCENT();
		
	private static final HSSFColor COLOR_HABER_DEBE = new HSSFColor.GREY_40_PERCENT();
	
	private static final String CLAVE_PRIMA_CEDIDA = "C23"; 
	
	private static final String D_REASEGURO = "0008";
	
	private static Logger LOG = Logger.getLogger("EdoCuentaCFDIServiceImpl");
			
	private enum Plantilla {
		EC_ABREVIADA(1, "LayoutECA_"), EC_ESTANDAR(2, "LayoutECE_"), CANCELACIONES(3, "LayoutCAN_");
	
		private int valor;
		
		private String nombre;
		
		Plantilla(int valor, String nombre) {
			this.valor = valor;
			this.nombre = nombre;
		}
		
		public int getValor(){return valor;}
		
		public String getNombre(){return nombre;}
	
	};
	
	@EJB
	private EntidadHistoricoDao entidadService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private EdoCuentaCFDIDao edoCuentaCFDIDao;
	
	@EJB
	private PrintReportClient printReportClient;
	
	@EJB
	private OracleCurrentDateService oracleCurrentDateService;
	
	@EJB
	private EnvioFacturaService envioFacturaService; 
		
}
