package mx.com.afirme.midas2.service.impl.sapamis.sistemas;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.sapamis.sistemas.SapAmisPrevencionesDao;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisPrevenciones;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisDistpatcherWSService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisPrevencionesService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisPrevencionesServiceImpl.
 * 
 * Descripcion: 		Se utiliza para el manejo del Modulo de Prevenciones.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisPrevencionesServiceImpl implements SapAmisPrevencionesService{
	private static final long serialVersionUID = 1L;

	@EJB private SapAmisPrevencionesDao sapAmisPrevencionesDao;
	@EJB private SapAmisUtilsService sapAmisUtilsService; 
	@EJB private SapAmisDistpatcherWSService sapAmisDistpatcherWSService;
	
	private static final Long ESTATUS_PENDIENTE = new Long(1);
    private static final int CANTIDAD_DE_REGISTROS_PROCESAR = 1000;

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis() {
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		this.sendRegSapAmis(accesos);
    }

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(String[] accesos) {
    	ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    	parametrosConsulta.setEstatus(ESTATUS_PENDIENTE);
    	this.sendRegSapAmis(accesos, parametrosConsulta);
	}

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(ParametrosConsulta parametrosConsulta){
		this.sendRegSapAmis(sapAmisUtilsService.obtenerAccesos(), parametrosConsulta);
	}

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(String[] accesos, ParametrosConsulta parametrosConsulta){
    	List<SapAmisPrevenciones> sapAmisPrevencionesList = this.obtenerPorFiltros(parametrosConsulta, CANTIDAD_DE_REGISTROS_PROCESAR, 1);
		for(SapAmisPrevenciones sapAmisPrevenciones : sapAmisPrevencionesList){
			sapAmisDistpatcherWSService.enviarPrevencion(sapAmisPrevenciones, accesos);
		}
    }

	@Override
	public List<SapAmisPrevenciones> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return sapAmisPrevencionesDao.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}
}