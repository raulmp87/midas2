package mx.com.afirme.midas2.service.impl.job.notifica.estratega;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.tarea.NotificaEstrategasDao;
import mx.com.afirme.midas2.dto.notificacion.estratega.NotificacionEstrategaDTO;
import mx.com.afirme.midas2.service.job.estrategas.ProgramacionNotificaEstrategas;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.TextFileUtils;

@Stateless
public class ProgramacionNotificaEstrategasImpl implements ProgramacionNotificaEstrategas {
	
	private static final Logger log = LoggerFactory.getLogger(ProgramacionNotificaEstrategasImpl.class);
	
	@EJB
	private MailService mailService;
	@EJB
	private NotificaEstrategasDao notificaEstrategasDAO;
	
	@Override
	@Schedule(hour = "22")
	public void jobNotificaEstrategas() {
		log.info(">>>>>>>>>>>>>> [  INFO  ] Entra al metodo notificarEstrategas()");
		
		List<NotificacionEstrategaDTO> notificaciones;
		
		notificaciones = notificaEstrategasDAO.obtenerNotificaciones();
		StringBuilder mensaje = new StringBuilder();
		if(notificaciones != null && !notificaciones.isEmpty()){
			for(NotificacionEstrategaDTO notificacion: notificaciones){
				mensaje.append(notificacion.getPoliza()).append("|")
					.append(notificacion.getIdSistema()).append("|")
					.append(notificacion.getIdOficina()).append("|")
					.append(notificacion.getSubGrupo()).append("|")
					.append(notificacion.getIdRamo()).append("|")
					.append(notificacion.getIdSubrContable()).append("|")
					.append(notificacion.getModulo()).append("|")
					.append(notificacion.getIdBanco()).append("|")
					.append(notificacion.getIdMoneda()).append("|")
					.append(notificacion.getIdTipoCuenta()).append("|")
					.append(notificacion.getNumeroCuenta()).append("|")
					.append(notificacion.getNumRecibo()).append("|")
					.append(notificacion.getFecCubreDesde()).append("|")
					.append(notificacion.getFecCubreHasta()).append("|")
					.append(notificacion.getImpPrimaTotal()).append("|")
					.append(notificacion.getIdEmpresa()).append("|")
					.append(notificacion.getIdLineaNeg()).append("|")
					.append(notificacion.getIdAgente()).append("|")
					.append(notificacion.getIdFuncionario()).append("|")
					.append(notificacion.getIdSucursal()).append("|")
					.append(notificacion.getTipoMov()).append("|")
    				.append(notificacion.getReciboPagado()).append("|")
    				.append(notificacion.getNumOperacion()).append("|")
    				.append(notificacion.getFormaPago()).append("|")
    				.append(notificacion.getRespCobro()).append("|")
    				.append(notificacion.getOrdenId()).append("|")
    				.append(notificacion.getOrigen()).append("|")
    				.append(notificacion.getSerieRecibo()).append("|")
    				.append(notificacion.getFechaIntento()).append("|")
    				.append(notificacion.getNumIntento()).append("|")
    				.append(notificacion.getDescRespCobro())
    				.append("\n");
			}
			List<ByteArrayAttachment> lista = new ArrayList<ByteArrayAttachment>();
			lista.add(this.generaArchivodeTexto(mensaje.toString()));
			enviarCorreo(lista);
		}
		log.info("<<<<<<<<<<<<<< [  INFO  ] Termina la ejecucion de notifica estrategas");
	}
	
	private ByteArrayAttachment generaArchivodeTexto(String contenido){
		log.info(">>>>>>>>>>>>>> [  INFO  ] Entra a generar el archivo de texto generaArchivodeTexto()");
		
		byte[] data = contenido.getBytes();
		ByteArrayAttachment adjunto = new ByteArrayAttachment(TextFileUtils.FILE_NAME_ESTRA, ByteArrayAttachment.TipoArchivo.TEXTO, data);
		
		log.info("<<<<<<<<<<<<<< [  INFO  ] Sale del metodo generaArchivodeTexto");
		return adjunto;
	}
	
	private void enviarCorreo(List<ByteArrayAttachment> doctoAdjunto){
		log.info(">>>>>>>>>>>>>> [  INFO  ] Entra a mandar el correo de notificación");
		
		List<String> mailsTo = notificaEstrategasDAO.obtenerEstrategasDestinatarios();
		List<String> tituloYCuerpo = notificaEstrategasDAO.obtenerTituloyCuerpoMensaje();
		
		if(mailsTo != null && !mailsTo.isEmpty() && tituloYCuerpo != null){
			
			mailService.sendMail(mailsTo, tituloYCuerpo.get(0), tituloYCuerpo.get(1), doctoAdjunto);
			
		}
		log.info("<<<<<<<<<<<<<< [  INFO  ] Sale del metodo enviar correo ");
	}

	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	public void setNotificaEstrategasDAO(NotificaEstrategasDao notificaEstrategasDAO) {
		this.notificaEstrategasDAO = notificaEstrategasDAO;
	}
}
