package mx.com.afirme.midas2.service.impl.siniestros.notificaciones;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.siniestros.notificaciones.BitacoraNotificacionDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.ConfiguracionNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraDestinoNotificacion;
import mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraDetalleNotificacion;
import mx.com.afirme.midas2.domain.siniestros.notificaciones.BitacoraNotificacion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EmailDestinaratios;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
public class BitacoraNotificacionServiceImpl implements
	BitacoraNotificacionService{

	@EJB
	BitacoraNotificacionDao bitacoraNotificacionDao;
	
	@EJB
	EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	UsuarioService usuarioService;
	
	private Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
	
	public static final Logger log = Logger.getLogger(BitacoraNotificacionServiceImpl.class);
	
	@Override
	public void cambiarReenvio(String folio, Boolean recurrente) {
		List<BitacoraNotificacion> bitacoras = entidadService.findByProperty(BitacoraNotificacion.class, "folio", folio);
		if(bitacoras != null && bitacoras.size() > 0){
			BitacoraNotificacion bitacora = bitacoras.get(0);
			bitacora.setRecurrente(recurrente);
			entidadService.save(bitacora);
		}
		
	}

	@Override
	public List<BitacoraRegistroDetalleNotificacionDTO> obtenerRecurrentes() {
		return  bitacoraNotificacionDao.obtenerRecurrentes();	
	}

	@Override
	public List<BitacoraDetalleNotificacion> obtenerBitacora(String folio) {
		BitacoraNotificacionFiltroDTO filtro = new BitacoraNotificacionFiltroDTO();
		filtro.setFolio(folio);
		return obtenerBitacora(filtro);
	}

	@Override
	public List<BitacoraDetalleNotificacion> obtenerBitacora(
			EnumCodigo codigo) {
		BitacoraNotificacionFiltroDTO filtro = new BitacoraNotificacionFiltroDTO();
		filtro.setCodigo(codigo);
		return obtenerBitacora(filtro);
	}

	@Override
	public List<BitacoraDetalleNotificacion> obtenerBitacora(
			BitacoraNotificacionFiltroDTO filtro) {
		return bitacoraNotificacionDao.obtenerRegistros(filtro);
	}
	
	@Override
	public boolean salvarBitacora(String folio, EnumCodigo codigo, EmailDestinaratios destinatarios, 
			Map<String, Serializable> mapaDatos){		
		return salvarBitacora(folio, codigo.toString(), destinatarios, mapaDatos);
	}
	
	@Override
	public boolean salvarBitacora(String folio, EnumCodigo codigo, String destinatario, 
			Map<String, Serializable> mapaDatos){		
		return salvarBitacora(folio, codigo.toString(), destinatario, mapaDatos);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean salvarBitacora(String folio, String codigo, String destinatario, 
			Map<String, Serializable> mapaDatos) {
		try{
			BitacoraNotificacion bitacora = obtenerBitacora(folio, codigo);
			generarDestinos(bitacora, destinatario, mapaDatos);
			entidadService.save(bitacora);
		}catch(Exception ex){
			log.error("Ocurrio un error al registrar la notificacion con folio: ".concat(folio),ex);
			return false;
		}
		return true;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean salvarBitacora(String folio, String codigo, EmailDestinaratios destinatarios, 
			Map<String, Serializable> mapaDatos) {
		try{
			BitacoraNotificacion bitacora = obtenerBitacora(folio, codigo);
			List<String> destinos = destinatarios.obtenerDestinatarios();		
			for(String destinatario : destinos){	
				generarDestinos(bitacora, destinatario, mapaDatos);
			}			
			entidadService.save(bitacora);
		}catch(Exception ex){
			log.error("Ocurrio un error al registrar la notificacion con folio: ".concat(folio),ex);
			return false;
		}
		return true;
	}
	
	private BitacoraNotificacion obtenerBitacora(String folio, String codigo){
		List<ConfiguracionNotificacionCabina> config = entidadService.findByProperty(ConfiguracionNotificacionCabina.class, 
				"movimientoProceso.codigo", codigo);
		String codigoUsuarioCreacion = usuarioService.getUsuarioActual() != null ? 
				usuarioService.getUsuarioActual().getNombreUsuario() : "ADMIN";
		if(config == null || config.get(0) == null){
			throw new RuntimeException("No se encontro configuracion para el codigo que se desea registrar");
		}
		List<BitacoraNotificacion> bitacoras = entidadService.findByProperty(BitacoraNotificacion.class, "folio", folio);		
		if(bitacoras == null || bitacoras.size() == 0){		
			return new BitacoraNotificacion(folio, config.get(0), codigoUsuarioCreacion);			
		}
		return bitacoras.get(0);
	}

	private void generarDestinos(BitacoraNotificacion bitacora, String destinatario, Map<String, Serializable> mapaDatos){		
		BitacoraDestinoNotificacion destino = bitacora.obtener(destinatario);
		String codigoUsuarioCreacion = usuarioService.getUsuarioActual() != null ? 
				usuarioService.getUsuarioActual().getNombreUsuario() : "ADMIN";
		String mapa = gson.toJson(mapaDatos);	
		if(destino != null){				
			destino.agregar(new BitacoraDetalleNotificacion(destino.siguienteSecuencia(), mapa, codigoUsuarioCreacion));
		}else{
			destino = new BitacoraDestinoNotificacion(destinatario, codigoUsuarioCreacion);
			destino.agregar(new BitacoraDetalleNotificacion(null, mapa, codigoUsuarioCreacion));
			bitacora.agregar(destino);	
		}					
	}
	
	@Override
	public void agregarDetalle(Long idBitacora, String destinatario,
			Map<String, Serializable> mapaDatos) {
		BitacoraNotificacion bitacora = entidadService.findById(BitacoraNotificacion.class, idBitacora);		
		if(bitacora != null){
			generarDestinos(bitacora, destinatario, mapaDatos);
			entidadService.save(bitacora);
		}		
	}

	@Override
	public void agregarDetalle(String folio, String destinatario,
			Map<String, Serializable> mapaDatos) {
		List<BitacoraNotificacion> bitacoras = entidadService.findByProperty(BitacoraNotificacion.class, "folio", folio);
		if(bitacoras != null && bitacoras.size() > 0){
			agregarDetalle(bitacoras.get(0).getId(), destinatario, mapaDatos);
		}		
	}

}
