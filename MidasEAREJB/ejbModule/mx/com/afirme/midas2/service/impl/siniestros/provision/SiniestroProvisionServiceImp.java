package mx.com.afirme.midas2.service.impl.siniestros.provision;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.siniestros.provision.SiniestrosProvisionDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionCiaDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CoberturaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.PaseRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.VentaSalvamento;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionCiaDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.provision.SiniestrosProvisionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionProveedorService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.util.MidasException;




@Stateless
public class SiniestroProvisionServiceImp implements SiniestrosProvisionService {
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private SiniestrosProvisionDao siniestrosProvisionDao;
	
	@EJB
	private RecuperacionCiaDao recuperacionCiaDao;
	
	@EJB
	private RecuperacionCiaService recuperacionCiaService; 
	
	@EJB
	private RecuperacionDao  recuperacionDao;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@EJB
	private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	@EJB
	private RecuperacionProveedorService recuperacionProveedorService;
	
	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	
	@Override
	public void revierteProvisionAlCancelarRecuperacionCia(Long recuperacionId){
		SiniestrosProvisionService processor = this.sessionContext.getBusinessObject(SiniestrosProvisionService.class);
		processor.creaMovimientosAlCancelarRecuperacionCia(recuperacionId);
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		EstimacionCoberturaReporteCabina estimacion = this.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
		processor.actualizaProvision(estimacion.getId(), MovimientoProvisionSiniestros.Origen.PASE);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void creaMovimientosAlCancelarRecuperacionCia(Long recuperacionId){
		
		BigDecimal montoGrua =  BigDecimal.ZERO;
		BigDecimal nuevaProvision = BigDecimal.ZERO;
		BigDecimal montoExclusion  =BigDecimal.ZERO; 
		
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		EstimacionCoberturaReporteCabina estimacion = this.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
		MovimientoProvisionSiniestros movimientoProvisionado = this.obtenerMovimientoProvisionado(estimacion.getId(), MovimientoProvisionSiniestros.Origen.PASE );
		
		if(movimientoProvisionado!=null){
			
			montoGrua = recuperacion.getMontoGAGrua();
			nuevaProvision = movimientoProvisionado.getProvisionActual();
			if(montoGrua.compareTo(BigDecimal.ZERO)!=0){
				nuevaProvision = nuevaProvision.subtract(montoGrua);
				MovimientoProvisionSiniestros movimientoGrua = this.crearMovimimento(estimacion.getId(), montoGrua.negate(), nuevaProvision, 
						MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.CANCELACION_RECUPERACION.getValue());
				this.entidadService.save(movimientoGrua);
			}
			
			List<CartaCompania> cartas = this.ordenaCartas(recuperacion.getCartas());
			if( !cartas.isEmpty() ){
				CartaCompania ultimaCarta = cartas.get(cartas.size()-1);
				montoExclusion  = ultimaCarta.getMontoExclusion();
				if(montoExclusion!=null && montoExclusion.compareTo(BigDecimal.ZERO)!=0){
					nuevaProvision = nuevaProvision.add(montoExclusion);
					MovimientoProvisionSiniestros movimientoExclusion = this.crearMovimimento(estimacion.getId(), montoExclusion, nuevaProvision, 
							MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.CANCELACION_RECUPERACION.getValue());
					this.entidadService.save(movimientoExclusion);
				}
			}
			
		}
	}
	
	@Override
	public void actualizaProvisionAlAsignarGruas(Long recuperacionId, BigDecimal montoGruasInicial){
		SiniestrosProvisionService processor = this.sessionContext.getBusinessObject(SiniestrosProvisionService.class);
		MovimientoProvisionSiniestros movimientoGrua  = processor.creaMovimientosAlAsignarGruasRecuperacionCia(recuperacionId, montoGruasInicial);
		if(movimientoGrua!=null){
			RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
			EstimacionCoberturaReporteCabina estimacion = this.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
			processor.actualizaProvision(estimacion.getId(), MovimientoProvisionSiniestros.Origen.PASE);
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public MovimientoProvisionSiniestros creaMovimientosAlAsignarGruasRecuperacionCia(Long recuperacionId , BigDecimal montoGruasInicial){
		//BigDecimal movimientosGruas = BigDecimal.ZERO;
		BigDecimal diferenciaGruas = BigDecimal.ZERO;
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionId);
		EstimacionCoberturaReporteCabina estimacion = this.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
		
		
		//movimientosGruas = this.siniestrosProvisionDao.obtenerSumaGruasPorEstimacion(estimacion.getId());
	
		MovimientoProvisionSiniestros movimientoGrua = null;
		
		//if( movimientosGruas.compareTo(BigDecimal.ZERO) != 0 ){
			//System.out.println("Se provisiona");
			
			BigDecimal movimientoProvisionar = BigDecimal.ZERO;
			if( montoGruasInicial.compareTo(BigDecimal.ZERO) == 0 ){
				movimientoProvisionar = recuperacion.getMontoGAGrua();
			}else{
				diferenciaGruas = recuperacion.getMontoGAGrua().subtract(montoGruasInicial);
				movimientoProvisionar = diferenciaGruas;
			}
			
			if( movimientoProvisionar.compareTo(BigDecimal.ZERO)!= 0){
				
				MovimientoProvisionSiniestros montoProvisionado = this.obtenerMovimientoProvisionado(estimacion.getId(), MovimientoProvisionSiniestros.Origen.PASE);
				
				if( montoProvisionado != null ){
					
					BigDecimal nuevaProvision = montoProvisionado.getProvisionActual().add(movimientoProvisionar);
					
					movimientoGrua = this.crearMovimimento(
							estimacion.getId(), 
							movimientoProvisionar, 
							nuevaProvision, 
							MovimientoProvisionSiniestros.Origen.PASE.getValue(), 
							MovimientoProvisionSiniestros.CAUSA.GRUA.getValue()
					);
					
					this.entidadService.save(movimientoGrua);
				}

			
			}
		//}
		
		return movimientoGrua;
	}
	
	
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void generaYGuardaMovimientoDeAplicacionDeIngreso(Long ingresoId){
		Ingreso ingreso = this.entidadService.findById(Ingreso.class, ingresoId);
		
		if( ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue())
				|| ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.PROVEEDOR.getValue())
				|| ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.COMPANIA.getValue())
		  ){

			
			BigDecimal montoIngreso = ingreso.getMonto();
			
			if(ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue())){
				//SE CREA EL MOVIMIENTO PARA AFECTAR LA PROVISION DEL PASE DE ATENCION 
				RecuperacionSalvamento salvamento = ((RecuperacionSalvamento)ingreso.getRecuperacion());
				EstimacionCoberturaReporteCabina estimacion = salvamento.getEstimacion();
				VentaSalvamento venta = this.recuperacionSalvamentoService.obtenerVentaSalvamentoActiva(salvamento.getId());
				BigDecimal montoSinImpuestos = venta.getSubtotal(); 
				BigDecimal montoIngresoConPorcentaje = this.obtenerMontoConsiderandoPorcentajeParticipacion(estimacion.getId(), montoSinImpuestos, null);
				MovimientoProvisionSiniestros movimientoProvisionado = this.obtenerMovimientoProvisionado(estimacion.getId(),MovimientoProvisionSiniestros.Origen.PASE);
				BigDecimal nuevaProvision = movimientoProvisionado.getProvisionActual().subtract(montoIngresoConPorcentaje);
				MovimientoProvisionSiniestros movimientoSalvamentoDePase = this.crearMovimimento(estimacion.getId(), montoIngresoConPorcentaje.negate(), nuevaProvision, 
						MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.APLICACION_INGRESO_SALVAMENTO.getValue());
				this.entidadService.save(movimientoSalvamentoDePase);
				
				try{
					//SE CREA EL MOVIMIENTO PARA AFECTAR LA PROVISION DE LA RECUPERACION DE SALVAMENTO
					MovimientoProvisionSiniestros movimientoSalvamentoProvisionado = this.obtenerMovimientoProvisionado(salvamento.getId(), MovimientoProvisionSiniestros.Origen.RECUPERACION_SALVAMENTO);
					BigDecimal montoAProvisionar = movimientoSalvamentoProvisionado.getProvisionActual();
					MovimientoProvisionSiniestros movimientoSalvamento = this.crearMovimimento(salvamento.getId(),montoAProvisionar.negate(), BigDecimal.ZERO, 
							MovimientoProvisionSiniestros.Origen.RECUPERACION_SALVAMENTO.getValue(), MovimientoProvisionSiniestros.CAUSA.APLICACION_INGRESO_SALVAMENTO.getValue());
					this.entidadService.save(movimientoSalvamento);
					
					MovimientoProvisionSiniestros movimientoSalvamentoProvisionadoIva = this.obtenerMovimientoProvisionado(salvamento.getId(), MovimientoProvisionSiniestros.Origen.RECUPERACION_SALVAMENTO_IVA);
					BigDecimal montoIvaProvisionar = movimientoSalvamentoProvisionadoIva.getProvisionActual();
					MovimientoProvisionSiniestros movimientoSalvamentoIva = this.crearMovimimento(salvamento.getId(), montoIvaProvisionar.negate(), BigDecimal.ZERO,
							MovimientoProvisionSiniestros.Origen.RECUPERACION_SALVAMENTO_IVA.getValue(), MovimientoProvisionSiniestros.CAUSA.APLICACION_INGRESO_SALVAMENTO.getValue());
					this.entidadService.save(movimientoSalvamentoIva);
				}catch (Throwable e) {
					e.printStackTrace();
				}
				
			}
			
			if(ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.PROVEEDOR.getValue())){
				RecuperacionProveedor recuperacion = (RecuperacionProveedor)ingreso.getRecuperacion();
				Long estimacionId = recuperacion.getOrdenCompra().getIdTercero();
				BigDecimal montoSinImpuestos = recuperacion.getSubTotal();
				BigDecimal montoIngresoConPorcentaje = this.obtenerMontoConsiderandoPorcentajeParticipacion(estimacionId, montoSinImpuestos, null);
				MovimientoProvisionSiniestros movimientoProvisionado = this.obtenerMovimientoProvisionado(estimacionId,MovimientoProvisionSiniestros.Origen.PASE);
				BigDecimal nuevaProvision = movimientoProvisionado.getProvisionActual().subtract(montoIngresoConPorcentaje);
				MovimientoProvisionSiniestros movimientoProveedorDePase = this.crearMovimimento(estimacionId, montoIngresoConPorcentaje.negate(), nuevaProvision, 
						MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.APLICACION_INGRESO_PROVEEDOR.getValue());
				this.entidadService.save(movimientoProveedorDePase);
			}
			
			if(ingreso.getRecuperacion().getTipo().equals(Recuperacion.TipoRecuperacion.COMPANIA.getValue())){
				
				EstimacionCoberturaReporteCabina estimacion = this.obtenerEstimacionDeRecuperacionCia( ingreso.getRecuperacion().getId() );
				MovimientoProvisionSiniestros movimientoProvisionado = this.obtenerMovimientoProvisionado(estimacion.getId(),MovimientoProvisionSiniestros.Origen.PASE);
				BigDecimal nuevaProvision = movimientoProvisionado.getProvisionActual().subtract(montoIngreso);
				MovimientoProvisionSiniestros movimientoProveedorDePase = this.crearMovimimento(estimacion.getId(), montoIngreso.negate(), nuevaProvision, 
						MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.APLICACION_INGRESO_COMPANIA.getValue());
				this.entidadService.save(movimientoProveedorDePase);
				
			}			
			
			
		}
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void generaYGuardaMovimientoDeExclusionDeCartaCia(Long recuperacionCiaId){
		RecuperacionCompania recuperacion = this.entidadService.findById(RecuperacionCompania.class, recuperacionCiaId);
		EstimacionCoberturaReporteCabina estimacion = this.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
//		List<CartaCompania> cartas = this.entidadService.findByProperty(CartaCompania.class, "recuperacion.id", recuperacionCiaId);
//		if(cartas!=null && cartas.size()>0){
			
//			cartas = this.ordenaCartas(cartas);
//			CartaCompania original = cartas.get(0);
//			CartaCompania ultima = cartas.get(cartas.size()-1);
//			BigDecimal diferencia = original.getMontoARecuperar().subtract(ultima.getMontoARecuperar());
		CartaCompania cartaActiva = this.recuperacionCiaService.obtenerCartaActiva(recuperacionCiaId);
		BigDecimal diferencia = cartaActiva.getMontoExclusion();
		if(diferencia.compareTo(BigDecimal.ZERO)>0){
			MovimientoProvisionSiniestros movimientoProvisionado = this.obtenerMovimientoProvisionado(estimacion.getId(),MovimientoProvisionSiniestros.Origen.PASE);
			BigDecimal nuevaProvision = movimientoProvisionado.getProvisionActual().subtract(diferencia);
			MovimientoProvisionSiniestros movimiento = this.crearMovimimento(estimacion.getId(), diferencia.negate(), nuevaProvision, 
					MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.EXCLUSION_CARTA_CIA.getValue());
			this.entidadService.save(movimiento);
		}
//		}
	}
	
	private List<CartaCompania> ordenaCartas(List<CartaCompania> cartas){
		Comparator<CartaCompania> comparator = new Comparator<CartaCompania>() {
		    public int compare(CartaCompania c1, CartaCompania c2) {
		        return c1.getId().compareTo(c2.getId());
		    }
		};
		Collections.sort(cartas,comparator);
		return cartas;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelaProvisionesDeCiaPorReporte(Long reporteId){
		List<MovimientoProvisionSiniestros> movimientos = this.siniestrosProvisionDao.obtenerMovimientosProvisionadosPorPreporte(reporteId,MovimientoProvisionSiniestros.Origen.PASE );
		for(MovimientoProvisionSiniestros movimiento : movimientos){
			//Cancela las provisiones activas
			this.cancelaProvision(movimiento.getId());
			MovimientoProvisionSiniestros movimientoCancelacion = this.generaMovimientoDeCancelacion(movimiento,MovimientoProvisionSiniestros.CAUSA.INACTIVACION_DE_CIA);
			this.entidadService.save(movimientoCancelacion);
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void habilitaProvisionesDeCiaPorReporte(Long reporteCabinaId){
		//Obtiene las recuperaciones en caso de tener
		SiniestrosProvisionService processor = this.sessionContext.getBusinessObject(SiniestrosProvisionService.class);
		RecuperacionCiaDTO filtro = new RecuperacionCiaDTO(); 
		filtro.setReporteCabinaId(reporteCabinaId); 
		List<RecuperacionCompania> recuperaciones = this.recuperacionCiaDao.buscarRecuperacionesCia(filtro);
		for( RecuperacionCompania recuperacion : recuperaciones  ){
			EstimacionCoberturaReporteCabina estimacion = this.obtenerEstimacionDeRecuperacionCia(recuperacion.getId());
			if(estimacion != null){
				MovimientoProvisionSiniestros movimientoAProvisionar = processor.generaMovimientosAlHabilitar(estimacion);
				processor.creaProvision(movimientoAProvisionar.getId());
			}
		}
	}
	
	
	@Override
	public  EstimacionCoberturaReporteCabina obtenerEstimacionDeRecuperacionCia(Long recuperacionId){
		
		EstimacionCoberturaReporteCabina estimacion = null;
		List<CoberturaRecuperacion> cobRecList = this.entidadService.findByProperty(CoberturaRecuperacion.class, "recuperacion.id", recuperacionId);
		
		if(cobRecList!= null && cobRecList.size()>0){
			CoberturaRecuperacion  coberturaRecuperacion = cobRecList.get(0);
			List<PaseRecuperacion> pases =   coberturaRecuperacion.getPasesAtencion();
			if(pases!=null && pases.size()>0){
				estimacion = pases.get(0).getEstimacionCobertura();
			}
		}
		return estimacion;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public MovimientoProvisionSiniestros generaMovimientosAlHabilitar(EstimacionCoberturaReporteCabina estimacion){
		MovimientoProvisionSiniestros movimientoAProvisionar = null;
		BigDecimal montoProvision = BigDecimal.ZERO;
		BigDecimal estimacionActual = this.movimientoSiniestroService.obtenerMontoEstimacionActual(null, null, estimacion.getId(), null, null, null);
		
		
		if(estimacionActual.compareTo(BigDecimal.ZERO)!=0){
			
			montoProvision = estimacionActual;
			MovimientoProvisionSiniestros movimientoInicial = this.crearMovimimento(estimacion.getId(), estimacionActual, montoProvision, 
					MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.AJUSTE_RESERVA.getValue());
			this.entidadService.save(movimientoInicial);
			
			
			//Valida si se necesita insertar un movimiento de salvamento
			RecuperacionSalvamento salvamento = this.recuperacionSalvamentoService.obtenerRecuperacionSalvamento(estimacion.getId());
			if(salvamento!=null && salvamento.getEstatus().equals(Recuperacion.EstatusRecuperacion.RECUPERADO.getValue())){
				BigDecimal montoSalvamento = this.recuperacionSalvamentoService.obtenerVentaSalvamentoActiva(salvamento.getId()).getTotalVenta();
				BigDecimal montoSalvamentoConPorcentaje = this.obtenerMontoConsiderandoPorcentajeParticipacion(estimacion.getId(), montoSalvamento, null); 
				montoProvision = montoProvision.subtract(montoSalvamentoConPorcentaje);
				MovimientoProvisionSiniestros movimientoSalvamento = this.crearMovimimento(estimacion.getId(), montoSalvamentoConPorcentaje.negate(),montoProvision, 
						MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.ACTIVACION_DE_CIA.getValue());
				this.entidadService.save(movimientoSalvamento);
				
			}
			//Valida si se necesita insertar un movimiento de piezas ( Recuperación de Proveedor )
			BigDecimal montoPiezasRecuperadas = this.recuperacionProveedorService.montoTotalRecuperacionProveedorPagadas(estimacion.getId());
			BigDecimal montoPiezasRecuperadasConPorcentaje = this.obtenerMontoConsiderandoPorcentajeParticipacion(estimacion.getId(), montoPiezasRecuperadas, null); 
			if(montoPiezasRecuperadasConPorcentaje.compareTo(BigDecimal.ZERO)>0){
				montoProvision = montoProvision.subtract(montoPiezasRecuperadasConPorcentaje);
				MovimientoProvisionSiniestros movimientoPiezas = this.crearMovimimento(estimacion.getId(), montoPiezasRecuperadasConPorcentaje.negate(),montoProvision, 
						MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.ACTIVACION_DE_CIA.getValue());
				this.entidadService.save(movimientoPiezas);
				movimientoAProvisionar = movimientoPiezas;
			}
			//Valida si se necesita insertar un movimiento de recuperaciones de compañias aplicadas
			BigDecimal montoRecuperacionRecuperada = this.obtenerMontoRecuperacionesConsiderandoPorcentajePorEstimacion(estimacion.getId());
			if( montoRecuperacionRecuperada.compareTo(BigDecimal.ZERO)!= 0 ){
				montoProvision = montoProvision.subtract(montoRecuperacionRecuperada);
				MovimientoProvisionSiniestros movimientoRecuperacionRecuperarada = this.crearMovimimento(estimacion.getId(), montoRecuperacionRecuperada.negate(),montoProvision, 
						MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.ACTIVACION_DE_CIA.getValue());
				this.entidadService.save(movimientoRecuperacionRecuperarada);
			}
			BigDecimal	montoAProvisionar = this.obtenerMontoConsiderandoPorcentajeParticipacionPorRecuperacion(estimacion.getId(), montoProvision);
			BigDecimal montoDeAjuste = montoProvision.subtract(montoAProvisionar);
			montoProvision = montoAProvisionar;
			movimientoAProvisionar = this.crearMovimimento(estimacion.getId(), montoDeAjuste.negate(),montoProvision, 
					MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.ACTIVACION_DE_CIA.getValue());
			this.entidadService.save(movimientoAProvisionar);
			
			
		}

		return movimientoAProvisionar;
	}
	
	
	@Override
	public BigDecimal obtenerMontoRecuperacionesConsiderandoPorcentajePorEstimacion(Long estimacionId){
		BigDecimal monto = BigDecimal.ZERO;
		BigDecimal montoCien = new BigDecimal(100);
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("estimacionCobertura.id", estimacionId);
		params.put("coberturaRecuperacion.recuperacion.tipo", Recuperacion.TipoRecuperacion.COMPANIA.getValue());
		params.put("coberturaRecuperacion.recuperacion.estatus", Recuperacion.EstatusRecuperacion.RECUPERADO.getValue());
		List<PaseRecuperacion> pases = entidadService.findByPropertiesWithOrder(PaseRecuperacion.class,params, "coberturaRecuperacion.recuperacion.id DESC");
		if(pases!=null && !pases.isEmpty()){
			for(PaseRecuperacion pase : pases){
				Integer porcentajeDeParticipacion = ((RecuperacionCompania)pase.getCoberturaRecuperacion().getRecuperacion()).getPorcentajeParticipacion();
				BigDecimal importeIncial = ((RecuperacionCompania)pase.getCoberturaRecuperacion().getRecuperacion()).getImporteInicial();
				BigDecimal montoTotal = (importeIncial.multiply(montoCien)).divide(new BigDecimal(porcentajeDeParticipacion));
				monto = monto.add(montoTotal);
			}
		}
		return monto;
	}
	
	

	/*
	 * Al invocar a este método ya se debio generar el movimiento de reserva correspondiente para poder
	 *	obtener la diferencia del monto anterior y el actual
	 * */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void generaMovimientoDeAjusteReserva(Long estimacionId) {
		EstimacionCoberturaReporteCabina pase = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
		if(esValidaLaEstimacionParaCrearProvision(pase)){
			HashMap<String, Object> params = new HashMap<String, Object>();
			params.put("estimacionCobertura.id", estimacionId);
			List<MovimientoCoberturaSiniestro> movimientos = entidadService.findByPropertiesWithOrder(MovimientoCoberturaSiniestro.class,params, "id DESC");
			if(movimientos!=null && !movimientos.isEmpty()){
				MovimientoCoberturaSiniestro ultimoMovimiento = movimientos.get(0);
				BigDecimal nuevaProvision = BigDecimal.ZERO;
				BigDecimal montoMovimientoAProvisionar = BigDecimal.ZERO;
				if(ultimoMovimiento.getTipoMovimiento().equals(MovimientoCoberturaSiniestro.TipoMovimiento.ESTIMACION_ORIGINAL.getValue())){
					nuevaProvision = this.obtenerMontoConsiderandoPorcentajeParticipacion(estimacionId, ultimoMovimiento.getImporteMovimiento(),null);
					montoMovimientoAProvisionar = nuevaProvision; 
					
				}else{
					montoMovimientoAProvisionar = this.obtenerMontoConsiderandoPorcentajeParticipacion(estimacionId, ultimoMovimiento.getImporteMovimiento(),null);
					
					MovimientoProvisionSiniestros movimientoProvisionado = this.obtenerMovimientoProvisionado(estimacionId,MovimientoProvisionSiniestros.Origen.PASE);
					if(movimientoProvisionado!=null){
						nuevaProvision = montoMovimientoAProvisionar.add(movimientoProvisionado.getProvisionActual());
					}else{
						nuevaProvision = montoMovimientoAProvisionar;
					}
				}
				MovimientoProvisionSiniestros movimientoProvision = this.crearMovimimento(estimacionId, montoMovimientoAProvisionar, nuevaProvision, 
						MovimientoProvisionSiniestros.Origen.PASE.getValue(), MovimientoProvisionSiniestros.CAUSA.AJUSTE_RESERVA.getValue());
				this.entidadService.save(movimientoProvision);
			}
		}
	}
	
	public BigDecimal obtenerMontoConsiderandoPorcentajeParticipacion(Long estimacionId, BigDecimal montoInicial,Integer porcentajeDeParticipacion ){
		if(porcentajeDeParticipacion==null){
			EstimacionCoberturaReporteCabina estimacion = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
			porcentajeDeParticipacion = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getPorcentajeParticipacion();
		}
		BigDecimal monto = montoInicial.multiply(new BigDecimal((porcentajeDeParticipacion == null ? 0 : porcentajeDeParticipacion )).divide(new BigDecimal (100)));
		return monto;
	}
	
	
	
	
	private BigDecimal obtenerMontoConsiderandoPorcentajeParticipacionPorRecuperacion(Long estimacionId, BigDecimal montoEstimacion){
		BigDecimal monto = BigDecimal.ZERO;
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("estimacionCobertura.id", estimacionId);
		params.put("coberturaRecuperacion.recuperacion.tipo", Recuperacion.TipoRecuperacion.COMPANIA.getValue());
		List<PaseRecuperacion> pases = entidadService.findByPropertiesWithOrder(PaseRecuperacion.class,params, "coberturaRecuperacion.recuperacion.id DESC");
		if(pases!=null && !pases.isEmpty()){
			Integer porcentajeDeParticipacion = ((RecuperacionCompania)pases.get(0).getCoberturaRecuperacion().getRecuperacion()).getPorcentajeParticipacion();
			monto = this.obtenerMontoConsiderandoPorcentajeParticipacion(null,montoEstimacion,porcentajeDeParticipacion);
		}
		
		return monto;
	}
	
	
	
	private MovimientoProvisionSiniestros generaMovimientoDeCancelacion(MovimientoProvisionSiniestros movimiento,MovimientoProvisionSiniestros.CAUSA causa){
		MovimientoProvisionSiniestros movimientoDeCancelacion = new MovimientoProvisionSiniestros();
		
		movimientoDeCancelacion.setIdRefrencia(movimiento.getIdRefrencia());
		movimientoDeCancelacion.setOrigen(movimiento.getOrigen());
		movimientoDeCancelacion.setCausa(causa.getValue());
		movimientoDeCancelacion.setMontoMovimiento(movimiento.getProvisionActual().negate());
		movimientoDeCancelacion.setProvisionActual(BigDecimal.ZERO);
		movimientoDeCancelacion.setEstaProvisionado(Boolean.FALSE);
		movimientoDeCancelacion.setFechaCreacion(new Date());
		movimientoDeCancelacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		movimientoDeCancelacion.setFechaModificacion(new Date());
		movimientoDeCancelacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		
		return movimientoDeCancelacion;
	}
	
	//Solamente debe existir un movimiento provisionado por pase de atencion
	public MovimientoProvisionSiniestros obtenerMovimientoProvisionado(Long idReferencia,MovimientoProvisionSiniestros.Origen origen ){
		MovimientoProvisionSiniestros movimientoProvisionado = null;
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("idRefrencia", idReferencia);
		params.put("origen", origen.getValue());
		params.put("estaProvisionado", Boolean.TRUE);
		List<MovimientoProvisionSiniestros> movimientos = this.entidadService.findByProperties(MovimientoProvisionSiniestros.class, params);
		if(movimientos!=null && !movimientos.isEmpty()){
			movimientoProvisionado = movimientos.get(0);
		}
		return movimientoProvisionado;
	}
	
	
	private boolean esValidaLaEstimacionParaCrearProvision(EstimacionCoberturaReporteCabina estimacion){
        if (estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() != null 
                && estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1) {
        	return Boolean.TRUE;
        }
        return Boolean.FALSE;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void actualizaProvision(Long idReferencia,MovimientoProvisionSiniestros.Origen origen){
		HashMap<String, Object> params = new HashMap<String, Object>();
		params.put("idRefrencia", idReferencia);
		params.put("origen", origen.getValue());
		List<MovimientoProvisionSiniestros> movimientos = entidadService.findByPropertiesWithOrder(MovimientoProvisionSiniestros.class,params, "id DESC");
		if(movimientos!=null && !movimientos.isEmpty()){
			MovimientoProvisionSiniestros movimientoAProvisionar = movimientos.get(0); 
			//Si hay mas de un movimiento, se tiene que tomar el provisionado para cancelarlo
			if(movimientos.size()>1){
				MovimientoProvisionSiniestros movimientoACancelar = this.obtieneMovimientoProvisionado(movimientos);
				if(movimientoACancelar!=null){
					this.cancelaProvision(movimientoACancelar.getId());
				}
			}
			if(movimientoAProvisionar.getProvisionActual().compareTo(BigDecimal.ZERO)>0){
				this.creaProvision(movimientoAProvisionar.getId());
			}
		}
	}
	
	private MovimientoProvisionSiniestros obtieneMovimientoProvisionado(List<MovimientoProvisionSiniestros> movimientos){
		MovimientoProvisionSiniestros movtoProvisionado = null;
		for(MovimientoProvisionSiniestros movimiento: movimientos){
			if(movimiento.getEstaProvisionado()){
				movtoProvisionado = movimiento;
				break;
			}
		}
		return movtoProvisionado;
	}
	
	

	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void creaProvision(Long idMovimientoProvision){
		try {
			this.recuperacionDao.invocaProvision(idMovimientoProvision);
		} catch (MidasException e) {
			e.printStackTrace();
		}
		MovimientoProvisionSiniestros movimiento = this.entidadService.findById(MovimientoProvisionSiniestros.class, idMovimientoProvision);
		movimiento.setEstaProvisionado(Boolean.TRUE);
		this.entidadService.save(movimiento);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelaProvision(Long idMovimientoProvision){
		try {
			this.recuperacionDao.invocaProvision(idMovimientoProvision);
		} catch (MidasException e) {
			e.printStackTrace();
		}
		MovimientoProvisionSiniestros movimiento = this.entidadService.findById(MovimientoProvisionSiniestros.class, idMovimientoProvision);
		movimiento.setEstaProvisionado(Boolean.FALSE);
		this.entidadService.save(movimiento);
	}
	
	
	
	
	private MovimientoProvisionSiniestros crearMovimimento(final Long idRef, final BigDecimal monto, 
						final BigDecimal provision,final String origen,final String causa){
		MovimientoProvisionSiniestros movimiento = new MovimientoProvisionSiniestros();
		movimiento.setIdRefrencia(idRef);
		movimiento.setOrigen(origen);
		movimiento.setCausa(causa);
		movimiento.setMontoMovimiento(monto);
		movimiento.setProvisionActual(provision);
		movimiento.setEstaProvisionado(Boolean.FALSE);
		movimiento.setFechaCreacion(new Date());
		movimiento.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		movimiento.setFechaModificacion(new Date());
		movimiento.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		return movimiento;
	}

	public MovimientoProvisionSiniestros generaMovimientoDeRoboTotal(Long idSalvamento, BigDecimal monto){
        MovimientoProvisionSiniestros movimientoDeCancelacion = new MovimientoProvisionSiniestros();
        
        movimientoDeCancelacion.setIdRefrencia(idSalvamento);
        movimientoDeCancelacion.setOrigen(MovimientoProvisionSiniestros.Origen.RECUPERACION_SALVAMENTO.getValue());
        movimientoDeCancelacion.setCausa(MovimientoProvisionSiniestros.CAUSA.ACTIVACION_DE_SVM.getValue());
        movimientoDeCancelacion.setMontoMovimiento(monto);
        movimientoDeCancelacion.setProvisionActual(monto);
        movimientoDeCancelacion.setEstaProvisionado(Boolean.FALSE);
        movimientoDeCancelacion.setFechaCreacion(new Date());
        movimientoDeCancelacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
        movimientoDeCancelacion.setFechaModificacion(new Date());
        movimientoDeCancelacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
        
        return movimientoDeCancelacion;
  }


}
