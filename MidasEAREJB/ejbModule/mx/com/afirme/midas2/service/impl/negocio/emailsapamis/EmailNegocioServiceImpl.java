package mx.com.afirme.midas2.service.impl.negocio.emailsapamis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocio;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioTipoUsuarioService;

@Stateless
public class EmailNegocioServiceImpl implements EmailNegocioService{
	private EntidadService entidadService;
	private EmailNegocioTipoUsuarioService emailNegocioTipoUsuarioService;
	
	@Override
	public EmailNegocio completeObject(EmailNegocio arg0) {
		EmailNegocio retorno = new EmailNegocio(); 
		if(arg0 != null){
			if(arg0.getIdEmailNegocio() < 1){
				retorno = findIdByAttributes(arg0);
			}else{
				if(!validateAttributes(arg0)){
					retorno = findById(arg0.getIdEmailNegocio());
				}
			}
		}else{
			retorno.setIdEmailNegocio(Long.valueOf(-1));
		}
		return retorno;
	}

	@Override
	public List<EmailNegocio> findAll() {
		List<EmailNegocio> retorno = entidadService.findAll(EmailNegocio.class);
		return retorno;
	}

	@Override
	public EmailNegocio findById(Long arg0) {
		EmailNegocio retorno = new EmailNegocio();
		if(arg0 != null && arg0 >= 0){
			retorno = entidadService.findById(EmailNegocio.class, arg0);
		}
		return retorno;
	}

	@Override
	public List<EmailNegocio> findByProperty(String arg0, Object arg1) {
		List<EmailNegocio> emailNegocioAlertasList = new ArrayList<EmailNegocio>();
		if(arg0 != null && !arg0.equals("") && arg1 != null){
			emailNegocioAlertasList = entidadService.findByProperty(EmailNegocio.class, arg0, arg1);
		}
		return emailNegocioAlertasList;
	}

	@Override
	public List<EmailNegocio> findByStatus(boolean arg0) {
		List<EmailNegocio> emailNegocioAlertas = entidadService.findByProperty(EmailNegocio.class, "estatus", arg0?0:1);
		return emailNegocioAlertas;
	}

	@Override
	public EmailNegocio saveObject(EmailNegocio arg0) {
		if(arg0 != null && arg0.getIdEmailNegocio() >= 0){
			arg0.setIdEmailNegocio((Long)entidadService.saveAndGetId(arg0));
		}
		return arg0;
	}
	
	private boolean validateAttributes(EmailNegocio arg0){
		boolean retorno = true;
		if(retorno){
			retorno = arg0.getEmail() != null;
		}
		if(retorno){
			retorno = arg0.getEmailNegocioTipoUsuario() != null;
		}
		return retorno;
	}

	@Override
	public EmailNegocio findIdByAttributes(EmailNegocio arg0) {
		if(validateAttributes(arg0)){
			arg0.setEmailNegocioTipoUsuario(emailNegocioTipoUsuarioService.completeObject(arg0.getEmailNegocioTipoUsuario()));
			if(arg0.getEmailNegocioTipoUsuario().getIdEmailNegocioTipoUsuario() > 0){
				Map<String,Object> parametros = new HashMap<String,Object>();
				parametros.put("email", arg0.getEmail());
				parametros.put("idNegocio", arg0.getIdNegocio());
				parametros.put("emailNegocioTipoUsuario", arg0.getEmailNegocioTipoUsuario());			
				List<EmailNegocio> emailList = entidadService.findByProperties(EmailNegocio.class, parametros);
				if(emailList.size()>0){
					arg0.setIdEmailNegocio(emailList.get(0).getIdEmailNegocio());
				}else{
					arg0.setIdEmailNegocio(Long.valueOf(0));
				}
			}else{
				arg0.setIdEmailNegocio(Long.valueOf(-1));
			}
		}else{
			arg0.setIdEmailNegocio(Long.valueOf(-1));
		}
		return arg0;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setEmailNegocioTipoUsuarioService(
			EmailNegocioTipoUsuarioService emailNegocioTipoUsuarioService) {
		this.emailNegocioTipoUsuarioService = emailNegocioTipoUsuarioService;
	}
}
