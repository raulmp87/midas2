package mx.com.afirme.midas2.service.impl.siniestros.catalogo.conceptos;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas2.dao.siniestros.catalogo.CatalogoSiniestroDao;
import mx.com.afirme.midas2.dao.siniestros.catalogo.conceptoajuste.ConceptoAjusteDao;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoTipoPrestador;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoCobertura;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos.ConceptoCoberturaDTO;
import mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos.SeccionDTOConcepto;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ConceptoAjusteDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.conceptos.ConceptoAjusteService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.commons.collections.map.MultiValueMap;
import org.jfree.util.Log;

@Stateless
public class ConceptoAjusteServiceImpl implements ConceptoAjusteService  {
	
	private static final Long CONCEPTO_FUE_EDITADO	= new Long(0);

	private static final Long CONCEPTO_YA_EXISTE = new Long(-2);

	@PersistenceContext
	private EntityManager entityManager;
	
	@Resource
	private Validator validator;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService ;
	
	@EJB
	protected ConceptoAjusteDao conceptoAjusteDao;
	
	
	private static final String TIPO_MANOOBRA="MO";

	
	private static final String TIPO_REFACCIONES="REF";
	
	private static final String TIPO_CONCEPTO_OTROS="OT";
	
	private static final String TIPO_CONCEPTO_REEMBOLSO_GASTO_AJUSTE = "RGA";
	
	private static final Short CATEGORIA_CONCEPTO_REEMBOLSO_GASTO_AJUSTE = 3;
	


	@Override
	public void asociarCoberturas(Long keyConcepto,Long keyLineaDeNegocio, List<String> lListadoCoberturas) {
		
		ConceptoAjuste concepto = this.entidadService.findById(ConceptoAjuste.class, keyConcepto);
		
		// # BUSCA COBERTURAS POR CONCEPTO Y SECCION Y LAS ELIMINA ANTES DE AGREGAR
		this.borrarCoberturas(concepto, keyLineaDeNegocio);
		
		List<ConceptoCobertura> lConceptoCoberturas = new ArrayList<ConceptoCobertura>();
		
		for( int i=0; i<=lListadoCoberturas.size()-1; i++ ){
			
			// # DIVIDIR STRING YA QUE VIENE EN FORMATO 289-RCV
			String[] datoCob = lListadoCoberturas.get(i).split("-");
			
			CoberturaSeccionDTO coberturaSeccionDto = new CoberturaSeccionDTO();
			CoberturaSeccionDTOId key = new CoberturaSeccionDTOId( new BigDecimal(keyLineaDeNegocio) , new BigDecimal( datoCob[0] ));
				
			coberturaSeccionDto = this.entidadService.findById(CoberturaSeccionDTO.class, key);
			
			ConceptoCobertura conceptoCobertura = new ConceptoCobertura();
			conceptoCobertura.setFechaCreacion(new Date());
			conceptoCobertura.setCodigoUsuarioCreacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
			conceptoCobertura.setConceptoAjuste(concepto);
			conceptoCobertura.setCoberturaSeccion(coberturaSeccionDto);
			conceptoCobertura.setClaveSubcobertura(datoCob[1]);
			
			lConceptoCoberturas.add(conceptoCobertura);
		}
			
		if ( !lConceptoCoberturas.isEmpty() ){
			this.entidadService.saveAll(lConceptoCoberturas);
		}
			
		
	}
	
	@Override
	public void borrarCoberturas(ConceptoAjuste concepto,Long keyLineaDeNegocio) {
		
		// # BORRAR COBERTURAS
		
		Map<String,Object> params = new LinkedHashMap<String,Object>();
		params.put("conceptoAjuste.id"               , concepto.getId());
		params.put("coberturaSeccion.id.idtoseccion" , keyLineaDeNegocio);
		List<ConceptoCobertura> lConceptoCobertura = this.entidadService.findByProperties(ConceptoCobertura.class, params);
		
		if( !lConceptoCobertura.isEmpty() ){
			this.entidadService.removeAll(lConceptoCobertura);
		}
		
	}
	
	@Override
	public List<ConceptoCoberturaDTO> obtenerCoberturasAsociadasSeccionAjuste(Long keyConceptoAjuste, Long keyLineaDeNegocio) {
		
		// # OBTENER COBERTURAS
		
		StringBuilder queryString = new StringBuilder("SELECT new mx.com.afirme.midas2.dto.siniestros.catalogos.conceptos.ConceptoCoberturaDTO ");
		queryString.append(" ( cc.claveSubcobertura,cc.coberturaSeccion.id.idtocobertura ) ");
		queryString.append(" FROM ConceptoCobertura cc WHERE cc.conceptoAjuste.id = :keyConceptoAjuste AND cc.coberturaSeccion.id.idtoseccion = :keyLineaDeNegocio ");
		
		TypedQuery<ConceptoCoberturaDTO> query = entityManager.createQuery(queryString.toString(), ConceptoCoberturaDTO.class);
		
		query.setParameter("keyConceptoAjuste", keyConceptoAjuste);
		query.setParameter("keyLineaDeNegocio", keyLineaDeNegocio);
		
		return query.getResultList();		

	}
	
	@Override
	public List<ConceptoCobertura> buscarConceptoAjusteByCobertura(Long keyCobertura,
			Long keySeccion) {
		
		Map<String,Object> params = new LinkedHashMap<String,Object>();
		
		params.put("coberturaSeccion.id.idtoseccion", keySeccion);
		params.put("coberturaSeccion.id.idtocobertura", keyCobertura);
		
		List<ConceptoCobertura> lConceptoAjuste = this.entidadService.findByProperties(ConceptoCobertura.class, params);
	
		return lConceptoAjuste;

	}
	
	@Override
	public MultiValueMap obtenerCobertuasAsociadas(Long keyConceptoAjuste, Long keyLineaDeNegocio) {
		
		// # OBTENER ID DE COBERTURAS
		
		MultiValueMap data = new MultiValueMap();
		
		List<ConceptoCoberturaDTO> lConcepToCobertura = this.obtenerCoberturasAsociadasSeccionAjuste(keyConceptoAjuste, keyLineaDeNegocio);
		
		if( !lConcepToCobertura.isEmpty() ){
			for ( ConceptoCoberturaDTO cc : lConcepToCobertura ){
				data.put( cc.getClaveSubCobertura() , cc.getIdToCobertura() );
			}
		}

		return data;
	}

	@Override
	public Long crearCobertura(ConceptoAjuste conceptoAjuste) {
		
		conceptoAjuste.setTipoConcepto(TIPO_CONCEPTO_OTROS);
		
		Set<ConstraintViolation<ConceptoAjuste>> constraintViolations = validator.validate( conceptoAjuste );
		
		if (constraintViolations.size() > 0) {
				
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
				
		}

		
		
		if(conceptoAjuste.getId()==null){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("nombre", conceptoAjuste.getNombre());
			Long existeConceptoAjuste=this.entidadService.findByPropertiesCount(ConceptoAjuste.class, params);
			if(existeConceptoAjuste > 0){
				return CONCEPTO_YA_EXISTE;
			}
		}
		
		
		/*
		Map<String, Object> params = new HashMap<String, Object>();	
		if(conceptoAjuste.getTipoConcepto().equalsIgnoreCase(TIPO_REFACCIONES) || conceptoAjuste.getTipoConcepto().equalsIgnoreCase(TIPO_MANOOBRA) ){
			params.put("tipoConcepto", conceptoAjuste.getTipoConcepto());
			params.put("nombre", conceptoAjuste.getNombre());
			params.put("categoria", conceptoAjuste.getCategoria());
			Long existeConceptoAjuste=this.entidadService.findByPropertiesCount(ConceptoAjuste.class, params);
			if(existeConceptoAjuste > 1){
				return CONCEPTO_YA_EXISTE;
			}
		}
		*/
		
		if( conceptoAjuste.getId() != null ){
			// # ACTUALIZA
			ConceptoAjuste temp = this.entidadService.findById(ConceptoAjuste.class, conceptoAjuste.getId());
			String tipoConceptoActual = temp.getTipoConcepto();
			conceptoAjuste.setTipoConcepto(tipoConceptoActual);
			conceptoAjuste.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
			conceptoAjuste.setFechaModificacion(new Date());
			conceptoAjuste.setCodigoUsuarioCreacion(temp.getCodigoUsuarioCreacion());
			conceptoAjuste.setFechaCreacion(temp.getFechaCreacion());
			
			// # SI CAMBIO ESTATUS
			if( conceptoAjuste.getEstatus() != temp.getEstatus() ){
				conceptoAjuste.setFechaCambioEstatus(new Date());
			}
			this.establecerCuentaContableEnConceptoAjuste(conceptoAjuste);
			this.entidadService.save(conceptoAjuste);
			return CONCEPTO_FUE_EDITADO;
		}else{
			if(conceptoAjuste.getCategoria().compareTo(CATEGORIA_CONCEPTO_REEMBOLSO_GASTO_AJUSTE)==0){
				conceptoAjuste.setTipoConcepto(TIPO_CONCEPTO_REEMBOLSO_GASTO_AJUSTE);
			}else{
				conceptoAjuste.setTipoConcepto(TIPO_CONCEPTO_OTROS);
			}
			conceptoAjuste.setFechaCreacion(new Date());
			conceptoAjuste.setCodigoUsuarioCreacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
			this.establecerCuentaContableEnConceptoAjuste(conceptoAjuste);
			return (Long) this.entidadService.saveAndGetId(conceptoAjuste);
		}
	}

	/**
	 * Establece el valor de la Id de la cuenta contable dependiendo de la Categoria que se eligio para el concepto de ajuste
	 * @param conceptoAjuste
	 */
	private void establecerCuentaContableEnConceptoAjuste(ConceptoAjuste conceptoAjuste){
		Short categoria = conceptoAjuste.getCategoria();
		String idParametroGlobal = this.parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS , ParametroGlobalService.CONCEPTO_CUENTA_CONTABLE_PREFIJO);
		idParametroGlobal += categoria.toString();
		
		String idCuentaString = this.parametroGlobalService.obtenerValorPorIdParametro(ParametroGlobalService.AP_MIDAS, idParametroGlobal);
		conceptoAjuste.setIdCuentaContable(Long.parseLong(idCuentaString));
	}
	
	@Override
	public List<ConceptoAjuste> listarConceptos(short categoria) {
		
		Map<String,Object> params = new LinkedHashMap<String,Object>();
		params.put("categoria", categoria);
		
		return this.entidadService.findByProperties(ConceptoAjuste.class, params);
	}

	@Override
	public String[] obtenerDatoLineaNegocio(String nombreLineaNegocio){
		
		try{
			String[] aNombreLinea = null;
			
			if( nombreLineaNegocio != null ){
				aNombreLinea = nombreLineaNegocio.split("-");
			}
			
			return aNombreLinea;
		
		}catch(Exception e){
			return null;
		}
		 
	}


	@Override
	public void asociarTipoPrestador(Long keyConcepto,List<Long> listadoTipoPrestador) {
		
		ConceptoAjuste concepto = this.entidadService.findById(ConceptoAjuste.class, keyConcepto);
		
		// # BUSCA TIPO PRESTADOR POR CONCEPTO 
		this.borrarTipoPrestador(concepto);		
		List<ConceptoTipoPrestador> lConceptoTipoPrestador = new ArrayList<ConceptoTipoPrestador>();
		
		for(Long idTipo  : listadoTipoPrestador){
			ConceptoTipoPrestador conceptoTipoPrestador = new ConceptoTipoPrestador();
			conceptoTipoPrestador.setFechaCreacion(new Date());
			conceptoTipoPrestador.setCodigoUsuarioCreacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
			conceptoTipoPrestador.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
			conceptoTipoPrestador.setFechaModificacion(new Date());
			conceptoTipoPrestador.setConceptoAjuste(concepto);
			TipoPrestadorServicio tipoPrestador = this.entidadService.findById(TipoPrestadorServicio.class,idTipo);
			conceptoTipoPrestador.setTipoPrestadorServicio(tipoPrestador);
			lConceptoTipoPrestador.add(conceptoTipoPrestador);
		}
			
		if ( !lConceptoTipoPrestador.isEmpty() ){
			this.entidadService.saveAll(lConceptoTipoPrestador);
		}
			
		
	}
	
	
	@Override
	public List<ConceptoTipoPrestador> obtenerTipoPrestadorAsociados(Long keyConceptoAjuste) {
		// # OBTENER TIPO CONCEPTO		
		Map<String,Object> params = new LinkedHashMap<String,Object>();
		params.put("conceptoAjuste.id"   , keyConceptoAjuste);
		return this.entidadService.findByProperties(ConceptoTipoPrestador.class, params);

	}
	
	
	@Override
	public void borrarTipoPrestador(ConceptoAjuste concepto) {
		// # BORRAR TIPO PRESTADOR
		List<ConceptoTipoPrestador> lConceptoTipoPrestador = this.obtenerTipoPrestadorAsociados(concepto.getId());
		if( !lConceptoTipoPrestador.isEmpty() ){
			this.entidadService.removeAll(lConceptoTipoPrestador);
		}
		
	}
	
	@Override
	public List<ConceptoAjusteDTO> obtenerCoberturasPorConcepto( Long keyConcepto ){
		
		List<ConceptoAjusteDTO> lConceptoAjuste = new ArrayList<ConceptoAjusteDTO>();
		List lMultiSelecionadas = null;
		List<CoberturaDTO> conceptosGrid = new ArrayList<CoberturaDTO>();
		String claveSubcalculo = "n";
		Long coberturaId;
		
		try{
			
			// # OBTENER SECCIONES POR CONCEPTO
			List<BigDecimal> lSeccionesPorConcepto = this.obtenerSeccionesPorConcepto(keyConcepto);
			
			if( !lSeccionesPorConcepto.isEmpty() ){
				for( BigDecimal lsp : lSeccionesPorConcepto ){			
					
					List<CoberturaSeccionSiniestroDTO> lCoberturasSiniestro = this.estimacionCoberturaSiniestroService.obtenerCoberturasPorSeccion( new Long( lsp.toString() ) );
					
					MultiValueMap lConceptoCoberturaSeleccionadas = this.obtenerCobertuasAsociadas(keyConcepto, new Long( lsp.toString() ) );
					
					for ( CoberturaSeccionSiniestroDTO lCoberturaSeccion : lCoberturasSiniestro ){
					
						claveSubcalculo = ( StringUtil.isEmpty(lCoberturaSeccion.getClaveSubCalculo()) ? "n" : lCoberturaSeccion.getClaveSubCalculo() );
						coberturaId     = new Long ( lCoberturaSeccion.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura().toString()) ;
						
						Set entrySet = lConceptoCoberturaSeleccionadas.entrySet();
						Iterator it = entrySet.iterator();
						while (it.hasNext()) {
							
							Map.Entry mapEntry = (Map.Entry) it.next();
							lMultiSelecionadas = (List) lConceptoCoberturaSeleccionadas.get(mapEntry.getKey());
							for (int j = 0; j < lMultiSelecionadas.size(); j++) {
								if ( ( lMultiSelecionadas.get(j).toString().equals( coberturaId.toString() ) ) & ( claveSubcalculo.trim().equals(mapEntry.getKey().toString().trim()) ) ){
									
									ConceptoAjusteDTO conceptoAjusteDto = new ConceptoAjusteDTO();
									conceptoAjusteDto.setTipoConcepto( this.obtenerDatoLineaNegocio( lCoberturaSeccion.getNombreCobertura() )[0]   );
									conceptoAjusteDto.setNombre		 ( lCoberturaSeccion.getCoberturaSeccionDTO().getSeccionDTO().getDescripcion() );
									
									lConceptoAjuste.add(conceptoAjusteDto);
								}
							}
							
						}
					
					}
					
				}
				
			}

		}catch(Exception e){
			Log.error("Error al obtener coberturas por concepto metodo: obtenerCoberturasPorConcepto(Long) ",e);
		}
		
		return lConceptoAjuste;
	}
	
	
	private List<BigDecimal> obtenerSeccionesPorConcepto(Long keyConceptoAjuste){
		
		StringBuilder queryString = new StringBuilder("SELECT cc.coberturaSeccion.seccionDTO.idToSeccion ");
		queryString.append(" FROM ConceptoCobertura cc WHERE cc.conceptoAjuste.id = :keyConceptoAjuste GROUP BY cc.coberturaSeccion.seccionDTO.idToSeccion ");
		
		TypedQuery<BigDecimal> query = entityManager.createQuery(queryString.toString(), BigDecimal.class);
		
		query.setParameter("keyConceptoAjuste", keyConceptoAjuste);
		
		return query.getResultList();
	}
	
	
	public Map<Long,String> getListarSeccionesVigentesAutosUsablesSiniestros(){
		
		Map<Long,String> mDatosSeccion = new LinkedHashMap<Long,String>();
		
		List<SeccionDTOConcepto> lSecciones = conceptoAjusteDao.getListarSeccionesVigentesAutosUsablesSiniestros();
		
		if( !lSecciones.isEmpty() ) {
			for( SeccionDTOConcepto ls : lSecciones ){
				mDatosSeccion.put( new Long( ls.getIdSeccionConcepto().toString() ), ls.getNombreSeccionConcepto() );
			}
		}
		
		return mDatosSeccion;
	}
	
}
