package mx.com.afirme.midas2.service.impl.pago;

import java.sql.SQLException;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.conductodecobro.amex.gateway.AmexGatewayException;
import mx.com.afirme.conductodecobro.amex.gateway.ConductoCobroAmex;
import mx.com.afirme.conductodecobro.amex.gateway.dto.AmexDTO;
import mx.com.afirme.conductodecobro.amex.gateway.dto.AmexTransactionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dto.pago.ConfiguracionPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.ReciboDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaAmexDTO;
import mx.com.afirme.midas2.service.pago.AmexService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import com.js.util.StringUtil;



@Stateless
public class AmexServiceImpl implements AmexService{

	
	private static final String AMEX_GATEWAY_CODE_APROBADO = "APROBADO";
	private static final String AMEX_API_OPERATION_PAY = "PAY";
	private static final String AMEX_GATEWAY_ERROR_CODE = "9999";
	private static final String AMEX_GATEWAY_SECURITY_CODE_MATCH = "MATCH";
	private static final String AMEX_REF_MIDAS2 = "M2-";
	private static final String AMEX_TRANS_SRC_CARD_PRESENT = "CARD_PRESENT";
	private static final String AMEX_TRANS_CURRENCY_MXN = "MXN";
	private static final String AMEX_API_OPERATION_AUTHORIZE = "AUTHORIZE";
	private static final String AMEX_GATEWAY_API_PARAM_MIDAS2 = "MIDAS2";
	
	
	protected UsuarioService usuarioService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}


	/**
	 * Consultar saldo y aplicar el cobro a una tarjeta de credito American Exmpress. 
	 * @param pago
	 * @param recibo
	 * @return
	 */

	@Override
	public RespuestaAmexDTO consultarSaldoAplicarCargoTarjetaCredito(ConfiguracionPagoDTO pago, ReciboDTO recibo){
		
		RespuestaAmexDTO  resAmexDto = new RespuestaAmexDTO();

		AmexDTO amexDto = null;
		try {
			amexDto = getAmexDTO(AMEX_GATEWAY_API_PARAM_MIDAS2);
		} catch (Exception e) {
			resAmexDto.setCodigoErrorAmex(AMEX_GATEWAY_ERROR_CODE);
			resAmexDto.setDescripcionErrorAmex(e.getMessage());
		}
		AmexTransactionDTO amexTransaction = getAmexTransactionDTO(pago,recibo);
		resAmexDto = this.sendTransaction(amexDto,amexTransaction,AMEX_API_OPERATION_PAY);
		
		return resAmexDto;
	}




	/**
	* Consultar AmexDTO, parametrización de American Express Gateway API.
	*@return AmexDTO
	*/
	private AmexDTO getAmexDTO(String aplicacion) throws Exception{
		StoredProcedureHelper storedHelper = null;
		
		try {

			LogDeMidasInterfaz.log("Entrando a AmexService.getAmexDTO..." + this, Level.INFO, null);
			
			storedHelper = new StoredProcedureHelper("SEYCOS.PKG_AMEX.GET_AMEX_GAPI_PARAM_2");

			String [] atributosDTO = {
					"gatewayHost", "gatewayUrl", "version", "merchant",
					"apiUsername", "apiPassword", "aplicacion"
			};
			
			String [] columnasResulset = {
					"Gateway_Host", "Gateway_Url", "Version", "Merchant",
					"Api_username", "Api_Password", "Aplicacion"
			};
			
			storedHelper.estableceMapeoResultados(
							AmexDTO.class.getCanonicalName(),
							atributosDTO,
							columnasResulset);
			
			storedHelper.estableceParametro("pAplicacion", aplicacion);
			
			
			
			AmexDTO amexDto =  (AmexDTO)storedHelper.obtieneResultadoSencillo();
			
			LogDeMidasInterfaz.log("Saliendo de AmexService.getAmexDTO..." + this, Level.INFO, null);
			return amexDto;
			
			
		} catch (SQLException e) {
			
			Integer codErr = null;
			String descErr = null;
			
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
			}
			
			StoredProcedureErrorLog.doLog(usuarioService.getUsuarioActual().getNombreCompleto(),
					StoredProcedureErrorLog.TipoAccion.BUSCAR,
					"SEYCOS.PKG_AMEX.GET_AMEX_GAPI_PARAM_2", AmexDTO.class, codErr, descErr);
			LogDeMidasInterfaz.log("Excepcion en BD de AmexService.getAmexDTO..." + this, Level.WARNING, e);
			throw e;
		} catch (Exception e) {
			LogDeMidasInterfaz.log("Excepcion general en AmexService.getAmexDTO..." + this, Level.WARNING, e);
			throw e;
		} 
	}


	private AmexTransactionDTO getAmexTransactionDTO(ConfiguracionPagoDTO pago, ReciboDTO recibo){

		CuentaPagoDTO tarjetaCredito = (CuentaPagoDTO)pago.getInformacionCuenta();

	    	    
		AmexTransactionDTO transactionDto = new AmexTransactionDTO(AMEX_REF_MIDAS2 + pago.getReferenciaPago(),
																	pago.getDescripcionPago(), 
																	recibo.getCantidad().toString(), 
																	AMEX_TRANS_CURRENCY_MXN, 
																	AMEX_REF_MIDAS2 + pago.getReferenciaPago(), 
																	AMEX_TRANS_SRC_CARD_PRESENT, 
																	tarjetaCredito.getCuentaNoEncriptada(), 
																	tarjetaCredito.getFechaVencimiento().substring(0,2), 
																	tarjetaCredito.getFechaVencimiento().substring(2,4), 
																	tarjetaCredito.getCodigoSeguridad());

		return transactionDto;

	}


	private RespuestaAmexDTO sendTransaction(AmexDTO amexDto,AmexTransactionDTO transactionDto, String transaction){

		RespuestaAmexDTO respuestaAmex = null;
		
		ConductoCobroAmex  gateway = new ConductoCobroAmex();
	
		try {
			respuestaAmex = new RespuestaAmexDTO();

			String message = null;
			if (AMEX_API_OPERATION_PAY.equals(transaction)) {
				message = gateway.pay(amexDto,transactionDto);
			}else if(AMEX_API_OPERATION_AUTHORIZE.equals(transaction)){
				message = gateway.authorize(amexDto,transactionDto);
			}
			
			if (AMEX_GATEWAY_CODE_APROBADO.equals(message)) {
	            if (AMEX_GATEWAY_SECURITY_CODE_MATCH.equals(gateway.getResponse().getResponse().getCardSecurityCode().getGatewayCode())) {
	            	respuestaAmex.setAmexNumAutorizacion(gateway.getResponse().getTransaction().getAuthorizationCode());
					respuestaAmex.setReferencia(gateway.getResponse().getTransaction().getReference());
	            }else{
	            	respuestaAmex.setCodigoErrorAmex(AMEX_GATEWAY_ERROR_CODE);
					respuestaAmex.setDescripcionErrorAmex(message + 
					(StringUtil.isEmpty(message) ? "." : ": " + message));
	            }
			}else{
				respuestaAmex.setCodigoErrorAmex(AMEX_GATEWAY_ERROR_CODE);
				respuestaAmex.setDescripcionErrorAmex(message + 
				(StringUtil.isEmpty(message) ? "." : ": " + message));
			}
		}catch(AmexGatewayException e) {
			respuestaAmex.setCodigoErrorAmex(AMEX_GATEWAY_ERROR_CODE);
			respuestaAmex.setDescripcionErrorAmex(e.getMessage() + 
				(StringUtil.isEmpty(e.getMessage()) ? "." : ": " + e.getMessage()));
		}
		return respuestaAmex;	
	}
	
	
	/**
	 * Aplicar un cargo a una tarjeta American Express. devuelve una objeto con los detalles de la transaccion
	 * @param cardNumber
	 * @param securityCode
	 * @param expirationDate
	 * @param reference
	 * @return RespuestaAmexDTO
	 */
	@Override
	public RespuestaAmexDTO aplicarCargo(String cardNumber, String securityCode, String expirationDate, String reference,String amount, String description){
		RespuestaAmexDTO  resAmexDto = new RespuestaAmexDTO();

		AmexDTO amexDto = null;
		try {
			amexDto = getAmexDTO(AMEX_GATEWAY_API_PARAM_MIDAS2);
		} catch (Exception e) {
			resAmexDto.setCodigoErrorAmex(AMEX_GATEWAY_ERROR_CODE);
			resAmexDto.setCodigoErrorAmex(e.getMessage());
			return resAmexDto;
		}
		if (amexDto != null && expirationDate!= null ) {
			AmexTransactionDTO transactionDto = new AmexTransactionDTO(AMEX_REF_MIDAS2 + reference,
					description, 
					amount, 
					AMEX_TRANS_CURRENCY_MXN, 
					AMEX_REF_MIDAS2 + reference, 
					AMEX_TRANS_SRC_CARD_PRESENT, 
					cardNumber, 
					expirationDate.substring(0,2), 
					expirationDate.substring(2,4), 
					securityCode);
			resAmexDto = this.sendTransaction(amexDto,transactionDto,AMEX_API_OPERATION_PAY);
		}
		
		
		return resAmexDto;
	}
}