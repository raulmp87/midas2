package mx.com.afirme.midas2.service.impl.bonos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

import mx.com.afirme.midas2.dao.bonos.ConfigBonoRangoAplicaDao;
import mx.com.afirme.midas2.domain.bonos.ConfigBonoRangoAplica;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.service.bonos.ConfigBonoRangoAplicaService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class ConfigBonoRangoAplicaServiceImpl implements ConfigBonoRangoAplicaService{
		
	@Override
	public List<ConfigBonoRangoAplica> loadByConfigBono(ConfigBonos configBono)throws Exception {
		return dao.loadByConfigBono(configBono);
	}

	@Override
	public ConfigBonos saveConfigBonoExcepAgente(ConfigBonos config, List<ConfigBonoRangoAplica> aQuedar) throws Exception {
				
		Predicate filtro;
		List<ConfigBonoRangoAplica> aBorrar = new ArrayListNullAware<ConfigBonoRangoAplica>();
		List<ConfigBonoRangoAplica> aCrear = new ArrayListNullAware<ConfigBonoRangoAplica>();
		     
		if(!aQuedar.isEmpty())
		{
			dao.validarRangofactores(aQuedar);
		}
		
		
		List<ConfigBonoRangoAplica> actuales = entidadService.findByProperty(ConfigBonoRangoAplica.class,"configBono.id",config.getId());
				
		for (ConfigBonoRangoAplica actual : actuales) {
			
			filtro = getConfigBonoRangoPredicate(actual);
			
			if (!CollectionUtils.exists(aQuedar, filtro)) {
				
				aBorrar.add(actual);
				
			}
			
		}
		 
		for (ConfigBonoRangoAplica elemento : aQuedar) {
			
			filtro = getConfigBonoRangoForUpdatePredicate(elemento);
			
			if (!CollectionUtils.exists(actuales, filtro)) {
				
				elemento.setConfigBono(config);
				aCrear.add(elemento);
				
			}
			
		}
		
		for (ConfigBonoRangoAplica elemento : aCrear) {
			
			entidadService.save(elemento);
			
		}
		
		for (ConfigBonoRangoAplica elemento : aBorrar) {
			
			entidadService.remove(elemento);
			
		}
		
		return config;
	}

	@Override
	public void validarRangofactores(List<ConfigBonoRangoAplica> lista) throws Exception {
		dao.validarRangofactores(lista);
	}
	
	
	private Predicate getConfigBonoRangoPredicate(final ConfigBonoRangoAplica filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				ConfigBonoRangoAplica elemento = (ConfigBonoRangoAplica) arg0;
										
				if (filtro.getId().equals(elemento.getId())) {
					
					return true;
					
				}
				
				return false;
				
			}
		};
		
	}
	
	private Predicate getConfigBonoRangoForUpdatePredicate(final ConfigBonoRangoAplica filtro) {

		return new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				ConfigBonoRangoAplica elemento = (ConfigBonoRangoAplica) arg0;
										
				if ((filtro.getImportePrimaInicial() != null && filtro.getImportePrimaInicial().equals(elemento.getImportePrimaInicial())
								   || (filtro.getImportePrimaInicial()== null && elemento.getImportePrimaInicial() == null))
						&& (filtro.getImportePrimaFinal() != null && filtro.getImportePrimaFinal().equals(elemento.getImportePrimaFinal()) 
								|| (filtro.getImportePrimaFinal()== null && elemento.getImportePrimaFinal()== null))
						&& (filtro.getPctSiniestralidadInicial()!= null && filtro.getPctSiniestralidadInicial().equals(elemento.getPctSiniestralidadInicial()) 
								||(filtro.getPctSiniestralidadInicial()==null && elemento.getPctSiniestralidadInicial() == null))
						&& (filtro.getPctSiniestralidadFinal()!=null && filtro.getPctSiniestralidadFinal().equals(elemento.getPctSiniestralidadFinal()) 
								|| (filtro.getPctSiniestralidadFinal()==null && elemento.getPctSiniestralidadFinal()==null))
						&& ( filtro.getPctCrecimientoInicial() != null && filtro.getPctCrecimientoInicial().equals(elemento.getPctCrecimientoInicial()) 
								|| (filtro.getPctCrecimientoInicial() == null && elemento.getPctCrecimientoInicial() == null))
						&& (filtro.getPctCrecimientoFinal() != null && filtro.getPctCrecimientoFinal().equals(elemento.getPctCrecimientoFinal()) 
								|| (filtro.getPctCrecimientoFinal() == null && elemento.getPctCrecimientoFinal() == null))
						&& (filtro.getPctSupMetaInicial() != null && filtro.getPctSupMetaInicial().equals(elemento.getPctSupMetaInicial()) 
								|| (filtro.getPctSupMetaInicial() == null && elemento.getPctSupMetaInicial() == null))
						&& (filtro.getPctSupMetaFinal() != null && filtro.getPctSupMetaFinal().equals(elemento.getPctSupMetaFinal()) 
								|| (filtro.getPctSupMetaFinal()==null && elemento.getPctSupMetaFinal() == null))
						&& (filtro.getValorAplicacion()!= null && filtro.getValorAplicacion().equals(elemento.getValorAplicacion()) 
								|| (filtro.getValorAplicacion()==null && elemento.getValorAplicacion()== null))
						&& (filtro.getNumPolizasEmitidasInicial() != null && filtro.getNumPolizasEmitidasInicial().equals(elemento.getNumPolizasEmitidasInicial())
								|| (filtro.getNumPolizasEmitidasInicial()==null && elemento.getNumPolizasEmitidasInicial() == null))
						&& (filtro.getNumPolizasEmitidasFinal() != null && filtro.getNumPolizasEmitidasFinal().equals(elemento.getNumPolizasEmitidasFinal()) 
								|| (filtro.getNumPolizasEmitidasFinal()==null && elemento.getNumPolizasEmitidasFinal()== null))
						) {
				
					return true;
					
				}
				
				return false;
				
			}
		};
		
	}
	

	@EJB
	private EntidadService entidadService;

	@EJB
	private ConfigBonoRangoAplicaDao dao;
	
}
