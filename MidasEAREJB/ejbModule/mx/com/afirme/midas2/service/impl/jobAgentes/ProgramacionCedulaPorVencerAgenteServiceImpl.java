package mx.com.afirme.midas2.service.impl.jobAgentes;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.lang.String;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.impl.jobAgentes.ProgramacionCalculoBonosServiceImpl.conceptoEjecucionAutomatica;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCedulaPorVencerAgenteService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.MailServiceSupport;

@Stateless
public class ProgramacionCedulaPorVencerAgenteServiceImpl implements
		ProgramacionCedulaPorVencerAgenteService {
	@EJB
	private AgenteMidasService agenteMidasService;
	@PersistenceContext
	private EntityManager entityManager;
	@EJB
	private MailService mailService;
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProgramacionCedulaPorVencerAgenteServiceImpl.class);

	@Override
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		String queryString = new String(
				" SELECT a.ID, a.fechavencimientocedula FROM MIDAS.vw_persona p INNER JOIN MIDAS.toagente a " +
				" ON a.idpersona = p.idpersona WHERE a.fechavencimientocedula = to_date(':today','dd/MM/yyyy') " +
				" AND p.email IS NOT NULL " +
				" AND a.IDSITUACIONAGENTE = (select sitAgente.ID " +
													   " from MIDAS.toValorCatalogoAgentes sitAgente "+ 
                           							   " where sitAgente.GRUPOCATALOGOAGENTES_ID = (select tcgrupagente.ID "+ 
                           																		" from MIDAS.tcgrupocatalogoagentes tcgrupagente "+ 
                           																		" where descripcion = 'Estatus de Agente (Situacion)')" +
                           								" and sitAgente.VALOR ='AUTORIZADO')");
		Calendar today = Calendar.getInstance();
		if (today.get(Calendar.MONTH) == 11) {
			today.set(Calendar.MONTH, 0);
			today.add(Calendar.YEAR, 1);
			queryString = queryString.replace(":today",Utilerias.cadenaDeFecha(today.getTime()));
		} else {
			today.add(Calendar.MONTH, 1);
			queryString = queryString.replace(":today",Utilerias.cadenaDeFecha(today.getTime()));
		}
		Query query = entityManager.createNativeQuery(queryString);
		@SuppressWarnings("unchecked")
		List<Object> result = query.getResultList();
		if (result != null && !result.isEmpty()) {
			List<TareaProgramada> listTask = makeListTask(result);
			return listTask;
		}
		return null;
	}

	@Override
	public void executeTasks() {
		LOG.info("Ejecutando ProgramacionCedulaPorVencerAgenteService.executeTasks()...");
		List<TareaProgramada> tasks = getTaskToDo(conceptoEjecucionAutomatica.BONOS.getValue());
		if (tasks != null) {
			Agente filtroAgente = new Agente();
			for (TareaProgramada task : tasks) {
				Map<String,Map<String, List<String>>> mapCorreos = agenteMidasService
						.obtenerCorreos(task.getId(),
								GenericMailService.P_CEDULA_VENCIDA,
								GenericMailService.M_DOCUMENTO_POR_VENCER);
				filtroAgente.setId(task.getId());
				List<AgenteView> agente = agenteMidasService
						.findByFilterLightWeight(filtroAgente);
				if (mapCorreos != null && !mapCorreos.isEmpty()) {
					for (Entry<String, Map<String, List<String>>> map : mapCorreos
							.entrySet()) {
						String mensaje = MailServiceSupport
											.mensajeAgenteDocumentoPorVencer(agente
														.get(0).getNombreCompleto());
						
						StringBuilder message = null;
						if (StringUtils.isNotBlank(mensaje)) {
							 message = new StringBuilder(mensaje);
						} else {
							message = new StringBuilder();
						}
						if (mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0) != null && StringUtils.isNotBlank(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0))) {
							message.append(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0));
						}
						mailService.sendMailAgenteNotificacion(
								mapCorreos.get(map.getKey()).get("Para"),
								mapCorreos.get(map.getKey()).get("C.C."),
								mapCorreos.get(map.getKey()).get("C.CO."),
								"CEDULA POR VENCER", message.toString().replace(" null ", " "),
								null, "CEDULA POR VENCER", null,
								GenericMailService.T_GENERAL);
					}
				}
			}
		}
	}

	/**
	 * Crea la lista de Tareas Pramadas a ejecutar
	 * 
	 * @param result
	 * @return
	 */
	private List<TareaProgramada> makeListTask(List<Object> result) {
		List<TareaProgramada> listResultAgente = new LinkedList<TareaProgramada>();
		for (Object item : result) {
			Object[] array = (Object[]) item;
			if (array[1] != null) {
				TareaProgramada tareaProgramada = new TareaProgramada();
				BigDecimal id = (BigDecimal) array[0];
				tareaProgramada.setId(id.longValue());
				tareaProgramada.setFechaEjecucion((Date) array[1]);
				listResultAgente.add(tareaProgramada);
			}
		}
		return listResultAgente;
	}
	
	public void initialize() {
		String timerInfo = "TimerProgramacionDocPorVencer";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 12 * * ?
				expression.minute(0);
				expression.hour(12);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerProgramacionDocPorVencer", false));
				
				LOG.info("Tarea TimerProgramacionDocPorVencer configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerProgramacionDocPorVencer");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerProgramacionDocPorVencer:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		executeTasks();
	}
}
