
package mx.com.afirme.midas2.service.impl.movimientosmanuales;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.movimientosmanuales.MovimientosManualesDao;
import mx.com.afirme.midas2.domain.movimientosmanuales.MovimientosManuales;
import mx.com.afirme.midas2.service.movimientosmanuales.MovimientosManualesService;

@Stateless
public class MovimientosManualesServiceImpl implements MovimientosManualesService{
	private MovimientosManualesDao dao;

	public MovimientosManualesDao getDao() {
		return dao;
	}

	@EJB
	public void setDao(MovimientosManualesDao dao) {
		this.dao = dao;
	}

	@Override
	public List<MovimientosManuales> findByFilters(MovimientosManuales filtro)
			throws Exception {
		return dao.findByFilters(filtro);
	}
	
	@Override
	public int aplicarMovimientosManuales(String[] idsMovimientosManuales, String idUsuarioAplica)
	{
		int movimientosAplicadosExitosamente = 0;
		
		for(String idMovimiento:idsMovimientosManuales)
		{
			try {
				dao.aplicarMovimientoManual(Long.valueOf(idMovimiento), idUsuarioAplica);
				movimientosAplicadosExitosamente ++;
				System.out.println("movimientosAplicadosExitosamente="+movimientosAplicadosExitosamente);
			}catch (Exception e) {				
				e.printStackTrace();
			}
		}
		
		return movimientosAplicadosExitosamente;		
	}
	
	@Override
	public int eliminarMovimientosManuales(String[] idsMovimientosManuales)
	{
		int movimientosEliminadosExitosamente = 0;
		
		for(String idMovimiento:idsMovimientosManuales)
		{
			dao.eliminarMovimientoManual(Long.valueOf(idMovimiento));
			movimientosEliminadosExitosamente ++;
		}
		
		return movimientosEliminadosExitosamente;
	}
	
}
