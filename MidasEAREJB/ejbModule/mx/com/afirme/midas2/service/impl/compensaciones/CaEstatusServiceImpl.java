/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaEstatusDao;
import mx.com.afirme.midas2.domain.compensaciones.CaEstatus;
import mx.com.afirme.midas2.service.compensaciones.CaEstatusService;

import org.apache.log4j.Logger;

@Stateless

public class CaEstatusServiceImpl  implements CaEstatusService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@EJB
	private CaEstatusDao estatuscaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaEstatusServiceImpl.class);
		/**
	 Perform an initial save of a previously unsaved CaEstatus entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaEstatus entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaEstatus entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaEstatus 	::		CaEstatusServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	estatuscaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaEstatus 	::		CaEstatusServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaEstatus 	::		CaEstatusServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaEstatus entity.
	  @param entity CaEstatus entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaEstatus entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaEstatus 	::		CaEstatusServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaEstatus.class, entity.getId());
	        	estatuscaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaEstatus 	::		CaEstatusServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaEstatus 	::		CaEstatusServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaEstatus entity and return it or a copy of it to the sender. 
	 A copy of the CaEstatus entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaEstatus entity to update
	 @return CaEstatus the persisted CaEstatus entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaEstatus update(CaEstatus entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaEstatus 	::		CaEstatusServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaEstatus result = estatuscaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaEstatus 	::		CaEstatusServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaEstatus 	::		CaEstatusServiceImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaEstatus findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaEstatusServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaEstatus instance = estatuscaDao.findById(id);//entityManager.find(CaEstatus.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaEstatusServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaEstatusServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaEstatus entities with a specific property value.  
	 
	  @param propertyName the name of the CaEstatus property to query
	  @param value the property value to match
	  	  @return List<CaEstatus> found by query
	 */
    public List<CaEstatus> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaEstatusServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaEstatus model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);			
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaEstatusServiceImpl	::	findByProperty	::	FIN	::	");
			return estatuscaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaEstatusServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaEstatus> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaEstatus> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaEstatus> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaEstatus> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaEstatus entities.
	  	  @return List<CaEstatus> all CaEstatus entities
	 */
	public List<CaEstatus> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaEstatusServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaEstatus model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaEstatusServiceImpl	::	findAll	::	FIN	::	");
			return estatuscaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaEstatusServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}