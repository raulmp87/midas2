package mx.com.afirme.midas2.service.impl.cobranza;

import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.cobranza.NotificaFacturaReciboDAO;
import mx.com.afirme.midas2.domain.cobranza.FiltroRecibos;
import mx.com.afirme.midas2.domain.cobranza.Recibos;
import mx.com.afirme.midas2.service.cobranza.NotificaFacturaReciboService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;
import mx.com.afirme.midas2.util.MailService;
import net.sf.jasperreports.engine.JRException;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;

@Stateless
public class NotificaFacturaReciboServiceImpl implements NotificaFacturaReciboService {

	@EJB
	private NotificaFacturaReciboDAO notificaFacturaReciboDao;

	@EJB
	private MailService mailService;

	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private NotificaFacturaReciboService notificaFacturaRecibo;	
	
	private static Logger log = Logger.getLogger("NotificaFacturaReciboServiceImpl");
	
	@EJB
	private ImpresionEndosoService impresionEndosoService;
	
	@EJB
	private ImpresionRecibosService impresionRecibosService;
	
	@Resource	
	private TimerService timerService;
		
	//Notificacion Masiva de Forma Automatica desde Job del Quartz
	//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	
	public void jobEnviaFacturaRecibo(){
		
		FiltroRecibos filtro = new FiltroRecibos();
		
		filtro.setPoliza    ("0");
		filtro.setAsegurado (0);
		filtro.setAgente    (0);
		filtro.setFiltro    (1);
		
		
	
		
		log.info( "Inicio del Proceso de Notificacion - Job"+ new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format( new Date()));
		notificaFacturaRecibo.guardalognotif("Inicio del Proceso de Notificacion - Job");
	
		procesaEnvioFacturaRecibo( filtro);	
		
		
		log.info( "Fin del Proceso de Notificacion - Job"+ new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format( new Date()));
		notificaFacturaRecibo.guardalognotif("Fin del Proceso de Notificacion - Job");
		
	}
	
	//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public String guardalognotif(String descripcion) {		
		return notificaFacturaReciboDao.registrarproceso(descripcion);
	}
	
	
	
	//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	//Generico para modo manual y por job
	public void procesaEnvioFacturaRecibo(FiltroRecibos filtro) {
		// TODO Apéndice de método generado automáticamente
		
		List <Recibos> listaRecibos = notificaFacturaReciboDao.buscarRecibos(filtro);		
		log.info( "Generacion de la Lista de Recibos");

		for (Recibos reciboaenviar : listaRecibos) {
			enviaFacturaRecibo (reciboaenviar);
		}		
	}
	
	//@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void enviaFacturaRecibo (Recibos reciboaenviar){
		try {
			notificarFacturaRecibo(reciboaenviar);
		} catch (JRException e) {
			// TODO Bloque catch generado automáticamente
			e.printStackTrace();
		}
	}
	
	//de printreport
	private PrintReport initPrintReportService(){				
		
		try{
			PrintReportServiceLocator locator = new PrintReportServiceLocator();			
			
			String endpoint = sistemaContext.getPrintReportEndpoint();
			
			return locator.getPrintReport(new URL(endpoint));
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
	}

 
	@Override
	public synchronized void notificarFacturaRecibo(Recibos reciboaenviar)
			throws JRException {
				
		
		
		String nombreArchivo = "";
		String mensaje = "";
		String correo = "";
		Integer x;
		//contenedor del byte
		byte[] reciboFiscal   = null;
		
		//Lista de Archivos para el atach. Será uno solo 
		List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
		
		//Byte del pdf 
		ByteArrayAttachment file = new ByteArrayAttachment();
		//Byte de xml
		ByteArrayAttachment file2 = new ByteArrayAttachment();
		
		//Lista de Correos
		List<String> address = null;
		address = new ArrayList<String>();
		
		//*********************************************************************************
		//*********************************************************************************
		//Con el metodo getFileFromDisk
		//inicio
		
		//----------------obtiene el cuerpo del correo			
		//mensaje = getmensajeFacturaRecibo();			
		//mensaje = replacemensajeFacturaRecibo(reciboaenviar,mensaje);
		
		mensaje = mailService.getmensajeFacturaRecibos(reciboaenviar);
		
		//---------------Correos destinatarios
		correo = reciboaenviar.getEmail();			
		correo = correo.replaceAll(";","," );		
		if (StringUtils.isNotBlank(correo)) {
			address.add(correo);
		} else {
			address.add("cobranza@afirme.com");
		}
		
		//----------------------Crea pdf en base al path
		PrintReport pr = this.initPrintReportService();	
		
		for ( x = 0; x < 2; x++){
					
				try {
					log.info( "Archivo["+ x +"]");
					if (x==0) {
						reciboFiscal = impresionRecibosService.getReciboFiscal(reciboaenviar.getLlaveComprobante(), "pdf");
					}
					if (x==1) {
						reciboFiscal = impresionRecibosService.getReciboFiscal(reciboaenviar.getLlaveComprobante(), "xml");
					}
					
				} catch (Exception e1) {		
					log.error("Error al consumir Servicio getFileFromDisk");
					log.error(e1);
				}		
				
				//------prepara el envio simpre y cuando se haya extraido el pdf y xml correctamente -------------------
				if (reciboFiscal!= null && reciboFiscal.length>0) {
					
						//-----INI------------------ARCHIVO ADJUNTO
						//--------- incluyendo archivo para el atach			
						
						if (x==0) { 
							nombreArchivo = "Factura.pdf";
							file.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
							file.setNombreArchivo(nombreArchivo);
							log.info( "Preparando PDF");
							//------------------------Preparando archivo para el atach
								file.setContenidoArchivo( reciboFiscal);								
							//-------------------------asignando archivo Adjunto
								attachment.add(file); //.add(file);
							
						}
						if (x==1) { 
							nombreArchivo = "Factura.xml";
						  	file2.setTipoArchivo(ByteArrayAttachment.TipoArchivo.DESCONOCIDO );
						  	file2.setNombreArchivo(nombreArchivo);
						  	log.info( "Preparando XML");
							//------------------------Preparando archivo para el atach
								file2.setContenidoArchivo( reciboFiscal);
							//-------------------------asignando archivo Adjunto
								attachment.add( file2); 
						} 
						//---FIN-------------------ARCHIVO ADJUNTO 			
				}else{
					log.info( "No se Extrajo nigun archivo");					
				}
				//----------------------Crea pdf en base al path
		}
		
		
		
		//-----------------------------envio del Correo
		if (file!= null || file2!=null) {
			try{
				mailService.sendMailAgenteNotificacion(address, null, null,"Notificacion Afirme Seguros",
														mensaje, attachment, "Notificacion Afirme Seguros", null,-1);
			}catch (Exception e) {				
				log.warn("Error al Enviar el mensaje de  la llave :"+reciboaenviar.getLlaveComprobante());
			}	
		} 		
		//----------------------------
		
		
		//con el metodo getFileFromDisk
		//fin
		//*****************************************************************************************

		
  		//reinicio de valriables
		nombreArchivo 	= "";		 
		mensaje 		= "";		 
		correo		 	= "";		 
		x				= 0 ;
		//contenedor del byte
		reciboFiscal   = null;		
		//Lista de Archivos para el atach. Será uno solo 
		attachment.clear();		
		//Byte del pdf 
		file=null;		 
		//Byte de xml
		file2 = null;		
		//Lista de Correos
		address.clear();
		
	}	 

	public String getmensajeFacturaRecibo()
	{		
		return null;
	}

	public String replacemensajeFacturaRecibo(Recibos reciboaenviar, String elmensaje)
	{		
		return null;		
	}
	
	public void initialize() {
		String timerInfo = "TimerNotificaFacturaRecibo";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 19 * * ?
				expression.minute(0);
				expression.hour(19);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerNotificaFacturaRecibo", false));
				
				log.info("Tarea TimerNotificaFacturaRecibo configurado");
			} catch (Exception e) {
				log.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		log.info("Cancelar Tarea TimerNotificaFacturaRecibo");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			log.error("Error al detener TimerNotificaFacturaRecibo:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		jobEnviaFacturaRecibo();
	}
}
