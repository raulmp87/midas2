package mx.com.afirme.midas2.service.impl.componente;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.catalogos.giro.SubGiroFacadeRemote;
import mx.com.afirme.midas.interfaz.agente.AgenteFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas2.dao.catalogos.GrupoVariablesModificacionPrimaDao;
import mx.com.afirme.midas2.domain.catalogos.VarModifPrima;
import mx.com.afirme.midas2.domain.tarifa.TarifaConcepto;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersion;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.poliza.VistaConsultaPolizasDanios;
import mx.com.afirme.midas2.dto.poliza.VistaconsultaPolizasAutos;
import mx.com.afirme.midas2.service.catalogos.TarifaAutoModifPrimaService;
import mx.com.afirme.midas2.service.catalogos.VarModifDescripcionService;
import mx.com.afirme.midas2.service.catalogos.VarModifPrimaService;
import mx.com.afirme.midas2.service.componente.ComponenteService;
import mx.com.afirme.midas2.service.poliza.PolizaService;
import mx.com.afirme.midas2.service.tarifa.TarifaAutoModifDescService;
import mx.com.afirme.midas2.service.tarifa.TarifaAutoProliberService;
import mx.com.afirme.midas2.service.tarifa.TarifaAutoRangoSAService;
import mx.com.afirme.midas2.service.tarifa.TarifaAutoService;
import mx.com.afirme.midas2.service.tarifa.TarifaService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.utils.Convertidor;

import org.apache.commons.lang.StringUtils;

@Stateless

public class ComponenteServiceImpl implements ComponenteService {
	
	TarifaAutoModifPrimaService tarifaAutoModifPrimaService;
	
	TarifaAutoService tarifaAutoService;
	
	TarifaAutoModifDescService tarifaAutoModifDescService;
	
	TarifaAutoProliberService tarifaAutoProliberService;
	
	TarifaAutoRangoSAService tarifaAutoRangoSAService;
	
	VarModifPrimaService varModifPrimaService;
	
	PolizaService polizaService;
	
	AgenteFacadeRemote agenteFacade;
	
	@PersistenceContext
	EntityManager entityManager;
	
	/**
	 * Genera un listado de registros dinamicos a partir de un TarifaVersion Id.
	 * @param clave Para el modulo de tarifas se recibe como 	T_idLineaNegocio_idMoneda_idRiesgo_idConcepto_version_[camposRestantesDelID]
	 * @param claveVista Clave que corresponde a una vista que se quiere recuperar
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<RegistroDinamicoDTO> getListado(String clave, String claveVista) {
		
		String [] arrayClave = validarClave(clave);
		
		List<RegistroDinamicoDTO> listadoRegistros = null;
				
		Class<?> vista = getVista(claveVista);
		
		//deducir primero el tipo de registro que solicita el cliente
		if(arrayClave[0].equals(RegistroDinamicoDTO.TARIFA)){
			//el tipo de registro es Tarifa, verificar qué tipo de tarifa requiere el cliente
			
			TarifaVersion tarifaVersion = consultaTarifaVersion(arrayClave);
			
			@SuppressWarnings("rawtypes")
			TarifaService service = getTarifaServiceCorrespondiente(tarifaVersion);
			listadoRegistros = service.getRegistrosDinamicosPorTarifaVersionId(tarifaVersion.getId(), vista);
			
		}else if(RegistroDinamicoDTO.VARIABLES_MODIFICADORAS_PRIMA.equals(arrayClave[0])){
			//Aplica para catalogo de Variables Modificadoras de Prima
			listadoRegistros=varModifPrimaService.getRegistrosDinamicos();
		}
		else{
			throw new RuntimeException("Tipo de catalogo no definido: "+clave);
		}
		
		return listadoRegistros;
	}
	
	public List<RegistroDinamicoDTO> getListado(String clave, String claveVista,Object filtro) {
		
		String [] arrayClave = validarClave(clave);
		
		List<RegistroDinamicoDTO> listadoRegistros = null;
				
		Class<?> vista = getVista(claveVista);
		
		//deducir primero el tipo de registro que solicita el cliente
		if(arrayClave[0].equals(RegistroDinamicoDTO.TARIFA) || 
				RegistroDinamicoDTO.VARIABLES_MODIFICADORAS_PRIMA.equals(arrayClave[0])){
			//Para estos catalogos no hay consulta por filtro
			listadoRegistros = getListado(clave, claveVista);
		}else if(RegistroDinamicoDTO.POLIZA_DANIO.equals(arrayClave[0]) || 
				RegistroDinamicoDTO.POLIZA_AUTO.equals(arrayClave[0])){
			listadoRegistros = polizaService.getRegistrosDinamicos(filtro);
		}
		else{
			throw new RuntimeException("Tipo de catalogo no definido: "+clave);
		}
		
		return listadoRegistros;
	}
	
	/**
	 * En base a la accion recibida, genera un nuevo registro dinamico del tipo especifico
	 * establecido en el parametro "clave"  y lo pobla con los datos de su objeto representado.
	 * @param clave Para el modulo de tarifas se recibe como 	T_idLineaNegocio_idMoneda_idRiesgo_idConcepto_version_[camposRestantesDelID]
	 * @param claveVista Clave que corresponde a una vista que se quiere recuperar
	 * @param accion Especifica la operacion a realizar:
	 * 				"agregar"
	 * 				"editar"
	 * 				"borrar"
	 * @param objeto Objeto cuyos valores se poblaran en el registro dinamico
	 * @return
	 */
	public RegistroDinamicoDTO getRegistroParaValidacion(String clave, String claveVista, String accion, Object objeto) {
		
		String [] arrayClave = validarClave(clave);
		//Se obtiene el registro dinamico original
		RegistroDinamicoDTO registro =  getRegistro(clave, claveVista, accion);
		//Conserva los atributos del objeto a validar si es clave T(Tarifas) y si no es la opcion Agregar
		//if(arrayClave[0].equals("T") && !TipoAccionDTO.getAgregarModificar().equals(accion)){
		if(arrayClave[0].equals(RegistroDinamicoDTO.TARIFA)){
			for (ControlDinamicoDTO control : registro.getControles()) {
				//TODO a partir del atributoMapeo del control obtener un metodo getter del objeto y setear su valor al control
				//Utilizar ConvertidorImpl (debe de poder navegar en objetos anidados)
				Object value = convertidor.getValorEspecifico(control.getAtributoMapeo(), objeto);
				control.setValor((value != null?value.toString():null));
				
				//Encuentra los valores del combo
				if(control.getTipoControl() == ControlDinamicoDTO.SELECT){
					for (ControlDinamicoDTO control2 : registro.getControles()) {
						if(control2.getValor() != null && 
								(control2.getJavaScriptOnChange() != null && control2.getJavaScriptOnChange().contains(control.getAtributoMapeo()))){
							String[] totalCascade = control2.getJavaScriptOnChange().split("cascadeoGenerico");
							for(int i = 0; i < totalCascade.length; i++){
								if(totalCascade[i].contains(control.getAtributoMapeo())){
									String[] param = totalCascade[i].split(",");
									String tipoBusqueda = param[1].replaceAll("'", "") + "_" + control2.getValor();
									control.setElementosCombo(getListadoRegistros(tipoBusqueda));
									break;
								}
							}
							break;
						}
					}
				}
			}
		}
		
		return registro;
	}
	
	
	/**
	 * En base a la accion recibida, genera un nuevo registro dinamico del tipo especifico
	 * establecido en el parametro "clave".
	 * @param clave Para el modulo de tarifas se recibe como 	T_idLineaNegocio_idMoneda_idRiesgo_idConcepto_version_[camposRestantesDelID]
	 * @param claveVista Clave que corresponde a una vista que se quiere recuperar
	 * @param accion Especifica la operacion a realizar:
	 * 				"agregar"
	 * 				"editar"
	 * 				"borrar"
	 * @return
	 */
	@Override
	public RegistroDinamicoDTO getRegistro(String clave, String claveVista, String accion) {
		String [] arrayClave = validarClave(clave);
		
		RegistroDinamicoDTO registroDinamico = null;
		
		Class<?> vista = getVista(claveVista);
		
		if(arrayClave[0].equals(RegistroDinamicoDTO.TARIFA)){
			
			TarifaVersion tarifaVersion = consultaTarifaVersion(arrayClave);
			
			@SuppressWarnings("rawtypes")
			TarifaService service = getTarifaServiceCorrespondiente(tarifaVersion);
			registroDinamico = service.verDetalle(tarifaVersion.getId(), clave, accion, vista);
			
			//Aplica para catalogo de Variables Modificadoras de Prima
		}else if(RegistroDinamicoDTO.VARIABLES_MODIFICADORAS_PRIMA.equals(arrayClave[0])){
			registroDinamico=varModifPrimaService.verDetalle(clave, accion,vista);
		}
		else if (RegistroDinamicoDTO.POLIZA_AUTO.equals(arrayClave[0])){
			//El registro dinamico de póliza sólo se usa para la funcionalidad de listar filtrado
			vista = VistaconsultaPolizasAutos.class;
			registroDinamico = polizaService.verDetalle(clave, accion,vista);
		}
		else if (RegistroDinamicoDTO.POLIZA_DANIO.equals(arrayClave[0])){
			//El registro dinamico de póliza sólo se usa para la funcionalidad de listar filtrado
			vista = VistaConsultaPolizasDanios.class;
			registroDinamico = polizaService.verDetalle(clave, accion,vista);
		}
		else{
			throw new RuntimeException("Tipo de catalogo no definido: "+clave);
		}
		
		return registroDinamico;
	}
	
	/**
	 * Consulta el registro en BD de acuerdo a la clave recibida.
	 * Deduce si se intenta agregar un nuevo registro en vase a la consitución de la clave y regresa
	 * ya sea un nuevo objeto o un objeto existente en BD del tipo específico declarado como Object.
	 * @param clave. Clave del catálogo que se está procesando
	 * @return Object objeto del tipo específico de acuerdo a la clave recibida.
	 */
	@Override
	public Object getRegistroEspecifico(String clave) {
		String [] arrayClave = validarClave(clave);
		
		Object registroEspecifico = null;
		
		if(arrayClave[0].equals(RegistroDinamicoDTO.TARIFA)){
			
			TarifaVersion tarifaVersion = consultaTarifaVersion(arrayClave);
			
			@SuppressWarnings("rawtypes")
			TarifaService service = getTarifaServiceCorrespondiente(tarifaVersion);
			registroEspecifico = service.getNewObject();
			
		}else if(RegistroDinamicoDTO.VARIABLES_MODIFICADORAS_PRIMA.equals(arrayClave[0])){
			//Aplica para catalogo de Variables Modificadoras de Prima
			registroEspecifico=new VarModifPrima();
		}
		else if(RegistroDinamicoDTO.POLIZA_AUTO.equals(arrayClave[0]) || RegistroDinamicoDTO.POLIZA_DANIO.equals(arrayClave[0])){
			registroEspecifico = new PolizaDTO();
		}
		else{
			throw new RuntimeException("Tipo de catalogo no definido: "+clave);
		}
		
		return registroEspecifico;
	}
	
	
	@Override
	public void actualizarRegistro(Object registro, String accion) {
		if(accion == null || registro == null) {
			throw new IllegalArgumentException("Parametros no validos: accion= "+accion+", registro = "+registro);
		}
		
		//Esta accion es solo para agregar
		if(accion.equals(TipoAccionDTO.getAgregarModificar())){
			entityManager.persist(registro);
		}
		else if(accion.equals(TipoAccionDTO.getEditar())){
			entityManager.merge(registro);
		}
		else if(accion.equals(TipoAccionDTO.getEliminar())){
			registro = entityManager.merge(registro);
			entityManager.remove(registro);
		}
		else{
			throw new IllegalArgumentException("Parametros no validos: accion= "+accion);
		}
	}

	/**
	 * Consulta el listado de registros dependientes de un registro cuyo id se recibe en la clave.
	 * @param clave. La clave contiene el tipo de catálogos a consultar y el identificador del registro padre.
	 * @return
	 */
	@Override
	public LinkedHashMap<String, String> getMapaRegistros(String clave){
		
		String [] arrayClave = validarClave(clave);
		
		List<CacheableDTO> listaCacheables = getListaCacheablePorClave(arrayClave);
		
		return getMapFromList(listaCacheables);
	}
	
	/**
	 * Consulta el listado de registros dependientes de un registro cuyo id se recibe en la clave.
	 * @param clave. La clave contiene el tipo de catálogos a consultar y el identificador del registro padre.
	 * @return
	 */
	@Override
	public List<CacheableDTO> getListadoRegistros(String clave){
		
		String [] arrayClave = validarClave(clave);
		
		return getListaCacheablePorClave(arrayClave);
	}
	
	@SuppressWarnings({ "unchecked", "rawtypes" })
	private List<CacheableDTO> getListaCacheablePorClave(String [] arrayClave ) {
		
		List<CacheableDTO> listaResultado = new ArrayList<CacheableDTO>(1);
		
		try{
		
			ServiceLocatorP serviceLocator = ServiceLocatorP.getInstance();
			
		if(arrayClave[0].equals("SG")){
			if(arrayClave != null && arrayClave.length > 1){
			BigDecimal idGiro = new BigDecimal(arrayClave[1]);
			
			SubGiroFacadeRemote facade = serviceLocator.getEJB(SubGiroFacadeRemote.class);
			listaResultado = (List)facade.findByProperty("giroDTO.idTcGiro", idGiro);
			}
			
		}
		else if(arrayClave[0].equals("GPO-VAR-MOD-PRIMA")){
			GrupoVariablesModificacionPrimaDao dao = serviceLocator.getEJB(GrupoVariablesModificacionPrimaDao.class);
			
			listaResultado = (List)dao.findAll();
		}
		else if(arrayClave[0].equals("SEC-GPO-VAR-MOD-PRIMA")){
			if(arrayClave != null && arrayClave.length > 1){
			Long idGrupo = new Long(arrayClave[1]);
			
			VarModifPrimaService service = serviceLocator.getEJB(VarModifPrimaService.class);
			
			listaResultado = (List) service.getListaVarModifPrimaComboDTO(idGrupo);
			}
		}
		else if(arrayClave[0].equals("GPO-VAR-MOD-DESC")){
			VarModifDescripcionService service = serviceLocator.getEJB(VarModifDescripcionService.class);
			
			listaResultado = (List)service.listarComboGrupos();
		}
		else if(arrayClave[0].equals("SEC-GPO-VAR-MOD-DESC")){
			if(arrayClave != null && arrayClave.length > 1){
			Integer idGrupo = new Integer(arrayClave[1]);
			
			VarModifDescripcionService service = serviceLocator.getEJB(VarModifDescripcionService.class);
			
			listaResultado = (List) service.findByGroup(idGrupo);
			}
		}
		else if(arrayClave[0].equals("USO-TIPO-VEH-PROL")){
			if(arrayClave != null && arrayClave.length > 1){
			BigDecimal idTcTipoVehiculo = new BigDecimal(arrayClave[1]);
			
			TarifaAutoProliberService service = serviceLocator.getEJB(TarifaAutoProliberService.class);
			
			listaResultado = (List) service.getListaTipoUsoVehiculoComboDTO(idTcTipoVehiculo);
			}
		}
		else if(arrayClave[0].equals("SERV-TIPO-VEH-PROL")){
			if(arrayClave != null && arrayClave.length > 1){
			BigDecimal idTcTipoVehiculo = new BigDecimal(arrayClave[1]);
			
			TarifaAutoProliberService service = serviceLocator.getEJB(TarifaAutoProliberService.class);
			
			listaResultado = (List) service.getListaTipoServicioVehiculoComboDTO(idTcTipoVehiculo);
			}
		}
		else if(arrayClave[0].equals("AGENTES")){
			listaResultado = (List)agenteFacade.listarAgentes("", null);
		}
		else{
			throw new RuntimeException("Clave no v\u00e1lida, no se encontr\u00f3 tipo de cat\u00e1logo: "+arrayClave);
		}
		
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
		return listaResultado;
	}
	
	private LinkedHashMap<String, String> getMapFromList(List<CacheableDTO> listaCacheables){
		LinkedHashMap<String, String> mapaValores = new LinkedHashMap<String, String>();
		
		for(CacheableDTO cacheable : listaCacheables){
			mapaValores.put(cacheable.getId().toString(), cacheable.getDescription());
		}
		
		return mapaValores;
	}
	
	private String[] validarClave(String clave){
		if(clave == null || clave.trim().equals("") ){
			return null;
		}
		
		String [] arrayClave = clave.split(UtileriasWeb.SEPARADOR);
		
		return arrayClave;
	}
	
	@SuppressWarnings("rawtypes")
	private TarifaService getTarifaServiceCorrespondiente(TarifaVersion tarifaVersion){
		
		TarifaService service = null;
		
		switch(tarifaVersion.getTarifaConcepto().getTipoTarifa()){
			case TarifaConcepto.TARIFA_ESTANDAR:
				service = tarifaAutoService;
				break;
			case TarifaConcepto.TARIFA_POR_RANGO_SA:
				service = tarifaAutoRangoSAService;
				break;
			case TarifaConcepto.TARIFA_POR_SERVICIOS_TERCEROS:
				service = tarifaAutoProliberService;
				break;
			case TarifaConcepto.TARIFA_POR_PRIMA:
				service = tarifaAutoModifPrimaService;
				break;
			case TarifaConcepto.TARIFA_POR_DESCRIPCION:
				service = tarifaAutoModifDescService;
				break;
			default:
				throw new RuntimeException("Tipo de tarifa no definido.");
		}
		
		return service;
	}
	
	private TarifaVersion consultaTarifaVersion(String[] arrayClave){
		TarifaVersionId tfId = new TarifaVersionId();
		
		Long parteDelId = new Long(arrayClave[1]);
		tfId.setIdMoneda(parteDelId);
		
		parteDelId = new Long(arrayClave[2]);
		tfId.setIdRiesgo(parteDelId);
		
		parteDelId = new Long(arrayClave[3]);
		tfId.setIdConcepto(parteDelId);
		
		parteDelId = new Long(arrayClave[4]);
		tfId.setVersion(parteDelId);
		
		return entityManager.find(TarifaVersion.class, tfId);
	}
		
	private Class<?> getVista (String claveVista) {
		//TODO : Despues implementar que la busqueda de la vista se haga median Acceso a Datos de BD
		try {
			
			if (!StringUtils.isBlank(claveVista)) {
				return Class.forName(vistas.get(claveVista));
			}
			
			return null; 
			
		} catch (ClassNotFoundException e) {
			throw new RuntimeException("Vista no definida: " + claveVista);
		}
		
	}
	
	
	private static final HashMap<String, String> vistas;
	static {
		vistas = new HashMap<String, String>();
		
		vistas.put("VistaTarifaVarModificadoraPrima", "mx.com.afirme.midas2.dto.componente.vista.VistaTarifaVarModificadoraPrima");
	}
	
	private Convertidor convertidor;
	
	@EJB
	public void setConvertidor(Convertidor convertidor) {
		this.convertidor = convertidor;
	}
	
	@EJB
	public void setPolizaService(PolizaService polizaService) {
		this.polizaService= polizaService;
	}

	@EJB
	public void setVarModifPrimaService(VarModifPrimaService varModifPrimaService) {
		this.varModifPrimaService= varModifPrimaService;
	}
	
	@EJB
	public void setTarifaAutoRangoSAService(TarifaAutoRangoSAService tarifaAutoRangoSAService) {
		this.tarifaAutoRangoSAService= tarifaAutoRangoSAService;
	}
	
	@EJB
	public void setTarifaAutoProliberService(TarifaAutoProliberService tarifaAutoProliberService) {
		this.tarifaAutoProliberService= tarifaAutoProliberService;
	}
	
	@EJB
	public void setTarifaAutoModifDescService(TarifaAutoModifDescService tarifaAutoModifDescService) {
		this.tarifaAutoModifDescService= tarifaAutoModifDescService;
	}
	
	@EJB
	public void setTarifaAutoService(TarifaAutoService tarifaAutoService) {
		this.tarifaAutoService= tarifaAutoService;
	}
	
	@EJB
	public void setTarifaAutoModifPrimaService(TarifaAutoModifPrimaService tarifaAutoModifPrimaService) {
		this.tarifaAutoModifPrimaService= tarifaAutoModifPrimaService;
	};

	@EJB
	public void setAgenteFacadeRemote(AgenteFacadeRemote agenteFacade) {
		this.agenteFacade= agenteFacade;
	};
}
