package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.GerenciaJPADao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.dto.fuerzaventa.GerenciaView;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaJPAService;
@Stateless
//@TransactionManagement(TransactionManagementType.CONTAINER)
public class GerenciaJPAServiceImpl implements GerenciaJPAService{
	private GerenciaJPADao gerenciaDao;

	@Override
	public Gerencia saveFull(Gerencia gerencia) throws Exception {
		return gerenciaDao.saveFull(gerencia);
	}
	
	@EJB
	public void setGerenciaDao(GerenciaJPADao gerenciaDao){
		this.gerenciaDao=gerenciaDao;
	}

	@Override
	public void unsubscribe(Gerencia arg0) throws Exception {
		gerenciaDao.unsubscribe(arg0);
	}

	@Override
	public List<Gerencia> findByFilters(Gerencia arg0) {
		return gerenciaDao.findByFilters(arg0, null);
	}

	@Override
	public List<Gerencia> findByCentroOperacion(Long arg0) {
		return gerenciaDao.findByCentroOperacion(arg0);
	}
	
	@Override
	public Gerencia loadById(Gerencia entidad){
		return gerenciaDao.loadById(entidad, null);
	}
	
	@Override
	public List<GerenciaView> findByFiltersView(Gerencia arg0) {
		return gerenciaDao.findByFiltersView(arg0);
	}
	
	@Override
	public List<GerenciaView> getList(boolean onlyActive){
		return gerenciaDao.getList(onlyActive);
	}
	@Override
	public List<GerenciaView> findByCentroOperacionLightWeight(Long idParent) {
		return gerenciaDao.findByCentroOperacionLightWeight(idParent);
	}

	@Override
	public List<GerenciaView> findGerenciasConCentroOperacionExcluyentes(List<Long> gerencias,List<Long> centrosOperacionExcluyentes){
		return gerenciaDao.findGerenciasConCentroOperacionExcluyentes(gerencias, centrosOperacionExcluyentes);
	}
	
	@Override
	public Gerencia loadById(Gerencia gerencia, String fechaHistorico)
	{
		return gerenciaDao.loadById(gerencia, fechaHistorico);
		
	}
}
