/**
 * Clase que llena Paginas y Permisos para el rol de Suscriptor de Orden de Trabajo
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.suscriptorordentrabajo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 * 
 */
public class PaginaPermisoSuscriptorOrdenTrabajo {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();

	public PaginaPermisoSuscriptorOrdenTrabajo(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}

	private Pagina nuevaPagina(String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(),
				nombreAccionDo.trim(), "Descripcion pagina");
	}

	public List<PaginaPermiso> obtienePaginaPermisos() {

		// Permisos 0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 =
		// AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE, 15=TE ,16=RC,
		// 17=EM ,18=IM, 19=ET

		PaginaPermiso pp;

		// SUSCRIPTOR ORDEN DE TRABAJO

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/cobertura/mostrarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/endoso/cobertura/mostrarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("coberturas.jsp",
				"/MidasWeb/cotizacion/cobertura/listarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("coberturas.jsp",
				"/MidasWeb/cotizacion/endoso/cobertura/listarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/getValoresCoaseguroDeducible.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("void.jsp", "/MidasWeb/cotizacion/void.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("secciones.jsp",
				"/MidasWeb/cotizacion/mostrarModificarSecciones.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("seccionesPorInciso.jsp",
						"/MidasWeb/cotizacion/cotizacion/mostrarSeccionesPorIncisoCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("secciones.jsp",
				"/MidasWeb/cotizacion/endoso/mostrarModificarSecciones.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("seccionesPorInciso.jsp",
						"/MidasWeb/cotizacion/endoso/mostrarSeccionesPorIncisoCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("CotizacionArea.jsp",
				"/MidasWeb/cotizacion/cotizacion/mostrarCot.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("cotizacionArea.jsp",
				"/MidasWeb/cotizacion/cotizacion/mostrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarCotizaciones.jsp",
				"/MidasWeb/cotizacion/cotizacion/listarCotizaciones.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("listarCotizaciones.jsp",
						"/MidasWeb/cotizacion/cotizacion/listarCotizacionesFiltrado.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarCotizaciones.jsp","/MidasWeb/cotizacion/cotizacion/listarFiltradoPaginado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("cancelarCotizacion.jsp",
						"/MidasWeb/cotizacion/cotizacion/mostrarCancelarCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/cotizacion/cancelarCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("editarCotizacion.jsp",
				"/MidasWeb/cotizacion/editarCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/cotizacion/guardarCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("verArbol.jsp",
				"/MidasWeb/cotizacion/poblarTreeView.do"));
		listaPaginaPermiso.add(pp);		
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("verArbol.jsp", "/MidasWeb/cotizacion/cargarArbolCotizacion.do"));
		listaPaginaPermiso.add(pp);				

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/cotizacion/copiarCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/inciso/separarInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("cotizacionArea.jsp",
				"/MidasWeb/cotizacion/mostrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("cotizacion.jsp",
				"/MidasWeb/cotizacion/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("seccionesPorInciso.jsp",
				"/MidasWeb/cotizacion/mostrarSeccionesPorInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosGenerales.jsp",
				"/MidasWeb/cotizacion/mostrarDatosGenerales.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosGenerales.jsp",
				"/MidasWeb/cotizacion/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/cotizacion/inciso/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/cotizacion/inciso/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/inciso/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/cotizacion/inciso/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/cotizacion/inciso/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/cotizacion/inciso/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/inciso/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/cotizacion/inciso/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/cotizacion/inciso/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/cotizacion/inciso/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/inciso/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/cotizacion/inciso/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/cotizacion/inciso/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/cotizacion/inciso/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/inciso/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/cotizacion/inciso/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ubicacion.jsp",
				"/MidasWeb/cotizacion/inciso/mostrarUbicacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/inciso/mostrarDatosInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/cotizacion/inciso/copiar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("copiar.jsp",
				"/MidasWeb/cotizacion/inciso/copiar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/cotizacion/inciso/copiar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/inciso/copiar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/inciso/calcularRiesgosHidro.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ordenTrabajoArea.jsp",
				"/MidasWeb/cotizacion/mostrarODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ordenTrabajoArea.jsp",
				"/MidasWeb/cotizacion/autorizacion/mostrarODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosOrdenTrabajo.jsp",
				"/MidasWeb/cotizacion/mostrarDatosODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosOrdenTrabajo.jsp",
				"/MidasWeb/cotizacion/mostrarODTCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ordenTrabajo.jsp",
				"/MidasWeb/cotizacion/mostrarODTDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenes.jsp",
				"/MidasWeb/cotizacion/listarOrdenesTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenes.jsp",
				"/MidasWeb/cotizacion/listarOrdenesTrabajoFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenes.jsp", "/MidasWeb/cotizacion/ordenTrabajo/listarFiltradoPaginado.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("cancelarOrdenTrabajo.jsp",
				"/MidasWeb/cotizacion/mostrarCancelarOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/cancelarOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("editarOrdenTrabajo.jsp",
				"/MidasWeb/cotizacion/editarOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/terminarOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPersona.jsp",
				"/MidasWeb/cotizacion/mostrarPersona.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarPersona.jsp",
				"/MidasWeb/cotizacion/mostrarAgregarPersona.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("buscarPersona.jsp",
				"/MidasWeb/cotizacion/mostrarBuscarPersona.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cotizacion/agregarPersona.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cotizacion/asignarCliente.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosOrdenTrabajo.jsp",
				"/MidasWeb/cotizacion/guardarOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/autorizacion/autorizarOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/autorizacion/rechazarOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/guardarCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("verArbol.jsp",
				"/MidasWeb/cotizacion/cargarArbolOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/cargarSeccionesPorInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/cotizacion/modificarSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("seccionesPorInciso.jsp",
				"/MidasWeb/cotizacion/mostrarModificarSeccionesPorInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("secciones.jsp",
				"/MidasWeb/cotizacion/mostrarModificarSeccionesODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("cotizacionArea.jsp",
				"/MidasWeb/cotizacion/mostrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/ordentrabajo/cobertura/mostrarCoberturasPorSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("coberturas.jsp",
				"/MidasWeb/ordentrabajo/cobertura/listarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("coberturas.jsp",
				"/MidasWeb/cotizacion/cobertura/listarCoberturasODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/ordentrabajo/cobertura/guardarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("primerRiesgo.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrarPrimerRiesgo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("primerRiesgo.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrarPrimerRiesgo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrarPrimerRiesgo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("LUC.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrarLUC.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("LUC.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrarLUC.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/mostrarLUC.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("primerRiesgo.jsp",
						"/MidasWeb/cotizacion/primerRiesgoLUC/calcularPrimerRiesgoODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("primerRiesgo.jsp",
						"/MidasWeb/cotizacion/primerRiesgoLUC/calcularPrimerRiesgoODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("error.jsp",
						"/MidasWeb/cotizacion/primerRiesgoLUC/calcularPrimerRiesgoODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("/cotizacion/primerRiesgoLUC/calcularLUCODT",
				"/MidasWeb/cotizacion/primerRiesgoLUC/calcularLUCODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("LUC.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/calcularLUCODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/calcularLUCODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/primerRiesgoLUC/datosPrimerRiesgo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("listar.jsp",
						"/MidasWeb/cotizacion/subinciso/listarSubIncisosPorSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("registroUsuario.jsp",
						"/MidasWeb/cotizacion/subinciso/listarSubIncisosPorSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("error.jsp",
						"/MidasWeb/cotizacion/subinciso/listarSubIncisosPorSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/cotizacion/subinciso/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/subinciso/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/cotizacion/subinciso/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/subinciso/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrar.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/cotizacion/subinciso/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/subinciso/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/subinciso/mostrarDatosSubInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cliente/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cliente/asociarClienteCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarClienteCotizacion.jsp",
				"/MidasWeb/cliente/mostrarAsociarClienteCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregarDireccion.jsp",
				"/MidasWeb/direccion/mostrarAgregarDireccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/direccion/agregarDireccion.do"));
		listaPaginaPermiso.add(pp);

		// SISTEMA

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/sistema/cargaCombo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/sistema/cargaCombo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/estado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/ciudad.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/colonia.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/codigoPostal.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/municipio.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subRamo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subGiro.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subGiroRC.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subTipoEquipoContratista.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subTipoMaquinaria.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subTipoEquipoElectronico.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/subTipoRecipientePresion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/monedaTipoPoliza.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/formaPagoMoneda.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/reaseguradorCorredor.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cuentaBancoPesos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cuentaBancoDolares.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/contacto.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/sistema/download/descargarArchivo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/sistema/gestionPendientes/listarPendientes.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/sistema/mail/enviarMail.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/mensaje/verMensaje.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/sistema/mensajePendiente/listarPendientesDanios.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/sistema/mensajePendiente/listarPendientesReaseguro.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/sistema/mensajePendiente/listarPendientesSiniestros.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/sistema/mensajePendiente/remover.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("acerca.jsp",
				"/MidasWeb/sistema/mensajePendiente/mostrarAcerca.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/sistema/mensajePendiente/uploadHandler.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/sistema/mensajePendiente/getInfoHandler.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/sistema/mensajePendiente/getIdHandler.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/cargaMenu.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/sistema/vault/uploadHandler.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/sistema/vault/getInfoHandler.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/sistema/vault/getIdHandler.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/sistema/vault/getFileInformation.do"));
		listaPaginaPermiso.add(pp);

		// USUARIO

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("inicio.jsp", "/MidasWeb/validarUsuario.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/validarUsuario.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp", "/MidasWeb/validarUsuario.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("login.jsp", "/MidasWeb/sistema/logout.do"));
		listaPaginaPermiso.add(pp);

		// CAT�LOGOS

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/ajustador/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/ajustador/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/ajustador/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/ajustador/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/ajustador/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/ajustador/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/aumentovario/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/aumentovario/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/aumentovario/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/aumentovario/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/aumentovario/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/aumentovario/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/aumentovario/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/aumentovario/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/aumentovario/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/aumentovario/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/aumentovario/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/aumentovario/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/aumentovario/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/aumentovario/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/clasificacionembarcacion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/catalogos/clasificacionembarcacion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/clasificacionembarcacion/getPorId.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/codigoPostal/listarPaisTodos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/ajustador/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ajustador/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ajustador/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/ajustador/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/ajustador/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/ajustador/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/ajustador/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario",
				"jsp,/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/codigopostalzonahidro/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/codigopostalzonahidro/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/codigopostalzonasismo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/codigopostalzonasismo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/codigopostalzonasismo/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/contacto/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/contacto/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/contacto/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/contacto/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/contacto/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/contacto/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/contacto/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/contacto/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/contacto/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/contacto/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/contacto/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/contacto/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/contacto/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/contacto/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/contacto/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/cuentabanco/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/cuentabanco/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/cuentabanco/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/cuentabanco/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/cuentabanco/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/cuentabanco/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/cuentabanco/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/cuentabanco/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/cuentabanco/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/cuentabanco/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/cuentabanco/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/cuentabanco/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/cuentabanco/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/cuentabanco/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/descuento/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/descuento/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/descuento/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/descuento/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/descuento/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/descuento/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/descuento/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/descuento/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/descuento/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/descuento/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/descuento/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/distanciaciudad/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/distanciaciudad/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/distanciaciudad/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/distanciaciudad/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/distanciaciudad/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/distanciaciudad/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/distanciaciudad/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/distanciaciudad/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/distanciaciudad/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/distanciaciudad/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/giro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/giro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/giro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/giro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/giro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/giro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/giro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/giro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/giro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/giro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/giro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/giro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/giro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/giro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/giro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/giro/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/giro/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/giro/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/giro/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girorc/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/girorc/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girorc/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girorc/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/girorc/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girorc/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girorc/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/girorc/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girorc/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girorc/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/girorc/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girorc/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girorc/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/girorc/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girorc/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/girorc/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/girorc/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/girorc/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/girorc/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/girotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/girotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/girotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/girotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/girotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/girotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/girotransporte/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/girotransporte/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/girotransporte/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/girotransporte/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/materialcombustible/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/materialcombustible/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/materialcombustible/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/materialcombustible/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/materialcombustible/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/materialcombustible/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/materialcombustible/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/materialcombustible/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/materialcombustible/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/materialcombustible/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/materialcombustible/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/materialcombustible/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/materialcombustible/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/materialcombustible/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/materialcombustible/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/materialcombustible/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/materialcombustible/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/mediotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/mediotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/mediotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/mediotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/mediotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/mediotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/mediotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/mediotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/mediotransporte/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/mediotransporte/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/mediotransporte/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/mediotransporte/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("listar.jsp",
						"/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("registroUsuario.jsp",
						"/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("error.jsp",
						"/MidasWeb/catalogos/paistipodestinotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/paistipodestinotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/descuento/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/descuento/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/descuento/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/descuento/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/descuento/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/descuento/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/distanciaciudad/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/distanciaciudad/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/distanciaciudad/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/paistipodestinotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("agregarborrar.jsp",
						"/MidasWeb/catalogos/paistipodestinotransporte/agregarborrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("borrar.jsp",
						"/MidasWeb/catalogos/paistipodestinotransporte/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/paistipodestinotransporte/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("desplegar.jsp",
						"/MidasWeb/catalogos/paistipodestinotransporte/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/catalogos/paistipodestinotransporte/mostrarPaisesAsociados.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/catalogos/paistipodestinotransporte/mostrarPaisesPorAsociar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/ramo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ramo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ramo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/ramo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ramo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ramo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/ramo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/ramo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ramo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/ramo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ramo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ramo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/ramo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/ramo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/ramo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/ramo/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/ramo/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/ramo/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/ramo/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/reaseguradorcorredor/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/reaseguradorcorredor/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/recargovario/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/recargovario/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/recargovario/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/recargovario/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/recargovario/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/recargovario/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/recargovario/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/recargovario/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/recargovario/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/recargovario/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/recargovario/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/recargovario/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/recargovario/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/recargovario/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/recargovario/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/recargovario/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgiro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subgiro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgiro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgiro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subgiro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgiro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgiro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/subgiro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgiro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgiro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subgiro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgiro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgiro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subgiro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgiro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/subgiro/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/subgiro/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/subgiro/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/subgiro/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgirorc/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subgirorc/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgirorc/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgirorc/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgirorc/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/subgirorc/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgirorc/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgirorc/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subgirorc/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgirorc/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subgirorc/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subgirorc/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subgirorc/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/subgirorc/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/subgirorc/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/subgirorc/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/subgirorc/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subramo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subramo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subramo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subramo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/subramo/listarFiltradoPaginado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subramo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subramo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subramo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/subramo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subramo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subramo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subramo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subramo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subramo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subramo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subramo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/subramo/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/subramo/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/subramo/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/subramo/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("listar.jsp",
						"/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("registroUsuario.jsp",
						"/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("error.jsp",
						"/MidasWeb/catalogos/subtipoequipocontratista/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("agregar.jsp",
						"/MidasWeb/catalogos/subtipoequipocontratista/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipoequipocontratista/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("borrar.jsp",
						"/MidasWeb/catalogos/subtipoequipocontratista/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/subtipoequipocontratista/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("desplegar.jsp",
						"/MidasWeb/catalogos/subtipoequipocontratista/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/subtipomontajemaquina/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/subtipomontajemaquina/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("listar.jsp",
						"/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("registroUsuario.jsp",
						"/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("error.jsp",
						"/MidasWeb/catalogos/subtiporecipientepresion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("agregar.jsp",
						"/MidasWeb/catalogos/subtiporecipientepresion/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/subtiporecipientepresion/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("borrar.jsp",
						"/MidasWeb/catalogos/subtiporecipientepresion/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/subtiporecipientepresion/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("desplegar.jsp",
						"/MidasWeb/catalogos/subtiporecipientepresion/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tipobandera/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tipobandera/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tipobandera/getPorId.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/tipodestinotransporte/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/tipodestinotransporte/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tipoembarcacion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tipoembarcacion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tipoembarcacion/getPorId.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoempaque/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoempaque/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoempaque/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoempaque/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoempaque/agregar.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipoempaque/agregar.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoempaque/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoempaque/modificar.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipoempaque/modificar.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoempaque/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoempaque/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoempaque/borrar.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoempaque/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipoempaque/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/tipoempaque/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipoempaque/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/tipoempaque/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/tipoequipocontratista/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/tipoequipocontratista/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/tipomontajemaquina/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomuro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipomuro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomuro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomuro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomuro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipomuro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomuro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomuro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipomuro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomuro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipomuro/borrar.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipomuro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipomuro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipomuro/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/tipomuro/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipomuro/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/tipomuro/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tiponavegacion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tiponavegacion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/catalogos/tiponavegacion/getPorId.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoobracivil/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoobracivil/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoobracivil/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoobracivil/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoobracivil/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipoobracivil/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipoobracivil/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/tipoobracivil/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("modificar.jsp",
						"/MidasWeb/catalogos/tiporecipientepresion/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/tiporecipientepresion/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipotecho/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipotecho/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipotecho/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipotecho/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipotecho/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipotecho/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipotecho/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipotecho/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipotecho/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipotecho/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/tipotecho/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipotecho/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipotecho/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/tipotecho/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/tipotecho/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/tipotecho/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/tipotecho/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonacresta/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonacresta/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonacresta/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/zonacresta/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/zonacresta/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/zonacresta/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/zonacresta/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("listar.jsp",
						"/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("registroUsuario.jsp",
						"/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("error.jsp",
						"/MidasWeb/catalogos/zonacresta/viejo/regresarASeleccionado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/listarDisponibles.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarFiltrado",
				"/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/zonacresta/viejo/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonacresta/agregarTr.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonacresta/agregarTr.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonacresta/agregarTr.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonahidro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonahidro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonahidro/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonahidro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonahidro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonahidro/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonahidro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonahidro/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonahidro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonahidro/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonahidro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonahidro/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/zonahidro/mostrarAgrega.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/zonahidro/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/zonahidro/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/zonahidro/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonasismo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonasismo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonasismo/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonasismo/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonasismo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonasismo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonasismo/agregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonasismo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonasismo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonasismo/modificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/catalogos/zonasismo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/catalogos/zonasismo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/catalogos/zonasismo/borrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/catalogos/zonasismo/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/catalogos/zonasismo/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificar.jsp",
				"/MidasWeb/catalogos/zonasismo/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegar.jsp",
				"/MidasWeb/catalogos/zonasismo/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		// CONSULTAS

		// OBSOLETOS !!!!!! ??????

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		// OBSOLETOS !!!!!! ??????

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tipoaeronave/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tipoaeronave/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tipolicenciapiloto/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tipolicenciapiloto/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tipopropietariocamion/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tipopropietariocamion/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tiposerviciotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/consultas/tiposerviciotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tipotransporte/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/consultas/tipotransporte/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		// COTIZACION SOLO LECTURA

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("void.jsp",
				"/MidasWeb/cotizacionsololectura/void.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("cotizacionArea.jsp",
				"/MidasWeb/cotizacionsololectura/cotizacion/mostrar.do"));
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();

		pp.setPagina(nuevaPagina("verArbol.jsp",
				"/MidasWeb/cotizacionsololectura/cargarArbolCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina

		("datosGenerales.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarDatosGenerales.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina

		("datosGenerales.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("mostrarResumenCotizacion.jsp",
						"/MidasWeb/cotizacionsololectura/resumen/mostrarResumenCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/cotizacionsololectura/resumen/guardarBonificacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("docAnexos.jsp",
						"/MidasWeb/cotizacionsololectura/docAnexoCot/mostrarDocAnexo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/cotizacionsololectura/docAnexoCot/mostrarListarAnexos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/cotizacionsololectura/docAnexoCot/modificarDocAnexo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacionsololectura/docAnexoCot/guardarOrden.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina(
						"documentosDigitalesComplementarios.jsp",
						"/MidasWeb/cotizacionsololectura/listarDocumentosDigitalesComplementarios.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPersona.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarPersona.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("coberturas.jsp",
						"/MidasWeb/cotizacionsololectura/cobertura/listarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("secciones.jsp",
						"/MidasWeb/cotizacionsololectura/mostrarModificarSecciones.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("cotizacion.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificarComisiones.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarComisiones.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("autorizarComisiones.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarComisiones.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/cotizacionsololectura/mostrarComisionesCotizacion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIncisos.jsp",
				"/MidasWeb/cotizacionsololectura/listarIncisos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIncisos.jsp",
				"/MidasWeb/cotizacionsololectura/listarIncisos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacionsololectura/listarIncisos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarInciso.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarDetalleInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("secciones.jsp",
						"/MidasWeb/cotizacionsololectura/mostrarModificarSecciones.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacionsololectura/cargarSeccionesPorInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacionsololectura/mostrarDatosInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacionsololectura/mostrarDatosSubInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("listarSubIncisos.jsp",
						"/MidasWeb/cotizacionsololectura/listarSubIncisosPorSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("registroUsuario.jsp",
						"/MidasWeb/cotizacionsololectura/listarSubIncisosPorSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("error.jsp",
						"/MidasWeb/cotizacionsololectura/listarSubIncisosPorSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("modificarSubInciso.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarModificar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("textosAdicionales.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarTexAdicional.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("",
						"/MidasWeb/cotizacionsololectura/mostrarListarTextosAdicionales.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacionsololectura/mostrarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("coberturas.jsp",
				"/MidasWeb/cotizacionsololectura/listarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("autorizacionCoberturas.jsp",
				"/MidasWeb/cotizacionsololectura/listarCoberturas.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ARDs.jsp",
				"/MidasWeb/cotizacionsololectura/mostrarARD.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("riesgos.jsp",
						"/MidasWeb/cotizacionsololectura/mostrarRiesgosPorCobertura.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("autorizacionRiesgos.jsp",
						"/MidasWeb/cotizacionsololectura/mostrarRiesgosPorCobertura.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacionsololectura/listarRiesgos.do"));
		listaPaginaPermiso.add(pp);

		// /////////////////////// EXTRAS //////////////////////////////////

		// Mostrar resumen cotizacion endoso
		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("mostrarResumenCotizacion.jsp",
						"/MidasWeb/cotizacion/endoso/resumen/mostrarResumenCotizacion.do"));
		listaPaginaPermiso.add(pp);

		// Agregar subincisos en cotizaci�n
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("agregar.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);

		// Borrar subincisos en cotizaci�n
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("borrar.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("registroUsuario.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/cotizacion/subinciso/mostrarBorrar.do"));
		listaPaginaPermiso.add(pp);

		// Listar P�lizas

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/poliza/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp", "/MidasWeb/poliza/listar.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarModal.jsp",
				"/MidasWeb/poliza/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/poliza/listarFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp",
				"/MidasWeb/poliza/listarPolizaFiltrado.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/poliza/listarPolizaFiltrado.do"));
		listaPaginaPermiso.add(pp);

		// Guardar coberturas

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/cobertura/guardarCobertura.do"));
		listaPaginaPermiso.add(pp);

		// Imprimir endoso

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("seleccionarEndosoPorImprimir.jsp",
				"/MidasWeb/poliza/mostrarImprimirEndoso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("error.jsp",
				"/MidasWeb/poliza/mostrarImprimirEndoso.do"));
		listaPaginaPermiso.add(pp);

		// Mostrar Datos ODT/COT

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosOrdenTrabajo.jsp",
				"/MidasWeb/endoso/mostrarODTCotizacion.do"));
		listaPaginaPermiso.add(pp);

		// Listar cotizaciones endoso
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarCotizaciones.jsp",
				"/MidasWeb/cotizacion/endoso/listarCotizacionesFiltrado.do"));
		listaPaginaPermiso.add(pp);

		// Eliminar LUC
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("LUC.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/eliminarLUC.do"));
		listaPaginaPermiso.add(pp);

		// Eliminar Primer Riesgo
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("primerRiesgo.jsp",
				"/MidasWeb/cotizacion/primerRiesgoLUC/eliminarPRR.do"));
		listaPaginaPermiso.add(pp);

		// Obtener incisos de endoso
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/endoso/getIncisos.do"));
		listaPaginaPermiso.add(pp);

		// Obtener secciones de endoso
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/endoso/getSecciones.do"));
		listaPaginaPermiso.add(pp);

		// Guardar embarque en endosos (declaraci�n)
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("",
				"/MidasWeb/cotizacion/endoso/guardarEmbarque.do"));
		listaPaginaPermiso.add(pp);

		// Orden de trabajo (s�lo lectura)
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ordenTrabajoArea.jsp",
				"/MidasWeb/ordentrabajosololectura/mostrarODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIncisos.jsp",
				"/MidasWeb/ordentrabajosololectura/listarIncisos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosOrdenTrabajo.jsp",
				"/MidasWeb/ordentrabajosololectura/mostrarDatosODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarInciso.jsp",
				"/MidasWeb/ordentrabajosololectura/mostrarDetalleInciso.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("ordenTrabajo.jsp",
				"/MidasWeb/ordentrabajosololectura/mostrarODTDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("verArbol.jsp",
						"/MidasWeb/ordentrabajosololectura/cargarArbolOrdenTrabajo.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarPersona.jsp",
				"/MidasWeb/ordentrabajosololectura/mostrarPersona.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentosSolicitud.jsp",
				"/MidasWeb/ordentrabajosololectura/listarDocumentos.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("secciones.jsp",
						"/MidasWeb/ordentrabajosololectura/mostrarModificarSeccionesODT.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp
				.setPagina(nuevaPagina("listarSubIncisos.jsp",
						"/MidasWeb/ordentrabajosololectura/listarSubIncisosPorSeccion.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("mostrarSubInciso.jsp",
				"/MidasWeb/ordentrabajosololectura/mostrarDetalle.do"));
		listaPaginaPermiso.add(pp);

		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("coberturas.jsp",
				"/MidasWeb/ordentrabajosololectura/listarCoberturasODT.do"));
		listaPaginaPermiso.add(pp);
		
		//Cat�logo de colonias
	       
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarColonias.do"));
	    listaPaginaPermiso.add(pp);
	    
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarFiltrado.do"));
	    listaPaginaPermiso.add(pp);
	      
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/colonia/listarFiltradoPaginado.do"));
	    listaPaginaPermiso.add(pp);
	       
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/colonia/mostrarModificar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/modificar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/colonia/mostrarAgregar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/validaCodigoPostal.do"));
	    listaPaginaPermiso.add(pp);
	   
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/colonia/agregar.do"));
	    listaPaginaPermiso.add(pp);
	   
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/colonia/mostrarDetalle.do"));
	    listaPaginaPermiso.add(pp);	    	    	    
	    
	    //Comisiones - Cotizaci�n READ-ONLY
	    
	    pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("autorizarComisiones.jsp","/MidasWeb/cotizacionsololectura/comision/mostrar.do"));
        listaPaginaPermiso.add(pp);
       
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("modificarComisiones.jsp","/MidasWeb/cotizacionsololectura/comision/mostrar.do"));
        listaPaginaPermiso.add(pp);
        
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("mostrarSubInciso.jsp"," /MidasWeb/cotizacionsololectura/subinciso/mostrarDetalle.do"));
        listaPaginaPermiso.add(pp);
        
        //Listar filtrado colonias
	    pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("listar.jsp"," /MidasWeb/catalogos/colonia/listarFiltradoPaginado.do"));
	    listaPaginaPermiso.add(pp);
	    
	    //Listar cotizaciones endoso (paginado)
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("listarCotizaciones.jsp","/MidasWeb/cotizacion/endoso/listarFiltradoPaginado.do"));
        listaPaginaPermiso.add(pp);
        
	    //Listar polizas (paginado)
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/poliza/listarFiltradoPaginado.do"));
        listaPaginaPermiso.add(pp);
        
	    //Carga m�siva de incisos
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/cotizacion/inciso/cargaMasiva.do"));
        listaPaginaPermiso.add(pp);

        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/inciso/generarTemplateCargaMasiva.do"));
        listaPaginaPermiso.add(pp);
        
        //Para cuando es un endoso de modificacion se manda a cargar el arbol de Orden de Trabajo 
	pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("acciones.jsp","/MidasWeb/endoso/solicitud/mostrarAcciones.do"));
        listaPaginaPermiso.add(pp);
        
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/endoso/solicitud/autoAsignarOrden.do"));
        listaPaginaPermiso.add(pp);
        
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("ordenTrabajoArea.jsp","/MidasWeb/endoso/solicitud/autoAsignarOrden.do"));
        listaPaginaPermiso.add(pp);
        
        pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
        //Registro de Aclaraciones
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("aclaraciones.jsp","/MidasWeb/ordenTrabajo/mostrarAclaraciones.do"));
        listaPaginaPermiso.add(pp);

        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("aclaraciones.jsp","/MidasWeb/ordenTrabajo/guardarAclaraciones.do"));
        listaPaginaPermiso.add(pp);
        
        //Tab Endoso        
        pp = new PaginaPermiso();
        pp.setPagina(nuevaPagina("","/MidasWeb/cotizacion/endoso/consultarDatosEndoso.do"));
        listaPaginaPermiso.add(pp);
        
		return this.listaPaginaPermiso;
	}

}