/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;
import oracle.jdbc.internal.OracleConnection;
import java.math.BigDecimal;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import oracle.jdbc.driver.OracleTypes;
import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.sql.DataSource;
import oracle.sql.ARRAY;
import oracle.sql.ArrayDescriptor;
import java.sql.SQLException;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas2.dao.compensaciones.CaConfiguracionBancaDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaEstadoDaoImpl;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaGerenciaDaoImpl;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaLineaVentaDaoImpl;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaRamoDaoImpl;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBitacoraDaoImpl;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaConfiguracionBancaDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaEstado;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaGerencia;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaLineaVenta;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaRamo;
import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;
import mx.com.afirme.midas2.domain.compensaciones.CaConfiguracionBanca;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycos;
import mx.com.afirme.midas2.domain.emision.ppct.GerenciaSeycosId;
import mx.com.afirme.midas2.domain.emision.ppct.RamoSeycos;
import mx.com.afirme.midas2.dto.compensaciones.BancaEstadosView;
import mx.com.afirme.midas2.dto.compensaciones.CalculosBancaView;
import mx.com.afirme.midas2.dto.compensaciones.ConfiguracionContraprestacionBanca;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.compensaciones.CaBancaEstadoService;
import mx.com.afirme.midas2.service.compensaciones.CaBancaGerenciaService;
import mx.com.afirme.midas2.service.compensaciones.CaBancaLineaVentaService;
import mx.com.afirme.midas2.service.compensaciones.CaBancaRamoService;
import mx.com.afirme.midas2.service.compensaciones.CaBitacoraService;
import mx.com.afirme.midas2.service.compensaciones.CaConfiguracionBancaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;
import mx.com.afirme.midas2.exeption.ApplicationException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.datasource.DataSourceUtils;

@Stateless

public class CaConfiguracionBancaServiceImpl  implements CaConfiguracionBancaService {	

	private static final Logger LOG = LoggerFactory.getLogger(CaConfiguracionBancaDaoImpl.class);
	
	@PersistenceContext
	private EntityManager entityManager;

	@EJB
	private CaConfiguracionBancaDao CaConfiguracionBanca;	
	@EJB
	private CaBancaEstadoService caBancaEstadoService;
	@EJB
	private CaBancaGerenciaService caBancaGerenciaService;
	@EJB
	private CaBancaLineaVentaService caBancaLineaVentaService;
	@EJB
	private CaBancaRamoService caBancaRamoService;	
	@EJB
	private EstadoFacadeRemote estadoFacadeRemote;
	@EJB
	private ImpresionesService impresionesService;
	@EJB
	private CaBitacoraService caBitacoraService;
	
	
	private static final String PAIS_MEXICO = "PAMEXI";
	
	private Connection con ;
	private DataSource dataSource;
	    
	@Resource(name = "jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
	     this.con = DataSourceUtils.getConnection(dataSource);
	}
	
    public void save(CaConfiguracionBanca entity) {   
	    try {
	    	LOG.info(">> save()");
	    	CaConfiguracionBanca.save(entity);	  
	    	LOG.info("<< save()");
	    } catch (RuntimeException re) {	 
	    	LOG.error("Información del Error", re);
	        throw re;
	    }
    }
    

    public void delete(CaConfiguracionBanca entity) { 
    	try {
    		LOG.info(">> delete()");
    		CaConfiguracionBanca.delete(entity);
    		LOG.info("<< delete()");
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
    		throw re;
    	}
    }
    

    public CaConfiguracionBanca update(CaConfiguracionBanca entity) {   
        try {
        	LOG.info(">> update()");
	        CaConfiguracionBanca result = CaConfiguracionBanca.update(entity);
	        LOG.info("<< update()");
	        return result;
	    } catch (RuntimeException re) {
	    	LOG.error("Información del Error", re);
            throw re;
	    }
    }
    
    public CaConfiguracionBanca findById( Long id) {
    	LOG.info(">> findById()");
	    try {
            CaConfiguracionBanca instance = CaConfiguracionBanca.findById(id);
           	LOG.info("<< findById()");
            return instance;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        }
        return null;
    }   


    public List<CaConfiguracionBanca> findByProperty(String propertyName, final Object value) {    	
		try {
			return CaConfiguracionBanca.findByProperty(propertyName, value);
		} catch (RuntimeException re) {
			
			return null;
		}
	}			
	public List<CaConfiguracionBanca> findByNombre(Object nombre) {
		return findByProperty(CaConfiguracionBancaDaoImpl.ID, nombre);
	}
	
	public List<CaConfiguracionBanca> findByValor(Object valor) {
		return findByProperty(CaConfiguracionBancaDaoImpl.NOMBRE, valor);
	}
	
	public List<CaConfiguracionBanca> findByDivisor(Object valor) {
		return findByProperty(CaConfiguracionBancaDaoImpl.USUARIO, valor);
	}
	
	public List<CaConfiguracionBanca> findByUsuario(Object usuario) {
		return findByProperty(CaConfiguracionBancaDaoImpl.FECHAMODIFICACION, usuario);
	}
	
	public List<CaConfiguracionBanca> findByBorradologico(Object borradologico) {
		return findByProperty(CaConfiguracionBancaDaoImpl.BORRADOLOGICO, borradologico);
	}
	
	public List<CaConfiguracionBanca> findAll() {	
		try {
			return CaConfiguracionBanca.findAll();
		} catch (RuntimeException re) {
			return null;
		}
	}


	@Override
	public void cargarConfiguradorContraprestacion(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca) {
		LOG.info(">> cargarConfiguradorContraprestacion()");
		try{
			if(configuracionContraprestacionBanca != null){
				/**
				 * Busca la configuracion guarda
				 */
				Long configuracionBancaId = configuracionContraprestacionBanca.getCaConfiguracionBanca().getId();
				CaConfiguracionBanca caConfiguracionBanca = this.findById(configuracionBancaId);
				if(caConfiguracionBanca != null){
					configuracionContraprestacionBanca.setCaConfiguracionBanca(caConfiguracionBanca);
					/**
					 * Busqueda de Linea de Negocio
					 */
					List<CaBancaLineaVenta> listCaBancaLineaVenta = this.caBancaLineaVentaService.findByProperty(CaBancaLineaVentaDaoImpl.CABANCACONFIGURACION_ID, configuracionBancaId);
					if(listCaBancaLineaVenta != null && !listCaBancaLineaVenta.isEmpty()){
						configuracionContraprestacionBanca.setLineaVentaId(listCaBancaLineaVenta.get(0).getRamoId());
					}
					/**
					 * Busqueda de Ramos
					 */
					List<CaBancaRamo> listCaBancaRamo = this.caBancaRamoService.findByProperty(CaBancaRamoDaoImpl.CABANCACONFIGURACION_ID, configuracionBancaId);
					if(listCaBancaRamo != null && !listCaBancaRamo.isEmpty()){
						List<String> listRamosIds = new ArrayList<String>();						
						for(CaBancaRamo caBancaRamo : listCaBancaRamo){
							listRamosIds.add(caBancaRamo.getRamoSeycosId());
						}
						configuracionContraprestacionBanca.setListRamosId(listRamosIds);
					}
					/**
					 * Busqueda de Gerencias
					 */
					List<CaBancaGerencia> listCaBancaGerencia = this.caBancaGerenciaService.findByProperty(CaBancaGerenciaDaoImpl.CABANCACONFIGURACION_ID, configuracionBancaId);
					if(listCaBancaGerencia != null && !listCaBancaGerencia.isEmpty()){
						CaBancaGerencia caBancaGerencia = listCaBancaGerencia.get(0);
						configuracionContraprestacionBanca.setGerenciaSeycosId(new GerenciaSeycosId(caBancaGerencia.getIdEmpresa(), caBancaGerencia.getIdGerencia(), caBancaGerencia.getFsit()));
					}					
					
				}else{
					configuracionContraprestacionBanca.getCaConfiguracionBanca().setId(null);
				}
			}else{
				/**
				 * Crea una nueva entidad para crear el nuevo registro
				 */
				configuracionContraprestacionBanca = new ConfiguracionContraprestacionBanca(); 
				configuracionContraprestacionBanca.setCaConfiguracionBanca(new CaConfiguracionBanca());
				configuracionContraprestacionBanca.setLineaVentaId(-1L);
				configuracionContraprestacionBanca.setListGerenciasSeycosId(new ArrayList<GerenciaSeycosId>());
				configuracionContraprestacionBanca.setListRamosId(new ArrayList<String>());
			}
		}catch (Exception e) {
			LOG.error("Información del Error", e);
		}		
		LOG.info("<< cargarConfiguradorContraprestacion()");
	}


	/**
	 * Busca los Estados de Mexico
	 */
	@Override
	public List<EstadoDTO> findEstadosDTO() {
		LOG.info(">> findEstadosDTO()");
		 List<EstadoDTO> listEstadosDTO = null;
		try{
			listEstadosDTO = estadoFacadeRemote.findByProperty("countryId", PAIS_MEXICO);			
		}catch(Exception e){
			listEstadosDTO = new ArrayList<EstadoDTO>();
			LOG.error("Información del Error", e);			
		}
		LOG.info("<< findEstadosDTO()");
		return listEstadosDTO;
	}


	@SuppressWarnings("unchecked")
	@Override
	public List<GerenciaSeycos> findGerenciasSeycos() {
		LOG.info(">> findGerenciasSeycos()");
		List<GerenciaSeycos> listGerenciaSeycos = null;
		try{		
			StringBuilder queryString = new StringBuilder(" SELECT ger.* FROM SEYCOS.GERENCIA ger ");
			queryString.append("  WHERE ger.b_ult_mod = 'V'  ");
			queryString.append(" AND ger.id_gerencia IN (405, 406, 407, 408, 409) ");
			Query query = entityManager.createNativeQuery(queryString.toString(), GerenciaSeycos.class);
			listGerenciaSeycos = query.getResultList();		
		}catch(Exception e){
			listGerenciaSeycos = new ArrayList<GerenciaSeycos>();
			LOG.error("Información del Error", e);			
		}
		LOG.info("<< findGerenciasSeycos()");
		return listGerenciaSeycos;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<RamoSeycos> findRamosSeycos() {
		LOG.info(">> findRamosSeycos()");
		List<RamoSeycos> listRamosSeycos = null;
		try{
			StringBuilder queryString = new StringBuilder(" SELECT * FROM SEYCOS.ING_RAMO ");
			Query query = entityManager.createNativeQuery(queryString.toString(), RamoSeycos.class);
			listRamosSeycos = query.getResultList();					
		}catch(Exception e){
			listRamosSeycos = new ArrayList<RamoSeycos>();
			LOG.error("Información del Error", e);			
		}
		LOG.info("<< findRamosSeycos()");
		return listRamosSeycos;
	}
	
	/**
	 * Retorna un Map con los Estados Activos
	 * para la configuracion
	 * @param configuracionContraprestacionBanca
	 * @return
	 */
	@SuppressWarnings("unchecked")
	@Override
	public List<BancaEstadosView> cargarEstadosActivos(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca){
		LOG.info(">> cargarEstadosActivos()");
		
		List<BancaEstadosView> listBancaEstados = new ArrayList<BancaEstadosView>();
		
		try{
			if(configuracionContraprestacionBanca != null && configuracionContraprestacionBanca.getCaConfiguracionBanca() != null){
				
				Long idConfiguracionBanca = configuracionContraprestacionBanca.getCaConfiguracionBanca().getId();
				
				if(idConfiguracionBanca != null && idConfiguracionBanca > 0){
					int index = 1;
					StringBuilder queryString = new StringBuilder(" SELECT ");
					queryString.append(" bansta.id, bansta.cabancaconfiguracion_id, bansta.estado_id, sta.state_name");
					queryString.append(" FROM MIDAS.ca_banca_estado bansta ");
					queryString.append(" INNER JOIN MIDAS.vw_state sta ON(sta.state_id = bansta.estado_id) ");
					queryString.append(" WHERE sta.country_id = ? ");
					queryString.append(" AND bansta.cabancaconfiguracion_id = ? ");
					
					Query query = entityManager.createNativeQuery(queryString.toString());
					query.setParameter(index++, PAIS_MEXICO);
					query.setParameter(index++, idConfiguracionBanca);
					
					List<Object[]> listResult = query.getResultList();
					
					BancaEstadosView bancaEstadosView = null;
					for(Object[] result : listResult){
						bancaEstadosView = new  BancaEstadosView();
						bancaEstadosView.setId((BigDecimal) result[0]);
						bancaEstadosView.setConfiguracionBancaId((BigDecimal) result[1]);
						bancaEstadosView.setEstadoId((String) result[2]);
						bancaEstadosView.setEstadoNombre((String) result[3]);
						listBancaEstados.add(bancaEstadosView);
					}
				}
				configuracionContraprestacionBanca.setListBancaEstadosActivos(listBancaEstados);			
			}else{
				configuracionContraprestacionBanca = new ConfiguracionContraprestacionBanca();
				configuracionContraprestacionBanca.setListBancaEstadosActivos(listBancaEstados);
			}
		}catch (Exception e) {
			LOG.error("Información del Error", e);
		}
		LOG.info("<< cargarEstadosActivos()");
		
		return listBancaEstados;
		
	}

	/**
	 * Guardado General de la Configuracion de Contraprestacion de Banca
	 * @param configuracionContraprestacionBanca
	 */
	@Override	
	public void guardarConfiguracion(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca) {
		LOG.info(">> guardarConfiguracion()");		
		try{
			/**
			 * Guarda la Configuracion de Banca
			 */
			CaConfiguracionBanca caConfiguracionBanca = configuracionContraprestacionBanca.getCaConfiguracionBanca();
			boolean isNewRecord = caConfiguracionBanca.getId() == null;
			
			if(isNewRecord){
				caConfiguracionBanca.setFechaModificacion(new Date());	
				this.save(caConfiguracionBanca);
			}else{
				caConfiguracionBanca.setFechaModificacion(new Date());	
				this.update(caConfiguracionBanca);
			}
			Long configuracionBancaId = caConfiguracionBanca.getId();			
			this.guardarLineaVenta(isNewRecord, configuracionBancaId, configuracionContraprestacionBanca);
			this.guardarRamos(isNewRecord, configuracionBancaId, configuracionContraprestacionBanca);
			this.guardarGerencias(isNewRecord, configuracionBancaId, configuracionContraprestacionBanca);
			this.guardarEstados(isNewRecord, configuracionBancaId, configuracionContraprestacionBanca);		
			
			this.guardarBitacoraConfiguracionBanca(isNewRecord, configuracionBancaId, caConfiguracionBanca.getUsuario());
		}catch(RuntimeException e){
			LOG.error("Información del Error", e);
			throw e;
		}
		LOG.info("<< guardarConfiguracion()");		
	}
	
	/**
	 * Guarda la Linea de Venta
	 * @param isNewRecord
	 * @param configuracionBancaId
	 * @param configuracionBancaId
	 */
	private void guardarLineaVenta(boolean isNewRecord,long configuracionBancaId, ConfiguracionContraprestacionBanca  configuracionContraprestacionBanca){
		LOG.info(">> guardarLineaVenta()");
		Long lineaVentaId = configuracionContraprestacionBanca.getLineaVentaId();			
		CaBancaLineaVenta caBancaLineaVenta = null;
		if(isNewRecord){
			caBancaLineaVenta = new CaBancaLineaVenta();
			caBancaLineaVenta.setConfiguracionBancaId(configuracionBancaId);
			caBancaLineaVenta.setRamoId(lineaVentaId);
			this.caBancaLineaVentaService.save(caBancaLineaVenta);
		}else{
			List<CaBancaLineaVenta> listCaBancaLineaVenta = this.caBancaLineaVentaService.findByProperty(CaBancaLineaVentaDaoImpl.CABANCACONFIGURACION_ID, configuracionBancaId);
			caBancaLineaVenta = listCaBancaLineaVenta.get(0);
			caBancaLineaVenta.setRamoId(lineaVentaId);
			this.caBancaLineaVentaService.update(caBancaLineaVenta);
		}
		LOG.info("<< guardarLineaVenta()");
	}
	
	
	/**
	 * Guarda los Ramos 
	 * @param isNewRecord
	 * @param configuracionBancaId
	 * @param configuracionContraprestacionBanca
	 */
	private void guardarRamos(boolean isNewRecord,long configuracionBancaId, ConfiguracionContraprestacionBanca  configuracionContraprestacionBanca){
		LOG.info(">> guardarRamos()");
		List<String> listRamoIds = configuracionContraprestacionBanca.getListRamosId();
		CaBancaRamo caBancaRamo = null;
		if(isNewRecord){			
			for(String idRamo : listRamoIds){
				caBancaRamo = new CaBancaRamo();
				caBancaRamo.setConfiguracionBancaId(configuracionBancaId);
				caBancaRamo.setRamoSeycosId(idRamo);
				this.caBancaRamoService.save(caBancaRamo);
			}
		}else{
			List<CaBancaRamo> listCaBancaRamo = this.caBancaRamoService.findByProperty(CaBancaRamoDaoImpl.CABANCACONFIGURACION_ID, configuracionBancaId);
			/**
			 * Ramos a eliminar
			 */
			Iterator<CaBancaRamo> iteratorRamo = listCaBancaRamo.iterator();			
			while(iteratorRamo.hasNext()){
				caBancaRamo = iteratorRamo.next();
				if(!listRamoIds.contains(caBancaRamo.getRamoSeycosId())){
					this.caBancaRamoService.delete(caBancaRamo);
					iteratorRamo.remove();
				}	
			}
			
			/**
			 * Ramos a agregar
			 */			
			boolean isNewRamo = true;
			for(String idRamo : listRamoIds){
				isNewRamo = true;
				while(iteratorRamo.hasNext()){
					caBancaRamo = iteratorRamo.next();
					if(idRamo.equals(caBancaRamo.getRamoSeycosId())){
						isNewRamo = false;
						iteratorRamo.remove();
						break;
					}	
				}				
				if(isNewRamo){
					caBancaRamo = new CaBancaRamo();
					caBancaRamo.setConfiguracionBancaId(configuracionBancaId);
					caBancaRamo.setRamoSeycosId(idRamo);
					this.caBancaRamoService.save(caBancaRamo);
				}
			}			
		}
		LOG.info("<< guardarRamos()");
	}
	
	/**
	 * Guarda las Gerencias
	 * @param isNewRecord
	 * @param configuracionBancaId
	 * @param configuracionContraprestacionBanca
	 */
	private void guardarGerencias(boolean isNewRecord,long configuracionBancaId, ConfiguracionContraprestacionBanca  configuracionContraprestacionBanca){
		LOG.info(">> guardarGerencias()");
		GerenciaSeycosId gerenciaSeycosId = configuracionContraprestacionBanca.getGerenciaSeycosId();
		CaBancaGerencia caBancaGerencia = null;
		if(isNewRecord){
			caBancaGerencia = new CaBancaGerencia();
			caBancaGerencia.setConfiguracionBancaId(configuracionBancaId);
			caBancaGerencia.setFsit(gerenciaSeycosId.getFSit());
			caBancaGerencia.setIdEmpresa(gerenciaSeycosId.getIdEmpresa());
			caBancaGerencia.setIdGerencia(gerenciaSeycosId.getIdGerencia());
			this.caBancaGerenciaService.save(caBancaGerencia);
		}else{
			List<CaBancaGerencia> listCaBancaGerencia = this.caBancaGerenciaService.findByProperty(CaBancaGerenciaDaoImpl.CABANCACONFIGURACION_ID, configuracionBancaId);
			caBancaGerencia = listCaBancaGerencia.get(0);
			caBancaGerencia.setConfiguracionBancaId(configuracionBancaId);
			caBancaGerencia.setFsit(gerenciaSeycosId.getFSit());
			caBancaGerencia.setIdEmpresa(gerenciaSeycosId.getIdEmpresa());
			caBancaGerencia.setIdGerencia(gerenciaSeycosId.getIdGerencia());
			this.caBancaGerenciaService.update(caBancaGerencia);
		}	
		LOG.info("<< guardarGerencias()");
	}
	
	/**
	 * Guarda la relacion de los Estados
	 * @param isNewRecord
	 * @param configuracionBancaId
	 * @param configuracionContraprestacionBanca
	 */
	private void guardarEstados(boolean isNewRecord,long configuracionBancaId, ConfiguracionContraprestacionBanca  configuracionContraprestacionBanca){
		LOG.info(">> guardarEstados()");
		List<BancaEstadosView> listBancaEstadosView = configuracionContraprestacionBanca.getListBancaEstadosActivos();
		CaBancaEstado caBancaEstado = null;
		BancaEstadosView bancaEstadoView = null;
		
		if(isNewRecord){
			for(BancaEstadosView bancaEstadosView : listBancaEstadosView){
				caBancaEstado = new CaBancaEstado();
				caBancaEstado.setConfiguracionBancaId(configuracionBancaId);
				caBancaEstado.setEstadoId(bancaEstadosView.getEstadoId());
				this.caBancaEstadoService.save(caBancaEstado);
			}
		}else{
			List<CaBancaEstado> listCaBancaEstado = this.caBancaEstadoService.findByProperty(CaBancaEstadoDaoImpl.CABANCACONFIGURACION_ID, configuracionBancaId);
			/**
			 * Estados a eliminar
			 */
			Iterator<CaBancaEstado> iteratorEstado = listCaBancaEstado.iterator();		
			Iterator<BancaEstadosView> iteratorEstadoView = listBancaEstadosView.iterator();
			
			boolean delete = true;
			while(iteratorEstado.hasNext()){
				delete = true;
				caBancaEstado = iteratorEstado.next();						
				while(iteratorEstadoView.hasNext()){
					bancaEstadoView = iteratorEstadoView.next();
					if(bancaEstadoView.getEstadoId().equals(caBancaEstado.getEstadoId())){						
						delete = false;
						iteratorEstado.remove();
						iteratorEstadoView.remove();
						break;
					}	
				}
				if(delete){
					this.caBancaEstadoService.delete(caBancaEstado);
				}
			}
			
			/**
			 * Estados a agregar
			 */			
			boolean isNewRamo = true;
			for(BancaEstadosView estadoView : listBancaEstadosView){
				isNewRamo = true;
				while(iteratorEstado.hasNext()){
					caBancaEstado = iteratorEstado.next();
					if(estadoView.getEstadoId().equals(caBancaEstado.getEstadoId())){
						isNewRamo = false;
						iteratorEstado.remove();
						break;
					}	
				}				
				if(isNewRamo){
					caBancaEstado = new CaBancaEstado();
					caBancaEstado.setConfiguracionBancaId(configuracionBancaId);
					caBancaEstado.setEstadoId(estadoView.getEstadoId());
					this.caBancaEstadoService.save(caBancaEstado);
				}
			}			
		}
		LOG.info("<< guardarEstados()");
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CaConfiguracionBanca> filterCalculosCaConfiguracionesBanca(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca){
		LOG.info(">> filterCaConfiguracionesBanca()");
		 List<CaConfiguracionBanca> listCaConfiguracionBanca = null;
		 try{			 
			 
			List<GerenciaSeycosId> listGerencias = configuracionContraprestacionBanca.getListGerenciasSeycosId();
			List<Long> listLineas = configuracionContraprestacionBanca.getListLineasVenta();
			List<String> listRamos = configuracionContraprestacionBanca.getListRamosId();
					    
			StringBuilder queryString = new StringBuilder(" SELECT ");
			queryString.append(" caba.id, caba.nombre, caba.usuario, caba.fechamodificacion ");
			queryString.append(" FROM MIDAS.ca_configuracion_banca caba ");
			/**
			 * Gerencias
			 */
			if(listGerencias != null && !listGerencias.isEmpty()){
				queryString.append(" INNER JOIN MIDAS.ca_banca_gerencia bager ON(bager.cabancaconfiguracion_id = caba.id AND bager.id_gerencia IN ( ");				
				Iterator<GerenciaSeycosId> iterator = listGerencias.iterator();				
				while(iterator.hasNext()){
					queryString.append(iterator.next().getIdGerencia()).append(iterator.hasNext() ? ", " : "" );			
				}				
				queryString.append(" ) ) ");
			}
			/**
			 * Lineas de Venta
			 */
			if(listLineas != null && !listLineas.isEmpty()){
				queryString.append(" INNER JOIN MIDAS.ca_banca_lineaventa liven ON(liven.cabancaconfiguracion_id = caba.id  AND liven.ramo_id IN ( ");
				Iterator<Long> iterator = listLineas.iterator();				
				while(iterator.hasNext()){
					queryString.append(iterator.next()).append(iterator.hasNext() ? ", " : "" );
				}				
				queryString.append(" ) ) ");
			}			
			/**
			 * Ramos
			 */
			if(listRamos != null && !listRamos.isEmpty()){
				queryString.append(" INNER JOIN MIDAS.ca_banca_ramo bara ON(bara.cabancaconfiguracion_id = caba.id AND bara.ramoseycos_id IN ( ");
				Iterator<String> iterator = listRamos.iterator();				
				while(iterator.hasNext()){
					queryString.append(iterator.next()).append(iterator.hasNext() ? ", " : "" );					
				}				
				queryString.append(" ) ) ");
			}
			
			queryString.append(" GROUP BY caba.id, caba.nombre, caba.usuario, caba.fechamodificacion ORDER BY caba.id ");
			
			Query query = entityManager.createNativeQuery(queryString.toString(), CaConfiguracionBanca.class);
							 
			listCaConfiguracionBanca = query.getResultList();
			
		 }catch(Exception e){
			 LOG.error("Información del Error", e);
			 listCaConfiguracionBanca = new ArrayList<CaConfiguracionBanca>();
		 }
		 LOG.info("<< filterCaConfiguracionesBanca()");			
		 return listCaConfiguracionBanca;
	}
	
	
	@SuppressWarnings("unchecked")
	public List<CalculosBancaView> ejecutarCalculosBanca(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca, List<CaConfiguracionBanca> listCaConfiguracionBanca, Date fecha){
		LOG.info(">> ejecutarCalculosBanca()");
		List<CalculosBancaView> listCalculosBancaView = null;
		StoredProcedureHelper storedHelper = null;
		try{
			storedHelper = new StoredProcedureHelper("MIDAS.PKG_CA_BANCA_COMPENSACIONES.calcularProvision", "jdbc/MidasDataSource");
			
			List<GerenciaSeycosId> listGerenciasSeycos = configuracionContraprestacionBanca.getListGerenciasSeycosId();			
			List<String> listGerenciasIds = new ArrayList<String>();			
			for(GerenciaSeycosId gerenciaSeycosId:listGerenciasSeycos){
				listGerenciasIds.add(gerenciaSeycosId.getIdGerencia());
			}
			
			List<Long> listLineaVentaIds = configuracionContraprestacionBanca.getListLineasVenta();
			List<String> listRamosIds = configuracionContraprestacionBanca.getListRamosId();
			List<Long> listConfiguraciones = new ArrayList<Long>();
			
			for(CaConfiguracionBanca configuracionBanca: listCaConfiguracionBanca){
				listConfiguraciones.add(configuracionBanca.getId());
			}
			
			/**
			 * Parametros de Entrada
			 */
			storedHelper.estableceParametro("pGerencia",Arrays.toString(listGerenciasIds.toArray()).replace("[", "").replace("]", ""));
			storedHelper.estableceParametro("pRamo",Arrays.toString(listRamosIds.toArray()).replace("[", "").replace("]", ""));
			storedHelper.estableceParametro("pLineaVenta",Arrays.toString(listLineaVentaIds.toArray()).replace("[", "").replace("]", ""));
			storedHelper.estableceParametro("pConfiguraciones",Arrays.toString(listConfiguraciones.toArray()).replace("[", "").replace("]", ""));
			storedHelper.estableceParametro("pFecha",fecha);
			
			/**
			 * Parametros de Salida
			 */
			String[] propiedades = {"ramo","codigoRamo","primaNetaEmitida", "primaNetaPagada", "porcentajeContraprestacionProducto",
												 "montoContraprestacionProducto", "porcentajeContraprestacionCumplimientoMeta", "montoContraprestacionCumplimientoMeta",
												 "porcentajeCalidadCartera", "montoCalidadCartera", "bonoFijo", "descuento", "total", "comentarios"};			
			
			String[] columnas = {"RAMO","CODIGORAMO", "PRIMA_NETA_EMITIDA", "PRIMA_NETA_PAGADA", "PORC_CONTRAPRES_PROD",
											  "MONTO_CONTRAPRES_PROD", "PORC_CONTRAPRES_CUMP_META", "MONTO_CONTRAPRES_CUMP_META",
											  "PORCENTAJE_CALIDAD_CARTERA", "MONTO_CALIDAD_CARTERA", "BONO_FIJO", "DESCUENTO", "TOTAL", "COMENTARIOS"};
			
			storedHelper.estableceMapeoResultados(CalculosBancaView.class.getCanonicalName(), propiedades, columnas);	
			
			listCalculosBancaView = storedHelper.obtieneListaResultados();
			
		}catch(Exception ex){
			LOG.error("Información del Error", ex);
			listCalculosBancaView = new ArrayList<CalculosBancaView>();
		}
		LOG.info("<< ejecutarCalculosBanca()");
		return listCalculosBancaView;
	}
	
	
	public void procesarListaConfiguracionBanca(List<CalculosBancaView> listCalculosBancaview, Date fecha){
		LOG.info("procesarListaConfiguracionBanca() >>");
		Object[] cargarCalculosBanca = procesarListaCalculosBanca(listCalculosBancaview);
		guardarCalculosBanca(cargarCalculosBanca,fecha);

	}

	public Object[] procesarListaCalculosBanca(List<CalculosBancaView> listCalculosBancaview){
		LOG.info("procesarListaCalculosBanca() >>");
		List<CalculosBancaView> listCalculosBancaView = new ArrayList<CalculosBancaView>();
		Object[] arrayObjects = null;
		try{
			arrayObjects = new Object[listCalculosBancaview.size()];
			int i = 0;
			for (CalculosBancaView calculosBanca : listCalculosBancaview){
				calculosBanca.setComentarios("");
				calculosBanca.setBonoFijo(null);
				Object[] arrayVector = new Object[]{
						calculosBanca.getCodigoRamo(),
						calculosBanca.getComentarios(),
						calculosBanca.getDescuento(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getTotal(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getComentarios(),
						calculosBanca.getBonoFijo(),
						calculosBanca.getBonoFijo(),
						calculosBanca.getComentarios(),
						calculosBanca.getBonoFijo(),
						calculosBanca.getBonoFijo(),
						calculosBanca.getBonoFijo()
				};
				arrayObjects[i] = arrayVector;
				i = i + 1;
			}
		}catch(Exception e){
			LOG.info("Informacion del Error : " +e);

		}
		return arrayObjects;
	}

	public void guardarCalculosBanca(Object[] listCalculosBancaview,Date fecha){
		java.sql.Date sqlDate = new java.sql.Date(fecha.getTime());
		LOG.info("Fecha Transformada : " + sqlDate);
		ErrorBuilder eb = new ErrorBuilder();
		try{
			OracleConnection oracleCon = con.unwrap(OracleConnection.class);
			ArrayDescriptor arrayDescrConfigBanca = ArrayDescriptor.createDescriptor("MIDAS.CA_COSTOPRIMAS_LIST",oracleCon);
			ARRAY arrayConfigBanca = new ARRAY(arrayDescrConfigBanca,oracleCon,listCalculosBancaview);
			StringBuilder sb = new StringBuilder();
			sb.append(" BEGIN ");
			 sb.append(" MIDAS.PKG_CA_BANCA_COMPENSACIONES.contabilizarProvision( " );
			 sb.append(" CA_COSTOPRIMASLISTA => ?, ");
			 sb.append(" pFecha => ?, ");
			 sb.append(" pId_Cod_Resp => ?,");
             sb.append(" pDesc_Resp => ?);" );
             sb.append(" END; ");
             LOG.info("Procedimiento a ejecutar : " + sb.toString());
             CallableStatement vectoresInsertSP = oracleCon.prepareCall(sb.toString());
             vectoresInsertSP.setArray(1, arrayConfigBanca);
             vectoresInsertSP.setDate(2, sqlDate);
             vectoresInsertSP.registerOutParameter(3,OracleTypes.NUMBER);
             vectoresInsertSP.registerOutParameter(4,OracleTypes.VARCHAR);
             vectoresInsertSP.execute();
             BigDecimal codigoRespuestaSiniMes =(BigDecimal)vectoresInsertSP.getBigDecimal(3);
            String descripcionRespuestaSiniMes = vectoresInsertSP.getString(4);
            int numError = 1;
            BigDecimal errorBD = new BigDecimal(String.valueOf(numError));
            
            if(codigoRespuestaSiniMes == errorBD ){
                throw new Exception("Error al guardar los datos : "+ descripcionRespuestaSiniMes);
            }

		}catch(SQLException sqlEx){
			throw new ApplicationException(eb.addFieldError("guardarCalculosBanca",
                    "Error en la carga de CalculosBanca"+sqlEx));
		}catch(Exception e){
			LOG.info("Informacion del Error : " +e);
		}

	}

	

	
	public TransporteImpresionDTO exportarExcelCalculos(List<CalculosBancaView> listResultCalculosBanca){
		LOG.info(">> exportarExcelCalculos()");
		TransporteImpresionDTO transporteImpresionDTO = null;
		try{
			if(listResultCalculosBanca != null && !listResultCalculosBanca.isEmpty()){
				transporteImpresionDTO = new TransporteImpresionDTO();
				transporteImpresionDTO = this.impresionesService.getExcel(listResultCalculosBanca, "midas.impresion.compensacionadicionales.resultadocalculosbanca.archivo.nombre", ConstantesReporte.TIPO_XLS);
			}
		}catch(Exception ex){
			LOG.error("Información del Error", ex);
		}
		LOG.info("<< exportarExcelCalculos()");
		return transporteImpresionDTO;
	}
	
	
	public List<CaBitacora> getListHistoricoConfiguracion(ConfiguracionContraprestacionBanca configuracionContraprestacionBanca){
		LOG.info(">> getListHistoricoConfiguracion()");
		List<CaBitacora> listBitacora = null;		
		try{
			Long configuracionBancaId = configuracionContraprestacionBanca.getCaConfiguracionBanca().getId();
			listBitacora = this.caBitacoraService.findByProperty(CaBitacoraDaoImpl.CACONFIGURACIONBANCA, configuracionBancaId);			
		}catch(Exception ex){
			LOG.error("Información del Error", ex);
			listBitacora = new ArrayList<CaBitacora>();
		}
		LOG.info("<< getListHistoricoConfiguracion()");
		return listBitacora;
	}
	
	private void guardarBitacoraConfiguracionBanca(boolean isNewRecord, Long configuracionBancaId, String userName){
		LOG.info(">> saveBitacoraConfiguracionBanca()");
		CaBitacora caBitacora = null;
		try{
			caBitacora = new CaBitacora();
			caBitacora.setFecha(new Date());
			caBitacora.setUsuario(userName);
			caBitacora.setConfiguracionBancaId(configuracionBancaId);
			if(isNewRecord){
				caBitacora.setMovimiento(ConstantesCompensacionesAdicionales.CONFIGURACION);
			}else{
				caBitacora.setMovimiento(ConstantesCompensacionesAdicionales.MODIFICACION);				
			}
			this.caBitacoraService.save(caBitacora);
		}catch(Exception ex){
			LOG.error("Información del Error", ex);
		}
		LOG.info("<< saveBitacoraConfiguracionBanca()");		
	}


	 public DataSource getDataSource() {
	        return dataSource;
	    }

}