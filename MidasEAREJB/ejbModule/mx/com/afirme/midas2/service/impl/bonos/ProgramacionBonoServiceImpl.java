package mx.com.afirme.midas2.service.impl.bonos;
import static mx.com.afirme.midas2.utils.CommonUtils.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.bonos.ProgramacionBonoDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.fuerzaventa.CalculoBonoEjecucionesView;
import mx.com.afirme.midas2.dto.fuerzaventa.GenericaAgentesView;
import mx.com.afirme.midas2.service.bonos.ProgramacionBonoService;
import mx.com.afirme.midas2.service.calculos.CalculoBonosService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.impl.jobAgentes.ProgramacionCalculoBonosServiceImpl.conceptoEjecucionAutomatica;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class ProgramacionBonoServiceImpl implements ProgramacionBonoService {
	private static final Integer CALCULO_NEGOCIO = 1;
	private static final Integer CALCULO_CONFIG = 0;
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ProgramacionBonoDao programacionBonoDao;
	
	@EJB
	private ValorCatalogoAgentesService valorCatalogoAgentesService;
	
	@EJB
	private EntidadDao entidadDao;
	
	@EJB
	private CalculoBonosService calculoBonoService;

	@Override
	public List<ProgramacionBono> getProgramacionBonos() {
		List<ProgramacionBono> list = entidadService.findAll(ProgramacionBono.class);
		
		if(list != null && !list.isEmpty()){
			ValorCatalogoAgentes tipoConfiguracionNegocio = new ValorCatalogoAgentes();
			try{
				tipoConfiguracionNegocio = valorCatalogoAgentesService.obtenerElementoEspecifico("Tipo Config Programacion Cliente", "NEGOCIO ESPECIAL");
			}catch(Exception e){
			}
			ValorCatalogoAgentes tipoConfiguracionBono = new ValorCatalogoAgentes();
			try{
				tipoConfiguracionBono = valorCatalogoAgentesService.obtenerElementoEspecifico("Tipo Config Programacion Cliente", "CONFIGURACION BONO");
			}catch(Exception e){
			}
			for(ProgramacionBono item: list){
				if(item.getTipoConfiguracion().getId().equals(tipoConfiguracionNegocio.getId())){
					Negocio negocio = entidadService.findById(Negocio.class, item.getIdBono());
					item.setDescripcionBono(negocio.getDescripcionNegocio());
				}
				if(item.getTipoConfiguracion().getId().equals(tipoConfiguracionBono.getId())){
					ConfigBonos configBonos = entidadService.findById(ConfigBonos.class, item.getIdBono());
					if (configBonos != null) {
						item.setDescripcionBono(configBonos.getDescripcion());
					}
				}
			}
			
			//Sort
			Collections.sort(list, 
					new Comparator<ProgramacionBono>() {				
						public int compare(ProgramacionBono n1, ProgramacionBono n2){
							return n2.getId().compareTo(n1.getId());
						}
			});
		}
		
		return list;
	}
	
	@Override
	public List<GenericaAgentesView> getNegocioEspeciales(Long idTipoBono) throws Exception {
		return programacionBonoDao.getNegocioEspeciales();
	}
	
	@Override
	public List<GenericaAgentesView> getConfiguracionBonos(Long idTipoBono) throws Exception {
		return programacionBonoDao.getConfiguracionBonos(idTipoBono);
	}
	/**
	 * Obtiene el objeto persistido 
	 * @param cat
	 * @return
	 * @throws MidasException
	 */
	private ValorCatalogoAgentes getValorCatalogoAgenteInPersistantContext(ValorCatalogoAgentes cat) throws MidasException{
		ValorCatalogoAgentes persisted=null;
		if(isNotNull(cat) && isNotNull(cat.getId())){
			try {
				persisted=valorCatalogoAgentesService.loadById(cat);
			} catch (Exception e) {
				onError(e.getMessage());
			}
		}
		return persisted;
	}
	
	@Override
	public void saveProgramacionBono(ProgramacionBono programacionBono, List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonoList,Usuario usuario, Boolean ejecutar) throws MidasException{
		ValorCatalogoAgentes claveEstatusInicial = new ValorCatalogoAgentes();
		ValorCatalogoAgentes tipoPeriodoEjecucion=getValorCatalogoAgenteInPersistantContext(programacionBono.getPeriodoEjecucion());
		ValorCatalogoAgentes tipoBono=getValorCatalogoAgenteInPersistantContext(programacionBono.getTipoBono());
		try{
			claveEstatusInicial = valorCatalogoAgentesService.obtenerElementoEspecifico("Estatus Programacion Cliente", "CALCULO PENDIENTE");
		}catch(Exception e){
		}
		if(negociosEspecialesList != null && !negociosEspecialesList.isEmpty()){
			ValorCatalogoAgentes tipoConfiguracionNegocio = new ValorCatalogoAgentes();
			try{
				tipoConfiguracionNegocio = valorCatalogoAgentesService.obtenerElementoEspecifico("Tipo Config Programacion Cliente", "NEGOCIO ESPECIAL");
			}catch(Exception e){
			}
			for(Negocio item : negociosEspecialesList){
				if(item != null){
				ProgramacionBono programacionNeg = new ProgramacionBono();
				programacionNeg.setActivo(1);
				programacionNeg.setClaveEstatus(claveEstatusInicial);
				programacionNeg.setFechaAlta(new Date());
				programacionNeg.setFechaEjecucion(programacionBono.getFechaEjecucion());
				programacionNeg.setHora(programacionBono.getHora());
				programacionNeg.setIdBono(item.getIdToNegocio());
				programacionNeg.setPeriodoEjecucion(tipoPeriodoEjecucion);
				programacionNeg.setTipoBono(tipoBono);
				programacionNeg.setTipoConfiguracion(tipoConfiguracionNegocio);
				programacionNeg.setUsuarioAlta(usuario.getNombreUsuario());
				Long id = null;
				try{
					id = (Long) entidadService.saveAndGetId(programacionNeg);
				}catch(Exception e){
					e.printStackTrace();
				}
				//Desactiva todas las programaciones de ese negocio
				/*List<ProgramacionBono> programacionDesactivarList = new ArrayList<ProgramacionBono>(1);
				try{
					Map<String,Object> params = new LinkedHashMap<String,Object>();
					params.put("tipoConfiguracion.id", tipoConfiguracionNegocio.getId());
					params.put("idBono", item.getIdToNegocio());
					programacionDesactivarList = entidadService.findByProperties(ProgramacionBono.class, params);
					if(programacionDesactivarList != null && !programacionDesactivarList.isEmpty()){
						for(ProgramacionBono desactivar: programacionDesactivarList){
							if(!desactivar.getId().equals(id)){
								desactivar.setActivo(0);
								entidadService.save(desactivar);
							}
						}
					}
				}catch(Exception e){
				}*/
			}//null
			}
		}
		
		if(configuracionBonoList != null && !configuracionBonoList.isEmpty()){
			ValorCatalogoAgentes tipoConfiguracionBono = new ValorCatalogoAgentes();
			try{
				tipoConfiguracionBono = valorCatalogoAgentesService.obtenerElementoEspecifico("Tipo Config Programacion Cliente", "CONFIGURACION BONO");
			}catch(Exception e){
			}
			for(ConfigBonos item : configuracionBonoList){
				if(item != null){
					ProgramacionBono programacionNeg = new ProgramacionBono();
					programacionNeg.setActivo(1);
					programacionNeg.setClaveEstatus(claveEstatusInicial);
					programacionNeg.setFechaAlta(new Date());
					programacionNeg.setFechaEjecucion(programacionBono.getFechaEjecucion());
					programacionNeg.setHora(programacionBono.getHora());
					programacionNeg.setIdBono(item.getId());
					programacionNeg.setPeriodoEjecucion(tipoPeriodoEjecucion);
					programacionNeg.setTipoBono(tipoBono);
					programacionNeg.setTipoConfiguracion(tipoConfiguracionBono);
					programacionNeg.setUsuarioAlta(usuario.getNombreUsuario());
					Long id = null;
					try{
						id = (Long) entidadService.saveAndGetId(programacionNeg);
					}catch(Exception e){
						e.printStackTrace();
					}
					//Desactiva todas las programaciones de ese negocio
					/*List<ProgramacionBono> programacionDesactivarList = new ArrayList<ProgramacionBono>(1);
					try{
						Map<String,Object> params = new LinkedHashMap<String,Object>();
						params.put("tipoConfiguracion.id", tipoConfiguracionBono.getId());
						params.put("idBono", item.getId());
						programacionDesactivarList = entidadService.findByProperties(ProgramacionBono.class, params);
						if(programacionDesactivarList != null && !programacionDesactivarList.isEmpty()){
							for(ProgramacionBono desactivar: programacionDesactivarList){
								if(!desactivar.getId().equals(id)){
									desactivar.setActivo(0);
									entidadService.save(desactivar);
								}
							}
						}
					}catch(Exception e){
					}*/
				}//null
			}
		}
		
		
	}
	
	@Override
	public void activaProgramacionBono(String accion, ProgramacionBono programacionBono){
		if(accion.equals("deleted")){
			programacionBono = entidadService.findById(ProgramacionBono.class, programacionBono.getId());
			entidadDao.executeActionGrid(accion, programacionBono);
		}else{
			Integer activo = programacionBono.getActivo();
			programacionBono = entidadService.findById(ProgramacionBono.class, programacionBono.getId());
			programacionBono.setActivo(activo);
			entidadService.save(programacionBono);
			
			if(activo.intValue() == 1){
				//Desactiva todas las programaciones de ese negocio
				/*List<ProgramacionBono> programacionDesactivarList = new ArrayList<ProgramacionBono>(1);
				try{
					Map<String,Object> params = new LinkedHashMap<String,Object>();
					params.put("tipoConfiguracion.id", programacionBono.getTipoConfiguracion().getId());
					params.put("idBono", programacionBono.getIdBono());
					programacionDesactivarList = entidadService.findByProperties(ProgramacionBono.class, params);
					if(programacionDesactivarList != null && !programacionDesactivarList.isEmpty()){
						for(ProgramacionBono desactivar: programacionDesactivarList){
							if(!desactivar.getId().equals(programacionBono.getId())){
								desactivar.setActivo(0);
								entidadService.save(desactivar);
							}
						}
					}
				}catch(Exception e){
				}*/
			}
			
		}
		
	}
	
	@Override
	public Map<String, Integer> obtenerProgramados(Long idTipoBono){
		Map<String, Integer> bonosProgYPorProg = new HashMap<String, Integer>();
		int bonosProgramados = 0;
		int bonosPorProgramar = 0;
		if(idTipoBono != null){
		try{
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("tipoBono.id", idTipoBono);
			//params.put("activo", 1);
			List<ProgramacionBono> bonosProgramadosList = entidadService.findByProperties(ProgramacionBono.class, params);
			if(bonosProgramadosList != null && !bonosProgramadosList.isEmpty()){
				bonosProgramados = bonosProgramadosList.size();
			}	
			int totalProgramar = 0;
			List<GenericaAgentesView> configuracionesList = programacionBonoDao.getConfiguracionBonos(idTipoBono);
			if(configuracionesList != null && !configuracionesList.isEmpty()){
				totalProgramar += configuracionesList.size();
			}
			if(idTipoBono.equals(getTipoBonoNegocioEspecial().getId())){
				List<GenericaAgentesView> negociosList = programacionBonoDao.getNegocioEspeciales();
				if(negociosList != null && !negociosList.isEmpty()){
					totalProgramar += negociosList.size();
				}	
			}
			bonosPorProgramar = totalProgramar - bonosProgramados;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		}
		bonosProgYPorProg.put("bonosProgramados", bonosProgramados);
		bonosProgYPorProg.put("bonosPorProgramar", bonosPorProgramar);
		return bonosProgYPorProg;
	}
	
	@Override
	public ValorCatalogoAgentes getTipoBonoNegocioEspecial(){
		ValorCatalogoAgentes tipoBonoNegocioEspecial = new ValorCatalogoAgentes();
		try{
			tipoBonoNegocioEspecial = valorCatalogoAgentesService.obtenerElementoEspecifico("Tipos de Bono", "Negocio Especial");
			if(tipoBonoNegocioEspecial == null){
				tipoBonoNegocioEspecial = new ValorCatalogoAgentes();
			}
		}catch(Exception e){
		}
		return tipoBonoNegocioEspecial;
	}

	@Override
	public void calcularBono(List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonosList) {
		if(negociosEspecialesList != null && !negociosEspecialesList.isEmpty()){
			for(Negocio negocio: negociosEspecialesList){
				List<PolizaDTO> polizaList = programacionBonoDao.getProduccionDePolizasNegocioEspecial(negocio);
			}			
		}
		if(configuracionBonosList != null && !configuracionBonosList.isEmpty()){
			for(ConfigBonos config: configuracionBonosList){
				List<PolizaDTO> polizaList = programacionBonoDao.getProduccionDePolizas(config);
				if(config.getModoAplicacion().getValor().toUpperCase().trim().equals("IMPORTE")){
					
				}
				if(config.getModoAplicacion().getValor().toUpperCase().trim().equals("PORCENTAJE")){
					
				}
			}			
		}
	}
	
	public void obtieneMontoDeAplicacion(List<PolizaDTO> polizaList){
		
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void ejecCalcBono(ProgramacionBono programacionBono, List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonoList,
			Usuario usuario, Boolean isManual) throws MidasException{
		calculoBonoService.enviarAListadoEjecucionCalculoBonoManual(programacionBono,negociosEspecialesList,configuracionBonoList,usuario,isManual);	
			
	}
	
	/**
	 * Carga la programacion del bono por id
	 * @param id
	 * @return
	 * @throws MidasException
	 */
	public ProgramacionBono loadById(Long id) throws MidasException{
		return programacionBonoDao.loadById(id);
	}
	
	/**
	 * Carga la programacion del bono por id
	 * @param id
	 * @return
	 * @throws MidasException
	 */
	public ProgramacionBono loadById(ProgramacionBono programacion) throws MidasException{
		return programacionBonoDao.loadById(programacion);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void recalcularBono(Long idCalculo) throws MidasException{
		CalculoBono calc=entidadService.findById(CalculoBono.class, idCalculo);
		ProgramacionBono prog=entidadService.findById(ProgramacionBono.class, calc.getProgramacionBono().getId());
		Integer isNegocio=prog.getTipoBono().getClave().equals("NEGESPE")?1:0;
		calculoBonoService.ejecutarCalculo(prog, calc, calc.getModoEjecucion().getId(), isNegocio, conceptoEjecucionAutomatica.BONOS.getValue());
	}
	@Override
	public String eliminarBonosCero(String ids) throws MidasException {
		return programacionBonoDao.eliminarBonosCero(ids);
	}
	@Override 
	public  List<ConfigBonos> bonoIsVigente(List<ConfigBonos> configuracionBonoList) throws MidasException{
		return programacionBonoDao.bonoIsVigente(configuracionBonoList);
	}
	@Override
	public  List<ConfigBonos> excluirBonoNoVigentes(List<ConfigBonos> listaBonosAExcluir, List<ConfigBonos> configuracionBonoList) throws MidasException{
		return programacionBonoDao.excluirBonoNoVigentes(listaBonosAExcluir, configuracionBonoList);
	}

	@Override
	public List<CalculoBonoEjecucionesView> listaCalculoBonosMonitorEjecucion()	throws MidasException {
		return programacionBonoDao.listaCalculoBonosMonitorEjecucion();
	}
}
