package mx.com.afirme.midas2.service.impl.catalogos;

import java.util.Collection;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

@Stateless
public class EntidadServiceImpl implements EntidadService {

	private EntidadDao entidadDao;

	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	
	@Override
	public <E extends Entidad> Object saveAndGetId(E entity){
		Object id = null;
		if (entity.getKey() != null) {
			entidadDao.update(entity);
			id = entity.getKey();
		} else {
			id = entidadDao.persistAndReturnId(entity);			
		}
		return id;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <E extends Entidad> E save(E entity) {
		
		if (entity.getKey() != null) {
			entidadDao.update(entity);
			return entity;
		} else {
			entidadDao.persist(entity);
			if (entity.getKey() != null) {
				return (E) entidadDao.getReference(entity.getClass(), entity.getKey());
			}else{
				return null;
			}
		}

	}
	
	@Override
	public <E extends Entidad> Collection<? extends E> saveAll(
			Collection<? extends E> entities) {
		for (E entity : entities) {
			save(entity);
		}
		return entities;
	}

	@Override
	public <E extends Entidad> void remove(E entity) {
		entidadDao.remove(entity);

	}
	
	@Override
	public <E extends Entidad> void removeAll(Collection<? extends E> entities) {
		for (E entity : entities) {
			remove(entity);
		}
	}
	
	@Override
	public <E extends Entidad> void detach(E entity) {
		entidadDao.detach(entity);
	}

	@Override
	public <E extends Entidad,K> E findById(Class<E> entityClass, K key) {

		return entidadDao.findById(entityClass,key);
	}
	
	@Override
	public <E extends Entidad, K> E evictAndFindById(Class<E> entityClass, K id) {
		return entidadDao.evictAndFindById(entityClass, id);
	}

	@Override
	public <E extends Entidad> List<E> findAll(Class<E> entityClass) {
		return entidadDao.findAll(entityClass);
	}

	@Override
	public <E extends Entidad,K> E getReference(Class<E> entityClass, K key) {
		return entidadDao.getReference(entityClass, key);
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> entityClass,Map<String, Object> params) {
		return entidadDao.findByProperties(entityClass, params);
	}
	
	@Override
	public <E extends Entidad> Long findByPropertiesCount(Class<E> entityClass,Map<String, Object> params) {
		return entidadDao.findByPropertiesCount(entityClass, params);
	}
	

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> entityClass,Map<String, Object> params, String... orderByAttributes) {
		return entidadDao.findByPropertiesWithOrder(entityClass, params, orderByAttributes);
	}

	@Override
	public <E extends Entidad> List<E> findByProperty(Class<E> entityClass,
			String propertyName, Object value) {
		return entidadDao.findByProperty(entityClass, propertyName, value);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List executeQueryMultipleResult(String query, Map<String,Object> parameters){
		return entidadDao.executeQueryMultipleResult(query, parameters);
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List executeNativeQueryMultipleResult(String query){
		return entidadDao.executeNativeQueryMultipleResult(query);
	}

	@Override
	public <E extends Entidad> void refresh(E entity) {
		entidadDao.refresh(entity);
	}


	@Override
	public <E extends Entidad> List<E> findByColumnsAndProperties(
			Class<E> entityClass,String columns,Map<String, Object> params,
			Map<String,Object> queryParams,StringBuilder queryWhere,StringBuilder queryOrder){
		return entidadDao.findByColumnsAndProperties(entityClass, columns, params, queryParams, queryWhere, queryOrder);
	}
	public <E extends Entidad> Long findByColumnsAndPropertiesCount(
			Class<E> entityClass,Map<String, Object> params,
			Map<String,Object> queryParams,StringBuilder queryWhere,StringBuilder queryOrder){
		return entidadDao.findByColumnsAndPropertiesCount(entityClass, params, queryParams, queryWhere, queryOrder);
	}

	@Override
	public <E extends Entidad> void executeActionGrid(String accion,
			E entity) {
		entidadDao.executeActionGrid(accion, entity);
	}
	
	@Override
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter) {
		return this.findByFilterObject(entityClass, filter, null);
	}
	
	@Override
	public <E extends Entidad, T> List<E> findByFilterObject(
			Class<E> entityClass, T filter, String orderBy){
		return entidadDao.findByFilterObject(entityClass, filter, orderBy);
	}
	
	/**
	 * Gets next value for number property. Useful when entity has an autonumber column as well as an autogenerated id
	 * @param <E>
	 * @param entityClass
	 * @param property
	 * @return
	 */
	public  <E extends Entidad, T>  Long getIncrementedProperty(Class<E> entityClass, String property, T filter){
		return entidadDao.getIncrementedProperty(entityClass, property, filter);
	}
	
	/**
	 * Gets next value for a property. Useful when entity has an autonumber column as well as an autogenerated id or getting the max actual
	 * value for some filters
	 * @param <E>
	 * @param entityClass
	 * @param property
	 * @return
	 */
	public  <E extends Entidad, T> Object getIncrementedProperty(Class<E> entityClass, String property, Map<String, Object> filter){
		return entidadDao.getIncrementedProperty(entityClass, property, filter);
	}
	
	/**
	 * Gets next value for a named sequence. Useful when there is a need for a sequenced value
	 * 
	 * @param sequenceName, use schema in the paramenter. Ex. MIDAS.SEQUENCE
	 * @return
	 */
	public Long getIncrementedSequence(String sequenceName) {
		return entidadDao.getIncrementedSequence(sequenceName);
	}
}
