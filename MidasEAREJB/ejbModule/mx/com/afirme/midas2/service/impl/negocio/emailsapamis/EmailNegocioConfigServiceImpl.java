package mx.com.afirme.midas2.service.impl.negocio.emailsapamis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioConfig;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioAlertasService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioConfigService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioService;

@Stateless
public class EmailNegocioConfigServiceImpl implements EmailNegocioConfigService{
	private EntidadService entidadService;
	private EmailNegocioAlertasService alertasEmailNegocioService;
	private EmailNegocioService emailNegocioService;
	
	@Override
	public EmailNegocioConfig completeObject(EmailNegocioConfig arg0) {
		EmailNegocioConfig retorno = new EmailNegocioConfig(); 
		if(arg0 != null){
			if(arg0.getIdEmailNegocioConfig() < 1){
				retorno = findIdByAttributes(arg0);
			}else{
				if(!validateAttributes(arg0)){
					retorno = findById(arg0.getIdEmailNegocioConfig());
				}
			}
		}else{
			retorno.setIdEmailNegocioConfig(Long.valueOf(-1));
		}
		return retorno;
	}

	@Override
	public List<EmailNegocioConfig> findAll() {
		List<EmailNegocioConfig> retorno = entidadService.findAll(EmailNegocioConfig.class);
		return retorno;
	}

	@Override
	public EmailNegocioConfig findById(Long arg0) {
		EmailNegocioConfig retorno = new EmailNegocioConfig();
		if(arg0 != null && arg0 >= 0){
			retorno = entidadService.findById(EmailNegocioConfig.class, arg0);
		}
		return retorno;
	}

	@Override
	public List<EmailNegocioConfig> findByProperty(String arg0, Object arg1) {
		List<EmailNegocioConfig> emailNegocioAlertasList = new ArrayList<EmailNegocioConfig>();
		if(arg0 != null && !arg0.equals("") && arg1 != null){
			emailNegocioAlertasList = entidadService.findByProperty(EmailNegocioConfig.class, arg0, arg1);
		}
		return emailNegocioAlertasList;
	}

	@Override
	public List<EmailNegocioConfig> findByStatus(boolean arg0) {
		List<EmailNegocioConfig> emailNegocioAlertas = entidadService.findByProperty(EmailNegocioConfig.class, "estatus", arg0?0:1);
		return emailNegocioAlertas;
	}

	@Override
	public EmailNegocioConfig saveObject(EmailNegocioConfig arg0) {
		if(arg0 != null && arg0.getIdEmailNegocioConfig() >= 0){
			arg0.setIdEmailNegocioConfig((Long)entidadService.saveAndGetId(arg0));
		}
		return arg0;
	}
	
	private boolean validateAttributes(EmailNegocioConfig arg0){
		boolean retorno = true;
		if(retorno){
			retorno = arg0.getAlertasEmailNegocio() != null;
		}
		if(retorno){
			retorno = arg0.getEmailNegocio() != null;
		}
		return retorno;
	}

	@Override
	public EmailNegocioConfig findIdByAttributes(EmailNegocioConfig arg0) {
		if(validateAttributes(arg0)){
			arg0.setAlertasEmailNegocio(alertasEmailNegocioService.completeObject(arg0.getAlertasEmailNegocio()));
			arg0.setEmailNegocio(emailNegocioService.completeObject(arg0.getEmailNegocio()));
			if(arg0.getAlertasEmailNegocio().getIdEmailNegocioAlertas() > 0 && arg0.getEmailNegocio().getIdEmailNegocio() > 0){
				Map<String,Object> parametros = new HashMap<String,Object>();
				parametros.put("alertasEmailNegocio", arg0.getAlertasEmailNegocio());
				parametros.put("emailNegocio", arg0.getEmailNegocio());
				List<EmailNegocioConfig> emailList = entidadService.findByProperties(EmailNegocioConfig.class, parametros);
				if(emailList.size()>0){
					arg0.setIdEmailNegocioConfig(emailList.get(0).getIdEmailNegocioConfig());
				}else{
					arg0.setIdEmailNegocioConfig(Long.valueOf(0));
				}
			}else{
				arg0.setIdEmailNegocioConfig(Long.valueOf(-1));
			}
		}else{
			arg0.setIdEmailNegocioConfig(Long.valueOf(-1));
		}
		return arg0;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setAlertasEmailNegocioService(EmailNegocioAlertasService alertasEmailNegocioService) {
		this.alertasEmailNegocioService = alertasEmailNegocioService;
	}

	@EJB
	public void setEmailNegocioService(EmailNegocioService emailNegocioService) {
		this.emailNegocioService = emailNegocioService;
	}
}