package mx.com.afirme.midas2.service.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.fuerzaventa.AgenteMidasDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CentroOperacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ProductoBancarioAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.notificaciones.ConfiguracionNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.DestinatariosNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.MovimientosProcesos;
import mx.com.afirme.midas2.domain.notificaciones.ProcesosAgentes;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.DomicilioView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote.TipoAccionFuerzaVenta;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.fuerzaventa.ProductoBancarioAgenteService;
import mx.com.afirme.midas2.service.notificaciones.NotificacionesService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.MailServiceSupport;

@Stateless
public class AgenteMidasServiceImpl implements AgenteMidasService {
	private AgenteMidasDao agenteMidasDao;
	
	private NotificacionesService notificacionesService;
	private ListadoService listadoService;
	private MailService mailService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ProductoBancarioAgenteService productoBancarioAgenteService;
	
	@EJB
	private ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade;
	
	private static ProductoBancarioAgente productoBancarioAgente;
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	public static final Logger LOG = Logger.getLogger(AgenteMidasServiceImpl.class);
	
//	@EJB
//	private MailService mailServiceRemote;

	@EJB
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	@EJB
	public void setNotificacionesService(NotificacionesService notificacionesService) {
		this.notificacionesService = notificacionesService;
	}

	@EJB
	public void setAgenteMidasDao(AgenteMidasDao agenteMidasDao) {
		this.agenteMidasDao = agenteMidasDao;
	}

	@Override
	public List<Agente> findByFilters(Agente agente) {
		return agenteMidasDao.findByFilters(agente);
	}
	
	@Override
	public List<AgenteView> agentesPorAutorizar(Agente filtroAgente) throws Exception{
		return agenteMidasDao.agentesPorAutorizar(filtroAgente);
	}

	@Override
	public List<Agente> findAgentesByPromotoria(Long arg0) {
		try {
			return agenteMidasDao.findAgentesByPromotoria(arg0);
		} catch (Exception e) {			
			e.printStackTrace();
		}
		return new ArrayList<Agente>();
	}
	@Override
	public List<AgenteView> findByFiltersWithAddressSupport(Agente filtroAgente){
		return agenteMidasDao.findByFiltersWithAddressSupport(filtroAgente);
	}

	@Override
	public Agente saveDatosContablesAgente(Agente agente) throws Exception {		
		return agenteMidasDao.saveDatosContablesAgente(agente);
	}
	@Override
	public Agente saveDatosFiscalesAgente(Agente agente) throws Exception {
		return agenteMidasDao.saveDatosFiscalesAgente(agente);
	}
	@Override
	public Agente saveDatosGeneralesAgente(Agente agente) throws Exception {
		return agenteMidasDao.saveDatosGeneralesAgente(agente);
	}
	@Override
	public Agente saveDomiciliosAgente(Agente agente) throws Exception {
		return agenteMidasDao.saveDomiciliosAgente(agente);
	}
	@Override
	public Agente unsuscribe(Agente agente,String accion) throws Exception {
		return agenteMidasDao.unsuscribe(agente,accion);
	}	
	@Override
	public Agente loadById(Agente agente){
		return agenteMidasDao.loadById(agente);
	}
	
	@Override
	public Agente loadByIdImpresiones(Agente agente){
		return agenteMidasDao.loadByIdImpresiones(agente);
	}
	
	@Override
	public Agente loadByClave(Agente agente){
		return agenteMidasDao.loadByClave(agente);
	}
	
	@Override
	public Agente loadByClaveSolicitudes(Agente agente){
		return agenteMidasDao.loadByClaveSolicitudes(agente);
	}
	
	@Override
	public String[] generateExpedientAgent(Long id) throws Exception {		
		return agenteMidasDao.generateExpedientAgent(id);
	}
	
	public boolean validarClaveAgente(Agente agente)throws Exception{
		return agenteMidasDao.validarClaveAgente(agente);
	}
	
	@Override
	public PersonaSeycosDTO llenarPersonaSeycos(Persona persona) {
		PersonaSeycosDTO personaSeycosDTO = new PersonaSeycosDTO();
		personaSeycosDTO.setIdPersona(persona.getIdPersona());
		personaSeycosDTO.setClaveTipoPersona(persona.getClaveTipoPersona()==null?null:persona.getClaveTipoPersona().shortValue());
		personaSeycosDTO.setNombre(persona.getNombre());
		personaSeycosDTO.setApellidoPaterno(persona.getApellidoPaterno());
		personaSeycosDTO.setApellidoMaterno(persona.getApellidoMaterno());
		personaSeycosDTO.setSexo(persona.getClaveSexo());
		personaSeycosDTO.setCodigoRFC(persona.getRfc());
		personaSeycosDTO.setEmail(persona.getEmail());
		personaSeycosDTO.setCodigoCURP(persona.getCurp());
		personaSeycosDTO.setTelefono(persona.getTelefonoCasa());
		personaSeycosDTO.setEstadoCivil(persona.getClaveEstadoCivil());
		personaSeycosDTO.setFechaNacimiento(persona.getFechaNacimiento());
		personaSeycosDTO.setClavePaisNacimiento(persona.getClaveNacionalidad());
		personaSeycosDTO.setClaveEstadoNacimiento(persona.getClaveEstadoNacimiento());
		personaSeycosDTO.setClaveCiudadNacimiento(persona.getClaveMunicipioNacimiento());
		personaSeycosDTO.setTipoSituacion(persona.getClaveSituacion()==null?null:Short.valueOf(persona.getClaveSituacion()));
		personaSeycosDTO.setClaveSectorFinanciero(persona.getClaveTipoSector());		
		personaSeycosDTO.setDomicilios(persona.getDomicilios());		
		personaSeycosDTO.setRazonSocial(persona.getRazonSocial());
		personaSeycosDTO.setFechaConstitucion(persona.getFechaConstitucion());
		personaSeycosDTO.setTelCasa(persona.getTelefonoCasa());
		personaSeycosDTO.setTelOficina(persona.getTelefonoOficina());
		personaSeycosDTO.setTelFax(persona.getTelefonoFax());
		personaSeycosDTO.setTelCelular(persona.getTelefonoCelular());
		personaSeycosDTO.setNombreContacto(persona.getNombreContacto());
		personaSeycosDTO.setPuestoContacto(persona.getPuestoContacto());
		personaSeycosDTO.setFaceBook(persona.getFacebook());
		personaSeycosDTO.setTwitter(persona.getTwitter());
		personaSeycosDTO.setPaginaWeb(persona.getPaginaWeb());
		personaSeycosDTO.setTelefonosAdicionales(persona.getTelefonosAdicionales());
		personaSeycosDTO.setCorreosAdicionales(persona.getCorreosAdicionales());
		personaSeycosDTO.setIdRepresentante(persona.getIdRepresentante());
		//this.extension = persona.extension;
		personaSeycosDTO.setFechaAlta(persona.getFechaAlta());
		personaSeycosDTO.setFechaBaja(persona.getFechaBaja());
		personaSeycosDTO.setNombreCompleto(persona.getNombreCompleto());
		personaSeycosDTO.setDomicilios(persona.getDomicilios());
		return personaSeycosDTO;
	}

	@Override
	public boolean llenarYGuardarListaEntretenimientosAgente(String entretenimientosAgregados, Agente agenteInternet) {
		return agenteMidasDao.llenarYGuardarListaEntretenimientosAgente(entretenimientosAgregados, agenteInternet);
	}

	@Override
	public boolean llenarYGuardarListaHijosAgente(String hijosAgregados, Agente agenteInternet) {
		return agenteMidasDao.llenarYGuardarListaHijosAgente(hijosAgregados, agenteInternet);
	}

	@Override
	public void llenarYGuardarListaProductosBancarios(String productosAgregados, Agente agenteInternet) {
		try {
			List<String> productosSerializados = new ArrayListNullAware<String>();
			List<ProductoBancarioAgente> productosBancariosOriginales = new ArrayListNullAware<ProductoBancarioAgente>();
			List<ProductoBancarioAgente> productosBancariosActualizados = new ArrayListNullAware<ProductoBancarioAgente>();
			List<ProductoBancarioAgente> productosBancariosAEliminar = new ArrayListNullAware<ProductoBancarioAgente>();
					
			Predicate prodBancAgtePredicate = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					ProductoBancarioAgente obj = (ProductoBancarioAgente)arg0; 
					return AgenteMidasServiceImpl.getProductoBancarioAgente().getId().equals(obj.getId());
				}
			};
			
			productosBancariosOriginales = productoBancarioAgenteService.findByAgente(agenteInternet.getId());
			
			if (productosAgregados != null && !productosAgregados.trim().equals("")) {
				productosSerializados = divideRegistros(productosAgregados, ".|.", 14);
				
				for (String productoSerializado: productosSerializados) {
					productosBancariosActualizados.add(obtieneProdBancAgteDeSerializado(productoSerializado, agenteInternet));
				}
			}
			
			for (ProductoBancarioAgente productoBancarioOriginal : productosBancariosOriginales) {
				
				AgenteMidasServiceImpl.setProductoBancarioAgente(productoBancarioOriginal);
				
				if (productosBancariosActualizados.size() == 0 || !CollectionUtils.exists(productosBancariosActualizados, prodBancAgtePredicate)) {
					productosBancariosAEliminar.add(productoBancarioOriginal);
				}
			}
			
			for (ProductoBancarioAgente productoBancarioActualizado : productosBancariosActualizados) {
				if (!productosBancariosOriginales.contains(productoBancarioActualizado)) {
					entidadService.save(productoBancarioActualizado);
				}
			}
			
			for (ProductoBancarioAgente productoBancarioAEliminar : productosBancariosAEliminar) {
				entidadService.remove(productoBancarioAEliminar);
			}
			
			if (agenteInternet != null && agenteInternet.getIdAgente() != null) {
				replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(agenteInternet, TipoAccionFuerzaVenta.GUARDAR); //para replicar datos del producto
			}
			
		} catch (Exception ex) {
			throw new RuntimeException("Error al guardar el producto bancario");
		}
	}
	
	private List<String> divideRegistros(String cadenaCompleta, String separador, int numeroCampos) {
		
		List<String> registros = new ArrayListNullAware<String>();
		
		if (cadenaCompleta != null && !cadenaCompleta.trim().equals("")) {
			Integer indiceChunk = 0;
			int longitudSeparador = separador.length();
			
			cadenaCompleta = separador + cadenaCompleta;
			
			while (cadenaCompleta.length() > longitudSeparador) {
				
				indiceChunk = 0;
				
				for (int i = 0; i < numeroCampos; i++) {
					indiceChunk = cadenaCompleta.indexOf(separador, indiceChunk + longitudSeparador);
				}
				
				registros.add(cadenaCompleta.substring(longitudSeparador, indiceChunk));
				
				cadenaCompleta = cadenaCompleta.substring(indiceChunk);
			}
		}
		
		return registros;
	}
	
	private ProductoBancarioAgente obtieneProdBancAgteDeSerializado (String productoBancAgteSerializado, Agente agente) {
		
		String[] arrayProducto = productoBancAgteSerializado.split(".\\|.");
		
		ProductoBancarioAgente productoBancarioAgente = new ProductoBancarioAgente();
		productoBancarioAgente.setId((arrayProducto[1] != null && !arrayProducto[1].trim().equals("0"))?Long.valueOf(arrayProducto[1].trim()):null); 
		productoBancarioAgente.setProductoBancario(entidadService.getReference(ProductoBancario.class, (isValid(arrayProducto[3]))?Long.valueOf(arrayProducto[3].trim()):null));
		productoBancarioAgente.setNumeroCuenta((isValid(arrayProducto[6]) && !"N/A".equalsIgnoreCase(arrayProducto[6].trim()))?arrayProducto[6].trim():null);
		productoBancarioAgente.setNumeroClabe((isValid(arrayProducto[7]) && !"N/A".equalsIgnoreCase(arrayProducto[7].trim()))?arrayProducto[7].trim():null);
		productoBancarioAgente.setMontoCredito((isValid(arrayProducto[9]) && !"N/A".equalsIgnoreCase(arrayProducto[9].trim()))?new BigDecimal(arrayProducto[9].trim()):BigDecimal.ZERO);
		productoBancarioAgente.setNumeroPlazos((isValid(arrayProducto[10]) && !"N/A".equalsIgnoreCase(arrayProducto[10].trim()))?new Integer(arrayProducto[10].trim()):0);
		productoBancarioAgente.setMontoPago((isValid(arrayProducto[11]) && !"N/A".equalsIgnoreCase(arrayProducto[11].trim()))?new BigDecimal(arrayProducto[11].trim()):BigDecimal.ZERO);			
		productoBancarioAgente.setSucursal((isValid(arrayProducto[12]))?arrayProducto[12].trim():null);
		productoBancarioAgente.setPlaza((isValid(arrayProducto[13]))?arrayProducto[13].trim():null);
		productoBancarioAgente.setClaveDefault((isValid(arrayProducto[8]) && "Si".equalsIgnoreCase(arrayProducto[8].trim()))?1:0);
		productoBancarioAgente.setAgente(agente);
			
		return productoBancarioAgente;
		
	}
	
	

	@Override
	public void eliminarDocumentosAgente(Long idAgente) {
		agenteMidasDao.eliminarDocumentosAgente(idAgente);		
	}

	@Override
	public void eliminarEntretenimientosAgente(Long idAgente) {
		agenteMidasDao.eliminarEntretenimientosAgente(idAgente);
		
	}

	@Override
	public void eliminarHijosAgente(Long idAgente) {
		agenteMidasDao.eliminarHijosAgente(idAgente);
		
	}
	
	@Override
	public Agente findByClaveAgente(Agente claveAgente) throws Exception {
		return agenteMidasDao.findByClaveAgente(claveAgente);
	}

	public void disable(Agente agente) throws Exception{
		agenteMidasDao.disable(agente);
	}

	@Override
	public void saveInSeycos(Agente agente) throws Exception {
		agenteMidasDao.saveInSeycos(agente);
	}
	@Override
	public Agente saveAltaPorInternet(Agente agente) throws Exception{
		return agenteMidasDao.saveAltaPorInternet(agente);
	}
	
	public List<AgenteView> findByFiltersWithAddressSupportPaging(Agente filtroAgente,Integer pagingCount,Long pagingStart){
		return agenteMidasDao.findByFiltersWithAddressSupportPaging(filtroAgente,pagingCount,pagingStart);
	}
	public Long getMaxRowNumFindByFiltersWithAddressSupport(Agente filtroAgente){
		return agenteMidasDao.getMaxRowNumFindByFiltersWithAddressSupportPaging(filtroAgente);
	}
	@Override
	public List<AgenteView> findByFilterLightWeight(Agente filtroAgente){
		return agenteMidasDao.findByFilterLightWeight(filtroAgente);
	}
	@Override
	public List<AgenteView> findByFilterLightWeight(Map<String, Object> filtroAgente){
		return agenteMidasDao.findByFilterLightWeight(filtroAgente);
	}
	@Override
	public List<String> cargaMasiva(String url) throws Exception{
		return agenteMidasDao.cargaMasiva(url);
	}

	@Override
	public void mailThreadMethodSupport(Long idCalculo, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate,String methodToExcecute) {
		// TODO Auto-generated method stub
		
	}


	@Override
	public synchronized Map<String,Map<String, List<String>>>obtenerCorreos(Long id, Long idProceso,
			Long idMovimiento) {
		if (id == null) {
			return null;
		}
		int count = 0;
		Agente agente = new Agente();
		Map<String,Map<String, List<String>>> correos = new HashMap<String, Map<String,List<String>>>();
		Map<Long, String> modoEnvio = listadoService
				.mapValorCatalogoAgente("Modos de Envio");
		Map<Long, String> tipoDestinatario = listadoService
				.mapValorCatalogoAgente("Tipos de Destinatario");
		List<ConfiguracionNotificaciones> configList = null;
		if (id != null) {
			agente.setId(id);
			agente = loadById(agente);
		}
		// Carga las configuraciones
		ConfiguracionNotificaciones configNotificaciones = new ConfiguracionNotificaciones();
		configNotificaciones.setIdProceso(new ProcesosAgentes(idProceso));
		configNotificaciones.setIdMovimiento(new MovimientosProcesos(idMovimiento));
		configList = notificacionesService.findByFilters(configNotificaciones);
		// Obtiene los correos
		for (ConfiguracionNotificaciones configuracion : configList) {
			Map<String,List<String>> mapCorreos = new HashMap<String, List<String>>();
			List<String> cco = new ArrayList<String>();
			List<String> para = new ArrayList<String>();
			List<String> cc = new ArrayList<String>();
			List<DestinatariosNotificaciones> destinatariosNotificaciones = configuracion
					.getDestinatariosList();
			for (DestinatariosNotificaciones destinatario : destinatariosNotificaciones) {
				if (destinatario.getIdTipoDestinatario() != null) {
					if ("AGENTES".equals(tipoDestinatario.get(destinatario
							.getIdTipoDestinatario().getId()))) {
						MailServiceSupport.clasificaCorreo(agente.getPersona(), destinatario, modoEnvio, para, cc, cco);
					} else if ("PROMOTORIAS".equals(tipoDestinatario
							.get(destinatario.getIdTipoDestinatario().getId()))) {
						MailServiceSupport.clasificaCorreo(agente.getPromotoria().getPersonaResponsable(), destinatario, modoEnvio, para, cc, cco);
					} else if ("EJECUTIVOS DE VENTAS".equals(tipoDestinatario
							.get(destinatario.getIdTipoDestinatario().getId()))) {
						MailServiceSupport.clasificaCorreo(agente.getPromotoria().getEjecutivo().getPersonaResponsable(), destinatario, modoEnvio, para, cc, cco);
					} else if ("OTROS".equals(tipoDestinatario
							.get(destinatario.getIdTipoDestinatario().getId()))) {
						Persona persona = new Persona();
						persona.setEmail(destinatario.getCorreo());
						MailServiceSupport.clasificaCorreo(persona, destinatario, modoEnvio, para, cc, cco);
					}
				}
			}
			if (!para.isEmpty()) {
				mapCorreos.put(GenericMailService.PARA, para);
			}
			if (!cc.isEmpty()) {
				mapCorreos.put(GenericMailService.COPIA, cc);
			}
			if (!cco.isEmpty()) {
				mapCorreos.put(GenericMailService.COPIA_OCULTA, cco);
			}
			if (mapCorreos != null && !mapCorreos.isEmpty()) {
				ArrayList<String> notas = new ArrayList<String>();
				notas.add("<br/>Notas:<br/>"+configuracion.getNotas());
				mapCorreos.put(GenericMailService.NOTA,notas);
				correos.put("config"+count, mapCorreos);
				count++;
			}
		}
		return correos;
	}


	@Override
	public void enviarCorreo(Map<String,Map<String, List<String>>> mapCorreos,String mensaje,String tituloMensaje,int tipoTemplate,List<ByteArrayAttachment> attachment) {
		if (mapCorreos != null && !mapCorreos.isEmpty()) {
			for (Entry<String, Map<String, List<String>>> map : mapCorreos
					.entrySet()) {
				StringBuilder message = null;
				if (StringUtils.isNotBlank(mensaje)) {
					 message = new StringBuilder(mensaje);
				} else {
					message = new StringBuilder();
				}
				
				if (mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0) != null && StringUtils.isNotBlank(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0))) {
					message.append((mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0)!="")?mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0):"");
				}
				mailService.sendMailAgenteNotificacion(
						mapCorreos.get(map.getKey()).get(GenericMailService.PARA), mapCorreos
								.get(map.getKey()).get(GenericMailService.COPIA),
						mapCorreos.get(map.getKey()).get(GenericMailService.COPIA_OCULTA), null,
						message.toString(), null, "Notificacion", null, tipoTemplate);
			}
		}
	}
	
	@Override
	public Agente loadById(Agente agente, String fechaHistorico)
	{
		return agenteMidasDao.loadById(agente, fechaHistorico);		
	}
	
	@Override
	public <E extends Entidad> List<E> findByPropertyWithHistorySupport(Class<E> entityClass, String propertyName,
			final Object value, String fechaHistorico)
	{
		return agenteMidasDao.findByPropertyWithHistorySupport(entityClass, propertyName, value, fechaHistorico);
	}

	@Override
	public Agente findByCodigoUsuario(String codigoUsuario) {
		return agenteMidasDao.findByCodigoUsuario(codigoUsuario);
	}
	
	@Override
	public AgenteView findByCodigoUsuarioLightWeight(String codigoUsuario) {
		Agente agente = new Agente();
		agente.setCodigoUsuario(codigoUsuario);
		List<AgenteView> list = agenteMidasDao.findByFilterLightWeight(agente);
		if (list.size() == 1) {
			return list.get(0);
		}
		return null;
	}

	@Override
	public DomicilioView findDomicilioAgente(Agente agente) throws Exception {
		 return agenteMidasDao.findDomicilioAgente(agente);
	}
	
	@Override
	public void rechazarAgentesVenceDiasEmision() {
		try {
			LOG.info("Ejecutando tarea rechazarAgentesVenceDiasEmision");
			agenteMidasDao.rechazarAgentesVenceDiasEmision();
		} catch(Exception e) {
			LOG.error("Error en rechazarAgentesVenceDiasEmision()...", e);
		}		
	}
	
	public static ProductoBancarioAgente getProductoBancarioAgente() {
		return productoBancarioAgente;
	}
	
	public static void setProductoBancarioAgente(
			ProductoBancarioAgente productoBancarioAgente) {
		AgenteMidasServiceImpl.productoBancarioAgente = productoBancarioAgente;
	}

	@Override
	public Agente getAgenteByIdSucursal(Integer idSucursal) {
		
		return agenteMidasDao.getAgenteByIdSucursal(idSucursal);
	}
	
	@Override
	public boolean isSucursal(Integer idAgente) {
		
		return agenteMidasDao.isSucursal(idAgente);
	}
	
	
	@Override
	public Map<String, String> obtenerDireccionesCorreoSuperioresAgente(Agente agente)
	{
		Map<String, String> correosSuperiores = new HashMap<String, String>();
		
		Promotoria promotoria = agente.getPromotoria();
		Ejecutivo ejecutivo = promotoria.getEjecutivo();
		Gerencia gerencia = ejecutivo.getGerencia();
		CentroOperacion centroOperacion = gerencia.getCentroOperacion();		

		//Promotoria
		if(promotoria != null && promotoria.getCorreoElectronico() != null)
		{
			correosSuperiores.put("PROMOTORIA", promotoria.getCorreoElectronico());			
		}		
		
		//Ejecutivo
		if(ejecutivo != null && ejecutivo.getPersonaResponsable() != null && 
				ejecutivo.getPersonaResponsable().getEmail() != null)
		{
			correosSuperiores.put("EJECUTIVO", ejecutivo.getPersonaResponsable().getEmail());
		}
				
		//Gerencia
		if(gerencia != null && gerencia.getCorreoElectronico() != null)
		{
			correosSuperiores.put("GERENCIA", gerencia.getCorreoElectronico());
		}
				
		//Centro Operacion
		if(centroOperacion != null && centroOperacion.getCorreoElectronico() != null)
		{
			correosSuperiores.put("CENTRO_OPERACION", centroOperacion.getCorreoElectronico());			
		}		
		
		return correosSuperiores;
	}
	
	public void initialize() {
		String timerInfo = "TimerRechazarAgentesAlVencerDiasDeEmision";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 12 * * ?
				expression.minute(0);
				expression.hour(12);
				expression.dayOfMonth("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerRechazarAgentesAlVencerDiasDeEmision", false));
				
				LOG.info("Tarea TimerRechazarAgentesAlVencerDiasDeEmision configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerRechazarAgentesAlVencerDiasDeEmision");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerRechazarAgentesAlVencerDiasDeEmision:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		try {
			rechazarAgentesVenceDiasEmision();
		} catch(Exception e) {
			LOG.error("Error al ejecutar tarea TimerRechazarAgentesAlVencerDiasDeEmision", e);
		}
		
	}
	
	
	/**
	 * Revisa si el agente ha sido autorizado alguna vez desde que fue creado su registro
	 * @param id Surrogate key del agente (id del sistema MIDAS)
	 * @return true si el agente ha sido autorizado alguna vez desde que fue creado su registro, false en caso contrario
	 */
	public Boolean isAgenteAutorizadoPreviamente(Long id) {
				
		return (agenteMidasDao.buscarAgenteAutorizadoEnHistorial(id).intValue() > 0);
		
	}
	
	
	/**
	 * Revisa si el agente es agente promotor
	 * @param id Surrogate key del agente (id del sistema MIDAS)
	 * @return true si el agente es agente promotor, false en caso contrario
	 */
	public boolean isAgentePromotor(Long id) {
		
		Agente agente = entidadService.findById(Agente.class, id);
		
		if (agente.getIdAgente() != null && agente.getPromotoria() != null) {
			
			return (agente.getIdAgente().equals(agente.getPromotoria().getIdAgentePromotor()));
			
		}
		
		return false;
				
	}
	
	
}
