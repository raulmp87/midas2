/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoAutorizacionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoAutorizacion;
import mx.com.afirme.midas2.service.compensaciones.CaTipoAutorizacionService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoAutorizacionServiceImpl  implements CaTipoAutorizacionService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaTipoAutorizacionDao tipoAutorizacioncaDao;

//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoAutorizacionServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoAutorizacion entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoAutorizacion entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoAutorizacioncaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoAutorizacion entity.
	  @param entity CaTipoAutorizacion entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoAutorizacion.class, entity.getId());
	        	tipoAutorizacioncaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoAutorizacion entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoAutorizacion entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoAutorizacion entity to update
	 @return CaTipoAutorizacion the persisted CaTipoAutorizacion entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoAutorizacion update(CaTipoAutorizacion entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoAutorizacion result = tipoAutorizacioncaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoAutorizacion 	::		CaTipoAutorizacionServiceImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaTipoAutorizacion findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoAutorizacionServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoAutorizacion instance = tipoAutorizacioncaDao.findById(id);//entityManager.find(CaTipoAutorizacion.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoAutorizacionServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoAutorizacionServiceImpl	::	findById	::	ERROR	::	",re);
	            throw re;
        }
    }    
    

/**
	 * Find all CaTipoAutorizacion entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoAutorizacion property to query
	  @param value the property value to match
	  	  @return List<CaTipoAutorizacion> found by query
	 */    
    public List<CaTipoAutorizacion> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoAutorizacionServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoAutorizacionServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoAutorizacioncaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoAutorizacionServiceImpl	::	findByProperty	::	ERROR	::	",re);
			throw re;
		}
	}			
	public List<CaTipoAutorizacion> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoAutorizacion> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoAutorizacion> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoAutorizacion> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoAutorizacion entities.
	  	  @return List<CaTipoAutorizacion> all CaTipoAutorizacion entities
	 */	
	public List<CaTipoAutorizacion> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoAutorizacionServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoAutorizacion model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoAutorizacionServiceImpl	::	findAll	::	FIN	::	");
			return tipoAutorizacioncaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoAutorizacionServiceImpl	::	findAll	::	ERROR	::	",re);
			throw re;
		}
	}	
}