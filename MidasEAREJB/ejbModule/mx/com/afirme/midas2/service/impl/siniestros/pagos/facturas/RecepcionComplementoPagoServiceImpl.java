package mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacade;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaDTO;
import mx.com.afirme.midas.siniestro.finanzas.pagos.FacturaPteComplMonitor;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.envioxml.EnvioFacturaDao;
import mx.com.afirme.midas2.dao.siniestros.pagos.facturas.RecepcionFacturaDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.envioxml.EnvioFacturaDet;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionComplementoPagoService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.ws.axosnet.WsValidacionesSATResponse;
import mx.com.afirme.midas2.util.ExcelExporter;
import mx.com.afirme.midas2.util.MailService;

import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Node;
import org.w3c.dom.Element;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.joda.time.LocalDate;


@Stateless
public class RecepcionComplementoPagoServiceImpl implements RecepcionComplementoPagoService {
	
	private static final Logger log = Logger.getLogger(RecepcionComplementoPagoServiceImpl.class);
	private static final String STRING_EMPTY = "";
	public static final int MIDAS_APP_ID = 5;
	public static final String FACTURA_VALIDAR_PDF = "FACTURA_VALIDAR_PDF";
	public static final String ORIGEN_ENVIO_ZIPFACT = "ZIPFACT";
	public static final String ORIGEN_ENVIO_COMP = "COMP";
	public static final String RFC_AFIRME = "RFC_AFIRME";
	private String origenEnvio;
	private Long idLote;
	public static final BigDecimal GRUPO_PARAMETRO_RECEPCION_COMPLEMENTO_PAGO = new BigDecimal(28);
	public static final BigDecimal PARAMETRO_SWITCH_NOTIFICACION_REC_COMP_PAGO_SINIESTROS_AUTOS = new BigDecimal(280020);
	public static final BigDecimal PARAMETRO_CORREOS_REC_COMP_PAGO_SINIESTROS_AUTOS = new BigDecimal(280010);
	public static final BigDecimal PARAMETRO_SWITCH_NOTIFICACION_REC_COMP_PAGO_COMPENSACIONES = new BigDecimal(280050);
	public static final BigDecimal PARAMETRO_CORREOS_REC_COMP_PAGO_COMPENSACIONES = new BigDecimal(280040);
	public static final String TITULO_CORREO="Aviso de Complementos de Egresos pendientes de recibir"; 
	public static final String SALUDO_CORREO="A quien corresponda.";
	
	@EJB
	private RecepcionFacturaDao recepcionFacturaDao;
	
	@EJB
	private EntidadService entidadService;
	
	
	@EJB
	private ParametroGlobalService parametroGlobalService ; 
	
	@EJB
	private RecepcionFacturaService  recepcionFacturaService;
	
	@EJB
	private EnvioFacturaService envioFacturaService;
	
	
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@EJB
	protected MailService mailService;
	
	
	@EJB
	public void setParametroGeneralFacade(ParametroGeneralFacadeRemote parametroGeneralFacade){
		this.parametroGeneralFacade = parametroGeneralFacade;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Long obtenerIdLoteCarga() throws Exception{
		return recepcionFacturaDao.obtenerIdLoteCarga();
	}
	
	public List<FacturaPteComplMonitor> obtenerFacturasPendientesComplementoMonitor(Date fechaPagoInicio, Date fechaPagoFin, String origenEnvio) throws Exception{
		return recepcionFacturaDao.obtenerFacturasPendientesComplementoMonitor(fechaPagoInicio,fechaPagoFin,origenEnvio);
	}

	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<EnvioFactura> procesarFacturasZip(Integer proveedorId, File zipFile, String extensionArchivo,String origenEnvio, Long idLote) throws Exception {
		
		List<EnvioFactura> complementos = new ArrayList<EnvioFactura>();
		List<String> validFileNameList = this.findValidFileNames(zipFile); // ???
		this.origenEnvio = origenEnvio;
		this.idLote= idLote;
		
		log.info("FileName :"+extensionArchivo);
		FileInputStream fis = null;
		try {
			if(extensionArchivo.endsWith("zip")){
				ZipInputStream dis = new ZipInputStream(new FileInputStream(zipFile));
				ZipEntry ze = dis.getNextEntry();
				byte[] buffer = new byte[2048];
				while (ze != null) {
					String fileName = ze.getName();
					
					if( (ze.getName().endsWith(".xml") || ze.getName().endsWith(".XML")) && validFileNameList.contains(fileName.substring(0, fileName.lastIndexOf(".")))){
						int size;
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						while ((size = dis.read(buffer, 0, buffer.length)) != -1) {
							bos.write(buffer, 0, size);
						}
						complementos.addAll(this.procesaXMLComplemento(bos,proveedorId));
					}
					dis.closeEntry();
					ze = dis.getNextEntry();
				}
				dis.closeEntry();
				dis.close();
			}else if(extensionArchivo.endsWith("xml") || extensionArchivo.endsWith("XML")){
				fis = new FileInputStream(zipFile);
				byte[] bytes = new byte[(int) zipFile.length()];
				fis.read(bytes);
				ByteArrayOutputStream bos = new ByteArrayOutputStream( );
				bos.write(bytes);
				complementos.addAll(this.procesaXMLComplemento(bos,proveedorId));
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(),e);
		} catch (IOException e) {
			log.error(e.getMessage(),e);
		} finally {
			IOUtils.closeQuietly(fis);
		}		
		return complementos;

	}
	
	private List<String> findValidFileNames(File zipFile){
		List<String> validFileNameList = new ArrayList<String>();
		List<String> xmlFileNameList = new ArrayList<String>();
		List<String> pdfFileNameList = new ArrayList<String>();
		String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , FACTURA_VALIDAR_PDF);
		Boolean validaPdf = Boolean.FALSE;
		if(value != null && Integer.valueOf(value).intValue() == 1){
			validaPdf = Boolean.TRUE;
		}
		try {
			ZipInputStream dis = new ZipInputStream(new FileInputStream(zipFile));
			ZipEntry ze = dis.getNextEntry();
			while (ze != null) {
				String fileName = ze.getName();
				log.info("ZIP FileName :"+fileName);
				if(fileName.endsWith(".xml") || fileName.endsWith(".XML")){
					xmlFileNameList.add(fileName.substring(0, fileName.lastIndexOf(".")));
				}
				if(validaPdf && fileName.endsWith(".pdf")){
					pdfFileNameList.add(fileName.substring(0, fileName.lastIndexOf(".")));
				}
				dis.closeEntry();
				ze = dis.getNextEntry();
			}
			dis.closeEntry();
			dis.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(validaPdf){
			for(String xmlFile : xmlFileNameList){
				if(pdfFileNameList.contains(xmlFile)){
					validFileNameList.add(xmlFile);
				}
			}
		}else{
			validFileNameList = xmlFileNameList;
		}
		return validFileNameList;
	}
	
	private EnvioFactura validacionesPrevios(ByteArrayOutputStream bos,Integer proveedorId) throws Exception{
		EnvioFactura envioFactura = new EnvioFactura();
		envioFactura.setValid(true); 
		envioFactura.setNumeroErrores(0);
		envioFactura.setIdLote(idLote);

		String rfcEmisorCfdi = STRING_EMPTY;
        String rfcReceptorCfdi = STRING_EMPTY;
        String tipoComprobanteCfdi = STRING_EMPTY;
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new ByteArrayInputStream( bos.toByteArray()));         
        doc.getDocumentElement().normalize();
        Node nroot = doc.getDocumentElement();  
        Element eRoot = (Element)nroot;
        

		tipoComprobanteCfdi =  eRoot.getAttribute("TipoDeComprobante");

		 NodeList nNodos = nroot.getChildNodes();
         
			for (int pos = 0; pos < nNodos.getLength(); pos++) {
				Node nNodo = nNodos.item(pos);
				if (nNodo.getNodeType() == Node.ELEMENT_NODE){
					 Element eElement = (Element) nNodo;
					if (nNodo.getNodeName().equals("cfdi:Emisor")){
						rfcEmisorCfdi = eElement.getAttribute("Rfc");
					}else if (nNodo.getNodeName().equals("cfdi:Receptor")){
						rfcReceptorCfdi = eElement.getAttribute("Rfc");
					}					
				}
			}
			
			if (tipoComprobanteCfdi==null || !tipoComprobanteCfdi.equals("P")){
				envioFactura.setValid(false);
				EnvioFacturaDet detEnvio = new EnvioFacturaDet();
				envioFactura.setNumeroErrores(envioFactura.getNumeroErrores()+1);
				detEnvio.setDescripcionRespuesta("ESTE DOCUMENTO NO ES UNA COMPLEMENTO DE PAGO: TIPO DE COMPROBANTE NO ES VÁLIDO.");
				detEnvio.setTipoRespuesta(2);
				detEnvio.setIdTipoRespuesta(999);
				envioFactura.getRespuestas().add(detEnvio);
			}
			boolean esRfcValido = false;
			if (origenEnvio.equals(ORIGEN_ENVIO_ZIPFACT)){
				esRfcValido = recepcionFacturaService.esValidoRFCPorProveedor(rfcEmisorCfdi, proveedorId);
			}else if (origenEnvio.equals(ORIGEN_ENVIO_COMP)){
				esRfcValido = this.esValidoRFCPorAgente(rfcEmisorCfdi, proveedorId);
			}
			
			if (!esRfcValido){
				envioFactura.setValid(false); 
				EnvioFacturaDet detEnvio = new EnvioFacturaDet();
				envioFactura.setNumeroErrores(envioFactura.getNumeroErrores()+1);
				detEnvio.setDescripcionRespuesta("El RFC del emisor no es válido.");
				detEnvio.setTipoRespuesta(2);
				detEnvio.setIdTipoRespuesta(999);
				envioFactura.getRespuestas().add(detEnvio);
			}
			
			if(envioFactura.isValid()){
				//OBTENIENDO LOS RFCS DE AFIRME CONFIGURADOS EN LA TABLA DE PARAMETROS GLOBALES
				String stringRfcsAfirme = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , RFC_AFIRME);
				String rfcAfirme = "";
				//SI ALGUNO COINCIDE CON EL DE LA FACTURA, SE ENVIA A AXOSNET, SI NO SE ENVIA EL PRIMERO PARA QUE DESPLIEGUE EL ERROR
				String[] rfcsAfirme = stringRfcsAfirme.split(",");
				for(String rfc : rfcsAfirme){
					if(rfc.equalsIgnoreCase(rfcReceptorCfdi)){
						rfcAfirme = rfc;
						break;
					}
				}
				if(rfcAfirme.isEmpty()){
					rfcAfirme = rfcsAfirme[0];
				}
				envioFactura.setComprobante(Arrays.toString(bos.toByteArray()));
				envioFactura.setOrigenEnvio(origenEnvio);
				envioFactura.setIdSociedad(rfcAfirme);
				if (origenEnvio.equals(ORIGEN_ENVIO_ZIPFACT)){
					envioFactura.setIdDepartamento("0003");
				}else if (origenEnvio.equals(ORIGEN_ENVIO_COMP)){
					envioFactura.setIdDepartamento("0007");
				}
				envioFactura = envioFacturaService.saveEnvio(envioFactura, false);
			}	
		return envioFactura;
	}
	@Override
	public List<EnvioFactura> procesaXMLComplemento(ByteArrayOutputStream bos,Integer idProveedor)throws Exception{
		List<EnvioFactura>  complementos = new ArrayList<EnvioFactura>();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss", Locale.ENGLISH);
		
		EnvioFactura envioFactura = this.validacionesPrevios(bos,idProveedor);
        
		if (!envioFactura.isValid()){
			complementos.add(envioFactura);
			return complementos;
		}
		
		DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(new ByteArrayInputStream( bos.toByteArray()));
        
        doc.getDocumentElement().normalize();
        Node nroot = doc.getDocumentElement();
        
        Map<String,Node> nodosComprobante = obtenerNodosElemento(nroot);
        Map<String,Node> nodosComplemento = obtenerNodosElemento(nodosComprobante.get("cfdi:Complemento"));
        List<Node> nodosPago = obtenerListaNodosElemento(nodosComplemento.get("pago10:Pagos"));
        
        Element comprobante = (Element)nroot;
        Element emisor = (Element)nodosComprobante.get("cfdi:Emisor");
        Element receptor = (Element)nodosComprobante.get("cfdi:Receptor");
        Element timbreFiscal = (Element)nodosComplemento.get("tfd:TimbreFiscalDigital");

		envioFactura.setUuidCfdi(timbreFiscal.getAttribute("UUID"));
        if (!esComplementoNoProcesado(envioFactura).isValid()){
            complementos.add(envioFactura);
            return complementos;
        }
        
        for(int p = 0; p < nodosPago.size(); p ++){
            EnvioFactura complemento = new EnvioFactura();
            Element pago = (Element)nodosPago.get(p);
            Element doctoRelacionado = (Element)obtenerListaNodosElemento(nodosPago.get(p)).get(0);
            
            //Verificar que se copien todos los datos necesarios de envioFactura
            complemento.setIdOrigenEnvio(envioFactura.getIdOrigenEnvio());
            complemento.setOrigenEnvio(envioFactura.getOrigenEnvio());
            complemento.setCuentaAfectada(envioFactura.getCuentaAfectada());

            complemento.setNumeroErrores(0);
            complemento.setValid(true);
            //
            
            complemento.setFechaEnvio(new Date());
            complemento.setRfcEmisorCfdi(emisor.getAttribute("Rfc"));
			complemento.setRfcReceptorCfdi(receptor.getAttribute("Rfc"));
			complemento.setTipoComprobante(comprobante.getAttribute("TipoDeComprobante"));
			complemento.setFolioCfdi(comprobante.getAttribute("Folio"));
			complemento.setFechaCfdi(comprobante.getAttribute("Fecha"));
			complemento.setMonedaCfdi(comprobante.getAttribute("Moneda"));
			complemento.setSerieCfdi(comprobante.getAttribute("Serie"));						                       
			complemento.setTotal(comprobante.getAttribute("Total"));
            complemento.setUuidCfdi(timbreFiscal.getAttribute("UUID"));
            complemento.setNomedaPago(pago.getAttribute("MonedaP"));
            complemento.setFormaPagoPago(pago.getAttribute("FormaDePagoP"));
            complemento.setFechaPagoP(dateFormat.parse(pago.getAttribute("FechaPago")));
            complemento.setUuidDocRelacionado(doctoRelacionado.getAttribute("IdDocumento"));
            complemento.setIdLote(idLote);
			
            String tipoCambioP =pago.getAttribute("TipoCambioP");
            String parcialidadPago = doctoRelacionado.getAttribute("NumParcialidad");
            String saldoInsolutoPago = doctoRelacionado.getAttribute("ImpSaldoInsoluto");
            String saldoAntPago = doctoRelacionado.getAttribute("ImpSaldoAnt");
            String totalPago = doctoRelacionado.getAttribute("ImpPagado");

            tipoCambioP = UtileriasWeb.isNumeric(tipoCambioP)? tipoCambioP:"1";
            parcialidadPago = parcialidadPago!=null? parcialidadPago:null;
            saldoInsolutoPago = UtileriasWeb.isDecimalValido(saldoInsolutoPago,999999999,2)? saldoInsolutoPago : "0";
            saldoAntPago = UtileriasWeb.isDecimalValido(saldoAntPago,999999999,2)? saldoAntPago : "0";
            totalPago = UtileriasWeb.isDecimalValido(totalPago,999999999,2)? totalPago : "0";
                    
            complemento.setTipoCambioPago(Float.valueOf(tipoCambioP));
            complemento.setNumeroParcialidad(Integer.valueOf(parcialidadPago));
            complemento.setImporteSaldoInsoltulo(new BigDecimal(saldoInsolutoPago));
            complemento.setImporteSaldoAnt(new BigDecimal (saldoAntPago) );
            complemento.setImportePagado(new BigDecimal(totalPago));

            if( validacionesNegocioComplementoPago(complemento,idProveedor).isValid()){								                         
                envioFacturaService.guardaDatosEnvio(complemento);
            }
            complementos.add(complemento);	
         }
		return complementos;
	}
	
	
		
	private EnvioFactura validacionesNegocioComplementoPago (EnvioFactura complementoPago,Integer idProveedor){
		complementoPago.setValid(false);		
		try{			
			if(this.existeFacturaComplemento(complementoPago).isValid()){
				if (validarImporPagoFactura(complementoPago,idProveedor).isValid()) {
					complementoPago.setValid(true);
				}else{
					EnvioFacturaDet detEnvio = new EnvioFacturaDet();
					complementoPago.setNumeroErrores(complementoPago.getNumeroErrores()+1);
					detEnvio.setDescripcionRespuesta("EL IMPORTE DEL PAGO ES INVALIDO");
					detEnvio.setTipoRespuesta(2);
					detEnvio.setIdTipoRespuesta(999);
					complementoPago.getRespuestas().add(detEnvio);
				}
			}else{
				EnvioFacturaDet detEnvio = new EnvioFacturaDet();
				complementoPago.setNumeroErrores(complementoPago.getNumeroErrores()+1);
				detEnvio.setDescripcionRespuesta("NO SE ENCONTRO LA FACTURA PAGADA Y CON PENDIENTE DE SUBIR COMPLEMENTO");
				detEnvio.setTipoRespuesta(2);
				detEnvio.setIdTipoRespuesta(999);
				complementoPago.getRespuestas().add(detEnvio);
			}
		}catch(Exception ex){
			complementoPago.setValid(false);
			complementoPago.setNumeroErrores(complementoPago.getNumeroErrores()+1);
		}
		return complementoPago;
	}
	
	
	private EnvioFactura esComplementoNoProcesado(EnvioFactura complementoPago){
		complementoPago.setValid(true);
		if (complementoPago.getUuidCfdi()==null ||complementoPago.getUuidCfdi().trim().equals("") ){
			complementoPago.setValid(false);
		}else{
			for (EnvioFactura env : envioFacturaService.findByProperty("uuidCfdi",complementoPago.getUuidCfdi())) {
				if (env.getEstatusEnvio().equals(1)){
					complementoPago.setValid(false);
					break;
				}
			} 
		}
		
		if(!complementoPago.isValid()){
			EnvioFacturaDet detEnvio = new EnvioFacturaDet();
			complementoPago.setNumeroErrores(complementoPago.getNumeroErrores()+1);
			detEnvio.setDescripcionRespuesta("EL COMPLEMENTO DE PAGO YA SE ENCUENTRA REGISTRADO");
			detEnvio.setTipoRespuesta(2);
			detEnvio.setIdTipoRespuesta(999);
			complementoPago.getRespuestas().add(detEnvio);
		}
		return complementoPago;
	}
	private EnvioFactura existeFacturaComplemento(EnvioFactura complementoPago){
		complementoPago.setValid(false);
		for (EnvioFactura env : envioFacturaService.findByProperty("uuidCfdi",complementoPago.getUuidDocRelacionado())) {
			if (env.getEstatusEnvio().equals(1) && env.getTipoComprobante().equals("I")){
				complementoPago.setValid(true);
				complementoPago.setIdOrigenEnvio( env.getIdOrigenEnvio());
				complementoPago.setOrigenEnvio(env.getOrigenEnvio());
				break;
			}
		} 
		return complementoPago;
	}
	
	
	private EnvioFactura validarImporPagoFactura(EnvioFactura complementoPago, Integer idProveedor)throws Exception{
		complementoPago.setValid(false);
		List<FacturaDTO> facturas = new ArrayList<FacturaDTO>();
		if (origenEnvio.equals(ORIGEN_ENVIO_ZIPFACT)){
			facturas = recepcionFacturaService.obtenerFacturasPendientesComplementoProveedor(idProveedor,origenEnvio);

		}else if (origenEnvio.equals(ORIGEN_ENVIO_COMP)){
			facturas = recepcionFacturaService.obtenerFacturasPendientesComplementoAgente(idProveedor);

		}
		for (FacturaDTO fact : facturas) {
			if (fact.getUuidCfdi().equals(complementoPago.getUuidDocRelacionado())) {
				BigDecimal totalPagado = complementoPago.getImportePagado().multiply(new BigDecimal(complementoPago.getTipoCambioPago()));
				int rCompare = fact.getImporteRestante().compareTo(totalPagado);
				if (rCompare >= 0){
					complementoPago.setValid(true);										
				}
			}
		}
		return complementoPago;
	}

	@Override
	public Boolean esValidoRFCPorAgente(String rfc,Integer agenteId)throws Exception{
		
		Boolean esValido = Boolean.FALSE;
		Agente agente = this.entidadService.findByProperty(Agente.class, "idAgente", (long)agenteId).get(0);
		String rfcPreestador = agente.getPersona().getRfc();
		if(rfcPreestador.equals(rfc.trim())){
			esValido = Boolean.TRUE;
		}
		return esValido;
	}
	
	public Map<String,Node> obtenerNodosElemento(Node nroot){

        HashMap<String,Node> elementos = new HashMap<String,Node>();
        NodeList nNodos = nroot.getChildNodes();  

		for (int n = 0; n < nNodos.getLength(); n++) {
			Node nNodo = nNodos.item(n);
			if (nNodo.getNodeType() == Node.ELEMENT_NODE){
                elementos.put(nNodo.getNodeName(),nNodo);
            }
        }
        return elementos;
    }

    public List<Node> obtenerListaNodosElemento(Node nroot){

        List<Node> elementos = new ArrayList<Node>();
        NodeList nNodos = nroot.getChildNodes();  

		for (int n = 0; n < nNodos.getLength(); n++) {
			Node nNodo = nNodos.item(n);
			if (nNodo.getNodeType() == Node.ELEMENT_NODE){
                elementos.add(nNodo);
            }
        }
        return elementos;
    }
	
	@Override
	@Schedule(dayOfMonth = "Last", persistent = false)
	public void notificaRecepcionComplementoPago(String origenEnvio) throws Exception {
		log.info("Inicia Aviso de Complementos de Egresos pendientes de recibir");
		String switchTimbrado = "";
		String cadenaCorreos = "";
		this.origenEnvio = origenEnvio;

		if (origenEnvio.equals(ORIGEN_ENVIO_ZIPFACT)){
			switchTimbrado = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_RECEPCION_COMPLEMENTO_PAGO,PARAMETRO_SWITCH_NOTIFICACION_REC_COMP_PAGO_SINIESTROS_AUTOS),true).getValor();
		}else if (origenEnvio.equals(ORIGEN_ENVIO_COMP)){
			switchTimbrado = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_RECEPCION_COMPLEMENTO_PAGO,PARAMETRO_SWITCH_NOTIFICACION_REC_COMP_PAGO_COMPENSACIONES),true).getValor();
		}

		if (Integer.parseInt(switchTimbrado) == 1) {
			if (origenEnvio.equals(ORIGEN_ENVIO_ZIPFACT)){
				cadenaCorreos = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_RECEPCION_COMPLEMENTO_PAGO,PARAMETRO_CORREOS_REC_COMP_PAGO_SINIESTROS_AUTOS),true).getValor();
			}else if (origenEnvio.equals(ORIGEN_ENVIO_COMP)){
				cadenaCorreos = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_RECEPCION_COMPLEMENTO_PAGO,PARAMETRO_CORREOS_REC_COMP_PAGO_COMPENSACIONES),true).getValor();
			}
			String msg = "";
			List<ByteArrayAttachment> attachment = getAttachment();
			
			List<String> correos = new ArrayList<String>();
			for (String correo : cadenaCorreos.split(";")) {
				correos.add(correo);
			}
			mailService.sendMail(correos, TITULO_CORREO, msg, attachment,TITULO_CORREO, SALUDO_CORREO);
		}else{
			log.info("Servicio de notificacion apagado");
		}
		log.info("Termina Aviso Complementos de Egresos pendientes de recibir");
	}
	

	@SuppressWarnings("deprecation")
	private List<ByteArrayAttachment> getAttachment() throws Exception{
		List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
		String nombreArchivo = "";
		Date fechaFin = new Date();
		Date fechainicio = new Date(fechaFin.getYear(),fechaFin.getMonth(),1);
			
		List<FacturaPteComplMonitor> resultList = recepcionFacturaDao.obtenerFacturasPendientesComplementoNotificacion(fechainicio, fechaFin, origenEnvio);
		ExcelExporter exporter = new ExcelExporter(FacturaPteComplMonitor.class);
		
		if (origenEnvio.equals(ORIGEN_ENVIO_ZIPFACT)){
			nombreArchivo = "Lista_Complementos_Egresos_Ptes_Recibir_Autos";
		}
		
		TransporteImpresionDTO transporte = exporter.exportXLS(resultList, nombreArchivo);
		
		ByteArrayAttachment arrayAttachment = new ByteArrayAttachment();
		arrayAttachment.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
		arrayAttachment.setContenidoArchivo(IOUtils.toByteArray(transporte.getGenericInputStream()));
		arrayAttachment.setNombreArchivo(nombreArchivo+".xls");
		attachment.add(arrayAttachment);

		return attachment;
	}
		
	
}
