package mx.com.afirme.midas2.service.impl.siniestros.solicitudcheque;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.siniestros.solicitudcheque.SolicitudChequeDao;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro.OrigenSolicitud;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestroDetalle;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.solicitudcheque.SolicitudChequeService;

import org.apache.log4j.Logger;

@Stateless
public class SolicitudChequeServiceImpl implements SolicitudChequeService{
	public  static final Logger log = Logger.getLogger(SolicitudChequeServiceImpl.class);
	@EJB
	private SolicitudChequeDao solicitudChequeDao;
	@EJB
	private EntidadService entidadService;
	

	@Override
	public void migrarChequeMizar(Long idSolCheque) throws Exception {
		SolicitudChequeSiniestro solicitudCheque =entidadService.findById(SolicitudChequeSiniestro.class, idSolCheque);
		if(solicitudCheque==null){
			throw new Exception(String.format("No existe el cheque a migrar."));
		}
		List<SolicitudChequeSiniestroDetalle> lstDetalleCheque = this.entidadService.findByProperty(SolicitudChequeSiniestroDetalle.class, "solicitudChequeSiniestro.id", solicitudCheque.getId());
		Long idCalculo=solicitudCheque.getId();
		try{
			solicitudChequeDao.actualizarSolitudChequeMizar(solicitudCheque);
		}catch (Exception e){
			throw new Exception(String.format("Error al actualizar la solitud de Cheque a Mizar"));
		}
		try{
			for(SolicitudChequeSiniestroDetalle cheque: lstDetalleCheque){
				solicitudChequeDao.actualizarDetalleSolicitudChequeMizar(cheque, solicitudCheque.getIdSolCheque(),idCalculo,new Long(0),cheque.getPctIva(), cheque.getPctIvaR(), cheque.getPctIsr(), cheque.getTipoOperacion());

			}
		}catch (Exception e){
			throw new Exception(String.format("Error al actualizar el Detalle de Solicitud de Cheque a Mizar"));
		}
		for(SolicitudChequeSiniestroDetalle cheque: lstDetalleCheque){
			solicitudChequeDao.importarSolicitudesChequesAMizar(idCalculo);
		}		
		
	}

	@Override
	public SolicitudChequeSiniestro solicitarChequeMizar(Long identificador,
			OrigenSolicitud tipo) throws Exception {
		return solicitudChequeDao.solicitarChequeMizar(identificador, tipo);
	}

}