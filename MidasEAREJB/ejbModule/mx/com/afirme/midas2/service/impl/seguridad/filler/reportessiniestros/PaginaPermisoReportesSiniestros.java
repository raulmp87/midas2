package mx.com.afirme.midas2.service.impl.seguridad.filler.reportessiniestros;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoReportesSiniestros {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	private final int AG = 0;
	private final int AC = 1;
	private final int BR = 2;
	private final int RE = 3;
	private final int EX = 4;
	private final int AD = 5;
	private final int VD = 6;
	private final int AS = 7;
	private final int BU = 8;
	private final int CO = 9;
	private final int CT = 10;
	private final int GU = 11;
	private final int NV = 12;
	private final int SE = 14;
	
	public PaginaPermisoReportesSiniestros(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		PaginaPermiso pp;
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("reporteSiniestralidad.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestralidad.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("reporteSiniestralidadAnexo.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestralidadAnexo.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSiniestralidadYAnexo.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("reporteSiniestrosRRCSONORv7.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestrosRRCSONORv7.do"));
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSiniestrosRRCSONORv7.do")); 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/reportes/mostrarReporteSPSinParametros.do"));  
		pp.getPermisos().add(listaPermiso.get(VD));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/cargarReporteSPFechas.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSPFechas.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
		
		return this.listaPaginaPermiso;
	}
	
}
