package mx.com.afirme.midas2.service.impl.siniestros.reportecondiciones;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.siniestros.reportecondiciones.ReporteCondicionesSiniestroDao;
import mx.com.afirme.midas2.dto.siniestros.reportecondiciones.ReporteCondicionesSiniestroDTO;
import mx.com.afirme.midas2.service.siniestros.reportecondiciones.ReporteCondicionesSiniestroService;

import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFFont;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.IndexedColors;

@Stateless
public class ReporteCondicionesSiniestroServiceImpl implements ReporteCondicionesSiniestroService {

	private static final short	TAMAÑO_FUENTE	= 10;

	private static final short	MONEY_FORMAT	= 8;

	private static final int	COLUMNA_INICIO	= 3;

	@EJB
	private ReporteCondicionesSiniestroDao reporteCondicionesSiniestroDao;

	private CellStyle headerStyle;
	private HSSFWorkbook workbook;
	private int generalDataRows;
	private int detailDataRows;
	private HSSFSheet generalSheet;
	private HSSFSheet detailSheet;
	
	
	@Override
	public InputStream generaReporte(String numeroPoliza, Short tipoPoliza,
			Long idGerencia, Long idCondicionEspecial, Date fechaSiniestroIni,
			Date fechaSiniestroFin) {

		ByteArrayOutputStream bos  = null;
		generalDataRows = 0;
		detailDataRows = 0;
		
		try {
			workbook = new HSSFWorkbook();
			
			List<ReporteCondicionesSiniestroDTO>  lstGeneralReportInfo = reporteCondicionesSiniestroDao.obtenerCondicionesSiniestro(
					numeroPoliza, tipoPoliza, idGerencia, idCondicionEspecial, fechaSiniestroIni, fechaSiniestroFin);
			
			List<ReporteCondicionesSiniestroDTO>  lstDetailReportInfo = reporteCondicionesSiniestroDao.obtenerCondicionesPolizaSiniestro(
					numeroPoliza, tipoPoliza, idGerencia, idCondicionEspecial, fechaSiniestroIni, fechaSiniestroFin);
			
			if (lstGeneralReportInfo != null && !lstGeneralReportInfo.isEmpty() 
					&& lstDetailReportInfo != null && !lstDetailReportInfo.isEmpty()) {		
				
				if (lstGeneralReportInfo.get(0).getNumeroPolizas() == 0) {
					return null;
				}
	
				createHeaderStyle(workbook);
	
				generalSheet = workbook.createSheet("General");		
						
				HSSFRow generalRow = generalSheet.createRow(generalDataRows);	
				
				printGeneralHeaderColumns(generalRow);
				
				generalDataRows++;	
				
				for (ReporteCondicionesSiniestroDTO rowInfo: lstGeneralReportInfo) {
					generalRow = generalSheet.createRow(generalDataRows);
					
					if (generalDataRows == 1) {
						generalDataRows++;
						printGeneralHeaderSection(generalRow, rowInfo);
					} else {
						generalDataRows++;
						printGeneralInfoSection(generalRow, rowInfo);
					}				
				}			
				
				detailSheet = workbook.createSheet("Detalle");
				
				HSSFRow detailRow = detailSheet.createRow(detailDataRows);	
				
				printDetailHeaderColumns(detailRow);
				
				detailDataRows++;
				
				for (ReporteCondicionesSiniestroDTO rowInfo: lstDetailReportInfo) {
					detailRow = detailSheet.createRow(detailDataRows);	
					
					if (detailDataRows == 1) {
						detailDataRows++;
						printDetailHeaderSection(detailRow, rowInfo);
					} else {
						detailDataRows++;
						printDetailInfoSection(detailRow, rowInfo);
					}				
				}		
				
				workbook.setActiveSheet(0);
				bos = new ByteArrayOutputStream();
	
				workbook.write(bos);
			} else {
				return null;
			}
			
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (bos != null) {
				try {
					bos.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}

		if (bos == null){
			return null;
		}
		
		return new ByteArrayInputStream(bos.toByteArray());

	}
	
	private void printGeneralInfoSection(HSSFRow row, ReporteCondicionesSiniestroDTO rowInfo) {
		int column = 0;
		printStringValueCell(row, rowInfo.getCodigoCondicion(), column);
		generalSheet.autoSizeColumn(column);
		printStringValueCell(row, rowInfo.getNombreCondicion(), ++column);
		generalSheet.autoSizeColumn(column);
		printFormulaPercentageCell(row, "IF(H" + generalDataRows + "=0,0,D" + generalDataRows + " /H" + generalDataRows + ")", ++column, false);
		generalSheet.autoSizeColumn(column);
		printMoneyValueCell(row, rowInfo.getCostoSiniestros() != null ? rowInfo.getCostoSiniestros().doubleValue(): 0, ++column, false);
		generalSheet.autoSizeColumn(column);
		printFormulaPercentageCell(row, "IF($D$2=0,0,D" + generalDataRows + " /$D$2)", ++column, false);
		generalSheet.autoSizeColumn(column);
		printDoubleValueCell(row, rowInfo.getNumeroSiniestros() != null ? rowInfo.getNumeroSiniestros().doubleValue(): 0, ++column, false);
		generalSheet.autoSizeColumn(column);
		printFormulaPercentageCell(row, "IF($F$2=0,0,F" + generalDataRows + " /$F$2)", ++column, false);
		generalSheet.autoSizeColumn(column);
		printMoneyValueCell(row, rowInfo.getPrimaDevengada() != null ? rowInfo.getPrimaDevengada().doubleValue(): 0, ++column, false);
		generalSheet.autoSizeColumn(column);
		printDoubleValueCell(row, rowInfo.getNumeroPolizas()!= null ? rowInfo.getNumeroPolizas().doubleValue(): 0, ++column, false);
		generalSheet.autoSizeColumn(column);
		
	}
	
	private void printGeneralHeaderSection(HSSFRow row, ReporteCondicionesSiniestroDTO headerInfo) {
		int column = 2;
		printFormulaPercentageCell(row, "IF(H2=0,0,D2/H2)", column, true);
		printMoneyValueCell(row, headerInfo.getCostoSiniestros() != null ? headerInfo.getCostoSiniestros().doubleValue(): 0, ++column, true);
		printPercentageValueCell(row, headerInfo.getPorcPartCosto() != null ? headerInfo.getPorcPartCosto().doubleValue(): 0, ++column, true);
		printDoubleValueCell(row, headerInfo.getNumeroSiniestros() != null ? headerInfo.getNumeroSiniestros().doubleValue(): 0, ++column, true);
		printPercentageValueCell(row, headerInfo.getPorcPartNumSiniestros() != null ? headerInfo.getPorcPartNumSiniestros().doubleValue(): 0, ++column, true);
		printMoneyValueCell(row, headerInfo.getPrimaDevengada() != null ? headerInfo.getPrimaDevengada().doubleValue(): 0, ++column, true);
		printDoubleValueCell(row, headerInfo.getNumeroPolizas()!= null ? headerInfo.getNumeroPolizas().doubleValue(): 0, ++column, true);
	}
	
	private void printGeneralHeaderColumns(HSSFRow row) {
		int column = 0;
		printGeneralHeaderCell(row, "# de Condición Especial", column++);
		printGeneralHeaderCell(row, "Condición Especial", column++);
		printGeneralHeaderCell(row, "Siniestralidad", column++);
		printGeneralHeaderCell(row, "Costo de Siniestros", column++);
		printGeneralHeaderCell(row, "% de Part. en Costo de Siniestros", column++);
		printGeneralHeaderCell(row, "# de Siniestros", column++);
		printGeneralHeaderCell(row, "% de Part. en # de Siniestros", column++);
		printGeneralHeaderCell(row, "Prima Devengada", column++);
		printGeneralHeaderCell(row, "# de Pólizas", column++);
	}
	
	
	private void printDetailInfoSection(HSSFRow row, ReporteCondicionesSiniestroDTO rowInfo) {
		int column = 0;
		printStringValueCell(row, rowInfo.getNumeroPoliza(), column);
		detailSheet.autoSizeColumn(column);
		printStringValueCell(row, rowInfo.getCodigoCondicion(), ++column);
		detailSheet.autoSizeColumn(column);
		printStringValueCell(row, rowInfo.getNombreCondicion(), ++column);
		detailSheet.autoSizeColumn(column);
		printFormulaPercentageCell(row,  "IF(I" + detailDataRows + "=0,0,E" + detailDataRows + " /I" + detailDataRows + ")", ++column, false);
		detailSheet.autoSizeColumn(column);
		printMoneyValueCell(row, rowInfo.getCostoSiniestros() != null ? rowInfo.getCostoSiniestros().doubleValue(): 0, ++column, false);
		detailSheet.autoSizeColumn(column);
		printFormulaPercentageCell(row,"IF($E$2=0,0,E" + detailDataRows + " /$E$2)", ++column, false);
		detailSheet.autoSizeColumn(column);
		printDoubleValueCell(row, rowInfo.getNumeroSiniestros() != null ? rowInfo.getNumeroSiniestros().doubleValue(): 0, ++column, false);
		detailSheet.autoSizeColumn(column);
		printFormulaPercentageCell(row, "IF($G$2=0,0,G" + detailDataRows + " /$G$2)", ++column, false);
		detailSheet.autoSizeColumn(column);
		printMoneyValueCell(row, rowInfo.getPrimaDevengada() != null ? rowInfo.getPrimaDevengada().doubleValue(): 0, ++column, false);
		detailSheet.autoSizeColumn(column);
		
	}
	
	private void printDetailHeaderSection(HSSFRow row, ReporteCondicionesSiniestroDTO headerInfo) {
		int column = COLUMNA_INICIO;
		printFormulaPercentageCell(row, "IF(I2=0,0,E2/I2)", column, true);
		printMoneyValueCell(row, headerInfo.getCostoSiniestros() != null ? headerInfo.getCostoSiniestros().doubleValue(): 0, ++column, true);
		printPercentageValueCell(row, headerInfo.getPorcPartCosto() != null ? headerInfo.getPorcPartCosto().doubleValue(): 0, ++column, true);
		printDoubleValueCell(row, headerInfo.getNumeroSiniestros() != null ? headerInfo.getNumeroSiniestros().doubleValue(): 0, ++column, true);
		printPercentageValueCell(row, headerInfo.getPorcPartNumSiniestros() != null ? headerInfo.getPorcPartNumSiniestros().doubleValue(): 0, ++column, true);
		printMoneyValueCell(row, headerInfo.getPrimaDevengada() != null ? headerInfo.getPrimaDevengada().doubleValue(): 0, ++column, true);
	}
	
	private void printDetailHeaderColumns(HSSFRow row) {
		int column = 0;
		printDetailHeaderCell(row, "Póliza", column++);
		printDetailHeaderCell(row, "# de Condición Especial", column++);
		printDetailHeaderCell(row, "Condición Especial", column++);
		printDetailHeaderCell(row, "Siniestralidad", column++);
		printDetailHeaderCell(row, "Costo de Siniestros", column++);
		printDetailHeaderCell(row, "% de Part. en Costo de Siniestros", column++);
		printDetailHeaderCell(row, "# de Siniestros", column++);
		printDetailHeaderCell(row, "% de Part. en # de Siniestros", column++);
		printDetailHeaderCell(row, "Prima Devengada", column++);
	}
	
	private HSSFCell printStringValueCell(HSSFRow row, String value, int index) {
		HSSFCell cell = row.createCell(index);
		cell.setCellValue(value);
		return cell;
	}
	
	private HSSFCell printDoubleValueCell(HSSFRow row, double value, int index, boolean isHeader) {
		HSSFCell cell = row.createCell(index);
		
		CellStyle style = workbook.createCellStyle();
		
		if (isHeader) {
			style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
			style.setFillPattern(CellStyle.SOLID_FOREGROUND);
			cell.setCellStyle(style);
		}
		
		cell.setCellValue(value);
		return cell;
	}
	
	private HSSFCell printMoneyValueCell(HSSFRow row, double value, int index, boolean isHeader) {
		HSSFCell cell = row.createCell(index);
		CellStyle style = workbook.createCellStyle();
		
		if (isHeader) {
			style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
			style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		}
		
		cell.setCellValue(value);
		style.setDataFormat(MONEY_FORMAT);
		cell.setCellStyle(style);
		return cell;
	}
	
	private HSSFCell printPercentageValueCell(HSSFRow row, double value, int index, boolean isHeader) {
		HSSFCell cell = row.createCell(index);
		CellStyle style = workbook.createCellStyle();
		
		if (isHeader) {
			style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
			style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		}
		
		cell.setCellValue(value);		
		style.setDataFormat(workbook.createDataFormat().getFormat("0.000%"));
		cell.setCellStyle(style);
		return cell;
	}
	
	private HSSFCell printFormulaPercentageCell(HSSFRow row, String formula,  int index, boolean isHeader) {
		HSSFCell cell = row.createCell(index);		
		CellStyle style = workbook.createCellStyle();
		
		if (isHeader) {
			style.setFillForegroundColor(IndexedColors.LIGHT_CORNFLOWER_BLUE.getIndex());
			style.setFillPattern(CellStyle.SOLID_FOREGROUND);
		}
		
		cell.setCellFormula(formula);
		style.setDataFormat(workbook.createDataFormat().getFormat("0.000%"));
		cell.setCellStyle(style);
		return cell;
	}

	private void printGeneralHeaderCell(HSSFRow row, String columnName, int index) {
		HSSFCell cell = row.createCell(index);
		cell.setCellValue(columnName);
		cell.setCellStyle(headerStyle);
		generalSheet.autoSizeColumn(index);
	}
	
	private void printDetailHeaderCell(HSSFRow row, String columnName, int index) {
		HSSFCell cell = row.createCell(index);
		cell.setCellValue(columnName);
		cell.setCellStyle(headerStyle);
		detailSheet.autoSizeColumn(index);
	}
	
	private void createHeaderStyle(HSSFWorkbook workbook) {
		HSSFFont headerFont =  workbook.createFont();
		headerFont.setBoldweight(HSSFFont.BOLDWEIGHT_BOLD);
		headerFont.setFontHeightInPoints(TAMAÑO_FUENTE);
		headerStyle = workbook.createCellStyle();
		headerStyle.setFont(headerFont);
		headerStyle.setAlignment(CellStyle.ALIGN_CENTER);
		headerStyle.setFillForegroundColor(IndexedColors.SKY_BLUE.getIndex());
		headerStyle.setFillPattern(CellStyle.SOLID_FOREGROUND);
	}
}
