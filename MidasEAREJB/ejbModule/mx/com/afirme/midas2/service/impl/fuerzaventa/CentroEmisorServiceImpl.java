package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.service.fuerzaventa.CentroEmisorService;

@Stateless
public class CentroEmisorServiceImpl extends FuerzaDeVentaServiceImpl implements
		CentroEmisorService {
	@PersistenceContext
	private EntityManager entityManager;
	
	@Override
	public List<RegistroFuerzaDeVentaDTO> listarCentrosEmisores() {
		return super.listar(null, null, 
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.CENTRO_EMISOR.obtenerTipo());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Map<Long, String> listarCentrosEmisoresMap() {
		List<Object> result = new LinkedList<Object>();
		Query query = entityManager
				.createNativeQuery("Select c.id, c.nombre from midas.TCCENTROEMISOR c order by c.nombre");
		result = query.getResultList();
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		Object[] array = new Object[2];
		for (Object item : result) {
			array = (Object[]) item;
			BigDecimal id = (BigDecimal) array[0];
			map.put(id.longValue(), (String) array[1]);
		}
		return map;
	}

}
