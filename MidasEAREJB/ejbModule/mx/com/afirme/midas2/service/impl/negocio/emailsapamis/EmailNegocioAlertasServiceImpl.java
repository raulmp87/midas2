package mx.com.afirme.midas2.service.impl.negocio.emailsapamis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.negocio.emailsapamis.EmailNegocioAlertas;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.emailsapamis.EmailNegocioAlertasService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatAlertasSapAmisService;

@Stateless
public class EmailNegocioAlertasServiceImpl implements EmailNegocioAlertasService{
	private EntidadService entidadService;
	private CatAlertasSapAmisService catAlertasSapAmisService;
	
	@Override
	public EmailNegocioAlertas completeObject(EmailNegocioAlertas arg0) {
		EmailNegocioAlertas retorno = new EmailNegocioAlertas(); 
		if(arg0 != null){
			if(arg0.getIdEmailNegocioAlertas() < 1){
				retorno = findIdByAttributes(arg0);
			}else{
				if(!validateAttributes(arg0)){
					retorno = findById(arg0.getIdEmailNegocioAlertas());
				}
			}
		}else{
			retorno.setIdEmailNegocioAlertas(Long.valueOf(-1));
		}
		return retorno;
	}

	@Override
	public List<EmailNegocioAlertas> findAll() {
		List<EmailNegocioAlertas> retorno = entidadService.findAll(EmailNegocioAlertas.class);
		return retorno;
	}

	@Override
	public EmailNegocioAlertas findById(Long arg0) {
		EmailNegocioAlertas retorno = new EmailNegocioAlertas();
		if(arg0 != null && arg0 >= 0){
			retorno = entidadService.findById(EmailNegocioAlertas.class, arg0);
		}
		return retorno;
	}

	@Override
	public List<EmailNegocioAlertas> findByProperty(String arg0, Object arg1) {
		List<EmailNegocioAlertas> emailNegocioAlertasList = new ArrayList<EmailNegocioAlertas>();
		if(arg0 != null && !arg0.equals("") && arg1 != null){
			emailNegocioAlertasList = entidadService.findByProperty(EmailNegocioAlertas.class, arg0, arg1);
		}
		return emailNegocioAlertasList;
	}

	@Override
	public List<EmailNegocioAlertas> findByStatus(boolean arg0) {
		List<EmailNegocioAlertas> emailNegocioAlertas = entidadService.findByProperty(EmailNegocioAlertas.class, "estatus", arg0?0:1);
		return emailNegocioAlertas;
	}

	@Override
	public EmailNegocioAlertas saveObject(EmailNegocioAlertas arg0) {
		if(arg0 != null && arg0.getIdEmailNegocioAlertas() >= 0){
			arg0.setIdEmailNegocioAlertas((Long)entidadService.saveAndGetId(arg0));
		}
		return arg0;
	}
	
	private boolean validateAttributes(EmailNegocioAlertas arg0){
		boolean retorno = true;
		if(retorno){
			retorno = arg0.getCatAlertasSapAmis() != null;
		}
		return retorno;
	}

	@Override
	public EmailNegocioAlertas findIdByAttributes(EmailNegocioAlertas arg0) {
		if(validateAttributes(arg0)){
			arg0.setCatAlertasSapAmis(catAlertasSapAmisService.completeObject(arg0.getCatAlertasSapAmis()));
			if(arg0.getCatAlertasSapAmis().getId().intValue() > 0){
				Map<String,Object> parametros = new HashMap<String,Object>();
				parametros.put("idNegocio", arg0.getIdNegocio());
				parametros.put("catAlertasSapAmis", arg0.getCatAlertasSapAmis());
				List<EmailNegocioAlertas> emailList = entidadService.findByProperties(EmailNegocioAlertas.class, parametros);
				if(emailList.size()>0){
					arg0.setIdEmailNegocioAlertas(emailList.get(0).getIdEmailNegocioAlertas());
				}else{
					arg0.setIdEmailNegocioAlertas(Long.valueOf(0));
				}
			}else{
				arg0.setIdEmailNegocioAlertas(Long.valueOf(-1));
			}
		}else{
			arg0.setIdEmailNegocioAlertas(Long.valueOf(-1));
		}
		return arg0;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCatAlertasSapAmisService(CatAlertasSapAmisService catAlertasSapAmisService) {
		this.catAlertasSapAmisService = catAlertasSapAmisService;
	}
}