package mx.com.afirme.midas2.service.impl.siniestros.cabina.reportecabina;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.SiniestroCabinaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina.TerminoAjuste;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.BitacoraEventoSiniestro.TipoEvento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CondicionEspecialReporte;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.ClaveSubCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina.EstatusReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso.EstatusIngreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCruceroJuridica;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ValuacionReporte;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.bitemporal.siniestros.PolizaSiniestroServiceImpl;
import mx.com.afirme.midas2.service.ocra.EnvioOcraService;
import mx.com.afirme.midas2.service.siniestros.sipac.ReporteLayoutSipacService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCruceroJuridicaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.siniestros.spv.ServicioPublicoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService;
import mx.com.afirme.midas2.service.siniestros.valuacion.hgs.HgsService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.EventoService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.CommonUtils;


import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;


@Stateless
public class SiniestroCabinaServiceImpl implements SiniestroCabinaService{
	
	private static final String MSG_CANCELACION_RECUPERACION = "Cancelación por cambio en término Siniestro";
	
	public static final Logger log = Logger.getLogger(SiniestroCabinaService.class);
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@Resource
	private Validator validator;
	
	@EJB
	private SiniestroCabinaDao siniestroCabinaDao;

	@EJB
	private PolizaSiniestroService polizaSiniestroService;

	@EJB
	private ReporteCabinaService reporteCabinaService;
	
	@EJB
	private EnvioOcraService envioOcraService;
	
	@EJB
	private EnvioNotificacionesService notificacionService;
	
	@EJB
	private ValuacionReporteService valuacionReporteService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB
	private HgsService hgsService;
	
	@EJB
	private EnvioOcraService ocraService;
	
	@EJB
    private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;

	@EJB
	private RecuperacionCruceroJuridicaService recuperacionCruceroJuridicaService;
	
	@EJB
	private RecuperacionService recuperacionService;	
	
	@EJB
	private RecuperacionCiaService recuperacionCiaService;	
	
    @EJB
    BitacoraService bitacoraService;	
    
    @EJB
	private ServicioPublicoService servicioPublicoService;
    
	@EJB
	private EventoService eventoService;
	
	@EJB
	private ReporteLayoutSipacService reporteLayoutService;
	
	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@Override
	public SiniestroCabinaDTO obtenerSiniestroCabina(Long reporteCabinaId) {
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, reporteCabinaId);
		//si no existe poliza no se puede afectar el inciso
		if(reporteCabina.getPoliza() == null){
			throw new RuntimeException("No existe poliza registrada para el reporte ".concat(reporteCabinaId.toString()));
		}
		
		SiniestroCabinaDTO siniestroCabina = new SiniestroCabinaDTO();
		siniestroCabina.setReporteCabinaId(reporteCabinaId);
		siniestroCabina.setNumeroReporte(reporteCabina.getNumeroReporte());	
		siniestroCabina.setPoliza(reporteCabina.getPoliza().getNumeroPolizaFormateada());
		siniestroCabina.setTipoCotizacion(reporteCabina.getTipoCotizacion());
		siniestroCabina.setCargoDetalle(false);
		siniestroCabina.setEstatusReporte(reporteCabina.getEstatus());
		
		List<SiniestroCabina> siniestro = entidadService.findByProperty(SiniestroCabina.class, 
				"reporteCabina.id", reporteCabinaId);
		
		if (siniestro != null && !siniestro.isEmpty()) {
			siniestroCabina.setNumeroSiniestro(siniestro.get(0).getNumeroSiniestro());
		}
		
		IncisoSiniestroDTO filtro = new IncisoSiniestroDTO();

		Long incisoContinuityId = reporteCabinaService.obtenerIncisoReporteCabina(reporteCabinaId);   
		
		
		            // # OBTENER DATOS DEL AUTO MEDIANTE EL FILTRO-DTO
		if(incisoContinuityId != null && incisoContinuityId != 0){
			siniestroCabina.setFechaOcurrido(reporteCabina.getFechaOcurrido());
			siniestroCabina.setFechaOcurridoMillis(reporteCabina.getFechaOcurrido().getTime());
			siniestroCabina.setIncisoContinuity(incisoContinuityId);
			siniestroCabina.setIdToPoliza(reporteCabina.getPoliza().getIdToPoliza());
	        filtro.setIncisoContinuityId(incisoContinuityId);
	        filtro.setFechaReporteSiniestro(reporteCabina.getFechaHoraOcurrido());
	        
	        List<IncisoReporteCabina> incisosReporte = entidadService.findByProperty(IncisoReporteCabina.class,
	        								"seccionReporteCabina.reporteCabina.id", reporteCabinaId);
	        
	        if (incisosReporte != null && !incisosReporte.isEmpty()) {
	        	 IncisoSiniestroDTO incisoSiniestroDTO  = this.polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(reporteCabinaId);
	 	        if(incisoSiniestroDTO != null){
	 	        	siniestroCabina.setLineaNegocio(incisoSiniestroDTO.getAutoInciso().getDescLineaNegocio());
	 	        	siniestroCabina.setNumeroInciso(String.valueOf(incisoSiniestroDTO.getNumeroInciso()));
	 	        	siniestroCabina.setEstatus(incisoSiniestroDTO.getDescEstatus());
	 	        	siniestroCabina.setMotivoSituacion(incisoSiniestroDTO.getMotivo());
	 	        	siniestroCabina.setConductorHabitual(incisoSiniestroDTO.getAutoInciso().getNombreConductor());
	 	        	siniestroCabina.setDescripcion(incisoSiniestroDTO.getAutoInciso().getDescripcionFinal());
	 	        	siniestroCabina.setIniVigencia(incisoSiniestroDTO.getFechaIniVigencia());
	 	        	siniestroCabina.setFinVigencia(incisoSiniestroDTO.getFechaFinVigencia());
	 	        	siniestroCabina.setNoAplicaDepuracion(incisoSiniestroDTO.getNoAplicaDepuracion());
	 	        	siniestroCabina.setFechaVigIniRealIncSiniestro(incisoSiniestroDTO.getFechaVigenciaIniRealInciso());
	 	        	
	 	        }
	        }
	       
		}

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("incisoReporteCabina.seccionReporteCabina.reporteCabina.id", reporteCabinaId);
		List<AutoIncisoReporteCabina> incisos = entidadService.findByProperties(AutoIncisoReporteCabina.class, params);
		AutoIncisoReporteCabina inciso;
		if(incisos.size() > 0){ 
			inciso = incisos.get(0);
			siniestroCabina.setId(inciso.getId());
			siniestroCabina.setCausaSiniestro( inciso.getCausaSiniestro() );
			siniestroCabina.setTipoResponsabilidad( inciso.getTipoResponsabilidad() );
			siniestroCabina.setTipoPerdida(inciso.getTipoPerdida());
			siniestroCabina.setTerminoAjuste( inciso.getTerminoAjuste() );
			
			CatValorFijo valor = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.COLOR, inciso.getColor());
			if (valor != null) {
				siniestroCabina.setColor(valor.getDescripcion()); 
			}
			
			if (inciso.getPersonaAseguradoId() != null) {
				ClienteGenericoDTO cliente = new ClienteGenericoDTO();
				cliente.setIdCliente(new BigDecimal(inciso.getPersonaAseguradoId()));
				
				try {
					cliente = clienteFacadeRemote.loadById( cliente );
					
					siniestroCabina.setRfcAsegurado(cliente.getCodigoRFC());
					siniestroCabina.setCurpAsegurado(cliente.getCodigoCURP());
				} catch (Exception e) {
				}
			}
			
		}
		Boolean tieneEnviosPendienteOCRA = this.ocraService.isPendienteOcra(reporteCabina.getNumeroReporte());
		Boolean tieneEnviosPendienteHGS  = this.hgsService.isPendientesHgs(reporteCabinaId);
		Boolean tieneEnvioPendientesSPV  = this.servicioPublicoService.isPendientesSpv(reporteCabina.getId());
		
		siniestroCabina.setEnviosPendientesHGSOCRA((tieneEnviosPendienteHGS || tieneEnviosPendienteOCRA || tieneEnvioPendientesSPV ));
		siniestroCabina.setEsSiniestro(validaEsSiniestro(siniestroCabina));
		
		//identificar si existen condiciones especiales
		List<CondicionEspecialReporte> condEspecialReporte = entidadService.findByProperty(CondicionEspecialReporte.class, 
				"reporteCabina.id", reporteCabinaId);
		if(condEspecialReporte != null && condEspecialReporte.size() > 0){
			siniestroCabina.setTieneCondicionesEspeciales(Boolean.TRUE);
		}		
		
		return siniestroCabina;
	}

	@Override
	public SiniestroCabina convertirSiniestro(SiniestroCabinaDTO siniestroCabina) {
		SiniestroCabina siniestro = new SiniestroCabina();
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, siniestroCabina.getReporteCabinaId());
		this.entidadService.refresh(reporteCabina);
		List<SiniestroCabina> siniestros = entidadService.findByProperty(SiniestroCabina.class, "reporteCabina.id", reporteCabina.getId());
		
		log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Inicia Validaciones");
		if (CollectionUtils.isNotEmpty(siniestros)) {
			throw new NegocioEJBExeption("NSINError", "El reporte ya se encuentra convertido a Siniestro");
		}
		
		//No poliza/inciso cancelado
		if(!StringUtil.isEmpty(reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getEstatusInciso()) &&  
				reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getEstatusInciso().equals(
						IncisoReporteCabina.EstatusVigenciaInciso.CANCELADO.toString()) ){
			IncisoSiniestroDTO incisoSiniestroDTO = polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(reporteCabina.getId());
			Short incisoAutorizado = polizaSiniestroService.estatusIncisoAutorizacion(reporteCabina.getPoliza().getIdToPoliza().longValue(),
					  reporteCabina.getId(), incisoSiniestroDTO.getNumeroInciso(), null);
			if( (!incisoSiniestroDTO.getMotivo().equals(PolizaSiniestroServiceImpl.MOTIVO_FALTA_PAGO)) 
					|| ( incisoSiniestroDTO.getMotivo().equals(PolizaSiniestroServiceImpl.MOTIVO_FALTA_PAGO) && incisoAutorizado != 1) ){
				throw new NegocioEJBExeption("NSINError", "No puede convertirse a siniestro un reporte de una póliza/inciso cancelado");
			}
		}
		
		//validar que existe lugar de atencion
		if(reporteCabina.getLugarOcurrido() == null || StringUtil.isEmpty(reporteCabina.getLugarOcurrido().getZona())){
			throw new NegocioEJBExeption("LAError", "Es necesario contar con un lugar de ocurrido para convertir el siniestro");
		}
		
		//validar que exista fecha y hora de termino
		if(reporteCabina.getFechaHoraTerminacion() == null){
			throw new NegocioEJBExeption("FHTError", "Es necesario generar fecha y hora de término antes de convertir el siniestro");
		}
		
		if (reporteCabina.getPoliza().getCotizacionDTO().getTipoCotizacion() == null 
				|| !reporteCabina.getPoliza().getCotizacionDTO().getTipoCotizacion().equals(CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO)) {
			if(siniestroCabina.getRfc()== null){
				throw new NegocioEJBExeption("FHTError", "Es necesario capturar el RFC antes de convertir el siniestro");
			}
			
			if(siniestroCabina.getCurp()== null){
				throw new NegocioEJBExeption("FHTError", "Es necesario capturar el CURP antes de convertir el siniestro");
			}
		}
		
		log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Termina Validaciones");
		
		
		
		siniestro.setClaveOficina(reporteCabina.getOficina().getClaveOficina());
		siniestro.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		siniestro.setFechaCreacion(new Date());
		siniestro.setReporteCabina(reporteCabina);
		siniestro.setAnioReporte(String.valueOf(Calendar.getInstance().get(Calendar.YEAR)));
		siniestro.setConsecutivoReporte(siniestroCabinaDao.obtenerConsecutivoReporte(siniestro));
		
		entidadService.save(siniestro);
		
		log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Se guarda la entidad de siniestro");
		
		if (EnumUtil.equalsValue(EstatusReporteCabina.TRAMITE_REPORTE, reporteCabina.getEstatus())) {
			reporteCabina.setEstatus(EstatusReporteCabina.TRAMITE_SINIESTRO.getValue());				
		} else if (EnumUtil.equalsValue(EstatusReporteCabina.REPORTE_REAPERTURADO, reporteCabina.getEstatus())) {
			reporteCabina.setEstatus(EstatusReporteCabina.SINIESTRO_REAPERTURADO.getValue());				
		}
			
		entidadService.save(reporteCabina);
		siniestroCabina.setEsSiniestro(true);
		siniestroCabina.setNumeroSiniestro(siniestro.getNumeroSiniestro());
		
		this.reporteLayoutService.actualizaSiniestro(reporteCabina, siniestro);
		
		/////////////////// WEB SERVICE
		
		// # NOTIFICAR A OCRA
		//CS_13 = Robo con violencia,  CS_14 = Robo Estacionado
		if ( siniestroCabina.getCausaSiniestro().equals("CS_13") || siniestroCabina.getCausaSiniestro().equals("CS_14") ){
			log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Inicia notificación a Ocra");
			try{
				this.envioOcraService.enviarAltaRoboOCRAServicio(siniestroCabina.getReporteCabinaId());
			}catch(Exception ex){
				log.error("Error envioOcraService: "+ex);
			}
			log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Termina notificación a Ocra");
		}
		
		// # NOTIFICAR HGS
		if ( afectadasRCVDMA(siniestroCabina.getReporteCabinaId()) ){
			log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Inicia notificación a HGS");
			try{
				this.hgsService.insertarReporte(siniestroCabina.getReporteCabinaId());
			}catch(Exception e){
				log.error("Error hgsService: "+e);
			}
			log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Termina notificación a HGS");
		}
		
		log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Inicia notificación a SPV");
		// # NOTIFICA SPV
		this.notificarComplementoSpv(reporteCabina.getId());
		log.info("CONVERTIR SINIESTRO. (convertirSiniestro) : Termina notificación a SPV");
		
		return siniestro;
	}
	
	
	public boolean afectadasRCVDMA(Long keyReporteCabina){

		boolean notificarHgs = false;
		
		List<AfectacionCoberturaSiniestroDTO> lAfectaciones = this.estimacionCoberturaSiniestroService.
		obtenerCoberturasAfectacion(keyReporteCabina, null , null , null , true, false);

		for (AfectacionCoberturaSiniestroDTO afc : CollectionUtils.emptyIfNull(lAfectaciones) ){
			String claveTipoCalculo = afc.getConfiguracionCalculoCobertura().getCveTipoCalculoCobertura();
			if (EnumUtil.equalsValue(ClaveTipoCalculo.DANIOS_MATERIALES, claveTipoCalculo)
					|| (EnumUtil.equalsValue(ClaveTipoCalculo.RESPONSABILIDAD_CIVIL, claveTipoCalculo) 
							&& EnumUtil.equalsValue(ClaveSubCalculo.RC_VEHICULO, afc.getConfiguracionCalculoCobertura().getCveSubTipoCalculoCobertura()) ) ){
				notificarHgs = true;
				break;
			}
		}
		return notificarHgs;
	}
	
	
	
	@Override
	public void guardarYValidaCias(SiniestroCabinaDTO siniestroCabina)throws Exception{
		ReporteCabina reporteAntes = this.entidadService.findById(ReporteCabina.class, siniestroCabina.getReporteCabinaId());
		Integer porcentajeViejo = reporteAntes.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getPorcentajeParticipacion();
		Integer recibioOrdenCiaOrigen = reporteAntes.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia();
		SiniestroCabinaService processor = this.sessionContext.getBusinessObject(SiniestroCabinaService.class);
		processor.guardar(siniestroCabina);
		recuperacionCiaService.actualizarInfoCompaniasYProvisiona(siniestroCabina, porcentajeViejo,recibioOrdenCiaOrigen);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardar(SiniestroCabinaDTO siniestroCabina) {
		AutoIncisoReporteCabina incisoReporte;
		List<String> mensajes = new ArrayList<String>();
		boolean notificaRecomendaciones = false;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reporteCabina.id", siniestroCabina.getReporteCabinaId());
		params.put("claveContrato", true);
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, siniestroCabina.getReporteCabinaId());
		List<CondicionEspecialReporte> condEspecialReporte = entidadService.findByProperties(CondicionEspecialReporte.class, params);
		
		Set<ConstraintViolation<SiniestroCabinaDTO>> constraintViolations = validator.validate( siniestroCabina );
		if (!constraintViolations.isEmpty()) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
		}else{
			if (reporteCabina.getPoliza().getCotizacionDTO().getTipoCotizacion() == null 
					|| !reporteCabina.getPoliza().getCotizacionDTO().getTipoCotizacion().equals(CotizacionDTO.TIPO_COTIZACION_SERVICIO_PUBLICO)) {
				if(siniestroCabina.getRfc()== null){
					mensajes.add("Es necesario capturar el RFC antes de convertir el siniestro");
				}
				
				if(siniestroCabina.getCurp()== null){
					mensajes.add("Es necesario capturar el CURP antes de convertir el siniestro");
				}
			}
			
			if (siniestroCabina.getGenero()==null || siniestroCabina.getGenero().isEmpty()){
				mensajes.add("Es necesario capturar el genero");
			}
			
			if(siniestroCabina.getTipoLicencia()==null || siniestroCabina.getTipoLicencia().isEmpty()){
				mensajes.add("Es necesario capturar el tipo de licencia");
			}
			
			if(siniestroCabina.getColor()==null || siniestroCabina.getColor().isEmpty()){
				mensajes.add("Es necesario capturar el color del vehiculo");
			}
			
			if (siniestroCabina.getPorcentajeParticipacion() != null && (siniestroCabina.getPorcentajeParticipacion() < 0 || siniestroCabina.getPorcentajeParticipacion() > 100)) {
				mensajes.add("El porcentaje de participacion debe ser entre 0 y 100");
			}
			//SPV CABIN Validacion lectura una vez convertido a siniestro
			if(reporteCabinaService.noEsEditableParaElRol(reporteCabina)){
				mensajes.add("El rol del usuario no permite modificaciones de un reporte convertido a siniestro");
			}
			
			if(siniestroCabina.getId() != null && siniestroCabina.getId() > 0){
				incisoReporte = entidadService.findById(AutoIncisoReporteCabina.class, siniestroCabina.getId());
				if(!permiteCausaTipoTerminoVacios(siniestroCabina.getId(), 
						siniestroCabina.getCausaSiniestro(), siniestroCabina.getTipoResponsabilidad(), siniestroCabina.getTerminoAjuste())){
					mensajes.add("No se permite dejar causa, responsabilidad o término vacíos");
				}else {
					List<String> mensajesError = permiteCambioCausaTipoTermino(siniestroCabina.getId(), siniestroCabina.getReporteCabinaId(),
							siniestroCabina.getCausaSiniestro(), siniestroCabina.getTipoResponsabilidad(), siniestroCabina.getTerminoAjuste());
					if( mensajesError!= null && !mensajesError.isEmpty()){
						mensajes.addAll(mensajesError);
					}
				}
				/*
				 * Si existe un cambio en el término del siniestro la recuperación de sipac se debe de cancelar siempre y cuando no se haya ingresado (esto es, si el termino cambia de Sipac a otro)
				 * Si se cambia el termino de Sipac a otro y ya existe una recuperación ingresada no debe de permitirse el cambio hasta que se realice la cancelación de ese ingreso
				 */
				if(incisoReporte!=null && mensajes.isEmpty()			
						&& (TerminoAjuste.SE_RECIBIO_SIPAC.getValue().equals(incisoReporte.getTerminoAjuste()) 
								|| TerminoAjuste.SE_RECIBIO_SIPAC_REEMBOLSOCIA.getValue().equals(incisoReporte.getTerminoAjuste()))
						&& (!TerminoAjuste.SE_RECIBIO_SIPAC.getValue().equals(siniestroCabina.getTerminoAjuste()) 
								&& !TerminoAjuste.SE_RECIBIO_SIPAC_REEMBOLSOCIA.getValue().equals(siniestroCabina.getTerminoAjuste()))
						){
					
					params = new HashMap<String, Object>();
					params.put("reporteCabina.id", siniestroCabina.getReporteCabinaId());
					params.put("tipo", Recuperacion.TipoRecuperacion.SIPAC.toString());					
					List<Recuperacion> recs = entidadService.findByProperties(Recuperacion.class, params);
					
					for(Recuperacion element : recs){
						for(Ingreso in : element.getIngresos()){
							if( in.getEstatus().equals(Ingreso.EstatusIngreso.APLICADO.getValue()) ){
								mensajes.add("Ingreso " +  in.getNumero() + " Aplicado, Favor de Cancelar para realizar el termino de Ajuste");
							}
						}
					}
					if(mensajes.isEmpty()){
						for(Recuperacion element : recs){
							for(Ingreso in : element.getIngresos()){
								Date hoy = new Date();
	
					            if( in.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.getValue()) ){
					                  in.setFechaModificacion(hoy);
					                  in.setFechaCancelacion (hoy);
					                  in.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
					                  in.setCodigoUsuarioCancelacion (usuarioService.getUsuarioActual().getNombreUsuario());
					                  in.setEstatus(EstatusIngreso.CANCELADO_INGRESO_PENDIENTE_POR_APLICAR.getValue());
					                  in.setMotivoCancelacion(MSG_CANCELACION_RECUPERACION);
	
					                  this.entidadService.save(in);
	
					                  this.bitacoraService.registrar(TIPO_BITACORA.HGS, EVENTO.CANCELAR_INGRESO_PENDIENTE_APLICAR, in.getId().toString() , "Ingreso a Cancelar: "+in.getId(),"", usuarioService.getUsuarioActual().getNombreUsuario());
	
					            }
							}
							if(element.getEstatus().equals(Recuperacion.EstatusRecuperacion.PENDIENTE.toString()) || element.getEstatus().equals(Recuperacion.EstatusRecuperacion.REGISTRADO.toString())){
								recuperacionService.cancelarRecuperacion(element.getId(), MSG_CANCELACION_RECUPERACION);
							}
						}
					}
			   }
			}else{
				incisoReporte = new AutoIncisoReporteCabina();
				notificaRecomendaciones = true;
			}
			incisoReporte.setCausaSiniestro(siniestroCabina.getCausaSiniestro());
			incisoReporte.setCelular(siniestroCabina.getCelular());			
			
			if(siniestroCabina.getCondMismoAsegurado() != null){
				incisoReporte.setConductorMismoAsegurado("S".equalsIgnoreCase(siniestroCabina.getCondMismoAsegurado()) ? 1 : 0);
			}
			incisoReporte.setCurp(siniestroCabina.getCurp());
			incisoReporte.setEdad(siniestroCabina.getEdad());
			if(siniestroCabina.getFugoTerceroResponsable() != null){
				incisoReporte.setFugoTerceroResponsable("S".equalsIgnoreCase(siniestroCabina.getFugoTerceroResponsable()) ? 1 : 0);
			}	
			
			incisoReporte.setGenero(siniestroCabina.getGenero());
			incisoReporte.setImporteEstimado(siniestroCabina.getImporteEstimado());
			incisoReporte.setImporteReserva(siniestroCabina.getImporteReserva());
			incisoReporte.setLadaCelular(siniestroCabina.getLadaCelular());
			incisoReporte.setCelular(siniestroCabina.getCelular());
			incisoReporte.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().setLadaTelefono(siniestroCabina.getLadaTelefono());
			incisoReporte.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().setTelefono(siniestroCabina.getTelefono());
			incisoReporte.setNumOcupantes(siniestroCabina.getNumOcupantes());
			incisoReporte.setPlaca(siniestroCabina.getPlacas());
			incisoReporte.setConductor(siniestroCabina.getConductor());
			incisoReporte.setRfc(siniestroCabina.getRfc());
			incisoReporte.setTerminoAjuste(siniestroCabina.getTerminoAjuste());
			incisoReporte.setTerminoSiniestro(siniestroCabina.getTerminoSiniestro());
			incisoReporte.setTipoLicencia(siniestroCabina.getTipoLicencia());
			incisoReporte.setTipoPerdida(siniestroCabina.getTipoPerdida());
			incisoReporte.setTipoResponsabilidad(siniestroCabina.getTipoResponsabilidad());
			if(siniestroCabina.getUnidadEquipoPesado() != null){
				incisoReporte.setUnidadEquipoPesado("S".equalsIgnoreCase(siniestroCabina.getUnidadEquipoPesado()) ? 1 : 0);
			}
			incisoReporte.setColor(siniestroCabina.getColor());
			incisoReporte.setFechaModificacion(new Date());
			incisoReporte.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			polizaSiniestroService.setAutoInciso(incisoReporte);
			for(CondicionEspecialReporte condicion:condEspecialReporte){
				mensajes.addAll(polizaSiniestroService.validaCondicionEspecial(condicion.getCondicionEspecial(), reporteCabina, 
						PolizaSiniestroService.TIPO_VALIDACION_CONDICION.VALIDAR_SINIESTRO));
			}
			if(!mensajes.isEmpty()){
				throw new NegocioEJBExeption("Error", mensajes.toArray(), "validacion");
			}
			incisoReporte.getIncisoReporteCabina().setNoAplicaDepuracion(siniestroCabina.getNoAplicaDepuracion());
		
			if (siniestroCabina.getRecibidaOrdenCia() != null) {
				incisoReporte.setRecibidaOrdenCia("S".equalsIgnoreCase(siniestroCabina.getRecibidaOrdenCia()) ? 1 : 0);
			}
			
			incisoReporte.setCiaSeguros(siniestroCabina.getCiaSeguros());	
			incisoReporte.setPolizaCia(siniestroCabina.getPolizaCia());
			incisoReporte.setSiniestroCia(siniestroCabina.getSiniestroCia());
			incisoReporte.setPorcentajeParticipacion(siniestroCabina.getPorcentajeParticipacion());
			incisoReporte.setIncisoCia(siniestroCabina.getIncisoCia());
			
			incisoReporte.setMontoDanos(siniestroCabina.getMontoDanos());
			incisoReporte.setMotivoRechazo(siniestroCabina.getMotivoRechazo());
			incisoReporte.setObservacionesRechazo(siniestroCabina.getObservacionRechazo());
			if (siniestroCabina.getRechazoDesistimiento() != null){
				incisoReporte.setDesistimiento("S".equalsIgnoreCase(siniestroCabina.getRechazoDesistimiento()) ? 1 : 0);
			}
			incisoReporte.setOrigen(siniestroCabina.getOrigen());
			incisoReporte.setFechaExpedicion(siniestroCabina.getFechaExpedicion());
			incisoReporte.setFechaDictamen(siniestroCabina.getFechaDictamen());
			incisoReporte.setFechaJuicio(siniestroCabina.getFechaJuicio());
			incisoReporte.setClaveAmisSupervisionCampo(siniestroCabina.getClaveAmisSupervisionCampo());
			
			entidadService.save(incisoReporte);
			this.reporteLayoutService.actualizaDatosAutoInciso(siniestroCabina.getReporteCabinaId(), incisoReporte);
			this.estimacionCoberturaSiniestroService.actualizaCampoEsEditableEnLasEstimaciones( siniestroCabina.getReporteCabinaId(),
					siniestroCabina.getCausaSiniestro(), siniestroCabina.getTipoResponsabilidad(), siniestroCabina.getTerminoAjuste());
			
		   if(siniestroCabina.getEsSiniestro() && siniestroCabina.getMontoRecuperado() != null && siniestroCabina.getMontoRecuperado().compareTo(BigDecimal.ZERO) == 1){
				generarRecuperacion(siniestroCabina);
			}
		
			try{
				if(notificaRecomendaciones){					
					notificacionService.notificacionAfectacionIncisoCorreoCliente(reporteCabina);
				}
			}catch(Exception ex){
				log.error(ex);
			}

		}
	}

	@Override
	public SiniestroCabinaDTO obtenerDetalleSiniestroCabina(SiniestroCabinaDTO siniestroDTO) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("incisoReporteCabina.seccionReporteCabina.reporteCabina.id", siniestroDTO.getReporteCabinaId());
		List<AutoIncisoReporteCabina> incisos = entidadService.findByProperties(AutoIncisoReporteCabina.class, params);
		AutoIncisoReporteCabina inciso;
		
		if (CollectionUtils.isNotEmpty(incisos)) {
			inciso = incisos.get(0);
			Long reporteId = inciso.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
			siniestroDTO.setId(inciso.getId());
			siniestroDTO.setCausaSiniestro(inciso.getCausaSiniestro());
			siniestroDTO.setCelular(inciso.getCelular());		
			siniestroDTO.setConductor(inciso.getConductor());
			if(inciso.getConductorMismoAsegurado() != null){
				siniestroDTO.setCondMismoAsegurado(inciso.getConductorMismoAsegurado() == 1 ? "S" : "N");
			}
			
			siniestroDTO.setCiaSeguros(inciso.getCiaSeguros());		
			siniestroDTO.setCurp(inciso.getCurp());
			siniestroDTO.setEdad(inciso.getEdad());
			if(inciso.getFugoTerceroResponsable() != null){
				siniestroDTO.setFugoTerceroResponsable(inciso.getFugoTerceroResponsable() == 1 ? "S" : "N");
			}
			siniestroDTO.setGenero(inciso.getGenero());
			BigDecimal importeEstimado = movimientoSiniestroService.obtenerMontoEstimacionReporte(reporteId);
			siniestroDTO.setImporteEstimado(importeEstimado);
			BigDecimal reserva = movimientoSiniestroService.obtenerReservaReporte(reporteId, Boolean.TRUE);
			siniestroDTO.setImporteReserva(reserva);
			siniestroDTO.setLadaCelular(inciso.getLadaCelular());
			siniestroDTO.setCelular(inciso.getCelular());
			siniestroDTO.setLadaTelefono(inciso.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getLadaTelefono());
			siniestroDTO.setTelefono(inciso.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getTelefono());
			siniestroDTO.setNumOcupantes(inciso.getNumOcupantes());
			siniestroDTO.setPlacas(inciso.getPlaca());
			if(inciso.getRecibidaOrdenCia() != null){
				siniestroDTO.setRecibidaOrdenCia(inciso.getRecibidaOrdenCia() == 1 ? "S" : "N");
			}
			siniestroDTO.setPolizaCia(inciso.getPolizaCia());
			siniestroDTO.setSiniestroCia(inciso.getSiniestroCia());
			siniestroDTO.setPorcentajeParticipacion(inciso.getPorcentajeParticipacion());
			siniestroDTO.setIncisoCia(inciso.getIncisoCia());
			siniestroDTO.setRfc(inciso.getRfc());
			siniestroDTO.setTerminoAjuste(inciso.getTerminoAjuste());
			siniestroDTO.setTerminoSiniestro(inciso.getTerminoSiniestro());
			siniestroDTO.setTipoLicencia(inciso.getTipoLicencia());
			siniestroDTO.setTipoPerdida(inciso.getTipoPerdida());
			siniestroDTO.setTipoResponsabilidad(inciso.getTipoResponsabilidad());
			siniestroDTO.setMontoDanos(inciso.getMontoDanos());
			siniestroDTO.setMotivoRechazo(inciso.getMotivoRechazo());
			siniestroDTO.setObservacionRechazo(inciso.getObservacionesRechazo());
			if(inciso.getDesistimiento() != null){
				siniestroDTO.setRechazoDesistimiento(inciso.getDesistimiento() == 1 ? "S" : "N");
			}
			if(inciso.getUnidadEquipoPesado() != null){
				siniestroDTO.setUnidadEquipoPesado(inciso.getUnidadEquipoPesado() == 1 ? "S" : "N");
			}
			siniestroDTO.setVerificado(String.valueOf(inciso.getVerificado()));
			ValuacionReporte valuacion = valuacionReporteService.obtenerValuacion(siniestroDTO.getReporteCabinaId());
			if(valuacion != null){
				siniestroDTO.setNumValuacion(valuacion.getId());
				siniestroDTO.setEstatusValuacion(catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION_CRUCERO, String.valueOf(valuacion.getEstatus())).getDescripcion());
				siniestroDTO.setMontoValuacion(CommonUtils.isNullOrZero(valuacion.getTotal()) ? BigDecimal.ZERO : new BigDecimal(valuacion.getTotal()));
			}
			
			siniestroDTO = obtenerValoresRecuperacion(inciso.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getSiniestroCabina(), siniestroDTO);
			siniestroDTO.setColor(inciso.getColor());
			
			if (inciso.getPersonaAseguradoId() != null) {
				ClienteGenericoDTO cliente = new ClienteGenericoDTO();
				cliente.setIdCliente(new BigDecimal(inciso.getPersonaAseguradoId()));
				
				try {
					cliente = clienteFacadeRemote.loadById( cliente );
					
					siniestroDTO.setRfcAsegurado(cliente.getCodigoRFC());
					siniestroDTO.setCurpAsegurado(cliente.getCodigoCURP());
				} catch (Exception e) {
				}
			}
			
			siniestroDTO.setOrigen(inciso.getOrigen());
			siniestroDTO.setFechaExpedicion(inciso.getFechaExpedicion());
			siniestroDTO.setFechaDictamen(inciso.getFechaDictamen());
			siniestroDTO.setFechaJuicio(inciso.getFechaJuicio());
			siniestroDTO.setClaveAmisSupervisionCampo(inciso.getClaveAmisSupervisionCampo());
		}
		return siniestroDTO;
	}
	
	private SiniestroCabinaDTO obtenerValoresRecuperacion(SiniestroCabina siniestro, SiniestroCabinaDTO siniestroDTO){
		if(CommonUtils.isNotNull(siniestro)){
			if(!StringUtils.isEmpty(siniestroDTO.getTerminoSiniestro())){
				List<RecuperacionCruceroJuridica> recuperaciones = entidadService.findByProperty(RecuperacionCruceroJuridica.class, "siniestroCabina", siniestro);
				if (recuperaciones.size() > 0){
					RecuperacionCruceroJuridica recuperacion = null;
					for(RecuperacionCruceroJuridica e : recuperaciones){
						if(esRecuperacionValida(e.getEstatus())){
							recuperacion = e;
							break;
						}
					}
					
					if(recuperacion != null){
						siniestroDTO.setMontoRecuperado(recuperacion.getMontoFinal());
						siniestroDTO.setBanco(recuperacion.getBanco());
						siniestroDTO.setNumAprobacion(recuperacion.getNumAprobacion());
						siniestroDTO.setNumDeposito(recuperacion.getNumDeposito());
						siniestroDTO.setFechaDeposito(recuperacion.getFechaDeposito());
						siniestroDTO.setRecuperacionId(recuperacion.getId());
						siniestroDTO.setTipoRecuperacion(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString());
					}
				} 
			}
		
		}
		
		return siniestroDTO;
	}
	
	private boolean esRecuperacionValida(String estatus){
		boolean retVal = true;
		if(estatus.equals(Recuperacion.EstatusRecuperacion.CANCELADO.toString()) ||
				estatus.equals(Recuperacion.EstatusRecuperacion.INACTIVO.toString())){
			retVal = false;
		}
		return retVal;
	}
	
	private boolean validaEsSiniestro(SiniestroCabinaDTO siniestroCabina){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reporteCabina.id", siniestroCabina.getReporteCabinaId());
		List<SiniestroCabina> siniestros = entidadService.findByProperties(SiniestroCabina.class, params);
		if(siniestros.size() > 0){
			siniestroCabina.setNumeroSiniestro(siniestros.get(0).getNumeroSiniestro());
			return true;
		}else{
			return false;
		}
	}

	
	@Override
	public List<CatValorFijo> obtenerTerminosAjuste(String codigoTipoSiniestro, String codigoResponsabilidad) {
		return siniestroCabinaDao.obtenerTerminosAjuste(codigoTipoSiniestro, codigoResponsabilidad);		
	}

	@Override
	public AutoIncisoReporteCabina obtenerAutoIncisoByReporteCabina(
			Long reporteCabinaId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("incisoReporteCabina.seccionReporteCabina.reporteCabina.id", reporteCabinaId);
		List<AutoIncisoReporteCabina> incisos = entidadService.findByProperties(AutoIncisoReporteCabina.class, params);
		if(incisos.size() > 0){
			AutoIncisoReporteCabina autoInciso = incisos.get(0);
			
			if (autoInciso.getTipoUsoId() != null) {
				TipoUsoVehiculoDTO tipoUso = entidadService.findById(TipoUsoVehiculoDTO.class, new BigDecimal(autoInciso.getTipoUsoId()));
				
				if (tipoUso != null) {
					autoInciso.setDescTipoUso(tipoUso.getDescription());
				}
			}
			
			if (autoInciso.getMarcaId() != null) {
				autoInciso.setDescMarca(entidadService.findById(MarcaVehiculoDTO.class, autoInciso.getMarcaId()).getDescription());	
			}			
			
			if (autoInciso.getEstiloId() != null) {
				EstiloVehiculoDTO estilo = entidadService.findById(EstiloVehiculoDTO.class, getEstiloVehiculoId(autoInciso.getEstiloId()));
				
				if (estilo != null) {
					autoInciso.setDescEstilo(estilo.getDescription());
				}				
			}
			
			if (autoInciso.getColor() != null) {
				autoInciso.setDescColor(catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.COLOR, autoInciso.getColor()).getDescripcion());
			}
			return autoInciso;
			
		}else{
			return null;
		}
	}
	
	private EstiloVehiculoId getEstiloVehiculoId(String estiloId) {
		EstiloVehiculoId estiloVehiculoId = null;
		String[] arrayEstilo = StringUtils.split(estiloId, '_');
		if (arrayEstilo.length > 2) {
			estiloVehiculoId = new EstiloVehiculoId(arrayEstilo[0], arrayEstilo[1], new BigDecimal(arrayEstilo[2]));
		}		
		return estiloVehiculoId;		
	}
	
	public Boolean permiteCambioCausaTipoTermino2(Long autoIncisoReporteId, Long reporteCabinaId, 
			String causaSiniestro, String tipoResponsabilidad, String terminoAjuste){
		Boolean permite = Boolean.TRUE;		
		if(autoIncisoReporteId != null){
			if(estimacionCoberturaSiniestroService.contieneCoberturasAfectadas(reporteCabinaId)){
				AutoIncisoReporteCabina autoInciso = entidadService.findById(AutoIncisoReporteCabina.class, autoIncisoReporteId);
				if(!StringUtil.isEmpty(autoInciso.getCausaSiniestro())){
					if(StringUtil.isEmpty(causaSiniestro) || !causaSiniestro.equals(autoInciso.getCausaSiniestro())){
						return Boolean.FALSE;
					}
				}
				if(!StringUtil.isEmpty(autoInciso.getTipoResponsabilidad())){
					if(StringUtil.isEmpty(tipoResponsabilidad) || !tipoResponsabilidad.equals(autoInciso.getTipoResponsabilidad())){
						return Boolean.FALSE;
					}
				}
				if(!StringUtil.isEmpty(autoInciso.getTerminoAjuste())){
					if(StringUtil.isEmpty(terminoAjuste) || !terminoAjuste.equals(autoInciso.getTerminoAjuste())){
						return Boolean.FALSE;
					}
				}
			}
		}
		return permite;
	}
	
	public Boolean permiteCausaTipoTerminoVacios(Long autoIncisoReporteId, 
			String causaSiniestro, String tipoResponsabilidad, String terminoAjuste){
		AutoIncisoReporteCabina autoInciso = entidadService.findById(AutoIncisoReporteCabina.class, autoIncisoReporteId);
		if(!StringUtil.isEmpty(autoInciso.getCausaSiniestro()) && StringUtil.isEmpty(causaSiniestro)){
			return Boolean.FALSE;
		}
		if(!StringUtil.isEmpty(autoInciso.getTipoResponsabilidad()) && StringUtil.isEmpty(tipoResponsabilidad)){
			return Boolean.FALSE;
		}		
		if(!StringUtil.isEmpty(autoInciso.getTerminoAjuste()) && StringUtil.isEmpty(terminoAjuste)){
			return Boolean.FALSE;
		}
		return Boolean.TRUE;
	}
	
	public List<String> permiteCambioCausaTipoTermino(Long autoIncisoReporteId, Long reporteCabinaId, 
			String causaSiniestro, String tipoResponsabilidad, String terminoAjuste){		
		List<String> mensajesError = null;
		if(!StringUtil.isEmpty(causaSiniestro) && !StringUtil.isEmpty(tipoResponsabilidad) && !StringUtil.isEmpty(terminoAjuste)){
			mensajesError =  estimacionCoberturaSiniestroService.validaCambioEnCoberturasPorCausaTipoTermino(
					reporteCabinaId, causaSiniestro, tipoResponsabilidad, terminoAjuste);
		}		
		return mensajesError;
	}
	
	@Override
	public void generarRecuperacion(SiniestroCabinaDTO siniestroDTO){
		List<SiniestroCabina> siniestros = entidadService.findByProperty(SiniestroCabina.class, "reporteCabina.id", siniestroDTO.getReporteCabinaId());
		
		if (CommonUtils.isNotNull(siniestroDTO.getRecuperacionId())){
			Recuperacion r = entidadService.findById(Recuperacion.class, siniestroDTO.getRecuperacionId());
			if(terminoSiniestroDiferente(siniestroDTO) || montoDiferente(siniestroDTO)){
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("reporteCabina.id", siniestroDTO.getReporteCabinaId());
				params.put("tipo", Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString());
				List<Recuperacion> recs = entidadService.findByProperties(Recuperacion.class, params);
				for(Recuperacion element : recs){
					for(Ingreso in : element.getIngresos()){
						Date hoy = new Date();

			            if( in.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.getValue()) ){
			                  in.setFechaModificacion(hoy);
			                  in.setFechaCancelacion (hoy);
			                  in.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			                  in.setCodigoUsuarioCancelacion (usuarioService.getUsuarioActual().getNombreUsuario());
			                  in.setEstatus(EstatusIngreso.CANCELADO_INGRESO_PENDIENTE_POR_APLICAR.getValue());
			                  in.setMotivoCancelacion(MSG_CANCELACION_RECUPERACION);

			                  this.entidadService.save(in);

			                  this.bitacoraService.registrar(TIPO_BITACORA.HGS, EVENTO.CANCELAR_INGRESO_PENDIENTE_APLICAR, in.getId().toString() , "Ingreso a Cancelar: "+in.getId(),"", usuarioService.getUsuarioActual().getNombreUsuario());

			            }
						
					}
					if(element.getEstatus().equals(Recuperacion.EstatusRecuperacion.PENDIENTE.toString()) || element.getEstatus().equals(Recuperacion.EstatusRecuperacion.REGISTRADO.toString())){
						recuperacionService.cancelarRecuperacion(element.getId(), MSG_CANCELACION_RECUPERACION);
					}
				}
				generarNuevaRecuperacion(siniestroDTO, siniestros.get(0));
			}else{
				actualizarRecuperacion(siniestroDTO, r);
			}
		}else{
			generarNuevaRecuperacion(siniestroDTO, siniestros.get(0));
		}
	}
	
	private void actualizarRecuperacion(SiniestroCabinaDTO siniestroDTO, Recuperacion recuperacion){
		guardarDatosRecuperacion(siniestroDTO);
	}
	
	private void generarNuevaRecuperacion(SiniestroCabinaDTO siniestroDTO, SiniestroCabina siniestro){
		Long recuperacionId = null;
		if (siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_EFECTIVO) ||
				siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_TDC) ||
						siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_JURIDICA_EFECTIVO) ||
						siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_JURIDICA_TDC)){
					RecuperacionCruceroJuridica recuperacion = new RecuperacionCruceroJuridica();
					recuperacion.setMontoFinal(siniestroDTO.getMontoRecuperado());
					recuperacion.setBanco(siniestroDTO.getBanco());
					recuperacion.setNumAprobacion(siniestroDTO.getNumAprobacion());
					recuperacion.setNumDeposito(siniestroDTO.getNumDeposito());
					recuperacion.setFechaDeposito(siniestroDTO.getFechaDeposito());
					recuperacion.setMedio(siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_EFECTIVO)? 
											Recuperacion.MedioRecuperacion.EFECTIVO.toString() : 
											Recuperacion.MedioRecuperacion.TDC.toString());
					recuperacion.setSiniestroCabina(siniestro);
					recuperacion.setReporteCabina(entidadService.findById(ReporteCabina.class, siniestroDTO.getReporteCabinaId()));
					recuperacion = recuperacionCruceroJuridicaService.guardar(recuperacion);
					recuperacionCruceroJuridicaService.generarIngreso(recuperacion);
					recuperacion.setEstatus(Recuperacion.EstatusRecuperacion.PENDIENTE.toString());
					entidadService.save(recuperacion);
					recuperacionId = recuperacion.getId();
			}
		if(CommonUtils.isNotNull(recuperacionId)){
			recuperacionService.generarReferenciaBancaria(recuperacionId);
		}
	}
	
	private boolean terminoSiniestroDiferente(SiniestroCabinaDTO siniestroDTO){
		Recuperacion r = entidadService.findById(Recuperacion.class, siniestroDTO.getRecuperacionId());
		if(r.getTipo().equals(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString()) && 
				r.getMedio().equals(Recuperacion.MedioRecuperacion.EFECTIVO.toString()) && 
				siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_EFECTIVO.toString())){
			return false;
		}else if(r.getTipo().equals(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString()) && 
				r.getMedio().equals(Recuperacion.MedioRecuperacion.TDC.toString()) && 
				siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_TDC.toString())){
			return false;
		}else if(r.getTipo().equals(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString()) && 
				r.getMedio().equals(Recuperacion.MedioRecuperacion.EFECTIVO.toString()) && 
				siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_JURIDICA_EFECTIVO.toString())){
			return false;
		}else if(r.getTipo().equals(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString()) && 
				r.getMedio().equals(Recuperacion.MedioRecuperacion.TDC.toString()) && 
				siniestroDTO.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_JURIDICA_TDC.toString())){
			return false;
		}

		return true;
	}
	
	private boolean montoDiferente(SiniestroCabinaDTO siniestroDTO){
		Recuperacion r = entidadService.findById(Recuperacion.class, siniestroDTO.getRecuperacionId());
		BigDecimal montoOriginal = BigDecimal.ZERO;
		if(r.getTipo().equals(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString())){
			RecuperacionCruceroJuridica rc = (RecuperacionCruceroJuridica)r;
			montoOriginal = rc.getMontoFinal() != null ? rc.getMontoFinal() : BigDecimal.ZERO;
		}
		
		return(montoOriginal.compareTo(siniestroDTO.getMontoRecuperado()) != 0);

	}
	
	private void guardarDatosRecuperacion(SiniestroCabinaDTO siniestroCabina){
		Recuperacion recuperacion = entidadService.findById(Recuperacion.class, siniestroCabina.getRecuperacionId());
		if(siniestroCabina.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_EFECTIVO) ||
				siniestroCabina.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_JURIDICA_EFECTIVO) ||
				siniestroCabina.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_CRUCERO_TDC) ||
				siniestroCabina.getTerminoSiniestro().equals(SiniestroCabinaService.TERMINO_SINIESTRO_JURIDICA_TDC)){
			if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString())){
				RecuperacionCruceroJuridica rc = (RecuperacionCruceroJuridica)recuperacion;
				rc.setMontoFinal(siniestroCabina.getMontoRecuperado());
				rc.setBanco(siniestroCabina.getBanco());
				rc.setNumAprobacion(siniestroCabina.getNumAprobacion());
				rc.setNumDeposito(siniestroCabina.getNumDeposito());
				rc.setFechaDeposito(siniestroCabina.getFechaDeposito());
				entidadService.save(rc);
			}
		}else{
			if(recuperacion.getIngresoActivo().getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.toString())){
				Ingreso ingreso = recuperacion.getIngresoActivo();
	            Date hoy = new Date();

	            if( ingreso.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.getValue()) ){
	                  ingreso.setFechaModificacion(hoy);
	                  ingreso.setFechaCancelacion (hoy);
	                  ingreso.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
	                  ingreso.setCodigoUsuarioCancelacion (usuarioService.getUsuarioActual().getNombreUsuario());
	                  ingreso.setEstatus(EstatusIngreso.CANCELADO_INGRESO_PENDIENTE_POR_APLICAR.getValue());
	                  ingreso.setMotivoCancelacion(MSG_CANCELACION_RECUPERACION);

	                  this.entidadService.save(ingreso);

	                  this.bitacoraService.registrar(TIPO_BITACORA.HGS, EVENTO.CANCELAR_INGRESO_PENDIENTE_APLICAR, ingreso.getId().toString() , "Ingreso a Cancelar: "+ingreso.getId(),"", usuarioService.getUsuarioActual().getNombreUsuario());

	            }
				
				
				
				recuperacionService.cancelarRecuperacion(siniestroCabina.getRecuperacionId(), MSG_CANCELACION_RECUPERACION);				
			}else{
				throw new RuntimeException("No es posible cancelar la Recuperación");
			}

		}

	}



	
	
	
	public String obtenerEstatusValuacion(Long valuacionId){
		if (CommonUtils.isNotNullNorZero(valuacionId)){
			ValuacionReporte valuacion = entidadService.findById(ValuacionReporte.class, valuacionId);
			return catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION_CRUCERO, String.valueOf(valuacion.getEstatus())).getDescripcion();
		}
		return "";
	}
	
	@Override
	public ReporteCabina terminarSiniestro(Long reporteCabinaId) {
		
		reporteCabinaService.depurarReservaReporte(reporteCabinaId);
		
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, reporteCabinaId);
		
		reporte.setEstatus(EstatusReporteCabina.TERMINADO.getValue());		
		
		reporte = entidadService.save(reporte);
		
		reporteCabinaService.guardarEvento(reporteCabinaId, TipoEvento.EVENTO_48, "Se cambia el estatus a Terminado");
		
		return reporte;
	}
	
	@Override
	public ReporteCabina rechazarSiniestro(Long reporteCabinaId) {
		
		reporteCabinaService.depurarReservaReporte(reporteCabinaId);
		
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, reporteCabinaId);
		
		reporte.setEstatus(EstatusReporteCabina.RECHAZADO.getValue());	
		
		reporte = entidadService.save(reporte);
		
		reporteCabinaService.guardarEvento(reporteCabinaId, TipoEvento.EVENTO_47, "Se cambia el estatus a rechazado");
		eventoService.guardarEvento("reporteSiniestroCambioEstatus",
				"{\"reporteSiniestroId\":" + reporte.getId().toString() + ", \"estatus\":\"RECH\""
					+", \"oldEstatus\":null}");
		
		return reporte;
	}
	
	@Override
	public ReporteCabina cancelarSiniestro(Long reporteCabinaId) {
		
		reporteCabinaService.depurarReservaReporte(reporteCabinaId);
		
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, reporteCabinaId);
		
		reporte.setEstatus(EstatusReporteCabina.CANCELADO.getValue());		
		
		reporte = entidadService.save(reporte);
		
		reporteCabinaService.guardarEvento(reporteCabinaId, TipoEvento.EVENTO_46, "Se cambia el estatus a cancelado");
		eventoService.guardarEvento("reporteSiniestroCambioEstatus",
				"{\"reporteSiniestroId\":" + reporte.getId().toString() + ", \"estatus\":\"CANC\""
					+", \"oldEstatus\":null}");
		
		return reporte;
	}
	
	@Override
	public ReporteCabina reaperturarReporte(Long reporteCabinaId) {
	
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, reporteCabinaId);		
				
		reporte.setEstatus(EstatusReporteCabina.REPORTE_REAPERTURADO.getValue());		
		
		List<SiniestroCabina> siniestro = entidadService.findByProperty(SiniestroCabina.class, "reporteCabina.id", reporteCabinaId);
		
		if (CollectionUtils.isNotEmpty(siniestro)) {
			reporte.setEstatus(EstatusReporteCabina.SINIESTRO_REAPERTURADO.getValue());		
		}				
		
		reporte = entidadService.save(reporte);
		
		if (CollectionUtils.isNotEmpty(siniestro)) {
			reporteCabinaService.guardarEvento(reporteCabinaId, TipoEvento.EVENTO_50, "Se realiza reapertura de siniestro");
		} else {
			reporteCabinaService.guardarEvento(reporteCabinaId, TipoEvento.EVENTO_49, "Se realiza reapertura de reporte");
		}
		eventoService.guardarEvento("reporteSiniestroAsignado",
				"{\"reporteSiniestroId\":" + reporte.getId().toString() + ", \"ajustadorId\":"+reporte.getAjustador().getIdAjustador().toString()
					+", \"oldAjustadorId\":null}");
		
		return reporte;
		
	}
	
	@Override
	public void validarCambioEstatus(Long reporteCabinaId) throws NegocioEJBExeption {
	
		List<OrdenCompra> ordenes = entidadService.findByProperty(OrdenCompra.class, "reporteCabina.id", reporteCabinaId);
		
		for (OrdenCompra orden : CollectionUtils.emptyIfNull(ordenes)) {
			if (!orden.getEstatus().equals(OrdenCompraService.ESTATUS_PAGADA) &&
					!orden.getEstatus().equals(OrdenCompraService.ESTATUS_CANCELADA) && 
					!orden.getEstatus().equals(OrdenCompraService.ESTATUS_RECHAZADA) &&
					!orden.getEstatus().equals(OrdenCompraService.ESTATUS_INDEMNIZACION_CANCELADA)) {
				throw new NegocioEJBExeption("NE_O", "Existen Ordenes de Compra pendientes de liquidar");
			}
		}
		
		BigDecimal reservaPendiente = movimientoSiniestroService.obtenerReservaReporte(reporteCabinaId, Boolean.TRUE);
		
		if (reservaPendiente.compareTo(BigDecimal.ZERO) != 0) {
			throw new NegocioEJBExeption("NE_R", "Existe reserva pendiente");
		}
	}
	
	private void notificarComplementoSpv(Long keyReporteCabina) {
		try {
			servicioPublicoService.notificarReporteServicioPublico(keyReporteCabina);
		}catch(Exception e) {
			log.error("Error EstimacionCoberturaSiniestroService notificarComplementoSpv ",e);
		}
	}
}

