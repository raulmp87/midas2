package mx.com.afirme.midas2.service.impl.clientesapi;


import java.io.OutputStream;
import java.math.BigDecimal;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacade;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.sistema.ClientSystemException;
import mx.com.afirme.midas2.clientesapi.dto.ClienteDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClienteUnicoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClienteViewDTO;
import mx.com.afirme.midas2.clientesapi.dto.ClientesApiService;
import mx.com.afirme.midas2.clientesapi.dto.ComunicacionRedSocialDTO;
import mx.com.afirme.midas2.clientesapi.dto.DocumentationException;
import mx.com.afirme.midas2.clientesapi.dto.DocumentoDTO;
import mx.com.afirme.midas2.clientesapi.dto.EntrevistaDTO;
import mx.com.afirme.midas2.clientesapi.dto.ErrorClienteUnicoDTO;
import mx.com.afirme.midas2.clientesapi.dto.ErrorDocumentoDTO;
import mx.com.afirme.midas2.clientesapi.dto.FilterListClientes;
import mx.com.afirme.midas2.clientesapi.dto.InterviewException;
import mx.com.afirme.midas2.clientesapi.dto.OcupacionCNSFDTO;
import mx.com.afirme.midas2.clientesapi.exception.RegisterClientException;
import mx.com.afirme.midas2.clientesapi.exception.ValidateClientException;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosDTO;
import mx.com.afirme.midas2.dto.persona.PersonaSeycosFacadeRemote;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpStatusCodeException;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import com.asm.ejb.ASMRemote;
import com.asm.web.webservice.ASMWSC;
import com.google.gson.Gson;
import com.js.service.SystemException;


/**
 * Clase utilizada para encapsular la funcionalidad de la invocacion a los
 * Servicios Rest de ClientesAPI usada como interface entre la aplicación de cliente único y MIDAS-Autos
 * 
 * LogInterceptor
 * 
 */
@Stateless
public class ClientesApiServiceImpl extends ClienteFacade implements
		ClientesApiService {

	private static final Logger LOG = LoggerFactory
			.getLogger(ClientesApiServiceImpl.class);
	
	/**
	 * @ 
	 * CAP, PROD)
	 */
	

	private String DOMAIN_CLIENTES_API = "";
	
	static final String AUTHENTICATE_TOKEN = "X-Authenticate-Token";

	static final String PATH_UNIFICADO = "/unificado";

	static final String PATH_VALIDATE = "/validate";
	
	static final String PATH_CLIENTES_API_VALIDATEPRIMA = "/validatePrima";

	static final String PATH_VALIDATE_REGISTER = "/register";
	
	static final String PATH_INTERVIEWS = "/entrevistas";
	
	static final String PATH_PEPS = "/peps";
	
	static final String PATH_PDF = "/{id}/entrevistas/pdf";
	
	static final String PATH_PARAMETER_ID = "/{id}";

	static final String REQUEST_PARAM_CLIENTE_ID = "clienteId";

	static final String REQUEST_PARAM_SEYCOS_ID_CLIENTE = "seycosIdCliente";
	
	static final String PATH_ACTIVIDADES_LIST= "/actividadEconomica/list";
	
	static final String PATH_DOCUMENTOS= "/personas/documentos";
	
	static final String PATH_DOCUMENTACION= "/personas/{id}/documentacion";
	static final String PM="PM";
	static final String PF="PF";
	static final String PATH_INFORMATION= "/{id}/information";
	
	static final BigDecimal ID_GRUPO_PARAMETRO_GENERAL_SERVICIOS_CLIENTE_UNICO= new BigDecimal(101);
	static final BigDecimal CODIGO_PARAMETRO_GENERAL_SERVICIOS_CLIENTE_UNICO= new BigDecimal(101010);
	
	
	static final String PATH_FORTIMAX= "/personas/documentacion/fortimax";
	
	static final String PATH_SAVE_DOCUMENT= "/personas/{personaId}/documentacion";
	
	
	static final SimpleDateFormat formaterDateClienteUnico = new SimpleDateFormat(
			"yyyy-MM-dd'T'HH:mm:ss'Z'");

	static final int PER_PAGINA = 100;

	private RestTemplate restTemplate;
	//QA
	private String token = "";
	//dev
	@EJB
	private PersonaSeycosFacadeRemote personaSeycosFacadeRemote;
	private SistemaContext sistemaContext;
	private ASMRemote asmRemote;
	private UsuarioService usuarioService;
	private ParametroGeneralFacadeRemote prametroGeneralFacadeRemote;
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@EJB
	@Autowired
	@Qualifier("parametroGeneralServiceEJB")
	public void setParametroGeneralFacadeRemote(
			ParametroGeneralFacadeRemote prametroGeneralFacadeRemote) {
		this.prametroGeneralFacadeRemote = prametroGeneralFacadeRemote;
	}
	public ClientesApiServiceImpl() {
		restTemplate = new RestTemplate();
		
		
		
	}

	public void setRestTemplate(RestTemplate restTemplate) {
		this.restTemplate = restTemplate;
	}
	
	@PostConstruct
	private void init(){
		
		

		ParametroGeneralId id= new ParametroGeneralId();
		id.setCodigoParametroGeneral(CODIGO_PARAMETRO_GENERAL_SERVICIOS_CLIENTE_UNICO);
		id.setIdToGrupoParametroGeneral(ID_GRUPO_PARAMETRO_GENERAL_SERVICIOS_CLIENTE_UNICO);
		ParametroGeneralDTO parametro = prametroGeneralFacadeRemote.findById(id, true);
		
		//
		
		DOMAIN_CLIENTES_API = parametro.getValor();
//		DOMAIN_CLIENTES_API = "http://10.247.26.6:45210/clientes/api/";
//		DOMAIN_CLIENTES_API = " http://dev6.afirme.com.mx/clientes/api/";
		
		
	}
	/**
	 * Almacena en cliente unico el cliente que recibe como parámetro
	 * @param clienteDTO representacion del cliente
	 * @return map, el cliente creado
	 */
	private ResponseEntity<Object> createCliente(ClienteDTO clienteDTO,Boolean verfi) {
		
		ResponseEntity<Object> responseEntity = null;
		try {
			String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
					.append("?validateRegister="+verfi)
					.toString();
			HttpEntity<ClienteDTO> entity = new HttpEntity<ClienteDTO>(
					clienteDTO, getHeaders());
			responseEntity = restTemplate.exchange(url, HttpMethod.POST,
					entity, Object.class);
		} catch (HttpStatusCodeException e) {
			
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- createCliente()", e);
			throw e;
		}
		
		return responseEntity;
	}
	
	/**
	 * Consulta prima acumulada y valida si es mayor al límite establecido.
	 * @param Long clienteId
	 * @return ResponseEntity<String>, JSON en String
	 */
	@Override
	public ResponseEntity<String> validateClientePrima(Long clienteId) {
		
		ResponseEntity<String> responseEntity = null;
		
		try {
			String url = new StringBuilder()
					.append(DOMAIN_CLIENTES_API)
					.append(clienteId)
					.append(PATH_CLIENTES_API_VALIDATEPRIMA)
					.toString();
			
			HttpEntity<Object> entity = new HttpEntity<Object>(getHeaders());
			responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity,
					String.class);
		} catch (HttpStatusCodeException e) {
			LOG.error("-- validateClientePrima()", e);
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- validateClientePrima()", e);
			throw e;
		}
		
		return responseEntity;
	}

	/**
	 * Actuzaliza el cliente que recibe como paremetro
	 * @param clienteDTO
	 * @return map, el cliente actualizado
	 */
	private ResponseEntity<Object> updateCliente(ClienteDTO clienteDTO,Boolean verfi) {
		
		ResponseEntity<Object> responseEntity = null;
		try {
			String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
					.append(PATH_PARAMETER_ID)
					.append("?validateRegister="+verfi)
					.toString();
			HttpEntity<ClienteDTO> entity = new HttpEntity<ClienteDTO>(
					clienteDTO, getHeaders());
			responseEntity = restTemplate.exchange(url, HttpMethod.PUT, entity,
					Object.class, clienteDTO.getId());
		} catch (HttpStatusCodeException e) {
			LOG.error("-- updateCliente()", e);
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- updateCliente()", e);
			throw e;
		}
		return responseEntity;
	}
	/**
	 * Actuzaliza o crea el cliente que recibe como parámetro dependiendo si este tiene id o no
	 * @param clienteDTO
	 * @return map, el cliente actualizado
	 */
	@Override
	public ResponseEntity<Object> saveCliente(ClienteUnicoDTO clienteUnicoDTO) {
		ResponseEntity<Object> responseEntity = null;
		ClienteDTO clienteDTO = convertClienteUnicoDTOToClienteDTO(clienteUnicoDTO);
		if (clienteUnicoDTO.getIdClienteUnico()==null ||clienteUnicoDTO.getIdClienteUnico().length()<=0) {
			Long seycosIdCliente = clienteUnicoDTO
					.getIdCliente().longValue();
			if (seycosIdCliente != null && seycosIdCliente >= 0) {
				responseEntity = createCliente(clienteDTO,clienteUnicoDTO.getVerificarRepetidos());
				/**
				 * Si el cliente fue creado retorna un status 201 CREATED,
				 * cuando el cliente se crea por primera vez se debe establecer
				 * la relacion del Nuevo Cliente con el antiguo Cliente de
				 * Seycos @Asynchronous
				 */
				if (responseEntity.getStatusCode() == HttpStatus.CREATED) {
					
					
					Map enttiMap = (Map) responseEntity.getBody();
					
									
					
					createClienteUnificado(Long.valueOf(enttiMap.get("id").toString()), seycosIdCliente);
				}
			}
		} else {
			responseEntity = updateCliente(clienteDTO,clienteUnicoDTO.getVerificarRepetidos());
		}
		
		return responseEntity;
	}
	/**
	 * Busca el cliente asociado al id que recibe como parametro
	 * @param clienteId identificador de cliente, por ejemplo 1
	 * @return ResponseEntity<ClienteDTO> el cliente correspondiente a la búsqueda, nulo en caso de no encontra coincidencias
	 */
	@Override
	public ResponseEntity<ClienteDTO> findById(Long clienteId) {
		ResponseEntity<ClienteDTO> responseEntity = null;
		try {
			String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
					.append(PATH_PARAMETER_ID)
					.toString();
			HttpEntity<Object> entity = new HttpEntity<Object>(getHeaders());
			responseEntity = restTemplate.exchange(url, HttpMethod.GET, entity,
					ClienteDTO.class, clienteId);
		} catch (HttpStatusCodeException e) {
			LOG.error("-- findById()", e);
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- findById()", e);
			throw e;
		}
		return responseEntity;
	}

	
	/**
	 * Busca los clientes asociados al flitro que recibe como parametro
	 * @param FilterListClientes dto con parámetros de búsqueda, por ejemplo nombre, apellidos etc
	 * @return regresa la lista de coincidencias
	 */
	@SuppressWarnings("unchecked")
	@Override
	public ResponseEntity<Map<String, Object>> getListClientesByFilter(
			FilterListClientes filter) {
		ResponseEntity<Map<String, Object>> responseEntity = null;
		try {
			StringBuilder urlBuilder = new StringBuilder().append(
					DOMAIN_CLIENTES_API);
			setUrlFilterParams(urlBuilder, filter);
			HttpEntity<Object> entity = new HttpEntity<Object>(getHeaders());
			@SuppressWarnings("rawtypes")
			ResponseEntity<Map> response = restTemplate.exchange(
					urlBuilder.toString(), HttpMethod.GET, entity, Map.class);
			responseEntity = new ResponseEntity<Map<String, Object>>(
					response.getBody(), response.getStatusCode());
			converterListClientesToObject(filter, responseEntity.getBody());
		} catch (HttpStatusCodeException e) {
			LOG.error("-- getListClientesByFilter()", e);
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- getListClientesByFilter()", e);
			throw e;
		}
		return responseEntity;
	}
	/**
	 * Busca el id de cliente de seycos y cliente unico dependiendo el id que reciba como parametro
	 * @param clienteId identificador dentro de cliente unico
	 * @param seycosIdCliente identificador dentro de seycos
	 * @return identificador asociado al identificador que recibe como parámetro
	 */
	@Override
	public ResponseEntity<Long> clienteUnificado(Long clienteId,
			Long seycosIdCliente) {
		ResponseEntity<Long> responseEntity = null;
		try {
			StringBuilder urlBuilder = new StringBuilder()
					.append(DOMAIN_CLIENTES_API)
					.append(PATH_UNIFICADO).append("?");
			if (clienteId != null) {
				urlBuilder
						.append(ClientesApiServiceImpl.REQUEST_PARAM_CLIENTE_ID)
						.append("=").append(clienteId);
			} else if (seycosIdCliente != null) {
				urlBuilder
						.append(ClientesApiServiceImpl.REQUEST_PARAM_SEYCOS_ID_CLIENTE)
						.append("=").append(seycosIdCliente);
			}
			HttpEntity<Object> entity = new HttpEntity<Object>(getHeaders());
			responseEntity = restTemplate.exchange(urlBuilder.toString(),
					HttpMethod.GET, entity, Long.class, clienteId,
					seycosIdCliente);
		} catch (HttpStatusCodeException e) {
			LOG.error("-- clienteUnificado()", e);
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- clienteUnificado()", e);
			throw e;
		}
		return responseEntity;
	}
	/**
	 * valida que el cliente que recibe como parámtro cumpla con las reglas de negocio 
	 * @param ClienteUnicoDTO dto representado de cliente unico
	 * @return regresa la lista de errores nulo si el cliente cumple con todas las reglas de negocio
	 */
	@Override
	public List<ErrorClienteUnicoDTO> validateCliente(
			ClienteUnicoDTO clienteUnico) {
		
		ClienteDTO clienteDTO = convertClienteUnicoDTOToClienteDTO(clienteUnico);
		ResponseEntity<Object> responseEntity = null;

		List<ErrorClienteUnicoDTO> errorsList = null;
		try {
			StringBuilder urlBuilder = new StringBuilder()
					.append(DOMAIN_CLIENTES_API)
					.append(PATH_VALIDATE);
			HttpEntity<ClienteDTO> entity = new HttpEntity<ClienteDTO>(
					clienteDTO, getHeaders());
			responseEntity = restTemplate.exchange(urlBuilder.toString(),
					HttpMethod.POST, entity, Object.class);

		} catch (HttpStatusCodeException e) {
		
		if (e.getResponseBodyAsString()!=null&&e.getResponseBodyAsString().trim().compareTo("")>0){
				ErrorClienteUnicoDTO[] tempArray = (new Gson().fromJson(
						e.getResponseBodyAsString(),
						ErrorClienteUnicoDTO[].class));
				errorsList = Arrays.asList(tempArray);
		}

			
		} catch (RuntimeException e) {
			LOG.error("-- createClienteUnificado() RuntimeException", e.getMessage());
			
		}

		return errorsList;
	}

	/**
	 * valida que el cliente no se encuentre registrado previamente, compara nomre completo o nombre social fecha de nacimiento y RFC
	 * @param ClienteUnicoDTO dto representado de cliente unico
	 * @return regresa la lista de clientes que coincidan nulo en caso de no existir coincidencias
	 */
	@Override
	public List<ClienteUnicoDTO> validateRegisterCliente(
			ClienteUnicoDTO clienteUnico) {
		
		ClienteDTO clienteDTO = convertClienteUnicoDTOToClienteDTO(clienteUnico);
		ResponseEntity<Object> responseEntity = null;
		List<ClienteUnicoDTO> clientsList= new ArrayList<ClienteUnicoDTO>();
		List<ClienteDTO> clienteDTOList= null;
			
		List<ErrorClienteUnicoDTO> errorsList = null;
		try {
			StringBuilder urlBuilder = new StringBuilder()
					.append(DOMAIN_CLIENTES_API)
					.append(PATH_VALIDATE_REGISTER);
			HttpEntity<ClienteDTO> entity = new HttpEntity<ClienteDTO>(
					clienteDTO, getHeaders());

			responseEntity = restTemplate.exchange(urlBuilder.toString(),
					HttpMethod.POST, entity, Object.class);
			
			

		} 
	
		catch (HttpStatusCodeException e) {
		
			ClienteDTO[] tempArray = (new Gson().fromJson(
					e.getResponseBodyAsString(), ClienteDTO[].class));
			clienteDTOList = Arrays.asList(tempArray);
			for(ClienteDTO cliente:clienteDTOList){
				 clienteUnico = convertClienteDTOToClienteUnicoDTO(cliente);
				clientsList.add(clienteUnico);
			}
			LOG.error("-- createClienteUnificado()", e);
			
		} 
			
		catch (RuntimeException e) {
			LOG.error("-- createClienteUnificado()", e);
			
		}

		return clientsList;
	}

	
	
	/**
	 * Asocia el par de id recibidos como parámetros
	 * @param clienteId identificador de cliente dentro de cliente único
	 * @param seycosIdCliente identificador de cliente dentro de seycos
	 * @return
	 */
	private ResponseEntity<Integer> createClienteUnificado(Long clienteId,
			Long seycosIdCliente) {

		ResponseEntity<Integer> responseEntity = null;
		try {
			StringBuilder urlBuilder = new StringBuilder()
					.append(DOMAIN_CLIENTES_API)
					.append(PATH_UNIFICADO)
					.append("?")
					.append(ClientesApiServiceImpl.REQUEST_PARAM_CLIENTE_ID)
					.append("=")
					.append(clienteId)
					.append("&")
					.append(ClientesApiServiceImpl.REQUEST_PARAM_SEYCOS_ID_CLIENTE)
					.append("=").append(seycosIdCliente);
			HttpEntity<Object> entity = new HttpEntity<Object>(getHeaders());
			responseEntity = restTemplate.exchange(urlBuilder.toString(),
					HttpMethod.POST, entity, Integer.class);
		} catch (HttpStatusCodeException e) {
			LOG.error("-- createClienteUnificado()", e);
			
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- createClienteUnificado()", e);
			throw e;
		}
		return responseEntity;
	}

	/**
	 * Convirte los dto a JSON
	 * @param filter
	 * @param mapData
	 */
	@SuppressWarnings("rawtypes")
	private void converterListClientesToObject(FilterListClientes filter,
			Map<String, Object> response) {
		@SuppressWarnings("unchecked")
		List<Map> listClientes = (List<Map>) response.get(KEY_DATA);
		if (filter.isView()) {
			List<ClienteViewDTO> list = new LinkedList<ClienteViewDTO>();
			Gson gson = new Gson();
			for (Map map : listClientes) {
				String json = gson.toJson(map, LinkedHashMap.class);
				list.add(gson.fromJson(json, ClienteViewDTO.class));
			}
			response.put(KEY_DATA, list);
		} else {
			List<ClienteDTO> list = new LinkedList<ClienteDTO>();
			Gson gson = new Gson();
			for (Map map : listClientes) {
				String json = gson.toJson(map, LinkedHashMap.class);
				list.add(gson.fromJson(json, ClienteDTO.class));
			}
			response.put(KEY_DATA, list);
		}
	}

	

	/**
	 * Convirte convierte un objeto ClienteUnicoDTO a ClienteDTO
	 * @param clienteUnicoDTO objeto clienteUnicoDTO a convertir
	 * @return ClienteDTO resultante
	 */
	@Override
	public ClienteDTO convertClienteUnicoDTOToClienteDTO(
			ClienteUnicoDTO clienteUnicoDTO) {
		ClienteDTO clienteDTO = new ClienteDTO();

		// Domicilio
		clienteDTO.getPersona().getDomicilio()
				.setCalle(clienteUnicoDTO.getNombreCalle());
		clienteDTO.getPersona().getDomicilio().setNumero(clienteUnicoDTO.getNumeroDom());
		
		clienteDTO.getPersona().getDomicilio()
				.setColonia(clienteUnicoDTO.getNombreColonia());
		
		clienteDTO.getPersona().getDomicilio()
				.setCodigoPostal(clienteUnicoDTO.getCodigoPostal());
		
		clienteDTO.getPersona().getDomicilio()
				.setCveEstado(clienteUnicoDTO.getIdEstadoDirPrin());
		clienteDTO.getPersona().getDomicilio()
				.setCvePais(clienteUnicoDTO.getIdPaisString());

		clienteDTO.getPersona().getDomicilio()
				.setCveCiudad(clienteUnicoDTO.getIdMunicipioDirPrin());
		clienteDTO.getPersona().getDomicilio().setTipoDomicilio("PERS");

		// Domicilio fiscal
		clienteDTO.getPersona().getDomicilioFiscal()
				.setCalle(clienteUnicoDTO.getNombreCalleFiscal());
		clienteDTO.getPersona().getDomicilioFiscal()
		.setNumero(clienteUnicoDTO.getNumeroDomFiscal());
		clienteDTO.getPersona().getDomicilioFiscal()
				.setCodigoPostal(clienteUnicoDTO.getCodigoPostalFiscal());
		clienteDTO.getPersona().getDomicilioFiscal()
				.setColonia(clienteUnicoDTO.getNombreColoniaFiscal());
		clienteDTO.getPersona().getDomicilioFiscal()
				.setCveEstado(clienteUnicoDTO.getIdEstadoFiscal());
		clienteDTO.getPersona().getDomicilioFiscal()
				.setCveCiudad(clienteUnicoDTO.getIdMunicipioFiscal());
		clienteDTO.getPersona().getDomicilio().setTipoDomicilio("FISC");
		
		clienteDTO.getPersona().getDomicilioFiscal()
				.setCvePais(clienteUnicoDTO.getIdPaisFiscal());
		// generales
		if(clienteUnicoDTO.getNacionalidad()!=null&&!clienteUnicoDTO.getNacionalidad().isEmpty()){
			clienteDTO.getPersona().setCveNacionalidad(
					clienteUnicoDTO.getNacionalidad());

		}
		else{
		clienteDTO.getPersona().setCveNacionalidad(
				clienteUnicoDTO.getClaveNacionalidad());
		}
		if(clienteUnicoDTO.getGradoRiesgo()!=null&&"ALTO".compareTo(clienteUnicoDTO.getGradoRiesgo())==0){
			clienteDTO.setRiesgo("A");
			
		} else if(clienteUnicoDTO.getGradoRiesgo()!=null&&"BAJO".compareTo(clienteUnicoDTO.getGradoRiesgo())==0){
			
		}clienteDTO.setRiesgo("B");
	
		if(clienteUnicoDTO.getIdClienteUnico()!=null&&clienteUnicoDTO.getIdClienteUnico().length()>0){
			
			clienteDTO.setId(Long.valueOf(clienteUnicoDTO.getIdClienteUnico()));

		}

		clienteDTO.getPersona().setRfc(clienteUnicoDTO.getCodigoRFC());
		// contacto
		clienteDTO.getPersona().getComunicacion()
				.setCorreoElectronico(clienteUnicoDTO.getEmail());
		clienteDTO.getPersona().getComunicacion()
				.setFax(clienteUnicoDTO.getFaxContacto());
		
		if(clienteUnicoDTO.getTwitterContacto()!=null &&clienteUnicoDTO.getTwitterContacto().length()>0){
			ComunicacionRedSocialDTO redTwitter = new ComunicacionRedSocialDTO();
			redTwitter.setRedSocialId(2);
			redTwitter.setValor(clienteUnicoDTO.getTwitterContacto());
			clienteDTO.getPersona().getComunicacion().getListRedesSociales()
			.add(redTwitter);

		}
		if(clienteUnicoDTO.getFacebookContacto()!=null &&clienteUnicoDTO.getFacebookContacto().length()>0){
			
		ComunicacionRedSocialDTO redFaceBook = new ComunicacionRedSocialDTO();
		redFaceBook.setRedSocialId(1);
		redFaceBook.setValor(clienteUnicoDTO.getFacebookContacto());
		clienteDTO.getPersona().getComunicacion().getListRedesSociales()
				.add(redFaceBook);}
		
		
		clienteDTO.getPersona().getComunicacion()
				.setPaginaWeb(clienteUnicoDTO.getPaginaWebContacto());
		clienteDTO
				.getPersona()
				.getComunicacion()
				.setTelefonosAdicionales(
						clienteUnicoDTO.getTelefonosAdicionalesContacto());
		clienteDTO
				.getPersona()
				.getComunicacion()
				.setTelefonosAdicionales(
						clienteUnicoDTO.getTelefonosAdicionalesContacto());
		clienteDTO
				.getPersona()
				.getComunicacion()
				.setCorreosAdicionales(
						clienteUnicoDTO.getCorreosAdicionalesContacto());
		clienteDTO.getPersona().getComunicacion()
				.setExtension(clienteUnicoDTO.getExtensionContacto());
		clienteDTO
				.getPersona()
				.getComunicacion()
				.setTelefonoOficina(
						clienteUnicoDTO.getTelefonoOficinaContacto());
		clienteDTO.getPersona().getComunicacion()
				.setCelular(clienteUnicoDTO.getCelularContacto());
		clienteDTO.getPersona().getComunicacion()
				.setTelefonoCasa(clienteUnicoDTO.getNumeroTelefono());
		
		if ((clienteUnicoDTO.getClaveTipoPersonaString()!=null&&clienteUnicoDTO.getClaveTipoPersonaString().compareTo("1") == 0
			)||(clienteUnicoDTO.getClaveTipoPersona()!=null
				&&clienteUnicoDTO.getClaveTipoPersona().compareTo(Short.valueOf("1"))==0)) {
			clienteDTO.getPersona().setTipoPersona(PF);
			clienteDTO.getPersona().getFisica()
					.setCurp(clienteUnicoDTO.getCodigoCURP());
			clienteDTO.getPersona().getFisica()
					.setApellidoMaterno(clienteUnicoDTO.getApellidoMaterno());
			clienteDTO.getPersona().getFisica()
					.setApellidoPaterno(clienteUnicoDTO.getApellidoPaterno());
			clienteDTO.getPersona().getFisica()
					.setNombre(clienteUnicoDTO.getNombre());
			clienteDTO.getPersona().getFisica()
					.setSexo(clienteUnicoDTO.getSexo());
			clienteDTO.getPersona().getFisica()
					.setCurp(clienteUnicoDTO.getCodigoCURP());
			clienteDTO
					.getPersona()
					.getFisica()
					.setCveCiudadNacimiento(
							clienteUnicoDTO.getClaveCiudadNacimiento());
			clienteDTO
					.getPersona()
					.getFisica()
					.setCveEstadoNacimiento(
							clienteUnicoDTO.getClaveEstadoNacimiento());
			
			clienteDTO.getPersona().getFisica()
					.setCveOcupacion(clienteUnicoDTO.getClaveOcupacion());
			
			if(clienteUnicoDTO.getOcupacionCNSF() != null && !clienteUnicoDTO.getOcupacionCNSF().isEmpty()){
				clienteDTO.getPersona().getFisica()
					.setCveOcupacionCnsf(clienteUnicoDTO.getOcupacionCNSF());
			}else{
				clienteDTO.getPersona().getFisica()
				.setCveOcupacionCnsf(null);
			}
			
			
			clienteDTO
			.getPersona()
			.getFisica()
			.setCvePaisNacimiento(clienteUnicoDTO.getIdPaisNacimiento());
			
			clienteDTO.getPersona().getFisica()
					.setEstadoCivil(clienteUnicoDTO.getEstadoCivil());
			
			clienteDTO.getPersona().getFisica()
					.setListDependientes(clienteUnicoDTO.getListDependientes());
			
			if (clienteUnicoDTO.getFechaNacimiento() != null) {
				clienteDTO
						.getPersona()
						.getFisica()
						.setFechaNacimiento(
								formaterDateClienteUnico.format(clienteUnicoDTO
										.getFechaNacimiento()));
			}
			clienteDTO.getPersona().getFisica()
					.setEstadoCivil(clienteUnicoDTO.getEstadoCivil());
		}
		// else PM persna moral
		else {
			clienteDTO.getPersona().setTipoPersona(PM);
			clienteDTO.getPersona().getMoral()
					.setCveGiro(clienteUnicoDTO.getIdGiro());
			
			
			if(clienteUnicoDTO.getIdPaisConstitucion()!=null&&!clienteUnicoDTO.getIdPaisConstitucion().isEmpty()){
				
				clienteDTO
				.getPersona()
				.getMoral()
				.setCvePaisConstitucion(
						clienteUnicoDTO.getIdPaisConstitucion());

			}
			else{
				clienteDTO
				.getPersona()
				.getMoral()
				.setCvePaisConstitucion(null);
				
				
			}
			
			if(clienteUnicoDTO.getCveGiroCNSF()!=null&&!clienteUnicoDTO.getCveGiroCNSF().isEmpty()){
				clienteDTO
					.getPersona()
					.getMoral().setCveGiroCnsf(clienteUnicoDTO.getCveGiroCNSF());
			}else{
				clienteDTO
				.getPersona()
				.getMoral().setCveGiroCnsf(null);
			}
			if (clienteUnicoDTO.getFechaConstitucion() != null) {
				clienteDTO
						.getPersona()
						.getMoral()
						.setFechaConstitucion(
								formaterDateClienteUnico.format(clienteUnicoDTO
										.getFechaConstitucion()));
			}
			clienteDTO.getPersona().getMoral()
					.setSectorId(clienteUnicoDTO.getClaveSectorFinanciero());
			clienteDTO.getPersona().getMoral()
					.setRazonSocial(clienteUnicoDTO.getRazonSocial());
			if (clienteUnicoDTO.getIdRepresentante() != null) {

				PersonaSeycosDTO personaSeycos=null;
				try {	personaSeycos = personaSeycosFacadeRemote.findById(clienteUnicoDTO.getIdRepresentante().longValue());
						
				} catch (Exception e) {
					LOG.error("-- converting", e.getMessage());
					
				}
				
				if ((personaSeycos.getClaveTipoPersona()!=null
						&&personaSeycos.getClaveTipoPersona() ==1)
						) {
					clienteDTO
					.getPersona()
					.getMoral().getRepresentanteLegal().setTipoPersona(PF);
				}else {
					
					clienteDTO
					.getPersona()
					.getMoral().getRepresentanteLegal().setTipoPersona(PM);
				}
				clienteDTO
				.getPersona()
				.getMoral().getRepresentanteLegal()
				.setApellidoMaterno(personaSeycos.getApellidoMaterno());
				clienteDTO
				.getPersona()
				.getMoral().getRepresentanteLegal()
				.setApellidoPaterno(personaSeycos.getApellidoPaterno());
				clienteDTO
				.getPersona()
				.getMoral().getRepresentanteLegal()
				.setCurp(personaSeycos.getCodigoCURP());
				clienteDTO
				.getPersona()
				.getMoral().getRepresentanteLegal()
				.setCveEstadoNacimiento(personaSeycos.getClaveEstadoNacimiento());
				clienteDTO
				.getPersona()
				.getMoral().getRepresentanteLegal()
				.setCveNacionalidad(personaSeycos.getClavePaisNacimiento());
				
				if(personaSeycos.getFechaNacimiento()!=null){
					
					clienteDTO
					.getPersona()
					.getMoral().getRepresentanteLegal()
					.setFechaNacimiento(formaterDateClienteUnico.format(personaSeycos.getFechaNacimiento()));
				}
				else if(personaSeycos.getFechaConstitucion()!=null){
					clienteDTO
					.getPersona()
					.getMoral().getRepresentanteLegal()
					.setFechaNacimiento(formaterDateClienteUnico.format(personaSeycos.getFechaConstitucion()));
					
				}
				clienteDTO
				.getPersona()
				.getMoral().getRepresentanteLegal()
				.setNombre(personaSeycos.getNombre());
				clienteDTO
				.getPersona()
				.getMoral().getRepresentanteLegal()
				.setRfc(personaSeycos.getCodigoRFC());
				clienteDTO
				.getPersona()
				.getMoral().getRepresentanteLegal()
				.setSexo(personaSeycos.getSexo());
				
			}
			clienteDTO.setSituacion(clienteUnicoDTO.getTipoSituacionString());
		}

		return clienteDTO;
	}
	/**
	 * Convirte convierte un objeto  ClienteDTO a ClienteUnicoDTO
	 * @param ClienteDTO objeto clienteUnicoDTO a convertir
	 * @return ClienteUnicoDTO resultante
	 */
	@Override
	public ClienteUnicoDTO convertClienteDTOToClienteUnicoDTO(
			ClienteDTO clienteDTO) {
		ClienteUnicoDTO clienteUnicoDTO = new ClienteUnicoDTO();
		
		clienteUnicoDTO.setNombreCalle(clienteDTO.getPersona().getDomicilio()
				.getCalle());
		
		clienteUnicoDTO.setNumeroDom(clienteDTO.getPersona().getDomicilio()
				.getNumero());
		clienteUnicoDTO.setNombreColonia(clienteDTO.getPersona().getDomicilio()
				.getColonia());
		clienteUnicoDTO.setCodigoPostal(clienteDTO.getPersona().getDomicilio()
				.getCodigoPostal());

		clienteUnicoDTO.setIdEstado(initBigDecimal(clienteDTO.getPersona()
				.getDomicilio().getCveEstado()));
		if(clienteDTO.getPersona()
				.getDomicilio().getCveEstado()!=null){
		clienteUnicoDTO.setIdEstadoString(String.format("%05d",Long.valueOf(clienteDTO.getPersona()
				.getDomicilio().getCveEstado())));}
		clienteUnicoDTO.setIdPaisString(clienteDTO.getPersona().getDomicilio()
				.getCvePais());
		clienteUnicoDTO.setIdEstadoDirPrin(clienteDTO.getPersona()
				.getDomicilio().getCveEstado());
		
		clienteUnicoDTO.setIdMunicipio(initBigDecimal(clienteDTO.getPersona()
				.getDomicilio().getCveCiudad()));
		if(clienteDTO.getPersona()
				.getDomicilio().getCveCiudad()!=null){
		clienteUnicoDTO.setIdMunicipioString(String.format("%05d",Long.valueOf(clienteDTO.getPersona()
				.getDomicilio().getCveCiudad())));}
		clienteUnicoDTO.setIdMunicipioDirPrin(clienteDTO.getPersona()
				.getDomicilio().getCveCiudad());
		
		clienteUnicoDTO.setNumeroInterior(clienteDTO.getPersona()
				.getDomicilio().getNumero());
		// Domicilio fiscal
		clienteUnicoDTO.setNombreCalleFiscal(clienteDTO.getPersona()
				.getDomicilioFiscal().getCalle());
		clienteUnicoDTO.setNumeroDomFiscal(clienteDTO.getPersona()
				.getDomicilioFiscal().getNumero());
		
		clienteUnicoDTO.setIdColoniaFiscal(clienteDTO.getPersona()
				.getDomicilioFiscal().getColonia());
		
		clienteUnicoDTO.setCodigoPostalFiscal(clienteDTO.getPersona()
				.getDomicilioFiscal().getCodigoPostal());
		clienteUnicoDTO.setNombreColoniaFiscal(clienteDTO.getPersona()
				.getDomicilioFiscal().getColonia());
		clienteUnicoDTO.setIdEstadoFiscal(clienteDTO.getPersona()
				.getDomicilioFiscal().getCveEstado());
		clienteUnicoDTO.setIdMunicipioFiscal(clienteDTO.getPersona()
				.getDomicilioFiscal().getCveCiudad());
		clienteUnicoDTO.setIdPaisFiscal(clienteDTO.getPersona()
				.getDomicilioFiscal().getCvePais());
		clienteUnicoDTO.setRfcCobranza(clienteDTO.getPersona().getRfc());

		//
		// //generales
		clienteUnicoDTO.setIdToPersonaString(clienteDTO.getPersona()
				.getTipoPersona());
		clienteUnicoDTO.setNacionalidad(clienteDTO.getPersona()
				.getCveNacionalidad());
		clienteUnicoDTO.setClaveNacionalidad(clienteDTO.getPersona()
				.getCveNacionalidad());
		
		
		//
		clienteUnicoDTO.setIdCliente(initBigDecimal(this.clienteUnificado(
				clienteDTO.getId(), null).getBody()));
		clienteUnicoDTO.setIdClienteString(String.valueOf(this.clienteUnificado(
				clienteDTO.getId(), null).getBody()));
		clienteUnicoDTO.setIdClienteUnico(String.valueOf(clienteDTO.getId()));
		clienteUnicoDTO.setCodigoRFC(clienteDTO.getPersona().getRfc());
		// //contacto
		clienteUnicoDTO.setEmail(clienteDTO.getPersona().getComunicacion()
				.getCorreoElectronico());
		clienteUnicoDTO.setFaxContacto(clienteDTO.getPersona()
				.getComunicacion().getFax());
		
	if(clienteDTO.getRiesgo()!=null&&"A".compareTo(clienteDTO.getRiesgo())==0){
		clienteUnicoDTO.setGradoRiesgo("ALTO");
		
	}	else if(clienteDTO.getRiesgo()!=null&&"B".compareTo(clienteDTO.getRiesgo())==0){
		clienteUnicoDTO.setGradoRiesgo("BAJO");
		
	}
		
		
		// // 
		for (ComunicacionRedSocialDTO redesSociales : clienteDTO.getPersona()
				.getComunicacion().getListRedesSociales()) {
			if (redesSociales.getRedSocialId().compareTo(2) == 0) {
				clienteUnicoDTO.setTwitterContacto(redesSociales.getValor());

			}
			if (redesSociales.getRedSocialId().compareTo(1) == 0) {
				clienteUnicoDTO.setFacebookContacto(redesSociales.getValor());
			}
		}
		clienteUnicoDTO.setPaginaWebContacto(clienteDTO.getPersona()
				.getComunicacion().getPaginaWeb());
		clienteUnicoDTO.setTelefonosAdicionalesContacto(clienteDTO.getPersona()
				.getComunicacion().getTelefonosAdicionales());
		clienteUnicoDTO.setCorreosAdicionalesContacto(clienteDTO.getPersona()
				.getComunicacion().getCorreosAdicionales());
		clienteUnicoDTO.setCorreosAdicionalesContacto(clienteDTO.getPersona()
				.getComunicacion().getCorreosAdicionales());
		clienteUnicoDTO.setCorreosAdicionalesContacto(clienteDTO.getPersona()
				.getComunicacion().getCorreosAdicionales());
		clienteUnicoDTO.setExtensionContacto(clienteDTO.getPersona()
				.getComunicacion().getExtension());
		clienteUnicoDTO.setTelefonoOficinaContacto(clienteDTO.getPersona()
				.getComunicacion().getTelefonoOficina());
		clienteUnicoDTO.setCelularContacto(clienteDTO.getPersona()
				.getComunicacion().getCelular());
		clienteUnicoDTO.setNumeroTelefono(clienteDTO.getPersona()
				.getComunicacion().getTelefonoCasa());

		if (clienteDTO.getPersona().getTipoPersona().compareTo(PF) == 0) {
			clienteUnicoDTO.setCodigoCURP(clienteDTO.getPersona().getFisica()
					.getCurp());
			clienteUnicoDTO.setIdToPersona(BigDecimal.ONE);
			clienteUnicoDTO.setClaveTipoPersona((short) 1);
			clienteUnicoDTO.setClaveTipoPersonaString("1");
			clienteUnicoDTO.setIdToPersonaString("1");
			clienteUnicoDTO.setApellidoMaterno(clienteDTO.getPersona()
					.getFisica().getApellidoMaterno());
			clienteUnicoDTO.setApellidoPaterno(clienteDTO.getPersona()
					.getFisica().getApellidoPaterno());
			clienteUnicoDTO.setNombre(clienteDTO.getPersona().getFisica()
					.getNombre());
			clienteUnicoDTO.setNombreCompleto(clienteUnicoDTO.getNombre()+" "+clienteUnicoDTO.getApellidoPaterno()+" "+clienteUnicoDTO.getApellidoMaterno());

			clienteUnicoDTO.setSexo(clienteDTO.getPersona().getFisica()
					.getSexo());
			clienteUnicoDTO.setCodigoCURP(clienteDTO.getPersona().getFisica()
					.getCurp());
			clienteUnicoDTO.setClaveCiudadNacimiento(clienteDTO.getPersona()
					.getFisica().getCveCiudadNacimiento());
			clienteUnicoDTO.setClaveEstadoNacimiento(clienteDTO.getPersona()
					.getFisica().getCveEstadoNacimiento());
			clienteUnicoDTO.setClaveOcupacion(clienteDTO.getPersona()
					.getFisica().getCveOcupacion());
			clienteUnicoDTO.setOcupacionCNSF(clienteDTO.getPersona()
					.getFisica().getCveOcupacionCnsf());
			clienteUnicoDTO.setIdPaisNacimiento(clienteDTO.getPersona()
					.getFisica().getCvePaisNacimiento());
			
			clienteUnicoDTO.setEstadoCivil(clienteDTO.getPersona().getFisica()
					.getEstadoCivil());
			clienteUnicoDTO.setListDependientes(clienteDTO.getPersona()
					.getFisica().getListDependientes());
			clienteUnicoDTO
					.setFechaNacimiento(formatDateClienteUnico(clienteDTO
							.getPersona().getFisica().getFechaNacimiento()));
			clienteUnicoDTO.setEstadoCivil(clienteDTO.getPersona().getFisica()
					.getEstadoCivil());
			
			
		}
		// else PM persna moral
		else {
			clienteUnicoDTO.setIdToPersona(new BigDecimal("2"));
			clienteUnicoDTO.setClaveTipoPersona((short) 2);
			clienteUnicoDTO.setClaveTipoPersonaString("2");
			clienteUnicoDTO.setIdToPersonaString("2");
			clienteUnicoDTO.setCveGiroCNSF(clienteDTO.getPersona().getMoral().getCveGiroCnsf());
			clienteUnicoDTO.setIdGiro(clienteDTO.getPersona().getMoral()
					.getCveGiro());
			clienteUnicoDTO.setIdPaisConstitucion(clienteDTO.getPersona()
					.getMoral().getCvePaisConstitucion());
			clienteUnicoDTO
					.setFechaConstitucion(formatDateClienteUnico(clienteDTO
							.getPersona().getMoral().getFechaConstitucion()));
			clienteUnicoDTO.setClaveSectorFinanciero(clienteDTO.getPersona()
					.getMoral().getSectorId());
			clienteUnicoDTO.setRazonSocial(clienteDTO.getPersona().getMoral()
					.getRazonSocial());
			clienteUnicoDTO.setNombreCompleto(clienteUnicoDTO.getRazonSocial());
			clienteUnicoDTO.setRazonSocialFiscal(clienteDTO.getPersona().getMoral()
					.getRazonSocial());
			clienteUnicoDTO.setFechaNacimientoFiscal(formatDateClienteUnico(clienteDTO
					.getPersona().getMoral().getFechaConstitucion()));
			
			
			
			

			clienteUnicoDTO.setTipoSituacionString(clienteDTO.getSituacion());
		}

		return clienteUnicoDTO;
	}

	
	/**
	 * Convierte un ClienteDTO en formato JSON a un objeto ClienteUnicoDTO
	 * @param jsonClienteDTO cadena que contiene un objecto ClienteDTO
	 * @return ClienteUnicoDTO resultante
	 * 
	 */
	@Override
	public ClienteUnicoDTO convertClienteDTOToClienteUnicoDTO(
			String jsonClienteDTO) {
		

		ClienteDTO clienteDTO = fromJsonClienteDTO(jsonClienteDTO);
		ClienteUnicoDTO clienteUnicoDTO = convertClienteDTOToClienteUnicoDTO(clienteDTO);
		return clienteUnicoDTO;
	}

	/**
	 * Convierte un ClienteDTO en formato JSON a un objeto ClienteDTO
	 * @param jsonClienteDTO cadena que contiene un objecto ClienteDTO
	 * @return ClienteDTO resultante
	 * 
	 */
	private ClienteDTO fromJsonClienteDTO(String jsonClienteDTO) {
		return new Gson().fromJson(jsonClienteDTO, ClienteDTO.class);
	}

	private Date formatDateClienteUnico(String dateString) {
		Date resulDate = null;
		try {
			resulDate = formaterDateClienteUnico.parse(dateString);

		} catch (ParseException e) {

			LOG.error("-- formatDateClienteUnico()", e.getMessage());		}

		return resulDate;
	}
	/**
	 * Inicializa un Bigdecimal en base a objeto que recibe como parámetro
	 * @param objectToConvert valor para preiniclizar
	 * @return BigDecimal resultante
	 * 
	 */
	private BigDecimal initBigDecimal(Object objectToConvert) {
		BigDecimal number = null;
		if (objectToConvert == null) {
			return null;
		}
		try {
			if (objectToConvert instanceof String) {
				number = new BigDecimal((String) objectToConvert);

			}
			if (objectToConvert instanceof Long) {
				number = new BigDecimal((Long) objectToConvert);

			}
			if (objectToConvert instanceof Integer) {
				number = new BigDecimal((Integer) objectToConvert);

			}
			if (objectToConvert instanceof Short) {
				number = new BigDecimal((Short) objectToConvert);

			}

		} catch (Exception e) {

			LOG.error("-- initBigDecimal", e.getMessage());
		}

		return number;
	}

	/**
	 * 
	 * @param token
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * 
	 * @param urlBuilder
	 * @param filter
	 */
	private void setUrlFilterParams(StringBuilder urlBuilder,
			FilterListClientes filter) {
		urlBuilder.append("?view=").append(filter.isView());
		if (filter.getPage() > 0) {
			urlBuilder.append("&page=").append(filter.getPage());
		}
		if (filter.getPerPage() > 0) {
			urlBuilder.append("&perPage=").append(filter.getPerPage());
		}
		if (filter.getTipoPersona() != null) {
			urlBuilder.append("&tipoPersona=").append(filter.getTipoPersona());
		}
		if (filter.getRfc() != null) {
			urlBuilder.append("&rfc=").append(filter.getRfc());
		}
		if (filter.getNombre() != null) {
			urlBuilder.append("&nombre=").append(filter.getNombre().toUpperCase());
		}
		if (filter.getApellidoPaterno() != null) {
			urlBuilder.append("&apellidoPaterno=").append(
					filter.getApellidoPaterno().toUpperCase());
		}
		if (filter.getApellidoMaterno() != null) {
			urlBuilder.append("&apellidoMaterno=").append(
					filter.getApellidoMaterno().toUpperCase());
		}
		if (filter.getCurp() != null) {
			urlBuilder.append("&curp=").append(filter.getCurp());
		}
		if (filter.getRazonSocial() != null) {
			urlBuilder.append("&razonSocial=").append(filter.getRazonSocial().toUpperCase());
		}
		if (filter.getFechaConstitucion() != null) {
			urlBuilder.append("&fechaConstitucion=").append(
					filter.getFechaConstitucion());
		}
	}

	/**
	 * Obtiene el token asociado al usuario 
	 * @return token asociado al usuario en la applicación
	 */
	private String getTokenSession(){
		
		this.asmRemote = new ASMWSC(sistemaContext.getAsmWscContext());
		
		
		try {
			token=	asmRemote.getLoggedUserView( usuarioService.getUsuarioActual().getNombreUsuario(),Integer.valueOf(usuarioService.getUsuarioActual().getIdSesionUsuario())).getToken();
		} catch (NumberFormatException e) {
			LOG.error("-- getTokenSession()", e.getMessage());
		} catch (SystemException e) {
			LOG.error("-- getTokenSession()", e.getMessage());
		}
		
		
		return token;
	}

	
	/**
	 * Inicializa las cabeceras para las peticiones al servicio REST de tipo http
	 * @return cabeceras para peticiones REST
	 */
	
	private HttpHeaders getHeaders() {
		HttpHeaders headers = new HttpHeaders();
		headers.add(AUTHENTICATE_TOKEN, getTokenSession());
		headers.add("Content-Type", "application/json; charset=UTF-8");
		return headers;
	}
	/**
	 * Inicializa las cabeceras para las peticiones al servicio REST considerando que se va a descargar un archivo PDF
	 * @return cabeceras para peticiones REST
	 */
	private HttpHeaders getHeadersPdf() {
		HttpHeaders headers = new HttpHeaders();
		headers.add(AUTHENTICATE_TOKEN, getTokenSession());
		headers.add("Content-Type", "application/pdf; charset=UTF-8");
		return headers;
	}

	/**
	 * Busca un cliente en base al su identificador
	 * @param idCliente identificador de cliente
	 * @return ClienteUnicoDTO el cliente que coincide con el identificador
	 */
	@Override
	public ClienteUnicoDTO findClienteUnicoById(Long idCliente) {
		ResponseEntity<ClienteDTO> clientEntity = findById(idCliente);
		ClienteDTO clienteDTO = clientEntity.getBody();
		return convertClienteDTOToClienteUnicoDTO(clienteDTO);
	}

	/**
	 * Convierte el objeto que se usa como filtro dentro de midas al objeto filtro en cliente unico	
	 * @param filter filtro de midas
	 * @return filtro en cliente unico en lista
	 */
	@Override
	public List<ClienteUnicoDTO> getListClientesByFilter(ClienteUnicoDTO filter) {
		List<ClienteUnicoDTO> listClientes = new ArrayList<ClienteUnicoDTO>();
		ResponseEntity<Map<String, Object>> mapClientes = getListClientesByFilter(convertClienteUnicoDTOToFilterListClientes(filter));
		List<ClienteViewDTO> listClientesDTO = (List<ClienteViewDTO>) mapClientes
				.getBody().get(KEY_DATA);

		for (ClienteViewDTO clienteDTO : listClientesDTO) {
			ClienteUnicoDTO clienteUnicoDTO = new ClienteUnicoDTO();
			clienteUnicoDTO.setIdCliente(initBigDecimal(clienteDTO
					.getSeycosIdCliente()));
			clienteUnicoDTO
					.setIdClienteUnico(String.valueOf(clienteDTO.getId()));
			if (clienteDTO.getNombre() != null) {
				clienteUnicoDTO.setNombreCompleto(clienteDTO.getNombre() + " "
						+ clienteDTO.getApellidoPaterno() + " "
						+ clienteDTO.getApellidoMaterno() + " ");

			} else {
				clienteUnicoDTO.setNombreCompleto(clienteDTO.getRazonSocial());
			}

			clienteUnicoDTO.setCodigoRFC(clienteDTO.getRfc());
			clienteUnicoDTO.setCodigoCURP(clienteDTO.getCurp());
			clienteUnicoDTO.setNombreCalleFiscal(clienteDTO
					.getDomicilioFiscal());

			listClientes.add(clienteUnicoDTO);
		}

		return listClientes;
	}
	/**
	 * Convierte el objeto que se usa como filtro dentro de midas al objeto filtro en cliente unico	
	 * @param filter filtro de midas
	 * @return filtro en cliente unico en dto
	 */
	private FilterListClientes convertClienteUnicoDTOToFilterListClientes(
			ClienteUnicoDTO filter) {

		FilterListClientes filterList = new FilterListClientes();

		if (filter != null) {
			
			if(filter.getApellidoMaterno()!=null&&!filter.getApellidoMaterno().isEmpty()){
				
				filterList.setApellidoMaterno("%"+filter.getApellidoMaterno().toUpperCase()+"%");
			}
			if(filter.getApellidoMaterno()!=null&&!filter.getApellidoPaterno().isEmpty()){
				
				filterList.setApellidoPaterno("%"+filter.getApellidoPaterno().toUpperCase()+"%");
			}
			if(filter.getCodigoCURP()!=null&&!filter.getCodigoCURP().isEmpty()){
				
				filterList.setCurp("%"+filter.getCodigoCURP().toUpperCase()+"%");
			}
			

			if (filter.getClaveTipoPersonaString()!=null&&filter.getClaveTipoPersonaString().equals("1")) {
				
				filterList.setTipoPersona(PF);
			} else if (filter.getClaveTipoPersonaString()!=null&&filter.getClaveTipoPersonaString().equals("2")) {

				filterList.setTipoPersona(PM);
			}else if (filter.getClaveTipoPersona()==1)
			{	
				filterList.setTipoPersona(PF);
				
			}else if (filter.getClaveTipoPersona()==2)
			{
				filterList.setTipoPersona(PM);
				
			}	else {filterList.setTipoPersona(PF);}

			if (filter.getFechaConstitucion() != null) {
				filterList.setFechaConstitucion(formaterDateClienteUnico
						.format(filter.getFechaConstitucion()));
			}
			if(filter.getNombre()!=null&&!filter.getNombre().isEmpty()){
				
				filterList.setNombre("%"+filter.getNombre()+"%");
					
			}
			if(filter.getRazonSocial()!=null&&!filter.getRazonSocial().isEmpty()){
				
				filterList.setRazonSocial("%"+filter.getRazonSocial()+"%");
			}
			if(filter.getRfcCobranza()!=null&&!filter.getRazonSocial().isEmpty()){
				
				filterList.setRfc("%"+filter.getRfcCobranza()+"%");	
			}
			
			
		}
		filterList.setPerPage(PER_PAGINA);
		filterList.setPage(1);
		filterList.setView(true);

		return filterList;

	}
	
	/**
	 * Método usado para persistir los cambios realizados en el cliente tanto en midas auto como cliente único
	 * @param entity, cliente a persistir
	 * @param nombreUsuario, usuario que esta llevando acabo la transaccion
	 * @param validaNegocio si validaNegocio=true se validan las reglas de negocio si validaNegocio =false no se validan
	 * 
	 */
	@Transactional
	@Override
	public ClienteUnicoDTO saveFullDataClienteUnico(ClienteUnicoDTO entity,
			String nombreUsuario, boolean validaNegocio)
			throws ClientSystemException, Exception,RegisterClientException {
		List<ErrorClienteUnicoDTO> erroresList = validateCliente(entity);
		String temp=
			new Gson().toJson(entity).toString();
		System.out.println(temp);
		if (erroresList == null||erroresList.isEmpty()) {
			List<ClienteUnicoDTO> cliestesRepetidosList=null;
			if(entity.getVerificarRepetidos()!=null&&entity.getVerificarRepetidos()){
				cliestesRepetidosList=	validateRegisterCliente(entity);	
			
			}
			if(cliestesRepetidosList==null|| cliestesRepetidosList.isEmpty()){
				
				ClienteGenericoDTO res =saveFullData(entity, nombreUsuario, false);
				
				entity.setIdCliente(res.getIdCliente());
				ResponseEntity<Object> responseEntity = null;
				try {
				
				responseEntity = 	saveCliente(entity);
				}catch(HttpStatusCodeException e){
					List<ClienteDTO> clienteDTOList= null;
					List<ClienteUnicoDTO> clientsList= new ArrayList<ClienteUnicoDTO>();
					ClienteUnicoDTO clienteUnico=null;
					ClienteDTO[] tempArray = (new Gson().fromJson(
							e.getResponseBodyAsString(), ClienteDTO[].class));
					clienteDTOList = Arrays.asList(tempArray);
					for(ClienteDTO cliente:clienteDTOList){
						 clienteUnico = convertClienteDTOToClienteUnicoDTO(cliente);
						clientsList.add(clienteUnico);
					}
					String erroresString=
						new Gson().toJson(cliestesRepetidosList).toString();
						
						RegisterClientException	eR =new RegisterClientException(erroresString) ;
						throw eR;
					
				}
				if(entity.getIdClienteUnico()!=null&&entity.getIdCliente()!=null){
					Map enttiMap = (Map) responseEntity.getBody();
					entity.setIdClienteUnico(enttiMap.get("id").toString());
				}else {
					entity.setIdCliente(initBigDecimal(entity.getIdClienteUnico()));
					
				} 

			}else{
				
				String erroresString=
				new Gson().toJson(cliestesRepetidosList).toString();
				
				RegisterClientException	e =new RegisterClientException(erroresString) ;
				throw e;
					
				}
				
		}else {
			
			String erroresString ="";
			for (ErrorClienteUnicoDTO error:erroresList ){
				erroresString+=	error.getLabel()+":"+error.getValue()+"\n"; 
			}
			ValidateClientException	e =new ValidateClientException(erroresString) ;
	
			throw e;
		}

		return entity;
	}

	/**
	 * Obtiene la lista de ococupaciones de la CNSF contenidas en el catalogo de cliente único
	 * @return la lista de ocupaciones de la CNSF
	 */
	@Override
	public List<OcupacionCNSFDTO> getGiros() {
		
		List <OcupacionCNSFDTO> list = new ArrayList<OcupacionCNSFDTO>();
		ResponseEntity<Object> responseEntity = null;
		try {
			String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
					.append(PATH_ACTIVIDADES_LIST).toString();
			HttpEntity<List<OcupacionCNSFDTO>> entity = new HttpEntity<List<OcupacionCNSFDTO>>(
					list, getHeaders());
			responseEntity = restTemplate.exchange(url, HttpMethod.GET,
					entity, Object.class);
			list=	(List<OcupacionCNSFDTO>)responseEntity.getBody();
		} catch (HttpStatusCodeException e) {
			LOG.error("-- List Actividades()", e);
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- List Actividades()", e);
			throw e;
		}
		return list;
		
	}
	
	/**
	 * Crea el expediente de la documentacion existente y la faltante del usuario
	 * @param exception, la excepcion del arrojada por el método getDocumentacionFaltante
	 * @return lista con estatus de la documentación (expediente al día)
	 */
	@Override 
	public List<DocumentoDTO> getDocumentos(String exception) {
		
		ErrorDocumentoDTO []erroresDocumen=(new Gson().fromJson(
				exception,
				ErrorDocumentoDTO[].class));
		
		DocumentoDTO[] arrayDocument = new DocumentoDTO[11];
		List<DocumentoDTO> list= new ArrayList<DocumentoDTO>();
		List<DocumentoDTO> listTemp=null;
		ResponseEntity<DocumentoDTO[]> responseEntity = null;
		try {
			String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
					.append(PATH_DOCUMENTOS).toString();
			HttpEntity<DocumentoDTO[]> entity = new HttpEntity<DocumentoDTO[]>(
					arrayDocument, getHeaders());
			responseEntity = restTemplate.exchange(url, HttpMethod.GET,
					entity, DocumentoDTO[].class);
			
			
			listTemp = Arrays.asList(responseEntity.getBody());
			for(int i=0;i<erroresDocumen.length;i++){
				
				for (DocumentoDTO documento:listTemp){
					
					if(erroresDocumen[i].getLabel().contains("DOCU.")&&
							erroresDocumen[i].getLabel().contains(documento.getNombre())){
						documento.setRequerido(true);
						break;
					}
					
					
				}
				
			}
			
			for (DocumentoDTO documento:listTemp){
				if(documento.getRequerido()==null){
					documento.setRequerido(false);
					
				}
				if(documento.getNombre().compareTo("DECLARACION_FIRMA")==0){
					documento.setNombre("Declaración firmada");
					
				}
				if(documento.getNombre().compareTo("IDENTIFICACION")==0){
					documento.setNombre("Identificación Oficial con fotografía");
					
				}
				if(documento.getNombre().compareTo("CURP")==0){
					documento.setNombre("Copia CURP");
					
				}
				
				if(documento.getNombre().compareTo("RFC")==0){
					documento.setNombre("Copia Cédula Fiscal o R.F.C.");
					
				}
				if(documento.getNombre().compareTo("FIRMA_ELECTRONICA")==0){
					documento.setNombre("Comprobante Firma electrónica");
					
				}if(documento.getNombre().compareTo("COMPROBANTE_DOMICILIO")==0){
					documento.setNombre("Comprobante Domicilio");
					
				}
				if(documento.getNombre().compareTo("ACREDITACION_MIGRATORIA")==0){
					documento.setNombre("Documento con que acredita su calidad migratoria");
					
				}
				if(documento.getNombre().compareTo("PASAPORTE")==0){
					documento.setNombre("Pasaporte");
					
				}
				if(documento.getNombre().compareTo("ACREDITACION_ESTANCIA_MEXICO")==0){
					documento.setNombre("Documento con que acredita su legal estancia en México");
					
				}
				if(documento.getNombre().compareTo("ACREDITACION_DOMICILIO_PERMANENTE")==0){
					documento.setNombre("Documento con que accredita su domicilio permanente");
					
				}
				if(documento.getNombre().compareTo("ACREDITACION_LEGAL_EXISTENCIA")==0){
					documento.setNombre("Documento que acredite su legal existencia (Actas)");
					
				}
				
			}
			for (DocumentoDTO documento:listTemp){
				
				if(documento.getNombre().compareTo("Documento que acredite su legal existencia (Actas)")==0){
					list.add(6, documento);
					
					
				}else{
					list.add(documento);
					
				}
			}
			
		} catch (HttpStatusCodeException e) {
			LOG.error("-- List Actividades()", e);
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- List Actividades()", e);
			throw e;
		}
		return list;
		
	}
	/**
	 * Lista la documentación en que tiene un cliente determinado
	 * @param idCliente identificador del cliente
	 * @return La lista de documentos que tiene el cliente
	 */

	@Override 
	public List<DocumentoDTO> getDocumentacion(Long idCliente) {
		
		DocumentoDTO[] arrayDocument = new DocumentoDTO[11];
		List<DocumentoDTO> list=null;	
		ResponseEntity<DocumentoDTO[]> responseEntity = null;
		
		
		 ResponseEntity<ClienteDTO> cliente= findById(idCliente);
		try {
			String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
					.append(PATH_SAVE_DOCUMENT).toString();
			HttpEntity<DocumentoDTO[]> entity = new HttpEntity<DocumentoDTO[]>(
					arrayDocument, getHeaders());
			responseEntity = restTemplate.exchange(url, HttpMethod.GET,
					entity, DocumentoDTO[].class,cliente.getBody().getPersona().getId());
			list = Arrays.asList(responseEntity.getBody());
			
			
		} catch (HttpStatusCodeException e) {
			LOG.error("-- List documentacion()", e);
			throw e;
		} catch (RuntimeException e) {
			LOG.error("-- List documentacion()", e);
			throw e;
		}
		return list;
		
	}
	/**
	 * Verifica si a un determinado cliente le falta documentación
	 * @param idCliente identificador de cliente
	 * @throws DocumentationException la documentación faltante 
	 */
	
	@Override 
	public void getDocumentacionFaltante(Long idCliente) throws DocumentationException {
		
		DocumentoDTO[] arrayDocument = new DocumentoDTO[11];
		List<DocumentoDTO> list=null;
		ResponseEntity<ClienteDTO> clientEntity = findById(idCliente);
		ResponseEntity<DocumentoDTO[]> responseEntity = null;
		try {
			String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
					.append(PATH_INFORMATION).toString();
			HttpEntity<DocumentoDTO[]> entity = new HttpEntity<DocumentoDTO[]>(
					arrayDocument, getHeaders());
			responseEntity = restTemplate.exchange(url, HttpMethod.GET,
					entity, DocumentoDTO[].class,clientEntity.getBody().getPersona().getId());
			list = Arrays.asList(responseEntity.getBody());
		} catch (HttpStatusCodeException e) {
			
			if(e.getResponseBodyAsString().contains("documento")){
				throw new DocumentationException(e.getResponseBodyAsString());

				
			}
			
		} catch (RuntimeException e) {
			LOG.error("-- List documentacion()", e);
			throw e;
		}
		
		
	}
	/**
	 * Verifica si a un determinado cliente le falta entrevista
	 * @param idCliente identificador de cliente
	 * @throws InterviewException si no cuenta con entrevista 
	 */
	
public	EntrevistaDTO findInterViews(Long idCliente/*id Seycos*/
		) throws InterviewException{
	
	ResponseEntity<Long> idClienteUnico= clienteUnificado(null,idCliente);
	ResponseEntity<EntrevistaDTO> responseEntity = null;
	EntrevistaDTO entrevista= new EntrevistaDTO();
	
	try {
		StringBuilder urlBuilder = new StringBuilder()
				.append(DOMAIN_CLIENTES_API)
				.append(PATH_PARAMETER_ID)
				.append(PATH_INTERVIEWS);
		
		HttpEntity<EntrevistaDTO> entity = new HttpEntity<EntrevistaDTO>(
				entrevista, getHeaders());

		responseEntity = restTemplate.exchange(urlBuilder.toString(),
				HttpMethod.GET,entity ,EntrevistaDTO.class,idClienteUnico.getBody());
		entrevista=responseEntity.getBody();
	} catch (HttpStatusCodeException e) {
		
		
		throw new InterviewException("El usuario no cuenta con entrevista");
		

		
	} catch (RuntimeException e) {
		throw new InterviewException("El usuario no cuenta con entrevista");
		
	}

	return  entrevista;
	
}
/**
 * Método para guardar la entrevista dentro de la aplicación de cliente único
 * @param idCliente identificador de cliente
 * @param entrevista La entrevista
 * @return Regresa la entrevista
 */
public	EntrevistaDTO createInterview(Long idCliente,EntrevistaDTO entrevista){
	ResponseEntity<Long> idClienteUnico= clienteUnificado(null,idCliente);
	ResponseEntity<EntrevistaDTO> responseEntity = null;
	try {
		StringBuilder urlBuilder = new StringBuilder()
				.append(DOMAIN_CLIENTES_API)
				.append(PATH_PARAMETER_ID)
				.append(PATH_INTERVIEWS);
		
		HttpEntity<EntrevistaDTO> entity = new HttpEntity<EntrevistaDTO>(
				entrevista, getHeaders());

		responseEntity = restTemplate.exchange(urlBuilder.toString(),
				HttpMethod.POST,entity ,EntrevistaDTO.class,idClienteUnico.getBody());
		entrevista=responseEntity.getBody();
	} catch (HttpStatusCodeException e) {
		
		LOG.error("-- createInterview()", e.getMessage());
		
		

		
	} catch (RuntimeException e) {
		
		LOG.error("-- createInterview()", e.getMessage());
	}

	
	return entrevista;
}
	/**
	 * Genera el pdf correspendiente a la entrevista del cliente
	 * @param idCliente identificador del cliente
	 * @param ramo Ramo de los seguros por ejemplo Daños = D, Autos A, etc
	 * @param moneda, clave de la moneda p.e. pesos =484 dolares 840
	 * @param primaEmitida corresponde al monto de la prima neta
	 * @return el stream correspondiente al pdf de entrevista
	 */
public	byte[] createPDFInterviewApp(Long idCliente,String ramo,String moneda,String primaEmitida,final OutputStream os){
	
	byte[] res=null;
	ResponseEntity<Object> responseEntity = null;
	try {
		StringBuilder urlBuilder = new StringBuilder()
				.append(DOMAIN_CLIENTES_API)
				.append(PATH_PDF);
				
		
		UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(urlBuilder.toString());
		builder.queryParam("primaEmitida",primaEmitida);
		builder.queryParam("ramo",ramo);
		builder.queryParam("moneda",moneda);
		builder.queryParam("token",token);
		res = restTemplate.getForObject(builder.build().toString(), byte[].class,idCliente);
		
		
	} catch (HttpStatusCodeException e) {
		
		LOG.error("-- createPDFInterviewApp()", e.getMessage());
			
	} catch (RuntimeException e) {
		
		LOG.error("-- createPDFInterviewApp()", e.getMessage());
	}

	return res;
}

/**
 * Verifica si un cliente se encuentra en peps, esta validación se reliaza dentro de la aplicación cliente unico y no entera de su resultado a MIDAS
 *param idCliente identificador del cliente
 *param idPoliza, identificador de la poliza contratada
 */
@Autowired
public void validatePeps(Long idCliente,String idPoliza){
ResponseEntity<Long> idClienteUnico= clienteUnificado(null,idCliente);
	
	try {
		String res="";
		StringBuilder urlBuilder = new StringBuilder()
				.append(DOMAIN_CLIENTES_API)
				.append(PATH_PARAMETER_ID)
				.append(PATH_PEPS)
				.append("?numPoliza="+idPoliza);
		
		HttpEntity<Object> entity = new HttpEntity<Object>(res, getHeaders());
		restTemplate.exchange(urlBuilder.toString(), 
							 HttpMethod.POST, 
							 entity,
							 Object.class, 
							 idClienteUnico.getBody());
		
	} catch (HttpStatusCodeException e) {
		//do nothig
		LOG.trace(e.getMessage());
	} catch (RuntimeException e) {
		LOG.trace(e.getMessage());
	}
	
}
@Autowired
@Qualifier("personaSeycosFacadeRemoteEJB")
public void setPersonaSeycosFacadeRemote(
		PersonaSeycosFacadeRemote personaSeycosFacadeRemote) {
	this.personaSeycosFacadeRemote = personaSeycosFacadeRemote;
}

/**
 * Recupera la liga de la aplicación de fortimax correspondiente al cliente
 * @param idCliente identificador del cliente
 * @return la url de la gabeta de la documentación del cliente
 * 
 */

@Override
public String getLinkFortmax(Long idCliente) {
	String res =""; 
	ResponseEntity<Object> responseEntity = null;
	try {
		String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
				.append(PATH_PARAMETER_ID)
				.append(PATH_FORTIMAX).toString();
		HttpEntity<Object> entity = new HttpEntity<Object>(
				res, getHeaders());
		responseEntity = restTemplate.exchange(url, HttpMethod.GET,
				entity, Object.class,idCliente);
		Map	resMap =(Map)responseEntity.getBody();
		res=(String)resMap.get("link");
		
		
	} catch (HttpStatusCodeException e) {
		LOG.error("-- getLinkFortmax", e.getMessage());
		
	} catch (RuntimeException e) {
		LOG.error("-- getLinkFortmax", e.getMessage());
		
	}
	
	return res;
	
}

/**
 * 
 * Almacena los datos capturados en la pantalla de documentación 
 * @param idCliente identificador del cliente
 * @param documentos lista de documentos a guardar
 */
@Override
public void saveDocument(Long idCliente,List<DocumentoDTO> documentos) {
	
	 ResponseEntity<Long> resp=clienteUnificado(null,idCliente);
	 
	 if(documentos!=null){
	 for(DocumentoDTO documento: documentos){
		 
		 
		 if(documento.getFechaVigencia()!=null){
			 
			String[]temp =documento.getFechaVigencia().split("/");
			if(temp!=null &&temp.length==3){
				documento.setFechaVigencia(temp[2]+"-"+temp[1]+"-"+temp[0]);

			}
		 }
	 }
	 }
	
	 ResponseEntity<ClienteDTO> cliente= findById(resp.getBody());
	 
	 try {
		String url = new StringBuilder().append(DOMAIN_CLIENTES_API)
				.append(PATH_SAVE_DOCUMENT)
				.toString();
		HttpEntity<List<DocumentoDTO>> entity = new HttpEntity<List<DocumentoDTO>>(
				documentos, getHeaders());
		restTemplate.exchange(url, HttpMethod.POST,
				entity, Object.class,cliente.getBody().getPersona().getId());
		
	} catch (HttpStatusCodeException e) {
		
		LOG.error("-- saveDocument()", e.getMessage());
		
		throw e;
	} catch (RuntimeException e) {
		LOG.error("-- saveDocument()", e.getMessage());
		
		throw e;
	}	
}
	
}
