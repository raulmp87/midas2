package mx.com.afirme.midas2.service.impl.siniestros.indemnizacion.perdidatotal;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.siniestros.indemnizacion.perdidatotal.PerdidaTotalDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.ClaveSubCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.PerdidaTotalFiltro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RolesIndemnizacionAutorizacion;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento.TipoSalvamento;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionHgs;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.CartaPTSNoDocumentadaDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.FiniquitoInformacionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.ImpresionEntregaDocumentosDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal.DeterminacionInformacionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal.DeterminacionInformacionSiniestroDTO.InfoRecuperacionDeducible;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.CartaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.util.EnumUtil;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

@Stateless
public class PerdidaTotalServiceImpl implements PerdidaTotalService{

	public static final Logger	log	= Logger.getLogger(PerdidaTotalServiceImpl.class);
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private PerdidaTotalDao perdidaTotalDao;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private SiniestroCabinaService siniestroCabinaService;
	
	@EJB
	private AgenteMidasService agenteService;
	
	@EJB
	private CartaSiniestroService cartaSiniestroService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private EndosoService endosoService;
	
	@EJB
    private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	@EJB
	RecuperacionDeducibleService recuperacionDeducibleService;
	
	@EJB
	EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	private static final String INDEMNIZACION_ORDEN_COMPRA = "ordenCompra";
	
	private static final String TIPO_BENEFICIARIO_PERSONA_FISICA = "PB";
	
	public static final String BAJA_PLACAS_PERDIDA_TOTAL = "BPPT";
	public static final String BAJA_PLACAS_ROBO_TOTAL = "BPRT";
	
	public static final String TIPO_SINIESTRO_PERDIDA_TOTAL = "PT";
	public static final String TIPO_SINIESTRO_ROBO_TOTAL = "RB";
	public static final String TIPO_SINIESTRO_OTROS = "OT";
	
	private static final String ESTATUS_RECHAZADA = "R";
	private static final String AUTORIZACIONPERDIDATOTAL = "AT";
	private static final String AUTORIZACIONPERDIDATOTAL_MSJ = "PERDIDA TOTAL";

	private static final String AUTORIZACIONINDEMNIZACION= "AI";
	private static final String AUTORIZACIONINDEMNIZACION_MSJ= "INDEMNIZACION";
	

	/**
	 * Metodo antiguo para realizar la busqueda de indemnizaciones 
	 * @deprecated el nuevo metodo buscarIndemnizaciones utiliza un stored procedure para hacer la busqueda mas rapida
	 * @param filtroPT
	 * @return
	 */
	public List<PerdidaTotalFiltro> listar(PerdidaTotalFiltro filtroPT){
		List<PerdidaTotalFiltro> resultados = new ArrayList<PerdidaTotalFiltro>();
		
		List<OrdenCompra> ordenesCompra 	= perdidaTotalDao.buscarOrdenesCompraDePerdidaTotal(filtroPT); 
		resultados = construirListaPerdidaTotal(ordenesCompra);
		
		return resultados;
	}
	
	@Override
	public List<PerdidaTotalFiltro> buscarIndemnizaciones(PerdidaTotalFiltro filtroPT){
		List<PerdidaTotalFiltro> resultados = perdidaTotalDao.buscarIndemnizaciones(filtroPT);
		return resultados;
	}
	
	/**
	 * Prepara la informacion que se va a prensentar en la pantalla de listado de perdidas totales
	 * @param ordenesCompra
	 * @deprecated ahora se construlle la lista desde base de datos
	 * @return
	 */
	private List<PerdidaTotalFiltro> construirListaPerdidaTotal(List<OrdenCompra> ordenesCompra){
		List<PerdidaTotalFiltro> listaPerdidasTotales = new ArrayList<PerdidaTotalFiltro>();
		PerdidaTotalFiltro filtroPT = null;
		Map<String, String> etapasPT = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ETAPAS_PERDIDA_TOTAL);
		Map<String, String> estatusesPT = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_AUT_INDEMNIZACION);
		Map<String, String> tiposSiniestro = obtenerTiposSiniestroPerdidaTotal();
		Map<String, String> indemnizacionCoberturas = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.INDEMNIZACION_COBERTURAS);
		
		for(OrdenCompra orden: ordenesCompra){
			IndemnizacionSiniestro indemnizacion = obtenerIndemnizacion(orden);
			filtroPT = new PerdidaTotalFiltro();
			
			filtroPT.setIdOrdenCompra(orden.getId());
			if(indemnizacion != null){
				filtroPT.setIdIndemnizacion(indemnizacion.getId());
				filtroPT.setEtapa(indemnizacion.getEtapa());
				filtroPT.setEtapaDesc(etapasPT.get(filtroPT.getEtapa()));
				filtroPT.setEstatusPerdidaTotal(indemnizacion.getEstatusPerdidaTotal());
				filtroPT.setEstatusPerdidaTotalDesc(estatusesPT.get(filtroPT.getEstatusPerdidaTotal()));
				filtroPT.setEstatusIndemnizacion(indemnizacion.getEstatusIndemnizacion());
				filtroPT.setEstatusIndemnizacionDesc(estatusesPT.get(filtroPT.getEstatusIndemnizacion()));
				filtroPT.setExisteOrdenCompraFinal(existeOrdenCompraFinal(indemnizacion.getId())? "true" : "false");
				List<RecepcionDocumentosSiniestro> resultados = entidadService.findByProperty(RecepcionDocumentosSiniestro.class,"indemnizacion.id",indemnizacion.getId());
				if(resultados != null && !resultados.isEmpty()){
					filtroPT.setExisteRecepcionDocumentos("true");
					filtroPT.setFormaPago(resultados.get(0).getFormaPago());
				}else{
					filtroPT.setExisteRecepcionDocumentos("false");
				}
			}
			if(orden.getIdTercero() != null){
				EstimacionCoberturaReporteCabina estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,orden.getIdTercero() );
				filtroPT.setNumPaseAtencion(estimacion.getFolio());
			}
			if(orden.getCoberturaReporteCabina() != null 
					&& orden.getCoberturaReporteCabina().getClaveTipoCalculo() != null){
				filtroPT.setCobertura(indemnizacionCoberturas.get(orden.getCoberturaReporteCabina().getClaveTipoCalculo()));
			}
			if(orden.getReporteCabina() != null){
				filtroPT.setFechaSiniestro(orden.getReporteCabina().getFechaOcurrido());
				if(orden.getReporteCabina().getOficina() != null){
					filtroPT.setIdOficina(orden.getReporteCabina().getOficina().getId());
					filtroPT.setOficinaNombre(orden.getReporteCabina().getOficina().getNombreOficina());
				}
				if(orden.getReporteCabina().getPoliza() != null){
					filtroPT.setNumeroPoliza(orden.getReporteCabina().getPoliza().getNumeroPolizaFormateada());
				}
				filtroPT.setNumeroReporte(orden.getReporteCabina().getNumeroReporte());
				if(orden.getReporteCabina().getSeccionReporteCabina() != null && 
						orden.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() != null &&
						orden.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina() != null){
					filtroPT.setNumeroSerie(orden.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie());
				}
				filtroPT.setNumeroValuacion(orden.getNumeroValuacion());
				filtroPT.setReporteAnioReporte(orden.getReporteCabina().getAnioReporte());
				filtroPT.setReporteClaveOficina(orden.getReporteCabina().getClaveOficina());
				filtroPT.setReporteConsecutivoReporte(orden.getReporteCabina().getConsecutivoReporte());
				if(orden.getReporteCabina().getSiniestroCabina() != null){
					filtroPT.setIdSiniestro(orden.getReporteCabina().getSiniestroCabina().getId());
					filtroPT.setNumeroSiniestro(orden.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
					filtroPT.setSiniestroAnioReporte(orden.getReporteCabina().getSiniestroCabina().getAnioReporte());
					filtroPT.setSiniestroClaveOficina(orden.getReporteCabina().getSiniestroCabina().getClaveOficina());
					filtroPT.setSiniestroConsecutivoReporte(orden.getReporteCabina().getSiniestroCabina().getConsecutivoReporte());
				}
			}
			filtroPT.setTipoSiniestro(obtenerTipoSiniestro(orden, tiposSiniestro));
			filtroPT.setTipoSiniestroDesc(tiposSiniestro.get(filtroPT.getTipoSiniestro()));
			if(filtroPT.getTipoSiniestro().compareTo(TIPO_SINIESTRO_PERDIDA_TOTAL) == 0){
				filtroPT.setTipoBajaPlacas(BAJA_PLACAS_PERDIDA_TOTAL);
			}else if(filtroPT.getTipoSiniestro().compareTo(TIPO_SINIESTRO_ROBO_TOTAL) == 0){
				filtroPT.setTipoBajaPlacas(BAJA_PLACAS_ROBO_TOTAL);
			}else if(filtroPT.getTipoSiniestro().compareTo(TIPO_SINIESTRO_OTROS) == 0){
				filtroPT.setTipoBajaPlacas(BAJA_PLACAS_PERDIDA_TOTAL);
			}
			listaPerdidasTotales.add(filtroPT);
		}
		
		return listaPerdidasTotales;
	}
	
	private IndemnizacionSiniestro obtenerIndemnizacion(OrdenCompra orden){
		List<IndemnizacionSiniestro> resultados = null;
		IndemnizacionSiniestro indemnizacion = null;
		
		resultados = entidadService.findByProperty(IndemnizacionSiniestro.class, INDEMNIZACION_ORDEN_COMPRA, orden);
		if(resultados != null && !resultados.isEmpty()){
			indemnizacion = resultados.get(0);
		}
		
		return indemnizacion;
	}
	
	/**
	 * Devuelte el primer tipo de concepto de perdida total de la orden de compra que encuentre
	 * Si la orden compra no contiene ningun detalle que concida con la lista de tipos de siniestro
	 * que se le pasa, retorna null
	 * @param orden
	 * @param tiposSiniestro
	 * @return
	 */
	@Override
	public String obtenerTipoSiniestro(OrdenCompra orden, Map<String, String> tiposSiniestro){
		String tipoSiniestro = null;
		
		if(orden != null && orden.getDetalleOrdenCompras() != null){
			for(DetalleOrdenCompra detalle: orden.getDetalleOrdenCompras()){
				for(String key: tiposSiniestro.keySet()){
					if(detalle.getConceptoAjuste().getTipoConcepto().compareTo(key) == 0){
						tipoSiniestro = key;
						return tipoSiniestro;
					}
				}
			}
		}
		
		return tipoSiniestro;
	}
	
	@Override
	public Map<String, String> obtenerTiposSiniestroPerdidaTotal(){
		Map<String, String> tiposConceptos		= listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_CAT_CONCEPTOS);
		Map<String, String> claveConceptosRobo 	= listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPOS_CONCEPTOS_PERDIDA_TOTAL);
		Map<String, String> tiposConceptosRobo 	= new LinkedHashMap<String, String>();
		
		for(String key: claveConceptosRobo.keySet()){
			tiposConceptosRobo.put(claveConceptosRobo.get(key), tiposConceptos.get(claveConceptosRobo.get(key)));
		}
		return tiposConceptosRobo;
	}
	
	@Override
	public void generarIndemnizacion(OrdenCompra informacionOrdenCompra){
		OrdenCompra orden = entidadService.findById(OrdenCompra.class, informacionOrdenCompra.getId());
		ReporteCabina reporte = orden.getReporteCabina();
		if(reporte.getSiniestroCabina()==null){
		}else if (reporte.getSiniestroCabina().getId() == null){
		}
		if(reporte.getSiniestroCabina() == null 
				|| reporte.getSiniestroCabina().getId() == null){
			List<ReporteCabina> resultados = entidadService.findByProperty(ReporteCabina.class, "id",orden.getReporteCabina().getId());
			reporte = resultados.get(0);
		}
			IndemnizacionSiniestro indemnizacion = new IndemnizacionSiniestro();
			indemnizacion.setSiniestro(reporte.getSiniestroCabina());
			indemnizacion.setOrdenCompra(orden);
			
			indemnizacion.setEtapa(IndemnizacionSiniestro.EtapasIndemnizacion.DETERMINACION.codigo);
			
			entidadService.save(indemnizacion);
	}
	
	@Override
	public IndemnizacionSiniestro buscarIndemnizacionSiniestro(Long idOrdenCompra){
		IndemnizacionSiniestro indemnizacion = null;
		OrdenCompra ordenCompra = entidadService.findById(OrdenCompra.class, idOrdenCompra);
		if(ordenCompra != null ){
			List<IndemnizacionSiniestro> resultados = entidadService.findByProperty(IndemnizacionSiniestro.class, INDEMNIZACION_ORDEN_COMPRA, ordenCompra);
			if(resultados != null && resultados.size() > 0){
				indemnizacion = resultados.get(0);
			}
		}

		return indemnizacion;
	}

	/**
	 * Genera la recuperacion de salvamento para la indemnizacion. 
	 * @param indemnizacionCapturada
	 */
	private void generarRecuperacionSalvamentoIndemnizacion(IndemnizacionSiniestro indemnizacionCapturada){
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, indemnizacionCapturada.getId());
		Boolean abandonoIncosteable = 
			(indemnizacion.getEstimadoValuador() != null 
					&& indemnizacion.getEstimadoValuador().doubleValue() > 0
					&& !indemnizacion.getEsPagoDanios())? 
							Boolean.FALSE : 
								Boolean.TRUE;
		if(abandonoIncosteable){
			String motivo = "";
			if(indemnizacion.getEsPagoDanios()){
				motivo = "La indemnizaci\u00F3n es un pago de da\u00F1os";
			}else{
				motivo = "No se ha capturado el estimado del valuador en la indemnizaci\u00F3n";
			}
			recuperacionSalvamentoService.guardarRecuperacionSalvamento(
					indemnizacion.getOrdenCompra().getIdTercero(), indemnizacion.getEstimadoValuador(), indemnizacion.getUbicacionSalvamento(), 
					TipoSalvamento.POR_DANIOS, abandonoIncosteable, "", 
					indemnizacion.getOrdenCompra().getReporteCabina().getLugarOcurrido().getCiudad().getId(), motivo);
		}else{
			recuperacionSalvamentoService.guardarRecuperacionSalvamento(
					indemnizacion.getOrdenCompra().getIdTercero(), indemnizacion.getEstimadoValuador(), indemnizacion.getUbicacionSalvamento(), 
					TipoSalvamento.POR_DANIOS, abandonoIncosteable, "", 
					indemnizacion.getOrdenCompra().getReporteCabina().getLugarOcurrido().getCiudad().getId());
		}
	}
	
	@Override
	public void autorizarPerdidaTotal(IndemnizacionSiniestro indemnizacionCapturada, String tipoAutorizacion){
		cambiarEstatusDeAutorizacionIndemnizacion(indemnizacionCapturada, tipoAutorizacion, IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion.AUTORIZADA.codigo);
		if(tipoAutorizacion.equals(IndemnizacionSiniestro.EtapasIndemnizacion.AUTORIZAR_PERDIDA_TOTAL.codigo)){
			generarRecuperacionSalvamentoIndemnizacion(indemnizacionCapturada);}
	}
	
	@Override
	public void rechazarPerdidaTotal(IndemnizacionSiniestro indemnizacionCapturada, String tipoAutorizacion){
		cambiarEstatusDeAutorizacionIndemnizacion(indemnizacionCapturada, tipoAutorizacion, IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion.RECHAZADA.codigo);
		if(indemnizacionCapturada != null && indemnizacionCapturada.getId() != null){
			IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, indemnizacionCapturada.getId());
			if(indemnizacion != null){
				if(indemnizacion.getOrdenCompra() != null && indemnizacion.getOrdenCompra().getId() != null){
					OrdenCompra orden = entidadService.findById(OrdenCompra.class, indemnizacion.getOrdenCompra().getId());
					orden.setEstatus(OrdenCompraService.ESTATUS_INDEMNIZACION_CANCELADA);
					orden.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
					orden.setFechaModificacion(new Date());
					entidadService.save(orden);
					entidadService.save(indemnizacion);
				}else{
					throw new NegocioEJBExeption("CAM_IND_00","No se pudo cancelar la orden de compra para la indemnizacion con id " + indemnizacionCapturada.getId());
				}
			}else{
				throw new NegocioEJBExeption("CAM_IND_01","Error al Cancelar la Orden de Compra. No se encontro una indemnizacion con la id " + indemnizacionCapturada.getId());
			}
		}
	}
	
	/**
	 * Configura la indemnizacion como autorizada o rechazada 
	 * @param indemnizacionCapturada
	 * @param tipoAutorizacion
	 * @param autorizacionEstatus
	 */
	private void cambiarEstatusDeAutorizacionIndemnizacion(IndemnizacionSiniestro indemnizacionCapturada, String tipoAutorizacion, String autorizacionEstatus){
		if(  this.tieneRolAutoriza(indemnizacionCapturada.getId(), tipoAutorizacion) ){
			if(  !this.usuarioAutorizo(indemnizacionCapturada.getId(), tipoAutorizacion) ){
				IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, indemnizacionCapturada.getId());
				if(indemnizacion != null){
					if(tipoAutorizacion.compareTo(IndemnizacionSiniestro.EtapasIndemnizacion.AUTORIZAR_INDEMNIZACION.codigo) == 0){
						this.registraFirmaAutorizacion(indemnizacionCapturada.getId(), tipoAutorizacion, autorizacionEstatus, indemnizacionCapturada.getMotivoAutIndemnizacion());
					}else if(tipoAutorizacion.compareTo(IndemnizacionSiniestro.EtapasIndemnizacion.AUTORIZAR_PERDIDA_TOTAL.codigo) == 0){
						this.registraFirmaAutorizacion(indemnizacionCapturada.getId(), tipoAutorizacion, autorizacionEstatus, indemnizacionCapturada.getMotivoAutPerdidaTotal());
					}
					if ( this.tieneRolFinal(indemnizacionCapturada.getId(), tipoAutorizacion)
							||  this.tieneFirmasRequeridas(indemnizacionCapturada.getId(), tipoAutorizacion)
							|| autorizacionEstatus.equalsIgnoreCase(ESTATUS_RECHAZADA)){
						if(tipoAutorizacion.compareTo(IndemnizacionSiniestro.EtapasIndemnizacion.AUTORIZAR_PERDIDA_TOTAL.codigo) == 0){
							indemnizacion.setMotivoAutPerdidaTotal(indemnizacionCapturada.getMotivoAutPerdidaTotal());
							indemnizacion.setFechaAutPerdidaTotal(new Date());
							indemnizacion.setUsuarioPerdidaTotal(usuarioService.getUsuarioActual().getNombreUsuario());
							indemnizacion.setEstatusPerdidaTotal(autorizacionEstatus);
							if(autorizacionEstatus.compareTo(IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion.AUTORIZADA.codigo) == 0){
								indemnizacion.setEtapa(IndemnizacionSiniestro.EtapasIndemnizacion.REGISTRO_DOCUMENTOS.codigo);
							}
						}else if(tipoAutorizacion.compareTo(IndemnizacionSiniestro.EtapasIndemnizacion.AUTORIZAR_INDEMNIZACION.codigo) == 0){
							indemnizacion.setMotivoAutIndemnizacion(indemnizacionCapturada.getMotivoAutIndemnizacion());
							indemnizacion.setFechaAutIndemnizacion(new Date());
							indemnizacion.setUsuarioIndemnizacion(usuarioService.getUsuarioActual().getNombreUsuario());
							indemnizacion.setEstatusIndemnizacion(autorizacionEstatus);
						}
						entidadService.save(indemnizacion);
					}
				}
			}else{
				throw new NegocioEJBExeption("APT_CAB_00","El usuario ya ha autorizado ");
			}	
		}else{
			throw new NegocioEJBExeption("APT_CAB_00","El usuario no tiene permisos ");
		}
	}
	

	@Override
	public IndemnizacionSiniestro buscarIndemnizacionSiniestroPorId(Long idIndemnizacion){
		IndemnizacionSiniestro resultado = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		if(null!=resultado && null!= resultado.getOrdenCompra()){
			List<ValuacionHgs> ValuacionHgsLst = new ArrayList<ValuacionHgs>();
			ValuacionHgsLst =this.entidadService.findByProperty(ValuacionHgs.class, "ordenCompraId", resultado.getOrdenCompra().getId());
			if(!ValuacionHgsLst.isEmpty()){
				ValuacionHgs valuacionHgs=ValuacionHgsLst.get(0);
				if(null==resultado.getManoObraHP()){
					resultado.setManoObraHP(valuacionHgs.getImpNanoObra());
				}
				
				if(null==resultado.getRefaccionesCarroceria()){
					resultado.setRefaccionesCarroceria(valuacionHgs.getImRefacciones());
				}
			}
		}
		if(null!=resultado){
			if (cartaSiniestroService.esCoberturaAsegurado(resultado.getId())){
				if(null==resultado.getPrimasPendientes()){
					BigDecimal prima= getPrimasPendientesPago (resultado.getId());
					resultado.setPrimasPendientes(prima);
				}
				BigDecimal deducible = recuperacionDeducibleService.obtenerDeducibleSiniestroNoRCV(resultado.getOrdenCompra().getReporteCabina().getId());
				resultado.setDeducible(deducible);
			}else{
				BigDecimal deducible = recuperacionDeducibleService.obtenerDeduciblePorEstimacion(resultado.getOrdenCompra().getIdTercero());
				resultado.setDeducible(deducible);
			}}
		return resultado;
	}
	
	
	private BigDecimal getPrimasPendientesPago (Long idIndemnizacion){
		IndemnizacionSiniestro resultado = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		BigDecimal primasPendientePago=new BigDecimal (0.0);
		/*validar */
		if(null!=resultado.getOrdenCompra().getReporteCabina()  &&  null!=resultado.getOrdenCompra().getReporteCabina().getPoliza() ){
			if(null!=resultado.getOrdenCompra().getReporteCabina().getSeccionReporteCabina()   &&   null!=resultado.getOrdenCompra().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() ){
				IncisoReporteCabina inciso=resultado.getOrdenCompra().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() ;
				PolizaDTO poliza=resultado.getOrdenCompra().getReporteCabina().getPoliza() ;
				 primasPendientePago=	endosoService.validarPrimasPendientesPagoInciso(poliza.getIdToPoliza(), inciso.getNumeroInciso(), null);

			}
			

		}
		return primasPendientePago;
	}
	
	@Override
	public Short obtenerNumeroPuertas(String estiloId){
		EstiloVehiculoDTO estiloVehiculoDTO = null;
		Short numeroPuertas = null;
		estiloVehiculoDTO = entidadService.findById(EstiloVehiculoDTO.class, getEstiloVehiculoId(estiloId));
		if(estiloVehiculoDTO != null){
			numeroPuertas = estiloVehiculoDTO.getNumeroPuertas();
		}
		return numeroPuertas;
	}
	
	private EstiloVehiculoId getEstiloVehiculoId(String estiloId) {
		String[] arrayEstilo = StringUtils.split(estiloId, '_');
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(arrayEstilo[0], arrayEstilo[1], new BigDecimal(arrayEstilo[2]));
		return estiloVehiculoId;		
	}
	
	@Override
	public void guardarDeterminacionPT(
			IndemnizacionSiniestro indemnizacionCapturada,
			String tipoSiniestro,
			DeterminacionInformacionSiniestroDTO  informacionSiniestro
			,String beneficiarioEditado
			){
		
		List<InfoRecuperacionDeducible> deducibles=	informacionSiniestro.getDeducibles();

		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, indemnizacionCapturada.getId());
		if(!beneficiarioEditado.isEmpty()){
			OrdenCompra ordenCompraInfo = entidadService.findById(OrdenCompra.class, indemnizacion.getOrdenCompra().getId());
			ordenCompraInfo.setNomBeneficiario(beneficiarioEditado);
			this.entidadService.save(ordenCompraInfo);
		}
		
		if(indemnizacion != null){
			indemnizacion.setGuiaNada(indemnizacionCapturada.getGuiaNada());
			indemnizacion.setGuiaKbb(indemnizacionCapturada.getGuiaKbb());
			indemnizacion.setGuiaEbc(indemnizacionCapturada.getGuiaEbc());
			indemnizacion.setGuiaAutometrica(indemnizacionCapturada.getGuiaAutometrica());
			indemnizacion.setValorPeritoEqPesado(indemnizacionCapturada.getValorPeritoEqPesado());
			indemnizacion.setValorPorcPerdidaTotal(indemnizacionCapturada.getValorPorcPerdidaTotal());
			indemnizacion.setValorFactura(indemnizacionCapturada.getValorFactura());
			indemnizacion.setManoObraHP(indemnizacionCapturada.getManoObraHP());
			indemnizacion.setRefaccionesCarroceria(indemnizacionCapturada.getRefaccionesCarroceria());
			indemnizacion.setManoObraMecanica(indemnizacionCapturada.getManoObraMecanica());
			indemnizacion.setRefaccionesMecanicas(indemnizacionCapturada.getRefaccionesMecanicas());
			indemnizacion.setFaltanteDesarme(indemnizacionCapturada.getFaltanteDesarme());
			indemnizacion.setTotalMoPiezas(indemnizacionCapturada.getTotalMoPiezas());
			indemnizacion.setPorcentajeDanioValorUnidad(indemnizacionCapturada.getPorcentajeDanioValorUnidad());
			indemnizacion.setSumaGuias(indemnizacionCapturada.getSumaGuias());
			indemnizacion.setNumeroGuias(indemnizacionCapturada.getNumeroGuias());
			indemnizacion.setPromedioGuias(indemnizacionCapturada.getPromedioGuias());
			indemnizacion.setValorUnidad(indemnizacionCapturada.getValorUnidad());
			indemnizacion.setSubTotal(indemnizacionCapturada.getSubTotal());
			indemnizacion.setValorEqAdaptaciones(indemnizacionCapturada.getValorEqAdaptaciones());
			indemnizacion.setDeducibleEqAdaptaciones(indemnizacionCapturada.getDeducibleEqAdaptaciones());
			indemnizacion.setPrimasPendientes(indemnizacionCapturada.getPrimasPendientes());
			indemnizacion.setTotalIndemnizar(indemnizacionCapturada.getTotalIndemnizar());
			indemnizacion.setGuiaAutometricaSalv(indemnizacionCapturada.getGuiaAutometricaSalv());
			indemnizacion.setUltimaTenencia(indemnizacionCapturada.getUltimaTenencia());
			indemnizacion.setUbicacionSalvamento(indemnizacionCapturada.getUbicacionSalvamento());
			indemnizacion.setTipoDeposito(indemnizacionCapturada.getTipoDeposito());
			indemnizacion.setCostoPension(indemnizacionCapturada.getCostoPension());
			indemnizacion.setEstimadoValuador(indemnizacionCapturada.getEstimadoValuador());
			indemnizacion.setSugeridoDireccion(indemnizacionCapturada.getSugeridoDireccion());
			indemnizacion.setDerivadoAutoplazo(indemnizacionCapturada.getDerivadoAutoplazo());
			indemnizacion.setPersonaMoralRenovacion(indemnizacionCapturada.getPersonaMoralRenovacion());
			indemnizacion.setPersonaFisicaRenovacion(indemnizacionCapturada.getPersonaFisicaRenovacion());
			indemnizacion.setUnidadInspecContratacion(indemnizacionCapturada.getUnidadInspecContratacion());
			indemnizacion.setPolizaAnteriorInmediata(indemnizacionCapturada.getPolizaAnteriorInmediata());
			indemnizacion.setSiniestrosAnteriores(indemnizacionCapturada.getSiniestrosAnteriores());
			indemnizacion.setObservaciones(indemnizacionCapturada.getObservaciones());
			indemnizacion.setDescuento(indemnizacionCapturada.getDescuento());
			indemnizacion.setNotaDescuento(indemnizacionCapturada.getNotaDescuento());
			if( tipoSiniestro.compareTo(TIPO_SINIESTRO_ROBO_TOTAL) != 0 ){
				RecuperacionSalvamento recuperacion = 
					recuperacionSalvamentoService.obtenerRecuperacionSalvamento(indemnizacion.getOrdenCompra().getIdTercero());
				if(recuperacion != null){
//			ACTUALIZANDO LA RECUPERACION DE SALVAMENTO SI LA INDEMNIZACION NO ES DE ROBOS
					recuperacion.setValorEstimado(indemnizacionCapturada.getEstimadoValuador());
					recuperacion.setUbicacionDeterminacion(indemnizacionCapturada.getUbicacionSalvamento());
					Boolean abandonoIncosteable = 
						(indemnizacionCapturada.getEstimadoValuador() != null 
								&& indemnizacionCapturada.getEstimadoValuador().doubleValue() > 0
								&& !indemnizacionCapturada.getEsPagoDanios())? 
										Boolean.FALSE : 
											Boolean.TRUE;
					recuperacion.setAbandonoIncosteable(abandonoIncosteable);
					if(abandonoIncosteable){
						if(indemnizacionCapturada.getEsPagoDanios()){
							recuperacion.setMotivoAbandonoIncosteable("La indemnizaci\u00F3n es un pago de da\u00F1os");
						}else{
							recuperacion.setMotivoAbandonoIncosteable("No se ha capturado el estimado del valuador en la indemnizaci\u00F3n");
						}
					}
					recuperacionSalvamentoService.guardarRecuperacionSalvamento(recuperacion);
//			REACTIVANDO LA RECUPERACION DE SALVAMENTO SI LA INDEMNIZACION NO ES DE ROBOS Y SE DESACTIVE LA BANDERA DE PAGO DE DANIOS
					if(indemnizacion.getEsPagoDanios() != null && indemnizacion.getEsPagoDanios() == true
							&& indemnizacionCapturada.getEsPagoDanios() != null && indemnizacionCapturada.getEsPagoDanios() == false){
						recuperacionSalvamentoService.reactivarRecuperacionSalvamento(recuperacion.getId(),null);
			}}}
//			COPIANDO EL VALOR DEL TOTAL A INDEMNIZAR AL CAMPO DE TOTAL DE PAGO DE DANIOS
			indemnizacion.setEsPagoDanios(indemnizacionCapturada.getEsPagoDanios());
			if(indemnizacion.getEsPagoDanios() != null && indemnizacion.getEsPagoDanios()){
				indemnizacion.setTotalPagoDanios(indemnizacionCapturada.getTotalPagoDanios());
			}else{
				indemnizacion.setTotalPagoDanios(indemnizacionCapturada.getTotalIndemnizar());
			}
//			CAMBIA EL ESTATUS DE AUTORIZACION A REGISTRADA PARA SABER QUE YA SE GUARDO LA DETERMINACION AL MENOS UNA VEZ
			if(indemnizacion.getEstatusPerdidaTotal().compareTo(IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion.PENDIENTE.codigo) == 0){
				indemnizacion.setEstatusPerdidaTotal(IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion.REGISTRADA.codigo);
			}
//			ESTO HARA QUE PASE A LA ETAPA DE REGISTRO DE DOCUMENTOS CUANDO ES ROBO TOTAL
			if(indemnizacion.getEtapa().compareTo(IndemnizacionSiniestro.EtapasIndemnizacion.DETERMINACION.codigo) == 0    &&
					tipoSiniestro.compareTo(TIPO_SINIESTRO_ROBO_TOTAL) == 0  	){
				//Si esta en DT  y es Robo Total pasa a RD
				indemnizacion.setEtapa(IndemnizacionSiniestro.EtapasIndemnizacion.REGISTRO_DOCUMENTOS.codigo);
				indemnizacion.setFechaAutDeterminacion(new Date());
				indemnizacion.setUsuarioDeterminacion(usuarioService.getUsuarioActual().getNombreUsuario());
			}
//			ENVIANDO LA INFORMACION A LA ORDEN DE COMPRA
			if(indemnizacion.getOrdenCompra() != null && indemnizacion.getOrdenCompra().getDetalleOrdenCompras() != null){
				BigDecimal deducible = (indemnizacion.getDeducible() != null )? indemnizacion.getDeducible() : BigDecimal.ZERO;
				indemnizacion.getOrdenCompra().setDeducible(deducible);
				BigDecimal descuento = (indemnizacion.getDescuento() != null)? indemnizacion.getDescuento() : BigDecimal.ZERO;
				indemnizacion.getOrdenCompra().setDescuento(descuento);
				
				BigDecimal valorUnidad = BigDecimal.ZERO;
				if(indemnizacion.getEsPagoDanios()){
					valorUnidad = ((indemnizacion.getTotalPagoDanios() != null)? indemnizacion.getTotalPagoDanios() : BigDecimal.ZERO)
									.add(deducible).add(descuento);
				}else{
					valorUnidad = (indemnizacion.getValorUnidad() != null )? indemnizacion.getValorUnidad() : BigDecimal.ZERO;
				}
				indemnizacion.getOrdenCompra().getDetalleOrdenCompras().get(0).setCostoUnitario(valorUnidad);
				indemnizacion.getOrdenCompra().getDetalleOrdenCompras().get(0).setImporte(valorUnidad);
				
				indemnizacion.getOrdenCompra().getDetalleOrdenCompras().get(0).setIva(BigDecimal.ZERO);
				indemnizacion.getOrdenCompra().getDetalleOrdenCompras().get(0).setPorcIva(BigDecimal.ZERO);
			}
//			ACTUALIZANDO LA RECEPCION DE DOCUMENTOS
			if(indemnizacion.getEsPagoDanios() != null){
				RecepcionDocumentosSiniestro recepcion = cartaSiniestroService.buscarRecepcionDeDocumentos(indemnizacion.getId());
				if(recepcion != null){
						recepcion.setEsPagoDanios(indemnizacion.getEsPagoDanios());
						entidadService.save(recepcion);
			}}
			
			//Actualizar montos de deducible
			if(deducibles != null){		
				if(mx.com.afirme.midas2.util.CollectionUtils.isNotEmpty(deducibles)){
					indemnizacion.getOrdenCompra().setAplicaDeducible(Boolean.TRUE);
				}else{
					indemnizacion.getOrdenCompra().setAplicaDeducible(Boolean.FALSE);
				}
				for(InfoRecuperacionDeducible deducible : mx.com.afirme.midas2.util.CollectionUtils.emptyIfNull(deducibles)){
					
					if(!deducible.getIngresoGenerado().booleanValue()){
						recuperacionDeducibleService.actualizarRecuperacion(deducible.getIdRecuperacion(), deducible.getMonto());
					}
				}
			}  
			entidadService.save(indemnizacion);
//			SE ACOMPLETAN LOS MONTOS DE PROVISION DEL SALVAMENTO (IVA, PORCENTAJE IVA Y SUBTOTAL)
			recuperacionSalvamentoService.actualizarInformacionRecuperacionSalvamentoEnIndemnizacion(indemnizacion.getId());
			informacionSiniestro.setEsCoberturaAsegurado(cartaSiniestroService.esCoberturaAsegurado(indemnizacion.getId()));
			try {
				if(!informacionSiniestro.getEsCoberturaAsegurado()){
					
					EstimacionCoberturaReporteCabina estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
					TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
//					SI ES TERCERO SE MODIFICARA EL VEHICULO
					tercero.setMarca(informacionSiniestro.getMarca());
					tercero.setEstiloVehiculo(informacionSiniestro.getTipo());
					tercero.setNumeroMotor(informacionSiniestro.getMotor());
					tercero.setPlacas(informacionSiniestro.getPlaca());
					tercero.setNumeroSerie(informacionSiniestro.getNumSerie());
					tercero.setModeloVehiculo(informacionSiniestro.getModelo());
					entidadService.save(tercero);			
				}	
			} catch (Exception e) {
				e.printStackTrace();
				System.out.println("Error al guardar vehiculo modificado de tercero");
			}
		}
	}
	
	@Override
	public DeterminacionInformacionSiniestroDTO obtenerInformacionSiniestro(Long idIndemnizacion, String tipoSiniestro, String tipoSiniestroDesc){
		IndemnizacionSiniestro indemnizacion = null;
		DeterminacionInformacionSiniestroDTO informacionSiniestro = null;
		if(idIndemnizacion != null){
			indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			if(indemnizacion != null){ 
				informacionSiniestro = new DeterminacionInformacionSiniestroDTO();
				this.buscarInformacionCabinaDeterminacion(informacionSiniestro, indemnizacion);
				this.buscarInformacionOrdenCompraDeterminacion(informacionSiniestro, indemnizacion);
				this.buscarInformacionAutoDeterminacion(informacionSiniestro, indemnizacion);
				informacionSiniestro.setTipoSiniestro(tipoSiniestroDesc);
				if(tipoSiniestro.compareTo(TIPO_SINIESTRO_PERDIDA_TOTAL) == 0){
					informacionSiniestro.setTipoBajaPlacas(BAJA_PLACAS_PERDIDA_TOTAL);
				}else if(tipoSiniestro.compareTo(TIPO_SINIESTRO_ROBO_TOTAL) == 0){
					informacionSiniestro.setTipoBajaPlacas(BAJA_PLACAS_ROBO_TOTAL);
				}else if(tipoSiniestro.compareTo(TIPO_SINIESTRO_OTROS) == 0){
					informacionSiniestro.setTipoBajaPlacas(BAJA_PLACAS_PERDIDA_TOTAL);
				}
				List<RecuperacionDeducible> deducibles = obtenerRecuperacionesDeducibles(indemnizacion.getOrdenCompra().getReporteCabina().getId());
				OrdenCompra orden = indemnizacion.getOrdenCompra();
				Boolean esAsegurado = cartaSiniestroService.esCoberturaAsegurado(indemnizacion.getId());
				for(RecuperacionDeducible deducible : mx.com.afirme.midas2.util.CollectionUtils.emptyIfNull(deducibles)){					
					CoberturaReporteCabina cobReporte = null;
					Boolean tieneIngreso = Boolean.FALSE;
					if(deducible.getCoberturas() != null && deducible.getCoberturas().size() > 0){
						cobReporte = deducible.getCoberturas().get(0).getCoberturaReporte();
					}else{
						cobReporte = orden.getCoberturaReporteCabina();
					}					
					if(!deducible.getEstatus().equals(Recuperacion.EstatusRecuperacion.REGISTRADO.toString())){
						tieneIngreso = Boolean.TRUE; //en realidad si esta cancelado o inactivo tampoco se permite modificacion
					}
					InfoRecuperacionDeducible infoDeducible = new InfoRecuperacionDeducible(
							estimacionCoberturaSiniestroService.obtenerNombreCobertura(cobReporte.getCoberturaDTO(),  
									cobReporte.getClaveTipoCalculo(), orden.getCveSubTipoCalculoCobertura(), 
									null, null), deducible.getMontoFinalDeducible(), deducible.getId(), tieneIngreso);	
					
					String cveSubCalculo = deducible.getEstimacionCobertura().getCveSubCalculo();
					if(esAsegurado && (StringUtil.isEmpty(cveSubCalculo) || 
							(!StringUtil.isEmpty(cveSubCalculo) && 
									!EnumUtil.equalsValue(ClaveSubCalculo.RC_VEHICULO, cveSubCalculo)
									&& !EnumUtil.equalsValue(ClaveSubCalculo.RC_BIENES, cveSubCalculo)
									&& !EnumUtil.equalsValue(ClaveSubCalculo.RC_PERSONA, cveSubCalculo)))) {
						informacionSiniestro.getDeducibles().add(infoDeducible);
					}else if(!esAsegurado 
							&& (!StringUtil.isEmpty(cveSubCalculo) 
									&& ( (EnumUtil.equalsValue(ClaveSubCalculo.RC_VEHICULO, cveSubCalculo)
											&& (indemnizacion.getOrdenCompra().getIdTercero().equals(deducible.getEstimacionCobertura().getId())))
									|| EnumUtil.equalsValue(ClaveSubCalculo.RC_BIENES, cveSubCalculo)
									|| EnumUtil.equalsValue(ClaveSubCalculo.RC_PERSONA, cveSubCalculo)))) {
						informacionSiniestro.getDeducibles().add(infoDeducible);
					}
					
				}
			}
		}
		
		return informacionSiniestro;
	}
	
	@SuppressWarnings("unchecked")
	private List<RecuperacionDeducible> obtenerRecuperacionesDeducibles(Long idReporte){
		Map<String,Object> params = new HashMap<String, Object>();
		StringBuilder query = new StringBuilder("SELECT model from RecuperacionDeducible model ");
		query.append("WHERE model.estatus <> 'CANCELADO' and model.estatus <> 'INACTIVO' and model.estatus <> 'RECUPERADO' AND model.reporteCabina.id = :idReporte AND model.origenDeducible = :origen");
		params.put("idReporte", idReporte);
		params.put("origen", RecuperacionDeducible.Origen.PASE.getValue());
		return entidadService.executeQueryMultipleResult(query.toString(), params);		 
	}
	

	/**
	 * Guarda la informacion del siniestro en el DTO que se le pasa como parametro
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void buscarInformacionCabinaDeterminacion(DeterminacionInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		if(indemnizacion != null &&
				indemnizacion.getSiniestro() != null){
			informacionSiniestro.setIdSiniestro(indemnizacion.getSiniestro().getId());
			informacionSiniestro.setNumSiniestro(indemnizacion.getSiniestro().getNumeroSiniestro());
			if(indemnizacion.getSiniestro().getReporteCabina() != null){
				informacionSiniestro.setIdReporteCabina(indemnizacion.getSiniestro().getReporteCabina().getId());
				if(indemnizacion.getSiniestro().getReporteCabina().getAgenteId() != null){
					Agente agente = new Agente();
					agente.setId(indemnizacion.getSiniestro().getReporteCabina().getAgenteId().longValue());
					try{
						List<Agente> resultados = agenteService.findByFilters(agente);
						if(resultados != null && !resultados.isEmpty()){
							agente = resultados.get(0);
						}
						informacionSiniestro.setAgente(agente.getPersona().getNombreCompleto());
					}catch(Exception ex){
					}
				}
				informacionSiniestro.setNumReporte(indemnizacion.getSiniestro().getReporteCabina().getNumeroReporte());
				informacionSiniestro.setFechaSiniestro(indemnizacion.getSiniestro().getReporteCabina().getFechaOcurrido());
				if(indemnizacion.getSiniestro().getReporteCabina().getPoliza() != null){
					informacionSiniestro.setNumPoliza(indemnizacion.getSiniestro().getReporteCabina().getPoliza().getFolioPoliza());
					informacionSiniestro.setAsegurado(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getNombreAsegurado());
				}
				if(indemnizacion.getSiniestro().getReporteCabina().getLugarOcurrido() != null){
					informacionSiniestro.setLugarSiniestro(indemnizacion.getSiniestro().getReporteCabina().getLugarOcurrido().getCiudad().getDescripcion() + ", " + 
							indemnizacion.getSiniestro().getReporteCabina().getLugarOcurrido().getEstado().getDescripcion());
				}
				if(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina() != null){
					if(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() != null){
						informacionSiniestro.setInciso(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso());
					}
				}
			}
		}
	}
	
	/**
	 * Guarda la informacion del robo en el DTO que se le pasa como parametro
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void buscarInformacionOrdenCompraDeterminacion(DeterminacionInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		if(indemnizacion != null){
			if(indemnizacion.getOrdenCompra() != null){
				if(indemnizacion.getOrdenCompra().getTipoPago().compareTo(TIPO_BENEFICIARIO_PERSONA_FISICA) == 0){
					informacionSiniestro.setBeneficiario(indemnizacion.getOrdenCompra().getNomBeneficiario());
				}else{
					Integer idBeneficiario = indemnizacion.getOrdenCompra().getIdBeneficiario().intValue();
					PrestadorServicio prestador = entidadService.findById(PrestadorServicio.class, idBeneficiario);
					if(prestador != null){
						informacionSiniestro.setBeneficiario(prestador.getNombrePersona());
					}
				}
				if(indemnizacion.getOrdenCompra().getNumeroValuacion() != null){
					ValuacionHgs valuacion = entidadService.findById(ValuacionHgs.class, indemnizacion.getOrdenCompra().getNumeroValuacion());
					if(valuacion != null){
						informacionSiniestro.setIdValuador(Long.parseLong(valuacion.getValuadorId()));
					}
				}
			}
		}
	}
	
	/**
	 * Carga la informacion del Auto en el DTO que se le pasa como parametro
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void buscarInformacionAutoDeterminacion(DeterminacionInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		if(indemnizacion != null){
			if(indemnizacion.getSiniestro() != null){
				if(indemnizacion.getSiniestro().getReporteCabina() != null){
					if(indemnizacion.getSiniestro().getReporteCabina().getId() != null){
						informacionSiniestro.setEsCoberturaAsegurado(cartaSiniestroService.esCoberturaAsegurado(indemnizacion.getId()));
						if(informacionSiniestro.getEsCoberturaAsegurado()){
							buscarInformacionAutoDeterminacionAsegurado(informacionSiniestro, indemnizacion);
						}else{
							buscarInformacionAutoDeterminacionTercero(informacionSiniestro, indemnizacion);
						}
					}
				}
			}
		}
	}
	
	/**
	 * Busca la informacion del auto para la determinacion en caso de que sea una indemnizacion de asegurado
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void buscarInformacionAutoDeterminacionAsegurado(DeterminacionInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		Map<String,String> terminosAjusteSiniestro = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		AutoIncisoReporteCabina autoInciso = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(indemnizacion.getSiniestro().getReporteCabina().getId());
		if(autoInciso != null){
			informacionSiniestro.setColor(autoInciso.getDescColor());
			informacionSiniestro.setMarcaTipo(
					((autoInciso.getDescMarca() != null)? autoInciso.getDescMarca():"")	+ " " 
						+ ((autoInciso.getDescripcionFinal() != null)? autoInciso.getDescripcionFinal() : ""));
			informacionSiniestro.setMarca(((autoInciso.getDescMarca() != null)? autoInciso.getDescMarca():""));
			informacionSiniestro.setTipo((autoInciso.getDescripcionFinal() != null)? autoInciso.getDescripcionFinal() : "");
			informacionSiniestro.setModelo(autoInciso.getModeloVehiculo());
			informacionSiniestro.setNumSerie(autoInciso.getNumeroSerie());
			informacionSiniestro.setMotor(autoInciso.getNumeroMotor());
			if(autoInciso.getEstiloId() != null){
				informacionSiniestro.setPuertas(this.obtenerNumeroPuertas(autoInciso.getEstiloId()));
			}
			informacionSiniestro.setPlaca(autoInciso.getPlaca());
			informacionSiniestro.setTerminoAjuste(terminosAjusteSiniestro.get(autoInciso.getTerminoAjuste()));
			informacionSiniestro.setTerminoSiniestro(terminosAjusteSiniestro.get(autoInciso.getTerminoSiniestro()));
			informacionSiniestro.setEsTercero("N");
		}
	}
	
	/**
	 * Busca la informacion del auto para la determinacion en caso de que sea una indemnizacion de tercero
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void buscarInformacionAutoDeterminacionTercero(DeterminacionInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		Map<String,String> terminosAjusteSiniestro = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
		EstimacionCoberturaReporteCabina estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
		
		if(estimacion != null 
				&& estimacion.getCoberturaReporteCabina() != null
				&& estimacion.getCoberturaReporteCabina().getIncisoReporteCabina() != null
				&& estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina() != null){
			informacionSiniestro.setTerminoAjuste(terminosAjusteSiniestro.get(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoAjuste()));
			informacionSiniestro.setTerminoSiniestro(terminosAjusteSiniestro.get(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoSiniestro()));
		}
		
		TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
		if(tercero != null){
			if(tercero.getModeloVehiculo() != null){
				informacionSiniestro.setModelo(tercero.getModeloVehiculo());
			}
			if ( !StringUtil.isEmpty(  tercero.getNumeroSerie())){
				informacionSiniestro.setNumSerie(tercero.getNumeroSerie());
			}
			informacionSiniestro.setMarcaTipo(
					((tercero.getMarca() != null)? tercero.getMarca().toUpperCase():"")	+ " " 
						+ ((tercero.getEstiloVehiculo() != null)? tercero.getEstiloVehiculo(): ""));
			informacionSiniestro.setMarca(((tercero.getMarca() != null)? tercero.getMarca():""));
			informacionSiniestro.setTipo((tercero.getEstiloVehiculo() != null)? tercero.getEstiloVehiculo() : "");
			informacionSiniestro.setMotor((tercero.getNumeroMotor() != null)? tercero.getNumeroMotor() : "");
			informacionSiniestro.setPlaca((tercero.getPlacas() != null)? tercero.getPlacas() : "");
			informacionSiniestro.setEsTercero("S");
			if( !StringUtil.isEmpty(tercero.getColor())){
				informacionSiniestro.setColor((catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.COLOR, tercero.getColor())).getDescripcion());
			}
		}
		
	}
	
	@Override
	public FiniquitoInformacionSiniestroDTO buscarInformacionAseguradoFiniquito(Long idIndemnizacion){
		FiniquitoInformacionSiniestroDTO informacionSiniestro = null;
		IndemnizacionSiniestro indemnizacion = null;
		if(idIndemnizacion != null){
			informacionSiniestro = new FiniquitoInformacionSiniestroDTO();
			indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			CartaSiniestro cartaFiniquito = cartaSiniestroService.obtenerCartaFiniquito(idIndemnizacion);
			if(cartaFiniquito != null){
				informacionSiniestro.setDireccion(cartaFiniquito.getDomicilioAsegurado());
			}
			if(indemnizacion != null){ 
				cargarInformacionCabinaFiniquitoAsegurado(informacionSiniestro, indemnizacion);
				cargarInformacionAutoFiniquitoAsegurado(informacionSiniestro, indemnizacion);
			}
		}
		return informacionSiniestro;
	}
	
	/**
	 * Carga la informacion general del siniestro en el DTO de la carta de finiquito
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void cargarInformacionCabinaFiniquitoAsegurado(FiniquitoInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		if(indemnizacion != null){ 
			if(indemnizacion.getSiniestro() != null){
				informacionSiniestro.setNumeroSiniestro(indemnizacion.getSiniestro().getNumeroSiniestro());
				if(indemnizacion.getSiniestro().getReporteCabina() != null){
					if(indemnizacion.getSiniestro().getReporteCabina().getPoliza() != null){
						informacionSiniestro.setNumeroPoliza(indemnizacion.getSiniestro().getReporteCabina().getPoliza().getFolioPoliza());
					}
					if(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina() != null){
						if(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() != null){
							informacionSiniestro.setInciso(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso());
							informacionSiniestro.setNombrePropietario(indemnizacion.getOrdenCompra().getNomBeneficiario());
						}
					}
				}
			}
			if(indemnizacion.getOrdenCompra() != null
					&& indemnizacion.getOrdenCompra().getIdTercero() != null
					&& indemnizacion.getOrdenCompra().getCoberturaReporteCabina() != null
					&& indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getClaveTipoCalculo() != null
					&& EnumUtil.equalsValue(ClaveTipoCalculo.DANIOS_MATERIALES, 
							indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getClaveTipoCalculo())){
				EstimacionCoberturaReporteCabina estimacion = null;
				estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
				if(estimacion != null){
					if( !StringUtil.isEmpty(estimacion.getNombreAfectado()) ){
						informacionSiniestro.setNombrePropietario(estimacion.getNombreAfectado());
					}
				}
			}
		}
	}
	
	/**
	 * Carga la informacion del auto del asegurado en el DTO de la carta de finiquito
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void cargarInformacionAutoFiniquitoAsegurado(FiniquitoInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		if(indemnizacion != null){
			if(indemnizacion.getSiniestro() != null){
				if(indemnizacion.getSiniestro().getReporteCabina() != null){
					if(indemnizacion.getSiniestro().getReporteCabina().getId() != null){
						AutoIncisoReporteCabina autoInciso = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(indemnizacion.getSiniestro().getReporteCabina().getId());
						if(autoInciso != null){
							informacionSiniestro.setColor(autoInciso.getDescColor());
							informacionSiniestro.setMarcaTipo(
									((autoInciso.getDescripcionFinal() != null)? autoInciso.getDescripcionFinal() :"")	+ " " 
										+ ((autoInciso.getDescEstilo() != null)? autoInciso.getDescEstilo(): ""));
							informacionSiniestro.setModelo(autoInciso.getModeloVehiculo());
							informacionSiniestro.setNumeroSerie(autoInciso.getNumeroSerie());
							informacionSiniestro.setNumeroPlacas(autoInciso.getPlaca());
						}
					}
				}
			}
		}
	}
	
	@Override
	public FiniquitoInformacionSiniestroDTO buscarInformacionTerceroFiniquito(Long idIndemnizacion){
		FiniquitoInformacionSiniestroDTO informacionSiniestro = null;
		IndemnizacionSiniestro indemnizacion = null;
		if(idIndemnizacion != null){
			informacionSiniestro = new FiniquitoInformacionSiniestroDTO();
			indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			CartaSiniestro cartaFiniquito = cartaSiniestroService.obtenerCartaFiniquito(idIndemnizacion);
			if(cartaFiniquito != null){
				informacionSiniestro.setDireccion(cartaFiniquito.getDomicilioTercero());
			}
			if(indemnizacion != null){
				cargarInformacionCabinaFiniquitoTercero(informacionSiniestro, indemnizacion);
				cargarInformacionAutoFiniquitoTercero(informacionSiniestro, indemnizacion);
			}
		}
		return informacionSiniestro;
	}
	
	/**
	 * Carga la informacion general del siniestro en el DTO de la carta de finiquito para el tercero
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void cargarInformacionCabinaFiniquitoTercero(FiniquitoInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		if(indemnizacion != null && 
				indemnizacion.getSiniestro() != null){
			informacionSiniestro.setNumeroSiniestro(indemnizacion.getSiniestro().getNumeroSiniestro());
			if(indemnizacion.getSiniestro().getReporteCabina() != null){
				if(indemnizacion.getSiniestro().getReporteCabina().getPoliza() != null){
					informacionSiniestro.setNumeroPoliza(indemnizacion.getSiniestro().getReporteCabina().getPoliza().getFolioPoliza());
				}
				if(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina() != null){
					if(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() != null){
						informacionSiniestro.setInciso(indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso());
					}
				}
			}
		}
	}
	
	/**
	 * Carga la informacion del auto en el DTO de la carta de finiquito de tercero
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void cargarInformacionAutoFiniquitoTercero(FiniquitoInformacionSiniestroDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		if(indemnizacion != null){
			if(indemnizacion.getOrdenCompra() != null &&
					indemnizacion.getOrdenCompra().getIdTercero() != null){
				EstimacionCoberturaReporteCabina estimacion = null;
				estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
				if(estimacion != null){
					if( !StringUtil.isEmpty(estimacion.getNombreAfectado()) ){
						informacionSiniestro.setNombrePropietario(indemnizacion.getOrdenCompra().getNomBeneficiario());
					}
					if (estimacion != null && EnumUtil.equalsValue(ClaveTipoCalculo.RESPONSABILIDAD_CIVIL, 
						indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getClaveTipoCalculo())
						&& EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())){
					TerceroRCVehiculo terceroRCV= ((TerceroRCVehiculo) estimacion);
					if(terceroRCV != null){				
						if(terceroRCV.getModeloVehiculo() != null){
							informacionSiniestro.setModelo(terceroRCV.getModeloVehiculo());
						}
						if(terceroRCV.getPlacas() != null){
							informacionSiniestro.setNumeroPlacas(terceroRCV.getPlacas());
						}
						if ( !StringUtil.isEmpty(  terceroRCV.getNumeroSerie())){
							
								informacionSiniestro.setNumeroSerie(terceroRCV.getNumeroSerie());
						}
						String vehiculo = "";
						if ( !StringUtil.isEmpty(  terceroRCV.getMarca())){
							vehiculo+=terceroRCV.getMarca().toUpperCase()+" ";
						}
						if (!StringUtil.isEmpty(  terceroRCV.getEstiloVehiculo())){
							vehiculo+= terceroRCV.getEstiloVehiculo();
						}
						informacionSiniestro.setMarcaTipo(vehiculo);
						if(!StringUtil.isEmpty(terceroRCV.getColor())){
							informacionSiniestro.setColor(
									(catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.COLOR, terceroRCV.getColor())).getDescripcion());
						}
						
					}
				}
				}

			}
		}
	}
	
	@Override
	public ImpresionEntregaDocumentosDTO buscarInformacionSiniestroEntregaDocumentos(Long idIndemnizacion){
		ImpresionEntregaDocumentosDTO informacionSiniestro = new ImpresionEntregaDocumentosDTO();
		SiniestroCabina siniestro = null;
		IndemnizacionSiniestro indemnizacion = null;
		if(idIndemnizacion != null){
			indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			siniestro = indemnizacion.getSiniestro();
			if(siniestro != null){
				informacionSiniestro.setNumeroSiniestro(siniestro.getNumeroSiniestro());
				if(cartaSiniestroService.esCoberturaAsegurado(idIndemnizacion)){
					//AGREGANDO LA INFORMACION DEL VEHICULO DEL ASEGURADO 
					obtenerInformacionEntregaDocumentosAutoAsegurado(informacionSiniestro, indemnizacion, siniestro);
				}else{
					//AGREGANDO LA INFORMACION DEL VEHICULO DEL TERCERO
					obtenerInformacionEntregaDocumentosAutoTercero(informacionSiniestro, indemnizacion);
				}
			}
		}
		return informacionSiniestro;
	}
	
	/**
	 * obtiene la informacion del auto del asegurado para la carta de entrega de documentos
	 * @param informacionSiniestro
	 * @param indemnizacion
	 * @param siniestro
	 */
	private void obtenerInformacionEntregaDocumentosAutoAsegurado(ImpresionEntregaDocumentosDTO informacionSiniestro,
			IndemnizacionSiniestro indemnizacion, SiniestroCabina siniestro){
		if(siniestro.getReporteCabina() != null){
			if(siniestro.getReporteCabina().getId() != null){
				AutoIncisoReporteCabina autoInciso = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(siniestro.getReporteCabina().getId());
				if(autoInciso != null){
					informacionSiniestro.setMarcaTipo(
							((autoInciso.getDescMarca() != null)? autoInciso.getDescMarca():"")	+ " " 
								+ ((autoInciso.getDescripcionFinal() != null)? autoInciso.getDescripcionFinal() : ""));
					informacionSiniestro.setModelo(autoInciso.getModeloVehiculo().toString());
					informacionSiniestro.setNumeroSerie(autoInciso.getNumeroSerie());
				}
			}
		}
	}
	
	/**
	 * obtiene la informacion del auto del tercero del pase de atencion para la carta de entrega de documentos
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void obtenerInformacionEntregaDocumentosAutoTercero(ImpresionEntregaDocumentosDTO informacionSiniestro, IndemnizacionSiniestro indemnizacion){
		EstimacionCoberturaReporteCabina estimacion= 
			entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
		TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
		if(tercero != null){
				if(tercero.getModeloVehiculo() != null){
					informacionSiniestro.setModelo(tercero.getModeloVehiculo().toString());
				}
				if ( !StringUtil.isEmpty(  tercero.getNumeroSerie())){
					informacionSiniestro.setNumeroSerie(tercero.getNumeroSerie());
				}
				informacionSiniestro.setMarcaTipo(
						((tercero.getMarca() != null)? tercero.getMarca().toUpperCase():"")	+ " " 
							+ ((tercero.getEstiloVehiculo() != null)? tercero.getEstiloVehiculo(): ""));
		}
	}
	
	@Override
	public CartaPTSNoDocumentadaDTO buscarInformacionCartaPTS(Long idIndemnizacion){
		CartaPTSNoDocumentadaDTO cartaPTS = new CartaPTSNoDocumentadaDTO();
		cartaPTS.setNombreUsuario(usuarioService.getUsuarioActual().getNombreCompleto());
		SiniestroCabina siniestro = null;
		IndemnizacionSiniestro indemnizacion = null;
		if(idIndemnizacion != null){
			indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			if(indemnizacion != null){
					siniestro = indemnizacion.getSiniestro();
					if(siniestro != null){
						cartaPTS.setNumeroSiniestro(siniestro.getNumeroSiniestro());
						if(cartaSiniestroService.esCoberturaAsegurado(idIndemnizacion)){
							//SI ES CARTA PARA ASEGURADO
							obtenerInformacionCartaPTSAutoAsegurado(cartaPTS, siniestro);
						}else{
							//SI ES CARTA PARA TERCERO
							obtenerInformacionCartaPTSAutoTercero(cartaPTS, indemnizacion);
						}
					}
			}
		}
		return cartaPTS;
	}
	
	/**
	 * obtiene la informacion del auto del tercero del pase de atencion para la carta de PTS no documentadas
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void obtenerInformacionCartaPTSAutoAsegurado(CartaPTSNoDocumentadaDTO cartaPTS, SiniestroCabina siniestro){
		if(siniestro.getReporteCabina() != null){
			if(siniestro.getReporteCabina().getPoliza() != null){
				cartaPTS.setNumeroPoliza(siniestro.getReporteCabina().getPoliza().getFolioPoliza());
				cartaPTS.setNombreCliente(siniestro.getReporteCabina().getPoliza().getNombreAsegurado());
			}
			if(siniestro.getReporteCabina().getId() != null){
				AutoIncisoReporteCabina autoInciso = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(siniestro.getReporteCabina().getId());
				if(autoInciso != null){
					cartaPTS.setMarca(autoInciso.getDescMarca());
					cartaPTS.setTipo(autoInciso.getDescripcionFinal());
					cartaPTS.setModelo(autoInciso.getModeloVehiculo().toString());
					cartaPTS.setNumeroSerie(autoInciso.getNumeroSerie());
				}
			}
			if(siniestro.getReporteCabina().getSeccionReporteCabina() != null && 
					siniestro.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() != null){
				SimpleDateFormat sdf = new SimpleDateFormat("dd/M/yyyy");
				if(siniestro.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getValidFrom() != null){
					String vigenciaInicio = sdf.format(siniestro.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getValidFrom());
					cartaPTS.setPeriodoInicio(vigenciaInicio);
				}
				if(siniestro.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getValidTo() != null){
					String vigenciaFin = sdf.format(siniestro.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getValidTo());
					cartaPTS.setPeriodoFin(vigenciaFin);
				}
			}
		}
	}
	
	/**
	 * obtiene la informacion del auto del tercero del pase de atencion para la carta de PTS no documentadas
	 * @param informacionSiniestro
	 * @param indemnizacion
	 */
	private void obtenerInformacionCartaPTSAutoTercero(CartaPTSNoDocumentadaDTO cartaPTS, IndemnizacionSiniestro indemnizacion){
		EstimacionCoberturaReporteCabina estimacion= 
			entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
		TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
		if(tercero != null){
				if(tercero.getModeloVehiculo() != null){
					cartaPTS.setModelo(tercero.getModeloVehiculo().toString());
				}
				cartaPTS.setNumeroSerie(tercero.getNumeroSerie());
				cartaPTS.setMarca(tercero.getMarca());
				cartaPTS.setTipo(tercero.getEstiloVehiculo());
		}
	}

	@Override
	public Boolean existeIndemnizacionActiva(Long idEstimacion) {
		Boolean existeIndemnizacion = null;
		if(idEstimacion != null){
			List<IndemnizacionSiniestro> resultados = null;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ordenCompra.idTercero", idEstimacion);
			params.put("ordenCompra.origen", OrdenCompraService.ORIGEN_INDEMNIZACION_AUTOMATICA);
			params.put("ordenCompra.estatus", OrdenCompraService.ESTATUS_INDEMNIZACION);
			resultados = entidadService.findByProperties(IndemnizacionSiniestro.class, params);
			if(resultados != null && !resultados.isEmpty()){
				existeIndemnizacion = true;
			}else{
				existeIndemnizacion = false;
			}
		}
		return existeIndemnizacion;
	}

	@Override
	public Boolean existeIndemnizacionFinalizada(Long idEstimacion) {
		Boolean existeIndemnizacion = null;
		
		if(idEstimacion != null){
			List<IndemnizacionSiniestro> resultados = null;
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("ordenCompra.idTercero", idEstimacion);
			params.put("ordenCompra.origen", OrdenCompraService.ORIGEN_INDEMNIZACION_AUTOMATICA);
			params.put("ordenCompra.estatus", OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA);
			resultados = entidadService.findByProperties(IndemnizacionSiniestro.class, params);
			if(resultados != null && !resultados.isEmpty()){
				existeIndemnizacion = true;
			}else{
				existeIndemnizacion = false;
			}
		}
		
		return existeIndemnizacion;
	}

	@Override
	public Boolean existeIndemnizacionHGS(Long idEstimacion) {
		Boolean existeIndemnizacion = null;
		
		if(idEstimacion != null){
			List<IndemnizacionSiniestro> resultadosPT = null;
			
			Map<String, Object> paramsPT = new HashMap<String, Object>();
			paramsPT.put("ordenCompra.idTercero", idEstimacion);
			paramsPT.put("ordenCompra.origen", OrdenCompraService.ORIGENHGS_PERDIDATOTAL);
			paramsPT.put("ordenCompra.estatus", OrdenCompraService.ESTATUS_INDEMNIZACION);
			resultadosPT = entidadService.findByProperties(IndemnizacionSiniestro.class, paramsPT);
			
			if((resultadosPT != null && !resultadosPT.isEmpty())){
				existeIndemnizacion = true;
			}else{
				existeIndemnizacion = false;
			}
		}
		
		return existeIndemnizacion;
	}
	
	@Override
	public Boolean existeIndemnizacionFinalizadaHGS(Long idEstimacion) {
		Boolean existeIndemnizacion = null;
		
		if(idEstimacion != null){
			List<IndemnizacionSiniestro> resultadosPT = null;
			
			Map<String, Object> paramsPT = new HashMap<String, Object>();
			paramsPT.put("ordenCompra.idTercero", idEstimacion);
			paramsPT.put("ordenCompra.origen", OrdenCompraService.ORIGENHGS_PERDIDATOTAL);
			paramsPT.put("ordenCompra.estatus", OrdenCompraService.ESTATUS_INDEMNIZACION_FINALIZADA);
			resultadosPT = entidadService.findByProperties(IndemnizacionSiniestro.class, paramsPT);
			
			if((resultadosPT != null && !resultadosPT.isEmpty())){
				existeIndemnizacion = true;
			}else{
				existeIndemnizacion = false;
			}
		}
		
		return existeIndemnizacion;
	}
	
	@Override
	public void cancelarIndemnizacion(Long idIndemnizacion, String motivoCancelacion){
		if(idIndemnizacion != null){
			IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			if(indemnizacion != null){
				indemnizacion.setEstatusIndemnizacion(IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion.CANCELADA.codigo);
				indemnizacion.setMotivoAutPerdidaTotal(motivoCancelacion);
				indemnizacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
				indemnizacion.setFechaModificacion(new Date());
				if(indemnizacion.getOrdenCompra() != null && indemnizacion.getOrdenCompra().getId() != null){
					OrdenCompra orden = entidadService.findById(OrdenCompra.class, indemnizacion.getOrdenCompra().getId());
					orden.setEstatus(OrdenCompraService.ESTATUS_INDEMNIZACION_CANCELADA);
					orden.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
					orden.setFechaModificacion(new Date());
					entidadService.save(orden);
					entidadService.save(indemnizacion);
				}else{
					throw new NegocioEJBExeption("CAM_IND_00","No se pudo cancelar la orden de compra para la indemnizacion con id " + idIndemnizacion);
				}
			}else{
				throw new NegocioEJBExeption("CAM_IND_01","Error al Cancelar Indemnizacion. No se encontro una indemnizacion con la id " + idIndemnizacion);
			}
			RecuperacionSalvamento recuperacion = 
				recuperacionSalvamentoService.obtenerRecuperacionSalvamento(indemnizacion.getOrdenCompra().getIdTercero());
			if(recuperacion != null){
				recuperacionSalvamentoService.cancelarRecuperacion(recuperacion.getId(), "Cancelación de Indemnización");
			}
		}
	}

	@Override
	public Boolean existeOrdenCompraFinal(Long idIndemnizacion) {
		Boolean existeOC = null;
		
		if(idIndemnizacion != null){
			IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			if(indemnizacion != null && indemnizacion.getOrdenCompra() != null){
				Long idOrdenCompraIndemnizacion = indemnizacion.getOrdenCompra().getId();
				List<OrdenCompra> resultados = null; 
				
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("idOrdenCompraOrigen", idOrdenCompraIndemnizacion);
				resultados = entidadService.findByProperties(OrdenCompra.class, params);
				
				existeOC = Boolean.FALSE;
				if(resultados != null && !resultados.isEmpty()){
					for(OrdenCompra orden: resultados){
						if(orden.getEstatus().compareTo(OrdenCompraService.ESTATUS_CANCELADA) != 0
								&& orden.getEstatus().compareTo(OrdenCompraService.ESTATUS_RECHAZADA) != 0){
							existeOC = Boolean.TRUE;
							return existeOC;
						}
					}
				}
			}
		}
		
		return existeOC;
	}
	
	@Override
	public void cambiarEtapaIndemnizacionAOrdenCompraGenerada(Long idOrdenCompraOrigen, Long idOrdenCompraGenerada){
		IndemnizacionSiniestro indemnizacion = buscarIndemnizacionSiniestro(idOrdenCompraOrigen);
		indemnizacion.setEtapa(IndemnizacionSiniestro.EtapasIndemnizacion.ORDEN_COMPRA_GENERADA.codigo);
		List<OrdenCompra> resultado = entidadService.findByProperty(OrdenCompra.class, "id", idOrdenCompraGenerada);
		OrdenCompra ordenGenerada = resultado.get(0);
		indemnizacion.setOrdenCompraGenerada(ordenGenerada);
		entidadService.save(indemnizacion);
	}
	
	@Override
	public void cambiarEtapaIndemnizacionAAutorizacionIndemnizacion(Long idOrdenCompraOrigen){
		IndemnizacionSiniestro indemnizacion = buscarIndemnizacionSiniestro(idOrdenCompraOrigen);
		indemnizacion.setEtapa(IndemnizacionSiniestro.EtapasIndemnizacion.AUTORIZAR_INDEMNIZACION.codigo);
		indemnizacion.setOrdenCompraGenerada(null);
		entidadService.save(indemnizacion);
	}
	
	private boolean tieneRolAutoriza(Long idIndemnizacion, String tipoAutorizacion){
		List<RolesIndemnizacionAutorizacion> lstRoles = null;
		boolean permiso=false;
		Map<String, Object> paramsPT = new HashMap<String, Object>();
		Map<String, String> tiposSiniestro = obtenerTiposSiniestroPerdidaTotal();
		IndemnizacionSiniestro indemnizacion = this.entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		String tipoSiniestro=this.obtenerTipoSiniestro(indemnizacion.getOrdenCompra(), tiposSiniestro);
		paramsPT.put("tipoSiniestro", tipoSiniestro);
		paramsPT.put("tipo", tipoAutorizacion);
		paramsPT.put("activa", "S");

		lstRoles = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, paramsPT);
		for (RolesIndemnizacionAutorizacion rol:lstRoles){
			if(usuarioService.tieneRolUsuarioActual(rol.getRol())){
				permiso= true;
				break;
			}
			if(rol.getAutorizacionFinal().equals("S")){
				if(usuarioService.tieneRolUsuarioActual(rol.getRolSuplente())){
					permiso= true;
					break;
				}
			}
		}
		
		
		
		return permiso;
		
	}
	
	
	
	private boolean usuarioAutorizo(Long idIndemnizacion, String tipoAutorizacion){
		boolean autorizo=false;
		//valida que no haya ya autorizado 
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("indemnizacionSiniestro.id", idIndemnizacion);
		params.put("tipo", tipoAutorizacion);
		params.put("estatus", "A");	
		params.put("codigoUsuario",usuarioService.getUsuarioActual().getNombreUsuario());
		List<IndemnizacionAutorizacion>  autorizacionLst = this.entidadService.findByProperties(IndemnizacionAutorizacion.class,params);
		if(!autorizacionLst.isEmpty()){
			return true;
		}
		
		
		return autorizo;
		
	}
	
	private boolean tieneRolFinal(Long idIndemnizacion, String tipoAutorizacion){
		List<RolesIndemnizacionAutorizacion> lstRoles = null;
		Map<String, String> tiposSiniestro = obtenerTiposSiniestroPerdidaTotal();
		boolean permiso=false;
		Map<String, Object> paramsPT = new HashMap<String, Object>();
		IndemnizacionSiniestro indemnizacion = this.entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		String tipoSiniestro=this.obtenerTipoSiniestro(indemnizacion.getOrdenCompra(), tiposSiniestro);
		paramsPT.put("tipoSiniestro", tipoSiniestro);
		paramsPT.put("tipo", tipoAutorizacion);
		paramsPT.put("autorizacionFinal", "S");
		paramsPT.put("activa", "S");
		lstRoles = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, paramsPT);
		for (RolesIndemnizacionAutorizacion rol:lstRoles){
			if(usuarioService.tieneRolUsuarioActual(rol.getRol())
					|| usuarioService.tieneRolUsuarioActual(rol.getRolSuplente())){
				permiso= true;
				break;
			}
		}
		
		return permiso;
	}
	
	@Override
	public boolean tieneRolPermisoDeAutorizacionFinal(){
		boolean permiso = false;
		List<RolesIndemnizacionAutorizacion> listaRoles = null;
		Map<String, Object> paramsPT = new HashMap<String, Object>();
		paramsPT.put("autorizacionFinal", "S");
		paramsPT.put("activa", "S");
		listaRoles = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, paramsPT);
		for (RolesIndemnizacionAutorizacion rol:listaRoles){
			if(usuarioService.tieneRolUsuarioActual(rol.getRol())
					|| usuarioService.tieneRolUsuarioActual(rol.getRolSuplente())){
				permiso= true;
				break;
			}
		}
		
		return permiso;
	}
	
	private boolean tieneFirmasRequeridas(Long idIndemnizacion, String tipoAutorizacion){
		List<IndemnizacionAutorizacion>  autorizacionLst=null;
		Map<String, String> tiposSiniestro = obtenerTiposSiniestroPerdidaTotal();
		boolean permiso=true;
		Map<String, Object> paramsPT = new HashMap<String, Object>();
		IndemnizacionSiniestro indemnizacion = this.entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		String tipoSiniestro=this.obtenerTipoSiniestro(indemnizacion.getOrdenCompra(), tiposSiniestro);
		paramsPT.put("tipoSiniestro", tipoSiniestro);
		paramsPT.put("tipo", tipoAutorizacion);	
		paramsPT.put("activa", "S");
		paramsPT.put("requerida", "S");
		List<RolesIndemnizacionAutorizacion> lstRolesRequeridos = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, paramsPT);
		if(lstRolesRequeridos.isEmpty()){
			return false;
		}
		
		for (RolesIndemnizacionAutorizacion rol:lstRolesRequeridos){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("indemnizacionSiniestro.id", idIndemnizacion);
			params.put("tipo", tipoAutorizacion);
			params.put("estatus", "A");
			params.put("rol", rol.getRol());			
			autorizacionLst = this.entidadService.findByProperties(IndemnizacionAutorizacion.class,params);
			if(autorizacionLst.isEmpty()){
				permiso= false;
				break;
			}
			
		}
		return permiso;
	}
	
	
	private RolesIndemnizacionAutorizacion getRolAutoriza(String tipoSiniestro, String tipoAutorizacion){
		List<RolesIndemnizacionAutorizacion> lstRoles = null;
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tipoSiniestro", tipoSiniestro);
		param.put("tipo", tipoAutorizacion);	
		param.put("autorizacionFinal", "S");	
		param.put("activa", "S");
		lstRoles = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, param);
		for ( RolesIndemnizacionAutorizacion rol:  lstRoles){
			if(usuarioService.tieneRolUsuarioActual(rol.getRol())){
				return rol;
			}
			if(usuarioService.tieneRolUsuarioActual(rol.getRolSuplente())){
				param.clear();
				param.put("activa", "S");
				param.put("rol", rol.getRolSuplente());
				List<RolesIndemnizacionAutorizacion> listaRoles = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, param);
				for( RolesIndemnizacionAutorizacion rolSuplente : listaRoles){
					return rolSuplente;
				}
			}
		}
			
		param.clear();
		param.put("tipoSiniestro", tipoSiniestro);
		param.put("tipo", tipoAutorizacion);
		lstRoles = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, param);
		for ( RolesIndemnizacionAutorizacion rol:  lstRoles){
			if(usuarioService.tieneRolUsuarioActual(rol.getRol())){
				return rol;
			}
		}
			
		
		
		return null;
	}
	
	
	
	
	
	private void registraFirmaAutorizacion(Long idIndemnizacion ,String tipoAutorizacion, String estatus, String comentarios){
		IndemnizacionSiniestro indemnizacion = this.entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		IndemnizacionAutorizacion autorizacion = new IndemnizacionAutorizacion();
		Map<String, String> tiposSiniestro = obtenerTiposSiniestroPerdidaTotal();
		String tipoSiniestro=this.obtenerTipoSiniestro(indemnizacion.getOrdenCompra(), tiposSiniestro);
		autorizacion.setTipo(tipoAutorizacion);
		autorizacion.setIndemnizacionSiniestro(indemnizacion);
		autorizacion.setComentarios(comentarios);
		autorizacion.setEstatus(estatus);
		autorizacion.setCodigoUsuario(usuarioService.getUsuarioActual().getNombreUsuario() );
		RolesIndemnizacionAutorizacion rol = this.getRolAutoriza(tipoSiniestro, tipoAutorizacion);
		autorizacion.setRol(rol.getRol());
		autorizacion.setFechaAutorizacion(new Date());
		this.entidadService.save(autorizacion);
		
	}

	@Override
	public List<IndemnizacionAutorizacion> listaAutorizaciones(
			String tipoAutorizacion, Long idIndemnizacion) {
		List<IndemnizacionAutorizacion>  autorizacionLst=new ArrayList<IndemnizacionAutorizacion>();

		Map<String, Object> param = new HashMap<String, Object>();
		param.put("indemnizacionSiniestro.id", idIndemnizacion);
		if(!StringUtil.isEmpty(tipoAutorizacion))
			param.put("tipo", tipoAutorizacion);		
		List<IndemnizacionAutorizacion>  lista=this.entidadService.findByPropertiesWithOrder(IndemnizacionAutorizacion.class, param, "tipo desc ");
		for (IndemnizacionAutorizacion autoriza : lista){
			IndemnizacionAutorizacion dto = new IndemnizacionAutorizacion();
			if(!StringUtil.isEmpty(autoriza.getCodigoUsuario())){
				Usuario usuario=usuarioService.buscarUsuarioPorNombreUsuario(autoriza.getCodigoUsuario());
				if(null!=usuario && !StringUtil.isEmpty(usuario.getNombreCompleto())){
					dto.setCodigoUsuario(usuario.getNombreCompleto());

				}
			}
			dto.setComentarios(autoriza.getComentarios());
			dto.setEstatus(autoriza.getEstatus());
			dto.setFechaAutorizacion(autoriza.getFechaAutorizacion());
			dto.setId(autoriza.getId());
			dto.setRol(autoriza.getRol());
			List<RolesIndemnizacionAutorizacion>  roles =this.entidadService.findByProperty(RolesIndemnizacionAutorizacion.class, "rol", autoriza.getRol());
			if(!roles.isEmpty()){
				RolesIndemnizacionAutorizacion rol = roles.get(0);
				dto.setRol(rol.getDescripcionRol());
			}
			if(autoriza.getTipo().equalsIgnoreCase(AUTORIZACIONINDEMNIZACION))
			{
				dto.setTipo(AUTORIZACIONINDEMNIZACION_MSJ);
			}else if(autoriza.getTipo().equalsIgnoreCase(AUTORIZACIONPERDIDATOTAL)){
				dto.setTipo(AUTORIZACIONPERDIDATOTAL_MSJ);
			}
			autorizacionLst.add(dto);
		}
		return autorizacionLst;
	}

	@Override
	public String obtenerPuestoUsuarioActualIndemnizacion(){
		List<RolesIndemnizacionAutorizacion> lstRoles = null;
		String puesto = "";
		Map<String, Object> param = new HashMap<String, Object>();
		param.put("tipoSiniestro", "NA");
		param.put("tipo", "NA");	
		param.put("autorizacionFinal", "N");	
		param.put("activa", "N");
		lstRoles = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, param);
		for ( RolesIndemnizacionAutorizacion rol:  lstRoles){
			if(usuarioService.tieneRolUsuarioActual(rol.getRol())){
				puesto = rol.getDescripcionRol();
			}
		}
		return puesto;
	}

	@Override
	public Map<String, String> obtenerListaRolesAutorizacionSinAutorizacionFinal(){
		Map<String, String> mapaRoles = new HashMap<String, String>();
		List<RolesIndemnizacionAutorizacion> listaRoles = perdidaTotalDao.obtenerListaRolesAutorizacionSinAutorizacionFinal();
		
		for(RolesIndemnizacionAutorizacion rol : listaRoles){
			mapaRoles.put(rol.getRol(), rol.getDescripcionRol());
		}
		
		return mapaRoles;
	}

	@Override
	public void guardarConfiguracionAutorizacionIndemnizacion(String rolSuplente){
		List<RolesIndemnizacionAutorizacion> listaRoles = null;
		Map<String, Object> param = new HashMap<String, Object>();	
		param.put("autorizacionFinal", "S");
		param.put("activa", "S");
		listaRoles = entidadService.findByProperties(RolesIndemnizacionAutorizacion.class, param);
		for(RolesIndemnizacionAutorizacion rol: listaRoles){
			if(rolSuplente.compareTo("NO_APLICA") == 0){
				rol.setRolSuplente(null);
			}else{
				rol.setRolSuplente(rolSuplente);
			}
			entidadService.saveAll(listaRoles);
		}
	}
	
	@Override
	public void relacionarOrdenCompraGeneradaConIndemnizacion(Long idOrdenCompraOriginal, Long idOrdenCompraGenerada){
		IndemnizacionSiniestro indemnizacion = buscarIndemnizacionSiniestro(idOrdenCompraOriginal);
		
		List<OrdenCompra> resultado = entidadService.findByProperty(OrdenCompra.class, "id", idOrdenCompraGenerada);
		OrdenCompra ordenCompra = resultado.get(0);
		
		indemnizacion.setOrdenCompraGenerada(ordenCompra);
		entidadService.save(indemnizacion);
	}
	
	/**
	 * Retornar indemnizacion asociada a un reporte de robo para obener los datos de la perdida tota y orden de compra generada
	 * @param roboId
	 * @return
	 */
	@Override
	public IndemnizacionSiniestro obtenerOrdenCompraRobo(Long roboId){
		return perdidaTotalDao.obtenerOrdenCompraRobo(roboId);
	}

	@Override
	public String obtenerEtapaIndemnizacionPorEstimacion(Long idEstimacion) {
		String etapa = null;
		
		if(idEstimacion != null){
			IndemnizacionSiniestro indemnizacion = obtenerIndemnizacionActivaPorEstimacion(idEstimacion);
			if(indemnizacion != null){
				etapa = indemnizacion.getEtapa();
		}}
		
		return etapa;
	}

	@Override
	public void actualizarDeducibleYTotalIndemnizacion(Long idEstimacion) {
		if(idEstimacion != null){
			IndemnizacionSiniestro indemnizacion = obtenerIndemnizacionActivaPorEstimacion(idEstimacion);
			if(indemnizacion != null){
				if (cartaSiniestroService.esCoberturaAsegurado(indemnizacion.getId())){
					indemnizacion.setDeducible(recuperacionDeducibleService.obtenerDeducibleSiniestroNoRCV(indemnizacion.getOrdenCompra().getReporteCabina().getId()));
				}else{
					indemnizacion.setDeducible(recuperacionDeducibleService.obtenerDeduciblePorEstimacion(indemnizacion.getOrdenCompra().getIdTercero()));
				}
				if(indemnizacion.getDeducible().compareTo(BigDecimal.ZERO)==1){
					indemnizacion.getOrdenCompra().setAplicaDeducible(true);
				}else{
					indemnizacion.getOrdenCompra().setAplicaDeducible(false);

				}
				indemnizacion.setSubTotal(recalcularSubtotalIndemnizar(indemnizacion));
				indemnizacion.setTotalIndemnizar(recalcularTotalIndemnizar(indemnizacion));
				indemnizacion.setTotalPagoDanios(indemnizacion.getTotalIndemnizar());
				entidadService.save(indemnizacion);
			}
		}
	}
	
	private BigDecimal recalcularTotalIndemnizar(IndemnizacionSiniestro indemnizacion){
		BigDecimal totalIndemnizar = BigDecimal.ZERO;
		BigDecimal valorUnidad = (indemnizacion.getValorUnidad() != null)? indemnizacion.getValorUnidad() : BigDecimal.ZERO;
		BigDecimal deducible = (indemnizacion.getDeducible() != null)? indemnizacion.getDeducible() : BigDecimal.ZERO;
		BigDecimal descuento = (indemnizacion.getDescuento() != null)? indemnizacion.getDescuento() : BigDecimal.ZERO;
		BigDecimal primasPendientes = (indemnizacion.getPrimasPendientes() != null)? indemnizacion.getPrimasPendientes() : BigDecimal.ZERO;
		BigDecimal valorEquipoEspecial = (indemnizacion.getValorEqAdaptaciones() != null)? indemnizacion.getValorEqAdaptaciones() : BigDecimal.ZERO;
		BigDecimal deducibleValorEquipo = (indemnizacion.getDeducibleEqAdaptaciones() != null)? indemnizacion.getDeducibleEqAdaptaciones(): BigDecimal.ZERO;
		
		totalIndemnizar = totalIndemnizar.add(valorUnidad).subtract(deducible).subtract(descuento)
			.add(valorEquipoEspecial).subtract(deducibleValorEquipo).subtract(primasPendientes);
		
		return totalIndemnizar;
	}
	
	private BigDecimal recalcularSubtotalIndemnizar(IndemnizacionSiniestro indemnizacion){
		BigDecimal subtotal = BigDecimal.ZERO;
		BigDecimal valorUnidad = (indemnizacion.getValorUnidad() != null)? indemnizacion.getValorUnidad() : BigDecimal.ZERO;
		BigDecimal deducible = (indemnizacion.getDeducible() != null)? indemnizacion.getDeducible() : BigDecimal.ZERO;
		
		subtotal = subtotal.add(valorUnidad).subtract(deducible);
		
		return subtotal;
	}

	@Override
	public IndemnizacionSiniestro obtenerIndemnizacionActivaPorEstimacion(Long idEstimacion) {
		IndemnizacionSiniestro indemnizacion = null;
		
		if(idEstimacion != null){
			indemnizacion = perdidaTotalDao.obtenerIndemnizacionActivaPorEstimacion(idEstimacion);
		}
		
		return indemnizacion;
	}
	
	@Override		
	public BigDecimal obtenerPrimaNoDevengada(IndemnizacionSiniestro indemnizacion){ 
		BigDecimal primaNoDevengada = BigDecimal.ZERO;
		
		BigDecimal idToPoliza = indemnizacion.getOrdenCompra().getReporteCabina()
		.getPoliza().getIdToPoliza();

		Integer numeroInciso = indemnizacion.getOrdenCompra().getReporteCabina()
					.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso();
		
		List<EndosoIDTO> validaciones = endosoService.validaEndosoPT(idToPoliza, numeroInciso, 
				indemnizacion.getOrdenCompra().getReporteCabina().getFechaOcurrido(), // fechaSiniestro
				indemnizacion.getEsPagoDanios() ?
						SolicitudDTO.CVE_MOTIVO_ENDOSO_PAGO_DANIOS_SUSTITUCION_PERDIDA_TOTAL 
						:SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL);
		
		Predicate predicate = new Predicate() {		
			@Override
			public boolean evaluate(Object arg0) {
				EndosoIDTO validacion = (EndosoIDTO)arg0;
				if(validacion.getTipoMovimiento().equals(TipoMovimientoEndoso.INCISO.getValue()))
				{
					return true;
				}					
				return false;
			}
		};					
		
		EndosoIDTO validacionNivelInciso = (EndosoIDTO)CollectionUtils.find(validaciones,predicate);
		
		if(validacionNivelInciso == null)
		{
			throw new NegocioEJBExeption("", " Ha ocurrido un error al consultar la prima no devengada."); 
		}
		
		//Calculo de la prima total del endoso de Perdida Total
		BigDecimal totalPrimas = validacionNivelInciso.getPrimaNeta() != null ? 
				new BigDecimal(validacionNivelInciso.getPrimaNeta(),
						new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO;
		BigDecimal descuentos = validacionNivelInciso.getBonificacionComision() != null ?
				new BigDecimal(validacionNivelInciso.getBonificacionComision(),
						new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO;
		BigDecimal primaNeta = totalPrimas.subtract(descuentos);
		BigDecimal recargo = (validacionNivelInciso.getRecargoPF() != null ? 
				new BigDecimal(validacionNivelInciso.getRecargoPF(),new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO)
				.subtract((validacionNivelInciso.getBonificacionComisionRPF() != null ? 
						new BigDecimal(validacionNivelInciso.getBonificacionComisionRPF(),new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO));
		BigDecimal derechos = BigDecimal.ZERO;
		BigDecimal iva = validacionNivelInciso.getIva() != null ? 
				new BigDecimal(validacionNivelInciso.getIva(),
						new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO;
				
		primaNoDevengada = primaNeta.add(recargo).add(derechos).add(iva); //Prima Total
		
		return primaNoDevengada;		
	}
	
	@Override
	public boolean esPTDocumentada(Long idEstimacion){
		if(idEstimacion != null){
			List<RecepcionDocumentosSiniestro> resultados = entidadService.findByProperty(RecepcionDocumentosSiniestro.class,"indemnizacion.ordenCompra.idTercero",idEstimacion);
			if(resultados != null
					&& !resultados.isEmpty()){
				return true;
			}
		}
		return false;
	}
	
	@Override
	public boolean esPTIndemnizada(Long idEstimacion){
		if(idEstimacion != null){
			List<IndemnizacionSiniestro> resultados = entidadService.findByProperty(IndemnizacionSiniestro.class, "ordenCompraGenerada.idTercero", idEstimacion);
			IndemnizacionSiniestro indemnizacion = null;
			if(resultados != null 
					&& !resultados.isEmpty()){
				indemnizacion = resultados.get(0);
			}
			if(indemnizacion != null
					&& indemnizacion.getOrdenCompraGenerada() != null
					&& indemnizacion.getOrdenCompraGenerada().getOrdenPago() != null
					&& indemnizacion.getOrdenCompraGenerada().getOrdenPago().getLiquidacion() != null
					&& indemnizacion.getOrdenCompraGenerada().getOrdenPago().getLiquidacion().getEstatus() != null
					&& indemnizacion.getOrdenCompraGenerada().getOrdenPago().getLiquidacion().getEstatus().equals(
							LiquidacionSiniestro.EstatusLiquidacionSiniestro.APLICADA.getValue())){
				return true;
			}
		}
		return false;
	}
}