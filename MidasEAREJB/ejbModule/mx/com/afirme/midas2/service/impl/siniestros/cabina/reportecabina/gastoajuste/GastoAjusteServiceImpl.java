package mx.com.afirme.midas2.service.impl.siniestros.cabina.reportecabina.gastoajuste;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.catalogo.PrestadorServicioDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteReporte;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.TipoPrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.ValeTallerDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.gastoajuste.PrestadorGastoAjusteDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.gastoajuste.GastoAjusteService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.serviciosiniestro.ServicioSiniestroService;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

@Stateless
public class GastoAjusteServiceImpl implements GastoAjusteService{

	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private PrestadorServicioDao prestadorDao;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private PrestadorDeServicioService prestadorService;
	
	@EJB
	private ServicioSiniestroService servicioSiniestroService;


	@Override
	public void guardarGastoAjuste(Long idReporteCabina,
			GastoAjusteDTO gastoAjusteDTO) {
		Usuario usuario = usuarioService.getUsuarioActual();
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporteCabina);
		GastoAjusteReporte gastoAjusteRep;
		if(gastoAjusteDTO.getId() != null && gastoAjusteDTO.getId() > 0){
			gastoAjusteRep = buscarGasto(gastoAjusteDTO.getId());
		}else{
			gastoAjusteRep = new GastoAjusteReporte();
			gastoAjusteRep.setTipoPrestadorServicio(gastoAjusteDTO.getTipoPrestadorId());
			if(gastoAjusteDTO.getOrigen() == GastoAjusteReporte.Origen.PRESTADOR.getValue()){
				gastoAjusteRep.setPrestadorServicio(prestadorService.buscarPrestador(gastoAjusteDTO.getPrestadorAjusteId()));
			}else if(gastoAjusteDTO.getOrigen() == GastoAjusteReporte.Origen.SERVICIO_SINIESTRO.getValue()){
				gastoAjusteRep.setServicioSiniestro(servicioSiniestroService.obtener(ServicioSiniestro.class, new Long(gastoAjusteDTO.getPrestadorAjusteId())));
			}
			gastoAjusteRep.setOrigen(gastoAjusteDTO.getOrigen());
		}
		gastoAjusteRep.setMotivoSituacion( (gastoAjusteDTO.getMotivoSituacion() != null && ! "".equalsIgnoreCase(gastoAjusteDTO.getMotivoSituacion()) ) 
											? Integer.parseInt(gastoAjusteDTO.getMotivoSituacion()) : null);
		gastoAjusteRep.setNumeroFactura(gastoAjusteDTO.getNumeroFactura());
		if(gastoAjusteDTO.getMostrarSeccionGrua()){
			gastoAjusteRep.setNumeroReporteExterno(gastoAjusteDTO.getNumeroReporteExterno());
			gastoAjusteRep.setTallerAsignado(gastoAjusteDTO.getTallerAsignadoId() != null ? prestadorService.buscarPrestador(gastoAjusteDTO.getTallerAsignadoId()) : null);
			gastoAjusteRep.setTipoGrua( (gastoAjusteDTO.getTipoGrua() != null &&  !"".equals(gastoAjusteDTO.getTipoGrua()) )
										? Integer.parseInt(gastoAjusteDTO.getTipoGrua()) : null);
		}
		if(gastoAjusteRep.getBusinessKey() == null){
			gastoAjusteRep.setFechaCreacion(new Date());
			gastoAjusteRep.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
			reporte.getGastosAjuste().add(gastoAjusteRep);
		}else{
			gastoAjusteRep.setFechaModificacion(new Date());
			gastoAjusteRep.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		}
		gastoAjusteRep.setReporteCabina(reporte);
		entidadService.save(gastoAjusteRep);
	}

	@Override
	public List<Map<String, Object>> obtenerGastosAjuste(Long idReporteCabina) {
		Map<String, Object> map;
		List<Map<String, Object>> gastoList = new ArrayList<Map<String, Object>>();
		Map<String, String> mapPrestadores = consultarPrestadoresGastoAjuste();
		Map<String, String> motivosSituacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.MOTIVO_SITUACION);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reporteCabina.id", idReporteCabina);
		List<GastoAjusteReporte> gastos = entidadService.findByProperties(GastoAjusteReporte.class, params);
		for(GastoAjusteReporte gasto: gastos){
			map = new LinkedHashMap<String, Object>();
			map.put("id", gasto.getId());
			map.put("tipoPrestador", mapPrestadores.get(gasto.getTipoPrestadorServicio()));
			if(gasto.getOrigen() == GastoAjusteReporte.Origen.PRESTADOR.getValue()){
				map.put("nombrePrestador", gasto.getPrestadorServicio().getPersonaMidas().getNombre());
			}else{
				map.put("nombrePrestador", gasto.getServicioSiniestro().getPersonaMidas().getNombre());
			}
			map.put("motivoSituacion", gasto.getMotivoSituacion() != null ? motivosSituacion.get(String.valueOf(gasto.getMotivoSituacion())) : null);
			map.put("fechaAsignacion", gasto.getFechaCreacion());
			gastoList.add(map);
		}
		return gastoList;
	}

	@Override
	public Map<String, String> consultarPrestadoresGastoAjuste() {
		Map<String, String> tipoPresMap = prestadorDao.consultarPrestadoresGastoAjuste();
		Map<String, String> sevSerMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.SIN_TIPO_SERVICIO);
		Map<String, String> map= new LinkedHashMap<String, String>();
		map.putAll(tipoPresMap);
		map.putAll(sevSerMap);
		return map;
	}

	@Override
	public List<PrestadorGastoAjusteDTO> buscarPrestadorPorNombreYTipo(String nombre, String tipoPrestador) {
		// TODO Auto-generated method stub
		return prestadorDao.buscarPrestadorPorNombre(nombre, tipoPrestador);
	}

	@Override
	public GastoAjusteReporte buscarGasto(Long idGasto) {
		// TODO Auto-generated method stub
		return entidadService.findById(GastoAjusteReporte.class, idGasto);
	}

	@Override
	public TipoPrestadorServicio buscarTipoPrestadorbyNombre(String nombre) {
		// TODO Auto-generated method stub
		return prestadorDao.buscarTipoPrestadorbyNombre(nombre);
	}
	
	@Override
	public TipoPrestadorServicio buscarTipoPrestador(Long idTipoPrestador) {
		return entidadService.findById(TipoPrestadorServicio.class, idTipoPrestador);
	}
	
	@Override
	public TransporteImpresionDTO imprimirValeTaller( ValeTallerDTO valeTaller ){
		List<ValeTallerDTO> dataSourceValeTaller = new ArrayList<ValeTallerDTO>();
		JRBeanCollectionDataSource collectionDataSource = null;
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		Map<String, Object> params = new HashMap<String, Object>();
		
		dataSourceValeTaller.add( valeTaller );
		
		/* Compilado de jReport y generaciÃ³n del .jasper */
		String jrxml = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/siniestros/CaratulaValeTaller.jrxml";
		JasperReport jReport = gImpresion.getOJasperReport( jrxml );
		
		/* Se definen los parÃ¡metros a mapear en el jrxml */
		params.put("PRUTAIMAGEN", SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		
		collectionDataSource = new JRBeanCollectionDataSource( dataSourceValeTaller );
		
		// Rellena el reporte con los datos y exporta el PDF
		transporte = gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
		
		return transporte;
	}

}
