package mx.com.afirme.midas2.service.impl.notificacionCierreMes;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.notificacionCierreMes.NotificacionCierreMesDao;
import mx.com.afirme.midas2.domain.catalogos.notificacionCierreMes.NotificacionCM;
import mx.com.afirme.midas2.domain.movil.cliente.CorreosCosultaMovil;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.service.NotificacionesCierreMes.NotificacionCierreMesService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.log4j.Logger;

@Stateless
public class NotificacionCierreMesServiceImpl implements
		NotificacionCierreMesService {

	private static final Logger LOG = Logger
			.getLogger(NotificacionCierreMesServiceImpl.class);

	/**
	 * Inicializaciones Objetos Listas y Servicios
	 */
	private EntidadService entidadService;
	private ParametroGlobalService parametroGlobalService;
	private MailService mailService;
	private NotificacionCierreMesService notificacionCierreMesService;
	private NotificacionCierreMesDao notificacionCierreMesDao;

	private List<CorreosCosultaMovil> lstCorreos = new ArrayList<CorreosCosultaMovil>();
	private ParametroGlobal paraGlo = new ParametroGlobal();

	@EJB
	public void setParametroGlobalService(
			ParametroGlobalService parametroGlobalService) {
		this.parametroGlobalService = parametroGlobalService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setNotificacionCierreMesDao(
			NotificacionCierreMesDao notificacionCierreMesDao) {
		this.notificacionCierreMesDao = notificacionCierreMesDao;
	}

	@EJB
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	/**
	 * Variables Globales
	 */
	private String fechaEjecucion;

	public void setLstCorreos(List<CorreosCosultaMovil> lstCorreos) {
		this.lstCorreos = lstCorreos;
	}

	public List<CorreosCosultaMovil> getLstCorreos() {
		return lstCorreos;
	}

	
	@Override
	public List<CorreosCosultaMovil> getCorreosLista() {
		// TODO Auto-generated method stub
		LOG.info("NotificacionCierreMesServiceImpl");
		return notificacionCierreMesDao.getCorreosLista();
	}

	/**
	 * Se guarda la nueva fecha de configuracion por el administrador para la
	 * ejecucion del job. Esto se guarda en la tabla de configuracion.
	 * 
	 * */
	public void saveNotificacionMes(ParametroGlobal pGlobal) {

		String fechaActual = "";
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaActual = sdf.format(Calendar.getInstance().getTime());
		String userName = "";
		String fechaNueva;
		boolean active;

		Date ahora = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
		LOG.info(formateador.format(ahora));

		try {
			String fecha = formateador.format(ahora);
			Date dt = null;
			if ((fecha != null) && (!"".equals(fecha))) {
				dt = formateador.parse(fecha);
			}

			active = pGlobal.isActivo();
			fechaNueva = pGlobal.getValor();

			/** Obtener la fecha de la Base de Datos */
			fechaEjecucion = parametroGlobalService.obtenerValorPorIdParametro(
					ParametroGlobalService.AP_MIDAS,
					ParametroGlobalService.NOTIFICACION_CIERRE_MES);

			LOG.info("saveNotificacionMes() fechaEjecucion :" + fechaEjecucion
					+ " fecha Nueva :" + pGlobal.getValor());

			if (fechaEjecucion == fechaNueva) {
				LOG.info("saveNotificacionMes () - No se modifica el job");

			} else {
				LOG.info("saveNotificacionMes () - update Job");

				String timerInfo = "StartTimerNotificacion";
				String[] fechaFinal = fechaNueva.split("/");
				String dia = fechaFinal[0];
				LOG.info("dia :" + dia);
				cancelTimer(timerInfo);
				createTimer(dia);
			}

			userName = usuarioService.getUsuarioActual().getNombreUsuario();
			LOG.info("saveNotificacionMes () - nameUser: " + userName);

			/** Guardar en la tabla de configuracion */
			entidadService.save(pGlobal);
			LOG.info("saveNotificacionMes () - save Succes");

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("saveNotificacionMes () - Error al guardar en Parametro Global: "
					+ e);
		}

	}

	/**
	 * Metodo para validar antes de realizar el envio de correo electronico para
	 * los agentes que esten en la tabla con el estatus indicado. Validaciones:
	 * -Se obtiene la fecha y el estatus del ultimo correo exitoso enviado -Se
	 * obtiene la lista de correos de los agentes. -Se valida que el servicio
	 * este Activo en la tabla de configuraciones. -Se realiza validacion que el
	 * Mes de la ultima ejecucion exitosa sea diferente a la del mes actual para
	 * poder realizar el envio del correo en caso de no cumplir con esta
	 * validacion no se envian los correos. -Se manda ejecutar el metodo para
	 * guardar el registro en la bitacora en caso de que se ejecute el envio de
	 * correo
	 */
	public void validaenviaNotificacionCierre() {

		List<CorreosCosultaMovil> listAgentes = new ArrayList<CorreosCosultaMovil>();
		List<NotificacionCM> listBitacora = new ArrayList<NotificacionCM>();
		List<NotificacionCM> regBitacora = new ArrayList<NotificacionCM>();
		ParametroGlobal pGlobal = new ParametroGlobal();
		NotificacionCM notifiModel = new NotificacionCM();

		Map<String, Object> params = new HashMap<String, Object>();
		StringBuilder queryWhere = new StringBuilder();
		StringBuilder queryWhereCount = new StringBuilder();
		List<String> bitData = new ArrayList<String>();

		String fechaActual = "";
		String cuerpoMensaje;
		String titulo = "";
		String userName = "";
		String fechSys;
		boolean active;

		String mAct = "";
		String mBit = "";
		String desBit = "";

		/* Variables de Estatus */

		String descrip = "";
		String estatus = "";
		String usuarioAlta = "";
		Date fechaAlta = null;
		String usuarioActualiza ="";
		Date fechaActualiza = null;

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		fechaActual = sdf.format(Calendar.getInstance().getTime());

		Date dt = new Date();
		SimpleDateFormat formateador = new SimpleDateFormat("dd/MM/yyyy");
		LOG.info(formateador.format(dt));

		try {
			LOG.info("validaenviaNotificacionCierre() - Ejecutando tarea programada NotifiacionCierreMes");
			/**Se agrega el texto a la variable titulo
			 * que se agrega al correo enviado*/
			titulo = "Notificaciones Cierre Mes";
			/**Se obtiene el registr de configuracion de la BD*/
			pGlobal = parametroGlobalService.obtenerPorIdParametro(
					ParametroGlobalService.AP_MIDAS,
					ParametroGlobalService.NOTIFICACION_CIERRE_MES);
			
			/**Se obtiene la fecha, estatus, usuario que creo el registro
			 *  para la ejecucion del proceso de envio de correos*/
			
			pGlobal.getValue();
			active = pGlobal.isActivo();
			fechSys = pGlobal.getValor();
			userName = pGlobal.getCodigoUsuarioCreacion();

			/**Se obtiene el cuerpo del correo que sera enviado en el correo*/
			String bodyCorreo = Utilerias
					.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS,
							"midas.agente.reportes.reporteNotificacionCierreMensual.correo.cuerpo");
			LOG.info("validaenviaNotificacionCierre() -  bodyCorreo : -"
					+ bodyCorreo);
			List<String> destinatarios = new ArrayList<String>();

			queryWhere
					.append(" model.idBitacora = (select max(model2.idBitacora) from NotificacionCM model2)"
							+ " and model.descripcionEnvio like '%Succes%'");

			listBitacora = entidadService
					.findByColumnsAndProperties(
							NotificacionCM.class,
							"idBitacora,descripcionEnvio,estatus,usuarioAlta,fechaAlta,usuarioAtualiza,fechaActualiza",
							null, null, queryWhere, null);
			LOG.info("liscant listBitacora.size(): " + listBitacora.size());
			int liscant = listBitacora.size();
			
			queryWhereCount.append(" model.idBitacora = (select max(model2.idBitacora) from NotificacionCM model2)");
			regBitacora = entidadService
					.findByColumnsAndProperties(
							NotificacionCM.class,
							"idBitacora,descripcionEnvio,estatus,usuarioAlta,fechaAlta,usuarioAtualiza,fechaActualiza",
							null, null, queryWhereCount, null);
			LOG.info("liscant: " + regBitacora);
			
			/**
			 * Valida si existe un registro en la tabla, en caso de no existir se le asigna 1 a la variable 
			 * liscant para que sea ejecutado el servicio por primera vez
			 */	
			Long existeRegistro = null;
			int regE = regBitacora.size();
			if(regE ==0){
				liscant=1;
				LOG.info("regEx entro : "+liscant);
			}
			
			/** Valida si existe un registro almacenado en la bitacora */
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");  
			Date mifecha = new Date();
			try{
				mifecha = dateFormat.parse("11/11/1999");	
			}catch(Exception e){
				e.printStackTrace();
			}
			if (liscant != 0) {
				NotificacionCM bitacs = null;
				Date feBit;
				LOG.info("validaenviaNotificacionCierre() - registro Succes");
				if(existeRegistro==null ){
					LOG.info("existeRegistro ");
					feBit = mifecha;
					desBit="Succes";
				}else{
					bitacs = listBitacora.get(0);
					feBit = bitacs.getFechaAlta();
					desBit = bitacs.getDescripcionEnvio();
				}
				/** Fecha de alta Bitacora */
				String fechaCadena = sdf.format(feBit);
				String[] mesBit = fechaCadena.split("/");
				/** Se obtiene el mes de la fecha de la bitacora */
				mBit = mesBit[1];
				/** Se obtiene el Mes Actual */
				String[] mesAct = fechaActual.split("/");
				mAct = mesAct[1];
				LOG.info("validaenviaNotificacionCierre() - active :" + active
						+ " liscant :" + liscant + " mAct "+ mAct + "!= mBit : "+ mBit + !mAct.equalsIgnoreCase(mBit)
						+ " descBit = Succes :"	+ desBit.equalsIgnoreCase("Succes"));

				if (active && !mAct.equalsIgnoreCase(mBit)) {
					LOG.info("validaenviaNotificacionCierre() - active");

					listAgentes = getCorreosLista();/* Llamada local */

					String blo = parametroGlobalService
							.obtenerValorPorIdParametro(
									ParametroGlobalService.AP_MIDAS,
									ParametroGlobalService.VARIABLEBLOQUESNOTIFICACIONCM);
					int bloque = Integer.parseInt(blo);

					LOG.info(listAgentes.size());
					int contador = 0;
					int cantRegistros = listAgentes.size();
					int vueltaBloque = 0;
					try {
						List<String> listCorreo = new ArrayList<String>();
						LOG.info("Cantidad de vueltas :" + cantRegistros / bloque);
			
						for (CorreosCosultaMovil lstAg : listAgentes) {
							if (lstAg.getCorreo() != null) {
								destinatarios.add(lstAg.getCorreo());
								contador++;
							}
							/**
							 * se valida que el contador sea igual al numero de
							 * bloques permitidos
							 */
							if (contador == bloque) {
								vueltaBloque++;
								LOG.info("validaenviaNotificacionCierre() - destinatarios resto :"
										+ destinatarios);
								mailService.sendMessage(listCorreo, titulo,
										bodyCorreo,
										"administrador.midas@afirme.com", null,
										null, null, destinatarios, null);

								contador = 0;
								destinatarios.clear();
								LOG.info("vueltaBloque : " + vueltaBloque);
							}
						}
						/** Envio de reciduo de correos pendientes de salida */
						if (contador > 0) {
							LOG.info("validaenviaNotificacionCierre() - destinatarios resto :"
									+ destinatarios);
							
							mailService.sendMessage(listCorreo, titulo, bodyCorreo,
									"administrador.midas@afirme.com", null,
									null, null, destinatarios, null);

							vueltaBloque++;
							//destinatarios.clear();
							LOG.info("vueltaBloque : " + vueltaBloque);

						}
						vueltaBloque = 0;

						descrip = "Succes";
						estatus = "Envio Exitoso";
						usuarioAlta = userName;
						fechaAlta = dt;
						usuarioActualiza = "";
						fechaActualiza = dt;
						LOG.info("validaenviaNotificacionCierre() - saveBitacora Hitorial Notificacion Cierre de Mes");
					} catch (Exception e) {
						
						descrip = "Error";
						estatus = "Error en el Envio de Correo";
						usuarioAlta = userName;
						fechaAlta = dt;
						usuarioActualiza = "";
						fechaActualiza = dt;
						
						LOG.error("validaenviaNotificacionCierre() -Ocurrio un problema al enviar los correos :"
								+ e);
					}
				} else if (!active) {

					descrip = "Fail";
					estatus = "El Servicio esta Inactivo";
					usuarioAlta = userName;
					fechaAlta = dt;
					usuarioActualiza = "";
					fechaActualiza = dt;

					LOG.error("validaenviaNotificacionCierre() - No cumple con las condiciones para el envio de notificacion");
				} else if (mAct.equalsIgnoreCase(mBit)) {

					descrip = "Fail";
					estatus = "Ya fue Ejecutado en el Mes";
					usuarioAlta = userName;
					fechaAlta = dt;
					usuarioActualiza = "";
					fechaActualiza = dt;

					LOG.error("validaenviaNotificacionCierre() - Ya fue Ejecutado en el Mes");
				}
			} else {
				
				descrip = "Succes";
				estatus = "Se ingresa el primer Registro";
				usuarioAlta = userName;
				fechaAlta = mifecha;
				usuarioActualiza = "";
				fechaActualiza = mifecha;

				LOG.error("validaenviaNotificacionCierre() - Se ingresa el primer Registro");
			}
			notifiModel.setDescripcionEnvio(descrip);
			notifiModel.setEstatus(estatus);
			notifiModel.setUsuarioAlta(usuarioAlta);
			notifiModel.setFechaAlta(fechaAlta);
			notifiModel.setUsuarioAtualiza(usuarioActualiza);
			notifiModel.setFechaActualiza(fechaActualiza);
			
			saveBitacora(notifiModel);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("validaenviaNotificacionCierre() - Exception e: " + e);
		}
	}

	/**
	 * Se guarda el registro en la bitacora indicando el estatus
	 * de la ejecucion del proceso de envio de correos.
	 * @param notifiModel
	 * @throws ParseException
	 */
	public void saveBitacora(NotificacionCM notifiModel) throws ParseException {
		LOG.info("saveBitacora()");
		try {
			entidadService.save(notifiModel);
			LOG.info("saveBitacora() - save Succes");
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("saveBitacora() - e " + e);
		}
	}

	/**
	 * Se inicia el Job al cargar la pantalla
	 * con los parametros de la BD.
	 */
	@Resource
	TimerService timerService;

	public void initialize() {
		LOG.info(">>> initialize JOB NotificacionCierreMes <<<");
		ScheduleExpression expression = new ScheduleExpression();
		String diaEjeBD = null;
		try {
			try {
				fechaEjecucion = parametroGlobalService
						.obtenerValorPorIdParametro(
								ParametroGlobalService.AP_MIDAS,
								ParametroGlobalService.NOTIFICACION_CIERRE_MES);

				String[] diaBD = fechaEjecucion.split("/");
				diaEjeBD = diaBD[0];
				LOG.info("initialize() - Dia :" + diaEjeBD);

			} catch (Exception e) {
				e.printStackTrace();
				LOG.error("initialize() - Se envia fecha default - Error :" + e);
				diaEjeBD = "28";
			}

			String horario = parametroGlobalService.obtenerValorPorIdParametro(
					ParametroGlobalService.AP_MIDAS,
					ParametroGlobalService.TIMERTASKNOTIFICACIONCIERREMES);

			String minutos = parametroGlobalService.obtenerValorPorIdParametro(
					ParametroGlobalService.AP_MIDAS,
					ParametroGlobalService.MINTASKNOTIFICACIONCIERREMES);

			int hr = Integer.parseInt(horario.trim());
			int min = Integer.parseInt(minutos);
			
			LOG.info("createTimer() - Hora de Configuracion Job ="+ hr +":" + min);

			expression.minute(min);
			expression.hour(hr);
			expression.dayOfMonth(diaEjeBD);

			timerService.createCalendarTimer(expression, new TimerConfig(
					"StartTimerNotificacion", false));

		} catch (Exception e) {
			LOG.error("initialize() - Error  :" + e);
		}

	}

	@Timeout
	public void execute() {
		Calendar calendario = new GregorianCalendar();
		int hora, minutos, segundos;
		hora = calendario.get(Calendar.HOUR_OF_DAY);
		minutos = calendario.get(Calendar.MINUTE);
		segundos = calendario.get(Calendar.SECOND);
		LOG.info("execute() - Invoked: " + hora + ":" + minutos + ":"+ segundos);
		validaenviaNotificacionCierre();
	}

	/**
	 * Proceso para iniciar el Job nuevamente con algun cambio echo por el
	 * usuario
	 * 
	 * @param day
	 */
	public void createTimer(String day) {
		try {
			ScheduleExpression expression = new ScheduleExpression();

			String horario = parametroGlobalService.obtenerValorPorIdParametro(
					ParametroGlobalService.AP_MIDAS,
					ParametroGlobalService.TIMERTASKNOTIFICACIONCIERREMES);

			String minutos = parametroGlobalService.obtenerValorPorIdParametro(
					ParametroGlobalService.AP_MIDAS,
					ParametroGlobalService.MINTASKNOTIFICACIONCIERREMES);
			
			int hr = Integer.parseInt(horario.trim());
			int min = Integer.parseInt(minutos.trim());
			LOG.info("createTimer() - Hora de Configuracion Job :"+ hr +" :" + min);

			/**
			 * Minutos Horas de la Ejecucion del Job Dia de Ejecucion del Job
			 * expression.minute("* /2").hour("*");
			 */
			expression.minute(min);
			expression.hour(hr);
			expression.dayOfMonth(day);

			timerService.createCalendarTimer(expression, new TimerConfig(
					"StartTimerNotificacion", false));
			LOG.info("createTimer() -create Succes Job");

		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("createTimer() - Error createTimer: " + e);
		}

	}

	/**
	 * Proceso para cancelacion del proceso del Job y agregar nuevo parametro
	 * agregado
	 */
	public void cancelTimer(String timerInfo) {
		LOG.info("cancelTimer() - " + timerService.getTimers());
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
				LOG.info("cancelTimer() - Elimino Job");

			}
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("cancelTimer() - Error cancelTimer :" + e);
		}

	}

	public String convertFecha(String diaEjecucion) {
		String fechaActual = "";
		try {
			int incrementoMes = 1;
			int mesAc;
			int anoActual = 0;
			int diaActual = 0;
			/**
			 * Iterar hasta encontra el mes mas proximo que tiene el dia
			 * configurado....
			 */
			Calendar calendar = Calendar.getInstance();

			diaActual = calendar.get(Calendar.DAY_OF_MONTH);
			mesAc = calendar.get(Calendar.MONTH);
			anoActual = calendar.get(Calendar.YEAR);

			int iYear = anoActual;
			int iMonth = mesAc;
			int iDay = Integer.parseInt(diaEjecucion);

			calendar = new GregorianCalendar(iYear, iMonth, 1);
			/** Incrementa un mes Si mi dia pero del siguente mes */
			if (diaActual >= Integer.parseInt(diaEjecucion)) {
				calendar.add(Calendar.MONTH, incrementoMes);
			}
			int min = calendar.getActualMinimum(Calendar.DAY_OF_MONTH);
			int maxD = calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
			if (iDay <= maxD) {
			} else {
				calendar.add(Calendar.MONTH, incrementoMes);
			}
			/**
			 * validar SI el dia configurado se encuentra en el rango del
			 * siguiente mes Regresa esa fecha
			 **/
			calendar.set(Calendar.DAY_OF_MONTH, iDay);
			Date fechProx = calendar.getTime();

			/**
			 * De lo contrario incrementar un mes mas y continuar con la
			 * siguiente iteracion
			 */
			SimpleDateFormat sdfs = new SimpleDateFormat("dd/MM/yyyy");
			fechaActual = sdfs.format(fechProx);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return fechaActual;
	}

}
