package mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion.paquete.cobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.CobPaquetesSeccionDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.catalogos.ValorSeccionCobAutosDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionDAO;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutos;
import mx.com.afirme.midas2.domain.catalogos.ValorSeccionCobAutosId;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionCobertura;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionCoberturaId;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.cobertura.RelacionesNegocioCoberturaDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionService;
import mx.com.afirme.vida.domain.movil.cotizador.SumaAseguradaDTO;

import org.apache.log4j.Logger;

@Stateless
public class NegocioCobPaqSeccionServiceImpl implements NegocioCobPaqSeccionService{

	public static final Logger LOG = Logger.getLogger(NegocioCobPaqSeccionServiceImpl.class);
	
	private CoberturaFacadeRemote coberturaFacadeRemote;
	private CobPaquetesSeccionDao cobPaquetesSeccionDao;	
	private EntidadDao entidadDao;
	private NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO;
	private ValorSeccionCobAutosDao valorSeccionCobAutosDao;	

	protected EntidadService entidadService;
	
	@Override
	public RelacionesNegocioCoberturaDTO getRelationLists(
			NegocioCobPaqSeccion negocioCobPaqSeccion) {
			List<NegocioCobPaqSeccion> negocioCobPaqSeccionAsociadas = new ArrayList<NegocioCobPaqSeccion>(1); 
			negocioCobPaqSeccionAsociadas = negocioCobPaqSeccionDAO.getNegocioCobPaqSeccionAsociados(negocioCobPaqSeccion);			
			
		List<CobPaquetesSeccion> posibles = cobPaquetesSeccionDao
				.getPorSeccionPaquete(negocioCobPaqSeccion.getNegocioPaqueteSeccion().getNegocioSeccion()
						.getSeccionDTO().getIdToSeccion().longValue(),
						negocioCobPaqSeccion.getNegocioPaqueteSeccion().getPaquete().getId());
			List<NegocioCobPaqSeccion> negocioCobPaqSeccionDisponibles = new ArrayList<NegocioCobPaqSeccion>(1);
			
			RelacionesNegocioCoberturaDTO relacionesNegocioCoberturaDTO = new RelacionesNegocioCoberturaDTO();
			
			List<CobPaquetesSeccion> coberturasAsociadas = new ArrayList<CobPaquetesSeccion>(1);
			
			for(NegocioCobPaqSeccion item: negocioCobPaqSeccionAsociadas){
				CobPaquetesSeccionId cobPaquetesSeccionId = new CobPaquetesSeccionId();
				cobPaquetesSeccionId.setIdPaquete(item.getNegocioPaqueteSeccion().getPaquete().getId());
				cobPaquetesSeccionId.setIdToCobertura(item.getCoberturaDTO().getIdToCobertura().longValue());
				cobPaquetesSeccionId.setIdToSeccion(item.getNegocioPaqueteSeccion().getNegocioSeccion().getSeccionDTO().getIdToSeccion().longValue());
				
				CobPaquetesSeccion cobPaquetesSeccion = new CobPaquetesSeccion();
				cobPaquetesSeccion.setId(cobPaquetesSeccionId);
				//cobPaquetesSeccion.setClaveObligatoriedad(0);
				coberturasAsociadas.add(cobPaquetesSeccion);
			}
			
			for(CobPaquetesSeccion item:posibles){
				if(!coberturasAsociadas.contains(item)){
					NegocioCobPaqSeccion tempNegocioCobPaqSeccion = new NegocioCobPaqSeccion();
					tempNegocioCobPaqSeccion.setNegocioPaqueteSeccion(negocioCobPaqSeccion.getNegocioPaqueteSeccion());					
					CoberturaDTO coberturaDTO = coberturaFacadeRemote.findById(BigDecimal.valueOf(item.getId().getIdToCobertura()));
					tempNegocioCobPaqSeccion.setCoberturaDTO(coberturaDTO);
				
					//Asignar valores de sumas aseguradas a las posibles coberturas.
					ValorSeccionCobAutos valorSeccionCobAutos = getValorSeccionCobAutos(negocioCobPaqSeccion, coberturaDTO);
					
					tempNegocioCobPaqSeccion.setValorSumaAseguradaDefault(valorSeccionCobAutos.getValorSumaAseguradaDefault());
					tempNegocioCobPaqSeccion.setValorSumaAseguradaMax(valorSeccionCobAutos.getValorSumaAseguradaMax());
					tempNegocioCobPaqSeccion.setValorSumaAseguradaMin(valorSeccionCobAutos.getValorSumaAseguradaMin());
					tempNegocioCobPaqSeccion.setClaveTipoSumaAsegurada(valorSeccionCobAutos.getClaveTipoLimiteSumaAseg());
					
					tempNegocioCobPaqSeccion.setClaveTipoDeduciblePP((short) 0);
					tempNegocioCobPaqSeccion.setClaveTipoDeduciblePT((short) 0);
					
					tempNegocioCobPaqSeccion.setCiudadDTO(negocioCobPaqSeccion.getCiudadDTO());
					tempNegocioCobPaqSeccion.setMonedaDTO(negocioCobPaqSeccion.getMonedaDTO());
					tempNegocioCobPaqSeccion.setEstadoDTO(negocioCobPaqSeccion.getEstadoDTO());
					tempNegocioCobPaqSeccion.setTipoUsoVehiculo(negocioCobPaqSeccion.getTipoUsoVehiculo());
					tempNegocioCobPaqSeccion.setAgente(negocioCobPaqSeccion.getAgente());
					tempNegocioCobPaqSeccion.setRenovacion(negocioCobPaqSeccion.isRenovacion());
					
					negocioCobPaqSeccionDisponibles.add(tempNegocioCobPaqSeccion);
				}
			}
			
			relacionesNegocioCoberturaDTO.setAsociadas(negocioCobPaqSeccionAsociadas);
			relacionesNegocioCoberturaDTO.setDisponibles(negocioCobPaqSeccionDisponibles);		
		return relacionesNegocioCoberturaDTO;
	}

	@Override
	public RespuestaGridRelacionDTO relacionarNegocioCobPaqSeccion(String accion,
			NegocioCobPaqSeccion negocioCobPaqSeccion) {
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		if(negocioCobPaqSeccion.getValorSumaAseguradaMin().intValue()>negocioCobPaqSeccion.getValorSumaAseguradaMax().intValue()){
			respuesta.setMensaje("El Valor Suma Asegurada M\u00EDnimo no puede ser Mayor a la Suma Asegurada M\u00E1xima");
			respuesta.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			return respuesta;
		}

		if(negocioCobPaqSeccion.getValorSumaAseguradaDefault().intValue()< negocioCobPaqSeccion.getValorSumaAseguradaMin().intValue()){
			respuesta.setMensaje("El Valor Suma Asegurada Default no puede ser menor a la Suma Asegurada M\u00EDnima");
			respuesta.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			return respuesta;
		}

		if(negocioCobPaqSeccion.getValorSumaAseguradaDefault().intValue()> negocioCobPaqSeccion.getValorSumaAseguradaMax().intValue()){
			respuesta.setMensaje("El Valor Suma Asegurada Default no puede ser Mayor a la Suma Asegurada M\u00E1xima");
			respuesta.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_ERROR);
			return respuesta;
		}		

		entidadDao.executeActionGrid(accion, negocioCobPaqSeccion);
		respuesta.setTipoMensaje(MensajeDTO.TIPO_MENSAJE_EXITO);
		return respuesta;
	}
	
	@Override
	@SuppressWarnings("unchecked")
	public List<NegocioRenovacionCobertura> getCoberturasPorNegSeccion(Long idToNegocio, Short tipoRiesgo, BigDecimal idToNegSeccion){
		List<NegocioRenovacionCobertura> list = new ArrayList<NegocioRenovacionCobertura>(1);		
		try{
			//Obtiene seccion
			NegocioSeccion negocioSeccion = entidadDao.findById(NegocioSeccion.class, idToNegSeccion);
			Map<String, Object> param = new HashMap<String, Object>(1);
			param.put("1", idToNegocio);
			param.put("2", tipoRiesgo);
			param.put("3", negocioSeccion.getSeccionDTO().getIdToSeccion());
			param.put("4", idToNegSeccion);
			
			String queryStr = "SELECT DISTINCT NSS.IDTOSECCION,  CC.IDTOCOBERTURA, CC.NOMBRECOMERCIALCOBERTURA, " +
					" NVL(NCC.APLICADEFAULT,0) APLICADEFAULT " +
					" FROM MIDAS.TONEGCOBPAQSECCION COB, MIDAS.TONEGPAQUETESECCION PAQ, MIDAS.TONEGSECCION NSS, MIDAS.TOCOBERTURA CC " +
					" LEFT JOIN MIDAS.TONEGRENCOBERTURA NCC " +
					"	ON NCC.IDTONEGOCIO = ? " + 
					"	and NCC.TIPORIESGO = ? " + 
					" 	AND NCC.IDTOSECCION = ? " + 
					" 	AND NCC.IDTOCOBERTURA = CC.IDTOCOBERTURA " +
					" WHERE COB.IDTONEGPAQUETESECCION = PAQ.IDTONEGPAQUETESECCION " +
					" AND COB.IDTOCOBERTURA = CC.IDTOCOBERTURA " +
					" AND PAQ.IDTONEGSECCION = NSS.IDTONEGSECCION  " +
					" AND NSS.IDTONEGSECCION = ? " +
					" ORDER BY CC.NOMBRECOMERCIALCOBERTURA ";
			
			List<Object[]> listQuery = entidadDao.executeNativeQueryMultipleResult(queryStr, param);
			list.addAll(populateList(listQuery, idToNegocio, tipoRiesgo));
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return list;
	}
	
	public List<NegocioCobPaqSeccion> getCoberturaPorNegSeccion(BigDecimal idToCobertura,String stateId,String cityId, Short idTcMoneda,Long idToNegPaqueteSeccion, BigDecimal IdTcTipoUsoVehiculo, Long agenteId, Boolean esRenovacion){
		List<NegocioCobPaqSeccion> negocioCobPaqSeccionAsociadas = new ArrayList<NegocioCobPaqSeccion>(1); 
		negocioCobPaqSeccionAsociadas = negocioCobPaqSeccionDAO.getNegocioCobPaqSeccionAsociados(idToCobertura, stateId, cityId,  idTcMoneda, idToNegPaqueteSeccion, IdTcTipoUsoVehiculo, agenteId, esRenovacion);		
		return negocioCobPaqSeccionAsociadas;
	}
	
	public NegocioCobPaqSeccion getLimitesSumaAseguradaPorCobertura(
			BigDecimal idToCobertura,String stateId,String cityId, BigDecimal idTcMoneda,Long idToNegPaqueteSeccion){
		return negocioCobPaqSeccionDAO.getLimitesSumaAseguradaPorCobertura(
				idToCobertura, stateId, cityId,  idTcMoneda, idToNegPaqueteSeccion);
	}
	
	private List<NegocioRenovacionCobertura> populateList(List<Object[]> listQuery, Long idToNegocio, Short tipoRiesgo) {
		List<NegocioRenovacionCobertura> list = new ArrayList<NegocioRenovacionCobertura>(1);	
		if(listQuery != null && !listQuery.isEmpty()){
			for(Object[] item: listQuery){
				NegocioRenovacionCobertura negocioRenovacionCobertura = new NegocioRenovacionCobertura();
				NegocioRenovacionCoberturaId id = new NegocioRenovacionCoberturaId();
				id.setIdToNegocio(idToNegocio);
				id.setTipoRiesgo(tipoRiesgo);
				
				Object item0 = item[0];
				id.setIdToSeccion(getValue(item0));
				
				item0 = item[1];					
				id.setIdToCobertura(getValue(item0));
				
				item0 = item[2];
				negocioRenovacionCobertura.setNombreComercialCobertura((String) item0);
				
				item0 = item[3];					
				negocioRenovacionCobertura.setAplicaDefault(getValue(item0).shortValue());					
				negocioRenovacionCobertura.setId(id);
				
				list.add(negocioRenovacionCobertura);					
			}
		}
		return list;
	}

	private BigDecimal getValue(Object item) {
		BigDecimal value = BigDecimal.ZERO;
		if(item instanceof BigDecimal){
			value = (BigDecimal)item;
		}else if (item instanceof Long){
			value = BigDecimal.valueOf((Long)item);
		}else if (item instanceof Double){
			value = BigDecimal.valueOf((Double)item);
		}
		return value;
	}

	private long getTipoUsoGeneral(NegocioCobPaqSeccion negocioCobPaqSeccion) {
		long tipoUsoGeneral = 0;
		try{
			Map<String,Object> params = new HashMap<String,Object>(1);
			params.put("negocioSeccion.idToNegSeccion", negocioCobPaqSeccion.getNegocioPaqueteSeccion().getNegocioSeccion().getIdToNegSeccion());
			params.put("claveDefault", true);
			List<NegocioTipoUso> negocioTipoUsoList = entidadDao.findByProperties(NegocioTipoUso.class, params);
			if(negocioTipoUsoList != null && !negocioTipoUsoList.isEmpty()){
				NegocioTipoUso negocioTipoUso = negocioTipoUsoList.get(0);
				tipoUsoGeneral = negocioTipoUso.getTipoUsoVehiculoDTO().getIdTcTipoUsoVehiculo().longValue();
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return tipoUsoGeneral;
	}

	private ValorSeccionCobAutos getValorSeccionCobAutos(NegocioCobPaqSeccion negocioCobPaqSeccion, CoberturaDTO coberturaDTO) {
		
		//Valor asignado al Tipo Uso Vehiculo. Por el momento se usa como default "Uso general"
		long tipoUsoGeneral = getTipoUsoGeneral(negocioCobPaqSeccion);
		ValorSeccionCobAutosId valorSeccionCobAutosId = new ValorSeccionCobAutosId(negocioCobPaqSeccion.getNegocioPaqueteSeccion().getNegocioSeccion().getSeccionDTO().getIdToSeccion().longValue(), coberturaDTO.getIdToCobertura().longValue(), negocioCobPaqSeccion.getMonedaDTO().getIdTcMoneda().longValue(), tipoUsoGeneral);
		ValorSeccionCobAutos valorSeccionCobAutos = valorSeccionCobAutosDao.findById(valorSeccionCobAutosId);
		
		if(valorSeccionCobAutos == null && tipoUsoGeneral > 0){
			valorSeccionCobAutosId = new ValorSeccionCobAutosId(
					negocioCobPaqSeccion.getNegocioPaqueteSeccion().getNegocioSeccion().getSeccionDTO().getIdToSeccion().longValue(), 
					coberturaDTO.getIdToCobertura().longValue(), negocioCobPaqSeccion.getMonedaDTO().getIdTcMoneda().longValue(), (long) 0);
			valorSeccionCobAutos = valorSeccionCobAutosDao.findById(valorSeccionCobAutosId);
		}
		
		if(valorSeccionCobAutos == null){
			valorSeccionCobAutos = new ValorSeccionCobAutos();
			double valorDefault = 0;
			Short claveTipoSumaAseguradaDefault = 1;
			if(coberturaDTO.getClaveTipoSumaAsegurada() != null){
				try{
					claveTipoSumaAseguradaDefault = Short.parseShort(coberturaDTO.getClaveTipoSumaAsegurada());
				}catch(Exception e){
					LOG.error(e.getMessage(), e);
				}
			}					
			valorSeccionCobAutos.setValorSumaAseguradaDefault(valorDefault);
			valorSeccionCobAutos.setValorSumaAseguradaMax(valorDefault);
			valorSeccionCobAutos.setValorSumaAseguradaMin(valorDefault);
			valorSeccionCobAutos.setClaveTipoLimiteSumaAseg(claveTipoSumaAseguradaDefault);
		}
		return valorSeccionCobAutos;
	}

	public List<NegocioCobSumAse> obtenerSumasAseguradas(BigDecimal idToNegCobPaqSeccion){
		List<NegocioCobSumAse> sumasAseguradas = entidadService.findByProperty(NegocioCobSumAse.class, "negocioCobPaqSeccion.idToNegCobPaqSeccion",idToNegCobPaqSeccion);
		
		if(sumasAseguradas != null && !sumasAseguradas.isEmpty()){
			Collections.sort(sumasAseguradas, new Comparator<NegocioCobSumAse>() {				
						public int compare(NegocioCobSumAse n1, NegocioCobSumAse n2){
							return n1.getValorSumaAsegurada().compareTo(n2.getValorSumaAsegurada());
						}
					});
		}
		return sumasAseguradas;
	}
	
	public RespuestaGridRelacionDTO relacionarSumasAseguradasCobertura(String accion, BigDecimal idToNegCobPaqSeccion, Long idSumaAsegurada, Double valorSumaAsegurada, short defaultValor) {
		Map<String, Object> parameters = new LinkedHashMap<String, Object>();
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		NegocioCobPaqSeccion negocioCobPaqSeccion = entidadService.findById(NegocioCobPaqSeccion.class, idToNegCobPaqSeccion);  
		NegocioCobSumAse negocioSumAseCob= new NegocioCobSumAse();
		
		if(idSumaAsegurada != null && !accion.equals("inserted")){
			negocioSumAseCob.setIdSumaAsegurada(idSumaAsegurada);
			
		}

		List<NegocioCobSumAse> sumasAseguradas = this.obtenerSumasAseguradas(idToNegCobPaqSeccion);
		if(sumasAseguradas == null || sumasAseguradas.size()==0){
			negocioSumAseCob.setDefaultValor((short)1);
		}

		String queryValor = "Select model.valorSumaAsegurada from NegocioCobSumAse model where model.negocioCobPaqSeccion.idToNegCobPaqSeccion = :idToNegCobPaqSeccion";
		parameters = new LinkedHashMap<String, Object>();
		parameters.put("idToNegCobPaqSeccion", idToNegCobPaqSeccion);
		@SuppressWarnings("unchecked")
		List<Double> valorSumasAseguradas = entidadDao.executeQueryMultipleResult(queryValor, parameters);
		
		if(valorSumasAseguradas!=null & valorSumasAseguradas.size()>0 & !accion.equals("deleted")){
			for(NegocioCobSumAse sumaAsegurada : sumasAseguradas){
				if(sumaAsegurada.getValorSumaAsegurada().equals(valorSumaAsegurada)){
					if(accion.equals("inserted")){
						respuesta.setTipoMensaje(mensajeError);
						respuesta.setMensaje("Registro duplicado");
						return respuesta;
					}else {
						if(sumaAsegurada.getDefaultValor() == defaultValor){
							respuesta.setTipoMensaje(mensajeError);
							respuesta.setMensaje("Registro duplicado");
							return respuesta;
						}
					}
				}
			}
//			if(valorSumasAseguradas.contains(negocioSumAseCob.getValorSumaAsegurada())){
//				respuesta.setTipoMensaje(mensajeError);
//				respuesta.setMensaje("Registro duplicado");
//				return respuesta;
//			}
		}
		
		String query = "Select max(model.numeroSecuencia) from NegocioCobSumAse model where model.negocioCobPaqSeccion.idToNegCobPaqSeccion = :idToNegCobPaqSeccion";
		Long numeroSecuencia = (Long) entidadDao.executeQuerySimpleResult(query, parameters);
		if(numeroSecuencia==null){
			numeroSecuencia=new Long(0);
		}
		negocioSumAseCob.setNumeroSecuencia(numeroSecuencia+1);
		if (accion.equals("inserted")) {

			negocioSumAseCob.setNegocioCobPaqSeccion(negocioCobPaqSeccion);
			negocioSumAseCob.setDefaultValor(defaultValor);
			negocioSumAseCob.setValorSumaAsegurada(valorSumaAsegurada != null ? valorSumaAsegurada : 0d );

			entidadService.save(negocioSumAseCob);
			obtenerSumasAseguradas(idToNegCobPaqSeccion);
			negocioSumAseCob = entidadService.getReference(NegocioCobSumAse.class, negocioSumAseCob.getIdSumaAsegurada());
			respuesta.setTipoMensaje(mensajeExito);
			
		} else if (accion.equalsIgnoreCase("updated")) {	
			negocioSumAseCob = entidadDao.getReference(NegocioCobSumAse.class, idSumaAsegurada);
			negocioSumAseCob.setValorSumaAsegurada(valorSumaAsegurada);

			negocioSumAseCob.setNegocioCobPaqSeccion(negocioCobPaqSeccion);
			negocioSumAseCob.setDefaultValor(defaultValor);
			negocioSumAseCob.setValorSumaAsegurada(valorSumaAsegurada != null ? valorSumaAsegurada : 0d );
            
        if (negocioCobPaqSeccion.getNegocioCobSumAseList().contains(negocioSumAseCob)) {
        	negocioCobPaqSeccion.getNegocioCobSumAseList().get(negocioCobPaqSeccion.getNegocioCobSumAseList().indexOf(negocioSumAseCob)).setValorSumaAsegurada(valorSumaAsegurada);
        	negocioCobPaqSeccion.getNegocioCobSumAseList().get(negocioCobPaqSeccion.getNegocioCobSumAseList().indexOf(negocioSumAseCob)).setDefaultValor(defaultValor);
              
         }else{
        	 negocioCobPaqSeccion.getNegocioCobSumAseList().add(negocioSumAseCob);
               
        }
       
        entidadDao.update(negocioSumAseCob);
        respuesta.setTipoMensaje(mensajeExito);

		} else if (accion.equals("deleted")) {
			entidadService.remove(entidadService.getReference(NegocioCobSumAse.class, negocioSumAseCob.getIdSumaAsegurada()));
			respuesta.setTipoMensaje(mensajeExito);
		}		
		
		return respuesta;
	}
	
	private String mensajeExito ="30";
	private String mensajeError ="10";
	
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}


	@EJB
	public void setCobPaquetesSeccionDao(CobPaquetesSeccionDao cobPaquetesSeccionDao) {
		this.cobPaquetesSeccionDao = cobPaquetesSeccionDao;
	}
	
	@EJB
	public void setCoberturaFacadeRemote(CoberturaFacadeRemote coberturaFacadeRemote) {
		this.coberturaFacadeRemote = coberturaFacadeRemote;
	}

	@EJB
	public void setNegocioCobPaqSeccionDAO(
			NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO) {
		this.negocioCobPaqSeccionDAO = negocioCobPaqSeccionDAO;
	}
	
	@EJB
	public void setValorSeccionCobAutosDao(
			ValorSeccionCobAutosDao valorSeccionCobAutosDao) {
		this.valorSeccionCobAutosDao = valorSeccionCobAutosDao;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}
