package mx.com.afirme.midas2.service.impl.endoso.cotizacion.auto;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.endoso.EndosoId;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.EndosoWSMidasAutosDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.DatosAseguradoView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ParametrosAutoIncisoView;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.poliza.seguroobligatorio.ParametroSeguroObligatorio;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.endoso.EndosoAdicionalDTO;
import mx.com.afirme.midas2.dto.endoso.EndosoTransporteDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.impresiones.ImpresionEndosoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoPolizaService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.cobertura.CoberturaBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.EndosoWSMidasAutosService;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.UtileriasWeb;
import mx.com.afirme.midas2.validators.NumSerieValidador;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.springframework.beans.BeanUtils;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class EndosoWSMidasAutosServiceImpl implements EndosoWSMidasAutosService{
	private static final Logger log = Logger.getLogger(EndosoWSMidasAutosServiceImpl.class);
	protected static final Short TIPO_SOLICITUD = (short) 2;
	private static String datosRiesgoExpresion = "datosRiesgo";
	
	private EndosoWSMidasAutosDao endosoWsMidasAutosDao;	

	@EJB
	private EndosoService endosoService;
	@EJB
	protected EntidadService entidadService;
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;	
	@EJB
	protected EntidadBitemporalService entidadBitemporalService;
	@EJB
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	@EJB
	private ImpresionEndosoService impresionEndosoService; 
    @EJB
    private CalculoService calculoService;
	@EJB
	private MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;
	@EJB(beanName="UsuarioServiceDelegate")
	protected UsuarioService usuarioService;	
	@EJB
	private RenovacionMasivaService renovacionMasivaService;
	@EJB
    private SeguroObligatorioService seguroObligatorioService;
	@EJB
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	@EJB
	private ConfiguracionDatoIncisoBitemporalService configuracionDatoIncisoBitemporalService;
	@EJB
    private ClienteFacadeRemote clienteFacadeRemote;
    @EJB
	private PolizaFacadeRemote polizaFacadeRemote;
    @EJB
    private EndosoPolizaService endosoPolizaService;
    @EJB
	private MarcaVehiculoFacadeRemote marcaVehiculoFacade;
	@EJB
	private NegocioAgrupadorTarifaSeccionDao negAgrupadorTarifaSeccionDao;
	@EJB
	private TarifaAgrupadorTarifaService tarifaAgrupadorService;
	@EJB 
	private EstiloVehiculoFacadeRemote estiloVehiculoFacade;
	@EJB
	private IncisoService incisoService;
	@EJB
	private CoberturaBitemporalService coberturaBitemporalService;
	@EJB
    private CobranzaIncisoService cobranzaIncisoService;
	@EJB
	private EntidadContinuityService entidadContinuityService;
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	private BitemporalCotizacion cotizacion;
	private BitemporalCotizacion bitemporalCotizacion;
	private PolizaDTO polizaDTO;
	private ResumenCostosDTO resumenCostosDTO;
    private Boolean habilitaCobranzaInciso = false;    
	private List<BitemporalInciso> listaIncisosCotizacion = new ArrayList<BitemporalInciso>(1);
    private IncisoCotizacionDTO filtros = new IncisoCotizacionDTO();
	private MensajeDTO mensajeDTO;
    private String accionEndoso;
   
	private BitemporalInciso bitemporalInciso = new BitemporalInciso();
	private BitemporalAutoInciso bitemporalAutoInciso = new BitemporalAutoInciso();
	private List<BitemporalCoberturaSeccion> bitemporalCoberturaSeccionList;	

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Object> cotizar(EndosoTransporteDTO dto, PolizaDTO poliza) throws SystemException{
		Map<String, Object> result = new HashMap<String, Object>();
		
		switch (dto.getClaveTipoEndoso()) {
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO:
			result = this.cotizarEndosoConductoCobro(dto, poliza);
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS:
			result = this.cotizarCambioDatos(dto, poliza);
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO:
			result = this.cotizarEndosoCambioFormaPago(dto, poliza);
			this.getResumenCotizacion(result, dto);
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA:
			result = this.cotizarEndosoCancelacionPoliza(dto, poliza);
			break;
		default:
			throw new SystemException("Este tipo de endoso no está disponible todav\u00EDa.");
		}
		
		return result;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Short cotizar(EndosoAdicionalDTO dto, PolizaDTO poliza) throws SystemException{
		log.trace(">> cotizar()");
		Short endoso  ;
		
		switch (dto.getClaveTipoEndoso()) {
		case SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS:
			endoso = this.cotizarEndosoMovimientos(dto, poliza);
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA:
			endoso = this.cotizarEndosoAjustePrima(dto, poliza);
			break;
		default:
			throw new SystemException("Este tipo de endoso no está disponible todavía.");
		}	
		log.trace("<< cotizar()");
		return endoso;
	}
	
	private Short cotizarEndosoAjustePrima(EndosoAdicionalDTO dto, PolizaDTO poliza) {
		
		log.trace(">> cotizarEndosoAjustePrima()");
		
		Date fechaIniVigenciaEndoso = dto.getFechaInicioVigenciaEndoso();
		String observaciones= dto.getObservacionesInciso();
		BigDecimal primaTotalIgualar =dto.getValorprima();
		BigDecimal polizaId = poliza.getIdToPoliza();
		Short tipoEndoso = dto.getClaveTipoEndoso();
		ResumenCostosDTO resumenCostosEndosoAjustar;
		Short numeroEndoso = dto.getNumeroEndoso();
		ControlEndosoCot controlEndosoCotEAP;
		Integer tipoFormaPago = 1;
		Long cotizacionContinuityId;

		polizaDTO = entidadService.findById(PolizaDTO.class, polizaId);

		controlEndosoCotEAP = new ControlEndosoCot();
		
		controlEndosoCotEAP.setPrimaTotalIgualar(primaTotalIgualar);

		endosoService.guardaCotizacionAjustePrima(polizaDTO.getCotizacionDTO().getIdToCotizacion(), polizaDTO.getIdToPoliza(), controlEndosoCotEAP, numeroEndoso, tipoFormaPago);
		
		cotizacionContinuityId = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class, "numero", polizaDTO.getCotizacionDTO().getIdToCotizacion().longValue()).getId();
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(),TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		resumenCostosEndosoAjustar = calculoService.resumenCostosEndoso(polizaDTO.getIdToPoliza(), controlEndosoCotEAP.getNumeroEndosoAjusta());
		
		log.info("Emitiendo Endoso");

		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacionContinuityId, TimeUtils.getDateTime(fechaIniVigenciaEndoso), SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA);

		log.info("Endoso emitido ");
		
		try {
			endosoWsMidasAutosDao.actualizaObservaciones(controlEndosoCotEAP.getId(), observaciones);
		} catch (Exception e) {
			log.error("Información del error", e);
		}
		log.trace("<< cotizarEndosoAjustePrima()");
		return endoso.getId().getNumeroEndoso();
	}
	

	private Short cotizarEndosoMovimientos(EndosoAdicionalDTO dto, PolizaDTO poliza) throws SystemException{ 
		log.trace(">> cotizarEndosoMovimientos()");
		
		Long paqueteCoberturaAmplia = Long.parseLong(parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PAQUETE_COBERTURA_AMPLIA), true).getValor());

		DateTime dateTime = new DateTime(dto.getFechaInicioVigenciaEndoso());
		Date fechaIniVigenciaEndoso = dto.getFechaInicioVigenciaEndoso();
		String observaciones= dto.getObservacionesInciso();
		BigDecimal primaAIgualar =dto.getValorprima();
		BigDecimal polizaId = poliza.getIdToPoliza();
		Short tipoEndoso = dto.getClaveTipoEndoso();
		
		//EndosoMovimientosAction.prepareMostrar
		polizaDTO = entidadService.findById(PolizaDTO.class,polizaId);
		cotizacion = endosoService.getCotizacionEndosoMovimientos(polizaId, fechaIniVigenciaEndoso, TipoAccionDTO.getNuevoEndosoCot());
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), dateTime);

		//ListadoincisoDinamicoActio.busquedaIncisos
		habilitaCobranzaInciso = cobranzaIncisoService.habilitaCobranzaInciso(poliza.getCotizacionDTO().getIdToCotizacion());
		listaIncisosCotizacion = listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros,tipoEndoso, polizaId.longValue(), fechaIniVigenciaEndoso, true, 3);

		//IncisoBitemporalAction.verDetalleInciso
		bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(), dateTime);
		bitemporalInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalInciso.class,listaIncisosCotizacion.get(0).getContinuity().getId(), bitemporalCotizacion.getContinuity().getId(),dateTime);
		bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, bitemporalInciso.getContinuity().getAutoIncisoContinuity().getId(), bitemporalInciso.getContinuity().getId(),dateTime);
		
		//IncisoBitemporalAction.mostrarCoberturaInciso
		bitemporalAutoInciso.getEntidadContinuity().getParentContinuity().setId(bitemporalInciso.getEntidadContinuity().getId());
		bitemporalCoberturaSeccionList = coberturaBitemporalService.mostrarCoberturas(bitemporalAutoInciso, bitemporalCotizacion.getContinuity().getId(), dateTime);
		
		//Asignar paquete Amplia
		bitemporalAutoInciso.getValue().setNegocioPaqueteId(paqueteCoberturaAmplia);
		
		//IncisoBitemporalAction.mostrarCoberturaInciso
		bitemporalAutoInciso.getEntidadContinuity().getParentContinuity().setId(bitemporalInciso.getEntidadContinuity().getId());
		bitemporalCoberturaSeccionList = coberturaBitemporalService.mostrarCoberturas(bitemporalAutoInciso, bitemporalCotizacion.getContinuity().getId(), dateTime);

		//IncisoBitemporalAction.prepareCalcularInciso
		bitemporalInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalInciso.class, bitemporalInciso.getContinuity().getId(), bitemporalCotizacion.getContinuity().getId(),dateTime);
		bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, bitemporalAutoInciso.getContinuity().getId(), bitemporalInciso.getContinuity().getId(),dateTime);	
		bitemporalAutoInciso.getValue().setNegocioPaqueteId(paqueteCoberturaAmplia);

		//IncisoBitemporalAction.calcularInciso
		bitemporalInciso = incisoService.prepareGuardarIncisoBorrador(bitemporalInciso, bitemporalAutoInciso, null, bitemporalCoberturaSeccionList,dateTime);
		bitemporalInciso = calculoService.calcular(bitemporalInciso, dateTime, dto.getClaveTipoEndoso().intValue(), false);	
		
		//IncisoBitemporalAction.verDetalleInciso
		bitemporalCotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, polizaDTO.getCotizacionDTO().getIdToCotizacion().intValue(), dateTime);
		bitemporalInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalInciso.class,bitemporalInciso.getContinuity().getId(), bitemporalCotizacion.getContinuity().getId(),dateTime);
		bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, bitemporalInciso.getContinuity().getAutoIncisoContinuity().getId(), bitemporalInciso.getContinuity().getId(),dateTime);			

		//IncisoBitemporalAction.mostrarCoberturaInciso
		bitemporalAutoInciso.getEntidadContinuity().getParentContinuity().setId(bitemporalInciso.getEntidadContinuity().getId());
		bitemporalCoberturaSeccionList = coberturaBitemporalService.mostrarCoberturas(bitemporalAutoInciso, bitemporalCotizacion.getContinuity().getId(), dateTime);
		
		//Asignar paquete Amplia
		bitemporalAutoInciso.getValue().setNegocioPaqueteId(paqueteCoberturaAmplia);
				
		//EndosoMovimientosAction.prepareMostrar
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), dateTime);
		
		//EndosoMovimientosAction.mostrar
		mensajeDTO = configuracionDatoIncisoBitemporalService.validarListaParaEmitir(cotizacion,tipoEndoso , fechaIniVigenciaEndoso);

		//EndosoMovimientosAction.prepareCotizacion
		bitemporalCotizacion = endosoService.prepareCotizacionEndoso(bitemporalCotizacion.getContinuity().getId(), dateTime);		
				
		//EndosoMovimientosAction.cotizar
		endosoService.validaPrimaNetaCeroEndosoCotizacion(tipoEndoso, polizaId.longValue(), fechaIniVigenciaEndoso);
		endosoService.guardaCotizacionEndosoMovimientos(cotizacion);
		
		//EndosoMovimientosAction.prepareMostrar
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), dateTime);
		
		//EndosoMovimientosAction.mostrar
		mensajeDTO = configuracionDatoIncisoBitemporalService.validarListaParaEmitir(cotizacion, tipoEndoso, fechaIniVigenciaEndoso);

		//IgualacionPrimeaAction.mostrarIgualcionPriema
		ControlEndosoCot cec = calculoService.obtenerControlEndosoCotizacion(poliza.getCotizacionDTO().getIdToCotizacion().intValue());
		
		//IgualacionPrimeaAction.prepareIgualarPrima
		cotizacion = endosoService.prepareCotizacionEndosoEV(polizaId, TipoAccionDTO.getEditarEndosoCot());

		//IgualacionPrimeaAction.igualarPrima
		calculoService.igualarPrimaEndoso(new Long(cotizacion.getContinuity().getNumero()), primaAIgualar);

		//IgualacionPrimeaAction.mostrarIgualcionPriema
		cec = calculoService.obtenerControlEndosoCotizacion(poliza.getCotizacionDTO().getIdToCotizacion().intValue());

		//EndosoMovimientosAction.prepareMostrar
		resumenCostosDTO = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), dateTime);

		//EndosoMovimientosAction.mostrar
		mensajeDTO = configuracionDatoIncisoBitemporalService.validarListaParaEmitir(cotizacion, tipoEndoso, fechaIniVigenciaEndoso);

		log.info("Emitiendo Endoso");

		//Emitir
		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),dateTime,cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		
		log.info("Endoso emitido ");
		
		try {
			endosoWsMidasAutosDao.actualizaObservaciones(cec.getId(), observaciones);
		} catch (Exception e) {
			log.error("Información del error", e);
		}
		
		log.trace("<< cotizarEndosoMovimientos()");
		return endoso.getId().getNumeroEndoso();
	}
		
	private Map<String, Object> cotizarEndosoConductoCobro(EndosoTransporteDTO dto, PolizaDTO poliza) throws SystemException{
		log.info("--> entrando a cotizar el endoso");
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		BitemporalCotizacion cotizacion = null;
		SolicitudDTO solicitud = null;
		
		if (dto.getClaveTipoEndoso() != SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO) {
			throw new SystemException("Este tipo de endoso no está disponible todavía.");
		}
		
		try {
			EndosoId endosoId = new EndosoId(poliza.getIdToPoliza(), Short.valueOf("0"));
    		EndosoDTO endosoCero = entidadService.findById(EndosoDTO.class, endosoId);
    		DateTime validoEn = TimeUtils.getDateTime(endosoCero.getValidFrom());
			
			cotizacion = entidadBitemporalService.getByBusinessKey
    	 	(CotizacionContinuity.class, "numero", poliza.getCotizacionDTO().getIdToCotizacion(), validoEn);
		} catch(Exception e) {
			log.error("Error al obtener la entidad bitemporal C:\n"+e.getMessage());
			throw new SystemException("Error al obtener la cotización bitemporal");
		} 
		
		try {
			solicitud = prepareSolicitudDTO(cotizacion, poliza.getIdToPoliza(), 
						new BigDecimal(dto.getClaveTipoEndoso()), 
						format.parse(dto.getFechaInicioVigenciaEndoso()), null);
		} catch (ParseException e1) {
			log.error("Error al preparar la solicitud:\n"+e1.getMessage());
			throw new SystemException("Error al preparar la solicitud");
		}
			
		BitemporalCotizacion cotizacionTemp;
		try {
			cotizacionTemp = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), 
					TimeUtils.getDateTime(format.parse(dto.getFechaInicioVigenciaEndoso())));
		} catch (Exception e){
			log.error("Error al preparar la cotización para el endoso:\n"+e.getMessage());
			throw new SystemException("error al preparar la cotizaci\u00F3n para el endoso");
		}
		
		ControlEndosoCot ctrlEndosoCot = null;
		log.info("--> idMedioPagoOld:"+cotizacionTemp.getValue().getMedioPago().getIdMedioPago()+" idMedioPagoNew:"+dto.getDatosEndoso().getConductoCobro().getIdMedioPago().intValue());
		if ( dto.getDatosEndoso().getConductoCobro().getIdMedioPago().intValue() == cotizacionTemp.getValue().getMedioPago().getIdMedioPago() ) {
			throw new SystemException("Debe seleccionar un conducto de cobro diferente al actual");
		} else {
			ctrlEndosoCot = guardarCotizacionEndoso(cotizacionTemp, solicitud.getIdToSolicitud(), dto.getDatosEndoso().getConductoCobro().getIdConductoCobroCliente(), dto.getDatosEndoso().getConductoCobro().getIdMedioPago().intValue());
		}
		
		Map<String, Object> result = new HashMap<String, Object>();
		result.put("idCotizacion", ctrlEndosoCot.getCotizacionId());
		
		return result;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Object> emitir(EndosoTransporteDTO dto) throws SystemException {
		Map<String, Object> result = new HashMap<String, Object>();
		ControlEndosoCot controlEndosoCot = null;
		
		if (dto.getIdCotizacion() == null) {
			throw new SystemException("El idToCotizacion es requerido");
		}
		
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, dto.getIdCotizacion());

		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", cotizacionDTO.getIdToCotizacion().longValue());
		properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);
		
		if (controlEndosoCotList.size() > 0) {
			controlEndosoCot = controlEndosoCotList.iterator().next();
		} else {
			throw new SystemException("No hay cotizaciones de endoso en proceso");
		}
	
		BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME,
				cotizacionDTO.getIdToCotizacion(), TimeUtils.getDateTime(controlEndosoCot.getValidFrom()));
		
		switch (cotizacion.getValue().getSolicitud().getClaveTipoEndoso().intValue()) {
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO:
			result = this.emitirEndoso(dto);
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS:
			result = this.emitirCambioDatos(dto);
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO:
			result = this.emitirCambioFormaPagos(dto);
			break;
		case SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA:
			result = this.emitirEndosoCancelacionPoliza(dto);
			break;
		default:
			break;
		}
		
		BigDecimal idToPoliza = (BigDecimal) result.get("idToPoliza");
		Integer idMedioPago = (Integer) result.get("idMedioPago");
			
		this.programaPagoEndoso(idToPoliza, idMedioPago);
		
		return result;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void programaPagoEndoso(BigDecimal idToPoliza, Integer idMedioPago) throws SystemException{
		try {
			MensajeDTO mensaje = renovacionMasivaService.programaPagoEndoso(idToPoliza);

			if(mensaje != null && mensaje.getMensaje() != null && !mensaje.getMensaje().isEmpty()){
				log.info("--> mensaje:"+mensaje.getMensaje());
			}
		} catch(Exception e) {
			log.error("--> Error al intentar programar el pago: "+e.getMessage());
			throw new SystemException("Error al intentar programar los pagos del endoso");
		}
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private Map<String, Object> emitirEndoso(EndosoTransporteDTO dto) throws SystemException {
		Map<String, Object> result = new HashMap<String, Object>();
		final EndosoDTO endoso;
		
		if (dto.getIdCotizacion() == null) {
			throw new SystemException("El idToCotizacion es requerido");
		}
		
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, dto.getIdCotizacion());
		BitemporalCotizacion cotizacion;
		
		try {
			cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, 
					cotizacionDTO.getIdToCotizacion(), TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia()));
		} catch(Exception e){
			log.info("Error al obtener la cotizaci\u00F3n bitemporal:\n"+e.getMessage());
			throw new SystemException("Error al obtener la cotizaci\u00F3n bitemporal");
		}
		log.info("--> * cotizacion.idMedioPago:"+cotizacion.getValue().getMedioPago().getIdMedioPago()+" " +
				"id:"+cotizacion.getId()+" idSolicitud:"+cotizacion.getValue().getSolicitud().getIdToSolicitud());
		try {
			cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), 
					TimeUtils.getDateTime(cotizacion.getValue().getFechaInicioVigencia()));
		} catch (Exception e){
			log.error("Error al preparar la cotizaci\u00F3n para el endoso:\n"+e.getMessage());
			throw new SystemException("error al preparar la cotizaci\u00F3n para el endoso");
		}
		log.info("--> ** cotizacion.idMedioPago:"+cotizacion.getValue().getMedioPago().getIdMedioPago()+" " +
				"id:"+cotizacion.getId()+" idSolicitud:"+cotizacion.getValue().getSolicitud().getIdToSolicitud());
		try {
			endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),
					TimeUtils.getDateTime(cotizacion.getValue().getSolicitud().getFechaInicioVigenciaEndoso()), 
					cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());

			result.put("numeroEndoso", endoso.getId().getNumeroEndoso().intValue());
			
			ResumenCostosDTO resumenCostos = calculoService.resumenCostosEndoso(endoso.getId().getIdToPoliza(), endoso.getId().getNumeroEndoso());
			
			result.put("primaNeta", resumenCostos.getPrimaNetaCoberturas());
			result.put("descuento", resumenCostos.getDescuento());
			result.put("recargo", resumenCostos.getRecargo());
			result.put("derechos", resumenCostos.getDerechos());
			result.put("iva", resumenCostos.getIva());
			result.put("primaTotal", resumenCostos.getPrimaTotal());
			result.put("idToPoliza", endoso.getId().getIdToPoliza());
			result.put("idMedioPago", cotizacion.getValue().getMedioPago().getIdMedioPago());
		} catch (Exception e) {
			log.error("Error al emitir el endoso:\n"+e.getMessage());
			throw new SystemException("error al emitir la cotizaci\u00F3n para el endoso");
		}
		
		log.info("--> *** cotizacion.idMedioPago:"+cotizacion.getValue().getMedioPago().getIdMedioPago()+" " +
				"id:"+cotizacion.getId()+" idSolicitud:"+cotizacion.getValue().getSolicitud().getIdToSolicitud());
		
		try {
			updateBitEndosoCMP(cotizacion.getValue().getSolicitud().getIdToSolicitud(), cotizacion.getValue().getMedioPago().getIdMedioPago());
		} catch (Exception e) {
			log.error("Error al actualizar el medioPago en el endoso:\n"+e.getMessage());
		}
		
		try {
			addPolTarjetaHabiente(endoso.getId().getIdToPoliza(), cotizacion.getValue().getMedioPago().getIdMedioPago(), cotizacion.getValue().getIdConductoCobroCliente());
		} catch (Exception e) {
			log.error("Error al agregar en pol_tarjetaHabiente:\n"+e);
		}
		
		return result;
	}
	
	private ControlEndosoCot guardarCotizacionEndoso(BitemporalCotizacion cotizacion, BigDecimal idToSolicitud, Long idConductoCobroCliente, int idMedioPagoTemp) throws SystemException {
		log.info("--> entrando a guardarCotizacionEndoso");
		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, idToSolicitud);
		
		PolizaDTO poliza = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", new BigDecimal(cotizacion.getContinuity().getNumero())).get(0);
		
		ControlEndosoCot ctrlEndosoCot = null;
		
		if (cotizacion.getContinuity().getCotizaciones().hasRecordsInProcess()) {
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
			throw new SystemException("Existen cotizaciones en proceso");
		} else {
			//Guardo registro historico en tabla control endosos
			ctrlEndosoCot = saveControlEndosoCot(solicitudDTO, cotizacion, Long.valueOf(CotizacionDTO.ESTATUS_COT_EN_PROCESO), solicitudDTO.getFechaInicioVigenciaEndoso());
		}
		
		generaBitEndosoCMP(poliza.getIdToPoliza(), solicitudDTO.getIdToSolicitud(), idMedioPagoTemp, idConductoCobroCliente);
		
		movimientoEndosoBitemporalService.calcular(ctrlEndosoCot);
		
		return ctrlEndosoCot;
	}
	
	protected ControlEndosoCot saveControlEndosoCot(SolicitudDTO solicitudDTO, BitemporalCotizacion cotizacion, 
			Long claveEstatus, Date fechaInicioVigencia) {
		return saveControlEndosoCot(solicitudDTO, cotizacion, claveEstatus, fechaInicioVigencia, null);
	}
	
	protected ControlEndosoCot saveControlEndosoCot(SolicitudDTO solicitudDTO, BitemporalCotizacion cotizacion, 
			Long claveEstatus, Date fechaInicioVigencia, Long idCancela) {
		log.info("--> Entrando a saveControlEndosoCot");
		ControlEndosoCot cecSaved = null;
		
		//Valida que no exista un controlendoso en proceso
    	Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", cotizacion.getContinuity().getNumero().longValue());
		properties.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByPropertiesWithOrder(ControlEndosoCot.class, properties, "id DESC");
		
		if(controlEndosoCotList == null || controlEndosoCotList.isEmpty()){
	    	ControlEndosoCot cecToSave = new ControlEndosoCot();
			cecToSave.setCotizacionId(cotizacion.getContinuity().getNumero().longValue());
			cecToSave.setClaveEstatusCot(claveEstatus);
			cecToSave.setSolicitud(solicitudDTO);
			cecToSave.setValidFrom(fechaInicioVigencia);
			cecToSave.setIdCancela(idCancela);
			
			cecSaved = entidadService.save(cecToSave);
		}else{
			cecSaved = controlEndosoCotList.get(0);
		}
		
		log.info("Saliendo de saveControlEndosoCot");
		return   cecSaved;
	}

	@Override
	public byte[] imprimirEndoso(EndosoTransporteDTO dto, PolizaDTO poliza) throws SystemException {
		ControlEndosoCot ctrlEndosoCot = getControlFromEndoso(poliza.getCotizacionDTO().getIdToCotizacion().longValue(), 
				dto.getNumeroEndoso());
		
		ImpresionEndosoDTO impresionEndosoDTO;
		try {
			impresionEndosoDTO = impresionEndosoService.imprimirEndosoM2(
					poliza.getCotizacionDTO().getIdToCotizacion(), ctrlEndosoCot.getRecordFrom(), 
					ctrlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue());
			return impresionEndosoDTO.getImpresionEndoso();
		} catch (Exception e) {
			log.error("Error al imprimir el endoso:\n"+e.getMessage());
			throw new SystemException("Error al imprimir el endoso");
		} 
	}
	
	private ControlEndosoCot getControlFromEndoso(Long cotizacionId, Short numeroEndoso) {
        
        ControlEndosoCot controlEndoso = null;
        Map<String, Object> filtros = new HashMap<String, Object>();
        
        filtros.put("id.numeroEndoso", numeroEndoso);
        filtros.put("idToCotizacion", cotizacionId);
        List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, filtros);
        
        if (endosos != null && endosos.size() > 0) {
              filtros = new HashMap<String, Object>();
              filtros.put("cotizacionId", cotizacionId);
              filtros.put("recordFrom", endosos.get(0).getRecordFrom());
              
              List<ControlEndosoCot> controles = entidadService.findByProperties(ControlEndosoCot.class, filtros);
              
              if (controles != null && controles.size() > 0) {
                    controlEndoso = controles.get(0);
              }
        }        
        return controlEndoso;        
	}
	
	private MensajeDTO generaBitEndosoCMP(BigDecimal idToPoliza, BigDecimal idToSolicitud, 
			Integer idMedioPago, Long idConductoCobroCliente) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Gen_Cot_Endoso_CMP";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pIdSolicitud", idToSolicitud);
			storedHelper.estableceParametro("pIdMedioPago", idMedioPago);
			
			if (idConductoCobroCliente != null) {
				storedHelper.estableceParametro("pIdClienteConductoCobro", idConductoCobroCliente.intValue());
			} else {
				storedHelper.estableceParametro("pIdClienteConductoCobro", null);
			}
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	private SolicitudDTO prepareSolicitudDTO(BitemporalCotizacion cotizacion, BigDecimal idToPoliza, 
			BigDecimal tipoEndoso, Date validoEn, Short motivoEndoso) throws SystemException {
		SolicitudDTO nuevaSolicitud = new SolicitudDTO();
		BeanUtils.copyProperties(cotizacion.getValue().getSolicitud(), nuevaSolicitud);
		
		nuevaSolicitud.setIdToSolicitud(null);
		nuevaSolicitud.setComentarioList(null);
		nuevaSolicitud.setClaveTipoSolicitud(TIPO_SOLICITUD);
		nuevaSolicitud.setFechaInicioVigenciaEndoso(validoEn);
		nuevaSolicitud.setIdToPolizaEndosada(idToPoliza);
		nuevaSolicitud.setClaveTipoEndoso(tipoEndoso);
		nuevaSolicitud.setClaveMotivoEndoso(motivoEndoso);
		
		try {
			Usuario usuario = usuarioService.getUsuarioActual();
			nuevaSolicitud.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
			nuevaSolicitud.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
			nuevaSolicitud.setFechaCreacion(new Date());
			nuevaSolicitud.setFechaModificacion(new Date());
		} catch(Exception e) {

		}
		
		entidadService.save(nuevaSolicitud);
		
		return nuevaSolicitud;
	}
	
	private MensajeDTO updateBitEndosoCMP(BigDecimal idToSolicitud, Integer idMedioPago) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_Upd_MedioPago_Bitemporal";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdSolicitud", idToSolicitud);
			storedHelper.estableceParametro("pIdMedioPago", idMedioPago);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	@SuppressWarnings("unused")
	private Map<String, Object> cotizarCambioDatos(EndosoTransporteDTO dto, PolizaDTO poliza) throws SystemException{
		log.info("--> entrando al endoso cotizarCambioDatos WS");
		BitemporalCotizacion cotizacion;
		Map<String, Object> result = new HashMap<String, Object>();		
		Date fechaIniVigenciaEndoso = this.traerFechaInicioVigenciaEndoso(dto);
		BitemporalInciso biInciso;
		BitemporalAutoInciso biAutoInciso;		
		
		Usuario usuario = null;	
		Integer usuarioExterno = 2;
		
		if(dto.getToken() != null) {
			usuario = validarToken(dto.getToken());
		}

		if(usuario!=null && usuario.getTipoUsuario()!=null && usuario.getTipoUsuario()>0) {
			usuarioExterno = usuario.getTipoUsuario();
		}
		
		if(usuarioExterno != 1 
				&& dto != null 
				&& dto.getDatosEndoso()!= null 
				&& dto.getDatosEndoso().getCambioDatos()!= null 
				&& dto.getDatosEndoso().getCambioDatos().getDatosComplementariosVehiculo() != null
				&& dto.getDatosEndoso().getCambioDatos().getDatosComplementariosVehiculo().getObservacionesInciso() != null
				&& StringUtils.isNotEmpty(dto.getDatosEndoso().getCambioDatos().getDatosComplementariosVehiculo().getObservacionesInciso())
				) {
			throw new SystemException("Los datos de observaciones no aplica para usuarios externos");
		}
		
		//mostrar
		try {
			cotizacion = endosoService.getCotizacionEndosoCambioDatos(poliza.getIdToPoliza(),
				fechaIniVigenciaEndoso, TipoAccionDTO.getNuevoEndosoCot());
		} catch (NegocioEJBExeption ne) {
			throw new SystemException(((NegocioEJBExeption)ne).getMessageClean());
		}
		
		//datosRiesgo.....................
		EndosoDTO ultimoEndoso = this.getUltimoEndoso(poliza.getIdToPoliza());
		
		//guardar nuevo contratante
		if (dto.getDatosEndoso().getCambioDatos().getIdContratante() != null) {
			ClienteGenericoDTO clienteGenerico = endosoService.getClienteGenerico(dto.getDatosEndoso().getCambioDatos().getIdContratante());
			if (!endosoService.actualizarDatosContratanteCotizacion(cotizacion, clienteGenerico, null, fechaIniVigenciaEndoso)) {
				throw new SystemException("No es posible continuar con la emisi\u00F3n del endoso ya que  el domicilio del contratante se encuentra incompleto \u00F3 cambia el IVA ya registrado");
			}
		}
		
		//guardar datos complementarios del vehículo
		try{			
			biInciso = this.traerInciso(poliza.getIdToPoliza(),fechaIniVigenciaEndoso);
			biAutoInciso = biInciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(TimeUtils.getDateTime(fechaIniVigenciaEndoso));
		}catch(Exception e){
			log.error("Error al traer bitemporales en cotizarCambioDatos WS\n",e);
			throw new SystemException("Error al traer bitemporales");			
		}
		
		//carga valores	
		biInciso = this.cargarValInciso(biInciso, dto.getDatosEndoso().getCambioDatos().getDatosComplementariosVehiculo());
		biAutoInciso = this.cargarValoresAutoInciso(biAutoInciso, dto.getDatosEndoso().getCambioDatos().getDatosComplementariosVehiculo());

		//carga datos asegurado
		try {
			biAutoInciso = this.cargarDatosAsegurado(biAutoInciso,dto.getDatosEndoso().getCambioDatos().getDatosAsegurado(),
					cotizacion.getValue().getPersonaContratanteId());
		} catch (SystemException e) {
			throw new SystemException(e.getMessage());
		} catch (Exception e) {
			log.error("Error al cargar datos del asegurado en cotizarCambioDatos WS\n",e);
			throw new SystemException("Error al cargar datos del asegurado");
		}
		
		//guarda datos adicionales paquete
		Map<String, String>  datosRiesgo = new HashMap<String, String>(1);
		if (this.validarDatosRiesgo(datosRiesgo, biInciso.getId(), fechaIniVigenciaEndoso, cotizacion.getId())){
			configuracionDatoIncisoBitemporalService.guardarDatosAdicionalesPaquete(null, //TODO
					biInciso.getId(), fechaIniVigenciaEndoso);			
		}
		
		//guarda bitemporales cotizacion
		try{
			List<String> errors = NumSerieValidador.getInstance().validate(biAutoInciso.getValue().getNumeroSerie());
			entidadBitemporalService.saveInProcess(biInciso, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			entidadBitemporalService.saveInProcess(biAutoInciso, TimeUtils.getDateTime(fechaIniVigenciaEndoso));
			
		}catch(Exception e){
			log.error("Error al guardar bitemporales en cotizarCambioDatos WS\n",e);
			throw new SystemException("Error al guardar bitemporales");
		}
		
		//prepare cotizar
		try {
			cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(),
					TimeUtils.getDateTime(fechaIniVigenciaEndoso));

			//actualizar dt contratante
			//cotizacion = this.actualizarDatosContratante(cotizacion, nuevoContratanteId);
		} catch (Exception e) {
			log.error("Error en prepareCotizacionEndoso\n",e);
			throw new SystemException("Error en prepareCotizacionEndoso");
		}
		
		//cotizar
		try {
			endosoService.guardaCotizacionEndosoCambioDatos(cotizacion);
		} catch (Exception e) {
			log.error("Error al guardar la cotizacion\n",e);
			throw new SystemException("Error al guardar la cotizaci\u00F3n");
		}
		
		result.put("idCotizacion", cotizacion.getContinuity().getNumero());
		result.put("numeroSolicitud", cotizacion.getValue().getSolicitud().getNumeroSolicitud());
		result.put("numeroEndoso", cotizacion.getValue().getNumeroEndoso());
		log.info("--> saliendo del endoso cotizarCambioDatos WS");
		return result;
	}
	
	private BitemporalCotizacion traerCotizacion(PolizaDTO poliza,Date fechaIniVigenciaEndoso,
			String accionEndoso) throws SystemException{
		BitemporalCotizacion cotizacion;
		@SuppressWarnings("unused")
		ResumenCostosDTO resumenCostosDTO;
		try {
			if (accionEndoso.equals(TipoAccionDTO.getAgregarModificar())){
	    		EndosoDTO endoso = this.getUltimoEndoso(poliza.getIdToPoliza());
	    		DateTime validoEn = TimeUtils.getDateTime(endoso.getValidFrom());
				
				cotizacion = entidadBitemporalService.getByBusinessKey
	    	 	(CotizacionContinuity.class, "numero", poliza.getCotizacionDTO().getIdToCotizacion(), validoEn);				
			}
			else{
			cotizacion = endosoService.getCotizacionEndosoCambioDatos(poliza.getIdToPoliza(),
						fechaIniVigenciaEndoso, accionEndoso);
		}

		} catch(Exception e) {
			log.error("Error al obtener la entidad bitemporal C:\n",e);
			throw new SystemException("Error al obtener la cotizaci\u00F3n bitemporal");
		}
		return cotizacion;
	}
		
	//@Override
	private Map<String, Object> emitirCambioDatos(EndosoTransporteDTO dto) 
	throws SystemException{
		PolizaDTO poliza = null;
		
		log.info("--> entrando al endoso emitirCambioDatos WS");
		BitemporalCotizacion cotizacion;
		Map<String, Object> result = new HashMap<String, Object>();		
		//Date fechaIniVigenciaEndoso = this.traerFechaInicioVigenciaEndoso(dto);
		List<ExcepcionSuscripcionReporteDTO> excepcionesList;
		
		if (dto.getIdCotizacion() == null) {
			throw new SystemException("El idToCotizacion es requerido");
		}
		
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, dto.getIdCotizacion());
		
		List<PolizaDTO> listPoliza = polizaFacadeRemote.findByProperty("cotizacionDTO.idToCotizacion", dto.getIdCotizacion());
		
		if (listPoliza.size() > 0) {
			poliza = listPoliza.get(0);
		}
		
		//prepare
		try {
			cotizacion = entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, 
					cotizacionDTO.getIdToCotizacion(), TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia())); 
		} catch (Exception e) {
			log.error("Error en prepareEmitirCambioDatos\n",e);
			throw new SystemException("Error en prepareEmitirCambioDatos");
		}
		
		//validar
		TerminarCotizacionDTO terminarCotizacionDTO = endosoService.validarEmisionCotizacion(cotizacion, TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia()));
		if (terminarCotizacionDTO != null) {
			excepcionesList = 	terminarCotizacionDTO.getExcepcionesList();	
			log.info("Se obtuvieron excepciones al emitir. emitirCambioDatos WS\n"+terminarCotizacionDTO.getMensajeError());
			throw new SystemException(terminarCotizacionDTO.getMensajeError());
		}
		
		//emitir
		EndosoDTO endoso;
		try {
			endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),
					TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia()),
					cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		} catch (Exception e) {
			log.error("Error al emitir endoso cambio de datos WS\n",e);
			throw new SystemException("Error al emitir endoso cambio de datos WS");
		}
		
		if (endoso != null) {			
			String endosoNum = String.format("%06d", endoso.getId().getNumeroEndoso().intValue());
			result.put("numero endoso", endosoNum);
			
			if(seguroObligatorioService.estaHabilitado(ParametroSeguroObligatorio.PARAMETRO_CAMBIO_DATOS)){
				this.creaPolizaAnexaEndosoCambioDatos(poliza, endoso.getId().getNumeroEndoso().shortValue());
			}
		}
		else{
			result.put("numero endoso", "No se emitio el endoso");
		}
		log.info("--> saliendo del endoso emitirCambioDatos WS");
		return result;
	}
	
	private Date traerFechaInicioVigenciaEndoso(EndosoTransporteDTO dto) throws SystemException{
		Date fechaIniVigenciaEndoso;
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		try {
			fechaIniVigenciaEndoso = format.parse(dto.getFechaInicioVigenciaEndoso());
		} catch (ParseException e) {
			log.error("Error al obtener la fecha de inicio de vigencia\n"+e.getMessage());
			throw new SystemException("Error al obtener la fecha de inicio de vigencia");
		}
		return fechaIniVigenciaEndoso;
	}
	
	private void creaPolizaAnexaEndosoCambioDatos(PolizaDTO poliza, Short numeroEndoso){
		try{
			//Si es poliza anexa no entra en el proceso
			if(poliza.getClaveAnexa() == null || !poliza.getClaveAnexa().equals(PolizaDTO.CLAVE_POLIZA_ANEXA)){
				//Valida exista poliza Anexa
				PolizaDTO polizaAnexa = seguroObligatorioService.obtienePolizaAnexa(poliza.getIdToPoliza());
				if(polizaAnexa != null){
					try{							
						seguroObligatorioService.emiteEndosoDatosSO(poliza.getIdToPoliza(), numeroEndoso);	
					}catch(Exception e){
						log.error("Error en emiteEndosoDatosSO\n"+e.getMessage());
					}
				}
			}
		}catch(Exception e){
			log.error("Error en emiteEndosoDatosSO\n"+e.getMessage());
		}				
	}
	
	private BitemporalInciso traerInciso(BigDecimal idToPoliza,Date fechaIniVigenciaEndoso){	
		IncisoCotizacionDTO filtros = new IncisoCotizacionDTO();
		List<BitemporalInciso> list = listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros,
				SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS,idToPoliza.longValue(),fechaIniVigenciaEndoso,true,2);
		return list.get(0);
	}
	
	private List<ControlDinamicoRiesgoDTO> traerDatosRiesgo(Long incisoId,Date fechaIniVigenciaEndoso,Long cotizacionId)
	throws SystemException{
		
		List<ControlDinamicoRiesgoDTO> riesgosList;
		try {
			Map<String, String> valores = new LinkedHashMap<String, String>();
			riesgosList = configuracionDatoIncisoBitemporalService.getDatosRiesgo(incisoId,
					fechaIniVigenciaEndoso,valores,cotizacionId,TipoAccionDTO.getVer(),true);
		} catch (Exception e) {
			log.error("Error al traerDatosRiesgo\n",e);
			throw new SystemException("Error al traerDatosRiesgo");
		}		
		return riesgosList;
	}	
	
	private BitemporalAutoInciso cargarValoresAutoInciso(BitemporalAutoInciso biAutoInciso,
			ParametrosAutoIncisoView parametrosAutoIncisoView) throws SystemException{
		
		if(biAutoInciso != null && biAutoInciso.getValue() != null && parametrosAutoIncisoView != null){
			
			SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
			
			if (parametrosAutoIncisoView.getConductor() != null) {
				if (parametrosAutoIncisoView.getConductor().getNombreConductor() != null) {
					String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getConductor().getNombreConductor());
					biAutoInciso.getValue().setNombreConductor(valor);
				}
				
				if (parametrosAutoIncisoView.getConductor().getApellidoMaternoConductor() != null) {
					String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getConductor().getApellidoMaternoConductor());
					biAutoInciso.getValue().setMaternoConductor(valor);
				}
				
				if (parametrosAutoIncisoView.getConductor().getApellidoPaternoConductor() != null) {
					String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getConductor().getApellidoPaternoConductor());
					biAutoInciso.getValue().setPaternoConductor(valor);
				}
				
				if (parametrosAutoIncisoView.getConductor().getOcupacion() != null) {
					String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getConductor().getOcupacion());
					biAutoInciso.getValue().setOcupacionConductor(valor);
				}
				
				if (parametrosAutoIncisoView.getConductor().getNumeroLicencia() != null) {
					String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getConductor().getNumeroLicencia());
					biAutoInciso.getValue().setNumeroLicencia(valor);
				}
				
				if (parametrosAutoIncisoView.getConductor().getFechaNacimiento() != null) {
					String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getConductor().getFechaNacimiento());
					biAutoInciso.getValue().setFechaNacConductor(DateUtils.convertirStringToUtilDate(valor, "yyyy-MM-dd HH:mm:ss"));
				}
			}
			
			if (parametrosAutoIncisoView.getNumeroMotor() != null) {
				String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getNumeroMotor());
				biAutoInciso.getValue().setNumeroMotor(valor);
			}
			
			if (parametrosAutoIncisoView.getNumeroSerie() != null) {
				String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getNumeroSerie());
				biAutoInciso.getValue().setNumeroSerie(valor);
			}
			
			if(parametrosAutoIncisoView.getPlaca() != null) {
				String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getPlaca());
				biAutoInciso.getValue().setPlaca(valor);
			}
			
			if (parametrosAutoIncisoView.getEstiloId() != null) {
				this.getEstiloVehiculoId(biAutoInciso, parametrosAutoIncisoView.getEstiloId());
			}
			
			if (parametrosAutoIncisoView.getRepuve() != null) {
				String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getRepuve());
				biAutoInciso.getValue().setRepuve(valor);
			}
			
			if (parametrosAutoIncisoView.getRutaCirculacion() != null) {
				String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getRutaCirculacion());
				biAutoInciso.getValue().setRutaCirculacion(valor);
			}
			
			if (parametrosAutoIncisoView.getObservacionesInciso() != null) {
				String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getObservacionesInciso());
				biAutoInciso.getValue().setObservacionesinciso(valor);
			}
		}
		return biAutoInciso;
	}
		
	private BitemporalInciso cargarValInciso(BitemporalInciso inciso,
			ParametrosAutoIncisoView parametrosAutoIncisoView){
		
		if (parametrosAutoIncisoView!=null && parametrosAutoIncisoView.getEmailContacto()!=null){
			String valor = UtileriasWeb.parseEncodingISO(parametrosAutoIncisoView.getEmailContacto());
			inciso.getValue().setEmailContacto(valor);
		}
		return inciso;
	}
	
	private BitemporalAutoInciso cargarDatosAsegurado(BitemporalAutoInciso biAutoInciso,
			DatosAseguradoView asegurado, Long contratanteId) throws SystemException{
		
		ClienteDTO cliente = null;
		if (asegurado!=null){			
			if(biAutoInciso != null && biAutoInciso.getValue() != null){
				
				if (asegurado.getOpcionAsegurado() == 1 && contratanteId == 0) {
					throw new SystemException("No existe un Contratante");
				}
			}			
			switch (asegurado.getOpcionAsegurado()) {
			case 1:
				if (contratanteId != 0) {
					try {
						cliente = clienteFacadeRemote.findById(BigDecimal.valueOf(contratanteId),
																							"nombreUsuario");
					} catch (Exception e) {
						log.error("Error al buscar contratante. Endoso cambio de datos WS\n",e);
						throw new SystemException("Error al buscar contratante.");
					}
				}
				
				if (cliente != null) {
					biAutoInciso.getValue().setPersonaAseguradoId(cliente.getIdCliente().longValue());
					biAutoInciso.getValue().setNombreAsegurado(cliente.obtenerNombreCliente());			
				}
				break;				
			case 2:
				biAutoInciso.getValue().setPersonaAseguradoId(null);
				
				if(asegurado.getNombreAsegurado() != null){
					String nombre = asegurado.getNombreAsegurado().toUpperCase();
					biAutoInciso.getValue().setNombreAsegurado(nombre);
				}			
				break;				
			case 3:
				if (asegurado.getClienteAseguradoId() != 0) {
					try {
						cliente = clienteFacadeRemote.findById(new BigDecimal(asegurado.getClienteAseguradoId()), "nombreUsuario");
					} catch (Exception e) {
						log.error("Error al buscar cliente. Endoso cambio de datos WS\n",e);
						throw new SystemException("Error al buscar cliente.");
					}				
				}			
				
				if (cliente != null) {
					biAutoInciso.getValue().setPersonaAseguradoId(cliente.getIdCliente().longValue());
					biAutoInciso.getValue().setNombreAsegurado(cliente.obtenerNombreCliente());
				}			
				break;
			}
		}		
		
		return biAutoInciso;		
	}
		
	private boolean validarDatosRiesgo(Map<String, String>  datosRiesgo,Long incisoId,Date fechaIniVigenciaEndoso,Long cotizacionId)
	throws SystemException {
		
		@SuppressWarnings("unused")
		List<ControlDinamicoRiesgoDTO> controles;
		controles = this.traerDatosRiesgo(incisoId,fechaIniVigenciaEndoso,cotizacionId);//TODO modificar para que tome en cuenta la lista obtenida

		List<String> processedElements = new ArrayList<String>();
		List<ControlDinamicoRiesgoDTO> controlDinamicos = new ArrayList<ControlDinamicoRiesgoDTO>();
		boolean result = true;

		for(Entry<String, String> entry : datosRiesgo.entrySet()) {
			String key = entry.getKey();
			String value = entry.getValue();
			ConfiguracionDatoIncisoId id = configuracionDatoIncisoBitemporalService.getConfiguracionDatoIncisoId(key);			
			ControlDinamicoRiesgoDTO controlDinamicoRiesgoDTO = configuracionDatoIncisoBitemporalService.findByConfiguracionDatoIncisoId(id);			
			if (controlDinamicoRiesgoDTO != null) { 
				controlDinamicoRiesgoDTO.setValor(value);
				controlDinamicos.add(controlDinamicoRiesgoDTO);
			}
		}		
		for (ControlDinamicoRiesgoDTO control: controlDinamicos) {
			String id = control.getId();
			if (!processedElements.contains(id)) {
				result = validarPorTipoValidador(control);
					processedElements.add(id);
			}
		}
		return result;
	}
	
	private MensajeDTO addPolTarjetaHabiente(BigDecimal idToPoliza, Integer idMedioPago, Long idCondCobroCliente) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_ENDOSOS.spAut_addPolTarjetaHabiente";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pIdMedioPago", idMedioPago);
			storedHelper.estableceParametro("pIdCondCobroCliente", idCondCobroCliente.intValue());
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}	
	private boolean validarPorTipoValidador(ControlDinamicoRiesgoDTO controlDinamicoDTO) {
		String id = controlDinamicoDTO.getId();
		String valor = controlDinamicoDTO.getValor();
		Integer tipoValidador = controlDinamicoDTO.getTipoValidador();
		boolean result = true;
		
		if (StringUtils.isNotBlank(valor)) {
			if (tipoValidador != null) {
				switch (tipoValidador){
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO:
					result = validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 4);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_DOS:
					result = validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 2);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS: 
					result = validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 16, 2);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO:
					result = validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 16, 4);
					break;
				case ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO:
					result = validarDecimal(UtileriasWeb.getOGNLCollectionExpression(datosRiesgoExpresion, id), 
							valor, 8, 0);
					break;
				}			
			}
		}
		else{
			result = false;
		}
		return result; //TODO
	}

	private boolean validarDecimal(String fieldName, String numero, int maxEnteros, int maxDecimales) {
		boolean result = true;
		if (!UtileriasWeb.isDecimalValido(numero, maxEnteros, maxDecimales)) {
			result = false; //TODO 
		}
		return result;
	}
	
	public EndosoDTO getUltimoEndoso (BigDecimal idToPoliza){		
		EndosoDTO ultimoEndosoDTO = new EndosoDTO();
		List<EndosoDTO> list = endosoService.findByPropertyWithDescriptions("id.idToPoliza", idToPoliza, Boolean.TRUE);
		if (list!=null && !list.isEmpty()){
			ultimoEndosoDTO = list.get(list.size() - 1);
		}		
		return ultimoEndosoDTO;
	}

	@Override
	public List<ControlDinamicoRiesgoDTO> traerDatosRiesgo(PolizaDTO poliza) throws SystemException{
		
		EndosoDTO ultimoEndosoDTO = this.getUltimoEndoso(poliza.getIdToPoliza());
		BitemporalInciso bInciso = this.traerInciso(poliza.getIdToPoliza(),ultimoEndosoDTO.getValidFrom());		
		Map<String,String> valores = new LinkedHashMap<String, String>();
		BitemporalCotizacion cotizacion;
		
		try {
			cotizacion = this.traerCotizacion(poliza,ultimoEndosoDTO.getValidFrom(),TipoAccionDTO.getAgregarModificar());
		} catch (SystemException e) {
			log.error("Error en traerDatosRiesgo\n",e);
			throw new SystemException("Error en traerDatosRiesgo. Al obtener el bitemporal");
		}		
		
		List<ControlDinamicoRiesgoDTO> controles;
		try {
			controles = configuracionDatoIncisoBitemporalService.getDatosRiesgo(bInciso.getId(),
					ultimoEndosoDTO.getValidFrom(), ultimoEndosoDTO.getRecordFrom(),
					valores, cotizacion.getId(), null, SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS, false);
		} catch (Exception e) {
			log.error("Error en traerDatosRiesgo\n",e);
			throw new SystemException("Error en traerDatosRiesgo.");
		}		
		return controles;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private Map<String, Object> cotizarEndosoCambioFormaPago(EndosoTransporteDTO dto, PolizaDTO poliza) throws SystemException{
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		Map<String, Object> result = new HashMap<String, Object>();
		
		//se crea Solicitud
		BitemporalCotizacion cotizacion;
		try {
			cotizacion = endosoService.getInitCotizacionEndoso(poliza.getIdToPoliza(), format.parse(dto.getFechaInicioVigenciaEndoso()), 
					 SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO, SolicitudDTO.CVE_MOTIVO_ENDOSO_PETICION_ASEGURADO, TipoAccionDTO.getNuevoEndosoCot());
		} catch (ParseException e1) {
			throw new SystemException("Error en el formato de la fecha");
		} catch (NegocioEJBExeption ne) {
			throw new SystemException(((NegocioEJBExeption)ne).getMessageClean());
		} catch (Exception e) {
			throw new SystemException("Error al crear la solicitud para la cotizaci\u00F3n del endoso:"+ e.getMessage());
		}
		
		if(cotizacion.getValue().getFormaPago().getId().equals(dto.getDatosEndoso().getCambioFormaPago().getIdFormaPago())){
			throw new SystemException("Debe seleccionar una forma de pago diferente a la actual");
		}else{
			FormaPagoDTO formaPagoDTO = entidadService.findById(FormaPagoDTO.class, 
					dto.getDatosEndoso().getCambioFormaPago().getIdFormaPago());
			
			if(poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getAplicaPctPagoFraccionado()){
				cotizacion.getValue().setPorcentajePagoFraccionado(movimientoEndosoBitemporalService.getPctFormaPago(formaPagoDTO.getIdFormaPago(), 
					cotizacion.getValue().getMoneda().getIdTcMoneda()));
			} else {
				cotizacion.getValue().setPorcentajePagoFraccionado(dto.getDatosEndoso().getCambioFormaPago().getPorcentajePagoFraccionado());
			}
			
			cotizacion.getValue().setFormaPago(formaPagoDTO);
			
			try {
				endosoService.guardaCotizacionEndoso(cotizacion);
			} catch (NegocioEJBExeption e) {
				throw new SystemException(((NegocioEJBExeption)e).getMessageClean());
			}
		}
		
		result.put("idCotizacion", cotizacion.getContinuity().getNumero());
		
		return result;
	}
	
	
	private Map<String, Object> emitirCambioFormaPagos(EndosoTransporteDTO dto) throws SystemException{
		Map<String, Object> result = new HashMap<String, Object>();		
		if (dto.getIdCotizacion() == null) {
			throw new SystemException("El idToCotizacion es requerido");
		}
		
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, dto.getIdCotizacion());
		
		BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
				CotizacionContinuity.BUSINESS_KEY_NAME, cotizacionDTO.getIdToCotizacion(), TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia()));

		EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),TimeUtils.getDateTime(cotizacion.getValue().getFechaInicioVigencia()),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
		
		if(endoso!=null){
			result = this.llenarMap(endoso);
		}
		
		return result;
	}
	
	private Map<String, Object> cotizarEndosoCancelacionPoliza(EndosoTransporteDTO dto, PolizaDTO poliza) throws SystemException {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    	BigDecimal idToPoliza = null;
		Map<String, Object> result = new HashMap<String, Object>();
    	
    	try {    		
        	log.info("Cancelando Poliza " + poliza.getIdToPoliza());
        	idToPoliza = poliza.getIdToPoliza();
	        BitemporalCotizacion bitCotizacion = null;
	        BigDecimal idToSolicitud = null;
	        Date fechaIniVigenciaEndoso = format.parse(dto.getFechaInicioVigenciaEndoso());
	        
            bitCotizacion = endosoService.getInitCotizacionEndoso(idToPoliza, fechaIniVigenciaEndoso, 
					 SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA, (short)dto.getMotivoEndoso(), TipoAccionDTO.getNuevoEndosoCot());
            
            if(bitCotizacion!= null){
                idToSolicitud = bitCotizacion.getValue().getSolicitud().getIdToSolicitud();
            }

            //PrepareCotizacion
            bitCotizacion = endosoPolizaService.prepareBitemporalEndoso(bitCotizacion.getContinuity().getId(), 
            		TimeUtils.getDateTime(fechaIniVigenciaEndoso));

            //Crea Cotizacion
            endosoPolizaService.guardaCotizacionEndosoCancelacionPoliza(bitCotizacion,idToSolicitud,
                    fechaIniVigenciaEndoso);
            
            result.put("idCotizacion", bitCotizacion.getContinuity().getNumero());

            log.info("La poliza idToPoliza => " + idToPoliza + " fue cancelada correctamente");
        
            ResumenCostosDTO resumenCostosDTO = calculoService.resumenCotizacionEndoso(bitCotizacion.getEntidadContinuity().getBusinessKey(), 
    				TimeUtils.getDateTime(fechaIniVigenciaEndoso));
            
            result.put("primatotal", resumenCostosDTO.getPrimaTotal());
            result.put("Descuentos", resumenCostosDTO.getDescuento());
            result.put("primaNeta", resumenCostosDTO.getPrimaNetaCoberturas());
            result.put("recargo", resumenCostosDTO.getRecargo());
            result.put("derechoPago", resumenCostosDTO.getDerechos());
            result.put("iva", resumenCostosDTO.getIva());
            
        } catch(NegocioEJBExeption ne){
        	log.error("No fue posible cancelar la poliza " + idToPoliza, ne);
        	throw new SystemException("Ocurri\u00F3 un error al cancelar la p\u00F3liza, " + ne.getMessage());
        } catch (Exception e) {
        	log.error("No fue posible cancelar la poliza " + idToPoliza, e);
        	throw new SystemException("Ocurri\u00F3 un error al cancelar la p\u00F3liza " + e.getMessage());
		}
    	
        return result;
	}
	
	private Map<String, Object> emitirEndosoCancelacionPoliza(EndosoTransporteDTO dto) throws SystemException {
		Map<String, Object> result = new HashMap<String, Object>();
		EndosoDTO endoso = null;
    	
		if (dto.getIdCotizacion() == null) {
			throw new SystemException("El idToCotizacion es requerido");
		}
		
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, dto.getIdCotizacion());
		
		BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, 
				CotizacionContinuity.BUSINESS_KEY_NAME, cotizacionDTO.getIdToCotizacion(), TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia()));
		
		try {
            //prepare emitir
			cotizacion = endosoPolizaService.prepareBitemporalEndoso(cotizacion.getContinuity().getId(),
                     TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia()));
            
            //emitir
            endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),
                    TimeUtils.getDateTime(cotizacionDTO.getFechaInicioVigencia()),
                    cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());

    		result = this.llenarMap(endoso);
		} catch(NegocioEJBExeption ne){
        	log.error("No fue posible cancelar la poliza ", ne);
        	throw new SystemException("Ocurri\u00F3 un error al cancelar la p\u00F3liza " + ne.getMessage());
        } catch (Exception e) {
        	log.error("No fue posible cancelar la poliza ", e);
        	throw new SystemException("Ocurri\u00F3 un error al cancelar la p\u00F3liza " + e.getMessage());
		}
		
		return result;
	}
	
	private Map<String, Object> llenarMap(EndosoDTO endoso){
		Map<String, Object> result = new HashMap<String, Object>(1);
		
		result.put("idCotizacion", endoso.getIdToCotizacion());
		result.put("fechaInicioVigencia", endoso.getFechaInicioVigencia());
		result.put("fechaFinVigencia", endoso.getFechaFinVigencia());
		result.put("valorPrimaNeta", endoso.getValorPrimaNeta());
		result.put("valorRecargoPagoFrac", endoso.getValorRecargoPagoFrac());
		result.put("valorDerechos", endoso.getValorDerechos());
		result.put("porcentajeBonifComision", endoso.getPorcentajeBonifComision());
		result.put("valorBonifComision", endoso.getValorBonifComision());
		result.put("valorIVA", endoso.getValorIVA());
		result.put("valorPrimaTotal", endoso.getValorPrimaTotal());
		result.put("valorBonifComisionRPF", endoso.getValorBonifComisionRPF());
		result.put("valorComision", endoso.getValorComision());
		result.put("valorComisionRPF", endoso.getValorComisionRPF());
		result.put("tipoCambio", endoso.getTipoCambio());
		result.put("llaveFiscal", endoso.getLlaveFiscal());
		result.put("fechaCreacion", endoso.getFechaCreacion());		
		result.put("descripcionTipoEndoso", Utilerias.calcularDescripcionTipoEndoso(endoso));
		result.put("numeroEndoso", endoso.getId().getNumeroEndoso().intValue());
		
		return result;
		
	}
	
	private EstiloVehiculoDTO getEstiloVehiculoId(BitemporalAutoInciso biAutoInciso, String estiloId) throws SystemException {
		final int claveEstiloLength = 5;
		
		String claveEstilo = StringUtils.leftPad(estiloId, claveEstiloLength,"0");
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.setClaveEstilo(claveEstilo);
		
		NegocioSeccion negocioSeccion =  null;
		
		if (biAutoInciso.getValue().getNegocioSeccionId() != null) {
			negocioSeccion = entidadService.findById(NegocioSeccion.class, new BigDecimal(biAutoInciso.getValue().getNegocioSeccionId()));
		} else {
			throw new SystemException("Error al obtener el estiloVehiculoId ya que el negocioSeccionId es requerido");
		}
		
		MarcaVehiculoDTO marcaVehiculo = marcaVehiculoFacade.findById(biAutoInciso.getValue().getMarcaId());
		
		if (marcaVehiculo != null) {
			estiloVehiculoId.setClaveTipoBien(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien());
		} else {
			throw new SystemException("Error al obtener el estiloVehiculoId ya que No fue posible encontrar la marca "+biAutoInciso.getValue().getMarcaId()+" dada");
		}
		
		NegocioAgrupadorTarifaSeccion negAgrupadorTarifaSeccion = negAgrupadorTarifaSeccionDao.buscarPorNegocioSeccionYMoneda(negocioSeccion, new BigDecimal(484));
		TarifaAgrupadorTarifa tarifaAgrupador = tarifaAgrupadorService.getPorNegocioAgrupadorTarifaSeccion(negAgrupadorTarifaSeccion);
		
		estiloVehiculoId.setIdVersionCarga( new BigDecimal(tarifaAgrupador.getId().getIdVertarifa()));
		
		EstiloVehiculoDTO estiloVehiculo = estiloVehiculoFacade.findById(estiloVehiculoId);
		
		if (estiloVehiculo == null) {
			throw new SystemException("No fue posible encontrar el estilo "+claveEstilo+" dado");
		}
		
		biAutoInciso.getValue().setDescripcionFinal(claveEstilo.concat(" - ").concat(estiloVehiculo.getDescripcionEstilo()));
		biAutoInciso.getValue().setEstiloId(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien().concat("_").concat(claveEstilo).concat("_").concat(tarifaAgrupador.getId().getIdVertarifa().toString()));
		
		return estiloVehiculo;
	}
	
	private Map<String, Object> getResumenCotizacion(Map<String, Object> result, EndosoTransporteDTO dto) throws SystemException {
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
		ResumenCostosDTO resumenCostosDTO;
        
		try {
			resumenCostosDTO = calculoService.resumenCotizacionEndoso(Integer.parseInt(""+ result.get("idCotizacion")), 
					TimeUtils.getDateTime(format.parse(dto.getFechaInicioVigenciaEndoso())));
            result.put("primatotal", resumenCostosDTO.getPrimaTotal());
	        result.put("Descuentos", resumenCostosDTO.getDescuento());
	        result.put("primaNeta", resumenCostosDTO.getPrimaNetaCoberturas());
	        result.put("recargo", resumenCostosDTO.getRecargo());
	        result.put("derechoPago", resumenCostosDTO.getDerechos());
	        result.put("iva", resumenCostosDTO.getIva());
		} catch (ParseException e) {
			throw new SystemException("Error al obtener el resumen de pago");
		} catch (Exception e) {
			throw new SystemException("Error al obtener el resumen de pagos");
		}
		
		return result;
	}

	public EndosoWSMidasAutosDao getEndosoWsMidasAutosDao() {
		return endosoWsMidasAutosDao;
	}
	@EJB
	public void setEndosoWsMidasAutosDao(EndosoWSMidasAutosDao endosoWsMidasAutosDao) {
		this.endosoWsMidasAutosDao = endosoWsMidasAutosDao;
	}
	
	/**
	 * 
	 * @param token
	 * @return
	 * @throws SystemException
	 */
	private Usuario validarToken(String token) throws SystemException{
		
		Usuario usuario = null;
		if (token != null && !token.equals("")){
			try {
				usuario = usuarioService.buscarUsuarioRegistrado(token);
			} catch(Exception e){
				log.info("No fue posible obtener el usuario actual\n"+e.getMessage());
				throw new SystemException("No fue posible obtener el usuario actual con el token "+token+" dado.");
			}
		} else{
			throw new SystemException("El token de sesion es requerido");
		}
		return usuario;
	}
}
