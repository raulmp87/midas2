package mx.com.afirme.midas2.service.impl.fuerzaventa;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import com.asm.dto.RoleDTO;
import com.asm.dto.UserDTO;
import com.asm.dto.UserRoleDTO;

import mx.com.afirme.midas2.dao.fuerzaventa.AgenteMidasDao;
import mx.com.afirme.midas2.dao.fuerzaventa.PromotoriaJPADao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente.Situacion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.fuerzaventa.PromotoriaView;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote;
import mx.com.afirme.midas2.dto.fuerzaventa.ReplicarFuerzaVentaSeycosFacadeRemote.TipoAccionFuerzaVenta;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.PromotoriaJPAService;
import mx.com.afirme.midas2.service.fuerzaventa.ValorCatalogoAgentesService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
@Stateless
public class PromotoriaJPAServiceImpl implements PromotoriaJPAService{
	
	@Override
	public Promotoria saveFull(Promotoria promotoria) {
				
		if(promotoria == null){
			
			throw new RuntimeException("Promotoria es nula");
			
		}
		
		validaPromotoriaSinAgentes(promotoria);
		
		boolean esNueva = (promotoria.getId() != null ? false : true);
				
		Persona personaResponsable = promotoria.getPersonaResponsable();
		
		if(isNull(personaResponsable) || isNull(personaResponsable.getIdPersona())) {
			
			throw new RuntimeException("Favor de registrar un responsable para la promotoria.");
			
		}
		
		if(isEmptyList(personaResponsable.getDomicilios())) {
			
			throw new RuntimeException("El responsable debe de tener registrado un domicilio fiscal para poder guardar los datos de la promotoria.");
			
		}
		
		
		promotoria.setCorreoElectronico(personaResponsable.getEmail());
		
		
		if (promotoria.getEjecutivo().getId() != null) {
					
			Ejecutivo ejecutivo = entidadService.findById(Ejecutivo.class, promotoria.getEjecutivo().getId());
		
			promotoria.setEjecutivo(ejecutivo);
		
		}
		
		
		if (promotoria.getTipoPromotoria().getId() != null) {
			
			ValorCatalogoAgentes tipoPromotoria = entidadService.findById(ValorCatalogoAgentes.class, promotoria.getTipoPromotoria().getId());

			promotoria.setTipoPromotoria(tipoPromotoria);

		}
		
		
		if (promotoria.getAgrupadorPromotoria().getId() != null) {
			
			ValorCatalogoAgentes agrupador = entidadService.findById(ValorCatalogoAgentes.class, promotoria.getAgrupadorPromotoria().getId());

			promotoria.setAgrupadorPromotoria(agrupador);

		}

		
		if (esNueva) {
			
			if (promotoria.isCapturaManualClave()) {
				
				if (!esClaveUnica(promotoria.getIdPromotoria())) {
					
					throw new RuntimeException("La clave de la promotoria ya se encuentra asignada a otra promotoria");
					
				}
				
			} else {
			
				//Para evitar posibles inconsistencias en el historico
				
				promotoria.setIdPromotoria(null);
				
			}
			
		}		
		
		
		//Comienza una transacción manejada de modo manual porque se incluyen actualizaciones por WS que están fuera de la transacción del EJB
		//que pudiera dejar la información inconsistente
		
		ActualizacionRol actualizacionRol = null;
		
		try {
		
			if (!esNueva) {
				
				actualizacionRol = actualizaAgentePromotor(promotoria);
				
			}
				
			promotoria = entidadService.save(promotoria);
					
			if(esNueva && !promotoria.isCapturaManualClave()) { 
				
				//Por default, se setea la clave (SEYCOS) igual al id (MIDAS)
				
				promotoria.setIdPromotoria(promotoria.getId());
				
				promotoria = entidadService.save(promotoria);
				
			}
			
			//Se replica en SEYCOS
		
			replicarFuerzaVentaSeycosFacade.replicarFuerzaVenta(promotoria, TipoAccionFuerzaVenta.GUARDAR);
			
			
			if (!esNueva && actualizacionRol != null) {
				
				//Se cambian los roles del agente promotor y de su antecesor en caso de que aplique
				
				if (actualizacionRol.getNuevoPromotor() != null) {
					
					asignaRolPromotor(actualizacionRol.getNuevoPromotor());
					
					actualizacionRol.setAsignacionExitosa(true);
					
				}
				
				if (actualizacionRol.getPromotorOriginal() != null) {
					
					desasignaRolPromotor(actualizacionRol.getPromotorOriginal());
					
				}
				
			}
			
			
		} catch (Exception e) {
			
			if (actualizacionRol != null && actualizacionRol.isAsignacionExitosa()) {								
				
				desasignaRolPromotor(actualizacionRol.getNuevoPromotor());
				
			}
			
			if (e instanceof RuntimeException) {
				
				throw (RuntimeException) e;
				
			}
						
			throw new RuntimeException(e);
			
		}
						
		return promotoria;
		
	}
	
	@Override
	public void unsubscribe(Promotoria promotoria) {
		
		if(promotoria == null || promotoria.getId() == null) {
			
			throw new RuntimeException("Promotoria es nula");
			
		}
		
		promotoria = entidadService.findById(Promotoria.class, promotoria.getId());

		promotoria.setClaveEstatus(Promotoria.ESTATUS_INACTIVO);
				
	    validaPromotoriaSinAgentes(promotoria);
	    
		promotoria.setAgentePromotor(null);
		promotoria.setAgrupadorPromotoria(null);
		promotoria.setEjecutivo(null);
		
		entidadService.save(promotoria);
				
	}

	@Override
	public List<Promotoria> findByFilters(Promotoria arg0) {
		return promotoriaDao.findByFilters(arg0, null);
	}
	
	@Override
	public List<Promotoria> findByEjecutivo(Long arg0) {
		return promotoriaDao.findByEjecutivo(arg0);
	}
	
	@Override
	public Promotoria loadById(Promotoria entidad){
		return promotoriaDao.loadById(entidad, null);
	}
	
	@Override
	public List<PromotoriaView> findByFiltersView(Promotoria filtroPromotoria){
		return promotoriaDao.findByFiltersView(filtroPromotoria);
	}
	
	@Override
	public List<PromotoriaView> getList(boolean onlyActive){
		return promotoriaDao.getList(onlyActive);
	}
	
	@Override
	public List<PromotoriaView> findByEjecutivoLightWeight(Long idParent){
		return promotoriaDao.findByEjecutivoLightWeight(idParent);
	}
	@Override
	public List<PromotoriaView> findPromotoriaConEjecutivosExcluyentes(List<Long> promotorias,List<Long> ejecutivosExcluyentes,List<Long> gerenciasExcluyentes,List<Long> centrosExcluyentes){
		return promotoriaDao.findPromotoriaConEjecutivosExcluyentes(promotorias,ejecutivosExcluyentes,gerenciasExcluyentes,centrosExcluyentes);
	}
	
	@Override
	public Promotoria loadById(Promotoria entidad, String fechaHistorico){
		return promotoriaDao.loadById(entidad, fechaHistorico);
	}
	
	@Override
	public List<Promotoria> findByFilters(Promotoria arg0, String fechaHistorico) {
		return promotoriaDao.findByFilters(arg0, fechaHistorico);
	}
		
	private void validaPromotoriaSinAgentes(Promotoria promotoria) {
		
		if (Promotoria.ESTATUS_INACTIVO.equals(promotoria.getClaveEstatus()) 
				&& promotoriaDao.agentesAsignadosEnPromotoria(promotoria) > 0) {
			
			throw new RuntimeException("La Promotor\u00EDa todav\u00EDa tiene agentes asignados");
			
		}
		
	}
	
	
	/*
	 * Se valida de que la clave de la promotoria sea única en el catálogo
	 */
	private boolean esClaveUnica(Long clavePromotoria) {
				
		List<Promotoria> resultado = entidadService.findByProperty(Promotoria.class, "idPromotoria", clavePromotoria);
		
		if (!resultado.isEmpty() && resultado.get(0) != null) {
			
			return false;
			
		}
		
		return true;
				
	}

	
	private ActualizacionRol actualizaAgentePromotor(Promotoria promotoria) {
	
		ActualizacionRol actualizacionRol = new ActualizacionRol();
		
		Promotoria promotoriaOriginal = entidadService.evictAndFindById(Promotoria.class, promotoria.getId());
		
		boolean desasignarPromotorOriginal = false;
		
		
		if (promotoria.getAgentePromotor().getId() != null) {
			
			Agente agente = entidadService.findById(Agente.class, promotoria.getAgentePromotor().getId());
			
			if (agente.getIdAgente() == null) {
				
				throw new RuntimeException("El agente debe contar con una clave.");
				
			}
					
			if (!agente.getIdAgente().equals(promotoriaOriginal.getIdAgentePromotor())) {
				
				desasignarPromotorOriginal = true;
								 
				// Se revisa que el agente pertenezca a la promotoría
				
				if (agente.getPromotoria() == null || !promotoriaOriginal.getId().equals(agente.getPromotoria().getId())) {
					
					throw new RuntimeException("El agente debe pertenecer a la promotoria.");
					
				}
				
				// Se revisa que el agente esté autorizado
				
				ValorCatalogoAgentes situacionAutorizado;
				
				try {
					
					situacionAutorizado = catalogoService.obtenerElementoEspecifico(Situacion.GRUPO_CATALOGO, Situacion.AUTORIZADO.getValue());
					
				} catch (Exception e) {
					
					throw new RuntimeException(e);
					
				}
								
				if (agente.getTipoSituacion() == null || !situacionAutorizado.getId().equals(agente.getTipoSituacion().getId())) {
				
					throw new RuntimeException("El agente debe tener una situacion de autorizado.");
					
				}
								
				// Se marca al agente como agente promotor
				
				promotoria.setIdAgentePromotor(agente.getIdAgente()); // Notese que lo que se asigna es la clave del agente, no el id
				
				// Se cambia de rol a promotor (MIDAS y Emisión Delegada)
				
				actualizacionRol.setNuevoPromotor(agente);
				
				
			} 			
			
		} else {
			
			desasignarPromotorOriginal = true;
			
		}
				
		if (promotoriaOriginal.getIdAgentePromotor() != null && desasignarPromotorOriginal) {
			
			// Desasigna al promotor original regresando su rol al de agente (MIDAS y Emisión Delegada)		
			
			Agente agentePromotorOriginal = entidadService.findByProperty(Agente.class, "idAgente", promotoriaOriginal.getIdAgentePromotor()).get(0);
			
			if (agentePromotorOriginal == null) {
				
				throw new RuntimeException("No se pudo encontrar al agente promotor original.");
				
			}
			
			actualizacionRol.setPromotorOriginal(agentePromotorOriginal);
			
		}
		
		return actualizacionRol;
				
	}
	
	
	private void asignaRolPromotor(Agente agente) {
		
		cambiaRolesAgente(agente, true);
		
	}
	
	private void desasignaRolPromotor(Agente agente) {
				
		cambiaRolesAgente(agente, false);
				
	}
	
	private void cambiaRolesAgente(Agente agente, boolean esAsignacionPromotor) {
		
		RoleDTO rolNuevoMidas;
		RoleDTO rolNuevoED;
		RoleDTO rolViejoMidas;
		RoleDTO rolViejoED;
		
		//Se obtiene el usuario a partir del agente
		
		UserDTO usuarioAgente = usuarioService.getUserDTOById(usuarioService.findUserIdByUsername(agente.getCodigoUsuario()));
		
		if (usuarioAgente == null) {
			
			throw new RuntimeException("El agente debe tener un usuario en el sistema.");
			
		}
				
		if (esAsignacionPromotor) {
			// Al asignar el rol de promotor a un agente, el rol se le debe asignar tanto para MIDAS como para Emisión Delegada.
			// Al asignar el rol de promotor a un agente, se le debe de quitar el rol de agente tanto para MIDAS como para Emisión Delegada.
			
			rolNuevoMidas = getRolPromotorMidas();
			rolNuevoED = getRolPromotorED();
			rolViejoMidas = getRolAgenteMidas();
			rolViejoED = getRolAgenteED();
			
		} else {
			// Al desasignar el rol de promotor a un agente, se le debe de quitar el rol tanto para MIDAS como para Emisión Delegada.
			// Al desasignar el rol de promotor a un agente, se le debe de reasignar el rol de agente tanto para MIDAS como para Emisión Delegada.
			
			rolNuevoMidas = getRolAgenteMidas();
			rolNuevoED = getRolAgenteED();
			rolViejoMidas = getRolPromotorMidas();
			rolViejoED = getRolPromotorED();
			
		}
				
		List<RoleDTO> roles = getRoles(usuarioAgente);
				
		if (roles.indexOf(rolViejoMidas) >= 0) {
		
			roles.remove(roles.indexOf(rolViejoMidas));
			
		}
		
		if (roles.indexOf(rolViejoED) >= 0) {
			
			roles.remove(roles.indexOf(rolViejoED));
			
		}
				
		if (roles.indexOf(rolNuevoMidas) < 0) {
			
			roles.add(rolNuevoMidas);
			
		}
		
		if (roles.indexOf(rolNuevoED) > 0) {
			
			roles.remove(roles.indexOf(rolNuevoED));
			
		}
						
		if (roles.indexOf(rolNuevoED) < 0) {
			
			roles.add(0, rolNuevoED);	
			
		}
						
		usuarioAgente.setUserRoles(getNuevosUsuarioRol(usuarioAgente, roles));
				
		usuarioService.updateMidasUser(usuarioAgente, usuarioAgente, getRolesMidas(usuarioAgente)); //Aqui (por alguna extraña razon) deben de ir unicamente todos los roles de MIDAS del usuario
			
	}
			
	private RoleDTO getRolAgenteMidas() {
		
		return usuarioService.getRoleDTOById(agenteMidasDao.buscarIdRolAgenteMidas());
		
	}
	
	private RoleDTO getRolPromotorMidas() {
		
		return usuarioService.getRoleDTOById(agenteMidasDao.buscarIdRolPromotorMidas());
		
	}
	
	private RoleDTO getRolAgenteED() {
		
		return usuarioService.getRoleDTOById(agenteMidasDao.buscarIdRolAgenteED());
		
	}
	
	private RoleDTO getRolPromotorED() {
		
		return usuarioService.getRoleDTOById(agenteMidasDao.buscarIdRolPromotorED());
		
	}	
	
	private List<RoleDTO> getRoles(UserDTO usuario) {
		
		return getRolesDetallado(usuario, false);
		
	}
	
	private List<RoleDTO> getRolesMidas(UserDTO usuario) {
		
		return getRolesDetallado(usuario, true);
		
	}
		
	private List<RoleDTO> getRolesDetallado(UserDTO usuario, boolean soloMidas) {
		
		List<RoleDTO> roles = new ArrayList<RoleDTO>();
	    
	    int midasAppId = new Integer(sistemaContext.getMidasAppId()).intValue();
	    
	    for(UserRoleDTO usuarioRol: usuario.getUserRoles()) {
	    	
	    	if (!soloMidas || usuarioRol.getRole().getApplication().getApplicationId() == midasAppId) {
	    	
	    		roles.add(usuarioRol.getRole());
	    		
	    	}
	    	
	    }
	    
	    return roles;
	    
	}
		
	private List<UserRoleDTO> getNuevosUsuarioRol(UserDTO usuario, List<RoleDTO> roles) {
		
		List<UserRoleDTO> usuarioRoles = new ArrayList<UserRoleDTO>();
		
		UserRoleDTO usuarioRol;
		
		for (RoleDTO rol : roles) {
			
			usuarioRol = new UserRoleDTO();
			
			usuarioRol.setUser(usuario);
			
			usuarioRol.setRole(rol);
			
			usuarioRoles.add(usuarioRol);
			
		}
		
		return usuarioRoles;
				
	}
	
	
	private class ActualizacionRol {
		
		private Agente nuevoPromotor;
		
		private Agente promotorOriginal;
		
		private boolean asignacionExitosa = false;
		
		public ActualizacionRol() {
			
		}

		public Agente getNuevoPromotor() {
			return nuevoPromotor;
		}

		public void setNuevoPromotor(Agente nuevoPromotor) {
			this.nuevoPromotor = nuevoPromotor;
		}

		public Agente getPromotorOriginal() {
			return promotorOriginal;
		}

		public void setPromotorOriginal(Agente promotorOriginal) {
			this.promotorOriginal = promotorOriginal;
		}

		public boolean isAsignacionExitosa() {
			return asignacionExitosa;
		}

		public void setAsignacionExitosa(boolean asignacionExitosa) {
			this.asignacionExitosa = asignacionExitosa;
		}

		
	};
	
	
	private EntidadService entidadService;
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	private ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade;
	
	@EJB
	public void setReplicarFuerzaVentaSeycosFacade(
			ReplicarFuerzaVentaSeycosFacadeRemote replicarFuerzaVentaSeycosFacade) {
		this.replicarFuerzaVentaSeycosFacade = replicarFuerzaVentaSeycosFacade;
	}
	
	private PromotoriaJPADao promotoriaDao;
	
	@EJB
	public void setPromotoriaDao(PromotoriaJPADao promotoriaDao) {
		this.promotoriaDao = promotoriaDao;
	}
	
	private ValorCatalogoAgentesService catalogoService;
	
	@EJB
	public void setCatalogoService(ValorCatalogoAgentesService catalogoService) {
		this.catalogoService = catalogoService;
	}
	
	private AgenteMidasDao agenteMidasDao;
	
	@EJB
	public void setAgenteMidasDao(AgenteMidasDao agenteMidasDao) {
		this.agenteMidasDao = agenteMidasDao;
	}
	
	private UsuarioService usuarioService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	private SistemaContext sistemaContext;
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
		
}
