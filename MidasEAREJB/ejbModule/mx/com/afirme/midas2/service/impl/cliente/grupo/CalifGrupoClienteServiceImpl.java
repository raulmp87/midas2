package mx.com.afirme.midas2.service.impl.cliente.grupo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.cliente.grupo.CalifGrupoClienteDao;
import mx.com.afirme.midas2.domain.cliente.CalifGrupoCliente;
import mx.com.afirme.midas2.service.cliente.grupo.CalifGrupoClienteService;

@Stateless
public class CalifGrupoClienteServiceImpl implements CalifGrupoClienteService {

	private CalifGrupoClienteDao califGrupoClienteDao;
		
	@EJB
	public void setCalifGrupoClienteDao(
			CalifGrupoClienteDao califGrupoClienteDao) {
		this.califGrupoClienteDao = califGrupoClienteDao;
	}

	@Override
	public List<CalifGrupoCliente> buscarTodos() {
		return califGrupoClienteDao.findAll();
	}

}
