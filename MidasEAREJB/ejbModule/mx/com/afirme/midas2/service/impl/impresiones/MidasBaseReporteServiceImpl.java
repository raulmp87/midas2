package mx.com.afirme.midas2.service.impl.impresiones;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.com.afirme.midas2.service.impl.impresiones.ReportPOISupport.CopyRowSupport;
import mx.com.afirme.midas2.service.reportes.MidasBaseReporteService;
import mx.com.afirme.midas2.service.reportes.MidasBaseReporte.MidasBaseReporteExcel;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

import org.apache.commons.io.IOUtils;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.streaming.SXSSFRow;
import org.apache.poi.xssf.streaming.SXSSFSheet;
import org.apache.poi.xssf.streaming.SXSSFWorkbook;
import org.apache.tools.ant.DirectoryScanner;

@Stateless
@TransactionManagement(TransactionManagementType.BEAN)
public class MidasBaseReporteServiceImpl implements MidasBaseReporteService {
	@EJB
	private SistemaContext sistemaContext;
	private int numRows = 0;
	private int rowIndex = 1;
	private Field[] columns;
	private Method[] m;
	private int gap = 0;
	private List<String> fileNames = null;
	private String uploadFolder; 
	@Override
	public InputStream generateExcelVeryLong(MidasBaseReporteExcel sheet)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, IOException, FileNotFoundException,
			InvalidFormatException {
		
		InputStream is = null;
//		if(sheet.getDataSource().size() < 10000){
			is = generateExcelVeryLongXlS(sheet);
//		}else{
//			is = generateExcelVeryLongTXT(sheet);
//		}
		return is;
	}
	
//	private InputStream generateExcelVeryLongXlS(MidasBaseReporteExcel sheet) 
//	throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException, InvalidFormatException{
//		initParams(sheet.getDataSource());
//		List<Object> ds = new ArrayList<Object>();
////		int index = 0;
//		String fileName;
//		SXSSFWorkbook wb = new SXSSFWorkbook();
//		List<SXSSFSheet> listSheet1 = new ArrayList<SXSSFSheet>(); 
//		listSheet1.add((SXSSFSheet) wb.createSheet("foja1_"
//				+ Calendar.getInstance().getTimeInMillis()));
//		listSheet1.add((SXSSFSheet) wb.createSheet("foja2_"
//				+ Calendar.getInstance().getTimeInMillis()));
////		for(int a = 1; a>=2;a++){
////			System.out.println("**************************** numRows = "+numRows);
////			for (int x = 0; x < numRows; x++) {
////				if (x != 0 && (x % gap)== 0) {
////					List<?> d = sheet.getDataSource().subList(index, x);
////					ds.addAll(d);
////					index = x;
////					fileNames.add(generateExcel(d, wb, listSheet1, gap, 1));
////				}
////			}
////		}
//		// get sublist no processed
//		List<?> subList = (List<?>) CollectionUtils.subtract(
//				sheet.getDataSource(), ds);
//		fileName = (generateExcel(subList, wb, listSheet1, subList.size(), 1));
//		return compress(fileName);
//	}
	
	private InputStream generateExcelVeryLongXlS(MidasBaseReporteExcel sheet) 
	throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException, InvalidFormatException{
		initParams(sheet.getDataSource());
		String fileName;
		fileName = (generateExcelSXSSF(sheet.getDataSource()));
		return compress(fileName);
	}
	
	private InputStream generateExcelVeryLongTXT(MidasBaseReporteExcel sheet) 
	throws IllegalArgumentException, IllegalAccessException, InvocationTargetException, IOException, InvalidFormatException{
		initParams(sheet.getDataSource());	
		String nameFile = "reportExcelSupport_" + Calendar.getInstance().getTimeInMillis() + ".csv";
		File file = new File(uploadFolder + nameFile);
		try {
			FileWriter outFile = new FileWriter(file);

			BufferedWriter out = new BufferedWriter(outFile);
			
			out.write(generateTitlesTxt());
			out.newLine();
			System.out.println("**************************** numRows = "+numRows);
			for (int x = 0; x < numRows; x++) {
				out.write(generateRowTxt(sheet.getDataSource().get(x)));
				out.newLine();
			}
			out.flush();
			out.close();
			outFile.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return compress(file.getName());
	}
	
	private String generateTitlesTxt() throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {

		StringBuilder linea = new StringBuilder();
		for (int x = 0; x < columns.length; x++) {
			for (Method method : m) {
				String mname = method.getName();
			if (!mname.equalsIgnoreCase("get" + columns[x].getName())) {
				continue;
			}
			if (!linea.toString().isEmpty()) {
				linea.append(",");
			}
			linea.append(columns[x].getName());
			}
		}
		return linea.toString();
	}
	
	private String generateRowTxt(Object obj) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		
		StringBuilder linea = new StringBuilder();
		for (int x = 0; x < columns.length; x++) {
				for (Method method : m) {
					String mname = method.getName();
					if (!mname.equalsIgnoreCase("get" + columns[x].getName())) {
						continue;
					}
					Object v = method.invoke(obj);
					if(!linea.toString().isEmpty()){
						linea.append("\t");
					}
					linea.append(populateTxt(v));
				}
		}
		return linea.toString();
	}
	
	private String populateTxt(Object v) {
		String text = "";
		if (v == null) {
			return text;
		}
		if (v instanceof String) {
			text = (String) v;
		} else if (v instanceof Long) {
			Long value = (Long) v;
			text = value.toString();
		} else if (v instanceof Integer) {
			Integer value = (Integer) v;
			text = value.toString();
		} else if (v instanceof Double) {
			Double value = (Double) v;
			text = value.toString();
		} else if (v instanceof BigDecimal) {
			BigDecimal value = (BigDecimal) v;
			text = value.toString();
		} else if (v instanceof Calendar) {
			// Todo: ponerle un formato a la fecha
			Calendar value = (Calendar) v;
			text = value.toString();
		} else if (v instanceof Date) {
			// Todo: ponerle un formato a la fecha
			Date value = (Date) v;
			text = value.toString();
		} else if (v instanceof Boolean) {
			Boolean value = (Boolean) v;
			text = value.toString();
		}
		return text;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public InputStream  generateExcel(MidasBaseReporteExcel sheet) throws IllegalAccessException, IllegalArgumentException,
	InvocationTargetException, IOException, FileNotFoundException,
	InvalidFormatException {
		List<?> dataSource = sheet.getDataSource();
		initParams(dataSource);
		Workbook wb = new SXSSFWorkbook();
		try {
			Sheet sheet1 = wb.createSheet(sheet.getSheetName());
			for (int x = 0; x < numRows; x++) {
				// Rows Columns name
				Row rowColumnsName = sheet1.createRow(0);
				// Rows are 1 based.
				Row row = sheet1.createRow(rowIndex);
				populateRows(row, rowColumnsName, dataSource.get(x));
				rowIndex++;
			}
			// Write the output to a file
			FileOutputStream fileOut = new FileOutputStream(uploadFolder
					+ sheet.getBookName());
			wb.write(fileOut);
			fileOut.close();
			return compress(sheet.getBookName());
		} catch (IllegalAccessException e) {
			throw new RuntimeException("Error " +  e.getCause());
		} catch (InvocationTargetException e) {
			throw new RuntimeException("Error " +  e.getCause());
		} catch (IOException e) {
			throw new RuntimeException("Error " +  e.getCause());
		}
	}
	
	public void initParams(List<?> dataSource){
		Thread t = new Thread(new InnerReportSupport());
		t.start();
		numRows = dataSource.size();
		rowIndex = 1;
		columns = dataSource.get(0).getClass().getDeclaredFields();
		m = dataSource.get(0).getClass().getDeclaredMethods();
		gap = calculateGap(numRows);
		fileNames = new LinkedList<String>();
		uploadFolder = sistemaContext.getUploadFolder();
	}
	
	@Override
	public InputStream generateExcel(List<MidasBaseReporteExcel> sheets) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void populateRows(Row row, Row rowColumnsName, Object obj)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException {
		// Create a cell and put a value in it.
		for (int x = 0; x < columns.length; x++) {
			for (Method method : m) {
				String mname = method.getName();
				if (!mname.equalsIgnoreCase("get" + columns[x].getName())) {
					continue;
				}
				// Set the property name to colum
				rowColumnsName.createCell(x).setCellValue(columns[x].getName());
				Object v = method.invoke(obj);
				if (v != null) {
					populateRows_setValue(row, x, v);
				}
			}
		}
	}

	@Override
	public void populateRows(Row row, Row rowColumnsName,
			String[] columnsNames, Object obj) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException {
		// TODO Auto-generated method stub

	}

	@Override
	public void populateRows_setValue(Row row, int x, Object v) {
		if (v instanceof String) {
			row.createCell(x).setCellValue((String) v);
//			row.createCell(x).setCellType(Cell.CELL_TYPE_STRING);
		}
		if (v instanceof Long) {
			row.createCell(x).setCellValue((Long) v);
//			row.createCell(x).setCellType(Cell.CELL_TYPE_NUMERIC);
		}
		if (v instanceof Integer) {
			row.createCell(x).setCellValue((Integer) v);
//			row.createCell(x).setCellType(Cell.CELL_TYPE_NUMERIC);
		}
		if (v instanceof Double) {
			row.createCell(x).setCellValue((Double) v);
//			row.createCell(x).setCellType(Cell.CELL_TYPE_NUMERIC);
		}
		if (v instanceof BigDecimal) {
			BigDecimal vs = (BigDecimal) v;
			row.createCell(x).setCellValue(vs.doubleValue());
//			row.createCell(x).setCellType(Cell.CELL_TYPE_NUMERIC);
		}
		if (v instanceof Calendar) {
			// Todo: ponerle un formato a la fecha
			row.createCell(x).setCellValue((Calendar) v);
//			row.createCell(x).setCellType(Cell.CELL_TYPE_STRING);
		}
		if (v instanceof Date) {
			// Todo: ponerle un formato a la fecha
			row.createCell(x).setCellValue((Date) v);
//			row.createCell(x).setCellType(Cell.CELL_TYPE_STRING);
		}
		if (v instanceof Boolean) {
			row.createCell(x).setCellValue((Boolean) v);
//			row.createCell(x).setCellType(Cell.CELL_TYPE_BOOLEAN);
		}

	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public InputStream margeFiles(List<String> fileNames, String fileName)
			throws FileNotFoundException, IOException, InvalidFormatException {
		List<Workbook> files = new ArrayList<Workbook>();
        for (String file : fileNames) {
        	FileInputStream inp = new FileInputStream(uploadFolder + file);
        	Workbook wb = WorkbookFactory.create(inp);
            files.add(wb);
        }
        // Create new Excel 
        Workbook wb = new SXSSFWorkbook();
        Sheet snewWb = wb.createSheet("Hoja1");
        List<Sheet> sheetList = new LinkedList<Sheet>();
        int numTotalRows = 0;
        for (Workbook w : files) {
            final int ns = w.getNumberOfSheets();
            for (int x = 0; x < ns; x++) {
                sheetList.add(w.getSheetAt(x));
                numTotalRows += w.getSheetAt(x).getLastRowNum();
            }
        }
        int rowCopyIndex = 0;
        for (Sheet s : sheetList) {
            for (int x = 0; x < numTotalRows + 1; x++) {
                if (x > s.getLastRowNum()) {
                    break;
                } else {
                    CopyRowSupport.copyRow(s, snewWb, x, rowCopyIndex);
                    rowCopyIndex++;
                }
            }
        }
//        String fileNameSupport = "reporteExcel_" + Calendar.getInstance().getTimeInMillis() + ".xls";
//        fileNames.add(fileName);
		FileOutputStream fileOut = new FileOutputStream(uploadFolder + fileName);
        wb.write(fileOut);
        fileOut.close();
		return compress(fileName);
	}

	@Override
	public String generateExcel(List<?> subDataSource, Workbook wb, Sheet sheet1,
			int numRows, int rowIndex) throws IllegalAccessException,
			IllegalArgumentException, InvocationTargetException, IOException {
		for (int x = 0; x < numRows; x++) {
			// Rows Columns name
			SXSSFRow rowColumnsName = (SXSSFRow) sheet1.createRow(0);
			// Rows are 1 based.
			SXSSFRow row = (SXSSFRow) sheet1.createRow(rowIndex);
			populateRows(row, rowColumnsName, subDataSource.get(x));
			rowIndex++;
		}
		// Write the output to a file
		String nameFile = "reportExcelSupport_" + Calendar.getInstance().getTimeInMillis()
				+ ".xlsx";
		FileOutputStream fileOut = new FileOutputStream(uploadFolder + nameFile);
		wb.write(fileOut);
		fileOut.close();
		return nameFile;
	}

	@Override
	public int calculateGap(int dataSourceSize) {
		// TODO validar si es un xls no puede tener mas de 65 mil
		if (dataSourceSize >= 1000 && dataSourceSize < 10000) {
			return 1000;
		}
		if (dataSourceSize > 10000 && dataSourceSize < 65000) {
			return 10000;
		}
		return 100;
	}
	
	
	private InputStream compress(String fileName)
    throws IOException {
		byte[] buffer = new byte[1024];
    	try{
			String zipName = fileName + ".zip";
    		FileOutputStream fos = new FileOutputStream(uploadFolder + zipName);
    		ZipOutputStream zos = new ZipOutputStream(fos);
    		ZipEntry ze= new ZipEntry(fileName);
    		zos.putNextEntry(ze);
    		FileInputStream in = new FileInputStream(uploadFolder + fileName);
 
    		int len;
    		while ((len = in.read(buffer)) > 0) {
    			zos.write(buffer, 0, len);
    		}
 
    		in.close();
    		zos.closeEntry();
 
    		//remember close it
    		zos.close();
    		System.out.println("Done");
    		InputStream fileInputStream = new FileInputStream(uploadFolder + zipName);
    		return fileInputStream;
    	} catch (RuntimeException error) {
    		error.printStackTrace();
    	}
    	return null;
	}

	@Override
	public InputStream findFile(String fileName) {
		if (uploadFolder == null) {
			uploadFolder = sistemaContext.getUploadFolder();
		}
		DirectoryScanner s = new DirectoryScanner();
		s.setBasedir(new File(uploadFolder));
		s.setIncludes(new String[]{"**/"+fileName});
		s.setCaseSensitive(false);
		s.scan();
		String[] files = s.getIncludedFiles();
		for (int i = 0; i < files.length; i++) {
			if (!files[i].equalsIgnoreCase(fileName)) {
				continue;
			}
			InputStream file = null;
			try {
				file = new FileInputStream(uploadFolder + files[i]);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
				throw new RuntimeException("Error to try load the file "
						+ files[i]);
			} finally {
				IOUtils.closeQuietly(file);
			}
			return file;
		}
		return null;
	}

	@Override
	public synchronized void deleteTemporalsFiles() {
		// TODO identificar cuales archivos tengo que borrar por el inicio del
		// nombre
		if (uploadFolder == null) {
			uploadFolder = sistemaContext.getUploadFolder();
		}
		DirectoryScanner s = new DirectoryScanner();
		s.setBasedir(new File(uploadFolder));
		s.setIncludes(new String[]{"**/reporteExcel*.xls","**/reportExcelSupport*.xls"});
		s.setCaseSensitive(false);
		s.scan();
		String[] files = s.getIncludedFiles();
		for (int i = 0; i < files.length; i++) {
			File f = new File(uploadFolder + files[i]);
			f.delete();
		}
	}

	class InnerReportSupport implements Runnable {
		/**
		 * This method delete all the temporal files generated in the creation
		 * of a report
		 */
		@Override
		public void run() {
			deleteTemporalsFiles();
		}

	}

	@Override
	public String generateExcelSXSSF(List<?> dataSource)
			throws IllegalAccessException, IllegalArgumentException,
			InvocationTargetException, IOException {

		SXSSFWorkbook wb = new SXSSFWorkbook(100);
		int num_hoja = 1;
		List<List<?>> subDataSource = new ArrayList<List<?>>(getSublist(dataSource));
		for(List<?> subLista : subDataSource){
			Sheet hoja = (SXSSFSheet) wb.createSheet("hoja "+num_hoja);
			int cont = 0;
			rowIndex = 1;
			// Rows Columns name
			Row rowColumnsName =  hoja.createRow(0);
			for(int x = 0; x< ((subLista.size()<65000)?subLista.size():65000); x++){
					// Rows are 1 based.
					Row row = hoja.createRow(rowIndex);
					populateRows(row, rowColumnsName, subLista.get(cont));
					cont++;
					rowIndex++;
				}
			num_hoja++;

		}
			// Write the output to a file
			String nameFile = "reportExcelSupport_" + Calendar.getInstance().getTimeInMillis()
					+ ".xls";
	        
			FileOutputStream fileOut = new FileOutputStream(uploadFolder + nameFile);
			wb.write(fileOut);
			fileOut.close();
			return nameFile;
	}

	public List<List<?>> getSublist(List<?> lista){
		int tamanioLista =  lista.size()+1;
		List<List<?>> list_Sublist = new ArrayList<List<?>>();
		int cont = 0;
		if ( tamanioLista < 65000) {
			list_Sublist.add(lista);			
		}else{
			for(int start = 0; start<= tamanioLista; start=start+65000){
				System.out.println("cont"+ cont++);
				int end =  (start+64999<=tamanioLista)?start+64999:tamanioLista-1;
				System.out.println("start: "+ start +" -  end: "+ end+" - tamanioLista: "+tamanioLista);
				list_Sublist.add(lista.subList(start, end));
			}
		}
		return list_Sublist;
	}
}