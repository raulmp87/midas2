/**
 * Clase que llena Paginas y Permisos para el rol de Coordinador Siniestros Facultativo
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.coordinadorsiniestrofacultativo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author Christian Antonio Gomez Flores
 *
 */
public class PaginaPermisoCoordinadorSiniestrosFacultativo {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoCoordinadorSiniestrosFacultativo(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		//Permisos  0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 = AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE
		
		PaginaPermiso pp;
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("crearReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/crearReporte.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listaPolizasReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/listarPolizasSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosPoliza.jsp", "/MidasWeb/siniestro/cabina/reportePoliza.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //GU
		pp.getPermisos().add(listaPermiso.get(13)); //RE
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/mostrarListaReporteDetallePoliza.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosPolizaCrear.jsp", "/MidasWeb/siniestro/cabina/datosPolizaCrear.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/obtenerListaAjustadores.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/guardarReporte.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/listar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp", "/MidasWeb/siniestro/cabina/reporteSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/listarTodos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosReporteSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosPolizaRS.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosPoliza.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosEstadoSiniestroRS.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosEstadoSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadoSiniestro.jsp", "/MidasWeb/siniestro/cabina/reporteEstadoSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/repoGuadSini.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/asignarAjustador.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("caratulaSiniestro.jsp", "/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/siniestro/salvamento/listar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIngresos.jsp", "/MidasWeb/siniestro/finanzas/listarIngresos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarGastos.jsp", "/MidasWeb/siniestro/finanzas/listarGastos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenesDePago.jsp", "/MidasWeb/siniestro/finanzas/listarOrdenesDePago.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentosSiniestro.jsp", "/MidasWeb/siniestro/documentos/listarDocumentosSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIndemnizaciones.jsp", "/MidasWeb/siniestro/finanzas/indemnizacion/listarIndemnizaciones.do"));
		listaPaginaPermiso.add(pp);
		
		//Permisos agregados del excel
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarDatosPreguntasRS.jsp","/MidasWeb/siniestro/cabina/desplegarDatosPreguntasEspeciales.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarDatosTransporteRS.jsp","/MidasWeb/siniestro/cabina/desplegarDatosTransporte.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaIncisosPoliza.jsp","/MidasWeb/siniestro/cabina/listarIncisosPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaSeccionSubIncisos.jsp","/MidasWeb/siniestro/cabina/listarSeccionSubIncisos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaCoberturasRiesgo.jsp","/MidasWeb/siniestro/cabina/listarCoberturasRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaIncisosPoliza.jsp","/MidasWeb/siniestro/cabina/listarIncisosUbicacionPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("preguntasEspeciales.jsp","/MidasWeb/siniestro/cabina/reportePreguntasEspeciales.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("preguntasEspeciale.jsp","/MidasWeb/siniestro/cabina/reporteGuardarEspeciales.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("asociarCartaRechazo.jsp","/MidasWeb/siniestro/finanzas/cartarechazo/asociarCartaRechazo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("aprobarCartaRechazo.jsp","/MidasWeb/siniestro/finanzas/cartarechazo/mostrarAprobarCartaRechazo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("aprobarCartaRechazo.jsp","/MidasWeb/siniestro/finanzas/cartarechazo/aprobarCartaRechazo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("aprobarCartaRechazo.jsp","/MidasWeb/siniestro/finanzas/cartarechazo/rechazarCartaRechazo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("imprimirCartaRechazo.jsp","/MidasWeb/siniestro/finanzas/cartarechazo/mostrarImprimirCartaRechazo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("imprimirCartaRechazo.jsp","/MidasWeb/siniestro/finanzas/cartarechazo/impresaCartaRechazo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarCartaRechazo.jsp","/MidasWeb/siniestro/finanzas/cartarechazo/modificarCartaRechazo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarDocumentosSiniestro.jsp","/MidasWeb/siniestro/documentos/listarDocumentosSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarDocumentosSiniestro.jsp","/MidasWeb/siniestro/documentos/agregarDocumentosSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarDocumentosSiniestro.jsp","/MidasWeb/siniestro/documentos/modificarDocumentosSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarDocumentosSiniestro.jsp","/MidasWeb/siniestro/documentos/borrarDocumentosSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarDocumentoSiniestro.jsp","/MidasWeb/siniestro/documentos/mostrarAgregarDocumentoSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarDocumentoSiniestro.jsp","/MidasWeb/siniestro/documentos/mostrarModificarDocumentoSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarDetalleIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/listarDetalleIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizarIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/mostrarAutorizarIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizarIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/autorizarIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizarIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/cancelarIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("generarConvenioFiniquito.jsp","/MidasWeb/siniestro/finanzas/conveniofiniquito/mostrarGenerarConvenioFiniquito.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/conveniofiniquito/generarConvenioFiniquito.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("recibirConvenioFirmado.jsp","/MidasWeb/siniestro/finanzas/conveniofiniquito/mostrarRecibirConvenioFirmado.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/conveniofiniquito/recibirConvenioFirmado.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("presentarInformePreliminar.jsp","/MidasWeb/siniestro/finanzas/presentarInformePreliminar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/procedeElAnalisisInterno.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/noProcedeElAnalisisInterno.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("presentarInformeFinal.jsp","/MidasWeb/siniestro/finanzas/presentarInformeFinal.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/procedeAIndemnizar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/noProcedeAIndemnizar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizacionTecnicaGasto.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaGasto.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizacionTecnicaIngreso.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIngreso.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizacionTecnicaIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaGasto.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaIngreso.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizarAutorizacionTecnica.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/cancelarAutorizacionTecnica.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("mostrarAutorizacionTecnica.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarAutorizacionTecnica.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaAutorizacionesPorAutorizar.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/listarAutorizacionesTecnicas.do")); 
		pp.getPermisos().add(listaPermiso.get(1)); //AC
		pp.getPermisos().add(listaPermiso.get(11)); //GU
		pp.getPermisos().add(listaPermiso.get(13)); //RE
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("showLigas.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/showLigas.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteSiniestralidad.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestralidad.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteSiniestralidadAnexo.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestralidadAnexo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSiniestralidadYAnexo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteSiniestrosRRCSONORv7.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestrosRRCSONORv7.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSiniestrosRRCSONORv7.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/poliza/guardarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("afectarCoberturaRiesgo.jsp","/MidasWeb/siniestro/poliza/afectarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarAfectarCoberturaRiesgo.jsp","/MidasWeb/siniestro/poliza/desplegarAfectarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/asignarCoordinadorPorZona.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteListarPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/repoGuadSini.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/modificarReporte.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/listarTodos")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/listarFiltrado.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarReporteSiniestro.jsp","/MidasWeb/siniestro/cabina/mostrarModificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarReporteSiniestro.jsp","/MidasWeb/siniestro/cabina/mostrarDetalle.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/comboCoordinadores.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/confirmarAjustador.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/rechazarAjustador.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSPSinParametros.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("estadoSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteGuardarSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/cerrarReporteSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("transportes.jsp","/MidasWeb/siniestro/cabina/reporteTransportes.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reserva.jsp","/MidasWeb/siniestro/finanzas/estimarReserva.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/guardarEstimacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarHistorialReserva.jsp","/MidasWeb/siniestro/finanzas/listarReserva.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarReserva.jsp","/MidasWeb/siniestro/finanzas/mostrarModificarReserva.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/agregarReserva.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizarReserva.jsp","/MidasWeb/siniestro/finanzas/mostrarReservaAutorizar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizarReserva.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarHistorialReservaDetalle.jsp","/MidasWeb/siniestro/finanzas/listarReservaDetalle.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("terminarSiniestro.jsp","/MidasWeb/siniestro/finanzas/mostrarReporteSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/terminarReporteSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/cancelarReporteSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/direccionPoliza.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/listar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/ajustador/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/ajustador/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/ajustador/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/ajustador/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/ajustador/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/aumentovario/listar.do"));  
		listaPaginaPermiso.add(pp); 
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/listarFiltrado.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/agregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/modificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("registroUsuario.jsp","/MidasWeb/catalogos/distanciaciudad/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("error.jsp","/MidasWeb/catalogos/distanciaciudad/borrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarAgregar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarBorrar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarModificar.do"));  
		listaPaginaPermiso.add(pp); 

		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/distanciaciudad/mostrarDetalle.do"));  
		listaPaginaPermiso.add(pp); 
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/distanciaciudad/listar.do")); 
		listaPaginaPermiso.add(pp); 
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listaAfectarCoberturasRiesgo.jsp","/MidasWeb/siniestro/cabina/listarIncisosCoberturaRiesgo.do")); 
		listaPaginaPermiso.add(pp); 
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listaSeccionSubIncisosCoberturaRiesgo.jsp","/MidasWeb/siniestro/cabina/listarSeccionSubIncisosCoberturasRiesgo.do")); 
		listaPaginaPermiso.add(pp); 
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("listaAfectarCoberturasRiesgo.jsp","/MidasWeb/siniestro/cabina/listarCoberturasRiesgoAAfectar.do")); 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/validaMovimientosExistentes.do")); 
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso(); 
		pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/guardarAsignacionPolizaModificar.do")); 
		listaPaginaPermiso.add(pp);		
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestroPDF.do")); 
		listaPaginaPermiso.add(pp);
		
		//Permisos para agregar Ingresos
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarIngresos.jsp","/MidasWeb/siniestro/finanzas/ingresos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/guardarIngresosSinietro.do")); listaPaginaPermiso.add(pp);
		
		//Permisos para agregar Gastos
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarGastoAjuste.jsp","/MidasWeb/siniestro/finanzas/gastosAjuste.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/guardarGastoSiniestro.do")); listaPaginaPermiso.add(pp);
		
		//Permisos para eventos catastroficos
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/eventocatastrofico/listar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/eventocatastrofico/mostrarAgregar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/eventocatastrofico/agregar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/eventocatastrofico/mostrarModificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/eventocatastrofico/modificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/eventocatastrofico/mostrarDetalle.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/eventocatastrofico/mostrarBorrar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/eventocatastrofico/borrar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/eventocatastrofico/listarFiltrado.do")); listaPaginaPermiso.add(pp);
		
		// Permisos para ejecutar el proceso de actualizacion de estatus de ordenes de pago
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/revisarEstatusOrdenDePago.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
		
		return this.listaPaginaPermiso;
	}
	
	
	
}
