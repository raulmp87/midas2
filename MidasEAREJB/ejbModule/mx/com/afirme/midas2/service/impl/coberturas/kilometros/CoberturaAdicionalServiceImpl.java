package mx.com.afirme.midas2.service.impl.coberturas.kilometros;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.coberturas.kilometros.CoberturaAdicionalDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;
import mx.com.afirme.midas2.domain.coberturas.kilometros.GeneraSolicitudRequest;
import mx.com.afirme.midas2.domain.coberturas.kilometros.GeneraSolicitudResponse;
import mx.com.afirme.midas2.domain.coberturas.kilometros.SolicitudCoberturaAdicionalDTO;
import mx.com.afirme.midas2.domain.coberturas.kilometros.SolicitudCoberturaAdicionalDTO.Estatus;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.coberturaadicional.CoberturaAdicionalValoresDTO;
import mx.com.afirme.midas2.dto.endoso.EndosoAdicionalDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.coberturas.kilometros.CoberturaAdicionalService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.EndosoWSMidasAutosService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.BeanUtils;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class CoberturaAdicionalServiceImpl implements CoberturaAdicionalService {
	
	private static final Logger LOG = Logger.getLogger(CoberturaAdicionalServiceImpl.class);
	private static final String SEPARADOR = "-";
	private static final String SEPARADOR_COMA = ",";
	private static final String USUARIO_CREA_MODIFICA = "SISTEMAS";
	private static final String CLAVE_TIPO_DOCUMENTO = "16";
	private static final String POLIZA_VIGENTE = "VIGENTE";
	public static final String TITULO_CORREO="Ingreso de Solicitud Seguros Afirme Autos "; 
	public static final String SALUDO_CORREO="A quien corresponda.";
	private static final String TEXTO_SOLICITUD_CONTRATADA = "**SOLICITUD CONTRATADA**";
	private static final String TEXTO_ERROR_SOLICITUD = "**ERROR EN SOLICITUD**";
	private static final String TEXTO_SOLICITUD_COTIZADA = "**SOLICITUD COTIZADA**";
	private static final String TEXTO_SOLICITUD_NO_CONTRATADA = "**SOLICITUD NO CONTRATADA**";
	protected static final Short TIPO_SOLICITUD = (short) 2;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	private CoberturaAdicionalDao coberturaAdicionalDao;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	@EJB
	protected MailService mailService;
	
	@EJB
	private ComentariosService comentariosService;
	
    @EJB
    private EndosoWSMidasAutosService endosoWSMidasAutosService;

    @EJB
	private EndosoService endosoService;
    
	@Override
	public Map<String, Object> generaSolicitud(GeneraSolicitudRequest generaSolicitudRequest) {
		Map<String, Object> regresoSolicitud = new HashMap<String, Object>();
		GeneraSolicitudResponse generaSolicitudResponse = new GeneraSolicitudResponse();
		
		regresoSolicitud = cargaParametrosGenerales();
		
		ErrorBuilder errorBuilder = this.validaParametros(generaSolicitudRequest);
		
		if (errorBuilder.getFieldErrors().size() > 0) {
			throw new ApplicationException(errorBuilder);
		}
		
		BigDecimal idToPoliza = this.obtenerPolizaBase(generaSolicitudRequest.getNumeroPoliza());
		if (idToPoliza != null && idToPoliza.intValue() > 0){
			PolizaDTO polizaBase = entidadService.findById(PolizaDTO.class, idToPoliza);
			
			errorBuilder = this.validaParametros(polizaBase, generaSolicitudRequest, (List<String>)regresoSolicitud.get("paquetesCoberturaAdicional"),(List<String>)regresoSolicitud.get("formasPagoCoberturaAdicional"));
			if (errorBuilder.getFieldErrors().size() > 0) {
				throw new ApplicationException(errorBuilder);
			}
			
			SolicitudDTO solicitudBase = polizaBase.getCotizacionDTO().getSolicitudDTO();
			if (solicitudBase != null){
				SolicitudDTO nuevaSolicitud = this.inicializaSolicitud(solicitudBase, regresoSolicitud);
				BigDecimal idToSolicitud = (BigDecimal) entidadService.saveAndGetId(nuevaSolicitud);
				generaSolicitudResponse.setNumeroSolicitud(idToSolicitud);
				
				CoberturaAdicionalValoresDTO montosCoberturaAdicional = this.obtenerMontoCobertura(generaSolicitudRequest, idToPoliza);
				BigDecimal valorPrimaCobertura = montosCoberturaAdicional.getMontoPrimaKilometros().setScale(2, RoundingMode.HALF_UP);
				generaSolicitudResponse.setValorPrimaCobertura(valorPrimaCobertura);
				
				regresoSolicitud.put("generaSolicitudResponse", generaSolicitudResponse);
				
				SolicitudCoberturaAdicionalDTO solicitudCoberturaDTO = new SolicitudCoberturaAdicionalDTO();
				solicitudCoberturaDTO.setIdToSolicitud(idToSolicitud);
				solicitudCoberturaDTO.setIdToPoliza(idToPoliza);
				solicitudCoberturaDTO.setFechaHoraInicio(generaSolicitudRequest.getFechaHoraInicio());
				solicitudCoberturaDTO.setFechaHoraFin(generaSolicitudRequest.getFechaHoraFin());
				solicitudCoberturaDTO.setValorPrimaCobertura(valorPrimaCobertura);
				solicitudCoberturaDTO.setDistanciaTotal(generaSolicitudRequest.getDistanciaTotal());
				solicitudCoberturaDTO.setObservacionesInciso(this.observacionesCaraturaEndosoAdicional(generaSolicitudRequest));
				solicitudCoberturaDTO.setEstatus(Estatus.PENDIENTE.getValor());
				coberturaAdicionalDao.save(solicitudCoberturaDTO);
				
				//this.enviarCorreoSolicitud(regresoSolicitud.get("parametroCorreosNotificacion").toString(), generaSolicitudResponse, generaSolicitudRequest);
			}
		}else{
			errorBuilder.addFieldError("numeroPoliza", "El Número de Poliza especificado no se Encontró.");
			throw new ApplicationException(errorBuilder);
		}
		
		return regresoSolicitud;
	}
	
	@Override
	public String obtenerParametrosSolicitud(boolean conEncabezado, GeneraSolicitudRequest generaSolicitudRequest, GeneraSolicitudResponse generaSolicitudResponse){
		StringBuilder comentarios = new StringBuilder();
		
		String telefonoMonterrey = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PARAMETRO_TELEFONO_MONTERREY), true).getValor();
		String telefonoMexico = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PARAMETRO_TELEFONO_MEXICO), true).getValor();
		String telefonoRepublica = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PARAMETRO_TELEFONO_REPUBLICA), true).getValor();
		String separador = "\n";
		String parametros = null;
		
		if (conEncabezado){
			comentarios.append("Par\u00E1metros de la Solicitud:");
		}else{
			separador = "<br>";
		}
		
		comentarios.append(separador + "N\u00FAmero de P\u00F3liza: " + generaSolicitudRequest.getNumeroPoliza());
		comentarios.append(separador + "Distancia total: " + generaSolicitudRequest.getDistanciaTotal().toString() + " kil\u00F3metros");
		comentarios.append(separador + "Fecha y Hora de Inicio: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(generaSolicitudRequest.getFechaHoraInicio()));
		comentarios.append(separador + "Fecha y Hora de Fin: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(generaSolicitudRequest.getFechaHoraFin()));
		
		if (!conEncabezado){
			comentarios.append(separador + separador + "Tel\u00E9fonos para reporte de siniestros:");
			comentarios.append(separador + telefonoMonterrey);
			comentarios.append(separador + telefonoMexico);
			comentarios.append(separador + telefonoRepublica);
			
			parametros = String.format("La solicitud de Cobertura Adicional ha sido creada."+
					separador + "El n\u00famero de tr\u00e1mite creado es: [%s] esta es prima neta antes de impuestos: [%s]"+
					separador + separador + "Lo anterior obedece a su solicitud realizada a trav\u00E9s de su aplicaci\u00F3n m\u00F3vil y es complemento de su p\u00F3liza de seguros b\u00E1sica previamente contratada con nosotros, con el n\u00FAmero de solitud usted podr\u00E1 en caso de siniestro solicitar el servicio de ajuste."+
					separador + separador + "%s",
					generaSolicitudResponse.getNumeroSolicitud(),
					Utilerias.formatoMoneda(generaSolicitudResponse.getValorPrimaCobertura()),
					comentarios.toString());
		}else{
			comentarios.append(separador + separador + TEXTO_SOLICITUD_COTIZADA);
			parametros = comentarios.toString();
		}
		
		return parametros;
	}
	
	private Map<String, Object> cargaParametrosGenerales(){
		Map<String, Object> parametrosGenerales = new HashMap<String, Object>();
		String usuarioCreaModifica = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PARAMETRO_USUARIO_CREACION_MODIFICACION), true).getValor();
		String nombreUsuarioCreaModifica = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PARAMETRO_NOMBRE_USUARIO_CREACION_MODIFICACION), true).getValor();
		String parametroPaquetesDisponibles = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PARAMETRO_COBERTURAS_DISPONIBLES_INTER_X), true).getValor();
		String parametroFormaPagoDisponibles = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PARAMETRO_FORMA_PAGO_DISPONIBLES_INTER_X), true).getValor();
		String parametroCorreosNotificacion = parametroGeneralFacade.findById(new ParametroGeneralId(GRUPO_PARAMETRO_COBERTURA_ADICIONAL, PARAMETRO_CORREOS_NOTIFICACION_SOLICITUD), true).getValor();
		List<String> paquetesCoberturaAdicional = Arrays.asList(parametroPaquetesDisponibles.split(SEPARADOR_COMA));
		List<String> formasPagoCoberturaAdicional = Arrays.asList(parametroFormaPagoDisponibles.split(SEPARADOR_COMA));
		
		parametrosGenerales.put("usuarioCreaModifica", usuarioCreaModifica);
		parametrosGenerales.put("nombreUsuarioCreaModifica", nombreUsuarioCreaModifica);
		parametrosGenerales.put("paquetesCoberturaAdicional", paquetesCoberturaAdicional);
		parametrosGenerales.put("formasPagoCoberturaAdicional", formasPagoCoberturaAdicional);
		parametrosGenerales.put("parametroCorreosNotificacion", parametroCorreosNotificacion);
		
		return parametrosGenerales;
	}
	
	private ErrorBuilder validaParametros(PolizaDTO polizaDTO, GeneraSolicitudRequest generaSolicitudRequest, List<String> paquetesCoberturaAdicional, List<String> formasPagoCoberturaAdicional){
		ErrorBuilder errorBuilder = new ErrorBuilder();
		
		//Obtener el estatus de la Poliza
		if (!polizaDTO.getEstatus().equals(POLIZA_VIGENTE)){
			errorBuilder.addFieldError("numeroPoliza", String.format("El estatus de la Póliza especificada es [%s].", polizaDTO.getEstatus()));
		}
		
		//Se valida si hay solicitudes pendientes
		List<SolicitudCoberturaAdicionalDTO> solicitudesPoliza = coberturaAdicionalDao.findByPoliza(polizaDTO.getIdToPoliza());
		
		if (solicitudesPoliza.size() > 0){
			for(SolicitudCoberturaAdicionalDTO solicitudes :solicitudesPoliza){
				if(solicitudes.getEstatus().equals(Estatus.PENDIENTE.getValor())){
					errorBuilder.addFieldError("numeroPoliza", "Actualmente se cuenta con una solicitud pendiente para esta Póliza.");
					break;
				}
			}
		}
		
		//Se valida la vigencia de la Póliza respecto a los parametros de fecha
		if (generaSolicitudRequest.getFechaHoraInicio().before(polizaDTO.getCotizacionDTO().getFechaInicioVigencia())){
			errorBuilder.addFieldError("fechaHoraInicio", "La fecha Inicial es menor a la fecha de Inicio de Vigencia de la Póliza.");
		}
		
		if (generaSolicitudRequest.getFechaHoraInicio().after(polizaDTO.getCotizacionDTO().getFechaFinVigencia())){
			errorBuilder.addFieldError("fechaHoraInicio", "La fecha Inicial es mayor a la fecha de Fin de Vigencia de la Póliza.");
		}
		
		if (generaSolicitudRequest.getFechaHoraFin().before(polizaDTO.getCotizacionDTO().getFechaInicioVigencia())){
			errorBuilder.addFieldError("fechaHoraFin", "La fecha Final es menor a la fecha de inicio de Vigencia de la Póliza.");
		}
		
		if (generaSolicitudRequest.getFechaHoraFin().after(polizaDTO.getCotizacionDTO().getFechaFinVigencia())){
			errorBuilder.addFieldError("fechaHoraFin", "La fecha Final es mayor a la fecha de Fin de Vigencia de la Póliza.");
		}
			
		//Validar la forma de pago
		BigDecimal idIdFormaPago = polizaDTO.getCotizacionDTO().getIdFormaPago();

		if (!formasPagoCoberturaAdicional.contains(idIdFormaPago.toString())){
			
			errorBuilder.addFieldError("numeroPoliza", String.format("La forma de pago [%s] de la Póliza no esta disponible para este servicio", idIdFormaPago));
		}
		
		try{
			//Verificar el paquete de la Poliza
			List<BitemporalInciso> bitemporalIncisos = (List<BitemporalInciso>)(polizaDTO.getCotizacionDTO().getCotizacionContinuity().getIncisoContinuities().get(TimeUtils.getDateTime(generaSolicitudRequest.getFechaHoraInicio())));
			if (bitemporalIncisos != null && bitemporalIncisos.size() > 0)
			{
				AutoInciso autoInciso = bitemporalIncisos.get(0).getContinuity().getAutoIncisoContinuity().getIncisoAutos().get(TimeUtils.getDateTime(generaSolicitudRequest.getFechaHoraInicio())).getValue();
				
				Long idPaquete = autoInciso.getPaquete().getId();
				String nombrePaquete = autoInciso.getPaquete().getDescripcion();
				if (!paquetesCoberturaAdicional.contains(idPaquete.toString())){
					errorBuilder.addFieldError("numeroPoliza", String.format("El paquete [%s] de la Póliza no está disponible para la Cobertura Adicional", nombrePaquete));
				}
			}else{
				errorBuilder.addFieldError("numeroPoliza", "No se encontraron incisos Vigentes con las Fechas proporcionadas");
			}
		}catch(Exception ex){
			LOG.error("CoberturaAdicionalServiceImpl.validaParametros, Error al buscar la informacion del inciso.", ex);
			errorBuilder.addFieldError("numeroPoliza", "Error al buscar la informacion del inciso.");
		}
		
		return errorBuilder;
	}
	
	private SolicitudDTO inicializaSolicitud(SolicitudDTO solicitudDTOBase, Map<String, Object> parametrosGenerales){
		SolicitudDTO solicitudDTO = new SolicitudDTO();
		
		BeanUtils.copyProperties(solicitudDTOBase, solicitudDTO);
		solicitudDTO.setClaveEstatus(SolicitudDTO.Estatus.PROCESO.getEstatus());
		solicitudDTO.setIdToSolicitud(null);
		solicitudDTO.setFechaCreacion(new Date());
		solicitudDTO.setNombrePersona(parametrosGenerales.get("nombreUsuarioCreaModifica").toString());
		solicitudDTO.setApellidoMaterno(SEPARADOR_COMA);
		solicitudDTO.setApellidoPaterno(SEPARADOR_COMA);
		solicitudDTO.setCodigoUsuarioCreacion(parametrosGenerales.get("usuarioCreaModifica").toString());
		solicitudDTO.setCodigoUsuarioModificacion(parametrosGenerales.get("usuarioCreaModifica").toString());
		solicitudDTO.setClaveTipoSolicitud(new Short("2"));
		solicitudDTO.setClaveTipoEndoso(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS));
		
		
		return solicitudDTO;
	}
	
	private ErrorBuilder validaParametros(GeneraSolicitudRequest generaSolicitudRequest){
		ErrorBuilder errorBuilder = new ErrorBuilder();
		
		//Se valida el numero de Póliza
		if (StringUtils.isBlank(generaSolicitudRequest.getNumeroPoliza())){
			errorBuilder.addFieldError("numeroPoliza", "Parametro requerido.");
		}else{
			String[] estructuraNumeroPoliza = generaSolicitudRequest.getNumeroPoliza().split(SEPARADOR, -1);
			if (estructuraNumeroPoliza.length !=3 || StringUtils.isBlank(estructuraNumeroPoliza[0]) || StringUtils.isBlank(estructuraNumeroPoliza[1]) || StringUtils.isBlank(estructuraNumeroPoliza[2])){
				errorBuilder.addFieldError("numeroPoliza", "Se ha especificado un formato invalido.");
			}
		}
		
		//Se valida la distancia Total
		Integer validaDistaciaTotal = generaSolicitudRequest.getDistanciaTotal().compareTo(BigDecimal.ZERO);
		if (validaDistaciaTotal == 0 || validaDistaciaTotal == -1){
			errorBuilder.addFieldError("distanciaTotal", "Se requiere un valor mayor a Cero.");
		}
		
		//Se validan las fechas
		if (generaSolicitudRequest.getFechaHoraInicio().equals(generaSolicitudRequest.getFechaHoraFin())){
			errorBuilder.addFieldError("fechaHoraInicio", "La fecha Inicial es igual a la Final.");
		}else if (generaSolicitudRequest.getFechaHoraInicio().after(generaSolicitudRequest.getFechaHoraFin())){
			errorBuilder.addFieldError("fechaHoraInicio", "La fecha Inicial es mayor a la Final.");
		}
		
		return errorBuilder;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private BigDecimal obtenerPolizaBase(String numeroPolizaFormateada){
		String[] partesPoliza = numeroPolizaFormateada.split(SEPARADOR);
		final String queryString = " SELECT MIDAS.PKG_COBERTURA_ADICIONAL.fnObtienePolizaBase(?1, ?2, ?3) AS IDTOSOLICITUD FROM DUAL ";
		final Query query = entityManager.createNativeQuery(queryString);
		
		query.setParameter(1, partesPoliza[0]);
		query.setParameter(2, partesPoliza[1]);
		query.setParameter(3, partesPoliza[2]);
		return (BigDecimal) query.getSingleResult();
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private CoberturaAdicionalValoresDTO obtenerMontoCobertura(GeneraSolicitudRequest generaSolicitudRequest, BigDecimal idToPoliza){
		String spName = "MIDAS.PKG_COBERTURA_ADICIONAL.spAUT_CalculaRiesgo_DM";
		
		String[] atributosDTO = { "totalKilometros", "totalMinutos", "montoPrimaKilometros", "montoPrimaMinutos", "montoAnualCobertura" };
		String[] columnasCursor = { "totalKilometros", "totalMinutos", "montoPrimaKilometros", "montoPrimaMinutos", "montoAnualCobertura" };
		
		try{
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pidToPoliza", idToPoliza);
			storedHelper.estableceParametro("pdistanciaTotal", generaSolicitudRequest.getDistanciaTotal());
			storedHelper.estableceParametro("pfechaHoraInicio", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(generaSolicitudRequest.getFechaHoraInicio()));
			storedHelper.estableceParametro("fechaHoraFin", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(generaSolicitudRequest.getFechaHoraFin()));
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.coberturaadicional.CoberturaAdicionalValoresDTO", atributosDTO, columnasCursor);
			
			CoberturaAdicionalValoresDTO coberturaAdicionalValoresDTO = (CoberturaAdicionalValoresDTO)storedHelper.obtieneResultadoSencillo();
			
			return coberturaAdicionalValoresDTO;
		}catch(Exception ex){
			LOG.error("CoberturaAdicionalServiceImpl.obtenerMontoCobertura, error al obtener las primas. " , ex);
			return null;
		}
	}
	
	private String observacionesCaraturaEndosoAdicional(GeneraSolicitudRequest generaSolicitudRequest){
		StringBuilder comentarios = new StringBuilder();
		String separador = "\n";
		
		comentarios.append(separador + "Distancia total: " + generaSolicitudRequest.getDistanciaTotal().toString() + " kil\u00F3metros ");
		comentarios.append(separador + " Fecha y Hora de Inicio: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(generaSolicitudRequest.getFechaHoraInicio() ));
		comentarios.append(separador + " Fecha y Hora de Fin: " + new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(generaSolicitudRequest.getFechaHoraFin()));

		return comentarios.toString();
	}
	
	private void enviarCorreoSolicitud(String cadenaCorreos, GeneraSolicitudResponse generaSolicitudResponse, GeneraSolicitudRequest generaSolicitudRequest) {
		if (cadenaCorreos != null && cadenaCorreos.trim().length() > 0) {
			String msg = this.obtenerParametrosSolicitud(false, generaSolicitudRequest, generaSolicitudResponse);
			
			List<String> correos = new ArrayList<String>();
			for(String correo: cadenaCorreos.split(";")){
				correos.add(correo);
			}
			
			mailService.sendMail(correos,
					TITULO_CORREO + generaSolicitudResponse.getNumeroSolicitud(),
					msg, null,
					TITULO_CORREO + generaSolicitudResponse.getNumeroSolicitud(),
					SALUDO_CORREO);
		}
	}
	
	@Override
	public String contrataSolicitud(BigDecimal numeroSolicitud, boolean contratar){
		LOG.trace(">> contrataSolicitud()");
		
		SolicitudCoberturaAdicionalDTO solicitudAdicional = coberturaAdicionalDao.findBySolicitud(numeroSolicitud);
		BigDecimal idToPoliza = solicitudAdicional.getIdToPoliza();
		List<SolicitudCoberturaAdicionalDTO> solicitudesPoliza = coberturaAdicionalDao.findByPoliza(idToPoliza);
		Short numeroUltimoEndoso = endosoWSMidasAutosService.getUltimoEndoso(idToPoliza).getId().getNumeroEndoso();

		SolicitudDTO solicitudDTO = entidadService.findById(SolicitudDTO.class, numeroSolicitud);
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, idToPoliza);
		Collection<EndosoDTO> listado = new ArrayList<EndosoDTO>();
		listado = endosoService.findForAdjust(poliza.getCotizacionDTO().getIdToCotizacion().longValue());

		EndosoAdicionalDTO dto = new EndosoAdicionalDTO();
		BigDecimal primaEndoso = solicitudAdicional.getValorPrimaCobertura();
		BigDecimal primaIgualar = new BigDecimal(0);
		
		String comentarios = TEXTO_SOLICITUD_NO_CONTRATADA;
		String regreso = null;
		Short numeroEndoso;
		
		if (solicitudDTO != null){
			if(solicitudAdicional.getEstatus().equals(Estatus.PENDIENTE.getValor())){
				if (contratar){
			        	try {
			        		
			        		Integer decideEndoso = 0;
			        		for(SolicitudCoberturaAdicionalDTO solicitudes :solicitudesPoliza){
			        			if(solicitudes.getNumeroEndoso()!=null){
			        				decideEndoso = 1;
			        				break;
			        			}
			        		}
	
			        		if(decideEndoso==0){
			        			LOG.info("Endoso Movimiento" + idToPoliza);
				        		dto.setClaveTipoEndoso(SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS);
				        		primaIgualar = primaEndoso;
				        		
			        		}else{
			        			LOG.info("Ajuste de Prima" + idToPoliza);
			        			dto.setClaveTipoEndoso(SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA);
			        			dto.setNumeroEndoso(numeroUltimoEndoso);
			        			primaIgualar = this.sumaPrimaEndosos(listado,numeroUltimoEndoso,primaEndoso);
			        		}
	
			        		dto.setNumeroPoliza(poliza.getNumeroPoliza().toString());
			        		dto.setFechaInicioVigenciaEndoso(solicitudAdicional.getFechaHoraInicio());
			        		dto.setFechaFinVigenciaEndoso(solicitudAdicional.getFechaHoraFin());
			        		dto.setValorprima(primaIgualar);
			        		dto.setDistanciaTotal(solicitudAdicional.getDistanciaTotal());
			        		dto.setObservacionesInciso(solicitudAdicional.getObservacionesInciso());
							
			        		numeroEndoso = endosoWSMidasAutosService.cotizar(dto,poliza);
			        		
			        		solicitudAdicional.setNumeroEndoso(numeroEndoso);
			        		solicitudAdicional.setEstatus(Estatus.CONTRATADO.getValor());
			        		
			        		coberturaAdicionalDao.update(solicitudAdicional);
			        		
			        		comentarios = TEXTO_SOLICITUD_CONTRATADA;
							
						} catch (Exception e) {
							
							LOG.info("Error en Endoso Movimiento" + idToPoliza);
							comentarios = TEXTO_ERROR_SOLICITUD;
						}
				}else{
	        		solicitudAdicional.setEstatus(Estatus.NOCONTRATADO.getValor());
	        		coberturaAdicionalDao.update(solicitudAdicional);
				}
				
				Comentario comentario = new Comentario();
				comentario.setSolicitudDTO(solicitudDTO);
				comentario.setCodigoUsuarioCreacion(solicitudDTO.getCodigoUsuarioCreacion());
				comentario.setValor(comentariosService.validaComentarioSolicitudCotizacion(comentarios, TIPO_COMENTARIO).substring(4));
				comentariosService.guardarComentarioCoberturaAdicional(comentario);
				regreso = "{\"numeroSolicitud\":\"" + numeroSolicitud + "\", \"contratada\":\"" + contratar + "\", \"mensaje\":\"" + comentarios + "\"}";
				
			}else{
				if(solicitudAdicional.getEstatus().equals(Estatus.CONTRATADO.getValor())){
					regreso = "{\"mensaje\":\"Solicitud anteriormente contratada.\"}";
				}else{
					regreso = "{\"mensaje\":\"Solicitud anteriormente rechazada.\"}";

				}             
			}
		}else
		{
			regreso = "{\"mensaje\":\"No se encontr\u00f3 la Solicitud especificada.\"}";
		}
		LOG.trace("<< contrataSolicitud()");

		return regreso;
	}

	public BigDecimal sumaPrimaEndosos(Collection<EndosoDTO> listadoa,Short numeroUltimoEndoso,BigDecimal primaEndoso){
		LOG.trace(">> contrataSolicitud()");
		BigDecimal primaTotalIgualar = new BigDecimal(0);
		
		for(EndosoDTO endosos :listadoa){
			if(endosos.getId().getNumeroEndoso()==numeroUltimoEndoso){
				primaTotalIgualar = primaEndoso.add(new BigDecimal(endosos.getValorPrimaTotal()));
				break;
			}
		}
		LOG.trace("<< sumaPrimaEndosos()");
		return primaTotalIgualar;		
	}

	public CoberturaAdicionalDao getCoberturaAdicionalDao() {
		return coberturaAdicionalDao;
	}

	@EJB
	public void setCoberturaAdicionalDao(CoberturaAdicionalDao coberturaAdicionalDao) {
		this.coberturaAdicionalDao = coberturaAdicionalDao;
	}

}