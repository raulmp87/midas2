package mx.com.afirme.midas2.service.impl.endoso.cotizacion.auto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas2.dao.endoso.cotizacion.auto.CotizacionEndosoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.CotizacionBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;

import org.apache.commons.lang.builder.CompareToBuilder;
import org.joda.time.DateTime;

@Stateless
public class CotizacionEndosoServiceImpl implements CotizacionEndosoService{
	
	private CotizacionEndosoDao cotizacionEndosoDao;
	private EntidadService entidadService;
	private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
	private CotizacionBitemporalService cotizacionBitemporalService;
	private ImpresionEndosoService impresionEndosoService;
	private EntidadContinuityService entidadContinuityService;
	private CatalogoValorFijoFacadeRemote catalogValorFijoService;
	
	@EJB
	private IncisoViewService incisoViewService ;
	
	/**
	 * Listado de Cotizaciones de Endoso
	 * Desde la Bandeja de Cotizaciones: Autos/Cotizaciones/Endosos
	 * @param cotizacionEndosoDTO objeto con filtros
	 */
	@Override
	public List<CotizacionEndosoDTO> buscarCotizacionesEndosoFiltrado(
			CotizacionEndosoDTO cotizacionEndosoDTO) {
		
		if (cotizacionEndosoDTO == null){
			return null;
		}
		
		List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>();
		
		listaCotizacionesEndoso.addAll(
				convertCotizacionesToCotizacionEndosoDTOList(this.buscarCotizacionFiltrado(cotizacionEndosoDTO)));
		
		return listaCotizacionesEndoso;
	}
	
	/**
	 * Total de Cotizaciones de Endoso
	 * Desde la Bandeja de Cotizaciones: Autos/Cotizaciones/Endosos
	 * @param cotizacionEndosoDTO objeto con filtros
	 */
	@Override
	public Long obtenerTotalPaginacionCotizacionesEndoso(CotizacionEndosoDTO cotizacionEndosoDTO) {
		Long total = 0L;
		total += cotizacionEndosoDao.obtenerTotalPaginacionCotizaciones(cotizacionEndosoDTO);
		
		return total;
	}
	
	/**
	 * Listado de Cotizaciones de Endoso de una poliza
	 * Desde Listado de Cotizaciones de Poliza: Autos/Emision/Consulta Por Poliza
	 * @param cotizacionEndosoDTO objeto con filtro Poliza y Cotizacion
	 */
	public List<CotizacionEndosoDTO> buscarCotizacionesEndosoPoliza(CotizacionEndosoDTO cotizacionEndosoDTO){
		if (cotizacionEndosoDTO == null){
			return null;
		}
		
		List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>();
		
		listaCotizacionesEndoso.addAll(
				convertCotizacionesPolizaToCotizacionEndosoDTOList(this.buscarCotizacionPorPoliza(cotizacionEndosoDTO)));			
		
		return listaCotizacionesEndoso;
	}
	
	/**
	 * Total de Cotizaciones de Endoso de una poliza
	 * Desde Listado de Cotizaciones de Poliza: Autos/Emision/Consulta Por Poliza
	 * @param cotizacionEndosoDTO objeto con filtro Poliza y Cotizacion
	 */
	public Long obtenerTotalPaginacionCotizacionesEndosoPoliza(CotizacionEndosoDTO cotizacionEndosoDTO){
		Long total = 0L;
		total += cotizacionEndosoDao.obtenerTotalPaginacionCotizacionesEndosoPoliza(cotizacionEndosoDTO);
		return total;
	}
	
	/**
	 * Se prepara la lista de objetos CotizacionEndosoDTO para pintar {jsp/endosos/solicitudEndoso/definirSolicitudEndosoGrid.jsp}
	 * @param listaCotizaciones
	 * @return
	 */
	private  List<CotizacionEndosoDTO> convertCotizacionesPolizaToCotizacionEndosoDTOList(List<ControlEndosoCot> listaCotizaciones){
		List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>();
		if (listaCotizaciones != null) {
			Iterator<ControlEndosoCot> itListaCotizacion = listaCotizaciones.iterator();
			ControlEndosoCot control;
			CotizacionEndosoDTO cotizacionEndoso;
			PolizaDTO poliza;
			EndosoDTO endosoRs;
			while (itListaCotizacion.hasNext()) {
				control = itListaCotizacion.next();
				cotizacionEndoso = new CotizacionEndosoDTO();
			
				cotizacionEndoso.setEstatusCotizacionEndoso(control.getClaveEstatusCot().shortValue());	
				cotizacionEndoso.setIdTipoEndoso(control.getSolicitud().getClaveTipoEndoso().shortValue());
				cotizacionEndoso.setFechaInicioVigencia(control.getSolicitud().getFechaInicioVigenciaEndoso());
				cotizacionEndoso.setFechaEmisionEndoso(control.getRecordFrom());
				cotizacionEndoso.setEstatusCotizacionEndoso(control.getClaveEstatusCot().shortValue());
				cotizacionEndoso.setIdTipoEndoso(control.getSolicitud().getClaveTipoEndoso().shortValue());
				cotizacionEndoso.setFechaCotizacionEndoso(control.getValidFrom());
				cotizacionEndoso.setFechaEmisionEndosoMidas(control.getRecordFrom());
				cotizacionEndoso.setNumeroCotizacionEndoso(new BigDecimal(control.getCotizacionId()));
				
				poliza =  entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", control.getCotizacionId()).get(0);
				cotizacionEndoso.setIdPoliza(poliza.getIdToPoliza());
				cotizacionEndoso.setNumeroPolizaFormateado(poliza.getNumeroPolizaFormateada());
				
				endosoRs = getEndoso(poliza, control);
				if (endosoRs != null) {
					cotizacionEndoso.setNumeroEndoso(endosoRs.getId().getNumeroEndoso().toString());
					cotizacionEndoso.setFechaEmisionEndoso(endosoRs.getFechaCreacion());
					cotizacionEndoso.setPrimaEndoso(endosoRs.getValorPrimaTotal());
					cotizacionEndoso.setClaveTipoEndoso(endosoRs.getClaveTipoEndoso());
				} if (control.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA) {
					BigDecimal valorPrimaTotalEmitida = BigDecimal.ZERO;
					
					//Se obtiene el total de primas emitido del endoso a ajustar
					Map<String,Object> properties = new HashMap<String,Object>();
					properties.put("idToCotizacion", control.getCotizacionId());
					properties.put("id.numeroEndoso", control.getNumeroEndosoAjusta());
					
					List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, properties);
					
					if (endosos != null && endosos.size() > 0) {
						valorPrimaTotalEmitida = new BigDecimal(endosos.get(0).getValorPrimaTotal());
					}
					
					//La prima total que se emite para que se ajuste a la prima a la que quiere llegar el usuario
					cotizacionEndoso.setPrimaEndoso((control.getPrimaTotalIgualar().subtract(valorPrimaTotalEmitida)).doubleValue());
				}
				
				if(cotizacionEndoso.getEstatusCotizacionEndoso() == CotizacionDTO.ESTATUS_COT_TERMINADA 
						|| cotizacionEndoso.getEstatusCotizacionEndoso() == CotizacionDTO.ESTATUS_COT_EN_PROCESO ){
					BitemporalCotizacion cotizacion = this.cotizacionBitemporalService.getBitemporalCotizacion(
							new BigDecimal(control.getCotizacionId()), 
							new DateTime(control.getValidFrom()));
					cotizacionEndoso.setContinuityId(cotizacion.getContinuity().getId());
				}
				
				cotizacionEndoso.setCodigoUsuarioCreacion(control.getSolicitud().getCodigoUsuarioCreacion());
				
				listaCotizacionesEndoso.add(cotizacionEndoso);
			}
		}
		return listaCotizacionesEndoso;
	}
	
	private List<CotizacionEndosoDTO> convertCotizacionesToCotizacionEndosoDTOList(List<Object[]> listaCotizaciones) {
		
		Object[] resultado = null;
		CotizacionEndosoDTO cotizacionEndoso = null;
		PolizaDTO poliza = null;
		EndosoDTO endoso = new EndosoDTO();
		List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>();
				
		if (listaCotizaciones != null) {
		
			Iterator<Object[]> itListaCotizacion = listaCotizaciones.iterator();
			while (itListaCotizacion.hasNext()) {
				resultado = itListaCotizacion.next();
				ControlEndosoCot control = (ControlEndosoCot)resultado[0];
				BitemporalCotizacion cotizacion = (BitemporalCotizacion)resultado[1];
				EndosoDTO endosoRs = null;
				cotizacionEndoso = new CotizacionEndosoDTO();
				
				poliza =  entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", cotizacion.getContinuity().getNumero()).get(0);
				
				cotizacionEndoso.setContinuityId(cotizacion.getContinuity().getId());
				cotizacionEndoso.setNumeroCotizacionEndoso(new BigDecimal(cotizacion.getContinuity().getNumero()));
				cotizacionEndoso.setNumeroPoliza(poliza.getNumeroPoliza());
				cotizacionEndoso.setIdPoliza(poliza.getIdToPoliza());
				cotizacionEndoso.setNumeroPolizaFormateado(poliza.getNumeroPolizaFormateada());
				cotizacionEndoso.setNombreAsegurado(cotizacion.getValue().getNombreContratante());		
				cotizacionEndoso.setIdTipoEndoso(control.getSolicitud().getClaveTipoEndoso().shortValue());
				cotizacionEndoso.setSolicitudId(control.getSolicitud().getIdToSolicitud());
				cotizacionEndoso.setFechaCotizacionEndoso(control.getValidFrom());
				cotizacionEndoso.setEstatusCotizacionEndoso(control.getClaveEstatusCot().shortValue());
				cotizacionEndoso.setFechaInicioVigencia(control.getSolicitud().getFechaInicioVigenciaEndoso());
				cotizacionEndoso.setFechaEmisionEndoso(control.getRecordFrom());
				cotizacionEndoso.setFechaEmisionEndosoMidas(control.getRecordFrom());
				
				endosoRs = getEndoso(poliza, control);
				if (endosoRs != null) {
					cotizacionEndoso.setNumeroEndoso(endosoRs.getId().getNumeroEndoso().toString());
					cotizacionEndoso.setFechaEmisionEndoso(endosoRs.getFechaCreacion());
				}			
				
				if (control.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_AJUSTE_PRIMA) {
					BigDecimal valorPrimaTotalEmitida = BigDecimal.ZERO;
					
					//Se obtiene el total de primas emitido del endoso a ajustar
					Map<String,Object> properties = new HashMap<String,Object>();
					properties.put("idToCotizacion", control.getCotizacionId());
					properties.put("id.numeroEndoso", control.getNumeroEndosoAjusta());
					
					List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, properties);
					
					if (endosos != null && endosos.size() > 0) {
						valorPrimaTotalEmitida = new BigDecimal(endosos.get(0).getValorPrimaTotal());
					}
					
					//La prima total que se emite para que se ajuste a la prima a la que quiere llegar el usuario
					cotizacionEndoso.setPrimaEndoso((control.getPrimaTotalIgualar().subtract(valorPrimaTotalEmitida)).doubleValue());
					
					
				} else if (cotizacion.getValue().getValorPrimaTotalEndoso() != null 
						&& esEndosoConPrima(control.getSolicitud().getClaveTipoEndoso().shortValue() )) {
					if ((control.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_ENDOSO
							|| control.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_ENDOSO)
							&& endosoRs != null ) {
						
						ControlEndosoCot  controlEndosoCancelado = getControlFromEndoso(poliza, endosoRs.getCancela() );
							if (controlEndosoCancelado != null 
									&& esEndosoConPrima(controlEndosoCancelado.getSolicitud().getClaveTipoEndoso().shortValue()))  {
								cotizacionEndoso.setPrimaEndoso(cotizacion.getValue().getValorPrimaTotalEndoso().doubleValue());
							}
					} else {
						cotizacionEndoso.setPrimaEndoso(cotizacion.getValue().getValorPrimaTotalEndoso().doubleValue());
					}
					
				}
				
				endoso.setClaveTipoEndoso(cotizacionEndoso.getIdTipoEndoso());
				endoso.setValorPrimaTotal(cotizacionEndoso.getPrimaEndoso() != null ? cotizacionEndoso.getPrimaEndoso():0D);
				cotizacionEndoso.setClaveTipoEndoso(emisionEndosoBitemporalService.calculaTipoEndoso(endoso));		
								
				listaCotizacionesEndoso.add(cotizacionEndoso);
			}			
		}
		
		return listaCotizacionesEndoso;	
	}
	
	private EndosoDTO getEndoso(PolizaDTO poliza, ControlEndosoCot control) {
		EndosoDTO endoso = null;
        Map<String, Object> filtros = new HashMap<String, Object>();
       try{
       filtros.put("id.idToPoliza", poliza.getIdToPoliza());
       filtros.put("idToCotizacion", poliza.getCotizacionDTO().getIdToCotizacion());
       filtros.put("recordFrom", control.getRecordFrom());
       List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, filtros);
       if (endosos != null && !endosos.isEmpty()) {
       		endoso = endosos.get(0);
       }
       }catch(Exception e){
    	   e.printStackTrace();
       }
       return endoso;
	}
	
private ControlEndosoCot getControlFromEndoso(PolizaDTO poliza, Short numeroEndoso) {
        
        ControlEndosoCot controlEndoso = null;
        Map<String, Object> filtros = new HashMap<String, Object>();
        
        filtros.put("id.numeroEndoso", numeroEndoso);
        filtros.put("idToCotizacion", poliza.getCotizacionDTO().getIdToCotizacion());
        List<EndosoDTO> endosos = entidadService.findByProperties(EndosoDTO.class, filtros);
        
        if (endosos != null && endosos.size() > 0) {
              filtros = new HashMap<String, Object>();
              filtros.put("cotizacionId", poliza.getCotizacionDTO().getIdToCotizacion());
              filtros.put("recordFrom", endosos.get(0).getRecordFrom());
              
              List<ControlEndosoCot> controles = entidadService.findByProperties(ControlEndosoCot.class, filtros);
              
              if (controles != null && controles.size() > 0) {
                    controlEndoso = controles.get(0);
              }
        }        
        return controlEndoso;        
	}

	private boolean esEndosoConPrima(Short tipoEndoso)  {
		if (tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_AGENTE
				&& tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS
				&& tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO
				&& tipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_TEXTO) {
			return true;
		}
		return false;	
	}
	
	private List<Object[]> buscarCotizacionFiltrado(CotizacionEndosoDTO cotizacionEndosoDTO) {		
		return cotizacionEndosoDao.buscarCotizacionFiltrado(cotizacionEndosoDTO);
	}
	
	private List<ControlEndosoCot> buscarCotizacionPorPoliza(CotizacionEndosoDTO cotizacionEndosoDTO) {		
		return cotizacionEndosoDao.buscarCotizacionesEndosoPoliza(cotizacionEndosoDTO);
	}
	
	@Override
	public List<CotizacionEndosoDTO> obtenerEndosoInciso(Long incisoContinuityId) {
		return this.obtenerEndosoInciso(incisoContinuityId, null , null, null);
	}
	
	
	@Override
	public List<CotizacionEndosoDTO> obtenerEndosoInciso(Long incisoContinuityId, Date fechaBusquedaInicial,Date fechaBusquedaFinal, List<String> usuarios) {
		EndosoDTO endosoRs; 

		List<CotizacionEndosoDTO> lCotizacionDto = new ArrayList<CotizacionEndosoDTO>();
		IncisoContinuity incisoContinuity        = entidadContinuityService.findContinuityByKey(IncisoContinuity.class , incisoContinuityId);
		Integer cotizacionId                     = incisoContinuity.getCotizacionContinuity().getNumero();

		// # OBTIENE LOS ENDOSOS AL INCISO
		List<ControlEndosoCot> lCotizacionEndoso = this.cotizacionEndosoDao.obtenerEndosoInciso(new Long(cotizacionId), incisoContinuityId, fechaBusquedaInicial , fechaBusquedaFinal, usuarios);
		
		String descripcionMovEndoso;
		
		if( !lCotizacionEndoso.isEmpty() ){
			
			for( ControlEndosoCot lCot : lCotizacionEndoso ){
				
				BitemporalInciso bitemporalInciso = incisoContinuity.getIncisos().get(new DateTime(lCot.getValidFrom()), new DateTime(lCot.getRecordFrom()));
				
				BitemporalCotizacion bitemporalCotizacion = incisoContinuity.getCotizacionContinuity().getCotizaciones().get(new DateTime(lCot.getValidFrom()));
				
				PolizaDTO poliza =  entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", new BigDecimal( cotizacionId ) ).get(0);
			
				descripcionMovEndoso = this.impresionEndosoService.obtenerTextoEndoso( 
						lCot, 
						poliza, 
						bitemporalCotizacion, 
						new DateTime(lCot.getValidFrom() ) , 
						new DateTime(lCot.getRecordFrom()) , 
						bitemporalInciso
				);
				
				// # LLENAR DTO QUE SE MUESTRA EN VISTA
				
				CotizacionEndosoDTO cotizacionEndosoDto = new CotizacionEndosoDTO();

				cotizacionEndosoDto.setFechaInicioEndoso	  ( lCot.getSolicitud().getFechaCreacion() );
				cotizacionEndosoDto.setFechaInicioVigencia    ( lCot.getSolicitud().getFechaInicioVigenciaEndoso() );
				cotizacionEndosoDto.setDescripcionMovimiento  ( descripcionMovEndoso );
				cotizacionEndosoDto.setIdTipoEndoso           ( Short.valueOf( lCot.getSolicitud().getClaveTipoEndoso().toString() ) );
				cotizacionEndosoDto.setFechaInicioVigencia    ( lCot.getSolicitud().getFechaInicioVigenciaEndoso());
				cotizacionEndosoDto.setDescripcionMotivoEndoso( this.descripcionMotivoEndoso( Short.valueOf( lCot.getSolicitud().getClaveTipoEndoso().toString() ) ) );
				cotizacionEndosoDto.setCodigoUsuarioCreacion  ( lCot.getSolicitud().getCodigoUsuarioCreacion() );
				cotizacionEndosoDto.setFechaInicioEndoso	  ( lCot.getSolicitud().getFechaInicioVigenciaEndoso() ); 
				
				
				endosoRs = getEndoso(poliza, lCot);
				if (endosoRs != null) {
					cotizacionEndosoDto.setNumeroEndoso          ( endosoRs.getId().getNumeroEndoso().toString() );
					cotizacionEndosoDto.setPrimaEndoso           ( endosoRs.getValorPrimaTotal() );
					cotizacionEndosoDto.setClaveTipoEndoso       ( endosoRs.getClaveTipoEndoso() );
					cotizacionEndosoDto.setFechaEmisionEndoso    ( endosoRs.getFechaCreacion() );
					cotizacionEndosoDto.setDescripcionTipoEndoso ( this.especificaTipoEndoso( endosoRs.getValorPrimaTotal() ) );
				}
				
				lCotizacionDto.add(cotizacionEndosoDto);
				
			}
			

			Collections.sort(lCotizacionDto, new Comparator<CotizacionEndosoDTO>() {  
			    @Override  
			    public int compare(CotizacionEndosoDTO p1, CotizacionEndosoDTO p2) {  
			        return new CompareToBuilder().append(p1.getNumeroEndoso(), p2.getNumeroEndoso()).toComparison();  
			    }
			});
			
		}

		
		return lCotizacionDto;
	}
	
	private String especificaTipoEndoso(Double cantidad){
		String tipo = "";
		
		if(cantidad < 0 ){
			tipo += "D";
		}else if(cantidad > 0){
			tipo += "A";
		}else if(cantidad == 0d){
			tipo += "B";
		}
		
		return tipo;
	}
	
	/***
	 * Obtiene el catalogo generico para los valores 332 y busca el texto equivalente al ID que pasa por parámetro
	 * @return
	 */
	private String descripcionMotivoEndoso(Short claveTipoEndoso ){
		
		String valor = this.catalogValorFijoService.getDescripcionCatalogoValorFijo(332, claveTipoEndoso);
		
		if( valor == "" || valor == null ){
			return "-";
		}else{
			return valor;
		}

	}


	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCotizacionEndosoDao(CotizacionEndosoDao cotizacionEndosoDao) {
		this.cotizacionEndosoDao = cotizacionEndosoDao;
	}
	
	@EJB
	public void setEmisionEndosoBitemporalService(
			EmisionEndosoBitemporalService emisionEndosoBitemporalService) {
		this.emisionEndosoBitemporalService = emisionEndosoBitemporalService;
	}

	@EJB
	public void setCotizacionBitemporalService(
			CotizacionBitemporalService cotizacionBitemporalService) {
		this.cotizacionBitemporalService = cotizacionBitemporalService;
	}

	@EJB
	public void setImpresionEndosoService(
			ImpresionEndosoService impresionEndosoService) {
		this.impresionEndosoService = impresionEndosoService;
	}

	@EJB
	public void setEntidadContinuityService(
			EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@EJB
	public void setCatalogValorFijoService(
			CatalogoValorFijoFacadeRemote catalogValorFijoService) {
		this.catalogValorFijoService = catalogValorFijoService;
	}


	
}