package mx.com.afirme.midas2.service.impl.negocio.impresion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.impresion.NegocioUsuarioEdicionImpresionDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.impresion.NegocioUsuarioEdicionImpresion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.impresion.NegocioUsuarioEdicionImpresionService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

@Stateless
public class NegocioUsuarioEdicionImpresionServiceImpl implements NegocioUsuarioEdicionImpresionService{

	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;

	@EJB
	public EntidadService entidadService;

	@EJB
	public EntidadDao entidadDao;
	
	@EJB
	public NegocioUsuarioEdicionImpresionDao negocioUsuarioEdicionImpresionDao;
	
	
	@Override
	public List<NegocioUsuarioEdicionImpresion> obtenerUsuariosDisponibles(
			Long idToNegocio) {
		return negocioUsuarioEdicionImpresionDao.obtenerUsuariosDisponibles(idToNegocio);
	}

	@Override
	public List<NegocioUsuarioEdicionImpresion> obtenerUsuariosAsociados(
			Long idToNegocio) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("negocio.idToNegocio", idToNegocio);
		return entidadService.findByPropertiesWithOrder(NegocioUsuarioEdicionImpresion.class, params, "claveUsuario");
	}

	@Override
	public void relacionar(String accion,
			NegocioUsuarioEdicionImpresion negocioUsuarioImpresion) {
		entidadService.executeActionGrid(accion, negocioUsuarioImpresion);
	}

	@Override
	public void asociarTodas(Long idToNegocio) {
		List<NegocioUsuarioEdicionImpresion> listaDisponibles = obtenerUsuariosDisponibles(idToNegocio);
		String usuario = usuarioService.getUsuarioActual().getNombreUsuario();
		Negocio negocio = new Negocio();
		negocio.setIdToNegocio(idToNegocio);
		
		for (NegocioUsuarioEdicionImpresion usuarioEdicion : listaDisponibles) {
			usuarioEdicion.setFechaCreacion(new Date());
			usuarioEdicion.setNegocio(negocio);
			usuarioEdicion.setCodigoUsuarioCreacion(usuario);
			usuarioEdicion.setPermisoEditar(0);
			usuarioEdicion.setPermisoImprimir(0);
		}

		entidadService.saveAll(listaDisponibles);
		
	}

	@Override
	public void desasociarTodas(Long idToNegocio) {
		List<NegocioUsuarioEdicionImpresion> listaAsociadas = this.obtenerUsuariosAsociados(idToNegocio);
		Negocio negocio = new Negocio();
		negocio.setIdToNegocio(idToNegocio);

		entidadService.removeAll(listaAsociadas);
	}
	
	@Override
	public boolean tienePermisoImprimir(String nombreUsuario, BigDecimal idToPoliza){
		boolean tienePermiso = false;
		if(nombreUsuario != null 
				&& !nombreUsuario.isEmpty()
				&& idToPoliza != null){
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class, idToPoliza);
			if(poliza != null
					&& poliza.getCotizacionDTO() != null
					&& poliza.getCotizacionDTO().getSolicitudDTO() != null
					&& poliza.getCotizacionDTO().getSolicitudDTO().getNegocio() != null){
				Negocio negocio = poliza.getCotizacionDTO().getSolicitudDTO().getNegocio();
				Map<String,Object> params = new HashMap<String,Object>();
				params.put("negocio.idToNegocio", negocio.getIdToNegocio());
				params.put("claveUsuario", nombreUsuario);
				List<NegocioUsuarioEdicionImpresion> listaNegocioUsuario = 
					entidadService.findByProperties(NegocioUsuarioEdicionImpresion.class, params);
				if(listaNegocioUsuario != null 
						&& !listaNegocioUsuario.isEmpty()){
					NegocioUsuarioEdicionImpresion negocioUsuario = listaNegocioUsuario.get(0);
					if(negocioUsuario.getPermisoImprimir() != null
							&& negocioUsuario.getPermisoImprimir().equals(1)){
						tienePermiso = true;
					}
				}
			}
		}
		
		return tienePermiso;
	}
	
	@Override
	public boolean tienePermisoEditar(String nombreUsuario, BigDecimal idToPoliza){
		boolean tienePermiso = false;
		if(nombreUsuario != null 
				&& !nombreUsuario.isEmpty()
				&& idToPoliza != null){
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class, idToPoliza);
			if(poliza != null
					&& poliza.getCotizacionDTO() != null
					&& poliza.getCotizacionDTO().getSolicitudDTO() != null
					&& poliza.getCotizacionDTO().getSolicitudDTO().getNegocio() != null){
				Negocio negocio = poliza.getCotizacionDTO().getSolicitudDTO().getNegocio();
				Map<String,Object> params = new HashMap<String,Object>();
				params.put("negocio.idToNegocio", negocio.getIdToNegocio());
				params.put("claveUsuario", nombreUsuario);
				List<NegocioUsuarioEdicionImpresion> listaNegocioUsuario = 
					entidadService.findByProperties(NegocioUsuarioEdicionImpresion.class, params);
				if(listaNegocioUsuario != null 
						&& !listaNegocioUsuario.isEmpty()){
					NegocioUsuarioEdicionImpresion negocioUsuario = listaNegocioUsuario.get(0);
					if(negocioUsuario.getPermisoImprimir() != null
							&& negocioUsuario.getPermisoEditar().equals(1)){
						tienePermiso = true;
					}
				}
			}
		}
		return tienePermiso;
	}

}
