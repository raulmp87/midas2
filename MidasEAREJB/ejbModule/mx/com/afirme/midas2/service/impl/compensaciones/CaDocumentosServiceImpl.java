/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaDocumentosDao;
import mx.com.afirme.midas2.domain.compensaciones.CaDocumentos;
import mx.com.afirme.midas2.service.compensaciones.CaDocumentosService;

import org.apache.log4j.Logger;

@Stateless

public class CaDocumentosServiceImpl  implements CaDocumentosService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaDocumentosDao documentoscaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaDocumentosServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaDocumentos entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaDocumentos entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaDocumentos entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaDocumentos 	::		CaDocumentosServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	documentoscaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaDocumentos 	::		CaDocumentosServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaDocumentos 	::		CaDocumentosServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaDocumentos entity.
	  @param entity CaDocumentos entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaDocumentos entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaDocumentos 	::		CaDocumentosServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaDocumentos.class, entity.getId());
	        	documentoscaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaDocumentos 	::		CaDocumentosServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaDocumentos 	::		CaDocumentosServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaDocumentos entity and return it or a copy of it to the sender. 
	 A copy of the CaDocumentos entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaDocumentos entity to update
	 @return CaDocumentos the persisted CaDocumentos entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaDocumentos update(CaDocumentos entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaDocumentos 	::		CaDocumentosServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaDocumentos result = documentoscaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaDocumentos 	::		CaDocumentosServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaDocumentos 	::		CaDocumentosServiceImpl	::	update	::	ERROR	::	",re);
            throw re;
        }
    }
    
    public CaDocumentos findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaDocumentosServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaDocumentos instance = documentoscaDao.findById(id);//entityManager.find(CaDocumentos.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaDocumentosServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaDocumentosServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaDocumentos entities with a specific property value.  
	 
	  @param propertyName the name of the CaDocumentos property to query
	  @param value the property value to match
	  	  @return List<CaDocumentos> found by query
	 */
    public List<CaDocumentos> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaDocumentosServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaDocumentos model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaDocumentosServiceImpl	::	findByProperty	::	FIN	::	");
			return documentoscaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaDocumentosServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaDocumentos> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaDocumentos> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaDocumentos> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaDocumentos> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaDocumentos entities.
	  	  @return List<CaDocumentos> all CaDocumentos entities
	 */
	public List<CaDocumentos> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaDocumentosServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaDocumentos model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaDocumentosServiceImpl	::	findAll	::	FIN	::	");
			return documentoscaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaDocumentosServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}