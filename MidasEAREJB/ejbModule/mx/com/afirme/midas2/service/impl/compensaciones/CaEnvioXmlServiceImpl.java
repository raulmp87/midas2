/**
 * 
 * Powered by AddMotions. 
 */
package mx.com.afirme.midas2.service.impl.compensaciones;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas2.domain.compensaciones.CaEnvioXml;
import mx.com.afirme.midas2.service.compensaciones.CaEnvioXmlService;

/**
 * Facade for entity CaEnvioXml.
 * @see .CaEnvioXml
  * @author MyEclipse Persistence Tools 
 */
@Stateless

public class CaEnvioXmlServiceImpl  implements CaEnvioXmlService {
	//property constants


    @PersistenceContext 
    private EntityManager entityManager;
    
    private static final Logger LOG = LoggerFactory.getLogger(CaEnvioXmlServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaEnvioXml entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaEnvioXml entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaEnvioXml entity) {
    				LOG.info("saving CaEnvioXml instance", Level.INFO, null);
	        try {
            entityManager.persist(entity);
            			LOG.info("save successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LOG.info("save failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaEnvioXml entity.
	  @param entity CaEnvioXml entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaEnvioXml entity) {
    				LOG.info("deleting CaEnvioXml instance", Level.INFO, null);
	        try {
        	entity = entityManager.getReference(CaEnvioXml.class, entity.getIdenvioca());
            entityManager.remove(entity);
            			LOG.info("delete successful", Level.INFO, null);
	        } catch (RuntimeException re) {
        				LOG.error("delete failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaEnvioXml entity and return it or a copy of it to the sender. 
	 A copy of the CaEnvioXml entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaEnvioXml entity to update
	 @return CaEnvioXml the persisted CaEnvioXml entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaEnvioXml update(CaEnvioXml entity) {
    				LOG.info("updating CaEnvioXml instance", Level.INFO, null);
	        try {
            CaEnvioXml result = entityManager.merge(entity);
            			LOG.info("update successful", Level.INFO, null);
	            return result;
        } catch (RuntimeException re) {
        				LOG.error("update failed", Level.SEVERE, re);
	            throw re;
        }
    }
    
    public CaEnvioXml findById(CaEnvioXml id) {
    				LOG.info("finding CaEnvioXml instance with id: " + id, Level.INFO, null);
	        try {
            CaEnvioXml instance = entityManager.find(CaEnvioXml.class, id);
            return instance;
        } catch (RuntimeException re) {
        				LOG.error("find failed", Level.SEVERE, re);
	            throw re;
        }
    }    
    

/**
	 * Find all CaEnvioXml entities with a specific property value.  
	 
	  @param propertyName the name of the CaEnvioXml property to query
	  @param value the property value to match
	  	  @return List<CaEnvioXml> found by query
	 */
    @SuppressWarnings("unchecked")
    public List<CaEnvioXml> findByProperty(String propertyName, final Object value) {
    				LOG.info("finding CaEnvioXml instance with property: " + propertyName + ", value: " + value, Level.INFO, null);
			try {
			final String queryString = "select model from CaEnvioXml model where model." 
			 						+ propertyName + "= :propertyValue";
								Query query = entityManager.createQuery(queryString);
					query.setParameter("propertyValue", value);
					return query.getResultList();
		} catch (RuntimeException re) {
						LOG.error("find by property name failed", Level.SEVERE, re);
				throw re;
		}
	}			
	
	/**
	 * Find all CaEnvioXml entities.
	  	  @return List<CaEnvioXml> all CaEnvioXml entities
	 */
	@SuppressWarnings("unchecked")
	public List<CaEnvioXml> findAll() {
					LOG.info("finding all CaEnvioXml instances", Level.INFO, null);
			try {
			final String queryString = "select model from CaEnvioXml model";
								Query query = entityManager.createQuery(queryString);
					return query.getResultList();
		} catch (RuntimeException re) {
						LOG.error("find all failed", Level.SEVERE, re);
				throw re;
		}
	}
	
}