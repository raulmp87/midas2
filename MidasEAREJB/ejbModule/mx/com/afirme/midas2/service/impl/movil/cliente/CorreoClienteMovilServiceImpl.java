package mx.com.afirme.midas2.service.impl.movil.cliente;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.domain.movil.cliente.CorreoClienteMovil;
import mx.com.afirme.midas2.service.movil.cliente.CorreoClienteMovilService;

/**
 * Facade for entity Trusrcorreo.
 * 
 * @see com.mx.CorreoClienteMovil
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class CorreoClienteMovilServiceImpl implements CorreoClienteMovilService {
	// property constants

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved Trusrcorreo entity. All
	 * subsequent persist actions of this entity should use the #update()
	 * method.
	 * 
	 * @param entity
	 *            Trusrcorreo entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(CorreoClienteMovil entity) {
		LogDeMidasEJB3.log("saving Trusrcorreo instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent Trusrcorreo entity.
	 * 
	 * @param entity
	 *            Trusrcorreo entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(CorreoClienteMovil entity) {
		LogDeMidasEJB3.log("deleting Trusrcorreo instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(CorreoClienteMovil.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved Trusrcorreo entity and return it or a copy of
	 * it to the sender. A copy of the Trusrcorreo entity parameter is returned
	 * when the JPA persistence mechanism has not previously been tracking the
	 * updated entity.
	 * 
	 * @param entity
	 *            Trusrcorreo entity to update
	 * @return Trusrcorreo the persisted Trusrcorreo entity instance, may not be
	 *         the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public CorreoClienteMovil update(CorreoClienteMovil entity) {
		LogDeMidasEJB3.log("updating Trusrcorreo instance", Level.INFO, null);
		try {
			CorreoClienteMovil result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public CorreoClienteMovil findById(Long id) {
		LogDeMidasEJB3.log("finding Trusrcorreo instance with id: " + id, Level.INFO,
				null);
		try {
			CorreoClienteMovil instance = entityManager.find(CorreoClienteMovil.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Trusrcorreo entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the Trusrcorreo property to query
	 * @param value
	 *            the property value to match
	 * @return List<Trusrcorreo> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<CorreoClienteMovil> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding Trusrcorreo instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from MIDAS.Trusrcorreo model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all Trusrcorreo entities.
	 * 
	 * @return List<Trusrcorreo> all Trusrcorreo entities
	 */
	@SuppressWarnings("unchecked")
	public List<CorreoClienteMovil> findAll() {
		LogDeMidasEJB3.log("finding all Trusrcorreo instances", Level.INFO, null);
		try {
			final String queryString = "select model from MIDAS.Trusrcorreo model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}