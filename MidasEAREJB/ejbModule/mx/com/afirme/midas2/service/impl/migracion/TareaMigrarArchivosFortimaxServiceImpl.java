package mx.com.afirme.midas2.service.impl.migracion;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.service.migracion.MigracionService;
import mx.com.afirme.midas2.service.migracion.TareaMigrarArchivosFortimaxService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class TareaMigrarArchivosFortimaxServiceImpl implements TareaMigrarArchivosFortimaxService {
	
	private static final Logger LOG = Logger.getLogger(TareaMigrarArchivosFortimaxServiceImpl.class);
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private MigracionService migracionService;
	
	@Resource	
	private TimerService timerService;
	
	public void initialize() {
		String timerInfo = "TimerMigrarArchivosFortimax";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(1);
				expression.hour(21);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerMigrarArchivosFortimax", false));
				
				LOG.info("Tarea TimerMigrarArchivosFortimax configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerMigrarArchivosFortimax");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerMigrarArchivosFortimax:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		migracionService.migrarArchivosFortimax();
	}

}
