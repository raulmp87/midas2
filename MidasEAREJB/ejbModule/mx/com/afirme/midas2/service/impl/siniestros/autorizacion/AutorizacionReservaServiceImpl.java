/**
 * 
 */
package mx.com.afirme.midas2.service.impl.siniestros.autorizacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.siniestros.autorizacion.ParametroAutorizacionReserva;
import mx.com.afirme.midas2.domain.siniestros.autorizacion.ParametroReservaAntiguo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.autorizacion.AutorizacionReservaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;
import org.joda.time.LocalDate;
import org.joda.time.Months;

/**
 * @author simavera	
 *
 */
@Stateless
public class AutorizacionReservaServiceImpl implements AutorizacionReservaService {

	private static final Logger log = Logger.getLogger(AutorizacionReservaServiceImpl.class);
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	
	private static final String IGUAL = "IG";
	private static final String MAYOR_QUE = "MAY" ;
	private static final String MENOR_QUE = "MEN";
	private static final String MAYOR_IGUAL = "MAQ";
	private static final String MENOR_IGUAL = "MEQ"; 
	private static final int 	CIEN = 100;
	
	private Map<String, String> roles = obtenerRolesAutorizacion();
	
	/**
	 * Metodo que elimina un parametro
	 */
	@Override
	public void eliminarParamstatic( Long idParametro ){
		ParametroAutorizacionReserva parametroAutorizacionReserva = null;
		parametroAutorizacionReserva = entidadService.findById(ParametroAutorizacionReserva.class, idParametro );
		entidadService.remove( parametroAutorizacionReserva );
	}

	/**
	 * Metodo que valida que la nueva estimacion de la Cobertura 
	 * cumpla con los parametros de Configuracion.
	 * 
	 * @param idEstimacionCob id de la estimacion de la cobertura
	 * @param estimacion
	 */
	public String validarParametros (Long idEstimacionCob, BigDecimal estimacionNueva){
		EstimacionCoberturaReporteCabina estimacionCobertura = null;
		List<ParametroAutorizacionReserva> parametrosList = null;
		Map<String,Object> params = new HashMap<String,Object>();
//		BigDecimal reserva = null;
		String mensaje = null;
		
		estimacionCobertura = entidadService.findById( EstimacionCoberturaReporteCabina.class, idEstimacionCob );
		
		Long oficinaId = estimacionCobertura.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getOficina().getId();
				
		ConfiguracionCalculoCoberturaSiniestro confCalculo = estimacionCoberturaSiniestroService.obtenerConfiguracionCalcCobertura(
				estimacionCobertura.getCoberturaReporteCabina().getClaveTipoCalculo(),
				null, estimacionCobertura.getTipoEstimacion(),
				estimacionCobertura.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());
		
		if (confCalculo != null){
			params.clear();
			params.put( "seccion.idToSeccion", estimacionCobertura.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getSeccionDTO().getIdToSeccion() );
			params.put( "cobertura.idToCobertura", estimacionCobertura.getCoberturaReporteCabina().getCoberturaDTO().getIdToCobertura() );
			params.put( "estatus", Boolean.TRUE );
			params.put( "oficina.id", oficinaId );
			if(confCalculo.getCveSubTipoCalculoCobertura()!=null){
				params.put( "claveSubCalculo", confCalculo.getCveSubTipoCalculoCobertura() );
			}
			parametrosList = entidadService.findByProperties( ParametroAutorizacionReserva.class, params );
			if( parametrosList == null ){
				return null;
			}
			BigDecimal estimacionActual = movimientoSiniestroService.obtenerMontoEstimacionActual(null, null, estimacionCobertura.getId(), null, null, null);
			BigDecimal diferencia = estimacionActual.subtract(estimacionNueva).abs();
			
			for( ParametroAutorizacionReserva parametro : parametrosList ){
//				BigDecimal montoPagos = movimientoSiniestroService.obtenerMontoPagosAfectacion(idEstimacionCob);
//				reserva = estimacionNueva.subtract(montoPagos);
				if(!this.esCriterioValidoParaMontoReserva(parametro, diferencia)){
					mensaje = "Debido al monto del movimiento en el ajuste de reserva se requiere  autorización";
					break;
				}
				if(estimacionCobertura.getCoberturaReporteCabina().getClaveTipoDeducible() != null && estimacionCobertura.getCoberturaReporteCabina().getClaveTipoDeducible() == 1){
					Double porcentajeDeducible = estimacionCobertura.getCoberturaReporteCabina().getPorcentajeDeducible();
					if(!this.esCriterioValidoParaPorcentajeDeDeducible(parametro, porcentajeDeducible)){
						mensaje = "Debido al porcentaje de deducible de la cobertura se requiere de Autorización";
						break;
					}
					BigDecimal sumaAseguradaValorComercial = estimacionCobertura.getSumaAseguradaObtenida();
					BigDecimal montoDeducible = new BigDecimal(porcentajeDeducible).divide(new BigDecimal(CIEN)).multiply( sumaAseguradaValorComercial);
					if(!this.esCriterioValidoParaMontoDeducible(parametro, montoDeducible)){
						mensaje = "Debido al monto de deducible de la cobertura se requiere de Autorización";
						break;
					}
				}
			}
			
		}
		return mensaje;
	}
	
	@Override
	public Long validarParametrosAntiguedad(Long idEstimacionCob,BigDecimal estimacionNueva){
		List<Boolean> listaCoincidencias = new ArrayList<Boolean>();
		Long parametroId = null;
		Map<String,Object> params = new HashMap<String,Object>();
		
		EstimacionCoberturaReporteCabina estimacion = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstimacionCob);

		ConfiguracionCalculoCoberturaSiniestro confCalc = estimacionCoberturaSiniestroService.obtenerConfiguracionCalcCobertura(
				estimacion.getCoberturaReporteCabina().getClaveTipoCalculo(),
				null, estimacion.getTipoEstimacion(), estimacion.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());

		params.put("cobertura.idToCobertura",estimacion.getCoberturaReporteCabina().getCoberturaDTO().getIdToCobertura() );
		params.put("estatus",Boolean.TRUE);
		if(confCalc.getCveSubTipoCalculoCobertura()!=null){
			params.put("claveSubCalculo",confCalc.getCveSubTipoCalculoCobertura());
		}
		List<ParametroReservaAntiguo> parametrosList = this.entidadService.findByProperties(ParametroReservaAntiguo.class, params);
		if(parametrosList==null){
			return null;
		}
		for(ParametroReservaAntiguo parametro : parametrosList ){
			listaCoincidencias.clear();
			if(parametro.getOficina()!=null){
				Long oficinaId = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getOficina().getId();
				if(oficinaId.intValue()== parametro.getOficina().getId().intValue()){
					listaCoincidencias.add(Boolean.TRUE);
				}else{
					listaCoincidencias.add(Boolean.FALSE);
				}
			}
			if(parametro.getTipoAjuste()!=null){
				if(validaTipoDeAjuste(idEstimacionCob, estimacionNueva, parametro.getTipoAjuste())){
					listaCoincidencias.add(Boolean.TRUE);
				}else{
					listaCoincidencias.add(Boolean.FALSE);
				}
			}
			if(parametro.getTipoSiniestro()!=null){
				String causaSiniestro = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getCausaSiniestro();
				if(parametro.getTipoSiniestro().equals(causaSiniestro)){
					listaCoincidencias.add(Boolean.TRUE);
				}else{
					listaCoincidencias.add(Boolean.FALSE);
				}
			}
			if(parametro.getMonto()!=null){
				String condicionMonto = parametro.getCondicionMonto();
				if(this.validaMontoConParametroAntiguo(condicionMonto, parametro.getMonto(), estimacionNueva,idEstimacionCob)){
					listaCoincidencias.add(Boolean.TRUE);
				}else{
					listaCoincidencias.add(Boolean.FALSE);
				}
			}
			if(parametro.getMeses()!=null){
				String condicionMeses = parametro.getCondicionMeses();
				if(validaMesesConParametroAntiguo(estimacion, parametro.getMeses(), condicionMeses)){
					listaCoincidencias.add(Boolean.TRUE);
				}else{
					listaCoincidencias.add(Boolean.FALSE);
				}
			}
			boolean aplica = true;
			for(Boolean coincidencia : listaCoincidencias){
				if(coincidencia==Boolean.FALSE){
					aplica = false;
					break;
				}
			}
			if(aplica){
				parametroId = parametro.getId();
				break;
			}
		}
		return parametroId;
	}
	
	private boolean validaTipoDeAjuste(Long idEstimacion, BigDecimal estimacionNueva,String tipoAjuste){
		boolean esValido = Boolean.FALSE;
		BigDecimal estimacionActual = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(idEstimacion);
		
		BigDecimal monto = estimacionNueva.subtract(estimacionActual);
		if(tipoAjuste.equals("AA")){
			if(monto.compareTo(BigDecimal.ZERO)>0){
				esValido = Boolean.TRUE;
			}
		}else if(tipoAjuste.equals("AD")){
			if(monto.compareTo(BigDecimal.ZERO)<0){
				esValido = Boolean.TRUE;
			}
		}
		return esValido;
	}
	
	private boolean validaMontoConParametroAntiguo(String condicion,BigDecimal montoParametro,BigDecimal estimacionNueva,Long idEstimacion){
		BigDecimal estimacionActual = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(idEstimacion);
		BigDecimal importeDelMovimiento = estimacionNueva.subtract(estimacionActual).abs();
		boolean esValido = Boolean.FALSE;
		if(condicion.toUpperCase().equals(IGUAL)){
			if(importeDelMovimiento.equals(montoParametro)){
				esValido = Boolean.TRUE;
			}
		}else if(condicion.toUpperCase().equals(MAYOR_QUE)){
			if(importeDelMovimiento.compareTo(montoParametro)>0){
				esValido = Boolean.TRUE;
			}
		}else if(condicion.toUpperCase().equals(MAYOR_IGUAL)){
			if(importeDelMovimiento.compareTo(montoParametro)>=0){
				esValido = Boolean.TRUE;
			}
		}else if(condicion.toUpperCase().equals(MENOR_QUE)){
			if(importeDelMovimiento.compareTo(montoParametro)<0){
				esValido = Boolean.TRUE;
			}
		}else if(condicion.toUpperCase().equals(MENOR_IGUAL)){
			if(importeDelMovimiento.compareTo(montoParametro)<=0){
				esValido = Boolean.TRUE;
			}
		}
		return esValido;
	}
	
	private boolean validaMesesConParametroAntiguo(EstimacionCoberturaReporteCabina estimacion, Integer mesesParametro,String condicionMeses ){
		boolean esValido = Boolean.FALSE;
		Date fechaCreacionSiniestro = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getFechaCreacion();
		int monthsBetween = Months.monthsBetween(new LocalDate(fechaCreacionSiniestro), new LocalDate()).getMonths();
		if(condicionMeses.toUpperCase().equals(IGUAL)){
			if(monthsBetween == mesesParametro.intValue()){
				esValido = Boolean.TRUE;
			}
		}else if(condicionMeses.toUpperCase().equals(MAYOR_QUE)){
			if(monthsBetween> mesesParametro.intValue()){
				esValido = Boolean.TRUE;
			}
		}else if(condicionMeses.toUpperCase().equals(MAYOR_IGUAL)){
			if(monthsBetween>= mesesParametro.intValue()){
				esValido = Boolean.TRUE;
			}
		}else if(condicionMeses.toUpperCase().equals(MENOR_QUE)){
			if(monthsBetween< mesesParametro.intValue()){
				esValido = Boolean.TRUE;
			}
		}else if(condicionMeses.toUpperCase().equals(MENOR_IGUAL)){
			if(monthsBetween<= mesesParametro.intValue()){
				esValido = Boolean.TRUE;
			}
		}
		return esValido;
	}
	
	private boolean esCriterioValidoParaPorcentajeDeDeducible(ParametroAutorizacionReserva parametro,Double porcentajeDeducible){
		String criterio = parametro.getCriterioRangoPorcentaje();
		BigDecimal valueInicial = parametro.getMontoPorcentajeInicial();
		BigDecimal valueFinal = parametro.getMontoPorcentajeFinal();
		return this.validaCondicion(criterio, valueInicial, valueFinal, new BigDecimal(porcentajeDeducible));
	}
	
	private boolean esCriterioValidoParaMontoReserva(ParametroAutorizacionReserva parametro,BigDecimal montoDiferencia){
		String criterio = parametro.getCriterioRangoReserva();
		BigDecimal valueInicial = parametro.getMontoReservaInicial();
		BigDecimal valueFinal = parametro.getMontoReservaFinal();
		boolean validacion = this.validaCondicion(criterio, valueInicial, valueFinal, montoDiferencia); 
		return validacion;
	}
	
	private boolean esCriterioValidoParaMontoDeducible(ParametroAutorizacionReserva parametro,BigDecimal montoDeducible){
		String criterio = parametro.getCriterioRangoDeducible();
		BigDecimal valueInicial = parametro.getMontoDeducibleInicial();
		BigDecimal valueFinal = parametro.getMontoDeducibleFinal();
		return this.validaCondicion(criterio, valueInicial, valueFinal, montoDeducible);
	}
	
	private boolean validaCondicion(String criterio,BigDecimal valueInicial ,BigDecimal valueFinal,BigDecimal value ){
		boolean esValido = true;
		if(criterio!=null){
			BigDecimal valorComparable = null;
			valorComparable = (valueInicial!=null)?valueInicial:null;
			valorComparable = (valorComparable==null && valueFinal!=null)?valueFinal:valorComparable;
			if(criterio.equals("IG")){
				if(valorComparable.compareTo(value)==0){
					esValido = false;
				}
			}else if(criterio.equals("MAY")){
				if(value.compareTo(valorComparable)>0){
					esValido = false;
				}
			}else if(criterio.equals("MEN")){
				if(value.compareTo(valorComparable)<0){
					esValido = false;
				}
			}else if(criterio.equals("MAQ")){
				if(value.compareTo(valorComparable)>=0){
					esValido = false;
				}
			}else if(criterio.equals("MEQ")){
				if(value.compareTo(valorComparable)<=0){
					esValido = false;
				}
			}
		}else if(valueInicial != null && valueFinal!=null ){
			if(value.compareTo(valueInicial)>=0 && value.compareTo(valueFinal)<=0){
				esValido = false;
			}
		}
		return esValido;
	}
	
	

	/**
	 * Metodo que obtiene el listado de coberturas que pertenecen a una seccion 
	 * y no es de Suma Asegurada Amparada
	 * 
	 * @param idToSeccion id de la Seccion relacionada a las coberturas
	 */
	@Override
	public List<CoberturaSeccionSiniestroDTO> obtenerCoberturasPorSeccion(Long idToSeccion){
		List<CoberturaSeccionSiniestroDTO> listCoberturaSeccionSiniestro = null;
		List<CoberturaSeccionSiniestroDTO> listReturnCoberturaSeccionSiniestro = new ArrayList<CoberturaSeccionSiniestroDTO>();
		listCoberturaSeccionSiniestro = estimacionCoberturaSiniestroService.obtenerCoberturasPorSeccion( idToSeccion );
		if( listCoberturaSeccionSiniestro != null ){
			for( CoberturaSeccionSiniestroDTO coberturaSeccionSiniestro : listCoberturaSeccionSiniestro ){
				if( !coberturaSeccionSiniestro.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals("3") ){
					listReturnCoberturaSeccionSiniestro.add( coberturaSeccionSiniestro );
				}
			}
		}
		
		return listReturnCoberturaSeccionSiniestro;
	}

	/**
	 * Metodo que obtiene el listado de todos los parametros de autorizacion de reserva existentes
	 * 
	 */
	public List<ParametroAutorizacionReserva> listarParametros(){
		List<ParametroAutorizacionReserva> listParametroAutorizacionReserva = null;			
		listParametroAutorizacionReserva = entidadService.findAll( ParametroAutorizacionReserva.class );
		Oficina oficina = null; 
		
		for( ParametroAutorizacionReserva parametro : listParametroAutorizacionReserva ){
			oficina = entidadService.findById( Oficina.class, parametro.getOficina().getId() );
			if( oficina != null && !oficina.getNombreOficina().equals("") ){
				parametro.getOficina().setNombreOficina( oficina.getNombreOficina() );
				parametro.setFechaCreacion( new Date() );
			}
			String nombreCobertura= estimacionCoberturaSiniestroService.obtenerNombreCobertura(
						parametro.getCobertura(),
						parametro.getCobertura().getClaveTipoCalculo(),
						parametro.getClaveSubCalculo(), null, parametro.getCobertura().getTipoConfiguracion());			
			
			parametro.setNombreCobertura(nombreCobertura);
			
		}
		return listParametroAutorizacionReserva; 
	}

	/**
	 * Metodo que obtiene el ParametroAutorizacionReserva 
	 * 
	 * @param idParametro id del Parametro de Autorizacion de Reserva
	 */
	@Override
	public ParametroAutorizacionReserva obtenerParametro(Long idParametro){
		ParametroAutorizacionReserva parametroAutorizacionReserva = null;
		parametroAutorizacionReserva = entidadService.findById( ParametroAutorizacionReserva.class, idParametro );
		parametroAutorizacionReserva.getCobertura().hashCode();
		parametroAutorizacionReserva.getOficina().hashCode();
		parametroAutorizacionReserva.getSeccion().hashCode();
		return parametroAutorizacionReserva;
	}

	/**
	 * Metodo que  guarda un nuevo o modifica un parametro de autorizacion.
	 * 
	 * @param parametro entidad con los parametros configurados 
	 *                  para la autorizacion de la reserva
	 */
	@Override
	public void guardarParametro(ParametroAutorizacionReserva parametro){
		ParametroAutorizacionReserva parametroAux = null;
		parametro.getClaveSubCalculo().replace(",", "");
		String[] aux = parametro.getClaveSubCalculo().split("-");
		String claveSubCalculo = aux[1];
		if( parametro.getId() == null ){
			parametro.setFechaCreacion( parametro.getFechaCreacion() );
			parametro.setCodigoUsuarioCreacion( usuarioService.getUsuarioActual().getNombreUsuario() );
		}else{
			parametroAux = entidadService.findById( ParametroAutorizacionReserva.class, parametro.getId() );
			parametro.setFechaCreacion( parametroAux.getFechaCreacion() );
			parametro.setCodigoUsuarioCreacion( parametroAux.getCodigoUsuarioCreacion() );
			parametro.setFechaModificacion( parametro.getFechaCreacion() );
			parametro.setCodigoUsuarioModificacion( usuarioService.getUsuarioActual().getNombreUsuario() );
		}
		
		CoberturaDTO cobertura = new CoberturaDTO();
		cobertura.setIdToCobertura(new BigDecimal(aux[0]));
		parametro.setCobertura(cobertura);
		
		if( !claveSubCalculo.contains(",") && !claveSubCalculo.equals("") ){
			parametro.setClaveSubCalculo( claveSubCalculo );
		}else{
			parametro.setClaveSubCalculo( null );
		}
		entidadService.save( parametro );
	}

	@Override
	public Map<String, String> obtenerRolesAutorizacion() {
		Map<String, String> rolesMap = new HashMap<String, String>();
		rolesMap.put("Rol_M2_Director_General_Seguros", "Director General de Seguros");
		rolesMap.put("Rol_M2_Director_Indemnizacion_Operacion_Siniestros", "Director Indemnización y Operación.");
		rolesMap.put("Rol_M2_SubDirector_Siniestros_Autos", "Sub Director Siniestros Autos");
		rolesMap.put("Rol_M2_Gerente_Actuarial", "Gerente Actuarial");
		rolesMap.put("Rol_M2_Analista_Actuaria_Automoviles", "Analista Actuario Automóviles");
		return rolesMap;
	}
	
	private Map<String, List<String>> obtenerUsuariosPorRol(){
		Map<String, List<String>> usuariosPorRol = new HashMap<String, List<String>>();
		Iterator<Entry<String, String>> it = roles.entrySet().iterator();	
		while(it.hasNext()){			
			Map.Entry<String, String> par = (Map.Entry<String, String>)it.next();
			try{
				List<Usuario> usuarios = usuarioService.buscarUsuariosPorNombreRol(par.getKey());
				List<String> nombres = new ArrayList<String>();
				if(usuarios != null){					
					for(Usuario usuario : usuarios){
						if(!StringUtil.isEmpty(usuario.getNombreCompleto())){
							nombres.add(usuario.getNombreCompleto());
						}					
					}
				}
				usuariosPorRol.put(par.getKey(), nombres);
			}catch(Exception ex){
				log.error(ex);
			}
		}						
		return usuariosPorRol;
	}
	
	private String obtenerUsuariosAutorizadores(String[] roles, Map<String, List<String>> usuariosPorRol){
		StringBuilder autorizadores = new StringBuilder("");
		for(String rol : roles){
			List<String> nombres = usuariosPorRol.get(rol);
			if(nombres != null){
				for(String nombre : nombres){
					if(autorizadores.length()!=0){
						autorizadores.append(",");
					}
					autorizadores.append(nombre);
				}
			}
		}
		return autorizadores.toString();	
	}

	
	@Override
	public List<ParametroReservaAntiguo> listarParametrosAntiguedad() {
		List<ParametroReservaAntiguo> parametros = this.entidadService.findAll(ParametroReservaAntiguo.class);
		Map<String,String> tipoAjusteMap = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_AJUSTE);
		Map<String,String> tipoSiniestro = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSA_SINIESTRO);
		Map<String,String> listCriterio = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CRITERIO_COMPARACION);
		Map<String, List<String>> usuariosPorRol = this.obtenerUsuariosPorRol();	
		
		for(ParametroReservaAntiguo parametro : parametros){
			String nombreCobertura = estimacionCoberturaSiniestroService.obtenerNombreCobertura(
						parametro.getCobertura(),
						parametro.getCobertura().getClaveTipoCalculo(),
						parametro.getClaveSubCalculo(), null,
						parametro.getCobertura().getTipoConfiguracion());
			parametro.setNombreCoberturaDesc(nombreCobertura);
			parametro.setAutorizadores(obtenerUsuariosAutorizadores(parametro.getRolArray(), usuariosPorRol));
			
			/*List<Usuario> usuarios = null;
				
			try {
				usuarios = this.usuarioService.buscarUsuariosPorNombreRol(parametro.getRolArray());
			} catch(Exception e) {
				e.printStackTrace();
			}
			
			if (usuarios != null && !usuarios.isEmpty()) {
				StringBuilder nombresDeUsuarios = new StringBuilder();
				for(Usuario usuario : usuarios){
					if(nombresDeUsuarios.length()!=0){
						nombresDeUsuarios.append(",");
					}
					nombresDeUsuarios.append(usuario.getNombreCompleto());
				}
				parametro.setAutorizadores(nombresDeUsuarios.toString());
			}
			*/
			if(parametro.getEstatus()){
				parametro.setEstatusDesc("Activo");
			}else{
				parametro.setEstatusDesc("Inactivo");
			}
			parametro.setConfiguracionDesc(this.generaDecripcionDeLaConfiguracion(parametro,tipoAjusteMap,listCriterio));
			parametro.setTipoSiniestroDesc(tipoSiniestro.get(parametro.getTipoSiniestro()));
		}
		return parametros;
	}
	
	private String generaDecripcionDeLaConfiguracion(ParametroReservaAntiguo parametro, Map<String,String> ajustesMap,Map<String,String> listCriterio ){
		String condicionMonto= listCriterio.get(parametro.getCondicionMonto());
		String condicionMeses= listCriterio.get(parametro.getCondicionMeses());
		String descripcion = "";
		descripcion+=ajustesMap.get(parametro.getTipoAjuste())+" "+condicionMonto+" a $"+parametro.getMonto()+
		" y "+condicionMeses+" a "+parametro.getMeses()+" meses";
		return descripcion;
	}

	@Override
	public void inactivarParametroAntiguedad(Long idParametro) {
		ParametroReservaAntiguo parametro = this.entidadService.findById(ParametroReservaAntiguo.class, idParametro);
		parametro.setEstatus(Boolean.FALSE);
		this.entidadService.save(parametro);
	}

	@Override
	public ParametroReservaAntiguo obtenerParametroAntiguedad(Long idParametro) {
		return this.entidadService.findById(ParametroReservaAntiguo.class, idParametro);
	}

	public void prepareGuardarParametroAntiguedad(){
		
	}
	
	@Override
	public void guardarParametroAntiguedad(ParametroReservaAntiguo parametro) {
		final int primerElemento = 0;
		final int segundoElemento = 1;
		String[] coberturaInfo = parametro.getIdCoberturaClaveSubCalculo().trim().split("-");
		parametro.setCobertura(new CoberturaDTO());
		parametro.getCobertura().setIdToCobertura(new BigDecimal(coberturaInfo[primerElemento]));
		if(coberturaInfo.length>1){
			parametro.setClaveSubCalculo(coberturaInfo[segundoElemento].trim());
		}
		if(parametro.getId()==null){
			parametro.setFechaCreacion(new Date());
			parametro.setCodigoUsuarioCreacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
		}
		parametro.setCodigoUsuarioModificacion(this.usuarioService.getUsuarioActual().getNombreUsuario());
		parametro.setEstatus(Boolean.TRUE);
		this.entidadService.save(parametro);
	}

}
