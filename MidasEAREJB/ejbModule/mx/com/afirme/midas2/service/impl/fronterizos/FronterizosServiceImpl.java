package mx.com.afirme.midas2.service.impl.fronterizos;

import java.math.BigDecimal;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.concurrent.TimeUnit;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.xml.datatype.DatatypeFactory;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.estadodescuento.NegocioEstadoDescuentoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta;
import mx.com.afirme.midas2.domain.fronterizos.BitacoraCESVI;
import mx.com.afirme.midas2.domain.fronterizos.BitacoraKBB;
import mx.com.afirme.midas2.domain.fronterizos.LoginKBB;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.inciso.IncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fronterizos.BitacoraCESVIService;
import mx.com.afirme.midas2.service.fronterizos.BitacoraKBBService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.wsClient.kbb.ApplicationCategory;
import mx.com.afirme.midas2.wsClient.kbb.ArrayOfVehicleConfiguration;
import mx.com.afirme.midas2.wsClient.kbb.IdStringPair;
import mx.com.afirme.midas2.wsClient.kbb.Release2008R2Proxy;
import mx.com.afirme.midas2.wsClient.kbb.VINStatus;
import mx.com.afirme.midas2.wsClient.kbb.VehicleClass;
import mx.com.afirme.midas2.wsClient.kbb.VehicleCondition;
import mx.com.afirme.midas2.wsClient.kbb.VehicleConfiguration;
import mx.com.afirme.midas2.wsClient.kbb.VehicleValuation;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

@Stateless
public class FronterizosServiceImpl implements FronterizosService {

	private static final Logger LOG = Logger
			.getLogger(FronterizosServiceImpl.class);

	final Long ID_KEY = 1L;
	public static int VALIDO_VIN = 1;
	public static final String VALUE_TRUE = "V";
	public final Short CONSULTA_NUM_TRUE = 1;
	public final Short CONSULTA_NUM_FALSE = 1;
	public final long DIFERENCIA_MINUTOS = 60;

	@EJB
	private SistemaContext sistemaContext;
	@EJB
	protected EntidadService entidadService;
	@EJB
	private NegocioEstadoDescuentoDao negocioEstadoDescuentoDao;
	@EJB
	private BitacoraKBBService bitacoraKBBService;
	@EJB
	private BitacoraCESVIService bitacoraCESVIService;
	@EJB
	protected TipoCambioFacadeRemote tipoCambioService = null;	
	@EJB
	private InformacionVehicularService informacionVehicularService;
	@EJB
	protected EntidadDao entidadDao;
	@EJB
	private ListadoService listadoService;
	@EJB
	public IncisoViewService incisoViewService;
	@EJB
	private EntidadContinuityDao entidadContinuityDao;
	@EJB
	private IncisoBitemporalService incisoBitemporalService;
	@EJB
	CargaMasivaService cargaMasivaService;
	@Resource	
	private TimerService timerService;
	
	@Override
	public String getLogin() throws Exception {

		String[] user_password = null;
		String respuesta = null;
		
		try {
			Release2008R2Proxy release2008R2Proxy = new Release2008R2Proxy();			
			user_password = this.getUserPassword();			
			if( user_password != null && user_password.length > 1 ){
				release2008R2Proxy._getDescriptor().setEndpoint(sistemaContext.getKBBEndPoint());
				respuesta = release2008R2Proxy.login( user_password[0], user_password[1] );
			}
			
		} catch (Exception e) {			
			LOG.error("Ocurrio un error en FronterizosService.getLogin()", e);
			throw e;
		}
		return respuesta;
	}
	
	private String[] getUserPassword() throws Exception {
		
		String[] user_password = null;
		String user = sistemaContext.getKBBUser();
		if ( user != null ){			
			user_password = user.split("\\|"); 
		}		
		return user_password;
	}
	
	private String getMarcaFronterizo() throws Exception {
		
		return sistemaContext.getMarcaFronterizo();
	}
	
	private String getEstiloFronterizo() throws Exception {
		LOG.info("::::::::: >>>> ENTRA AL METODO getEstiloFronterizo();");
		return sistemaContext.getEstiloFronterizo();
	}

	@Override
	public LoginKBB saveKey(LoginKBB loginKBB){

		return entidadService.save(loginKBB);
	}

	@Override
	public LoginKBB findKey() {

		List<LoginKBB> list = entidadService.findAll(LoginKBB.class);
		if (list != null && list.size() > 0){
			long minutosDiferencia = getDateDiff(list.get(0).getCreationDate(),new Date(),TimeUnit.MINUTES);
			if (minutosDiferencia > this.DIFERENCIA_MINUTOS){
				return this.generateKeyWithout();
			}
			else{
				return list.get(0);
			}
		}			
		else{
			return this.generateKeyWithout();
		}
	}
	
	private LoginKBB generateKeyWithout(){
		LOG.info("Generando llave KBB independiente del Job");
		this.generateKey();
		return this.findKeySecond();
	}
	
	private LoginKBB findKeySecond(){
		List<LoginKBB> list = entidadService.findAll(LoginKBB.class);
		if (list != null && list.size() > 0)
			return list.get(0);
		else
			return null;
	}
	
	@Override
	public String validateVIN(String VIN) throws Exception{
		
		try {
			Release2008R2Proxy release2008R2Proxy = new Release2008R2Proxy();
			VINStatus VINStatusObj;
			String authKey = this.findKey() !=null ? this.findKey().getAuthKey() : "";

			release2008R2Proxy._getDescriptor().setEndpoint(sistemaContext.getKBBEndPoint());
			VINStatusObj = release2008R2Proxy.validateVIN( authKey, VIN );
			
			return VINStatusObj.value();
		} catch (Exception e) {
			LOG.error("Ocurrio un error en FronterizosService.validateVIN()", e);
			throw e;
		}
	}
	
	@Override
	public boolean isActive(){
		
		String activo = sistemaContext.getKBBActiva();
		
		if ( activo != null && activo.trim().equals("1")){
			return true;
		}else{
			LOG.info("Estatus de servicio KBB: INACTIVO. Para activarlo modifica el Parámetro General.");
			return false;
		}
	}
	
	private VehicleConfiguration obtenerInformacionVehiculo(String vin, Long idToNegocio, 
			String stateId) throws Exception {
		LOG.info("::::>>>> entra al metodo obtenerInformacionVehiculo(" + vin + ", " + idToNegocio + ", " + stateId + ");");
		VehicleConfiguration vehicleConfiguration = null;
		ArrayOfVehicleConfiguration arrayVehicleConfiguration = null;
		
		try {
			String zipCode = negocioEstadoDescuentoDao.obtenerZipCode(idToNegocio, stateId);
			String authKey = this.findKey() != null ? this.findKey().getAuthKey() : "";
			GregorianCalendar versionDate = new GregorianCalendar();
			versionDate.setTime(new Date());
			
			Release2008R2Proxy release2008R2Proxy = new Release2008R2Proxy();
			release2008R2Proxy._getDescriptor().setEndpoint(sistemaContext.getKBBEndPoint());
			LOG.info("::::::::: [ INFO ]  MANDA A LLAMAR OBTENER VEHICULO DE 'KBB'");
			arrayVehicleConfiguration = release2008R2Proxy.getVehicleConfigurationByVINAndClass(authKey, vin, zipCode, 
					DatatypeFactory.newInstance().newXMLGregorianCalendar(versionDate), VehicleClass.USED_CAR);
			
			if (arrayVehicleConfiguration != null && arrayVehicleConfiguration.getVehicleConfiguration() != null) {
				vehicleConfiguration = arrayVehicleConfiguration.getVehicleConfiguration().get(0);
			}
		}catch(ApplicationException e){			
			List<String> errors = e.getErrors();			
			throw new Exception(this.traerError(errors));
		}		
		catch (Exception e) {
			LOG.error("Ocurrio un error en FronterizosService.obtenerInformacionVehiculo()", e);
		}
		LOG.info("::::::::::: <<<<< REGRESA LA CONFIGURACI\u00d3N DEL VEHICULO");
		return vehicleConfiguration;
	}
	
	private BigDecimal obtenerValuacionVehiculoCESVI(String vin, BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idReporteCabina) throws Exception {
		BigDecimal valorEstimadoPesos = null;
		
		try {	
			Respuesta respuestaCesvi = informacionVehicularService.obtenerInformacionVehiculo(vin);
			
			if (respuestaCesvi != null && DiferenciasAmis.VIN_SIN_ERRORES.equals(respuestaCesvi.getRESULTADO()) &&
					respuestaCesvi.getPRECIO_VENTA() != null) {
				valorEstimadoPesos = new BigDecimal (respuestaCesvi.getPRECIO_VENTA());
				
				if (idToCotizacion != null) {
					BitacoraCESVI bitacoraCESVI = new BitacoraCESVI();
					bitacoraCESVI.setIdToCotizacion(idToCotizacion);
					bitacoraCESVI.setNumeroInciso(numeroInciso == null ? BigDecimal.ONE: numeroInciso);
					bitacoraCESVI.setIdReporteCabina(idReporteCabina);
					bitacoraCESVI.setNumeroSerie(vin);
					bitacoraCESVI.setValorComercial(valorEstimadoPesos);
					bitacoraCESVIService.guardarBitacora(bitacoraCESVI);
				}
			}
		} catch (Exception e) {
			LOG.error("Ocurrio un error en FronterizosService.obtenerValuacionVehiculoCESVI()", e);
			throw e;
		}
		
		return valorEstimadoPesos;
	}
	
	private BigDecimal obtenerValuacionVehiculoKBB(String vin, Long idToNegocio, 
			String stateId, VehicleConfiguration vehicleConfiguration, BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idReporteCabina) throws Exception {
		VehicleValuation vehicleValuation = null;
		BigDecimal valorEstimadoDolares = null;
		BigDecimal valorEstimadoPesos = null;
		
		try {			
			String zipCode = negocioEstadoDescuentoDao.obtenerZipCode(idToNegocio, stateId);
			String authKey = this.findKey() != null ? this.findKey().getAuthKey() : "";
			
			GregorianCalendar versionDate = new GregorianCalendar();
			versionDate.setTime(new Date());
			Release2008R2Proxy release2008R2Proxy = new Release2008R2Proxy();
			release2008R2Proxy._getDescriptor().setEndpoint(sistemaContext.getKBBEndPoint());
			
			vehicleValuation = release2008R2Proxy.getVehicleValuesByVehicleConfiguration(authKey, vehicleConfiguration,
			ApplicationCategory.CONSUMER, this.getVehicleCondition(sistemaContext.getCondicionFronterizo()), zipCode, 
			DatatypeFactory.newInstance().newXMLGregorianCalendar(versionDate));
			
			if (vehicleValuation != null && vehicleValuation.getValuationPrices() != null &&
					vehicleValuation.getValuationPrices().getValue() != null &&
					vehicleValuation.getValuationPrices().getValue().getValuation() != null) {
				valorEstimadoDolares = vehicleValuation.getValuationPrices().getValue().getValuation().get(0).getValue();
				
				if (valorEstimadoDolares != null) {
					valorEstimadoPesos = obtieneValorEstimadoPesos(valorEstimadoDolares);
					
					if (idToCotizacion != null) {
						BitacoraKBB bitacoraKBB = new BitacoraKBB();
						bitacoraKBB.setIdToCotizacion(idToCotizacion);
					    bitacoraKBB.setNumeroInciso(numeroInciso == null ? BigDecimal.ONE: numeroInciso);
					    bitacoraKBB.setIdReporteCabina(idReporteCabina);
						bitacoraKBB.setNumeroSerie(vin);
						bitacoraKBB.setValorComercial(valorEstimadoPesos);
						bitacoraKBB.setValorComercialUSD(valorEstimadoDolares);
						bitacoraKBBService.guardarBitacora(bitacoraKBB);
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Ocurrio un error en FronterizosService.obtenerValuacionVehiculo()", e);
			throw e;
		}
		
		return valorEstimadoPesos;
	}
	
	private VehicleCondition getVehicleCondition( String param ){
		
		if (param != null){
			switch (Integer.parseInt(param)) {
            case 1:  return VehicleCondition.EXCELLENT;
			case 2:  return VehicleCondition.VERY_GOOD;
			case 3:  return VehicleCondition.GOOD;
			case 4:  return VehicleCondition.FAIR;
			case 5:  return VehicleCondition.POOR;
			default: return VehicleCondition.EXCELLENT;
			}
		}
		else{
			return VehicleCondition.EXCELLENT;
		}		
	}
	
	private BigDecimal obtieneValorEstimadoPesos (BigDecimal valorEstimadoDolares) throws Exception {
		Double cambio = null;
		BigDecimal valorEstimadoPesos = null;
		
		try {
			cambio = tipoCambioService.obtieneTipoCambioPorDia(new Date(), "SISTEMAS");
			
			if (cambio != null) {
				valorEstimadoPesos = valorEstimadoDolares.multiply(new BigDecimal(cambio));
			}
		} catch(Exception e) {
			LOG.error("Ocurrio un error en FronterizosService.obtieneValorEstimadoPesos()", e);
			throw e;
		}
		
		return valorEstimadoPesos;
	}
	
	public String traerError( List<String> errores ){	
		return StringUtils.join(errores.iterator(), "");
	}

	@Override
	public HashMap<String, Map<String, String>> mostrarInformacionVehicular(String vin, NegocioSeccion negocioSeccion, String idMoneda, String idEstado,
			BigDecimal idToCotizacion, BigDecimal numeroInciso, CotizacionDTO cotizacion) throws Exception{
		
		HashMap<String, Map<String, String>> map = null;
		String respuesta = null;
		Short consultaNumSerie = negocioSeccion.getConsultaNumSerie();
		VehicleConfiguration vehicleConfiguration = null;
		Long idToNegocio = negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio();
		
		if ( consultaNumSerie == this.CONSULTA_NUM_TRUE ){			
			Short tipoValidacion = negocioSeccion.getSeccionDTO().getTipoValidacionNumSerie();
			
			if ( tipoValidacion == SeccionDTO.TIPO_VALIDACION_CESVI){
				if (informacionVehicularService.validacionActiva(DiferenciasAmis.VALIDACION_VIN_INDIVIDUAL_ACTIVADA)){
					Respuesta respuestaCesvi = informacionVehicularService.obtenerInformacionVehiculo(vin);
					if (respuestaCesvi != null) {
						if (DiferenciasAmis.VIN_SIN_ERRORES.equals(respuestaCesvi.getRESULTADO())) {
							map = this.getCesvi( respuestaCesvi, negocioSeccion, idMoneda, cotizacion );
							if (respuestaCesvi.getCVE_AMIS() == null) {
								map = this.llenarMap("claveAmis", respuestaCesvi.getCVE_AMIS(), "clavesAmis", map);
							}
							
							if (respuestaCesvi.getCVE_SESA() == null) {
								map = this.llenarMap("claveSesa", respuestaCesvi.getCVE_SESA(), "clavesSesa", map);
							}
						} else {
							map = this.llenarMap("ob", respuestaCesvi.getObservacionesSesa(), "oSesas", map);
						}
					}
				}
				else{
					map = this.llenarMap("desactivada", "validacion desactivada", "desactivada", map);
				}
			}
			else{	/***TIPO VALIDACION KBB*/
				if (this.isActive()){
					
					LOG.info(":::: entra a validar mediante kbb :::");
					
					respuesta = this.validateVIN(vin);				
					if ( respuesta != null && respuesta.trim().equalsIgnoreCase(VALUE_TRUE) ){
						
						vehicleConfiguration = this.obtenerInformacionVehiculo(vin, idToNegocio, idEstado );
						if ( vehicleConfiguration != null ){					
							map = this.getFronterizo( vehicleConfiguration, negocioSeccion, idMoneda );					
						}
					}					
				}
				else{
					map = this.llenarMap("desactivada", "validacion desactivada", "desactivada", map);
				}
			}
		}
		return map;
	}
	
	private HashMap<String, Map<String, String>> getFronterizo(VehicleConfiguration vehicleConfiguration,
			NegocioSeccion negocioSeccion, String idMoneda) {
		LOG.info(":::::::::>>>> ENTRA AL METODO getFronterizo");
		HashMap<String, Map<String, String>> maps = new HashMap<String, Map<String, String>>();

		try {
			
			BigDecimal idToNegSeccion = negocioSeccion.getIdToNegSeccion();

			IdStringPair marca = vehicleConfiguration.getMake();
			MarcaVehiculoDTO marcaDTO = this.obtenerMarca(marca.getValue().getValue(), idToNegSeccion);
			LOG.info("::::::::: [ INFO ] VALIDA SI LA DESCRIPCION DE MARCA DE VEHICULO ES NULL O VACIA " + (marcaDTO.getDescripcionMarcaVehiculo() != null && !marcaDTO.getDescripcionMarcaVehiculo().equals("")));
			if ( marcaDTO.getDescripcionMarcaVehiculo() != null && !marcaDTO.getDescripcionMarcaVehiculo().equals("") ){
				LOG.info(":::: [ INFO ] MANDA A LLENAR EL MAPA 'marca' ");
				maps = this.llenarMap( marcaDTO.getIdTcMarcaVehiculo().toString() , marcaDTO.getDescripcionMarcaVehiculo(),	"marca", maps );
				IdStringPair descripcion = vehicleConfiguration.getTrim();
				LOG.info(":::: [ INFO ] MANDA A OBTENERT EL ESTILO DE VEHICULO");
				EstiloVehiculoDTO estiloVehiculoDTO = this.obtenerEstilo(marcaDTO.getIdTcMarcaVehiculo(), descripcion.getValue().getValue(),idToNegSeccion);
				LOG.info("EVALIA LA DESCRICION DEL ESTILO DEL VEHICULO SI ES DIF DE NULL O VACIO " + ( estiloVehiculoDTO.getDescription() != null && !estiloVehiculoDTO.getDescription().equals("")));
				if ( estiloVehiculoDTO.getDescription() != null && !estiloVehiculoDTO.getDescription().equals("") ){
					LOG.info(" :::: [ INFO ] MANDA A OBTENER LOS MODELOS");
					Map<String, String> listModelo= listadoService.
					getMapModeloVehiculoPorMonedaEstiloNegocioSeccionString(new BigDecimal(idMoneda), estiloVehiculoDTO.getId().toString(), idToNegSeccion);
					LOG.info(":::: [ INFO ] MANDA A LLENAR EL MAPA 'estilo' ");
					maps = this.llenarMap( estiloVehiculoDTO.getId().toString(), estiloVehiculoDTO.getDescription(), "estilo", maps );					
					IdStringPair model = vehicleConfiguration.getYear();
					LOG.info("::: [ INFO ] MANDA A COMPARAR MODELOS");
					if ( this.compararModelos(model.getValue().getValue(), listModelo) ){
						LOG.info(":::: [ INFO ] ENCONTR\u00d3 UN MODELO QUE CONICIDE.");
						maps.put("modelo", listModelo);
						HashMap<String, String> map = this.llenarMapValue( descripcion.getValue().getValue() );
						maps.put("descripcion", map);
						LOG.info(":::: [ INFO ] DESPUES DE COMPARAMODELO 'TRUE' HAPPYROAD " + descripcion.getValue().getValue());
						maps = this.llenarMap( "coincideEstilo", "COINCIDE", "coincideEstilo", maps );
						maps = this.llenarMap( "modeloSel", model.getValue().getValue(), "modeloSel", maps );
					}
					else{
						LOG.info(":::: [ INFO ] NO ENCONTR\u00d3 UN MODELO QUE CONICIDA.");
						maps.put("modelo", listModelo);
						LOG.info(":::: [ INFO ] DESPUES DE COMPARAMODELO 'FALSE' HAPPYROAD " + descripcion.getValue().getValue());
						HashMap<String, String> map = this.llenarMapValue( descripcion.getValue().getValue() );
						maps.put("descripcion", map);
					}
				}
				else{
					LOG.info("::::::: [ INFO ] LA DESCRIPCION DEL VEHICULO ES NULA \u00d3 VACIA MANDA A LLAMAR A TRAER DEFAULTS 2 ");
					maps = this.traerDefaults(vehicleConfiguration, idToNegSeccion, idMoneda);
				}
			}
			else{
				LOG.info("::::::: [ INFO ] LA DESCRIPCION DEL VEHICULO ES NULA \u00d3 VACIA MANDA A LLAMAR A TRAER DEFAULTS 1 ");
				maps = this.traerDefaults(vehicleConfiguration, idToNegSeccion, idMoneda);
			}

		} catch (Exception e) {			
			LOG.error("Ocurrio un error en FronterizosService.getFronterizo()", e);
			maps = new HashMap<String, Map<String, String>>();
		}
		return maps;
	}
	
	private  HashMap<String, Map<String, String>> llenarMap( String id, String value, 
			String caracteristica, HashMap<String, Map<String, String>> maps ){
		LOG.info(":::::: >>>> ENTRA AL METODO llenarMap(" + id + ", " + value + ", " + caracteristica + ", mapa" + ");");
		HashMap<String, String> map = new HashMap<String, String>();
		map.put( id, value );		
			
		if (maps == null) {
			maps = new HashMap<String, Map<String, String>>();
		}
		maps.put(caracteristica, map);
		LOG.info(":::::: <<<<< RETORNA MAPS");
		return maps;		
	}
	
	private HashMap<String, String> llenarMapValue( String value ){
		
		HashMap<String, String> map = new HashMap<String, String>();		
		map.put( "value", value );		
		return map;		
	}
	
	@SuppressWarnings("unchecked")
	private MarcaVehiculoDTO obtenerMarca( String marca, BigDecimal idNegocioSeccion ) throws Exception {
		
		MarcaVehiculoDTO respuesta = new MarcaVehiculoDTO();
		String queryString = "";
		
		if(idNegocioSeccion != null && marca != null){
			
			marca = "%" +  marca.trim().toUpperCase() + "%";
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idNegocioSeccion);
			SeccionDTO seccion = entidadService.findById(SeccionDTO.class, negocioSeccion.getSeccionDTO().getIdToSeccion());
			String claveTipoBien = seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien();
			
			Map<String,Object> params = new HashMap<String, Object>();			
			
			queryString = "select model from MarcaVehiculoDTO model " +
			"where UPPER(model.descripcionMarcaVehiculo) like :marca " +			
			"and model.tipoBienAutosDTO.claveTipoBien = :claveTipoBien ";
			
			params.put("marca", marca);
			params.put("claveTipoBien", claveTipoBien);			
			
			List<MarcaVehiculoDTO> list = (List<MarcaVehiculoDTO>)entidadService.executeQueryMultipleResult(queryString, params);			
			if (list != null && list.size() > 0 ){
				respuesta = list.get(0);
			}	
		}
		return respuesta;
	}	

	@SuppressWarnings("unchecked")
	private EstiloVehiculoDTO obtenerEstilo( BigDecimal idMarcaVehiculo, String descripcion, BigDecimal idToNegSeccion ) throws Exception {
		LOG.info(":::: >>>> ENTRA AL METODO obtenerEstilo(" + idMarcaVehiculo + ", " + descripcion + ", " + idToNegSeccion + ");");
		EstiloVehiculoDTO estiloVehiculoDTO = new EstiloVehiculoDTO();
		Map<String, Object> params = new HashMap<String, Object>();
		String queryString = "";	
		descripcion = "%" + descripcion.trim().toUpperCase() + "%";
		
		queryString = "select model from EstiloVehiculoDTO model " +
		"where model.marcaVehiculoDTO.idTcMarcaVehiculo = :idTcMarcaVehiculo " +
		"and UPPER(model.descripcionEstilo) like :descripcion ";
		
		params.put("idTcMarcaVehiculo", idMarcaVehiculo);
		params.put("descripcion", descripcion);

		List<EstiloVehiculoDTO> estilos = (List<EstiloVehiculoDTO>)entidadService.executeQueryMultipleResult(queryString, params);
		
		if ( estilos != null && estilos.size() > 0 ){			
			estiloVehiculoDTO = estilos.get(0);
		}
		LOG.info(":::: <<<<<< SALE DEL METODO obtenerEstilo");
		return estiloVehiculoDTO;
	}
	
	private boolean compararModelos( String modelo, Map<String, String> map ){
		LOG.info("::: >>>> ENTRA AL METODO compararModelos(" + modelo + ", " + map + ");");
		for (Entry<String, String> entry : map.entrySet())
		{
			if (entry.getValue().equalsIgnoreCase(modelo)){
				LOG.info("::: [ INFO ] ENCUENTRA UN MODELO COMPATIBBLE");
				LOG.info("::: <<<<< REGRESA LA BANDERA EN TRUE");
				return true;
			}
		}
		LOG.info("::: [ INFO ] NO ENCUENTRA UN MODELO COMPATIBBLE");
		LOG.info("::: <<<<< REGRESA LA BANDERA EN FALSE");
		return false;
	}
	
	private HashMap<String, Map<String, String>> traerDefaults( VehicleConfiguration vehicleConfiguration,
			BigDecimal idToNegSeccion, String idMoneda){
		LOG.info("::::::::: >>>>>>>> ENTRA AL METODO traerDefaults");
		HashMap<String, Map<String, String>> maps = new HashMap<String, Map<String, String>>();
		LOG.info(":::::: [ INFO ] INTENTA TRAER LA MARCA DEFAULT DE TOSECCION");
		try {
			MarcaVehiculoDTO marcaDTO = this.traerMarcaDefault( idToNegSeccion );
			LOG.info(":::::: [ INFO ] VALIDA SI LA MARCADTO EN EL ATRIBUTO IDMARCAVEHICULO ES DIF DE NULO O VACIO " + (marcaDTO.getIdTcMarcaVehiculo() != null && marcaDTO.getIdTcMarcaVehiculo().toString() != null));
			if (marcaDTO.getIdTcMarcaVehiculo() != null && marcaDTO.getIdTcMarcaVehiculo().toString() != null){				
				maps = this.llenarMap( marcaDTO.getIdTcMarcaVehiculo().toString(), marcaDTO.getDescripcionMarcaVehiculo(), "marca", maps );
				LOG.info(":::::: [ INFO ] MANDA A TRAER EL ESTILO DEFAULT ");
				EstiloVehiculoDTO estiloVehiculoDTO = this.traerEstiloDefault();
				LOG.info(":::::: [ INFO ] EVALUA SI EL ESTILO VEHICULO EN EL ATRIBUTO ID ES DIF DE NULL " + (estiloVehiculoDTO.getId()!=null && estiloVehiculoDTO.getId().toString()!=null));
				if (estiloVehiculoDTO.getId()!=null && estiloVehiculoDTO.getId().toString()!=null){
					maps = this.llenarMap( estiloVehiculoDTO.getId().toString(), estiloVehiculoDTO.getId().getClaveEstilo() + " - " + estiloVehiculoDTO.getDescripcionEstilo(), "estilo", maps );
					LOG.info("::::::: [ INFO ] MANDA A LLENAR LA LISTA DE MODELOS  ");
					Map<String, String> listModelo= listadoService.
					getMapModeloVehiculoPorMonedaEstiloNegocioSeccionString(new BigDecimal(idMoneda), estiloVehiculoDTO.getId().toString(), idToNegSeccion);

					IdStringPair idStringPair = vehicleConfiguration.getYear();
					String modelo = idStringPair.getValue().getValue() != null ? idStringPair.getValue().getValue() : "";
					LOG.info("::::::: [ INFO ] MANDA A COMPARAR LOS MODELOS  ");
					if ( this.compararModelos(modelo, listModelo) ){
						LOG.info("::::: [ INFO ] ENCONTR\u00d3 UN MODELO EXISTENTE ");
						maps.put("modelo", listModelo);
						LOG.info("::::: [ INFO ] DESPUES COMPARARMODELOS 'TRUE' AGREGA EL MODELO QUE TRAE 'vehicleConfiguration' AL MAPA ");
						LOG.info("::::: [ INFO ] " + vehicleConfiguration.getMake().getValue().getValue() + " - " + vehicleConfiguration.getModel().getValue().getValue() + " - " +  vehicleConfiguration.getTrim().getValue().getValue());
						HashMap<String, String> map = this.llenarMapValue( vehicleConfiguration.getMake().getValue().getValue() + " - " + vehicleConfiguration.getModel().getValue().getValue() + " - " +  vehicleConfiguration.getTrim().getValue().getValue() );
						maps.put("descripcion", map);
						maps = this.llenarMap( "coincideEstilo", "COINCIDE", "coincideEstilo", maps );
						maps = this.llenarMap( "modeloSel", modelo, "modeloSel", maps );
					}
					else{
						LOG.info("::::: [ INFO ] NO ENCONTR\u00d3 UN MODELO EXISTENTE ");
						maps.put("modelo", listModelo);
						LOG.info("::::: [ INFO ] DESPUES COMPARARMODELOS 'FALSE' AGREGA EL MODELO QUE TRAE 'vehicleConfiguration' AL MAPA ");
						LOG.info("::::: [ INFO ] " + vehicleConfiguration.getMake().getValue().getValue() + " - " + vehicleConfiguration.getModel().getValue().getValue() + " - " +  vehicleConfiguration.getTrim().getValue().getValue());
						HashMap<String, String> map = this.llenarMapValue( vehicleConfiguration.getMake().getValue().getValue() + " - " + vehicleConfiguration.getModel().getValue().getValue() + " - " +  vehicleConfiguration.getTrim().getValue().getValue() );
						maps.put("descripcion", map);
					}
				}
				else{
					LOG.info("::::: [ INFO ] CREA UN MAP VACIO ");
					maps = new HashMap<String, Map<String, String>>();
				}
			}
			
		} catch (Exception e) {
			LOG.error("Ocurrio un error en FronterizosService.traerDefaults()", e);
		}
		LOG.info(":::::::: <<<<<<<<<< REGRESA EL MAPA CON LOS DEFAULTS   ");
		return maps;
	}
	
	private  MarcaVehiculoDTO traerMarcaDefault( BigDecimal idNegocioSeccion ) throws Exception{
		
		MarcaVehiculoDTO respuesta = new MarcaVehiculoDTO();
		
		String marca = this.getMarcaFronterizo();
		if ( marca != null ){
			respuesta = this.obtenerMarca(marca, idNegocioSeccion);
		}
		
		return respuesta;
	}
	
	private  EstiloVehiculoDTO traerEstiloDefault() throws Exception{
		LOG.info(":::::::: >>>> ENTRA AL METODO traerEstiloDefault();");
		EstiloVehiculoDTO respuesta = new EstiloVehiculoDTO();
		LOG.info(":::::::: [ INFO ] MANDA A OBTENER EL ESTILOFRONTERIZO");
		String idEstilo = this.getEstiloFronterizo();
		if ( idEstilo != null ){
			List<EstiloVehiculoDTO> estilos = entidadService.findByProperty(EstiloVehiculoDTO.class, "id.claveEstilo", idEstilo);
			
			if ( estilos != null && estilos.size() > 0 ){			
				respuesta = estilos.get(0);
			}
		}
		LOG.info(":::::::: <<<<<<<<< REGRESA EL ESTILO");
		return respuesta;
	}	
	
	private HashMap<String, Map<String, String>> getCesvi( Respuesta respuesta,
			NegocioSeccion negocioSeccion, String idMoneda, CotizacionDTO cotizacion ){
		HashMap<String, Map<String, String>> maps = new HashMap<String, Map<String, String>>();
		EstiloVehiculoDTO estiloVehiculo = null;
		MarcaVehiculoDTO marcaDTO = null;
		BigDecimal idToNegSeccion = negocioSeccion.getIdToNegSeccion();
		
		try{
			
			if ( respuesta.getCVE_AFIRME()!=null ){
				String clave = Utilerias.llenarIzquierda(respuesta.getCVE_AFIRME(), "0", 5);
				
				estiloVehiculo = cargaMasivaService.obtieneEstiloVehiculoDTOPorClaveAMIS(cotizacion, negocioSeccion, clave);
				if (estiloVehiculo!=null && estiloVehiculo.getId()!=null 
						&& estiloVehiculo.getId().toString()!=null && !estiloVehiculo.getId().toString().equals("")){
					
					marcaDTO = estiloVehiculo.getMarcaVehiculoDTO();
					if (marcaDTO!=null && marcaDTO.getIdTcMarcaVehiculo()!=null){
						maps = this.llenarMap( marcaDTO.getIdTcMarcaVehiculo().toString(), marcaDTO.getDescripcionMarcaVehiculo(), "marca", maps );
						
						String claveYDescripcion = estiloVehiculo.getId().getClaveEstilo() + " - " + estiloVehiculo.getDescription();
						maps = this.llenarMap( estiloVehiculo.getId().toString(), claveYDescripcion, "estilo", maps );
						Map<String, String> listModelo= listadoService.
						getMapModeloVehiculoPorMonedaEstiloNegocioSeccionString(new BigDecimal(idMoneda), estiloVehiculo.getId().toString(), idToNegSeccion);
						
						String modelo = respuesta.getANO();	
						if ( this.compararModelos(modelo, listModelo) ){
							maps.put("modelo", listModelo);
							HashMap<String, String> map = this.llenarMapValue( claveYDescripcion );
							maps.put("descripcion", map);
							maps = this.llenarMap( "coincideEstilo", "COINCIDE", "coincideEstilo", maps );
							maps = this.llenarMap( "modeloSel", modelo, "modeloSel", maps );
						}
						else{
							maps.put("modelo", listModelo);
							HashMap<String, String> map = this.llenarMapValue( claveYDescripcion );
							maps.put("descripcion", map);
						}
					}
					else{
						maps = new HashMap<String, Map<String, String>>();
					}					
				}
			}
		}
		catch(Exception e){
			LOG.error("Ocurrio un error en FronterizosService.getCesvi()", e);
			maps = new HashMap<String, Map<String, String>>();			
		}
		return maps;
	}
	
	public Double obtieneValorSumaAseguradaCot (NegocioCobPaqSeccion coberturaNegocio, CoberturaSeccionDTO coberturaSeccion, BigDecimal idCotizacion, Long numeroInciso, String numeroSerie, String estadoId) {
		Double valorSumaAsegurada = null;

		try {
			if (NegocioSeccion.PERMITE_CONSULTA_NUM_SERIE.compareTo(coberturaNegocio.getNegocioPaqueteSeccion().getNegocioSeccion().getConsultaNumSerie()) == 0) {
				if (SeccionDTO.TIPO_VALIDACION_KBB.compareTo(coberturaSeccion.getSeccionDTO().getTipoValidacionNumSerie()) == 0) {
					if (isActive()) {
						Double valorSumaAseguradaKBB = obtieneValorComercialKBBCot(coberturaNegocio, idCotizacion, numeroInciso, numeroSerie);
						
						if (valorSumaAseguradaKBB == null) {
							valorSumaAseguradaKBB = obtieneValorComercialKBB(idCotizacion, 
									coberturaNegocio.getNegocioPaqueteSeccion().getNegocioSeccion().getNegocioTipoPoliza().
									getNegocioProducto().getNegocio().getIdToNegocio(), 
									numeroSerie, estadoId, numeroInciso.intValue(), null, null, null);
						}
						
						if (valorSumaAseguradaKBB != null) {
							valorSumaAsegurada = valorSumaAseguradaKBB;
						}
					}
				} else {
					if (informacionVehicularService.validacionActiva(DiferenciasAmis.VALIDACION_VIN_INDIVIDUAL_ACTIVADA)){
						Double valorSumaAseguradaCESVI = obtieneValorComercialCESVICot(coberturaNegocio, idCotizacion, numeroInciso, numeroSerie);
						
						if (valorSumaAseguradaCESVI == null) {
							valorSumaAseguradaCESVI = obtieneValorComercialCESVI(idCotizacion, 
							coberturaNegocio.getNegocioPaqueteSeccion().getNegocioSeccion().getNegocioTipoPoliza().
							getNegocioProducto().getNegocio().getIdToNegocio(), 
							numeroSerie, estadoId, numeroInciso.intValue(), null, null, null);
						}
						
						if (valorSumaAseguradaCESVI != null) {
							valorSumaAsegurada = valorSumaAseguradaCESVI;
						}
					}
				}  
			}
		} catch (Exception e) {
			LOG.error("FronterizosServiceImpl.obtieneValorSumaAseguradaCot() --> Ocurrio un error al obtener la suma asegurada", e);
		}

		return valorSumaAsegurada;
	}	
	
	private Double obtieneValorComercialKBBCot (NegocioCobPaqSeccion coberturaNegocio, BigDecimal idCotizacion, Long numeroInciso, String numeroSerie) {
		Double valorComercial = null;
		
		try {
			BitacoraKBB bitacoraKBB = new BitacoraKBB();
			bitacoraKBB.setIdToCotizacion(idCotizacion);
			bitacoraKBB.setNumeroInciso(new BigDecimal(numeroInciso));
			bitacoraKBB.setNumeroSerie(numeroSerie.toUpperCase());
			valorComercial = bitacoraKBBService.obtenerValorComercial(bitacoraKBB);
		} catch (Exception e) {
			LOG.error("FronterizosServiceImpl.obtieneValorComercialKBB() --> Ocurrio un error al obtener la suma asegurada", e);
		}		
		
		return valorComercial;
	}
	
	private Double obtieneValorComercialCESVICot (NegocioCobPaqSeccion coberturaNegocio, BigDecimal idCotizacion, Long numeroInciso, String numeroSerie) {
		Double valorComercial = null;
		
		try {
			BitacoraCESVI bitacoraCESVI = new BitacoraCESVI();
			bitacoraCESVI.setIdToCotizacion(idCotizacion);
			bitacoraCESVI.setNumeroInciso(new BigDecimal(numeroInciso));
			bitacoraCESVI.setNumeroSerie(numeroSerie.toUpperCase());
			valorComercial = bitacoraCESVIService.obtenerValorComercial(bitacoraCESVI);
		} catch (Exception e) {
			LOG.error("FronterizosServiceImpl.obtieneValorComercialCESVICot() --> Ocurrio un error al obtener la suma asegurada", e);
		}		
		
		return valorComercial;
	}
	
	public Double obtieneValorSumaAseguradaPol (Date fechaHoraOcurrido, BigDecimal idToCotizacion, 
			Integer numeroInciso, Short tipoValidacionNumSerie, String numeroSerie, String estadoId, Long idReporteCabina, 
			Double valorSumaAseguradaMin, Double valorSumaAseguradaMax) {
		Double valorSumaAsegurada = null;
		
		try {
			BitemporalCotizacion bitempCotizacion = incisoViewService.getCotizacion(idToCotizacion.longValue(), fechaHoraOcurrido);

			if (bitempCotizacion != null) {
				BitemporalInciso biInciso = incisoBitemporalService.findBitemporalIncisoByNumero(bitempCotizacion.getContinuity(),
						numeroInciso, new DateTime(fechaHoraOcurrido), null, false);

				if (biInciso != null) {
					Map<String, Object> params = new HashMap<String, Object>();
					params.put("continuity.id", biInciso.getContinuity().getAutoIncisoContinuity().getId());
					List<BitemporalAutoInciso> biAutoIncisos = (List<BitemporalAutoInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalAutoInciso.class, 
							params, new DateTime(fechaHoraOcurrido), null, false);

					for (BitemporalAutoInciso biAutoInciso : biAutoIncisos) {
						NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, new BigDecimal(biAutoInciso.getValue().
								getNegocioSeccionId()));
						if (NegocioSeccion.PERMITE_CONSULTA_NUM_SERIE.compareTo(negocioSeccion.getConsultaNumSerie()) == 0) {
							if (SeccionDTO.TIPO_VALIDACION_KBB.compareTo(tipoValidacionNumSerie) == 0) {
								if (isActive()) {
									Double valorSumaAseguradaKBB = obtieneValorComercialKBB(idToCotizacion, 
											negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
											numeroSerie, estadoId, numeroInciso, idReporteCabina, valorSumaAseguradaMin, valorSumaAseguradaMax);
									if (valorSumaAseguradaKBB != null) {
										valorSumaAsegurada = valorSumaAseguradaKBB;
									}
								}
							} else {
								if (informacionVehicularService.validacionActiva(DiferenciasAmis.VALIDACION_VIN_INDIVIDUAL_ACTIVADA)){
									Double valorSumaAseguradaCESVI = obtieneValorComercialCESVI(idToCotizacion, 
											negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
											numeroSerie, estadoId, numeroInciso, idReporteCabina, valorSumaAseguradaMin, valorSumaAseguradaMax);
									if (valorSumaAseguradaCESVI != null) {
										valorSumaAsegurada = valorSumaAseguradaCESVI;
									}
								}
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("FronterizosServiceImpl.obtieneValorSumaAseguradaPol() --> Ocurrio un error al obtener la suma asegurada", e);
		}
		
		return valorSumaAsegurada;
	}
	
	private Double obtieneValorComercialKBB (BigDecimal idToCotizacion, Long idToNegocio, String numeroSerie,
			String estadoId, Integer numeroInciso, Long idReporteCabina, Double valorSumaAseguradaMin, Double valorSumaAseguradaMax) {
		Double valorComercial = null;
		
		try {
			String vinValido = validateVIN(numeroSerie);	
			
			if (vinValido != null && vinValido.trim().equalsIgnoreCase(FronterizosServiceImpl.VALUE_TRUE) ){		
				VehicleConfiguration vehicleConfiguration = obtenerInformacionVehiculo(numeroSerie, idToNegocio, estadoId);
				
				if (vehicleConfiguration != null) {
					BigDecimal valoraComercialBD = obtenerValuacionVehiculoKBB(numeroSerie, idToNegocio, estadoId, vehicleConfiguration,
							idToCotizacion, new BigDecimal(numeroInciso), idReporteCabina);
					
					if (valoraComercialBD != null) {
						Double valorComercialKBB = valoraComercialBD.doubleValue();
						if (valorComercialKBB != null) {
							if (valorSumaAseguradaMin == null && valorSumaAseguradaMax == null) {
								valorComercial = valorComercialKBB;
							} else if (valorComercialKBB.compareTo(valorSumaAseguradaMin) >= 0 &&
									valorComercialKBB.compareTo(valorSumaAseguradaMax) <= 0) {
								valorComercial = valorComercialKBB;
							}
						}
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Ocurrio un error en FronterizosServiceImpl.obtieneValorComercialKBBPol()", e);
		}
		
		return valorComercial;
	}
	
	private Double obtieneValorComercialCESVI (BigDecimal idToCotizacion, Long idToNegocio, String numeroSerie,
			String estadoId, Integer numeroInciso, Long idReporteCabina, Double valorSumaAseguradaMin, Double valorSumaAseguradaMax) {
		Double valorComercial = null;
		
		try {
			BigDecimal valoraComercialBD = obtenerValuacionVehiculoCESVI(numeroSerie, idToCotizacion, new BigDecimal(numeroInciso), idReporteCabina);
			
			if (valoraComercialBD != null) {
				Double valorComercialCESVI = valoraComercialBD.doubleValue();
				if (valorComercialCESVI != null) {
					if (valorSumaAseguradaMin == null && valorSumaAseguradaMax == null) {
						valorComercial = valorComercialCESVI;
					} else if (valorComercialCESVI.compareTo(valorSumaAseguradaMin) >= 0 &&
							valorComercialCESVI.compareTo(valorSumaAseguradaMax) <= 0) {
						valorComercial = valorComercialCESVI;
					}
				}
			}
		} catch (Exception e) {
			LOG.error("Ocurrio un error en FronterizosServiceImpl.obtieneValorComercialCESVI()", e);
		}
		
		return valorComercial;
	}
	
	@Override
	public void migrarClavesCotizacion(CotizacionDTO cotizacionDTO) throws Exception{
		
		for (IncisoCotizacionDTO inciso : cotizacionDTO.getIncisoCotizacionDTOs()) {			
			if(inciso.getIncisoAutoCot() != null) {
				String claveAmis = inciso.getIncisoAutoCot().getClaveAmis();
				String claveSesas = inciso.getIncisoAutoCot().getClaveSesa();					
				String estiloId = inciso.getIncisoAutoCot().getEstiloId();
				this.actualizarClaves(claveAmis, claveSesas, estiloId);
			}
		}
	}
	
	@Override
	public void migrarClavesCotizacionBitemporal(BitemporalCotizacion cotizacion, DateTime validoEn) throws Exception{
	
		Map<String, Object> params = new HashMap<String, Object>();
        params.put("continuity.cotizacionContinuity.id", cotizacion.getContinuity().getId());
        List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(
        		BitemporalInciso.class, params, validoEn, null, false);
        
        for (BitemporalInciso inciso : incisos) {
			Map<String, Object> paramsAut = new HashMap<String, Object>();
			paramsAut.put("continuity.id", inciso.getContinuity().getAutoIncisoContinuity().getId());
			List<BitemporalAutoInciso> autos = (List<BitemporalAutoInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalAutoInciso.class, 
					paramsAut, validoEn, null, false);
			
			BitemporalAutoInciso auto = null;
			if(autos != null && !autos.isEmpty()){
				auto = autos.get(0);
			}
			if(auto != null) {
				String claveAmis = auto.getValue().getClaveAmis();
				String claveSesas = auto.getValue().getClaveSesa();				
				String estiloId = auto.getValue().getEstiloId();				
				this.actualizarClaves(claveAmis, claveSesas, estiloId);				
			}
        }
	}
	
	@Override
	public void actualizarClaves(String claveAmis, String claveSesas, String estiloId) throws Exception{
		
		boolean actualizar = false;
		EstiloVehiculoId id = new EstiloVehiculoId();
		id.setStrId(estiloId);
		EstiloVehiculoDTO estiloVehiculoDTO = entidadService.findById(EstiloVehiculoDTO.class, id);
		
		if ( estiloVehiculoDTO!=null ){
			if ( claveAmis != null && !claveAmis.equals("") && (estiloVehiculoDTO.getClaveAmis() == null 
					|| estiloVehiculoDTO.getClaveAmis().equals("")) ){
				estiloVehiculoDTO.setClaveAmis(claveAmis);
				actualizar = true;
			}
			if ( claveSesas != null && !claveSesas.equals("") && (estiloVehiculoDTO.getClaveSesas() == null 
					|| estiloVehiculoDTO.getClaveSesas().equals("")) ){
				estiloVehiculoDTO.setClaveSesas(claveSesas);
				actualizar = true;
			}
			if (actualizar){
				entidadDao.update(estiloVehiculoDTO);
			}
		}		
	}
	
	@Override
	public void guardarBitacora( BigDecimal idToCotizacion, BigDecimal numeroInciso, Long idReporteCabina,
			String vin, BigDecimal valorEstimadoPesos, BigDecimal valorEstimadoDolares, Short tipoValidacion ){
		
		try{
			tipoValidacion = tipoValidacion != null ? tipoValidacion : 0;
			if (SeccionDTO.TIPO_VALIDACION_KBB.compareTo(tipoValidacion) == 0) {
				BitacoraKBB bitacoraKBB = new BitacoraKBB();
				bitacoraKBB.setIdToCotizacion(idToCotizacion);
			    bitacoraKBB.setNumeroInciso(numeroInciso == null ? BigDecimal.ONE: numeroInciso);
			    bitacoraKBB.setIdReporteCabina(idReporteCabina);
				bitacoraKBB.setNumeroSerie(vin);
				bitacoraKBB.setValorComercial(valorEstimadoPesos);
				bitacoraKBB.setValorComercialUSD(valorEstimadoDolares);
				bitacoraKBBService.guardarBitacora(bitacoraKBB);		
			}
			else{
				BitacoraCESVI bitacoraCESVI = new BitacoraCESVI();
				bitacoraCESVI.setIdToCotizacion(idToCotizacion);
				bitacoraCESVI.setNumeroInciso(numeroInciso == null ? BigDecimal.ONE: numeroInciso);
				bitacoraCESVI.setIdReporteCabina(idReporteCabina);
				bitacoraCESVI.setNumeroSerie(vin);
				bitacoraCESVI.setValorComercial(valorEstimadoPesos);
				bitacoraCESVIService.guardarBitacora(bitacoraCESVI);				
			}
		}
		catch( Exception e ){
			LOG.error("Ocurrio un error en FronterizosServiceImpl.guardarBitacora()", e);
		}
	}
	
	@Override
	public void generateKey(){
    	
		try {
    		String respuesta = this.getLogin();				
			LoginKBB loginKBB = new LoginKBB();
			loginKBB.setId(this.ID_KEY);
			loginKBB.setAuthKey(respuesta);
			loginKBB.setCreationDate(new Date());				
			this.saveKey(loginKBB);
		
		} catch (Exception e) {
			LOG.error("Ocurrió un error en FronterizosServiceImpl.generateKey(): ",e);
		}
    }

    public void generateKeyJob() {
		
		LOG.info("Iniciando Tarea generateKeyJob: Iniciar sesión en KBB.");
		if ( this.isActive() ){
			LOG.info("Tarea generateKeyJob. Activa.");    			
			this.generateKey();
		}
		LOG.info("Saliendo de Tarea generateKeyJob: Iniciar sesión en KBB.");	
	}
	
	private static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
	    long diffInMillies = date2.getTime() - date1.getTime();
	    return timeUnit.convert(diffInMillies,TimeUnit.MILLISECONDS);
	}
	
	public void initialize() {
		String timerInfo = "TimerFronterizos";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(0);
				expression.hour("*/1");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerFronterizos", false));
				
				LOG.info("Tarea TimerFronterizos configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerFronterizos");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerFronterizos:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		generateKeyJob();
	}
}