package mx.com.afirme.midas2.service.impl.sapamis.sistemas;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.sapamis.sistemas.SapAmisSalvamentoDao;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisSalvamento;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisDistpatcherWSService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisSalvamentoService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisSalvamentoServiceImpl.
 * 
 * Descripcion: 		Se utiliza para el manejo del Modulo de Salvamentos.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisSalvamentoServiceImpl implements SapAmisSalvamentoService{
	private static final long serialVersionUID = 1L;
	
    @EJB private SapAmisSalvamentoDao sapAmisSalvamentoDao;
	@EJB private SapAmisDistpatcherWSService sapAmisDistpatcherWSService;
	@EJB private SapAmisUtilsService sapAmisUtilsService; 
	
	private static final Long ESTATUS_PENDIENTE = new Long(1);
    private static final int CANTIDAD_DE_REGISTROS_PROCESAR = 1000;

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis() {
		this.sendRegSapAmis(sapAmisUtilsService.obtenerAccesos());
    }

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(String[] accesos) {
    	ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
    	parametrosConsulta.setEstatus(ESTATUS_PENDIENTE);
    	this.sendRegSapAmis(accesos, parametrosConsulta);
	}

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(ParametrosConsulta parametrosConsulta){
		this.sendRegSapAmis(sapAmisUtilsService.obtenerAccesos(), parametrosConsulta);
	}

    @Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
    public void sendRegSapAmis(String[] accesos, ParametrosConsulta parametrosConsulta){
    	List<SapAmisSalvamento> sapAmisSalvamentoList = this.obtenerPorFiltros(parametrosConsulta, CANTIDAD_DE_REGISTROS_PROCESAR, 1);
		for(SapAmisSalvamento sapAmisSalvamento: sapAmisSalvamentoList){
			sapAmisDistpatcherWSService.enviarSalvamento(sapAmisSalvamento, accesos);
		}
    }

	@Override
	public List<SapAmisSalvamento> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return sapAmisSalvamentoDao.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}
}