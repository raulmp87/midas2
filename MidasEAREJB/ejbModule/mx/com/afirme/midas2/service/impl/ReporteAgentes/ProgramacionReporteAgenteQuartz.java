package mx.com.afirme.midas2.service.impl.ReporteAgentes;

import static mx.com.afirme.midas2.utils.CommonUtils.isEmptyList;
import static mx.com.afirme.midas2.utils.CommonUtils.isNotNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isNull;
import static mx.com.afirme.midas2.utils.CommonUtils.isValid;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.domain.reporteAgentes.ConfigReporteAgenteDTO;
import mx.com.afirme.midas2.domain.reporteAgentes.LogReporteAgenteDTO;
import mx.com.afirme.midas2.domain.reporteAgentes.ReporteAgenteDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.MidasException;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

public class ProgramacionReporteAgenteQuartz {

	private EntidadService entidad;
	private ListadoService listadoService;
	// private ProgramacionReporteAgenteDelegate
	// programacionReporteAgenteDelegate;
	private ProgramacionReporteAgenteDelegateQuartz programacionReporteAgenteDelegateQuartz;

	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		try {
			List<ConfigReporteAgenteDTO> configReportes = entidad
					.findAll(ConfigReporteAgenteDTO.class);
			List<TareaProgramada> tasks = null;
			if (configReportes != null) {
				tasks = new ArrayList<TareaProgramada>();
				Map<Long, String> mapValorCatalogo = listadoService
						.mapValorCatalogoAgente("Periodicidad-Reportes");
				if (!isEmptyList(configReportes)) {
					for (ConfigReporteAgenteDTO config : configReportes) {
						if (isNotNull(config)) {
							Long idReporte = (isNotNull(config) && isNotNull(config
									.getReporteAgenteDTO())) ? config
									.getReporteAgenteDTO().getId() : null;
							Map<String, Object> params = new HashMap<String, Object>();
							params.put("reporteAgenteDTO.id", idReporte);
							List<LogReporteAgenteDTO> logs = entidad
									.findByPropertiesWithOrder(
											LogReporteAgenteDTO.class, params,
											"fechaEjecucion DESC");
							List<LogReporteAgenteDTO> list=new ArrayList<LogReporteAgenteDTO>();
							for(LogReporteAgenteDTO log:logs){
								if(isNotNull(log)){
									list.add(log);
								}
							}
							LogReporteAgenteDTO logReporteAgenteDTO = (!isEmptyList(list)) ? list
									.get(0) : null;
							// Si es nulo, es pk nunca fue ejecuta o no esta en
							// el log y es la primera vez,
							// O si se encuentra, pero no fue ejecutado en el
							// mismo dia de hoy.
							if (isNull(logReporteAgenteDTO)
									|| !DateUtils.isSameDay(logReporteAgenteDTO
											.getFechaEjecucion(), new Date())) {
								TareaProgramada task = new TareaProgramada();
								task.setId(config.getReporteAgenteDTO().getId());
								task.setFechaEjecucion(obtenerFecha(mapValorCatalogo
										.get(config.getPeriodicidad())));
								tasks.add(task);
							}
						}
					}
				}
			}
			return tasks;
		} catch (RuntimeException error) {
			error.printStackTrace();
		}
		return null;
	}

	public void executeTasks() {
		List<TareaProgramada> tasks = getTaskToDo(null);
		if (!isEmptyList(tasks)) {
			for (TareaProgramada task : tasks) {
				if (isNotNull(task)
						&& DateUtils.isSameDay(new Date(),
								task.getFechaEjecucion())) {
					// TODO llamar el delegate y pasarle el id del reporte
					Long id = task.getId();
					ReporteAgenteDTO reporte = entidad.findById(
							ReporteAgenteDTO.class, id);
					if (isNotNull(reporte)) {
						try {
							try {
								programacionReporteAgenteDelegateQuartz
										.cargarDatosInicialesPorReporte(reporte);
							} catch (MidasException e) {
								e.printStackTrace();
							}
							// Thread t = new Thread(new Ejecutable(reporte));
							// t.start();
						} catch (RuntimeException e) {
							String nombre = (isNotNull(reporte) && isValid(reporte
									.getNombre())) ? reporte.getNombre() : "";
							System.out
									.println("Error al cargar los datos del reporte ["
											+ nombre + "]");
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Genera la fecha en que se tiene que ejecutar una tarea
	 * 
	 * @param periodicidad
	 * @return fecha de ejecucion
	 */
	private Date obtenerFecha(String periodicidad) {
		Calendar today = Calendar.getInstance();
		Calendar supportDay = Calendar.getInstance();
		if (StringUtils.equals(periodicidad, "DIARIO")) {
			return today.getTime();
		}
		if (StringUtils.equals(periodicidad, "DIA ULTIMO")) {
			supportDay.set(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
					today.getActualMaximum(Calendar.DAY_OF_MONTH));
			return supportDay.getTime();
		}
		if (StringUtils.equals(periodicidad, "DIA PRIMERO")) {
			supportDay.set(today.get(Calendar.YEAR),
					(today.get(Calendar.MONTH) + 1), 1);
			return supportDay.getTime();
		}
		return null;
	}

	class Ejecutable implements Runnable {
		private ReporteAgenteDTO reporte;

		public Ejecutable(ReporteAgenteDTO reporte) {
			this.reporte = reporte;
		}

		@Override
		public void run() {
			try {
				programacionReporteAgenteDelegateQuartz
						.cargarDatosInicialesPorReporte(reporte);
			} catch (MidasException e) {
				e.printStackTrace();
			}
		}
	}

	public EntidadService getEntidad() {
		return entidad;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	// public ProgramacionReporteAgenteDelegate
	// getProgramacionReporteAgenteDelegate() {
	// return programacionReporteAgenteDelegate;
	// }

	@Autowired
	@Qualifier("entidadEJB")
	public void setEntidad(EntidadService entidad) {
		this.entidad = entidad;
	}

	@Autowired
	@Qualifier("listadoServiceEJB")
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@Autowired
	@Qualifier("programacionReporteAgenteDelegateQuartz")
	public void setProgramacionReporteAgenteDelegateQuartz(
			ProgramacionReporteAgenteDelegateQuartz programacionReporteAgenteDelegateQuartz) {
		this.programacionReporteAgenteDelegateQuartz = programacionReporteAgenteDelegateQuartz;
	}

	// @Autowired
	// @Qualifier("programacionReporteAgenteDelegate")
	// public void setProgramacionReporteAgenteDelegate(
	// ProgramacionReporteAgenteDelegate programacionReporteAgenteDelegate) {
	// this.programacionReporteAgenteDelegate =
	// programacionReporteAgenteDelegate;
	// }
}
