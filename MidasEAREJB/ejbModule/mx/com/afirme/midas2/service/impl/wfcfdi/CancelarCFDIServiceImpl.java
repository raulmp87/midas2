package mx.com.afirme.midas2.service.impl.wfcfdi;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Schedule;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.wfcfdi.CancelarCFDIService;

@Stateless
public class CancelarCFDIServiceImpl implements CancelarCFDIService {		
	
	private PrintReportClient printReportClient;

	@EJB
	private EntidadService entidadService;
	
	private static final Logger LOG = Logger.getLogger(CancelarCFDIServiceImpl.class);
	
	@EJB
	public void setPrintReportClient(PrintReportClient printReportClient) {
		this.printReportClient = printReportClient;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Schedule(minute="*/30", hour="*", persistent=false)
	public void cancelDigitalBill(){
		
		boolean esActivoParametroCancelacion = this.getValorParameter(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES, ParametroGeneralDTO.CODIGO_PARAM_GENERAL_ACTIVA_DESACTIVA_CANCELACION_CFDI);
		if (!esActivoParametroCancelacion){
			LOG.info("CancelarCFDIServiceImpl.cancelDigitalBill... Parametro inactivo, No se ejecuta el proceso...");
			return;
		}
		
		try {
			LOG.info("Entrando a CancelarCFDIService.cancelDigitalBill... ");
			
			//Obtener referencias de cancelaciones a procesar
	        DateTime fecha = new DateTime();
	        Integer referencia1FormateadaInt = this.formateaReferencias(fecha.plusDays(-2));
	        Integer referencia2FormateadaInt = this.formateaReferencias(fecha.plusDays(-1));
	        
	        //Proceso de cancelación de CFDIs
			printReportClient.cancelDigitalBill(referencia1FormateadaInt);
			LOG.info("tarea cancelDigitalBill ejecutada con fecha : " + referencia1FormateadaInt);
	        
	        printReportClient.cancelDigitalBill(referencia2FormateadaInt);	        
	        LOG.info("tarea cancelDigitalBill ejecutada con fecha : " + referencia2FormateadaInt);
			
		} catch (Exception e) {
			LOG.error("Error general en CancelarCFDIService.cancelDigitalBill... ", e);
		}
	}
	
	private boolean getValorParameter(BigDecimal idToGrupoParamGen, BigDecimal codigoParam){
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId();
		parametroGeneralId.setIdToGrupoParametroGeneral(idToGrupoParamGen);
		parametroGeneralId.setCodigoParametroGeneral(codigoParam);
		ParametroGeneralDTO parameter = entidadService.findById(ParametroGeneralDTO.class, parametroGeneralId);
		if(parameter!=null){
			return (parameter.getValor().trim().equals("1"));
		}
		return false;
	}
	
	private Integer formateaReferencias(DateTime referenceDate){
		String formatDate = String.valueOf(referenceDate.getYear())
				+ String.format("%02d", referenceDate.getMonthOfYear())
				+ String.format("%02d", referenceDate.getDayOfMonth());
		
		return Integer.parseInt(formatDate);
	}
}
