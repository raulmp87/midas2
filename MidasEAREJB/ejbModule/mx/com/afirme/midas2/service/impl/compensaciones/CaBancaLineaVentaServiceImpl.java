/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaLineaVentaDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaLineaVentaDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaLineaVenta;
import mx.com.afirme.midas2.service.compensaciones.CaBancaLineaVentaService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless

public class CaBancaLineaVentaServiceImpl  implements CaBancaLineaVentaService {
	
	@EJB
	private CaBancaLineaVentaDao caBancaLineaVenta;
	private static final Logger LOG = LoggerFactory.getLogger(CaBancaLineaVentaDaoImpl.class);

    public void save(CaBancaLineaVenta entity) {   
	    try {
	    	LOG.info(">> save()");
	    	caBancaLineaVenta.save(entity);	  
	    	LOG.info("<< save()");
	    } catch (RuntimeException re) {	 
	    	LOG.error("Información del Error", re);
	        throw re;
	    }
    }
    

    public void delete(CaBancaLineaVenta entity) { 
    	try {
    		LOG.info(">> delete()");
    		caBancaLineaVenta.delete(entity);
    		LOG.info("<< delete()");
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
    		throw re;
    	}
    }
    

    public CaBancaLineaVenta update(CaBancaLineaVenta entity) {   
        try {
        	LOG.info(">> update()");
	        CaBancaLineaVenta result = caBancaLineaVenta.update(entity);
	        LOG.info("<< update()");
	        return result;
	    } catch (RuntimeException re) {
	    	LOG.error("Información del Error", re);
            throw re;
	    }
    }
    
    public CaBancaLineaVenta findById( Long id) {
    	LOG.info(">> findById()");
	    try {
            CaBancaLineaVenta instance = caBancaLineaVenta.findById(id);
           	LOG.info("<< findById()");
            return instance;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        }
        return null;
    }   


    public List<CaBancaLineaVenta> findByProperty(String propertyName, final Object value) {    	
		try {
			return caBancaLineaVenta.findByProperty(propertyName, value);
		} catch (RuntimeException re) {
			
			return null;
		}
	}			

	
	public List<CaBancaLineaVenta> findAll() {	
		try {
			return caBancaLineaVenta.findAll();
		} catch (RuntimeException re) {
			return null;
		}
	}
	
}