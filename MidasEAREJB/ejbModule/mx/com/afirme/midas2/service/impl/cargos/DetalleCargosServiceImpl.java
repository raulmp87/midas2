package mx.com.afirme.midas2.service.impl.cargos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.cargos.DetalleCargosDao;
import mx.com.afirme.midas2.domain.cargos.ConfigCargos;
import mx.com.afirme.midas2.domain.cargos.DetalleCargos;
import mx.com.afirme.midas2.service.cargos.DetalleCargosService;

@Stateless
public class DetalleCargosServiceImpl implements DetalleCargosService {

	DetalleCargosDao dao;
	
	@Override
	public boolean llenarYGuardarListaDetalles(String detalle, ConfigCargos configCargos)throws Exception {
		return dao.llenarYGuardarListaDetalles(detalle, configCargos);
	}

	@Override
	public DetalleCargos loadById(DetalleCargos detalleCargos) throws Exception {
		return dao.loadById(detalleCargos);
	}

	@Override
	public List<DetalleCargos> loadByIdConfigCargos(ConfigCargos configCargos)throws Exception {
		return dao.loadByIdConfigCargos(configCargos);
	}

	@Override
	public DetalleCargos save(DetalleCargos detalleCargos, Long idConfigCargos) throws Exception {
		return dao.save(detalleCargos, idConfigCargos);
	}

	@Override
	public DetalleCargos save(List<DetalleCargos> listDetalleCargos, Long idConfigCargos)throws Exception {
		return dao.save(listDetalleCargos, idConfigCargos);
	}

	@Override
	public DetalleCargos updateEstatusDetalleCargos(DetalleCargos detalleCargos,String estatus) throws Exception {
		return dao.updateEstatusDetalleCargos(detalleCargos, estatus);
	}

	@Override
	public List<DetalleCargos> updateEstatusDetalleCargos(List<DetalleCargos> listDetalleCargos, String estatus) throws Exception {
		return dao.updateEstatusDetalleCargos(listDetalleCargos, estatus);
	}
	
	@Override
	public void ejecutarCargo() throws Exception{
		 dao.ejecutarCargo();
	}
	
	@Override
	public void aplicarCargosEdocta(ConfigCargos config, DetalleCargos objDetalleCargos, String cargoAbono) throws Exception{
		dao.aplicarCargosEdocta(config, objDetalleCargos, cargoAbono);
	}
	
	@Override
	public void iterarAplicarMovtoEdocta(List<DetalleCargos> listaDetalleCargos, String tipoMovto) throws Exception{
		dao.iterarAplicarMovtoEdocta(listaDetalleCargos, tipoMovto);
	}
	
	@EJB
	public void setDao(DetalleCargosDao dao) {
		this.dao = dao;
	}

	@Override
	public List<DetalleCargos> cancelarMovimientoAplicado(ConfigCargos configCargos)throws Exception {
		return dao.cancelarMovimientoAplicado(configCargos);
	}

}
