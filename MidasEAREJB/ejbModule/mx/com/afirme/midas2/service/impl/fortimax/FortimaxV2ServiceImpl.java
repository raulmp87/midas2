package mx.com.afirme.midas2.service.impl.fortimax;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fortimax.FortimaxV2Dao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax.PROCESO_FORTIMAX;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;

@Stateless
public class FortimaxV2ServiceImpl implements FortimaxV2Service{

	private FortimaxV2Dao fortimaxDao;
	
	@Override
	public String[] generateDocument(Long id, String tituloAplicacion, String documentName,String folderName)throws Exception {
		return fortimaxDao.generateDocument(id,tituloAplicacion,documentName,folderName);		
	}
	
	@Override
	public String[] generateExpedient(String tituloAplicacion, String[] fieldValues) throws Exception {
		return fortimaxDao.generateExpedient(tituloAplicacion, fieldValues);
	}

	@Override
	public String generateLinkToDocument(Long id, String tituloAplicacion, String nombreDocumento, String user, String password, String nodo)
			throws Exception {
		return fortimaxDao.generateLinkToDocument(id,tituloAplicacion, nombreDocumento, user, password,nodo); 
	}
	

	@Override
	public String[] uploadFile(String fileName,  TransporteImpresionDTO transporteImpresionDTO, String tituloAplicacion, String[] expediente, String carpeta) throws Exception {
		return fortimaxDao.uploadFile(fileName, transporteImpresionDTO, tituloAplicacion, expediente, carpeta);
	}
	
	@Override
	public String[] getDocumentFortimax(Long id,String tituloAplicacion) throws Exception {
		return fortimaxDao.getDocumentFortimax(id, tituloAplicacion);
	}
	
	@EJB
	public void setFortimaxDao(FortimaxV2Dao fortimaxDao) {
		this.fortimaxDao = fortimaxDao;
	}

	@Override
	public String getToken(long duration) throws Exception {
		return fortimaxDao.getToken(duration);
	}

	@Override
	public String getNodo(Long id, String tituloAplicacion,
			String nombreElemento, String tipoElemento, boolean nodoOnly) throws Exception {
		return fortimaxDao.getNodo(id, tituloAplicacion, nombreElemento, tipoElemento, nodoOnly);
	}

	@Override
	public byte[] downloadFile(String fileName, String tipoElemento,
			String tituloAplicacion, Long id) throws Exception {
		return fortimaxDao.downloadFile(fileName, tipoElemento, tituloAplicacion, id);
	}
	
	@Override
	public byte[] downloadFileBP(String fileName, String tipoElemento,
			String tituloAplicacion, Long id) throws Exception {
		return fortimaxDao.downloadFileBP(fileName, tipoElemento, tituloAplicacion, id);
	}

	@Override
	public String generateLinkToDocumentByProceso(Long id, String aplicacion, String nombreDocumento,boolean defaultUser,PROCESO_FORTIMAX proceso, String nodo) throws Exception {
		// TODO Auto-generated method stub
		return  fortimaxDao.generateLinkToDocumentByProceso(id, aplicacion, nombreDocumento, defaultUser, proceso,nodo);
	}

}