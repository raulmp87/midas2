package mx.com.afirme.midas2.service.impl.poliza.renovacionmasiva;

import javax.ejb.EJB;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoFacadeRemote;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionDAO;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoDao;
import mx.com.afirme.midas2.dao.poliza.renovacionmasiva.OrdenRenovacionMasivaDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.calculo.CalculoDao;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.custShipmentTracking.TrackingService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;
import mx.com.afirme.midas2.service.negocio.producto.NegocioProductoService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.NegocioEstadoService;
import mx.com.afirme.midas2.service.negocio.zonacirculacion.negocioMunicipio.NegocioMunicipioService;
import mx.com.afirme.midas2.service.poliza.PolizaService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;

public class RenovacionMasivaSoporteService {
	@EJB
	protected PolizaService polizaService;
	
	@EJB
	protected EntidadService entidadService;
	
	@EJB
	protected CotizacionService cotizacionService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	protected UsuarioService usuarioService;
	
	@EJB
	protected OrdenRenovacionMasivaDao ordenRenovacionMasivaDao;
	
	@EJB
	protected NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO;
	
	@EJB
	protected NegocioTipoUsoDao negocioTipoUsoDao;
	
	@EJB
	protected NegocioProductoService negocioProductoService;
	
	@EJB
	protected NegocioTipoPolizaService negocioTipoPolizaService;
	
	@EJB
	protected IncisoService incisoService;
	
	@EJB
	protected CalculoService calculoService;
	
	@EJB
	protected EmisionService emisionService;
	
	@EJB
	protected EntidadBitemporalService entidadBitemporalService;
	
	@EJB
	protected MarcaVehiculoFacadeRemote marcaVehiculoFacadeRemote;
	
	@EJB
	protected MonedaFacadeRemote monedaFacadeRemote;
	
	@EJB
	protected NegocioEstadoService estadoService;
	
	@EJB
	protected NegocioMunicipioService municipioService;
	
	//@EJB
	//protected GenerarPlantillaReporte generarPlantillaReporte;
	
	@EJB
	protected GeneraPlantillaReporteBitemporalService generarPlantillaReporte;
	
	@EJB
	protected EndosoService endosoService;
	
	@EJB
	protected ComisionCotizacionFacadeRemote comisionCotizacionFacadeRemote;
	
	@EJB
	protected DocAnexoCotFacadeRemote docAnexoCotFacadeRemote;
	
	@EJB
	protected DocumentoAnexoReaseguroCotizacionFacadeRemote documentoAnexoReaseguroCotizacionFacadeRemote;
	
	@EJB
	protected DocumentoDigitalCotizacionFacadeRemote documentoDigitalCotizacionFacadeRemote;
	
	@EJB
	protected AgenteMidasService agenteMidasService;
	
	@EJB
	protected CoberturaService coberturaService;
	
	@EJB
	protected CalculoDao calculoDao;
	
	@EJB
	protected DomicilioFacadeRemote domicilioFacadeRemote;
	
	@EJB
	protected ClienteFacadeRemote clienteFacade;
	
	@EJB
	protected MedioPagoFacadeRemote medioPagoFacadeRemote;
	
	@EJB
	protected CotizacionEndosoService cotizacionEndosoService;

	@EJB
	protected EntidadContinuityService entidadContinuityService;
	
	@EJB
	protected MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;
	
	@EJB
	protected PolizaPagoService pagoService;
	
	@EJB
	protected SolicitudAutorizacionService autorizacionService;
	
	@EJB 
	protected DocAnexoCotFacadeRemote anexoCotFacadeRemote;
	
	@EJB
	protected NegocioTarifaService negocioTarifaService;
	
	@EJB
	protected EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	
	@EJB
	protected NegocioDerechosService negocioDerechosService;
	
	@EJB
	protected SistemaContext sistemaContext;
	
	@EJB
	protected CargaMasivaService cargaMasivaService;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;
	
	@EJB
	protected ListadoService listadoService;

	@EJB
	protected TrackingService trackingService;
}
