/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoProvisionDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoProvision;
import mx.com.afirme.midas2.service.compensaciones.CaTipoProvisionService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoProvisionServiceImpl  implements CaTipoProvisionService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaTipoProvisionDao tipoProvisioncaDao;

//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoProvisionServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoProvision entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoProvision entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoProvision entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoProvisioncaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoProvision entity.
	  @param entity CaTipoProvision entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoProvision entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoProvision.class, entity.getId());
	        	tipoProvisioncaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoProvision entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoProvision entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoProvision entity to update
	 @return CaTipoProvision the persisted CaTipoProvision entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoProvision update(CaTipoProvision entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoProvision result = tipoProvisioncaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoProvision 	::		CaTipoProvisionServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoProvision findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoProvisionServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoProvision instance = tipoProvisioncaDao.findById(id);//entityManager.find(CaTipoProvision.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoProvisionServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoProvisionServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaTipoProvision entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoProvision property to query
	  @param value the property value to match
	  	  @return List<CaTipoProvision> found by query
	 */
    public List<CaTipoProvision> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoProvisionServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoProvision model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoProvisionServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoProvisioncaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoProvisionServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoProvision> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoProvision> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoProvision> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoProvision> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoProvision entities.
	  	  @return List<CaTipoProvision> all CaTipoProvision entities
	 */
	public List<CaTipoProvision> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoProvisionServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoProvision model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoProvisionServiceImpl	::	findAll	::	FIN	::	");
			return tipoProvisioncaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoProvisionServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}