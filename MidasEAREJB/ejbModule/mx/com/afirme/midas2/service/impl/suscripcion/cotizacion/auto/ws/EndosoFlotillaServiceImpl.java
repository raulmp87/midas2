package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.ws;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.CotizarEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.Inciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.endoso.inciso.IncisoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.ws.EndosoFlotillaService;

@Stateless
public class EndosoFlotillaServiceImpl implements EndosoFlotillaService {
	
	private static final Logger LOG = Logger.getLogger(EndosoFlotillaServiceImpl.class);	
	
	private static final String NOMBRE_CAMPO_NUMERO_REMOLQUES = "NUMERO REMOLQUES";
	private static final String NOMBRE_CAMPO_TIPO_CARGA = "TIPO CARGA";
	private static final String NOMBRE_LIMITE_RC_TERCEROS = "Responsabilidad Civil";
	private static final String NOMBRE_LIMITE_GASTOS_MEDICOS = "GASTOS M\u00C9DICOS";
	private static final String NOMBRE_ASISTENCIA_JURIDICA = "ASISTENCIA JUR\u00CDDICA";
	private static final String NOMBRE_ASISTENCIA_EN_VIAJES = "ASISTENCIA EN VIAJES Y VIAL KM";
	private static final String NOMBRE_EXENCION_DEDUCIBLE_DANOS = "EXENCI\u00D3N DE DEDUCIBLES DM";
	private static final String NOMBRE_LIMITE_ACCIDENTE_CONDUCTOR = "ACCIDENTES AUTOMOVIL\u00CDSTICOS AL CONDUCTOR";
	private static final String NOMBRE_EXTENCION_RC = "EXTENSI\u00D3N DE RESPONSABILIDAD CIVIL";
	private static final String NOMBRE_LIMITE_ADAPTACION_CONVERSION = "ADAPTACIONES Y CONVERSIONES";
	private static final String NOMBRE_LIMITE_EQUIPO_ESPECIAL = "EQUIPO ESPECIAL";
	private static final String NOMBRE_EXENCION_DEDUCIBLE_ROBO = "EXENCI\u00D3N DE DEDUCIBLES RT";
	private static final String NOMBRE_RESPONSABILIDAD_CIVIL_USA = "Responsabilidad Civil en USA Y CANADA LUC";
	private static final String NOMBRE_LIMITE_RC_VIAJERO = "Responsabilidad Civil Viajero";
	private static final String NOMBRE_LIMITE_MUERTE = "RESPONSABILIDAD CIVIL EN EXCESO POR MUERTE";
	
	private CotizacionDTO cotizacion;
	private BitemporalCotizacion bitemporalCotizacion;
	private DateTime validoEn;
			
	@EJB
	private IncisoService incisoServiceBitemporal;
	@EJB
	private CargaMasivaService cargaMasivaService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private EntidadContinuityService entidadContinuityService;
	@EJB
	private MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;
	@EJB
	private EndosoService endosoService;
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<LogErroresCargaMasivaDTO> validaCargaMasiva(BitemporalCotizacion bitemporalCotizacion, CotizarEndosoFlotillaRequest request) throws SystemException {
		this.bitemporalCotizacion = bitemporalCotizacion;
		this.validoEn = TimeUtils.getDateTime(request.getFechaInicioVigencia());
		this.cotizacion = new CotizacionDTO(bitemporalCotizacion.getValue());
		return leeLineasArchivoCarga(request.getLstInciso());
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelarCotizacion(BitemporalCotizacion cotizacion) {		
		endosoService.deleteCotizacionEndoso(cotizacion.getEntidadContinuity().getId());
		CotizacionContinuity continuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class,cotizacion.getContinuity().getId());
		cotizacion.setContinuity(continuity);
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", continuity.getNumero());
		properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);
		
		if(controlEndosoCotList.isEmpty()) {
			properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_TERMINADA);
			controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);				
		}
		
		Iterator<ControlEndosoCot> itControlEndosoCot = controlEndosoCotList.iterator();
		
		ControlEndosoCot controlEndosoCot = null;
		Boolean movimientosBorrados = false;
		while(itControlEndosoCot.hasNext()) {	
			controlEndosoCot = itControlEndosoCot.next();
			if(!movimientosBorrados){
				movimientoEndosoBitemporalService.borrarMovimientos(controlEndosoCot);
				movimientosBorrados = true;
			}
			entidadService.remove(controlEndosoCot);			
		}		
		//Remover Solicitudes de Autorizacion
		endosoService.cancelarSolicitudesAutorizacion(cotizacion);
	}

	
	public List<LogErroresCargaMasivaDTO> leeLineasArchivoCarga(List<Inciso> lstInciso) {
		
		int i = 0;
		List<LogErroresCargaMasivaDTO> listErrores = new ArrayList<LogErroresCargaMasivaDTO>();
		List<DetalleCargaMasivaAutoCot> detalleCargaMasivaAutoCotList = new ArrayList<DetalleCargaMasivaAutoCot>(1);
		for(Inciso inciso: lstInciso) {
			DetalleCargaMasivaAutoCot detalle = creaLineaDetalleCarga(inciso, ++i, listErrores);
			if (detalle != null) {
				detalleCargaMasivaAutoCotList.add(detalle);
			} else {
				DetalleCargaMasivaAutoCot detalleVacio = new DetalleCargaMasivaAutoCot();
				detalleCargaMasivaAutoCotList.add(detalleVacio);
			}

		}

		// Valida datos para incisos
		if (listErrores.isEmpty()) {
				this.validaDetalleCargaMasiva(detalleCargaMasivaAutoCotList, listErrores);
				
				try{
					if(bitemporalCotizacion.getValue().getNegocioDerechoEndosoId() == null){
						cargaMasivaService.obtieneNegocioDerechoEndosoAltaInciso(bitemporalCotizacion);
					}
					cargaMasivaService.guardaCotizacionEndosoMovimientos(bitemporalCotizacion);

				}catch(Exception e){
					e.printStackTrace();
				}
				
		}
		return listErrores;
	}


	/** Valida que los datos obligatorio no sean nulos
	 * @param listErrores 
	 * @param hssfRow
	 * @return
	 */
	private DetalleCargaMasivaAutoCot creaLineaDetalleCarga(Inciso inciso, int fila, List<LogErroresCargaMasivaDTO> listErrores) {

		DetalleCargaMasivaAutoCot detalle = new DetalleCargaMasivaAutoCot();
		StringBuilder mensajeError = new StringBuilder("");
		String negocioSeccion = "";
		LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
		
		if(inciso.getInformacionDelVehiculo().getIdNegocioSeccion() == null) {
			error.setNumeroInciso(new BigDecimal(fila));
			error.setNombreSeccion(negocioSeccion);
			error.setNombreCampoError("LINEA DE NEGOCIO");
			error.setRecomendaciones("Seleccione un campo del combo");
			listErrores.add(error);
			mensajeError.append("Campo: LINEA DE NEGOCIO -  Seleccione un campo del combo ").append("\r\n").append(System.getProperty("line.separator"));
		} else {
			detalle.setIdNegocioSeccion(inciso.getInformacionDelVehiculo().getIdNegocioSeccion());
			NegocioSeccion negocioSeccionEntity = entidadService.findById(NegocioSeccion.class, BigDecimal.valueOf(detalle.getIdNegocioSeccion()));
			if(negocioSeccionEntity == null) {
				error.setNumeroInciso(new BigDecimal(fila));
				error.setNombreSeccion(negocioSeccion);
				error.setNombreCampoError("LINEA DE NEGOCIO");
				error.setRecomendaciones("Seleccione un campo del combo");
				listErrores.add(error);
				mensajeError.append("Campo: LINEA DE NEGOCIO -  Seleccione un campo del combo ").append("\r\n").append(System.getProperty("line.separator"));
			} else {
				negocioSeccion = negocioSeccionEntity.getSeccionDTO().getNombreComercial();
			}
			
		}
		
		if (inciso.getInformacionDelVehiculo().getIdTipoDeUso() == null) {
			error.setNumeroInciso(new BigDecimal(fila));
			error.setNombreSeccion(negocioSeccion);
			error.setNombreCampoError("TIPO DE USO");
			error.setRecomendaciones("Seleccione un campo del combo");
			listErrores.add(error);
			mensajeError.append("Campo: TIPO DE USO -  Seleccione un campo del combo ").append("\r\n").append(System.getProperty("line.separator"));
		} else {
			detalle.setIdTipoDeUso(inciso.getInformacionDelVehiculo().getIdTipoDeUso());
		}
		
		if (StringUtils.isNotBlank(inciso.getInformacionDelVehiculo().getDescripcion())) {
			detalle.setDescripcion(inciso.getInformacionDelVehiculo().getDescripcion());
		}
		
		if (StringUtils.isNotBlank(inciso.getInformacionDelVehiculo().getClaveAmis())) {
			detalle.setClaveAMIS(inciso.getInformacionDelVehiculo().getClaveAmis());
		} else {
			error.setNumeroInciso(new BigDecimal(fila));
			error.setNombreSeccion(negocioSeccion);
			error.setNombreCampoError("CLAVE AMIS");
			error.setRecomendaciones("Ingrese la Clave AMIS");
			listErrores.add(error);
			mensajeError.append("Campo: CLAVE AMIS -  Ingrese la Clave AMIS ").append("\r\n").append(System.getProperty("line.separator"));
		}
		
		if (StringUtils.isNotBlank(inciso.getInformacionDelVehiculo().getModelo())) {
			try {
				detalle.setModeloVehiculo(Short.valueOf(inciso.getInformacionDelVehiculo().getModelo()));
			} catch (Exception e) {
				error.setNumeroInciso(new BigDecimal(fila));
				error.setNombreSeccion(negocioSeccion);
				error.setNombreCampoError("MODELO");
				error.setRecomendaciones("Modelo del Vehiculo Invalido");
				listErrores.add(error);
				mensajeError.append("Campo: MODELO -  Modelo del Vehiculo Invalido ").append("\r\n").append(System.getProperty("line.separator"));	
			}
			
		} else {
			error.setNumeroInciso(new BigDecimal(fila));
			error.setNombreSeccion(negocioSeccion);
			error.setNombreCampoError("MODELO");
			error.setRecomendaciones("Ingrese el modelo del Vehiculo");
			listErrores.add(error);
			mensajeError.append("Campo: MODELO -  Ingrese el modelo del Vehiculo ").append("\r\n").append(System.getProperty("line.separator"));
		}
		
		if (StringUtils.isNotBlank(inciso.getPaquete())) {
			detalle.setPaqueteDescripcion(inciso.getPaquete());
		} else {
			error.setNumeroInciso(new BigDecimal(fila));
			error.setNombreSeccion(negocioSeccion);
			error.setNombreCampoError("PAQUETE");
			error.setRecomendaciones("Seleccione un campo del combo");
			listErrores.add(error);
			mensajeError.append("Campo: PAQUETE -  Seleccione un campo del combo " ).append("\r\n").append(System.getProperty("line.separator"));
		}
		
		if (StringUtils.isNotBlank(inciso.getZonaCirculacion().getCodigoPostal())) {
			detalle.setCodigoPostal(inciso.getZonaCirculacion().getCodigoPostal());
		} else {
			error.setNumeroInciso(new BigDecimal(fila));
			error.setNombreSeccion(negocioSeccion);
			error.setNombreCampoError("CODIGO POSTAL");
			error.setRecomendaciones("Ingrese el Codigo Postal");
			listErrores.add(error);
			mensajeError.append("Campo: CODIGO POSTAL -  Ingrese el Codigo Postal " ).append("\r\n").append(System.getProperty("line.separator"));
		}

		if (inciso.getDetalleDeCoberturas() != null) {
			if (inciso.getDetalleDeCoberturas().getPctDanosMateriales() != null) {
				detalle.setDeducibleDanosMateriales(inciso.getDetalleDeCoberturas().getPctDanosMateriales().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getPctRoboTotal() != null) {
				detalle.setDeducibleRoboTotal(inciso.getDetalleDeCoberturas().getPctRoboTotal().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getLimiteRCTerceros() != null) {
				detalle.setLimiteRcTerceros(inciso.getDetalleDeCoberturas().getLimiteRCTerceros().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getLimiteGastosMedicos() != null) {
				detalle.setLimiteGastosMedicos(inciso.getDetalleDeCoberturas().getLimiteGastosMedicos().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getAsistenciaJuridica() != null) {
				detalle.setAsistenciaJuridica(toShort(inciso.getDetalleDeCoberturas().getAsistenciaJuridica()));
			}
			
			if (inciso.getDetalleDeCoberturas().getAsistenciaEnViajes() != null) {
				detalle.setAsistenciaViajes(toShort(inciso.getDetalleDeCoberturas().getAsistenciaEnViajes()));
			}
			
			if (inciso.getDetalleDeCoberturas().getExencionDeducibleDanosMateriales() != null) {
				detalle.setExencionDeducibleDanosMateriales(toShort(inciso.getDetalleDeCoberturas().getExencionDeducibleDanosMateriales()));
			}
			
			if (inciso.getDetalleDeCoberturas().getLimiteAccidenteConductor() != null) {
				detalle.setLimiteAccidenteConductor(inciso.getDetalleDeCoberturas().getLimiteAccidenteConductor().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getExtencionRC() != null) {
				detalle.setExencionRC(toShort(inciso.getDetalleDeCoberturas().getExtencionRC()));
			}
			
			if (inciso.getDetalleDeCoberturas().getLimiteAdaptacionesConversiones() != null) {
				detalle.setLimiteAdaptacionConversion(inciso.getDetalleDeCoberturas().getLimiteAdaptacionesConversiones().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getLimiteEquipoEspecial() != null) {
				detalle.setLimiteEquipoEspecial(inciso.getDetalleDeCoberturas().getLimiteEquipoEspecial().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getPctDeducibleEquipoEspecial() != null) {
				detalle.setDeducibleEquipoEspecial(inciso.getDetalleDeCoberturas().getPctDeducibleEquipoEspecial().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getExencionDeducibleRoboTotal() != null) {
				detalle.setExencionDeducibleRoboTotal(toShort(inciso.getDetalleDeCoberturas().getExencionDeducibleRoboTotal()));
			}
			
			if (inciso.getDetalleDeCoberturas().getResponsabilidadCivilUsaCanada() != null) {
				detalle.setResponsabilidadCivilUsaCanada(toShort(inciso.getDetalleDeCoberturas().getResponsabilidadCivilUsaCanada()));
			}
			
			if (inciso.getDetalleDeCoberturas().getIgualacion() != null) {
				detalle.setIgualacion(inciso.getDetalleDeCoberturas().getIgualacion().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getNumeroRemolques() != null) {
				detalle.setNumeroRemolques(inciso.getDetalleDeCoberturas().getNumeroRemolques());
			}
			
			if (inciso.getDetalleDeCoberturas().getTipoCarga() != null) {
					CatalogoValorFijoDTO tipoCarga = cargaMasivaService.obtieneTipoCargaPorDescripcion(inciso.getDetalleDeCoberturas().getTipoCarga());
					detalle.setTipoCarga(tipoCarga);
			} 
			
			if (inciso.getDetalleDeCoberturas().getLimiteMuerte() != null) {
				detalle.setLimiteMuerte(inciso.getDetalleDeCoberturas().getLimiteMuerte().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getSumaAseguradaDanosMateriales() != null) {
				detalle.setValorSumaAseguradaDM(inciso.getDetalleDeCoberturas().getSumaAseguradaDanosMateriales().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getSumaAseguradaRoboTotal() != null) {
				detalle.setValorSumaAseguradaRT(inciso.getDetalleDeCoberturas().getSumaAseguradaRoboTotal().doubleValue());
			}
			
			if (inciso.getDetalleDeCoberturas().getDiasSalarioMinimo() != null) {
				detalle.setDiasSalarioMinimo(inciso.getDetalleDeCoberturas().getDiasSalarioMinimo().doubleValue());
			}
		}
		
		
		detalle.setMensajeError(mensajeError.toString());
		if (mensajeError.toString().isEmpty()) {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_PENDIENTE);
		} else {
			detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
		}
		
		return detalle;
	}

	private Short toShort(boolean bol) {
		if (bol) {
			return Short.valueOf("1");
		} else {
			return Short.valueOf("0");
		}
	}


	public void validaDetalleCargaMasiva(List<DetalleCargaMasivaAutoCot> detalleCargaMasivaList, List<LogErroresCargaMasivaDTO> listErrores) {

		int fila = 1;
		StringBuilder mensajeError =  new StringBuilder();
		for (DetalleCargaMasivaAutoCot detalle : detalleCargaMasivaList) {

			fila++;
			if (detalle.getClaveEstatus() != null && !detalle.getClaveEstatus().equals(
					DetalleCargaMasivaAutoCot.ESTATUS_ERROR)) {
				BigDecimal numeroInciso = BigDecimal.valueOf(fila);

				
				BitemporalInciso bitemporalInciso = new BitemporalInciso();
				BitemporalAutoInciso bitemporalAutoInciso = new BitemporalAutoInciso();
				BitemporalCoberturaSeccion responsabilidadCivilCoberturaCotizacion = null;
				BitemporalCoberturaSeccion danosOcasionadosPorCargaCoberturaCotizacion = null;
				
				
				bitemporalInciso.getContinuity().setParentContinuity(bitemporalCotizacion.getContinuity());				
				bitemporalAutoInciso.getValue().setAsociadaCotizacion(1);
				
				mensajeError.delete(0,mensajeError.length());
				String lineaDeNegocio = "";				

				NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, BigDecimal.valueOf(detalle.getIdNegocioSeccion()));
				negocioSeccion = cargaMasivaService
						.obtieneNegocioSeccionPorDescripcion(cotizacion, negocioSeccion.getSeccionDTO().getDescripcion());
				if (negocioSeccion != null) {
					lineaDeNegocio = negocioSeccion.getSeccionDTO()
							.getDescripcion();
					bitemporalAutoInciso.getValue().setNegocioSeccionId(negocioSeccion
							.getIdToNegSeccion().longValue());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion("");
					error.setNombreCampoError("LINEA DE NEGOCIO");
					error.setRecomendaciones("Linea de Negocio Invalida");
					listErrores.add(error);
					mensajeError.append("Campo: LINEA DE NEGOCIO -  Linea de Negocio Invalida ").append("\r\n").append(System.getProperty("line.separator"));
					continue;
				}
				
				TipoUsoVehiculoDTO tipoUso = entidadService.findById(TipoUsoVehiculoDTO.class, BigDecimal.valueOf(detalle.getIdTipoDeUso()));
				if (tipoUso != null) {
					bitemporalAutoInciso.getValue().setTipoUsoId(tipoUso.getIdTcTipoUsoVehiculo().longValue());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("TIPO DE USO");
					error.setRecomendaciones("TipoUso Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: TIPO DE USO -  TipoUso Invalido ").append("\r\n").append(System.getProperty("line.separator"));
				}

				EstiloVehiculoDTO estiloVehiculo = cargaMasivaService
						.obtieneEstiloVehiculoDTOPorClaveAMIS(cotizacion,
								negocioSeccion, detalle.getClaveAMIS());
				if (estiloVehiculo != null) {
					bitemporalAutoInciso.getValue().setMarcaId(estiloVehiculo
							.getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
					bitemporalAutoInciso.getValue()
							.setEstiloId(estiloVehiculo.getId().getStrId());
					
					try{
						bitemporalAutoInciso.getValue().setClaveTipoBien(estiloVehiculo.getId().getClaveTipoBien());
						bitemporalAutoInciso.getValue().setIdVersionCarga(estiloVehiculo.getId().getIdVersionCarga());
						bitemporalAutoInciso.getValue().setIdMoneda(cotizacion.getIdMoneda().shortValue());
					}catch(Exception e){
					}
					
					if(detalle.getDescripcion() != null && !detalle.getDescripcion().isEmpty()){
						bitemporalAutoInciso.getValue().setDescripcionFinal(detalle.getDescripcion().toUpperCase());
					}else{						
						bitemporalAutoInciso.getValue().setDescripcionFinal(estiloVehiculo.getDescripcionEstilo());
						detalle.setDescripcion(estiloVehiculo.getDescripcionEstilo().toUpperCase());
					}					
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CLAVE AMIS");
					error.setRecomendaciones("Clave AMIS Invalida");
					listErrores.add(error);
					mensajeError.append("Campo: CLAVE AMIS -  Clave AMIS Invalida ").append("\r\n").append(System.getProperty("line.separator"));
				}

				//ValidaModelo
				if(estiloVehiculo != null){
					if(cargaMasivaService.validaModeloVehiculo(cotizacion.getIdMoneda(), estiloVehiculo.getId().getStrId(), 
							negocioSeccion.getIdToNegSeccion(), detalle.getModeloVehiculo())){
						bitemporalAutoInciso.getValue().setModeloVehiculo(detalle.getModeloVehiculo());
					}else{
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("MODELO VEHICULO");
						error.setRecomendaciones("Modelo Vehiculo Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: ").append(error.getNombreCampoError()).append(" -  ").append(error.getRecomendaciones()).append(" ").append("\r\n").append(System.getProperty("line.separator"));			
					}
				}

				NegocioPaqueteSeccion negocioPaquete = cargaMasivaService
						.obtieneNegocioPaqueteSeccionPorDescripcion(
								negocioSeccion, detalle.getPaqueteDescripcion());

				if (negocioPaquete != null) {
					bitemporalAutoInciso.getValue().setNegocioPaqueteId(negocioPaquete
							.getIdToNegPaqueteSeccion());
					bitemporalAutoInciso.getValue().setPaquete(negocioPaquete.getPaquete());
				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("PAQUETE");
					error.setRecomendaciones("Paquete Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: PAQUETE -  Paquete Invalido ").append("\r\n").append(System.getProperty("line.separator"));			
				}

				String municipioId = cargaMasivaService
						.obtieneMunicipioIdPorCodigoPostal(detalle
								.getCodigoPostal());
				if (municipioId != null) {
					bitemporalAutoInciso.getValue().setMunicipioId(municipioId);

					String estadoId = cargaMasivaService
							.obtieneEstadoIdPorMunicipio(municipioId);
					bitemporalAutoInciso.getValue().setEstadoId(estadoId);
					
					if(!cargaMasivaService.validaEstadoMunicipio(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
							estadoId, municipioId)){
						LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						error.setNumeroInciso(new BigDecimal(fila));
						error.setNombreSeccion("");
						error.setNombreCampoError("CODIGO POSTAL");
						error.setRecomendaciones("Codigo Postal Invalido");
						listErrores.add(error);
						mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido " ).append("\r\n").append(System.getProperty("line.separator"));
					}

				} else {
					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					error.setNumeroInciso(new BigDecimal(fila));
					error.setNombreSeccion(lineaDeNegocio);
					error.setNombreCampoError("CODIGO POSTAL");
					error.setRecomendaciones("Codigo Postal Invalido");
					listErrores.add(error);
					mensajeError.append("Campo: CODIGO POSTAL -  Codigo Postal Invalido ").append("\r\n").append(System.getProperty("line.separator"));
				}

				if (mensajeError.toString().isEmpty()) {
					
					List<BitemporalCoberturaSeccion> bitemporalCoberturaSeccionList = cargaMasivaService.mostrarCoberturas(
							bitemporalAutoInciso, bitemporalCotizacion.getContinuity().getId(), validoEn);
					
					
				    if(bitemporalCoberturaSeccionList != null && !bitemporalCoberturaSeccionList.isEmpty()){
						Short claveContrato = 1;
						Short claveObligatoriedad = 0;
						Short valorSI = 1;
						Short valorNO = 0;
						for(BitemporalCoberturaSeccion coberturaCotizacion : bitemporalCoberturaSeccionList){
							
			    			//Valida si es obligatoria
							Boolean esObligatoria = false;
			    			if(coberturaCotizacion.getValue().getClaveContrato().equals(claveContrato) && 
			    					coberturaCotizacion.getValue().getClaveObligatoriedad().equals(claveObligatoriedad)){
			    				esObligatoria = true;
			    			}
			    			
			    			//Valida Permite Suma Asegurada
			    			Boolean permiteSumaAsegurada = false;				    			
			    			if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO) ||
			    					coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA)){
			    				permiteSumaAsegurada = true;
			    			}			    			
			    			
			    			//Valida Rangos
			    			Boolean validaRangos = false;
			    			if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada().equals("0") &&
			    					coberturaCotizacion.getValue().getValorSumaAseguradaMin() != 0 && coberturaCotizacion.getValue().getValorSumaAseguradaMax() != 0){
			    				validaRangos = true;
			    			}
			    			
			    			//Valida Deducibles
			    			Boolean validaDeducibles = false;
			    			if(!coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible().equals("0")){
			    				validaDeducibles = true;
			    			}
			    			
				    		if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().toUpperCase().equals(
				    				CoberturaDTO.CLAVE_CALCULO_DANOS_MATERIALES.toUpperCase())){
				    			if(permiteSumaAsegurada && detalle.getValorSumaAseguradaDM() != null){
				    				boolean valido = true;
					    			if(validaRangos){
					    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
					    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getValorSumaAseguradaDM());
					    			}
					    			if(valido){
					    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getValorSumaAseguradaDM());
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion("");				    					
				    					error.setRecomendaciones("Valor no valido");
				    					mensajeError.append("Campo: SUMA ASEGURADA DM -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
				    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append(" ");
				    					error.setNombreCampoError(mensajeError.toString());
				    					listErrores.add(error);
				    				}
				    			}				    			
				    			if(detalle.getDeducibleDanosMateriales() != null){
				    				boolean valido = true;
				    				if(validaDeducibles){
				    					valido = validaDeducible(coberturaCotizacion.getValue().getDeducibles(), detalle.getDeducibleDanosMateriales());
				    				}
				    				if(valido){
				    					coberturaCotizacion.getValue().setValorDeducible(detalle.getDeducibleDanosMateriales());
				    					coberturaCotizacion.getValue().setPorcentajeDeducible(detalle.getDeducibleDanosMateriales());
				    					coberturaCotizacion.getValue().setClaveContrato(claveContrato);
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion(lineaDeNegocio);
				    					error.setRecomendaciones("Valor no valido");
				    					mensajeError.append("Campo: DEDUCIBLE DA\u00d1OS MATERIALES -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
				    					error.setNombreCampoError(mensajeError.toString());
				    					listErrores.add(error);
				    				}
				    			}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().trim().toUpperCase().equals(
					 				CoberturaDTO.CLAVE_CALCULO_ROBO_TOTAL.trim().toUpperCase())){
					 			if(permiteSumaAsegurada && detalle.getValorSumaAseguradaRT() != null){
				    				boolean valido = true;
					    			if(validaRangos){
					    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
					    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getValorSumaAseguradaRT());
					    			}
					    			if(valido){
					    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getValorSumaAseguradaRT());
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion("");
				    					error.setRecomendaciones("Valor no valido");
				    					mensajeError.append("Campo: SUMA ASEGURADA RT -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
				    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append(" ");
				    					error.setNombreCampoError(mensajeError.toString());
				    					listErrores.add(error);
				    				}
				    			}					 			
				    			if(detalle.getDeducibleRoboTotal() != null){
				    				boolean valido = true;
				    				if(validaDeducibles){
				    					valido = validaDeducible(coberturaCotizacion.getValue().getDeducibles(), detalle.getDeducibleRoboTotal());
				    				}
				    				if(valido){
				    					coberturaCotizacion.getValue().setValorDeducible(detalle.getDeducibleRoboTotal());
				    					coberturaCotizacion.getValue().setPorcentajeDeducible(detalle.getDeducibleRoboTotal());
				    					coberturaCotizacion.getValue().setClaveContrato(claveContrato);
				    				}else{
				    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
				    					error.setNumeroInciso(new BigDecimal(fila));
				    					error.setNombreSeccion(lineaDeNegocio);
				    					error.setRecomendaciones("Valor no valido");
				    					mensajeError.append("Campo: DEDUCIBLE ROBO TOTAL -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
				    					error.setNombreCampoError(mensajeError.toString());
				    					listErrores.add(error);
				    				}
				    			}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
								 NOMBRE_LIMITE_RC_TERCEROS.toUpperCase())){
						    		if(detalle.getLimiteRcTerceros() != null){
						    			boolean valido = true;
						    			if(validaRangos){
						    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
						    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteRcTerceros());
						    			}
						    			if(valido){
						    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteRcTerceros());
						    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);						    				
						    			}else{
					    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
					    					error.setNumeroInciso(new BigDecimal(fila));
					    					error.setNombreSeccion(lineaDeNegocio);
					    					error.setRecomendaciones("Valor no valido");
					    					mensajeError.append("Campo: LIMITE RC TERCEROS -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
					    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
					    					error.setNombreCampoError(mensajeError.toString());
					    					listErrores.add(error);
						    			}
						    		}
						    		responsabilidadCivilCoberturaCotizacion = coberturaCotizacion;
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_GASTOS_MEDICOS.toUpperCase())){
							    		if(detalle.getLimiteGastosMedicos() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteGastosMedicos());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteGastosMedicos());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE GASTOS MEDICOS -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_ASISTENCIA_JURIDICA.toUpperCase())){
							    		if(detalle.getAsistenciaJuridica() != null){
							    			if(detalle.getAsistenciaJuridica().equals(valorSI)){
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
							    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().startsWith(
									 NOMBRE_ASISTENCIA_EN_VIAJES.toUpperCase())){
							    		if(detalle.getAsistenciaViajes() != null){
							    			if(detalle.getAsistenciaViajes().equals(valorSI)){
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
							    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXENCION_DEDUCIBLE_DANOS.toUpperCase())){
							    		if(detalle.getExencionDeducibleDanosMateriales() != null){
							    			if(detalle.getExencionDeducibleDanosMateriales().equals(valorSI)){
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    			}else{
							    				if(!esObligatoria){
							    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
								    			}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_ACCIDENTE_CONDUCTOR.toUpperCase())){
							    		if(detalle.getLimiteAccidenteConductor() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteAccidenteConductor());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteAccidenteConductor());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE ACCIDENTE CONDUCTOR -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXTENCION_RC.toUpperCase())){
					    		if(detalle.getExencionRC() != null){
					    			if(detalle.getExencionRC().equals(valorSI)){
					    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
					    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_ADAPTACION_CONVERSION.toUpperCase())){
							    		if(detalle.getLimiteAdaptacionConversion() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteAdaptacionConversion());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteAdaptacionConversion());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE ADAPTACION CONVERSION -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_EQUIPO_ESPECIAL.toUpperCase())){
							    		if(detalle.getLimiteEquipoEspecial() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteEquipoEspecial());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteEquipoEspecial());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE EQUIPO ESPECIAL -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    			if(valido && detalle.getDeducibleEquipoEspecial() != null){
							    				if(validaDeducibles){
							    					valido = validaDeducible(coberturaCotizacion.getValue().getDeducibles(), detalle.getDeducibleEquipoEspecial());
							    				}
							    				if(valido){
							    					coberturaCotizacion.getValue().setValorDeducible(detalle.getDeducibleEquipoEspecial());
							    					coberturaCotizacion.getValue().setPorcentajeDeducible(detalle.getDeducibleEquipoEspecial());
							    					coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    				}else{
							    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							    					error.setNumeroInciso(new BigDecimal(fila));
							    					error.setNombreSeccion(lineaDeNegocio);
							    					error.setRecomendaciones("Valor no valido");
							    					mensajeError.append("Campo: DEDUCIBLE EQUIPO ESPECIAL -  Valor no valido ").append("\r\n").append(System.getProperty("line.separator"));
							    					error.setNombreCampoError(mensajeError.toString());
							    					listErrores.add(error);
							    				}
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_EXENCION_DEDUCIBLE_ROBO.toUpperCase())){
					    		if(detalle.getExencionDeducibleRoboTotal() != null){
					    			if(detalle.getExencionDeducibleRoboTotal().equals(valorSI)){
					    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
					    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_RESPONSABILIDAD_CIVIL_USA.toUpperCase())){
					    		if(detalle.getResponsabilidadCivilUsaCanada() != null){
					    			if(detalle.getResponsabilidadCivilUsaCanada().equals(valorSI)){
					    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
					    			}else{
					    				if(!esObligatoria){
					    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
						    			}
					    			}
					    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
					 				CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL.toUpperCase())){
				 				boolean danosCargaContratada = esObligatoria || detalle.isDanosCargaContratada();
				 				coberturaCotizacion.getValue().setClaveContratoBoolean(danosCargaContratada);
				 				
				 				danosOcasionadosPorCargaCoberturaCotizacion = coberturaCotizacion;
				 				
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
									 NOMBRE_LIMITE_MUERTE.toUpperCase())){
							    		if(detalle.getLimiteMuerte() != null){
							    			boolean valido = true;
							    			if(validaRangos){
							    				valido = validaRangos(coberturaCotizacion.getValue().getValorSumaAseguradaMin(), 
							    						coberturaCotizacion.getValue().getValorSumaAseguradaMax(), detalle.getLimiteMuerte());
							    			}
							    			if(valido){
							    				coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteMuerte());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);					    				
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE MUERTE -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}else{
						    				if(!esObligatoria){
						    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
							    			}
							    		}
					 		}else if(coberturaCotizacion.getValue().getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(
					 				NOMBRE_LIMITE_RC_VIAJERO.toUpperCase())){
							    		if(detalle.getDiasSalarioMinimo() != null){
							    			boolean valido = true;
							    			valido = validaDiasSalarioMinimo(detalle.getDiasSalarioMinimo());
							    			if(valido){
							    				coberturaCotizacion.getValue().setDiasSalarioMinimo(detalle.getDiasSalarioMinimo().intValue());
							    				//coberturaCotizacion.getValue().setValorSumaAsegurada(detalle.getLimiteMuerte());
							    				coberturaCotizacion.getValue().setClaveContrato(claveContrato);
							    			}else{
						    					LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
						    					error.setNumeroInciso(new BigDecimal(fila));
						    					error.setNombreSeccion(lineaDeNegocio);
						    					error.setRecomendaciones("Valor no valido");
						    					mensajeError.append("Campo: LIMITE MUERTE -  Valor debe ser entre ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMin()).append(
						    					" y ").append(coberturaCotizacion.getValue().getValorSumaAseguradaMax()).append("").append("\r\n").append(System.getProperty("line.separator"));
						    					error.setNombreCampoError(mensajeError.toString());
						    					listErrores.add(error);
							    			}
							    		}else{
						    				if(!esObligatoria){
						    					coberturaCotizacion.getValue().setClaveContrato(valorNO);
							    			}
							    		}
					 		}
					 	}
					 
					}
				    
				    //Guarda Inciso
				    if (mensajeError.toString().isEmpty()) {
				    	Map<String, String> datosRiesgo = null;
				    	if(detalle.getIgualacion() != null && detalle.getIgualacion() > 0) {
				    		bitemporalInciso.getValue().setPrimaTotalAntesDeIgualacion(detalle.getIgualacion());
				    	}
				    	bitemporalAutoInciso.getValue().setPctDescuentoEstado(0.0);
				    	
				    	bitemporalInciso = incisoServiceBitemporal.prepareGuardarIncisoBorrador(
				    			bitemporalInciso, bitemporalAutoInciso, datosRiesgo, bitemporalCoberturaSeccionList, validoEn);

						if (bitemporalInciso == null) {
							LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
							error.setNumeroInciso(new BigDecimal(fila));
							error.setNombreSeccion(lineaDeNegocio);
							error.setRecomendaciones("");
							mensajeError.append("Error al guardar Inciso ").append("\r\n").append(System.getProperty("line.separator"));
							error.setNombreCampoError(mensajeError.toString());
	    					listErrores.add(error);
						} else {
				    
				    
						    //Carga Datos de Riesgo
						    Map<String, String> valores = new LinkedHashMap<String, String>();
						    List<ControlDinamicoRiesgoDTO> controles = cargaMasivaService.getDatosRiesgo(bitemporalInciso.getContinuity().getId(), validoEn.toDate(), 
									valores, SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO, TipoAccionDTO.getAltaIncisoEndosoCot(), true);
							
						    if(controles != null && controles.size() > 0 ){
						    
								if (responsabilidadCivilCoberturaCotizacion != null && responsabilidadCivilCoberturaCotizacion.getValue().getClaveContrato().intValue() == 1) {
									
									for(int i = 0; i < controles.size(); i++){
										ControlDinamicoRiesgoDTO control = controles.get(i);
										if(control.getEtiqueta().equals(ConfiguracionDatoInciso.DescripcionEtiqueta.NUMERO_REMOLQUES.toString())){
											List<LogErroresCargaMasivaDTO> tmpErrors = new ArrayList<LogErroresCargaMasivaDTO>();				 			
											cargaMasivaService.validaNoNulo(detalle.getNumeroRemolques(), NOMBRE_CAMPO_NUMERO_REMOLQUES, numeroInciso, lineaDeNegocio, tmpErrors);
											cargaMasivaService.validaRangos(0, 9, detalle.getNumeroRemolques(), NOMBRE_CAMPO_NUMERO_REMOLQUES, numeroInciso, lineaDeNegocio, tmpErrors);
											listErrores.addAll(tmpErrors);
											mensajeError.append(cargaMasivaService.generaMensajeError(tmpErrors));								
											control.setValor(detalle.getNumeroRemolques().toString());
										}
									}
									
								}
						    
								if (danosOcasionadosPorCargaCoberturaCotizacion != null && danosOcasionadosPorCargaCoberturaCotizacion.getValue().getClaveContrato().intValue() == 1) {
									List<LogErroresCargaMasivaDTO> tmpErrors = new ArrayList<LogErroresCargaMasivaDTO>();				 			
									
									
									if (cargaMasivaService.validaNoNulo(detalle.getTipoCargaDescripcion(), NOMBRE_CAMPO_TIPO_CARGA, numeroInciso, lineaDeNegocio, tmpErrors)) {
										cargaMasivaService.validaNoNulo2(detalle.getTipoCarga(), NOMBRE_CAMPO_TIPO_CARGA, numeroInciso, lineaDeNegocio, tmpErrors);			
									}
									
									if (responsabilidadCivilCoberturaCotizacion == null) {
										throw new RuntimeException("Paquete mal configurado ya que los paquetes que incluyen la cobertura de " +
												"daños por la carga deben tambien incluir la cobertura de responsabilidad civil.");
									}
									
									if (responsabilidadCivilCoberturaCotizacion.getValue().getClaveContrato().intValue() == 0) {
										cargaMasivaService
												.agregaError(
														"La cobertura responsabilidad civil debe estar contratada para poder contratar la cobertura de daños por la carga.",
														CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL, numeroInciso, lineaDeNegocio,
														tmpErrors);
									}
									
									listErrores.addAll(tmpErrors);
									mensajeError.append(cargaMasivaService.generaMensajeError(tmpErrors));
									if (tmpErrors.size() == 0) {
										
										for(int i = 0; i < controles.size(); i++){
											ControlDinamicoRiesgoDTO control = controles.get(i);
											
											if(control.getEtiqueta().equals(ConfiguracionDatoInciso.DescripcionEtiqueta.TIPO_CARGA.toString())){
												if (detalle.getTipoCarga() == null) {
													throw new RuntimeException(
															"Error en la configuración de datos de inciso. Debe estar configurado el dato de inciso de tipo de carga para "
																	+ "la cobertura de daños ocasionados por la carga.");
												}								
												control.setValor(String.valueOf(detalle.getTipoCarga().getId().getIdDato()));
											}
										}
									}
								}
								
								//Guarda datos de riesgo
								cargaMasivaService.guardarDatosAdicionalesPaquete(controles, bitemporalInciso.getContinuity().getId(), validoEn.toDate());
								
						    }
						}
				    }

				}
				detalle.setMensajeError(mensajeError.toString());
				if (mensajeError.toString().isEmpty()) {
					detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_COTIZADO);
					detalle.setNumeroInciso(new BigDecimal(bitemporalInciso.getContinuity().getNumero()));	
				} else {
					detalle.setClaveEstatus(DetalleCargaMasivaAutoCot.ESTATUS_ERROR);
					break;
				}
			}
		}//FOR
		
	}
		
	public boolean validaDeducible(List<NegocioDeducibleCob> deducibles, Double valor){
		boolean esValido = false;
		if(deducibles != null){
			for(NegocioDeducibleCob deducible : deducibles){
				if(deducible.getValorDeducible().equals(valor)){
					esValido = true;
					break;
				}
			}
		}
		return esValido;
	}
	
	public boolean validaRangos(Number valorMinimo, Number valorMaximo, Number valor){
		if (valor == null) {
			return true;
		}
		boolean esValido = true;
		if(valor.doubleValue() < valorMinimo.doubleValue() || valor.doubleValue() > valorMaximo.doubleValue()){
			esValido = false;
		}
		return esValido;
	}
	
	private boolean validaDiasSalarioMinimo(Number valor){
		if (valor == null) {
			return true;
		}
		
		boolean esValido = false;
		List<CatalogoValorFijoDTO> catalogoValorFijoDTOs = entidadService.findByProperty(CatalogoValorFijoDTO.class, 
				"id.idGrupoValores", Integer.valueOf(CatalogoValorFijoDTO.IDGRUPO_DIAS_SALARIO));
		if (!catalogoValorFijoDTOs.isEmpty()
				&& catalogoValorFijoDTOs != null) {
				for (CatalogoValorFijoDTO estatus : catalogoValorFijoDTOs) {
					if(valor.doubleValue() == Double.parseDouble(estatus.getDescripcion())){
						esValido = true;
					}
				}
		}
		return esValido;
	}	
	
	public String remplazaGuion(String valor) {
		return valor.replaceAll("_", " ");
	}
	


	public void setCotizacion(CotizacionDTO cotizacion) {
		this.cotizacion = cotizacion;
	}

	public CotizacionDTO getCotizacion() {
		return cotizacion;
	}

	public BitemporalCotizacion getBitemporalCotizacion() {
		return bitemporalCotizacion;
	}


	public void setBitemporalCotizacion(BitemporalCotizacion bitemporalCotizacion) {
		this.bitemporalCotizacion = bitemporalCotizacion;
	}


	public IncisoService getIncisoServiceBitemporal() {
		return incisoServiceBitemporal;
	}


	public void setIncisoServiceBitemporal(IncisoService incisoServiceBitemporal) {
		this.incisoServiceBitemporal = incisoServiceBitemporal;
	}


	public CargaMasivaService getCargaMasivaService() {
		return cargaMasivaService;
	}


	public void setCargaMasivaService(CargaMasivaService cargaMasivaService) {
		this.cargaMasivaService = cargaMasivaService;
	}


	public EntidadService getEntidadService() {
		return entidadService;
	}


	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	

}
