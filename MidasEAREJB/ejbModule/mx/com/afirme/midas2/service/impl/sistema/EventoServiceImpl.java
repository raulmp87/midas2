package mx.com.afirme.midas2.service.impl.sistema;

import java.util.Date;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import mx.com.afirme.midas2.domain.sistema.Evento;
import mx.com.afirme.midas2.domain.sistema.EventoMidas;
import mx.com.afirme.midas2.service.sistema.EventoService;
@Stateless
public class EventoServiceImpl implements EventoService {

	@PersistenceContext
	private EntityManager em;
	
	@Override
	public List<Evento> obtenerEventos() {
		
		String jpql = "select m from EventoSeycos m order by m.fechaCreacion";
		TypedQuery<Evento> query = em.createQuery(jpql, Evento.class);
		List<Evento> eventos = query.getResultList();
		
		
		jpql =  "select m from EventoMidas m order by m.fechaCreacion";
		query = em.createQuery(jpql, Evento.class);
		List<Evento> eventosMidas = query.getResultList();
		
		eventos.addAll(eventosMidas);
		
		return eventos;
		
	}
	
	@Override
	public EventoMidas guardarEvento(String nombre, String mensaje) {
		EventoMidas evento = new EventoMidas();
		evento.setNombre(nombre);
		evento.setMensaje(mensaje);
		evento.setFechaCreacion ( new Date());
		em.persist(evento);
		return evento;
	}
	
	@Override
	public void borrarEventoProcesado(Evento evento) {
		Evento eventoEnContexto = em.merge(evento);
		em.remove(eventoEnContexto);
		
	}
	
}
