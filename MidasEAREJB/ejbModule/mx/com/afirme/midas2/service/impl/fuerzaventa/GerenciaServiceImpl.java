package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaService;

@Stateless
public class GerenciaServiceImpl extends FuerzaDeVentaServiceImpl implements GerenciaService {
	@EJB private EntidadService entidadService;
	@Override
	public List<RegistroFuerzaDeVentaDTO> listarGerencias() {		
		return super.listar(null, null,
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.GERENCIA.obtenerTipo());
	}

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarGerenciasPorCentroEmisor(
			Object id) {
		return super.listar(id,
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.CENTRO_EMISOR.obtenerTipo(),
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.GERENCIA.obtenerTipo());
	}

	@Override
	public List<Gerencia> listarGerenciasPorCentroOperacion(
			Long idCentroOperacion) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("centroOperacion.idCentroOperacion", idCentroOperacion);
		return entidadService.findByProperty(Gerencia.class, "centroOperacion.idCentroOperacion", idCentroOperacion);
	}

}
