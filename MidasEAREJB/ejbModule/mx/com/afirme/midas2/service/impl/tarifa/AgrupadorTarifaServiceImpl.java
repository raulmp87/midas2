package mx.com.afirme.midas2.service.impl.tarifa;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.dao.catalogos.AgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.tarifa.AgrupadorTarifaDao;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaId;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaService;
import mx.com.afirme.midas2.util.UtileriasWeb;

@Stateless
public class AgrupadorTarifaServiceImpl implements AgrupadorTarifaService {

	final static String VERSION = "Version: ";
	
	protected AgrupadorTarifaSeccionDao agrupadorTarifaSeccionDao;
	
	@EJB
	public void setAgrupadorTarifaSeccionDao(
			AgrupadorTarifaSeccionDao agrupadorTarifaSeccionDao) {
		this.agrupadorTarifaSeccionDao = agrupadorTarifaSeccionDao;
	}
	

	@Override
	public List<AgrupadorTarifa> findByFilters(
			AgrupadorTarifa filtroAgrupadorTarifa) {

		return agrupadorTarifaDao.findByFilters(filtroAgrupadorTarifa);
	}

	/**
	 * Busca una Entidad de tipo AgrupadorTarifa
	 * 
	 * @param id
	 *            corresponde al id de la entidad a buscar se sompone de una
	 *            cadena de 2 valores separada por _ (valor_valor)
	 * @return AgrupadorTarifa entidad encontrada
	 */
	@Override
	public AgrupadorTarifa findById(String id) {
		
		String[] arrId = id.split(UtileriasWeb.SEPARADOR);
		AgrupadorTarifaId agrupadorTarifaId = new AgrupadorTarifaId(
				new BigDecimal(arrId[0]), new BigDecimal(arrId[1]));
		return entidadDao.findById(AgrupadorTarifa.class, agrupadorTarifaId);
		
	}

	private EntidadDao entidadDao;

	private AgrupadorTarifaDao agrupadorTarifaDao;

	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	@EJB
	public void setAgrupadorTarifaDao(AgrupadorTarifaDao agrupadorTarifaDao) {
		this.agrupadorTarifaDao = agrupadorTarifaDao;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public AgrupadorTarifa save(AgrupadorTarifa agrupadorTarifa) {
		
		List<AgrupadorTarifa> agrupadores = agrupadorTarifaDao
				.findByFilters(agrupadorTarifa);
		
		Calendar finVigencia = Calendar.getInstance();
		finVigencia.set(4712, 11, 31);		
		

		if (agrupadorTarifa.getId() != null) {
			if (agrupadorTarifa.getClaveEstatus() == AgrupadorTarifa.ESTATUS_ACTIVO) {
				this.actualizarAgrupadores(agrupadores,
						AgrupadorTarifa.ESTATUS_NO_ACTIVO);
				agrupadorTarifa.setFechaActivacion(new Date());	
				agrupadorTarifa.setFechaInicioVigencia(new Date());
				agrupadorTarifa.setFechaFinVigencia(finVigencia.getTime());
			}
			entidadDao.update(agrupadorTarifa);
			return agrupadorTarifa;
		} else {
			this.actualizarAgrupadores(agrupadores,
					AgrupadorTarifa.ESTATUS_NO_ACTIVO);
			AgrupadorTarifaId id;
			if (agrupadores != null && agrupadores.size() > 0) {
				Collections.sort(agrupadores);
				AgrupadorTarifa agrupadorBase = agrupadores.get(agrupadores.size()-1);

//				id = new AgrupadorTarifaId(agrupadorBase.getId()
//						.getIdToAgrupadorTarifa(), agrupadorBase.getId()
//						.getIdVerAgrupadorTarifa().add(BigDecimal.ONE));
				
				AgrupadorTarifa agrupador = new AgrupadorTarifa();
				AgrupadorTarifaId agrupadorId = new AgrupadorTarifaId();
				Long idVerAgrupadorTarifa = new Long (0);
				idVerAgrupadorTarifa = agrupadorTarifaDao.createNewVersion(
						agrupadorBase.getId().getIdToAgrupadorTarifa(), 
						agrupadorBase.getId().getIdVerAgrupadorTarifa());
				agrupadorId.setIdToAgrupadorTarifa(agrupadorBase.getId().getIdToAgrupadorTarifa());
				agrupadorId.setIdVerAgrupadorTarifa(new BigDecimal(idVerAgrupadorTarifa));
				agrupador.setId(agrupadorId);
				return entidadDao.getReference(AgrupadorTarifa.class,
						agrupador.getId());
			} else {

				id = new AgrupadorTarifaId(this.getNextSequence(), BigDecimal.ONE);
				agrupadorTarifa.setId(id);
				agrupadorTarifa.setClaveEstatus(AgrupadorTarifa.ESTATUS_CREADO);
				agrupadorTarifa.setFechaActivacion(new Date());
				agrupadorTarifa.setFechaFinVigencia(finVigencia.getTime());
				agrupadorTarifa.setFechaInicioVigencia(new Date());
				agrupadorTarifa.setFechaCreacion(new Date());
				agrupadorTarifa.setDescripcionVersion(VERSION
						+ id.getIdVerAgrupadorTarifa().toString());
				System.out.println("id-->"+id);
				entidadDao.persist(agrupadorTarifa);
				return entidadDao.getReference(AgrupadorTarifa.class,
						agrupadorTarifa.getId());
			}

			
		}

	}

	private BigDecimal BigDecimal(Long idVerAgrupadorTarifa) {
		return null;
	}


	private void actualizarAgrupadores(List<AgrupadorTarifa> agrupadores,
			Short estaus) {
		for (AgrupadorTarifa agrupador : agrupadores) {
			if (agrupador.getClaveEstatus() != AgrupadorTarifa.ESTATUS_BORRADO) {
				if (agrupador.getClaveEstatus() != AgrupadorTarifa.ESTATUS_CREADO) {
					agrupador.setClaveEstatus(estaus);
					entidadDao.update(agrupador);
				}
			}
		}
	}

	public BigDecimal getNextSequence() {
		String query = "select max(IDTOAGRUPADORTARIFA) from MIDAS.TOAGRUPADORTARIFA";
		BigDecimal nextSequence = BigDecimal.ONE;
		return nextSequence.add((BigDecimal) entidadDao
				.executeNativeQuerySimpleResult(query));
	}


	public AgrupadorTarifa createNewVersion(AgrupadorTarifa agrupadorTarifa) {
		Long nuevaVersion = null;
		if(agrupadorTarifa != null){
			nuevaVersion = agrupadorTarifaDao.createNewVersion(agrupadorTarifa
					.getId().getIdToAgrupadorTarifa(), agrupadorTarifa.getId()
					.getIdVerAgrupadorTarifa());
		}
		return entidadDao.findById(AgrupadorTarifa.class, nuevaVersion);
	}
	
	public Map<BigDecimal, String> findByNegocio(String claveNegocio, Long idMoneda){
		
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		
		String filtro = "";
		
		List<AgrupadorTarifa> agrupadorTarifaList = agrupadorTarifaDao.findByNegocio(claveNegocio, filtro);
		
		for(AgrupadorTarifa item : agrupadorTarifaList){
			map.put(item.getId().getIdToAgrupadorTarifa(), item.getDescripcionAgrupador());
		}
		
		return map;
	}

	@Override
	public AgrupadorTarifa consultaVersionActiva(BigDecimal id) {
		return agrupadorTarifaDao.consultaVersionActiva(id);
	}
	
	@SuppressWarnings("unchecked")
	public Map<BigDecimal, String> findByAgrupadorPorMonedaPorSeccion(
			String claveNegocio, BigDecimal idSeccion, Long idMoneda) {
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		final String query = "SELECT DISTINCT agrupadorSeccion FROM " +
			"AgrupadorTarifaSeccion agrupadorSeccion, AgrupadorTarifa  agrupador " +
			"WHERE agrupadorSeccion.id.idToSeccion = :idToSeccion  " +
			"AND agrupadorSeccion.id.idMoneda = :idMoneda " +			
			"AND agrupadorSeccion.id.idToAgrupadorTarifa = agrupador.id.idToAgrupadorTarifa " +
			"AND agrupador.claveNegocio = :claveNegocio " +
			"AND agrupador.claveEstatus IN (1,2) ";
		Map<String, Object> params = new HashMap<String, Object>();
		
		List<AgrupadorTarifaSeccion> agrupadorTarifaSeccion = null;
		params.put("idToSeccion", idSeccion);
		params.put("idMoneda", idMoneda);
		params.put("claveNegocio", claveNegocio);
		
		agrupadorTarifaSeccion = agrupadorTarifaSeccionDao.executeQueryMultipleResult(query, params);
		
		for (AgrupadorTarifaSeccion item : agrupadorTarifaSeccion) {
			if(item.getId() != null){					
				item.setAgrupadorTarifa(agrupadorTarifaDao
					.consultaVersionActiva(item.getId().getIdToAgrupadorTarifa()));
			}
		}
		
		for (AgrupadorTarifaSeccion item : agrupadorTarifaSeccion) {
			AgrupadorTarifa agrupadorTarifa = entidadDao.findById(AgrupadorTarifa.class, item.getAgrupadorTarifa().getId());
			map.put(item.getAgrupadorTarifa().getId().getIdToAgrupadorTarifa(), 
					agrupadorTarifa.getDescripcionAgrupador());
		}	
		
	return map;
	}

}
