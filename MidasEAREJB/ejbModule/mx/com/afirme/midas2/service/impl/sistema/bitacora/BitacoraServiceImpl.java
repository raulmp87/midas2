package mx.com.afirme.midas2.service.impl.sistema.bitacora;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;

import org.apache.log4j.Logger;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

@Stateless
public class BitacoraServiceImpl implements BitacoraService {

	@EJB
	private EntidadService entidadService;

	private Gson gson = new GsonBuilder().setDateFormat("dd/MM/yyyy").create();
	
	private static Logger log = Logger.getLogger("BitacoraServiceImpl"); 

	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void registrar(Bitacora bitacora) {
		try{
			entidadService.save(bitacora);
		}catch(Exception ex){
			log.error(ex);
		}
	}
	@Override
	public void registrar(TIPO_BITACORA tipoBitacora, EVENTO evento,
			String identificador, String mensaje, String detalle, String contenedorDetalle, String cveUsuario) {
		Bitacora bitacora = new Bitacora(tipoBitacora, evento, identificador,
				mensaje, detalle, contenedorDetalle, cveUsuario);
		registrar(bitacora);
	}

	@Override
	public void registrar(TIPO_BITACORA tipoBitacora, EVENTO evento,
			String identificador, String mensaje, String detalle, String cveUsuario) {
		registrar(tipoBitacora, evento, identificador, mensaje, detalle, null, cveUsuario);
	}
	
	@Override
	public <T extends Object> void registrar(TIPO_BITACORA bitacora,
			EVENTO evento, String identificador, String mensaje, T objetoDetalle, String cveUsuario) {
		try{
			String objSerializado = gson.toJson(objetoDetalle);
			String tipoObjeto = objetoDetalle.getClass().getCanonicalName();			
			registrar(bitacora, evento, identificador, mensaje, objSerializado,
					tipoObjeto, cveUsuario);
		}catch(Exception ex){
			log.error(ex);
		}
	}

	@Override
	public List<Bitacora> obtener(TIPO_BITACORA bitacora, EVENTO evento,
			String identificador) {
		BitacoraFiltro filtro = new BitacoraFiltro();
		filtro.setBitacora(bitacora);
		filtro.setEvento(evento);
		filtro.setIdentificador(identificador);
		return obtener(filtro);
	}

	@Override
	public List<Bitacora> obtener(BitacoraFiltro filtro) {
		List<Bitacora> bitacoras = entidadService.findByFilterObject(
				Bitacora.class, filtro, "fechaCreacion");
		return bitacoras;
	}

}
