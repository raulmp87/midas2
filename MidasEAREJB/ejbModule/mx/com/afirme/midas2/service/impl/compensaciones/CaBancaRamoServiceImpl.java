/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Abril 2016
 * @author Mario Dominguez
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaBancaRamoDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaBancaRamoDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaBancaRamo;
import mx.com.afirme.midas2.service.compensaciones.CaBancaRamoService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Stateless

public class CaBancaRamoServiceImpl  implements CaBancaRamoService {
	
	@EJB
	private CaBancaRamoDao caBancaRamo;
	private static final Logger LOG = LoggerFactory.getLogger(CaBancaRamoDaoImpl.class);

    public void save(CaBancaRamo entity) {   
	    try {
	    	LOG.info(">> save()");
	    	caBancaRamo.save(entity);	  
	    	LOG.info("<< save()");
	    } catch (RuntimeException re) {	 
	    	LOG.error("Información del Error", re);
	        throw re;
	    }
    }
    

    public void delete(CaBancaRamo entity) { 
    	try {
    		LOG.info(">> delete()");
    		caBancaRamo.delete(entity);
    		LOG.info("<< delete()");
    	} catch (RuntimeException re) {
    		LOG.error("Información del Error", re);
    		throw re;
    	}
    }
    

    public CaBancaRamo update(CaBancaRamo entity) {   
        try {
        	LOG.info(">> update()");
	        CaBancaRamo result = caBancaRamo.update(entity);
	        LOG.info("<< update()");
	        return result;
	    } catch (RuntimeException re) {
	    	LOG.error("Información del Error", re);
            throw re;
	    }
    }
    
    public CaBancaRamo findById( Long id) {
    	LOG.info(">> findById()");
	    try {
            CaBancaRamo instance = caBancaRamo.findById(id);
           	LOG.info("<< findById()");
            return instance;
        } catch (RuntimeException re) {
        	LOG.error("Información del Error", re);
        }
        return null;
    }   


    public List<CaBancaRamo> findByProperty(String propertyName, final Object value) {    	
		try {
			return caBancaRamo.findByProperty(propertyName, value);
		} catch (RuntimeException re) {
			
			return null;
		}
	}			

	
	public List<CaBancaRamo> findAll() {	
		try {
			return caBancaRamo.findAll();
		} catch (RuntimeException re) {
			return null;
		}
	}
	
}