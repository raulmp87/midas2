/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.sql.Timestamp;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import mx.com.afirme.midas2.dao.compensaciones.CaBitacoraDao;
import mx.com.afirme.midas2.domain.compensaciones.CaBitacora;
import mx.com.afirme.midas2.service.compensaciones.CaBitacoraService;


@Stateless

public class CaBitacoraServiceImpl  implements CaBitacoraService {
	public static final String USUARIO = "usuario";
	public static final String FECHA = "fecha";
	public static final String HORA = "hora";
	public static final String MOVIMIENTO = "movimiento";
	public static final String COMPENSACION_ID = "compensacionId";
	public static final String VALOR_ANTERIOR = "valorAnterior";
	public static final String VALOR_POSTERIOR = "valorPosterior";
	
	
	@EJB
	private CaBitacoraDao bitacoracaDao;
	
	@PersistenceContext
	  private EntityManager entityManager;
	
	
	/** Log de CaBitacoraServiceImpl */
	private static final Logger LOGGER= LoggerFactory
			.getLogger(CaBitacoraServiceImpl.class);
  
    /**
	 Perform an initial save of a previously unsaved CaBitacora entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaBitacora entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaBitacora entity) {
    	LOGGER.info(">> save()");
        try {
        	bitacoracaDao.save(entity);
        	LOGGER.info(">> update()");
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al guardar CaBitacora 	::		CaBitacoraServiceImpl	::	save	::	ERROR	::	",re);
            throw re;
        }
    }
    
    /**
	 Delete a persistent CaBitacora entity.
	  @param entity CaBitacora entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaBitacora entity) {
    	LOGGER.info(">> delete()");
	        try {
//        	entity = entityManager.getReference(CaBitacora.class, entity.getId());
//            entityManager.remove(entity);
        	bitacoracaDao.delete(entity);
            LOGGER.info(">>	delete()");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaBitacora 	::		CaBitacoraServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaBitacora entity and return it or a copy of it to the sender. 
	 A copy of the CaBitacora entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaBitacora entity to update
	 @return CaBitacora the persisted CaBitacora entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaBitacora update(CaBitacora entity) {
    	LOGGER.info(">> update() ");
	        try {
            CaBitacora result = bitacoracaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info(">> update()");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaBitacora 	::		CaBitacoraServiceImpl	::	update	::	ERROR	::	",re);
	        throw re;
        }
    }
    
    public CaBitacora findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaBitacoraServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaBitacora instance = bitacoracaDao.findById(id);//entityManager.find(CaBitacora.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaBitacoraServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaBitacoraServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    

/**
	 * Find all CaBitacora entities with a specific property value.  
	 
	  @param propertyName the name of the CaBitacora property to query
	  @param value the property value to match
	  	  @return List<CaBitacora> found by query
	 */

    public List<CaBitacora> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaBitacoraServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaBitacora model where model." 
//								+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaBitacoraServiceImpl	::	findByProperty	::	FIN	::	");
			return bitacoracaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaBitacoraServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaBitacora> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaBitacora> findByHora(Object hora
	) {
		return findByProperty(HORA, hora
		);
	}
	
	public List<CaBitacora> findByMovimiento(Object movimiento
	) {
		return findByProperty(MOVIMIENTO, movimiento
		);
	}
	
	public List<CaBitacora> findByValorAnterior(Object valorAnterior
	) {
		return findByProperty(VALOR_ANTERIOR, valorAnterior
		);
	}
	
	public List<CaBitacora> findByValorPosterior(Object valorPosterior
	) {
		return findByProperty(VALOR_POSTERIOR, valorPosterior
		);
	}
	
	
	/**
	 * Find all CaBitacora entities.
	  	  @return List<CaBitacora> all CaBitacora entities
	 */
	public List<CaBitacora> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaBitacoraServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaBitacora model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaBitacoraServiceImpl	::	findAll	::	FIN	::	");
			return bitacoracaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaBitacoraServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
	public List<CaBitacora> obtenerBitacoraPorConfiguracionCalc(Long caBancaConfigCalId){
		LOGGER.info("obtenerBitacoraPorConfiguracionCalc() >> ");
		List<CaBitacora> lista = new ArrayList<CaBitacora>();
		try{
			final StringBuilder queryString = new StringBuilder();
			queryString.append(" SELECT FECHA,USUARIO,MOVIMIENTO FROM MIDAS.CA_BITACORA");
			queryString.append(" where CABANCACONFIGCAL_ID = ?1 " );
			queryString.append(" ORDER BY FECHA DESC");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1,caBancaConfigCalId);
			List<Object[]> resultList = query.getResultList();
			for(Object[] result : resultList){
				CaBitacora bitacora = new CaBitacora();
				bitacora.setFecha((Date) result[0]);
				bitacora.setUsuario(result[1].toString());
				bitacora.setMovimiento(result[2].toString());
				lista.add(bitacora);
			}
			
		}catch(RuntimeException re){
			LOGGER.error("-- obtenerBitacoraPorConfiguracionCalc()", re);
	        throw re; 
			
		}
		return lista;
	}
	
	@Override
	public List<CaBitacora> obtenerBitacoraPorCompensacionId(Long compensacionId) {
		LOGGER.info("obtenerBitacoraPorCompensacionId() compensacionId => {}", compensacionId);
		
		List<CaBitacora > lista = new ArrayList<CaBitacora>();
	    try {
	      final StringBuilder queryString = new StringBuilder();
	      queryString.append(" select USUARIO,FECHA,MOVIMIENTO,COMPENSACION_ID from MIDAS.ca_bitacora ");
	      queryString.append(" where compensacion_id = ?1 ");
	      queryString.append(" ORDER BY FECHA DESC ");
	      
	      Query query=entityManager.createNativeQuery(queryString.toString());
	      query.setParameter(1, compensacionId); 	      
	      List<Object[]> resultList=query.getResultList();
	      for (Object[] result : resultList){ 
	    	  
	    	  CaBitacora bitacoraTmp= new CaBitacora();
	    	  bitacoraTmp.setUsuario(result[0].toString());
	    	  bitacoraTmp.setFecha(Timestamp.valueOf(result[1].toString()));
	    	  bitacoraTmp.setMovimiento(result[2].toString());	    	 
	    	  bitacoraTmp.setCompensacionId(new Long(((BigDecimal)result[3]).toString()));     	 
	    	  
                lista.add(bitacoraTmp);
              }
	    } catch (RuntimeException re) {
	    	LOGGER.error("-- obtenerBitacoraPorCompensacionId()", re);
	        throw re; 
	    }    	    
	    return lista;
	}	
}