package mx.com.afirme.midas2.service.impl.siniestros.cabina.reportecabina;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroDao;
import mx.com.afirme.midas2.dao.siniestros.cabina.reportecabina.MovimientoSiniestroDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionCiaDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.DireccionMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.domain.personadireccion.PersonaMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina.TerminoAjuste;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CondicionEspecialReporte;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoPaseAtencion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionGenerica;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina.EstatusReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SeccionReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicos;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicosConductor;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCBienes;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCPersonas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCViajero;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion;
import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CartaCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ComplementoCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.juridico.ReclamacionReservaJuridicoDTO;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosEstimacionCoberturaDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosReporteSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionExportacionDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.PaseAtencionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.depuracion.CoberturaSeccionSiniestroDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.impl.bitemporal.siniestros.PolizaSiniestroServiceImpl;
import mx.com.afirme.midas2.service.siniestros.sipac.ReporteLayoutSipacService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.provision.SiniestrosProvisionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionCiaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSipacService;
import mx.com.afirme.midas2.service.siniestros.spv.ServicioPublicoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.hgs.HgsService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.permisos.OrdenCompraAutorizacionService;
import mx.com.afirme.midas2.service.vehiculo.ValorComercialVehiculoService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;


@Stateless
public class EstimacionCoberturaSiniestroServiceImpl implements EstimacionCoberturaSiniestroService{
	private static final Logger LOG = Logger.getLogger(EstimacionCoberturaSiniestroServiceImpl.class);
	private static final int	DIAS_EXPIRAR_PASE	= 5;	
	private static final String COBERTURA_INDEPENDIENTE = "INDP";
	private static final String SUMA_ASEG_X_PASAJERO = "PASAJERO";
	private static final String SUMA_ASEG_NO_REINSTALABLE   = "0";
	private static final String SUMA_ASEG_REINSTALABLE      = "1";

//	Titulos estimacion
	private static final String TITULO_ESTIMACION_DMA 	 = "DAÑOS MATERIALES";
	private static final String TITULO_ESTIMACION_RCV 	 = "RC VEH�?CULO";
	private static final String TITULO_ESTIMACION_GME 	 = "PASE DE SERVICIO MÉDICO";
	private static final String TITULO2_ESTIMACION_GME 	 = "LESIONADOS EN ACCIDENTES DE AUTOS";
	private static final String TITULO_ESTIMACION_RCP 	 = "RC PERSONAS";
	private static final String TITULO_ESTIMACION_RCJ 	 = "PASE DE SERVICIO MÉDICO PARA R.C. VIAJERO";
	private static final String TITULO_ESTIMACION_GMC    = "PASE DE SERVICIO MÉDICO PARA ACCIDENTES AUTOMOVILISTICOS AL CONDUCTOR";
	private static final String TITULO_ESTIMACION_RCB 	 = "BIENES";
	
//	Numero maximo de impresiones con cierta leyenda
	private static final String   ORIGINAL_IMPRESION = "Original";
	private static final String   ORIGINAL_REIMPRESION = "Original Reimpresión";
	private static final String   COPIA = "Copia";
	private static final String DOCUMENTO_NO_VALIDO = "Documento no válido";
	private static final Long   MAXIMO_ORIGINAL_IMPRESION = 3L;
	
//	Rutas de jrxml a utilizar
	private static final String PASE_ATENCION_DMA       = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/siniestros/paseAtencionDMA.jrxml";
	private static final String PASE_ATENCION_VEHICULO  = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/siniestros/paseAtencionVehiculo.jrxml";
	private static final String PASE_ATENCION_MEDICO    = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/siniestros/paseAtencionMedico.jrxml";
	private static final String PASE_ATENCION_BIENES    = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/siniestros/paseAtencionBienes.jrxml";

	
//	Tipos de Rol
	private static final String ROL_AJUSTADOR = "Rol_M2_Ajustador";
	private static final String ROL_COORDINADOR_CABINA_AJUSTES_SINIESTROS = "Rol_M2_Coordinador_Cabina_Ajustes_Siniestros";
	private static final String ROL_COORDINADOR_VALUACION_SINIESTROS = "Rol_M2_Coordinador_Valuacion_Siniestros";
	private static final String ROL_COORDINADOR_AJUSTES_AUTOS = "Rol_M2_Coordinador_Ajustes_Autos";
	private static final String ROL_SUBDIRECTOR_SINIESTROS_AUTOS = "Rol_M2_SubDirector_Siniestros_Autos";	
	
	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private EstimacionCoberturaSiniestroDao estimacionCoberturaDao;
	
	@EJB
    private ReporteCabinaService reporteCabinaService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private SiniestroCabinaService siniestroCabinaService;
	
	@EJB
	private PolizaSiniestroService polizaSiniestroService;
	
	@EJB
	private ValorComercialVehiculoService valorComercialVehiculoService;

	@EJB
	private EnvioNotificacionesService	notificacionService;	
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
		
	@EJB	 
	private OrdenCompraService ordenCompraService;
	
	@EJB
	private HgsService hgsService;
	
	@EJB
	private RecuperacionDeducibleService recuperacionDeducibleService;
	
	@EJB
	private RecuperacionCiaService recuperacionCiaService;

	@EJB
	private RecuperacionSipacService recuperacionSipacService;

	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;

	@EJB
	private MovimientoSiniestroDao movimientoSiniestroDao;
	
	@EJB
	private PerdidaTotalService perdidaTotalService;
	
	@EJB
	private RecuperacionCiaDao recuperacionCiaDao;
	
	@EJB
	private ServicioPublicoService servicioPublicoService;
	@EJB
	protected FronterizosService fronterizosService;
	
	@EJB
	private OrdenCompraAutorizacionService ordenCompraAutorizacionService;

	@EJB
	private SiniestrosProvisionService  siniestrosProvisionService;
	
	@EJB
	private ReporteLayoutSipacService reporteLayoutService;

	
	@Override
	public EstimacionCoberturaReporteCabina obtenerEstimacionCoberturaReporteCabina( Long idEstimacionCoberturaReporteCabina ){
		return entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstimacionCoberturaReporteCabina);

	}
	
	@Override
	public List<PaseAtencionSiniestroDTO> buscarPasesAtencion( PaseAtencionSiniestroDTO filtro ) {		
		
		List<PaseAtencionSiniestroDTO> pasesAtencionSiniestro = estimacionCoberturaDao.buscarPasesAtencion(filtro);
		
		return pasesAtencionSiniestro;
	}
	
	@Override
	public String obtenerNombreCobertura(CoberturaDTO cobertura, String claveTipoCalculo, String claveSubTipoCalculo,
			String tipoEstimacion, String tipoConfiguracion) {
		String nombreCobertura = cobertura.getNombreComercial();
		
		ConfiguracionCalculoCoberturaSiniestro config = obtenerConfiguracionCalcCobertura(claveTipoCalculo, 
				claveSubTipoCalculo, tipoEstimacion, tipoConfiguracion);
		
		if (config != null && config.getNombreCobertura() != null) {
			nombreCobertura = config.getNombreCobertura();
		}
			
		return nombreCobertura;
		
	}

	@Override
	public BigDecimal calculoSumaAseguradaDSMVGDF(Integer diasSalarioMinimo) {
		BigDecimal sumaAsegurada = BigDecimal.ZERO;
		//Obtiene dias de salario minimo
		try{
			CatalogoValorFijoId idDSMVGDF = new CatalogoValorFijoId();
			idDSMVGDF.setIdDato(1);
			idDSMVGDF.setIdGrupoValores(CatalogoValorFijoDTO.IDGRUPO_CLAVE_TIPO_DSMGVDF);
			CatalogoValorFijoDTO dsmvgdf = entidadService.findById(CatalogoValorFijoDTO.class, idDSMVGDF);
			sumaAsegurada = BigDecimal.valueOf(diasSalarioMinimo.doubleValue() * Double.valueOf(dsmvgdf.getDescripcion()));
		}catch(Exception e){
			LOG.error(e);
		}
		return sumaAsegurada;
	}
	
	
	/**
	 * 
	 * Método que obtiene el deducible dado los días de salario mínimo.
	 * @param deducible
	 * @return
	 */
	@Override
	public BigDecimal calculoDeducibleDSMVGDF(BigDecimal deducible){
		BigDecimal sumaAsegurada = BigDecimal.ZERO;
		//Obtiene dias de salario minimo
		try{
			CatalogoValorFijoId idDSMVGDF = new CatalogoValorFijoId();
			idDSMVGDF.setIdDato(1);
			idDSMVGDF.setIdGrupoValores(CatalogoValorFijoDTO.IDGRUPO_CLAVE_TIPO_DSMGVDF);
			CatalogoValorFijoDTO dsmvgdf = entidadService.findById(CatalogoValorFijoDTO.class, idDSMVGDF);
			sumaAsegurada = deducible .multiply(BigDecimal.valueOf(Double.valueOf(dsmvgdf.getDescripcion())));
			
		}catch(Exception e){
			LOG.error(e);
		}
		return sumaAsegurada;
	}

	@Override
	public EstimacionCoberturaReporteCabina generarFolio(ReporteCabina reporteCabina,EstimacionCoberturaReporteCabina estimacionCobertura) {
		Integer folio = this.estimacionCoberturaDao.obtenerSecuenciaPaseAtencion(reporteCabina.getId());
		estimacionCobertura.setSecuenciaPaseAtencion(folio);
		estimacionCobertura.setFolio(reporteCabina.getClaveOficina() + reporteCabina.getConsecutivoReporte() + '-' 
				+ reporteCabina.getAnioReporte() + '-' + folio);
		
		return estimacionCobertura;
	}
	
	//QUE PASA CON ESTE METODO.... PORQUE SE INVOCA DOS VECES EL guardarEstimacionCobertura ?
//	@Override
//	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
//	public List<String> guardarEstimacionCoberturaProvision(EstimacionCoberturaSiniestroDTO dto, Long idCoberturaReporte, String tipoEstimacion){
//		List<String> errores = this.guardarEstimacionCobertura(dto,idCoberturaReporte,tipoEstimacion);
//		this.guardarEstimacionCobertura(dto, idCoberturaReporte, tipoEstimacion);
// 		return errores;
//	}
	
	@Override
	public List<String> guardarEstimacionCobertura(EstimacionCoberturaSiniestroDTO dto, Long idCoberturaReporte, String tipoEstimacion) {
		EstimacionCoberturaReporteCabina estimacion = null;
		List<String> errorMessages = new ArrayList<String>();		
		boolean esNuevo = Boolean.TRUE;
		boolean cambioDeducible = Boolean.FALSE;
		try {
			
			CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporte);
			estimacion = this.selectEstimacionCoberturaReporteCabina(dto, tipoEstimacion, coberturaReporteCabina);	
			Map<String,Object> params = new HashMap<String,Object>();
			String cveTipoCalculoCobertura = estimacion.getCoberturaReporteCabina().getClaveTipoCalculo();
			params.put( "tipoEstimacion", tipoEstimacion );
			params.put( "cveTipoCalculoCobertura", cveTipoCalculoCobertura );
			List<ConfiguracionCalculoCoberturaSiniestro> confCalculos =  this.entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, params);
			ConfiguracionCalculoCoberturaSiniestro confCalculo = null;
			if(confCalculos.size()>0){
				confCalculo = confCalculos.get(0);
				estimacion.setCveSubCalculo(confCalculo.getCveSubTipoCalculoCobertura());
			}
			
			estimacion.setCoberturaReporteCabina(coberturaReporteCabina);
			estimacion.setMotivoNoAplicaDeducible(dto.getMotivoNoAplicaDeducible());
			
			if(estimacion.getId()==null){
				estimacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
				Integer secuenciaPaseDeCobertura = this.estimacionCoberturaDao.obtenerSecuenciaPaseDeCobertura(estimacion.getCoberturaReporteCabina().getId());
				estimacion.setSecuenciaPaseDeCobertura(secuenciaPaseDeCobertura);
				//estimacion.setSumaAseguradaObtenida(dto.getEstimacionGenerica().getSumaAseguradaObtenida());
			}else{
				esNuevo = Boolean.FALSE;
				EstimacionCoberturaReporteCabina estimacionRegistrada = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacion.getId());
				estimacion.setCodigoUsuarioCreacion(estimacionRegistrada.getCodigoUsuarioCreacion());
				estimacion.setFechaCreacion(estimacionRegistrada.getFechaCreacion());
				estimacion.setCodigoUsuarioModificacion(estimacionRegistrada.getCodigoUsuarioModificacion());
				estimacion.setFechaModificacion(Calendar.getInstance().getTime());
				estimacion.setDepuracionTotal(estimacionRegistrada.getDepuracionTotal());
				estimacion.setFechaDepuracionTotal(estimacionRegistrada.getFechaDepuracionTotal());
				estimacion.setConfiguracionDepuracion(estimacionRegistrada.getConfiguracionDepuracion());
				//only for robos coverage
				if(coberturaReporteCabina != null){
					if(coberturaReporteCabina.getClaveTipoCalculo().equals("RT")){
						estimacion.setSumaAseguradaObtenida(dto.getEstimacionGenerica().getSumaAseguradaObtenida());	
					}
				}
				//revisar deducible guardado y validar si se puede editar
				BigDecimal deducible = recuperacionDeducibleService.obtenerDeduciblePorEstimacion(estimacion.getId());
				if(dto.getMontoDeducible() != null && deducible.longValue() != dto.getMontoDeducible().longValue()){
					cambioDeducible = true;
					/*
					if(!permiteCambiarDeduciblePorIndemnizacion(estimacion.getId())){
						errorMessages.add("No se permite editar el deducible ya que existe una orden de compra con al menos un nivel de autorizaciÃ³n para la cobertura afectada.");
						return errorMessages;
					}*/
					if(!permiteCambiarDeduciblePorRecuperacion(estimacion.getId())){
						errorMessages.add("No se permite editar el deducible ya que existe una recuperacion con estatus diferente a registrada.");
						return errorMessages;
					}
				}							
			}
			
			if( ( ( !EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion)
                    && StringUtil.isEmpty(estimacion.getEmail()) )
                    || ( EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion) 
                                && StringUtil.isEmpty(dto.getEstimacionGenerica().getCorreo()) ) )
                    && (!estimacion.getEmailNoProporcionado()
                                || estimacion.getEmailNoProporcionado() 
                                && StringUtil.isEmpty(estimacion.getEmailNoProporcionadoMotivo()))){
              errorMessages.add("Debe ingresar el campo Email Interesado o seleccionar el Motivo Email No Proporcionado");
              return errorMessages;
			}else if( ( ( !EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion)
                    && !StringUtil.isEmpty(estimacion.getEmail()) )
                    || ( EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion) 
                                && !StringUtil.isEmpty(dto.getEstimacionGenerica().getCorreo()) ) )
                    && estimacion.getEmailNoProporcionado() != null
                    && estimacion.getEmailNoProporcionado()){
              errorMessages.add("No debe seleccionar el Motivo Email No Proporcionado si ya ingreso el Email Interesado");
              return errorMessages;
			}
			
			//Validacion para duplicados
			if(validaEstimacionDuplicada(estimacion, dto)){
	              errorMessages.add("Estimacion duplicada en coberturas no permitidas.");
	              return errorMessages;				
			}			
			
			estimacion.setEsEditable("EDT");
			if(estimacion.getCompaniaSegurosTercero() != null && 
					estimacion.getCompaniaSegurosTercero().getId() == null){
				estimacion.setCompaniaSegurosTercero(null);
			}

			
			if( !EnumUtil.equalsValue(TipoPaseAtencion.SOLO_REGISTRO, estimacion.getTipoPaseAtencion())
					&& !EnumUtil.equalsValue(TipoPaseAtencion.SOLO_REG_SIPAC, estimacion.getTipoPaseAtencion()) ) {
				
				Long idOrdenCompraSipac = null;
				if(estimacion.getId()!=null && 
						EnumUtil.equalsValue(TipoPaseAtencion.SIPAC, estimacion.getTipoPaseAtencion())){					
					idOrdenCompraSipac = this.ordenCompraService.actualizarSipac(
								dto.getDatosEstimacion().getEstimacionNueva(),
								estimacion);
				}
				
				errorMessages = this.validarEstimacionCobertura(dto,idCoberturaReporte,estimacion); 
				
				if(!errorMessages.isEmpty()){			
					return errorMessages;
				}


				if(estimacion.getId() == null && !EnumUtil.equalsValue(TipoEstimacion.DIRECTA, estimacion.getTipoEstimacion())){
					this.generarFolio(coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina(), estimacion);
				}
				
				EstimacionCoberturaSiniestroService processor = this.sessionContext.getBusinessObject(EstimacionCoberturaSiniestroService.class);	
				
				estimacion = processor.guardarEstimacion(estimacion);
				MovimientoCoberturaSiniestro movimiento = processor.guardarMovimiento(estimacion.getId(), 
						dto.getDatosEstimacion().getEstimacionNueva(), dto.getDatosEstimacion().getCausaMovimiento());
				//Actualiza la provision
				this.siniestrosProvisionService.actualizaProvision(estimacion.getId(),MovimientoProvisionSiniestros.Origen.PASE );
				
				if(!coberturaReporteCabina.getSiniestrado()){
					coberturaReporteCabina.setSiniestrado(Boolean.TRUE);
					this.entidadService.save(coberturaReporteCabina);
				}

				//Enviar Reporte HGS
				if (EnumUtil.equalsValue(estimacion.getTipoPaseAtencion(), TipoPaseAtencion.PASE_ATENCION_TALLER, TipoPaseAtencion.PAGO_DANIOS)) {
					this.hgsService.insertarReportePaseAtencion(estimacion.getId());
				}
			
				// NOTIFICAR SPV
				this.notificarComplementoSpv(estimacion );
				// PERSISTIR DEDUCIBLE
				if( estimacion.getAplicaDeducible() != null && estimacion.getAplicaDeducible() ){  
					recuperacionDeducibleService.generar(coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId(), estimacion.getId() , dto.getMontoDeducibleCalculado(), dto.getMontoDeducible());
				}
				//actualizar estatus de deducible segun aplique
				if( !esNuevo ){				
					recuperacionDeducibleService.actualizarAplicaDeducible(
														coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId(), 
														estimacion.getId(), 
														estimacion.getAplicaDeducible());
				}				
				
				if (movimiento != null) {
					enviarNotificacion(movimiento);
					if(dto.getMontoRecuperacionSipac()==null){
						validarRecuperacionCia(movimiento);
					}
				}				
				
				if(dto.getMontoRecuperacionSipac()!=null){
					this.recuperacionSipacService.generar(dto.getMontoRecuperacionSipac(),
							estimacion, usuarioService.getUsuarioActual().getNombreUsuario());
					this.reporteLayoutService.guardarLayoutDM(estimacion);
				}
				
				if(EnumUtil.equalsValue(TipoPaseAtencion.SIPAC, estimacion.getTipoPaseAtencion())
						&& idOrdenCompraSipac ==null){
					this.ordenCompraService.crearSipac(
							dto.getDatosEstimacion().getEstimacionNueva(),
							estimacion);
				}		

			}else{
				if(estimacion.getId()!=null){
					String mensajeError = this.validaSiPuedeCambiarEstimacionASoloReporte(estimacion.getId());
					if(!StringUtil.isEmpty(mensajeError)){		
						errorMessages.add(mensajeError);
						return errorMessages;
					}
				}
				if(estimacion.getId() == null && !EnumUtil.equalsValue(TipoEstimacion.DIRECTA, estimacion.getTipoEstimacion())){
					this.generarFolio(coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina(), estimacion);
				}
				this.entidadService.save(estimacion);
			}
			
			if(EnumUtil.equalsValue(TipoPaseAtencion.SOLO_REG_SIPAC, estimacion.getTipoPaseAtencion())){
				this.reporteLayoutService.guardarLayoutRCV(estimacion);
			}
			
			if(!esNuevo && cambioDeducible   ){
				perdidaTotalService.actualizarDeducibleYTotalIndemnizacion(estimacion.getId());
			}else if (!esNuevo &&!estimacion.getAplicaDeducible()){
				perdidaTotalService.actualizarDeducibleYTotalIndemnizacion(estimacion.getId());
			}
			
			
			
		}catch(Exception ex){
			LOG.error(ex);
			LOG.error("No se pudo salvar la estimacion " + ex);
			if(esNuevo && estimacion != null){
				estimacion.setId(null);
				estimacion.setFolio(null);		
			}
			throw new RuntimeException(ex);
		}
		
		/*
		if(confCalculo != null ){
			String tipoEvento = confCalculo.getEvento();
			params.clear();
			params.put( "codigo", tipoEvento );
			params.put( "grupo.codigo", CatGrupoFijo.TIPO_CATALOGO.TIPO_EVENTO.toString() );
			List<CatValorFijo> catValorFijoList = this.entidadService.findByProperties(CatValorFijo.class, params);
			String descripcion = "";
			if(catValorFijoList!=null && catValorFijoList.size()>0){
				descripcion = catValorFijoList.get(0).getDescripcion();
			}
			for (TIPO_EVENTO item : TIPO_EVENTO.values()) {        	
	            if (item.name().equals(tipoEvento)) {
	            	this.reporteCabinaService.guardarEvento(idReporte, item, descripcion);
	            	break;
	            }
	        }
		}*/
		//ESTA LLAMADA A EVENTO SE DEBE DESCOMENTAR, POR EL MOMENTO FALLA 
//		agregaEvento( estimacion,idReporte );
		return errorMessages;
	}
	
	private boolean validaEstimacionDuplicada(EstimacionCoberturaReporteCabina estimacion, EstimacionCoberturaSiniestroDTO dto){

		if (CausaMovimiento.APERTURA_RESERVA.toString().equals(dto.getDatosEstimacion().getCausaMovimiento()) && estimacion.getCoberturaReporteCabina().getCoberturaDTO().getClaveTipoCalculo().matches("(?i)DM|RT|AYC|EQE")){
			List<EstimacionCoberturaReporteCabina> duplicados = entidadService.findByProperty(EstimacionCoberturaReporteCabina.class, "coberturaReporteCabina.id", estimacion.getCoberturaReporteCabina().getId());
			
			if(duplicados != null && duplicados.size() > 0){
				return true;
			}
		}		
		return false;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public MovimientoCoberturaSiniestro guardarMovimiento(Long estimacionId, BigDecimal importe, String causaMovimiento) {

		CausaMovimiento causa = EnumUtil.fromValue(CausaMovimiento.class, causaMovimiento);
		MovimientoCoberturaSiniestro movimiento = movimientoSiniestroService.generarAjusteEstimacion(estimacionId, importe, causa, null);
		
		// ACTUALIZAR DEDUDIBLE
		if(movimiento != null){
			this.recuperacionDeducibleService.actualizarAjusteReserva(estimacionId, importe, movimiento.getTipoMovimiento() );
		}
		
		// GUARDAR MOVIMIENTO DE PROVISION
		this.siniestrosProvisionService.generaMovimientoDeAjusteReserva(estimacionId);
		
		return movimiento;
	}
	
	
	private void enviarNotificacion(MovimientoCoberturaSiniestro movimiento) {		
		if(movimiento != null){
			EstimacionCoberturaReporteCabina estimacion = movimiento.getEstimacionCobertura();
			
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("estimacionCobertura.id", estimacion.getId());
			BigDecimal estimacionFinal = BigDecimal.ZERO;
			List<MovimientoCoberturaSiniestro> movimientos = this.entidadService.findByPropertiesWithOrder(MovimientoCoberturaSiniestro.class, params, "id ASC");
			if(movimientos!=null && !movimientos.isEmpty()){
				estimacionFinal = movimientos.get(0).getImporteMovimiento();
			}
			
			String tipoEstimacion = estimacion.getTipoEstimacion();
			ReporteCabina reporte = estimacion.getCoberturaReporteCabina()
					.getIncisoReporteCabina().getSeccionReporteCabina()
					.getReporteCabina();
			HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();			
			dataArg.put("reporte", reporte);
			dataArg.put("numeroReporte", reporte.getNumeroReporte());
			dataArg.put("estimacion", estimacion);
			dataArg.put("nombreAfectado", estimacion.getNombreAfectado());
			dataArg.put("numeroPoliza", reporte.getPoliza().getNumeroPolizaFormateada());
			dataArg.put("oficinaId", reporte.getOficina().getId());	
			dataArg.put("ajustador", reporte.getAjustador() != null ? reporte.getAjustador().getNombrePersonaAjustador() : "Sin asignar");
			dataArg.put("folioPase", estimacion.getFolio());
			dataArg.put("oficinaSiniestro", reporte.getOficina().getNombreOficina());
			if(!StringUtil.isEmpty(estimacion.getEmail())){
				dataArg.put("emailInteresado", estimacion.getEmail());
				dataArg.put("interesado", estimacion.getNombreAfectado());
			}else if(estimacion.getTipoEstimacion().equals(TipoEstimacion.DIRECTA.toString()) && !StringUtil.isEmpty(((EstimacionGenerica)estimacion).getCorreo())){
				dataArg.put("emailInteresado", ((EstimacionGenerica)estimacion).getCorreo());
				dataArg.put("interesado", ((EstimacionGenerica)estimacion).getCoberturaReporteCabina().getIncisoReporteCabina().getNombreAsegurado());
			}
			
			//Lugar siniestro joksrc notif
			LugarSiniestroMidas ocurrido = reporte.getLugarOcurrido();
			if(ocurrido != null){
				dataArg.put("ubicacionSiniestro", ocurrido.toString());
			}else{
				dataArg.put("ubicacionSiniestro", "Aun no ha sido definida");
			}
			
			
			if(EnumUtil.equalsValue(TipoMovimiento.ESTIMACION_ORIGINAL, movimiento.getTipoMovimiento())){	
				//notificacion robo
				if (EnumUtil.equalsValue(ClaveTipoCalculo.ROBO_TOTAL, estimacion.getCoberturaReporteCabina().getClaveTipoCalculo())) {	
					//notificacion SIGUE AFIRME
					if(contieneCoberturasContratadas(reporte.getId(), ClaveTipoCalculo.PROTECCION_AUTO_SIGUE_AFIRME.getValue())){
						notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.AFECTACION_DE_INCISO_AUTO_SIGUE_AFIRME.toString(), dataArg);
					}
					//mapa exclusivo ROBO_TOTAL.
					dataArg.put("RT_oficinaSiniestro", reporte.getOficina().getNombreOficina());
					dataArg.put("RT_numeroPoliza", reporte.getPoliza().getNumeroPolizaFormateada());
					dataArg.put("RT_asegurado", reporte.getPoliza().getNombreAsegurado());
					dataArg.put("RT_vehiculo", reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getDescripcionFinal() );
					dataArg.put("RT_modelo", reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getModeloVehiculo());
					dataArg.put("RT_serie", reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie() );
					dataArg.put("RT_estimacion_final", estimacionFinal);
					dataArg.put("RT_ciudadSiniestro", reporte.getLugarOcurrido().getCiudad().getDescripcion().toString() );
					dataArg.put("RT_estadoSiniestro", reporte.getLugarOcurrido().getEstado().getDescripcion().toString() );
					dataArg.put("RT_agente", reporte.getPoliza().getCotizacionDTO().getSolicitudDTO().getCodigoAgente() +" - "+ reporte.getPoliza().getCotizacionDTO().getSolicitudDTO().getNombreAgente() );
					
					notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.ROBO_TOTAL.toString(), dataArg);						
				}else if(EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)) {
					
					String tipoAtencion = ((TerceroRCPersonas) estimacion).getEstado();
					if (tipoAtencion.equals("HOM")) {
						// Homicidio
						notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.RC_PERSONAS_HOMICIDIO.toString(),  dataArg);					
					} else if (tipoAtencion.equals("LES")) {
						// lESION
						TerceroRCPersonas tercero = (TerceroRCPersonas)estimacion;
						dataArg.put("nombreAfectado", tercero.getNombreAfectado());
						dataArg.put("edadAfectado", tercero.getEdad());
						dataArg.put("hospital", tercero.getHospital()!= null ? 
								tercero.getHospital().getNombrePersona() : "");
						dataArg.put("lesiones", tercero.getDescripcion());
						notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.RC_PERSONAS_LESION.toString(),  dataArg);
					}
				} else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)) {
					String tipoAtencion = ((TerceroGastosMedicos) estimacion)
							.getEstado();
					if (tipoAtencion.equals("HOM")) {
						// Homicidio
						
						notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.GASTOS_MEDICOS_HOMICIDIO.toString(),  dataArg);
					} else if (tipoAtencion.equals("LES")) {
						// lESION
						TerceroGastosMedicos tercero = (TerceroGastosMedicos)estimacion;
						dataArg.put("nombreAfectado", tercero.getNombreAfectado());	
						dataArg.put("edadAfectado", tercero.getEdad());
						dataArg.put("hospital", tercero.getHospital()!= null ? 
								tercero.getHospital().getNombrePersona() : "");
						dataArg.put("lesiones", tercero.getDescripcion());
						dataArg.put("nombreAfectado", estimacion.getNombreAfectado());	
						
						notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.GASTOS_MEDICOS_LESION.toString(),  dataArg);
					}//recomendacion en caso de siniestro
					notificacionService.enviarNotificacionSiniestros( 
							EnvioNotificacionesService.EnumCodigo.AFECTACION_DE_INCISO_CORREO_CLIENTE.toString(), dataArg);
				} else if (EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)) {
					TerceroRCBienes terceroRCBienes = ((TerceroRCBienes) estimacion);
					PrestadorServicio ingeniero = terceroRCBienes
							.getResponsableReparacion();
					if (ingeniero != null) {					
						dataArg.put("ingeniero", ingeniero.getPersonaMidas().getNombre());
						dataArg.put("domicilio", obtenerDireccion(terceroRCBienes.getUbicacion(), 
								terceroRCBienes.getMunicipio().getId(), terceroRCBienes.getEstado().getId(), terceroRCBienes.getPais().getId()));
						dataArg.put("tipoBien", terceroRCBienes.getTipoBien());
						dataArg.put("ubicacionEvento", terceroRCBienes.getUbicacion()); 
						dataArg.put("ciudad", terceroRCBienes.getMunicipio().getDescripcion());
						dataArg.put("danios", terceroRCBienes.getObservacion());
						dataArg.put("estado", terceroRCBienes.getEstado().getDescripcion());
						dataArg.put("contacto", terceroRCBienes.getNombrePersonaContacto());
						dataArg.put("telefono", terceroRCBienes.getTelefonoContactoCompleto());
						notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.RC_BIENES_ASIGNACION_INGENIERO.toString(),  dataArg);					
					}
				}else if (EnumUtil.equalsValue(ClaveTipoCalculo.DANIOS_MATERIALES, estimacion.getCoberturaReporteCabina().getClaveTipoCalculo())){
					if(contieneCoberturasContratadas(reporte.getId(), ClaveTipoCalculo.PROTECCION_AUTO_SIGUE_AFIRME.getValue())){ //notificacion SIGUE AFIRME
						notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.AFECTACION_DE_INCISO_AUTO_SIGUE_AFIRME.toString(), dataArg);
					}
					//recomendacion en caso de siniestro
					notificacionService.enviarNotificacionSiniestros( 
							
							
							EnvioNotificacionesService.EnumCodigo.AFECTACION_DE_INCISO_CORREO_CLIENTE.toString(), dataArg);
				}
			}else{
				//movimientos a cobertura
				if(EnumUtil.equalsValue(ClaveTipoCalculo.ROBO_TOTAL, estimacion.getCoberturaReporteCabina().getClaveTipoCalculo())){
					
					notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.AFECTACION_RESERVA_ROBO_TOTAL.toString(), dataArg);
					
				}
			}
		}
	}
	
	private String obtenerDireccion(String ubicacion, String municipioId, String estadoId, String paisId){		
		StringBuilder direccion = new StringBuilder("");
		direccion.append(!StringUtil.isEmpty(ubicacion) ? ubicacion  : "");
		if(!StringUtil.isEmpty(municipioId)){
			CiudadMidas municipio = entidadService.findById(CiudadMidas.class, municipioId);
			direccion.append(municipio != null && !StringUtil.isEmpty(municipio.getDescripcion()) ? 
					", ".concat(municipio.getDescripcion()) : "");
		}
		if(!StringUtil.isEmpty(estadoId)){
			EstadoMidas estado = entidadService.findById(EstadoMidas.class, estadoId);
			direccion.append(estado != null && !StringUtil.isEmpty(estado.getDescripcion()) 
					? ", ".concat(estado.getDescripcion()) : "");
		}
		if(!StringUtil.isEmpty(paisId)){
			PaisMidas pais = entidadService.findById(PaisMidas.class, paisId);
			direccion.append(pais != null && !StringUtil.isEmpty(pais.getDescripcion()) 
					? ", ".concat(pais.getDescripcion()) : "");	
		}			
		return direccion.toString();		
	}
	


	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public EstimacionCoberturaReporteCabina guardarEstimacion(EstimacionCoberturaReporteCabina estimacion) {				
		estimacion = entidadService.save(estimacion);	
		return estimacion;
	}
	

	
	@Override
	public EstimacionCoberturaReporteCabina selectEstimacionCoberturaReporteCabina(EstimacionCoberturaSiniestroDTO dto, String tipoEstimacion){
		return this.selectEstimacionCoberturaReporteCabina(dto, tipoEstimacion, null);
	}
	
	private EstimacionCoberturaReporteCabina selectEstimacionCoberturaReporteCabina(
			EstimacionCoberturaSiniestroDTO dto, String tipoEstimacion, 
			CoberturaReporteCabina coberturaReporteCabina){
		EstimacionCoberturaReporteCabina estimacion = null;
		if(EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)){
			estimacion = dto.getEstimacionBienes();
		}else if(EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion)){
			estimacion = dto.getEstimacionGenerica();
		}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)){
			estimacion = dto.getEstimacionGastosMedicos();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)){
			estimacion = dto.getEstimacionPersonas();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, tipoEstimacion)){
			estimacion = dto.getEstimacionViajero();
		}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, tipoEstimacion)){
			estimacion = dto.getEstimacionGastosMedicosConductor();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, tipoEstimacion)){
			
			if(dto.getEstimacionVehiculos()!=null 
					&& dto.getEstimacionVehiculos().getTipoTransporte()!=null
					&& TipoPaseAtencion.SIPAC.getValue().equals(dto.getEstimacionVehiculos().getTipoPaseAtencion())){
				CatValorFijo tipoTransporte = catalogoGrupoValorService.obtenerValorPorCodigo(
						TIPO_CATALOGO.TIPO_TRANSPORTE, dto.getEstimacionVehiculos().getTipoTransporte());
				if(tipoTransporte!=null && tipoTransporte.getCodigoPadre()!=null){
					CatValorFijo tipoVehiculo = catalogoGrupoValorService.obtenerValorPorCodigo(
							TIPO_CATALOGO.TIPO_VEHICULO, tipoTransporte.getCodigoPadre());
					if(tipoVehiculo!=null && tipoVehiculo.getCodigoPadre()!=null){
						if(dto.getDatosEstimacion()==null){
							dto.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
						}
						dto.getDatosEstimacion().setEstimacionNueva(new BigDecimal(tipoVehiculo.getCodigoPadre()));
					}
				}
			}	
			estimacion = dto.getEstimacionVehiculos();
		}else if(EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, tipoEstimacion)){
			
			if(coberturaReporteCabina!=null){
				
				String terminoAjuste = coberturaReporteCabina.getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoAjuste();
				
				if((TerminoAjuste.SE_RECIBIO_SIPAC.getValue().equals(terminoAjuste) 
						|| TerminoAjuste.SE_RECIBIO_SIPAC_REEMBOLSOCIA.getValue().equals(terminoAjuste))
						&& dto.getEstimacionDanosMateriales().getTipoTransporte()!=null){
					CatValorFijo tipoTransporte = catalogoGrupoValorService.obtenerValorPorCodigo(
							TIPO_CATALOGO.TIPO_TRANSPORTE, dto.getEstimacionDanosMateriales().getTipoTransporte());
					if(tipoTransporte!=null && tipoTransporte.getCodigoPadre()!=null){
						CatValorFijo tipoVehiculo = catalogoGrupoValorService.obtenerValorPorCodigo(
								TIPO_CATALOGO.TIPO_VEHICULO, tipoTransporte.getCodigoPadre());
						if(tipoVehiculo!=null && tipoVehiculo.getCodigoPadre()!=null){
							dto.setMontoRecuperacionSipac(new BigDecimal(tipoVehiculo.getCodigoPadre()));
						}
					}
				}
			}
			
			estimacion = dto.getEstimacionDanosMateriales();
		} 
		estimacion.setAplicaDeducible(dto.getAplicaDeducible()); //Fix deducibles
		return estimacion;
	}
	
	/**
	 * 
	 * Método que obtiene el listado de coberturas que corresponden a un Reporte de Cabina.
	 * @param reporteCabinaId
	 * @return
	 */
	
	 @Override
		public List<AfectacionCoberturaSiniestroDTO> obtenerCoberturasAfectacion(Long reporteCabinaId) {
		   return obtenerCoberturasAfectacion(reporteCabinaId,null, null, null);
		}

	/**
	 * 
	 * Método que obtiene el listado de coberturas que corresponden a la configuración seleccionada de Tipo Siniestro, Termino Ajuste y Tipo de Responsabilidad en la pantalla de afectación del inciso.
	 * @param reporteCabinaId
	 * @param tipoSiniestro
	 * @param terminoAjuste
	 * @param tipoResponsabilidad
	 * @return
	 */
	@Override
	public List<AfectacionCoberturaSiniestroDTO> obtenerCoberturasAfectacion(Long reporteCabinaId, String tipoSiniestro, String terminoAjuste, String tipoResponsabilidad) {
		return obtenerCoberturasAfectacion( reporteCabinaId,  tipoSiniestro,  terminoAjuste, tipoResponsabilidad,  false, false);
	}
	
	/**
	 * 
	 * Método que obtiene el listado de coberturas que corresponden a la configuración seleccionada de Tipo Siniestro, Termino Ajuste y Tipo de Responsabilidad en la pantalla de afectación del inciso.
	 * @param reporteCabinaId
	 * @param tipoSiniestro
	 * @param terminoAjuste
	 * @param tipoResponsabilidad
	 * @param soloCoberturasAfectadas Solo regresa las coberturas que fueron afectadas en el Siniestro
	 * @return
	 */
	@Override
	public List<AfectacionCoberturaSiniestroDTO> obtenerCoberturasAfectacion(Long reporteCabinaId, String tipoSiniestro, 
			String terminoAjuste,String tipoResponsabilidad, Boolean soloAfectadas, Boolean soloAfectables) {
		List<AfectacionCoberturaSiniestroDTO> afectCobSiniestroList =  new ArrayList<AfectacionCoberturaSiniestroDTO>();
		List<CoberturaReporteCabina> coberturasReporteCabinaList = this.obtenerCoberturasReporte(reporteCabinaId);		
		if(reporteCabinaId!=null && StringUtil.isEmpty(tipoSiniestro) && StringUtil.isEmpty(terminoAjuste) && StringUtil.isEmpty(tipoResponsabilidad) ){
			ReporteCabina reporteCabina = this.entidadService.findById(ReporteCabina.class, reporteCabinaId);
			if(reporteCabina.getSeccionReporteCabina() != null){
				AutoIncisoReporteCabina autoInciso =  reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
				tipoSiniestro = autoInciso.getCausaSiniestro();
				terminoAjuste = autoInciso.getTerminoAjuste();
				tipoResponsabilidad = autoInciso.getTipoResponsabilidad();
			}
		}
		
		IncisoSiniestroDTO incisoSiniestroDTO = null;
		if(reporteCabinaId != null){
			incisoSiniestroDTO = polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(reporteCabinaId);
		}
		
		for(CoberturaReporteCabina coberturaReporteCabina : coberturasReporteCabinaList){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put( "cveTipoCalculoCobertura", coberturaReporteCabina.getClaveTipoCalculo());
			params.put( "tipoConfiguracion", coberturaReporteCabina.getCoberturaDTO().getTipoConfiguracion());					
			List<ConfiguracionCalculoCoberturaSiniestro> confCalculoCoberturaList = entidadService.findByProperties( ConfiguracionCalculoCoberturaSiniestro.class, params );
			for (ConfiguracionCalculoCoberturaSiniestro configuracion: confCalculoCoberturaList) {
				
				if (!soloAfectables || (soloAfectables && 
						!coberturaReporteCabina.getCoberturaDTO().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_AMPARADA) )) {
					AfectacionCoberturaSiniestroDTO dto = llenarConfiguracionCobertura(configuracion, coberturaReporteCabina,
							tipoSiniestro, terminoAjuste, tipoResponsabilidad, incisoSiniestroDTO);
					if(soloAfectadas){
						
						params = new HashMap<String, Object>();
						params.put("coberturaReporteCabina.id", coberturaReporteCabina.getId());
						
						String claveSubCalculo = dto.getConfiguracionCalculoCobertura().getCveSubTipoCalculoCobertura();
						if (!StringUtil.isEmpty(claveSubCalculo)) {
							params.put("cveSubCalculo", claveSubCalculo);
						}
						
						List<EstimacionCoberturaReporteCabina> estimaciones = 
											entidadService.findByProperties(EstimacionCoberturaReporteCabina.class, params);
						
						if (estimaciones != null && !estimaciones.isEmpty()) {
							afectCobSiniestroList.add(dto);
						}
						
					}else{
						afectCobSiniestroList.add(dto);
					}
				}
			}					
		}
		Collections.sort(afectCobSiniestroList, new Comparator<AfectacionCoberturaSiniestroDTO>() {
            @Override
            public int compare(AfectacionCoberturaSiniestroDTO n1,
            		AfectacionCoberturaSiniestroDTO n2) {
                  return n1.getCoberturaReporteCabina().getCoberturaDTO().getNumeroSecuencia()
                              .compareTo(
                            		  n2.getCoberturaReporteCabina().getCoberturaDTO().getNumeroSecuencia());
            }
      }); 
		return afectCobSiniestroList;
	}

	
	private AfectacionCoberturaSiniestroDTO llenarConfiguracionCobertura(ConfiguracionCalculoCoberturaSiniestro configuracionCalculo,
			CoberturaReporteCabina coberturaReporteCabina, String tipoSiniestro, String terminoAjuste,String tipoResponsabilidad, 
			IncisoSiniestroDTO incisoSiniestroDTO){
		
		Map<String,Object> params = new HashMap<String,Object>();
		AfectacionCoberturaSiniestroDTO dto = new AfectacionCoberturaSiniestroDTO();
		
		params.put("tipoSiniestro", tipoSiniestro);
		params.put("terminoAjuste", terminoAjuste);
		params.put("responsabilidad", tipoResponsabilidad);
		params.put("cveTipoCalculoCobertura", configuracionCalculo.getCveTipoCalculoCobertura());
		params.put("cveSubTipoCalculoCobertura", configuracionCalculo.getCveSubTipoCalculoCobertura());
		params.put("tipoConfiguracion", coberturaReporteCabina.getCoberturaDTO().getTipoConfiguracion());
		
		List<ConfiguracionCoberturaSiniestro> matrizCoberturas = entidadService.findByProperties(ConfiguracionCoberturaSiniestro.class, params);
		
		if (matrizCoberturas != null && !matrizCoberturas.isEmpty()) {
			ConfiguracionCoberturaSiniestro configCobertura = matrizCoberturas.get(0);
			dto.setConfiguracionCoberturaSiniestro(configCobertura);
			dto.setEsSiniestrable(true);
		}		
		dto.setCoberturaReporteCabina(coberturaReporteCabina);		
		dto.setConfiguracionCalculoCobertura(configuracionCalculo);
		final String nombreComercial = coberturaReporteCabina.getCoberturaDTO().getNombreComercial();
		final String nombreCobertura = configuracionCalculo.getNombreCobertura();
		dto.setNombreCobertura((nombreCobertura !=null)?nombreCobertura:nombreComercial);
		dto.setMontoRechazo( this.obtenerMontoRechazoCobertura(dto.getCoberturaReporteCabina(), dto.getConfiguracionCalculoCobertura()));
		String cveFuenteSumaAsegurada = coberturaReporteCabina.getCoberturaDTO().getClaveFuenteSumaAsegurada();
		String valorSumaEstimada = configuracionCalculo.getValorSumaEstimada();
		
		EstiloVehiculoDTO estilo = entidadService.findById(EstiloVehiculoDTO.class, 
				getEstiloVehiculoId(coberturaReporteCabina.getIncisoReporteCabina().getAutoIncisoReporteCabina().getEstiloId()));
		
		
		if (cveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO) && StringUtils.isNotEmpty(valorSumaEstimada) 
				&& !valorSumaEstimada.equals(COBERTURA_INDEPENDIENTE) ){
			
			BigDecimal historico = BigDecimal.ZERO;
			BigDecimal montoEnCobertura = BigDecimal.ZERO;
			
			if (coberturaReporteCabina.getCoberturaDTO().getReinstalable() == null 
					|| (coberturaReporteCabina.getCoberturaDTO().getReinstalable() != null 
					&& coberturaReporteCabina.getCoberturaDTO().getReinstalable().equals(SUMA_ASEG_NO_REINSTALABLE))) {
				
				historico = movimientoSiniestroDao.obtenerMontoSiniestrosAnteriores(coberturaReporteCabina.getId());				
			} else if (coberturaReporteCabina.getCoberturaDTO().getReinstalable() != null && coberturaReporteCabina.getCoberturaDTO().getReinstalable().equals(SUMA_ASEG_REINSTALABLE)
					&& coberturaReporteCabina.getCoberturaDTO().getClaveTipoSumaAsegurada().equals(CoberturaDTO.CLAVE_TIPO_SUMA_ASEGURADA_LUC)) {
				montoEnCobertura = movimientoSiniestroDao.obtenerMontoCoberturaReporteCabina(coberturaReporteCabina.getId(), null, null);
			}			
			
			BigDecimal sumaAseguradaAmparada = BigDecimal.valueOf(coberturaReporteCabina.getValorSumaAsegurada());
			
			if (valorSumaEstimada.equals(SUMA_ASEG_X_PASAJERO)) {		
				
				if (estilo != null && estilo.getNumeroAsientos() != null) {
					
					sumaAseguradaAmparada = sumaAseguradaAmparada.multiply(new BigDecimal(estilo.getNumeroAsientos()));
					dto.setMontoSumaAsegurada(sumaAseguradaAmparada);
				} else {
					dto.setMontoSumaAsegurada(sumaAseguradaAmparada);					
				}
				
			} 
			
			dto.setSumaAseguradaEstimada(sumaAseguradaAmparada.subtract(montoEnCobertura).subtract(historico));
		}
		if(!cveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_AMPARADA)){
			
			BigDecimal montoEstimado = movimientoSiniestroDao.obtenerMontoCoberturaReporteCabina(coberturaReporteCabina.getId(), null, configuracionCalculo.getCveSubTipoCalculoCobertura());

			dto.setMontoEstimado(montoEstimado);
			
			params.clear();
			params.put("coberturaReporteCabina.id", coberturaReporteCabina.getId());
			params.put("cveSubCalculo", configuracionCalculo.getCveSubTipoCalculoCobertura());
			List<EstimacionCoberturaReporteCabina> estimaciones = entidadService.findByProperties(EstimacionCoberturaReporteCabina.class, params);
			for (EstimacionCoberturaReporteCabina estimacion: estimaciones) {
				if (estimacion.getTipoPaseAtencion() == null || !EnumUtil.equalsValue(TipoPaseAtencion.SOLO_REGISTRO, estimacion.getTipoPaseAtencion())) {
					dto.setSiniestrado(true);
				}
			}
		}
		if(EnumUtil.equalsValue(ClaveTipoCalculo.RESP_CIVIL_VIAJERO, configuracionCalculo.getCveTipoCalculoCobertura())){
			if (coberturaReporteCabina.getDiasSalarioMinimo() != null) {
				BigDecimal sumaAseguradaViajero = this.calculoSumaAseguradaDSMVGDF(coberturaReporteCabina.getDiasSalarioMinimo());
				dto.setMontoSumaAsegurada(sumaAseguradaViajero);
			}

		} 
		
		//Se comenta obtener datos de riesgo ya que no se ocupan al momento de listar coberturas. Se debe verificar que no afecta a otra funcionalidad
		/*List<ControlDinamicoRiesgoDTO> controles = polizaSiniestroService.obtenerDatosRiesgo(coberturaReporteCabina.
				getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId(), coberturaReporteCabina.getCoberturaDTO().getIdToCobertura().longValue());
		
		if (controles != null && controles.size() > 0) {
			dto.setTieneDatosRiesgo((short) 1);
		}*/
		
		//Polizas canceladas no son siniestrables
		if(!StringUtil.isEmpty(coberturaReporteCabina.getIncisoReporteCabina().getEstatusInciso()) 
				&& coberturaReporteCabina.getIncisoReporteCabina().getEstatusInciso().equals(
						IncisoReporteCabina.EstatusVigenciaInciso.CANCELADO.toString())
				&& !incisoSiniestroDTO.getMotivo().equals(PolizaSiniestroServiceImpl.MOTIVO_FALTA_PAGO)){
			dto.setEsSiniestrable(Boolean.FALSE);			
		}
		
		dto.setMontoDeducible(recuperacionDeducibleService.obtenerDeduciblePorCobertura(
				coberturaReporteCabina.getId(), configuracionCalculo.getCveSubTipoCalculoCobertura()));
		
		return dto;
	}
	
	/**
	 * Obtiene el monto de rechazo de la cobertura, se agrega el monto debido a que solo hay una CoberturaReporteCabina para la Cobertura de RC
	 * @param coberturaReporteCabina
	 * @param configuracionCoberturaSiniestro
	 * @return
	 */
	private BigDecimal obtenerMontoRechazoCobertura(CoberturaReporteCabina coberturaReporteCabina, ConfiguracionCalculoCoberturaSiniestro configuracionCoberturaSiniestro){
		BigDecimal montoRechazo = BigDecimal.ZERO;
		if(coberturaReporteCabina != null
				&& coberturaReporteCabina.getClaveTipoCalculo() != null){
			if( coberturaReporteCabina.getClaveTipoCalculo().equals(CoberturaReporteCabina.ClaveTipoCalculo.RESPONSABILIDAD_CIVIL.toString()) ){
				if(configuracionCoberturaSiniestro != null
						&& coberturaReporteCabina.getMontoRechazo() != null){
					if(!StringUtil.isEmpty(configuracionCoberturaSiniestro.getCveSubTipoCalculoCobertura())){
						String[] montosRC = coberturaReporteCabina.getMontoRechazo().split("\\|");
							for(String montoRC : montosRC){
								if(montoRC.contains(configuracionCoberturaSiniestro.getCveSubTipoCalculoCobertura())){
									montoRechazo = new BigDecimal(montoRC.split(":")[1]);
									return montoRechazo;
								}}
					}else{
						montoRechazo = (coberturaReporteCabina.getMontoRechazo() != null)? new BigDecimal( coberturaReporteCabina.getMontoRechazo()) : BigDecimal.ZERO;
					}}
			}else{
				montoRechazo = (coberturaReporteCabina.getMontoRechazo() != null)? new BigDecimal( coberturaReporteCabina.getMontoRechazo()) : BigDecimal.ZERO;
			}
		}
		return montoRechazo;
	}


	/**
	 * 
	 * Método que regresa el listado de coberturas asociadas a un reporte de cabina. 
	 * @param idReporteCabina
	 * @return
	 */
	@Override
	public List<CoberturaReporteCabina> obtenerCoberturasReporte(Long reporteCabinaId) {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put( "incisoReporteCabina.seccionReporteCabina.reporteCabina.id", reporteCabinaId );
		List<CoberturaReporteCabina> coberturasReporteCabina = entidadService.findByProperties( CoberturaReporteCabina.class, params );
		return coberturasReporteCabina;
	}
	
	

	@Override
	public EstimacionCoberturaSiniestroDTO obtenerDatosEstimacionCobertura(Long idCoberturaRepCabina, String tipoCalculo, String tipoEstimacion) {
		
		BigDecimal importePagado = BigDecimal.ZERO; 
		BigDecimal montoFinalDeducible = BigDecimal.ZERO;
		
		EstimacionCoberturaSiniestroDTO                  dto = new EstimacionCoberturaSiniestroDTO();
		CoberturaReporteCabina                           coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaRepCabina);
		EstimacionCoberturaReporteCabina                 estimacion   = this.seleccionaObjetoPorTipoEstimacion(tipoEstimacion);
		List<? extends EstimacionCoberturaReporteCabina> estimaciones = this.entidadService.findByProperty(estimacion.getClass(),"coberturaReporteCabina.id", idCoberturaRepCabina);
		DatosEstimacionCoberturaDTO                      datos = new DatosEstimacionCoberturaDTO();
		
		Long                                             idReporteCabina = coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
		AutoIncisoReporteCabina                          autoInciso = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(idReporteCabina);
		DatosReporteSiniestroDTO                         datosReporteSiniestro = this.reporteCabinaService.obtenerDatosReporte(idReporteCabina);

		Boolean siniestrado = estimaciones != null && !estimaciones.isEmpty() ? true : false;
		Boolean consultaSumaAsegurada = false;
	
		if (!siniestrado){
			if ((coberturaReporteCabina.getCoberturaDTO().getNombreComercial().toUpperCase().equals(CoberturaDTO.NOMBRE_DM) ||
					coberturaReporteCabina.getCoberturaDTO().getNombreComercial().toUpperCase().equals(CoberturaDTO.NOMBRE_RT) ||
					coberturaReporteCabina.getCoberturaDTO().getNombreComercial().toUpperCase().equals(CoberturaDTO.NOMBRE_DM_CONVENIDO) ||
					coberturaReporteCabina.getCoberturaDTO().getNombreComercial().toUpperCase().equals(CoberturaDTO.NOMBRE_RT_CONVENIDO)) &&
					CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO.equals(coberturaReporteCabina.getCoberturaDTO().getClaveFuenteSumaAsegurada())) {
				
				Double valorSumaAsegurada = obtieneValorSumaAsegurada(coberturaReporteCabina, autoInciso);
				if (valorSumaAsegurada != null) {
					coberturaReporteCabina.setValorSumaAsegurada(valorSumaAsegurada);
					consultaSumaAsegurada = true;
				}
			}
		}		
	
		BigDecimal sumaAseguradaAmparada = BigDecimal.valueOf(coberturaReporteCabina.getValorSumaAsegurada());
		datos.setSumaAseguradaAmparada(sumaAseguradaAmparada);
		dto.setDatosReporte           (datosReporteSiniestro);
		dto.setCobertura              (coberturaReporteCabina.getCoberturaDTO());
		dto.setNombreCobertura        ( coberturaReporteCabina.getCoberturaDTO().getNombreComercial() );
		dto.setReporteCabina          (coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina());

		if(estimaciones.size()>0){
			estimacion = estimaciones.get(0);
			ConfiguracionCalculoCoberturaSiniestro configuracion = obtenerConfiguracionCalcCobertura(coberturaReporteCabina.getClaveTipoCalculo(), 
					null,  estimacion.getTipoEstimacion(),
					coberturaReporteCabina.getCoberturaDTO().getTipoConfiguracion());

			dto.setConfiguracionCalculoCoberturaSiniestro(configuracion);
			dto.setEstimacionCoberturaReporte(estimacion);
			dto.setDatosEstimacion(datos);
			
			importePagado = movimientoSiniestroService.obtenerMontoPagosAfectacion(estimacion.getId());
			BigDecimal monto = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(estimacion.getId());
			datos.setEstimacionActual(monto);
			datos.setSumaAseguradaAmparada(sumaAseguradaAmparada);
			datos.setReserva( movimientoSiniestroService.obtenerReservaAfectacion(estimacion.getId(), Boolean.TRUE));
			coberturaReporteCabina.getIncisoReporteCabina().setAutoIncisoReporteCabina(autoInciso);
			estimacion.setCoberturaReporteCabina(coberturaReporteCabina);
			estimacion.setConsultaSumaAsegurada(consultaSumaAsegurada);
			this.colocaEstimacionPorTipoEstimacion(dto, estimacion, tipoEstimacion);
		}else{
			datos.setEstimacionActual(BigDecimal.ZERO);
	     	datos.setEstimacionNueva(BigDecimal.ZERO);
			coberturaReporteCabina.getIncisoReporteCabina().setAutoIncisoReporteCabina(autoInciso);
			estimacion.setCoberturaReporteCabina(coberturaReporteCabina);
			estimacion.setConsultaSumaAsegurada(consultaSumaAsegurada);
			this.colocaEstimacionPorTipoEstimacion(dto, estimacion, tipoEstimacion);
			datos.setReserva(BigDecimal.ZERO);
			if (EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, tipoEstimacion)){
				datos.setCausaMovimiento(MovimientoCoberturaSiniestro.CausaMovimiento.APERTURA_RESERVA.toString());
			}
		}
		
		setInfoReferenciaValorComercial(autoInciso,datos,coberturaReporteCabina, tipoEstimacion);
		cargaInformacionAutoInciso(dto, autoInciso);
		datos.setImportePagado(importePagado);
		dto.setDatosEstimacion(datos);
		
		montoFinalDeducible = recuperacionDeducibleService.obtenerDeduciblePorEstimacion(estimacion.getId());
		dto.setMontoDeducible(montoFinalDeducible);
		dto.setMontoDeducibleCalculado(montoFinalDeducible);
		dto.setMotivoNoAplicaDeducible(estimacion.getMotivoNoAplicaDeducible());
		dto.setTipoCoberturaAutoInciso(coberturaReporteCabina.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCausaSiniestro());
		dto.setAplicaDeducible(estimacion.getAplicaDeducible());
		dto.setRecibidaOrdenCia(autoInciso.getRecibidaOrdenCia());
		dto.setNumeroSiniestroCia(autoInciso.getSiniestroCia());
		dto.setResponsabilidad(autoInciso.getTipoResponsabilidad());
		
		if (!StringUtil.isEmpty(autoInciso.getCiaSeguros())) {
			PrestadorServicio compania = entidadService.findById(PrestadorServicio.class, Integer.valueOf(autoInciso.getCiaSeguros()));
			dto.setCompaniaOrdenId(Integer.parseInt(autoInciso.getCiaSeguros()));	
			dto.setNombreCompania(compania.getNombrePersona());
		}
		return dto;
	}
	
	
	
	@Override
	public EstimacionCoberturaSiniestroDTO obtenerDatosEstimacionCoberturaNueva(Long idCoberturaRepCabina, String tipoEstimacion) {
		EstimacionCoberturaSiniestroDTO dto = new EstimacionCoberturaSiniestroDTO();
		CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaRepCabina);
		EstimacionCoberturaReporteCabina estimacion   = this.seleccionaObjetoPorTipoEstimacion(tipoEstimacion);
		BigDecimal sumaAseguradaAmparada = BigDecimal.valueOf(coberturaReporteCabina.getValorSumaAsegurada());
		DatosEstimacionCoberturaDTO datos = new DatosEstimacionCoberturaDTO();
		datos.setSumaAseguradaAmparada(sumaAseguradaAmparada);
		Long idReporteCabina = coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
		dto.setReporteCabina(coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina());
		AutoIncisoReporteCabina autoInciso = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(idReporteCabina);
		DatosReporteSiniestroDTO datosReporteSiniestro = this.reporteCabinaService.obtenerDatosReporte(idReporteCabina);
		dto.setDatosReporte(datosReporteSiniestro);
		dto.setCobertura(coberturaReporteCabina.getCoberturaDTO());
		dto.setNombreCobertura( coberturaReporteCabina.getCoberturaDTO().getNombreComercial() );
		BigDecimal importePagado = BigDecimal.ZERO; 
		datos.setEstimacionActual(BigDecimal.ZERO);
	    datos.setEstimacionNueva(BigDecimal.ZERO);
		coberturaReporteCabina.getIncisoReporteCabina().setAutoIncisoReporteCabina(autoInciso);
		estimacion.setCoberturaReporteCabina(coberturaReporteCabina);
		dto.setNumeroSiniestroCia(autoInciso.getSiniestroCia());
		this.colocaEstimacionPorTipoEstimacion(dto, estimacion, tipoEstimacion);
		datos.setReserva(BigDecimal.ZERO);
		cargaInformacionAutoInciso(dto, autoInciso);
		datos.setImportePagado(importePagado);
		dto.setDatosEstimacion(datos);
		dto.setTipoCoberturaAutoInciso(autoInciso.getCausaSiniestro());
		dto.setRecibidaOrdenCia(autoInciso.getRecibidaOrdenCia());
		dto.setResponsabilidad(autoInciso.getTipoResponsabilidad());
		
		if (!StringUtil.isEmpty(autoInciso.getCiaSeguros())) {
			PrestadorServicio compania = entidadService.findById(PrestadorServicio.class, Integer.valueOf(autoInciso.getCiaSeguros()));
			dto.setCompaniaOrdenId(Integer.parseInt(autoInciso.getCiaSeguros()));	
			dto.setNombreCompania(compania.getNombrePersona());
		}
	  
		
		return dto;
	}
	
	
	@Override
	public EstimacionCoberturaSiniestroDTO obtenerDatoEstimacionCobertura(EstimacionCoberturaReporteCabina estimacion) {
		BigDecimal montoFinalDeducible = BigDecimal.ZERO;
		EstimacionCoberturaSiniestroDTO dto = new EstimacionCoberturaSiniestroDTO();
		CoberturaReporteCabina coberturaReporteCabina = estimacion.getCoberturaReporteCabina();
		BigDecimal sumaAseguradaAmparada = BigDecimal.valueOf(coberturaReporteCabina.getValorSumaAsegurada());
		DatosEstimacionCoberturaDTO datos = new DatosEstimacionCoberturaDTO();
		datos.setSumaAseguradaAmparada(sumaAseguradaAmparada);
		Long idReporteCabina = coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
		AutoIncisoReporteCabina autoInciso = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(idReporteCabina);
		DatosReporteSiniestroDTO datosReporteSiniestro = this.reporteCabinaService.obtenerDatosReporte(idReporteCabina);
		dto.setDatosReporte(datosReporteSiniestro);
		dto.setCobertura(coberturaReporteCabina.getCoberturaDTO());
		dto.setNombreCobertura( coberturaReporteCabina.getCoberturaDTO().getNombreComercial() );
		dto.setResponsabilidad(autoInciso.getTipoResponsabilidad());
		BigDecimal importePagado = BigDecimal.ZERO; 
		
		ConfiguracionCalculoCoberturaSiniestro configuracion = obtenerConfiguracionCalcCobertura(coberturaReporteCabina.getClaveTipoCalculo(), 
				null,  estimacion.getTipoEstimacion(),
				coberturaReporteCabina.getCoberturaDTO().getTipoConfiguracion());
		dto.setConfiguracionCalculoCoberturaSiniestro(configuracion);
			
		dto.setDatosEstimacion(datos);
		importePagado = movimientoSiniestroService.obtenerMontoPagosAfectacion(estimacion.getId());
		BigDecimal monto = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(estimacion.getId());
		datos.setEstimacionActual(monto);
		datos.setSumaAseguradaAmparada(sumaAseguradaAmparada);
		BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(estimacion.getId(), Boolean.TRUE);
		datos.setReserva(reserva);
		coberturaReporteCabina.getIncisoReporteCabina().setAutoIncisoReporteCabina(autoInciso);
		estimacion.setCoberturaReporteCabina(coberturaReporteCabina);
		dto.setEstimacionCoberturaReporte(estimacion);
		this.colocaEstimacionPorTipoEstimacion(dto, estimacion, estimacion.getTipoEstimacion());
		cargaInformacionAutoInciso(dto, autoInciso);
		setInfoReferenciaValorComercial(autoInciso,datos,coberturaReporteCabina, estimacion.getTipoEstimacion());
		datos.setImportePagado(importePagado);
//		MovimientoCoberturaSiniestro ultimoMovimiento = this.obtenerUltimoMovimientoPorEstimacion(estimacion);
//		datos.setCausaMovimiento(ultimoMovimiento.getCausaMovimiento());
		dto.setDatosEstimacion(datos);
		
		montoFinalDeducible = this.obtenerMontoFinalDeducible(estimacion.getId());
		dto.setMontoDeducible(montoFinalDeducible);
		dto.setMontoDeducibleCalculado(montoFinalDeducible);
		dto.setMotivoNoAplicaDeducible(estimacion.getMotivoNoAplicaDeducible());
		dto.setAplicaDeducible(estimacion.getAplicaDeducible());
		dto.setRecibidaOrdenCia(autoInciso.getRecibidaOrdenCia());
		dto.setNumeroSiniestroCia(autoInciso.getSiniestroCia());
		if (!StringUtil.isEmpty(autoInciso.getCiaSeguros())) {
			PrestadorServicio compania = entidadService.findById(PrestadorServicio.class, Integer.valueOf(autoInciso.getCiaSeguros()));
			dto.setCompaniaOrdenId(Integer.parseInt(autoInciso.getCiaSeguros()));	
			dto.setNombreCompania(compania.getNombrePersona());
		}
		return dto;
	}
	
	
	private BigDecimal obtenerMontoFinalDeducible(Long keyEstimacion){
		BigDecimal montoFinalDeducible = BigDecimal.ZERO;
		
		List<RecuperacionDeducible> lRecuperacionDeducible = this.recuperacionDeducibleService.obtenerRecuperacionDeduciblePorEstimacion(keyEstimacion);
		
		if( !lRecuperacionDeducible.isEmpty() ){
			montoFinalDeducible = lRecuperacionDeducible.get(0).getMontoFinalDeducible();
		}
		
		return montoFinalDeducible;
	}
	
	
	public EstimacionCoberturaReporteCabina seleccionaObjetoPorTipoEstimacion( String tipoEstimacion){
		EstimacionCoberturaReporteCabina estimacion = null;
		if(EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)){
			estimacion = new TerceroRCBienes();
		}else if(EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion)){
			estimacion = new EstimacionGenerica();
		}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)){
			estimacion = new TerceroGastosMedicos();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)){
			estimacion = new TerceroRCPersonas();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, tipoEstimacion)){
			estimacion = new TerceroRCViajero();
		}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, tipoEstimacion)){
			estimacion = new TerceroGastosMedicosConductor();
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, tipoEstimacion)){
			estimacion = new TerceroRCVehiculo();
		}else if(EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, tipoEstimacion)){
			estimacion = new TerceroDanosMateriales();
		} 
		return estimacion;
	}
	
	
	private void colocaEstimacionPorTipoEstimacion( EstimacionCoberturaSiniestroDTO dto , EstimacionCoberturaReporteCabina estimacion, String tipoEstimacion){
		dto.setEstimacionCoberturaReporte(estimacion);
		if(EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)){
			dto.setEstimacionBienes((TerceroRCBienes) estimacion);
		}else if(EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion)){
			dto.setEstimacionGenerica((EstimacionGenerica) estimacion);
		}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)){
			dto.setEstimacionGastosMedicos((TerceroGastosMedicos) estimacion);
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)){
			dto.setEstimacionPersonas((TerceroRCPersonas) estimacion);
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, tipoEstimacion)){
			dto.setEstimacionViajero((TerceroRCViajero) estimacion);
		}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, tipoEstimacion)){
			dto.setEstimacionGastosMedicosConductor((TerceroGastosMedicosConductor) estimacion);
		}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, tipoEstimacion)){
			dto.setEstimacionVehiculos((TerceroRCVehiculo) estimacion);
			CatValorFijo color = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.COLOR, dto.getEstimacionVehiculos().getColor());
			if (color != null) {
				dto.getEstimacionVehiculos().setDescColor(color.getDescripcion());
			}			
			dto.getEstimacionVehiculos().setInvolucradoEnSiniestros(tieneNumeroDeSerieSiniestrosAnteriores(estimacion.getId(), dto.getEstimacionVehiculos().getNumeroSerie()));

		}else if(EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, tipoEstimacion)){
			dto.setEstimacionDanosMateriales((TerceroDanosMateriales) estimacion);
		} 
	}
	
	@Override
	public Boolean tieneNumeroDeSerieSiniestrosAnteriores(Long estimacionId, String noSerie){
		List<EstimacionCoberturaReporteCabina> pasesAnteriores = this.estimacionCoberturaDao.pasesRCVehiculoAnteriores(estimacionId, noSerie);
		return ((pasesAnteriores == null || pasesAnteriores.size()==0)? Boolean.FALSE: Boolean.TRUE);
	}

	/**
	 * Método que obtiene el listado de Pases de Atención de una Cobertura.
	 * @param idCoberturaReporteCabina
	 * @return
	 */
	@Override
	public List<PaseAtencionSiniestroDTO> obtenerListadoPases(Long idCoberturaReporteCabina, String tipoEstimacion) {
		List<PaseAtencionSiniestroDTO> pasesDeAtencionDTO = new ArrayList<PaseAtencionSiniestroDTO>();
		Map<String,Object> params = new HashMap<String,Object>();
		params.put( "coberturaReporteCabina.id", idCoberturaReporteCabina );
		
		if(!StringUtil.isEmpty(tipoEstimacion)) {
			params.put( "tipoEstimacion", tipoEstimacion );
		}
		List<EstimacionCoberturaReporteCabina>  estimacionCoberturaList = this.entidadService.findByProperties(EstimacionCoberturaReporteCabina.class,params );
		CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);

		Map<String,String> estatusEstimacion = this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTIMACION_ESTATUS);
		Map<String,String> pasesAtencion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.PASES_DE__ATENCION);
		
		for (EstimacionCoberturaReporteCabina estimacion : estimacionCoberturaList) {
			PaseAtencionSiniestroDTO dto = new PaseAtencionSiniestroDTO();
			dto.setEstimacionCoberturaReporte(estimacion);
			params.clear();
			
			String nombreCobertura = obtenerNombreCobertura(coberturaReporteCabina.getCoberturaDTO(),
					coberturaReporteCabina.getClaveTipoCalculo(), 
					null,  estimacion.getTipoEstimacion(),
					coberturaReporteCabina.getCoberturaDTO().getTipoConfiguracion());
			dto.setNombreCobertura(nombreCobertura);

			
			BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(estimacion.getId(), Boolean.TRUE);
			dto.setMontoReserva(reserva);
			
			dto.setEstimacionTipoPaseDeAtencion(pasesAtencion.get(estimacion.getTipoPaseAtencion()));
			
			//TODO Pendiente settear al DTO ya que este listo y preguntar si va el folio o la descripcion del listado service

			String tipoCalculoCobertura = coberturaReporteCabina.getClaveTipoCalculo();
			
			if(EnumUtil.equalsValue(tipoCalculoCobertura, ClaveTipoCalculo.ROBO_TOTAL, ClaveTipoCalculo.DANIOS_MATERIALES)) {
				Long incisoReporteCabinaId = coberturaReporteCabina.getIncisoReporteCabina().getId();
				List<AutoIncisoReporteCabina> autoIncisos = this.entidadService.findByProperty(AutoIncisoReporteCabina.class, "incisoReporteCabina.id",incisoReporteCabinaId);
				if (autoIncisos != null && !autoIncisos.isEmpty()) {
					String descripcionVehiculo = autoIncisos.get(0).getDescripcionFinal();
					dto.setDescripcionVehiculo(descripcionVehiculo);
					dto.setTipoPaseAtencionDescripcion("Taller "+(estimacion.getSecuenciaPaseAtencion() != null?estimacion.getSecuenciaPaseAtencion():""));
				}				
				
			} else if(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())){
				TerceroRCVehiculo pase = this.entidadService.findById(TerceroRCVehiculo.class, estimacion.getId());
				
				if (pase != null) {
					dto.setDescripcionVehiculo(pase.getEstiloVehiculo());
					dto.setTipoPaseAtencionDescripcion("Taller "+(estimacion.getSecuenciaPaseAtencion() != null?estimacion.getSecuenciaPaseAtencion():""));
				}
				
			} else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, estimacion.getTipoEstimacion())) {
				TerceroGastosMedicos pase = this.entidadService.findById(TerceroGastosMedicos.class, estimacion.getId());
				
				if (pase != null) {					
					if (pase.getHospital() != null || !StringUtil.isEmpty(pase.getOtroHospital())) {
						dto.setTipoPaseAtencionDescripcion("Hospital");
					} else if (pase.getMedico() != null) {
						dto.setTipoPaseAtencionDescripcion("Medico");
					}					
				}
			
				 
			} else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, estimacion.getTipoEstimacion())) {
				TerceroGastosMedicosConductor pase = this.entidadService.findById(TerceroGastosMedicosConductor.class, estimacion.getId());
				
				if (pase != null) {					
					if (pase.getHospital() != null || !StringUtil.isEmpty(pase.getOtroHospital())) {
						dto.setTipoPaseAtencionDescripcion("Hospital");
					} else if (pase.getMedico() != null) {
						dto.setTipoPaseAtencionDescripcion("Medico");
					}					
				}
			} else if(EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, estimacion.getTipoEstimacion())) {				
				TerceroRCPersonas pase = this.entidadService.findById(TerceroRCPersonas.class, estimacion.getId());
				
				if (pase != null) {					
					if (pase.getHospital() != null || !StringUtil.isEmpty(pase.getOtroHospital())) {
						dto.setTipoPaseAtencionDescripcion("Hospital");
					} else if (pase.getMedico() != null) {
						dto.setTipoPaseAtencionDescripcion("Medico");
					}					
				}
			} else if(EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, estimacion.getTipoEstimacion())) {				
				TerceroRCViajero pase = this.entidadService.findById(TerceroRCViajero.class, estimacion.getId());
				
				if (pase != null) {					
					if (pase.getHospital() != null || !StringUtil.isEmpty(pase.getOtroHospital())) {
						dto.setTipoPaseAtencionDescripcion("Hospital");
					} else if (pase.getMedico() != null) {
						dto.setTipoPaseAtencionDescripcion("Medico");
					}					
				}
			} else if(EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, estimacion.getTipoEstimacion()) ) {
				String tipoBien = ((TerceroRCBienes)estimacion).getTipoBien();
				Map<String,String> tiposBienMap = this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPOS_DE_BIEN);

				dto.setTipoPaseAtencionDescripcion(tiposBienMap.get(tipoBien) + ' ' + 
						(estimacion.getSecuenciaPaseAtencion() != null ? estimacion.getSecuenciaPaseAtencion( ): "")); 
			}

			
			BigDecimal importeDeducible = BigDecimal.ZERO;
			
			if (coberturaReporteCabina.getClaveTipoDeducible() != null && coberturaReporteCabina.getClaveTipoDeducible().compareTo((short) 0) != 0){
			
				switch(coberturaReporteCabina.getClaveTipoDeducible().intValue()){
				case TIPO_DEDUCIBLE_PORCENTAJE:
					String claveFuenteSumaAsegurada = coberturaReporteCabina.getCoberturaDTO().getClaveFuenteSumaAsegurada();
					if(claveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO)){
						BigDecimal porcentajeDeducible = BigDecimal.valueOf(coberturaReporteCabina.getPorcentajeDeducible());
						BigDecimal sumaAsegurada = BigDecimal.valueOf(coberturaReporteCabina.getValorSumaAsegurada());
						importeDeducible = porcentajeDeducible.multiply(sumaAsegurada);
					}else if (claveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_COMERCIAL) || 
							claveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA) || 
							claveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CONVENIDO)){
						BigDecimal porcentajeDeducible = BigDecimal.valueOf(coberturaReporteCabina.getPorcentajeDeducible());
						//TODO sumaAseguradaValorComercial de EstimacionCoberturaReporteCabina
						BigDecimal sumaAseguradaValorComercial = estimacion.getSumaAseguradaObtenida();
						importeDeducible = porcentajeDeducible.divide(new BigDecimal(100)).multiply( sumaAseguradaValorComercial); 
					}
					break;

				case TIPO_DEDUCIBLE_VALOR:
					importeDeducible = BigDecimal.valueOf(coberturaReporteCabina.getValorDeducible());
					break;

				case TIPO_DEDUCIBLE_DSMVGDF:
					importeDeducible = this.calculoDeducibleDSMVGDF(BigDecimal.valueOf(coberturaReporteCabina.getValorDeducible()));
					break;
				}

			}
			dto.setImporteDeducible(importeDeducible);
			if(dto.getEstimacionCoberturaReporte().getEstatus()!=null){
				dto.setEstatus(estatusEstimacion.get(dto.getEstimacionCoberturaReporte().getEstatus()));
			}
			pasesDeAtencionDTO.add(dto);
		}
		
		return pasesDeAtencionDTO;
	}
	
	
	@Override
	public List<String> validarEstimacionCobertura(EstimacionCoberturaSiniestroDTO dto, Long idCoberturaReporte,EstimacionCoberturaReporteCabina estimacion) {
		List<String> errorMessagesList = new ArrayList<String>();
		String tipoPaseAtencion = estimacion.getTipoPaseAtencion();
	
				
		//validar que no se pueda crear un pase de atencion con monto inicial cero
		if(estimacion.getId() == null && dto.getDatosEstimacion().getEstimacionNueva().compareTo(BigDecimal.ZERO)<= 0){
			errorMessagesList.add("No se puede crear un pase con Estimacion Nueva CERO");
		}
		
		//validar que se pueda cambiar el tipo de pase de atención
		if(estimacion.getId() != null){
			EstimacionCoberturaReporteCabina estimacionRegistrada = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacion.getId());
			if(estimacionRegistrada.getTipoPaseAtencion()!=null && !estimacionRegistrada.getTipoPaseAtencion().equals(tipoPaseAtencion)){
				String mensajeDeError = this.validaSiPuedeCambiarEstimacionASoloReporte(estimacion.getId());
				if(mensajeDeError!=null){
					errorMessagesList.add(" No se puede cambiar el tipo de pase. "+mensajeDeError);
				}
			}			
		}
		CoberturaDTO coberturaDTO = estimacion.getCoberturaReporteCabina().getCoberturaDTO();
		if(dto.getDatosEstimacion().getEstimacionNueva()== dto.getDatosEstimacion().getEstimacionActual()){
			errorMessagesList.add("El monto de la estimación debe ser diferente al monto de la estimacion actual");
		}
		
		if(EnumUtil.equalsValue(TipoEstimacion.DIRECTA, estimacion.getTipoEstimacion()) || 
				EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, estimacion.getTipoEstimacion())){
			if(dto.getCobertura().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_COMERCIAL) || 
					dto.getCobertura().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA)||
					dto.getCobertura().getClaveFuenteSumaAsegurada().equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CONVENIDO)) {
				if(dto.getDatosEstimacion().getEstimacionNueva().compareTo(estimacion.getSumaAseguradaObtenida()) >0){
					errorMessagesList.add("El monto de la estimación no debe sobrepasar el valor de la suma asegurada de la cobertura");
				}
			}else{
				if(dto.getDatosEstimacion().getEstimacionNueva().compareTo(dto.getDatosEstimacion().getSumaAseguradaAmparada())>0){
					errorMessagesList.add("El monto de la estimación no debe sobrepasar el valor de la suma asegurada de la cobertura");
				}
			}
		}
		if(estimacion.getId() != null){
			BigDecimal reservaEstimacion = movimientoSiniestroService.obtenerReservaAfectacion(estimacion.getId(), Boolean.TRUE);
			BigDecimal montoOc = dto.getDatosEstimacion().getEstimacionActual().subtract(reservaEstimacion);
			
			if (dto.getDatosEstimacion().getEstimacionNueva().compareTo(montoOc) < 0) {
				errorMessagesList.add("La reserva es menor a la diferencia del ajuste a la estimación");
			}
		}
		//String sumaAseguradaPaseAtencion = confCalculo.getSumaAseguradaPaseAtencion();
		String claveFuenteSumaAsegurada = coberturaDTO.getClaveFuenteSumaAsegurada();
		BigDecimal monto = BigDecimal.ZERO;
		BigDecimal montoEnCobertura = BigDecimal.ZERO;
		BigDecimal historicoMovimientos = BigDecimal.ZERO;
		
		if (coberturaDTO.getReinstalable() == null || 
				(coberturaDTO.getReinstalable() != null && coberturaDTO.getReinstalable().equals(SUMA_ASEG_NO_REINSTALABLE))) {
			historicoMovimientos = movimientoSiniestroDao.obtenerMontoSiniestrosAnteriores(idCoberturaReporte);			
		} else if (coberturaDTO.getReinstalable() != null && coberturaDTO.getReinstalable().equals(SUMA_ASEG_REINSTALABLE)
				&& coberturaDTO.getClaveTipoSumaAsegurada().equals(CoberturaDTO.CLAVE_TIPO_SUMA_ASEGURADA_LUC)) {
			montoEnCobertura = movimientoSiniestroDao.obtenerMontoCoberturaReporteCabina(idCoberturaReporte, estimacion.getId(), null);
		}
		
		monto = dto.getDatosEstimacion().getEstimacionNueva().add(montoEnCobertura).add(historicoMovimientos);
		
		
		/*if (claveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO)) {
			
			if (coberturaDTO.getReinstalable() != null && coberturaDTO.getReinstalable().equals(SUMA_ASEG_NO_REINSTALABLE)) {
				historicoMovimientos = movimientoSiniestroDao.obtenerMontoSiniestrosAnteriores(idCoberturaReporte);
			}
			
			monto = dto.getDatosEstimacion().getEstimacionNueva().add(montoEnCobertura).add(historicoMovimientos);
		} else {
			
			if (coberturaDTO.getReinstalable() != null && coberturaDTO.getReinstalable().equals(SUMA_ASEG_NO_REINSTALABLE)) {
				historicoMovimientos = movimientoSiniestroDao.obtenerMontoSiniestrosAnteriores(idCoberturaReporte);
			}
			monto = dto.getDatosEstimacion().getEstimacionNueva();
		}*/
		BigDecimal sumaAsegurada = BigDecimal.ZERO;
		
		

		if (claveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO)) {
			CoberturaReporteCabina coberturaSiniestroReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporte);
			sumaAsegurada = BigDecimal.valueOf(coberturaSiniestroReporteCabina.getValorSumaAsegurada());
		} else {
			sumaAsegurada = estimacion.getSumaAseguradaObtenida();
		}
		
		CoberturaReporteCabina coberturaSiniestroReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporte);
		
		Map<String, Object> paramss = new HashMap<String, Object>();
		paramss.put( "tipoEstimacion", estimacion.getTipoEstimacion());
		
		paramss.put( "cveTipoCalculoCobertura", coberturaSiniestroReporteCabina.getClaveTipoCalculo() );
		
		List<ConfiguracionCalculoCoberturaSiniestro> confCalculos =  this.entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, paramss);
		ConfiguracionCalculoCoberturaSiniestro confCalculo = null;

		if(confCalculos.size()>0){
			confCalculo = confCalculos.get(0);
		}

		if (StringUtils.isNotEmpty(confCalculo.getValorSumaEstimada()) && confCalculo.getValorSumaEstimada().equals(SUMA_ASEG_X_PASAJERO)) {
			

			EstiloVehiculoDTO estilo = entidadService.findById(EstiloVehiculoDTO.class, 
					getEstiloVehiculoId(coberturaSiniestroReporteCabina.getIncisoReporteCabina().getAutoIncisoReporteCabina().getEstiloId()));

			if (estilo != null && estilo.getNumeroAsientos() != null) {

				sumaAsegurada = sumaAsegurada.multiply(new BigDecimal(estilo.getNumeroAsientos()));

			} 
		}

		if(monto.compareTo(sumaAsegurada)>0 ){
			if (claveFuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO) && historicoMovimientos.intValue() > 0) {
				errorMessagesList.add("El monto de la estimación sobrepasa el valor de la suma asegurada de la cobertura debido a siniestros anteriores.");
			} else {
				errorMessagesList.add("El monto de la estimación no debe sobrepasar el valor de la suma asegurada de la cobertura");
			}
		}
		
		if(reporteCabinaService.noEsEditableParaElRol(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().
				getSeccionReporteCabina().getReporteCabina())){
			errorMessagesList.add("El rol del usuario no permite modificaciones de un reporte convertido a siniestro");
		}
		
		if(estimacion.getId() == null && EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, estimacion.getTipoEstimacion())){
			CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporte);
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("reporteCabina.id", coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId());
			params.put("claveContrato", true);
			List<CondicionEspecialReporte> condEspecialReporte = entidadService.findByProperties(CondicionEspecialReporte.class, params);
		 	for(CondicionEspecialReporte condicion:condEspecialReporte){
		 		errorMessagesList.addAll(polizaSiniestroService.validaCondicionEspecial(condicion.getCondicionEspecial(), 
		 				coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina(), 
						PolizaSiniestroService.TIPO_VALIDACION_CONDICION.VALIDAR_PASE_GM));
			}
		}
		
		// # ELIMINAR MENSAJES DUPLICADOS
		Set<String> tempErrorMessageList = new HashSet<String>(errorMessagesList);
		errorMessagesList.clear();
		errorMessagesList.addAll(tempErrorMessageList);
		tempErrorMessageList.clear();
		
		return errorMessagesList;
	}
	
	
	@Override
	public void cargaInformacionAutoInciso(EstimacionCoberturaSiniestroDTO dto, AutoIncisoReporteCabina auto){
		
		if( auto !=null && auto.getMarcaId() != null ){
			
			MarcaVehiculoDTO marca = entidadService.findById(MarcaVehiculoDTO.class, auto.getMarcaId());
			EstiloVehiculoDTO estilo = null;
			

			dto.setEstilo(auto.getDescripcionFinal());
			

			dto.setEstilo(auto.getDescripcionFinal());
			if(auto.getEstiloId() != null){
				estilo = entidadService.findById(EstiloVehiculoDTO.class, getEstiloVehiculoId(auto.getEstiloId()));
			}
			if(marca != null){
				dto.setMarca(marca.getDescripcionMarcaVehiculo());
			}
			if(estilo != null){
				dto.setTransmision(estilo.getClaveTransmision());
			}
		
		}
	}	
	
	
	private EstiloVehiculoId getEstiloVehiculoId(String estiloId) {
		String[] arrayEstilo = estiloId.split("_");
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(arrayEstilo[0], arrayEstilo[1], new BigDecimal(arrayEstilo[2]));
		return estiloVehiculoId;		
	}
	
	private Long getClaveAmis(String estiloId) {
		String[] arrayEstilo = estiloId.split("_");
		return Long.parseLong( arrayEstilo[1] );		
	}
	
	
   @Override
	public String descripcionMoneda(BigDecimal idMoneda){
		String descripcion 				= "";
		MonedaDTO monedaDTO 			= null;
		
		monedaDTO = entidadService.findById(MonedaDTO.class, Short.parseShort(idMoneda.toString()));
		
		if( monedaDTO != null){
			descripcion = monedaDTO.getDescripcion();
		}
		
		return descripcion;
	}

   
   /**
    * Metodo que obtiene el listado de coberturas que pertenecen a una seccion.
    */
   @Override
   public List<CoberturaSeccionSiniestroDTO> obtenerCoberturasPorSeccion( Long idToSeccion ){
		List<CoberturaSeccionSiniestroDTO> listCoberturaSeccionSiniestro = new ArrayList<CoberturaSeccionSiniestroDTO>();
		listCoberturaSeccionSiniestro =  estimacionCoberturaDao.obtenerCoberturasSiniestrosPorSeccion(idToSeccion);
		if(CollectionUtils.isNotEmpty(listCoberturaSeccionSiniestro)){
			Collections.sort(listCoberturaSeccionSiniestro, new Comparator<CoberturaSeccionSiniestroDTO>() {
				@Override
				public int compare(CoberturaSeccionSiniestroDTO object1, CoberturaSeccionSiniestroDTO object2) {
					return object1.getNombreCobertura().compareTo(object2.getNombreCobertura());
				}
			});
		}
		return listCoberturaSeccionSiniestro;
	}

	
	private void setInfoReferenciaValorComercial( AutoIncisoReporteCabina autoInciso , DatosEstimacionCoberturaDTO datos ,CoberturaReporteCabina coberturaReporteCabina, String tipoEstimacion){
		Long claveAmis 			= null;
		Date fechaConsulta 		= null;
		
		if(EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion)|| EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, tipoEstimacion)){
			claveAmis = getClaveAmis(autoInciso.getEstiloId());
			fechaConsulta = coberturaReporteCabina.getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getFechaOcurrido();
			
			valorComercialVehiculoService.getValorComercial(datos, claveAmis, autoInciso.getModeloVehiculo(), fechaConsulta);
		}
		
	}
	
	@Override
	public List<PaseAtencionSiniestroDTO> obtenerDesgloseCoberturas(Long idReporteCabina ){
	   List<CoberturaReporteCabina> coberturas = this.entidadService.findByProperty(CoberturaReporteCabina.class,"incisoReporteCabina.seccionReporteCabina.reporteCabina.id", idReporteCabina);
	   List<PaseAtencionSiniestroDTO> pasesDeAtencionPorReporte = new ArrayList<PaseAtencionSiniestroDTO>();
	   for(CoberturaReporteCabina cobertura : coberturas ){
		   List<PaseAtencionSiniestroDTO> pasesDeAtencionPorCobertura = this.obtenerListadoPases(cobertura.getId(), null);
		   if(pasesDeAtencionPorCobertura!=null){
			   pasesDeAtencionPorReporte.addAll(pasesDeAtencionPorCobertura);
		   }
	   }
	   return pasesDeAtencionPorReporte;
	}
	
/**
	 * Método que genera un movimiento para una cobertura o pase de atención.
	 * @param idEstimacionCobertura
	 * @param estimacionNueva
	 * @param causaMovimiento
	 */
	 
	 
	@Override
	public PaseAtencionSiniestroDTO cargarPaseAtencion( EstimacionCoberturaReporteCabina estimacion ){
		PaseAtencionSiniestroDTO paseAtencion = new PaseAtencionSiniestroDTO();
		CoberturaReporteCabina coberturaReporteCabina = estimacion.getCoberturaReporteCabina();
		IncisoReporteCabina incisoReporteCabina = coberturaReporteCabina.getIncisoReporteCabina();
		SeccionReporteCabina seccionReporteCabina = incisoReporteCabina.getSeccionReporteCabina();
		ReporteCabina reporteCabina = seccionReporteCabina.getReporteCabina();
		CoberturaDTO coberturaDTO = coberturaReporteCabina.getCoberturaDTO();
		String tipoDesc = ""; 
		BigDecimal montoEstimacion = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(estimacion.getId());
		paseAtencion.setLimiteMontoPaciente(montoEstimacion != null ? montoEstimacion.toString() : null);
		DatosReporteSiniestroDTO datosReporteSiniestro = reporteCabinaService.obtenerDatosReporte(reporteCabina.getId());
		paseAtencion.setNumeroSiniestro(datosReporteSiniestro.getNumeroSiniestro());
		paseAtencion.setIdReporteCabina(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId());
		paseAtencion.setMontoReserva(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getImporteReserva());
		paseAtencion.setDescripcionVehiculo(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getDescripcionFinal());
		paseAtencion.setNumeroReporte(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getNumeroReporte());
		
		Integer estatusReporte = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getEstatus();
		if (estatusReporte != null) {
			paseAtencion.setEstatus(estatusReporte.toString());
		}			
		
		String nombreCobertura = obtenerNombreCobertura(coberturaDTO, 
				estimacion.getCoberturaReporteCabina().getClaveTipoCalculo(), 
				null, estimacion.getTipoEstimacion(), 
				estimacion.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());
		
		paseAtencion.setNombreCobertura(nombreCobertura);
		paseAtencion.setEstimacionCoberturaReporte(estimacion);
		
		Map<String,String> tiposAtencionMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.PASES_DE__ATENCION);
		tipoDesc = tiposAtencionMap.get(estimacion.getTipoPaseAtencion());
		
		paseAtencion.setTipoPaseAtencionDescripcion(tipoDesc);
		
		paseAtencion.setFechaInicial(estimacion.getFechaCreacion());
		
		String[] listaRolesImpresionSoloCopia = new String[]{ROL_COORDINADOR_CABINA_AJUSTES_SINIESTROS,
				ROL_COORDINADOR_VALUACION_SINIESTROS,
				ROL_COORDINADOR_AJUSTES_AUTOS,
				ROL_SUBDIRECTOR_SINIESTROS_AUTOS}; 
		
		if( usuarioService.tieneRolUsuarioActual(listaRolesImpresionSoloCopia) && estimacion.getFechaImpresion()==null){
			paseAtencion.setPuedeImprimir(Boolean.FALSE);
		}else{
			if(EnumUtil.equalsValue(TipoPaseAtencion.SOLO_REGISTRO, estimacion.getTipoPaseAtencion())){
				paseAtencion.setPuedeImprimir(Boolean.FALSE);
			}else if (estimacion.getEsEditable() != null && estimacion.getEsEditable().equals("NA")){
				paseAtencion.setPuedeImprimir(Boolean.FALSE);			
			} else {
				paseAtencion.setPuedeImprimir(Boolean.TRUE);
			}
		}
		if(EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, estimacion.getTipoEstimacion()) 
				|| EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())){
			paseAtencion.setAplicaHGS(Boolean.TRUE);
			paseAtencion.setFueEnviadoAHGS(this.hgsService.isExitosoHgs(estimacion.getFolio()));
		}else{
			paseAtencion.setAplicaHGS(Boolean.FALSE);
		}
		return paseAtencion;
	}

	

	@Override
	public PaseAtencionImpresionDTO cargarPaseAtencionImpresion(
			EstimacionCoberturaReporteCabina estimacion) {
		
		String tieneCondicionesEspStr = "",
		ladaContacto = ""           ,ladaContacto2 = ""             ,telContacto = ""           ,telContacto2 = "",
		telefonoAfectado = ""       ,celularAfectado = ""           ,nombreCobertura = ""       ,observacionesTercero = "",
		daniosPreexistentes = ""    ,tipoPrestador = ""             ,marcaVehiculo = ""         ,tipoVehiculo = "",
		transmision = ""            ,placas = ""                    ,color = ""                 ,numeroSerie = "",
		aplicaDeducibleStr = ""     ,domicilioAfectado = ""         ,nombrePrestador = ""       ,telefonoPrestador = "",
		domicilioPrestador = ""     ,porcentajeDeducibleEE = "0.0%" ,porcentajeDeducible="0.0%" ,tieneEquipoEspecialStr = "No",
		daniosEncubiertos = ""         ,valorComercialEE = ""      ,daniosMecanicos = "",
		tituloImpresion = COPIA     ,daniosInteriores = ""          ,bastiadores = ""           ,marcaImpresion = "",
		modeloVehiculo = ""         ,consecuenciaAccidente = ""     ,cobDesc = ""               ,clausulasAplicables ="",
		tipoCalculo              = estimacion.getTipoEstimacion(),
		nombreAfectado           = estimacion.getNombreAfectado();

		Boolean tieneCondicionesEspeciales = false,aplicaDeducible = false,tieneEquipoEspecial = false;
		Integer totalLesionados = 0, numeroLesionados = 0, edadTercero = 0;
		Double valorComercial = 0.00, limiteMontoPaciente = 0.00;
		short tipoImpresion = 0;

		
		PaseAtencionImpresionDTO paseAtencion           = new PaseAtencionImpresionDTO();		
		CoberturaReporteCabina coberturaReporteCabina   = estimacion.getCoberturaReporteCabina();
		CoberturaReporteCabina coberturaEQE             = null;
		IncisoReporteCabina incisoReporteCabina         = coberturaReporteCabina.getIncisoReporteCabina();
		AutoIncisoReporteCabina autoIncisoReporteCabina = incisoReporteCabina.getAutoIncisoReporteCabina();
		SeccionReporteCabina seccionReporteCabina 		= incisoReporteCabina.getSeccionReporteCabina();
		ReporteCabina reporte 							= seccionReporteCabina.getReporteCabina();
		PolizaDTO poliza 								= reporte.getPoliza();
		DatosReporteSiniestroDTO datosReporte 			= reporteCabinaService.obtenerDatosReporte(reporte.getId());
		EstimacionCoberturaSiniestroDTO dto 			= new EstimacionCoberturaSiniestroDTO();
		ServicioSiniestro ajustador 					= new ServicioSiniestro();
		PersonaMidas datosAjustador 					= new PersonaMidas();
		Usuario usuarioActual 							= usuarioService.getUsuarioActual();
		SimpleDateFormat sdfFecha 						= new SimpleDateFormat( "dd/MM/yyyy" );    	
		String fechaOcurridoStr = "";
		
		
		//List<PersonaDireccionMidas> direcciones = entidadService.findAll( PersonaDireccionMidas.class );
		
/*---------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------*/
/*---------------------------------------------------------------------------------------------------------------------------------------*/

		this.cargaInformacionAutoInciso(dto, autoIncisoReporteCabina);

		if( reporte.getAjustador() != null ){
			ajustador = reporte.getAjustador();
			datosAjustador = ajustador.getPersonaMidas();
		} else {
			datosAjustador.setNombre("");
		}


		HashMap<String, Object> tieneCondicionParams = new HashMap<String, Object>();
		tieneCondicionParams.put("reporteCabina.id", reporte.getId());

		List<CondicionEspecialReporte> condicionesEspecialesReporte = entidadService.findByProperties(CondicionEspecialReporte.class, tieneCondicionParams);
		if( condicionesEspecialesReporte.size()>0 ){
			tieneCondicionesEspeciales = true;
		}

		tieneCondicionesEspStr = (tieneCondicionesEspeciales)? "Si":"No";
		clausulasAplicables    = (tieneCondicionesEspeciales)? "Condiciones Especiales":"";
		ladaContacto           = (estimacion.getLadaTelContacto()!=null)   ? estimacion.getLadaTelContacto():"";
		telContacto            = (estimacion.getTelContacto()!=null)       ? estimacion.getTelContacto():"";
		telContacto2           = (estimacion.getTelContactoDos()!=null)    ? estimacion.getTelContactoDos():"";
		telefonoAfectado       = ladaContacto + " " + telContacto;
		ladaContacto2          = (estimacion.getLadaTelContactoDos()!=null)? estimacion.getLadaTelContactoDos():"";
		celularAfectado        = ladaContacto2 + " " + telContacto2;

		HashMap<String, Object> totalLesionadosParams = new HashMap<String, Object>();
		totalLesionadosParams.put("tipoEstimacion", estimacion.getTipoEstimacion());
		totalLesionadosParams.put("coberturaReporteCabina.id", estimacion.getCoberturaReporteCabina().getId());
		List<EstimacionCoberturaReporteCabina> lTotalLesionados = entidadService.findByProperties(EstimacionCoberturaReporteCabina.class, totalLesionadosParams);
		totalLesionados = lTotalLesionados.size();
		
		if( estimacion.getSecuenciaPaseDeCobertura() != null ){
			numeroLesionados = estimacion.getSecuenciaPaseDeCobertura();
		}
		
		HashMap<String, Object> coberturaEQEParams = new HashMap<String, Object>();
		coberturaEQEParams.put("incisoReporteCabina.id", incisoReporteCabina.getId());
		coberturaEQEParams.put("claveTipoCalculo", ClaveTipoCalculo.EQUIPO_ESPECIAL.getValue());
		List<CoberturaReporteCabina> coberturaReporteCabinaLst = entidadService.findByProperties(CoberturaReporteCabina.class, coberturaEQEParams);

		if(coberturaReporteCabinaLst != null && !coberturaReporteCabinaLst.isEmpty()) {
			coberturaEQE = coberturaReporteCabinaLst.get(0);
		}
			
		if( coberturaEQE != null ){
			porcentajeDeducibleEE = (coberturaEQE!=null && coberturaEQE.getPorcentajeDeducible()!=null)?coberturaEQE.getPorcentajeDeducible().toString():porcentajeDeducible;
			tieneEquipoEspecialStr = (coberturaEQE!= null)?"Si":"No";		
			if( coberturaEQE.getValorSumaAsegurada() != null ) {
				valorComercialEE = coberturaEQE.getValorSumaAsegurada().toString();
			}
		}else{
			valorComercialEE = "0";
		}
		
		porcentajeDeducible = (coberturaReporteCabina!=null)?coberturaReporteCabina.getPorcentajeDeducible().toString()+"%":"0.0%";

		if( estimacion.getSecuenciaImpresion()==null ){
			estimacion.setSecuenciaImpresion(1L);
		} else {
			Long secuencia = estimacion.getSecuenciaImpresion()+1L;
			estimacion.setSecuenciaImpresion(secuencia);
		}
		
		// # SETEA LA FECHA SOLO LA PRIMERA VEZ QUE IMPRIME EL REPORTE
		if( estimacion.getFechaImpresion() == null ){
			estimacion.setFechaImpresion(new Date());
		}
		
		if( usuarioService.tieneRol(ROL_AJUSTADOR, usuarioActual)){
			
			if(estimacion.getSecuenciaImpresion() == 1L)
			{
				tituloImpresion = ORIGINAL_IMPRESION;
				
			}else if (estimacion.getSecuenciaImpresion() > 1L 
					&& estimacion.getSecuenciaImpresion() <= MAXIMO_ORIGINAL_IMPRESION)
			{
				tituloImpresion = ORIGINAL_REIMPRESION;				
				
			}else{   
				// A partir de la 4ta Impresion unicamente seran copias
				tituloImpresion = COPIA;
				marcaImpresion = DOCUMENTO_NO_VALIDO;								
			}
			
		}else if( usuarioService.tieneRol(ROL_COORDINADOR_CABINA_AJUSTES_SINIESTROS, usuarioActual) ||
					usuarioService.tieneRol(ROL_COORDINADOR_VALUACION_SINIESTROS, usuarioActual) ||
					usuarioService.tieneRol(ROL_COORDINADOR_AJUSTES_AUTOS, usuarioActual) ||
					usuarioService.tieneRol(ROL_SUBDIRECTOR_SINIESTROS_AUTOS, usuarioActual)
				){
			marcaImpresion = DOCUMENTO_NO_VALIDO;
			tituloImpresion = COPIA;
		}
		
			
		entidadService.save(estimacion);
			
/*******************************************************************************************************************************************/
		
		if( EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, estimacion.getTipoEstimacion())){
			paseAtencion.setTituloPaseAtencion(TITULO_ESTIMACION_DMA);
			TerceroDanosMateriales tercero = new TerceroDanosMateriales();
			tercero = entidadService.findById(TerceroDanosMateriales.class, estimacion.getId());
			 observacionesTercero = tercero.getObservaciones();
			 daniosPreexistentes = tercero.getDanosPreexistentes();
			 daniosInteriores = tercero.getDanosInteriores();
			 PrestadorServicio taller = tercero.getTaller();
			 PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();
			 if( taller != null ){
				 nombrePrestador = taller.getNombrePersona();
				 tipoPrestador = taller.getTipoPrestadoresStr();

				 if( taller.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(taller);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
			 } else if( ciaSeguros != null ){
				 nombrePrestador = ciaSeguros.getNombrePersona();
				 tipoPrestador= ciaSeguros.getTipoPrestadoresStr();
				 //personaMidas = ciaSeguros.getPersonaMidas();
				 
				 if( ciaSeguros.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(ciaSeguros);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
					 /*String lada = ( ciaSeguros.getPersonaMidas().getContacto().getTelCasaLada() == null )?"": ciaSeguros.getPersonaMidas().getContacto().getTelCasaLada();
					 String tel = ( ciaSeguros.getPersonaMidas().getContacto().getTelCasa() == null )?"":ciaSeguros.getPersonaMidas().getContacto().getTelCasa();
					 telefonoPrestador = lada + " " + tel;*/
				 }
			 }
			 placas = autoIncisoReporteCabina.getPlaca();
			 
			 if( autoIncisoReporteCabina.getColor() != null && !autoIncisoReporteCabina.getColor().equals("") ){
					color = catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.COLOR, autoIncisoReporteCabina.getColor()).getDescripcion();
				}
			 numeroSerie = autoIncisoReporteCabina.getNumeroSerie();
			 if( tercero.getSumaAseguradaObtenida() != null )
			 	valorComercial = tercero.getSumaAseguradaObtenida().doubleValue();
			 daniosEncubiertos = tercero.getDanosCubiertos();
			 daniosMecanicos = tercero.getDanosMecanicos();
			 bastiadores = tercero.getBastiadores();
			 this.cargaInformacionAutoInciso(dto, autoIncisoReporteCabina);
			 marcaVehiculo = (marcaVehiculo.equals(""))?dto.getMarca()+" "+dto.getEstilo():marcaVehiculo;
			 transmision = dto.getTransmision();
			 modeloVehiculo = (modeloVehiculo.equals(""))?autoIncisoReporteCabina.getModeloVehiculo().toString():modeloVehiculo;
			 aplicaDeducible = tercero.getAplicaDeducible();
			 
		} else if( EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, estimacion.getTipoEstimacion()) ){
			
			// OBTENER CONTADOR ESPECIAL PARA NUMERO_LESIONADOS EN CASO DE SER RCP
			numeroLesionados = this.obtenerPosicionParaReporte(estimacion.getId(), lTotalLesionados);
			
			paseAtencion.setTituloPaseAtencion(TITULO_ESTIMACION_GMC);
			TerceroGastosMedicosConductor tercero = new TerceroGastosMedicosConductor();
			tercero = entidadService.findById(TerceroGastosMedicosConductor.class, estimacion.getId());
			edadTercero = tercero.getEdad();
			if( tercero.getCoberturaReporteCabina() != null ){
				if( tercero.getCoberturaReporteCabina().getCoberturaDTO() != null ){
					Map<String,String> coberturasMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PASE_ATENCION);
					cobDesc = coberturasMap.get( tercero.getTipoEstimacion() );
					nombreCobertura = (nombreCobertura.equals(""))?cobDesc:"";
				}
			}
			PrestadorServicio medico = tercero.getMedico();
			PrestadorServicio hospitalPS = tercero.getHospital();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();
			if( medico != null ){
				nombrePrestador = medico.getNombrePersona();
				tipoPrestador = medico.getTipoPrestadoresStr();
				
				if( medico.getPersonaMidas() != null ){
					Map<String,String> datosPrestador = this.obtenerPersonaDireccion(medico);
					telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					domicilioPrestador = datosPrestador.get("domicilioPrestador");
				}
				
				//personaMidas = medico.getPersonaMidas();

			 } else if( hospitalPS != null ){
				 nombrePrestador = hospitalPS.getNombrePersona();
				 tipoPrestador=hospitalPS.getTipoPrestadoresStr();
				 
				 if( hospitalPS.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(hospitalPS);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
			 } else if( ciaSeguros != null ){
				 nombrePrestador = ciaSeguros.getNombrePersona();
				 tipoPrestador = ciaSeguros.getTipoPrestadoresStr();
				 
				 if( ciaSeguros.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(ciaSeguros);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
			 }
			placas = autoIncisoReporteCabina.getPlaca();
			
			if( tercero.getSumaAseguradaObtenida() != null )
				valorComercial = tercero.getSumaAseguradaObtenida().doubleValue();
			
			consecuenciaAccidente = tercero.getDescripcion();
			
		} else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, estimacion.getTipoEstimacion()) ){
			
			// OBTENER CONTADOR ESPECIAL PARA NUMERO_LESIONADOS EN CASO DE SER RCP
			numeroLesionados = this.obtenerPosicionParaReporte(estimacion.getId(), lTotalLesionados);
			
			paseAtencion.setTituloPaseAtencion(TITULO_ESTIMACION_GME);
			TerceroGastosMedicos tercero = new TerceroGastosMedicos();
			tercero = entidadService.findById(TerceroGastosMedicos.class, estimacion.getId());
			edadTercero = tercero.getEdad();
			PrestadorServicio medico = tercero.getMedico();
			PrestadorServicio hospitalPs = tercero.getHospital();
			
			if( medico != null ){
				Map<String,String> datosPrestador = this.obtenerPersonaDireccion(medico);
				nombrePrestador = datosPrestador.get("nombrePrestador");
				tipoPrestador = medico.getTipoPrestadoresStr();
				
				if( medico.getPersonaMidas() != null ){
					telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					domicilioPrestador = datosPrestador.get("domicilioPrestador");
				}

				
			 } else if( hospitalPs != null ){
				 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(hospitalPs);
				 nombrePrestador = datosPrestador.get("nombrePrestador");
				 tipoPrestador=hospitalPs.getTipoPrestadoresStr();
				 
				 if( hospitalPs.getPersonaMidas() != null ){
					telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				
			 }
			
			if( tercero.getSumaAseguradaObtenida()!=null )
				valorComercial = tercero.getSumaAseguradaObtenida().doubleValue();
			
			if( tercero.getCoberturaReporteCabina() != null ){
				if( tercero.getCoberturaReporteCabina().getCoberturaDTO() != null ){
					Map<String,String> coberturasMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PASE_ATENCION);
					cobDesc = coberturasMap.get( tercero.getTipoEstimacion() );
					nombreCobertura = (nombreCobertura.equals(""))?cobDesc:"";
				}
			}
			
			consecuenciaAccidente = tercero.getDescripcion();
//			domicilioPrestador = this.obtenerPrimeraDireccionPersona( personaMidas );
			
		} else if(EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, estimacion.getTipoEstimacion()) ){
			paseAtencion.setTituloPaseAtencion(TITULO_ESTIMACION_RCB);
			TerceroRCBienes tercero = new TerceroRCBienes();
			tercero = entidadService.findById(TerceroRCBienes.class, estimacion.getId());
			daniosPreexistentes = tercero.getDanosPreexistentes();
			
			PrestadorServicio responsableReparacion      = tercero.getResponsableReparacion();
			PrestadorServicio ciaSeguros 				 = tercero.getCompaniaSegurosTercero();
			
			if( responsableReparacion != null ){
				 nombrePrestador = tercero.getResponsableReparacion().getNombrePersona();
				 tipoPrestador=responsableReparacion.getTipoPrestadoresStr();
				 
				 if( responsableReparacion.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(responsableReparacion);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
			 }
			
			if( ciaSeguros != null ){
				 nombrePrestador = tercero.getCompaniaSegurosTercero().getNombrePersona();
				 tipoPrestador=ciaSeguros.getTipoPrestadoresStr();
				 
				 if( ciaSeguros.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(ciaSeguros);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
			 }

			valorComercial = (tercero.getSumaAseguradaObtenida()!= null)?tercero.getSumaAseguradaObtenida().doubleValue():0.00;
			
			CiudadMidas municipio = tercero.getMunicipio();
			EstadoMidas estado = tercero.getEstado();
			PaisMidas pais = tercero.getPais();
			
			domicilioAfectado = ((tercero.getUbicacion()!=null)?tercero.getUbicacion()+", ":"") + 
				((municipio!=null)?municipio.getDescripcion()+", ":"") +
				((estado!=null)?tercero.getEstado().getDescripcion()+", ":"") + "" + 
				((pais!=null)?tercero.getPais().getDescripcion():". ");

			telefonoAfectado     = tercero.getTelContacto();
			nombreAfectado       = tercero.getNombreAfectado();
			observacionesTercero = tercero.getObservacion();
			celularAfectado      = tercero.getTelContactoDos();
			
		} else if(EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, estimacion.getTipoEstimacion())){
			paseAtencion.setTituloPaseAtencion(TITULO_ESTIMACION_RCJ);
			TerceroRCViajero tercero = new TerceroRCViajero();
			tercero = entidadService.findById(TerceroRCViajero.class, estimacion.getId());
			edadTercero = tercero.getEdad();
			PrestadorServicio medico = tercero.getMedico();
			PrestadorServicio hospitalPs = tercero.getHospital();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();
			
			if( medico != null ){
				nombrePrestador = medico.getNombrePersona();
				tipoPrestador = medico.getTipoPrestadoresStr();
				
				if( medico.getPersonaMidas() != null ){
					Map<String,String> datosPrestador = this.obtenerPersonaDireccion(medico);
					telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					domicilioPrestador = datosPrestador.get("domicilioPrestador");
				}

			 } else if( hospitalPs != null ){
				 nombrePrestador = hospitalPs.getNombrePersona();
				 tipoPrestador=hospitalPs.getTipoPrestadoresStr();
				 
				 if( hospitalPs.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(hospitalPs);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
				 if( tercero.getCoberturaReporteCabina() != null ){
						if( tercero.getCoberturaReporteCabina().getCoberturaDTO() != null ){
							Map<String,String> coberturasMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PASE_ATENCION);
							cobDesc = coberturasMap.get( tercero.getTipoEstimacion() );
							nombreCobertura = (nombreCobertura.equals(""))?cobDesc:"";
						}
					}
				 
			 } else if( ciaSeguros != null ){
				 nombrePrestador = ciaSeguros.getNombrePersona();
				 tipoPrestador = ciaSeguros.getTipoPrestadoresStr();
				 
				 if( ciaSeguros.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(ciaSeguros);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
			 }
			valorComercial = (tercero.getSumaAseguradaObtenida()!= null)?tercero.getSumaAseguradaObtenida().doubleValue():0.00;
			consecuenciaAccidente = tercero.getDescripcion();
			
		} else if(EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, estimacion.getTipoEstimacion())){
			
			// OBTENER CONTADOR ESPECIAL PARA NUMERO_LESIONADOS EN CASO DE SER RCP
			numeroLesionados = this.obtenerPosicionParaReporte(estimacion.getId(), lTotalLesionados);
			
			paseAtencion.setTituloPaseAtencion(TITULO_ESTIMACION_RCP);
			TerceroRCPersonas tercero = new TerceroRCPersonas();
			tercero = entidadService.findById(TerceroRCPersonas.class, estimacion.getId());
			edadTercero = tercero.getEdad();
			PrestadorServicio medico = tercero.getMedico();
			PrestadorServicio hospitalPs = tercero.getHospital();
			PrestadorServicio ciaSeguros = tercero.getCompaniaSegurosTercero();
			if( medico != null ){
				nombrePrestador = medico.getNombrePersona();
				tipoPrestador = medico.getTipoPrestadoresStr();
				
				if( medico.getPersonaMidas() != null ){
					Map<String,String> datosPrestador = this.obtenerPersonaDireccion(medico);
					telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					domicilioPrestador = datosPrestador.get("domicilioPrestador");
				}

			 } else if( hospitalPs != null ){
				 nombrePrestador = hospitalPs.getNombrePersona();
				 tipoPrestador=hospitalPs.getTipoPrestadoresStr();
				 
				 if( hospitalPs.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(hospitalPs);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
			 } else if( ciaSeguros != null ){
				 nombrePrestador = ciaSeguros.getNombrePersona();
				 tipoPrestador = ciaSeguros.getTipoPrestadoresStr();
				 
				 if( ciaSeguros.getPersonaMidas() != null ){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(ciaSeguros);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				 }
				 
			 }
			placas = autoIncisoReporteCabina.getPlaca();
			valorComercial = (tercero.getSumaAseguradaObtenida()!= null)?tercero.getSumaAseguradaObtenida().doubleValue():0.00;

			if( tercero.getCoberturaReporteCabina() != null ){
				if( tercero.getCoberturaReporteCabina().getCoberturaDTO() != null ){
					Map<String,String> coberturasMap = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_PASE_ATENCION);
					cobDesc = coberturasMap.get( tercero.getTipoEstimacion() );
					nombreCobertura = (nombreCobertura.equals(""))?cobDesc:"";
				}
			}
			consecuenciaAccidente = tercero.getDescripcion();

		} else if(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())){
			paseAtencion.setTituloPaseAtencion(TITULO_ESTIMACION_RCV);
			TerceroRCVehiculo tercero = new TerceroRCVehiculo();
			tercero = entidadService.findById(TerceroRCVehiculo.class, estimacion.getId());
			observacionesTercero = tercero.getObservaciones();
			daniosPreexistentes = tercero.getDanosPreexistentes();
			PrestadorServicio taller = tercero.getTaller();
			PrestadorServicio ciaSegurosTercero = tercero.getCompaniaSegurosTercero();
			if( taller != null ){
				nombrePrestador = ( tercero.getTaller().getNombrePersona() != null )?tercero.getTaller().getNombrePersona():nombrePrestador;
				tipoPrestador = ( tercero.getTaller().getTipoPrestadoresStr() != null )?tercero.getTaller().getTipoPrestadoresStr():tipoPrestador;
				
				if( taller.getPersonaMidas() != null){
					Map<String,String> datosPrestador = this.obtenerPersonaDireccion(taller);
					telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					domicilioPrestador = datosPrestador.get("domicilioPrestador");
				}
				
			 } else if( ciaSegurosTercero != null ){
				 nombrePrestador = ( ciaSegurosTercero.getNombrePersona() != null )?ciaSegurosTercero.getNombrePersona():nombrePrestador;
				 tipoPrestador = ( ciaSegurosTercero.getTipoPrestadoresStr() != null )?ciaSegurosTercero.getTipoPrestadoresStr():tipoPrestador;
				 
				 if( ciaSegurosTercero.getPersonaMidas() != null){
					 Map<String,String> datosPrestador = this.obtenerPersonaDireccion(ciaSegurosTercero);
					 telefonoPrestador  = datosPrestador.get("telefonoPrestador");
					 domicilioPrestador = datosPrestador.get("domicilioPrestador");
				}
				 
			 }
			placas = autoIncisoReporteCabina.getPlaca();
			if(autoIncisoReporteCabina.getColor()!=null){
				Integer colorId = new Integer(autoIncisoReporteCabina.getColor());
				if( colorId > 0 )
					colorId-=1;
				
				color = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.COLOR).values().toArray()[colorId].toString();
			}
			valorComercial = (tercero.getSumaAseguradaObtenida()!=null)?tercero.getSumaAseguradaObtenida().doubleValue():0.00;
			daniosEncubiertos = tercero.getDanosCubiertos();
			daniosMecanicos = tercero.getDanosMecanicos();
			bastiadores = tercero.getBastiadores();
			marcaVehiculo = (marcaVehiculo.equals(""))?tercero.getMarca()+" "+tercero.getEstiloVehiculo():marcaVehiculo;
			modeloVehiculo = (modeloVehiculo.equals(""))?tercero.getModeloVehiculo().toString():modeloVehiculo;

			modeloVehiculo = (modeloVehiculo.equals(""))?tercero.getModeloVehiculo().toString():modeloVehiculo;
			if( tercero.getColor() != null && !tercero.getColor().equals("") ){
				Integer idColor = new Integer(tercero.getColor());
				color = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.COLOR).values().toArray()[idColor].toString();
			}
			marcaVehiculo = (marcaVehiculo.equals(""))?tercero.getMarca():marcaVehiculo;
			placas = tercero.getPlacas();
			transmision = dto.getTransmision();
			nombreAfectado = tercero.getNombreAfectado();
			numeroSerie = tercero.getNumeroSerie();
			consecuenciaAccidente=tercero.getDano();
			daniosPreexistentes = tercero.getDanosPreexistentes();
			observacionesTercero = tercero.getObservaciones();
			daniosMecanicos = tercero.getDanosMecanicos();
			daniosInteriores = tercero.getDanosInteriores();
			bastiadores = tercero.getBastiadores();
			tercero.getEstiloVehiculo();
//			domicilioPrestador = this.obtenerPrimeraDireccionPersona( personaMidas );
			
		}

		BigDecimal montoEstimacionActual = movimientoSiniestroService.obtenerMontoEstimacionAfectacion(estimacion.getId());
		limiteMontoPaciente = (montoEstimacionActual != null)?(montoEstimacionActual).doubleValue():0.00;
		aplicaDeducibleStr = (aplicaDeducible != null && aplicaDeducible) ? "Si" : "No";
		fechaOcurridoStr = sdfFecha.format(reporte.getFechaOcurrido()) ;
		 
/*--------------------------------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------------------------------*/
/*--------------------------------------------------------------------------------------------------------------------------------------------------*/

		paseAtencion.setNumeroPoliza          ( poliza.getNumeroPolizaFormateada() );
		paseAtencion.setNumeroSiniestro       ( datosReporte.getNumeroSiniestro() );
		paseAtencion.setNumeroReporte	      ( datosReporte.getNumeroReporte() );
		paseAtencion.setFolio				  ( estimacion.getFolio() );
		paseAtencion.setClaveAjustador        ( (ajustador!=null&&ajustador.getId()!=null)?ajustador.getId().toString():"" );
		paseAtencion.setNombreAjustador       ( ( datosAjustador != null )?datosAjustador.getNombre():"" );
		paseAtencion.setNombrePrestador       ( nombrePrestador );
		paseAtencion.setDomicilioPrestador    ( domicilioPrestador );
		paseAtencion.setTelefonoPrestador     ( telefonoPrestador );
		paseAtencion.setTieneCondicionesEsp   ( tieneCondicionesEspeciales );
		paseAtencion.setNombreAfectado        ( estimacion.getNombreAfectado() );
		paseAtencion.setDomicilioAfectado     ( domicilioAfectado );
		paseAtencion.setTelefonoAfectado      ( telefonoAfectado );
		paseAtencion.setCelularAfectado       ( celularAfectado );
		paseAtencion.setObservaciones         ( observacionesTercero );
		paseAtencion.setDaniosPreexistentes   ( daniosPreexistentes );
		paseAtencion.setFechaExpedicion       ( TimeUtils.now().toDate() );
		paseAtencion.setNumeroInciso          ( incisoReporteCabina.getNumeroInciso() );
		paseAtencion.setNombreCobertura       ( nombreCobertura );
		paseAtencion.setNumeroLesionados      ( numeroLesionados );
		paseAtencion.setTotalLesionados       ( totalLesionados );
		paseAtencion.setNombreContratante     ( incisoReporteCabina.getNombreAsegurado() );
		paseAtencion.setLimiteMontoPaciente   ( limiteMontoPaciente );
		paseAtencion.setEdad                  ( edadTercero );
		paseAtencion.setNombreAfectado        ( nombreAfectado);
		paseAtencion.setConsecuenciaAccidente ( consecuenciaAccidente );
		paseAtencion.setFechaOcurridoStr      ( fechaOcurridoStr );
		paseAtencion.setLugarExpedicion       ( reporte.getOficina().getNombreOficina() );
		paseAtencion.setTipoPrestador         ( tipoPrestador );
		paseAtencion.setMarcaVehiculo         ( marcaVehiculo );
		paseAtencion.setTipoVehiculo          ( tipoVehiculo );
		paseAtencion.setTransmision           ( transmision );
		paseAtencion.setPlacas                ( placas );
		paseAtencion.setColor                 ( color.toUpperCase() );
		paseAtencion.setNumeroSerie           ( numeroSerie );
		paseAtencion.setAplicaDeducible       ( aplicaDeducible );
		paseAtencion.setTipoCalculo           ( tipoCalculo );
		paseAtencion.setPorcentajeDeducible   ( porcentajeDeducible );
		paseAtencion.setValorComercial        ( valorComercial );
		paseAtencion.setTieneEquipoEspecial   ( tieneEquipoEspecial );
		paseAtencion.setPorcentajeDeducibleEE ( porcentajeDeducibleEE );
		paseAtencion.setValorComercialEE      ( valorComercialEE);
		paseAtencion.setDaniosEncubiertos     ( daniosEncubiertos);
		paseAtencion.setTipoImpresion         ( tipoImpresion );
		paseAtencion.setDaniosMecanicos       ( daniosMecanicos);
		paseAtencion.setDaniosInteriores      ( daniosInteriores);
		paseAtencion.setBastiadores           ( bastiadores);
		paseAtencion.setTituloImpresion       ( tituloImpresion);
		paseAtencion.setModeloVehiculo        ( modeloVehiculo);
		paseAtencion.setAplicaDeducibleStr    ( aplicaDeducibleStr );
		if( EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, estimacion.getTipoEstimacion())){ 
			paseAtencion.setAsegurado( "Si" ); 
		}else{ 
			paseAtencion.setAsegurado( "No" ); 
		}
		paseAtencion.setTieneCondicionesEspStr( tieneCondicionesEspStr );
		paseAtencion.setTieneEquipoEspecialStr( tieneEquipoEspecialStr );
		paseAtencion.setMarcaImpresion        ( marcaImpresion );
		paseAtencion.setClausulasAplicables   ( clausulasAplicables );
		paseAtencion.setTipoMovimiento        ( estimacion.getTipoPaseAtencion() );
		
		return paseAtencion;
	}


	/**
	 * 
	 * @param keyEstimacion    ID de la estimacion a imprimir
	 * @param lTotalLesionados Lista de Objetos Estimacion con todos los pertencientes al reporte y Cobertura {RCV o RCP o RCB}
	 * @return
	 */
	private int obtenerPosicionParaReporte( Long keyEstimacion, List<EstimacionCoberturaReporteCabina> lTotalLesionados ){
		int posicion = 1;
		List<Long> lKeyEstimacion = new ArrayList<Long>();
		if( !lTotalLesionados.isEmpty() ){
			// SI List TotalLesionados ES MAYOR A 1 SE APLICA ORDENAMIENTO
			if( lTotalLesionados.size() > 1 ){
				
				for( EstimacionCoberturaReporteCabina ecr : lTotalLesionados ){
					lKeyEstimacion.add( ecr.getId() );
				}
				// ORDENAR DE MAYOR A MENOR
				Collections.sort(lKeyEstimacion);
				// BUSCAR KeyEstimacion Y OBTENER LA POSICIÓN
				for(Long k : lKeyEstimacion ){
					if( keyEstimacion == k ){
						break;
					}else{
						posicion++;
					}
				}

			}
		}
		
		return posicion;
	}
	
	/***
	 * Obtiene la dirección del prestador de servicio
	 * @param PrestadorServicio prestadorServicio
	 * @return Map<String,String>
	 */
	private Map<String,String> obtenerPersonaDireccion(PrestadorServicio prestadorServicio){
		
		Map<String,String> mDatosPrestador = new HashMap<String,String>();
		String direccion = "";
		mDatosPrestador.put("nombrePrestador"   , "");
		mDatosPrestador.put("telefonoPrestador" , "");
		mDatosPrestador.put("domicilioPrestador", "");
		
		if( prestadorServicio != null ){
			
			mDatosPrestador.put("nombrePrestador" , prestadorServicio.getNombrePersona() );
			
			// # VALIDACION DE TELEFONO PRESTADOR
			try{
				mDatosPrestador.put("telefonoPrestador" , ( (prestadorServicio.getPersonaMidas().getContacto().getTelCasaLada() == null )? "": prestadorServicio.getPersonaMidas().getContacto().getTelCasaLada()) + " " + ( ( prestadorServicio.getPersonaMidas().getContacto().getTelCasa()     == null )? "": prestadorServicio.getPersonaMidas().getContacto().getTelCasa() ) );
			}catch(Exception e){
				e.printStackTrace();
			}
			
			// # VALIDACION DE DIRECCION
			try{
				DireccionMidas direccionMidas = this.entidadService.findById(DireccionMidas.class, prestadorServicio.getPersonaMidas().getPersonaDireccion().get(0).getId().getDireccionId() );
				
				direccion = 
					(( direccionMidas.getCalle()               !=null ) ? "CALLE: " + direccionMidas.getCalle() : "") + " " + 
					(( direccionMidas.getNumExterior()         !=null ) ? "# "      + direccionMidas.getNumExterior() +", ":"") + " " + 
					(( direccionMidas.getColonia()             !=null ) ? ",COL: "   + direccionMidas.getColonia().getDescripcion():"")+ " " + 
					(( direccionMidas.getCodigoPostalColonia() !=null ) ? ",C.P: "   + direccionMidas.getCodigoPostalColonia():"")+ " " + 
					(( direccionMidas.getColonia()!=null && direccionMidas.getColonia().getCiudad() !=null ) ? ",CIUDAD: " + direccionMidas.getColonia().getCiudad().getDescripcion() : "") + " " + 
					(( direccionMidas.getColonia()!=null && direccionMidas.getColonia().getCiudad() !=null && direccionMidas.getColonia().getCiudad().getEstado() !=null ) ? ",ESTADO: " + direccionMidas.getColonia().getCiudad().getEstado().getDescripcion() : "") ;
				
				mDatosPrestador.put("domicilioPrestador", direccion);
				
			}catch(Exception e){
				e.printStackTrace();
			}

		}
		
		return mDatosPrestador;
	}
	
	@Override
	public  TransporteImpresionDTO imprimirPaseAtencion(Long estimacionId) {
		EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		PaseAtencionImpresionDTO paseAtencionImpresion = new PaseAtencionImpresionDTO();
		List<PaseAtencionImpresionDTO> dataSource = new ArrayList<PaseAtencionImpresionDTO>();
		paseAtencionImpresion = this.cargarPaseAtencionImpresion(estimacion);
		String leyendaFinal = "";
		if( paseAtencionImpresion != null )
			dataSource.add(paseAtencionImpresion);
		
		String jrxml = "";
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEEEE dd MMMMM yyyy h.mm a",new Locale("es"));
		// Compilado de jReports y generación del .jasper
		if(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())){
			jrxml = PASE_ATENCION_VEHICULO;
		} else if( EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, estimacion.getTipoEstimacion())) {
			jrxml = PASE_ATENCION_DMA;
		} else if(	EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, estimacion.getTipoEstimacion()) || 
				EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, estimacion.getTipoEstimacion())|| 
				EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, estimacion.getTipoEstimacion())||
				EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, estimacion.getTipoEstimacion()) ){
			jrxml = PASE_ATENCION_MEDICO;
			if ( estimacion.getTipoPaseAtencion() != null && !estimacion.getTipoPaseAtencion().equals("RAC") ){
				Date fechaExpiracion = TimeUtils.addDaysToDate(estimacion.getFechaImpresion(),new Integer(DIAS_EXPIRAR_PASE));
				leyendaFinal = "ESTE PASE EXPIRA EL D�?A "+dateFormat.format(fechaExpiracion).toUpperCase();
				 
			}
		} else if( EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, estimacion.getTipoEstimacion()) ){
			jrxml = PASE_ATENCION_BIENES;
		}
		
		if( !jrxml.isEmpty()  ){
			JasperReport jReport = gImpresion.getOJasperReport(jrxml);
			
			// Se definen los parámetros a mapear en el jrxml
			Map<String, Object> params = new HashMap<String, Object>();
			params.put( "PRUTAIMAGEN",        	  SistemaPersistencia.LOGO_SEGUROS_AFIRME );
			params.put( "DERECHA_AUTO_IMG",   	  SistemaPersistencia.DERECHA_AUTO_IMG );
			params.put( "IZQUIERDA_AUTO_IMG", 	  SistemaPersistencia.IZQUIERDA_AUTO_IMG );
			params.put( "FRENTE_AUTO_IMG",    	  SistemaPersistencia.FRENTE_AUTO_IMG );
			params.put( "DETRAS_AUTO_IMG",    	  SistemaPersistencia.DETRAS_AUTO_IMG );
			params.put( "rutaImagenCheckOn",  	  SistemaPersistencia.CHECKBOX_ON );
			params.put( "rutaImagenCheckOff",     SistemaPersistencia.CHECKBOX_OFF );
			params.put( "LINE_GREEN",             SistemaPersistencia.LINE_GREEN );
			params.put( "TITULO2_ESTIMACION_GME", TITULO2_ESTIMACION_GME );
			params.put( "leyendaFinal",           leyendaFinal );
			params.put( "fechaImpresion",         estimacion.getFechaImpresion() );
			params.put(JRParameter.REPORT_LOCALE, new Locale("es"));
			
			//Se cargan los datos
			
			JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSource);
		
			// Rellena el documento con los datos y exporta el PDF
			if( jReport != null )
				transporteImpresionDTO = gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
		}

		return transporteImpresionDTO;	
	}


	@Override
	public List<PaseAtencionExportacionDTO> obtenerPaseAtencionExportacion( PaseAtencionSiniestroDTO filtro ){
		List<PaseAtencionExportacionDTO> estimaciones = estimacionCoberturaDao.buscarPasesAtencionExportacion(filtro);

		return estimaciones;
	}
	
	
	@Override
	public Boolean contieneCoberturasAfectadas(Long reporteCabinaId) {
		Boolean contiene = Boolean.FALSE;
		
		List<EstimacionCoberturaReporteCabina> estimaciones = entidadService.findByProperty(
				EstimacionCoberturaReporteCabina.class, 
				"coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id", 
				reporteCabinaId );
		
		if(estimaciones != null && estimaciones.size() > 0 ){
			contiene = Boolean.TRUE;
		}
		
		//TODO - Agregar cambio de SOLO REGISTRO que irá a otra tabla.
		return contiene;
	}
	
	/**
	 * Metodo que valida que las coberturas ya afectadas en un reporte se encuentren en una nueva configuracion de coberturas
	 * @param reporteCabinaId
	 * @param causaSiniestro
	 * @param tipoResponsabilidad
	 * @param terminoAjuste
	 * @return
	 */

	@Override
	public List<String> validaCambioEnCoberturasPorCausaTipoTermino(Long reporteCabinaId, 
		String causaSiniestro, String tipoResponsabilidad, String terminoAjuste){
		List<AfectacionCoberturaSiniestroDTO> coberturasAfectadasPorReporteDTO = obtenerCoberturasAfectacion(reporteCabinaId, null, null, null, true, false );
		List<ConfiguracionCoberturaSiniestro> configuracionesAfectablesNuevas = this.obtieneConfiguracionesAfectables(causaSiniestro, terminoAjuste, tipoResponsabilidad);
		List<String> mensajesError = new ArrayList<String>();
		for( AfectacionCoberturaSiniestroDTO afectacionCoberturaDTO : coberturasAfectadasPorReporteDTO){
			if(!this.presenteEnConfiguracion(afectacionCoberturaDTO, configuracionesAfectablesNuevas)){
				String mensajeError = this.validaCriteriosParaSerAfectable(afectacionCoberturaDTO);
				if(mensajeError!=null){
					mensajesError.add(mensajeError);
				}
			}else{
				//validar si algún pase de Tiene Reserva mayor a cero o con 
				List<EstimacionCoberturaReporteCabina> estimaciones = afectacionCoberturaDTO.getCoberturaReporteCabina().getlEstimacionCoberturaReporteCabina();
				   
				for(EstimacionCoberturaReporteCabina estimacion : estimaciones ){
					String tipoResponsabilidadRegistrada = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTipoResponsabilidad();
					if(!tipoResponsabilidadRegistrada.equals(tipoResponsabilidad)){
						String mensajeDeError = this.validaSiPuedeCambiarEstimacionASoloReporte(estimacion.getId());
						if(mensajeDeError!=null){
							mensajesError.add("En la cobertura "+afectacionCoberturaDTO.getNombreCobertura()+" : "+mensajeDeError);
						}
					}
				}
			}
		}
		return mensajesError;
	}
	
	@Override
	public void actualizaCampoEsEditableEnLasEstimaciones(Long reporteCabinaId, 
			String causaSiniestro, String tipoResponsabilidad, String terminoAjuste){
		this.estimacionCoberturaDao.actualizaEdicionDeEstimaciones(reporteCabinaId,causaSiniestro,tipoResponsabilidad,terminoAjuste);
	}
	
	private String validaSiPuedeCambiarEstimacionASoloReporte(Long idEstimacion){
		String mensajeDeError = null;
		BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(idEstimacion, Boolean.TRUE);
		if(reserva.compareTo(BigDecimal.ZERO)!=0){
			mensajeDeError="La reserva es mayor a cero";
		}
		BigDecimal montoOrdenesDeCompra = this.ordenCompraService.obtenerTotales(idEstimacion, null, null, null );
		if(montoOrdenesDeCompra.compareTo(BigDecimal.ZERO)!=0){
			mensajeDeError="Existen ordenes de compra pendientes";
		}
		return mensajeDeError;
	}
	
	
	private String validaCriteriosParaSerAfectable( AfectacionCoberturaSiniestroDTO afectacionCoberturaDTO) {
		String mensajeDeError = null;
		BigDecimal reserva = movimientoSiniestroService.obtenerReservaCobertura(afectacionCoberturaDTO.getCoberturaReporteCabina().getId(), null, Boolean.TRUE);
		
		if(reserva.compareTo(BigDecimal.ZERO)!=0){
			mensajeDeError="La reserva de la cobertura "+afectacionCoberturaDTO.getNombreCobertura()+" es diferente a cero";
		}
		BigDecimal montoOrdenesDeCompra = this.ordenCompraService.obtenerTotales(null, null, 
				afectacionCoberturaDTO.getCoberturaReporteCabina().getId(), 
				OrdenCompraAutorizacionService.TIPO_ORDEN_COMPRA_AFECTACION_RESERVA);
		if(montoOrdenesDeCompra.compareTo(BigDecimal.ZERO)!=0){
			mensajeDeError="La cobertura: "+afectacionCoberturaDTO.getNombreCobertura()+" tiene ordenes de compra pendientes";
		}
		return mensajeDeError;
	}



	private boolean presenteEnConfiguracion(AfectacionCoberturaSiniestroDTO afectacionCoberturaDTO, 
			List<ConfiguracionCoberturaSiniestro> configuracionesCobertura){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put( "cveTipoCalculoCobertura", afectacionCoberturaDTO.getCoberturaReporteCabina().getClaveTipoCalculo());
		params.put( "tipoConfiguracion", afectacionCoberturaDTO.getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());					
		List<ConfiguracionCalculoCoberturaSiniestro> confCalculoCoberturaList = entidadService.findByProperties( ConfiguracionCalculoCoberturaSiniestro.class, params );
		CoberturaDTO cobertura = afectacionCoberturaDTO.getCoberturaReporteCabina().getCoberturaDTO();
		for (ConfiguracionCalculoCoberturaSiniestro configuracionCalculo : confCalculoCoberturaList) {
			for (ConfiguracionCoberturaSiniestro configuracionCobertura: configuracionesCobertura) {
				if(configuracionCobertura.getCveTipoCalculoCobertura().equals(configuracionCalculo.getCveTipoCalculoCobertura())){
					Boolean validacionSubtipo = (configuracionCalculo.getCveSubTipoCalculoCobertura() != null)?configuracionCobertura.getCveSubTipoCalculoCobertura().equals(configuracionCalculo.getCveSubTipoCalculoCobertura()):Boolean.TRUE; 
					Boolean validacionTipoConfiguracion = Boolean.TRUE;
					if(cobertura.getTipoConfiguracion() != null){
						if(configuracionCalculo.getTipoConfiguracion()!=null && configuracionCobertura.getTipoConfiguracion()!=null
								&& cobertura.getTipoConfiguracion().equals(configuracionCalculo.getTipoConfiguracion())
								&& cobertura.getTipoConfiguracion().equals(configuracionCobertura.getTipoConfiguracion())){
							validacionTipoConfiguracion = Boolean.TRUE;
						} else {
							validacionTipoConfiguracion = Boolean.FALSE;
						}
					}
					if(validacionSubtipo && validacionTipoConfiguracion){
						return Boolean.TRUE;
					}
				}
			}
		}
		return Boolean.FALSE;
		
	}
	
	public List<ConfiguracionCoberturaSiniestro> obtieneConfiguracionesAfectables(String causaSiniestro, 
			String terminoAjuste, String tipoResponsabilidad){		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put( "tipoSiniestro", causaSiniestro );
		params.put( "terminoAjuste", terminoAjuste );
		params.put( "responsabilidad", tipoResponsabilidad );		
		return entidadService.findByProperties( ConfiguracionCoberturaSiniestro.class, params );
	}
	

	
	
	@Override
	public Boolean contieneCoberturasContratadas(Long reporteCabinaId, String cveTipoCalculo) {
		Boolean contiene = Boolean.FALSE;
		
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("incisoReporteCabina.seccionReporteCabina.reporteCabina.id", reporteCabinaId);
		if(!StringUtil.isEmpty(cveTipoCalculo)){
			parameters.put("claveTipoCalculo", cveTipoCalculo);
		}
		
		List<CoberturaReporteCabina> coberturas = entidadService.findByProperties(CoberturaReporteCabina.class, parameters);
		
		if (CollectionUtils.isNotEmpty(coberturas)) {
			contiene = Boolean.TRUE;
		}
		
		return contiene;
	}
	
	
	
	@Override
	public Boolean esSiniestrada(Long idReporteCabina, Long idEstimacionCobertura) {
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporteCabina);
		
		return EnumUtil.equalsValue(reporte.getEstatus(), EstatusReporteCabina.TRAMITE_SINIESTRO, EstatusReporteCabina.TERMINADO, EstatusReporteCabina.SINIESTRO_REAPERTURADO);
		
	}
	
	@Override
	public ConfiguracionCalculoCoberturaSiniestro obtenerConfiguracionCalcCobertura(String claveTipoCalculo, String claveSubTipoCalculo,
			String tipoEstimacion, String tipoConfiguracion) {
		
		ConfiguracionCalculoCoberturaSiniestro configuracion = null;
		
		Map<String, Object> params = new HashMap<String, Object>();
		
		if (claveTipoCalculo != null && !claveTipoCalculo.isEmpty() ) {
			params.put("cveTipoCalculoCobertura", claveTipoCalculo);
		}
		
		if ( claveSubTipoCalculo!=null && !claveSubTipoCalculo.isEmpty() ) {
			params.put("cveSubTipoCalculoCobertura", claveSubTipoCalculo);
		}
		
		if (tipoEstimacion != null && !tipoEstimacion.isEmpty() ) {
			params.put("tipoEstimacion", tipoEstimacion);
		}
		
		if (tipoConfiguracion != null && !tipoConfiguracion.isEmpty() ) {
			params.put("tipoConfiguracion", tipoConfiguracion);
		}
		
		List<ConfiguracionCalculoCoberturaSiniestro> configuraciones = 
			entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, params);
		
		if (configuraciones!= null && !configuraciones.isEmpty()) {
			configuracion = configuraciones.get(0);
		}		
		
		return configuracion;		
		
	}
	
	@Deprecated
	@Override
	public List<CatValorFijo> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad){
		return this.estimacionCoberturaDao.obtenerTipoDePasePorTipoEstimacion(codigoTipoEstimacion, codigoTipoResponsabilidad);
	}
	
	@Override
	public List<CatValorFijo> obtenerTipoDePasePorTipoEstimacion(String codigoTipoEstimacion,
			String codigoTipoResponsabilidad, String codigoCausaSiniestro, String codigoTerminoAjuste){
		return this.estimacionCoberturaDao.obtenerTipoDePasePorTipoEstimacion(codigoTipoEstimacion, codigoTipoResponsabilidad, codigoCausaSiniestro, codigoTerminoAjuste);
	}
	
	@Override
	public List<CatValorFijo> obtenerTipoDePaseMenosSoloRegistro(){
		return this.estimacionCoberturaDao.obtenerTipoDePaseMenosSoloRegistro();
	}
	
	@Override
	public void notificaSolicitudSiniestroAntiguo( EstimacionCoberturaSiniestroDTO estimacionCoberturaReporteCabinaDTO, Long keyReporteCabina ) {
		
		try{
			
			String causaMovimiento = estimacionCoberturaReporteCabinaDTO.getDatosEstimacion().getCausaMovimiento();
			
			ReporteCabina reporteCabina = this.entidadService.findById(ReporteCabina.class, keyReporteCabina);
			Map<String, String> lCatalogoValorFijo = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO);
			
			if( !lCatalogoValorFijo.isEmpty() ){
				for(Map.Entry<String, String> valor : lCatalogoValorFijo.entrySet() ){
					if( valor.getKey().equals( estimacionCoberturaReporteCabinaDTO.getDatosEstimacion().getCausaMovimiento() ) ){
						causaMovimiento = valor.getValue();
						break;
					}
				}
			}
			
			// # SI NUM_SINIESTRO ES NULO, SE MANDA NUM_REPORTE
			String numero = ( reporteCabina.getSiniestroCabina() == null ? reporteCabina.getClaveOficina()+"-"+reporteCabina.getConsecutivoReporte()+"-"+reporteCabina.getAnioReporte() : reporteCabina.getSiniestroCabina().getNumeroSiniestro() );
			LugarSiniestroMidas LugSiniMid = new LugarSiniestroMidas();
			HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();                                 
			//el nombre de la configuracion que provoco la notificacion
			dataArg.put("configuracion", causaMovimiento );
			//monto que activo la notificacion como string en formato de moneda
			dataArg.put("monto"        , estimacionCoberturaReporteCabinaDTO.getDatosEstimacion().getEstimacionNueva() ); 	
			//numero de siniestro
			dataArg.put("siniestro"    , numero );  											   							
			//numero de siniestro
			dataArg.put("oficinaId"    , reporteCabina.getOficina().getId() );  											
			//obtener usuario actual y enviar nombre - clave usuario
			dataArg.put("usuario"      , this.usuarioService.getUsuarioActual().getNombreCompleto() );
			//joksrc modif
			dataArg.put("oficinaSiniestro", reporteCabina.getOficina().getNombreOficina());
			dataArg.put("polizaNumSini", reporteCabina.getPoliza().getNumeroPolizaFormateada() );
			dataArg.put("nombreAsegurado", reporteCabina.getPoliza().getNombreAsegurado());
			dataArg.put("siniestroFecha", reporteCabina.getFechaOcurrido().toString());
			dataArg.put("siniestroTipo", LugSiniMid.getTipo());
			dataArg.put("coberturaAfectada", estimacionCoberturaReporteCabinaDTO.getNombreCobertura());
			
			this.notificacionService.enviarNotificacionSiniestros(EnvioNotificacionesService.EnumCodigo.SINIESTRO_ANTIG_MONTO_RESERVA.toString(),dataArg);

		}catch(Exception e){
			e.printStackTrace();
			LOG.error("Solicitud Autorizacion Siniestro Antiguo - Error al notificar autorizacion "+e.getMessage() );
		}
	}
	
	@Override
	public List<ReclamacionReservaJuridicoDTO> obtenerReservaCoberturaJuridico(Long idReporteCabina){
		List<ReclamacionReservaJuridicoDTO> reservasDTO = new ArrayList<ReclamacionReservaJuridicoDTO>();
		
		List<CoberturaReporteCabina> coberturas = obtenerCoberturasReporte(idReporteCabina);
		if(coberturas != null){
			for(CoberturaReporteCabina cobertura : coberturas){
				Map<String,Object> params = new HashMap<String,Object>();
				params.put( "cveTipoCalculoCobertura", cobertura.getClaveTipoCalculo());
                params.put( "tipoConfiguracion", cobertura.getCoberturaDTO().getTipoConfiguracion());                                                                  
                List<ConfiguracionCalculoCoberturaSiniestro> confCalculoCoberturaList = entidadService.findByProperties( ConfiguracionCalculoCoberturaSiniestro.class, params );
				if(confCalculoCoberturaList != null){
					for(ConfiguracionCalculoCoberturaSiniestro configuracion : confCalculoCoberturaList){
						ReclamacionReservaJuridicoDTO reservaDTO = new ReclamacionReservaJuridicoDTO();
						final String nombreComercial = cobertura.getCoberturaDTO().getNombreComercial();
                        final String nombreCobertura = configuracion.getNombreCobertura();
                        reservaDTO.setCoberturaId(cobertura.getId());
                        reservaDTO.setClaveSubCobertura((configuracion.getCveSubTipoCalculoCobertura() != null 
                        		&& !configuracion.getCveSubTipoCalculoCobertura().isEmpty())? 
                        		configuracion.getCveSubTipoCalculoCobertura() : cobertura.getClaveTipoCalculo());
                        reservaDTO.setNombreCobertura((nombreCobertura !=null)? nombreCobertura:nombreComercial);
                        reservaDTO.setReservaSiniestro(movimientoSiniestroService.obtenerReservaCobertura(cobertura.getId(), configuracion.getCveSubTipoCalculoCobertura(), Boolean.TRUE));
                       
                        reservasDTO.add(reservaDTO);
					}
				}
			}
		}
		return reservasDTO;
	}
	
	@Override
	public Boolean esPerdidaTotal(Long estimacionId) {
		Boolean esPerdidaTotal = Boolean.FALSE;		
		EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, estimacionId);
		ReporteCabina reporteCabina  = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina();
		
		if (EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.DANIOS_MATERIALES, TipoEstimacion.RC_VEHICULO)
				&& EnumUtil.equalsValue(reporteCabina.getEstatus(), EstatusReporteCabina.TERMINADO, 
						EstatusReporteCabina.TRAMITE_SINIESTRO, EstatusReporteCabina.SINIESTRO_REAPERTURADO )) {
		
			List<SiniestroCabina> siniestro = entidadService.findByProperty(SiniestroCabina.class, "reporteCabina.id", reporteCabina.getId());
			
			if (CollectionUtils.isNotEmpty(siniestro)) {
				
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("estatusIndemnizacion", EstatusAutorizacionIndemnizacion.AUTORIZADA.codigo);
				params.put("siniestro.id", siniestro.get(0).getId());
				
				List<IndemnizacionSiniestro> indemnizacion = entidadService.findByProperties(IndemnizacionSiniestro.class, params);
				if (CollectionUtils.isNotEmpty(indemnizacion)) {
					esPerdidaTotal = Boolean.TRUE;
				} 
			}
			
		}
		return esPerdidaTotal;
	}
	
	@Override
	public Boolean permiteCambiarDeducible(Long estimacionId){
		Boolean permite = Boolean.TRUE;
		permite = permiteCambiarDeduciblePorIndemnizacion(estimacionId);
		permite = permiteCambiarDeduciblePorRecuperacion(estimacionId);
		return permite;
	}
	
	private Boolean permiteCambiarDeduciblePorIndemnizacion(Long estimacionId){
		Boolean permite = Boolean.TRUE;
//		Se cambia validacion debido a cambio en deducible orden de compra manual. Ahora permitira cambiar deducible
//		mientras no exista una orden de compra con algun estatus de autorizacion y no solo cuando existia orden de compra en tramite
		permite = !ordenCompraAutorizacionService.tieneAutorizacionesEstimacion(estimacionId);		
		
//		String etapa = perdidaTotalService.obtenerEtapaIndemnizacionPorEstimacion(estimacionId);		
//		if(!StringUtil.isEmpty(etapa) && 
//				etapa.equals(IndemnizacionSiniestro.EtapasIndemnizacion.ORDEN_COMPRA_GENERADA.codigo)){
//			permite = Boolean.FALSE;
//		}
		return permite;
	}
	
	private Boolean permiteCambiarDeduciblePorRecuperacion(Long estimacionId){
		Boolean permite = Boolean.TRUE;
		List<RecuperacionDeducible> recuperaciones = recuperacionDeducibleService.obtenerRecuperacionDeduciblePorEstimacion(estimacionId);
		for(RecuperacionDeducible recuperacion : CollectionUtils.emptyIfNull(recuperaciones)){
			if(!EnumUtil.equalsValue(recuperacion.getEstatus(), Recuperacion.EstatusRecuperacion.REGISTRADO, 
					Recuperacion.EstatusRecuperacion.CANCELADO, Recuperacion.EstatusRecuperacion.INACTIVO)) {
				return Boolean.FALSE;
			}
		}
		return permite;
	}

	private void validarRecuperacionCia(MovimientoCoberturaSiniestro movimiento) {
		
		EstimacionCoberturaReporteCabina estimacion = movimiento.getEstimacionCobertura();
		
		if (EnumUtil.equalsValue(TipoMovimiento.AJUSTE_AUMENTO, movimiento.getTipoMovimiento()) || 
				EnumUtil.equalsValue(TipoMovimiento.ESTIMACION_ORIGINAL, movimiento.getTipoMovimiento()))  {			
			
			List<RecuperacionCompania> companias = recuperacionCiaDao.obtenerRecuperacionActiva(estimacion.getId());

			if (CollectionUtils.isNotEmpty(companias)) {
				
				for (RecuperacionCompania recuperacion : companias) {					
				
					//GENERAR EL COMPLEMENTO
					if (EnumUtil.equalsValue(EstatusRecuperacion.RECUPERADO, recuperacion.getEstatus())) {
						
						RecuperacionCompania recuperacionComplemento = recuperacionCiaService.generaComplementoAutomatico(recuperacion, movimiento.getImporteMovimiento());
						
						if (recuperacionComplemento != null) {
							recuperacionCiaService.notificaAjusteRecuperacion(recuperacionComplemento, movimiento.getImporteMovimiento());		
						}
						
						
					} else if (EnumUtil.equalsValue(EstatusRecuperacion.CANCELADO, recuperacion.getEstatus())
							&& estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() != null 
							&& estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1) {
						//GENERAR RECUPERACION AUTOMATICO
						recuperacionCiaService.generaRecuperacionCiaAutomatico(movimiento.getEstimacionCobertura(), movimiento.getImporteMovimiento());
						
						
					} else if ((EnumUtil.equalsValue(recuperacion.getEstatus(), EstatusRecuperacion.PENDIENTE, EstatusRecuperacion.REGISTRADO))) {
						//Marcar Complemento Pendiente
						
						CartaCompania ultimaCarta = this.recuperacionCiaService.obtenerCartaActiva(recuperacion.getId());
						
						if(EnumUtil.equalsValue(TipoMovimiento.AJUSTE_AUMENTO, movimiento.getTipoMovimiento())
								&& ultimaCarta.getEstatus().equals(CartaCompania.EstatusCartaCompania.PENDIENTE_ELABORACION.getValue())){
							ultimaCarta.setEstatus(CartaCompania.EstatusCartaCompania.REGISTRADO.getValue());
							entidadService.save(ultimaCarta);
						}
						else if(ultimaCarta.getEstatus().equals(CartaCompania.EstatusCartaCompania.ENTREGADO.getValue())){
							ComplementoCompania complemento = new ComplementoCompania();
							complemento.setActivo(Boolean.TRUE);
							complemento.setMonto(movimiento.getImporteMovimiento());
							complemento.setRecuperacion(recuperacion);
							complemento.setFechaCreacion(new Date());
							complemento.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual() != null ? usuarioService.getUsuarioActual().getNombreUsuario() : "MIDAS") ;
							entidadService.save(complemento);
						}
						BigDecimal multiplo = new BigDecimal (recuperacion.getPorcentajeParticipacion()).divide(new BigDecimal (100));
						recuperacion.setImporteInicial(movimiento.getImporteMovimiento().multiply(multiplo).add(recuperacion.getImporteInicial()));
						entidadService.save(recuperacion);
					}	
				}				
			} else {
				if (estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() != null 
						&& estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1) {

					//GENERAR RECUPERACION AUTOMATICO
					recuperacionCiaService.generaRecuperacionCiaAutomatico(movimiento.getEstimacionCobertura(),  movimiento.getImporteMovimiento());
				}
			}
			
		}else{
			List<RecuperacionCompania> companias = recuperacionCiaDao.obtenerRecuperacionActiva(estimacion.getId());

			if (CollectionUtils.isNotEmpty(companias)) {
				
				for (RecuperacionCompania recuperacion : companias) {
					BigDecimal multiplo = new BigDecimal (recuperacion.getPorcentajeParticipacion()).divide(new BigDecimal (100));
					recuperacion.setImporteInicial(movimiento.getImporteMovimiento().multiply(multiplo).add(recuperacion.getImporteInicial()));
					entidadService.save(recuperacion);
				}
			}
			
		}
		
	}
	
	/*private void llenarDatosCompaniaTercero(EstimacionCoberturaReporteCabina estimacion) {
		
		if (((EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion())
				&& (EnumUtil.equalsValue(TipoPaseAtencion.PASE_ATENCION_TALLER, estimacion.getTipoPaseAtencion())
					|| EnumUtil.equalsValue(TipoPaseAtencion.REEMBOLSO_A_CIA, estimacion.getTipoPaseAtencion())))
			|| (EnumUtil.equalsValue(estimacion.getTipoEstimacion(), TipoEstimacion.RC_PERSONA,
					TipoEstimacion.GASTOS_MEDICOS, TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, TipoEstimacion.RC_VIAJERO)
				&&  EnumUtil.equalsValue(TipoPaseAtencion.REEMBOLSO_A_CIA, estimacion.getTipoPaseAtencion())))
				&& estimacion.getTieneCompaniaSeguros()
			) {
			
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("coberturaReporte.id", estimacion.getCoberturaReporteCabina().getId());
			params.put("claveSubCalculo", estimacion.getCveSubCalculo());
			params.put("compania.id", estimacion.getCompaniaSegurosTercero().getId());
			
			List<CompaniaSiniestro> companiaSiniestros = entidadService.findByProperties(CompaniaSiniestro.class, params);
			
			for (CompaniaSiniestro companiaSiniestro : CollectionUtils.emptyIfNull(companiaSiniestros)) {
				estimacion.setNumeroSiniestroTercero(companiaSiniestro.getNumeroSiniestro());
				estimacion.setFolioCompaniaSeguros(companiaSiniestro.getFolioCia());
			}
		}
	}	*/

	@Override
	public void guardarCoberturaMontoRechazo(List<AfectacionCoberturaSiniestroDTO> listAfectacionCoberturaSiniestroDTO, Long idReporteCabina){
		BigDecimal montoRechazosTotal = BigDecimal.ZERO;
		for(AfectacionCoberturaSiniestroDTO dto : listAfectacionCoberturaSiniestroDTO){
			if(dto.getCoberturaReporteCabina() != null
					&& dto.getCoberturaReporteCabina().getId() != null){
				String montoRechazo = dto.getMontoRechazo().toString();
				montoRechazosTotal = montoRechazosTotal.add((dto.getMontoRechazo() != null)? dto.getMontoRechazo() : BigDecimal.ZERO);
				CoberturaReporteCabina cobertura = entidadService.findById(CoberturaReporteCabina.class, dto.getCoberturaReporteCabina().getId());
				if(cobertura.getClaveTipoCalculo().equals(CoberturaReporteCabina.ClaveTipoCalculo.RESPONSABILIDAD_CIVIL.toString())){
					if(!StringUtil.isEmpty(dto.getConfiguracionCalculoCobertura().getCveSubTipoCalculoCobertura())){
						if(!StringUtil.isEmpty(cobertura.getMontoRechazo())){
							String[] montosRC = cobertura.getMontoRechazo().split("\\|");
							boolean subTipoEncontrado = false;
							for(int i = 0; i < montosRC.length; i++){
								if(montosRC[i].contains(dto.getConfiguracionCalculoCobertura().getCveSubTipoCalculoCobertura())){
									montosRC[i] = dto.getConfiguracionCalculoCobertura().getCveSubTipoCalculoCobertura() + ":" + montoRechazo;
									subTipoEncontrado = true;
									break;
								}}
							if(subTipoEncontrado){
								montoRechazo = obtenerMontoRechazo(montosRC);
								
								if(montoRechazo.endsWith("|")){
									montoRechazo = montoRechazo.substring(0, montoRechazo.length()-1);
								}
							}else{
								montoRechazo = cobertura.getMontoRechazo() + ( "|" + dto.getConfiguracionCalculoCobertura().getCveSubTipoCalculoCobertura() + 
									":" + dto.getMontoRechazo());
							}
						}else{
							montoRechazo = "";
							montoRechazo += (dto.getConfiguracionCalculoCobertura().getCveSubTipoCalculoCobertura() + 
									":" + dto.getMontoRechazo());
						}
					}}
				
				cobertura.setMontoRechazo(montoRechazo);
				
				entidadService.save(cobertura);
			}
		}
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("incisoReporteCabina.seccionReporteCabina.reporteCabina.id", idReporteCabina);
		List<AutoIncisoReporteCabina> incisos = entidadService.findByProperties(AutoIncisoReporteCabina.class, params);
		AutoIncisoReporteCabina inciso = incisos.get(0);
		inciso.setMontoDanos(montoRechazosTotal);
		entidadService.save(inciso);
	}
	
	public String obtenerMontoRechazo(String[] montosRC) {
		StringBuilder montoRechazo = new StringBuilder("");
		for(String montoRC : montosRC){
			montoRechazo.append(montoRC).append("|");
		}
		return montoRechazo.toString();
	}

	private void notificarComplementoSpv(EstimacionCoberturaReporteCabina estimacion) {
		try {
			servicioPublicoService.notificarReporteServicioPublico(estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId());
		}catch(Exception e) {
			LOG.error("Error EstimacionCoberturaSiniestroService notificarComplementoSpv ",e);
		}
	}

	@Override
	public Double obtieneValorSumaAsegurada (CoberturaReporteCabina coberturaReporteCabina, AutoIncisoReporteCabina autoInciso) {
		Double valorSumaAsegurada = null;
		
		try {
			IncisoReporteCabina incisoReporteCabina = coberturaReporteCabina.getIncisoReporteCabina();
			SeccionReporteCabina seccionReporteCabina = incisoReporteCabina.getSeccionReporteCabina();
			
			valorSumaAsegurada = fronterizosService.obtieneValorSumaAseguradaPol(seccionReporteCabina.getReporteCabina().getFechaHoraOcurrido(), 
					seccionReporteCabina.getReporteCabina().getPoliza().getCotizacionDTO().getIdToCotizacion(), 
					incisoReporteCabina.getNumeroInciso(), seccionReporteCabina.getSeccionDTO().getTipoValidacionNumSerie(), 
					autoInciso.getNumeroSerie(), autoInciso.getEstadoId(), seccionReporteCabina.getReporteCabina().getId(), 
					coberturaReporteCabina.getValorSumaAseguradaMin(), coberturaReporteCabina.getValorSumaAseguradaMax());
		} catch (Exception e) {
			LOG.error("EstimacionCoberturaSiniestroServiceImpl.obtieneValorSumaAsegurada() --> Ocurrio un error al obtener la suma asegurada", e);
		}
		
		return valorSumaAsegurada;
	}
	
	
}