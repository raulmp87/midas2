package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fuerzaventa.FuerzaDeVentaDao;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.service.fuerzaventa.FuerzaDeVentaService;

@Stateless
public class FuerzaDeVentaServiceImpl implements FuerzaDeVentaService {

	protected FuerzaDeVentaDao fuerzaDeVentaDao;

	public enum TipoFuerzaDeVenta{
		AGENTE("A"), OFICINA("O"), GERENCIA("G"), PROMOTORIA("P"), CENTRO_EMISOR("C");
		TipoFuerzaDeVenta(String tipo) {
			this.tipo = tipo;
		}
		private String tipo;
		public String obtenerTipo(){
			return this.tipo;
		}
	};
	
	@EJB
	public void setFuerzaDeVentaDao(FuerzaDeVentaDao fuerzaDeVentaDao) {
		this.fuerzaDeVentaDao = fuerzaDeVentaDao;
	}
	
	@Override
	public List<RegistroFuerzaDeVentaDTO> listar(Object idCentroOperacion, Object idGerencia, Object idOficina, Object idPromotoria, String tipo){
		return fuerzaDeVentaDao.listar(idCentroOperacion, idGerencia, idOficina, idPromotoria, tipo);
	}
	
	@Override
	public List<RegistroFuerzaDeVentaDTO> listar(Object id, String tipoPadre, String tipoBusqueda){
		List<RegistroFuerzaDeVentaDTO> lista = null;		
		if(id == null && tipoPadre == null){
			lista = listar(null, null, null, null, tipoBusqueda);
		}else if(id != null && tipoPadre != null){
			if(tipoPadre.equals(TipoFuerzaDeVenta.CENTRO_EMISOR.obtenerTipo())){
				lista = listar(id,null,null,null,tipoBusqueda);
			}else if(tipoPadre.equals(TipoFuerzaDeVenta.GERENCIA.obtenerTipo())){
				lista = listar(null,id,null,null,tipoBusqueda);
			}else if(tipoPadre.equals(TipoFuerzaDeVenta.OFICINA.obtenerTipo())){
				lista = listar(null,null,id,null,tipoBusqueda);
			}else if(tipoPadre.equals(TipoFuerzaDeVenta.PROMOTORIA.obtenerTipo())){
				lista = listar(null,null,null,id,tipoBusqueda);
			}			
		}else{
			throw new RuntimeException("Los parametros de busqueda para fuerza de ventas son invalidos");
		}				
		return lista;
	}

}
