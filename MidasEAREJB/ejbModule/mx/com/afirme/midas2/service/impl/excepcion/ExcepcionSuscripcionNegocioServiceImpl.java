package mx.com.afirme.midas2.service.impl.excepcion;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;

import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas2.domain.excepcion.CondicionExcepcion;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.excepcion.ValorExcepcion;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionNegocioDescripcionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoValor;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.util.MailService;

public abstract class ExcepcionSuscripcionNegocioServiceImpl implements
		ExcepcionSuscripcionNegocioService {
	
	protected EntidadService entidad = null;
	
	protected MailService mailService = null;

	protected ExcepcionSuscripcionService excepcionService = null;	
	
	protected SolicitudAutorizacionService solicitudService = null;
	
	protected TipoCambioFacadeRemote tipoCambioService = null;
	
	protected UsuarioService usuarioService = null;
	
	public static final String TITULO_CORREO_EXCEPCION = "Se ha detectado una excepci\u00F3n en la cotizaci\u00F3n"; 
	
	public static final String SALUDO_CORREO_EXCEPCION = "A quien corresponda.";

	@EJB(beanName="UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}	
	
	@EJB
	public void setTipoCambioService(TipoCambioFacadeRemote tipoCambioService) {
		this.tipoCambioService = tipoCambioService;
	}

	@EJB
	public void setSolicitudService(SolicitudAutorizacionService solicitudService) {
		this.solicitudService = solicitudService;
	}

	@EJB
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}

	@EJB
	public void setEntidad(EntidadService entidad) {
		this.entidad = entidad;
	}

	@EJB
	public void setExcepcionService(ExcepcionSuscripcionService excepcionService) {
		this.excepcionService = excepcionService;
	}

	@Override
	public void agregarCondicion(Long idToExcepcion, TipoCondicion tipoCondicion, String valor) {
		this.agregarCondicion(idToExcepcion, tipoCondicion, ExcepcionSuscripcionService.TipoValor.VALOR_DIRECTO, valor);
	}

	@Override
	public void agregarCondicion(Long idToExcepcion, TipoCondicion tipoCondicion, TipoValor tipoValor,
			String... valor) {
		excepcionService.agregarCondicion(idToExcepcion, tipoCondicion, tipoValor, Arrays.asList(valor));
	}
	
	@Override
	public String obtenerCondicionValor(Long idToExcepcion, TipoCondicion tipoCondicion) {
		List<String> valores = this.obtenerCondicionValores(idToExcepcion, tipoCondicion);
		return valores.isEmpty()? null : valores.get(0);
	}

	@Override
	public List<String> obtenerCondicionValores(Long idToExcepcion, TipoCondicion tipoCondicion) {
		List<String> valores = new ArrayList<String>();
		CondicionExcepcion condicion = excepcionService.obtenerCondicion(idToExcepcion, tipoCondicion);
		if(condicion != null){
			for(ValorExcepcion valor : condicion.getValorExcepcions()){
				valores.add(valor.getValor());
			}
		}
		return valores;
	}

	@Override
	public Long salvarExcepcion(ExcepcionSuscripcionNegocioDescripcionDTO excepcion) {
		ExcepcionSuscripcion excepcionSuscripcion = new ExcepcionSuscripcion();
		excepcionSuscripcion.setId(excepcion.getIdExcepcion());
		excepcionSuscripcion.setActivo(excepcion.isActivo());
		excepcionSuscripcion.setCorreoNotificacion(excepcion.getCorreo());
		excepcionSuscripcion.setCveNegocio(excepcion.getCveNegocio());
		excepcionSuscripcion.setHabilitado(excepcion.isHabilitado());
		excepcionSuscripcion.setRequiereAutorizacion(excepcion.isAutorizacion());
		excepcionSuscripcion.setRequiereNotificacion(excepcion.isNotificacion());
		excepcionSuscripcion.setDescripcion(excepcion.getDescripcion());
		return excepcionService.guardarExcepcion(excepcionSuscripcion);
	}
	
	/**
	 * Obtener valores de una condicion dentro de una excepcion. Solo sirve dentro del contenedor
	 * @param excepcion
	 * @param tipoCondicion
	 * @return
	 */
	public List<ValorExcepcion> obtenerValorDeExcepcion(ExcepcionSuscripcion excepcion, TipoCondicion tipoCondicion){
		List<ValorExcepcion> valores = null;
		for(CondicionExcepcion condicion : excepcion.getCondicionExcepcion()){
			if(tipoCondicion == TipoCondicion.tipoCondicion(condicion.getTipoCondicion())){
				valores = condicion.getValorExcepcions(); 
			}
		}
		return valores;
	}

	@Override
	public abstract List<? extends ExcepcionSuscripcionNegocioDescripcionDTO> listarExcepciones(
			String cveNegocio);
	
	@Override
	public abstract ExcepcionSuscripcionNegocioDescripcionDTO obtenerExcepcion(Long idToExcepcion);

	@Override
	public abstract List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BigDecimal idCotizacion);
	
	@Override
	public abstract List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BigDecimal idCotizacion, BigDecimal idLineaNegocio, BigDecimal idInciso);	

	/**
	 * Obtiene el primer valor de una condicion. La lista debe venir lista para ser iterada.
	 * @param condiciones
	 * @param tipoCondicion
	 * @return
	 */
	public String obtenerValor(List<CondicionExcepcion> condiciones, TipoCondicion tipoCondicion){
		String valor = null;
		List<String> valores = obtenerValores(condiciones, tipoCondicion);
		if(!valores.isEmpty()){
			valor = valores.get(0);
		}
		return valor;
	}
	
	/**
	 * Obtiene la lista de valores de una condicion. La lista debe venir lista para ser iterada.
	 * @param condiciones
	 * @param tipoCondicion
	 * @return
	 */
	public List<String> obtenerValores(List<CondicionExcepcion> condiciones, TipoCondicion tipoCondicion){
		List<String> valores = new ArrayList<String>();		
		for(CondicionExcepcion condicion : condiciones){
			if(condicion.getTipoCondicion() == tipoCondicion.valor()){
				List<ValorExcepcion> valoresExcepcion = condicion.getValorExcepcions();
				Collections.sort(valoresExcepcion, 	new Comparator<ValorExcepcion>() {				
					public int compare(ValorExcepcion n1, ValorExcepcion n2){
						return n1.getId().compareTo(n2.getId());
					}
				});
				for(ValorExcepcion valor : valoresExcepcion){
					valores.add(valor.getValor());
				}
			}			
		}		
		return valores;
	}
	
	/**
	 * Rvisa si la lista de condiciones tiene alguna condicion de tipo <code>tipoCondicion</code>
	 * @param condiciones
	 * @param tipoCondicion
	 * @return
	 */
	public boolean contieneCondicion(List<CondicionExcepcion> condiciones, TipoCondicion tipoCondicion){
		boolean contiene = false;
		for(CondicionExcepcion condicion : condiciones){
			if(condicion.getTipoCondicion() == tipoCondicion.valor()){
				contiene = true;
				break;
			}
		}
		return contiene;
	}
	
	public void enviarNotificacion(String recipientes, String titulo, 
			String encabezado, String saludo, String cuerpo ){
		if(recipientes != null && recipientes.length() > 0){
			String[] correos = recipientes.split(";");
			List<String> direcciones = Arrays.asList(correos);	
			mailService.sendMail(direcciones, encabezado, cuerpo, null, titulo, saludo);
		}
	}
	
	public String generarCuerpoMensajeNotificacion(ExcepcionSuscripcionReporteDTO reporte){
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy HH:MM");
		StringBuilder cuerpo = new StringBuilder("");
		cuerpo.append("Excepci\u00F3n encontrada: " + reporte.getDescripcionExcepcion() + "<br/>");
		cuerpo.append("<ul><li>Cotizaci\u00F3n: " + reporte.getIdToCotizacion() + "</li></ul>");
		cuerpo.append("<ul><li>Secci\u00F3n (L\u00EDnea): " + reporte.getLineaNegocio() + "</li></ul>");
		cuerpo.append("<ul><li>Inciso: " + reporte.getSequenciaInciso() + "</li></ul>");
		if(reporte.getCobertura() != null){
			cuerpo.append("<ul><li>Cobertura: " + reporte.getCobertura() != null ? reporte.getCobertura():"NA" + "</li></ul>");
		}
		cuerpo.append("<ul><li>Error encontrado: " + reporte.getDescripcionRegistroConExcepcion() + "</li></ul>");
		cuerpo.append("<ul><li>Fecha: " + format.format(new Date()) + "</li></ul>");		
		return cuerpo.toString();
	}

	
}
