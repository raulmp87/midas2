package mx.com.afirme.midas2.service.impl.negocio.recuotificacion;

import java.math.BigDecimal;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.negocio.recuotificacion.NegocioRecuotificacionDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacion;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutProgPago;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutRecibo;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica.Status;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionAutomatica.TipoVigencia;
import mx.com.afirme.midas2.domain.negocio.recuotificacion.NegocioRecuotificacionUsuario;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.recuotificacion.NegocioRecuotificacionService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;

@Stateless
public class NegocioRecuotificacionServiceImpl implements NegocioRecuotificacionService {

	private static final long serialVersionUID = -1584749935785164256L;

	public static final Logger LOG = Logger.getLogger(NegocioRecuotificacionServiceImpl.class);
	
	public static final BigDecimal MAX_PCTE = BigDecimal.valueOf(100d);
	
	public static final int DIAS_ANUAL = 365;
	
	public static final int DIAS_SEMESTRAL = 183;
	
	private static final String  PROP_ENTIDAD_NEGOCIO ="negRecuotificacion.negocio.idToNegocio";
	
	private static final String PROP_ENTIDAD_AUTOMATICA = "automatica.id";
	
	private static final String PROP_ENTIDAD_PROGRAMA = "progPago.id";
	
	private static final String PROP_ENTIDAD_NEGOCIO_CLEAN = "negocio.idToNegocio";
	
	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private NegocioRecuotificacionDao negocioRecuotificacionDao;
	
	@Override
	public void activarRecuotificacionAutomatica(Long id, boolean activar) {
		NegocioRecuotificacion recuotificacion = obtenerRecuotificacion(id);
		recuotificacion.setActivarAutomatica(activar);
		guardarNegocioRecuotificacion(recuotificacion);
	}

	@Override
	public void activarRecuotificacionManual(Long id, boolean activar) {
		NegocioRecuotificacion recuotificacion = obtenerRecuotificacion(id);
		recuotificacion.setActivarManual(activar);
		guardarNegocioRecuotificacion(recuotificacion);
	}

	@Override
	public void activarRecuotificacionPrimaTotal(Long id, boolean activar) {
		NegocioRecuotificacion recuotificacion = obtenerRecuotificacion(id);
		recuotificacion.setActivarModificacionPrimaTotal(activar);
		guardarNegocioRecuotificacion(recuotificacion);
	}

	@Override
	public void modificarRecuotificacionPrimaTotal(Long id, BigDecimal total) {
		NegocioRecuotificacion recuotificacion = obtenerRecuotificacion(id);
		recuotificacion.setMontoPrimaTotal(total);
		guardarNegocioRecuotificacion(recuotificacion);
	}
		
	@Override
	public NegocioRecuotificacion obtener(Long idToNegocio){
		List<NegocioRecuotificacion> recuotificaciones =  entidadService.findByProperty(
				NegocioRecuotificacion.class, PROP_ENTIDAD_NEGOCIO_CLEAN, idToNegocio);
		if(recuotificaciones != null && !recuotificaciones.isEmpty()){
			//agregar valor de ultima modificacion
			return recuotificaciones.get(0);
		}
		NegocioRecuotificacion recuotificacion = new NegocioRecuotificacion();
		recuotificacion.setNegocio(entidadService.findById(Negocio.class, idToNegocio));
		guardarNegocioRecuotificacion(recuotificacion);		
		return recuotificacion;
	}
	
	private NegocioRecuotificacion guardarNegocioRecuotificacion(NegocioRecuotificacion recuotificacion){		
		if(StringUtil.isEmpty(recuotificacion.getCodigoUsuarioCreacion())){
			recuotificacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());		
			recuotificacion.setFechaCreacion(new Date());
		}
		recuotificacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		recuotificacion.setFechaModificacion(new Date());
		Long id =(Long)entidadService.saveAndGetId(recuotificacion);
		recuotificacion.setId(id);
		return recuotificacion;
	}
	
	private NegocioRecuotificacion obtenerRecuotificacion(Long negocioRecuotificacionId){
		if(negocioRecuotificacionId != null){
			return entidadService.findById(NegocioRecuotificacion.class, negocioRecuotificacionId);
		}		
		return new NegocioRecuotificacion();				
	}

	@Override
	public List<NegocioRecuotificacionUsuario> obtenerUsuariosDisponibles(
			Long idToNegocio) {
		return negocioRecuotificacionDao.obtenerUsuariosDisponibles(idToNegocio);
	}

	@Override
	public List<NegocioRecuotificacionUsuario> obtenerUsuariosAsociados(Long idToNegocio) {
		return negocioRecuotificacionDao.obtenerUsuariosAsociados(idToNegocio);
	}

	@Override
	public void relacionar(String accion, NegocioRecuotificacionUsuario usuario){		
		if(StringUtil.isEmpty(usuario.getCodigoUsuarioCreacion())){
			usuario.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());		
			usuario.setFechaCreacion(new Date());
		}
		usuario.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		usuario.setFechaModificacion(new Date());		
		entidadService.executeActionGrid(accion, usuario);		
	}

	@Override
	public void asociarTodos(Long idToNegocio) {
		List<NegocioRecuotificacionUsuario> disponibles = obtenerUsuariosDisponibles(idToNegocio);
		for(NegocioRecuotificacionUsuario usuario : disponibles){
			relacionar("inserted", usuario);
		}		
	}

	@Override
	public void desasociarTodos(Long idToNegocio) {
		List<NegocioRecuotificacionUsuario> asociados = obtenerUsuariosAsociados(idToNegocio);
		for(NegocioRecuotificacionUsuario usuario : asociados){
			relacionar("deleted", usuario);
		}		
	}

	@Override
	public void activarVersion(Long versionId) {
		NegocioRecuotificacionAutomatica version = entidadService.findById(NegocioRecuotificacionAutomatica.class, versionId);
		BigDecimal saldo = obtenerSaldo(versionId, TipoSaldo.PCTE_PROGPAGO);
		if(saldo.compareTo(BigDecimal.ZERO) != 0){
			throw new NegocioEJBExeption("NEGREC0004 ", " No puede realizarse la activación porque existe saldo por distribuir en los programas de pago");		
		}else{
			if(obtenerSaldo(versionId, TipoSaldo.PCTE_VERSION_DERECHOS).compareTo(BigDecimal.ZERO) != 0){
				throw new NegocioEJBExeption("NEGREC0004 ", 
						" No puede realizarse la activación porque existen derechos por distribuir en los recibos de los programas de pago");	
			}
			for(NegocioRecuotificacionAutProgPago prog : version.getProgramas()){
				if(obtenerSaldo(prog.getId(), TipoSaldo.PCTE_RECIBOS_PRIMA).compareTo(BigDecimal.ZERO) != 0|| 						
							obtenerSaldo(prog.getId(), TipoSaldo.DIAS_DURACION).compareTo(BigDecimal.ZERO) != 0){
					throw new NegocioEJBExeption("NEGREC0004 ", 
							" No puede realizarse la activación porque existe saldo por distribuir en los recibos de los programas de pago");					
				}				
			}
		}
		desactivarVersiones(version.getNegRecuotificacion().getNegocio().getIdToNegocio());
		version.setStatus(Status.ACTIVO);
		guardarNegocioRecuotificacionVersion(version);
	}

	@Override
	public NegocioRecuotificacionAutomatica nuevaVersion(Long idToNegocio) {
		desactivarVersiones(idToNegocio);
		return generarNuevaVersion(idToNegocio);	
	}
		
	private void desactivarVersiones(Long idToNegocio){
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_NEGOCIO, idToNegocio);
		List<NegocioRecuotificacionAutomatica> versiones = entidadService.findByProperties(NegocioRecuotificacionAutomatica.class, params);
		for(NegocioRecuotificacionAutomatica version : versiones){
			version.setStatus(Status.INACTIVO);
			guardarNegocioRecuotificacionVersion(version);			
		}		
	}
	

	@Override
	public NegocioRecuotificacionAutProgPago nuevoProgramaPago(Long versionId) {
		NegocioRecuotificacionAutProgPago prog;		
		BigDecimal pcte = obtenerTotal(versionId, TipoSaldo.PCTE_PROGPAGO);		
		if(pcte.compareTo(MAX_PCTE) == 0){
			throw new NegocioEJBExeption("NEGREC0001 ", "Se requiere que exista saldo para generar un nuevo programa");
		}else{
			String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
			prog = new NegocioRecuotificacionAutProgPago(codigoUsuario);
			prog.setPctDistribucionPrima(MAX_PCTE.subtract(pcte).doubleValue());
			prog.setNumProgPago(obtenerMaxNumProgPago(versionId));
			prog.setCodigoUsuarioCreacion(codigoUsuario);
			prog.setFechaCreacion(new Date());	
			prog.setAutomatica(entidadService.findById(NegocioRecuotificacionAutomatica.class, versionId));
			prog.setId((Long)entidadService.saveAndGetId(prog));
			generarRecibo(prog.getId());
		}
		return prog;
	}

	@Override
	public List<NegocioRecuotificacionAutProgPago> obtenerProgramasPago(
			Long versionId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(PROP_ENTIDAD_AUTOMATICA, versionId);		
		List<NegocioRecuotificacionAutProgPago> programas =  entidadService.findByPropertiesWithOrder(
				NegocioRecuotificacionAutProgPago.class, params, "numProgPago");
		for(NegocioRecuotificacionAutProgPago programa : programas){
			programa.setPcteDerechosTotales(obtenerTotal(programa.getId(), TipoSaldo.PCTE_PROGRAMA_DERECHOS).doubleValue());
		}
		return programas;
	}

	@Override
	public void modificarProgramaPago(NegocioRecuotificacionAutProgPago progPago) {
		NegocioRecuotificacionAutProgPago original = entidadService.findById(NegocioRecuotificacionAutProgPago.class, progPago.getId());	
		BigDecimal saldo = obtenerSaldo(progPago.getAutomatica().getId(), TipoSaldo.PCTE_PROGPAGO);
		Double movimiento = original.getPctDistribucionPrima() -  progPago.getPctDistribucionPrima();		
		int comparacion = original.getPctDistribucionPrima().compareTo(progPago.getPctDistribucionPrima());
		if(progPago.getPctDistribucionPrima().compareTo(0d) == 0){
			throw new NegocioEJBExeption("NEGREC0003 ", "No puede disminuir a cero el monto");
		}else if(comparacion < 0 && BigDecimal.valueOf(movimiento).abs().compareTo(saldo) > 0){
			throw new NegocioEJBExeption("NEGREC0002 ", "No puede se puede tener un saldo negativo, disminuya la cantidad a modificar");
		}else if(!BigDecimal.valueOf(movimiento).abs().equals(saldo)){
			desactivarVersion(progPago.getAutomatica().getId());
		}
		entidadService.save(progPago);		
	}

	@Override
	public void eliminarProgramaPago(Long progPagoId) {
		//eliminar
		NegocioRecuotificacionAutProgPago programa = entidadService.findById(NegocioRecuotificacionAutProgPago.class, progPagoId);	
		Long versionId = programa.getAutomatica().getId();
		entidadService.remove(programa);
		//reordenar
		reordenarProgramas(versionId);
		//desacticar
		desactivarVersion(programa.getAutomatica().getId());
	}
	
	private void reordenarProgramas(Long versionId){
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_AUTOMATICA, versionId);
		List<NegocioRecuotificacionAutProgPago> programas = entidadService.findByPropertiesWithOrder(
				NegocioRecuotificacionAutProgPago.class, params, "id");
		int i = 0;
		for(NegocioRecuotificacionAutProgPago prog : programas){
			prog.setNumProgPago(++i);
			entidadService.save(prog);
		}		
	}

	/**
	 * Obtener version activa. Si no existe obtener la ultima version. Si no existe, crear una nueva version activa.
	 */
	@Override
	public NegocioRecuotificacionAutomatica obtenerVersionActiva(
			Long idToNegocio) {
		NegocioRecuotificacionAutomatica version;
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("status", NegocioRecuotificacionAutomatica.Status.ACTIVO);
		params.put(PROP_ENTIDAD_NEGOCIO, idToNegocio);
		List<NegocioRecuotificacionAutomatica> versiones = 
			entidadService.findByProperties(NegocioRecuotificacionAutomatica.class, params);
		if(!versiones.isEmpty()){
			version = versiones.get(0);
		}else{
			params = new HashMap<String, Object>();
			params.put(PROP_ENTIDAD_NEGOCIO, idToNegocio);
			versiones = entidadService.findByPropertiesWithOrder(NegocioRecuotificacionAutomatica.class, 
					params, "id desc");
			if(!versiones.isEmpty()){
				version = versiones.get(0);
			}else{
				version = generarNuevaVersion(idToNegocio);
			}
		}
		return version;
	}
	
	
	private NegocioRecuotificacionAutomatica generarNuevaVersion(Long idToNegocio){
		NegocioRecuotificacionAutomatica version = new NegocioRecuotificacionAutomatica(usuarioService.getUsuarioActual().getNombreUsuario());
		version.setNegRecuotificacion((entidadService.findByProperty(
				NegocioRecuotificacion.class, PROP_ENTIDAD_NEGOCIO_CLEAN, idToNegocio)).get(0));					
		version.setVersion(obtenerMaxNumeroVersion(idToNegocio));
		version.setStatus(NegocioRecuotificacionAutomatica.Status.ACTIVO);	
		version.setTipoVigencia(TipoVigencia.ANUAL);
		version = guardarNegocioRecuotificacionVersion(version);		
		nuevoProgramaPago(version.getId());
		return version;		
	}
	
	private int obtenerMaxNumeroVersion(Long idToNegocio){
		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put(PROP_ENTIDAD_NEGOCIO, idToNegocio);
		return (Integer)entidadService.getIncrementedProperty(
				NegocioRecuotificacionAutomatica.class, "version", filter); 
	}
	
	private int obtenerMaxNumProgPago(Long versionId){
		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put(PROP_ENTIDAD_AUTOMATICA, versionId);
		return (Integer)entidadService.getIncrementedProperty(
				NegocioRecuotificacionAutProgPago.class, "numProgPago", filter);
	}
	
	private NegocioRecuotificacionAutomatica guardarNegocioRecuotificacionVersion(NegocioRecuotificacionAutomatica version){		
		if(StringUtil.isEmpty(version.getCodigoUsuarioCreacion())){
			version.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());		
			version.setFechaCreacion(new Date());
		}
		version.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		version.setFechaModificacion(new Date());
		Long id =(Long)entidadService.saveAndGetId(version);
		version.setId(id);	
		return version;
	}
	
	
	@Override
	public BigDecimal obtenerTotal(Long id, TipoSaldo tipo){		
		Double total = 0d;
		switch(tipo){
			case PCTE_PROGPAGO:				
				NegocioRecuotificacionAutomatica aut = entidadService.findByProperty(NegocioRecuotificacionAutomatica.class, "id", id).get(0);
				for(NegocioRecuotificacionAutProgPago prog : aut.getProgramas()){
					total += prog.getPctDistribucionPrima();
				}				
				break;
			case PCTE_PROGRAMA_DERECHOS:	
				NegocioRecuotificacionAutProgPago progPago = entidadService.findById(NegocioRecuotificacionAutProgPago.class, id);
				for(NegocioRecuotificacionAutRecibo recibo : progPago.getRecibos()){
					total += recibo.getPctDistDerechos();									
				}
				break;
			case PCTE_VERSION_DERECHOS:		
				NegocioRecuotificacionAutomatica automatica = entidadService.findById(NegocioRecuotificacionAutomatica.class, id);			
				for(NegocioRecuotificacionAutProgPago prog : automatica.getProgramas()){
					total += sumarTotalPctDistDerechos(prog.getRecibos());
				}							
				break;
			case PCTE_RECIBO_DERECHOS:
				NegocioRecuotificacionAutProgPago programa = entidadService.findById(NegocioRecuotificacionAutProgPago.class, id);				
				for(NegocioRecuotificacionAutProgPago prog : programa.getAutomatica().getProgramas()){
					total += sumarTotalPctDistDerechos(prog.getRecibos());
				}				
				break;				
			case PCTE_RECIBOS_PRIMA:				
				NegocioRecuotificacionAutProgPago progRecibos = entidadService.findByProperty(NegocioRecuotificacionAutProgPago.class, "id", id).get(0);
				for(NegocioRecuotificacionAutRecibo recibo : progRecibos.getRecibos()){
					total += recibo.getPctDistPrimaNeta();
				}
				break;
			case DIAS_DURACION:
				NegocioRecuotificacionAutProgPago progDias = entidadService.findByProperty(NegocioRecuotificacionAutProgPago.class, "id", id).get(0);
				for(NegocioRecuotificacionAutRecibo recibo : progDias.getRecibos()){
					total += recibo.getDiasDuracion();
				}
				break;
			default:
				break;
		}		
		return BigDecimal.valueOf(total);
	}
	
	private Double sumarTotalPctDistDerechos(List<NegocioRecuotificacionAutRecibo> recibos){
		Double total = 0d;
		for(NegocioRecuotificacionAutRecibo recibo : recibos){
			total += recibo.getPctDistDerechos();									
		}
		return total;
	}
	
	@Override
	public BigDecimal obtenerSaldo(Long id, TipoSaldo tipo){		
		BigDecimal total = obtenerTotal(id, tipo);	
		if(tipo.equals(TipoSaldo.DIAS_DURACION)){
			NegocioRecuotificacionAutProgPago progPago = entidadService.findById(NegocioRecuotificacionAutProgPago.class, id);
			if(progPago.getAutomatica().getTipoVigencia().equals(TipoVigencia.ANUAL)){
				return BigDecimal.valueOf(DIAS_ANUAL - total.doubleValue());
			}else{
				return BigDecimal.valueOf(DIAS_SEMESTRAL - total.doubleValue());
			}
		}else{
			return MAX_PCTE.subtract(total);
		}		
	}

	@Override
	public Map<Long, Integer> obtenerListadoVersiones(Long idToNegocio) {
		Map<Long, Integer> versiones = new LinkedHashMap<Long, Integer>();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(PROP_ENTIDAD_NEGOCIO, idToNegocio);
		List<NegocioRecuotificacionAutomatica> automaticas = entidadService.findByPropertiesWithOrder(
				NegocioRecuotificacionAutomatica.class, params, "id");
		for(NegocioRecuotificacionAutomatica version: automaticas){
			versiones.put(version.getId(), version.getVersion());
		}
		return versiones;
	}

	@Override
	public NegocioRecuotificacionAutomatica obtenerVersion(Long version) {	
		return entidadService.findById(NegocioRecuotificacionAutomatica.class, version);
	}

	@Override
	public NegocioRecuotificacionAutProgPago obtenerPrograma(Long programaId) {	
		return entidadService.findById(NegocioRecuotificacionAutProgPago.class, programaId);
	}

	@Override
	public void eliminarRecibo(Long reciboId) {
		//eliminar
		NegocioRecuotificacionAutRecibo recibo = entidadService.findById(NegocioRecuotificacionAutRecibo.class, reciboId);	
		Long progPagoId = recibo.getProgPago().getId();
		entidadService.remove(recibo);
		//reordenar
		reordenarRecibos(progPagoId);
		//desacticar
		desactivarVersion(recibo.getProgPago().getAutomatica().getId());
	}
	
	private void reordenarRecibos(Long progPagoId){
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put(PROP_ENTIDAD_PROGRAMA, progPagoId);
		List<NegocioRecuotificacionAutRecibo> recibos = entidadService.findByPropertiesWithOrder(
				NegocioRecuotificacionAutRecibo.class, params, "id");
		int i = 0;
		for(NegocioRecuotificacionAutRecibo rec : recibos){
			rec.setNumExhibicion(++i);
			entidadService.save(rec);
		}
	}
	
	private void desactivarVersion(Long versionId){
		NegocioRecuotificacionAutomatica version = entidadService.findById(
				NegocioRecuotificacionAutomatica.class, versionId);
		version.setStatus(Status.INACTIVO);
		entidadService.save(version);
	}
	

	@Override
	public NegocioRecuotificacionAutRecibo generarRecibo(Long progPagoId) {
		NegocioRecuotificacionAutRecibo recibo;				
		BigDecimal pctePrima = obtenerTotal(progPagoId, TipoSaldo.PCTE_RECIBOS_PRIMA);	
		BigDecimal saldoDias = obtenerSaldo(progPagoId, TipoSaldo.DIAS_DURACION);
		if(pctePrima.compareTo(MAX_PCTE) == 0 || saldoDias.compareTo(BigDecimal.ZERO) == 0){
			throw new NegocioEJBExeption("NEGREC0001 ", "Se requiere que exista saldo para generar un nuevo recibo");
		}else{
			NegocioRecuotificacionAutProgPago programa = obtenerPrograma(progPagoId);
			String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
			recibo = new NegocioRecuotificacionAutRecibo();
			recibo.setPctDistPrimaNeta(MAX_PCTE.subtract(pctePrima).doubleValue());
			recibo.setPctDistDerechos(MAX_PCTE.subtract(obtenerTotal(progPagoId, TipoSaldo.PCTE_RECIBO_DERECHOS)).doubleValue());
			recibo.setNumExhibicion(obtenerMaxNumExhibicion(progPagoId));
			recibo.setCodigoUsuarioCreacion(codigoUsuario);
			recibo.setFechaCreacion(new Date());
			recibo.setDiasDuracion(saldoDias.intValue());
			recibo.setProgPago(programa);
			recibo.setId((Long)entidadService.saveAndGetId(recibo));
		}
		return recibo;
	}
	
	private int obtenerMaxNumExhibicion(Long progPagoId){
		Map<String, Object> filter = new HashMap<String, Object>();
		filter.put(PROP_ENTIDAD_PROGRAMA, progPagoId);
		return (Integer)entidadService.getIncrementedProperty(
				NegocioRecuotificacionAutRecibo.class, "numExhibicion", filter); 
	}

	@Override
	public List<NegocioRecuotificacionAutRecibo> obtenerRecibos(Long progPagoId) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put(PROP_ENTIDAD_PROGRAMA, progPagoId);		
		return entidadService.findByPropertiesWithOrder(NegocioRecuotificacionAutRecibo.class, params, "numExhibicion");				
	}

	@Override
	public void modificarRecibo(NegocioRecuotificacionAutRecibo recibo, TipoSaldo tipo) {
		NegocioRecuotificacionAutRecibo original = entidadService.findById(NegocioRecuotificacionAutRecibo.class, recibo.getId());
		BigDecimal saldo = obtenerSaldo(recibo.getProgPago().getId(), tipo);
		BigDecimal movimiento;
		BigDecimal montoModificar;
		int comparacion;
		if(tipo.equals(TipoSaldo.PCTE_RECIBOS_PRIMA)){		
			montoModificar = BigDecimal.valueOf(recibo.getPctDistPrimaNeta());
			movimiento = BigDecimal.valueOf(original.getPctDistPrimaNeta()).subtract(montoModificar);				
			comparacion = BigDecimal.valueOf(original.getPctDistPrimaNeta()).compareTo(montoModificar);
		}else if(tipo.equals(TipoSaldo.PCTE_RECIBO_DERECHOS)){
			montoModificar = BigDecimal.valueOf(recibo.getPctDistDerechos());
			movimiento = BigDecimal.valueOf(original.getPctDistDerechos()).subtract(montoModificar);
			comparacion = BigDecimal.valueOf(original.getPctDistDerechos()).compareTo(montoModificar);
		}else{
			montoModificar = BigDecimal.valueOf(recibo.getDiasDuracion().doubleValue());
			movimiento = BigDecimal.valueOf(original.getDiasDuracion().longValue() - montoModificar.longValue());
			comparacion = original.getDiasDuracion().compareTo(montoModificar.intValue());
		}	
		if(!tipo.equals(TipoSaldo.PCTE_RECIBO_DERECHOS) && montoModificar.compareTo(BigDecimal.ZERO) == 0){
			throw new NegocioEJBExeption("NEGREC0003 ", "No puede disminuir a cero el monto");
		}else if(comparacion < 0 && movimiento.abs().compareTo(saldo) > 0){
			throw new NegocioEJBExeption("NEGREC0002 ", "No puede se puede tener un saldo negativo, disminuya la cantidad a modificar");
		}else if(!movimiento.abs().equals(saldo)){
			desactivarVersion(recibo.getProgPago().getAutomatica().getId());
		}
		entidadService.save(recibo);				
	}

	@Override
	public boolean validarPermisoRecuotificacionManual(Long negocioId,
			Long usuarioId) {
		NegocioRecuotificacion negRec = obtenerNegocioRecuotificacion(negocioId);
		boolean valido = negRec != null && negRec.getActivarManual();
		if(valido){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("recuotificacion.negocio.idToNegocio", negocioId);
			params.put("id.usuarioId", usuarioId);
			List<NegocioRecuotificacionUsuario> usuarios = entidadService.findByProperties(NegocioRecuotificacionUsuario.class, params);
			if(usuarios == null || usuarios.isEmpty()){
				valido = false;
			}
		}
		return valido;
	}

	/**
	 * Obtener negocio recuotificaicon por id negocio. 
	 * @param idToNegocio
	 * @return negocio recuotficacion o <code>null</code> si no existe
	 */
	@Override
	public NegocioRecuotificacion obtenerNegocioRecuotificacion(Long idToNegocio){
		List<NegocioRecuotificacion> listado = 
			entidadService.findByProperty(NegocioRecuotificacion.class, "negocio.idToNegocio", idToNegocio);
		if(listado != null && !listado.isEmpty()){
			return listado.get(0);
		}
		return null;
	}
}
