package mx.com.afirme.midas2.service.impl.siniestros.recuperacion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.RecuperacionDao;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.catalogos.banco.ConfigCuentaMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.CoberturaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.PaseRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.MedioRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.TipoRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCruceroJuridica;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSipac;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ReferenciaBancaria;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.InstructivoDepositoDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.InstructivoDepositoListadoBancosDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.RecuperacionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.MidasException;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.lang.StringUtils;


/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 12:37:18 p.m.
 */
@Stateless
public class RecuperacionServiceImpl implements RecuperacionService {
	
	private static final String ARCHIVO_ADJUNTO = "InstructivoDeposito.pdf";
	
	//se agregan los servicios que pueden ser de utilidad . 
	@EJB
	protected EstimacionCoberturaSiniestroService estimacionService;

	@EJB
	protected EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	protected UsuarioService usuarioService;

	@EJB
	protected IngresoService ingresoService;
	
	@EJB
	protected CatalogoGrupoValorService  catalogoGrupoValorService;
	
	@EJB
	protected BitacoraService bitacoraService;
	
	@EJB
	private ListadoService  listadoService;
	
	@EJB
	private RecuperacionDao recuperacionDao;
	
	private static final String TIENE_LIQUIDACION_ERROR_CODE = "TIENE_LIQ ";
	
	private static final String TIENE_INGRESO_ERROR_CODE = "TIENE_ING_APP";
	
	private static final String INSTRUCTIVO_DEPOSITO =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/InstructivoDeposito.jrxml";
	private static final String INSTRUCTIVO_DEPOSITO_LISTADO_BANCOS =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/InstructivoDeposito_ListadoBancos.jrxml";

	/**
	 * 
	 * @param filtro
	 */
	@Override
	public List<RecuperacionDTO> buscarRecuperaciones(RecuperacionDTO filtro){
		return this.recuperacionDao.buscarRecuperaciones(filtro);
	}


	/**

	 * </b>Metodo que cancela una Recuperacion 
	 * 
	 * @param recuperacionId
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cancelarRecuperacion(Long recuperacionId, String motivoCancelacion){
		Long numeroRecuperacion = new Long(0);
		try{
			Recuperacion recuperacion = entidadService.findById(Recuperacion.class, recuperacionId);
			numeroRecuperacion = recuperacion.getNumero();
			if(validarRecuperacionLigadaConLiquidacion(recuperacion)){
				throw new NegocioEJBExeption(TIENE_LIQUIDACION_ERROR_CODE, "No se pudo cancelar la recuperación con el número: " + 
						numeroRecuperacion + " debido a que esta ligada con liquidaciones");
			}			
			if(!cancelarIngreso(recuperacion.getId())){
				throw new NegocioEJBExeption(TIENE_INGRESO_ERROR_CODE, "No se pudo cancelar la recuperación con el número: " 
						+ numeroRecuperacion + " debido a que cuenta con un ingreso aplicado");
			}			
			recuperacion.setEstatus(Recuperacion.EstatusRecuperacion.CANCELADO.toString());
			recuperacion.setMotivoCancelacion(motivoCancelacion);
			recuperacion.setFechaCancelacion(new Date());
			recuperacion.setFechaModificacion(new Date());
			bitacoraService.registrar(Bitacora.TIPO_BITACORA.RECUPERACION, Bitacora.EVENTO.CANCELAR_RECUPERACION, 
					recuperacionId.toString(), "Se cancela la recuperacion:" + recuperacionId, recuperacion, 
					usuarioService.getUsuarioActual().getNombreUsuario());
			recuperacion.setCodigoUsuarioCancelacion(usuarioService.getUsuarioActual().getNombreUsuario());
			this.entidadService.save(recuperacion);			
		}catch(Exception ex){
			if( ex instanceof NegocioEJBExeption){
				throw new NegocioEJBExeption(((NegocioEJBExeption) ex).getErrorCode(), ((NegocioEJBExeption) ex).getMessageClean());
			}  					
			throw new NegocioEJBExeption("RECUP_CANC00","No se pudo cancelar la recuperacion con el número: " + numeroRecuperacion);
		}
	}
	
	private boolean cancelarIngreso(Long id){		
		Ingreso ingreso = obtenerIngresoActivo(id);
		if(ingreso != null){
			if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.APLICADO.toString())){
				return false; //no se puede cancelar ingreso
			}else if(ingreso.getEstatus().equals(Ingreso.EstatusIngreso.PENDIENTE.toString())){
				ingresoService.cancelarIngresoPendientePorAplicar(ingreso, "Por cancelación de recuperación");
			}			
		}
		return true;
	}
	
	
	/**
	 * valida si la recuperacion esta relacionada con una liquidacion para los tipos de recuperacion de salvamento, proveedor y deducible
	 * @param recuperacion
	 * @return 	true si esta ligada con alguna liquidacion, de lo contrario false
	 */
	private boolean validarRecuperacionLigadaConLiquidacion(Recuperacion recuperacion){
		boolean validacion = false;
		
		if(recuperacion.getTipo().compareTo(Recuperacion.TipoRecuperacion.PROVEEDOR.getValue()) == 0){
			RecuperacionProveedor recuperacionProveedor = (RecuperacionProveedor)recuperacion;
			if(recuperacionProveedor.getLiquidacion() != null){
				validacion = true;
			}
		}else if(recuperacion.getTipo().compareTo(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue()) == 0){
			validacion = this.tieneLiquidacionAsociada((RecuperacionSalvamento)recuperacion);
		}else if(recuperacion.getTipo().compareTo(Recuperacion.TipoRecuperacion.DEDUCIBLE.getValue()) == 0){
			validacion = this.tieneLiquidacionAsociada((RecuperacionDeducible)recuperacion);
		}
		
		return validacion;
	}
	
	/**
	 * valida si una recuperacion de salvamento esta ligado con una liquidacion
	 * @param recuperacion
	 * @return
	 */
	private boolean tieneLiquidacionAsociada(RecuperacionSalvamento recuperacion){	
		boolean tieneLiquidacionAsociada = false;
		Long estimacionId = recuperacion.getEstimacion().getId();
		List<OrdenPagoSiniestro> ordenesPago = entidadService.findByProperty(OrdenPagoSiniestro.class, "ordenCompra.idTercero", estimacionId);
		if(ordenesPago!=null && !ordenesPago.isEmpty()){
			for(OrdenPagoSiniestro orden : ordenesPago){
				if(orden.getLiquidacion() != null){
					return true;
				}
			}
		}
		return tieneLiquidacionAsociada;
	}
	
	/**
	 * valida si una recuperacion de deducible esta asociado a una liquidacion
	 * @param recuperacion
	 * @return
	 */
	private boolean tieneLiquidacionAsociada(RecuperacionDeducible recuperacion){
		boolean tieneLiquidacionAsociada = false;
		Long estimacionId = recuperacion.getEstimacionCobertura().getId();
		List<OrdenPagoSiniestro> ordenesPago = entidadService.findByProperty(OrdenPagoSiniestro.class, "ordenCompra.idTercero", estimacionId);
		if(ordenesPago!=null && !ordenesPago.isEmpty()){
			for(OrdenPagoSiniestro orden : ordenesPago){
				if(orden.getLiquidacion() != null){
					return true;
				}
			}
		}
		return tieneLiquidacionAsociada;
	}

	/**
	 * Metodo que genera un Ingreso en base a una Recuperacion
	 * 
	 * @param recuperacion
	 */
	@Override
	public Ingreso generarIngreso(Recuperacion recuperacion){		
		Ingreso ingreso = obtenerIngresoActivo(recuperacion.getId());		
		
		if(ingreso == null){		
			ingreso = new Ingreso();		
			
			if (EnumUtil.equalsValue(TipoRecuperacion.PROVEEDOR, recuperacion.getTipo())){
				 RecuperacionProveedor recuperacionProveedor= ((RecuperacionProveedor) recuperacion);
				 if(null !=recuperacionProveedor   ){
					if(recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.VENTA.toString())){
						ingreso.setMonto(recuperacionProveedor.getMontoVenta());
					}else if(recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.REEMBOLSO.toString())  ||  recuperacion.getMedio().equalsIgnoreCase(Recuperacion.MedioRecuperacion.NOTACRED.toString())   ){
						ingreso.setMonto(recuperacionProveedor.getMontoTotal());
					}
				}
			
			}else if(EnumUtil.equalsValue(TipoRecuperacion.CRUCEROJURIDICA, recuperacion.getTipo())){
				RecuperacionCruceroJuridica recuperacionCrucero = (RecuperacionCruceroJuridica) recuperacion;
				ingreso.setMonto(recuperacionCrucero.getMontoFinal());
			}else if(EnumUtil.equalsValue(TipoRecuperacion.DEDUCIBLE, recuperacion.getTipo())){
				RecuperacionDeducible deducible = (RecuperacionDeducible)recuperacion;
				ingreso.setMonto(deducible.getMontoFinalDeducible());
			}else if(EnumUtil.equalsValue(TipoRecuperacion.COMPANIA, recuperacion.getTipo())){
				RecuperacionCompania compania = (RecuperacionCompania)recuperacion;
				ingreso.setMonto(compania.getImporte());
			}else if(EnumUtil.equalsValue(TipoRecuperacion.SALVAMENTO, recuperacion.getTipo())){
				RecuperacionSalvamento salvamento = (RecuperacionSalvamento) recuperacion;
				if( salvamento.obtenerVentaActiva() != null ){
					ingreso.setMonto( salvamento.obtenerVentaActiva().getTotalVenta() );
				}
			}else if(EnumUtil.equalsValue(TipoRecuperacion.SIPAC, recuperacion.getTipo())){
				RecuperacionSipac sipac = (RecuperacionSipac) recuperacion;
				ingreso.setMonto(sipac.getValorEstimado());
			}
			
		 	ingreso.setEstatus(Ingreso.EstatusIngreso.PENDIENTE.getValue());
		 	ingreso.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			ingreso.setFechaCreacion(new Date());
			ingreso.setNumero(entidadService.getIncrementedSequence(Ingreso.SECUENCIA_NUMERO));
			ingreso.setRecuperacion(recuperacion);			
			entidadService.save(ingreso);
			
			//actualizar recuperacion y generar bitacora
			recuperacion.setEstatus(Recuperacion.EstatusRecuperacion.PENDIENTE.toString());
			recuperacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			recuperacion.setFechaModificacion(new Date());
			entidadService.save(recuperacion);
			bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.GENERAR_INGRESO, 
					recuperacion.getId().toString(), 
					String.format("Se genera ingreso pendiente para la recuperacion con id [ %1$s ]", 							 
							String.valueOf(recuperacion.getId())), 
					 recuperacion.toString(),
					usuarioService.getUsuarioActual().getNombreUsuario());
			
		}	
		
		return ingreso;
	}

	
	/**
	 * Metodo que asocia Coberturas a una Recuperacion .
	 * @param recuperacionId
	 * @param coberturasConcat
	 */
	@Override
	public void guardarCoberturasRecuperacion(Long recuperacionId, String coberturasConcat){
		Recuperacion recuperacion = this.entidadService.findById(Recuperacion.class, recuperacionId);
		if(!StringUtil.isEmpty(coberturasConcat)){
			coberturasConcat= coberturasConcat.trim();
			 String[] lstIdCoberturas=  coberturasConcat.split(",") ;
			 for (String cobertura: lstIdCoberturas){
				 if(!StringUtil.isEmpty(cobertura)){
					 String[] coberturaLst=  cobertura.split("\\|"); 
					 Long idCobertura = new Long (coberturaLst[0]);
					 CoberturaReporteCabina coberturaReporteCab = this.entidadService.findById(CoberturaReporteCabina.class,idCobertura);
					 if(null!=coberturaReporteCab){
						 CoberturaRecuperacion coberturaRecuperacion=new CoberturaRecuperacion();
						 String clave = coberturaLst[1];
						 if(!StringUtils.isEmpty(clave) &&  !clave.equalsIgnoreCase("0")){
							 coberturaRecuperacion.setClaveSubCalculo(clave);
							 
						 }
						 coberturaRecuperacion.setCoberturaReporte(coberturaReporteCab);
						 coberturaRecuperacion.setRecuperacion(recuperacion);
						 this.entidadService.save(coberturaRecuperacion);
					 }
				 }
				 
			 }
		}

	}	
	
	@Override
	public void guardarPasesRecuperacion(Long idRecuperacion, EstimacionCoberturaReporteCabina estimacion){
		if(null!=estimacion.getId()){
			Map<String,Object> params =  new LinkedHashMap<String, Object>();
			params.put("coberturaReporte.id", estimacion.getCoberturaReporteCabina().getId());
			params.put("recuperacion.id", idRecuperacion);
			if (null!=estimacion.getCveSubCalculo()) {
				params.put("claveSubCalculo", estimacion.getCveSubCalculo() );
			}
			List<CoberturaRecuperacion> coberturas=entidadService.findByProperties(CoberturaRecuperacion .class, params);
			if (null!=coberturas&& !coberturas.isEmpty()){
				 CoberturaRecuperacion cobertura = coberturas.get(0);
				 
				  PaseRecuperacion paseRecuperacion = new PaseRecuperacion(cobertura, estimacion);	
				    entidadService.save(paseRecuperacion);
			}
		}
	}
	
	@Override
	public void guardarCoberturasRecuperacion(Recuperacion recuperacion, EstimacionCoberturaReporteCabina estimacion){
		CoberturaRecuperacion coberturaRecuperacion = new CoberturaRecuperacion(estimacion.getCveSubCalculo(), 
				estimacion.getCoberturaReporteCabina(), recuperacion);
		entidadService.save(coberturaRecuperacion);		
	}
	

	/**
	 * Metodo que asocia Pases de Atencion a una Recuperacion .

	 * @param idRecuperacion
	 * @param pasesConcat
	 */
	@Override
	public void guardarPasesRecuperacion(Long idRecuperacion, String pasesConcat){
		//Recuperacion recuperacion = this.entidadService.findById(Recuperacion.class, idRecuperacion);
		 String[] lstIdPases=  pasesConcat.trim().split(",") ;
		 for (String idPase: lstIdPases){
			 Long idEstima= new Long (idPase);
			 EstimacionCoberturaReporteCabina pase = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstima);
			 guardarPasesRecuperacion(idRecuperacion, pase);
		 }
		 

	}

	/**
	 * Metodo que asocia referencias Bancarias a una Recuperacion.
	 * 
	 * @param recuperacionId
	 * @param referencias
	 */
	@Override
	public void guardarReferenciaBancaria(Long recuperacionId, List<ReferenciaBancaria> referencias){
		// TODO: 
	}

	/**
	 * Metodo que regresa una cadena de ids concatenados por coma de la Cobertuas
	 * asociadas  a la Recuperacion. 
	 * 
	 * @param recuperacionId
	 */
	@Override
	public Map<String, String> obtenerCoberturasRecuperacion(Long recuperacionId){
		Map<String, String> map = new HashMap<String, String>();
		List<CoberturaRecuperacion> coberturas= this.getCoberturasRecuperacion(recuperacionId);
		if(null!=coberturas && !coberturas.isEmpty()){
		 for(CoberturaRecuperacion cobertura : coberturas){
			 	String cveSubTipCal=OrdenCompraService.CVENULA;
				if (!StringUtil.isEmpty(cobertura.getClaveSubCalculo()))
			 		cveSubTipCal=cobertura.getClaveSubCalculo();
			 	String nombreCobertura = this.estimacionService.obtenerNombreCobertura
			 					(cobertura.getCoberturaReporte().getCoberturaDTO(), cobertura.getCoberturaReporte().getClaveTipoCalculo(), 
			 							cobertura.getClaveSubCalculo(), null, cobertura.getCoberturaReporte().getCoberturaDTO().getTipoConfiguracion());
				map.put(cobertura.getCoberturaReporte().getId()+ OrdenCompraService.SEPARADOR +cveSubTipCal, nombreCobertura);
			}
		}

		 
		 
		 
		 return map;
	}

	/**
	 * Metodo que retorna un mapa con las coberturas afectadas.
	 * 
	 * @param reporteId
	 */
	@Override
	public Map<String,String> obtenerCoberturasSiniestro(Long reporteId){
		return this.listadoService.getCoberturasOrdenCompra(reporteId, OrdenCompraService.TIPO_AFECTACION_RESERVA);
	}

	/**
	 * Metodo que obtiene el listado de Pases de un Siniestro en base a un filtro de
	 * Coberturas.
	 * 
	 * @param coberturasConcat
	 */
	@Override
	public Map<String,String> obtenerPasesCoberturaSiniestro(String coberturasConcat){
		// TODO: 
		return null;
	}

	/**
	 * Metodo que regresa una cadena de ids concatenados por coma de los Pases de
	 * Atencion asociados a la Recuperacion.
	 * 
	 * @param recuperacionId
	 */
	@Override
	public Map<Long, String> obtenerPasesRecuperacion(Long recuperacionId){
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		List<EstimacionCoberturaReporteCabina> lstPases= this.getPasesRecuperacion(recuperacionId);
		if(null!=lstPases && !lstPases.isEmpty()){
			for(EstimacionCoberturaReporteCabina pase : lstPases){
				String nombreAsegurado ="";
				if(!StringUtil.isEmpty( pase.getNombreAfectado())){
					nombreAsegurado=pase.getNombreAfectado();
					
				}else{
					try {
						nombreAsegurado= (!StringUtil.isEmpty(pase.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza().getNombreAsegurado())) ? pase.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getPoliza().getNombreAsegurado() : "";
					}catch (Exception e ){
						nombreAsegurado="";
					}
				}
				if(!StringUtil.isEmpty(pase.getCveSubCalculo())){
					map.put( new Long(pase.getId()),"["+pase.getCveSubCalculo()+"] "+  ((!StringUtil.isEmpty(pase.getFolio())) ? pase.getFolio() : "")+"	"+ nombreAsegurado);
				}else{
					map.put( new Long(pase.getId()),"["+pase.getCoberturaReporteCabina().getClaveTipoCalculo()+"] "+ ((!StringUtil.isEmpty(pase.getFolio())) ? pase.getFolio() : "")+"	"+ nombreAsegurado);
				}
			}
		}
		return map;
	}




	@Override
	public List<ReferenciaBancariaDTO> obtenerCuentasBancarias() {
		return this.recuperacionDao.obtenerCuentasBancarias();
	}
	
	/**
	
	La Referencia Bancaria se deberá de componer de la siguiente manera:
	
	Para generar la Referencia Bancaria se Utilizará el Algoritmo 97
	
	Definición de Algoritmo 97
	-- Calculo de digito verificados en base 97
	    -- Longitud de la referencia    25 posiciones
	    -- Longitud Fecha condensada     4 posiciones
	    -- Longitud Importe condensado   1 posiciones
	    -- Longitud Digito verificador   2 posiciones

	1.     Número de Siniestro - Se obtiene del ID de Recuperación (3+8+2 = 13 Posiciones)
	2.     Tipo de Recuperación - Se obtiene del ID de Recuperación (2 Posiciones)
	3.     Número Consecutivo de la Referencia Bancaria - Es el siguiente número consecutivo de la última Referencia Bancaria registrada. (10 Posiciones) / Se modifica a 8 para generar referencias de 30
	4.     Fecha Condensada .- Se obtiene de un algoritmo 97 (4 Posiciones)
	5.     Importe Condensado.- Se obtiene de un algoritmo 97 (1 Posiciones)
	6.     Digito Verificador - Se obtiene de un algoritmo 97 (2 Posiciones)**/

	@Override
	public void generarReferenciaBancaria(Long recuperacionId) {
		
		Recuperacion recuperacion = entidadService.findById(Recuperacion.class, recuperacionId);


		if(recuperacion != null){

			String referenciaPreliminar = "", referencia = "", algoritmo = "";
			Long consecutivo = null;
			

			//OBTENER NUMERO REPORTE (13 posiciones)
			String numReporte = this.convierteNumReporte(recuperacion.getReporteCabina());

			//OBTENER TIPO RECUPERACION (2 posiciones)
			String tipoRecuperacion = StringUtils.leftPad(this.obtenerTipoRecuperacion(recuperacion.getTipo()),2,"0");

			//desactivar set anterior de referencias
			recuperacionDao.desactivarReferenciasBancarias(recuperacionId);

			//ITERAR CUENTAS BANCARIAS
			for( ConfigCuentaMidas cuenta : recuperacionDao.listadoCuentasBancarias() ){
				
				if (cuenta.getCuenta().getAlgoritmo() != null) {
					if( referencia.isEmpty() || !cuenta.getCuenta().getAlgoritmo().equals(algoritmo) ){
						
						algoritmo = cuenta.getCuenta().getAlgoritmo();
						
						//CONSECUTIVO (10 Posiciones)
						consecutivo = entidadService.getIncrementedSequence(ReferenciaBancaria.SECUENCIA_NUMERO);

						//PRELIMINAR (25 Posiciones) -- Se cambia a 23
						referenciaPreliminar = numReporte+tipoRecuperacion+StringUtils.leftPad(String.valueOf(consecutivo),8,"0");
						referencia = recuperacionDao.generarReferenciaBancaria(referenciaPreliminar, cuenta, sumaFecha(recuperacion), this.obtenerTotalRecuperacion(recuperacion) );
					}

					if( !StringUtil.isEmpty(referencia)){
						ReferenciaBancaria referenciaBancaria = new ReferenciaBancaria();
						referenciaBancaria.setRecuperacion(recuperacion);
						referenciaBancaria.setReferencia(referencia);
						referenciaBancaria.setMedioRecuperacion(recuperacion.getMedio());
						referenciaBancaria.setClabe(!StringUtil.isEmpty(cuenta.getCuenta().getClabe())? cuenta.getCuenta().getClabe() : "NA");
						BancoMidas banco = new BancoMidas();
						banco.setId(cuenta.getCuenta().getBancoMidas().getId());
						referenciaBancaria.setBanco( banco );
						referenciaBancaria.setCuentaBancaria( cuenta.getCuenta().getNumeroCuenta() );
						referenciaBancaria.setConsecutivo(consecutivo);
						referenciaBancaria.setActivo(Boolean.TRUE);						
						entidadService.save(referenciaBancaria);
					}
				}

			}

		}

	}
	
	
	@Override
	public List<ReferenciaBancariaDTO> obtenerReferenciasBancaria(Long recuperacionId) {
		
		String medioRecuperacion;
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("recuperacion.id", recuperacionId);
		
		List<ReferenciaBancaria> referenciasBancarias=this.entidadService.findByProperties(ReferenciaBancaria.class, param);
		
		if( !referenciasBancarias.isEmpty() ){
			medioRecuperacion = referenciasBancarias.get(0).getMedioRecuperacion();
			
			return this.obtenerReferenciasBancaria(recuperacionId, medioRecuperacion);
		}
		
		return null;
	}	
	
	private BigDecimal obtenerTotalRecuperacion(Recuperacion recuperacion){
		BigDecimal total = null;
		if( recuperacion.getTipo().equals(TipoRecuperacion.PROVEEDOR.getValue()) ){
			//
			RecuperacionProveedor recuperacionProveedor = entidadService.findById(RecuperacionProveedor.class, recuperacion.getId());
			if( recuperacionProveedor != null ){
				total = recuperacionProveedor.getMontoTotal();
			}
		}else if(recuperacion.getTipo().equals(TipoRecuperacion.DEDUCIBLE.getValue())){
			RecuperacionDeducible recuperacionDeducible = entidadService.findById(RecuperacionDeducible.class, recuperacion.getId());
			if(recuperacionDeducible != null){
				total = recuperacionDeducible.getMontoFinalDeducible();
			}
		}else if(recuperacion.getTipo().equals(TipoRecuperacion.SALVAMENTO.getValue())){
			RecuperacionSalvamento recuperacionSalvamento = entidadService.findById(RecuperacionSalvamento.class, recuperacion.getId());
			if(recuperacionSalvamento != null){
				total = recuperacionSalvamento.obtenerVentaActiva().getTotalVenta();
			}
		}else if(recuperacion.getTipo().equals(TipoRecuperacion.COMPANIA.getValue())){
			RecuperacionCompania recuperacionCompania = entidadService.findById(RecuperacionCompania.class, recuperacion.getId());
			if(recuperacionCompania != null){
				total = recuperacionCompania.getImporte();
			}
		}else if( recuperacion.getTipo().equals(TipoRecuperacion.CRUCEROJURIDICA.getValue()) ){
			RecuperacionCruceroJuridica recuperacionCrucero = entidadService.findById(RecuperacionCruceroJuridica.class, recuperacion.getId());
			if( recuperacionCrucero != null ){
				total = recuperacionCrucero.getMontoFinal();
			}
		}		
		return total;
	}

	private String convierteNumReporte(ReporteCabina reporteCabina){		
		String[] aNumReporte = reporteCabina.getNumeroReporte().split("-");

		if( reporteCabina.getOficina() != null ){

			//OFICINA
			Long keyConsecutivoReferencia = reporteCabina.getOficina().getConsecutivoReferencia();
			if( keyConsecutivoReferencia != null ){
				aNumReporte[0] = StringUtils.leftPad(keyConsecutivoReferencia.toString(),3,"0"); 
			}else{
				throw new RuntimeException("No se encontró la referencia configurada para la oficina");
			}

			//CONSECUTIVO
			aNumReporte[1] = StringUtils.leftPad(aNumReporte[1],8,"0"); 

			//ANIO
			aNumReporte[2] = StringUtils.leftPad(aNumReporte[2],2,"0");

		}
		return aNumReporte[0]+aNumReporte[1]+aNumReporte[2];
	}

	private String obtenerTipoRecuperacion(String tipoRecuperacion){

		String keyRecuperacion = null;

		TipoRecuperacion recuperacionEnum = EnumUtil.fromValue(TipoRecuperacion.class, tipoRecuperacion);

		if (recuperacionEnum != null) {
			keyRecuperacion = recuperacionEnum.getCodigoReferencia();
		}
		return keyRecuperacion;
	}
	
	private Date sumaFecha(Recuperacion recuperacion){
		Calendar cal = Calendar.getInstance();
		if(recuperacion.getTipo().equals(Recuperacion.TipoRecuperacion.SALVAMENTO.getValue())){
			RecuperacionSalvamento salvamento = (RecuperacionSalvamento)recuperacion;
			if(salvamento.getFechaLimiteProrroga()!=null){
				cal.setTime(salvamento.getFechaLimiteProrroga());
			}else{
				cal.setTime(salvamento.obtenerVentaActiva().getFechaCierreDeSubasta());
				cal.add(Calendar.DATE, 4);
			}
		}else{
			cal.setTime(recuperacion.getFechaCreacion());
			cal.add(Calendar.YEAR, 2);
		}
		 
		return cal.getTime();
	}


	@Override
	public List<ReferenciaBancariaDTO> obtenerReferenciasBancaria(
			Long recuperacionId, String medioRecuperacion) {
		List<ReferenciaBancariaDTO> listaResultados = new ArrayList<ReferenciaBancariaDTO>();
		Map<String,Object> param = new HashMap<String, Object>();
		param.put("recuperacion.id", recuperacionId);
		param.put("medioRecuperacion", medioRecuperacion);
		List<ReferenciaBancaria> referenciasBancarias=this.entidadService.findByProperties(ReferenciaBancaria.class, param);
		for(ReferenciaBancaria referencia:referenciasBancarias){
			BancoMidas bancoMidas=referencia.getBanco();
			ReferenciaBancariaDTO dto =new ReferenciaBancariaDTO( bancoMidas,
					 referencia.getClabe(),
					 referencia.getCuentaBancaria(),
					 referencia.getId(),
					 referencia.getMedioRecuperacion(),
					 referencia.getReferencia(),
					 referencia
					);
			listaResultados.add(dto);
		}
		return listaResultados;
	}

	
	/**
	 * Obtener una recuperacion que extiende de Recuperacion para el Id dado 
	 * @param <R>
	 * @param id
	 * @return
	 */
	public <R extends Recuperacion> R obtenerRecuperacion(Class<R> tipo, Long id){
		return entidadService.findById(tipo, id);
	}

	@Override
	public void eliminarReferenciasBancariasByRecuperacionId(Long recuperacionId) {
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("recuperacion.id", recuperacionId);
		
		List<ReferenciaBancaria> lReferenciasBancarias = this.entidadService.findByProperties(ReferenciaBancaria.class, params);
		
		if( !lReferenciasBancarias.isEmpty() ){
			this.entidadService.removeAll(lReferenciasBancarias);
		}
		
	}


	@Override
	public List<CoberturaRecuperacion> getCoberturasRecuperacion(
			Long recuperacionId) {
		List<CoberturaRecuperacion> coberturas=entidadService.findByProperty(CoberturaRecuperacion.class, "recuperacion.id", recuperacionId);
		return coberturas;	
	}


	@Override
	public List<EstimacionCoberturaReporteCabina> getPasesRecuperacion(
			Long recuperacionId) {
		List<CoberturaRecuperacion> lstCoberturas= this.getCoberturasRecuperacion(recuperacionId);
		List<EstimacionCoberturaReporteCabina> listaPasesAtencion  = new ArrayList<EstimacionCoberturaReporteCabina>();
		if(null!= lstCoberturas && !lstCoberturas.isEmpty()){
			for(CoberturaRecuperacion cobertura :lstCoberturas){
				List<PaseRecuperacion> lstPases= cobertura.getPasesAtencion();
				for(PaseRecuperacion pase : lstPases){
					listaPasesAtencion.add(pase.getEstimacionCobertura());
				}
			}
		}		
		return listaPasesAtencion;
	}
	
	
	@SuppressWarnings("unchecked")
	public Ingreso obtenerIngresoActivo(Long recuperacionId){
		Map<String, Object> parameters = new HashMap<String, Object>();
		parameters.put("idRecuperacion", recuperacionId);
		StringBuilder query = new StringBuilder("SELECT ingreso from mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso ingreso ");
		query.append("WHERE ingreso.estatus in ('PENDIENTE', 'APLICADO') AND ingreso.recuperacion.id = :idRecuperacion ");
		List<Ingreso> ingresos = entidadService.executeQueryMultipleResult(query.toString(), parameters);
		for(Ingreso ingreso : CollectionUtils.emptyIfNull(ingresos)){
			return ingreso;
		}
		return null;
	}
	
	
	@Override
	public TransporteImpresionDTO imprimirInstructivoDepositoRecuperacion(Long idRecuperacion){
		if(idRecuperacion != null){
			Recuperacion recuperacion = entidadService.findById(Recuperacion.class, idRecuperacion);
			
			if(recuperacion != null){
				GeneradorImpresion gImpresion = new GeneradorImpresion();
				JasperReport jReport = gImpresion.getOJasperReport(INSTRUCTIVO_DEPOSITO);
				JasperReport jReportListadoBancos = gImpresion.getOJasperReport(INSTRUCTIVO_DEPOSITO_LISTADO_BANCOS);
				
				List<InstructivoDepositoDTO> dataSourceImpresion = new ArrayList<InstructivoDepositoDTO>();
				InstructivoDepositoDTO instructivo = obtenerInformacionInstructivoDepositoRecuperacion(recuperacion);
				dataSourceImpresion.add(instructivo);
				
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
				params.put("jReportListadoBancos", jReportListadoBancos);
				JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
				
				return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
			}
		}
		return null;
	}
	
	/**
	 * Prepara la informacion del instructivo para enviarla a la impresion
	 * @param recuperacion
	 * @return
	 */
	private InstructivoDepositoDTO obtenerInformacionInstructivoDepositoRecuperacion(Recuperacion recuperacion){
		InstructivoDepositoDTO instructivo = new InstructivoDepositoDTO();
		List<Recuperacion> recuperacionesActualizadas = entidadService.findByProperty(Recuperacion.class, "id", recuperacion.getId());
		Recuperacion recuperacionActualizada = recuperacionesActualizadas.get(0); 
		
		if(recuperacionActualizada != null){														//MONTO (IMPORTE)
			if(EnumUtil.equalsValue(Recuperacion.TipoRecuperacion.DEDUCIBLE, recuperacionActualizada.getTipo())){
				RecuperacionDeducible recuperacionDeducible = ((RecuperacionDeducible)recuperacionActualizada);
				instructivo.setImporte((recuperacionDeducible.getMontoFinalDeducible() != null)? 
						recuperacionDeducible.getMontoFinalDeducible() : BigDecimal.ZERO);
			}else{
				Ingreso ingresoActivo = obtenerIngresoActivo(recuperacionActualizada.getId());
				if(ingresoActivo != null){																
						instructivo.setImporte((ingresoActivo.getMonto() != null)? ingresoActivo.getMonto() : BigDecimal.ZERO);
				}else{
					instructivo.setImporte(BigDecimal.ZERO);
				}
			}
			if(recuperacionActualizada.getReporteCabina() != null){									//NUMERO REPORTE
				instructivo.setNumeroReporte(recuperacionActualizada.getReporteCabina().getNumeroReporte());
				if(recuperacionActualizada.getReporteCabina().getSiniestroCabina() != null){		//NUMERO SINIESTRO
					instructivo.setNumeroSiniestro(recuperacionActualizada.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
				}
			}
			instructivo.setTipoRecuperacion(obtenerConceptoInstructivo(recuperacionActualizada));	//TIPO RECUPERACION
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("recuperacion.id", recuperacionActualizada.getId());
			params.put("activo", Boolean.TRUE);
			List<ReferenciaBancaria> referencias = entidadService.findByPropertiesWithOrder(ReferenciaBancaria.class, params, "id asc");
			params = null;
			if(referencias != null){							//REFERENCIA
				for(ReferenciaBancaria ref : referencias){
					if( ref.getReferencia() != null ){
						instructivo.setReferencia(ref.getReferencia());
						break;
					}
				}
				//Para obtener la lista de referencias bancarias ordenada
				instructivo.setListadoBancos(obtenerListadoBancosDeInstructivo(referencias));
			}
		}
		
		return instructivo;
	}
	
	/**
	 * Prepara la informacion del listado de bancos para le impresion del instructivo de deposito
	 * @param referencias
	 * @return
	 */
	private List<InstructivoDepositoListadoBancosDTO> obtenerListadoBancosDeInstructivo(List<ReferenciaBancaria> referencias){
		List<InstructivoDepositoListadoBancosDTO> listado = new ArrayList<InstructivoDepositoListadoBancosDTO>();
		
		if(referencias != null){
			for(ReferenciaBancaria referencia : referencias){
				InstructivoDepositoListadoBancosDTO banco = new InstructivoDepositoListadoBancosDTO();
				banco.setBanco(referencia.getBanco().getNombre());				//NOMBRE DEL BANCO
				banco.setClaveInterbancaria(referencia.getClabe());				//CLABE INTERBANCARIA
				banco.setNumeroCuenta(referencia.getCuentaBancaria());			//CUENTA BANCARIA
				listado.add(banco);
			}
		}
		
		return listado;
	}
	
	@Deprecated
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void invocaProvision(Long idRecuperacion){
		try {
			this.recuperacionDao.invocaProvision(idRecuperacion);
		} catch (MidasException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * dependiendo del tipo de recuperacion es el concepto que se le asigna al instructivo
	 * @param recuperacion
	 * @return
	 */
	private String obtenerConceptoInstructivo(Recuperacion recuperacion){
		String concepto = "";
		
		if(EnumUtil.equalsValue(TipoRecuperacion.PROVEEDOR, recuperacion.getTipo())){
			if(recuperacion.getMedio().equals(MedioRecuperacion.REEMBOLSO.toString())){
				concepto = "Recuperaci\u00F3n de Proveedor";
			}else if(recuperacion.getMedio().equals(MedioRecuperacion.VENTA.toString())){
				concepto = "Venta de Piezas";
			}
		}
		else if(EnumUtil.equalsValue(TipoRecuperacion.CRUCEROJURIDICA, recuperacion.getTipo())){
			if(recuperacion.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoAjuste().equals("REC")){
				concepto = "Recuperaci\u00F3n de Crucero";
			}else if(recuperacion.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getTerminoAjuste().equals("REJ")){
				concepto = "Recuperaci\u00F3n de Jur\u00EDdico";
			}
		}
		else if(EnumUtil.equalsValue(TipoRecuperacion.DEDUCIBLE, recuperacion.getTipo())){
			concepto = "Recuperaci\u00F3n de Deducible";
		}
		else if(EnumUtil.equalsValue(TipoRecuperacion.SALVAMENTO, recuperacion.getTipo())){
			concepto = "Venta de Salvamento";
		}
		return concepto;
	}
	
	
	public List<ByteArrayAttachment> generarPDFInstrucctivoPago(Long recuperacionId){
		List<ByteArrayAttachment> pdfAdjunto = null;
		
		try{
			// OBTENER INSTRUCTIVO DE DEPOSITO 
	        TransporteImpresionDTO pdfInstrucctivo =  this.imprimirInstructivoDepositoRecuperacion(recuperacionId);
	        ByteArrayAttachment adjunto = new ByteArrayAttachment(ARCHIVO_ADJUNTO, TipoArchivo.PDF, pdfInstrucctivo.getByteArray());
	        pdfAdjunto = new ArrayList<ByteArrayAttachment>();
	        pdfAdjunto.add(adjunto);
		}catch(Exception e){
			
		}
		
		return pdfAdjunto;
	}
	
	/**
	 * Lista de Recuperaciones que cuentan con referencias bancarias ligadas a un reporte cabina
	 * @param idReporteCabina
	 * @return
	 */
	public List<RecuperacionDTO> buscarReferenciasBancarias(Long idReporteCabina) {
		return this.recuperacionDao.buscarReferenciasBancarias(idReporteCabina);
	}
	
}