package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoDao;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoService;

@Stateless
public class DatoIncisoCotAutoServiceImpl implements DatoIncisoCotAutoService {

	private DatoIncisoCotAutoDao dao;

	@Override
	public List<DatoIncisoCotAuto> getDatosCoberturaInciso(
			BigDecimal idCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo, BigDecimal idTcSubRamo,
			BigDecimal idToCobertura) {
		return dao.getDatosCoberturaInciso(idCotizacion, numeroInciso,
				idTcRamo, idTcSubRamo, idToCobertura);
	}

	@Override
	public List<DatoIncisoCotAuto> getDatosRamoInciso(BigDecimal idCotizacion,
			BigDecimal numeroInciso, BigDecimal idTcRamo) {
		return dao.getDatosRamoInciso(idCotizacion, numeroInciso, idTcRamo);
	}

	@Override
	public List<DatoIncisoCotAuto> getDatosSubRamoInciso(
			BigDecimal idCotizacion, BigDecimal numeroInciso,
			BigDecimal idTcRamo, BigDecimal idTcSubRamo) {
		return dao.getDatosSubRamoInciso(idCotizacion, numeroInciso, idTcRamo,
				idTcSubRamo);
	}
	
	@Override
	public DatoIncisoCotAuto getDatoIncisoByConfiguracion(
			BigDecimal idCotizacion, BigDecimal idToSeccion, BigDecimal numeroInciso,
			BigDecimal idTcRamo, BigDecimal idTcSubRamo, BigDecimal idToCobertura,
			Short claveDetalle, BigDecimal idDato) {
		//Hay veces que el cliente no manda el numeroInciso para estos casos conviene mejor regresar nulo para evitar
		//hacer una llamada a la base de datos inecesaria porque el resultado de esta llamada seria siempre nulo de todos modos.
		if (numeroInciso == null) {
			return null;
		}
		DatoIncisoCotAuto dato = null;
		try{
			dato = dao.getDatoIncisoByConfiguracion(idCotizacion, idToSeccion, numeroInciso, 
					idTcRamo, idTcSubRamo, idToCobertura, claveDetalle, idDato); 
		}catch(Exception e){

		}
		return dato;
	}
	

	@EJB
	public void setDao(DatoIncisoCotAutoDao dao) {
		this.dao = dao;
	}

}
