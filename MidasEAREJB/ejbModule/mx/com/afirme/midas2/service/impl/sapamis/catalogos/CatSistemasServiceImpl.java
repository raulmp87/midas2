package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSistemas;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatSistemasService;

/*******************************************************************************
 * Nombre Interface: 	CatSistemasServiceImpl.
 * 
 * Descripcion: 		Contiene la logica del manejo de los Servicios para el 
 * 						objeto CatSistemas.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class CatSistemasServiceImpl implements CatSistemasService{
	private static final long serialVersionUID = 1L;
	
	@EJB private EntidadService entidadService;
	
	@Override
	public CatSistemas completeObject(CatSistemas catSistemas) {
		CatSistemas retorno = new CatSistemas(); 
		if(catSistemas != null){
			if(catSistemas.getId() < 1){
				retorno = this.findIdByAttributes(catSistemas);
			}else{
				if(!this.validateAttributes(catSistemas)){
					retorno = this.findById(catSistemas.getId());
				}
			}
		}else{
			retorno.setId(new Long(-1));
		}
		return retorno;
	}
	
	@Override
	public boolean estatusProceso(long idSistema, int tipoProceso){
		CatSistemas catSistemas = entidadService.findById(CatSistemas.class, idSistema);
		if(tipoProceso == 1){
			return(catSistemas.getEstatusProcesoExtraccion()==0);
		}else if(tipoProceso == 2){
			return(catSistemas.getEstatusProcesoEnvio()==0);
		}
		return false;
	}
	
	@Override
	public boolean guardarRegistro(CatSistemas catSistemas){
		try{
			entidadService.save(catSistemas);
			return true;
		}catch(Exception e){
			return false;
		}
	}

	@Override
	public Long obtenerIdSistemaPorDescripcion(String descSistema){
		List<CatSistemas> catSistemas = entidadService.findByProperty(CatSistemas.class, "descCatSistemas", descSistema);
		return catSistemas.get(0).getId();
	}
	
	private CatSistemas findById(long id) {
		CatSistemas retorno = new CatSistemas();
		if(id >= 0){
			retorno = entidadService.findById(CatSistemas.class, id);
		}
		return retorno;
	}
	
	private boolean validateAttributes(CatSistemas catSistemas){
		return catSistemas.getDescCatSistemas() != null && !catSistemas.getDescCatSistemas().equals("");
	}

	private CatSistemas findIdByAttributes(CatSistemas catSistemas) {
		if(validateAttributes(catSistemas)){
			Map<String,Object> parametros = new HashMap<String,Object>();			
			parametros.put("descCatSistemas", catSistemas.getDescCatSistemas());			
			List<CatSistemas> catSistemasList = entidadService.findByProperties(CatSistemas.class, parametros);
			if(!catSistemasList.isEmpty()){
				catSistemas.setId(catSistemasList.get(0).getId());
			}else{
				catSistemas.setId(new Long(0));
			}
		}else{
			catSistemas.setId(new Long(-1));
		}
		return catSistemas;
	}
}