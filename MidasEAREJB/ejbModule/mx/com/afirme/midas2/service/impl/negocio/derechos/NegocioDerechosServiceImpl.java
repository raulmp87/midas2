package mx.com.afirme.midas2.service.impl.negocio.derechos;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.NegocioDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;

@Stateless
public class NegocioDerechosServiceImpl implements NegocioDerechosService {
	
	public List<NegocioDerechoEndoso> obtenerDerechosEndoso(Long idToNegocio, Short esAltaInciso) {
		Boolean bEsAltaInciso = (esAltaInciso==1?true:false);
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("negocio.idToNegocio", idToNegocio);
		params.put("esAltaInciso", bEsAltaInciso);
		params.put("activo", true);
		
		List<NegocioDerechoEndoso> items = entidadDao.findByProperties(NegocioDerechoEndoso.class, params);
		
		//Sort
		if(items != null && !items.isEmpty()){
			Collections.sort(items, 
					new Comparator<NegocioDerechoEndoso>() {				
						public int compare(NegocioDerechoEndoso n1, NegocioDerechoEndoso n2){
							return n1.getImporteDerecho().compareTo(n2.getImporteDerecho());
						}
					});
		}
		return items;		
	}
	
	public List<NegocioDerechoPoliza> obtenerDerechosPoliza(Long idToNegocio) {	
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("negocio.idToNegocio", idToNegocio);
		params.put("activo", true); // solo valores activos, los valores que tienen 0 se consideran borrados(logicos) y no deben mostrarse en pantalla.
			
		List<NegocioDerechoPoliza> items = entidadDao.findByProperties(NegocioDerechoPoliza.class, params);
		//Sort
		if(items != null && !items.isEmpty()){
			Collections.sort(items, 
					new Comparator<NegocioDerechoPoliza>() {				
						public int compare(NegocioDerechoPoliza n1, NegocioDerechoPoliza n2){
							return n1.getImporteDerecho().compareTo(n2.getImporteDerecho());
						}
					});
		}
		return items;	
	}
	
	@Override
	public NegocioDerechoPoliza obtenerDechosPolizaDefault(
			Long idToNegocio) {
		List<NegocioDerechoPoliza> negocioDerechoPolizas = new LinkedList<NegocioDerechoPoliza>();
		
		negocioDerechoPolizas =	obtenerDerechosPoliza(idToNegocio);
		for (NegocioDerechoPoliza negocioDerechoPoliza : negocioDerechoPolizas) {
			if (negocioDerechoPoliza.getClaveDefault()) {
				return negocioDerechoPoliza;
			}
		}
		return null;
	}
	
	@Override
	public NegocioDerechoEndoso obtenerDerechosEndosoDefault(SolicitudDTO solicitud) {
		
		  Short esAltaInciso = (solicitud.getClaveTipoEndoso().compareTo(new BigDecimal(SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)) == 0?(short)1:(short)0);  
		  List<NegocioDerechoEndoso> derechosList = obtenerDerechosEndoso(solicitud.getNegocio().getIdToNegocio(), esAltaInciso);
		for (NegocioDerechoEndoso item : derechosList) {
			if (item.getClaveDefault()) {
				return item;
			}
		}
		return null;
	}
	
	
	public RespuestaGridRelacionDTO relacionarDerechosEndoso(String accion, Long idToNegocio, Long idToNegDerechoEndoso, 
				Double importeDerecho, Short claveDefault, Short esAltaInciso) {		
		
		Boolean bClaveDefault = (claveDefault==1?true:false);
		Boolean bEsAltaInciso = (esAltaInciso==1?true:false);
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		String mensajeError = "Importe Derecho Duplicado";
		Negocio negocio = entidadDao.findById(Negocio.class, idToNegocio);
		
		NegocioDerechoEndoso derechoEndoso = new NegocioDerechoEndoso();
		if (idToNegDerechoEndoso!= null){
			derechoEndoso.setIdToNegDerechoEndoso(idToNegDerechoEndoso);
		}
		derechoEndoso.setNegocio(negocio);
		derechoEndoso.setImporteDerecho(importeDerecho);
		derechoEndoso.setClaveDefault(bClaveDefault);
		derechoEndoso.setEsAltaInciso(bEsAltaInciso);
		derechoEndoso.setActivo(true);
		
		//Validar que exista al menos un derechoEndosoDefault
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("negocio.idToNegocio", negocio.getIdToNegocio());
		params.put("esAltaInciso", bEsAltaInciso);
		params.put("claveDefault", true);
		params.put("activo", true);
		
		List<NegocioDerechoEndoso> negocioDerechoEndosoTemp = entidadDao.findByProperties(NegocioDerechoEndoso.class, params);
		if(negocioDerechoEndosoTemp == null || negocioDerechoEndosoTemp.size()==0){
			derechoEndoso.setClaveDefault(true);
		}
		
		boolean valido = true;
		if (accion.equals("inserted")) {
			//Valida que no exista el importe
			for(NegocioDerechoEndoso item : negocio.getNegocioDerechoEndosos()){
				if(item.getActivo() && item.getEsAltaInciso().equals(derechoEndoso.getEsAltaInciso()) 
						&& item.getImporteDerecho().equals(derechoEndoso.getImporteDerecho())){
					valido = false;
					break;
				}
			}
			if(valido){
				negocio.getNegocioDerechoEndosos().add(derechoEndoso);
			}
		} else if (accion.equals("updated")) {
			for(NegocioDerechoEndoso item : negocio.getNegocioDerechoEndosos()){
				if(item.getActivo() && item.getEsAltaInciso().equals(derechoEndoso.getEsAltaInciso()) && item.getImporteDerecho().equals(derechoEndoso.getImporteDerecho()) 
						&& !item.getIdToNegDerechoEndoso().equals(derechoEndoso.getIdToNegDerechoEndoso())){
					valido = false;
					break;
				}
			}
			if(valido){
				if (negocio.getNegocioDerechoEndosos().contains(derechoEndoso)) {
					negocio.getNegocioDerechoEndosos().get(negocio.getNegocioDerechoEndosos().indexOf(derechoEndoso)).setImporteDerecho(importeDerecho);
					negocio.getNegocioDerechoEndosos().get(negocio.getNegocioDerechoEndosos().indexOf(derechoEndoso)).setClaveDefault(bClaveDefault);
				}
			}
		} else if (accion.equals("deleted")) {
			if (negocio.getNegocioDerechoEndosos().contains(derechoEndoso)
					&& !negocio.getNegocioDerechoEndosos().get(negocio.getNegocioDerechoEndosos().indexOf(derechoEndoso)).getClaveDefault()) {
				negocio.getNegocioDerechoEndosos().get(negocio.getNegocioDerechoEndosos().indexOf(derechoEndoso)).setActivo(false);
			} else {
				mensajeError = "No se puede borrar valor Default";
				valido = false;
			}
		}		
		try{
			if(valido){
				negocioDao.update(negocio);
				respuesta.setTipoMensaje("30"); // Exito
				respuesta.setTipoRespuesta("result");
			}else{
				respuesta.setTipoMensaje("20"); // Error
				respuesta.setTipoRespuesta("result");
				respuesta.setMensaje(mensajeError);		
			}
		}catch(RuntimeException e){
			respuesta.setTipoMensaje("10");// Error
		}	
		return respuesta;
				
	}

	
	public RespuestaGridRelacionDTO relacionarDerechosPoliza(String accion, Long idToNegocio, Long idToNegDerechoPoliza, Double importeDerecho, Short claveDefault) {		
		Boolean bClaveDefault = (claveDefault==1?true:false);
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		
		Negocio negocio = entidadDao.findById(Negocio.class, idToNegocio);
		
		NegocioDerechoPoliza derechoPoliza = new NegocioDerechoPoliza();	
		if (idToNegDerechoPoliza!= null){
			derechoPoliza.setIdToNegDerechoPoliza(idToNegDerechoPoliza);
		}
		derechoPoliza.setNegocio(negocio);
		derechoPoliza.setImporteDerecho(importeDerecho);
		derechoPoliza.setClaveDefault(bClaveDefault);
		derechoPoliza.setActivo(true);
		
		//Validar que exista al menos un derechoEndosoDefault
		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("negocio.idToNegocio", idToNegocio);		
		params.put("claveDefault", true);
		params.put("activo", true);
		
		List<NegocioDerechoPoliza> negocioDerechoPolizaTemp = entidadDao.findByProperties(NegocioDerechoPoliza.class, params);		
		
		if(negocioDerechoPolizaTemp == null || negocioDerechoPolizaTemp.size()==0){
			derechoPoliza.setClaveDefault(true);
		}		
				
		boolean valido = true;
		boolean reactivar = false;
		NegocioDerechoPoliza derechoPolizaReactivar = new NegocioDerechoPoliza();
		
		if (accion.equals("inserted")) {
			//Valida que no exista el importe			
			// Busca todos los derechos de poliza tanto activos como inactivos
			params.remove("claveDefault");
			params.remove("activo");
			
			for(NegocioDerechoPoliza item : entidadDao.findByProperties(NegocioDerechoPoliza.class, params)){
				if(item.getImporteDerecho().equals(derechoPoliza.getImporteDerecho())){
					
					if(item.getActivo())
					{
						valido = false;
						break;
						
					}else
					{
						reactivar = true;
						derechoPolizaReactivar = item;
						break;
					}					
				}
			}
			
			if(valido){
				
				if(reactivar)//Si el valor nuevo ya existía en BD pero se encontraba desactivado(borrado logico), lo reactiva
				{
					negocio.getNegocioDerechoPolizas().get(negocio.getNegocioDerechoPolizas().indexOf(derechoPolizaReactivar)).setActivo(true);
					
				}else // nuevo registro
				{
					negocio.getNegocioDerechoPolizas().add(derechoPoliza);					
				}
				
			}
		} else if (accion.equals("updated")) {	
			
			// Busca todos los derechos de poliza tanto activos como inactivos
			params.remove("claveDefault");
			params.remove("activo");			
			
			for(NegocioDerechoPoliza item : entidadDao.findByProperties(NegocioDerechoPoliza.class, params)){
				if(item.getImporteDerecho().equals(derechoPoliza.getImporteDerecho()) && !item.getIdToNegDerechoPoliza().equals(derechoPoliza.getIdToNegDerechoPoliza())){
					
					if(item.getActivo())
					{
						valido = false;
						break;						
					}else
					{
						reactivar = true;
						derechoPolizaReactivar = item;
						break;					
					}
					
				}
			}
			if(valido){
				
				if(reactivar) //Si el valor nuevo ya existía en BD pero se encontraba desactivado(borrado logico), lo reactiva y acualiza el valor anterior dejandolo inactivo
				{				
					negocio.getNegocioDerechoPolizas().get(negocio.getNegocioDerechoPolizas().indexOf(derechoPolizaReactivar)).setActivo(true);
					negocio.getNegocioDerechoPolizas().get(negocio.getNegocioDerechoPolizas().indexOf(derechoPolizaReactivar)).setClaveDefault(bClaveDefault);
										
					if (negocio.getNegocioDerechoPolizas().contains(derechoPoliza)) {	
						negocio.getNegocioDerechoPolizas().get(negocio.getNegocioDerechoPolizas().indexOf(derechoPoliza)).setActivo(false);
						negocio.getNegocioDerechoPolizas().get(negocio.getNegocioDerechoPolizas().indexOf(derechoPoliza)).setClaveDefault(false);
					}
					
				}else
				{
					if (negocio.getNegocioDerechoPolizas().contains(derechoPoliza)) {
						negocio.getNegocioDerechoPolizas().get(negocio.getNegocioDerechoPolizas().indexOf(derechoPoliza)).setImporteDerecho(importeDerecho);
						negocio.getNegocioDerechoPolizas().get(negocio.getNegocioDerechoPolizas().indexOf(derechoPoliza)).setClaveDefault(bClaveDefault);
					}					
				}				
			}
		} else if (accion.equals("deleted")) {
			if (negocio.getNegocioDerechoPolizas().contains(derechoPoliza)) {
				try{// Borrado logico
					negocio.getNegocioDerechoPolizas().get(negocio.getNegocioDerechoPolizas().indexOf(derechoPoliza)).setActivo(false);
					
				}catch(Exception e){
					e.printStackTrace();
				}
			}
		}		
		try{
			if(valido){
				negocioDao.update(negocio);
				respuesta.setTipoMensaje("30"); // Exito
				respuesta.setTipoRespuesta("result");
			}else{
				respuesta.setTipoMensaje("20"); // Error
				respuesta.setTipoRespuesta("result");
				respuesta.setMensaje("Importe Derecho Duplicado");		
			}
		}catch(RuntimeException e){
			respuesta.setTipoMensaje("10");// Error
		}	
		return respuesta;
		
	}
	
	private EntidadDao entidadDao;
	
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	
	private NegocioDao negocioDao;
	
	@EJB
	public void setNegocioDao(NegocioDao negocioDao) {
		this.negocioDao = negocioDao;
	}

}
