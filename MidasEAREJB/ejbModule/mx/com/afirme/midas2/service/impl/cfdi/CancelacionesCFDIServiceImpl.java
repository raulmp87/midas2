package mx.com.afirme.midas2.service.impl.cfdi;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas2.service.cfdi.CancelacionesCFDIService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;


@Stateless
public class CancelacionesCFDIServiceImpl implements CancelacionesCFDIService {		
	
	private PrintReportClient printReportClient;
	
	public static final Logger LOG = Logger.getLogger(CancelacionesCFDIServiceImpl.class);
	
	@EJB
	public void setPrintReportClient(PrintReportClient printReportClient) {
		this.printReportClient = printReportClient;
	}
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@Override
	public void cancelDigitalBill(){
		try {
			LOG.info("entrando a CancelacionesCFDIService.cancelDigitalBill... ");
			//int referencia1FormateadaInt = 920170216;
			//LOG.info("referencia1FormateadaInt:= "+referencia1FormateadaInt);
			// Obtener referencias de cancelaciones a procesar			
	        DateTime fecha = new DateTime();
	        
	        DateTime referencia1 = fecha.plusDays(-2);
	        String fechaFormateadaStr = String.valueOf(referencia1.getYear()) + String.format("%02d", referencia1.getMonthOfYear()) 
	        	+ String.format("%02d", referencia1.getDayOfMonth());
	        fechaFormateadaStr = "8" + fechaFormateadaStr;
	        int referencia1FormateadaInt = Integer.parseInt(fechaFormateadaStr);
	        
	        DateTime referencia2 = fecha.plusDays(-1);
	        fechaFormateadaStr = String.valueOf(referencia2.getYear()) + String.format("%02d", referencia2.getMonthOfYear()) 
        	+ String.format("%02d", referencia2.getDayOfMonth());
	        fechaFormateadaStr = "8" + fechaFormateadaStr;
	        int referencia2FormateadaInt = Integer.parseInt(fechaFormateadaStr);
	        
	        // Proceso de cancelación de CFDIs
	        LOG.info("tarea cancelDigitalBill ejecutada con fecha : " + referencia1FormateadaInt);
			printReportClient.cancelDigitalBill(referencia1FormateadaInt);	        
	        
	        printReportClient.cancelDigitalBill(referencia2FormateadaInt);	        
	        LOG.info("tarea cancelDigitalBill ejecutada con fecha : " + referencia2FormateadaInt);
			
		} catch (Exception e) {
			e.printStackTrace();
			LOG.error("error general en CancelacionesCFDIService.cancelDigitalBill... ", e);
		}
	}
	
	public void initialize() {
		String timerInfo = "TimerCancelaciones";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(0);
				expression.hour("*");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerCancelaciones", false));
				
				LOG.info("Tarea TimerCancelaciones configurado por LGEC");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerCancelaciones");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerCancelaciones:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		cancelDigitalBill();
	}
}
