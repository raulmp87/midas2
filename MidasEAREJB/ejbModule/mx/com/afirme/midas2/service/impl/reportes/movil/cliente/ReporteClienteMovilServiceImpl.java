package mx.com.afirme.midas2.service.impl.reportes.movil.cliente;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.NegocioSeguros;
import mx.com.afirme.midas2.dao.movil.cliente.reportes.ReportePolizasClienteMovilDao;
import mx.com.afirme.midas2.domain.movil.cliente.ClientePolizas;
import mx.com.afirme.midas2.domain.movil.cliente.DatosPolizaSeycos;
import mx.com.afirme.midas2.domain.movil.cliente.PolizasClienteMovil;
import mx.com.afirme.midas2.service.movil.cliente.ClientePolizasService;
import mx.com.afirme.midas2.service.reportes.movil.cliente.ReporteClienteMovilService;

@Stateless
public class ReporteClienteMovilServiceImpl implements ReporteClienteMovilService{

	@EJB
	private ReportePolizasClienteMovilDao reporteClienteMovilDao;
	@EJB
	public ClientePolizasService clientePolizasService;
	
	@Override
	public List<ClientePolizas> getReportePolizasClienteMovil(String tipoPoliza){
		//String tipoPoliza = "A";
		List<ClientePolizas> clientePolizasList = new ArrayList<ClientePolizas>();
		List<PolizasClienteMovil> polizasMovilList = new ArrayList<PolizasClienteMovil>();
		polizasMovilList = reporteClienteMovilDao.getReportePolizasClienteMovil(tipoPoliza);
		for(PolizasClienteMovil polClienteMov : polizasMovilList ){
			ClientePolizas clientePol = new ClientePolizas();
			clientePol.setPolizaNo(polClienteMov.getPoliza());
			clientePol.setInciso(polClienteMov.getInciso().toString());
			clientePol.setDescripcion(polClienteMov.getDescripcion());
			clientePol.setUsuario(polClienteMov.getNombreUsuario());
			clientePol.setTipo(polClienteMov.getTipo());
			if(polClienteMov.getPolizaDTO()==null && polClienteMov.getIdCotizacionSeycos()!= null){
				DatosPolizaSeycos datosPolizaSey=  new DatosPolizaSeycos();
				datosPolizaSey= clientePolizasService.getIncisoPolizaVida(polClienteMov.getIdCotizacionSeycos(),new Long(polClienteMov.getInciso().toString()));
				clientePol.setAsegurado(datosPolizaSey.getNombreAsegurado());
				clientePol.setEstatus(datosPolizaSey.getSituacionPoliza());
				clientePol.setFechaInicioVigencia(datosPolizaSey.getIniVigencia());
				clientePol.setFechaFinVigencia(datosPolizaSey.getFinVigencia());
			}else if(polClienteMov.getPolizaDTO()!=null ){
				clientePol.setEstatus(polClienteMov.getPolizaDTO().getEstatus());
				clientePol.setAsegurado(polClienteMov.getPolizaDTO().getNombreAsegurado());
				clientePol.setFechaInicioVigencia(polClienteMov.getPolizaDTO().getCotizacionDTO().getFechaInicioVigencia());
				clientePol.setFechaFinVigencia(polClienteMov.getPolizaDTO().getCotizacionDTO().getFechaFinVigencia());
			}
			
			
			clientePolizasList.add(clientePol);
		}
		return clientePolizasList;
	}
	
	public List<NegocioSeguros> getTipoPolizas(){
		return reporteClienteMovilDao.getTipoPolizas();
	}
		
	public void setReporteClienteMovilDao(ReportePolizasClienteMovilDao reporteClienteMovilDao) {
		this.reporteClienteMovilDao = reporteClienteMovilDao;
	}
}