package mx.com.afirme.midas2.service.impl.portal.seguros;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas2.domain.portal.seguros.Departamento;
import mx.com.afirme.midas2.domain.portal.seguros.Oficina;
import mx.com.afirme.midas2.dto.portal.seguros.Comentario;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.portal.seguros.VozClienteService;
import java.util.Arrays;

import java.util.List;

@Stateless
public class VozClienteServiceImpl implements VozClienteService{
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacadeRemote;
	
	@Override
	public boolean enviarComentario(Comentario comentario){
		comentario.setDepartamento(this.obtenerDepartamentoPorId(comentario.getDepartamento().getId()));
		
		List<String> para = Arrays.asList(comentario.getDepartamento().getRecipientes().split(";"));
		List<String> conCopia = Arrays.asList(comentario.getDepartamento().getCopias().split(";"));
		String contenidoCorreo = this.obtenerFormatoEmailPortal(comentario);
		String correoOrigen = this.parametroGeneralFacadeRemote.getValueByCode(ParametroGeneralDTO.PARAMETRO_CORREO_ORIGEN);
		
		try{
			mailService.sendMessageWithExceptionHandler(para, comentario.getAsunto(), contenidoCorreo, correoOrigen, null, null, conCopia, null);
		}catch(Exception ex){
			return false;
		}
		
		return true;
	}
	
	private String obtenerFormatoEmailPortal(Comentario comentario){
		String messageContent = this.parametroGeneralFacadeRemote.getValueByCode(ParametroGeneralDTO.PARAMETRO_FORMATO_CORREO);
		
		messageContent = messageContent.replace("ppVozNombre", comentario.getNombre());
		
		if (!StringUtils.isEmpty(comentario.getTelefono())){
			messageContent = messageContent.replace("ppVozTelefono", comentario.getTelefono());
		}else{
			messageContent = messageContent.replace("ppVozTelefono", "-");
		}
		
		messageContent = messageContent.replace("ppVozCorreo", comentario.getCorreo());
		
		messageContent = messageContent.replace("ppVozComentarios", comentario.getTexto());
		
		messageContent = messageContent.replace("ppVozEstado", comentario.getEstado());
		
		return messageContent;
	}
	
	@Override
	public List<Departamento> obtenerDepartamentos(){
		return entidadService.findAll(Departamento.class);
	}
	
	@Override
	public List<Oficina> obtenerOficinas(){
		return entidadService.findAll(Oficina.class);
	}
	
	private Departamento obtenerDepartamentoPorId(Integer id){
		return entidadService.findById(Departamento.class, id);
	}
}