package mx.com.afirme.midas2.service.impl.wfcfdi;

import java.math.BigDecimal;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.Schedule;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas2.service.wfcfdi.CancelarCFDIAcuseService;

@Stateless
public class CancelarCFDIAcuseServiceImpl implements CancelarCFDIAcuseService {		
	
	private PrintReportClient printReportClient;
	
	@EJB
	private EntidadService entidadService;
	
	private static final Logger LOG = Logger.getLogger(CancelarCFDIAcuseServiceImpl.class);
	
	@EJB
	public void setPrintReportClient(PrintReportClient printReportClient) {
		this.printReportClient = printReportClient;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Schedule(minute="*/45" , hour="*", persistent=false)
	public void cancelarCFDI() {
		
		boolean esActivoParametroCancelacion = this.getValorParameter(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES, ParametroGeneralDTO.CODIGO_PARAM_GENERAL_ACTIVA_DESACTIVA_CANCELACION_CFDI);
		if (!esActivoParametroCancelacion){
			LOG.info("CancelarCFDIAcuseServiceImpl.cancelarCFDI... Parametro inactivo, No se ejecuta el proceso...");
			return;
		}
		
		try {
			LOG.info("entrando a CancelarCFDIAcuseServiceImpl.cancelarCFDI... ");
	        
	        /* 
	         * Proceso de cancelación de CFDIs con acuse
	         */
			printReportClient.cancelarCFDI();   
	        LOG.info("tarea cancelarCFDI ejecutada");
			
		} catch (Exception e) {
			LOG.error("Error general en CancelarCFDIAcuseServiceImpl.cancelarCFDI... ", e);
		}
	}
	
	private boolean getValorParameter(BigDecimal idToGrupoParamGen, BigDecimal codigoParam){
		ParametroGeneralId parametroGeneralId = new ParametroGeneralId();
		parametroGeneralId.setIdToGrupoParametroGeneral(idToGrupoParamGen);
		parametroGeneralId.setCodigoParametroGeneral(codigoParam);
		ParametroGeneralDTO parameter = entidadService.findById(ParametroGeneralDTO.class, parametroGeneralId);
		if(parameter!=null){
			return (parameter.getValor().trim().equals("1"));
		}
		return false;
	}
}