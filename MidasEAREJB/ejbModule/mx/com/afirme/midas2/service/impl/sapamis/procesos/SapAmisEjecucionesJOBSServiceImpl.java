package mx.com.afirme.midas2.service.impl.sapamis.procesos;

import java.util.Date;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.service.sapamis.catalogos.CatSistemasService;
import mx.com.afirme.midas2.service.repuve.RepuveSisService;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.otros.SapAmisUtilsService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisEjecucionesJOBSService;
import mx.com.afirme.midas2.service.sapamis.procesos.SapAmisEjecucionesSTPService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisEmisionService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisPTTService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisPrevencionesService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisRechazosService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisSalvamentoService;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisSiniestrosService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

/*******************************************************************************
 * Nombre Interface: 	SapAmisEjecucionesJOBSServiceImpl.
 * 
 * Descripcion: 		Esta clase ejecuta automaticamente el Proceso completo
 * 						de envio de Datos al SAP-AMIS por medio de un Scheduler.
 * 
 * 						Este procesos se ejecuta cada 2 horas en la horas Nones
 * 						(1,3,5,7, etc.).
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisEjecucionesJOBSServiceImpl implements SapAmisEjecucionesJOBSService{
	private static final long serialVersionUID = 1L;
	
	private static final Logger LOG = Logger.getLogger(SapAmisEjecucionesJOBSServiceImpl.class);
	
	@EJB private SapAmisEjecucionesSTPService sapAmisEjecucionesSTPService;
	@EJB private SapAmisEmisionService sapAmisEmisionService;
	@EJB private SapAmisSiniestrosService sapAmisSiniestrosService;
	@EJB private SapAmisRechazosService sapAmisRechazosService;
	@EJB private SapAmisPrevencionesService sapAmisPrevencionesService;
	@EJB private SapAmisPTTService sapAmisPTTService;
	@EJB private SapAmisSalvamentoService sapAmisSalvamentoService;
	@EJB private SapAmisUtilsService sapAmisUtilsService; 
	@EJB private CatSistemasService catSistemasService; 
	@EJB private RepuveSisService repuveSisService;

	private static final int PROCESO_EXTRACCION = 1;
	private static final int PROCESO_ENVIO = 2;
    private static final Long ESTATUS_PENDIENTE = new Long(1);
    private static final Long OPERACION_ALTA = new Long(1);
    private static final Long OPERACION_MODIFICACION = new Long(2);
    private static final Long OPERACION_CANCELACION = new Long(3);
    private static final Long OPERACION_REHABILITACION = new Long(4);
    private static final String BITACORAS_URL_APLICATIVO = "http://localhost:8080/bitacoras/";
    private static final String BITACORAS_PATH_PROCESOS = "sapamis/procesos/stp/ejecucionesJobsExecute.json";
	
	@Resource	
	private TimerService timerService;

	@Override
	public void todo() {
		this.inicializaProcesosPobladoSTP();
		this.ejecucionProcesosEnvios();
	}

	@Schedule(minute="30", hour="7", dayOfMonth="*", month="*", year="*", persistent= false)
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void ejecucionProcesosSapAmisBitacoras(){
		LOG.info("[SAP-AMIS - PROCESOS] - Inicializan procesos Automaticos del Aplicativo de Bitacoras. ");
		try {
			URL url = new URL(BITACORAS_URL_APLICATIVO + BITACORAS_PATH_PROCESOS);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setRequestMethod("GET");
			conn.setRequestProperty("Accept", "application/json");
			if (conn.getResponseCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
			}
			conn.disconnect();
		} catch (MalformedURLException e) {
			LOG.info("[SAP-AMIS - PROCESOS] - MalformedURLException: " + e.getMessage());
		} catch (IOException e) {
			LOG.info("[SAP-AMIS - PROCESOS] - IOException: " + e.getMessage());
		}
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void inicializaProcesosPobladoSTP(){
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("EMISION"), PROCESO_EXTRACCION)){
			sapAmisEjecucionesSTPService.emisionPoblado(new Date(), new Date());
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("SINIESTRO"), PROCESO_EXTRACCION)){
			sapAmisEjecucionesSTPService.siniestrosPoblado(new Date(), new Date());
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("PPT"), PROCESO_EXTRACCION)){
			sapAmisEjecucionesSTPService.pttPoblado(new Date(), new Date());
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("RECHAZOS"), PROCESO_EXTRACCION)){
			sapAmisEjecucionesSTPService.rechazosPoblado(new Date(), new Date());
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("SALVAMENTO"), PROCESO_EXTRACCION)){
			sapAmisEjecucionesSTPService.salvamentoPoblado(new Date(), new Date());
		}
		sapAmisEjecucionesSTPService.roboPobladoREPUVE(new Date(), new Date());
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private void ejecucionProcesosEnvios(){
		String[] accesos = sapAmisUtilsService.obtenerAccesos();
		ParametrosConsulta parametrosConsulta = new ParametrosConsulta();
		parametrosConsulta.setEstatus(ESTATUS_PENDIENTE);
		parametrosConsulta.setFechaOperacionIni(new Date());
		parametrosConsulta.setFechaOperacionFin(new Date());
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("EMISION"), PROCESO_ENVIO)){
			parametrosConsulta.setOperacion(OPERACION_ALTA);
			sapAmisEmisionService.sendRegSapAmis(accesos, parametrosConsulta);
			parametrosConsulta.setOperacion(OPERACION_MODIFICACION);
			sapAmisEmisionService.sendRegSapAmis(accesos, parametrosConsulta);
			parametrosConsulta.setOperacion(OPERACION_CANCELACION);
			sapAmisEmisionService.sendRegSapAmis(accesos, parametrosConsulta);
			parametrosConsulta.setOperacion(OPERACION_REHABILITACION);
			sapAmisEmisionService.sendRegSapAmis(accesos, parametrosConsulta);
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("SINIESTRO"), PROCESO_ENVIO)){
			parametrosConsulta.setOperacion(OPERACION_ALTA);
			sapAmisSiniestrosService.sendRegSapAmis(accesos, parametrosConsulta);
			parametrosConsulta.setOperacion(OPERACION_MODIFICACION);
			sapAmisSiniestrosService.sendRegSapAmis(accesos, parametrosConsulta);
			parametrosConsulta.setOperacion(OPERACION_CANCELACION);
			sapAmisSiniestrosService.sendRegSapAmis(accesos, parametrosConsulta);
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("PREVENCION"), PROCESO_ENVIO)){
			parametrosConsulta.setOperacion(OPERACION_ALTA);
			sapAmisPrevencionesService.sendRegSapAmis(accesos, parametrosConsulta);
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("PPT"), PROCESO_ENVIO)){
			parametrosConsulta.setOperacion(OPERACION_ALTA);
			sapAmisPTTService.sendRegSapAmis(accesos, parametrosConsulta);
			parametrosConsulta.setOperacion(OPERACION_MODIFICACION);
			sapAmisPTTService.sendRegSapAmis(accesos, parametrosConsulta);
			parametrosConsulta.setOperacion(OPERACION_CANCELACION);
			sapAmisPTTService.sendRegSapAmis(accesos, parametrosConsulta);
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("RECHAZOS"), PROCESO_ENVIO)){
			parametrosConsulta.setOperacion(OPERACION_ALTA);
			sapAmisRechazosService.sendRegSapAmis(accesos, parametrosConsulta);
		}
		if(catSistemasService.estatusProceso(catSistemasService.obtenerIdSistemaPorDescripcion("SALVAMENTO"), PROCESO_ENVIO)){
			parametrosConsulta.setOperacion(OPERACION_ALTA);
			sapAmisSalvamentoService.sendRegSapAmis(accesos, parametrosConsulta);
		}
	}
	
	public void initialize() {
		String timerInfo = "TimerSapAmisEjecucionesJOBS";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(1);
				expression.hour("1/2");
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerSapAmisEjecucionesJOBS", false));
				
				LOG.info("Tarea TimerSapAmisEjecucionesJOBS configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerSapAmisEjecucionesJOBS");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerSapAmisEjecucionesJOBS:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		this.todo();
	}

	@EJB
	private SistemaContext sistemaContext;
}