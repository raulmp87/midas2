package mx.com.afirme.midas2.service.impl.siniestro;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import org.apache.commons.lang.BooleanUtils;
import org.apache.commons.lang.StringUtils;

import mx.com.afirme.midas2.dao.fortimax.FortimaxV2Dao;
import mx.com.afirme.midas2.domain.documentosFortimax.FortimaxDocument;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax.PROCESO_FORTIMAX;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.fortimax.CatalogoAplicacionFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;
import mx.com.afirme.midas2.service.siniestro.AdministradorArchivosService;
import mx.com.afirme.midas2.util.StringUtil;


/**
 * Esta clase maneja la interacci�n los los diferentes WS expuestos por fortimax.
 * @author Armando Garcia 
 * @version 1.0
 * @created 03-jul-2014 05:12:48 p.m.
 */
@Stateless
public class AdministradorArchivosServiceImpl implements AdministradorArchivosService  {

	private static final String DESC_EXISTE_DOCUMENTO = "Ya existe una carpeta/documento de nombre";

	@EJB
	private FortimaxV2Service fortimax;
	
	@EJB
	private FortimaxV2Dao fortimaxDao;
	
	@EJB
    private	CatalogoAplicacionFortimaxService catApFortimaxService ;
	
	public  String REQUIERE_fOLIO = "1";
	 
	/**
	 * @see AdministradorArchivosService#generarExpediente
	 */
	
	@Override
	public boolean generarExpediente(FortimaxDocument document)
			throws Exception {		
		document.inicializar();
		if (StringUtil.isEmpty(document.getAplicacion())){
//			throw new Exception ("Debe propocionar nombre aplicacion [aplicacion]");
		}
		boolean result=false;
			try {
				if(null!= document.getFieldValues()){
					String[] response =fortimax.generateExpedient(document.getAplicacion(), document.getFieldValues());
					 result = BooleanUtils.toBoolean(response[0]);
						if (!result) {
							throw new Exception(String.format("No se pudo crear la gaveta fortimax, "+ response[1]+" ,"+ response[2]));
						}
						return result;
					
				}else{
					String[] response = fortimax.generateExpedient(document.getAplicacion(), document.getFieldValuesGenerico()  );	
					result = BooleanUtils.toBoolean(response[0]);
					System.out.print("RESULT: "+ response[0]+" ,"+ response[1]+" ,"+ response[2]);
					if (!result) {
						throw new Exception(String.format("No se pudo crear la gaveta fortimax, "+ response[1]+" ,"+ response[2]));
					}
					return result;
				}
			} catch (Exception e) {
				throw new Exception (e.getMessage());
			}
	}
	/**
	 * @see AdministradorArchivosService#descargarDocumentoFortimax
	 */
	@Override
	public FortimaxDocument descargarDocumentoFortimax(FortimaxDocument document)
			throws Exception {
			document.setArchivoDescarga(null);
			document.inicializar();
			try {
				TransporteImpresionDTO transporteDTO = new TransporteImpresionDTO ();
				 byte[] bytes =	fortimax.downloadFile(document.getNombreArchivo(), document.getTipoDoc(), document.getAplicacion(), document.getId());
				 transporteDTO.setByteArray(bytes);
				 document.setArchivoDescarga(transporteDTO);
				return document;
			} catch (Exception e) {
				throw new Exception (e.getMessage());
			}
	}
	/**
	 * @see AdministradorArchivosService#eliminarDocumento
	 */
	@Override
	public FortimaxDocument eliminarDocumento(FortimaxDocument document)
			throws Exception {
		document.inicializar();
		return document;
	}
	/**
	 * @see AdministradorArchivosService#generarCarpetaFortimax
	 */
	@Override
	public FortimaxDocument generarCarpetaFortimax(FortimaxDocument document)
			throws Exception {
		document.inicializar();
		return document;
	}
	/**
	 * @see AdministradorArchivosService#generarLigaFortimax
	 */
	@Override
	public FortimaxDocument generarLigaFortimax(FortimaxDocument document,boolean defaultUser)
			throws Exception {
		document.inicializar();
		document.setLigaIfimax(null);
		String documento=null;
		if ( !StringUtil.isEmpty( document.getNombreArchivo())){
			documento = document.getNombreArchivo();
		}
		String  resultado =fortimax.generateLinkToDocument(document.getId(), document.getAplicacion(), documento, null , null, document.getNodo());
		document.setLigaIfimax(resultado);
		return document;
	}
	/**
	 * @see AdministradorArchivosService.loginFortimax
	 */
	@Override
	public FortimaxDocument loginFortimax(FortimaxDocument document)
			throws Exception {
		document.inicializar();
		String token = fortimax.getToken(3600);
		document.setToken(token);
		return document;
	}
	/**
	 * @see AdministradorArchivosService#obtenerDocumentosFortimax
	 */
	@Override
	public FortimaxDocument obtenerDocumentosFortimax(FortimaxDocument document, boolean nodoOnly)
			throws Exception {
		document.inicializar();
		document.setDocumentosGaveta(null);
		Map<String, String> documentos;
		documentos=new LinkedHashMap<String, String>();		
		try {
			if(StringUtil.isEmpty(document.getAplicacion())){
				throw new Exception("Debe proporcionarse Nombre Aplicacion [aplicacion] "); 		
			}else if(null== document.getId() || 0== document.getId() ){
				throw new Exception("Debe proporcionarse ID [id] "); 		
			}			
			String [] resultado =fortimax.getDocumentFortimax(document.getId(), document.getAplicacion());
			
			for( String doc : resultado)	{
				if(!StringUtil.isEmpty(doc)){					
					String nodo=fortimax.getNodo(document.getId(), document.getAplicacion(), doc, "D",nodoOnly);					
					if(!StringUtil.isEmpty(nodo)){						
						documentos.put(nodo,doc);
					}
				}
			}
			document.setDocumentosGaveta(documentos);			
			return document;
		} catch (Exception e) {
			throw new Exception (e.getMessage());
		}
				
	}
	/**
	 * @see AdministradorArchivosService#obtenerTiposDocumentos
	 */
	@Override
	public FortimaxDocument obtenerTiposDocumentos(FortimaxDocument document)
			throws Exception {
		document.inicializar();
		return document;
	}
	/**
	 * @see AdministradorArchivosService#subirArchivoFortimax
	 */
	@Override
	public boolean subirArchivoFortimax(FortimaxDocument document)
			throws Exception {
		document.inicializar();
		return false;
	}
	/**
	 * @see AdministradorArchivosService#subirArchivoFortimax
	 */
	@Override
	public boolean subirArchivoFortimax(FortimaxDocument document, String carpeta)
			throws Exception {
		document.inicializar();
		return false;
	}
	/**
	 * @see AdministradorArchivosService#obtenerDocumentosByPrefijo
	 */
	@Override
	public FortimaxDocument obtenerDocumentosByPrefijo(FortimaxDocument document,String nombreArchivo)
	throws Exception {
		document.inicializar();
		document.setDocumentosGaveta(null);
		Map<String, String> documentos;
		documentos=new LinkedHashMap<String, String>();		
		try {
			this.obtenerDocumentosFortimax(document, true);
			if(!document.getDocumentosGaveta().isEmpty()){
				for (Map.Entry<String, String> entry : document.getDocumentosGaveta().entrySet()) {
					if(entry.getValue().startsWith(nombreArchivo)){
						documentos.put(entry.getKey(),entry.getValue());
					}
				}
				
			}
			document.setDocumentosGaveta(documentos);			
			return document;
		} catch (Exception e) {
			throw new Exception (e.getMessage());
		}
		
	}
	/**
	 * @see AdministradorArchivosService#obtenerDocumentosByCarpeta
	 */
	@Override
	public FortimaxDocument obtenerDocumentosByCarpeta(FortimaxDocument document,String carpeta)
	throws Exception {
		document.inicializar();
		document.setDocumentosGaveta(null);
		Map<String, String> documentos;
		documentos=new LinkedHashMap<String, String>();		
		try {
			this.obtenerDocumentosFortimax(document, false);
			if(!document.getDocumentosGaveta().isEmpty()){
				for (Map.Entry<String, String> entry : document.getDocumentosGaveta().entrySet()) {
					String line  = entry.getKey();
					String nodo = line.substring(0, line.indexOf(" "));
					 line = line.substring(line.indexOf(" "), line.length() );
					line=line.replace("/", "|");
					Pattern p = Pattern.compile("\\|");
					String[] reg =p.split(line);
					if (null!= reg && reg.length>0 && null!=reg[ reg.length -1 ] ){
						if(reg[ reg.length -2].startsWith(carpeta)){							
							documentos.put(nodo, entry.getValue());
						}
					}
				}
			}
			document.setDocumentosGaveta(documentos);			
			return document;
		} catch (Exception e) {
			throw new Exception (e.getMessage());
		}
	}


	/**
	 * @see AdministradorArchivosService#getListaArchivosByProceso
	 */
	@Override
	public Map<String, String> getListaArchivosByProceso(
			PROCESO_FORTIMAX proceso) throws Exception {
		return fortimaxDao.getListaArchivosByProceso(proceso);
	}
	/**
	 * @see AdministradorArchivosService#getDocumentosAlmacenadosByProceso
	 */
	@Override
	public Map<String, String> getDocumentosAlmacenadosByProceso(
			PROCESO_FORTIMAX proceso, Long fortimaxID) throws Exception {
		ProcesosFortimax procesoFX = this.getProceso(proceso);
		CatalogoAplicacionFortimax catApl = new CatalogoAplicacionFortimax ();
		catApl.setId(procesoFX.getIdAplicacion());
		CatalogoAplicacionFortimax catAplicacion= catApFortimaxService.findByFilters(catApl).get(0);//obtener Gaveta		
		return fortimaxDao.getDocumentosAlmacenadosByProceso(proceso, fortimaxID, catAplicacion.getNombreAplicacion());
	}

	/**
	 * @see AdministradorArchivosService#getProceso
	 */
	@Override
	public ProcesosFortimax getProceso(PROCESO_FORTIMAX proceso)
			throws Exception {
		
		return fortimaxDao.getProceso(proceso);
	}
	/**
	 * @see AdministradorArchivosService#generarDocumento
	 */
	@Override
	public boolean generarDocumento(FortimaxDocument document) throws Exception {
		document.inicializar();
		if (StringUtil.isEmpty(document.getAplicacion())){
			throw new Exception ("Debe propocionar nombre aplicacion [aplicacion]");
		}else if  (StringUtil.isEmpty(document.getNombreArchivo())){
			throw new Exception ("Debe propocionar el nombre del documento [nombreArchivo]");
		}else if  (StringUtil.isEmpty(document.getSubcarpeta())){
			throw new Exception ("Debe propocionar el nombre de carpeta [subcarpeta]");
		}
		boolean result=false;
			try {
					String[] response =fortimax.generateDocument(document.getId(), document.getAplicacion(),document.getNombreArchivo(),document.getSubcarpeta());
					document.setDescError (response[1]) ;
					document.setCodError(response[2]);
					document.setResultado(response[0]);
					result = BooleanUtils.toBoolean(response[0]);
						if (!result) {
							throw new Exception(String.format("No es posible Generar Documento, "+ response[1]+" ,"+ response[2]));
						}
						return result;
				
			} catch (Exception e) {
				throw new Exception (e.getMessage());
			}
	}
	/**
	 * @see AdministradorArchivosService#generarLigaByProceso
	 */
	@Override
	public String generarLigaByProceso(PROCESO_FORTIMAX proceso,
			Long idFortimax, String documento,boolean defaultUser) throws Exception {	
		FortimaxDocument document = new FortimaxDocument();	
		CatalogoAplicacionFortimax catAp = new CatalogoAplicacionFortimax ();
		ProcesosFortimax procesoFx = this.getProceso(proceso);//Traer Proceso 			
		catAp.setId(procesoFx.getIdAplicacion());
		CatalogoAplicacionFortimax app= catApFortimaxService.findByFilters(catAp).get(0);//obtener Gaveta
		document.setAplicacion(app.getNombreAplicacion());			
		document.setId(idFortimax);
		document.setNombreArchivo(documento);			
		document.inicializar();
		document.setLigaIfimax(null);
		String doc=null;
		if ( !StringUtil.isEmpty( document.getNombreArchivo())){
			doc = document.getNombreArchivo();
		}
		String  resultado =fortimax.generateLinkToDocumentByProceso (document.getId(), document.getAplicacion(), doc,defaultUser,proceso, document.getNodo());
		document.setLigaIfimax(resultado);
		return  resultado;
	}
	/**
	 * @see AdministradorArchivosService#generarDocumentoByProceso
	 */
	@Override
	public FortimaxDocument generarDocumentoByProceso(PROCESO_FORTIMAX proceso,Long idFortimax, String folio)throws Exception {		
		//INICIALIZAR VARIABLES
		FortimaxDocument documento = new FortimaxDocument();	
		CatalogoAplicacionFortimax gaveta = new CatalogoAplicacionFortimax ();
		//OBTENER PROCESO Y APLICACION 
		ProcesosFortimax fortimaxProceso= this.getProceso(proceso);//Traer Proceso 			
		gaveta.setId(fortimaxProceso.getIdAplicacion());
		CatalogoAplicacionFortimax aplicacion= catApFortimaxService.findByFilters(gaveta).get(0);//obtener Gaveta
		documento.setAplicacion(aplicacion.getNombreAplicacion());			
		documento.setId(idFortimax); 
		 // GENERAR DOCUMENTO ENVIAR NOMBRE DE DOCUMENTO A GENERAR
		documento.setSubcarpeta(fortimaxProceso.getRuta());
		//INICALIZAR DOCUMENTO FORTIMAX
		if ( !StringUtil.isEmpty( fortimaxProceso.getTipo()) && fortimaxProceso.getTipo().equalsIgnoreCase(PROCESO_TIPO_GENERICO)   ){
			documento.setPrefijo(fortimaxProceso.getNombreArchivo()); 
			String codigo=this.generarCodigo(documento);
			if ( StringUtil.isEmpty(codigo) ){
				codigo= fortimaxProceso.getNombreArchivo().trim()+SEPARADOR+"1";
			} 
				if ( (!StringUtil.isEmpty(folio))  && (null!=fortimaxProceso.getRequiereFolio() && fortimaxProceso.getRequiereFolio().toString().equalsIgnoreCase(REQUIERE_fOLIO))){
					codigo=fortimaxProceso.getNombreArchivo()+SEPARADOR+folio;
				}
				documento.setNombreArchivo(codigo);
			try {
				this.generarDocumento(documento);
			} catch (Exception e) {
				if (StringUtils.isNotBlank(documento.getCodError())) {
					if (documento.getCodError().equalsIgnoreCase(ERROR_EXISTE_DOCUMENTO)) {
						return documento;
					}
				}
				if (StringUtils.isNotBlank(documento.getDescError())) {
					if (documento.getDescError().equalsIgnoreCase(ERROR_EXISTE_DOCUMENTO)
							|| documento.getDescError().contains(DESC_EXISTE_DOCUMENTO)) {
						return documento;
					}
				}
				throw new Exception(String.format("No es posible Generar Documento, "+ e.getMessage()), e);
			}
			return documento;
			
		}else if (!StringUtil.isEmpty( fortimaxProceso.getTipo()) && fortimaxProceso.getTipo().equalsIgnoreCase(PROCESO_TIPO_LISTA) ){
			Map<String, String> archivos =new LinkedHashMap<String, String>();	
			archivos=this.getListaArchivosByProceso(proceso);
			String error= "";
			String descError="";
			for (Map.Entry<String, String> entry : archivos.entrySet()) {
				documento.setNombreArchivo(entry.getValue());
				try {
					this.generarDocumento(documento);
				} catch (Exception e){
				    if (StringUtils.isNotBlank(documento.getCodError())) {
						if (documento.getCodError().equalsIgnoreCase(ERROR_EXISTE_DOCUMENTO)) {
							error +="["+entry.getValue()+"] "+documento.getCodError();
							descError+="["+entry.getValue()+"] "+documento.getDescError();
							continue;
						}
					}
					if (StringUtils.isNotBlank(documento.getDescError())) {
						if (documento.getDescError().equalsIgnoreCase(ERROR_EXISTE_DOCUMENTO)
								|| documento.getDescError().contains(DESC_EXISTE_DOCUMENTO)) {
							error +="["+entry.getValue()+"] "+documento.getCodError();
							descError+="["+entry.getValue()+"] "+documento.getDescError();
							continue;
						}
					}
					throw new Exception(String.format("No es posible Generar Documento, "+ e.getMessage()), e);
				}
				
				
			}
			if ( StringUtil.isEmpty(error) )
				error=null;
				
			if ( StringUtil.isEmpty(descError) )
				descError=null;
			documento.setDescError(descError);
			documento.setCodError(error);
			return documento;
			
		}else return documento;
		
			
	}
	/**
	 * @see AdministradorArchivosService#generarCodigo
	 */
	@Override
	public String generarCodigo(FortimaxDocument document) throws Exception {
		this.obtenerDocumentosByPrefijo(document, document.getPrefijo());
		String codigo = null;
		if (!document.getDocumentosGaveta().isEmpty()){
			List<Long> listaNumero = new ArrayList<Long>();
			for (Map.Entry<String, String> entry : document.getDocumentosGaveta().entrySet()) {
				String line  = entry.getValue().trim();
				String numero ="";
				try{
				 numero=line.substring(line.indexOf(SEPARADOR)+1, line.length() ).trim();
				}catch (Exception e) {
					continue;
				}
				if(StringUtil.isNumeric(numero)){
					listaNumero.add(new Long (numero));
				}
			}
			Long mayor = new Long(-99999);
			for (Long n: listaNumero ){
				if (n>mayor){
					mayor = n;
				}
			}
			
			if (mayor > 0){
				mayor+=1;
				codigo = document.getPrefijo().trim().concat(SEPARADOR+mayor);
			}
			
				
		}
		return codigo;
	}
	@Override
	public String getNodo(Long id, String tituloAplicacion,
			String nombreElemento, String tipoElemento, boolean nodoOnly)
			throws Exception {
		return fortimax.getNodo(id, tituloAplicacion, nombreElemento, tipoElemento, nodoOnly);
	}

	






}