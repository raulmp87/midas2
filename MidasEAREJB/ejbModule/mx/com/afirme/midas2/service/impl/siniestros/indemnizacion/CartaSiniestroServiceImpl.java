package mx.com.afirme.midas2.service.impl.siniestros.indemnizacion;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.tools.ant.types.CommandlineJava.SysProperties;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.movil.cliente.EnvioCaratulaParameter;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ConfiguracionCalculoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteRoboSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestroEndoso;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.CartaSiniestroFactura;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.InfoContratoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.ReporteSiniestroRoboDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.CartaBajaPlacasDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.CartaPTSNoDocumentadaDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.FiniquitoInformacionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.FormatoFechasPT;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.ImpresionCartaFiniquitoDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.ImpresionEntregaDocumentosDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal.DeterminacionInformacionSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.perdidatotal.ImpresionDeterminacionPerdidaTotalDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.CartaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import com.js.util.StringUtil;

@Stateless
public class CartaSiniestroServiceImpl implements CartaSiniestroService{
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	public MailService mailService;
	@EJB
	private PerdidaTotalService perdidaTotalService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	@EJB
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	
	@EJB
	private EndosoService endosoService;
	
	@EJB
	private ReporteCabinaService reporteCabinaService;
	
	@EJB
	private SiniestroCabinaService siniestroCabinaService;
		
	@EJB
    private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService;
	
	private static final int MIDAS_APP_ID = 5;
	private static final String PARAMETRO_GLOBAL_APODERADO = "APODERADO_LEGAL_FINIQUITO";
	
	private static final String CARTA_PERDIDA_TOTAL = "PERDIDA TOTAL";
	private static final String CARTA_ROBO_TOTAL = "ROBO TOTAL";
	private static final Integer NUMERO_TENENCIAS = 5;
	private static final String CARTA_BAJA_PLACAS =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaBajaPlacasPerdidaTotal.jrxml";
	private static final String CARTA_RECEPCION_DOCUMENTOS = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/RecepcionDocumentosSiniestro.jrxml";
	private static final String CARTA_NOTIFICACION_PERDIDA_TOTAL = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/NotificacionPerdidaTotal.jrxml";
	private static final String CARTA_DETERMINACION_PERDIDA_TOTAL =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/DeterminacionPerdidaTotal.jrxml";
	private static final String CARTA_DETERMINACION_PERDIDA_TOTAL_AUTORIZACIONES =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/DeterminacionAutorizaciones.jrxml";
	private static final String CARTA_FINIQUITO_ASEGURADO =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/FiniquitoAsegurado.jrxml";
	private static final String CARTA_FINIQUITO_TERCERO =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/FiniquitoTerceros.jrxml";
	private static final String CARTA_ENTREGA_PERDIDA =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/EntregaDocumentosPerdidaTotal.jrxml";
	private static final String CARTA_ENTREGA_ROBO =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/EntregaDocumentosRoboTotal.jrxml";
	private static final String CARTA_PTS_NO_DOCUMENTADA =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaPTSNoDocumentadas.jrxml";
	private static final String CARTA_PTS_NO_DOCUMENTADA_TERCERO = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CartaPTSNoDocumentadasTercero.jrxml";
	private static final String CONTRATO_SALVAMENTO = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ContratoSiniestro.jrxml";
	
	private static final String AUTORIZACION_PERDIDA_TOTAL = "AT";

	private static final String BAJA_PLACAS_PERDIDA_TOTAL = "BPPT";
	private static final String BAJA_PLACAS_ROBO_TOTAL = "BPRT";
	private static final String TIPO_CARTA_FINIQUITO = "CSFI";
	private static final String TIPO_CARTA_ENTREGA_DOCUMENTOS = "CEDC";
	private static final String CODIGO_TRANSFERENCIA_BANCARIA = "TB";
	private static final String CARTA_FORMATO_FECHAS_PT = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/FormatoFechasPT.jrxml";
	private static final String CARTA_FORMATO_FECHAS_PT_TIPO = "CFFPT";
	private static final String ORDER_BY_ID = "id";
	private final long MODO_EJECUCION_IMPRIMIR = 1;
	private TransporteImpresionDTO transporte;
	private static final String	TIPO_PDF	= "application/pdf";
	private static final String	EXT_PDF	= ".pdf";
	@Override
	public TransporteImpresionDTO imprimirCartaBajaPlacas(CartaBajaPlacasDTO bajaPlacasDTO){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		if(bajaPlacasDTO.getTipoCarta().compareTo(BAJA_PLACAS_PERDIDA_TOTAL)==0){
			bajaPlacasDTO.setNombreTipoCarta(CARTA_PERDIDA_TOTAL);
		}else if(bajaPlacasDTO.getTipoCarta().compareTo(BAJA_PLACAS_ROBO_TOTAL)==0){
			bajaPlacasDTO.setNombreTipoCarta(CARTA_ROBO_TOTAL);
		}else{
			return null;
		}
		JasperReport jReport = gImpresion.getOJasperReport(CARTA_BAJA_PLACAS);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("nombreTipoCarta", bajaPlacasDTO.getNombreTipoCarta());
		params.put("personaAQuienSeDirige", bajaPlacasDTO.getPersonaAQuienSeDirige());
		params.put("informacionAuto", generarInformacionAuto(bajaPlacasDTO));
		params.put("informacionCapturada", generarInformacionCapturada(bajaPlacasDTO));
		params.put("nombreUsuario", bajaPlacasDTO.getNombreUsuario());
		params.put("puestoUsuario", bajaPlacasDTO.getPuestoUsuario());
		
		return gImpresion.getTImpresionDTO(jReport, new JREmptyDataSource(), params);
	}
	
	/**
	 * Crea el String para generar la frase con la informacion del auto para la carta de solicitud de baja de placas
	 * @param bajaPlacasDTO
	 * @return
	 */
	private String generarInformacionAuto(CartaBajaPlacasDTO bajaPlacasDTO){
		StringBuilder informacionAuto = new StringBuilder();
		if(bajaPlacasDTO.getTipoCarta().compareTo(BAJA_PLACAS_PERDIDA_TOTAL)==0){
			informacionAuto.append("   Por medio de la presente se hace constar que el vehículo Marca: ");
			informacionAuto.append(bajaPlacasDTO.getMarcaDesc());
			informacionAuto.append(" Tipo: ");
			informacionAuto.append(bajaPlacasDTO.getDescTipoUso());
			informacionAuto.append(" Modelo: ");
			informacionAuto.append(bajaPlacasDTO.getModelo());
			informacionAuto.append(" Número de Serie: ");
			informacionAuto.append(bajaPlacasDTO.getNumeroSerie());
			informacionAuto.append(" con placas de circulación ");
			informacionAuto.append(bajaPlacasDTO.getPlaca());
			informacionAuto.append(" del Estado de ");
			informacionAuto.append(bajaPlacasDTO.getEstado());
			informacionAuto.append(" sufrió un accidente vial el día ");
			informacionAuto.append(new SimpleDateFormat("EEEE dd 'de' MMMM 'de' yyyy",new Locale("es")).format(bajaPlacasDTO.getFechaSiniestro()));
			informacionAuto.append(". El cual derivado de la magnitud de los daños que este presenta fue considerado Pérdida Total por nuestra Institución.");
			return informacionAuto.toString();
		}else if(bajaPlacasDTO.getTipoCarta().compareTo(BAJA_PLACAS_ROBO_TOTAL)==0){
			informacionAuto.append("   Por medio de la presente hacemos de su conocimiento que esta compañía Aseguradora ha recibido reporte de Robo Total del vehículo Marca: ");
			informacionAuto.append(bajaPlacasDTO.getMarcaDesc());
			informacionAuto.append(" Tipo: ");
			informacionAuto.append(bajaPlacasDTO.getDescTipoUso());
			informacionAuto.append(" Modelo: ");
			informacionAuto.append(bajaPlacasDTO.getModelo());
			informacionAuto.append(" Número de Serie: ");
			informacionAuto.append(bajaPlacasDTO.getNumeroSerie());
			informacionAuto.append(" con placas de circulación: ");
			informacionAuto.append(bajaPlacasDTO.getPlaca());
			informacionAuto.append(" del Estado de ");
			informacionAuto.append(bajaPlacasDTO.getEstado());
			informacionAuto.append(" el cual por dicho del segurado a través de averiguación precia No. ");
			informacionAuto.append(bajaPlacasDTO.getNumeroAveriguacionPrevia());
			informacionAuto.append(" levantada ante el C. Agente del Ministerio Público ");
			informacionAuto.append(bajaPlacasDTO.getAgenteMinisterio());
			informacionAuto.append(" ocurrió el día ");
			informacionAuto.append(new SimpleDateFormat("dd/MM/yyyy",new Locale("es")).format(bajaPlacasDTO.getFechaSiniestro()));
			informacionAuto.append(". Por lo que en base a lo anterior se procederá a realizar la indemnización correspondiente.");
			return informacionAuto.toString();
		}else{
			return null;
		}
	}
	
	/**
	 * Crea el String para generar la frase con la informacion capturada para la carta de solicitud de baja de placas
	 * @param bajaPlacasDTO
	 * @return
	 */
	private String generarInformacionCapturada(CartaBajaPlacasDTO bajaPlacasDTO){
		StringBuilder informacionCapturada= new StringBuilder();
		informacionCapturada.append("   Se extiende la presente solicitud de ");
		informacionCapturada.append(bajaPlacasDTO.getDescripcionSol());
		informacionCapturada.append(" y/o ");
		informacionCapturada.append(bajaPlacasDTO.getDescripcionSolComp());
		informacionCapturada.append(" para que surta los efectos legales y fiscales correspondientes.");
		return informacionCapturada.toString();
	}

	@Override
	public TransporteImpresionDTO imprimirRecepcionDocumentosSiniestro(String numeroSiniestro, RecepcionDocumentosSiniestro recepcionDoctoSin, CartaBajaPlacasDTO informacionAuto){
		GeneradorImpresion gImpresion 		= new GeneradorImpresion();
		JasperReport jReport 				= gImpresion.getOJasperReport(CARTA_RECEPCION_DOCUMENTOS);
		
		recepcionDoctoSin = entidadService.findById(RecepcionDocumentosSiniestro.class, recepcionDoctoSin.getId());
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("rutaImagenCheckOn", SistemaPersistencia.CHECKBOX_ON);
		params.put("rutaImagenCheckOff", SistemaPersistencia.CHECKBOX_OFF);
		params.put("rutaImagenRadioOn", SistemaPersistencia.RADIO_BUTTON_ON);
		params.put("rutaImagenRadioOff", SistemaPersistencia.RADIO_BUTTON_OFF);
		params.put("rutaImagenPixel", SistemaPersistencia.PIXEL_IMG);
		
		params.put("numSiniestro",numeroSiniestro);
		params.put("vehiculoAfectado",informacionAuto.getTipoCarta());
		params.put("marca", informacionAuto.getMarcaDesc());
		params.put("tipo", informacionAuto.getDescTipoUso());
		params.put("anio", informacionAuto.getModelo() != null ? informacionAuto.getModelo().toString() : "");
		params.put("ubicacion",recepcionDoctoSin.getUbicacion());
		params.put("direccion",recepcionDoctoSin.getDireccion());
		params.put("disposicion",recepcionDoctoSin.getDisposicion());
		params.put("pension",recepcionDoctoSin.getPension());
		params.put("banco",buscarNombreBancoPorId(recepcionDoctoSin.getBancoId()));
		params.put("clabe",recepcionDoctoSin.getClabeBanco());
		params.put("usuario",usuarioService.getUsuarioActual().getNombreCompleto());
		params.put("comentarios",recepcionDoctoSin.getComentarios());
		params.put("juegoLlaves",recepcionDoctoSin.getJuegoLlaves().toString());
		params.put("seguimientoFactura",recepcionDoctoSin.getSeguimientoFactura());
		params.put("endosoAfirme",recepcionDoctoSin.getEndosoAfirme());
		params.put("denunciaRobo",recepcionDoctoSin.getDenunciaRobo());
		params.put("formaPago",recepcionDoctoSin.getFormaPago());
		params.put("tipoPersona",recepcionDoctoSin.getTipoPersona());
		params.put("facturaOriginal",recepcionDoctoSin.getFactruraOriginal());
		params.put("bajaPlacas",recepcionDoctoSin.getBajaPlacas());
		params.put("tenencias",recepcionDoctoSin.getRefrendoTenencia());
		params.put("beneficiario", recepcionDoctoSin.getBeneficiario());
		params.put("rfc", recepcionDoctoSin.getRfc());
		params.put("telefonoNumero", recepcionDoctoSin.getTelefonoNumero());
		params.put("telefonoLada", recepcionDoctoSin.getTelefonoLada());
		params.put("correo", recepcionDoctoSin.getCorreo());
		
		
		return gImpresion.getTImpresionDTO(jReport, new JREmptyDataSource(), params); 
	}
	
	/**
	 * Busca el nombre del banco dependiendo de la id que se le pase como parametro
	 * @param bancoId
	 * @return
	 */
	private String buscarNombreBancoPorId(Integer bancoId){
		String nombreBanco = "";
		
		if(bancoId != null){
			BancoMidas banco = entidadService.findById(BancoMidas.class, bancoId.longValue());
			if(banco != null){
				nombreBanco = banco.getNombre();
			}
		}
		
		return nombreBanco;
	}
	
	@Override
	public CartaBajaPlacasDTO obtenerDatosBajaPlacas(Long idIndemnizacion, String tipoCarta, CartaSiniestro informacionCapturada){
		CartaBajaPlacasDTO bajaPlacasDTO = new CartaBajaPlacasDTO();
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		if( indemnizacion != null &&
				indemnizacion.getSiniestro() != null &&
				indemnizacion.getSiniestro().getReporteCabina() != null &&
				indemnizacion.getSiniestro().getReporteCabina().getPoliza() !=null){
					bajaPlacasDTO.setTipoCarta(tipoCarta);
					Boolean esCoberturaAsegurado = esCoberturaAsegurado(indemnizacion.getId());
					if(esCoberturaAsegurado){
						AutoIncisoReporteCabina autoIncisoReporteCabina = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(indemnizacion.getSiniestro().getReporteCabina().getId());
						bajaPlacasDTO.setMarcaDesc(autoIncisoReporteCabina.getDescMarca());
						bajaPlacasDTO.setDescTipoUso(autoIncisoReporteCabina.getDescripcionFinal());
						bajaPlacasDTO.setModelo(autoIncisoReporteCabina.getModeloVehiculo() != null ? autoIncisoReporteCabina.getModeloVehiculo().toString() : null);
						bajaPlacasDTO.setNumeroSerie(autoIncisoReporteCabina.getNumeroSerie());
						bajaPlacasDTO.setPlaca(autoIncisoReporteCabina.getPlaca());
					}else{
						EstimacionCoberturaReporteCabina estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
						TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
						if(tercero != null){
							bajaPlacasDTO.setModelo((tercero.getModeloVehiculo() != null)? tercero.getModeloVehiculo().toString() : null);
							bajaPlacasDTO.setNumeroSerie(( !StringUtil.isEmpty(  tercero.getNumeroSerie()))? tercero.getNumeroSerie() : null);
							bajaPlacasDTO.setDescTipoUso(((tercero.getEstiloVehiculo() != null)? tercero.getEstiloVehiculo(): null));
							bajaPlacasDTO.setMarcaDesc(((tercero.getMarca() != null)? tercero.getMarca(): null));
							bajaPlacasDTO.setPlaca(((tercero.getPlacas() != null)? tercero.getPlacas(): null));
						}
					}
					bajaPlacasDTO.setFechaSiniestro(indemnizacion.getSiniestro().getReporteCabina().getFechaOcurrido());
					Usuario usuarioActual = usuarioService.getUsuarioActual();
					bajaPlacasDTO.setNombreUsuario(usuarioActual.getNombreCompleto());
					bajaPlacasDTO.setPuestoUsuario(usuarioActual.getRoles().get(0).getDescripcion());
					bajaPlacasDTO.setDescripcionSol(informacionCapturada.getDescripcionSol());
					bajaPlacasDTO.setDescripcionSolComp(informacionCapturada.getDescSolComplementaria());
					bajaPlacasDTO.setEstado(informacionCapturada.getEstadoTenencia());
					if(tipoCarta.compareTo(BAJA_PLACAS_ROBO_TOTAL) == 0){
						ReporteSiniestroRoboDTO filtroRobo = new ReporteSiniestroRoboDTO();
						filtroRobo.setServParticular(false); filtroRobo.setServPublico(false);
						filtroRobo.setClaveOficina(indemnizacion.getSiniestro().getReporteCabina().getClaveOficina());
						filtroRobo.setConsecutivoReporte(indemnizacion.getSiniestro().getReporteCabina().getConsecutivoReporte());
						filtroRobo.setAnioReporte(indemnizacion.getSiniestro().getReporteCabina().getAnioReporte());
						List<ReporteSiniestroRoboDTO> resultados = reporteCabinaService.buscarReportesRobo(filtroRobo);
						if(resultados != null && resultados.size() > 0 ){
							Long idReporteRobo = resultados.get(0).getReporteRoboId();
							ReporteRoboSiniestro reporteRobo = entidadService.findById(ReporteRoboSiniestro.class, idReporteRobo);
							bajaPlacasDTO.setAgenteMinisterio(reporteRobo.getNombreAgenteMinisterioPublico());
							bajaPlacasDTO.setNumeroAveriguacionPrevia(reporteRobo.getNumeroAveriguacionDen());
						}
					}
			}
		return bajaPlacasDTO;
	}
	
	@Override
	public List<String> validarBajaPlacas(CartaBajaPlacasDTO bajaPlacasDTO, String tipoBajaPlacas){
		List<String> camposIncorrectos = null;
		
		if(bajaPlacasDTO != null){
			camposIncorrectos = new ArrayList<String>();
			if(tipoBajaPlacas.compareTo(BAJA_PLACAS_PERDIDA_TOTAL) == 0){
				
				if( StringUtil.isEmpty(bajaPlacasDTO.getDescTipoUso() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.tipo");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getEstado() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.edoplacas");}
				if( bajaPlacasDTO.getFechaSiniestro() == null){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.fechaocurr");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getMarcaDesc() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.marca");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getModelo() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.modelo");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getNumeroSerie() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.numeroserie");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getPlaca() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.placas");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getDescripcionSol() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.descsol");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getDescripcionSolComp() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.descsolcomp");}
			
			}else if (tipoBajaPlacas.compareTo(BAJA_PLACAS_ROBO_TOTAL) == 0){
				
				if( StringUtil.isEmpty(bajaPlacasDTO.getDescTipoUso() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.tipo");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getEstado() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.edoplacas");}
				if( bajaPlacasDTO.getFechaSiniestro() == null){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.fechaocurr");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getMarcaDesc() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.marca");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getModelo() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.modelo");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getNumeroSerie() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.numeroserie");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getPlaca() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.placas");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getDescripcionSol() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.descsol");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getDescripcionSolComp() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.descsolcomp");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getAgenteMinisterio() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.agenteministerio");}
				if( StringUtil.isEmpty(bajaPlacasDTO.getNumeroAveriguacionPrevia() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.cartasiniestro.numerodenuncia");}
			}
		}	
		return camposIncorrectos;
	}
	
	@Override
	public List<String> validarEntregaDocumentos(CartaSiniestro informacionCapturada, String tipoCarta, Long modoEjecucion){
		List<String> camposIncorrectos = new ArrayList<String>();
		if(informacionCapturada != null){
			if(tipoCarta.compareTo(BAJA_PLACAS_PERDIDA_TOTAL) == 0){
				if( modoEjecucion == this.MODO_EJECUCION_IMPRIMIR){
					if( !validarEntregaDocumentosTieneFactura(informacionCapturada) ){
						camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.errorfactura");}}
				if( StringUtil.isEmpty(informacionCapturada.getFolioBajaPlacas() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.bajaplacasnumfolio");}
				if( StringUtil.isEmpty(informacionCapturada.getConstanciaPagoTenencia() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.constanciapagotenencia");}
			
			}else if (tipoCarta.compareTo(BAJA_PLACAS_ROBO_TOTAL) == 0){
				if( modoEjecucion == this.MODO_EJECUCION_IMPRIMIR){
					if( !validarEntregaDocumentosTieneFactura(informacionCapturada) ){
						camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.errorfactura");}}
				if( StringUtil.isEmpty(informacionCapturada.getConstanciaBajaPlacas() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.bajaplacasconstancia");}
				if( StringUtil.isEmpty(informacionCapturada.getFolioBajaPlacas() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.bajaplacasnumfolio");}
				if( StringUtil.isEmpty(informacionCapturada.getConstanciaPagoTenencia() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.altatenencia");}
				if( StringUtil.isEmpty(informacionCapturada.getFechaTenenciaInicial() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.tenenciadel");}
				if( StringUtil.isEmpty(informacionCapturada.getFechaTenenciaFin() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.tenenciaal");}
				if( StringUtil.isEmpty(informacionCapturada.getEstadoTenencia() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.tenenciaestado");}
				if( StringUtil.isEmpty(informacionCapturada.getNumeroAveriguacion() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.numaveriguacionrobo");}
				if( StringUtil.isEmpty(informacionCapturada.getLlavesEntregadas() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.numerollaves");}
				if( StringUtil.isEmpty(informacionCapturada.getFolioAltaTenencia() )){
					camposIncorrectos.add("midas.siniestros.indemnizacion.entregadoctos.altatenencianumfolio");}
			}
		}	
		return camposIncorrectos;
	}
	
	/**
	 * Valida si el usuario ha capturado la factura para la carta de entrega de documentos
	 * @param informacionCapturada
	 * @return
	 */
	private boolean validarEntregaDocumentosTieneFactura(CartaSiniestro informacionCapturada){
		boolean validaTieneFactura = false;
		List<CartaSiniestro> resultado = entidadService.findByProperty(CartaSiniestro.class, "id",informacionCapturada.getId());
		if(resultado != null
				&& !resultado.isEmpty()){
			CartaSiniestro cartaEntrega = resultado.get(0);
			List<CartaSiniestroFactura> facturas = cartaEntrega.getFacturas();
			if(facturas != null 
					&& !facturas.isEmpty()){
				validaTieneFactura = true;
			}
		}
		
		return validaTieneFactura;
	}
	
	@Override
	public List<String>	validarRecepcionDocumentos(RecepcionDocumentosSiniestro recepcionDoctoSin, CartaBajaPlacasDTO informacionAuto, String numeroSiniestro, String tipoCarta){
		List<String> camposIncorrectos = new ArrayList<String>();
		if(informacionAuto != null){
			if(recepcionDoctoSin.getJuegoLlaves() == null || recepcionDoctoSin.getJuegoLlaves() == 0){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.juegollaves");}
			if(recepcionDoctoSin.getRefrendoTenencia() == null || recepcionDoctoSin.getRefrendoTenencia().isEmpty()){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.refrendotenencia");}
			if(recepcionDoctoSin.getBajaPlacas() == null || recepcionDoctoSin.getBajaPlacas().isEmpty()){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.bajaplacas");}
			if(recepcionDoctoSin.getTipoPersona() == null || recepcionDoctoSin.getTipoPersona().isEmpty()){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.tipopersona");}
			if(recepcionDoctoSin.getFactruraOriginal() == null || recepcionDoctoSin.getFactruraOriginal().isEmpty()){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.facturaoriginal");}
			if(recepcionDoctoSin.getSeguimientoFactura() == null || recepcionDoctoSin.getSeguimientoFactura().isEmpty()){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.seguimientofactura");}
			if(recepcionDoctoSin.getEndosoAfirme() == null || recepcionDoctoSin.getEndosoAfirme().isEmpty()){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.endosoafirme");}
			if(recepcionDoctoSin.getDenunciaRobo() == null || recepcionDoctoSin.getDenunciaRobo().isEmpty()){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.denunciarobo");}
			if(recepcionDoctoSin.getFormaPago() == null || recepcionDoctoSin.getFormaPago().isEmpty()){
				camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.formapago");}
			if(recepcionDoctoSin.getFormaPago() != null && recepcionDoctoSin.getFormaPago().compareTo(CODIGO_TRANSFERENCIA_BANCARIA) == 0){
				if(recepcionDoctoSin.getBancoId() == null || recepcionDoctoSin.getBancoId() == 0){
					camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.banco");}
				if(recepcionDoctoSin.getClabeBanco() == null || recepcionDoctoSin.getClabeBanco().isEmpty()){
					camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.clabe");}
				if(recepcionDoctoSin.getBeneficiario() == null || recepcionDoctoSin.getBeneficiario().isEmpty()){
					camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.beneficiario");}
				if(recepcionDoctoSin.getRfc() == null || recepcionDoctoSin.getRfc().isEmpty()){
					camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.rfc");}
				if(recepcionDoctoSin.getCorreo() == null || recepcionDoctoSin.getCorreo().isEmpty()){
					camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.correo");}
				if(recepcionDoctoSin.getTelefonoLada() == null || recepcionDoctoSin.getTelefonoLada().isEmpty()){
					camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.telefono");}
				if(recepcionDoctoSin.getTelefonoNumero() == null || recepcionDoctoSin.getTelefonoNumero().isEmpty()){
					camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.telefono");}
			}
		}else{
			camposIncorrectos.add("midas.siniestros.indemnizacion.recepciondoctos.errorinfoauto");
		}
		
		return camposIncorrectos;
	}

	@Override
	public RecepcionDocumentosSiniestro mostrarRecepcionDeDocumentos(Long idIndemnizacion) {
		List<RecepcionDocumentosSiniestro> 	resultados;
		RecepcionDocumentosSiniestro 		 recepcionDoctos = null;
		
		resultados = entidadService.findByProperty(RecepcionDocumentosSiniestro.class, "indemnizacion.id", idIndemnizacion);
		if(resultados != null && !resultados.isEmpty()){
			recepcionDoctos = resultados.get(0);
		}
		else{
			recepcionDoctos = new RecepcionDocumentosSiniestro();
			IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			recepcionDoctos.setSiniestroCabina(indemnizacion.getSiniestro());
			recepcionDoctos.setIndemnizacion(indemnizacion);
			recepcionDoctos.setEsPagoDanios(indemnizacion.getEsPagoDanios());
		}
		return recepcionDoctos;
	}
	
	@Override
	public List<String> guardarRecepcionDocumentos(RecepcionDocumentosSiniestro recepcionCapturada, Long idIndemnizacion){
		List<String> mensajes 					= new ArrayList<String>();
		RecepcionDocumentosSiniestro entidad 	= null;
		List<RecepcionDocumentosSiniestro> 	resultados;
		
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		resultados = entidadService.findByProperty(RecepcionDocumentosSiniestro.class, "indemnizacion.id", idIndemnizacion);
		if(resultados != null && !resultados.isEmpty()){
			entidad = resultados.get(0);
			entidad.setBajaPlacas(recepcionCapturada.getBajaPlacas());
			entidad.setBancoId(recepcionCapturada.getBancoId());
			entidad.setClabeBanco(recepcionCapturada.getClabeBanco());
			entidad.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			entidad.setComentarios(recepcionCapturada.getComentarios());
			entidad.setDenunciaRobo(recepcionCapturada.getDenunciaRobo());
			entidad.setDireccion(recepcionCapturada.getDireccion());
			entidad.setDisposicion(recepcionCapturada.getDisposicion());
			entidad.setEndosoAfirme(recepcionCapturada.getEndosoAfirme());
			entidad.setEsPagoDanios(recepcionCapturada.getEsPagoDanios());
			entidad.setFactruraOriginal(recepcionCapturada.getFactruraOriginal());
			entidad.setFechaModificacion(new Date());
			entidad.setFormaPago(recepcionCapturada.getFormaPago());
			entidad.setJuegoLlaves(recepcionCapturada.getJuegoLlaves());
			entidad.setPension(recepcionCapturada.getPension());
			entidad.setRefrendoTenencia(recepcionCapturada.getRefrendoTenencia());
			entidad.setSeguimientoFactura(recepcionCapturada.getSeguimientoFactura());
			entidad.setTipoPersona(recepcionCapturada.getTipoPersona());
			entidad.setUbicacion(recepcionCapturada.getUbicacion());
			entidad.setBeneficiario(recepcionCapturada.getBeneficiario());
			entidad.setRfc(recepcionCapturada.getRfc());
			entidad.setCorreo(recepcionCapturada.getCorreo());
			entidad.setTelefonoLada(recepcionCapturada.getTelefonoLada());
			entidad.setTelefonoNumero(recepcionCapturada.getTelefonoNumero());
			indemnizacion.setRecepcionDocumentosSiniestro(entidadService.save(entidad));
		}else{
			SiniestroCabina siniestro = entidadService.findById(SiniestroCabina.class, recepcionCapturada.getSiniestroCabina().getId());
			recepcionCapturada.setSiniestroCabina(siniestro);
			recepcionCapturada.setIndemnizacion(indemnizacion);
			recepcionCapturada.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			indemnizacion.setRecepcionDocumentosSiniestro(entidadService.save(recepcionCapturada));
		}
//		ACTUALIZANDO LA INFORMACION DE LA DIRECCION DE LA RECUPERACION DE SALVAMENTO
		RecuperacionSalvamento recuperacion = 
			recuperacionSalvamentoService.obtenerRecuperacionSalvamento(indemnizacion.getOrdenCompra().getIdTercero());
		if(recuperacion != null){
			recuperacion.setDireccionSalvamento(recepcionCapturada.getDireccion());
			recuperacionSalvamentoService.guardarRecuperacionSalvamento(recuperacion);
//		INACTIVANDO Y ACTUALIZANDO LA RECUPERACION DE SALVAMENTO CUANDO SE MARCA COMO PAGO DE DANIOS LA INDEMNIZACION
			if( recepcionCapturada.getEsPagoDanios() != null && recepcionCapturada.getEsPagoDanios()){
				recuperacionSalvamentoService.inactivarRecuperacionSalvamento(recuperacion.getId(),
						"Inactivo Por Pago de Da\u00F1os en PT");
			}
		}
		indemnizacion.setEsPagoDanios(recepcionCapturada.getEsPagoDanios());
		if(indemnizacion.getEtapa().compareTo(IndemnizacionSiniestro.EtapasIndemnizacion.REGISTRO_DOCUMENTOS.codigo) == 0){
			indemnizacion.setEtapa(IndemnizacionSiniestro.EtapasIndemnizacion.AUTORIZAR_INDEMNIZACION.codigo);
		}
		
		OrdenCompra ordenCompra = entidadService.findById(OrdenCompra.class, indemnizacion.getOrdenCompra().getId());
		ordenCompra.setNomBeneficiario(recepcionCapturada.getBeneficiario());
		ordenCompra.setLada(recepcionCapturada.getTelefonoLada());
		ordenCompra.setTelefono(recepcionCapturada.getTelefonoNumero());
		ordenCompra.setRfc(recepcionCapturada.getRfc());
		ordenCompra.setCorreo(recepcionCapturada.getCorreo());
		ordenCompra.setBancoId(recepcionCapturada.getBancoId());
		ordenCompra.setClabe(recepcionCapturada.getClabeBanco());
		ordenCompra.setTipoPersona(recepcionCapturada.getTipoPersona());
		ordenCompra = entidadService.save(ordenCompra);
		indemnizacion.setOrdenCompra(ordenCompra);
		
		entidadService.save(indemnizacion);
//		SE ACOMPLETAN LOS MONTOS DE PROVISION DEL SALVAMENTO (IVA, PORCENTAJE IVA Y SUBTOTAL)
		recuperacionSalvamentoService.actualizarInformacionRecuperacionSalvamentoEnIndemnizacion(indemnizacion.getId());
		return mensajes;
	}
	
	@Override
	public Map<String, String> obtenerListaRefrendosTenenciasParaRecepcionDoctos(Integer anioModeloAuto, Date fechaOcurrido){
		Map<String, String> mapaRefrendos = new LinkedHashMap<String,String>();
		List<Integer> listaRefrendos = new ArrayList<Integer>();
		Calendar calendar = GregorianCalendar.getInstance();
		calendar.setTime(fechaOcurrido);
		int anioOcurrido = calendar.get(Calendar.YEAR); 
		
		for(int i = 0; i < NUMERO_TENENCIAS && anioOcurrido >= anioModeloAuto; i++, anioOcurrido--){
			listaRefrendos.add(new Integer(anioOcurrido));
		}
		
		if(listaRefrendos.isEmpty() && anioModeloAuto == anioOcurrido + 1){
			listaRefrendos.add(new Integer(anioOcurrido));
		}

		for(int i = listaRefrendos.size() - 1; i >= 0; i-- ){
			mapaRefrendos.put(listaRefrendos.get(i).toString(), listaRefrendos.get(i).toString());
		}
		return mapaRefrendos;
	}

	@Override
	public Boolean getEsRecepcionPagoDanios(Long idIndemnizacion){
		Boolean esRecepcionPagoDanios = Boolean.FALSE;
		if(idIndemnizacion != null){
			
			List<RecepcionDocumentosSiniestro> recepciones = entidadService.findByProperty(RecepcionDocumentosSiniestro.class, "indemnizacion.id",idIndemnizacion);
			if(recepciones != null && !recepciones.isEmpty()){
				RecepcionDocumentosSiniestro recepcion = recepciones.get(0);
				if(recepcion != null && recepcion.getEsPagoDanios() != null){
					esRecepcionPagoDanios = recepcion.getEsPagoDanios();
				}else{
					esRecepcionPagoDanios = Boolean.FALSE;
				}
			}
		}
		return esRecepcionPagoDanios;
	}
	
	@Override
	public TransporteImpresionDTO imprimirCartaNotificacionPerdidaTotal(Long idIndemnizacion, Integer idValuador){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		
		JasperReport jReport = gImpresion.getOJasperReport(CARTA_NOTIFICACION_PERDIDA_TOTAL);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("informacionAuto", generarInformacionAutoNotificacionPerdidaTotal(idIndemnizacion));
		List<String> informacionValuador = generarInformacionValuadorNotificacionPerdidaTotal(idValuador);
		params.put("valuadorNombre", informacionValuador.get(0));
		params.put("valuadorTelefono", informacionValuador.get(1));
		params.put("valuadorExtension", informacionValuador.get(2));
		
		return gImpresion.getTImpresionDTO(jReport, new JREmptyDataSource(), params);
	}
	

	@Override
	public String enviarCorreoCartaNotificacionPerdidaTotal(EnvioCaratulaParameter envioCaratulaParameter, Long idIndemnizacion, Integer idValuador){
		
		LogDeMidasEJB3.log(this.getClass().getName()+  " -- enviarCorreoCartaNotificacionPerdidaTotal---" , Level.INFO, null);
		String response="";
		/*GENERAR PDF*/
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = gImpresion.getOJasperReport(CARTA_NOTIFICACION_PERDIDA_TOTAL);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("informacionAuto", generarInformacionAutoNotificacionPerdidaTotal(idIndemnizacion));
		List<String> informacionValuador = generarInformacionValuadorNotificacionPerdidaTotal(idValuador);
		params.put("valuadorNombre", informacionValuador.get(0));
		params.put("valuadorTelefono", informacionValuador.get(1));
		params.put("valuadorExtension", informacionValuador.get(2));
		
		try {
			transporte = gImpresion.getTImpresionDTO(jReport, new JREmptyDataSource(), params);
		} catch (Exception e) {

			LogDeMidasEJB3.log(this.getClass().getName()+  " -- Error al tratar de generar el PDF --"+ e.getMessage(), Level.WARNING, null);
			
			return "Error al tratar de generar el PDF";
		
		}
		
		transporte.setGenericInputStream(new ByteArrayInputStream(transporte.getByteArray()));
		transporte.setContentType(TIPO_PDF);
		String fileName = "NotificacionPerdidaTotal_"+ idIndemnizacion +  EXT_PDF;
		transporte.setFileName(fileName);
		
		/*ENVIAR MAIL*/
		this.mailService = envioCaratulaParameter.getMailService();
		this.entidadService = envioCaratulaParameter.getEntidadService();
		this.generaPlantillaReporteBitemporalService = envioCaratulaParameter.getGeneraPlantillaReporteBitemporalService();
		List<String> destinatarios = new ArrayList<String>();
		try {
			//Lista de Archivos para el atach. Será uno solo 
			List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment pdf = new ByteArrayAttachment();
			byte[] cartaNotificacionPDF   = null;
			cartaNotificacionPDF = transporte.getByteArray();
			if (cartaNotificacionPDF!= null && cartaNotificacionPDF.length>0) {
				pdf.setTipoArchivo(ByteArrayAttachment.TipoArchivo.PDF);
				pdf.setNombreArchivo(fileName);
				LogDeMidasEJB3.log(this.getClass().getName()+  ": Preparando PDF CartaNotificacionPerdidaTotal" , Level.INFO, null);							
				pdf.setContenidoArchivo( cartaNotificacionPDF);								
				attachment.add(pdf);
			} else{
				LogDeMidasEJB3.log(this.getClass().getName()+  ": No se Extrajo el archivo PDF CartaNotificacionPerdidaTotal" , Level.INFO, null);		
			}
			LogDeMidasEJB3.log(" CartaNotificacionPerdidaTotal --> attachment.size:"+attachment.size(), Level.INFO, null);
			if(envioCaratulaParameter.getCorreo()!=null &&envioCaratulaParameter.getCorreo()!=""){
				destinatarios.add(envioCaratulaParameter.getCorreo());
			}
			mailService.sendMail(destinatarios, envioCaratulaParameter.getTitle(),envioCaratulaParameter.getMessage(), attachment, 
					envioCaratulaParameter.getSubject(),envioCaratulaParameter.getGreeting());
			
		} catch (Exception e) {
			LogDeMidasEJB3.log(this.getClass().getName()+  ": No se puede enviar correo "+ e.getMessage(), Level.INFO, null);		
			response="Error al tratar de enviar por correo el PDF";
			return	response;
		}		
		LogDeMidasEJB3.log(this.getClass().getName()+  "<-- enviarCorreoCartaNotificacionPerdidaTotal, se envia correo" , Level.INFO, null);
		response="Se envio PDF por correo ";
		return response;
		}
	/**
	 * Obtiene la cobertura a partir de la cual se genera la indemnizacion de siniestro
	 * @param idIndemnizacion
	 * @return
	 */
	private String obtenerCoberturaIndemnizacion(Long idIndemnizacion){
		String cobertura = "";
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		if( indemnizacion != null 
				&& indemnizacion.getOrdenCompra() != null
				&& indemnizacion.getOrdenCompra().getCoberturaReporteCabina() != null){
			EstimacionCoberturaReporteCabina estimacion  = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, indemnizacion.getOrdenCompra().getIdTercero());
			Map<String,Object> params = new HashMap<String,Object>();
			params.put( "cveTipoCalculoCobertura", indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getClaveTipoCalculo());
	        params.put( "tipoConfiguracion", indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getTipoConfiguracion());
	        params.put(	"cveSubTipoCalculoCobertura", estimacion.getCveSubCalculo());
	        List<ConfiguracionCalculoCoberturaSiniestro> confCalculoCoberturaList = entidadService.findByProperties( ConfiguracionCalculoCoberturaSiniestro.class, params );
			if(confCalculoCoberturaList != null){
				for(ConfiguracionCalculoCoberturaSiniestro configuracion : confCalculoCoberturaList){
					final String nombreComercial = indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getNombreComercial();
	                final String nombreCobertura = configuracion.getNombreCobertura();
	                cobertura = (nombreCobertura !=null)? nombreCobertura:nombreComercial;
		}}}
		return cobertura;
	}
	
	/**
	 * Genera el contenido de la carta de notificacion de perdida total relacionada con el auto
	 * @param reporteCabinaId
	 * @return
	 */
	private String generarInformacionAutoNotificacionPerdidaTotal(Long idIndemnizacion){
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		
		StringBuilder informacionAuto = new StringBuilder();
		
		if(indemnizacion != null) {
			if(esCoberturaAsegurado(idIndemnizacion)){
				AutoIncisoReporteCabina autoIncisoReporteCabina = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(indemnizacion.getSiniestro().getReporteCabina().getId());
				informacionAuto.append("Por medio de la presente y con relación al Número de Siniestro ");
				informacionAuto.append((indemnizacion.getSiniestro().getNumeroSiniestro() != null)?indemnizacion.getSiniestro().getNumeroSiniestro():"");
				informacionAuto.append(" hacemos de su conocimiento que el vehículo marca ");
				informacionAuto.append((autoIncisoReporteCabina.getDescMarca() != null) ? autoIncisoReporteCabina.getDescMarca() : "");
				informacionAuto.append(" Tipo ");
				informacionAuto.append((autoIncisoReporteCabina.getDescripcionFinal() != null) ? autoIncisoReporteCabina.getDescripcionFinal() : "");
				informacionAuto.append(" Modelo ");
				informacionAuto.append((autoIncisoReporteCabina.getModeloVehiculo() != null) ? autoIncisoReporteCabina.getModeloVehiculo() : "");
				informacionAuto.append(" Serie: ");
				informacionAuto.append((autoIncisoReporteCabina.getNumeroSerie() != null) ? autoIncisoReporteCabina.getNumeroSerie() : "");
				informacionAuto.append(" fue determinado por esta compañía como Pérdida Total, lo anterior derivado de la magnitud de los daños que este presenta a consecuencia del siniestro ocurrido el día ");
				informacionAuto.append((indemnizacion.getSiniestro().getReporteCabina().getFechaOcurrido() != null)?new SimpleDateFormat("dd/MM/yyyy",new Locale("es")).format(indemnizacion.getSiniestro().getReporteCabina().getFechaOcurrido()):"");
				informacionAuto.append(".");
			}else{
				EstimacionCoberturaReporteCabina estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
				TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
				if(tercero != null){
					informacionAuto.append("Por medio de la presente y con relación al Número de Siniestro ");
					informacionAuto.append((indemnizacion.getSiniestro().getNumeroSiniestro() != null)?indemnizacion.getSiniestro().getNumeroSiniestro():"");
					informacionAuto.append(" hacemos de su conocimiento que el vehículo marca ");
					informacionAuto.append((tercero.getMarca() != null) ? tercero.getMarca() : "");
					informacionAuto.append(" Tipo ");
					informacionAuto.append((tercero.getEstiloVehiculo() != null) ? tercero.getEstiloVehiculo() : "");
					informacionAuto.append(" Modelo ");
					informacionAuto.append((tercero.getModeloVehiculo() != null) ? tercero.getModeloVehiculo() : "");
					informacionAuto.append(" Serie: ");
					informacionAuto.append((tercero.getNumeroSerie() != null) ? tercero.getNumeroSerie() : "");
					informacionAuto.append(" fue determinado por esta compañía como Pérdida Total, lo anterior derivado de la magnitud de los daños que este presenta a consecuencia del siniestro ocurrido el día ");
					informacionAuto.append((indemnizacion.getSiniestro().getReporteCabina().getFechaOcurrido() != null)?new SimpleDateFormat("dd/MM/yyyy",new Locale("es")).format(indemnizacion.getSiniestro().getReporteCabina().getFechaOcurrido()):"");
					informacionAuto.append(".");
				}
			}
		}
		return informacionAuto.toString();
	}
	
	/**
	 * genera la informacion del valuador para la carta de notificacion de perdida total
	 * crea una lista de strings, con 3 elementos: el nombre del valuador, el telefono y la extension
	 * Si alguno de estos campos viene null, se pone una linea vacia
	 * @param idValuador
	 * @return
	 */
	private List<String> generarInformacionValuadorNotificacionPerdidaTotal(Integer idValuador){
		String vacio = "___________________________________";
		List<String> informacionValuador = new ArrayList<String>();
		
		if(idValuador != null){
			PrestadorServicio prestador = entidadService.findById(PrestadorServicio.class, idValuador);
			if(prestador != null && prestador.getPersonaMidas() != null){	
				informacionValuador.add((prestador.getNombrePersona() != null)? prestador.getNombrePersona() : vacio);
				informacionValuador.add((prestador.getPersonaMidas().getContacto() != null && prestador.getPersonaMidas().getContacto().getTelOficina() != null) ? 
								prestador.getPersonaMidas().getContacto().getTelOficina() : vacio);
				informacionValuador.add((prestador.getPersonaMidas().getContacto() != null &&	prestador.getPersonaMidas().getContacto().getTelOficinaExt() != null) ? 
						prestador.getPersonaMidas().getContacto().getTelOficinaExt() : vacio);
				return informacionValuador;
			}
		}
		informacionValuador.add(vacio);
		informacionValuador.add(vacio);
		informacionValuador.add(vacio);
		
		return informacionValuador;
	}
	
	@Override
	public TransporteImpresionDTO imprimirDeterminacionPT(Long idIndemnizacion, Date fechaDeterminacion, String tipoSiniestro, String tipoSiniestroDesc){
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		DeterminacionInformacionSiniestroDTO informacionSiniestro = perdidaTotalService.obtenerInformacionSiniestro(idIndemnizacion, tipoSiniestro, tipoSiniestroDesc);
		
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = gImpresion.getOJasperReport(CARTA_DETERMINACION_PERDIDA_TOTAL);
		JasperReport jReporteAutorizacionesIndemnizacion = gImpresion.getOJasperReport(CARTA_DETERMINACION_PERDIDA_TOTAL_AUTORIZACIONES);
		
		List<ImpresionDeterminacionPerdidaTotalDTO> dataSourceImpresion =new ArrayList<ImpresionDeterminacionPerdidaTotalDTO>();
		ImpresionDeterminacionPerdidaTotalDTO determinacionDTO = new ImpresionDeterminacionPerdidaTotalDTO();
		determinacionDTO.setIndemnizacion(indemnizacion);
		determinacionDTO.setInformacionSiniestro(informacionSiniestro);
		determinacionDTO.setFechaDeterminacion(fechaDeterminacion);
		dataSourceImpresion.add(determinacionDTO);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("rutaImagenCheckOn", SistemaPersistencia.CHECKBOX_ON);
		params.put("rutaImagenCheckOff", SistemaPersistencia.CHECKBOX_OFF);
		params.put("dataSourceImpresion", dataSourceImpresion);
		params.put("usuario", usuarioService.getUsuarioActual().getNombreUsuario());
		params.put("puestoUsuario", perdidaTotalService.obtenerPuestoUsuarioActualIndemnizacion());
		params.put("autorizacionesIndemnizacion", perdidaTotalService.listaAutorizaciones(AUTORIZACION_PERDIDA_TOTAL, idIndemnizacion));
		params.put("jReporteAutorizacionesIndemnizacion", jReporteAutorizacionesIndemnizacion);
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}

	@Override
	public CartaSiniestro obtenerCartaFiniquito(Long idIndemnizacion){
		CartaSiniestro cartaFiniquito = null;
		if(idIndemnizacion != null){
			IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
			if(indemnizacion != null){
				Map<String, Object> params = new HashMap<String, Object>();
				params.put("tipo", TIPO_CARTA_FINIQUITO);
				params.put("indemnizacion.id", idIndemnizacion);
				List<CartaSiniestro> resultados = entidadService.findByProperties(CartaSiniestro.class, params);
				if(resultados != null && !resultados.isEmpty()){
					cartaFiniquito = resultados.get(0);
				}
			}
		}
		return cartaFiniquito;
	}
	
	@Override
	public Boolean esCoberturaAsegurado(Long idIndemnizacion){
		Boolean esFiniquitoAsegurado = null;
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion); 
		String claveTipoCalculo = indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getClaveTipoCalculo();
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("cveTipoCalculoCobertura", claveTipoCalculo);
		List<ConfiguracionCalculoCoberturaSiniestro> resultados = 
			entidadService.findByProperties(ConfiguracionCalculoCoberturaSiniestro.class, params);
		if(resultados.get(0).getCubre().compareTo(
				ConfiguracionCalculoCoberturaSiniestro.ValoresCubre.ASEGURADO.toString()) == 0){
			esFiniquitoAsegurado = Boolean.TRUE;
		}else{
			esFiniquitoAsegurado = Boolean.FALSE;
		}
		return esFiniquitoAsegurado;
	}
	
	@Override
	public TransporteImpresionDTO imprimirCartaFiniquito(Long idIndemnizacion){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = null;
		if(esCoberturaAsegurado(idIndemnizacion)){
			jReport = gImpresion.getOJasperReport(CARTA_FINIQUITO_ASEGURADO);
		}else {
			jReport = gImpresion.getOJasperReport(CARTA_FINIQUITO_TERCERO);
		}
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		CartaSiniestro informacionFiniquito = new CartaSiniestro();
		informacionFiniquito.setIndemnizacion(indemnizacion);
		
		List<ImpresionCartaFiniquitoDTO> dataSourceImpresion =new ArrayList<ImpresionCartaFiniquitoDTO>();
		ImpresionCartaFiniquitoDTO informacionReporte = new ImpresionCartaFiniquitoDTO();
		informacionReporte.setInformacionFiniquito(informacionFiniquito);
		informacionReporte.setInformacionAsegurado(perdidaTotalService.buscarInformacionAseguradoFiniquito(idIndemnizacion));
		informacionReporte.setInformacionTercero(perdidaTotalService.buscarInformacionTerceroFiniquito(idIndemnizacion));
		informacionReporte.setPrimasPendientesPago(informacionReporte.getInformacionFiniquito().getIndemnizacion().getPrimasPendientes());
		informacionReporte.setPrimasNoDevengadas(null);
		dataSourceImpresion.add(informacionReporte);
		
		RecepcionDocumentosSiniestro recepcion = this.mostrarRecepcionDeDocumentos(
				informacionReporte.getInformacionFiniquito().getIndemnizacion().getId());
		EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class,
				informacionReporte.getInformacionFiniquito().getIndemnizacion().getOrdenCompra().getIdTercero());
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("coberturaIndemnizacion", obtenerCoberturaIndemnizacion(idIndemnizacion));
		params.put("asegurado", informacionReporte.getInformacionFiniquito().getIndemnizacion().getOrdenCompra().getReporteCabina().getPoliza().getNombreAsegurado());
		params.put("fechaOcurrido", informacionReporte.getInformacionFiniquito().getIndemnizacion().getOrdenCompra().getReporteCabina().getFechaHoraOcurrido());
		params.put("banco", buscarNombreBancoPorId(recepcion.getBancoId()));
		params.put("cuenta", recepcion.getClabeBanco());
		params.put("clabe", recepcion.getClabeBanco());
		params.put("afectado",
				(EnumUtil.equalsValue(ClaveTipoCalculo.ROBO_TOTAL, 
						indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getClaveTipoCalculo()))?
								indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getNombreAsegurado()
								: estimacion.getNombreAfectado());
		params.put("lugar", obtenerLugarDeSiniestro(informacionReporte.getInformacionFiniquito().getIndemnizacion()));
		params.put("conductor", informacionReporte.getInformacionFiniquito().getIndemnizacion().getOrdenCompra().getReporteCabina().getPersonaConductor());
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}
	
	/**
	 * Obtiene el nombre de la ciudad y el estado del lugar de ocurrido del reporte
	 * @param indemnizacion
	 * @return
	 */
	private String obtenerLugarDeSiniestro(IndemnizacionSiniestro indemnizacion){
		String lugar = "";
		if(indemnizacion.getOrdenCompra().getReporteCabina().getLugarOcurrido() != null){
			lugar = indemnizacion.getSiniestro().getReporteCabina().getLugarOcurrido().getCiudad().getDescripcion() + ", " + 
					indemnizacion.getSiniestro().getReporteCabina().getLugarOcurrido().getEstado().getDescripcion();
		}
		return lugar;
	}
	
	@Override
	public void guardarCartaFiniquito(CartaSiniestro cartaCapturada, Long idIndemnizacion){
		CartaSiniestro cartaFiniquito = null;
		if(cartaCapturada.getId() != null){
			cartaFiniquito = entidadService.findById(CartaSiniestro.class, cartaCapturada.getId());
		}else{
			if(idIndemnizacion != null){
				IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
				if(indemnizacion != null){
					cartaFiniquito = new CartaSiniestro();
					cartaFiniquito.setIndemnizacion(indemnizacion);
					cartaFiniquito.setFechaCreacion(new Date());
					cartaFiniquito.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
					cartaFiniquito.setTipo(TIPO_CARTA_FINIQUITO);
				}else{
					throw new NegocioEJBExeption("CSF_IND_00","Error al buscar la indemnizacion");
				}
			}else{
				throw new NegocioEJBExeption("CSF_IND_01","Id de la indemnizacion erronea");
			}
		}
		if(cartaFiniquito != null){
			cartaFiniquito.setNombreAsegurado(cartaCapturada.getNombreAsegurado());
			cartaFiniquito.setNombreConductor(cartaCapturada.getNombreConductor());
			cartaFiniquito.setNombreTercero(cartaCapturada.getNombreTercero());
			cartaFiniquito.setDomicilioTercero(cartaCapturada.getDomicilioTercero());
			cartaFiniquito.setTelefonoTercero(cartaCapturada.getTelefonoTercero());
			cartaFiniquito.setFechaCarta(cartaCapturada.getFechaCarta());
			cartaFiniquito.setDomicilioAsegurado(cartaCapturada.getDomicilioAsegurado());
			cartaFiniquito.setFechaReclamacion(cartaCapturada.getFechaReclamacion());
			cartaFiniquito.setResultadoAcc(cartaCapturada.getResultadoAcc());
			cartaFiniquito.setLugar(cartaCapturada.getLugar());
			
			cartaFiniquito.setFechaModificacion(new Date());
			cartaFiniquito.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			
			entidadService.save(cartaFiniquito);
		}else{
			throw new NegocioEJBExeption("CSF_IND_02","No se pudo crear/recuperar la carta de finiquito");
		}
	}


	/**
	 * obtiene las fechas de Perdida Total en base a la orden de compra
	 */
	@Override
	public FormatoFechasPT obtenerFechasPT( Long idOrdenCompra ){
		FormatoFechasPT formatoFechasPT = null;
		List<IndemnizacionSiniestro> listIndemnizacionSiniestro = null;
		IndemnizacionSiniestro indemnizacionSiniestro = null;
		List<CartaSiniestro> listCartaSiniestro = null; 
		CartaSiniestro cartaSiniestro = null;
		List<RecepcionDocumentosSiniestro> listRecepcionDocumentosSiniestros = null;
		RecepcionDocumentosSiniestro recepcionDocumentosSiniestro = null;
		Map<String, Object> params = null;
		
		listIndemnizacionSiniestro = entidadService.findByProperty( IndemnizacionSiniestro.class, "ordenCompra.id", idOrdenCompra );
		
		if( listIndemnizacionSiniestro != null && !listIndemnizacionSiniestro.isEmpty() ){
			indemnizacionSiniestro = listIndemnizacionSiniestro.get(0);
			formatoFechasPT = new FormatoFechasPT();
			formatoFechasPT.setIndemnizacion(indemnizacionSiniestro);
			formatoFechasPT.setReporteCabina( indemnizacionSiniestro.getOrdenCompra().getReporteCabina() );
			formatoFechasPT.setFechaAutorizacionOperaciones( indemnizacionSiniestro.getFechaAutIndemnizacion() );
			formatoFechasPT.setFechaAutorizacionSubSiniestro( indemnizacionSiniestro.getFechaAutPerdidaTotal() );
			formatoFechasPT.setUsuarioFinanzas( usuarioService.getUsuarioActual().getNombreUsuario() );
			formatoFechasPT.setTipo( CARTA_FORMATO_FECHAS_PT_TIPO );
			BigDecimal prima= getPrimasPendientesPago (indemnizacionSiniestro.getId());
			formatoFechasPT.setImportePrimaPend( prima );
			
			params = new HashMap<String, Object>();
			params.put( "indemnizacion.id", indemnizacionSiniestro.getId() );
			params.put( "tipo", formatoFechasPT.getTipo() );
			
			listCartaSiniestro = entidadService.findByProperties( CartaSiniestro.class, params ); 
			 
			if( listCartaSiniestro != null && !listCartaSiniestro.isEmpty() ){
				cartaSiniestro = listCartaSiniestro.get(0);
				formatoFechasPT.setTipoPerdida( cartaSiniestro.getTipoPerdida() );
				formatoFechasPT.setInvestigacion( cartaSiniestro.getInvestigacion() );
				formatoFechasPT.setComentarios( cartaSiniestro.getComentarios() );
				formatoFechasPT.setIdCartaSiniestro( cartaSiniestro.getId() );
			}
			
			params = new HashMap<String, Object>();
			params.put( "indemnizacion.id", indemnizacionSiniestro.getId() );
			
			listRecepcionDocumentosSiniestros = entidadService.findByProperties( RecepcionDocumentosSiniestro.class, params );
			
			if( listRecepcionDocumentosSiniestros != null && !listRecepcionDocumentosSiniestros.isEmpty() ){
				recepcionDocumentosSiniestro = listRecepcionDocumentosSiniestros.get(0);
				formatoFechasPT.setFechaRecepcionDoc( recepcionDocumentosSiniestro.getFechaCreacion() );
			}
			
			if(indemnizacionSiniestro.getOrdenCompraGenerada() != null
					&& indemnizacionSiniestro.getOrdenCompraGenerada().getOrdenPago() != null){
				
				formatoFechasPT.setFechaRecepOperaciones(
						indemnizacionSiniestro.getOrdenCompraGenerada().getOrdenPago().getFechaCreacion());
				
				List<OrdenPagoSiniestro> ordenesPago = 
					entidadService.findByProperty(OrdenPagoSiniestro.class, "id", 
							indemnizacionSiniestro.getOrdenCompraGenerada().getOrdenPago().getId());
				OrdenPagoSiniestro ordenPago = ordenesPago.get(0);
				LiquidacionSiniestro liquidacion = ordenPago.getLiquidacion();
				if( liquidacion != null ){
					formatoFechasPT.setFechaLiquidacionEgresos(liquidacion.getFechaCreacion());
					formatoFechasPT.setFechaRecepcionFinanzas(liquidacion.getFechaLiberadaCon());
					if(liquidacion.getSolicitudCheque() != null){
						formatoFechasPT.setFechaRecepcionCheque(liquidacion.getSolicitudCheque().getFechaPago());
						formatoFechasPT.setFechaEntregado(liquidacion.getSolicitudCheque().getFechaPago());
					}
				}
				
			}
		}
		return formatoFechasPT;
	}
	
	
	private BigDecimal getPrimasPendientesPago (Long idIndemnizacion){
		IndemnizacionSiniestro resultado = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		BigDecimal primasPendientePago=new BigDecimal (0.0);
		/*validar */
		if(null!=resultado.getOrdenCompra().getReporteCabina()  &&  null!=resultado.getOrdenCompra().getReporteCabina().getPoliza() ){
			if(null!=resultado.getOrdenCompra().getReporteCabina().getSeccionReporteCabina()   &&   null!=resultado.getOrdenCompra().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() ){
				IncisoReporteCabina inciso=resultado.getOrdenCompra().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() ;
				PolizaDTO poliza=resultado.getOrdenCompra().getReporteCabina().getPoliza() ;
				 primasPendientePago=	endosoService.validarPrimasPendientesPagoInciso(poliza.getIdToPoliza(), inciso.getNumeroInciso(), null);

			}
			

		}
		return primasPendientePago;
	}
	
	/**
	 * Guarda la informacion capturada en la pantalla de formato fechas PT, en la seccion de primas pendientes
	 */
	@Override
	public void guardarPrimasPendientes( FormatoFechasPT formatoFechasPT ){
		CartaSiniestro cartaSiniestro = null;
		
		if( formatoFechasPT.getIdCartaSiniestro() == null ){
			cartaSiniestro = new CartaSiniestro();
			cartaSiniestro = llenarBaseEntidadCartaSiniestro( cartaSiniestro, formatoFechasPT );
			cartaSiniestro.setFechaCreacion( new Date() );
			cartaSiniestro.setCodigoUsuarioCreacion( usuarioService.getUsuarioActual().getNombreUsuario() );
		}else{
			cartaSiniestro = entidadService.findById( CartaSiniestro.class, formatoFechasPT.getIdCartaSiniestro() );
			cartaSiniestro = llenarBaseEntidadCartaSiniestro( cartaSiniestro, formatoFechasPT );
			cartaSiniestro.setFechaModificacion( new Date() );
			cartaSiniestro.setCodigoUsuarioModificacion( usuarioService.getUsuarioActual().getNombreUsuario() );
		}

		entidadService.save( cartaSiniestro );
	}
	
	/**
	 * Llena los datos base para guardar las primas pendientes
	 * @param cartaSiniestro
	 * @param formatoFechasPT
	 * @return
	 */
	private CartaSiniestro llenarBaseEntidadCartaSiniestro( CartaSiniestro cartaSiniestro, FormatoFechasPT formatoFechasPT ){	
		if(formatoFechasPT.getIndemnizacion() != null && formatoFechasPT.getIndemnizacion().getId() != null){
			IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, formatoFechasPT.getIndemnizacion().getId());
			cartaSiniestro.setIndemnizacion(indemnizacion);
		}
		cartaSiniestro.setTipo( formatoFechasPT.getTipo() );
		cartaSiniestro.setImportePrimaPend( formatoFechasPT.getImportePrimaPend() );
		cartaSiniestro.setUsuarioFinanzas( formatoFechasPT.getUsuarioFinanzas() );
		cartaSiniestro.setTipoPerdida( formatoFechasPT.getTipoPerdida() );
		cartaSiniestro.setInvestigacion( formatoFechasPT.getInvestigacion() );
		cartaSiniestro.setComentarios( formatoFechasPT.getComentarios() );
		
		return cartaSiniestro;
	}
	
	
	/**
	 * Consulta las fechas y los datos de la carta para generar la impresion del formato fechas de Perdida Total
	 */
	@Override
	public TransporteImpresionDTO imprimirFormatoFechasPT( Long idOrdenCompra ){
		FormatoFechasPT formatoFechasPT = null;
		GeneradorImpresion gImpresion 		 = new GeneradorImpresion();
		JasperReport jReport 				 = gImpresion.getOJasperReport( CARTA_FORMATO_FECHAS_PT );
		String investigacion = "";
		
		formatoFechasPT = obtenerFechasPT( idOrdenCompra );
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put( "P_IMAGE_LOGO",       SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME );
		params.put( "LINE_GREEN",          SistemaPersistencia.LINE_GREEN );
		params.put( "rutaImagenRadioOn", SistemaPersistencia.RADIO_BUTTON_ON );
		params.put( "rutaImagenRadioOff",SistemaPersistencia.RADIO_BUTTON_OFF );
		
		if( formatoFechasPT.getTipoPerdida() == null){
			formatoFechasPT.setTipoPerdida("");
		}
		
		if( formatoFechasPT.getInvestigacion() != null){
			investigacion = formatoFechasPT.getInvestigacion().toString();
		}
		//se inserta la prima pendiente de pago hasta la fecha de la determinacionPerdidaTotal. No importa si los recibos fueron pagados posteriormente
		IndemnizacionSiniestro indemnizacion = this.entidadService.findById(IndemnizacionSiniestro.class, formatoFechasPT.getIndemnizacion().getId());
		params.put( "numeroSiniestro",   			 formatoFechasPT.getReporteCabina().getSiniestroCabina().getNumeroSiniestro() );
		params.put( "fechaSiniestro",                formatoFechasPT.getReporteCabina().getFechaOcurrido() );
		params.put( "fechaRecepcionDoc",             formatoFechasPT.getFechaRecepcionDoc() );
		params.put( "fechaAutorizacionSubSiniestro", formatoFechasPT.getFechaAutorizacionSubSiniestro() );
		params.put( "fechaRecepOperaciones",         formatoFechasPT.getFechaRecepOperaciones() );
		params.put( "fechaLiquidacionEgresos",       formatoFechasPT.getFechaLiquidacionEgresos() );
		params.put( "fechaAutorizacionOperaciones",  formatoFechasPT.getFechaAutorizacionOperaciones() );
		params.put( "fechaRecepcionFinanzas",        formatoFechasPT.getFechaRecepcionFinanzas() );
		params.put( "fechaRecepcionCheque",          formatoFechasPT.getFechaRecepcionCheque() ); 
		params.put( "fechaEntregado",                formatoFechasPT.getFechaEntregado() );
		params.put( "importePrimaPend",              indemnizacion.getPrimasPendientes() );
		params.put( "usuarioFinanzas",               formatoFechasPT.getUsuarioFinanzas() );
		params.put( "tipoPerdida",                   formatoFechasPT.getTipoPerdida() );
		params.put( "investigacion",                 investigacion );
		params.put( "comentarios",                   formatoFechasPT.getComentarios() );
		
		return gImpresion.getTImpresionDTO(jReport, new JREmptyDataSource(), params); 
	}

	@Override
	public void agregarEndosos(Long idFactura, String nombreEndoso) {
		CartaSiniestroEndoso endoso = new CartaSiniestroEndoso();
		CartaSiniestroFactura factura = entidadService.findById(CartaSiniestroFactura.class, idFactura);
		endoso.setCartaSiniestroFactura(factura);
		endoso.setNombreEndoso(nombreEndoso);
		factura.getEndosos().add(endoso);
		entidadService.save(endoso);
	}

	@Override
	public void eliminarEndoso(Long idEndoso) {
		CartaSiniestroEndoso endoso = entidadService.findById(CartaSiniestroEndoso.class, idEndoso);
		endoso.getCartaSiniestroFactura().getEndosos().remove(endoso);
		entidadService.remove(endoso);
	}
	
	@Override
	public void agregarFactura(Long idCartaSiniestro, CartaSiniestroFactura datosCapturados) {
		CartaSiniestro carta = entidadService.findById(CartaSiniestro.class, idCartaSiniestro);
		datosCapturados.setCartaSiniestro(carta);
		datosCapturados.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		datosCapturados.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		entidadService.save(datosCapturados);
	}

	@Override
	public void eliminarFactura(Long idFactura) {
		CartaSiniestroFactura factura = entidadService.findById(CartaSiniestroFactura.class, idFactura);
		factura.getCartaSiniestro().getFacturas().remove(factura);
		entidadService.remove(factura);
	}

	@Override
	public void guardarEntregaDocumentos(CartaSiniestro cartaCapturada, Long idIndemnizacion) {
		CartaSiniestro cartaEntrega = null;
		if(cartaCapturada.getId() != null){
			cartaEntrega = entidadService.findById(CartaSiniestro.class, cartaCapturada.getId());
		}else{
			if(idIndemnizacion != null){
				IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
				if(indemnizacion  != null){
					cartaEntrega = new CartaSiniestro();
					cartaEntrega.setIndemnizacion(indemnizacion);
					cartaEntrega.setFechaCreacion(new Date());
					cartaEntrega.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
					cartaEntrega.setTipo(TIPO_CARTA_ENTREGA_DOCUMENTOS);
				}else{
					throw new NegocioEJBExeption("CSF_IND_00","Error al buscar el siniestro");
				}
			}else{
				throw new NegocioEJBExeption("CSF_IND_01","Id del siniestro erroneo");
			}
		}
		if(cartaEntrega != null){
			cartaEntrega.setNombreFactura(cartaCapturada.getNombreFactura());
			cartaEntrega.setNumeroFactura(cartaCapturada.getNumeroFactura());
			cartaEntrega.setDetalleFactura(cartaCapturada.getDetalleFactura());
			cartaEntrega.setNombreCopiaFactura(cartaCapturada.getNombreCopiaFactura());
			cartaEntrega.setNumeroCopiaFactura(cartaCapturada.getNumeroCopiaFactura());
			cartaEntrega.setDetalleCopiaFactura(cartaCapturada.getDetalleCopiaFactura());
			cartaEntrega.setConstanciaBajaPlacas(cartaCapturada.getConstanciaBajaPlacas());
			cartaEntrega.setFolioBajaPlacas(cartaCapturada.getFolioBajaPlacas());
			cartaEntrega.setConstanciaPagoTenencia(cartaCapturada.getConstanciaPagoTenencia());
			cartaEntrega.setFolioAltaTenencia(cartaCapturada.getFolioAltaTenencia());
			cartaEntrega.setFechaTenenciaInicial(cartaCapturada.getFechaTenenciaInicial());
			cartaEntrega.setFechaTenenciaFin(cartaCapturada.getFechaTenenciaFin());
			cartaEntrega.setEstadoTenencia(cartaCapturada.getEstadoTenencia());
			cartaEntrega.setLlavesEntregadas(cartaCapturada.getLlavesEntregadas());
			cartaEntrega.setNumeroAveriguacion(cartaCapturada.getNumeroAveriguacion());
			
			cartaEntrega.setFechaModificacion(new Date());
			cartaEntrega.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			
			entidadService.save(cartaEntrega);
		}else{
			throw new NegocioEJBExeption("CSF_IND_02","No se pudo crear/recuperar la carta de finiquito");
		}
	}

	@Override
	public TransporteImpresionDTO imprimirCartaEntregaDocumentos(CartaSiniestro cartaSiniestro, String tipoCarta, Long idIndemnizacion) {
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = null;
		List<CartaSiniestroFactura> facturas = listarFacturas(cartaSiniestro.getId());
		
		List<ImpresionEntregaDocumentosDTO> dataSourceImpresion = new ArrayList<ImpresionEntregaDocumentosDTO>();
		ImpresionEntregaDocumentosDTO informacionReporte = 
			perdidaTotalService.buscarInformacionSiniestroEntregaDocumentos(idIndemnizacion);
		informacionReporte.setInformacionEntrega(cartaSiniestro);
		
		if(tipoCarta.compareTo(BAJA_PLACAS_PERDIDA_TOTAL) == 0){
			informacionReporte.setEndosos(generarTextoFacturasCartaEntregaDocumentosPerdidaTotal(cartaSiniestro.getId(), facturas));
			jReport = gImpresion.getOJasperReport(CARTA_ENTREGA_PERDIDA);
		}else {
			informacionReporte.setEndosos(generarTextoFacturasCartaEntregaDocumentosRoboTotal(cartaSiniestro.getId(), facturas));
			jReport = gImpresion.getOJasperReport(CARTA_ENTREGA_ROBO);
		}
		
		dataSourceImpresion.add(informacionReporte);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("NUMERACION_DOCUMENTO", facturas.size());
		params.put(JRParameter.REPORT_LOCALE, new Locale("es"));
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}
	
	private String generarTextoFacturasCartaEntregaDocumentosPerdidaTotal(Long idCartaSiniestro, List<CartaSiniestroFactura> facturas){
		StringBuilder texto = new StringBuilder();
		int numeracionDocumento = 0;     
		for(CartaSiniestroFactura factura : facturas){
			numeracionDocumento++;
			texto.append(numeracionDocumento);
			texto.append(" Factura N°: ");
			texto.append(factura.getNumeroFactura());
			texto.append(" de ");
			texto.append(factura.getEmisorFactura());
			texto.append(", a nombre de ");
			texto.append(factura.getReceptorFactura());
			texto.append(generarTextoEndososPerdidaTotal(factura.getEndosos()));
			texto.append(".\n\n");
		}
		
		return texto.toString();
	}
	
	private String generarTextoFacturasCartaEntregaDocumentosRoboTotal(Long idCartaSiniestro, List<CartaSiniestroFactura> facturas){
		StringBuilder texto = new StringBuilder();
		int numeracionDocumento = 0;
		
		for(CartaSiniestroFactura factura : facturas){
			numeracionDocumento++;
			texto.append(numeracionDocumento);
			texto.append(" Factura N°: ");
			texto.append(factura.getNumeroFactura());
			texto.append(" de ");
			texto.append(factura.getEmisorFactura());
			texto.append(", expedida a favor de: ");
			texto.append(factura.getReceptorFactura());
			texto.append(generarTextoEndososRoboTotal(factura.getEndosos()));
			texto.append(".\n\n");
		}
		
		return texto.toString();
	}
	
	/**
	 * Genera el texto a imprimir de los endosos de la entrega de documentos de robo a partir de la lista de endosos 
	 * @param endosos
	 * @return
	 */
	private String generarTextoEndososRoboTotal(List<CartaSiniestroEndoso> endosos){
		StringBuilder textoImpresion = new StringBuilder();
		for(CartaSiniestroEndoso endoso: endosos){
			textoImpresion.append(", quien a su vez endosa a favor de: ");
			textoImpresion.append(endoso.getNombreEndoso());
		}
		return textoImpresion.toString();
	}
	
	/**
	 * Genera el texto a imprimir de los endosos de la entrega de documentos de perdida a partir de la lista de endosos 
	 * @param endosos
	 * @return
	 */
	private String generarTextoEndososPerdidaTotal(List<CartaSiniestroEndoso> endosos){
		StringBuilder textoImpresion = new StringBuilder();
		for(CartaSiniestroEndoso endoso: endosos){
			textoImpresion.append(" y a su vez endosa a: ");
			textoImpresion.append(endoso.getNombreEndoso());
		}
		return textoImpresion.toString();
	}

	@Override
	public List<CartaSiniestroEndoso> listarEndosos(Long idCartaSiniestroFactura) {
		List<CartaSiniestroEndoso> endosos = null;
		if(idCartaSiniestroFactura != null){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("cartaSiniestroFactura.id", idCartaSiniestroFactura);
			endosos = entidadService.findByPropertiesWithOrder(CartaSiniestroEndoso.class, params, ORDER_BY_ID);
		}
		return endosos;
	}
	
	@Override
	public List<CartaSiniestroFactura> listarFacturas(Long idCartaSiniestro) {
		List<CartaSiniestroFactura> facturas = new ArrayList<CartaSiniestroFactura>();
		if(idCartaSiniestro != null){
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("cartaSiniestro.id", idCartaSiniestro);
			facturas = entidadService.findByPropertiesWithOrder(CartaSiniestroFactura.class, params, ORDER_BY_ID);
		}
		return facturas;
	}

	@Override
	public CartaSiniestro obtenerCartaEntregaDocumentos(Long idIndemnizacion) {
		CartaSiniestro cartaEntregaDocumentos = null;
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("tipo", TIPO_CARTA_ENTREGA_DOCUMENTOS);
		params.put("indemnizacion.id", idIndemnizacion);
		List<CartaSiniestro> resultados = entidadService.findByProperties(CartaSiniestro.class, params);
		if(resultados != null && !resultados.isEmpty()){
			cartaEntregaDocumentos = resultados.get(0);
		}
		
		return cartaEntregaDocumentos;
	}
	
	@Override
	public TransporteImpresionDTO imprimirCartaPTSNoDocumentada(Long idIndemnizacion){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = null;
		if(esCoberturaAsegurado(idIndemnizacion)){
			jReport = gImpresion.getOJasperReport(CARTA_PTS_NO_DOCUMENTADA);
		}else{
			jReport = gImpresion.getOJasperReport(CARTA_PTS_NO_DOCUMENTADA_TERCERO);
		}
		
		List<CartaPTSNoDocumentadaDTO> dataSourceImpresion = new ArrayList<CartaPTSNoDocumentadaDTO>();
		CartaPTSNoDocumentadaDTO informacionReporte = 
			perdidaTotalService.buscarInformacionCartaPTS(idIndemnizacion);
		
		dataSourceImpresion.add(informacionReporte);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}

	@Override
	public RecepcionDocumentosSiniestro buscarRecepcionDeDocumentos(Long idIndemnizacion){
		List<RecepcionDocumentosSiniestro> 	resultados;
		RecepcionDocumentosSiniestro 		 recepcionDoctos = null;
		
		resultados = entidadService.findByProperty(RecepcionDocumentosSiniestro.class, "indemnizacion.id", idIndemnizacion);
		if(resultados != null && !resultados.isEmpty()){
			recepcionDoctos = resultados.get(0);
		}

		return recepcionDoctos;
	}

	@Override
	public CartaBajaPlacasDTO obtenerInformacionAutoRecepcionDocumentos(Long idIndemnizacion){
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		CartaBajaPlacasDTO informacionAuto = new CartaBajaPlacasDTO();
		if(esCoberturaAsegurado(idIndemnizacion)){
			AutoIncisoReporteCabina autoIncisoReporteCabina = siniestroCabinaService.obtenerAutoIncisoByReporteCabina(indemnizacion.getSiniestro().getReporteCabina().getId());
			informacionAuto.setMarcaDesc(autoIncisoReporteCabina.getDescMarca());
			informacionAuto.setDescTipoUso(autoIncisoReporteCabina.getDescripcionFinal());
			informacionAuto.setModelo((autoIncisoReporteCabina.getModeloVehiculo() != null)? 
					autoIncisoReporteCabina.getModeloVehiculo().toString() : "");
			informacionAuto.setTipoCarta(ConfiguracionCalculoCoberturaSiniestro.ValoresCubre.ASEGURADO.toString());
		}else{
			EstimacionCoberturaReporteCabina estimacion= entidadService.findById(EstimacionCoberturaReporteCabina.class,indemnizacion.getOrdenCompra().getIdTercero() );
			TerceroRCVehiculo tercero = ((TerceroRCVehiculo)estimacion);
			informacionAuto.setMarcaDesc(tercero.getMarca());
			informacionAuto.setDescTipoUso(tercero.getEstiloVehiculo());
			informacionAuto.setModelo(tercero.getModeloVehiculo().toString());
			informacionAuto.setTipoCarta(ConfiguracionCalculoCoberturaSiniestro.ValoresCubre.TERCERO.toString());
		}
		return informacionAuto;
	}

	@Override
	public TransporteImpresionDTO imprimirCartaFiniquitoLiquidacion(Long idLiquidacion) {
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		if(idLiquidacion != null){
			LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
			List<byte[]> inputByteArray = new ArrayList<byte[]>();
			if(liquidacion != null){
				for(OrdenPagoSiniestro orden : liquidacion.getOrdenesPago()){
					if(orden.getOrdenCompra().getIndemnizacion() != null){
						inputByteArray.add(generarCartaFiniquito(orden.getOrdenCompra().getIndemnizacion().getId()));
			}}
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				GeneradorImpresion gImpresion = new GeneradorImpresion();
				gImpresion.pdfConcantenate(inputByteArray ,output );
				transporte.setByteArray(output.toByteArray());
			}
		}
		return transporte;
	}

	@Override
	public TransporteImpresionDTO imprimirContratoIndemnizacion(Long idIndemnizacion, InfoContratoSiniestro idInfoContrato) {
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		InfoContratoSiniestro infoContrato = entidadService.findById(InfoContratoSiniestro.class, idInfoContrato.getId());
//		infoContrato.setApoderadoAfirme(parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID, PARAMETRO_GLOBAL_APODERADO));
		FiniquitoInformacionSiniestroDTO informacionAsegurado = null;
		if(esCoberturaAsegurado(idIndemnizacion)){
			informacionAsegurado = perdidaTotalService.buscarInformacionAseguradoFiniquito(idIndemnizacion); 
		}else {
			informacionAsegurado = perdidaTotalService.buscarInformacionTerceroFiniquito(idIndemnizacion);
		}
		
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = gImpresion.getOJasperReport(CONTRATO_SALVAMENTO);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("infoContrato", infoContrato);
		params.put("informacionAsegurado", informacionAsegurado);
		params.put("totalIndemnizar", (indemnizacion.getEstimadoValuador() != null)? indemnizacion.getEstimadoValuador() : BigDecimal.ZERO);
		
		return gImpresion.getTImpresionDTO(jReport, new JREmptyDataSource(), params);
	}

	@Override
	public TransporteImpresionDTO imprimirContratoLiquidacion(Long idLiquidacion) {
		TransporteImpresionDTO transporte = new TransporteImpresionDTO();
		if(idLiquidacion != null){
			LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
			List<byte[]> inputByteArray = new ArrayList<byte[]>();
			if(liquidacion != null){
				for(OrdenPagoSiniestro orden : liquidacion.getOrdenesPago()){
					if(orden.getOrdenCompra().getIndemnizacion() != null){
						IndemnizacionSiniestro indemnizacion = orden.getOrdenCompra().getIndemnizacion();
						inputByteArray.add(generarContrato(indemnizacion.getId()));
			}}
				ByteArrayOutputStream output = new ByteArrayOutputStream();
				GeneradorImpresion gImpresion = new GeneradorImpresion();
				gImpresion.pdfConcantenate(inputByteArray ,output );
				transporte.setByteArray(output.toByteArray());
			}
		}
		return transporte;
	}
	
	/**
	 * Genera un arreglo de bytes del contrato generado para concatenarlo con otros reportes y así entregar un solo pdf de impresion
	 * @param idIndemnizacion
	 * @return
	 */
	private byte[] generarContrato(Long idIndemnizacion){
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		InfoContratoSiniestro infoContrato = this.buscarInfoContrato(idIndemnizacion);
//		infoContrato.setApoderadoAfirme(parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID, PARAMETRO_GLOBAL_APODERADO));
		FiniquitoInformacionSiniestroDTO informacionAsegurado = null;
		if(esCoberturaAsegurado(idIndemnizacion)){
			informacionAsegurado = perdidaTotalService.buscarInformacionAseguradoFiniquito(idIndemnizacion); 
		}else {
			informacionAsegurado = perdidaTotalService.buscarInformacionTerceroFiniquito(idIndemnizacion);
		}
		
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = gImpresion.getOJasperReport(CONTRATO_SALVAMENTO);
		List<Object> dataSourceImpresion =new ArrayList<Object>();
		dataSourceImpresion.add(infoContrato);//para concatenar requiere un datasource, y que el data source tenga al menos un elemento
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("infoContrato", infoContrato);
		params.put("informacionAsegurado", informacionAsegurado);
		params.put("totalIndemnizar", (indemnizacion.getEstimadoValuador() != null)? indemnizacion.getEstimadoValuador() : BigDecimal.ZERO);
		
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		byte[] byteReport = null;
		try {
			byteReport = gImpresion.generateReport(jReport, params, collectionDataSource);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return byteReport;
	}
	
	/**
	 * Genera un arreglo de bytes dela carta finiquito generada para concatenarlo con otros reporte y asi entregar un solo pdf de impresion 
	 * @param idIndemnizacion
	 * @return
	 */
	private byte[] generarCartaFiniquito(Long idIndemnizacion){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		JasperReport jReport = null;
		if(esCoberturaAsegurado(idIndemnizacion)){
			jReport = gImpresion.getOJasperReport(CARTA_FINIQUITO_ASEGURADO);
		}else {
			jReport = gImpresion.getOJasperReport(CARTA_FINIQUITO_TERCERO);
		}
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		CartaSiniestro informacionFiniquito = new CartaSiniestro();
		informacionFiniquito.setIndemnizacion(indemnizacion);
		List<ImpresionCartaFiniquitoDTO> dataSourceImpresion =new ArrayList<ImpresionCartaFiniquitoDTO>();
		ImpresionCartaFiniquitoDTO informacionReporte = new ImpresionCartaFiniquitoDTO();
		informacionReporte.setInformacionFiniquito(informacionFiniquito);
		informacionReporte.setInformacionAsegurado(perdidaTotalService.buscarInformacionAseguradoFiniquito(idIndemnizacion));
		informacionReporte.setInformacionTercero(perdidaTotalService.buscarInformacionTerceroFiniquito(idIndemnizacion));
		informacionReporte.setPrimasPendientesPago(informacionReporte.getInformacionFiniquito().getIndemnizacion().getPrimasPendientes());
		informacionReporte.setPrimasNoDevengadas(perdidaTotalService.obtenerPrimaNoDevengada(informacionReporte.getInformacionFiniquito().getIndemnizacion()));
		dataSourceImpresion.add(informacionReporte);
		
		RecepcionDocumentosSiniestro recepcion = this.mostrarRecepcionDeDocumentos(
				informacionReporte.getInformacionFiniquito().getIndemnizacion().getId());
		EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class,
				informacionReporte.getInformacionFiniquito().getIndemnizacion().getOrdenCompra().getIdTercero());
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("coberturaIndemnizacion", obtenerCoberturaIndemnizacion(idIndemnizacion));
		params.put("asegurado", informacionReporte.getInformacionFiniquito().getIndemnizacion().getOrdenCompra().getReporteCabina().getPoliza().getNombreAsegurado());
		params.put("fechaOcurrido", informacionReporte.getInformacionFiniquito().getIndemnizacion().getOrdenCompra().getReporteCabina().getFechaHoraOcurrido());
		params.put("banco", buscarNombreBancoPorId(recepcion.getBancoId()));
		params.put("cuenta", recepcion.getClabeBanco());
		params.put("clabe", recepcion.getClabeBanco());
		params.put("afectado",
				(EnumUtil.equalsValue(ClaveTipoCalculo.ROBO_TOTAL, 
						indemnizacion.getOrdenCompra().getCoberturaReporteCabina().getCoberturaDTO().getClaveTipoCalculo()))?
								indemnizacion.getSiniestro().getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getNombreAsegurado()
								: estimacion.getNombreAfectado());
		params.put("lugar", obtenerLugarDeSiniestro(informacionReporte.getInformacionFiniquito().getIndemnizacion()));
		params.put("conductor", informacionReporte.getInformacionFiniquito().getIndemnizacion().getOrdenCompra().getReporteCabina().getPersonaConductor());
		
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		byte[] byteReport = null;
		try {
			byteReport = gImpresion.generateReport(jReport, params, collectionDataSource);
		} catch (Exception ex) {
			throw new RuntimeException(ex);
		}
		return byteReport;
	}
	
	@Override
	public InfoContratoSiniestro guardarInfoContrato(InfoContratoSiniestro infoContratoCapturado, Long idIndemnizacion){
		InfoContratoSiniestro infoContrato = null;
		IndemnizacionSiniestro indemnizacion = entidadService.findById(IndemnizacionSiniestro.class, idIndemnizacion);
		
		if(infoContratoCapturado.getId() == null){
			infoContrato = new InfoContratoSiniestro();
			infoContrato.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			infoContrato.setFechaCreacion(new Date());
			infoContrato.setIndemnizacion(indemnizacion);
		}else{
			infoContrato = entidadService.findById(InfoContratoSiniestro.class, infoContratoCapturado.getId());
		}
		infoContrato.setApoderadoAfectado(infoContratoCapturado.getApoderadoAfectado());
		infoContrato.setApoderadoAfirme(parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID, PARAMETRO_GLOBAL_APODERADO));
		infoContrato.setCalleVendedor(infoContratoCapturado.getCalleVendedor());
		infoContrato.setCiudadVendedor(infoContratoCapturado.getCiudadVendedor());
		infoContrato.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		infoContrato.setColoniaVendedor(infoContratoCapturado.getColoniaVendedor());
		infoContrato.setCpVendedor(infoContratoCapturado.getCpVendedor());
		infoContrato.setDocumentacion(infoContratoCapturado.getDocumentacion());
		infoContrato.setEstadoVendedor(infoContratoCapturado.getEstadoVendedor());
		infoContrato.setFechaModificacion(new Date());
		infoContrato.setNumeroVendedor(infoContratoCapturado.getNumeroVendedor());
		infoContrato.setSegundoApoderadoAfirme(infoContratoCapturado.getSegundoApoderadoAfirme());
		
		infoContrato = entidadService.save(infoContrato);
		
		return infoContrato;
	}
	
	@Override
	public InfoContratoSiniestro buscarInfoContrato(Long idIndemnizacion){
		InfoContratoSiniestro contrato = null;
		if(idIndemnizacion != null){
			List<InfoContratoSiniestro> resultados = entidadService.findByProperty(InfoContratoSiniestro.class, "indemnizacion.id", idIndemnizacion);
			if(resultados != null
					&& !resultados.isEmpty()){
				contrato = resultados.get(0);
			}
		}
		return contrato;
	}
	
	@Override
	public InfoContratoSiniestro guardarInfoContratos(InfoContratoSiniestro infoContratoCapturado, Long idLiquidacion){
		if(idLiquidacion != null){
			LiquidacionSiniestro  liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
			if(liquidacion != null){
				for(OrdenPagoSiniestro orden : liquidacion.getOrdenesPago()){
					if(orden.getOrdenCompra().getIndemnizacion() != null){
						IndemnizacionSiniestro indemnizacion = orden.getOrdenCompra().getIndemnizacion();
						indemnizacion.setContrato(copiarInformacionContrato(infoContratoCapturado, indemnizacion));
						indemnizacion = entidadService.save(indemnizacion);
		}}}}
		infoContratoCapturado = buscarInfoContratos(idLiquidacion);
		return infoContratoCapturado;
	}
	
	/**
	 * Copia la informacion capturada del contrato al contrato existente en la indemnizacion
	 * De no contar con un contrato existente, se crea uno nuevo para la indemnizacion
	 * @param infoCapturada
	 * @param indemnizacion
	 * @return
	 */
	private InfoContratoSiniestro copiarInformacionContrato(InfoContratoSiniestro infoCapturada, IndemnizacionSiniestro indemnizacion){
		InfoContratoSiniestro infoIndemnizacion = indemnizacion.getContrato();
		if(infoIndemnizacion == null){
			infoIndemnizacion = new InfoContratoSiniestro();
			infoIndemnizacion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			infoIndemnizacion.setFechaCreacion(new Date());
			infoIndemnizacion.setIndemnizacion(indemnizacion);
		}
		infoIndemnizacion.setApoderadoAfectado(infoCapturada.getApoderadoAfectado());
		infoIndemnizacion.setApoderadoAfirme(parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID, PARAMETRO_GLOBAL_APODERADO));
		infoIndemnizacion.setCalleVendedor(infoCapturada.getCalleVendedor());
		infoIndemnizacion.setCiudadVendedor(infoCapturada.getCiudadVendedor());
		infoIndemnizacion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		infoIndemnizacion.setColoniaVendedor(infoCapturada.getColoniaVendedor());
		infoIndemnizacion.setCpVendedor(infoCapturada.getCpVendedor());
		infoIndemnizacion.setDocumentacion(infoCapturada.getDocumentacion());
		infoIndemnizacion.setEstadoVendedor(infoCapturada.getEstadoVendedor());
		infoIndemnizacion.setFechaModificacion(new Date());
		infoIndemnizacion.setNumeroVendedor(infoCapturada.getNumeroVendedor());
		infoIndemnizacion.setSegundoApoderadoAfirme(infoCapturada.getSegundoApoderadoAfirme());
		infoIndemnizacion = entidadService.save(infoIndemnizacion);
		return infoIndemnizacion;
	}
	
	@Override
	public InfoContratoSiniestro buscarInfoContratos(Long idLiquidacion){
		InfoContratoSiniestro contrato = null;
		if(idLiquidacion != null){
			LiquidacionSiniestro  liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
			if(liquidacion != null){
				for(OrdenPagoSiniestro orden : liquidacion.getOrdenesPago()){
					if(orden.getOrdenCompra().getIndemnizacion() != null){
						IndemnizacionSiniestro indemnizacion = orden.getOrdenCompra().getIndemnizacion();
						if(indemnizacion.getContrato() != null){
							contrato = indemnizacion.getContrato();
							break;
						}
		}}}}
		return contrato;
	}
}