package mx.com.afirme.midas2.service.impl.negocio.agente;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.agente.NegocioAgenteDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Gerencia;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.agente.NegocioAgente;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.negocio.agente.NegocioAgenteService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

@Stateless
public class NegocioAgenteServiceImpl implements NegocioAgenteService {
	
	private EntidadService entidadService;	
	
	private EntidadDao entidadDao;
	
	private NegocioAgenteDao negocioAgenteDao;
	
	private AgenteMidasService agenteMidasService;
	
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setNegocioAgenteDao(NegocioAgenteDao negocioAgenteDao) {
		this.negocioAgenteDao = negocioAgenteDao;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public void relacionarNegocioAgente(String accion, NegocioAgente negocioAgente) {
		try{			
			negocioAgente.setIdTcAgente(negocioAgente.getAgente().getId().intValue());
			negocioAgente.setNegocio(entidadService.findById(Negocio.class, negocioAgente.getNegocio().getIdToNegocio()));
			entidadDao.executeActionGrid(accion, negocioAgente);
		}catch(Exception ex){
			throw new RuntimeException("No puedo guardarse la relacion");
		}
	}

	@Override
	public void relacionarNegocioAgente(String accion, BigDecimal idToNegocioAgente, Long idToNegocio, Integer idTcAgente) {
		try{
			Negocio negocio = entidadService.findById(Negocio.class, idToNegocio);
			Agente agente = entidadService.findById(Agente.class, idTcAgente);			
			NegocioAgente negocioAgente = new NegocioAgente();
			negocioAgente.setAgente(agente);
			negocioAgente.setNegocio(negocio);
			negocioAgente.setIdToNegAgente(idToNegocioAgente);
			this.relacionarNegocioAgente(accion, negocioAgente);
		}catch(Exception ex){
			throw new RuntimeException("Imposible crear la relacion del agente con el negocio");
		}
		
	}
	
	@Override
	public List<NegocioAgente> listarNegocioAgentesAsociados(Long idToNegocio) {
		List<NegocioAgente> lista = negocioAgenteDao.listarAgentesAsociados(idToNegocio);
		try{
			for(NegocioAgente negocioAgente : lista){
				Agente agente = new Agente();
				agente.setId(negocioAgente.getIdTcAgente().longValue());
				negocioAgente.setAgente(agenteMidasService.loadById(agente));
			}
		}catch(Exception ex){
			throw new RuntimeException("Error al tratar de obtener el listado de agentes");
		}
		return lista;
	}
	public List<NegocioAgente> listarNegocioAgentesAsociadosLight(Long idToNegocio){
		List<NegocioAgente> lista = negocioAgenteDao.listarAgentesAsociados(idToNegocio);
		for(NegocioAgente negocioAgente : lista) {
			final Agente filtroAgente = new Agente();
			filtroAgente.setId(negocioAgente.getIdTcAgente().longValue());
			List<AgenteView> agenteViewList = agenteMidasService.findByFilterLightWeight(filtroAgente);
			if (agenteViewList != null && !agenteViewList.isEmpty()) {
				final AgenteView view = agenteViewList.get(0);
				negocioAgente.getAgente().setPersona(new Persona());
				negocioAgente.getAgente().getPersona().setNombreCompleto(view.getNombreCompleto());
				negocioAgente.getAgente().setId(negocioAgente.getIdTcAgente().longValue());
				negocioAgente.getAgente().setIdAgente(view.getIdAgente());
				negocioAgente.getAgente().getPersona().setRfc(view.getCodigoRfc());
				negocioAgente.getAgente().getPersona().setCurp(view.getCodigoCurp());
			}
		}
		return lista;
	}
	@Override
	public List<NegocioAgente> listarNegocioAgentesDisponibles(Long idToNegocio,
			Object oficinaId, Object gerenciaId, Object promotoriaId) {
		Agente filtroAgente = new Agente();
		List<Agente> todos = new ArrayList<Agente>();
		List<NegocioAgente> disponibles = new ArrayList<NegocioAgente>();
		if(gerenciaId == null && oficinaId == null && promotoriaId == null){
			todos = agenteMidasService.findByFilters(filtroAgente);
			//todos = agenteService.listarAgentes();
		}else{
			//todos = agenteService.listarAgentes();
			filtroAgente.setPromotoria(new Promotoria());
			filtroAgente.getPromotoria().setEjecutivo(new Ejecutivo());
			filtroAgente.getPromotoria().getEjecutivo().setGerencia(new Gerencia());
			if(!StringUtil.isEmpty(promotoriaId.toString())){
				filtroAgente.getPromotoria().setId(Long.valueOf(promotoriaId.toString()));	
			}
			if(!StringUtil.isEmpty(oficinaId.toString())){				
				filtroAgente.getPromotoria().getEjecutivo().setId(Long.valueOf(oficinaId.toString()));
			}
			if(!StringUtil.isEmpty(gerenciaId.toString())){				
				filtroAgente.getPromotoria().getEjecutivo().getGerencia().setId(Long.valueOf(gerenciaId.toString()));
			}
			todos = agenteMidasService.findByFilters(filtroAgente);
			//todos = agenteService.listarAgentes(null, gerenciaId, oficinaId, promotoriaId);
		}
		List<NegocioAgente> asociados = this.listarNegocioAgentesAsociados(idToNegocio);		
		for(NegocioAgente negocioAgente : asociados){
			todos = eliminarAgenteComoOpcion(todos, negocioAgente);
		}	
		disponibles = convertirAgentesANegocioAgente(idToNegocio, todos);
		return disponibles;
	}
	
	@SuppressWarnings("unchecked")
	private List<Agente> eliminarAgenteComoOpcion(List<Agente> lista, final NegocioAgente negocioAgente){		
		Predicate predicado = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				Agente obj = (Agente)arg0;
				return obj.getId().intValue() == negocioAgente.getIdTcAgente().intValue();
			}
		};		
		return (List<Agente>)CollectionUtils.selectRejected(lista, predicado);		
	}
	
	private List<NegocioAgente> convertirAgentesANegocioAgente(Long idToNegocio, List<Agente> listaAgentes){
		NegocioAgente negocioAgente = null;
		List<NegocioAgente> conversion = new ArrayList<NegocioAgente>();
		Negocio negocio = entidadService.findById(Negocio.class, idToNegocio);
		for(Agente agente : listaAgentes){
			negocioAgente = new NegocioAgente();
			negocioAgente.setNegocio(negocio);
			negocioAgente.setIdTcAgente(agente.getId().intValue());
			negocioAgente.setAgente(agente);
			conversion.add(negocioAgente);
		}
		return conversion;
	}

	@Override
	public void eliminarTodasLasRelaciones(Long idToNegocio) {
		List<NegocioAgente> lista = listarNegocioAgentesAsociados(idToNegocio);
		for(NegocioAgente negocioAgente : lista){
			entidadDao.remove(negocioAgente);
		}			
	}

	@Override
	public List<Negocio> listarNegociosPorAgente(Integer idToAgente) {
		return negocioAgenteDao.listarNegociosPorAgente(idToAgente);
	}

	@Override
	public List<Negocio> listarNegociosPorAgente(Integer idToAgente, String cveNegocio,
			Integer status, Boolean esExterno) {
		//Comentario integracion
		return negocioAgenteDao.listarNegociosPorAgente(idToAgente, cveNegocio, status, esExterno);
	}

	@Override
	public List<Negocio> listarNegociosPorAgenteClaveNegocio(Integer idToAgente,
			String claveNegocio) {
		return negocioAgenteDao.listarNegociosPorAgenteClaveNegocio(idToAgente, claveNegocio);
	}
	
	@Override
	public NegocioAgente getNegocioAgente(Negocio negocio, Agente agente) {
		return negocioAgenteDao.findByNegocioAndAgente(negocio, agente);
	}
}
