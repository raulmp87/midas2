package mx.com.afirme.midas2.service.impl.tarifa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.escalon.EscalonFacadeRemote;
import mx.com.afirme.midas2.dao.tarifa.ConfiguracionTarifaAutoDao;
import mx.com.afirme.midas2.dao.tarifa.TarifaAutoDao;
import mx.com.afirme.midas2.domain.tarifa.ConfiguracionTarifaAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAutoId;
import mx.com.afirme.midas2.domain.tarifa.TarifaVersionId;
import mx.com.afirme.midas2.dto.ControlDinamicoDTO;
import mx.com.afirme.midas2.dto.RegistroDinamicoDTO;
import mx.com.afirme.midas2.dto.RiesgoGrupoClasificacionDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.wrapper.CatalogoValorFijoComboDTO;
import mx.com.afirme.midas2.service.tarifa.TarifaAutoService;
import mx.com.afirme.midas2.utils.Convertidor;

@Stateless
public class TarifaAutoServiceImpl extends JpaTarifaService<TarifaAutoId, TarifaAuto> implements TarifaAutoService {

	private TarifaAutoDao tarifaAutoDao;
	private ConfiguracionTarifaAutoDao configuracionTarifaAutoDao;
	private Convertidor convertidor;
	
	@EJB
	public void setConvertidor(Convertidor convertidor) {
		this.convertidor = convertidor;
	}
	
	@Override
	public List<TarifaAuto> findByTarifaVersionId(TarifaVersionId tarifaVersionId) {
		return tarifaAutoDao.findByTarifaVersionId(tarifaVersionId);
	}

	@Override
	public RegistroDinamicoDTO verDetalle(
			TarifaVersionId tarifaVersionId,
			String id,
			String accion, Class<?> vista) {
		RegistroDinamicoDTO registro=null;
		TarifaAuto tarifa=null;
		if(TipoAccionDTO.getAgregarModificar().equals(accion)){
			tarifa=new TarifaAuto();
			TarifaAutoId idTarifa=new TarifaAutoId(tarifaVersionId);
			tarifa.setId(idTarifa);
			tarifa.setIdVerExterno(0l);tarifa.setValor(new BigDecimal("0"));tarifa.setIdExterno(0l);
			registro=getRegistroDinamico(tarifa,null,null,null);
						
		}
		else if(TipoAccionDTO.getEliminar().equals(accion) || TipoAccionDTO.getEditar().equals(accion)){
			TarifaAutoId tarifaId=new TarifaAutoId(id);
			tarifa=findById(TarifaAuto.class,tarifaId);
			
			//Generar un registro dinamico a partir del objeto tarifa
			registro=getRegistroDinamico(tarifa,null,null,null);
			
			if(registro!=null){
				
				List<ControlDinamicoDTO> controles=registro.getControles();
				//Cuando es baja, ningun campo es editable
				
				//Dependiendo de la acción, determinar qué campos no son editables
				if(TipoAccionDTO.getEliminar().equals(accion)){
					convertidor.setAttribute("editable", false, controles);
				}
				else if (TipoAccionDTO.getEditar().equals(accion)){
					//Cuando es cambio solo los ids no son editables
					convertidor.setAttribute("editable", false, controles,"id.idBase1","id.idBase2","id.idBase3","id.idBase4");
				}
			}
		}
		
		return registro;
	}
	
	@Override
	public List<RegistroDinamicoDTO> getRegistrosDinamicosPorTarifaVersionId(TarifaVersionId tarifaVersionId, Class<?> vista) {
		List<TarifaAuto> listadoTarifaAuto = findByTarifaVersionId(tarifaVersionId);
		
		List<RegistroDinamicoDTO> listaRegistros = new ArrayList<RegistroDinamicoDTO>();
		
		Map<RiesgoGrupoClasificacionDTO,List<ConfiguracionTarifaAuto>> mapaConfigPorRiesgoConcepto = new HashMap<RiesgoGrupoClasificacionDTO, List<ConfiguracionTarifaAuto>>();
		
		Map<Integer,List<CatalogoValorFijoDTO>> mapaCatalogoValorFijo = new HashMap<Integer, List<CatalogoValorFijoDTO>>();
		
		Map<String,List<CacheableDTO>> mapaCacheablesPorClase = new HashMap<String, List<CacheableDTO>>();
		
		for(TarifaAuto tarifa : listadoTarifaAuto){
			listaRegistros.add(
					getRegistroDinamico(tarifa,mapaConfigPorRiesgoConcepto,mapaCatalogoValorFijo,mapaCacheablesPorClase)
			);
		}
		return listaRegistros;
	}
	
	@Override
	public TarifaAuto getNewObject() {
		return new TarifaAuto();
	}
	
	private RegistroDinamicoDTO getRegistroDinamico(TarifaAuto tarifaAuto,
			Map<RiesgoGrupoClasificacionDTO,List<ConfiguracionTarifaAuto>> mapaConfigPorRiesgoConcepto,
			Map<Integer,List<CatalogoValorFijoDTO>> mapaCatalogoValorFijo,
			Map<String,List<CacheableDTO>> mapaCacheablesPorClase){
		RegistroDinamicoDTO registroDinamico = null;
		ControlDinamicoDTO controlDinamicoDTO = null;
		
		//Generar los controles dinamicos para los primeros campos del ID, que son fijos.
		registroDinamico = convertidor.getRegistroDinamico(tarifaAuto);
		
		//Consultar los registros de configuracion para la tarifa
		List<ConfiguracionTarifaAuto> listaConfiguracion = null;
		RiesgoGrupoClasificacionDTO ids = new RiesgoGrupoClasificacionDTO(tarifaAuto.getId().getIdToRiesgo(),tarifaAuto.getId().getIdConcepto());
		if(mapaConfigPorRiesgoConcepto != null){
			listaConfiguracion = mapaConfigPorRiesgoConcepto.get(ids);
		}
		
		if(listaConfiguracion == null){
			listaConfiguracion = configuracionTarifaAutoDao.getConfiguracionTarifaAuto(tarifaAuto);
		}
		
		if(mapaConfigPorRiesgoConcepto != null && !mapaConfigPorRiesgoConcepto.containsKey(ids)){
			mapaConfigPorRiesgoConcepto.put(ids, listaConfiguracion);
		}
		
		//Agregar los controles dinamicos correspondientes a los campos dinamicos de la tarifa
		for(ConfiguracionTarifaAuto configuracionPorColumna : listaConfiguracion){
			controlDinamicoDTO = getControlDinamico(configuracionPorColumna , tarifaAuto,mapaCatalogoValorFijo,mapaCacheablesPorClase);
			registroDinamico.getControles().add(controlDinamicoDTO);
		}
		
		return registroDinamico;
	}
	
	@SuppressWarnings("unchecked")
	private ControlDinamicoDTO getControlDinamico(ConfiguracionTarifaAuto configuracionTarifaAuto,TarifaAuto tarifaAuto,
			Map<Integer,List<CatalogoValorFijoDTO>> mapaCatalogoValorFijo,
			Map<String,List<CacheableDTO>> mapaCacheablesPorClase){
		ControlDinamicoDTO controlDinamicoDTO = new ControlDinamicoDTO();
		String claseRemota = null;
		MidasInterfaceBaseAuto<CacheableDTO> beanBaseAuto = null;
		MidasInterfaceBase<CacheableDTO> beanBase = null;		
		
		List<CacheableDTO> listaCacheables = null;
		int grupoValores = 0;
		
		String valor = "";
		
		if (tarifaAuto.getId().getIdMoneda().intValue() == MonedaDTO.MONEDA_PESOS){
			grupoValores = configuracionTarifaAuto.getIdGrupoMN().intValue();
		}
		else{
			grupoValores = configuracionTarifaAuto.getIdGrupoDLS().intValue();
		}
		
		String atributoMapeo=null;
		boolean setHidden = true;
		
		switch (configuracionTarifaAuto.getId().getIdDato().intValue()){
			case 5:
				atributoMapeo = "valor";
				valor = (tarifaAuto.getValor() != null ) ?tarifaAuto.getValor().toString() : "";
				break;
			case 1:
				valor = tarifaAuto.getId().getIdBase1().toString();
				atributoMapeo = "id.idBase1";
				break;
			case 2:
				valor = tarifaAuto.getId().getIdBase2().toString();
				//Danios Materiales - Factor de Dcto/Rcgo por Deducible DM
				if(tarifaAuto.getId().getIdToRiesgo() == 302 && tarifaAuto.getId().getIdConcepto() == 10){
					valor = "1";
					setHidden = false;
				}
				atributoMapeo = "id.idBase2";
				break;
			case 3:
				valor = tarifaAuto.getId().getIdBase3().toString();
				atributoMapeo = "id.idBase3";
				break;
			case 4:
				valor = tarifaAuto.getId().getIdBase4().toString();
				atributoMapeo = "id.idBase4";
				break;
		}
		
		controlDinamicoDTO.setEtiqueta(configuracionTarifaAuto.getDescripcionEtiqueta());
		controlDinamicoDTO.setEditable(true);
		controlDinamicoDTO.setAtributoMapeo(atributoMapeo);
		controlDinamicoDTO.setValor(valor);
		
		try{
		
		switch (configuracionTarifaAuto.getClaveTipoControl().intValue()){
			case ConfiguracionTarifaAuto.HIDDEN: //combo deshabilitado
				controlDinamicoDTO.setEtiqueta("");
				controlDinamicoDTO.setTipoControl(ControlDinamicoDTO.HIDDEN);
				controlDinamicoDTO.setEditable(false);
				if(setHidden){
					controlDinamicoDTO.setValor("0");
				}
				break;
			case ConfiguracionTarifaAuto.COMBO_PROPIO:	//combo de cat�logo propio
				claseRemota = configuracionTarifaAuto.getDescripcionClaseRemota().trim();
				
				if(mapaCacheablesPorClase != null){
					listaCacheables = mapaCacheablesPorClase.get(claseRemota);
				}
				
				if(listaCacheables == null){
					if (	//Da�os Materiales - Factor de Dcto/Rcgo por Tipo de Vehiculo DM	
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==303)) ||
							///Da�os Materiales - Factor de Dcto/Rcgo por Tipo de Vehiculo DM
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==304)) || 
							//Da�os Materiales - Factor de Dcto/Rcgo por Uso de Vehiculo DM	
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==2
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==304)) || 
							//Robo Total - Factor de Dcto/Rcgo por Tipo de Vehiculo RT		
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==313))|| 
							//Robo Total - Factor de Dcto/Rcgo por Uso de Vehiculo RT		
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==2								
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==314))|| 
							//Robo Total - Factor de Dcto/Rcgo por Tipo de Vehiculo RT			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1								
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==314))|| 
							//Responsabilidad Civil - Factor de Dcto/Rcgo por Tipo de Vehiculo RC		
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1								
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==323))|| 
							//Responsabilidad Civil - Factor de Dcto/Rcgo por Tipo de Vehiculo RC		
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==324))|| 
							//Responsabilidad Civil - Factor de Dcto/Rcgo por Uso de Vehiculo RC			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==2
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==324))|| 
							//Gastos Medicos - Factor de Dcto/Rcgo por Tipo de Vehiculo GMO			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==333))|| 
							//Gastos Medicos - Factor de Dcto/Rcgo por Tipo de Vehiculo GMO			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10	
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==334))||
							//Gastos Medicos - Factor de Dcto/Rcgo por Uso de Vehiculo GMO			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10	
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==2
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==334))|| 
							//Accidentes Automovilisticos al Conductor - Factor de Dcto/Rcgo por Tipo de Vehiculo AAC			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10	
									&& configuracionTarifaAuto.getId().getIdDato().longValue()==1								
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==383))|| 
							//Accidentes Automovilisticos al Conductor - Factor de Dcto/Rcgo por Tipo de Vehiculo AAC			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
									&& configuracionTarifaAuto.getId().getIdDato().longValue()==1							
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==384))|| 
							//Accidentes Automovilisticos al Conductor - Factor de Dcto/Rcgo por Uso de Vehiculo AAC			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
									&& configuracionTarifaAuto.getId().getIdDato().longValue()==2							
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==384))|| 
							//Adaptaciones y Conversiones- Factor de Dcto/Rcgo por Tipo de Vehiculo AYC			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10			
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1	
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==394))|| 
							//Equipo Especial- Factor de Dcto/Rcgo por Tipo de Vehiculo EQE			
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1	
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==404))|| 
							//Responsabilidad Civil Viajero - Factor de Dcto/Rcgo por Tipo de Vehiculo RCViajero	
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==1	
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==413))|| 
							//Responsabilidad Civil Viajero - Factor de Dcto/Rcgo por Uso de Vehiculo RCViajero	
							((configuracionTarifaAuto.getId().getIdConcepto().longValue()==10		
								&& configuracionTarifaAuto.getId().getIdDato().longValue()==2	
								&& configuracionTarifaAuto.getId().getIdToRiesgo().longValue()==413))){
						beanBaseAuto = ServiceLocatorP.getInstance().getEJB(claseRemota);
						listaCacheables = beanBaseAuto.findAll();
					}else{
						beanBase = ServiceLocatorP.getInstance().getEJB(claseRemota);
						listaCacheables = beanBase.findAll();
					}
										
				}
				
				if(mapaCacheablesPorClase != null && !mapaCacheablesPorClase.containsKey(claseRemota)){
					mapaCacheablesPorClase.put(claseRemota, listaCacheables);
				}
				
				controlDinamicoDTO.setTipoControl(ControlDinamicoDTO.SELECT);
				controlDinamicoDTO.setJavaScriptOnChange(configuracionTarifaAuto.getClaveComboHijo());
				controlDinamicoDTO.setElementosCombo(listaCacheables);
				
				break;
			case ConfiguracionTarifaAuto.COMBO_CATALOGO_VALOR_FIJO:	//combo de catalogo Valores Fijos (CatalogoValorFijoDTO)
				
				List<CatalogoValorFijoDTO> listaCatalogosFijos = null;
				
				if(mapaCatalogoValorFijo != null){
					listaCatalogosFijos = mapaCatalogoValorFijo.get(grupoValores);
				}
				
				if(listaCatalogosFijos == null){
					CatalogoValorFijoFacadeRemote beanCatalogoValorFijo = ServiceLocatorP.getInstance().getEJB(CatalogoValorFijoFacadeRemote.class);
					listaCatalogosFijos = beanCatalogoValorFijo.findByProperty("id.idGrupoValores", grupoValores);
				}
				
				if(mapaCatalogoValorFijo != null && !mapaCatalogoValorFijo.containsKey(grupoValores)){
					mapaCatalogoValorFijo.put(grupoValores, listaCatalogosFijos);
				}
				
				List<CacheableDTO> listaCombo = new ArrayList<CacheableDTO>();
				
				CatalogoValorFijoComboDTO cacheable = null;
				for(CatalogoValorFijoDTO catalogoValorFijoDTO : listaCatalogosFijos){
					cacheable = new CatalogoValorFijoComboDTO();
					cacheable.setCatalogoValorFijoDTO(catalogoValorFijoDTO);
					listaCombo.add((CacheableDTO)cacheable);
				}
				
				controlDinamicoDTO.setTipoControl(ControlDinamicoDTO.SELECT);
				controlDinamicoDTO.setElementosCombo(listaCombo);
				
				break;
			case ConfiguracionTarifaAuto.COMBO_ESCALON:	//combo de EscalonDTO
				EscalonFacadeRemote beanEscalon = ServiceLocatorP.getInstance().getEJB(EscalonFacadeRemote.class);
				
				@SuppressWarnings("rawtypes")
				List listaEscalon = beanEscalon.findByProperty("claveClasificacionEscalon", (short)grupoValores);
				
				controlDinamicoDTO.setTipoControl(ControlDinamicoDTO.SELECT);
				controlDinamicoDTO.setElementosCombo(listaEscalon);
				
				break;
			case ConfiguracionTarifaAuto.CAJA_TEXTO:	//caja de texto
				
//				String onkeypress = "return soloNumeros(this,event,true)";
//				String longitudMaxima = "9";
//				String onblur="";
//				switch (configuracionTarifaAuto.getClaveTipoValidacion().intValue()){
//					case 1:	{//cuota al millar, validar 8 digitos con 4 decimales
//						onblur="validarDecimal(this.form.registro"+atributoMapeo+", this.value, 8, 4)";
//						break;}
//					case 2:	{//porcentaje, validar 8 digitos con 2 decimales
//						onblur="validarDecimal(this.form.registro"+atributoMapeo+", this.value, 8, 2)";
//						break;}
//					case 3:	{//importe. validar 16 digitos con 2 decimales 
//						onblur="validarDecimal(this.form.registro"+atributoMapeo+", this.value, 16, 2)";
//						longitudMaxima = "17";
//						break;}
//					case 4:{	//factor, valodar 16 d�gitos con 4 decimales
//						onblur="validarDecimal(this.form.registro"+atributoMapeo+", this.value, 16, 4)";
//						longitudMaxima = "17";
//						break;}
//					case 5:	{//DSMGVDF, validar entero de 8 d�gitos
//						onblur="validarDecimal(this.form.registro"+atributoMapeo+", this.value, 8, 0)";
//						onkeypress = "return soloNumeros(this,event,false)";
//						break;}
//				}
//				
//				controlDinamicoDTO.setTipoControl(ControlDinamicoDTO.TEXTBOX);
//				controlDinamicoDTO.setJavaScriptOnBlur(onblur);
//				controlDinamicoDTO.setJavaScriptOnKeyPress(onkeypress);
//				controlDinamicoDTO.setLongitudMaxima(longitudMaxima);
				
				String onkeypress = "return soloNumeros(this,event";
				String longitudMaxima = "17";
				String params = "";
				switch (configuracionTarifaAuto.getClaveTipoValidacion().intValue()){
					case 1:	{//cuota al millar, validar 8 digitos con 10 decimales
						params = ",true,false, 8, 10";
						break;}
					case 2:	{//porcentaje, validar 8 digitos con 10 decimales
						params = ",true,false, 8, 10";
						break;}
					case 3:	{//importe. validar 16 digitos con 10 decimales
						params = ",true,false, 16, 10";
						longitudMaxima = "22";
						break;}
					case 4:{	//factor, valodar 16 d�gitos con 10 decimales
						params = ",true,false, 16, 10";
						longitudMaxima = "22";
						break;}
					case 5:	{//DSMGVDF, validar entero de 8 d�gitos
						params = ",false";
						longitudMaxima = "8";
						break;}
					case 6:	{//porcentaje con signo, validar 8 digitos con 10 decimales
						params = ",true,true, 8, 10";
						break;}
				}
				String onkeypressComplete = onkeypress + params + ")";
				controlDinamicoDTO.setTipoControl(ControlDinamicoDTO.TEXTBOX);
				controlDinamicoDTO.setJavaScriptOnKeyPress(onkeypressComplete);
				controlDinamicoDTO.setLongitudMaxima(longitudMaxima);
				
				break;
			case ConfiguracionTarifaAuto.COMBO_POR_TARIFA_VERSION_ID:
				claseRemota = configuracionTarifaAuto.getDescripcionClaseRemota().trim();
				beanBase = ServiceLocatorP.getInstance().getEJB(claseRemota);
				
				TarifaVersionId tarifaVersionId = new TarifaVersionId();
				tarifaVersionId.setIdMoneda(tarifaAuto.getId().getIdMoneda());
				tarifaVersionId.setIdConcepto(tarifaAuto.getId().getIdConcepto());
				tarifaVersionId.setIdRiesgo(tarifaAuto.getId().getIdToRiesgo());
				tarifaVersionId.setVersion(tarifaAuto.getId().getIdVerTarifa());
				
				listaCacheables = beanBase.listRelated(tarifaVersionId);
				
				controlDinamicoDTO.setTipoControl(ControlDinamicoDTO.SELECT);
				controlDinamicoDTO.setJavaScriptOnChange(configuracionTarifaAuto.getClaveComboHijo());
				controlDinamicoDTO.setElementosCombo(listaCacheables);
				break;
			case ConfiguracionTarifaAuto.COMBO_POR_TIPO_VEHICULO:
				
				claseRemota = configuracionTarifaAuto.getDescripcionClaseRemota().trim();
				beanBase = ServiceLocatorP.getInstance().getEJB(claseRemota);
				
				Long idTipoVehiculo = null;
				switch (configuracionTarifaAuto.getId().getIdDato().intValue()){
					case 5:
						idTipoVehiculo = new Long(tarifaAuto.getId().getIdBase4());
						break;
					case 2:
						idTipoVehiculo = new Long(tarifaAuto.getId().getIdBase1());
						break;
					case 3:
						idTipoVehiculo = new Long(tarifaAuto.getId().getIdBase2());
						break;
					case 4:
						idTipoVehiculo = new Long(tarifaAuto.getId().getIdBase3());
						break;
				}
				
				listaCacheables = beanBase.listRelated(idTipoVehiculo);
				
				controlDinamicoDTO.setTipoControl(ControlDinamicoDTO.SELECT);
				controlDinamicoDTO.setJavaScriptOnChange(configuracionTarifaAuto.getClaveComboHijo());
				controlDinamicoDTO.setElementosCombo(listaCacheables);
				
				break;
		}
		
		}catch(Exception e){
			throw new RuntimeException(e);
		}
		
		return controlDinamicoDTO;
	}

	@EJB
	public void setConfiguracionTarifaAutoDao(ConfiguracionTarifaAutoDao configuracionTarifaAutoDao) {
		this.configuracionTarifaAutoDao = configuracionTarifaAutoDao;
	}
	
	@EJB
	public void setTarifaAutoDao(TarifaAutoDao tarifaAutoDao) {
		this.tarifaAutoDao = tarifaAutoDao;
	}
}
