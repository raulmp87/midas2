package mx.com.afirme.midas2.service.impl.siniestros.hgs;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.TipoDocumentoMovimiento;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import javax.xml.ws.soap.SOAPFaultException;

import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.valuacion.ordencompra.OrdenCompraDao;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumModoEnvio;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoPaseAtencion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.MovimientoCoberturaSiniestro.CausaMovimiento;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina.EstatusReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.catalogo.conceptos.ConceptoAjuste;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.provision.MovimientoProvisionSiniestros;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.MedioRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.OrigenRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.TipoRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.DevolucionHgs;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.DevolucionHgsRefaccion;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.HgsEnvioLog;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.HgsEnvioLog.TipoLogHGS;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionHgs;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionHgsDetalle;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionValeRefaccion;
import mx.com.afirme.midas2.domain.siniestros.valuacion.hgs.ValuacionValeRefaccionFinal;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora.STATUS_BITACORA;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_PROCESO;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.dto.TransporteCommDTO;
import mx.com.afirme.midas2.dto.siniestros.hgs.ConsultaHgsDto;
import mx.com.afirme.midas2.dto.siniestros.hgs.Efile;
import mx.com.afirme.midas2.dto.siniestros.hgs.EfileDevolucion;
import mx.com.afirme.midas2.dto.siniestros.hgs.ValeRefaccion;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ValuacionHGSEditDTO;
import mx.com.afirme.midas2.exeption.CommException;
import mx.com.afirme.midas2.exeption.CommException.CODE;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.sistema.comm.CommServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.conceptos.ConceptoAjusteService;
import mx.com.afirme.midas2.service.siniestros.indemnizacion.perdidatotal.PerdidaTotalService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EmailDestinaratios;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;
import mx.com.afirme.midas2.service.siniestros.provision.SiniestrosProvisionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionProveedorService;
import mx.com.afirme.midas2.service.siniestros.valuacion.hgs.HgsService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.comm.CommBitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.wsClient.hgs.EfileServiceSOAPPortProxy;
import mx.com.afirme.midas2.wsClient.hgs.Reporte;
import mx.com.afirme.midas2.wsClient.hgs.Reporte.Vehiculo;

import org.apache.axis.types.PositiveInteger;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;


@Stateless
public class HgsServiceImpl extends CommServiceImpl implements HgsService {

	private static final Logger LOG = Logger.getLogger(HgsServiceImpl.class);
	private static final Integer[] TIPO_PROCESO_ESTATUS_VALIDO = {0,1,2,3,4,5,6,7,8};
	private static final Integer[] ESTATUS_VALE            = {0,1,2,3,4,5,6};
	private static final Integer[] ESTATUS_VALUACION       = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23};
	private static final short REPARACION                  = 1;
	private static final short PAGO_DANOS                  = 2;
	private static final short PERDIDA_TOTAL               = 3;
	private static final short DANOS_MENOR_DEDUCIBLE       = 4;
	private static final short NO_PROCEDE                  = 5;
	private static final short RECHAZADO                   = 6;
	private static final short VALE_REFACCION_AUTORIZADO   = 1;
	private static final short TIPO_VALUADO 			   = 7;
	private static final short TIPO_ASIGNADO_VALUADOR      = 9;
	private static final short TIPO_VALUADO_COTIZADO       = 24;
	private static final short TIPO_AUTORIZADO             = 10;
	private static final short TIPO_COMPLEMENTO_AUTORIZADO  = 17;
	private static final short TIPO_TERMINADO              = 8;
	private static final short TIPO_ENTREGADO              = 14;
	
	private final String RESPUESTA_OK                      = "00 | OK";
	
	//private String error                                   = "00 | OK";
	//private String errorPiezas                             = "00 | OK";
	private String errorRecuperacion                       = "00 | OK";
	//private String listaOrdenesVales                       = "";
	
	
	private final String LISTADO_VALES                     = "LISTADO_VALES";
	
	private Map<String,String> respuestaMap;
	private Map<String,String> respuestaPiezasMap;
	
	private static final String PAGADO                     = "pagado";
	private static final String EMAIL_BASE_NOTIFICA_ERROR  = "herbert.guzman@afirme.com";
	private static final String CODIGO_PAGO_DANIOS_VALE    = "PADA";
	private static final String TIPO_PROCESO_STR_DANIOS    = "PAGO_DANIOS";
	private static final String TIPO_PROCESO_STR_PTOTAL    = "PERDIDA_TOTAL";
	private static final String CODIGO_MANO_OBRA_VALE      = "MOPE2";   
	private static final String MOTIVO_DEVOLUCION_PIEZAS   = "OTROS";
	private static final String PERSONA_FISICA 			   = "PF";
	private static final String ALTA_VALUACION_MANUAL_HGS  = "ALTA";
	
	@EJB
	private EntidadService                      entidadService;
	@EJB
	private ParametroGlobalService              parametroGlobalService;
	@EJB
	private ListadoService                      listadoService;
	@EJB
	private PerdidaTotalService                 perdidaTotalService;
	@EJB
	private EnvioNotificacionesService          envioNotificacionesService;
	@EJB
	private BitacoraService 					bitacoraService;
	@EJB
	private RecuperacionProveedorService 		recuperacionProveedorService;
	@EJB
	private RecuperacionDeducibleService		recuperacionDeducibleService;
	@EJB
	private OrdenCompraDao                      ordenCompraDao;
	@EJB
	private CommBitacoraService                 commBitacoraService;
	@EJB
	private OrdenCompraService                  ordenCompraService;
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	@EJB
	private EstimacionCoberturaSiniestroService estimacionService;
	@EJB
	private ReporteCabinaService reporteCabinaService;
	
	@EJB
	private SiniestrosProvisionService siniestroPrivisionService;	
	
	
	
	public static final int MIDAS_APP_ID = 5;
	public static final String PARAMETRO_TOTAL_HGS = "VARIACION_TOTAL_HGS";
	
	
	
	private enum MOVIMIENTO_ORDEN_PAGO{
		VALE(1),GENERAL(2);
		private final int value;
		private MOVIMIENTO_ORDEN_PAGO(final int xVal) {
	            value = xVal;
	        }
		public int getValue() { return value; }
	}
	
	private enum TIPO_CONCEPTO{
		PT,MO,RB,REF,DA,OT
	}
	
	private enum TIPO_PAGO{
		PP,PB
	}
	
	
	

	// #########################################################################################
	// #####################
	// #####################     COLOCAR REPORTE EN HGS    ************************************
	// #####################
	// #########################################################################################
	
	public HgsServiceImpl() {
		super();
		if(this.respuestaMap == null){
			this.respuestaMap = new HashMap<String,String>();
		}
		if(this.respuestaPiezasMap == null){
			this.respuestaPiezasMap = new HashMap<String,String>();
		}
	}
	
	
	/*public HgsServiceImpl(Reporte reporteHgsEnviar) {
		this.reporteHgsEnviar = reporteHgsEnviar;
	}*/

	@Override
	@Asynchronous
	public void insertarReporte(Long idReporteSin) {
		
		LOG.info("HGS - Preparando envio de todos los pases");
		
		try {
		
			ReporteCabina reporteCabina 		     = this.obtenerReporteCabina(idReporteSin);
			List<EstimacionCoberturaReporteCabina>lCoberturaReporteCabina = this.buscarCoberturas(idReporteSin);
			
			if( !lCoberturaReporteCabina.isEmpty() ){
				
				for( EstimacionCoberturaReporteCabina lCor : lCoberturaReporteCabina ){
	
					// ENVIAR REPORTE
					this.envioReporte(reporteCabina, lCor);
	
				}
			}else{
				// # LOG
				this.salvaHistoricoEnvio(
										idReporteSin, 
										null,
										HgsEnvioLog.TipoLogHGS.REPORTE,
										"Reporte sin datos de estimacion",
										"","",true
										);
			}
		
		}catch(Exception e) {
			LOG.error("HGS - Preparando envio insertarReporte ",e);
		}
		
	}
	
	@Override
	public String insertarReportePaseAtencion(Long keyEstimacion){
		LOG.info("HGS - Preparando envio individual");
		String mensaje = "";
		
		try {
			
			EstimacionCoberturaReporteCabina estimacionCobertura = entidadService.findById(EstimacionCoberturaReporteCabina.class, keyEstimacion);
			
			if( estimacionCobertura != null ){
				
				// # OBTENER REPORTE CABINA
				ReporteCabina reporteCabina = estimacionCobertura.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina();
				
				if( reporteCabina != null ){
					LOG.info("HGS - envio individual");
					
					HgsService processor = super.sessionContext.getBusinessObject(HgsService.class);
					processor.envioReporte(reporteCabina, estimacionCobertura);
					mensaje = "Procesando envio";
				}
				
			}else{
				LOG.info("Error | Estimacion no existe");
				mensaje = "Error | Estimacion no existe ";
			}
		
		}catch(Exception e) {
			LOG.error("HGS - Preparando envio insertarReportePaseAtencion ",e);
		}
		
		return mensaje;
	}
	
	@SuppressWarnings("unchecked")
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	@Asynchronous
	@Override
	public void envioReporte(ReporteCabina reporteCabina,EstimacionCoberturaReporteCabina lCor){
		
		try{
			
			if(  reporteCabina.getOficina().getDerivaHGS() ){			

				if (EnumUtil.equalsValue(lCor.getTipoEstimacion(), TipoEstimacion.RC_VEHICULO, TipoEstimacion.DANIOS_MATERIALES)){	
					
					if(EnumUtil.equalsValue(lCor.getTipoPaseAtencion(), TipoPaseAtencion.PASE_ATENCION_TALLER,TipoPaseAtencion.PAGO_DANIOS)) {
						
						if( lCor.getEsEditable().equals("EDT") ){
					
							// # OBTENER DATOS PARA REPORTE A ENVIAR HGS
							// # COBERTURAS RC VEHICULO 2 - DAÑOS MATERIALES 1
							Reporte reporteHgsEnviar = this.llenarDatosReporte(reporteCabina, lCor, 
									(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, lCor.getTipoEstimacion()) ? 2 : 1), 
									lCor.getTipoPaseAtencion() );
			
							if( reporteHgsEnviar != null ){
							
								// # LOG
								this.salvaHistoricoEnvio(
										reporteCabina.getId() , 
										lCor.getId(),
										HgsEnvioLog.TipoLogHGS.REPORTE,
										"Preparando Reporte :"+lCor.getTipoEstimacion()+" Siniestro: "+reporteHgsEnviar.getNumSiniestro(),
										reporteToString(reporteHgsEnviar),
										reporteHgsEnviar.getNumReporte(),
										true
								);
			
								try{
									
									this.realizarEnvio(reporteHgsEnviar);
									
								}catch(Exception e){
									LOG.error("HGS - Error al relizar envio envioReporte - COMMSERVICE",e);
								}
								LOG.info("HGS - ENVIO REALIZADO");
								
							}else{
								LOG.error("Reporte a Enviar vacio");
								// # LOG
								this.salvaHistoricoEnvio(
										reporteCabina.getId() , 
										lCor.getId(),
										HgsEnvioLog.TipoLogHGS.REPORTE,
										"Error: Reporte a enviar nulo :"+lCor.getTipoEstimacion(),"","",true);
							}
							
						}// # CIERRA lCor.getEsEditable()
										
					}// # CIERRA lCor.getTipoPaseAtencion()
					
				}// # CIERRA lCor.getTipoEstimacion()
				
			}else{
				LOG.info("HGS - OFICINA NO TIENE CAMPO DERIVAR HGS ACTIVO: "+ reporteCabina.getOficina().getNombreOficina());
				this.registrarBitacora(EVENTO.DERIVACION_HGS, "La OFICINA seleccionada no tiene activada la derivacion", " Derivacion HGS | Reporte Cabina: ", reporteCabina.getId().toString() );
			}
		
		}catch(Exception e){
			LOG.error("HGS - Error al enviar reporte");
			// # LOG
			this.salvaHistoricoEnvio(
					reporteCabina.getId() , 
					lCor.getId(),
					HgsEnvioLog.TipoLogHGS.REPORTE,
					"Error: envioReporte() :"+this.imprimirTrazaError(e),
					"",
					"",
					true
			);
		}
	}
	
	
	private String getValorParametroGlobal(int appMidas, String parametro ){
		
		try{
			ParametroGlobal parametroGlobal = this.parametroGlobalService.obtenerPorIdParametro( appMidas , parametro );
			
			if( parametroGlobal!=null ){
				return parametroGlobal.getValor();
			}else{
				return "";
			}

		}catch(Exception e){
			LOG.error("Error en getValorParametroGlobal: "+ this.imprimirTrazaError(e));
			// # EN CASO DE ERROR RETORNA SIEMPRE DESARROLLO
			return "";
		}
		
	}
	
	private String reporteToString(Reporte reporte){
		
		StringBuilder str = new StringBuilder();

		try{

			str.append(" Reporte [ ");
			str.append(" IDReporteSin =  ")      	.append(reporte.getIDReporteSin()).append(",");
			str.append(" numFolio =  ")          	.append(reporte.getNumFolio()).append(",");
			str.append(" numSiniestro =  ")      	.append(reporte.getNumSiniestro()).append(",");
			str.append(" numReporte =  ")        	.append(reporte.getNumReporte()).append(",");
			str.append(" numPoliza =  ")         	.append(reporte.getNumPoliza()).append(",");
			str.append(" IDLineaNegocio =  ")    	.append(reporte.getIDLineaNegocio()).append(",");
			str.append(" IDInciso =  ")          	.append(reporte.getIDInciso()).append(",");
			str.append(" fechaReporte =  ")      	.append(reporte.getFechaReporte().toString() ).append(",");
			str.append(" IDCobertura =  ")       	.append(reporte.getIDCobertura()).append(",");
			str.append(" IDCategoria =  ")		  	.append(reporte.getIDCategoria()).append(",");
			str.append(" IDAseguradoTercero =  ")	.append(reporte.getIDAseguradoTercero()).append(",");
			str.append(" nombreAsegurado =  ")   	.append(reporte.getNombreAsegurado()).append(",");
			str.append(" nombreInteresado =  ")  	.append(reporte.getNombreInteresado()).append(",");
			str.append(" numTelefono =  ")       	.append(reporte.getNumTelefono()).append(",");
			
			str.append(" claveTaller =  ")       	.append(reporte.getClaveTaller()).append(",");
			str.append(" nombreTaller =  ")      	.append(reporte.getNombreTaller()).append(",");
			str.append(" sumaAsegurada =  ")     	.append(reporte.getSumaAsegurada()).append(",");
			str.append(" aplicaDeducible =  ")      .append(reporte.getAplicaDeducible()).append(",");
			str.append(" porcentajeDeducible =  ")  .append(reporte.getPorcentajeDeducible()).append(",");
			str.append(" vehiculo =  ")          	.append( this.vehiculoToString(reporte.getVehiculo() ) ).append("]");
		
		}catch(Exception e){
			LOG.error("Error en reporteToString: "+e);
		}
		
		return str.toString();
	}
	
	//ReporteVehiculo vehiculo
	private String vehiculoToString( Vehiculo vehiculo  ){
		
		StringBuilder toString = new StringBuilder();
		try{

			toString.append(" Vechiculo [ ");
			toString.append(" Marca =  ") .append(vehiculo.getMarca()).append(",");
			toString.append(" Modelo =  ").append(vehiculo.getModelo()).append(",");
			toString.append(" Placas =  ").append(vehiculo.getPlacas()).append(",");
			toString.append(" Serie =  ") .append(vehiculo.getSerie()).append(",");
			toString.append(" Tipo =  ")  .append(vehiculo.getTipo()).append("]");
		
		}catch(Exception e){
			LOG.error("Error en vehiculoToString: "+e);
		}
		
		return toString.toString();
		
	}
	
	private Reporte llenarDatosReporte(ReporteCabina reporteCabina,EstimacionCoberturaReporteCabina estimacionCoberturaReporteCabina,
			int categoria, String tipoPaseAtencion ){
		
		Reporte reporte         = new Reporte();
		String  nombreAsegurado = "";
		
		try{

			GregorianCalendar c               = new GregorianCalendar();
			c.setTime(reporteCabina.getFechaCreacion());
			XMLGregorianCalendar fechaReporte = DatatypeFactory.newInstance().newXMLGregorianCalendar(c);

			// # OBTENER DATOS DE VEHICULO
			Map<String,Object> dataVehiculo = this.obtenerDatosVehiculo( reporteCabina ,estimacionCoberturaReporteCabina.getId() );
			
			if( (Vehiculo) dataVehiculo.get("reporteVehiculo") != null ){
			
				ConsultaHgsDto consultaHgsDto   = obtenerSumaAseguradaHGSValuacion(reporteCabina.getId(),estimacionCoberturaReporteCabina);
				
				if (EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacionCoberturaReporteCabina.getTipoEstimacion())){
					nombreAsegurado = estimacionCoberturaReporteCabina.getNombreAfectado();  
				}else{
					nombreAsegurado = reporteCabinaService.buscarNombreAseguradoAutos(reporteCabina.getPoliza().getIdToPoliza(), 
							reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso(), 
							fechaReporte.toGregorianCalendar().getTime());
				}
				
				// # REPORTE_CABINA_ID
				reporte.setIDReporteSin       ( new PositiveInteger(reporteCabina.getId().toString()));
				reporte.setAplicaDeducible    ( consultaHgsDto.getAplicaDeducible() );
				reporte.setClaveTaller        ( dataVehiculo.get("claveTaller").toString() );
				//reporte.setClaveTaller        ( ( !EnumUtil.equalsValue(TipoPaseAtencion.PAGO_DANIOS, tipoPaseAtencion) ?  "2825" : dataVehiculo.get("claveTaller").toString() )   );
				reporte.setFechaReporte       ( fechaReporte );
				reporte.setIDAseguradoTercero ( new PositiveInteger( estimacionCoberturaReporteCabina.getId().toString() ) );
				reporte.setIDCategoria        ( new PositiveInteger( String.valueOf( categoria ) ) );
				reporte.setIDCobertura        ( new PositiveInteger( estimacionCoberturaReporteCabina.getCoberturaReporteCabina().getId().toString() ) );
				reporte.setIDInciso           ( new PositiveInteger( reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getId().toString() ) );
				reporte.setIDLineaNegocio     ( new PositiveInteger( reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getSeccionDTO().getIdToSeccion().toString() ) );
				reporte.setNombreAsegurado    ( nombreAsegurado  );
				reporte.setNombreInteresado   ( dataVehiculo.get("nombreInteresado").toString() );
				reporte.setNombreTaller       ( dataVehiculo.get("nombreTaller").toString() );
				reporte.setNumFolio           ( estimacionCoberturaReporteCabina.getFolio() );
				reporte.setNumPoliza          ( String.valueOf( reporteCabina.getPoliza().getNumeroPoliza() ) );
				reporte.setNumReporte         ( reporteCabina.getNumeroReporte() );
				// SI NO ESTA SINIESTRADO ENVIA 'SIN' SI NO EL NUMERO DE SINIESTRO
				reporte.setNumSiniestro       ( this.buscarNumSiniestro(reporteCabina.getId()) );
				reporte.setNumTelefono        ( estimacionCoberturaReporteCabina.getLadaTelContacto()+" "+estimacionCoberturaReporteCabina.getTelContacto() );
				reporte.setPorcentajeDeducible( consultaHgsDto.getPorcentajeDeducible() );
				reporte.setSumaAsegurada      ( consultaHgsDto.getSumaAsegurada() );
				reporte.setVehiculo           ( (Vehiculo) dataVehiculo.get("reporteVehiculo") );
				
			}else{
				LOG.error("HGS - Datos de vehiculo vacio");
				// # LOG
				this.salvaHistoricoEnvio(
						reporteCabina.getId() , 
						estimacionCoberturaReporteCabina.getId(),
						HgsEnvioLog.TipoLogHGS.REPORTE,
						"Datos de vehiculo vacio",
						"",
						"",
						true
				);
			}
		
		}catch (Exception e){
			
			LOG.error("Error en llenarDatosReporte: ",e);
			// # LOG
			this.salvaHistoricoEnvio(
					reporteCabina.getId() , 
					estimacionCoberturaReporteCabina.getId(),
					HgsEnvioLog.TipoLogHGS.REPORTE,
					"Datos de vehiculo vacio: "+this.imprimirTrazaError(e),
					"",
					"",
					true
			);
		}

		return reporte;
	}
	
	@Override
	public Map<String,Object> obtenerDatosVehiculo ( ReporteCabina reporteCabina, Long keyIdTercero ){
		
		Vehiculo reporteVehiculo = new Vehiculo();
		String nombreTaller = "";
		String     tallerId = "";
		Map<String,Object> dataVehiculo = new HashMap<String,Object>();
		dataVehiculo.put("nombreTaller"    ,"");
		dataVehiculo.put("nombreInteresado","");
		dataVehiculo.put("claveTaller"     ,"");
		dataVehiculo.put("reporteVehiculo" ,"");
		
		try{
		
			this.salvaHistoricoEnvio(
					reporteCabina.getId() , 
					keyIdTercero,
					HgsEnvioLog.TipoLogHGS.REPORTE,
					"Obtener Datos de Vehiculo : Reporte Cabina Id: "+reporteCabina.getId()+" ID tercero: "+keyIdTercero,
					"",
					reporteCabina.getNumeroReporte(),
					true
			);
			
			EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, keyIdTercero);

			if (EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacion.getTipoEstimacion()) ){
				
				TerceroRCVehiculo trc = (TerceroRCVehiculo) estimacion;
				
				// # OBTENER NOMBRE TALLER
				if( trc.getTaller() != null ){
					PrestadorServicio taller = this.obtenerPrestadorServicio( trc.getTaller().getId() );
					if( taller != null ){
						nombreTaller = taller.getNombrePersona();
						tallerId     = String.valueOf( trc.getTaller().getId() );
					}
				}else{
					nombreTaller = "";
					tallerId     = "";
				}
				
				dataVehiculo.put("nombreTaller"    , nombreTaller );
				dataVehiculo.put("nombreInteresado", trc.getNombreAfectado() );
				dataVehiculo.put("claveTaller"     , tallerId );
				
				reporteVehiculo.setMarca ( trc.getMarca() );
				reporteVehiculo.setModelo( trc.getModeloVehiculo().toString() );
				reporteVehiculo.setSerie ( trc.getNumeroSerie() );
				reporteVehiculo.setPlacas( trc.getPlacas() );
				reporteVehiculo.setTipo  ( trc.getEstiloVehiculo() );
				
				
			}else if (EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, estimacion.getTipoEstimacion()) ){
				
				TerceroDanosMateriales tdm = (TerceroDanosMateriales) estimacion;
				
				if( tdm != null ){
					
					dataVehiculo.put("nombreTaller"    , tdm.getTaller().getNombrePersona() );
					dataVehiculo.put("nombreInteresado", tdm.getNombreAfectado() );
					dataVehiculo.put("claveTaller"     , tdm.getTaller().getId() );
					
				}
				
				// # BUSCAR MARCA
				reporteVehiculo.setMarca ( this.buscaMarca( reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getMarcaId() ) );
				reporteVehiculo.setModelo( reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getModeloVehiculo().toString() );
				reporteVehiculo.setSerie ( reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie() );
				reporteVehiculo.setPlacas( reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getPlaca() );
				reporteVehiculo.setTipo  ( reporteCabina.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getDescripcionFinal() );
			}
			
			dataVehiculo.put("reporteVehiculo" , reporteVehiculo );
		
		}catch(Exception e){
			LOG.error("Error en obtenerDatosVehiculo: ",e);
		}
		
		return dataVehiculo;
	}
	
	private String buscaMarca(BigDecimal keyMarca){
		
		MarcaVehiculoDTO marcaVehiculo = this.entidadService.findById(MarcaVehiculoDTO.class, keyMarca);
		
		if( marcaVehiculo != null ){
			return marcaVehiculo.getDescripcionMarcaVehiculo();
		}else{
			return "";
		}
		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	private void salvaHistoricoEnvio(Long keyReporteCabina, Long keyEstimacionCoberturaReporteCabina, HgsEnvioLog.TipoLogHGS tipoLog , String resultado, String datosEnvio, String reporte, boolean bandera){
		
		try{
			
			HgsEnvioLog hgsEnvio = null;
			if( this.entornoAplicacion() == 0 ){
				hgsEnvio = this.llenarLog(keyReporteCabina,keyEstimacionCoberturaReporteCabina,tipoLog,resultado,datosEnvio,reporte);
			}else{
				if ( bandera ){
					hgsEnvio = this.llenarLog(keyReporteCabina,keyEstimacionCoberturaReporteCabina,tipoLog,resultado,datosEnvio,reporte);
				}
			}
			
			if ( hgsEnvio!=null ) this.entidadService.save(hgsEnvio);

		}catch(Exception e){
			LOG.error("Error en salvaHistoricoEnvio: ",e);
		}
	}
	
	private HgsEnvioLog llenarLog(Long keyReporteCabina, Long keyEstimacionCoberturaReporteCabina, HgsEnvioLog.TipoLogHGS tipoLog , String resultado, String datosEnvio, String reporte ){
		
		HgsEnvioLog hgsEnvio = new HgsEnvioLog(
				keyReporteCabina,
				keyEstimacionCoberturaReporteCabina,
				tipoLog.toString(),
				new Date(),
				resultado,
				datosEnvio,
				reporte
			); 
		
		return hgsEnvio;
	}
	
	private int entornoAplicacion(){
		
		try{
			
			ParametroGlobal parametroGlobal = this.parametroGlobalService.obtenerPorIdParametro( ParametroGlobalService.AP_MIDAS , ParametroGlobalService.ENTORNO_APLICACION);
			
			if( parametroGlobal!=null ){
				return Integer.parseInt( parametroGlobal.getValor() );
			}else{
				return 0;
			}

		}catch(Exception e){
			LOG.error("Error en la busqueda de parámero general: "+e);
			// # EN CASO DE ERROR RETORNA SIEMPRE DESARROLLO
			return 0;
		}
		
	}
	
	
	// #########################################################################################
	// #####################
	// #####################     SUMA ASEGURADA HGS   ******************************************
	// #####################
	// #########################################################################################
	
	public ConsultaHgsDto obtenerSumaAseguradaHGSValuacion(Long keyReporteCabina,EstimacionCoberturaReporteCabina estimacionCoberturaReporteCabina) {
		
		ConsultaHgsDto consultaHgsDto = new ConsultaHgsDto();
		
		try{
			if ( estimacionCoberturaReporteCabina!=null ){
				
				    estimacionCoberturaReporteCabina = this.entidadService.findByProperty(EstimacionCoberturaReporteCabina.class,"id",estimacionCoberturaReporteCabina.getId() ).get(0);
					
					consultaHgsDto.setAplicaDeducible    ( String.valueOf( isDeducible( estimacionCoberturaReporteCabina.getId() ) ) );
					consultaHgsDto.setIdReporteSin       ( keyReporteCabina );
					consultaHgsDto.setNumFolio           ( estimacionCoberturaReporteCabina.getFolio() );
					consultaHgsDto.setSumaAsegurada      ( this.validarSumaAseguradaHGS( keyReporteCabina,estimacionCoberturaReporteCabina ) );
					consultaHgsDto.setPorcentajeDeducible( estimacionCoberturaReporteCabina.getCoberturaReporteCabina().getPorcentajeDeducible() );

			
			}
		}catch(Exception e){
			LOG.error("HGS - ErroR en obtenerSumaAseguradaHGS(Long keyReporteCabina,EstimacionCoberturaReporteCabina estimacionCoberturaReporteCabina): ",e);
			this.respuestaMap.put(keyReporteCabina.toString(), "HGS - 01 | Error al obtener Suma Asegurada {1} ");
			//this.error = "HGS - 01 | Error al obtener Suma Asegurada {1} ";
			
		}
		
		return consultaHgsDto;
	}
	
	@Override
	public ConsultaHgsDto obtenerSumaAseguradaHGS(Long keyReporteCabina) {
		
		this.creaMapDeRespuesta(this.respuestaMap, keyReporteCabina.toString());
		
		ConsultaHgsDto consultaHgsDto = new ConsultaHgsDto();
		List<EstimacionCoberturaReporteCabina> lEstimacionCoberturaReporteCabina = null;
		
		// # LOG
		this.salvaHistoricoEnvio(keyReporteCabina, null,HgsEnvioLog.TipoLogHGS.CONSULTA_SUMA_ASEGURADA,"Solicitud de Suma Asegurada","Reporte Cabina:"+keyReporteCabina.toString(),"",true);
		
		if( keyReporteCabina != null ){
			
			try{
				lEstimacionCoberturaReporteCabina = this.obtenerListEstRepCabinaByReporteCabinaId(keyReporteCabina);
				
				if( !lEstimacionCoberturaReporteCabina.isEmpty() ){
					
					for( EstimacionCoberturaReporteCabina lEstimacion : lEstimacionCoberturaReporteCabina ){
						
						if (EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, lEstimacion.getTipoEstimacion())) {
					
							consultaHgsDto.setAplicaDeducible    ( String.valueOf( isDeducible( lEstimacion.getId() ) ) );
							consultaHgsDto.setIdReporteSin       ( keyReporteCabina );
							consultaHgsDto.setNumFolio           ( lEstimacion.getId().toString() );
							consultaHgsDto.setSumaAsegurada      ( this.validarSumaAseguradaHGS(keyReporteCabina,lEstimacion ) );
							consultaHgsDto.setPorcentajeDeducible( lEstimacion.getCoberturaReporteCabina().getPorcentajeDeducible() );
							
							// # LOG
							this.salvaHistoricoEnvio(keyReporteCabina, null,HgsEnvioLog.TipoLogHGS.CONSULTA_SUMA_ASEGURADA,"Suma Asegurada Respuesta",consultaHgsDto.toString(),"",true);
							
							break;
						}
					}
						
				}else{
					// # LOG
					this.salvaHistoricoEnvio(keyReporteCabina, null,HgsEnvioLog.TipoLogHGS.CONSULTA_SUMA_ASEGURADA,"Suma Asegurada SIN datos","Reporte Cabina: "+keyReporteCabina.toString(),"",true);
				}
			}catch(Exception e){
				LOG.error("HGS - Error en obtenerSumaAseguradaHGS: ",e);
				//this.error = "HGS - 01 | Error al obtener Suma Asegurada";
				this.respuestaMap.put(keyReporteCabina.toString(), "HGS - 01 | Error al obtener Suma Asegurada");
			}
		
		}
		
		return consultaHgsDto;
	}
	
	
	public Double validarSumaAseguradaHGS( Long keyReporteCabina, EstimacionCoberturaReporteCabina estimacionCoberturaReporteCabina){
		
		Double sumaAsegurada = 0.0;
		
		try{
			
			if( estimacionCoberturaReporteCabina != null  ){
				
				if( EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, estimacionCoberturaReporteCabina.getTipoEstimacion())) {
					sumaAsegurada = estimacionCoberturaReporteCabina.getCoberturaReporteCabina().getValorSumaAsegurada();
				}else{
					sumaAsegurada = Double.valueOf( estimacionCoberturaReporteCabina.getSumaAseguradaObtenida().toString() );
				}
			
			}
		
		}catch(Exception e){
			LOG.error("HGS - Error en validarSumaAseguradaHgs: ",e);
			this.respuestaMap.put(keyReporteCabina.toString(), "HGS - 02 | Error al Validar Suma Asegurada");
			//this.error = "HGS - 02 | Error al Validar Suma Asegurada";
		}
		
		return sumaAsegurada;
	}	
	
	
	private boolean isDeducible(Long keyEstimacionCobertura){
		
		TerceroDanosMateriales terceros = this.entidadService.findById(TerceroDanosMateriales.class, keyEstimacionCobertura);
		
		if( terceros != null ){
			return ( terceros.getAplicaDeducible() == null ? false: terceros.getAplicaDeducible() );
		}else{
			return false;
		}
		
	}
	
	
	private void creaMapDeRespuesta(Map<String,String> map,String key){
		String mensaje = map.get(key);
		if(mensaje!=null){
			map.remove(key);
		}
		map.put(key, RESPUESTA_OK);
	}
	
	
	
	// #########################################################################################
	// #####################
	// #####################     PROCESAR REPORTE ENVIADO POR HGS     **************************
	// #####################     1.- Se recibe el objeto y se guarda en una tabla todos los datos como historico (ValuacionHgsDetalle)
	// #####################     2.- Si la valuacion tiene lista de refacciones de igual forma este se guarda en un histórico  (ValuacionValeRefaccion)
    // #####################     3.- El método persistirDatoRespuestaHgs se encarga de guardar en la tabla final, si el dato que viene no existe SE CREA, si ya existe SE ACTUALIZA EN BASE AL ESTATUS DE LA VALUACION
	// #########################################################################################
	
	@Override
	public String insertarValuacion(Efile efile) {
		LOG.info("HGS  Peticion -->"+efile.toString());
		// # LOG
		this.salvaHistoricoEnvio( efile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"Insercion Valuacion",efile.toString(),efile.getNumFolio(),true );
		
		
		creaMapDeRespuesta(this.respuestaMap, efile.getNumFolio());
		String respuesta = RESPUESTA_OK;
		
		try{
			
			// # VALIDA DATOS COMO EL TIPO DE ESTATUS, CANTIDADES, PROCESO A INVOCAR
			Map<String,Object> validaciones = this.validaValuacion(efile);

			if ( (Boolean)validaciones.get("bandera") ){

				Long keyValuacion = null;

				// # DATOS GENERALES
				ValuacionHgsDetalle valuacionHgsDetalle = this.llenarObjetoValuacionDetalle(efile);

				keyValuacion = (Long) this.entidadService.saveAndGetId(valuacionHgsDetalle);

				if ( keyValuacion > 0 ){

					// # PIEZAS
					List<ValuacionValeRefaccion> lValuacionValeRefaccion = this.llenarObjetoValuacionRefaccion(efile,keyValuacion);
					if ( !lValuacionValeRefaccion.isEmpty() ){
						this.entidadService.saveAll(lValuacionValeRefaccion);
					}

					this.persistirDatoRespuestaHgs(valuacionHgsDetalle, (short) valuacionHgsDetalle.getEstatusExpediente(), lValuacionValeRefaccion);
				}
				
				// ASIGNA EL VALOR DE LAS ORDENES DE COMPRA A STRING DE ERROR QUE ES EL QUE SE RETORNA
				  
				  String ordenesAlMomento = this.respuestaMap.get(efile.getNumFolio()+this.LISTADO_VALES);
				  
				
				if( !StringUtil.isEmpty(ordenesAlMomento) ) {
					//this.error = "ordenes["+this.listaOrdenesVales+"]";
					respuesta = "ordenes["+ordenesAlMomento+"]";
//					this.respuestaMap.put(efile.getNumFolio(), "ordenes["+ordenesAlMomento+"]");
					this.respuestaMap.remove(efile.getNumFolio()+this.LISTADO_VALES);
				}
			}else{
				//this.error = "HGS - 03 | "+(String) validaciones.get("mensaje");
				this.respuestaMap.put(efile.getNumFolio(), "HGS - 03 | "+(String) validaciones.get("mensaje"));
				//this.notificarErrorValidacionValuacion(efile.getNumFolio(), (String) validaciones.get("mensaje") );
			}
		}catch(Exception e){
			LOG.error("HGS - Error en InsertarValuacion: "+e);
			//this.error = "HGS - 03 | Error al insertar Valuacion";
			this.respuestaMap.put(efile.getNumFolio(), "HGS - 03 | Error al insertar Valuacion");
			
			this.registrarBitacora(EVENTO.DERIVACION_HGS, 
								   "ERROR AL INSERTAR LA VALUACION EN MIDAS",
								   "ERROR:"+this.imprimirTrazaError(e) +" Efile: "+efile.toString(),
								   efile.getNumFolio());
		}
		
		this.salvaHistoricoEnvio( efile.getIDReporteSin(), null ,HgsEnvioLog.TipoLogHGS.VALUACION,"RESPUESTA DE LA VALUACION","MENSAJE: "+this.respuestaMap.get(efile.getNumFolio()),efile.getNumFolio(),true );
		if(respuesta.equals(RESPUESTA_OK)){
			respuesta = this.respuestaMap.get(efile.getNumFolio());
		}
		this.respuestaMap.remove(efile.getNumFolio());
		LOG.info("HGS  Respuesta de :"+efile.getNumFolio()+" -->"+efile.toString());
		return respuesta;
	}	
	
	
	private void persistirDatoRespuestaHgs( ValuacionHgsDetalle valuacionHgsDetalle, short clave, List<ValuacionValeRefaccion> lValuacionValeRefaccion ){
		
		// SE ENCARGA DE GUARDAR EN LA TABLA FINAL LA VALUACION DE HGS
		// 1. SE BUSCA SI LA VALUACION YA EXISTE EN ValuacionHgs
		// 2. SI NO EXISTE SE CREA LA VALUACION Y SE OBTIENE EL ID, SI EXISTE SE OBTIENE SOLO EL ID
		// 3. CON EL ID DE VALUACION YA SEA NUEVO O RECIENTE SE INVOCA A procesarValuacion QUE ES EL CUAL VALIDA EL ESTATUS DE LA MISMA,
		//    LOS DATOS QUE SI DEBEN ACTUALIZAR,SI SE GENERA ORDEN DE COMPRA Y ACTUALIZA ESTIMACION	
		
		
		try{
			
			Long keyValuacion;
			
			// # BUSCA SI LA VALUACION DE HGS YA EXISTE EN ENTIDAD ValuacionHgs
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("reporteCabina.id" , valuacionHgsDetalle.getReporteSinId());
			params.put("paseatencionId"   , valuacionHgsDetalle.getAseguradoTerceroId() );
			
			List<ValuacionHgs> lValuacionHgs = this.entidadService.findByProperties(ValuacionHgs.class, params);
			
			if ( lValuacionHgs.isEmpty() ){
				
				//OBTENER VALUACION
				
				ValuacionHgs tempValuacionHgs = new ValuacionHgs();
				
				tempValuacionHgs.setClaveCausaMov         ( clave);
				tempValuacionHgs.setClaveEstatusVal       ( Short.valueOf( valuacionHgsDetalle.getEstatusExpediente() ) );
				tempValuacionHgs.setCoberturaId			  ( valuacionHgsDetalle.getCoberturaId() );
				tempValuacionHgs.setCodigoUsuarioCreacion ( HgsService.USUARIO_HGS );
				tempValuacionHgs.setFechaCreacion         ( valuacionHgsDetalle.getFechaCreacion() );
				tempValuacionHgs.setFechaIngresoTaller    ( valuacionHgsDetalle.getFechaIngreso() );
				tempValuacionHgs.setFechaReingresoTaller  ( valuacionHgsDetalle.getFechaReingreso() );
				tempValuacionHgs.setFechaTerminacion      ( valuacionHgsDetalle.getFechaTermino() );
				tempValuacionHgs.setFechaUltimoSurtido    ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
				tempValuacionHgs.setFechaValuacion        ( valuacionHgsDetalle.getFechaValuacion() );
				tempValuacionHgs.setFolio                 ( valuacionHgsDetalle.getNumFolio() );
				tempValuacionHgs.setImpNanoObra           ( valuacionHgsDetalle.getMontoManoObra() );
				tempValuacionHgs.setImpTotalVal           ( valuacionHgsDetalle.getMontoValuacion() );
				tempValuacionHgs.setImRefacciones         ( valuacionHgsDetalle.getMontoRefacciones() );
				tempValuacionHgs.setIncisoReporteCabina   ( this.entidadService.findById(IncisoReporteCabina.class, valuacionHgsDetalle.getIncisoId() ) );
				tempValuacionHgs.setPaseatencionId        ( valuacionHgsDetalle.getAseguradoTerceroId() );
				tempValuacionHgs.setReporteCabina         ( this.obtenerReporteCabina(valuacionHgsDetalle.getReporteSinId()) );
				tempValuacionHgs.setLineaNegocioId        ( valuacionHgsDetalle.getLineaNegocioId() );
				tempValuacionHgs.setTipoMovimiento        ( "alta" );
				tempValuacionHgs.setValuadorId            ( valuacionHgsDetalle.getClaveValuador() );
				tempValuacionHgs.setValuadorNombre        ( valuacionHgsDetalle.getNombreValuador() );
				tempValuacionHgs.setValuacionHgs          ( this.obtenerValuacionSiniestro(valuacionHgsDetalle) );
				tempValuacionHgs.setImpIvaValuacion       ( valuacionHgsDetalle.getMontoIva() );
				tempValuacionHgs.setImpSubTotalValuacion  ( valuacionHgsDetalle.getMontoSubtotal() );
				
				
				keyValuacion = (Long) this.entidadService.saveAndGetId(tempValuacionHgs);
				
			}else{
				keyValuacion = lValuacionHgs.get(0).getId();
			}
			
			// # PROCESAR DATOS DEL VALE DE REFACCION
			String respuesta = this.procesarValeRefaccion(keyValuacion, lValuacionValeRefaccion);
			if(respuesta!=null){
				this.respuestaMap.put(valuacionHgsDetalle.getNumFolio(), respuesta);
			}
			
			// # PROCESAR REGLAS
			this.procesarValuacion(keyValuacion,valuacionHgsDetalle, valuacionHgsDetalle.getEstatusExpediente() );
		
		}catch(Exception e){
			LOG.error("HGS - Error en PersistirDatoRespuestaHgs: ",e);
			this.respuestaMap.put(valuacionHgsDetalle.getNumFolio(), "HGS - 04 | Error al persistir datos de la Valuacion");
			//this.error = "HGS - 04 | Error al persistir datos de la Valuacion";
		}
		
	}
	
	/***
	 * Obtener la valuacion guardada en el LOG
	 * @param valuacionHgsDetalle
	 * @return
	 */
	private Long obtenerValuacionSiniestro(ValuacionHgsDetalle valuacionHgsDetalle){
		
		Long idValuacion = 0L;
		
		try{
			
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("estimacionReporteCabinaId" , valuacionHgsDetalle.getAseguradoTerceroId() );
			params.put("tipo"              , TipoLogHGS.ID_VALUACION.name() );
			
			List<HgsEnvioLog> logValuacion = this.entidadService.findByProperties(HgsEnvioLog.class, params);
			
			
			if( !logValuacion.isEmpty() ){
				for( HgsEnvioLog hel : logValuacion ){
					idValuacion = Long.valueOf( hel.getResultado().toString().trim() );
				}
			}
		
		}catch(Exception e){
			LOG.error("HGS Error al buscar valuacion ");
		}
		
		return idValuacion;
	}
	
	@Override
	public void procesarValuacion(Long keyValuacion,ValuacionHgsDetalle valuacionHgsDetalle, short tipoExpediente){
		
		// SIMULA SESION PARA QUE MÉTODOS FUERA DE HGS QUE SOLICITAN EL USUARIO CREACION NO TRUENEN
		this.generaSesionUsuarioHgs();
		
		try{
			
			ValuacionHgs valuacionHgs = this.entidadService.findById(ValuacionHgs.class, keyValuacion);			
		
			switch ( valuacionHgsDetalle.getTipoProceso() ){
				case REPARACION:

					switch( tipoExpediente ){
						
						case TIPO_VALUADO:
							
								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
								valuacionHgs.setImpNanoObra         ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones       ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpTotalVal         ( valuacionHgsDetalle.getMontoValuacion() );
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
							
								valuacionHgs.setFechaValuacion  ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setClaveTipoProceso((short)REPARACION);
								valuacionHgs.setClaveEstatusVal ((short)TIPO_VALUADO);
								this.entidadService.save        (valuacionHgs);

								// # RECUPERACIONES
								this.recuperacionDeducibleService.actualizarHGS(valuacionHgs);
								
							break;
						case TIPO_ASIGNADO_VALUADOR:
							
								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
								valuacionHgs.setImpNanoObra         ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones       ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpTotalVal         ( valuacionHgsDetalle.getMontoValuacion() );
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
							
								valuacionHgs.setValuadorId      ( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  ( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setClaveTipoProceso((short)REPARACION);
								valuacionHgs.setClaveEstatusVal ((short)TIPO_ASIGNADO_VALUADOR);
								this.entidadService.save        (valuacionHgs);
							break;
						case TIPO_VALUADO_COTIZADO:

								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
								valuacionHgs.setImpNanoObra         ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones       ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpTotalVal         ( valuacionHgsDetalle.getMontoValuacion() );
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
							
								valuacionHgs.setClaveTipoProceso((short)REPARACION);
								valuacionHgs.setClaveEstatusVal ((short)TIPO_VALUADO_COTIZADO);
								this.entidadService.save        (valuacionHgs);
							break;
						case TIPO_AUTORIZADO:
	
								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
							
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setImpNanoObra         ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones       ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpTotalVal         ( valuacionHgsDetalle.getMontoValuacion() );
								valuacionHgs.setClaveTipoProceso    ((short)REPARACION);
								valuacionHgs.setClaveEstatusVal     ((short)TIPO_AUTORIZADO);
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
								this.entidadService.save            (valuacionHgs);
								
								// # RECUPERACIONES
								this.recuperacionDeducibleService.actualizarHGS(valuacionHgs);
								
								// # AJUSTE DE RESERVA
								this.ajustarReserva(valuacionHgs);
								// # ORDEN PAGO
								this.crearOrdenDeCompraGeneral(valuacionHgs, MOVIMIENTO_ORDEN_PAGO.VALE , OrdenCompraService.ORIGENHGS_AUTOMATICO , TIPO_PAGO.PP, null, OrdenCompraService.ESTATUS_AUTORIZADA,"TIPO_AUTORIZADO" );
								
								
							break;
						case TIPO_COMPLEMENTO_AUTORIZADO: 
							
								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
							
								valuacionHgs.setImpNanoObra     ( valuacionHgsDetalle.getMontoManoObra() );	
								valuacionHgs.setImRefacciones   ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpTotalVal     ( valuacionHgsDetalle.getMontoValuacion() );
								valuacionHgs.setClaveTipoProceso((short)REPARACION);
								valuacionHgs.setClaveEstatusVal ((short)TIPO_COMPLEMENTO_AUTORIZADO);
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
								this.entidadService.save        (valuacionHgs);
								
								
								// # RECUPERACIONES
								this.recuperacionDeducibleService.actualizarHGS(valuacionHgs); 
								
								// # AJUSTE DE RESERVA
								this.ajustarReserva(valuacionHgs);
								// # ORDEN PAGO
								this.crearOrdenDeCompraGeneral(valuacionHgs, MOVIMIENTO_ORDEN_PAGO.VALE , OrdenCompraService.ORIGENHGS_AUTOMATICO , TIPO_PAGO.PP, null, OrdenCompraService.ESTATUS_AUTORIZADA,"TIPO_COMPLEMENTO_AUTORIZADO" );
								
							break;
						case TIPO_TERMINADO:

								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setImpNanoObra         ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones       ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpTotalVal         ( valuacionHgsDetalle.getMontoValuacion() );
								
								valuacionHgs.setFechaTerminacion   ( valuacionHgsDetalle.getFechaTermino() );	
								valuacionHgs.setFechaUltimoSurtido ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
								valuacionHgs.setClaveTipoProceso   ((short)REPARACION);
								valuacionHgs.setClaveEstatusVal    ((short)TIPO_TERMINADO);
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
								this.entidadService.save           (valuacionHgs);
								
								// # RECUPERACIONES
								this.recuperacionDeducibleService.actualizarHGS(valuacionHgs);
								
								// # AJUSTE DE RESERVA
								this.ajustarReserva(valuacionHgs);
								// # ORDEN PAGO
								this.crearOrdenDeCompraGeneral(valuacionHgs, MOVIMIENTO_ORDEN_PAGO.VALE, OrdenCompraService.ORIGENHGS_AUTOMATICO, TIPO_PAGO.PP, null, OrdenCompraService.ESTATUS_AUTORIZADA,"TIPO_TERMINADO" );
								
								
								
							break;
						case TIPO_ENTREGADO:
							
								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );

								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
								valuacionHgs.setImpNanoObra         ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones       ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpTotalVal         ( valuacionHgsDetalle.getMontoValuacion() );
								valuacionHgs.setClaveTipoProceso    ((short)REPARACION);
								valuacionHgs.setClaveEstatusVal     ((short)TIPO_ENTREGADO);
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
								this.entidadService.save            (valuacionHgs);
								
								// # RECUPERACIONES
								this.recuperacionDeducibleService.actualizarHGS(valuacionHgs);
								
								// # AJUSTE DE RESERVA
								this.ajustarReserva(valuacionHgs);
								// # ORDEN DE COMPRA
								this.crearOrdenDeCompraGeneral(valuacionHgs, MOVIMIENTO_ORDEN_PAGO.VALE, OrdenCompraService.ORIGENHGS_AUTOMATICO, TIPO_PAGO.PP, null, OrdenCompraService.ESTATUS_AUTORIZADA, "TIPO_ENTREGADO" );
								
							break;
					}
					// # LOG
					this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"Tipo proceso: Reparacion | ID_Efile: "+valuacionHgsDetalle.getTipoProceso()+" | Tipo Expediente: "+tipoExpediente,valuacionHgs.toString(),valuacionHgs.getFolio(),false );
					break;
					
					case PAGO_DANOS:
						
						if ( tipoExpediente == TIPO_ENTREGADO ){
							
							valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
							valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
							valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
							valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
							valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
							valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
							valuacionHgs.setClaveEstatusVal     ((short)TIPO_ENTREGADO);
							
							valuacionHgs.setFechaIngresoTaller(valuacionHgsDetalle.getFechaIngreso());
							valuacionHgs.setImpNanoObra         ( valuacionHgsDetalle.getMontoManoObra() );
							valuacionHgs.setImRefacciones       ( valuacionHgsDetalle.getMontoRefacciones() );
							valuacionHgs.setImpTotalVal         ( valuacionHgsDetalle.getMontoValuacion() );
							valuacionHgs.setClaveTipoProceso    ((short)PAGO_DANOS);
							valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
							valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
							this.entidadService.save          (valuacionHgs);
							
							// # AJUSTE DE RESERVA
							this.ajustarReservaPerdidaDanios(valuacionHgs);
							// # ORDEN PAGO
							this.crearOrdenDeCompraGeneral(valuacionHgs, MOVIMIENTO_ORDEN_PAGO.GENERAL, OrdenCompraService.ORIGENHGS_DAÑOS, TIPO_PAGO.PB, TIPO_CONCEPTO.DA, OrdenCompraService.ESTATUS_TRAMITE, TIPO_PROCESO_STR_DANIOS );
							this.recuperacionDeducibleService.actualizarHGS(valuacionHgs);
						}

						// # LOG
						this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"PAGO DAÑOS: "+valuacionHgsDetalle.getTipoProceso(),valuacionHgs.toString(),valuacionHgs.getFolio(),false );
						break;
					case PERDIDA_TOTAL:

							valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
							valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
							valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
							valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
							valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
							valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
							valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
							valuacionHgs.setImpNanoObra         ( valuacionHgsDetalle.getMontoManoObra() );
							valuacionHgs.setImRefacciones       ( valuacionHgsDetalle.getMontoRefacciones() );
							valuacionHgs.setClaveEstatusVal     ( valuacionHgsDetalle.getEstatusExpediente());
						
							valuacionHgs.setClaveTipoProceso((short)PERDIDA_TOTAL);
							valuacionHgs.setImpTotalVal         ( valuacionHgsDetalle.getMontoValuacion() );
							valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
							valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
							this.entidadService.save            ( valuacionHgs);
						
							/*
							// VALIDAR SI ESTA CONVERTIDO EN SINIESTRO, SI NO NOTIFICAR
							if( EnumUtil.equalsValue(valuacionHgs.getReporteCabina().getEstatus(), EstatusReporteCabina.TRAMITE_SINIESTRO, 
									EstatusReporteCabina.TERMINADO, EstatusReporteCabina.SINIESTRO_REAPERTURADO)) {							
								this.ajustarReserva(valuacionHgs);
								this.crearOrdenDeCompraGeneral(valuacionHgs, MOVIMIENTO_ORDEN_PAGO.GENERAL, OrdenCompraService.ORIGENHGS_PERDIDATOTAL, TIPO_PAGO.PB, TIPO_CONCEPTO.PT, OrdenCompraService.ESTATUS_INDEMNIZACION, TIPO_PROCESO_STR_PTOTAL );
							}else{
								this.notificarConvertirSiniestro(valuacionHgs.getReporteCabina(),valuacionHgs.getFolio());
								// # LOG
								this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"PERDIDA TOTAL: "+valuacionHgsDetalle.getTipoProceso(),"Notificar convertir a Siniestro",valuacionHgs.getFolio(),false );
							}
							*/
							// # LOG
							this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"PERDIDA TOTAL: "+valuacionHgsDetalle.getTipoProceso(),valuacionHgs.toString(),valuacionHgs.getFolio(),false );
							
						break;
					case DANOS_MENOR_DEDUCIBLE:
						 // # DAÑO MENOR AL DEDUCIBLE  *YA NO EXISTE, SOLO SE SALVA EN LOG POR REGISTRO
						
								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
								valuacionHgs.setClaveEstatusVal     ( valuacionHgsDetalle.getEstatusExpediente() );
						
								valuacionHgs.setClaveTipoProceso( (short)DANOS_MENOR_DEDUCIBLE);
								valuacionHgs.setImpTotalVal     ( BigDecimal.ZERO );
								valuacionHgs.setImpNanoObra     ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones   ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
								this.entidadService.save        ( valuacionHgs);
								// # LOG
								this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"DANOS MENOR DEDUCIBLE: "+valuacionHgsDetalle.getTipoProceso(),valuacionHgs.toString(),valuacionHgs.getFolio(),false );
						break;
					case NO_PROCEDE: 
						
								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
								valuacionHgs.setClaveEstatusVal     ( valuacionHgsDetalle.getEstatusExpediente() );
						
								valuacionHgs.setClaveTipoProceso( (short)NO_PROCEDE);
								valuacionHgs.setImpTotalVal     ( BigDecimal.ZERO );
								valuacionHgs.setImpNanoObra     ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones   ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
								this.entidadService.save        ( valuacionHgs);
								
								// # LOG
								this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"NO PROCEDE: "+valuacionHgsDetalle.getTipoProceso(),valuacionHgs.toString(),valuacionHgs.getFolio(),false );
						break;
					case RECHAZADO: 
						
								valuacionHgs.setValuadorId      	( valuacionHgsDetalle.getClaveValuador()  ); 
								valuacionHgs.setValuadorNombre  	( valuacionHgsDetalle.getNombreValuador() );
								valuacionHgs.setFechaValuacion      ( valuacionHgsDetalle.getFechaValuacion() );
								valuacionHgs.setFechaIngresoTaller  ( valuacionHgsDetalle.getFechaIngreso() );
								valuacionHgs.setFechaReingresoTaller( valuacionHgsDetalle.getFechaReingreso() );
								valuacionHgs.setFechaTerminacion    ( valuacionHgsDetalle.getFechaTermino() );
								valuacionHgs.setFechaUltimoSurtido  ( valuacionHgsDetalle.getFechaRefaccionesEntregadas() );
								valuacionHgs.setClaveEstatusVal     ( valuacionHgsDetalle.getEstatusExpediente() );
						
								valuacionHgs.setClaveTipoProceso( (short)RECHAZADO);
								valuacionHgs.setImpTotalVal     ( BigDecimal.ZERO );
								valuacionHgs.setImpNanoObra     ( valuacionHgsDetalle.getMontoManoObra() );
								valuacionHgs.setImRefacciones   ( valuacionHgsDetalle.getMontoRefacciones() );
								valuacionHgs.setImpIvaValuacion     ( valuacionHgsDetalle.getMontoIva() );
								valuacionHgs.setImpSubTotalValuacion( valuacionHgsDetalle.getMontoSubtotal() );
								this.entidadService.save        ( valuacionHgs);
								
								// # LOG
								this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"RECHAZADO: "+valuacionHgsDetalle.getTipoProceso(),valuacionHgs.toString(),valuacionHgs.getFolio(),false );
						break;
					default:
						// # LOG
						this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"Tipo de proceso NO encontrado: "+valuacionHgsDetalle.getTipoProceso(),valuacionHgs.toString(),valuacionHgs.getFolio(),false );
					break;
			}
		
		}catch(Exception e){
			LOG.error("HGS - Error en procesarValuacion: ",e);
			this.respuestaMap.put(valuacionHgsDetalle.getNumFolio(), "HGS - 05 | Error al procesar la valuacion");
			//this.error = "HGS - 05 | Error al procesar la valuacion";
		}
	
	}
	
	
	private String procesarValeRefaccion(Long keyValuacion,List<ValuacionValeRefaccion> lValuacionValeRefaccion){
		String respuesta = null;
		try{
			if( !lValuacionValeRefaccion.isEmpty() ){
				for( ValuacionValeRefaccion lVal : lValuacionValeRefaccion ){
					
					// # BUSCAR SI VALE YA FUE AGREGADO
					ValuacionValeRefaccionFinal vfinalUpd = this.buscarValeRefaccion(keyValuacion,lVal);
					
					if( vfinalUpd != null ){
						// # ACTUALIZAR DATOS
						vfinalUpd.setClaveProveedor     ( lVal.getClaveProveedor() );
						vfinalUpd.setConcepto           ( lVal.getConcepto() );
						vfinalUpd.setEstatusDetalle     ( lVal.getEstatusDetalle() );
						vfinalUpd.setFechaActualizacion ( new Date());
						vfinalUpd.setFechaExpedicion    ( lVal.getFechaExpedicion() );
						vfinalUpd.setMonto              ( lVal.getMonto() );
						vfinalUpd.setMontoIva           ( lVal.getMontoIva() );
						vfinalUpd.setNombreProveedor    ( lVal.getNombreProveedor() );
						vfinalUpd.setPorcentajeIva      ( lVal.getPorcentajeIva() );
						vfinalUpd.setRefaccion          ( lVal.getRefaccion() );
						vfinalUpd.setFechaModificacion  ( new Date() );
						
						this.entidadService.save(vfinalUpd);
						
					}else{
						// # CREAR DATO
						ValuacionValeRefaccionFinal vfinal = new ValuacionValeRefaccionFinal();
						vfinal.setClaveProveedor     ( lVal.getClaveProveedor() );
						vfinal.setConcepto           ( lVal.getConcepto() );
						vfinal.setEstatusDetalle     ( lVal.getEstatusDetalle() );
						vfinal.setFechaCreacion      ( new Date() );
						vfinal.setFechaExpedicion    ( lVal.getFechaExpedicion() );
						vfinal.setMonto              ( lVal.getMonto() );
						vfinal.setMontoIva           ( lVal.getMontoIva() );
						vfinal.setNombreProveedor    ( lVal.getNombreProveedor() );
						vfinal.setPorcentajeIva      ( lVal.getPorcentajeIva() );
						vfinal.setRefaccion          ( lVal.getRefaccion() );
						vfinal.setValeId			 ( lVal.getValeId() );
						vfinal.setFolio              ( lVal.getFolio() );
						vfinal.setValuacionHgs       ( this.entidadService.findById(ValuacionHgs.class, keyValuacion) );
						
						this.entidadService.save(vfinal);
					}
					
				}
			}
		}catch(Exception e){
			LOG.error("HGS - Error en procesarValeRefaccion: "+e);
			respuesta = "HGS - 06 | Error al procesar el vale de refaccion";
		}
		return respuesta;
	}
	
	
	private ValuacionValeRefaccionFinal buscarValeRefaccion(Long keyValuacion,ValuacionValeRefaccion valuacion){
		
		ValuacionValeRefaccionFinal valuacionValeFinal = null;		
			
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("folio"           , valuacion.getFolio()  );
		params.put("valeId"          , valuacion.getValeId() );
		params.put("valuacionHgs.id" , keyValuacion);
		
		//Falta ordenar para obtener la ultima
		List<ValuacionValeRefaccionFinal> lValuacionValeRefaccion = this.entidadService.findByProperties(ValuacionValeRefaccionFinal.class,params );
		
		if (CollectionUtils.isNotEmpty(lValuacionValeRefaccion)) {
			valuacionValeFinal = lValuacionValeRefaccion.get(0);
		}
		
		return valuacionValeFinal;
	}
	
	
	
	private void crearOrdenDeCompraGeneral(ValuacionHgs valuacionHgs, MOVIMIENTO_ORDEN_PAGO mop , String tipoOrdenCompra, TIPO_PAGO tp, TIPO_CONCEPTO tc, String estatusOrdenPago, String tipoProceso  ){
		
		Long keyOrdenCompra = null;
		PrestadorServicio prestadorServicio;
		String nombreBeneficiario = "";
		String nomBeneficiarioHGS = "";
		
		// # BUSCAR ULTIMO REGISTRO INSERTADO EN LOG DE ESTA VALUACION PARA COMPARAR LAS CANTIDADES, SI NO ES IGUAL CREA LA ORDEN DE COMPRA
		
		
				try{
				
					Map<String,Object> general = new HashMap<String,Object>();
					Map<String,Object> vales   = new HashMap<String,Object>();
					
					EstimacionCoberturaReporteCabina estimacionCoberturaReporteCabina = estimacionService.obtenerEstimacionCoberturaReporteCabina(valuacionHgs.getPaseatencionId());
					
					// # REFRESCAR ENTIDAD
					this.entidadService.refresh(valuacionHgs);
					
					if( mop.getValue() ==  MOVIMIENTO_ORDEN_PAGO.GENERAL.getValue() ){
		
						// # OBTENER CONCEPTO AJUSTE
						ConceptoAjuste conceptoAjuste = this.obtenerCoberturaConcepto(valuacionHgs.getFolio(), estimacionCoberturaReporteCabina.getCoberturaReporteCabina().getId() ,
								estimacionCoberturaReporteCabina.getCveSubCalculo(), tc );
						vales.put("importe"       , valuacionHgs.getImpSubTotalValuacion() ); 
						vales.put("costoUnitario" , valuacionHgs.getImpSubTotalValuacion() );
						vales.put("importePagado" , 0.0);
						// SI EL PROCESO ES PAGO DE DAÑOS O PERDIDA TOTAL EL IVA SE PONE COMO 0
						if(tipoProceso.equals(TIPO_PROCESO_STR_DANIOS) ||  tipoProceso.equals(TIPO_PROCESO_STR_PTOTAL)     ){
							vales.put("iva"  , 0);
						}else{
							vales.put("iva" ,  valuacionHgs.getImpIvaValuacion()  );
						}
						vales.put("porcentajeIva" , 0.0);
						if( conceptoAjuste != null && validarImporteConcepto(vales,conceptoAjuste,valuacionHgs)){
							// # VALIDAR QUE LA VALUACION NO TENGA ORDEN DE COMPRA
							if( valuacionHgs.getOrdenCompraId() == null ){
								prestadorServicio = this.obtenerPrestadorServicio(valuacionHgs.getFolio(), new Long(valuacionHgs.getValuadorId()));
								if( prestadorServicio != null ){
									// OBTENER NOMBRE BENEFICIARIO
									nombreBeneficiario = this.obtenerNombreBeneficiario(valuacionHgs, tipoProceso);
		
									general.put("origen"        , tipoOrdenCompra );
									general.put("reporteCabina" , valuacionHgs.getReporteCabina() );
									general.put("tipoPago"      , tp );
									general.put("estatus"       , estatusOrdenPago );
									// #OBTENER DATOS BENEFICIARIO
									Object[] aBeneficiario = obtenerDatosBeneficiario(tp, estimacionCoberturaReporteCabina, null, nombreBeneficiario );
									general.put("beneficiarioId"    , aBeneficiario[0] );
									general.put("nombreBeneficiario", aBeneficiario[1] );
									general.put("tipoProceso"       , tipoProceso );
									general.put("tipoProveedor"     , "TALL");
									
									nomBeneficiarioHGS = (String)aBeneficiario[1];
									
									OrdenCompra ordenCompraExistente = null;
									
									if(tipoOrdenCompra.equals(OrdenCompraService.ORIGENHGS_DAÑOS)){
										//No debe validar monto
										ordenCompraExistente = this.ordenCompraDao.obtenerOrdenDeCompraExistente(valuacionHgs.getPaseatencionId(),valuacionHgs.getImpSubTotalValuacion(),tp.name(), String.valueOf(aBeneficiario[1]),(aBeneficiario[0]==null?null:Long.valueOf(aBeneficiario[0].toString())),Boolean.FALSE);
										
									}else{
										//Valida el monto
										ordenCompraExistente = this.ordenCompraDao.obtenerOrdenDeCompraExistente(valuacionHgs.getPaseatencionId(),valuacionHgs.getImpSubTotalValuacion(),tp.name(), String.valueOf(aBeneficiario[1]),(aBeneficiario[0]==null?null:Long.valueOf(aBeneficiario[0].toString())),Boolean.TRUE);
									}
									
																						   
									//SI NO EXISTE LA ORDEN DE COMPRA SE PROCEDE A CREAR UNA NUEVA
									if(  ordenCompraExistente == null ){
										keyOrdenCompra = this.procesarOrdenCompra(valuacionHgs, general);
										
										if( keyOrdenCompra.compareTo(new Long(0)) == 1 ) {
											
											// # VINCULAR LA ORDEN A LA VALUACION
											valuacionHgs.setOrdenCompraId(keyOrdenCompra);
											this.entidadService.save(valuacionHgs);
					
											/*
											vales.put("importe"       , valuacionHgs.getImpSubTotalValuacion() ); 
											vales.put("costoUnitario" , valuacionHgs.getImpSubTotalValuacion() );
											vales.put("importePagado" , 0.0);
											// SI EL PROCESO ES PAGO DE DAÑOS EL IVA SE PONE COMO 0
											vales.put("iva"           , ( tipoProceso.equals(TIPO_PROCESO_STR_DANIOS) ? 0 : valuacionHgs.getImpIvaValuacion() ) );
											vales.put("porcentajeIva" , 0.0);
											*/
											vales.put("observaciones" , "HGS folio: "+valuacionHgs.getFolio() );
											vales.put("conceptoAjuste", conceptoAjuste  );
											vales.put("estatus"       , estatusOrdenPago);
											DetalleOrdenCompra detalleOrdenCompra =this.procesarDetalleOrdenCompra(valuacionHgs.getReporteCabina().getId(),valuacionHgs.getFolio(),keyOrdenCompra, vales);
											List<DetalleOrdenCompra> lDetalleOrdenCompra = new ArrayList<DetalleOrdenCompra>();
											lDetalleOrdenCompra.add(detalleOrdenCompra);					
											// # GENERAR INDENMIZACION PARA PERDIDA TOTAL Y DAÑOS MATERIALES
											OrdenCompra ordenCompra = this.entidadService.findById(OrdenCompra.class, keyOrdenCompra);
											ordenCompra.setDetalleOrdenCompras(lDetalleOrdenCompra);
											this.perdidaTotalService.generarIndemnizacion( ordenCompra );
											
											//VALIDA RESERVA SI ES NEGATIVA
											BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(estimacionCoberturaReporteCabina.getId(), Boolean.TRUE);
											if(reserva.compareTo(BigDecimal.ZERO)<=0){
												MovimientoCoberturaSiniestro movimiento = movimientoSiniestroService.generarAjusteReserva( 
																									estimacionCoberturaReporteCabina.getId(),  
																									BigDecimal.ZERO, 
																									CausaMovimiento.HGS, 
																									HgsService.USUARIO_HGS
																								);
												this.actualizaProvisionEnPase(estimacionCoberturaReporteCabina.getId());

												// # LOG
												if(movimiento != null){
													this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Ajuste de Reserva generada debido a reserva insuficiente para crear la Orden de Compra:", movimiento.getId().toString()+" | Proceso: "+"Proceso: "+valuacionHgs.getClaveTipoProceso(), valuacionHgs.getFolio(),true );
												}

											}
										}else {
											LOG.error("NO SE PUEDO CREAR ORDEN DE COMPRA PARA LA VALUACION: "+valuacionHgs.getValuacionHgs());
										}
									}else if( ordenCompraExistente != null  && tipoOrdenCompra.equals(OrdenCompraService.ORIGENHGS_DAÑOS)  ){
										if(ordenCompraExistente.getEstatus().equals(OrdenCompraService.ESTATUS_TRAMITE) ){
											ordenCompraExistente.getDetalleOrdenCompras().get(0).setCostoUnitario(valuacionHgs.getImpSubTotalValuacion());
											ordenCompraExistente.getDetalleOrdenCompras().get(0).setImporte(valuacionHgs.getImpSubTotalValuacion());
											ordenCompraExistente.setNomBeneficiario(nomBeneficiarioHGS);
											this.entidadService.save(ordenCompraExistente.getDetalleOrdenCompras().get(0));
											this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Actualiza Orden Compra Danios","Cobertura: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
										}else{
											this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"No Actualiza Orden Compra Danios debido a que existe una","Cobertura: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
										}
									}
									else{
										this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"NO CREA ORDEN DE COMPRA","YA EXISTE LA ORDEN DE COMPRA: "+ordenCompraExistente.getId(),valuacionHgs.getFolio(),true );
									}
								
								}else{
									this.notificarPrestadorNoValido(valuacionHgs.getFolio(), valuacionHgs.getId().toString() , valuacionHgs.getValuadorNombre());
								}
							}
						}else{
							valuacionHgs.getReporteCabina().setError("No existe concepto ajuste válido para el reporte cabina: "+valuacionHgs.getReporteCabina().getOficina().getClaveOficina()+" - "+valuacionHgs.getReporteCabina().getConsecutivoReporte()+" - "+valuacionHgs.getReporteCabina().getAnioReporte()+" en el tipo de estimacion: "+estimacionCoberturaReporteCabina.getTipoEstimacion()+" | HGS KEY{ "+valuacionHgs.getKey()+" } ");
							this.notificaConceptoAjuste(valuacionHgs.getReporteCabina() );
							// # LOG
							this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Sin Concepto-Ajuste","Cobertura: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
						}
		
					}else{
		
						if( !valuacionHgs.getValuacionRefaccionFinal().isEmpty() ){
		
							for( ValuacionValeRefaccionFinal lvr : valuacionHgs.getValuacionRefaccionFinal() ){
		
								ConceptoAjuste conceptoAjuste = null;
		
								if( lvr.getRefaccion() ){
									conceptoAjuste = this.obtenerCoberturaConcepto(valuacionHgs.getFolio(),estimacionCoberturaReporteCabina.getCoberturaReporteCabina().getId() 
											, estimacionCoberturaReporteCabina.getCveSubCalculo() , TIPO_CONCEPTO.REF );
								}else{
									conceptoAjuste = this.obtenerCoberturaConcepto(valuacionHgs.getFolio(),estimacionCoberturaReporteCabina.getCoberturaReporteCabina().getId() , 
											estimacionCoberturaReporteCabina.getCveSubCalculo(), TIPO_CONCEPTO.MO );
								}
								vales.put("importe"       , lvr.getMonto() + lvr.getMontoIva() );
								vales.put("importePagado" , 0 );
								vales.put("costoUnitario" , (lvr.getMonto()) );
								vales.put("iva"           , lvr.getMontoIva() );
								vales.put("porcentajeIva" , lvr.getPorcentajeIva() );
								if( conceptoAjuste != null && validarImporteConcepto(vales,conceptoAjuste,valuacionHgs)){
									// # VALIDAR VALE DE REFACCION VALIDO
										//if( lvr.getOrdenCompraId() == null){
											
											prestadorServicio = this.obtenerPrestadorServicio(valuacionHgs.getFolio(), lvr.getClaveProveedor()); 
											
											if( prestadorServicio != null ){
												if ( Integer.parseInt( lvr.getEstatusDetalle() )== HgsServiceImpl.VALE_REFACCION_AUTORIZADO ){
		
														Object[] aBeneficiario = null;
														
														general.put("origen"       , tipoOrdenCompra);
														general.put("reporteCabina", valuacionHgs.getReporteCabina() );
														general.put("tipoPago"     , tp );
														general.put("estatus"      , estatusOrdenPago );
														general.put("tipoProceso"  , tipoProceso );
														// #OBTENER DATOS BENEFICIARIO
														aBeneficiario = obtenerDatosBeneficiario(tp, estimacionCoberturaReporteCabina, new Long( prestadorServicio.getId() ), null );
														
														general.put("beneficiarioId"    , aBeneficiario[0] );
														general.put("nombreBeneficiario", aBeneficiario[1] );
														general.put("tipoProveedor"     , (lvr.getConcepto().equals(CODIGO_MANO_OBRA_VALE)) ? "TALL" : "REFAC");
														
														OrdenCompra ordenCompraExistente = this.ordenCompraDao.obtenerOrdenDeCompraExistente(valuacionHgs.getPaseatencionId() , BigDecimal.valueOf(lvr.getMonto()), tp.name(), null,new Long( prestadorServicio.getId() ),Boolean.TRUE);
														
														if ( ordenCompraExistente == null ){
															
															keyOrdenCompra = this.procesarOrdenCompra(valuacionHgs, general);
															
															if( keyOrdenCompra.compareTo(new Long(0)) == 1 ) {
															
																// # VINCULAR LA ORDEN A LA VALUACION
																lvr.setOrdenCompraId(keyOrdenCompra);
																this.entidadService.save(lvr);
																vales.put("observaciones" , "HGS folio: "+lvr.getFolio()+" Nombre Proveedor: "+lvr.getNombreProveedor() );
																vales.put("conceptoAjuste", conceptoAjuste  );
																vales.put("estatus"       , estatusOrdenPago);
																this.procesarDetalleOrdenCompra(valuacionHgs.getReporteCabina().getId(),valuacionHgs.getFolio(), keyOrdenCompra, vales);
					
																general.clear();
																vales.clear();
																
																// GUARDA ORDENES DE COMPRA
																//this.listaOrdenesVales += this.listaOrdenVale(keyOrdenCompra, lvr.getValeId());
																
																if(keyOrdenCompra!=null){
																	String ordenesAlMomento = (this.respuestaMap.get(valuacionHgs.getFolio()+this.LISTADO_VALES)==null)?"":this.respuestaMap.get(valuacionHgs.getFolio()+this.LISTADO_VALES); 
																	this.respuestaMap.put(valuacionHgs.getFolio()+this.LISTADO_VALES,ordenesAlMomento+this.listaOrdenVale(keyOrdenCompra, lvr.getValeId()));
																}
															}else {
																// TODO: SALVAR EN BITACORA
															}
														}
												}else{
													//SE CANCELA LA ORDEN DE COMPRA
													OrdenCompra ordenCompraExistente = this.ordenCompraDao.obtenerOrdenDeCompraExistente(valuacionHgs.getPaseatencionId() , BigDecimal.valueOf(lvr.getMonto()), tp.name(), null,new Long( prestadorServicio.getId() ), Boolean.TRUE);
													if ( ordenCompraExistente != null && ordenCompraExistente.getEstatus().equals(OrdenCompraService.ESTATUS_AUTORIZADA)){
														ordenCompraExistente.setEstatus(OrdenCompraService.ESTATUS_CANCELADA);
														ordenCompraExistente.setCodigoUsuarioModificacion(HgsService.USUARIO_HGS);
														ordenCompraExistente.setFechaModificacion(new Date());
														this.entidadService.save(ordenCompraExistente);
														
													}
												}
		
											}else{
												// NOTIFICA SIN VALUADOR
												this.notificarPrestadorNoValido(valuacionHgs.getFolio(), lvr.getClaveProveedor().toString(), lvr.getNombreProveedor() );
												// # SALVAR LOG
												this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"SIN PRESTADOR DE SERVICIO","PRESTADOR: "+lvr.getClaveProveedor(),valuacionHgs.getFolio(),true );
											}
		
										//}
								}else{
									valuacionHgs.getReporteCabina().setError("No existe concepto ajuste válido para el reporte cabina: "+valuacionHgs.getReporteCabina().getOficina().getClaveOficina()+" - "+valuacionHgs.getReporteCabina().getConsecutivoReporte()+" - "+valuacionHgs.getReporteCabina().getAnioReporte()+" en el tipo de estimacion: "+estimacionCoberturaReporteCabina.getTipoEstimacion()+" | HGS KEY{ "+valuacionHgs.getKey()+" } HGS VALE{ "+lvr.getKey()+" } " );
									//this.notificaConceptoAjuste(valuacionHgs.getReporteCabina());
									// # LOG
									this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Sin Concepto-Ajuste","Cobertura {key Entity CoberturaReporteCabina}: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
								}
		
							}
							//VALIDA RESERVA SI ES NEGATIVA
							BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(estimacionCoberturaReporteCabina.getId(), Boolean.TRUE);
							if(reserva.compareTo(BigDecimal.ZERO)<=0){
								MovimientoCoberturaSiniestro movimiento = movimientoSiniestroService.generarAjusteReserva( 
																					estimacionCoberturaReporteCabina.getId(),  
																					BigDecimal.ZERO, 
																					CausaMovimiento.HGS, 
																					HgsService.USUARIO_HGS
																				);
								this.actualizaProvisionEnPase(estimacionCoberturaReporteCabina.getId());

								// # LOG
								if(movimiento != null){
									this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Ajuste de Reserva generada debido a reserva insuficiente para crear la Orden de Compra:", movimiento.getId().toString()+" | Proceso: "+"Proceso: "+valuacionHgs.getClaveTipoProceso(), valuacionHgs.getFolio(),true );
								}

							}

		
						}
		
					}
					
				//}
				
				}catch(Exception e){
					LOG.error("HGS - Error al generar crearOrdenDeCompraGeneral: "+this.imprimirTrazaError(e));
					String ordenesAlMomento = this.respuestaMap.get(valuacionHgs.getFolio()+this.LISTADO_VALES);
					this.registrarBitacora(EVENTO.GENERAR_ORDEN_COMPRA_HGS, 
							   "ERROR AL GENERAR ORDEN DE COMPRA EN MIDAS",
							   "ERROR:"+this.imprimirTrazaError(e) +" Ordenes: "+ordenesAlMomento,
							   valuacionHgs.getId().toString());
					this.respuestaMap.put(valuacionHgs.getFolio(), "HGS - 07 | Error al generar orden de compra");
					//this.error = "HGS - 07 | Error al generar orden de compra";
					//this.listaOrdenesVales = "";
					this.respuestaMap.remove(valuacionHgs.getFolio()+this.LISTADO_VALES);
				}
		
	}
	
	private void actualizaProvisionEnPase(Long estimacionId){
		try{
			//Genera el movimiento de provision
			this.siniestroPrivisionService.generaMovimientoDeAjusteReserva(estimacionId);
			this.siniestroPrivisionService.actualizaProvision(estimacionId, MovimientoProvisionSiniestros.Origen.PASE);
		}catch(Exception ex){
			log.error("Error al generar el movimiento de provision", ex);
		}
	}

	private String listaOrdenVale( long ordenCompra, long vale){
		
		return vale+"="+ordenCompra+",";
	}
	
	
	private PrestadorServicio obtenerPrestadorServicio(long keyPrestador){
		return obtenerPrestadorServicio("",keyPrestador);
	}
	
	private PrestadorServicio obtenerPrestadorServicio(String folio, long keyPrestador){
		
		PrestadorServicio prestadorServicio = null;
		
		try{
			
			prestadorServicio = this.entidadService.findById(PrestadorServicio.class, Integer.parseInt( String.valueOf( keyPrestador ) ) );
			
		}catch(Exception e){
			LOG.error("HGS - Error al obtener prestador de servicio en orden de compra",e);
			this.respuestaMap.put(folio, "HGS - 11 | Error al obtener prestador de servicio en orden de compra");
			//this.error = "HGS - 11 | Error al obtener prestador de servicio en orden de compra";
		}
		
		return prestadorServicio;
	}
	
	private ConceptoAjuste obtenerCoberturaConcepto(String folio, Long keyCoberturaCabina, String cveSubTipoCalculo, TIPO_CONCEPTO tc){
		
		Long keyConceptoAjuste 	      = null;
		ConceptoAjuste conceptoAjuste = null;
		
		try{
			
			Map<Long, String> mListaCoberturas = this.listadoService.listarConceptosPorCobertura(keyCoberturaCabina, cveSubTipoCalculo, ConceptoAjusteService.AFECTACION_RESERVA , tc.toString(),Boolean.TRUE  );
			if( !mListaCoberturas.isEmpty() ){
				
				Iterator<Long> it = mListaCoberturas.keySet().iterator();
				while(it.hasNext()){
					keyConceptoAjuste = it.next();
					break;
				}
				conceptoAjuste = this.entidadService.findById(ConceptoAjuste.class, keyConceptoAjuste);
			}
		
		}catch(Exception e){
			LOG.error("HGS Error al obtener cobertura concepto",e);
			//this.error = "HGS - 08 | Error al obtener cobertura concepto";
			this.respuestaMap.put(folio,"HGS - 08 | Error al obtener cobertura concepto" );
		}
		return conceptoAjuste;
	}
	
	private Long procesarOrdenCompra(ValuacionHgs valuacionHgs,Map<String,Object> parametros) {
		
		try{
			
			Long ordenCompraId = new Long(0);
			
			OrdenCompra ordenCompra = new OrdenCompra();
			
			EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, valuacionHgs.getPaseatencionId());
	
			ordenCompra.setCoberturaReporteCabina     ( estimacion.getCoberturaReporteCabina() );
			ordenCompra.setCodigoUsuarioCreacion      ( HgsService.USUARIO_HGS);
			ordenCompra.setCodigoUsuarioModificacion  ( HgsService.USUARIO_HGS);
			ordenCompra.setCveSubTipoCalculoCobertura ( !StringUtil.isEmpty(estimacion.getCveSubCalculo()) ? estimacion.getCveSubCalculo() : "0");
			
			ordenCompra.setFechaCreacion              ( new Date());
			ordenCompra.setFechaModificacion          ( new Date());
			ordenCompra.setTipoProveedor              ( parametros.get("tipoProveedor").toString() );
			ordenCompra.setIdBeneficiario             ( ( parametros.get("beneficiarioId")!=null ? new Integer( parametros.get("beneficiarioId").toString() ): null) );
			// # SI LA COBERTURA ES RCV SE CONCATENA CON EL KEYCOBERTURA, SI ES DM SE CONCATENA CON 0
			ordenCompra.setIdCoberturaCompuesta       (  estimacion.getCoberturaReporteCabina().getId() +"|"+(!StringUtil.isEmpty(estimacion.getCveSubCalculo()) ? estimacion.getCveSubCalculo() : "0") );
			ordenCompra.setIdTercero                  ( valuacionHgs.getPaseatencionId() );
			ordenCompra.setNomBeneficiario            ( ( parametros.get("nombreBeneficiario")!=null ? (String) parametros.get("nombreBeneficiario").toString() : null) );
			ordenCompra.setNumeroValuacion            ( valuacionHgs.getId());
			ordenCompra.setOrigen                     ( parametros.get("origen").toString() );
			ordenCompra.setReporteCabina              ( (ReporteCabina) parametros.get("reporteCabina") );
			ordenCompra.setTipo                       ( OrdenCompraService.TIPO_AFECTACION_RESERVA); 
			ordenCompra.setTipoPago                   ( parametros.get("tipoPago").toString() );
			ordenCompra.setTipoPersona                ( PERSONA_FISICA );
			ordenCompra.setAplicaDeducible            ( parametros.get("tipoPago").toString().equals(OrdenCompraService.PAGO_A_BENEFICIARIO) ? estimacion.getAplicaDeducible() : false  );
			
			// VALIDAR SI NO EXISTE ORDEN DE COMPRA MANUAL
			
			if( !this.validarSiExisteOrdenCompraManual(ordenCompra) ) {
				
				// SE SETEA EL PARAMETRO EN ESTE LUGAR YA QUE EL MÉTODO DE ordenCompraService.validarCrearOrdenCompra
				// RECHAZA EN AUTOMÁTICO OC CON EL ESTATUS AUTORIZA Y TODAS LAS OC DE HGS NACEN AUTORIZADAS.
				ordenCompra.setEstatus( parametros.get("estatus").toString() );
				
				ordenCompraId = (Long) this.entidadService.saveAndGetId(ordenCompra);
			
				// # LOG
				this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"GENERAR ORDEN DE COMPRA","ORDEN COMPRA: "+ordenCompraId,valuacionHgs.getFolio(),true );
				
				// # RECUPERACION DE DEDUCIBLE
				if( !parametros.get("tipoProceso").equals("PERDIDA_TOTAL") ){
					
					this.generaSesionUsuarioHgs();
					
					this.recuperacionDeducibleService.actualizarPendienteRecuperar(valuacionHgs.getReporteCabina().getId(), 
							valuacionHgs.getPaseatencionId());
					//Generacion de ingreso automatico
					List<RecuperacionDeducible> deducibles = recuperacionDeducibleService.obtenerRecuperacionDeduciblePorEstimacion(valuacionHgs.getPaseatencionId());
					
					for(RecuperacionDeducible deducible : CollectionUtils.emptyIfNull(deducibles)){
						recuperacionDeducibleService.generarIngreso(deducible);
					}
				}
			
			}

			return ordenCompraId;
			
		}catch(Exception e){
			LOG.error("Error al generar Orden de Compra ");
			return new Long(0);
		}
	}
	private boolean validarImporteConcepto(Map<String,Object> parametros ,ConceptoAjuste conceptoAjuste, ValuacionHgs valuacionHgs ){
		BigDecimal variacion= BigDecimal.ONE;
		String value = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , PARAMETRO_TOTAL_HGS);
		if (!StringUtils.isEmpty(value)  && StringUtils.isNumeric(value)){
			variacion=new BigDecimal (value);
		}		
		BigDecimal  importe= ( BigDecimal.valueOf( Double.valueOf( parametros.get("importe").toString() ) ) );
		BigDecimal  costoUnitario= ( BigDecimal.valueOf( Double.valueOf( parametros.get("costoUnitario").toString() ) ) );
		BigDecimal  iva= ( BigDecimal.valueOf( Double.valueOf( parametros.get("iva").toString() ) ) );
		BigDecimal  porcentajeIva= ( BigDecimal.valueOf( Double.valueOf( parametros.get("porcentajeIva").toString() ) ) );
		BigDecimal  calculo=BigDecimal.ZERO;
		if(conceptoAjuste.getAplicaIsr() || conceptoAjuste.getAplicaIvaRetenido()){
			this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Concepto-Ajuste aplica ISR, IVA Retenido","Cobertura: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
			return Boolean.FALSE;
		}
		if( 	(porcentajeIva!=null && porcentajeIva.compareTo(BigDecimal.ZERO)!=0 )	  &&  !conceptoAjuste.getAplicaIva()  ){
			this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Concepto-Ajuste no aplica IVA","Cobertura: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
			return Boolean.FALSE;
		}
		if( 	(iva!=null && iva.compareTo(BigDecimal.ZERO)!=0 )	  &&  !conceptoAjuste.getAplicaIva()  ){
			this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Concepto-Ajuste no aplica IVA","Cobertura: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
			return Boolean.FALSE;
		}		
		if( 	(porcentajeIva!=null && porcentajeIva.compareTo(BigDecimal.ZERO)!=0 )	  &&  conceptoAjuste.getAplicaIva()  ){
			if(porcentajeIva.compareTo(conceptoAjuste.getPorcIva())!=0 ){
				this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Concepto-Ajuste % IVA no coincide","Cobertura: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
				return Boolean.FALSE;
			}
		}		
		if(conceptoAjuste.getPorcIva()!=null && conceptoAjuste.getPorcIva().compareTo(BigDecimal.ZERO)!=0){
			calculo=conceptoAjuste.getPorcIva().divide(new BigDecimal(100));
			calculo=calculo.add(BigDecimal.ONE);
			calculo=calculo.multiply(costoUnitario);
			BigDecimal diferencia = importe.subtract( calculo).abs();
			if (diferencia.compareTo(variacion) ==1){
				this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Concepto-Ajuste monto no coincide","Cobertura: "+valuacionHgs.getCoberturaId()+" | Linea Negocio: "+valuacionHgs.getLineaNegocioId() ,valuacionHgs.getFolio(),true );
				return Boolean.FALSE;
			}			
		}
		return Boolean.TRUE;
	}
	private DetalleOrdenCompra procesarDetalleOrdenCompra(Long keyReporteCabina,String folio, Long keyOrdenCompra, Map<String,Object> parametros ){

		DetalleOrdenCompra detalleOrdenCompra = new DetalleOrdenCompra();
		try{
			ConceptoAjuste conceptoAjuste =(ConceptoAjuste) parametros.get("conceptoAjuste");
			detalleOrdenCompra.setCodigoUsuarioCreacion( HgsService.USUARIO_HGS);
			detalleOrdenCompra.setCostoUnitario        ( BigDecimal.valueOf(Double.valueOf( parametros.get("costoUnitario").toString() )) );
			detalleOrdenCompra.setConceptoAjuste       ( conceptoAjuste);
			detalleOrdenCompra.setEstatus              ( ( parametros.get("estatus").toString().equals(OrdenCompraService.ESTATUS_INDEMNIZACION) ) ? OrdenCompraService.ESTATUS_TRAMITE : parametros.get("estatus").toString() );
			detalleOrdenCompra.setFechaCreacion        ( new Date() );
			detalleOrdenCompra.setImporte              ( BigDecimal.valueOf( Double.valueOf( parametros.get("importe").toString() ) ) );
			detalleOrdenCompra.setImportePagado        ( BigDecimal.valueOf( Double.valueOf( parametros.get("importePagado").toString() ) ) );
			detalleOrdenCompra.setIva                  ( BigDecimal.valueOf( Double.valueOf( parametros.get("iva").toString() ) ) );
			detalleOrdenCompra.setObservaciones        ( parametros.get("observaciones").toString() );
			detalleOrdenCompra.setPorcIva              (conceptoAjuste.getPorcIva());
			detalleOrdenCompra.setOrdenCompra          ( this.entidadService.findById(OrdenCompra.class, keyOrdenCompra));
			detalleOrdenCompra.setIvaRetenido		   (BigDecimal.ZERO);
			detalleOrdenCompra.setPorcIvaRetenido      (BigDecimal.ZERO);
			detalleOrdenCompra.setPorcIsr              (BigDecimal.ZERO);
			//detalleOrdenCompra.setPorcIva              (BigDecimal.ZERO);
			
			// # LOG
			this.salvaHistoricoEnvio( keyReporteCabina, null ,HgsEnvioLog.TipoLogHGS.VALUACION,"Generar Detalle Orden de Compra | Orden de compra: "+keyOrdenCompra,detalleOrdenCompra.toString(),"",true );
			
			this.entidadService.save(detalleOrdenCompra);
			
		}catch(Exception e){
			LOG.error("HGS - Error al procesar detalle orden compra: "+e);
			this.respuestaMap.put(folio, "HGS - 09 | Error al procesar detalle de orden de compra");
			//this.error = "HGS - 09 | Error al procesar detalle de orden de compra";
		}
		
		return detalleOrdenCompra;
	}
	
	private void notificaConceptoAjuste(ReporteCabina reporte ){
		try{
			envioNotificacionesService.notificacionSinConceptoAjuste(reporte);
		}catch(Exception e){
			LOG.error("HGS - Error notificacion notificaConceptoAjuste :",e);
		}
	}
	
	private Object[] obtenerDatosBeneficiario(TIPO_PAGO tp, EstimacionCoberturaReporteCabina estimacionCoberturaReporteCabina, Long keyBeneficiario, String beneficiarioDanios ){
		
		// # POSICIONES: [0] = BENEFICIARIO_ID | [1] = BENEFICIARIO_NOMBRE
		Object[] aBeneficiario = new Object[2];
		
		try{

			if ( tp.name().equals("PB") ){
				
				if( beneficiarioDanios != null ){
					aBeneficiario[0] = null;
					aBeneficiario[1] = beneficiarioDanios;					
				}else{
					aBeneficiario[0] = null;
					aBeneficiario[1] = estimacionCoberturaReporteCabina.getNombreAfectado();
				}
			}else{
				aBeneficiario[0] = keyBeneficiario;
				aBeneficiario[1] = null; 
			}
		
		}catch(Exception e){
			LOG.error("Error obtenerDatosBeneficiario: ",e);
		}
		
		return aBeneficiario;
	}
	
	
	private void ajustarReserva(ValuacionHgs valuacionHgs){
		
		
		// SIMULA SESION PARA QUE MÉTODOS FUERA DE HGS QUE SOLICITAN EL USUARIO CREACION NO TRUENEN
		//this.generaSesionUsuarioHgs();
		
		//TipoMovimiento tipoMovimiento = null;
		BigDecimal diferencia = null;
		BigDecimal estimacionFinal = null;
		// # !!! IMPORTANTE - SI ES PERDIDA TOTAL O PAGO DE DAÑOS SE TOMA EL TOTAL DE LA VALUACION
		//BigDecimal totalValuacion = ( valuacionHgs.getClaveTipoProceso() == HgsServiceImpl.PERDIDA_TOTAL || valuacionHgs.getClaveTipoProceso() == HgsServiceImpl.PAGO_DANOS ?  valuacionHgs.getImpSubTotalValuacion() : valuacionHgs.getImpTotalVal() );
		BigDecimal totalValuacion =  valuacionHgs.getImpSubTotalValuacion() ;
		
		try{
			
			// # OBTENER ESTIMACION COBERTURA
			EstimacionCoberturaReporteCabina estimacionCoberturaReporteCabina = this.entidadService.findById(EstimacionCoberturaReporteCabina.class,valuacionHgs.getPaseatencionId() );
			
			if( estimacionCoberturaReporteCabina != null ){
				
				// # BUSCAR ULTIMO REGISTRO INSERTADO EN LOG DE ESTA VALUACION PARA COMPARAR LAS CANTIDADES, SI NO ES IGUAL ACTUALIZA LA RESERVA
				//Realmente el metodo se debe llamar .... tiene Montos Iguales ???
				if (!this.isCantidadDiferente(valuacionHgs)){				
					
					BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(estimacionCoberturaReporteCabina.getId(), Boolean.TRUE);
					estimacionFinal = this.movimientoSiniestroService.obtenerImporteMovimientos(valuacionHgs.getReporteCabina().getId(), null, estimacionCoberturaReporteCabina.getId(), null, null, TipoDocumentoMovimiento.ESTIMACION, null, null, null);
						
					// # LOG
					this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Consulta de estimacion final: ", estimacionFinal+" | "+valuacionHgs.toString(),valuacionHgs.getFolio(),false );
					
					// # El Sistema valida si el Importe Total de Valuacion es igual a la Reserva.
					if( totalValuacion.compareTo(estimacionFinal) != 0  ){
		
						// # OBTENER LA DIFERENCIA ENTRE LA ESTIMACION FINAL Y EL MONTO TOTAL ENVIADO POR HGS, 
						diferencia = totalValuacion.subtract(estimacionFinal);	
						
						BigDecimal montoMovimiento = reserva.add(diferencia);
						
						// # LOG
						this.salvaHistoricoEnvio( 
										valuacionHgs.getReporteCabina().getId(), 
										valuacionHgs.getPaseatencionId() ,
										HgsEnvioLog.TipoLogHGS.VALUACION,
										"Ajuste de Reserva generada ","Proceso: "+valuacionHgs.getClaveTipoProceso()+" | Sub Total Valuacion: "+totalValuacion+" - estimacion final: "+estimacionFinal+" = Diferencia : "+diferencia+" - Movimiento: "+montoMovimiento+" | "+valuacionHgs.toString(),
										valuacionHgs.getFolio(),true );
						
						//Podria desaparecer esta parte
						//******************************************************************************************************************************************************************
						MovimientoCoberturaSiniestro movimiento 
							= movimientoSiniestroService.generarAjusteReserva( 
															estimacionCoberturaReporteCabina.getId(),  
															montoMovimiento, 
															CausaMovimiento.HGS, 
															HgsService.USUARIO_HGS
														);
						this.actualizaProvisionEnPase(estimacionCoberturaReporteCabina.getId());
						//******************************************************************************************************************************************************************

						// # LOG
						if(movimiento != null){
							this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Llave Ajuste de Reserva generada:", movimiento.getId().toString()+" | Proceso: "+"Proceso: "+valuacionHgs.getClaveTipoProceso(), valuacionHgs.getFolio(),true );
						}
					}
				
				}
			
			}
			
		}catch(Exception e){
			LOG.error("HGS - Error en el ajuste de reserva: ",e);
			this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Proceso: "+valuacionHgs.getClaveTipoProceso()+" | Error al ajustar Reserva:",this.imprimirTrazaError(e),valuacionHgs.getFolio(),true );
			this.respuestaMap.put(valuacionHgs.getFolio(), "HGS - 10 | Error al generar ajuste de reserva" );
		}
		
		
	}
	
	
	private void ajustarReservaPerdidaDanios(ValuacionHgs valuacionHgs){
		
		
		// SIMULA SESION PARA QUE MÉTODOS FUERA DE HGS QUE SOLICITAN EL USUARIO CREACION NO TRUENEN
		//this.generaSesionUsuarioHgs();
		
		//TipoMovimiento tipoMovimiento = null;
		BigDecimal diferencia = null;
		BigDecimal estimacionFinal = null;
		// # !!! IMPORTANTE - SI ES PERDIDA TOTAL O PAGO DE DAÑOS SE TOMA EL TOTAL DE LA VALUACION
		//BigDecimal totalValuacion = ( valuacionHgs.getClaveTipoProceso() == HgsServiceImpl.PERDIDA_TOTAL || valuacionHgs.getClaveTipoProceso() == HgsServiceImpl.PAGO_DANOS ?  valuacionHgs.getImpSubTotalValuacion() : valuacionHgs.getImpTotalVal() );
		BigDecimal totalValuacion =  valuacionHgs.getImpSubTotalValuacion() ;
		
		
		try{
			
			// # OBTENER ESTIMACION COBERTURA
			EstimacionCoberturaReporteCabina estimacionCoberturaReporteCabina = this.entidadService.findById(EstimacionCoberturaReporteCabina.class,valuacionHgs.getPaseatencionId() );
			
			if( estimacionCoberturaReporteCabina != null ){
				
				// # BUSCAR ULTIMO REGISTRO INSERTADO EN LOG DE ESTA VALUACION PARA COMPARAR LAS CANTIDADES, SI NO ES IGUAL ACTUALIZA LA RESERVA
				//Realmente el metodo se debe llamar .... tiene Montos Iguales ???
				if (!this.isCantidadDiferente(valuacionHgs)){				
					
					BigDecimal reserva = movimientoSiniestroService.obtenerReservaAfectacion(estimacionCoberturaReporteCabina.getId(), Boolean.TRUE);
					estimacionFinal = this.movimientoSiniestroService.obtenerImporteMovimientos(valuacionHgs.getReporteCabina().getId(), null, estimacionCoberturaReporteCabina.getId(), null, null, TipoDocumentoMovimiento.ESTIMACION, null, null, null);
					//
						
					// # LOG
					this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Consulta de estimacion final: ", estimacionFinal+" | "+valuacionHgs.toString(),valuacionHgs.getFolio(),false );
					
					// # El Sistema valida si el Importe Total de Valuacion es igual a la Reserva.
					if( totalValuacion.compareTo(estimacionFinal) != 0  ){
		
						// # OBTENER LA DIFERENCIA ENTRE LA ESTIMACION FINAL Y EL MONTO TOTAL ENVIADO POR HGS, 
						diferencia = totalValuacion.subtract(estimacionFinal);	
						
						OrdenCompra ordenCompraExistente = this.ordenCompraDao.obtenerOrdenDeCompraExistente(valuacionHgs.getPaseatencionId(),null,null, null,null,Boolean.FALSE);
						
						BigDecimal montoMovimiento = BigDecimal.ZERO;
						
						//Si no existe orden de compra 
						if ( ordenCompraExistente != null && 
									( ordenCompraExistente.equals(OrdenCompraService.ESTATUS_AUTORIZADA) 
										|| ordenCompraExistente.equals(OrdenCompraService.ESTATUS_ASOCIADA)
										|| ordenCompraExistente.equals(OrdenCompraService.ESTATUS_PAGADA)
									) ){
							montoMovimiento = diferencia;
						}else{
							montoMovimiento = reserva.add(diferencia);
						}
						// # LOG
						this.salvaHistoricoEnvio( 
										valuacionHgs.getReporteCabina().getId(), 
										valuacionHgs.getPaseatencionId() ,
										HgsEnvioLog.TipoLogHGS.VALUACION,
										"Ajuste de Reserva generada ","Proceso: "+valuacionHgs.getClaveTipoProceso()+" | Sub Total Valuacion: "+totalValuacion+" - estimacion final: "+estimacionFinal+" = Diferencia : "+diferencia+" - Movimiento: "+montoMovimiento+" | "+valuacionHgs.toString(),
										valuacionHgs.getFolio(),true );
						
						MovimientoCoberturaSiniestro movimiento 
							= movimientoSiniestroService.generarAjusteReserva( 
															estimacionCoberturaReporteCabina.getId(),  
															montoMovimiento, 
															CausaMovimiento.HGS, 
															HgsService.USUARIO_HGS
														);

						// # LOG
						if(movimiento != null){
							this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Llave Ajuste de Reserva generada:", movimiento.getId().toString()+" | Proceso: "+"Proceso: "+valuacionHgs.getClaveTipoProceso(), valuacionHgs.getFolio(),true );
						}
					}
				
				}
			
			}
			
		}catch(Exception e){
			LOG.error("HGS - Error en el ajuste de reserva: ",e);
			this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Proceso: "+valuacionHgs.getClaveTipoProceso()+" | Error al ajustar Reserva:",this.imprimirTrazaError(e),valuacionHgs.getFolio(),true );
			this.respuestaMap.put(valuacionHgs.getFolio(), "HGS - 10 | Error al generar ajuste de reserva" );
		}
		
		
	}
	
	
	/**
	 * Compara la cantidad del total de la valuacion vs la última recibida
	 * false - cantidades iguales | true - cantidades diferentes
	 * @return
	 */
	private boolean isCantidadDiferente(ValuacionHgs valuacionHgs){
		
		boolean esIgual = false;
		
		try{
			
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("aseguradoTerceroId", valuacionHgs.getPaseatencionId() );
			params.put("numFolio"          , valuacionHgs.getFolio() );
			
			List<ValuacionHgsDetalle> lValuacionHgsPrevia = this.entidadService.findByProperties(ValuacionHgsDetalle.class, params);
			
			
			if (CollectionUtils.isNotEmpty(lValuacionHgsPrevia) && lValuacionHgsPrevia.size() > 1 ) {
				
				// ORDENAR LISTA DE VALUACION PREVIA
				Collections.sort(lValuacionHgsPrevia,
						new Comparator<ValuacionHgsDetalle>() {				
							public int compare(ValuacionHgsDetalle l1, ValuacionHgsDetalle l2){
								return l2.getId().compareTo(l1.getId());
							}
				});
				
				
				// # POSICION 1 YA QUE SIEMPRE SERA LA ANTERIOR AL ULTIMO REGISTRO
				if ( valuacionHgs.getImpTotalVal().compareTo(lValuacionHgsPrevia.get( 1 ).getMontoValuacion()) == 0 ){
					esIgual = true;
				}
				
				try{
					this.salvaHistoricoEnvio( valuacionHgs.getReporteCabina().getId(), valuacionHgs.getPaseatencionId() ,HgsEnvioLog.TipoLogHGS.VALUACION,"Comparacion cantidades valuacion","Importe total: "+valuacionHgs.getImpTotalVal()+" - Ultima Valuación: "+lValuacionHgsPrevia.get( 1 ).getMontoValuacion()+" .Resultado: "+(esIgual?"Es igual":"No es igual"),valuacionHgs.getFolio(),true );
				}catch(Exception ex){
					LOG.error("Error al isCantidadDiferente: fallo al obtener posicion ",ex);
				}
			}
			
		}catch(Exception e){
			LOG.error("Error al isCantidadDiferente: ",e);
		}
		return esIgual;
	}
	

	private ValuacionHgsDetalle llenarObjetoValuacionDetalle (Efile efile){
		
		ValuacionHgsDetalle valuacionHgsDetalle = new ValuacionHgsDetalle();
		
		try{
			
			valuacionHgsDetalle.setAseguradoTerceroId		 ( efile.getIDAseguradoTercero() );
			valuacionHgsDetalle.setLineaNegocioId            ( efile.getIDLineaNegocio() );
			valuacionHgsDetalle.setCategoriaId		 		 ( efile.getIDCategoria());
			valuacionHgsDetalle.setClaveValuador	 		 ( efile.getClaveValuador() );
			valuacionHgsDetalle.setCoberturaId               ( efile.getIDCobertura() );
			valuacionHgsDetalle.setEstatusExpediente 		 ( Short.valueOf( efile.getEstatusExpediente() ));
			valuacionHgsDetalle.setFechaRefaccionesEntregadas( efile.getFechaRefaccionesEntregadas() );
			valuacionHgsDetalle.setFechaIngreso				 ( efile.getFechaIngreso() );
			valuacionHgsDetalle.setFechaReingreso			 ( efile.getFechaReingreso() );
			valuacionHgsDetalle.setFechaTermino		         ( efile.getFechaTermino() );
			valuacionHgsDetalle.setIncisoId         		 ( efile.getIDInciso()  );
			valuacionHgsDetalle.setLineaNegocioId			 ( efile.getIDLineaNegocio() );
			valuacionHgsDetalle.setMontoManoObra			 ( BigDecimal.valueOf( efile.getMontoManoObra() ) );
			valuacionHgsDetalle.setMontoRefacciones			 ( BigDecimal.valueOf( efile.getMontoRefacciones() ) );
			valuacionHgsDetalle.setMontoValuacion			 ( BigDecimal.valueOf( efile.getMontoTotalValuacion() ));
			valuacionHgsDetalle.setNombreValuador			 ( efile.getNombreValuador() );
			valuacionHgsDetalle.setReporteSinId				 ( efile.getIDReporteSin() );
			valuacionHgsDetalle.setTipoProceso			     ( Short.valueOf( efile.getTipoProceso() ) );
			valuacionHgsDetalle.setNumFolio                  ( efile.getNumFolio() );
			valuacionHgsDetalle.setFechaCreacion             ( new Date() );
			valuacionHgsDetalle.setFechaValuacion            ( efile.getFechaValuacion() );
			valuacionHgsDetalle.setMontoIva					 ( BigDecimal.valueOf( efile.getMontoIvaTotalValuacion() ));
			valuacionHgsDetalle.setMontoSubtotal             ( BigDecimal.valueOf( efile.getMontoSubTotalValuacion() ));
			
			// # LOG
			this.salvaHistoricoEnvio( efile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"Llenar Objeto ValucionHgsDetalle",valuacionHgsDetalle.toString()+" - "+efile.toString(), efile.getNumFolio(),true );
			
		}catch(Exception e){
			LOG.error("Error en llenarObjetoValuacionDetalle: ",e);
		}
		return valuacionHgsDetalle;
	}
	
	private List<ValuacionValeRefaccion> llenarObjetoValuacionRefaccion (Efile efile, Long keyValuacion ){
		
		List<ValuacionValeRefaccion> lValuacionValeRefaccion = new ArrayList<ValuacionValeRefaccion>();
		
		try{
			
			ValuacionHgsDetalle valuacionHgsDetalle = this.entidadService.findById(ValuacionHgsDetalle.class, keyValuacion);
			
			if ( efile.getValesRefacciones() != null &&  efile.getValesRefacciones().length > 0 ){
				
				for( int i=0; i <= efile.getValesRefacciones().length - 1; i++ ){
					
					if( efile.getValesRefacciones()[i]!=null ){
					
						ValuacionValeRefaccion valuacionValeRefaccion = new ValuacionValeRefaccion();
						
						valuacionValeRefaccion.setClaveProveedor     ( new Long ( efile.getValesRefacciones()[i].getClaveProveedor() ) );
						valuacionValeRefaccion.setConcepto           ( efile.getValesRefacciones()[i].getConcepto() );
						valuacionValeRefaccion.setEstatusDetalle     ( efile.getValesRefacciones()[i].getEstatusVale() );
						valuacionValeRefaccion.setFechaActualizacion (efile.getValesRefacciones()[i].getFechaActualizacion() );
						valuacionValeRefaccion.setFolio              ( efile.getValesRefacciones()[i].getFolio() );
						valuacionValeRefaccion.setMonto              ( efile.getValesRefacciones()[i].getMonto() );
						valuacionValeRefaccion.setMontoIva           ( efile.getValesRefacciones()[i].getMontoIva() );
						valuacionValeRefaccion.setNombreProveedor    ( efile.getValesRefacciones()[i].getNombreProveedor() );
						valuacionValeRefaccion.setPorcentajeIva      ( efile.getValesRefacciones()[i].getPorcentajeIva() );
						valuacionValeRefaccion.setRefaccion          ( efile.getValesRefacciones()[i].isRefaccion() );
						valuacionValeRefaccion.setValeId             ( efile.getValesRefacciones()[i].getIDVale() );
						valuacionValeRefaccion.setValuacionHgsDetalle(valuacionHgsDetalle);
						
						lValuacionValeRefaccion.add(valuacionValeRefaccion);
					
					}
				}
			}
			
		}catch(Exception e){
			LOG.error("Error en llenarObjetoValuacionRefaccion: ",e);
		}
		
		return lValuacionValeRefaccion;
	}
	

	private ReporteCabina obtenerReporteCabina (Long keyReporteCabina){
		
		Map<String,Object> params = new LinkedHashMap<String,Object>();
		params.put("incisoReporteCabina.seccionReporteCabina.reporteCabina.id", keyReporteCabina);
		
		return this.entidadService.findById(ReporteCabina.class, keyReporteCabina);
	}
	
	private List<EstimacionCoberturaReporteCabina> buscarCoberturas(Long keyReporteCabina){
		
		try{
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id", keyReporteCabina);
			
			return this.entidadService.findByProperties(EstimacionCoberturaReporteCabina.class, params);
		}catch(Exception e){
			LOG.error("Error al buscarCoberturas :",e);
			return null;
		}
		
	}
	
	private List<EstimacionCoberturaReporteCabina> obtenerListEstRepCabinaByReporteCabinaId( Long keyReporteCabina){
		
		try{
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("coberturaReporteCabina.incisoReporteCabina.seccionReporteCabina.reporteCabina.id" , keyReporteCabina);
		
			return this.entidadService.findByProperties(EstimacionCoberturaReporteCabina.class, params);
			
		}catch(Exception e){
			LOG.error("Error en obtenerListEstRepCabinaByReporteCabinaId: ",e);
			return null;
		}
	}	
	

	
	private Map<String,Object> validaValuacion(Efile eFile){
		
		StringBuilder mensajeValidacion = new StringBuilder();
		boolean bandera = true;
		Map<String,Object> validaciones = new HashMap<String,Object>();
		
		try{
			
			EstimacionCoberturaReporteCabina idTercero      =  estimacionService.obtenerEstimacionCoberturaReporteCabina(eFile.getIDAseguradoTercero());

			ConsultaHgsDto                   consultaHgsDto =  this.obtenerSumaAseguradaHGSValuacion(eFile.getIDReporteSin(),idTercero);
			
			// # R2 NUMERO TIPO PROCESO VALIDO
			if ( !this.isEstatusValido( eFile.getTipoProceso(), HgsServiceImpl.TIPO_PROCESO_ESTATUS_VALIDO ) ){
				bandera = false;
				mensajeValidacion.append( "  | Tipo "+eFile.getEstatusExpediente()+" de PROCESO No Válido. ");
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R2 TIPO_PROCESO Proceso NO Valido: "+mensajeValidacion+" "+HgsServiceImpl.TIPO_PROCESO_ESTATUS_VALIDO.toString(),eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R3 NUMERO DE ESTATUS VALUACION
			if ( !this.isEstatusValido( eFile.getEstatusExpediente(), HgsServiceImpl.ESTATUS_VALUACION ) ){
				bandera = false;
				mensajeValidacion.append("  | Tipo "+eFile.getEstatusExpediente()+" de ESTATUS EXPEDIENTE No Valido. ");
				// # mensaje +=this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R3 ESTATUS VALUACION Proceso NO Valido: "+mensaje+" "+HgsServiceImpl.estatusValuacion.toString() ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R4 ESTATUS VALES REFACCIONES VALIDO
			if( eFile.getValesRefacciones()!=null ){
				if( !this.isEstatusVehiculoValido( eFile.getValesRefacciones() ) ){
					bandera = false;
					mensajeValidacion.append("  | Tipo Estatus refaccion vehiculo o No Valido.");
					// # LOG
					this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R4 VALES REFACCIONES VALIDOS Proceso NO Valido: "+mensajeValidacion+" "+HgsServiceImpl.ESTATUS_VALUACION.toString() ,eFile.toString(),eFile.getNumFolio(),false );
				}
			}
			
			// # R5 REPORTE SINIESTRO NULLO O 0
			if( eFile.getIDReporteSin() == null || eFile.getIDReporteSin() == 0 ){
				bandera = false;
				mensajeValidacion.append(" | Id Reporte No Valido o No Fue Recibido.");
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R5 REPORTE SINIESTRO NULO: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R6 LINEA DE NEGOCIO NULLO O 0
			if( eFile.getIDLineaNegocio() == null || eFile.getIDLineaNegocio() == 0 ){
				bandera = false;
				mensajeValidacion.append( "  | Linea de negocio no valida o nula : ").append(eFile.getIDLineaNegocio());
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R6 LINEA DE NEGOCIO NULLO: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R7 INCISO NULLO O 0
			if( eFile.getIDInciso() == null || eFile.getIDInciso() == 0 ){
				bandera = false;
				mensajeValidacion.append( "  | Inciso no valido o nulo :"+eFile.getIDInciso());
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R7 INCISO NULLO: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R8 COBERTURA NULLO O 0
			if( eFile.getIDCobertura() == null || eFile.getIDCobertura() == 0 ){
				bandera = false;
				mensajeValidacion.append( "  | Id Cobertura no valida o recibida: "+eFile.getIDCobertura());
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R8 COBERTURA NULLO: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R9 CATEGORIA NULLO O 0
			if( eFile.getIDCategoria() == null || eFile.getIDCategoria() == 0 ){
				bandera = false;
				mensajeValidacion.append( "  | Id categoría no valida o nula :"+eFile.getIDCategoria());
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R9 CATEGORIA NULLO: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R10 PASE DE ATENCION NULLO O 0
			if( eFile.getIDAseguradoTercero() == null || eFile.getIDAseguradoTercero() == 0 ){
				bandera = false;
				mensajeValidacion.append( "  | Identificador del Afectado No Valido o No Fue Recibido : "+eFile.getIDAseguradoTercero());
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R10 PASE DE ATENCION NULLO: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R11 ID DE REPORTE
			if ( this.entidadService.findById(ReporteCabina.class, eFile.getIDReporteSin() ) == null ){
				bandera = false;
				mensajeValidacion.append( "  | El siniestro no existe :"+eFile.getIDReporteSin());
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R11 ID DE REPORTE: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R12 FOLIO TERCERO NO ENCONTRADO
			if( idTercero == null ){
				bandera = false;
				mensajeValidacion.append( "  | ID Tercero no encontrado ");
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R12 FOLIO TERCERO NO ENCONTRADO: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R13 
			
			// # R14 MONTO VALUACION NULO
			if ( eFile.getMontoTotalValuacion() < 0 ){
				bandera = false;
				mensajeValidacion.append( "  | El Folio  No Puede Ser Afectado por un Importe Negativo :"+eFile.getMontoTotalValuacion() );
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R14 MONTO VALUACION NULO: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R15
			
			// # R16
			
			// # R17
			
			// # R18 SI TIPO DE PROCESO 1,2 y MONTO TOTAL REPARACION > SUMA ASEGURADA
			if ( !this.isMontoMenorSumaAsegurada12(consultaHgsDto, eFile.getMontoTotalValuacion(), eFile.getEstatusExpediente() ) ){
				bandera = false;
				mensajeValidacion.append( "  | Operacion Incorrecta entre el Tipo de Proceso y el Estatus de la Valuacion isMontoMenorSumaAsegurada12. ");
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R18 SI TIPO DE PROCESO 1,2 y MONTO TOTAL REPARACION > SUMA ASEGURADA: "+mensajeValidacion ,consultaHgsDto.toString()+" "+eFile.toString(),eFile.getNumFolio(),false );
			}
			// # R19
			
			// # R20
			
			// # R21
			
			// # R22
			
			// # R23
			
			// # R24 SI TIPO DE PROCESO 1,2,3,8 Y ESTATUS DE LA VALUACION 17
			if( ( Integer.parseInt(eFile.getEstatusExpediente()) == 17) & ( ( Integer.parseInt( eFile.getTipoProceso() ) == 2 ) || ( Integer.parseInt( eFile.getTipoProceso() ) == 3 ) || ( Integer.parseInt( eFile.getTipoProceso() ) == 8 )) ){
				bandera = false;
				mensajeValidacion.append( " | Operacion Incorrecta entre el Tipo de Proceso y el Estatus de la Valuacion. ");
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R24 SI TIPO DE PROCESO 1,2,3,8 Y ESTATUS DE LA VALUACION 17: "+mensajeValidacion ,eFile.toString(),eFile.getNumFolio(),false );
			}
			
			// # R25 SI TIPO DE PROCESO 3,8 Y SUMA ASEGURADA = 0
			if ( !this.isSumaAseguradaProceso38(consultaHgsDto , idTercero, eFile.getTipoProceso()) ){
				bandera = false;
				mensajeValidacion.append( "  | Operacion Incorrecta entre el Tipo de Proceso y el Estatus de la Valuacion isSumaAseguradaProceso38.  ");
				// # LOG
				this.salvaHistoricoEnvio( eFile.getIDReporteSin(), null,HgsEnvioLog.TipoLogHGS.VALUACION,"R25 SI TIPO DE PROCESO 3,8 Y SUMA ASEGURADA = 0: "+mensajeValidacion ,consultaHgsDto.toString()+" "+eFile.toString(),eFile.getNumFolio(),false );
			}
		
		}catch(Exception e){
			LOG.error("Error en validaValuacion: ",e);
		}
		
		validaciones.put("mensaje", mensajeValidacion.toString());
		validaciones.put("bandera", bandera);
		
		return validaciones;
	}
	
	private boolean isMontoMenorSumaAsegurada12(ConsultaHgsDto consultaHgsDto, Double montoTotalValuacion, String keyEstatusExpediente ){
		
		boolean bandera = false;
		
		try{
			if( Integer.parseInt(keyEstatusExpediente) == 1 || Integer.parseInt(keyEstatusExpediente) == 2 ){
				if ( montoTotalValuacion > consultaHgsDto.getSumaAsegurada() ){
					bandera = false;
				}else{
					bandera = true;
				}
			}else{
				bandera = true;
			}
		}catch(Exception e){
			LOG.error("Error en isMontoMenorSumaAsegurada12: ",e);
		}
		return bandera;
	}
	
	private boolean isSumaAseguradaProceso38(ConsultaHgsDto consultaHgsDto, EstimacionCoberturaReporteCabina idTercero, String keyTipoProceso ){
		
		boolean bandera = false;
		
		try{
			if ( consultaHgsDto == null ){
				bandera = false;
			}else{
				if( Integer.parseInt(keyTipoProceso) == 3 || Integer.parseInt(keyTipoProceso) == 8 ){
					if( consultaHgsDto.getSumaAsegurada() <= 0 ){
						bandera = false;
					}else{
						bandera = true;
					}
				}else{
					bandera = true;
				}
			}
		}catch(Exception e){
			LOG.error("Error en isSumaAseguradaProceso38: ",e);
		}
		
		return bandera;
	}
	
	private boolean isEstatusValido(String estatus,Integer[] tipos){
	
		boolean bandera = false;
		
		try{
			for ( int i=0; i<=tipos.length-1; i++ ){
				if ( tipos[i] == Integer.parseInt( estatus ) ){
					bandera = true; 
					break; 
				}
			}
		}catch(Exception e){
			LOG.error("Error en isEstatusValido: ",e);
		}
		
		return bandera;
	}
	

	private boolean isEstatusVehiculoValido(ValeRefaccion[] valesRefacciones){
		
		boolean bandera = false;
		
		try{
			if ( valesRefacciones.length == 0 ){
				// # SE PERMITE TRUE YA QUE EN CASO DE SER PERDIDA TOTAL NO LLEGA CON VALES DE REACCION
				bandera = true;
			}else{
				for ( int i=0; i <= valesRefacciones.length-1; i++ ){
					if ( this.isEstatusValido( valesRefacciones[i].getEstatusVale(), HgsServiceImpl.ESTATUS_VALE ) ){
						 bandera = true; 
						 break;
					}
				}
			}
		}catch(Exception e){
			LOG.error("Error en isEstatusVehiculoValido: ",e );
		}
		
		return bandera;
	}
	
	@Override
	public List<ValuacionHgs> consultaValuacion(Long keyPaseAtencion) {
		
		List<ValuacionHgs> resultado = null;
		
		Map<String,Object> parametros = new HashMap<String,Object>();
		parametros.put("paseatencionId", keyPaseAtencion);
		
		resultado = entidadService.findByProperties(ValuacionHgs.class, parametros);
		
		return resultado;
	}
	
	@Override
	public String imprimirTrazaError(Exception e){
		
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		e.printStackTrace(printWriter);
		return writer.toString();
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public TransporteCommDTO init(Object e) {
		
		@SuppressWarnings("rawtypes")
		TransporteCommDTO comm = new TransporteCommDTO<T>();
		
		try{
		
			if( e instanceof Reporte ){
				
				Reporte reporte = (Reporte) e;
				
				comm.setFolio			 (reporte.getNumFolio());
				comm.setTipoProceso		 (TIPO_PROCESO.HGS_ALTA_PASE);
				comm.setTipoComm		 (TIPO_COMM.WS);
				comm.setUsuario			 (usuarioService.getUsuarioActual());
				comm.setObjetoASerializar(e);
				
				//conf notificacion
				comm.setCodigoNotificacion(EnumCodigo.NOTIFICA_FALLA_HGS);
				Map<String, Serializable> datosNotificacion = new HashMap<String, Serializable>();
				datosNotificacion.put("folio", comm.getFolio());		
				comm.setDatosNotificacion(datosNotificacion);
				
			}else if ( e instanceof OrdenCompra ){
				
				OrdenCompra ordenCompra = (OrdenCompra) e;
				comm.setFolio(ordenCompra.getIdCoberturaCompuesta());
				comm.setTipoProceso		 (TIPO_PROCESO.ORDEN_COMPRA_PAGADA);
				comm.setTipoComm		 (TIPO_COMM.WS);
				comm.setUsuario			 (usuarioService.getUsuarioActual());
				comm.setObjetoASerializar(e);
				
			}
			
		}catch(Exception ex){
			LOG.error("HGS - Error init TransporteCommDTO ",ex);
		}
		
		processor = super.sessionContext.getBusinessObject(HgsService.class);
		
		return comm;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public TransporteCommDTO enviar(TransporteCommDTO comm)
			throws CommException {
		LOG.info("SIMULANDO ENVIO POR WS");
		
		if( comm.getObjetoASerializar() instanceof Reporte ){
			
			Reporte reporte = (Reporte) comm.getObjetoASerializar();
			
			this.registrarBitacora(EVENTO.DERIVACION_HGS, "INVOCACION WS HGS", "DERIVACION HGS", ( StringUtil.isEmpty(reporte.getNumReporte()) ? "0" : reporte.getNumReporte() ) );
			
			this.enviarHgs( (Reporte) reporte );
			
		}else if ( comm.getObjetoASerializar() instanceof OrdenCompra ){
			
			OrdenCompra orden = (OrdenCompra) comm.getObjetoASerializar();
			
			this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "PREVIO INVOCACION A WS DE ORDEN COMPRA", "OC: "+orden.getId(), ( orden.getId() == null ? "0" : orden.getId().toString() )  );
			
			this.enviarOrdenPagada( (OrdenCompra) orden );
		}

		return comm;
	}
	
	private void enviarOrdenPagada(OrdenCompra ordenCompra){
		
		String  respuestaWs  = "", wsMensaje="";
		String  origenError = "";
		EfileServiceSOAPPortProxy serviceInsertaReporte = new EfileServiceSOAPPortProxy(); 
		
		Long keyOrdenCompra = Long.valueOf( ordenCompra.getId() );
		// # LOG
		this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "INICIA INVOCACION A WS DE ORDEN COMPRA", "OC: "+ordenCompra.getId(), ( ordenCompra.getId() == null ? "0" : ordenCompra.getId().toString() )  );
		
		// OBTENER ENDPOINT
		serviceInsertaReporte._getDescriptor().setEndpoint(this.getValorParametroGlobal(ParametroGlobalService.AP_MIDAS,ParametroGlobalService.WS_HGS_REPORTE));

		// INSERTA REPORTE
		try{

			ValuacionValeRefaccionFinal refaccionFinal = this.obtenerFolioRefaccionByOrdenCompra(ordenCompra.getId());
			
			if (refaccionFinal.getFolio() !=  null){
				
				respuestaWs = serviceInsertaReporte.actualizarVale(refaccionFinal.getValeId().toString(),ordenCompra.getId().toString(), PAGADO);
				
				if (Integer.parseInt(respuestaWs) == 1){
					wsMensaje = "Resultado Exitoso - La orden "+ordenCompra.getId()+" se actualizo con exito en HGS";
				}else{
					wsMensaje = "Error al esperar respuessta HGS de la orden de compra : ---> ("+respuestaWs+") <---";
				}
				
			}else{
				wsMensaje = "La orden de compra "+ordenCompra.getId()+" no existe en la valuacion de refacciones";
			}
			
			this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "FINALIZA INVOCACION A WS DE ORDEN COMPRA -- RESPUESTA WSs", "OC: "+ordenCompra.getId()+" | MENSAJE WS: "+wsMensaje, ( ordenCompra.getId() == null ? "0" : ordenCompra.getId().toString() )  );

		}catch(SOAPFaultException e){
			LOG.error("HGS enviarOrdenPagada ERROR - SOAPFaultException fallo",e);
			this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "FINALIZA INVOCACION A WS DE ORDEN COMPRA -- SOAPFaultException ERROR", imprimirTrazaError(e), ( ordenCompra.getId() == null ? "0" : ordenCompra.getId().toString() )  );
			throw new CommException(CODE.COMM_ERROR, "SOAPFaultException falló: "+imprimirTrazaError(e));
		}catch(Exception  e){
			LOG.error("HGS enviarOrdenPagada ERROR - Exception ",e);
			this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "FINALIZA INVOCACION A WS DE ORDEN COMPRA -- Exception ERROR", imprimirTrazaError(e), ( ordenCompra.getId() == null ? "0" : ordenCompra.getId().toString() )  );
			throw new CommException(CODE.GENERAL_ERROR, origenError+" Exception|Datos falló: "+ imprimirTrazaError(e) );
		}

		
	}
	
	private ValuacionValeRefaccionFinal obtenerFolioRefaccionByOrdenCompra(Long idOrdenCompra){
		ValuacionValeRefaccionFinal refaccionFinal = null;
		
		List<ValuacionValeRefaccionFinal> refaccion = this.entidadService.findByProperty(ValuacionValeRefaccionFinal.class, "ordenCompraId", idOrdenCompra);
		
		for(ValuacionValeRefaccionFinal lRefacciones : CollectionUtils.emptyIfNull(refaccion)){
			refaccionFinal = lRefacciones;
			break;
		}
		
		return refaccionFinal;
	}
	
	private void enviarHgs( Reporte reporteHgsEnviar ){
		
		String respuestaWs  = "";
		String  origenError = "";
		EfileServiceSOAPPortProxy serviceInsertaReporte = new EfileServiceSOAPPortProxy(); 
		
		// INSERTA REPORTE
		try{
		
			Long keyReporte = Long.valueOf( reporteHgsEnviar.getIDReporteSin().toString() );
			// # LOG
			this.salvaHistoricoEnvio(keyReporte,null,HgsEnvioLog.TipoLogHGS.REPORTE,"Se inicia insercion de reporte","","",true);
			
			// OBTENER ENDPOINT
			serviceInsertaReporte._getDescriptor().setEndpoint(this.getValorParametroGlobal(ParametroGlobalService.AP_MIDAS,ParametroGlobalService.WS_HGS_REPORTE));

			String reporteToString = this.reporteToString(reporteHgsEnviar);

			respuestaWs = serviceInsertaReporte.insertarReporte(reporteHgsEnviar);

			// # SI HGS RESPONDE NULO LA COLOCACIÓN DEL REPORTE FUE EXITOSA
			if( !validarRespuestaEntero( respuestaWs,reporteHgsEnviar ) ){
				origenError = "*** ERROR devuelto por HGS al intentar salvar los datos *** | ";
				throw new CommException(CODE.DATA_ERROR, "Error HGS al escribir datos dentro de HGS: "+respuestaWs );
			}

			// # LOG
			this.salvaHistoricoEnvio(
					keyReporte, 
					Long.valueOf( reporteHgsEnviar.getIDAseguradoTercero().toString() ),
					HgsEnvioLog.TipoLogHGS.REPORTE,
					"Resultado: ---> ("+respuestaWs+") <---",
					reporteToString,
					reporteHgsEnviar.getNumReporte(),
					true);

		}catch(SOAPFaultException e){
			LOG.error("HGS Valuacion ERROR - OAPFaultException fallo",e);
			throw new CommException(CODE.COMM_ERROR, "SOAPFaultException falló: "+imprimirTrazaError(e));
		}catch(Exception  e){
			LOG.error("HGS Valuacion ERROR - Socket fallo",e);
			throw new CommException(CODE.GENERAL_ERROR, origenError+"Socket|Datos falló: "+ imprimirTrazaError(e) );
		}
		
	}
	
	/***
	 * Validar si la respuesta de HGS es el numero de la valuación, si es, se vincula al LOG
	 * @param respuesta
	 * @return
	 */
	private boolean validarRespuestaEntero( String respuestaWs, Reporte reporteHgsEnviar ){
		 
		boolean bandera = false;
		try{
			
			if( respuestaWs == null ){
				bandera = true;
			}else if( respuestaWs.contains("Error") ){
				bandera = false;
			}else if( isNumerico(respuestaWs) ){
				
				this.salvaHistoricoEnvio(
						Long.valueOf( reporteHgsEnviar.getIDReporteSin().toString() ), 
						Long.valueOf( reporteHgsEnviar.getIDAseguradoTercero().toString() ),
						HgsEnvioLog.TipoLogHGS.ID_VALUACION,
						respuestaWs,
						"",
						reporteHgsEnviar.getNumReporte(),
						true);
				
				bandera = true;
			}

		}catch(Exception e){
			LOG.error("HGS - Error al convertir numero: "+respuestaWs,e);
			bandera = false;
		}
		
		return bandera;
	}
	
	public boolean isNumerico(String str)
	{
	    return str.matches("\\d+");
	}
	
	@Override
	public boolean isReenvioPase(String folio){
		
		boolean bandera = false;
		
		try{
			
			CommBitacora bitacora = commBitacoraService.obtenerBitacora(folio);
			if ( bitacora != null ){
				if(bitacora.getStatus().equals(STATUS_BITACORA.ERROR.name() ) || bitacora.getStatus().equals(STATUS_BITACORA.CANCELADO.name() )){
					return true;
				}
			}
		
		}catch(Exception e){
			LOG.error("HGS - error al isReenvioPase",e);
		}
		
		return bandera;
	}
	
	
	@Override
	public boolean isPendientesHgs(Long keyReporteCabina){
	
		List<EstimacionCoberturaReporteCabina>lCoberturaReporteCabina = this.buscarCoberturas(keyReporteCabina);		

		for( EstimacionCoberturaReporteCabina lCor : CollectionUtils.emptyIfNull(lCoberturaReporteCabina) ){				
			if (EnumUtil.equalsValue(lCor.getTipoEstimacion(), TipoEstimacion.RC_VEHICULO,TipoEstimacion.DANIOS_MATERIALES)
					&& !EnumUtil.equalsValue(TipoPaseAtencion.SOLO_REGISTRO, lCor.getTipoPaseAtencion()) 
					&& lCor.getEsEditable() != null && lCor.getEsEditable().equals("EDT")  && this.isReenvioPase(lCor.getFolio())) {					
				return Boolean.TRUE;
			}
		}
		return Boolean.FALSE;
	}

	@Override
	public boolean isExitosoHgs(String folio) {
		
		CommBitacora commBitacora = this.commBitacoraService.obtenerBitacora(folio, CommProceso.TIPO_PROCESO.HGS_ALTA_PASE, CommBitacora.STATUS_BITACORA.TERMINADO );
		if( commBitacora != null ){
			return Boolean.TRUE;
		}
		
		return Boolean.FALSE;
	}
	
	
	
	private String buscarNumSiniestro(Long keyReporteCabina){
		
		String numSiniestro = "SIN";
		
		try{

			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("reporteCabina.id" , keyReporteCabina);

			List<SiniestroCabina> lSiniestroCabina = this.entidadService.findByProperties(SiniestroCabina.class, params);

			if( !lSiniestroCabina.isEmpty() ){
				numSiniestro = lSiniestroCabina.get(0).getNumeroSiniestro();
			}

		}catch(Exception e){
			LOG.error("HGS - error al buscarNumSiniestro: ",e);
		}
			
		return numSiniestro;
		
	}
	
	private void notificarConvertirSiniestro(ReporteCabina reporte, String folio){
		try{
			envioNotificacionesService.notificacionConvertirSiniestroHgs(reporte, folio);
			
			
			HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
			
			EmailDestinaratios destinatarios = new EmailDestinaratios();
	        destinatarios.agregar(EnumModoEnvio.PARA, EMAIL_BASE_NOTIFICA_ERROR , "Julio Vasquez" ); 
			
	        dataArg.put("folio", folio);
			dataArg.put("reporte", reporte);
			dataArg.put("oficinaId", reporte.getOficina().getId());
			
			this.envioNotificacionHgs(
						EnvioNotificacionesService.EnumCodigo.CONVERTIR_SINIESTRO_HGS_PERDIDA_TOTAL.toString(),
						dataArg,
						"HGS notificarConvertirSiniestro - "+reporte.getNumeroReporte(),
						destinatarios
			);
			
		}catch(Exception e){
			LOG.error("HGS - Error notificacion notificaConceptoAjuste :",e);
		}
	}
	
	private void notificarErrorValidacionValuacion(String reporte, String mensaje){
		try{
			HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
			
			EmailDestinaratios destinatarios = new EmailDestinaratios();
	        destinatarios.agregar(EnumModoEnvio.PARA, EMAIL_BASE_NOTIFICA_ERROR , "Julio Vasquez" ); 
			
			mensaje = mensaje.replace(".", "<br/>");
			dataArg.put("reporte", reporte);
			dataArg.put("reglas", mensaje);
			
			this.envioNotificacionHgs(
						EnvioNotificacionesService.EnumCodigo.ERROR_VALIDACION_VALUACION.toString(),
						dataArg,
						"HGS notificarErrorValidacionValuacion - "+reporte,
						destinatarios
			);
			
		}catch(Exception e){
			LOG.error("HGS - Error notificacion notificarErrorValidacionValuacion :",e);
		}
	}
	
	private void notificarPrestadorNoValido(String reporte, String id, String nombre){
		try{
			envioNotificacionesService.notificarErrorValidacionValuacion(reporte, id, nombre);
			
			HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();

			EmailDestinaratios destinatarios = new EmailDestinaratios();
	        destinatarios.agregar(EnumModoEnvio.PARA, EMAIL_BASE_NOTIFICA_ERROR , "Julio Vasquez" ); 
			
			dataArg.put("reporte", reporte);
			dataArg.put("id"     , id);
			dataArg.put("nombre" , nombre);
			
			this.envioNotificacionHgs(
						EnvioNotificacionesService.EnumCodigo.PRESTADOR_SERVICIO_HGS_NO_VALIDO.toString(),
						dataArg,
						"HGS notificarPrestadorNoValido - "+reporte+ " - "+nombre,
						destinatarios
			);
			
			
		}catch(Exception e){
			LOG.error("HGS - Error notificacion notificarPrestadorNoValido :",e);
		}
	}
	
	
	private void envioNotificacionHgs(String codigoCorreo,HashMap<String, Serializable> data,String subject, EmailDestinaratios destinatarios ){
		
		this.envioNotificacionesService.enviarNotificacion(
				codigoCorreo, 
				data,
				subject , 
				destinatarios);
		
	}
	
	// #########################################################################################
	// #####################
	// #####################     DEVOLUCION DE PIEZAS   ****************************************
	// #####################
	// #########################################################################################
	
	@Override
	public String generarDevolucionPiezas(EfileDevolucion efileDevolucion){
		
		try{
			
			this.generaSesionUsuarioHgs();
			
			this.creaMapDeRespuesta(this.respuestaPiezasMap, efileDevolucion.getValuacionId().toString());
			
			this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"INICIA DEVOLUCION DE PIEZAS", "OC KEY:"+efileDevolucion.getOrdenCompraId(), efileDevolucion.getValuacionId().toString() );
			//VALIDA NO EXISTA LA ORDEN DE COMPRA PREVIAMENTE CREADA EN LA DEVOLUCION
			if( !isOrdenCompraDevolucionHgsExistente(efileDevolucion.getOrdenCompraId()) ){
				
				// VALIDA LA VALUACION EXISTA  
				if ( this.buscarPorValuacion( efileDevolucion.getValuacionId() ) != null ){
			
					// VALIDA SI LA ORDEN DE COMPRA EXISTE
					OrdenCompra ordenCompra = this.validarOrdenCompra(efileDevolucion.getOrdenCompraId());
					
					if( ordenCompra != null ){
						
						if( this.isOrdenPerteneceValuacion(efileDevolucion) ){
							//this.salvaHistoricoEnvio(new Long(0), new Long(0),HgsEnvioLog.TipoLogHGS.RECIBE_DEVOLUCION_PIEZAS,"LA ORDEN DE COMPRA SI PERTENECE A LA VALUACION","OC:"+efileDevolucion.getOrdenCompraId(),ordenCompra.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getNumeroReporte(),true);
							this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"LA ORDEN DE COMPRA SI PERTENECE A LA VALUACION", "OC KEY:"+efileDevolucion.getOrdenCompraId(), efileDevolucion.getValuacionId().toString() );
							
							this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"ESTATUS ORDEN DE COMPRA", "OC ESTATUS: "+ordenCompra.getEstatus()+" - COMPARAR CON: "+OrdenCompraService.ESTATUS_PAGADA , efileDevolucion.getValuacionId().toString() );
							
							if( ordenCompra.getEstatus().equals(OrdenCompraService.ESTATUS_PAGADA) ){
								
								DevolucionHgs devolucionHgs = this.llenarObjetoDevolucionPiezas(efileDevolucion);
								
								if( devolucionHgs != null ){
									
									this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"SE GENERA RECUPERACIÓN DE PIEZAS", "ORDEN DE COMPRA: "+devolucionHgs.getOrdenCompra().getId()+" DEVOLUCIÓN: "+devolucionHgs.getOrdenCompra().getId(), efileDevolucion.getValuacionId().toString() );
									
									this.generarRecuperacion(devolucionHgs,ordenCompra);
									
								}else{
									
									this.respuestaPiezasMap.put(efileDevolucion.getValuacionId().toString(), "HGS - 14 | Error no se pudo procesar los datos de la devolución");
									//this.errorPiezas ="HGS - 14 | Error no se pudo procesar los datos de la devolución";
									this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, this.respuestaPiezasMap.get(efileDevolucion.getValuacionId().toString()), "ID valuacion",efileDevolucion.getValuacionId().toString());
								}
								
							}else{
								this.respuestaPiezasMap.put(efileDevolucion.getValuacionId().toString(), "HGS - 16 | Error la orden de compra no se encuentra como pagada");
								//this.errorPiezas = "HGS - 16 | Error la orden de compra no se encuentra como pagada";
								this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, this.respuestaPiezasMap.get(efileDevolucion.getValuacionId().toString()), "Orden Compra ",efileDevolucion.getOrdenCompraId().toString());
							}
						
						}else{
							this.salvaHistoricoEnvio(new Long(0), new Long(0),HgsEnvioLog.TipoLogHGS.RECIBE_DEVOLUCION_PIEZAS,"LA ORDEN DE COMPRA NO PERTENECE A LA VALUACION","OC:"+efileDevolucion.getOrdenCompraId(),ordenCompra.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getNumeroReporte(),true);
							this.respuestaPiezasMap.put(efileDevolucion.getValuacionId().toString(), "HGS - 18 | La orden de compra no pertenece a la valuacion");
							//this.errorPiezas = "HGS - 18 | La orden de compra no pertenece a la valuacion";
							this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, this.respuestaPiezasMap.get(efileDevolucion.getValuacionId().toString()), "Orden Compra ", efileDevolucion.getOrdenCompraId().toString() );
						}
						
					}else{
						//this.errorPiezas = "HGS - 15 | La orden de compra en la cancelación de refacción no existe";
						this.respuestaPiezasMap.put(efileDevolucion.getValuacionId().toString(), "HGS - 15 | La orden de compra en la cancelación de refacción no existe");
						this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, this.respuestaPiezasMap.get(efileDevolucion.getValuacionId().toString()), "Orden Compra ", efileDevolucion.getOrdenCompraId().toString() );
					}
				
				}else{
					//this.errorPiezas = "HGS - 17 | La valuacion en la cancelación de refacciones no existe";
					this.respuestaPiezasMap.put(efileDevolucion.getValuacionId().toString(), "HGS - 17 | La valuacion en la cancelación de refacciones no existe");
					this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, this.respuestaPiezasMap.get(efileDevolucion.getValuacionId().toString()), "", efileDevolucion.getOrdenCompraId().toString());
				}
			
			}else{
				this.respuestaPiezasMap.put(efileDevolucion.getValuacionId().toString(), "HGS - 13 | Error la orden de compra:"+efileDevolucion.getOrdenCompraId()+" ya fue procesada");
				//this.errorPiezas = "HGS - 13 | Error la orden de compra:"+efileDevolucion.getOrdenCompraId()+" ya fue procesada";
				this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, this.respuestaPiezasMap.get(efileDevolucion.getValuacionId().toString()), "", efileDevolucion.getOrdenCompraId().toString());
			}
		
		}catch(Exception e){
			this.respuestaPiezasMap.put(efileDevolucion.getValuacionId().toString(), "HGS - 20 | Error general en el registro de piezas");
			//this.errorPiezas = "HGS - 20 | Error general en el registro de piezas";
			this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, "Error general al procesar generarDevolucionPiezas. Valuacion Id", this.imprimirTrazaError(e), efileDevolucion.getValuacionId().toString());
		}
		
		return this.respuestaPiezasMap.get(efileDevolucion.getValuacionId().toString());
	}
	
	private void generarRecuperacion(DevolucionHgs devolucionHgs,OrdenCompra ordenCompra){
		try {
			
			PrestadorServicio prestador = null;
			Long prestadorId =  null;
			RecuperacionProveedor recuperacionProveedor = new RecuperacionProveedor();
			
			// OBTENER DATOS DEL VEHICULO
			Map<String,Object> mDatosVehiculo = this.obtenerDatosVehiculo(devolucionHgs.getValuacion().getReporteCabina(), devolucionHgs.getValuacion().getPaseatencionId() );
			Vehiculo reporteVehiculo = (Vehiculo) mDatosVehiculo.get("reporteVehiculo");
			
			List<ValuacionValeRefaccionFinal> lRefaccionFinal = devolucionHgs.getValuacion().getValuacionRefaccionFinal();
			
			if( !lRefaccionFinal.isEmpty() ) {
				
				// BUSCAR PROVEEDOR EN LA ORDEN DE COMPRA
				for( ValuacionValeRefaccionFinal refaccion : lRefaccionFinal ) {
					if( refaccion.getOrdenCompraId().equals(ordenCompra.getId()) ) {
						prestador = this.obtenerPrestadorServicio( devolucionHgs.getValuacion().getFolio(), refaccion.getClaveProveedor() );
						prestadorId = refaccion.getClaveProveedor();
						break;
					}
				}
				
				if( null != prestador ) {
				
					recuperacionProveedor.setActivo(true);
					recuperacionProveedor.setEsRefaccion(true);
					recuperacionProveedor.setOrdenCompra(ordenCompra);
					recuperacionProveedor.setMedio(MedioRecuperacion.REEMBOLSO.name());
					recuperacionProveedor.setEstatus(EstatusRecuperacion.PENDIENTE.getValue());
					recuperacionProveedor.setTipo(TipoRecuperacion.PROVEEDOR.getValue());
					recuperacionProveedor.setOrigen(OrigenRecuperacion.AUTOMATICA.name());
					recuperacionProveedor.setTipoOrdenCompra( ordenCompra.getTipo() );
					recuperacionProveedor.setAdminRefacciones(devolucionHgs.getNombreAdminRefacciones());
					recuperacionProveedor.setPersonaDevolucion(devolucionHgs.getNomprePersonaDevolucion());
					recuperacionProveedor.setMotivoCancelacion(MOTIVO_DEVOLUCION_PIEZAS);
					recuperacionProveedor.setMotivoDevolucion(MOTIVO_DEVOLUCION_PIEZAS);
					recuperacionProveedor.setOtroMotivoDevolucion(devolucionHgs.getMotivo());
					recuperacionProveedor.setModeloVehiculo(Integer.parseInt( reporteVehiculo.getModelo() ) );
					recuperacionProveedor.setMarcaDesc( reporteVehiculo.getMarca() );
					recuperacionProveedor.setIva(devolucionHgs.getIva());
					recuperacionProveedor.setSubTotal(devolucionHgs.getSubtotal());
					recuperacionProveedor.setMontoTotal(devolucionHgs.getMontoTotal());
					recuperacionProveedor.setConceptoDevolucion(this.obtenerNombrePiezasRefacciones(devolucionHgs));
					recuperacionProveedor.setNombreTaller(mDatosVehiculo.get("nombreTaller").toString());
					recuperacionProveedor.setEstiloDesc(reporteVehiculo.getModelo());
					recuperacionProveedor.setCorreoProveedor( prestador.getPersonaMidas().getContacto().getCorreoPrincipal() );
					recuperacionProveedor.setCodigoUsuarioCreacion(HgsService.USUARIO_HGS);
					recuperacionProveedor.setReporteCabina( ordenCompra.getReporteCabina() );
					recuperacionProveedor.setComprador(prestador);
					
					this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"GUARDA RECUPERACIÓN PROVEEDOR", "RECUPERACION PROVEEDOR:"+recuperacionProveedor.toString(), ordenCompra.getId().toString() );
					
					RecuperacionProveedor recuperacion = recuperacionProveedorService.guardarRecuperacionProveedor(recuperacionProveedor, devolucionHgs.getValuacion().getCoberturaId().toString()+"|0" , devolucionHgs.getValuacion().getPaseatencionId().toString() , null);
					this.errorRecuperacion = "00 | OK  se generó la recuperación: "+recuperacion.getNumero();
					
					this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"RECUPERACIÓN PROVEEDOR EXITOSA", "SE GENERO LA RECUPERACIÓN: "+recuperacion.getNumero(), ordenCompra.getId().toString() );
					
				}else {
					this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"GUARDA RECUPERACIÓN PROVEEDOR", "PROVEEDOR ("+prestadorId+") NO EXISTE", ordenCompra.getId().toString() );
				}
				
			}
	
			
		} catch (Exception e) {
			LOG.error("HGS generarRecuperacion ERROR:",e);
			this.errorRecuperacion = "HGS - 19 | Error al generar la recuperacion: "+ this.imprimirTrazaError(e);
			this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"ERROR AL SALVAR RECUPERACION DE PROVEEDOR"+ this.errorRecuperacion, this.imprimirTrazaError(e), devolucionHgs.getOrdenCompra().getId().toString());
		}

	}
	
	private boolean isOrdenPerteneceValuacion(EfileDevolucion efileDevolucion){
		boolean bandera = false;
		
		efileDevolucion.getValuacionId();
		
		List<ValuacionHgs> lValuacion = entidadService.findByProperty(ValuacionHgs.class,"valuacionHgs", efileDevolucion.getValuacionId() );
		
		if( !lValuacion.isEmpty() ){
			
				List<ValuacionValeRefaccionFinal> lRefacciones = lValuacion.get(0).getValuacionRefaccionFinal();
				if( !lRefacciones.isEmpty() ){
					for( ValuacionValeRefaccionFinal lfinalRefaccion : lRefacciones ){
						if( lfinalRefaccion.getOrdenCompraId().equals(efileDevolucion.getOrdenCompraId()) ){
							bandera = true;
							break;
						}
					}
				}else{
					bandera = false;
				}
			
		}else{
			bandera = false;
		}
		
		return bandera;
	}
	
	private String obtenerNombrePiezasRefacciones(DevolucionHgs devolucionHgs){
		StringBuilder nombrePiezas = new StringBuilder();
		if( devolucionHgs != null ){
			if( !devolucionHgs.getDevolucionHgsRefaccion().isEmpty() ){
				for(DevolucionHgsRefaccion lDevolucion : devolucionHgs.getDevolucionHgsRefaccion() ){
					nombrePiezas.append(lDevolucion.getNombre()).append(",");
				}
				nombrePiezas.toString().substring(0, nombrePiezas.length()-1);
			}
		}
		return nombrePiezas.toString();
	}
	
	private OrdenCompra validarOrdenCompra(Long keyOrdenCompra){
		try{
			return entidadService.findById(OrdenCompra.class, keyOrdenCompra);
		}catch(Exception e){
			this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, "ORDEN COMPRA NO ENCONTRADA. ID ORDEN COMPRA: "+keyOrdenCompra, this.imprimirTrazaError(e),keyOrdenCompra.toString());
			LOG.error("Error en busqueda de orden de compra para cancelacion de refacciones",e);
			return null;
		}
	}
	
	private boolean isOrdenCompraDevolucionHgsExistente(Long keyOrdenCompra){
		
		try{
			List<DevolucionHgs> lDevolucionHgs = entidadService.findByProperty(DevolucionHgs.class,"ordenCompra.id", keyOrdenCompra);
			if( !lDevolucionHgs.isEmpty() ){
				this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"ORDEN DE COMPRA YA EXISTE EN LA TABLA DE TOSNDEVOLUCIONHGS", "ID OC:"+keyOrdenCompra, keyOrdenCompra.toString() );
				return true;
			}else{
				this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS,"ORDEN DE COMPRA NO EXISTE EN LA TABLA DE TOSNDEVOLUCIONHGS", "ID OC:"+keyOrdenCompra, keyOrdenCompra.toString() );
				return false;
			}
		}catch(Exception e){
			return false;
		}
		
	}
	
	private ValuacionHgs buscarPorValuacion(Long keyValuacion){
		try{
			List<ValuacionHgs> lValuacionHgs = entidadService.findByProperty(ValuacionHgs.class,"valuacionHgs", keyValuacion);
			if( !lValuacionHgs.isEmpty() ){
				this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, "VALUACION EXISTENTE: "+keyValuacion, "",keyValuacion.toString());
				return lValuacionHgs.get(0);
			}else{
				this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, "VALUACION NO EXISTE: "+keyValuacion, "",keyValuacion.toString());
				return null;
			}
		}catch(Exception e){
			this.registrarBitacora(EVENTO.RECUPERACION_PIEZAS, "VALUACION NO EXISTE, ERROR EN LA BUSQUEDA: "+keyValuacion, this.imprimirTrazaError(e),keyValuacion.toString());
			return null;
		}
	}
	
	private DevolucionHgs llenarObjetoDevolucionPiezas(EfileDevolucion efileDevolucion){
		
		DevolucionHgs devolucionHgs = new DevolucionHgs();
		
		devolucionHgs.setCantidadRefacciones(efileDevolucion.getCantidadRefacciones());
		devolucionHgs.setCodigoUsuarioCreacion(HgsService.USUARIO_HGS);
		devolucionHgs.setFechaCreacion(new Date());
		devolucionHgs.setIva(efileDevolucion.getIva());
		devolucionHgs.setMontoTotal(efileDevolucion.getMontoTotal());
		devolucionHgs.setMotivo(efileDevolucion.getMotivo());
		devolucionHgs.setNombreAdminRefacciones(efileDevolucion.getNombreAdminRefacciones());
		devolucionHgs.setNomprePersonaDevolucion(efileDevolucion.getNomprePersonaDevolucion());
		devolucionHgs.setOrdenCompra(this.entidadService.findById(OrdenCompra.class, efileDevolucion.getOrdenCompraId() ));
		devolucionHgs.setSubtotal(efileDevolucion.getSubtotal());
		devolucionHgs.setValuacion(this.buscarPorValuacion( efileDevolucion.getValuacionId() ));

		List<DevolucionHgsRefaccion> lDevolucionHgsRefacciones = new ArrayList<DevolucionHgsRefaccion>();
		// ITERA LISTA DE PIEZAS
		if( efileDevolucion != null && efileDevolucion.getEfileDevolucionRefacciones().length > 0 ){
			for( int i=0; i<=efileDevolucion.getEfileDevolucionRefacciones().length-1; i++ ){
				DevolucionHgsRefaccion devolucionHgsRefaccion = new DevolucionHgsRefaccion();
				devolucionHgsRefaccion.setIva          ( efileDevolucion.getEfileDevolucionRefacciones()[i].getIva() );
				devolucionHgsRefaccion.setMontoTotal   ( efileDevolucion.getEfileDevolucionRefacciones()[i].getMontoTotal() );
				devolucionHgsRefaccion.setNombre       ( efileDevolucion.getEfileDevolucionRefacciones()[i].getNombre() );
				devolucionHgsRefaccion.setSubTotal     ( efileDevolucion.getEfileDevolucionRefacciones()[i].getSubtotal() );
				devolucionHgsRefaccion.setDevolucionHgs(devolucionHgs);
				
				lDevolucionHgsRefacciones.add(devolucionHgsRefaccion);
			}
			
			// AGREGA LISTA DE PIEZAS
			devolucionHgs.setDevolucionHgsRefaccion(lDevolucionHgsRefacciones);
			
			this.entidadService.save(devolucionHgs);
			
		}else{
			//this.errorPiezas ="HGS - 12 | Error no hay piezas para procesar la devolución";
			this.respuestaPiezasMap.put(efileDevolucion.getValuacionId().toString(), "HGS - 12 | Error no hay piezas para procesar la devolución");
			return  null;
		}
		
		return devolucionHgs;
		
	}
	
	private void registrarBitacora(Bitacora.EVENTO evento,String mensaje, String detalle, String identificador){
		try{
			this.bitacoraService.registrar(TIPO_BITACORA.HGS, evento, identificador , mensaje,detalle, HgsService.USUARIO_HGS);
		}catch(Exception e){
			LOG.error("Error al registrar bitacora",e);
		}
	}
	
	
	// #########################################################################################
	// #####################
	// #####################     NOTIFICACION DE ORDEN PAGADA***********************************
	// #####################
	// #########################################################################################
	
	@SuppressWarnings("unchecked")
	@Override
	public void notificaEstatusOrdenCompra( Long keyLiquidacion){
		
		// IMPORTANTE!!!! YA QUE ESTE MÉTODO SE INVOCA DESDE UN TRIGGER DE LA BASE DE DATOS
		// CUANDO LLEGA LA INFORMACIÓN A ESTE PUNTO EL COMMIT AÚN NO SE HA REALIZADO
		// POR LO CUAL SE TOMAN LOS ESTATUS PREVIOS A UNA LIQUIDACION PAGADA Y ORDEN PAGADA
		// SE VALIDA CON ORLANDO ABASTA QUE AL MOMENTO DE NOTIFICAR PAGADA DESDE SCRIPT-TRIGGER
		// SI REALIZA LA OPERACIÓN CON ÉXITO LLAME A ESTE MÉTODO
		
		this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "SE INVOCA DESDE STORE A NOTIFICACION DE ORDEN PAGADA", "INICIA NOTIFICA LIQUIDACION PAGADA", "0" );
		this.generaSesionUsuarioHgs();    
		
		LiquidacionSiniestro liquidacionSiniestro = this.obtenerLiquidacion(keyLiquidacion);
		if( liquidacionSiniestro != null ){
			if ( !liquidacionSiniestro.getFacturas().isEmpty() ){
				for( DocumentoFiscal lDocFiscal : liquidacionSiniestro.getFacturas() ){
					if( !lDocFiscal.getOrdenesCompra().isEmpty() ){
						for( OrdenCompra lOrdenCompra : lDocFiscal.getOrdenesCompra() ){
							if( isValeRefaccionOrdenCompra(lOrdenCompra.getId(),keyLiquidacion)){
								
								if( this.validarEstatusOrdenCompra(lOrdenCompra) && liquidacionSiniestro.getEstatus().equals(LiquidacionSiniestro.EstatusLiquidacionSiniestro.LIBERADA_CON_SOLICITUD.getValue() ) ) {
									this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "NOTIFICAR HGS ORDEN PAGADA", "NOTIFICA LIQUIDACION PAGADA", lOrdenCompra.getId().toString() );
									realizarEnvio( lOrdenCompra );
								}else {
									this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "NOTIFICAR HGS ORDEN PAGADA", "ORDEN COMPRA NO SE ENCUENTRA EN ESTATUS PAGADO: "+lOrdenCompra.getEstatus()+" | LIQUIDACION ESTATUS: "+liquidacionSiniestro.getEstatus(), lOrdenCompra.getId().toString() );
								}
								
							}
						}
					}
				}
			}else {
				this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "LIQUIDACION NO TIENE FACTURAS ASOCIADAS","", keyLiquidacion.toString() );
			}
		}else{
			this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "LA LIQUIDACION NO EXISTE", " LIQUIDACION: "+keyLiquidacion.toString(), keyLiquidacion.toString() );
		}
		
	}
	
	private boolean isValeRefaccionOrdenCompra(Long keyOrdenCompra, Long keyLiquidacion){
		try{
			List<ValuacionValeRefaccionFinal> lValuacioRefaccionFinal = this.entidadService.findByProperty(ValuacionValeRefaccionFinal.class, "ordenCompraId", keyOrdenCompra);
			if( !lValuacioRefaccionFinal.isEmpty() ){
				return true;
			}else{
				return false;
			}
		}catch(Exception e){
			this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "LA ORDEN DE COMPRA NO PERTENECE A UN VALE DE REFACCION", " ORDEN COMPRA: "+keyOrdenCompra.toString()+" , LIQUIDACION: "+keyLiquidacion , keyOrdenCompra.toString() );
			LOG.error("ERROR isValeRefaccionOrdenCompra"+this.imprimirTrazaError(e),e);
			return false;
		}
	}
	
	private LiquidacionSiniestro obtenerLiquidacion( Long keyLiquidacion ){
		LiquidacionSiniestro liquidacionSiniestro = null;
		try{
			liquidacionSiniestro = this.entidadService.findById(LiquidacionSiniestro.class, keyLiquidacion);
		}catch(Exception e){
			LOG.error("ERROR obtenerLiquidacion: ",e);
			this.registrarBitacora(EVENTO.NOTIFICACION_ORDEN_COMPRA_HGS, "ERROR AL OBTENER LIQUIDACION", this.imprimirTrazaError(e) , keyLiquidacion.toString() );
		}
		
		return liquidacionSiniestro;
	}
	
	/**
	 * Alguno metodos usados requieren la sesion que viene al loguearse a la aplicación, como HGS son web services
	 * se necesita llenar el objeto setUsuarioActual en el UsuarioService
	 */
	private void generaSesionUsuarioHgs(){
		
		Usuario usuarioHgs = null;
		
		try{
			usuarioHgs = this.usuarioService.buscarUsuarioPorNombreUsuario(HgsService.USUARIO_HGS);
			if( usuarioHgs == null ){
				// SOLO APLICA PARA PRUEBAS LOCALES SI NO SE TIENE ACTIVADO ASM
				usuarioHgs = new Usuario();
				usuarioHgs.setNombreUsuario(HgsService.USUARIO_HGS);
			}
		}catch(Exception e){
			//usuarioHgs = new Usuario();
			//usuarioHgs.setNombreUsuario("SINUSHGS");
			LOG.error("ERROR AL GENERAR SESION DE USUARIO",e);
		}
		
		this.usuarioService.setUsuarioActual(usuarioHgs);
		
	}
	
	@Override
	public boolean validarEstatusOrdenCompra(OrdenCompra ordenCompra) {
		
		boolean bandera =  Boolean.FALSE;
		
		OrdenCompra orden = this.ordenCompraDao.obtenerOrdenCompra(ordenCompra.getId());
		
		if( orden.getEstatus().equals(OrdenCompraService.ESTATUS_ASOCIADA) ) {
			bandera = Boolean.TRUE;
		}
		
		return bandera;
		
	}
	
	
	/**
	 * Si retorna false no tiene ordenes de compra manual 
	 * @param ordenCompra
	 * @return
	 */
	private boolean validarSiExisteOrdenCompraManual(OrdenCompra ordenCompra) {
		
		boolean isTieneOrdeManual = false;
		
		try {
			
			
				
			String respuesta = this.ordenCompraService.validarCrearOrdenCompra(ordenCompra);

			if( respuesta != null ) {

				isTieneOrdeManual = true;

				this.registrarBitacora(EVENTO.GENERAR_ORDEN_COMPRA_HGS, 
						"YA EXSTE OREN DE COMPRA MANUAL: "+respuesta,
						"NO SE PUEDE GENERAR ORDEN DE COMPRA POR HGS YA QUE EXISTE UNA MANUAL: "+ordenCompra.toString(),
						ordenCompra.getReporteCabina().getNumeroReporte());

				this.notificarOrdenCompraManual(ordenCompra.getReporteCabina().getNumeroReporte(),ordenCompra.getCoberturaReporteCabina().getCoberturaDTO().getDescripcion(), ordenCompra.getIdTercero() );
			}
				
			
				
			
		}catch(Exception e) {
			LOG.error("HGSSERVICE | validarSiExisteOrdenCompraManual | Error al validar la orden de compra manual",e);
		}
		
		return isTieneOrdeManual;
		
	}
	
	private void notificarOrdenCompraManual(String reporte, String cobertura, long idTercero ) {
		
		try {
		
			EstimacionCoberturaReporteCabina estimacion = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, idTercero);
			
			HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
			dataArg.put("reporte", reporte+" ("+cobertura+") " );
			dataArg.put("folio", estimacion.getFolio());
			
			this.envioNotificacionesService.enviarNotificacion(EnvioNotificacionesService.EnumCodigo.ORDEN_COMPRA_NO_VALIDA.toString(), dataArg);
			
		}catch(Exception e) {
			LOG.error("HGSSERVICE | notificarOrdenCompraManual | Error al notificar orden de compra manual",e);
		}
	}
	
	
	private String obtenerNombreBeneficiario(ValuacionHgs valuacionHgs, String tipoProceso) {
		
		String nombreBeneficiario = "";
		
		if( tipoProceso.equals(TIPO_PROCESO_STR_DANIOS) ) {
			
			if( !valuacionHgs.getValuacionRefaccionFinal().isEmpty() ){
				for( ValuacionValeRefaccionFinal lvr : valuacionHgs.getValuacionRefaccionFinal() ){
					if( lvr.getConcepto().equals(CODIGO_PAGO_DANIOS_VALE) ) {
						nombreBeneficiario = lvr.getNombreProveedor();
						break;
					}
				}
			}
			
		}
		
		return nombreBeneficiario;
	}
	
	//joksrc * * * * *
	@Override
	public ReporteCabina obtenerReportePorEstimacionId(Long idEstimacionCobertura){
		ReporteCabina reporte = null;
		
		EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstimacionCobertura);
		Long idReporteCabina = estimacion.getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId();
		reporte = this.obtenerReporteCabina(idReporteCabina);
		return reporte;
	}
	
	@Override
	public List<ValuacionHgs> guardarValuacionExistenteHgsDto (Long idEstimacionCoberturaReporteCabina, ValuacionHGSEditDTO informacionValuacion){
		List<ValuacionHgs> valuacionEditada = null;
		
		return valuacionEditada;
	}
	
	@Override
	public List<ValuacionHgs> guardarValuacionExistenteHgs(Long idEstimacionCoberturaReporteCabina, Short tipoProceso, Short estatusValuacion, 
			Long idValuacionHGS, Date fechaIngresoTaller,Date fechaTerminacion, String idValuador, Date fechaReingresoReparacion, 
			Date ultimoSurtidoRefacciones, String nombreValuador, BigDecimal montoRefacciones, BigDecimal montoManoObra, 
			BigDecimal montoTotalReparacion){
		List<ValuacionHgs> valuacionList = this.consultaValuacion(idEstimacionCoberturaReporteCabina);
		String userActual = this.usuarioService.getUsuarioActual().getNombreUsuario();
		ValuacionHgs valuacionData = valuacionList.get(0);
		
		valuacionData.setClaveTipoProceso(tipoProceso);
		valuacionData.setClaveEstatusVal(estatusValuacion);
		valuacionData.setValuacionHgs(idValuacionHGS);
		valuacionData.setFechaIngresoTaller(fechaIngresoTaller);
		valuacionData.setFechaTerminacion(fechaTerminacion);
		valuacionData.setValuadorId(idValuador);
		valuacionData.setFechaReingresoTaller(fechaReingresoReparacion);
		valuacionData.setFechaUltimoSurtido(ultimoSurtidoRefacciones);
		valuacionData.setCodigoUsuarioModificacion(userActual);
		valuacionData.setValuadorNombre(nombreValuador);
		valuacionData.setImRefacciones(montoRefacciones);
		valuacionData.setImpNanoObra(montoManoObra);
		valuacionData.setImpSubTotalValuacion(montoTotalReparacion);
		valuacionData.setFechaModificacion(new Date());
		
		entidadService.save(valuacionData);
		
		List<ValuacionHgs> valuacionEditada = this.consultaValuacion(idEstimacionCoberturaReporteCabina);
		
		
		
		return valuacionEditada;
		
	}
	
	@Override
	public List<ValuacionHgs> guardarValuacionNuevaManualHgsDto (Long idEstimacionCoberturaReporteCabina, ValuacionHGSEditDTO informacionValuacion){
		List<ValuacionHgs> valuacionNueva = null;
		
		return valuacionNueva;
	}
	
	@Override
	public List<ValuacionHgs> guardarValuacionNuevaHgs (Long idEstimacionCoberturaReporteCabina, Short tipoProceso, Short estatusValuacion, Long idValuacionHGS, 
			Date fechaIngresoTaller,Date fechaTerminacion, String idValuador, Date fechaReingresoReparacion, Date ultimoSurtidoRefacciones, 
			String nombreValuador, BigDecimal montoRefacciones, BigDecimal montoManoObra, BigDecimal montoTotalReparacion){
		
		ReporteCabina reporte = this.obtenerReportePorEstimacionId(idEstimacionCoberturaReporteCabina);
		String userActual = this.usuarioService.getUsuarioActual().getNombreUsuario();
		EstimacionCoberturaReporteCabina estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, idEstimacionCoberturaReporteCabina);
		Short claveCausaMov = 10;
		
		ValuacionHgs valuacionData = new ValuacionHgs();
		valuacionData.setReporteCabina(reporte);
		valuacionData.setLineaNegocioId(reporte.getSeccionReporteCabina().getSeccionDTO().getIdToSeccion().longValue());
		valuacionData.setIncisoReporteCabina(reporte.getSeccionReporteCabina().getIncisoReporteCabina());
		valuacionData.setCoberturaId(reporte.getSeccionReporteCabina().getIncisoReporteCabina().getCoberturaReporteCabina().get(0).getId());
		valuacionData.setFolio(estimacion.getFolio());
		valuacionData.setTipoMovimiento(ALTA_VALUACION_MANUAL_HGS);
		valuacionData.setClaveCausaMov(claveCausaMov);
		valuacionData.setFechaValuacion(new Date());
		valuacionData.setCodigoUsuarioCreacion(userActual);
		valuacionData.setPaseatencionId(idEstimacionCoberturaReporteCabina);
		valuacionData.setClaveTipoProceso(tipoProceso);
		valuacionData.setClaveEstatusVal(estatusValuacion);
		valuacionData.setValuacionHgs(idValuacionHGS);
		valuacionData.setFechaIngresoTaller(fechaIngresoTaller);
		valuacionData.setFechaTerminacion(fechaTerminacion);
		valuacionData.setValuadorId(idValuador);
		valuacionData.setFechaReingresoTaller(fechaReingresoReparacion);
		valuacionData.setFechaUltimoSurtido(ultimoSurtidoRefacciones);
		valuacionData.setValuadorNombre(nombreValuador);
		valuacionData.setImRefacciones(montoRefacciones);
		valuacionData.setImpNanoObra(montoManoObra);
		valuacionData.setImpSubTotalValuacion(montoTotalReparacion);
		valuacionData.setImpIvaValuacion(BigDecimal.ZERO);
		valuacionData.setImpTotalVal(montoTotalReparacion);
		
		entidadService.save(valuacionData);
		
		List<ValuacionHgs> valuacionNueva = this.consultaValuacion(idEstimacionCoberturaReporteCabina);
		
		return valuacionNueva;
	}
	
	@Override
	public List<String> obtenerValuadoresByNombreOrId(Long idValuador, String nombreValuador){
		List<PrestadorServicio> listaValuadores = null;
		List<String> valuadoresNombreId = new ArrayList<String>();
		try{
			int tope = 5241;
			for(int x=5232; x< tope; x++ ){
				PrestadorServicio valuador = this.entidadService.findById(PrestadorServicio.class, Integer.valueOf(x)); 
				if (valuador != null || !valuador.equals(null)){
					valuadoresNombreId.add(valuador.getId().toString().concat("-"+valuador.getNombrePersona()));
				}
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return valuadoresNombreId;
	}
	
	@Override
	public List<Map<String, Object>> obtenerListaValuadores(Long idValuador, String nombreValuador){
		List<Map<String, Object>> listaValuadores = new ArrayList<Map<String, Object>>();
		try{
			
			int tope = 5241;
			for(int x=5232; x< tope; x++ ){
				PrestadorServicio valuador = this.entidadService.findById(PrestadorServicio.class, Integer.valueOf(x));
				if (valuador != null || !valuador.equals(null)){
					Map<String, Object> mapaValuadores = new HashMap<String, Object>();
					Integer valuadorId = valuador.getId();
					mapaValuadores.put("id",valuadorId);
					mapaValuadores.put("label", valuadorId + "-" +valuador.getPersonaMidas().getNombre().replaceAll("\"", "'"));
					listaValuadores.add(mapaValuadores);
				}
			}
			
			
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return listaValuadores;
	}

}
