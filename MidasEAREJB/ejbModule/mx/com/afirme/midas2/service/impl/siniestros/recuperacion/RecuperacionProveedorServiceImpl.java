package mx.com.afirme.midas2.service.impl.siniestros.recuperacion;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.FolioVentaRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Ingreso;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion.EstatusRecuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ReferenciaBancaria;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.DatosRecuperacionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.OrdenCompraRecuperacionDTO;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.BitacoraNotificacionService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionProveedorService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.StringUtil;

/**
 * @author Lizeth De La Garza
 * @version 1.0
 * @created 19-may-2015 12:37:30 p.m.
 */
@Stateless
public class RecuperacionProveedorServiceImpl extends RecuperacionServiceImpl  implements RecuperacionProveedorService {
	
	//se agregan los servicios que pueden ser de utilidad . 
		
	@EJB
	private EnvioNotificacionesService envioNotificacionesService;

	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
		
	@EJB 
	private BitacoraNotificacionService bitacoraNotificacionService;
	
	@EJB
	private OrdenCompraService  ordenCompraService;
	

	@EJB
	private RecuperacionService recuperacionService;

	@EJB
	private IngresoService ingresoService;
	
	@EJB
	private RecuperacionProveedorService recuperacionProveedorService;
	
	private static final String ENVIO_CORREO_PRV_RECACCIONES="MOVTO_ENVIO_CORREO_PRV_REFACCION";
	private static final String ENVIO_CORREO_PRV_OTROS ="MOVTO_ENVIO_CORREO_PRV_OTROS";
	private static final String ENVIO_CORREO_COMPRADOR ="MOVTO_ENVIO_CORREO_COMPRADOR";
	private static final String SERV_PUBLICO = "2";
	private static final String FOLIO_VENTA = "FOLIO-REC-VENTA-";
	private static final String FOLIO_REEMBOLSO = "FOLIO-REC-REEMBOLSO-";
	private static final String TIENE_LIQUIDACION_ERROR_CODE = "TIENE_LIQ";


	private static final String ROL_GERENTE_OPERACIONES_INDEM = "Rol_M2_Gerente_Operaciones_Indemnizacion",
								ROL_COORDINADOR_PAGOS = "Rol_M2_Coordinador_Pagos",
								ROL_GERENTE_OPERACIONES_SP = "Rol_M2_Gerente_Operaciones_Servicio_Publico";
	
	/**
		Metodo que cancela una Recuperacion de Proveedor.
	 * @param idRecuperacion
	 * @param motivo
	 */
	@Override
	public String cancelarRecuperacionProveedor(Long idRecuperacion, String motivo){
		try{
			
			RecuperacionProveedor recuperacion = entidadService.findById(RecuperacionProveedor.class, idRecuperacion);

			
			if( recuperacion.getEstatus().equals(Recuperacion.EstatusRecuperacion.PENDIENTE.name()) ){
				this.ingresoService.cancelarIngresoPendientePorAplicar(recuperacion.getIngresoActivo(), "");
			}
			recuperacionService.cancelarRecuperacion(idRecuperacion, motivo);
			
			if(recuperacion.getMedio().equals(Recuperacion.MedioRecuperacion.NOTACRED.toString())){
				recuperacion.setNotasdeCredito(null);
				entidadService.save(recuperacion);
			}
			this.bitacoraNotificacionService.cambiarReenvio(FOLIO_REEMBOLSO + idRecuperacion, false);
			this.bitacoraNotificacionService.cambiarReenvio(FOLIO_VENTA + idRecuperacion, false);
			
		}catch(Exception ex){
			if( ex.getCause() instanceof NegocioEJBExeption  
					&& ((NegocioEJBExeption) ex.getCause()).getErrorCode().compareTo(TIENE_LIQUIDACION_ERROR_CODE) == 0){
				throw new NegocioEJBExeption(TIENE_LIQUIDACION_ERROR_CODE, "No se pudo cancelar la recuperacion con id: " + idRecuperacion + " debido a que esta ligada con liquidaciones");
			}
			throw new NegocioEJBExeption("RECUP_CANC00","No se pudo cancelar la recuperacion de proveedor  con id: " + idRecuperacion);
		}
			
		return "";
	}
	
	@Override
	public void cancelarEnvioNotificacion(Long recuperacionId)
	{
		this.bitacoraNotificacionService.cambiarReenvio(FOLIO_REEMBOLSO + recuperacionId, false);
		
	}

	/**
	 * Metodo que genera los folios de Venta para una Recuperacion de Proveedor.
	 * @param recuperacionId
	 */
	@Override
	public List<FolioVentaRecuperacion> generarFolioVenta(Long recuperacionId){
		RecuperacionProveedor recuperacion=	this.obtenerRecuperacionProveedor(recuperacionId);
		List<FolioVentaRecuperacion>  folios = this.obtenerFoliosVenta(recuperacion.getId());
		 Calendar calendar = Calendar.getInstance();
	     calendar.setTime(new Date());
		if (null!=folios && !folios.isEmpty()){
			return folios;
		}else{
			if(recuperacion.getOrigen().equalsIgnoreCase(Recuperacion.OrigenRecuperacion.MANUAL.toString())){
				FolioVentaRecuperacion folio = new FolioVentaRecuperacion();
				folio.setConcepto(recuperacion.getConceptoDevolucion());
				folio.setFolio(""+recuperacion.getNumeroValuacion()+calendar.get(Calendar.YEAR)+1);
				folio.setRecuperacion(recuperacion);
				folios.add(folio);
			}else {
				 String[] lstConceptos = recuperacion.getConceptoDevolucion().split(",") ;
				 int i =0;
				 for (String conceptos: lstConceptos){
					 	FolioVentaRecuperacion folio = new FolioVentaRecuperacion();
						folio.setConcepto(recuperacion.getConceptoDevolucion());
						folio.setFolio(""+recuperacion.getNumeroValuacion()+calendar.get(Calendar.YEAR)+i++);
						folio.setRecuperacion(recuperacion);
						folios.add(folio);
				 }
				 
			}
			this.entidadService.saveAll(folios);
			return folios;
		}
		
	}

	/**
	 * Metodo que guarda una Recuperacion de Proveedor.	
	 * 
	 * @param recuperacion
	 * @param coberturasConcat
	 * @param pasesConcat
	 * @param referencias
	 */
	@Override 
	public RecuperacionProveedor guardarRecuperacionProveedor(RecuperacionProveedor recuperacion, String coberturasConcat, String pasesConcat, List<ReferenciaBancaria> referencias)throws Exception {
		
		
		
		if (null==recuperacion.getOrdenCompra()){
			throw new NegocioEJBExeption("RECUP_SAVE00","Debe Asociar una Orden de Compra ala Recuperacion ");
		}
		if(null==recuperacion.getEsRefaccion()){
			recuperacion.setEsRefaccion(Boolean.FALSE);
		}
		if(StringUtil.isEmpty(recuperacion.getConceptoDevolucion())){
			
			throw new NegocioEJBExeption("RECUP_SAVE01","Debe Capturar Descripcion de Concepto Devolucion ");
		}
		
		if(StringUtil.isEmpty(recuperacion.getCorreoProveedor())){
			throw new NegocioEJBExeption("RECUP_SAVE02","Debe Capturar Correo electronico del proveedor ");
		}
		
		if (recuperacion.getSubTotal().compareTo(BigDecimal.ZERO)!=1){
			throw new NegocioEJBExeption("RECUP_SAVE03","Sub Total debe ser Mayor a cero");
		}
		
		ImportesOrdenCompraDTO importe=this.ordenCompraService.calcularImportes(recuperacion.getOrdenCompra().getId(), null, false);
		if(null!=importe){
			if (recuperacion.getMontoTotal().compareTo(importe.getTotal())==1){
				throw new NegocioEJBExeption("RECUP_SAVE04","El monto total no debe ser mayor que el monto de la orden de compra");
			}
		}
		
		if(null==recuperacion.getSubTotal()){
			recuperacion.setSubTotal(BigDecimal.ZERO);
		}
		if(null==recuperacion.getIva()){
			recuperacion.setIva(BigDecimal.ZERO);
		}
		if(null==recuperacion.getIsr()){
			recuperacion.setIsr(BigDecimal.ZERO);
		}
		if(null==recuperacion.getIvaRetenido()){
			recuperacion.setIvaRetenido(BigDecimal.ZERO);
		}
		if(null==recuperacion.getMontoTotal()){
			recuperacion.setMontoTotal(BigDecimal.ZERO);
		}
		
		if(null==recuperacion.getEsRefaccion()){
			recuperacion.setEsRefaccion(false);
		}

		
		if(null==recuperacion.getId()){
			recuperacion.setEstatus(Recuperacion.EstatusRecuperacion.PENDIENTE.toString());
		}
		
		
		if(recuperacion.getReporteCabina() != null  && recuperacion.getReporteCabina().getId() != null){
			recuperacion.setReporteCabina(entidadService.findById(ReporteCabina.class, recuperacion.getReporteCabina().getId()));
		}
		
		if(recuperacion.getOrdenCompra() != null  && recuperacion.getOrdenCompra() != null){
			recuperacion.setOrdenCompra(entidadService.findById(OrdenCompra.class, recuperacion.getOrdenCompra().getId()));
		}
		
		this.recuperacionProveedorService.guardarRecuperacionProveedor(recuperacion);
 		if(!StringUtil.isEmpty(coberturasConcat)){
 			this.recuperacionService.guardarCoberturasRecuperacion(recuperacion.getId(), coberturasConcat);
 		}
 		
 		if(!StringUtil.isEmpty(pasesConcat)){
 			this.recuperacionService.guardarPasesRecuperacion (recuperacion.getId(), pasesConcat);
 		}
 		
 		
 		
 		return recuperacion;
 		
 		
	}

	/**
	 * Metodo que guarda una Recuperacion de Proveedor.
	 * 
	 * @param recuperacion
	 */
	@Override
	public RecuperacionProveedor guardarRecuperacionProveedor(RecuperacionProveedor recuperacion){
		boolean crear=false;
		if(null==recuperacion.getId()){
			recuperacion.setCodigoUsuarioCreacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			recuperacion.setFechaCreacion(new Date());
			crear=true;
			Long numero=this.entidadService.getIncrementedSequence(Recuperacion.SECUENCIA_NUMERO);
			recuperacion.setNumero(numero);
			
		}else{
			recuperacion.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			recuperacion.setFechaModificacion(new Date()); 
		}	
		entidadService.save(recuperacion);
		if (crear){
			this.recuperacionService.generarIngreso(recuperacion);

			bitacoraService.registrar(TIPO_BITACORA.RECUPERACION, EVENTO.RECUPERACION_PIEZAS, recuperacion.getId().toString(),
					"Se ha generado una Recuperacion de  Proveedor con id" + recuperacion.getId(), recuperacion, usuarioService.getUsuarioActual().getNombreUsuario() );
			recuperacionService.generarReferenciaBancaria(recuperacion.getId());	
			if(recuperacion.getEsRefaccion()){
				this.enviarNotificacionRefaccion(recuperacion);
			}else {
				this.enviarNotificacionOtros(recuperacion);
			}
		}
		return recuperacion;
		
	}

	/**
	 * Metodo que guarda la informacion de una Venta para una Recuperacion de
	 * Proveedor.
	 * 
	 * @param idRecuperacion
	 * @param recuperacion
	 * @param referencias
	 */
	String ESTATUSESPERA_VENTA="ESPERA";
	String ESTATUSVENTA_VENTA="VENTA";
	@Override
	public String guardarVentaRecuperacionProveedor(Long idRecuperacion, RecuperacionProveedor recuperacion, List<ReferenciaBancaria> referencias){
		RecuperacionProveedor recuperacionSave=this.obtenerRecuperacionProveedor(recuperacion.getId());
		boolean bandera = false;
		if(null==recuperacion.getSubTotalVenta())
			recuperacion.setSubTotalVenta(BigDecimal.ZERO);
		
		if(null==recuperacion.getIvaVenta())
			recuperacion.setIvaVenta(BigDecimal.ZERO);
				
		if(null==recuperacion.getMontoVenta())
			recuperacion.setMontoVenta(BigDecimal.ZERO);
		
		if(recuperacion.getEstatusPreventa().equalsIgnoreCase(ESTATUSESPERA_VENTA)){
			if( !recuperacion.getEstatus().equalsIgnoreCase(Recuperacion.EstatusRecuperacion.PENDIENTE.toString())){
				return "El estatus de la Preventa es EN ESPERA DE VENTA , el estatus de la recuperacion no puede ser diferente de PENDIENTE POR RECUPERAR";
			}
			bandera=true;
			
			recuperacionSave.setEstatusPreventa(ESTATUSESPERA_VENTA);
			recuperacionSave.setEstatus(Recuperacion.EstatusRecuperacion.REGISTRADO.toString());
			
		}else if (recuperacion.getEstatusPreventa().equalsIgnoreCase(ESTATUSVENTA_VENTA)){
			if(  !recuperacion.getEstatus().equalsIgnoreCase(Recuperacion.EstatusRecuperacion.REGISTRADO.toString())){
				return "El estatus de la Preventa es VENTA , el estatus de la recuperacion no puede ser diferente de REGISTRADO";
			}
			
			if (null==recuperacion.getComprador().getId()){
				return "Debe Especificar un Comprador";
			}
			
			if (!(recuperacion.getMontoVenta().compareTo(BigDecimal.ZERO)==1)){
				return "EL Monto Final Recuperado debe ser mayor a cero";
			}
			if (StringUtil.isEmpty(recuperacion.getCorreoComprador())){
				return "Capture el correo Electronico del Comprador";
			}
			bandera=true;
			recuperacionSave.setEstatusPreventa(ESTATUSVENTA_VENTA);
			recuperacionSave.setEstatus(Recuperacion.EstatusRecuperacion.REGISTRADO.toString());

	
			}
		
		if(bandera ){
			/*if(null!=recuperacion.getEstadoVenta().getId()){
				estadoVenta= this.entidadService.findById(EstadoMidas.class, recuperacion.getEstadoVenta().getId());
			}
			if(null!=recuperacion.getCiudadVenta().getId()){
				ciudadVenta= this.entidadService.findById(CiudadMidas.class, recuperacion.getCiudadVenta().getId());
			}*/
			recuperacionSave.setMedio(Recuperacion.MedioRecuperacion.VENTA.toString());
			recuperacionSave.setFechaRegistroVenta(recuperacion.getFechaRegistroVenta());
			recuperacionSave.setEstadoVenta(recuperacion.getEstadoVenta());
			recuperacionSave.setCiudadVenta(recuperacion.getCiudadVenta());
			recuperacionSave.setUbicacionVenta(recuperacion.getUbicacionVenta());
			recuperacionSave.setPersonaRecogeRefaccion(recuperacion.getPersonaRecogeRefaccion());
			recuperacionSave.setPersonaEntregaRefaccion(recuperacion.getPersonaEntregaRefaccion());
			if(null!=recuperacion.getComprador() && null!=recuperacion.getComprador().getId())
				recuperacionSave.setComprador(recuperacion.getComprador());
			else 
				recuperacionSave.setComprador(null);
			recuperacionSave.setCorreoComprador(recuperacion.getCorreoComprador());
			recuperacionSave.setTelefonoComprador(recuperacion.getTelefonoComprador());		
			recuperacionSave.setComentariosVenta(recuperacion.getComentariosVenta());
			recuperacionSave.setSubTotalVenta(recuperacion.getSubTotalVenta());
			recuperacionSave.setIvaVenta(recuperacion.getIvaVenta())		;
			recuperacionSave.setMontoVenta(recuperacion.getMontoVenta());
		}
		this.entidadService.save(recuperacionSave);
		 if (recuperacion.getEstatusPreventa().equalsIgnoreCase(ESTATUSVENTA_VENTA)){
			 List<ReferenciaBancariaDTO> ref= this.recuperacionService.obtenerReferenciasBancaria(recuperacionSave.getId(), recuperacionSave.getMedio());
			 if(null==ref || ref.isEmpty()){
				 this.recuperacionService.generarReferenciaBancaria(recuperacionSave.getId());
				 this.recuperacionService.generarIngreso(recuperacionSave);
				 this.enviarNotificacionComprador(recuperacionSave);
			 }
		 }else if(recuperacion.getEstatusPreventa().equalsIgnoreCase(ESTATUSESPERA_VENTA)){			
			 List<Ingreso> ingresos  = recuperacionSave.getIngresos();
			 for (Ingreso ingreso: ingresos){
				 if (ingreso.getEstatus().equalsIgnoreCase(Ingreso.EstatusIngreso.PENDIENTE.getValue())){
					 this.ingresoService.cancelarIngresoPendientePorAplicar(ingreso, "Se ha registado la Venta");
				 }
			 }
		 }
		 
		 
		return "Recuperacion No."+recuperacion.getNumero()+" Guardada Exitosamente";
		
	}
	/**
	 * 
	 * Metodo que regresa los Folios de Venta para una Recuperacion de Proveedor.
	 * 
	 * @param recuperacionId
	 */
	@Override
	public List<FolioVentaRecuperacion> obtenerFoliosVenta(Long recuperacionId){
		List<FolioVentaRecuperacion>  Folios = this.entidadService.findByProperty(FolioVentaRecuperacion.class, "recuperacion.id", recuperacionId);
		return Folios;
	}

	/**
	 * Metodo que regresa el objeto correspondiente a una RecuperacionProveedor
	 * @param recuperacionId
	 */
	@Override
	public RecuperacionProveedor obtenerRecuperacionProveedor(Long recuperacionId){
		List<RecuperacionProveedor> lista = this.entidadService.findByProperty(RecuperacionProveedor.class, "id", recuperacionId);
		return lista.get(0);
	}
	
	@Override
	public DatosRecuperacionDTO obtenerDatosRecuperacionProveedor(Long recuperacionId){
		DatosRecuperacionDTO dto = new DatosRecuperacionDTO();
		
		RecuperacionProveedor recuperacion = this.obtenerRecuperacionProveedor(recuperacionId);
		if(null!=recuperacion){
			List<OrdenCompraRecuperacionDTO> lstRecuperacion=this.ordenCompraService.obtenerOrdenCompraRecuperacionProveedor(null, null, null, null,recuperacion.getOrdenCompra().getId());
			if(null!=lstRecuperacion && !lstRecuperacion.isEmpty()){
				OrdenCompraRecuperacionDTO recuperacionDto = lstRecuperacion.get(0);
				dto.setEsResponsabilidadCivil(recuperacionDto.isEsResponsabilidadCivil());
				dto.setEsDanosMateriales(recuperacionDto.isEsDanosMateriale());
				dto.setNoReporteSiniestro(recuperacion.getReporteCabina().getNumeroReporte());
				dto.setNoSiniestro(recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
				dto.setFactura(recuperacionDto.getFactura());
				dto.setFechaOrdenCompra(recuperacionDto.getFechaOrdenCompra());
				dto.setFechaOrdenPago(recuperacionDto.getFechaOrdenPago());
				dto.setNumOrdenCompra(recuperacionDto.getNumOrdenCompra());
				dto.setTelProveedor(recuperacionDto.getTelefonoProveedor());
				dto.setNombreProveedor(recuperacionDto.getNombreProveedor());
				dto.setNoProveedor(recuperacionDto.getNoProveedor());
				dto.setMontoTotalOC(recuperacionDto.getMontoOrdenCompra());
				dto.setMontoTotalOP(recuperacionDto.getMontoOrdenPago());
				dto.setEsRefaccion(recuperacionDto.getEsRefaccion());

			}
		}
		
		
		/*dto.setAdminRefacciones(adminRefacciones);
		dto.setAdminRefaccionesId(adminRefaccionesId);
		dto.setCorreoProveedor(correoProveedor);
		dto.setEsRefaccion(esRefaccion);
		dto.setEstatusDesc(estatusDesc);	
		dto.setMailProveedor(mailProveedor);
		dto.setModeloVehiculo(modeloVehiculo);		
		dto.setMarcaVehiculo(marcaVehiculo);	
		dto.setModeloVehiculo(modeloVehiculo);
		dto.setNombreTaller(nombreTaller);
		dto.setNombreValuador(nombreValuador);
		dto.setTelefonoProveedor(telefonoProveedor);		
		dto.setTipoOC(tipoOC);
		dto.setTipoVehiculo(tipoVehiculo);*/
		
		
		/*
		if (null!=recuperacionId){
			RecuperacionProveedor recuperacion = this.obtenerRecuperacionProveedor(recuperacionId);
			if(null!=recuperacion){
				dto.setEsResponsabilidadCivil(false);
				dto.setEsDanosMateriales(false);
				if(null!=recuperacion.getReporteCabina()){
					dto.setNoReporteSiniestro(recuperacion.getReporteCabina().getNumeroReporte());
					dto.setNoSiniestro(recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
				}
				if(null!=recuperacion.getOrdenCompra()){
					if(null!=recuperacion.getOrdenCompra().getIdBeneficiario()){
						if(StringUtils.isEmpty(recuperacion.getOrdenCompra().getFactura())){
							dto.setFactura(recuperacion.getOrdenCompra().getFactura());
						}						
						dto.setFechaOrdenCompra(recuperacion.getOrdenCompra().getFechaCreacion());
						dto.setFechaOrdenPago(recuperacion.getOrdenCompra().getOrdenPago().getFechaCreacion());
						dto.setNumOrdenCompra(recuperacion.getOrdenCompra().getId());
						
						Integer prestadorID = new Integer (recuperacion.getOrdenCompra().getIdBeneficiario().toString());
						PrestadorServicio prestador = this.entidadService.findById(PrestadorServicio.class, prestadorID);
						if(null!=prestador){
							String telProveedor=prestador.getPersonaMidas().getContacto().getTelCasaCompleto();
							dto.setTelProveedor(telProveedor);
							String nombreProveedor=prestador.getNombrePersona();
							dto.setNombreProveedor(nombreProveedor);
							dto.setNoProveedor(prestador.getId().toString());
						}
						
						
						
					}
					ImportesOrdenCompraDTO importeOC =this.ordenCompraService.calcularImportes(recuperacion.getOrdenCompra().getId(), null, false);
					BigDecimal montoTotalOC =importeOC.getTotal();
					dto.setMontoTotalOC(montoTotalOC);
					if(null!=recuperacion.getOrdenCompra().getOrdenPago()){
						DesglosePagoConceptoDTO desglose = this.pagosSiniestroService.obtenerTotales(recuperacion.getOrdenCompra().getOrdenPago().getId());
						if(null!= desglose){
							BigDecimal montoTotalOP=  desglose.getTotalesPagados();
							dto.setMontoTotalOP(montoTotalOP);
						}
					}
					
					if(null!=recuperacion.getOrdenCompra().getIdTercero()){
						TerceroRCVehiculo trc = (TerceroRCVehiculo) this.obtenerTercero( TerceroRCVehiculo.class,recuperacion.getOrdenCompra().getIdTercero() );
						if ( trc != null ){
							dto.setEsResponsabilidadCivil(true);
						}
						TerceroDanosMateriales tdm = (TerceroDanosMateriales) this.obtenerTercero( TerceroDanosMateriales.class,recuperacion.getOrdenCompra().getIdTercero() );
						if( tdm != null ){
							dto.setEsDanosMateriales(true);
						}
					}
					
					
				}
				
			}
		}
		*/
		
		return dto;
	}
	
	
	
	private void enviarNotificacionRefaccion (RecuperacionProveedor recuperacion  ){		
		List<String> nombresRol = new ArrayList<String>(); 
		nombresRol.add(ROL_GERENTE_OPERACIONES_INDEM);
		nombresRol.add(ROL_COORDINADOR_PAGOS);
		String servicio= recuperacion.getOrdenCompra().getReporteCabina().getOficina().getTipoServicio();
		if( !StringUtil.isEmpty(servicio) && servicio.equalsIgnoreCase(SERV_PUBLICO) ){
			nombresRol.add(ROL_GERENTE_OPERACIONES_SP);
		} 
		
		Integer prestadorID = new Integer (recuperacion.getOrdenCompra().getIdBeneficiario().toString());
		PrestadorServicio prestador = this.entidadService.findById(PrestadorServicio.class, prestadorID);
		EnvioNotificacionesService.EmailDestinaratios destinos = new EnvioNotificacionesService.EmailDestinaratios ();
        destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.PARA,recuperacion.getCorreoProveedor(),prestador.getPersonaMidas().getNombre()); 
        List<Usuario> user = usuarioService.buscarUsuariosPorNombreRol(nombresRol.toArray(new String[nombresRol.size()]));
        if(null!=user && !user.isEmpty()){
        	for(Usuario us : user){
            	if(!StringUtil.isEmpty(us.getEmail()))
            		destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, us.getEmail(), us.getNombreCompleto());
            }
        }
        
        
        // La configuracion ya tiene un correo dado de alta, aquí se está agregando otro en Runtime.                      
        //Mapa con los datos a reemplazar en el template
        HashMap<String, Serializable> data = new HashMap<String, Serializable>();        
        String rf="";
        rf+="<TABLE BORDER=1 WIDTH=300>";
        List<ReferenciaBancariaDTO> referencias = this.recuperacionService.obtenerReferenciasBancaria(recuperacion.getId(),recuperacion.getMedio());
        for (ReferenciaBancariaDTO dto : referencias){
        	rf+="<TR>";
        	rf+="<TD WIDTH=200>";
        	rf+=""+dto.getBancoMidas().getNombre();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getCuentaBancaria();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getClabe();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getNumeroReferencia();
        	rf+="</TD>";
        	rf+="</TR>";
        }
        rf+="</TABLE>";
        data.put("texto", rf);
        data.put("secuenciaEnvio", "1");
        if(!StringUtil.isEmpty(recuperacion.getConceptoDevolucion())){
        	data.put("concepto", recuperacion.getConceptoDevolucion());
        }else{
        	data.put("concepto", "");
        }
        data.put("monto",recuperacion.getMontoTotal());
        if(!StringUtil.isEmpty(recuperacion.getNombreTaller())){
        	data.put("taller", recuperacion.getNombreTaller());
        }else{
        	data.put("taller", "");
        }
        // Envio de la notificacion, el folio y el destino pueden ser nulos. 
        //Cuando se desea enviar una notificacion recurrente es recomendable agregar un folio, ya que eventualmente se deberá apagar la recurrencia cuando se hayan cumplido las condiciones.
        envioNotificacionesService.enviarNotificacion(ENVIO_CORREO_PRV_RECACCIONES,  data, FOLIO_REEMBOLSO+recuperacion.getId(), destinos,super.generarPDFInstrucctivoPago(recuperacion.getId()));
	}
	
	private void enviarNotificacionOtros (RecuperacionProveedor recuperacion  ){		
		String[] nombresRolFirmaCorreo  = {ROL_GERENTE_OPERACIONES_INDEM,ROL_COORDINADOR_PAGOS};
		List<String> nombresRol = new ArrayList<String>();
		nombresRol.add(ROL_GERENTE_OPERACIONES_INDEM);		
		nombresRol.add(ROL_COORDINADOR_PAGOS);		
		String servicio= recuperacion.getReporteCabina().getOficina().getTipoServicio();
		if( !StringUtil.isEmpty(servicio) && servicio.equalsIgnoreCase(SERV_PUBLICO) ){			
			nombresRol.add(ROL_GERENTE_OPERACIONES_SP);
		}
		
		Integer prestadorID = 0;
		if(null!=recuperacion.getOrdenCompra().getIdBeneficiario()){
			 prestadorID = new Integer (recuperacion.getOrdenCompra().getIdBeneficiario().toString());
		}
		PrestadorServicio prestador = this.entidadService.findById(PrestadorServicio.class, prestadorID);
		String nombre ="";
		if(null!=prestador  && null!=prestador.getPersonaMidas()){
			nombre=prestador.getPersonaMidas().getNombre();
		}
		EnvioNotificacionesService.EmailDestinaratios destinos = new EnvioNotificacionesService.EmailDestinaratios ();
        destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.PARA,recuperacion.getCorreoProveedor(),nombre); 
        List<Usuario> users = usuarioService.buscarUsuariosPorNombreRol(nombresRol.toArray(new String[nombresRol.size()]));
        for(Usuario user : CollectionUtils.emptyIfNull(users)){
        	if(!StringUtil.isEmpty(user.getEmail()))
        		destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, user.getEmail(), user.getNombreCompleto());
        }
        // La configuracion ya tiene un correo dado de alta, aquí se está agregando otro en Runtime.                      
        //Mapa con los datos a reemplazar en el template
        HashMap<String, Serializable> data = new HashMap<String, Serializable>();        
        String rf="";
        rf+="<TABLE BORDER=1 WIDTH=300>";
        List<ReferenciaBancariaDTO> referencias = this.recuperacionService.obtenerReferenciasBancaria(recuperacion.getId(),recuperacion.getMedio());
        for (ReferenciaBancariaDTO dto : referencias){
        	rf+="<TR>";
        	rf+="<TD WIDTH=200>";
        	rf+=""+dto.getBancoMidas().getNombre();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getCuentaBancaria();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getClabe();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getNumeroReferencia();
        	rf+="</TD>";
        	rf+="</TR>";
        	
        }
        rf+="</TABLE>";
        
        
        String personas = "";
        List<Usuario> userFirma = usuarioService.buscarUsuariosPorNombreRol(nombresRolFirmaCorreo);
        for(Usuario us : CollectionUtils.emptyIfNull(userFirma)){
        	personas+="<TR>";
        	personas+=" <td WIDTH=\"15%\" valign=\"top\" class= \"txt_mail\" >";        	
        	personas+=""+us.getNombreCompleto().toUpperCase();
        	personas+="</TD>";
        	personas+=" <td WIDTH=\"15%\" valign=\"top\" class= \"txt_mail\" >";
        	personas+=""+us.getEmail();
        	personas+="</TD>";
        	personas+=" <td WIDTH=\"70%\" valign=\"top\" class= \"txt_mail\" >";
        	personas+="Tel: "+us.getTelefonoOficina();
        	personas+="</TD>";
        	personas+="</TR>";
        }
        data.put("texto", rf);
        data.put("secuenciaEnvio", "1");
        if(!StringUtil.isEmpty(recuperacion.getConceptoDevolucion())){
        	data.put("concepto", recuperacion.getConceptoDevolucion());
        }else{
        	data.put("concepto", "");
        }
        data.put("monto",recuperacion.getMontoTotal());
        data.put("personas", personas);
        // Envio de la notificacion, el folio y el destino pueden ser nulos. 
        //Cuando se desea enviar una notificacion recurrente es recomendable agregar un folio, ya que eventualmente se deberá apagar la recurrencia cuando se hayan cumplido las condiciones.
        envioNotificacionesService.enviarNotificacion(ENVIO_CORREO_PRV_OTROS,  data,  FOLIO_REEMBOLSO+recuperacion.getId(), destinos,super.generarPDFInstrucctivoPago(recuperacion.getId()));
	}
	
	
	private void enviarNotificacionComprador(RecuperacionProveedor recuperacion  ){
		String[] nombresRol  = {ROL_GERENTE_OPERACIONES_INDEM,"Rol_M2_Coordinador_Cobro_Companias_Salvamentos"};
		String[] nombresRolFirmaCorreo  = {ROL_GERENTE_OPERACIONES_INDEM,"Rol_M2_Coordinador_Cobro_Companias_Salvamentos"};
		
		Integer prestadorID = new Integer (recuperacion.getOrdenCompra().getIdBeneficiario().toString());
		PrestadorServicio prestador = this.entidadService.findById(PrestadorServicio.class, prestadorID);
		EnvioNotificacionesService.EmailDestinaratios destinos = new EnvioNotificacionesService.EmailDestinaratios ();
        destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.PARA,recuperacion.getCorreoComprador(),prestador.getPersonaMidas().getNombre()); 
        List<Usuario> users = usuarioService.buscarUsuariosPorNombreRol(nombresRol);
        for(Usuario user : CollectionUtils.emptyIfNull(users)){
        	if(!StringUtil.isEmpty(user.getEmail()))
        		destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, user.getEmail(), user.getNombreCompleto());
        }
        // La configuracion ya tiene un correo dado de alta, aquí se está agregando otro en Runtime.                      
        //Mapa con los datos a reemplazar en el template
        HashMap<String, Serializable> data = new HashMap<String, Serializable>();        
        String rf="<br/>";
        rf+="<TABLE BORDER=1 WIDTH=300>";
        List<ReferenciaBancariaDTO> referencias = this.recuperacionService.obtenerReferenciasBancaria(recuperacion.getId(),recuperacion.getMedio());
        for (ReferenciaBancariaDTO dto : referencias){
        	rf+="<TR>";
        	rf+="<TD WIDTH=200>";
        	rf+=""+dto.getBancoMidas().getNombre();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getCuentaBancaria();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getClabe();
        	rf+="</TD>";
        	rf+="<TD WIDTH=100>";
        	rf+=""+dto.getNumeroReferencia();
        	rf+="</TD>";
        	rf+="</TR>";
        	
        }
        rf+="</TABLE>";
        data.put("texto", rf);
        data.put("secuenciaEnvio", "1");
        data.put("comprador", recuperacion.getComprador().getNombrePersona());
        
        if(!StringUtil.isEmpty(recuperacion.getConceptoDevolucion())){
        	data.put("concepto", recuperacion.getConceptoDevolucion());
        }else{
        	data.put("concepto", "");
        }
        if(!StringUtil.isEmpty(recuperacion.getComentariosVenta())){
        	data.put("comentarios", recuperacion.getComentariosVenta());
        }else{
        	data.put("comentarios", "");
        }
        data.put("monto",recuperacion.getMontoVenta());
        
        String personas = "";
        List<Usuario> userFirma = usuarioService.buscarUsuariosPorNombreRol(nombresRolFirmaCorreo);
        if(null!=userFirma && !userFirma.isEmpty()){
        	for(Usuario us : userFirma){
            	personas+="<TR>";
            	personas+=" <td WIDTH=\"15%\" valign=\"top\" class= \"txt_mail\" >";
            	personas+=""+us.getNombreCompleto().toUpperCase();
            	personas+="</TD>";
            	personas+=" <td WIDTH=\"15%\" valign=\"top\" class= \"txt_mail\" >";
            	personas+=""+us.getEmail();
            	personas+="</TD>";
            	personas+=" <td WIDTH=\"70%\"  valign=\"top\" class= \"txt_mail\" >";;
            	personas+="Tel: "+us.getTelefonoOficina();
            	personas+="</TD>";
            	personas+="</TR>";
            }
        }
        
        data.put("personas", personas);
        
        
        // Envio de la notificacion, el folio y el destino pueden ser nulos. 
        //Cuando se desea enviar una notificacion recurrente es recomendable agregar un folio, ya que eventualmente se deberá apagar la recurrencia cuando se hayan cumplido las condiciones.
        envioNotificacionesService.enviarNotificacion(ENVIO_CORREO_COMPRADOR,  data,  FOLIO_VENTA+recuperacion.getId(), destinos,super.generarPDFInstrucctivoPago(recuperacion.getId()));

        //	EnvioNotificacionCabinaService.notificarRecurrentes*/

	}


	
	@Override
	public BigDecimal montoTotalRecuperacionProveedor(Long idSiniestroCabina,
			EstatusRecuperacion estatus) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("reporteCabina.siniestroCabina.id", idSiniestroCabina);
		params.put("estatus", estatus.toString());
		BigDecimal monto  = BigDecimal.ZERO;
		List<RecuperacionProveedor>  lst=this.entidadService.findByProperties(RecuperacionProveedor.class, params);
		if(null!=lst && !lst.isEmpty()){
			for(RecuperacionProveedor recuperacion : lst){
				if(null!= recuperacion ){
					
				}
					monto= monto.add(recuperacion.getMontoTotal());
			}
		}
		return monto;
	}
	
	@Override
	public BigDecimal montoTotalRecuperacionProveedorPagadas(Long estimacionId){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("ordenCompra.idTercero", estimacionId);
		params.put("estatus", Recuperacion.EstatusRecuperacion.RECUPERADO.getValue());
		BigDecimal monto  = BigDecimal.ZERO;
		List<RecuperacionProveedor>  lst=this.entidadService.findByProperties(RecuperacionProveedor.class, params);
		if(lst != null && !lst.isEmpty()){
			for(RecuperacionProveedor recuperacion : lst){
				monto= monto.add(recuperacion.getMontoTotal());
			}
		}
		return monto;
	}
}