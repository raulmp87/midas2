package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatCausaBeneficio;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatCausaBeneficioService;

@Stateless
public class CatCausaBeneficioServiceImpl implements CatCausaBeneficioService{
	private static final long serialVersionUID = 1L;
	
	private EntidadService entidadService;
	
	@Override
	public CatCausaBeneficio completeObject(CatCausaBeneficio catCausaBeneficio) {
		CatCausaBeneficio retorno = new CatCausaBeneficio(); 
		if(catCausaBeneficio != null){
			if(catCausaBeneficio.getId().intValue() < 1){
				retorno = findIdByAttributes(catCausaBeneficio);
			}else{
				if(!validateAttributes(catCausaBeneficio)){
					retorno = this.findById(catCausaBeneficio.getId());
				}
			}
		}else{
			retorno.setId(new Long(-1));
		}
		return retorno;
	}

	private CatCausaBeneficio findById(long id) {
		CatCausaBeneficio retorno = new CatCausaBeneficio();
		if(id >= 0){
			retorno = entidadService.findById(CatCausaBeneficio.class, id);
		}
		return retorno;
	}

	@Override
	public List<CatCausaBeneficio> findByStatus(boolean status) {
		return entidadService.findByProperty(CatCausaBeneficio.class, "estatus", status?0:1);
	}
	
	private boolean validateAttributes(CatCausaBeneficio catCausaBeneficio){
		return catCausaBeneficio.getDescCatCausaBeneficio() != null && !catCausaBeneficio.getDescCatCausaBeneficio().equals("");
	}

	private CatCausaBeneficio findIdByAttributes(CatCausaBeneficio catCausaBeneficio) {
		if(validateAttributes(catCausaBeneficio)){
			Map<String,Object> parametros = new HashMap<String,Object>();			
			parametros.put("descCatCausaBeneficio", catCausaBeneficio.getDescCatCausaBeneficio());			
			List<CatCausaBeneficio> catCausaBeneficioList = entidadService.findByProperties(CatCausaBeneficio.class, parametros);
			if(!catCausaBeneficioList.isEmpty()){
				catCausaBeneficio.setId(catCausaBeneficioList.get(0).getId());
			}else{
				catCausaBeneficio.setId(new Long(0));
			}
		}else{
			catCausaBeneficio.setId(new Long(-1));
		}
		return catCausaBeneficio;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
}