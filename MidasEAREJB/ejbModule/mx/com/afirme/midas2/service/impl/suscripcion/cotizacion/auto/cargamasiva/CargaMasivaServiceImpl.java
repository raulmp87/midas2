package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargamasiva;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.codigo.postal.ColoniaDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVADTO;
import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAId;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.estilovehiculo.grupo.EstiloVehiculoGrupoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.grupo.EstiloVehiculoGrupoId;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoEndoso;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.estadodescuento.NegocioEstadoDescuento;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.autoexpedibles.AutoExpediblesDetalleAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.CargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasiva.DetalleCargaMasivaAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion.CargaMasivaCondicionesEsp;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivacotizacion.CargaMasivaCondicionesEspDet;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.CargaMasivaIndividualAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargamasivaindividual.DetalleCargaMasivaIndAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;


@Stateless
public class CargaMasivaServiceImpl extends CargaMasivaSoporteService implements
		CargaMasivaService {

	
	private static final Logger LOG = Logger.getLogger(CargaMasivaServiceImpl.class);
	
	private CatalogoValorFijoFacadeRemote catalogoValorFijoFacade;
		
	@EJB
	public void setCatalogoValorFijoFacade(
			CatalogoValorFijoFacadeRemote catalogoValorFijoFacade) {
		this.catalogoValorFijoFacade = catalogoValorFijoFacade;
	}
	
	
	@Override
	public CargaMasivaAutoCot guardaCargaMasiva(BigDecimal idCotizacion,
			ControlArchivoDTO controlArchivo, String usuario, Short tipoCarga) {
		CargaMasivaAutoCot cargaMasiva = null;
		if (controlArchivo != null) {
			cargaMasiva = new CargaMasivaAutoCot();
			cargaMasiva.setControlArchivo(controlArchivo);
			cargaMasiva.setIdToControlArchivo(controlArchivo
					.getIdToControlArchivo());
			cargaMasiva.setIdToCotizacion(idCotizacion);
			cargaMasiva.setFechaCreacion(new Date());
			Short uno = 1;
			Short dos = 2;
			if(tipoCarga.equals(uno)){
				cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_EN_PROCESO);
			}
			if(tipoCarga.equals(dos)){
				cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_PENDIENTE);
			}
			cargaMasiva.setCodigoUsuarioCreacion(usuario);

			BigDecimal idCargaMasiva = (BigDecimal) entidadService
					.saveAndGetId(cargaMasiva);
			cargaMasiva.setIdToCargaMasivaAutoCot(idCargaMasiva);
		}
		return cargaMasiva;
	}

	
	/**
	 * Guarda en la bd los datos complementarios del archivo por cada lÃ­nea del excel
	 * */
	@Override
	public void guardaDetalleCargaMasiva(CargaMasivaAutoCot cargaMasiva,
			List<DetalleCargaMasivaAutoCot> detalleList) {
		long start = System.currentTimeMillis();
		//Limpia detalle anterior
		deleteDetalleCargaMasiva(cargaMasiva.getIdToCargaMasivaAutoCot());
		
		//Guarda nuevo detalle
		for(DetalleCargaMasivaAutoCot detalle : detalleList){
			detalle.setIdToCargaMasivaAutoCot(cargaMasiva.getIdToCargaMasivaAutoCot());
			saveDetalleCargaMasiva(detalle);
		}
		LOG.info("guardaDetalleCargaMasiva elapsed time: " + ((System.currentTimeMillis() - start) / 1000.0));
	}
	
	/**
	 * Guarda en la bd los datos complementarios del archivo, nueva carga masiva
	 
	@Override
	public void guardaDetalleCargaMasiva(CargaMasivaAutoCot cargaMasiva, ArrayList<Resultado> resultados){
		long start = System.currentTimeMillis();
		//Limpia detalle anterior
		deleteDetalleCargaMasiva(cargaMasiva.getIdToCargaMasivaAutoCot());
		
	}*/


	/**
	 * Elimina los registros previos de la carga masiva.
	 * @param idToCargaMasivaAutoCot
	 */
	private void deleteDetalleCargaMasiva(BigDecimal idToCargaMasivaAutoCot){
		List<DetalleCargaMasivaAutoCot> detalleListAnterior = new ArrayList<DetalleCargaMasivaAutoCot>(1); 
		detalleListAnterior = entidadService.findByProperty(DetalleCargaMasivaAutoCot.class, "idToCargaMasivaAutoCot", idToCargaMasivaAutoCot);
		for (DetalleCargaMasivaAutoCot detalle : detalleListAnterior) {
			entidadService.remove(detalle);
		}
	}
	/**
	 * Guarda un nuevo reguistro para el detallede la carga masiva
	 * @param detalle
	 */
	private void saveDetalleCargaMasiva( DetalleCargaMasivaAutoCot detalle){
		if(detalle.getClaveEstatus() != null){
			entidadService.save(detalle);
		}
	}
	
	@Override
	public NegocioSeccion obtieneNegocioSeccionPorDescripcion(
			CotizacionDTO cotizacion, String descripcion) {
		final SolicitudDTO solicitud = cotizacion.getSolicitudDTO();
		final ProductoDTO producto = solicitud.getProductoDTO();
		final Negocio negocio = solicitud.getNegocio();
		final TipoPolizaDTO tipoPoliza = cotizacion.getTipoPolizaDTO();
		final String seccionDescripcion = descripcion;
		return negocioSeccionService.getByProductoNegocioTipoPolizaSeccionDescripcion(producto, negocio, tipoPoliza, seccionDescripcion);
	}

	@Override
	public NegocioTipoUso obtieneNegocioTipoUsoPorDescripcion(
			NegocioSeccion negocioSeccion, String descripcion) {
		NegocioTipoUso negocioTipoUso = negocioTipoUsoDao.findByNegocioSeccionAndTipoUsoDescripcion(negocioSeccion, descripcion);
		//Se intenta buscar el vehiculo usando el tipoVehiculo del negocioSeccion y la descripcion del tipo de uso. Sin embargo esto no me parece coherente ya que si
		//el negocio no tiene configurado el tipo uso porque deberiamos buscarlo tambien usando el tipo vehiculo. Todo esto surge ya que el metodo getNegocioTipoUsoListByNegSeccion
		//agrega a la lista los vehiculos usando el tipo vehiculo del negocio.
		if (negocioTipoUso == null) {
			TipoUsoVehiculoDTO tipoUsoVehiculoDTO = tipoUsoVehiculoFacadeRemote.findByTipoVehiculoAndDescripcion(negocioSeccion.getSeccionDTO().getTipoVehiculo(), descripcion);
			if (tipoUsoVehiculoDTO != null) {
				negocioTipoUso = new NegocioTipoUso();
				negocioTipoUso.setTipoUsoVehiculoDTO(tipoUsoVehiculoDTO);
			}
		}
		return negocioTipoUso;		
	}

	@Override
	public NegocioPaqueteSeccion obtieneNegocioPaqueteSeccionPorDescripcion(
			NegocioSeccion negocioSeccion, String descripcion) {
		return negocioPaqueteSeccionService.findByNegocioSeccionAndPaqueteDescripcion(negocioSeccion, descripcion);
	}

	@Override
	public EstiloVehiculoDTO obtieneEstiloVehiculoDTOPorClaveAMIS(
			CotizacionDTO cotizacion, NegocioSeccion negocioSeccion,
			String claveAMIS) {
		EstiloVehiculoDTO estiloVehiculo = null;

		try {
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion = new NegocioAgrupadorTarifaSeccion();
			negocioAgrupadorTarifaSeccion = negocioTarifaService
					.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(
							negocioSeccion.getIdToNegSeccion(),
							cotizacion.getIdMoneda());
			TarifaAgrupadorTarifa tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService
					.getPorNegocioAgrupadorTarifaSeccion(negocioAgrupadorTarifaSeccion);

			EstiloVehiculoGrupoDTO estiloVehiculoGrupo = new EstiloVehiculoGrupoDTO();
			EstiloVehiculoGrupoId estiloVehiculoGrupoId = new EstiloVehiculoGrupoId();
			estiloVehiculoGrupoId.setIdVersionCarga(new BigDecimal(
					tarifaAgrupadorTarifa.getId().getIdVertarifa()));
			estiloVehiculoGrupo.setClaveAMIS(claveAMIS);
			estiloVehiculoGrupo.setId(estiloVehiculoGrupoId);
			List<EstiloVehiculoGrupoDTO> estiloVehiculoGrupoList = estiloVehiculoGrupoFacade
					.listarFiltrado(estiloVehiculoGrupo);
			EstiloVehiculoGrupoDTO estiloVehiculoGrupoDTO = estiloVehiculoGrupoList
					.get(0);

			EstiloVehiculoId id = new EstiloVehiculoId();
			id.setClaveEstilo(estiloVehiculoGrupoDTO.getId().getClaveEstilo());
			id.setClaveTipoBien(estiloVehiculoGrupoDTO.getId()
					.getClaveTipoBien());
			id.setIdVersionCarga(estiloVehiculoGrupoDTO.getId()
					.getIdVersionCarga());

			estiloVehiculo = estiloVehiculoFacade.findById(id);
		} catch (Exception e) {
			LOG.error(e);
		}
		return estiloVehiculo;
	}

	@Override
	public String obtieneMunicipioIdPorCodigoPostal(String codigoPostal) {
		return ciudadFacadeRemote.getCityIdByZipCode(codigoPostal);
	}
	
	@Override
	public Double obtieneIVAPorCodigoPostalColonia(String codigoPostal, String nombreColonia){
		Double iva = new Double(16);
		try {
			List<ColoniaDTO> coloniaList = coloniaFacadeRemote
					.getColonyByZipCode(codigoPostal);
			if(coloniaList != null && !coloniaList.isEmpty()){
				for(ColoniaDTO colonia : coloniaList){
					if(colonia.getColonyName().trim().toUpperCase().equals(nombreColonia.toUpperCase().trim())){
						CodigoPostalIVAId codigoPostalIVAId= new CodigoPostalIVAId();
			    		codigoPostalIVAId.setNombreColonia(colonia.getColonyId());
			    		codigoPostalIVAId.setCodigoPostal(new BigDecimal(codigoPostal));
			    		CodigoPostalIVADTO  codigoPostalIVADTO = codigoPostalIVAFacadeRemote.findById(codigoPostalIVAId);				
		    		    if (codigoPostalIVADTO != null) {
		    		    	iva = codigoPostalIVADTO.getValorIVA();
		    		    }else{
		    			LogDeMidasEJB3.log("No se pudo obtener el valor del IVA, para la colonia: "+nombreColonia+" y codigo postal: "+ codigoPostal+"...", Level.WARNING,null);
		    		    }
		    		    break;
					}
				}
			}
		} catch (Exception e) {
			LOG.error(e);
		}
		return iva;
	}

	@Override
	public String obtieneEstadoIdPorMunicipio(String municipioId) {
		String estadoId = null;

		try {
			MunicipioDTO municipio = municipioFacadeRemote
					.findById(municipioId);
			estadoId = municipio.getStateId();
		} catch (Exception e) {
			LOG.error(e);
		}

		return estadoId;
	}

	@Override
	public List<CoberturaCotizacionDTO> obtieneCoberturasDelInciso(
			IncisoCotizacionDTO incisoCotizacion,
			NegocioPaqueteSeccion negocioPaqueteSeccion,
			EstiloVehiculoId estiloVehiculoId) {
		List<CoberturaCotizacionDTO> coberturaCotizacionList = null;
		try {
			Long numeroSecuencia = new Long("1");
			numeroSecuencia = numeroSecuencia
					+ incisoService.maxSecuencia(incisoCotizacion.getId()
							.getIdToCotizacion());
			coberturaCotizacionList = coberturaService.getCoberturas(
					negocioPaqueteSeccion.getNegocioSeccion()
							.getNegocioTipoPoliza().getNegocioProducto()
							.getNegocio().getIdToNegocio(),
					negocioPaqueteSeccion.getNegocioSeccion()
							.getNegocioTipoPoliza().getNegocioProducto()
							.getProductoDTO().getIdToProducto(),
					negocioPaqueteSeccion.getNegocioSeccion()
							.getNegocioTipoPoliza().getTipoPolizaDTO()
							.getIdToTipoPoliza(), negocioPaqueteSeccion
							.getNegocioSeccion().getSeccionDTO()
							.getIdToSeccion(), negocioPaqueteSeccion
							.getPaquete().getId(), incisoCotizacion
							.getIncisoAutoCot().getEstadoId(), incisoCotizacion
							.getIncisoAutoCot().getMunicipioId(),
					incisoCotizacion.getId().getIdToCotizacion(),
					incisoCotizacion.getCotizacionDTO().getIdMoneda()
							.shortValue(), numeroSecuencia, estiloVehiculoId
							.getClaveEstilo(),
					incisoCotizacion.getIncisoAutoCot().getModeloVehiculo()
							.longValue(), new BigDecimal(incisoCotizacion
							.getIncisoAutoCot().getTipoUsoId()), null, null);
		} catch (Exception e) {
			LOG.error(e);
		}
		return coberturaCotizacionList;
	}

	@Override
	public IncisoCotizacionDTO guardaIncisoCotizacion(
			IncisoCotizacionDTO incisoCotizacion,
			List<CoberturaCotizacionDTO> coberturaCotizacionList) {
		return guardaIncisoCotizacion(incisoCotizacion, coberturaCotizacionList, new ArrayList<DatoIncisoCotAuto>());
	}
	
	@Override
	public void guardaIncisoCotizacion(IncisoCotizacionDTO incisoCotizacion) {
		entidadService.save(incisoCotizacion);
	}
	
	@Override
	public IncisoCotizacionDTO guardaIncisoCotizacion(
			IncisoCotizacionDTO incisoCotizacion,
			List<CoberturaCotizacionDTO> coberturaCotizacionList, List<DatoIncisoCotAuto> datoIncisoCotAutos) {
		try {

			incisoCotizacion.getIncisoAutoCot().setAsociadaCotizacion(1);
			incisoCotizacion = incisoService.prepareGuardarInciso(
					incisoCotizacion.getId().getIdToCotizacion(),
					incisoCotizacion.getIncisoAutoCot(), incisoCotizacion,
					coberturaCotizacionList);
			
			for (DatoIncisoCotAuto datoIncisoCotAuto : datoIncisoCotAutos) {
				//Actualizar el numero de inciso con el del inciso que se guardo.
				datoIncisoCotAuto.setNumeroInciso(incisoCotizacion.getId().getNumeroInciso());
				entidadService.save(datoIncisoCotAuto);
			}
			
		} catch (Exception e) {
			Log.error(e);
			incisoCotizacion = null;
		}
		return incisoCotizacion;
	}
	
	@Override
	public IncisoCotizacionDTO obtieneIncisoCotizacion(
			CotizacionDTO cotizacion, BigDecimal numeroSecuencia) {
		//IncisoCotizacionId id = new IncisoCotizacionId();
		//id.setIdToCotizacion(cotizacion.getIdToCotizacion());
		//id.setNumeroInciso(numeroInciso);
		IncisoCotizacionDTO inciso = null;
		try {
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("id.idToCotizacion", cotizacion.getIdToCotizacion());
			parametros.put("numeroSecuencia",numeroSecuencia.longValue());
			List<IncisoCotizacionDTO> incisoList = entidadService.findByProperties(IncisoCotizacionDTO.class, parametros);
			//inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
			if(incisoList != null && !incisoList.isEmpty()){
				inciso = incisoList.get(0);
				/*
				try{
					List<SeccionCotizacionDTO> seccionCotizacionList = seccionCotizacionFacadeRemote.listarPorCotizacionNumeroInciso(cotizacion.getIdToCotizacion(), inciso.getId().getNumeroInciso());
					if(seccionCotizacionList != null && !seccionCotizacionList.isEmpty()){
						inciso.setSeccionCotizacionList(seccionCotizacionList);
					}
				}catch(Exception e){
					System.err.print("ERROR: Obtener Seccion Cotizacion");
					e.printStackTrace();
				}*/
			}
		} catch (Exception e) {
			LOG.error("ERROR: Obtener Inciso", e);
		}
		return inciso;
	}

	@Override
	public void guardarComplementarInciso(IncisoCotizacionDTO inciso, List<DatoIncisoCotAuto> datoIncisoCotAutos) {
		if(inciso != null){
			incisoService.complementarDatosInciso(inciso);
		}
		
		for (DatoIncisoCotAuto datoIncisoCotAuto : datoIncisoCotAutos) {
			if (datoIncisoCotAuto != null) {
				entidadService.save(datoIncisoCotAuto);
			}
		}
	}
	
	@Override
	public IncisoAutoCot obtieneIncisoAutoCot(IncisoCotizacionDTO inciso){
		IncisoCotizacionDTO incisoComp = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId()); 
		return incisoComp.getIncisoAutoCot();
	}

	@Override
	public Boolean datosDelConductorRequeridos(IncisoCotizacionDTO inciso) {
		return incisoService.infoConductorEsRequerido(inciso);
	}
	
	
	private enum DatoIncisoNivel {
		SUBRAMO,
		COBERTURA
	}
	
	@Override
	public DatoIncisoCotAuto obtieneDatoIncisoCotAuto(CotizacionDTO cotizacion,
			CoberturaCotizacionDTO cobertura, BigDecimal numeroInciso,
			String descripcion) {
		return obtieneDatoIncisoCotAuto(cotizacion, cobertura, numeroInciso, null, descripcion, DatoIncisoNivel.COBERTURA);
	}
	
	@Override
	public DatoIncisoCotAuto obtieneDatoIncisoCotAutoSubRamo(CotizacionDTO cotizacion,
			CoberturaCotizacionDTO cobertura, BigDecimal numeroInciso,
			ConfiguracionDatoInciso.DescripcionEtiqueta descripcionEtiqueta, String descripcion) {
		return obtieneDatoIncisoCotAuto(cotizacion, cobertura, numeroInciso, descripcionEtiqueta, descripcion, DatoIncisoNivel.SUBRAMO);
	}
	
	@Override
	public DatoIncisoCotAuto obtieneDatoIncisoCotAutoCobertura(
			CotizacionDTO cotizacion, CoberturaCotizacionDTO cobertura,
			BigDecimal numeroInciso, ConfiguracionDatoInciso.DescripcionEtiqueta descripcionEtiqueta,
			String descripcion) {
		return obtieneDatoIncisoCotAuto(cotizacion, cobertura, numeroInciso, descripcionEtiqueta, descripcion, DatoIncisoNivel.COBERTURA);
	}
	
	
	private DatoIncisoCotAuto obtieneDatoIncisoCotAuto(CotizacionDTO cotizacion,
			CoberturaCotizacionDTO cobertura, BigDecimal numeroInciso,
			ConfiguracionDatoInciso.DescripcionEtiqueta descripcionEtiqueta, String descripcion, DatoIncisoNivel datoIncisoNivel) {
		
		if (datoIncisoNivel == null) {
			throw new IllegalArgumentException();
		}

		if(descripcion == null){
			return null;
		}
		//List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList();
		List<RamoTipoPolizaDTO> ramos =  ramoTipoPolizaFacadeRemote.findByProperty("tipoPolizaDTO.idToTipoPoliza", cotizacion.getTipoPolizaDTO().getIdToTipoPoliza());
		DatoIncisoCotAuto datoIncisoCotAuto = null;

		for (RamoTipoPolizaDTO ramo : ramos) {
			BigDecimal idSubRamo = cobertura.getIdTcSubramo();
			// Busca configuracion de cobertura
			ConfiguracionDatoIncisoId id = new ConfiguracionDatoIncisoId();
			id.setClaveDetalle((short) 0);
			id.setIdDato(new BigDecimal(10));
			id.setIdTcRamo(ramo.getId().getIdtcramo());
			if (datoIncisoNivel.equals(DatoIncisoNivel.SUBRAMO)) {
				id.setIdTcSubRamo(idSubRamo);
				id.setIdToCobertura(BigDecimal.ZERO);
			} else if (datoIncisoNivel.equals(DatoIncisoNivel.COBERTURA)) {
				id.setIdTcSubRamo(idSubRamo);
				id.setIdToCobertura(cobertura.getId().getIdToCobertura());
			} else {
				throw new IllegalArgumentException();
			}
			ConfiguracionDatoInciso configuracion = configuracionDatoIncisoDao
					.findById(id);
			//Revisa que configuracion no este nulo y checa que la descripcionEtiqueta
			//coincida con el parametro que se envio siempre y cuando este no venga nulo.
			boolean encontrado = configuracion != null
					&& (descripcionEtiqueta == null || configuracion
							.getDescripcionEtiqueta().toUpperCase()
							.equals(descripcionEtiqueta.toString().toUpperCase()));
			if (encontrado) {
				datoIncisoCotAuto = datoIncisoCotAutoService
						.getDatoIncisoByConfiguracion(
								cotizacion.getIdToCotizacion(), cobertura
										.getCoberturaSeccionDTO()
										.getSeccionDTO().getIdToSeccion(),
								numeroInciso, id.getIdTcRamo(),
								id.getIdTcSubRamo(), id.getIdToCobertura(),
								id.getClaveDetalle(), id.getIdDato());

				if (datoIncisoCotAuto != null) {
					datoIncisoCotAuto.setValor(descripcion);
				} else {
					datoIncisoCotAuto = new DatoIncisoCotAuto();
					datoIncisoCotAuto.setClaveDetalle(new BigDecimal(id
							.getClaveDetalle()));
					datoIncisoCotAuto.setIdDato(id.getIdDato());
					datoIncisoCotAuto.setIdTcRamo(id.getIdTcRamo());
					datoIncisoCotAuto.setIdTcSubRamo(id.getIdTcSubRamo());
					datoIncisoCotAuto.setIdToCobertura(id.getIdToCobertura());
					datoIncisoCotAuto.setIdToCotizacion(cotizacion
							.getIdToCotizacion());
					datoIncisoCotAuto.setIdToSeccion(cobertura
							.getCoberturaSeccionDTO().getSeccionDTO()
							.getIdToSeccion());
					datoIncisoCotAuto.setNumeroInciso(numeroInciso);
					datoIncisoCotAuto.setValor(descripcion);
				}
				break;
			}
		}
		return datoIncisoCotAuto;
	}
	
	
	@Override
	public DatoIncisoCotAuto obtieneDatoIncisoCotAutoDelInciso(CotizacionDTO cotizacion,
			CoberturaCotizacionDTO cobertura, BigDecimal numeroInciso) {

		List<RamoTipoPolizaDTO> ramos =  ramoTipoPolizaFacadeRemote.findByProperty("tipoPolizaDTO.idToTipoPoliza", cotizacion.getTipoPolizaDTO().getIdToTipoPoliza());
		DatoIncisoCotAuto datoIncisoCotAuto = null;

		for (RamoTipoPolizaDTO ramo : ramos) {
			BigDecimal idSubRamo = cobertura.getIdTcSubramo();
			// Busca configuracion de cobertura
			ConfiguracionDatoIncisoId id = new ConfiguracionDatoIncisoId();
			id.setClaveDetalle((short) 0);
			id.setIdDato(new BigDecimal(10));
			id.setIdTcRamo(ramo.getId().getIdtcramo());
			id.setIdTcSubRamo(idSubRamo);
			id.setIdToCobertura(cobertura.getId().getIdToCobertura());

			ConfiguracionDatoInciso configuracion = configuracionDatoIncisoDao
					.findById(id);
			if (configuracion != null) {
				datoIncisoCotAuto = datoIncisoCotAutoService
						.getDatoIncisoByConfiguracion(
								cotizacion.getIdToCotizacion(), cobertura
										.getSeccionCotizacionDTO()
										.getSeccionDTO().getIdToSeccion(),
								numeroInciso, id.getIdTcRamo(),
								id.getIdTcSubRamo(), id.getIdToCobertura(),
								id.getClaveDetalle(), id.getIdDato());

				break;
			}
		}
		return datoIncisoCotAuto;
	}
	
	@Override
	public List<CoberturaCotizacionDTO> obtieneCoberturasContratadas(IncisoCotizacionDTO inciso){
		return coberturaService.getCoberturas(inciso, true);
	}
	
	@Override
	public List<CargaMasivaAutoCot> obtieneCargasMasivasPendientes(){
		return entidadService.findByProperty(CargaMasivaAutoCot.class, "claveEstatus", CargaMasivaAutoCot.ESTATUS_PENDIENTE);
	}
	
	
	@Override
	public CargaMasivaIndividualAutoCot guardaCargaMasivaIndividual(BigDecimal idToNegocio,
			BigDecimal idToNegProducto, BigDecimal idToNegTipoPoliza, 
			ControlArchivoDTO controlArchivo, String usuario, Short tipoCarga, Short claveTipo) {
		CargaMasivaIndividualAutoCot cargaMasiva = null;
		if (controlArchivo != null) {
			cargaMasiva = new CargaMasivaIndividualAutoCot();
			cargaMasiva.setControlArchivo(controlArchivo);
			cargaMasiva.setIdToControlArchivo(controlArchivo
					.getIdToControlArchivo());
			cargaMasiva.setIdNegocio(idToNegocio);
			cargaMasiva.setIdToNegProducto(idToNegProducto);
			cargaMasiva.setIdToNegTipoPoliza(idToNegTipoPoliza);
			cargaMasiva.setClaveTipo(claveTipo);
			cargaMasiva.setFechaCreacion(new Date());
			Short uno = 1;
			Short dos = 2;
			if(tipoCarga.equals(uno)){
				cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_EN_PROCESO);
			}
			if(tipoCarga.equals(dos)){
				cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_PENDIENTE);
			}
			cargaMasiva.setCodigoUsuarioCreacion(usuario);

			BigDecimal idCargaMasiva = (BigDecimal) entidadService
					.saveAndGetId(cargaMasiva);
			cargaMasiva.setIdToCargaMasivaIndAutoCot(idCargaMasiva);
		}
		return cargaMasiva;
	}
	
	@Override
	public AutoExpediblesAutoCot guardaAutoExpedibles(BigDecimal idToNegocio,
			BigDecimal idToNegProducto, BigDecimal idToNegTipoPoliza, 
			ControlArchivoDTO controlArchivo, String usuario, Short tipoCarga, Short claveTipo) {
		AutoExpediblesAutoCot cargaMasiva = null;
		if (controlArchivo != null) {
			cargaMasiva = new AutoExpediblesAutoCot();
			cargaMasiva.setControlArchivo(controlArchivo);
			cargaMasiva.setIdToControlArchivo(controlArchivo
					.getIdToControlArchivo());
			cargaMasiva.setIdNegocio(idToNegocio);
			cargaMasiva.setIdToNegProducto(idToNegProducto);
			cargaMasiva.setIdToNegTipoPoliza(idToNegTipoPoliza);
			cargaMasiva.setClaveTipo(claveTipo);
			cargaMasiva.setFechaCreacion(new Date());
			Short uno = 1;
			Short dos = 2;
			if(tipoCarga.equals(uno)){
				cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_EN_PROCESO);
			}
			if(tipoCarga.equals(dos)){
				cargaMasiva.setClaveEstatus(CargaMasivaAutoCot.ESTATUS_PENDIENTE);
			}
			cargaMasiva.setCodigoUsuarioCreacion(usuario);

			BigDecimal idToAutoExpediblesAutoCot = (BigDecimal) entidadService
					.saveAndGetId(cargaMasiva);
			cargaMasiva.setIdToAutoExpediblesAutoCot(idToAutoExpediblesAutoCot);
		}
		return cargaMasiva;
	}
	
	@Override
	public void guardaDetalleCargaMasivaIndividual(CargaMasivaIndividualAutoCot cargaMasiva,
			List<DetalleCargaMasivaIndAutoCot> detalleList) {
		
		//Limpia detalle anterior
		List<DetalleCargaMasivaIndAutoCot> detalleListAnterior = entidadService.findByProperty(DetalleCargaMasivaIndAutoCot.class, "idToCargaMasivaIndAutoCot", cargaMasiva.getIdToCargaMasivaIndAutoCot());
		for (DetalleCargaMasivaIndAutoCot detalle : detalleListAnterior) {
			try{
				entidadService.remove(detalle);
			}catch(Exception e){
				
			}
		}
		
		//Guarda nuevo detalle
		for (DetalleCargaMasivaIndAutoCot detalle : detalleList) {
			if(detalle.getClaveEstatus() != null){
				detalle.setIdToDetalleCargaMasivaIndAutoCot(null);
				detalle.setIdToCargaMasivaIndAutoCot(cargaMasiva.getIdToCargaMasivaIndAutoCot());
				detalle.setClaveTipo(cargaMasiva.getClaveTipo());
				detalle.setNombreCliente(detalle.getNombreCompleto());
				detalle.setFechaVigencia(detalle.getFechaVigenciaInicio());
				if(detalle.getMensajeError() != null && detalle.getMensajeError().length() > 500){
					String detalleSplit = detalle.getMensajeError().substring(0, 490) + "...";
					detalle.setMensajeError(detalleSplit);
				}
				entidadService.save(detalle);
			}
		}
	}
	
	@Override
	public void guardaDetalleAutoExpedibles(AutoExpediblesAutoCot autoExpedibles,
			List<AutoExpediblesDetalleAutoCot> detalleList) {
		
		//Limpia detalle anterior
		List<AutoExpediblesDetalleAutoCot> detalleListAnterior = entidadService.findByProperty(AutoExpediblesDetalleAutoCot.class, "idToAutoExpediblesAutoCot", autoExpedibles.getIdToAutoExpediblesAutoCot());
		for (AutoExpediblesDetalleAutoCot detalle : detalleListAnterior) {
			try{
				entidadService.remove(detalle);
			}catch(Exception e){
				LOG.error(e);
			}
		}
		
		//Guarda nuevo detalle
		for (AutoExpediblesDetalleAutoCot detalle : detalleList) {
			if(detalle.getClaveEstatus() != null){
				detalle.setIdToDetalleAutoExpediblesAutoCot(null);
				detalle.setIdToAutoExpediblesAutoCot(autoExpedibles.getIdToAutoExpediblesAutoCot());
				detalle.setClaveTipo(autoExpedibles.getClaveTipo());
				detalle.setNombreCliente(detalle.getNombreCompleto());
				detalle.setFechaVigencia(detalle.getFechaVigenciaInicio());
				entidadService.save(detalle);
			}
		}
	}
	
	@Override
	public CotizacionDTO crearCotizacion(CotizacionDTO cotizacion){
		return cotizacionService.crearCotizacion(cotizacion,null);
	}
	
	@Override
	public FormaPagoDTO obtieneFormaPago(String descripcion){
		FormaPagoDTO formaPago = null;
		try{
		List<FormaPagoDTO> list = entidadService.findByProperty(FormaPagoDTO.class, "descripcion", descripcion);
		formaPago = list.get(0);
		}catch(Exception e){
			LOG.error(e);
		}
		return formaPago;
	}
	
	@Override
	public NegocioDerechoPoliza obtieneNegocioDerechoPoliza(BigDecimal idToNegocio, Double importe){
		NegocioDerechoPoliza item = null;
		try{
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("negocio.idToNegocio", idToNegocio.longValue());
			parametros.put("importeDerecho",importe);
			List<NegocioDerechoPoliza> list = entidadService.findByProperties(NegocioDerechoPoliza.class, parametros);
			if(list != null && !list.isEmpty()){
				item = list.get(0);
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return item;
	}
	
	@Override
	public CotizacionDTO guardarCotizacion(CotizacionDTO cotizacion){
		return cotizacionService.guardarCotizacion(cotizacion);
	}
	
	@Override
	public ResumenCostosDTO calculaCotizacion(CotizacionDTO cotizacion){
		return calculoService.obtenerResumenCotizacion(cotizacion, false);
	}
	
	@Override
	public void calcularTodosLosIncisos(CotizacionDTO cotizacion){
		cotizacionService.calcularTodosLosIncisos(cotizacion.getIdToCotizacion());
	}
	
	@Override
	public TerminarCotizacionDTO terminaCotizacion(CotizacionDTO cotizacion){
		TerminarCotizacionDTO terminarCotizacionDTO = cotizacionService.terminarCotizacion(cotizacion.getIdToCotizacion(), false);		
		return terminarCotizacionDTO;
	}
	
	@Override
	public IncisoCotizacionDTO calculoInciso(IncisoCotizacionDTO incisoCotizacion){
		return calculoService.calcular(incisoCotizacion);
	}
	
	@Override
	public void igualacionInciso(BigDecimal idToCotizacion, BigDecimal numeroInciso, Double primaAIgualar){
		calculoService.igualarPrimaInciso(idToCotizacion, numeroInciso, primaAIgualar);
	}
	
	@Override
	public Long solicitudAutorizacion(List<ExcepcionSuscripcionReporteDTO> excepcionesList, BigDecimal idToCotizacion, Long usuarioId){
		Long solicitudId = autorizacionService.guardarSolicitudDeAutorizacion(idToCotizacion, "", 
				usuarioId, usuarioId,  "A", excepcionesList);
		return solicitudId;
	}
	
	/* (non-Javadoc)
	 * Lamentablemente por la culpa de stored procedures como el de emisiÃ³n MIDAS.pkgAUT_Generales.spAUT_EmitePoliza (PkgAutGeneralesDaoImpl.emitirCotizacion)
	 * que contienen commits y rollback se ha empezado a tener que poner NOT_SUPPORTED a los metodos, una razÃ³n es porque empieza ha haber bloqueos.
	 * Esto tiene la desventaja de que como no manejamos una sola unida de trabajo cada operacion de la emision maneja su propia transaccion ocasionando que
	 * no sea posible realizar un rollback o un commit de toda la emisiÃ³n. Se debe buscar corregir esto pero para poder realizarlo se debe partir eliminando
	 * los commits y rollbacks de estos stored procedures y despues ir quitando los NOT_SUPPORTED y no especificar ninguno para que tome el default que es
	 * el REQUIRED.
	 * @see mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService#emitirCotizacion(mx.com.afirme.midas.cotizacion.CotizacionDTO)
	 */
	@Override	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, String> emitirCotizacion(CotizacionDTO cotizacion){
		
		Map<String, String> mensajeEmision = new HashMap<String, String>();
		
		TerminarCotizacionDTO validacion = cotizacionService.validarEmisionCotizacion(cotizacion.getIdToCotizacion());
		if(validacion != null){
			if(validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_AGENTE.getEstatus().shortValue()){
				mensajeEmision.put("icono", "10");//Fallo
				mensajeEmision.put("mensaje", "No es posible actualizar el registro");
			}else if(validacion.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
				mensajeEmision.put("icono", "10");//Fallo
				mensajeEmision.put("mensaje", validacion.getMensajeError());
			}else{
				mensajeEmision.put("icono", "10");//Fallo
				mensajeEmision.put("mensaje", validacion.getMensajeError());	
			}
		}else{
			mensajeEmision = emisionService.emitir(cotizacion, false);
		}
		return mensajeEmision;
	}
	
	@Override
	public List<CargaMasivaIndividualAutoCot> obtieneCargasMasivasIndividualesPendientes(){
		return entidadService.findByProperty(CargaMasivaIndividualAutoCot.class, "claveEstatus", CargaMasivaIndividualAutoCot.ESTATUS_PENDIENTE);
	}
	
	@Override
	public List<AutoExpediblesAutoCot> obtieneAutoExpediblesPendientes(){
		return entidadService.findByProperty(AutoExpediblesAutoCot.class, "claveEstatus", AutoExpediblesAutoCot.ESTATUS_PENDIENTE);
	}
	
	@Override
	public void eliminaCotizacionError(CotizacionDTO cotizacion){
		try{
			try{
				List<ComisionCotizacionDTO> comisionList = entidadService.findByProperty(ComisionCotizacionDTO.class, "cotizacionDTO.idToCotizacion", cotizacion.getIdToCotizacion());
				if(comisionList != null && !comisionList.isEmpty()){
					for(ComisionCotizacionDTO comision : comisionList){
						entidadService.remove(comision);
					}
				}
			}catch(Exception e){
				
			}
			try{
				List<DocAnexoCotDTO> comisionList = entidadService.findByProperty(DocAnexoCotDTO.class, "cotizacionDTO.idToCotizacion", cotizacion.getIdToCotizacion());
				if(comisionList != null && !comisionList.isEmpty()){
					for(DocAnexoCotDTO comision : comisionList){
						entidadService.remove(comision);
					}
				}
			}catch(Exception e){
				
			}
			
			//Elimina inciso de la cotizacion
			try{
				IncisoCotizacionDTO inciso = entidadService.findById(
						IncisoCotizacionDTO.class, new IncisoCotizacionId(cotizacion.getIdToCotizacion(), BigDecimal.ONE));
				if (inciso != null){
					incisoService.borrarInciso(cotizacion.getIdToCotizacion(), BigDecimal.ONE);
				}
			}catch(Exception e){
			}
			
			CotizacionDTO cotizacionElimina = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
			entidadService.remove(cotizacionElimina);
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Override
	public List<NegocioSeccion> getSeccionListByCotizacion(CotizacionDTO cotizacion){
		return negocioSeccionService.getSeccionListByCotizacion(cotizacion);
	}
	
	@Override
	public SeccionDTO getSeccionDTOById(NegocioSeccion negocioSeccion){
		return entidadService.findById(SeccionDTO.class, negocioSeccion.getSeccionDTO().getIdToSeccion());
	}
	
	@Override
	public List<NegocioTipoUso> getNegocioTipoUsoListByNegSeccion(NegocioSeccion negocioSeccion){
		
		List<NegocioTipoUso> negocioTipoUsoList = new ArrayList<NegocioTipoUso>(1);
		List<TipoUsoVehiculoDTO> list = negocioTipoUsoDao.getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(negocioSeccion.getIdToNegSeccion(), negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
		if(list == null || list.isEmpty()){
			list = tipoUsoVehiculoFacadeRemote.findByProperty("idTcTipoVehiculo", negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());
		}
		if(list != null && !list.isEmpty()){
			for(TipoUsoVehiculoDTO tipoUso: list){
				NegocioTipoUso negocioTipoUso = new NegocioTipoUso();
				negocioTipoUso.setTipoUsoVehiculoDTO(tipoUso);
				negocioTipoUsoList.add(negocioTipoUso);
			}
		}
		//return entidadService.findByProperty(NegocioTipoUso.class, "negocioSeccion.idToNegSeccion", negocioSeccion.getIdToNegSeccion());
		return negocioTipoUsoList;
	}
	
	@Override
	public List<NegocioPaqueteSeccion> getNegocioPaqueteSeccionByNegSeccion(NegocioSeccion negocioSeccion){
		return entidadService.findByProperty(NegocioPaqueteSeccion.class,"negocioSeccion.idToNegSeccion",negocioSeccion.getIdToNegSeccion());
	}
	
	@Override
	public CotizacionDTO obtieneCotizacion(CargaMasivaAutoCot carga){
		return entidadService.findById(CotizacionDTO.class, carga.getIdToCotizacion());
	}
	
	@Override
	public void guardaCargaMasivaAutoCot(CargaMasivaAutoCot carga){
		entidadService.save(carga);
	}
	
	@Override
	public List<NegocioDerechoPoliza> getNegocioDerechoPolizaByNegTipoPoliza(NegocioTipoPoliza negocioTipoPoliza){
		return entidadService.findByProperty(NegocioDerechoPoliza.class, "negocio.idToNegocio", negocioTipoPoliza.getNegocioProducto().getNegocio().getIdToNegocio());
	}
	
	@Override
	public List<NegocioFormaPago> getNegocioFormaPagoByNegTipoPoliza(NegocioTipoPoliza negocioTipoPoliza){
		return entidadService.findByProperty(NegocioFormaPago.class, "negocioProducto.idToNegProducto", negocioTipoPoliza.getNegocioProducto().getIdToNegProducto());
	}
	
	@Override
	public List<NegocioSeccion> getNegocioSeccionByNegTipoPoliza(NegocioTipoPoliza negocioTipoPoliza){
		return negocioSeccionDao.listarNegSeccionPorIdNegTipoPoliza(negocioTipoPoliza.getIdToNegTipoPoliza());
	}
	
	@Override
	public List<RegistroFuerzaDeVentaDTO> getCentroEmisores(){
		return centroEmisorService.listarCentrosEmisores();
	}
	
	@Override
	public List<Ejecutivo> getEjecutivos(){
		return ejecutivoService.findAllEjecutivoResponsable();
	}
	
	@Override
	public CotizacionDTO getCotizacionByDetalleCargaMasivaIndAutoCot(BigDecimal idToCotizacion){
		return entidadService.findById(CotizacionDTO.class, idToCotizacion);
	}
	
	@Override
	public PolizaDTO getPolizaByStringId(String idPoliza){
		return entidadService.findById(PolizaDTO.class, BigDecimal.valueOf(Long.valueOf(idPoliza)));
	}
	
	@Override
	public PolizaDTO getPolizaByCotizacion(BigDecimal idCotizacion){
		PolizaDTO poliza = null;
		try{
			List<PolizaDTO> list = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", idCotizacion);		
			if(list != null && !list.isEmpty()){
				poliza = list.get(0);
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return poliza;
	}
	
	@Override
	public NegocioTipoPoliza getNegocioTipoPolizaByCarga(BigDecimal idToNegTipoPoliza){
		return entidadService.findById(NegocioTipoPoliza.class,idToNegTipoPoliza);
	}
	
	@Override
	public Usuario getUsuarioByCarga(String codigoUsuarioCreacion){
		Usuario usuario = null;
		try{
			usuario = usuarioService.buscarUsuarioPorNombreUsuario(codigoUsuarioCreacion);
		}catch(Exception e){
		}
		if(usuario == null || usuario.getNombreUsuario() == null || usuario.getNombreUsuario().isEmpty()){
			usuario = new Usuario();
			usuario.setId(1);
			usuario.setNombreUsuario("SYSTEM");
		}
		return usuario;
	}
	
	@Override
	public void setUsuarioActual(Usuario usuario){
		usuarioService.setUsuarioActual(usuario);
	}
	
	@Override
	public BigDecimal guardaCargaMasivaIndividualAutoCot(CargaMasivaIndividualAutoCot carga){
		BigDecimal id = (BigDecimal) entidadService.saveAndGetId(carga);
		return id;
	}
	
	@Override
	public BigDecimal guardaAutoExpediblesAutoCot(AutoExpediblesAutoCot autoExpedibles){
		BigDecimal id = (BigDecimal) entidadService.saveAndGetId(autoExpedibles);
		return id;
	}
	
	@Override
	public Agente validaAgente(Long claveAgente, String claveNegocio, Long idToNegocio){
		if (claveAgente == null || idToNegocio == null) {
			return null;
		}
		
		Agente agente = null;	
		List<Agente> agenteList = entidadService.findByProperty(Agente.class, "idAgente", claveAgente);
		
		if(agenteList == null || agenteList.isEmpty()){
			return null;
		}
		
		agente = agenteList.get(0);
		if(!agente.getTipoSituacion().getIdRegistro().equals(Agente.ID_REGISTRO_AUTORIZADO)){
			return null;
		}
		
		if(agente.getFechaVencimientoCedula() != null && agente.getFechaVencimientoCedula().before(new Date())){
			return null;
		}
		
		Negocio negocio = negocioService.findById(idToNegocio);
		
		if (negocio == null) {
			return null;
		}
		
		if (!negocioService.isAgenteTienePermitidoNegocio(agente, negocio)) {
			return null;
		}
		return agente;
	}
	
	@Override
	public List<ClienteGenericoDTO> validaClienteContratante(DetalleCargaMasivaIndAutoCot detalle){
		try{
			ClienteGenericoDTO filtroCliente = new ClienteGenericoDTO();
			filtroCliente.setClaveTipoPersona(detalle.getTipoPersonaCliente());
			Short uno = 1;
			if(filtroCliente.getClaveTipoPersona().equals(uno)){
				filtroCliente.setNombre(detalle.getNombreORazonSocial());
				filtroCliente.setApellidoPaterno(detalle.getApellidoPaternoCliente());
				filtroCliente.setApellidoMaterno(detalle.getApellidoMaternoCliente());
			}else{
				filtroCliente.setRazonSocial(detalle.getNombreORazonSocial());
			}
			filtroCliente.setCodigoRFC(detalle.getRfcCliente());
			if(detalle.getIdCliente() != null && detalle.getIdCliente().compareTo(BigDecimal.ZERO) > 0){
				filtroCliente.setIdCliente(detalle.getIdCliente());
			}
			return clienteFacade.findByIdRFC(filtroCliente, null);
		}catch(Exception e){
			LOG.error(e);
		}
		return null;
	}
	
	@Override
	public ClienteGenericoDTO validaClienteContratante(AutoExpediblesDetalleAutoCot detalle){
		ClienteGenericoDTO cliente = null;
		try{
			ClienteGenericoDTO filtroCliente = new ClienteGenericoDTO();
			filtroCliente.setClaveTipoPersona(detalle.getTipoPersonaCliente());
			Short uno = 1;
			if(filtroCliente.getClaveTipoPersona().equals(uno)){
				filtroCliente.setNombre(detalle.getNombreORazonSocial());
				filtroCliente.setApellidoPaterno(detalle.getApellidoPaternoCliente());
				filtroCliente.setApellidoMaterno(detalle.getApellidoMaternoCliente());
			}else{
				filtroCliente.setRazonSocial(detalle.getNombreORazonSocial());
			}
			filtroCliente.setCodigoRFC(detalle.getRfcCliente());
			if(detalle.getIdCliente() != null && detalle.getIdCliente().compareTo(BigDecimal.ZERO) > 0){
				filtroCliente.setIdCliente(detalle.getIdCliente());
			}
			List<ClienteGenericoDTO> listaClientes= clienteFacade.findByFilters(filtroCliente, null);
			if(listaClientes != null && !listaClientes.isEmpty()){
				if(listaClientes.size() == 1){
					cliente = listaClientes.get(0);
				}else{
					cliente = null;
				}
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return cliente;
	}
	
	@Override
	public Boolean validaModeloVehiculo(BigDecimal idMoneda, String claveEstilo, BigDecimal idToNegSeccion, 
			Short modeloVehiculo){
		Boolean valido = false;
		
		if(idMoneda != null && claveEstilo != null && idToNegSeccion != null){
			if(!claveEstilo.equals("")){				
				NegocioSeccion negocioSeccion = negocioSeccionDao.findById(idToNegSeccion);
				SeccionDTO seccion = entidadService.findById(SeccionDTO.class, negocioSeccion.getSeccionDTO().getIdToSeccion());
				
				EstiloVehiculoId id = new EstiloVehiculoId();
				id.valueOf(claveEstilo);
				
				ModeloVehiculoId modeloVehiculoId = new ModeloVehiculoId();
				modeloVehiculoId.setClaveEstilo(id.getClaveEstilo());
				modeloVehiculoId.setIdVersionCarga(id.getIdVersionCarga());
				modeloVehiculoId.setIdMoneda(idMoneda);
				modeloVehiculoId.setClaveTipoBien(seccion.getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien());
				modeloVehiculoId.setModeloVehiculo(modeloVehiculo);
								
				ModeloVehiculoDTO modeloVehiculoDTO = entidadService.findById(ModeloVehiculoDTO.class, modeloVehiculoId);
				if (modeloVehiculoDTO != null) {
					valido = true;
				}				
			}
		}
		
		return valido;
	}
	
	@Override
	public Boolean validaEstadoMunicipio(Long idToNegocio, String estadoId, String municipioId){
		boolean valido = true;
//		NegocioMunicipio negocioMunicipio = municipioService.findByNegocioAndEstadoAndMunicipio(idToNegocio, estadoId, municipioId);
//		valido = negocioMunicipio != null;
		//Actualmente esta validaciÃ³n siempre regresa true pero cuando se desee validar realmente debe descomentarse las lineas anteriores.
		return valido;
	}

	@Override
	public List<DetalleCargaMasivaIndAutoCot> getCargaMasivaDetalleIndividualList(BigDecimal idToCargaMasivaIndAutoCot){
		List<DetalleCargaMasivaIndAutoCot> cargaMasivaDetalleIndividualList = new ArrayList<DetalleCargaMasivaIndAutoCot>(1);
		cargaMasivaDetalleIndividualList = entidadService.findByProperty(DetalleCargaMasivaIndAutoCot.class, "idToCargaMasivaIndAutoCot", idToCargaMasivaIndAutoCot);
		if(cargaMasivaDetalleIndividualList != null && !cargaMasivaDetalleIndividualList.isEmpty()){
			Collections.sort(cargaMasivaDetalleIndividualList, 
					new Comparator<DetalleCargaMasivaIndAutoCot>() {				
						public int compare(DetalleCargaMasivaIndAutoCot n1, DetalleCargaMasivaIndAutoCot n2){
							return n1.getIdToDetalleCargaMasivaIndAutoCot().compareTo(n2.getIdToDetalleCargaMasivaIndAutoCot());
						}
					});
			for(DetalleCargaMasivaIndAutoCot detalle : cargaMasivaDetalleIndividualList){
				if(detalle.getIdToPoliza() != null){
					PolizaDTO poliza = entidadService.findById(PolizaDTO.class, detalle.getIdToPoliza());
					detalle.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
				}
			}
		}
		return cargaMasivaDetalleIndividualList;
	}
	
	@Override
	public NegocioSeccion getNegocioSeccionById(BigDecimal negocioSeccionId){
		return entidadService.findById(NegocioSeccion.class, negocioSeccionId);
	}
	
	@Override
	public Double getPctePagoFraccionado(Integer idFormaPago, Short idMoneda) {
		return listadoService.getPctePagoFraccionado(idFormaPago, idMoneda);
	}
	
	@Override
	public List<AutoExpediblesDetalleAutoCot> getAutoExpediblesDetalleList(BigDecimal idToAutoExpediblesAutoCot){
		List<AutoExpediblesDetalleAutoCot> autoExpediblesDetalleList = new ArrayList<AutoExpediblesDetalleAutoCot>(1);
		autoExpediblesDetalleList = entidadService.findByProperty(AutoExpediblesDetalleAutoCot.class, "idToAutoExpediblesAutoCot", idToAutoExpediblesAutoCot);
		if(autoExpediblesDetalleList != null && !autoExpediblesDetalleList.isEmpty()){
			Collections.sort(autoExpediblesDetalleList, 
					new Comparator<AutoExpediblesDetalleAutoCot>() {				
						public int compare(AutoExpediblesDetalleAutoCot n1, AutoExpediblesDetalleAutoCot n2){
							return n1.getIdToDetalleAutoExpediblesAutoCot().compareTo(n2.getIdToDetalleAutoExpediblesAutoCot());
						}
					});
			for(AutoExpediblesDetalleAutoCot detalle : autoExpediblesDetalleList){
				if(detalle.getIdToPoliza() != null){
					PolizaDTO poliza = entidadService.findById(PolizaDTO.class, detalle.getIdToPoliza());
					detalle.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
				}
			}
		}
		return autoExpediblesDetalleList;
	}
	
	@Override
	public void igualarPrima(BigDecimal idToCotizacion, Double primaTotalAIgualar){
		calculoService.igualarPrima(idToCotizacion, primaTotalAIgualar, false);
	}
	
	@Override
	public List<IncisoCotizacionDTO> obtieneIncisos(BigDecimal idToCotizacion){
		List<IncisoCotizacionDTO> incisos = new ArrayList<IncisoCotizacionDTO>(1);
		incisos = incisoService.findByCotizacionId(idToCotizacion);
		return incisos;
	}
	
	@Override
	public String getUploadFolder(){
		return sistemaContext.getUploadFolder();
	}
	
	@Override
	public String eliminarIncisosCreados(List<IncisoCotizacionDTO> incisos, BigDecimal idCotizacion){
		String mensaje = null;
		if(incisos != null && !incisos.isEmpty()){
			Iterator<IncisoCotizacionDTO> iterator = incisos.iterator();
			while(iterator.hasNext())
			{
				IncisoCotizacionDTO inciso = iterator.next();
				IncisoCotizacionId filtroId = new IncisoCotizacionId();
				filtroId.setIdToCotizacion(idCotizacion);
				filtroId.setNumeroInciso(inciso.getId().getNumeroInciso());
				
				try{
					inciso = entidadService.findById(IncisoCotizacionDTO.class, filtroId);
					
					try{
						calculoService.eliminarDescuentos(inciso);
						calculoService.eliminarRecargos(inciso);
						//Elimina datos de riesgo
						incisoService.eliminarDatosRiesgo(inciso);
					}catch(Exception e){
					}
					
					if(!inciso.getSeccionCotizacion().getCoberturaCotizacionLista().isEmpty()){
						entidadService.remove(inciso);
						iterator.remove(); // works correctly
					}
				}catch(Exception ex){
					mensaje = "ERROR AL ELIMINAR";
				}		
			}
		}
		return mensaje;
	}
	
	@Override
	public MedioPagoDTO obtieneMedioPago(String descripcion){
		MedioPagoDTO medioPago = null;
		try{
			List<MedioPagoDTO> medioPagoList = medioPagoFacadeRemote.findByProperty("descripcion", descripcion);
			if(medioPagoList != null && !medioPagoList.isEmpty()){
				medioPago = medioPagoList.get(0);
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return medioPago;
	}

	@Override
	public List<CatalogoValorFijoDTO> getTipoCargas() {
		return catalogoValorFijoFacade.findByProperty("id.idGrupoValores", CatalogoValorFijoDTO.IDGRUPO_TIPO_CARGA);
	}

	@Override
	public CatalogoValorFijoDTO obtieneTipoCargaPorDescripcion(String descripcion) {
		return catalogoValorFijoFacade.findByIdGrupoValoresAndDescripcion(CatalogoValorFijoDTO.IDGRUPO_TIPO_CARGA, descripcion);
	}
	
	
	/**
	 * Agrega el error a la lista de errores en caso de que el objeto sea nulo o vació. 
	 * Regresa true si el objeto es valido.
	 */
	@Override
	public boolean validaNoNulo(Object obj, String nombreCampo, BigDecimal numeroInciso,
			String lineaDeNegocio, List<LogErroresCargaMasivaDTO> errores) {
		boolean vacio = false;
		if (obj == null) {
			vacio = true;
		} else {
			if (obj instanceof String) {
				String str = (String) obj;
				vacio = StringUtils.isBlank(str);
			}
		}
		if (vacio) {
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(numeroInciso);
			error.setNombreSeccion(lineaDeNegocio);
			error.setNombreCampoError(nombreCampo);
			error.setRecomendaciones("Campo vació");
			errores.add(error);
		}
		return !vacio;
	}
	
	/**
	 * Se utiliza para validar que el objeto no sea nulo pero a diferencia de validaNoNulo este regresa otro mensaje de error.
	 * Este metodo se utiliza cuando necesitamos obtener un valor de un catalogo pero este no se pudo encontrar debido a que
	 * el valor dado es invalido.
	 * Regresa true si el objeto es valido.
	 */
	@Override
	public boolean validaNoNulo2(Object obj, String nombreCampo, BigDecimal numeroInciso,
			String lineaDeNegocio, List<LogErroresCargaMasivaDTO> errores) {
		if (obj == null) {
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(numeroInciso);
			error.setNombreSeccion(lineaDeNegocio);
			error.setNombreCampoError(nombreCampo);
			error.setRecomendaciones("Valor no valido");
			errores.add(error);
			return false;
		}
		return true;
	}
	
	public void agregaError(String descripcionError, String nombreCampo, BigDecimal numeroInciso,
			String lineaDeNegocio, List<LogErroresCargaMasivaDTO> errores) {
		LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
		error.setNumeroInciso(numeroInciso);
		error.setNombreSeccion(lineaDeNegocio);
		error.setNombreCampoError(nombreCampo);
		error.setRecomendaciones(descripcionError);
		errores.add(error);
	}
	
	/**
	 * Valida los rangos dados y agrega el error en caso de haberlo a la lista. Regresa true si es el objeto es valido.
	 * @param valorMinimo
	 * @param valorMaximo
	 * @param valor
	 * @param nombreCampo
	 * @param numeroInciso
	 * @param lineaDeNegocio
	 * @param errores
	 * @return
	 */
	@Override
	public boolean validaRangos(Number valorMinimo, Number valorMaximo, Number valor, String nombreCampo, BigDecimal numeroInciso,
			String lineaDeNegocio, List<LogErroresCargaMasivaDTO> errores){
		if (!validaRangos(valorMinimo, valorMaximo, valor)) {
			LogErroresCargaMasivaDTO error = new LogErroresCargaMasivaDTO();
			error.setNumeroInciso(numeroInciso);
			error.setNombreSeccion(lineaDeNegocio);
			error.setNombreCampoError(nombreCampo);
			error.setRecomendaciones("Valor no se encuentra dentro del rango de  " + valorMinimo + " a " + valorMaximo);
			errores.add(error);
			return false;
		}
		return true;
	}
	
	private boolean validaRangos(Number valorMinimo, Number valorMaximo, Number valor){
		if (valor == null) {
			return true;
		}
		boolean esValido = true;
		if(valor.doubleValue() < valorMinimo.doubleValue() || valor.doubleValue() > valorMaximo.doubleValue()){
			esValido = false;
		}
		return esValido;
	}
	
	/**
	 * A partir de una lista de errores genera una cadena que contiene los mensajes de errores.
	 * @param errores
	 * @return
	 */
	@Override
	public String generaMensajeError(List<LogErroresCargaMasivaDTO> errores) {
		StringBuilder mensajeError = new StringBuilder("");
		for (LogErroresCargaMasivaDTO error : errores) {
			mensajeError.append(error.getMensajeError() + "\r\n" + System.getProperty("line.separator"));
		}
		return mensajeError.toString();
	}


	@Override
	public boolean isCoberturaContratada(
			List<CoberturaCotizacionDTO> coberturaCotizacionDTOs,
			String nombreComercialCobertura) {
		return getCoberturaCotizacion(coberturaCotizacionDTOs, nombreComercialCobertura) != null;
	}


	@Override
	public CoberturaCotizacionDTO getCoberturaCotizacion(
			List<CoberturaCotizacionDTO> coberturaCotizacionDTOs,
			String nombreComercialCobertura) {
		if (coberturaCotizacionDTOs == null) {
			return null;
		}
		for (CoberturaCotizacionDTO coberturaCotizacionDTO : coberturaCotizacionDTOs) {
			LOG.info("-->nombreComercial:"+nombreComercialCobertura.toUpperCase()+" nombreCom:"+coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase());
			if (coberturaCotizacionDTO.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase().equals(nombreComercialCobertura.toUpperCase())){
				return coberturaCotizacionDTO;
			}
		}
		return null;
	}
	
	@Override
	public List<EndosoDTO> getEndosos(BigDecimal idToPoliza){
		return endosoService.findByPropertyWithDescriptions("id.idToPoliza", idToPoliza, Boolean.TRUE);
	}
	
	@Override
	public TransporteImpresionDTO imprimirPoliza(BigDecimal idToPoliza, Locale locale, DateTime validOnDT, 
			DateTime recordFromDT, Short claveTipoEndoso){
		return generarPlantillaReporte.imprimirPoliza(idToPoliza, locale, validOnDT,
				recordFromDT,true,true,true,true,true, claveTipoEndoso, false);
	}


	@Override
	public CargaMasivaCondicionesEsp guardaCargaMasivaCondiciones(BigDecimal id,
			ControlArchivoDTO controlArchivoDTO, String usuarioClave, short estatusCarga) {
	
		CargaMasivaCondicionesEsp cargaMasivaCondicionesEsp = null;
		
		if( controlArchivoDTO !=null ){
		
			cargaMasivaCondicionesEsp = new CargaMasivaCondicionesEsp();
			cargaMasivaCondicionesEsp.setCodigoUsuarioCreacion(usuarioClave);
			cargaMasivaCondicionesEsp.setIdToControlArchivo( Long.valueOf( controlArchivoDTO.getIdToControlArchivo().toString() )  );
			cargaMasivaCondicionesEsp.setIdToSolicitud( Long.valueOf( id.toString() ) );
			cargaMasivaCondicionesEsp.setClaveEstatus(estatusCarga);
			cargaMasivaCondicionesEsp.setFechaCreacion(new Date());
			cargaMasivaCondicionesEsp.setControlArchivoDTO(controlArchivoDTO);
			
			Long idCargaMasivaCondiciones = (Long) entidadService.saveAndGetId( cargaMasivaCondicionesEsp );
			cargaMasivaCondicionesEsp.setId(idCargaMasivaCondiciones);
		
		}
		
		return cargaMasivaCondicionesEsp;
		
	}


	@Override
	public void guardaDetalleCargaMasivaCondiciones(
			List<CargaMasivaCondicionesEspDet> lCargaMasivaCondicionesEspDet) {
		
		if(! lCargaMasivaCondicionesEspDet.isEmpty()){
			
			for(CargaMasivaCondicionesEspDet cmce: lCargaMasivaCondicionesEspDet){
			
				entidadService.save(cmce);
				
			}
		}
		
	}
	
	@Override
	public boolean getAplicaDescuentoNegocioPaqueteSeccion(Long idToNegPaqueteSeccion ){
		return listadoService.getAplicaDescuentoNegocioPaqueteSeccion(idToNegPaqueteSeccion);
	}

	@Override
	public NegocioEstadoDescuento findByNegocioAndEstado(Long idToNegocio, String idToEstado){
		return negocioEstadoDescuentoService.findByNegocioAndEstado(idToNegocio, idToEstado);
	}
	
	@Override
	public List<BitemporalCoberturaSeccion> mostrarCoberturas(BitemporalAutoInciso bitemporalAutoInciso, Long cotizacionContinuityId, DateTime validoEn) {
		return coberturaBitemporalService.mostrarCoberturas(bitemporalAutoInciso, cotizacionContinuityId, validoEn);
	}

	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, 
			Map<String, String> valores, Short claveFiltroRIesgo, String accion, Boolean getInProcess){
		return configuracionDatoIncisoService.getDatosRiesgo(incisoContinuityId, validoEn, 
				valores, claveFiltroRIesgo, accion, getInProcess);
	}
	
	@Override
	public void guardarDatosAdicionalesPaquete(List<ControlDinamicoRiesgoDTO> controles,Long incisoContinuityId, Date validoEn){
		Map<String, String> datosRiesgo = new HashMap<String, String>();
		for(ControlDinamicoRiesgoDTO control : controles){
			if(control.getValor() == null || control.getValor().isEmpty()){
				datosRiesgo.put(control.getId(), "0");
			}else{
				datosRiesgo.put(control.getId(), control.getValor());
			}
		}
		configuracionDatoIncisoService.guardarDatosAdicionalesPaquete(datosRiesgo, incisoContinuityId, validoEn);
	}
	
	@Override
	public void calcularIncisosEndosoAltaInciso(BitemporalCotizacion cotizacion, DateTime validoEn){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.cotizacionContinuity.id", cotizacion.getContinuity().getId());		
		List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class, 
				params, validoEn, null, true);

		for (BitemporalInciso bitemporalInciso : incisos) {
			Double primaTotalAIgualar = 0.0;
			if(bitemporalInciso != null && bitemporalInciso.getValue().getPrimaTotalAntesDeIgualacion() != null &&
					bitemporalInciso.getValue().getPrimaTotalAntesDeIgualacion() > 0){
				primaTotalAIgualar = bitemporalInciso.getValue().getPrimaTotalAntesDeIgualacion();
			}
			if (primaTotalAIgualar > 0) {
				calculoService.igualarPrimaIncisoEndoso(bitemporalInciso.getContinuity().getId(), primaTotalAIgualar,
						validoEn, (int) SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO);
			}else{
				calculoService.calcular(bitemporalInciso, validoEn, (int) SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO, false);
			}
		}
	}
	
	@Override
	public NegocioDerechoEndoso obtieneNegocioDerechoEndosoAltaInciso(BitemporalCotizacion cotizacion){
		NegocioDerechoEndoso item = null;
		try{
			Map<String, Object> params = new LinkedHashMap<String, Object>();
			params.put("negocio.idToNegocio", cotizacion.getValue().getSolicitud().getNegocio().getIdToNegocio());
			params.put("esAltaInciso", true);
			params.put("claveDefault", true);
			params.put("activo", true);
			
			List<NegocioDerechoEndoso> negocioDerechoEndosoTemp = entidadService.findByProperties(NegocioDerechoEndoso.class, params);
			
			if(negocioDerechoEndosoTemp != null && negocioDerechoEndosoTemp.size() > 0){
				item = negocioDerechoEndosoTemp.get(0);
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return item;
	}
	
	@Override
	public void guardaCotizacionEndosoMovimientos(BitemporalCotizacion cotizacion) {
		endosoService.guardaCotizacionEndosoMovimientos(cotizacion);
	}
	
}
