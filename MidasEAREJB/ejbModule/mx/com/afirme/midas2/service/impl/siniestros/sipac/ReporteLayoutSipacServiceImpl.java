package mx.com.afirme.midas2.service.impl.siniestros.sipac;

import java.io.InputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import mx.com.afirme.midas2.dao.siniestros.sipac.ReportesLayoutSipacDAO;
import mx.com.afirme.midas2.domain.amis.AmisAseguradora;
import mx.com.afirme.midas2.domain.ocra.AmisUbicacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SeccionReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.sipac.LayoutSipac;
import mx.com.afirme.midas2.domain.siniestros.sipac.LoteLayoutSipac;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSubMarcaVehiculo;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.ocra.EnvioOcraService;
import mx.com.afirme.midas2.service.siniestros.sipac.ReporteLayoutSipacService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.EnumUtil;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;


@Stateless
public class ReporteLayoutSipacServiceImpl implements ReporteLayoutSipacService, Serializable{

	private static final long serialVersionUID = -2345262066239671L;
	private static Logger log = LoggerFactory.getLogger(ReporteLayoutSipacServiceImpl.class);
	public static final String LOTEENPROCESO = "1";
	public static final String LOTECOMPLETADO = "2";
	public static final String LOTECANCELADO = "3";
	
	private static final String OTRA_CIRCUNSTANCIA = "20";
		
	@EJB 
	private transient ReportesLayoutSipacDAO reporteLayoutSipacDao;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public transient UsuarioService usuarioService;
	
	@EJB
	private transient CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private EnvioOcraService ocraService;
	
	@PersistenceContext
	protected EntityManager entityManager;
	
	
	/**
	 * Obtiene el registro SIPAC por idetificador del reporte
	 * @param idReporte
	 * @return
	 */
	private LayoutSipac buscarLayout(Long idReporte) {
		Map<String, Object> parametros = new HashMap<String, Object>();
		parametros.put("idReporte", idReporte );
		
		@SuppressWarnings("unchecked")
		List<LayoutSipac> resultados  =
			entidadService.executeQueryMultipleResult(
					"SELECT model FROM LayoutSipac model WHERE model.reporte.id = :idReporte", parametros);
		if (resultados != null && !resultados.isEmpty()) {
				return resultados.get(0);
		}
			return null;
	}
	
	private List<String> getNombreSeparado(String nombre)
	{
		//List<Integer> posiciones = new ArrayList<Integer>();
		List<String> nombreCompleto = null;
		String nombresConcat = "";
		
		
		if (nombre != null  && nombre != "")
		{
			String[] elementos = nombre.split(" ");
			
			if(elementos.length > 2)
			{
				int apellidoMaternoIndex = elementos.length -1;
				int apellidoPaternoIndex = elementos.length -2;
				
				for (int i = 0; i < apellidoPaternoIndex ; i++)
				{
					nombresConcat = nombresConcat + " " + elementos[i].toString();
				}
				nombreCompleto = new ArrayList<String>();
				nombreCompleto.add(nombresConcat);
				nombreCompleto.add(elementos[apellidoPaternoIndex].toString());
				nombreCompleto.add(elementos[apellidoMaternoIndex].toString());
				
			}
			else if (elementos.length == 2)
			{
				nombreCompleto = new ArrayList<String>();
				nombreCompleto.add(elementos[0].toString());
				nombreCompleto.add(elementos[1].toString());
				nombreCompleto.add(" ");
			}
			else if (elementos.length == 1)
			{
				nombreCompleto = new ArrayList<String>();
				nombreCompleto.add(elementos[0].toString());
				nombreCompleto.add(" ");
				nombreCompleto.add(" ");
			}
			else
			{
				nombreCompleto = new ArrayList<String>();
				nombreCompleto.add(nombre);
				nombreCompleto.add(" ");
				nombreCompleto.add(" ");
			}
		}
		
		
		return nombreCompleto;
	}
	
	
	/**
	 * Guarda la informacion del acreedor
	 * @param estimacion
	 */
	public void guardarLayoutDM(EstimacionCoberturaReporteCabina estimacion) {
		try {
			CoberturaReporteCabina coberturaReporte = estimacion.getCoberturaReporteCabina();
			IncisoReporteCabina incisoReporte = coberturaReporte.getIncisoReporteCabina();
			AutoIncisoReporteCabina autoIncisoReporte = incisoReporte.getAutoIncisoReporteCabina();
			SeccionReporteCabina seccionReporte = incisoReporte.getSeccionReporteCabina();
			ReporteCabina reporte = seccionReporte.getReporteCabina();
			
			LayoutSipac layout = this.buscarLayout(reporte.getId());
			
			if (layout == null) {
				layout = new LayoutSipac();
			}		
			
			if (!EnumUtil.equalsValue(layout.getEstatus(),  LayoutSipac.Estatus.ACEPTADO)){
				layout.setEstatus(LayoutSipac.Estatus.PENDIENTE.getValue());
				
				this.actualizaDatosReporte(layout, reporte, incisoReporte, autoIncisoReporte);		
				
				List<String> nombresSeparados = getNombreSeparado(estimacion.getNombreAfectado()); // se agrega funcionamiento para separar nombres por requerimiento de sipac 
				if (nombresSeparados != null && layout.getPersAfectado().equals(LayoutSipac.PERSONA_FISICA))
				{
					//Datos de la Estimacion DM (Afectado/Acreedor)				
					layout.setNombreAfectado(getValue(nombresSeparados.get(0),100,null));
					layout.setApPaternoAfectado(getValue(nombresSeparados.get(1),100,null));
					layout.setApMaternoAfectado(getValue(nombresSeparados.get(2),100,null));
				}
				else
				{
					layout.setNombreAfectado(getValue(estimacion.getNombreAfectado(),100,null));
					layout.setApPaternoAfectado("");
					layout.setApMaternoAfectado("");
				}
				
				if(estimacion.getEsOtraCircunstancia()!=null && estimacion.getEsOtraCircunstancia()){
					layout.setCircunstanciaAcr(OTRA_CIRCUNSTANCIA);
				} else {
					layout.setCircunstanciaAcr(getValue(estimacion.getCircunstancia(),4,null));
				}
				
				layout.setObsAcreedor(getValue(estimacion.getOtraCircunstancia(),255,null));
				//Datos del terceroDanosMat (Acreedor)
				TerceroDanosMateriales terceroDanosMat = entidadService.findById(TerceroDanosMateriales.class, estimacion.getId());
				String daniosCubiertos = getValue(terceroDanosMat.getDanosCubiertos(),252,null);
				if(daniosCubiertos == null || daniosCubiertos.isEmpty())
				{
					daniosCubiertos = "Otros";
				}
				layout.setDanosLesiones("0=" + daniosCubiertos.replace("\n",",").replace("\r",""));
				layout.setDanosPreexistentes(terceroDanosMat.getTieneDanosPreexistentes()?"1":"0");
				
				String descPrexistentes = getValue(terceroDanosMat.getDanosPreexistentes(),255,null);
				if (descPrexistentes !=  null && !descPrexistentes.isEmpty())
				{
					descPrexistentes = descPrexistentes.replace("\n",",").replace("\r","");
				}
				layout.setDescrDpreexistentes(descPrexistentes);	
				layout.setUsoVehiculoDeu("1");
				layout.setUsoVehiculoAcr("1");
				
				entidadService.save(layout);			
			}
		}catch (Exception e) {
			log.error("sipac metodo: guardarLayoutDM. clase: ReporteLayoutSipacServiceImpl."+ estimacion.getFolio()+ "error:" + e.getMessage(), e);
		}
	}
	
	/**
	 * Guarda la informacion del deudor
	 * @param estimacion
	 */
	public void guardarLayoutRCV(EstimacionCoberturaReporteCabina estimacion){
		
		try {
		
			CoberturaReporteCabina coberturaReporte = estimacion.getCoberturaReporteCabina();
			IncisoReporteCabina incisoReporte = coberturaReporte.getIncisoReporteCabina();
			AutoIncisoReporteCabina autoIncisoReporte = incisoReporte.getAutoIncisoReporteCabina();
			SeccionReporteCabina seccionReporte = incisoReporte.getSeccionReporteCabina();
			ReporteCabina reporte = seccionReporte.getReporteCabina();
			
			LayoutSipac layout = this.buscarLayout(reporte.getId());
			
			if (layout == null) {
				layout = new LayoutSipac();
			}
			
			if (!EnumUtil.equalsValue(layout.getEstatus(), LayoutSipac.Estatus.ACEPTADO)){
				layout.setEstatus(LayoutSipac.Estatus.PENDIENTE.getValue());
				this.actualizaDatosReporte(layout, reporte, incisoReporte, autoIncisoReporte);
				
				//Datos de la Estimacion RCV Deudor
				layout.setFolioOrden(getValue(estimacion.getDua(),25,1));				
				layout.setSiniestroDeudor(getValue(estimacion.getNumeroSiniestroTercero(),20,1));
				layout.setPersDeudor(getValue(estimacion.getTipoPersona(),1,null));
				
				List<String> nombresSeparados = getNombreSeparado(estimacion.getNombreAfectado()); // se agrega funcionamiento para separar nombres por requerimiento de sipac 
				if (nombresSeparados != null && layout.getPersDeudor().equals(LayoutSipac.PERSONA_FISICA))
				{
					//Datos de la Estimacion DM (Afectado/Acreedor)				
					layout.setNombreResponsable(getValue(nombresSeparados.get(0),100,null));
					layout.setApPaternoResponsable(getValue(nombresSeparados.get(1),100,null));
					layout.setApMaternoResponsable(getValue(nombresSeparados.get(2),100,null));
				}
				else
				{
					layout.setNombreResponsable(getValue(estimacion.getNombreAfectado(),100,null));
					layout.setApPaternoResponsable("");
					layout.setApMaternoResponsable("");
				}
				
				if(estimacion.getEsOtraCircunstancia()!=null && estimacion.getEsOtraCircunstancia()){
					layout.setCircunstanciaDeu(OTRA_CIRCUNSTANCIA);
				} else {
					layout.setCircunstanciaDeu(getValue(estimacion.getCircunstancia(),4,null));
				}
				
				layout.setObsDeudor(getValue(estimacion.getOtraCircunstancia(),255,null));
				if(estimacion.getCompaniaSegurosTercero()!=null){
					Map<String,Object> params = new HashMap<String, Object>();
					params.put("ciaId", estimacion.getCompaniaSegurosTercero().getId());		
					List<AmisAseguradora> amisAseguradoras = this.entidadService.findByProperties(
							AmisAseguradora.class, params);
					if(!amisAseguradoras.isEmpty()){
						layout.setCiaDeudora(getValue(amisAseguradoras.get(0).getAmisId(),3,null));
					}
				}
				
				//Datos del rcVehiculo (Deudor)
				TerceroRCVehiculo rcVehiculo = entidadService.findById(TerceroRCVehiculo.class, estimacion.getId());
				layout.setSerieDeudor(getValue(rcVehiculo.getNumeroSerie(),17,2));
				if(rcVehiculo.getModeloVehiculo()!=null){
					layout.setModeloDeudor(getValue(rcVehiculo.getModeloVehiculo().toString(),4,null));
				}		
				layout.setColorVehDeudor(getValue(rcVehiculo.getColor()!=null?rcVehiculo.getColor():"0",3, null));
				layout.setPlacaDeudor(getValue(rcVehiculo.getPlacas(),10,2));
				layout.setAjustadorDeudora(getValue(rcVehiculo.getNombreAjustador(),50,null));
				layout.setClaveAjustDeudora(getValue(rcVehiculo.getClaveAjustador(),10,1));
				layout.setTipoTransporteDeudor(getValue(rcVehiculo.getTipoTransporte(),4,null));
				
				List<CatSubMarcaVehiculo> marcaVehiculo = reporteLayoutSipacDao.findMarcaTipoTercero(rcVehiculo.getEstiloVehiculo());
				if(!marcaVehiculo.isEmpty())
				{
					layout.setTipoVehDeudor(marcaVehiculo.get(0).getId().toString());
					layout.setMarcaDeudor(marcaVehiculo.get(0).getCatMarcaVehiculo().getId().toString());
				}
				
				layout.setUsoVehiculoDeu("1"); // valores siempre van en 1
				layout.setUsoVehiculoAcr("1");
			
				entidadService.save(layout);					
			}			
		}catch (Exception e) {
			log.error("sipac metodo: guardarLayoutRCV. clase: ReporteLayoutSipacServiceImpl."+ estimacion.getFolio()+ "error:" + e.getMessage(), e);
		}
	}
	
	/**
	 * Actualiza los datos del reporte
	 * @param layout
	 * @param reporte
	 * @param incisoReporte
	 * @param autoIncisoReporte
	 */
	private void actualizaDatosReporte(LayoutSipac layout, ReporteCabina reporte,
			IncisoReporteCabina incisoReporte, AutoIncisoReporteCabina autoIncisoReporte){
		//Datos del Reporte
		layout.setReporte(reporte);
		layout.setFechaSiniestro(reporte.getFechaOcurrido());
		
		this.asignaLugarOcurrido(layout, reporte);
		
		if(reporte.getAjustador()!=null && 
				reporte.getAjustador().getPersonaMidas()!=null){
			layout.setAjustadorAcreedora(getValue(reporte.getAjustador().getPersonaMidas().getNombre(),50,null));
			layout.setClaveAjustAcreedora(getValue(reporte.getAjustador().getId().toString(),10,1));
		}
		
		this.asignaDatosAutoInciso(layout, autoIncisoReporte);
		
		//Datos del Asegurado (Afectado/Acreedor)
		if(reporte.getPoliza()!=null){
			layout.setPolizaAcreedor(getValue(reporte.getPoliza().getNumeroPolizaFormateada(),20,1));
		}
		if(incisoReporte.getNumeroInciso()!=null){
			layout.setIncisoAcreedor(getValue(incisoReporte.getNumeroInciso().toString(),5,null));
		}		
				
		Map<String, String> autoAfectadoOCRA = ocraService.convertirAutoOcra(incisoReporte);
		layout.setMarcaAfectado(getValue(autoAfectadoOCRA.get("marcaId"),4,null));
		layout.setTipoVehAfectado(getValue(autoAfectadoOCRA.get("tipoId"),4,null));
		layout.setTipoTransporteAfectado(getValue(autoAfectadoOCRA.get("transporteId"),4,null));
		
		SiniestroCabina siniestroCabina = reporte.getSiniestroCabina();
		if(siniestroCabina!=null){
			layout.setSiniestroAcreedor(getValue(siniestroCabina.getNumeroSiniestro(),20,1));
		}
	}
	
	/**
	 * Asigna el numero de siniestro
	 * @param reporte
	 * @param siniestroCabina
	 */
	public void actualizaSiniestro(ReporteCabina reporte, 
			SiniestroCabina siniestroCabina){
		try {
		
			SeccionReporteCabina seccionReporte = reporte.getSeccionReporteCabina();
			IncisoReporteCabina incisoReporte = seccionReporte.getIncisoReporteCabina();
			AutoIncisoReporteCabina autoIncisoReporte = incisoReporte.getAutoIncisoReporteCabina();		
			
			LayoutSipac layout = this.buscarLayout(reporte.getId());
			
			if (layout != null && !EnumUtil.equalsValue(layout.getEstatus(), LayoutSipac.Estatus.ACEPTADO)) {
				layout.setEstatus(LayoutSipac.Estatus.PENDIENTE.getValue());			
				this.actualizaDatosReporte(layout, reporte, incisoReporte, autoIncisoReporte);			
				layout.setSiniestroAcreedor(getValue(siniestroCabina.getNumeroSiniestro(),20,1));
				entidadService.save(layout);
			}
		}catch (Exception e) {
			log.error("sipac metodo: actualizaSiniestro. clase: ReporteLayoutSipacServiceImpl. "+ reporte.getNumeroReporte()+ " error:" + e.getMessage(), e);
		}
	}
	/**
	 * Reemplaza caracteres de acuerdo al tipo y trunca de acuerdo a longitud maxima
	 * @param value
	 * @param maxLength
	 * @param tipo
	 * @return
	 */
	private String getValue(String value, int maxLength, Integer tipo){
		if(value!=null ){
			value = StringUtils.trim(value);
			if(tipo!=null) {
				switch(tipo) {
				case 1: //Alfanumérico, solo caracteres especiales /,-,_
						value = StringUtils.replaceChars(value, "[!#$%@|¬°·~?¡()´+}{;:.,'¿] \"", "");
						break;
				case 2: //Alfanumérico no permite caracteres especiales
						value = StringUtils.replaceChars(value, "[!#$%@|¬°·~?¡()´+}{;:.,'¿] \"-/_", "");
						break;
				}
			}			
			
			if(value.length()>maxLength){
				value = value.substring(0, maxLength);
			}			
		}
		return value;
	}
	
	/**
	 * Genera Archivo TXT
	 * @param list
	 * @param lote
	 * @return
	 */
	@Override
	public InputStream generarArchivoTxt(List<LayoutSipac> list, LoteLayoutSipac lote){
		Integer contSecuencial = 0;
		try{
			for(LayoutSipac element: list){
				contSecuencial++;
				element.setNumeroSecuencial(contSecuencial.toString());
				element.setLote(lote);
				element.setEstatus(LayoutSipac.Estatus.ENVIADO.getValue());
				reporteLayoutSipacDao.updateLayout(element);
			}
		}catch(Exception e){
			log.error("Ocurrio un error al modificar el objeto Layout, metodo : generarArchivoTxt clase: ReporteLayoutSipacServiceImpl. error: " + e);
		}
		return reporteLayoutSipacDao.generarArchivoTxt(list);
	}
	
	/**
	 * Obtiene los registros de un lote determinado
	 * @param idLote
	 * @return
	 */
	@Override
	public List<LayoutSipac> findLayoutByIdLote(Long idLote){
		return reporteLayoutSipacDao.findLayoutByIdLote(idLote);		
	}
	
	/**
	 * Genera el Archivo TXT de un Lote determinado
	 * @param idLote
	 * @return
	 */
	@Override
	public InputStream generarTxtByLote(Long idLote){
		return reporteLayoutSipacDao.generarTxtByLote(idLote);
		
	}
	/**
	 * Obtiene Lotes SIPAC
	 * @return
	 */
	@Override
	public List<LoteLayoutSipac> obtenerLotesSipac(){
		return reporteLayoutSipacDao.obtenerLotesSipac();
	}
	
	/**
	 * Obtiene Lotes Pendientes
	 * @return
	 */
	@Override
	public List<LoteLayoutSipac> obtenerLotesPendientes(){
		return reporteLayoutSipacDao.obtenerLotesPendientes();
	}
	
	/**
	 * Busca Reportes pendientes
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	@Override
	public List<LayoutSipac> buscarReportesPend(Date fechaInicial, Date fechaFinal){
		return reporteLayoutSipacDao.buscarReportesPend(fechaInicial, fechaFinal);
	}
	
	/**
	 * Guarda el Lote
	 * @param fechaInicial
	 * @param fechaFinal
	 * @return
	 */
	@Override
	public LoteLayoutSipac saveLotes(Date fechaInicial, Date fechaFinal){
		LoteLayoutSipac loteSipac = new LoteLayoutSipac();
		loteSipac.setFechaCreacion(TimeUtils.now().toDate());
		loteSipac.setUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		loteSipac.setEstatus(LOTEENPROCESO);
		loteSipac.setFechaOcurridoDesde(fechaInicial);
		loteSipac.setFechaOcurridoHasta(fechaFinal);
		
		reporteLayoutSipacDao.saveLotes(loteSipac);
		
		return loteSipac;
	}
	
	/**
	 * Actualiza el Lote
	 * @param idLote
	 * @param estatusLote
	 * @return
	 */
	@Override
	public LoteLayoutSipac updateLote(Long idLote, String estatusLote){
		LoteLayoutSipac lote = reporteLayoutSipacDao.findLoteById(idLote);
		List<LayoutSipac> layout = reporteLayoutSipacDao.findLayoutByIdLote(idLote);
		
		if(!layout.isEmpty()){
			for(LayoutSipac element: layout){
				if(estatusLote.equals("1")){
					lote.setEstatus(LOTECANCELADO);
					element.setEstatus(LayoutSipac.Estatus.PENDIENTE.getValue());
					reporteLayoutSipacDao.updateLayout(element);
				}else if(estatusLote.equals("2")){
					lote.setEstatus(LOTECOMPLETADO);
					element.setEstatus(LayoutSipac.Estatus.ACEPTADO.getValue());
					reporteLayoutSipacDao.updateLayout(element);
				}else{
					lote.setEstatus(LOTEENPROCESO);
					element.setEstatus(LayoutSipac.Estatus.PENDIENTE.getValue());
					reporteLayoutSipacDao.updateLayout(element);
				}
			}
		}else{
			if(estatusLote.equals("1")){
				lote.setEstatus(LOTECANCELADO);
			}else if(estatusLote.equals("2")){
				lote.setEstatus(LOTECOMPLETADO);
			}else{
				lote.setEstatus(LOTEENPROCESO);
			}
		}
		
		reporteLayoutSipacDao.updateLote(lote);
		return lote;
	}	
	
	/**
	 * Asigna Lugar de Ocurrido
	 * @param layout
	 * @param reporte
	 */
	private void asignaLugarOcurrido(LayoutSipac layout, ReporteCabina reporte){
		LugarSiniestroMidas lugarLayout = reporte.getLugarOcurrido();
		AmisUbicacion ubicacion = ocraService.convertirUbicacionAMIS(
				lugarLayout.getPais().getId(), 
				lugarLayout.getEstado().getId(),
				lugarLayout.getCiudad().getId()
		);
		
		if(ubicacion!=null){
			layout.setPais(getValue(ubicacion.getId().getIdPais().toString(),4,null));
			layout.setEstado(getValue(ubicacion.getId().getIdEstado().toString(),4,null));
			layout.setMunicipio(getValue(ubicacion.getId().getIdMunicipio().toString(),4,null));
		}		
	}
	
	/**
	 * Actualiza Lugar de Ocurrido
	 * @param layout
	 * @param reporte
	 */
	public void actualizaLugarOcurrido(ReporteCabina reporte){
		LayoutSipac layout = this.buscarLayout(reporte.getId());
		if (layout!=null && !EnumUtil.equalsValue(layout.getEstatus(), LayoutSipac.Estatus.ACEPTADO)){
			layout.setEstatus(LayoutSipac.Estatus.PENDIENTE.getValue());
			this.asignaLugarOcurrido(layout, reporte);
			entidadService.save(layout);
		}
	}
	
	/**
	 * Asigna los datos que se guardan en la pestaña "Cierre Reporte Siniestro"
	 * @param layout
	 * @param autoIncisoReporte
	 */
	private void asignaDatosAutoInciso(LayoutSipac layout, AutoIncisoReporteCabina autoIncisoReporte){
		layout.setPolizaDeudor(getValue(autoIncisoReporte.getPolizaCia(),20,1));
		layout.setIncisoDeudor(getValue(autoIncisoReporte.getIncisoCia(),5,null));
		layout.setSiniestroDeudor(getValue(autoIncisoReporte.getSiniestroCia(),20, 1));
		layout.setOrigen(getValue(autoIncisoReporte.getOrigen(),1, null));
		layout.setFechaExpedicion(autoIncisoReporte.getFechaExpedicion());
		layout.setFechaDictamen(autoIncisoReporte.getFechaDictamen());
		layout.setFechaInicioJuicio(autoIncisoReporte.getFechaJuicio());
		
		if(autoIncisoReporte.getRfc()!=null){
			layout.setPersAfectado(autoIncisoReporte.getRfc().length()==12?LayoutSipac.PERSONA_MORAL:LayoutSipac.PERSONA_FISICA);
		}
		
		if(autoIncisoReporte.getCiaSeguros()!=null && StringUtils.isNotEmpty(autoIncisoReporte.getCiaSeguros())  ){
			Map<String,Object> params = new HashMap<String, Object>();
			params.put("ciaId", Long.parseLong(autoIncisoReporte.getCiaSeguros()));		
			List<AmisAseguradora> amisAseguradoras = this.entidadService.findByProperties(
					AmisAseguradora.class, params);
			if(!amisAseguradoras.isEmpty()){
				layout.setCiaDeudora(getValue(amisAseguradoras.get(0).getAmisId(),3,null));
			}
		}
		
		layout.setSerieAfectado(getValue(autoIncisoReporte.getNumeroSerie(),17,2));		
		layout.setColorVehAfectado(getValue(autoIncisoReporte.getColor()!=null?autoIncisoReporte.getColor():"0",3,null));
		layout.setPlacaAfectado(getValue(autoIncisoReporte.getPlaca(),10,2));	
		if(autoIncisoReporte.getModeloVehiculo()!=null){
			layout.setModeloAfectado(getValue(autoIncisoReporte.getModeloVehiculo().toString(),4,null));
		}		
	}
	
	/**
	 * Actualiza los datos que se guardan en la pestaña "Cierre Reporte Siniestro"
	 * @param idReporte
	 * @param autoIncisoReporte
	 */
	public void actualizaDatosAutoInciso(Long idReporte, AutoIncisoReporteCabina autoIncisoReporte){
		LayoutSipac layout = this.buscarLayout(idReporte);
		if (layout!=null && !EnumUtil.equalsValue(layout.getEstatus(), LayoutSipac.Estatus.ACEPTADO)){
			layout.setEstatus(LayoutSipac.Estatus.PENDIENTE.getValue());
			this.asignaDatosAutoInciso(layout, autoIncisoReporte);
			entidadService.save(layout);
		}
	}	
	
	/**
	 * Actualiza la informacion del LayoutSipac desde la consulta del Lote
	 * Si ya no hay ningun registro enviado (se actualiza el lote como completo)
	 * @param idLote
	 * @param layout
	 */
	public void save(Long idLote, LayoutSipac layout){
		LayoutSipac layoutBD = entidadService.findById(LayoutSipac.class, layout.getId());
		layoutBD.setEstatus(layout.getEstatus());
		entidadService.save(layoutBD);		

		Map<String,Object> params = new HashMap<String, Object>();
		params.put("lote.id", idLote);		
		params.put("estatus", LayoutSipac.Estatus.ENVIADO.getValue());	
		if(this.entidadService.findByProperties(
				LayoutSipac.class, params).isEmpty()){
			LoteLayoutSipac lote = reporteLayoutSipacDao.findLoteById(idLote);
			lote.setEstatus(LOTECOMPLETADO);
			reporteLayoutSipacDao.updateLote(lote);
		} 		
	}
}
