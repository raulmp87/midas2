package mx.com.afirme.midas2.service.impl.negocio.producto.formapago;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.dto.negocio.producto.formapago.RelacionesNegocioFormaPagoDTO;
import mx.com.afirme.midas2.service.negocio.producto.formapago.NegocioFormaPagoService;

@Stateless
public class NegocioFormaPagoServiceImpl implements NegocioFormaPagoService {

	@Override
	public RelacionesNegocioFormaPagoDTO getRelationLists(NegocioProducto negocioProducto) {
		RelacionesNegocioFormaPagoDTO relacionesNegocioFormaPagoDTO = new RelacionesNegocioFormaPagoDTO();
		//Listado asociadas
		List<NegocioFormaPago> negocioFormaPagoAsociadasList = entidadDao.findByProperty(NegocioFormaPago.class, "negocioProducto.idToNegProducto", negocioProducto.getIdToNegProducto());
		//Listado posibles
		ProductoDTO productoDTO = entidadDao.findById(ProductoDTO.class,negocioProducto.getProductoDTO().getIdToProducto());
		List<FormaPagoDTO> posibles =  productoDTO.getFormasPago();
		//Listado disponibles
		List<NegocioFormaPago> negocioFormaPagoDisponiblesList = new ArrayList<NegocioFormaPago>();
		
		List<FormaPagoDTO> formaPagoDTOAsociadasList = new ArrayList<FormaPagoDTO>();
		
		for(NegocioFormaPago item : negocioFormaPagoAsociadasList){
			formaPagoDTOAsociadasList.add(item.getFormaPagoDTO());
		}
		
		for(FormaPagoDTO item : posibles){
			if(!formaPagoDTOAsociadasList.contains(item)){
				NegocioFormaPago negocioFormaPago = new NegocioFormaPago();
				negocioFormaPago.setFormaPagoDTO(item);
				negocioFormaPago.setNegocioProducto(negocioProducto);
				negocioFormaPagoDisponiblesList.add(negocioFormaPago);
			}
		}
		
		relacionesNegocioFormaPagoDTO.setAsociadas(negocioFormaPagoAsociadasList);
		relacionesNegocioFormaPagoDTO.setDisponibles(negocioFormaPagoDisponiblesList);
			
		return relacionesNegocioFormaPagoDTO;
	}

	@Override
	public void relacionarNegocioFormaPago(String accion, NegocioFormaPago negocioFormaPago) {
		entidadDao.executeActionGrid(accion, negocioFormaPago);
	}
	
	private EntidadDao entidadDao;
	
	@EJB
	public void setEntidadDao(EntidadDao catalogoDato) {
		this.entidadDao = catalogoDato;
	}

}
