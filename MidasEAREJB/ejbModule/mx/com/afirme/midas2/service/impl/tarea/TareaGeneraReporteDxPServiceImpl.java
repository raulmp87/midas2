package mx.com.afirme.midas2.service.impl.tarea;

import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import mx.com.afirme.midas2.service.reportes.ReporteService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.solicitud.impresiones.GeneraPlantillaReporteSolicitudService;
import mx.com.afirme.midas2.service.tarea.TareaGeneraReporteDxPService;

@Stateless
public class TareaGeneraReporteDxPServiceImpl implements TareaGeneraReporteDxPService{
	
private static final Logger LOG = Logger.getLogger(TareaGeneraReporteDxPServiceImpl.class);

	private InputStream solicitudInputStream;
	private String     fileName;
	private String      contentType;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private ReporteService reporteService;
	
	@EJB
	private GeneraPlantillaReporteSolicitudService generaPlantillaReporteSolicitudService;
	
	@Autowired
	@Qualifier("reporteServiceEJB")
	public void setReporteService(ReporteService reporteService) {
		this.reporteService = reporteService;
	}
	
	@Autowired
	@Qualifier("generaPlantillaReporteSolicitudServiceEJB")
	public void setGeneraPlantillaReporteSolicitudService(
			GeneraPlantillaReporteSolicitudService generaPlantillaReporteSolicitudService) {
		this.generaPlantillaReporteSolicitudService = generaPlantillaReporteSolicitudService;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Schedule(minute="*/5", hour="*", dayOfWeek = "*", persistent = false)
	public void generaArchivoDxP()
	{
		 
		 try {
			 
			Integer ejecutandoDXP = reporteService.getEjecutandoGeneracionDXP();
			 
			if(ejecutandoDXP.intValue() == 0)
			{
				generaPlantillaReporteSolicitudService.creaDXPD2("D", new BigDecimal(0));
			}
			else
				LOG.info("generaArchivoDxP ... El Reporte esa en ejecución, no se genera archivo txt.");
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		 try {
			    TimeUnit.SECONDS.sleep(2);
		} catch (Exception e) {
			   e.printStackTrace();
		}		
	}
	
	public InputStream getSolicitudInputStream() {
		return solicitudInputStream;
	}

	public void setSolicitudInputStream(InputStream solicitudInputStream) {
		this.solicitudInputStream = solicitudInputStream;
	}

	public String getFileName() {
		return fileName;
	}

	public void setFileName(String fileName) {
		this.fileName = fileName;
	}

	public String getContentType() {
		return contentType;
	}

	public void setContentType(String contentType) {
		this.contentType = contentType;
	}
	
}
