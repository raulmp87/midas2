package mx.com.afirme.midas2.service.impl.Excluir;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.Excluir.ExcluirDao;
import mx.com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir;
import mx.com.afirme.midas2.service.Excluir.ExcluirService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.log4j.Logger;

@Stateless
public class ExcluirServiceImpl implements ExcluirService {

	private static final Logger LOG = Logger
			.getLogger(ExcluirServiceImpl.class);

	private ExcluirDao excluirDao;
	private EntidadService entidadService;

	/**
	 * EJB
	 * 
	 * @param entidadService
	 */
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setExcluirDao(ExcluirDao excluirDao) {
		this.excluirDao = excluirDao;
	}

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	/**
	 * Metodos Generales
	 */

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.afirme.midas2.service.Excluir.ExcluirService#saveExcluirAgente
	 * (mx.com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir)
	 */
	public void saveExcluirAgente(Excluir excluir) {
		Date ahora = new Date();
		String userName = "";
		try {
			userName = usuarioService.getUsuarioActual().getNombreUsuario();			
				excluir.setFechaAlta(ahora);
				excluir.setUsuario(userName);
				entidadService.save(excluir);
			
		} catch (Exception e) {
			LOG.info("Error " + e);
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.afirme.midas2.service.Excluir.ExcluirService#eliminarExcluir(mx
	 * .com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir)
	 */
	@Override
	public void eliminarExcluir(Excluir excluir) {
		excluirDao.eliminarExcluir(excluir);
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * mx.com.afirme.midas2.service.Excluir.ExcluirService#getListExcluirAgentes
	 * (mx.com.afirme.midas2.domain.catalogos.ExcluirAgentes.Excluir)
	 */
	@Override
	public List<Excluir> getListExcluirAgentes(Excluir excluir) {
		return excluirDao.getListExcluirAgentes(excluir);
	}

}
