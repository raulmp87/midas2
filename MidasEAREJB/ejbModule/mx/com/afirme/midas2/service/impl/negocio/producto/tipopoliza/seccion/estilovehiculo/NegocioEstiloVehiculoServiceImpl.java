package mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion.estilovehiculo;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculoDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.dto.negocio.estilovehiculo.RelacionesNegocioEstiloVehiculoDTO;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculoService;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;

@Stateless
public class NegocioEstiloVehiculoServiceImpl implements NegocioEstiloVehiculoService {

	@Override
	public List<EstiloVehiculoDTO> getListarEstiloVehiculo(
			BigDecimal idTcMarcaVehiculo, BigDecimal tipoVehiculo,
			String claveTipoBien, BigDecimal idToNegSeccion, BigDecimal idMoneda) {
		
		EstiloVehiculoDTO estilo = new EstiloVehiculoDTO();
		EstiloVehiculoId id = new EstiloVehiculoId();
		id.setClaveTipoBien(claveTipoBien);
		MarcaVehiculoDTO marca = new MarcaVehiculoDTO();
		marca.setIdTcMarcaVehiculo(idTcMarcaVehiculo);
		estilo.setMarcaVehiculoDTO(marca);
		estilo.setId(id);
		estilo.setIdTcTipoVehiculo(tipoVehiculo);
		
		NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion  = new NegocioAgrupadorTarifaSeccion ();
		negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, idMoneda);
		
		return estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupador(estilo, negocioAgrupadorTarifaSeccion, idToNegSeccion);
	}

	@Override
	public List<NegocioEstiloVehiculo> getEstiloVehiculoNoAsociados(
			NegocioSeccion negocioSeccion, BigDecimal idTcMarcaVehiculo,
			BigDecimal tipoVehiculo, BigDecimal idMoneda) {
		
		List<EstiloVehiculoDTO> posibles = this.getListarEstiloVehiculo(
				idTcMarcaVehiculo, tipoVehiculo,
				negocioSeccion.getSeccionDTO().getTipoVehiculo().getTipoBienAutosDTO().getClaveTipoBien(),
				negocioSeccion.getIdToNegSeccion(), idMoneda);

		List<NegocioEstiloVehiculo> asociados = negocioEstiloVehiculoDao
				.getEstilosVehiculo(negocioSeccion.getIdToNegSeccion());
		
		List<EstiloVehiculoDTO> estilosAsociados = new ArrayList<EstiloVehiculoDTO>();
		
		for(NegocioEstiloVehiculo item:asociados){
			estilosAsociados.add(item.getEstiloVehiculoDTO());
		}
		
		List<NegocioEstiloVehiculo> estilosPosibles = new ArrayList<NegocioEstiloVehiculo>();
		
		for(EstiloVehiculoDTO item: posibles){
			if(!estilosAsociados.contains(item)){
				NegocioEstiloVehiculo estilo = new NegocioEstiloVehiculo();
				estilo.setNegocioSeccion(negocioSeccion);
				estilo.setEstiloVehiculoDTO(item);
				estilosPosibles.add(estilo);
			}
		}
		return estilosPosibles;
	}

	@Override
	public RelacionesNegocioEstiloVehiculoDTO getRelationList(
			NegocioSeccion negocioSeccion) {
		if (negocioSeccion.getIdToNegSeccion() != null)
			negocioSeccion = entidadDao.getReference(NegocioSeccion.class,
					negocioSeccion.getIdToNegSeccion());

		List<NegocioEstiloVehiculo> estilosAsociados = new ArrayList<NegocioEstiloVehiculo>();

		estilosAsociados = negocioEstiloVehiculoDao
				.getEstilosVehiculo(negocioSeccion.getIdToNegSeccion());

		RelacionesNegocioEstiloVehiculoDTO relaciones = new RelacionesNegocioEstiloVehiculoDTO();
		relaciones.setAsociadas(estilosAsociados);
		return relaciones;
	}

	@Override
	public void relacionarNegocioEstiloVehiculo(String accion,
			NegocioEstiloVehiculo negocioEstiloVehiculo) {
		if (accion.equals("deleted")) {
			
			negocioEstiloVehiculo = negocioEstiloVehiculoDao.findById(negocioEstiloVehiculo.getIdToNegEstiloVehiculo());
		}else{
			NegocioSeccion temp = negocioEstiloVehiculo.getNegocioSeccion();
			temp = entidadDao.getReference(NegocioSeccion.class, temp.getIdToNegSeccion());
			NegocioEstiloVehiculo estiloVehiculo = new  NegocioEstiloVehiculo();
			estiloVehiculo.setNegocioSeccion(temp);
			estiloVehiculo.setEstiloVehiculoDTO(negocioEstiloVehiculo.getEstiloVehiculoDTO());
			
		}
		entidadDao.executeActionGrid(accion, negocioEstiloVehiculo);
		
	}

	@Override
	public List<NegocioEstiloVehiculo> getEstiloVehiculoAsociados(
			NegocioSeccion negocioSeccion, BigDecimal idTcMarcaVehiculo,
			BigDecimal tipoVehiculo) {
		
		return this.negocioEstiloVehiculoDao.getEstilosVehiculo(
				negocioSeccion.getIdToNegSeccion(), idTcMarcaVehiculo,
				tipoVehiculo);
	}	
	@Override
	public List<EstiloVehiculoDTO> getEstilosVehiculo(String claveTipoBien,Integer modelo, String descripcion) {
		
		return estiloVehiculoFacadeRemote.listarDistintosFiltrado(claveTipoBien, modelo, descripcion, null);
	}	
	private EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	private EntidadDao entidadDao;
	private NegocioEstiloVehiculoDao negocioEstiloVehiculoDao;
	private NegocioTarifaService negocioTarifaService;

	@EJB		
	public void setNegocioTarifaService(
			NegocioTarifaService negocioTarifaService) {
		this.negocioTarifaService = negocioTarifaService;
	}
	
	@EJB		
	public void setNegocioEstiloVehiculoDao(
			NegocioEstiloVehiculoDao negocioEstiloVehiculoDao) {
		this.negocioEstiloVehiculoDao = negocioEstiloVehiculoDao;
	}

	@EJB	
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}

	@EJB
	public void setEstiloVehiculoFacadeRemote(
			EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote) {
		this.estiloVehiculoFacadeRemote = estiloVehiculoFacadeRemote;
	}




}
