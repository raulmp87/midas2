package mx.com.afirme.midas2.service.impl.ReporteAgentes;

import java.io.File;
import java.io.FileInputStream;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportesAgente.ReportePolizasXSerieDTO;
import mx.com.afirme.midas2.service.reporteAgentes.ReportePolizasXSerieService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.ExcelExporter;

/**
 * Clase que contiene la implementaci\u00f3n de la interfaz <code>ReportePolizasXSerieService</code>
 * 
 * @author SEGUROS AFIRME
 * 
 * @since 24112016
 * 
 * @version 1.0
 *
 */
@Stateless
public class ReportePolizasXSerieServiceImpl implements ReportePolizasXSerieService, Serializable {

	private static final long serialVersionUID = -3567613188119266734L;
	private static final Logger LOG = LoggerFactory.getLogger(ReportePolizasXSerieServiceImpl.class);
	
	@EJB
	private EntidadHistoricoDao entidadService;
	
	@EJB
	public SistemaContext sistemaContext;
	/**
	 * Attributos del objeto que contendra el reporte.
	 */
	public static final String[] ATRIBUTOS_OBJETO_REPORTE = {"centro_emis", "num_poliza_midas", "num_poliza_seycos", "num_renov_pol", 
			"num_cotiza_midas", "num_cotiza_seycos", "f_ini_vigencia", "f_fin_vigencia", "sit_poliza", "lin_negocio", "inciso", 
			"estilo", "modelo_auto", "desc_vehic", "num_serie", "num_motor"};
	
	/**
	 * Campos que contendra a respuesta de la consulta nativa.
	 */
	public static final String[] CAMPOS_RESPUESTA_CONSULTA = {"CENTRO_EMIS", "NUM_POLIZA_MIDAS", "NUM_POLIZA_SEYCOS", "NUM_RENOV",
			"NUM_COTIZA_MIDAS", "NUM_COTIZA_SEYCOS", "F_INI_VIGENCIA", "F_FIN_VIGENCIA", "SIT_POLIZA", "LIN_NEGOCIO", "INCISO", 
			"ESTILO", "MODELO_AUTO", "DESC_VEHIC", "NUM_SERIE", "NUM_MOTOR"};
	

	private String series = null;
	
	/**
	 * Sql que contiene la consulta a ejecutar
	
	 */
	
	String SQL_QUERY_CONSULTAR_POLIZAS_X_NUMERO_SERIE_START = "SELECT DISTINCT LPAD(A.ID_CENTRO_EMIS, 3, 0)CENTRO_EMIS, "
			+ "        TRIM(CASE WHEN LENGTH(D.NUMEROPOLIZA) <= 6 THEN "
			+ "                    LPAD (D.NUMEROPOLIZA, 6, '0') "
			+ "                  ELSE LPAD (D.NUMEROPOLIZA, 12, '0') "
			+ "              END) NUM_POLIZA_MIDAS, "
			+ "         TRIM(CASE WHEN LENGTH(A.NUM_POLIZA) <= 6 THEN "
			+ "                    LPAD (A.NUM_POLIZA, 6, '0') "
			+ "                  ELSE LPAD (A.NUM_POLIZA, 12, '0') "
			+ "              END) NUM_POLIZA_SEYCOS, "
			+ "        LPAD(D.NUMERORENOVACION, 2, 0) NUM_RENOV, "
			+ "        D.IDTOCOTIZACION NUM_COTIZA_MIDAS, "
			+ "        A.ID_COTIZACION NUM_COTIZA_SEYCOS, "
			+ "        TO_CHAR (A.F_INI_VIGENCIA, 'DD/MM/YYYY') F_INI_VIGENCIA, "
			+ "               TO_CHAR (A.F_FIN_VIGENCIA, 'DD/MM/YYYY') F_FIN_VIGENCIA, "
			+ "               DECODE (A.SIT_POLIZA, 'PV', 'POLIZA VIGENTE', "
			+ "                                   'NV', 'POLIZA NO VIGENTE', "
			+ "                                   'CP', 'COTIZACION DE POLIZA', "
			+ "                                   '-') SIT_POLIZA, "
			+ "        E.ID_LIN_NEGOCIO LIN_NEGOCIO, "
			+ "        E.ID_INCISO INCISO, "
			+ "        E.ID_ESTILO ESTILO, "
			+ "        E.ID_MODELO_AUTO MODELO_AUTO, "
			+ "        E.DESC_VEHIC, "
			+ "        E.NUM_SERIE, "
			+ "        E.NUM_MOTOR "
			+ "FROM    SEYCOS.POL_POLIZA A "
			+ " "
			+ "INNER   JOIN (SELECT  ID_SEYCOS,SUBSTR(ID_MIDAS,1,INSTR(ID_MIDAS,'|')-1)ID_MIDAS "
			+ "                        FROM    SEYCOS.CONV_MIDAS_SEYCOS "
			+ "                        WHERE   TIPO='POLIZA') C "
			+ "        ON (A.ID_COTIZACION=C.ID_SEYCOS) "
			+ "INNER   JOIN MIDAS.TOPOLIZA D ON (C.ID_MIDAS=D.IDTOPOLIZA) "
			+ "INNER   JOIN SEYCOS.POL_AUTO E ON (E.ID_COTIZACION=A.ID_COTIZACION AND A.ID_VERSION_POL=E.ID_VERSION_POL) "
			+ "INNER   JOIN (SELECT  POLIZA.ID_COTIZACION, "
			+ "                      MAX(POLIZA.ID_VERSION_POL)ID_VERSION_POL, "
			+ "                      SERIES.NUM_SERIE "
			+ "              FROM    SEYCOS.POL_POLIZA POLIZA "
			+ "              INNER   JOIN (SELECT  MAX(A.ID_COTIZACION)ID_COTIZACION, "
			+ "                                    A.NUM_SERIE "
			+ "                            FROM    SEYCOS.POL_AUTO A "
			+ "                            WHERE   A.NUM_SERIE ";
	
	
	String SQL_QUERY_CONSULTAR_POLIZAS_X_NUMERO_SERIE_END = " GROUP   BY A.NUM_SERIE) SERIES "
			+ "                      ON (POLIZA.ID_COTIZACION=SERIES.ID_COTIZACION) "
			+ "              INNER   JOIN SEYCOS.POL_AUTO AUTO ON (POLIZA.ID_COTIZACION=AUTO.ID_COTIZACION "
			+ "                      AND POLIZA.ID_VERSION_POL=AUTO.ID_VERSION_POL "
			+ "                      AND SERIES.NUM_SERIE=AUTO.NUM_SERIE) "
			+ "              GROUP   BY POLIZA.ID_COTIZACION,SERIES.NUM_SERIE) B "
			+ "        ON (A.ID_COTIZACION=B.ID_COTIZACION AND A.ID_VERSION_POL=B.ID_VERSION_POL)";

	/**
	 * Constante que contiene el nombre del archivo a crear
	 */
	public static final String NOMBRE_ARCHIVO_EXCEL_RESPUESTA = "reporte_Serie";
	
	Integer posicionSeries;
	Integer posicionNumerosSeycos;
	Integer posicionNumerosMidas;
	
	Map<String, List<Object>> mapaValores = new HashMap<String, List<Object>>();
	
	List<Object> numerosSerie = new ArrayList<Object>();
	List<Object> numerosSeycos = new ArrayList<Object>();
	List<Object> numerosMidas = new ArrayList<Object>();
	
	
	@PersistenceContext
	private EntityManager entity;
	
	@Override
	public Map<String, List<Object>> procesaArchivoPorXSSF(File archivoProceso){
		LOG.info(">>>>>>>>>>>>>> [  INFO  ] entra al metodo procesaArchivoPorXSSF");
		
		LOG.info(":::::::::::::: [  INFO  ] :::::::::::::: Inicializa las variables a ocupar...");
		XSSFSheet hoja;
		try{
			FileInputStream datosExcel = new FileInputStream(archivoProceso);
			
			Workbook excelContent = new XSSFWorkbook(datosExcel);
			
			hoja = (XSSFSheet) excelContent.getSheetAt(0);
		} catch (Exception err){
			throw new RuntimeException(err);
		}
		
		
		return obtenerInformacionRow(hoja.iterator());
	}

	@Override
	public Map<String, List<Object>> procesaArchivoPorHSSF(File archivoProceso){
		LOG.info(">>>>>>>>>>>>>> [  INFO  ] entra al metodo procesaArchivoPorHSSF");
		
		LOG.info(":::::::::::::: [  INFO  ] :::::::::::::: Inicializa las variables a ocupar...");
		
		HSSFSheet hoja;
		try{
    		FileInputStream datosExcel = new FileInputStream(archivoProceso);
    		
    		Workbook excelContent = new HSSFWorkbook(datosExcel);
    		
    		hoja = (HSSFSheet) excelContent.getSheetAt(0);
		} catch (Exception err){
			throw new RuntimeException(err);
		}
		return obtenerInformacionRow(hoja.iterator());
	}
	
	/**
	 * M\u00e9todo que realiza el proceso de obtencion de la informacion
	 * que contienen los renglones de una hoja de excel.
	 * 
	 * @param rowIterator Iterador generico Row para los dos tipos de excel 
	 * que se permiten.
	 * 
	 * @return Mapa que contiene como llave el tipo de lista con la informacion que contiene.
	 */
	private Map<String, List<Object>> obtenerInformacionRow(Iterator<Row> rowIterator){
		
		LOG.info(">>>>>>>>>>>>>> [  INFO  ] entra al metodo obtenerInformacionRow");
		mapaValores = new HashMap<String, List<Object>>();
		
		numerosSerie = new ArrayList<Object>();
		numerosSeycos = new ArrayList<Object>();
		numerosMidas = new ArrayList<Object>();
		
		Integer cellPosition;
		
		while(rowIterator.hasNext()){
			Row row = rowIterator.next();
			
			Iterator<Cell> cellIterator = row.cellIterator();
			
			cellPosition = 0;
			String valueCell = "";
			while(cellIterator.hasNext()){
				
				Cell cell = cellIterator.next();
				
				switch (cell.getCellType()){
					case Cell.CELL_TYPE_STRING:
						valueCell = cell.getStringCellValue().toString();
						break;
					case Cell.CELL_TYPE_NUMERIC:
						Long valor = new Long(new Double(cell.getNumericCellValue()).longValue());
						valueCell = valor.toString();
						break;
					case Cell.CELL_TYPE_BLANK:
						valueCell =  null;
						break;
				}
				
				if (valueCell != null && valueCell.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna())) {
					posicionSeries = cellPosition;
				}
				
				if (valueCell != null && valueCell.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna())) {
					posicionNumerosSeycos = cellPosition;
				}
				
				if (valueCell != null && valueCell.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna())) {
					posicionNumerosMidas = cellPosition;
				}
				
				if(!StringUtils.isBlank(valueCell))
					agregarElementoALista(valueCell, cellPosition);
				
				cellPosition++;
			}
		}
		
		if(!numerosSerie.isEmpty())
			mapaValores.put(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna(), numerosSerie);
		if(!numerosSeycos.isEmpty())
			mapaValores.put(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna(), numerosSeycos);
		if(!numerosMidas.isEmpty())
			mapaValores.put(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna(), numerosMidas);
		
		return mapaValores;
	}

	@Override
	public TransporteImpresionDTO generarReporte(Map<String, List<Object>> excelData) {
		ExcelExporter exporter = new ExcelExporter(ReportePolizasXSerieDTO.class);
		List<ReportePolizasXSerieDTO> listaReportes = new ArrayList<ReportePolizasXSerieDTO>();
		
		String sqlNative = generaConsultaSQLNative(excelData);
		
		if(excelData != null && !excelData.isEmpty()){
			Query query = entity.createNativeQuery(sqlNative);
			@SuppressWarnings("unchecked")
			List<Object[]> results = (List<Object[]>) query.getResultList();
			if(results != null && !results.isEmpty()){
				for(Object[] obj: results){
					ReportePolizasXSerieDTO elemento = new ReportePolizasXSerieDTO();
					elemento.setCentroEmis(obj[0] != null ? (String) obj[0] : "");
					elemento.setNumPolizaMidas(obj[1] != null ? (String) obj[1] : "");
					elemento.setNumPolizaSeycos(obj[2] != null ? (String) obj[2] : "");
					elemento.setNumRenov(obj[3] != null ? (String) obj[3] : "");
					elemento.setNumCotizaMidas(obj[4] != null ? ((BigDecimal) obj[4]).longValue() : 0);
					elemento.setNumCotizaSeycos(obj[5] != null ? ((BigDecimal) obj[5]).longValue() : 0);
					elemento.setFechIniVigencia(obj[6] != null ? (String) obj[6] : null);
					elemento.setFechFinVigencia(obj[7] != null ? (String) obj[7] : null);
					elemento.setSitPoliza(obj[8] != null ? (String) obj[8] : "");
					elemento.setLinNegocio(obj[9] != null ? ((BigDecimal) obj[9]).longValue() : 0);
					elemento.setInciso(obj[10] != null ? ((BigDecimal) obj[10]).longValue() : 0);
					elemento.setEstilo(obj[11] != null ? (String) obj[11] : "");
					elemento.setModeloAuto(obj[12] != null ? ((BigDecimal) obj[12]).longValue() : 0);
					elemento.setDescVehic(obj[13] != null ? (String) obj[13] : "");
					elemento.setNumSerie(obj[14] != null ? (String) obj[14] : "");
					elemento.setNumMotor(obj[15] != null ? (String) obj[15] : "");
					listaReportes.add(elemento);
				}
			}
		}
		return exporter.exportXLS(listaReportes, NOMBRE_ARCHIVO_EXCEL_RESPUESTA);
	}
	
	@Override
	public TransporteImpresionDTO generarReporte(String numeroSerie, Long numeroSeycos, Long numeroMidas){
		mapaValores = new HashMap<String, List<Object>>();
		numerosSerie = new ArrayList<Object>();
		numerosSeycos = new ArrayList<Object>();
		numerosMidas = new ArrayList<Object>();
		
		if(!StringUtil.isEmpty(numeroSerie)){
			numerosSerie.add("'" + numeroSerie + "'");
			mapaValores.put(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna(), numerosSerie);
		}
		
		if(numeroSeycos != null && numeroSeycos > 0l){
			numerosSeycos.add(numeroSeycos);
			mapaValores.put(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna(), numerosSeycos);
		}
		
		if(numeroMidas != null && numeroMidas > 0l){
			numerosMidas.add(numeroMidas);
			mapaValores.put(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna(), numerosMidas);
		}
		
		return generarReporte(mapaValores);
	}
	
	/**
	 * Metodo que realiza la concatenacion mediante referencia al StringBuilder por medio de referencia
	 * 
	 * @param referecia StringBuilder del cual se realiza la concatenacion por referencia.
	 * 
	 * @param elementos Lista de objetos que se genero al procesar el archivo de excel.
	 */
	private String cargarElementosListaConsulta(List<Object> elementos){
		Integer numeroRegistros = elementos.size();
		Integer count = 0;
		StringBuilder result = new StringBuilder();
		if(!elementos.isEmpty()){
    		for(Object o : elementos){
    			count++;
    			if(o instanceof String){
        			if(count < numeroRegistros){
        				result.append((String) o)
        					.append(", ");
        			} else {
        				result.append((String) o);
        			}
    			} else if (o instanceof Long){
    				if(count < numeroRegistros){
    					result.append((Long) o)
        					.append(", ");
        			} else {
        				result.append((Long) o);
    			}
    		}
		}
		}
		
		return result.toString();
	}
	
	/**
	 * M&eacute;todo que asigna el valor correspondiente a la lista correspondiente.
	 * 
	 * @param valor Valor a agregar a la lista
	 * 
	 * @param posicionActual Posicion de la columna en la lista.
	 */
	private void agregarElementoALista(String valor, Integer posicionActual){
		
		if(posicionSeries == posicionActual &&
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna()) &&
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna()) &&
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna())){
			numerosSerie.add("'" + valor + "'");
		}
		
		if(posicionNumerosSeycos == posicionActual && 
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna()) &&
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna()) &&
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna())){
  			numerosSeycos.add(valor);
		}
		
		if(posicionNumerosMidas == posicionActual && 
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna()) &&
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna()) &&
				!valor.trim().toUpperCase().equals(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna())){
			numerosMidas.add(valor);
		}
	}
	
	/**
	 * M\u00e9todo que genera la consulta de SQL 
	 * a partir de la informacion que contiene el mapa que recibe como 
	 * argumento.
	 * 
	 * @param elementosConsulta Mapa que contiene la informacion para generar la 
	 * consulta de SQL.
	 * 
	 * @return consulta generada.
	 */
	private String generaConsultaSQLNative(Map<String, List<Object>> elementosConsulta){
		
		boolean contieneInformacion = Boolean.FALSE;

		StringBuilder consulta = new StringBuilder(SQL_QUERY_CONSULTAR_POLIZAS_X_NUMERO_SERIE_START);
		
		if(elementosConsulta.containsKey(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna()) &&
				elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna()) != null &&
				!elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna()).isEmpty()){
    		//Numero Serie MIDAS y SEYCOS
			series = cargarElementosListaConsulta(elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_SERIE.obtenerNombreColumna()));
    		consulta.append(" IN (").append(series).append(")").append(SQL_QUERY_CONSULTAR_POLIZAS_X_NUMERO_SERIE_END);
    		//consulta.append(")");
    		contieneInformacion = Boolean.TRUE;
		}
		
		if(elementosConsulta.containsKey(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna()) &&
				elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna()) != null &&
				!elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna()).isEmpty()){
    		//Numero de póliza SEYCOS
			series = cargarElementosListaConsulta(elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_SEYCOS.obtenerNombreColumna()));
			
			consulta.append(" = A.NUM_SERIE").append(SQL_QUERY_CONSULTAR_POLIZAS_X_NUMERO_SERIE_END);
			consulta.append(" WHERE A.NUM_POLIZA IN( ").append(series).append(" )");
	
			  
    		contieneInformacion = Boolean.TRUE;
		}
		
		if(elementosConsulta.containsKey(ReportePolizasXSerieService.CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna()) &&
				elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna()) != null &&
				!elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna()).isEmpty()){
			//Numero de póliza MIDAS
			series = cargarElementosListaConsulta(elementosConsulta.get(CAMPOS_EXCEL_CARGA.NUMERO_POLIZA_MIDAS.obtenerNombreColumna()));
			
			  consulta.append(" = A.NUM_SERIE").append(SQL_QUERY_CONSULTAR_POLIZAS_X_NUMERO_SERIE_END);
			  consulta.append(" WHERE D.NUMEROPOLIZA IN ( ").append(series).append(" )");
			  
		}
	
		LOG.info(":::::::::::::: [  INFO  ] :::::::::::::: SqlQuery a ejecutar.");
		LOG.info(consulta.toString());
		
		return consulta.toString();
	}

	public EntityManager getEntity() {
		return entity;
	}

	public void setEntity(EntityManager entity) {
		this.entity = entity;
	}

	@Override
	public String obtenerNombreCompleto(BigDecimal idToControlArchivo) {
		// TODO Auto-generated method stub
		ControlArchivoDTO controlArchivoDTO = entidadService.findById(ControlArchivoDTO.class, idToControlArchivo);
		StringBuilder nombreCompletoArchivo = new StringBuilder();
		String nombreOriginalArchivo = controlArchivoDTO.getNombreArchivoOriginal();
		String extension = (nombreOriginalArchivo.lastIndexOf(".") == -1) ? "" : nombreOriginalArchivo
				.substring(nombreOriginalArchivo.lastIndexOf("."), nombreOriginalArchivo.length());
		nombreCompletoArchivo.append(sistemaContext.getUploadFolder()).append(controlArchivoDTO.getIdToControlArchivo().toString()).append(extension);
		return nombreCompletoArchivo.toString();
	}

}
