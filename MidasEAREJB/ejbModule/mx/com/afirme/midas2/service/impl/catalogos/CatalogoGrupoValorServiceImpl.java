package mx.com.afirme.midas2.service.impl.catalogos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.CatValorFijo;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.util.StringUtil;

@Stateless
public class CatalogoGrupoValorServiceImpl implements CatalogoGrupoValorService {

	@EJB
	private EntidadService entidadService;

	
	@Override
	public List<CatValorFijo> obtenerCatalogoValores(TIPO_CATALOGO tipo, String codigoPadre) {
		return this.obtenerCatalogoValores(tipo, codigoPadre, Boolean.FALSE);
	}
	
	@Override
	public CatGrupoFijo obtenerGrupo(TIPO_CATALOGO tipo) {
		List<CatGrupoFijo> grupos = entidadService.findByProperty(CatGrupoFijo.class,
				"codigo", tipo);
		if(grupos != null){
			return grupos.get(0);
		}
		return null;
	}

	@Override
	public CatValorFijo obtenerValorPorCodigo(TIPO_CATALOGO tipo, String codigo) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("grupo.codigo" , tipo);
		params.put("codigo" , codigo);
		List<CatValorFijo> lista = entidadService.findByProperties(CatValorFijo.class, params);
		if(lista != null && lista.size() > 0){
			return lista.get(0);
		}
		return null;
	}

	@Override
	public String obtenerDescripcionValorPorCodigo(TIPO_CATALOGO tipo,
			String codigo) {
		String descripcion = "";
		if(!StringUtil.isEmpty(codigo)){
			CatValorFijo catValorFijo= this.obtenerValorPorCodigo(tipo, codigo);
			if(null!= catValorFijo && null!=catValorFijo.getDescripcion()){
				descripcion = catValorFijo.getDescripcion();
			}
		}
		return descripcion;
	}

	@Override
	public List<CatValorFijo> obtenerCatalogoValores(TIPO_CATALOGO codigo,
			String codigoPadreId, boolean orderByDescription) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("grupo.codigo", codigo);
		params.put("activo", Boolean.TRUE);
		List<CatValorFijo> valores = entidadService.findByPropertiesWithOrder(CatValorFijo.class, params, 
				orderByDescription ? "descripcion" : "orden");		
		if(!StringUtil.isEmpty(codigoPadreId)){
			List<CatValorFijo> valoresPorPadre = new ArrayList<CatValorFijo>();
			for(CatValorFijo val : valores){				
				if(!StringUtil.isEmpty(val.getCodigoPadre()) && Arrays.asList(val.getCodigoPadre().split(",")).contains(codigoPadreId)){
					valoresPorPadre.add(val);
				}				
			}
			return valoresPorPadre;
		}		
		return valores;	
	}

	

}
