package mx.com.afirme.midas2.service.impl.excepcion;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.cargacombo.ComboBean;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCot;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.excepcion.CondicionExcepcion;
import mx.com.afirme.midas2.domain.excepcion.ExcepcionSuscripcion;
import mx.com.afirme.midas2.domain.excepcion.ValorExcepcion;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.condicionespecial.NegocioCondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionDetalle;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionNegocioDescripcionDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.excepcion.auto.ExcepcionCotizacionEvaluacionAutoDTO;
import mx.com.afirme.midas2.dto.excepcion.auto.ExcepcionSuscripcionAutoDescripcionDTO;
import mx.com.afirme.midas2.dto.excepcion.auto.ExcepcionSuscripcionReporteAutoDTO;
import mx.com.afirme.midas2.service.ValidacionService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioAutosService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.NivelEvaluacion;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoCondicion;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.TipoValor;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.negocio.condicionespecial.NegocioCondicionEspecialService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.util.StringUtil;

import org.springframework.beans.BeanUtils;

@Stateless
public class ExcepcionSuscripcionNegocioAutosServiceImpl extends
		ExcepcionSuscripcionNegocioServiceImpl implements
		ExcepcionSuscripcionNegocioAutosService {

	public static final Logger LOG = Logger.getLogger(ExcepcionSuscripcionNegocioAutosServiceImpl.class);
	
	private IncisoService incisoService;
	
	private ValidacionService validacionService;
	
	private AgenteMidasService agenteMidasService;
	
	private NegocioCondicionEspecialService negocioCondEspecialService;
	
	private DateFormat format = new SimpleDateFormat("dd/MM/yyyy");
	
	private static final double MAX_PRIMA_TOTAL_PF_DLLS = 100000;
	
	@Override
	public List<? extends ExcepcionSuscripcionNegocioDescripcionDTO> listarExcepciones(
			String cveNegocio) {
		ExcepcionSuscripcionAutoDescripcionDTO excepcion = null;
		List<ExcepcionSuscripcionAutoDescripcionDTO> lista = new ArrayList<ExcepcionSuscripcionAutoDescripcionDTO>();
		//List<ExcepcionSuscripcion> excepciones = excepcionService.listarExcepciones(cveNegocio);
		ExcepcionSuscripcion filtro = new ExcepcionSuscripcion();
		filtro.setHabilitado(true);
		filtro.setCveNegocio(cveNegocio);
		List<ExcepcionSuscripcion> excepciones = excepcionService.listarExcepciones(filtro);
		for(ExcepcionSuscripcion excepcionSuscripcion : excepciones){
			excepcion = (ExcepcionSuscripcionAutoDescripcionDTO)this.obtenerExcepcion(excepcionSuscripcion.getId());			
			lista.add(excepcion);
		}		
		return lista;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BigDecimal idToCotizacion){
		return this.evaluarCotizacion(idToCotizacion, null);
	}
	
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(BigDecimal idToCotizacion, NivelEvaluacion nivelEvaluacion){
		CotizacionDTO cotizacion = entidad.findById(CotizacionDTO.class, idToCotizacion);
		return evaluarCotizacion(cotizacion, nivelEvaluacion);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(CotizacionDTO cotizacion, NivelEvaluacion nivelEvaluacion){
		List<ExcepcionSuscripcionReporteAutoDTO> reporte = new ArrayList<ExcepcionSuscripcionReporteAutoDTO>(1);
		List<ExcepcionSuscripcion> excepciones = excepcionService.listarExcepciones(Negocio.CLAVE_NEGOCIO_AUTOS, nivelEvaluacion);
		List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelCoberturas = prepararCotizacionParaEvaluacionNivelCobertura(cotizacion);
		List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelInciso = prepararCotizacionParaEvaluacionNivelInciso(cotizacion);
		List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelCotizacion = prepararCotizacionParaEvaluacionNivelCotizacion(cotizacion);
		for(ExcepcionSuscripcion excepcion : excepciones){
			evaluarExcepcion(reporte, registrosNivelCoberturas, registrosNivelInciso, registrosNivelCotizacion, excepcion.getId());
		}
		return reporte;
	}
	
	
	/*
	 * Genera todos los posibles registros a evaluar dentro de una cotizacion
	 *  
	 */	
	public List<ExcepcionCotizacionEvaluacionAutoDTO> prepararCotizacionParaEvaluacionNivelCotizacion(CotizacionDTO cotizacion){
		List<ExcepcionCotizacionEvaluacionAutoDTO> lista = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion =  llenarRegistroAEvaluar(cotizacion);	
		lista.add(evaluacion);
		return lista;
	}
	
	/*
	 * Genera todos los posibles registros a evaluar dentro de una cotizacion a nivel cobertura
	 *  
	 */	
	public List<ExcepcionCotizacionEvaluacionAutoDTO> prepararCotizacionParaEvaluacionNivelCobertura(BigDecimal idToCotizacion){
		CotizacionDTO cotizacion = entidad.findById(CotizacionDTO.class, idToCotizacion);
		List<ExcepcionCotizacionEvaluacionAutoDTO> lista = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion =  null;	
		ExcepcionCotizacionEvaluacionAutoDTO evaluacionCotizacion = llenarRegistroAEvaluar(cotizacion);
		List<IncisoCotizacionDTO> incisos = cotizacion.getIncisoCotizacionDTOs();
		for(IncisoCotizacionDTO inciso : incisos){
			ExcepcionCotizacionEvaluacionAutoDTO evaluacionInciso = new ExcepcionCotizacionEvaluacionAutoDTO();
			BeanUtils.copyProperties(evaluacionCotizacion,evaluacionInciso);
			evaluacion = llenarRegistroAEvaluar(evaluacionInciso, inciso);
			List<SeccionCotizacionDTO> secciones = inciso.getSeccionCotizacionList();
			for(SeccionCotizacionDTO seccion : secciones){
				List<CoberturaCotizacionDTO> coberturas = seccion.getCoberturaCotizacionLista();
				for(CoberturaCotizacionDTO cobertura : coberturas){
					ExcepcionCotizacionEvaluacionAutoDTO evaluacionNueva = new ExcepcionCotizacionEvaluacionAutoDTO();
					BeanUtils.copyProperties(evaluacion,evaluacionNueva);				
					evaluacionNueva.setCobertura(cobertura.getId().getIdToCobertura());
					if(cobertura.getClaveTipoDeducible().shortValue() == 0){
						evaluacionNueva.setDeducible(0d);
					}else if(cobertura.getClaveTipoDeducible().shortValue() == 1){
						evaluacionNueva.setDeducible(cobertura.getPorcentajeDeducible());
					}else{
						evaluacionNueva.setDeducible(cobertura.getValorDeducible());
					}															
					evaluacionNueva.setSumaAsegurada(cobertura.getValorSumaAsegurada());					
					lista.add(evaluacionNueva);
				}
			}		
		}	
		return lista;
	}
	
	public List<ExcepcionCotizacionEvaluacionAutoDTO> prepararCotizacionParaEvaluacionNivelCobertura(CotizacionDTO cotizacion){	
		List<ExcepcionCotizacionEvaluacionAutoDTO> lista = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion =  null;
		ExcepcionCotizacionEvaluacionAutoDTO evaluacionCotizacion = llenarRegistroAEvaluar(cotizacion);
		List<IncisoCotizacionDTO> incisos = cotizacion.getIncisoCotizacionDTOs();
		for(IncisoCotizacionDTO inciso : incisos){
			List<SeccionCotizacionDTO> secciones = inciso.getSeccionCotizacionList();
			ExcepcionCotizacionEvaluacionAutoDTO evaluacionInciso = new ExcepcionCotizacionEvaluacionAutoDTO();
			BeanUtils.copyProperties(evaluacionCotizacion,evaluacionInciso);
			evaluacion = llenarRegistroAEvaluar(evaluacionInciso, inciso);
			for(SeccionCotizacionDTO seccion : secciones){
				List<CoberturaCotizacionDTO> coberturas = seccion.getCoberturaCotizacionLista();
				for(CoberturaCotizacionDTO cobertura : coberturas){	
					ExcepcionCotizacionEvaluacionAutoDTO evaluacionNueva = new ExcepcionCotizacionEvaluacionAutoDTO();
					BeanUtils.copyProperties(evaluacion,evaluacionNueva);
					evaluacionNueva.setCobertura(cobertura.getId().getIdToCobertura());					
					evaluacionNueva.setDeducible(cobertura.getValorDeducible());										
					evaluacionNueva.setSumaAsegurada(cobertura.getValorSumaAsegurada());						
					lista.add(evaluacionNueva);
				}
			}		
		}	
		return lista;
	}
	
	
	/*
	 * Genera todos los posibles registros a evaluar dentro de una cotizacion a nivel inciso
	 *  
	 */	
	private List<ExcepcionCotizacionEvaluacionAutoDTO> prepararCotizacionParaEvaluacionNivelInciso(BigDecimal idToCotizacion){
		CotizacionDTO cotizacion = entidad.findById(CotizacionDTO.class, idToCotizacion);	
		List<ExcepcionCotizacionEvaluacionAutoDTO> lista = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();		
		List<IncisoCotizacionDTO> incisos = cotizacion.getIncisoCotizacionDTOs();
		ExcepcionCotizacionEvaluacionAutoDTO evaluacionCotizacion = llenarRegistroAEvaluar(cotizacion);
		for(IncisoCotizacionDTO inciso : incisos){	
			ExcepcionCotizacionEvaluacionAutoDTO evaluacionInciso = new ExcepcionCotizacionEvaluacionAutoDTO();
			BeanUtils.copyProperties(evaluacionCotizacion ,evaluacionInciso);
			lista.add(llenarRegistroAEvaluar(evaluacionInciso, inciso));							
		}	
		return lista;
	}
	
	private List<ExcepcionCotizacionEvaluacionAutoDTO> prepararCotizacionParaEvaluacionNivelInciso(CotizacionDTO cotizacion){
		List<ExcepcionCotizacionEvaluacionAutoDTO> lista = new ArrayList<ExcepcionCotizacionEvaluacionAutoDTO>();		
		List<IncisoCotizacionDTO> incisos = cotizacion.getIncisoCotizacionDTOs();
		ExcepcionCotizacionEvaluacionAutoDTO evaluacionCotizacion = llenarRegistroAEvaluar(cotizacion);
		for(IncisoCotizacionDTO inciso : incisos){	
			ExcepcionCotizacionEvaluacionAutoDTO evaluacionInciso = new ExcepcionCotizacionEvaluacionAutoDTO();
			BeanUtils.copyProperties(evaluacionCotizacion ,evaluacionInciso);
			lista.add(llenarRegistroAEvaluar(evaluacionInciso, inciso));	
			evaluacionCotizacion = llenarRegistroAEvaluar(cotizacion);
		}	
		return lista;
	}
	
	private ExcepcionCotizacionEvaluacionAutoDTO llenarRegistroAEvaluar(CotizacionDTO cotizacion){
		ExcepcionCotizacionEvaluacionAutoDTO evaluacion =  new ExcepcionCotizacionEvaluacionAutoDTO();	
		//Buscar el agente solo si es necesario (cuando la solicitud no tenga el objeto agente) y la solicitud tenga el codigoAgente.
		if(cotizacion.getSolicitudDTO().getAgente() == null && cotizacion.getSolicitudDTO().getCodigoAgente() != null){			
			try{
				Agente agente = new Agente();
				agente.setId(cotizacion.getSolicitudDTO().getCodigoAgente().longValue());
				agente = agenteMidasService.loadById(agente);
				cotizacion.getSolicitudDTO().setAgente(agente);
			}catch(Exception e){
				e.printStackTrace();
			}
		}

		Agente agente = cotizacion.getSolicitudDTO().getAgente();
		if(agente != null){
			evaluacion.setAgente(agente.getIdAgente().intValue());
		}
		
		evaluacion.setCotizacion(cotizacion.getIdToCotizacion());					
		evaluacion.setNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
		evaluacion.setProducto(cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto());
		evaluacion.setTipoPoliza(cotizacion.getTipoPolizaDTO().getIdToTipoPoliza());
		evaluacion.setUsuario(cotizacion.getCodigoUsuarioCotizacion());
		evaluacion.setVigenciaIncial(cotizacion.getFechaInicioVigencia());
		evaluacion.setVigenciaFinal(cotizacion.getFechaFinVigencia());
		evaluacion.setDescuentoGlobal(cotizacion.getPorcentajeDescuentoGlobal());
		evaluacion.setIdCliente(cotizacion.getIdToPersonaContratante());
		evaluacion.setCveTipoPersona(cotizacion.getSolicitudDTO().getClaveTipoPersona());
		evaluacion.setPrimaTotal(cotizacion.getValorPrimaTotal().doubleValue());
		evaluacion.setIgualacionPrimasCotizacion(cotizacion.getIgualacionNivelCotizacion());
		evaluacion.setDescuentoIgualacionCotizacion(cotizacion.getDescuentoIgualacionPrimas());
		evaluacion.setAplicaValidacionPersonaMoral(cotizacion.getSolicitudDTO().getNegocio().getAplicaValidacionPersonaMoral());
		List<NegocioCondicionEspecial> negCondicionEspecial = negocioCondEspecialService.obtenerCondicionesNegocio(cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), CondicionEspecial.NivelAplicacion.TODAS);
		if(negCondicionEspecial != null && cotizacion.getCondicionesEspeciales().size() > 0){
			for(NegocioCondicionEspecial negocio: negCondicionEspecial){
				if(negocio.getProduceExcepcion() == 1){
					if(cotizacion.getCondicionesEspeciales().contains(negocio.getCondicionEspecial())){
						evaluacion.getCondicionesCotizacion().add(negocio.getCondicionEspecial());
					}
				}
			}
		}
		//
		//La cotizacion ya tiene como lista los texAdicionalCotDTOs no tenemos porque utilizad entidad.findByProperty ya que esto causa
		//problemas de performance porque findByProperty siempre hace refresh.
		List<TexAdicionalCotDTO> texAdicionalList = cotizacion.getTexAdicionalCotDTOs();
//		List<TexAdicionalCotDTO> texAdicionalList = entidad.findByProperty(TexAdicionalCotDTO.class, "cotizacion.idToCotizacion", cotizacion.getIdToCotizacion());
		if(texAdicionalList != null && !texAdicionalList.isEmpty()){
			List<ComboBean>texAdicionalMap = new ArrayList<ComboBean>(1);
			for(TexAdicionalCotDTO item: texAdicionalList){
				if(item.getClaveAutorizacion().equals(TexAdicionalCot.CLAVEAUTORIZACION_EN_PROCESO)){
					ComboBean comboBean = new ComboBean();
					comboBean.setId(item.getIdToTexAdicionalCot().toString());
					comboBean.setDescripcion(item.getDescripcionTexto());
					texAdicionalMap.add(comboBean);
				}
			}
			evaluacion.setTexAdicionalMap(texAdicionalMap);
		}
		return evaluacion;
	}
		
	private ExcepcionCotizacionEvaluacionAutoDTO llenarRegistroAEvaluar(ExcepcionCotizacionEvaluacionAutoDTO evaluacion, IncisoCotizacionDTO inciso){
		IncisoAutoCot auto = inciso.getIncisoAutoCot();		
		evaluacion.setInciso(inciso.getId().getNumeroInciso());					
		evaluacion.setSecuenciaInciso(inciso.getNumeroSecuencia());			
		evaluacion.setIgualacionPrimasInciso(inciso.getIgualacionNivelInciso());
		evaluacion.setDescuentoIgualacionInciso(inciso.getDescuentoIgualacionPrimas());
		
		List<NegocioCondicionEspecial> negCondicionEspecial = negocioCondEspecialService.obtenerCondicionesNegocio(inciso.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio(), CondicionEspecial.NivelAplicacion.TODAS);
		if(negCondicionEspecial != null && inciso.getCondicionesEspeciales().size() > 0){
			for(NegocioCondicionEspecial negocio: negCondicionEspecial){
				if(negocio.getProduceExcepcion() == 1){
					if(inciso.getCondicionesEspeciales().contains(negocio.getCondicionEspecial())){
						evaluacion.getCondicionesInciso().add(negocio.getCondicionEspecial());
					}
				}
			}
		}
		if(auto!=null){			
			evaluacion.setDescripcionEstilo(auto.getDescripcionFinal());
			evaluacion.setEstado(auto.getEstadoId());
			evaluacion.setModelo(Integer.valueOf(auto.getModeloVehiculo()));
			evaluacion.setMunicipio(auto.getMunicipioId());
			evaluacion.setPaquete(auto.getNegocioPaqueteId());
			evaluacion.setLinea(BigDecimal.valueOf(auto.getNegocioSeccionId()));
			evaluacion.setAmis(auto.getEstiloId()!=null?
					auto.getEstiloId().split("_")[1]:null);
			evaluacion.setNumSerie(auto.getNumeroSerie());	
			evaluacion.setPctDescuentoEstado(auto.getPctDescuentoEstado());
		}		
		return evaluacion;
	}
	
	public void evaluarExcepcion(List<ExcepcionSuscripcionReporteAutoDTO> reporte, List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelCoberturas, 
			List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelInciso, List<ExcepcionCotizacionEvaluacionAutoDTO> registrosNivelCotizacion, Long excepcionId){
		
		ConvertidorEstatus convertidor = new ConvertidorEstatus();
		//Integer status = null;
		String descripcionRegistroConExcepcion = "";
		ExcepcionSuscripcionReporteAutoDTO reporteDTO = null;	
		List<ExcepcionCotizacionEvaluacionAutoDTO> registrosParaEvaluar = null;
		ExcepcionSuscripcion excepcion = entidad.findById(ExcepcionSuscripcion.class, excepcionId);		
		//entidad.findById(ExcepcionSuscripcion.class, excepcionId);
		List<CondicionExcepcion> condiciones = excepcion.getCondicionExcepcion();
		//si la excepcion tiene condicion de cobertura evaluar todas las coberturas de todos los incisos
		if(contieneCondicion(condiciones, TipoCondicion.COBERTURA) || 
				contieneCondicion(condiciones, TipoCondicion.SUMA_ASEGURADA) || 
				contieneCondicion(condiciones, TipoCondicion.DEDUCIBLE)){
			//usar todos los registros
			registrosParaEvaluar = registrosNivelCoberturas;
		}else if (contieneCondicion(condiciones, TipoCondicion.SECCION) ||
					contieneCondicion(condiciones, TipoCondicion.PAQUETE) || 
					contieneCondicion(condiciones, TipoCondicion.AMIS) || 
					contieneCondicion(condiciones, TipoCondicion.DESCRIPCION_ESTILO) ||
					contieneCondicion(condiciones, TipoCondicion.MODELO) || 
					contieneCondicion(condiciones, TipoCondicion.ESTADO) ||
					contieneCondicion(condiciones, TipoCondicion.MUNICIPIO) ||
					contieneCondicion(condiciones, TipoCondicion.NUM_SERIE) || 
					contieneCondicion(condiciones, TipoCondicion.MODELO_ANTIGUEDAD) ||
					contieneCondicion(condiciones, TipoCondicion.IGUALACION_PRIMA) || 
					contieneCondicion(condiciones, TipoCondicion.CONDICION_ESPECIAL_INCISO) ||
					contieneCondicion(condiciones, TipoCondicion.PCT_DESCUENTO_ESTADO)){
			//filtrar por registros de tipo inciso
			registrosParaEvaluar = registrosNivelInciso;
		}else{
			registrosParaEvaluar = registrosNivelCotizacion;
		}
		for(ExcepcionCotizacionEvaluacionAutoDTO evaluador : registrosParaEvaluar){
			for(CondicionExcepcion condicion : condiciones){
				List<ValorExcepcion> valores = condicion.getValorExcepcions();
				Collections.sort(valores, 
						new Comparator<ValorExcepcion>() {				
							public int compare(ValorExcepcion n1, ValorExcepcion n2){
								return n1.getId().compareTo(n2.getId());
							}
						});				
				convertidor = evaluarCondicion(convertidor, evaluador, 
						condicion.getTipoValor(), condicion.getTipoCondicion(), valores);
				if(convertidor.getStatus() != null && convertidor.getStatus() == 0){					
					break;
				}				
			}			
			//cumple con excepcion
			if(convertidor.getStatus() != null && convertidor.getStatus() == 1){								
				//realizar operaciones para reportar excepcion para la cotizacion
				convertidor.setDescripcion(convertidor.getDescripcion().trim());
				descripcionRegistroConExcepcion = convertidor.getDescripcion().substring(0, convertidor.getDescripcion().length() -1);
				reporteDTO = generarRegistroReporte(evaluador, excepcion, descripcionRegistroConExcepcion);
				/*Se modifica para que solo envie correo cuando se solicita la autorizacion
				if(excepcion.getRequiereNotificacion()){
					//notificar
					String recipientes = excepcion.getCorreoNotificacion();
					String titulo = TITULO_CORREO_EXCEPCION + " " + evaluador.getCotizacion();
					String saludo = SALUDO_CORREO_EXCEPCION;
					String cuerpo = generarCuerpoMensajeNotificacion(reporteDTO);
					enviarNotificacion(recipientes, titulo, titulo, saludo, cuerpo);					
				}
				*/
								
				if(excepcion.getRequiereAutorizacion() && 
						!revisarValorAutorizado(excepcionId, evaluador)){
					reporte.add(reporteDTO);
				}
			}
			convertidor.setStatus(null);
			convertidor.setDescripcion("");
		}	
	}
	
	/**
	 * Metodo para revisar que el valor de la suma asergurada y deducibles sea el mismo que se autorizo (temporal en lo que se hace dinamico)
	 * @param descripcionRegistroConExcepcion
	 * @param evaluacion
	 */
	private boolean revisarValorAutorizado(Long excepcionId, ExcepcionCotizacionEvaluacionAutoDTO evaluacion){		
		boolean esValida = false;
		if(solicitudService.obtenerEstatusParaExcepcion(evaluacion.getCotizacion(), evaluacion.getLinea(), 
				evaluacion.getInciso(), evaluacion.getCobertura(), excepcionId) == 
							SolicitudAutorizacionService.EstatusDetalleSolicitud.AUTORIZADA.valor()){
			esValida = true;
			SolicitudExcepcionDetalle detalle = solicitudService.obtenerDetalleParaExcepcion(evaluacion.getCotizacion(), 
					excepcionId, evaluacion.getLinea(), evaluacion.getInciso(), evaluacion.getCobertura());			
			if(detalle != null){					
				//obtener cadena de la evaluacion autorizada y sacar valores de suma y deducible			
				String[] condiciones = detalle.getProvocadorExcepcion().split(",");
				for(String condicion : condiciones){
					String[] valores = condicion.split(":");
					String llave = valores[0].trim();
					String valor = null;
					if(valores.length == 2) {
						valor = valores[1].trim();
					} else {
						esValida = false;
						//Contiene comas extras
						LOG.error("El provocador de excepciones no es valido, cotizacion --> " 
						+ evaluacion.getCotizacion() + ", provocador de excepcion --> " +  detalle.getProvocadorExcepcion());
					}

					if(llave.equals("DEDUCIBLE")){
						Double deducible = Double.valueOf(valor);
						if(evaluacion.getDeducible().doubleValue() != deducible.doubleValue()){
							esValida = false;
						}
					}else if(llave.equals("SUMA ASEGURADA")){
						Double sumaAsegurada = Double.valueOf(valor);
						if(evaluacion.getSumaAsegurada().doubleValue() != sumaAsegurada.doubleValue()){
							esValida = false;
						}
					}else if(llave.equals("MODELO")){
						Integer modelo = Integer.valueOf(valor);
						if(evaluacion.getModelo().intValue() != modelo.intValue()){
							esValida = false;
						}
					}else if (llave.equals("DESC ESTILO")){
						if(!(evaluacion.getDescripcionEstilo().replace(",", "")).contains(valor)){
							esValida = false;
						}
					}else if (llave.equals("NUM SERIE REPETIDO")){
						if(!evaluacion.getNumSerie().equals(valor)){
							esValida = false;
						}
					}else if (llave.equals("ART 140")){
						if(!valor.equals("CLIENTE (" + evaluacion.getIdCliente() + ")")){
							esValida = false;
						}
					}else if (llave.equals("IGUALACION PRIMAS COT")){
						if(!valor.equals(evaluacion.getDescuentoIgualacionCotizacion() + "%")){
							esValida = false;
						}
					}else if (llave.equals("IGUALACION PRIMAS INCISO")){
						if(!valor.equals(evaluacion.getInciso() + " - " + evaluacion.getDescuentoIgualacionInciso() + "%")){
							esValida = false;
						}
					}else if (llave.equals("INCLUSION DE TEXTO ADICIONAL")){
						if(evaluacion.getTexAdicionalMap() != null && !evaluacion.getTexAdicionalMap().isEmpty() && valor != null){
							String ids = obtenerIds(evaluacion);
							if(!valor.trim().equals(ids.trim())){
								esValida = false;
							}
						}
					}else if (llave.equals("CONDICION ESPECIAL A NIVEL COTIZACION")){
						String condicionesEspeciales = obtenerCondicionesEspecialesCotizacion(evaluacion);
						if(!condicionesEspeciales.equals(valor)){
							esValida = false;
						}
					}else if (llave.equals("CONDICION ESPECIAL A NIVEL INCISO")){
						String condicionesEspeciales = obtenerCondicionesEspecialesInciso(evaluacion);
						if(!condicionesEspeciales.equals(valor)){
							esValida = false;
						}
					} else if (llave.equals("DESCUENTO POR ESTADO")){
						if(!valor.equals(evaluacion.getPctDescuentoEstado())){
								esValida = false;
						}
					}
				}
			}
			
		}
		return esValida;
	}
	
	
	public String obtenerCondicionesEspecialesCotizacion(
			ExcepcionCotizacionEvaluacionAutoDTO evaluacion) {
		StringBuilder condicionesEspeciales = new StringBuilder("");
		boolean isFirst = true;
		for(CondicionEspecial item: evaluacion.getCondicionesCotizacion()){
			if(isFirst){
				isFirst = false;
			}else{
				condicionesEspeciales.append("-");
			}
			condicionesEspeciales.append(item.getNombre());
		}	
		return condicionesEspeciales.toString();
	}

	public String obtenerCondicionesEspecialesInciso(
			ExcepcionCotizacionEvaluacionAutoDTO evaluacion) {
		StringBuilder condicionesEspeciales = new StringBuilder("");
		boolean isFirst = true;
		for(CondicionEspecial item: evaluacion.getCondicionesInciso()){
			if(isFirst){
				isFirst = false;
			}else{
				condicionesEspeciales.append("-");
			}
			condicionesEspeciales.append(item.getNombre());
		}	
		return condicionesEspeciales.toString();
	}

	public String obtenerIds(ExcepcionCotizacionEvaluacionAutoDTO evaluacion) {
		StringBuilder id = new StringBuilder("");
		boolean isFirst = true;
		for(ComboBean item : evaluacion.getTexAdicionalMap()){
			if(isFirst){
				isFirst = false;
			}else{
				id.append("-");
			}
			id.append(item.getId()).append("");
		}
		return id.toString();
	}

	public ExcepcionSuscripcionReporteAutoDTO generarRegistroReporte(ExcepcionCotizacionEvaluacionAutoDTO evaluacion, ExcepcionSuscripcion excepcion, 
			String descripcionRegistroConExcepcion){
		NegocioSeccion seccion = null;
		CoberturaDTO cobertura = null;
		ExcepcionSuscripcionReporteAutoDTO reporte = new ExcepcionSuscripcionReporteAutoDTO();
		reporte.setIdToCotizacion(evaluacion.getCotizacion());		
		reporte.setIdLineaNegocio(evaluacion.getLinea());	
		reporte.setIdInciso(evaluacion.getInciso());		
		reporte.setIdCobertura(evaluacion.getCobertura());
		reporte.setSequenciaInciso(evaluacion.getSecuenciaInciso());
		reporte.setIdExcepcion(excepcion.getId());
		reporte.setDescripcionExcepcion(excepcion.getDescripcion().toUpperCase());
		reporte.setNumExcepcion(excepcion.getNumero());
		reporte.setDescripcionRegistroConExcepcion(descripcionRegistroConExcepcion);
		if(evaluacion.getLinea() != null){
			seccion = entidad.findById(NegocioSeccion.class, evaluacion.getLinea());
			if(seccion!=null){
				reporte.setLineaNegocio(seccion.getSeccionDTO().getDescripcion());
			}
		}
		if(evaluacion.getCobertura()!=null){
			cobertura = entidad.findById(CoberturaDTO.class, evaluacion.getCobertura()); 			
			if(cobertura != null){
				reporte.setCobertura(cobertura.getDescripcion());
			}
		}		
		return reporte;
	}
	
	public ConvertidorEstatus evaluarCondicion(ConvertidorEstatus status, ExcepcionCotizacionEvaluacionAutoDTO cotizacion, 
			short tipoValor, Integer tipoCondicion, List<ValorExcepcion> valores){
		Integer match = 1;
		Integer noMatch = 0;
		String valor = null;
		String valorRango = null;
		if(!valores.isEmpty()){
			switch(tipoValor){
				case 0:
				case 2:
				case 3:				
					valor = valores.get(0).getValor();
					break;
				case 1:
					valor = valores.get(0).getValor();
					valorRango = valores.get(1).getValor();
					break;
				default:
					break;					
			}			
		}
		switch(TipoCondicion.tipoCondicion(tipoCondicion)){
			case AGENTE:
				if(valor != null){
					try{
						Agente agente = new Agente();
						agente.setId(Long.valueOf(valor));
						agente = agenteMidasService.loadById(agente);
						if(agente.getIdAgente() != null){
							valor = agente.getIdAgente().toString();
						}
					}catch(Exception e){
						e.printStackTrace();
					}
				}
				if(cotizacion.getAgente() != null && cotizacion.getAgente().intValue() == Integer.valueOf(valor)){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + " AGENTE: " + cotizacion.getAgente() + ", "); 
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}			
				break;
			
			case AMIS:			
				if(cotizacion.getAmis() != null && cotizacion.getAmis().equals(valor)){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "AMIS: " + cotizacion.getAmis() + ", ");
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case COBERTURA:
				if(cotizacion.getCobertura() != null && cotizacion.getCobertura().longValue() ==Long.valueOf(valor)){
					String descripcion = "";
					status.setStatus(match);
					CoberturaDTO cobertura = entidad.findById(CoberturaDTO.class, cotizacion.getCobertura());
					if(cobertura != null){
						descripcion = "COBERTURA: " + cobertura.getDescripcion() + ", "; 
					}			
					status.setDescripcion(status.getDescripcion() + descripcion);					
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}				
				break;
			
			case DEDUCIBLE:				
				if(cotizacion.getDeducible() != null && 
						(cotizacion.getDeducible() >= Double.valueOf(valor) && 
								cotizacion.getDeducible() <= Double.valueOf(valorRango))){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "DEDUCIBLE: " + cotizacion.getDeducible() + ", ");				
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case DESCRIPCION_ESTILO:
				if(cotizacion.getDescripcionEstilo() != null && cotizacion.getDescripcionEstilo().trim().toUpperCase().contains(valor.toUpperCase())){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "DESC ESTILO: " + cotizacion.getDescripcionEstilo().replace(",","") + ", ");		
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case ESTADO:
				if(cotizacion.getEstado() != null && cotizacion.getEstado().equals(valor)){
					String descripcion = "";
					status.setStatus(match);
					EstadoDTO obj = entidad.findById(EstadoDTO.class, cotizacion.getEstado());
					if(obj != null){
						descripcion = "ESTADO: " + obj.getDescription()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);						
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case MODELO:
				if(cotizacion.getModelo() != null && (cotizacion.getModelo() >= Integer.valueOf(valor) && 
						cotizacion.getModelo() <=  Integer.valueOf(valorRango))){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "MODELO: " + cotizacion.getModelo() + ", ");				
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case MUNICIPIO:
				if(cotizacion.getMunicipio() != null && cotizacion.getMunicipio().equals(valor)){
					String descripcion = "";
					status.setStatus(match);
					MunicipioDTO obj = entidad.findById(MunicipioDTO.class, cotizacion.getMunicipio());
					if(obj != null){
						descripcion = "MUNICIPIO: " + obj.getDescription()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);	
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case NEGOCIO:
				if(cotizacion.getNegocio() != null && cotizacion.getNegocio().longValue() == Long.valueOf(valor)){
					String descripcion = "";
					status.setStatus(match);
					Negocio obj = entidad.findById(Negocio.class, cotizacion.getNegocio());
					if(obj != null){
						descripcion = "NEGOCIO: " + obj.getDescripcionNegocio()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);				
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			
			case PAQUETE:
				if(cotizacion.getPaquete() != null && cotizacion.getPaquete().longValue() == Long.valueOf(valor)){
					String descripcion = "";
					status.setStatus(match);
					NegocioPaqueteSeccion obj = entidad.findById(NegocioPaqueteSeccion.class, cotizacion.getPaquete());
					if(obj != null){
						descripcion = "PAQUETE: " + obj.getPaquete().getDescripcion()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);	
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case PRODUCTO:
				BigDecimal idProducto = null;
				NegocioProducto negProducto = entidad.findById(NegocioProducto.class, Long.valueOf(valor));
				if(negProducto != null){					
					idProducto = negProducto.getProductoDTO().getIdToProducto();					
					if(cotizacion.getProducto() != null && cotizacion.getProducto().longValue() == idProducto.longValue()){
						String descripcion = "";
						status.setStatus(match);
						ProductoDTO obj = entidad.findById(ProductoDTO.class, cotizacion.getProducto());
						if(obj != null){
							descripcion = "PRODUCTO: " + obj.getDescripcion()+ ", "; 
						}		
						status.setDescripcion(status.getDescripcion() + descripcion);					
					}else{
						status.setStatus(noMatch);
						status.setDescripcion("");
					}
				}
				break;
			
			case SECCION:
				if(cotizacion.getLinea() != null && cotizacion.getLinea().longValue() == Long.valueOf(valor)){					
					String descripcion = "";
					status.setStatus(match);
					NegocioSeccion obj = entidad.findById(NegocioSeccion.class, cotizacion.getLinea());
					if(obj != null){
						descripcion = "SECCION: " + obj.getSeccionDTO().getDescripcion()+ ", "; 
					}		
					status.setDescripcion(status.getDescripcion() + descripcion);			
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case SUMA_ASEGURADA:
				if(cotizacion.getSumaAsegurada() != null && 
						((tipoValor == TipoValor.VALOR_DIRECTO.valor() && cotizacion.getSumaAsegurada() == Double.valueOf(valor)) ||
					     (tipoValor == TipoValor.MENOR_IGUAL.valor() && cotizacion.getSumaAsegurada() <= Double.valueOf(valor)) ||
					     (tipoValor == TipoValor.MAYOR_IGUAL.valor() && cotizacion.getSumaAsegurada() >= Double.valueOf(valor))
						)){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "SUMA ASEGURADA: " + cotizacion.getSumaAsegurada() + ", ");			
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			
			case TIPO_POLIZA:	
				BigDecimal idTipoPoliza = null;
				NegocioTipoPoliza negTipoPoliza = entidad.findById(NegocioTipoPoliza.class, new BigDecimal(valor));
				if(negTipoPoliza != null){					
					idTipoPoliza = negTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza();				
					if(cotizacion.getTipoPoliza() != null && cotizacion.getTipoPoliza().longValue() == idTipoPoliza.longValue()){
						String descripcion = "";
						status.setStatus(match);
						TipoPolizaDTO obj = entidad.findById(TipoPolizaDTO.class, cotizacion.getTipoPoliza());
						if(obj != null){
							descripcion = "TIPO POLIZA: " + obj.getDescripcion()+ ", "; 
						}		
						status.setDescripcion(status.getDescripcion() + descripcion);								
					}else{
						status.setStatus(noMatch);
						status.setDescripcion("");
					}
				}
				break;
			
			case USUARIO:
				status.setStatus(noMatch);
				status.setDescripcion("");
				
				Usuario usuario = usuarioService.getUsuarioActual();
				try{
					if(usuario.isOutsider() && new BigDecimal(valor).equals(BigDecimal.ONE)){
						status.setStatus(match);
						status.setDescripcion("USUARIO: EXTERNO, ");					
					}
					if(!usuario.isOutsider() && new BigDecimal(valor).equals(BigDecimal.ZERO)){
						status.setStatus(match);
						status.setDescripcion("USUARIO: INTERNO, ");					
					}
				}catch(Exception e){
					LOG.error("Usuario no registrado --> Excepcion");
				}
				break;
			
			
			case MODELO_ANTIGUEDAD:
				status.setStatus(noMatch);
				status.setDescripcion("");
				NegocioPaqueteSeccion negocioPaquete = entidad.findById(NegocioPaqueteSeccion.class, cotizacion.getPaquete());
				BigDecimal maxAntiguedad = negocioPaquete.getModeloAntiguedadMax();
				if(maxAntiguedad != null && maxAntiguedad.intValue() > 0){
					GregorianCalendar fechaSeguimiento = new GregorianCalendar();
					fechaSeguimiento.setTime(new Date());
					int anioActual = fechaSeguimiento.get(GregorianCalendar.YEAR);
					int antiguedad = anioActual - cotizacion.getModelo();
					if(antiguedad > maxAntiguedad.intValue()){
						status.setStatus(match);
						status.setDescripcion("ANTIG MODELO MAX: " + maxAntiguedad.intValue() + ", ");
					} 	
				}
				break;
				
			
			case RANGO_VIGENCIA:				
				status.setStatus(noMatch);
				status.setDescripcion("");
				Negocio negocio = entidad.findById(Negocio.class, cotizacion.getNegocio());
				Date vencimientoNegocio = negocio.getFechaFinVigencia();								
				Date actual = new Date();
				Date today =  null;
				try{
					today = format.parse(format.format(actual));
				}catch(Exception ex){
					today = new Date();
				}
				int diasRetroactividad = negocio.getDiasRetroactividad() != null ? negocio.getDiasRetroactividad().intValue() : 0;
				int diasDiferimiento = negocio.getDiasDiferimiento() != null ? negocio.getDiasDiferimiento().intValue() : 0;
				
				if(cotizacion.getVigenciaIncial().getTime() < today.getTime() - diasRetroactividad * 86400000l || 
						cotizacion.getVigenciaIncial().getTime() > today.getTime() + diasDiferimiento * 86400000l){
					status.setStatus(match);
					status.setDescripcion("INICIO VIGENCIA ENTRE Fecha actual - " + diasRetroactividad + " d\u00edas" + " y " +
								"Fecha actual + " + diasDiferimiento + " d\u00edas : " + 
								format.format(cotizacion.getVigenciaIncial()) + ", ");					
				}
				
				if(vencimientoNegocio != null && cotizacion.getVigenciaFinal().getTime() > vencimientoNegocio.getTime()){
					status.setStatus(match);
					status.setDescripcion(status.getDescripcion() + "FIN VIGENCIA MAXIMA " + format.format(vencimientoNegocio) + " : " + 
							format.format(cotizacion.getVigenciaFinal()) + ", ");
				}				
				break;
				
				
			case NUM_SERIE:				
				status.setStatus(noMatch);
				status.setDescripcion("");
				Negocio negocioNumSerie = entidad.findById(Negocio.class, cotizacion.getNegocio());
				if(negocioNumSerie.getAplicaValidacionNumeroSerie()){
					if(!StringUtil.isEmpty(cotizacion.getNumSerie()) && cotizacion.getVigenciaIncial() != null &&
						cotizacion.getVigenciaFinal() != null){
						boolean repetidos = incisoService.existenNumeroSerieRepetidos(cotizacion.getCotizacion(), cotizacion.getInciso(), cotizacion.getNumSerie(), 
							cotizacion.getVigenciaIncial(), cotizacion.getVigenciaFinal());
						if(repetidos){
							status.setStatus(match);
							status.setDescripcion("NUM SERIE REPETIDO:" + cotizacion.getNumSerie() + ", ");
						}
					}
				}
				break;
			
			
			case DESCUENTO_GLOBAL:			
				status.setStatus(noMatch);
				status.setDescripcion("");
				Map<Boolean,Double> resultDescuento = validacionService.validaDescuentoGlobal(cotizacion.getDescuentoGlobal(), cotizacion.getCotizacion());
				if(resultDescuento != null){
					if(resultDescuento.containsKey(true)){
						status.setStatus(match);
						status.setDescripcion("DESCUENTO GLOBAL MAX: " + resultDescuento.get(true) + ", ");
					}
				}
				break;

			case ARTICULO_140:
				status.setStatus(noMatch);
				status.setDescripcion("");
				//TODO REVISAR BANDERA DE SI YA SE VALIDARON LOS MENSAJES
				Negocio negocioArt140 = entidad.findById(Negocio.class, cotizacion.getNegocio());
				if(negocioArt140.getAplicaValidacionArticulo140()){
					if(cotizacion.getIdCliente() != null && cotizacion.getCveTipoPersona() != null){
						if(cotizacion.getCveTipoPersona().shortValue() == 2 || 
							(cotizacion.getCveTipoPersona().shortValue() == 1 && 
									obtenerValorPrimaTotalDolares(cotizacion.getPrimaTotal()) > ExcepcionSuscripcionNegocioAutosServiceImpl.MAX_PRIMA_TOTAL_PF_DLLS)){
							status.setStatus(match);
							status.setDescripcion("ART 140: CLIENTE (" + cotizacion.getIdCliente() + "), ");
						}
					}
				}
				break;
			
			case IGUALACION_PRIMA:
				status.setStatus(noMatch);
				status.setDescripcion("");
				Negocio negocioIgualacion = entidad.findById(Negocio.class, cotizacion.getNegocio());
				if(negocioIgualacion.getAplicaValidacionIgualacionPrimas()){
					if(cotizacion.getIgualacionPrimasCotizacion() != null && cotizacion.getIgualacionPrimasCotizacion().booleanValue()){
						status.setStatus(match);
						status.setDescripcion("IGUALACION PRIMAS COT: " + cotizacion.getDescuentoIgualacionCotizacion() + "%, ");
					}else if(cotizacion.getIgualacionPrimasInciso() != null && cotizacion.getIgualacionPrimasInciso().booleanValue()){
						status.setStatus(match);
						status.setDescripcion("IGUALACION PRIMAS INCISO: " + 
							cotizacion.getInciso() + " - " + cotizacion.getDescuentoIgualacionInciso() + "%, ");
					}
				}
				break;
				
			case INCLUSION_TEXTO:				
				if(cotizacion.getTexAdicionalMap() != null && cotizacion.getTexAdicionalMap().size() > 0){
					StringBuilder ids = new StringBuilder("");
					boolean isFirst = true;
					for(ComboBean item : cotizacion.getTexAdicionalMap()){
						if(isFirst){
							isFirst = false;
						}else{
							ids.append("-");
						}
						ids.append(item.getId()).append("");
					}
					status.setStatus(match);
					status.setDescripcion("INCLUSION DE TEXTO ADICIONAL: " + ids.toString() + ", ");
				}
				break;
			case CONDICION_ESPECIAL_COTIZACION:
				if(cotizacion.getCondicionesCotizacion() != null && cotizacion.getCondicionesCotizacion().size() > 0){
					String descripcion = "";
					StringBuilder descripcionStrBuilder = new StringBuilder();
					status.setStatus(match);
					descripcionStrBuilder.append("CONDICION ESPECIAL A NIVEL COTIZACION: ");
					for(CondicionEspecial condicion: cotizacion.getCondicionesCotizacion()){
						descripcionStrBuilder.append(condicion.getNombre());
						descripcionStrBuilder.append("-");
					}
					descripcion = descripcionStrBuilder.toString();
					status.setDescripcion(status.getDescripcion() + descripcion);				
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			case CONDICION_ESPECIAL_INCISO:
				if(cotizacion.getCondicionesInciso() != null && cotizacion.getCondicionesInciso().size() > 0){
					String descripcion = "";
					StringBuilder descripcionStrBuilder = new StringBuilder();
					status.setStatus(match);
					descripcionStrBuilder.append("CONDICION ESPECIAL A NIVEL INCISO: ");
					for(CondicionEspecial condicion: cotizacion.getCondicionesInciso()){
						descripcionStrBuilder.append(condicion.getNombre());
						descripcionStrBuilder.append("-");
					}
					descripcion = descripcionStrBuilder.toString();
					status.setDescripcion(status.getDescripcion() + descripcion);				
				}else{
					status.setStatus(noMatch);
					status.setDescripcion("");
				}
				break;
			case PCT_DESCUENTO_ESTADO:			
				status.setStatus(noMatch);
				status.setDescripcion("");
				Map<Boolean, String> resultDescuentoPorEstado = validacionService.validaDescuentoPorEstado(cotizacion.getPctDescuentoEstado(), cotizacion.getNegocio(), cotizacion.getEstado(), cotizacion.getCotizacion(), cotizacion.getPaquete());
				if(resultDescuentoPorEstado != null){
					if(resultDescuentoPorEstado.containsKey(true)){
						status.setStatus(match);
						status.setDescripcion("DESCUENTO POR ESTADO MAX: " + resultDescuentoPorEstado.get(true) + ", ");
					}
				}
				break;
			case EMISION_PERSONA_MORAL:
				status.setStatus(noMatch);
				status.setDescripcion("");
				if((cotizacion.getAplicaValidacionPersonaMoral() != null && cotizacion.getCveTipoPersona() != null)
					&& (cotizacion.getAplicaValidacionPersonaMoral() && cotizacion.getCveTipoPersona().shortValue() == 2)){
					status.setStatus(match);
					status.setDescripcion("EMISION DE POLIZA CON PERSONA MORAL CLIENTE: " + cotizacion.getIdCliente().toString());
				}
				break;
			default:
				break;
		}
		
		return status;
	
	}
		
	private double obtenerValorPrimaTotalDolares(Double primaTotal){		
		double cambio = 10.0f; //default
		try{
			cambio = tipoCambioService.obtieneTipoCambioPorDia(new Date(), "SISTEMAS").doubleValue();
		}catch(Exception ex){
			//No se pudo obtener el tipo de cambio. Ya se logueo en la consola y en la tabla de log de errores. 
			//El proceso continua con un tipo de cambio default
			//LogDeMidasEJB3.log("Imposible obtener tipo de cambio", Level.SEVERE, ex);
		}
		return primaTotal / cambio;
	} 
	
	private ExcepcionSuscripcionAutoDescripcionDTO obtenerDatosGeneralesExcepcion(ExcepcionSuscripcion excepcionSuscripcion){
		ExcepcionSuscripcionAutoDescripcionDTO excepcion = new ExcepcionSuscripcionAutoDescripcionDTO();
		//ExcepcionSuscripcion excepcionSuscripcion = excepcionService.listarExcepcion(idToExcepcion);
		excepcion.setIdExcepcion(excepcionSuscripcion.getId());
		excepcion.setNumExcepcion(excepcionSuscripcion.getNumero());
		excepcion.setActivo(excepcionSuscripcion.getActivo());
		excepcion.setDescripcion(StringUtil.isEmpty(excepcionSuscripcion.getDescripcion())?
				null:excepcionSuscripcion.getDescripcion().toUpperCase());
		excepcion.setCorreo(excepcionSuscripcion.getCorreoNotificacion());
		excepcion.setHabilitado(excepcionSuscripcion.getHabilitado());
		excepcion.setNotificacion(excepcionSuscripcion.getRequiereNotificacion());
		excepcion.setAutorizacion(excepcionSuscripcion.getRequiereAutorizacion());
		return excepcion;
	}

	@Override
	public ExcepcionSuscripcionNegocioDescripcionDTO obtenerExcepcion(
			Long idToExcepcion) {
		Object obj = null;
		String descripcion = null;		
		ExcepcionSuscripcion excepcionSuscripcion = entidad.findById(ExcepcionSuscripcion.class, idToExcepcion);
		ExcepcionSuscripcionAutoDescripcionDTO excepcion = obtenerDatosGeneralesExcepcion(excepcionSuscripcion);
		//List<CondicionExcepcion> condiciones = excepcionSuscripcion.getCondicionExcepcion();
		List<CondicionExcepcion> condiciones = excepcionService.obtenerCondiciones(idToExcepcion);
		//inicializar
		excepcion.setUsuario("NI");
		excepcion.setAgente("NI");
		excepcion.setNegocio("NI");
		excepcion.setProducto("NI");
		excepcion.setTipoPoliza("NI");
		excepcion.setLineaNegocio("NI");
		excepcion.setPaquete("NI");
		excepcion.setCobertura("NI");		
		excepcion.setZona("NI, NI");
		excepcion.setSumaAsegurada("SA: NI");
		excepcion.setDeducible("DED: NI");
		
		//agente
		String valor = super.obtenerValor(condiciones, TipoCondicion.AGENTE);
		if(valor != null){
			Agente agente = new Agente();
			try {
				agente.setId(Long.parseLong(valor));
				agente = agenteMidasService.loadById(agente);
				if (agente != null) {
					excepcion.setAgente(agente.getIdAgente().toString());
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		//tipo usuario
		valor = super.obtenerValor(condiciones, TipoCondicion.USUARIO);
		if(valor != null && valor.equals("0")){
			excepcion.setUsuario("INTERNO");
		}else if(valor != null && valor.equals("1")){
			excepcion.setUsuario("EXTERNO");
		}
		
						
		//negocio
		descripcion = null;
		valor =  super.obtenerValor(condiciones, TipoCondicion.NEGOCIO);	
		if(valor != null){
			//obj = entidad.findById(Negocio.class, Long.valueOf(valor));			
			obj = entidad.findById(Negocio.class, Long.valueOf(valor));
			if(obj != null){
				descripcion = ((Negocio)obj).getDescripcionNegocio();
				if(descripcion != null){
					excepcion.setNegocio(descripcion);
				}
			}
			
			//producto
			descripcion = null;
			valor =  super.obtenerValor(condiciones, TipoCondicion.PRODUCTO);
			if(valor != null){
				//obj = entidad.findById(NegocioProducto.class, Long.valueOf(valor));
				obj = entidad.findById(NegocioProducto.class, Long.valueOf(valor));
				if(obj != null){
					descripcion = ((NegocioProducto)obj).getProductoDTO().getDescription();
					if(descripcion != null){
						excepcion.setProducto(descripcion);
					}
				}
				
				//tipo poliza
				descripcion = null;
				valor =  super.obtenerValor(condiciones, TipoCondicion.TIPO_POLIZA);
				if(valor != null){
					//obj = entidad.findById(NegocioTipoPoliza.class, new BigDecimal(valor));
					obj = entidad.findById(NegocioTipoPoliza.class, new BigDecimal(valor));
					if(obj != null){
						descripcion = ((NegocioTipoPoliza)obj).getTipoPolizaDTO().getDescripcion();
						if(descripcion != null){
							excepcion.setTipoPoliza(descripcion);
						}
						
					}
					
					//seccion - linea de negocio
					descripcion = null;
					valor =  super.obtenerValor(condiciones, TipoCondicion.SECCION);
					if(valor != null){
						//obj = entidad.findById(NegocioSeccion.class, new BigDecimal(valor));
						obj = entidad.findById(NegocioSeccion.class, new BigDecimal(valor));
						if(obj != null){
							descripcion = ((NegocioSeccion)obj).getSeccionDTO().getDescripcion();
							if(descripcion != null){
								excepcion.setLineaNegocio(descripcion);
							}
						}
						
						//paquete
						descripcion = null;
						valor =  super.obtenerValor(condiciones, TipoCondicion.PAQUETE);
						if(valor != null){
							//obj = entidad.findById(NegocioPaqueteSeccion.class, Long.valueOf(valor));
							obj = entidad.findById(NegocioPaqueteSeccion.class, Long.valueOf(valor));
							if(obj != null){
								descripcion = ((NegocioPaqueteSeccion)obj).getPaquete().getDescripcion();
								if(descripcion != null){
									excepcion.setPaquete(descripcion);
								}								
							}
						}
						
						//cobertura
						descripcion = null;
						valor =  super.obtenerValor(condiciones, TipoCondicion.COBERTURA);
						if(valor != null){
							//obj = entidad.findById(CoberturaDTO.class, new BigDecimal(valor));
							obj = entidad.findById(CoberturaDTO.class, new BigDecimal(valor));
							if(obj != null){
								descripcion = ((CoberturaDTO)obj).getDescripcion();
								if(descripcion != null){
									excepcion.setCobertura(descripcion);
								}
							}
							
							//valores suma asegurada
							descripcion = null;
							valor =  super.obtenerValor(condiciones, TipoCondicion.SUMA_ASEGURADA);						
							if(valor != null){	
								descripcion = "SA >= " + valor;
							}else{
								descripcion = "SA: NI";
							}
							excepcion.setSumaAsegurada(descripcion);		
							
							
							//valores deducuble
							descripcion = null;
							List<String> valores =  super.obtenerValores(condiciones, TipoCondicion.DEDUCIBLE);
							descripcion = "DED: ";
							if(!valores.isEmpty()){	
								descripcion = descripcion + valores.get(0) + " - " + valores.get(1);
							}else{
								descripcion = descripcion + "NI";
							}
							excepcion.setDeducible(descripcion);								
						}													
					}			
				}			
			}
		}
		
		//vehiculo
		descripcion = null;
		valor =  super.obtenerValor(condiciones, TipoCondicion.AMIS);
		descripcion = "AMIS: " + (valor != null ? valor + " - " : "NI - ");
		valor =  super.obtenerValor(condiciones, TipoCondicion.DESCRIPCION_ESTILO);
		descripcion = descripcion + "ESTILO: " + (valor != null ? valor : "NI");
		excepcion.setAmisDescripcionVehiculo(descripcion);
		
		//modelo
		descripcion = obtenerDescripcion(super.obtenerValores(condiciones, TipoCondicion.MODELO));
		excepcion.setRangoModelos(!descripcion.equals("")  ? descripcion : "NI");
		
		//estado - municipio
		descripcion = null;
		valor =  super.obtenerValor(condiciones, TipoCondicion.ESTADO);
		if(valor != null){		
			obj = entidad.findById(EstadoDTO.class, valor);			
			descripcion = obj != null ? ((EstadoDTO)obj).getStateName(): "NI";			
			valor =  super.obtenerValor(condiciones, TipoCondicion.MUNICIPIO);	
			if(valor != null){		
				obj = entidad.findById(MunicipioDTO.class, valor);				 
				descripcion = (descripcion != null ? descripcion : "NI") + ", " + 
				(((MunicipioDTO)obj).getMunicipalityName() != null ? ((MunicipioDTO)obj).getMunicipalityName() : "NI");			
			}
		}		
		if(descripcion != null){
			excepcion.setZona(descripcion);	
		}
		return excepcion;
	}
	
	public String obtenerDescripcion(List<String> modelo) {
		if(!modelo.isEmpty())
			return " "+StringUtils.join(modelo.iterator()," ");
		return	"";
	}

	@Override
	public void eliminarExcepcion(Long idToExcepion) {
		excepcionService.removerExcepcion(idToExcepion);		
	}


	@Override
	public short obtenerTipoCondicionValor(Long idToExcepcion, TipoCondicion tipoCondicion) {
		short tipoValor = 0;
		CondicionExcepcion condicion = excepcionService.obtenerCondicion(idToExcepcion, tipoCondicion);
		if(condicion != null){
			tipoValor = condicion.getTipoValor();
		}
		return tipoValor;
	}

	@Override
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(
			BigDecimal idCotizacion, BigDecimal idLineaNegocio,
			BigDecimal idInciso) {
		// TODO Auto-generated method stub
		return null;
	}
	
	/**
	 * Clase de apoyo para evaluar los estatus de las excepciones
	 * @author Softnet01
	 *
	 */
	public class ConvertidorEstatus{
		
		private Integer status = null;
		
		private String descripcion = "";
		
		public Integer getStatus() {
			return status;
		}
		public void setStatus(Integer status) {
			this.status = status;
		}
		public String getDescripcion() {
			return descripcion;
		}
		public void setDescripcion(String descripcion) {
			this.descripcion = descripcion;
		}
		
	}
	@EJB
	public void setValidacionService(ValidacionService validacionService) {
		this.validacionService = validacionService;
	}

	@EJB
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	public NegocioCondicionEspecialService getNegocioCondEspecialService() {
		return negocioCondEspecialService;
	}
	@EJB
	public void setNegocioCondEspecialService(
			NegocioCondicionEspecialService negocioCondEspecialService) {
		this.negocioCondEspecialService = negocioCondEspecialService;
	}
	
}
