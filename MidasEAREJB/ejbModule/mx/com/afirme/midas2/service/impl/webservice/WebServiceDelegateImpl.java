package mx.com.afirme.midas2.service.impl.webservice;

import java.net.URL;
import java.rmi.RemoteException;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.webservice.WebServiceDelegate;

import com.afirme.eibs.services.payments.EibsPaymentsService;
import com.afirme.eibs.services.payments.EibsPaymentsServiceServiceLocator;
import com.afirme.eibs.services.payments.vo.CreditCardPromotionVO;
import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;
import com.afirme.ws.archivos.AvisoPrivacidad;

@Stateless
public class WebServiceDelegateImpl implements WebServiceDelegate {
	
	private enum EIBSServiceType {
		USER_SERVICE("EibsUserService"),
		LIFE_INSURANCE_SERVICE("EibsLifeInsuranceService"),
		PAYMENT_SERVICE("EibsPaymentsService"),
		CHECK_PAYMENT_SERVICE("EibsPaymentsService"),
		ACCOUNT_SERVICE("EibsAccountService");
		
		private String endpoint;
		
		private EIBSServiceType(String endpoint){
			this.endpoint = endpoint;
		}
		
		public String getEndpoint(){
			return endpoint;
		}
	}
	
	private SistemaContext contexto = null;
	
	private PrintReport initPrintReportService(){
		try{
			PrintReportServiceLocator locator = new PrintReportServiceLocator();
			
			String endpoint = contexto.getPrintReportEndpoint();
			
			return locator.getPrintReport(new URL(endpoint));
		}catch(Exception e){
			return null;
		}
	}
	
	private URL getEibsServiceURL(EIBSServiceType type){
		try{
			String baseUrl = contexto.getEibsServiceURL();
			
			return new URL(baseUrl + type.getEndpoint());
		}catch(Exception e){
			return null;
		}
	}
	
	private EibsPaymentsService initEibsPaymentsService(){
		try{
			EibsPaymentsServiceServiceLocator locator = new EibsPaymentsServiceServiceLocator();
			return locator.getEibsPaymentsService(getEibsServiceURL(EIBSServiceType.PAYMENT_SERVICE));
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public byte[] obtenerAvisoPrivacidad(AvisoPrivacidad aviso) throws RemoteException{
		
		PrintReport pr = this.initPrintReportService();
		
		String rootDir = contexto.getRootDirFiles();
		
		return pr.getFileFromDisk(rootDir + aviso.getArchivo());
	}
	
	@Override
	public byte[] obtenerCartaDeCargosAutomaticos(String identifierId) throws RemoteException{
		PrintReport pr = this.initPrintReportService();
		
		//El parametro "contentType" no es utilizado
		return pr.getAccountChargeLetter(identifierId, null);
	}
	
	@Override
	public CreditCardPromotionVO[] obtenerPromocionesTC(String numTarjeta)throws RemoteException{
		EibsPaymentsService service = this.initEibsPaymentsService();
		
		CreditCardPromotionVO[] promociones = null;
		
		if(numTarjeta != null && !numTarjeta.trim().equals(""))
			promociones = service.getTDCPromotions(numTarjeta);
		else
			promociones = new CreditCardPromotionVO[0];
		
		return promociones;
	}
	
	@EJB
	public void setContexto(SistemaContext contexto){
		this.contexto = contexto;
	}

}
