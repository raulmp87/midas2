package mx.com.afirme.midas2.service.impl.siniestros.liquidacion;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.institucionbancaria.BancoDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoFacadeRemote;
import mx.com.afirme.midas.interfaz.endoso.EndosoIDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.siniestros.liquidacion.LiquidacionSiniestroDao;
import mx.com.afirme.midas2.dao.siniestros.pagos.PagosSiniestroDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.DireccionMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina.ClaveTipoCalculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro.EstatusAutorizacionIndemnizacion;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.RecepcionDocumentosSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro.EstatusLiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros.ParametroAutorizacionLiquidacion;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.parametros.ParametroAutorizacionLiquidacionDetalle;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal.EstatusDocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.notasDeCredito.ConjuntoOrdenCompraNota;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestroDetalle;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.DetalleCuentasContablesDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.DetalleOrdenExpedicionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.ImpresionOrdenExpedicionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.FacturaLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.LiquidacionSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionDTO;
import mx.com.afirme.midas2.dto.siniestros.liquidacion.ParamAutLiquidacionRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.DesglosePagoConceptoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas.RecepcionFacturaServiceImpl;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.liquidacion.LiquidacionSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.bloqueo.BloqueoPagoService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionSalvamentoService;
import mx.com.afirme.midas2.service.siniestros.solicitudcheque.SolicitudChequeService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.tarea.EmisionPendienteService;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;

@Stateless
public class LiquidacionSiniestroServiceImpl implements LiquidacionSiniestroService{
	
	public  static final Logger log = Logger.getLogger(LiquidacionSiniestroServiceImpl.class);
	
	@EJB
	private LiquidacionSiniestroDao liquidacionSiniestroDao;
	
	@EJB 
	private SolicitudChequeService solicitudChequeService ;
	
	@EJB
	BitacoraService bitacoraService;
	
	@EJB
	private BloqueoPagoService bloqueoPagoService;

	@EJB
	private EntidadService entidadService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@EJB
	CatalogoGrupoValorService  catalogoGrupoValorService;
	
	@EJB
	RecepcionFacturaService recepcionFacturaService;
	
	@EJB
	private PagosSiniestroService pagosSiniestroService;
	
	@EJB
	private PagosSiniestroDao pagosSiniestroDao;	
	
	@EJB 
	EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB
	RecuperacionDeducibleService recuperacionDeducibleService;
	
	public static final String CODIGOCORREO = "CANCELACION_ORDENPAGO";
	
	@EJB
	private DireccionMidasService direccionService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB
	private EndosoService endosoService;
	
	@EJB
	private EmisionPendienteService emisionPendienteService;
	
	@EJB
	private EntidadContinuityService entidadContinuityService;
	
	@EJB
	private BancoFacadeRemote bancoFacadeRemote;
	
	@EJB
	private OrdenCompraService ordenCompraService;
	
	@EJB
	private RecuperacionSalvamentoService recuperacionSalvamentoService;
	
	private static final String SOLICITUD_CHEQUE_NATURALEZA_CARGO = "C";
	private static final String SOLICITUD_CHEQUE_NATURALEZA_ABONO = "A";
	private static final String CONCEPTO_PAGO_SINIESTROS = "PAGO DE SINIESTROS";
	private static final String FOLIO_FACTURA_EN_CARTA_PAGO = "0";
	
	private static final String ORDEN_EXPEDICION_CHEQUE = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionCheque.jrxml";
	private static final String ORDEN_EXPEDICION_DETALLE = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionDetalle.jrxml";
	private static final String ORDEN_EXPEDICION_CUENTAS = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionCuentas.jrxml";
	private static final String ORDEN_EXPEDICION_DETALLES_NOTAS_CREDITO =
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionDetalleNotasCredito.jrxml";
	
	private static final String ORDEN_EXPEDICION_CHEQUE_BENEFICIARIO = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionChequeBeneficiario.jrxml";
	private static final String ORDEN_EXPEDICION_DETALLE_BENEFICIARIO = 
		"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionDetalleBeneficiario.jrxml";

	@Override
	public LiquidacionSiniestro guardarLiquidacion(
			LiquidacionSiniestro liquidacionSiniestro) {    
		 
		 if(liquidacionSiniestro.getProveedor() != null 
				 && liquidacionSiniestro.getProveedor().getId() != null)
		 {
			 PrestadorServicio proveedor = entidadService.findById(PrestadorServicio.class, liquidacionSiniestro.getProveedor().getId());
			 liquidacionSiniestro.setProveedor(proveedor);
		 }
		 else
		 {
			 liquidacionSiniestro.setProveedor(null);	
			 String beneficiarioDecodificado = (StringUtil.decodificadorUri(liquidacionSiniestro.getBeneficiario() ));
			 liquidacionSiniestro.setBeneficiario(beneficiarioDecodificado);
		 }
		 
		 liquidacionSiniestro.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		 liquidacionSiniestro.setFechaModificacion(new Date());
		 liquidacionSiniestro = this.entidadService.save(liquidacionSiniestro);		 
		 
		 return liquidacionSiniestro;	
	}

	@Override
	public LiquidacionSiniestro asociarFactura(DocumentoFiscal factura, Long idLiquidacion) throws Exception 
	{		
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();			
		
		LiquidacionSiniestro liquidacion = this.entidadService.findById(LiquidacionSiniestro.class,idLiquidacion);
		
		for(OrdenCompra ordenCompra : factura.getOrdenesCompra())
		{			
			OrdenPagoSiniestro ordenPago = ordenCompra.getOrdenPago();
			ordenPago.setEstatus(PagosSiniestroService.ESTATUS_ASOCIADA);		
			
			entidadService.save(ordenPago);				
		}
		  
		if(factura.getConjuntoOrdenCompraNota() != null)
		{
			for(ConjuntoOrdenCompraNota conjunto : factura.getConjuntoOrdenCompraNota())
			{
				for(DocumentoFiscal notaCredito : conjunto.getNotasDeCredito())
				{
					notaCredito.setEstatus(EstatusDocumentoFiscal.ASOCIADA.getValue());
					entidadService.save(notaCredito);					
				}
			}			
		}		
		
		liquidacion.getFacturas().add(factura);
		
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("", " Ha ocurrido un error al asociar una factura a la liquidacion");
		}	
		
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;
	}	
	
	
	@Override
	public LiquidacionSiniestro asociaFacturas(String idFacturas, Long idLiquidacion) throws Exception 
	{		
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();			
		
		LiquidacionSiniestro liquidacion = this.entidadService.findById(LiquidacionSiniestro.class,idLiquidacion);
		
		String[] facturasIdsArray = idFacturas.trim().split(",");
		for(String facturaIdStr : facturasIdsArray){
			DocumentoFiscal factura = this.entidadService.findById(DocumentoFiscal.class, Long.valueOf(facturaIdStr));
			
			for(OrdenCompra ordenCompra : factura.getOrdenesCompra())
			{			
				OrdenPagoSiniestro ordenPago = ordenCompra.getOrdenPago();
				ordenPago.setEstatus(PagosSiniestroService.ESTATUS_ASOCIADA);		
				entidadService.save(ordenPago);				
			}
			  
			if(factura.getConjuntoOrdenCompraNota() != null)
			{
				for(ConjuntoOrdenCompraNota conjunto : factura.getConjuntoOrdenCompraNota())
				{
					for(DocumentoFiscal notaCredito : conjunto.getNotasDeCredito())
					{
						notaCredito.setEstatus(EstatusDocumentoFiscal.ASOCIADA.getValue());
						entidadService.save(notaCredito);					
					}
				}			
			}
			
			liquidacion.getFacturas().add(factura);
		}
		
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("", " Ha ocurrido un error al asociar una factura a la liquidacion");
		}	
		
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;
	}

	@Override
	public LiquidacionSiniestro desasociarFactura(DocumentoFiscal factura, Long idLiquidacion) throws Exception 
	{	
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();		
		
		LiquidacionSiniestro liquidacion = this.entidadService.findById(LiquidacionSiniestro.class,idLiquidacion);
		
		for(OrdenCompra ordenCompra : factura.getOrdenesCompra())
		{			
			OrdenPagoSiniestro ordenPago = ordenCompra.getOrdenPago();
			ordenPago.setEstatus(PagosSiniestroService.ESTATUS_LIBERADA);		
			
			entidadService.save(ordenPago);				
		}
		
		if(factura.getConjuntoOrdenCompraNota() != null)
		{
			for(ConjuntoOrdenCompraNota conjunto : factura.getConjuntoOrdenCompraNota())
			{
				for(DocumentoFiscal notaCredito : conjunto.getNotasDeCredito())
				{
					notaCredito.setEstatus(EstatusDocumentoFiscal.REGISTRADA.getValue());
					entidadService.save(notaCredito);					
				}
			}			
		}		
		
		liquidacion.getFacturas().remove(factura);
		
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("", "  Ha ocurrido un error al desasociar una factura a la liquidacion");
		}
		
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;
	}
	
	@Override
	public LiquidacionSiniestro desasociarFacturas(String idFacturas, Long idLiquidacion) throws Exception 
	{	
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();		
		
		LiquidacionSiniestro liquidacion = this.entidadService.findById(LiquidacionSiniestro.class,idLiquidacion);
		
		String[] facturasIdsArray = idFacturas.trim().split(",");
		for(String facturaIdStr : facturasIdsArray){
			DocumentoFiscal factura = this.entidadService.findById(DocumentoFiscal.class, Long.valueOf(facturaIdStr));
			for(OrdenCompra ordenCompra : factura.getOrdenesCompra())
			{			
				OrdenPagoSiniestro ordenPago = ordenCompra.getOrdenPago();
				ordenPago.setEstatus(PagosSiniestroService.ESTATUS_LIBERADA);		
				
				entidadService.save(ordenPago);				
			}
			
			if(factura.getConjuntoOrdenCompraNota() != null)
			{
				for(ConjuntoOrdenCompraNota conjunto : factura.getConjuntoOrdenCompraNota())
				{
					for(DocumentoFiscal notaCredito : conjunto.getNotasDeCredito())
					{
						notaCredito.setEstatus(EstatusDocumentoFiscal.REGISTRADA.getValue());
						entidadService.save(notaCredito);					
					}
				}			
			}		
			
			liquidacion.getFacturas().remove(factura);
		}
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("", "  Ha ocurrido un error al desasociar una factura a la liquidacion");
		}
		
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;
	}


	@Override
	public List<LiquidacionSiniestroRegistro> buscarLiquidacionDetalle(LiquidacionSiniestroFiltro filtro){
		
		List<LiquidacionSiniestroRegistro> lLiquidacionSiniestroRegistro = liquidacionSiniestroDao.buscarLiquidacionDetalle(filtro);
		
		if( !lLiquidacionSiniestroRegistro.isEmpty() ){
			Collections.sort(lLiquidacionSiniestroRegistro,
				new Comparator<LiquidacionSiniestroRegistro>() {				
					public int compare(LiquidacionSiniestroRegistro l1, LiquidacionSiniestroRegistro l2){
						return l2.getIdLiquidacion().compareTo(l1.getIdLiquidacion());
					}
			});
		}
		return lLiquidacionSiniestroRegistro;
	}

	@Override
	public List<LiquidacionSiniestroRegistro> buscarLiquidacionDetalle(Map<String,String> estatus, String origenLiquidacion ){
		
		List<LiquidacionSiniestroRegistro> lkLiquidacionSiniestroRegistro = new LinkedList<LiquidacionSiniestroRegistro>();
		//List<LiquidacionSiniestroRegistro> lTempLiquidaciones = new LinkedList<LiquidacionSiniestroRegistro>();
		LiquidacionSiniestroFiltro filtro = new LiquidacionSiniestroFiltro();
		
		// DEFINE SI ES PROVEEDOR O BENEFICIARIO
		filtro.setOrigenLiquidacion(origenLiquidacion);
		
		// OBTENER TODAS POR AUTORIZAR
		filtro.setEstatusLiquidacion("XAUT");
		List<LiquidacionSiniestroRegistro> lPorAutorizar = liquidacionSiniestroDao.buscarLiquidacionDetalle(filtro);
		// ORDENAR LAS POR AUTORIZAR DE FECHA MAYOR A MENOR
		if( !lPorAutorizar.isEmpty() ){
			Collections.sort(lPorAutorizar,
				new Comparator<LiquidacionSiniestroRegistro>() {				
					public int compare(LiquidacionSiniestroRegistro l1, LiquidacionSiniestroRegistro l2){
						return l2.getFechaSolicitud().compareTo(l1.getFechaSolicitud());
					}
			});
			
			// AGREGAR A LISTA ORDENADA
			lkLiquidacionSiniestroRegistro.addAll(lPorAutorizar);
		}
		
		
		// OBTENER LOS DEMAS ESTATUS
		/*if( estatus != null  ){
			if( !estatus.isEmpty() ){
				for (Map.Entry<String, String> mEstatus : estatus.entrySet()){
					if( !mEstatus.getKey().equals("ELIM") & !mEstatus.getKey().equals("XAUT") ){
						filtro.setEstatusLiquidacion(mEstatus.getKey());
						lTempLiquidaciones.addAll(liquidacionSiniestroDao.buscarLiquidacionDetalle(filtro));
					}

				}
			}
		}*/
		// ORDENAR LOS DEMAS ESTATUS
		// ORDENAR LAS POR AUTORIZAR DE FECHA MAYOR A MENOR
		/*if( !lTempLiquidaciones.isEmpty() ){
			Collections.sort(lTempLiquidaciones,
				new Comparator<LiquidacionSiniestroRegistro>() {				
					public int compare(LiquidacionSiniestroRegistro l1, LiquidacionSiniestroRegistro l2){
						return l2.getIdLiquidacion().compareTo(l1.getIdLiquidacion());
					}
			});
			
			// AGREGAR A LISTA ORDENADA
			lkLiquidacionSiniestroRegistro.addAll(lTempLiquidaciones);
		}*/
		
		return lkLiquidacionSiniestroRegistro;
	}	

	@Override
	public void validarEnvioSolicitudLiquidacion(Long idLiquidacion) throws Exception {

		LiquidacionSiniestro liquidacionSiniestro=this.entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		if(null==liquidacionSiniestro){
     	   	throw new Exception(String.format("La liquidacion "+idLiquidacion + " No existe"));
		}
		String pagosBloqueados="";
		boolean tienePagoBloqueado = false;
		
		if(liquidacionSiniestro.getOrigenLiquidacion().equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())){
			/*Valida Bloqueo Prestador*/
			if(null!=liquidacionSiniestro.getProveedor()){
				if (bloqueoPagoService.estaElProovedorBloqueado(liquidacionSiniestro.getProveedor().getId())){
					throw new Exception("No se puede realizar la Autorización de la Liquidación ya que se cuenta con los siguientes Proveedores Bloqueados # " 
							+ liquidacionSiniestro.getProveedor().getId() + " " + liquidacionSiniestro.getProveedor().getNombrePersona() +  ".");
					
				}
			}
			/*Valida Bloqueo Pagos*/
			List<DocumentoFiscal> lstFacturas = liquidacionSiniestro.getFacturas();
			for (DocumentoFiscal factura :lstFacturas){
				List<OrdenCompra> lstOrdenesCompra =factura.getOrdenesCompra();
				for(OrdenCompra ordencompra : lstOrdenesCompra){
					if(null!=ordencompra.getOrdenPago()){
							if(bloqueoPagoService.estaElPagoBloqueadoDirectamente(ordencompra.getOrdenPago().getId()) || 
									bloqueoPagoService.estaElPagoBloqueadoPorProovedor(ordencompra.getOrdenPago().getId())	){
								tienePagoBloqueado=true;
								pagosBloqueados+="	"+ordencompra.getOrdenPago().getId();
							}
					}
					
				}
				
			}
		}else if(liquidacionSiniestro.getOrigenLiquidacion().equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue())){
			
			for(OrdenPagoSiniestro ordenPago : liquidacionSiniestro.getOrdenesPago()){
				if(bloqueoPagoService.estaElPagoBloqueadoDirectamente(ordenPago.getId()) || 
							bloqueoPagoService.estaElPagoBloqueadoPorProovedor(ordenPago.getId())	){
							tienePagoBloqueado=true;
							pagosBloqueados+="	"+ordenPago.getId();
				}
				
				
			}
		}
		
		if (tienePagoBloqueado){
			throw new Exception("No se puede Autorizar la Liquidacion ya que se cuenta con las siguientes Ordene(s) de Pago Bloqueada(s)"+": "+pagosBloqueados );
			
		}
	
	}
	
	@Override
	public void enviarSolicitudLiquidacion(Long idLiquidacion) throws Exception {
		this.validarEnvioSolicitudLiquidacion(idLiquidacion);
		LiquidacionSiniestro liquidacionSiniestro=this.entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		SolicitudChequeSiniestro solicitudCheque = null;
		if(liquidacionSiniestro.getOrigenLiquidacion().equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue())){
			solicitudCheque =solicitudChequeService.solicitarChequeMizar(idLiquidacion,SolicitudChequeSiniestro.OrigenSolicitud.LIQUIDACION_SINIESTRO_BENEFICIARIO);
		}else if(liquidacionSiniestro.getOrigenLiquidacion().equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())){
			 solicitudCheque =solicitudChequeService.solicitarChequeMizar(idLiquidacion,SolicitudChequeSiniestro.OrigenSolicitud.LIQUIDACION_SINIESTRO_PROVEEDOR);
		}else{
			throw new Exception(String.format("Error al solicitar cheque, registro sin origen liquidacion."));
		}
		solicitudChequeService.migrarChequeMizar(solicitudCheque.getId());
			
		
		 //TODO revisar si aplica esto
		/*List<DetalleLiquidacionSiniestro> lstDetalle = this.entidadService.findByProperty(DetalleLiquidacionSiniestro.class, "liquidacionSiniestro.id", liquidacionSiniestro.getId());
			for(DetalleLiquidacionSiniestro detalle: lstDetalle){
				//	detalle.setEstatus(LiquidacionSiniestroService.ESTATUS_PROCESADA);
					this.entidadService.save(detalle);
			}
			liquidacionSiniestro.setSolicitudCheque(solicitudCheque);
			liquidacionSiniestro.setEstatus(LiquidacionSiniestroService.ESTATUS_PROCESADA);
			liquidacionSiniestro.setCodigoUsuarioModificacion(String.valueOf(  usuarioService.getUsuarioActual().getNombreUsuario() ));
			liquidacionSiniestro.setFechaModificacion(new Date());
			this.guardarLiquidacion(liquidacionSiniestro);*/
		
		this.guardarLiquidacion(liquidacionSiniestro, EstatusLiquidacionSiniestro.LIBERADA_CON_SOLICITUD);
		
		reactivarRecuperacionesSalvamentos(liquidacionSiniestro);
	}
	
	private String obtenerLocalidadBeneficiario(LiquidacionSiniestro liquidacion){
		String localidadBeneficiario = "";
		if(liquidacion != null
				&& liquidacion.getOrdenesPago() != null	){
			for(OrdenPagoSiniestro orden : liquidacion.getOrdenesPago()){
				if(orden.getOrdenCompra() != null
						&& orden.getOrdenCompra().getReporteCabina() != null
						&& orden.getOrdenCompra().getReporteCabina().getLugarOcurrido() != null){
					LugarSiniestroMidas ocurrido = orden.getOrdenCompra().getReporteCabina().getLugarOcurrido();
					localidadBeneficiario += 
						(ocurrido.getCiudad() != null 
							&& ocurrido.getCiudad().getDescripcion() != null)? 
									( ocurrido.getCiudad().getDescripcion() + ", " + ocurrido.getCiudad().getEstado().getDescripcion() )
									: "";
					if(!localidadBeneficiario.isEmpty()){break;}
		}}}
		return localidadBeneficiario;
	}

	@Override
	public TransporteImpresionDTO imprimirOrdenExpedicionCheque(Long idLiquidacion){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		
		List<LiquidacionSiniestro> liquidaciones = entidadService.findByProperty(LiquidacionSiniestro.class, "id", idLiquidacion);
		LiquidacionSiniestro liquidacion = liquidaciones.get(0);
		
		JasperReport jReport = null;
		JasperReport jReporteDetalleOrden = null;
		JasperReport jReporteDetalleNotasCredito = null;
		JasperReport jReporteDetalleCuentas = gImpresion.getOJasperReport(ORDEN_EXPEDICION_CUENTAS);
		
		if(liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())){
			jReport = gImpresion.getOJasperReport(ORDEN_EXPEDICION_CHEQUE);
			jReporteDetalleOrden = gImpresion.getOJasperReport(ORDEN_EXPEDICION_DETALLE);
			jReporteDetalleNotasCredito = gImpresion.getOJasperReport(ORDEN_EXPEDICION_DETALLES_NOTAS_CREDITO);
		}else{
			jReport = gImpresion.getOJasperReport(ORDEN_EXPEDICION_CHEQUE_BENEFICIARIO);
			jReporteDetalleOrden = gImpresion.getOJasperReport(ORDEN_EXPEDICION_DETALLE_BENEFICIARIO);
		}
		
		List<ImpresionOrdenExpedicionDTO> dataSourceImpresion = new ArrayList<ImpresionOrdenExpedicionDTO>();
		ImpresionOrdenExpedicionDTO informacionOrden = 	obtenerInformacionOrdenExpedicion(liquidacion);
		
		dataSourceImpresion.add(informacionOrden);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.NUEVO_LOGO_SEGUROS_AFIRME);
		params.put("ES_PREVIEW", Boolean.FALSE); //solo si viene FALSE se imprime sin marca de agua. EDIT: ya no se va a usar la marca de agua
		params.put("jReporteDetalleOrden", jReporteDetalleOrden);
		params.put("jReporteDetalleCuentas", jReporteDetalleCuentas);
		params.put("jReporteDetalleNotasCredito", jReporteDetalleNotasCredito);
		
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}
	
	/**
	 * obtiene la informacion necesaria para generar el reporte de orden de expedicion de cheques
	 * @param idLiquidacion
	 * @return
	 */
	private ImpresionOrdenExpedicionDTO obtenerInformacionOrdenExpedicion(LiquidacionSiniestro liquidacion){
		ImpresionOrdenExpedicionDTO ordenExpedicion = new ImpresionOrdenExpedicionDTO();
		ImpresionOrdenExpedicionDTO ordenExpedicionBySP = new ImpresionOrdenExpedicionDTO();
			
			if(liquidacion != null){

				if(liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue())){
					ordenExpedicion.setaFavor(liquidacion.getBeneficiario());					//A FAVOR (BENEFICIARIO)
					ordenExpedicion.setCuenta(													//CUENTA
							liquidacion.getClabeBeneficiario());
					ordenExpedicion.setBanco(													//BANCO
							(liquidacion.getBancoBeneficiario() != null
									&& StringUtil.isNumeric(liquidacion.getBancoBeneficiario()))? 
									this.buscarNombreBancoPorId(Integer.parseInt(liquidacion.getBancoBeneficiario())) : "");
					ordenExpedicion.setCorreo(liquidacion.getCorreo());							//CORREO
					ordenExpedicion.setTelefono(												//TELEFONO
							((!StringUtil.isEmpty(liquidacion.getTelefonoLada()))? liquidacion.getTelefonoLada() : "")
								+ ((!StringUtil.isEmpty(liquidacion.getTelefonoNumero()))? liquidacion.getTelefonoNumero() : "") );
					ordenExpedicion.setLocalidad(												//LOCALIDAD
							this.obtenerLocalidadBeneficiario(liquidacion));
					/*INICIA ESPACIO SOLAMENTE PARA BENEFICIARIOS*/
					
					/*TERMINA ESPACIO SOLAMENTE PARA BENEFICIARIOS*/
					
				}else if(liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())){
					
					ordenExpedicionBySP = liquidacionSiniestroDao.getDatosImportesLiquidacionProv(liquidacion.getId());
					//A FAVOR (PROVEEDOR)
					ordenExpedicion.setaFavor(ordenExpedicionBySP.getaFavor());
					//LOCALIDAD
					ordenExpedicion.setLocalidad(ordenExpedicionBySP.getLocalidad());
					//CORREO
					ordenExpedicion.setCorreo(ordenExpedicionBySP.getCorreo());
					//TELEFONO
					ordenExpedicion.setTelefono(ordenExpedicionBySP.getTelefono());
					//CUENTA
					ordenExpedicion.setCuenta(ordenExpedicionBySP.getCuenta());
					//BANCO
					ordenExpedicion.setBanco(ordenExpedicionBySP.getBanco());
					//USUARIO QUE SOLICITA
					//ordenExpedicion.setNombreCompletoUsuarioSolicita(ordenExpedicionBySP.getNombreCompletoUsuarioSolicita());
					/*
					//IMPORTES LIQUIDACION
					//SUBTOTAL
					ordenExpedicion.setSubtotal(ordenExpedicionBySP.getSubtotal());
					//DESCUENTO
					ordenExpedicion.setDescuento(ordenExpedicionBySP.getDescuento());
					//IVA
					ordenExpedicion.setIvaAcred(ordenExpedicionBySP.getIvaAcred());
					//IVA RETENIDO
					ordenExpedicion.setIvaRet(ordenExpedicionBySP.getIvaRet());
					//ISR RETENIDO
					ordenExpedicion.setIsrRet(ordenExpedicionBySP.getIsrRet());
					//NETO POR PAGAR
					ordenExpedicion.setaPagar(ordenExpedicionBySP.getaPagar());
					*/
					/*
					PrestadorServicio prestador = liquidacion.getProveedor(); 	
					ordenExpedicion.setaFavor(prestador.getNombrePersona());					//A FAVOR (PROVEEDOR)
					if(prestador.getPersonaMidas() != null){
							if(prestador.getPersonaMidas().getPersonaDireccion() != null
							&& !prestador.getPersonaMidas().getPersonaDireccion().isEmpty()
							&& prestador.getPersonaMidas().getPersonaDireccion().get(0) != null){
						DireccionMidas direccion = prestador.getPersonaMidas().getPersonaDireccion().get(0).getDireccion();
						CiudadMidas ciudad = null;
						if(direccion.getColonia() != null){
							ColoniaMidas colonia = entidadService.findById(ColoniaMidas.class, direccion.getColonia().getId());
							ciudad = direccionService.obtenerCiudadPorColonia(colonia.getId());
							//ciudad = direccionService.obtenerCiudadPorCP(colonia.getCodigoPostal());
						}else{
							ciudad = direccionService.obtenerCiudadPorCP(direccion.getCodigoPostalColonia());
						}
						if(ciudad != null){
							ordenExpedicion.setLocalidad(										//LOCALIDAD
									ciudad.getDescripcion() + ", " + ciudad.getEstado().getDescripcion());
							}}
							if(prestador.getPersonaMidas().getContacto() != null){
								ordenExpedicion.setCorreo(										//CORREO
										prestador.getPersonaMidas().getContacto().getCorreoPrincipal());
								ordenExpedicion.setTelefono(									//TELEFONO
										prestador.getPersonaMidas().getContacto().getTelCasaCompleto());
								ordenExpedicion.setCuenta(										//CUENTA
										(prestador.getCuentaActiva() != null)? prestador.getCuentaActiva().getClabe() : null);				
								ordenExpedicion.setBanco(										//BANCO
										(prestador.getCuentaActiva() != null)? 
												this.buscarNombreBancoPorId(prestador.getCuentaActiva().getBancoId()) : null);
					}}
					*/
					DetalleOrdenExpedicionDTO importesLiquidacion = 
						new DetalleOrdenExpedicionDTO();										//IMPORTES DE LA LIQUIDACION
					importesLiquidacion.setSubtotal(liquidacion.getSubtotal());					//SUBTOTAL
					importesLiquidacion.setDescuento(liquidacion.getDescuento());				//DESCUENTO
					importesLiquidacion.setIvaAcred(liquidacion.getIva());						//IVA
					importesLiquidacion.setIvaRet(liquidacion.getIvaRet());						//IVA RETENIDO
					importesLiquidacion.setIsrRet(liquidacion.getIsr());						//ISR RETENIDO
					importesLiquidacion.setaPagar(liquidacion.getNetoPorPagar());				//NETO POR PAGAR
					ordenExpedicion.setImportesLiquidacion(importesLiquidacion);
					
					obtenerDetallesNotasCredito(liquidacion, ordenExpedicion);
				}
				
				ordenExpedicion.setPorConcepto(													//POR CONCEPTO DE
						CONCEPTO_PAGO_SINIESTROS);
						
				ordenExpedicion.setFechaSolicitud(liquidacion.getFechaPorAut());				//FECHA DE SOLICITUD
				
				ordenExpedicion.setLiquidacionEgreso(											//LIQUIDACION DE EGRESO
						liquidacion.getNumeroLiquidacion().toString());			

				if(!StringUtil.isEmpty(liquidacion.getTipoOperacion())){						//TIPO DE OPERACION (no aplica,otros,servicios profesionales)
					Map<String, String> tiposOperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_OPERACION);
					ordenExpedicion.setTipoOperacion(tiposOperacion.get(liquidacion.getTipoOperacion()));			
				}
				if(liquidacion.getSolicitadoPor() != null 
						&& !liquidacion.getSolicitadoPor().isEmpty() 
						&& liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())){
					ordenExpedicion.setUsuarioSolicita(ordenExpedicionBySP.getNombreCompletoUsuarioSolicita()); //USUARIO QUE SOLICITA PROVEEDOR
				}else{
					Usuario usuarioSolicita = usuarioService.buscarUsuarioPorNombreUsuario(liquidacion.getSolicitadoPor());
					String nombreUsuarioSolicita = (usuarioSolicita!=null)? usuarioSolicita.getNombreCompleto():liquidacion.getSolicitadoPor(); 
					ordenExpedicion.setUsuarioSolicita(nombreUsuarioSolicita);						//USUARIO QUE SOLICITA BENEFICIARIO
				}
				
				ordenExpedicion.setSolicitudChequeMizar(										//SOLICITUD CHEQUE MIZAR
						(liquidacion.getSolicitudCheque() != null)? liquidacion.getSolicitudCheque().getIdSolCheque() : null);
				
				ordenExpedicion.setFechaElaboracionCheque(										//FECHA ELABORACION CHEQUE
						(liquidacion.getSolicitudCheque() != null)? liquidacion.getSolicitudCheque().getFechaPago() : null);
				
				ordenExpedicion.setNumeroChequeReferencia(										//NO. DE CHEQUE/REFERENCIA DE MIZAR
						(liquidacion.getSolicitudCheque() != null)? liquidacion.getSolicitudCheque().getCheque() : null);
				
				if(!StringUtil.isEmpty(liquidacion.getTipoLiquidacion())){						//TIPO (Tipo de Liquidacion: cheque, transferencia bancaria)
					Map<String, String> tiposLiquidacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
					ordenExpedicion.setTipo(tiposLiquidacion.get(liquidacion.getTipoLiquidacion()));
				}
				
				ordenExpedicion.setNumeroPagos(													//NUMERO DE PAGOS
						obtenerNumeroPagosLiquidacion(liquidacion));												
				//change for pdf print into stored procedure. by joksrc
				ordenExpedicion.setDetallesOrden(												//DETALLES DE ORDEN DE EXPEDICION DE CHEQUES
						(liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue()))?
								obtenerDetallesOrdenExpedicionProvSP(liquidacion)
						: obtenerDetallesOrdenExpedicionBeneficiario(liquidacion));	
				
				ordenExpedicion.setTotal(this.calcularTotalPagosDeLiquidacion(
						ordenExpedicion.getDetallesOrden()));									//DETALLES DE ORDEN DE EXPEDICION (TOTALES)
				
				ordenExpedicion.setDetalleCuentas(obtenerDetallesCuentas(liquidacion));			//DETALLES DE CUENTAS CONTABLES
				llenarTotalesDetalleCuenta(														//DETALLES DE CUENTAS CONTABLES (CARGO TOTAL)
						ordenExpedicion, ordenExpedicion.getDetalleCuentas());					//DETALLES DE CUENTAS CONTABLES (ABONO TOTAL)
				
				if(liquidacion.getCodigoUsuarioCreacion() != null 
						&& !liquidacion.getCodigoUsuarioCreacion().isEmpty()){
					Usuario usuarioElaborado = usuarioService.buscarUsuarioPorNombreUsuario(liquidacion.getCodigoUsuarioCreacion());
					String nombreUsuarioElaborado = (usuarioElaborado!=null)? usuarioElaborado.getNombreCompleto():liquidacion.getCodigoUsuarioCreacion();
					ordenExpedicion.setElaborado(nombreUsuarioElaborado);						//ELABORADO - USUARIO QUE CREO LA LIQUIDACION
				}
				
				if(esLiquidacionAutorizadaImpresion(liquidacion)
						&& liquidacion.getAutorizadoPor() != null
						&& !liquidacion.getAutorizadoPor().isEmpty()){
					String codigoUsuarioAutorizador = liquidacion.getAutorizadoPor();
					Usuario usuarioAprobado = usuarioService.buscarUsuarioPorNombreUsuario(codigoUsuarioAutorizador);
					String nombreUsuarioAprobado = (usuarioAprobado!=null)? usuarioAprobado.getNombreCompleto():codigoUsuarioAutorizador;
					ordenExpedicion.setAprobado(nombreUsuarioAprobado);							//APROBADO - USUARIO QUE AUTORIZO LA LIQUIDACION
				}
				

					ordenExpedicion.setRamo(													//RAMO (vida, autos, danios)
						obtenerClaveNegocio(liquidacion));
				
				ordenExpedicion.setObservaciones(												//OBSERVACIONES
						liquidacion.getComentarios());
			}
	
		return ordenExpedicion;
	}
	
	/**
	 * Busca el nombre del banco dependiendo de la id que se le pase como parametro
	 * @param bancoId
	 * @return
	 */
	private String buscarNombreBancoPorId(Integer bancoId){
		String nombreBanco = "";
		
		if(bancoId != null){
			BancoDTO banco = bancoFacadeRemote.findById(bancoId);
			if(banco != null){
				nombreBanco = banco.getNombreBanco();
		}}
		
		return nombreBanco;
	}
	
	/**
	 * Obtiene los detalles de todas las notas de credito asociadas a la liquidacion
	 * y calcula la suma de los totales de las de credito
	 * @param liquidacion
	 * @param ordenExpedicion
	 * @return
	 */
	private BigDecimal obtenerDetallesNotasCredito(LiquidacionSiniestro liquidacion, ImpresionOrdenExpedicionDTO ordenExpedicion){
		List<DetalleOrdenExpedicionDTO> detallesNotasCredito = new ArrayList<DetalleOrdenExpedicionDTO>();
		BigDecimal totalNotasCredito = new BigDecimal(0);
		
		if(liquidacion.getRecuperaciones() != null
				&& !liquidacion.getRecuperaciones().isEmpty()){
			for(RecuperacionProveedor recuperacion : liquidacion.getRecuperaciones()){
				if(recuperacion.getNotasdeCredito() != null
						&& !recuperacion.getNotasdeCredito().isEmpty()){
					for(DocumentoFiscal notaCredito : recuperacion.getNotasdeCredito()){
						DetalleOrdenExpedicionDTO detalleNota = new DetalleOrdenExpedicionDTO();
						detalleNota.setFactura(notaCredito.getNumeroFactura());
						detalleNota.setSubtotal(notaCredito.getSubTotal());
						detalleNota.setIvaAcred(notaCredito.getIva());
						detalleNota.setIvaRet(notaCredito.getIvaRetenido());
						detalleNota.setIsrRet(notaCredito.getIsr());
						detalleNota.setaPagar(notaCredito.getMontoTotal());
						totalNotasCredito = totalNotasCredito.add(notaCredito.getMontoTotal());
						detallesNotasCredito.add(detalleNota);
					}
				}
			}
		}
		ordenExpedicion.setDetallesNotasCredito(detallesNotasCredito);
		ordenExpedicion.setTotalNotasCredito(totalNotasCredito);
		
		return totalNotasCredito;
	}
	
	/**
	 * Obtiene el numero de pagos de la liquidacion
	 * @param liquidacion
	 * @return
	 */
	private Integer obtenerNumeroPagosLiquidacion(LiquidacionSiniestro liquidacion){
		Integer numeroPagos = 0;
		
		if(liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())){
			for(DocumentoFiscal factura : liquidacion.getFacturas()){
				if(factura.getOrdenesCompra() != null){
					numeroPagos += factura.getOrdenesCompra().size();
				}
			}
		}else if(liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue())){
			for(OrdenPagoSiniestro ordenPago : liquidacion.getOrdenesPago()){
				if(ordenPago.getOrdenCompra() != null){
					numeroPagos++;
				}
			}
		}
		
		return numeroPagos;
	}

	/**
	 * Get information for liquidation details through stored procedure. This for PDF Print, Expedition Cheque.
	 * @param liquidacion
	 * @return resumenLiquidacion
	 */
	private List<DetalleOrdenExpedicionDTO> obtenerDetallesOrdenExpedicionProvSP(LiquidacionSiniestro liquidacion){
		List<DetalleOrdenExpedicionDTO> resumenLiquidacion = new ArrayList<DetalleOrdenExpedicionDTO>();
		
		if(liquidacion != null
				&& liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())
				&& liquidacion.getFacturas() != null ){
			resumenLiquidacion = liquidacionSiniestroDao.getResumenLiquidacionProveedor(liquidacion.getId());
		}
		
		return resumenLiquidacion;
	}
	
	/**
	 * Obtiene la informacion de los detalles de liquidacion para la orden de expedicion de cheques para proveedor
	 * @param liquidacion
	 * @return
	 */
	@Deprecated
	private List<DetalleOrdenExpedicionDTO> obtenerDetallesOrdenExpedicion(LiquidacionSiniestro liquidacion){
		List<DetalleOrdenExpedicionDTO> detallesOrden = new ArrayList<DetalleOrdenExpedicionDTO>();
	
		if(liquidacion != null
				&& liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())
				&& liquidacion.getFacturas() != null ){
			
			for(DocumentoFiscal factura : liquidacion.getFacturas()){
				
				if(factura != null 
						&& factura.getOrdenesCompra() != null){
					
					for(OrdenCompra ordenCompra : factura.getOrdenesCompra()){
						
						if(ordenCompra != null){ 
							DetalleOrdenExpedicionDTO detalleOrden = new DetalleOrdenExpedicionDTO();
							
							if(ordenCompra.getReporteCabina() != null){
								
								if(ordenCompra.getReporteCabina().getSeccionReporteCabina() != null
									&& ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() != null
									&& ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina() != null){
										Map<String,String> terminosAjusteSiniestro = 
											listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
										AutoIncisoReporteCabina autoInciso = 					//TERMINO AJUSTE
											ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
										detalleOrden.setTerminoAjuste(terminosAjusteSiniestro.get(autoInciso.getTerminoAjuste()));										
									}
								
								if(ordenCompra.getReporteCabina().getSiniestroCabina() != null){
									detalleOrden.setSiniestro(									//SINIESTRO
											ordenCompra.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
								}
							}
							
							if(ordenCompra.getOrdenPago() != null){
								OrdenPagoSiniestro ordenPago = ordenCompra.getOrdenPago();
								if(ordenPago.getId() != null){
									detalleOrden.setOrdenPago(ordenPago.getId().toString());	//ORDEN DE PAGO
								}
								
								List<DocumentoFiscal> facturasRegistradas = recepcionFacturaService.obtenerFacturasRegistradaByOrdenCompra(
										ordenCompra.getId(), RecepcionFacturaServiceImpl.FACTURA_REGISTRADA);
								List<DocumentoFiscal> facturasPagadas = recepcionFacturaService.obtenerFacturasRegistradaByOrdenCompra(
										ordenCompra.getId(), RecepcionFacturaServiceImpl.FACTURA_PAGADA);
								facturasPagadas.addAll(facturasRegistradas);
								if(!facturasPagadas.isEmpty()){
									detalleOrden.setFactura(									//FACTURA
										(facturasPagadas.get(0).getNumeroFactura().equals(FOLIO_FACTURA_EN_CARTA_PAGO))? 
												(ordenCompra.getCartaPago() != null
														&& !ordenCompra.getCartaPago().getSiniestroTercero().isEmpty()) ? 
														ordenCompra.getCartaPago().getSiniestroTercero() 
														: facturasPagadas.get(0).getNumeroFactura()
												: facturasPagadas.get(0).getNumeroFactura());
								}
								
								if(ordenPago.getOrdenCompra().getDetalleOrdenCompras() != null){
									llenarCantidadesDetalleOrdenExpedicionCheque(				//CALCULA LAS CANTIDADES POR ORDEN DE PAGO
											detalleOrden, ordenPago);				
								}
							}
							detallesOrden.add(detalleOrden);
						}
					}
				}
			}
		}
					
		return detallesOrden;
	}
	
	/**
	 * Obtiene la informacion de los detalles de liquidacion para la orden de expedicion de cheques para beneficiario
	 * @param liquidacion
	 * @return
	 */
	private List<DetalleOrdenExpedicionDTO> obtenerDetallesOrdenExpedicionBeneficiario(LiquidacionSiniestro liquidacion){
		List<DetalleOrdenExpedicionDTO> detallesOrden = new ArrayList<DetalleOrdenExpedicionDTO>();
	
		if(liquidacion != null
				&& liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue())
				&& liquidacion.getOrdenesPago() != null){
			
			for(OrdenPagoSiniestro ordenPago : liquidacion.getOrdenesPago()){
				
				if(ordenPago != null 
						&& ordenPago.getOrdenCompra() != null){
					
						OrdenCompra ordenCompra = ordenPago.getOrdenCompra();
						
						if(ordenCompra != null){ 
							DetalleOrdenExpedicionDTO detalleOrden = new DetalleOrdenExpedicionDTO();
							
							if(ordenCompra.getReporteCabina() != null){
								
								if(ordenCompra.getReporteCabina().getSeccionReporteCabina() != null
									&& ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina() != null
									&& ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina() != null){
										Map<String,String> terminosAjusteSiniestro = 
											listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
										AutoIncisoReporteCabina autoInciso = 					//TERMINO AJUSTE
											ordenCompra.getReporteCabina().getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina();
										detalleOrden.setTerminoAjuste(terminosAjusteSiniestro.get(autoInciso.getTerminoAjuste()));										
									}
								
								if(ordenCompra.getReporteCabina().getSiniestroCabina() != null){
									detalleOrden.setSiniestro(									//SINIESTRO
											ordenCompra.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
								}
							}
							
							detalleOrden.setDescuento(ordenCompra.getDescuento());				//DESCUENTO
							
							detalleOrden.setPrimasPendientesPago(								//PRIMAS PENDIENTES DE PAGO
									(tieneIndemnizacionesPT(ordenPago))?
									liquidacion.getPrimaPendientePago():
										BigDecimal.ZERO);						
							
							detalleOrden.setPrimasADevolver(									//PRIMAS A DEVOLVER
									(tieneIndemnizacionesPT(ordenPago))?
									liquidacion.getPrimaNoDevengada():
										BigDecimal.ZERO);	
							
							if(ordenPago.getId() != null){
								detalleOrden.setOrdenPago(ordenPago.getId().toString());		//ORDEN DE PAGO
							}
							
							
							if(ordenPago.getOrdenCompra().getDetalleOrdenCompras() != null){
								llenarCantidadesDetalleOrdenExpedicionCheque(					//CALCULA LAS CANTIDADES POR ORDEN DE PAGO
										detalleOrden, ordenPago);				
							}
							
							detallesOrden.add(detalleOrden);
						}
				}
			}
		}
					
		return detallesOrden;
	}
	
	/**
	 * Suma los valores de los detalles de las ordenes de pago para obtener las cantidades del reporte de liquidacion
	 * @param detalleOrden
	 * @param detallesOrdenPago
	 */
	private void llenarCantidadesDetalleOrdenExpedicionCheque(DetalleOrdenExpedicionDTO detalleOrden, OrdenPagoSiniestro ordenPago){
		
		if(detalleOrden != null
				&& ordenPago != null){
			
			//the original call is obtenerTotales. This change is just for PDF print.
			DesglosePagoConceptoDTO desglosePago = pagosSiniestroService.obtenerTotalesSP(ordenPago.getId(),false);
			
			detalleOrden.setConcepto(
					desglosePago.getConceptoPago());										 	//CONCEPTO
			detalleOrden.setSubtotal(desglosePago.getSubTotalPorPagar());						//SUBTOTAL
			detalleOrden.setIvaAcred(desglosePago.getIvaTotalPorPagar());						//IVA ACRED.
			detalleOrden.setIvaRet(desglosePago.getIvaRetenidoTotalPorPagar());					//IVA RET.
			detalleOrden.setIsrRet(desglosePago.getIsrTotalPorPagar());							//ISR RET.
			BigDecimal totalPorPagar = (desglosePago.getTotalesPorPagar() != null)? desglosePago.getTotalesPorPagar() : BigDecimal.ZERO;
//			El total pagado ya no se suma al total por pagar debido a un cambio en las ordenes de compra
//			BigDecimal totalPagado = (desglosePago.getTotalesPagados() != null)? desglosePago.getTotalesPagados() : BigDecimal.ZERO;
			BigDecimal primasPendientes = (detalleOrden.getPrimasPendientesPago() != null)? detalleOrden.getPrimasPendientesPago() : BigDecimal.ZERO;
			BigDecimal primasDevolver = (detalleOrden.getPrimasADevolver() != null)? detalleOrden.getPrimasADevolver() : BigDecimal.ZERO;
//			BigDecimal aPagar = totalPorPagar.add(totalPagado).add(primasDevolver).subtract(primasPendientes);
			BigDecimal aPagar = totalPorPagar.add(primasDevolver).subtract(primasPendientes);
			detalleOrden.setaPagar(aPagar);														//A PAGAR
			
			BigDecimal deducible = new BigDecimal(0);
			if(ordenPago.getOrdenCompra() != null && ordenPago.getOrdenCompra().getAplicaDeducible().booleanValue()
					&& !ordenCompraService.deducibleYaRecuperado(ordenPago.getOrdenCompra())){
				if(tieneIndemnizacionesPT(ordenPago)){
						if(ordenPago.getOrdenCompra().getReporteCabina() != null){
							deducible = recuperacionDeducibleService.obtenerDeducibleSiniestroNoRCV(ordenPago.getOrdenCompra().getReporteCabina().getId());
						}
				}else if(ordenPago.getOrdenCompra().getIdTercero() != null){
					deducible = recuperacionDeducibleService.obtenerDeduciblePorEstimacion(ordenPago.getOrdenCompra().getIdTercero());
				}
			}
			detalleOrden.setDeducible(deducible);												//DEDUCIBLES
		}
	}
	
	private BigDecimal calcularTotalPagosDeLiquidacion(List<DetalleOrdenExpedicionDTO> detalles){
		BigDecimal total = new BigDecimal(0);
		
		for(DetalleOrdenExpedicionDTO detalle: detalles){
			total = total.add((detalle.getaPagar() != null)? detalle.getaPagar() : BigDecimal.ZERO);
		}
		
		return total;
	}
	
	
	/**
	 * obtiene los detalles de las cuentas contables para la orden de expedicion de cheques
	 * @param liquidacion
	 * @return
	 */
	private List<DetalleCuentasContablesDTO> obtenerDetallesCuentas(LiquidacionSiniestro liquidacion){
		List<DetalleCuentasContablesDTO> detalles = new ArrayList<DetalleCuentasContablesDTO>();
		
		if(liquidacion != null
				&& liquidacion.getSolicitudCheque() != null
				&& liquidacion.getSolicitudCheque().getSolicitudChequeSiniestroDetalle() != null){
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("solicitudChequeSiniestro.id", liquidacion.getSolicitudCheque().getId());
			List<SolicitudChequeSiniestroDetalle> detallesCheque = 
				entidadService.findByPropertiesWithOrder(SolicitudChequeSiniestroDetalle.class, params, "cuentaContable ASC");
			
			for(SolicitudChequeSiniestroDetalle detalleCheque: detallesCheque){
				DetalleCuentasContablesDTO detalleCuenta = new DetalleCuentasContablesDTO();
				
				detalleCuenta.setCuenta(detalleCheque.getCuentaContable());				//CUENTA
				
				if( liquidacion.getSolicitudCheque().getIdMoneda() != null){
					Map<Long, String> monedas = listadoService.getMapMonedas();			//MONEDA
					detalleCuenta.setMoneda(monedas.get(liquidacion.getSolicitudCheque().getIdMoneda()));
				}
				
				if(detalleCheque.getRubro() != null){
					Map<String, String> rubros = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CONCEPTOS_RUBROS_SOLICITUD_CHEQUE);
					if(rubros != null 
							&& !rubros.isEmpty()){										//CONCEPTO
						detalleCuenta.setConcepto(rubros.get(detalleCheque.getRubro()));
					}
				}
				
				if(detalleCheque.getNaturaleza().compareTo(SOLICITUD_CHEQUE_NATURALEZA_ABONO) == 0){
					detalleCuenta.setAbono(detalleCheque.getImporte());					//ABONO (NATURALEZA A)
					detalleCuenta.setCargo(BigDecimal.ZERO);							//CARGO	(NATURALEZA A)
				}else if(detalleCheque.getNaturaleza().compareTo(SOLICITUD_CHEQUE_NATURALEZA_CARGO) == 0){
					detalleCuenta.setAbono(BigDecimal.ZERO);							//ABONO (NATURALEZA C)
					detalleCuenta.setCargo(detalleCheque.getImporte());					//CARGO	(NATURALEZA C)
				}
				detalles.add(detalleCuenta);
			}
		}
		return detalles;
	}
	
	/**
	 * Calcula los totales de los detalles de cuenta y los asigna a la orden de expedicion que se pasa como parametro
	 * @param ordenExpedicion
	 * @param detallesCuenta
	 */
	private void llenarTotalesDetalleCuenta(ImpresionOrdenExpedicionDTO ordenExpedicion, List<DetalleCuentasContablesDTO> detallesCuenta){
		BigDecimal abonoTotal = new BigDecimal(0);
		BigDecimal cargoTotal = new BigDecimal(0);
		for(DetalleCuentasContablesDTO detalleCuenta: detallesCuenta){
			abonoTotal = abonoTotal.add((detalleCuenta.getAbono() != null)? detalleCuenta.getAbono(): BigDecimal.ZERO);
			cargoTotal = cargoTotal.add((detalleCuenta.getCargo() != null)? detalleCuenta.getCargo(): BigDecimal.ZERO);
		}
		ordenExpedicion.setAbonoTotal(abonoTotal);
		ordenExpedicion.setCargoTotal(cargoTotal);
	}
	
	@Override
	public String obtenerClaveNegocio(Long idLiqSiniestro){
		
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiqSiniestro);
		String claveNegocio = obtenerClaveNegocio(liquidacion);
		
		return claveNegocio;
	}
	
	/**
	 * Obtiene la clave de negocio del primer reporte de la liquidacion que tenga valor.
	 * @param liquidacion
	 * @return
	 */
	private String obtenerClaveNegocio(LiquidacionSiniestro liquidacion){
		String claveNegocio = "";
		for(DocumentoFiscal factura: liquidacion.getFacturas()){
			if(factura != null 
					&& factura.getOrdenesCompra() != null){
				for(OrdenCompra ordenCompra : factura.getOrdenesCompra()){
						if(ordenCompra.getReporteCabina() != null
							&& !StringUtil.isEmpty(ordenCompra.getReporteCabina().getClaveNegocio())){
						Map<String,String> ramosLiq = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_LIQUIDACION);
						claveNegocio = ramosLiq.get(ordenCompra.getReporteCabina().getClaveNegocio());
						break;
					}
				}
				if(!StringUtil.isEmpty(claveNegocio)){
					break;
					}
			}
		}
		return claveNegocio;
	}
	
	@Override
	public Boolean validarImpresionLiquidacion(Long idLiqSiniestro){
		Boolean esImprimible = false;
		
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiqSiniestro);
		
		if(liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue())){
			List<DocumentoFiscal> facturas = liquidacion.getFacturas();
			if(facturas != null){
				for(DocumentoFiscal factura: facturas){
					if(factura.getOrdenesCompra() != null){
						for(OrdenCompra ordenCompra: factura.getOrdenesCompra()){
							if(ordenCompra.getOrdenPago() != null){
								esImprimible = true;
								break;
							}}}
					if(esImprimible){
						break;
		}}}}else if(liquidacion.getOrigenLiquidacion().equals(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue())){
			List<OrdenPagoSiniestro> ordenesPago = liquidacion.getOrdenesPago();
			if(ordenesPago != null && !ordenesPago.isEmpty()){
				esImprimible = true;
		}}
		return esImprimible;
	}
	
	/**
	 * Metodo para determinar si se debe mostrar el campo AUTORIZADO en la impresion de la liquidacion
	 * @param liquidacion
	 * @return
	 */
	private Boolean esLiquidacionAutorizadaImpresion(LiquidacionSiniestro liquidacion){
		Boolean esAutorizada = Boolean.FALSE;
		if(liquidacion.getEstatus().compareTo(LiquidacionSiniestro.EstatusLiquidacionSiniestro.LIBERADA_CON_SOLICITUD.getValue()) == 0 
				|| liquidacion.getEstatus().compareTo(LiquidacionSiniestro.EstatusLiquidacionSiniestro.APLICADA.getValue()) == 0){
			esAutorizada = Boolean.TRUE;
		}
		return esAutorizada;
	}
	

	@Override
	public void eliminarLiquidacion(Long idLiquidacion) throws Exception
	{
		LiquidacionSiniestro liquidacion = this.entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		desasociaFacturasLiquidacion(liquidacion);
		
		this.guardarLiquidacion(liquidacion, EstatusLiquidacionSiniestro.ELIMINADA);		
	}
	
	private void desasociaFacturasLiquidacion(LiquidacionSiniestro liquidacion) throws Exception{
		
		for(DocumentoFiscal factura:liquidacion.getFacturas())
		{
			this.desasociarFactura(factura, liquidacion.getId());
			
		}
		/*for(DetalleLiquidacionSiniestro detalle : liquidacion.getDetalleLiquidacionSiniestro()){
			OrdenPagoSiniestro pago = detalle.getOrdenPagoSiniestro();
			pago.setEstatus(PagosSiniestroService.ESTATUS_LIBERADA);
			this.entidadService.save(pago);
			detalle.setOrdenPagoSiniestro(null);
		}*/
	}
	
	@Override
	public void reexpedir(Long idLiquidacion) throws Exception
	{
		LiquidacionSiniestro liquidacionSiniestro = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		
		if(liquidacionSiniestro.getSolicitudCheque() != null &&
				(liquidacionSiniestro.getSolicitudCheque().getSituacion().trim()).
				equalsIgnoreCase(SolicitudChequeSiniestro.EstatusSolicitudCheque.CANCELADA_MIZAR.getValue()))
		{			
				this.bitacoraService.registrar(TIPO_BITACORA.LIQUIDACION, EVENTO.MODIFICACION_LIQUIDACION, liquidacionSiniestro.getId().toString(), 
						 String.format("Se remueve solicitud de cheque previamente asociada. SolicitudChequeSiniestroId = %1$s ", 
								 liquidacionSiniestro.getSolicitudCheque().getId().toString()), 
						 liquidacionSiniestro.getSolicitudCheque(), usuarioService.getUsuarioActual().getNombreUsuario());
				
				liquidacionSiniestro.setSolicitudCheque(null);
				this.guardarLiquidacion(liquidacionSiniestro, EstatusLiquidacionSiniestro.POR_AUTORIZAR);
				
			
		}else
		{
			throw new NegocioEJBExeption("_", "No es posible realizar la Re Expedici\u00F3n de la solicitud del Cheque ya que " +
					"se requiere la cancelaci\u00F3n de la solicitud actual");			
		}		
	}
	/*joksrc: 
	 * inside Liquidaciones for this method, is needed to call obtenerTotalesSP, 
	 * that contains method ObtenerDetalleOrdenPago and this execute new procedure.*/
	@Override
	public void solicitarAutorizacion(Long idLiquidacion) throws Exception
	{
		
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		//here get obtenerTotalesSP
		validarMontoNCRsRecuperacion(liquidacion);
		
		validarSeleccionPrimasPdtsPago(liquidacion);
		
		this.guardarLiquidacion(liquidacion, EstatusLiquidacionSiniestro.POR_AUTORIZAR);
		
	}
	
	@Override
	public List<DocumentoFiscal> buscarFacturasLiquidacion(Long idLiquidacion){
		
		List<DocumentoFiscal> listaFacturasLiquidacion = new ArrayList<DocumentoFiscal>();

		// OBTENER DATOS
		listaFacturasLiquidacion = liquidacionSiniestroDao.buscarFacturasLiquidacion(idLiquidacion);		
		// ORDENAR FACTURAS
		return this.ordenaFacturasLiquidacion(listaFacturasLiquidacion);
	
		 
	}
	
	@Override
	public List<FacturaLiquidacionDTO> buscarOrdenesPago(Long idLiquidacion)
	{		
		List<FacturaLiquidacionDTO> listaOrdenesPagoSiniestros = new ArrayList<FacturaLiquidacionDTO>();			
		
		listaOrdenesPagoSiniestros = liquidacionSiniestroDao.buscarOrdenesPago(idLiquidacion);
		
		for(FacturaLiquidacionDTO registro:listaOrdenesPagoSiniestros)
		{				
			DesglosePagoConceptoDTO desglose;
			desglose = pagosSiniestroService.obtenerTotales(registro.getOrdenPago().getId(),Boolean.FALSE);
			registro.setDesgloseConceptos(desglose);
		}
		
		return listaOrdenesPagoSiniestros;		
	}	
	
	@Override
	public List<Map<String, Object>> buscarProveedoresLiquidacion()
	{
		List<Map<String, Object>> listaPrestadoresServicio = new ArrayList<Map<String, Object>>();
		List<PrestadorServicioRegistro> list = this.liquidacionSiniestroDao.buscarProveedoresLiquidacion();
		for(PrestadorServicioRegistro prestador : list){
			Map<String, Object> proveedorMap = new HashMap<String, Object>();		
		
				proveedorMap.put("id", prestador.getId());
				proveedorMap.put("label", prestador.getNombrePersona().replaceAll("\"", "'"));
				proveedorMap.put("oficina", prestador.getNombreOficinaDesc());
				listaPrestadoresServicio.add(proveedorMap);					
		}
		return listaPrestadoresServicio;
	}


	@Override
	public void solicitarChequeLiquidacion(Long idLiquidacion) throws Exception {
		LiquidacionSiniestro liquidacionSiniestro=this.entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		if(null==liquidacionSiniestro){
     	   	throw new Exception(String.format("La liquidacion "+idLiquidacion + " No existe"));
		}	
		SolicitudChequeSiniestro solicitudCheque =null;
		if(liquidacionSiniestro.getOrigenLiquidacion().equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.toString())){
			solicitudCheque =solicitudChequeService.solicitarChequeMizar(idLiquidacion,SolicitudChequeSiniestro.OrigenSolicitud.LIQUIDACION_SINIESTRO_BENEFICIARIO);
		}else if(liquidacionSiniestro.getOrigenLiquidacion().equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.toString())){
			 solicitudCheque =solicitudChequeService.solicitarChequeMizar(idLiquidacion,SolicitudChequeSiniestro.OrigenSolicitud.LIQUIDACION_SINIESTRO_PROVEEDOR);
		}else{
			throw new Exception(String.format("Error al solicitar Cheque, registro sin origen liquidacion."));
		}
		liquidacionSiniestro.setSolicitudCheque(solicitudCheque);
		this.guardarLiquidacion(liquidacionSiniestro);
	}
	
	@Override
	public void rechazarLiquidacion(Long idLiquidacion) {
	    
	    LiquidacionSiniestro liquidacionSiniestro = this.entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
	    this.guardarLiquidacion(liquidacionSiniestro, EstatusLiquidacionSiniestro.EN_TRAMITE_POR_RECHAZO);
	}


	@Override
	public List<ParamAutLiquidacionRegistro> buscarParametrosAutorizacion(ParamAutLiquidacionDTO filtro) {
		List<ParamAutLiquidacionRegistro> resultados = this.liquidacionSiniestroDao.buscarParametrosAutorizacion(filtro);
		acompletaDescripcionesDeParametroRegistro(resultados);
		return resultados;
	}
	
	private void acompletaDescripcionesDeParametroRegistro(List<ParamAutLiquidacionRegistro> registros){
		final String TODOS = "Todos";
		Map<String,String> estatusMap =  this.listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ESTATUS);
		Map<String,String> origenLiquidacionMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ORIGEN_LIQUIDACION);
		Map<String,String> tipoLiquidacionMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
		Map<String,String> tipoPagoMap = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_ORDEN_COMPRA);
		for(ParamAutLiquidacionRegistro registro : registros){
			Boolean estatus = registro.getEstatus();
			String origen = registro.getLiquidacion();
			String tipoLiquidacion = registro.getTipoLiquidacion();
			String tipoPago = registro.getTipoPago();
			if(estatus != null){
				if(estatus){
					registro.setEstatusDesc(estatusMap.get("1"));
				}else{
					registro.setEstatusDesc(estatusMap.get("0"));
				}
			}else{
				registro.setEstatusDesc(TODOS);
			}
			if(origen != null && !origen.equals("")){
				registro.setLiquidacionDesc(origenLiquidacionMap.get(origen));
			}else{
				registro.setLiquidacionDesc(TODOS);
			}
			if(tipoLiquidacion != null && !tipoLiquidacion.equals("")){
				registro.setTipoLiquidacionDesc((tipoLiquidacionMap.get(tipoLiquidacion)));
			}else{
				registro.setTipoLiquidacionDesc(TODOS);
			}
			if(tipoPago != null && !tipoPago.equals("")){
				registro.setTipoPagoDesc((tipoPagoMap.get(tipoPago)));
			}else{
				registro.setTipoPagoDesc(TODOS);
			}
		}
	}


	@Override
	public ParametroAutorizacionLiquidacion obtenerParametroAutorizacion(Long idParametro) {
		return this.entidadService.findById(ParametroAutorizacionLiquidacion.class, idParametro);
	}


	@Override
	public String guardarParametroAutorizacion(ParametroAutorizacionLiquidacion parametro, String oficinasConcat,
			String estadosConcat, String proveedoresConcat,
			String usuariosSolConcat, String usuariosAutConcat) {
		// TODO Auto-generated method stub
		
		String mensajeError = this.validaGuardadoParametroAutorizacion(oficinasConcat, parametro.getTipoServicio(), estadosConcat, 
				proveedoresConcat, usuariosSolConcat, usuariosAutConcat, parametro.getVigenciaInicial(), parametro.getMontoInicial(), parametro.getId());
		
		if(mensajeError==null){
			String usuario = this.usuarioService.getUsuarioActual().getNombreUsuario();
			if(parametro.getId()!=null){
				if(!parametro.getEstatus()){
					ParametroAutorizacionLiquidacion parametroGuardado = this.entidadService.findById(ParametroAutorizacionLiquidacion.class, parametro.getId());
					parametroGuardado.setEstatus(parametro.getEstatus());
					parametroGuardado.setFechaInactivo(new Date());
					parametroGuardado.setCodigoUsuarioModificacion(usuario);
					parametroGuardado.setFechaModificacion(new Date());
					this.entidadService.save(parametroGuardado);
				}
			}else{
				parametro.setFechaCreacion(new Date());
				parametro.setCodigoUsuarioCreacion(usuario);
				parametro.setCodigoUsuarioModificacion(usuario);
				parametro.setEstatus(Boolean.TRUE);
				parametro.setNumero(this.liquidacionSiniestroDao.obtenerSiguienteNumeroDeLiquidacion());
				parametro.setNombreUsuarioConfigurador(usuarioService.getUsuarioActual().getNombreCompleto());
				List<ParametroAutorizacionLiquidacionDetalle> detalles = new ArrayList<ParametroAutorizacionLiquidacionDetalle>();
				
				List<ParametroAutorizacionLiquidacionDetalle> detallesPorTipo = new ArrayList<ParametroAutorizacionLiquidacionDetalle>();
				
				detallesPorTipo = this.generaListaDetalleDeParametro(parametro, ParametroAutorizacionLiquidacionDetalle.TipoParametroAutLiquidacion.OFICINA, oficinasConcat, usuario);
				if(detallesPorTipo!=null){
					detalles.addAll(detallesPorTipo);
				}
				detallesPorTipo = this.generaListaDetalleDeParametro(parametro, ParametroAutorizacionLiquidacionDetalle.TipoParametroAutLiquidacion.ESTADO, estadosConcat, usuario);
				if(detallesPorTipo!=null){
					detalles.addAll(detallesPorTipo);
				}
				detallesPorTipo = this.generaListaDetalleDeParametro(parametro, ParametroAutorizacionLiquidacionDetalle.TipoParametroAutLiquidacion.PROVEEDOR, proveedoresConcat, usuario);
				if(detallesPorTipo!=null){
					detalles.addAll(detallesPorTipo);
				}
				detallesPorTipo = this.generaListaDetalleDeParametro(parametro, ParametroAutorizacionLiquidacionDetalle.TipoParametroAutLiquidacion.USUARIO_SOLICITADOR, usuariosSolConcat, usuario);
				if(detallesPorTipo!=null){
					detalles.addAll(detallesPorTipo);
				}
				detallesPorTipo = this.generaListaDetalleDeParametro(parametro, ParametroAutorizacionLiquidacionDetalle.TipoParametroAutLiquidacion.USUARIO_AUTORIZADOR, usuariosAutConcat, usuario);
				if(detallesPorTipo!=null){
					detalles.addAll(detallesPorTipo);
				}
				parametro.setDetalles(detalles);
				this.entidadService.save(parametro);
			}
		}
		return mensajeError;
	}
	
	private String validaGuardadoParametroAutorizacion(String oficinasConcat,Integer tipoServicio, 
			String estadosConcat, String proveedoresConcat,
			String usuariosSolConcat, String usuariosAutConcat, Date vigenciaInicial, BigDecimal montoInicial, Long parametroId){
		String mensajeError = null;
		
		mensajeError = this.validaOficinasConTipoDeServicio(oficinasConcat, tipoServicio, parametroId);
		if(mensajeError == null){
			mensajeError = this.validaParametrosDeConfiguracionDeAutorizacion(oficinasConcat, estadosConcat, 
				proveedoresConcat, usuariosSolConcat, usuariosAutConcat, vigenciaInicial, montoInicial, parametroId);
		}else{
			mensajeError += this.validaParametrosDeConfiguracionDeAutorizacion(oficinasConcat, estadosConcat, 
					proveedoresConcat, usuariosSolConcat, usuariosAutConcat, vigenciaInicial, montoInicial, parametroId);
		}
		
		return mensajeError;
	}
	
	private String validaOficinasConTipoDeServicio(String oficinasConcat,Integer tipoServicio, Long parametroId){
		String respuesta = null;
		if(parametroId != null){
			return null;
		}
		
		if(tipoServicio!=null && oficinasConcat != null && !oficinasConcat.equals("")){
			String[] valuesArray = oficinasConcat.trim().split(",");
			for(String value : valuesArray){
				Oficina oficina = this.entidadService.findById(Oficina.class, Long.valueOf(value.trim()));
				if(Integer.valueOf(oficina.getTipoServicio()).intValue() != tipoServicio.intValue()){
					respuesta = "<li>El tipo de Servicio y la oficina no concuerdan.</li>";
					break;
				}
			}
		}
		return respuesta;
	}
	
	private String validaParametrosDeConfiguracionDeAutorizacion(String oficinasConcat,
			String estadosConcat, String proveedoresConcat,
			String usuariosSolConcat, String usuariosAutConcat, Date vigenciaInicial, BigDecimal montoInicial, Long parametroId){
		
		if(parametroId != null){
			return null;
		}
		
		String mensajeError = "";
		
		if(StringUtil.isEmpty(usuariosAutConcat)){
			mensajeError += "<li>Debe seleccionar al menos un usuario autorizador.</li>";
		}
		if(StringUtil.isEmpty(oficinasConcat) 
				&& StringUtil.isEmpty(estadosConcat) 
				&& StringUtil.isEmpty(proveedoresConcat) 
				&& StringUtil.isEmpty(usuariosSolConcat)
				&& vigenciaInicial == null
				&& montoInicial == null){
				mensajeError += "<li>Debe seleccionar al menos un par\u00E1metro de configuraci\u00F3n diferente al usuario autorizador.</li>";
		}
		if(StringUtil.isEmpty(mensajeError)){
			mensajeError = null;
		}
		
		return mensajeError;
	}
	

	
	private List<ParametroAutorizacionLiquidacionDetalle> generaListaDetalleDeParametro(ParametroAutorizacionLiquidacion parametro, ParametroAutorizacionLiquidacionDetalle.TipoParametroAutLiquidacion tipo, String valuesConcat, String codigoUsuario){
		List<ParametroAutorizacionLiquidacionDetalle> detalles = null;
		if(valuesConcat!=null && !valuesConcat.equals("")){
			detalles = new ArrayList<ParametroAutorizacionLiquidacionDetalle>();
			String[] valuesArray = valuesConcat.trim().split(",");
			for(String value : valuesArray){
				ParametroAutorizacionLiquidacionDetalle detalle = new ParametroAutorizacionLiquidacionDetalle();
				detalle.setIdRelacionado(value.trim());
				detalle.setTipoRelacion(String.valueOf(tipo.getValue()));
				detalle.setParametro(parametro);
				detalle.setFechaCreacion(new Date());
				detalle.setFechaModificacion(new Date());
				detalle.setCodigoUsuarioCreacion(codigoUsuario);
				detalle.setCodigoUsuarioModificacion(codigoUsuario);
				detalles.add(detalle);
			}
		}
		return detalles;
	}


	@Override
	public void cambiarEstatusParametroAut(Long idParametro, Integer estatus) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public LiquidacionSiniestro procesarFacturaSeleccionada(Long idFactura, Long idLiquidacion, Short tipoOperacion) throws Exception
	{		
		
		DocumentoFiscal factura = entidadService.findById(DocumentoFiscal.class, idFactura);
		LiquidacionSiniestro liquidacion = null;
	
			if(tipoOperacion == LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO)
			{
				liquidacion = this.asociarFactura(factura, idLiquidacion);
				
			}else if(tipoOperacion == LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO)
			{						
				liquidacion = this.desasociarFactura(factura, idLiquidacion);				
			}	
		   
		return liquidacion;
	}
	/**
	 * Get process for selected bills to associated liquidation.
	 * @param idFactura
	 * @param idLiquidacion
	 * @param tipoOperacion
	 * @return
	 */
	
	@Override
	public LiquidacionSiniestro procesarFacturaSeleccionadaSP(String listaFacturas, Long idLiquidacion, Short tipoOperacion) throws Exception{
		LiquidacionSiniestro liquidacion = null;
		
		liquidacion = liquidacionSiniestroDao.asociaDesasociaFacturas(listaFacturas,idLiquidacion, tipoOperacion);
		return liquidacion;
	}
	
	@Deprecated
	@Override
	public LiquidacionSiniestro procesarFacturasSeleccionadas(String idFacturas, Long idLiquidacion, Short tipoOperacion) throws Exception
	{		
		
		LiquidacionSiniestro liquidacion = null;
	
			if(tipoOperacion == LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO)
			{
				liquidacion = this.asociaFacturas(idFacturas, idLiquidacion);
				
			}else if(tipoOperacion == LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO)
			{						
				liquidacion = this.desasociarFacturas(idFacturas, idLiquidacion);				
			}	
		   
		return liquidacion;
	}   
	
	@Override
	public LiquidacionSiniestro guardarLiquidacion(LiquidacionSiniestro liquidacionSiniestro,
			LiquidacionSiniestro.EstatusLiquidacionSiniestro estatus) throws IllegalArgumentException
	{
		switch(estatus)
		{		
			case EN_TRAMITE:
				if(liquidacionSiniestro.getId() == null)
				{						
					liquidacionSiniestro.setNumeroLiquidacion(liquidacionSiniestroDao.getNumeroLiquidacion());
					liquidacionSiniestro.setSubtotal(BigDecimal.ZERO);
					liquidacionSiniestro.setDeducible(BigDecimal.ZERO);
					liquidacionSiniestro.setDescuento(BigDecimal.ZERO);
					liquidacionSiniestro.setIva(BigDecimal.ZERO);
					liquidacionSiniestro.setIvaRet(BigDecimal.ZERO);
					liquidacionSiniestro.setIsr(BigDecimal.ZERO);
					liquidacionSiniestro.setNetoPorPagar(BigDecimal.ZERO);
					liquidacionSiniestro.setImporteSalvamento(BigDecimal.ZERO);
					liquidacionSiniestro.setPrimaPendientePago(BigDecimal.ZERO);
					liquidacionSiniestro.setPrimaNoDevengada(BigDecimal.ZERO);
					liquidacionSiniestro.setEstatus(estatus.getValue());
					liquidacionSiniestro.setFechaTramite(new Date());						
					liquidacionSiniestro.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());					
				}
				
				break;
				
			case POR_AUTORIZAR:		 		
				liquidacionSiniestro.setEstatus(estatus.getValue());
				liquidacionSiniestro.setFechaPorAut(new Date());
				liquidacionSiniestro.setSolicitadoPor(usuarioService.getUsuarioActual().getNombreUsuario());
				break;			
			case EN_TRAMITE_POR_RECHAZO:
				liquidacionSiniestro.setEstatus(estatus.getValue());
				liquidacionSiniestro.setFechaTramiteRec(new Date());
				break;
			case LIBERADA_CON_SOLICITUD:
				liquidacionSiniestro.setEstatus(estatus.getValue());
				liquidacionSiniestro.setFechaLiberadaCon(new Date());
				liquidacionSiniestro.setAutorizadoPor(usuarioService.getUsuarioActual().getNombreUsuario());				
				break;
			case LIBERADA_SIN_SOLICITUD:
				liquidacionSiniestro.setEstatus(estatus.getValue());
				liquidacionSiniestro.setFechaLiberadaSin(new Date());
				break;
			case APLICADA:
				liquidacionSiniestro.setEstatus(estatus.getValue());
				liquidacionSiniestro.setFechaAplicada(new Date());
				break;	
			case ELIMINADA:
				liquidacionSiniestro.setEstatus(estatus.getValue());
				liquidacionSiniestro.setFechaEliminada(new Date());
				break;	
			case CANCELADA:
				liquidacionSiniestro.setEstatus(estatus.getValue());
				liquidacionSiniestro.setFechaCancelada(new Date());
				break;				
				
			default: 
				throw new IllegalArgumentException("Estatus de liquidacion no válido.");					
		}		
		
		liquidacionSiniestro = this.guardarLiquidacion(liquidacionSiniestro);
		
		this.bitacoraService.registrar(TIPO_BITACORA.LIQUIDACION, EVENTO.MODIFICACION_LIQUIDACION, liquidacionSiniestro.getId().toString(), 
				 String.format("Registro/Cambio estatus de liquidacion a [ %1$s ]", estatus.toString()), 
				 liquidacionSiniestro.toString(), usuarioService.getUsuarioActual().getNombreUsuario());
	
		return liquidacionSiniestro;
	}
	
	@Override
	public void recalcularMontosLiquidacion(LiquidacionSiniestro liquidacion) throws IllegalArgumentException
	{
		BigDecimal subtotal = BigDecimal.ZERO;
		BigDecimal deducible = BigDecimal.ZERO;
		BigDecimal descuento = BigDecimal.ZERO;
		BigDecimal iva = BigDecimal.ZERO;
		BigDecimal ivaRetenido = BigDecimal.ZERO;
		BigDecimal isr = BigDecimal.ZERO;
		BigDecimal total = BigDecimal.ZERO;
		BigDecimal salvamento = BigDecimal.ZERO;
		
		for(DocumentoFiscal factura : liquidacion.getFacturas())
		{	
			for(OrdenCompra ordenCompra:factura.getOrdenesCompra())
			{
				//original_call
				//DesglosePagoConceptoDTO desgloseOrdenPago = pagosSiniestroService.obtenerTotales(ordenCompra.getOrdenPago().getId(),Boolean.FALSE);
				DesglosePagoConceptoDTO desgloseOrdenPago = pagosSiniestroService.obtenerTotalesSP(ordenCompra.getOrdenPago().getId(),Boolean.FALSE);
				
				subtotal = subtotal.add(desgloseOrdenPago.getSubTotalPorPagar() == null ? 
						BigDecimal.ZERO : desgloseOrdenPago.getSubTotalPorPagar() );
				
				iva = iva.add(desgloseOrdenPago.getIvaTotalPorPagar() == null ? 
						BigDecimal.ZERO : desgloseOrdenPago.getIvaTotalPorPagar());
				
				ivaRetenido = ivaRetenido.add(desgloseOrdenPago.getIvaRetenidoTotalPorPagar() == null ? 
						BigDecimal.ZERO : desgloseOrdenPago.getIvaRetenidoTotalPorPagar());
				
				isr = isr.add(desgloseOrdenPago.getIsrTotalPorPagar() == null ? 
						BigDecimal.ZERO : desgloseOrdenPago.getIsrTotalPorPagar());
				
				total = total.add(desgloseOrdenPago.getTotalesPorPagar() == null ? BigDecimal.ZERO : desgloseOrdenPago.getTotalesPorPagar());
			}			
		}
		
		for(RecuperacionProveedor recuperacion: liquidacion.getRecuperaciones())
		{
			subtotal = subtotal.subtract(recuperacion.getSubTotal() == null ? 
					BigDecimal.ZERO : recuperacion.getSubTotal());			
			
			iva = iva.subtract(recuperacion.getIva() == null ? 
					BigDecimal.ZERO : recuperacion.getIva());
			
			ivaRetenido = ivaRetenido.subtract(recuperacion.getIvaRetenido() == null ? 
					BigDecimal.ZERO : recuperacion.getIvaRetenido());
			
			isr = isr.subtract(recuperacion.getIsr() == null ? 
					BigDecimal.ZERO : recuperacion.getIsr());
			
			total = total.subtract(recuperacion.getMontoTotal() == null ? BigDecimal.ZERO : recuperacion.getMontoTotal());	
		}
		
		for(OrdenPagoSiniestro ordenPago : liquidacion.getOrdenesPago())
		{
			//original_call joksrc
			//DesglosePagoConceptoDTO desgloseOrdenPago = pagosSiniestroService.obtenerTotales(ordenPago.getId(),Boolean.FALSE);
			
			DesglosePagoConceptoDTO desgloseOrdenPago = pagosSiniestroService.obtenerTotalesSP(ordenPago.getId(),Boolean.FALSE);
			
			subtotal = subtotal.add(desgloseOrdenPago.getSubTotalPorPagar() == null ? 
					BigDecimal.ZERO : desgloseOrdenPago.getSubTotalPorPagar() );

			deducible = deducible.add(desgloseOrdenPago.getDeduciblesTotal() == null ? 
					BigDecimal.ZERO : desgloseOrdenPago.getDeduciblesTotal() );
			
			descuento = descuento.add(desgloseOrdenPago.getDescuentosTotal() == null ? 
					BigDecimal.ZERO : desgloseOrdenPago.getDescuentosTotal() );
			
			iva = iva.add(desgloseOrdenPago.getIvaTotalPorPagar() == null ? 
					BigDecimal.ZERO : desgloseOrdenPago.getIvaTotalPorPagar());
			
			ivaRetenido = ivaRetenido.add(desgloseOrdenPago.getIvaRetenidoTotalPorPagar() == null ? 
					BigDecimal.ZERO : desgloseOrdenPago.getIvaRetenidoTotalPorPagar());
			
			isr = isr.add(desgloseOrdenPago.getIsrTotalPorPagar() == null ? 
					BigDecimal.ZERO : desgloseOrdenPago.getIsrTotalPorPagar());
			
			total = total.add(desgloseOrdenPago.getTotalesPorPagar() == null ? BigDecimal.ZERO : desgloseOrdenPago.getTotalesPorPagar());	
			
			salvamento = isr.add(desgloseOrdenPago.getSalvamentoTotalPorPagar() == null ? 
					BigDecimal.ZERO : desgloseOrdenPago.getSalvamentoTotalPorPagar());
		}	
		
		liquidacion.setSubtotal(subtotal);
		liquidacion.setDeducible(deducible);
		liquidacion.setDescuento(descuento);
		liquidacion.setIva(iva);
		liquidacion.setIvaRet(ivaRetenido);
		liquidacion.setIsr(isr);
		liquidacion.setTotal(total);
		if(liquidacion.getOrigenLiquidacion().equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue()))
		{
			liquidacion.setNetoPorPagar(total);
		}
		liquidacion.setImporteSalvamento(salvamento);
	}
	
	@Override
	public void recalcularMontosLiquidacionSP(Long liquidacionId) {
		this.liquidacionSiniestroDao.recalculaLiquidacion(liquidacionId);
	}

	public Boolean validarUsuarioEsAutorizador(Long idLiquidacion) {
		
		return liquidacionSiniestroDao.validarUsuarioEsAutorizador(idLiquidacion, Long.parseLong(usuarioService.getUsuarioActual().getId().toString()), new Date());
	}
	
	
	@Override
	public List<OrdenPagoSiniestro> buscarOrdenesPagoIndemnizacion(Long idLiquidacion)
	{
		List<OrdenPagoSiniestro> listaOrdenesPago = liquidacionSiniestroDao.buscarOrdenesPagoIndemnizacion(idLiquidacion);		
		
		for(OrdenPagoSiniestro registro:listaOrdenesPago)
		{				
			DesglosePagoConceptoDTO desglose;
			desglose = pagosSiniestroService.obtenerTotalesSP(registro.getId(), Boolean.FALSE);
			
			registro.setTotales(desglose);
		}
		
		return listaOrdenesPago;
	}
	
	@Override
	public LiquidacionSiniestro procesarOrdenPagoSeleccionada(Long idOrdenPago, Long idLiquidacion, Short tipoOperacion) throws Exception
	{
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		
		OrdenPagoSiniestro ordenPago = entidadService.findById(OrdenPagoSiniestro.class, idOrdenPago);
	
		if(tipoOperacion == LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO)
		{
			liquidacion = this.asociarOrdenPago(ordenPago, liquidacion);
			
		}else if(tipoOperacion == LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO)
		{						
			liquidacion = this.desasociarOrdenPago(ordenPago, liquidacion);		
		}
		
		//Actualizar Informacion Bancaria Beneficiario		
		copiarInformacionBancariaBeneficiario(idLiquidacion, idOrdenPago);			
	
		return liquidacion;		
	}
	
	@Override
	public LiquidacionSiniestro asociarOrdenPago(OrdenPagoSiniestro ordenPago, LiquidacionSiniestro liquidacion) throws Exception
	{		
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();			
			
		ordenPago.setEstatus(PagosSiniestroService.ESTATUS_ASOCIADA);		
			
		entidadService.save(ordenPago);		
		
		liquidacion.getOrdenesPago().add(ordenPago);
		
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("", " Ha ocurrido un error al asociar una factura a la liquidacion");
		}		
		
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;		  
	}
	  
	@Override
	public LiquidacionSiniestro desasociarOrdenPago(OrdenPagoSiniestro ordenPago, LiquidacionSiniestro liquidacion) throws Exception
	{
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();		
		
		ordenPago.setEstatus(PagosSiniestroService.ESTATUS_LIBERADA);		
			
		entidadService.save(ordenPago);	
		
		liquidacion.getOrdenesPago().remove(ordenPago);
		
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("_", "Ha ocurrido un error al desasociar una factura a la liquidacion");
		}
				
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;
	}
	
	public Boolean tieneIndemnizacionesPT(OrdenPagoSiniestro ordenPago)
	{
		Boolean resultado = Boolean.FALSE;		
		if(null==ordenPago){
			return resultado;
		}
		  
		OrdenCompra ordenCompra = ordenPago.getOrdenCompra();
		IndemnizacionSiniestro indemnizacion = ordenCompra.getIndemnizacion();
		
		if(indemnizacion != null && 
				indemnizacion.getEstatusIndemnizacion().compareToIgnoreCase(EstatusAutorizacionIndemnizacion.AUTORIZADA.codigo ) == 0
				&& indemnizacion.getEsPagoDanios() == Boolean.FALSE && 
				(EnumUtil.equalsValue(ordenCompra.getCoberturaReporteCabina().getClaveTipoCalculo(), 
						ClaveTipoCalculo.ROBO_TOTAL, ClaveTipoCalculo.DANIOS_MATERIALES)))
		{
			resultado = Boolean.TRUE;
		}		
		
		return resultado;
	}

	@Override	
	public BigDecimal obtenerPrimaPendientePago(Long idLiquidacion)
	{
		BigDecimal primaPendiente = BigDecimal.ZERO;
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		
		for(OrdenPagoSiniestro ordenPago : liquidacion.getOrdenesPago())
		{
			primaPendiente = this.obtenerPrimaPendientePagoParaPT(ordenPago);
			if(primaPendiente.compareTo(BigDecimal.ZERO) != 0)
			{
				break;
			}
		}		
		
		return primaPendiente;		
	}
	
	@Override
	public BigDecimal obtenerPrimaNoDevengada(Long idLiquidacion)
	{
		BigDecimal primaNoDevengada = BigDecimal.ZERO;
		
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		
		for(OrdenPagoSiniestro ordenPago : liquidacion.getOrdenesPago())
		{
			primaNoDevengada = this.obtenerPrimaNoDevengadaParaPT(ordenPago);
			if(primaNoDevengada.compareTo(BigDecimal.ZERO) != 0)
			{
				break;
			}
		}		
		
		return primaNoDevengada;		
	}
		
	private BigDecimal obtenerPrimaPendientePagoParaPT(OrdenPagoSiniestro ordenPago)
	{
		BigDecimal primaPendiente = BigDecimal.ZERO;
		
		ordenPago = entidadService.findById(OrdenPagoSiniestro.class, ordenPago.getId());
		
		if(tieneIndemnizacionesPT(ordenPago))
		{
			BigDecimal idToPoliza = ordenPago.getOrdenCompra().getReporteCabina().getPoliza().getIdToPoliza();			
			Integer numeroInciso = ordenPago.getOrdenCompra().getReporteCabina()
			.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso();
			
			primaPendiente = endosoService.validarPrimasPendientesPagoInciso(idToPoliza, numeroInciso, null);			
		}					
		
		return primaPendiente;		
	}
		
	private BigDecimal obtenerPrimaNoDevengadaParaPT(OrdenPagoSiniestro ordenPago)
	{ 
		BigDecimal primaNoDevengada = BigDecimal.ZERO;
		
		ordenPago = entidadService.findById(OrdenPagoSiniestro.class, ordenPago.getId());
		
		if(tieneIndemnizacionesPT(ordenPago))
		{
			BigDecimal idToPoliza = ordenPago.getOrdenCompra().getReporteCabina()
			.getPoliza().getIdToPoliza();

			Integer numeroInciso = ordenPago.getOrdenCompra().getReporteCabina()
						.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso();
			
			List<EndosoIDTO> validaciones = endosoService.validaEndosoPT(idToPoliza, numeroInciso, 
					ordenPago.getOrdenCompra().getReporteCabina().getFechaOcurrido(), // fechaSiniestro
					ordenPago.getOrdenCompra().getIndemnizacion().getEsPagoDanios() ?
							SolicitudDTO.CVE_MOTIVO_ENDOSO_PAGO_DANIOS_SUSTITUCION_PERDIDA_TOTAL 
							:SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL);
			
			Predicate predicate = new Predicate() {		
				@Override
				public boolean evaluate(Object arg0) {
					EndosoIDTO validacion = (EndosoIDTO)arg0;
					if(validacion.getTipoMovimiento().equals(TipoMovimientoEndoso.INCISO.getValue()))
					{
						return true;
					}					
					return false;
				}
			};					
			
			EndosoIDTO validacionNivelInciso = (EndosoIDTO)CollectionUtils.find(validaciones,predicate);
			
			if(validacionNivelInciso == null)
			{
				throw new NegocioEJBExeption("", " Ha ocurrido un error al consultar la prima no devengada."); 
			}
			
			//Calculo de la prima total del endoso de Perdida Total
			BigDecimal totalPrimas = validacionNivelInciso.getPrimaNeta() != null ? 
					new BigDecimal(validacionNivelInciso.getPrimaNeta(),
							new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO;
			BigDecimal descuentos = validacionNivelInciso.getBonificacionComision() != null ?
					new BigDecimal(validacionNivelInciso.getBonificacionComision(),
							new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO;
			BigDecimal primaNeta = totalPrimas.subtract(descuentos);
			BigDecimal recargo = (validacionNivelInciso.getRecargoPF() != null ? 
					new BigDecimal(validacionNivelInciso.getRecargoPF(),new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO)
					.subtract((validacionNivelInciso.getBonificacionComisionRPF() != null ? 
							new BigDecimal(validacionNivelInciso.getBonificacionComisionRPF(),new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO));
			BigDecimal derechos = BigDecimal.ZERO;
			BigDecimal iva = validacionNivelInciso.getIva() != null ? 
					new BigDecimal(validacionNivelInciso.getIva(),
							new MathContext(16, RoundingMode.HALF_UP)):BigDecimal.ZERO;
					
			primaNoDevengada = primaNeta.add(recargo).add(derechos).add(iva); //Prima Total			
		}				
		
		return primaNoDevengada;		
	}
	
	public Short cancelarLiquidacion(Long idLiquidacion, Short tipoCancelacion) {
		return liquidacionSiniestroDao.cancelarLiquidacion(idLiquidacion, tipoCancelacion);

	}
	private LiquidacionSiniestro copiarInformacionBancariaBeneficiario(Long idLiquidacion, Long idOrdenPago)
	{
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion); 

		

		List<Long> listaIdsOrdenes = new ArrayList<Long>();
		for(OrdenPagoSiniestro ordenPago : liquidacion.getOrdenesPago())
		{
			listaIdsOrdenes.add(ordenPago.getId());
		}
		
		List<RecepcionDocumentosSiniestro> listaRecepcionDocumentosSiniestro = liquidacionSiniestroDao.obtenerInformacionBancariaIndemnizacionPorOrdenesPago(listaIdsOrdenes);		
		
		if(!listaRecepcionDocumentosSiniestro.isEmpty())
		{	
			RecepcionDocumentosSiniestro informacionBancaria = listaRecepcionDocumentosSiniestro.get(0);//Se asume que unicamente puede existir un registro.
			
			liquidacion.setBancoBeneficiario(String.valueOf(informacionBancaria.getBancoId()));
			liquidacion.setClabeBeneficiario(informacionBancaria.getClabeBanco());
			liquidacion.setRfcBeneficiario(informacionBancaria.getRfc());
			liquidacion.setCorreo(informacionBancaria.getCorreo());
			liquidacion.setTelefonoNumero(informacionBancaria.getTelefonoNumero());
			liquidacion.setTelefonoLada(informacionBancaria.getTelefonoLada());
			liquidacion.setTipoLiquidacion(LiquidacionSiniestro.TipoLiquidacion.TRANSFERENCIA_BANCARIA.getValue());
		}else
		{			
			liquidacion.setBancoBeneficiario(null);
			liquidacion.setClabeBeneficiario(null);
			liquidacion.setRfcBeneficiario(null);
			liquidacion.setCorreo(null);
			liquidacion.setTelefonoNumero(null);
			liquidacion.setTelefonoLada(null);
			liquidacion.setTipoLiquidacion(LiquidacionSiniestro.TipoLiquidacion.CHEQUE.getValue());
		}
		
		liquidacion = this.guardarLiquidacion(liquidacion);			
				
		return liquidacion;		
	}	
	
	private void ejecutarEndosoPTLiquidacion(Long idLiquidacion) throws Exception
	{
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion); 
		BigDecimal idToPoliza;
		BigDecimal idToCotizacion;
		Integer numeroInciso;
		IncisoContinuity incisoContinuity = new IncisoContinuity();
		IndemnizacionSiniestro indemnizacion;
		SiniestroCabina siniestro;			
		
		for(OrdenPagoSiniestro ordenPago:liquidacion.getOrdenesPago())
		{
			//Si es liquidacion de una PT y no existe prima pendiente de pago o existe valor de prima pendiente de pago seleccionado en la liquidacion.
			if((tieneIndemnizacionesPT(ordenPago) && liquidacion.getPrimaPendientePago() != null 
					&& liquidacion.getPrimaPendientePago().compareTo(BigDecimal.ZERO) > 0 ) || 
					(tieneIndemnizacionesPT(ordenPago) && this.obtenerPrimaPendientePagoParaPT(ordenPago).compareTo(BigDecimal.ZERO) == 0))
			{
				idToPoliza = ordenPago.getOrdenCompra().getReporteCabina()
				.getPoliza().getIdToPoliza();

				idToCotizacion = ordenPago.getOrdenCompra().getReporteCabina()
								.getPoliza().getCotizacionDTO().getIdToCotizacion();
				
				siniestro = ordenPago.getOrdenCompra().getReporteCabina().getSiniestroCabina();
				
				numeroInciso = ordenPago.getOrdenCompra().getReporteCabina()
							.getSeccionReporteCabina().getIncisoReporteCabina().getNumeroInciso();
				
				CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(CotizacionContinuity.class, 
					CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion);			

				indemnizacion = ordenPago.getOrdenCompra().getIndemnizacion();
				
				for(IncisoContinuity incisoC : cotizacionContinuity.getIncisoContinuities().getValue())
				{
					if(incisoC.getNumero() == numeroInciso)
					{
						incisoContinuity = incisoC;	
						break;
					}			
				}						
							
				emisionPendienteService.generarEndosoPerdidaTotalAutomatico(idToPoliza, indemnizacion.getEsPagoDanios() ?
						OrdenCompraService.TIPO_INDEMNIZACION_PAGO_DANIOS:
							OrdenCompraService.TIPO_INDEMNIZACION_PERDIDATOTAL, 
							siniestro.getReporteCabina().getFechaHoraOcurrido(), incisoContinuity, siniestro.getId());				
			
				break; //unicamente puede existir una indemnizacion.
			
			}//TODO revisar a quien se debe enviar notificacion.
		}		
							
	}
	
	@Override
	public List<RecuperacionProveedor> obtenerRecuperacionesAsociadas(Long idLiquidacion){
		
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		
		return liquidacion.getRecuperaciones();
	} 

	@Override
	public List<RecuperacionProveedor> obtenerRecuperacionesPendientesPorAsignar(Integer idProveedor){
				
		return liquidacionSiniestroDao.obtenerRecuperacionesPendientesPorAsignar(idProveedor);
	}
	
	@Override
	public LiquidacionSiniestro procesarRecuperacionSeleccionada(Long idRecuperacion, Long idLiquidacion, Short tipoOperacion) throws Exception
	{
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		
		RecuperacionProveedor recuperacion = entidadService.findById(RecuperacionProveedor.class, idRecuperacion);
	
		if(tipoOperacion == LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO)
		{
			liquidacion = this.asociarRecuperacion(recuperacion, liquidacion);
			
		}else if(tipoOperacion == LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO)
		{						
			liquidacion = this.desasociarRecuperacion(recuperacion, liquidacion);	
		}		
	
		return liquidacion;		
	}
	
	@Override
	public LiquidacionSiniestro procesarRecuperacionesSeleccionadas(String idRecuperacionesStr, Long idLiquidacion, Short tipoOperacion) throws Exception
	{
		LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
		
		if(tipoOperacion == LiquidacionSiniestroService.ASOCIAR_ORDEN_PAGO)
		{
			liquidacion = this.asociarRecuperaciones(idRecuperacionesStr, liquidacion);
			
		}else if(tipoOperacion == LiquidacionSiniestroService.DESASOCIAR_ORDEN_PAGO)
		{						
			liquidacion = this.desasociarRecuperaciones(idRecuperacionesStr, liquidacion);	
		}		
	
		return liquidacion;		
	}
	
	

	
	
	private LiquidacionSiniestro asociarRecuperaciones(String idRecuperacionesStr, LiquidacionSiniestro liquidacion) throws Exception
	{
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();	
		String[] idsRecuperacionesArray = idRecuperacionesStr.trim().split(",");
		
		for(String idRecuperacionStr: idsRecuperacionesArray){
			RecuperacionProveedor recuperacion = entidadService.findById(RecuperacionProveedor.class, Long.valueOf(idRecuperacionStr));
			liquidacion.getRecuperaciones().add(recuperacion);
			
			try {
				this.recalcularMontosLiquidacion(liquidacion);
				
			} catch (Exception e) {			
				e.printStackTrace();
				throw new NegocioEJBExeption("", " Ha ocurrido un error al asociar una recuperacion a la liquidacion");
			}		
			liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
			liquidacion.setFechaModificacion(new Date());
			liquidacion = entidadService.save(liquidacion);
		}
		return liquidacion;		
	}
	
	public LiquidacionSiniestro asociarRecuperacion(RecuperacionProveedor recuperacion, LiquidacionSiniestro liquidacion) throws Exception
	{
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();			
		
		liquidacion.getRecuperaciones().add(recuperacion);
		
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("", " Ha ocurrido un error al asociar una recuperacion a la liquidacion");
		}		
		
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;		
	}
	
	public LiquidacionSiniestro desasociarRecuperacion(RecuperacionProveedor recuperacion, LiquidacionSiniestro liquidacion) throws Exception
	{
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();			
	
		liquidacion.getRecuperaciones().remove(recuperacion);
		
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("", "  Ha ocurrido un error al desasociar una factura a la liquidacion");
		}
		
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;
	}
	
	
	public LiquidacionSiniestro desasociarRecuperaciones(String idRecuperacionesStr, LiquidacionSiniestro liquidacion) throws Exception
	{
		String codigoUsuario = 	usuarioService.getUsuarioActual().getNombreUsuario();	
		
		String[] idsRecuperacionesArray = idRecuperacionesStr.trim().split(",");
		
		for(String idRecuperacionStr: idsRecuperacionesArray){
			RecuperacionProveedor recuperacion = entidadService.findById(RecuperacionProveedor.class, Long.valueOf(idRecuperacionStr));
			liquidacion.getRecuperaciones().remove(recuperacion);
		}
		try {
			this.recalcularMontosLiquidacion(liquidacion);
			
		} catch (Exception e) {			
			e.printStackTrace();
			throw new NegocioEJBExeption("", "  Ha ocurrido un error al desasociar una factura a la liquidacion");
		}
		liquidacion.setCodigoUsuarioModificacion(codigoUsuario);
		liquidacion.setFechaModificacion(new Date());
		liquidacion = entidadService.save(liquidacion);
		
		return liquidacion;
	}
	
	private void validarMontoNCRsRecuperacion(LiquidacionSiniestro liquidacion) throws NegocioEJBExeption
	{  
		if(liquidacion.getOrigenLiquidacion()
				.equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.PROVEEDOR.getValue()))
		{
			BigDecimal totalOC = BigDecimal.ZERO;
			BigDecimal totalRecuperacion = BigDecimal.ZERO;
			
			for(DocumentoFiscal factura : liquidacion.getFacturas())
			{
				for(OrdenCompra orden : factura.getOrdenesCompra())
				{	//original
					//DesglosePagoConceptoDTO desgloseOrdenPago = pagosSiniestroService.obtenerTotales(orden.getOrdenPago().getId(),Boolean.FALSE);				
					//copia
					DesglosePagoConceptoDTO desgloseOrdenPago = pagosSiniestroService.obtenerTotalesSP(orden.getOrdenPago().getId(),Boolean.FALSE);
					
					totalOC = totalOC.add(desgloseOrdenPago.getTotalesPorPagar() == null ? BigDecimal.ZERO : desgloseOrdenPago.getTotalesPorPagar());							
				}							
			}
			
			for(RecuperacionProveedor recuperacion: liquidacion.getRecuperaciones())
			{			
				totalRecuperacion = totalRecuperacion.add(recuperacion.getMontoTotal() == null ? BigDecimal.ZERO : recuperacion.getMontoTotal());	
			}
			
			if(totalOC.compareTo(totalRecuperacion) < 0)
			{
				throw new NegocioEJBExeption("", "La suma del monto total de las Notas de Cr\u00E9dito de Recuperacion " +
						"No puede ser mayor a la suma del monto total de las ordenes de pago correspondientes a las facturas relacionadas a la " +
						"liquidaci\u00F3n.");
			}			
		}			
	}
	
	private void validarSeleccionPrimasPdtsPago(LiquidacionSiniestro liquidacion)throws NegocioEJBExeption
	{
		if(liquidacion.getOrigenLiquidacion()
				.equalsIgnoreCase(LiquidacionSiniestro.OrigenLiquidacion.BENEFICIARIO.getValue()))
		{
			BigDecimal totalPrimaPdtPago = BigDecimal.ZERO;			
			
			totalPrimaPdtPago = this.obtenerPrimaPendientePago(liquidacion.getId());
			
			//Existen primas pendientes de pago y el usuario no selecciona el check correspondiente
			//No es posible debido a que este escenario dejaria el endoso de PT sin emitir, abriendo la posibilidad de 
			//omitir el cobro de las primas pendientes de pago.
			//Si el usuario no desea que las primas pendientes se descuenten del monto de la liquidacion, entonces deberá solicitarle el pago
			//de las mismas antes de continuar con la liquidacion.
			if(totalPrimaPdtPago.compareTo(BigDecimal.ZERO) != 0 
					&& (liquidacion.getPrimaPendientePago() == null 
							|| liquidacion.getPrimaPendientePago().compareTo(BigDecimal.ZERO) == 0))
			{
				throw new NegocioEJBExeption("", "No es posible solicitar la autorizaci\u00F3n de la liquidaci\u00F3n " +
						"debido a que existen Primas Pendientes de Pago sin seleccionar, debe marcar la opci\u00F3n correspondiente en pantalla " +
						"o realizar el cobro del adeudo pendiente e ingresar nuevamente a esta liquidaci\u00F3n.");				
			}		
		}		
	}
	
	private void reactivarRecuperacionesSalvamentos(LiquidacionSiniestro liquidacionSiniestro)
	{  
		// Validar que la liquidacion cuenta con valor en el campo fecha de En Tramite Mizar, 
		// lo que indicaría que proviene del flujo de "reexpedicion" 
		if(liquidacionSiniestro.getFechaTramiteMizar() != null)
		{
			for(OrdenPagoSiniestro ordenPago : liquidacionSiniestro.getOrdenesPago())
			{
				RecuperacionSalvamento recuperacion = recuperacionSalvamentoService.obtenerRecuperacionSalvamento(ordenPago.getOrdenCompra().getIdTercero());
				
				if(recuperacion != null && recuperacion.getEstatus().equalsIgnoreCase(Recuperacion.EstatusRecuperacion.INACTIVO.toString()))
				{
					recuperacionSalvamentoService.reactivarRecuperacionSalvamento(recuperacion.getId(), "Reexpedicion de liquidacion"); //TODO definir constantes				
				}			
			}
		}				
	}
	
	@Override
	public Boolean tieneIndemnizaciones(Long idLiquidacion) {
		Boolean tieneIndemnizaciones = Boolean.FALSE;
		
		if(idLiquidacion != null){
			LiquidacionSiniestro liquidacion = entidadService.findById(LiquidacionSiniestro.class, idLiquidacion);
			for(OrdenPagoSiniestro orden : liquidacion.getOrdenesPago()){
				IndemnizacionSiniestro indemnizacion = orden.getOrdenCompra().getIndemnizacion();
				if(indemnizacion != null){
						tieneIndemnizaciones = Boolean.TRUE;
				}
			}
		}
		
		return tieneIndemnizaciones;
	}
	
	@Override
	public Boolean isIndemnizacionesPT(OrdenPagoSiniestro ordenPago)
	{
		Boolean resultado = Boolean.FALSE;		
		if(null==ordenPago){
			return resultado;
		}
		  
		OrdenCompra ordenCompra = ordenPago.getOrdenCompra();
		IndemnizacionSiniestro indemnizacion = ordenCompra.getIndemnizacion();
		
		if(indemnizacion != null && 
				indemnizacion.getEstatusIndemnizacion().compareToIgnoreCase(EstatusAutorizacionIndemnizacion.AUTORIZADA.codigo ) == 0
				&& indemnizacion.getEsPagoDanios() == Boolean.FALSE )
		{
			resultado = Boolean.TRUE;
		}		
		
		return resultado;
	}
	
	@Override
	public List<DocumentoFiscal> ordenaFacturasLiquidacion(List<DocumentoFiscal> listaFacturasLiquidacion){
		
		List<FacturaLiquidacionDTO> listaOrdenesPagoSiniestros = new ArrayList<FacturaLiquidacionDTO>();
		StringBuilder ordenesFactura = new StringBuilder();
		Boolean esPrimero;
		BigDecimal totalDetalleFactura = BigDecimal.ZERO;
		
		if( !listaFacturasLiquidacion.isEmpty() ) {
			
			for( DocumentoFiscal documentoFiscal : listaFacturasLiquidacion ) {
						
				// OBTIENA LAS FACTURAS ASOCIADAS A LA FACTURA - Dentro de este método, invoca el método de obtenerTotalesSP
				listaOrdenesPagoSiniestros = pagosSiniestroService.buscarOrdenesPagoFactura(documentoFiscal.getId());
				esPrimero = true;
						
				if( !listaOrdenesPagoSiniestros.isEmpty() ) {
							
					for( FacturaLiquidacionDTO facturaDTO : listaOrdenesPagoSiniestros ) {
						// CONCATENA LA ORDEN DE COMPRA
						ordenesFactura.append( facturaDTO.getOrdenPago().getOrdenCompra().getId()).append(",");
								
						// LAS ORDENES DE COMPRA REGRESAN ORDENADAS DE LA MENOR A LA MAYOR,
						// POR LO CUAL SE HACE SET EN LA FECHA_CRECION DEL DOCUMENTO_FISCAL
						// CON LA PRIMER ORDEN DE COMPRA (LA FECHA MAS BAJA)
						if( esPrimero ) {
							documentoFiscal.setFechaCreacionOrdenCompra(facturaDTO.getOrdenPago().getOrdenCompra().getFechaCreacion());
							esPrimero = false;
						}
						
						totalDetalleFactura = totalDetalleFactura.add( ( facturaDTO.getDesgloseConceptos().getSubTotalPorPagar() == null ? BigDecimal.ZERO : facturaDTO.getDesgloseConceptos().getSubTotalPorPagar() ) );
					}
					// CONCATENA EL NÚMERO DE FACTURA O DOCUMENTO FISCAL
					documentoFiscal.setNumeroFactura(ordenesFactura.toString().substring(0,ordenesFactura.toString().length()-1)+" - "+( documentoFiscal.getNombreAgrupador() == null  ? documentoFiscal.getNumeroFactura() : documentoFiscal.getNombreAgrupador() ) );
					documentoFiscal.setMontoTotal(totalDetalleFactura);
					totalDetalleFactura = BigDecimal.ZERO;
					ordenesFactura.setLength(0);
				}
						
			}
					
			Collections.sort(listaFacturasLiquidacion, 
				new Comparator<DocumentoFiscal>() {				
					public int compare(DocumentoFiscal n1, DocumentoFiscal n2){
						return n1.getFechaCreacionOrdenCompra().compareTo(n2.getFechaCreacionOrdenCompra());
					}
			});
					
		}	
		
		return listaFacturasLiquidacion;
		
	}
	
	@Override
	public List<DesglosePagoConceptoDTO> obtenerPagosPorLiquidacion (Long idLiquidacion){
		return this.pagosSiniestroDao.obtenerPagosPorLiquidacion(idLiquidacion);
	}
	
	
}