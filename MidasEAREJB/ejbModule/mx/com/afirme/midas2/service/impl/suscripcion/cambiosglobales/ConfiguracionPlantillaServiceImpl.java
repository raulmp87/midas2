package mx.com.afirme.midas2.service.impl.suscripcion.cambiosglobales;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas2.dao.suscripcion.cambiosglobales.ConfiguracionPlantillaDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNeg;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNegCobertura;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNegCoberturaPK;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cambiosglobales.ConfiguracionPlantillaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;

@Stateless
public class ConfiguracionPlantillaServiceImpl implements
		ConfiguracionPlantillaService {
	
	private EntidadService entidadService;

	private ConfiguracionPlantillaDao plantillaDao;
	
	private CoberturaService coberturaService;
	
	private CotizacionFacadeRemote cotizacionService;
	
	private IncisoService incisoService;
	
	private CalculoService calculoService;
	
	protected SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote;
	
	
	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	@EJB
	public void setSeccionCotizacionFacadeRemote(
			SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote) {
		this.seccionCotizacionFacadeRemote = seccionCotizacionFacadeRemote;
	}

	@EJB
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}	
	
	@EJB	
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}

	@EJB
	public void setPlantillaDao(ConfiguracionPlantillaDao plantillaDao) {
		this.plantillaDao = plantillaDao;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public void agregarCobertura(Long idPlantilla, BigDecimal idCobertura, 
			Boolean contratada, Double sumaAsegurada, Double deducible) {
		ConfiguracionPlantillaNegCoberturaPK id = new ConfiguracionPlantillaNegCoberturaPK();
		id.setCoberturaId(idCobertura);
		id.setPlantillaId(idPlantilla);
		ConfiguracionPlantillaNegCobertura cobertura = entidadService.findById(ConfiguracionPlantillaNegCobertura.class, id);
		if(cobertura == null){
			cobertura = new ConfiguracionPlantillaNegCobertura();
			cobertura.setId(id);
		}
		cobertura.setContratada(contratada);
		cobertura.setDeducible(deducible);
		cobertura.setSumaAsegurada(sumaAsegurada);
		entidadService.save(cobertura);		
	}

	@Override
	public ConfiguracionPlantillaNeg guardarConfiguracion(BigDecimal idCotizacion, 
			BigDecimal idLinea, BigDecimal idPaquete, String comentario) {		
		ConfiguracionPlantillaNeg plantilla = obtenerPlantilla(idCotizacion, idLinea, idPaquete);
		if(plantilla == null){
			plantilla = new ConfiguracionPlantillaNeg();			
		}
		plantilla.setComentario(comentario);
		plantilla.setCotizacionId(idCotizacion);
		plantilla.setLineaId(idLinea);
		plantilla.setPaqueteId(idPaquete);
		entidadService.save(plantilla);
		plantilla.setConfiguracionCoberturaList(new ArrayList<ConfiguracionPlantillaNegCobertura>());
		return plantilla;
	}

	@Override
	public ConfiguracionPlantillaNegCobertura obtenerCobertura(Long idPlantilla, 
			BigDecimal idCobertura) {
		ConfiguracionPlantillaNegCoberturaPK id = new ConfiguracionPlantillaNegCoberturaPK();
		id.setCoberturaId(idCobertura);
		id.setPlantillaId(idPlantilla);
		return entidadService.findById(ConfiguracionPlantillaNegCobertura.class, 
				id);	
	}

	@Override
	public ConfiguracionPlantillaNegCobertura obtenerCobertura(BigDecimal idCotizacion, 
			BigDecimal idLinea, BigDecimal idPaquete, BigDecimal idCobertura) {
		ConfiguracionPlantillaNeg filtro = crearFiltro(idCotizacion, idLinea, idPaquete);
		if(idCobertura == null){
			throw new RuntimeException("Filtros definidos incorrectamente");
		}		
		return obtenerCobertura(filtro, idCobertura);
	}

	@Override
	public List<ConfiguracionPlantillaNegCobertura> obtenerCoberturas(Long idPlantilla) {
		return plantillaDao.listarCoberturas(idPlantilla);
	}

	@Override
	public ConfiguracionPlantillaNeg obtenerPlantilla(BigDecimal idCotizacion,
			BigDecimal idLinea, BigDecimal idPaquete) {
		return plantillaDao.findByCotizacionLineaPaquete(idCotizacion, idLinea, idPaquete);
	}

	@Override
	public ConfiguracionPlantillaNegCobertura obtenerCobertura(
			ConfiguracionPlantillaNeg filtro, BigDecimal idCobertura) {
		Long idPlantilla = null;
		List<ConfiguracionPlantillaNeg> plantillas = plantillaDao.findByFilters(filtro);
		if(!plantillas.isEmpty()){
			idPlantilla = plantillas.get(0).getPlantillaId();
		}
		return obtenerCobertura(idPlantilla, idCobertura);
	}
	
	private ConfiguracionPlantillaNeg crearFiltro(BigDecimal idCotizacion, BigDecimal idLinea, BigDecimal idPaquete){
		ConfiguracionPlantillaNeg filtro = new ConfiguracionPlantillaNeg();
		if(idCotizacion == null || idLinea == null || idPaquete == null){
			throw new RuntimeException("Filtros definidos incorrectamente");
		}
		filtro.setCotizacionId(idCotizacion);
		filtro.setLineaId(idLinea);
		filtro.setPaqueteId(idPaquete);
		return filtro;
	}

	@Override
	public ConfiguracionPlantillaNeg guardarConfiguracion(
			ConfiguracionPlantillaNeg plantilla) {
		entidadService.save(plantilla);
		plantilla.setConfiguracionCoberturaList(new ArrayList<ConfiguracionPlantillaNegCobertura>());
		return plantilla;
	}

	@Override
	public List<CoberturaCotizacionDTO> listarCoberturasDisponiblesPlantilla(
			BigDecimal idCotizacion, BigDecimal idLinea, BigDecimal idPaquete) {
		CotizacionDTO cotizacion = cotizacionService.findById(idCotizacion);
		Long idNegocio = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio();
		BigDecimal idProducto = cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto();
		BigDecimal idTipoPoliza = cotizacion.getTipoPolizaDTO().getIdToTipoPoliza();
		BigDecimal idMoneda = cotizacion.getIdMoneda();
		return coberturaService.getCoberturas(idNegocio, idProducto, idTipoPoliza, idLinea, idPaquete.longValue(), idCotizacion, idMoneda.shortValue());
	}

	/**
	 * Metodo que aplica plantillas a los incisos de una cotizacion de acuerdo
	 * al paquete que se selleccione, si se envia un 0 en el aplicarAPaquete aplicara
	 * indistintamente a cualquier inciso capturada hasta el momento en la
	 * cotizacion
	 * 
	 * @param idToCotizacion
	 * @param idLinea
	 * @param idPaquete
	 * @param aplicarAPaquete
	 */
	@Override	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public List<String> aplicarPlantilla(BigDecimal idToCotizacion, BigDecimal idLinea, Long idNegocioPaquete, Long aplicarAPaquete) {
		List<String> incisosInvalidos = new ArrayList<String>();
		BigDecimal modeloAntiguedadMaxima = null;
		short modelo;		
		NegocioPaqueteSeccion paqueteDelInciso = null;		
		NegocioPaqueteSeccion updatePaquete = entidadService.findById(NegocioPaqueteSeccion.class, idNegocioPaquete);
		
		List<IncisoCotizacionDTO> incisos =  new ArrayList<IncisoCotizacionDTO>();
		//Obtiene campos por Negocio
		NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idLinea);		
		if(aplicarAPaquete.intValue() == 0){
			incisos = incisoService.getIncisosPorLinea(idToCotizacion, negocioSeccion.getSeccionDTO().getIdToSeccion());		
		}else{
			incisos = incisoService.getIncisosPorLineaPaquete(idToCotizacion, negocioSeccion.getSeccionDTO().getIdToSeccion(), 
					aplicarAPaquete);
		}		
		ConfiguracionPlantillaNeg plantilla = this.obtenerPlantilla(
				idToCotizacion, idLinea, BigDecimal.valueOf(idNegocioPaquete));		
		if(plantilla != null){
			int anioActual = Calendar.getInstance().get(Calendar.YEAR);
			for(IncisoCotizacionDTO inciso: incisos){
				paqueteDelInciso = entidadService.findById(NegocioPaqueteSeccion.class, inciso.getIncisoAutoCot().getNegocioPaqueteId());
				modelo = inciso.getIncisoAutoCot().getModeloVehiculo();
				modeloAntiguedadMaxima = paqueteDelInciso.getModeloAntiguedadMax();						
				if(modeloAntiguedadMaxima == null || modeloAntiguedadMaxima.intValue() == 0 || modelo >= anioActual - modeloAntiguedadMaxima.shortValue()){												
					inciso.getIncisoAutoCot().setNegocioPaqueteId(plantilla.getPaqueteId().longValue());
					List<CoberturaCotizacionDTO> nuevasCoberturas = incisoService.obtenerCoberturasParaLaConfiguracion(idToCotizacion, 
							inciso.getId().getNumeroInciso(), idNegocioPaquete);
					// Actualiza paquete del inciso afectado
					inciso.getIncisoAutoCot().setPaquete(updatePaquete.getPaquete());
					incisoService.prepareGuardarInciso(idToCotizacion, inciso.getIncisoAutoCot(), inciso, nuevasCoberturas);
					calculoService.calcular(inciso);
				}else{
					StringBuilder builder = new StringBuilder("");					
					//retornar inciso invalido
					builder.append(paqueteDelInciso.getNegocioSeccion().getSeccionDTO().getDescripcion());
					builder.append("/ inciso ");
					builder.append(inciso.getId().getNumeroInciso().toString());
					builder.append("/");
					builder.append(String.valueOf(modelo));
					builder.append("Antig " + modeloAntiguedadMaxima != null ? modeloAntiguedadMaxima : "NA");
					incisosInvalidos.add(builder.toString());
				}
							
			}
		}else{
			throw new RuntimeException("No existe una plantilla para la combinaci\u00f3n de cotizaci\u00f3n, l\u00ednea y paquete seleccionados. Primero guarde una plantilla.");
		}
		return incisosInvalidos;		
	}

	/**
	 * Metodo que reestablece las condiciones del negocio a un determinado
	 * paquete,linea y cotizacion
	 * 
	 * @param idCotizacion
	 * @param idLinea
	 * @param idPaquete
	 */	
	@Override
	public void reestablecerValoresDelNegocio(BigDecimal idCotizacion,
			BigDecimal idLinea, Long idPaquete) {
		this.eliminarConfiguracionDeCoberturas(idCotizacion, idLinea, idPaquete);
	}

	@Override
	public void eliminarConfiguracionDeCoberturas(BigDecimal idCotizacion,
			BigDecimal idLinea, Long idPaquete) {
		ConfiguracionPlantillaNeg plantilla = this.obtenerPlantilla(
				idCotizacion, idLinea,  BigDecimal.valueOf(idPaquete));		
		List<ConfiguracionPlantillaNegCobertura> items;
		if(plantilla != null){
			items = this.obtenerCoberturas(plantilla.getPlantillaId());
			for(ConfiguracionPlantillaNegCobertura item: items){
				entidadService.remove(item);
			}			
		}	
	}
}
