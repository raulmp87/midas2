package mx.com.afirme.midas2.service.impl.movil.cliente;
import java.util.List;
import java.util.logging.Level;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.domain.movil.cliente.PolizaDescargaMovil;
import mx.com.afirme.midas2.service.movil.cliente.PolizaDescargaMovilService;

/**
 * Facade for entity PolizaDescargaMovil.
 * 
 * @author MyEclipse Persistence Tools
 */
@Stateless
public class PolizaDescargaMovilServiceImpl implements
		PolizaDescargaMovilService {
	// property constants

	@PersistenceContext
	private EntityManager entityManager;

	/**
	 * Perform an initial save of a previously unsaved PolizaDescargaMovil
	 * entity. All subsequent persist actions of this entity should use the
	 * #update() method.
	 * 
	 * @param entity
	 *            PolizaDescargaMovil entity to persist
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void save(PolizaDescargaMovil entity) {
		LogDeMidasEJB3.log("saving PolizaDescargaMovil instance", Level.INFO, null);
		try {
			entityManager.persist(entity);
			LogDeMidasEJB3.log("save successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("save failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Delete a persistent PolizaDescargaMovil entity.
	 * 
	 * @param entity
	 *            PolizaDescargaMovil entity to delete
	 * @throws RuntimeException
	 *             when the operation fails
	 */
	public void delete(PolizaDescargaMovil entity) {
		LogDeMidasEJB3.log("deleting PolizaDescargaMovil instance", Level.INFO, null);
		try {
			entity = entityManager.getReference(PolizaDescargaMovil.class,
					entity.getId());
			entityManager.remove(entity);
			LogDeMidasEJB3.log("delete successful", Level.INFO, null);
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("delete failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Persist a previously saved PolizaDescargaMovil entity and return it or a
	 * copy of it to the sender. A copy of the PolizaDescargaMovil entity
	 * parameter is returned when the JPA persistence mechanism has not
	 * previously been tracking the updated entity.
	 * 
	 * @param entity
	 *            PolizaDescargaMovil entity to update
	 * @return PolizaDescargaMovil the persisted PolizaDescargaMovil entity
	 *         instance, may not be the same
	 * @throws RuntimeException
	 *             if the operation fails
	 */
	public PolizaDescargaMovil update(PolizaDescargaMovil entity) {
		LogDeMidasEJB3.log("updating PolizaDescargaMovil instance", Level.INFO, null);
		try {
			PolizaDescargaMovil result = entityManager.merge(entity);
			LogDeMidasEJB3.log("update successful", Level.INFO, null);
			return result;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("update failed", Level.SEVERE, re);
			throw re;
		}
	}

	public PolizaDescargaMovil findById(Long id) {
		LogDeMidasEJB3.log("finding PolizaDescargaMovil instance with id: " + id,
				Level.INFO, null);
		try {
			PolizaDescargaMovil instance = entityManager.find(
					PolizaDescargaMovil.class, id);
			return instance;
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PolizaDescargaMovil entities with a specific property value.
	 * 
	 * @param propertyName
	 *            the name of the PolizaDescargaMovil property to query
	 * @param value
	 *            the property value to match
	 * @return List<PolizaDescargaMovil> found by query
	 */
	@SuppressWarnings("unchecked")
	public List<PolizaDescargaMovil> findByProperty(String propertyName,
			final Object value) {
		LogDeMidasEJB3.log("finding PolizaDescargaMovil instance with property: "
				+ propertyName + ", value: " + value, Level.INFO, null);
		try {
			final String queryString = "select model from PolizaDescargaMovil model where model."
					+ propertyName + "= :propertyValue";
			Query query = entityManager.createQuery(queryString);
			query.setParameter("propertyValue", value);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find by property name failed", Level.SEVERE, re);
			throw re;
		}
	}

	/**
	 * Find all PolizaDescargaMovil entities.
	 * 
	 * @return List<PolizaDescargaMovil> all PolizaDescargaMovil entities
	 */
	@SuppressWarnings("unchecked")
	public List<PolizaDescargaMovil> findAll() {
		LogDeMidasEJB3.log("finding all PolizaDescargaMovil instances", Level.INFO,
				null);
		try {
			final String queryString = "select model from PolizaDescargaMovil model";
			Query query = entityManager.createQuery(queryString);
			return query.getResultList();
		} catch (RuntimeException re) {
			LogDeMidasEJB3.log("find all failed", Level.SEVERE, re);
			throw re;
		}
	}

}