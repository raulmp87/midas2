package mx.com.afirme.midas2.service.impl.diferenciasAmis;

import static mx.com.afirme.midas2.utils.CommonUtils.onError;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.cobranza.prorroga.ProrrogaDao;
import mx.com.afirme.midas2.dao.informacionVehicular.DiferenciasAmisDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.diferenciasAmis.DiferenciasAmisService;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class DiferenciasAmisServiceImpl implements DiferenciasAmisService {
	@EJB
	protected EntidadService entidadService;
	@EJB
	private DiferenciasAmisDao diferenciasAmisDao;
	@EJB
	private EntidadDao entidadDao;
	
	@Override
	public void guardarDiferenciasAmis(DiferenciasAmis diferenciasAmis) {
		entidadService.save(diferenciasAmis);
	}
	
	@Override
	public void eliminarDiferenciasAmis(BigDecimal idToPoliza) {
		List<DiferenciasAmis> diferencias =  entidadService.findByProperty(DiferenciasAmis.class, "idToPoliza", idToPoliza);
		entidadService.removeAll(diferencias);
	}
	
	@Override
	public void eliminarDiferenciasAmisPorInciso(BigDecimal idToPoliza, BigDecimal numeroInciso) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("idToPoliza", idToPoliza);
		params.put("numeroInciso", numeroInciso);
		List<DiferenciasAmis> diferencias =  entidadService.findByProperties(DiferenciasAmis.class, params);
		entidadService.removeAll(diferencias);
	}
	
	@Override
	public void cambiarEstatus(BigDecimal idToPoliza, Short estatus) {
		List<DiferenciasAmis> diferencias =  entidadService.findByProperty(DiferenciasAmis.class, "idToPoliza", idToPoliza);
		for(DiferenciasAmis diferenciasAmis : diferencias){
			diferenciasAmis.setEstatus(estatus);
		}
		entidadService.saveAll(diferencias);
	}
	
	@Override
	public void cambiarEstatusPorInciso(BigDecimal idToPoliza, Short estatus, BigDecimal numeroInciso) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("idToPoliza", idToPoliza);
		params.put("numeroInciso", numeroInciso);
		List<DiferenciasAmis> diferencias =  entidadService.findByProperties(DiferenciasAmis.class, params);
		for(DiferenciasAmis diferenciasAmis : diferencias){
			diferenciasAmis.setEstatus(estatus);
		}
		entidadService.saveAll(diferencias);
	}

	
		@Override
	public List<DiferenciasAmis> traeLista()  throws MidasException{
		
		List<DiferenciasAmis> listResult = new ArrayList<DiferenciasAmis>();
		
		try {
			return this.callSP( null );
		}
		catch ( Exception e ){
			LogDeMidasInterfaz.log("Excepcion general en DiferenciasAmisServiceImpl.traeLista" + this, Level.WARNING, e);
			onError(e);
		}		
		return listResult;
	}

	@Override
	public List<DiferenciasAmis> traeListaFiltros( DiferenciasAmis filtro ) 
			throws MidasException {
		
		List<DiferenciasAmis> listResult = new ArrayList<DiferenciasAmis>();
		
		try {
			return this.callSP( filtro );
		}
		catch ( Exception e ){
			LogDeMidasInterfaz.log("Excepcion general en DiferenciasAmisServiceImpl.traeLista" + this, Level.WARNING, e);
			onError(e);
		}
		
		return listResult;
	}
	
	@SuppressWarnings("unchecked")
	private List<DiferenciasAmis> callSP( DiferenciasAmis filtro ) throws RuntimeException{	
		
		String spName = "MIDAS.PKGAUT_GENERALES.SPAUT_POLIZASDIFERENCIAS";
		String [] atributosDTO = { "id", "idToPoliza", "version", "marca", "anos", "puertas", "cilindros", "combustible", "interiores", "pasajeros", "numeroInciso", "estatus", "claveError", "numeroSerie", "numeroPoliza" };
		String [] columnasCursor = { "ID", "IDTOPOLIZA", "VERSION", "MARCA", "ANOS", "PUERTAS", "CILINDROS", "COMBUSTIBLE", "INTERIORES", "PASAJEROS", "NUMEROINCISO", "ESTATUS", "CLAVEERROR", "NUMEROSERIE", "NUMEROPOLIZA" };
		List<DiferenciasAmis> list = new ArrayList<DiferenciasAmis>();
		//StoredProcedureHelper storedHelper = null;
		
		try {			
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			//storedHelper = new StoredProcedureHelper(spName);

			if ( filtro != null ){
				storedHelper = this.estableceParams( storedHelper, filtro );
			}
			else{
				storedHelper.estableceParametro( "pFechaIni", null );
				storedHelper.estableceParametro( "pFechaFin", null );
				storedHelper.estableceParametro( "pNumPoliza", 0 );
				storedHelper.estableceParametro( "pIdAgente", 0 );
				storedHelper.estableceParametro( "pEstatus", 0 );
			}
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis", atributosDTO, columnasCursor);
			list = (List<DiferenciasAmis>)storedHelper.obtieneListaResultados();
			
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		
		return list;		
	}	
	
	private StoredProcedureHelper estableceParams( StoredProcedureHelper storedHelper, DiferenciasAmis filtro ) throws Exception{
		
		storedHelper.estableceParametro( "pFechaIni", filtro.getFechaIni() );
		storedHelper.estableceParametro( "pFechaFin", filtro.getFechaFin() );
		storedHelper.estableceParametro( "pNumPoliza", filtro.getNumeroPoliza() );
		storedHelper.estableceParametro( "pIdAgente", filtro.getAgenteId() );
		storedHelper.estableceParametro( "pEstatus", filtro.getEstatus() );
		
		return storedHelper;
	}

	@Override
	public void solicitarCambio( DiferenciasAmis obj ) throws MidasException {
		

		List<DiferenciasAmis> list = new ArrayList<DiferenciasAmis>();
		
		try{		 
			Long id = obj.getId();
			BigDecimal idToPoliza = obj.getIdToPoliza();		 
			list = diferenciasAmisDao.findByProperty("idToPoliza", idToPoliza);	
		 
		 this.actualizarGrupo( list, id );
			
		}
		catch( Exception e ){
			LogDeMidasInterfaz.log("Excepcion general en DiferenciasAmisServiceImpl.solicitarCambio" + this, Level.WARNING, e);
			onError(e);
			
		}
	}
	
	/**
	 * Metodo para cambiar el estatus a 2 
	 * (Cambio solicitado) de la opcion seleccionada,
	 * ademas cambiamos el ACTIVO a 0 de las demas opciones para
	 * ya no mostrarlas en el listado.
	 * 
	 * @param list
	 * @param id
	 * @throws SQLException
	 * @throws Exception
	 */
	private void actualizarGrupo(List<DiferenciasAmis> list, Long id)
			throws SQLException, Exception {

		short estatus = 2;

		for (DiferenciasAmis item : list) {

			if (id.equals(item.getId())) {
				item.setEstatus(estatus);
			} else {
				item.setActivo(0);
			}

			diferenciasAmisDao.update(item);
			
		}
	}

	@Override
	public DiferenciasAmis traerDiferenciasObject(Long id)
			throws MidasException {
		return diferenciasAmisDao.findById(id);
	}

	@Override
	public Agente traerAgenteObject( BigDecimal idToCotizacion ) throws MidasException {
		
		CotizacionDTO cotizacion = entidadDao.findById( CotizacionDTO.class, idToCotizacion );
		
		BigDecimal codigoAgente = cotizacion.getSolicitudDTO().getCodigoAgente();
		Long idAgente = codigoAgente.longValue();
		
		Agente agente = entidadDao.findById( Agente.class, idAgente );
		
		return agente;
		
	}
}
