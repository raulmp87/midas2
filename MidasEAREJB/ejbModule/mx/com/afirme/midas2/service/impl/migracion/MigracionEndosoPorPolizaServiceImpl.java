package mx.com.afirme.midas2.service.impl.migracion;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.domain.migracion.MigEndososValidos;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;
import mx.com.afirme.midas2.service.migracion.EstadoMigracion;
import mx.com.afirme.midas2.service.migracion.MigEndososValidosService;
import mx.com.afirme.midas2.service.migracion.MigracionEndosoPorPolizaService;
import mx.com.afirme.midas2.service.migracion.MigracionEndosoService;
import mx.com.afirme.midas2.service.migracion.MigracionService;

import org.apache.log4j.Logger;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class MigracionEndosoPorPolizaServiceImpl implements
		MigracionEndosoPorPolizaService {

	private MigracionEndosoService migracionEndosoService;
	private MigracionService migracionService;
	private MigEndososValidosService migEndososValidosService;
	private static final Logger log = Logger.getLogger(MigracionEndosoPorPolizaServiceImpl.class);
		
	@EJB
	public void setMigracionEndosoService(
			MigracionEndosoService migracionEndosoService) {
		this.migracionEndosoService = migracionEndosoService;
	}
	
	/**
	 * Dependencia circular todo debido a que esta clase sabe el estatus de la migración.
	 * @param migracionService
	 */
	@EJB
	public void setMigracionService(MigracionService migracionService) {
		this.migracionService = migracionService;
	}
	
	@EJB
	public void setMigEndososValidosService(
			MigEndososValidosService migEndososValidosService) {
		this.migEndososValidosService = migEndososValidosService;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Asynchronous
	public void migrarEndosos(List<MigEndososValidos> migEndososValidosList) {		
		Map<Long, MigEndososValidos> idCotizacionInvalidos = new HashMap<Long, MigEndososValidos>();
		
		MigEndososValidos primerElemento = migEndososValidosList.get(0);
		if (primerElemento != null) {			
			log.info("Procesando los endosos de "
					+ primerElemento.getId().getIdCotizacion()
					+ " total de endosos para procesar: " + migEndososValidosList.size());
		}
		
		int i = 0;
		for (MigEndososValidos migEndososValidos : migEndososValidosList) {
			i++;
			log.info("Procesando el endoso  " + migEndososValidos.getId() + " " + i + "/" + migEndososValidosList.size());
			//Se ha ejecutado la señal de que debemos detener la migración.
			if (migracionService.getEstadoMigracion().equals(EstadoMigracion.DETENIDA)) {
				log.info("Deteniendo el procesos de la migración para el idCotizacion: " + migEndososValidos.getId().getIdCotizacion());
				return;
			}
			if (!migEndososValidos.getClaveProceso().equals(MigEndososValidos.CLAVE_PROCESO_NO_PROCESADO)) {
				log.info("El registro idCotizacion: "
						+ migEndososValidos.getId().getIdCotizacion()
						+ " idVersionPol"
						+ migEndososValidos.getId().getIdVersionPol()
						+ " ya habia sido procesado.");
				if (migEndososValidos.getClaveProceso().equals(MigEndososValidos.CLAVE_PROCESO_INVALIDO)) {
					idCotizacionInvalidos.put(migEndososValidos.getId().getIdCotizacion(), migEndososValidos);
				}
				continue;
			}
			
			try {
				MigEndososValidos posibleEndosoInvalido = idCotizacionInvalidos.get(migEndososValidos.getId().getIdCotizacion());
				if (posibleEndosoInvalido != null) {
					final String texto = "No se procesa este endoso debido a que el endoso "
							+ posibleEndosoInvalido.getId()
							+ " no fue procesado correctamente.";
					log.info(texto);
					migEndososValidos.setClaveProceso(MigEndososValidos.CLAVE_PROCESO_INVALIDO);
					migEndososValidos.setTexto(texto);
					actualizar(migEndososValidos);
					continue;
				}
				
				migracionEndosoService.migrarEndoso(migEndososValidos);
			} catch(Exception e) {
				log.info("No se pudo procesar la migracion de endoso de " + migEndososValidos.getId(), e);
				idCotizacionInvalidos.put(migEndososValidos.getId().getIdCotizacion(), migEndososValidos);
			}
			//Borrar la referencia por cada endoso procesado.
			TimeUtils.clearReference();
		}
	}
	
	/* (non-Javadoc)
	 * Si se corre asincrono puede causar problemas ya que si mas de un timer corre sin haber dejado terminar a los otros entonces
	 * esto causuaria conflicto. Existe solución para esto sólo que lo actual no esta preparado para soportarlo.
	 * @see mx.com.afirme.midas2.service.migracion.MigracionEndosoPorPolizaService#migrarEndososEmisionDelegada(java.util.List)
	 */
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void migrarEndososEmisionDelegada(List<MigEndososValidosEd> migEndososValidosEdList) {
		Map<Long, MigEndososValidosEd> idCotizacionInvalidos = new HashMap<Long, MigEndososValidosEd>();
		
		MigEndososValidosEd primerElemento = migEndososValidosEdList.get(0);
		if (primerElemento != null) {			
			log.info("Procesando los endosos de "
					+ primerElemento.getId().getIdCotizacion()
					+ " total de endosos para procesar: " + migEndososValidosEdList.size());
		}
		
		int i = 0;
		for (MigEndososValidosEd migEndososValidosEd : migEndososValidosEdList) {
			i++;
			log.info("Procesando el endoso  " + migEndososValidosEd.getId() + " " + i + "/" + migEndososValidosEdList.size());
			//Se ha ejecutado la señal de que debemos detener la migración.
			if (migracionService.getEstadoMigracionEd().equals(EstadoMigracion.DETENIDA)) {
				log.info("Deteniendo el procesos de la migración para el idCotizacion: " + migEndososValidosEd.getId().getIdCotizacion());
				return;
			}
			if (!migEndososValidosEd.getClaveProceso().equals(MigEndososValidosEd.CLAVE_PROCESO_NO_PROCESADO)) {
				log.info("El registro idCotizacion: "
						+ migEndososValidosEd.getId().getIdCotizacion()
						+ " idVersionPol"
						+ migEndososValidosEd.getId().getIdVersionPol()
						+ " ya habia sido procesado.");
				if (migEndososValidosEd.getClaveProceso().equals(MigEndososValidosEd.CLAVE_PROCESO_INVALIDO)) {
					idCotizacionInvalidos.put(migEndososValidosEd.getId().getIdCotizacion(), migEndososValidosEd);
				}
				continue;
			}
			
			try {
				MigEndososValidosEd posibleEndosoInvalido = idCotizacionInvalidos.get(migEndososValidosEd.getId().getIdCotizacion());
				if (posibleEndosoInvalido != null) {
					final String texto = "No se procesa este endoso debido a que el endoso "
							+ posibleEndosoInvalido.getId()
							+ " no fue procesado correctamente.";
					log.info(texto);
					migEndososValidosEd.setClaveProceso(MigEndososValidosEd.CLAVE_PROCESO_INVALIDO);
					migEndososValidosEd.setTexto(texto);
					actualizar(migEndososValidosEd);
					continue;
				}
				
				migracionEndosoService.migrarEndosoEmisionDelegada(migEndososValidosEd);
			} catch(Exception e) {
				log.info("No se pudo procesar la migracion de endoso de " + migEndososValidosEd.getId(), e);
				idCotizacionInvalidos.put(migEndososValidosEd.getId().getIdCotizacion(), migEndososValidosEd);
			}
			//Borrar la referencia por cada endoso procesado.
			TimeUtils.clearReference();
		}
	}
	
	private void actualizar(MigEndososValidos migEndososValidos) {
		try {
			migEndososValidosService.actualizar(migEndososValidos);
		} catch(Exception e) {
			log.info("No se pudo guardar el codigo de error de " + migEndososValidos.getId(), e);
		}
	}
	
	private void actualizar(MigEndososValidosEd migEndososValidosEd) {
		try {
			migEndososValidosService.actualizar(migEndososValidosEd);
		} catch(Exception e) {
			log.info("No se pudo guardar el codigo de error de " + migEndososValidosEd.getId(), e);
		}
	}
}
