package mx.com.afirme.midas2.service.impl.catalogos.reaseguradorcnsf;

import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDTO;
import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas2.dao.catalogos.reaseguradorcnsf.ReaseguradorCnsfDao;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsf;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;
import mx.com.afirme.midas2.service.catalogos.reaseguradorcnsf.ReaseguradorCnsfService;
import mx.com.afirme.midas2.service.impl.catalogos.EntidadServiceImpl;

@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class ReaseguradorCnsfServiceImpl extends EntidadServiceImpl implements
		ReaseguradorCnsfService {

	private ReaseguradorCnsfDao reaseguradorCnsfDao;

	@Override
	public List<ReaseguradorCnsfMov> findByFilters(ReaseguradorCnsfMov filtro) {
		ReaseguradorCnsf reaseguradorCnsf;
		CalificacionAgenciaDTO calificacionAgenciaDTO;
		ArrayList<ReaseguradorCnsfMov> listaarray = new ArrayList<ReaseguradorCnsfMov>();
		
		if (filtro == null)
			filtro = new ReaseguradorCnsfMov();
		
		List<ReaseguradorCnsfMov> lista = reaseguradorCnsfDao.findByFilters(filtro);
		
		for (Iterator<ReaseguradorCnsfMov> iterator = lista.iterator() ; iterator.hasNext(); ) {
			ReaseguradorCnsfMov reaseguradorCnsfMov = (ReaseguradorCnsfMov) iterator.next();

			reaseguradorCnsf = reaseguradorCnsfDao.findById(ReaseguradorCnsf.class, reaseguradorCnsfMov.getIdreasegurador());
			reaseguradorCnsfMov.setClaveReasegurador(reaseguradorCnsf.getClaveCnsf());
			reaseguradorCnsfMov.setClaveReaseguradorAnt(reaseguradorCnsf.getClaveCnsfAnterior());
			reaseguradorCnsfMov.setNombreReasegurador(reaseguradorCnsf.getNombreReasegurador());

			calificacionAgenciaDTO = reaseguradorCnsfDao.findById(CalificacionAgenciaDTO.class, reaseguradorCnsfMov.getIdcalificacion());
			reaseguradorCnsfMov.setCalificacion(calificacionAgenciaDTO.getCalificacion());
			reaseguradorCnsfMov.setNombreAgencia(calificacionAgenciaDTO.getAgencia().getDescription());

			listaarray.add(reaseguradorCnsfMov);
		}

		return listaarray;
	}

	public byte[] toArrayByte(List<ReaseguradorCnsfMov> list)
			throws IOException {

		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);
		out.write(encabezadoDetalle());

		for (ReaseguradorCnsfMov item : list) {

			out.writeBytes(item.getFechacorte() + "|"
					+ item.getNombreReasegurador() + "|"
					+ item.getClaveReasegurador() + "|"
					+ item.getClaveReaseguradorAnt() + "|"
					+ item.getNombreAgencia() + "|" + item.getCalificacion()
					+ System.getProperty("line.separator"));

		}

		return baos.toByteArray();
	}

	public byte[] encabezadoDetalle() throws IOException {
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
		DataOutputStream out = new DataOutputStream(baos);

		out.writeBytes("FECHA CORTE" + '|' + "NOMBRE REASEGURADORA" + '|'
				+ "CLAVE CNSF" + '|' + "CLAVE CNSF ANTERIOR" + '|'
				+ "AGENCIA CALIFICACION" + '|' + "CALIFICACION"
				+ System.getProperty("line.separator"));

		return baos.toByteArray();
	}

	@EJB
	public void setReaseguradorCnsfDao(ReaseguradorCnsfDao reaseguradorCnsfDao) {
		this.reaseguradorCnsfDao = reaseguradorCnsfDao;
	}

	public Map<BigDecimal, String> getMapAgencias() {

		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();

		List<AgenciaCalificadoraDTO> derechosList = reaseguradorCnsfDao
				.findAll(AgenciaCalificadoraDTO.class);
		for (AgenciaCalificadoraDTO item : derechosList) {
			map.put(item.getIdagencia(), item.getDescription());
		}
		return map;
	}

	public List<CalificacionAgenciaDTO> getListCalificaciones(
			BigDecimal idAgencia) {

		List<CalificacionAgenciaDTO> derechosList;
		derechosList = reaseguradorCnsfDao.findByProperty(
				CalificacionAgenciaDTO.class, "agencia.idagencia", idAgencia);

		return derechosList;
	}

	public Map<BigDecimal, String> getMapCalificaciones(BigDecimal idAgencia) {

		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<CalificacionAgenciaDTO> derechosList;
		if (idAgencia == null)
			derechosList = reaseguradorCnsfDao
					.findAll(CalificacionAgenciaDTO.class);
		else
			derechosList = reaseguradorCnsfDao.findByProperty(
					CalificacionAgenciaDTO.class, "agencia.idagencia",
					idAgencia);
		for (CalificacionAgenciaDTO item : derechosList) {
			map.put(item.getId(), item.getCalificacion());
		}
		return map;
	}

	public Map<BigDecimal, String> getMapCalificacionesAll() {

		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<CalificacionAgenciaDTO> derechosList;

		derechosList = reaseguradorCnsfDao
				.findAll(CalificacionAgenciaDTO.class);

		for (CalificacionAgenciaDTO item : derechosList) {
			map.put(item.getId(), item.getCalificacion());
		}
		return map;
	}

	public void deleteReaseguradorMovs(BigDecimal idAgencia) {

		reaseguradorCnsfDao.deleteReaseguradorMovs(idAgencia);
	}

	public void saveReaseguradorMovs(ReaseguradorCnsfMov reaseguradorCnsfMov) {
		reaseguradorCnsfDao.saveReaseguradorMovs(reaseguradorCnsfMov);
	}
	
	public Date getFechaUltimoCorteProcesado() {
		Date fechaCorte;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			fechaCorte = reaseguradorCnsfDao.getFechaUltimoCorteProcesado();
			LogDeMidasEJB3.log("Fecha de ultimo corte encontrada " + sdf.format(fechaCorte), Level.INFO, null);
		} catch(Exception e) {
			LogDeMidasEJB3.log("Ha ocurrido un error al tratar de obtener la ultima fecha de corte procesada. " + e.getMessage(), Level.WARNING,null);
			fechaCorte = new Date();
		}
		
		return fechaCorte;
	}

}
