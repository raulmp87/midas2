package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.HashMap;
import java.util.Map;

import javax.ejb.Stateless;
import javax.ejb.EJB;

import java.io.Serializable;
import java.util.List;

import mx.com.afirme.midas2.dao.sapamis.catalogos.CatSapAmisSupervisionCampoDao;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSapAmisSupervisionCampo;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatSapAmisSupervisionCampoService;

@Stateless 
public class CatSapAmisSupervisionCampoServiceImpl implements CatSapAmisSupervisionCampoService {
	private static final long serialVersionUID = 1L;

	@EJB 
	private EntidadService entidadService;
	@EJB 
	private CatSapAmisSupervisionCampoDao catSapAmisSupervisionCampoDao;
	
	@Override
	public boolean existeClaveAmis(Long claveAmis){
		List<CatSapAmisSupervisionCampo> catSapAmisSupervisionCampoList = entidadService.findByProperty(CatSapAmisSupervisionCampo.class, "claveAmis", claveAmis);
		return (catSapAmisSupervisionCampoList.size()>0);
	}
	
	@Override
	public CatSapAmisSupervisionCampo obtenerPorId(Long id) {
		CatSapAmisSupervisionCampo catSapAmisSupervisionCampo = new CatSapAmisSupervisionCampo();
		if(id!=null){
			catSapAmisSupervisionCampo = entidadService.findById(CatSapAmisSupervisionCampo.class, id);
		}
		return catSapAmisSupervisionCampo;
	}
	
	@Override
	public List<CatSapAmisSupervisionCampo> obtenerPorFiltros(CatSapAmisSupervisionCampo catSapAmisSupervisionCampo) {
		if(catSapAmisSupervisionCampo==null) {
			catSapAmisSupervisionCampo = new CatSapAmisSupervisionCampo();
		}
		return catSapAmisSupervisionCampoDao.findByFilters(catSapAmisSupervisionCampo);
	}

	public Map<String, String> obtenerCatalogo() {
		List<CatSapAmisSupervisionCampo> catSapAmisSupervisionCampoList;
		Map<String, String> catSapAmisSupervisionCampoMap = new HashMap<String, String>();;
		catSapAmisSupervisionCampoList = entidadService.findAll(CatSapAmisSupervisionCampo.class);
		for(CatSapAmisSupervisionCampo catSapAmisSupervisionCampo : catSapAmisSupervisionCampoList){
			catSapAmisSupervisionCampoMap.put(catSapAmisSupervisionCampo.getClaveAmis().toString(), catSapAmisSupervisionCampo.getSupervisionCampo());
		}
		return catSapAmisSupervisionCampoMap;
	}
	
	@Override
	public boolean guardar(CatSapAmisSupervisionCampo catSapAmisSupervisionCampo){
		if(catSapAmisSupervisionCampo!=null){
			try{
				entidadService.save(catSapAmisSupervisionCampo);
				return true;
			}catch(Exception e){
				return false;
			}
		}else{
			return false;
		}
	}
}