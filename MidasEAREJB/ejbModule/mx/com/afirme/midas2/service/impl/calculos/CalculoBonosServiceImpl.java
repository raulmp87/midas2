package mx.com.afirme.midas2.service.impl.calculos;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeDTO;
import mx.com.afirme.midas.interfaz.solicitudcheque.SolicitudChequeFacadeRemote;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.bonos.ConfigBonosDao;
import mx.com.afirme.midas2.dao.calculos.CalculoBonosDao;
import mx.com.afirme.midas2.dao.calculos.CalculoBonosDao.EstatusCalculoBonos;
import mx.com.afirme.midas2.domain.bonos.CalculoBono;
import mx.com.afirme.midas2.domain.bonos.ConfigBonos;
import mx.com.afirme.midas2.domain.bonos.DetalleCalculoBono;
import mx.com.afirme.midas2.domain.bonos.ProgramacionBono;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.CalculoBonoEjecuciones;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgenteEst;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HonorariosAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.bonos.DetalleBonoPolizaView;
import mx.com.afirme.midas2.dto.bonos.DetalleBonoRamoSubramoView;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteView;
import mx.com.afirme.midas2.dto.fuerzaventa.PreviewCalculoBonoView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.calculos.CalculoBonosService;
import mx.com.afirme.midas2.service.calculos.SolicitudChequesMizarService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte.TipoSalidaReportes;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.threads.MailThreadSupport;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.MidasException;
import mx.com.afirme.midas2.utils.MailServiceSupport;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

@Stateless
public class CalculoBonosServiceImpl implements CalculoBonosService{
	
	@Override
	public List<CalculoBono> findByFiltersCalculo(CalculoBono calculoBono) {
		return calculoBonosDao.findByFiltersCalculo(calculoBono);
	}

	@Override
	public List<DetalleCalculoBono> findByFiltersDetalleCalculo(DetalleCalculoBono detalleCalculoBono) {
		return calculoBonosDao.findByFiltersDetalleCalculo(detalleCalculoBono);
	}

	@Override
	public CalculoBono loadByIdCalculo(Long idCalculo) {
		return calculoBonosDao.loadByIdCalculo(idCalculo);
	}

	@Override
	public DetalleCalculoBono loadByIdDetalleCalculo(Long idDetalleCalculo) {
		return calculoBonosDao.loadByIdDetalleCalculo(idDetalleCalculo);
	}
	
	public CalculoBonosDao getCalculoBonosDao() {
		return calculoBonosDao;
	}

	

	@Override
	public List<BigDecimal> getCentrosOperacionByConfiguracion(Long idConfig) throws MidasException{
		return calculoBonosDao.getCentrosOperacionByConfiguracion(idConfig);
	}

	@Override
	public List<BigDecimal> getEjecutivosByConfiguracion(Long idConfig) throws MidasException{
		return calculoBonosDao.getEjecutivosByConfiguracion(idConfig);
	}

	@Override
	public List<BigDecimal> getGerenciasByConfiguracion(Long idConfig) throws MidasException{
		return calculoBonosDao.getGerenciasByConfiguracion(idConfig);
	}

	@Override
	public List<BigDecimal> getPrioridadesByConfiguracion(Long idConfig) throws MidasException{
		return calculoBonosDao.getPrioridadesByConfiguracion(idConfig);
	}

	@Override
	public List<BigDecimal> getPromotoriasByConfiguracion(Long idConfig) throws MidasException{
		return calculoBonosDao.getPromotoriasByConfiguracion(idConfig);
	}

	@Override
	public List<BigDecimal> getSituacionesByConfiguracion(Long idConfig) throws MidasException{
		return calculoBonosDao.getSituacionesByConfiguracion(idConfig);
	}

	@Override
	public List<BigDecimal> getTiposAgentesByConfiguracion(Long idConfig) throws MidasException{
		return calculoBonosDao.getTiposAgentesByConfiguracion(idConfig);
	}

	@Override
	public List<BigDecimal> getTiposPromotoriasByConfiguracion(Long idConfig) throws MidasException{
		return calculoBonosDao.getTiposPromotoriasByConfiguracion(idConfig);
	}

	@Override
	public List<AgenteView> obtenerAgentesPorConfiguracion(ConfigBonos configComisiones) throws Exception {
		return calculoBonosDao.obtenerAgentesPorConfiguracion(configComisiones);
	}

	@Override
	public Long generarCalculo(Long idConfigComisiones,List<AgenteView> agentesDelCalculo) throws MidasException {
		return calculoBonosDao.generarCalculo(idConfigComisiones, agentesDelCalculo);
	}

	@Override
	public void validarAgentesEnCalculosPendientes(Long idCalculoTemporal) throws MidasException {
		calculoBonosDao.validarAgentesEnCalculosPendientes(idCalculoTemporal);
	}

	@Override
	public Long ejecutarCalculo(ProgramacionBono progBono, CalculoBono calculoBono, Long modoEjecucion, Integer isNegocio, String conceptoEjecucionAutomatica) throws MidasException {		
		return calculoBonosDao.ejecutarCalculo(progBono,calculoBono,modoEjecucion, isNegocio, conceptoEjecucionAutomatica);
	}
	
	@Override
	public Long saveCalculoBono(CalculoBono calculo) {
		return calculoBonosDao.saveCalculoBono(calculo);
	}

	@Override
	public Long saveDetalleCalculoBonos(DetalleCalculoBono detalleCalculoBono)throws MidasException {
		return calculoBonosDao.saveDetalleCalculoBonos(detalleCalculoBono);
	}

	@Override
	public Long saveListDetalleCalculoBonos(List<DetalleCalculoBono> list)throws MidasException {
		return calculoBonosDao.saveListDetalleCalculoBonos(list);
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void autorizarPreview(CalculoBono calculo) throws Exception {
		
		if (calculo == null || calculo.getId() == null) {
			
			throw new RuntimeException(getMessage("midas.agente.comisiones.autorizacion.calculo.vacio"));
			
		}
		
		calculo = entidadService.evictAndFindById(CalculoBono.class, calculo.getId());
		
		
		if (calculo.getStatus() == null || calculo.getStatus().getValor() == null 
				|| (!calculo.getStatus().getValor().equals(EstatusCalculoBonos.PENDIENTE_AUTORIZAR.getValue())
						&& !calculo.getStatus().getValor().equals(EstatusCalculoBonos.PROCESO_AUTORIZACION.getValue()))) {
			
			throw new RuntimeException(getMessage("midas.agente.comisiones.autorizacion.estatus.incorrecto"));
			
		}
		
		bloquearProcesoCalculoBonos(calculo);
		
		try {
		
			Long idCalculo = calculo.getId();
						
			/*
			Si el estatus actual es 'EN PROCESO DE AUTORIZACION', saltarse todas las validaciones iniciales y 
			empezar a partir de la generacion de solicitudes de cheque.
			*/
			
			if (calculo.getStatus().getValor().equals(EstatusCalculoBonos.PENDIENTE_AUTORIZAR.getValue())) {
				
				if (calculo.getImporteTotal() == null 
						|| calculo.getImporteTotal().compareTo(BigDecimal.ZERO) == 0) {
					
					throw new RuntimeException(getMessage("midas.agente.bonos.autorizacion.cheque.proceso.error", idCalculo));
					
				}
								
				//Se cambia al estatus 'EN PROCESO DE AUTORIZACION' para marcar al calculo
				
				actualizarEstatus(calculo, EstatusCalculoBonos.PROCESO_AUTORIZACION);
								
				if (!esAutorizacionValida(calculo.getId())) {
					
					actualizarEstatus(calculo, EstatusCalculoBonos.PENDIENTE_AUTORIZAR);
					
					throw new RuntimeException(getMessage("midas.agente.bonos.autorizacion.duplicado"));
					
				}
				
			}
									
			//Se crean las solicitudes de cheque en Seycos
			
			calculoBonosDao.generarSolicitudesDeChequePorCalculo(idCalculo);
			
			//Si se autorizaron (crearon) todas las solicitudes de cheque en Mizar, se autoriza el calculo
			
			generarSolicitudesChequeMizar(idCalculo);
			
			actualizarEstatus(calculo, EstatusCalculoBonos.AUTORIZADO);
			
			//Se notifica por correo
			mailThreadMethodSupport(idCalculo,
					GenericMailService.P_PAGO_BONOS,
					GenericMailService.M_AUTORIZACION_DEL_MOVIMIENTO_BONO, null,
					GenericMailService.T_GENERAL,"enviarCorreoAutorizacionMovimiento");
							
		} catch (RuntimeException e) {
			
			throw e;
			
		} catch (Exception e) {
			
			throw new RuntimeException(e.getMessage());
			
		} finally {

			desbloquearProcesoCalculoBonos(calculo);

		}
		
	}

	/**
	 * Metodo que obtiene las programaciones de bonos activas automaticas por ejecutar.
	 * conceptoEjecucionAutomatica = parametro para identificar si se ejecuto para Provisiones o Bonos
	 * @return
	 * @throws MidasException
	 */
	@Override
	public List<ProgramacionBono> obtenerConfiguracionesAutomaticasActivas(String conceptoEjecucionAutomatica) throws MidasException{
		return calculoBonosDao.obtenerConfiguracionesAutomaticasActivas(conceptoEjecucionAutomatica);
	}
	/**
	 * Permite ejecutar bono automatico
	 * @param progBono
	 * @param calculoBono
	 * @param isNegocio
	 * @return
	 * @throws MidasException
	 */
	@Override
	public void ejecutarCalculoAutomatico(ProgramacionBono progBono, CalculoBono calculoBono,String conceptoEjecucionAutomatica) throws MidasException{
		calculoBonosDao.ejecutarCalculoAutomatico(progBono, calculoBono,conceptoEjecucionAutomatica);
	}
	/**
	 * Ejecuta el preview de los calculos de bonos automaticos
	 * @param lista
	 * @return
	 * @throws MidasException
	 */
	@Override
	public void ejecutarCalculosAutomaticos(List<ProgramacionBono> lista,String conceptoEjecucionAutomatica) throws MidasException{
		calculoBonosDao.ejecutarCalculosAutomaticos(lista,conceptoEjecucionAutomatica);
	}

	@Override
	public Map<String,Map<String, List<String>>> obtenerCorreos(Long id, Long idProceso,
			Long idMovimiento) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void enviarCorreo(Map<String,Map<String, List<String>>> mapCorreos,
			String mensaje,String tituloMensaje ,int tipoTemplate,List<ByteArrayAttachment> attachment) {
		
	}

	@Override
	public void mailThreadMethodSupport(Long id, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate,String methodToExcecute) {
		MailThreadSupport t = MailThreadSupport.getInstance();
		t.setService(this);
		t.setIdProceso(idProceso);
		t.setIdMovimiento(idMovimiento);
		t.setTipoTemplate(tipoTemplate);
		t.setId(id);
		t.setMethodToExcecute(methodToExcecute);
		Thread test =  new Thread(t);
		test.setName("Notificacion bono" + id);
		test.start();
	}
	@Override
	public synchronized void enviarCorreoGeneracionPreview(Long id,Long idProceso, Long idMovimiento,String mensaje,int tipoTemplate){
		CalculoBono calculo = calculoBonosDao.findById(CalculoBono.class, id);
		Calendar fechaCorte = Calendar.getInstance();
		fechaCorte.setTime(calculo.getFechaCorte());
		TransporteImpresionDTO resumenPreview = generarPlantillaReporte
				.imprimirReportePreviewBonosExcel(null, null, null, null, null,
						(fechaCorte.get(Calendar.MONTH)+1) + "",
						fechaCorte.get(Calendar.YEAR) + "", null, null,
						calculo.getId(),TipoSalidaReportes.TO_EXCEL.getValue());
		List<ByteArrayAttachment> attachment = null;
		if (resumenPreview != null) {
			attachment = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment arrayAttachment = new ByteArrayAttachment(
					"resumenPreviewBonos.xls", ByteArrayAttachment.TipoArchivo.XLS,
					resumenPreview.getByteArray());
			attachment.add(arrayAttachment);
		}
		System.out.println("*********************** Anexo " + attachment.get(0).getContenidoArchivo());
		mailService.sendMailToAdminAgentes("GENERACION PREVIEW",
				MailServiceSupport
						.mensajeAdminAgentePagoBonoGeneraPreview(calculo
								.getImporteTotal().toString()),
				"GENERACION PREVIEW DE BONOS", attachment,null);
	}
	@Override
	public synchronized void enviarCorreoAutorizacionMovimiento(Long id, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate) {
		CalculoBono calculo = calculoBonosDao.findById(CalculoBono.class, id);
		Calendar fechaCorte = Calendar.getInstance();
		fechaCorte.setTime(calculo.getFechaCorte());
		String anio = fechaCorte.get(Calendar.YEAR) + "";
		String mes = fechaCorte.get(Calendar.MONTH) < 10 ? "0"
				+ fechaCorte.get(Calendar.MONTH) : fechaCorte
				.get(Calendar.MONTH) + "";
		// Evia la notificacion al administrador de agentes
		List<ByteArrayAttachment> attachmentGeneral = null;
		TransporteImpresionDTO reporteGlobal = generarPlantillaReporte
				.imprimirRepGlobalComisionYbono(null,
						calculo.getFechaCorte(), calculo.getId(),
						Boolean.TRUE);
		if (reporteGlobal != null && reporteGlobal.getByteArray() != null) {
			attachmentGeneral = new ArrayList<ByteArrayAttachment>();
			ByteArrayAttachment byteArrayAttachment = new ByteArrayAttachment();
			byteArrayAttachment
					.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
			byteArrayAttachment.setContenidoArchivo(reporteGlobal
					.getByteArray());
			byteArrayAttachment
					.setNombreArchivo("reportePagoBonosAgenteGlobal_bono"
							+ calculo.getFechaCorte() + ".xls");
			attachmentGeneral.add(byteArrayAttachment);
		}
		mailService.sendMailToAdminAgentes("PAGO DE BONOS NOTIFICACION ADMINISTRADOR", MailServiceSupport
				.mensajeAdminAgentePagoBono(calculo.getImporteTotal().toString()),
				"PAGO DE BONOS", attachmentGeneral,null);
		List<DetalleCalculoBono> detalleCalculoBonos = calculoBonosDao
				.findByProperty(DetalleCalculoBono.class, "calculoBono.id", id);
		Agente agente = new Agente();
		// Envia la notificacion por agente
		for (DetalleCalculoBono detalle : detalleCalculoBonos) {
			final Long idAgente = detalle.getIdBeneficiario();
			if (idAgente != null) {
				agente.setIdAgente(idAgente);
				try {
					agente = agenteMidasService.findByClaveAgente(agente);
					Map<String,Map<String, List<String>>> mapCorreos = agenteMidasService
							.obtenerCorreos(idAgente, idProceso, idMovimiento);
					// TODO: Adjuntar reporte Siniestralidad
					List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
					TransporteImpresionDTO reporteBono = generarPlantillaReporte
					.imprimirRepGlobalComisionYbono(idAgente,
							calculo.getFechaCorte(), calculo.getId(),
							Boolean.TRUE);
					if (reporteBono != null) {
						ByteArrayAttachment detalleBonoAttach = new ByteArrayAttachment(
								"reporteBonoAgente"+idAgente,
								ByteArrayAttachment.TipoArchivo.XLS,
								reporteBono.getByteArray());
						attachment.add(detalleBonoAttach);
					}
					for (Entry<String, Map<String, List<String>>> map : mapCorreos
							.entrySet()) {
						mensaje = MailServiceSupport
								.mensajePagoBonosAutorizacionMovimiento(
										agente.getPersona().getNombreCompleto(),
										detalle.getBonoPagar().toString(),
										mes,anio,
										mapCorreos.get(map.getKey()).get(
												GenericMailService.NOTA));
						mailService.sendMailAgenteNotificacion(
								mapCorreos.get(map.getKey()).get(
										GenericMailService.PARA),
								mapCorreos.get(map.getKey()).get(
										GenericMailService.COPIA),
								mapCorreos.get(map.getKey()).get(
										GenericMailService.COPIA_OCULTA),
								"PAGO DE BONOS", mensaje, attachment,
								"PAGO DE BONOS", null,
								GenericMailService.T_GENERAL);
					}
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		}

	}
	
	@Override
	public List<PreviewCalculoBonoView> findByFiltersCalculoView(CalculoBono calculoBono) {
		return calculoBonosDao.findByFiltersCalculoView(calculoBono);
	}

	@Override
	public CalculoBono eliminaCalculoBonos(CalculoBono calculoBono) throws MidasException {
		return calculoBonosDao.eliminaCalculoBonos(calculoBono);		
	}
	
	@Override	
	public TransporteImpresionDTO getDetalleBonosExcel(CalculoBono calculoBono) {
		
		List<DetalleBonoPolizaView> detalleBonoPoliza = calculoBonosDao.obtenerDetalleBonoPoliza(calculoBono);
		
		return impresionesService.getExcel(detalleBonoPoliza, 
				"midas.agente.reporte.bono.detallePoliza.plantilla.archivo.nombre",ConstantesReporte.TIPO_XLSX);
	}
	@Override
	public TransporteImpresionDTO getDetalleBonosNivelCoberturaExcel(
			CalculoBono calculoBono) {
		List<DetalleBonoPolizaView> detalleBonoPoliza = calculoBonosDao.obtenerDetalleBonoNivelCobertura(calculoBono);
		return impresionesService.getExcel(detalleBonoPoliza, 
				"midas.agente.reporte.bono.detalleBonoNivelCobertura.plantilla.archivo.nombre",ConstantesReporte.TIPO_XLSX);
	}

	@Override
	public void enviarAListadoEjecucionCalculoBonoManual(ProgramacionBono programacionBono, List<Negocio> negociosEspecialesList, List<ConfigBonos> configuracionBonoList,
			Usuario usuario, Boolean isManual) throws MidasException{	
	    calculoBonosDao.enviarAListadoEjecucionCalculoBonoManual(programacionBono,negociosEspecialesList,configuracionBonoList,usuario,isManual);
	} 


//	@Override
//	public void ejecutarDetalleMensualPolizasEmitidasYPagadasLog() throws MidasException {
//		calculoBonosDao.ejecutarDetalleMensualPolizasEmitidasYPagadasLog();	
//	}
	
	@Override
	public TransporteImpresionDTO getDetalleBonosNivelRamoSubramoExcel(CalculoBono calculoBono) {
		List<DetalleBonoRamoSubramoView> detalleBonoRamoSubramo = calculoBonosDao.obtenerDetalleBonoRamoSubramo(calculoBono);
		
		return impresionesService.getExcel(detalleBonoRamoSubramo, 
				"midas.agente.reporte.bono.detalleBonoRamoSubramo.plantilla.archivo.nombre",ConstantesReporte.TIPO_XLSX);
	}

	@Override
	public TransporteImpresionDTO getDetalleBonosNivelAmisExcel(CalculoBono calculoBono) {
		List<DetalleBonoPolizaView> detalleBonoAmis = calculoBonosDao.getDetalleBonosNivelAmisExcel(calculoBono);
		
		return impresionesService.getExcel(detalleBonoAmis, 
				"midas.agente.reporte.bono.detalleBonoAmis.plantilla.archivo.nombre",ConstantesReporte.TIPO_XLSX);
	}

	public void autorizacionAutomaticaBonos() {
		
		logger.info(getMessage("midas.agente.bonos.autorizacion.automatica.inicio"));
		
		List<CalculoBono> calculos = entidadService.findByProperty(CalculoBono.class, "status.valor", EstatusCalculoBonos.PROCESO_AUTORIZACION.getValue());
		
		for (CalculoBono calculo : calculos) {
			
			try {
				
				autorizarPreview(calculo);
				
				logger.info(getMessage("midas.agente.bonos.autorizacion.automatica.detalle.fin", calculo.getId()));
				
			} catch (Exception ex) {
				
				logger.error(getMessage("midas.agente.bonos.autorizacion.automatica.detalle.error", calculo.getId(), ex.getMessage()));
				
			}			
			
		}		
		
		logger.info(getMessage("midas.agente.bonos.autorizacion.automatica.fin"));
		
	}
	
	@Override
	public void rehabilitarDetalleBono(DetalleCalculoBono detalleCalculoBono) {
		
		detalleCalculoBono = entidadService.getReference(DetalleCalculoBono.class, detalleCalculoBono.getId());
				
		if (detalleCalculoBono != null && detalleCalculoBono.getStatus().getValor().equals(EstatusCalculoBonos.CANCELADO.getValue())) {
			
			try {
				
				actualizarDetalle(detalleCalculoBono, EstatusCalculoBonos.PENDIENTE_AUTORIZAR);
				
			} catch (Exception ex) {
				
				throw new RuntimeException(getMessage("midas.agente.bonos.autorizacion.cheque.rehabilitacion.error.especifico", detalleCalculoBono.getId()));
				
			}
					
		} else {
			
			throw new RuntimeException(getMessage("midas.agente.bonos.autorizacion.cheque.rehabilitacion.error"));
						
		}
				
	}
	
	
	private void generarSolicitudesChequeMizar(Long idCalculo) throws Exception {
		
		List<DetalleCalculoBono> detallesCalculo = null;
		SolicitudChequeDTO solicitudCheque = null;
		BigDecimal idSolicitudCheque;
		StringBuilder msg = new StringBuilder();
		String msgInvalido = getMessage("midas.agente.bonos.autorizacion.invalido");
		String msgError = getMessage("midas.agente.bonos.autorizacion.error");
		String msgCancelados = getMessage("midas.agente.bonos.autorizacion.cheque.cancelado");
		
		//A menos que exista algun problema de validacion
		boolean valido = true;
		
		//A menos de que se presente un error técnico
		boolean conErrores = false;
		
		//A menos que algun cheque se encuentre cancelado
		boolean conChequesCancelados = false;
		
		detallesCalculo = calculoBonosDao.listarDetalleDeCalculoPorAutorizar(idCalculo);
			
		for (DetalleCalculoBono detalleCalculo : detallesCalculo) {
			
			if (detalleCalculo == null || detalleCalculo.getId() == null) {
				
				continue;
				
			}
			
			idSolicitudCheque = null;
			
			if (!esDetalleValido(detalleCalculo)) {
				
				valido = false;
				
				continue;
				
			}
									
			try {
				
				solicitudCheque = obtenerSolicitudChequeCalculoBono(detalleCalculo);
				
				idSolicitudCheque = solicitudCheque.getIdSolCheque();
				
				logger.info(getMessage("midas.agente.bonos.autorizacion.cheque.proceso.detalle.inicio", idSolicitudCheque, idCalculo));
								
				mizarService.agregarSolicitudChequeMIZAR(solicitudCheque);
				
				mizarService.procesarSolicitudesChequeEnMIZAR(detalleCalculo.getId());
				
				logger.info(getMessage("midas.agente.bonos.autorizacion.cheque.proceso.detalle.fin", idSolicitudCheque, idCalculo));
				
				actualizarDetalle(detalleCalculo, EstatusCalculoBonos.AUTORIZADO);
								
			} catch (Exception e) {
				
				logger.error(getMessage("midas.agente.bonos.autorizacion.cheque.proceso.detalle.error", 
						idSolicitudCheque, detalleCalculo.getIdBeneficiario(), idCalculo, e.getMessage()));
								
				if (detalleCalculo.getStatus().getValor().equals(EstatusCalculoBonos.CANCELADO.getValue())) {
					
					conChequesCancelados = true;
					
				} else {
					
					conErrores = true;
					
					actualizarDetalle(detalleCalculo, EstatusCalculoBonos.ERROR_PROCESAR);
					
				}
				
			}
			
		}
				
		if (!valido || conErrores || conChequesCancelados) {
						
			if (!valido) {
				
				msg.append(msgInvalido);
				
			}
			
			if (conErrores) {
			
				if (msg.length() > 0) {
					
					msg.append(" / ");
				
				}
				
				msg.append(msgError);
				
			}
			
			if (conChequesCancelados) {
				
				if (msg.length() > 0) {
					
					msg.append(" / ");
				
				}
				
				msg.append(msgCancelados);
				
			}
			
			throw new RuntimeException(getMessage("midas.agente.bonos.autorizacion.cheque.proceso.error", msg.toString()));
			
		}
		
	}
	
	private boolean esDetalleValido(DetalleCalculoBono detalleCalculo) throws Exception {
		
		boolean esValido = true;
		HashMap<String,Object> properties;
		
		CalculoBonoEjecuciones ejecucion = entidadService
			.findByProperty(CalculoBonoEjecuciones.class, "idCalculoBono", detalleCalculo.getCalculoBono().getId()).get(0);
				
		Long idConfiguracion = ejecucion.getIdConfiguracion();
		
		Date fechaEjecucion = ejecucion.getFechaFinEjecucion();
		
		ConfigBonos config = configBonosDao.findById(idConfiguracion, fechaEjecucion);
				
		if (config.getPagoSinFactura().intValue() == 0) {
			
			properties = new HashMap<String,Object>();
			properties.put("idAgente", detalleCalculo.getIdBeneficiario());
			properties.put("estatus", 0L);
						
			List <EnvioFacturaAgenteEst> enviosAnteriores = entidadService.findByProperties(EnvioFacturaAgenteEst.class, properties);
			
			if (enviosAnteriores != null && !enviosAnteriores.isEmpty()) {
				
				esValido = false;
			
			} else {
				
				//Busca en los honorarios facturas pendientes por entregar
				
				List<HonorariosAgente> honorariosAgente = calculoBonosDao.obtenerHonorariosFacturasPendientes (
						detalleCalculo.getIdBeneficiario(),	getPeriodo(obtenerFechaMesAnterior(fechaEjecucion))); 
				
				if (honorariosAgente != null && !honorariosAgente.isEmpty()) {
					
					esValido = false;
				
				}
				
			}
			
		}
				
		return esValido;
		
	}
	
	private boolean esAutorizacionValida(Long idCalculo) {
		/*
		   Si se encuentra otro preview de bonos con estatus 'EN PROCESO DE AUTORIZACIÓN', 'AUTORIZADO' o 'APLICADO' que proceda 
		   de la misma configuración y comprenda el mismo periodo de producción que el preview en cuestión, entonces
		   la autorización del preview no es valida.
		*/
		if (calculoBonosDao.obtenerNumeroCalculosBonoPrevios(idCalculo).intValue() > 0) {
		
			return false;
		
		}
		
		return true;
		
	}
	
	private Date obtenerFechaMesAnterior (Date fechaOriginal) {
		
		Calendar calendar = Calendar.getInstance();  
        calendar.setTime(fechaOriginal);  

        calendar.add(Calendar.MONTH, -1);  
        
		return DateUtils.truncate(calendar.getTime(), Calendar.DATE);
		
	}
	
	private Integer getPeriodo (Date fechaPeriodo) {
        
        return Integer.parseInt(new SimpleDateFormat("yyyyMM").format(fechaPeriodo));
 		
	}
			
	private SolicitudChequeDTO obtenerSolicitudChequeCalculoBono(DetalleCalculoBono detalleCalculo) {
		
		if (detalleCalculo.getIdSolicitudCheque() == null) {
			
			String mensajeError;
			
			if (detalleCalculo.getStatus().getValor().equals(EstatusCalculoBonos.CANCELADO.getValue())) {
				
				mensajeError = getMessage("midas.agente.bonos.autorizacion.cheque.proceso.cancelado"); 
				
			} else {
				
				mensajeError = getMessage("midas.agente.bonos.autorizacion.cheque.proceso.error", "SEYCOS");
				
			}
						
			throw new RuntimeException(mensajeError);
			
		}
		
		SolicitudChequeDTO solicitudCheque = solicitudChequeFacade.obtenerSolicitudCheque(detalleCalculo.getIdSolicitudCheque());	
		
		solicitudCheque.setIdSesion(new BigDecimal(detalleCalculo.getId()));
		solicitudCheque.setTipoPago("AGT");
		solicitudCheque.setIdAgente(detalleCalculo.getIdBeneficiario());
		solicitudCheque.setAuxiliar(detalleCalculo.getIdBeneficiario());
    
	    return solicitudCheque;   
		
	}
	
	private void bloquearProcesoCalculoBonos(CalculoBono calculo) {
		
		if (calculo.isEnProceso()) {
			
			throw new RuntimeException(getMessage("midas.agente.comisiones.calculo.enproceso"));
			
		}
		
		calculo.setEnProceso(true);
		
		calculoBonosDao.saveCalculoBono(calculo);
		
	}
	
	private void desbloquearProcesoCalculoBonos(CalculoBono calculo) {
					
		calculo.setEnProceso(false);
		
		calculoBonosDao.saveCalculoBono(calculo);
		
	}
	
	private void actualizarEstatus(CalculoBono calculo, EstatusCalculoBonos estatus) throws Exception {
		
		ValorCatalogoAgentes claveEstatus = calculoBonosDao.obtenerClaveEstatusCalculo(estatus);
		
		calculo.setStatus(claveEstatus);
		
		calculoBonosDao.saveCalculoBono(calculo);
		
		
	}
	
	private void actualizarDetalle(DetalleCalculoBono detalleCalculo, EstatusCalculoBonos estatus) throws Exception {
			
		ValorCatalogoAgentes claveEstatus = calculoBonosDao.obtenerClaveEstatusCalculo(estatus);
		
		detalleCalculo.setStatus(claveEstatus);
		
		calculoBonosDao.saveDetalleCalculoBonos(detalleCalculo);
	
	}
	
	private String getMessage(String key, Object... params) {
		
		return Utilerias.getMensajeRecurso(
				sistemaContext.getArchivoRecursosBack(), key, params);
		
	}
	
	public void initialize() {
			String timerInfo = "TimerAutorizacionAutomaticaBonos";
			cancelarTemporizador(timerInfo);
			iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				expression.minute(0);
				expression.hour("12");
				expression.dayOfWeek("Tue, Fri");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerAutorizacionAutomaticaBonos", false));
				
				logger.info("Timer TimerAutorizacionAutomaticaBonos configurado");
			} catch (Exception e) {
				logger.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		logger.info("Cancelar Timer TimerAutorizacionAutomaticaBonos");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			logger.error("Error al detener Timer TimerAutorizacionAutomaticaBonos:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		autorizacionAutomaticaBonos();
	}
		
	@EJB
	private CalculoBonosDao calculoBonosDao;
	
	@EJB
	private ConfigBonosDao configBonosDao;
	
	@EJB
	private AgenteMidasService agenteMidasService;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;	
	
	@EJB
	private ImpresionesService impresionesService;
	
	@EJB
	private SistemaContext sistemaContext;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private SolicitudChequeFacadeRemote solicitudChequeFacade;
	
	@EJB
	private SolicitudChequesMizarService mizarService;
	
	private static Logger logger = Logger.getLogger(CalculoBonosServiceImpl.class);
	
	@Resource	
	private TimerService timerService;
}
