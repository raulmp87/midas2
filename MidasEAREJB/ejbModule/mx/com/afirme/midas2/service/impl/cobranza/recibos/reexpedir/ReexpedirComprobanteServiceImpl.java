package mx.com.afirme.midas2.service.impl.cobranza.recibos.reexpedir;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.ws.cliente.PrintReportClient;
import mx.com.afirme.midas2.domain.cobranza.recibos.reexpedir.FiltroReciboReexpedicionDTO;
import mx.com.afirme.midas2.domain.cobranza.recibos.reexpedir.FormaPagoSAT;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.service.cobranza.recibos.reexpedir.ReexpedirComprobanteService;
import mx.com.afirme.midas2.service.poliza.PolizaService;

@Stateless
public class ReexpedirComprobanteServiceImpl implements ReexpedirComprobanteService{
	private static Logger log = Logger.getLogger(ReexpedirComprobanteServiceImpl.class);
	
	@EJB
	private PolizaFacadeRemote polizaFacadeRemote;
	
	@EJB
	protected PolizaService polizaService;
	
	@PersistenceContext
	private EntityManager entityManager;
	
	@EJB
	private PrintReportClient printReport;
	
	@Override
	public List<FiltroReciboReexpedicionDTO> listarRecibosFiltrado(FiltroReciboReexpedicionDTO filtro)  throws SystemException{
		List<FiltroReciboReexpedicionDTO> list = new ArrayList<FiltroReciboReexpedicionDTO>(1);
		
		try {
			String queryString = "select * from ( "
					+ "select rownum r, t.* from "
					+ "( "
					+ "select DISTINCT "
					+ "pr.cve_t_recibo, pr.NUM_FOLIO_FISCAL, pr.f_cubre_desde, pr.f_cubre_hasta, pr.sit_recibo, "
					+ "pr.imp_prima_total, pr.ID_RECIBO, pr.num_folio_rbo, pr.num_exhibicion, pr.serie_folio_fiscal, "
					+ "pr.serie_folio_fiscal || ' ' || pr.num_folio_fiscal as llaveFiscal, "
					+ "pp.id_agente, "
					+ "p.nombre as nom_agente, "
					+ "pp.id_contratante, pp.nom_solicitante, "
					+ "case when length(pp.num_poliza) <= 10 then " 
					+ "	lpad(pp.id_centro_emis,3,0)||'-'||lpad (pp.num_poliza, 10, '0')||'-'||lpad(pp.num_renov_pol,2,0) "
					+ "else "
					+ "	lpad(pp.id_centro_emis,3,0)||'-'||lpad (pp.num_poliza, 12, '0')||'-'||lpad(pp.num_renov_pol,2,0) "
					+ "end as numero_poliza "
					+ "from seycos.pol_poliza pp "
					+ "left join seycos.pol_recibo pr on pp.id_cotizacion = pr.id_cotizacion ";
			if (filtro.getNumeroInciso() != null) {
				queryString+= "left join seycos.pol_desg_recibo pdr on pr.id_recibo = pdr.id_recibo ";
			}
					queryString+= "left join midas.toAgente a on pp.id_agente = a.idagente "
					+ "left join seycos.persona p on a.idPersona = p.id_persona "
					+ "where pr.f_ter_reg = to_date('31/12/4712', 'dd/MM/yyyy') ";
					
			if (filtro.getNumeroPoliza() != null && !"".equals(filtro.getNumeroPoliza())) {
				//validar la longitud del numeroPoliza para ver como proceder
				String strNumeroPolizaCompleto = StringUtils.trim(filtro.getNumeroPoliza()).replace(NumeroPolizaCompleto.SEPARADOR, "");
				if (!strNumeroPolizaCompleto.matches("\\d+")) {
					throw new IllegalArgumentException("Contiene caracteres inv\u00E1lidos.");			
				}
				
				String numeroPoliza;
				int numeroRenovacion;
				int numeroEndoso = 0;
				
				if (strNumeroPolizaCompleto.length() == NumeroPolizaCompleto.OLD_TOTAL_DIGITOS_FORMATO_MIDAS) {
					numeroPoliza = strNumeroPolizaCompleto.substring(4,10);
					numeroRenovacion = Integer.parseInt(strNumeroPolizaCompleto.substring(10,12));
				} else if (strNumeroPolizaCompleto.length() == NumeroPolizaCompleto.TOTAL_DIGITOS_FORMATO_MIDAS) {
					numeroPoliza = strNumeroPolizaCompleto.substring(4,12);
					numeroRenovacion = Integer.parseInt(strNumeroPolizaCompleto.substring(12,14));
				} else if (strNumeroPolizaCompleto.length() == NumeroPolizaCompleto.TOTAL_DIGITOS_FORMATO_SEYCOS_UNO) {
					numeroPoliza = strNumeroPolizaCompleto.substring(3,13);
					numeroRenovacion = Integer.parseInt(strNumeroPolizaCompleto.substring(13,15));
				} else if (strNumeroPolizaCompleto.length() == NumeroPolizaCompleto.TOTAL_DIGITOS_FORMATO_SEYCOS_DOS) {
					numeroPoliza = strNumeroPolizaCompleto.substring(3,15);
					numeroRenovacion = Integer.parseInt(strNumeroPolizaCompleto.substring(15,17));
				} else {
					throw new SystemException("Formato incorrecto de la p\u00F3liza.");
				}
				
				if (filtro.getNumeroEndoso() != null && filtro.getNumeroEndoso().compareTo(BigDecimal.ZERO) > 0) {
					numeroEndoso = filtro.getNumeroEndoso().intValue();
				}
				
				log.info("--> numeroPoliza:"+numeroPoliza+" numeroRenovacion="+numeroRenovacion+" numeroEndoso="+numeroEndoso);
				
				filtro.setNumeroCotizacion(polizaFacadeRemote.obtenerNumeroCotizacionSeycos(numeroPoliza, numeroRenovacion, numeroEndoso));
				
				queryString += "and pr.id_cotizacion = ? and pp.id_docto_vers = ? ";
			}
			
			if (filtro.getIdAgente() != null) {
				queryString += "and pp.id_agente = ? ";
			}
			
			if (filtro.getNumeroCliente() != null) {
				queryString += "and pp.id_contratante = ? ";
			}
			
			if (filtro.getNumeroRecibo() != null) {
				queryString += "and pr.num_folio_rbo = ? ";
			}
			
			if (filtro.getNumeroInciso() != null) {
				queryString += "and pdr.id_inciso = ? ";
			}
				
			queryString += " and pr.sit_recibo <> 'PEN' "
					+ " order by pr.num_folio_rbo ) t "
					+ " ) ";
			
			if ((filtro.getRowsStart() != null) && (filtro.getRowsCount() != null && filtro.getRowsCount() > 0)) {
				queryString += "where r between ? and ? ";
			
			}
				
			Query query = entityManager.createNativeQuery(queryString);			
			
			int pos  = 1;
			
			if (filtro.getNumeroPoliza() != null && !"".equals(filtro.getNumeroPoliza())) {
				query.setParameter(pos, filtro.getNumeroCotizacion());
				pos++;
				query.setParameter(pos, filtro.getNumeroCotizacion());
				pos++;
			}
			
			if (filtro.getIdAgente() != null) {
				query.setParameter(pos, filtro.getIdAgente());
				pos++;
			}
			
			if (filtro.getNumeroCliente() != null) {
				query.setParameter(pos, filtro.getNumeroCliente());
				pos++;
			}
			
			if (filtro.getNumeroRecibo() != null) {
				query.setParameter(pos, filtro.getNumeroRecibo());
				pos++;
			}
			
			if (filtro.getNumeroInciso() != null) {
				query.setParameter(pos, filtro.getNumeroInciso());
				pos++;
			}
				
			if ((filtro.getRowsStart() != null) && (filtro.getRowsCount() != null && filtro.getRowsCount() > 0)) {
				query.setParameter(pos, (filtro.getRowsStart() + 1));
				pos++;
				query.setParameter(pos, (filtro.getRowsStart() + filtro.getRowsCount()));
				pos++;
			}
			
			List<Object[]> results = query.getResultList();
			
			if (results != null) {
				Iterator<Object[]> itResult = results.iterator();
				while (itResult.hasNext()) {
					Object[] resultado = itResult.next();
					
					FiltroReciboReexpedicionDTO dto = new FiltroReciboReexpedicionDTO();
					
					dto.setNumeroPoliza((filtro.getNumeroPoliza() != null && !"".equals(filtro.getNumeroPoliza()))? filtro.getNumeroPoliza(): "" + resultado[16]);
					dto.setNumeroEndoso((BigDecimal) ((filtro.getNumeroEndoso()!=null)? filtro.getNumeroEndoso(): BigDecimal.ZERO));
					dto.setClave((String) resultado[1]);
					dto.setNumFolioFiscal((BigDecimal) resultado[2]);
					dto.setSituacion((String) resultado[5]);
					dto.setPrimaTotal((BigDecimal) resultado[6]);
					dto.setNumeroRecibo((BigDecimal) resultado[8]);
					dto.setNumeroExhibicion((BigDecimal) resultado[9]);
					dto.setSerieFolioFiscal((String) resultado[10]);
					dto.setLlaveFiscal((String) resultado[11]);
					dto.setIdAgente((BigDecimal) resultado[12]);
					dto.setNombreAgente((String) resultado[13]);
					dto.setNumeroCliente((BigDecimal) resultado[14]);
					dto.setNombreCliente((String) resultado[15]);
					
					list.add(dto);
				}
			}
		} catch (Exception e) {
			log.error("Ocurri\u00F3 un error al obtener los recibos\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener los recibos. "+e.getMessage());
		}
		
		return list;
	}

	@Override
	public void reexpedirComprobantes(String idMetodoPago, BigDecimal numeroCuenta, String listaRecibos, FiltroReciboReexpedicionDTO filtro)  throws SystemException{
		String spName = "MIDAS.PKG_REGENERAR_RECIBOS.regenerarComprobante";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("listaRecibos", listaRecibos);
			storedHelper.estableceParametro("numeroPoliza", filtro.getNumeroPoliza());
			storedHelper.estableceParametro("idAgente", filtro.getIdAgente());
			storedHelper.estableceParametro("idCliente", filtro.getNumeroCliente());
			storedHelper.estableceParametro("numeroRecibo", filtro.getNumeroRecibo());
			storedHelper.estableceParametro("idMetodoPago", idMetodoPago);
			storedHelper.estableceParametro("numeroCuenta", numeroCuenta);
					
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new SystemException("Error en :" + spName, e);
		}	
	}
	
	@Override
	public byte[] descargarComprobante(String llaveFiscal, String tipoDocumento) throws SystemException {
		byte[] reciboFiscal   = null;
		
		try {
			reciboFiscal = printReport.getDigitalBill(llaveFiscal, tipoDocumento);
		} catch (Exception e) {
			log.error("No se pudo descargar el comprobante.. "+e.getMessage()+" ::: "+ e);
			throw new SystemException("No se pudo descargar el "+tipoDocumento+" ...");
		}
		
		return reciboFiscal;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<FormaPagoSAT> getFormasPagoSAT() throws SystemException {
		List<FormaPagoSAT> list = new ArrayList<FormaPagoSAT>(1);
		
		String queryString = "select * from MIDAS.FORMAS_PAGO_SAT";
		
		try {
			Query query = entityManager.createNativeQuery(queryString);			
			
			List<Object[]> results = query.getResultList();
			
			if (results != null) {
				Iterator<Object[]> itResult = results.iterator();
				while (itResult.hasNext()) {
					Object[] resultado = itResult.next();
					
					FormaPagoSAT dto = new FormaPagoSAT();
					
					dto.setId((BigDecimal) resultado[0]);
					dto.setClave((String) resultado[1]);
					dto.setConcepto((String) (resultado[1] + " - " + resultado[2]));
					
					list.add(dto);
				}
			}
		} catch (Exception e) {
			log.error("Ocurri\u00F3 un error al obtener las formas de pago SAT...\n"+e);
			throw new SystemException("Ocurri\u00F3 un error al obtener las formas de pago SAT");
		} 
		
		return list;
	}
}
