package mx.com.afirme.midas2.service.impl.fronterizos;

import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.domain.fronterizos.BitacoraCESVI;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fronterizos.BitacoraCESVIService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;

import org.apache.log4j.Logger;

@Stateless
public class BitacoraCESVIServiceImpl implements BitacoraCESVIService {
	
	private static final Logger LOG = Logger.getLogger(BitacoraCESVIServiceImpl.class);
	
	@EJB
	protected EntidadService entidadService;
	@PersistenceContext
	private EntityManager entityManager;
	private UsuarioService usuarioService;

	@EJB(beanName = "UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@Override
	public void guardarBitacora(BitacoraCESVI bitacoraCESVI) {
		bitacoraCESVI.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		bitacoraCESVI.setFechaCreacion(new Date());
		this.save(bitacoraCESVI);
	}

	private void save(BitacoraCESVI bitacoraCESVI) {
		entidadService.save(bitacoraCESVI);
	}
	
	@Override
	public Double obtenerValorComercial (BitacoraCESVI bitacoraCESVI) {
		Double valorComercial = null;
		
		try {
			final StringBuilder sb = new StringBuilder();
			sb.append("select model from BitacoraCESVI model ");
			sb.append("where model.idToCotizacion = :idToCotizacion ");
			sb.append("and model.numeroInciso = :numeroInciso ");
			sb.append("and trim(model.numeroSerie) = :numeroSerie ");
			sb.append("order by model.fechaCreacion desc ");

			Query query = entityManager.createQuery(sb.toString(), BitacoraCESVI.class);
			query.setParameter("idToCotizacion", bitacoraCESVI.getIdToCotizacion());
			query.setParameter("numeroInciso", bitacoraCESVI.getNumeroInciso());
			query.setParameter("numeroSerie", bitacoraCESVI.getNumeroSerie().trim());
			query.setMaxResults(1);
			@SuppressWarnings("unchecked")
			List<BitacoraCESVI> resultado = query.getResultList();
			
			if (resultado != null && resultado.size() > 0) {
				valorComercial = resultado.get(0).getValorComercial().doubleValue();
				LOG.info("BitacoraCESVIServiceImpl.obtieneValorComercial() --> El valor comercial del vehiculo es $ " + valorComercial);
			}
		} catch (Exception e) {
			LOG.error("BitacoraCESVIServiceImpl.obtieneValorComercial() --> Ocurrio un error al consultar el valor comercial del vehiculo", e);
		}
		
		return valorComercial;
	}
}
