package mx.com.afirme.midas2.service.impl.negocio.tarifa;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.dao.tarifa.AgrupadorTarifaDao;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaId;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.dto.negocio.tarifa.RelacionesNegocioTarifaDTO;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaSeccionService;


@Stateless
public class NegocioTarifaServiceImpl implements NegocioTarifaService {

	protected NegocioSeccionDao negocioSeccionDao;
	protected NegocioProductoDao negocioProductoDao;
	protected EntidadDao entidadDao;
	protected AgrupadorTarifaDao agrupadorTarifaDao;
	protected MonedaFacadeRemote monedaFacadeRemote;
	protected UsuarioService usuarioService;
	
	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }	
	
	@EJB
	private AgrupadorTarifaSeccionService agrupadorTarifaSeccionService;
	
	@EJB	
	public void setNegocioSeccionDao(NegocioSeccionDao negocioSeccionDao) {
		this.negocioSeccionDao = negocioSeccionDao;
	}
	@EJB	
	public void setNegocioProductoDao(NegocioProductoDao negocioProductoDao) {
		this.negocioProductoDao = negocioProductoDao;
	}	
	@EJB	
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	@EJB	
	public void setAgrupadorTarifaDao(AgrupadorTarifaDao agrupadorTarifaDao) {
		this.agrupadorTarifaDao = agrupadorTarifaDao;
	}
	@EJB
	public void setMonedaFacadeRemote(MonedaFacadeRemote monedaFacadeRemote) {
		this.monedaFacadeRemote = monedaFacadeRemote;
	}
	
	@Override
	public List<RelacionesNegocioTarifaDTO> getNegSeccionList(NegocioTipoPoliza negocioTipoPoliza) {
		List<RelacionesNegocioTarifaDTO> relacionesNegocioTarifaDTOList = new ArrayList<RelacionesNegocioTarifaDTO>(1);
		
		List<NegocioSeccion> negocioSeccionAsociadas = new ArrayList<NegocioSeccion>(1);
		//Obtiene todos los negocioSeccion asociados a negTipoPoliza
		if(negocioSeccionDao.listarNegSeccionPorIdNegTipoPoliza(negocioTipoPoliza.getIdToNegTipoPoliza()).size() > 0){
			negocioSeccionAsociadas = negocioSeccionDao.listarNegSeccionPorIdNegTipoPoliza(negocioTipoPoliza.getIdToNegTipoPoliza());
			
			for(NegocioSeccion item :  negocioSeccionAsociadas){												
				
				//Obtiene las monedas asociadas a la poliza
				//List<MonedaTipoPolizaDTO> monedas = item.getNegocioTipoPoliza().getTipoPolizaDTO().getMonedas();
				List<MonedaDTO> monedas = monedaFacadeRemote.listarMonedasAsociadasTipoPoliza(item.getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza());
				List<AgrupadorTarifaSeccion> agrupadorTarifaSeccionList = new ArrayList<AgrupadorTarifaSeccion>(1);
				for(MonedaDTO moneda : monedas){
					//Agrega un elemento negocioSeccion por cada moneda asociada
					RelacionesNegocioTarifaDTO relacionesNegocioTarifaDTO = new RelacionesNegocioTarifaDTO();
					relacionesNegocioTarifaDTO.setNegocioSeccion(item);
					//Obtiene moneda
					relacionesNegocioTarifaDTO.setMonedaDTO(moneda);
					//Agrega el agrupadorTarifaSeccion asignado al negocioSeccion
					if(item.getNegocioAgrupadorTarifaSeccions() != null && !item.getNegocioAgrupadorTarifaSeccions().isEmpty()){
					for(NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion : item.getNegocioAgrupadorTarifaSeccions()){
						if(negocioAgrupadorTarifaSeccion.getIdMoneda().equals(new BigDecimal(moneda.getIdTcMoneda()))){
							relacionesNegocioTarifaDTO.setNegocioAgrupadorTarifaSeccion(negocioAgrupadorTarifaSeccion);
						}
							agrupadorTarifaSeccionList = agrupadorTarifaSeccionService
									.consultaAgrupadoresPosiblesConVersiones(
											item, new BigDecimal(moneda.getIdTcMoneda()),"A");
							relacionesNegocioTarifaDTO.setPosibles(agrupadorTarifaSeccionList);
					}
					}else{
						agrupadorTarifaSeccionList = agrupadorTarifaSeccionService
						.consultaAgrupadoresPosiblesConVersiones(
								item, new BigDecimal(moneda.getIdTcMoneda()), "A");
						relacionesNegocioTarifaDTO.setPosibles(agrupadorTarifaSeccionList);						
					}
					if(relacionesNegocioTarifaDTO.getPosibles() == null){
						relacionesNegocioTarifaDTO.setPosibles(new ArrayList<AgrupadorTarifaSeccion>(1));
					}
					relacionesNegocioTarifaDTOList.add(relacionesNegocioTarifaDTO);
				}
				
				
			}
		}
		
		return relacionesNegocioTarifaDTOList;
	}
	@Override
	public List<NegocioTipoPoliza> getNegTipoPolizaList(NegocioProducto negocioProducto) {
		//Llena combo de TipoPoliza
		List<NegocioTipoPoliza> negTipoPolizaList = new ArrayList<NegocioTipoPoliza>(1);
		
		if(negocioProductoDao.listarNegTipoPolizaPorIdToNegProducto(negocioProducto.getIdToNegProducto()).size() > 0)
			negTipoPolizaList = negocioProductoDao.listarNegTipoPolizaPorIdToNegProducto(negocioProducto.getIdToNegProducto());
		
		return negTipoPolizaList;
	}	
	/**
	 * Actualiza todos los NegocioAgrupadorTarifaSeccion
	 * que tiene un producto 
	 * @param idToNegSeccion
	 * @param idTcMoneda
	 * @param idToAgrupadorTarifa String con todos los id 
	 * @param idVerAgrupadorTarifa
	 * @autor martin
	 */
	@Override
	public void relacionarNegocioTarifa(List<String> idToNegSeccion,
			List<String> idTcMoneda,List<String> idToAgrupadorTarifa, List<String> idVerAgrupadorTarifa) {

		int x = 0;
		for (String negSeccion : idToNegSeccion) {
			relacionarNegocioTarifa(new BigDecimal(negSeccion),
					new BigDecimal(idTcMoneda.get(x)), new BigDecimal(
							idToAgrupadorTarifa.get(x)), new BigDecimal(idVerAgrupadorTarifa.get(x)));
			x++;
		}
	}
	
	@Override
	public void relacionarNegocioTarifa(BigDecimal idToNegSeccion, BigDecimal idTcMoneda, BigDecimal idToAgrupadorTarifa,BigDecimal idVerAgrupadorTarifa) {
		NegocioSeccion negocioSeccion = negocioSeccionDao.findById(idToNegSeccion);
		
		//Remueve agrupadortarifaanterior
		List<NegocioAgrupadorTarifaSeccion> negocioAgrupadorTarifaSeccionList = entidadDao.findByProperty(NegocioAgrupadorTarifaSeccion.class, "negocioSeccion.idToNegSeccion", idToNegSeccion);
		for(NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion : negocioAgrupadorTarifaSeccionList){
			if(negocioAgrupadorTarifaSeccion.getIdMoneda().equals(idTcMoneda)){
				entidadDao.remove(negocioAgrupadorTarifaSeccion);
			}
		}		
		//Persite agrupadortarifaseccion
		NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion = new NegocioAgrupadorTarifaSeccion();
		negocioAgrupadorTarifaSeccion.setIdMoneda(idTcMoneda);
		negocioAgrupadorTarifaSeccion.setIdToAgrupadorTarifa(idToAgrupadorTarifa);		 
		negocioAgrupadorTarifaSeccion.setNegocioSeccion(negocioSeccion);
		negocioAgrupadorTarifaSeccion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		AgrupadorTarifa agrupadorTarifa = null;
		if (idVerAgrupadorTarifa != null && !idVerAgrupadorTarifa.equals(idToAgrupadorTarifa)) {
			AgrupadorTarifaId id = new AgrupadorTarifaId();
			id.setIdToAgrupadorTarifa(idToAgrupadorTarifa);
			id.setIdVerAgrupadorTarifa(idVerAgrupadorTarifa);
			agrupadorTarifa = agrupadorTarifaDao.findById(AgrupadorTarifa.class, id);
		} else{
			agrupadorTarifa = agrupadorTarifaDao.consultaVersionActiva(idToAgrupadorTarifa);
		}
		if(agrupadorTarifa != null && agrupadorTarifa.getId() != null && agrupadorTarifa.getId().getIdVerAgrupadorTarifa() != null){
			negocioAgrupadorTarifaSeccion.setIdVerAgrupadorTarifa(agrupadorTarifa.getId().getIdVerAgrupadorTarifa());
			try {
				entidadDao.persist(negocioAgrupadorTarifaSeccion);
			} catch(RuntimeException exception) {
				exception.printStackTrace();
			}
		}
		
	}
	@Override
	public NegocioAgrupadorTarifaSeccion getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(
			BigDecimal idToNegSeccion, BigDecimal idMoneda) {
		 
		String query = "SELECT model FROM NegocioAgrupadorTarifaSeccion model WHERE "+
							"model.negocioSeccion.idToNegSeccion = :idToNegSeccion "+
							"AND model.idMoneda = :idMoneda";
		
		Map<String, Object>  parameters = new LinkedHashMap<String, Object>();
		parameters.put("idToNegSeccion", idToNegSeccion);
		parameters.put("idMoneda", idMoneda);
		
		@SuppressWarnings("unchecked")
		List<NegocioAgrupadorTarifaSeccion> negocioAgrupadorTarifaSeccionList = entidadDao.executeQueryMultipleResult(query, parameters);
		
		NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion = new NegocioAgrupadorTarifaSeccion();
		if(negocioAgrupadorTarifaSeccionList != null && negocioAgrupadorTarifaSeccionList.size() > 0){
			negocioAgrupadorTarifaSeccion = negocioAgrupadorTarifaSeccionList.get(0);
		}
		return negocioAgrupadorTarifaSeccion;
	}

}
