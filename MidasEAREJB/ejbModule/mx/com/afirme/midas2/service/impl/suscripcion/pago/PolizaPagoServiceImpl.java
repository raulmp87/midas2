package mx.com.afirme.midas2.service.impl.suscripcion.pago;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;


import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;


import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.interfaz.tipocambio.TipoCambioFacadeRemote;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.suscripcion.pago.PolizaPagoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.persona.Persona;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.pago.ConfiguracionPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoCobro;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoCuenta;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoTarjeta;
import mx.com.afirme.midas2.dto.pago.FolioReciboDTO;
import mx.com.afirme.midas2.dto.pago.ReciboDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaAmexDTO;
import mx.com.afirme.midas2.dto.pago.RespuestaIbsDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.programapago.RecuotificacionService;
import mx.com.afirme.midas2.service.emision.ejecutivoColoca.EjecutivoColocaService;
import mx.com.afirme.midas2.service.pago.AmexService;
import mx.com.afirme.midas2.service.pago.IbsService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;


import org.apache.log4j.Logger;
import org.joda.time.DateTime;


import com.afirme.eibs.services.EibsUserService.UserEibsVO;
import com.js.util.StringUtil;


@Stateless
public class PolizaPagoServiceImpl implements PolizaPagoService {
	
	private static final Logger LOG = Logger.getLogger(PolizaPagoServiceImpl.class);
	
	private IbsService ibsService;

	private AmexService amexService;
	
	private EntidadService entidadService;
	
	private ClienteFacadeRemote clienteService;
	
	private PolizaPagoDao polizaPagoDao;
	
	private UsuarioService usuarioService;
	
	private DomicilioFacadeRemote domicilioService;
	
	private CotizacionService cotizacionService;
	
	private EntidadBitemporalService entidadBitemporalService;
	
	private TipoCambioFacadeRemote tipoCambioService;
	
	private CalculoService calculoService;
	
	private IncisoService incisoService;
		
	private String fechaVencimiento;
	private String codigoSeguridad;
	
	private EjecutivoColocaService ejecutivoColocaService;
	
	private RecuotificacionService recuotificacionService;
		

	@EJB 	
	public void setRecuotificacionService(RecuotificacionService recuotificacionService) {
		this.recuotificacionService = recuotificacionService;
	}

	@EJB 
	public void setEjecutivoColocaService(
			EjecutivoColocaService ejecutivoColocaService) {
		this.ejecutivoColocaService = ejecutivoColocaService;
	}

	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	@EJB
	public void setTipoCambioService(TipoCambioFacadeRemote tipoCambioService) {
		this.tipoCambioService = tipoCambioService;
	}

	@EJB
	public void setCotizacionService(CotizacionService cotizacionService) {
		this.cotizacionService = cotizacionService;
	}

	@EJB
	public void setDomicilioService(DomicilioFacadeRemote domicilioService) {
		this.domicilioService = domicilioService;
	}

	@EJB(beanName="UsuarioServiceDelegate")	
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}

	@EJB	
	public void setClienteService(ClienteFacadeRemote clienteService) {
		this.clienteService = clienteService;
	}

	@EJB
	public void setPolizaPagoDao(PolizaPagoDao polizaPagoDao) {
		this.polizaPagoDao = polizaPagoDao;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setIbsService(IbsService ibsService) {
		this.ibsService = ibsService;
	}	
	
	@EJB
	public void setAmexService(AmexService amexService) {
		this.amexService = amexService;
	}	
	
	@EJB
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@EJB
	public void setIncisoService(
			IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	

	@Override
	public Map<String, String> cargarListaEjecutivosPorUsuario(String usuario){
		Map<String, String> ejecutivos = new HashMap<String,String>();
		String nombreEjecutivo = "";
		try{
			//ejecutivos = polizaPagoDao.obtenerListaEjecutivosByUsuario(userActual);
			List<UserEibsVO> listaEjecutivos = ejecutivoColocaService.getEjecutivoColocaList(usuario);
			
			if(!listaEjecutivos.isEmpty()){
				for(int x=0; x<listaEjecutivos.size(); x++){
					nombreEjecutivo = listaEjecutivos.get(x).getUserFullName();
					ejecutivos.put(nombreEjecutivo, nombreEjecutivo); 
				}
			}
		}catch(Exception e){
			LOG.error("Ocurre un error al cargar la lista de ejecutivos con el usuario "+usuario+" Revisar: "+e.getMessage(), e);
		}
		
		return ejecutivos;
		
	}
	@Override
	public String validarUsuarioActualSiEsEjecutivoOrSucursal(){
		String usuario = PolizaPagoService.NO_TIENE_ROL_EJECUTIVO;
		List<Rol> roleUser = this.usuarioService.getUsuarioActual().getRoles();
		if(roleUser.size()>0){
			for(int x=0; x<roleUser.size();x++){
				if(roleUser.get(x).getDescripcion().equals(TIENE_ROL_EJECUTIVO_SUCURSAL)){
					usuario = this.usuarioService.getUsuarioActual().getNombreUsuario();
					break;	
				}
			}
		}
		
		return usuario;
	}
	
	@Override
	public RespuestaIbsDTO aplicarPago(BigDecimal idToCotizacion) throws Exception{
		RespuestaIbsDTO respuestaIbs = null;	
		CuentaPagoDTO cuentaPagoDTO = null;
		ConfiguracionPagoDTO pagoDTO = null;				
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);	
		if(cotizacion.getIdMedioPago().shortValue() == TipoCobro.CUENTA_AFIRME.valor() || 
				cotizacion.getIdMedioPago().shortValue() == TipoCobro.TARJETA_CREDITO.valor()){
			ReciboDTO recibo = obtenerInformacionRecibos(idToCotizacion).get(0);	
			
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.aplicarPago... idToCliente => " + cotizacion.getIdToPersonaContratante(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.aplicarPago... idToConductoCobro => " + cotizacion.getIdConductoCobroCliente(), Level.INFO, null);
			
			cuentaPagoDTO = obtenerConductoCobro(cotizacion.getIdToPersonaContratante(), cotizacion.getIdConductoCobroCliente());
			
			if (cuentaPagoDTO == null)
				throw new SystemException("No fue posible obtener el conducto cobro");				
			
			pagoDTO = new ConfiguracionPagoDTO();
			pagoDTO.setInformacionCuenta(cuentaPagoDTO);	
			pagoDTO.setReferenciaPago(obtenerReferenciaPago(idToCotizacion));
			respuestaIbs = aplicarPago(pagoDTO, recibo);
			//respuestaIbs = new RespuestaIbsDTO();
			if(respuestaIbs.esValido()){
				cambiarEstatusPrimerReciboPagado(idToCotizacion, respuestaIbs.getIbsNumAutorizacion(),recibo);
				//cambiarEstatusPrimerReciboPagado(idToCotizacion, "081292",recibo);
			}
		}else{
			respuestaIbs = new RespuestaIbsDTO();
		}
		return respuestaIbs;
	}
	
	@Override
	public RespuestaAmexDTO aplicarPagoAmex(BigDecimal idToCotizacion,String fechaVencimiento,String codigoSeguridad) throws Exception{
		RespuestaAmexDTO respuestaAmex = null;	
		CuentaPagoDTO cuentaPagoDTO = null;
		ConfiguracionPagoDTO pagoDTO = null;				
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);	
		if(cotizacion.getIdMedioPago().shortValue() == TipoCobro.AMERICAN_EXPRESS.valor()){
			ReciboDTO recibo = obtenerInformacionRecibos(idToCotizacion).get(0);	
			
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.aplicarPagoAmex... idToCliente => " + cotizacion.getIdToPersonaContratante(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.aplicarPagoAmex... idToConductoCobro => " + cotizacion.getIdConductoCobroCliente(), Level.INFO, null);
			
			cuentaPagoDTO = obtenerConductoCobro(cotizacion.getIdToPersonaContratante(), cotizacion.getIdConductoCobroCliente());
			
			if (cuentaPagoDTO == null)
				throw new SystemException("No fue posible obtener el conducto cobro");				
			cuentaPagoDTO.setCodigoSeguridad(codigoSeguridad);
			cuentaPagoDTO.setFechaVencimiento(fechaVencimiento);
			pagoDTO = new ConfiguracionPagoDTO();
			pagoDTO.setInformacionCuenta(cuentaPagoDTO);	
			pagoDTO.setReferenciaPago(obtenerReferenciaPago(idToCotizacion) + "|" + recibo.getFolioRecibo().getNumRecibo());
			respuestaAmex = aplicarPagoAmex(pagoDTO, recibo);
			//respuestaIbs = new RespuestaIbsDTO();
			if(respuestaAmex.esValido()){
				cambiarEstatusPrimerReciboPagado(idToCotizacion, respuestaAmex.getAmexNumAutorizacion(),recibo);
				//cambiarEstatusPrimerReciboPagado(idToCotizacion, "081292",recibo);
			}
		}
		return respuestaAmex;
	}
	
	@Override
	public RespuestaIbsDTO aplicarPagoInciso(BigDecimal idToCotizacion) throws Exception{
		RespuestaIbsDTO respuestaIbs = null;	
		CuentaPagoDTO cuentaPagoDTO = null;
		ConfiguracionPagoDTO pagoDTO = null;	
		boolean pagoRecibo = false;
		boolean usaTarjeta = false;
		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
		for(IncisoCotizacionDTO inciso : incisos){	
			if(inciso.getIdMedioPago() != null && (inciso.getIdMedioPago().shortValue() == TipoCobro.CUENTA_AFIRME.valor() || 
					inciso.getIdMedioPago().shortValue() == TipoCobro.TARJETA_CREDITO.valor() ||
					inciso.getIdMedioPago().shortValue() == TipoCobro.AMERICAN_EXPRESS.valor()
			)){
				usaTarjeta = true;
				ReciboDTO recibo = obtenerInformacionRecibosInciso(inciso);		
				cuentaPagoDTO = obtenerConductoCobro(inciso.getIdClienteCob(), inciso.getIdConductoCobroCliente());		
				cuentaPagoDTO.setCodigoSeguridad(codigoSeguridad);
				cuentaPagoDTO.setFechaVencimiento(fechaVencimiento);
				pagoDTO = new ConfiguracionPagoDTO();
				pagoDTO.setInformacionCuenta(cuentaPagoDTO);
				if (inciso.getIdMedioPago().shortValue() == TipoCobro.AMERICAN_EXPRESS.valor()) {
					pagoDTO.setReferenciaPago(obtenerReferenciaPago(idToCotizacion) + "|" + inciso.getNumeroSecuencia() + "|" + recibo.getFolioRecibo().getNumRecibo());
				}else{
					pagoDTO.setReferenciaPago(obtenerReferenciaPago(idToCotizacion));
				}
				respuestaIbs = aplicarPago(pagoDTO, recibo);
				//Quitar / Solo en pruebas			
				//respuestaIbs = new RespuestaIbsDTO();
				if(respuestaIbs.esValido()){
					cambiarEstatusPrimerReciboPagadoInciso(inciso, respuestaIbs.getIbsNumAutorizacion(), recibo);
					//cambiarEstatusPrimerReciboPagadoInciso(inciso, "081292", recibo);
					pagoRecibo = true;
				}
			}else{
				respuestaIbs = new RespuestaIbsDTO();
			}
		}
		if(pagoRecibo && usaTarjeta){
			respuestaIbs = new RespuestaIbsDTO();
		}
		return respuestaIbs;
	}


	@Override
	public RespuestaAmexDTO aplicarPagoIncisoAmex(BigDecimal idToCotizacion) throws Exception{
		RespuestaAmexDTO respuestaAmex = null;	
		CuentaPagoDTO cuentaPagoDTO = null;
		ConfiguracionPagoDTO pagoDTO = null;	
		boolean pagoRecibo = false;
		boolean usaTarjeta = false;
		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
		for(IncisoCotizacionDTO inciso : incisos){	
			if(inciso.getIdMedioPago() != null && (inciso.getIdMedioPago().shortValue() == TipoCobro.AMERICAN_EXPRESS.valor())){
				usaTarjeta = true;
				ReciboDTO recibo = obtenerInformacionRecibosInciso(inciso);		
				cuentaPagoDTO = obtenerConductoCobro(inciso.getIdClienteCob(), inciso.getIdConductoCobroCliente());		
				pagoDTO = new ConfiguracionPagoDTO();
				pagoDTO.setInformacionCuenta(cuentaPagoDTO);	
				pagoDTO.setReferenciaPago(obtenerReferenciaPago(idToCotizacion));
				respuestaAmex = aplicarPagoAmex(pagoDTO, recibo);
				//Quitar / Solo en pruebas			
				//respuestaIbs = new RespuestaIbsDTO();
				if(respuestaAmex.esValido()){
					cambiarEstatusPrimerReciboPagadoInciso(inciso, respuestaAmex.getAmexNumAutorizacion(), recibo);
					//cambiarEstatusPrimerReciboPagadoInciso(inciso, "081292", recibo);
					pagoRecibo = true;
				}
			}else{
				respuestaAmex = new RespuestaAmexDTO();
			}
		}
		if(pagoRecibo && usaTarjeta){
			respuestaAmex = new RespuestaAmexDTO();
		}
		return respuestaAmex;
	}
	
	private String obtenerReferenciaPago(BigDecimal idToCotizacion){
		String referencia = null;
		List<PolizaDTO> polizas = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", idToCotizacion);
		if(polizas != null && !polizas.isEmpty()){
			referencia = polizas.get(0).getNumeroPolizaFormateada();
		}
		return referencia;
	}
	
	@Override
	public RespuestaIbsDTO cambiarConductoCobro(BigDecimal idToCotizacion){
		//TODO
		return null;
	}
		
	@Override
	public RespuestaIbsDTO aplicarPago(ConfiguracionPagoDTO pagoDTO, ReciboDTO recibo) {			
		RespuestaIbsDTO respuestaIbs = new RespuestaIbsDTO();
		if(pagoDTO.getInformacionCuenta().getTipoConductoCobro().valor() == TipoCobro.CUENTA_AFIRME.valor()){												
			respuestaIbs = ibsService.aplicarCargoCuentaAfirme(pagoDTO, recibo);				
		}else if(pagoDTO.getInformacionCuenta().getTipoConductoCobro().valor() == TipoCobro.TARJETA_CREDITO.valor()){
			//TODO 
			//Revisar promociones y pago posterior						
			respuestaIbs = ibsService.consultarSaldoAplicarCargoTarjetaCredito(pagoDTO, recibo);
		}else if(pagoDTO.getInformacionCuenta().getTipoConductoCobro().valor() == TipoCobro.AMERICAN_EXPRESS.valor()){
			RespuestaAmexDTO respuestaAmexDTO = new RespuestaAmexDTO();
			respuestaAmexDTO = aplicarPagoAmex(pagoDTO, recibo);
			if (respuestaAmexDTO.esValido()) {
				respuestaIbs.setIbsNumAutorizacion(respuestaAmexDTO.getAmexNumAutorizacion());
			}else{
				respuestaIbs.setCodigoErrorIBS(respuestaAmexDTO.getCodigoErrorAmex());
				respuestaIbs.setDescripcionErrorIBS(respuestaAmexDTO.getDescripcionErrorAmex());
			}
		}
		return respuestaIbs;
	}


	@Override
	public RespuestaAmexDTO aplicarPagoAmex(ConfiguracionPagoDTO pagoDTO, ReciboDTO recibo) {			
		RespuestaAmexDTO respuestaAmex = new RespuestaAmexDTO();
		if(pagoDTO.getInformacionCuenta().getTipoConductoCobro().valor() == TipoCobro.AMERICAN_EXPRESS.valor()){												
			respuestaAmex = amexService.consultarSaldoAplicarCargoTarjetaCredito(pagoDTO, recibo);				
		}
		return respuestaAmex;
	}
	
	private void cambiarEstatusPrimerReciboPagado(BigDecimal idToCotizacion, String numeroAutorizacion, ReciboDTO recibo){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.setPrimerReciboPagado(Boolean.TRUE);
		cotizacion.setNumeroAutorizacion(numeroAutorizacion);
		cotizacion.setNumeroPrimerRecibo(recibo.getFolioRecibo().getNumRecibo());
		cotizacion.setDigitoVerificadorRecibo(recibo.getFolioRecibo().getDigitoVerificador());
		entidadService.save(cotizacion);
	}
	
	private void cambiarEstatusPrimerReciboPagadoInciso(IncisoCotizacionDTO inciso, String numeroAutorizacion, ReciboDTO recibo){
		inciso.setPrimerReciboPagado(Boolean.TRUE);
		inciso.setNumeroAutorizacion(numeroAutorizacion);
		inciso.setNumeroPrimerRecibo(recibo.getFolioRecibo().getNumRecibo());
		inciso.setDigitoVerificadorRecibo(recibo.getFolioRecibo().getDigitoVerificador());
		entidadService.save(inciso);
	}

	@Override
	public RespuestaIbsDTO consultarSaldo(BigDecimal idToCotizacion) throws Exception{
		CuentaPagoDTO cuentaPagoDTO = null;
		ConfiguracionPagoDTO pagoDTO = null;
		RespuestaIbsDTO respuestaIbs = new RespuestaIbsDTO();
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if(cotizacion.getIdMedioPago().shortValue() == TipoCobro.CUENTA_AFIRME.valor()){
			ReciboDTO recibo = obtenerInformacionRecibos(idToCotizacion).get(0);	
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.consultarSaldo... idToCliente => " + cotizacion.getIdToPersonaContratante(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.consultarSaldo... idConductoCobroCliente => " + cotizacion.getIdConductoCobroCliente(), Level.INFO, null);
			cuentaPagoDTO = obtenerConductoCobro(cotizacion.getIdToPersonaContratante(), cotizacion.getIdConductoCobroCliente());		
			if (cuentaPagoDTO == null)
				throw new Exception("El conducto de cobro es null");
			pagoDTO = new ConfiguracionPagoDTO();
			pagoDTO.setInformacionCuenta(cuentaPagoDTO);
			respuestaIbs = ibsService.consultarSaldoCuentaAfirme(pagoDTO, recibo);
		}
		return respuestaIbs;
	}	
	
	@Override
	public RespuestaIbsDTO consultarSaldoIncisos(BigDecimal idToCotizacion) throws Exception{
		CuentaPagoDTO cuentaPagoDTO = null;
		ConfiguracionPagoDTO pagoDTO = null;
		RespuestaIbsDTO respuestaIbs = new RespuestaIbsDTO();				
		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
		for(IncisoCotizacionDTO inciso : incisos){
			if(inciso.getIdMedioPago() != null && inciso.getIdMedioPago().shortValue() == TipoCobro.CUENTA_AFIRME.valor()){
				ReciboDTO recibo = obtenerInformacionRecibosInciso(inciso);
				LogDeMidasEJB3.log("PolizaPagoServiceImpl.consultarSaldoIncisos... idClienteCob => " + inciso.getIdClienteCob(), Level.INFO, null);
				LogDeMidasEJB3.log("PolizaPagoServiceImpl.consultarSaldoIncisos... idConductoCobroCliente => " + inciso.getIdConductoCobroCliente(), Level.INFO, null);
				cuentaPagoDTO = obtenerConductoCobro(inciso.getIdClienteCob(), inciso.getIdConductoCobroCliente());		
				pagoDTO = new ConfiguracionPagoDTO();
				pagoDTO.setInformacionCuenta(cuentaPagoDTO);
				respuestaIbs = ibsService.consultarSaldoCuentaAfirme(pagoDTO, recibo);
				if(respuestaIbs.esValido()){
					break;
				}
			}
		}
		return respuestaIbs;
	}	
	
	protected List<ReciboDTO> obtenerInformacionRecibos(BigDecimal idToCotizacion) throws Exception{
		boolean nivelCotizacion = true;
		List<ReciboDTO> recibos = new ArrayList<ReciboDTO>(1);
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		//revisar si el cobro sera a nivel inciso o poliza
		if(nivelCotizacion){			
			recibos.add(obtenerInformacionRecibo(idToCotizacion, null, null));
		}else{
			for(IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()){
				for(SeccionCotizacionDTO seccion : inciso.getSeccionCotizacionList()){
					ReciboDTO recibo = obtenerInformacionRecibo(idToCotizacion, 
							inciso.getId().getNumeroInciso(), 
							inciso.getSeccionCotizacion().getSeccionDTO().getIdToSeccion());
					recibos.add(recibo);
				}
			}
		}
		return recibos;
		
	}
	
	protected ReciboDTO obtenerInformacionRecibosInciso(IncisoCotizacionDTO inciso) throws Exception{
		ReciboDTO recibo = null;
		try{
			recibo = obtenerInformacionRecibo(inciso.getId().getIdToCotizacion(),
					inciso.getSeccionCotizacion().getSeccionDTO().getIdToSeccion(),
					inciso.getId().getNumeroInciso() 
					);
		}catch(Exception e){
			e.printStackTrace();
		}
		return recibo;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	protected ReciboDTO obtenerInformacionRecibo(BigDecimal idToCotizacion, BigDecimal idToSeccion, BigDecimal idToInciso) throws Exception{
		boolean nivelCotizacion = true;
		if(idToInciso != null){
			nivelCotizacion = false;
		}
		ReciboDTO recibo = new ReciboDTO();
		CotizacionDTO cotizacion =  entidadService.findById(CotizacionDTO.class, idToCotizacion);	
		if(nivelCotizacion){
			if(!StringUtil.isEmpty(cotizacion.getNumeroPrimerRecibo())){				
				recibo.setFolioRecibo(new FolioReciboDTO(cotizacion.getNumeroPrimerRecibo(), cotizacion.getDigitoVerificadorRecibo()));
			}else{			
				recibo.setFolioRecibo(polizaPagoDao.obtenerInformacionPrimerRecibo());
				cotizacion.setNumeroPrimerRecibo(recibo.getFolioRecibo().getNumRecibo());
				cotizacion.setDigitoVerificadorRecibo(recibo.getFolioRecibo().getDigitoVerificador());
				entidadService.save(cotizacion);
			}							
		}else{
			IncisoCotizacionId id = new IncisoCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(idToInciso);
			IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
			if(!StringUtil.isEmpty(inciso.getNumeroPrimerRecibo())){				
				recibo.setFolioRecibo(new FolioReciboDTO(inciso.getNumeroPrimerRecibo(), inciso.getDigitoVerificadorRecibo()));
			}else{			
				recibo.setFolioRecibo(polizaPagoDao.obtenerInformacionPrimerRecibo());
				inciso.setNumeroPrimerRecibo(recibo.getFolioRecibo().getNumRecibo());
				inciso.setDigitoVerificadorRecibo(recibo.getFolioRecibo().getDigitoVerificador());
				entidadService.save(inciso);
			}
		}
		recibo.setCantidad(obtenerMontoReciboInicial(idToCotizacion, idToSeccion, idToInciso));
		return recibo;
	}
	
	@Override
	public double obtenerMontoReciboInicial(BigDecimal idToCotizacion, BigDecimal idToSeccion, BigDecimal idToInciso){
		double cantidadPagoInicial = 0.0d;
		double tipoCambio = 1.0d;		
		CotizacionDTO cotizacion =  entidadService.findById(CotizacionDTO.class, idToCotizacion);
		ResumenCostosDTO resumen = null;
		BigDecimal valorDerechos = BigDecimal.ZERO; 
		
	
		if(idToInciso == null){
			resumen = calculoService.obtenerResumenCotizacion(cotizacion, true);
			valorDerechos = BigDecimal.valueOf(cotizacion.getValorDerechosUsuario() * cotizacion.getIncisoCotizacionDTOs().size());
		}else{
			IncisoCotizacionId id = new IncisoCotizacionId();
			id.setIdToCotizacion(idToCotizacion);
			id.setNumeroInciso(idToInciso);
			IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
			resumen = calculoService.obtenerResumen(inciso);
			valorDerechos = BigDecimal.valueOf(cotizacion.getValorDerechosUsuario());
		}
		EsquemaPagoCotizacionDTO esquema = cotizacionService.findEsquemaPagoCotizacion(cotizacion.getFechaInicioVigencia(), cotizacion.getFechaInicioVigencia(),
					cotizacion.getFechaFinVigencia(), cotizacion.getIdFormaPago().intValue(), BigDecimal.valueOf(resumen.getPrimaNetaCoberturas()), 
					valorDerechos, 
					BigDecimal.valueOf(cotizacion.getPorcentajeIva()), cotizacion.getIdMoneda().intValue(), 
					resumen.getPrimaNetaCoberturas().doubleValue() * (cotizacion.getPorcentajePagoFraccionado() / 100),
					false);
		cantidadPagoInicial = esquema.getPagoInicial().getImporte().doubleValue();
		if(cotizacion.getIdMoneda().intValue() != MONEDA_MXP){							
			try{
				tipoCambio = tipoCambioService.obtieneTipoCambioPorDia(new Date(), "MIDAS 2");
			}catch(Exception ex){
				LogDeMidasEJB3.log("Imposible obtener tipo de cambio", Level.SEVERE, ex);					
			}
		}
		
		if(recuotificacionService.validarAplicaRecuotificacion(idToCotizacion))
			return tipoCambio*recuotificacionService.obtenerMontoPrimerReciboRecuotificado(idToCotizacion);
		
		return tipoCambio * cantidadPagoInicial;
	}

	@Override
	public CuentaPagoDTO obtenerConductoCobro(BigDecimal idToCotizacion) throws Exception{
		CuentaPagoDTO cuenta = null;
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		BigDecimal idToCliente = cotizacion.getIdToPersonaContratante();
		BigDecimal idDomicilio = cotizacion.getIdDomicilioContratante();
		Long idConducto = cotizacion.getIdConductoCobroCliente();
		if(idToCliente != null && idConducto != null){
			cuenta = obtenerConductoCobro(idToCliente, idConducto); 
		}else if(idToCliente != null){
			cuenta = obtenerConductoCobro(idToCliente, idDomicilio);
		}		
		return cuenta;
	}

	@Override
	public List<CuentaPagoDTO> obtenerConductosCobro (
			BigDecimal idCliente, TipoCobro tipoCobro) throws Exception{
		List<CuentaPagoDTO> conductos = new ArrayList<CuentaPagoDTO>(1);			
		CuentaPagoDTO cuenta = null;
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		filtro.setIdCliente(idCliente);
		List<ClienteGenericoDTO> mediosPago = clienteService.loadMediosPagoPorCliente(filtro, 
				usuarioService.getUsuarioActual().getNombreUsuario());
		for(ClienteGenericoDTO cliente : mediosPago){
			if(cliente.getIdTipoConductoCobro().shortValue() == tipoCobro.valor()){				
				cuenta = convertirClienteCobranza(cliente);				
				conductos.add(cuenta);
			}
		}		
		return conductos;
	}
	
	private CuentaPagoDTO convertirClienteCobranza(ClienteGenericoDTO cliente){
		CuentaPagoDTO cuenta = null;
		Domicilio domicilio = new Domicilio();		
		Integer tipoConductoCobro = cliente.getIdTipoConductoCobro();
		if(cliente.getIdTipoConductoCobro()!=null){
			if(tipoConductoCobro.shortValue() == TipoCobro.TARJETA_CREDITO.valor()){
				cuenta = new CuentaPagoDTO(TipoCobro.TARJETA_CREDITO);
			}else if(tipoConductoCobro.shortValue() == TipoCobro.CUENTA_AFIRME.valor()){
				cuenta = new CuentaPagoDTO(TipoCobro.CUENTA_AFIRME);
			}else if(tipoConductoCobro.shortValue() == TipoCobro.DOMICILIACION.valor()){
				cuenta = new CuentaPagoDTO(TipoCobro.DOMICILIACION);
			}else if(tipoConductoCobro.shortValue() == TipoCobro.AMERICAN_EXPRESS.valor()){
				cuenta = new CuentaPagoDTO(TipoCobro.AMERICAN_EXPRESS);
			}	
		}
		cuenta.setIdCliente(cliente.getIdCliente());
		if( cliente.getIdBancoCobranza() != null ) {
			cuenta.setIdBanco(cliente.getIdBancoCobranza().longValue());
		} 
		
		cuenta.setTarjetaHabiente(cliente.getNombreTarjetaHabienteCobranza());
		cuenta.setIdConductoCliente(cliente.getIdConductoCobranza());
		cuenta.setCorreo(cliente.getEmailCobranza());
		if(cliente.getTelefonoCobranza() != null){
			String telStr = cliente.getTelefonoCobranza();
			cuenta.setTelefono(telStr.replaceAll("\\D+", ""));
		}
		cuenta.setCuenta(cliente.getNumeroTarjetaCobranza());
		cuenta.setCuentaNoEncriptada(cliente.getCuentaNoEncriptada());
		cuenta.setCodigoSeguridad(cliente.getCodigoSeguridadCobranza());
		cuenta.setFechaVencimiento(cliente.getFechaVencimientoTarjetaCobranza());
		cuenta.setTipoPromocion(cliente.getTipoPromocion());
		cuenta.setDiaPago(cliente.getDiaPagoTarjetaCobranza() != null ? cliente.getDiaPagoTarjetaCobranza().intValue(): null);		
		
		if(cuenta.getTipoConductoCobro().equals(TipoCobro.TARJETA_CREDITO)){
			cuenta.setTipoCuenta(TipoCuenta.TARJETA_CREDITO);
		}else if(cuenta.getTipoConductoCobro().equals(TipoCobro.CUENTA_AFIRME)){
			cuenta.setTipoCuenta(TipoCuenta.CUENTA);
		}else if(cuenta.getTipoConductoCobro().equals(TipoCobro.DOMICILIACION)){
			if(!StringUtil.isEmpty(cuenta.getCuenta())){
				if(cuenta.getCuenta().length() == 18){
					cuenta.setTipoCuenta(TipoCuenta.CLABE);
				}else if(cuenta.getCuenta().length() == 16){
					cuenta.setTipoCuenta(TipoCuenta.DEBITO);
				}						
			}
		}else if(cuenta.getTipoConductoCobro().equals(TipoCobro.AMERICAN_EXPRESS)){
			cuenta.setTipoCuenta(TipoCuenta.TARJETA_CREDITO);
			
		}	
		if(!StringUtil.isEmpty(cliente.getIdTipoTarjetaCobranza())){
			if(cliente.getIdTipoTarjetaCobranza().equals(TipoTarjeta.VISA.valor())){
				cuenta.setTipoTarjeta(TipoTarjeta.VISA);
			}else if(cliente.getIdTipoTarjetaCobranza().equals(TipoTarjeta.MASTERCARD.valor())){
				cuenta.setTipoTarjeta(TipoTarjeta.MASTERCARD);
			}else if(cliente.getIdTipoTarjetaCobranza().equals("AMEX")){
				cuenta.setTipoTarjeta(TipoTarjeta.AMERICAN_EXPRESS);
			}else if(cliente.getIdTipoTarjetaCobranza().equals(TipoTarjeta.CREDIT_CARD.valor())){
				cuenta.setTipoTarjeta(TipoTarjeta.CREDIT_CARD);
			}
		}
		domicilio.setCalleNumero(cliente.getNombreCalleCobranza());
		domicilio.setCodigoPostal(cliente.getCodigoPostalCobranza());
		domicilio.setNombreColonia(cliente.getNombreColoniaCobranza());
		domicilio.setIdColonia(cliente.getIdColoniaCobranza());
		domicilio.setClaveCiudad(cliente.getIdMunicipioCobranza());
		domicilio.setClaveEstado(cliente.getIdEstadoCobranza());
		cuenta.setDomicilio(domicilio);
		cuenta.setRfc(cliente.getRfcCobranza());
		return cuenta;
	}
	
	private ClienteGenericoDTO convertirClienteCobranza(CuentaPagoDTO cuenta){
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		// Este negocio no sera validado al momento de guardar los datos de cobranza
		cliente.setIdNegocio(-1l); 
		cliente.setClaveTipoPersona(Short.valueOf(cuenta.getTipoPersona()));
		cliente.setIdTipoConductoCobro(Integer.valueOf(cuenta.getTipoConductoCobro().valor()));
		cliente.setIdCliente(cuenta.getIdCliente());
		cliente.setIdBancoCobranza(BigDecimal.valueOf(cuenta.getIdBanco()));
		cliente.setNombreTarjetaHabienteCobranza(cuenta.getTarjetaHabiente());
		cliente.setIdConductoCobranza(cuenta.getIdConductoCliente());
		cliente.setEmailCobranza(cuenta.getCorreo());
		cliente.setTelefonoCobranza(cuenta.getTelefono());	
		cliente.setNumeroTarjetaCobranza(cuenta.getCuenta());
		cliente.setTipoPromocion(cuenta.getTipoPromocion());
		if(cuenta.getTipoConductoCobro().equals(TipoCobro.TARJETA_CREDITO)){	
			cliente.setCodigoSeguridadCobranza(cuenta.getCodigoSeguridad());
			cliente.setFechaVencimientoTarjetaCobranza(cuenta.getFechaVencimiento());
			cliente.setDiaPagoTarjetaCobranza(BigDecimal.valueOf(cuenta.getDiaPago()));
			cliente.setIdTipoTarjetaCobranza(cuenta.getTipoTarjeta().valor());		
		}
		cliente.setIdPaisString(cuenta.getDomicilio().getClavePais());
		cliente.setIdEstadoCobranza(cuenta.getDomicilio().getClaveEstado());
		cliente.setIdMunicipioCobranza(cuenta.getDomicilio().getClaveCiudad());
		cliente.setIdColoniaCobranza(cuenta.getDomicilio().getIdColonia());
		cliente.setNombreColoniaCobranza(cuenta.getDomicilio().getNombreColonia());
		cliente.setNombreCalleCobranza(cuenta.getDomicilio().getCalleNumero());
		cliente.setCodigoPostalCobranza(cuenta.getDomicilio().getCodigoPostal());
		cliente.setRfcCobranza(cuenta.getRfc());
		return cliente;
	}

	@Override
	public CuentaPagoDTO obtenerConductoCobro(BigDecimal idToCliente,
			Long idConducto) throws Exception {
		CuentaPagoDTO cuentaPago = null;
		ClienteGenericoDTO cliente = null;
		if(idConducto != null){
			cliente = clienteService.getConductoCobro(idToCliente.longValue(), idConducto);
			cuentaPago = convertirClienteCobranza(cliente);
		}
		return cuentaPago;
	}
	@Override
	public CuentaPagoDTO obtenerDatosTitularSiEsIgualDatosCotizacion(BigDecimal clienteId) throws Exception{
		CuentaPagoDTO retorno = new CuentaPagoDTO();
		ClienteGenericoDTO cliente = null;
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		filtro.setIdCliente(clienteId);
		cliente = clienteService.loadByIdNoAddress(filtro);
		Long idDomicilioCliente = null;
		Domicilio domicilio  = null;
		if(cliente != null && cliente.getIdToPersona()!= null){
			idDomicilioCliente = getIdDomicilioContratanteByIdPersona(cliente.getIdToPersona().longValue());
		}
		DomicilioImpresion domiImp = domicilioService.findDomicilioImpresionById(idDomicilioCliente);
		domicilio = domiImp.getDomicilio();
		retorno.setTarjetaHabiente(cliente.getNombreCompleto());
		retorno.setRfc(cliente.getCodigoRFC());
		retorno.setDomicilio(domicilio);
		
		return retorno;
	}
	
	//joksrc
	@Override
	public CuentaPagoDTO obtenerConductoCobro(BigDecimal idToCliente, BigDecimal idDomicilio) throws Exception{		
		ClienteGenericoDTO cliente = null;
		Domicilio domicilio  = null;
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		filtro.setIdCliente(idToCliente);
		cliente = clienteService.loadByIdNoAddress(filtro);
		//domicilio = domicilioService.findById(idDomicilio.longValue());
		if(idDomicilio == null){
			Long idDomicilioCliente = null;
			if (cliente != null && cliente.getIdToPersona() !=null){
				idDomicilioCliente = getIdDomicilioContratanteByIdPersona(cliente.getIdToPersona().longValue());
			}
			DomicilioImpresion domicilioImp = domicilioService.findDomicilioImpresionById(idDomicilioCliente);
			domicilio = domicilioImp.getDomicilio();
		}else{
			DomicilioImpresion domicilioImp = domicilioService.findDomicilioImpresionById(idDomicilio.longValue());
			domicilio = domicilioImp.getDomicilio();
		}
		
		return convertirClienteContratante(cliente, domicilio);
	}
	
	private Long getIdDomicilioContratanteByIdPersona(Long idPersona){
		Long idDomicilio = null;
		Persona persona = entidadService.findById(Persona.class, idPersona);
		if(!persona.equals(null)|| persona!=null){
			for(int x=0; x<persona.getDomicilios().size(); x++){
				if(persona.getDomicilios().get(x).getTipoDomicilio().equals("FISC")){
					idDomicilio = persona.getDomicilios().get(x).getIdDomicilio().getIdDomicilio();
				}else if(idDomicilio == null && persona.getDomicilios().get(x).getTipoDomicilio().equals("PERS")){
					idDomicilio = persona.getDomicilios().get(x).getIdDomicilio().getIdDomicilio();
				}
			}
		}
			
		return idDomicilio;
	}
	
	@Override
	public CuentaPagoDTO obtenerConductoCobroCliente(BigDecimal idToCliente) throws Exception{		
		ClienteGenericoDTO cliente = null;
		Domicilio domicilio  = null;
		ClienteGenericoDTO filtro = new ClienteGenericoDTO();
		filtro.setIdCliente(idToCliente);
		cliente = clienteService.loadById(filtro);
		DomicilioImpresion domicilioImp = domicilioService.findDomicilioImpresionById(cliente.getIdDomicilioConsulta());
		domicilio = domicilioImp.getDomicilio();
		return convertirClienteContratante(cliente, domicilio);
	}
	
	@Override
	public CuentaPagoDTO obtenerConductoCobro(Long cotizacionContinuityId, DateTime validoEn) throws Exception {
		CuentaPagoDTO cuenta = null;
		BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByKey(CotizacionContinuity.class, 
																					cotizacionContinuityId, validoEn);
		BigDecimal idToCliente = BigDecimal.valueOf(cotizacion.getValue().getPersonaContratanteId());
		BigDecimal idDomicilio = cotizacion.getValue().getDomicilioContratanteId();
		Long idConducto = cotizacion.getValue().getIdConductoCobroCliente();
		if(idToCliente != null && idConducto != null){
			cuenta = obtenerConductoCobro(idToCliente, idConducto); 
		}else if(idToCliente != null){
			cuenta = obtenerConductoCobro(idToCliente, idDomicilio);
		}		
		return cuenta;
	}

	@Override
	public void guardarConductoCobro(BigDecimal idToCotizacion,
			CuentaPagoDTO cuenta) throws Exception {
		ClienteGenericoDTO cliente = null;
		Long idConductoCobroCliente = null;
		CotizacionDTO cotizacion = null;
		cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		
		//Validacion forma de pago diferente a anual con efectivo
		if(cotizacion.getSolicitudDTO().getNegocio().getAplicaValidacionExterno()){
			if(usuarioService.getUsuarioActual().isOutsider()){
				if(cotizacion.getIdFormaPago().intValue() != FORMA_PAGO_ANUAL && 
					cuenta.getTipoConductoCobro().valor() == TipoCobro.EFECTIVO.valor()){ 
					throw new RuntimeException("No est\u00E1 permitido seleccionar EFECTIVO con una forma de pago diferente a ANUAL.");
				}			
			}
		}
		
		//Validacion recibo menor a mil pesos
		if(cotizacion.getSolicitudDTO().getNegocio().getAplicaValidacionRecibo()){
			if(cuenta.getTipoConductoCobro().valor() == TipoCobro.EFECTIVO.valor()){
				if(obtenerMontoReciboInicial(idToCotizacion, null, null) < RECIBO_MINIMO_PAGO_ANUAL){
					throw new RuntimeException("Para recibos menores a Mil pesos se debe seleccionar con conducto de cobro diferente a EFECTIVO");
				}
			}
		}
		
		if(cuenta.getTipoConductoCobro() != null && !cuenta.getTipoConductoCobro().equals(TipoCobro.EFECTIVO)){
			cliente = convertirClienteCobranza(cuenta);
			reconstruirCuenta(cliente);
			idConductoCobroCliente = clienteService.guardarDatosCobranza(cliente, 
					usuarioService.getUsuarioActual().getNombreUsuario(),true);
		}
		LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro... idConductoCobro => " + idConductoCobroCliente, Level.INFO, null);
		cotizacion.setIdConductoCobroCliente(idConductoCobroCliente);		
		cotizacion.setIdMedioPago(cuenta.getTipoConductoCobro() != null ? 
				BigDecimal.valueOf(cuenta.getTipoConductoCobro().valor()) : null);
		entidadService.save(cotizacion);
	}
	
	@Override
	public Long guardarDatosCobranzaCliente(CuentaPagoDTO cuenta) throws Exception{
		ClienteGenericoDTO cliente = null;
		Long idConductoCobroCliente = null;		
		if(cuenta.getTipoConductoCobro() != null && !cuenta.getTipoConductoCobro().equals(TipoCobro.EFECTIVO)){
			if(cuenta.getTipoPersona() == null){
				cliente = new ClienteGenericoDTO();
				cliente.setIdCliente(cuenta.getIdCliente());
				cliente = clienteService.loadById(cliente);
				cuenta.setTipoPersona(cliente.getClaveTipoPersona().toString());
			}
			cliente = convertirClienteCobranza(cuenta);
			reconstruirCuenta(cliente);
			idConductoCobroCliente = clienteService.guardarDatosCobranza(cliente, 
					usuarioService.getUsuarioActual().getNombreUsuario(),true);
		}
		return idConductoCobroCliente;
	}
	
	@Override
	public void guardarConductoCobroCotizacionAInciso(BigDecimal idToCotizacion){
		try{
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			incisoService.guardarConductoCobroCotizacionAInciso(cotizacion);		
		}catch(Exception e){
			LogDeMidasEJB3.getLogger().log(Level.SEVERE, "guardarConductoCobroCotizacionAInciso" + idToCotizacion, e);
		}
	}
	
	private CuentaPagoDTO convertirClienteContratante(ClienteGenericoDTO cliente, Domicilio domicilio){
		CuentaPagoDTO cuenta = new CuentaPagoDTO();				
		cuenta.setIdCliente(cliente.getIdCliente());		
		cuenta.setTarjetaHabiente(cliente.getNombreCompleto());		
		cuenta.setCorreo(cliente.getEmail());
		if(cliente.getTelefono() != null){
			String telStr = cliente.getTelefono();
			cuenta.setTelefono(telStr.replaceAll("\\D+", ""));
		}		
		cuenta.setRfc(cliente.getCodigoRFC());		
		cuenta.setDomicilio(domicilio);
		return cuenta;
	}
	
	private void reconstruirCuenta(ClienteGenericoDTO cliente){
		ClienteGenericoDTO clienteGuardado = null;
		if(cliente.getIdConductoCobranza() != null){
			try{
				clienteGuardado = clienteService.getConductoCobro(cliente.getIdCliente().longValue(), 
					cliente.getIdConductoCobranza());
			}catch(Exception ex){		
				LogDeMidasEJB3.log("PolizaPagoServiceImpl.reconstruirCuenta ... ERROR al obtener el conductoCobro", Level.INFO, null);
			}
			if(clienteGuardado != null){
				if(!StringUtil.isEmpty(cliente.getNumeroTarjetaCobranza()) && 
						cliente.getNumeroTarjetaCobranza().equals(clienteGuardado.getNumeroTarjetaCobranza())){
					cliente.setNumeroTarjetaCobranza(clienteGuardado.getCuentaNoEncriptada());
				}
			}
		}
	}
	
	@Override
	public Long guardarConductoCobro(BigDecimal idToCotizacion,	CuentaPagoDTO cuenta, Usuario usuario, boolean esEndoso) throws Exception {
		ClienteGenericoDTO cliente = null;
		Long idConductoCobroCliente = null;
		CotizacionDTO cotizacion = null;
		cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		
		//Validacion forma de pago diferente a anual con efectivo
		if(cotizacion.getSolicitudDTO().getNegocio().getAplicaValidacionExterno()){
			if(usuario.isOutsider()){
				if(cotizacion.getIdFormaPago().intValue() != FORMA_PAGO_ANUAL && 
					cuenta.getTipoConductoCobro().valor() == TipoCobro.EFECTIVO.valor()){ 
					throw new RuntimeException("No est\u00E1 permitido seleccionar EFECTIVO con una forma de pago diferente a ANUAL.");
				}			
			}
		}
		
		//Validacion recibo menor a mil pesos
		if(cotizacion.getSolicitudDTO().getNegocio().getAplicaValidacionRecibo()){
			if(cuenta.getTipoConductoCobro().valor() == TipoCobro.EFECTIVO.valor()){
				if(obtenerMontoReciboInicial(idToCotizacion, null, null) < RECIBO_MINIMO_PAGO_ANUAL){
					throw new RuntimeException("Para recibos menores a Mil pesos se debe seleccionar con conducto de cobro diferente a EFECTIVO");
				}
			}
		}
		LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... antes de validacion de tipoconductocobro\n" +
				"--> tipoConductoCobro = "+cuenta.getTipoConductoCobro(), Level.INFO, null);
		if(cuenta.getTipoConductoCobro() != null && !cuenta.getTipoConductoCobro().equals(TipoCobro.EFECTIVO)){
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... cuenta.tipoConductoCobro => " + cuenta.getTipoConductoCobro(), Level.INFO, null);
			cliente = convertirClienteCobranza(cuenta);
			reconstruirCuenta(cliente);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... cliente.idTipoTarjetaCobranza => " + cliente.getIdTipoTarjetaCobranza(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... cliente.mesFechaVencimiento => " + cliente.getMesFechaVencimiento(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... cliente.anioFechaVencimiento => " + cliente.getAnioFechaVencimiento(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... cliente.FechaVencimiento => " + cliente.getFechaVencimientoTarjetaCobranza(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... cliente.idNegocio => " + cliente.getIdNegocio(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... cliente.idCliente => " + cliente.getIdCliente(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... cliente.claveTipoPersona => " + cliente.getClaveTipoPersona(), Level.INFO, null);
			LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro ... usuario.nombre => " + usuario.getNombre(), Level.INFO, null);
			try {
				idConductoCobroCliente = clienteService.guardarDatosCobranza(cliente, usuario.getNombreUsuario(),true);
			}catch (RuntimeException e){
				throw new RuntimeException(e.getMessage());
			}catch (Exception e){
				throw new RuntimeException(e.getMessage());
			}
		}
		LogDeMidasEJB3.log("PolizaPagoServiceImpl.guardarConductoCobro 2... idConductoCobro => " + idConductoCobroCliente+" nombreUsuario:"+usuario.getNombreUsuario(), Level.INFO, null);
		if (!esEndoso) {
			cotizacion.setIdConductoCobroCliente(idConductoCobroCliente);		
			cotizacion.setIdMedioPago(cuenta.getTipoConductoCobro() != null ? 
					BigDecimal.valueOf(cuenta.getTipoConductoCobro().valor()) : null);
			entidadService.save(cotizacion);
		}
		
		return idConductoCobroCliente;
	}

	public String getCodigoSeguridad() {
		return codigoSeguridad;
	}

	public void setCodigoSeguridad(String codigoSeguridad) {
		this.codigoSeguridad = codigoSeguridad;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	@Override
	public void setDatosTarjetaEmision(String fechaVencimiento, String codigoSeguridad) {
		this.setFechaVencimiento(fechaVencimiento);
		this.setCodigoSeguridad(codigoSeguridad);
	}


}
