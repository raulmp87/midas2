package mx.com.afirme.midas2.service.impl.bitemporal;

import java.util.Collection;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;

@Stateless
@SuppressWarnings("rawtypes")
public class EntidadContinuityServiceImpl implements EntidadContinuityService {

	@Override
	public <C extends EntidadContinuity, K> C findContinuityByBusinessKey(Class<C> continuityEntityClass,String businessKeyName, K businessKey) {
		Collection<C> continuities =  this.findByProperty(continuityEntityClass, businessKeyName, businessKey);
		if (continuities != null && continuities.size() > 0) {
			return continuities.iterator().next();
		}
		return null;
	}
	
	@Override
	public <C extends EntidadContinuity, K> C findContinuityByKey(Class<C> continuityEntityClass, K key) {
		return entidadContinuityDao.findByKey(continuityEntityClass, key);
	}
	
	@Override
	public <C extends EntidadContinuity, K> Collection<C> findContinuitiesByParentBusinessKey(Class<C> continuityEntityClass,String parentBusinessKeyName, K parentBusinessKey) {
		return this.findByProperty(continuityEntityClass, parentBusinessKeyName, parentBusinessKey);
	}

	@Override
	public <C extends EntidadContinuity, K> Collection<C> findContinuitiesByParentKey(Class<C> continuityEntityClass,String paraentKeyName, K parentKey) {
		return this.findByProperty(continuityEntityClass, paraentKeyName, parentKey);
	}
	
	private <C extends EntidadContinuity, K> Collection<C> findByProperty(Class<C> continuityEntityClass, String property, K value){
		return entidadContinuityDao.findByProperty(continuityEntityClass, property, value);
	}
	
	private EntidadContinuityDao entidadContinuityDao;
	
	@EJB
	public void setEntidadContinuityDao(
			EntidadContinuityDao entidadContinuityDao) {
		this.entidadContinuityDao = entidadContinuityDao;
	}


}
