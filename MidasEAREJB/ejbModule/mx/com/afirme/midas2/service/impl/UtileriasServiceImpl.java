package mx.com.afirme.midas2.service.impl;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.base.StringUtil;
import mx.com.afirme.midas2.domain.personadireccion.PersonaFisicaMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.UtileriasService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.MovimientoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.util.DateUtils;

@Stateless
public class UtileriasServiceImpl implements UtileriasService {
	
	String CVE_PERSONAFISICA="PF";
	String CVE_PERSONAMORAL="PM";
	
	@EJB
	private EntidadService entidadService;	
	
	@EJB	 
	private OrdenCompraService ordenCompraService;
	
	@EJB	
	private ListadoService listadoService;
	

	@EJB
	private MovimientoSiniestroService movimientoSiniestroService;
	
	@Override
	public int diferenciaDiasFechas(String fechaInicial, String fechaFinal) {
		return DateUtils.diferenciaDiasFechas(fechaInicial, fechaFinal);	
	}

	@Override
	public String obtenerRFCbeneficiario(Integer idPrestador) {
		PrestadorServicio prestador=	entidadService.findById(PrestadorServicio.class, idPrestador);
		if (!StringUtil.isEmpty (prestador.getPersonaMidas().getRfc()) ){
			return prestador.getPersonaMidas().getRfc();		
		
		}else return "";
	}

	@Override
	public String obtenerCURPbeneficiario(Integer idPrestador) {
		PrestadorServicio prestador=	entidadService.findById(PrestadorServicio.class, idPrestador);
		if(null!=prestador && null!=prestador.getPersonaMidas()){
			String tipoPersona = prestador.getPersonaMidas().getTipoPersona();
			String curp="";
			if(tipoPersona.equalsIgnoreCase(CVE_PERSONAFISICA))
				curp=((PersonaFisicaMidas)prestador.getPersonaMidas()).getCurp();	
			else if(tipoPersona.equalsIgnoreCase(CVE_PERSONAMORAL))
				curp="";
			
			return curp;
		}
		else 
			return "";
			
		
	}

	public EntidadService getEntidadService() {
		return entidadService;
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public BigDecimal obtenerTotalesOrdenCompra(Long idEstimacionCobRepCab,
			Long idReporteCabina, Long idcoberturaReporteCabina) {
		return ordenCompraService.obtenerTotales(idEstimacionCobRepCab, idReporteCabina, idcoberturaReporteCabina, null);
	}

	@Override
	public BigDecimal obtenerReserva(Long idToCobertura,
			Long idEstimacionCobertura) {
		BigDecimal reserva = null;
		
		if (idEstimacionCobertura != null) {
			reserva = movimientoSiniestroService.obtenerReservaAfectacion(idEstimacionCobertura, Boolean.FALSE);
		} else {
			reserva = movimientoSiniestroService.obtenerReservaCobertura(idToCobertura, null, Boolean.FALSE);
		}
		
		return reserva;
	}
	

	@Override
	public BigDecimal obtenerReservaDisponibleOrdenCompra(Long idEstimacionCobRepCab,
			Long idReporteCabina, Long idcoberturaReporteCabina) {
	BigDecimal reserva = null;
		
		if (idEstimacionCobRepCab != null) {
			reserva = movimientoSiniestroService.obtenerReservaAfectacion(idEstimacionCobRepCab, Boolean.TRUE);
		} else if (idcoberturaReporteCabina != null){
			reserva = movimientoSiniestroService.obtenerReservaCobertura(idcoberturaReporteCabina, null, Boolean.TRUE);
		} else {
			reserva = movimientoSiniestroService.obtenerReservaReporte(idReporteCabina, Boolean.TRUE);
		}
		
		return reserva;
	}

	@Override
	public boolean ordenCompraAplicaBeneficiario(Long idcoberturaReporteCabina,String cveSubTipoCalculo) {
		return ordenCompraService.ordenCompraAplicaBeneficiario(idcoberturaReporteCabina,cveSubTipoCalculo);
	}

	@Override
	public Map<Long, String> getListasTercerosAfectadorPorCoberturas(
			String cadenaCoberturas) {
		Map<Long, String> map = new HashMap<Long, String>();

		if(!StringUtil.isEmpty(cadenaCoberturas)){
			cadenaCoberturas= cadenaCoberturas.trim();
			 String[] lstIdCoberturas=  cadenaCoberturas.split(",") ;
			 for (String cobertura: lstIdCoberturas){
				 if(!StringUtil.isEmpty(cobertura)){
					 String[] clave=    cobertura.split("\\|") ;
					 Long idCober=new Long(clave[0]);
					 String cveSubcal=clave[1];
					 Map<Long, String> mapLst =listadoService.getListasTercerosAfectadorPorCobertura(idCober,cveSubcal);
					 
					 if(null!=mapLst && !mapLst.isEmpty()){
						 for (Map.Entry<Long, String> entry : mapLst.entrySet())
						 {		
							 EstimacionCoberturaReporteCabina pase = this.entidadService.findById(EstimacionCoberturaReporteCabina.class, entry.getKey());
							 if(!StringUtil.isEmpty(pase.getCveSubCalculo())){
									map.put(entry.getKey(),"["+pase.getCveSubCalculo()+"] "+ entry.getValue());
								}else{
									map.put(entry.getKey(),"["+pase.getCoberturaReporteCabina().getClaveTipoCalculo()+"] "+ entry.getValue());
								}
							 
						     
						 }
					 }
					 


					 
					/* if(null!=mapLst && !mapLst.isEmpty()){
						 map.putAll(mapLst);
					 }*/
				 }
				 
			 }
		}
		
		
		
		
		
		return map;
		
	}

	@Override
	public ImportesOrdenCompraDTO calcularImportes(Long idOrdenCompra,
			Long idOrdenCompraDetalle, boolean calcularPorConcepto) {
		return this.ordenCompraService.calcularImportes(idOrdenCompra, idOrdenCompraDetalle, calcularPorConcepto);
	}
	
	

}

