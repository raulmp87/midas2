/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaTipoCondicionCalculoDao;
import mx.com.afirme.midas2.domain.compensaciones.CaTipoCondicionCalculo;
import mx.com.afirme.midas2.service.compensaciones.CaTipoCondicionCalculoService;

import org.apache.log4j.Logger;

@Stateless

public class CaTipoCondicionCalculoServiceImpl  implements CaTipoCondicionCalculoService {
	public static final String NOMBRE = "nombre";
	public static final String VALOR = "valor";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaTipoCondicionCalculoDao tipoCondicionesCalccaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaTipoCondicionCalculoServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaTipoCondicionCalculo entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaTipoCondicionCalculo entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaTipoCondicionCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	tipoCondicionesCalccaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaTipoCondicionCalculo entity.
	  @param entity CaTipoCondicionCalculo entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaTipoCondicionCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaTipoCondicionCalculo.class, entity.getId());
	        	tipoCondicionesCalccaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaTipoCondicionCalculo entity and return it or a copy of it to the sender. 
	 A copy of the CaTipoCondicionCalculo entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaTipoCondicionCalculo entity to update
	 @return CaTipoCondicionCalculo the persisted CaTipoCondicionCalculo entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaTipoCondicionCalculo update(CaTipoCondicionCalculo entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaTipoCondicionCalculo result = tipoCondicionesCalccaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	update	::	FIN	::	");
	            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaTipoCondicionCalculo 	::		CaTipoCondicionCalculoServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaTipoCondicionCalculo findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaTipoCondicionCalculoServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaTipoCondicionCalculo instance = tipoCondicionesCalccaDao.findById(id);//entityManager.find(CaTipoCondicionCalculo.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaTipoCondicionCalculoServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaTipoCondicionCalculoServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaTipoCondicionCalculo entities with a specific property value.  
	 
	  @param propertyName the name of the CaTipoCondicionCalculo property to query
	  @param value the property value to match
	  	  @return List<CaTipoCondicionCalculo> found by query
	 */
    public List<CaTipoCondicionCalculo> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaTipoCondicionCalculoServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoCondicionCalculo model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaTipoCondicionCalculoServiceImpl	::	findByProperty	::	FIN	::	");
			return tipoCondicionesCalccaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaTipoCondicionCalculoServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaTipoCondicionCalculo> findByNombre(Object nombre
	) {
		return findByProperty(NOMBRE, nombre
		);
	}
	
	public List<CaTipoCondicionCalculo> findByValor(Object valor
	) {
		return findByProperty(VALOR, valor
		);
	}
	
	public List<CaTipoCondicionCalculo> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaTipoCondicionCalculo> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaTipoCondicionCalculo entities.
	  	  @return List<CaTipoCondicionCalculo> all CaTipoCondicionCalculo entities
	 */
	public List<CaTipoCondicionCalculo> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaTipoCondicionCalculoServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaTipoCondicionCalculo model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaTipoCondicionCalculoServiceImpl	::	findAll	::	FIN	::	");
			return tipoCondicionesCalccaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaTipoCondicionCalculoServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}