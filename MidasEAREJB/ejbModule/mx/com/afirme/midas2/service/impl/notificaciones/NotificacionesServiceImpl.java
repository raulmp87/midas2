package mx.com.afirme.midas2.service.impl.notificaciones;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.notificaciones.NotificacionesDao;
import mx.com.afirme.midas2.domain.notificaciones.ConfiguracionNotificaciones;
import mx.com.afirme.midas2.domain.notificaciones.DestinatariosNotificaciones;
import mx.com.afirme.midas2.domain.sistema.Evento;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.notificaciones.NotificacionesService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.MailServiceImpl;

@Stateless
public class NotificacionesServiceImpl implements NotificacionesService{
	
	
	
	@Override
	public List<ConfiguracionNotificaciones> findAllConfig() {
		return notifDao.findAllConfig();
	}

	@Override
	public ConfiguracionNotificaciones findByIdConfig(Long id) {
		return notifDao.findByIdConfig(id);
	}

	@Override
	public List<DestinatariosNotificaciones> findDestinatariosByIdConfig(ConfiguracionNotificaciones idConfiguracion) {
		return notifDao.findDestinatariosByIdConfig(idConfiguracion);
	}

	@Override
	public Long saveConfigNotificaciones(ConfiguracionNotificaciones configNotificaciones) {
		return notifDao.saveConfigNotificaciones(configNotificaciones);
	}

	@Override
	public List<ConfiguracionNotificaciones> findByFilters(ConfiguracionNotificaciones configuracionNotificaciones) {		
		return notifDao.findByFilters(configuracionNotificaciones) ;
	}
	
	

	@Override
	public void eliminarNotificacion(ConfiguracionNotificaciones configuracionNotificaciones) {
		notifDao.eliminarNotificacion(configuracionNotificaciones);		
	}
	
	@Override
	public void notificarNuevoElementoEmision(Evento eventoNuevoElementoEmision) {
		
		DetalleNotificacionElementoEmision detalle;
		String descripcionNuevoElemento = null;
		String descripcionPadre = null;
		String recursoEspecialElemento = null;
						
		try {
						
			ObjectMapper mapper = new ObjectMapper();
			
			detalle = mapper.readValue(eventoNuevoElementoEmision.getMensaje(), DetalleNotificacionElementoEmision.class);
		
		} catch (IOException e) {
			
			throw new RuntimeException(e);
			
		}
				
		List<String> para = obtenerCorreosAreaAdministracionAgentes();
		
		List<String> cc = obtenerCorreosAreasEmision();
				
		if (detalle.getTipo().equals(DetalleNotificacionElementoEmision.TIPO_PRODUCTO)) {
			
			descripcionNuevoElemento = obtenerDescripcionProducto(detalle.getId());
			
			descripcionPadre = obtenerDescripcionNegocio((String) detalle.getIdPadre());
			
			recursoEspecialElemento = "midas.notificaciones.elemento.emision.correo.cuerpo.producto";
			
		} else if (detalle.getTipo().equals(DetalleNotificacionElementoEmision.TIPO_SECCION)) {
			
			descripcionNuevoElemento = obtenerDescripcionSeccion(detalle.getId());
			
			descripcionPadre = obtenerDescripcionProductoPorTipoPoliza(new Long(detalle.getIdPadre().toString()));
			
			recursoEspecialElemento = "midas.notificaciones.elemento.emision.correo.cuerpo.seccion";
			
		} else if (detalle.getTipo().equals(DetalleNotificacionElementoEmision.TIPO_COBERTURA)) {
			
			descripcionNuevoElemento = obtenerDescripcionCobertura(detalle.getId());
			
			descripcionPadre = obtenerDescripcionSeccion(new Long(detalle.getIdPadre().toString()));
						
			recursoEspecialElemento = "midas.notificaciones.elemento.emision.correo.cuerpo.cobertura";
			
		}
				
		
		String asunto = Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.notificaciones.elemento.emision.correo.asunto");
		
		String textoEspecialElemento = Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, recursoEspecialElemento, descripcionPadre);
					
		String cuerpo = Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.notificaciones.elemento.emision.correo.cuerpo", textoEspecialElemento, descripcionNuevoElemento);
						
		/*
		 sendMessageWithExceptionHandler(List<String> recipients, String subject, String messageContent, 
		 	String from, String[] attachments, List<ByteArrayAttachment> fileAttachments,
		 	List<String> cc, List<String> cco)
		 */
		
		LOG.debug("Enviando notificacion nuevo elemento Emision...");
		
		try {
			
			mailService.sendMessageWithExceptionHandler(para, asunto, cuerpo,
					MailServiceImpl.EMAIL_ADMINISTRADOR_MIDAS, null, null, 
					cc, null);
			
		} catch (Exception e) {
			
			throw new RuntimeException(e);
			
		}
		
		LOG.debug("Notificacion nuevo elemento Emision enviada exitosamente.");
		
	}
	
	private String obtenerDescripcionProducto(Long idProducto) {
		
		ProductoDTO producto = entidadService.findById(ProductoDTO.class, new BigDecimal(idProducto));
		
		return producto.getNombreComercial();
		
	}
	
	/**
	 * Obtiene una descripcion del negocio a partir de su clave
	 * @param claveNegocio Clave del negocio (sin considerar VIDA)
	 * @return Descripcion del negocio
	 */
	private String obtenerDescripcionNegocio(String claveNegocio) {
		
		String recurso = "midas.notificaciones.elemento.emision.negocio.danios";
				
		if (CLAVE_NEGOCIO_AUTOS.equalsIgnoreCase(claveNegocio.trim())) {
			
			recurso = "midas.notificaciones.elemento.emision.negocio.autos";
						
		}
		
		return Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, recurso);
		
	}
	
	private String obtenerDescripcionProductoPorTipoPoliza(Long idTipoPoliza) {
		
		TipoPolizaDTO tipoPoliza = entidadService.findById(TipoPolizaDTO.class, new BigDecimal(idTipoPoliza));
		
		return tipoPoliza.getProductoDTO().getNombreComercial();
		
	}
	
	private String obtenerDescripcionSeccion(Long idSeccion) {
	
		SeccionDTO seccion = entidadService.findById(SeccionDTO.class, new BigDecimal(idSeccion));
		
		return seccion.getNombreComercial();
				
	}

	private String obtenerDescripcionCobertura(Long idCobertura) {
	
		CoberturaDTO cobertura = entidadService.findById(CoberturaDTO.class, new BigDecimal(idCobertura));
		
		return cobertura.getNombreComercial();
		
	}
	
	private List<String> obtenerCorreosAreaAdministracionAgentes() {
		
		List<String> correos = new ArrayListNullAware<String>();
			
		List<Usuario> usuariosAreaAdministracionAgentes = usuarioService.buscarUsuariosPorNombreRol(sistemaContext.getRolAdministradorAgentes(), sistemaContext.getRolCoordinadorAgentes());
		
		for (Usuario usuario : usuariosAreaAdministracionAgentes) {
			
			correos.add(usuario.getEmail());
			
		}
		
		return correos;
		
	}
		
	private List<String> obtenerCorreosAreasEmision() {
		
		String cadenaCorreos = parametroGeneralService.getValueByCode(CORREOS_AREAS_EMISION);
				
		return Arrays.asList(cadenaCorreos.split(SEPARADOR));
		
	}
	
	
	private static class DetalleNotificacionElementoEmision {
		
		private static final String TIPO_PRODUCTO = "PRO";
		
		private static final String TIPO_SECCION = "SEC";
		
		private static final String TIPO_COBERTURA = "COB";
		
		private Long id;
		
		private Object idPadre;
		
		private String tipo;

		
		public Long getId() {
			return id;
		}


		public Object getIdPadre() {
			return idPadre;
		}

		
		public String getTipo() {
			return tipo;
		}


		@SuppressWarnings("unused")
		public void setId(Long id) {
			this.id = id;
		}


		@SuppressWarnings("unused")
		public void setIdPadre(Object idPadre) {
			this.idPadre = idPadre;
		}


		@SuppressWarnings("unused")
		public void setTipo(String tipo) {
			this.tipo = tipo;
		}
		
		
				
	}
	
	
	@EJB
	public void setNotifDao(NotificacionesDao notifDao) {
		this.notifDao = notifDao;
	}
	
	private NotificacionesDao notifDao;
	
	
	@EJB
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
		
	private MailService mailService;
	
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
		
	private EntidadService entidadService;
	
	
	@EJB(beanName = "UsuarioServiceDelegate") 
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	private UsuarioService usuarioService;
	
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	private SistemaContext sistemaContext;
	
	
	@EJB
	public void setParametroGeneralService(ParametroGeneralFacadeRemote parametroGeneralService) {
		this.parametroGeneralService = parametroGeneralService;
	}
	
	private ParametroGeneralFacadeRemote parametroGeneralService;
	
	
	
	private static final BigDecimal CORREOS_AREAS_EMISION = new BigDecimal("88001");
	private static final String SEPARADOR = ";";
	private static final String CLAVE_NEGOCIO_AUTOS = "A";
	
	private static final Logger LOG = Logger.getLogger(NotificacionesServiceImpl.class);	
			
}
