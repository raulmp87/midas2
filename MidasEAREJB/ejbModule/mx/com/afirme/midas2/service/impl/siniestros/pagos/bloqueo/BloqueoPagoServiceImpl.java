package mx.com.afirme.midas2.service.impl.siniestros.pagos.bloqueo;

import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.BloqueoPago;
import mx.com.afirme.midas2.domain.siniestros.pagos.OrdenPagoSiniestro;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.CatalogoSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.bloqueo.BloqueoPagoService;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class BloqueoPagoServiceImpl implements BloqueoPagoService {

	private static final String TIPO_BLOQUEO_PROVEEDOR = "PROVEEDOR";
	private static final String TIPO_BLOQUEO_PAGO = "PAGO";
	private static final String ESTATUS_PAGADO = "2";
	
    private static final Map<String, String> MAPA_TIPO_BLOQUEO_DESC;
    private static final Map<Integer, String> MAPA_ESTATUS_BLOQUEO_DESC;
    
    static {
      Map<String, String> tipoDeBloqueo = new HashMap<String, String>();
      tipoDeBloqueo.put("PROVEEDOR", "Proveedor");
      tipoDeBloqueo.put("PAGO", "Pago");
      MAPA_TIPO_BLOQUEO_DESC = Collections.unmodifiableMap(tipoDeBloqueo);
      Map<Integer, String> estatusDeBloqueoMap = new HashMap<Integer, String>();
		estatusDeBloqueoMap.put(new Integer(1), "Bloqueado");
		estatusDeBloqueoMap.put(new Integer(0), "Desbloqueado");
      MAPA_ESTATUS_BLOQUEO_DESC = Collections.unmodifiableMap(estatusDeBloqueoMap);
    }


	@EJB
	private CatalogoSiniestroService catalogoService;

	@EJB
	private EntidadService entidadService;

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;

	@Override
	public List<BloqueoPago> buscarBloqueos(BloqueoPagoFiltro filtroBloqueos) {
		List<BloqueoPago> bloqueoDePagosList = catalogoService.buscar(BloqueoPago.class, filtroBloqueos);
		this.acompletaTransient(bloqueoDePagosList);
		return bloqueoDePagosList;
	}

	private void acompletaTransient(List<BloqueoPago> bloqueoDePagosList) {
		Map<String, String> tiposDeBloqueoMap = this.obtenerTipoDeBloqueoDescripcion();
		Map<Integer, String> situacionesActualesMap = this.obtenerEstatusBloqueoDescripcion();
		for (BloqueoPago bloqueoDePagos : bloqueoDePagosList) {
			bloqueoDePagos.setSituacionActualDesc(situacionesActualesMap.get((bloqueoDePagos.getEstatus())? new Integer(1):new Integer(0)));
			bloqueoDePagos.setTipoDeBloqueoDesc(tiposDeBloqueoMap.get(bloqueoDePagos.getTipo()));
		}
	}
	
	
	@Override
	public void guardarBloqueo(BloqueoPago bloqueo) throws MidasException {
		BloqueoPago bloqueoActual = null;
		String tipoBloqueo = "";
		if(bloqueo.getId()!=null){
			bloqueoActual = this.entidadService.findById(BloqueoPago.class, bloqueo.getId());
			tipoBloqueo = bloqueoActual.getTipo();
		}else{
			tipoBloqueo = bloqueo.getTipo();
		}
		if (tipoBloqueo.equals(TIPO_BLOQUEO_PROVEEDOR) && bloqueo.getEstatus()) {
			this.validarProveedorBloqueado(bloqueo.getProveedor().getId());
		} else if (tipoBloqueo.equals(TIPO_BLOQUEO_PAGO)  && bloqueo.getEstatus() ) {
			this.validarBloqueoPago(bloqueo);
			if(bloqueoActual==null){
				this.agregaPrestador(bloqueo);
			}
		}
		String usuarioActual = usuarioService.getUsuarioActual().getNombreUsuario();
		if(bloqueoActual==null){
			bloqueo.setFechaBloqueo(new Date());
			bloqueo.setUsuarioBloqueo(usuarioActual);
			bloqueo.setEstatus(Boolean.TRUE);
			bloqueo.setCodigoUsuarioCreacion(usuarioActual);
		}else{
			if(bloqueoActual.getEstatus()){
				bloqueoActual.setFechaDesbloqueo(new Date());
				bloqueoActual.setUsuarioDesbloqueo(usuarioActual);
				bloqueoActual.setMotivoDesbloqueo(bloqueo.getMotivoDesbloqueo());
			}else{
				bloqueoActual.setFechaBloqueo(new Date());
				bloqueoActual.setUsuarioBloqueo(usuarioActual);
				bloqueoActual.setMotivoBloqueo(bloqueo.getMotivoBloqueo());
			}
			bloqueoActual.setEstatus(!bloqueoActual.getEstatus());
			bloqueoActual.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
			bloqueoActual.setFechaModificacion(new Date());
			bloqueo = bloqueoActual;
		}
		this.entidadService.save(bloqueo);
		this.notificarBloqueoSeycos(bloqueo);
	}
	
	private void agregaPrestador(BloqueoPago bloqueo) throws MidasException{
		Long pagoId = bloqueo.getOrdenPago().getId();
		OrdenPagoSiniestro ordenPago = this.entidadService.findById(OrdenPagoSiniestro.class, pagoId);
		if(ordenPago == null){
			throw new MidasException("La orden de pago "+pagoId+" no se encontro");
		}
		OrdenCompra ordenCompra = this.entidadService.findById(OrdenCompra.class, ordenPago.getOrdenCompra().getId());
		Integer idProveedor = ordenCompra.getIdBeneficiario();
		if(idProveedor!=null){
			String tipoPrestador = ordenCompra.getTipoProveedor();
			PrestadorServicio proveedor = this.entidadService.findById(PrestadorServicio.class, idProveedor);
			bloqueo.setTipoPrestador(tipoPrestador);
			bloqueo.setProveedor(proveedor);
		}
	}
	
	
	@Override
	public void notificarBloqueoSeycos(BloqueoPago bloqueoPago) {
		// TODO Auto-generated method stub

	}

	@Override
	public BloqueoPago obtenerBloqueoPago(Long idBloquePago) {
		String usuarioActual = usuarioService.getUsuarioActual().getNombreUsuario();
		BloqueoPago bloqueo =  this.entidadService.findById(BloqueoPago.class, idBloquePago);
		if(bloqueo.getEstatus()){
			bloqueo.setFechaDesbloqueo(new Date());
			bloqueo.setUsuarioDesbloqueo(usuarioActual);
		}else{
			bloqueo.setFechaBloqueo(new Date());
			bloqueo.setUsuarioBloqueo(usuarioActual);
		}
		return bloqueo;
	}

	@Override
	public void validarBloqueoPago(BloqueoPago bloqueo) throws MidasException {
		Long idPago = bloqueo.getOrdenPago().getId();
		OrdenPagoSiniestro pago = this.entidadService.findById(OrdenPagoSiniestro.class, idPago);
		if(pago == null){
			throw new MidasException("El pago "+idPago+" no se encontro");
		}
		if(pago.getEstatus().equals(ESTATUS_PAGADO)){
			throw new MidasException("El pago ya se encuuentra en estatus pagado");
		}
		if(estaElPagoBloqueadoPorProovedor(idPago)){
			throw new MidasException("El pago ya se encuuentra bloqueado por el proveedor");
		}
		if(estaElPagoBloqueadoDirectamente(idPago)){
			throw new MidasException("El pago ya se encuuentra bloqueado");
		}
	}

	@Override
	public void validarProveedorBloqueado(Integer idProveedor) throws MidasException {
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("proveedor.id", idProveedor);
		params.put("tipo", TIPO_BLOQUEO_PROVEEDOR);
		params.put("estatus", Boolean.TRUE);
		List<BloqueoPago> listaDeBloqueos = this.entidadService.findByProperties(BloqueoPago.class, params);
		if(listaDeBloqueos!=null && listaDeBloqueos.size()>0){
			throw new MidasException("El proovedor ya se encuentra bloqueado");
		}
	}

	@Override
	public Map<Integer, String> obtenerEstatusBloqueoDescripcion() {
		return MAPA_ESTATUS_BLOQUEO_DESC;
	}

	@Override
	public Map<String, String> obtenerTipoDeBloqueoDescripcion() {
		return MAPA_TIPO_BLOQUEO_DESC;
	}
	
	@Override
	public Boolean estaElProovedorBloqueado(Integer proveedorId){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("proveedor.id", proveedorId);
		params.put("tipo", TIPO_BLOQUEO_PROVEEDOR );
		params.put("estatus", Boolean.TRUE );
		List<BloqueoPago> bloqueos = this.entidadService.findByProperties(BloqueoPago.class, params);
		if(bloqueos.size()>0){
			return true;
		}
		return false;
	}
	
	
	@Override
	public Boolean estaElPagoBloqueadoPorProovedor(Long pagoId){
		OrdenPagoSiniestro ordenPago = this.entidadService.findById(OrdenPagoSiniestro.class, pagoId);
		Long idOrdenCompra = ordenPago.getOrdenCompra().getId();
		OrdenCompra ordenCompra = this.entidadService.findById(OrdenCompra.class, idOrdenCompra);
		Integer idbeneficiario = ordenCompra.getIdBeneficiario();
		if(idbeneficiario!=null){
			return this.estaElProovedorBloqueado(idbeneficiario);
		}else{
			return false;
		}
	}
	
	@Override
	public Boolean estaElPagoBloqueadoDirectamente(Long pagoId) {
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("ordenPago.id", pagoId );
		params.put("tipo", TIPO_BLOQUEO_PAGO );
		params.put("estatus", Boolean.TRUE );
		List<BloqueoPago> bloqueos = this.entidadService.findByProperties(BloqueoPago.class, params);
		if(bloqueos.size()>0){
			return true;
		}
		return false;
	}
	
	

}
