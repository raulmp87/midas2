package mx.com.afirme.midas2.service.impl.seguridad;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.seguridad.AtributoUsuario;
import mx.com.afirme.midas.sistema.seguridad.Menu;
import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.usuario.UsuarioDTO;
import mx.com.afirme.midas2.service.seguridad.MidasASMTranslator;

import com.asm.dto.ConsentDTO;
import com.asm.dto.MenuDTO;
import com.asm.dto.MenuView;
import com.asm.dto.PageConsentDTO;
import com.asm.dto.PageDTO;
import com.asm.dto.PageView;
import com.asm.dto.RoleDTO;
import com.asm.dto.RoleView;
import com.asm.dto.UserAttributeDTO;
import com.asm.dto.UserAttributeView;
import com.asm.dto.UserDTO;
import com.asm.dto.UserRoleDTO;
import com.asm.dto.UserView;

@Stateless
public class MidasASMTranslatorImpl implements MidasASMTranslator {

	public MidasASMTranslatorImpl() {

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.afirme.midas2.service.impl.seguridad.MidasASMTranslator#
	 * obtieneListaUsuariosMidasSinRoles(java.util.List)
	 */
	@Override
	public List<Usuario> obtieneListaUsuariosMidasSinRoles(
			List<UserDTO> listaUsuariosASM) throws ClassCastException {

		List<Usuario> usuarios = new ArrayList<Usuario>();

		for (UserDTO usuarioASM : listaUsuariosASM) {
			// Esto sucede cuando un query de ASM retorna en la lista usuarios
			// nulos. Estos son ignorados.
			if (usuarioASM == null) {
				continue;
			}
			usuarios.add(obtieneUsuarioMidasSinRoles(usuarioASM));
		}

		return usuarios;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.afirme.midas2.service.impl.seguridad.MidasASMTranslator#
	 * obtieneUsuarioMidasSinRoles(com.asm.dto.UserDTO)
	 */
	@Override
	public Usuario obtieneUsuarioMidasSinRoles(UserDTO usuarioASM)
			throws ClassCastException {

		Usuario usuario = new Usuario();

		if (usuarioASM == null) {
			return null;
		}

		usuario.setId(usuarioASM.getUserId());
		usuario.setNombreUsuario(usuarioASM.getUsername());
		usuario.setActivo(usuarioASM.isActive());
		usuario.setApellidoMaterno(usuarioASM.getMotherLastname());
		usuario.setApellidoPaterno(usuarioASM.getFatherLastname());
		usuario.setEmail(usuarioASM.getEmail());
		usuario.setFechaRFC(usuarioASM.getDateRFC());
		usuario.setCurp(usuarioASM.getCurp());
		usuario.setFechaNacimiento(usuarioASM.getDateOfBirth());
		usuario.setHomoclaveRFC(usuarioASM.getHomoclaveRFC());
		usuario.setInicialesRFC(usuarioASM.getSinglesRFC());
		usuario.setNombre(usuarioASM.getFirstname());
		usuario.setPersonaId(usuarioASM.getPersonId());
		usuario.setTelefonoCasa(usuarioASM.getHomePhone());
		usuario.setTelefonoCelular(usuarioASM.getCellPhone());
		usuario.setTelefonoOficina(usuarioASM.getWorkPhone());
		usuario.setOutsider(usuarioASM.isOutsider());
		usuario.setPromoCode(usuarioASM.getPromoCode());
		usuario.setDeviceUuid(usuarioASM.getDeviceUuid());

		return usuario;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.afirme.midas2.service.impl.seguridad.MidasASMTranslator#
	 * obtieneUsuarioMidas(com.asm.dto.UserDTO)
	 */

	public Usuario obtieneUsuarioMidas(UserView usuarioASM,
			UsuarioDTO usuarioDTO) throws ClassCastException {

		Usuario usuario = new Usuario();
		List<Rol> roles = new ArrayList<Rol>();

		if (usuarioASM == null) {
			return null;
		}

		if (usuarioDTO != null) {
			usuario.setCabina(usuarioDTO.getCabina());
		}

		usuario.setId(usuarioASM.getId());
		usuario.setNombreUsuario(usuarioASM.getUsername());
		usuario.setActivo(usuarioASM.isActive());
		usuario.setApellidoMaterno(usuarioASM.getMotherLastname());
		usuario.setApellidoPaterno(usuarioASM.getFatherLastname());
		usuario.setEmail(usuarioASM.getEmail());
		usuario.setFechaRFC(usuarioASM.getDateRFC());
		usuario.setCurp(usuarioASM.getCurp());
		usuario.setFechaNacimiento(usuarioASM.getDateOfBirth());
		usuario.setHomoclaveRFC(usuarioASM.getHomoclaveRFC());
		usuario.setInicialesRFC(usuarioASM.getSinglesRFC());
		usuario.setNombre(usuarioASM.getFirstname());
		usuario.setPersonaId(usuarioASM.getPersonId());
		usuario.setTelefonoCasa(usuarioASM.getHomePhone());
		usuario.setTelefonoCelular(usuarioASM.getCellPhone());
		usuario.setTelefonoOficina(usuarioASM.getWorkPhone());
		usuario.setOutsider(usuarioASM.isOutsider());
		usuario.setPromoCode(usuarioASM.getPromoCode());
		usuario.setDeviceUuid(usuarioASM.getDeviceUuid());
		usuario.setToken(usuarioASM.getToken());

		for (RoleView userRolASM : usuarioASM.getRoles()) {
			roles.add(obtieneRolMidas(userRolASM));
		}
		usuario.setRoles(roles);

		List<Menu> menus = new ArrayList<Menu>();
		for (MenuView menuASM : usuarioASM.getMenus()) {
			menus.add(obtieneMenuMidas(menuASM));
		}
		usuario.setMenus(menus);

		List<Pagina> pages = new ArrayList<Pagina>();
		for (PageView pageASM : usuarioASM.getPages()) {
			pages.add(obtienePaginaMidas(pageASM));
		}
		usuario.setPages(pages);

		if (usuarioASM.getUserAttributes() != null) {
			for (UserAttributeView atributoASM : usuarioASM.getUserAttributes()) {
				usuario.agregarAtributo(obtieneAtributoUsuario(atributoASM));
			}
		}

		return usuario;
	}

	private Rol obtieneRolMidas(RoleView rolASM) throws ClassCastException {
		final Rol rol = new Rol();
		rol.setId(rolASM.getId());
		rol.setDescripcion(rolASM.getName());
		return rol;
	}

	private Menu obtieneMenuMidas(MenuView menuASM) {
		final Menu menu = new Menu();
		menu.setId(menuASM.getId());
		menu.setCodigo(menuASM.getCode());
		menu.setNombre(menuASM.getName());
		menu.setDescripcion(menuASM.getDescription());
		menu.setUrl(menuASM.getUrl());
		// Se pone como default true al no haber equivalente en ASM
		menu.setEstatus(true);
		return menu;
	}

	private Pagina obtienePaginaMidas(PageView paginaASM) {
		final Pagina pagina = new Pagina();
		pagina.setId(paginaASM.getId());
		pagina.setNombre(paginaASM.getName());
		pagina.setNombreAccionDo(paginaASM.getDoAction());
		// Se pone como default '' al no haber equivalente en ASM
		pagina.setDescripcion("");
		return pagina;
	}

	private AtributoUsuario obtieneAtributoUsuario(UserAttributeView atributoASM) {
		return new AtributoUsuario(atributoASM.getName(),
				atributoASM.getValue(), atributoASM.isActive());

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see mx.com.afirme.midas2.service.impl.seguridad.MidasASMTranslator#
	 * obtieneUsuarioMidas(com.asm.dto.UserDTO)
	 */
	@Override
	public Usuario obtieneUsuarioMidas (UserDTO usuarioASM, UsuarioDTO usuarioDTO) throws ClassCastException {
		
		Usuario usuario = new Usuario();
		List<Rol> roles = new ArrayList<Rol>();
		List<Menu> menus = new ArrayList<Menu>();
		List<Pagina> pages = new ArrayList<Pagina>();
		
		if (usuarioASM == null) {
			return null;
		}
		
		if (usuarioDTO != null) {
			usuario.setCabina(usuarioDTO.getCabina());
		}
		
		usuario.setId(usuarioASM.getUserId());
		usuario.setNombreUsuario(usuarioASM.getUsername());
		usuario.setActivo(usuarioASM.isActive());
		usuario.setApellidoMaterno(usuarioASM.getMotherLastname());
		usuario.setApellidoPaterno(usuarioASM.getFatherLastname());
		usuario.setEmail(usuarioASM.getEmail());
		usuario.setFechaRFC(usuarioASM.getDateRFC());
		usuario.setCurp(usuarioASM.getCurp());
		//usuario.setEstadoCivil(Byte.valueOf(usuarioASM.getMaritalStatus()));
		usuario.setFechaNacimiento(usuarioASM.getDateOfBirth());
		usuario.setHomoclaveRFC(usuarioASM.getHomoclaveRFC());
		usuario.setInicialesRFC(usuarioASM.getSinglesRFC());
		usuario.setNombre(usuarioASM.getFirstname());
		usuario.setPersonaId(usuarioASM.getPersonId());
		//usuario.setSexo(Byte.valueOf(usuarioASM.getGender()));
		usuario.setTelefonoCasa(usuarioASM.getHomePhone());
		usuario.setTelefonoCelular(usuarioASM.getCellPhone());
		usuario.setTelefonoOficina(usuarioASM.getWorkPhone());
		usuario.setOutsider(usuarioASM.isOutsider());
		usuario.setPromoCode(usuarioASM.getPromoCode());
		usuario.setDeviceUuid(usuarioASM.getDeviceUuid());
		usuario.setToken(usuarioASM.getToken());
		
		for (UserRoleDTO userRolASM : usuarioASM.getUserRoles()) {
			
			RoleDTO rolASM = userRolASM.getRole();
			Rol rol = obtieneRolMidas(rolASM);
			
			for(MenuDTO menuASM: rolASM.getMenuList()) {
				menus.add(obtieneMenuMidas(menuASM));
			}
			
			for(PageConsentDTO paginaPermisoASM: rolASM.getPageConsents()) {
				pages.add(obtienePaginaMidas(paginaPermisoASM.getPage()));
			}
			
			List<RoleDTO> rights = rolASM.getRights();
			for (RoleDTO right : rights) {
				roles.add(obtieneRolMidas(right));
				
				for(MenuDTO menuASM: right.getMenuList()) {
					menus.add(obtieneMenuMidas(menuASM));
				}
				
				for(PageConsentDTO paginaPermisoASM: right.getPageConsents()) {
					pages.add(obtienePaginaMidas(paginaPermisoASM.getPage()));
				}
				
			}
			roles.add(rol);
		}
		
		usuario.setRoles(roles);
		usuario.setMenus(menus);
		usuario.setPages(pages);
		
		//Agregar los atributos del usuario
		if(usuarioASM.getUserAttributes() != null){
			for(UserAttributeDTO atributoASM : usuarioASM.getUserAttributes()){
				AtributoUsuario atributoMidas = new AtributoUsuario(
						atributoASM.getAttributeName(),
						atributoASM.getAttributeValue(),
						atributoASM.isActive() );
				usuario.agregarAtributo(atributoMidas);
			}
		}
		
		return usuario;
	}

	private Rol obtieneRolMidas(RoleDTO rolASM) throws ClassCastException {

		Rol rol = new Rol();
		List<Menu> menues = new ArrayList<Menu>();
		List<PaginaPermiso> paginasPermisos = new ArrayList<PaginaPermiso>();

		rol.setId(rolASM.getRoleId());
		rol.setDescripcion(rolASM.getRoleName());

		for (Object menuASMObj : rolASM.getMenuList()) {
			if (!(menuASMObj instanceof MenuDTO))
				throw new ClassCastException();
			MenuDTO menuASM = (MenuDTO) menuASMObj;
			menues.add(obtieneMenuMidas(menuASM));
		}

		List<PageConsentDTO> paginaPermisoASMList = rolASM.getPageConsents();
		if (paginaPermisoASMList != null) {
			for (PageConsentDTO paginaPermisoASMObj : paginaPermisoASMList) {
				if (!(paginaPermisoASMObj instanceof PageConsentDTO))
					throw new ClassCastException();
				PageConsentDTO paginaPermisoASM = (PageConsentDTO) paginaPermisoASMObj;
				paginasPermisos
						.add(obtienePaginaPermisoMidas(paginaPermisoASM));
			}
		}

		return rol;
	}

	private Menu obtieneMenuMidas(MenuDTO menuASM) {

		Menu menu = new Menu();

		menu.setId(menuASM.getMenuId());
		menu.setCodigo(menuASM.getMenuCode());
		menu.setNombre(menuASM.getMenuName());
		menu.setDescripcion(menuASM.getMenuDescription());
		menu.setUrl(menuASM.getMenuURL());
		menu.setEstatus(true); // Se pone como default true al no haber
								// equivalente en ASM

		return menu;
	}

	private PaginaPermiso obtienePaginaPermisoMidas(
			PageConsentDTO paginaPermisoASM) throws ClassCastException {

		PaginaPermiso paginaPermiso = new PaginaPermiso();
		List<Permiso> permisos = new ArrayList<Permiso>();

		paginaPermiso.setId(paginaPermisoASM.getPageConsentId());
		paginaPermiso.setPagina(obtienePaginaMidas(paginaPermisoASM.getPage()));

		for (Object permisoASMObj : paginaPermisoASM.getConsentList()) {
			if (!(permisoASMObj instanceof ConsentDTO))
				throw new ClassCastException();
			ConsentDTO permisoASM = (ConsentDTO) permisoASMObj;
			permisos.add(obtienePermisoMidas(permisoASM));
		}

		paginaPermiso.setPermisos(permisos);

		return paginaPermiso;
	}

	private Pagina obtienePaginaMidas(PageDTO paginaASM) {

		Pagina pagina = new Pagina();

		pagina.setId(paginaASM.getPageId());
		pagina.setNombre(paginaASM.getPageName());
		pagina.setNombreAccionDo(paginaASM.getPageDoAction());
		pagina.setDescripcion(""); // Se pone como default '' al no haber
									// equivalente en ASM

		return pagina;
	}

	private Permiso obtienePermisoMidas(ConsentDTO permisoASM) {

		Permiso permiso = new Permiso();

		permiso.setId(permisoASM.getConsentId());
		permiso.setCodigo(permisoASM.getConsentCode());
		permiso.setNombre(permisoASM.getConsentName());
		permiso.setDescripcion(permisoASM.getConsentDescription());

		return permiso;
	}

}
