package mx.com.afirme.midas2.service.impl.bitemporal.suscripcion.cotizacion.inciso;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.Inciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento.BitemporalCoberturaDescuento;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.descuento.CoberturaDescuentoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo.BitemporalCoberturaRecargo;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.recargo.CoberturaRecargoContinuity;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.cotizacion.inciso.IncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class IncisoBitemporalServiceImpl implements IncisoBitemporalService {

	@EJB
	protected EntidadService entidadService;
	
	@EJB 
	protected EntidadContinuityService entidadContinuityService;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;
	
	@EJB
	protected EntidadBitemporalService entidadBitemporalService;
	
	@Override
	public List<BitemporalInciso> getIncisoList(
			CotizacionContinuity cotizacionContinuity) {
		return this.getIncisoList(cotizacionContinuity, TimeUtils.now());
	}

	@Override
	public List<BitemporalInciso> getIncisoList(
			CotizacionContinuity cotizacionContinuity, DateTime validoEn) {
		LogDeMidasInterfaz.log("Entrando a getIncisoList", Level.INFO, null);
		if (cotizacionContinuity == null || cotizacionContinuity.getId() == null 
				|| validoEn == null) {
			throw new IllegalArgumentException();
		}
		
		List<BitemporalInciso> incisos = new ArrayList<BitemporalInciso>();
		
		List<IncisoContinuity> incisoContinuityList = entidadService.findByProperty(IncisoContinuity.class,
			"cotizacionContinuity.id", cotizacionContinuity.getId());
		//LogDeMidasInterfaz.log("incisoContinuityList => " + incisoContinuityList, Level.INFO, null);
		
		for (IncisoContinuity continuity : incisoContinuityList) {
		 BitemporalInciso incisosValidos = continuity.getIncisos().get(validoEn);
			incisos.add(incisosValidos);
		}
		//LogDeMidasInterfaz.log("incisos => " + incisos, Level.INFO, null);
		LogDeMidasInterfaz.log("Saliendo de getIncisoList", Level.INFO, null);
		return incisos;
	}

	@Override
	public BitemporalInciso initEdicion(IncisoContinuity incisoContinuity) {
		return this.initEdicion(incisoContinuity, TimeUtils.now());
	}

	@Override
	public BitemporalInciso initEdicion(IncisoContinuity incisoContinuity,
			DateTime validoEn) {
		if (incisoContinuity == null || incisoContinuity.getId() == null
				|| validoEn == null) {
			throw new IllegalArgumentException();
		}
		incisoContinuity = entidadService.getReference(IncisoContinuity.class,
				incisoContinuity.getId());
		return incisoContinuity.getIncisos().get(validoEn);
	}
	
	
	@Override
	public void borrarIncisosNoAsociados(CotizacionContinuity continuity,
			DateTime validoEn){
		//CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class, continuity.getId());
		//List<BitemporalInciso> incisos = (List<BitemporalInciso>)cotizacionContinuity.getIncisoContinuities().getInProcess(validoEn);
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.cotizacionContinuity.id", continuity.getId());		
		List<BitemporalInciso> incisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class, 
				params, validoEn, null, true);
		if(incisos != null && !incisos.isEmpty()){
			for(BitemporalInciso inciso : incisos){
				if(inciso.getRecordInterval() == null && inciso.getContinuity().getIncisos().get(validoEn)==null && inciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(validoEn).getValue().getAsociadaCotizacion() == null){
					entidadService.remove(inciso.getContinuity());
				}
			}
		}
		
	}

	public BitemporalInciso initAgregar(
			CotizacionContinuity cotizacionContinuity, DateTime validoEn) {
		if (cotizacionContinuity == null
				|| cotizacionContinuity.getId() == null || validoEn == null) {
			throw new IllegalArgumentException();
		}
		cotizacionContinuity = entidadService.getReference(
				CotizacionContinuity.class, cotizacionContinuity.getId());
		IncisoContinuity incisoContinuity = new IncisoContinuity(
				cotizacionContinuity);
		Inciso inciso = new Inciso();
		return new BitemporalInciso(inciso, incisoContinuity);
	}
	
	public BitemporalInciso findBitemporalIncisoByNumero(CotizacionContinuity cotizacionContinuity, Integer numero, DateTime validOn, DateTime recordOn, Boolean inProcess) {
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("continuity.cotizacionContinuity", cotizacionContinuity);
		map.put("continuity.numero", numero);
		Collection<BitemporalInciso> bitemporalIncisos = entidadBitemporalService.listarFiltrado(BitemporalInciso.class, map, validOn, recordOn, inProcess, new String[]{});
		if (bitemporalIncisos.isEmpty()) {
			return null;
		}
		return bitemporalIncisos.iterator().next();
	}

	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	
	public void setEntidadContinuityService(
			EntidadContinuityService entidadContinuityService) {
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void restaurarPrimaIncisoEndoso(BitemporalInciso inciso, DateTime validoEn, Integer tipoEndoso){
		inciso = entidadBitemporalService.getInProcessByKey(IncisoContinuity.class, inciso.getContinuity().getId(), validoEn);
		eliminarDescuentoCoberturaEndoso(-1l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-1l, inciso, validoEn);
		//Se eliminan todos los tipos de descuentos
		eliminarDescuentoCoberturaEndoso(-6l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-6l, inciso, validoEn);
		eliminarDescuentoCoberturaEndoso(-5l, inciso, validoEn);
		eliminarRecargoCoberturaEndoso(-5l, inciso, validoEn);		
	}
	
	public void eliminarDescuentoCoberturaEndoso(Long idTipoDescuento, BitemporalInciso inciso, DateTime validoEn) {

		final Collection<BitemporalSeccionInciso> lstBitemporalSeccionInciso = inciso.getContinuity()
				.getIncisoSeccionContinuities().getInProcess(validoEn);

		for (BitemporalSeccionInciso bitemporalSeccionInciso : lstBitemporalSeccionInciso) {
			final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = bitemporalSeccionInciso
					.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
			for (BitemporalCoberturaSeccion bitemporalCoberturaSeccion : lstBitemporalCoberturaSeccion) {
				final CoberturaDTO cobertura = bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO();
				if ("1".equals(cobertura.getCoberturaEsPropia())) {					
					List<CoberturaDescuentoContinuity> seccionContList = (List<CoberturaDescuentoContinuity>) entidadContinuityService.findContinuitiesByParentKey(CoberturaDescuentoContinuity.class, 
							CoberturaDescuentoContinuity.PARENT_KEY_NAME, bitemporalCoberturaSeccion.getContinuity().getId());
					for(CoberturaDescuentoContinuity coberturaDescuentoContinuity : seccionContList){
						BitemporalCoberturaDescuento bitemporalCoberturaDescuento = coberturaDescuentoContinuity.getCoberturaDescuentos().getInProcess(validoEn);
						try{
						if(bitemporalCoberturaDescuento != null && bitemporalCoberturaDescuento.getContinuity().getDescuentoDTO().getIdToDescuentoVario() != null && 
								bitemporalCoberturaDescuento.getContinuity().getDescuentoDTO().getIdToDescuentoVario().longValue() == idTipoDescuento){
								entidadBitemporalService.remove(bitemporalCoberturaDescuento, validoEn);
						}
						}catch(Exception e){
							
						}
					}
				}
			}

		}
	}	
	
	private void eliminarRecargoCoberturaEndoso(Long idTipoRecargo, BitemporalInciso inciso, DateTime validoEn) {
		final Collection<BitemporalSeccionInciso> lstBitemporalSeccionInciso = inciso.getContinuity()
				.getIncisoSeccionContinuities().getInProcess(validoEn);

		for (BitemporalSeccionInciso bitemporalSeccionInciso : lstBitemporalSeccionInciso) {
			final Collection<BitemporalCoberturaSeccion> lstBitemporalCoberturaSeccion = bitemporalSeccionInciso
					.getContinuity().getCoberturaSeccionContinuities().getInProcess(validoEn);
			for (BitemporalCoberturaSeccion bitemporalCoberturaSeccion : lstBitemporalCoberturaSeccion) {
				final CoberturaDTO cobertura = bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO();
				if ("1".equals(cobertura.getCoberturaEsPropia())) {
					List<CoberturaRecargoContinuity> seccionContList = (List<CoberturaRecargoContinuity>) entidadContinuityService.findContinuitiesByParentKey(CoberturaRecargoContinuity.class, 
							CoberturaRecargoContinuity.PARENT_KEY_NAME, bitemporalCoberturaSeccion.getContinuity().getId());
					for(CoberturaRecargoContinuity coberturaRecargoContinuity : seccionContList){
						try{							
							BitemporalCoberturaRecargo bitemporalCoberturaRecargo = coberturaRecargoContinuity.getCoberturaRecargos().getInProcess(validoEn);
							if(bitemporalCoberturaRecargo != null && bitemporalCoberturaRecargo.getContinuity().getRecargoVarioDTO().getIdtorecargovario() != null && 
								  bitemporalCoberturaRecargo.getContinuity().getRecargoVarioDTO().getIdtorecargovario().longValue() == idTipoRecargo){							
							  entidadBitemporalService.remove(bitemporalCoberturaRecargo, validoEn);
							}
						}catch(Exception e){
							
						}
					}
				}
			}
		}
	}

}
