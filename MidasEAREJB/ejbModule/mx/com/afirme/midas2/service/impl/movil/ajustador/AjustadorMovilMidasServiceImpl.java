package mx.com.afirme.midas2.service.impl.movil.ajustador;

import static mx.com.afirme.midas.sistema.Utilerias.convertToUTF8;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.UnsupportedEncodingException; 
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.lang.String;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.sql.DataSource;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Path;
import javax.validation.Validator;

import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.constantes.ConstantesReporte;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.Coordenadas;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.documentosFortimax.FortimaxDocument;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ProcesosFortimax.PROCESO_FORTIMAX;
import mx.com.afirme.midas2.domain.movil.ajustador.Acciones;
import mx.com.afirme.midas2.domain.movil.ajustador.Ajustador;
import mx.com.afirme.midas2.domain.movil.ajustador.AjustadorMovil;
import mx.com.afirme.midas2.domain.movil.ajustador.Alertas;
import mx.com.afirme.midas2.domain.movil.ajustador.AtencionATallerAsegurado;
import mx.com.afirme.midas2.domain.movil.ajustador.AtencionATercero;
import mx.com.afirme.midas2.domain.movil.ajustador.AtencionParaPagosDeDanios;
import mx.com.afirme.midas2.domain.movil.ajustador.Bien;
import mx.com.afirme.midas2.domain.movil.ajustador.Catalogo;
import mx.com.afirme.midas2.domain.movil.ajustador.Cobertura;
import mx.com.afirme.midas2.domain.movil.ajustador.CoberturaIncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.CondicionEspecialMovil;
import mx.com.afirme.midas2.domain.movil.ajustador.DeclaracionSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.DireccionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Domicilio;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.FindPolizaResponse;
import mx.com.afirme.midas2.domain.movil.ajustador.IncisoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.LlegadaCiaResponse;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarContactoParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.MarcarTerminacionParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.PaseServicioMedico;
import mx.com.afirme.midas2.domain.movil.ajustador.PaseServicioObraCivil;
import mx.com.afirme.midas2.domain.movil.ajustador.Persona;
import mx.com.afirme.midas2.domain.movil.ajustador.Poliza;
import mx.com.afirme.midas2.domain.movil.ajustador.ProcedeSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.Recibo;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.ReporteSiniestrosAsignados;
import mx.com.afirme.midas2.domain.movil.ajustador.SaveDocumentoSiniestroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.Tercero;
import mx.com.afirme.midas2.domain.movil.ajustador.TerceroParameter;
import mx.com.afirme.midas2.domain.movil.ajustador.TipoDocumentoSiniestro;
import mx.com.afirme.midas2.domain.movil.ajustador.ValeGrua;
import mx.com.afirme.midas2.domain.movil.ajustador.Vehiculo;
import mx.com.afirme.midas2.domain.personadireccion.CiudadMidas;
import mx.com.afirme.midas2.domain.personadireccion.ColoniaMidas;
import mx.com.afirme.midas2.domain.personadireccion.EstadoMidas;
import mx.com.afirme.midas2.domain.personadireccion.PaisMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina.CausaSiniestro;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.CoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina.TipoEstimacion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionGenerica;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina.EstatusVigenciaInciso;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.LlegadaCiaSeguros;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroDanosMateriales;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicos;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroGastosMedicosConductor;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCBienes;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCPersonas;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCVehiculo;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.TerceroRCViajero;
import mx.com.afirme.midas2.domain.siniestros.catalogo.oficina.Oficina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ValuacionReporte;
import mx.com.afirme.midas2.dto.CondicionEspecialDTO;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAcciones;
import mx.com.afirme.midas2.dto.sapamis.accionesalertas.SapAmisAccionesRelacion;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSapAmisSupervisionCampo;
import mx.com.afirme.midas2.dto.siniestros.AfectacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosContratanteDTO;
import mx.com.afirme.midas2.dto.siniestros.DatosEstimacionCoberturaDTO;
import mx.com.afirme.midas2.dto.siniestros.EstimacionCoberturaSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.IncisoSiniestroDTO;
import mx.com.afirme.midas2.dto.siniestros.ProgramaPagoDTO;
import mx.com.afirme.midas2.dto.siniestros.ReciboProgramaPagoDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.SiniestroCabinaDTO;
import mx.com.afirme.midas2.dto.siniestros.cabina.reportecabina.ReporteSiniestroVehiculoDTO;
import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.exeption.ErrorBuilder;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.bitemporal.siniestros.SiniestroService;
import mx.com.afirme.midas2.service.catalogos.CatalogoGrupoValorService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.CatalogoAplicacionFortimaxService;
import mx.com.afirme.midas2.service.fortimax.FortimaxV2Service;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.emision.ImpresionesServiceImpl.GestorReportes;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cobertura.CoberturaServiceImpl;
import mx.com.afirme.midas2.service.movil.ajustador.AjustadorMovilMidasService;
import mx.com.afirme.midas2.service.movil.cliente.NotificacionClienteMovilService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestro.AdministradorArchivosService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.EstimacionCoberturaSiniestroService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.ReporteCabinaService.TIPO_FECHA_HORA;
import mx.com.afirme.midas2.service.siniestros.cabina.reportecabina.SiniestroCabinaService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioFiltro;
import mx.com.afirme.midas2.service.siniestros.catalogo.prestadordeservicios.PrestadorDeServicioService.PrestadorServicioRegistro;
import mx.com.afirme.midas2.service.siniestros.recuperacion.RecuperacionDeducibleService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ValuacionReporteService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.utils.CommonUtils;
import mx.com.afirme.midas2.util.StringUtil;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import oracle.jdbc.driver.OracleTypes;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jfree.util.Log;
import org.joda.time.DateTime;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.SqlOutParameter;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import mx.com.afirme.midas2.dto.sapamis.catalogos.CatSapAmisSupervisionCampo;

@Stateless
public class AjustadorMovilMidasServiceImpl implements
		AjustadorMovilMidasService {

	@PersistenceContext
	private EntityManager em;

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSinServices;
	@EJB
	private ReporteCabinaService reporteCabinaService;
	@EJB
	private ListadoService listadoService;
	@EJB
	private PrestadorDeServicioService prestadorDeServicioService;
	@EJB
	private SiniestroCabinaService siniestroCabinaService;

	@EJB
	private PolizaSiniestroService polizaSiniestroService;
	
	@EJB
	public EntidadService entidadService;
	
	@EJB
	private FortimaxV2Service fortimaxV2Service;
	
	@EJB
	private EstimacionCoberturaSiniestroService estimacionCoberturaSiniestroService;
	
	@EJB
	private RecuperacionDeducibleService recuperacionDeducibleService;
	
	@EJB
	private SistemaContext sistemaContext;	
	
	@EJB
    private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
    private ValuacionReporteService valuacionReporteService;
	
	@EJB
	private CatalogoGrupoValorService catalogoGrupoValorService;
	
	@EJB
	private AdministradorArchivosService archivosService;

	@EJB
	private   CatalogoAplicacionFortimaxService catApFortimaxService ;
	
	@EJB
	private   SiniestroService siniestroService ;
	
	@EJB
	private ParametroGeneralService parametroGeneralService;
	
	@EJB
	private NotificacionClienteMovilService notificacionClienteMovilService;

	private SimpleJdbcCall siniestrosEnAtencionFunc;
	private SimpleJdbcCall reporteSiniestroFunc;
	private SimpleJdbcCall getPaseDeclaracionSiniestro;
	private JdbcTemplate jdbcTemplate;
	private NamedParameterJdbcTemplate nJdbcTemplate;
	private SimpleJdbcCall getTerceros;
	private SimpleJdbcCall getTercerosReporte;
	private SimpleJdbcCall getValeDeGrua;
	private SimpleJdbcCall getPaseAtencionATallerAsegurado;
	private SimpleJdbcCall getPaseAtencionATercero;
	private SimpleJdbcCall getPaseServicioObraCivil;
	private SimpleJdbcCall getAtencionParaPagosDeDanios;
	private SimpleJdbcCall getAtencionParaPagosDeDaniosVehiculo;
	private SimpleJdbcCall getAtencionParaPagosDeDaniosBienes;
	private SimpleJdbcCall getPaseServicioMedicoConductor;
	private SimpleJdbcCall getPaseServicioMedicoTerceroPersona;
	private SimpleJdbcCall getPaseServicioMedicoViajero;
	private SimpleJdbcCall getPaseServicioMedicoOcupantes;
	private SimpleJdbcCall borrarTablaTemp;
	private static final String GAVETA_DOC_SINIESTROS_FORTIMAX = "SEG_SIN_AUTOS";
	private static Logger log = Logger.getLogger(AjustadorMovilMidasServiceImpl.class);	
	
	@Resource
	private Validator validator;
	
	@Resource
	private javax.ejb.SessionContext sessionContext;

	@SuppressWarnings("unused")
	private DataSource dataSource;

	private static final String SEPARADOR = "-";

	@Resource(name = "jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
		this.nJdbcTemplate = new NamedParameterJdbcTemplate(dataSource);
		this.siniestrosEnAtencionFunc = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("MIDAS")
				.withCatalogName("app_moviles_ajustadores")
				.withProcedureName("get_Siniestros_En_Atencion")
				.declareParameters(
						new SqlParameter("pidAjustador", Types.INTEGER))
				.declareParameters(
						new SqlOutParameter("pCursor", OracleTypes.CURSOR,
								new RowMapper<ReporteSiniestro>() {
									@Override
									public ReporteSiniestro mapRow(
											ResultSet rs, int rowNum)
											throws SQLException {
										ReporteSiniestro rep = new ReporteSiniestro();
										rep.setId(rs.getLong("idReporte"));
										rep.setNumeroReporte(rs
												.getLong("numeroReporte"));
										rep.setNombreReporta(rs
												.getString("nombreReporta"));
										rep.setTelefono(rs
												.getString("telefonoReporta"));
										rep.setCalleNumero(rs
												.getString("calleNumero"));
										rep.setColonia(rs.getString("colonia"));
										rep.setTipo(rs.getString("tipo"));
										rep.setCausaSiniestro(rs
												.getString("causaSiniestro"));
										rep.setNumeroReporteCabina(rs.getString("reporteCabina"));
										rep.setFechaEnteradoAsignacion(rs.getDate("fechaEnterado"));
										rep.setIdTicket(rs.getLong("idTicket"));
										return rep;
									}
								}));

		this.reporteSiniestroFunc = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName("MIDAS")
				.withCatalogName("app_moviles_ajustadores")
				.withFunctionName("get_Reporte_Siniestro")
				.declareParameters(
						new SqlParameter("pidReporteid", Types.INTEGER))
				.declareParameters(
						new SqlOutParameter("return", OracleTypes.CURSOR,
								new RowMapper<ReporteSiniestro>() {
									@Override
									public ReporteSiniestro mapRow(
											ResultSet rs, int rowNum)
											throws SQLException {
										ReporteSiniestro rep = new ReporteSiniestro();
										rep.setId(rs.getLong("idReporte"));
										rep.setNumeroReporte(rs
												.getLong("numeroReporte"));
										rep.setNombreReporta(rs
												.getString("nombreReporta"));
										rep.setNumeroReporteCabina(rs.getString("reporteCabina"));
										rep.setTelefono(rs
												.getString("telefonoReporta"));
										rep.setTipo(rs.getString("tipo"));
										rep.setCausaSiniestro(rs
												.getString("causaSiniestro"));
										rep.setPoliza(rs.getString("poliza"));
										rep.setNumeroInciso(rs.getInt("numeroInciso"));
										rep.setEstatusInciso(rs.getString("estatus_inciso"));
										rep.setDescripcionInciso(rs.getString("descripcionDelInciso"));
										rep.setPais(rs.getString("pais"));
										rep.setEstado(rs.getString("estado"));
										rep.setCiudad(rs.getString("ciudad"));
										rep.setColonia(rs.getString("colonia"));
										rep.setCodigoPostal(rs.getString("codigoPostal"));
										rep.setCalleNumero(rs.getString("calleNumero"));
										rep.setReferencia(rs.getString("referencia"));
										rep.setKilometro(rs.getString("kilometro"));
										Coordenadas ubicacion = new Coordenadas(rs.getDouble("ubicacionLat"),rs.getDouble("ubicacionLon"));
										rep.setUbicacionCoordenadas(ubicacion);
										rep.setVehiculosParticipantes(rs.getString("vehiculosParticipantes"));
										//rep.setNombreReporta(rs.getString("nombreReporte"));
										rep.setNombreConductor(rs.getString("nombreConductor"));
										rep.setCausaSiniestro(rs.getString("causaSiniestro"));
										Ajustador ajustador = new Ajustador();
										ajustador.setId(rs.getLong("ajustadorId"));
										ajustador.setNombre(rs.getString("ajustador"));
										rep.setAjustador(ajustador);
										rep.setFechaReporte(rs.getTimestamp("fechaReporte"));
										rep.setFechaAsignacion(rs.getTimestamp("fechaAsignacion"));
										rep.setFechaOcurrido(rs.getDate("fechaOcurrido"));
										rep.setHoraOcurrido(rs.getString("horaOcurrido"));
										rep.setFechaCita(rs.getTimestamp("fechaCita"));
										rep.setFechaContacto(rs.getTimestamp("fechaContacto"));
										rep.setFechaTerminacion(rs.getTimestamp("fechaTerminacion"));
										rep.setFechaEnteradoAsignacion(rs.getTimestamp("fechaEnterado"));
										rep.setTipoSiniestro(rs.getLong("tipoSiniestro"));
										rep.setCodigoTipoSiniestro(rs.getString("codigotipoSiniestro"));
										rep.setTerminoAjuste(rs.getString("terminoAjuste"));
										rep.setCodigoTerminoAjuste(rs.getString("codigoTerminoAjuste"));
										rep.setResponsabilidad(rs.getString("responsabilidad"));
										rep.setCodigoResponsabilidad(rs.getString("codigoResponsabilidad"));
										rep.setDeclaracion(rs.getString("declaracion"));
										if(rs.getInt("ciaResponsable") != 0){
											rep.setCiaRespId(rs.getInt("ciaResponsable"));
										}
										/*Seteando datos de Sipac*/
										rep.setOrigen(rs.getString("origen"));
										rep.setIncisoCia(rs.getString("incisoCia"));
										rep.setFechaExpedicion(rs.getDate("fechaExpedicion"));
										rep.setFechaDictamen(rs.getDate("fechaDictamen"));
										rep.setFechaJuicio(rs.getDate("fechaJuicio"));
										/*Seteando datos de Rechazo*/
										rep.setMotivoRechazo(rs.getString("motivoRechazo"));
										rep.setRechazoDesistimiento(rs.getString("rechazoDesistimiento"));
										rep.setObservacionRechazo(rs.getString("observacionRechazo"));
										rep.setMontoDanos(rs.getBigDecimal("montoDanos"));
										rep.setConductorMismoAsegurado(rs.getBoolean("conductorMismoAseg"));
										rep.setConductor(rs.getString("conductor"));
										rep.setEdad(rs.getInt("edadCond"));
										rep.setSexo(rs.getString("sexoCond"));
										rep.setCurp(rs.getString("curpAseg"));
										rep.setRfc(rs.getString("rfcAseg"));
										rep.setLadaTelefonoAseg(rs.getString("ladaTelefonoAseg"));
										rep.setTelefonoAseg(rs.getString("telefonoAseg"));
										rep.setLadaCelular(rs.getString("ladaCelular"));
										rep.setCelular(rs.getString("celular"));
										rep.setEstadoNacimiento(rs.getString("estadoAseg"));
										rep.setColor(rs.getString("color"));
										rep.setPlaca(rs.getString("placa"));
										rep.setParticipoUnidadEqPesado(rs.getBoolean("participoEqPesado"));
										//lugar de ajuste no esta en midas
										//expedienteFortimaxId
										rep.setNumeroSiniestro(rs.getLong("numeroSiniestro"));
										rep.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
										rep.setTipoLicencia(rs.getString("tipoLicencia"));
										rep.setTerminoSiniestro(rs.getString("terminoSiniestro"));
										rep.setPorcientoResponsabilidad(rs.getInt("porcentajeResponsabilidad"));
										rep.setSeFugoTerceroResp(rs.getBoolean("seFugoTerceroResp"));
										rep.setRecibidaOrdenCia(rs.getBoolean("recibidaOrdenCia"));
										rep.setNumeroValuacion(rs.getLong("numeroValuacion"));
										rep.setPolizaCia(rs.getString("polizaCia"));
										rep.setSiniestroCia(rs.getString("siniestroCia"));
										if(rep.getPoliza()!=null){
											IncisoSiniestroDTO detalleInciso = 
												polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(rep.getId());
											rep.setEstatusVigenciaInciso(polizaSiniestroService.obtenerSituacionVigenciaInciso(rep.getId()));
											if(detalleInciso!=null){
												rep.setPolizaId(detalleInciso.getIdToPoliza() + SEPARADOR
														+ detalleInciso.getIncisoContinuityId() + SEPARADOR
														+ detalleInciso.getValidOnMillis() + SEPARADOR
														+ detalleInciso.getRecordFromMillis());
												rep.setProcedeSiniestro(new ProcedeSiniestro());
												rep.getProcedeSiniestro().setProcede(detalleInciso.getEstatus() == 0);
												rep.getProcedeSiniestro().setDescripcion(detalleInciso.getMotivo());
												if(detalleInciso.getEstatus() == EstatusVigenciaInciso.NOVIGENTE.ordinal()){
													rep.setIncisoAutorizado(polizaSiniestroService.estatusIncisoAutorizacion(detalleInciso.getIdToPoliza().longValue(), 
															rep.getId(),detalleInciso.getNumeroInciso(), null));
													}
												rep.setClaveEstatus(detalleInciso.getEstatus());
											}
										}
										if(rs.getLong("personaAseguradoId") != 0){
											ClienteGenericoDTO cliente = new ClienteGenericoDTO();
											cliente.setIdCliente(new BigDecimal(rs.getLong("personaAseguradoId")));
											try {
												cliente = clienteFacadeRemote.loadById(cliente);
												rep.setRfcAseg(cliente.getCodigoRFC());
												rep.setCurpAseg(cliente.getCodigoCURP());
											} catch (Exception e) {
												// TODO Auto-generated catch block
												e.printStackTrace();
											}
												
										}
										ValuacionReporte valuacion = valuacionReporteService.obtenerValuacion(rep.getId());
										if(valuacion != null){
											rep.setNumeroValuacion(valuacion.getId());
											rep.setEstatusValuacion(catalogoGrupoValorService.obtenerValorPorCodigo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_VALUACION_CRUCERO, String.valueOf(valuacion.getEstatus())).getDescripcion());
											rep.setMontoValuacion(CommonUtils.isNullOrZero(valuacion.getTotal()) ? BigDecimal.ZERO : new BigDecimal(valuacion.getTotal()));
										}
									return rep;
									}
								}));
		this.getTerceros = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withFunctionName("get_Terceros")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pid_cobertura", Types.VARCHAR), 
				new SqlOutParameter("return", OracleTypes.CURSOR,
						new RowMapper<Tercero>() {
							@Override
							public Tercero mapRow(ResultSet rs, int index)
									throws SQLException {
								Tercero tercero = new Tercero();
								tercero.setId(rs.getLong("idTercero"));
								tercero.setNombre(rs.getString("nombreTercero"));
								tercero.setDescripcion(rs.getString("cobertura"));
								tercero.setTipoPase(rs.getString("tipoPase"));
								tercero.setEstimacion(rs.getBigDecimal("estimacion"));
						return tercero;
					}
				}));
		this.getTercerosReporte = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_TercerosReporte")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<Tercero>() {
							@Override
							public Tercero mapRow(ResultSet rs, int index)
									throws SQLException {
								Tercero tercero = new Tercero();
								tercero.setId(rs.getLong("idTercero"));
								tercero.setNombre(rs.getString("nombreTercero"));
								tercero.setCobertura(rs.getString("cobertura"));
								tercero.setTipoPase(rs.getString("tipoPase"));
								tercero.setEstimacion(rs.getBigDecimal("estimacion"));
								tercero.setTelefono(rs.getString("telefonoInteresado"));
								tercero.setTipo(rs.getString("tipoVehiculoAfectado"));
								tercero.setMarca(rs.getString("marcaVehiculoAfectado"));
						return tercero;
					}
				}));
		this.getPaseDeclaracionSiniestro = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_Declaracion")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<DeclaracionSiniestro>() {
							@Override
							public DeclaracionSiniestro mapRow(ResultSet rs, int index)
									throws SQLException {
								DeclaracionSiniestro declaracionSiniestro = new DeclaracionSiniestro();
								declaracionSiniestro.setIdReporte(rs.getLong("idReporteSin"));
								declaracionSiniestro.setfSiniestro(rs.getTimestamp("fSiniestro"));
								declaracionSiniestro.setfAtencion(rs.getTimestamp("fAtencion"));
								declaracionSiniestro.setNumeroReporte(rs.getLong("numeroReporte"));
								declaracionSiniestro.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								declaracionSiniestro.setPoliza(rs.getString("poliza"));
								declaracionSiniestro.setInciso(rs.getInt("inciso")>0?rs.getInt("inciso"):null);
								declaracionSiniestro.setEstatusPoliza(rs.getString("estatusPoliza"));
								declaracionSiniestro.setMotivoSituacion(rs.getString("motivoSituacion"));
								if(declaracionSiniestro.getInciso()!=null && declaracionSiniestro.getMotivoSituacion() == null){
									IncisoSiniestroDTO detalleInciso = 
										polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(declaracionSiniestro.getIdReporte());
									declaracionSiniestro.setMotivoSituacion(detalleInciso.getMotivo());
								}
								declaracionSiniestro.setNombreAsegurado(rs.getString("nombreAsegurado"));
								declaracionSiniestro.setNombreConductor(rs.getString("nombreConductor"));
								declaracionSiniestro.setMarcaVehiculoAsegurado(rs.getString("marcaVehiculoAsegurado"));
								declaracionSiniestro.setTipoVehiculoAsegurado(rs.getString("tipoVehiculoAsegurado"));
								declaracionSiniestro.setModeloVehiculoAsegurado(rs.getInt("modeloVehiculoAsegurado")>0?rs.getInt("modeloVehiculoAsegurado"):null);
								declaracionSiniestro.setPlacasVehiculoAsegurado(rs.getString("placaVehiculoAsegurado"));
								declaracionSiniestro.setNumSerieVehiculoAsegurado(rs.getString("numSerieVehiculoAsegurado"));
								declaracionSiniestro.setDeclaracionSiniestro(rs.getString("declaracionSiniestro")); 
								declaracionSiniestro.setTerceros(getTerceros(rs.getLong("idReporteSin")));
								declaracionSiniestro.setTerminoAjuste(rs.getString("terminoAjuste"));
								declaracionSiniestro.setResponsabilidad(rs.getString("responsabilidad"));
								declaracionSiniestro.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								declaracionSiniestro.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
							return declaracionSiniestro;
					}
				}));
		
		this.getValeDeGrua = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_ValeGrua")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<ValeGrua>() {
							@Override
							public ValeGrua mapRow(ResultSet rs, int index)
									throws SQLException {
								ValeGrua valeGrua = new ValeGrua();
								valeGrua.setNumFolio(rs.getString("numFolio"));
								valeGrua.setfVale(rs.getDate("fPaseAtencion"));
								valeGrua.setNumeroReporte(rs.getLong("numeroReporte"));
								valeGrua.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								valeGrua.setfSiniestro(rs.getTimestamp("fSiniestro"));
								valeGrua.setPoliza(rs.getString("poliza"));
								valeGrua.setNumeroInciso(rs.getInt("inciso"));
								valeGrua.setMarcaVehiculo(rs.getString("marcaVehiculo"));
								valeGrua.setTipoVehiculo(rs.getString("tipoVehiculo"));
								valeGrua.setModeloVehiculo(rs.getInt("modeloVehiculo"));
								valeGrua.setPlacasVehiculo(rs.getString("placaVehiculo"));
								valeGrua.setNumSerieVehiculo(rs.getString("numeroSerieVehiculo"));
								valeGrua.setColor(rs.getString("color"));
								valeGrua.setVehiculoAseguradoOTercero(rs.getString("vehiculoAseguradoTercero"));
								valeGrua.setNombreInteresado(rs.getString("nombreInteresado"));
								valeGrua.setServicioManiobraOArrastre(rs.getString("servicioManoObraOArrastre"));
								valeGrua.setRecogerEn(rs.getString("recogeEn"));
								valeGrua.setEntregarEn(rs.getString("entregaEn"));
								valeGrua.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								valeGrua.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return valeGrua;
					}
				}));
		this.getPaseAtencionATallerAsegurado = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseAtencionATallerAseg")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<AtencionATallerAsegurado>() {
							@Override
							public AtencionATallerAsegurado mapRow(ResultSet rs, int index)
									throws SQLException {
								AtencionATallerAsegurado atencionATallerAsegurado = new AtencionATallerAsegurado();
								atencionATallerAsegurado.setNumFolio(rs.getString("numFolio"));
								atencionATallerAsegurado.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								atencionATallerAsegurado.setNumeroReporte(rs.getLong("numeroReporte"));
								atencionATallerAsegurado.setNumeroSiniestro(rs.getLong("numeroReporte")>0?rs.getLong("numeroSiniestro"):null);
								atencionATallerAsegurado.setPoliza(rs.getString("poliza"));
								atencionATallerAsegurado.setNumeroInciso(rs.getInt("inciso"));
								atencionATallerAsegurado.setNombreTallerAsignado(rs.getString("nombreTallerAsignado"));
								atencionATallerAsegurado.setTelefonoTallerAsignado(rs.getString("telefonoTallerAsignado"));
								atencionATallerAsegurado.setDomicilioTaller(new Domicilio(rs.getString("paisTaller"), rs.getString("estadoTaller"), rs.getString("ciudadTaller"), rs.getString("coloniaTaller"), rs.getString("codigoPostalTaller"), rs.getString("calleNumero")));
								atencionATallerAsegurado.setNombreAsegurado(rs.getString("nombreAsegurado"));
								atencionATallerAsegurado.setNombreInteresado(rs.getString("nombreInteresado"));
								//Direccion del interesado -Lugar de Atencion, es correcto?
								atencionATallerAsegurado.setDomicilioInteresado(new Domicilio(rs.getString("paisI"), rs.getString("estadoI"), rs.getString("cuidadI"), rs.getString("coloniaI"), rs.getString("codigoPostalI"), rs.getString("calleNumeroI")));
								atencionATallerAsegurado.setTelefonos(rs.getString("telefonos"));
								atencionATallerAsegurado.setEmail(rs.getString("EMAIL"));
								atencionATallerAsegurado.setMarcaVehiculoAsegurado(rs.getString("marcavehiculoAsegurado"));
								atencionATallerAsegurado.setTipoVehiculoAsegurado(rs.getString("tipoVehiculoAsegurado"));
								atencionATallerAsegurado.setModeloVehiculoAsegurado(rs.getInt("modeloVehiculoAsegurado"));
								atencionATallerAsegurado.setPlacasVehiculoAsegurado(rs.getString("placaVehiculoAsegurado"));
								atencionATallerAsegurado.setNumSerieVehiculoAsegurado(rs.getString("numSerieVehiculoAsegurado"));
								atencionATallerAsegurado.setAreasDaniadas(rs.getString("areasDaniadas"));
								atencionATallerAsegurado.setDaniosPreexistentes(rs.getString("daniosPreexistentes"));
								atencionATallerAsegurado.setAplicaDeducible(rs.getString("aplDeducible"));
								// esta bien?
								atencionATallerAsegurado.setDeducible(recuperacionDeducibleService.obtenerDeduciblePorEstimacion(rs.getLong("idTercero")).doubleValue());
								atencionATallerAsegurado.setPct_deducible(rs.getString("pctDeducible") != null ?  new BigDecimal(rs.getString("pctDeducible")).divide( new BigDecimal(100) ).doubleValue() : 0l);
								atencionATallerAsegurado.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								atencionATallerAsegurado.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return atencionATallerAsegurado;
					}
				}));
		
		this.getPaseAtencionATercero = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseAtencionATercero")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<AtencionATercero>() {
							@Override
							public AtencionATercero mapRow(ResultSet rs, int index)
									throws SQLException {
								AtencionATercero atencionATercero = new AtencionATercero();
								atencionATercero.setNumFolio(rs.getString("numFolio"));
								atencionATercero.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								atencionATercero.setNumeroReporte(rs.getLong("numeroReporte"));
								atencionATercero.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								atencionATercero.setPoliza(rs.getString("poliza"));
								atencionATercero.setNumeroInciso(rs.getInt("inciso"));
								atencionATercero.setNombreTallerAsignado(rs.getString("nombreTallerAsignado"));
								atencionATercero.setTelefonoTallerAsignado(rs.getString("telefonoTallerAsignado"));
								atencionATercero.setCia(rs.getString("nomCiaAseg"));
								atencionATercero.setDomicilioTaller(new Domicilio(rs.getString("paisTaller"), rs.getString("estadoTaller"),
										rs.getString("ciudadTaller"), rs.getString("coloniaTaller"), rs.getString("codigoPostalTaller"), rs.getString("calleNumero")));				
								atencionATercero.setNombreInteresado(rs.getString("nombreInteresado"));	
								//Lugar de atencion, es correcto?
								atencionATercero.setDomicilioInteresado(new Domicilio(rs.getString("paisI"), rs.getString("estadoI"),
										rs.getString("ciudadI"), rs.getString("coloniaI"), rs.getString("codigoPostalI"), rs.getString("calleNumeroI")));				
								atencionATercero.setTelefonos(rs.getString("telefonos"));
								atencionATercero.setEmail(rs.getString("email"));	
								atencionATercero.setMarcaVehiculo(rs.getString("marcaVehiculoAsegurado"));
								atencionATercero.setTipoVehiculo(rs.getString("tipoVehiculoAsegurado"));
								atencionATercero.setModeloVehiculo(rs.getInt("modeloVehiculoAsegurado"));
								atencionATercero.setPlacasVehiculo(rs.getString("placaVehiculoAsegurado"));
								atencionATercero.setNumSerieVehiculo(rs.getString("numSerieVehiculoAsegurado"));
								atencionATercero.setAreasDaniadas(rs.getString("areasDaniadas"));
								atencionATercero.setDaniosPreexistentes(rs.getString("daniosPreexistentes"));
								atencionATercero.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								atencionATercero.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return atencionATercero;
					}
				}));
		
		this.getPaseServicioObraCivil = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseAtencionObraCivil")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<PaseServicioObraCivil>() {
							@Override
							public PaseServicioObraCivil mapRow(ResultSet rs, int index)
									throws SQLException {
								PaseServicioObraCivil paseServicioObraCivil = new PaseServicioObraCivil();
								paseServicioObraCivil.setNumFolio(rs.getString("numFolio"));
								paseServicioObraCivil.setfSiniestro(rs.getDate("fSiniestro"));
								paseServicioObraCivil.setfPaseDeAtencion(rs.getDate("fPaseAtencion"));
								paseServicioObraCivil.setNumeroReporte(rs.getLong("numeroReporte"));
								paseServicioObraCivil.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								paseServicioObraCivil.setPoliza(rs.getString("poliza"));
								paseServicioObraCivil.setInciso(rs.getInt("inciso"));
								paseServicioObraCivil.setNombreIngenieroAsignadoHacerReparacion(rs.getString("nombreIngenieroHacerReparacion"));
								paseServicioObraCivil.setTelefonoIngeniero(rs.getString("telefonoIngeniero"));
								paseServicioObraCivil.setDescripcionBienDaniado(rs.getString("descripcionBienDaniado"));
								//campo ubicacion se guarda la colonia, la calle y el numero
								paseServicioObraCivil.setDomicilioBienDaniado(new Domicilio(rs.getString("pais"), rs.getString("estado"), 
										rs.getString("ciudad"), rs.getString("ubicacion"), null, null));				
								paseServicioObraCivil.setNombreInteresado(rs.getString("nombreInteresado"));	
								paseServicioObraCivil.setTelefono(rs.getString("telefonos")); 
								paseServicioObraCivil.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								paseServicioObraCivil.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return paseServicioObraCivil;
					}
				}));
		
		this.getAtencionParaPagosDeDanios= new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseAtencionPagoDanosMat")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<AtencionParaPagosDeDanios>() {
							@Override
							public AtencionParaPagosDeDanios mapRow(ResultSet rs, int index)
									throws SQLException {
								AtencionParaPagosDeDanios atencionParaPagosDeDanios = new AtencionParaPagosDeDanios();
								atencionParaPagosDeDanios.setNumFolio(rs.getString("numFolio"));
								atencionParaPagosDeDanios.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								atencionParaPagosDeDanios.setNumeroReporte(rs.getLong("numeroReporte"));
								atencionParaPagosDeDanios.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								atencionParaPagosDeDanios.setPoliza(rs.getString("poliza"));
								atencionParaPagosDeDanios.setNumeroInciso(rs.getInt("inciso"));
								atencionParaPagosDeDanios.setNombreAsegurado(rs.getString("nombreAsegurado"));
								atencionParaPagosDeDanios.setNombreInteresado(rs.getString("nombreInteresado"));
								atencionParaPagosDeDanios.setCobertura(rs.getString("coberturaAfectada"));
								//Lugar de atencion, es correcto?
								atencionParaPagosDeDanios.setDomicilioInteresado(new Domicilio(rs.getString("paisI"), rs.getString("estadoI"), rs.getString("ciudadI"), rs.getString("coloniaI"), rs.getString("codigoPostalI"), rs.getString("calleNumeroI")));
								atencionParaPagosDeDanios.setTelefonos(rs.getString("telefonos"));
								atencionParaPagosDeDanios.setEmail(rs.getString("email"));	
								atencionParaPagosDeDanios.setMarcaVehiculoAsegurado(rs.getString("marcaVehiculoAsegurado"));
								atencionParaPagosDeDanios.setTipoVehiculoAsegurado(rs.getString("tipoVehiculoAsegurado"));
								atencionParaPagosDeDanios.setModeloVehiculoAsegurado(rs.getInt("modeloVehiculoAsegurado")>0?rs.getInt("MODELOVEHICULOASEGURADO"):null);
								atencionParaPagosDeDanios.setPlacasVehiculoAsegurado(rs.getString("placasVehiculoAsegurado"));
								atencionParaPagosDeDanios.setNumSerieVehiculoAsegurado(rs.getString("numSerieVehiculoAsegurado"));
								atencionParaPagosDeDanios.setAreasDaniadas(rs.getString("areasDaniadas"));
								atencionParaPagosDeDanios.setDaniosPreexistentes(rs.getString("daniosPreexistentes"));
								atencionParaPagosDeDanios.setAplicaDeducible(rs.getString("aplDeducible"));
								//Es correcto?
								atencionParaPagosDeDanios.setDeducible(recuperacionDeducibleService.obtenerDeduciblePorEstimacion(rs.getLong("idTercero")).doubleValue()); 
								atencionParaPagosDeDanios.setPct_deducible(rs.getString("pctDeducible") != null ?  new BigDecimal(rs.getString("pctDeducible")).divide( new BigDecimal(100) ).doubleValue() : 0l);
								//Es correcto?
								atencionParaPagosDeDanios.setValorComercial(rs.getDouble("valorComercial"));
								atencionParaPagosDeDanios.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								atencionParaPagosDeDanios.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return atencionParaPagosDeDanios;
					}
				}));
		
		this.getAtencionParaPagosDeDaniosVehiculo= new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseAtencionPagoDanosVehic")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<AtencionParaPagosDeDanios>() {
							@Override
							public AtencionParaPagosDeDanios mapRow(ResultSet rs, int index)
									throws SQLException {
								AtencionParaPagosDeDanios atencionParaPagosDeDanios = new AtencionParaPagosDeDanios();
								atencionParaPagosDeDanios.setNumFolio(rs.getString("numFolio"));
								atencionParaPagosDeDanios.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								atencionParaPagosDeDanios.setNumeroReporte(rs.getLong("numeroReporte"));
								atencionParaPagosDeDanios.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								atencionParaPagosDeDanios.setPoliza(rs.getString("poliza"));
								atencionParaPagosDeDanios.setNumeroInciso(rs.getInt("inciso"));
								atencionParaPagosDeDanios.setNombreAsegurado(rs.getString("nombreAsegurado"));
								atencionParaPagosDeDanios.setNombreInteresado(rs.getString("nombreInteresado"));
								atencionParaPagosDeDanios.setCobertura(rs.getString("coberturaAfectada"));
								//lugar de atencion?
								atencionParaPagosDeDanios.setDomicilioInteresado(new Domicilio(rs.getString("paisI"), rs.getString("estadoI"), rs.getString("ciudadI"), rs.getString("coloniaI"), rs.getString("codigoPostalI"), rs.getString("calleNumeroI")));
								atencionParaPagosDeDanios.setTelefonos(rs.getString("telefonos"));
								atencionParaPagosDeDanios.setEmail(rs.getString("email"));	
								atencionParaPagosDeDanios.setMarcaVehiculoAsegurado(rs.getString("marcaVehiculoAsegurado"));
								atencionParaPagosDeDanios.setTipoVehiculoAsegurado(rs.getString("marcaVehiculoAsegurado"));
								atencionParaPagosDeDanios.setModeloVehiculoAsegurado(rs.getInt("modeloVehiculoAsegurado")>0?rs.getInt("MODELOVEHICULOASEGURADO"):null);
								atencionParaPagosDeDanios.setPlacasVehiculoAsegurado(rs.getString("placasVehiculoAsegurado"));
								atencionParaPagosDeDanios.setNumSerieVehiculoAsegurado(rs.getString("numSerieVehiculoAsegurado"));
								atencionParaPagosDeDanios.setAreasDaniadas(rs.getString("areasDaniadas"));
								atencionParaPagosDeDanios.setDaniosPreexistentes(rs.getString("daniosPreexistentes"));
								atencionParaPagosDeDanios.setAplicaDeducible(rs.getString("aplDeducible"));
								//es correcto
								atencionParaPagosDeDanios.setDeducible(rs.getDouble("deducible")); 
								atencionParaPagosDeDanios.setPct_deducible(rs.getDouble("pctDeducible"));
								//es correcto?
								atencionParaPagosDeDanios.setValorComercial(rs.getDouble("valorComercial"));
								atencionParaPagosDeDanios.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								atencionParaPagosDeDanios.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return atencionParaPagosDeDanios;
							}
				}));
		this.getAtencionParaPagosDeDaniosBienes= new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseAtencionPagoDanosBien")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<AtencionParaPagosDeDanios>() {
							@Override
							public AtencionParaPagosDeDanios mapRow(ResultSet rs, int index)
									throws SQLException {
								AtencionParaPagosDeDanios atencionParaPagosDeDanios = new AtencionParaPagosDeDanios();
								atencionParaPagosDeDanios.setNumFolio(rs.getString("numFolio"));
								atencionParaPagosDeDanios.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								atencionParaPagosDeDanios.setNumeroReporte(rs.getLong("numeroReporte"));
								atencionParaPagosDeDanios.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								atencionParaPagosDeDanios.setPoliza(rs.getString("poliza"));
								atencionParaPagosDeDanios.setNumeroInciso(rs.getInt("inciso"));
								atencionParaPagosDeDanios.setNombreAsegurado(rs.getString("nombreAsegurado"));
								atencionParaPagosDeDanios.setNombreInteresado(rs.getString("nombreInteresado"));
								atencionParaPagosDeDanios.setCobertura(rs.getString("coberturaAfectada"));
								//Lugar de atencion, es correcto?
								atencionParaPagosDeDanios.setDomicilioInteresado(new Domicilio(rs.getString("paisI"), rs.getString("estadoI"), rs.getString("ciudadI"), rs.getString("coloniaI"), rs.getString("codigoPostalI"), rs.getString("calleNumeroI")));
								atencionParaPagosDeDanios.setTelefonos(rs.getString("telefonos"));
								atencionParaPagosDeDanios.setEmail(rs.getString("email"));	
								atencionParaPagosDeDanios.setMarcaVehiculoAsegurado(rs.getString("marcaVehiculoAsegurado"));
								atencionParaPagosDeDanios.setTipoVehiculoAsegurado(rs.getString("marcaVehiculoAsegurado"));
								atencionParaPagosDeDanios.setModeloVehiculoAsegurado(rs.getInt("modeloVehiculoAsegurado")>0?rs.getInt("MODELOVEHICULOASEGURADO"):null);
								atencionParaPagosDeDanios.setPlacasVehiculoAsegurado(rs.getString("placasVehiculoAsegurado"));
								atencionParaPagosDeDanios.setNumSerieVehiculoAsegurado(rs.getString("numSerieVehiculoAsegurado"));
								atencionParaPagosDeDanios.setAreasDaniadas(rs.getString("areasDaniadas"));
								atencionParaPagosDeDanios.setDaniosPreexistentes(rs.getString("daniosPreexistentes"));
								atencionParaPagosDeDanios.setAplicaDeducible(rs.getString("aplDeducible"));
								//es correcto?
								atencionParaPagosDeDanios.setDeducible(rs.getDouble("deducible")); 
								atencionParaPagosDeDanios.setPct_deducible(rs.getDouble("pctDeducible"));
								//es correcto?
								atencionParaPagosDeDanios.setValorComercial(rs.getDouble("valorComercial"));
								atencionParaPagosDeDanios.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								atencionParaPagosDeDanios.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return atencionParaPagosDeDanios;
							}
				}));
		
	
		this.getPaseServicioMedicoConductor= new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseServicioMedicoCond")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<PaseServicioMedico>() {
							@Override
							public PaseServicioMedico mapRow(ResultSet rs, int index)
									throws SQLException {
								PaseServicioMedico paseServicioMedico = new PaseServicioMedico();
								paseServicioMedico.setNumFolio(rs.getString("numFolio"));
								paseServicioMedico.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								paseServicioMedico.setNumeroReporte(rs.getLong("numeroReporte"));
								paseServicioMedico.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								paseServicioMedico.setPoliza(rs.getString("poliza"));
								paseServicioMedico.setNumeroInciso(rs.getInt("inciso"));
								paseServicioMedico.setCoberturaAfectada(rs.getString("cobertura"));
								paseServicioMedico.setInstitucionHospitalaria(rs.getString("institucionHospitalaria"));
								paseServicioMedico.setTelefonoHospital(rs.getString("telefonoHospital"));
								paseServicioMedico.setDomicilioHospital(new Domicilio(rs.getString("pais"), rs.getString("estado"), rs.getString("ciudad"), rs.getString("colonia"), rs.getString("codigoPostal"), rs.getString("calleNumero")));
								paseServicioMedico.setCia(rs.getString("nomCiaAseg"));
								paseServicioMedico.setEdad(rs.getInt("edad"));
								paseServicioMedico.setTelefonos(rs.getString("telefonos"));
								paseServicioMedico.setfOcurrio(rs.getDate("fSiniestro"));
								paseServicioMedico.setDescripcionLesiones(rs.getString("descripcionLesiones"));
								//es correcto?
								//paseServicioMedico.setAtencionHastaCantidad(rs.getDouble("atencionHastaCantidad"));
								paseServicioMedico.setNombreLesionado(rs.getString("nombreLesionado"));
								paseServicioMedico.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								paseServicioMedico.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return paseServicioMedico;
							}
				}));
	
		this.getPaseServicioMedicoTerceroPersona= new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseServicioMedicoTercPers")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<PaseServicioMedico>() {
							@Override
							public PaseServicioMedico mapRow(ResultSet rs, int index)
									throws SQLException {
								PaseServicioMedico paseServicioMedico = new PaseServicioMedico();
								paseServicioMedico.setNumFolio(rs.getString("numFolio"));
								paseServicioMedico.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								paseServicioMedico.setNumeroReporte(rs.getLong("numeroReporte"));
								paseServicioMedico.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								paseServicioMedico.setPoliza(rs.getString("poliza"));
								paseServicioMedico.setNumeroInciso(rs.getInt("inciso"));
								paseServicioMedico.setCoberturaAfectada(rs.getString("cobertura"));
								paseServicioMedico.setInstitucionHospitalaria(rs.getString("institucionHospitalaria"));
								paseServicioMedico.setTelefonoHospital(rs.getString("telefonoHospital"));
								paseServicioMedico.setDomicilioHospital(new Domicilio(rs.getString("pais"), rs.getString("estado"), rs.getString("ciudad"), rs.getString("colonia"), rs.getString("codigoPostal"), rs.getString("calleNumero")));
								paseServicioMedico.setCia(rs.getString("nomCiaAseg"));
								paseServicioMedico.setEdad(rs.getInt("edad"));
								paseServicioMedico.setTelefonos(rs.getString("telefonos"));
								paseServicioMedico.setfOcurrio(rs.getDate("fSiniestro"));
								paseServicioMedico.setDescripcionLesiones(rs.getString("descripcionLesiones"));
								//es correcto?
								//paseServicioMedico.setAtencionHastaCantidad(rs.getDouble("atencionHastaCantidad"));
								paseServicioMedico.setNombreLesionado(rs.getString("nombreLesionado"));
								paseServicioMedico.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								paseServicioMedico.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return paseServicioMedico;
							}
				}));
		
	
		this.getPaseServicioMedicoViajero= new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseServicioMedicoViajero")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<PaseServicioMedico>() {
							@Override
							public PaseServicioMedico mapRow(ResultSet rs, int index)
									throws SQLException {
								PaseServicioMedico paseServicioMedico = new PaseServicioMedico();
								paseServicioMedico.setNumFolio(rs.getString("numFolio"));
								paseServicioMedico.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								paseServicioMedico.setNumeroReporte(rs.getLong("numeroReporte"));
								paseServicioMedico.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								paseServicioMedico.setPoliza(rs.getString("poliza"));
								paseServicioMedico.setNumeroInciso(rs.getInt("inciso"));
								paseServicioMedico.setCoberturaAfectada(rs.getString("cobertura"));
								paseServicioMedico.setInstitucionHospitalaria(rs.getString("institucionHospitalaria"));
								paseServicioMedico.setTelefonoHospital(rs.getString("telefonoHospital"));
								paseServicioMedico.setDomicilioHospital(new Domicilio(rs.getString("pais"), rs.getString("estado"), rs.getString("ciudad"), rs.getString("colonia"), rs.getString("codigoPostal"), rs.getString("calleNumero")));
								paseServicioMedico.setCia(rs.getString("nomCiaAseg"));
								paseServicioMedico.setEdad(rs.getInt("edad"));
								paseServicioMedico.setTelefonos(rs.getString("telefonos"));
								paseServicioMedico.setfOcurrio(rs.getDate("fSiniestro"));
								paseServicioMedico.setDescripcionLesiones(rs.getString("descripcionLesiones"));
								//es correcto
								//paseServicioMedico.setAtencionHastaCantidad(rs.getDouble("atencionHastaCantidad"));
								paseServicioMedico.setNombreLesionado(rs.getString("nombreLesionado"));
								paseServicioMedico.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								paseServicioMedico.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return paseServicioMedico;
							}
				}));		
	
		this.getPaseServicioMedicoOcupantes= new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("get_PaseServicioMedico")
		.declareParameters(
				new SqlParameter("pidReporteid", Types.NUMERIC),
				new SqlParameter("pTipoEstimacion", Types.VARCHAR),
				new SqlParameter("pTipoPase", Types.VARCHAR),
				new SqlParameter("pEstimacionId", Types.NUMERIC),
				new SqlOutParameter("pCursor", OracleTypes.CURSOR,
						new RowMapper<PaseServicioMedico>() {
							@Override
							public PaseServicioMedico mapRow(ResultSet rs, int index)
									throws SQLException {
								PaseServicioMedico paseServicioMedico = new PaseServicioMedico();
								paseServicioMedico.setNumFolio(rs.getString("numFolio"));
								paseServicioMedico.setfPaseAtencion(rs.getDate("fPaseAtencion"));
								paseServicioMedico.setNumeroReporte(rs.getLong("numeroReporte"));
								paseServicioMedico.setNumeroSiniestro(rs.getLong("numeroSiniestro")>0?rs.getLong("numeroSiniestro"):null);
								paseServicioMedico.setPoliza(rs.getString("poliza"));
								paseServicioMedico.setNumeroInciso(rs.getInt("inciso"));
								paseServicioMedico.setCoberturaAfectada(rs.getString("cobertura"));
								paseServicioMedico.setInstitucionHospitalaria(rs.getString("institucionHospitalaria"));
								paseServicioMedico.setTelefonoHospital(rs.getString("telefonoHospital"));
								paseServicioMedico.setDomicilioHospital(new Domicilio(rs.getString("pais"), rs.getString("estado"), rs.getString("ciudad"), rs.getString("colonia"), rs.getString("codigoPostal"), rs.getString("calleNumero")));
								paseServicioMedico.setCia(rs.getString("nomCiaAseg"));
								paseServicioMedico.setEdad(rs.getInt("edad"));
								paseServicioMedico.setTelefonos(rs.getString("telefonos"));
								paseServicioMedico.setfOcurrio(rs.getDate("fSiniestro"));
								paseServicioMedico.setDescripcionLesiones(rs.getString("descripcionLesiones"));
								//es correcto?
								//paseServicioMedico.setAtencionHastaCantidad(rs.getDouble("atencionHastaCantidad"));
								paseServicioMedico.setNombreLesionado(rs.getString("nombreLesionado"));
								paseServicioMedico.setNumeroReporteCabina(rs.getString("numeroReporteCabina"));
								paseServicioMedico.setNumeroSiniestroCabina(rs.getString("numeroSiniestroCabina")!=null? rs.getString("numeroSiniestroCabina"):null);
						return paseServicioMedico;
							}
				}));
		this.borrarTablaTemp = new SimpleJdbcCall(jdbcTemplate)
		.withSchemaName("MIDAS")
		.withCatalogName("app_moviles_ajustadores")
		.withProcedureName("deleteTemp")
		.declareParameters(
				new SqlParameter("pidTicket", Types.NUMERIC)
		);
	}

	
	public ReporteSiniestro getReporteSiniestroAsignado(String id, String tipo) {
		Long reporteSiniestroId = null;
		if(tipo!=null && "N".equals(tipo)){
			reporteSiniestroId = getReporteSiniestroId(id);
		} else {
			reporteSiniestroId = Long.parseLong(id);
		}		
		
		ErrorBuilder eb = new ErrorBuilder();
		if (reporteSiniestroId == null) {
			throw new ApplicationException(eb.addFieldError("reporteSiniestroId", "requerido"));
		}		
		ReporteSiniestro reporteSiniestro = getReporteSiniestro(reporteSiniestroId);
		if (reporteSiniestro == null) {
			throw new ApplicationException(eb.addFieldError("reporteSiniestroId", "no existe"));			
		}
		validarReporteAsignadoAjustadorUsuarioActual(reporteSiniestro);
		if(reporteSiniestro.getFechaEnteradoAsignacion() == null){
			confirmarEnteradoAsignacion(reporteSiniestroId, reporteSiniestro.getAjustador().getId());
		}
		
		return reporteSiniestro;
	}
	
	private Long getReporteSiniestroId(String numeroReporte) {
		
		ReporteCabina reporte  = null;
		
		if(StringUtils.isNotEmpty(numeroReporte)){
			String[] id = numeroReporte.split(SEPARADOR);
			if(id!=null){
				if(id.length == 1 || id.length ==2){					
					AjustadorMovil ajustadorMovilUsuarioActual = usuarioService.getAjustadorMovilMidasUsuarioActual();
					ServicioSiniestro ajustador = entidadService.findById(
							ServicioSiniestro.class, ajustadorMovilUsuarioActual.getId());
					Oficina oficina = entidadService.findById(Oficina.class, ajustador.getOficina().getId());
					if(id.length == 1){
						
						reporte = this.reporteCabinaService.buscarReporte(
								oficina.getClaveOficina(), id[0], new SimpleDateFormat("yy").format(new Date()));
					}else {
						reporte = this.reporteCabinaService.buscarReporte(oficina.getClaveOficina(), id[0],  id[1]);
					}
					
				}else if(id.length == 3){
					reporte = this.reporteCabinaService.buscarReporte(id[0].toUpperCase(), id[1], id[2]);
				}
			}
		}
		
		return reporte!=null?reporte.getId():null;
	}
	
	private void validarReporteAsignadoAjustadorUsuarioActual(ReporteSiniestro reporteSiniestro) {
		if (!isReporteAsignadoAjustadorUsuarioActual(reporteSiniestro)) {
			throw new ApplicationException("No se pudo confirmar de enterado porque el reporte esta asignado a otro ajustador.");			
		}		
	}
	
	private boolean isReporteAsignadoAjustadorUsuarioActual(ReporteSiniestro reporteSiniestro) {
		AjustadorMovil ajustadorMovilUsuarioActual = usuarioService.getAjustadorMovilMidasUsuarioActual();
		return reporteSiniestro.getAjustador().getId().equals(ajustadorMovilUsuarioActual.getId());
	}

	@Override
	public Ajustador getAjustador(Usuario usuario) {
		AjustadorMovil ajustadorMovil = usuarioService
				.getAjustadorMovilMidasUsuario(usuario);
		Ajustador ajustador = new Ajustador();
		ajustador.setId(ajustadorMovil.getId());
		ajustador.setNombre(ajustadorMovil.getNombre());
		return ajustador;
	}

	@Override
	public ReporteSiniestro getReporteSiniestro(Long reporteId) {
		SqlParameterSource parameter = new MapSqlParameterSource(
				"pidReporteid", (reporteId));
		Map<String, Object> execute = reporteSiniestroFunc.execute(parameter);
		@SuppressWarnings("unchecked")
		List<ReporteSiniestro> reporteSiniestroList = (List<ReporteSiniestro>) execute.get("return");
		return reporteSiniestroList.get(0);
	}

	@Override
	public ReporteSiniestrosAsignados getReporteSiniestrosAsignados(
			String ajustadorId) {
		SqlParameterSource parameter = new MapSqlParameterSource(
				"pidAjustador", Long.parseLong(ajustadorId));
		Map<String, Object> execute = siniestrosEnAtencionFunc
				.execute(parameter);
		ReporteSiniestrosAsignados reporteSiniestrosAsignados = new ReporteSiniestrosAsignados();
		@SuppressWarnings("unchecked")
		List<ReporteSiniestro> enAtencion = (List<ReporteSiniestro>) execute
				.get("pCursor");
		reporteSiniestrosAsignados.setEnAtencion(enAtencion);
		reporteSiniestrosAsignados
				.setSinTerminoAjuste(new ArrayList<ReporteSiniestro>());
		if(enAtencion != null && !enAtencion.isEmpty()){
			this.borrarDatosTablaTemp(enAtencion.get(0).getIdTicket());
		}
		return reporteSiniestrosAsignados;
	}
	@Asynchronous
	public void borrarDatosTablaTemp(Long idTicket){
		SqlParameterSource parameter = new MapSqlParameterSource(
				"pidTicket", idTicket);
		this.borrarTablaTemp.execute(parameter);
	} 
	@Override
	public List<Cobertura> getCoberturasAfecta(IncisoParameter parameter) {
		List<AfectacionCoberturaSiniestroDTO> coberturasAfectadas =  new ArrayList<AfectacionCoberturaSiniestroDTO>();
		if(parameter.getCodigoTipoSiniestro() != null && parameter.getCodigoTerminoAjuste() != null && parameter.getCodigoResponsabilidad() != null){
			coberturasAfectadas = estimacionCoberturaSinServices
					.obtenerCoberturasAfectacion(parameter.getReporteSiniestroId(), parameter.getCodigoTipoSiniestro(), 
							parameter.getCodigoTerminoAjuste(), parameter.getCodigoResponsabilidad());
		} else {
			coberturasAfectadas = estimacionCoberturaSinServices
					.obtenerCoberturasAfectacion(parameter.getReporteSiniestroId());
		}
		List<Cobertura> coberturas = new ArrayList<Cobertura>();
		for (AfectacionCoberturaSiniestroDTO dto : coberturasAfectadas) {

			Cobertura coberturaReporte = new Cobertura();
			coberturaReporte.setId(dto.getCoberturaReporteCabina().getId());
			coberturaReporte.setNombre(dto.getNombreCobertura());
			coberturaReporte.setAfectable(dto.getEsSiniestrable() != null
					&& dto.getEsSiniestrable());
			coberturaReporte.setRegistro(dto.getSiniestrado() != null
					&& dto.getSiniestrado());
			coberturaReporte.setTipoEstimacion(dto
					.getConfiguracionCalculoCobertura().getTipoEstimacion());
			coberturaReporte.setCveTipoCalculoCobertura(dto
					.getConfiguracionCalculoCobertura()
					.getCveTipoCalculoCobertura());
			coberturaReporte.setClaveFuenteSumaAsegurada(dto
					.getCoberturaReporteCabina()
					.getCoberturaDTO().getClaveFuenteSumaAsegurada()); 
			coberturas.add(coberturaReporte);

		}

		return coberturas;
	}

	@Override
	public void marcarContacto(MarcarContactoParameter parameter) {
		if(parameter.getCoordenadas() == null){
			reporteCabinaService.registrarEventoAjustador(parameter.getReporteSiniestroId(), new Date(),
					parameter.getAjustadorId(),TIPO_FECHA_HORA.CONTACTO); 
		} else {
			reporteCabinaService.registrarEventoAjustador(parameter.getReporteSiniestroId(), new Date(),
					parameter.getAjustadorId(), TIPO_FECHA_HORA.CONTACTO, parameter.getCoordenadas()); 
		}
		notificacionClienteMovilService.notificarArriboAjustador(parameter.getReporteSiniestroId());
	}

	public void confirmarEnteradoAsignacion(Long idReporteSiniestro, Long idAjustador ){
		reporteCabinaService.registrarEventoAjustador(idReporteSiniestro, new Date(), idAjustador, TIPO_FECHA_HORA.ENTERADO);
		notificar(idReporteSiniestro, idAjustador);
	}
	
	private void notificar(Long idReporteSiniestro, Long idAjustador) {
		try {
			ServicioSiniestro ajustador = entidadService.findById(ServicioSiniestro.class, idAjustador);
			if (ServicioSiniestro.AMBITO_PRESTADOR_SERVICIO_INTERNO.equals(ajustador.getAmbitoPrestadorServicio())) {
				final ParametroGeneralDTO parametro = parametroGeneralService
						.findById(new ParametroGeneralId(
								ParametroGeneralDTO.GRUPO_CONFIGURACION_SINIESTRO_MOVIL,
								ParametroGeneralDTO.CODIGO_CONFIGURACION_ESTADOS_SINIESTRO_MOVIL));
				List<String> estadosSoportados = Arrays.asList(parametro.getValor().split(","));
				ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporteSiniestro);
				//Carga el Lazy
				Log.debug("notificar()" + reporte.getLugarOcurrido() );
				if (reporte.getLugarOcurrido() != null) {
					if (estadosSoportados.contains(reporte.getLugarOcurrido()
							.getEstado().getId())) {
						notificacionClienteMovilService.notificarComienzoServicioSiniestro(idReporteSiniestro);
					}
				}
			}
		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}
	
	
	@Override
	public List<String> getRegistrationIds(Long ajustadorId) {
		AjustadorMovil ajustadorMovil = usuarioService
				.getAjustadorMovilMidasUsuario(ajustadorId);
		if (ajustadorMovil == null || ajustadorMovil.getCodigoUsuario() == null) {
			return new ArrayList<String>();
		}
		return usuarioService.getRegistrationIds(ajustadorMovil.getCodigoUsuario(), 
				sistemaContext.getAjustadorMovilDeviceApplicationId());
	}

	@Override
	public List<FindPolizaResponse> findPoliza(FindPolizaParameter parameter) {

		if (StringUtils.isBlank(parameter.getPoliza())
				&& StringUtils.isBlank(parameter.getNumeroSerie())
				&& StringUtils.isBlank(parameter.getPlaca())
				& StringUtils.isBlank(parameter.getContratante())) {
			throw new ApplicationException("Se requiere al menos un dato.");

		}

		if (parameter.getFechaOcurrido() == null) {
			throw new ApplicationException("fechaOcurrido requerido.");
		}
		
		Integer inciso= null;
		String numeroPolizaStr =  "";
		ErrorBuilder errorBuilder = new ErrorBuilder();
		String codigoTipoPolizaProductoStr = "";
		// Validaciones
		if (!StringUtils.isBlank(parameter.getNumeroSerie())) {
			final int minLength = 7;
			String numSerie = parameter.getNumeroSerie().trim().toUpperCase();
			if (numSerie.length() < minLength) {
				errorBuilder.addFieldError("numeroSerie",
						"Se requiere al menos " + minLength
								+ " caracteres para el número de serie.");
				throw new ApplicationException(errorBuilder);
			}
		} else if (!StringUtils.isBlank(parameter.getPoliza())) {
			String[] polizaBloques = parameter.getPoliza().split(SEPARADOR);
			// Busqueda por poliza e inciso
			if (polizaBloques.length <= 4) {
				if (polizaBloques.length == 1){
					numeroPolizaStr = polizaBloques[0];

					if (StringUtils.isBlank(numeroPolizaStr)) {
						errorBuilder.addFieldError("numeroPoliza", "requerido");
						throw new ApplicationException(errorBuilder);
					}

					try {
						Long numeroPoliza = Long.parseLong(numeroPolizaStr);
						if (numeroPoliza > 99999999) {
							errorBuilder.addFieldError("poliza", "Formato Inválido");
							throw new ApplicationException(errorBuilder);
						}

					} catch (NumberFormatException e) {
						errorBuilder.addFieldError("poliza", "Formato Inválido");
						throw new ApplicationException(errorBuilder);
					}
				}else{

					codigoTipoPolizaProductoStr = polizaBloques[0];
					if (!StringUtils.isBlank(codigoTipoPolizaProductoStr)) {
						try {
							Integer codTipoPoliza = Integer
									.parseInt(codigoTipoPolizaProductoStr);
							if (codTipoPoliza > 9999) {
								errorBuilder.addFieldError("poliza", "Formato Inválido");
								throw new ApplicationException(errorBuilder);
							}
						} catch (NumberFormatException e) {
							errorBuilder.addFieldError("poliza", "Formato Inválido");
							throw new ApplicationException(errorBuilder);
						}
					}
					numeroPolizaStr = polizaBloques[1];

					if (StringUtils.isBlank(numeroPolizaStr)) {
						errorBuilder.addFieldError("numeroPoliza", "requerido");
						throw new ApplicationException(errorBuilder);
					}
		
					try {
						Long numeroPoliza = Long.parseLong(numeroPolizaStr);
						if (numeroPoliza > 99999999) {
							errorBuilder.addFieldError("poliza", "Formato Inválido");
							throw new ApplicationException(errorBuilder);
						}
		
					} catch (NumberFormatException e) {
						errorBuilder.addFieldError("poliza", "Formato Inválido");
						throw new ApplicationException(errorBuilder);
					}
					if (polizaBloques.length == 2 && !StringUtils.isBlank(codigoTipoPolizaProductoStr )){
						numeroPolizaStr = parameter.getPoliza().substring(0, 14);
					}
				}
				if (polizaBloques.length ==3){
					if(!StringUtils.isBlank(codigoTipoPolizaProductoStr)){
						numeroPolizaStr = parameter.getPoliza().substring(0, 16);
					} else {
						numeroPolizaStr = parameter.getPoliza().substring(1, 12);
					}
					
					String numeroRenovStr = polizaBloques[2];
					if (!StringUtils.isBlank(numeroRenovStr)) {
						try {
							Integer numeroRenov = Integer.parseInt(numeroRenovStr);
							if (numeroRenov > 99) {
								errorBuilder.addFieldError("poliza", "Formato Inválido");
								throw new ApplicationException(errorBuilder);
							}
						} catch (NumberFormatException e) {
							errorBuilder.addFieldError("poliza", "Formato Inválido");
							throw new ApplicationException(errorBuilder);
						}
					}
				}
	
			} else {
				errorBuilder.addFieldError("poliza", "Formato Inválido");
				throw new ApplicationException(errorBuilder);
			}					
			
			if (parameter.getInciso()!=null) {					
				if (parameter.getInciso() > 999) {
					errorBuilder.addFieldError("inciso", "Formato Inválido");
					throw new ApplicationException(errorBuilder);
				}
				inciso = parameter.getInciso();
			}			
		}
		List<FindPolizaResponse> findPolizaResponseList = new ArrayList<FindPolizaResponse>();
		IncisoSiniestroDTO filtro = new IncisoSiniestroDTO();
		filtro.setNumeroPoliza(numeroPolizaStr);
		filtro.setAutoInciso(new AutoInciso());
		filtro.getAutoInciso().setNumeroSerie(parameter.getNumeroSerie()!=null ? parameter.getNumeroSerie():"");
		filtro.setNumeroInciso(inciso);
		filtro.getAutoInciso().setPlaca(parameter.getPlaca()!=null ? parameter.getPlaca():"");
		filtro.setDatosContratante(new DatosContratanteDTO());
		filtro.getDatosContratante().setNombreContratante(parameter.getContratante()!=null ? parameter.getContratante():"");
		filtro.getAutoInciso().setNumeroMotor(parameter.getNumeroMotor()!=null ? parameter.getNumeroMotor():"");
		filtro.setFechaReporteSiniestro(parameter.getFechaOcurrido());
		filtro.setBusquedaPolizaSeycos(false);
		filtro.setServicioParticular(false);
		filtro.setServicioPublico(false);

		List<IncisoSiniestroDTO> incisos = polizaSiniestroService
				.buscarPolizaFiltro(filtro, parameter.getFechaOcurrido(), parameter.getFechaOcurrido());

			for (IncisoSiniestroDTO incisoSin : incisos) {

				FindPolizaResponse findPolizaResponse = new FindPolizaResponse();
				findPolizaResponse.setId(incisoSin.getIdToPoliza() + SEPARADOR
						+ incisoSin.getIncisoContinuityId() + SEPARADOR
						+ incisoSin.getValidOnMillis() + SEPARADOR
						+ incisoSin.getRecordFromMillis());
				findPolizaResponse.setEstatusVigenciaInciso(polizaSiniestroService.obtenerSituacionVigenciaInciso(
						incisoSin.getValidOn(), parameter.getFechaOcurrido(), 
						incisoSin.getNumeroSerie(), incisoSin.getIdToPoliza())); 
				findPolizaResponse.setFechaInicioVigencia(incisoSin.getFechaIniVigencia());
				findPolizaResponse.setFechaFinVigencia(incisoSin.getFechaFinVigencia());
				findPolizaResponse.setNumeroPoliza(incisoSin.getNumeroPoliza());
				findPolizaResponse.setNumeroSerie(incisoSin.getNumeroSerie());
				findPolizaResponse.setVehiculo(incisoSin.getMarca());
				findPolizaResponse.setNumeroInciso(incisoSin.getNumeroInciso());
				findPolizaResponse.setEstatus(incisoSin.getEstatus() == 0 ? "VIGENTE"
						: (incisoSin.getEstatus() == 1 ? "NO VIGENTE" : (incisoSin
								.getEstatus() == 2 ? "CANCELADO"
								: "NO ESPECIFICADO")));
				findPolizaResponse.setPlaca(incisoSin.getPlaca());
				findPolizaResponse.setNombreContratante(incisoSin
						.getNombreContratante());
				ProcedeSiniestro procede = new ProcedeSiniestro();
				procede.setProcede(incisoSin.getClaveEstatus() == 0);
				procede.setDescripcion(incisoSin.getMotivo());
				findPolizaResponse.setProcedeSiniestro(procede);
				findPolizaResponseList.add(findPolizaResponse);
		}
		return findPolizaResponseList;
	}

	public List<Catalogo> getCatalogoSimple(String tipo) {

		List<Catalogo> catalogo = new ArrayList<Catalogo>();

		if (tipo.equals("CIA") || tipo.equals("HOSP") || tipo.equals("MED")
				|| tipo.equals("TALL") || tipo.equals("ING")) {

			PrestadorServicioFiltro filtro = new PrestadorServicioFiltro();
			filtro.setTipoPrestadorStr(tipo);
			List<PrestadorServicioRegistro> prestadorServicio = prestadorDeServicioService
					.buscar(filtro);
			for (PrestadorServicioRegistro dto : prestadorServicio) {
				Catalogo catalogoSimple = new Catalogo();
				catalogoSimple.setValue(Long.toString(dto.getId()));
				catalogoSimple.setLabel(dto.getNombrePersona());
				catalogo.add(catalogoSimple);
			}
		} else {

			Map<String, String> map = null;

			if (tipo.equals("ESTADO")) {
				map = listadoService.getMapEstadosPorPaisMidas(null);
			} else if (tipo.equals("ESTIMATE_TRANSACTION_CAUSE")) {
				map = listadoService
						.obtenerCatalogoValorFijo(TIPO_CATALOGO.CAUSAS_DE_MOVIMIENTO);
			} else if (tipo.equals("BIEN")) {
				map = listadoService
						.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPOS_DE_BIEN);
			} else if (tipo.equals("COLOR")) {
				map = listadoService
						.obtenerCatalogoValorFijo(TIPO_CATALOGO.COLOR);
			} else if (tipo.equals("TIPO_LICENCIA")){
				map = listadoService
						.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LICENCIA_CONDUCIR);
			} else if (tipo.equals("TERMINO_SIN")){
				map = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TERMINO_AJUSTE_SINIESTRO);
			} else if (tipo.equals("VALUADORES") ){
				AjustadorMovilMidasService processor = this.sessionContext.getBusinessObject(AjustadorMovilMidasService.class);
			    map = processor.catalogoValuadores();
			} else if (tipo.equals("TIPO_ATENCION")){
				map = listadoService
						.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPOS_DE_ATENCION);
			} else if (tipo.equals("ORIGEN_SINIESTRO")){
				map = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.ORIGEN_SINIESTRO);
			} else if (tipo.equals("PERSONA_FISCAL")){
				map = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.PERSONA_FISCAL);
			} else if (tipo.equals("TIPO_TRANSPORTE")){
				map = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_TRANSPORTE);
			} else if (tipo.equals("CIRCUNSTANCIA")){
				map = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.CIRCUNSTANCIA);
			} else if (tipo.equals("MOTIVO_RECHAZO")){
				map = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.MOTIVO_RECHAZO_SINIESTRO, Boolean.TRUE);
			}
			
			if (map != null) {
				for (Map.Entry<String, String> entry : map.entrySet()) {
					Catalogo catalogoSimple = new Catalogo();
					catalogoSimple.setValue(entry.getKey());
					catalogoSimple.setLabel(entry.getValue());
					catalogo.add(catalogoSimple);
				}
			}
		}

		return catalogo;
	}
	public List<Catalogo> getTalleres(PrestadorServicioFiltro filtro) {
		List<Catalogo> catalogo = new ArrayList<Catalogo>();
		List<PrestadorServicioRegistro> prestadorServicio = prestadorDeServicioService
					.buscar(filtro);
		for (PrestadorServicioRegistro dto : prestadorServicio) {
				Catalogo catalogoSimple = new Catalogo();
				catalogoSimple.setValue(Long.toString(dto.getId()));
				catalogoSimple.setLabel(dto.getNombrePersona());
				catalogo.add(catalogoSimple);
		}
		return catalogo;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, String> catalogoValuadores(){	
		return listadoService.obtenerValuadores();
	}
	
	
	/**
	 * Asigna un Inciso de una Poliza a un Reporte Cabina
	 */
	@Override
	public void asignarInciso(IncisoParameter incisoParameter, String Usuario) {
		Long idReporte = incisoParameter.getReporteSiniestroId();

		if (polizaSiniestroService
				.validacionTieneCoberturasAfectadas(idReporte)) {
			throw new ApplicationException(
					MensajeDTO.MENSAJE_ERROR_COBERTURAS_AFECTADAS);
		}

		String[] elementosNumPoliza = incisoParameter.getId().split(SEPARADOR);

		if (elementosNumPoliza.length == 4) {

			Long idToPoliza = Long.valueOf(elementosNumPoliza[0]);
			Long incisoContinuityId = Long.valueOf(elementosNumPoliza[1]);
			Date validOn = new Date(Long.valueOf(elementosNumPoliza[2]));
			Date knownOn = new Date(Long.valueOf(elementosNumPoliza[3]));

			ReporteCabina reporte = this.reporteCabinaService
					.buscarReporte(idReporte);

			try {
				polizaSiniestroService.asignarInciso(idReporte, idToPoliza,
						incisoContinuityId, validOn, knownOn,
						reporte.getFechaHoraOcurrido());
			} catch (Exception e) {
				throw new ApplicationException(e.getMessage());
			}
		}
	}

	public List<Catalogo> getMunicipios(DireccionParameter parameter) {
		List<Catalogo> catalogo = new ArrayList<Catalogo>();
		String estadoId = parameter.getEstadoId();
		Map<String, String> map = null;
		map = listadoService.getMapMunicipiosPorEstadoMidas(estadoId);
		if (map != null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				Catalogo catalogoSimple = new Catalogo();
				catalogoSimple.setValue(entry.getKey());
				catalogoSimple.setLabel(entry.getValue());
				catalogo.add(catalogoSimple);
			}
		}

		return catalogo;
	}

	public List<Catalogo> getTiposSiniestro(String tipo) {
		List<Catalogo> catalogo = new ArrayList<Catalogo>();
		Map<String, String> map = null;
		if (tipo.equals("causa_siniestro")) {
			map = listadoService
					.obtenerCatalogoValorFijo(TIPO_CATALOGO.CAUSA_SINIESTRO);
		}
		if (map != null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				Catalogo catalogoSimple = new Catalogo();
				catalogoSimple.setValue(entry.getKey());
				catalogoSimple.setLabel(entry.getValue());
				catalogo.add(catalogoSimple);
			}
		}
		return catalogo;
	}

	public List<Catalogo> getResponsabilidad(String tipo, String tipoSiniestro) {
		List<Catalogo> catalogo = new ArrayList<Catalogo>();
		Map<String, String> map = listadoService.obtenerCatalogoValorFijoByStr(
				tipo.toUpperCase(), tipoSiniestro);
		if (map != null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				Catalogo catalogoSimple = new Catalogo();
				catalogoSimple.setValue(entry.getKey());
				catalogoSimple.setLabel(entry.getValue());
				catalogo.add(catalogoSimple);
			}
		}

		return catalogo;
	}

	public List<Catalogo> getTerminosAjuste(String tipoSiniestro,
			String responsabilidad) {
		List<Catalogo> catalogo = new ArrayList<Catalogo>();
		Map<String, String> map = null;
		map = listadoService.obtenerTerminosAjuste(tipoSiniestro,
				responsabilidad);
		if (map != null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				Catalogo catalogoSimple = new Catalogo();
				catalogoSimple.setValue(entry.getKey());
				catalogoSimple.setLabel(entry.getValue());
				catalogo.add(catalogoSimple);
			}
		}

		return catalogo;
	}

	public List<Catalogo> getTiposPases(CoberturaIncisoParameter parameter) {
		List<Catalogo> catalogo = new ArrayList<Catalogo>();
		Map<String, String> map = null;
		map = listadoService.obtenerTipoDePasePorTipoEstimacion(
				parameter.getTipoEstimacion(), parameter.getResponsabilidad(), 
				parameter.getCodigoCausaSiniestro(), parameter.getCodigoTerminoAjuste());
		if (map != null) {
			for (Map.Entry<String, String> entry : map.entrySet()) {
				Catalogo catalogoSimple = new Catalogo();
				catalogoSimple.setValue(entry.getKey());
				catalogoSimple.setLabel(entry.getValue());
				catalogo.add(catalogoSimple);
			}
		}

		return catalogo;
	}

	public List<ColoniaMidas> getInfoCP(DireccionParameter parameter) {

		List<ColoniaMidas> colonia = listadoService
				.getColoniasPorCPMidas(parameter.getCodigoPostal());

		return colonia;
	}

	@Override
	public Poliza getPoliza(String id, Long idReporte, Date fechaOcurrido) {
		ErrorBuilder eb = new ErrorBuilder();
		if (fechaOcurrido == null) {
			throw new ApplicationException(eb.addFieldError("fechaOcurrido",
					"requerido"));
		}
		
		Poliza poliza = null;
		
		String[] elementosNumPoliza = id.split(SEPARADOR);

		if (elementosNumPoliza.length == 4) {
			Long idToPoliza  = Long.valueOf(elementosNumPoliza[0]);
			Long incisoContinuityId = Long.valueOf(elementosNumPoliza[1]);
			Date validOn = new Date(Long.valueOf(elementosNumPoliza[2]));
			Date knownOn = new Date(Long.valueOf(elementosNumPoliza[3]));
			
			ReporteCabina reporte = this.reporteCabinaService
					.buscarReporte(idReporte);
			
			IncisoSiniestroDTO filtro = new IncisoSiniestroDTO();
			filtro.setIncisoContinuityId(incisoContinuityId);
			filtro.setFechaReporteSiniestro(reporte.getFechaHoraOcurrido());
			IncisoSiniestroDTO inciso = polizaSiniestroService
					.buscarDetalleInciso(filtro, validOn, knownOn, reporte.getFechaHoraOcurrido());
			
			poliza = new Poliza();
			poliza.setNumeroPoliza(inciso.getNumeroPoliza());
			poliza.setFechaInicioVigencia(inciso.getFechaIniVigencia());
			poliza.setFechaFinVigencia(inciso.getFechaFinVigencia());
			poliza.setEstatusVigenciaInciso(polizaSiniestroService.obtenerSituacionVigenciaInciso(
					inciso.getValidOn(), fechaOcurrido, 
					inciso.getNumeroSerie(), inciso.getIdToPoliza()));
			poliza.setEstatus(inciso.getDescEstatus());
			ProcedeSiniestro procede = new ProcedeSiniestro();
			procede.setProcede(inciso.getEstatus() == 0);
			procede.setDescripcion(inciso.getMotivo());
			poliza.setProcedeSiniestro(procede);
			poliza.setNombreContratante(inciso.getNombreContratante());
			poliza.setNumeroInciso(inciso.getNumeroInciso());
			if(inciso.getAutoInciso() != null) {
				if(inciso.getAutoInciso().getNombreCompletoConductor() != null) {
					poliza.setNombreConductor(inciso.getAutoInciso().getNombreCompletoConductor());
				}
				if(inciso.getAutoInciso().getDescMarca() != null){
					poliza.setVehiculo(inciso.getAutoInciso().getDescMarca() + " " + inciso.getAutoInciso().getDescripcionFinal() +
							" " + inciso.getAutoInciso().getModeloVehiculo() );
				}
				if(inciso.getAutoInciso().getNumeroSerie() != null){
					poliza.setNumeroSerie(inciso.getAutoInciso().getNumeroSerie());
				}
				if(inciso.getAutoInciso().getTipoServicioId() != null){
					TipoServicioVehiculoDTO tipoServicioDto = entidadService.findById(TipoServicioVehiculoDTO.class, new BigDecimal(
							inciso.getAutoInciso().getTipoServicioId()));
					poliza.setServicio(tipoServicioDto.getDescripcionTipoServVehiculo());
				}
				if (inciso.getAutoInciso().getDescTipoUso() != null){
					poliza.setUso(inciso.getAutoInciso().getDescTipoUso());
				}
				if(inciso.getAutoInciso().getNumeroMotor() != null){
					poliza.setNumeroMotor(inciso.getAutoInciso().getNumeroMotor());
				}
				if (inciso.getAutoInciso().getPlaca() != null){
					poliza.setPlaca(inciso.getAutoInciso().getPlaca());
				}
				if(inciso.getNombreLinea() != null){
					poliza.setLineaNegocio(inciso.getNombreLinea());
				}
				if(inciso.getAutoInciso().getPaquete().getDescripcion() != null){
					poliza.setPaquete(inciso.getAutoInciso().getPaquete().getDescripcion());
				}
			}
			
					
			List<BitemporalCoberturaSeccion> coberturaSeccion = new ArrayList<BitemporalCoberturaSeccion>();
			coberturaSeccion = polizaSiniestroService.obtenerCoberturasInciso(incisoContinuityId, 
					new DateTime(validOn), new DateTime(knownOn), reporte.getFechaHoraOcurrido());
			List<Cobertura> coberturas = new ArrayList<Cobertura>();
			if( coberturaSeccion != null && !coberturaSeccion.isEmpty() ){
				for(BitemporalCoberturaSeccion bitemporalCoberturaSeccion : coberturaSeccion){
					
					CoberturaDTO coberturaDto = entidadService.findById( CoberturaDTO.class, 
							bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO().getIdToCobertura()); 
					Cobertura cobertura = new Cobertura();
					cobertura.setId(coberturaDto.getIdToCobertura().longValue());
					cobertura.setNombre(coberturaDto.getNombreComercial());
					if(coberturaDto.getClaveTipoDeducible().equals("1")){
						cobertura.setDeducible(new BigDecimal( 
								bitemporalCoberturaSeccion.getValue().getPorcentajeDeducible()));
						cobertura.setDescripcionDeducible(bitemporalCoberturaSeccion.getValue().getDescripcionDeducible());
					} else if(!coberturaDto.getClaveTipoDeducible().equals("0") && !coberturaDto.getClaveTipoDeducible().equals("1")){
						cobertura.setDeducible( new BigDecimal(
								bitemporalCoberturaSeccion.getValue().getValorDeducible())) ;
						cobertura.setDescripcionDeducible(bitemporalCoberturaSeccion.getValue().getDescripcionDeducible());	
					}
				
					if(coberturaDto.getClaveTipoCalculo().equals("RCV")){
						cobertura.setLimiteMaxResponsabilidad(bitemporalCoberturaSeccion.getValue().getDiasSalarioMinimo()!=null? Double.toString(bitemporalCoberturaSeccion.getValue().getDiasSalarioMinimo()):"" + "UMA");
					} else if(coberturaDto.getClaveFuenteSumaAsegurada().equals("0")){
						cobertura.setLimiteMaxResponsabilidad(Double.toString(bitemporalCoberturaSeccion.getValue().getValorSumaAsegurada()));
					} else if(coberturaDto.getClaveFuenteSumaAsegurada().equals("1")){
						cobertura.setLimiteMaxResponsabilidad("Valor Comercial");
					} else if(coberturaDto.getClaveFuenteSumaAsegurada().equals("2")){
						cobertura.setLimiteMaxResponsabilidad(Double.toString(bitemporalCoberturaSeccion.getValue().getValorSumaAsegurada()));
					} else if(coberturaDto.getClaveFuenteSumaAsegurada().equals("9")){
						cobertura.setLimiteMaxResponsabilidad("Valor Convenido");
					} else if(coberturaDto.getClaveFuenteSumaAsegurada().equals("3")){
						cobertura.setLimiteMaxResponsabilidad("Amparada");
					}
					
					if (cobertura.getDescripcionDeducible() != null && cobertura.getDescripcionDeducible().indexOf(CoberturaServiceImpl.TIPO_VALOR_SA_DSMGVDF)>=0){
						cobertura.setDescripcionDeducible(cobertura.getDescripcionDeducible().replace(CoberturaServiceImpl.TIPO_VALOR_SA_DSMGVDF, CoberturaServiceImpl.TIPO_VALOR_SA_UMA));
					}
					
					coberturas.add(cobertura);
					
				}
			}
			poliza.setCoberturas(coberturas);
			List<Recibo> reciboPago = new ArrayList<Recibo>();
			poliza.setRecibos(reciboPago);
			poliza.setAlertas(getAlertas(idToPoliza, poliza.getNumeroSerie()));
		}
	
		return poliza;
	}
	
	/**
	 * Obtiene las alertas de un numero de serie y las acciones por numero de cotizacion
	 * @param idToPoliza Numero de cotizacion
	 * @param noSerie 
	 * @author Luis Ibarra
	 * @since 03/Dic/2015
	 * @return Lista de alertas para el numero de serie
	 */
	private List<Alertas> getAlertas(long idToPoliza, String noSerie){
		
		System.out.println("ObteniendoAcciones getAlertas Midas:::");
		
		List<Alertas> alertas = new ArrayList<Alertas>();
		
		
		PolizaDTO poliza  = entidadService.findByProperty(PolizaDTO.class, "idToPoliza", idToPoliza).get(0);
		Map<String, Object> param = new LinkedHashMap<String, Object>();
		param.put("vin", noSerie);
		param.put("idCotizacion", poliza.getCotizacionDTO().getIdToCotizacion());
		
		List<SapAmisAccionesRelacion> relaciones = entidadService.findByProperties(SapAmisAccionesRelacion.class, param);
		System.out.println("ObteniendoAcciones getAlertas relaciones:::" + relaciones.size());
		for(int i = 0; i<relaciones.size(); i++){
			System.out.println("ObteniendoAcciones getAlertas for relaciones:::" + i);
			String sistema = relaciones.get(i).getCatAlertasSapAmis().getCatSistemas().getDescCatSistemas();
			
			System.out.println("ObteniendoAcciones getAlertas for relaciones:::" + sistema);
			if(sistema != null && (sistema.equals("PPT") || sistema.equals("OCRA"))) {
				Alertas alerta = new Alertas();
				
				System.out.println("ObteniendoAcciones getAlertas for relaciones:::" + relaciones.get(i).getCatAlertasSapAmis().getDescCatAlertasSapAmis());
				System.out.println("ObteniendoAcciones getAlertas for relaciones:::" + relaciones.get(i).getCatAlertasSapAmis().getCatSistemas().getDescCatSistemas());
				alerta.setAlerta(relaciones.get(i).getCatAlertasSapAmis().getDescCatAlertasSapAmis());
				alerta.setSistema(relaciones.get(i).getCatAlertasSapAmis().getCatSistemas().getDescCatSistemas());
				
				List<SapAmisAcciones> acciones = entidadService.findByProperty(SapAmisAcciones.class, "sapAmisAccionesRelacion", relaciones.get(i));
				
				List<Acciones> accionesList = new ArrayList<Acciones>();
				System.out.println("ObteniendoAcciones getAlertas  esta alerta tiene acciones :::" +acciones.size());
				for(int j=0; j<acciones.size();j++){
					
					System.out.println("ObteniendoAcciones getAlertas for acciones :::" +j);
					System.out.println("ObteniendoAcciones getAlertas for acciones :::" +acciones.get(0).getDescripcionAccion());
					
					
					Acciones accion = new Acciones();
					accion.setAccion(acciones.get(0).getDescripcionAccion());

					accionesList.add(accion);
				}
				alerta.setAcciones(accionesList);
				alertas.add(alerta);
			}
		}
		return alertas;
	}
	

	@Override
	public void marcarTerminacion(MarcarTerminacionParameter parameter) {
		Long idReporteCabina = parameter.getReporteSiniestroId();
		reporteCabinaService.generarFechaHora(idReporteCabina, TIPO_FECHA_HORA.TERMINACION, "NO",null);
		notificacionClienteMovilService.notificarTerminoServicioSiniestro(idReporteCabina);
	}
	
	@SuppressWarnings("unchecked")
	public List<Tercero> getTerceros(CoberturaIncisoParameter parameter){
		MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
		sqlParameter
			.addValue("pidReporteid", parameter.getReporteSiniestroId())
			.addValue("pTipoEstimacion", parameter.getTipoEstimacion())
			.addValue("pidCobertura", parameter.getCoberturaId());
		
		Map<String, Object> execute = this.getTerceros.execute(sqlParameter);
		
		return (List<Tercero>) execute.get("return");
	}

	public String convertirReclamacion(ReporteSiniestroParameter reporteSiniestro, String claveUsuario){	
	
		String numSinietro = null;
		ReporteCabina reporte = new ReporteCabina();
		SiniestroCabinaDTO  sinDTO = new SiniestroCabinaDTO();
		SiniestroCabina siniestro = new SiniestroCabina();
		try {
			reporte = entidadService.findById(ReporteCabina.class, reporteSiniestro.getId());
			sinDTO.setReporteCabinaId(reporte.getId());
			sinDTO.setCurp(reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getCurp());
			sinDTO.setRfc(reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getRfc());
			sinDTO.setCausaSiniestro(reporte.getSeccionReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina().getCausaSiniestro());
			siniestro = siniestroCabinaService.convertirSiniestro(sinDTO);
	
		if (siniestro != null){
				numSinietro = siniestro.getClaveOficina() + "-" + siniestro.getConsecutivoReporte() + "-" + siniestro.getAnioReporte();
		}
		} catch (Exception e) {
			throw new ApplicationException(e.getMessage());
		}
		
		return numSinietro;
	}

	@Override
	public void guardaInfoGralReporte(ReporteSiniestroParameter parameter) {
		try {
			
			String declaracion = convertToUTF8(parameter.getDeclaracion());
			ReporteCabina reporte = entidadService.findById(ReporteCabina.class,
					parameter.getId());
			Boolean soloDeclaracion;
			if(reporte.getDeclaracionTexto() == null){
				soloDeclaracion = true;
			} else{
				soloDeclaracion = false;
			}
			reporteCabinaService.salvarDeclaracionSiniestro(parameter.getId(),
					declaracion);
			AutoIncisoReporteCabina autoInciso = siniestroCabinaService
					.obtenerAutoIncisoByReporteCabina(parameter.getId());
			if(autoInciso != null && !soloDeclaracion){
				// Tipo Inciso o bien
				SiniestroCabinaDTO siniestroCabina;
				
				if(autoInciso.getIncisoReporteCabina() != null 
						&& autoInciso.getIncisoReporteCabina().getAutoIncisoReporteCabina() != null){
					siniestroCabina = siniestroCabinaService.obtenerSiniestroCabina(parameter.getId());
					siniestroCabina.setColor(autoInciso.getColor());
				} else {
					siniestroCabina = new SiniestroCabinaDTO();
					siniestroCabina.setEsSiniestro(false);
				}			
				siniestroCabina.setReporteCabinaId(parameter.getId());
				siniestroCabina.setCausaSiniestro(parameter.getCodigoTipoSiniestro());
				siniestroCabina.setTerminoAjuste(parameter.getCodigoTerminoAjuste());
				siniestroCabina.setTipoResponsabilidad(parameter
						.getCodigoResponsabilidad());
				if(parameter.isConductorMismoAsegurado()){
					siniestroCabina.setCondMismoAsegurado("S");
					if(autoInciso.getPersonaAseguradoId() != null && parameter.getCurp()==null && parameter.getRfc()==null){
						ClienteGenericoDTO cliente = new ClienteGenericoDTO();
						cliente.setIdCliente(new BigDecimal(autoInciso.getPersonaAseguradoId()));
						try {
							cliente = clienteFacadeRemote.loadById(cliente);
							siniestroCabina.setRfc(cliente.getCodigoRFC());
							siniestroCabina.setCurp(cliente.getCodigoCURP());
						} catch (Exception e) {
							log.info("Excepcion clienteFacadeRemote loadById:" + e.getMessage());
						}
					} else {
					    siniestroCabina.setCurp(convertToUTF8(parameter.getCurp()));
						siniestroCabina.setRfc(convertToUTF8(parameter.getRfc()));
						}
				} else {
					siniestroCabina.setCondMismoAsegurado("N");
	  				siniestroCabina.setCurp(convertToUTF8(parameter.getCurp()));
					siniestroCabina.setRfc(convertToUTF8(parameter.getRfc()));
				}
				siniestroCabina.setConductor(convertToUTF8(parameter.getConductor()));
				siniestroCabina.setGenero(parameter.getSexo());
				siniestroCabina.setEdad(parameter.getEdad());
				siniestroCabina.setTelefono(parameter.getTelefonoAseg());
				siniestroCabina.setLadaTelefono(parameter.getLadaTelefonoAseg());
				siniestroCabina.setLadaCelular(parameter.getLadaCelular());
				siniestroCabina.setCelular(parameter.getCelular());
				// estado:
				//autoIncisoReporteCabina.setEstadoId(parameter.getEstadoNacimiento());
				siniestroCabina.setUnidadEquipoPesado(Boolean.toString(parameter
						.isParticipoUnidadEqPesado()));
				if(parameter.getCiaRespId() != null ){
					siniestroCabina.setCiaSeguros(Integer.toString(parameter
							.getCiaRespId()));
				}

				// lugar de ajuste no hay en midas
				siniestroCabina.setMontoRecuperado(parameter.getMontoRecuperado());
				siniestroCabina.setPlacas(convertToUTF8(parameter.getPlaca()));
				siniestroCabina.setColor(parameter.getColor());
				siniestroCabina.setTerminoSiniestro(parameter.getTerminoSiniestro());
				if(parameter.isSeFugoTerceroResp()){
					siniestroCabina.setFugoTerceroResponsable("S");
				} else{
					siniestroCabina.setFugoTerceroResponsable("N");
				}
				siniestroCabina.setTipoLicencia(parameter.getTipoLicencia());
				if(parameter.isRecibidaOrdenCia()){
					siniestroCabina.setRecibidaOrdenCia("S");
				} else{
					siniestroCabina.setRecibidaOrdenCia("N");
				}
				siniestroCabina.setOrigen(parameter.getOrigen());
				siniestroCabina.setIncisoCia(parameter.getIncisoCia());
				siniestroCabina.setFechaExpedicion(parameter.getFechaExpedicion());
				siniestroCabina.setFechaDictamen(parameter.getFechaDictamen());
				siniestroCabina.setFechaJuicio(parameter.getFechaJuicio());
				siniestroCabina.setMotivoRechazo(parameter.getMotivoRechazo());
				siniestroCabina.setRechazoDesistimiento(parameter.getRechazoDesistimiento());
				siniestroCabina.setObservacionRechazo(parameter.getObservacionRechazo());
				siniestroCabina.setMontoDanos(parameter.getMontoDanos());

				if(parameter.getPorcentajeParticipacion()!=null){
					siniestroCabina.setPorcentajeParticipacion(parameter.getPorcentajeParticipacion());
				}
				if(parameter.getPolizaCia()!=null && parameter.getPolizaCia().length()>0){
					siniestroCabina.setPolizaCia(parameter.getPolizaCia());
				}
				if(parameter.getSiniestroCia()!=null && parameter.getSiniestroCia().length()>0){
					siniestroCabina.setSiniestroCia(parameter.getSiniestroCia());
				}
				siniestroCabinaService.guardar(siniestroCabina);
			}
		} catch (NegocioEJBExeption e)	{
			log.info("Excepcion NegocioEJBExeption al Guardar:" + e.getMessage());
			StringBuilder msg = new StringBuilder("");
			for(Object o: e.getValues()){
				msg.append((String)o).append("\n");
			}
			throw new ApplicationException(msg.toString());
			
		} catch (Exception e) {
				
			if (e.getCause() != null && e.getCause() instanceof ConstraintViolationException) {
				log.info("Excepcion ConstraintViolationException al Guardar:" + e.getCause().getMessage());
				ConstraintViolationException constraintViolationException = 
					(ConstraintViolationException) e.getCause();
				StringBuilder error = new StringBuilder("");
				for (ConstraintViolation<?> constraintViolation : constraintViolationException
						.getConstraintViolations()) {
					String errorMessage = constraintViolation.getMessage();
					Path propertyPath = constraintViolation.getPropertyPath();
					String errorName = propertyPath.toString();
					error.append(errorName ).append(" " ).append(errorMessage ).append("\n");
				}
				throw new ApplicationException(error.toString());
			} else {
				throw new ApplicationException(e.getMessage());
			}
		}
	}
	
	@Override
	public void saveDocumentoSiniestro(SaveDocumentoSiniestroParameter parameter) {
		Set<ConstraintViolation<SaveDocumentoSiniestroParameter>> constraintViolations = validator.validate(parameter);
		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(new HashSet<ConstraintViolation<?>>(constraintViolations));
		}
		
		ErrorBuilder eb = new ErrorBuilder();
		if (parameter.getReporteSiniestroId() == null) {
			eb.addFieldError("reporteSiniestroId", "No existe el reporte de siniestro.");
		}
		
		TipoDocumentoSiniestro tipoDocumentoSiniestro = em.find(TipoDocumentoSiniestro.class, parameter.getTipoDocumentoSiniestroId());
		
		if (tipoDocumentoSiniestro == null) {
			eb.addFieldError("idTipoDocumentoSiniestro", "No existe el tipo de documento siniestro.");
		}
		
		if (eb.hasErrors()) {
			throw new ApplicationException(eb);
		}
		
		TransporteImpresionDTO transporteImpresionDTO = new TransporteImpresionDTO();
		byte[] fileBytes;
		try {
			fileBytes = FileUtils.readFileToByteArray(parameter.getFile());
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		transporteImpresionDTO.setByteArray(fileBytes);
		// Se crea el expediente en fortimax
	
		try { 		 	
			ProcesosFortimax proceso=archivosService.getProceso(PROCESO_FORTIMAX.valueOf(parameter.getCveProceso()));				
			CatalogoAplicacionFortimax ap= catApFortimaxService.loadById(proceso.getIdAplicacion());	
			/*GENERAR EXPEDIENTE */ 
			FortimaxDocument document = new FortimaxDocument ();
			document.setAplicacion(ap.getNombreAplicacion());// Nombre de la Gaveta
			document.setId(new Long (parameter.getReporteSiniestroId()));
			if(StringUtil.isEmpty(parameter.getFolioExpediente())){
				document.setFolio(parameter.getReporteSiniestroId().toString());
			}else {
				document.setFolio(parameter.getFolioExpediente());
			} 
			document.setFieldValues(null);
			//TODO
			synchronized(this){
				archivosService.generarExpediente(document);
			
			//Termina la creacion del expediente
				document = archivosService.generarDocumentoByProceso(PROCESO_FORTIMAX.valueOf(parameter.getCveProceso()), parameter.getReporteSiniestroId(), parameter.getFolio());
            }
			String documentoNombre;
			if(parameter.getCveProceso().equals("DOCS_AJUSTE")){
				String[] documentosNombre = tipoDocumentoSiniestro.getFortimaxDocumentoNombre().split(SEPARADOR);
				documentoNombre = documentosNombre[1]+ "." + FilenameUtils.getExtension(parameter.getFileFileName()); 
			} else {
				documentoNombre = document.getNombreArchivo() + "." + FilenameUtils.getExtension(parameter.getFileFileName());
			}
			String[] rutaDocumento = tipoDocumentoSiniestro.getFortimaxDocumentoDirectorio().split(SEPARADOR);
			log.info("documentoNombre:" + documentoNombre);
			log.info("Nombre de la aplicacion:" + ap.getNombreAplicacion());
			log.info("idReporteSiniestro:" + parameter.getReporteSiniestroId());
			log.info("ruta del documento:" + rutaDocumento[1]);
			String[] procesofortimax = fortimaxV2Service.uploadFile(documentoNombre, transporteImpresionDTO, ap.getNombreAplicacion(), new String[] {Long.toString(parameter.getReporteSiniestroId())}, 
					rutaDocumento[1]);
			log.info("Respuesta de fortimax:" + Arrays.toString(procesofortimax));
		}catch (Exception e) {					
			throw new RuntimeException(e);
		}  			
	}
	
	@Override
	public List<TipoDocumentoSiniestro> getTiposDocumentosSiniestro() {
		TypedQuery<TipoDocumentoSiniestro> query = em.createQuery("select m from TipoDocumentoSiniestro m order by m.id", TipoDocumentoSiniestro.class);
		return query.getResultList();
	}
	
	public String generarImpresion(TerceroParameter tercero, String tipoPase, String usuario){
        StringBuilder plantilla = new StringBuilder("");
        byte[] pase = null;
        InputStream is ;
        String line;
        String lineAux;

        try {
        	pase = getBytesPaseTxt(tercero, tipoPase, usuario);
            String textpase = new String(pase,"UTF-8");
            is = new ByteArrayInputStream(textpase.getBytes("UTF-8"));
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            plantilla.append(tipoPase + "-").append(tercero.getTipoEstimacion()!=null?tercero.getTipoEstimacion():"").append("\n");
            while ((line = br.readLine()) != null ) {
                //Se guarda la linea en la variable de linea axuliar para conservar el formato
                lineAux = line;
                line = line.trim();
                //Se eliminan los espacios en blanco para validar no se guarden lineas vacias
                if(!line.isEmpty()){
                    plantilla.append(lineAux).append("\n");
                }

            }
            br.close();
        } catch (UnsupportedEncodingException e1) {
            // TODO Auto-generated catch block
        	log.error(e1);
        } catch (IOException e) {
            log.error(e);
        }

        return plantilla.toString();

    } 
	
	@SuppressWarnings("unchecked")
	public byte[] getBytesPaseTxt(TerceroParameter tercero, String tipoPase, String usuario) {
		byte[] pdfPase = null;

		try {
			InputStream caratula = null;
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("usuario", usuario);
			parameters.put("tipo", tipoPase);
			parameters.put("idCobertura", tercero.getCoberturaId());
			JasperReport report = null;
			JRBeanCollectionDataSource jrDataSource = null;
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter
				.addValue("pidReporteid", tercero.getReporteSiniestroId())
				.addValue("pTipoPase", tipoPase)
				.addValue("pEstimacionId", tercero.getId())
				.addValue("pTipoEstimacion", tercero.getTipoEstimacion());
			
			if(tipoPase.equals("DDS")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeclaracionDeSiniestro.jrxml");
				InputStream terceros =  this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/Terceros.jrxml");
				JasperReport subreport = JasperCompileManager.compileReport(terceros);
				parameters.put("SUBREPORT_DIR", subreport);
				
				Map<String, Object> execute = getPaseDeclaracionSiniestro.execute(sqlParameter);
				jrDataSource =  new JRBeanCollectionDataSource( (List<DeclaracionSiniestro>) execute.get("pCursor"));
			} else if(tipoPase.equals("VDG")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseValeDeGrua.jrxml");
				Map<String, Object> execute = getValeDeGrua.execute(sqlParameter);
				jrDataSource = new JRBeanCollectionDataSource((List<ValeGrua>) execute.get("pCursor"));
			} else if(tipoPase.equals("PAT")){
				if(tercero.getTipoEstimacion().equalsIgnoreCase("DMA")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATallerAsegurado.jrxml");
					Map<String, Object> execute = getPaseAtencionATallerAsegurado.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionATallerAsegurado>) execute.get("pCursor"));
				}else if (tercero.getTipoEstimacion().equalsIgnoreCase("RCV")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATercero.jrxml");
					Map<String, Object> execute = getPaseAtencionATercero.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionATercero>) execute.get("pCursor"));
				}
			}else if(tipoPase.equals("RAC")){
				if(tercero.getTipoEstimacion().equalsIgnoreCase("RCV")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATercero.jrxml");
					Map<String, Object> execute = getPaseAtencionATercero.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionATercero>) execute.get("pCursor"));
				}else if (tercero.getTipoEstimacion().equalsIgnoreCase("GMC")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoConductor.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				}else if(tercero.getTipoEstimacion().equalsIgnoreCase("RCP")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoTerceroPersona.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				}else if (tercero.getTipoEstimacion().equalsIgnoreCase("RCJ")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoViajero.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				}			
			}else if(tipoPase.equals("PDA")){
				if(tercero.getTipoEstimacion().equalsIgnoreCase("DMA")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionParaPagoDeDanios.jrxml");
					Map<String, Object> execute = getAtencionParaPagosDeDanios.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionParaPagosDeDanios>) execute.get("pCursor"));
				} else if(tercero.getTipoEstimacion().equalsIgnoreCase("RCV")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionParaPagoDeDanios.jrxml");
					Map<String, Object> execute = getAtencionParaPagosDeDaniosVehiculo.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionParaPagosDeDanios>) execute.get("pCursor"));
				} else if(tercero.getTipoEstimacion().equalsIgnoreCase("RCB")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionParaPagoDeDanios.jrxml");
					Map<String, Object> execute = getAtencionParaPagosDeDaniosBienes.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionParaPagosDeDanios>) execute.get("pCursor"));
				}
			} else if(tipoPase.equals("GMO") ){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
				Map<String, Object> execute = getPaseServicioMedicoOcupantes.execute(sqlParameter);
				jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
			} else if(tipoPase.equals("GMT") ){
				if(tercero.getTipoEstimacion().equalsIgnoreCase("GMC")){ 
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoConductor.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				} else if(tercero.getTipoEstimacion().equalsIgnoreCase("RCP")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoTerceroPersona.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				} else if (tercero.getTipoEstimacion().equalsIgnoreCase("RCJ")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoViajero.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				}
			} else if(tipoPase.equals("OBC")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioObraCivil.jrxml");
				Map<String, Object> execute = getPaseServicioObraCivil.execute(sqlParameter);
				jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioObraCivil>) execute.get("pCursor"));
			} 	

			report = JasperCompileManager.compileReport(caratula);
			GestorReportes gestorReportes = new GestorReportes();
			pdfPase = gestorReportes.generaReporte(ConstantesReporte.TIPO_TXT_MOVIL, report, parameters, jrDataSource);

		} catch (JRException ex) {
			log.error("Falla jasperreport opcion: " + tipoPase , ex);
			throw new RuntimeException(ex);
		}
		return pdfPase;
	}
	
	@SuppressWarnings("unchecked")
	public byte[] getBytesPase(TerceroParameter tercero, String tipoPase, String usuario) {
		byte[] pdfPase = null;

		try {
			InputStream caratula = null;
			Map<String, Object> parameters = new HashMap<String, Object>();
			parameters.put("usuario", usuario);
			parameters.put("tipo", tipoPase);
			parameters.put("idCobertura", tercero.getCoberturaId());
			JasperReport report = null;
			JRBeanCollectionDataSource jrDataSource = null;
			MapSqlParameterSource sqlParameter = new MapSqlParameterSource();
			sqlParameter
				.addValue("pidReporteid", tercero.getReporteSiniestroId())
				.addValue("pTipoPase", tipoPase)
				.addValue("pEstimacionId", tercero.getId())
				.addValue("pTipoEstimacion", tercero.getTipoEstimacion());
			
			if(tipoPase.equals("DDS")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeclaracionDeSiniestro.jrxml");
				InputStream terceros =  this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/Terceros.jrxml");
				JasperReport subreport = JasperCompileManager.compileReport(terceros);
				parameters.put("SUBREPORT_DIR", subreport);
				
				Map<String, Object> execute = getPaseDeclaracionSiniestro.execute(sqlParameter);
				jrDataSource =  new JRBeanCollectionDataSource( (List<DeclaracionSiniestro>) execute.get("pCursor"));
			} else if(tipoPase.equals("VDG")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseValeDeGrua.jrxml");
				Map<String, Object> execute = getValeDeGrua.execute(sqlParameter);
				jrDataSource = new JRBeanCollectionDataSource((List<ValeGrua>) execute.get("pCursor"));
			} else if(tipoPase.equals("PAT")){
				if(tercero.getTipoEstimacion().equalsIgnoreCase("DMA")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATallerAsegurado.jrxml");
					Map<String, Object> execute = getPaseAtencionATallerAsegurado.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionATallerAsegurado>) execute.get("pCursor"));
				}else if (tercero.getTipoEstimacion().equalsIgnoreCase("RCV")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATercero.jrxml");
					Map<String, Object> execute = getPaseAtencionATercero.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionATercero>) execute.get("pCursor"));
				}
			}else if(tipoPase.equals("RAC")){
				if(tercero.getTipoEstimacion().equalsIgnoreCase("RCV")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionATercero.jrxml");
					Map<String, Object> execute = getPaseAtencionATercero.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionATercero>) execute.get("pCursor"));
				}else if (tercero.getTipoEstimacion().equalsIgnoreCase("GMC")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoConductor.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				}else if(tercero.getTipoEstimacion().equalsIgnoreCase("RCP")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoTerceroPersona.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				}else if (tercero.getTipoEstimacion().equalsIgnoreCase("RCJ")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoViajero.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				}			
			}else if(tipoPase.equals("PDA")){
				if(tercero.getTipoEstimacion().equalsIgnoreCase("DMA")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionParaPagoDeDanios.jrxml");
					Map<String, Object> execute = getAtencionParaPagosDeDanios.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionParaPagosDeDanios>) execute.get("pCursor"));
				} else if(tercero.getTipoEstimacion().equalsIgnoreCase("RCV")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionParaPagoDeDanios.jrxml");
					Map<String, Object> execute = getAtencionParaPagosDeDaniosVehiculo.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionParaPagosDeDanios>) execute.get("pCursor"));
				} else if(tercero.getTipoEstimacion().equalsIgnoreCase("RCB")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeAtencionParaPagoDeDanios.jrxml");
					Map<String, Object> execute = getAtencionParaPagosDeDaniosBienes.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<AtencionParaPagosDeDanios>) execute.get("pCursor"));
				}
			} else if(tipoPase.equals("GMO") ){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
				Map<String, Object> execute = getPaseServicioMedicoOcupantes.execute(sqlParameter);
				jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
			} else if(tipoPase.equals("GMT") ){
				if(tercero.getTipoEstimacion().equalsIgnoreCase("GMC")){ 
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoConductor.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				} else if(tercero.getTipoEstimacion().equalsIgnoreCase("RCP")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoTerceroPersona.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				} else if (tercero.getTipoEstimacion().equalsIgnoreCase("RCJ")){
					caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioMedico.jrxml");
					Map<String, Object> execute = getPaseServicioMedicoViajero.execute(sqlParameter);
					jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioMedico>) execute.get("pCursor"));
				}
			} else if(tipoPase.equals("OBC")){
				caratula = this.getClass().getResourceAsStream("/mx/com/afirme/midas2/service/impl/movil/ajustador/jrxml/PaseDeServicioObraCivil.jrxml");
				Map<String, Object> execute = getPaseServicioObraCivil.execute(sqlParameter);
				jrDataSource = new JRBeanCollectionDataSource((List<PaseServicioObraCivil>) execute.get("pCursor"));
			} 	

			report = JasperCompileManager.compileReport(caratula);
			pdfPase = JasperRunManager.runReportToPdf(report,parameters, jrDataSource);
			
		} catch (JRException ex) {
			log.error("Falla jasperreport opcion: " + tipoPase , ex);
			throw new RuntimeException(ex);
		}
		return pdfPase;
	}
	
	private List<Tercero> getTerceros(Long id){
		SqlParameterSource parameter = new MapSqlParameterSource(
				"pidReporteid", (id));
		Map<String, Object> execute = getTercerosReporte.execute(parameter);
		@SuppressWarnings("unchecked")
		List<Tercero> terceroList = (List<Tercero>) execute.get("pCursor");
		return terceroList;
	}
	public Object getTercero(TerceroParameter parameter){
		Object result = null;
		EstimacionCoberturaSiniestroDTO estimacionCobSin = new EstimacionCoberturaSiniestroDTO();
		String paseAtencion				= "";
		BigDecimal montoDeducible = BigDecimal.ZERO;
				
		if(parameter.getTipoEstimacion().equals("DMA")
				||parameter.getTipoEstimacion().equals("ESD")){
			
			estimacionCobSin = estimacionCoberturaSiniestroService.obtenerDatosEstimacionCobertura(
					parameter.getCoberturaId().longValue(), 
					parameter.getTipoCalculo(), parameter.getTipoEstimacion());			
		}else{
			EstimacionCoberturaReporteCabina estimacion  = new EstimacionCoberturaReporteCabina(); 
			if(parameter.getId() != null){
				estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, parameter.getId());	
				estimacionCobSin = estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);	
			}else{
				estimacionCobSin = estimacionCoberturaSiniestroService.obtenerDatosEstimacionCoberturaNueva(
						parameter.getCoberturaId().longValue(), parameter.getTipoEstimacion());
			}
		}
		if (EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES,
				parameter.getTipoEstimacion())) {
			Vehiculo tercero = new Vehiculo();
			paseAtencion = "DMA";
			tercero.setTipo("ASEG");
			tercero.setTipoEstimacion(parameter.getTipoEstimacion());
			tercero.setTipoCalculo(parameter.getTipoCalculo());	
			tercero.setCoberturaId(parameter.getCoberturaId());
			tercero.setReporteSiniestroId(parameter.getReporteSiniestroId());
			if(parameter.getId()!= null && parameter.getId()>0){
				tercero.setId(parameter.getId());
				tercero.setFolio(estimacionCobSin.getEstimacionDanosMateriales().getFolio());
			}
			tercero.setNombre(estimacionCobSin.getEstimacionDanosMateriales().getNombreAfectado());
			tercero.setContacto(estimacionCobSin.getEstimacionDanosMateriales().getNombrePersonaContacto());
			tercero.setTelefono(estimacionCobSin.getEstimacionDanosMateriales().getTelContacto());
			tercero.setCorreo(estimacionCobSin.getEstimacionDanosMateriales().getEmail());
			if(estimacionCobSin.getDatosEstimacion().getEstimacionActual()!= BigDecimal.ZERO){
				tercero.setEstimacion(estimacionCobSin.getDatosEstimacion().getEstimacionActual());
			}			
			tercero.setEstimacionAnterior(estimacionCobSin.getDatosEstimacion().getReserva());
			tercero.setAplicaDeducible(estimacionCobSin.getEstimacionDanosMateriales().getAplicaDeducible());
			tercero.setObservaciones(estimacionCobSin.getEstimacionDanosMateriales().getObservaciones());
			tercero.setDescripcion(estimacionCobSin
					.getEstimacionDanosMateriales().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getSeccionReporteCabina()
					.getReporteCabina().getDeclaracionTexto());
			tercero.setTipoPase(estimacionCobSin.getEstimacionDanosMateriales()
					.getTipoPaseAtencion());
			if (estimacionCobSin.getMarca() != null) {
				tercero.setMarcaId(estimacionCobSin.getMarca());
			} else {
				tercero.setMarcaId(estimacionCobSin.getEstimacionDanosMateriales()
						.getCoberturaReporteCabina().getIncisoReporteCabina()
						.getAutoIncisoReporteCabina().getMarcaId().toString());
			}
			tercero.setModelo(new Integer(estimacionCobSin
					.getEstimacionDanosMateriales().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina()
					.getModeloVehiculo()));
			tercero.setDescripcionTipoEstilo(estimacionCobSin
					.getEstimacionDanosMateriales().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina()
					.getDescEstilo());
			tercero.setSerie(estimacionCobSin.getEstimacionDanosMateriales()
					.getCoberturaReporteCabina().getIncisoReporteCabina()
					.getAutoIncisoReporteCabina().getNumeroSerie());
			if (estimacionCobSin.getEstimacionDanosMateriales().getPlacas() != null) {
				tercero.setPlaca(estimacionCobSin.getEstimacionDanosMateriales().getPlacas());
			} else {
				tercero.setPlaca(estimacionCobSin.getEstimacionDanosMateriales()
						.getCoberturaReporteCabina().getIncisoReporteCabina()
						.getAutoIncisoReporteCabina().getPlaca());
			}
			tercero.setColor(estimacionCobSin.getEstimacionDanosMateriales()
					.getCoberturaReporteCabina().getIncisoReporteCabina()
					.getAutoIncisoReporteCabina().getColor());
			if (estimacionCobSin.getEstimacionDanosMateriales().getTaller() != null) {
				tercero.setTallerId(Integer.toString(estimacionCobSin
						.getEstimacionDanosMateriales().getTaller().getId()));
			}
			if(estimacionCobSin.getEstimacionDanosMateriales().getDanosCubiertos() != null){
			tercero.setAreasDanadas(estimacionCobSin
					.getEstimacionDanosMateriales().getDanosCubiertos());
			}
			if (estimacionCobSin.getEstimacionDanosMateriales().getDanosPreexistentes() != null) {
				tercero.setDanosPreexistentes(estimacionCobSin.getEstimacionDanosMateriales().getDanosPreexistentes());
			}
			if( estimacionCobSin.getEstimacionCoberturaReporte().getCoberturaReporteCabina().getClaveTipoDeducible() != null &&  estimacionCobSin.getEstimacionCoberturaReporte().getCoberturaReporteCabina().getClaveTipoDeducible() == 1 ){
				if(estimacionCobSin.getEstimacionCoberturaReporte().getCoberturaReporteCabina().getIncisoReporteCabina()
						.getAutoIncisoReporteCabina().getCausaSiniestro().equalsIgnoreCase(CausaSiniestro.CRISTALES.getValue()))
				{
					tercero.setPctDeducible(recuperacionDeducibleService.obtenerPorcentajeDeducible());
				}
			}
			montoDeducible = recuperacionDeducibleService.calcularDeducible(estimacionCobSin
					.getEstimacionDanosMateriales().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getSeccionReporteCabina()
					.getReporteCabina(), paseAtencion);
			tercero.setPorcentajeDeducibleValorComercial(recuperacionDeducibleService.obtenerPorcentajeDeducibleValorComercial());	
			
			if (estimacionCobSin.getEstimacionDanosMateriales().getTieneCompaniaSeguros() != null) {
				tercero.setTieneCia(estimacionCobSin.getEstimacionDanosMateriales().getTieneCompaniaSeguros());
			}
			if (estimacionCobSin.getEstimacionDanosMateriales().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros() != null) {
				tercero.setCiaId(new Integer (estimacionCobSin.getEstimacionDanosMateriales().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros()));
			} else if(estimacionCobSin.getEstimacionDanosMateriales().getCompaniaSegurosTercero() != null){
				tercero.setCiaId( estimacionCobSin.getEstimacionDanosMateriales().getCompaniaSegurosTercero().getId() );
			}
			if (estimacionCobSin.getEstimacionDanosMateriales().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getSiniestroCia() != null) {
				tercero.setSiniestroCia( estimacionCobSin.getEstimacionDanosMateriales().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getSiniestroCia());
			}
		
			if (estimacionCobSin.getEstimacionDanosMateriales().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1 ) {
				tercero.setRecibeOrdenCia(true);
			} else {
				tercero.setRecibeOrdenCia(false);
			}
			String fuenteSumaAsegurada = estimacionCobSin.getEstimacionDanosMateriales().getCoberturaReporteCabina()
					.getCoberturaDTO().getClaveFuenteSumaAsegurada();
			tercero.setFuenteSumaAsegurada(fuenteSumaAsegurada);
			if ((fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_COMERCIAL)
					|| fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA)
					|| fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CONVENIDO)) 
					&& estimacionCobSin.getEstimacionDanosMateriales().getSumaAseguradaObtenida() == null) {
				tercero.setSumaAmparada(new BigDecimal(estimacionCobSin.getEstimacionDanosMateriales()
						.getCoberturaReporteCabina().getValorSumaAsegurada()));
			} else if (fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO)) {
				if (tercero.getId() == null && (tercero.getSerie() != null && !tercero.getSerie().isEmpty())) {
					Double valorSumaAsegurada = estimacionCoberturaSinServices.obtieneValorSumaAsegurada(estimacionCobSin.getEstimacionDanosMateriales().getCoberturaReporteCabina(), 
							estimacionCobSin.getEstimacionCoberturaReporte().getCoberturaReporteCabina().getIncisoReporteCabina().getAutoIncisoReporteCabina());
					if (valorSumaAsegurada != null) {
						tercero.setSumaAmparada(new BigDecimal(valorSumaAsegurada));
						tercero.setConsultaSumaAsegurada(true);
					}else{
						tercero.setSumaAmparada(new BigDecimal(estimacionCobSin.getEstimacionDanosMateriales()
						.getCoberturaReporteCabina().getValorSumaAsegurada()));
					}
				}
			} else if(estimacionCobSin.getEstimacionDanosMateriales().getSumaAseguradaObtenida() != null){
				tercero.setSumaAmparada(estimacionCobSin.getEstimacionDanosMateriales().getSumaAseguradaObtenida());
			}
			
			if (estimacionCobSin.getEstimacionDanosMateriales().getEstatus() == null || 
					estimacionCobSin.getEstimacionDanosMateriales().getEstatus().equals("")) {
				estimacionCobSin.getEstimacionDanosMateriales().setEstatus( "PEN");
				tercero.setEstatus(estimacionCobSin.getEstimacionDanosMateriales().getEstatus());
			}
			if(estimacionCobSin.getEstimacionDanosMateriales().getEstado() != null){
				tercero.setIdEstado(estimacionCobSin.getEstimacionDanosMateriales().getEstado().getId());
			}
			tercero.setMotivoNoAplicaDeducibleSel(estimacionCobSin.getMotivoNoAplicaDeducible());
			if(parameter.getId() == null){
				tercero.setDeducible(recuperacionDeducibleService.calcularDeducible(estimacionCobSin.getReporteCabina(), paseAtencion));	
				tercero.setDeducibleCalculado(recuperacionDeducibleService.calcularDeducible(estimacionCobSin.getReporteCabina(), paseAtencion));
			} else {
				tercero.setDeducible(estimacionCobSin.getMontoDeducible());	
				tercero.setDeducibleCalculado(estimacionCobSin.getMontoDeducibleCalculado());
			}
			if(estimacionCobSin.getEstimacionDanosMateriales().getTieneDanosPreexistentes()==null){
				tercero.setTieneDanosPreexistentes(false);
			}else{
				tercero.setTieneDanosPreexistentes(estimacionCobSin.getEstimacionDanosMateriales().getTieneDanosPreexistentes());
			}
			tercero.setCircunstancia(estimacionCobSin.getEstimacionDanosMateriales().getCircunstancia());
			tercero.setOtraCircunstancia(estimacionCobSin.getEstimacionDanosMateriales().getOtraCircunstancia());
			tercero.setEsOtraCircunstancia(estimacionCobSin.getEstimacionDanosMateriales().getEsOtraCircunstancia());
			tercero.setDua(estimacionCobSin.getEstimacionDanosMateriales().getDua());
			tercero.setTipoTransporte(estimacionCobSin.getEstimacionDanosMateriales().getTipoTransporte());
			result = tercero;
		}else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, parameter.getTipoEstimacion())) {
			Persona tercero = new Persona();
			tercero.setTipo("PERS_ASEG");
			tercero.setTipoEstimacion(parameter.getTipoEstimacion());
			tercero.setTipoCalculo(parameter.getTipoCalculo());	
			tercero.setCoberturaId(parameter.getCoberturaId());
			tercero.setReporteSiniestroId(parameter.getReporteSiniestroId());
			if(parameter.getId()!= null && parameter.getId()>0){
				tercero.setId(parameter.getId());
				tercero.setFolio(estimacionCobSin.getEstimacionGastosMedicos().getFolio());
			}
			tercero.setNombre(estimacionCobSin.getEstimacionGastosMedicos().getNombreAfectado());
			tercero.setContacto(estimacionCobSin.getEstimacionGastosMedicos().getNombrePersonaContacto());
			tercero.setTelefono(estimacionCobSin.getEstimacionGastosMedicos().getTelContacto());
			tercero.setCorreo(estimacionCobSin.getEstimacionGastosMedicos().getEmail());
			if(estimacionCobSin.getDatosEstimacion().getEstimacionActual()!= BigDecimal.ZERO){
				tercero.setEstimacion(estimacionCobSin.getDatosEstimacion().getEstimacionActual());
			}	
			tercero.setEstimacionAnterior(estimacionCobSin.getDatosEstimacion().getReserva());
			tercero.setAplicaDeducible(estimacionCobSin.getEstimacionGastosMedicos().getAplicaDeducible());
			tercero.setDescripcion(estimacionCobSin.getEstimacionGastosMedicos().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getDeclaracionTexto());
			tercero.setSumaAmparada(estimacionCobSin.getDatosEstimacion().getSumaAseguradaAmparada());
			tercero.setTipoPase(estimacionCobSin.getEstimacionGastosMedicos().getTipoPaseAtencion());
			tercero.setTipoAtencion(estimacionCobSin.getEstimacionGastosMedicos().getTipoAtencion()); 
			if(estimacionCobSin.getEstimacionGastosMedicos().getEstado() != null && estimacionCobSin.getEstimacionGastosMedicos().getEstado() .equals("HOM")){
				tercero.setHomicidio(true);				
			}else{
				tercero.setHomicidio(false);
			}
			if(estimacionCobSin.getEstimacionGastosMedicos().getHospital() != null){
				tercero.setHospitalId(Long.toString(estimacionCobSin.getEstimacionGastosMedicos().getHospital().getId()));	
			}
			if(estimacionCobSin.getEstimacionGastosMedicos().getMedico() != null){
				tercero.setMedicoId(Integer.toString(estimacionCobSin.getEstimacionGastosMedicos().getMedico().getId()));	
			}
			if(estimacionCobSin.getCompaniaOrdenId() != null){
				tercero.setNumOrdenAtencion(estimacionCobSin.getCompaniaOrdenId().toString());
			}
			if(estimacionCobSin.getEstimacionGastosMedicos().getDescripcion() != null){
				tercero.setLesiones(estimacionCobSin.getEstimacionGastosMedicos().getDescripcion());	
			}
			if(estimacionCobSin.getEstimacionGastosMedicos().getEdad() != null){
				tercero.setEdad(estimacionCobSin.getEstimacionGastosMedicos().getEdad());	
			}
			if(estimacionCobSin.getEstimacionGastosMedicos().getTieneCompaniaSeguros() != null){
				tercero.setTieneCia(estimacionCobSin.getEstimacionGastosMedicosConductor().getTieneCompaniaSeguros());
			}
			if (estimacionCobSin.getEstimacionGastosMedicos().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1 && estimacionCobSin.getEstimacionGastosMedicos().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros() != null) {
				tercero.setCiaId(new Integer (estimacionCobSin.getEstimacionGastosMedicos().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros()));
			} else if(estimacionCobSin.getEstimacionGastosMedicos().getCompaniaSegurosTercero() != null){
				tercero.setCiaId( estimacionCobSin.getEstimacionGastosMedicos().getCompaniaSegurosTercero().getId() );
				tercero.setSiniestroCia(estimacionCobSin.getEstimacionGastosMedicos().getNumeroSiniestroTercero() );
			}
			
			if (estimacionCobSin.getEstimacionGastosMedicos().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1 ) {
				tercero.setRecibeOrdenCia(true);
			}else {
				tercero.setRecibeOrdenCia(false);
			}

			result = tercero;
		}else if (EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, parameter.getTipoEstimacion())) {
			Persona tercero = new Persona();
			tercero.setTipoEstimacion(parameter.getTipoEstimacion());
			tercero.setTipoCalculo(parameter.getTipoCalculo());	
			tercero.setCoberturaId(parameter.getCoberturaId());
			tercero.setReporteSiniestroId(parameter.getReporteSiniestroId());
			if(parameter.getId()!= null && parameter.getId()>0){
				tercero.setId(parameter.getId());
				tercero.setFolio(estimacionCobSin.getEstimacionGastosMedicosConductor().getFolio());
			}
			tercero.setNombre(estimacionCobSin.getEstimacionGastosMedicosConductor().getNombreAfectado());
			tercero.setContacto(estimacionCobSin.getEstimacionGastosMedicosConductor().getNombrePersonaContacto());
			tercero.setTelefono(estimacionCobSin.getEstimacionGastosMedicosConductor().getTelContacto());
			tercero.setCorreo(estimacionCobSin.getEstimacionGastosMedicosConductor().getEmail());
			if(estimacionCobSin.getDatosEstimacion().getEstimacionActual()!= BigDecimal.ZERO){
				tercero.setEstimacion(estimacionCobSin.getDatosEstimacion().getEstimacionActual());
			}	
			tercero.setEstimacionAnterior(estimacionCobSin.getDatosEstimacion().getReserva());
			tercero.setAplicaDeducible(estimacionCobSin.getEstimacionGastosMedicosConductor().getAplicaDeducible());
			tercero.setDescripcion(estimacionCobSin.getEstimacionGastosMedicosConductor().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getDeclaracionTexto());
			tercero.setSumaAmparada(estimacionCobSin.getDatosEstimacion().getSumaAseguradaAmparada());
			tercero.setTipoPase(estimacionCobSin.getEstimacionGastosMedicosConductor().getTipoPaseAtencion());
			tercero.setTipoAtencion(estimacionCobSin.getEstimacionGastosMedicosConductor().getTipoAtencion());
			if(estimacionCobSin.getEstimacionGastosMedicosConductor().getEstado() != null && estimacionCobSin.getEstimacionGastosMedicosConductor().getEstado().equalsIgnoreCase("HOM")){
				tercero.setHomicidio(true);				
			}else{
				tercero.setHomicidio(false);
			}
			if(estimacionCobSin.getEstimacionGastosMedicosConductor().getHospital() != null){
				tercero.setHospitalId(Long.toString(estimacionCobSin.getEstimacionGastosMedicosConductor().getHospital().getId()));	
			}
			if(estimacionCobSin.getEstimacionGastosMedicosConductor().getMedico() != null){
				tercero.setMedicoId(Integer.toString(estimacionCobSin.getEstimacionGastosMedicosConductor().getMedico().getId()));	
			}
			if(estimacionCobSin.getCompaniaOrdenId() != null){
				tercero.setNumOrdenAtencion(estimacionCobSin.getCompaniaOrdenId().toString());
			}
			if(estimacionCobSin.getEstimacionGastosMedicosConductor().getDescripcion() != null){
				tercero.setLesiones(estimacionCobSin.getEstimacionGastosMedicosConductor().getDescripcion());	
			}
			if(estimacionCobSin.getEstimacionGastosMedicosConductor().getEdad() !=null){
				tercero.setEdad(estimacionCobSin.getEstimacionGastosMedicosConductor().getEdad());	
			}
			if(estimacionCobSin.getEstimacionGastosMedicosConductor().getTieneCompaniaSeguros() != null){
				tercero.setTieneCia(estimacionCobSin.getEstimacionGastosMedicosConductor().getTieneCompaniaSeguros());
			}
			if (parameter.getId()== null && estimacionCobSin.getEstimacionGastosMedicosConductor().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1 && estimacionCobSin.getEstimacionGastosMedicosConductor().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros() != null) {
				tercero.setCiaId(new Integer (estimacionCobSin.getEstimacionGastosMedicosConductor().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros()));
				tercero.setSiniestroCia(estimacionCobSin.getEstimacionGastosMedicosConductor().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getSiniestroCia());
			} else if(estimacionCobSin.getEstimacionGastosMedicosConductor().getCompaniaSegurosTercero() != null){
				tercero.setCiaId( estimacionCobSin.getEstimacionGastosMedicosConductor().getCompaniaSegurosTercero().getId() );
				tercero.setSiniestroCia(estimacionCobSin.getEstimacionGastosMedicosConductor().getNumeroSiniestroTercero() );
			}
			
			if (estimacionCobSin.getEstimacionGastosMedicosConductor().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1 ) {
				tercero.setRecibeOrdenCia(true);
			}else {
				tercero.setRecibeOrdenCia(false);
			}

			result = tercero;
		}else if (EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, parameter.getTipoEstimacion())) {
			Bien tercero = new Bien();
			tercero.setTipoEstimacion(parameter.getTipoEstimacion());
			tercero.setTipoCalculo(parameter.getTipoCalculo());	
			tercero.setCoberturaId(parameter.getCoberturaId());
			tercero.setReporteSiniestroId(parameter.getReporteSiniestroId());
			if(parameter.getId()!= null && parameter.getId()>0){
				tercero.setId(parameter.getId());
				tercero.setFolio(estimacionCobSin.getEstimacionBienes().getFolio());
			}
			tercero.setNombre(estimacionCobSin.getEstimacionBienes().getNombreAfectado());
			tercero.setContacto(estimacionCobSin.getEstimacionBienes().getNombrePersonaContacto());
			tercero.setTelefono(estimacionCobSin.getEstimacionBienes().getTelContacto());
			tercero.setCorreo(estimacionCobSin.getEstimacionBienes().getEmail());
			if(estimacionCobSin.getDatosEstimacion().getEstimacionActual()!= BigDecimal.ZERO){
				tercero.setEstimacion(estimacionCobSin.getDatosEstimacion().getEstimacionActual());
			}	
			tercero.setEstimacionAnterior(estimacionCobSin.getDatosEstimacion().getReserva());
			tercero.setAplicaDeducible(estimacionCobSin.getEstimacionBienes().getAplicaDeducible());
			tercero.setObservaciones(estimacionCobSin.getEstimacionBienes().getDanosPreexistentes());
			tercero.setSumaAmparada(estimacionCobSin.getDatosEstimacion().getSumaAseguradaAmparada());
			tercero.setTipoPase(estimacionCobSin.getEstimacionBienes().getTipoPaseAtencion());
			tercero.setAreasDanadas(estimacionCobSin.getEstimacionBienes().getObservacion());
			tercero.setTipoBien(estimacionCobSin.getEstimacionBienes().getTipoBien());
			if(estimacionCobSin.getEstimacionBienes().getResponsableReparacion() != null){
				tercero.setIngeniero(estimacionCobSin.getEstimacionBienes().getResponsableReparacion().getNombrePersona());	
				tercero.setIdIngeniero(estimacionCobSin.getEstimacionBienes().getResponsableReparacion().getId());
			}
			if(estimacionCobSin.getEstimacionBienes().getPais() != null){
				tercero.setPaisId(estimacionCobSin.getEstimacionBienes().getPais().getId());	
			}
			if(estimacionCobSin.getEstimacionBienes().getEstado() != null){
				tercero.setEstadoId(estimacionCobSin.getEstimacionBienes().getEstado().getId());	
			}
			if(estimacionCobSin.getEstimacionBienes().getMunicipio() != null){
				tercero.setCiudadId(estimacionCobSin.getEstimacionBienes().getMunicipio().getId());	
			}
			if(estimacionCobSin.getEstimacionBienes().getUbicacion() != null){
				tercero.setReferencia(estimacionCobSin.getEstimacionBienes().getUbicacion());
			}
			result = tercero;
		}else if (EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, parameter.getTipoEstimacion())) {
			Persona tercero = new Persona();
			tercero.setTipoEstimacion(parameter.getTipoEstimacion());
			tercero.setTipoCalculo(parameter.getTipoCalculo());	
			tercero.setCoberturaId(parameter.getCoberturaId());
			tercero.setReporteSiniestroId(parameter.getReporteSiniestroId());
			if(parameter.getId()!= null && parameter.getId()>0){
				tercero.setId(parameter.getId());
				tercero.setFolio(estimacionCobSin.getEstimacionPersonas().getFolio());
			}
			tercero.setNombre(estimacionCobSin.getEstimacionPersonas().getNombreAfectado());
			tercero.setContacto(estimacionCobSin.getEstimacionPersonas().getNombrePersonaContacto());
			tercero.setTelefono(estimacionCobSin.getEstimacionPersonas().getTelContacto());
			tercero.setCorreo(estimacionCobSin.getEstimacionPersonas().getEmail());
			if(estimacionCobSin.getDatosEstimacion().getEstimacionActual()!= BigDecimal.ZERO){
				tercero.setEstimacion(estimacionCobSin.getDatosEstimacion().getEstimacionActual());
			}	
			tercero.setEstimacionAnterior(estimacionCobSin.getDatosEstimacion().getReserva());
			tercero.setAplicaDeducible(estimacionCobSin.getEstimacionPersonas().getAplicaDeducible());
			tercero.setDescripcion(estimacionCobSin.getEstimacionPersonas().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getDeclaracionTexto());
			tercero.setSumaAmparada(estimacionCobSin.getDatosEstimacion().getSumaAseguradaAmparada());
			tercero.setTipoPase(estimacionCobSin.getEstimacionPersonas().getTipoPaseAtencion());
			tercero.setTipoAtencion(estimacionCobSin.getEstimacionPersonas().getTipoAtencion());
			if(estimacionCobSin.getEstimacionPersonas().getEstado() != null && estimacionCobSin.getEstimacionPersonas().getEstado().equalsIgnoreCase("HOM")){
				tercero.setHomicidio(true);				
			}else{
				tercero.setHomicidio(false);
			}
			if(estimacionCobSin.getEstimacionPersonas().getHospital() != null){
				tercero.setHospitalId(Long.toString(estimacionCobSin.getEstimacionPersonas().getHospital().getId()));	
			}
			if(estimacionCobSin.getEstimacionPersonas().getMedico() != null){
				tercero.setMedicoId(Integer.toString(estimacionCobSin.getEstimacionPersonas().getMedico().getId()));
			}
			if(estimacionCobSin.getCompaniaOrdenId() != null){
				tercero.setNumOrdenAtencion(estimacionCobSin.getCompaniaOrdenId().toString());
			}
			if(estimacionCobSin.getEstimacionPersonas().getDescripcion() != null){
				tercero.setLesiones(estimacionCobSin.getEstimacionPersonas().getDescripcion());	
			}
			if(estimacionCobSin.getEstimacionPersonas().getEdad()!= null){
				tercero.setEdad(estimacionCobSin.getEstimacionPersonas().getEdad());	
			}
			
			if(estimacionCobSin.getEstimacionPersonas().getCompaniaSegurosTercero() != null){
				tercero.setCiaId( estimacionCobSin.getEstimacionPersonas().getCompaniaSegurosTercero().getId() );
				tercero.setSiniestroCia( estimacionCobSin.getEstimacionPersonas().getNumeroSiniestroTercero() );
			}
										
			if (estimacionCobSin.getEstimacionPersonas().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1 ) {
				tercero.setRecibeOrdenCia(true);
			}else {
				tercero.setRecibeOrdenCia(false);
			}
			result = tercero;
		}else if (EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, parameter.getTipoEstimacion())) {
			Vehiculo tercero = new Vehiculo();
			tercero.setTipoEstimacion(parameter.getTipoEstimacion());
			tercero.setTipoCalculo(parameter.getTipoCalculo());	
			tercero.setCoberturaId(parameter.getCoberturaId());
			tercero.setReporteSiniestroId(parameter.getReporteSiniestroId());
			if(parameter.getId()!= null && parameter.getId()>0){
				tercero.setId(parameter.getId());
				tercero.setFolio(estimacionCobSin.getEstimacionVehiculos().getFolio());
			}
			tercero.setNombre(estimacionCobSin.getEstimacionVehiculos().getNombreAfectado());
			tercero.setContacto(estimacionCobSin.getEstimacionVehiculos().getNombrePersonaContacto());
			tercero.setTelefono(estimacionCobSin.getEstimacionVehiculos().getTelContacto());
			tercero.setCorreo(estimacionCobSin.getEstimacionVehiculos().getEmail());
			if(estimacionCobSin.getDatosEstimacion().getEstimacionActual()!= BigDecimal.ZERO){
				tercero.setEstimacion(estimacionCobSin.getDatosEstimacion().getEstimacionActual());
			}	
			tercero.setEstimacionAnterior(estimacionCobSin.getDatosEstimacion().getReserva());
			tercero.setAplicaDeducible(estimacionCobSin.getEstimacionVehiculos().getAplicaDeducible());
			tercero.setObservaciones(estimacionCobSin.getEstimacionVehiculos().getObservaciones());
			tercero.setDescripcion(estimacionCobSin.getEstimacionVehiculos().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getDeclaracionTexto());
			tercero.setSumaAmparada(estimacionCobSin.getDatosEstimacion().getSumaAseguradaAmparada());
			tercero.setTipoPase(estimacionCobSin.getEstimacionVehiculos().getTipoPaseAtencion());
			if(estimacionCobSin.getEstimacionVehiculos().getMarca()!= null){
				tercero.setMarcaId(estimacionCobSin.getEstimacionVehiculos().getMarca());
			}
			if(estimacionCobSin.getEstimacionVehiculos().getModeloVehiculo()!=null){
				tercero.setModelo(new Integer (estimacionCobSin.getEstimacionVehiculos().getModeloVehiculo()));
			}
			if(estimacionCobSin.getEstimacionVehiculos().getEstiloVehiculo()!=null){
				tercero.setDescripcionTipoEstilo(estimacionCobSin.getEstimacionVehiculos().getEstiloVehiculo());
			}
			if(estimacionCobSin.getEstimacionVehiculos().getNumeroSerie()!=null){
				tercero.setSerie(estimacionCobSin.getEstimacionVehiculos().getNumeroSerie());
			}
			if(estimacionCobSin.getEstimacionVehiculos().getPlacas()!=null){
				tercero.setPlaca(estimacionCobSin.getEstimacionVehiculos().getPlacas());
			}
			if(estimacionCobSin.getEstimacionVehiculos().getColor()!=null){
				tercero.setColor(estimacionCobSin.getEstimacionVehiculos().getColor());
			}
			if(estimacionCobSin.getEstimacionVehiculos().getTaller() != null){
				tercero.setTallerId(Integer.toString(estimacionCobSin.getEstimacionVehiculos().getTaller().getId()));	
			}
			if(estimacionCobSin.getEstimacionVehiculos().getEstado() != null){
				tercero.setIdEstado(estimacionCobSin.getEstimacionVehiculos().getEstado().getId());
			}
			if(estimacionCobSin.getEstimacionVehiculos().getDanosCubiertos() != null){
				tercero.setAreasDanadas(estimacionCobSin.getEstimacionVehiculos().getDanosCubiertos());
			}
			if(estimacionCobSin.getEstimacionVehiculos().getDanosPreexistentes() != null){
				tercero.setDanosPreexistentes(estimacionCobSin.getEstimacionVehiculos().getDanosPreexistentes());	
			}
			tercero.setPctDeducible(new BigDecimal(estimacionCobSin.getEstimacionVehiculos().getCoberturaReporteCabina().getPorcentajeDeducible()));
			if(estimacionCobSin.getEstimacionVehiculos().getTieneCompaniaSeguros() != null){
				tercero.setTieneCia(estimacionCobSin.getEstimacionVehiculos().getTieneCompaniaSeguros());
			}
			if(estimacionCobSin.getEstimacionVehiculos().getTipoPaseAtencion() == null && estimacionCobSin.getEstimacionVehiculos().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1){
				tercero.setCiaId(new Integer (estimacionCobSin.getEstimacionVehiculos().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros()));	
				tercero.setSiniestroCia( estimacionCobSin.getEstimacionVehiculos().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getSiniestroCia());
				
			} else if(estimacionCobSin.getEstimacionVehiculos().getCompaniaSegurosTercero() != null){
				tercero.setCiaId( estimacionCobSin.getEstimacionVehiculos().getCompaniaSegurosTercero().getId() );
				tercero.setSiniestroCia( estimacionCobSin.getEstimacionVehiculos().getNumeroSiniestroTercero() );
			}
			if (estimacionCobSin.getEstimacionVehiculos().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1 ) {
				tercero.setRecibeOrdenCia(true);
			}else {
				tercero.setRecibeOrdenCia(false);
			}
			tercero.setTipoPersona(estimacionCobSin.getEstimacionVehiculos().getTipoPersona());
			tercero.setCircunstancia(estimacionCobSin.getEstimacionVehiculos().getCircunstancia());
			tercero.setOtraCircunstancia(estimacionCobSin.getEstimacionVehiculos().getOtraCircunstancia());
			tercero.setEsOtraCircunstancia(estimacionCobSin.getEstimacionVehiculos().getEsOtraCircunstancia());
			tercero.setDua(estimacionCobSin.getEstimacionVehiculos().getDua());
			tercero.setClaveAjustadorTercero(estimacionCobSin.getEstimacionVehiculos().getClaveAjustador());
			tercero.setNombreAjustadorTercero(estimacionCobSin.getEstimacionVehiculos().getNombreAjustador());
			tercero.setTipoTransporte(estimacionCobSin.getEstimacionVehiculos().getTipoTransporte());
			tercero.setNumeroMotor(estimacionCobSin.getEstimacionVehiculos().getNumeroMotor());
			if(estimacionCobSin.getEstimacionVehiculos().getTieneDanosPreexistentes()==null){
				tercero.setTieneDanosPreexistentes(false);
			}else{
				tercero.setTieneDanosPreexistentes(estimacionCobSin.getEstimacionVehiculos().getTieneDanosPreexistentes());
			}
			result = tercero;			
		}else if (EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, parameter.getTipoEstimacion())) {
			Persona tercero = new Persona();
			tercero.setTipoEstimacion(parameter.getTipoEstimacion());
			tercero.setTipoCalculo(parameter.getTipoCalculo());	
			tercero.setCoberturaId(parameter.getCoberturaId());
			tercero.setReporteSiniestroId(parameter.getReporteSiniestroId());
			if(parameter.getId()!= null && parameter.getId()>0){
				tercero.setId(parameter.getId());
				tercero.setFolio(estimacionCobSin.getEstimacionViajero().getFolio());
			}
			tercero.setNombre(estimacionCobSin.getEstimacionViajero().getNombreAfectado());
			tercero.setContacto(estimacionCobSin.getEstimacionViajero().getNombrePersonaContacto());
			tercero.setTelefono(estimacionCobSin.getEstimacionViajero().getTelContacto());
			tercero.setCorreo(estimacionCobSin.getEstimacionViajero().getEmail());
			if(estimacionCobSin.getDatosEstimacion().getEstimacionActual()!= BigDecimal.ZERO){
				tercero.setEstimacion(estimacionCobSin.getDatosEstimacion().getEstimacionActual());
			}	
			tercero.setEstimacionAnterior(estimacionCobSin.getDatosEstimacion().getReserva());
			tercero.setAplicaDeducible(estimacionCobSin.getEstimacionViajero().getAplicaDeducible());
		    tercero.setDescripcion(estimacionCobSin.getEstimacionViajero().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getDeclaracionTexto());
			tercero.setSumaAmparada(estimacionCobSin.getDatosEstimacion().getSumaAseguradaAmparada());
			tercero.setTipoPase(estimacionCobSin.getEstimacionViajero().getTipoPaseAtencion());
			tercero.setTipoAtencion(estimacionCobSin.getEstimacionViajero().getTipoAtencion());
			if(estimacionCobSin.getEstimacionViajero().getEstado() != null && estimacionCobSin.getEstimacionViajero().getEstado().equalsIgnoreCase("HOM")){
				tercero.setHomicidio(true);				
			}else{
				tercero.setHomicidio(false);
			}
			if(estimacionCobSin.getEstimacionViajero().getHospital() != null){
				tercero.setHospitalId(Long.toString(estimacionCobSin.getEstimacionViajero().getHospital().getId()));	
			}
			if(estimacionCobSin.getEstimacionViajero().getMedico() != null){
				tercero.setMedicoId(Integer.toString(estimacionCobSin.getEstimacionViajero().getMedico().getId()));	
			}
			if(estimacionCobSin.getCompaniaOrdenId() != null){
				tercero.setNumOrdenAtencion(estimacionCobSin.getCompaniaOrdenId().toString());
			}
			if(estimacionCobSin.getEstimacionViajero().getDescripcion() != null){
				tercero.setLesiones(estimacionCobSin.getEstimacionViajero().getDescripcion());	
			}
			if(estimacionCobSin.getEstimacionViajero().getEdad() != null){
				tercero.setEdad(estimacionCobSin.getEstimacionViajero().getEdad());	
			}
			if(estimacionCobSin.getEstimacionViajero().getTieneCompaniaSeguros() != null){
				tercero.setTieneCia(estimacionCobSin.getEstimacionViajero().getTieneCompaniaSeguros());
			}
			if (estimacionCobSin.getEstimacionViajero().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros() != null) {
				tercero.setCiaId(new Integer (estimacionCobSin.getEstimacionViajero().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getCiaSeguros()));
			}else if(estimacionCobSin.getEstimacionViajero().getCompaniaSegurosTercero() != null){
				tercero.setCiaId( estimacionCobSin.getEstimacionViajero().getCompaniaSegurosTercero().getId() );
			}
			if (estimacionCobSin.getEstimacionViajero().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getSiniestroCia() != null) {
				tercero.setSiniestroCia( estimacionCobSin.getEstimacionViajero().getCoberturaReporteCabina()
						.getIncisoReporteCabina().getAutoIncisoReporteCabina().getSiniestroCia());
			}
			if (estimacionCobSin.getEstimacionViajero().getCoberturaReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getRecibidaOrdenCia() == 1 ) {
				tercero.setRecibeOrdenCia(true);
			}else {
				tercero.setRecibeOrdenCia(false);
			}
			result = tercero;
		}else {
			Tercero tercero = new Tercero();
			tercero.setTipoEstimacion(parameter.getTipoEstimacion());
			tercero.setTipoCalculo(parameter.getTipoCalculo());			
			tercero.setId(estimacionCobSin.getEstimacionGenerica().getId());
			tercero.setReporteSiniestroId(estimacionCobSin.getEstimacionGenerica().getCoberturaReporteCabina().getIncisoReporteCabina().getSeccionReporteCabina().getReporteCabina().getId());
			tercero.setCoberturaId(parameter.getCoberturaId());
			if(estimacionCobSin.getDatosEstimacion().getEstimacionActual()!= BigDecimal.ZERO){
				tercero.setEstimacion(estimacionCobSin.getDatosEstimacion().getEstimacionActual());
			}	
			tercero.setEstimacionAnterior(estimacionCobSin.getDatosEstimacion().getReserva());
			tercero.setAplicaDeducible(estimacionCobSin.getAplicaDeducible());
			String fuenteSumaAsegurada = estimacionCobSin.getEstimacionGenerica().getCoberturaReporteCabina().getCoberturaDTO().getClaveFuenteSumaAsegurada();
			tercero.setFuenteSumaAsegurada(fuenteSumaAsegurada);
			if ((fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_COMERCIAL) || 
					fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_FACTURA) || 
					fuenteSumaAsegurada.equals(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CONVENIDO)) && 
					estimacionCobSin.getEstimacionGenerica().getSumaAseguradaObtenida() == null) {
				tercero.setSumaAmparada(new BigDecimal(estimacionCobSin.getEstimacionGenerica().getCoberturaReporteCabina().getValorSumaAsegurada()));
			} else if(estimacionCobSin.getEstimacionGenerica().getSumaAseguradaObtenida() != null){
				tercero.setSumaAmparada(estimacionCobSin.getEstimacionGenerica().getSumaAseguradaObtenida());
			} else if(estimacionCobSin.getDatosEstimacion().getSumaAseguradaAmparada()!= BigDecimal.ZERO){
				tercero.setSumaAmparada(estimacionCobSin.getDatosEstimacion().getSumaAseguradaAmparada());
			}
			tercero.setCorreo(estimacionCobSin.getEstimacionGenerica().getCorreo());
			result = tercero;
		}
		return result;
	}	
	public BigDecimal afectaReserva(Object tercero, String claveUsuario){
		
		BigDecimal idPaseAtencion = null;
		List<String> errores = null;
		String tipoEstimacion = null;
		EstimacionCoberturaSiniestroDTO estimacionCobsin = new EstimacionCoberturaSiniestroDTO();
		EstimacionCoberturaReporteCabina estimacion = new EstimacionCoberturaReporteCabina();
		
		try{	
		    Long idCoberturaReporteCabina = 0l ;		
		  	if(tercero instanceof Persona){
				Persona t = (Persona) tercero;
				idCoberturaReporteCabina = t.getCoberturaId().longValue();
				if(t.getTipoEstimacion().equals("RCP")){
					tipoEstimacion = "RCP";
					if(t.getId() != null){
						estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, t.getId());	
						estimacionCobsin = estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento(t.getCausaMovimiento());
					}else{
						estimacionCobsin.setEstimacionPersonas(new TerceroRCPersonas());
						estimacionCobsin.getEstimacionPersonas().setTipoEstimacion(tipoEstimacion);
						estimacionCobsin.getEstimacionPersonas().setEstatus( "PEN" );
						estimacionCobsin.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
						estimacionCobsin.getEstimacionPersonas().setId(t.getId());
						CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);
						estimacionCobsin.getEstimacionPersonas().setCoberturaReporteCabina(coberturaReporteCabina);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento("APRE");
					}
					estimacionCobsin.setAplicaDeducible(t.getAplicaDeducible());
					estimacionCobsin.getEstimacionPersonas().setNombreAfectado(convertToUTF8(t.getNombre()));
					estimacionCobsin.getEstimacionPersonas().setNombrePersonaContacto(convertToUTF8(t.getContacto()));
					estimacionCobsin.getEstimacionPersonas().setTelContacto(t.getTelefono());
					estimacionCobsin.getEstimacionPersonas().setEmail(convertToUTF8(t.getCorreo()));
					estimacionCobsin.getDatosEstimacion().setEstimacionNueva(t.getEstimacion());
					if(estimacionCobsin.getEstimacionPersonas().getId()==null){
						estimacionCobsin.getDatosEstimacion().setImportePagado(BigDecimal.ZERO);
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(BigDecimal.ZERO);
					}else{
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(t.getEstimacionAnterior());
					}					
					estimacionCobsin.getDatosEstimacion().setReserva(t.getEstimacionAnterior());
					estimacionCobsin.getEstimacionPersonas().setTipoPaseAtencion(t.getTipoPase());
					if(t.getTipoPase().equals("GMT")){
						PrestadorServicio prestadorHospital = this.entidadService.findById(PrestadorServicio.class, new Integer(t.getHospitalId()));
						estimacionCobsin.getEstimacionPersonas().setHospital(prestadorHospital);
						PrestadorServicio prestadorMedico = this.entidadService.findById(PrestadorServicio.class, new Integer(t.getMedicoId()));
						estimacionCobsin.getEstimacionPersonas().setMedico(prestadorMedico);	
					}
					if(t.getNumOrdenAtencion() != null && !t.getNumOrdenAtencion().equals("")){
						estimacionCobsin.setCompaniaOrdenId(new Integer(t.getNumOrdenAtencion()));
					}	
					estimacionCobsin.getDatosEstimacion().setSumaAseguradaAmparada(t.getSumaAmparada());
					if(t.isHomicidio()){
						estimacionCobsin.getEstimacionPersonas().setEstado("HOM");
					}else{
						estimacionCobsin.getEstimacionPersonas().setEstado("LES");
					}
					estimacionCobsin.getEstimacionPersonas().setTipoAtencion(t.getTipoAtencion()); 
					estimacionCobsin.getEstimacionPersonas().setDescripcion(convertToUTF8(t.getLesiones()));
					estimacionCobsin.getEstimacionPersonas().setEdad(t.getEdad());
					estimacionCobsin.getEstimacionPersonas().setTieneCompaniaSeguros(t.getTieneCia());
					
					if(t.getTieneCia()){
						PrestadorServicio prestadosSeguroTercero = entidadService.findById(PrestadorServicio.class, t.getCiaId());
						estimacionCobsin.getEstimacionPersonas().setCompaniaSegurosTercero(prestadosSeguroTercero);
					}
					
					estimacionCobsin.getEstimacionPersonas().setNumeroSiniestroTercero(t.getSiniestroCia());
					if(t.getRecibeOrdenCia()){
						estimacionCobsin.setRecibidaOrdenCia(1);
					}else{
						estimacionCobsin.setRecibidaOrdenCia(0);
					}
									
				}else if(t.getTipoEstimacion().equals("RCJ")) {
					tipoEstimacion = "RCJ";
					if(t.getId() != null){
						estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, t.getId());	
						estimacionCobsin = estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento(t.getCausaMovimiento());
					}else{
						estimacionCobsin.setEstimacionViajero(new TerceroRCViajero());
						estimacionCobsin.getEstimacionViajero().setTipoEstimacion(tipoEstimacion);
						estimacionCobsin.getEstimacionViajero().setEstatus( "PEN" );
						estimacionCobsin.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
						estimacionCobsin.getEstimacionViajero().setId(t.getId());
						CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);
						estimacionCobsin.getEstimacionViajero().setCoberturaReporteCabina(coberturaReporteCabina);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento("APRE");
					}
					estimacionCobsin.setAplicaDeducible(t.getAplicaDeducible());
					estimacionCobsin.getEstimacionViajero().setNombreAfectado(convertToUTF8(t.getNombre()));
					estimacionCobsin.getEstimacionViajero().setNombrePersonaContacto(convertToUTF8(t.getContacto()));
					estimacionCobsin.getEstimacionViajero().setTelContacto(convertToUTF8(t.getTelefono()));
					estimacionCobsin.getEstimacionViajero().setEmail(convertToUTF8(t.getCorreo()));
					estimacionCobsin.getDatosEstimacion().setEstimacionNueva(t.getEstimacion());
					if(estimacionCobsin.getEstimacionViajero().getId()==null){
						estimacionCobsin.getDatosEstimacion().setImportePagado(BigDecimal.ZERO);
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(BigDecimal.ZERO);
					}else{
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(t.getEstimacionAnterior());
					}
					estimacionCobsin.getDatosEstimacion().setReserva(t.getEstimacionAnterior());
					estimacionCobsin.getEstimacionViajero().setTipoPaseAtencion(t.getTipoPase());
					if(t.getTipoPase().equals("GMT")){
						PrestadorServicio prestadorHospital = this.entidadService.findById(PrestadorServicio.class, new Integer(t.getHospitalId()));
						estimacionCobsin.getEstimacionViajero().setHospital(prestadorHospital);
						PrestadorServicio prestadorMedico = this.entidadService.findById(PrestadorServicio.class, new Integer(t.getMedicoId()));
						estimacionCobsin.getEstimacionViajero().setMedico(prestadorMedico);
					}
					if(t.getNumOrdenAtencion() != null && !t.getNumOrdenAtencion().equals("")){
						estimacionCobsin.setCompaniaOrdenId(new Integer(t.getNumOrdenAtencion()));
					}						
					estimacionCobsin.getDatosEstimacion().setSumaAseguradaAmparada(t.getSumaAmparada());
					if(t.isHomicidio()){
						estimacionCobsin.getEstimacionViajero().setEstado("HOM");
					}else{
						estimacionCobsin.getEstimacionViajero().setEstado("LES");
					}
					estimacionCobsin.getEstimacionViajero().setTipoAtencion(t.getTipoAtencion());
					estimacionCobsin.getEstimacionViajero().setDescripcion(convertToUTF8(t.getLesiones()));
					estimacionCobsin.getEstimacionViajero().setEdad(t.getEdad());
					if(t.getTieneCia()){
						PrestadorServicio prestadosSeguroTercero = entidadService.findById(PrestadorServicio.class, t.getCiaId());
						estimacionCobsin.getEstimacionViajero().setCompaniaSegurosTercero(prestadosSeguroTercero);
					}
					
					estimacionCobsin.getEstimacionViajero().setNumeroSiniestroTercero(t.getSiniestroCia());
					if(t.getRecibeOrdenCia()){
						estimacionCobsin.setRecibidaOrdenCia(1);
					}else{
						estimacionCobsin.setRecibidaOrdenCia(0);
					}
								
				}else if(t.getTipoEstimacion().equals("GME")) {
					tipoEstimacion = "GME";
					if(t.getId() != null){
						estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, t.getId());	
						estimacionCobsin = estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento(t.getCausaMovimiento());
					}else{
						estimacionCobsin.setEstimacionGastosMedicos(new TerceroGastosMedicos());
						estimacionCobsin.getEstimacionGastosMedicos().setTipoEstimacion(tipoEstimacion);
						estimacionCobsin.getEstimacionGastosMedicos().setEstatus( "PEN" );
						estimacionCobsin.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
						estimacionCobsin.getEstimacionGastosMedicos().setId(t.getId());
						CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);
						estimacionCobsin.getEstimacionGastosMedicos().setCoberturaReporteCabina(coberturaReporteCabina);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento("APRE");
					}
					estimacionCobsin.setAplicaDeducible(t.getAplicaDeducible());
					estimacionCobsin.getEstimacionGastosMedicos().setNombreAfectado(convertToUTF8(t.getNombre()));
					estimacionCobsin.getEstimacionGastosMedicos().setNombrePersonaContacto(convertToUTF8(t.getContacto()));
					estimacionCobsin.getEstimacionGastosMedicos().setTelContacto(convertToUTF8(t.getTelefono()));
					estimacionCobsin.getEstimacionGastosMedicos().setEmail(convertToUTF8(t.getCorreo()));
					estimacionCobsin.getDatosEstimacion().setEstimacionNueva(t.getEstimacion());
					if(estimacionCobsin.getEstimacionGastosMedicos().getId()==null){
						estimacionCobsin.getDatosEstimacion().setImportePagado(BigDecimal.ZERO);
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(BigDecimal.ZERO);
					}else{
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(t.getEstimacionAnterior());
					}
					estimacionCobsin.getDatosEstimacion().setReserva(t.getEstimacionAnterior());
					estimacionCobsin.getEstimacionGastosMedicos().setTipoPaseAtencion(t.getTipoPase());
					if(t.getTipoPase().equals("GMO")){
						PrestadorServicio prestadorHospital = this.entidadService.findById(PrestadorServicio.class, new Integer( t.getHospitalId()));
						estimacionCobsin.getEstimacionGastosMedicos().setHospital(prestadorHospital);
						PrestadorServicio prestadorMedico = this.entidadService.findById(PrestadorServicio.class, new Integer(t.getMedicoId()));
						estimacionCobsin.getEstimacionGastosMedicos().setMedico(prestadorMedico);
					}
					if(t.getNumOrdenAtencion() != null && !t.getNumOrdenAtencion().equals("")){
						estimacionCobsin.setCompaniaOrdenId(new Integer(t.getNumOrdenAtencion()));
					}	
					estimacionCobsin.getDatosEstimacion().setSumaAseguradaAmparada(t.getSumaAmparada());
					if(t.isHomicidio()){
						estimacionCobsin.getEstimacionGastosMedicos().setEstado("HOM");
					}else{
						estimacionCobsin.getEstimacionGastosMedicos().setEstado("LES");
					}
					estimacionCobsin.getEstimacionGastosMedicos().setTipoAtencion(t.getTipoAtencion());
					estimacionCobsin.getEstimacionGastosMedicos().setDescripcion(convertToUTF8(t.getLesiones()));
					estimacionCobsin.getEstimacionGastosMedicos().setEdad(t.getEdad());
					if(t.getCiaId() != null && !t.getCiaId().equals("")){
						PrestadorServicio prestadosSeguroTercero = entidadService.findById(PrestadorServicio.class, t.getCiaId());
						estimacionCobsin.getEstimacionGastosMedicos().setCompaniaSegurosTercero(prestadosSeguroTercero);
					}
					
					estimacionCobsin.getEstimacionGastosMedicos().setNumeroSiniestroTercero(t.getSiniestroCia());
					if(t.getRecibeOrdenCia()){
						estimacionCobsin.setRecibidaOrdenCia(1);
					}else{
						estimacionCobsin.setRecibidaOrdenCia(0);
					}
					
				}else if(t.getTipoEstimacion().equals("GMC")) {
					tipoEstimacion = "GMC";
					if(t.getId() != null){
						estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, t.getId());	
						estimacionCobsin = estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento(t.getCausaMovimiento());
					}else{
						estimacionCobsin.setEstimacionGastosMedicosConductor(new TerceroGastosMedicosConductor());
						estimacionCobsin.getEstimacionGastosMedicosConductor().setTipoEstimacion(tipoEstimacion);
						estimacionCobsin.getEstimacionGastosMedicosConductor().setEstatus( "PEN" );
						estimacionCobsin.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
						estimacionCobsin.getEstimacionGastosMedicosConductor().setId(t.getId());
						CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);
						estimacionCobsin.getEstimacionGastosMedicosConductor().setCoberturaReporteCabina(coberturaReporteCabina);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento("APRE");
					}
					estimacionCobsin.setAplicaDeducible(t.getAplicaDeducible());
					estimacionCobsin.getEstimacionGastosMedicosConductor().setNombreAfectado(convertToUTF8(t.getNombre()));
					estimacionCobsin.getEstimacionGastosMedicosConductor().setNombrePersonaContacto(convertToUTF8(t.getContacto()));
					estimacionCobsin.getEstimacionGastosMedicosConductor().setTelContacto(t.getTelefono());
					estimacionCobsin.getEstimacionGastosMedicosConductor().setEmail(convertToUTF8(t.getCorreo()));
					estimacionCobsin.getDatosEstimacion().setEstimacionNueva(t.getEstimacion());
					if(estimacionCobsin.getEstimacionGastosMedicosConductor().getId()==null){
						estimacionCobsin.getDatosEstimacion().setImportePagado(BigDecimal.ZERO);
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(BigDecimal.ZERO);
					}else{
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(t.getEstimacionAnterior());
					}					
					estimacionCobsin.getDatosEstimacion().setReserva(t.getEstimacionAnterior());
					estimacionCobsin.getEstimacionGastosMedicosConductor().setTipoPaseAtencion(t.getTipoPase());
					if(t.getTipoPase().equals("GMT")){
						PrestadorServicio prestadorHospital = this.entidadService.findById(PrestadorServicio.class,new Integer(t.getHospitalId()));
						estimacionCobsin.getEstimacionGastosMedicosConductor().setHospital(prestadorHospital);
						PrestadorServicio prestadorMedico = this.entidadService.findById(PrestadorServicio.class, new Integer(t.getMedicoId()));
						estimacionCobsin.getEstimacionGastosMedicosConductor().setMedico(prestadorMedico);
					}
					if(t.getNumOrdenAtencion() != null && !t.getNumOrdenAtencion().equals("")){
						estimacionCobsin.setCompaniaOrdenId(new Integer(t.getNumOrdenAtencion()));
					}			
					estimacionCobsin.getDatosEstimacion().setSumaAseguradaAmparada(t.getSumaAmparada());
					if(t.isHomicidio()){
						estimacionCobsin.getEstimacionGastosMedicosConductor().setEstado("HOM");
					}else{
						estimacionCobsin.getEstimacionGastosMedicosConductor().setEstado("LES");
					}
					estimacionCobsin.getEstimacionGastosMedicosConductor().setTipoAtencion(t.getTipoAtencion());
					estimacionCobsin.getEstimacionGastosMedicosConductor().setDescripcion(convertToUTF8(t.getLesiones()));
					estimacionCobsin.getEstimacionGastosMedicosConductor().setEdad(t.getEdad());
					estimacionCobsin.getEstimacionGastosMedicosConductor().setTieneCompaniaSeguros(t.getTieneCia());
					if(t.getTieneCia()){
						PrestadorServicio prestadosSeguroTercero = entidadService.findById(PrestadorServicio.class, t.getCiaId());
						estimacionCobsin.getEstimacionGastosMedicosConductor().setCompaniaSegurosTercero(prestadosSeguroTercero);
					}
					estimacionCobsin.getEstimacionGastosMedicosConductor().setNumeroSiniestroTercero(t.getSiniestroCia());
					if(t.getRecibeOrdenCia()){
						estimacionCobsin.setRecibidaOrdenCia(1);
					}else{
						estimacionCobsin.setRecibidaOrdenCia(0);
					}
				}
			}else if(tercero instanceof Vehiculo){
				Vehiculo t = (Vehiculo)tercero;
				idCoberturaReporteCabina = t.getCoberturaId().longValue();
				if(t.getTipoEstimacion().equals("DMA")){
					tipoEstimacion = "DMA";
					if(t.getId() != null){
						estimacionCobsin = estimacionCoberturaSiniestroService.obtenerDatosEstimacionCobertura( 
								idCoberturaReporteCabina, t.getTipoCalculo(), tipoEstimacion );		
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento(t.getCausaMovimiento());
					}else{
						estimacionCobsin.setEstimacionDanosMateriales(new TerceroDanosMateriales());
						estimacionCobsin.getEstimacionDanosMateriales().setTipoEstimacion(tipoEstimacion);
						estimacionCobsin.getEstimacionDanosMateriales().setEstatus( "PEN" );
						estimacionCobsin.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
						CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);
						estimacionCobsin.setCobertura(coberturaReporteCabina.getCoberturaDTO());
						estimacionCobsin.getEstimacionDanosMateriales().setCoberturaReporteCabina(coberturaReporteCabina);
						estimacionCobsin.getEstimacionDanosMateriales().setId(t.getId());
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento("APRE");
					}
					
					estimacionCobsin.getEstimacionDanosMateriales().setAplicaDeducible(t.getAplicaDeducible());
					estimacionCobsin.setAplicaDeducible(t.getAplicaDeducible());
					estimacionCobsin.getEstimacionDanosMateriales().setMotivoNoAplicaDeducible(t.getMotivoNoAplicaDeducibleSel());
					estimacionCobsin.setMontoDeducible(t.getDeducible());
					estimacionCobsin.setMontoDeducibleCalculado(t.getDeducibleCalculado());
					estimacionCobsin.getEstimacionDanosMateriales().setNombreAfectado(convertToUTF8(t.getNombre()));
					estimacionCobsin.getEstimacionDanosMateriales().setNombrePersonaContacto(convertToUTF8(t.getContacto()));
					estimacionCobsin.getEstimacionDanosMateriales().setTelContacto(t.getTelefono());
					estimacionCobsin.getEstimacionDanosMateriales().setEmail(convertToUTF8(t.getCorreo()));
					estimacionCobsin.getDatosEstimacion().setEstimacionNueva(t.getEstimacion());
					if(estimacionCobsin.getEstimacionDanosMateriales().getId()==null){
						estimacionCobsin.getDatosEstimacion().setImportePagado(BigDecimal.ZERO);
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(BigDecimal.ZERO);
					}else{
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(t.getEstimacionAnterior());
					}
					
					estimacionCobsin.getDatosEstimacion().setReserva(t.getEstimacionAnterior());
					estimacionCobsin.getEstimacionDanosMateriales().setTipoPaseAtencion(t.getTipoPase());
					estimacionCobsin.getEstimacionDanosMateriales().setDanosCubiertos(convertToUTF8(t.getAreasDanadas()));
					estimacionCobsin.getEstimacionDanosMateriales().setDanosPreexistentes(convertToUTF8(t.getDanosPreexistentes()));
					estimacionCobsin.getEstimacionDanosMateriales().setObservaciones(convertToUTF8(t.getObservaciones()));
					estimacionCobsin.setMarca(t.getMarcaId());
					estimacionCobsin.getEstimacionDanosMateriales().setPlacas(convertToUTF8(t.getPlaca()));
					PrestadorServicio prestadorServicioTaller = this.entidadService.findById(PrestadorServicio.class, new Integer ( t.getTallerId()));
					estimacionCobsin.getEstimacionDanosMateriales().setTaller(prestadorServicioTaller);
					estimacionCobsin.getEstimacionDanosMateriales().setEstado(new EstadoMidas());
					if(t.getIdEstado() != null){
						EstadoMidas estado = entidadService.findById(EstadoMidas.class, t.getIdEstado());
						estimacionCobsin.getEstimacionDanosMateriales().setEstado(estado);
					}
					
					estimacionCobsin.getEstimacionDanosMateriales().setTieneCompaniaSeguros(t.getTieneCia());
					if(t.getTieneCia()){
						PrestadorServicio prestadosSeguroTercero = entidadService.findById(PrestadorServicio.class, t.getCiaId());
						estimacionCobsin.getEstimacionDanosMateriales().setCompaniaSegurosTercero(prestadosSeguroTercero);
					}
					estimacionCobsin.getEstimacionDanosMateriales().setNumeroSiniestroTercero(t.getSiniestroCia());
					if(t.getRecibeOrdenCia()){
						estimacionCobsin.setRecibidaOrdenCia(1);
					}else{
						estimacionCobsin.setRecibidaOrdenCia(0);
					}
					estimacionCobsin.getDatosEstimacion().setSumaAseguradaAmparada(t.getSumaAmparada());
					estimacionCobsin.getEstimacionDanosMateriales().setSumaAseguradaObtenida(t.getSumaAmparada());
					if(StringUtils.isNotEmpty(t.getCircunstancia())){
						estimacionCobsin.getEstimacionDanosMateriales().setCircunstancia(t.getCircunstancia());
						estimacionCobsin.getEstimacionDanosMateriales().setEsOtraCircunstancia(false);
						estimacionCobsin.getEstimacionDanosMateriales().setOtraCircunstancia(null);
					} else {
						estimacionCobsin.getEstimacionDanosMateriales().setOtraCircunstancia(t.getOtraCircunstancia());
						estimacionCobsin.getEstimacionDanosMateriales().setEsOtraCircunstancia(true);
						estimacionCobsin.getEstimacionDanosMateriales().setCircunstancia(null);
					}	
					estimacionCobsin.getEstimacionDanosMateriales().setDua(t.getDua());
					estimacionCobsin.getEstimacionDanosMateriales().setTipoTransporte(t.getTipoTransporte());
					estimacionCobsin.getEstimacionDanosMateriales().setTieneDanosPreexistentes(t.isTieneDanosPreexistentes());
				}else{
					
					tipoEstimacion = "RCV";
					if(t.getId() != null){
						estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, t.getId());	
						estimacionCobsin = estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento(t.getCausaMovimiento());
					}else{
						estimacionCobsin.setEstimacionVehiculos(new TerceroRCVehiculo());
						estimacionCobsin.getEstimacionVehiculos().setTipoEstimacion(tipoEstimacion);
						estimacionCobsin.getEstimacionVehiculos().setEstatus( "PEN" );
						estimacionCobsin.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
						estimacionCobsin.getEstimacionVehiculos().setId(t.getId());
						CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);
						estimacionCobsin.setCobertura(coberturaReporteCabina.getCoberturaDTO());
						estimacionCobsin.getEstimacionVehiculos().setCoberturaReporteCabina(coberturaReporteCabina);
						estimacionCobsin.getDatosEstimacion().setCausaMovimiento("APRE");
					}
					estimacionCobsin.setAplicaDeducible(t.getAplicaDeducible());
					estimacionCobsin.getEstimacionVehiculos().setNombreAfectado(convertToUTF8(t.getNombre()));
					estimacionCobsin.getEstimacionVehiculos().setNombrePersonaContacto(convertToUTF8(t.getContacto()));
					estimacionCobsin.getEstimacionVehiculos().setTelContacto(t.getTelefono());
					estimacionCobsin.getEstimacionVehiculos().setEmail(convertToUTF8(t.getCorreo()));
					estimacionCobsin.getDatosEstimacion().setEstimacionNueva(t.getEstimacion());
					if(estimacionCobsin.getEstimacionVehiculos().getId()==null){
						estimacionCobsin.getDatosEstimacion().setImportePagado(BigDecimal.ZERO);
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(BigDecimal.ZERO);
					}else{
						estimacionCobsin.getDatosEstimacion().setEstimacionActual(t.getEstimacionAnterior());
					}
					estimacionCobsin.getDatosEstimacion().setReserva(t.getEstimacionAnterior());
					estimacionCobsin.getEstimacionVehiculos().setTipoPaseAtencion(t.getTipoPase());
					estimacionCobsin.getEstimacionVehiculos().setDanosCubiertos(convertToUTF8(t.getAreasDanadas()));
					estimacionCobsin.getEstimacionVehiculos().setDanosPreexistentes(convertToUTF8(t.getDanosPreexistentes()));
					estimacionCobsin.getEstimacionVehiculos().setObservaciones(convertToUTF8(t.getObservaciones()));
					estimacionCobsin.getEstimacionVehiculos().setMarca(t.getMarcaId());
					estimacionCobsin.getEstimacionVehiculos().setModeloVehiculo( t.getModelo().shortValue());
					estimacionCobsin.getEstimacionVehiculos().setEstiloVehiculo(convertToUTF8(t.getDescripcionTipoEstilo()));
					estimacionCobsin.getEstimacionVehiculos().setNumeroSerie(convertToUTF8(t.getSerie()));
					estimacionCobsin.getEstimacionVehiculos().setPlacas(convertToUTF8(t.getPlaca()));
					estimacionCobsin.getEstimacionVehiculos().setColor(t.getColor());
					if(t.getTipoPase().equals("PAT") || t.getTipoPase().equals("PDA")){
						PrestadorServicio prestadorServicioTaller = this.entidadService.findById(PrestadorServicio.class, new Integer ( t.getTallerId()));
						estimacionCobsin.getEstimacionVehiculos().setTaller(prestadorServicioTaller);
						estimacionCobsin.getEstimacionVehiculos().setEstado(new EstadoMidas());
						if(t.getIdEstado() != null){
							EstadoMidas estado = entidadService.findById(EstadoMidas.class, t.getIdEstado());
							estimacionCobsin.getEstimacionVehiculos().setEstado(estado);
						}
					}					
					estimacionCobsin.getEstimacionVehiculos().setTieneCompaniaSeguros(t.getTieneCia());
					if(t.getTieneCia()){
						PrestadorServicio prestadosSeguroTercero = entidadService.findById(PrestadorServicio.class, t.getCiaId());
						estimacionCobsin.getEstimacionVehiculos().setCompaniaSegurosTercero(prestadosSeguroTercero);
					} else {
						estimacionCobsin.getEstimacionVehiculos().setCompaniaSegurosTercero(null);
					}
					estimacionCobsin.getEstimacionVehiculos().setNumeroSiniestroTercero(t.getSiniestroCia());
					if(t.getRecibeOrdenCia()){
						estimacionCobsin.setRecibidaOrdenCia(1);
					}else{
						estimacionCobsin.setRecibidaOrdenCia(0);
					}
					estimacionCobsin.getDatosEstimacion().setSumaAseguradaAmparada(t.getSumaAmparada());
					/*Datos RCV*/
					estimacionCobsin.getEstimacionVehiculos().setTipoPersona(t.getTipoPersona());
					if(StringUtils.isNotEmpty(t.getCircunstancia())){
						estimacionCobsin.getEstimacionVehiculos().setCircunstancia(t.getCircunstancia());
						estimacionCobsin.getEstimacionVehiculos().setEsOtraCircunstancia(false);
						estimacionCobsin.getEstimacionVehiculos().setOtraCircunstancia(null);
					} else {
						estimacionCobsin.getEstimacionVehiculos().setOtraCircunstancia(t.getOtraCircunstancia());
						estimacionCobsin.getEstimacionVehiculos().setEsOtraCircunstancia(true);
						estimacionCobsin.getEstimacionVehiculos().setCircunstancia(null);
					}	
					estimacionCobsin.getEstimacionVehiculos().setDua(t.getDua());
					estimacionCobsin.getEstimacionVehiculos().setClaveAjustador(t.getClaveAjustadorTercero());
					estimacionCobsin.getEstimacionVehiculos().setNombreAjustador(t.getNombreAjustadorTercero());
					estimacionCobsin.getEstimacionVehiculos().setTipoTransporte(t.getTipoTransporte());
					estimacionCobsin.getEstimacionVehiculos().setNumeroMotor(t.getNumeroMotor());
					estimacionCobsin.getEstimacionVehiculos().setTieneDanosPreexistentes(t.isTieneDanosPreexistentes());
				}
			}
			else if(tercero instanceof Bien){
				Bien t = (Bien)tercero;
				idCoberturaReporteCabina = t.getCoberturaId().longValue();
				tipoEstimacion = "RCB";
				if(t.getId() != null){
					estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, t.getId());	
					estimacionCobsin = estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
					estimacionCobsin.getDatosEstimacion().setCausaMovimiento(t.getCausaMovimiento());
				}else{
					estimacionCobsin.setEstimacionBienes(new TerceroRCBienes());
					estimacionCobsin.getEstimacionBienes().setTipoEstimacion(tipoEstimacion);
					estimacionCobsin.getEstimacionBienes().setEstatus( "PEN" );
					estimacionCobsin.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
					estimacionCobsin.getEstimacionBienes().setId(t.getId());
					CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);
					estimacionCobsin.getEstimacionBienes().setCoberturaReporteCabina(coberturaReporteCabina);
					estimacionCobsin.getDatosEstimacion().setCausaMovimiento("APRE");
				}	
				estimacionCobsin.setAplicaDeducible(t.getAplicaDeducible());
				estimacionCobsin.getDatosEstimacion().setSumaAseguradaAmparada(t.getSumaAmparada());
				estimacionCobsin.getEstimacionBienes().setNombreAfectado(convertToUTF8(t.getNombre()));
				estimacionCobsin.getEstimacionBienes().setNombrePersonaContacto(convertToUTF8(t.getContacto()));
				estimacionCobsin.getEstimacionBienes().setTelContacto(convertToUTF8(t.getTelefono()));
				estimacionCobsin.getEstimacionBienes().setEmail(convertToUTF8(t.getCorreo()));
				estimacionCobsin.getDatosEstimacion().setEstimacionNueva(t.getEstimacion());
				if(estimacionCobsin.getEstimacionBienes().getId()==null){
					estimacionCobsin.getDatosEstimacion().setImportePagado(BigDecimal.ZERO);
					estimacionCobsin.getDatosEstimacion().setEstimacionActual(BigDecimal.ZERO);
				}else{
					estimacionCobsin.getDatosEstimacion().setEstimacionActual(t.getEstimacionAnterior());
				}
				
				estimacionCobsin.getDatosEstimacion().setReserva(t.getEstimacionAnterior());
				
				estimacionCobsin.getEstimacionBienes().setTipoPaseAtencion(t.getTipoPase());
				estimacionCobsin.getEstimacionBienes().setObservacion(convertToUTF8(t.getAreasDanadas()));
				estimacionCobsin.getEstimacionBienes().setDanosPreexistentes(convertToUTF8(t.getObservaciones()));
				estimacionCobsin.getEstimacionBienes().setTipoBien(t.getTipoBien());
				if(t.getTipoPase().equals("OBC")){
					PrestadorServicio prestadorResponsableReparacion = this.entidadService.findById(PrestadorServicio.class, t.getIdIngeniero());
					estimacionCobsin.getEstimacionBienes().setResponsableReparacion(prestadorResponsableReparacion);
				}
				PaisMidas pais = entidadService.findById(PaisMidas.class, "PAMEXI");
				estimacionCobsin.getEstimacionBienes().setPais(pais);
				EstadoMidas estado = entidadService.findById(EstadoMidas.class, t.getEstadoId());
				estimacionCobsin.getEstimacionBienes().setEstado(estado);
				CiudadMidas ciudad = entidadService.findById(CiudadMidas.class, t.getCiudadId());
				estimacionCobsin.getEstimacionBienes().setMunicipio(ciudad);
				if(t.getReferencia() != null ){
					estimacionCobsin.getEstimacionBienes().setUbicacion(convertToUTF8(t.getReferencia()));
				}
				
									
			}else{
				Tercero t = (Tercero)tercero;
				idCoberturaReporteCabina = t.getCoberturaId().longValue();
				tipoEstimacion = "ESD";
				if(t.getId() != null){
					estimacion = entidadService.findById(EstimacionCoberturaReporteCabina.class, t.getId());	
					estimacionCobsin = estimacionCoberturaSiniestroService.obtenerDatoEstimacionCobertura(estimacion);
					estimacionCobsin.getDatosEstimacion().setCausaMovimiento(t.getCausaMovimiento());
				}else{
					estimacionCobsin.setEstimacionGenerica(new EstimacionGenerica());
					estimacionCobsin.getEstimacionGenerica().setTipoEstimacion(tipoEstimacion);
					estimacionCobsin.getEstimacionGenerica().setEstatus( "PEN" );
					estimacionCobsin.setDatosEstimacion(new DatosEstimacionCoberturaDTO());
					estimacionCobsin.getEstimacionGenerica().setId(t.getId());
					CoberturaReporteCabina coberturaReporteCabina = this.entidadService.findById(CoberturaReporteCabina.class, idCoberturaReporteCabina);
					estimacionCobsin.setCobertura(coberturaReporteCabina.getCoberturaDTO());
					estimacionCobsin.getEstimacionGenerica().setCoberturaReporteCabina(coberturaReporteCabina);
					estimacionCobsin.getDatosEstimacion().setCausaMovimiento("APRE");
				}
				estimacionCobsin.getEstimacionGenerica().setEmail(convertToUTF8(t.getCorreo()));
				estimacionCobsin.getEstimacionGenerica().setCorreo(convertToUTF8(t.getCorreo()));
				estimacionCobsin.getDatosEstimacion().setEstimacionNueva(t.getEstimacion());
				if(estimacionCobsin.getEstimacionGenerica().getId()==null){
					estimacionCobsin.getDatosEstimacion().setImportePagado(BigDecimal.ZERO);
					estimacionCobsin.getDatosEstimacion().setEstimacionActual(BigDecimal.ZERO);
				}else{
					estimacionCobsin.getDatosEstimacion().setEstimacionActual(t.getEstimacionAnterior());
				}
				estimacionCobsin.getDatosEstimacion().setReserva(t.getEstimacionAnterior());
				estimacionCobsin.setAplicaDeducible(t.getAplicaDeducible());
				if(t.getFuenteSumaAsegurada().equals("1") || t.getFuenteSumaAsegurada().equals("2")|| t.getFuenteSumaAsegurada().equals("9")){
					estimacionCobsin.getEstimacionGenerica().getCoberturaReporteCabina().setValorSumaAsegurada(t.getSumaAmparada().doubleValue());
					estimacionCobsin.getEstimacionGenerica().setSumaAseguradaObtenida(t.getSumaAmparada());
				} else if (t.getFuenteSumaAsegurada().equals("0") ){
					estimacionCobsin.getDatosEstimacion().setSumaAseguradaAmparada(t.getSumaAmparada());
				}
				
			}
			
		  	AjustadorMovilMidasService processor = this.sessionContext.getBusinessObject(AjustadorMovilMidasService.class);
		  	errores = processor.guardarEstimacion(estimacionCobsin, idCoberturaReporteCabina, tipoEstimacion);
		  		
		}catch (Exception e) {
			errores = new ArrayList<String>();		
			errores.add("No se pudo salvar la informacion");
			log.error("Afecta Reserva", e);
		}
		
		if(errores != null && errores.isEmpty() ){
			if(EnumUtil.equalsValue(TipoEstimacion.RC_BIENES, tipoEstimacion)){
				idPaseAtencion = new BigDecimal(estimacionCobsin.getEstimacionBienes().getId());
			}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS, tipoEstimacion)){
				idPaseAtencion = new BigDecimal(estimacionCobsin.getEstimacionGastosMedicos().getId());
			}else if(EnumUtil.equalsValue(TipoEstimacion.RC_PERSONA, tipoEstimacion)){
				idPaseAtencion = new BigDecimal(estimacionCobsin.getEstimacionPersonas().getId());
			}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VIAJERO, tipoEstimacion)){
				idPaseAtencion = new BigDecimal(estimacionCobsin.getEstimacionViajero().getId());
			}else if(EnumUtil.equalsValue(TipoEstimacion.GASTOS_MEDICOS_OCUPANTES, tipoEstimacion)){
				idPaseAtencion = new BigDecimal(estimacionCobsin.getEstimacionGastosMedicosConductor().getId());
			}else if(EnumUtil.equalsValue(TipoEstimacion.RC_VEHICULO, tipoEstimacion)){
				idPaseAtencion = new BigDecimal(estimacionCobsin.getEstimacionVehiculos().getId());
			}else if(EnumUtil.equalsValue(TipoEstimacion.DANIOS_MATERIALES, tipoEstimacion)){
				idPaseAtencion = new BigDecimal(estimacionCobsin.getEstimacionDanosMateriales().getId());
			}else if(EnumUtil.equalsValue(TipoEstimacion.DIRECTA, tipoEstimacion)){
				idPaseAtencion = new BigDecimal(estimacionCobsin.getEstimacionGenerica().getId());
			}
		} else if(errores!=null){
			throw new ApplicationException(errores.toString());
		}
		
		return idPaseAtencion;
	}
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public List<String> guardarEstimacion(EstimacionCoberturaSiniestroDTO estimacionCobertura, Long idCoberturaReporte, String tipoEstimacion){
		return estimacionCoberturaSiniestroService.guardarEstimacionCobertura(estimacionCobertura, idCoberturaReporte, tipoEstimacion);
	}
	public List<CondicionEspecialMovil> obtenerCondicionesEspeciales(String id, Long idReporte){
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, idReporte);
		List<CondicionEspecialDTO> condicionEspecialList = new ArrayList<CondicionEspecialDTO>();
		List<CondicionEspecialMovil> condicionEspecialRespuesta = new ArrayList<CondicionEspecialMovil>();
		String[] elementosNumPoliza = id.split(SEPARADOR);
		if (elementosNumPoliza.length == 4) {
			Long incisoContinuityId = Long.valueOf(elementosNumPoliza[1]);
			Date validOn = new Date(Long.valueOf(elementosNumPoliza[2]));
			Date knownOn = new Date(Long.valueOf(elementosNumPoliza[3]));
			condicionEspecialList = polizaSiniestroService.buscarCondicionesEspeciales(incisoContinuityId, idReporte,
					reporteCabina.getFechaHoraReporte(), validOn, knownOn);
		}
		for (CondicionEspecialDTO dto : condicionEspecialList) {
			CondicionEspecialMovil condicionEspecial = new CondicionEspecialMovil();
			condicionEspecial.setId(dto.getCondicion().getId());
			condicionEspecial.setNombre(dto.getCondicion().getNombre());
			condicionEspecial.setCodigo(dto.getCondicion().getCodigo());
			condicionEspecial.setDescripcion(dto.getCondicion().getDescripcion());
			condicionEspecial.setEstatus(dto.getCondicion().getEstatus());
			condicionEspecialRespuesta.add(condicionEspecial);
		}
		return condicionEspecialRespuesta;
	}
	public Long guardarValuacion(Long idReporte, String codigoValuador){
		Long valuacion = 0l ;
		valuacion = valuacionReporteService.guardar(idReporte, codigoValuador, ValuacionReporteService.ESTATUS_EN_PROCESO);
		return valuacion;
	}
	public List<ReporteSiniestro> getListadoSiniestros(String numeroSerie){
		List<ReporteSiniestro> reporteSiniestrosList = new ArrayList<ReporteSiniestro>();
		List<ReporteSiniestroVehiculoDTO> reporteSiniestrosDTO = siniestroService.buscarSiniestro( numeroSerie );
		for (ReporteSiniestroVehiculoDTO dto : reporteSiniestrosDTO) {
			ReporteSiniestro reporteSiniestro = new ReporteSiniestro();
			reporteSiniestro.setId(dto.getReporteCabinaId());
			reporteSiniestro.setNumeroReporteCabina(dto.getNumeroReporte());
			reporteSiniestro.setNumeroSiniestroCabina(dto.getNumeroSiniestro());
			reporteSiniestro.setFechaOcurrido(dto.getFechaSiniestro());
			reporteSiniestro.setAjustador(new Ajustador ());
			reporteSiniestro.getAjustador().setNombre(dto.getAjustador());
			reporteSiniestro.setPoliza(dto.getNumeroPoliza());
			reporteSiniestro.setCausaSiniestro(dto.getTipoSiniestro());
			reporteSiniestro.setTerminoSiniestro(dto.getTerminoSiniestro());
			reporteSiniestro.setAsegurado(dto.getNombreAsegurado());
			reporteSiniestro.setNumeroSerie(dto.getNumeroSerie());
			reporteSiniestrosList.add(reporteSiniestro);
		}
		return reporteSiniestrosList;
		
	}
	
	public List<Catalogo> getListadoSapAmisSupervisionDeCampo(){
		
		List<Catalogo> catalogoDto = new ArrayList<Catalogo>();
		
		// buscamos catalogo sap amis
		List<CatSapAmisSupervisionCampo> sapAmisCatalogo;
		sapAmisCatalogo = entidadService.findAll(CatSapAmisSupervisionCampo.class);
		
		for (CatSapAmisSupervisionCampo dto : sapAmisCatalogo) {
			Catalogo catalogoSimple = new Catalogo();
			catalogoSimple.setValue(dto.getClaveAmis().toString());
			catalogoSimple.setLabel(dto.getSupervisionCampo());
			catalogoDto.add(catalogoSimple);
		}
		
		return catalogoDto;
		
	}
	
	public Boolean getTieneAntiguedadNumSerie(String estimacionId, String numeroSerie){
		Long idEstimacion = 0l;	
		if(estimacionId != null && !estimacionId.isEmpty()){
			idEstimacion = Long.parseLong(estimacionId);
		}
		Boolean tieneAntiguedadNumSerie = estimacionCoberturaSiniestroService.tieneNumeroDeSerieSiniestrosAnteriores(idEstimacion, numeroSerie);
		return tieneAntiguedadNumSerie;
	}
	public Short solicitarAutorizacionVigencia( Long reporteId){
		Short estatusSolicitud = -1;
		IncisoSiniestroDTO detalleInciso = 
				polizaSiniestroService.obtenerDatosIncisoAsignadoReporte(reporteId);
		polizaSiniestroService.enviarSolicitudAutorizacionVigencia(
					detalleInciso.getIdToPoliza().longValue(),
					new BigDecimal(detalleInciso.getNumeroInciso()),
					reporteId,
					detalleInciso.getEstatus());
		estatusSolicitud = polizaSiniestroService.estatusIncisoAutorizacion(detalleInciso.getIdToPoliza().longValue(), 
				reporteId, detalleInciso.getNumeroInciso(), null);
		return estatusSolicitud;
	}
	public void guardarLlegadaCia(Long idReporteSiniestro, Integer idCia, Integer orden ){
		PrestadorServicio proovedor = entidadService.findById(PrestadorServicio.class, idCia);
		ReporteCabina reporteCabina = entidadService.findById(ReporteCabina.class, idReporteSiniestro);
		AjustadorMovil ajustadorMovil = usuarioService.getAjustadorMovilMidasUsuarioActual();
		LlegadaCiaSeguros llegadaCiaSeguros = new LlegadaCiaSeguros();
		llegadaCiaSeguros.setReporteCabina(reporteCabina);
		llegadaCiaSeguros.setProveedor(proovedor);
		llegadaCiaSeguros.setCodigoUsuarioCreacion(String.valueOf(ajustadorMovil.getCodigoUsuario()));
		llegadaCiaSeguros.setOrden(orden);
		entidadService.save(llegadaCiaSeguros);
	}
	public List<LlegadaCiaResponse> llegadaCiasReporte(Long idReporteSiniestro){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("reporteCabina.id", idReporteSiniestro);
		List<LlegadaCiaSeguros> llegadaCiaList = new ArrayList<LlegadaCiaSeguros>();
		List<LlegadaCiaResponse> llegadaCiaResponseList = new ArrayList<LlegadaCiaResponse>();
		llegadaCiaList = entidadService.findByPropertiesWithOrder(LlegadaCiaSeguros.class, params, "orden asc");
		log.info("Llegada cia List" + llegadaCiaList);
		for (LlegadaCiaSeguros list : llegadaCiaList) {
			LlegadaCiaResponse llegadaCiaResponse = new LlegadaCiaResponse();
			llegadaCiaResponse.setId(list.getId());
			llegadaCiaResponse.setIdReporte(list.getReporteCabina().getId());
			llegadaCiaResponse.setIdProveedor(list.getProveedor().getId());
			llegadaCiaResponse.setNombreProveedor(list.getProveedor().getNombrePersona());
			llegadaCiaResponse.setOrden(list.getOrden());
			llegadaCiaResponseList.add(llegadaCiaResponse);
		}
		log.info("Llegada cia List Response" + llegadaCiaResponseList);
		System.out.println("Lista Llegada Cia Response" + llegadaCiaResponseList);
		return llegadaCiaResponseList;
	}
	public void eliminarLlegadaCia (Long idLlegadaCia){
		LlegadaCiaSeguros llegadaCiaSeguros = entidadService.findById(LlegadaCiaSeguros.class, Integer.valueOf(Long.toString(idLlegadaCia)));
		entidadService.remove(llegadaCiaSeguros);
	}
}

