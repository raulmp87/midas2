package mx.com.afirme.midas2.service.impl.siniestros.pagos.notasDeCredito;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.siniestros.pagos.facturas.RecepcionFacturaDao;
import mx.com.afirme.midas2.dao.siniestros.pagos.notasDeCredito.NotasCreditoDAO;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.EstimacionCoberturaReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.liquidacion.LiquidacionSiniestro;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal.TipoDocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.notasDeCredito.ConjuntoOrdenCompraNota;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.DetalleOrdenCompra;
import mx.com.afirme.midas2.domain.siniestros.valuacion.ordencompra.OrdenCompra;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.BusquedaNotasCreditoDTO;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.FacturaSiniestroRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.NotasCreditoRegistro;
import mx.com.afirme.midas2.dto.siniestros.pagos.notasDeCredito.OrdenCompraRegistro;
import mx.com.afirme.midas2.dto.siniestros.valuacion.ordencompra.ImportesOrdenCompraDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas.RecepcionFacturaServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.pagos.PagosSiniestroService;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.pagos.notasDeCredito.NotasCreditoService;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;





/**
 * @author Israel
 * @version 1.0
 * @created 31-mar.-2015 12:44:28 p. m.
 */
@Stateless
public class NotasCreditoServiceImpl implements NotasCreditoService {
	
	
	public static final String MENSAJE_ERROR_TOTAL = "El TOTAL de las notas de credito y la suma del TOTAL de las ordenes de compra no es el mismo";
	public static final String MENSAJE_ERROR_SUBTOTAL = "El SUBTOTAL de las notas de credito y la suma del SUBTOTAL de las ordenes de compra no es el mismo";
	public static final String MENSAJE_ERROR_IVA = "El IVA de la suma de las notas de credito  y la suma del IVA de las ordenes de compra no es el mismo";
	public static final String MENSAJE_ERROR_IVA_RETENIDO = "El IVA RETENIDO de la suma de las notas de credito y la suma del IVA RETENIDO de las ordenes de compra no es el mismo";
	public static final String MENSAJE_ERROR_ISR = "El ISR de la suma de las notas de cr�dito y la suma del ISR de las ordenes de compra no es el mismo";
	
	public static final String MENSAJE_ERROR_TOTAL_FACTURA = "El TOTAL de las notas de credito y el de la factura no es el mismo";
	public static final String MENSAJE_ERROR_SUBTOTAL_FACTURA = "El SUBTOTAL de las notas de credito y el de la factura no es el mismo";
	public static final String MENSAJE_ERROR_IVA_FACTURA = "El IVA de la suma de las notas de credito  y el de la factura  no es el mismo";
	public static final String MENSAJE_ERROR_IVA_RETENIDO_FACTURA = "El IVA RETENIDO de la suma de las notas de credito y el de la factura no es el mismo";
	public static final String MENSAJE_ERROR_ISR_FACTURA = "El ISR de la suma de las notas de cr�dito y el de la factura no es el mismo";
	
	public static final String MENSAJE_RECUPERACION_ERROR_TOTAL = "El TOTAL de las notas de credito difiere del TOTAL de la recuperación";
	public static final String MENSAJE_RECUPERACION_ERROR_SUBTOTAL = "El SUBTOTAL de las notas de credito difiere del SUBTOTAL de la recuperación";
	public static final String MENSAJE_RECUPERACION_ERROR_IVA = "El IVA de la suma de las notas de credito  difiere del IVA de la recuperación";
	public static final String MENSAJE_RECUPERACION_ERROR_IVA_RETENIDO = "El IVA RETENIDO de la suma de las notas de credito difiere del IVA RETENIDO de la recuperación";
	public static final String MENSAJE_RECUPERACION_ERROR_ISR = "El ISR de la suma de las notas de crédito difiere del ISR de la recuperación";
	
	
	private static final Logger log = Logger.getLogger(NotasCreditoServiceImpl.class);

	@EJB
	private NotasCreditoDAO notaCreditoDAO;
	
	@EJB
	private PagosSiniestroService pagoSiniestroService;
	
	@EJB
	private OrdenCompraService  ordenCompraService;
	
	@EJB
	private RecepcionFacturaDao recepcionFacturaDao;
	
	@EJB
	private RecepcionFacturaService recepcionFacturaService;
	
	@EJB
	private EntidadService entidadService;
	
	@EJB
	private ListadoService listadoService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private BitacoraService bitacoraService;
	
	@EJB
	private ParametroGlobalService parametroGlobalService ; 
	
	public static final int MIDAS_APP_ID = 5;
	public static final String VALIDA_AXOSNET = "SE_VALIDA_CON_AXOSNET";
	
	
	
	
	private void acompletaDescripcionEstatus(List<NotasCreditoRegistro> registros){
		Map<String,String> estatusFacturas = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_FACTURA);
		for(NotasCreditoRegistro registro : registros){
			String estatusDesc = estatusFacturas.get(registro.getEstatusDesc());
			registro.setEstatusDesc(estatusDesc);
		}
	}

	/**
	 * Parsear idNotasCreditoConcat a una lista de Long de Ids.
	 * 
	 * Crear lista "notasAEliminar" de tipo DocumentoFiscal
	 * 
	 * usar entidadService para obtener el ConjuntoOrdenCompraNota por cualquier
	 * idNotaCredito de credito parseado
	 * 
	 * Eliminar de la lista de notas de credito contenido en el objeto conjunto que
	 * corresponda a la lista de ids de notas a eliminar y asu vez agregar el objeto
	 * eliminado a la lista notasAEliminar cambiando el estatus de la nota de credito
	 * a Cancelada
	 * 
	 * Guardar el conjuntoOrdenCompraNota
	 * Guardar la lista notasAEliminar
	 * 
	 * Devolver TRUE si no hubo ninguna excepci�n, FALSE en caso contrario
	 * 
	 * @param idNotasCreditoConcat
	 */
	public Boolean cancelarNotaDeCredito(String idNotasCreditoConcat){
		List<Long> idsNotasList = this.convertToList(idNotasCreditoConcat);
		this.cancelaNotasConConjunto(idsNotasList);
//		ConjuntoOrdenCompraNota conjunto = null;
//		for(Long idNota : idsNotasList){
//			DocumentoFiscal nota = this.entidadService.findById(DocumentoFiscal.class, idNota);
//			if(conjunto == null){
//				conjunto = nota.getConjuntoDeNota();
//			}
//			nota.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.CANCELADA.getValue());
//			nota.setFechaBaja(new Date());
//			this.entidadService.save(nota);
//			// BITACORA SERVICE CANCELAR_NOTA_CREDITO
//			bitacoraService.registrar(TIPO_BITACORA.NOTA_CREDITO, EVENTO.CANCELACION_NOTA_CREDITO,idNota.toString(),"Cancelar Nota Credito", nota, usuarioService.getUsuarioActual().getNombreUsuario());
//		}
//		if(conjunto!=null){
//			DocumentoFiscal factura = conjunto.getFactura();
//			factura.getConjuntoOrdenCompraNota().remove(conjunto);
//			for(DocumentoFiscal nota: conjunto.getNotasDeCredito()){
//				nota.setConjuntoDeNota(null);
//			}
//			conjunto.setNotasDeCredito(null);
//			conjunto.setOrdenesCompra(null);
//			this.entidadService.remove(conjunto);
//			this.entidadService.save(factura);
//		}
		return true;
	}
	
	private void cancelaNotasConConjunto(List<Long> notasId){
		String nombreUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		for(Long idNota : notasId){
			List<DocumentoFiscal> notasResult = this.entidadService.findByProperty(DocumentoFiscal.class,"id",idNota);
			DocumentoFiscal nota = notasResult.get(0);
			ConjuntoOrdenCompraNota conjunto = nota.getConjuntoDeNota();
			if(conjunto != null){
				List<DocumentoFiscal> notas = conjunto.getNotasDeCredito();
				if(notas != null){
					for(DocumentoFiscal notaDeConjunto : notas){
						notaDeConjunto.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.CANCELADA.getValue());
						notaDeConjunto.setFechaBaja(new Date());
						notaDeConjunto.setFechaModificacion(new Date());
						notaDeConjunto.setCodigoUsuarioModificacion(nombreUsuario);
						notaDeConjunto.setConjuntoDeNota(null);
						this.entidadService.save(nota);
						// BITACORA SERVICE CANCELAR_NOTA_CREDITO
						bitacoraService.registrar(TIPO_BITACORA.NOTA_CREDITO, EVENTO.CANCELACION_NOTA_CREDITO,idNota.toString(),"Cancelar Nota Credito", nota, nombreUsuario);
					}
				}
				conjunto.setNotasDeCredito(null);
				conjunto.setOrdenesCompra(null);
				this.entidadService.remove(conjunto);
			}
		}
	}

	/**
	 * Iterar los DocumentosFiscales y crear un NotasCreditoRegistro por cada .
	 * 
	 * 
	 * proveedor = entidadService.findById(PrestadorServicio.class, ordenDeCompra.
	 * <font color="#0000c0">idBeneficiario) y de ahi tomar el nombre.</font>
	 * <font color="#0000c0">
	 * </font><b>registro</b>.noProveedor<font color="#0000c0"> = proveedor.id</font>
	 * <b>
	 * </b><b>registro</b>.nombreProveedor = <u>proveedor.personaMidas.nombre</u>
	 * <u>
	 * </u><b>registro.noFactura  </b>
	 * Tomar el primer elemento de la orden de compra e
	 * Invocar el metodo de Armando para traer la Factura Activa ( Documento Fiscal)
	 * de esa orden de compra y despu�s tomar  el folio.
	 * 
	 * <b>registro.noSiniestro</b>
	 * Iterar la lista de ordenesDeCompra y validar.
	 * Si todas las �rdenes cuentan con el mismo No. Siniestro asociarlo, en caso
	 * contrario asociar la palabra "VARIOS"
	 * 
	 * <b>registro.fechaRegistro </b>= documentoFiscal.fechaCreacion
	 * 
	 * <b>registro.estatusDesc</b>
	 * El valor del CATALOGO_VALOR_FIJO.ESTATUS_FACTURA
	 * documentoFiscal.estatus
	 * 
	 * @param registros
	 */
	public List<NotasCreditoRegistro> convierteANotasCreditoRegistro(List<DocumentoFiscal> registros){
		return null;
	}

	/**
	 * Iterar cada orden de compra, crear un objeto de tipo OrdenCompraRegistro y
	 * asociar los respectivos valores.
	 * 
	 * <b>registro.siniestro</b>
	 * ordenCompra.reporteCabina.siniestroCabina.numeroSiniestro
	 * 
	 * <b>registro.ordenPago</b>
	 * ordenCompra.ordenPago.id
	 * 
	 * <b>registro.ordenCompraId</b>
	 * ordenCompra.id
	 * 
	 * <b>registro.fechaRegistro</b>
	 * ordenCompra.fechaDeCreacion
	 * 
	 * <b>registro.concepto</b>
	 * Iterar por cada ordenCompra la lista de detallesOrdenCompra
	 * Y validar todos los detalle.conceptoAjuste.nombre
	 * En caso de ser todos los nombres de los conceptos el mismo asignarlo al DTO.
	 * En caso contrario asignar la palabra VARIOS
	 * 
	 * 
	 * Invocar <font color="#0000c0">ordenCompraService</font>.
	 * calcularImportes(idOrdenCompra,<font color="#7f0055"><b>null</b></font>, <font
	 * color="#7f0055"><b>false</b></font>)
	 * 
	 * Del DTO resultante asociar los respectivos valores al registro
	 * registro
	 * <ul>
	 * 	<li>subTotal</li>
	 * 	<li>iva</li>
	 * 	<li>ivaRetenido</li>
	 * 	<li>ISR</li>
	 * 	<li>total</li>
	 * </ul>
	 * 
	 * 
	 * <b>
	 * </b>
	 * 
	 * @param ordenes
	 */
	public List<OrdenCompraRegistro> convierteAOrdenesCompraRegistro(List<OrdenCompra> ordenes){
		List<OrdenCompraRegistro> registros = new ArrayList<OrdenCompraRegistro>();
		for(OrdenCompra ordenCompra : ordenes ){
			String siniestro = (ordenCompra.getReporteCabina().getSiniestroCabina()!=null)?ordenCompra.getReporteCabina().getSiniestroCabina().getNumeroSiniestro():null;
			Long idPago = ordenCompra.getOrdenPago().getId();
			Long idOrdenCompra = ordenCompra.getId();
			Date fechaRegistro = ordenCompra.getFechaCreacion();
			String concepto = this.generaConceptoDesc(ordenCompra);
			ImportesOrdenCompraDTO importesDTO = this.ordenCompraService.calcularImportes(idOrdenCompra,null,false);
			
			OrdenCompraRegistro registro = new OrdenCompraRegistro();
			registro.setSiniestro(siniestro);
			registro.setOrdenPago(idPago);
			registro.setOrdenCompraId(idOrdenCompra);
			registro.setFechaRegistro(fechaRegistro);
			registro.setConcepto(concepto);
			registro.setSubTotal(importesDTO.getSubtotal());
			registro.setTotal(importesDTO.getTotal());
			registro.setIsr(importesDTO.getIsr());
			registro.setIva(importesDTO.getIva());
			registro.setIvaRetenido(importesDTO.getIvaRetenido());
			if(ordenCompra.getIdTercero() != null)
			{
				EstimacionCoberturaReporteCabina estimacion = 
					entidadService.findById(EstimacionCoberturaReporteCabina.class,ordenCompra.getIdTercero());
				registro.setAfectado(estimacion.getFolio() + " " + estimacion.getNombreAfectado());
			}
			
			registros.add(registro);
		}
		return registros;
	}
	
	/**
	 * Iterar por cada ordenCompra la lista de detallesOrdenCompra
	 * Y validar todos los detalle.conceptoAjuste.nombre
	 * En caso de ser todos los nombres de los conceptos el mismo asignarlo al DTO.
	 * En caso contrario asignar la palabra VARIOS
	 * @return
	 */
	private String generaConceptoDesc(OrdenCompra ordenCompra){
		Boolean esElMismo = Boolean.TRUE;
		String descPorDetalle = null;
		for(DetalleOrdenCompra detalle: ordenCompra.getDetalleOrdenCompras() ){
			if(descPorDetalle == null){
				descPorDetalle = detalle.getConceptoAjuste().getNombre();
			}else if(!descPorDetalle.equals(detalle.getConceptoAjuste().getNombre())){
				esElMismo = Boolean.FALSE;
				break;
			}
		}
		return (esElMismo)?descPorDetalle:"VARIOS"; 
	}

	/**
	 * Invocar notasCreditoDAO.obtenerFacturasParaCrearNotasDeCredito
	 * y retornar los resultados.
	 */
	public List<FacturaSiniestroRegistro> obtenerFacturasParaCrearNotasDeCredito(){
		return this.notaCreditoDAO.obtenerFacturasParaCrearNotasDeCredito();
	}

	/**
	 * factura = entidadService.findById(facturaId)
	 * 
	 * Iterar las ordenes de compra de la factura y por cada ordend e compra crear un
	 * objeto OrdenesCompraRegistro,
	 * validar que la orden de compra no este asociada a una orden de credito, y que
	 * la orden de compra se encuentre en estatus AUTORIZADA  y se agrega a  una lista
	 * 
	 * 
	 * Invocar this.convierteAOrdenesCompraRegistro con la lista y retornar el
	 * resultado.
	 * 
	 * @param facturaId
	 */
	public List<OrdenCompraRegistro> obtenerOrdenesDeCompraPorFactura(Long facturaId){
		List<DocumentoFiscal> resultado = this.entidadService.findByProperty(DocumentoFiscal.class,"id", facturaId);
		DocumentoFiscal factura = resultado.get(0);
		return this.convierteAOrdenesCompraRegistro(factura.getOrdenesCompra());
	}

	/**
	 * Validar la extensi�n del Archivo
	 * 
	 * En caso de ser .ZIP, iterar cada XML y por cada iteracion aplica los siguiente,
	 * en caso de ser un .XML aplica sola una vez:
	 * 
	 * invocar recepcionFacturaDao.obtenerBatchId
	 * 
	 * Crear una lista de EnvioValidacionFactura
	 * invocar recepcionFacturaService.parsearCFD
	 * invocar recepcionFacturaService.parsearCFD.
	 * convierteComprobanteAFacturaSiniestro
	 * 
	 * Ya teniendo el documentoSiniestro ( doc )
	 * 
	 * Crear una lista de String de mensajesDeError
	 * Por cada Error encontrado se agrega a la lista
	 * 
	 * Validar que el RFC Corresponda al del proveedor
	 * recepcionFacturaService.esValidoRFCPorProveedor
	 * En caso de que no lo sea agregar a la lista de errores
	 * "EL RFC NO CORRESPONDE AL PROVEEDOR"
	 * 
	 * Si la factura se encuentra Registrada, agregar a la lista de errores "LA NOTA
	 * DE CREDITO SE ENCUENTRA REGISTRADA"
	 * 
	 * Si la factura se encuentra Registrada, agregar a la lista de errores "LA NOTA
	 * DE CREDITO SE ENCUENTRA PAGADA"
	 * 
	 * Si el documentoFiscal no es NOTA DE CREDITO, agregar a la lista de errores
	 * "ESTE DOCUMENTO NO ES UNA NOTA DE CREDITO"
	 * 
	 * Invocar recepcionFacturaService.creaEnvioValidacion
	 * Iterar la lista de mensajes de error y por cada uno encontrado
	 * invocar recepcionFacturaService.creaMensajeValidacionFactura
	 * y agregar al envio validacionFactura y guardar
	 * 
	 * @param idProveedor
	 * @param fileNotasDeCredito
	 */
	public List<EnvioValidacionFactura> procesarArchivoNotasDeCredito(Integer idProveedor, File fileNotasDeCredito,String fileName){
		List<DocumentoFiscal> facturas = new ArrayList<DocumentoFiscal>();
		log.info("FileName :"+fileName);
		FileInputStream fis = null;
		try {
			if(fileName.endsWith("zip")){
				ZipInputStream dis = new ZipInputStream(new FileInputStream(fileNotasDeCredito));
				ZipEntry ze = dis.getNextEntry();
				byte[] buffer = new byte[2048];
				while (ze != null) {
					if(ze.getName().endsWith(".xml")){
						int size;
						ByteArrayOutputStream bos = new ByteArrayOutputStream();
						while ((size = dis.read(buffer, 0, buffer.length)) != -1) {
							bos.write(buffer, 0, size);
						}
						DocumentoFiscal factura = this.recepcionFacturaService.procesaXML(bos,idProveedor,DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO);
						this.entidadService.save(factura);
						facturas.add(factura);
					}
					dis.closeEntry();
					ze = dis.getNextEntry();
				}
				dis.closeEntry();
				dis.close();
			}else if(fileName.endsWith("xml")){
				fis = new FileInputStream(fileNotasDeCredito);
				byte[] bytes = new byte[(int) fileNotasDeCredito.length()];
				fis.read(bytes);
				ByteArrayOutputStream bos = new ByteArrayOutputStream( );
				bos.write(bytes);
				DocumentoFiscal factura = this.recepcionFacturaService.procesaXML(bos, idProveedor,TipoDocumentoFiscal.NOTA_CREDITO);
				this.entidadService.save(factura);
				facturas.add(factura);
			}
		} catch (FileNotFoundException e) {
			log.error(e.getMessage(),e);
		} catch (IOException e) {
			log.error(e.getMessage(),e);
		} finally {
			IOUtils.closeQuietly(fis);
		}
		List<EnvioValidacionFactura> envioValidacionList = this.recepcionFacturaService.creaEnvioValidacion(facturas);
		this.recepcionFacturaService.validaProcesamientoDeArchivos(envioValidacionList,idProveedor);
		envioValidacionList = (List<EnvioValidacionFactura>) this.entidadService.saveAll(envioValidacionList);
		this.recepcionFacturaService.agregaEstatusDescripcion(envioValidacionList);
		return envioValidacionList;
	}
	
	private DocumentoFiscal validaQueNoSeAgreguenFacturasExistentes(DocumentoFiscal notaDeCredito){
		if(!notaDeCredito.getTipo().equals(DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue())){
			notaDeCredito.setId(null);
		}
		return notaDeCredito;
	}
	
	
	
	
	/**
	 * identificar los ids de las ordenes de compra a asociar, las que se van a
	 * cancelar y las notas de creditoSeleccionadas.
	 * 
	 * Usar entidadService para obtener el listado de EnvioValidacionService por
	 * batchId
	 * 
	 * Por todas las ordenes de compra que no se encuentran en la lista de
	 * ordenesCanceladas realizar el siguiente flujo:
	 * 
	 * Utilizar el entidadService para recuperar los EnvioValidaionFactura por batchId,
	 * filtrar esa lista solamente por aquellas notas que fueron seleccionadas.
	 * Invocar this.validaAsociacionConLiquidacion
	 * Si regresa un MensajeDeError
	 * 	Agregar el mensaje a cada elemento de la lista de 		envioValidacionFactura
	 * 	Cambiar el estatus de cada nota de credito a 	RecepcionServiceImpl.
	 * FACTURA_ERROR
	 * 	,actualizar la lista en la BD	y retornar la lista de 	EnvioValicionFactura.
	 * Caso contrario continuar
	 * 
	 * 
	 * 
	 * Invocar this.validaSumatoriaNotasOrdenes
	 * 
	 * Si la lista de mensajes de error NO se encuentra vacia, agregar esa lista a
	 * cada elemento de la lista de envioValidacionFactura
	 * 	Cambiar el estatus de cada nota de credito a RecepcionServiceImpl.
	 * FACTURA_ERROR
	 * ,actualizar la lista en la BD	y retornar la lista de EnvioValicionFactura.
	 * 
	 * 
	 * Si la lista de mensajes de error se encuentra vacia:
	 * 	Iterar las notas de cr�dito y por cada nota basarse en el siguiente c�digo:
	 * 
	 * 	EnvioFactura envioFactura = <font color="#7f0055"><b>new</b></font>
	 * EnvioFactura();
	 * 	envioFactura.setComprobante(factura.getXmlFactura());
	 * 	envioFactura.setOrigenEnvio(<font color="#0000c0"><i>ORIGEN_ENVIO</i></font>);
	 * 
	 * 	envioFactura.setIdOrigenEnvio(factura.getId());
	 * 	envioFactura = recepcioFacturaService.enviaAAxosNET(envioFactura);
	 * 	mensajes = recepcioFacturaService.
	 * convierteAMensajesValidacionFactura(envioFactura.getRespuestas(),
	 * envioValidacionFactura);
	 * 	envioValidacionFactura.setMensajes(mensajes);
	 * 
	 * 
	 * 	i<font color="#7f0055"><b>f</b></font>(envioFactura.getEstatusEnvio().
	 * intValue()== 1){
	 * 		factura.setEstatus(<font color="#0000c0"><i>FACTURA_REGISTRADA</i></font>);
	 * 		facturasAprobadas.add(factura.getNumeroFactura());
	 * 	}<font color="#7f0055"><b>else</b></font>{
	 * 		factura.setEstatus(<font color="#0000c0"><i>FACTURA_ERROR</i></font>);
	 * 		facturasRechazadas.add(envioValidacionFactura);
	 * 		EnvioValidacion.esCorrecto = FALSE
	 * 	}
	 * 
	 * Si alguna validaci�n mand� Error,
	 * 	Guardar la informaci�n en la BD y retornar la lista.
	 * En caso contrario continuar:
	 * 
	 * Si la factura tiene una liquidaci�n relacionda :
	 * 	invocar this.desasociarOrdenesDeCompraDeFactura
	 * 	cambiar el estatus de cada elemento nota de credito dentro 	de la lista
	 * EnvioValidacionFactura a "Asociada"
	 * 
	 * Si la lista de OrdenesDeCompra canceladas tiene elementos
	 * 	Invocar desasociarOrdenesDeCompraDeFactura
	 * 	Invocar servicio para cancelar ordenes de pago
	 * 	Invocar servicio para cancelar ordenes de pago
	 * 
	 * Crear un objeto de tipo ConjuntoOrdenCompraNota agregale la lista de Notas de
	 * Credito y la Lista de ordenes.
	 * Dicho objeto agregarlo a la factura
	 * 
	 * Guardar la informaci�n en la BD y retornar la lista.
	 * 
	 * @param batchId
	 * @param facturaId
	 * @param ordenesCompraConcat
	 * @param ordenesCompraCanceladasConcat
	 * @param notasCreditoSeleccionadasConcat
	 */
	public List<EnvioValidacionFactura> registrarNotasDeCredito(Long batchId, Long facturaId, String ordenesCompraConcat, String ordenesCompraCanceladasConcat, String notasCreditoSeleccionadasConcat){
		List<EnvioValidacionFactura> envioValidacionPorBatchList = this.entidadService.findByProperty(EnvioValidacionFactura.class, "idBatch", batchId);
		DocumentoFiscal factura = this.entidadService.findById(DocumentoFiscal.class, facturaId);
		
		List<Long> idsOrdenCompraList = this.convertToList(ordenesCompraConcat);
		List<Long> idsOrdenCompraCanceladaList = this.convertToList(ordenesCompraCanceladasConcat);
		List<Long> idsNotasCreditoSeleccionadas = this.convertToList(notasCreditoSeleccionadasConcat);
		
		List<OrdenCompra> ordenCompraList = new ArrayList<OrdenCompra>();
		List<OrdenCompra> ordenCompraCanceladaList = new ArrayList<OrdenCompra>();
		List<EnvioValidacionFactura> envioValidacionesSeleccionados = new ArrayList<EnvioValidacionFactura>();
		List<EnvioValidacionFactura> envioValidacionesAEliminar = new ArrayList<EnvioValidacionFactura>();
		List<DocumentoFiscal> notasDeCreditoList = new ArrayList<DocumentoFiscal>();
		
		for(OrdenCompra ordenCompra: factura.getOrdenesCompra()){
			if(idsOrdenCompraList.contains(ordenCompra.getId())){
				if(idsOrdenCompraCanceladaList.contains(ordenCompra.getId())){
					ordenCompraCanceladaList.add(ordenCompra);
				}else{
					ordenCompraList.add(ordenCompra);
				}
			}
		}
		for(EnvioValidacionFactura envioValidacion : envioValidacionPorBatchList){
			if(idsNotasCreditoSeleccionadas.contains(envioValidacion.getFactura().getId())){
				envioValidacionesSeleccionados.add(envioValidacion);
				notasDeCreditoList.add(envioValidacion.getFactura());
			}else{
				envioValidacionesAEliminar.add(envioValidacion);
			}
		}
		
		//Valida si esta asociada a la liquidacion y esta se puede editar todavia 
		MensajeValidacionFactura mensajeError = this.validaAsociacionConLiquidacion(factura);
		
		if(mensajeError!= null){
			List<MensajeValidacionFactura> mensajesError = new ArrayList<MensajeValidacionFactura>();
			mensajesError.add(mensajeError);
			for(EnvioValidacionFactura envioValidacion : envioValidacionesSeleccionados){
				envioValidacion.setMensajes(mensajesError);
				envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
				this.entidadService.save(envioValidacion.getFactura());
				this.agregaEnvioValidacion(mensajesError, envioValidacion);
			}
			this.entidadService.removeAll(envioValidacionesAEliminar);
			return this.recepcionFacturaService.agregaEstatusDescripcion(envioValidacionesSeleccionados);
		}
		
		//Valida si los montos cuadran
		List<MensajeValidacionFactura> mensajesErrorImportesList = this.validaSumatoriaNotasOrdenes(ordenCompraList, notasDeCreditoList, factura);
//		mensajesErrorImportesList = new ArrayList<MensajeValidacionFactura>();
		
		if(mensajesErrorImportesList.size()>0){
			for(EnvioValidacionFactura envioValidacion : envioValidacionesSeleccionados){
				envioValidacion.setMensajes(mensajesErrorImportesList);
				envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
				this.entidadService.save(envioValidacion.getFactura());
				this.agregaEnvioValidacion(mensajesErrorImportesList, envioValidacion);
			}
			this.entidadService.removeAll(envioValidacionesAEliminar);
			return this.recepcionFacturaService.agregaEstatusDescripcion(envioValidacionesSeleccionados);
		}
		
		
		//Se mandan todos a AXOS NET
		Boolean enviosExitosos = Boolean.TRUE;
		for(EnvioValidacionFactura envioValidacion: envioValidacionesSeleccionados){
			List<MensajeValidacionFactura> mensajesAxosNet = new ArrayList<MensajeValidacionFactura>();
			try{
				EnvioFactura envioFactura = new EnvioFactura();
				envioFactura.setComprobante(envioValidacion.getFactura().getXmlFactura());
				envioFactura.setOrigenEnvio(RecepcionFacturaServiceImpl.ORIGEN_ENVIO);
				envioFactura.setIdOrigenEnvio(envioValidacion.getFactura().getId());
				String seValidaConAxosNet = this.parametroGlobalService.obtenerValorPorIdParametro(MIDAS_APP_ID , VALIDA_AXOSNET);
				seValidaConAxosNet = "false";
				if(Boolean.valueOf(seValidaConAxosNet)){
					envioFactura = this.recepcionFacturaService.enviaAAxosNET(envioFactura);
					mensajesAxosNet = this.recepcionFacturaService.convierteAMensajesValidacionFactura(envioFactura.getRespuestas(),envioValidacion);
					envioValidacion.setMensajes(mensajesAxosNet);
				}
				if(Boolean.valueOf(seValidaConAxosNet)){
					if(envioFactura.getEstatusEnvio().intValue()== 1){
						if(factura.getLiquidacionSiniestro()!=null){
							envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ASOCIADA.getValue());
						}else{
							envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue());
						}
						// REGISTRA BITACORA CREACION_NOTAS_CREDITO
						bitacoraService.registrar(TIPO_BITACORA.NOTA_CREDITO, EVENTO.CREACION_NOTA_CREDITO,envioFactura.getIdEnvio().toString(),"Crear Nota Credito", envioFactura, usuarioService.getUsuarioActual().getNombreUsuario());
					}else{
						envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
						enviosExitosos = Boolean.FALSE;
					}
				}else{
					if(factura.getLiquidacionSiniestro()!=null){
						envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ASOCIADA.getValue());
					}else{
						envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue());
					}
					// REGISTRA BITACORA CREACION_NOTAS_CREDITO
					bitacoraService.registrar(TIPO_BITACORA.NOTA_CREDITO, EVENTO.CREACION_NOTA_CREDITO,envioValidacion.getFactura().getId().toString(),"Crear Nota Credito", envioFactura, usuarioService.getUsuarioActual().getNombreUsuario());
				}
				this.entidadService.save(envioValidacion.getFactura());
			}catch (Exception e) {
				log.error(e.getMessage(), e);
				envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
				MensajeValidacionFactura mensajeException = this.recepcionFacturaService.creaMensajeValidacionFactura(envioValidacion, 666L, e.getMessage());
				ArrayList<MensajeValidacionFactura> mensajesErrorList = new ArrayList<MensajeValidacionFactura>();
				mensajesErrorList.add(mensajeException);
				envioValidacion.setMensajes(mensajesErrorList);
				enviosExitosos = Boolean.FALSE;
			}
		}
		//Si alguna Nota de Credito no fue exitosa regresa sin registrar
		if(!enviosExitosos){
			for(EnvioValidacionFactura envioValidacion: envioValidacionesSeleccionados){
				if(envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA) || 
						envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.ASOCIADA)){
					envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.PROCESADA.getValue());
					this.entidadService.save(envioValidacion.getFactura());
					envioValidacion.setMensajes(null);
				}
			}
			this.entidadService.saveAll(envioValidacionesSeleccionados);
			return envioValidacionesSeleccionados;
		}
		
		factura.getOrdenesCompra().removeAll(ordenCompraList);
		
		try {
			for(OrdenCompra orden : ordenCompraCanceladaList){
				this.pagoSiniestroService.cancelarOrdenPago(orden.getOrdenPago().getId());
				this.ordenCompraService.cancelarOrdenPago(orden.getId());
			}
			factura.getOrdenesCompra().removeAll(ordenCompraCanceladaList);
		} catch (Exception e) {
			log.error(e.getMessage(),e);
		}
		
		this.entidadService.saveAll(envioValidacionesSeleccionados);
		this.entidadService.saveAll(ordenCompraList);
		this.entidadService.saveAll(ordenCompraCanceladaList);
		this.entidadService.removeAll(envioValidacionesAEliminar);
		
		
		
		List<ConjuntoOrdenCompraNota> conjuntoOrdenCompraNotaList = factura.getConjuntoOrdenCompraNota();
		if(conjuntoOrdenCompraNotaList==null){
			conjuntoOrdenCompraNotaList = new ArrayList<ConjuntoOrdenCompraNota>();
		}
		 
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		ConjuntoOrdenCompraNota conjuntoOrdenCompraNotas = new ConjuntoOrdenCompraNota();
		conjuntoOrdenCompraNotas.setOrdenesCompra(ordenCompraList);
		conjuntoOrdenCompraNotas.setNotasDeCredito(notasDeCreditoList);
		conjuntoOrdenCompraNotas.setCodigoUsuarioCreacion(codigoUsuario);
		conjuntoOrdenCompraNotas.setCodigoUsuarioModificacion(codigoUsuario);
		conjuntoOrdenCompraNotas.setFechaCreacion(new Date());
		conjuntoOrdenCompraNotaList.add(conjuntoOrdenCompraNotas);
		
		factura.setConjuntoOrdenCompraNota(conjuntoOrdenCompraNotaList);
		this.entidadService.save(factura);
//		for(DocumentoFiscal notaDeCredito : notasDeCreditoList){
//			notaDeCredito.setConjuntoDeNota(conjuntoOrdenCompraNotas);
//		}
//		this.entidadService.saveAll(notasDeCreditoList);
		return envioValidacionesSeleccionados;
		
		
		
	}
	
	private void agregaEnvioValidacion(List<MensajeValidacionFactura> mensajesError,EnvioValidacionFactura envioValidacion){
		for(MensajeValidacionFactura mensajeError : mensajesError){
			mensajeError.setEnvioValidacionFactura(envioValidacion);
		}
	}
	
	private List<Long> convertToList(String valuesConcat){
		List<Long> valuesLong = new ArrayList<Long>();
		if(valuesConcat != null && !valuesConcat.isEmpty()){
			String[] valuesArray = valuesConcat.split(","); 
			for(String str : valuesArray){
				valuesLong.add(Long.valueOf(str.trim()));
			}
		}
		return valuesLong;
	}

	/**
	 * 
	 * Si factura.liquidacion != null (Tiene Liquidaci�n asociada):
	 * 	Si el estatus de la liquidacion es diferente a "EN TRAMITE" o "PENDIENTE POR
	 * AUTORIZAR" crear un objeto MensajeValidacionFactura con el texto de que la
	 * liquidaci�n se encuentra en el estatus actual ( la descripci�n  del estatus
	 * donde se encuentre)
	 * 
	 * @param factura
	 */
	private MensajeValidacionFactura validaAsociacionConLiquidacion(DocumentoFiscal factura){
		
		LiquidacionSiniestro liquidacion = this.notaCreditoDAO.obtenerLiquidacionDeFactura(factura.getId());
		MensajeValidacionFactura mensajeError = null;
		if(liquidacion!=null){
			List<String> estatusValidos = new ArrayList<String>();
			estatusValidos.add(LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE.getValue());
			estatusValidos.add(LiquidacionSiniestro.EstatusLiquidacionSiniestro.EN_TRAMITE_POR_RECHAZO.getValue());
			estatusValidos.add(LiquidacionSiniestro.EstatusLiquidacionSiniestro.POR_AUTORIZAR.getValue());
			if(!estatusValidos.contains(liquidacion.getEstatus())){
				Map<String,String> estatusMap = this.listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_LIQUIDACION_SINIESTRO);
				String mensajeStr = "No se puede asociar a liquidacion en estatus "+estatusMap.get(liquidacion.getEstatus());
				mensajeError = this.recepcionFacturaService.creaMensajeValidacionFactura(null,0L,mensajeStr );
			}
		}
		return mensajeError;
	}

	/**
	 * De la lista de ordenes de compra realizar la sumatoria y almacenar los valores
	 * de los siguientes montos :
	 * <ul>
	 * 	<li>total</li>
	 * 	<li>subtotal</li>
	 * 	<li>iva</li>
	 * 	<li>ivaRetenido	</li>
	 * 	<li>isr</li>
	 * </ul>
	 * Apoyarse del servicio:
	 * ordenesCompraService.calcularImportes(ordenId,null.false)
	 * 
	 * Por la lista de notas de credito
	 * Realizar la sumatoria y almacenar los valores de los siguientes montos :
	 * <ul>
	 * 	<li>total</li>
	 * 	<li>subtotal</li>
	 * 	<li>iva</li>
	 * 	<li>ivaRetenido	</li>
	 * 	<li>isr</li>
	 * </ul>
	 * 
	 * Comparar los respectivos montos de las notas y de las ordenes y por cada monto
	 * que no se igual crear un MensajeValidacionFactura y agregarlo a una lista.
	 * 
	 * Una vez que se validen todos los montos, retornar la lista de los
	 * MensajesValidacionFactura
	 * 
	 * Los mensajes podrian ser los siguientes:
	 * <font color="#0000c0"><i>MENSAJE_ERROR_TOTAL</i></font> = <font
	 * color="#2a00ff">"El TOTAL de las notas de credito y la suma del TOTAL de las
	 * ordenes de compra no es el mismo"</font>;
	 * <font color="#0000c0"><i>MENSAJE_ERROR_SUBTOTAL</i></font> = <font
	 * color="#2a00ff">"El SUBTOTAL de las notas de credito y la suma del SUBTOTAL de
	 * las ordenes de compra no es el mismo"</font>
	 * <font color="#0000c0"><i>MENSAJE_ERROR_IVA</i></font> = <font
	 * color="#2a00ff">"El IVA de la suma de las notas de credito  y la suma del IVA
	 * de las ordenes de compra no es el mismo"</font>
	 * <font color="#0000c0"><i>MENSAJE_ERROR_IVA_RETENIDO</i></font> = <font
	 * color="#2a00ff">"El IVA RETENIDO de la suma de las notas de credito y la suma
	 * del IVA RETENIDO de las ordenes de compra no es el mismo"</font>
	 * <font color="#0000c0"><i>MENSAJE_ERROR_ISR</i></font> = <font
	 * color="#2a00ff">"El ISR de la suma de las notas de cr�dito y la suma del ISR de
	 * las ordenes de compra no es el mismo"</font>
	 * 
	 * @param ordenes
	 * @param notas
	 */
	
	
	private List<MensajeValidacionFactura> validaSumatoriaNotasOrdenes(List<OrdenCompra> ordenes, List<DocumentoFiscal> notas,DocumentoFiscal factura){
		List<MensajeValidacionFactura> mensajesError = new ArrayList<MensajeValidacionFactura>();
		
		BigDecimal totalOrdenes = BigDecimal.ZERO;
		BigDecimal subtotalOrdenes = BigDecimal.ZERO;
		BigDecimal ivaOrdenes = BigDecimal.ZERO;
		BigDecimal ivaRetenidoOrdenes = BigDecimal.ZERO;
		BigDecimal isrOrdenes = BigDecimal.ZERO;
		
		BigDecimal totalNotas = BigDecimal.ZERO;
		BigDecimal subtotalNotas = BigDecimal.ZERO;
		BigDecimal ivaNotas = BigDecimal.ZERO;
		BigDecimal ivaRetenidoNotas = BigDecimal.ZERO;		
		BigDecimal isrNotas = BigDecimal.ZERO;
		Boolean validaContraFactura = Boolean.FALSE;
		
		if(ordenes.size()==factura.getOrdenesCompra().size()){
			totalOrdenes = factura.getMontoTotal();
			subtotalOrdenes = factura.getSubTotal();
			ivaOrdenes = factura.getIva();
			ivaRetenidoOrdenes = factura.getIvaRetenido();
			isrOrdenes = factura.getIsr();
			validaContraFactura = Boolean.TRUE;
		}else{
			for(OrdenCompra orden : ordenes){
				ImportesOrdenCompraDTO importesDTO = this.ordenCompraService.calcularImportes(orden.getId(),null,false);
				totalOrdenes = totalOrdenes.add(importesDTO.getTotal());
				subtotalOrdenes = subtotalOrdenes.add(importesDTO.getSubtotal());
				ivaOrdenes = ivaOrdenes.add(importesDTO.getIva());
				ivaRetenidoOrdenes = ivaRetenidoOrdenes.add(importesDTO.getIvaRetenido());
				isrOrdenes = isrOrdenes.add(importesDTO.getIsr());
			}
		}
		
		for(DocumentoFiscal nota:  notas){
			totalNotas = totalNotas.add(nota.getMontoTotal());
			subtotalNotas = subtotalNotas.add(nota.getSubTotal());
			ivaNotas = ivaNotas.add(nota.getIva());
			ivaRetenidoNotas = ivaRetenidoNotas.add(nota.getIvaRetenido());
			isrNotas = isrNotas.add(nota.getIsr());
		}
		String mensajeError = (validaContraFactura)?MENSAJE_ERROR_TOTAL_FACTURA : MENSAJE_ERROR_TOTAL;
		if(totalOrdenes.compareTo(totalNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(null,0L,mensajeError ));
		}
		String mensajeSubtotal = (validaContraFactura)?MENSAJE_ERROR_SUBTOTAL_FACTURA : MENSAJE_ERROR_SUBTOTAL;
		if(subtotalOrdenes.compareTo(subtotalNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(null,0L,mensajeSubtotal ));
		}
		String mensajeIva = (validaContraFactura)?MENSAJE_ERROR_IVA_FACTURA : MENSAJE_ERROR_IVA;
		if(ivaOrdenes.compareTo(ivaNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(null,0L,mensajeIva ));
		}
		String mensajeIvaRetenido = (validaContraFactura)?MENSAJE_ERROR_IVA_RETENIDO_FACTURA : MENSAJE_ERROR_IVA_RETENIDO;
		if(ivaRetenidoOrdenes.compareTo(ivaRetenidoNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(null,0L,mensajeIvaRetenido ));
		}
		String mensajeIsr = (validaContraFactura)?MENSAJE_ERROR_ISR_FACTURA : MENSAJE_ERROR_ISR;
		if(isrOrdenes.compareTo(isrNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(null,0L,mensajeIsr ));
		}
		
		return mensajesError;
	}

	/**
	 * Invoca notasCreditoDAO.NotasCreditoDAO.
	 * 
	 * Los documentos fiscales (Notas de Credito) devueltos se pasan al m�todo
	 * convierteANotasCreditoRegistroy postriormente se retorna el resultado.
	 * 
	 * @param notasCreditoFIltro
	 */
	
	@Override
	public List<NotasCreditoRegistro> buscarNotasCeCredito(BusquedaNotasCreditoDTO notasCreditoFIltro) {
		List<NotasCreditoRegistro> registros = this.notaCreditoDAO.buscarNotasCeCredito(notasCreditoFIltro); 
		this.acompletaDescripcionEstatus(registros);
		return registros;
	}
	
	
	@Override
	public List<EnvioValidacionFactura> obtenerEnvioValidacionFacturaPorConjunto(Long conjuntoId){
		ConjuntoOrdenCompraNota conjunto = this.entidadService.findById(ConjuntoOrdenCompraNota.class, conjuntoId);
		List<EnvioValidacionFactura> envioList = new ArrayList<EnvioValidacionFactura>();
		for(DocumentoFiscal nota : conjunto.getNotasDeCredito()){
			EnvioValidacionFactura envio = new EnvioValidacionFactura();
			envio.setFactura(nota);
			envioList.add(envio);
		}

		recepcionFacturaService.agregaEstatusDescripcion(envioList);
		
		return envioList;
	}
	
	@Override
	public List<OrdenCompraRegistro> obtenerOrdenesDeCompraPorConjunto(Long conjuntoId){
		List<ConjuntoOrdenCompraNota> conjuntos = this.entidadService.findByProperty(ConjuntoOrdenCompraNota.class,"id", conjuntoId);
		List<OrdenCompraRegistro> ordenes = null;
		if(conjuntos != null && conjuntos.size()>0){
			ConjuntoOrdenCompraNota conjunto = conjuntos.get(0);
			ordenes =  this.convierteAOrdenesCompraRegistro(conjunto.getOrdenesCompra());
		}
		return ordenes;
	}

	@Override
	public Boolean existenFacturasEstatus(List<EnvioValidacionFactura> facturas, String estatusDocumentoFiscal){
		Boolean existenFacturas = null;
		
		if(facturas != null 
				&& estatusDocumentoFiscal != null){
			for(EnvioValidacionFactura factura: facturas){
				if(factura.getFactura().getEstatus() != null
						&& factura.getFactura().getEstatus().compareTo(estatusDocumentoFiscal) == 0){
					existenFacturas = Boolean.TRUE;
					break;
				}
			}
			if(existenFacturas == null){
				existenFacturas = Boolean.FALSE;
			}
		}
		
		return existenFacturas;
	}
	
	@Override
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorFactura(Long idFactura){
		return this.notaCreditoDAO.obtenerNotasDeCreditoPorFactura(idFactura);
	}
	
	@Override
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorRecuperacion(Long idRecuperacion)
	{
		return this.notaCreditoDAO.obtenerNotasDeCreditoPorRecuperacion(idRecuperacion);		
	}
	
	@Override
	public List<NotasCreditoRegistro> obtenerNotasDeCreditoPorLiquidacion(Long idLiquidacion)
	{
		return this.notaCreditoDAO.obtenerNotasDeCreditoPorLiquidacion(idLiquidacion);		
	}
	
	
	@Override
	public List<NotasCreditoRegistro> obtenerRecuperacionesNotasDeCreditoPorLiquidacion(Long idLiquidacion){
		return this.notaCreditoDAO.obtenerRecuperacionesNotasDeCreditoPorLiquidacion(idLiquidacion);		
	}
	
	@Override
	public String cancelarNotaCreditoRecuperacion (Long idNotaCredito ){
		final String MENSAJE_ERROR_NOTA_NO_REGISTRADA = "NO SE PUEDE CANCELAR UNA NOTA DE CRÉDITO QUE NO SE ENCUENTRE EN ESTATUS REGISTRADA";
		DocumentoFiscal nota = this.entidadService.findById(DocumentoFiscal.class, idNotaCredito);
		String nombreUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		if(!nota.getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue())){
			return MENSAJE_ERROR_NOTA_NO_REGISTRADA;
		}
		nota.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.CANCELADA.getValue());
		nota.setCodigoUsuarioModificacion(nombreUsuario);
		bitacoraService.registrar(TIPO_BITACORA.NOTA_CREDITO, EVENTO.CANCELACION_NOTA_CREDITO,idNotaCredito.toString(),"Cancelar Nota Credito", nota, nombreUsuario);
		this.entidadService.save(nota);
		return null;
	}
	
	@Override
	public List<EnvioValidacionFactura> obtenerNotasCreditoPendientesPorProovedor (Integer idProveedor){
		List<DocumentoFiscal> notas = this.notaCreditoDAO.obtenerNotasDeCreditoParaAsociarRecuperacion(idProveedor);
		return this.recepcionFacturaService.creaEnvioValidacion(notas);
	}
	
	@Override
	public List<EnvioValidacionFactura> procesarNotasCreditoRecuperacionProveedor (Integer idProveedor,File fileNotasDeCredito,String extension){
		List<EnvioValidacionFactura> envioValidacionList = this.procesarArchivoNotasDeCredito(idProveedor, fileNotasDeCredito, extension);
		Long batchId = null;
		if(envioValidacionList!=null && envioValidacionList.size()>0){
			batchId = envioValidacionList.get(0).getIdBatch();
		}
		List<DocumentoFiscal> notasPendientes = this.notaCreditoDAO.obtenerNotasDeCreditoParaAsociarRecuperacion(idProveedor);
		
		for(DocumentoFiscal nota: notasPendientes){
			EnvioValidacionFactura envioValidacion = this.recepcionFacturaService.creaEnvioValidacion(nota, batchId);
			this.entidadService.save(envioValidacion);
			envioValidacionList.add(envioValidacion);
		}
		return envioValidacionList;
	}
	
	@Override
	public List<EnvioValidacionFactura> registrarNotasParaRecuperaciones (Long idBatch,Long idRecuperacion,String notasCreditoSeleccionadasConcat,Integer idProveedor){
		List<EnvioValidacionFactura> envioValidacionList = this.entidadService.findByProperty(EnvioValidacionFactura.class, "idBatch", idBatch);
		RecuperacionProveedor recuperacion = this.entidadService.findById(RecuperacionProveedor.class, idRecuperacion);
		List<Long> idsNotasList = this.convertToList(notasCreditoSeleccionadasConcat);
		List<EnvioValidacionFactura> envioValidacionSeleccionaList = new ArrayList<EnvioValidacionFactura>();
		for(EnvioValidacionFactura envioValidacion : envioValidacionList){
			if( idsNotasList.contains(envioValidacion.getFactura().getId())){
				envioValidacionSeleccionaList.add(envioValidacion);
			}else{
				this.entidadService.remove(envioValidacion);
			}
		}
		
		List<MensajeValidacionFactura> mensajesErrorList = this.validaSumatoriaNotasRecuperacion(recuperacion, envioValidacionSeleccionaList);
		if(mensajesErrorList.size()==0){
			List<DocumentoFiscal> notas = new ArrayList<DocumentoFiscal>();
			recuperacion.setMedio(Recuperacion.MedioRecuperacion.NOTACRED.toString());
			for(EnvioValidacionFactura envioValidacion : envioValidacionSeleccionaList){
				envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue());
				this.entidadService.save(envioValidacion.getFactura());
				notas.add(envioValidacion.getFactura());
			}
			recuperacion.setNotasdeCredito(notas);
			this.entidadService.save(recuperacion);
		}
		this.entidadService.saveAll(envioValidacionSeleccionaList);
		return envioValidacionSeleccionaList;
	}
	
	@Override
	public List<MensajeValidacionFactura> validaSumatoriaNotasRecuperacion (RecuperacionProveedor recuperacion, List<EnvioValidacionFactura> envioValidacionList){
		
		List<MensajeValidacionFactura> mensajesError = new ArrayList<MensajeValidacionFactura>();
		BigDecimal totalNotas = BigDecimal.ZERO;
		BigDecimal subtotalNotas = BigDecimal.ZERO;
		BigDecimal ivaNotas = BigDecimal.ZERO;
		BigDecimal ivaRetenidoNotas = BigDecimal.ZERO;
		BigDecimal isrNotas = BigDecimal.ZERO;
		EnvioValidacionFactura primerEnvioValidacion = envioValidacionList.get(0);
		
		for(EnvioValidacionFactura envioValidacion: envioValidacionList){
			DocumentoFiscal nota = envioValidacion.getFactura();
			totalNotas = totalNotas.add(nota.getMontoTotal());
			subtotalNotas = subtotalNotas.add(nota.getSubTotal());
			ivaNotas = ivaNotas.add(nota.getIva());
			ivaRetenidoNotas = ivaRetenidoNotas.add(nota.getIvaRetenido());
			isrNotas = isrNotas.add(nota.getIsr());
		}
			
		if(recuperacion.getMontoTotal().compareTo(totalNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(primerEnvioValidacion,0L,MENSAJE_RECUPERACION_ERROR_TOTAL ));
		}
		if(recuperacion.getSubTotal().compareTo(subtotalNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(primerEnvioValidacion,0L,MENSAJE_RECUPERACION_ERROR_SUBTOTAL ));
		}
		if(recuperacion.getIva().compareTo(ivaNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(primerEnvioValidacion,0L,MENSAJE_RECUPERACION_ERROR_IVA ));
		}
		if(recuperacion.getIvaRetenido().compareTo(ivaRetenidoNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(primerEnvioValidacion,0L,MENSAJE_RECUPERACION_ERROR_IVA_RETENIDO ));
		}
		if(recuperacion.getIsr().compareTo(isrNotas)!= 0){
			mensajesError.add(this.recepcionFacturaService.creaMensajeValidacionFactura(primerEnvioValidacion,0L,MENSAJE_RECUPERACION_ERROR_ISR ));
		}
		
		if(mensajesError.size()>0){
			primerEnvioValidacion.setMensajes(mensajesError);
			for(EnvioValidacionFactura envioValidacion: envioValidacionList){
				if(!envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue())){
					envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
					this.entidadService.save(envioValidacion.getFactura());
				}
			}
		}
		return mensajesError;
	}
	
}