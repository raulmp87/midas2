package mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion.tiposervicio;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoDTO;
import mx.com.afirme.midas.catalogos.tiposerviciovehiculo.TipoServicioVehiculoFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicioDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicio;
import mx.com.afirme.midas2.dto.negocio.seccion.tiposervicio.RelacionesNegocioTipoServicioDTO;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicioService;

@Stateless
public class NegocioTipoServicioServiceImpl implements
		NegocioTipoServicioService {

	/**
	 * 
	 */
	private static final long serialVersionUID = -348161699586719529L;

	private NegocioTipoServicioDao negocioTipoServicioDao;
	
	@EJB
	public void setNegocioTipoServicioDao(
			NegocioTipoServicioDao negocioTipoServicioDao) {
		this.negocioTipoServicioDao = negocioTipoServicioDao;
	}
	
	@Override
	public RelacionesNegocioTipoServicioDTO getRelationLists(NegocioSeccion negocioSeccion) {
		RelacionesNegocioTipoServicioDTO relacionesNegocioTipoServicioDTO = new RelacionesNegocioTipoServicioDTO();
		//Listado asociadas
		
		List<NegocioTipoServicio> negocioTipoServicioAsociadasList = entidadDao.findByProperty(NegocioTipoServicio.class, "negocioSeccion.idToNegSeccion", negocioSeccion.getIdToNegSeccion());
		//Listado posibles
		TipoServicioVehiculoDTO tipoServicioVehiculoDTO = new TipoServicioVehiculoDTO(null, negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo(), null);
		List<TipoServicioVehiculoDTO> posibles =  tipoServicioVehiculoFacadeRemote.listarFiltrado(tipoServicioVehiculoDTO);
		//Listado disponibles
		List<NegocioTipoServicio> negocioTipoServicioDisponiblesList = new ArrayList<NegocioTipoServicio>();
		
		List<TipoServicioVehiculoDTO> tipoServicioVehiculoDTOAsociadasList = new ArrayList<TipoServicioVehiculoDTO>();
		
		for(NegocioTipoServicio item : negocioTipoServicioAsociadasList){
			tipoServicioVehiculoDTOAsociadasList.add(item.getTipoServicioVehiculoDTO());
		}
		
		for(TipoServicioVehiculoDTO item : posibles){
			if(!tipoServicioVehiculoDTOAsociadasList.contains(item)){
				NegocioTipoServicio negocioTipoServicio = new NegocioTipoServicio();
				negocioTipoServicio.setTipoServicioVehiculoDTO(item);
				negocioTipoServicio.setNegocioSeccion(negocioSeccion);
				negocioTipoServicio.setClaveDefault(false);
				negocioTipoServicioDisponiblesList.add(negocioTipoServicio);
			}
		}
		
		relacionesNegocioTipoServicioDTO.setAsociadas(negocioTipoServicioAsociadasList);
		relacionesNegocioTipoServicioDTO.setDisponibles(negocioTipoServicioDisponiblesList);
		return relacionesNegocioTipoServicioDTO;
	}

	@Override
	public void relacionarNegocioTipoServicio(String accion,
			NegocioTipoServicio negocioTipoServicio) {
		
		if(accion.equals("deleted")){
			negocioTipoServicio = entidadDao.findById(NegocioTipoServicio.class, negocioTipoServicio.getIdToNegTipoServicio());
		}else{
			NegocioSeccion negocioSeccion = entidadDao.findById(NegocioSeccion.class, negocioTipoServicio.getNegocioSeccion().getIdToNegSeccion());
			negocioTipoServicio.setNegocioSeccion(negocioSeccion);
			TipoServicioVehiculoDTO tipoServicioVehiculoDTO = tipoServicioVehiculoFacadeRemote.findById(negocioTipoServicio.getTipoServicioVehiculoDTO().getIdTcTipoServicioVehiculo());
			negocioTipoServicio.setTipoServicioVehiculoDTO(tipoServicioVehiculoDTO);
			negocioTipoServicio.setClaveDefault(negocioTipoServicio.getClaveDefault());
			  /* Si no existen mas negocioTipoServcioAsociedados es default */
			List<NegocioTipoServicio> negocioTipoServicioList = entidadDao.findByProperty(NegocioTipoServicio.class, "negocioSeccion.idToNegSeccion", negocioTipoServicio.getNegocioSeccion().getIdToNegSeccion());
			if(negocioTipoServicioList.size()==0)negocioTipoServicio.setClaveDefault(true);

		}
		entidadDao.executeActionGrid(accion, negocioTipoServicio);

	}
	
	private EntidadDao entidadDao;
	private TipoServicioVehiculoFacadeRemote tipoServicioVehiculoFacadeRemote;
	
	@EJB
	public void setEntidadDao(EntidadDao catalogoDato) {
		this.entidadDao = catalogoDato;
	}
	
	@EJB
	public void setTipoServicioVehiculoFacadeRemote(TipoServicioVehiculoFacadeRemote tipoServicioVehiculoFacadeRemote) {
		this.tipoServicioVehiculoFacadeRemote = tipoServicioVehiculoFacadeRemote;
	}

	@Override
	public NegocioTipoServicio buscarDefault(NegocioSeccion negocioSeccion) {
		return negocioTipoServicioDao.buscarDefault(negocioSeccion);
	}


}
