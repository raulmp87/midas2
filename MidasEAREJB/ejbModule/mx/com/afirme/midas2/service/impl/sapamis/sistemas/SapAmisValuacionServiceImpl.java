package mx.com.afirme.midas2.service.impl.sapamis.sistemas;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.sapamis.sistemas.SapAmisValuacionDao;
import mx.com.afirme.midas2.dto.sapamis.sistemas.SapAmisValuacion;
import mx.com.afirme.midas2.dto.sapamis.utiles.ParametrosConsulta;
import mx.com.afirme.midas2.service.sapamis.sistemas.SapAmisValuacionService;

/*******************************************************************************
 * Nombre Interface: 	SapAmisValuacionServiceImpl.
 * 
 * Descripcion: 		Se utiliza para el manejo del Modulo de Valuacion.
 * 
 * Unidad de Fabrica:	Avance Solution Corporation.
 * 
 * Lider Tecnico:		Eduardo Valentín Chávez Oliveros. 
 *
 *******************************************************************************/
@Stateless
public class SapAmisValuacionServiceImpl implements SapAmisValuacionService{
	private static final long serialVersionUID = 1L;
	
	@EJB private SapAmisValuacionDao sapAmisValuacionDao;

	@Override
	public List<SapAmisValuacion> obtenerPorFiltros(ParametrosConsulta parametrosConsulta, long numRegXPag, long numPagina) {
		return sapAmisValuacionDao.obtenerPorFiltros(parametrosConsulta, numRegXPag, numPagina);
	}
}
