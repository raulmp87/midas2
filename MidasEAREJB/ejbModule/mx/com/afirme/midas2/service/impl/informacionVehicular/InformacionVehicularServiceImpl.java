package mx.com.afirme.midas2.service.impl.informacionVehicular;

import java.math.BigDecimal;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoEquivalenciaDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.domain.cesvi.vinplus.Respuesta;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.poliza.diferenciasamis.DiferenciasAmis;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.VarModifDescripcionService;
import mx.com.afirme.midas2.service.datosAmis.DatosAmisService;
import mx.com.afirme.midas2.service.diferenciasAmis.DiferenciasAmisService;
import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.wsClient.cesvi.vinplus.VinplusBindingStub;
import mx.com.afirme.midas2.wsClient.cesvi.vinplus.VinplusLocator;

import org.apache.log4j.Logger;

@Stateless
public class InformacionVehicularServiceImpl implements InformacionVehicularService {
	
	@EJB
	protected EstiloVehiculoFacadeRemote estiloVehiculoFacadeRemote;
	
	@EJB
	private VarModifDescripcionService varModifDescripcionService;
	
	@EJB
	private DiferenciasAmisService diferenciasAmisService;
	
	@EJB
	protected EntidadService entidadService;
	
	@EJB
	protected ParametroGeneralService parametroGeneralService;
	
	@EJB
	private DatosAmisService datosAmisService;
	
	private UsuarioService usuarioService;
	
	@EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }
	
	@EJB
	private SistemaContext sistemaContext;
	
	public static final String SEPARADOR_VEHICULO_INICIO = "<inicio>";
	public static final String SEPARADOR_VEHICULO_FIN = "<fin>";
	public static final String SEPARADOR_VEHICULO_CARACTERISTICAS = "\\|";
	public static final String EXITO = "30";
	public static final String ERROR = "10";
	public final int LENGTH_ORIGINAL = 36;
	public final int LENGTH_CON_CLAVES = 41;
	public final int LENGTH_TIMEOUT = 9999;
	
	private static final Logger LOG = Logger.getLogger(InformacionVehicularServiceImpl.class);
	
	@Override
	public byte[] getConsultaVin(String vin) {
		byte[] respuesta = null;
        try {
        	String url = sistemaContext.getCesviIndividualEndpoint();
        	LOG.info("Cesvi Endpoint Individual: "+url);
        	VinplusLocator locator = new VinplusLocator();
			VinplusBindingStub service = (VinplusBindingStub)locator.getvinplusPort(new URL(url));
			service.setUrl(url);
			service.setTimeout(LENGTH_TIMEOUT);
			respuesta = service.consultaVin(vin);
        }
        catch (MalformedURLException e) {
        	LOG.error("Información Vehicular Individual getConsultaVin. Al traer el URL: ", e);
        }
        catch (Exception e) {
            LOG.error("Información Vehicular Individual getConsultaVin:", e);
        }
		return respuesta;
    }

	private Map<String, String> validaInformacionVehicular(BigDecimal idToCotizacion, BigDecimal idToPoliza) {
		Map<String, String> mensajeInformacionVehicular = new HashMap<String, String>();
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, idToPoliza);
		Usuario usuario = usuarioService.getUsuarioActual();
		String icono = EXITO;
		String mensaje = "";
		
		for (IncisoCotizacionDTO inciso : cotizacion.getIncisoCotizacionDTOs()) {
			
			if(inciso.getIncisoAutoCot() != null) {
				NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, 
						new BigDecimal(inciso.getIncisoAutoCot().getNegocioSeccionId()));
				
				if (SeccionDTO.TIPO_VALIDACION_CESVI.compareTo(negocioSeccion.getSeccionDTO().getTipoValidacionNumSerie()) == 0 &&
						NegocioSeccion.PERMITE_CONSULTA_NUM_SERIE.compareTo(negocioSeccion.getConsultaNumSerie()) == 0) {
					
					boolean vinConError = inciso.getIncisoAutoCot().getObservacionesSesa() != null &&
							!inciso.getIncisoAutoCot().getObservacionesSesa().isEmpty();
					
					if (vinConError) {
						icono = ERROR;
						mensaje = inciso.getIncisoAutoCot().getObservacionesSesa();
					} else {
						if (!inciso.getIncisoAutoCot().getCoincideEstilo()) {
							LinkedList<DiferenciasAmis> listaDiferencias = new LinkedList<DiferenciasAmis>();
							List<Respuesta> vehiculosCesvi = this.obtenerVin(inciso.getIncisoAutoCot().getNumeroSerie());
							if(vehiculosCesvi != null && vehiculosCesvi.size() != 0) {
								Map<Long, String> datosAmis = datosAmisService.getListarDatos();
								listaDiferencias= this.getDiferenciasInformacionVehicular(vehiculosCesvi, inciso.getIncisoAutoCot(), idToPoliza, datosAmis);
								boolean vinCorrecto = false;
								if(listaDiferencias != null && listaDiferencias.size() > 0) {
									//Guardar diferencias entre el inciso y cada uno de los vehiculos de cesvi
									for (DiferenciasAmis diferencias :listaDiferencias) {
										if(diferencias.getClaveError() != 0) {
											if(usuario != null && usuario.getNombre() != null) {
												diferencias.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
											} else {
												diferencias.setCodigoUsuarioCreacion(poliza.getCodigoUsuarioCreacion() != null ? poliza.getCodigoUsuarioCreacion() : "");
											}
											diferenciasAmisService.guardarDiferenciasAmis(diferencias);
										} else {
											vinCorrecto = true;
											break;
										}
									}
									
									if(!vinCorrecto){
										Collections.sort(listaDiferencias, new Comparator<DiferenciasAmis>() {
											@Override
											public int compare(DiferenciasAmis o1, DiferenciasAmis o2) {
												return o1.getNumeroDiferencias().compareTo(o2.getNumeroDiferencias());
											}
										});
										
										if(listaDiferencias.getFirst().getMensajeError() != null) {
											mensaje = listaDiferencias.getFirst().getMensajeError();
										}
										
										//Obtener mensaje con diferencias entre el inciso y el vehiculo mas parecido de cesvi
										if(listaDiferencias.getFirst().getClaveError() == 1 && !mensaje.isEmpty()) {
											mensaje = this.getMensaje(1, inciso.getIncisoAutoCot().getNumeroSerie(), " [" + listaDiferencias.getFirst().getMensajeError() + "].");
										}
									}
								}
							} else {
								//Si no se encontro informacion del numero de serie, guardar error
								mensaje = this.getMensaje(DiferenciasAmis.ERROR_VIN_NO_ENCONTRADO, inciso.getIncisoAutoCot().getNumeroSerie(), "");
								DiferenciasAmis diferenciasAmis = new DiferenciasAmis();
								diferenciasAmis = this.poblarDiferencias(diferenciasAmis, inciso.getIncisoAutoCot(), idToPoliza);
								diferenciasAmis.setClaveError(DiferenciasAmis.ERROR_VIN_NO_ENCONTRADO);
								diferenciasAmis.setMensajeError(mensaje);
								if(usuario != null && usuario.getNombre() != null) {
									diferenciasAmis.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
								} else {
									diferenciasAmis.setCodigoUsuarioCreacion(poliza.getCodigoUsuarioCreacion() != null ? poliza.getCodigoUsuarioCreacion() : "");
								}
								diferenciasAmisService.guardarDiferenciasAmis(diferenciasAmis);
							}
							
							//Guardar mensaje de error para el inciso
							if (mensaje != null && !mensaje.isEmpty()) {
								icono = ERROR;
								inciso.getIncisoAutoCot().setObservacionesSesa(mensaje);
								entidadService.save(inciso.getIncisoAutoCot());
							}
						}
					}
				}
			}
		}
		
		mensajeInformacionVehicular.put("mensaje" ,mensaje);
		mensajeInformacionVehicular.put("icono", icono);
		
		return mensajeInformacionVehicular;
	}

	private LinkedList<DiferenciasAmis> getDiferenciasInformacionVehicular (List<Respuesta> vehiculosCesvi, IncisoAutoCot auto, BigDecimal idToPoliza, Map<Long, String> datosAmis) {
		LinkedList<DiferenciasAmis> listaDiferencias = new LinkedList<DiferenciasAmis>();
		
		//Determinar numero de diferencias entre el inciso y los vehiculos retornados por cesvi
		try {
			StringBuilder mensajeError = new StringBuilder("");
			for (Respuesta informacionVehicular : vehiculosCesvi) {
				Integer numeroDiferencias = 0;
				mensajeError.delete(0,mensajeError.length());
				DiferenciasAmis diferenciasAmis = new DiferenciasAmis();

				//Si cesvi retorna un resultado diferente de 1 entonces es error del numero de serie
				if(!DiferenciasAmis.VIN_SIN_ERRORES.equals(informacionVehicular.getRESULTADO())) {
					diferenciasAmis = new DiferenciasAmis();
					listaDiferencias = new LinkedList<DiferenciasAmis>();
					diferenciasAmis = this.poblarDiferencias(diferenciasAmis, auto, idToPoliza);
					diferenciasAmis.setNumeroDiferencias(0);
					diferenciasAmis.setClaveError(Integer.parseInt(informacionVehicular.getRESULTADO()));
					diferenciasAmis.setMensajeError(this.getMensaje(Integer.parseInt(informacionVehicular.getRESULTADO()), diferenciasAmis.getNumeroSerie(), ""));
					listaDiferencias.add(diferenciasAmis);
					break;
				} else {
					//Si se encontro informacion del numero de serie, compararla con el vehiculo emitido
					String[] estiloId = null;
					estiloId = auto.getEstiloId().split("_");
					EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId(estiloId[0], estiloId[1],
								BigDecimal.valueOf(Long.valueOf(estiloId[2]))); 
					EstiloVehiculoDTO estilo = estiloVehiculoFacadeRemote.findById(estiloVehiculoId);

					if (estilo != null) {
						if (datosAmis.containsValue("MARCA") && !informacionVehicular.getMARCA().equalsIgnoreCase(estilo.getMarcaVehiculoDTO().getDescripcionMarcaVehiculo() != null ?  estilo.getMarcaVehiculoDTO().getDescripcionMarcaVehiculo() : "")) {
							boolean marcaCorrecta = false;
							List<MarcaVehiculoEquivalenciaDTO> marcaVehiculoEquivalenciaDTOs = entidadService.findByProperty(MarcaVehiculoEquivalenciaDTO.class, 
									"codigoMarcaVehiculo", estilo.getMarcaVehiculoDTO().getCodigoMarcaVehiculo() != null ? estilo.getMarcaVehiculoDTO().getCodigoMarcaVehiculo() : "");
							for (MarcaVehiculoEquivalenciaDTO marcaVehiculoEquivalenciaDTO : marcaVehiculoEquivalenciaDTOs) {
								if (informacionVehicular.getMARCA().equalsIgnoreCase(marcaVehiculoEquivalenciaDTO.getDescripcionMarcaVehiculo())) {
									marcaCorrecta = true;
									break;
								}
							}
							
							if (!marcaCorrecta) {
								diferenciasAmis.setMarca(informacionVehicular.getMARCA() != null ? informacionVehicular.getMARCA() : "");
								numeroDiferencias ++;
								mensajeError.append(mensajeError.toString().isEmpty() ? "Marca" : ", Marca");
							}
						}

						if(datosAmis.containsValue("PUERTAS") && !informacionVehicular.getPUERTAS().replaceAll(" PUERTAS", "").equalsIgnoreCase(estilo.getNumeroPuertas() != null ? estilo.getNumeroPuertas().toString() : "")) {
							diferenciasAmis.setPuertas(informacionVehicular.getPUERTAS() != null ? informacionVehicular.getPUERTAS() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Numero de puertas" : ", Numero de puertas");
						}

						if(datosAmis.containsValue("CILINDROS") && !informacionVehicular.getNO_CILINDRO().equalsIgnoreCase(this.getModificadoresDescripcion(DiferenciasAmis.GRUPO_CILINDROS, estilo.getClaveCilindros()))) {
							diferenciasAmis.setCilindros(informacionVehicular.getNO_CILINDRO() != null ? informacionVehicular.getNO_CILINDRO() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Cilindros" : ", Cilindros");
						}
							
						if(datosAmis.containsValue("COMBUSTIBLE") && !informacionVehicular.getCOMBUSTIBLE().equalsIgnoreCase(this.getModificadoresDescripcion(DiferenciasAmis.GRUPO_COMBUSTIBLE, estilo.getClaveTipoCombustible()))) {
							diferenciasAmis.setCombustible(informacionVehicular.getCOMBUSTIBLE() != null ? informacionVehicular.getCOMBUSTIBLE() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Combustible" : ", Combustible");
						}
					
						if(datosAmis.containsValue("INTERIORES") && !informacionVehicular.getINTERIORES().equalsIgnoreCase(this.getModificadoresDescripcion(DiferenciasAmis.GRUPO_VESTIDURAS, estilo.getClaveVestidura()))) {
							diferenciasAmis.setInteriores(informacionVehicular.getINTERIORES() != null ? informacionVehicular.getINTERIORES() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Interiores" : ", Interiores");
						}
							
						if(datosAmis.containsValue("PASAJEROS") && !informacionVehicular.getPASAJEROS().equalsIgnoreCase(estilo.getNumeroAsientos() != null ? estilo.getNumeroAsientos().toString() : "")) {
							diferenciasAmis.setPasajeros(informacionVehicular.getPASAJEROS() != null ? informacionVehicular.getPASAJEROS() : "");
							numeroDiferencias ++;
							mensajeError.append(mensajeError.toString().isEmpty() ? "Numero de pasajeros" : ", Numero de pasajeros");
						}
							
					}
					
					if(datosAmis.containsValue("ANO") && !informacionVehicular.getANO().equalsIgnoreCase(auto.getModeloVehiculo() != null ? auto.getModeloVehiculo().toString() : "")) {
						diferenciasAmis.setAnos(informacionVehicular.getANO() != null ? informacionVehicular.getANO() : "");
						numeroDiferencias ++;
						mensajeError.append(mensajeError.toString().isEmpty() ?  "Modelo" : ", Modelo");
					}

					if(numeroDiferencias == 0) {
						//Si se encontro que la informacion retornada por cesvi coincide exactamente con el vehiculo emitido entonces se guardan sus claves
						if(datosAmis != null && datosAmis.size() > 0) {
							auto.setClaveAmis(informacionVehicular.getCVE_AMIS());
							auto.setClaveSesa(informacionVehicular.getCVE_SESA());
							entidadService.save(auto);
						}

						diferenciasAmis = new DiferenciasAmis();
						listaDiferencias = new LinkedList<DiferenciasAmis>();
						diferenciasAmis.setClaveError(0);
						diferenciasAmis.setNumeroDiferencias(0);
						listaDiferencias.add(diferenciasAmis);
						break;
					} else {
						//Si se encontraron diferencias entre la informacion del numero de serie y el vehiculo emitido, guardar error
						diferenciasAmis = this.poblarDiferencias(diferenciasAmis, auto, idToPoliza);
						diferenciasAmis.setClaveUnica(informacionVehicular.getCLAVE_UNICA() != null ? informacionVehicular.getCLAVE_UNICA() : "");
						diferenciasAmis.setVersion(informacionVehicular.getVERSION_C() != null ? informacionVehicular.getVERSION_C() : "");
						diferenciasAmis.setNumeroDiferencias(numeroDiferencias);
						diferenciasAmis.setClaveError(DiferenciasAmis.ERROR_VIN_INFORMACION_NO_COINCIDE);
						diferenciasAmis.setMensajeError(mensajeError.toString());
						listaDiferencias.add(diferenciasAmis);
					}
				}
			}	
		} catch (Exception e) {
			LOG.error("Informacion Vehicular Individual getDiferenciasInformacionVehicular:", e);
		}

		return listaDiferencias;
	}
	
	@Override
	public String getModificadoresDescripcion (int grupo, String clave) {
		String descripcion = "";
		if(clave != null) {
			List<VarModifDescripcion> listVarModifDescripcion = null;
			VarModifDescripcion filtroVarModifDescripcion = new VarModifDescripcion();
			filtroVarModifDescripcion.setIdGrupo(grupo);
			filtroVarModifDescripcion.setId(Long.parseLong(clave));
			listVarModifDescripcion = varModifDescripcionService.findByFilters(filtroVarModifDescripcion);
			if(listVarModifDescripcion != null && listVarModifDescripcion.size()>0){
				descripcion = listVarModifDescripcion.get(0).getDescription();
			}
		}
		return descripcion;
	}
	
	@Override
	public boolean validacionActiva (BigDecimal codigoParametroGeneral) {
		boolean activa = false;
		ParametroGeneralId id = new ParametroGeneralId();
		id.setIdToGrupoParametroGeneral(new BigDecimal(13));
		id.setCodigoParametroGeneral(codigoParametroGeneral);
		ParametroGeneralDTO parametro = parametroGeneralService.findById(id );
		if(parametro != null && parametro.getValor() != null && parametro.getValor().equals("1")) {
			activa = true;
		}
		return activa;
	}
	
	@Override
	public String getMensaje (int respuesta, String vin, String mensajeAdicional) {
		String mensaje = "";

		switch (respuesta) {
			case DiferenciasAmis.ERROR_VIN_INFORMACION_NO_COINCIDE:
				mensaje = "Se encontraron diferencias de informacion para el numero de serie " + vin + mensajeAdicional;
				break;
			case DiferenciasAmis.ERROR_VIN_ALTERADO:
				mensaje = "Numero de serie " + vin + " alterado, verifique su captura.";
				break;
			case DiferenciasAmis.ERROR_VIN_NO_REGISTRADO:
				mensaje = "Numero de serie " + vin + " no registrado, favor de reportarlo a CESVI MEXICO.";
				break;
			case DiferenciasAmis.ERROR_VIN_ERRONEO:
				mensaje = "Numero de serie " + vin + " erroneo o fuera de rango.";
				break;
			case DiferenciasAmis.ERROR_VIN_INVALIDO:
				mensaje = "Se encontraron caracteres invalidos para el numero de serie " + vin + ".";
				break;
			case DiferenciasAmis.ERROR_VIN_NO_ENCONTRADO:
				mensaje = "No se encontro informacion para el numero de serie " + vin + ".";
				break;
			case DiferenciasAmis.ERROR_VIN_LONGITUD_RESPUESTA:
				mensaje = "Se recibio una respuesta inesperada para el numero de serie " + vin + ".";
				break;
			default:
				break;
		}

		return mensaje;
	}
	
	private DiferenciasAmis poblarDiferencias (DiferenciasAmis diferenciasAmis, IncisoAutoCot auto, BigDecimal idToPoliza) {
		BigDecimal idToCotizacion = auto.getIncisoCotizacionDTO().getId().getIdToCotizacion();
		BigDecimal numeroInciso = auto.getIncisoCotizacionDTO().getId().getNumeroInciso();
		
		diferenciasAmis.setIdToPoliza(idToPoliza);
		diferenciasAmis.setIdToCotizacion(idToCotizacion);
		diferenciasAmis.setNumeroInciso(numeroInciso);
		diferenciasAmis.setNumeroSerie(auto.getNumeroSerie() != null ? auto.getNumeroSerie() : "");
		diferenciasAmis.setFechaCreacion(new Date());
		diferenciasAmis.setEstatus(DiferenciasAmis.ESTATUS_PENDIENTE_REVISION);
		diferenciasAmis.setActivo(DiferenciasAmis.ACTIVO);
		return diferenciasAmis;
	}
	
	public List<Respuesta> obtenerVin(String numeroSerie) {
		List<Respuesta> lista = new ArrayList<Respuesta>();
        try {
    		//Consultar informacion vehicular
    		String vinResultadoStr = "";
    		byte[] vinResultado = this.getConsultaVin(numeroSerie);
    		if(vinResultado != null && vinResultado.length > 0) {
    			vinResultadoStr = new String(vinResultado);
    		}
    		
    		if(!vinResultadoStr.isEmpty()) {
    			String[] vehiculos = vinResultadoStr.split(SEPARADOR_VEHICULO_FIN);
            	for (String vin: vehiculos) {
            		String[] caracteristicasVehiculo = vin.trim().split(SEPARADOR_VEHICULO_CARACTERISTICAS);
            		if (caracteristicasVehiculo.length == LENGTH_ORIGINAL || 
            				caracteristicasVehiculo.length == LENGTH_CON_CLAVES) {
            			int i = 0;
            			Respuesta respuesta = new Respuesta();
            			respuesta.setCLAVE_UNICA(caracteristicasVehiculo[i++].replaceAll(SEPARADOR_VEHICULO_INICIO, ""));
            			respuesta.setSEGMENTO(caracteristicasVehiculo[i++]);
            			respuesta.setMARCA(caracteristicasVehiculo[i++]);
            			respuesta.setMODELO(caracteristicasVehiculo[i++]);
            			respuesta.setVERSION_C(caracteristicasVehiculo[i++]);
            			respuesta.setANO(caracteristicasVehiculo[i++]);
            			respuesta.setP_LISTA(caracteristicasVehiculo[i++]);
            			respuesta.setCLASE(caracteristicasVehiculo[i++]);
            			respuesta.setTIPO(caracteristicasVehiculo[i++]);
            			respuesta.setPUERTAS(caracteristicasVehiculo[i++]);
            			respuesta.setMOTOR(caracteristicasVehiculo[i++]);
            			respuesta.setNO_CILINDRO(caracteristicasVehiculo[i++]);
            			respuesta.setPOTENCIA(caracteristicasVehiculo[i++]);
            			respuesta.setCOMBUSTIBLE(caracteristicasVehiculo[i++]);
            			respuesta.setTRANSMISION(caracteristicasVehiculo[i++]);
            			respuesta.setTRACCION(caracteristicasVehiculo[i++]);
            			respuesta.setTIPO_DE_DIRECCION(caracteristicasVehiculo[i++]);
            			respuesta.setINTERIORES(caracteristicasVehiculo[i++]);
            			respuesta.setBOLSA_DE_AIRE(caracteristicasVehiculo[i++]);
            			respuesta.setAIRE_ACONDICIONADO(caracteristicasVehiculo[i++]);
            			respuesta.setELEVADORES_ELECTRICOS(caracteristicasVehiculo[i++]);
            			respuesta.setQUEMACOCOS(caracteristicasVehiculo[i++]);
            			respuesta.setESTEREO(caracteristicasVehiculo[i++]);
            			respuesta.setTIPO_DE_RIN(caracteristicasVehiculo[i++]);
            			respuesta.setRIN(caracteristicasVehiculo[i++]);
            			respuesta.setNEUMATICO(caracteristicasVehiculo[i++]);
            			respuesta.setFRENOS_DEL(caracteristicasVehiculo[i++]);
            			respuesta.setSUSPENSION_DELANTERA(caracteristicasVehiculo[i++]);
            			respuesta.setSUSPENSION_TRASERA(caracteristicasVehiculo[i++]);
            			respuesta.setTIPO_DE_CABINA(caracteristicasVehiculo[i++]);
            			respuesta.setTECHO(caracteristicasVehiculo[i++]);
            			respuesta.setPASAJEROS(caracteristicasVehiculo[i++]);
            			respuesta.setVALVULAS(caracteristicasVehiculo[i++]);
            			respuesta.setPRECIO_VENTA(caracteristicasVehiculo[i++]);
            			if (caracteristicasVehiculo.length == LENGTH_CON_CLAVES) {
            				respuesta.setCVE_SESA(caracteristicasVehiculo[i++]);
                			respuesta.setOCRA(caracteristicasVehiculo[i++]);
                			respuesta.setCVE_AMIS(caracteristicasVehiculo[i++]);
                			respuesta.setID_TRANSPORTE(caracteristicasVehiculo[i++]);
                			respuesta.setCVE_AFIRME(caracteristicasVehiculo[i++]);
            			}
            			respuesta.setVIN(caracteristicasVehiculo[i++]);
            			respuesta.setRESULTADO(caracteristicasVehiculo[i++].replaceAll(SEPARADOR_VEHICULO_FIN, ""));
            			lista.add(respuesta);
            		} else {
            			Respuesta respuesta = new Respuesta();
            			respuesta.setRESULTADO(String.valueOf(DiferenciasAmis.ERROR_VIN_LONGITUD_RESPUESTA));
            			lista.add(respuesta);
            		}
            	}
    		}
        } catch (Exception e) {
        	LOG.error("Informacion Vehicular Individual obtenerVin:", e);
        }
        return lista;
    }
	
	@Override
	public String validaInfoVehicularByEstiloCotInd(BigDecimal idToCotizacion, BigDecimal polizaId) throws Exception {
		String mensajeInformacionVehicular = "";
		try {
			if (this.validacionActiva(DiferenciasAmis.VALIDACION_VIN_INDIVIDUAL_ACTIVADA)) {
				Map<String, String> mensajeValidaInformacionVehicular = this.validaInformacionVehicular(idToCotizacion, polizaId);
				String iconoInformacionVehicular = mensajeValidaInformacionVehicular.get("icono");
				if (iconoInformacionVehicular.equals("10")) {
					mensajeInformacionVehicular = " " + mensajeValidaInformacionVehicular.get("mensaje");
				}
			}
		} catch(Exception e){
			throw e;
		}
		
		return mensajeInformacionVehicular;
	}

	@Override
	public Respuesta obtenerInformacionVehiculo(String numeroSerie) throws Exception {
		Respuesta vehiculo = new Respuesta();

		try {
			List<Respuesta> vehiculos = this.obtenerVin(numeroSerie);
			
			if (vehiculos != null && vehiculos.size() != 0) {
				vehiculo = vehiculos.get(0);
				if(!DiferenciasAmis.VIN_SIN_ERRORES.equals(vehiculo.getRESULTADO())) {
					vehiculo.setObservacionesSesa(this.getMensaje(Integer.parseInt(vehiculo.getRESULTADO()), numeroSerie, ""));
				}
			} else {
				vehiculo.setObservacionesSesa(this.getMensaje(DiferenciasAmis.ERROR_VIN_NO_ENCONTRADO, numeroSerie, ""));
			}
		} catch(Exception e){
			throw e;
		}
		
		return vehiculo;
	}
}
