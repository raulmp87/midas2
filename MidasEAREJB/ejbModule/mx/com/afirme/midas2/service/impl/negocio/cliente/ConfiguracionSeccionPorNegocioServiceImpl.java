package mx.com.afirme.midas2.service.impl.negocio.cliente;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionGrupoDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionGrupoPorNegocioDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionSeccionDao;
import mx.com.afirme.midas2.dao.negocio.cliente.ConfiguracionSeccionPorNegocioDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionCampo;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionGrupo;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionGrupoPorNegocio;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccion;
import mx.com.afirme.midas2.domain.negocio.cliente.ConfiguracionSeccionPorNegocio;
import mx.com.afirme.midas2.dto.negocio.cliente.ResultadoActualizacionNodo;
import mx.com.afirme.midas2.service.negocio.cliente.ConfiguracionSeccionPorNegocioService;

@Stateless
public class ConfiguracionSeccionPorNegocioServiceImpl implements
		ConfiguracionSeccionPorNegocioService {

	@EJB
	private ConfiguracionSeccionDao configuracionSeccionDao;
	
	@EJB
	private ConfiguracionSeccionPorNegocioDao configuracionSeccionPorNegocioDao;
	
	@EJB
	private ConfiguracionGrupoPorNegocioDao configuracionGrupoPorNegocioDao;
	
	@EJB
	private ConfiguracionGrupoDao configuracionGrupoDao;
	
	@EJB
	private EntidadDao entidadDao;
	
	@Override
	public List<ConfiguracionSeccionPorNegocio> getConfiguracionPorNegocio(Long idNegocio) {
		List<ConfiguracionSeccionPorNegocio> listaConfigNegocio = obtenerConfiguracionPorNegocio(idNegocio, true);
		return listaConfigNegocio;
	}

	@Override
	public ResultadoActualizacionNodo actualizarNodo(String idNodo,Short claveObligatorio) {
		
		String [] idArray = idNodo.split("_");
		ResultadoActualizacionNodo resultadoActualizacionNodo = new ResultadoActualizacionNodo();
		try{
		
			if(idArray[0].equals("CONFSEC")){
				
				ConfiguracionSeccionPorNegocio confSecPorNeg = configuracionSeccionPorNegocioDao.findById(new Long(idArray[1]));
				
				confSecPorNeg.setObligatoriedad(claveObligatorio);
				configuracionSeccionPorNegocioDao.update(confSecPorNeg);
				
				resultadoActualizacionNodo.setIdNegocio(confSecPorNeg.getIdNegocio().toString());
				resultadoActualizacionNodo.setIdResultado(confSecPorNeg.getId().toString());
				resultadoActualizacionNodo.setIdOriginal(idArray[1]);	
			}
			else if(idArray[0].equals("CONFGPO")){
				
				ConfiguracionGrupoPorNegocio confGpoPorNeg = configuracionGrupoPorNegocioDao.findById(new Long(idArray[1]));
				
				confGpoPorNeg.setObligatoriedad(claveObligatorio);
				configuracionGrupoPorNegocioDao.update(confGpoPorNeg);
				
				resultadoActualizacionNodo.setIdNegocio(confGpoPorNeg.getIdNegocio().toString());
				resultadoActualizacionNodo.setIdResultado(confGpoPorNeg.getId().toString());
				resultadoActualizacionNodo.setIdOriginal(idArray[1]);
			}
			else{
				throw new RuntimeException("Parámetro no válido: "+idArray[0]);
			}
		
			resultadoActualizacionNodo.setOperacionExitosa(true);
			resultadoActualizacionNodo.setTipoMensaje(SistemaPersistencia.CLAVE_MENSAJE_EXITO);
		
		}catch(Exception e){
			resultadoActualizacionNodo.setOperacionExitosa(true);
			resultadoActualizacionNodo.setTipoMensaje(SistemaPersistencia.CLAVE_MENSAJE_ERROR);
			resultadoActualizacionNodo.setMensaje("Ocurrió un error al realizar la operación. "+e.getMessage());
		}
		
		return resultadoActualizacionNodo;
	}
	
	@Override
	public List<ConfiguracionGrupo> getCamposRequeridos(Long idNegocio, Short tipoPersona) {
		return getCamposRequeridos(idNegocio, tipoPersona, null);
	}

	@Override
	public List<ConfiguracionGrupo> getCamposRequeridos(Long idNegocio, Short tipoPersona, String codigoSeccion) {
		
		List<ConfiguracionSeccionPorNegocio> listaSeccionesPorNegocio = obtenerConfiguracionPorNegocio(idNegocio, false);
		
		if(listaSeccionesPorNegocio == null || listaSeccionesPorNegocio.isEmpty()){
			listaSeccionesPorNegocio = obtenerConfiguracionPorNegocio((long) 0, false);
		}
		
		//Obtiene Configuracion curp
		boolean aplicaCurpObligatorio = false;
		Short valorCurp = 0;
		try{
			Negocio negocio = entidadDao.findById(Negocio.class, idNegocio);
			if(negocio != null){
				aplicaCurpObligatorio = negocio.getAplicaCURPObligatorio();
			}
			if(aplicaCurpObligatorio){
				valorCurp = 1;
			}
		}catch(Exception e){
		}
		
		if(listaSeccionesPorNegocio == null || listaSeccionesPorNegocio.isEmpty()){
			throw new RuntimeException("No existe configuración para el negocio: "+idNegocio);
		}
		
		List<ConfiguracionGrupo> listaResultado = new ArrayList<ConfiguracionGrupo>();
		
		for(ConfiguracionSeccionPorNegocio confSeccionNeg : listaSeccionesPorNegocio){
			
			//Se valida el código de la sección, si es != null
			if(confSeccionNeg.esObligatorio() && 
					(codigoSeccion == null || codigoSeccion.equals(confSeccionNeg.getConfiguracionSeccion().getCodigo()))){
				
				for(ConfiguracionGrupoPorNegocio confGrupo : confSeccionNeg.getConfiguracionGrupoPorNegocios()){
					
					//Se valida el tipo de persona
					if(confGrupo.getConfiguracionGrupo().getCodigo().toUpperCase().equals("CURP")){
						confGrupo.setObligatoriedad(valorCurp);
					}
					if(confGrupo.esObligatorio() && 
							(confGrupo.getConfiguracionGrupo().getTipoPersona().equals((short)3) || 
									tipoPersona.equals(confGrupo.getConfiguracionGrupo().getTipoPersona()))
							){
						//VMHS: Se agrego por que tronaba al accesar al grupo y no estaba sincronizado con el de db
						ConfiguracionGrupo configuracionGrupo=null;
						if(confGrupo.getConfiguracionGrupo()!=null){
							configuracionGrupo=confGrupo.getConfiguracionGrupo();
						}
						ConfiguracionGrupo confGrupoResultado = new ConfiguracionGrupo(
								confGrupo.getId(),configuracionGrupo.getConfiguracionSeccion(),
								configuracionGrupo.getCodigo(),
								configuracionGrupo.getDescripcion(),
								configuracionGrupo.getValorDefault(),
								configuracionGrupo.getTipoPersona(),
								new ArrayList<ConfiguracionCampo>(),null
						);
						
						for(ConfiguracionCampo confCampo : configuracionGrupo.getConfiguracionCampos()){
							
							if(confCampo.esObligatorio() && !confCampo.getCodigo().toUpperCase().equals("CURP")){								
								confGrupoResultado.getConfiguracionCampos().add(confCampo);
							}else if(confCampo.getCodigo().toUpperCase().equals("CURP")){
								if(aplicaCurpObligatorio){
									confGrupoResultado.getConfiguracionCampos().add(confCampo);
								}
								confGrupoResultado.setValorDefault(valorCurp);
								confCampo.setValorDefault(valorCurp);
							}
						}
						
						if(!confGrupoResultado.getConfiguracionCampos().isEmpty()){
							listaResultado.add(confGrupoResultado);
						}
					}
					
				}
				
			}
			
		}
		
		return listaResultado;
	}

	private List<ConfiguracionSeccionPorNegocio> obtenerConfiguracionPorNegocio(Long idNegocio,boolean siNoExisteSeCrea){

		List<ConfiguracionSeccionPorNegocio> listaConfigNegocio = configuracionSeccionPorNegocioDao.findByNegocio(idNegocio);
		
		if(listaConfigNegocio == null || listaConfigNegocio.isEmpty() && siNoExisteSeCrea){
			
			List<ConfiguracionSeccion> listaConfigSeccionGenerales = configuracionSeccionDao.findAll();
			
			listaConfigNegocio = new ArrayList<ConfiguracionSeccionPorNegocio>();
			
			List<ConfiguracionGrupoPorNegocio> listaConfigGpoPorNeg = null;
			ConfiguracionGrupoPorNegocio gpoPorNeg = null;
			ConfiguracionSeccionPorNegocio configSecPorNegocio = null;
			
			for(ConfiguracionSeccion configSeccion : listaConfigSeccionGenerales ){
				
				configSecPorNegocio = new ConfiguracionSeccionPorNegocio();
				configSecPorNegocio.setConfiguracionGrupoPorNegocios(listaConfigGpoPorNeg);
				configSecPorNegocio.setConfiguracionSeccion(configSeccion);
				configSecPorNegocio.setIdNegocio(idNegocio);
				configSecPorNegocio.setObligatoriedad(configSeccion.getValorDefault());
				
				configuracionSeccionPorNegocioDao.persist(configSecPorNegocio);
				listaConfigNegocio.add(configSecPorNegocio);
				
				configSecPorNegocio.setConfiguracionGrupoPorNegocios(new ArrayList<ConfiguracionGrupoPorNegocio>());
				
				for(ConfiguracionGrupo configGrupo : configSeccion.getConfiguracionGrupos()){
					
					gpoPorNeg = new ConfiguracionGrupoPorNegocio();
					gpoPorNeg.setConfiguracionGrupo(configGrupo);
					gpoPorNeg.setIdNegocio(idNegocio);
					gpoPorNeg.setObligatoriedad(configGrupo.getValorDefault());
					gpoPorNeg.setConfiguracionSeccionPorNegocio(configSecPorNegocio);
					
					configSecPorNegocio.getConfiguracionGrupoPorNegocios().add(gpoPorNeg);
				}
				
				configuracionSeccionPorNegocioDao.update(configSecPorNegocio);
				
			}
			
		}
		
		return listaConfigNegocio;
	}
}
