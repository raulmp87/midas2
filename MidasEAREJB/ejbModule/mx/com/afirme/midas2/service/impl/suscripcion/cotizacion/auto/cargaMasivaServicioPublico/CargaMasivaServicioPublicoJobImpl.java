package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargaMasivaServicioPublico;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.Asynchronous;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.vigencia.VigenciaDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.sistema.seguridad.SeguridadContext;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublico.EstatusArchivo;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.cargaMasivaServiciPublico.CargaMasivaServicioPublicoDetalle.EstatusDetalle;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublico;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoDeduciblesAd;
import mx.com.afirme.midas2.domain.tarifa.TarifaServicioPublicoSumaAseguradaAd;
import mx.com.afirme.midas2.dto.RelacionesTarifaServicioPublicoDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CargaMasivaServicioPublicoEstatusDescriDetalleDTO.EstatusDescriDetalle;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasivaserviciopublico.CoberturaDetalleDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.utility.ValidateInfoFile;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.motordecision.MotorDecisionAtributosService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.personadireccion.DireccionMidasService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoJobService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasivaServicioPublico.CargaMasivaServicioPublicoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.service.tarifa.TarifaServicioPublicoService;
import mx.com.afirme.midas2.util.MailService;

import org.apache.log4j.Logger;

@Stateless
public class CargaMasivaServicioPublicoJobImpl implements CargaMasivaServicioPublicoJobService{
	
	private static final Logger LOG = Logger.getLogger(CargaMasivaServicioPublicoJobImpl.class);
	
	public static final short ID_MONEDA_NACIONAL_PESOS = 484;
	private static final String ID_COBERTURA_RESP_CIVIL = "2530";
	public static final int TIPO_VALOR_DEDUCIBLE = 1;
	public static final int TIPO_VALOR_SUMA = 2;
	public static final int TIPO_VALOR_AMPARADA = 3;
	
	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@EJB
	private CargaMasivaServicioPublicoDao cargaMasivaServicioPublicoDao;
	@EJB
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	
	@EJB
	private EntidadService entidadService;
	@EJB
	private NegocioService negocioService;
	@EJB
	private DireccionMidasService direccionMidasService;
	@EJB
	private ParametroGeneralService parametroGeneralService;
	@EJB
	private MotorDecisionAtributosService motorDecisionAtributosService;
	@EJB
	private AgenteMidasService agenteMidasService;

	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private CalculoService calculoService;
	@EJB
	private SolicitudAutorizacionService autorizacionService;
	@EJB
	private CotizacionService cotizacionService;
	
	@EJB
	private TarifaServicioPublicoService tarifaServicioPublicoService;
	
	@EJB
	private CargaMasivaServicioPublicoService cargaMasivaServicioPublicoService;
	
	@EJB
	private MailService mailService;
	
	@EJB
	private BitacoraService bitacoraService;
	
	/**
	 * Método de implementado de service para la validación de campos de DTO
	 * @author SOFTNET - ISCJJBV 
	 * 
	 * @param Lista de {@link CargaMasivaServicioPublicoDetalle} listEmision
	 */
	@Asynchronous
	public void validaDatosEmision(List<CargaMasivaServicioPublicoDetalle> listEmisionDetalle, Usuario usuario){
		LOG.info("CMSP - validaDatosEmision...");
		Iterator<CargaMasivaServicioPublicoDetalle> iterEmisionDetalle = listEmisionDetalle.iterator();
		String descriDetalle = "";
		Boolean error = false;
		
		SeguridadContext context = SeguridadContext.getContext();
		context.setUsuario(usuario);
		
		CargaMasivaServicioPublicoJobService processor = this.sessionContext.getBusinessObject(CargaMasivaServicioPublicoJobService.class);
		
		LOG.info("VAL_INI VALIDACIONES");
		while (iterEmisionDetalle.hasNext()) {
			error = false;
			descriDetalle = "";
			CargaMasivaServicioPublicoEstatusDescriDetalleDTO infoItem = new CargaMasivaServicioPublicoEstatusDescriDetalleDTO(); 
			ValidateInfoFile validaCampo = new ValidateInfoFile(infoItem, negocioService, entidadService, direccionMidasService, parametroGeneralService, 
					motorDecisionAtributosService, agenteMidasService, bitacoraService);
			infoItem.setListEstatusDescri(new ArrayList<EstatusDescriDetalle>());
			
			CargaMasivaServicioPublicoDetalle item = (CargaMasivaServicioPublicoDetalle) iterEmisionDetalle.next();
			
			infoItem.setNumPoliza(item.getNumToPoliza());
			infoItem.setIdDetalle(item.getIdDetalleCargaMasiva());
			
			//EJECUTA VALIDACION DE CAMPOS
			infoItem.getListEstatusDescri().add(validaCampo.validaIdNegocio(item.getIdNegocio()));
			infoItem.getListEstatusDescri().add(validaCampo.validaEstatus(item.getEstatus()));
			infoItem.getListEstatusDescri().add(validaCampo.validaIdAgente(item.getIdAgente(), item.getIdNegocio()));
			infoItem.getListEstatusDescri().add(validaCampo.validaIdPaquete(item.getIdPaquete(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdTipoPoliza()));
			infoItem.getListEstatusDescri().add(validaCampo.validaIdCentroEmisor(item.getIdCentroEmisor(), item.getCodigoUsuarioCreacion()));
			infoItem.getListEstatusDescri().add(validaCampo.validaIdOficina(item.getIdOficina(), item.getIdAgente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaNumeroPoliza(item.getNumToPoliza(), item.getExistePolizaSeycos()));
			infoItem.getListEstatusDescri().add(validaCampo.validaNumeroLiquidacion(item.getNumLiquidacion()));
//			SE DESHABILITA ESTA VALIDACION POR EL MOMENTO (EL NUMERO DE AUTORIZACION NO ESTA EN USO)
//			infoItem.getListEstatusDescri().add(validaCampo.validaAutorizacionProsa(item.getAutorizacionProsa()));
			infoItem.getListEstatusDescri().add(validaCampo.validaFechaVigencia(item.getFechaVigencia(), item.getIdNegocio()));
			infoItem.getListEstatusDescri().add(validaCampo.validaFechaEmision(item.getFechaEmision(), item.getIdNegocio()));
			infoItem.getListEstatusDescri().add(validaCampo.validaVigencia(item.getVigencia(), item.getIdPaquete()));
			infoItem.getListEstatusDescri().add(validaCampo.validaTipoPersonaCliente(item.getTipoPersonaCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaRfcCliente(item.getRFCCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaNombreRazonSocial(item.getTipoPersonaCliente(), item.getNombreRazonSocial()));
			infoItem.getListEstatusDescri().add(validaCampo.validaApellidoPaternoCLiente(item.getApellidoPaterno()));
			infoItem.getListEstatusDescri().add(validaCampo.validaApellidoMAternoCLiente(item.getApellidoMaterno()));
			infoItem.getListEstatusDescri().add(validaCampo.validaCodigoPostal(item.getCodigoPostalCliente(), item.getVigencia(), item.getIdNegocio()));
			infoItem.getListEstatusDescri().add(validaCampo.validaCalle(item.getCalleCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaColonia(item.getColoniaCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaTelefono(item.getTelefonoCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaCveAMIS(item.getClaveAMIS()));
			infoItem.getListEstatusDescri().add(validaCampo.validaModelo(item.getModelo(), item.getIdDetalleCargaMasiva(), item.getIdPaquete(), item.getIdNegocio(), item.getIdTipoPoliza()));
			infoItem.getListEstatusDescri().add(validaCampo.validaNcRepube(item.getNcRepuve()));
			infoItem.getListEstatusDescri().add(validaCampo.validaCveUso(item.getClaveUso(), item.getLineaNegocio(), item.getIdNegocio(), item.getIdDetalleCargaMasiva(), item.getIdProducto(), item.getIdTipoPoliza()));
			infoItem.getListEstatusDescri().add(validaCampo.validaPlacas(item.getPlacas()));
			infoItem.getListEstatusDescri().add(validaCampo.validaNumeroMotor(item.getNumeroMotor()));
			infoItem.getListEstatusDescri().add(validaCampo.validaNumeroSerie(item.getNumeroSerie(),item.getNumToPoliza().toString(), usuarioService.getUsuarioActual().getNombreUsuario()));
			infoItem.getListEstatusDescri().add(validaCampo.validaValorComercial(item.getValorComercial(), item.getLimiteEquipoEspecial()));
			infoItem.getListEstatusDescri().add(validaCampo.validaTotalPasajeros(item.getTotalPasajeros()));
			infoItem.getListEstatusDescri().add(validaCampo.validaDescriVehiculo(item.getDescripcionVehiculo(), item.getClaveAMIS()));
			infoItem.getListEstatusDescri().add(validaCampo.validaDerrotero(item.getDerrotero()));
			infoItem.getListEstatusDescri().add(validaCampo.validaRamal(item.getRamal()));
			infoItem.getListEstatusDescri().add(validaCampo.validaRuta(item.getRuta()));
			infoItem.getListEstatusDescri().add(validaCampo.validaNumeroEconomico(item.getNumeroEconomico()));
			infoItem.getListEstatusDescri().add(validaCampo.validaPrimaTotal(item.getPrimaTotal()));
			infoItem.getListEstatusDescri().add(validaCampo.validaDeduDanosMateriales(item.getDeducibleDanosMateriales(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaDeduRobTotal(item.getDeducibleRoboTotal(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaLimiteRcTerceros(item.getLimiteRcTerceros(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaDeducibleRcTerceros(item.getDeducibleRcTerceros(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaLimiteGastosMedicos(item.getLimiteGastosMedicos(),item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaLimiteMuerte(item.getLimiteMuerte(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaLimiteRcViajero(item.getLimiteRcViajero(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaDedRCV(item.getDeducibleRCV(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaAsistenciaJuridica(item.getAsistenciaJuridica(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaLimiteEquipoEspecial(item.getLimiteEquipoEspecial(), item.getIdDetalleCargaMasiva(), item.getIdPaquete(), item.getIdNegocio(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaAsistenciaVial(item.getAsistenciaVial(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdPaquete(), item.getIdTipoPoliza(), item.getClaveUso(), item.getIdAgente(), item.getCodigoPostalCliente()));
			infoItem.getListEstatusDescri().add(validaCampo.validaIgualacion(item.getIgualacion()));
			infoItem.getListEstatusDescri().add(validaCampo.validaDerechos(item.getDerechos(), item.getCodigoPostalCliente(), item.getIdNegocio(), item.getIdProducto(),item.getIdTipoPoliza(), item.getIdDetalleCargaMasiva(), item.getIdPaquete(), item.getClaveUso()));
			infoItem.getListEstatusDescri().add(validaCampo.validaSolicitarAutorizacion(item.getSolicitarAutorizacion()));
			infoItem.getListEstatusDescri().add(validaCampo.validaCausaAutorizacion(item.getCausaAutorizacion()));
			infoItem.getListEstatusDescri().add(validaCampo.validaLineaNegocio(item.getLineaNegocio(), item.getIdDetalleCargaMasiva(), item.getIdNegocio(), item.getIdTipoPoliza()));
			infoItem.getListEstatusDescri().add(validaCampo.validaAnualMesesSinIntereses(item.getAnualMesesSinIntereses()));
			infoItem.getListEstatusDescri().add(validaCampo.validaIdProducto(item.getIdProducto(), item.getIdNegocio(), item.getIdDetalleCargaMasiva()));
			infoItem.getListEstatusDescri().add(validaCampo.validaIdTipoPoliza(item.getIdTipoPoliza(), item.getIdProducto(), item.getIdNegocio(), item.getIdDetalleCargaMasiva()));
		
			LOG.info("CMSP - validaciones finalizadas...");
			//LLENA LISTA DE ERRORES (numero de poliza - estatus - descripción)
			@SuppressWarnings("rawtypes")
			Iterator itInfo = infoItem.getListEstatusDescri().iterator();
			while (itInfo.hasNext()) {
				EstatusDescriDetalle it = (EstatusDescriDetalle) itInfo.next();
				if(it.getEstatusDetalle().equals(EstatusDetalle.ERROR.toString())){
					descriDetalle += it.getDescriEstatus() + "\n";
					error = true;
				}
				if(it.getEstatusDetalle().equals(EstatusDetalle.AVISO.toString())){
					descriDetalle += it.getDescriEstatus() + "\n";
				}
			}
			LOG.info("CMSP - actualizando el estatus del detalle...");
			//ACTUALIZA ESTATUS DE REGISTRO
			if(error){
				item.setEstatusProceso(EstatusDetalle.ERROR.toString());
				item.setDescripcionEstatusProceso(descriDetalle);
			}else{
				descriDetalle += "REGISTRO LISTO PARA EMITIR";
				item.setEstatusProceso(EstatusDetalle.POR_EMITIR.toString());
				item.setDescripcionEstatusProceso(descriDetalle);
			}
			processor.updateEstatusDetalleDaoTransaction(item);
		}
		LOG.info("VAL_FIN VALIDACIONES");
		LOG.info("CMSP - actualizando estatus del archivo...");
		//ACTUALIZA ESTATUS DE ARCHIVO
		if(!listEmisionDetalle.isEmpty()){
			String msj = "";
			if(error){
				listEmisionDetalle.get(0).getArchivoCargaMasivaSP_ID().setEstatusArchivoCargaMasiva(EstatusArchivo.TERMINADO_ERRORES.getEstatus());
				msj = "Existen algunas p\u00F3lizas con errores, accede al detalle para ver descripciones";
			}else{
				listEmisionDetalle.get(0).getArchivoCargaMasivaSP_ID().setEstatusArchivoCargaMasiva(EstatusArchivo.TERMINADO_EXITO.getEstatus());
				msj = "Todas las p\u00F3liazas fueron validadas con \u00E9xito.";
			}
			
			processor.updateEstatusArchivoDaoTransaction(listEmisionDetalle.get(0).getArchivoCargaMasivaSP_ID());
			try{
				this.enviarNotificacionValidacion(msj, listEmisionDetalle.get(0).getArchivoCargaMasivaSP_ID(), usuario);
			}catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try{
			LOG.info("CMSPJOB - Generando la cotizacion desde CMSPJob...");
			processor.generarCotizacionesCargaMasivaServicioPublicoTransaction(listEmisionDetalle.get(0).getArchivoCargaMasivaSP_ID(), usuario);
			LOG.info("COTIZA_REALIZADA");
		}catch(Exception ex){
			LOG.error(ex);
		}
		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateEstatusDetalleDaoTransaction(CargaMasivaServicioPublicoDetalle item){
		cargaMasivaServicioPublicoDao.updateEstatusDetalleDao(item);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void updateEstatusArchivoDaoTransaction(CargaMasivaServicioPublico archivo){
		cargaMasivaServicioPublicoDao.updateEstatusArchivoDao(archivo);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void generarCotizacionesCargaMasivaServicioPublicoTransaction(CargaMasivaServicioPublico archivo, Usuario usuario){
		LOG.info("CMSPJOB - generarCotizacionesCargaMasivaServicioPublicoTransaction ( archivo: " + archivo.getIdArchivoCargaMasiva() + ", usuario: " + usuario.getNombreUsuario() + ")");
		cargaMasivaServicioPublicoService.generarCotizacionesCargaMasivaServicioPublico(archivo, usuario);
	}

	@Override
	public boolean igualarTarifas(Long idToNegocio, BigDecimal idToProducto, BigDecimal idToTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
								  Long idMoneda, String idTcEstado, String idTcCiudad, Long idVigencia, Long totalPasajeros,
								  List<CoberturaDetalleDTO> listCoberturasDetalleDto, Double montoPrima) {
	
		boolean error = false;
		
		VigenciaDTO vigenciaDTO = new VigenciaDTO();
		MonedaDTO monedaDTO = new MonedaDTO();
		EstadoDTO estadoDTO = new EstadoDTO();
		CiudadDTO ciudadDTO = new CiudadDTO();
		short esLineaAutobuses = (short) 0;
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao.findByNegocioProductoTipoPolizaSeccionPaquete(idToNegocio, idToProducto, idToTipoPoliza, idToSeccion, idPaquete);
		monedaDTO = entidadService.findById(MonedaDTO.class, idMoneda.shortValue());
		estadoDTO = entidadService.findById(EstadoDTO.class, idTcEstado);
		ciudadDTO = entidadService.findById(CiudadDTO.class, idTcCiudad);
		vigenciaDTO = entidadService.findById(VigenciaDTO.class, new BigDecimal(idVigencia.toString()));
		esLineaAutobuses=(tarifaServicioPublicoService.esParametroValido(new BigDecimal(negocioPaqueteSeccion.getNegocioSeccion().getIdToNegSeccion().toString()),ParametroGeneralDTO.CODIGO_PARAM_GENERAL_LINEA_AUTOBUSES_SERVICIO_PUBLICO)) ? new Short("1") : new Short("0");
		
		TarifaServicioPublico tarifaServicioPublico = new TarifaServicioPublico();
		tarifaServicioPublico.setMonedaDTO(monedaDTO);
		tarifaServicioPublico.setNegocioPaqueteSeccion(negocioPaqueteSeccion);
		tarifaServicioPublico.setVigenciaDTO(vigenciaDTO);
		if(estadoDTO != null){
			tarifaServicioPublico.setEstadoDTO(estadoDTO);
		}else{
			estadoDTO = new EstadoDTO();
			tarifaServicioPublico.setEstadoDTO(estadoDTO);			
		}
		if(ciudadDTO != null){
			tarifaServicioPublico.setCiudadDTO(ciudadDTO);
		}else{
			ciudadDTO = new CiudadDTO();
			tarifaServicioPublico.setCiudadDTO(ciudadDTO);			
		}
		
		RelacionesTarifaServicioPublicoDTO relacionesTarifasSP = tarifaServicioPublicoService.getTarifasServicioPublico(tarifaServicioPublico); 
		
		error = this.realizaIgualacionTarifa(listCoberturasDetalleDto, relacionesTarifasSP, totalPasajeros, esLineaAutobuses, montoPrima);
		
		return error;
	}

	@Override
	public boolean procesaIgualacion(BigDecimal idToCotizacion, Double primaTotalAIgualar, Boolean restaurarDescuento, Usuario usuario) {
		boolean error = true;
		try{
			if(idToCotizacion == null || primaTotalAIgualar == null){
				error = true;
			}else{
				calculoService.igualarPrima(idToCotizacion, primaTotalAIgualar.doubleValue(), restaurarDescuento);
				
				TerminarCotizacionDTO terminarCotizacionDTO = cotizacionService.validaIgualacionCotizadorAgentes(idToCotizacion);
				
				Long solicitudId = null;
				if(terminarCotizacionDTO != null){
					if(terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
						solicitudId = autorizacionService.guardarSolicitudDeAutorizacion(idToCotizacion, "", 
								usuario.getId().longValue(), usuario.getId().longValue(),
								"A", terminarCotizacionDTO.getExcepcionesList());
					}
				}
				if(solicitudId == null
						&& terminarCotizacionDTO != null){
					error = true;
				}else{
					error = false;
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
			error = true;
		}
		return error;
	}
	
	/******************METODOS PRIVADOS DE USO LOCAL***************************/

	
	private void enviarNotificacionValidacion(String msj, CargaMasivaServicioPublico archivo, Usuario usuario){
		if(usuario.getEmail() != null && usuario.getEmail() != "" ){
			List<String> destinatarios = new ArrayList<String>();
			destinatarios.add(usuario.getEmail());
			
			String title = "Resumen de validaci\u00F3n de los campos del archivo.";
			
			StringBuilder body = new StringBuilder();
			body.append("Se realizo la carga masiva del archivo: ");
			body.append(archivo.getNombreArchivoCargaMasiva().toString());
			body.append("<br>Con el siguiente resumen: <br>");
			
			body.append(msj);
			
			try {
				mailService.sendMail(destinatarios, title, body.toString());
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}
	
	private boolean realizaIgualacionTarifa (List<CoberturaDetalleDTO> listCoberturasDetalleDto, RelacionesTarifaServicioPublicoDTO relacionesTarifasSP, Long totalPasajeros, short esLineaAutobuses, Double montoPrima){
		boolean error = true;
		String valorSuma = "";
		Double primaTotal = 0.00;
		CoberturaDetalleDTO sumaRespCivilDetalle = new CoberturaDetalleDTO();
		CoberturaDetalleDTO deducibleRespCivilDetalle = new CoberturaDetalleDTO();
		
		List<CoberturaDetalleDTO> listCoberturasDetalleDtoCopy = new ArrayList<CoberturaDetalleDTO>(listCoberturasDetalleDto);
		
		for(CoberturaDetalleDTO cobDetDto : listCoberturasDetalleDtoCopy){
			if(cobDetDto.getIdToCobertura().toString().equals(ID_COBERTURA_RESP_CIVIL)){
				if(cobDetDto.getTipoValor() == TIPO_VALOR_DEDUCIBLE){//1 - Deducible
					deducibleRespCivilDetalle = cobDetDto;
					listCoberturasDetalleDto.remove(cobDetDto);
					error = false;
				}else if(cobDetDto.getTipoValor() == TIPO_VALOR_SUMA){//2 - Suma asegurada (Límite)
					sumaRespCivilDetalle = cobDetDto;
					listCoberturasDetalleDto.remove(cobDetDto);
					error = false;
				}else{
					error = true;
				}
			}
		}
		
		if(!error){
			for(TarifaServicioPublicoDeduciblesAd tarifaSpDe : relacionesTarifasSP.getDeduciblesAdicionales()){
				if(tarifaSpDe.getCoberturaDTO().getIdToCobertura().toString().equals(ID_COBERTURA_RESP_CIVIL)){
					if(tarifaSpDe.getValorSumaAsegurada().toString().equals(sumaRespCivilDetalle.getValor().toString())){
						if(tarifaSpDe.getNegocioDeducibleCob().getValorDeducible().toString().equals(deducibleRespCivilDetalle.getValor().toString())){
							if(esLineaAutobuses == (short)1){
								error = false;
								if(totalPasajeros == 25){
									primaTotal = tarifaSpDe.getValorDeducible1();
								}else if(totalPasajeros >= 26 && totalPasajeros <= 29){
									primaTotal = tarifaSpDe.getValorDeducible2();
								}else if(totalPasajeros == 30){
									primaTotal = tarifaSpDe.getValorDeducible3();
								}else if(totalPasajeros >= 31 && totalPasajeros <= 35){
									primaTotal = tarifaSpDe.getValorDeducible4();
								}else if(totalPasajeros >= 36 && totalPasajeros <= 40){
									primaTotal = tarifaSpDe.getValorDeducible5();
								}else if(totalPasajeros >= 41 && totalPasajeros <= 45){
									primaTotal = tarifaSpDe.getValorDeducible6();
								}else{
									error = true;
								}
							}else if (esLineaAutobuses == (short) 0){
								error = false;
								if(totalPasajeros >= 3 && totalPasajeros <= 5){
									primaTotal = tarifaSpDe.getValorDeducible1();
								}else if(totalPasajeros >= 6 && totalPasajeros <= 15){
									primaTotal = tarifaSpDe.getValorDeducible2();
								}else if(totalPasajeros >= 16 && totalPasajeros <= 23){
									primaTotal = tarifaSpDe.getValorDeducible3();
								}else{
									error = true;
								}
							}else{
								error = true;
							}
							//ojos
							if(!error){
								String esAdicional = "NO";
								for(TarifaServicioPublico adicional : relacionesTarifasSP.getAdicionales()){
									for(CoberturaDetalleDTO cobDetDto : listCoberturasDetalleDto){
										if(cobDetDto.getIdToCobertura().toString().equals(adicional.getCoberturaDTO().getIdToCobertura().toString())){
											if(cobDetDto.getTipoValor() == TIPO_VALOR_AMPARADA){
												valorSuma = CoberturaDTO.DESCRIPCION_TIPO_SUMA_ASEGURADA_AMPARADA;
											}else{
												valorSuma = cobDetDto.getValor() == null ? "0.0" : cobDetDto.getValor().toString();
											}
											if(adicional.getValorSumaAsegurada().toString().equals(valorSuma)){
												primaTotal += adicional.getPrima();
												esAdicional = "SI";
												break;
											}
										}
									}
								}
								if(esAdicional.equals("NO")){
									for(TarifaServicioPublicoSumaAseguradaAd sumaAseAdicional : relacionesTarifasSP.getSumasAseguradasAdicionales()){
										for(CoberturaDetalleDTO cobDetDto : listCoberturasDetalleDto){
											if(cobDetDto.getIdToCobertura().toString().equals(sumaAseAdicional.getCoberturaDTO().getIdToCobertura().toString())){
												if(sumaAseAdicional.getValorSumaAsegurada().toString().equals(cobDetDto.getValor().toString())){
													primaTotal += sumaAseAdicional.getPrima();
													break;
												}
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
		if(primaTotal.toString().equals(montoPrima.toString())){
			error = false;
		}else{
			error = true;
		}
		
		return error;
	}
}
