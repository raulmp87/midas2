package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.inciso.configuracion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;
import mx.com.afirme.midas.base.TipoHorizontal;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.catalogos.EntidadServiceImpl;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoGrupoProliberAJService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoGrupoProliberAVService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

@Stateless
public class ConfiguracionDatoIncisoServiceImpl extends EntidadServiceImpl
		implements ConfiguracionDatoIncisoService {
	public enum NivelConfiguracion {
		RAMO, SUBRAMO, COBERTURA
	}

	private ConfiguracionDatoIncisoDao configuracionDatoIncisoDao;
	private CotizacionFacadeRemote cotizacionFacadeRemote;
	private RamoTipoPolizaFacadeRemote ramoTipoPolizaFacadeRemote;
	private SubRamoFacadeRemote subRamoFacadeRemote;
	private CoberturaCotizacionFacadeRemote coberturaCotizacionFacadeRemote;
	private DatoIncisoCotAutoService datoIncisoCotAutoService;
	private IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote;
	private NegocioTarifaService negocioTarifaService;
	private TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService;
	private EntidadService entidadService;
	private ServiceLocatorP serviceLocator;
	private DatoIncisoCotAutoDao datoIncisoCotAutoDao;
	private static final String COBERTURA_VALORCERO="Remolques";
	
	
	@Override
	public List<ConfiguracionDatoInciso> getDatosRamoInciso(BigDecimal idTcRamo) {
		return configuracionDatoIncisoDao.getDatosRamoInciso(idTcRamo,ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus());
	}

	@Override
	public List<ConfiguracionDatoInciso> getDatosSubRamoInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo) {
		return configuracionDatoIncisoDao.getDatosSubRamoInciso(idTcRamo,
				idTcSubRamo, ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus());
	}

	@Override
	public List<ConfiguracionDatoInciso> getDatosCoberturaInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo,
			BigDecimal idToCobertura) {
		return configuracionDatoIncisoDao.getDatosCoberturaInciso(idTcRamo,
				idTcSubRamo, idToCobertura,ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus());
	}
	
	@Override
	public ControlDinamicoRiesgoDTO findByConfiguracionDatoIncisoId(ConfiguracionDatoIncisoId id) {
		ConfiguracionDatoInciso configuracionDatoInciso = configuracionDatoIncisoDao.findById(id);
		
		ControlDinamicoRiesgoDTO control = null;
		if (configuracionDatoInciso != null) { 
			control = new ControlDinamicoRiesgoDTO();
			control.setDependencia(configuracionDatoInciso.getDescripcionClaseRemota());
			control.setEtiqueta(configuracionDatoInciso.getDescripcionEtiqueta());
			control.setId(getIdControl(configuracionDatoInciso));
			control.setTipoControl(convertirTipoControl(configuracionDatoInciso.getClaveTipoControl()));
			control.setTipoValidador(convertirTipoValidador(configuracionDatoInciso));
			control.setTipoDependencia(null);
		}
		return control;
	}

	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(
			Map<String, String> valores, BigDecimal idToCotizacion,
			BigDecimal numeroInciso) {
			
			IncisoCotizacionDTO inciso = new IncisoCotizacionDTO();
			try{
				CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
				inciso = entidadService.findById(IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, numeroInciso));
				Short estatusConfiguracion = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
				if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
					estatusConfiguracion = ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus();
				}
				return this.getDatosRiesgo(valores,estatusConfiguracion,inciso);
			}catch(RuntimeException e){
				throw e;
			}
	}
	
	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgoCotizacion(
			Map<String, String> valores,IncisoCotizacionDTO inciso) {
			//TODO formar inciso para mandarlo
			return this.getDatosRiesgo(valores, ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus(),inciso);
	}
	
	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(
			Map<String, String> valores,Short claveFiltroRIesgo,IncisoCotizacionDTO inciso) {
		List<ControlDinamicoRiesgoDTO> controles = new ArrayList<ControlDinamicoRiesgoDTO>(1);
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, inciso.getId().getIdToCotizacion());
		List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList(); 	
		final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
			+ "(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion " +
					" and toc.id.numeroInciso = :numeroInciso )";
		Map<String, Object>map = new HashMap<String, Object>(1);
		map.put("idToCotizacion", cotizacion.getIdToCotizacion());
		map.put("numeroInciso", inciso.getId().getNumeroInciso());
		@SuppressWarnings({ "unchecked" })
		List<SubRamoDTO> subRamos = entidadService.executeQueryMultipleResult(queryString, map);
		List<ConfiguracionDatoInciso> configuracion = 
			configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(claveFiltroRIesgo);
		List<DatoIncisoCotAuto> riesgoCotizacion = datoIncisoCotAutoDao.getDatosIncisoCotAutoPorCotizacion(cotizacion.getIdToCotizacion());
		validaConfiguracionDatosRiesgo(controles, configuracion, riesgoCotizacion, inciso, ramos, subRamos);
		return controles;
		
		
//		// 1. Debemos de hacer un barrido de los controles por
//		// tipo ramo, subramo y cobertura
//		List<ControlDinamicoRiesgoDTO> controles = new ArrayList<ControlDinamicoRiesgoDTO>();
//		List<ConfiguracionDatoInciso> configuracionRamo = new ArrayList<ConfiguracionDatoInciso>();
//		List<ConfiguracionDatoInciso> configuracionSubRamo = new ArrayList<ConfiguracionDatoInciso>();
//		List<ConfiguracionDatoInciso> configuracionCobertura = new ArrayList<ConfiguracionDatoInciso>();
//
//		//TODO: Filtrar por Cotizacion e Inciso
//		List<RamoTipoPolizaDTO> ramos = ramoTipoPolizaFacadeRemote
//				.findByProperty("id.idtotipopoliza", cotizacion
//						.getTipoPolizaDTO().getIdToTipoPoliza());
//		
//		CoberturaCotizacionDTO filtro = new CoberturaCotizacionDTO();
//		filtro.setId(new CoberturaCotizacionId());
//		filtro.getId().setIdToCotizacion(inciso.getId().getIdToCotizacion());
//		filtro.getId().setNumeroInciso(inciso.getId().getNumeroInciso());
//		filtro.setClaveContrato((short)1);
//
//		for (RamoTipoPolizaDTO ramoTipoPolizaDTO : ramos) {
//			
//			// Datos de riesgo por ramo.
//			configuracionRamo = configuracionDatoIncisoDao
//					.getDatosRamoInciso(ramoTipoPolizaDTO.getId().getIdtcramo(),claveFiltroRIesgo);
//			//TODO Poblar la lista de controles a nivel Ramo
//			populateControlList(controles,	ConfiguracionDatoIncisoServiceImpl.NivelConfiguracion.RAMO, 
//					configuracionRamo, inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso(), ramoTipoPolizaDTO.getId().getIdtcramo(), 
//					new BigDecimal(0), new BigDecimal(0), valores, ramoTipoPolizaDTO.getRamoDTO().getDescripcion(),inciso);
//			
//			
//			
//			List<SubRamoDTO> subRamos = subRamoFacadeRemote
//					.getSubRamosPorIncisoCotizacion(inciso.getId().getIdToCotizacion(),
//							inciso.getId().getNumeroInciso());
//			for (SubRamoDTO subRamo : subRamos) {
//				// Datos de riesgo por ramo.
//				configuracionSubRamo = configuracionDatoIncisoDao
//						.getDatosSubRamoInciso(ramoTipoPolizaDTO.getId().getIdtcramo(), subRamo.getIdTcSubRamo(),claveFiltroRIesgo);
//				//TODO Poblar la lista de controles a nivel Ramo
//				populateControlList(controles,	ConfiguracionDatoIncisoServiceImpl.NivelConfiguracion.SUBRAMO, 
//						configuracionSubRamo, inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso(), 
//						ramoTipoPolizaDTO.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), 
//						new BigDecimal(0), valores, subRamo.getDescripcionSubRamo(),inciso);
//				
//				
//				filtro.setIdTcSubramo(subRamo.getIdTcSubRamo());
//				List<CoberturaCotizacionDTO> coberturas = coberturaCotizacionFacadeRemote
//						.listarFiltrado(filtro);				
//				for(CoberturaCotizacionDTO cobertura: coberturas){
//					
//					configuracionCobertura = configuracionDatoIncisoDao
//							.getDatosCoberturaInciso(ramoTipoPolizaDTO.getId().getIdtcramo(),
//									subRamo.getIdTcSubRamo(), cobertura.getId()
//											.getIdToCobertura(),claveFiltroRIesgo);
//					//TODO Poblar la lista de controles a nivel Cobertura
//					populateControlList(controles,	ConfiguracionDatoIncisoServiceImpl.NivelConfiguracion.COBERTURA, 
//							configuracionCobertura, inciso.getId().getIdToCotizacion(), inciso.getId().getNumeroInciso(), 
//							ramoTipoPolizaDTO.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), 
//							cobertura.getId().getIdToCobertura(), valores, cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion(),inciso);
//				}
//				
//			}
//		}
//		populateList(configuracion, controlDinamicoRamos,
//				cotizacion.getIdToCotizacion(), valores, codigoCampos);		
		//return controles;
	}
	
	/**
	 * Valida si existen configuraciones de riego complementadas para un inciso
	 * @param configuracionPrincipal
	 * @param datoRiesgoCotizacion
	 * @param seccion
	 * @param ramos
	 * @return
	 */
	private void validaConfiguracionDatosRiesgo(List<ControlDinamicoRiesgoDTO> controles, List<ConfiguracionDatoInciso> configuracionPrincipal, 
			List<DatoIncisoCotAuto> datoRiesgoCotizacion, IncisoCotizacionDTO inciso, List<RamoTipoPolizaDTO> ramos, List<SubRamoDTO> subRamos){					
		List<ConfiguracionDatoInciso> configuraciones = null;
		List<DatoIncisoCotAuto> datosIncisoCotAuto = null;
		//resolver lazy list
		inciso = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId()); 
		SeccionCotizacionDTO seccion = inciso.getSeccionCotizacion();
		for (RamoTipoPolizaDTO ramo : ramos){	
			configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
					ramo.getId().getIdtcramo(),BigDecimal.valueOf(0d), BigDecimal.valueOf(0d));
			if(configuraciones != null){
				for(ConfiguracionDatoInciso configuracion : configuraciones){
					datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, seccion.getId().getIdToSeccion(), 
							seccion.getId().getNumeroInciso(), BigDecimal.valueOf(0d), ramo.getId().getIdtcramo(), 
							BigDecimal.valueOf(0d), BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
					crearControles(controles, configuracion, datosIncisoCotAuto, 
							ramo.getRamoDTO().getDescripcion(), inciso);		
				}
			}
			for (SubRamoDTO subRamo : subRamos) {
				configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
						ramo.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d));
				if(configuraciones != null){
					for(ConfiguracionDatoInciso configuracion : configuraciones){
						datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, seccion.getId().getIdToSeccion(), 
								seccion.getId().getNumeroInciso(), BigDecimal.valueOf(0d), ramo.getId().getIdtcramo(), 
								subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
						crearControles(controles, configuracion, datosIncisoCotAuto, 
								subRamo.getDescripcionSubRamo(), inciso);		
					}
				}				
			}
			for(CoberturaCotizacionDTO cobertura : seccion.getCoberturaCotizacionLista()){
				
				if(cobertura.getClaveContratoBoolean().booleanValue()){
					BigDecimal idSubRamo = cobertura.getIdTcSubramo();
					configuraciones = encontrarConfiguracion(configuracionPrincipal, cobertura.getId().getIdToCobertura(), 
							ramo.getId().getIdtcramo(), idSubRamo, BigDecimal.valueOf(0d));
					if(configuraciones != null
							&& !configuraciones.isEmpty()){
						CoberturaSeccionDTO coberturaSeccion;
						if(cobertura.getCoberturaSeccionDTO() == null){
						CoberturaSeccionDTOId idCobSeccion = new CoberturaSeccionDTOId();
						idCobSeccion.setIdtocobertura(cobertura.getId().getIdToCobertura());
						idCobSeccion.setIdtoseccion(cobertura.getId().getIdToSeccion());
						coberturaSeccion = entidadService.findById(CoberturaSeccionDTO.class, idCobSeccion);
						}else{
							coberturaSeccion = cobertura.getCoberturaSeccionDTO();
						}
						for(ConfiguracionDatoInciso configuracion : configuraciones){
							datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, seccion.getId().getIdToSeccion(), 
									seccion.getId().getNumeroInciso(), cobertura.getId().getIdToCobertura(), ramo.getId().getIdtcramo(), 
									idSubRamo, BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
							crearControles(controles, configuracion, datosIncisoCotAuto, 
									coberturaSeccion.getCoberturaDTO().getNombreComercial(), inciso);													
						}
					}					
				}
			}			
		}		
		
	}
	

	public void populateControlList(
			List<ControlDinamicoRiesgoDTO> controles,			
			NivelConfiguracion nivelConfiguracion,
			List<ConfiguracionDatoInciso> configuracion,
			BigDecimal idCotizacion,
			BigDecimal numeroInciso,
			BigDecimal idTcRamo,
			BigDecimal idTcSubRamo,
			BigDecimal idToCobertura,
			Map<String, String> valores,
			String descripcion, IncisoCotizacionDTO inciso) {
		List<DatoIncisoCotAuto> datos = null; 
		switch (nivelConfiguracion) {
		case RAMO:
			//Buscar Coincidencias en DatoIncisoAuto por Ramo
			datos = datoIncisoCotAutoService.getDatosRamoInciso(idCotizacion, numeroInciso,idTcRamo);
			if(datos != null && !datos.isEmpty()){
				
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}else{
				//Crear lista de controles por ramo
				// si la lista configuracion es diferente de vacio
				//generar la lista de controles apartir de la configuracion
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}
			break;
		case SUBRAMO:
			//Buscar Coincidencias en DatoIncisoAuto por SubRamo
			datos = datoIncisoCotAutoService.getDatosSubRamoInciso(idCotizacion, numeroInciso,idTcRamo, idTcSubRamo);
			if(datos != null && !datos.isEmpty()){
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}else{
				//Crear lista de controles por ramo
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}			
			break;

		case COBERTURA:
			//Buscar Coincidencias en DatoIncisoAuto por Cobertura
			datos = datoIncisoCotAutoService.getDatosCoberturaInciso(idCotizacion, numeroInciso,idTcRamo, idTcSubRamo,idToCobertura);
			if(datos != null && !datos.isEmpty()){
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}else{
				//Crear lista de controles por ramo
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}			
			break;
		default:
			break;
		}
	}
	
	public void crearControles(List<ControlDinamicoRiesgoDTO> controles,
			ConfiguracionDatoInciso configuracion, List<DatoIncisoCotAuto> datos,
			String descripcion, IncisoCotizacionDTO inciso){
		
			
			ControlDinamicoRiesgoDTO control = new ControlDinamicoRiesgoDTO();
			control.setDependencia(configuracion.getDescripcionClaseRemota());
			control.setEtiqueta(configuracion.getDescripcionEtiqueta());
			control.setId(getIdControl(configuracion));
			control.setTipoControl(convertirTipoControl(configuracion.getClaveTipoControl()));
			control.setTipoValidador(convertirTipoValidador(configuracion));
			control.setVisible(true);
			control.setLista(obtenerLista(control.getTipoControl(), control.getDependencia(),inciso, configuracion.getIdGrupo()));
			control.setTipoDependencia(null);
			control.setDescripcionNivel(descripcion);
			control.setIdNivel(getIdNivel(configuracion));

			// Actualmente no existe un valor por valor
			if(configuracion!=null && configuracion.getDescripcionEtiqueta() !=null 
					&& configuracion.getDescripcionEtiqueta().contains( COBERTURA_VALORCERO ) ) { 
				control.setValor("0"); // Evita que se muestre en primera opción
			}
			
			if(datos != null){
				for(DatoIncisoCotAuto dato : datos){
					if(getIdControl(configuracion).equals(getIdControl(dato))){
						control.setValor(dato.getValor());
					}
				}
			}

			controles.add(control);
		
	}

	public void crearControles(List<ControlDinamicoRiesgoDTO> controles,
			List<ConfiguracionDatoInciso> configuracion, List<DatoIncisoCotAuto> datos,
			String descripcion, IncisoCotizacionDTO inciso){
		for(ConfiguracionDatoInciso item : configuracion){
			
			ControlDinamicoRiesgoDTO control = new ControlDinamicoRiesgoDTO();
			control.setDependencia(item.getDescripcionClaseRemota());
			control.setEtiqueta(item.getDescripcionEtiqueta());
			control.setId(getIdControl(item));
			control.setTipoControl(convertirTipoControl(item.getClaveTipoControl()));
			control.setTipoValidador(convertirTipoValidador(item));
			control.setVisible(true);
			control.setLista(obtenerLista(control.getTipoControl(), control.getDependencia(),inciso, item.getIdGrupo()));
			control.setTipoDependencia(null);
			control.setDescripcionNivel(descripcion.substring(descripcion.indexOf("-")+1));
			control.setIdNivel(getIdNivel(item));
			
			if(datos != null){
				for(DatoIncisoCotAuto dato : datos){
					if(getIdControl(item).equals(getIdControl(dato))){
						control.setValor(dato.getValor());
					}
				}
			}

			controles.add(control);
		}
	}
	public void populateControlDinamico(
			List<ControlDinamicoRiesgoDTO> controles,
			List<ConfiguracionDatoInciso> configuracion,
			Map<String, String> valores, BigDecimal idCotizacion,
			BigDecimal numeroInciso, List<DatoIncisoCotAuto> datos,
			String descripcion, IncisoCotizacionDTO inciso) {
		if (controles == null)
			controles = new ArrayList<ControlDinamicoRiesgoDTO>();

		crearControles(controles, configuracion, datos, descripcion,inciso);

	}
	
	/**
	 * 
	 */
	private String getIdControl(DatoIncisoCotAuto id) {
		String del ="_";
		String controlId = id.getIdTcRamo() + del
				+ id.getIdTcSubRamo() + del + id.getIdToCobertura() + del
				+ id.getClaveDetalle() + del + id.getIdDato();
		return controlId;
	}
	
	private String getIdControl(ConfiguracionDatoInciso configuracionDatoInciso) {
		ConfiguracionDatoIncisoId id = configuracionDatoInciso.getId();
		String del ="_";
		String controlId = id.getIdTcRamo() + del
				+ id.getIdTcSubRamo() + del + id.getIdToCobertura() + del
				+ id.getClaveDetalle() + del + id.getIdDato();
		return controlId;
	}
	
	private String getIdNivel(ConfiguracionDatoInciso configuracionDatoInciso) {
		ConfiguracionDatoIncisoId id = configuracionDatoInciso.getId();
		String del ="_";
		String idNivel = id.getIdTcRamo() + del
				+ id.getIdTcSubRamo() + del + id.getIdToCobertura();
		return idNivel;
	}
	
	private Integer convertirTipoControl(Short tipoControl) {
		int tipo = 0;

		switch (tipoControl) {
		case 1:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_CATALOGO_PROPIO;
			break;
		case 2:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_VALORES_FIJOS;
			break;
		case 3:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_TEXTFIELD;
			break;
		case 4:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_TEXTAREA;
			break;
		case 5:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_CHECKBOX;
		}
		
		return tipo;
	}
	
	private Integer convertirTipoValidador(ConfiguracionDatoInciso configuracionDatoInciso) {
		Integer tipoValidador = null;
		Short validadorMidas = configuracionDatoInciso.getClaveTipoValidacion();
			if (validadorMidas != null && validadorMidas > 0) {
				switch (validadorMidas) {
				case 1:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO;
					break;
				case 2:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_DOS;
					break;
				case 3:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS;
					break;
				case 4:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO;
					break;
				case 5:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO;
					break;
				}
			}
		return tipoValidador;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, String> obtenerLista(Integer tipoControl, String dependencia, IncisoCotizacionDTO inciso, Short idGrupo){
		Map<String, String> lista = new LinkedHashMap<String, String>();
		if(tipoControl == ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_CATALOGO_PROPIO || tipoControl == ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_VALORES_FIJOS){
			try {
				serviceLocator = ServiceLocatorP.getInstance();
				@SuppressWarnings("rawtypes")
				MidasInterfaceBaseAuto beanBase = serviceLocator.getEJB(dependencia);
				@SuppressWarnings("rawtypes")
				List optionList = null;
			
				if(beanBase instanceof ConfiguracionIncisoGrupoProliberAJService || beanBase instanceof ConfiguracionIncisoGrupoProliberAVService){
					EstiloVehiculoId estiloId = new EstiloVehiculoId();
					estiloId.valueOf(inciso.getIncisoAutoCot().getEstiloId());
					EstiloVehiculoDTO estiloDTO = entidadService.findById(EstiloVehiculoDTO.class,estiloId);
					TarifaAgrupadorTarifa tarifaAgrupadorTarifa = tarifaAgrupadorTarifaService.getPorNegocioAgrupadorTarifaSeccion(negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(new BigDecimal(inciso.getIncisoAutoCot().getNegocioSeccionId()),inciso.getCotizacionDTO().getIdMoneda()));
					NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class,new BigDecimal(inciso.getIncisoAutoCot().getNegocioSeccionId()));
					optionList = beanBase.findAll(new BigDecimal[]{new BigDecimal(tarifaAgrupadorTarifa.getId().getIdToAgrupadorTarifa()),new BigDecimal(tarifaAgrupadorTarifa.getId().getIdVerAgrupadorTarifa()),
							negocioSeccion.getSeccionDTO().getIdToSeccion(),inciso.getCotizacionDTO().getIdMoneda(),estiloDTO.getIdTcTipoVehiculo()});
				}else if((beanBase instanceof CatalogoValorFijoFacadeRemote) && idGrupo != null){
					int grupoTipoCarga = idGrupo;
					optionList = ((CatalogoValorFijoFacadeRemote) beanBase).findByProperty("id.idGrupoValores", grupoTipoCarga);
				}
				else{
					optionList = beanBase.findAll();
				}
				
				lista = convertCacheableDTOListToMap(optionList);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		return lista;
	}
	
	private Map<String, String> convertCacheableDTOListToMap(List<? extends CacheableDTO> list) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (CacheableDTO cacheableDTO : list) {
			Object id = cacheableDTO.getId();
			String strId = null;
			if (id instanceof BigDecimal) {
				strId = ((BigDecimal) id).toPlainString();
			} else if (cacheableDTO.getId() instanceof CatalogoValorFijoId) {
				CatalogoValorFijoId idCast = (CatalogoValorFijoId)cacheableDTO.getId();
				map.put(String.valueOf(idCast.getIdDato()),cacheableDTO.getDescription());
			}else {
				strId = id.toString();
				map.put(strId, cacheableDTO.getDescription());
			}
		}
		return map;
	}
	

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isHorizontal(String idDatoRiesgo, String id)
			throws IllegalArgumentException {
		return isHorizontal(getConfiguracionDatoIncisoId(idDatoRiesgo), new BigDecimal(id));
	}
	
	private boolean isHorizontal(ConfiguracionDatoIncisoId configDatoIncisoId, BigDecimal id) {
		boolean isHorizontal = false;
		Object obj = findCacheableDTOById(configDatoIncisoId, id);
		TipoHorizontal tipoHorizontal = null;
		if (obj instanceof TipoHorizontal) {
			tipoHorizontal = (TipoHorizontal) obj;
			Boolean hor = tipoHorizontal.getTipoHorizontal();
			if (hor != null && hor) {
				isHorizontal = true;
			}
		} else {
			throw new IllegalArgumentException("El objeto encontrado para la configuracion no implementa TipoHorizontal.");
		}
		return isHorizontal;
	}
	
	private CacheableDTO findCacheableDTOById(ConfiguracionDatoIncisoId configDatoIncisoId, BigDecimal id) {
		//Ir a buscar la configuracion.
		ConfiguracionDatoInciso configuracion = 
			findConfiguracionDatoIncisoCotizacionDTOById(configDatoIncisoId);
		if (configuracion == null) {
			throw new IllegalArgumentException("No se pudo encontrar ConfiguracionDatoIncisoCotizacion.");
		}
		
		String jndi = configuracion.getDescripcionClaseRemota();
		MidasInterfaceBase<? extends CacheableDTO> midasInterfaceBase = lookupMidasInterfaceBase(jndi);
		CacheableDTO cacheableDTO = midasInterfaceBase.findById(id);
		if (cacheableDTO == null) {
			throw new IllegalArgumentException(
					"No se encontro coincidencia para el id " + id
							+ " por medio de la busqueda realizada con: "
							+ jndi);
		}		
		return cacheableDTO;
	}
	
	@SuppressWarnings("unchecked")
	private MidasInterfaceBase<? extends CacheableDTO> lookupMidasInterfaceBase(String jndiName) {
		return 	(MidasInterfaceBase<? extends CacheableDTO>) lookup(jndiName);
	}
	
	private Object lookup(String jndiName) {		
		System.out.println("buscando EJB: "+jndiName);
		Object obj = null;
		try {
			obj = ServiceLocatorP.getInstance().getEJB(jndiName);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		System.out.println("encontrado: "+obj);
		return obj; 
	}
	
	private ConfiguracionDatoInciso findConfiguracionDatoIncisoCotizacionDTOById(
			ConfiguracionDatoIncisoId id) {
		return configuracionDatoIncisoDao.findById(id);
	}
	
	/**
	 * Este metodo genera una ConfiguracionDatoIncisoCotizacionId a partir de un id separado por el delimitador.
	 * @param idDatoRiesgo
	 * @return
	 */
	@Override
	public ConfiguracionDatoIncisoId getConfiguracionDatoIncisoId(String idDatoRiesgo) 
		throws IllegalArgumentException{
		//idDatoRiesgo = getIdDatoRiesgo(idDatoRiesgo); //Lo transforma por si no viene en el formato adecuado.
		String del = "_";
		String[] idTokens = idDatoRiesgo.split(del);
		ConfiguracionDatoIncisoId configuracionId = null;
		if(idTokens.length >= 5){
			
			BigDecimal idTcRamo = new BigDecimal(idTokens[0]);
			BigDecimal idTcSubramo = new BigDecimal(idTokens[1]);
			BigDecimal idToCobertura = new BigDecimal(idTokens[2]);
			Short claveDetalle = new Short(idTokens[3]);
			BigDecimal idDato = new BigDecimal(idTokens[4]);
			
			configuracionId = new ConfiguracionDatoIncisoId();
			configuracionId.setIdTcRamo(idTcRamo);
			configuracionId.setIdTcSubRamo(idTcSubramo);
			configuracionId.setIdToCobertura(idToCobertura);
			configuracionId.setClaveDetalle(claveDetalle);
			configuracionId.setIdDato(idDato);
		}
		else{
			throw new IllegalArgumentException("Los datos recibidos no son una referencia válida a la configuración de datos de riesgo.");
		}
		return configuracionId;
	}
	
	@SuppressWarnings("unused")
	private String getIdDatoRiesgo(String id) {
    	int index = id.lastIndexOf("_");
    	if (index == -1) {
    		return id;
    	}
    	id = id.substring(0, id.lastIndexOf("_"));
    	id = id.substring(id.lastIndexOf("_") + 1);
    	id = id.replaceAll(Pattern.quote("'"), "");
    	return id;
	}
	
	
	/**
	 * Encuentra las configuraciones de datos de riego para los parametros proporcionados
	 * @param configuracion
	 * @param idCobertura
	 * @param idRamo
	 * @param idSubRamo
	 * @param claveDetalle
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<ConfiguracionDatoInciso> encontrarConfiguracion(List<ConfiguracionDatoInciso> configuracion, final BigDecimal idCobertura, 
			final BigDecimal idRamo, final BigDecimal idSubRamo, final BigDecimal claveDetalle){				
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				ConfiguracionDatoInciso config = (ConfiguracionDatoInciso)arg0;
				ConfiguracionDatoIncisoId id  = config.getId();
				return id.getIdTcRamo().longValue() == idRamo.longValue() && id.getIdTcSubRamo().longValue() == idSubRamo.longValue() &&
				id.getClaveDetalle().longValue() == claveDetalle.longValue() &&	id.getIdToCobertura().longValue() == idCobertura.longValue();
			}
		};		
		return (List<ConfiguracionDatoInciso>)CollectionUtils.select(configuracion, predicate);	
		
	}
	
	/**
	 * Obtiene un registro dato inciso cot auto para la configuracion recibida
	 * @param datoRiesgoCotizacion
	 * @param idSeccion
	 * @param numInciso
	 * @param idCobertura
	 * @param idRamo
	 * @param idSubRamo
	 * @param claveDetalle
	 * @param idDato
	 * @return
	 */
	private List<DatoIncisoCotAuto> encontrarRiesgoParaConfiguracion( List<DatoIncisoCotAuto> datoRiesgoCotizacion, final BigDecimal idSeccion, final BigDecimal numInciso, 
			final BigDecimal idCobertura, final BigDecimal idRamo, final BigDecimal idSubRamo, final BigDecimal claveDetalle, final BigDecimal idDato){		
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				DatoIncisoCotAuto datoRiesgo = (DatoIncisoCotAuto)arg0;
				return datoRiesgo.getIdTcRamo().longValue() == idRamo.longValue() && datoRiesgo.getIdTcSubRamo().longValue() == idSubRamo.longValue() &&
					datoRiesgo.getClaveDetalle().longValue() == claveDetalle.longValue() && datoRiesgo.getIdDato().longValue() == idDato.longValue() &&
					datoRiesgo.getIdToSeccion().longValue() == idSeccion.longValue() && datoRiesgo.getNumeroInciso().longValue() == numInciso.longValue() && 
					datoRiesgo.getIdToCobertura().longValue() == idCobertura.longValue();
			}
		};		
		return (List<DatoIncisoCotAuto>)CollectionUtils.select(datoRiesgoCotizacion, predicate);		
	}
	

	@EJB
	public void setConfiguracionDatoIncisoDao(
			ConfiguracionDatoIncisoDao configuracionDatoIncisoDao) {
		this.configuracionDatoIncisoDao = configuracionDatoIncisoDao;
	}

	@EJB
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}

	@EJB
	public void setRamoTipoPolizaFacadeRemote(
			RamoTipoPolizaFacadeRemote ramoTipoPolizaFacadeRemote) {
		this.ramoTipoPolizaFacadeRemote = ramoTipoPolizaFacadeRemote;
	}

	@EJB	
	public void setSubRamoFacadeRemote(SubRamoFacadeRemote subRamoFacadeRemote) {
		this.subRamoFacadeRemote = subRamoFacadeRemote;
	}
	
	@EJB	
	public void setCoberturaCotizacionFacadeRemote(
			CoberturaCotizacionFacadeRemote coberturaCotizacionFacadeRemote) {
		this.coberturaCotizacionFacadeRemote = coberturaCotizacionFacadeRemote;
	}
	
	@EJB	
	public void setDatoIncisoCotAutoService(
			DatoIncisoCotAutoService datoIncisoCotAutoService) {
		this.datoIncisoCotAutoService = datoIncisoCotAutoService;
	}
	
	@EJB
	public void setIncisoCotizacionFacadeRemote(
			IncisoCotizacionFacadeRemote incisoCotizacionFacadeRemote) {
		this.incisoCotizacionFacadeRemote = incisoCotizacionFacadeRemote;
	}

	@EJB
	public void setNegocioTarifaService(NegocioTarifaService negocioTarifaService) {
		this.negocioTarifaService = negocioTarifaService;
	}
	
	@EJB
	public void setTarifaAgrupadorTarifaService(
			TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService) {
		this.tarifaAgrupadorTarifaService = tarifaAgrupadorTarifaService;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> entityClass,
			Map<String, Object> params) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(
			Class<E> entityClass, Map<String, Object> params,
			String... orderByAttributes) {
		// TODO Auto-generated method stub
		return null;
	}

	@EJB
	public void setDatoIncisoCotAutoDao(DatoIncisoCotAutoDao datoIncisoCotAutoDao) {
		this.datoIncisoCotAutoDao = datoIncisoCotAutoDao;
	}
	
	

	

}
