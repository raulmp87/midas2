package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.codigopostaliva.CodigoPostalIVAFacadeRemote;
import mx.com.afirme.midas.consultas.formapago.FormaPagoFacadeRemote;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotId;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionId;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.danios.estadisticas.EstadisticasEstadoFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.producto.ProductoFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaFacadeRemote;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.DescuentoPorVolumenAutoDao;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioAutosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteService;
import mx.com.afirme.midas2.service.fuerzaventa.GerenciaService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUsoService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionExpressService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.comision.ComisionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.autorizacion.SolicitudAutorizacionService;
import mx.com.afirme.midas2.service.suscripcion.solicitud.comentarios.ComentariosService;

import org.springframework.beans.BeanUtils;

public class CotizacionSoporteService {
	protected ComentariosService comentariosService;
	protected DocumentoDigitalCotizacionFacadeRemote documentoDigitalCotizacionFacadeRemote;
	protected TexAdicionalCotFacadeRemote texAdicionalCotFacadeRemote;
	protected CotizacionFacadeRemote cotizacionFacadeRemote;
	protected EntidadService entidadService;
	protected EstadisticasEstadoFacadeRemote estadisticas; 
	protected SolicitudFacadeRemote solicitudFacadeRemote;
	protected TipoPolizaFacadeRemote tipoPolizaFacadeRemote;
	protected ProductoFacadeRemote productoFacadeRemote;
	protected GerenciaService gerenciaService;
	protected AgenteService agenteService;
	protected FormaPagoFacadeRemote formaPagoFacadeRemote;
	protected UsuarioService usuarioService;
	protected ComisionService comisionService;
	protected CotizacionExpressService cotizacionExpressService;
	protected IncisoCotizacionFacadeRemote incisoCotizacionFacadeLocal;
	protected IncisoService incisoService;
	protected ExcepcionSuscripcionNegocioAutosService excepcionSuscripcionNegocioAutosService;
	protected CalculoService calculoService;
	protected SolicitudAutorizacionService solicitudAutorizacionService;
	protected ClienteFacadeRemote clienteFacadeRemote;
	protected CodigoPostalIVAFacadeRemote codigoPostalIVAFacadeRemote;
	protected NegocioTipoUsoService negocioTipoUsoService;
	protected CoberturaService coberturaService;
	protected PolizaPagoService pagoService;
	@EJB
	protected ComisionCotizacionFacadeRemote comisionCotizacionFacadeRemote;
	@EJB
	protected DocAnexoCotFacadeRemote docAnexoCotFacadeRemote;
	@EJB
	protected DocumentoAnexoReaseguroCotizacionFacadeRemote documentoAnexoReaseguroCotizacionFacadeRemote;
	@EJB
	protected CobranzaIncisoService cobranzaIncisoService;
	
	protected DescuentoPorVolumenAutoDao descuentoPorVolumenAutoDao;
	
	public void copiarComentariosSolicitud(BigDecimal idSolicitudBase,
			BigDecimal idSolicitudCopia) {
		comentariosService.copiarComentarios(idSolicitudBase, idSolicitudCopia);
	}

	public void copiarDocumentosDigitalesCotizacion(
			BigDecimal idCotizacionBase, BigDecimal idCotizacionCopia) {
		documentoDigitalCotizacionFacadeRemote.copiarDocumentos(
				idCotizacionBase, idCotizacionCopia);
	}

	public void copiarTextosAdicionales(BigDecimal idCotizacionBase,
			BigDecimal idCotizacionCopia) {
		texAdicionalCotFacadeRemote.copiarTextosAdicionales(idCotizacionBase,
				idCotizacionCopia);
	}
	

	public CotizacionDTO copiarCotizacion(CotizacionDTO cotizacionBase){
		Usuario usuario = usuarioService.getUsuarioActual();
		cotizacionBase = entidadService.findById(CotizacionDTO.class,cotizacionBase.getIdToCotizacion());
		List<IncisoCotizacionDTO> incisosBase = incisoCotizacionFacadeLocal.findByCotizacionId(cotizacionBase.getIdToCotizacion());
		CotizacionDTO cotizacion = new CotizacionDTO();
		if(cotizacion.getSolicitudDTO() == null)
			cotizacion.setSolicitudDTO(new SolicitudDTO());
		if(cotizacion.getNegocioTipoPoliza() == null)
			cotizacion.setNegocioTipoPoliza(new NegocioTipoPoliza());
		if(cotizacion.getSolicitudDTO().getNegocio()== null)
			cotizacion.getSolicitudDTO().setNegocio(new Negocio());
		if(cotizacion.getNegocioTipoPoliza().getNegocioProducto() == null)
			cotizacion.getNegocioTipoPoliza().setNegocioProducto(new NegocioProducto());
		
		SolicitudDTO solicitudBase = cotizacionBase.getSolicitudDTO();
		SolicitudDTO solicitud = new SolicitudDTO();
		BeanUtils.copyProperties(solicitudBase, solicitud);
		solicitud.setIdToSolicitud(null);

		solicitud.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		solicitud.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		solicitud.setFechaCreacion(new Date());
		solicitud.setFechaModificacion(new Date());

		BigDecimal solicitudId = (BigDecimal)entidadService.saveAndGetId(solicitud);
	
		BeanUtils.copyProperties(cotizacionBase, cotizacion);
		cotizacion.setIdToCotizacion(null);
		cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		cotizacion.setFechaCreacion(new Date());
		cotizacion.setFechaModificacion(new Date());
		cotizacion.setCodigoUsuarioCotizacion(usuario.getNombreUsuario());
		cotizacion.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		cotizacion.setSolicitudDTO(solicitud);	
		cotizacion.setIdToCotizacion(null);
		cotizacion.setIdToCotizacion(solicitudId);
		cotizacion.getIncisoCotizacionDTOs().clear();
		cotizacion.setCondicionesEspeciales(cotizacionBase.getCondicionesEspeciales());
		entidadService.save(cotizacion);
		for(IncisoCotizacionDTO item: incisosBase){
			incisoService.copiarInciso(item, cotizacion.getIdToCotizacion());
		}
		estadisticas.registraEstadistica(cotizacion, CotizacionDTO.ACCION.CREACION);
		comisionService.generarComision(cotizacion);
		
		try{
			copiaDocumentosCotizacion(cotizacionBase, cotizacion);
		}catch(Exception e){
		}
		
		return cotizacion;
	}
	
	private void copiaDocumentosCotizacion(CotizacionDTO cotizacionDTO, CotizacionDTO cotizacionDestino){
		
		List<DocAnexoCotDTO> documentoAnexoCotizacionDTOs = null;
		if(cotizacionDTO.getDocumentoAnexoCotizacionDTOs() != null && !cotizacionDTO.getDocumentoAnexoCotizacionDTOs().isEmpty()){
			documentoAnexoCotizacionDTOs = cotizacionDTO.getDocumentoAnexoCotizacionDTOs(); 
			for(DocAnexoCotDTO item: documentoAnexoCotizacionDTOs){
				try{
					DocAnexoCotDTO newItem = new DocAnexoCotDTO();					
					newItem.setClaveObligatoriedad(item.getClaveObligatoriedad());
					newItem.setClaveSeleccion(item.getClaveSeleccion());
					newItem.setClaveTipo(item.getClaveTipo());
					newItem.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
					newItem.setCodigoUsuarioModificacion(item.getCodigoUsuarioModificacion());
					newItem.setDescripcionDocumentoAnexo(item.getDescripcionDocumentoAnexo());
					newItem.setFechaCreacion(item.getFechaCreacion());
					newItem.setFechaModificacion(item.getFechaModificacion());
					newItem.setIdToCobertura(item.getIdToCobertura());
					newItem.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
					newItem.setNombreUsuarioModificacion(item.getNombreUsuarioModificacion());
					newItem.setOrden(item.getOrden());
					
					DocAnexoCotId id = new DocAnexoCotId();
					id.setIdToControlArchivo(item.getId().getIdToControlArchivo());
					id.setIdToCotizacion(cotizacionDestino.getIdToCotizacion());
					newItem.setCotizacionDTO(cotizacionDestino);
					newItem.setId(id);
					docAnexoCotFacadeRemote.save(newItem);
				}catch(Exception e){
				}
			}
		}
		List<DocumentoAnexoReaseguroCotizacionDTO> documentoAnexoReaseguroCotizacionDTO = null;
		if(cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs() != null && !cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs().isEmpty()){
			documentoAnexoReaseguroCotizacionDTO = cotizacionDTO.getDocumentoAnexoReaseguroCotizacionDTOs();
			for(DocumentoAnexoReaseguroCotizacionDTO item: documentoAnexoReaseguroCotizacionDTO){
				try{
					DocumentoAnexoReaseguroCotizacionDTO newItem = new DocumentoAnexoReaseguroCotizacionDTO();
					DocumentoAnexoReaseguroCotizacionId id = new DocumentoAnexoReaseguroCotizacionId();
					id.setIdToCotizacion(cotizacionDestino.getIdToCotizacion());
					id.setIdToControlArchivo(item.getId().getIdToControlArchivo());
					
					newItem.setId(id);
					newItem.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
					newItem.setCodigoUsuarioModificacion(item.getCodigoUsuarioModificacion());
					newItem.setCotizacionDTO(cotizacionDestino);
					newItem.setDescripcionDocumentoAnexo(item.getDescripcionDocumentoAnexo());
					newItem.setFechaCreacion(item.getFechaCreacion());
					newItem.setFechaModificacion(item.getFechaModificacion());
					newItem.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
					newItem.setNombreUsuarioModificacion(item.getNombreUsuarioModificacion());
					newItem.setNumeroSecuencia(item.getNumeroSecuencia());
					
					documentoAnexoReaseguroCotizacionFacadeRemote.save(newItem);
				}catch(Exception e){
				}
			}
		}
		List<DocumentoDigitalCotizacionDTO> documentoDigitalCotizacionDTO = null;
		if(cotizacionDTO.getDocumentoDigitalCotizacionDTOs() != null && !cotizacionDTO.getDocumentoDigitalCotizacionDTOs().isEmpty()){
			documentoDigitalCotizacionDTO = cotizacionDTO.getDocumentoDigitalCotizacionDTOs(); 
			for(DocumentoDigitalCotizacionDTO item: documentoDigitalCotizacionDTO){
				try{
					DocumentoDigitalCotizacionDTO newItem = new DocumentoDigitalCotizacionDTO();
					
					newItem.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
					newItem.setCodigoUsuarioModificacion(item.getCodigoUsuarioModificacion());
					newItem.setControlArchivo(item.getControlArchivo());
					newItem.setCotizacionDTO(cotizacionDestino);
					newItem.setFechaCreacion(item.getFechaCreacion());
					newItem.setFechaModificacion(item.getFechaModificacion());
					newItem.setIdControlArchivo(item.getIdControlArchivo());
					newItem.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
					newItem.setNombreUsuarioModificacion(item.getNombreUsuarioModificacion());
					
					documentoDigitalCotizacionFacadeRemote.save(newItem);
				}catch(Exception e){
				}
			}
		}
		List<TexAdicionalCotDTO> textAdicionalCotDTOs = null;
		if(cotizacionDTO.getTexAdicionalCotDTOs() != null && !cotizacionDTO.getTexAdicionalCotDTOs().isEmpty()){
			textAdicionalCotDTOs = cotizacionDTO.getTexAdicionalCotDTOs();
			for(TexAdicionalCotDTO item: textAdicionalCotDTOs){
				try{
					TexAdicionalCotDTO newItem = new TexAdicionalCotDTO();
					newItem.setClaveAutorizacion(item.getClaveAutorizacion());
					newItem.setCodigoUsuarioAutorizacion(item.getCodigoUsuarioAutorizacion());
					newItem.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
					newItem.setCodigoUsuarioModificacion(item.getCodigoUsuarioModificacion());
					newItem.setCotizacion(cotizacionDestino);
					newItem.setDescripcionTexto(item.getDescripcionTexto());
					newItem.setFechaCreacion(item.getFechaCreacion());
					newItem.setFechaModificacion(item.getFechaModificacion());
					newItem.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
					newItem.setNombreUsuarioModificacion(item.getNombreUsuarioModificacion());
					newItem.setNumeroSecuencia(item.getNumeroSecuencia());
					
					entidadService.save(newItem);
				}catch(Exception e){
				}
			}
		}

		List<Comentario> comentarioList = null;
		if(cotizacionDTO.getSolicitudDTO().getComentarioList() != null && !cotizacionDTO.getSolicitudDTO().getComentarioList().isEmpty()){
			comentarioList = cotizacionDTO.getSolicitudDTO().getComentarioList();
			for(Comentario item: comentarioList){
				try{
					Comentario newItem = new Comentario();
					newItem.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
					newItem.setDocumentoDigitalSolicitudDTO(item.getDocumentoDigitalSolicitudDTO());
					newItem.setExtension(item.getExtension());
					newItem.setFechaCreacion(item.getFechaCreacion());
					newItem.setNombreArchivoOriginal(item.getNombreArchivoOriginal());
					newItem.setSolicitudDTO(cotizacionDestino.getSolicitudDTO());
					newItem.setValor(item.getValor());
					
					entidadService.save(newItem);
				}catch(Exception e){
				}
			}
		}
		
		List<DocumentoDigitalSolicitudDTO> documentoDigitalSolicitudDTO = null;
		if(cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs() != null && !cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs().isEmpty()){
			documentoDigitalSolicitudDTO = cotizacionDTO.getSolicitudDTO().getDocumentoDigitalSolicitudDTOs();
			for(DocumentoDigitalSolicitudDTO item: documentoDigitalSolicitudDTO){
				try{
					boolean noComentario = true;
					if(comentarioList != null && !comentarioList.isEmpty()){
						for(Comentario itemValido: comentarioList){
							if(itemValido.getDocumentoDigitalSolicitudDTO() != null &&
									itemValido.getDocumentoDigitalSolicitudDTO().getIdToDocumentoDigitalSolicitud() != null && 
									itemValido.getDocumentoDigitalSolicitudDTO().getIdToDocumentoDigitalSolicitud().equals(item.getIdToDocumentoDigitalSolicitud())){
								noComentario = false;
								break;
							}
						}
					}
					if(noComentario){
						DocumentoDigitalSolicitudDTO newItem = new DocumentoDigitalSolicitudDTO();
						newItem.setCodigoUsuarioCreacion(item.getCodigoUsuarioCreacion());
						newItem.setCodigoUsuarioModificacion(item.getCodigoUsuarioModificacion());
						newItem.setControlArchivo(item.getControlArchivo());
						newItem.setFechaCreacion(item.getFechaCreacion());
						newItem.setFechaModificacion(item.getFechaModificacion());
						newItem.setIdToControlArchivo(item.getIdToControlArchivo());
						newItem.setNombreUsuarioCreacion(item.getNombreUsuarioCreacion());
						newItem.setNombreUsuarioModificacion(item.getNombreUsuarioModificacion());
						newItem.setSolicitudDTO(cotizacionDestino.getSolicitudDTO());
					
					entidadService.save(newItem);
					}
				}catch(Exception e){
				}
				
			}
		}
		
		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public BigDecimal guardarCopiaCotizacion(CotizacionDTO cotizacionNueva){
		cotizacionNueva.setIdToCotizacion(cotizacionNueva.getSolicitudDTO().getIdToSolicitud());
		entidadService.save(cotizacionNueva);
		return cotizacionNueva.getSolicitudDTO().getIdToSolicitud();				
	}
	
	/*public BigDecimal[][] getPrimaTotalCotizacionExpressMatriz(CotizacionExpress cotizacionExpress,
			List<NegocioPaqueteSeccion> filas,
			List<NegocioFormaPago> encabezados) {
		//Este podria ser la misma cotizacionExpress en lugar de un array de BigDecimal el problema es que estamos usando un mismo objeto lo que
		//podria ocasionar tal vez que como el objeto es mutable entonces los valores no se estarian perservando y por cada llamada a cotizar este
		//estaria siendo modificado.
		BigDecimal[][] primaTotales = new BigDecimal[filas.size()][encabezados.size()];
		int fila = 0;
		for (NegocioPaqueteSeccion negocioPaqueteSeccion : filas) {
			int encabezado = 0;
			cotizacionExpress.setPaquete(negocioPaqueteSseccion.getPaquete());
			for (NegocioFormaPago negocioFormaPago : encabezados) {
				cotizacionExpress.setFormaPago(negocioFormaPago.getFormaPagoDTO());
				CotizacionExpress cotizacionExpress2 = cotizacionExpressService.cotizar(cotizacionExpress);
				primaTotales[fila][encabezado] = cotizacionExpress2.getPrimaTotal();
				encabezado++;
			}
			fila++;
		}
		return primaTotales;
	}	*/

	public void asignar(CotizacionDTO cotizacion){
		cotizacionFacadeRemote.update(cotizacion);
		estadisticas.registraEstadistica(cotizacion, CotizacionDTO.ACCION.MODIFICACION);
	}
	@EJB
	public void setComentariosService(ComentariosService comentariosService) {
		this.comentariosService = comentariosService;
	}

	@EJB
	public void setDocumentoDigitalCotizacionFacadeRemote(
			DocumentoDigitalCotizacionFacadeRemote documentoDigitalCotizacionFacadeRemote) {
		this.documentoDigitalCotizacionFacadeRemote = documentoDigitalCotizacionFacadeRemote;
	}

	@EJB
	public void setTexAdicionalCotFacadeRemote(
			TexAdicionalCotFacadeRemote texAdicionalCotFacadeRemote) {
		this.texAdicionalCotFacadeRemote = texAdicionalCotFacadeRemote;
	}

	@EJB
	public void setCotizacionExpressService(
			CotizacionExpressService cotizacionExpressService) {
		this.cotizacionExpressService = cotizacionExpressService;
	}

	
	@EJB
	public void setFormaPagoFacadeRemote(
			FormaPagoFacadeRemote formaPagoFacadeRemote) {
		this.formaPagoFacadeRemote = formaPagoFacadeRemote;
	}
	
	@EJB
	public void setCotizacionFacadeRemote(
			CotizacionFacadeRemote cotizacionFacadeRemote) {
		this.cotizacionFacadeRemote = cotizacionFacadeRemote;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
	@EJB
	public void setComisionService(ComisionService comisionService) {
		this.comisionService = comisionService;
	}

	
	@EJB
	public void setEstadisticas(EstadisticasEstadoFacadeRemote estadisticas) {
		this.estadisticas = estadisticas;
	}

	@EJB
	public void setSolicitudFacadeRemote(SolicitudFacadeRemote solicitudFacadeRemote) {
		this.solicitudFacadeRemote = solicitudFacadeRemote;
	}

	@EJB
	public void setTipoPolizaFacadeRemote(
			TipoPolizaFacadeRemote tipoPolizaFacadeRemote) {
		this.tipoPolizaFacadeRemote = tipoPolizaFacadeRemote;
	}

	@EJB
	public void setProductoFacadeRemote(ProductoFacadeRemote productoFacadeRemote) {
		this.productoFacadeRemote = productoFacadeRemote;
	}
	
	@EJB
	public void setGerenciaService(GerenciaService gerenciaService) {
		this.gerenciaService = gerenciaService;
	}
	
	@EJB
	public void setAgenteService(AgenteService agenteService) {
		this.agenteService = agenteService;
	}
	
	@EJB
	public void setIncisoCotizacionFacadeRemote(
			IncisoCotizacionFacadeRemote incisoCotizacionFacadeLocal) {
		this.incisoCotizacionFacadeLocal = incisoCotizacionFacadeLocal;
	}

    @EJB(beanName="UsuarioServiceDelegate")
    public void setUsuarioService(UsuarioService usuarioService) {
          this.usuarioService = usuarioService;
    }
	 @EJB
	public void setIncisoService(IncisoService incisoService) {
		this.incisoService = incisoService;
	}
	 
	@EJB
	public void setExcepcionSuscripcionNegocioAutosService(
			ExcepcionSuscripcionNegocioAutosService excepcionSuscripcionNegocioAutosService) {
		this.excepcionSuscripcionNegocioAutosService = excepcionSuscripcionNegocioAutosService;
	}
	 
	 @EJB
	public void setCalculoService(CalculoService calculoService) {
			this.calculoService = calculoService;
	}
	 
	 @EJB
	public void setSolicitudAutorizacionService(
			SolicitudAutorizacionService solicitudAutorizacionService) {
		this.solicitudAutorizacionService = solicitudAutorizacionService;
	}

	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	@EJB
	public void setCodigoPostalIVAFacadeRemote(CodigoPostalIVAFacadeRemote codigoPostalIVAFacadeRemote) {
		this.codigoPostalIVAFacadeRemote = codigoPostalIVAFacadeRemote;
	}
	
	@EJB
	public void setNegocioTipoUsoService(
			NegocioTipoUsoService negocioTipoUsoService) {
		this.negocioTipoUsoService = negocioTipoUsoService;
	}
	
	@EJB
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}

	@EJB
	public void setPagoService(PolizaPagoService pagoService) {
		this.pagoService = pagoService;
	}
	
	@EJB
	public void setDescuentoPorVolumenAutoDao(
			DescuentoPorVolumenAutoDao descuentoPorVolumenAutoDao) {
		this.descuentoPorVolumenAutoDao = descuentoPorVolumenAutoDao;
	}
	
}
