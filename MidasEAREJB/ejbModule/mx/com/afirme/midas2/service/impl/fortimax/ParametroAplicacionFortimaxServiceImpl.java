package mx.com.afirme.midas2.service.impl.fortimax;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.fortimax.ParametroAplicacionFortimaxDao;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.ParametroAplicacionFortimax;
import mx.com.afirme.midas2.service.fortimax.ParametroAplicacionFortimaxService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author vmhersil
 *
 */
@Stateless
public class ParametroAplicacionFortimaxServiceImpl implements ParametroAplicacionFortimaxService{
	private ParametroAplicacionFortimaxDao dao;
	@Override
	public void delete(Long arg0) throws MidasException {
		dao.delete(arg0);
	}

	@Override
	public Long delete(ParametroAplicacionFortimax arg0) throws MidasException {
		return dao.delete(arg0);
	}

	@Override
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(String arg0) throws MidasException {
		return dao.obtenerParametrosPorAplicacion(arg0);
	}

	@Override
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(CatalogoAplicacionFortimax arg0) throws MidasException {
		return dao.obtenerParametrosPorAplicacion(arg0);
	}

	@Override
	public List<ParametroAplicacionFortimax> obtenerParametrosPorAplicacion(Long arg0) throws MidasException {
		return dao.obtenerParametrosPorAplicacion(arg0);
	}
	@Override
	public ParametroAplicacionFortimax obtenerParametroLlavePorAplicacion(String nombreAplicacion) throws MidasException{
		return dao.obtenerParametroLlavePorAplicacion(nombreAplicacion);
	}

	@Override
	public Long save(ParametroAplicacionFortimax arg0) throws MidasException {
		return dao.save(arg0);
	}
	/**
	 * ====================================================================
	 * Setters and getters
	 * ====================================================================
	 */
	@EJB
	public void setDao(ParametroAplicacionFortimaxDao dao) {
		this.dao = dao;
	}
}
