package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto;

import java.lang.reflect.InvocationTargetException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO.TIPO_AGRUPACION_RECIBOS;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.descuento.DescuentoCoberturaCot;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.recargo.RecargoCoberturaCot;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.bitemporal.endoso.inciso.IncisoDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoDao;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Promotoria;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.mediopago.NegocioMedioPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion_;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion_;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.DescuentoPorVolumenAuto;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.autorizacion.SolicitudExcepcionCotizacion;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoCobro;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.CotizacionExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.DatosCotizacionExpressDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ImporteCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.ValidacionService;
import mx.com.afirme.midas2.service.cobranza.programapago.RecuotificacionService;
import mx.com.afirme.midas2.service.componente.condicionespecial.CondicionEspecialCotizacionService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionService.NivelEvaluacion;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.CentroEmisorService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.calculo.CalculoServiceImpl;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.eclipse.persistence.config.HintValues;
import org.eclipse.persistence.config.QueryHints;
import org.joda.time.DateTime;
import org.joda.time.Days;

import com.js.util.StringUtil;

@Stateless
public class CotizacionServiceImpl extends CotizacionSoporteService implements CotizacionService {
	private static final Logger LOG = Logger.getLogger(CotizacionServiceImpl.class);
	public static final Integer REGENAR_PREVIO_RECIBOS = 1;
	public static String CLAVE_UNIDAD1 = "1"; 
	public static String CLAVE_UNIDAD2 = "2"; 
	public static String CLAVE_UNIDAD3 = "3";
	public static Short ESTATUS_COT_EMITIR  = 14;
	@PersistenceContext private EntityManager entityManager;
	protected ListadoService listadoService;
	protected NegocioProductoDao negocioProductoDao;
	protected DatoIncisoCotAutoService datoIncisoCotAutoService;
	private ConfiguracionDatoIncisoDao configuracionDatoIncisoDao;
	private DatoIncisoCotAutoDao datoIncisoCotAutoDao;
	@EJB private IncisoDao incisoDao;
	private AgenteMidasService agenteMidasService;
	private EjecutivoService ejecutivoService;
	private Map<Long, String> centrosEmisores;
	private CentroEmisorService centroEmisorService;
	@EJB private DocAnexoCotFacadeRemote anexoCotFacadeRemote;	
	@EJB private CondicionEspecialCotizacionService condicionEspecialCotizacionService;
	private ValidacionService validacionService;
	
	@EJB
	private RecuotificacionService recuotificacionService;
	
	@Override
	public void guardarEjecutivoColoca(BigDecimal idToCotizacion, String ejecutivoColoca){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if(cotizacion != null){
			if(ejecutivoColoca!=null){
				cotizacion.setEjecutivoColoca(ejecutivoColoca);
			}
			entidadService.save(cotizacion);
		}
	}
	
	@Override
	public List<CotizacionDTO> listarFiltrado(CotizacionDTO cotizacionDTO) {
		if(cotizacionDTO.getSolicitudDTO()== null){
			cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
		}
		if (cotizacionDTO.getSolicitudDTO().getNegocio()==null) {
			cotizacionDTO.getSolicitudDTO().setNegocio(new Negocio());
		}
		cotizacionDTO.getSolicitudDTO().getNegocio().setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);

		if (cotizacionDTO.getCodigoUsuarioCotizacion() == null) {
			cotizacionDTO.setCodigoUsuarioCotizacion(usuarioService.getUsuarioActual().getNombreUsuario());
		}
		if (cotizacionDTO.getCodigoUsuarioCreacion() == null) {
			cotizacionDTO.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		}
		return this.buscarCotizacion(cotizacionDTO, null, null, null,null,null,null,null);
	}
	
	@Override
	public Long obtenerTotalFiltradoCotizacion(CotizacionDTO cotizacionDTO) {
		if (cotizacionDTO.getSolicitudDTO()== null) {
			cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
		}
		if (cotizacionDTO.getSolicitudDTO().getNegocio()==null) {
			cotizacionDTO.getSolicitudDTO().setNegocio(new Negocio());
		}
		
		cotizacionDTO.getSolicitudDTO().getNegocio().setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);
		
		if (cotizacionDTO.getCodigoUsuarioCotizacion() == null) {
			cotizacionDTO.setCodigoUsuarioCotizacion(usuarioService.getUsuarioActual().getNombreUsuario());
		}
		if (cotizacionDTO.getCodigoUsuarioCreacion() == null) {
			cotizacionDTO.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
		}
		return cotizacionFacadeRemote.obtenerTotalFiltradoCotizacion(cotizacionDTO, null, null,null, null, null,null,null);
	}
	
	private Query listrarFiltradoGridSetParameteres(StringBuilder queryString,CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal,
			String agentes, String gerencias, BigDecimal valorPrimaTotalFin,
			Double descuento, Double descuentoFin) {
		queryString.append(" AND MODEL.solicitudDTO.negocio.claveNegocio = '"
				+ Negocio.CLAVE_NEGOCIO_AUTOS + "'");
		// Conditionals
		if (cotizacionDTO != null) {
			// Busqueda por descripcion vehiculo
			if (cotizacionDTO.getIncisoCotizacionDTOs() != null
					&& !cotizacionDTO.getIncisoCotizacionDTOs().isEmpty()) {
						queryString.append(" AND MODEL.idToCotizacion IN ( SELECT inciso.cotizacionDTO.idToCotizacion FROM IncisoCotizacionDTO inciso where " +
								"inciso.incisoAutoCot.descripcionFinal LIKE '%"
								+ cotizacionDTO.getIncisoCotizacionDTOs()
										.get(0).getIncisoAutoCot()
										.getDescripcionFinal() + "%' " +
												(cotizacionDTO.getIdToCotizacion()!= null?" AND inciso.cotizacionDTO.idToCotizacion = " + cotizacionDTO.getIdToCotizacion():"") +")" );
			} else if (cotizacionDTO.getIdToCotizacion() != null) {
				queryString.append(" AND MODEL.idToCotizacion = "
						+ cotizacionDTO.getIdToCotizacion());
			}
			
			if (cotizacionDTO.getIdCentroEmisor() != null) {
				queryString.append(" AND MODEL.idCentroEmisor = " + cotizacionDTO.getIdCentroEmisor());
			}
			if (cotizacionDTO.getFechaCreacion() != null) {
				queryString.append(" AND MODEL.fechaCreacion >= :fechaInicial");
			}
			if (cotizacionDTO.getFechaCreacion() != null) {
				queryString.append(" AND MODEL.fechaCreacion <= :fechaFinal");
			}
			if (cotizacionDTO.getClaveEstatus() != null) {
				queryString.append(" AND MODEL.claveEstatus = " + cotizacionDTO.getClaveEstatus());
			}
			if (StringUtils.isNotBlank(cotizacionDTO.getNombreContratante())) {
				queryString.append(" AND MODEL.nombreContratante LIKE '" + cotizacionDTO.getNombreContratante() + "%'");
			}
			if (StringUtils.isNotBlank(cotizacionDTO.getCodigoUsuarioCotizacion())) {
				queryString.append(" AND MODEL.codigoUsuarioCotizacion LIKE '" + cotizacionDTO.getCodigoUsuarioCotizacion() +"%'");
			}
			if (StringUtils.isNotBlank(cotizacionDTO.getCodigoUsuarioCreacion())) {
				queryString.append(" AND TRIM(UPPER(MODEL.codigoUsuarioCreacion)) ="+cotizacionDTO.getCodigoUsuarioCreacion());
			}
			if (cotizacionDTO.getSolicitudDTO() != null) {
				if (cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio() != null) {
					queryString.append(" AND MODEL.solicitudDTO.negocio.idToNegocio = " + cotizacionDTO.getSolicitudDTO().getNegocio().getIdToNegocio());
				}
				if (cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto() != null) {
					queryString.append(" AND MODEL.solicitudDTO.productoDTO.idToProducto = " + cotizacionDTO.getSolicitudDTO().getProductoDTO().getIdToProducto());
				}
			}
			if (cotizacionDTO.getValorPrimaTotal() != null) {
				queryString.append(" AND MODEL.valorPrimaTotal >= " + cotizacionDTO.getValorPrimaTotal());
				if (valorPrimaTotalFin != null) {
					queryString.append(" AND MODEL.valorPrimaTotal <= " + valorPrimaTotalFin);
				}
			}
			if (descuento != null && descuentoFin != null) {
				queryString.append(" AND model.porcentajeDescuentoGlobal BETWEEN " + descuento + "AND " + descuentoFin );
			}
			if (cotizacionDTO.getTipoPolizaDTO() != null && cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas() != null) {
				queryString.append(" AND MODEL.tipoPolizaDTO.claveAplicaFlotillas = " + cotizacionDTO.getTipoPolizaDTO().getClaveAplicaFlotillas());
			}
			if (StringUtils.isNotBlank(agentes)){
				queryString.append(" AND MODEL.solicitudDTO.codigoAgente IN ("+agentes+")") ;
			}
			if (StringUtils.isNotBlank(gerencias)) {
				queryString.append(" AND MODEL.solicitudDTO.idOficina in ("+gerencias+")");
			}
			if (cotizacionDTO.getFechaSeguimiento() != null) {
				queryString.append(" AND MODEL.fechaSeguimiento = :fechaSeguimiento");
			}
			if(StringUtils.isNotBlank(cotizacionDTO.getFolio())){
				queryString.append(" AND MODEL.folio = '" + cotizacionDTO.getFolio() + "'");
			}
		}
		// ORDER
		queryString.append(" ORDER BY MODEL.fechaCreacion DESC, MODEL.idToCotizacion");
		Query query = entityManager.createQuery(queryString.toString(),
				CotizacionDTO.class);
		if (cotizacionDTO.getFechaSeguimiento() != null) {
			final GregorianCalendar fechaSeguimiento = new GregorianCalendar();
			fechaSeguimiento.setTime(cotizacionDTO.getFechaSeguimiento());
			fechaSeguimiento.set(GregorianCalendar.HOUR_OF_DAY, 0);
			fechaSeguimiento.set(GregorianCalendar.MINUTE, 0);
			fechaSeguimiento.set(GregorianCalendar.SECOND, 0);
			fechaSeguimiento.set(GregorianCalendar.MILLISECOND, 0);
			query.setParameter("fechaSeguimiento",fechaSeguimiento.getTime());
		}
		if(fechaInicial != null && fechaFinal != null){
			final GregorianCalendar gcFechaInicio = new GregorianCalendar();
			gcFechaInicio.setTime(fechaInicial);
			gcFechaInicio.set(GregorianCalendar.HOUR_OF_DAY, 0);
			gcFechaInicio.set(GregorianCalendar.MINUTE, 0);
			gcFechaInicio.set(GregorianCalendar.SECOND, 0);
			gcFechaInicio.set(GregorianCalendar.MILLISECOND, 0);
			
			final GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(fechaFinal);
			gcFechaFin.add(GregorianCalendar.DATE, 1);
			gcFechaFin.add(GregorianCalendar.MILLISECOND, -1);
			
			query.setParameter("fechaInicial", gcFechaInicio.getTime(), TemporalType.TIMESTAMP);
			query.setParameter("fechaFinal", gcFechaFin.getTime(), TemporalType.TIMESTAMP);					
		}else{
			if(fechaInicial != null){			
				query.setParameter("fechaInicial", fechaInicial);
			}
			if(fechaFinal != null){
				query.setParameter("fechaFinal", fechaFinal);
			}
		}
		return query;
	}
	
	@SuppressWarnings({ "unchecked" })
	private Long listrarFiltradoGridCount(
			CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal,
			String agentes, String gerencias, BigDecimal valorPrimaTotalFin,
			Double descuento, Double descuentoFin)
			throws IllegalAccessException, InvocationTargetException , RuntimeException{
		StringBuilder queryString = new StringBuilder(
				"SELECT COUNT(MODEL.idToCotizacion) FROM CotizacionDTO AS MODEL");
		// filter request in process
		queryString.append(" WHERE MODEL.solicitudDTO.claveEstatus <> 0");
		final Query query = listrarFiltradoGridSetParameteres(queryString,
				cotizacionDTO, fechaInicial, fechaFinal, agentes, gerencias,
				valorPrimaTotalFin, descuento, descuentoFin);
		if(cotizacionDTO.getPrimerRegistroACargar() != null && cotizacionDTO.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(cotizacionDTO.getPrimerRegistroACargar().intValue());
			query.setMaxResults(cotizacionDTO.getNumeroMaximoRegistrosACargar().intValue());
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return (Long)query.getSingleResult();
	}
	
	@SuppressWarnings({ "unchecked" })
	private List<CotizacionDTO> listrarFiltradoGrid(
			CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal,
			String agentes, String gerencias, BigDecimal valorPrimaTotalFin,
			Double descuento, Double descuentoFin)
			throws IllegalAccessException, InvocationTargetException , RuntimeException{
		centrosEmisores = centroEmisorService.listarCentrosEmisoresMap();
		StringBuilder queryString = new StringBuilder(
				"SELECT MODEL.idToCotizacion,MODEL.nombreContratante,MODEL.codigoUsuarioCotizacion,MODEL.fechaCreacion," +
				"MODEL.claveEstatus,MODEL.porcentajeDescuentoGlobal,MODEL.valorPrimaTotal,MODEL.idCentroEmisor, " +
				"MODEL.tipoPolizaDTO.claveAplicaFlotillas, MODEL.folio " +
				"FROM CotizacionDTO MODEL ");
	
		// filter request in process
		queryString.append(" WHERE MODEL.solicitudDTO.claveEstatus <> 0");
		final Query query = listrarFiltradoGridSetParameteres(queryString,
				cotizacionDTO, fechaInicial, fechaFinal, agentes, gerencias,
				valorPrimaTotalFin, descuento, descuentoFin);

		if(cotizacionDTO.getPrimerRegistroACargar() != null && cotizacionDTO.getNumeroMaximoRegistrosACargar() != null) {
			query.setFirstResult(cotizacionDTO.getPrimerRegistroACargar().intValue());
			query.setMaxResults(cotizacionDTO.getNumeroMaximoRegistrosACargar().intValue());
		}else{
//			query.setFirstResult(1)
//			query.setMaxResults(20);
		}
		query.setHint(QueryHints.REFRESH, HintValues.TRUE);
		return makeListCotizacionDTO(query.getResultList());
	}
	
	
	private List<CotizacionDTO> makeListCotizacionDTO(List<Object> restultList) throws RuntimeException{
		final List<CotizacionDTO> cotizacionDTOs = new ArrayList<CotizacionDTO>();
		Object[] array = new Object[9];
		StringBuilder idToCotizaciones = new StringBuilder();
		for(Object item : restultList) {
			 array = (Object[])item;
			final CotizacionDTO dto = new CotizacionDTO();
			 dto.setIdToCotizacion((BigDecimal)array[0]);
			 idToCotizaciones.append(array[0]+",");
			 dto.setNombreContratante(array[1] != null ? (String)array[1]:null);
			 dto.setCodigoUsuarioCotizacion(array[2] != null ? (String) array[2]: null);
			 dto.setFechaCreacion((Date)array[3]);
			 dto.setClaveEstatus((Short)array[4]);
			 dto.setPorcentajeDescuentoGlobal(array[5] != null ? (Double)array[5]: (double)0.0);
			 dto.setValorPrimaTotal(array[6] != null? (BigDecimal)array[6]: BigDecimal.valueOf((double)0.0));
			 if ((BigDecimal)array[7] != null) {
				 BigDecimal id = (BigDecimal)array[7];
				 dto.setCentroEmisorDescripcion((String)centrosEmisores.get(id.longValue()));
			 }
			 try{
				 if ((Integer)array[8] != null) {
					 Integer claveAplicaFlotillas = (Integer)array[8];
					 TipoPolizaDTO tipoPoliza = new TipoPolizaDTO();
					 tipoPoliza.setClaveAplicaFlotillas(claveAplicaFlotillas);
					 dto.setTipoPolizaDTO(tipoPoliza);
				 }
			 }catch(Exception e){}
			 //Obtiene descuento aplicado a la cotizacion
			 try{
				 String valorDescuento = obtieneDescuentoAplicadoCotizacion(dto.getIdToCotizacion());
				 dto.setDescuendoAplicadoCotizacion(valorDescuento);
			 }catch(Exception e){
				 LOG.error(e.getMessage(), e);
			 }
			 dto.setFolio(array[9] != null? (String)array[9]: null);
			 cotizacionDTOs.add(dto);
		}
		if (StringUtils.isNotBlank(idToCotizaciones.toString())) { 
			idToCotizaciones.replace(idToCotizaciones.lastIndexOf(","),idToCotizaciones.length(),"");
			StringBuilder queryString = new StringBuilder(
					"select p.cotizacionDTO.idToCotizacion, p.numeroPoliza, p.claveEstatus from PolizaDTO p where p.cotizacionDTO.idToCotizacion in ("
							+ idToCotizaciones.toString() + ")");
			
			Query query = entityManager.createQuery(queryString.toString());
			List<Object> polizaList = query.getResultList();
			Object[] arrayPoliza = new Object[2];
			for (Object item : polizaList) {
				arrayPoliza = (Object[]) item;
				for(CotizacionDTO cotizacion : cotizacionDTOs) {
					if(!cotizacion.getIdToCotizacion().equals((BigDecimal)arrayPoliza[0]))  {
						continue;
					}
					cotizacion.setNumeroPoliza(BigDecimal.valueOf((Integer)arrayPoliza[1]));
					cotizacion.setEstatusPoliza((Short)arrayPoliza[2]==1?"Vigente":"Cancelada");
				}
			}
		}
		return cotizacionDTOs;
	}
	
	@SuppressWarnings("unchecked")
	private String obtieneDescuentoAplicadoCotizacion(BigDecimal idToCotizacion){
		String valorDescuentoStr = "0";
		BigDecimal valorDescuento = BigDecimal.ZERO;
		BigDecimal valorRecargo = BigDecimal.ZERO;
		BigDecimal totalIncisos = BigDecimal.ONE;
		boolean existeIgualacion = false;
		//Obtiene descuentos
		StringBuilder queryString1 = new StringBuilder(
				"SELECT DISTINCT dc.id.numeroInciso, dc.valorDescuento, dc.id.idToDescuentoVario FROM DescuentoCoberturaCot dc  " +
				"WHERE dc.id.idToCotizacion = "+ idToCotizacion + " ORDER BY dc.id.numeroInciso ");
		
		Query query = entityManager.createQuery(queryString1.toString());
		List<Object> descuentosList = query.getResultList();		
		for (Object item : descuentosList) {
			Object[] array = (Object[]) item;			
			BigDecimal valor = (BigDecimal) array[1];
			valorDescuento = valorDescuento.add(valor);
			
			Long idToDescuentoVario = (Long) array[2];
			if(idToDescuentoVario.intValue() == -1){
				existeIgualacion = true;
			}
		}
		
		//Obtiene recargos
		StringBuilder queryString2 = new StringBuilder(
				"SELECT DISTINCT dc.id.numeroInciso, dc.valorRecargo, dc.id.idToRecargoVario FROM RecargoCoberturaCot dc  " +
				"WHERE dc.id.idToCotizacion = "+ idToCotizacion + " ORDER BY dc.id.numeroInciso ");
		
		Query query2 = entityManager.createQuery(queryString2.toString());
		List<Object> recargosList = query2.getResultList();		
		for (Object item : recargosList) {
			Object[] array = (Object[]) item;
			//Long numeroInciso = (Long) array[0];
			BigDecimal valor = (BigDecimal) array[1];
			valorRecargo = valorRecargo.add(valor);
			
			Long idToRecargoVario = (Long) array[2];
			if(idToRecargoVario.intValue() == -1){
				existeIgualacion = true;
			}
		}		
		
//		//Obtiene total de Incisos de la cotizacion
//		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
//		if(incisos != null && incisos.size() > 0){
//			totalIncisos = new BigDecimal(incisos.size());
//		}
		totalIncisos = incisoDao.getTotalIncisosCotizacion(idToCotizacion);
		
		if (totalIncisos.compareTo(BigDecimal.ZERO) != 0){
			valorDescuento = valorDescuento.subtract(valorRecargo);
			valorDescuento = valorDescuento.divide(totalIncisos, RoundingMode.HALF_UP);		
			
			valorDescuentoStr = valorDescuento.setScale(2, RoundingMode.HALF_EVEN).toPlainString();
			
			if(totalIncisos.intValue() > 1 && existeIgualacion){
				valorDescuentoStr = valorDescuentoStr + " IF";
			}					
		}else{
			valorDescuentoStr = "0";
		}


		return valorDescuentoStr;
	}
	
	@Override
	public List<CotizacionDTO> buscarCotizacion(CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal, BigDecimal valorPrimaTotalFin,Agente agente, Promotoria promotoria,Double descuento,Double descuentoFin) {
		List<CotizacionDTO> cotizaciones = new ArrayList<CotizacionDTO>(1);		
		StringBuilder agentes = null;
		StringBuilder gerencias = null;
		
		if (cotizacionDTO.getSolicitudDTO()== null) {
			cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
		}
		if (cotizacionDTO.getSolicitudDTO().getNegocio()==null) {
			cotizacionDTO.getSolicitudDTO().setNegocio(new Negocio());
		}
		
		cotizacionDTO.getSolicitudDTO().getNegocio().setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);
		
		// Busqueda por agente
		if (agente != null
				&& agente.getId() != null) {
			agentes = new StringBuilder();
			agentes.append(agente.getId());
		}
		
		// Busqueda por promtoria
		if (agente != null && agente.getId() == null 
				&& promotoria != null && promotoria.getId() != null ){
			agentes =  new StringBuilder();
		Promotoria filtro = entidadService.findByProperty(Promotoria.class, "idPromotoria", promotoria.getId()).get(0);
		List<Agente> agenteList = entidadService.findByProperty(Agente.class, "promotoria.id", filtro.getId());
			if(agenteList != null){
				for (Agente item: agenteList) {
					agentes.append(item.getId().toString() + ",");
				}	
				if (agentes.length() > 0) {
					agentes.append(agentes.substring(0,agentes.length()-1));
				}
			}
		}
		try {
			cotizaciones = listrarFiltradoGrid(cotizacionDTO, fechaInicial, fechaFinal,(agentes == null)?null:agentes.toString(), (gerencias == null)?null:gerencias.toString(), valorPrimaTotalFin,descuento,descuentoFin);
		} catch (IllegalAccessException e) {
			LOG.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			LOG.error(e.getMessage(), e);
		} catch (RuntimeException e) {
			LOG.error(e.getMessage(), e);
		}
		return cotizaciones;
		
	}	
	
	@Override
	public Long obtenerTotalFiltradoCotizacion(CotizacionDTO cotizacionDTO, Date fechaInicial, Date fechaFinal, BigDecimal valorPrimaTotalFin,Agente agente, Promotoria promotoria,Double descuento,Double descuentoFin) {

		StringBuilder agentes = new StringBuilder();
		StringBuilder gerencias = new StringBuilder();
		
		if (cotizacionDTO.getSolicitudDTO()== null) {
			cotizacionDTO.setSolicitudDTO(new SolicitudDTO());
		}
		if (cotizacionDTO.getSolicitudDTO().getNegocio()==null) {
			cotizacionDTO.getSolicitudDTO().setNegocio(new Negocio());
		}
		
		cotizacionDTO.getSolicitudDTO().getNegocio().setClaveNegocio(Negocio.CLAVE_NEGOCIO_AUTOS);
		// Busqueda por agente
		if (agente != null
				&& agente.getId() != null) {
			agentes = new StringBuilder();
			agentes.append(agente.getId());
		}
		
		//  Busqueda por promotoria
		if (agente != null && agente.getId() == null 
				&& promotoria != null && promotoria.getId() != null ){
			agentes =  new StringBuilder();
		Promotoria filtro = entidadService.findByProperty(Promotoria.class, "idPromotoria", promotoria.getId()).get(0);
		List<Agente> agenteList = entidadService.findByProperty(Agente.class, "promotoria.id", filtro.getId());
			if(agenteList != null){
				for (Agente item: agenteList) {
					agentes.append(item.getId().toString() + ",");
				}	
				if (agentes.length() > 0) {
					agentes.append(agentes.substring(0,agentes.length()-1));
				}
			}
		}
		
		
//		if (agente != null && agente.getId() == null
//				&& promotoria != null && promotoria.getId() == null
//				&& cotizacionDTO.getSolicitudDTO().getCodigoEjecutivo() == null
//				&& cotizacionDTO.getSolicitudDTO().getIdCentroEmisor() != null) {
//			List<RegistroFuerzaDeVentaDTO> gerenciaList = gerenciaService.listarGerenciasPorCentroEmisor(cotizacionDTO.getSolicitudDTO().getIdCentroEmisor());
//			for (RegistroFuerzaDeVentaDTO item: gerenciaList) {
//					gerencias.append(item.getId().toString() + ",");
//			}
//			if (gerencias.length() > 0) {
//				gerencias.append(gerencias.substring(0,gerencias.length()-1));
//			}
//		}

		try {
			return listrarFiltradoGridCount(cotizacionDTO, fechaInicial, fechaFinal,(agentes == null)?null:agentes.toString(), gerencias.toString(), valorPrimaTotalFin,descuento,descuentoFin);
		} catch (IllegalAccessException e) {
			LOG.error(e.getMessage(), e);
		} catch (InvocationTargetException e) {
			LOG.error(e.getMessage(), e);
		} catch (RuntimeException e) {
			LOG.error(e.getMessage(), e);
		}
		return 0l;
	}

	@Override
	public DatosCotizacionExpressDTO obtenerDatosCotizacionExpressDTO(
			CotizacionExpressDTO cotizacionExpressDTO) {
		NegocioSeccion negocioSeccion = cotizacionExpressDTO.getNegocioSeccion();
		DatosCotizacionExpressDTO datosCotizacionExpressDTO = new DatosCotizacionExpressDTO();
		
		List<NegocioFormaPago> encabezados = new ArrayList<NegocioFormaPago>();
		List<NegocioPaqueteSeccion> filas = new ArrayList<NegocioPaqueteSeccion>();
		
		encabezados = entidadService.findByProperty(NegocioFormaPago.class, "negocioProducto.idToNegProducto", negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getIdToNegProducto());
		
		filas = entidadService.findByProperty(NegocioPaqueteSeccion.class, "negocioSeccion.idToNegSeccion", negocioSeccion.getIdToNegSeccion());		
		
		//Filtrar filas por BASICA-LIMITADA-AMPLIA-PLATA-ORO-PLATINO
		Paquete paquete = new Paquete();
		for(int i = 0; i < filas.size(); i++){
			NegocioPaqueteSeccion negocioPaquete = filas.get(i);
			
			if(!paquete.getPaquetesCotizacionExpress().contains(negocioPaquete.getPaquete().getId())){
				filas.remove(i);
				i--;
			}
		}
		
		//Filtrar encabezados por ANUAL-SEMESTRAL
		for(int i = 0; i < encabezados.size(); i++){
			NegocioFormaPago negocioFormaPago = encabezados.get(i);
			if(!negocioFormaPago.getFormaPagoDTO().getIdFormaPago().equals(new Integer(1)) 
					&& !negocioFormaPago.getFormaPagoDTO().getIdFormaPago().equals(new Integer(2))
					&& !negocioFormaPago.getFormaPagoDTO().getIdFormaPago().equals(new Integer(3)) 
					&& !negocioFormaPago.getFormaPagoDTO().getIdFormaPago().equals(new Integer(4))){
				encabezados.remove(i);
				i--;
			}	
		}
		
		datosCotizacionExpressDTO.setEncabezados(encabezados);
		datosCotizacionExpressDTO.setFila(filas);
		datosCotizacionExpressDTO.setTotalEncabezados(encabezados.size());
		datosCotizacionExpressDTO.setTotalFilas(filas.size());
			
		return datosCotizacionExpressDTO;
	}
	
	@Override
	public CotizacionDTO crearCotizacion(SolicitudDTO solicitud, Short estatus) {
		StringBuilder nombreCliente =  new StringBuilder();
		nombreCliente.append(solicitud.getNombrePersona());
		nombreCliente.append(solicitud.getApellidoPaterno() != null ? " " + solicitud.getApellidoPaterno():"");
		nombreCliente.append(solicitud.getApellidoMaterno() != null ? " " + solicitud.getApellidoMaterno():"");
		Usuario usuario = usuarioService.getUsuarioActual();
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
        cotizacionDTO.setSolicitudDTO(solicitud);
        cotizacionDTO.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
        cotizacionDTO.setCodigoUsuarioCotizacion(usuario.getNombreUsuario());
        cotizacionDTO.setFechaCreacion(new Date());
        cotizacionDTO.setFechaModificacion(new Date());
        cotizacionDTO.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
        cotizacionDTO.setClaveEstatus(estatus);
        //FIXME Cambiar hasta implementar la forma de definir el medio de pago
        //para obligar que seleccionen un medio de pago al terminar la cotizacion
        /*BigDecimal medioPagoAnual = BigDecimal.valueOf(15);
        cotizacionDTO.setIdMedioPago(medioPagoAnual);*/
        cotizacionDTO.setTipoCambio(Double.valueOf(1));
        cotizacionDTO.setNombreContratante(nombreCliente.toString());
        cotizacionDTO.setNombreAsegurado(nombreCliente.toString());
        //cotizacionDTO.setIdToCotizacion((BigDecimal)entidadService.saveAndGetId(cotizacionDTO));
        cotizacionDTO.setIdToCotizacion(cotizacionDTO.getSolicitudDTO().getIdToSolicitud());
        entidadService.save(cotizacionDTO);
        estadisticas.registraEstadistica(cotizacionDTO, CotizacionDTO.ACCION.CREACION);        
		return cotizacionDTO;
	}
	
	@Override
	public CotizacionDTO solicitudCotizacionFormal(CotizacionExpress cotizacionExpress){
		
		Usuario usuario = usuarioService.getUsuarioActual();
		SolicitudDTO 		solicitud 			= new SolicitudDTO();
		CotizacionDTO 		cotizacion 			= new CotizacionDTO();
		
		NegocioSeccion negocioSeccion = cotizacionExpress.getNegocioSeccion();
		cotizacion.setTipoPolizaDTO(negocioSeccion.getNegocioTipoPoliza().getTipoPolizaDTO());
		cotizacion.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
		cotizacion.setFechaCreacion(new Date());
		cotizacion.setFechaModificacion(new Date());
		cotizacion.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		cotizacion.setCodigoUsuarioCotizacion(usuario.getNombreUsuario());
		cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		cotizacion.setTipoCambio(new Double(1));

		solicitud.setProductoDTO(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getProductoDTO());
		solicitud.setNegocio(negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio());
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.PROCESO.getEstatus());
		solicitud.setFechaCreacion(Calendar.getInstance().getTime());
		solicitud.setClaveTipoSolicitud((short) 1);
		solicitud.setIdToPolizaAnterior(BigDecimal.ZERO);
        solicitud.setCodigoAgente(new BigDecimal(cotizacionExpress.getAgente().getId()));
        if(cotizacionExpress.getAgente().getId() != null){
        	try{
        		Agente filtro = new Agente();
        		filtro.setId(cotizacionExpress.getAgente().getId());
        		filtro = agenteMidasService.loadById(filtro);
        		solicitud.setNombreAgente(filtro.getPersona().getNombreCompleto());
        		if(filtro.getPromotoria() != null && filtro.getPromotoria().getEjecutivo() != null){
        			if(filtro.getPromotoria().getEjecutivo().getId() != null){
        				solicitud.setCodigoEjecutivo(new BigDecimal(filtro.getPromotoria().getEjecutivo().getId()));
        			}
        			if(filtro.getPromotoria().getEjecutivo().getPersonaResponsable() != null){
        				solicitud.setNombreEjecutivo(filtro.getPromotoria().getEjecutivo().getPersonaResponsable().getNombreCompleto());
        			}
        		}
        	}catch(Exception e){
        		
        	}
        }
        solicitud.setClaveOpcionEmision((short) 0);
        solicitud.setFechaCreacion(new Date());
        solicitud.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
        solicitud.setFechaModificacion(new Date());
        solicitud.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
        solicitud.setClaveTipoPersona((short) 1);
        solicitud.setNombrePersona(" ");
        solicitud.setApellidoPaterno(usuario.getApellidoPaterno());
        solicitud.setApellidoMaterno(usuario.getApellidoMaterno());
        solicitud.setClaveTipoSolicitud((short) 0);
        solicitud = solicitudFacadeRemote.save(solicitud);
        
//        cotizacion.setNombreAsegurado(usuario.getNombreCompleto());
		cotizacion.setSolicitudDTO(solicitud);
		cotizacion.setIdMoneda(new BigDecimal(cotizacionExpress.getIdMoneda()));
		cotizacion.setIdToCotizacion(solicitud.getIdToSolicitud());	    
		cotizacion.setIdFormaPago(BigDecimal.valueOf(cotizacionExpress.getFormaPago().getIdFormaPago()));
		cotizacion.setDerechosPoliza(cotizacionExpress.getGastosExpedicion().doubleValue());
		cotizacion.setPorcentajePagoFraccionado(listadoService.getPctePagoFraccionado(
		cotizacion.getIdFormaPago().intValue(), cotizacion.getIdMoneda().shortValue()));
		//obligar que  seleccionen un medio de pago
		//cotizacion.setIdMedioPago(BigDecimal.valueOf(15d));//efectivo
		cotizacion.setPorcentajeIva(cotizacionExpress.getIva().doubleValue());
		cotizacion.setValorPrimaTotal(cotizacionExpress.getPrimaTotal());
		cotizacion.setPorcentajebonifcomision(solicitud.getNegocio().getPctPrimaCeder());
		cotizacion.setPorcentajeDescuentoGlobal(new Double(0)); //Dejara de aplicarse el descuento global
		List<NegocioDerechoPoliza> derechosPolizaList  = solicitud.getNegocio().getNegocioDerechoPolizas();
    	if(cotizacion.getNegocioDerechoPoliza()==null){
    		cotizacion.setNegocioDerechoPoliza(new NegocioDerechoPoliza());
	    	for(NegocioDerechoPoliza negocioDerechoPoliza: derechosPolizaList ){
	    		if(negocioDerechoPoliza.getClaveDefault()){
	    			cotizacion.getNegocioDerechoPoliza().setIdToNegDerechoPoliza(negocioDerechoPoliza.getIdToNegDerechoPoliza());
	    			cotizacion.setValorDerechosUsuario(negocioDerechoPoliza.getImporteDerecho());
	    		}
	    	}
    	}
		cotizacion.setFechaInicioVigencia(new Date());
		cotizacion.setFechaFinVigencia(getFinVigenciaDefault(solicitud.getProductoDTO(),cotizacion.getFechaInicioVigencia()));
		if (solicitud.getNegocio().getFechaFinVigenciaFija() != null) {
			cotizacion.setFechaFinVigencia(solicitud.getNegocio().getFechaFinVigenciaFija());
		}
		entidadService.save(cotizacion);
		estadisticas.registraEstadistica(solicitud, SolicitudDTO.ACCION.CREACION);
		estadisticas.registraEstadistica(cotizacion, CotizacionDTO.ACCION.CREACION);
		//comisionService.generarComision(cotizacion);
		
		this.guardaIncisoCotExpress(cotizacionExpress, cotizacion);

		return cotizacion;
	}
	
	private void guardaIncisoCotExpress(CotizacionExpress cotizacionExpress, CotizacionDTO cotizacion){

		IncisoAutoCot auto = new IncisoAutoCot();
		auto.setClaveTipoBien(cotizacionExpress.getModeloVehiculo().getId().getClaveTipoBien());
		auto.setAsociadaCotizacion(1);
		auto.setDescripcionFinal(cotizacionExpress.getModeloVehiculo().getEstiloVehiculoDTO().getId().getClaveEstilo()+
				" - "+cotizacionExpress.getModeloVehiculo().getEstiloVehiculoDTO().getDescripcionEstilo());
		auto.setEstadoId(cotizacionExpress.getEstado().getStateId());
		auto.setIdMoneda(cotizacionExpress.getIdMoneda());
		auto.setIdVersionCarga(cotizacionExpress.getModeloVehiculo().getId().getIdVersionCarga());
		auto.setMarcaId(cotizacionExpress.getModeloVehiculo().getEstiloVehiculoDTO().getMarcaVehiculoDTO().getIdTcMarcaVehiculo());
		auto.setMunicipioId(cotizacionExpress.getMunicipio().getMunicipalityId());
		auto.setPaquete(cotizacionExpress.getPaquete());
		auto.setNegocioSeccionId(cotizacionExpress.getNegocioSeccion().getIdToNegSeccion().longValue());
		auto.setEstiloId(cotizacionExpress.getModeloVehiculo().getEstiloVehiculoDTO().getId().getStrId());
		auto.setModeloVehiculo(cotizacionExpress.getModeloVehiculo().getId().getModeloVehiculo());
		
		NegocioTipoUso negocioTipoUsoDefault = negocioTipoUsoService.buscarDefault(cotizacionExpress.getNegocioSeccion());
		
		auto.setTipoUsoId(negocioTipoUsoDefault.getTipoUsoVehiculoDTO().getIdTcTipoUsoVehiculo().longValue());
		
		List<NegocioPaqueteSeccion> list = new ArrayList<NegocioPaqueteSeccion>();
			list = entidadService.findByProperty(
				NegocioPaqueteSeccion.class,
				NegocioPaqueteSeccion_.negocioSeccion.getName() + "."
						+ NegocioSeccion_.idToNegSeccion.getName(),
				cotizacionExpress.getNegocioSeccion().getIdToNegSeccion());
		for (NegocioPaqueteSeccion negPaq : list) {
			if (negPaq.getPaquete().getId().intValue() == cotizacionExpress
					.getPaquete().getId().intValue()) {
				cotizacionExpress.setNegocioPaqueteSeccion(negPaq);
				auto.setNegocioPaqueteId(negPaq.getIdToNegPaqueteSeccion());
				break;
			}
		}
		
		IncisoCotizacionDTO inciso = new IncisoCotizacionDTO();
		inciso.setValorPrimaNeta(0D);
		inciso.setIncisoAutoCot(auto);
		
		// Aumenta en 1 el numero de inciso
		IncisoCotizacionId id = new IncisoCotizacionId();
		id.setIdToCotizacion(cotizacion.getIdToCotizacion());
		id.setNumeroInciso(incisoCotizacionFacadeLocal.maxIncisos(
				cotizacion.getIdToCotizacion()).add(BigDecimal.ONE));
		inciso.setId(id);
		// Aumenta en 1 el numero de secuencia
		Long maxSecuencia = new Long("1");
		maxSecuencia = maxSecuencia
				+ incisoCotizacionFacadeLocal.maxSecuencia(
						cotizacion.getIdToCotizacion());
		
		inciso.setNumeroSecuencia(maxSecuencia);
		
		List<CoberturaCotizacionDTO> coberturas = coberturaService.getCoberturas(cotizacionExpress, inciso);
		incisoService.prepareGuardarInciso(cotizacion.getIdToCotizacion(), auto, inciso, coberturas);
	}
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public CotizacionDTO crearCotizacion(CotizacionDTO cotizacion,String claveUsuario){
		
		Usuario usuario = null;
		//Se agrega claveUsuario para poder usar este mismo servicio en WSCotizacionAutoPlazo
		if(claveUsuario == null || StringUtils.isEmpty(claveUsuario)){
			 usuario = usuarioService.getUsuarioActual();
		}
		
		
		if(cotizacion.getSolicitudDTO() == null) {
			cotizacion.setSolicitudDTO(new SolicitudDTO());
		}
		if(cotizacion.getNegocioTipoPoliza() == null) {
			cotizacion.setNegocioTipoPoliza(new NegocioTipoPoliza());
		}
		if(cotizacion.getSolicitudDTO().getNegocio()== null) {
			cotizacion.getSolicitudDTO().setNegocio(new Negocio());
		}
		if(cotizacion.getNegocioTipoPoliza().getNegocioProducto() == null) {
			cotizacion.getNegocioTipoPoliza().setNegocioProducto(new NegocioProducto());
		}
		
		NegocioTipoPoliza negocioTipoPoliza  = cotizacion.getNegocioTipoPoliza();
		negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class,cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza());
		// Se crea una solicitud nueva si no existe una
		SolicitudDTO solicitud = cotizacion.getSolicitudDTO();
		if(solicitud != null && solicitud.getIdToSolicitud() != null) {
			solicitud = entidadService.findById(SolicitudDTO.class, cotizacion.getSolicitudDTO().getIdToSolicitud());
		}else{
			//solicitud = new SolicitudDTO();
			Negocio negocio = cotizacion.getSolicitudDTO().getNegocio();
			NegocioProducto   negocioProducto = cotizacion.getNegocioTipoPoliza().getNegocioProducto();
			negocioProducto = entidadService.findById(NegocioProducto.class,
					cotizacion.getNegocioTipoPoliza().getNegocioProducto()
							.getIdToNegProducto());
			solicitud.setProductoDTO(negocioProducto.getProductoDTO());
			negocio = entidadService.findById(Negocio.class,cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			solicitud.setNegocio(negocio);
			solicitud.setClaveEstatus(SolicitudDTO.Estatus.TERMINADA.getEstatus());
			solicitud.setFechaCreacion(Calendar.getInstance().getTime());
			solicitud.setClaveTipoSolicitud((short) 1);
			if (solicitud.getIdToPolizaAnterior() == null) {
				solicitud.setIdToPolizaAnterior(BigDecimal.ZERO);
			}
			//Valores de Fuerza de Venta
			if (solicitud.getCodigoAgente() != null) {
				Agente filtro = new Agente();
				try{
					filtro.setId(solicitud.getCodigoAgente().longValue());				
					Agente agente = agenteMidasService.loadById(filtro);
				//Agente agente = entidadService.findById(Agente.class, solicitud.getCodigoAgente().longValue());
					if(agente != null){
						solicitud.setCodigoEjecutivo(new BigDecimal(agente.getPromotoria().getEjecutivo().getIdEjecutivo()));
						solicitud.setNombreAgente(agente.getPersona().getNombreCompleto());
						if(agente.getPromotoria().getEjecutivo() != null){
							Ejecutivo filtroEjecutivo =new Ejecutivo();
							filtroEjecutivo.setId(agente.getPromotoria().getEjecutivo().getId());
				            Ejecutivo ejecutivo = ejecutivoService.loadById(filtroEjecutivo);
				            if(ejecutivo != null){
								solicitud.setNombreEjecutivo(ejecutivo.getPersonaResponsable().getNombreCompleto());
								solicitud.setNombreOficina(ejecutivo.getGerencia().getPersonaResponsable().getNombreCompleto());
								solicitud.setIdOficina(new BigDecimal(ejecutivo.getGerencia().getId()));
				            }
						}					
						solicitud.setNombreOficinaAgente(agente.getPromotoria().getDescripcion());
					}
				}catch(Exception e){
				
				}
			}
	        solicitud.setCodigoAgente(solicitud.getCodigoAgente()==null?BigDecimal.ZERO:solicitud.getCodigoAgente());
	        solicitud.setClaveOpcionEmision((short) 0);
	        solicitud.setFechaCreacion(new Date());
	        solicitud.setCodigoUsuarioCreacion(usuario != null ? usuario.getNombreUsuario():claveUsuario);
	        solicitud.setFechaModificacion(new Date());
	        solicitud.setCodigoUsuarioModificacion(usuario != null ? usuario.getNombreUsuario():claveUsuario);
	        solicitud.setClaveTipoPersona((short) 1);
	        solicitud.setNombrePersona(usuario != null ? usuario.getNombreUsuario():claveUsuario);
	        solicitud.setApellidoPaterno(usuario != null ? usuario.getApellidoPaterno() : null);
	        solicitud.setApellidoMaterno(usuario != null ? usuario.getApellidoMaterno() : null);
		}
		solicitud.setClaveTipoSolicitud((short) 0);
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.TERMINADA.getEstatus());
		cotizacion.setCodigoUsuarioCotizacion(usuario != null ?  usuario.getNombreUsuario() : claveUsuario);
		cotizacion.setSolicitudDTO(solicitudFacadeRemote.update(solicitud));
		cotizacion.setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
		cotizacion.setCodigoUsuarioCreacion(usuario != null ? usuario.getNombreUsuario():claveUsuario);
		cotizacion.setFechaCreacion(new Date());
		cotizacion.setFechaModificacion(new Date());
		cotizacion.setCodigoUsuarioModificacion(usuario != null ? usuario.getNombreUsuario():claveUsuario);
		cotizacion.setCodigoUsuarioCotizacion(usuario != null ? usuario.getNombreUsuario():claveUsuario);
		cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		if(cotizacion.getTipoCambio() == null){
			cotizacion.setTipoCambio(new Double(1));
		}
		//FIXME Cambiar hasta implementar la forma de definir el medio de pago
		//obligar seleccionar un medio de pago
        /*BigDecimal medioPagoAnual = BigDecimal.valueOf(15);
        if(cotizacion.getIdMedioPago() == null){
        	cotizacion.setIdMedioPago(medioPagoAnual);
        }*/
		//cotizacion.setNombreContratante(usuario.getNombreCompleto());
        
        cotizacion.setNegocioTipoPoliza(null);
        cotizacion.setDireccionCobroDTO(null);
		//cotizacion.setIdToCotizacion((BigDecimal)entidadService.saveAndGetId(cotizacion));
        cotizacion.setIdToCotizacion(cotizacion.getSolicitudDTO().getIdToSolicitud());
        
        try{
        if(cotizacion.getNegocioDerechoPoliza() != null && cotizacion.getNegocioDerechoPoliza().getIdToNegDerechoPoliza() != null){
        	NegocioDerechoPoliza negocioDerechoPoliza = entidadService.findById(NegocioDerechoPoliza.class, cotizacion.getNegocioDerechoPoliza().getIdToNegDerechoPoliza());
        	if(negocioDerechoPoliza != null){
        		cotizacion.setValorDerechosUsuario(negocioDerechoPoliza.getImporteDerecho());
        	}
        }
        }catch(Exception e){
        	
        }
        
        cotizacion = entidadService.save(cotizacion);
		estadisticas.registraEstadistica(cotizacion, CotizacionDTO.ACCION.CREACION);
		comisionService.generarComision(cotizacion);
		anexoCotFacadeRemote.generateDocumentsByCotizacion(cotizacion.getIdToCotizacion());
		return cotizacion;
	}
	
	@Override
	public CotizacionDTO complementarCreacionCotizacion(CotizacionDTO cotizacionDTO){
		cotizacionDTO.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class,cotizacionDTO.getNegocioTipoPoliza().getIdToNegTipoPoliza());
		cotizacionDTO.setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
		cotizacionDTO = cotizacionFacadeRemote.update(cotizacionDTO);
		comisionService.generarComision(cotizacionDTO);
		return cotizacionDTO;
	}
	

	@Override
	public Map<Boolean,Double>  compararDescuento(CotizacionDTO cotizacion){
		Map<Boolean,Double> map = new HashMap<Boolean, Double>(1);
		if (cotizacion.getPorcentajeDescuentoGlobal() != null) {
			if (cotizacion.getPorcentajeDescuentoGlobal() > cotizacion
					.getSolicitudDTO().getNegocio().getPctMaximoDescuento()) {
				map.put(true, cotizacion.getSolicitudDTO().getNegocio()
						.getPctMaximoDescuento());
				
			}else{
				map.put(false, cotizacion.getPorcentajeDescuentoGlobal());
			}
		}
		return map;
	}

	@Override
	public Short getTipoPersona(CotizacionDTO cotizacion){
		return ((Short) cotizacion.getSolicitudDTO().getClaveTipoPersona());
	}

	@Override
	public Short getDatosAsegurado(CotizacionDTO cotizacion,Short tipoAsegurado ){
		return tipoAsegurado;
	}
	
	@Override
	public String actualizarAsegurado(CotizacionDTO cotizacion,Short tipoAsegurado){
		String datosAsegurado=null;
		
		if(cotizacion.getSolicitudDTO().getApellidoPaterno()== null){
			datosAsegurado=cotizacion.getSolicitudDTO().getNombrePersona().toString();
		}else{
			datosAsegurado=cotizacion.getSolicitudDTO().getNombrePersona().toString()+" "+cotizacion.getSolicitudDTO().getApellidoPaterno().toString();
		}
		
		if(cotizacion.getSolicitudDTO().getClaveTipoPersona().intValue() == 1 && tipoAsegurado.intValue() == 2){
			cotizacion.setNombreAsegurado(cotizacion.getNombreAsegurado());
			cotizacion.setNombreEmpresaAsegurado(" ");
			cotizacionFacadeRemote.update(cotizacion);
		}

		if(cotizacion.getSolicitudDTO().getClaveTipoPersona().intValue()!= 1 && tipoAsegurado.intValue()== 2){
			cotizacion.setNombreEmpresaAsegurado(cotizacion.getNombreEmpresaAsegurado());
			cotizacion.setNombreAsegurado(" ");
			cotizacionFacadeRemote.update(cotizacion);
		}
		
		if(cotizacion.getSolicitudDTO().getClaveTipoPersona().intValue() == 1 && tipoAsegurado.intValue()== 1){
			   cotizacion.setNombreAsegurado(datosAsegurado);
			   cotizacionFacadeRemote.update(cotizacion);	
		}
		
		if(cotizacion.getSolicitudDTO().getClaveTipoPersona().intValue()!= 1 && tipoAsegurado.intValue()== 1){
			   cotizacion.setNombreEmpresaAsegurado(datosAsegurado);
			   cotizacionFacadeRemote.update(cotizacion);
		}
		return (datosAsegurado);
	}
	
	@Override
	public CotizacionDTO guardarCotizacion(CotizacionDTO cotizacion) throws NegocioEJBExeption{
		DateFormat dateFormat = null;
		if(cotizacion.getNegocioDerechoPoliza() != null){
			NegocioDerechoPoliza negocioDerechoPoliza = entidadService.findById(NegocioDerechoPoliza.class, cotizacion.getNegocioDerechoPoliza().getIdToNegDerechoPoliza());
			cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
			cotizacion.setValorDerechosUsuario(negocioDerechoPoliza.getImporteDerecho());
			DateTime inicioVigencia = new DateTime(cotizacion.getFechaInicioVigencia());
			DateTime finVigencia = new DateTime(cotizacion.getFechaFinVigencia());
			long days = Days.daysBetween(inicioVigencia,finVigencia).getDays();
			
			// Valida que los días de vigencia de la cotizacion no sean mayor a 365 dias
			if (cotizacion.getTipoPolizaDTO().getProductoDTO().getValorMaximoUnidadVigencia().longValue() <= 365){
				if (days > 365){
					throw new NegocioEJBExeption("","Los d\u00edas de vigencia de la cotizaci\u00f3n no pueden ser mayores a un a\u00f1o");
				}
			}
			// Valida que los días de vigencia de la cotizacion no sean mayores a los del producto
			if (days > cotizacion.getTipoPolizaDTO().getProductoDTO().getValorMaximoUnidadVigencia().longValue()) {
				throw new NegocioEJBExeption("", "Los d\u00edas de la cotizaci\u00f3n son mayores a los d\u00edas de vigencia del producto");
			}
			// Valida que la fecha fin fija del negocio sea igual que la 
			// Fecha fin de la cotizacion
			if (cotizacion.getSolicitudDTO().getNegocio()
					.getFechaFinVigenciaFija() != null) {
				//Compara fechas
				if(!DateUtils.isSameDay(cotizacion.getSolicitudDTO().getNegocio().getFechaFinVigenciaFija(), cotizacion.getFechaFinVigencia())){								
					Date fechaFija = cotizacion.getSolicitudDTO().getNegocio().getFechaFinVigenciaFija();
					dateFormat = new SimpleDateFormat("dd/MM/yyyy");
					throw new NegocioEJBExeption("","La fecha fin de vigencia debe de ser igual a <br/>" + dateFormat.format(fechaFija));
				}
			}
			cotizacionFacadeRemote.update(cotizacion);
		}else {
			return null;
		}
		 return cotizacion;
	}
	
	
	@Override
	public Map<Integer,Integer> compararVigenciaCotizacionPorProducto(CotizacionDTO cotizacionDTO, Date vigenciaInicial, Date vigenciaFinal) {
		MensajeDTO mensajeDTO  = new MensajeDTO();
		Map<Integer,Integer> map = new  HashMap<Integer, Integer>(1);
		int vigenciaCotizacionEnNumeroDeDias = 0;
		int vigenciaMinima = 0;
		int vigenciaMaxima = 0;
		if(cotizacionDTO.getSolicitudDTO()!= null) {		
			vigenciaCotizacionEnNumeroDeDias = getDaysBetweenDates(vigenciaInicial, vigenciaFinal);
			vigenciaMinima = getDays(cotizacionDTO.getSolicitudDTO().getProductoDTO().getClaveUnidadVigencia(), (cotizacionDTO.getSolicitudDTO().getProductoDTO().getValorMinimoUnidadVigencia()).intValue());
			vigenciaMaxima = getDays(cotizacionDTO.getSolicitudDTO().getProductoDTO().getClaveUnidadVigencia(), (cotizacionDTO.getSolicitudDTO().getProductoDTO().getValorMaximoUnidadVigencia()).intValue());
			
			if(vigenciaCotizacionEnNumeroDeDias >= vigenciaMinima && vigenciaCotizacionEnNumeroDeDias > vigenciaMaxima){
				mensajeDTO.setVisible(true);
				mensajeDTO.setTipoMensaje("20");
				// rango mayor al maximo permitido
				map.put(Integer.valueOf(1),vigenciaMaxima);
				return map;
			}else if(vigenciaCotizacionEnNumeroDeDias < vigenciaMinima && vigenciaCotizacionEnNumeroDeDias <= vigenciaMaxima){
				mensajeDTO.setVisible(true);
				mensajeDTO.setTipoMensaje("20");
				// rengo menor al minimo permitido
				map.put(Integer.valueOf(0), vigenciaMinima);
				return map;
			}else{
				map.put(Integer.valueOf(-1), Integer.valueOf(0));
				return map;
			}
		}
		return null;
	}		

	@Override
	public Date getFinVigenciaDefault(ProductoDTO producto, Date vigenciaInicial) {
		Date vigenciaFinalDefault = new Date();
		int vigenciaFinalEnDias = 0;
		if(producto != null) {
			vigenciaFinalEnDias = getDays(producto.getClaveUnidadVigencia(), producto.getValorDefaultUnidadVigencia());
			//LPV Si los dias son 365 se le suma un año en lugar de los dias para no afectar anios bisiestos
			if(vigenciaFinalEnDias == 365){ 
				GregorianCalendar gcFechaFin = new GregorianCalendar();
				gcFechaFin.setTime(vigenciaInicial);
				gcFechaFin.add(GregorianCalendar.YEAR, 1);
				vigenciaFinalDefault = gcFechaFin.getTime();
			}else{
				vigenciaFinalDefault = getNewDate(vigenciaFinalEnDias, vigenciaInicial);
			}
		}
		return vigenciaFinalDefault;
	}

@Override
@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
public CotizacionDTO descuentoGlobalCalculado(CotizacionDTO cotizacion){ 
	cotizacion.setDescuentoVolumen(new Double(getDescuentoPorVolumenaNivelPoliza(cotizacion)));
	if((cotizacion.getPorcentajeDescuentoGlobal()==null)){
		cotizacion.setPorcentajeDescuentoGlobal(cotizacion.getDescuentoVolumen());
	}

  return cotizacion;
}


    @Override
	public int validaFechaOperacion(CotizacionDTO cotizacion){
		int mensaje=0;
		Calendar fechaInicioVigencia =Calendar.getInstance();
		fechaInicioVigencia.setTime(cotizacion.getFechaInicioVigencia());
		
		Calendar fechaFinVigencia =Calendar.getInstance();
		fechaFinVigencia.setTime(cotizacion.getFechaFinVigencia());
		
		Calendar fechaNegocioOperacion =Calendar.getInstance();
		fechaNegocioOperacion.setTime(cotizacion.getSolicitudDTO().getNegocio().getFechaInicioVigencia());
		
		if((cotizacion.getFechaInicioVigencia().compareTo(cotizacion.getSolicitudDTO().getNegocio().getFechaInicioVigencia())==-1) ||
		   (cotizacion.getSolicitudDTO().getNegocio().getFechaFinVigencia().compareTo(cotizacion.getFechaFinVigencia())==-1))
		 {
		   mensaje=-1;
		 }
		
	 return mensaje;
		
   }

@Override
public CotizacionDTO getFechasDefaults(CotizacionDTO cotizacion){
	   if((cotizacion.getFechaInicioVigencia()==null) &&
		  (cotizacion.getFechaFinVigencia()==null)){
		   cotizacion.setFechaInicioVigencia(new Date());
		   if(cotizacion.getSolicitudDTO() == null || cotizacion.getSolicitudDTO().getProductoDTO() == null){
				cotizacion = entidadService.findById(CotizacionDTO.class,cotizacion.getIdToCotizacion());
		   }
		   cotizacion.setFechaFinVigencia(getFinVigenciaDefault(cotizacion.getSolicitudDTO().getProductoDTO(),cotizacion.getFechaInicioVigencia()));
	   }
	return cotizacion;
}
	
	@Override
	public Map<BigDecimal, String> getMapContratantes(CotizacionDTO cotizacionDTO){
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		List<NegocioCliente> negocioClienteList = cotizacionDTO.getSolicitudDTO().getNegocio().getNegocioClientes();
		for (NegocioCliente negocioCliente : negocioClienteList){
			map.put(negocioCliente.getClienteDTO().getIdCliente(), negocioCliente.getClienteDTO().getNombre() +" "+ negocioCliente.getClienteDTO().getApellidoPaterno() +" "+ negocioCliente.getClienteDTO().getApellidoMaterno());
		}
		return map;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Double getDescuentoPorVolumenaNivelPoliza(CotizacionDTO cotizacionDTO){
		int numeroIncisos = incisoCotizacionFacadeLocal.contarIncisosPorCotizacion(cotizacionDTO.getIdToCotizacion());
		Double descuento = new Double(0.0);
		DescuentoPorVolumenAuto descuentoPorVolumenAuto = descuentoPorVolumenAutoDao.findByTotalIncisoDentroDeMinMaxTotalSubsections(numeroIncisos);
		if (descuentoPorVolumenAuto != null) {
			descuento = descuentoPorVolumenAuto.getDefaultDiscountVolumen();
		}
		
		return descuento;
	}
	

	
	private int getDays(String claveUnidad, int unidadVigencia){
		int dias = 0;
		
		if (CLAVE_UNIDAD1.equals(claveUnidad)){
			dias = unidadVigencia;
		}
		if (CLAVE_UNIDAD2.equals(claveUnidad)){
			dias = unidadVigencia * 30;
		}
		if (CLAVE_UNIDAD3.equals(claveUnidad)){
			dias = unidadVigencia * 365;
		}
		return dias;
	}
	
	private int getDaysBetweenDates(Date fInicial, Date fFinal){
		return (int)( (fFinal.getTime() - fInicial.getTime()) / (1000 * 60 * 60 * 24));
	}
	
	private Date getNewDate(int vigenciaFinalEnDias, Date vigenciaInicial){
		Calendar c = Calendar.getInstance();
		c.setTime(vigenciaInicial);
		c.add(Calendar.DATE, vigenciaFinalEnDias);
		return c.getTime();
	}
	
	@Override
	public List<EsquemaPagoCotizacionDTO> setEsquemaPagoCotizacion(CotizacionDTO cotizacion,Date fechaIniBase, Date fechaInicial, Date fechaFinal,
		 BigDecimal primaNeta, BigDecimal derecho, BigDecimal iva, BigDecimal impTotal) {
		Double impRecargosFraccionados = null;
		NegocioProducto negocioProducto = null;
		List<EsquemaPagoCotizacionDTO> esquema = new ArrayList<EsquemaPagoCotizacionDTO>(1);	
		BigDecimal pcteCesionComision = (cotizacion.getPorcentajebonifcomision() != null)? BigDecimal.valueOf(cotizacion.getPorcentajebonifcomision()) : BigDecimal.ZERO;
		final BigDecimal idToCotizacion = cotizacion.getIdToCotizacion();
		BigDecimal comisionMenosBonificacion = BigDecimal.ZERO;
		final BigDecimal CIEN = new BigDecimal(100);
		cotizacion = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
		SolicitudDTO solicitud = solicitudFacadeRemote.findById(cotizacion.getSolicitudDTO().getIdToSolicitud());
		negocioProducto = negocioProductoDao
				.getNegocioProductoByIdNegocioIdProducto(solicitud.getNegocio()
						.getIdToNegocio(), solicitud.getProductoDTO()
						.getIdToProducto());	
		int totalIncisos = incisoCotizacionFacadeLocal.contarIncisosPorCotizacion(cotizacion.getIdToCotizacion());
		List<NegocioFormaPago> formasPagoList = entidadService.findByProperty(
				NegocioFormaPago.class, "negocioProducto.idToNegProducto",
				negocioProducto.getIdToNegProducto());
		
		Collections.sort(formasPagoList, 
				new Comparator<NegocioFormaPago>() {				
					public int compare(NegocioFormaPago n1, NegocioFormaPago n2){
						return n1.getFormaPagoDTO().getIdFormaPago().compareTo(n2.getFormaPagoDTO().getIdFormaPago());
					}
				});
		
	   final Predicate findId = new Predicate() {
			@Override
			public boolean evaluate(Object arg0) {
				ComisionCotizacionDTO id = (ComisionCotizacionDTO) arg0;
				return (id.getId().getIdToCotizacion().equals(idToCotizacion)
						&& id.getId().getIdTcSubramo().equals(CalculoServiceImpl.getIdSubRamoC()) && id.getId()
						.getTipoPorcentajeComision().equals((short) 1));
			}
		};
		
		final ComisionCotizacionDTO comisionCotizacionDTO = (ComisionCotizacionDTO) CollectionUtils.find(
															cotizacion.getComisionCotizacionDTOs(), findId);
		
		if (comisionCotizacionDTO != null) {
			if (cotizacion.getPorcentajebonifcomision() != null) {
				comisionMenosBonificacion = pcteCesionComision.divide(CIEN).setScale(16, RoundingMode.HALF_EVEN).multiply(
						comisionCotizacionDTO.getPorcentajeComisionDefault().divide(CIEN).setScale(16, RoundingMode.HALF_EVEN).
						setScale(16, RoundingMode.HALF_EVEN));						
			}
		}
		
		primaNeta = primaNeta.subtract(primaNeta.multiply(comisionMenosBonificacion)).setScale(2, RoundingMode.HALF_EVEN);
		
		for(NegocioFormaPago item : formasPagoList){	
		  if(cotizacion.getPorcentajePagoFraccionado() != null && 
				  item.getFormaPagoDTO().getIdFormaPago().intValue() == cotizacion.getIdFormaPago().intValue()){
			  impRecargosFraccionados = primaNeta.doubleValue() * (cotizacion.getPorcentajePagoFraccionado() / 100);
		  }
		  esquema.add(this.findEsquemaPagoCotizacion( fechaIniBase, fechaInicial,fechaFinal,
		              item.getFormaPagoDTO().getIdFormaPago(),primaNeta, 
		              derecho.multiply(BigDecimal.valueOf(totalIncisos)), iva, 
		              cotizacion.getIdMoneda().intValue(), impRecargosFraccionados, true));
		  impRecargosFraccionados = null;
		 }
		return esquema;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public EsquemaPagoCotizacionDTO findEsquemaPagoCotizacion(Date fechaIniBase, Date fechaInicial, Date fechaFinal,
			Integer idFormaPago,  BigDecimal primaNeta, BigDecimal derecho, BigDecimal iva, Integer idMoneda, 
			Double impRecargoFraccionado, boolean obtenerRecargoDefault) {
		
		String spName="SEYCOS.PKG_SEGURO_FLOTILLA.stpConsDistRec";
		
		String [] atributosDTO = { "numExhibicion", "importe" };
		String [] columnasCursor = { "NUM_EXHIBICION", "IMPORTE"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_INTERFAZ);
			storedHelper.estableceParametro("pf_ini_base", fechaIniBase);
			storedHelper.estableceParametro("pf_inicial", fechaInicial);
			storedHelper.estableceParametro("pf_final", fechaFinal);
			storedHelper.estableceParametro("pid_forma_pago", idFormaPago);
			storedHelper.estableceParametro("pimp_subtotal", primaNeta);
			storedHelper.estableceParametro("pimp_derechos", derecho);
			storedHelper.estableceParametro("pimp_recargo", impRecargoFraccionado);
			storedHelper.estableceParametro("ppct_iva", iva);
			storedHelper.estableceParametro("pid_moneda", idMoneda);
			storedHelper.estableceParametro("pObtenerRcgoDefault", obtenerRecargoDefault ? new Integer(1): new Integer(0));
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ImporteCotizacionDTO", atributosDTO, columnasCursor);
			
			Object result = storedHelper.obtieneListaResultados();
			EsquemaPagoCotizacionDTO esquema = new EsquemaPagoCotizacionDTO();
			if(result instanceof List)  {
				@SuppressWarnings("unchecked")
				List<ImporteCotizacionDTO> listaResultados = (List<ImporteCotizacionDTO>) result;
				boolean isFirst = true;
				FormaPagoDTO formaPagoDTO = new FormaPagoDTO();
				formaPagoDTO.setIdFormaPago(idFormaPago);
				formaPagoDTO = formaPagoFacadeRemote.findById(formaPagoDTO);
				
				esquema.setCantidad(listaResultados.size() - 1);
				esquema.setFormaPagoDTO(formaPagoDTO);
				for(ImporteCotizacionDTO item : listaResultados){
					if(isFirst){
						esquema.setPagoInicial(item);
						isFirst = false;
					}else{
						if(item.getImporte() != null && item.getNumExhibicion() != null){
							item.setImporte(new BigDecimal((item.getImporte().doubleValue() / 
									item.getNumExhibicion().intValue())));
						}
						esquema.setPagoSubsecuente(item);						
					}
				}
			}
			return esquema;
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName,e);
		}
	}	

	@Override
	public void reasignarSuscriptor(CotizacionDTO cotizacion) {
		Usuario usuario = usuarioService.getUsuarioActual();
        cotizacion.setFechaModificacion(new Date());
        cotizacion.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
        //FIXME: Modificar por el tipo de cambio Real
        cotizacion.setTipoCambio(new Double(1));
		cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		cotizacionFacadeRemote.update(cotizacion);
		
		try{
			entidadService.save(cotizacion.getSolicitudDTO());
		}catch(Exception e){
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TerminarCotizacionDTO terminarCotizacion(BigDecimal idToCotizacion, boolean esRenovacion){				
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		entidadService.refresh(cotizacion);
		TerminarCotizacionDTO terminarCotizacionDTO = null;

		incisoService.borrarIncisoAutoCotNoAsignados(idToCotizacion);
		
		if(!esRenovacion){
			terminarCotizacionDTO = validarIncisosFlotilla(cotizacion);
			if(terminarCotizacionDTO != null){
				return terminarCotizacionDTO;
			}

			terminarCotizacionDTO = validarMonto(cotizacion);
			if(terminarCotizacionDTO != null){
				return terminarCotizacionDTO;
			}
			
			terminarCotizacionDTO = validarSolicitudes(cotizacion);
			if(terminarCotizacionDTO != null){
				return terminarCotizacionDTO;
			}
		}
		
		//Valida prima cero en coberturas
		terminarCotizacionDTO = validarPrimaCeroCoberturas(cotizacion);
		if(terminarCotizacionDTO != null){
			return terminarCotizacionDTO;
		}
		
		if(!esRenovacion){
			terminarCotizacionDTO = validarExcepciones(cotizacion, NivelEvaluacion.TERMINAR_COTIZACION);
			if(terminarCotizacionDTO != null){			
				return terminarCotizacionDTO;
			}
		} else {
			//Para renovacion solo validar excepcion de descuento por estado
			terminarCotizacionDTO = validarDescuentosPorEstado(cotizacion);
		}
		
		if(terminarCotizacionDTO == null){
			//Generar condiciones obligatorias
			condicionEspecialCotizacionService.generaCondicionesBaseCompletas(idToCotizacion);
			
			//Calcular Descuento Global (Ya no se aplica como descuento, solamente se utiliza para la exclusion de bonos)
			cotizacion.setPorcentajeDescuentoGlobal(calculoService.calculaPorcentajeDescuentoGlobal(idToCotizacion));
			
			//Cambia Estatus
			cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_TERMINADA);
			entidadService.save(cotizacion);
			try{
				anexoCotFacadeRemote.generateCobDocumentsByCotizacion(idToCotizacion);
			}catch(Exception e){
				LOG.error(e.getMessage(), e);
			}
		}
				
		//20170104 Recuotificacion - Generar Previo Recibos via motor
		if(terminarCotizacionDTO == null){		
			recuotificacionService.generaProgramasPago(idToCotizacion, REGENAR_PREVIO_RECIBOS, null, null);			
		}
		
		return terminarCotizacionDTO;
	}
	
	@Override
	public void validaTerminarCotizacionWS(TerminarCotizacionDTO terminarCotizacionDTO) throws SystemException{
		if(terminarCotizacionDTO != null){
			if(terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_AGENTE.getEstatus().shortValue()){
				throw new SystemException("No es posible actualizar el registro");
			}else if(terminarCotizacionDTO.getEstatus() == TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus().shortValue()){
				List<ExcepcionSuscripcionReporteDTO> excepcionesList = 	terminarCotizacionDTO.getExcepcionesList();
				throw new SystemException("La siguiente excepción está configurada para el negocio: " + excepcionesList.get(0).getDescripcionExcepcionPlana());
			}else{
				throw new SystemException(terminarCotizacionDTO.getMensajeErrorPlano());
			}
		}
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TerminarCotizacionDTO validaIgualacionCotizadorAgentes(BigDecimal idToCotizacion){				
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		entidadService.refresh(cotizacion);
		TerminarCotizacionDTO terminarCotizacionDTO = null;

		incisoService.borrarIncisoAutoCotNoAsignados(idToCotizacion);
		
		terminarCotizacionDTO = validarExcepciones(cotizacion, NivelEvaluacion.TERMINAR_COTIZACION);
		if(terminarCotizacionDTO != null){			
			return terminarCotizacionDTO;
		}
		return terminarCotizacionDTO;
	}
	
	/**
	 * Actualizar cotizacion con conflicto de numero de serie
	 * @param excepcionesList
	 */
	private void revisarConflictoNumeroSerie(List<? extends ExcepcionSuscripcionReporteDTO> excepcionesList, BigDecimal idToCotizacion){
		boolean conflicto = false;
		CotizacionDTO cotizacion = null;
		for(ExcepcionSuscripcionReporteDTO reporte : excepcionesList){
			if(reporte.getIdExcepcion().shortValue() == 
				ExcepcionSuscripcionService.TipoExcepcionFija.NUMERO_SERIE_REPETIDO.valor()){
				conflicto = true;				
				break;
			}
		}		
		cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if(conflicto){
			cotizacion.setConflictoNumeroSerie(Boolean.TRUE);
			entidadService.save(cotizacion);
		}
		
	}
	
	private TerminarCotizacionDTO validarAgente(){
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		return terminarCotizacionDTO;
	}
	
	private TerminarCotizacionDTO validarPrimaCeroCoberturas(CotizacionDTO cotizacion){
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		try{
		StringBuilder mensaje = new StringBuilder("\u003Cul\u003E\n");
			List<CoberturaCotizacionDTO> coberturas = coberturaService.getCoberturasPrimaNetaCero(cotizacion.getIdToCotizacion(), true);
			for(CoberturaCotizacionDTO coberturaCot: coberturas){
				if(coberturaCot.getValorPrimaNeta() == 0){
					CoberturaDTO coberturaDTO = entidadService.findById(CoberturaDTO.class, coberturaCot.getId().getIdToCobertura());

					if(coberturaDTO.getClaveImporteCero().equals("0")){
						if(coberturaDTO.getIdToCobertura().equals(CoberturaDTO.IDTOCOBERTURA_DANIOSOCASIONADOSPORLACARGA)){
							//Obtiene Tipo de Carga 
							Map<String,Object> params = new LinkedHashMap<String,Object>();
							params.put("idToCobertura", CoberturaDTO.IDTOCOBERTURA_DANIOSOCASIONADOSPORLACARGA);
							params.put("idToCotizacion", cotizacion.getIdToCotizacion());
							params.put("numeroInciso", coberturaCot.getId().getNumeroInciso());
							params.put("idToSeccion", coberturaCot.getId().getIdToSeccion());
							params.put("idTcSubRamo", coberturaCot.getIdTcSubramo());
							List<DatoIncisoCotAuto> datoRiesgoList = entidadService.findByProperties(DatoIncisoCotAuto.class, params);
							if(datoRiesgoList != null && !datoRiesgoList.isEmpty()){
								DatoIncisoCotAuto item = datoRiesgoList.get(0);
								if(item.getValor() != null && !item.getValor().equals("10")){
									terminarCotizacionDTO = new TerminarCotizacionDTO();
									terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_MONTO.getEstatus());
									IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
									incisoCotizacionId.setIdToCotizacion(cotizacion.getIdToCotizacion());
									incisoCotizacionId.setNumeroInciso(coberturaCot.getId().getNumeroInciso());
									IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionId);
									mensaje.append("\u003Cli\u003EInciso ").append(inciso.getNumeroSecuencia()).append(": La Cobertura ").append(coberturaDTO.getNombreComercial()).append(" no permite importe cero.\u003C/li\u003E\n");
									terminarCotizacionDTO.setMensajeError(mensaje.toString());
								}
							}
							
						}else{
							terminarCotizacionDTO = new TerminarCotizacionDTO();
							terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_MONTO.getEstatus());
							IncisoCotizacionId incisoCotizacionId = new IncisoCotizacionId();
							incisoCotizacionId.setIdToCotizacion(cotizacion.getIdToCotizacion());
							incisoCotizacionId.setNumeroInciso(coberturaCot.getId().getNumeroInciso());
							IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacionId);
							mensaje.append("\u003Cli\u003EInciso ").append(inciso.getNumeroSecuencia()).append(": La Cobertura ").append(coberturaDTO.getNombreComercial()).append(" no permite importe cero.\u003C/li\u003E\n");
							terminarCotizacionDTO.setMensajeError(mensaje.toString());
						//	break;
						}
					}
				}
			}
			if(terminarCotizacionDTO != null){
				mensaje.append("\u003C/ul\u003E\n");
				terminarCotizacionDTO.setMensajeError(mensaje.toString());
			}
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return terminarCotizacionDTO;
	}
	
	private TerminarCotizacionDTO validarIncisosFlotilla(CotizacionDTO cotizacion){
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		if(cotizacion.getTipoPolizaDTO().getClaveAplicaFlotillas().intValue() == 1 &&
				cotizacion.getIncisoCotizacionDTOs().size() < 2){
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(
					TerminarCotizacionDTO.Estatus.ERROR_INCISOS_INSUFICIENTES.getEstatus());
			terminarCotizacionDTO.setMensajeError("Para poder terminar una flotilla se " +
					"necesitan al menos dos incisos.");
		}
		return terminarCotizacionDTO;
	}
	
	private TerminarCotizacionDTO validarMonto(CotizacionDTO cotizacion){
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		ResumenCostosDTO resumenCostosDTO = calculoService.obtenerResumenCotizacion(cotizacion, false);
		if(resumenCostosDTO.getPrimaNetaCoberturas() == null || resumenCostosDTO.getPrimaNetaCoberturas() == 0){
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_MONTO.getEstatus());
			terminarCotizacionDTO.setMensajeError("Esta cotizaci\u00F3n no puede ser terminada debido a que el monto de prima " +
					"no esta debidamente calculado, por favor recalcula la cotizaci\u00F3n y vuelva a intentarlo.");
		}
		return terminarCotizacionDTO;		
	}
	
	private TerminarCotizacionDTO validarConductoCobro(CotizacionDTO cotizacion){
		double montoPrimerRecibo = 0.0;
		CuentaPagoDTO cuenta = null;
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		MedioPagoDTO medioPagoDto = null;

		List<MedioPagoDTO> medioTmp = new ArrayList<MedioPagoDTO>();
		
		if(cotizacion.getIdMedioPago()!=null && cotizacion.getIdMedioPago().compareTo(BigDecimal.ZERO)>0) {
			medioPagoDto  = entidadService.findById(MedioPagoDTO.class,cotizacion.getIdMedioPago().intValue());
		}

		List<NegocioProducto> negocioProductos = entidadService.findByProperty(NegocioProducto.class, "negocio",cotizacion.getSolicitudDTO().getNegocio());
		
		for (NegocioProducto negocioProducto : negocioProductos) {
			if (cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto().compareTo((negocioProducto.getProductoDTO().getIdToProducto()))==0){
				List<NegocioMedioPago> negocioMedioPagoAsociadasList = entidadService.findByProperty(NegocioMedioPago.class, "negocioProducto.idToNegProducto", negocioProducto.getIdToNegProducto());
				for (NegocioMedioPago negocioMedioPago : negocioMedioPagoAsociadasList) {
					if(!medioTmp.contains(negocioMedioPago.getMedioPagoDTO())){
						medioTmp.add(negocioMedioPago.getMedioPagoDTO());
					}
				}
			}
		}

		if(cotizacion.getIdMedioPago() == null){
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_COBRANZA.getEstatus());
			terminarCotizacionDTO.setMensajeError("No se ha asignado un medio de pago para la cotizaci\u00F3n.");
		}else if(medioPagoDto!= null && !medioTmp.contains(medioPagoDto)) {
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO
					.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_COBRANZA
							.getEstatus());
			terminarCotizacionDTO
					.setMensajeError("El medio de pago asignado a la cotizaci\u00F3n no es valido.");
		}else if(cotizacion.getIdMedioPago().shortValue() != TipoCobro.EFECTIVO.valor()){
			try{
				cuenta = pagoService.obtenerConductoCobro(cotizacion.getIdToCotizacion());				
			}catch(Exception ex){
				LOG.error(ex.getMessage(), ex);
			}		
			if(cuenta == null || StringUtil.isEmpty(cuenta.getCuenta())){
				terminarCotizacionDTO = new TerminarCotizacionDTO();
				terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_DATOS_COBRANZA.getEstatus());
				terminarCotizacionDTO.setMensajeError("No se ha complementado la informaci\u00F3n del medio de pago. " +
						"Complem\u00E9ntela en la secci\u00F3n de Cobranza.");
			}
		}else{
//			montoPrimerRecibo = pagoService.obtenerMontoReciboInicial(cotizacion.getIdToCotizacion(), null, null);
//			if(montoPrimerRecibo < PolizaPagoService.RECIBO_MINIMO_PAGO_ANUAL){
//				terminarCotizacionDTO = new TerminarCotizacionDTO();
//				terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_DATOS_COBRANZA.getEstatus());
//				terminarCotizacionDTO.setMensajeError("El monto del primer recibo es indebido para el medio de pago seleccionado, " +
//						"favor de ajustar el medio de pago en la secci\u00F3n de Cobranza.");
//			}
		}		
		return terminarCotizacionDTO;
	}
	
	private TerminarCotizacionDTO validarConductoCobroPorInciso(CotizacionDTO cotizacion){
		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(cotizacion.getIdToCotizacion());
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		for(IncisoCotizacionDTO inciso: incisos){
			if(terminarCotizacionDTO != null){
				break;
			}
			if(inciso.getIdMedioPago() == null){
				terminarCotizacionDTO = new TerminarCotizacionDTO();
				terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_COBRANZA.getEstatus());
				terminarCotizacionDTO.setMensajeError("No se ha asignado un medio de pago para el Inciso (" + inciso.getNumeroSecuencia()+")");
			}else if(inciso.getIdMedioPago().shortValue() != TipoCobro.EFECTIVO.valor()){
				CuentaPagoDTO cuenta = null;
				try{
					cuenta = pagoService.obtenerConductoCobro(inciso.getIdClienteCob(), inciso.getIdConductoCobroCliente());				
				}catch(Exception ex){
					LOG.error(ex.getMessage(), ex);
				}		
				if(cuenta == null || StringUtil.isEmpty(cuenta.getCuenta())){
					terminarCotizacionDTO = new TerminarCotizacionDTO();
					terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_DATOS_COBRANZA.getEstatus());
					terminarCotizacionDTO.setMensajeError("No se ha complementado la informaci\u00F3n del medio de pago para el Inciso ("+ inciso.getNumeroSecuencia()+")");
				}
				//Validacion para en caso de traer datos en excel avance la emision
				if(inciso.getIdMedioPago().shortValue() == TipoCobro.TARJETA_CREDITO.valor()){
					if(inciso.getConductoCobro()!=""&&inciso.getInstitucionBancaria()!=""&&inciso.getTipoTarjeta()!=""&&inciso.getNumeroTarjetaClave()!=""&&inciso.getCodigoSeguridad()!=""&&inciso.getFechaVencimiento()!=""){
						terminarCotizacionDTO = null;	
					}
				}else if(inciso.getIdMedioPago().shortValue() == TipoCobro.DOMICILIACION.valor()||inciso.getIdMedioPago().shortValue() == TipoCobro.CUENTA_AFIRME.valor()){
					if(inciso.getConductoCobro()!=""&&inciso.getInstitucionBancaria()!=""&&inciso.getNumeroTarjetaClave()!=""){
						terminarCotizacionDTO = null;
					}
				}
			}
		}
		return terminarCotizacionDTO;
	}
	
	@SuppressWarnings("unchecked")
	private TerminarCotizacionDTO validarExcepciones(CotizacionDTO cotizacion, NivelEvaluacion nivelEvaluacion){
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		List<ExcepcionSuscripcionReporteDTO> lista = 
			(List<ExcepcionSuscripcionReporteDTO>) excepcionSuscripcionNegocioAutosService.evaluarCotizacion(
					cotizacion, nivelEvaluacion);
		if(!lista.isEmpty()){
			for(ExcepcionSuscripcionReporteDTO item : lista){
				if(item.getIdLineaNegocio() != null){
					NegocioSeccion seccion = entidadService.findById(NegocioSeccion.class, item.getIdLineaNegocio());
					item.setDescripcionSeccion(seccion != null ? seccion.getSeccionDTO().getDescripcion(): "NA");
				}else{
					item.setDescripcionSeccion("NA");
				}
				if(item.getIdCobertura() != null){					
					CoberturaCotizacionDTO cobertura = entidadService.findById(CoberturaCotizacionDTO.class, 
							new CoberturaCotizacionId(item.getIdToCotizacion(), item.getIdInciso(), 
									item.getIdLineaNegocio(), item.getIdCobertura()));
					item.setDescripcionCobertura(cobertura != null ? 
							cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getDescripcion() : "NA");					
				}else{
					item.setDescripcionCobertura("NA");
				}
				if(item.getIdInciso() != null){
					IncisoCotizacionId id = new IncisoCotizacionId();
					id.setIdToCotizacion(item.getIdToCotizacion());
					id.setNumeroInciso(item.getIdInciso());
					IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, id);
					if(inciso != null){
						item.setDescripcionInciso(inciso.getIncisoAutoCot().getDescripcionFinal());
					}
				}else{
					item.setDescripcionInciso("NA");					
				}
			}
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_EXCEPCIONES.getEstatus());
			terminarCotizacionDTO.setExcepcionesList(lista);
			terminarCotizacionDTO.setMensajeError("Algunos datos de la cotizaci\u00f3n provocaron excepciones que " +
					"requieren ser revisadas");
		}
		return terminarCotizacionDTO;
	}
		
	private TerminarCotizacionDTO validarSolicitudes(CotizacionDTO cotizacion){
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		List<SolicitudExcepcionCotizacion> lista = solicitudAutorizacionService.listarSolicitudesInvalidas(cotizacion.getIdToCotizacion());
		if(!lista.isEmpty()){
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_SOLICITUDES.getEstatus());
			terminarCotizacionDTO.setMensajeError("Esta cotizaci\u00F3n no puede ser terminada debido a que existen solicitudes de autorizacion " +
					"pendientes. Favor de revisar la bandeja de solicitudes.");
		}
		return terminarCotizacionDTO;
	}
	
	private TerminarCotizacionDTO validarDescuentosPorEstado(CotizacionDTO cotizacion){
		TerminarCotizacionDTO terminarCotizacionDTO = null;
		Map<Boolean, String> resultDescuentoPorEstado = null;
		for(IncisoCotizacionDTO incisoDTO : cotizacion.getIncisoCotizacionDTOs()){
			Double pctDescuentoPorEstado = incisoDTO.getIncisoAutoCot().getPctDescuentoEstado() != null ?
					incisoDTO.getIncisoAutoCot().getPctDescuentoEstado() : 0.0;
			resultDescuentoPorEstado = validacionService.validaDescuentoPorEstado(pctDescuentoPorEstado, 
					cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(), 
					incisoDTO.getIncisoAutoCot().getEstadoId(), 
					cotizacion.getIdToCotizacion(), 
					incisoDTO.getIncisoAutoCot().getNegocioPaqueteId());
			if(resultDescuentoPorEstado != null && resultDescuentoPorEstado.containsKey(true)){
				terminarCotizacionDTO = new TerminarCotizacionDTO();
				terminarCotizacionDTO.setEstatus(
				TerminarCotizacionDTO.Estatus.ERROR_MAX_DESCUENTO_POR_ESTADO.getEstatus());
				terminarCotizacionDTO.setMensajeError("Algunos datos de la cotización provocaron" +
						" excepciones que requieren ser revisadas. DESCUENTO POR ESTADO MAX:" + resultDescuentoPorEstado.get(true));
				break;
			}
		}
		return terminarCotizacionDTO;
	}
	
	/**
	 * Validar si la cotizacion esta completa y lista para emitir
	 */
	@Override
	public MensajeDTO validarListaParaEmitir(BigDecimal idToCotizacion) {
		MensajeDTO mensajeDTO = new MensajeDTO();
		boolean isRdyForEmission = true;
		boolean isValid = true;		
		String msj 	= "";		
		int complementados = 0;		
		Map<String, Object> map = new HashMap<String, Object>(1);
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);		
		Short estatusConfiguracion = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
		if(cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
			estatusConfiguracion = ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus();
		}		
		List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList(); 	
		final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
			+ "(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion )";
		map = new HashMap<String, Object>(1);
		map.put("idToCotizacion", idToCotizacion);		
		@SuppressWarnings({ "unchecked" })
		List<SubRamoDTO> subRamos = entidadService.executeQueryMultipleResult(queryString, map);
		List<ConfiguracionDatoInciso> configuracion = 
			configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(estatusConfiguracion);
		List<DatoIncisoCotAuto> riesgoCotizacion = datoIncisoCotAutoDao.getDatosIncisoCotAutoPorCotizacion(idToCotizacion);
		
		for(IncisoCotizacionDTO incisoDTO : cotizacion.getIncisoCotizacionDTOs()){
			isValid = true;
			IncisoAutoCot incisoAutoCot = incisoDTO.getIncisoAutoCot();			
			if(StringUtil.isEmpty(incisoAutoCot.getNumeroSerie()) || StringUtil.isEmpty(incisoAutoCot.getNumeroMotor()) || 
					StringUtil.isEmpty(incisoAutoCot.getPlaca()) || StringUtil.isEmpty(incisoAutoCot.getNombreAsegurado())){
				isValid = false;				
			}else{
				if(incisoService.infoConductorEsRequerido(incisoAutoCot.getIncisoCotizacionDTO())){
					if(StringUtil.isEmpty(incisoAutoCot.getNumeroLicencia()) || StringUtil.isEmpty(incisoAutoCot.getNombreConductor()) ||
							StringUtil.isEmpty(incisoAutoCot.getPaternoConductor()) || StringUtil.isEmpty(incisoAutoCot.getOcupacionConductor()) ||
							incisoAutoCot.getFechaNacConductor() == null){
						isValid = false;
					}				
				}
				if(isValid){	
					isValid = validaConfiguracionDatosRiesgo(configuracion, riesgoCotizacion, incisoDTO.getSeccionCotizacion(), ramos, subRamos);										
				}
			}
			//Valida Observaciones
			try{
			if(isValid){
				
				Integer usuarioExterno = 2;
				Usuario usuario = usuarioService.getUsuarioActual();
				if(usuario!=null && usuario.getTipoUsuario()!=null && usuario.getTipoUsuario()>0) {
					usuarioExterno = usuario.getTipoUsuario();
				}
				
				if(incisoService.observacionesEsRequerido(incisoDTO) && usuarioExterno != 2 && (incisoAutoCot.getObservacionesinciso() == null || incisoAutoCot.getObservacionesinciso().isEmpty())){
						isValid = false;
				}
			}
			}catch(Exception e){}
						
			if(isValid){
				complementados++;
			}else{
				isRdyForEmission = false;
			}
		}		
		msj = "Se tienen complementados " + complementados + " incisos de "+ cotizacion.getIncisoCotizacionDTOs().size();
		
		
		//validar contratante y validar forma pago
		if(isRdyForEmission){
			if(cotizacion.getIdToPersonaContratante() == null){
				isRdyForEmission = false;
			}
			//TODO validar forma de pago
		}
		
		mensajeDTO.setMensaje(msj);
		mensajeDTO.setVisible(isRdyForEmission);
		return mensajeDTO;		
	}
	
	@Override
	public boolean requiereDatosRiesgo(BigDecimal idToCotizacion, BigDecimal numeroInciso, Short nivelConfiguracionDatosRiesgo) {
		boolean requiere = false;
		Map<String, Object> map = new HashMap<String, Object>(1);		
		CotizacionDTO cotizacion 				= entidadService.findById(CotizacionDTO.class, idToCotizacion);
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion,numeroInciso));
		List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList(); 	
		final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
			+ "(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion " 
			+ " and toc.id.numeroInciso = :numeroInciso)";
		map = new HashMap<String, Object>(1);
		map.put("idToCotizacion", idToCotizacion);	
		map.put("numeroInciso", numeroInciso);
		@SuppressWarnings({ "unchecked" })
		List<SubRamoDTO> subRamos = entidadService.executeQueryMultipleResult(queryString, map);
		List<ConfiguracionDatoInciso> configuracion = 
			configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(nivelConfiguracionDatosRiesgo);
		List<DatoIncisoCotAuto> riesgoCotizacion = datoIncisoCotAutoDao.getDatosIncisoCotAutoPorCotizacion(idToCotizacion);		
		requiere = existenConfiguracionDatosRiesgo(configuracion, riesgoCotizacion, inciso.getSeccionCotizacion(), ramos, subRamos);
		return requiere;
	}
	
	@Override
	public boolean validaDatosRiesgo(BigDecimal idToCotizacion, BigDecimal numeroInciso, Short nivelConfiguracionDatosRiesgo) {
		boolean esValido = false;
		Map<String, Object> map = new HashMap<String, Object>(1);		
		CotizacionDTO cotizacion 				= entidadService.findById(CotizacionDTO.class, idToCotizacion);
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion,numeroInciso));
		List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList(); 	
		final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
			+ "(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion " 
			+ " and toc.id.numeroInciso = :numeroInciso)";
		map = new HashMap<String, Object>(1);
		map.put("idToCotizacion", idToCotizacion);	
		map.put("numeroInciso", numeroInciso);
		@SuppressWarnings({ "unchecked" })
		List<SubRamoDTO> subRamos = entidadService.executeQueryMultipleResult(queryString, map);
		List<ConfiguracionDatoInciso> configuracion = 
			configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(nivelConfiguracionDatosRiesgo);
		List<DatoIncisoCotAuto> riesgoCotizacion = datoIncisoCotAutoDao.getDatosIncisoCotAutoPorCotizacion(idToCotizacion);		
		esValido = validaConfiguracionDatosRiesgo(configuracion, riesgoCotizacion, inciso.getSeccionCotizacion(), ramos, subRamos);
		return esValido;
	}
	
	/**
	 * Encuentra las configuraciones de datos de riego para los parametros proporcionados
	 * @param configuracion
	 * @param idCobertura
	 * @param idRamo
	 * @param idSubRamo
	 * @param claveDetalle
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<ConfiguracionDatoInciso> encontrarConfiguracion(List<ConfiguracionDatoInciso> configuracion, final BigDecimal idCobertura, 
			final BigDecimal idRamo, final BigDecimal idSubRamo, final BigDecimal claveDetalle){				
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				ConfiguracionDatoInciso config = (ConfiguracionDatoInciso)arg0;
				ConfiguracionDatoIncisoId id  = config.getId();
				return id.getIdTcRamo().longValue() == idRamo.longValue() && id.getIdTcSubRamo().longValue() == idSubRamo.longValue() &&
				id.getClaveDetalle().longValue() == claveDetalle.longValue() &&	id.getIdToCobertura().longValue() == idCobertura.longValue();
			}
		};		
		return (List<ConfiguracionDatoInciso>)CollectionUtils.select(configuracion, predicate);	
		
	}
	
	/**
	 * Obtiene un registro dato inciso cot auto para la configuracion recibida
	 * @param datoRiesgoCotizacion
	 * @param idSeccion
	 * @param numInciso
	 * @param idCobertura
	 * @param idRamo
	 * @param idSubRamo
	 * @param claveDetalle
	 * @param idDato
	 * @return
	 */
	private DatoIncisoCotAuto encontrarRiesgoParaConfiguracion( List<DatoIncisoCotAuto> datoRiesgoCotizacion, final BigDecimal idSeccion, final BigDecimal numInciso, 
			final BigDecimal idCobertura, final BigDecimal idRamo, final BigDecimal idSubRamo, final BigDecimal claveDetalle, final BigDecimal idDato){		
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				DatoIncisoCotAuto datoRiesgo = (DatoIncisoCotAuto)arg0;
				return datoRiesgo.getIdTcRamo().longValue() == idRamo.longValue() && datoRiesgo.getIdTcSubRamo().longValue() == idSubRamo.longValue() &&
					datoRiesgo.getClaveDetalle().longValue() == claveDetalle.longValue() && datoRiesgo.getIdDato().longValue() == idDato.longValue() &&
					datoRiesgo.getIdToSeccion().longValue() == idSeccion.longValue() && datoRiesgo.getNumeroInciso().longValue() == numInciso.longValue() && 
					datoRiesgo.getIdToCobertura().longValue() == idCobertura.longValue();
			}
		};		
		return (DatoIncisoCotAuto)CollectionUtils.find(datoRiesgoCotizacion, predicate);		
	}
	
	/**
	 * Valida si existen configuraciones de riego para un inciso
	 * @param configuracionPrincipal
	 * @param datoRiesgoCotizacion
	 * @param seccion
	 * @param ramos
	 * @return
	 */
	private boolean existenConfiguracionDatosRiesgo(List<ConfiguracionDatoInciso> configuracionPrincipal, List<DatoIncisoCotAuto> datoRiesgoCotizacion, 
			SeccionCotizacionDTO seccion, List<RamoTipoPolizaDTO> ramos, List<SubRamoDTO> subRamos){
		boolean existen = false;		
		List<ConfiguracionDatoInciso> configuraciones = null;
		DatoIncisoCotAuto datoIncisoCotAuto = null;
		for (RamoTipoPolizaDTO ramo : ramos){						
			for(CoberturaCotizacionDTO cobertura : seccion.getCoberturaCotizacionLista()){
				if(cobertura.getClaveContratoBoolean().booleanValue()){
					BigDecimal idSubRamo = cobertura.getIdTcSubramo();
					configuraciones = encontrarConfiguracion(configuracionPrincipal, cobertura.getId().getIdToCobertura(), 
							ramo.getId().getIdtcramo(), idSubRamo, BigDecimal.valueOf(0d));
					if(!configuraciones.isEmpty()){
						existen = true;
						break;
					}
				}
			}
			if(!existen){				
				for (SubRamoDTO subRamo : subRamos) {
					configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
							ramo.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d));
					if(!configuraciones.isEmpty()){
						existen = true;
						break;
					}					
				}
			}
			if(!existen){
				configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
						ramo.getId().getIdtcramo(),BigDecimal.valueOf(0d), BigDecimal.valueOf(0d));
				if(!configuraciones.isEmpty()){
					existen = true;
					break;
				}
			}
			
		}		
		return existen;
	}
	
	
	/**
	 * Valida si existen configuraciones de riego complementadas para un inciso
	 * @param configuracionPrincipal
	 * @param datoRiesgoCotizacion
	 * @param seccion
	 * @param ramos
	 * @return
	 */
	public boolean validaConfiguracionDatosRiesgo(List<ConfiguracionDatoInciso> configuracionPrincipal, List<DatoIncisoCotAuto> datoRiesgoCotizacion, 
			SeccionCotizacionDTO seccion, List<RamoTipoPolizaDTO> ramos, List<SubRamoDTO> subRamos){
		boolean isValid = true;		
		List<ConfiguracionDatoInciso> configuraciones = null;
		DatoIncisoCotAuto datoIncisoCotAuto = null;
		for (RamoTipoPolizaDTO ramo : ramos){						
			for(CoberturaCotizacionDTO cobertura : seccion.getCoberturaCotizacionLista()){
				if(cobertura.getClaveContratoBoolean().booleanValue()){
					BigDecimal idSubRamo = cobertura.getIdTcSubramo();
					configuraciones = encontrarConfiguracion(configuracionPrincipal, cobertura.getId().getIdToCobertura(), 
							ramo.getId().getIdtcramo(), idSubRamo, BigDecimal.valueOf(0d));
					if(configuraciones != null){
						for(ConfiguracionDatoInciso configuracion : configuraciones){
							datoIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, seccion.getId().getIdToSeccion(), 
									seccion.getId().getNumeroInciso(), cobertura.getId().getIdToCobertura(), ramo.getId().getIdtcramo(), idSubRamo, BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
							if(datoIncisoCotAuto == null || StringUtil.isEmpty(datoIncisoCotAuto.getValor())){
								isValid = false;
								break;
							}
						}
					}
					if(!isValid){
						break;
					}
				}
			}
			if(isValid){				
				for (SubRamoDTO subRamo : subRamos) {
					configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
							ramo.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d));
					if(configuraciones != null){
						for(ConfiguracionDatoInciso configuracion : configuraciones){
							datoIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, seccion.getId().getIdToSeccion(), 
									seccion.getId().getNumeroInciso(), BigDecimal.valueOf(0d), ramo.getId().getIdtcramo(), 
									subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
							if(datoIncisoCotAuto == null || StringUtil.isEmpty(datoIncisoCotAuto.getValor())){
								isValid = false;
								break;
							}
						}
					}
					if(!isValid){
						break;
					}
				}
			}
			if(isValid){
				configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
						ramo.getId().getIdtcramo(),BigDecimal.valueOf(0d), BigDecimal.valueOf(0d));
				if(configuraciones != null){
					for(ConfiguracionDatoInciso configuracion : configuraciones){
						datoIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, seccion.getId().getIdToSeccion(), 
								seccion.getId().getNumeroInciso(), BigDecimal.valueOf(0d), ramo.getId().getIdtcramo(), 
								BigDecimal.valueOf(0d), BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
						if(datoIncisoCotAuto == null || StringUtil.isEmpty(datoIncisoCotAuto.getValor())){
							isValid = false;
							break;
						}
					}
				}
			}
			
		}		
		return isValid;
	}
	
	@Override
	public void actualizarEstatusCotizacion(BigDecimal idToCotizacion) {
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.setClaveEstatus(ESTATUS_COT_EMITIR);
		entidadService.save(cotizacion);
	}

	@Override
	public void actualizarEstatusCotizacionEnProceso(BigDecimal idToCotizacion){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		entidadService.save(cotizacion);
	}
	
	@Override
	public void actualizarEstatusCotizacionEmitida(BigDecimal idToCotizacion){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EMITIDA);
		entidadService.save(cotizacion);
	}
	@Override
	public void actualizarTipoImpresionCliente(BigDecimal idToCotizacion,BigDecimal idTipoImpresionCliente){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.setTipoImpresionCliente(idTipoImpresionCliente);
		entidadService.save(cotizacion);
	}
	@Override
	public void cancelarCotizacion(BigDecimal idToCotizacion){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_CANCELADA);
		entidadService.save(cotizacion);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void cancelarCotizacionByFolio(BigDecimal idToCotizacion, String folio){
		List<CotizacionDTO> cotizaciones = entidadService.findByProperty(CotizacionDTO.class, "folio",folio);
		
		if(!cotizaciones.isEmpty() && cotizaciones.size() > 1){
			for (CotizacionDTO cotizacionDTO : cotizaciones) {
				if(!cotizacionDTO.getIdToCotizacion().equals(idToCotizacion)){
					this.cancelarCotizacion(cotizacionDTO.getIdToCotizacion());
				}
			}
		}
		
	}
	
	@Override
	public BigDecimal actualizarDatosContratanteCotizacion(BigDecimal idToCotizacion, BigDecimal idCliente, String nombreUsuario){
		try {
			CotizacionDTO cotizacion = entidadService.findById(
					CotizacionDTO.class, idToCotizacion);
			ClienteGenericoDTO filtro = new ClienteGenericoDTO();
			filtro.setIdCliente(idCliente);
			ClienteGenericoDTO cliente = clienteFacadeRemote.loadById(filtro);
			
			if (cliente != null) {
				if (cliente.getIdDomicilioConsulta() == null || cliente.getCodigoPostalFiscal() == null
						|| cliente.getCodigoPostalFiscal().equals("")) {
					return null;
				}
				cotizacion.setIdDomicilioContratante(BigDecimal.valueOf(cliente.getIdDomicilioConsulta()));
				cotizacion.setIdToPersonaContratante(cliente.getIdCliente());
				cotizacion.setNombreContratante(cliente.getNombreCompleto());
				cotizacion.getSolicitudDTO().setClaveTipoPersona(cliente.getClaveTipoPersona());				
				entidadService.save(cotizacion);
			}
		} catch (Exception e) {
			LOG.error(e.getMessage(), e);
		}
		return idToCotizacion;
	}
	
	@Override
	public boolean validaIVACotizacion(BigDecimal idToCotizacion, String nombreUsuario){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		boolean cambioIVA = false;
		try {
			Double iva = codigoPostalIVAFacadeRemote.getIVAPorIdCotizacion(idToCotizacion, nombreUsuario);
			if(iva == null){
				throw new RuntimeException("Imposible encontrar iva para el domicilio");
			}			
			if((cotizacion.getPorcentajeIva() == null || !cotizacion.getPorcentajeIva().equals(iva))){
				cotizacion.setPorcentajeIva(iva);
				cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
				entidadService.save(cotizacion);
				cambioIVA = true;
			}
		} catch (SystemException e) {
			LOG.error(e.getMessage(), e);
		} 
		return cambioIVA;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public TerminarCotizacionDTO validarEmisionCotizacion(BigDecimal idToCotizacion){				
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		entidadService.refresh(cotizacion);
		TerminarCotizacionDTO terminarCotizacionDTO = null;		
		terminarCotizacionDTO = validarIncisosFlotilla(cotizacion);
		if(terminarCotizacionDTO != null){
			return terminarCotizacionDTO;
		}
		
		terminarCotizacionDTO = validarMonto(cotizacion);
		if(terminarCotizacionDTO != null){
			return terminarCotizacionDTO;
		}
		
		//La validacion en caso de que no permita cobranza por inciso
		Boolean habilitaCobInciso =  cobranzaIncisoService.habilitaCobranzaInciso(idToCotizacion);
		if(!habilitaCobInciso){
			terminarCotizacionDTO = validarConductoCobro(cotizacion);
			if(terminarCotizacionDTO != null){				
				return terminarCotizacionDTO;
			}
		}else{
			terminarCotizacionDTO = validarConductoCobroPorInciso(cotizacion);
			if(terminarCotizacionDTO != null){
				return terminarCotizacionDTO;
			}
		}
		
		
		terminarCotizacionDTO = validarSolicitudes(cotizacion);
		if(terminarCotizacionDTO != null){
			return terminarCotizacionDTO;
		}
		
		terminarCotizacionDTO = validarExcepciones(cotizacion, NivelEvaluacion.EMITIR);
		if(terminarCotizacionDTO != null){
			revisarConflictoNumeroSerie(terminarCotizacionDTO.getExcepcionesList(), idToCotizacion);
			return terminarCotizacionDTO;
		}	
		
		terminarCotizacionDTO = validarCambioAgrupacionRecibos(cotizacion);
		return terminarCotizacionDTO;
	}
	
	@Override
	public String obtenerNumeroPrimerRecibo() {
		String numeroPrimerRecibo = null;
				
		String spName="SEYCOS.PKG_SEGURO_FLOTILLA.stpConsDistRec";				
		String [] columnasCursor = { "NUMERO_RECIBO"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_INTERFAZ);									
			Object result = storedHelper.obtieneResultadoSencillo();
			if(result != null){
				numeroPrimerRecibo = result.toString();
			}
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}	
		return numeroPrimerRecibo;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public void calcularTodosLosIncisos(BigDecimal idCotizacion) {
		//List<IncisoCotizacionDTO> incisos = incisoService.getIncisos(idCotizacion);
		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idCotizacion);
		for(IncisoCotizacionDTO inciso : incisos){
			calculoService.eliminarDescuentos(inciso);
			calculoService.eliminarRecargos(inciso);
			calculoService.calcular(inciso);
			restauraIgualacionInciso(inciso);
		}		
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean calcularTodosLosIncisosNoIgualacion(BigDecimal idCotizacion) {
		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idCotizacion);
		boolean recalculo = false;
		for(IncisoCotizacionDTO inciso : incisos){
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("id.idToCotizacion", idCotizacion);
			params.put("id.numeroInciso", inciso.getId().getNumeroInciso());
			params.put("id.idToDescuentoVario", -1l);
			
			Map<String,Object> params2 = new LinkedHashMap<String,Object>();
			params2.put("id.idToCotizacion", idCotizacion);
			params2.put("id.numeroInciso", inciso.getId().getNumeroInciso());
			params2.put("id.idToRecargoVario", -1l);
		
			List<DescuentoCoberturaCot> descuentoCoberturaList = entidadService.findByProperties(DescuentoCoberturaCot.class, params);
			List<RecargoCoberturaCot> recargoCoberturaList = entidadService.findByProperties(RecargoCoberturaCot.class, params2);
			if((descuentoCoberturaList == null || descuentoCoberturaList.isEmpty()) &&
					(recargoCoberturaList == null || recargoCoberturaList.isEmpty())){
				calculoService.eliminarDescuentos(inciso);
				calculoService.eliminarRecargos(inciso);
				calculoService.calcular(inciso);
				restauraIgualacionInciso(inciso);
				recalculo = true;
			}
		}
		return recalculo;
	}
	
	private void restauraIgualacionInciso(IncisoCotizacionDTO incisoBase){
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, incisoBase.getId());
		inciso.setIgualacionNivelInciso(false);
		inciso.setPrimaTotalAntesDeIgualacion(null);
		inciso.setDescuentoIgualacionPrimas(null);
		entidadService.save(inciso);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Boolean calcularCambioDescuento(BigDecimal idCotizacion) {
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
		if(cotizacion.getIgualacionNivelCotizacion() != null && cotizacion.getIgualacionNivelCotizacion().booleanValue()){
			return false;
		}
		BigDecimal descuentoCoberturas = BigDecimal.ZERO;
		Boolean recalcular = false;
		if(cotizacion != null && cotizacion.getPorcentajeDescuentoGlobal() != null){
			Double descuentoGlobal = cotizacion.getPorcentajeDescuentoGlobal();
			Double descuentoVolumen = 0d;
			try{
				descuentoVolumen = getDescuentoPorVolumenaNivelPoliza(cotizacion);
			}catch(Exception e){
			}
			Double descuentoSobreCoberturas = descuentoGlobal > (descuentoVolumen != null ? descuentoVolumen.doubleValue() : 0d) ? 
					descuentoGlobal : descuentoVolumen;
			descuentoCoberturas = new BigDecimal(descuentoSobreCoberturas);
		}
		
		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idCotizacion);
		if(incisos != null && !incisos.isEmpty()){
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("id.idToCotizacion", idCotizacion);
			params.put("id.numeroInciso", incisos.get(0).getId().getNumeroInciso());
			params.put("id.idToDescuentoVario", -5l);
		
			List<DescuentoCoberturaCot> descuentoCoberturaList = entidadService.findByProperties(DescuentoCoberturaCot.class, params);
			if(descuentoCoberturaList != null && !descuentoCoberturaList.isEmpty()){
				if(descuentoCoberturas.compareTo(descuentoCoberturaList.get(0).getValorDescuento()) != 0){
					recalcular = true;
				}
			}else if(descuentoCoberturas.compareTo(BigDecimal.ZERO) != 0){
				recalcular = true;
			}
		}

		if(recalcular){
			recalcular = calcularTodosLosIncisosNoIgualacion(idCotizacion);
		}
		return recalcular;
	}
	
	@Override
	public CotizacionDTO updateSolicitudCotizacion(CotizacionDTO cotizacion){
		Usuario usuario = usuarioService.getUsuarioActual();
		
		NegocioTipoPoliza negocioTipoPoliza  = cotizacion.getNegocioTipoPoliza();
		negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class,cotizacion.getNegocioTipoPoliza().getIdToNegTipoPoliza());

		SolicitudDTO solicitud = cotizacion.getSolicitudDTO();
		if(solicitud != null && solicitud.getIdToSolicitud() != null) {
			solicitud = entidadService.findById(SolicitudDTO.class, cotizacion.getSolicitudDTO().getIdToSolicitud());
		
			Negocio negocio = cotizacion.getSolicitudDTO().getNegocio();
			NegocioProducto   negocioProducto = cotizacion.getNegocioTipoPoliza().getNegocioProducto();
			negocioProducto = entidadService.findById(NegocioProducto.class,
					cotizacion.getNegocioTipoPoliza().getNegocioProducto()
							.getIdToNegProducto());
			solicitud.setProductoDTO(negocioProducto.getProductoDTO());
			negocio = entidadService.findById(Negocio.class,cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio());
			solicitud.setNegocio(negocio);
			solicitud.setClaveEstatus(SolicitudDTO.Estatus.TERMINADA.getEstatus());
			solicitud.setFechaCreacion(Calendar.getInstance().getTime());
			solicitud.setClaveTipoSolicitud((short) 1);
			if (solicitud.getIdToPolizaAnterior() == null) {
				solicitud.setIdToPolizaAnterior(BigDecimal.ZERO);
			}
			solicitud.setCodigoAgente(cotizacion.getSolicitudDTO().getCodigoAgente());
			
			//Valores de Fuerza de Venta
			if (solicitud.getCodigoAgente() != null) {
				Agente filtro = new Agente();
				try{
					filtro.setId(solicitud.getCodigoAgente().longValue());				
					Agente agente = agenteMidasService.loadById(filtro);
				//Agente agente = entidadService.findById(Agente.class, solicitud.getCodigoAgente().longValue());
					if(agente != null){
						solicitud.setCodigoEjecutivo(new BigDecimal(agente.getPromotoria().getEjecutivo().getIdEjecutivo()));
						solicitud.setNombreAgente(agente.getPersona().getNombreCompleto());
						if(agente.getPromotoria().getEjecutivo() != null){
							Ejecutivo filtroEjecutivo =new Ejecutivo();
							filtroEjecutivo.setId(agente.getPromotoria().getEjecutivo().getId());
				            Ejecutivo ejecutivo = ejecutivoService.loadById(filtroEjecutivo);
				            if(ejecutivo != null){
								solicitud.setNombreEjecutivo(ejecutivo.getPersonaResponsable().getNombreCompleto());
								solicitud.setNombreOficina(ejecutivo.getGerencia().getPersonaResponsable().getNombreCompleto());
								solicitud.setIdOficina(new BigDecimal(ejecutivo.getGerencia().getId()));
				            }
						}					
						solicitud.setNombreOficinaAgente(agente.getPromotoria().getDescripcion());
					}
				}catch(Exception e){
				
				}
			}
	        solicitud.setCodigoAgente(solicitud.getCodigoAgente()==null?BigDecimal.ZERO:solicitud.getCodigoAgente());
	        solicitud.setClaveOpcionEmision((short) 0);
	        solicitud.setFechaCreacion(new Date());
	        solicitud.setCodigoUsuarioCreacion(usuario.getNombreUsuario());
	        solicitud.setFechaModificacion(new Date());
	        solicitud.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
	        solicitud.setClaveTipoPersona((short) 1);
	        solicitud.setNombrePersona(usuario.getNombre());
	        solicitud.setApellidoPaterno(usuario.getApellidoPaterno());
	        solicitud.setApellidoMaterno(usuario.getApellidoMaterno());
		}
		solicitud.setClaveTipoSolicitud((short) 0);
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.TERMINADA.getEstatus());
		cotizacion.setSolicitudDTO(solicitudFacadeRemote.update(solicitud));
		cotizacion.setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
		cotizacion.setFechaModificacion(new Date());
		cotizacion.setCodigoUsuarioModificacion(usuario.getNombreUsuario());
		cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_EN_PROCESO);
		if(cotizacion.getTipoCambio() == null){
			cotizacion.setTipoCambio(new Double(1));
		}
        
        cotizacion.setNegocioTipoPoliza(null);
        cotizacion.setDireccionCobroDTO(null);
        
        try{
        if(cotizacion.getNegocioDerechoPoliza() != null && cotizacion.getNegocioDerechoPoliza().getIdToNegDerechoPoliza() != null){
        	NegocioDerechoPoliza negocioDerechoPoliza = entidadService.findById(NegocioDerechoPoliza.class, cotizacion.getNegocioDerechoPoliza().getIdToNegDerechoPoliza());
        	if(negocioDerechoPoliza != null){
        		cotizacion.setValorDerechosUsuario(negocioDerechoPoliza.getImporteDerecho());
        	}
        }
        }catch(Exception e){
        	
        }
        
        entidadService.save(cotizacion);
        
        //Elimina comisiones y documentos pasados
		try{
			List<ComisionCotizacionDTO> comisionList = entidadService.findByProperty(ComisionCotizacionDTO.class, "cotizacionDTO.idToCotizacion", cotizacion.getIdToCotizacion());
			if(comisionList != null && !comisionList.isEmpty()){
				for(ComisionCotizacionDTO comision : comisionList){
					entidadService.remove(comision);
				}
			}
		}catch(Exception e){
			
		}
		try{
			List<DocAnexoCotDTO> comisionList = entidadService.findByProperty(DocAnexoCotDTO.class, "cotizacionDTO.idToCotizacion", cotizacion.getIdToCotizacion());
			if(comisionList != null && !comisionList.isEmpty()){
				for(DocAnexoCotDTO comision : comisionList){
					entidadService.remove(comision);
				}
			}
		}catch(Exception e){
			
		}
        //Registra nuevos valores
		estadisticas.registraEstadistica(cotizacion, CotizacionDTO.ACCION.MODIFICACION);
		comisionService.generarComision(cotizacion);
		anexoCotFacadeRemote.generateDocumentsByCotizacion(cotizacion.getIdToCotizacion());
		
		return cotizacion;
	}
	
	@Override
	public void inicializaIgualacionCotizacion(BigDecimal idToCotizacion){
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);	
		int totalIncisos = 0;
		try{
			List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
			if(incisos != null){
				totalIncisos = incisos.size();
			}
		}catch(Exception e){
		}
		if(totalIncisos <= 1){
			cotizacionDTO.setIgualacionNivelCotizacion(Boolean.FALSE);
			cotizacionDTO.setPrimaTotalAntesDeIgualacion(null);
			cotizacionDTO.setDescuentoIgualacionPrimas(null);
			entidadService.save(cotizacionDTO);			
		}
	}
		
	@Override	
	public void actualizarTipoAgrupacionRecibos(BigDecimal idToCotizacion,
			String tipoAgrupacionRecibos) {
		boolean isValid = false;
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		Negocio negocio = cotizacion.getSolicitudDTO().getNegocio();
		TipoPolizaDTO tipoPoliza = cotizacion.getTipoPolizaDTO();
		
		//validar que negocio tenga habilitada opcion y aplique flotilla
		if(negocio.getHabilitaAgrupacionRecibos() == null || !negocio.getHabilitaAgrupacionRecibos().booleanValue() || 
				tipoPoliza.getClaveAplicaFlotillas() == null || tipoPoliza.getClaveAplicaFlotillas().intValue() !=  1){
			throw new NegocioEJBExeption("AGRUP_EX_001", "Negocio no configurado para modificar tipo de agrupaci\u00F3n");
		}		
		
		//validar que la seleccion este incluida 
		List<TIPO_AGRUPACION_RECIBOS> listTipoAgrupacionRecibos = Arrays.asList(TIPO_AGRUPACION_RECIBOS.values());
		for(TIPO_AGRUPACION_RECIBOS tipo : listTipoAgrupacionRecibos){
			if(!tipoAgrupacionRecibos.equals(TIPO_AGRUPACION_RECIBOS.FILIAL.getValue()) && 
					tipo.getValue().equals(tipoAgrupacionRecibos)){ //Filial no aplica para autos
				isValid = true;
				break;
			}
		}
		if(!isValid){
			throw new NegocioEJBExeption("AGRUP_EX_002", "Tipo de agrupaci\u00F3n inv\u00E1lida");
		}		
		
		cotizacion.setTipoAgrupacionRecibos(tipoAgrupacionRecibos);
		entidadService.save(cotizacion);
		
		//20170104 Recuotificacion - regenrar programas de pago cuando se cambia el tipo de agrupacion
		recuotificacionService.generaProgramasPago(idToCotizacion, REGENAR_PREVIO_RECIBOS, null, null);
	}

	@Override
	public Boolean validarCotizacionRecuotificada(BigDecimal idToCotizacion){
		boolean recuotificada = false;
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if((cotizacion.getRecuotificadaAutomatica() != null && cotizacion.getRecuotificadaAutomatica())
				|| (cotizacion.getRecuotificadaManual() != null && cotizacion.getRecuotificadaManual())){
			recuotificada = true;
		}
		return recuotificada;
	}
	
	private TerminarCotizacionDTO validarCambioAgrupacionRecibos(CotizacionDTO cotizacion){
		TerminarCotizacionDTO terminarCotizacionDTO = null;			
		Negocio negocio = cotizacion.getSolicitudDTO().getNegocio();
		TipoPolizaDTO tipoPoliza = cotizacion.getTipoPolizaDTO();
		
		//validar que negocio tenga habilitada opcion y aplique flotilla
		if(cotizacion.getTipoAgrupacionRecibos() != null && (negocio.getHabilitaAgrupacionRecibos() == null || 
				!negocio.getHabilitaAgrupacionRecibos().booleanValue() || tipoPoliza.getClaveAplicaFlotillas() == null || 
					tipoPoliza.getClaveAplicaFlotillas().intValue() !=  1)){
			terminarCotizacionDTO = new TerminarCotizacionDTO();
			terminarCotizacionDTO.setEstatus(TerminarCotizacionDTO.Estatus.ERROR_AGRUPACION_RECIBOS.getEstatus());
			terminarCotizacionDTO.setMensajeError("Negocio no configurado para modificar tipo de agrupaci\u00F3n");
		}		
				
		return terminarCotizacionDTO;		
	}
	
	public CotizacionDTO obtenerCotizacionPorId(BigDecimal idToCotizacion){
		return  entidadService.findById(CotizacionDTO.class, idToCotizacion);
	}
	
	@Override
	public EsquemaPagoCotizacionDTO findEsquemaPagoCotizacion(CotizacionDTO cotizacion){
		return findEsquemaPagoCotizacion(cotizacion.getFechaInicioVigencia(), cotizacion.getFechaInicioVigencia(),
		cotizacion.getFechaFinVigencia(), Integer.valueOf(cotizacion.getIdFormaPago().toString()),
		BigDecimal.valueOf(cotizacion.getPrimaNetaTotal()),
		BigDecimal.valueOf(cotizacion.getValorDerechosUsuario()*cotizacion.getIncisoCotizacionDTOs().size()),
		BigDecimal.valueOf(cotizacion.getPorcentajeIva()), Integer.valueOf(cotizacion.getIdMoneda().toString()),
		Double.valueOf(cotizacion.getPrimaNetaTotal().doubleValue() * (cotizacion.getPorcentajePagoFraccionado() / 100)),
		true);
	}
	
	
	@Override
	public void eliminarCotizacion(BigDecimal idToCotizacion) {
				
		String spName="MIDAS.pkgAUT_Generales.spAut_EliminaCotizacion";				
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToCotizacion", idToCotizacion);
			int result = storedHelper.ejecutaActualizar();

			if(result == 1){
				throw new Exception("Error al borrar cotizacion");
			}
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}	
	}
	
	@EJB
	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}
	
	@EJB	
	public void setNegocioProductoDao(NegocioProductoDao negocioProductoDao) {
		this.negocioProductoDao = negocioProductoDao;
	}
	
	@EJB
	public void setConfiguracionDatoIncisoDao(
			ConfiguracionDatoIncisoDao configuracionDatoIncisoDao) {
		this.configuracionDatoIncisoDao = configuracionDatoIncisoDao;
	}
	
	@EJB
	public void setDatoIncisoCotAutoService(
			DatoIncisoCotAutoService datoIncisoCotAutoService) {
		this.datoIncisoCotAutoService = datoIncisoCotAutoService;
	}
	
	@EJB
	public void setDatoIncisoCotAutoDao(DatoIncisoCotAutoDao datoIncisoCotAutoDao) {
		this.datoIncisoCotAutoDao = datoIncisoCotAutoDao;
	}

	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}

	@EJB
	public void setEjecutivoService(EjecutivoService ejecutivoService) {
		this.ejecutivoService = ejecutivoService;
	}

	@EJB
	public void setCentroEmisorService(CentroEmisorService centroEmisorService) {
		this.centroEmisorService = centroEmisorService;
	}
	
	@EJB
	public void setValidacionService(ValidacionService validacionService) {
		this.validacionService = validacionService;
	}
}
	
