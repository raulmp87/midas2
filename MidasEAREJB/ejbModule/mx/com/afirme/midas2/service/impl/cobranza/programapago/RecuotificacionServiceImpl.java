package mx.com.afirme.midas2.service.impl.cobranza.programapago;

import java.math.BigDecimal;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.cobranza.programapago.ProgramaPagoService;
import mx.com.afirme.midas2.service.cobranza.programapago.RecuotificacionService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.util.StringUtil;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Stateless
public class RecuotificacionServiceImpl implements RecuotificacionService{
	
	
	private static final long serialVersionUID = -6780139391604873594L;
	
	private static final Logger LOG = Logger.getLogger(RecuotificacionServiceImpl.class);
	
	@SuppressWarnings("unused")
	private DataSource dataSource;
	
	private SimpleJdbcCall ejecucionProgramasPago;
	private SimpleJdbcCall ejecucionValidacionTotalesCotizacion;
	
	private SimpleJdbcCall ejecucionValidacionRecuotificacion;
	
	private SimpleJdbcCall ejecucionMontoPrimerReciboRecuotificado;

	private static final String CATALOG_NAME = "PKGAUT_RECUOTIFICAII";
	
	private static final String ENCENDIDO = "1";
	
	//LLAMADA ENDOSO
	private SimpleJdbcCall llamadaStoreProgramaPagoEndoso;
	
	//LLAMADA ENDOSO AJUSTE DE PRIMA
	private SimpleJdbcCall llamadaStorePPAjustePrima;

	//LLAMADA VALIDACION ENDOSO MOVIMIENTOS
	private SimpleJdbcCall validaPrimasDeEndoso;
	
	//LLamada obtener idSol
	private SimpleJdbcCall obtenerIdSolicitudDeContinuidad;
	
	@EJB 
	private CalculoService calculoService;
	@EJB 
	private CotizacionService cotizacionService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private ProgramaPagoService programaPagoService;	
	@EJB
	private ParametroGeneralService parametroGeneralService;
	

	
	
	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
		this.ejecucionProgramasPago = new SimpleJdbcCall(jdbcTemplate)
				.withCatalogName(CATALOG_NAME)
				.withProcedureName("generaProgramasPago")
				.withSchemaName("MIDAS");
		
		//Inicializacion Store
		this.llamadaStoreProgramaPagoEndoso = new SimpleJdbcCall(jdbcTemplate)
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("generaProgramaPagoEndosoAuto")
		.withSchemaName("MIDAS");
		
		//Inicializacion Store validacion totales
		this.ejecucionValidacionTotalesCotizacion = new SimpleJdbcCall(jdbcTemplate)
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("prcValidaSaldo")
		.withSchemaName("MIDAS");
		
		
		this.ejecucionValidacionRecuotificacion = new SimpleJdbcCall(jdbcTemplate)
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("prcValidaRecuotificacion")
		.withSchemaName("MIDAS");
		
		this.ejecucionMontoPrimerReciboRecuotificado = new SimpleJdbcCall(jdbcTemplate)
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("prcMontoPrimerRecibo")
		.withSchemaName("MIDAS");
		
		
		//Inicializacion Store PP ajuste prima
		this.llamadaStorePPAjustePrima = new SimpleJdbcCall(jdbcTemplate)
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("generaProgramaPagoEndosoAuto2")
		.withSchemaName("MIDAS");
		
		//Inicializacion Store PP validaPrimasDeEndoso
		this.validaPrimasDeEndoso = new SimpleJdbcCall(jdbcTemplate)
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("validaPrimasDeEndoso")
		.withSchemaName("MIDAS");
		
		this.obtenerIdSolicitudDeContinuidad = new SimpleJdbcCall(jdbcTemplate)
		.withCatalogName(CATALOG_NAME)
		.withProcedureName("obtenerSolicitudDeContinuidad")
		.withSchemaName("MIDAS");
		
	}
	
	@Override	
	public Map<String, Object> generaProgramasPago(BigDecimal idToCotizacion, Integer banderaRecuotifica,  String tipoMov, BigDecimal idToSolicitud) {
		Map<String, Object> resultado = null;
		
		try{
			ParametroGeneralDTO parametroGeneral  = parametroGeneralService.findByDescCode(
					ParametroGeneralDTO.GRUPO_RECUOTIFICACION_AUTOS, ParametroGeneralDTO.PARAMETRO_RECUOTIFICACION_AUTOS_MODULO_ACTIVO);
			if(parametroGeneral != null && parametroGeneral.getValor().equals(ENCENDIDO)){
				
				if(tipoMov == null){
					CotizacionDTO cotizacionDTO = cotizacionService.obtenerCotizacionPorId(idToCotizacion);	
					ResumenCostosDTO resumenCostosDTO= calculoService.obtenerResumenCotizacion(cotizacionDTO, false);	
					LOG.debug("pImpPrimaNeta: " + resumenCostosDTO.getPrimaNetaCoberturas());
					LOG.debug("pImpDerechos: " +  resumenCostosDTO.getDerechos());
					LOG.debug("pImpRcgoPagoFr: " + resumenCostosDTO.getRecargo());
					LOG.debug("pImpIva: " + resumenCostosDTO.getIva());
					LOG.debug("pImpPrimaTotal: " + resumenCostosDTO.getPrimaTotal());
					
					 SqlParameterSource in = new MapSqlParameterSource()
					.addValue("PidToCotizacion", idToCotizacion)
					.addValue("PRECUOTIFICA", banderaRecuotifica)
					
					//PARAMETROS DE MODIFICACION
					.addValue("pImpPrimaNeta", resumenCostosDTO.getPrimaNetaCoberturas())
					.addValue("pImpDerechos", resumenCostosDTO.getDerechos())
					.addValue("pImpRcgoPagoFr", resumenCostosDTO.getRecargo())
					.addValue("pImpIva", resumenCostosDTO.getIva())
					.addValue("pImpPrimaTotal", resumenCostosDTO.getPrimaTotal());
					
					resultado = ejecucionProgramasPago.execute(in);
					
					BigDecimal codigoRespuesta = (BigDecimal) resultado.get("pId_Cod_Resp");
					String descripcionRespuesta = (String) resultado.get("pDesc_Resp");
					if (codigoRespuesta != null) {
						LOG.info("REGENERACION PREVIO RECIBOS PARA ID COT ".concat(idToCotizacion.toString()).concat(": ").concat(
								codigoRespuesta.toString()).concat(" - ").concat(StringUtil.isEmpty(descripcionRespuesta)? "NA" : descripcionRespuesta));				
					}
					
				}else if (tipoMov != null && tipoMov.equals("E")){
					LOG.info("Cargar Programa Original para Endoso" );
					SqlParameterSource in = new MapSqlParameterSource()
					.addValue("inIdToSolicitud", idToSolicitud)
					.addValue("precuotifica", banderaRecuotifica);
					resultado = llamadaStoreProgramaPagoEndoso.execute(in);
					
				}else if (tipoMov != null && tipoMov.equals("AP")){
					LOG.info("EL MODULO DE RECUOTIFICACION SE ENCUENTRA APAGADO" );
				}
				
			}else{
				LOG.info("EL MODULO DE RECUOTIFICACION SE ENCUENTRA APAGADO" );
			}
		}catch(Exception ex){
			LOG.error("Ocurrió un error al generar el previo de recibos para la cotizacion ".concat(idToCotizacion.toString()), ex);
		}
		return resultado;
	}


	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Object> generaProgramasPagoEndoso(BigDecimal idToSolicitud, String precuotifica) {		
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("inIdToSolicitud", idToSolicitud)
		.addValue("precuotifica", precuotifica);
		Map<String, Object> res = llamadaStoreProgramaPagoEndoso.execute(in);
		return res;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Object> validaTotalesDeCotizacionSalir(BigDecimal idToCotizacion) {
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("pidtocotizacion", idToCotizacion);
		return ejecucionValidacionTotalesCotizacion.execute(in);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Object> generaProgramasPagoEndosoAP(BigDecimal idContinuity, BigDecimal tipoFormaPago, String precuotifica, BigDecimal numeroEndoso) {		
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("inIdToContinuidad", idContinuity)
		.addValue("inBMismaFp", tipoFormaPago)
		.addValue("precuotifica", precuotifica)
		.addValue("inNumeroEndoso", numeroEndoso);
		Map<String, Object> res = llamadaStorePPAjustePrima.execute(in);
		return res;
	}
	
	public boolean validarPrevioDeRecibos(BigDecimal idToCotizacion){
		return  programaPagoService.validarPrevioDeRecibos(idToCotizacion);
		//return false;
	}
	
	public Integer validarEstatusRecuotificacion(BigDecimal idGrupoParametro, BigDecimal codigoParametro){
		return programaPagoService.obtenerParametroGeneral(idGrupoParametro, codigoParametro);
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Object> validaPrimasDeEndoso(BigDecimal idToSolicitud) {		
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("inIdToSolicitud", idToSolicitud);
		Map<String, Object> res = validaPrimasDeEndoso.execute(in);
		return res;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BigDecimal obtenerSolicitudDeContinuidad(BigDecimal idContinuity){
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("inIdToContinuidad", idContinuity);
		Map<String, Object> res = obtenerIdSolicitudDeContinuidad.execute(in);
		BigDecimal result = (BigDecimal)res.get("outIdEjecucion");
		return result;
	}


	@Override
	public void marcarCotizacionRecuotificada(BigDecimal idToCotizacion,
			TipoRecuotificacion tipo) {
		marcarCotizacionRecuotificada(idToCotizacion, tipo, Boolean.TRUE);
	}


	@Override
	public boolean validarCotizacionRecuotificada(BigDecimal idToCotizacion,
			TipoRecuotificacion tipo) {
		Boolean flag;
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if(tipo.equals(TipoRecuotificacion.AUTOMATICA)){
			flag = cotizacion.getRecuotificadaAutomatica();
		}else{
			flag = cotizacion.getRecuotificadaManual();
		}		
		return flag != null && flag.booleanValue() ? true : false;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public boolean validarAplicaRecuotificacion(BigDecimal idToCotizacion) {
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("pidtocotizacion", idToCotizacion);
		Map<String, Object> res = ejecucionValidacionRecuotificacion.execute(in);
		return ((BigDecimal) res.get("outIdEjecucion")).equals(new BigDecimal(1));
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public double obtenerMontoPrimerReciboRecuotificado(BigDecimal idToCotizacion) {
		SqlParameterSource in = new MapSqlParameterSource()
		.addValue("pidtocotizacion", idToCotizacion);
		Map<String, Object> res = ejecucionMontoPrimerReciboRecuotificado.execute(in);
		return Double.parseDouble(res.get("outIdEjecucion").toString());
	
	}

	@Override
	public void marcarCotizacionRecuotificada(BigDecimal idToCotizacion,
			TipoRecuotificacion tipo, Boolean valor) {
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		if(tipo.equals(TipoRecuotificacion.AUTOMATICA)){
			cotizacion.setRecuotificadaAutomatica(valor);
		}else{
			cotizacion.setRecuotificadaManual(valor);
		}
		entidadService.save(cotizacion);
	}

}
