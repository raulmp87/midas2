package mx.com.afirme.midas2.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas2.service.OracleCurrentDateService;

import org.joda.time.DateTime;

/**
 * Obtains the current system date from an Oracle Database.
 */
@Stateless
public class OracleCurrentDateServiceImpl implements OracleCurrentDateService {

	private EntityManager entityManager;
	
	@PersistenceContext
	void setEntityManager(EntityManager entityManager) {
		this.entityManager = entityManager;
	}
	
	private static final String DATE_PATTERN = "yyyy-MM-dd HH:mm:ss.SSS";
	
	@Override
	public DateTime getCurrentDate() {
		String strQuery = "SELECT to_char(systimestamp, 'YYYY-MM-DD HH24:MI:SS.FF3') FROM dual";
		Query query = entityManager.createNativeQuery(strQuery);
		String strDate = (String) query.getSingleResult();
		Date date;
		try {
			SimpleDateFormat SDF = new SimpleDateFormat(DATE_PATTERN);
			date = SDF.parse(strDate);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		return new DateTime(date.getTime());
	}

}
