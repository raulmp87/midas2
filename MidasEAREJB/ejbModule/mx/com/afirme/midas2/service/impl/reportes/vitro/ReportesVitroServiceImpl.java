package mx.com.afirme.midas2.service.impl.reportes.vitro;

import java.io.IOException;
import java.io.Serializable;
import java.lang.reflect.InvocationTargetException;
import java.util.Date;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.poi.openxml4j.exceptions.InvalidFormatException;

import mx.com.afirme.midas2.dao.reportes.vitro.ReportesVitroDAO;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.reportes.vitro.ReportesVitroDTO;
import mx.com.afirme.midas2.service.reportes.MidasBaseReporteService;
import mx.com.afirme.midas2.service.reportes.vitro.ReportesVitroService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.util.ExcelExporter;

import com.afirme.nomina.model.*;

@Stateless
public class ReportesVitroServiceImpl implements ReportesVitroService, Serializable {
	
	private static final long serialVersionUID = -335262066234639671L;

	@EJB
	private ReportesVitroDAO reportesVitroDao;
	
	@EJB
	private MidasBaseReporteService impresionService;
	
	@EJB(beanName = "UsuarioServiceDelegate")
	public UsuarioService usuarioService;

	@Override
	public List<Date> consultarCalendarioCobro(Long idGrupo) {
		List<Date> cobros = null;
		if(idGrupo != null && idGrupo > 0L){
			cobros = reportesVitroDao.consultarCalendarioGrupo(idGrupo);
		}
		return cobros;
	}

	@Override
	public List<Dpngrupo> consultarGruposVitro() {
		return reportesVitroDao.consultarGruposVitro();
	}
	
	@Override
	public List<ReportesVitroDTO> consultarPolizasVitroSemanal(Date fechaCorte, Long idNegocio, Long idFormaPago){
		return consultarPolizasVitro(fechaCorte, NOMINA_SEMANAL, idNegocio, idFormaPago, DIAS_SEMANA, GRUPO_NOMINA_SEMANAL);
	}
	
	@Override
	public List<ReportesVitroDTO> consultarPolizasVitroQuincenal(Date fechaCorte, Long idNegocio, Long idFormaPago){
		return consultarPolizasVitro(fechaCorte, NOMINA_QUINCENAL, idNegocio, idFormaPago, DIAS_QUINCENA, GRUPO_NOMINA_QUINCENAL);
	}
	
	@Override
	public List<Dpncliente> consultarCliente(String idNegocio){
		return reportesVitroDao.consultarCliente(LIKE_GRUPOS_VITRO, idNegocio);
	}
	
	@Override
	public List<Negocio> consultarNegocios(){
		Agente agente = usuarioService.getAgenteUsuarioActual();
		if(agente == null || agente.getId() == null || agente.getId() < 0){
			agente = new Agente();
			agente.setId(ID_AGENTE_VENTA_VITRO);
		}
		return reportesVitroDao.consultarNegocios(agente.getId());
	}
	
	@Override
	public List<ReportesVitroDTO> consultarPolizasVitro(Date fechaCorte, String tipoNomina, Long idNegocio, Long idFormaPago, Long numeroDivide, String nombreGrupo){
		return reportesVitroDao.consultarPolizasVitro(fechaCorte, tipoNomina, idNegocio, idFormaPago, numeroDivide, nombreGrupo);
	}
	
	@Override
	public TransporteImpresionDTO generarReporteVitro(List<ReportesVitroDTO> dataSource, String nombreLibro) throws InvalidFormatException, IllegalAccessException, InvocationTargetException, IOException{
		ExcelExporter exporter = new ExcelExporter(ReportesVitroDTO.class);
		return exporter.exportXLS(dataSource, nombreLibro);
	}

	public ReportesVitroDAO getReportesVitroDao() {
		return reportesVitroDao;
	}

	public void setReportesVitroDao(ReportesVitroDAO reportesVitroDao) {
		this.reportesVitroDao = reportesVitroDao;
	}

	public MidasBaseReporteService getImpresionService() {
		return impresionService;
	}

	public void setImpresionService(MidasBaseReporteService impresionService) {
		this.impresionService = impresionService;
	}

}
