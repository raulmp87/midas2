package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.cobertura;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaSeccionDTOId;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.coaseguro.CoaseguroCoberturaDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.dao.catalogos.CobPaquetesSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionDAO;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cobertura.CoberturaDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionCoberturaView;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccion;
import mx.com.afirme.midas2.domain.catalogos.CobPaquetesSeccionId;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.sumaasegurada.NegocioCobSumAse;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNeg;
import mx.com.afirme.midas2.domain.suscripcion.cambiosglobales.ConfiguracionPlantillaNegCobertura;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CoberturaCotizacionExpress;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.CotizacionExpress;
import mx.com.afirme.midas2.dto.negocio.motordecision.MotorDecisionDTO;
import mx.com.afirme.midas2.dto.negocio.motordecision.ResultadoMotorDecisionAtributosDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fronterizos.FronterizosService;
import mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccionServiceImpl;
import mx.com.afirme.midas2.service.negocio.motordecision.MotorDecisionAtributosService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducible.NegocioDeduciblesService;
import mx.com.afirme.midas2.service.suscripcion.cambiosglobales.ConfiguracionPlantillaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.util.UtileriasWeb;

import org.apache.axis.utils.StringUtils;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.log4j.Logger;

@Stateless
public class CoberturaServiceImpl implements CoberturaService {

	public static final Logger LOG = Logger.getLogger(NegocioCobPaqSeccionServiceImpl.class);
	
	EntidadService entidadService;
	NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO;
	CobPaquetesSeccionDao cobPaquetesSeccionDao;
	CalculoService calculoService; 
	ConfiguracionPlantillaService configuracionPlantillaService;
	CoberturaDao coberturaDao;
	SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote;
	MotorDecisionAtributosService motorDecisionAtributosService;
	
	public static List<CatalogoValorFijoDTO> posiblesDeducibles= null;
	protected NegocioProductoDao negocioProductoDao;
	protected NegocioTipoPolizaService negocioTipoPolizaService;
	protected NegocioSeccionDao negocioSeccionDao;
	protected NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	protected NegocioDeduciblesService negocioDeduciblesService;
	protected FronterizosService fronterizosService;
	public final static String TIPO_VALOR_SA_DSMGVDF = "DSMGVDF";
	public final static String TIPO_VALOR_SA_UMA = "UMA";


	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion y
	 * el paquete.
	 * 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @return List<CoberturaCotizacionDTO>
	 */
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,Short idMoneda) {
		return this.getCoberturas(IdNegocio, idToProducto, idTipoPoliza, idToSeccion, idPaquete, null, null,
									null,idMoneda,null,null,null,null,null,null,false, null, false, null);	}

	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * paquete y cotizacion.
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idCotizacion
	 * @param idMoneda
	 * @return
	 */
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, BigDecimal idCotizacion,
			Short idMoneda) {
		return this.getCoberturas(IdNegocio, idToProducto, idTipoPoliza, idToSeccion, idPaquete, null, null,
				idCotizacion,idMoneda,null,null,null,null,null,null,false, null, false, null);
	}
	
	
	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * el paquet, estado, municipio, cotizacion, moneda, numero de secuencia
	 * (inciso), clave de estilo vehiculo, modelo vehiculo, tipo de uso
	 * vehiculo, modificadores de prima y modificadores de descripcion. 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCotizacion
	 * @param idMoneda
	 * @param numeroInciso
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUso
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @return
	 */
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion){
		
		return getCoberturas(IdNegocio,idToProducto,
				idTipoPoliza, idToSeccion, idPaquete,
				idEstado, idMunicipio, idCotizacion, idMoneda,numeroInciso, claveEstilo, modeloVehiculo,
				tipoUso, modificadoresPrima, modificadoresDescripcion,true, null, false, null);
		
	}
	
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion, String numeroSerie, Boolean vinValido){
		
		return getCoberturas(IdNegocio,idToProducto,
				idTipoPoliza, idToSeccion, idPaquete,
				idEstado, idMunicipio, idCotizacion, idMoneda,numeroInciso, claveEstilo, modeloVehiculo,
				tipoUso, modificadoresPrima, modificadoresDescripcion,true, numeroSerie, vinValido, null);
		
	}
	
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion, String numeroSerie, Boolean vinValido, BigDecimal codigoAgente){
		
		return getCoberturas(IdNegocio,idToProducto,
				idTipoPoliza, idToSeccion, idPaquete,
				idEstado, idMunicipio, idCotizacion, idMoneda,numeroInciso, claveEstilo, modeloVehiculo,
				tipoUso, modificadoresPrima, modificadoresDescripcion,true, numeroSerie, vinValido, codigoAgente);
		
	}
	
	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * el paquet, estado, municipio, cotizacion, moneda, numero de secuencia
	 * (inciso), clave de estilo vehiculo, modelo vehiculo, tipo de uso
	 * vehiculo, modificadores de prima y modificadores de descripcion. 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCotizacion
	 * @param idMoneda
	 * @param numeroInciso
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUso
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @return
	 */
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion,boolean aplicaCalculo, String numeroSerie, Boolean vinValido){		
		boolean tienePlantilla = false;
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = 
			negocioPaqueteSeccionDao.findByNegocioProductoTipoPolizaSeccionPaquete(IdNegocio, idToProducto, idTipoPoliza, idToSeccion, idPaquete);
		ConfiguracionPlantillaNeg plantilla =  configuracionPlantillaService.obtenerPlantilla(idCotizacion, 
				negocioPaqueteSeccion.getNegocioSeccion().getIdToNegSeccion(), 
				new BigDecimal(negocioPaqueteSeccion.getIdToNegPaqueteSeccion()));
		Long plantillaId = null;
		if(plantilla != null && plantilla.getPlantillaId() != null){
			tienePlantilla = true;
			plantillaId = plantilla.getPlantillaId();
		}
		return getCoberturas(IdNegocio,idToProducto, idTipoPoliza,  idToSeccion,  idPaquete, idEstado,  idMunicipio,  
				idCotizacion,  idMoneda,  numeroInciso,  claveEstilo,  modeloVehiculo, tipoUso,  modificadoresPrima,  
				modificadoresDescripcion, tienePlantilla, aplicaCalculo, plantillaId, numeroSerie, vinValido, null);
	}
	
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,
			String idEstado, String idMunicipio, BigDecimal idCotizacion, Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,
			BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion,boolean aplicaCalculo, String numeroSerie, Boolean vinValido, BigDecimal codigoAgente){		
		boolean tienePlantilla = false;
		
		NegocioPaqueteSeccion negocioPaqueteSeccion = 
			negocioPaqueteSeccionDao.findByNegocioProductoTipoPolizaSeccionPaquete(IdNegocio, idToProducto, idTipoPoliza, idToSeccion, idPaquete);
		ConfiguracionPlantillaNeg plantilla =  configuracionPlantillaService.obtenerPlantilla(idCotizacion, 
				negocioPaqueteSeccion.getNegocioSeccion().getIdToNegSeccion(), 
				new BigDecimal(negocioPaqueteSeccion.getIdToNegPaqueteSeccion()));
		Long plantillaId = null;
		if(plantilla != null && plantilla.getPlantillaId() != null){
			tienePlantilla = true;
			plantillaId = plantilla.getPlantillaId();
		}
		return getCoberturas(IdNegocio,idToProducto, idTipoPoliza,  idToSeccion,  idPaquete, idEstado,  idMunicipio,  
				idCotizacion,  idMoneda,  numeroInciso,  claveEstilo,  modeloVehiculo, tipoUso,  modificadoresPrima,  
				modificadoresDescripcion, tienePlantilla, aplicaCalculo, plantillaId, numeroSerie, vinValido, codigoAgente);
	}
	
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long IdNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, String idEstado,
			String idMunicipio, BigDecimal idCotizacion, Short idMoneda,
			Long numeroSecuenciaInciso, String claveEstilo,
			Long modeloVehiculo, BigDecimal tipoUso, String modificadoresPrima,
			String modificadoresDescripcion, boolean tienePlantilla, boolean aplicaCalculo,  Long plantillaId, String numeroSerie, Boolean vinValido){
		
		return getCoberturas(IdNegocio,idToProducto, idTipoPoliza,  idToSeccion,  idPaquete, idEstado,  idMunicipio,  
				idCotizacion,  idMoneda,  numeroSecuenciaInciso,  claveEstilo,  modeloVehiculo, tipoUso,  modificadoresPrima,  
				modificadoresDescripcion, tienePlantilla, aplicaCalculo, plantillaId, numeroSerie, vinValido, null);
	}
	
	/**
	 * Metodo que obtiene el grupo de coberturas que le corresponden a un inciso
	 * de acuerdo al negocio, el tipo de poliza, la linea de negocio o seccion,
	 * el paquet, estado, municipio, cotizacion, moneda, numero de secuencia (inciso),
	 * clave de estilo vehiculo, modelo vehiculo, tipo de uso vehiculo, modificadores de prima y
	 * modificadores de descripcion.
	 * 
	 * @param IdNegocio
	 * @param idToProducto
	 * @param idTipoPoliza
	 * @param idToSeccion
	 * @param idPaquete
	 * @param idEstado
	 * @param idMunicipio
	 * @param idCotizacion
	 * @param idMoneda
	 * @param numeroSecuenciaInciso
	 * @param claveEstilo
	 * @param modeloVehiculo
	 * @param tipoUso
	 * @param modificadoresPrima
	 * @param modificadoresDescripcion
	 * @return List<CoberturaCotizacionDTO>
	 */
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(Long idNegocio,BigDecimal idToProducto,
			BigDecimal idTipoPoliza, BigDecimal idToSeccion, Long idPaquete,String idEstado, String idMunicipio, BigDecimal idCotizacion, 
			Short idMoneda, Long numeroInciso, String claveEstilo, Long modeloVehiculo,BigDecimal tipoUso, String modificadoresPrima, String modificadoresDescripcion,
			boolean tienePlantilla, boolean aplicaCalculo, Long plantillaId, String numeroSerie, Boolean vinValido, BigDecimal codigoAgente) {
		boolean renovacion = false;
		if(idCotizacion != null){
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
			if(cotizacion != null
					&& cotizacion.getSolicitudDTO() != null){
				codigoAgente = (cotizacion.getSolicitudDTO().getCodigoAgente() != null)? cotizacion.getSolicitudDTO().getCodigoAgente() : codigoAgente;
				renovacion = (cotizacion.getSolicitudDTO().getEsRenovacion() == null 
						|| cotizacion.getSolicitudDTO().getEsRenovacion() == 0)? false : true;
			}
		}
		List<NegocioCobPaqSeccion> coberturasNegocio = 
			getCoberturaNegocioConfig(idNegocio, idToProducto,idTipoPoliza, idToSeccion, idPaquete, idEstado, idMunicipio,idMoneda, 
					tipoUso, codigoAgente, renovacion );		
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>(1);
		this.getPosiblesDeducibles();
		CoberturaCotizacionDTO cobertura= null;
		CoberturaSeccionDTO coberturaSeccion = null;
		CoberturaSeccionDTOId idCob= null;
		CobPaquetesSeccionId id = null;
		for(NegocioCobPaqSeccion coberturaNegocio: coberturasNegocio){
			id = new CobPaquetesSeccionId(idToSeccion.longValue(), idPaquete, coberturaNegocio.getCoberturaDTO().getIdToCobertura().longValue());
			CobPaquetesSeccion configuracion = this.getCobPaqueteSeccionConfig(id);
			
			idCob = new CoberturaSeccionDTOId(idToSeccion, coberturaNegocio.getCoberturaDTO().getIdToCobertura());
			coberturaSeccion = entidadService.findById(CoberturaSeccionDTO.class, idCob);
			cobertura = new CoberturaCotizacionDTO();
			CoberturaCotizacionId coberturaCotizacionId = new CoberturaCotizacionId(
					idCotizacion, BigDecimal.valueOf(numeroInciso), idToSeccion, BigDecimal.valueOf(id.getIdToCobertura()));
			cobertura.setId(coberturaCotizacionId);
			cobertura.setCoberturaSeccionDTO(coberturaSeccion);
			cobertura.setConsultaAsegurada(false);
			cobertura.setClaveContrato(getClaveContrato(configuracion.getClaveObligatoriedad()));
			
			cobertura.setClaveObligatoriedad(configuracion.getClaveObligatoriedad());
			cobertura.setClaveTipoDeducible(coberturaNegocio.getClaveTipoDeduciblePP());
			cobertura.setClaveTipoLimiteDeducible(Short.valueOf(coberturaNegocio.getCoberturaDTO().getClaveTipoLimiteDeducible()));
			cobertura.setIdTcSubramo(coberturaNegocio.getCoberturaDTO().getSubRamoDTO().getIdTcSubRamo());
									
			CoaseguroCoberturaDTO coaseguro = getCoaseguroConfi(coberturaNegocio);			
			cobertura.setValorCoaseguro(coaseguro.getValor());			
			cobertura.setDeducibles(getDedicibleNegocio(coberturaNegocio.getIdToNegCobPaqSeccion()));
			NegocioDeducibleCob deduclible = getDeducibleConfig(cobertura.getDeducibles());
			cobertura.setValorDeducible(deduclible.getValorDeducible());	 	
			cobertura.setPorcentajeDeducible(deduclible.getValorDeducible());			
			cobertura.setDescripcionDeducible(getDescripcionDeducible(coberturaNegocio.getCoberturaDTO().getClaveTipoDeducible()));
			
			cobertura.setValorMinimoLimiteDeducible(coberturaNegocio.getCoberturaDTO().getValorMinimoDeducible());
			cobertura.setValorMaximoLimiteDeducible(coberturaNegocio.getCoberturaDTO().getValorMaximoDeducible());
			
			Double valorSumaAsegurada = null;
			String nombreComercial = cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial().toUpperCase();
			valorSumaAsegurada = getValorSumaAseguradaConf(cobertura, coberturaNegocio, claveEstilo, modeloVehiculo, idMoneda,
					coberturaSeccion, idCotizacion, numeroInciso, numeroSerie, idEstado, vinValido, nombreComercial);
			
			if(nombreComercial.equals(CoberturaDTO.NOMBRE_LIMITE_RC_VIAJERO)){
				cobertura.setDiasSalarioMinimo(this.obtenerValorNegCobPaqSeccionDefault(coberturaNegocio).intValue());
			}
			
			if (valorSumaAsegurada == null && !(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CARATULA.equals(
					cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada()))) {
				cobertura.setConsultaAsegurada(true);
				valorSumaAsegurada = this.obtenerValorNegCobPaqSeccionDefault(coberturaNegocio);
			}
			cobertura.setValorSumaAsegurada(valorSumaAsegurada);
			cobertura.setValorSumaAseguradaMax(coberturaNegocio.getValorSumaAseguradaMax());
			cobertura.setValorSumaAseguradaMin(coberturaNegocio.getValorSumaAseguradaMin());
			cobertura.setSumasAseguradas(coberturaNegocio.getNegocioCobSumAseList());
			cobertura.setClaveTipoDeducible(Short.valueOf(coberturaNegocio.getCoberturaDTO().getClaveTipoDeducible()));
			cobertura.setValorPrimaNeta(0D);		
			
			cobertura.setClaveAutCoaseguro((short)0);
			cobertura.setClaveAutDeducible((short)0);
			cobertura.setNumeroAgrupacion((short)0);
			if(tienePlantilla){
				ConfiguracionPlantillaNegCobertura configuracionPlantilla = configuracionPlantillaService.obtenerCobertura(plantillaId, cobertura.getId().getIdToCobertura());			
				this.obtenerCoberturaDePlantilla(cobertura,configuracionPlantilla);
			}
			
			cobertura.setDescripcionDeducible((deduclible.getId()==null)?"N/A":cobertura.getDescripcionDeducible());
			
			if (cobertura.getDescripcionDeducible() != null && cobertura.getDescripcionDeducible().indexOf(CoberturaServiceImpl.TIPO_VALOR_SA_DSMGVDF)>=0){
				cobertura.setDescripcionDeducible(cobertura.getDescripcionDeducible().replace(CoberturaServiceImpl.TIPO_VALOR_SA_DSMGVDF, CoberturaServiceImpl.TIPO_VALOR_SA_UMA));
			}
			
			coberturas.add(cobertura);
		}
		
		Collections.sort(coberturas, new Comparator<CoberturaCotizacionDTO>() {
			@Override
			public int compare(CoberturaCotizacionDTO n1,
					CoberturaCotizacionDTO n2) {
				return n1
						.getCoberturaSeccionDTO()
						.getCoberturaDTO()
						.getNumeroSecuencia()
						.compareTo(
								n2.getCoberturaSeccionDTO().getCoberturaDTO()
										.getNumeroSecuencia());
			}
		});		
	
		return coberturas;
	}
	
	private Double obtenerValorNegCobPaqSeccionDefault(NegocioCobPaqSeccion configuracion){
		Double valorDefault = null;
		if(configuracion.getNegocioCobSumAseList()!= null
				&& !configuracion.getNegocioCobSumAseList().isEmpty()){
			boolean tieneDefault = false;
			for(NegocioCobSumAse sumaDeListaConfigurada : configuracion.getNegocioCobSumAseList()){
				if(sumaDeListaConfigurada.getDefaultValor() == 1){
					valorDefault = sumaDeListaConfigurada.getValorSumaAsegurada();
					tieneDefault = true;
					break;
				}
			}
			if(!tieneDefault){
				valorDefault = configuracion.getNegocioCobSumAseList().get(0).getValorSumaAsegurada();
			}
		}else{
			valorDefault = configuracion.getValorSumaAseguradaDefault();
		}
		return valorDefault;
	}

	private void obtenerCoberturaDePlantilla(CoberturaCotizacionDTO cobertura,
			ConfiguracionPlantillaNegCobertura configuracion) {
		// TODO: Para endosos, se debe buscar la poliza que se emitio y obtener la
		// cotizacion correspondiente, y ver si existe una plantilla para la
		// cotizacion original
		if (configuracion != null) {
			cobertura.setClaveContrato(configuracion.getContratada() ? (short) 1: (short) 0);
			cobertura.setValorSumaAsegurada(configuracion.getSumaAsegurada());
			if(cobertura.getClaveTipoDeducible() != null && 
					cobertura.getClaveTipoDeducible().intValue() == CoberturaDTO.CLAVE_TIPO_DEDUCIBLE_PCTE){
				cobertura.setPorcentajeDeducible(configuracion.getDeducible());
			}else{
				cobertura.setValorDeducible(configuracion.getDeducible());
			}
		}
	}	

	@Override
	public List<CoberturaCotizacionDTO> getCoberturasContratadas(IncisoCotizacionDTO inciso) {		
		return this.getCoberturas(inciso, true);
	}	

	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(IncisoCotizacionDTO incisoCotizacion) {
		IncisoCotizacionDTO incisoPersistido = entidadService.findById(IncisoCotizacionDTO.class, incisoCotizacion.getId());
		if(incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId() == null){
			return new ArrayList<CoberturaCotizacionDTO>(1);
		}
		if (incisoPersistido != null
				&& (!(incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId().intValue() == incisoPersistido
						.getIncisoAutoCot().getNegocioPaqueteId().intValue()) || 
						 !incisoCotizacion.getIncisoAutoCot().getEstadoId().equals(incisoPersistido.getIncisoAutoCot().getEstadoId())
						 || !incisoCotizacion.getIncisoAutoCot().getMunicipioId().equals(incisoPersistido.getIncisoAutoCot().getMunicipioId())
						 || !incisoCotizacion.getIncisoAutoCot().getEstiloId().equals(incisoPersistido.getIncisoAutoCot().getEstiloId()))
						 || !incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().equals(incisoPersistido.getIncisoAutoCot().getModeloVehiculo())
					     || !incisoCotizacion.getIncisoAutoCot().getTipoUsoId().equals(incisoPersistido.getIncisoAutoCot().getTipoUsoId())) {
			
			NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,
					incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
			
			return this.getCoberturas(negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
					negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(), 
					negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
					negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
					negocioPaqueteSeccion.getPaquete().getId(), incisoCotizacion.getIncisoAutoCot().getEstadoId(), incisoCotizacion.getIncisoAutoCot().getMunicipioId(), incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getCotizacionDTO().getIdMoneda().shortValue(), incisoCotizacion.getNumeroSecuencia().longValue(),
					incisoCotizacion.getIncisoAutoCot().getEstiloId(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), new BigDecimal(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()), null, null);
		
		} else {
			return this.getCoberturas(incisoCotizacion, false);
		}
	}	

	@Override 
	public List<CoberturaCotizacionDTO> getCoberturas(Long negocioPaqueteId, IncisoCotizacionId id) {
		IncisoCotizacionDTO incisoCotizacion = entidadService.findById(IncisoCotizacionDTO.class, id);
		if(negocioPaqueteId == null){
			return new ArrayList<CoberturaCotizacionDTO>(1);
		}
		
		if (incisoCotizacion != null
				&& !(negocioPaqueteId.intValue() == incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId().intValue())) {

			incisoCotizacion.getIncisoAutoCot().setNegocioPaqueteId(negocioPaqueteId);
			NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class,
					incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId());
			
			return this.getCoberturas(negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
					negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(), 
					negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
					negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
					negocioPaqueteSeccion.getPaquete().getId(), incisoCotizacion.getIncisoAutoCot().getEstadoId(), incisoCotizacion.getIncisoAutoCot().getMunicipioId(), 
					incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getCotizacionDTO().getIdMoneda().shortValue(), incisoCotizacion.getNumeroSecuencia().longValue(),
					incisoCotizacion.getIncisoAutoCot().getEstiloId() , incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), new BigDecimal(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()), null, null,
					true,incisoCotizacion.getIncisoAutoCot().getNumeroSerie(),incisoCotizacion.getIncisoAutoCot().getVinValido(), null);
		
		} else {
			return this.getCoberturas(incisoCotizacion, false);
		}
	}
	
	public List<CoberturaCotizacionDTO> getCoberturas(IncisoCotizacionDTO inciso, boolean soloContratadas){		
		List<SeccionCotizacionDTO> secciones = seccionCotizacionFacadeRemote
				.listarPorIncisoCotizacionId(inciso.getId());
		inciso.setSeccionCotizacionList(secciones);	
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>(1);
		NegocioCobPaqSeccion negocioCobPaqSeccion = null;
		IncisoCotizacionDTO incisoCotizacionTemp = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId());
		getPosiblesDeducibles();
		
		for(SeccionCotizacionDTO seccion: inciso.getSeccionCotizacionList()){
			coberturas =coberturaDao.listarCoberturasSeccionCotizacion(seccion.getId(), soloContratadas);
			seccion.setCoberturaCotizacionLista(coberturas);
			for(CoberturaCotizacionDTO cobertura: coberturas){

				try{
					negocioCobPaqSeccion =  
						this.getLimitesSumaAseguradaPorCobertura(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getIdToCobertura(), 
								incisoCotizacionTemp.getIncisoAutoCot().getNegocioPaqueteId(),
								incisoCotizacionTemp.getIncisoAutoCot().getEstadoId(), 
								incisoCotizacionTemp.getIncisoAutoCot().getMunicipioId(), 
								incisoCotizacionTemp.getCotizacionDTO().getIdMoneda(), 
								new BigDecimal(incisoCotizacionTemp.getIncisoAutoCot().getTipoUsoId()), 
								incisoCotizacionTemp.getCotizacionDTO().getSolicitudDTO().getCodigoAgente(), false);
					
					cobertura.setDeducibles(getDedicibleNegocio(negocioCobPaqSeccion.getIdToNegCobPaqSeccion()));
					
					cobertura.setDescripcionDeducible(getDescripcionDeducible(cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoDeducible()));

					NegocioDeducibleCob deduclible = getDeducibleConfig(cobertura.getDeducibles());
					cobertura.setDescripcionDeducible((deduclible.getId()==null)?"N/A":cobertura.getDescripcionDeducible());
					
					if (cobertura.getDescripcionDeducible() != null && cobertura.getDescripcionDeducible().indexOf(CoberturaServiceImpl.TIPO_VALOR_SA_DSMGVDF)>=0){
						cobertura.setDescripcionDeducible(cobertura.getDescripcionDeducible().replace(CoberturaServiceImpl.TIPO_VALOR_SA_DSMGVDF, CoberturaServiceImpl.TIPO_VALOR_SA_UMA));
					}
					
					if(negocioCobPaqSeccion.getValorSumaAseguradaMax() != null && negocioCobPaqSeccion.getValorSumaAseguradaMin() != null){
						cobertura.setValorSumaAseguradaMax(negocioCobPaqSeccion.getValorSumaAseguradaMax());
						cobertura.setValorSumaAseguradaMin(negocioCobPaqSeccion.getValorSumaAseguradaMin());
					}else{
						cobertura.setValorSumaAseguradaMax(Double.valueOf(UtileriasWeb.STRING_CERO));
						cobertura.setValorSumaAseguradaMin(Double.valueOf(UtileriasWeb.STRING_CERO));
					}
					
					cobertura.setSumasAseguradas(negocioCobPaqSeccion.getNegocioCobSumAseList());

				}catch(Exception ex){
					//no hay configuracion para los limites
					cobertura.setDeducibles(new ArrayList<NegocioDeducibleCob>(1));
					cobertura.setValorSumaAseguradaMax(Double.valueOf(UtileriasWeb.STRING_CERO));
					cobertura.setValorSumaAseguradaMin(Double.valueOf(UtileriasWeb.STRING_CERO));
				}
			}
		}		
		return coberturas;		
	}
	
	@Override
	public List<CoberturaCotizacionDTO> getCoberturasPrimaNetaCero(BigDecimal idToCotizacion, boolean soloContratadas){
		
		List<CoberturaCotizacionDTO> coberturas = new ArrayList<CoberturaCotizacionDTO>(1);
		coberturas =coberturaDao.listarCoberturasSeccionCotizacionPrimaCero(idToCotizacion, soloContratadas);
		return coberturas;
	}
	
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(CotizacionExpress cotizacionExpress,IncisoCotizacionDTO inciso) {
		NegocioPaqueteSeccion negocioPaqueteSeccion = cotizacionExpress.getNegocioPaqueteSeccion();
		
		List<CoberturaCotizacionDTO> coberturas = this.getCoberturas(negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
				negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(), 
				negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
				negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
				negocioPaqueteSeccion.getPaquete().getId(), cotizacionExpress.getEstado().getStateId(), 
				cotizacionExpress.getMunicipio().getMunicipalityId(), inciso.getId().getIdToCotizacion(), 
				cotizacionExpress.getIdMoneda(), inciso.getNumeroSecuencia().longValue(),
				inciso.getIncisoAutoCot().getEstiloId(), inciso.getIncisoAutoCot().getModeloVehiculo().longValue(), 
				new BigDecimal(inciso.getIncisoAutoCot().getTipoUsoId()), null, null);
		

		for (CoberturaCotizacionExpress express : cotizacionExpress
				.getCoberturaCotizacionExpresses()) {
			for (CoberturaCotizacionDTO cobertura : coberturas) {
				if (express.getCoberturaPrimaCotExpress().getCobertura()
						.getIdToCobertura().intValue() == cobertura.getId()
						.getIdToCobertura().intValue()) {
					
						cobertura.setClaveContrato((short) 1);
						cobertura.setValorPrimaNeta(express
								.getCoberturaPrimaCotExpress().getPrimaNetaB()
								.doubleValue());
	
				}
				if (cobertura.getValorPrimaNeta().intValue()<=0){
					cobertura.setClaveContrato((short) 0);
				}
			}

		}
		
		return coberturas;
	}	
	
	@Override
	public String getCoberturaSumaAseguradaStr(CoberturaDTO cobertura, BigDecimal sumaAsegurada) {
		String sumaAseguradaStr = getSumaAseguradaLeyenda(cobertura);
		if (sumaAseguradaStr == null) {
			sumaAseguradaStr = sumaAsegurada.toPlainString();
		}
		return sumaAseguradaStr;
	}
	
	/**
	 * Retorna una leyenda para la suma asegurada siempre y cuando aplique. En caso de no tener leyenda se regresa <code>null</code>.
	 * @return la leyenda o <code>null</code> en caso de no tener leyenda.
	 */
	private String getSumaAseguradaLeyenda(CoberturaDTO cobertura) {
		String sumaAseguradaLeyenda = null;
		if (CoberturaDTO.CLAVE_FUENTE_SA_VALOR_COMERCIAL.equals(cobertura.getClaveFuenteSumaAsegurada())) {
			sumaAseguradaLeyenda = CoberturaDTO.DESCRIPCION_TIPO_SA_VALOR_COMERCIAL;
		} else if (CoberturaDTO.CLAVE_FUENTE_SA_AMPARADA.equals(cobertura.getClaveFuenteSumaAsegurada())) {
			sumaAseguradaLeyenda = CoberturaDTO.DESCRIPCION_TIPO_SUMA_ASEGURADA_AMPARADA;
		}
		return sumaAseguradaLeyenda;
	}

	private CobPaquetesSeccion getCobPaqueteSeccionConfig(CobPaquetesSeccionId id) {
		CobPaquetesSeccion configuracion = entidadService.findById(CobPaquetesSeccion.class, id);
		if(configuracion == null){
			//se elimino la configuracion del producto pero sigue existiendo en el negocio
			configuracion = new CobPaquetesSeccion();
			configuracion.setClaveObligatoriedad(CoberturaDTO.OPCIONAL);
		}
		return configuracion;
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<NegocioCobPaqSeccion> getCoberturaNegocioConfig(Long idNegocio, BigDecimal idToProducto, BigDecimal idTipoPoliza, 
			BigDecimal idToSeccion, Long idPaquete, String idEstado, String idMunicipio, Short idMoneda, BigDecimal idTipoUso, BigDecimal idAgente, boolean renovacion) {
		MotorDecisionDTO filtroCoberturas = new MotorDecisionDTO();;
		
		filtroCoberturas.setTipoConsulta(MotorDecisionDTO.TipoConsulta.TIPO_CONSULTA_SUMASASEGURADAS);
		
		List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosMotor = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
		
		if(idEstado != null && idEstado.trim().equals("")){
			idEstado = null;
		}
		
		if(idMunicipio!= null && idMunicipio.trim().equals("")){
			idMunicipio = null;
		}
		
		//filtros requeridos para la busqueda
		NegocioPaqueteSeccion negocioPaqueteSeccion = 
            negocioPaqueteSeccionDao.findByNegocioProductoTipoPolizaSeccionPaquete(idNegocio, idToProducto, idTipoPoliza, idToSeccion, idPaquete);
      
		//filtros requeridos para la busqueda
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.TRUE,"negocioPaqueteSeccion",negocioPaqueteSeccion));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.TRUE,"monedaDTO.idTcMoneda",idMoneda));
		
		EstadoDTO estado = null;
		CiudadDTO municipio = null;
		TipoUsoVehiculoDTO tipoUsoVehiculo = null;
		Agente agente = null;
		
		if(idEstado != null){
			estado = entidadService.findById(EstadoDTO.class, idEstado);
		}
		if(idMunicipio != null){
			municipio = entidadService.findById(CiudadDTO.class, idMunicipio);
		}
		if(idTipoUso != null){
			tipoUsoVehiculo = entidadService.findById(TipoUsoVehiculoDTO.class,idTipoUso);
		}
		if(idAgente != null){
			List<Agente> agenteList = entidadService.findByProperty(Agente.class, "id", idAgente);
			agente = agenteList.get(0);
		}
		
		//filtros opcionales
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"estadoDTO",estado));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"ciudadDTO",municipio));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"tipoUsoVehiculo",tipoUsoVehiculo));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"agente",agente));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"renovacion",renovacion));
		
		
		filtroCoberturas.setFiltrosDTO(filtrosMotor);
		
		List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = motorDecisionAtributosService.consultaValoresService(filtroCoberturas);
		
		return (List<NegocioCobPaqSeccion>)(Object)resultadoBusqueda.get(0).getValores();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public NegocioCobPaqSeccion getLimitesSumaAseguradaPorCobertura(
			BigDecimal idToCobertura, Long negocioPaqueteId, String idEstado, String idMunicipio, BigDecimal idMoneda,
			BigDecimal idTipoUso, BigDecimal idAgente, boolean renovacion) {

		MotorDecisionDTO filtroCoberturas = new MotorDecisionDTO();;
		
		filtroCoberturas.setTipoConsulta(MotorDecisionDTO.TipoConsulta.TIPO_CONSULTA_SUMASASEGURADAS);
		
		List<MotorDecisionDTO.FiltrosMotorDecisionDTO> filtrosMotor = new ArrayList<MotorDecisionDTO.FiltrosMotorDecisionDTO>();
		
		if(idEstado != null && idEstado.trim().equals("")){
			idEstado = null;
		}
		
		if(idMunicipio!= null && idMunicipio.trim().equals("")){
			idMunicipio = null;
		}
		
		//filtros requeridos para la busqueda
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.TRUE,"negocioPaqueteSeccion.idToNegPaqueteSeccion",negocioPaqueteId));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.TRUE,"coberturaDTO.idToCobertura",idToCobertura));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.TRUE,"monedaDTO.idTcMoneda",idMoneda));
		
		EstadoDTO estado = null;
		CiudadDTO municipio = null;
		TipoUsoVehiculoDTO tipoUsoVehiculo = null;
		Agente agente = null;
		
		if(idEstado != null){
			estado = entidadService.findById(EstadoDTO.class, idEstado);
		}
		if(idMunicipio != null){
			municipio = entidadService.findById(CiudadDTO.class, idMunicipio);
		}
		if(idTipoUso != null){
			tipoUsoVehiculo = entidadService.findById(TipoUsoVehiculoDTO.class,idTipoUso);
		}
		if(idAgente != null){
			List<Agente> agenteList = entidadService.findByProperty(Agente.class, "id", idAgente);
			agente = agenteList.get(0);
		}
		
		//filtros opcionales
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"estadoDTO",estado));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"ciudadDTO",municipio));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"tipoUsoVehiculo",tipoUsoVehiculo));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"agente",agente));
		filtrosMotor.add(filtroCoberturas.new FiltrosMotorDecisionDTO(
				Boolean.FALSE,"renovacion",renovacion));	
		
		filtroCoberturas.setFiltrosDTO(filtrosMotor);
		
		List<ResultadoMotorDecisionAtributosDTO> resultadoBusqueda = motorDecisionAtributosService.consultaValoresService(filtroCoberturas);
		
		List<NegocioCobPaqSeccion> resultado = (List<NegocioCobPaqSeccion>)(Object)resultadoBusqueda.get(0).getValores();
		
		if(resultado != null 
				&& !resultado.isEmpty()){
			return resultado.get(0);
		}
		
		return null;
	}

	private Short getClaveContrato(Short claveObligatoriedad) {
		Short claveContrato = null;
		switch (claveObligatoriedad) {
		case CoberturaDTO.OPCIONAL:
			claveContrato = Short.valueOf("0");
			break;
		default:
			claveContrato = Short.valueOf("1");
			break;
		}
		return claveContrato;
	}

	private CoaseguroCoberturaDTO getCoaseguroConfi(NegocioCobPaqSeccion coberturaNegocio) {
		Predicate equalCoa = new Predicate() {				
			@Override
			public boolean evaluate(Object arg0) {
				CoaseguroCoberturaDTO dto = (CoaseguroCoberturaDTO)arg0;
				return dto.getClaveDefault() == (short)1;
			}
		};
					
		CoaseguroCoberturaDTO coaseguro = (CoaseguroCoberturaDTO)CollectionUtils.find(
				coberturaNegocio.getCoberturaDTO().getCoaseguros(), equalCoa);
		if(coaseguro==null){
			coaseguro = new CoaseguroCoberturaDTO();
			coaseguro.setValor(0.0d);
		}
		return coaseguro;
	}
	
	private void getPosiblesDeducibles() {		
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		
		if(posiblesDeducibles == null){
			params.put("id.idGrupoValores", CatalogoValorFijoDTO.IDGRUPO_POSIBLES_DEDUCIBLES);
			posiblesDeducibles = entidadService.findByProperties(CatalogoValorFijoDTO.class, params);
		}
	}

	private NegocioDeducibleCob getDeducibleConfig(List<NegocioDeducibleCob> deducibles) {
		
		Predicate equalDed = new Predicate() {				
			@Override
			public boolean evaluate(Object arg0) {
				NegocioDeducibleCob dto = (NegocioDeducibleCob)arg0;
				return dto.getClaveDefault() == (short)1;
			}
		};
		
		NegocioDeducibleCob deduclible = (NegocioDeducibleCob)CollectionUtils.find(deducibles, equalDed);
		if(deduclible == null){
			deduclible = new NegocioDeducibleCob();
			deduclible.setValorDeducible(0.0d);
		}
		
		return deduclible;
	}

	private List<NegocioDeducibleCob> getDedicibleNegocio(BigDecimal idToNegCobPaqSeccion) {
		
		List<NegocioDeducibleCob> deducibles = new ArrayList<NegocioDeducibleCob>(1);
		try{
			deducibles = negocioDeduciblesService.obtenerDeduciblesCobertura(idToNegCobPaqSeccion);
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		return deducibles;
	}

	private Double getValorSumaAseguradaConf(CoberturaCotizacionDTO cobertura, NegocioCobPaqSeccion coberturaNegocio, String claveEstilo, 
			Long modeloVehiculo, Short idMoneda, CoberturaSeccionDTO coberturaSeccion, BigDecimal idCotizacion, Long numeroInciso, 
			String numeroSerie, String idEstado, Boolean vinValido, String nombreComercial) {
		Double valorSumaAsegurada = null;
		String claveFuenteSumaAsegurada = cobertura.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada();
		
		if(CoberturaDTO.CLAVE_FUENTE_SA_VALOR_CARATULA.equals(claveFuenteSumaAsegurada)){
			valorSumaAsegurada = getSumaAseguradaValorCaratula(claveEstilo, idMoneda, modeloVehiculo, nombreComercial);
		}else{
			if(CoberturaDTO.NOMBRE_LIMITE_RC_VIAJERO.equals(nombreComercial)){
				valorSumaAsegurada = 0d;
			} else if ((CoberturaDTO.NOMBRE_DM.equals(nombreComercial) || CoberturaDTO.NOMBRE_RT.equals(nombreComercial) ||
					CoberturaDTO.NOMBRE_DM_CONVENIDO.equals(nombreComercial) ||	CoberturaDTO.NOMBRE_RT_CONVENIDO.equals(nombreComercial)) &&
					CoberturaDTO.CLAVE_FUENTE_SA_VALOR_PROPORCIONADO.equals(claveFuenteSumaAsegurada) &&
							vinValido && (numeroSerie != null && !numeroSerie.isEmpty())) {
				
					valorSumaAsegurada = fronterizosService.obtieneValorSumaAseguradaCot(coberturaNegocio, coberturaSeccion, idCotizacion, 
							numeroInciso, numeroSerie, idEstado);
			} else {
				valorSumaAsegurada = this.obtenerValorNegCobPaqSeccionDefault(coberturaNegocio);
			}
		}
		return valorSumaAsegurada;		
	}

	private Double getSumaAseguradaValorCaratula(String claveEstilo,
			Short idMoneda, Long modeloVehiculo, String nombreComercial) {
		Double valorSumaAsegurada = null;
		EstiloVehiculoId idEstilo = new EstiloVehiculoId();
		ModeloVehiculoDTO modelo = null;
		idEstilo.valueOf(claveEstilo);
		if(!StringUtils.isEmpty(idEstilo.getClaveEstilo())){
			ModeloVehiculoId modeloVehiculoId = new ModeloVehiculoId(idEstilo.getClaveTipoBien(), idEstilo.getClaveEstilo(),
					modeloVehiculo.shortValue(), BigDecimal.valueOf(idMoneda.longValue()), idEstilo.getIdVersionCarga());
			 modelo = entidadService.findById(ModeloVehiculoDTO.class, modeloVehiculoId);
		}
		if(modelo!=null && modelo.getValorCaratula()!=null){
			valorSumaAsegurada = modelo.getValorCaratula().doubleValue();
		}else{
			throw new RuntimeException("El modelo asignado a la cotizacion no cuenta con un Valor Carátula configurado para la cobertura "+nombreComercial);
		}
		return valorSumaAsegurada;
	}

	private String getDescripcionDeducible(String tipoDeducible){
		String descripcion = "";
		int valorTipoDeducible;
		if(tipoDeducible != null){
			valorTipoDeducible = Integer.valueOf(tipoDeducible);

			for(CatalogoValorFijoDTO valor: posiblesDeducibles){
				if(valor.getId().getIdDato() == valorTipoDeducible){
					descripcion = valor.getDescripcion();					
					break;
				}
			}
		}
		return descripcion;
	}
	
	public List<CoberturaCotizacionDTO> getCoberturas(BigDecimal idToCotizacion, boolean soloContratadas){
		return coberturaDao.listarCoberturasCotizacion(idToCotizacion, soloContratadas);
	}
	
	@Override
	public List<CoberturaCotizacionDTO> getCoberturasDistinct(
			BigDecimal idToCotizacion, boolean soloContratadas) {
		return coberturaDao.listarCoberturasCotizacionDistinct(idToCotizacion, soloContratadas);
	}
	
	@Override
	public List<CoberturaCotizacionDTO> getCoberturas(CotizacionCoberturaView cot){
		return getCoberturas(cot.getIdNegocio(),
				cot.getIdToProducto(), cot.getIdToTipoPoliza(),
				cot.getIdToSeccion(), cot.getIdPaquete(),
				cot.getIdEstadoCirculacion(), cot.getIdMunicipioCirculacion(), null,
				cot.getIdMoneda(), 1L,
				cot.getEstilo(), cot.getModelo(), null, null,
				null);
	}
		
	@EJB
	public void setNegocioCobPaqSeccionDAO(
			NegocioCobPaqSeccionDAO negocioCobPaqSeccionDAO) {
		this.negocioCobPaqSeccionDAO = negocioCobPaqSeccionDAO;
	}
	
	@EJB
	public void setCobPaquetesSeccionDao(CobPaquetesSeccionDao cobPaquetesSeccionDao) {
		this.cobPaquetesSeccionDao = cobPaquetesSeccionDao;
	}

	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}


	@EJB
	public void setSeccionCotizacionFacadeRemote(
			SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote) {
		this.seccionCotizacionFacadeRemote = seccionCotizacionFacadeRemote;
	}

	@EJB
	public void setConfiguracionPlantillaService(
			ConfiguracionPlantillaService configuracionPlantillaService) {
		this.configuracionPlantillaService = configuracionPlantillaService;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setCoberturaDao(CoberturaDao coberturaDao) {
		this.coberturaDao = coberturaDao;
	}	
	
	@EJB
	public void setNegocioProductoDao(NegocioProductoDao negocioProductoDao) {
		this.negocioProductoDao = negocioProductoDao;
	}
	
	@EJB
	public void setNegocioTipoPolizaService(NegocioTipoPolizaService negocioTipoPolizaService) {
		this.negocioTipoPolizaService = negocioTipoPolizaService;
	}
	
	@EJB
	public void setNegocioSeccionDao(NegocioSeccionDao negocioSeccionDao) {
		this.negocioSeccionDao = negocioSeccionDao;
	}
	
	@EJB
	public void setNegocioPaqueteSeccionDao(NegocioPaqueteSeccionDao negocioPaqueteSeccionDao) {
		this.negocioPaqueteSeccionDao = negocioPaqueteSeccionDao;
	}
	
	@EJB
	public void setNegocioDeduciblesService(NegocioDeduciblesService negocioDeduciblesService) {
		this.negocioDeduciblesService = negocioDeduciblesService;
	}

	@EJB
	public void setFronterizosService(FronterizosService fronterizosService) {
		this.fronterizosService = fronterizosService;
	}
	
	@EJB
	public void setMotorDecisionAtributosService(MotorDecisionAtributosService motorDecisionAtributosService){
		this.motorDecisionAtributosService = motorDecisionAtributosService;
	}
	
}