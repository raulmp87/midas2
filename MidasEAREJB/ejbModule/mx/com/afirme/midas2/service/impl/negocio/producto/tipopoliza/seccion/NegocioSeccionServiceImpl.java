package mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza.seccion;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.NegocioSeccionDao;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifa;
import mx.com.afirme.midas2.domain.tarifa.AgrupadorTarifaSeccion;
import mx.com.afirme.midas2.dto.negocio.seccion.RelacionesNegocioSeccionDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.NegocioProductoService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaSeccionService;
import mx.com.afirme.midas2.service.tarifa.AgrupadorTarifaService;

@Stateless
public class NegocioSeccionServiceImpl implements NegocioSeccionService {

	protected SeccionFacadeRemote seccionFacadeRemote;
	protected AgrupadorTarifaSeccionService agrupadorTarifaSeccionService;
	protected AgrupadorTarifaService agrupadorTarifaService;

	private EntidadService entidadService;
	protected NegocioSeccionService negocioSeccionService;
	protected EntidadDao entidadDao;
	protected NegocioSeccionDao negocioSeccionDao;
	protected NegocioProductoDao negocioProductoDao;
	protected NegocioProductoService negocioProductoService;
	protected NegocioTipoPolizaService negocioTipoPolizaService;

	@EJB	
	public void setNegocioTipoPolizaService(NegocioTipoPolizaService negocioTipoPolizaService) {
		this.negocioTipoPolizaService = negocioTipoPolizaService;
	}
	
	@EJB	
	public void setNegocioProductoService(NegocioProductoService negocioProductoService) {
		this.negocioProductoService = negocioProductoService;
	}
	@EJB	
	public void setNegocioProductoDao(NegocioProductoDao negocioProductoDao) {
		this.negocioProductoDao = negocioProductoDao;
	}	
	@EJB	
	public void setNegocioSeccionDao(NegocioSeccionDao negocioSeccionDao) {
		this.negocioSeccionDao = negocioSeccionDao;
	}
	@EJB
	public void setSeccionFacadeRemote(SeccionFacadeRemote seccionFacadeRemote) {
		this.seccionFacadeRemote = seccionFacadeRemote;
	}
	@EJB
	public void setAgrupadorTarifaSeccionService(
			AgrupadorTarifaSeccionService agrupadorTarifaSeccionService) {
		this.agrupadorTarifaSeccionService = agrupadorTarifaSeccionService;
	}
	@EJB
	public void setAgrupadorTarifaService(
			AgrupadorTarifaService agrupadorTarifaService) {
		this.agrupadorTarifaService = agrupadorTarifaService;
	}
	
	@EJB
	public void setCatalogoService(
			EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
	@EJB
	public void setNegocioSeccionService(
			NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}	
	@EJB
	public void setEntidadDao(
			EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}	
	
	/**
	 * Metodo que sirve para prepara un objeto NegocioSeccion apartir de un id
	 * seccion, busca el tipo de poliza que tiene y obtiene los N agrupadores de
	 * tarifa y los setea en el objeto para posteriormente ser persistido
	 * Nota: Este metodo no se tea el Negocio al Objeto
	 * 
	 * @param IdToSeccion
	 * @return NegocioSeccion
	 */	
	@Override
	public NegocioSeccion prepare(BigDecimal idToSeccion) {
        NegocioSeccion negocioSeccion = new NegocioSeccion();
        SeccionDTO seccionDTO = entidadService.findById(SeccionDTO.class, idToSeccion);
        negocioSeccion.setSeccionDTO(seccionDTO);
        List<NegocioAgrupadorTarifaSeccion> negAgrupTarifaSeccionList = getListaAgrupadorTarifa(negocioSeccion);
        
        negocioSeccion.setNegocioAgrupadorTarifaSeccions(negAgrupTarifaSeccionList);
        return negocioSeccion;

	}
	
	/**
	 * Metodo que sirve para pobla la lista NegocioAgrupadorTarifaSeccions NegocioSeccion,
	 * busca el tipo de poliza que tiene y obtiene los N agrupadores de
	 * tarifa y los setea en el objeto para posteriormente ser persistido
	 * 
	 * @param IdToSeccion
	 * @return NegocioSeccion
	 */
	public NegocioSeccion poblarAgrupTarifaSeccion(NegocioSeccion negocioSeccion) {
		if(negocioSeccion.getSeccionDTO().getIdToSeccion() == null){
			throw new IllegalArgumentException("La seccion no puede ser nula");
		}
		
		if(negocioSeccion.getSeccionDTO() == null){
			throw new IllegalArgumentException("La seccion no puede ser nula");
		}
		
		List<NegocioAgrupadorTarifaSeccion> negAgrupTarifaSeccionList = getListaAgrupadorTarifa(negocioSeccion);
        negocioSeccion.setNegocioAgrupadorTarifaSeccions(negAgrupTarifaSeccionList);
        
        return negocioSeccion;

	}
	
	private List<NegocioAgrupadorTarifaSeccion> getListaAgrupadorTarifa(NegocioSeccion negocioSeccion){
		String claveNegocio = null;
		if(negocioSeccion.getNegocioTipoPoliza() != null && negocioSeccion.getNegocioTipoPoliza().getNegocioProducto() != null
				&& negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio() != null) {
			claveNegocio = negocioSeccion.getNegocioTipoPoliza().getNegocioProducto().getNegocio().getClaveNegocio();
		}else{
			claveNegocio = "A";
		}
		List<AgrupadorTarifaSeccion> agrupadorTarifaSeccionList = agrupadorTarifaSeccionService.consultaAgrupadoresDefault(negocioSeccion.getSeccionDTO().getIdToSeccion(), claveNegocio);
        List<NegocioAgrupadorTarifaSeccion> negAgrupTarifaSeccionList = new ArrayList<NegocioAgrupadorTarifaSeccion>();
        
        for(AgrupadorTarifaSeccion item :  agrupadorTarifaSeccionList){
        	NegocioAgrupadorTarifaSeccion negAgrupTarifaSeccion = new NegocioAgrupadorTarifaSeccion();
            AgrupadorTarifa agrupadorTarifa =  agrupadorTarifaService.consultaVersionActiva(BigDecimal.valueOf(item.getId().getIdToAgrupadorTarifa()));
            if(agrupadorTarifa != null){
            	negAgrupTarifaSeccion.setNegocioSeccion(negocioSeccion);
                negAgrupTarifaSeccion.setIdToAgrupadorTarifa(agrupadorTarifa.getId().getIdToAgrupadorTarifa());
                negAgrupTarifaSeccion.setIdVerAgrupadorTarifa(agrupadorTarifa.getId().getIdVerAgrupadorTarifa());
                negAgrupTarifaSeccion.setIdMoneda(BigDecimal.valueOf(item.getId().getIdMoneda()));
                negAgrupTarifaSeccionList.add(negAgrupTarifaSeccion);
            }
        }
        
        return negAgrupTarifaSeccionList;
	}
	
	
	
	@Override
	public RelacionesNegocioSeccionDTO getRelationList(NegocioTipoPoliza negocioTipoPoliza){
		//Negocio Seccion Asociadas
		List<NegocioSeccion> negocioSeccionAsociadas = new ArrayList<NegocioSeccion>(1);

		negocioSeccionAsociadas = negocioSeccionDao.listarNegSeccionPorIdNegTipoPoliza(negocioTipoPoliza.getIdToNegTipoPoliza());

		//TipoPoliza posibles a traves del uso del EJB seccionFacade para obtener listado de SeccionDTO 
		List<SeccionDTO> posibles = new ArrayList<SeccionDTO>(1);
		if(seccionFacadeRemote.listarPorIdTipoPoliza(negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza(), false, true).size() > 0){
			posibles = seccionFacadeRemote.listarPorIdTipoPoliza(negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza(), false, true);
		}
		//NegociotTipoPoliza disponibles
		List<NegocioSeccion> negocioSeccionDisponibles = new ArrayList<NegocioSeccion>();
		//Objeto RelacionesNegocioTipoPolizaDTO
		RelacionesNegocioSeccionDTO relacionesNegocioSeccionDTO = new RelacionesNegocioSeccionDTO();
		//Listado usado para comparar las ya asociadas con las posibles
		List<SeccionDTO> seccionDTOAsociadas = new ArrayList<SeccionDTO>();
		//Llenado de listado seccionDTOAsociadas
		for(NegocioSeccion item: negocioSeccionAsociadas){
			seccionDTOAsociadas.add(item.getSeccionDTO());
		}
		
		//Llenado de listado negocioSeccionDisponible
		for(SeccionDTO item: posibles){
			if(!seccionDTOAsociadas.contains(item)){
				NegocioSeccion negocioSeccion = new NegocioSeccion();
				negocioSeccion.setNegocioTipoPoliza(negocioTipoPoliza);
				negocioSeccion.setSeccionDTO(item);
				negocioSeccion.setIdToNegSeccion(item.getIdToSeccion());
				
				negocioSeccionDisponibles.add(negocioSeccion);
			}
		}
		
		//Lleanado del objeto relacionesNegocioSeccionDTO
		relacionesNegocioSeccionDTO.setAsociadas(negocioSeccionAsociadas);
		relacionesNegocioSeccionDTO.setDisponibles(negocioSeccionDisponibles);
		
		return relacionesNegocioSeccionDTO;
	}
	@Override
	public void relacionarNegocioSeccion(String accion, NegocioSeccion negocioSeccion) {
		//Persiste negocio seccion
		if (accion.equals("deleted")) {
			negocioSeccion = entidadService.findById(NegocioSeccion.class, negocioSeccion.getIdToNegSeccion());
		}	else if (accion.equals("inserted")) {
			NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, negocioSeccion.getNegocioTipoPoliza().getIdToNegTipoPoliza());
			negocioSeccion = negocioSeccionService.prepare(negocioSeccion.getSeccionDTO().getIdToSeccion());
			negocioSeccion.setNegocioTipoPoliza(negocioTipoPoliza);
		}
		entidadDao.executeActionGrid(accion, negocioSeccion);
	}
	@Override
	public List<NegocioTipoPoliza> getNegTipoPolizaList(NegocioProducto negocioProducto) {
		//Llena combo de TipoPoliza
		List<NegocioTipoPoliza> negTipoPolizaList = new ArrayList<NegocioTipoPoliza>(1);
		
		negTipoPolizaList = negocioProductoDao.listarNegTipoPolizaPorIdToNegProducto(negocioProducto.getIdToNegProducto());
		
		return negTipoPolizaList;
	}
	
	@Override
	public List<NegocioSeccion> getSeccionListByCotizacion(CotizacionDTO cotizacion){
		List<NegocioSeccion> negocioSeccionList = new ArrayList<NegocioSeccion>(1);
		try{		
			SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class, cotizacion.getSolicitudDTO().getIdToSolicitud());
			negocioSeccionList = negocioSeccionDao.listarNegocioSeccionPorProductoNegocioTipoPoliza(solicitud.getProductoDTO(), solicitud.getNegocio(), cotizacion.getTipoPolizaDTO());			
		}catch(Exception e){
			
		}		
		return negocioSeccionList;
	}
	
	@Override
	public List<NegocioSeccion> getSeccionListByIdNegocioProductoTipoPoliza(BigDecimal idToProducto, Long idToNegocio, BigDecimal idToTipoPoliza){
		List<NegocioSeccion> negocioSeccionList = new ArrayList<NegocioSeccion>(1);
		try{		
			negocioSeccionList = negocioSeccionDao.listarNegocioSeccionPorIdProductoNegocioTipoPoliza(idToProducto, idToNegocio, idToTipoPoliza);			
		}catch(Exception e){
			
		}		
		return negocioSeccionList;
	}

	@Override
	public List<NegocioSeccion> getSeccionListByCotizacionInciso(
			CotizacionDTO cotizacion) {
		List<NegocioSeccion> negocioSeccionList = new ArrayList<NegocioSeccion>(1);
		
		try{
			if(cotizacion != null){
				negocioSeccionList = negocioSeccionDao.listarNegSeccionPorCotizacion(cotizacion.getIdToCotizacion());
			}else{
				negocioSeccionList = negocioSeccionDao.listarNegSeccionPorCotizacion(null);
			}
		}catch(RuntimeException e){
			throw e;
		}
		
		return negocioSeccionList;
	}
	
	@Override
	public List<NegocioSeccion> getSeccionListByExpress( BigDecimal idNegocioSeccion){
		List<NegocioSeccion> negocioSeccionList = new ArrayList<NegocioSeccion>(1);
		
		try{
			
		NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, idNegocioSeccion);		
		negocioSeccionList = negocioSeccionDao.listarNegSeccionPorIdNegTipoPoliza(negocioSeccion.getNegocioTipoPoliza().getIdToNegTipoPoliza());
		
		}catch(Exception e){
			
		}
		
		return negocioSeccionList;
	}
	
	@Override
	public NegocioSeccion getByProductoNegocioTipoPolizaSeccionDescripcion(
			ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza,
			String seccionDescripcion) {
		return negocioSeccionDao.getPorProductoNegocioTipoPolizaSeccionDescripcion(producto, negocio, tipoPoliza, seccionDescripcion);
	}
	
	@Override
	public List<SeccionDTO> listarSeccionNegocioByClaveNegocio(String claveNegocio){
		return negocioSeccionDao.listarSeccionNegocioByClaveNegocio(claveNegocio);
	}

	@Override
	public List<SeccionDTO> listarSeccionNegocioByIdNegocio(BigDecimal idToNegocio) {
		return negocioSeccionDao.listarSeccionNegocioByIdNegocio(idToNegocio);
	}
	
	@Override
	public NegocioSeccion getPorProductoNegocioTipoPolizaSeccionDescripcion(
			ProductoDTO producto, Negocio negocio, TipoPolizaDTO tipoPoliza, SeccionDTO seccion) {
		return negocioSeccionDao.getPorProductoNegocioTipoPolizaSeccionDescripcion(producto, negocio, tipoPoliza, seccion);
	}
	
	@Override
	public List<NegocioSeccion> listarNegSeccionByIdNegocio(Long idToNegocio) {
		return negocioSeccionDao.listarNegSeccionByIdNegocio(idToNegocio);
	}

	/* (non-Javadoc)
	 * @see mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService#getPorIdNegTipoPolizaIdToSeccion(java.math.BigDecimal, java.math.BigDecimal)
	 */
	@Override
	public NegocioSeccion getPorIdNegTipoPolizaIdToSeccion(
			BigDecimal idToNegTipoPoliza, BigDecimal idToSeccion) {
		return negocioSeccionDao.getPorIdNegTipoPolizaIdToSeccion(idToNegTipoPoliza, idToSeccion);
	}

	public List<NegocioSeccion> getNegSeccionesPorIdNegTipoPoliza(BigDecimal idToNegTipoPoliza){
		return negocioSeccionDao.listarNegSeccionPorIdNegTipoPoliza(idToNegTipoPoliza);
	}
}
