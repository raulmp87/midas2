package mx.com.afirme.midas2.service.impl.sistema.parametro;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobalId;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;

@Stateless
public class ParametroGlobalServiceImpl implements ParametroGlobalService{

	@EJB
	private EntidadService entidadService;
	
	@Override
	public List<ParametroGlobal> obtenerListadoPorAplicacion(int aplicacion) {
		try{
			return entidadService.findByProperty(ParametroGlobal.class, "id.aplicacionId", aplicacion);
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public ParametroGlobal obtenerPorIdParametro(int aplicacion, String id) {
		try{
			ParametroGlobalId parametroGlobalId = new ParametroGlobalId(aplicacion, id);
			
			ParametroGlobal instance =entidadService.findById(ParametroGlobal.class, parametroGlobalId);
			entidadService.refresh(instance);
			return instance;
		}catch(Exception e){
			return null;
		}
	}

	@Override
	public String obtenerValorPorIdParametro(int aplicacion, String id) {
		try{
			ParametroGlobal parametroGlobal = obtenerPorIdParametro(aplicacion, id);
			return parametroGlobal.getValor();
		}catch(Exception e){
			return null;
		}
	}
	
	@Override
	public String obtenerValorPorIdParametro(int aplicacion, String id, String valorDefault){
		String valor = obtenerValorPorIdParametro(aplicacion, id);
		return valor != null ? valor : valorDefault;
	}

	
	
	@Override
	public void guardarParametroGlobal(ParametroGlobal parametroGlobal) {
		this.entidadService.save(parametroGlobal);
	}
	

}
