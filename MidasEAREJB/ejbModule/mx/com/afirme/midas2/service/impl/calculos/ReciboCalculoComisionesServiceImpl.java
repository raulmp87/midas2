package mx.com.afirme.midas2.service.impl.calculos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.calculos.ReciboCalculoComisionesDao;
import mx.com.afirme.midas2.domain.calculos.DetalleCalculoComisiones;
import mx.com.afirme.midas2.domain.calculos.ReciboCalculoComisiones;
import mx.com.afirme.midas2.service.calculos.ReciboCalculoComisionesService;
import mx.com.afirme.midas2.util.MidasException;
@Stateless
public class ReciboCalculoComisionesServiceImpl implements ReciboCalculoComisionesService{
	private ReciboCalculoComisionesDao dao;
	@Override
	public void deleteRecibosPorDetalleCalculo(Long arg0) throws MidasException {
		dao.deleteRecibosPorDetalleCalculo(arg0);
	}

	@Override
	public void deleteRecibosPorDetalleCalculo(DetalleCalculoComisiones arg0)throws MidasException {
		dao.deleteRecibosPorDetalleCalculo(arg0);
	}

	@Override
	public List<ReciboCalculoComisiones> findByFilters(ReciboCalculoComisiones arg0) {
		return dao.findByFilters(arg0);
	}

	@Override
	public List<ReciboCalculoComisiones> listarRecibosDeDetalleCalculo(Long arg0)throws MidasException {
		return dao.listarRecibosDeDetalleCalculo(arg0);
	}

	@Override
	public List<ReciboCalculoComisiones> listarRecibosDeDetalleCalculo(DetalleCalculoComisiones arg0) throws MidasException {
		return dao.listarRecibosDeDetalleCalculo(arg0);
	}

	@Override
	public ReciboCalculoComisiones loadById(Long arg0) throws MidasException {
		return dao.loadById(arg0);
	}

	@Override
	public ReciboCalculoComisiones loadById(ReciboCalculoComisiones arg0)throws MidasException {
		return dao.loadById(arg0);
	}

	@Override
	public void rehabilitarRecibosPorDetalleCalculo(Long arg0)throws MidasException {
		dao.rehabilitarRecibosPorDetalleCalculo(arg0);
	}

	@Override
	public void rehabilitarRecibosPorDetalleCalculo(DetalleCalculoComisiones arg0) throws MidasException {
		dao.rehabilitarRecibosPorDetalleCalculo(arg0);
	}

	@Override
	public Long saveReciboCalculoComisiones(ReciboCalculoComisiones arg0)throws MidasException {
		return dao.saveReciboCalculoComisiones(arg0);
	}
	@Override
	public void generarRecibosCalculoComisiones(Long idCalculo) throws MidasException{
		dao.generarRecibosCalculoComisiones(idCalculo);
	}
	@EJB
	public void setDao(ReciboCalculoComisionesDao dao) {
		this.dao = dao;
	}
}
