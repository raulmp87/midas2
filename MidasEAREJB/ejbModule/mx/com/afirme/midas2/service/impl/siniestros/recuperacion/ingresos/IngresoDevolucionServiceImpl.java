package mx.com.afirme.midas2.service.impl.siniestros.recuperacion.ingresos;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.pagos.facturas.RecepcionFacturaDao;
import mx.com.afirme.midas2.dao.siniestros.recuperacion.ingresos.IngresoDevolucionDao;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo;
import mx.com.afirme.midas2.domain.catalogos.CatGrupoFijo.TIPO_CATALOGO;
import mx.com.afirme.midas2.domain.catalogos.banco.BancoMidas;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.DocumentoFiscal.OrigenDocumentoFiscal;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.EnvioValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.pagos.facturas.MensajeValidacionFactura;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.Recuperacion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCompania;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionCruceroJuridica;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionDeducible;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionProveedor;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSalvamento;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.RecuperacionSipac;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ReferenciaBancaria;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.cobranza.ConceptoGuia;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion.Estatus;
import mx.com.afirme.midas2.domain.siniestros.recuperacion.ingresos.IngresoDevolucion.TipoDevolucion;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestro;
import mx.com.afirme.midas2.domain.siniestros.solicitudcheque.SolicitudChequeSiniestroDetalle;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.EVENTO;
import mx.com.afirme.midas2.domain.sistema.bitacora.Bitacora.TIPO_BITACORA;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.DetalleCuentasContablesDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.DetalleOrdenExpedicionDTO;
import mx.com.afirme.midas2.dto.siniestros.indemnizacion.ImpresionOrdenExpedicionDTO;
import mx.com.afirme.midas2.dto.siniestros.recuperacion.ingresos.IngresoDevolucionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.siniestros.pagos.facturas.RecepcionFacturaServiceImpl;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;
import mx.com.afirme.midas2.service.siniestros.pagos.facturas.RecepcionFacturaService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoDevolucionService;
import mx.com.afirme.midas2.service.siniestros.recuperacion.ingresos.IngresoService;
import mx.com.afirme.midas2.service.siniestros.solicitudcheque.SolicitudChequeService;
import mx.com.afirme.midas2.service.sistema.bitacora.BitacoraService;
import mx.com.afirme.midas2.util.CollectionUtils;
import mx.com.afirme.midas2.util.EnumUtil;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.utils.CommonUtils;
import mx.com.afirme.midas2.utils.GeneradorImpresion;
import mx.gob.sat.cfd.x3.ComprobanteDocument;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Retenciones.Retencion;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.Impuestos.Traslados.Traslado;
import mx.gob.sat.cfd.x3.ComprobanteDocument.Comprobante.TipoDeComprobante;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;

import org.apache.axis.utils.ByteArrayOutputStream;
import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.apache.xmlbeans.XmlException;

@Stateless
public class IngresoDevolucionServiceImpl implements IngresoDevolucionService {
	
	private static final Logger			log									= Logger.getLogger(RecepcionFacturaServiceImpl.class);
	public static final String			NUMERO_FACTURA_FICTICIA				= "xxxxxxxx";
	private static final String			MSG_CANCELAR_DEVOLUCION_SOLICITUDCHEQUE = "Se cancela la solicitud de cheque [ %1$s ] de la devolución del ingreso [ %2$s ]";
	private static final String			MSG_RECHAZO_DEVOLUCION_SOLICITUDCHEQUE = "Se rechaza la devolucion de ingreso";
	private static final String			MSG_AUTORIZA_DEVOLUCION_SOLICITUDCHEQUE = "Se autoriza la solicitud de cheque [ %1$s ] de la devolución del ingreso [ %2$s ]";
	private static final String			MSG_REGISTRO_DEVOLUCION_SOLICITUDCHEQUE = "Se registra la solicitud de cheque [ %1$s ] de la devolución del ingreso [ %2$s ]";

	@EJB
	private EntidadService entidadService;
	
	@EJB	
	private SolicitudChequeService solicitudChequeService;
	
	@EJB(beanName="UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	
	@EJB
	private BitacoraService bitacoraService;
	

	
	@EJB
	private IngresoDevolucionDao ingresoDevolucionDao;
	
	@EJB
	private ListadoService listadoService;

	@EJB
	private RecepcionFacturaDao recepcionFacturaDao;

	@EJB
	private RecepcionFacturaService recepcionFacturaService;
	
	@EJB
	private EnvioNotificacionesService envioNotificacionesService;
	
	@EJB
	private IngresoService ingresoService;
	
	private static String CONCEPTO_RECUPERACION = "RECUPERACIÓN";
	private static final String SOLICITUD_CHEQUE_NATURALEZA_CARGO = "C";
	private static final String SOLICITUD_CHEQUE_NATURALEZA_ABONO = "A";
	private static Integer NUMERO_RECUPERACIONES = 1;
	private static final String ORDEN_EXPEDICION_CHEQUE_DEVINGRESO = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionChequeDevIngreso.jrxml";
	private static final String ORDEN_EXPEDICION_DETALLE_DEVINGRESO = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionDetalleDevIngreso.jrxml";
	private static final String ORDEN_EXPEDICION_CUENTAS_DEVINGRESO = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/ordenExpedicionCuentasDevIngreso.jrxml";

	@Override
	public List<IngresoDevolucionDTO> buscar(IngresoDevolucionFiltro filtro){
		List<IngresoDevolucionDTO> list = ingresoDevolucionDao.buscar(filtro);
		if(CommonUtils.isNotEmptyList(list)){
			try{
				for(IngresoDevolucionDTO e: list){
					if (CommonUtils.isNotNull(e.getSolicitadoPor())){
						Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(e.getSolicitadoPor());
						e.setSolicitadoPor(usuario.getNombreCompleto() + "(" + e.getSolicitadoPor() + ")");
					}
					if (CommonUtils.isNotNull(e.getAutorizadoPor())){
						Usuario usuario = usuarioService.buscarUsuarioPorNombreUsuario(e.getAutorizadoPor());
						e.setAutorizadoPor(usuario.getNombreCompleto() + "(" + e.getSolicitadoPor() + ")");
					}	
				}
			}catch(Exception e){
			}
		}
		
		return list;
	}

	private void notificarCancelacion(IngresoDevolucion ingresoDevolucion) {
		String[] nombresRol = new String[1];
		// Gerente de Operaciones de Indemnizaciones
		nombresRol[0] = "Rol_M2_Gerente_Operaciones_Indemnizacion";
		EnvioNotificacionesService.EmailDestinaratios destinos = new EnvioNotificacionesService.EmailDestinaratios();
		// Se agrega a la lista de destinatarios, los correos de los usuarios
		// con el Rol Rol_M2_Gerente_Operaciones_Indemnizacion
		List<Usuario> usuarios  = new ArrayList<Usuario>();

		try{
			usuarios = usuarioService.buscarUsuariosPorNombreRol(nombresRol);
			usuarios.add(usuarioService.buscarUsuarioPorNombreUsuario(ingresoDevolucion.getUsuarioSolicita()));
			if(CommonUtils.isNotNull(ingresoDevolucion.getUsuarioAutorizaRechaza())){
				usuarios.add(usuarioService.buscarUsuarioPorNombreUsuario(ingresoDevolucion.getUsuarioAutorizaRechaza()));			
			}
			for (Usuario us : usuarios) {
				if (!StringUtil.isEmpty(us.getEmail()))
					destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, us.getEmail(), us.getNombreCompleto());
			}			
		}catch(Exception e){
			//Ocurre error al obtener destinatarios
			destinos.agregar(DestinatarioConfigNotificacionCabina.EnumModoEnvio.CC, "0arturo.saldana@afirme.com", "Arturo Saldana Fikir");
		}


		HashMap<String, Serializable> data = new HashMap<String, Serializable>();

		if (CommonUtils.isNotNullNorZero((ingresoDevolucion.getSolicitudCheque()))) {
			data.put("numSolicitud", ingresoDevolucion.getSolicitudCheque().getId());
		} else {
			data.put("numSolicitud", "");
		}
		if (CommonUtils.isNotNullNorZero((ingresoDevolucion.getFechaAutorizaRechaza()))) {
			data.put("fechaAutorizacion", ingresoDevolucion.getFechaAutorizaRechaza());
		} else {
			data.put("fechaAutorizacion", "");
		}
		data.put("fechaCreacion", ingresoDevolucion.getFechaCreacion());
		data.put("fechaCancelacion", new Date());
		data.put("beneficiario", ingresoDevolucion.getBeneficiario());
		data.put("importe", ingresoDevolucion.getImporteTotal());
		String cheque= "";
		if( ingresoDevolucion.getSolicitudCheque()!= null && ingresoDevolucion.getSolicitudCheque().getId()!=null){
			cheque	="-"+ingresoDevolucion.getSolicitudCheque().getId();
		}

		envioNotificacionesService.enviarNotificacion(EnumCodigo.NOTIF_CANCELACION_CHEQUE_DEVINGRESO.toString(), data, ingresoDevolucion.getId() +cheque, destinos);
	}

	@Override
	public Short cancelar(Long ingresoDevolucionId){
		Short respuesta;
		IngresoDevolucion solicitudCheque = entidadService.findById(IngresoDevolucion.class, ingresoDevolucionId);
		if(CommonUtils.isNotNull(solicitudCheque) && CommonUtils.isNotNull(solicitudCheque.getSolicitudCheque())){
			respuesta = ingresoDevolucionDao.solicitarCancelacionCheque(solicitudCheque.getSolicitudCheque().getIdSolCheque());
		}else{
			if(solicitudCheque.getEstatus().equals(IngresoDevolucion.Estatus.PA)){
				respuesta = IngresoDevolucionDao.RESULTADO_OK;
			}else{
				respuesta = IngresoDevolucionDao.RESULTADO_ERROR;
			}
		}
		if(respuesta == IngresoDevolucionDao.RESULTADO_OK){
			solicitudCheque.setEstatus(Estatus.C);
			guardar(solicitudCheque);
			String resp = ingresoService.reversaMontoCuentaAcreedora(solicitudCheque);
			if (resp != null){
				throw new NegocioEJBExeption("CONTABILIDAD_CANCDEVCHEQUE", resp);
			}
			notificarCancelacion(solicitudCheque);
			bitacoraService.registrar(TIPO_BITACORA.INGRESO, EVENTO.CANCELAR_DEVOLUCION_SOLICITUDCHEQUE, solicitudCheque.getId().toString(), 
					 String.format(MSG_CANCELAR_DEVOLUCION_SOLICITUDCHEQUE, 
							 String.valueOf(solicitudCheque.getId()), 
							 String.valueOf(solicitudCheque.getIngreso().getId())), 
					 solicitudCheque.toString(), usuarioService.getUsuarioActual().getNombreUsuario());
		}
		return respuesta;
		
	}

	@Override
	public IngresoDevolucion enviarSolicitudAutorizacion(IngresoDevolucion ingresoDevolucion){
		IngresoDevolucion solicitudCheque =null;
		List<IngresoDevolucion> lst = entidadService.findByProperty(IngresoDevolucion.class, "id", ingresoDevolucion.getId());
		if (CollectionUtils.isEmpty(lst)) {
			throw new NegocioEJBExeption("INGRESO_NO_EXISTE","No se encuentra el Ingreso Devolucion");
		}
		solicitudCheque=lst.get(0);
		
		Recuperacion recuperacion = solicitudCheque.getIngreso().getRecuperacion();
		solicitudCheque.setUsuarioSolicita(usuarioService.getUsuarioActual().getNombreUsuario());
		solicitudCheque.setFechaSolicita(new Date());
		solicitudCheque.setEstatus(Estatus.PA);
		solicitudCheque.setTipoDevolucion(ingresoDevolucion.getTipoDevolucion());
		solicitudCheque.setFormaPago(ingresoDevolucion.getFormaPago());
		solicitudCheque.setBeneficiario(ingresoDevolucion.getBeneficiario());
		solicitudCheque.setLadaTelefono(ingresoDevolucion.getLadaTelefono());
		solicitudCheque.setTelefono(ingresoDevolucion.getTelefono());
		solicitudCheque.setCorreo(ingresoDevolucion.getCorreo());
		solicitudCheque.setBanco(ingresoDevolucion.getBanco());
		solicitudCheque.setClabe(ingresoDevolucion.getClabe());
		solicitudCheque.setObservaciones(ingresoDevolucion.getObservaciones());
		solicitudCheque.setFechaAutorizaRechaza(null);
		solicitudCheque.setUsuarioAutorizaRechaza(null);
		
		if(ingresoDevolucion.getTipoDevolucion().equals(TipoDevolucion.P)){
			solicitudCheque.setCuentaAcreedoraRestante(entidadService.findById(ConceptoGuia.class, ingresoDevolucion.getCuentaAcreedoraRestante().getId()));
			obtenerMontosRecuperacion(recuperacion, solicitudCheque);
			//Si es parcial, se calculan los montos como se muestran en pantalla.
			solicitudCheque.setSubtotal(ingresoDevolucion.getSubtotal());
			//Calcular IVA
			solicitudCheque.setIva(solicitudCheque.getIvaRec().multiply(ingresoDevolucion.getSubtotal()).divide(solicitudCheque.getSubtotalRec(), 2, BigDecimal.ROUND_HALF_UP));
			//Calcular IVA Retenido
			solicitudCheque.setIvaRetenido(solicitudCheque.getIvaRetenidoRec().multiply(ingresoDevolucion.getSubtotal()).divide(solicitudCheque.getSubtotalRec(), 2, BigDecimal.ROUND_HALF_UP));
			//Calcular ISR
			solicitudCheque.setIsr(solicitudCheque.getIsrRec().multiply(ingresoDevolucion.getSubtotal()).divide(solicitudCheque.getSubtotalRec(), 2, BigDecimal.ROUND_HALF_UP));
			//Calcular Importe Cheque
			solicitudCheque.setImporteTotal(solicitudCheque.getSubtotal().add(solicitudCheque.getIva().subtract(solicitudCheque.getIvaRetenido()).subtract(solicitudCheque.getIsr())));
			//Calcular Monto Restante
			solicitudCheque.setMontoRestante(solicitudCheque.getImporteDevolucion().subtract(solicitudCheque.getImporteTotal()));			
		}else{
			//Si es total, se obtienen nuevamente los montos, descartando los cambios si es que los hubiese.
			solicitudCheque = obtenerMontosRecuperacion(recuperacion, solicitudCheque);
			solicitudCheque.setSubtotal(solicitudCheque.getSubtotalRec());
			solicitudCheque.setIva(solicitudCheque.getIvaRec());
			solicitudCheque.setIvaRetenido(solicitudCheque.getIvaRetenidoRec());
			solicitudCheque.setIsr(solicitudCheque.getIsrRec());
			solicitudCheque.setImporteTotal(solicitudCheque.getImporteTotalRec());
			solicitudCheque.setMontoRestante(BigDecimal.ZERO);
		}	
		
		if(BigDecimal.valueOf(ingresoService.obtenerSaldoCuentaAcreedora(solicitudCheque.getCuentaAcreedora().getId())).compareTo(solicitudCheque.getImporteTotal()) == -1){
			throw new NegocioEJBExeption("SALDO_CUENTA_ACREEDORA","EL SALDO DE LA CUENTA ACREEDORA ES INSUFICIENTE");
		}else{
			String resp = ingresoService.aplicarMontoCuentaAcreedora(solicitudCheque);
			if (resp != null){
				throw new NegocioEJBExeption("CONTABILIDAD_DEVCHEQUE", resp);
			}			
		}
		guardar(solicitudCheque);
		return solicitudCheque;
	}

	private DocumentoFiscal procesaXML(ByteArrayOutputStream bos){
		DocumentoFiscal factura = new DocumentoFiscal();
		try {
			DocumentoFiscal facturaCargada = this.convertirComprobanteDocumentoFiscal(bos);
			if(facturaCargada.getTipo().equals(DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue())){
				Integer proveedorId = getProveedor(facturaCargada.getRfcEmisor());
				if(CommonUtils.isNotNullNorZero(proveedorId)){
					DocumentoFiscal facturaExistente = obtieneFacturaExistente(facturaCargada);
					if(CommonUtils.isNotNull(facturaExistente)){
						if(!facturaExistente.getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue()) && !facturaExistente.getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.PAGADA.getValue())){
							facturaCargada.setId(facturaExistente.getId());
							facturaCargada.setFechaCreacion(facturaExistente.getFechaCreacion());
							facturaCargada.setCodigoUsuarioCreacion(facturaExistente.getCodigoUsuarioCreacion());
							facturaCargada.setFechaDevolucion(facturaExistente.getFechaModificacion());
							factura = facturaCargada;
						}else{
							factura = facturaExistente;
						}
					}else{
						factura = facturaCargada;
					}
				}else{
					log.error("No existe el proveedor o existe más de uno con el mismo RFC en el sistema");
					factura = facturaCargada;
					factura.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
				}
			}else{
				log.error("El Comprobante cargado no es una factura");
				factura = facturaCargada;
				factura.setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
			}
		} catch (Exception e) {
			e.printStackTrace();
			log.error(e.getMessage(), e);
		}
		return factura;
	}

	@Override
	public EnvioValidacionFactura cargarFactura(File file){
		DocumentoFiscal factura;
		EnvioValidacionFactura facturaCargada = null;
		FileInputStream fis = null;
		try{
			fis = new FileInputStream(file);
			byte[] bytes = new byte[(int) file.length()];
			fis.read(bytes);
			ByteArrayOutputStream bos = new ByteArrayOutputStream( );
			bos.write(bytes);
			factura = this.procesaXML(bos);
			this.entidadService.save(factura);
			
			
			facturaCargada = creaEnvioValidacion(factura);
			this.validaProcesamientoFactura(facturaCargada);
			facturaCargada = (EnvioValidacionFactura) this.entidadService.save(facturaCargada);
			
			Map<String,String> estatusFacturas = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.ESTATUS_FACTURA);
			String estatusDesc = estatusFacturas.get(facturaCargada.getFactura().getEstatus());
			facturaCargada.getFactura().setEstatusStr(estatusDesc);

		}catch(Exception e){
			e.printStackTrace();
			return null;
		} finally {
			IOUtils.closeQuietly(fis);
		}

		return facturaCargada;
	}
	
	private void validaProcesamientoFactura(EnvioValidacionFactura envioValidacion){
		final String MENSAJE_ERROR_PROCESAMIENTO = "EL ARCHIVO NO SE PUDO PROCESAR";
//		final String MENSAJE_ERROR_RFC_PROVEEDOR = "EL RFC NO CORRESPONDE AL PROVEEDOR";
		final String MENSAJE_ERROR_FACTURA_REGISTRADA = "LA FACTURA SE ENCUENTRA REGISTRADA";
		final String MENSAJE_ERROR_FACTURA_PAGADA = "LA FACTURA SE ENCUENTRA PAGADA";
		final String MENSAJE_NO_ES_FACTURA = "ESTE DOCUMENTO NO ES UNA FACTURA";
		final String MENSAJE_FOLIO_EXISTENTE = "El folio se encuentra duplicado";
		
		List<String> foliosEnArchivo = new ArrayList<String>();
		
		final Long NO_TIPO_MENSAJE_INTERNO = 0L;
		
		List<MensajeValidacionFactura> mensajes = new ArrayList<MensajeValidacionFactura>();
		if(!envioValidacion.getFactura().getTipo().equals(DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue())){
			envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
			mensajes.add(recepcionFacturaService.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_NO_ES_FACTURA ));
		}else{
			if(!envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue())){
				if(envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.REGISTRADA.getValue())){
					mensajes.add(recepcionFacturaService.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_FACTURA_REGISTRADA ));
				}else if(envioValidacion.getFactura().getEstatus().equals(DocumentoFiscal.EstatusDocumentoFiscal.PAGADA.getValue())){
					mensajes.add(recepcionFacturaService.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_FACTURA_PAGADA ));
				}else if(foliosEnArchivo.contains( envioValidacion.getFactura().getNumeroFactura())){
					envioValidacion.getFactura().setEstatus(DocumentoFiscal.EstatusDocumentoFiscal.ERROR.getValue());
					mensajes.add(recepcionFacturaService.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_FOLIO_EXISTENTE ));
				}else{
					foliosEnArchivo.add(envioValidacion.getFactura().getNumeroFactura());
				}
			}else{
				if(envioValidacion.getFactura().getNumeroFactura().equals(NUMERO_FACTURA_FICTICIA)){
					mensajes.add(recepcionFacturaService.creaMensajeValidacionFactura(envioValidacion,NO_TIPO_MENSAJE_INTERNO,MENSAJE_ERROR_PROCESAMIENTO ));
				}
			}
		}
			envioValidacion.setMensajes(mensajes);

	}
	
	private EnvioValidacionFactura creaEnvioValidacion(DocumentoFiscal factura){
		String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
		List<EnvioValidacionFactura> envioValidacionList = new ArrayList<EnvioValidacionFactura>();
		Long batchId = this.recepcionFacturaDao.obtenerBatchId();
		EnvioValidacionFactura envioValidacionFactura = new EnvioValidacionFactura();
		envioValidacionFactura.setFactura(factura);
		envioValidacionFactura.setIdBatch(batchId);
		envioValidacionFactura.setEstatus(RecepcionFacturaServiceImpl.ENVIO_VALIDACION_PENDIENTE);
		envioValidacionFactura.setOrigen(OrigenDocumentoFiscal.ISN.toString());
		envioValidacionFactura.setFechaEnvio(new Date());
		envioValidacionFactura.setCodigoUsuarioCreacion(codigoUsuario);
		envioValidacionFactura.setCodigoUsuarioModificacion(codigoUsuario);
		envioValidacionList.add(envioValidacionFactura);

		return envioValidacionFactura;
	}

	private DocumentoFiscal obtieneFacturaExistente(DocumentoFiscal facturaCargada){
		Map<String,Object> params = new HashMap<String, Object>();
		params.put("rfcEmisor", facturaCargada.getRfcEmisor());
		params.put("numeroFactura", facturaCargada.getNumeroFactura());
		List<DocumentoFiscal> facturas = entidadService.findByProperties(DocumentoFiscal.class, params);
		return facturas != null && facturas.size() > 0 ? facturas.get(0) : null;
	}

	private DocumentoFiscal convertirComprobanteDocumentoFiscal(ByteArrayOutputStream bos){
		try {
			String facturaXML = bos.toString("UTF-8");
			ComprobanteDocument comprobanteDocument = ComprobanteDocument.Factory.parse(new ByteArrayInputStream(bos.toByteArray()));
			Comprobante comprobante = comprobanteDocument.getComprobante();
			
			DocumentoFiscal factura = new DocumentoFiscal();
			String numeroFactura = comprobante.getFolio();
			String nombreEmisor = comprobante.getEmisor().getNombre();
			String rfcEmisor = comprobante.getEmisor().getRfc();
			String nombreReceptor = comprobante.getReceptor().getNombre();
			String rfcReceptor = comprobante.getReceptor().getRfc();
			String monedaPago = comprobante.getMoneda();
			String tipoDocumentoFiscal = comprobante.getTipoDeComprobante() == TipoDeComprobante.INGRESO ? DocumentoFiscal.TipoDocumentoFiscal.FACTURA.getValue() : DocumentoFiscal.TipoDocumentoFiscal.NOTA_CREDITO.getValue();
			BigDecimal montoTotal = comprobante.getTotal();
			BigDecimal subTotal = comprobante.getSubTotal();
			Impuestos impuestos = comprobante.getImpuestos();
			BigDecimal iva = obtenerImpuestoTraslados(impuestos, Traslado.Impuesto.IVA);
			BigDecimal ivaRetenido = obtenerImpuestoRetenciones(impuestos, Retencion.Impuesto.IVA);
			BigDecimal isr = obtenerImpuestoRetenciones(impuestos, Retencion.Impuesto.ISR);
			String codigoUsuario = usuarioService.getUsuarioActual().getNombreUsuario();
			System.out.println("-------------------------------------------------------------");
			System.out.println(comprobante.toString());
			factura.setXmlFactura(facturaXML);
			factura.setNumeroFactura(numeroFactura);
			factura.setEstatus(RecepcionFacturaServiceImpl.FACTURA_PROCESADA);
			factura.setNombreEmisor(nombreEmisor);
			factura.setRfcEmisor(rfcEmisor);
			factura.setNombreReceptor(nombreReceptor);
			factura.setRfcReceptor(rfcReceptor);
			factura.setMonedaPago(monedaPago);
			factura.setTipo(tipoDocumentoFiscal);
			factura.setIva(iva);
			factura.setIvaRetenido(ivaRetenido);
			factura.setIsr(isr);
			factura.setSubTotal(subTotal);
			factura.setMontoTotal(montoTotal);
			factura.setCodigoUsuarioCreacion(codigoUsuario);
			factura.setCodigoUsuarioModificacion(codigoUsuario);
			factura.setOrigen(OrigenDocumentoFiscal.ISN);
			return factura;
			
			
		} catch (UnsupportedEncodingException e) {
			log.error(e.getMessage(), e);
		} catch (XmlException e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		} catch (IOException e) {
			log.error(e.getMessage(), e);
			e.printStackTrace();
		}
		
		return null;
	}

	@Override
	public IngresoDevolucion obtenerSolicitudCheque(Long ingresoDevolucionId){
		IngresoDevolucion ingresoDevolucion = entidadService.findById(IngresoDevolucion.class, ingresoDevolucionId);
		Recuperacion recuperacion = ingresoDevolucion.getIngreso().getRecuperacion();
		ingresoDevolucion = obtenerMontosRecuperacion(recuperacion, ingresoDevolucion);

		return ingresoDevolucion;
	}
	
	public Integer getProveedor(String rfc){
		Integer id = null;
		List<PrestadorServicio> proveedores = entidadService.findByProperty(PrestadorServicio.class, "personaMidas.rfc", rfc);
		if(proveedores.size() == 1){
			id = proveedores.get(0).getId();
		}
		return id;
	}

	@Override
	public IngresoDevolucion generar(IngresoDevolucion ingresoDevolucion, Long cuentaAcreedora){

		if(CommonUtils.isNotNull(ingresoDevolucion.getIngreso())){
			Recuperacion recuperacion = ingresoDevolucion.getIngreso().getRecuperacion();
			ConceptoGuia cuenta = entidadService.findById(ConceptoGuia.class, cuentaAcreedora);
			ingresoDevolucion.setCuentaAcreedora(cuenta);
			if(recuperacion.getReporteCabina().getSiniestroCabina()!=null){
				ingresoDevolucion.setSiniestroOrigen(recuperacion.getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
			}
			ingresoDevolucion.setTipoDevolucion(IngresoDevolucion.TipoDevolucion.T);
			ingresoDevolucion.setEstatus(IngresoDevolucion.Estatus.S);
			ingresoDevolucion.setFormaPago(IngresoDevolucion.FormaPago.CHQ);
			ingresoDevolucion.setFechaCreacion(new Date());
			ingresoDevolucion.setCodigoUsuarioCreacion(usuarioService.getUsuarioActual().getNombreUsuario());
			ingresoDevolucion = obtenerMontosRecuperacion(recuperacion, ingresoDevolucion);
			ingresoDevolucion = obtenerDatosDeudor(recuperacion, ingresoDevolucion);
			
			guardar(ingresoDevolucion);
			
			bitacoraService.registrar(TIPO_BITACORA.INGRESO, EVENTO.REGISTRAR_DEVOLUCION_SOLICITUDCHEQUE, ingresoDevolucion.getId().toString(), 
					 String.format(MSG_REGISTRO_DEVOLUCION_SOLICITUDCHEQUE, 
							 String.valueOf(ingresoDevolucion.getId()), 
							 String.valueOf(ingresoDevolucion.getIngreso().getId())), 
							 ingresoDevolucion.toString(), usuarioService.getUsuarioActual().getNombreUsuario());
		}

		return ingresoDevolucion;
	}

	private IngresoDevolucion obtenerMontosRecuperacion(Recuperacion recuperacion, IngresoDevolucion ingresoDevolucion){
		if(CommonUtils.isNotNull(ingresoDevolucion.getId())){
			if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.PROVEEDOR.toString())){
				RecuperacionProveedor r = (RecuperacionProveedor) recuperacion;
				if(r.getMedio().equals(Recuperacion.MedioRecuperacion.VENTA.toString())){
					ingresoDevolucion.setSubtotalRec(r.getSubTotalVenta());
					ingresoDevolucion.setIvaRec(r.getIvaVenta());
					ingresoDevolucion.setIvaRetenidoRec(BigDecimal.ZERO);
					ingresoDevolucion.setIsrRec(BigDecimal.ZERO);
					ingresoDevolucion.setImporteTotalRec(r.getMontoVenta());
				}else{
					ingresoDevolucion.setSubtotalRec(r.getSubTotal());
					ingresoDevolucion.setIvaRec(r.getIva());
					ingresoDevolucion.setIvaRetenidoRec(r.getIvaRetenido());
					ingresoDevolucion.setIsrRec(r.getIsr());
					ingresoDevolucion.setImporteTotalRec(r.getMontoTotal());					
				}
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.DEDUCIBLE.toString()) ){
				RecuperacionDeducible r = (RecuperacionDeducible) recuperacion;
				ingresoDevolucion.setSubtotalRec(r.getMontoFinalDeducible());
				ingresoDevolucion.setIvaRec(BigDecimal.ZERO);
				ingresoDevolucion.setIvaRetenidoRec(BigDecimal.ZERO);
				ingresoDevolucion.setIsrRec(BigDecimal.ZERO);				
				ingresoDevolucion.setImporteTotalRec(r.getMontoFinalDeducible());
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.SALVAMENTO.toString()) ){
				RecuperacionSalvamento r = (RecuperacionSalvamento) recuperacion;
				ingresoDevolucion.setSubtotalRec(r.obtenerVentaActiva().getSubtotal());
				ingresoDevolucion.setIvaRec(r.obtenerVentaActiva().getIva());
				ingresoDevolucion.setIvaRetenidoRec(BigDecimal.ZERO);
				ingresoDevolucion.setIsrRec(BigDecimal.ZERO);
				ingresoDevolucion.setImporteTotalRec(r.obtenerVentaActiva().getTotalVenta());
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.COMPANIA.toString()) ){
				RecuperacionCompania r = (RecuperacionCompania) recuperacion;
				ingresoDevolucion.setSubtotalRec(r.getImporte());
				ingresoDevolucion.setIvaRec(BigDecimal.ZERO);
				ingresoDevolucion.setIvaRetenidoRec(BigDecimal.ZERO);
				ingresoDevolucion.setIsrRec(BigDecimal.ZERO);
				ingresoDevolucion.setImporteTotalRec(r.getImporte());
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString()) ){
				RecuperacionCruceroJuridica r = (RecuperacionCruceroJuridica) recuperacion;
				ingresoDevolucion.setSubtotalRec(r.getMontoFinal());
				ingresoDevolucion.setIvaRec(BigDecimal.ZERO);
				ingresoDevolucion.setIvaRetenidoRec(BigDecimal.ZERO);
				ingresoDevolucion.setIsrRec(BigDecimal.ZERO);
				ingresoDevolucion.setImporteTotalRec(r.getMontoFinal());
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.SIPAC.toString()) ){
				RecuperacionSipac r = (RecuperacionSipac) recuperacion;
				ingresoDevolucion.setSubtotalRec(r.getValorEstimado());
				ingresoDevolucion.setIvaRec(BigDecimal.ZERO);
				ingresoDevolucion.setIvaRetenidoRec(BigDecimal.ZERO);
				ingresoDevolucion.setIsrRec(BigDecimal.ZERO);
				ingresoDevolucion.setImporteTotalRec(r.getValorEstimado());
			}
		}else{
			if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.PROVEEDOR.toString())){
				RecuperacionProveedor r = (RecuperacionProveedor) recuperacion;
				if(r.getMedio().equals(Recuperacion.MedioRecuperacion.VENTA.toString())){
					ingresoDevolucion.setSubtotal(r.getSubTotalVenta());
					ingresoDevolucion.setIva(r.getIvaVenta());
					ingresoDevolucion.setIvaRetenido(BigDecimal.ZERO);
					ingresoDevolucion.setIsr(BigDecimal.ZERO);
					ingresoDevolucion.setImporteTotal(r.getMontoVenta());
					ingresoDevolucion.setImporteDevolucion(r.getMontoVenta());
				}else{
					ingresoDevolucion.setSubtotal(r.getSubTotal());
					ingresoDevolucion.setIva(r.getIva());
					ingresoDevolucion.setIvaRetenido(r.getIvaRetenido());
					ingresoDevolucion.setIsr(r.getIsr());
					ingresoDevolucion.setImporteTotal(r.getMontoTotal());
					ingresoDevolucion.setImporteDevolucion(r.getMontoTotal());					
				}
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.DEDUCIBLE.toString()) ){
				RecuperacionDeducible r = (RecuperacionDeducible) recuperacion;
				ingresoDevolucion.setSubtotal(r.getMontoFinalDeducible());
				ingresoDevolucion.setImporteTotal(r.getMontoFinalDeducible());
				ingresoDevolucion.setImporteDevolucion(r.getMontoFinalDeducible());
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.SALVAMENTO.toString()) ){
				RecuperacionSalvamento r = (RecuperacionSalvamento) recuperacion;
				ingresoDevolucion.setSubtotal(r.obtenerVentaActiva().getSubtotal());
				ingresoDevolucion.setIva(r.obtenerVentaActiva().getIva());
				ingresoDevolucion.setIvaRetenido(BigDecimal.ZERO);
				ingresoDevolucion.setIsr(BigDecimal.ZERO);
				ingresoDevolucion.setImporteTotal(r.obtenerVentaActiva().getTotalVenta());
				ingresoDevolucion.setImporteDevolucion(r.obtenerVentaActiva().getTotalVenta());	
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.COMPANIA.toString()) ){
				RecuperacionCompania r = (RecuperacionCompania) recuperacion;
				ingresoDevolucion.setSubtotal(r.getImporte());
				ingresoDevolucion.setIva(BigDecimal.ZERO);
				ingresoDevolucion.setIvaRetenido(BigDecimal.ZERO);
				ingresoDevolucion.setIsr(BigDecimal.ZERO);
				ingresoDevolucion.setImporteTotal(r.getImporte());
				ingresoDevolucion.setImporteDevolucion(r.getImporte());				
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.CRUCEROJURIDICA.toString()) ){
				RecuperacionCruceroJuridica r = (RecuperacionCruceroJuridica) recuperacion;
				ingresoDevolucion.setSubtotal(r.getMontoFinal());
				ingresoDevolucion.setIva(BigDecimal.ZERO);
				ingresoDevolucion.setIvaRetenido(BigDecimal.ZERO);
				ingresoDevolucion.setIsr(BigDecimal.ZERO);
				ingresoDevolucion.setImporteTotal(r.getMontoFinal());
				ingresoDevolucion.setImporteDevolucion(r.getMontoFinal());
			}else if(recuperacion.getTipo().equalsIgnoreCase(Recuperacion.TipoRecuperacion.SIPAC.toString()) ){
				RecuperacionSipac r = (RecuperacionSipac) recuperacion;
				ingresoDevolucion.setSubtotal(r.getValorEstimado());
				ingresoDevolucion.setImporteTotal(r.getValorEstimado());
				ingresoDevolucion.setImporteDevolucion(r.getValorEstimado());
			}
			ingresoDevolucion.setMontoRestante(BigDecimal.ZERO);
		}

		return ingresoDevolucion;
	}
	
	private IngresoDevolucion obtenerDatosDeudor(Recuperacion recuperacion, IngresoDevolucion ingresoDevolucion){
		Recuperacion.TipoRecuperacion tr = EnumUtil.fromValue(Recuperacion.TipoRecuperacion.class, recuperacion.getTipo());
		switch(tr){
			case PROVEEDOR:
				RecuperacionProveedor rp = (RecuperacionProveedor)recuperacion;
				if(rp.getMedio().equals(Recuperacion.MedioRecuperacion.VENTA.toString())){
					ingresoDevolucion.setBeneficiario(rp.getComprador().getNombrePersona());
					ingresoDevolucion.setTelefono(rp.getComprador().getPersonaMidas().getContacto().getTelCasa());
					ingresoDevolucion.setLadaTelefono(rp.getComprador().getPersonaMidas().getContacto().getTelCasaLada());
					ingresoDevolucion.setCorreo(rp.getComprador().getPersonaMidas().getContacto().getCorreoPrincipal());
					ingresoDevolucion.setBanco(rp.getComprador().getCuentaActiva().getBancoId());
					ingresoDevolucion.setClabe(rp.getComprador().getCuentaActiva().getClabe());
				}else{
					PrestadorServicio ps = entidadService.findById(PrestadorServicio.class, Integer.parseInt(rp.getOrdenCompra().getIdBeneficiario().toString()));
					if(ps != null){
						ingresoDevolucion.setBeneficiario(ps.getNombrePersona());
						ingresoDevolucion.setTelefono(ps.getPersonaMidas().getContacto().getTelCasa());
						ingresoDevolucion.setLadaTelefono(ps.getPersonaMidas().getContacto().getTelCasaLada());
						ingresoDevolucion.setCorreo(ps.getPersonaMidas().getContacto().getCorreoPrincipal());
						ingresoDevolucion.setBanco(ps.getCuentaActiva().getBancoId());
						ingresoDevolucion.setClabe(ps.getCuentaActiva().getClabe());
					}

				}
				break;
			case DEDUCIBLE:
				RecuperacionDeducible rd = (RecuperacionDeducible)recuperacion;
				ingresoDevolucion.setBeneficiario(rd.getAfectado());
				break;
			case SALVAMENTO:
				//TODO 
				break;
			case COMPANIA:
				RecuperacionCompania rc = (RecuperacionCompania)recuperacion;
				ingresoDevolucion.setBeneficiario(rc.getCompania().getNombrePersona());
				ingresoDevolucion.setTelefono(rc.getCompania().getPersonaMidas().getContacto().getTelCasaCompleto());
				ingresoDevolucion.setCorreo(rc.getCompania().getPersonaMidas().getContacto().getCorreoPrincipal());
				ingresoDevolucion.setBanco(rc.getCompania().getCuentaActiva().getBancoId());
				ingresoDevolucion.setClabe(rc.getCompania().getCuentaActiva().getClabe());
				break;
			case CRUCEROJURIDICA:
				break;
		
		}
		return ingresoDevolucion;
	}
	
	@Override
	public IngresoDevolucion guardar(IngresoDevolucion ingresoDevolucion){
		if(ingresoDevolucion.getId() != null){
			ingresoDevolucion.setFechaModificacion(new Date());
			ingresoDevolucion.setCodigoUsuarioModificacion(usuarioService.getUsuarioActual().getNombreUsuario());
		}
		return entidadService.save(ingresoDevolucion);
	}

	@Override
	public Map<String, String> obtenerUsuariosSolicitadores(){
		List<IngresoDevolucion> list = entidadService.findAll(IngresoDevolucion.class);
		Map<String, String> map = new LinkedHashMap<String, String>();
		if(list.size() > 0){
			for(IngresoDevolucion element : list){
				if(CommonUtils.isValid(element.getUsuarioSolicita()) && !map.containsKey(element.getUsuarioSolicita())){
					try{
						map.put(element.getUsuarioSolicita(), usuarioService.buscarUsuarioPorNombreUsuario(element.getUsuarioSolicita()).getNombreCompleto());
					}catch(Exception e){
						map.put(element.getUsuarioSolicita(), element.getUsuarioSolicita());
					}
				}
			}
		}
		return map;
	}

	@Override
	public Map<String, String> obtenerUsuariosAutorizadores(){
		List<IngresoDevolucion> list = entidadService.findAll(IngresoDevolucion.class);
		Map<String, String> map = new LinkedHashMap<String, String>();
		if(list.size() > 0){
			for(IngresoDevolucion element : list){
				if(CommonUtils.isValid(element.getUsuarioAutorizaRechaza()) && !map.containsKey(element.getUsuarioAutorizaRechaza())){
					try{
						map.put(element.getUsuarioAutorizaRechaza(), usuarioService.buscarUsuarioPorNombreUsuario(element.getUsuarioAutorizaRechaza()).getNombreCompleto());
					}catch(Exception e){
						map.put(element.getUsuarioAutorizaRechaza(), element.getUsuarioAutorizaRechaza());
					}
				}
			}
		}
		return map;
	}
	
	public Map<String, String> obtenerCuentasAcreedoras(){
		return null;
	}

	@Override
	public void autorizar(Long ingresoDevolucionId)throws Exception {
		//ENVIAR A MIZAR
		SolicitudChequeSiniestro solicitudCheque =solicitudChequeService.solicitarChequeMizar(ingresoDevolucionId,SolicitudChequeSiniestro.OrigenSolicitud.DEVOLUCION_INGRESO);
		solicitudChequeService.migrarChequeMizar(solicitudCheque.getId());
		IngresoDevolucion ingresoDevolucion = entidadService.findById(IngresoDevolucion.class, ingresoDevolucionId);
		ingresoDevolucion.setEstatus(Estatus.A);
		ingresoDevolucion.setUsuarioAutorizaRechaza(usuarioService.getUsuarioActual().getNombreUsuario());
		ingresoDevolucion.setFechaAutorizaRechaza(new Date());
		ingresoDevolucion.setSolicitudCheque(solicitudCheque);
		guardar(ingresoDevolucion);	
		bitacoraService.registrar(TIPO_BITACORA.INGRESO, EVENTO.AUTORIZAR_DEVOLUCION_SOLICITUDCHEQUE, ingresoDevolucion.getId().toString(), 
				 String.format(MSG_AUTORIZA_DEVOLUCION_SOLICITUDCHEQUE, 
						 String.valueOf(ingresoDevolucion.getId()), 
						 String.valueOf(ingresoDevolucion.getIngreso().getId())), 
						 ingresoDevolucion.toString(), usuarioService.getUsuarioActual().getNombreUsuario());
	}
	
	@Override
	public void rechazar(Long ingresoDevolucionId){
		IngresoDevolucion ingresoDevolucion = entidadService.findById(IngresoDevolucion.class, ingresoDevolucionId);
		ingresoDevolucion.setEstatus(Estatus.S);
		ingresoDevolucion.setUsuarioAutorizaRechaza(usuarioService.getUsuarioActual().getNombreUsuario());
		ingresoDevolucion.setFechaAutorizaRechaza(new Date());
		guardar(ingresoDevolucion);
		
		String resp = ingresoService.reversaMontoCuentaAcreedora(ingresoDevolucion);
		bitacoraService.registrar(TIPO_BITACORA.INGRESO, EVENTO.RECHAZA_DEVOLUCION_INGRESO, ingresoDevolucionId.toString(), 
				 String.format(MSG_RECHAZO_DEVOLUCION_SOLICITUDCHEQUE, 
						 String.valueOf(ingresoDevolucionId), 
						 String.valueOf(ingresoDevolucionId)), 
						 ingresoDevolucionId.toString(), usuarioService.getUsuarioActual().getNombreUsuario());//RECHAZA_DEVOLUCION_INGRESO
		
		if (resp != null){
			throw new NegocioEJBExeption("CONTABILIDAD_CANCDEVCHEQUE", resp);
		}
	}

	@Override
	public List<EnvioValidacionFactura> obtenerFacturaCargada(Long ingresoDevolucionId){
		List<EnvioValidacionFactura> facturas = null;
		List<EnvioValidacionFactura> factura = null;
		try{
			IngresoDevolucion ingresoDevolucion = entidadService.findById(IngresoDevolucion.class, ingresoDevolucionId);
			if(CommonUtils.isNotNull(ingresoDevolucion.getFactura())){
				Map<String,Object> params = new HashMap<String, Object>();
				params.put("factura", ingresoDevolucion.getFactura());
		//		params.put("origen", OrigenDocumentoFiscal.ISN.toString());
				facturas = entidadService.findByPropertiesWithOrder(EnvioValidacionFactura.class, params, "fechaEnvio");
				if(!facturas.isEmpty()){
					factura = new ArrayList<EnvioValidacionFactura>();
					factura.add(facturas.get(facturas.size()-1));
				}
			}			
		}catch(Exception e){
			e.printStackTrace();
		}


		return factura;
	}

	@Override
	public TransporteImpresionDTO obtenerImpresionCheque(Long ingresoDevolucionId){
		GeneradorImpresion gImpresion = new GeneradorImpresion();
		
		List<IngresoDevolucion> ingresosDevolucion = entidadService.findByProperty(IngresoDevolucion.class, "id", ingresoDevolucionId);
		IngresoDevolucion ingresoDev = ingresosDevolucion.get(0);
		
		JasperReport jReport = gImpresion.getOJasperReport(ORDEN_EXPEDICION_CHEQUE_DEVINGRESO);;
		JasperReport jReporteDetalleOrden = gImpresion.getOJasperReport(ORDEN_EXPEDICION_DETALLE_DEVINGRESO);
		JasperReport jReporteDetalleCuentas = gImpresion.getOJasperReport(ORDEN_EXPEDICION_CUENTAS_DEVINGRESO);
		
		List<ImpresionOrdenExpedicionDTO> dataSourceImpresion = new ArrayList<ImpresionOrdenExpedicionDTO>();
		ImpresionOrdenExpedicionDTO informacionOrden = 	obtenerInformacionOrdenExpedicion(ingresoDev);
		
		dataSourceImpresion.add(informacionOrden);
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("PRUTAIMAGEN", SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		params.put("jReporteDetalleOrden", jReporteDetalleOrden);
		params.put("jReporteDetalleCuentas", jReporteDetalleCuentas);
		
		JRBeanCollectionDataSource collectionDataSource = new JRBeanCollectionDataSource(dataSourceImpresion);
		return gImpresion.getTImpresionDTO(jReport, collectionDataSource, params);
	}

	private ImpresionOrdenExpedicionDTO obtenerInformacionOrdenExpedicion(IngresoDevolucion ingresoDev){
		ImpresionOrdenExpedicionDTO ordenExpedicion = new ImpresionOrdenExpedicionDTO();
		
		if(ingresoDev != null){
			
			ordenExpedicion.setaFavor(ingresoDev.getBeneficiario());						//A FAVOR
			ordenExpedicion.setTelefono(ingresoDev.getTelefono());							//TELEFONO
			ordenExpedicion.setCorreo(ingresoDev.getCorreo());								//CORREO
			
			ordenExpedicion.setPorConcepto(													//POR CONCEPTO DE
					CONCEPTO_RECUPERACION);
					
			ordenExpedicion.setFechaSolicitud(ingresoDev.getFechaSolicita());				//FECHA DE SOLICITUD

			if(ingresoDev.getUsuarioSolicita() != null
					&& !ingresoDev.getUsuarioSolicita().isEmpty()){
				Usuario usuarioSolicita = usuarioService.buscarUsuarioPorNombreUsuario(ingresoDev.getUsuarioSolicita());
				String nombreUsuarioSolicita = (usuarioSolicita!=null)? usuarioSolicita.getNombreCompleto():ingresoDev.getUsuarioSolicita();
				ordenExpedicion.setUsuarioSolicita(nombreUsuarioSolicita);	  				//USUARIO QUE SOLICITA
			}
			
			ordenExpedicion.setSolicitudChequeMizar(										//SOLICITUD CHEQUE MIZAR
					(ingresoDev.getSolicitudCheque() != null)? ingresoDev.getSolicitudCheque().getIdSolCheque() : null);
			
			ordenExpedicion.setFechaElaboracionCheque(										//FECHA ELABORACION CHEQUE
					(ingresoDev.getSolicitudCheque() != null)? ingresoDev.getSolicitudCheque().getFechaPago() : null);
			
			ordenExpedicion.setNumeroChequeReferencia(										//NO. DE CHEQUE/REFERENCIA DE MIZAR
					(ingresoDev.getSolicitudCheque() != null)? ingresoDev.getSolicitudCheque().getCheque() : null);
			
			if(ingresoDev.getFormaPago() != null){											//TIPO (Tipo de Liquidacion: cheque, transferencia bancaria)
				Map<String, String> tiposLiquidacion = listadoService.obtenerCatalogoValorFijo(TIPO_CATALOGO.TIPO_LIQUIDACION_SINIESTRO);
				ordenExpedicion.setTipo(tiposLiquidacion.get(ingresoDev.getFormaPago().toString()));
			}
			
			ordenExpedicion.setCuenta(ingresoDev.getClabe());								//CUENTA
		
			ordenExpedicion.setBanco(														//BANCO
					(ingresoDev.getBanco() != null)? buscarNombreBancoPorId(ingresoDev.getBanco()) : null);	
			
			ordenExpedicion.setNumeroPagos(													//NUMERO DE PAGOS
					(ingresoDev.getIngreso() != null 
							&& ingresoDev.getIngreso().getRecuperacion() != null)?
									NUMERO_RECUPERACIONES : 0);												
			
			ordenExpedicion.setDetallesOrden(												//DETALLES DE ORDEN DE EXPEDICION DE CHEQUES
					obtenerDetallesOrdenExpedicion(ingresoDev));
			
			ordenExpedicion.setTotal(														//DETALLES DE ORDEN DE EXPEDICION (TOTALES)
					(ordenExpedicion.getDetallesOrden() != null 
							&& !ordenExpedicion.getDetallesOrden().isEmpty())?
									ordenExpedicion.getDetallesOrden().get(0).getaPagar() : BigDecimal.ZERO );					
			
			ordenExpedicion.setDetalleCuentas(obtenerDetallesCuentas(ingresoDev));			//DETALLES DE CUENTAS CONTABLES
			llenarTotalesDetalleCuenta(														//DETALLES DE CUENTAS CONTABLES (CARGO TOTAL)
					ordenExpedicion, ordenExpedicion.getDetalleCuentas());					//DETALLES DE CUENTAS CONTABLES (ABONO TOTAL)
			
			if(ingresoDev.getCodigoUsuarioCreacion() != null 
					&& !ingresoDev.getCodigoUsuarioCreacion().isEmpty()){
				Usuario usuarioElaborado = usuarioService.buscarUsuarioPorNombreUsuario(ingresoDev.getCodigoUsuarioCreacion());
				String nombreUsuarioElaborado = (usuarioElaborado!=null)? usuarioElaborado.getNombreCompleto():ingresoDev.getCodigoUsuarioCreacion();
				ordenExpedicion.setElaborado(nombreUsuarioElaborado);						//ELABORADO - USUARIO QUE CREO LA LIQUIDACION
			}
			
			
			
			if(esIngresoDevolucionAutorizado(ingresoDev)
					&& ingresoDev.getUsuarioAutorizaRechaza() != null
					&& !ingresoDev.getUsuarioAutorizaRechaza().isEmpty()){
				String codigoUsuarioAutorizador = (esIngresoDevolucionAutorizado(ingresoDev))? ingresoDev.getUsuarioAutorizaRechaza() : "";
				Usuario usuarioAprobado = usuarioService.buscarUsuarioPorNombreUsuario(codigoUsuarioAutorizador);
				String nombreUsuarioAprobado = (usuarioAprobado!=null)? usuarioAprobado.getNombreCompleto():codigoUsuarioAutorizador;
				ordenExpedicion.setAprobado(nombreUsuarioAprobado); 						//APROBADO - USUARIO QUE AUTORIZO LA LIQUIDACION
			}

				ordenExpedicion.setRamo(													//RAMO (vida, autos, danios)
					obtenerClaveNegocio(ingresoDev));
			
			ordenExpedicion.setObservaciones(												//OBSERVACIONES
					ingresoDev.getObservaciones());
		}
		
		return ordenExpedicion;
	}
	
	/**
	 * Obtiene la clave de negocio del primer reporte de la liquidacion que tenga valor.
	 * @param liquidacion
	 * @return
	 */
	private String obtenerClaveNegocio(IngresoDevolucion ingresoDev){
		String claveNegocio = "";
		
		if(ingresoDev != null 
				&& ingresoDev.getIngreso() != null
				&& ingresoDev.getIngreso().getRecuperacion() != null
				&& ingresoDev.getIngreso().getRecuperacion().getReporteCabina() != null
				&& ingresoDev.getIngreso().getRecuperacion().getReporteCabina().getClaveNegocio() != null){
			
			Map<String,String> ramosLiq = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.RAMO_LIQUIDACION);
			claveNegocio = ramosLiq.get(ingresoDev.getIngreso().getRecuperacion().getReporteCabina().getClaveNegocio());
		}

		return claveNegocio;
	}
	
	/**
	 * Busca el nombre del banco dependiendo de la id que se le pase como parametro
	 * @param bancoId
	 * @return
	 */
	private String buscarNombreBancoPorId(Integer bancoId){
		String nombreBanco = "";
		
		if(bancoId != null){
			BancoMidas banco = entidadService.findById(BancoMidas.class, bancoId.longValue());
			if(banco != null){
				nombreBanco = banco.getNombre();
			}
		}
		
		return nombreBanco;
	}

	/**
	 * Metodo para determinar si se debe mostrar el campo AUTORIZADO en la impresion de la liquidacion
	 * @param liquidacion
	 * @return
	 */
	private Boolean esIngresoDevolucionAutorizado(IngresoDevolucion ingresoDev){
		Boolean esAutorizada = Boolean.FALSE;
		if(ingresoDev.getEstatus().equals(Estatus.A) 
				|| ingresoDev.getEstatus().equals(Estatus.P)){
			esAutorizada = Boolean.TRUE;
		}
		return esAutorizada;
	}

	private List<DetalleOrdenExpedicionDTO> obtenerDetallesOrdenExpedicion(IngresoDevolucion ingresoDev){
		List<DetalleOrdenExpedicionDTO> detallesOrden = new ArrayList<DetalleOrdenExpedicionDTO>();
		
		if(ingresoDev != null){
			DetalleOrdenExpedicionDTO detalleOrden = new DetalleOrdenExpedicionDTO();
			
			detalleOrden.setSubtotal(ingresoDev.getSubtotal());
			detalleOrden.setIvaAcred(ingresoDev.getIva());
			detalleOrden.setIvaRet(ingresoDev.getIvaRetenido());
			detalleOrden.setIsrRet(ingresoDev.getIsr());
			detalleOrden.setaPagar(ingresoDev.getImporteTotal());
			
			if(ingresoDev.getIngreso() != null
					&& ingresoDev.getIngreso().getRecuperacion() != null){							//NUMERO RECUPERACION
				detalleOrden.setNumeroRecuperacion(ingresoDev.getIngreso().getRecuperacion().getNumero().toString());
				
				Map<String,String> tiposRecuperacion = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.TIPO_RECUPERACION);
				detalleOrden.setTipoRecuperacion(													//TIPO RECUPERACION
						tiposRecuperacion.get(ingresoDev.getIngreso().getRecuperacion().getTipo()));
				
				if(ingresoDev.getIngreso().getRecuperacion().getReferenciasBancarias() != null
						&& !ingresoDev.getIngreso().getRecuperacion().getReferenciasBancarias().isEmpty()){
					ReferenciaBancaria referencia = ingresoDev.getIngreso().getRecuperacion().getReferenciasBancarias().get(0);
					detalleOrden.setReferencia(referencia.getReferencia());							//REFERENCIA
				}
				
				
			
				if( ingresoDev.getIngreso().getRecuperacion().getReporteCabina() != null
						&& ingresoDev.getIngreso().getRecuperacion().getReporteCabina().getSiniestroCabina() != null){
					detalleOrden.setSiniestro(														//SINIESTRO
							ingresoDev.getIngreso().getRecuperacion().getReporteCabina().getSiniestroCabina().getNumeroSiniestro());
				}
			}
			detallesOrden.add(detalleOrden);
		}
		
		return detallesOrden;
	}

	private List<DetalleCuentasContablesDTO> obtenerDetallesCuentas(IngresoDevolucion ingresoDev){
		List<DetalleCuentasContablesDTO> detalles = new ArrayList<DetalleCuentasContablesDTO>();
		
		if(ingresoDev != null
				&& ingresoDev.getSolicitudCheque() != null
				&& ingresoDev.getSolicitudCheque().getSolicitudChequeSiniestroDetalle() != null){
			
			Map<String, Object> params = new HashMap<String, Object>();
			params.put("solicitudChequeSiniestro.id", ingresoDev.getSolicitudCheque().getId());
			List<SolicitudChequeSiniestroDetalle> detallesCheque = 
				entidadService.findByPropertiesWithOrder(SolicitudChequeSiniestroDetalle.class, params, "cuentaContable ASC");
			
			for(SolicitudChequeSiniestroDetalle detalleCheque: detallesCheque){
				DetalleCuentasContablesDTO detalleCuenta = new DetalleCuentasContablesDTO();
				
				detalleCuenta.setCuenta(detalleCheque.getCuentaContable());				//CUENTA
				
				if( ingresoDev.getSolicitudCheque().getIdMoneda() != null){
					Map<Long, String> monedas = listadoService.getMapMonedas();			//MONEDA
					detalleCuenta.setMoneda(monedas.get(ingresoDev.getSolicitudCheque().getIdMoneda()));
				}
				
				if(detalleCheque.getRubro() != null){
					Map<String, String> rubros = listadoService.obtenerCatalogoValorFijo(CatGrupoFijo.TIPO_CATALOGO.CONCEPTOS_RUBROS_SOLICITUD_CHEQUE);
					if(rubros != null 
							&& !rubros.isEmpty()){										//CONCEPTO
						detalleCuenta.setConcepto(rubros.get(detalleCheque.getRubro()));
					}
				}
				
				if(detalleCheque.getNaturaleza().compareTo(SOLICITUD_CHEQUE_NATURALEZA_ABONO) == 0){
					detalleCuenta.setAbono(detalleCheque.getImporte());					//ABONO (NATURALEZA A)
					detalleCuenta.setCargo(BigDecimal.ZERO);							//CARGO	(NATURALEZA A)
				}else if(detalleCheque.getNaturaleza().compareTo(SOLICITUD_CHEQUE_NATURALEZA_CARGO) == 0){
					detalleCuenta.setAbono(BigDecimal.ZERO);							//ABONO (NATURALEZA C)
					detalleCuenta.setCargo(detalleCheque.getImporte());					//CARGO	(NATURALEZA C)
				}
				detalles.add(detalleCuenta);
			}
		}
		return detalles;
	}
	
	/**
	 * Calcula los totales de los detalles de cuenta y los asigna a la orden de expedicion que se pasa como parametro
	 * @param ordenExpedicion
	 * @param detallesCuenta
	 */
	private void llenarTotalesDetalleCuenta(ImpresionOrdenExpedicionDTO ordenExpedicion, List<DetalleCuentasContablesDTO> detallesCuenta){
		BigDecimal abonoTotal = new BigDecimal(0);
		BigDecimal cargoTotal = new BigDecimal(0);
		for(DetalleCuentasContablesDTO detalleCuenta: detallesCuenta){
			abonoTotal = abonoTotal.add((detalleCuenta.getAbono() != null)? detalleCuenta.getAbono(): BigDecimal.ZERO);
			cargoTotal = cargoTotal.add((detalleCuenta.getCargo() != null)? detalleCuenta.getCargo(): BigDecimal.ZERO);
		}
		ordenExpedicion.setAbonoTotal(abonoTotal);
		ordenExpedicion.setCargoTotal(cargoTotal);
	}

	private BigDecimal obtenerImpuestoTraslados(Impuestos impuestos, Traslado.Impuesto.Enum impuesto){
		BigDecimal importeImpuesto = null;
		Traslados traslados = impuestos.getTraslados();
		Traslado[] trasladosArray = traslados.getTrasladoArray();
		for(Traslado traslado : trasladosArray){
			if(impuesto == traslado.getImpuesto()){
				importeImpuesto = traslado.getImporte();
			}
		}
		return importeImpuesto;
	}
	
	private BigDecimal obtenerImpuestoRetenciones(Impuestos impuestos, Retencion.Impuesto.Enum impuesto){
		BigDecimal importeImpuesto = BigDecimal.ZERO;
		Retenciones retenciones = impuestos.getRetenciones();
		if(retenciones!=null){
			Retencion[] retencionesArray = retenciones.getRetencionArray();
			for(Retencion retencion : retencionesArray){
				if(impuesto == retencion.getImpuesto()){
					importeImpuesto = retencion.getImporte();
				}
			}
		}
		return importeImpuesto;
	}

}