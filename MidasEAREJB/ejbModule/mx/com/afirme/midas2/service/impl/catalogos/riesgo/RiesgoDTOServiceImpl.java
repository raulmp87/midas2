package mx.com.afirme.midas2.service.impl.catalogos.riesgo;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoDTO;
import mx.com.afirme.midas2.dao.catalogos.riesgo.RiesgoDTODao;
import mx.com.afirme.midas2.service.catalogos.riesgo.RiesgoDTOService;


@Stateless
public class RiesgoDTOServiceImpl implements RiesgoDTOService{
	
	private RiesgoDTODao riesgoDTODao;
	
	@EJB
	public void setRiesgoDTODao(RiesgoDTODao riesgoDTODao) {
		this.riesgoDTODao = riesgoDTODao;
	}


	@Override
	public List<RiesgoDTO> getListarFiltrado(RiesgoDTO riesgoDTO,Boolean mostrarInactivos){
		return riesgoDTODao.getListarFiltrado(riesgoDTO,mostrarInactivos);
	}
	
	@Override
	public List<RiesgoDTO> listarVigentesAutos(){
		return riesgoDTODao.listarVigentesAutos();
	}
	
}
