package mx.com.afirme.midas2.service.impl.seguridad;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;

import com.asm.dto.ApplicationDTO;
import com.asm.dto.CreatePasswordUserParameterDTO;
import com.asm.dto.CreatePortalUserParameterDTO;
import com.asm.dto.PageConsentDTO;
import com.asm.dto.ResendConfirmationEmailDTO;
import com.asm.dto.RoleDTO;
import com.asm.dto.SearchUserParametersDTO;
import com.asm.dto.SendResetPasswordTokenDTO;
import com.asm.dto.UserDTO;
import com.asm.dto.UserInboxEntryDTO;
import com.asm.dto.UserUpdateDTO;
import com.js.service.SystemException;

import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.movil.ajustador.AjustadorMovil;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
public class UsuarioServiceDelegate implements UsuarioService {

	public static final Logger LOG = Logger.getLogger(UsuarioServiceDelegate.class);
	private SistemaContext sistemaContext;
	private UsuarioService usuarioServiceImpl;
	private UsuarioService usuarioServiceMock;

	private UsuarioService delegate;
	
	
	@EJB(beanName="UsuarioServiceImpl")
	public void setUsuarioServiceImpl(UsuarioService usuarioServiceImpl) {
		this.usuarioServiceImpl = usuarioServiceImpl;
	}

	@EJB(beanName="UsuarioServiceMock")
	public void setUsuarioServiceMock(UsuarioService usuarioServiceMock) {
		this.usuarioServiceMock = usuarioServiceMock;
	}
	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}

	@PostConstruct
	public void initialize() {
		//Determinar que implementacion se va a utilizar.
		if (sistemaContext.getAsmActivo()) {
			delegate = usuarioServiceImpl;
		} else {
			delegate = usuarioServiceMock;
		}
	}
	
	@Override
	public Usuario getUsuarioActual() {
		return delegate.getUsuarioActual();
	}
	
	@Override
	public void setUsuarioActual(Usuario usuario){
		delegate.setUsuarioActual(usuario);
	}

	@Override
	public List<Usuario> buscarUsuariosSinRolesPorNombreRol(String nombreRol,
			String nombreUsuario, String idSesionUsuario) {
		return delegate.buscarUsuariosSinRolesPorNombreRol(nombreRol, nombreUsuario, idSesionUsuario);
	}

	@Override
	public List<Usuario> buscarUsuariosSinRolesPorNombreRol(
			String nombreUsuario, String idSesionUsuario, String... nombresRol) {
		return delegate.buscarUsuariosSinRolesPorNombreRol(nombreUsuario, idSesionUsuario, nombresRol);
	}

	@Override	
	@TransactionAttribute(TransactionAttributeType.SUPPORTS)
	public Usuario buscarUsuarioRegistrado(String nombreUsuario,
			String idSesionUsuario) {
		Usuario usuario = delegate.buscarUsuarioRegistrado(nombreUsuario, idSesionUsuario);
		return usuario;
	}

	@Override
	public boolean logOutUsuario(String nombreUsuario, String idSesionUsuario) {
		return delegate.logOutUsuario(nombreUsuario, idSesionUsuario);
	}

	@Override
	public List<PageConsentDTO> buscarConsentimientosPorRol(int idRole) {
		return delegate.buscarConsentimientosPorRol(idRole);
	}

	@Override
	public Usuario buscarUsuarioPorId(int idUsuario) {
		return delegate.buscarUsuarioPorId(idUsuario);
	}

	@Override
	public Usuario buscarUsuarioPorNombreUsuario(String nombreUsuario) {
		return delegate.buscarUsuarioPorNombreUsuario(nombreUsuario);
	}

	@Override
	public List<Usuario> buscarUsuariosPorNombreRol(String... nombresRol) {
		return delegate.buscarUsuariosPorNombreRol(nombresRol);
	}

	@Override
	public boolean tieneRol(String nombreRol, Usuario usuario) {
		return delegate.tieneRol(nombreRol, usuario);
	}

	public boolean tieneRolUsuarioActual(String nombreRol){
		return delegate.tieneRolUsuarioActual(nombreRol);
	}

	@Override
	public boolean tienePermisoUsuarioActual(String nombrePermiso) {
		LOG.info("Iniciando tienePermisoUsuarioActual en UsuarioServiceDelegate para el permiso: "+nombrePermiso);
		return delegate.tienePermisoUsuarioActual(nombrePermiso);
	}

	@Override
	public boolean tienePermisoUsuario(String nombrePermiso, Usuario usuario) {
		return delegate.tienePermisoUsuario(nombrePermiso, usuario);
	}

	@Override
	public boolean tieneRolUsuarioActual(String[] nombreRoles) {
		return delegate.tieneRolUsuarioActual(nombreRoles);
	}

	@Override
	public boolean tieneRol(String[] nombreRoles, Usuario usuario) {
		return delegate.tieneRol(nombreRoles, usuario);
	}

	@Override
	public boolean tienePermisoUsuarioActual(String[] nombrePermisos) {
		return delegate.tienePermisoUsuarioActual(nombrePermisos);
	}

	@Override
	public boolean tienePermisoUsuario(String[] nombrePermisos, Usuario usuario) {
		return delegate.tienePermisoUsuario(nombrePermisos, usuario);
	}

	@Override
	public Agente getAgenteUsuarioActual() {
		return delegate.getAgenteUsuarioActual();
	}
	
	@Override
	public List<Usuario> buscarUsuarioPorNombreCompleto(String likeCompleteName) {
		return delegate.buscarUsuarioPorNombreCompleto(likeCompleteName);
	}

	@Override
	public Usuario login(LoginParameter parameter) {
		return delegate.login(parameter);
	}

	@Override
	public AjustadorMovil getAjustadorMovilUsuarioActual() {
		return delegate.getAjustadorMovilUsuarioActual();
	}

	@Override
	public AjustadorMovil getAjustadorMovilUsuario(Usuario usuario) {
		return delegate.getAjustadorMovilUsuario(usuario);
	}

	@Override
	public AjustadorMovil getAjustadorMovilMidasUsuarioActual() {
		return delegate.getAjustadorMovilMidasUsuarioActual();
	}
	
	@Override
	public AjustadorMovil getAjustadorMovilMidasUsuario(Usuario usuario) {
		return delegate.getAjustadorMovilMidasUsuario(usuario);
	}
	@Override
	public AjustadorMovil getAjustadorMovilMidasUsuario(Long ajustadorId) {
		return delegate.getAjustadorMovilMidasUsuario(ajustadorId);
	}
	@Override
	public Usuario buscarUsuarioRegistrado(String token) {
		return delegate.buscarUsuarioRegistrado(token);
	}

	@Override
	public void deshabilitarToken(String token) {
		delegate.deshabilitarToken(token);
	}

	/**
	 * Obtiene los registrationIds para un <code>userName</code>
	 * 
	 * @deprecated reemplazado por {@link #getRegistrationIds(String, String)} o
	 *             {@link #getRegistrationId(String, String, String)} instead.
	 * @param username
	 * @return
	 */
	@Deprecated
	@Override
	public List<String> getRegistrationIds(String nombreUsuario) {
		return delegate.getRegistrationIds(nombreUsuario);
	}
	
	@Override
	public List<String> getRegistrationIds(String nombreUsuario, String deviceApplicationId) {
		return delegate.getRegistrationIds(nombreUsuario, deviceApplicationId);
	}
	
	@Override
	public String getRegistrationId(String nombreUsuario, String deviceApplicationId, String deviceUuid) {
		return delegate.getRegistrationId(nombreUsuario, deviceApplicationId, deviceUuid);
	}
		
	@Override
	public Usuario createPortalUser(CreatePortalUserParameterDTO parameter) {
		return delegate.createPortalUser(parameter);
	}

	@Override
	public void resendConfirmationEmail(Integer userId) {
		delegate.resendConfirmationEmail(userId);
	}
	
	@Override
	public void resendConfirmationEmail(ResendConfirmationEmailDTO resendConfirmationEmail) {
		delegate.resendConfirmationEmail(resendConfirmationEmail);
	}
	
	@Override
	public int confirmEmail(String confirmationCode) {
		return delegate.confirmEmail(confirmationCode);
	}

	@Override
	public void changePassword(String oldPassword, String newPassword) {
		delegate.changePassword(oldPassword, newPassword);
	}
	
	@Override
	public void resetPassword(String token, String newPassword) {
		delegate.resetPassword(token, newPassword);
	}
	
	@Override
	public void sendResetPasswordToken(Integer userId) {
		delegate.sendResetPasswordToken(userId);
	}
	
	@Override
	public void sendResetPasswordToken(SendResetPasswordTokenDTO sendResetPasswordToken) {
		delegate.sendResetPasswordToken(sendResetPasswordToken);
		
	}
		
	@Override
	public void updateUser(UserUpdateDTO userUpdate) {
		delegate.updateUser(userUpdate);
	}

	@Override
	public void changePassword2(String newPassword) {
		delegate.changePassword2(newPassword);		
	}

	@Override
	public void activateDeactivateUser(Integer userId, Boolean active, Integer administratorUserId) {
		delegate.activateDeactivateUser(userId, active, administratorUserId);
	}

	@Override
	public List<UserInboxEntryDTO> getUserList(SearchUserParametersDTO searchParameters) {
		return delegate.getUserList(searchParameters);
	}

	@Override
	public Usuario createPasswordUser(CreatePasswordUserParameterDTO parameter) {
		return delegate.createPasswordUser(parameter);
	}

	@Override
	public Integer findUserIdByUsername(String username) {
		return delegate.findUserIdByUsername(username);
	}

	@Override
	public Integer findRoleId(Integer applicationId, String roleName) {
		return delegate.findRoleId(applicationId, roleName);
	}
	
	@Override
	public boolean isUserNeedsToSetPassword(Integer userId) {
		return delegate.isUserNeedsToSetPassword(userId);
	}

	@Override
	public String getResetPasswordUrl(Integer userId) {
		return delegate.getResetPasswordUrl(userId);
	}

	@Override
	public Agente getAgenteUsuarioActual(Usuario usuario) {
		return delegate.getAgenteUsuarioActual(usuario);
	}
	
	@Override
	public boolean isUserHasAccessToApplication(Integer userId,
			Integer applicationId) {
		return delegate.isUserHasAccessToApplication(userId,applicationId);
	}
	
	@Override
	public boolean isUserPortalUser(Integer userId) {
		return delegate.isUserPortalUser(userId);
	}
	
	@Override
	public List<ApplicationDTO> getUserApplications(Integer userId) {
		return delegate.getUserApplications(userId);
	}

	@Override
	public boolean isTokenActive(String token) {
		return delegate.isTokenActive(token);
	}

	@Override
	public UserDTO getUserDTOById(Integer userId) {
		return delegate.getUserDTOById(userId);
	}
	
	@Override
	public void validateToken( String token ) throws SystemException{
		if (StringUtils.isBlank(token)){
			throw new SystemException("El token de sesión es requerido");
		} else{
			if(!delegate.isTokenActive(token))
				throw new SystemException("El token "+token+" es un token inactivo.");
		}
	}

	@Override
	public List<Usuario> buscarUsuariosPorNombreRolSimple(String... arg0) {
		return delegate.buscarUsuariosPorNombreRolSimple(arg0);
	}
	
	@Override
	public void updateMidasUser(UserDTO updateUser, UserDTO administrator,
			List<RoleDTO> roleList) {
		delegate.updateMidasUser(updateUser, administrator, roleList);
	}
	
	@Override
	public RoleDTO getRoleDTOById(int id) {
		
		return delegate.getRoleDTOById(id);
		
	}
	
}
