package mx.com.afirme.midas2.service.impl.ocra;

import java.io.PrintWriter;
import java.io.Serializable;
import java.io.StringWriter;
import java.io.Writer;
import java.net.ConnectException;
import java.rmi.MarshalException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeoutException;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.xml.ws.soap.SOAPFaultException;

import mx.com.afirme.midas2.domain.ocra.AmisConversionEstilo;
import mx.com.afirme.midas2.domain.ocra.AmisUbicacion;
import mx.com.afirme.midas2.domain.ocra.EnvioOcraLog;
import mx.com.afirme.midas2.domain.siniestros.cabina.notificaciones.DestinatarioConfigNotificacionCabina.EnumModoEnvio;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.AutoIncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.IncisoReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.ReporteCabina;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.lugar.LugarSiniestroMidas;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora.STATUS_BITACORA;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_PROCESO;
import mx.com.afirme.midas2.domain.sistema.parametro.ParametroGlobal;
import mx.com.afirme.midas2.dto.TransporteCommDTO;
import mx.com.afirme.midas2.exeption.CommException;
import mx.com.afirme.midas2.exeption.CommException.CODE;
import mx.com.afirme.midas2.service.bitemporal.siniestros.PolizaSiniestroService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.sistema.comm.CommServiceImpl;
import mx.com.afirme.midas2.service.ocra.EnvioOcraService;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EmailDestinaratios;
import mx.com.afirme.midas2.service.siniestros.notificaciones.EnvioNotificacionesService.EnumCodigo;
import mx.com.afirme.midas2.service.sistema.comm.CommBitacoraService;
import mx.com.afirme.midas2.service.sistema.parametro.ParametroGlobalService;
import mx.com.afirme.midas2.util.StringUtil;
import mx.com.afirme.midas2.wsClient.ocra.robo.AltaRoboBean;
import mx.com.afirme.midas2.wsClient.ocra.robo.MensajeBean;
import mx.com.afirme.midas2.wsClient.ocra.robo.RoboWSPortProxy;

import org.apache.log4j.Logger;
import org.apache.poi.ss.formula.functions.T;


@Stateless
public class EnvioOcraServiceImpl extends CommServiceImpl implements EnvioOcraService {
	
	public  static final Logger log = Logger.getLogger(EnvioOcraServiceImpl.class);
	private Long xIdReporteCabina;
	private String errorConversion = "";
	private static final String NOMBRE_RESPONSABLE_OCRA  = "ADOLFO PUENTES";
	
	@EJB
	private EntidadService               entidadService;
	@EJB
	private PolizaSiniestroService       polizaSiniestroService;
	@EJB
	private EnvioNotificacionesService   envioNotificacionesService;
	@EJB
	protected CommBitacoraService        commBitacoraService;
	@EJB
	private EnvioNotificacionesService   notificacionService;
	@EJB
	private ParametroGlobalService       parametroGlobalService;
	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService usuarioService;
	

	@SuppressWarnings("unchecked")
	@Override
	public String enviarAltaRoboOCRAServicio(Long idReporteCabina) {

		String mensajeGerente = "";
		Date fechaInicial = this.getFecha();
		
		try{
			
			this.xIdReporteCabina = idReporteCabina;
			Map<String,String> mDatosVehiculo = this.obtenerDatosVehiculo(idReporteCabina);
			ReporteCabina reporte = entidadService.findById(ReporteCabina.class, idReporteCabina);
			
			if( !mDatosVehiculo.isEmpty() ){
				
				// # VALIDAR SI HAY ERROR EN MAP
				//if ( mDatosVehiculo.get("error").equals("no") ){
	
					AltaRoboBean altaRoboBean = new AltaRoboBean();
	
					// # DATOS OBLIGATARIOS A ENVIAR: usuario,clave,id_municipio,id_tipo_robo,fecha_robo,hora_robo,cp_robo,num_siniestro,
					//                                id_color,id_tipo,no_serie,modelo,origen,num_motor,id_transporte
					
					altaRoboBean.setUsuario       ( this.obtenerCredecialesOcra(ParametroGlobalService.WS_OCRA_INSERTA_REPORTE_USER ) );
					altaRoboBean.setClave         ( this.obtenerCredecialesOcra(ParametroGlobalService.WS_OCRA_INSERTA_REPORTE_PASS ) );
					altaRoboBean.setIdMunicipio   ( this.convierteStringToInt(mDatosVehiculo.get("idMunicipio"), "IdMunicipio") );
					altaRoboBean.setIdTipoRobo    ( this.convierteStringToInt(mDatosVehiculo.get("tipoRobo"), "TipoRobo") );
					altaRoboBean.setFechaRobo     ( mDatosVehiculo.get("fechaOcurrido"));
					altaRoboBean.setHoraRobo      ( mDatosVehiculo.get("horOcurrido"));
					altaRoboBean.setCpRobo        ( mDatosVehiculo.get("cpRobo"));
					altaRoboBean.setNumSiniestro  ( mDatosVehiculo.get("numeroSiniestro"));
					altaRoboBean.setIdColor       ( this.convierteStringToInt(mDatosVehiculo.get("color"), "Color"));
					altaRoboBean.setIdTipo        ( this.convierteStringToInt(mDatosVehiculo.get("tipoId"), "TipoId") );
					altaRoboBean.setNoSerie       ( mDatosVehiculo.get("numeroSerie") );
					altaRoboBean.setModelo		  ( this.convierteStringToInt(mDatosVehiculo.get("anio"), "Año") );
					altaRoboBean.setOrigen		  ( 0 ); // # NACIONAL
					altaRoboBean.setIdMoneda      ( 1 );
					altaRoboBean.setNumAsaltantes ( 0 );
					altaRoboBean.setImporte       ( 0.0 );
					altaRoboBean.setNumMotor	  ( mDatosVehiculo.get("numMotor"));
					altaRoboBean.setIdTransporte  ( this.convierteStringToInt(mDatosVehiculo.get("transporteId"), "TransporteId") );
					altaRoboBean.setPlaca         ( limpiarString(mDatosVehiculo.get("placa")) );
					
					//List<MensajeBean> lMensaje = roboWSPortProxy.altaRobo(altaRoboBean);
					
					if( permiteEnviar( altaRoboBean.getNumSiniestro() ) ){
						this.realizarEnvio(altaRoboBean);	
						log.info("ENVIO REALIZADO");
						this.guardaLog("ENVIO PERMITIDO",altaRoboBean.toString(),fechaInicial,new Date(),idReporteCabina,"SE INICIA PROCESO DE ENVIO OCRA");
					}else{
						log.info("NO SE PERMITE ENVIO OCRA");
						this.guardaLog("ENVIO NO PERMITIDO",altaRoboBean.toString(),fechaInicial,new Date(),idReporteCabina,"ENVÍO NO PERMITIDO");
					}
					
				}else{
					// NOTIFICA EL ERROR EN LOS DATOS
					mensajeGerente += "ERROR | "+mDatosVehiculo.get("error");
					reporte.setError(mensajeGerente);
					this.guardaLog(mensajeGerente,null,fechaInicial,this.getFecha(),idReporteCabina,"Error datos Envio");
					
					try{
						envioNotificacionesService.notificacionErrorOCRA(reporte, mensajeGerente);
					}catch(Exception e){
						log.error("Error notificacion:"+e);
					}
				}
			
			//}
		
		}catch(Exception e){
			mensajeGerente = "ERROR | "+this.errorConversion+ " - "+imprimirTrazaError(e);
			this.guardaLog("Error enviarAltaRoboOCRAServicio ",mensajeGerente,fechaInicial,new Date(),idReporteCabina,"ERROR EN METODO PRINCIPAL");

			// GUARDE EN COMMSERVICE ERROR
			this.registrarErrorPrevioEnvio(this.setInitComm(idReporteCabina), this.imprimirTrazaError(e));
			
			// NOTIFICACION DIRECTA A RESPONSABLE OCRA
			notificaResponsableOcra(idReporteCabina,mensajeGerente);
		}
		this.errorConversion = "";
		return mensajeGerente;
	}
	

	/**
	 * Retorna un mapa con los datos necesarios a mandar al WS
	 * @param idReporteCabina
	 * @return
	 */
	private Map<String,String> obtenerDatosVehiculo(Long idReporteCabina){
		
		Map<String,String> datosOcra  = new LinkedHashMap<String, String>();
		Map<String,String> mDatosAuto = null; 
		SimpleDateFormat sdFecha      = new SimpleDateFormat("yyyy/MM/dd");
		String mensajeError           = "",horaOcurrido="",estadoId="0",ciudadId="0";
		Date fechaOcurrido            = null;
		
		
		// # LLENAR DATOS OCRA CON NO VALIDOS
		datosOcra.put("idMunicipio"    , "0" );
		datosOcra.put("tipoRobo"       , "no" );
		datosOcra.put("fechaOcurrido"  , "no" );
		datosOcra.put("horOcurrido"    , "no" );
		datosOcra.put("cpRobo"         , "00000" );
		datosOcra.put("numeroSiniestro", "no" );
		datosOcra.put("color"          , "no" );
		datosOcra.put("numeroSerie"    , "no" );
		datosOcra.put("numMotor"       , "no" );
		datosOcra.put("anio"           , "no" );
		datosOcra.put("tipoId"         , "no" );
		datosOcra.put("transporteId"   , "no" );
		datosOcra.put("placa"          , "no" );
		
		try{
			
			// # BUSQUEDA POR ID REPORTE
			ReporteCabina reporteCabina = this.entidadService.findById(ReporteCabina.class, idReporteCabina);
			// # BUSQUEDA AUTOINCISO
			AutoIncisoReporteCabina autoIncisoReporteCabina = this.obtenerAutoIncisoByReporteCabina(idReporteCabina);
			
			if ( reporteCabina != null & autoIncisoReporteCabina !=null ){
				
				estadoId = ( reporteCabina.getLugarOcurrido() != null ? reporteCabina.getLugarOcurrido().getEstado().getId() : "0" );
				ciudadId = ( reporteCabina.getLugarOcurrido() != null ? reporteCabina.getLugarOcurrido().getCiudad().getId() : "0" );
				
				datosOcra.put("idMunicipio" , convertirMunicioToOcra( estadoId,ciudadId ) );
				datosOcra.put("tipoRobo"    , convertirTipoRobo( autoIncisoReporteCabina.getCausaSiniestro() ));
				
				// # VALIDAR CONVERSION DE HORA
				try{
					fechaOcurrido = reporteCabina.getFechaOcurrido();
					datosOcra.put("fechaOcurrido"  , sdFecha.format( fechaOcurrido ) );
				}catch (Exception e){
					log.error(e);
					mensajeError+= " La fecha de ocurrido no pudo ser convertida. ";
				}
				
				try{
					horaOcurrido = validarHora( reporteCabina.getHoraOcurrido().trim() );
					datosOcra.put("horOcurrido" , horaOcurrido );
				}catch (Exception e){
					log.error(e);
					mensajeError+= " La hora de ocurrido no pudo ser convertida. ";
				}

				datosOcra.put("cpRobo"         , obtenerCp( reporteCabina.getLugarOcurrido() ) );
				datosOcra.put("numeroSiniestro", (reporteCabina.getSiniestroCabina()!=null)?reporteCabina.getSiniestroCabina().getNumeroSiniestro():reporteCabina.getNumeroReporte() );
				datosOcra.put("color"          , ( ( autoIncisoReporteCabina.getColor() == null || autoIncisoReporteCabina.getColor().isEmpty() || autoIncisoReporteCabina.getColor().equals("")  ) ? "0" : autoIncisoReporteCabina.getColor() ) );
				// # SI NUMERO_SERIE_OCRA ES NULO, ENTONCES TOMA NUMERO_SERIE
				datosOcra.put("numeroSerie"    , ( (autoIncisoReporteCabina.getNumeroSerieOcra() == null) || (autoIncisoReporteCabina.getNumeroSerieOcra().equals("")) )? autoIncisoReporteCabina.getNumeroSerie() : autoIncisoReporteCabina.getNumeroSerieOcra() );
				datosOcra.put("placa"          , autoIncisoReporteCabina.getPlaca() );
				
				// # OBTENER DATOS DE AUTO
				mDatosAuto = this.convertirAutoOcra(idReporteCabina);
				
				if( !mDatosAuto.isEmpty() ){
					
					datosOcra.put("tipoId"      , mDatosAuto.get("tipoId") );
					datosOcra.put("transporteId", mDatosAuto.get("transporteId") );
					datosOcra.put("numMotor"    , mDatosAuto.get("numMotor") );
					datosOcra.put("anio"        , mDatosAuto.get("anio") );
					
				}else{
					mensajeError += " No se encontraron datos para el auto del reporte: "+idReporteCabina+" .";
				}
				
			}else{
				mensajeError+= " Los datos del reporte: "+idReporteCabina+" no fueron encontrados. ";
			}
		
		}catch(Exception e){
			log.error(e);
			mensajeError+= " Hubo un error al accesar a los datos; notifique el siguiente error a su administrador: "+this.imprimirTrazaError(e)+".";
			
			// GUARDE EN COMMSERVICE ERROR
			this.registrarErrorPrevioEnvio(this.setInitComm(idReporteCabina), mensajeError);
			
			// NOTIFICACION DIRECTA A RESPONSABLE OCRA
			notificaResponsableOcra(idReporteCabina,mensajeError);

		}
		
		// GUARDA EN LOG MAPA DE DATOS
		this.guardaLog(datosOcra.toString(),null,null,this.getFecha(),idReporteCabina,"DATOS VEHICULO");
		
		// # VALIDA QUE EL MAPA NO CONTENGA DATOS NO VALIDOS 
		mensajeError+= this.validadorGeneralMapAuto(datosOcra,estadoId,ciudadId);
		
		// # VALIDACION DE ERRORES
		if( !mensajeError.isEmpty() ){
			datosOcra.put("error", mensajeError );
		}else{
			datosOcra.put("error", "no" );
		}
		
		return datosOcra;
	}
	
	
	private String validadorGeneralMapAuto(Map<String,String> datosOcra , String... valores){
		
		String mensajeError = "";
		
		
		// # VALIDACION DE ID MUNICIPIO
		try{
			if( datosOcra.get("idMunicipio").equals("0") || datosOcra.get("idMunicipio").isEmpty() ){
				mensajeError+= "Equivalencia de municipio Ocra para los datos - "+
										" Estado: "+ valores[0] +
										" Ciudad: "+ valores[1] +
										" - no encontrado";
			}
		}catch (Exception e){
			log.error("Validacion Id Municipio: "+e);
			mensajeError+= "Equivalencia de municipio Ocra para los datos no encontrado";
		}
		
		// # VALIDACION DE TIPOID
		try{
			if( datosOcra.get("tipoId").equals("no") || datosOcra.get("tipoId").isEmpty() ){
				mensajeError+= " El tipo de auto en Ocra no encontrado. ";
			}
		}catch (Exception e){
			log.error("Validacion TipoId: "+e);
			mensajeError+= " El tipo de auto en Ocra no encontrado.";
		}
		
		// # VALIDACION DE TRANSPORTEID
		try{
			if( datosOcra.get("transporteId").equals("no") || datosOcra.get("transporteId").isEmpty() ){
				mensajeError+= " El tipo de transporte en Oca no encontrado. ";
			}
		}catch (Exception e){
			log.error("Validacion TransporteId: "+e);
			mensajeError+= "El tipo de transporte en Oca no encontrado.";
		}
		
		// # NUMMOTOR
		try{
			if( datosOcra.get("numMotor").equals("no") || datosOcra.get("numMotor").isEmpty() ){
				mensajeError+= " El número de motor no pudo se recuperado de la BD. ";
			}
		}catch (Exception e){
			log.error("Validacion Numero Motor: "+e);
			mensajeError+= "El número de motor no pudo se recuperado de la BD.";
		}
		
		// # ANIO
		try{
			if( datosOcra.get("anio").equals("no") || datosOcra.get("anio").isEmpty() ){
				mensajeError+= " El año del modelo del auto no pudo ser recuperado. ";
			}
		}catch (Exception e){
			log.error("Validacion Año: "+e);
			mensajeError+= " El año del modelo del auto no pudo ser recuperado.";
		}
		
		// # COLOR
		try{
			if( datosOcra.get("color").equals("no") || datosOcra.get("color").isEmpty()  ){
				mensajeError+= " El color del auto no pudo ser recuperado. ";
			}
		}catch (Exception e){
			log.error("Validación color Auto: "+e);
			mensajeError+= "El color del auto no pudo ser recuperado.";
		}
		
		// # CP
		try{
			if( datosOcra.get("cpRobo").isEmpty() ){
				mensajeError+= " El CP del auto no pudo ser recuperado. ";
			}
		}catch (Exception e){
			log.error("Validación CP: "+e);
			mensajeError+= "El CP del auto no pudo ser recuperado.";
		}
		
		// # TIPO ROBO
		try{
			if( datosOcra.get("tipoRobo").equals("0") || datosOcra.get("tipoRobo").isEmpty() ){
				mensajeError+= " El tipo de robo es no válido. ";
			}
		}catch (Exception e){
			log.error("Validacion Tipo Robo: "+e);
			mensajeError+= "El tipo de robo es no válido.";
		}
		
		return mensajeError;
	}
	
	
	/*private String obtenerNumeroSiniestro(Long idReporteCabina){
		
		try{
			
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			
			params.put("reporteCabina.id", idReporteCabina);
			List<SiniestroCabina> lSiniestroCabina = this.entidadService.findByProperties(SiniestroCabina.class, params);
			
			if( !lSiniestroCabina.isEmpty() ){
				return lSiniestroCabina.get(0).getNumeroSiniestro();
			}else{
				return "no";
			}
		
		}catch(Exception e){
			log.error("Error al obtenerNumeroSiniestro: "+e);
			return "no";
		}
		
	}*/
	
	/**
	 * La hora desde la BD puede venir con solo un digito,2,3 ó 4, si es asi se valida para concatenar la información restante
	 * @param hora
	 * @return
	 */
	public String validarHora(String hora){
		
		try{
			String[] aHora = hora.split(":");
			
			if( hora.length() < 5){
			
				if( aHora.length == 1 ){
					if( aHora[0].length() == 1 ){
						hora = "0"+aHora[0]+":00";
					}else{
						hora = aHora[0]+":00";
					}
				}else{
					StringBuilder tempHora = new StringBuilder("");
					for(int i=0; i<=1; i++){
						if( aHora[i].length() == 1 ){
							tempHora.append("0").append(aHora[0]).append(":00");
						}else{
							tempHora.append(aHora[0]).append(":00");
						}
					}
					hora = tempHora.toString();
				}
			
			}
		}catch(Exception e){
			log.error("Error al validarHora: "+e);
		}
		
		return hora;
	}
	
	/**
	 * Obtiene el CP en base al objeto LugarSiniestroMidas
	 * @param lugarSiniestroMidas
	 * @return
	 */
	private String obtenerCp(LugarSiniestroMidas lugarSiniestroMidas){
		
		String cp = "00000";
		
		if( lugarSiniestroMidas != null ){
			cp = ( lugarSiniestroMidas.getCodigoPostalOtraColonia() == null || lugarSiniestroMidas.getCodigoPostalOtraColonia().isEmpty() ) ? lugarSiniestroMidas.getCodigoPostal() : lugarSiniestroMidas.getCodigoPostalOtraColonia() ;
		}
		
		return cp;
	}
	
	/**
	 * Busca la equivalencia Ocra en municipio con los datos de midas
	 * @param estadoId
	 * @param ciudadId
	 * @return
	 */
	private String convertirMunicioToOcra(String estadoId, String ciudadId){
		
		try{
			
			List<AmisUbicacion> lMunicipioOcra = this.convertirUbicacion("PAMEXI", estadoId, ciudadId);	
			
			if ( !lMunicipioOcra.isEmpty()){
				return lMunicipioOcra.get(0).getId().getIdMunicipio().toString();
			}else{
				
				lMunicipioOcra = this.convertirUbicacion("PAMEXI", estadoId, "DESCONOCIDO");
				
				if ( !lMunicipioOcra.isEmpty()){
					return lMunicipioOcra.get(0).getId().getIdMunicipio().toString();
				}else{
					return "0";
				}
				
			}
		
		}catch(Exception e){
			log.error("Error al converirMunicipioOcra: "+e);
			return "0";
		}
		
	}
	
	/**
	 * Obtiene equivalencias de ubicaciones AMIS
	 * @param pais
	 * @param estado
	 * @param ciudad
	 * @return
	 */
	private List<AmisUbicacion> convertirUbicacion(String pais, String estado, String ciudad) {
		Map<String, Object> params = new HashMap<String, Object>();		
		params.put("cveSeycosPais", pais);
		params.put("cveSeycosEstado", estado);
		params.put("cveSeycosMunicipio", ciudad);
		return entidadService.findByProperties(AmisUbicacion.class, params);
	}
	
	/**
	 * Obtiene la ubicacion amis
	 * @param pais
	 * @param estado
	 * @param ciudad
	 * @return
	 */
	@Override
	public AmisUbicacion convertirUbicacionAMIS(String pais, String estado, String ciudad) {
		AmisUbicacion ubicacionConvertida = null;
		
		List<AmisUbicacion> ubicacionList = this.convertirUbicacion(pais, estado, ciudad);	
				
		if (ubicacionList != null && !ubicacionList.isEmpty()) {			
			ubicacionConvertida = ubicacionList.get(0);
		}	
		
		return ubicacionConvertida;
	}
	
	/**
	 * Convierte el tipo robo a equivalencia Ocra
	 * @param tipoRobo
	 * @return
	 */
	private String convertirTipoRobo(String tipoRobo ){
		
		if( tipoRobo != null){
			tipoRobo = ( (tipoRobo.toUpperCase().trim().equals("CS_13")) ? "2":"1" );
		}else{
			tipoRobo = "1";
		}
		
		return tipoRobo ;
	}
	
	/**
	 * Convierte los datos de auto Midas a equivalencia Ocra 	
	 * @param idReporteCabina
	 * @return
	 */
	private Map<String,String> convertirAutoOcra(Long idReporteCabina){
				
		Map<String, String> respuesta = new LinkedHashMap<String, String>();

		try{			
			IncisoReporteCabina incisoReporteCabina = polizaSiniestroService.obtenerIncisoAsignadoAlReporte(idReporteCabina);
			respuesta = this.convertirAutoOcra(incisoReporteCabina);
		}catch(Exception e){
			log.error("Error al convertirAutoOcra ("+idReporteCabina+") Error:"+e);
		}
		
		return respuesta;
	}
	
	/**
	 * Convierte los datos de auto Midas a equivalencia Ocra 	
	 * @param incisoReporteCabina
	 * @return
	 */
	public Map<String,String> convertirAutoOcra(IncisoReporteCabina incisoReporteCabina){
		Map<String, String> respuesta = new LinkedHashMap<String, String>();
		Long midarMarcaId    = 0L;
		String midasEstiloId = "";
		
		try{			
			respuesta.put("numMotor" , limpiarString(incisoReporteCabina.getAutoIncisoReporteCabina().getNumeroMotor()));
			respuesta.put("anio"     , incisoReporteCabina.getAutoIncisoReporteCabina().getModeloVehiculo().toString() );
			
			// # OBTENER DATOS DEL AUTO MEDIANTE EL FILTRO-DTO			
			midarMarcaId   = incisoReporteCabina.getAutoIncisoReporteCabina().getMarcaId().longValue();
			midasEstiloId = obtenerEstiloId(incisoReporteCabina.getAutoIncisoReporteCabina().getEstiloId() );
			
			Map<String, Object> params    = new LinkedHashMap<String, Object>();	
			params.put("midasMarcaId", midarMarcaId  );
			params.put("midasEstiloId",midasEstiloId );

			List<AmisConversionEstilo> lEquivalenciaOcra = this.entidadService.findByProperties(AmisConversionEstilo.class, params);
			
			if( !lEquivalenciaOcra.isEmpty() ){
				respuesta.put("marcaId",       lEquivalenciaOcra.get(0).getId().getAmisMarcaId().toString() );
				respuesta.put("tipoId",       lEquivalenciaOcra.get(0).getId().getAmisSubMarcaId().toString() );
				respuesta.put("transporteId", lEquivalenciaOcra.get(0).getId().getAmisTipoTransporteId().toString() );
			}

		}catch(Exception e){
			log.error("Error al convertirAutoOcra MarcaId:"+midarMarcaId+" midasEstiloId:"+midasEstiloId+" Error:"+e);
		}
		
		return respuesta;		
	}
	
	/**
	 * Limpia el STRING de caracteres especiales
	 * @param dato
	 * @return
	 */
	private String limpiarString(String dato){
		
		dato = dato.trim().replace(" ","").replace("/","").replace(".","").replace(",","").replace("-","");
		
		if( dato.trim().equals("-") ){
			dato = "NA";
		}
		
		return dato;
	}
	
	/**
	 * Convierte el estilo Midas al Id correspondiente en ocra, se recibe un String concatenado y se obtiene la parte de enmedio 
	 * @param estiloId
	 * @return
	 */
	private String obtenerEstiloId(String estiloId){
		
		try{
			String[] aEstiloId = estiloId.split("_");
			if( aEstiloId.length > 0 ){
				return String.format("%05d", Integer.parseInt(aEstiloId[1]));
			}else{
				return "";
			}
		}catch(Exception e){
			log.error("Error al obtener el estilo del vehiculo ("+estiloId+") error: "+ e);
			return "";
		}
		
	}
	
	/**
	 * Obtiene si el entorno es desarrollo(0) o produccion(1)
	 * @return
	 */
	/*private String getConfiguracionOcra(String llaveParametroGlobal){
		
		try{
			
			ParametroGlobal parametroGlobal = this.parametroGlobalService.obtenerPorIdParametro( ParametroGlobalService.AP_MIDAS , llaveParametroGlobal );
			
			if( parametroGlobal!=null ){
				return parametroGlobal.getValor();
			}else{
				return "";
			}

		}catch(Exception e){
			log.error("Error en la busqueda de parámero general: "+e);
			// # EN CASO DE ERROR RETORNA SIEMPRE DESARROLLO
			return "";
		}
		
	}*/
	
	private AutoIncisoReporteCabina obtenerAutoIncisoByReporteCabina( Long keyReporteCabina ){
		
		List<AutoIncisoReporteCabina> lAutoIncisoReporteCabina = null;
		
		try{
			Map<String,Object> params = new LinkedHashMap<String,Object>();
			params.put("incisoReporteCabina.seccionReporteCabina.reporteCabina.id", keyReporteCabina);
			
			lAutoIncisoReporteCabina = this.entidadService.findByProperties(AutoIncisoReporteCabina.class, params);
			
			if( !lAutoIncisoReporteCabina.isEmpty() ){
				return lAutoIncisoReporteCabina.get(0);
			}else{
				return null;
			}
		}catch(Exception e){
			return null;
		}
		
	}
	

	@Override
	public String reenvioAltaRoboOCRAServicio() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void guardaLog(String msnEnvio, String msnRespuesta, Date fEnvio,
			Date fRespueta,Long reporteCabina, String observaciones) {
		
		EnvioOcraLog envioOcraLog = new EnvioOcraLog(msnEnvio,msnRespuesta,fEnvio,fRespueta, reporteCabina,observaciones);
		this.entidadService.save(envioOcraLog);
		
	}
	
	private Date getFecha(){
		return new Date();
	}
	
	private String obtenerCredecialesOcra(String tipo){
		
		try{
			
			ParametroGlobal parametroGlobal = this.parametroGlobalService.obtenerPorIdParametro( ParametroGlobalService.AP_MIDAS , tipo);
			
			if( parametroGlobal!=null ){
				return parametroGlobal.getValor();
			}else{
				return "";
			}

		}catch(Exception e){
			log.error("Error en la busqueda de parámero general para user y pass ocra tipo: "+tipo+" : "+e);
			// # EN CASO DE ERROR RETORNA SIEMPRE DESARROLLO
			return "";
		}
	}


	@SuppressWarnings("rawtypes")
	@Override
	public TransporteCommDTO enviar(TransporteCommDTO comm)
			throws CommException {

		log.debug("OCRA - COMENZANDO ENVIO ");
		
		AltaRoboBean altaRoboBean = (AltaRoboBean) comm.getObjetoASerializar();
		RoboWSPortProxy roboWSPortProxy = new RoboWSPortProxy();
		
		// # OBTIENE ENDPOINT DE LA BASE DE DATOS
		roboWSPortProxy._getDescriptor().setEndpoint( this.obtenerCredecialesOcra(ParametroGlobalService.WS_OCRA) );
	
		try{
			List<MensajeBean> lMensaje = roboWSPortProxy.altaRobo(altaRoboBean);
			this.validarRespuesta(lMensaje,altaRoboBean);
		}catch(Exception e){
			log.info("OCRA - ERROR ENVIO ");
			e.printStackTrace();
			if( e instanceof TimeoutException ){
				log.info("OCRA - ERROR ENVIO TimeoutException fallo");
				throw new CommException(CODE.COMM_ERROR, "TimeoutException fallo:  "+this.imprimirTrazaError(e));
			}else if ( e instanceof MarshalException ){
				log.info("OCRA - ERROR ENVIO MarshalException fallo");
				throw new CommException(CODE.DATA_ERROR, "MarshalException fallo:  "+this.imprimirTrazaError(e));
			}else if ( e instanceof ConnectException ){
				log.info("OCRA - ERROR ENVIO ConnectException fallo");
				throw new CommException(CODE.COMM_ERROR, "ConnectException fallo:  "+this.imprimirTrazaError(e));
			}else if ( e instanceof SOAPFaultException ){
				log.info("OCRA - ERROR ENVIO SOAPFaultException fallo");
				throw new CommException(CODE.COMM_ERROR, "SOAPFaultException fallo: "+this.imprimirTrazaError(e));
			}else{
				log.info("OCRA - ERROR ENVIO Socket fallo ");
				throw new CommException(CODE.GENERAL_ERROR, "Socket fallo: "+this.imprimirTrazaError(e));
			}
		}
		
		return comm;
	}
	
	private void validarRespuesta(List<MensajeBean> lMensaje,AltaRoboBean altaRoboBean){
		
		String mensajeGerente = "";
		Date fechaInicial = this.getFecha();
		Date fechaFinal = this.getFecha();
		ReporteCabina reporte = entidadService.findById(ReporteCabina.class, this.xIdReporteCabina);
		
		if( !lMensaje.isEmpty()){
			
			mensajeGerente = obtenerMensajeGerente(lMensaje);
			
			// # NOTIFICAR A GERENTE ERROR
			mensajeGerente += ". Datos del siniestro: no de serie: "+altaRoboBean.getNoSerie()+" . Placa: "+altaRoboBean.getPlaca()+" . no siniestro: "+altaRoboBean.getNumSiniestro();
			this.guardaLog(altaRoboBean.toString(),mensajeGerente,fechaInicial,fechaFinal,reporte.getId(),"Petición colocada en Ocra");
			
			try{
				if( mensajeGerente.contains("ERROR") ){
					notificaErrorOcra(reporte,mensajeGerente);
				}	
			
			}catch (Exception e){
				this.guardaLog(altaRoboBean.toString(),mensajeGerente,fechaInicial,fechaFinal,reporte.getId(),"Error notificacion");
				log.error(e);
			}
		}
	}
	
	public String obtenerMensajeGerente(List<MensajeBean> lMensaje){
		StringBuilder mensajeGerente = new StringBuilder("");
		for( MensajeBean msn:lMensaje){

			if( msn.getId() == 0){
				mensajeGerente.append("OK | ");
				break;
			}else{
				mensajeGerente.append("ERROR | ").append(msn.getId()).append(" ").append(msn.getDescripcion()).append(" /// ");
			}
		}
		return mensajeGerente.toString();
	}
	

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public TransporteCommDTO init(Object e) {
		
		TransporteCommDTO comm = new TransporteCommDTO<T>();
		
		try{
			
			AltaRoboBean altaRoboBean = (AltaRoboBean) e;
			
			comm.setFolio( altaRoboBean.getNumSiniestro() );
			comm.setTipoProceso(TIPO_PROCESO.OCRA_ALTA_ROBO);
			comm.setTipoComm(TIPO_COMM.WS);
			comm.setUsuario(usuarioService.getUsuarioActual());
			comm.setObjetoASerializar(e);
			
			//conf notificacion
			comm.setCodigoNotificacion(EnumCodigo.ERROR_ENVIO_OCRA);
			Map<String, Serializable> datosNotificacion = new HashMap<String, Serializable>();
			datosNotificacion.put("folio", comm.getFolio());		
			comm.setDatosNotificacion(datosNotificacion);
			log.error("OCRA - Se ha cargado datos al mapa de datosNotificacion ");
			
			
			
			this.generarInstanciaCommService();
		
		}catch(Exception ex){
			log.error("OCRA - error init TransporteCommDTO ");
		}
		
		return comm;
	}
	
	@SuppressWarnings("null")
	@Override
	public boolean isPendienteOcra(String numReporte){
		
		boolean bandera = false;
		
		try{
			
			//this.guardaLog("*isPENDIENTEOCRA buscar por: "+numReporte,"",this.getFecha(),this.getFecha(),0l,"");
			
			if( numReporte!= null || !numReporte.equals("") ){
				
				CommBitacora bitacora = commBitacoraService.obtenerBitacora(numReporte);
				if ( bitacora != null ){
					if(bitacora.getStatus().equals(STATUS_BITACORA.ERROR.name() ) || bitacora.getStatus().equals(STATUS_BITACORA.CANCELADO.name() )){
						return true;
					}
				}
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
			log.error("OCRA - error al isPendienteOcra");
		}
		
		return bandera;
	}
	
	@Override
	public String imprimirTrazaError(Exception e){
		
		Writer writer = new StringWriter();
		PrintWriter printWriter = new PrintWriter(writer);
		e.printStackTrace(printWriter);
		return writer.toString();
		
	}
	
	/***
	 * Se usa solo para inicializar el Comm service cuando un error sucede antes de enviarse el WS, por ejemplo un nullpointer y entra al catch
	 * @param idReporteCabina
	 * @return
	 */
	private TransporteCommDTO<?> setInitComm(Long idReporteCabina) {
		
		TransporteCommDTO<?> comm = new TransporteCommDTO<T>();
		
		// OBTENER REPORTE CABINA PARA PASARLO COMO FOLIO
		ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, idReporteCabina);
		
		comm.setFolio( reporte.getNumeroReporte() );
		comm.setTipoProceso(TIPO_PROCESO.OCRA_ALTA_ROBO);
		comm.setTipoComm(TIPO_COMM.WS);
		comm.setUsuario(usuarioService.getUsuarioActual());
		
		this.generarInstanciaCommService();
		
		return comm;
		
	}


	@SuppressWarnings("unchecked")
	private void generarInstanciaCommService() {
		super.processor = super.sessionContext.getBusinessObject(
				EnvioOcraService.class);	
	}
	
	
	private void notificaErrorOcra(ReporteCabina reporte, String mensajeGerente) {
	
		// # SE USA PARA SETEAR EL ERROR EN LA NOTIFICACION EN CASO QUE APLIQUE
		reporte.setError(mensajeGerente);
		envioNotificacionesService.notificacionErrorOCRA(reporte, mensajeGerente);
		
		// NOTIFICACION DIRECTA A RESPONSABLE OCRA
		this.notificaResponsableOcra(this.xIdReporteCabina,mensajeGerente);
		
	}
	
	private void notificaResponsableOcra(Long idReporteCabina,String mensaje) {
		
		try {
		
			// OBTENER CORREO DE USUARIO DE PARAMETRO GLOBAL SERVICE
			String correo = this.obtenerCredecialesOcra(ParametroGlobalService.WS_CORREO_OCRA_RESPONSABLE);
			
			if( !StringUtil.isEmpty(correo) ) {
				
				ReporteCabina reporte = this.entidadService.findById(ReporteCabina.class, idReporteCabina);
				StringBuilder errorCtg = new StringBuilder();
				
				// VALIDAR SI OCRA REGRESA UN MENSAJE DE ERROR ASOCIADO A UN CATALOGO
				if( validaErrorCatalogoOcraMidas(mensaje) ) {
					
					errorCtg.append("<p ALIGN='JUSTIFY'>");
					errorCtg.append("<b><font color='red'>");
					errorCtg.append(" IMPORTANTE: Hay un error asociado a la conversi&oacute;n de cat&aacute;logos Ocra - Midas ");
					errorCtg.append(" favor de actualizar los mismos o crear el reporte de forma manual en el portal de Ocra. ");
					errorCtg.append(" </br> ");
					errorCtg.append(" Los errores asociados a cat&aacute;logos son: id_tipo, id_municipio e id_color ");
					errorCtg.append("</font></b>");
					errorCtg.append("</p>");
					
				}
				
				EmailDestinaratios destinatarios = new EmailDestinaratios();
		        destinatarios.agregar(EnumModoEnvio.PARA, correo , NOMBRE_RESPONSABLE_OCRA ); 
		        
		        HashMap<String, Serializable> dataArg = new HashMap<String, Serializable>();
		        dataArg.put("reporte", reporte);
				dataArg.put("oficinaId", reporte.getOficina().getId());
				dataArg.put("error", mensaje );
				dataArg.put("errorCtg", errorCtg );
				
		        
		        this.notificacionService.enviarNotificacion(
						EnvioNotificacionesService.EnumCodigo.ERROR_ENVIO_OCRA.toString() , 
						dataArg , 
						"ERROR ENVIO OCRA" , destinatarios); 	
			}
		
		}catch(Exception e) {
			log.error("Error Ocra notificaResponsableOcra :"+this.imprimirTrazaError(e));
		}
		
	}
	
	
	/*
	 * Valida si en el mensaje de error que se recibe de ocra viene alguno vinculado
	 * a algun catalogo en específico id_tipo, id_municipio, id_color, si es así, agrega texto adicional al correo.
	 */
	private boolean validaErrorCatalogoOcraMidas(String mensajeError) {
		
		if( mensajeError.contains("id_municipio") || mensajeError.contains("id_color") || mensajeError.contains("id_tipo") ) {
			return true;
		}else {
			return false;
		}
		
	}
	
	
	private int convierteStringToInt(String dato, String tipo) {
		
		try {
			return Integer.parseInt(dato);
		}catch(Exception e){
			this.errorConversion += " ["+tipo+"] dato:"+dato+" - Error de conversion/ ";
			return -1;
		}
	}

}
