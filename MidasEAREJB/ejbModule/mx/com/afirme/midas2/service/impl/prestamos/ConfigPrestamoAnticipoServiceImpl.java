package mx.com.afirme.midas2.service.impl.prestamos;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas2.dao.fortimax.CarpetaAplicacionFortimaxDao;
import mx.com.afirme.midas2.dao.fortimax.DocumentoCarpetaFortimaxDao;
import mx.com.afirme.midas2.dao.prestamos.ConfigPrestamoAnticipoDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CarpetaAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoAplicacionFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.CatalogoDocumentoFortimax;
import mx.com.afirme.midas2.domain.documentosGenericosFortimax.DocumentosAgrupados;
import mx.com.afirme.midas2.domain.prestamos.ConfigPrestamoAnticipo;
import mx.com.afirme.midas2.dto.fuerzaventa.EntregoDocumentosView;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.prestamos.ConfigPrestamoAnticipoView;
import mx.com.afirme.midas2.dto.prestamos.ReporteIngresosAgente;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.fortimax.FortimaxService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.prestamos.ConfigPrestamoAnticipoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.emision.ImpresionesService;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.util.MidasException;
import mx.com.afirme.midas2.utils.MailServiceSupport;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.lang.StringUtils;

@Stateless
public class ConfigPrestamoAnticipoServiceImpl implements ConfigPrestamoAnticipoService{
	@EJB
	private MailService mailService;
	@EJB
	private AgenteMidasService agenteMidasService;
	@EJB
	private ImpresionesService impresionesService;
	private ConfigPrestamoAnticipoDao dao;
	
	@EJB
	private DocumentoCarpetaFortimaxDao documentoCarpetaFortimaxDao;
	
	@EJB
	private CarpetaAplicacionFortimaxDao carpetaAplicacionFortimaxDao;
	
	@EJB
	private FortimaxService fortimaxService;
	
	@EJB
	private EntidadService entidadService;
		
	private static String regEx;
	
	private Predicate nombreDocumentoPredicate = new Predicate() {
		@Override
		public boolean evaluate(Object arg0) {
			
			Matcher matcher;
							
			Pattern pattern = Pattern.compile(ConfigPrestamoAnticipoServiceImpl.getRegEx());
			
			if (arg0 instanceof DocumentosAgrupados) {
				matcher = pattern.matcher(((DocumentosAgrupados)arg0).getNombre());
			} else if (arg0 instanceof String) {
				matcher = pattern.matcher((String)arg0);
			} else if (arg0 instanceof EntregoDocumentosView) {
				matcher = pattern.matcher(((EntregoDocumentosView)arg0).getValor());
			} else {
				return false;
			}
			
			return matcher.find();
		}
	};
	
	
	@Override
	public List<ConfigPrestamoAnticipoView> findByFilters(ConfigPrestamoAnticipo obj) throws Exception {
		// TODO Auto-generated method stub
		return dao.findByFilters(obj);
	}

	@Override
	public ConfigPrestamoAnticipo loadById(ConfigPrestamoAnticipo obj)	throws Exception {
		// TODO Auto-generated method stub
		return dao.loadById(obj);
	}

	@Override
	public ConfigPrestamoAnticipo save(ConfigPrestamoAnticipo obj)	throws Exception {
		// TODO Auto-generated method stub
		return dao.save(obj);
	}
	
	@Override
	public ConfigPrestamoAnticipo updateEstatus(ConfigPrestamoAnticipo configPrestamoAnticipo, String elementoCatalogo)throws Exception{
		return dao.updateEstatus(configPrestamoAnticipo, elementoCatalogo);
	}
	
	@Override
	public ReporteIngresosAgente obtenerDatosReporte(ConfigPrestamoAnticipo configPrestamoAnticipo) throws Exception{
		return dao.obtenerDatosReporte(configPrestamoAnticipo);
	}
	
	@Override
	public ConfigPrestamoAnticipo autorizarMovimiento(ConfigPrestamoAnticipo config) throws Exception {
		
		validaDocumentosDigitalizados(config);
		
		return dao.autorizarMovimiento(config);
	}
	
	@Override 
	public Map<String,Map<String, List<String>>> obtenerCorreos(Long id, Long idProceso,
			Long idMovimiento) {
		return agenteMidasService.obtenerCorreos(id, idProceso, idMovimiento);
	}
	
	@Override
	public void enviarCorreo(Long id,Map<String,Map<String, List<String>>> mapCorreos,
			String mensaje,String tituloMensaje, int tipoTemplate) {
		List<ByteArrayAttachment> attachment = null;
		ConfigPrestamoAnticipo prestamoAnticipo = new ConfigPrestamoAnticipo();
		if(id!=null){
			prestamoAnticipo = dao.findById(ConfigPrestamoAnticipo.class, id);
		}
		if (tituloMensaje.equalsIgnoreCase("ALTAREGISTRO")) {
			mensaje = MailServiceSupport.mensajePrestamoAnticipoAltaRegistro(mensaje, prestamoAnticipo.getImporteOtorgado());
		}else if(tituloMensaje.equalsIgnoreCase("AUTORIZACION")) {
			mensaje = MailServiceSupport.mensajePrestamoAnticipoAltaAutorizacionMov(mensaje, prestamoAnticipo.getImporteOtorgado());
			TransporteImpresionDTO transporteImpresionDTO = impresionesService
					.imprimirTablaAmortizacion(prestamoAnticipo, new Locale("es","MX"));
			if (transporteImpresionDTO != null && transporteImpresionDTO.getByteArray() !=  null) {
				ByteArrayAttachment arrayAttachment = new ByteArrayAttachment();
				arrayAttachment.setContenidoArchivo(transporteImpresionDTO.getByteArray());
				arrayAttachment.setNombreArchivo("tablaAmortizacion.xls");
				arrayAttachment.setTipoArchivo(ByteArrayAttachment.TipoArchivo.XLS);
				attachment = new ArrayList<ByteArrayAttachment>();
				attachment.add(arrayAttachment);
			}
		}else {
			mensaje = MailServiceSupport.mensajePrestamoAnticiporechazoMov(mensaje, prestamoAnticipo.getImporteOtorgado());
		}
		for (Entry<String, Map<String, List<String>>> map : mapCorreos
				.entrySet()) {
			StringBuilder message = new StringBuilder(mensaje);
			if (mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0) != null && StringUtils.isNotBlank(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0))) {
				message.append(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0)); 
			} 
			mailService.sendMailAgenteNotificacion(mapCorreos.get(map.getKey())
					.get(GenericMailService.PARA), mapCorreos.get(map.getKey()).get(GenericMailService.COPIA),
					mapCorreos.get(map.getKey()).get(GenericMailService.COPIA_OCULTA), tituloMensaje,
					message.toString().replace(" null ", " "), attachment, "Notificacion", null, tipoTemplate);
		}
	}
	
	@Override
	public void enviarCorreo(Map<String, Map<String, List<String>>> mapCorreos,
			String mensaje, String tituloMensaje, int tipoTemplate,List<ByteArrayAttachment> attachment) {
		for (Entry<String, Map<String, List<String>>> map : mapCorreos
				.entrySet()) {
			StringBuilder message = new StringBuilder(mensaje);
			if (mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0) != null && StringUtils.isNotBlank(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0))) {
				message.append(mapCorreos.get(map.getKey()).get(GenericMailService.NOTA).get(0)); 
			} 
			mailService.sendMailAgenteNotificacion(mapCorreos.get(map.getKey())
					.get(GenericMailService.PARA), mapCorreos.get(map.getKey()).get(GenericMailService.COPIA),
					mapCorreos.get(map.getKey()).get(GenericMailService.COPIA_OCULTA), tituloMensaje,
					message.toString().replace(" null ", " "), null, "Notificacion", null, tipoTemplate);
		}
	}

	@Override
	public void mailThreadMethodSupport(Long id, Long idProceso,
			Long idMovimiento, String mensaje, int tipoTemplate,
			String methodToExcecute) {
		// TODO Auto-generated method stub
		
	}
	
	@Override
	public ConfigPrestamoAnticipo solicitarChequeDelMovimiento(ConfigPrestamoAnticipo config) throws Exception {
		return dao.solicitarChequeDelMovimiento(config);
	}
	
	@Override
	public List<EntregoDocumentosView> consultaEstatusDocumentos(Long idPrestamo,Long idAgente) throws Exception {
		return dao.consultaEstatusDocumentos(idPrestamo,idAgente);
	}

	@SuppressWarnings("unchecked")
	@Override
	public void crearYGenerarDocumentosFortimax(ConfigPrestamoAnticipo config, Long numPagares) throws Exception {
				
		if (config == null) {
			throw new Exception("No se encuentra la configuracion");
		} 
		
		if (config.getId() == null) {	
			throw new Exception("No se encuentra el id de la configuracion");
		}
		
		if (config.getAgente() == null || config.getAgente().getId() == null) {
			throw new Exception("No se indico el id del agente");
		}
		
		if (numPagares == null) {
			throw new Exception("No se indico el numero de pagares a generar");
		}
						
		List<CatalogoDocumentoFortimax> catalogoDocumentos;
		List<DocumentosAgrupados> documentosOriginales;
		List<String> documentosFortimax;
		List<DocumentosAgrupados> documentosOriginalesFiltrado;
		List<DocumentosAgrupados> documentosOriginalesAEliminar;
		List<String> documentosFortimaxFiltrado;
		CarpetaAplicacionFortimax carpeta;
		StringBuilder nombreDocumentosGenerados = new StringBuilder();
		String nombreDocumentoGenerado;
		DocumentosAgrupados documento;
		String prefijo;
		Long idDocumentoPagare = null;
		String cadPagare = "PAGARE";
		String prefijoTipoMovimiento = "PRES";
				
		config.setAgente(entidadService.getReference(Agente.class, config.getAgente().getId()));
		
		carpeta = getCarpetaPorTipoMovimiento (config);
		
		if (carpeta.getNombreCarpeta().trim().equalsIgnoreCase(ConfigPrestamoAnticipo.CARPETA_ANTICIPOS)) {
			prefijoTipoMovimiento = "ANTI";
		}
		
		prefijo = prefijoTipoMovimiento + config.getId() + "_" + config.getAgente().getIdAgente() + "_";
		
		catalogoDocumentos = documentoCarpetaFortimaxDao
			.obtenerDocumentosPorCarpetaConTipoPersona(CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX, 
					carpeta.getNombreCarpeta(), config.getAgente().getPersona().getClaveTipoPersona());
		
		//Se crea la carpeta del agente en caso de no existir
		agenteMidasService.generateExpedientAgent(config.getAgente().getId());
		
		//Lista de documentos que estan dados de alta en el portal de Fortimax
		documentosFortimax = Arrays.asList(fortimaxService
				.getDocumentFortimax(config.getAgente().getId(), CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX));
		
		documentosOriginales = dao.documentosFortimaxGuardadosDB(config.getId(), config.getAgente().getId());
		
		for (CatalogoDocumentoFortimax documentoCatalogo : catalogoDocumentos) {
						
			ConfigPrestamoAnticipoServiceImpl.setRegEx("^" + prefijo + "(.)*" + documentoCatalogo.getNombreDocumentoFortimax().substring(0,5) 
					+ "(.)*[^0-9]$");
			nombreDocumentosGenerados.delete(0,nombreDocumentosGenerados.length());
			if (documentoCatalogo.getNombreDocumentoFortimax().substring(0,6).equalsIgnoreCase(cadPagare)) {
				
				//Se utilizara para los pagares mas adelante
				idDocumentoPagare = documentoCatalogo.getId();
				
				nombreDocumentosGenerados.append(cadPagare).append(" PRINCIPAL");
				
			} else {
				nombreDocumentosGenerados.append(documentoCatalogo.getNombreDocumentoFortimax());
			}
			nombreDocumentoGenerado = nombreDocumentosGenerados.insert(0,prefijo).toString();
			if (!CollectionUtils.exists(documentosFortimax, nombreDocumentoPredicate)) {
				
				fortimaxService.generateDocument(config.getAgente().getId(), 
						CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX,	nombreDocumentoGenerado , carpeta.getNombreCarpetaFortimax());
				
			}
			
			if (!CollectionUtils.exists(documentosOriginales, nombreDocumentoPredicate)) {
				
				documento = new DocumentosAgrupados();
				documento.setIdAgrupador(null);
				documento.setIdDocumento(documentoCatalogo.getId());
				documento.setIdEntidad(config.getAgente().getId());
				documento.setNombre(nombreDocumentoGenerado);
				documento.setExisteDocumento(0);
				entidadService.save(documento);
				
			}
			
		}
		
		//Pagares
		
		nombreDocumentoGenerado = prefijo + cadPagare + "_";
		                          
		ConfigPrestamoAnticipoServiceImpl.setRegEx("^" + nombreDocumentoGenerado + "(.)*[0-9]$");
		
		documentosFortimaxFiltrado = (List<String>) CollectionUtils.select(documentosFortimax, nombreDocumentoPredicate);
		
		if (documentosFortimaxFiltrado.size() < numPagares.intValue()) {
			
			for (int i = documentosFortimaxFiltrado.size() + 1; i <= numPagares.intValue(); i++) {
				
				fortimaxService.generateDocument(config.getAgente().getId(), 
						CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX,	nombreDocumentoGenerado + i , carpeta.getNombreCarpetaFortimax());
				
			}
			
		}
		//TODO: Lo anterior solo aplica cuando se deben agregar documentos a Fortimax. 
		//Para eliminar documentos de mas en Fortimax, primero pedir esa funcionalidad al proveedor 
		//y luego agregar la logica del borrado aqui
		
		documentosOriginalesFiltrado = (List<DocumentosAgrupados>) CollectionUtils.select(documentosOriginales, nombreDocumentoPredicate);
		
		if (documentosOriginalesFiltrado.size() < numPagares.intValue()) {
			
			for (int i = documentosOriginalesFiltrado.size() + 1; i <= numPagares.intValue(); i++) {
				
				documento = new DocumentosAgrupados();
				documento.setIdAgrupador(null);
				documento.setIdDocumento(idDocumentoPagare); //Se asume que ya debe estar seteado con el id de Documento del Pagare Principal
				documento.setIdEntidad(config.getAgente().getId());
				documento.setNombre(nombreDocumentoGenerado + i);
				documento.setExisteDocumento(0);
				entidadService.save(documento);	
							
			}
			
		} else if (documentosOriginalesFiltrado.size() > numPagares.intValue()) {
			
			for (int i = numPagares.intValue() + 1; i <= documentosOriginalesFiltrado.size(); i++) {
				
				ConfigPrestamoAnticipoServiceImpl.setRegEx("^" + nombreDocumentoGenerado + i + "$");
				
				documentosOriginalesAEliminar = (List<DocumentosAgrupados>) CollectionUtils
					.select(documentosOriginales, nombreDocumentoPredicate);
				
				if (documentosOriginalesAEliminar.size() > 0) {
					entidadService.remove(documentosOriginalesAEliminar.get(0));
				}
			}
			
		}
		
	}
	
	@Override
	public void auditarDocumentosEntregadosPrestamos(Long idPrestamo,Long idAgente, String nombreAplicacion) throws Exception {
		dao.auditarDocumentosEntregadosPrestamos(idPrestamo,idAgente, nombreAplicacion);			
	}
	
	@SuppressWarnings("unchecked")
	private void validaDocumentosDigitalizados(ConfigPrestamoAnticipo configPrestamoAnticipo) {
		try {
			
			List<CatalogoDocumentoFortimax> catalogoDocumentos;
			List<EntregoDocumentosView> documentosMovimiento;
			List<EntregoDocumentosView> documentosFiltradosMovimiento;
			
			configPrestamoAnticipo.setAgente(entidadService.getReference(Agente.class, configPrestamoAnticipo.getAgente().getId()));
			
			CarpetaAplicacionFortimax carpeta = getCarpetaPorTipoMovimiento (configPrestamoAnticipo);
			
			catalogoDocumentos = documentoCarpetaFortimaxDao
				.obtenerDocumentosPorCarpetaConTipoPersona(CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX, 
						carpeta.getNombreCarpeta(), configPrestamoAnticipo.getAgente().getPersona().getClaveTipoPersona());
		
			documentosMovimiento = consultaEstatusDocumentos(configPrestamoAnticipo.getId(), configPrestamoAnticipo.getAgente().getId());
			
			for (CatalogoDocumentoFortimax documentoCatalogo : catalogoDocumentos) {
				
				if (documentoCatalogo.getRequerido().intValue() == 1) {
					
					ConfigPrestamoAnticipoServiceImpl.setRegEx("^(.)*_" + documentoCatalogo.getNombreDocumentoFortimax().substring(0,5) + "(.)*$");
					
					documentosFiltradosMovimiento = (List<EntregoDocumentosView>) CollectionUtils
						.select(documentosMovimiento, nombreDocumentoPredicate);
					
					for (EntregoDocumentosView  documentoFiltradoMovimiento : documentosFiltradosMovimiento) {
						if (documentoFiltradoMovimiento.getChecado().intValue() == 0) {
							throw new RuntimeException("Hay documentos por digitalizar");
						}
					}
				}
			}
		
		} catch (Exception e) {
			e.printStackTrace();
			
			if (e instanceof RuntimeException) {
				throw (RuntimeException)e;
			}
			
			throw new RuntimeException(e.getMessage());
		}
	}
	
	private CarpetaAplicacionFortimax getCarpetaPorTipoMovimiento(ConfigPrestamoAnticipo configPrestamoAnticipo) {
		try {
			configPrestamoAnticipo = entidadService.getReference(ConfigPrestamoAnticipo.class, configPrestamoAnticipo.getId());
			String tipo = configPrestamoAnticipo.getTipoMovimiento().getValor().substring(0, 8);
			
			String nombreCarpeta = (tipo.equalsIgnoreCase(ConfigPrestamoAnticipo.CARPETA_PRESTAMOS.substring(0, 8))
					? ConfigPrestamoAnticipo.CARPETA_PRESTAMOS : ConfigPrestamoAnticipo.CARPETA_ANTICIPOS);
			
			return carpetaAplicacionFortimaxDao
				.obtenerCarpetaEspecificaPorAplicacion(CatalogoAplicacionFortimax.APLICACION_AGENTES_FORTIMAX, nombreCarpeta);
		} catch (MidasException e) {
			throw new RuntimeException(e);
		}
	}
	
	
	
	/************************setters & getters*******************************/

	@EJB
	public void setDao(ConfigPrestamoAnticipoDao dao) {
		this.dao = dao;
	}
		
	public static String getRegEx() {
		return regEx;
	}
	
	public static void setRegEx(String regEx) {
		ConfigPrestamoAnticipoServiceImpl.regEx = regEx;
	}
	
}
