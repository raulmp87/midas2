package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.inciso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.detalleprima.DetallePrimaCoberturaCotizacionId;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.riesgo.RiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.aumento.AumentoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.descuento.DescuentoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.detalleprima.DetallePrimaRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.riesgo.recargo.RecargoRiesgoCotizacionId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionDTOId;
import mx.com.afirme.midas.cotizacion.seccion.SeccionCotizacionFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.riesgo.RiesgoCoberturaFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.VarModifDescripcionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.tiposervicio.NegocioTipoServicioDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.cambiosglobales.IncisoAutoDao;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoService;

import org.springframework.beans.BeanUtils;

public class IncisoSoporteService {
	protected CatalogoValorFijoFacadeRemote catalogoValorFijoFacadeRemote;		
	protected VarModifDescripcionDao varModifDescripcionDao;
	protected IncisoCotizacionFacadeRemote incisoCotizacionFacadeLocal;
	protected RiesgoCotizacionFacadeRemote riesgoCotizacionFacadeRemote;
	protected CoberturaService coberturaService;
	protected SeccionFacadeRemote seccionFacadeRemote;
	protected SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote;
	protected EntidadService entidadService;
	protected CoberturaCotizacionFacadeRemote coberturaCotizacionFacadeRemote;
	protected NegocioSeccionService negocioSeccionService;
	protected RiesgoCoberturaFacadeRemote riesgoCoberturaFacadeLocal;
	protected CalculoService calculoService;
	protected DatoIncisoCotAutoService datoIncisoCotAutoService;
	protected ConfiguracionDatoIncisoService configuracionDatoIncisoService;
	protected IncisoAutoDao incisoAutoDao;
	protected NegocioTipoServicioDao negocioTipoServicioDao;
	protected ClienteFacadeRemote clienteFacadeRemote;
	protected NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	

	public void copiarEstructura(IncisoCotizacionDTO incisoNuevo,IncisoCotizacionDTO incisoBase){
		List<SeccionCotizacionDTO> listaSeccionCotizacion = incisoBase
		.getSeccionCotizacionList();
		List<SeccionCotizacionDTO> listaSeccionesNuevasCotizacion = incisoNuevo
		.getSeccionCotizacionList();		
		
		// Copia datos IncisoAutoCot
		if(incisoBase.getIncisoAutoCot() != null){			
			IncisoAutoCot auto = new IncisoAutoCot();
			BeanUtils.copyProperties(incisoBase.getIncisoAutoCot(), auto);
			auto.setId(null);
			auto.setIncisoCotizacionDTO(incisoNuevo);
			incisoNuevo.setIncisoAutoCot(auto);			
		}
		
		for (SeccionCotizacionDTO seccionCot : listaSeccionCotizacion) {
			// Copiar los registros SeccionCotizacion
			SeccionCotizacionDTO seccionCotizacionNueva = new SeccionCotizacionDTO();
			seccionCotizacionNueva.setId(new SeccionCotizacionDTOId());
			seccionCotizacionNueva.getId().setIdToCotizacion(
					incisoNuevo.getId().getIdToCotizacion());
			seccionCotizacionNueva.getId().setIdToSeccion(
					seccionCot.getId().getIdToSeccion());
			seccionCotizacionNueva.getId().setNumeroInciso(
					incisoNuevo.getId().getNumeroInciso());
			// Los datos de la SeccionCotizacion obtenida del incisoOriginal se
			// pasan al nuevo seccionCotizacion
			seccionCotizacionNueva.setValorPrimaNeta(seccionCot
					.getValorPrimaNeta());
			seccionCotizacionNueva.setValorPrimaNetaOriginal(seccionCot
					.getValorPrimaNetaOriginal());
			seccionCotizacionNueva.setValorSumaAsegurada(seccionCot
					.getValorSumaAsegurada());
			seccionCotizacionNueva.setClaveObligatoriedad(seccionCot
					.getClaveObligatoriedad());
			seccionCotizacionNueva.setClaveContrato(seccionCot
					.getClaveContrato());
			seccionCotizacionNueva.setIncisoCotizacionDTO(incisoNuevo);
			seccionCotizacionNueva.setSeccionDTO(seccionCot.getSeccionDTO());
			// Se guarda el nuevo registro de SeccionCotizacion correspondiente
			// al nuevo inciso
			listaSeccionesNuevasCotizacion.add(seccionCotizacionNueva);
			// seccionCotizacionFacade.save(seccionCotizacionNueva);

			List<CoberturaCotizacionDTO> listaCoberturaCot = seccionCot
					.getCoberturaCotizacionLista();
			
			List<CoberturaCotizacionDTO> coberturasNuevasCotizacion = new ArrayList<CoberturaCotizacionDTO>();
			
			// Copia las Coberturas
			for (CoberturaCotizacionDTO coberturaCot : listaCoberturaCot) {
				// Se recuperan los registros CoberturaCotizacion del inciso
				// Original
				CoberturaCotizacionDTO coberturaCotizacionNueva = new CoberturaCotizacionDTO();
				coberturaCotizacionNueva.setId(new CoberturaCotizacionId());
				coberturaCotizacionNueva.getId().setIdToCobertura(
						coberturaCot.getId().getIdToCobertura());
				coberturaCotizacionNueva.getId().setIdToCotizacion(
						incisoNuevo.getId().getIdToCotizacion());
				coberturaCotizacionNueva.getId().setIdToSeccion(
						coberturaCot.getId().getIdToSeccion());
				coberturaCotizacionNueva.getId().setNumeroInciso(
						incisoNuevo.getId().getNumeroInciso());
				coberturaCotizacionNueva
						.setSeccionCotizacionDTO(seccionCotizacionNueva);
				coberturaCotizacionNueva.setCoberturaSeccionDTO(coberturaCot
						.getCoberturaSeccionDTO());
				coberturaCotizacionNueva.setValorPrimaDiaria(coberturaCot.getValorPrimaDiaria());
				// Al nuevo registro se le establecen los valores del registro
				// perteneciente al inciso original
				coberturaCotizacionNueva.setIdTcSubramo(coberturaCot
						.getIdTcSubramo());
				coberturaCotizacionNueva.setValorPrimaNeta(coberturaCot
						.getValorPrimaNeta());
				coberturaCotizacionNueva.setValorPrimaNetaOriginal(coberturaCot
						.getValorPrimaNetaOriginal());
				coberturaCotizacionNueva.setValorSumaAsegurada(coberturaCot
						.getValorSumaAsegurada());
				coberturaCotizacionNueva.setValorCoaseguro(coberturaCot
						.getValorCoaseguro());
				coberturaCotizacionNueva.setValorDeducible(coberturaCot
						.getValorDeducible());
				coberturaCotizacionNueva.setClaveObligatoriedad(coberturaCot
						.getClaveObligatoriedad());
				coberturaCotizacionNueva.setValorCuota(coberturaCot
						.getValorCuota());
				coberturaCotizacionNueva.setValorCuotaOriginal(coberturaCot
						.getValorCuotaOriginal());
				coberturaCotizacionNueva.setPorcentajeCoaseguro(coberturaCot
						.getPorcentajeCoaseguro());
				coberturaCotizacionNueva.setClaveContrato(coberturaCot
						.getClaveContrato());
				if (coberturaCot.getClaveAutCoaseguro().intValue() != 0)
					coberturaCotizacionNueva.setClaveAutCoaseguro((short) 1);
				else
					coberturaCotizacionNueva.setClaveAutCoaseguro((short) 0);
				coberturaCotizacionNueva.setPorcentajeDeducible(coberturaCot
						.getPorcentajeDeducible());
				if (coberturaCot.getClaveAutCoaseguro().intValue() != 0)
					coberturaCotizacionNueva.setClaveAutDeducible((short) 1);
				else
					coberturaCotizacionNueva.setClaveAutDeducible((short) 0);
				coberturaCotizacionNueva.setNumeroAgrupacion(coberturaCot
						.getNumeroAgrupacion());
				// Campos agregados el 26/01/2010
				coberturaCotizacionNueva.setClaveTipoDeducible(coberturaCot
						.getClaveTipoDeducible());
				coberturaCotizacionNueva
						.setClaveTipoLimiteDeducible(coberturaCot
								.getClaveTipoLimiteDeducible());
				coberturaCotizacionNueva
						.setValorMinimoLimiteDeducible(coberturaCot
								.getValorMinimoLimiteDeducible());
				coberturaCotizacionNueva
						.setValorMaximoLimiteDeducible(coberturaCot
								.getValorMaximoLimiteDeducible());

				coberturaCotizacionNueva.setIdToVersionAgrupadorTarifa(coberturaCot.getIdToVersionAgrupadorTarifa());
				coberturaCotizacionNueva.setIdVersionCarga(coberturaCot.getIdVersionCarga());
				coberturaCotizacionNueva.setIdVersionTarifa(coberturaCot.getIdVersionTarifa());
				coberturaCotizacionNueva.setIdTarifaExt(coberturaCot.getIdTarifaExt());
				coberturaCotizacionNueva.setIdVersionTarifaExt(coberturaCot.getIdVersionTarifaExt());
				
				coberturasNuevasCotizacion.add(coberturaCotizacionNueva);
				// coberturaCotizacionFacade.save(coberturaCotizacionNueva);
				
				List<DetallePrimaCoberturaCotizacionDTO> listaDetallePrimaCoberturaCotizacion = coberturaCot
						.getListaDetallePrimacoberturaCotizacion();
				List<DetallePrimaCoberturaCotizacionDTO> detallesPrimaCoberturaNuevaCotizacion = new ArrayList<DetallePrimaCoberturaCotizacionDTO>();
				
				// Copia el detalle de la prima Cobertura
				for (DetallePrimaCoberturaCotizacionDTO detallePrimaCoberturaCot : listaDetallePrimaCoberturaCotizacion) {
					DetallePrimaCoberturaCotizacionDTO detallePrimaCoberturaCotTMP = new DetallePrimaCoberturaCotizacionDTO();
					detallePrimaCoberturaCotTMP
							.setId(new DetallePrimaCoberturaCotizacionId());
					detallePrimaCoberturaCotTMP.getId()
							.setIdToCobertura(
									coberturaCotizacionNueva.getId()
											.getIdToCobertura());
					detallePrimaCoberturaCotTMP.getId().setIdToCotizacion(
							incisoNuevo.getId().getIdToCotizacion());
					detallePrimaCoberturaCotTMP.getId().setIdToSeccion(
							coberturaCotizacionNueva.getId().getIdToSeccion());
					detallePrimaCoberturaCotTMP.getId().setNumeroInciso(
							incisoNuevo.getId().getNumeroInciso());
					detallePrimaCoberturaCotTMP.getId().setNumeroSubInciso(
							detallePrimaCoberturaCot.getId()
									.getNumeroSubInciso());
					detallePrimaCoberturaCotTMP
							.setCoberturaCotizacionDTO(coberturaCotizacionNueva);
					detallePrimaCoberturaCotTMP
							.setValorCuota(detallePrimaCoberturaCot
									.getValorCuota());
					detallePrimaCoberturaCotTMP
							.setValorCuotaARDT(detallePrimaCoberturaCot
									.getValorCuotaARDT());
					detallePrimaCoberturaCotTMP
							.setValorCuotaARDV(detallePrimaCoberturaCot
									.getValorCuotaARDV());
					detallePrimaCoberturaCotTMP
							.setValorCuotaB(detallePrimaCoberturaCot
									.getValorCuotaB());
					detallePrimaCoberturaCotTMP
							.setValorPrimaNeta(detallePrimaCoberturaCot
									.getValorPrimaNeta());
					detallePrimaCoberturaCotTMP
							.setValorPrimaNetaARDT(detallePrimaCoberturaCot
									.getValorPrimaNetaARDT());
					detallePrimaCoberturaCotTMP
							.setValorPrimaNetaB(detallePrimaCoberturaCot
									.getValorPrimaNetaB());

					detallesPrimaCoberturaNuevaCotizacion
							.add(detallePrimaCoberturaCotTMP);
					// detallePrimaCoberturaCotizacionFacade.save(detallePrimaCoberturaCotTMP);
				}
				coberturaCotizacionNueva
						.setListaDetallePrimacoberturaCotizacion(detallesPrimaCoberturaNuevaCotizacion);

				// Copiar los registros de DetallePrimaCoberturaCotizacion Por
				// cada cobertura se obtienen los riesgos
				List<RiesgoCotizacionDTO> listaRiesgoCotizacion = coberturaCot
						.getRiesgoCotizacionLista();
				List<RiesgoCotizacionDTO> riesgosNuevosCotizacion = new ArrayList<RiesgoCotizacionDTO>();
				
				for (RiesgoCotizacionDTO riesgoCot : listaRiesgoCotizacion) {
					RiesgoCotizacionDTO riesgoCotizacionNuevo = new RiesgoCotizacionDTO();
					riesgoCotizacionNuevo.setId(new RiesgoCotizacionId());
					riesgoCotizacionNuevo.getId().setIdToCobertura(
							riesgoCot.getId().getIdToCobertura());
					riesgoCotizacionNuevo.getId().setIdToCotizacion(
							incisoNuevo.getId().getIdToCotizacion());
					riesgoCotizacionNuevo.getId().setIdToRiesgo(
							riesgoCot.getId().getIdToRiesgo());
					riesgoCotizacionNuevo.getId().setIdToSeccion(
							riesgoCot.getId().getIdToSeccion());
					riesgoCotizacionNuevo.getId().setNumeroInciso(
							incisoNuevo.getId().getNumeroInciso());

					// Al nuevo registro se le establecen los valores del
					// registro original
					riesgoCotizacionNuevo.setRiesgoCoberturaDTO(riesgoCot
							.getRiesgoCoberturaDTO());
					riesgoCotizacionNuevo
							.setCoberturaCotizacionDTO(coberturaCotizacionNueva);
					riesgoCotizacionNuevo.setIdTcSubramo(riesgoCot
							.getIdTcSubramo());
					riesgoCotizacionNuevo.setValorSumaAsegurada(riesgoCot
							.getValorSumaAsegurada());
					riesgoCotizacionNuevo.setValorCoaseguro(riesgoCot
							.getValorCoaseguro());
					riesgoCotizacionNuevo.setValorDeducible(riesgoCot
							.getValorDeducible());
					if (riesgoCot.getClaveAutCoaseguro().intValue() != 0)
						riesgoCotizacionNuevo.setClaveAutCoaseguro((short) 1);
					else
						riesgoCotizacionNuevo.setClaveAutCoaseguro((short) 0);
					if (riesgoCot.getClaveAutDeducible().intValue() != 0)
						riesgoCotizacionNuevo.setClaveAutDeducible((short) 1);
					else
						riesgoCotizacionNuevo.setClaveAutDeducible((short) 0);
					riesgoCotizacionNuevo.setClaveObligatoriedad(riesgoCot
							.getClaveObligatoriedad());
					riesgoCotizacionNuevo.setClaveContrato(riesgoCot
							.getClaveContrato());
					// TODO riesgoCotizacionNuevo.setClaveContrato((short)0);
					riesgoCotizacionNuevo.setValorAumento(riesgoCot
							.getValorAumento());
					riesgoCotizacionNuevo.setValorDescuento(riesgoCot
							.getValorDescuento());
					riesgoCotizacionNuevo.setValorRecargo(riesgoCot
							.getValorRecargo());
					riesgoCotizacionNuevo.setPorcentajeCoaseguro(riesgoCot
							.getPorcentajeCoaseguro());
					riesgoCotizacionNuevo.setPorcentajeDeducible(riesgoCot
							.getPorcentajeDeducible());
					riesgoCotizacionNuevo.setValorCuotaB(riesgoCot
							.getValorCuotaB());
					riesgoCotizacionNuevo.setValorCuotaARDT(riesgoCot
							.getValorCuotaARDT());
					riesgoCotizacionNuevo.setValorPrimaNeta(riesgoCot
							.getValorPrimaNeta());
					riesgoCotizacionNuevo.setValorPrimaNetaARDT(riesgoCot
							.getValorPrimaNetaARDT());
					riesgoCotizacionNuevo.setValorPrimaNetaB(riesgoCot
							.getValorPrimaNetaB());
					// Campos agregados el 26/01/2010
					riesgoCotizacionNuevo.setClaveTipoDeducible(riesgoCot
							.getClaveTipoDeducible());
					riesgoCotizacionNuevo.setClaveTipoLimiteDeducible(riesgoCot
							.getClaveTipoLimiteDeducible());
					riesgoCotizacionNuevo
							.setValorMinimoLimiteDeducible(riesgoCot
									.getValorMinimoLimiteDeducible());
					riesgoCotizacionNuevo
							.setValorMaximoLimiteDeducible(riesgoCot
									.getValorMaximoLimiteDeducible());
					riesgosNuevosCotizacion.add(riesgoCotizacionNuevo);
					// riesgoCotizacionFacade.save(riesgoCotizacionNuevo);
					// Recuperar los aumentos del riesgoCotizacionOriginal
					List<AumentoRiesgoCotizacionDTO> aumentosRiesgoCotizacionOriginal = riesgoCot
							.getAumentoRiesgoCotizacionDTOs();
					List<AumentoRiesgoCotizacionDTO> aumentosRiesgoNuevoCotizacion = new ArrayList<AumentoRiesgoCotizacionDTO>();
					// A cada aumento del riesgoCotizacion Original, se le
					// establecen los id�s del nuevo inciso
					for (AumentoRiesgoCotizacionDTO aumento : aumentosRiesgoCotizacionOriginal) {
						AumentoRiesgoCotizacionDTO aumentoTMP = new AumentoRiesgoCotizacionDTO();
						aumentoTMP.setAumentoVarioDTO(aumento
								.getAumentoVarioDTO());
						aumentoTMP.setClaveAutorizacion(aumento
								.getClaveAutorizacion());
						aumentoTMP.setClaveComercialTecnico(aumento
								.getClaveComercialTecnico());
						aumentoTMP.setClaveContrato(aumento.getClaveContrato());
						aumentoTMP.setClaveNivel(aumento.getClaveNivel());
						aumentoTMP.setClaveObligatoriedad(aumento
								.getClaveObligatoriedad());
						aumentoTMP.setCodigoUsuarioAutorizacion(aumento
								.getCodigoUsuarioAutorizacion());
						aumentoTMP.setValorAumento(aumento.getValorAumento());
						aumentoTMP.setId(new AumentoRiesgoCotizacionId());
						aumentoTMP.getId().setIdToAumentoVario(
								aumento.getId().getIdToAumentoVario());
						aumentoTMP.getId().setIdToCobertura(
								aumento.getId().getIdToCobertura());
						aumentoTMP.getId().setIdToCotizacion(
								incisoNuevo.getId().getIdToCotizacion());
						aumentoTMP.getId().setIdToRiesgo(
								aumento.getId().getIdToRiesgo());
						aumentoTMP.getId().setIdToSeccion(
								aumento.getId().getIdToSeccion());
						aumentoTMP.getId().setNumeroInciso(
								incisoNuevo.getId().getNumeroInciso());
						// TODO aumento.setClaveContrato((short)0);
						if (aumento.getClaveAutorizacion() != 0)
							aumentoTMP.setClaveAutorizacion((short) 1);
						aumentosRiesgoNuevoCotizacion.add(aumentoTMP);
						// aumentoRiesgoCotizacionFacade.save(aumentoTMP);
					}
					riesgoCotizacionNuevo
							.setAumentoRiesgoCotizacionDTOs(aumentosRiesgoNuevoCotizacion);
					// Recuperar los descuentos del riesgoCotizacionOriginal
					List<DescuentoRiesgoCotizacionDTO> descuentosRiesgoCotizacionOriginal = riesgoCot
							.getDescuentoRiesgoCotizacionDTOs();
					List<DescuentoRiesgoCotizacionDTO> descuentosRiesgoNuevoCotizacion = new ArrayList<DescuentoRiesgoCotizacionDTO>();
					for (DescuentoRiesgoCotizacionDTO descuento : descuentosRiesgoCotizacionOriginal) {
						DescuentoRiesgoCotizacionDTO descuentoTMP = new DescuentoRiesgoCotizacionDTO();
						descuentoTMP.setDescuentoDTO(descuento
								.getDescuentoDTO());
						descuentoTMP.setClaveAutorizacion(descuento
								.getClaveAutorizacion());
						descuentoTMP.setClaveComercialTecnico(descuento
								.getClaveComercialTecnico());
						descuentoTMP.setClaveContrato(descuento
								.getClaveContrato());
						descuentoTMP.setClaveNivel(descuento.getClaveNivel());
						descuentoTMP.setClaveObligatoriedad(descuento
								.getClaveObligatoriedad());
						descuentoTMP.setCodigoUsuarioAutorizacion(descuento
								.getCodigoUsuarioAutorizacion());
						descuentoTMP.setClaveTipo(descuento.getClaveTipo());
						descuentoTMP
								.setRiesgoCotizacionDTO(riesgoCotizacionNuevo);
						descuentoTMP.setValorDescuento(descuento
								.getValorDescuento());
						descuentoTMP.setId(new DescuentoRiesgoCotizacionId());
						descuentoTMP.getId().setIdToDescuentoVario(
								descuento.getId().getIdToDescuentoVario());
						descuentoTMP.getId().setIdToCobertura(
								descuento.getId().getIdToCobertura());
						descuentoTMP.getId().setIdToCotizacion(
								incisoNuevo.getId().getIdToCotizacion());
						descuentoTMP.getId().setIdToRiesgo(
								descuento.getId().getIdToRiesgo());
						descuentoTMP.getId().setIdToSeccion(
								descuento.getId().getIdToSeccion());
						descuentoTMP.getId().setNumeroInciso(
								incisoNuevo.getId().getNumeroInciso());
						// TODO descuento.setClaveContrato((short)0);
						if (descuento.getClaveAutorizacion() != 0)
							descuentoTMP.setClaveAutorizacion((short) 1);
						descuentosRiesgoNuevoCotizacion.add(descuentoTMP);
						// descuentoRiesgoCotizacionFacade.save(descuentoTMP);
					}
					riesgoCotizacionNuevo
							.setDescuentoRiesgoCotizacionDTOs(descuentosRiesgoNuevoCotizacion);
					// Recuperar los recargos del riesgoCotizacionOriginal
					List<RecargoRiesgoCotizacionDTO> recargosRiesgoCotizacionOriginal = riesgoCot
							.getRecargoRiesgoCotizacionDTOs();

					List<RecargoRiesgoCotizacionDTO> recargosRiesgoNuevoCotizacion = new ArrayList<RecargoRiesgoCotizacionDTO>();
					for (RecargoRiesgoCotizacionDTO recargo : recargosRiesgoCotizacionOriginal) {
						RecargoRiesgoCotizacionDTO recargoTMP = new RecargoRiesgoCotizacionDTO();
						recargoTMP.setClaveAutorizacion(recargo
								.getClaveAutorizacion());
						recargoTMP.setClaveComercialTecnico(recargo
								.getClaveComercialTecnico());
						recargoTMP.setClaveContrato(recargo.getClaveContrato());
						recargoTMP.setClaveNivel(recargo.getClaveNivel());
						recargoTMP.setClaveObligatoriedad(recargo
								.getClaveObligatoriedad());
						recargoTMP.setClaveTipo(recargo.getClaveTipo());
						recargoTMP.setCodigoUsuarioAutorizacion(recargo
								.getCodigoUsuarioAutorizacion());
						recargoTMP.setRecargoVarioDTO(recargo
								.getRecargoVarioDTO());
						recargoTMP
								.setRiesgoCotizacionDTO(riesgoCotizacionNuevo);
						recargoTMP.setValorRecargo(recargo.getValorRecargo());
						recargoTMP.setId(new RecargoRiesgoCotizacionId());
						recargoTMP.getId().setIdToRecargoVario(
								recargo.getId().getIdToRecargoVario());
						recargoTMP.getId().setIdToCobertura(
								recargo.getId().getIdToCobertura());
						recargoTMP.getId().setIdToCotizacion(
								incisoNuevo.getId().getIdToCotizacion());
						recargoTMP.getId().setIdToRiesgo(
								recargo.getId().getIdToRiesgo());
						recargoTMP.getId().setIdToSeccion(
								recargo.getId().getIdToSeccion());
						recargoTMP.getId().setNumeroInciso(
								incisoNuevo.getId().getNumeroInciso());
						// recargo.setClaveContrato((short)0);
						if (recargo.getClaveAutorizacion() != 0)
							recargo.setClaveAutorizacion((short) 1);
						recargosRiesgoNuevoCotizacion.add(recargoTMP);
						// recargoRiesgoCotizacionFacade.save(recargoTMP);
					}
					riesgoCotizacionNuevo
							.setRecargoRiesgoCotizacionDTOs(recargosRiesgoNuevoCotizacion);
					// Copiar los registros de DetallePrimaRiesgoCotizacion
					List<DetallePrimaRiesgoCotizacionDTO> listaDetallePrimaRiesgo = riesgoCot
							.getDetallePrimaRiesgoCotizacionList();
					List<DetallePrimaRiesgoCotizacionDTO> detallesPrimaRiesgoNuevoCotizacion = new ArrayList<DetallePrimaRiesgoCotizacionDTO>();
					for (DetallePrimaRiesgoCotizacionDTO detalle : listaDetallePrimaRiesgo) {
						DetallePrimaRiesgoCotizacionDTO detallePrimaRiesgoTMP = new DetallePrimaRiesgoCotizacionDTO();
						detallePrimaRiesgoTMP
								.setId(new DetallePrimaRiesgoCotizacionId());
						detallePrimaRiesgoTMP.getId().setIdToCobertura(
								detalle.getId().getIdToCobertura());
						detallePrimaRiesgoTMP.getId().setIdToCotizacion(
								incisoNuevo.getId().getIdToCotizacion());
						detallePrimaRiesgoTMP.getId().setIdToRiesgo(
								detalle.getId().getIdToRiesgo());
						detallePrimaRiesgoTMP.getId().setIdToSeccion(
								detalle.getId().getIdToSeccion());
						detallePrimaRiesgoTMP.getId().setNumeroInciso(
								incisoNuevo.getId().getNumeroInciso());
						detallePrimaRiesgoTMP.getId().setNumeroSubInciso(
								detalle.getId().getNumeroSubInciso());
						detallePrimaRiesgoTMP.setRiesgoCotizacionDTO(riesgoCot);
						detallePrimaRiesgoTMP.setValorCuota(detalle
								.getValorCuota());
						detallePrimaRiesgoTMP.setValorCuotaARDT(detalle
								.getValorCuotaARDT());
						detallePrimaRiesgoTMP.setValorCuotaARDV(detalle
								.getValorCuotaARDV());
						detallePrimaRiesgoTMP.setValorCuotaB(detalle
								.getValorCuotaB());
						detallePrimaRiesgoTMP.setValorPrimaNeta(detalle
								.getValorPrimaNeta());
						detallePrimaRiesgoTMP.setValorPrimaNetaARDT(detalle
								.getValorPrimaNetaARDT());
						detallePrimaRiesgoTMP.setValorPrimaNetaB(detalle
								.getValorPrimaNetaB());
						detallePrimaRiesgoTMP.setValorSumaAsegurada(detalle
								.getValorSumaAsegurada());
						detallesPrimaRiesgoNuevoCotizacion
								.add(detallePrimaRiesgoTMP);
						// detallePrimaRiesgoCotizacionFacade.save(detallePrimaRiesgoTMP);
					}
					riesgoCotizacionNuevo
							.setDetallePrimaRiesgoCotizacionList(detallesPrimaRiesgoNuevoCotizacion);
				}// Fin iterar riesgosCotizacion
				coberturaCotizacionNueva
						.setRiesgoCotizacionLista(riesgosNuevosCotizacion);
			}// Fin iterar CoberturasCotuzacion
			seccionCotizacionNueva
					.setCoberturaCotizacionLista(coberturasNuevasCotizacion);
		}// Fin iterar SeccionCotizacion
		incisoNuevo.setSeccionCotizacionList(listaSeccionesNuevasCotizacion);
		incisoNuevo.setCondicionesEspeciales(incisoBase.getCondicionesEspeciales());
	}
	
	public void validarEstructura(IncisoCotizacionDTO inciso){
		if(inciso.getClaveEstatusInspeccion() == null){
			inciso.setClaveEstatusInspeccion((short)0);
		}
		if(inciso.getClaveTipoOrigenInspeccion() == null){
			inciso.setClaveTipoOrigenInspeccion((short)0);
		}
		if(inciso.getClaveMensajeInspeccion() == null){
			inciso.setClaveMensajeInspeccion((short)0);
		}
		if(inciso.getClaveAutInspeccion() == null){
			inciso.setClaveAutInspeccion((short)0);
		}
		if(inciso.getValorPrimaNeta() == null){
			inciso.setValorPrimaNeta(0d);
		}
		//Guardar los datos del inciso - auto
		//TODO: asignar valores del incisoautocot
		//ya vienen en la estructura
		List<SeccionCotizacionDTO> listaSeccionCotizacion = inciso.getSeccionCotizacionList();
		//List<SeccionCotizacionDTO> listaSeccionesNuevasCotizacion = new ArrayList<SeccionCotizacionDTO>();
		//inciso.setSeccionCotizacionList(null);
		for(SeccionCotizacionDTO seccion:listaSeccionCotizacion){
			System.out.println("Validando estrucutura seccion " + seccion.getId().getIdToSeccion());
			seccion.setValorPrimaNeta(seccion.getValorPrimaNeta()==null?0D:seccion.getValorPrimaNeta());
			seccion.setValorPrimaNetaOriginal(seccion.getValorPrimaNetaOriginal()==null?0D:seccion.getValorPrimaNetaOriginal());
			seccion.setValorSumaAsegurada(seccion.getValorSumaAsegurada()==null?0D:seccion.getValorSumaAsegurada());
			seccion.setClaveObligatoriedad(new Short(seccion.getSeccionDTO()
					.getClaveObligatoriedad() == null ? "1" : seccion
							.getSeccionDTO().getClaveObligatoriedad()));
			seccion.setClaveContrato((short)1);
			if(seccion.getId().getNumeroInciso() == null){
				seccion.getId().setNumeroInciso(inciso.getId().getNumeroInciso());
			}
			
			List<CoberturaCotizacionDTO> listaCoberturaCot = seccion.getCoberturaCotizacionLista();			
			for(CoberturaCotizacionDTO cobertura : listaCoberturaCot) {
				cobertura.getId().setIdToCotizacion(inciso.getId().getIdToCotizacion());
				cobertura.getId().setNumeroInciso(inciso.getId().getNumeroInciso());
				cobertura.setSeccionCotizacionDTO(seccion);				
				cobertura.setValorPrimaNeta(cobertura.getValorPrimaNeta()==null?0D:cobertura.getValorPrimaNeta());				
				cobertura.setValorSumaAsegurada(cobertura.getValorSumaAsegurada()==null?0D:cobertura.getValorSumaAsegurada());
				cobertura.setValorCoaseguro(cobertura.getValorCoaseguro()==null?0D:cobertura.getValorCoaseguro());
				cobertura.setValorDeducible(cobertura.getValorDeducible()==null?0D:cobertura.getValorDeducible());
				cobertura.setValorCuota(cobertura.getValorCuota()==null?0D:cobertura.getValorCuota());
				cobertura.setValorCuotaOriginal(cobertura.getValorCuotaOriginal()==null?0D:cobertura.getValorCuotaOriginal());
				cobertura.setValorCuotaOriginal(cobertura.getValorCuotaOriginal()==null?0D:cobertura.getValorCuotaOriginal());
				cobertura.setPorcentajeCoaseguro(cobertura.getPorcentajeCoaseguro()==null?0D:cobertura.getPorcentajeCoaseguro());
				cobertura.setPorcentajeDeducible(cobertura.getPorcentajeDeducible()==null?0D:cobertura.getPorcentajeDeducible());
				if (cobertura.getClaveAutCoaseguro().intValue() != 0)
					cobertura.setClaveAutCoaseguro((short)1);
				else
					cobertura.setClaveAutCoaseguro((short)0);

				if (cobertura.getClaveAutCoaseguro().intValue() != 0)
					cobertura.setClaveAutDeducible((short)1);
				else
					cobertura.setClaveAutDeducible((short)0);							
			}
		}		
		validaRiesgos(inciso);
	}
	
	public void validaRiesgos(IncisoCotizacionDTO inciso){
		SeccionCotizacionDTO seccionCotizacion = inciso.getSeccionCotizacion();
		Map<BigDecimal, List<RiesgoCoberturaDTO>> coberturaRiesgoCoberturaVigentes = riesgoCoberturaFacadeLocal
				.getMapCoberturaRiesgoCoberturaVigentes(seccionCotizacion
						.getId().getIdToSeccion());
		for(CoberturaCotizacionDTO cobertura : seccionCotizacion.getCoberturaCotizacionLista()){
			List<RiesgoCotizacionDTO> riesgosCotizacion = obtenerRiesgos(cobertura, coberturaRiesgoCoberturaVigentes);
			cobertura.setRiesgoCotizacionLista(riesgosCotizacion);
		}
	}

	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public List<RiesgoCotizacionDTO> obtenerRiesgos(CoberturaCotizacionDTO coberturaCotizacionNueva){
		return obtenerRiesgos(coberturaCotizacionNueva, null);
	}
	
	private List<RiesgoCotizacionDTO> obtenerRiesgos(CoberturaCotizacionDTO coberturaCotizacionNueva, Map<BigDecimal, List<RiesgoCoberturaDTO>> coberturaRiesgoCoberturaVigentes){
		List<RiesgoCotizacionDTO> riesgosCotizacion = new ArrayList<RiesgoCotizacionDTO>();
		List<RiesgoCoberturaDTO> riesgosCotizacionTmp = getRiesgosVigentes(coberturaCotizacionNueva.getId().getIdToCobertura(), 
				coberturaCotizacionNueva.getId().getIdToSeccion(), coberturaRiesgoCoberturaVigentes);
		RiesgoCotizacionDTO riesgoCotizacionDTO = null;
		for(RiesgoCoberturaDTO riesgo : riesgosCotizacionTmp) {
			riesgoCotizacionDTO = new RiesgoCotizacionDTO();
			riesgoCotizacionDTO.setId(new RiesgoCotizacionId());
			riesgoCotizacionDTO.getId().setIdToCobertura(riesgo.getId().getIdtocobertura());
			riesgoCotizacionDTO.getId().setIdToCotizacion(coberturaCotizacionNueva.getId().getIdToCotizacion());
			riesgoCotizacionDTO.getId().setIdToRiesgo(riesgo.getId().getIdtoriesgo());
			riesgoCotizacionDTO.getId().setIdToSeccion(riesgo.getId().getIdtoseccion());
			riesgoCotizacionDTO.getId().setNumeroInciso(coberturaCotizacionNueva.getId().getNumeroInciso());
			riesgoCotizacionDTO.setRiesgoCoberturaDTO(riesgo);
			riesgoCotizacionDTO.setCoberturaCotizacionDTO(coberturaCotizacionNueva);
			riesgoCotizacionDTO.setIdTcSubramo(coberturaCotizacionNueva.getIdTcSubramo());
			riesgoCotizacionDTO.setValorPrimaNeta(coberturaCotizacionNueva.getValorPrimaNeta());
			riesgoCotizacionDTO.setValorSumaAsegurada(coberturaCotizacionNueva.getValorSumaAsegurada());
			riesgoCotizacionDTO.setValorCoaseguro(coberturaCotizacionNueva.getValorCoaseguro());
			riesgoCotizacionDTO.setValorDeducible(coberturaCotizacionNueva.getValorDeducible());
			riesgoCotizacionDTO.setClaveAutCoaseguro(coberturaCotizacionNueva.getClaveAutCoaseguro());
			riesgoCotizacionDTO.setClaveAutDeducible(coberturaCotizacionNueva.getClaveAutDeducible());
			riesgoCotizacionDTO.setClaveObligatoriedad(coberturaCotizacionNueva.getClaveObligatoriedad());
			riesgoCotizacionDTO.setClaveContrato(coberturaCotizacionNueva.getClaveContrato());
			riesgoCotizacionDTO.setValorAumento(0D);
			riesgoCotizacionDTO.setValorDescuento(0D);
			riesgoCotizacionDTO.setValorRecargo(0D);
			riesgoCotizacionDTO.setPorcentajeCoaseguro(coberturaCotizacionNueva.getPorcentajeCoaseguro());
			riesgoCotizacionDTO.setPorcentajeDeducible(coberturaCotizacionNueva.getPorcentajeDeducible());
			riesgoCotizacionDTO.setValorCuotaARDT(coberturaCotizacionNueva.getValorCuota());
			riesgoCotizacionDTO.setValorCuotaB(coberturaCotizacionNueva.getValorCuota());
			riesgoCotizacionDTO.setValorPrimaNetaARDT(coberturaCotizacionNueva.getValorPrimaNeta());
			riesgoCotizacionDTO.setValorPrimaNetaB(coberturaCotizacionNueva.getValorPrimaNeta());
			riesgoCotizacionDTO.setClaveTipoDeducible(coberturaCotizacionNueva.getClaveTipoDeducible());
			riesgoCotizacionDTO.setClaveTipoLimiteDeducible(coberturaCotizacionNueva.getClaveTipoLimiteDeducible());
			riesgoCotizacionDTO.setValorMinimoLimiteDeducible(coberturaCotizacionNueva.getValorMinimoLimiteDeducible());
			riesgoCotizacionDTO.setValorMaximoLimiteDeducible(coberturaCotizacionNueva.getValorMaximoLimiteDeducible());
			riesgosCotizacion.add(riesgoCotizacionDTO);
		}
		return riesgosCotizacion;
	}
	
	/**
	 * Obtiene los riesgos vigentes para una <code>idToSeccion</code> y <code>idToCobertura</code>. Si se envia un mapa con 
	 * los riesgos de la seccion <code>coberturaRiesgoCoberturaVigentes</code> la lista se obtiene de ahi.
	 * @param idToSeccion
	 * @param idToCobertura
	 * @param coberturaRiesgoCoberturaVigentes
	 * @return
	 */
	private List<RiesgoCoberturaDTO> getRiesgosVigentes(BigDecimal idToCobertura, BigDecimal idToSeccion, Map<BigDecimal, List<RiesgoCoberturaDTO>> coberturaRiesgoCoberturaVigentes) {
		List<RiesgoCoberturaDTO> riesgoCoberturaList = new ArrayList<RiesgoCoberturaDTO>();
		if (coberturaRiesgoCoberturaVigentes == null) {
			//Como el mapa viene nulo entonces buscamos los riesgos vigentes individualmente.
			riesgoCoberturaList = riesgoCoberturaFacadeLocal
					.listarVigentesPorCoberturaSeccion(idToCobertura, idToSeccion);
		} else {
			//Como el mapa no viene vacio obtenemos los RiesgoCobertura de este.
			List<RiesgoCoberturaDTO> riesgoCoberturaListTmp = coberturaRiesgoCoberturaVigentes.get(idToCobertura);
			if (riesgoCoberturaListTmp != null) {
				riesgoCoberturaList = riesgoCoberturaListTmp;
			}
		}
		return riesgoCoberturaList;
	}
	

	public List<IncisoCotizacionDTO> getIncisosPorLineaPaquete(
			BigDecimal cotizacionId, BigDecimal lineaId, Long paqueteId) {
		return incisoCotizacionFacadeLocal.getIncisosPorLineaPaquete(
				cotizacionId, lineaId, paqueteId);
	}

	public List<IncisoCotizacionDTO> getIncisosPorLinea(
			BigDecimal cotizacionId, BigDecimal lineaId) {
		return incisoCotizacionFacadeLocal.getIncisosPorLinea(cotizacionId,
				lineaId);
	}
	
	public void cambiarPaqueteActualizarCoberturas(IncisoCotizacionDTO inciso,
			Long idToNegPaqueteSeccion, List<CoberturaCotizacionDTO> coberturas){
		inciso = entidadService.findById(IncisoCotizacionDTO.class, inciso.getId());
		List<CoberturaCotizacionDTO> coberturasPersistidas = coberturaService.getCoberturas(inciso,false);
		for(CoberturaCotizacionDTO cobertura: coberturasPersistidas){
			if(cobertura != null){
				coberturaCotizacionFacadeRemote.delete(cobertura);				
			}
		}
		inciso.getSeccionCotizacion().setCoberturaCotizacionLista(coberturas);
		NegocioPaqueteSeccion negocioPaquete = entidadService.findById(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);
		inciso.getIncisoAutoCot().setNegocioPaqueteId(idToNegPaqueteSeccion);
		inciso.getIncisoAutoCot().setPaquete(negocioPaquete.getPaquete());
		guardar(inciso);		
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void eliminarCoberturasDeInciso(IncisoCotizacionDTO inciso){		
		if(inciso != null){
			SeccionCotizacionDTO seccion = inciso.getSeccionCotizacion();
			if(seccion != null){				
				Iterator<CoberturaCotizacionDTO> iterator = seccion.getCoberturaCotizacionLista().iterator();
				while(iterator.hasNext())
				{
					CoberturaCotizacionDTO cobertura = iterator.next();
					/*CoberturaCotizacionDTO coberturaEliminar = 
						entidadService.findById(CoberturaCotizacionDTO.class, new CoberturaCotizacionId(idToCotizacion, idInciso, seccion.getId().getIdToSeccion(), cobertura.getId().getIdToCobertura()));*/					
						cobertura.setSeccionCotizacionDTO(null);
						coberturaCotizacionFacadeRemote.delete(cobertura);
						//entidadService.remove(cobertura);
					
				    iterator.remove(); // works correctly				    
				}
				seccion.getCoberturaCotizacionLista().clear();			
			}					
		}
	}	
	
	public void cambiarPaquete(BigDecimal idToCotizacion, BigDecimal idInciso, Long idToNegPaqueteSeccion){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);	
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, idInciso));
		SeccionCotizacionDTO seccionCotizacion = inciso.getSeccionCotizacion();
		Long negocioId = cotizacion.getSolicitudDTO().getNegocio().getIdToNegocio(); 
		BigDecimal productoId = cotizacion.getSolicitudDTO().getProductoDTO().getIdToProducto();		
		BigDecimal tipoDePolizaId = cotizacion.getTipoPolizaDTO().getIdToTipoPoliza();				
		BigDecimal lineaId = seccionCotizacion.getSeccionDTO().getIdToSeccion();
		Short idMoneda = cotizacion.getIdMoneda().shortValue();		
		NegocioPaqueteSeccion negocioPaquete = entidadService.findById(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);		
		List<CoberturaCotizacionDTO> nuevasCoberturas = coberturaService
				.getCoberturas(negocioId, productoId, tipoDePolizaId, lineaId,
						negocioPaquete.getPaquete().getId(), cotizacion.getIdToCotizacion(), idMoneda);	
		for(CoberturaCotizacionDTO cobertura : nuevasCoberturas){
			cobertura.getId().setNumeroInciso(idInciso);
			cobertura.setSeccionCotizacionDTO(seccionCotizacion);
		}
		cambiarPaquete(idToCotizacion, idInciso, idToNegPaqueteSeccion, nuevasCoberturas);
	}
	
	
	public void cambiarPaqueteAgente(BigDecimal idToCotizacion, BigDecimal idInciso, Long idToNegPaqueteSeccion, 
			List<CoberturaCotizacionDTO> nuevasCoberturas, boolean isServicioPublico){
		cambiarPaqueteBase( idToCotizacion,  idInciso, idToNegPaqueteSeccion, nuevasCoberturas, isServicioPublico); 
	}
	
	public void cambiarPaquete(BigDecimal idToCotizacion, BigDecimal idInciso, Long idToNegPaqueteSeccion, 
			List<CoberturaCotizacionDTO> nuevasCoberturas){
		cambiarPaqueteBase( idToCotizacion,  idInciso, idToNegPaqueteSeccion, nuevasCoberturas, false); 
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void cambiarPaqueteBase(BigDecimal idToCotizacion, BigDecimal idInciso, Long idToNegPaqueteSeccion, 
			List<CoberturaCotizacionDTO> nuevasCoberturas, boolean isServicioPublico){							
		IncisoCotizacionDTO inciso = entidadService.findById(IncisoCotizacionDTO.class, new IncisoCotizacionId(idToCotizacion, idInciso));
		NegocioPaqueteSeccion negocioPaquete = entidadService.findById(NegocioPaqueteSeccion.class, idToNegPaqueteSeccion);			
		List<CoberturaCotizacionDTO> viejasCoberturas = inciso.getSeccionCotizacion().getCoberturaCotizacionLista();		
		inciso.getIncisoAutoCot().setNegocioPaqueteId(idToNegPaqueteSeccion);		
		inciso.getIncisoAutoCot().setPaquete(negocioPaquete.getPaquete());		
		Iterator<CoberturaCotizacionDTO> iterator = viejasCoberturas.iterator();
		if(!isServicioPublico){
			while(iterator.hasNext())
			{
				CoberturaCotizacionDTO vieja = iterator.next();
				if(!nuevasCoberturas.contains(vieja)){
					vieja.setSeccionCotizacionDTO(null);
					entidadService.remove(vieja);
					iterator.remove(); // works correctly		
				}
			}
		}
		inciso.getSeccionCotizacion().setCoberturaCotizacionLista(nuevasCoberturas);	
		inciso = entidadService.save(inciso);
		inciso.getIncisoAutoCot().setNegocioPaqueteId(idToNegPaqueteSeccion);		
		inciso.getIncisoAutoCot().setPaquete(negocioPaquete.getPaquete());	
		guardar(inciso);	
	}

	public IncisoCotizacionDTO guardar(IncisoCotizacionDTO inciso) {
		CotizacionDTO cotizacion = inciso.getCotizacionDTO();

		if (inciso.getId() == null)
			inciso.setId(new IncisoCotizacionId());
		if (inciso.getId().getNumeroInciso() == null) {
			inciso.getId()
					.setNumeroInciso(
							incisoCotizacionFacadeLocal.maxIncisos(
									cotizacion.getIdToCotizacion()).add(
									BigDecimal.ONE));
		}
		if(inciso.getNumeroSecuencia() == null){
			Long numeroSecuencia = new Long("1");
			numeroSecuencia = numeroSecuencia
					+ incisoCotizacionFacadeLocal.maxSecuencia(cotizacion.getIdToCotizacion());
			inciso.setNumeroSecuencia(numeroSecuencia);			
		}
//		inciso.setDescripcionGiroAsegurado(inciso.getSeccionCotizacion().getSeccionDTO().getDescripcion() + " / " + inciso.getIncisoAutoCot().getDescripcionFinal());
		if (inciso.getId().getIdToCotizacion() == null)
			inciso.getId().setIdToCotizacion(cotizacion.getIdToCotizacion());			
		validarEstructura(inciso);
		inciso = entidadService.save(inciso);

		try{
			//Evita ejecutar queries inecesarios si no tiene riesgos.
			if (tieneRiesgos(inciso)) {
				riesgoCotizacionFacadeRemote.insertarARD(cotizacion.getIdToCotizacion(), inciso.getId()
						.getNumeroInciso(), cotizacion.getSolicitudDTO()
						.getProductoDTO().getIdToProducto(), cotizacion
						.getTipoPolizaDTO().getIdToTipoPoliza());  
			}
		}catch(RuntimeException e){
			e.printStackTrace();
		}			
	 		
		return inciso;
	}	

	private boolean tieneRiesgos(IncisoCotizacionDTO inciso) {
		for (CoberturaCotizacionDTO coberturaCotizacion : inciso.getSeccionCotizacion().getCoberturaCotizacionLista()) {
			if (!coberturaCotizacion.getRiesgoCotizacionLista().isEmpty()) {
				return true;
			}
		}
		return false;
	}

	protected void updateCoberturas(List<CoberturaCotizacionDTO> coberturaCotizacionList){
		for(CoberturaCotizacionDTO cobertura: coberturaCotizacionList){
			entidadService.save(cobertura);
		}
	}
	@EJB
	public void setVarModifDescripcionDao(
			VarModifDescripcionDao varModifDescripcionDao) {
		this.varModifDescripcionDao = varModifDescripcionDao;
	}

	@EJB
	public void setCatalogoValorFijoFacadeRemote(
			CatalogoValorFijoFacadeRemote catalogoValorFijoFacadeRemote) {
		this.catalogoValorFijoFacadeRemote = catalogoValorFijoFacadeRemote;
	}
	
	@EJB
	public void setIncisoCotizacionFacadeLocal(
			IncisoCotizacionFacadeRemote incisoCotizacionFacadeLocal) {
		this.incisoCotizacionFacadeLocal = incisoCotizacionFacadeLocal;
	}

	@EJB
	public void setRiesgoCotizacionFacadeRemote(
			RiesgoCotizacionFacadeRemote riesgoCotizacionFacadeRemote) {
		this.riesgoCotizacionFacadeRemote = riesgoCotizacionFacadeRemote;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCoberturaService(CoberturaService coberturaService) {
		this.coberturaService = coberturaService;
	}
	
	@EJB
	public void setSeccionFacadeRemote(SeccionFacadeRemote seccionFacadeRemote) {
		this.seccionFacadeRemote = seccionFacadeRemote;
	}	
	
	@EJB
	public void setCoberturaCotizacionFacadeRemote(
			CoberturaCotizacionFacadeRemote coberturaCotizacionFacadeRemote) {
		this.coberturaCotizacionFacadeRemote = coberturaCotizacionFacadeRemote;
	}
	
	@EJB
	public void setNegocioSeccionService(NegocioSeccionService negocioSeccionService) {
		this.negocioSeccionService = negocioSeccionService;
	}
	@EJB
	public void setRiesgoCoberturaFacadeRemote(
			RiesgoCoberturaFacadeRemote riesgoCoberturaFacadeRemote) {
		this.riesgoCoberturaFacadeLocal = riesgoCoberturaFacadeRemote;
	}

	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}

	@EJB
	public void setDatoIncisoCotAutoService(
			DatoIncisoCotAutoService datoIncisoCotAutoService) {
		this.datoIncisoCotAutoService = datoIncisoCotAutoService;
	}

	@EJB
	public void setConfiguracionDatoIncisoService(
			ConfiguracionDatoIncisoService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}

	@EJB
	public void setSeccionCotizacionFacadeRemote(
			SeccionCotizacionFacadeRemote seccionCotizacionFacadeRemote) {
		this.seccionCotizacionFacadeRemote = seccionCotizacionFacadeRemote;
	}

	@EJB
	public void setIncisoAutoDao(IncisoAutoDao incisoAutoDao) {
		this.incisoAutoDao = incisoAutoDao;
	}
	
	@EJB
	public void setNegocioTipoServicioDao(NegocioTipoServicioDao negocioTipoServicioDao) {
		this.negocioTipoServicioDao = negocioTipoServicioDao;
	}
	
	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}

	@EJB
	public void setNegocioPaqueteSeccionDao(
			NegocioPaqueteSeccionDao negocioPaqueteSeccionDao) {
		this.negocioPaqueteSeccionDao = negocioPaqueteSeccionDao;
	}
}
