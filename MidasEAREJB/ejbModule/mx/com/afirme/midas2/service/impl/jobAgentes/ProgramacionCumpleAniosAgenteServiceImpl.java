package mx.com.afirme.midas2.service.impl.jobAgentes;

import java.math.BigDecimal;
import java.util.Calendar;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.GenericMailService;
import mx.com.afirme.midas2.service.impl.jobAgentes.ProgramacionCalculoBonosServiceImpl.conceptoEjecucionAutomatica;
import mx.com.afirme.midas2.service.jobAgentes.ProgramacionCumpleAniosAgenteService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class ProgramacionCumpleAniosAgenteServiceImpl implements ProgramacionCumpleAniosAgenteService {
	@EJB
	private AgenteMidasService agenteMidasService;
	@PersistenceContext private EntityManager entityManager;
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProgramacionCumpleAniosAgenteServiceImpl.class);
	
	/**
	 * Obtiene una lista de agentes que cumplen anios 
	 * este día
	 */
	@Override
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		String queryString = new String(
				" SELECT a.id, p.fechanacimiento " +
				" from MIDAS.vw_persona p " +
				" inner join MIDAS.toagente a on a.IDPERSONA = p.IDPERSONA " +
				" where EXTRACT(month FROM p.fechanacimiento) = :month " +
				" and   EXTRACT(day FROM p.fechanacimiento) = :day " +
				" AND a.IDSITUACIONAGENTE = (select sitAgente.ID "+
                                    " from MIDAS.toValorCatalogoAgentes sitAgente "+  
									" where sitAgente.GRUPOCATALOGOAGENTES_ID = (select tcgrupagente.ID  "+
                                                                        " from MIDAS.tcgrupocatalogoagentes tcgrupagente "+  
                                                                        " where descripcion = 'Estatus de Agente (Situacion)') "+
                                     " and sitAgente.VALOR ='AUTORIZADO') ");
		Calendar today = Calendar.getInstance();
		queryString = queryString.replace(":month",today.get(Calendar.MONTH)+1+"");
		queryString = queryString.replace(":day",today.get(Calendar.DAY_OF_MONTH)+"");
		Query query = entityManager.createNativeQuery(queryString);
		@SuppressWarnings("unchecked")
		List <Object> result = query.getResultList();
		if (result != null && !result.isEmpty()) {
			List<TareaProgramada> listTask = makeListTask(result);
			return listTask;
		}
		return null;
	}

	@Override
	public void executeTasks() {
		LOG.info("Ejecutando ProgramacionCumpleAniosAgenteService.executeTasks()...");
		List<TareaProgramada> tasks=getTaskToDo(conceptoEjecucionAutomatica.BONOS.getValue());
		if (tasks != null) {
			for (TareaProgramada task : tasks) {
				Map<String,Map<String, List<String>>> mapCorreos = agenteMidasService
						.obtenerCorreos(task.getId(),
								GenericMailService.P_CUMPLEANIOS,
								GenericMailService.M_CUMPLIMIENTO_DEFECHA);
				agenteMidasService.enviarCorreo(mapCorreos, null, null,
						GenericMailService.T_FELIZ_CUMPLEANIOS, null);
			}
		}
	}
	
	/**
	 * Crea la lista de Tareas Pramadas a ejecutar
	 * @param result
	 * @return
	 */
	private List<TareaProgramada> makeListTask(List<Object> result){
		List<TareaProgramada> listResultAgente = new LinkedList<TareaProgramada>();
		for (Object item : result) {
			Object[] array = (Object[])item;
			if (array[1] != null) {
				TareaProgramada tareaProgramada = new TareaProgramada();
				BigDecimal id = (BigDecimal)array[0];
				tareaProgramada.setId(id.longValue());
				tareaProgramada.setFechaEjecucion((Date)array[1]);
				listResultAgente.add(tareaProgramada);
			} 
		}
		return listResultAgente;
	}
	
	public void initialize() {
		String timerInfo = "TimerProgramacionCumpleAgente";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 12 * * ?
				expression.minute(0);
				expression.hour(12);
				expression.dayOfWeek("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerProgramacionCumpleAgente", false));
				
				LOG.info("Tarea TimerProgramacionCumpleAgente configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerProgramacionCumpleAgente");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerProgramacionCumpleAgente:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		executeTasks();
	}
}
