package mx.com.afirme.midas2.service.impl.seguridad;

import java.lang.reflect.Method;
import java.math.BigDecimal;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas2.exeption.ApplicationException;
import mx.com.afirme.midas2.service.seguridad.EncryptorService;

import org.apache.log4j.Logger;

import com.ibm.misc.BASE64Decoder;
import com.ibm.misc.BASE64Encoder;

@Stateless
@TransactionAttribute(TransactionAttributeType.SUPPORTS)
@EJB(name="com.utility.ejb.PropertyRemote")
public class EncryptorServiceImpl implements EncryptorService {

	private static final Logger log = Logger.getLogger(EncryptorServiceImpl.class);
	private String qs = "?qs=";
	private final String ALGO = "AES";
    private final byte[] keyValue = new byte[] {
    		'A', 'f', 'i', 'r','m', 'e',
    		'S', 'e', 'g', 'u', 'r', 'o', 's', 
    		'K', 'e', 'y' 
	};

    /**
     * Usa los sets de la clase que se le envíe y por medio de Reflect settea los valores
	 * @param obj 		Action object
	 * @param params 	Format: [parameter]=[value]&[param2]=[value2] 
	 * 					example: "id=123&noCotizacion=3214"
	 * @throws ApplicationException
	 */
	@Override
	public void setValuesByParams( Object obj , String params ) {
		Class<?> classObj = obj.getClass();
		
        try {
            for( String x : params.split("&") ){
                String[] paramValue = x.split("=");
                String method ="set"+ paramValue[0].substring(0, 1).toUpperCase() + paramValue[0].substring(1);
                String strValue = paramValue[1];
                Method[] methods = classObj.getMethods();

                if( this.isMethod(methods, method) ){
                    Class<?> param = this.getParamType(methods, method)[0];
                    Object value = getCorrectValueType( param , strValue );
                    Method m = classObj.getMethod(method, param);
                    m.invoke(obj, value);
                }
            }
        } catch (Exception e) {
            log.info(e);
        }
    }
	
	/**
	 * Primero desencripta la cadena(parametros) y después usa los sets de la clase 
	 * que se le envíe y por medio de Reflect settea los valores
	 * @param obj 		Action object
	 * @param params 	Parametros encriptados
	 */
	@Override
	public void setValuesByEncriptedParams( Object obj , String params ) throws ApplicationException {
		String paramsDec = this.decrypt(params);
		this.setValuesByParams( obj , paramsDec );
	}
	
	/**
	 * 
	 * @param url		La url destino
	 * @param params 	Format: [parameter]=[value]&[param2]=[value2] 
	 * 					example: "id=123&noCotizacion=3214"
	 * @throws ApplicationException
	 */
	@Override
	public String getUrlEncryptedParams(String url, String params) throws ApplicationException {
		return url+qs+this.encrypt(params);
	}
	
	/**
	 * Encripta la cadena
	 */
	@Override
	public String encrypt(String strValue) {
        String encryptedValue = "";
        
        try {
            Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.ENCRYPT_MODE, key);
            encryptedValue = new BASE64Encoder().encode(c.doFinal(strValue.getBytes()));
        } catch (Exception e) {
        	log.info(e);
        }
        return replacementSymbols(encryptedValue);
    }

	
	/**
	 * 	Desencripta la cadena
	 */
	@Override
    public String decrypt(String encryptedStr) {
        String decryptedValue = "";
        BASE64Decoder b64Dec = new BASE64Decoder();

        try {
            Key key = generateKey();
            Cipher c = Cipher.getInstance(ALGO);
            c.init(Cipher.DECRYPT_MODE, key);
            decryptedValue = new String(c.doFinal(b64Dec.decodeBuffer(encryptedStr)));
        } catch (Exception e) {
            log.info(e);
        }
        return decryptedValue;
    }

	/**
	 * Reemplaza simbolos para que sean valido al metodo GET
	 * @param str
	 * @return
	 */
	private String replacementSymbols( String str ){
		String replaceStr = str.replace("+", "%2B");
		return replaceStr;
	}
	
	/**
	 * Verifica si determinado metodo se encuentra en el listado principal
	 * @param methods Listado de Metodos
	 * @param method  Metodo a buscar
	 * @return
	 */
	private Boolean isMethod( Method[] methods , String method ){
        Boolean isMethod = false;
        for( Method m : methods ){
            if( m.getName().equals(method) )
                return m.getName().equals(method);
        }
            
        return isMethod;
    }
    
	/**
	 * Obtiene el tipo de clase correcta de los parametros del metodo 
	 * @param methods Listado de metodos
	 * @param method  Metodo a buscar
	 * @return
	 */
    private Class<?>[] getParamType( Method[] methods , String method ){
        Class<?>[] parammType = null;
        for( Method m : methods ){
            if( m.getName().equals(method) )
                return m.getParameterTypes();
        }
            
        return parammType;
    }
    
    /**
     * Retorna el objeto correcto con el valor correcto
     * @param param Tipo de clase del objeto a retornar
     * @param value	Valor a settear en el objeto a retornar
     * @return
     */
    private Object getCorrectValueType( Class<?> param , String value ){
        Object correctValueType = null;
        String simpleName = param.getSimpleName();
        
        if( simpleName.contains("Integer") ){
            return new Integer(value);
        } else if( simpleName.contains("String") ){
            return new String(value);
        } else if( simpleName.contains("BigDecimal") ){
            return new BigDecimal(value);
        } else if( simpleName.contains("Long") ){
            return new Long(value);
        } else if( simpleName.contains("Double") ){
            return new Double(value);
        } else if( simpleName.contains("Boolean") ){
            return new Boolean(value);
        }
        
        return correctValueType;
    }
	
    /**
     * 
     * @return
     * @throws Exception
     */
	private Key generateKey() throws Exception {
        Key key = new SecretKeySpec(keyValue, ALGO);
        return key;
    }
	
//	------------------------------------------------------------------------------
//	------------------------------------------------------------------------------
	
	public String getQs() {
		return qs;
	}

	public void setQs(String qs) {
		this.qs = qs;
	}

	public static Logger getLog() {
		return log;
	}

}
