package mx.com.afirme.midas2.service.impl.sistema.comm;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.jfree.util.Log;

import mx.com.afirme.midas2.domain.sistema.comm.CommBitacora;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacoraDetalle;
import mx.com.afirme.midas2.domain.sistema.comm.CommBitacoraDetalle.STATUS_COMM;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso;
import mx.com.afirme.midas2.domain.sistema.comm.CommProceso.TIPO_PROCESO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sistema.comm.CommBitacoraService;
import mx.com.afirme.midas2.util.StringUtil;

@Stateless
public class CommBitacoraServiceImpl implements CommBitacoraService{

	@EJB
	private EntidadService entidadService;
	
	
	@Override
	public CommProceso obtenerProceso(TIPO_PROCESO proceso) {
		List<CommProceso> procesos = entidadService.findByProperty(CommProceso.class, "codigo", proceso.toString());
		if(procesos != null && procesos.size() > 0){
			return procesos.get(0);
		}
		return null;
	}

	@Override
	public void registrarBitacora(CommBitacora bitacora) {
		entidadService.save(bitacora);
	}

	@Override
	public void registarDetalle(CommBitacoraDetalle detalle) {
		entidadService.save(detalle);		
	}
	
	@Override
	public CommBitacora obtenerBitacora(String folio) {	
		List<CommBitacora> bitacoras = entidadService.findByProperty(CommBitacora.class, "folio", folio);
		if(bitacoras != null && bitacoras.size() > 0 ){
			return bitacoras.get(0);
		}
		return null;
	}

	@Override
	public List<CommBitacora> obtenerBitacora(CommBitacora filtro) {
		Map<String, Object>  params = new HashMap<String, Object>();
		params.put("folio", filtro.getFolio());
		//TODO add filtros
		return entidadService.findByProperties(CommBitacora.class, params);
	}
	
	@Override
	public List<CommBitacoraDetalle> obtenerBitacoraDetalle(CommBitacoraDetalle filtro) {
		Map<String, Object>  params = new HashMap<String, Object>();
		params.put("bitacora.folio", filtro.getBitacora().getFolio());
		if(!StringUtil.isEmpty(filtro.getStatusComm())){
			params.put("statusComm", filtro.getStatusComm());
		}
		return entidadService.findByProperties(CommBitacoraDetalle.class, params);
	}

	@Override
	public CommBitacoraDetalle obtenerUltimoDetalle(String folio) {
		return obtenerUltimoDetalle(folio, null);
	}
	
	
	@Override
	public CommBitacoraDetalle obtenerUltimoDetalle(String folio, STATUS_COMM status) {
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("bitacora.folio", folio);
		if(status != null){
			params.put("statusComm", status.toString());
		}
		List<CommBitacoraDetalle> detalles = 
			entidadService.findByPropertiesWithOrder(CommBitacoraDetalle.class, params, "id");
		if(detalles != null && detalles.size() > 0){
			return detalles.get(detalles.size() - 1);
		}
		return null;
	}

	@Override
	public List<CommBitacoraDetalle> obtenerDetallesConStatusErrorComm(String folio){
		CommBitacoraDetalle filtro = new CommBitacoraDetalle();
		filtro.getBitacora().setFolio(folio);
		filtro.setStatusComm(STATUS_COMM.NOK_COMM_ERROR.toString());
		return obtenerBitacoraDetalle(filtro);
	}
	
	@Override
	public CommBitacora obtenerBitacora(String folio,CommProceso.TIPO_PROCESO proceso,CommBitacora.STATUS_BITACORA status ){
		
		CommBitacora bitacora    = null;
		CommProceso  commProceso = obtenerProceso(proceso);
		
		try{
		
			if( commProceso != null ){
				
				Map<String,Object> parametros = new HashMap<String,Object>();
				parametros.put("folio"  , folio);
				parametros.put("proceso", commProceso);
				parametros.put("status" , status.name());
				
				List<CommBitacora> lCommBitacora = this.entidadService.findByProperties(CommBitacora.class, parametros);
				
				if( !lCommBitacora.isEmpty() ){
					bitacora = lCommBitacora.get(0);
				}
				
			}
		
		}catch(Exception e){
			e.printStackTrace();
			Log.error("Error CommBitacoraService obtenerBitacora por folio, proceso, estatus "+e);
		}
		
		return bitacora;
	}
}
