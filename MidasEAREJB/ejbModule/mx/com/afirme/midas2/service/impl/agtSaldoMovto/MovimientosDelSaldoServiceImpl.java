package mx.com.afirme.midas2.service.impl.agtSaldoMovto;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.agtSaldoMovto.MovimientosDelSaldoDao;
import mx.com.afirme.midas2.domain.agtSaldoMovto.MovimientosDelSaldo;
import mx.com.afirme.midas2.dto.agtSaldoMovto.MovimientosDelSaldoView;
import mx.com.afirme.midas2.service.agtSaldoMovto.MovimientosDelSaldoService;
import mx.com.afirme.midas2.util.MidasException;
@Stateless
public class MovimientosDelSaldoServiceImpl implements MovimientosDelSaldoService{

	private MovimientosDelSaldoDao movimientosDelSaldoDao;
	
	

	@Override
	public List<MovimientosDelSaldo> findByFilters(MovimientosDelSaldo filter) throws MidasException {
		return movimientosDelSaldoDao.findByFilters(filter);
	}

	@Override
	public List<MovimientosDelSaldoView> findByFiltersView(MovimientosDelSaldo filtros) throws MidasException {
		return movimientosDelSaldoDao.findByFiltersView(filtros);
	}
	
	@EJB
	public void setMovimientosDelSaldoDao(MovimientosDelSaldoDao movimientosDelSaldoDao) {
		this.movimientosDelSaldoDao = movimientosDelSaldoDao;
	}


	

}
