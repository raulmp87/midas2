/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */ 
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaCalculoBancaDao;
import mx.com.afirme.midas2.domain.compensaciones.CaCalculoBanca;
import mx.com.afirme.midas2.service.compensaciones.CaCalculoBancaService;

import org.apache.log4j.Logger;

@Stateless

public class CaCalculoBancaServiceImpl  implements CaCalculoBancaService {
	public static final String LINEANEGOCIO_ID = "lineaNegocioId";
	public static final String MONTO = "monto";
	public static final String PORCENTAJE = "porcentaje";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";

	@EJB
	private CaCalculoBancaDao calculoBancacaDao; 
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaCalculoBancaServiceImpl.class);
    
		/**
	 Perform an initial save of a previously unsaved CaCalculoBanca entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaCalculoBanca entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaCalculoBanca entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	save	::	INICIO	::	");
	        try {
//            entityManager.persist(entity);
	        	calculoBancacaDao.save(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaCalculoBanca entity.
	  @param entity CaCalculoBanca entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaCalculoBanca entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaCalculoBanca.class, entity.getId());
//            entityManager.remove(entity);
	        	calculoBancacaDao.delete(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaCalculoBanca entity and return it or a copy of it to the sender. 
	 A copy of the CaCalculoBanca entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaCalculoBanca entity to update
	 @return CaCalculoBanca the persisted CaCalculoBanca entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaCalculoBanca update(CaCalculoBanca entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaCalculoBanca result = calculoBancacaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	update	::	FIN	::	");
	        return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaCalculoBanca 	::		CaCalculoBancaServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaCalculoBanca findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaCalculoBancaServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaCalculoBanca instance = calculoBancacaDao.findById(id);//entityManager.find(CaCalculoBanca.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaCalculoBancaServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaCalculoBancaServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaCalculoBanca entities with a specific property value.  
	 
	  @param propertyName the name of the CaCalculoBanca property to query
	  @param value the property value to match
	  	  @return List<CaCalculoBanca> found by query
	 */
    public List<CaCalculoBanca> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaCalculoBancaServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaCalculoBanca model where model." 
//			 						+ propertyName + "= :propertyValue";
//								Query query = entityManager.createQuery(queryString);
//					query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaCalculoBancaServiceImpl	::	findByProperty	::	FIN	::	");
			return calculoBancacaDao.findByProperty(propertyName,value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaCalculoBancaServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaCalculoBanca> findByLineanegocioId(Object lineanegocioId
	) {
		return findByProperty(LINEANEGOCIO_ID, lineanegocioId
		);
	}
	
	public List<CaCalculoBanca> findByMonto(Object monto
	) {
		return findByProperty(MONTO, monto
		);
	}
	
	public List<CaCalculoBanca> findByPorcentaje(Object porcentaje
	) {
		return findByProperty(PORCENTAJE, porcentaje
		);
	}
	
	public List<CaCalculoBanca> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaCalculoBanca> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaCalculoBanca entities.
	  	  @return List<CaCalculoBanca> all CaCalculoBanca entities
	 */
	public List<CaCalculoBanca> findAll() {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaCalculoBancaServiceImpl	::	findAll	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaCalculoBanca model";
//			Query query = entityManager.createQuery(queryString);
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaCalculoBancaServiceImpl	::	findAll	::	FIN	::	");
			return calculoBancacaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaCalculoBancaServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
}