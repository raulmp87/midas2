package mx.com.afirme.midas2.service.impl.suscripcion.endoso;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.logging.Level;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.suscripcion.endoso.MovimientoEndosoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.Cotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.BitemporalCondicionEspCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.condicionesespeciales.CondicionEspCotContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.documento.BitemporalDocAnexoCot;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.condicionesespeciales.CondicionEspecial;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.MovimientoEndoso;
import mx.com.afirme.midas2.domain.suscripcion.impresion.EdicionEndoso;
import mx.com.afirme.midas2.dto.CondicionesEspIncisosDTO;
import mx.com.afirme.midas2.dto.domicilio.DomicilioFacadeRemote;
import mx.com.afirme.midas2.dto.impresiones.DatosCaratulaInciso;
import mx.com.afirme.midas2.dto.impresiones.DatosCoberturasDTO;
import mx.com.afirme.midas2.dto.impresiones.DatosIncisoDTO;
import mx.com.afirme.midas2.dto.impresiones.ImpresionEndosoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.endoso.condicionesEspeciales.CondicionEspecialBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.TipoMovimientoEndoso;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.catalogos.condicionesespeciales.CondicionEspecialService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.poliza.seguroobligatorio.SeguroObligatorioService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.endoso.ImpresionEndosoService;
import mx.com.afirme.midas2.util.UtileriasWeb;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JRExporterParameter;
import net.sf.jasperreports.engine.JRParameter;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.JasperRunManager;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.engine.fill.JRGzipVirtualizer;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.apache.commons.collections.Transformer;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.js.util.StringUtil;
import com.lowagie.text.Document;
import com.lowagie.text.pdf.PRAcroForm;
import com.lowagie.text.pdf.PdfCopy;
import com.lowagie.text.pdf.PdfImportedPage;
import com.lowagie.text.pdf.PdfReader;
import com.lowagie.text.pdf.SimpleBookmark;

import com.afirme.insurance.services.PrintReport.PrintReport;
import com.afirme.insurance.services.PrintReport.PrintReportServiceLocator;

@Stateless
public class ImpresionEndosoServiceImpl implements ImpresionEndosoService{
	
	private static Logger LOG = Logger.getLogger(ImpresionEndosoServiceImpl.class);

	private DomicilioImpresion domicilioCliente;
	private ClienteGenericoDTO cliente;
	
	private EntidadService entidadService;
	private EntidadBitemporalService entidadBitemporalService;
	private DomicilioFacadeRemote domicilioFacadeRemote;
	private ClienteFacadeRemote clienteFacadeRemote;
    private CalculoService calculoService;
	@EJB
	protected AgenteMidasService agenteMidasService;
	private EntidadContinuityService entidadContinuityService;
	protected ImpresionBitemporalService impresionBitemporalService;
	protected ListadoIncisosDinamicoService listadoIncisosDinamicoService;
	protected IncisoViewService incisoViewService;
	private final static String LEYENDA_PRIMATOTAL_CARGO_CONTRATANTE = "Por lo anterior, se emite recibo de ajuste a cargo del contratante por %s";
	private final static String LEYENDA_PRIMATOTAL_A_FAVOR_CONTRATANTE = "Por lo anterior, se BONIFICA al contratante la cantidad de: %s como ajuste a la prima SIEMPRE QUE LA PÓLIZA SE ENCUENTRE AL CORRIENTE EN EL PAGO DE LAS PRIMAS";
	private static String MENSAGES=SistemaPersistencia.RECURSO_MENSAJES_EXCEPCIONES;
	private final static String  ENDOSOALTACONDICIONESESPECIALES =Utilerias.getMensajeRecurso(MENSAGES, "midas.condicionespecial.endosos.inclucionCondiciones");
	private final static String  ENDOSOBAJACONDICIONESESPECIALES =Utilerias.getMensajeRecurso(MENSAGES,"midas.condicionespecial.endosos.bajaCondiciones");
	
	private static BigDecimal seccionId;
	private static Long movimientoId;
	
	private SeguroObligatorioService seguroObligatorioService;
	
	@EJB
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	
	@EJB
	private CondicionEspecialBitemporalService condicionEspecialBitemporalService;
	
	@EJB
	private CondicionEspecialService condicionEspecialService;
	
	@EJB
	private MovimientoEndosoDao movimientoEndosoDao;
	
	@EJB
	private EndosoService endosoService;
	@EJB
	private ParametroGeneralFacadeRemote parametroGeneralFacade;
	
	
	@EJB
	private ImpresionRecibosService impresionRecibosService;
	
	/**
	 * 	MAIE se extrae del metodo de impresion la logica donde se carga la informacion de la poliza para utilizarlo tanto 
	 *	en la impresion como en la precarga de informacion del modulo de edicion de impresion de endoso
	 * @param idToCotizacion
	 * @param recordFrom
	 * @param claveTipoEndoso
	 * @return
	 */
	private ImpresionEndosoDTO cargarInformacionParaImpresion(BigDecimal idToCotizacion, Date recordFrom, 
			Short claveTipoEndoso){
		ImpresionEndosoDTO impresionEndosoDTO = new ImpresionEndosoDTO();
		
		//CARGANDO INFORMACION EndosoDTO
		List<EndosoDTO> endosoList2;
        
		Map<String, Object> params2 = new LinkedHashMap<String, Object>();
		params2.put("idToCotizacion", idToCotizacion);
		params2.put("recordFrom", recordFrom);
        endosoList2 = entidadService.findByProperties(
        		EndosoDTO.class, params2);
        LogDeMidasInterfaz.log("endosoList2 => " + endosoList2, Level.INFO, null);
        
		EndosoDTO endoso;
		endoso = endosoList2.iterator().next();
		LogDeMidasInterfaz.log("endoso => " + endoso, Level.INFO, null);
		
		impresionEndosoDTO.setEndoso(endoso);
		
		//CARGANDO INFORMACION ControlEndosoCot
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("cotizacionId", idToCotizacion);
		params.put("recordFrom", recordFrom);
        List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(
        		ControlEndosoCot.class, params);
        
        ControlEndosoCot controlEndosoCot = controlEndosoCotList.iterator().next();
        impresionEndosoDTO.setControlEndosoCot(controlEndosoCot);

        //CARGANDO INFORMACION PolizaDTO
        PolizaDTO polizaDTO;	
        
		List<PolizaDTO> polizaDTOList = entidadService.findByProperty(
				PolizaDTO.class, "cotizacionDTO.idToCotizacion", idToCotizacion);
		polizaDTO = polizaDTOList.iterator().next();
		LogDeMidasInterfaz.log("polizaDTO => " + polizaDTO, Level.INFO, null);
		
		impresionEndosoDTO.setPoliza(polizaDTO);
		
		//CARGANDO validFrom en DateTime y idToCotizacion en Integer
		Integer idToCotizacionInt = idToCotizacion.intValue();
		LogDeMidasInterfaz.log("idToCotizacionInt => " + idToCotizacionInt, Level.INFO, null);
		DateTime dateTimeTemp = TimeUtils.getDateTime(controlEndosoCot.getValidFrom());
		LogDeMidasInterfaz.log("dateTimeTemp => " + dateTimeTemp, Level.INFO, null);
		
		//CARGANDO BitemporalCotizacion
		BitemporalCotizacion bCotizacionFiltrado;
		
		CotizacionContinuity cotizacionContinuity = entidadContinuityService.findContinuityByBusinessKey(
				CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacionInt);
		
		impresionEndosoDTO.setCotizacionContinuity(cotizacionContinuity);
		
		 bCotizacionFiltrado = cotizacionContinuity.getCotizaciones().get(dateTimeTemp, new DateTime(recordFrom));
		 if(bCotizacionFiltrado == null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
			 Long id = cotizacionContinuity.getId();
			 Collection<BitemporalCotizacion> cBitemporalCotizacion = entidadBitemporalService.obtenerCancelados(
					BitemporalCotizacion.class, id, dateTimeTemp, new DateTime(recordFrom));

			 if( !cBitemporalCotizacion.isEmpty() ){
				 bCotizacionFiltrado = cBitemporalCotizacion.iterator().next();
			 }
		 }
		 impresionEndosoDTO.setBitemporalCotizacion(bCotizacionFiltrado);
		
		return impresionEndosoDTO;
	}
	
	/**
	 *  MAIE se extrae del metodo de impresion la logica donde se carga la informacion de la poliza para utilizarlo tanto 
	 *	en la impresion como en la precarga de informacion del modulo de edicion de impresion de endoso
	 * @param informacionImpresionEndoso
	 * @return
	 */
	private Map<String,Object> obtenerParametrosImpresionEndoso(ImpresionEndosoDTO informacionImpresionEndoso){
		
		PolizaDTO polizaDTO = informacionImpresionEndoso.getPoliza();
		BitemporalCotizacion bitemporalCotizacion = informacionImpresionEndoso.getBitemporalCotizacion();
		Cotizacion cotizacion = bitemporalCotizacion.getEmbedded();
		EndosoDTO endoso = informacionImpresionEndoso.getEndoso();
		ControlEndosoCot controlEndosoCot = informacionImpresionEndoso.getControlEndosoCot();
		
		
		String numPoliza = polizaDTO.getNumeroPolizaFormateada();
		//LogDeMidasInterfaz.log("numPoliza => " + numPoliza, Level.INFO, null);
		Long personaContratanteId = cotizacion.getPersonaContratanteId();
		// Se obtiene el objeto ClienteGenerico asociado al contratante de la poliza
		// para obtener posterirmenta algunos datos generales
		ClienteGenericoDTO filtroClienteByIdContratante = new ClienteGenericoDTO();
		BigDecimal idPersonaContratante = new BigDecimal(personaContratanteId);
		filtroClienteByIdContratante.setIdCliente(idPersonaContratante);
		try{
			cliente = clienteFacadeRemote.loadByIdNoAddress(filtroClienteByIdContratante);
			//LogDeMidasInterfaz.log("cliente => " + this.cliente, Level.INFO, null);
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		// Se obtiene el domicilio del contratante de la poliza
		try{
			BigDecimal idDomicilioContratante = cotizacion.getDomicilioContratanteId();
			if(idDomicilioContratante != null){
				domicilioCliente = domicilioFacadeRemote.findDomicilioImpresionById(idDomicilioContratante.longValue());
			}else{
				if(cliente != null){
					cliente = clienteFacadeRemote.loadById(filtroClienteByIdContratante);
					if(cliente.getIdDomicilioConsulta() != null){
						domicilioCliente = domicilioFacadeRemote.findDomicilioImpresionById(cliente.getIdDomicilioConsulta());
					}
				}
			}
			//LogDeMidasInterfaz.log("domicilioCliente => " + domicilioCliente, Level.INFO, null);
		}catch(Exception ex){	
			LOG.error(ex);
		}
		
		String calleNumeroDomCont = null;
		String coloniaDomCont = null;
		String codigoPostal = null;
		String ciudad = null;
		String estado = null;
		if(domicilioCliente!=null){
			calleNumeroDomCont = domicilioCliente.getCalleNumero();
			coloniaDomCont = domicilioCliente.getNombreColonia();
			codigoPostal = domicilioCliente.getCodigoPostal();
			ciudad = domicilioCliente.getCiudad();
			estado = domicilioCliente.getEstado();
		}
		
		BigDecimal clienteId = idPersonaContratante;//"" + personaContratanteId;
		
		Date fechaInicioVigenciaPoliza = cotizacion.getFechaInicioVigencia();
		Date fechaFinVigenciaPoliza = cotizacion.getFechaFinVigencia();
		
		HashMap<String, Object> parametrosImprEndoso = new HashMap<String, Object>();
		
		Integer idFormaPago = cotizacion.getFormaPago().getIdFormaPago();
		Short idMoneda = cotizacion.getMoneda().getIdTcMoneda();		
		
		FormaPagoDTO formaPagoDTO = entidadService.findById(FormaPagoDTO.class, idFormaPago);
		MonedaDTO monedaDTO = entidadService.findById(MonedaDTO.class, idMoneda);
		
		String descTipoEndoso = getTipoEndosoDesc(controlEndosoCot);
		
		String descNumEndoso = endoso.getId().getNumeroEndoso() + "";
		
		String descProducto;
		SolicitudDTO solicitudDTO = cotizacion.getSolicitud();
		descProducto = solicitudDTO.getProductoDTO().getNombreComercial();
		
		String identificador = getIdentificadorCaratulaPoliza(bitemporalCotizacion);
		
		String tipoEndosoTitulo = "ENDOSO-";
		
		if(endoso.getValorPrimaTotal() < 0 ){
			tipoEndosoTitulo += "D";
		}else if(endoso.getValorPrimaTotal() > 0){
			tipoEndosoTitulo += "A";
		}else if(endoso.getValorPrimaTotal() == 0d){
			tipoEndosoTitulo += "B";
		}
		
		ResumenCostosDTO resumenCostos = calculoService.resumenCostosEndoso(endoso.getId().getIdToPoliza(), endoso.getId().getNumeroEndoso());
		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		String fechaInicioVigenciaEndosoStr = sdf.format(endoso.getFechaInicioVigencia());
		String fechaFinVigenciaEndosoStr = sdf.format(endoso.getFechaFinVigencia());
		String fechaInicioVigenciaPolizaStr = sdf.format(fechaInicioVigenciaPoliza);
		String fechaFinVigenciaPolizaStr = sdf.format(fechaFinVigenciaPoliza);
		
		SimpleDateFormat sdf_larga = new SimpleDateFormat("dd 'de' MMMM 'de' yyyy", new Locale("es", "MX")); 
		String fechaInicioVigenciaEndosoLargaStr = sdf_larga.format(endoso.getFechaInicioVigencia());
		
		parametrosImprEndoso.put("P_COVERAGE_DETAIL", null);
		parametrosImprEndoso.put("P_IMAGE_LOGO", SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		parametrosImprEndoso.put("P_QUOTATION_ID", null);
		
		parametrosImprEndoso.put("P_V_PRIMA_NETA", resumenCostos.getPrimaNetaCoberturas());
		parametrosImprEndoso.put("P_F_INI_VIGENCIA_POLIZA", fechaInicioVigenciaPoliza);
		parametrosImprEndoso.put("P_F_INI_VIGENCIA_POLIZA_STR", fechaInicioVigenciaPolizaStr);
		parametrosImprEndoso.put("P_F_FIN_VIGENCIA_POLIZA", fechaFinVigenciaPoliza);
		parametrosImprEndoso.put("P_F_FIN_VIGENCIA_POLIZA_STR", fechaFinVigenciaPolizaStr);
		parametrosImprEndoso.put("P_F_INI_VIGENCIA_ENDOSO", fechaInicioVigenciaEndosoStr);
		parametrosImprEndoso.put("P_F_FIN_VIGENCIA_ENDOSO", fechaFinVigenciaEndosoStr);
		parametrosImprEndoso.put("P_POLICY_NUMBER", numPoliza);
				
		parametrosImprEndoso.put("P_DESCUENTO", resumenCostos.getDescuento());
		parametrosImprEndoso.put("P_IMP_RCGOS_PAGOFR", resumenCostos.getRecargo());
		parametrosImprEndoso.put("P_IMP_DERECHOS", resumenCostos.getDerechos());
		parametrosImprEndoso.put("P_V_IVA", resumenCostos.getIva());
		parametrosImprEndoso.put("P_IMP_PRIMA_TOTAL", resumenCostos.getPrimaTotal());
		parametrosImprEndoso.put("P_DESC_MONEDA", monedaDTO.getDescripcion());
		parametrosImprEndoso.put("P_PAYMENT_METHOD", formaPagoDTO.getDescripcion());
		parametrosImprEndoso.put("P_NOM_SOLICITANTE", cotizacion.getNombreContratante());
		parametrosImprEndoso.put("P_CALLE_NUMERO", calleNumeroDomCont);
		parametrosImprEndoso.put("P_COLONIA", coloniaDomCont);
		parametrosImprEndoso.put("P_CLIENT_ID", clienteId);
		parametrosImprEndoso.put("P_RFC", cliente.getCodigoRFC());
		
		parametrosImprEndoso.put("P_CODIGO_POSTAL", codigoPostal);
		parametrosImprEndoso.put("P_CITY", ciudad);
		parametrosImprEndoso.put("P_STATE", estado);
		
		parametrosImprEndoso.put("P_TIPO_ENDOSO", descTipoEndoso);
		
		//parametrosImprEndoso.put("P_IMAGE_LOGO", SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		parametrosImprEndoso.put("P_SIGNATURES", SistemaPersistencia.FIRMA_FUNCIONARIO);
		
		parametrosImprEndoso.put("P_DESC_NUM_ENDOSO", descNumEndoso);
		parametrosImprEndoso.put("P_DESC_PRODUCTO", descProducto);
		parametrosImprEndoso.put("P_IDENTIFICADOR", identificador);
		parametrosImprEndoso.put("P_TIPO_ENDOSO_TITULO", tipoEndosoTitulo);
		parametrosImprEndoso.put("P_FECHA_INICIO_VIGENCIA_ENDOSO_LARGA", fechaInicioVigenciaEndosoLargaStr);
		
		parametrosImprEndoso.put("P_NUM_POLIZA_SEYCOS", endoso.getPolizaDTO().getClavePolizaSeycos());
		parametrosImprEndoso.put("P_NUM_ENDOSO_SEYCOS", impresionBitemporalService
				.obtenerNumeroEndosoSeycos(endoso, endoso.getPolizaDTO().getClavePolizaSeycos()));
		
		return parametrosImprEndoso;
	}
	
	@Override
	public Map<String,Object> obtenerInformacionImpresionEndoso(BigDecimal idToCotizacion, 
			Date recordFrom, Short claveTipoEndoso){
		
		ImpresionEndosoDTO datosEndoso = cargarInformacionParaImpresion(idToCotizacion, recordFrom, claveTipoEndoso);
		return obtenerParametrosImpresionEndoso(datosEndoso);
	}
	
	//MAIE se sobrecarga el metodo para el modulo de impresion de endosos editados
	@Override
	public ImpresionEndosoDTO imprimirEndosoM2(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso){
		boolean imprimirEdicionEndoso = false;
		return imprimirEndosoM2(idToCotizacion, recordFrom, claveTipoEndoso, imprimirEdicionEndoso);
	}
	
	@Override
	public ImpresionEndosoDTO imprimirEndosoM2(BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso, boolean imprimirEdicionEndoso){
		
		BitemporalInciso bitemporalInciso = null;
		
		LogDeMidasInterfaz.log("idToCotizacion => " + idToCotizacion, Level.FINE, null);
		LogDeMidasInterfaz.log("recordFrom => " + recordFrom, Level.FINE, null);
		LogDeMidasInterfaz.log("claveTipoEndoso => " + claveTipoEndoso, Level.FINE, null);
		
		ImpresionEndosoDTO informacionImpresionEndoso = 
			cargarInformacionParaImpresion(idToCotizacion, recordFrom, claveTipoEndoso);
		ControlEndosoCot controlEndosoCot = informacionImpresionEndoso.getControlEndosoCot();
		BitemporalCotizacion bCotizacionFiltrado = informacionImpresionEndoso.getBitemporalCotizacion();
		PolizaDTO polizaDTO = informacionImpresionEndoso.getPoliza();
		EndosoDTO endoso = informacionImpresionEndoso.getEndoso();
		CotizacionContinuity cotizacionContinuity = informacionImpresionEndoso.getCotizacionContinuity();
		
		ImpresionEndosoDTO impresionEndosoDTO = new ImpresionEndosoDTO();

		//CARGANDO validFrom en DateTime y idToCotizacion en Integer
		Integer idToCotizacionInt = idToCotizacion.intValue();
		LogDeMidasInterfaz.log("idToCotizacionInt => " + idToCotizacionInt, Level.INFO, null);
		DateTime dateTimeTemp = TimeUtils.getDateTime(controlEndosoCot.getValidFrom());
		LogDeMidasInterfaz.log("dateTimeTemp => " + dateTimeTemp, Level.INFO, null);

        Long controlEndosoCotId = controlEndosoCot.getId();
        LogDeMidasInterfaz.log("controlEndosoCotId => " + controlEndosoCotId, Level.INFO, null);
        
        Map<String,Object> parametrosImprEndoso = obtenerParametrosImpresionEndoso(informacionImpresionEndoso);
        String numPoliza = polizaDTO.getNumeroPolizaFormateada();
        Double primaTotal = (Double)parametrosImprEndoso.get("P_IMP_PRIMA_TOTAL");
        
        if(imprimirEdicionEndoso){
			setInformacionEdicionEndoso(parametrosImprEndoso, idToCotizacion, recordFrom, claveTipoEndoso);
		}
        
        String textoDelEndoso = obtenerTextoEndoso(controlEndosoCot, polizaDTO, bCotizacionFiltrado,
        											dateTimeTemp, new DateTime(recordFrom) , bitemporalInciso );    
		
		String archivoPlantillaPath = "/mx/com/afirme/midas2/service/impl/impresiones/jrxml/EndosoEnDepuracionWellFormed.jrxml";
		//LogDeMidasInterfaz.log("archivoPlantillaPath => " + archivoPlantillaPath, Level.INFO, null);
		
		InputStream plantillaEndoso = this.getClass().getResourceAsStream(archivoPlantillaPath);
		
		JasperReport jrImpresionEndoso = null;
		try {
			jrImpresionEndoso = JasperCompileManager.compileReport(plantillaEndoso);
			ByteArrayOutputStream out = new ByteArrayOutputStream();
			
			//Para evitar que se desborde el textoDelEndoso es necesario separarlo en varias paginas si se require.
			//Esto es logica que no hubiera sido necesaria si se hubiera utilizado como datasource JRBeanCollectionDataSource donde cada
			//linea del texto del endoso fuera un elemento y usar este en el "Detail" del reporte, sin embargo el diseño del  reporte utiliza puros parametros. 
			//Si esto se vuelve complicado se sugiere cambiarlo a esta manera.
			List<JasperPrint> jasperPrintList = new ArrayList<JasperPrint>();			
			final int lineasMaxTextoEndoso = 26;
			final String regex = "\n";
			String textoEndosoSinProcesar = textoDelEndoso;
			int totalPaginas = (int) Math.ceil((double) getTotalMatches(textoDelEndoso, regex) / lineasMaxTextoEndoso);
			if(totalPaginas == 0) {
				totalPaginas = 1;
			}
			parametrosImprEndoso.put("P_TOTAL_PAGES", totalPaginas);
			for (int paginaActual = 1; paginaActual <= totalPaginas ; paginaActual++) {
				String textoDelEndosoPaginaActual;
				int indexOfOccurrence = indexOfOccurrence(textoEndosoSinProcesar, regex, lineasMaxTextoEndoso);
				if (indexOfOccurrence != -1) {
					textoDelEndosoPaginaActual = textoEndosoSinProcesar.substring(0, indexOfOccurrence + regex.length());
					textoEndosoSinProcesar = textoEndosoSinProcesar.substring(indexOfOccurrence + regex.length());
				} else {
					textoDelEndosoPaginaActual = textoEndosoSinProcesar;
				}
				
				parametrosImprEndoso.put("P_ENDORSEMENT_TEXT", textoDelEndosoPaginaActual);
				parametrosImprEndoso.put("P_CURRENT_PAGE", paginaActual);
				boolean esSeguroObligatorio = esSeguroObligatorio(polizaDTO);
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
				Date fechaRegistro = esSeguroObligatorio?dateFormat.parse("21/01/2015"):polizaDTO.getIncisoPolizaDTOs().iterator().next().getSeccionPolizaDTO().getSeccionDTO().getFechaRegistro();
				String numeroRegistro = esSeguroObligatorio?"CNSF-S0094-0597-2014":polizaDTO.getIncisoPolizaDTOs().iterator().next().getSeccionPolizaDTO().getSeccionDTO().getNumeroRegistro();
				parametrosImprEndoso.put("leyendaCNSF", this.getLeyendaISyF(fechaRegistro, numeroRegistro));
				if (paginaActual == totalPaginas) {
					parametrosImprEndoso.put("P_LEYENDA_PRIMATOTAL",getLeyendaPrimaTotal(primaTotal));
					parametrosImprEndoso.put("P_LEYENDA_FINAL", getLeyendaFinal(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue()));
				}
				
				JasperPrint jasperPrint = JasperFillManager.fillReport(
						jrImpresionEndoso, parametrosImprEndoso, new JREmptyDataSource());
				jasperPrintList.add(jasperPrint);				
			}
						
			JRPdfExporter exporter = new JRPdfExporter();
			exporter.setParameter(JRExporterParameter.JASPER_PRINT_LIST, jasperPrintList);
			exporter.setParameter(JRExporterParameter.OUTPUT_STREAM, out);
			exporter.exportReport();
			
			//reporte = JasperRunManager.runReportToPdf(configuracionReporte, parametros, colleccionObjeto);

			//LogDeMidasInterfaz.log("Saliendo de generarReporte", Level.INFO, null);
			//out.toByteArray();
						
			//impresionEndosoBA = JasperRunManager.runReportToPdf(jrImpresionEndoso, parametrosImprEndoso);
			
			//impresionEndosoDTO.setImpresionEndoso(impresionEndosoBA);
			
			List<byte[]> inputByteArray = new ArrayList<byte[]>();
			inputByteArray.add(out.toByteArray());
			//ImprimirRecibos
			try{
				String llaveFiscal = null;		
				byte[] reciboFiscal = null;
				if(endoso!= null){
					llaveFiscal = endoso.getLlaveFiscal();
					LogDeMidasEJB3.getLogger().log(Level.INFO, "Obteniendo recibo para llave fiscal " + 
						llaveFiscal + " para idToCotizacion " + idToCotizacion);
				}
				reciboFiscal = impresionRecibosService.getReciboFiscal(llaveFiscal);				
				if(reciboFiscal != null){
					inputByteArray.add(reciboFiscal);	
				}
			}catch(Exception e){
				LOG.error(e.getMessage(), e);
			}
			
			//National Unity
	        if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO || 
	        		controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS)
	        {
	        	//Obtiene incisos del endoso
	        	DateTime dateTimeTemp2 = new DateTime(recordFrom);
	        	List<BitemporalInciso> bitemporalIncisoList = (List<BitemporalInciso>) cotizacionContinuity.getIncisoContinuities().get(
	        			dateTimeTemp, dateTimeTemp2);
	        	if(bitemporalIncisoList != null && !bitemporalIncisoList.isEmpty()){
	    		for(BitemporalInciso bitempInciso : bitemporalIncisoList){
	    			BitemporalSeccionInciso bitempSeccionInciso = null;	    			
	    			try{
	    				if(bitempInciso.getRecordInterval().getInterval().getStart().isEqual(dateTimeTemp2.getMillis())){
	    			Collection<BitemporalSeccionInciso> collectionBitemporalSeccionInciso =	
	    				bitempInciso.getEntidadContinuity().getIncisoSeccionContinuities().get(dateTimeTemp, dateTimeTemp2);
	    			if(collectionBitemporalSeccionInciso != null && !collectionBitemporalSeccionInciso.isEmpty()){
	    				bitempSeccionInciso = collectionBitemporalSeccionInciso.iterator().next();
	    				Collection<BitemporalCoberturaSeccion> bitempCobertSeccCollection = null;
	    				SeccionIncisoContinuity sic_local = bitempSeccionInciso.getEntidadContinuity();
	    				bitempCobertSeccCollection = sic_local.getCoberturaSeccionContinuities().get(dateTimeTemp, dateTimeTemp2);
	    				List<BitemporalCoberturaSeccion> bitemporalCoberturaSeccionListSoloContratadas = 
	    					filtrarCoberturasNoContratadas(bitempCobertSeccCollection);
	    				
	    				BitemporalAutoInciso bai = null;
	    				AutoIncisoContinuity aic = bitempInciso.getContinuity().getAutoIncisoContinuity();
	    				bai = aic.getIncisoAutos().get(dateTimeTemp, dateTimeTemp2);
	    				
	    				//parametro general
	    				ParametroGeneralId idParametro=new ParametroGeneralId(ParametroGeneralDTO.GRUPO_PARAM_GENERAL_COBERTURA_RC_USA,new BigDecimal(920010));
	    				ParametroGeneralDTO parametro=parametroGeneralFacade.findById(idParametro);
	    				String[] valoresParametros=null;
	    				if(parametro !=null && parametro.getValor()!=null){
	    					 valoresParametros=parametro.getValor().split(UtileriasWeb.SEPARADOR_COMA);	
	    				}
	    				//Valida NationalUnity
	    				boolean bandera =false;
	    				for(BitemporalCoberturaSeccion cobertura: bitemporalCoberturaSeccionListSoloContratadas){
	    					for( int i = 0; valoresParametros!=null && i <= valoresParametros.length - 1; i++)
	    					{	    						
	    						if( StringUtils.isNumeric(valoresParametros[i]) ){
	    							BigDecimal idCobertura= new  BigDecimal(valoresParametros[i]);
	    							if(cobertura.getContinuity().getCoberturaDTO().getIdToCobertura().equals(idCobertura))
	    							{
	    								if(!StringUtil.isEmpty(bai.getValue().getNombreAsegurado()) && !StringUtil.isEmpty(bai.getValue().getNombreConductor())){
		    								String idCotizacion = idToCotizacion.multiply(BigDecimal.valueOf(-1)).toString();
			    							Integer idSeccion = bitempSeccionInciso.getContinuity().getSeccion().getIdToSeccion().intValue();
			    							Integer idInciso = bitempInciso.getContinuity().getNumero();
			    							byte[] nationalUnity = this.getNationalUnity(idCotizacion, idSeccion, idInciso);
			    							if (nationalUnity != null && nationalUnity.length > 0) {
			    								inputByteArray.add(nationalUnity);
			    							}
		    								bandera = true;
		    								break;
	    								}
	    							}
	    						}
	    					}
	    					if(bandera){
	    						break;
	    					}
	    				}
	    			}
	    				}
	    			}catch(Exception e){
	    				LOG.error(e.getMessage(), e);
	    			}
	        	}
	        	}
	        }
			
			//Anexos
			Collection<byte[]> anexos = impresionBitemporalService.imprimirAnexosEndoso(bCotizacionFiltrado, dateTimeTemp, new DateTime(recordFrom), controlEndosoCot);
			inputByteArray.addAll(anexos);
			
			
			if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO){
				
				//Se agrega el reporte de Alta de Inciso
				List<BitemporalInciso> listaIncisosBitemporales =  (List<BitemporalInciso>) bCotizacionFiltrado.getContinuity().getIncisoContinuities().get(dateTimeTemp,  new DateTime(recordFrom));
				//List<Integer> secuenaciasIncisosEndoso = new ArrayList<Integer>();
				
				
				// Se iteran los incisos bitemporales para obtener los id de los incisos afectados en el endoso
				/*for(BitemporalInciso incisoBitemporal:listaIncisosBitemporales){
					if(incisoBitemporal.getRecordInterval().getInterval().getStart().isEqual(new DateTime(recordFrom).getMillis())){
						secuenaciasIncisosEndoso.add(incisoBitemporal.getValue().getNumeroSecuencia());
					}
				}
				
				
				List<DatosCaratulaInciso> dataSourceIncisos = new ArrayList<DatosCaratulaInciso>();
				
				dataSourceIncisos = impresionBitemporalService.llenarIncisoDTO(
						bCotizacionFiltrado, numPoliza, dateTimeTemp, new DateTime(recordFrom),
						polizaDTO, claveTipoEndoso);
				
				
				for(DatosCaratulaInciso incisoLleno:dataSourceIncisos){
					
					for(Integer secucia:secuenaciasIncisosEndoso){
						if(secucia.toString().equals(incisoLleno.getDataSourceIncisos().getNumeroSecuenciaInciso())){
							
							List<CondicionEspecial> condicionesEspecialesInciso = condicionEspecialBitemporalService.obtenerCondicionesEspecialesInciso(bCotizacionFiltrado, incisoLleno.getDataSourceIncisos().getNumeroSecuenciaInciso() , dateTimeTemp, new DateTime(recordFrom));
							
							if(!condicionesEspecialesInciso.isEmpty()){
								incisoLleno.getDataSourceIncisos().setCondicionesEspeciales( condicionesEspecialesInciso );
								inputByteArray.add( generaPlantillaReporteBitemporalService.imprimirCondicionesInciso(incisoLleno.getDataSourceIncisos(), "", locale));
								break;
							}
						}
					}
					
				}*/
				
				List<BitemporalInciso> lstBitemporalIncisoValidos = new ArrayList<BitemporalInciso>();
				
				for (BitemporalInciso incisoBitemporal:listaIncisosBitemporales) {
					if(incisoBitemporal.getRecordInterval().getInterval().getStart().isEqual(new DateTime(recordFrom).getMillis())){
						lstBitemporalIncisoValidos.add(incisoBitemporal);						
					}					
				}
				
				Collections.sort(lstBitemporalIncisoValidos, new Comparator<BitemporalInciso>(){
					public int compare(BitemporalInciso o1, BitemporalInciso o2) {
						return ((BitemporalInciso)o1).getContinuity().getNumero().compareTo(
								((BitemporalInciso)o2).getContinuity().getNumero());
					}
				});
				
				for (BitemporalInciso incisoBitemporal:lstBitemporalIncisoValidos) {
					
					DatosIncisoDTO datoIncisoDto = new DatosIncisoDTO();
					Integer numeroInciso = incisoBitemporal.getContinuity().getNumero();
					
					if (numeroInciso != null) {
						datoIncisoDto.setNumeroSecuenciaInciso(numeroInciso.toString());
						
						datoIncisoDto.setNumeroPoliza(polizaDTO.getNumeroPolizaFormateada());
						
						List<CondicionEspecial> condicionesEspecialesInciso = condicionEspecialBitemporalService.obtenerCondicionesEspecialesInciso(bCotizacionFiltrado, numeroInciso.toString(), dateTimeTemp, new DateTime(recordFrom));
						
						if (condicionesEspecialesInciso != null && !condicionesEspecialesInciso.isEmpty()) {
							datoIncisoDto.setCondicionesEspeciales(condicionesEspecialesInciso);
							inputByteArray.add( generaPlantillaReporteBitemporalService.imprimirCondicionesInciso(datoIncisoDto, "", null));
							
						}	
					}					
				}
				
			}
			
			if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES) {
				
				String titulo;
				
				if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES){
					titulo = ENDOSOALTACONDICIONESESPECIALES;
				}else{
					titulo = ENDOSOBAJACONDICIONESESPECIALES;
				}
				
				
				//Reporte de Conduiciones especiales a nivel poliza
				List<CondicionesEspIncisosDTO> dataSourcePoliza = new ArrayList<CondicionesEspIncisosDTO>();
				List<CondicionEspecial> condiciones = new ArrayList<CondicionEspecial>();
				List<BitemporalCondicionEspCot> listaBitemporalConCotEspReporte = new ArrayList<BitemporalCondicionEspCot>();
				
				Long cotizacionContinuityId = bCotizacionFiltrado.getContinuity().getId();
				
				Collection<CondicionEspCotContinuity> condicionesCotContinuitiesEndoso = new ArrayList<CondicionEspCotContinuity>();
				Collection<CondicionEspCotContinuity> condicionesCotContinuities = entidadContinuityService
				.findContinuitiesByParentBusinessKey(
						CondicionEspCotContinuity.class,
						CondicionEspCotContinuity.PARENT_KEY_NAME,
						cotizacionContinuityId);
				
				for(CondicionEspCotContinuity condicionCont:condicionesCotContinuities){
				
					BitemporalCondicionEspCot bitConEspCot =condicionCont.getCondicionesEspecialesCotizacion().get(dateTimeTemp, new DateTime(recordFrom));
					
					if(bitConEspCot != null){
						if(condicionCont.getCondicionesEspecialesCotizacion().get(dateTimeTemp, new DateTime(recordFrom)).getRecordInterval().getInterval().getStart().isEqual(new DateTime(recordFrom).getMillis())){
							
							listaBitemporalConCotEspReporte.add(bitConEspCot);
							condicionesCotContinuitiesEndoso.add(condicionCont);
						}
					}
					
				}
				
				for(BitemporalCondicionEspCot bitempCotCondEsp: listaBitemporalConCotEspReporte){
					
					Long idCondicion = bitempCotCondEsp.getContinuity().getCondicionEspecialId();
					condiciones.add(condicionEspecialService.obtenerCondicion(idCondicion));
					
				}
				
				for(CondicionEspecial condicion:condiciones){
					CondicionesEspIncisosDTO condicionesEspPoliza = new CondicionesEspIncisosDTO();
					condicionesEspPoliza.setCondicionEspecial(condicion);
					condicionesEspPoliza.setIncisos("Nivel Póliza");
					dataSourcePoliza.add(condicionesEspPoliza);
				}
				
				if(!dataSourcePoliza.isEmpty()){
					inputByteArray.add( generaPlantillaReporteBitemporalService.imprimirCondicionesEspecialesEndoso(dataSourcePoliza, titulo, numPoliza));
				}
				//Reporte de Conduiciones especiales a nivel poliza
				
				
				// Reporte de Condiciones especiales sin repetir con cadena de incisos afectados
				List<CondicionEspecial> condicionesEspecialesSinRepetir = generaPlantillaReporteBitemporalService.obtenerCondicionesEspecialesIncisosEndoso(bCotizacionFiltrado, dateTimeTemp, new DateTime(recordFrom));
				List<CondicionesEspIncisosDTO> dataSource = new ArrayList<CondicionesEspIncisosDTO>();
				
				//Seleccionar las condiciones especiales sin repetir a nivel inciso del endoso
				
				//Enviarle las condicioens especiales sin repetir a nivel inciso del endoso
				for(CondicionEspecial condicion:condicionesEspecialesSinRepetir){
					
					//Invocacion al servicio que regresa la lisata de id que aplican a una condicion especial
					//Con esta lisata de iD's se llenara el DTO de condiciones especiales y String de id incisos
					String cadenaIcisosCondicionEspecial = generaPlantillaReporteBitemporalService.obtenerCadenaIncisoCondicionEspecial(condicion, bCotizacionFiltrado, dateTimeTemp, new DateTime(recordFrom));
					CondicionesEspIncisosDTO condicionesEspIncisos = new CondicionesEspIncisosDTO();
					condicionesEspIncisos.setCondicionEspecial(condicion);
					condicionesEspIncisos.setIncisos("Incisos " + cadenaIcisosCondicionEspecial);
					dataSource.add(condicionesEspIncisos);
					
					LOG.info("Condicion: " + condicion.getNombre() + " Lista Incisos: " + cadenaIcisosCondicionEspecial);
					
				}
				
				if(!dataSource.isEmpty()){
					inputByteArray.add( generaPlantillaReporteBitemporalService.imprimirCondicionesEspecialesEndoso(dataSource, titulo, numPoliza));
				}

				
			}
			
			if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_CONDICIONES){
					
					String titulo;
					
					if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_CONDICIONES){
						titulo = ENDOSOALTACONDICIONESESPECIALES;
					}else{
						titulo = ENDOSOBAJACONDICIONESESPECIALES;
					}
					
					
					//Reporte de Conduiciones especiales a nivel poliza
					List<CondicionesEspIncisosDTO> dataSourcePoliza = new ArrayList<CondicionesEspIncisosDTO>();
					List<CondicionEspecial> condiciones = new ArrayList<CondicionEspecial>();
					List<BitemporalCondicionEspCot> listaBitemporalConCotEspReporte = new ArrayList<BitemporalCondicionEspCot>();
						
					Long cotizacionContinuityId = bCotizacionFiltrado.getContinuity().getId();
					
					Collection<CondicionEspCotContinuity> condicionesCotContinuities = entidadContinuityService
					.findContinuitiesByParentBusinessKey(
							CondicionEspCotContinuity.class,
							CondicionEspCotContinuity.PARENT_KEY_NAME,
							cotizacionContinuityId);
					
					for(CondicionEspCotContinuity condicionCont:condicionesCotContinuities){
					
						List<BitemporalCondicionEspCot> bitConEspCot =condicionCont.getCondicionesEspecialesCotizacion().getHistory();
						for(BitemporalCondicionEspCot bitempCotCondEsp: bitConEspCot){
							
							bitempCotCondEsp.getValidityInterval().getInterval().getEnd();
							if(bitempCotCondEsp.getValidityInterval().getInterval().getEnd().equals(dateTimeTemp)){
								listaBitemporalConCotEspReporte.add(bitempCotCondEsp);
							}
							
						}

						
					}
					
					
					for(BitemporalCondicionEspCot bitempCotCondEsp: listaBitemporalConCotEspReporte){
						
						Long idCondicion = bitempCotCondEsp.getContinuity().getCondicionEspecialId();
						condiciones.add(condicionEspecialService.obtenerCondicion(idCondicion));
						
					}
					
					for(CondicionEspecial condicion:condiciones){
						CondicionesEspIncisosDTO condicionesEspPoliza = new CondicionesEspIncisosDTO();
						condicionesEspPoliza.setCondicionEspecial(condicion);
						condicionesEspPoliza.setIncisos("Nivel Póliza");
						dataSourcePoliza.add(condicionesEspPoliza);
					}
					
					if(!dataSourcePoliza.isEmpty()){
						inputByteArray.add( generaPlantillaReporteBitemporalService.imprimirCondicionesEspecialesEndoso(dataSourcePoliza, titulo, numPoliza));
					}
					//Reporte de Conduiciones especiales a nivel poliza
					
					
					// Reporte de Condiciones especiales sin repetir con cadena de incisos afectados
					List<CondicionEspecial> condicionesEspecialesSinRepetir = generaPlantillaReporteBitemporalService.obtenerCondicionesEspecialesIncisosEndoso(bCotizacionFiltrado, dateTimeTemp, new DateTime(recordFrom));
					List<CondicionesEspIncisosDTO> dataSource = new ArrayList<CondicionesEspIncisosDTO>();
					
					//Seleccionar las condiciones especiales sin repetir a nivel inciso del endoso
					
					//Enviarle las condicioens especiales sin repetir a nivel inciso del endoso
					for(CondicionEspecial condicion:condicionesEspecialesSinRepetir){
						
						//Invocacion al servicio que regresa la lisata de id que aplican a una condicion especial
						//Con esta lisata de iD's se llenara el DTO de condiciones especiales y String de id incisos
						String cadenaIcisosCondicionEspecial = generaPlantillaReporteBitemporalService.obtenerCadenaIncisoCondicionEspecial(condicion, bCotizacionFiltrado, dateTimeTemp, new DateTime(recordFrom));
						CondicionesEspIncisosDTO condicionesEspIncisos = new CondicionesEspIncisosDTO();
						condicionesEspIncisos.setCondicionEspecial(condicion);
						condicionesEspIncisos.setIncisos(cadenaIcisosCondicionEspecial);
						dataSource.add(condicionesEspIncisos);
						
						LOG.info("Condicion: " + condicion.getNombre() + " Lista Incisos: " + cadenaIcisosCondicionEspecial);
						
					}
					
					if(!dataSource.isEmpty()){
						inputByteArray.add( generaPlantillaReporteBitemporalService.imprimirCondicionesEspecialesEndoso(dataSource, titulo, numPoliza));
					}

					
				}
			
			
			
			this.pdfConcantenate(inputByteArray, out);

			impresionEndosoDTO.setImpresionEndoso(out.toByteArray());
			impresionEndosoDTO.setEndoso(informacionImpresionEndoso.getEndoso());
		} catch (JRException e) {
			LOG.error(e.getMessage(), e);
		} catch (IOException e) {			
			LOG.error(e.getMessage(), e);
		} catch (ParseException e) {
			throw new RuntimeException(e);
		}
		
		LogDeMidasInterfaz.log("Saliendo de imprimirEndosoM2", Level.INFO, null);
		return impresionEndosoDTO;
	}
	
	private void setInformacionEdicionEndoso(Map<String,Object> parametrosImprEndoso, 
			BigDecimal idToCotizacion, Date recordFrom, Short claveTipoEndoso){
		EdicionEndoso edicion = generaPlantillaReporteBitemporalService.obtenerEdicionEndoso(
				idToCotizacion, recordFrom, claveTipoEndoso);
		if(edicion != null){
			parametrosImprEndoso.put("horaInicioVigenciaPoliza", edicion.getHoraInicioVigenciaPoliza());
			parametrosImprEndoso.put("horaFinVigenciaPoliza", edicion.getHoraFinVigenciaPoliza());
			parametrosImprEndoso.put("horaInicioVigenciaEndoso", edicion.getHoraInicioVigenciaEndoso());
			parametrosImprEndoso.put("horaFinVigenciaEndoso", edicion.getHoraFinVigenciaEndoso());
		}
	}

	private Boolean esSeguroObligatorio(PolizaDTO poliza){
		Negocio negocio = seguroObligatorioService.getNegocioSeguroObligatorio(poliza.getIdToPoliza());
		if(negocio != null && poliza != null && poliza.getCotizacionDTO() != null && poliza.getCotizacionDTO().getSolicitudDTO() != null &&
				poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getIdToNegocio().equals(negocio.getIdToNegocio())){
			return true;
		}
		return false;
	}
	
	@Override
	public String obtenerTextoEndoso(ControlEndosoCot controlEndosoCot, PolizaDTO polizaDTO, BitemporalCotizacion bCotizacion,
									 DateTime validFrom, DateTime recordFrom, BitemporalInciso bitemporalInciso) {
	    
		String textoDelEndoso = "";
	    List<MovimientoEndoso> movimientoEndosoList;
	    
	    //FILTRAR SOLO LOS MOVIMIENTOS DONDE PARTICIPO EL INCISO INDICADO
	    if (bitemporalInciso != null) {
	    	
	    	movimientoEndosoList = this.movimientoEndosoDao.movimientosEndosoInciso(controlEndosoCot, bitemporalInciso);
	    	
	    } else {
	    	//Traer todos los movimientos del endoso.
	    	movimientoEndosoList  = entidadService.findByProperty(MovimientoEndoso.class, 
	        		"controlEndosoCot.id", controlEndosoCot.getId());
	    	
	    }
		
        LogDeMidasInterfaz.log("movimientoEndosoList => " + movimientoEndosoList, Level.INFO, null);
        
        if(movimientoEndosoList != null && !movimientoEndosoList.isEmpty()){
	        //Obtener
	        MovimientoEndoso movimientoEndosoRaiz = filtrarEndosoRaiz(movimientoEndosoList);
	        
	        List<MovimientoEndoso> endososDelMismoTipo = filtrarEndososMismoTipo(movimientoEndosoList, movimientoEndosoRaiz);
	        
	        boolean esPolizaMigrada = (polizaDTO.getClavePolizaSeycos() != null && !(polizaDTO.getClavePolizaSeycos().trim().equals(""))?true:false);
	        
	        if (controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO || 
	        		controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO ||
	        		controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS) {
	        	textoDelEndoso = obtenerRedaccionDeMovimientos(movimientoEndosoList, esPolizaMigrada);        	
	        } else {
	        	textoDelEndoso = obtenerRedaccionDeMovimientos(endososDelMismoTipo, esPolizaMigrada);        	
	        }        
	        
	        if (controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_INCLUSION_ANEXO) {
	        	textoDelEndoso = textoDelEndoso.concat(obtenerRedaccionAnexos(bCotizacion.getContinuity().getId(), 
	        			validFrom, recordFrom, controlEndosoCot));
	        }
	        
	        if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL 
	        		|| controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL )
	        {
	        	textoDelEndoso = textoDelEndoso.concat(obtenerDetalleIncisoPerdidaTotal(controlEndosoCot));
	        }
	        
        }
        
        LogDeMidasInterfaz.log("textoDelEndoso => " + textoDelEndoso, Level.INFO, null);
        
        return textoDelEndoso;
	}
	
	private int indexOfOccurrence(String str, String regex, int occurrence) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		int totalOccurrence = 0;
		while (matcher.find()) {
			if (++totalOccurrence == occurrence) {
				return matcher.start();
			}
		}		
		return -1;
	}

	private int getTotalMatches(String str, String regex) {
		Pattern pattern = Pattern.compile(regex);
		Matcher matcher = pattern.matcher(str);
		int total = 0;
		while (matcher.find()) {
			total++;
		}
		return total;
	}

	private List<BitemporalCoberturaSeccion> filtrarCoberturasNoContratadas(
			Collection<BitemporalCoberturaSeccion> bitempCobertSeccCollection) {
		List<BitemporalCoberturaSeccion> soloCoberturasContratadasList = new ArrayList<BitemporalCoberturaSeccion>();
		
		for(BitemporalCoberturaSeccion bcs : bitempCobertSeccCollection){
			if(bcs.getEmbedded().getClaveContrato().intValue() == 1){
				soloCoberturasContratadasList.add(bcs);
			}
		}
		
		return soloCoberturasContratadasList;
	}
	
	/**
	 * Usa el Servicio Web de recibos 
	 * @param idToCotizacion
	 * @param idLinea
	 * @param idInciso
	 * @return
	 */
	private byte[] getNationalUnity(String idToCotizacion, Integer idLinea, Integer idInciso) {
		byte[] pdf = null;
		
		try{
			pdf = this.getServicioImpresionED().getNationalUnityCertificateNew(idToCotizacion, idLinea, 
		        idInciso, "application/pdf");
		}catch(Exception ex){
			LOG.error(ex);
		}		
		return pdf;
	}
	
	/**
	 * Obtiene y devuelve un objeto del tipo PrintReport, por medio del Servicio Web de reciboss
	 * @return Devuelve un objeto del tipo PrintReport
	 */
	private PrintReport getServicioImpresionED(){		
		PrintReportServiceLocator locator = null;
		PrintReport pr = null;		
		try {
			String urlWsImp = SistemaPersistencia.URL_WS_IMPRESION;//"172.20.73.33";
			String puertoWsImp = SistemaPersistencia.PUERTO_WS_IMPRESION;//"9080";
			locator = new PrintReportServiceLocator(urlWsImp, puertoWsImp);
			pr = locator.getPrintReport();
		} catch (Exception e) {
			throw new RuntimeException("WebService de recibos no disponible");
		}		
		return pr;
	}

	private String getTipoEndosoDesc(ControlEndosoCot controlEndosoCot) {
		String descTipoEndoso = null;
		BigDecimal claveTipoEndoso2 = controlEndosoCot.getSolicitud().getClaveTipoEndoso();
		// Obtiene las descripciones de los endosos
		List<CatalogoValorFijoDTO> catalogoEndosoList = entidadService.findByProperty(CatalogoValorFijoDTO.class, "id.idGrupoValores", Integer.valueOf(331));
		CatalogoValorFijoDTO cvf =  new CatalogoValorFijoDTO();
		CatalogoValorFijoId cvf_id = new CatalogoValorFijoId();
		cvf_id.setIdDato(claveTipoEndoso2.intValue());
		cvf_id.setIdGrupoValores(331);
		cvf.setId(cvf_id);
		int catalogoEndosoIdx = catalogoEndosoList.indexOf(cvf);
		CatalogoValorFijoDTO endosoCVFdto = catalogoEndosoList.get(catalogoEndosoIdx);
		descTipoEndoso = endosoCVFdto.getDescripcion();
		return descTipoEndoso;
	}
	
	private List<MovimientoEndoso> filtrarEndososMismoTipo(List<MovimientoEndoso> movimientoEndosoList, 
			MovimientoEndoso movimientoEndoso) {
		LogDeMidasInterfaz.log("Entrando a filtrarEndososMismoTipo", Level.INFO, null);
		
		List<MovimientoEndoso> movimientoEndosoListFiltrada = new ArrayList<MovimientoEndoso>();
		
		if(movimientoEndoso != null){
			TipoMovimientoEndoso tipoMovimientoEndosoRaiz = movimientoEndoso.getTipo();
			//LogDeMidasInterfaz.log("tipoMovimientoEndosoRaiz => " + tipoMovimientoEndosoRaiz, Level.INFO, null);
		
			for (MovimientoEndoso movimientoEndosoCurrent : movimientoEndosoList) {
				if(movimientoEndosoCurrent.getTipo().equals(tipoMovimientoEndosoRaiz)){
					movimientoEndosoListFiltrada.add(movimientoEndosoCurrent);
				}
			}
		}else{
			MovimientoEndoso movimientoEndosoCurrent = new MovimientoEndoso();
			movimientoEndosoCurrent.setDescripcion("NO SE REGISTRO MOVIMIENTO");
			ControlEndosoCot controlEndosoCot = new ControlEndosoCot();
			SolicitudDTO solicitud = new SolicitudDTO();
			solicitud.setClaveTipoEndoso(BigDecimal.ZERO);
			controlEndosoCot.setSolicitud(solicitud);
			movimientoEndosoCurrent.setControlEndosoCot(controlEndosoCot);
			movimientoEndosoListFiltrada.add(movimientoEndosoCurrent);
		}
		
		//LogDeMidasInterfaz.log("listaFiltrada => " + movimientoEndosoListFiltrada, Level.INFO, null);
		
		LogDeMidasInterfaz.log("Entrando a filtrarEndososMismoTipo", Level.INFO, null);
		return movimientoEndosoListFiltrada;
	}

	//@SuppressWarnings("unchecked")
	private MovimientoEndoso filtrarEndosoRaiz(List<MovimientoEndoso> movimientoEndosoList) {
		LogDeMidasInterfaz.log("Entrando a filtrarEndosoRaiz", Level.INFO, null);
		
		LogDeMidasInterfaz.log("movimientoEndosoList => " + movimientoEndosoList, Level.INFO, null);
		
		MovimientoEndoso movimientoEndosoMenorId = null;
		try{
		List<MovimientoEndoso> listaFiltrada = new ArrayList<MovimientoEndoso>();
		
		Collections.sort(movimientoEndosoList, new Comparator<MovimientoEndoso>(){
			public int compare(MovimientoEndoso o1, MovimientoEndoso o2) {
				return (o1.getId().compareTo(o2.getId()));
			}
		});	
		
		movimientoEndosoMenorId = movimientoEndosoList.get(0);
		//LogDeMidasInterfaz.log("movimientoEndosoMenorId => " + movimientoEndosoMenorId, Level.INFO, null);
		}catch(Exception e){
			LOG.error(e.getMessage(), e);
		}
		
		LogDeMidasInterfaz.log("Saliendo de filtrarEndosoRaiz", Level.INFO, null);
		return movimientoEndosoMenorId;
	}
	
	@SuppressWarnings("unchecked")
	private String obtenerRedaccionDeMovimientos(List<MovimientoEndoso> movimientos, boolean esPolizaMigrada) {
		
		StringBuilder redaccion = new StringBuilder("");
		short claveTipoEndoso = 0;
		boolean esAltaInciso = false;
		boolean esBajaInciso = false;
		String llaveRecurso = "midas.impresion.endoso.altainciso.encabezado";
		
		List<MovimientoEndoso> movimientosPoliza;
		List<MovimientoEndoso> movimientosInciso;
		List<MovimientoEndoso> movimientosIncisoPorSeccion;
		List<MovimientoEndoso> movimientosCobertura;
		List<MovimientoEndoso> movimientosCondicionesEspIncisos = new ArrayList<MovimientoEndoso>();
		List<SeccionDTO> secciones;
		Map <SeccionDTO, Integer> distintasSecciones;
		
		if (movimientos != null && !movimientos.isEmpty()) {
			claveTipoEndoso = movimientos.get(0).getControlEndosoCot().getSolicitud().getClaveTipoEndoso().shortValue();
		}
		
		//Se procesan los encabezados de inciso unicamente si el endoso es de : Alta de Inciso, Baja de Inciso
		if (claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
			esAltaInciso = true;
		} else if (claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO) {
			esBajaInciso = true;
			llaveRecurso = "midas.impresion.endoso.bajainciso.encabezado";
		}else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL 
				|| claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL)
		{
			redaccion.append("Dado que el vehículo asegurado ha presentado un siniestro que amerita Pérdida total \n" + System.getProperty("line.separator"));
			//llaveRecurso = "midas.impresion.endoso.bajainciso.encabezado";
		}
		
		
		if (esAltaInciso || esBajaInciso) {
						
			Predicate predicateMovimientoInciso = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					MovimientoEndoso obj = (MovimientoEndoso)arg0; 
					return obj.getTipo().equals(TipoMovimientoEndoso.INCISO);
				}
			};
			
			Predicate predicateMovimientoPoliza = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					MovimientoEndoso obj = (MovimientoEndoso)arg0; 
					return obj.getTipo().equals(TipoMovimientoEndoso.POLIZA);
				}
			};
			
			Predicate predicateSeccionInciso = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					MovimientoEndoso obj = (MovimientoEndoso)arg0; 
					return obj.getBitemporalInciso().getContinuity().getIncisoSeccionContinuities().getValue().iterator().next()
						.getSeccion().getIdToSeccion().equals(ImpresionEndosoServiceImpl.getSeccionId());
				}
			};
			
			Predicate predicateMovimientoPadre = new Predicate() {
				@Override
				public boolean evaluate(Object arg0) {
					MovimientoEndoso obj = (MovimientoEndoso)arg0; 
					if (obj.getMovimientoEndoso() != null && obj.getMovimientoEndoso().getId() != null) {
						return obj.getMovimientoEndoso().getId().equals(ImpresionEndosoServiceImpl.getMovimientoId());
					} else {
						return false;
					}
				}
			};
			
			
			//Se filtran los movimientos generales (nivel Poliza) y en caso de encontrarse se imprimen
			movimientosPoliza = (List<MovimientoEndoso>) CollectionUtils.select(movimientos, predicateMovimientoPoliza);
			
			if (!movimientosPoliza.isEmpty()) {
				for (MovimientoEndoso movimientoPoliza : movimientosPoliza) {
					redaccion.append(movimientoPoliza.getDescripcion() + System.getProperty("line.separator"));
				}
			}
						
			//Se filtran los movimientos de inciso
			movimientosInciso = (List<MovimientoEndoso>) CollectionUtils.select(movimientos, predicateMovimientoInciso);
			
			List<MovimientoEndoso> movimientosSoloInciso = new ArrayList<MovimientoEndoso>();
			
			// # FILTRA SI EL TEXTO CONTIENE CONDICION ESPECIAL PARA AGREGARLO AL FINAL DE CADA SEGMENTO DE MOVIMIENTO DEL INCISO
			for (MovimientoEndoso movimientoInciso : movimientosInciso) {
				if( movimientoInciso.getDescripcion().contains("Condición Especial") ){
					movimientosCondicionesEspIncisos.add(movimientoInciso);
				}else{
					movimientosSoloInciso.add(movimientoInciso);
				}
			}
			
			//Se obtienen las Lineas de Negocio de todos los movimientos de inciso
			secciones = (List<SeccionDTO>) CollectionUtils.collect(movimientosSoloInciso, new Transformer() {
				@Override
				public Object transform(Object arg0) {
					MovimientoEndoso movimiento = (MovimientoEndoso) arg0;
	
					return movimiento.getBitemporalInciso().getContinuity().getIncisoSeccionContinuities()
					.getValue().iterator().next().getSeccion();
				}
			});

			//Se obtienen las distintas lineas de Negocio con el numero de movimientos que tiene cada una
			distintasSecciones = CollectionUtils.getCardinalityMap(secciones);
			
			
			//Por cada linea de Negocio, se pone un encabezado
			for (SeccionDTO seccion : distintasSecciones.keySet()) {
				
				redaccion.append(Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, llaveRecurso,
						distintasSecciones.get(seccion), seccion.getNombreComercial().trim()) + System.getProperty("line.separator"));
				
				ImpresionEndosoServiceImpl.setSeccionId(seccion.getIdToSeccion());
				
				movimientosIncisoPorSeccion = (List<MovimientoEndoso>) CollectionUtils.select(movimientosSoloInciso, predicateSeccionInciso);
				
				//Se agregan los movimientos de inciso de la linea de Negocio				
				for (MovimientoEndoso movimientoIncisoPorSeccion : movimientosIncisoPorSeccion) {
				
					movimientoIncisoPorSeccion.getTipo();
					
					// # SI EL TEXTO NO CONTIENE LA PALABRA CONDICION ESPECIAL SE AGREGA FORMATO ESPECIAL (ELSE), SI NO SOLO LA DESCRIPCION DE LA CONDICION
					if( movimientoIncisoPorSeccion.getDescripcion().contains("Condición Especial") ){
						redaccion.append( movimientoIncisoPorSeccion.getDescripcion() + System.getProperty("line.separator") );
					}else{
						redaccion.append(" ").append(this.obtenerDetalleInciso(movimientoIncisoPorSeccion, " ", esPolizaMigrada) + System.getProperty("line.separator"));
					}
					
					//A las Bajas de inciso no se le agregan los movimientos de baja de sus coberturas (solo a las Altas)
					if (esAltaInciso) {
						ImpresionEndosoServiceImpl.setMovimientoId(movimientoIncisoPorSeccion.getId());
						
						movimientosCobertura = (List<MovimientoEndoso>) CollectionUtils.select(movimientos, predicateMovimientoPadre);
						
						//Por cada movimiento de inciso, se ponen sus movimientos de cobertura
						for (MovimientoEndoso movimientoCobertura : movimientosCobertura) {
							redaccion.append(" ").append(movimientoCobertura.getDescripcion() + System.getProperty("line.separator"));
						}
						
						// # ITERA LA LISTA DE OBJETOS QUE CONTIENEN EL TEXTO CONDICION ESPECIAL PARA AGREGAR AL FNAL DEL BLOQUE DE TEXTO LA CONDICION AGREGADA EN LA DESCRIPCION
						for (MovimientoEndoso movimientoCondicion : movimientosCondicionesEspIncisos) {
							// # SI EL ID QUE VIENE DE movimientosIncisoPorSeccion ES IGUAL AL QUE ESTA DENTRO DEL ARRAY movimientosCondicionesEspIncisos SE PINTA LA CONDICION ESPECIAL
							if ( movimientoIncisoPorSeccion.getBitemporalInciso().getId() ==  movimientoCondicion.getBitemporalInciso().getId() ){
								redaccion.append(" ").append( movimientoCondicion.getDescripcion() + System.getProperty("line.separator") );
							}
						}
						
						
					}
					
				}
				
			}
			
		} else {
			
			for (MovimientoEndoso movimiento : movimientos) {
				
				if (movimiento.getTipo().equals(TipoMovimientoEndoso.INCISO)) {
					redaccion.append(this.obtenerDetalleInciso(movimiento, "", esPolizaMigrada) + System.getProperty("line.separator"));				
				} else {
					redaccion.append(movimiento.getDescripcion() + System.getProperty("line.separator"));
				}
				
			}
			
		}
		
		return redaccion.toString();
	}
	

	private String obtenerRedaccionAnexos(Long cotizacionContinuityId, DateTime validOn, DateTime knownOn, ControlEndosoCot controlEndosoCot)
	{
		StringBuilder stringBuilder = new StringBuilder();
		Collection<BitemporalDocAnexoCot> listaAnexos = new ArrayList<BitemporalDocAnexoCot>();
		
		if(controlEndosoCot != null)
		{
			Map<String,Object> params = new HashMap<String,Object>();
			params.put("continuity.cotizacionContinuity.id", cotizacionContinuityId);
			params.put("recordInterval.from", controlEndosoCot.getRecordFrom());
			listaAnexos = entidadBitemporalService.listarFiltrado(BitemporalDocAnexoCot.class, params, validOn, knownOn, false);			
		}
		
		for(BitemporalDocAnexoCot anexo:listaAnexos)
		{
			stringBuilder.append("- " + anexo.getValue().getDescripcionDocumentoAnexo() + "\n");			
		}		
		
		String redaccion = stringBuilder.toString();
		
		return redaccion;		
	}

	private String getIdentificadorCaratulaPoliza(BitemporalCotizacion bCotizacion) {
		//Identificador
		//Centro Emisor - Gerencia - Oficina - Promotoria - Clave Agente
		String identificador = "";
		if (bCotizacion.getValue().getSolicitud().getCodigoAgente() != null) {
			String idAgente = "000";
			try {
			Agente agente = new Agente();
			agente.setId(bCotizacion.getValue().getSolicitud().getCodigoAgente().longValue());
			agente = agenteMidasService.loadById(agente);
			idAgente = agente.getIdAgente().toString();
			if (agente != null) {
				if (agente.getPromotoria().getEjecutivo().getGerencia().getCentroOperacion().getIdCentroOperacion() != null) {
					identificador += agente.getPromotoria().getEjecutivo()
							.getGerencia().getCentroOperacion().getIdCentroOperacion() + "-";
				} else {
					identificador += "000-";
				}
				if (bCotizacion.getValue().getSolicitud().getCodigoEjecutivo() != null) {
					identificador += bCotizacion.getValue().getSolicitud().getCodigoEjecutivo() + "-";
				} else {
					identificador += "000-";
				}
				if (bCotizacion.getValue().getSolicitud().getIdOficina() != null) {
					identificador += bCotizacion.getValue().getSolicitud().getIdOficina() + "-";
				} else {
					identificador += "000-";
				}
				if (agente.getPromotoria().getIdPromotoria() != null) {
					identificador += agente.getPromotoria().getIdPromotoria() + "-";
				} else {
					identificador += "0000-";
				}
				if (bCotizacion.getValue().getSolicitud().getCodigoAgente() != null) {
					identificador += idAgente + "";
				} else {
					identificador += "0";
				}
			} else {
				identificador = "000-000-000-0000-" + idAgente;;
			}
			} catch(Exception e) {
				identificador = "000-000-000-0000-" + idAgente;
			}
		}
		
		return identificador;
	}
	
	private void pdfConcantenate(List<byte[]> inputByteArray,
			ByteArrayOutputStream outputStream) {

	      try {
	          int pageOffset = 0;    
	          int f = 0;     
	          ArrayList master = new ArrayList();
	          Document document = null;
	          PdfCopy  writer = null;	          
	          // we create a reader for a certain document	          
	          Iterator iterator = inputByteArray.iterator();	          
	          while(iterator.hasNext()){
	            byte[] data = (byte[])iterator.next();
	            if(data == null || data.length == 0){
	              f++;
	              continue;
	            }
	            PdfReader reader = new PdfReader(data);
	            reader.consolidateNamedDestinations();
	            // we retrieve the total number of pages
	            int n = reader.getNumberOfPages();
	            List bookmarks = SimpleBookmark.getBookmark(reader);
	            if (bookmarks != null) {
	              if (pageOffset != 0)
	                SimpleBookmark.shiftPageNumbers(bookmarks, pageOffset, null);
	                master.addAll(bookmarks);
	            }	            
	            pageOffset += n;	                
	            if (f == 0) {
	              // step 1: creation of a document-object
	              document = new Document(reader.getPageSizeWithRotation(1));
	              // step 2: we create a writer that listens to the document                          
	              writer = new PdfCopy(document, outputStream);
	              // step 3: we open the document
	              document.open();           
	            }	              
	            // step 4: we add content
	            PdfImportedPage page;
	            for (int i = 0; i < n; ) {
	              ++i;
	              page = writer.getImportedPage(reader, i);
	              writer.addPage(page);
	            }	  
	            PRAcroForm form = reader.getAcroForm();
	            if (form != null){
	              writer.copyAcroForm(reader);
	            }
	            f++;
	          }
	          if (!master.isEmpty()){
	            writer.setOutlines(master);
	          }
	          // step 5: we close the document
	          if(document != null){
	            document.close();
	          }
	      }
	      catch(Exception e) {	   
	    	  ;
	      }
	}
	
	private String obtenerDetalleInciso(MovimientoEndoso movimientoInciso, String separador, boolean esPolizaMigrada)
	{
		BitemporalInciso bitemporalInciso= movimientoInciso.getBitemporalInciso();
		if (bitemporalInciso != null) {
			fillAutoInciso(bitemporalInciso);
			
			NegocioPaqueteSeccion paqueteSeccion = entidadService.findById
			(NegocioPaqueteSeccion.class, bitemporalInciso.getValue().getAutoInciso().getNegocioPaqueteId());
			
			if (esPolizaMigrada) {
				return Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.impresion.endoso.detalleinciso.seycos",
						bitemporalInciso.getContinuity().getNumero(),
						impresionBitemporalService.obtenerNumeroIncisoSeycos(bitemporalInciso, "-"),
						bitemporalInciso.getValue().getAutoInciso().getDescripcionFinal(),
						paqueteSeccion.getNegocioSeccion().getSeccionDTO().getNombreComercial().trim(),
						System.getProperty("line.separator") + separador + movimientoInciso.getDescripcion());
			} else {
				return Utilerias.getMensajeRecurso(
						SistemaPersistencia.ARCHIVO_RECURSOS, "midas.impresion.endoso.detalleinciso",
						bitemporalInciso.getContinuity().getNumero(),
						bitemporalInciso.getValue().getAutoInciso().getDescripcionFinal(),
						paqueteSeccion.getNegocioSeccion().getSeccionDTO().getNombreComercial().trim(),
						System.getProperty("line.separator") + separador + movimientoInciso.getDescripcion());
			}
		} else {
			return movimientoInciso.getDescripcion();
		}
		
	}	
	
	private void fillAutoInciso(BitemporalInciso bitemporalInciso) {

		BitemporalAutoInciso biAutoInciso = incisoViewService.getAutoInciso(bitemporalInciso.getContinuity().getId(),
																				new Date(bitemporalInciso.getValidityInterval().getInterval().getStartMillis()), 
																				new Date(bitemporalInciso.getRecordInterval().getInterval().getStartMillis()),
																				EndosoDTO.TIPO_ENDOSO_CANCELACION);
		if (biAutoInciso != null) {
			bitemporalInciso.getValue().setAutoInciso(biAutoInciso.getValue());
		}
	}
	
	private String getLeyendaISyF(Date fechaRegistro, String numeroRegistro){
		SimpleDateFormat sdf = new SimpleDateFormat("dd 'de' MMMMM 'del' yyyy", new Locale("es", "MX"));
		String leyendaCNSF = "En cumplimiento a lo dispuesto en el art\u00EDculo 202 de la Ley de Instituciones de Seguros y de Fianzas, la documentaci\u00F3n contractual " +
				"y la nota t\u00E9cnica que integran este producto de seguro, quedaron registradas ante la Comisi\u00F3n Nacional de Seguros y Fianzas, " +
				"a partir del d\u00EDa "+sdf.format(fechaRegistro)+", con el n\u00FAmero "+numeroRegistro+".";
		return leyendaCNSF;
	}
	
	private String getLeyendaPrimaTotal(Double primaTotal)
	{
		String leyenda = "";
		
		if(primaTotal > 0)
		{
			leyenda = String.format(LEYENDA_PRIMATOTAL_CARGO_CONTRATANTE, 
					NumberFormat.getCurrencyInstance().format(primaTotal)); 
			
		}else if(primaTotal < 0)
		{
			leyenda = String.format(LEYENDA_PRIMATOTAL_A_FAVOR_CONTRATANTE, 
					NumberFormat.getCurrencyInstance().format(Math.abs(primaTotal)));
		}
		
		return leyenda;
	}
	
	private String getLeyendaFinal(short claveTipoEndoso) {
		//Se agrega una frase final a todos los endosos excepto el de Cancelacion de Poliza
		if (claveTipoEndoso != SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA) {
			return Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.impresion.endoso.oracionfinal") 
					+ System.getProperty("line.separator");
		} else {
			return "";
		}
	}
	
	@Override
	public TransporteImpresionDTO imprimirDesgloseCoberturas(
			BitemporalCotizacion cotizacion, String incisoContinuityId,
			PolizaDTO poliza, ResumenCostosDTO resumen, Date validoEn) {
		
		TransporteImpresionDTO reporte = new TransporteImpresionDTO();
		List<DatosCoberturasDTO> dataSourceCoberturas = new ArrayList<DatosCoberturasDTO>();
		DatosCaratulaInciso DatosCaratula = new DatosCaratulaInciso();
		DatosIncisoDTO datosInciso = new DatosIncisoDTO();
		List<MovimientoEndoso> movimientos = new ArrayList<MovimientoEndoso>();
		int contadorCoberturas = 1;

		ControlEndosoCot controlEndosoCot = endosoService
				.obtenerControlEndosoCotCotizacion(poliza.getCotizacionDTO()
						.getIdToCotizacion());

		Map<String, Object> values = new LinkedHashMap<String, Object>();

		if (controlEndosoCot != null) {
			values.put("controlEndosoCot.id", controlEndosoCot.getId());
			values.put("tipo", TipoMovimientoEndoso.COBERTURA);

			movimientos = entidadService.findByProperties(
					MovimientoEndoso.class, values);
		}

		Iterator<MovimientoEndoso> itMovimientos = movimientos.iterator();
		while (itMovimientos.hasNext()) {
			MovimientoEndoso movimiento = itMovimientos.next();  
			
			BigDecimal valorPrimaNeta = BigDecimal.ZERO;
			if(movimiento.getValorMovimiento() != null)
			{
				valorPrimaNeta = movimiento.getFactorCobranzaPrimaNeta()!= null?
						movimiento.getValorMovimiento().multiply(movimiento.getFactorCobranzaPrimaNeta())
						:movimiento.getValorMovimiento();				
			}
						
			if (valorPrimaNeta.compareTo(BigDecimal.ZERO) != 0) {
				DatosCoberturasDTO datosCobertura = new DatosCoberturasDTO();
				datosCobertura.setRiesgo(contadorCoberturas);
				datosCobertura.setNombreCobertura(movimiento
						.getBitemporalCoberturaSeccion().getContinuity().getCoberturaDTO().getNombreComercial());
				datosCobertura.setPrimaCobertura(valorPrimaNeta.doubleValue());
				datosCobertura.setDeducible(null);
				datosCobertura.setSumaAsegurada(null);
				dataSourceCoberturas.add(datosCobertura);

				contadorCoberturas++;
			}
		}

		IncisoContinuity incisoContinuity = null;
		
		if(incisoContinuityId != null && !incisoContinuityId.isEmpty())
		{
			incisoContinuity = entidadContinuityService.findContinuityByBusinessKey(
					IncisoContinuity.class, "id", Long.valueOf(incisoContinuityId));			
		}		

		datosInciso.setNumeroPoliza(poliza.getNumeroPolizaFormateada());
		datosInciso.setIdInciso(incisoContinuity != null ?String.valueOf(incisoContinuity.getNumero()):"N.D.");
		datosInciso.setNumeroEndoso(cotizacion.getValue().getNumeroEndoso());
		datosInciso.setFechaInicioVigencia(cotizacion.getValue()
				.getFechaInicioVigencia());
		datosInciso.setFechaFinVigencia(cotizacion.getValue()
				.getFechaFinVigencia());
		datosInciso.setNombreProducto(cotizacion.getValue()
				.getSolicitud().getProductoDTO().getNombreComercial());
		datosInciso.setTipoInciso("P\u00F3liza:");  

		if (resumen != null) {
			datosInciso
					.setPrimaNetaCotizacion(resumen.getPrimaNetaCoberturas() != null ? resumen
							.getPrimaNetaCoberturas() : 0.00);
			datosInciso.setDescuentoComisionCedida(resumen
					.getDescuentoComisionCedida() != null ? resumen
					.getDescuentoComisionCedida() : 0.00);
			datosInciso
					.setDescuento(resumen.getDescuentoComisionCedida() != null ? resumen
							.getDescuentoComisionCedida() : 0.00);
			datosInciso
					.setDerechosPoliza((resumen.getDerechos() != null) ? resumen
							.getDerechos() : 0.00);
			datosInciso
					.setPrimaNetaTotal((resumen.getPrimaTotal() != null) ? resumen
							.getPrimaTotal() : 0.00);
			datosInciso.setDescuento(resumen.getDescuento());
			datosInciso
					.setTotalPrimas((resumen.getTotalPrimas() != null) ? resumen
							.getTotalPrimas() : 0.00);
			datosInciso
					.setMontoRecargoPagoFraccionado((resumen.getRecargo() != null) ? resumen
							.getRecargo() : 0.00);
			datosInciso.setMontoIVA(resumen.getIva() != null ? resumen.getIva()
					: 0.00);
		}

		DatosCaratula.setDataSourceIncisos(datosInciso);
		DatosCaratula.setDataSourceCoberturas(dataSourceCoberturas);

		byte[] pdf = this.imprimirIncisoDesgloseCoberturas(DatosCaratula, null);
		reporte.setGenericInputStream(new ByteArrayInputStream(pdf));
		reporte.setByteArray(pdf);
		reporte.setContentType("application/pdf");
		reporte.setFileName("Desglose de primas " + datosInciso.getNumeroPoliza() + ".pdf");

		return reporte;
	}
	
	private byte[] imprimirIncisoDesgloseCoberturas(DatosCaratulaInciso inciso, Locale locale) {
		byte[] pdfInciso = null;

		try {
			InputStream caratulaIncisoStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/CaratulaDesgloseCoberturas.jrxml");
			JasperReport reporteInciso = JasperCompileManager
					.compileReport(caratulaIncisoStream);
			InputStream desgloseCoberturasStream = this
					.getClass()
					.getResourceAsStream(
							"/mx/com/afirme/midas2/service/impl/impresiones/jrxml/DesgloseCoberturasM2.jrxml");
			JasperReport reporteCoberturas = JasperCompileManager
					.compileReport(desgloseCoberturasStream);
			Map<String, Object> parameters = new HashMap<String, Object>();
			// Por cada uno de estos DatosIncisoDTO se hará una página
			List<DatosIncisoDTO> reporteDatasourceInciso = new ArrayList<DatosIncisoDTO>();
			DatosIncisoDTO diDTO = inciso.getDataSourceIncisos();
			reporteDatasourceInciso.add(diDTO);
			if (locale != null) {
				parameters.put(JRParameter.REPORT_LOCALE, locale);
			}
			parameters.put("P_IMAGE_LOGO",
					SistemaPersistencia.LOGO_SEGUROS_AFIRME);
		
			// Estas son las coberturas para cada inciso
			List<DatosCoberturasDTO> dcDTOList = inciso
					.getDataSourceCoberturas();
			parameters.put("coberturasDataSource", dcDTOList);
			parameters.put("coberturasReport", reporteCoberturas);
			parameters.put(JRParameter.REPORT_VIRTUALIZER,
					new JRGzipVirtualizer(2));
			JRBeanCollectionDataSource dsInciso = new JRBeanCollectionDataSource(
					reporteDatasourceInciso);
			pdfInciso = JasperRunManager.runReportToPdf(reporteInciso,
					parameters, dsInciso);
		} catch (JRException ex) {
			throw new RuntimeException(ex);
		}
		return pdfInciso;
	}
	
	private String obtenerDetalleIncisoPerdidaTotal(ControlEndosoCot controlEndosoCot)
	{
		StringBuilder redaccion = new StringBuilder("");
		
		//obtener Endoso
		EndosoDTO endoso;
        
		Map<String, Object> params = new LinkedHashMap<String, Object>();
		params.put("idToCotizacion", controlEndosoCot.getCotizacionId());
		params.put("validFrom", controlEndosoCot.getValidFrom());
		params.put("recordFrom", controlEndosoCot.getRecordFrom());
		
		endoso = entidadService.findByProperties(EndosoDTO.class, params).get(0);	
		
		SiniestroCabina siniestro = entidadService.findById(SiniestroCabina.class, endoso.getIdSiniestroPT());	
		  
		if(controlEndosoCot.getSolicitud().getClaveTipoEndoso().shortValue() == SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL){
			redaccion.append("Descripcion: " + siniestro.getReporteCabina().getSeccionReporteCabina()
					.getIncisoReporteCabina().getAutoIncisoReporteCabina().getDescripcionFinal() + "\n");
		}
		
		redaccion.append("Serie: " + siniestro.getReporteCabina().getSeccionReporteCabina()
				.getIncisoReporteCabina().getAutoIncisoReporteCabina().getNumeroSerie() + "\n" );
		redaccion.append("No. De siniestro: " + siniestro.getNumeroSiniestro() + " \n");
		redaccion.append("Concepto: " + (controlEndosoCot.getSolicitud().getClaveMotivoEndoso()
				.equals(SolicitudDTO.CVE_MOTIVO_ENDOSO_PERDIDA_TOTAL)?  
						Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.impresion.endoso.perdidaTotal.concepto.pt"):
									Utilerias.getMensajeRecurso(
								SistemaPersistencia.ARCHIVO_RECURSOS, "midas.impresion.endoso.perdidaTotal.concepto.pd")) + "\n");
	
		
		return redaccion.toString();
	}
	
	public static BigDecimal getSeccionId() {
		return seccionId;
	}

	public static void setSeccionId(BigDecimal seccionId) {
		ImpresionEndosoServiceImpl.seccionId = seccionId;
	}
	

	public static Long getMovimientoId() {
		return movimientoId;
	}

	public static void setMovimientoId(Long movimientoId) {
		ImpresionEndosoServiceImpl.movimientoId = movimientoId;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService){
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setEntidadBitemporalService(EntidadBitemporalService entidadBitemporalService){
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@EJB
	public void setDomicilioFacadeRemote(DomicilioFacadeRemote domicilioFacadeRemote) {
		this.domicilioFacadeRemote = domicilioFacadeRemote;
	}
	
	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote){
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
		
	@EJB
	public void setCalculoService(CalculoService calculoService){
		this.calculoService = calculoService;
	}
	
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	@EJB
	public void setEntidadContinuityService(EntidadContinuityService entidadContinuityService){
		this.entidadContinuityService = entidadContinuityService;
	}

	@EJB
	public void setImpresionBitemporalService(ImpresionBitemporalService impresionBitemporalService) {
		this.impresionBitemporalService = impresionBitemporalService;
	}

	@EJB
	public void setListadoIncisosDinamicoService(ListadoIncisosDinamicoService listadoIncisosDinamicoService) {
		this.listadoIncisosDinamicoService = listadoIncisosDinamicoService;
	}

	@EJB
	public void setIncisoViewService(IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}

	public GeneraPlantillaReporteBitemporalService getGeneraPlantillaReporteBitemporalService() {
		return generaPlantillaReporteBitemporalService;
	}

	public void setGeneraPlantillaReporteBitemporalService(
			GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService) {
		this.generaPlantillaReporteBitemporalService = generaPlantillaReporteBitemporalService;
	}

	public CondicionEspecialService getCondicionEspecialService() {
		return condicionEspecialService;
	}

	public void setCondicionEspecialService(
			CondicionEspecialService condicionEspecialService) {
		this.condicionEspecialService = condicionEspecialService;
	}

	public CondicionEspecialBitemporalService getCondicionEspecialBitemporalService() {
		return condicionEspecialBitemporalService;
	}

	public void setCondicionEspecialBitemporalService(
			CondicionEspecialBitemporalService condicionEspecialBitemporalService) {
		this.condicionEspecialBitemporalService = condicionEspecialBitemporalService;
	}

	public MovimientoEndosoDao getMovimientoEndosoDao() {
		return movimientoEndosoDao;
	}

	public void setMovimientoEndosoDao(MovimientoEndosoDao movimientoEndosoDao) {
		this.movimientoEndosoDao = movimientoEndosoDao;
	}

	/**
	 * @return el seguroObligatorioService
	 */
	public SeguroObligatorioService getSeguroObligatorioService() {
		return seguroObligatorioService;
	}

	/**
	 * @param seguroObligatorioService el seguroObligatorioService a establecer
	 */
	@EJB
	public void setSeguroObligatorioService(
			SeguroObligatorioService seguroObligatorioService) {
		this.seguroObligatorioService = seguroObligatorioService;
	}

	
}
