package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.util.List;

import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.fuerzaventa.RegistroFuerzaDeVentaDTO;
import mx.com.afirme.midas2.service.fuerzaventa.OficinaService;

@Stateless
public class OficinaServiceImpl extends FuerzaDeVentaServiceImpl implements OficinaService {

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarOficinas() {
		return super.listar(null, null, 
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.OFICINA.obtenerTipo());
	}

	@Override
	public List<RegistroFuerzaDeVentaDTO> listarOficinasPorGerencia(Object id) {
		return super.listar(id,
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.GERENCIA.obtenerTipo(),
				FuerzaDeVentaServiceImpl.TipoFuerzaDeVenta.OFICINA.obtenerTipo());
	}

}
