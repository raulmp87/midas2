package mx.com.afirme.midas2.service.impl.tarea.informacionVehicular;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.log4j.Logger;

import mx.com.afirme.midas2.service.informacionVehicular.InformacionVehicularLotesService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.tareas.informacionVehicular.EnvioVINFlotillasService;

@Stateless
public class EnvioVINFlotillasServiceImpl implements EnvioVINFlotillasService {
	private static final Logger LOG = Logger.getLogger(ValidacionVINFlotillasServiceImpl.class);
	
	@EJB
	private InformacionVehicularLotesService informacionVehicularLotesService;
	
	@Resource	
	private TimerService timerService;
	
	@EJB
	private SistemaContext sistemaContext;

	@Override
	public void enviarVINFlotillas() {
		try {	
			LOG.info("Log Informacion Vehicular Lotes: Ejecutando tarea programada enviaInformacionVehicular");
			//llamar a la clase que ejecuta el envio de VIN por flotillas
			informacionVehicularLotesService.enviaInformacionVehicular();
			LOG.info("Log Informacion Vehicular Lotes: Tarea programada enviaInformacionVehicular ejecutada");
		} catch (Exception e) {
			LOG.error("Log Informacion Vehicular Lotes: Fallo ejecucion de tarea programada enviaInformacionVehicular", e);
		}
	}
	
	public void initialize() {
		String timerInfo = "TimerEnviaVINFlotillas";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 30 1 * * ?
				expression.minute(30);
				expression.hour(1);
				expression.dayOfMonth("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerEnviaVINFlotillas", false));
				
				LOG.info("Tarea TimerEnviaVINFlotillas configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerEnviaVINFlotillas");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerEnviaVINFlotillas:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		enviarVINFlotillas();
	}

}
