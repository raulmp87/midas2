package mx.com.afirme.midas2.service.impl.ReporteAgentes;

import static mx.com.afirme.midas2.utils.CommonUtils.*;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.ScheduleExpression;
import javax.ejb.Stateless;
import javax.ejb.Timeout;
import javax.ejb.Timer;
import javax.ejb.TimerConfig;
import javax.ejb.TimerService;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import mx.com.afirme.midas2.domain.jobAgentes.TareaProgramada;
import mx.com.afirme.midas2.domain.reporteAgentes.ConfigReporteAgenteDTO;
import mx.com.afirme.midas2.domain.reporteAgentes.LogReporteAgenteDTO;
import mx.com.afirme.midas2.domain.reporteAgentes.ReporteAgenteDTO;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.reporteAgentes.ProgramacionReporteAgenteDelegate;
import mx.com.afirme.midas2.service.reporteAgentes.ProgramacionReporteAgenteService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MidasException;

@Stateless
public class ProgramacionReporteAgenteServiceImpl implements ProgramacionReporteAgenteService {

	@EJB
	private EntidadService entidad;
	@EJB
	private ListadoService listadoService;
	@EJB
	private ProgramacionReporteAgenteDelegate programacionReporteAgenteDelegate;
	@Resource	
	private TimerService timerService;	
	@EJB
	private SistemaContext sistemaContext;
	public static final Logger LOG = Logger.getLogger(ProgramacionReporteAgenteServiceImpl.class);

	@Override
	public List<TareaProgramada> getTaskToDo(String conceptoEjecucionAutomatica) {
		try {
		List<ConfigReporteAgenteDTO> configReportes = entidad
				.findAll(ConfigReporteAgenteDTO.class);
		List<TareaProgramada> tasks = null;
		if (configReportes != null) {
			tasks = new ArrayList<TareaProgramada>();
			Map<Long, String> mapValorCatalogo = listadoService
					.mapValorCatalogoAgente("Periodicidad-Reportes");
			if(!isEmptyList(configReportes)){
				for (ConfigReporteAgenteDTO config : configReportes) {
					if(isNotNull(config)){
						Long idReporte=(isNotNull(config) && isNotNull(config.getReporteAgenteDTO()))?config.getReporteAgenteDTO().getId():null;
						//List<LogReporteAgenteDTO> logs=entidad.findByProperty(LogReporteAgenteDTO.class,"reporteAgenteDTO.id",idReporte);
						Map<String,Object> params=new HashMap<String, Object>();
						params.put("reporteAgenteDTO.id", idReporte);
						List<LogReporteAgenteDTO> logs=entidad.findByPropertiesWithOrder(LogReporteAgenteDTO.class,params,"fechaEjecucion");
						LogReporteAgenteDTO logReporteAgenteDTO =(!isEmptyList(logs))?logs.get(0):null;
						//Si es nulo, es pk nunca fue ejecuta o no esta en el log y es la primera vez,
						//O si se encuentra, pero no fue ejecutado en el mismo dia de hoy.
						if (isNull(logReporteAgenteDTO) || !DateUtils.isSameDay(logReporteAgenteDTO.getFechaEjecucion(), new Date())) {
							TareaProgramada task = new TareaProgramada();
							task.setId(config.getReporteAgenteDTO().getId());
							task.setFechaEjecucion(obtenerFecha(mapValorCatalogo.get(config.getPeriodicidad())));
							tasks.add(task);
						}
					}
				}
			}
		}
		return tasks;
		} catch (RuntimeException error) {
			error.printStackTrace();
		}
		return null;
	}

	@Override
	public void executeTasks() {
		LOG.info("Ejecutando ProgramacionReporteAgenteService.executeTasks()...");
		List<TareaProgramada> tasks = getTaskToDo(null);
		if (!isEmptyList(tasks)) {
			for (TareaProgramada task : tasks) {
				if (isNotNull(task) && DateUtils.isSameDay(new Date(), task.getFechaEjecucion())) {
					// TODO llamar el delegate y pasarle el id del reporte
					Long id=task.getId();
					ReporteAgenteDTO reporte=entidad.findById(ReporteAgenteDTO.class,id);
					if(isNotNull(reporte)){
						try {
							Thread t = new Thread(new Ejecutable(reporte));
							t.start();
						} catch (RuntimeException e) {
							String nombre=(isNotNull(reporte)&& isValid(reporte.getNombre()))?reporte.getNombre():"";
							System.out.println("Error al cargar los datos del reporte ["+nombre+"]");
							e.printStackTrace();
						}
					}
				}
			}
		}
	}

	/**
	 * Genera la fecha en que se tiene que ejecutar una tarea
	 * 
	 * @param periodicidad
	 * @return fecha de ejecucion
	 */
	private Date obtenerFecha(String periodicidad) {
		Calendar today = Calendar.getInstance();
		Calendar supportDay = Calendar.getInstance();
		if (StringUtils.equals(periodicidad, "DIARIO")) {
			return today.getTime();
		}
		if (StringUtils.equals(periodicidad, "DIA ULTIMO")) {
			supportDay.set(today.get(Calendar.YEAR), today.get(Calendar.MONTH),
					today.getActualMaximum(Calendar.DAY_OF_MONTH));
			return supportDay.getTime();
		}
		if (StringUtils.equals(periodicidad, "DIA PRIMERO")) {
			supportDay.set(today.get(Calendar.YEAR),
					(today.get(Calendar.MONTH) + 1), 1);
			return supportDay.getTime();
		}
		return null;
	}

	class Ejecutable implements Runnable {
		private ReporteAgenteDTO reporte;

		public Ejecutable(ReporteAgenteDTO reporte) {
			this.reporte = reporte;
		}

		@Override
		public void run() {
			try {
				programacionReporteAgenteDelegate
						.cargarDatosInicialesPorReporte(reporte);
			} catch (MidasException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void initialize() {
		String timerInfo = "TimerProgramadaProgramacionReporteAgente";
		cancelarTemporizador(timerInfo);
		iniciarTemporizador();
	}
	
	public void iniciarTemporizador() {
		if(sistemaContext.getTimerActivo()) {
			ScheduleExpression expression = new ScheduleExpression();
			try {
				//0 0 20 * * ?
				expression.minute(0);
				expression.hour(20);
				expression.dayOfMonth("*");
				
				timerService.createCalendarTimer(expression, new TimerConfig("TimerProgramadaProgramacionReporteAgente", false));
				
				LOG.info("Tarea TimerProgramadaProgramacionReporteAgente configurado");
			} catch (Exception e) {
				LOG.error("Error al configurar Timer:" + e.getMessage(), e);
			}
		}
	}
	
	public void cancelarTemporizador(String timerInfo) {
		LOG.info("Cancelar Tarea TimerProgramadaProgramacionReporteAgente");
		try {
			if (timerService.getTimers() != null) {
				for (Timer timer : timerService.getTimers())
					if (timer.getInfo() != null
							|| timer.getInfo().equals(timerInfo))
						timer.cancel();
			}
		} catch (Exception e) {
			LOG.error("Error al detener TimerProgramadaProgramacionReporteAgente:" + e.getMessage(), e);
		}
	}
	
	@Timeout
	public void execute() {
		executeTasks();
	}
}
