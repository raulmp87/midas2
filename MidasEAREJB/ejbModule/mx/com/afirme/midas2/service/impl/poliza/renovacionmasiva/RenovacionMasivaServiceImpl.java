package mx.com.afirme.midas2.service.impl.poliza.renovacionmasiva;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Properties;
import java.util.zip.Deflater;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.catalogos.codigo.postal.EstadoDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.PaisDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO_;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.catalogos.tipobienautos.TipoBienAutosDTO_;
import mx.com.afirme.midas.catalogos.tipousovehiculo.TipoUsoVehiculoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.comision.ComisionCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocAnexoCotDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoAnexoReaseguroCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.DocumentoDigitalCotizacionDTO;
import mx.com.afirme.midas.cotizacion.documento.TexAdicionalCotDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.StoredProcedureErrorLog;
import mx.com.afirme.midas.interfaz.StoredProcedureHelper;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.documento.DocumentoDigitalSolicitudDTO;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.DatoSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.texto.TexAdicionalCotContinuity;
import mx.com.afirme.midas2.domain.catalogos.DomicilioImpresion;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.formapago.NegocioFormaPago;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.estilovehiculo.NegocioEstiloVehiculo;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.NegocioCobPaqSeccion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacion;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionCobertura;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionCoberturaId;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDesc;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionDescId;
import mx.com.afirme.midas2.domain.negocio.renovacion.NegocioRenovacionId;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioMunicipio;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasiva;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDet;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.OrdenRenovacionMasivaDetId;
import mx.com.afirme.midas2.domain.poliza.renovacionmasiva.bitacoraguia.RegistroGuiasProveedorId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.suscripcion.endoso.control.ControlEndosoCot;
import mx.com.afirme.midas2.domain.suscripcion.solicitud.comentarios.Comentario;
import mx.com.afirme.midas2.dto.ControlDescripcionArchivoDTO;
import mx.com.afirme.midas2.dto.LayoutRenovacion;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.poliza.renovacionmasiva.ValidacionRenovacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporal;
import mx.com.afirme.midas2.service.poliza.renovacionmasiva.RenovacionMasivaService;
import mx.com.afirme.midas2.utils.SimpleVerifier;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import ch.ethz.ssh2.Connection;
import ch.ethz.ssh2.InteractiveCallback;
import ch.ethz.ssh2.KnownHosts;
import ch.ethz.ssh2.SFTPv3Client;
import ch.ethz.ssh2.SFTPv3FileHandle;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;


@Stateless
@SuppressWarnings("rawtypes")
public class RenovacionMasivaServiceImpl extends RenovacionMasivaSoporteService implements RenovacionMasivaService {

	public static final Logger LOG = Logger.getLogger(RenovacionMasivaServiceImpl.class);
	private static final Double valorMinimoPorcentaje = 3.0;
	
	@Override
	public PolizaDTO getPolizaDTOById(BigDecimal idToPoliza){
		return entidadService.findById(PolizaDTO.class, idToPoliza);
	}
	
	@Override
	public CotizacionDTO obtenerCotizacionPorId(BigDecimal idToCotizacion){
		return entidadService.findById(CotizacionDTO.class, idToCotizacion);
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<CotizacionDTO> obtieneCotizacionesPorPolizaAnterior(BigDecimal idToPoliza){
		try{
		String query = "Select t1 FROM CotizacionDTO t1  " +
				" WHERE t1.solicitudDTO.idToPolizaAnterior =  " + idToPoliza + "" +
				" AND t1.claveEstatus IN ("+ CotizacionDTO.ESTATUS_COT_EN_PROCESO + ", " + CotizacionDTO.ESTATUS_COT_TERMINADA +", " + CotizacionDTO.ESTATUS_COT_EMITIDA +" )" +
				" ";
		return (List<CotizacionDTO>) entidadService.executeQueryMultipleResult(query, null);
		}catch(Exception e){
			LOG.error(e.getMessage());
			return entidadService.findByProperty(CotizacionDTO.class, "solicitudDTO.idToPolizaAnterior", idToPoliza);
		}
		
	}
	
	@Override
	public BitemporalCotizacion obtieneBitemporalCotizacionByIdCotizacion(BigDecimal idToCotizacion){
		return entidadBitemporalService.getByBusinessKey(CotizacionContinuity.class,CotizacionContinuity.BUSINESS_KEY_NAME, idToCotizacion);
	}
	
	@Override
	public NegocioRenovacion obtieneNegocioRenovacionById(NegocioRenovacionId id){
		return entidadService.findById(NegocioRenovacion.class, id);
	}
	
	@Override
	public List<MonedaDTO> listarMonedasAsociadasTipoPoliza(BigDecimal idToTipoPoliza){
		return monedaFacadeRemote.listarMonedasAsociadasTipoPoliza(idToTipoPoliza);
	}
	
	@Override
	public List<NegocioFormaPago> obtieneNegocioFormaPagoByFormaPagoNegocioProducto(BigDecimal idFormaPago, Long idToNegProducto){
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("formaPagoDTO.idFormaPago", idFormaPago);
		parametros.put("negocioProducto.idToNegProducto", idToNegProducto);
		return entidadService.findByProperties(NegocioFormaPago.class, parametros);
	}
	
	@Override
	public List<NegocioDerechoPoliza> obtieneNegocioDerechoPolizaByNegocioImporte(Long idToNegocio, Double importe){
		Map<String, Object> parametros2 = new LinkedHashMap<String, Object>();
		if(importe != null){
			parametros2.put("importeDerecho", importe);
		}
		parametros2.put("negocio.idToNegocio", idToNegocio);
		return entidadService.findByProperties(NegocioDerechoPoliza.class, parametros2);
	}
	
	@Override
	public NegocioSeccion obtieneNegocioSeccionByid(BigDecimal idNegocioSeccion){
		return entidadService.findById(NegocioSeccion.class, idNegocioSeccion);
	}
	
	@Override
	public List<NegocioEstado> obtenerEstadosPorNegocioId(Long idToNegocio){
		return estadoService.obtenerEstadosPorNegocioId(idToNegocio);
	}
	
	@Override
	public List<NegocioMunicipio> obtenerMunicipiosPorEstadoId(Long idToNegocio, String estadoId){
		return municipioService.obtenerMunicipiosPorEstadoId(idToNegocio, estadoId);
	}
	
	@Override
	public List<MarcaVehiculoDTO> obtenerMarcaVehiculoPorClaveTipoBien(String claveTipoBien){
		return marcaVehiculoFacadeRemote.findByProperty(MarcaVehiculoDTO_.tipoBienAutosDTO.getName()+"."+TipoBienAutosDTO_.claveTipoBien.getName(), claveTipoBien);
	}
	
	@Override
	public MarcaVehiculoDTO obtenerMarcaVehiculoPorMarcaId(BigDecimal marcaId){
		return entidadService.findById(MarcaVehiculoDTO.class, marcaId);
	}
	
	@Override
	public List<NegocioEstiloVehiculo> obtenerNegocioEstiloVehiculoPorIdNegocioSeccion(BigDecimal idToNegSeccion){
		return entidadService.findByProperty(NegocioEstiloVehiculo.class, "negocioSeccion.idToNegSeccion", idToNegSeccion);
	}
	
	@Override
	public ModeloVehiculoDTO obtenerModeloVehiculoPorId(ModeloVehiculoId modeloVehiculoId){
		return entidadService.findById(ModeloVehiculoDTO.class, modeloVehiculoId);
	}
	
	@Override
	public List<TipoUsoVehiculoDTO> getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(BigDecimal idToNegSeccion, BigDecimal idTcTipoVehiculo){
		return negocioTipoUsoDao.getTipoUsoVehiculoDTOByNegSeccionEstiloVehiculo(idToNegSeccion, idTcTipoVehiculo);
	}
	
	@Override
	public List<NegocioPaqueteSeccion> obtenerNegocioPaqueteSeccionPorNegocioSeccion(BigDecimal idToNegSeccion){
		return entidadService.findByProperty(NegocioPaqueteSeccion.class, "negocioSeccion.idToNegSeccion", idToNegSeccion);
	}
	
	@Override
	public List<NegocioCobPaqSeccion> getCoberturas(Long idNegocio,
			BigDecimal idToProducto, BigDecimal idTipoPoliza,
			BigDecimal idToSeccion, Long idPaquete, String idEstado,
			String idMunicipio, Short idMoneda){
		return negocioCobPaqSeccionDAO.getCoberturas(idNegocio,
				idToProducto, idTipoPoliza,
				idToSeccion, idPaquete, idEstado,
				idMunicipio, idMoneda);
	}
	
	
	@Override
	public List<EntidadBitemporal> obtenerIncisoCotinuityList(Long continuityId){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(IncisoContinuity.class, IncisoContinuity.PARENT_KEY_NAME, continuityId);
	}
	
	@Override
	public List<EntidadBitemporal> obtenerAutoIncisoCotinuityList(Long continuityId){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(AutoIncisoContinuity.class, AutoIncisoContinuity.PARENT_KEY_NAME, continuityId);
	}
	
	@Override
	public List<EntidadBitemporal> obtenerSeccionIncisoCotinuityList(Long continuityId){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(SeccionIncisoContinuity.class, SeccionIncisoContinuity.PARENT_KEY_NAME, continuityId);
	}
	
	@Override
	public List<EntidadBitemporal> obtenerCoberturasCotinuityList(Long continuityId){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, continuityId);
	}
	
	@Override
	public NegocioProducto getNegocioProductoByCotizacion(CotizacionDTO cotizacionARenovar){
		return negocioProductoService.getNegocioProductoByCotizacion(cotizacionARenovar);
	}
	
	@Override
	public NegocioTipoPoliza getPorIdNegocioProductoIdToTipoPoliza(Long idToNegProducto, BigDecimal idToTipoPoliza){
		return negocioTipoPolizaService.getPorIdNegocioProductoIdToTipoPoliza(idToNegProducto, idToTipoPoliza);
	}
	
	@Override
	public CotizacionDTO crearCotizacion(CotizacionDTO cotizacionDTO){
		return cotizacionService.crearCotizacion(cotizacionDTO,null);
	}
	
	@Override
	public void saveComisionCotizacionDTO(ComisionCotizacionDTO item){
		comisionCotizacionFacadeRemote.save(item);
	}
	
	@Override
	public void saveDocAnexoCotDTO(DocAnexoCotDTO item){
		docAnexoCotFacadeRemote.save(item);
	}
	
	@Override
	public void saveDocumentoAnexoReaseguroCotizacionDTO(DocumentoAnexoReaseguroCotizacionDTO item){
		documentoAnexoReaseguroCotizacionFacadeRemote.save(item);
	}
	
	@Override
	public void saveDocumentoDigitalCotizacionDTO(DocumentoDigitalCotizacionDTO item){
		documentoDigitalCotizacionFacadeRemote.save(item);
	}
	
	@Override
	public void saveTexAdicionalCotDTO(TexAdicionalCotDTO item){
		entidadService.save(item);
	}
	
	@Override
	public void saveDocumentoDigitalSolicitudDTO(DocumentoDigitalSolicitudDTO item){
		entidadService.save(item);
	}

	@Override
	public void saveComentario(Comentario item){
		entidadService.save(item);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public CotizacionDTO guardarCotizacion(CotizacionDTO cotizacionDTO){
		return cotizacionService.guardarCotizacion(cotizacionDTO);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public IncisoCotizacionDTO prepareGuardarInciso(BigDecimal idToCotizacion,
			IncisoAutoCot incisoAutoCot, IncisoCotizacionDTO incisoCotizacionDTO,
			List<CoberturaCotizacionDTO> coberturaCotizacionList, Double valorPrimaNeta,
			Double primaAsegurada, Short claveContrato, Short claveObligatoriedad){
		return incisoService.prepareGuardarInciso(idToCotizacion,incisoAutoCot, 
				incisoCotizacionDTO,coberturaCotizacionList, valorPrimaNeta, primaAsegurada,
				claveContrato, claveObligatoriedad);
	}
	
	@Override
	public void calcular(IncisoCotizacionDTO nuevoIncisoCotizacion){
		calculoService.calcular(nuevoIncisoCotizacion);
	}
	
	@Override
	public ResumenCostosDTO obtenerResumenCotizacion(CotizacionDTO cotizacionDTO){
		return calculoService.obtenerResumenCotizacion(cotizacionDTO, false);
	}
	
	@Override
	public ResumenCostosDTO resumenCostosEmitidos(BigDecimal idToPoliza){
		Short numeroEndoso = null;
		return calculoService.resumenCostosEmitidos(idToPoliza, numeroEndoso, null);
	}
	
	@Override
	public void igualarPrima(BigDecimal idTpCotizacion, Double primaTotal){
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idTpCotizacion);
		List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idTpCotizacion);
		cotizacion.setIncisoCotizacionDTOs(incisos);
		entidadService.refresh(cotizacion);
		calculoService.igualarPrima(idTpCotizacion, primaTotal, false);
	}
	
	@Override
	public List<NegocioPaqueteSeccion> obtieneNegocioPaqueteSeccionPorNegSeccionPaqueteId(BigDecimal idToNegSeccion, Long paqueteId){
		Map<String, Object> parametros3 = new LinkedHashMap<String, Object>();
		parametros3.put("negocioSeccion.idToNegSeccion", idToNegSeccion);
		parametros3.put("paquete.id", paqueteId);
		return entidadService.findByProperties(NegocioPaqueteSeccion.class, parametros3);
	}
	
	@Override
	public Usuario getUsuarioActual(){
		return usuarioService.getUsuarioActual();
	}
	
	@Override
	public BigDecimal saveAndGetIdOrdenRenovacion(OrdenRenovacionMasiva ordenRenovacion){
		return (BigDecimal) entidadService.saveAndGetId(ordenRenovacion);
	}
	
	@Override
	public void saveOrdenRenovacionMasivaDet(OrdenRenovacionMasivaDet item){
		entidadService.save(item);
	}
	
	@Override
	public TerminarCotizacionDTO terminarCotizacion(BigDecimal idToCotizacion){
		return cotizacionService.terminarCotizacion(idToCotizacion, true);
	}
	
	@Override
	public void guardarCotizacionSimple(CotizacionDTO item){
		entidadService.save(item);
	}
	
	@Override
	public void complementarDatosInciso(IncisoCotizacionDTO incisoCotizacion){
		incisoService.complementarDatosInciso(incisoCotizacion);
	}
	
	@Override
	public Map<String, String> emitir(CotizacionDTO cotizacionDTO){
		return emisionService.emitir(cotizacionDTO, true);
	}
	
	@Override
	public void actualizarEstatusCotizacionEmitida(BigDecimal idToCotizacion){
		cotizacionService.actualizarEstatusCotizacionEmitida(idToCotizacion);
	}
	
	@Override
	public Long obtenerTotalPolizasRenovacion(PolizaDTO polizaDTO, Boolean filtrar, Boolean isCount) {
		if(!filtrar){
			polizaDTO = new PolizaDTO();
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
			
			//Fecha de Vencimiento
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(new Date());
			gcFechaFin.add(GregorianCalendar.DAY_OF_MONTH, 1);

			polizaDTO.getCotizacionDTO().setFechaFinVigencia(new Date());
			polizaDTO.getCotizacionDTO().setFechaFinVigenciaHasta(gcFechaFin.getTime());
		}else{			
			if(polizaDTO.getIdToNegProducto() != null){
				NegocioProducto negocioProducto = entidadService.findById(NegocioProducto.class, polizaDTO.getIdToNegProducto());
				polizaDTO.getCotizacionDTO().getSolicitudDTO().setProductoDTO(negocioProducto.getProductoDTO());
			}
			if(polizaDTO.getIdToNegTipoPoliza() != null){
				NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, polizaDTO.getIdToNegTipoPoliza());
				polizaDTO.getCotizacionDTO().setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
			}
		}
		return polizaService.obtenerTotalPolizaRenovacion(polizaDTO, polizaDTO.getFechaCreacion(), polizaDTO.getFechaCreacionHasta(), isCount, false);
	}

	@Override
	public List<PolizaDTO> listarPolizasRenovacion(PolizaDTO poliza, Boolean filtrar, Boolean isCount, Boolean isFixEndoso) {

		PolizaDTO polizaDTO = new PolizaDTO();
		if(!filtrar){
			polizaDTO = new PolizaDTO();
			polizaDTO.setCotizacionDTO(new CotizacionDTO());
			polizaDTO.getCotizacionDTO().setSolicitudDTO(new SolicitudDTO());
			
			//Fecha de Vencimiento
			GregorianCalendar gcFechaFin = new GregorianCalendar();
			gcFechaFin.setTime(new Date());
			gcFechaFin.add(GregorianCalendar.DAY_OF_MONTH, 1);

			polizaDTO.getCotizacionDTO().setFechaFinVigencia(new Date());
			polizaDTO.getCotizacionDTO().setFechaFinVigenciaHasta(gcFechaFin.getTime());
		}else{
			polizaDTO = poliza;
			if(polizaDTO.getIdToNegProducto() != null){
				NegocioProducto negocioProducto = entidadService.findById(NegocioProducto.class, polizaDTO.getIdToNegProducto());
				polizaDTO.getCotizacionDTO().getSolicitudDTO().setProductoDTO(negocioProducto.getProductoDTO());
			}
			if(polizaDTO.getIdToNegTipoPoliza() != null){
				NegocioTipoPoliza negocioTipoPoliza = entidadService.findById(NegocioTipoPoliza.class, polizaDTO.getIdToNegTipoPoliza());
				polizaDTO.getCotizacionDTO().setTipoPolizaDTO(negocioTipoPoliza.getTipoPolizaDTO());
			}
		}
		List<PolizaDTO> polizaDTOList = polizaService.buscarPolizaRenovacion(poliza, polizaDTO.getFechaCreacion(), polizaDTO.getFechaCreacionHasta(), isCount, isFixEndoso, false);
		
		if(polizaDTOList != null && !polizaDTOList.isEmpty()){
			Collections.sort(polizaDTOList, 
					new Comparator<PolizaDTO>() {				
						public int compare(PolizaDTO n1, PolizaDTO n2){
							return n2.getFechaCreacion().compareTo(n1.getFechaCreacion());
						}
					});
		}
		
		if(isFixEndoso){
			for(PolizaDTO polizaDto: polizaDTOList){
				fixEndosoRenovacion(polizaDto);
			}
		}
		
		return polizaDTOList;
	}
	
	@Override
	public void fixEndosoRenovacion(PolizaDTO poliza){
		try{
			BitemporalCotizacion cotizacion = endosoService.encuentraBitemporalCotizacion(poliza.getCotizacionDTO().getIdToCotizacion(), TimeUtils.getDateTime(new Date()), SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS, TipoAccionDTO.getNuevoEndosoCot());
			entidadContinuityDao.rollBackContinuity(cotizacion.getContinuity().getId());
		}catch(Exception e){
			LOG.error(e);
		}
	}
	
	@Override
	public Long listarOrdenesRenovacionCount(OrdenRenovacionMasiva orden, Boolean filtrar, String nombreUsuario){
		OrdenRenovacionMasiva ordenRenovacionM = new OrdenRenovacionMasiva();
		if(!filtrar){
			ordenRenovacionM = new OrdenRenovacionMasiva();
			ordenRenovacionM.setCodigoUsuarioCreacion(nombreUsuario);
		}else{
			ordenRenovacionM = orden;
		}
		return ordenRenovacionMasivaDao.listByFilterCount(ordenRenovacionM);
	}
	
	@Override
	public List<OrdenRenovacionMasiva> listarOrdenesRenovacion(OrdenRenovacionMasiva orden, Boolean filtrar, String nombreUsuario){
		OrdenRenovacionMasiva ordenRenovacionM = new OrdenRenovacionMasiva();
		if(!filtrar){
			int primerRegistro = orden.getPrimerRegistroACargar();
			int numeroMaximo = orden.getNumeroMaximoRegistrosACargar();
			ordenRenovacionM.setPrimerRegistroACargar(primerRegistro);
			ordenRenovacionM.setNumeroMaximoRegistrosACargar(numeroMaximo);
			ordenRenovacionM.setCodigoUsuarioCreacion(nombreUsuario);
		}else{
			ordenRenovacionM = orden;
		}
		List<OrdenRenovacionMasiva> ordenes = ordenRenovacionMasivaDao.listByFilter(ordenRenovacionM);
		
		
		//Obtiene detalle de ordenes pendientes y actualiza segun detalle
		for(OrdenRenovacionMasiva ordenRenovacion : ordenes){
		boolean ordenTerminada = true;
		boolean ordenCancelada = true;
		if(ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_INICIADA) || ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_CONTROL_USUARIO)){
			//Update estatus de cotizaciones con poliza emitida
			try{
				List<OrdenRenovacionMasivaDet> ordenesDetalleFix = listarOrdenRenovacionCotizacionesDesincronizadas(ordenRenovacion.getIdToOrdenRenovacion());
				for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalleFix){
					cotizacionService.actualizarEstatusCotizacionEmitida(ordenDet.getIdToCotizacion());
				}
			}catch(Exception e){
				LOG.error(e);
			}
			List<OrdenRenovacionMasivaDet> ordenesDetalle = getListOrdenesDetalle(ordenRenovacion.getIdToOrdenRenovacion());
			for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
				if(ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION) || ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_EMISION)){
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
						ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_EMISION);
					} else if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA)){
						try{
							List<PolizaDTO> polizaList = entidadService.findByProperty(PolizaDTO.class, "cotizacionDTO.idToCotizacion", ordenDet.getIdToCotizacion());
							ordenDet.setIdToPolizaRenovada(polizaList.get(0).getIdToPoliza());
							ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
						}catch(Exception e){
							LOG.error(e);
						}
					} else if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_CANCELADA)){
						ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA);
					}
					entidadService.save(ordenDet);
				}
				if(ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION) || ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_EMISION)
						|| ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_FALLO_EMISION)){
					ordenTerminada = false;
				}	
				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA)){
					ordenCancelada = false;
				}	
			}
			if(ordenTerminada){
				ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_TERMINADA);
				ordenRenovacion.setFechaModificacion(new Date());
				entidadService.save(ordenRenovacion);
			}
			if(ordenCancelada){
				ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_CANCELADA);
				ordenRenovacion.setFechaModificacion(new Date());
				entidadService.save(ordenRenovacion);
			}
		}
		}
		
		//Sort
		if(ordenes != null && !ordenes.isEmpty()){
			Collections.sort(ordenes, 
					new Comparator<OrdenRenovacionMasiva>() {				
						public int compare(OrdenRenovacionMasiva n1, OrdenRenovacionMasiva n2){
							return n2.getIdToOrdenRenovacion().compareTo(n1.getIdToOrdenRenovacion());
						}
					});
		}
		
		return ordenes;
	}
	
	@SuppressWarnings("unchecked")
	private List<OrdenRenovacionMasivaDet> listarOrdenRenovacionCotizacionesDesincronizadas(BigDecimal idToOrdenRenovacion){
		String query = "select o " +
				" from OrdenRenovacionMasivaDet o, PolizaDTO p  " +
				" where o.id.idToOrdenRenovacion =  " + idToOrdenRenovacion + "" +
				" and o.cotizacion.claveEstatus in ("+ CotizacionDTO.ESTATUS_COT_EN_PROCESO + ", " + CotizacionDTO.ESTATUS_COT_TERMINADA +" )" +
				" and p.cotizacionDTO.idToCotizacion = o.cotizacion.idToCotizacion ";
		return (List<OrdenRenovacionMasivaDet>) entidadService.executeQueryMultipleResult(query, null);
	}
	
	@Override
	public List<OrdenRenovacionMasivaDet> listarOrdenRenovacionDetalleByOrdenRenovacion(BigDecimal idToOrdenRenovacion){
		List<OrdenRenovacionMasivaDet> ordenesDetalle = getListOrdenesDetalle(idToOrdenRenovacion);
		for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
			if(ordenDet.getIdToPolizaRenovada() != null){
				PolizaDTO poliza = entidadService.findById(PolizaDTO.class, ordenDet.getIdToPolizaRenovada());
				ordenDet.setPolizaRenovada(poliza);
			}else{
				ordenDet.setPolizaRenovada(new PolizaDTO());
			}
		}
		//Sort
		if(ordenesDetalle != null && !ordenesDetalle.isEmpty()){
			Collections.sort(ordenesDetalle, 
					new Comparator<OrdenRenovacionMasivaDet>() {				
						public int compare(OrdenRenovacionMasivaDet n1, OrdenRenovacionMasivaDet n2){
							return n2.getId().getIdToPoliza().compareTo(n1.getId().getIdToPoliza());
						}
					});
		}
		return ordenesDetalle;
	}

	@Override
	public void cancelarOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		if(idToOrdenRenovacion != null){
			OrdenRenovacionMasiva ordenRenovacion = entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
			
			List<OrdenRenovacionMasivaDet> ordenesDetalle = getListOrdenesDetalle(idToOrdenRenovacion);
			for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA) &&
						!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA) &&
						ordenDet.getIdToCotizacion() != null){
					cancelarCotizacion(ordenDet.getId().getIdToPoliza());
					ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA);
					entidadService.save(ordenDet);
				}
			}
			ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_CANCELADA);
			ordenRenovacion.setFechaModificacion(new Date());
			entidadService.save(ordenRenovacion);
		}
	}
	
	@Override
	public void cancelarCotizacion(BigDecimal idToPoliza){
		List<CotizacionDTO> cotizaciones = obtieneCotizacionesPorPolizaAnterior(idToPoliza);			
		for(CotizacionDTO cotizacion : cotizaciones){
			if(!cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA) &&
					!cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_CANCELADA)){
				cotizacionService.cancelarCotizacion(cotizacion.getIdToCotizacion());
			}
		}
	}

	@Override
	public String terminarOrdenRenovacion(BigDecimal idToOrdenRenovacion) {
		StringBuilder mensajeError = new StringBuilder("");
		if(idToOrdenRenovacion != null){
			OrdenRenovacionMasiva ordenRenovacion = entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
			
			List<OrdenRenovacionMasivaDet> ordenesDetalle = getListOrdenesDetalle(idToOrdenRenovacion);
			for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
				LOG.debug("RenovacionMasiva Emitiendo Poliza: " + ordenDet.getPolizaAnterior().getNumeroPolizaFormateada());
				try{
				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA) &&
						!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA) &&
						ordenDet.getIdToCotizacion() != null){
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EN_PROCESO)){
						TerminarCotizacionDTO terminarCotizacionDTO =cotizacionService.terminarCotizacion(ordenDet.getIdToCotizacion(), true);
						if(terminarCotizacionDTO == null){
						
							CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, ordenDet.getIdToCotizacion());
							Map<String, String> mensajeEmision = emisionService.emitir(cotizacionDTO, true);
							String icono = mensajeEmision.get("icono");							
							if(icono.equals("30")){
								cotizacionService.actualizarEstatusCotizacionEmitida(cotizacionDTO.getIdToCotizacion());
								String idPoliza = mensajeEmision.get("idpoliza");
								ordenDet.setIdToPolizaRenovada(BigDecimal.valueOf(Long.valueOf(idPoliza)));
								ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
								entidadService.save(ordenDet);
								LOG.debug("RenovacionMasiva Poliza Emitida: " + ordenDet.getPolizaAnterior().getNumeroPolizaFormateada());
							}else if(icono.equals("10")){
								String mensaje = mensajeEmision.get("mensaje");
								mensajeError.append("Poliza Anterior ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(
								" causo una Exception. ").append(mensaje);								
							}
						}else{
							mensajeError.append("\nERROR  EN POLIZA: ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(" \n");
							mensajeError.append(terminarCotizacionDTO.getMensajeError());
						}
					}
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
						CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, ordenDet.getIdToCotizacion());
						Map<String, String> mensajeEmision = emisionService.emitir(cotizacionDTO, true);
						String icono = mensajeEmision.get("icono");
						if(icono.equals("30")){
							cotizacionService.actualizarEstatusCotizacionEmitida(cotizacionDTO.getIdToCotizacion());
							String idPoliza = mensajeEmision.get("idpoliza");
							ordenDet.setIdToPolizaRenovada(BigDecimal.valueOf(Long.valueOf(idPoliza)));
							ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
							entidadService.save(ordenDet);
							LOG.debug("RenovacionMasiva Poliza Emitida: " + ordenDet.getPolizaAnterior().getNumeroPolizaFormateada());
						}else{
							mensajeError.append("Poliza Anterior ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(" causo una Exception. ");								
						}						
					}
					
				}
				}catch(Exception e){
					LOG.error(e);
					mensajeError.append("Poliza Anterior ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append( 
					" causo una Exception. " ).append(e.getMessage());
				}
			}
			if(mensajeError == null || mensajeError.toString().isEmpty()){
				ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_TERMINADA);
				ordenRenovacion.setFechaModificacion(new Date());
				entidadService.save(ordenRenovacion);
			}
		}
		return mensajeError.toString();
	}
	

	@Override
	public String terminarPolizasDetalleOrdenRenovacion(BigDecimal idToOrdenRenovacion, List<PolizaDTO> polizaList) {
		StringBuilder mensajeError = new StringBuilder("");
		if(idToOrdenRenovacion != null){
			for(PolizaDTO poliza: polizaList){
				OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
				id.setIdToOrdenRenovacion(idToOrdenRenovacion);
				id.setIdToPoliza(poliza.getIdToPoliza());
				OrdenRenovacionMasivaDet ordenDet = entidadService.findById(OrdenRenovacionMasivaDet.class, id);

				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA) &&
						!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA) &&
						ordenDet.getIdToCotizacion() != null){
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EN_PROCESO)){
						TerminarCotizacionDTO terminarCotizacionDTO =cotizacionService.terminarCotizacion(ordenDet.getIdToCotizacion(), true);
						if(terminarCotizacionDTO == null){

							CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, ordenDet.getIdToCotizacion());
							Map<String, String> mensajeEmision = emisionService.emitir(cotizacionDTO, true);
							String icono = mensajeEmision.get("icono");
							if(icono.equals("30")){
								cotizacionService.actualizarEstatusCotizacionEmitida(cotizacionDTO.getIdToCotizacion());
								String idPoliza = mensajeEmision.get("idpoliza");
								ordenDet.setIdToPolizaRenovada(BigDecimal.valueOf(Long.valueOf(idPoliza)));
								ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
								entidadService.save(ordenDet);
							}else if(icono.equals("10")){
								String mensaje = mensajeEmision.get("mensaje");
								mensajeError.append("\nERROR EN POLIZA: ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(" \n");
								mensajeError.append(mensaje);
							}
						}else{
							mensajeError.append("\nERROR  EN POLIZA: ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(" \n");
							mensajeError.append(terminarCotizacionDTO.getMensajeError());							
						}
					}
					if(ordenDet.getCotizacion().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)){
						TerminarCotizacionDTO validacion = cotizacionService.validarEmisionCotizacion(ordenDet.getIdToCotizacion());
						if(validacion != null){
							LOG.debug(validacion.getMensajeError());
							//mensajeError += "\nERROR EN POLIZA: " + ordenDet.getPolizaAnterior().getNumeroPolizaFormateada() + " \n";
							//mensajeError += validacion.getMensajeError();
						}
						CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, ordenDet.getIdToCotizacion());
						Map<String, String> mensajeEmision = emisionService.emitir(cotizacionDTO, true);
						String icono = mensajeEmision.get("icono");
						String mensaje = mensajeEmision.get("mensaje");
						if(icono.equals("30")){
							cotizacionService.actualizarEstatusCotizacionEmitida(cotizacionDTO.getIdToCotizacion());
							String idPoliza = mensajeEmision.get("idpoliza");
							ordenDet.setIdToPolizaRenovada(BigDecimal.valueOf(Long.valueOf(idPoliza)));
							ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA);
							entidadService.save(ordenDet);
						}else if(icono.equals("10")){
							mensajeError.append("\nERROR EN POLIZA: ").append(ordenDet.getPolizaAnterior().getNumeroPolizaFormateada()).append(" \n");
							mensajeError.append(mensaje);
						}					
					}
					
				}
			}
			
			//Actualiza Estatus de la Orden
			boolean ordenTerminada = true;
			OrdenRenovacionMasiva ordenRenovacion = entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
			if(ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_INICIADA) || ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_CONTROL_USUARIO)){
				List<OrdenRenovacionMasivaDet> ordenesDetalle = getListOrdenesDetalle(ordenRenovacion.getIdToOrdenRenovacion());
				for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
					if(ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION) || ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_EMISION)
							|| ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_FALLO_EMISION)){
						ordenTerminada = false;
					}		
				}
				if(ordenTerminada){
					ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_TERMINADA);
					ordenRenovacion.setFechaModificacion(new Date());
					entidadService.save(ordenRenovacion);
				}
			}
		}
		return mensajeError.toString();
	}
	
	@Override
	public void cancelarDetalleOrdenRenovacion(BigDecimal idToOrdenRenovacion, List<PolizaDTO> polizaList) {
		if(idToOrdenRenovacion != null){
			for(PolizaDTO poliza: polizaList){
				OrdenRenovacionMasivaDetId id = new OrdenRenovacionMasivaDetId();
				id.setIdToOrdenRenovacion(idToOrdenRenovacion);
				id.setIdToPoliza(poliza.getIdToPoliza());
				OrdenRenovacionMasivaDet ordenDet = entidadService.findById(OrdenRenovacionMasivaDet.class, id);
				
				if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA) &&
						!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_TERMINADA) &&
						ordenDet.getIdToCotizacion() != null){
					cancelarCotizacion(ordenDet.getId().getIdToPoliza());
					ordenDet.setClaveEstatus(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA);
					entidadService.save(ordenDet);
				}
			}
			//Actualiza Estatus de la Orden
			boolean ordenTerminada = true;
			boolean ordenCancelada = true;
			OrdenRenovacionMasiva ordenRenovacion = entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
			if(ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_INICIADA) || ordenRenovacion.getClaveEstatus().equals(OrdenRenovacionMasiva.ESTATUS_CONTROL_USUARIO)){
				List<OrdenRenovacionMasivaDet> ordenesDetalle = getListOrdenesDetalle(ordenRenovacion.getIdToOrdenRenovacion());
				for(OrdenRenovacionMasivaDet ordenDet : ordenesDetalle){
					if(ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION) || ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_EMISION)
							|| ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_FALLO_EMISION)){
						ordenTerminada = false;
					}
					if(!ordenDet.getClaveEstatus().equals(OrdenRenovacionMasivaDet.ESTATUS_CANCELADA)){
						ordenCancelada = false;
					}
				}
				if(ordenTerminada && !ordenCancelada){
					ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_TERMINADA);
					ordenRenovacion.setFechaModificacion(new Date());
					entidadService.save(ordenRenovacion);
				}
				if(ordenCancelada){
					ordenRenovacion.setClaveEstatus(OrdenRenovacionMasiva.ESTATUS_CANCELADA);
					ordenRenovacion.setFechaModificacion(new Date());
					entidadService.save(ordenRenovacion);
				}
			}
		}
	}
	
	private List<OrdenRenovacionMasivaDet> getListOrdenesDetalle(BigDecimal idToOrdenRenovacion) {
		return entidadService.findByProperty(OrdenRenovacionMasivaDet.class, "id.idToOrdenRenovacion", idToOrdenRenovacion);
	}

	@Override
	public OrdenRenovacionMasiva obtieneOrdenRenovacionMasiva(BigDecimal idToOrdenRenovacion){
		return entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
	}
	
	@Override
	public OrdenRenovacionMasivaDet obtieneOrdenRenovacionMasivaDet(OrdenRenovacionMasivaDetId id){
		return entidadService.findById(OrdenRenovacionMasivaDet.class, id);
	}
	
	@Override
	public TerminarCotizacionDTO validarEmisionCotizacion(BigDecimal idToCotizacion){
		return cotizacionService.validarEmisionCotizacion(idToCotizacion);
	}
	
	@Override
	public IncisoAutoCot regresaIncisoAutoCot(BigDecimal idToCotizacion){
		IncisoAutoCot incisoAutoCot = null;
		
		List<IncisoCotizacionDTO> incisosList = incisoService.findByCotizacionId(idToCotizacion);
		if(incisosList != null && !incisosList.isEmpty()){
			incisoAutoCot = incisosList.get(0).getIncisoAutoCot();
		}
		
		return incisoAutoCot;
	}
	
	@Override
	public String getCamposExcel(IncisoAutoCot incisoAutoCot, int campo, List<EntidadBitemporal> coberturasCotinuityList){
		String valor = "";
		//LINEA NEGOCIO
		if(campo == OrdenRenovacionMasiva.NOM_LINEA_NEG || campo == OrdenRenovacionMasiva.LINEA_ANTERIOR){
			NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, new BigDecimal(incisoAutoCot.getNegocioSeccionId()));
			if(negocioSeccion != null){
			valor = negocioSeccion.getSeccionDTO().getDescripcion();
			}
			//PAQUETE
		} else 	if(campo == OrdenRenovacionMasiva.PAQUETE){
			NegocioPaqueteSeccion negocioPaqueteSeccion = entidadService.findById(NegocioPaqueteSeccion.class, incisoAutoCot.getNegocioPaqueteId());
			if(negocioPaqueteSeccion != null){
			valor = negocioPaqueteSeccion.getPaquete().getDescripcion();
			}
			//DESCRIPCION VEHICULO
		} else 	if(campo == OrdenRenovacionMasiva.DESC_VEHIC){
			/*
			 * EstiloVehiculoId id = new EstiloVehiculoId();
			id.setStrId(incisoAutoCot.getEstiloId());
			EstiloVehiculoDTO estiloVehiculo = entidadService.findById(EstiloVehiculoDTO.class, id);
			if(estiloVehiculo != null){
			valor = estiloVehiculo.getDescripcionEstilo();
			}
			*/
			valor = incisoAutoCot.getDescripcionFinal();
			//ESTADO
		} else 	if(campo == OrdenRenovacionMasiva.ESTADO){
			EstadoDTO estado = entidadService.findById(EstadoDTO.class, incisoAutoCot.getEstadoId());
			if(estado != null){
			valor = estado.getDescription();
			}
			//DEDUCIBLE
		} else 	if(campo == OrdenRenovacionMasiva.DEDUCIBLE_RT){
			List<CoberturaCotizacionDTO> coberturas = coberturaService.getCoberturas(incisoAutoCot.getIncisoCotizacionDTO(), true);
			if(coberturas != null && !coberturas.isEmpty()){
				for(CoberturaCotizacionDTO coberturaCotizacion: coberturas){
					if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().trim().toUpperCase().equals(
							CoberturaDTO.CLAVE_CALCULO_ROBO_TOTAL)){
						if(coberturaCotizacion.getClaveTipoDeducible() != null){
							switch(coberturaCotizacion.getClaveTipoDeducible().intValue()){
							case 1:
								valor = coberturaCotizacion.getPorcentajeDeducible() + "";
								break;
							default:
								valor = coberturaCotizacion.getValorDeducible() + "";
								break;
							}
						}else{
							try{
								if(coberturaCotizacion.getPorcentajeDeducible() == null) coberturaCotizacion.setPorcentajeDeducible(0.0);
								if(coberturaCotizacion.getValorDeducible() == null) coberturaCotizacion.setValorDeducible(0.0);
								if(coberturaCotizacion.getPorcentajeDeducible().doubleValue() >= coberturaCotizacion.getValorDeducible().doubleValue()){
									valor = coberturaCotizacion.getPorcentajeDeducible() + "";
								}else{
									valor = coberturaCotizacion.getValorDeducible() + "";
								}
							}catch(Exception e){
								valor = "0";
							}
						}
					    break;
					}					
				}
			}
			//DEDUCIBLE
		} else 	if(campo == OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES){
			List<CoberturaCotizacionDTO> coberturas = coberturaService.getCoberturas(incisoAutoCot.getIncisoCotizacionDTO(), true);
			if(coberturas != null && !coberturas.isEmpty()){
				for(CoberturaCotizacionDTO coberturaCotizacion: coberturas){
					if(coberturaCotizacion.getCoberturaSeccionDTO().getCoberturaDTO().getClaveTipoCalculo().trim().toUpperCase().equals(
							CoberturaDTO.CLAVE_CALCULO_DANOS_MATERIALES)){
						if(coberturaCotizacion.getClaveTipoDeducible() != null){
							switch(coberturaCotizacion.getClaveTipoDeducible().intValue()){
							case 1:
								valor = coberturaCotizacion.getPorcentajeDeducible() + "";
								break;
							default:
								valor = coberturaCotizacion.getValorDeducible() + "";
								break;
							}
						}else{
							try{
								if(coberturaCotizacion.getPorcentajeDeducible() == null) coberturaCotizacion.setPorcentajeDeducible(0.0);
								if(coberturaCotizacion.getValorDeducible() == null) coberturaCotizacion.setValorDeducible(0.0);
								if(coberturaCotizacion.getPorcentajeDeducible().doubleValue() >= coberturaCotizacion.getValorDeducible().doubleValue()){
									valor = coberturaCotizacion.getPorcentajeDeducible() + "";
								}else{
									valor = coberturaCotizacion.getValorDeducible() + "";
								}
							}catch(Exception e){
								valor = "0";
							}
						}
					    break;
					}					
				}
			}
			//DEDUCIBLE
		} else 	if(campo == OrdenRenovacionMasiva.DEDUCIBLE_DANOS_MATERIALES_BIT){
			try{
			if(coberturasCotinuityList != null && !coberturasCotinuityList.isEmpty()){
				for(EntidadBitemporal item3: coberturasCotinuityList){
					BitemporalCoberturaSeccion bitemporalCoberturaSeccion = (BitemporalCoberturaSeccion) item3;
					if(bitemporalCoberturaSeccion != null && bitemporalCoberturaSeccion.getValue() != null){
						CoberturaCotizacionDTO coberturaCotizacion =  new CoberturaCotizacionDTO(bitemporalCoberturaSeccion.getValue());
						if(bitemporalCoberturaSeccion.getContinuity().getCoberturaDTO().getClaveTipoCalculo().trim().toUpperCase().equals(
							CoberturaDTO.CLAVE_CALCULO_DANOS_MATERIALES)){
							if(coberturaCotizacion.getClaveTipoDeducible() != null){
								switch(coberturaCotizacion.getClaveTipoDeducible().intValue()){
								case 1:
									valor = coberturaCotizacion.getPorcentajeDeducible() + "";
									break;
								default:
									valor = coberturaCotizacion.getValorDeducible() + "";
									break;
								}
							}else{
								try{
									if(coberturaCotizacion.getPorcentajeDeducible() == null) coberturaCotizacion.setPorcentajeDeducible(0.0);
									if(coberturaCotizacion.getValorDeducible() == null) coberturaCotizacion.setValorDeducible(0.0);
									if(coberturaCotizacion.getPorcentajeDeducible().doubleValue() >= coberturaCotizacion.getValorDeducible().doubleValue()){
										valor = coberturaCotizacion.getPorcentajeDeducible() + "";
									}else{
										valor = coberturaCotizacion.getValorDeducible() + "";
									}
								}catch(Exception e){
									valor = "0";
								}
							}
							break;
						}
					}
				}
			}
			}catch(Exception e){
				LOG.error(e);
			}
		}
		return valor;
	}
	
	@Override
	public TransporteImpresionDTO obtieneXMLOrdenRenovacion(OrdenRenovacionMasivaDet orden, Locale locale){
		
		List<EndosoDTO> endosoPolizaList = endosoService.findByPropertyWithDescriptions("id.idToPoliza", orden.getIdToPolizaRenovada(), Boolean.TRUE);
		DateTime validOnDT = null;
		DateTime recordFromDT = null;
		Short claveTipoEndoso = 2;
		if(endosoPolizaList != null && !endosoPolizaList.isEmpty()){
			EndosoDTO primerEndoso = endosoPolizaList.get(0);
			
			validOnDT = new DateTime(primerEndoso.getValidFrom().getTime());
			recordFromDT = new DateTime(primerEndoso.getRecordFrom().getTime());
			claveTipoEndoso = primerEndoso.getClaveTipoEndoso();
		}		
		boolean incluirReferenciasBancarias = false;
		return generarPlantillaReporte.imprimirPoliza(orden.getPolizaRenovada().getIdToPoliza(), locale, validOnDT,
				recordFromDT,true,true,true,true,true,incluirReferenciasBancarias, claveTipoEndoso,false,false);
	}
	
	@Override
	public List<BigDecimal> cancelaCotizacionesValidacion(List<ValidacionRenovacionDTO> validaciones){
		List<BigDecimal> cotizacionesCanceladas = new ArrayList<BigDecimal>(1);
		for(ValidacionRenovacionDTO validacion : validaciones){
			PolizaDTO poliza = entidadService.findById(PolizaDTO.class, validacion.getIdToPoliza());
			List<CotizacionDTO> cotizacionList = entidadService.findByProperty(CotizacionDTO.class, "solicitudDTO.idToPolizaAnterior", poliza.getIdToPoliza());
			for(CotizacionDTO cotizacion : cotizacionList){
				if(cotizacion.getClaveEstatus().shortValue() == CotizacionDTO.ESTATUS_COT_EN_PROCESO ||
						cotizacion.getClaveEstatus().shortValue() == CotizacionDTO.ESTATUS_COT_TERMINADA){
					cotizacion.setClaveEstatus(CotizacionDTO.ESTATUS_COT_CANCELADA);
					entidadService.save(cotizacion);
					cotizacionesCanceladas.add(cotizacion.getIdToCotizacion());
				}
					
			}
			
			//Busca si tiene Endosos en proceso //EndosoServiceImpl initCotizacionEndoso
			try{
				BitemporalCotizacion cotizacionBT = obtieneBitemporalCotizacionByIdCotizacion(
					poliza.getCotizacionDTO().getIdToCotizacion());
				//valida que no tenga records en proceso la cotización.
				if (cotizacionBT != null && cotizacionBT.getContinuity().getCotizaciones().hasRecordsInProcess()) {
					CotizacionEndosoDTO filtrosBusqueda = new CotizacionEndosoDTO();
					filtrosBusqueda.setBusquedaEstatusEnProceso(true);
					filtrosBusqueda.setNumeroPoliza(poliza.getNumeroPoliza());
					filtrosBusqueda.setNumeroCotizacionEndoso(poliza.getCotizacionDTO().getIdToCotizacion());
					
					List<CotizacionEndosoDTO> pendientes = cotizacionEndosoService.buscarCotizacionesEndosoFiltrado(filtrosBusqueda);
					if(pendientes != null && !pendientes.isEmpty()){
						for(CotizacionEndosoDTO pendiente: pendientes){
							try{
								BitemporalCotizacion cotizacion = new BitemporalCotizacion();
								CotizacionContinuity cotizacionContinuity = new CotizacionContinuity();
								cotizacionContinuity.setId(pendiente.getContinuityId());
								cotizacion.setContinuity(cotizacionContinuity);
								
								endosoService.deleteCotizacionEndoso(pendiente.getContinuityId());
								CotizacionContinuity continuity = entidadContinuityService.findContinuityByKey(CotizacionContinuity.class,cotizacion.getContinuity().getId());
								cotizacion.setContinuity(continuity);
								Map<String,Object> properties = new HashMap<String,Object>();
								properties.put("cotizacionId", continuity.getNumero());
								properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_EN_PROCESO);
								List<ControlEndosoCot> controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);
								
								if(controlEndosoCotList.isEmpty())
								{
									properties.put("claveEstatusCot",CotizacionDTO.ESTATUS_COT_TERMINADA);
									controlEndosoCotList = entidadService.findByProperties(ControlEndosoCot.class,properties);				
								}
								
								Iterator<ControlEndosoCot> itControlEndosoCot = controlEndosoCotList.iterator();
								
								ControlEndosoCot controlEndosoCot = null;
								Boolean movimientosBorrados = false;
								while(itControlEndosoCot.hasNext())
								{
									controlEndosoCot = itControlEndosoCot.next();
									if(!movimientosBorrados){
										movimientoEndosoBitemporalService.borrarMovimientos(controlEndosoCot);
										movimientosBorrados = true;
									}
									entidadService.remove(controlEndosoCot);			
								}		
								
								//Remover Solicitudes de Autorizacion
								endosoService.cancelarSolicitudesAutorizacion(cotizacion);
								cotizacionesCanceladas.add(pendiente.getSolicitudId());
							}catch(Exception e){
								LOG.error(e);
							}
						}
					}
				}
			}catch(Exception e){
				LOG.error(e);
			}
			
		}
		
		return cotizacionesCanceladas;
	}
	
	@Override
	public List<EntidadBitemporal> obtenerDatoSeccionContinuityList(Long continuityId){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(DatoSeccionContinuity.class, DatoSeccionContinuity.PARENT_KEY_NAME, continuityId);
	}
	
	@Override
	public void saveDatoIncisoCotAuto(DatoIncisoCotAuto datoInciso){
		entidadService.save(datoInciso);
	}
	
	@Override
	public PolizaDTO getCantidadSiniestro(BigDecimal idToPoliza){
		PolizaDTO poliza = null;
		try{
			PolizaDTO polizaBase = entidadService.findById(PolizaDTO.class, idToPoliza);
			List<PolizaDTO> polizaList = calculoDao.getCantidadSiniestro(idToPoliza);
			if(polizaList != null && !polizaList.isEmpty()){
				for(PolizaDTO item : polizaList){
					if(item.getNumeroRenovacion().equals(polizaBase.getNumeroRenovacion())){
						poliza = item;
						break;
					}
				}
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return poliza;
	}
	
	@Override
	public int getCantidadSiniestroTotal(BigDecimal idToPoliza){
		int totalSiniestros = 0;
		try{
			List<PolizaDTO> itemList = calculoDao.getCantidadSiniestro(idToPoliza);
			if(itemList != null && !itemList.isEmpty()){
				for(PolizaDTO item2 : itemList){
					totalSiniestros = totalSiniestros + Integer.parseInt(item2.getNumeroSiniestro());
				}
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return totalSiniestros;
	}
	
	@Override
	public NegocioRenovacionDesc obtenerNegocioRenovacionDescPorId(NegocioRenovacionDescId id){
		return entidadService.findById(NegocioRenovacionDesc.class, id);
	}
	
	@Override
	public String enviarProveedorOrdenRenovacion(BigDecimal idToOrdenRenovacion, List<ControlDescripcionArchivoDTO> pdfList, InputStream excel){
		//FTPSClient client = new FTPSClient();
		LOG.trace("Inicia Envio a Proovedor");
		String mensaje = null;
		
		OrdenRenovacionMasiva orden = entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdfH = new SimpleDateFormat("HHmm");
		String nameFile = "OrdendeRenovacion" + idToOrdenRenovacion.toPlainString() + "_" + sdf.format(orden.getFechaCreacion()) + "_" + sdfH.format(orden.getFechaCreacion());
		
        Connection conn = null;
        SFTPv3Client client = null;

		try {			
			String directory = sistemaContext.getEnvioProovedorDirectory();
			String remoteDirectory = directory;

			conn = this.getConnectionSFTPAuth();
			
			System.out.println("Inicia envio de Archivos a Proveedor");
			client = new SFTPv3Client(conn);
			
			if(pdfList != null && pdfList.size() > 0){
				InputStream zip = armaZip(pdfList);
				try{
					enviarArchivoSFTP(client, zip, remoteDirectory + "/" + nameFile + ".zip");
				}catch(Exception e){
					LOG.error(e);
					mensaje = e.getMessage();
				}
			}
			
			if(excel != null){
				try{
					enviarArchivoSFTP(client, excel, remoteDirectory + "/" + nameFile + ".xls");
				}catch(Exception e){
					LOG.error(e);
					mensaje = e.getMessage();
				}
			}
			
			
			LOG.trace("Termina envio de Archivos a Proveedor");
	        client.close();
			conn.close();
		} catch (java.net.ConnectException e) {
			LOG.error(e);
			mensaje = e.getMessage();
		} catch (IOException e) {
			LOG.error(e);
			mensaje = e.getMessage();
		} catch (Exception e) {
			LOG.error(e);
			mensaje = e.getMessage();
		} finally {
            if(client!=null){
                client.close();
          }
          
          if(conn!=null){
                conn.close();
          }

		}
		LOG.trace("Termina a Envio a Proveedor");
		return mensaje;
	}
	
	@Override
	public String enviarProveedorPolizasSeleccionadas(String nameFile, InputStream pdfList){		
		LOG.trace("Inicia Envio a Proovedor");
		String mensaje = null;
		
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdfH = new SimpleDateFormat("HHmm");
		Date now = new Date();
		String nameFileComplete = nameFile + "_" + sdf.format(now) + "_" + sdfH.format(now);
		
        Connection conn = null;
        SFTPv3Client client = null;

		try {			
			String directory = sistemaContext.getEnvioProovedorDirectory();
			String remoteDirectory = directory;

			conn = this.getConnectionSFTPAuth();
			
			LOG.trace("Inicia envio de Archivos a Proveedor");
			client = new SFTPv3Client(conn);
			
			if(pdfList != null){
				try{
					enviarArchivoSFTP(client, pdfList, remoteDirectory + "/" + nameFileComplete + ".zip");
				}catch(Exception e){
					e.printStackTrace();
					mensaje = e.getMessage();
				}
			}			
			
			LOG.trace("Termina envio de Archivos a Proveedor");
	        client.close();
			conn.close();
		} catch (java.net.ConnectException e) {
			LOG.error(e.getMessage());
			mensaje = e.getMessage();
		} catch (IOException e) {
			LOG.error(e.getMessage());
			mensaje = e.getMessage();
		} catch (Exception e) {
			LOG.error(e.getMessage());
			mensaje = e.getMessage();
		} finally {
            if(client!=null){
                client.close();
          }
          
          if(conn!=null){
                conn.close();
          }

		}
		LOG.trace("Termina a Envio a Proveedor");
		return mensaje;
	}
	
	@SuppressWarnings("unused")
	private Connection getConnectionSFTPAuth() throws IOException{
		File home = new File(System.getProperty("user.home"));
		String sFTP = sistemaContext.getEnvioProovedorFTP();
		int port = sistemaContext.getEnvioProovedorPort();
		String sUser = sistemaContext.getEnvioProovedorUser();
		String sPassword = sistemaContext.getEnvioProovedorPassword();
		String directory = sistemaContext.getEnvioProovedorDirectory();
		String privateKeyPath = null;
        int connectTimeout  = 30000;
        int kexTimeout   = 30000;
        
		String serverAddress = sFTP;
		String username = sUser;
		final String password = sPassword;
		String remoteDirectory = directory;
		
		LOG.debug("Username: " + username);
		LOG.debug("Password: " + password);
		LOG.debug("serverAddress: " + serverAddress);

		File knownHosts = new File("~/.ssh/known_hosts");
		File knownHosts2 = new File("/root/.ssh/known_hosts");
		File knownHosts3 = new File(home, ".ssh/known_hosts");
		File knownHosts4 = null;
		
    	try {
    		Properties prop = new Properties();
            //load a properties file from class path, inside static method
    		InputStream p = RenovacionMasivaServiceImpl.class.getClassLoader().getResourceAsStream("MidasSistema.properties");
    		if(p == null){
    			p = new FileInputStream("MidasSistema.properties");
    		}
    		prop.load(p);

            //get the property value and print it out
    		String pathProp = prop.getProperty("midas.sistema.envioProveedor.knowhost");
            LOG.debug("Path KnowHost Properties = " + pathProp);
            if(pathProp != null){
            	knownHosts4 = new File(pathProp);
            }
    	} catch (Exception ex) {    
			LOG.error(ex.getStackTrace());		
    	}
		KnownHosts database = new KnownHosts();

		/* Load known_hosts file into in-memory database */

		if (knownHosts != null && knownHosts.exists()) {
			database.addHostkeys(knownHosts);
		} else if (knownHosts2 != null && knownHosts2.exists()) {
			database.addHostkeys(knownHosts2);
		} else if (knownHosts3 != null && knownHosts3.exists()) {
			database.addHostkeys(knownHosts3);
		} else if (knownHosts4 != null && knownHosts4.exists()) {
			database.addHostkeys(knownHosts4);
		} else {
			LOG.debug("File /root/.ssh/known_hosts not found.");
		}

		Connection conn = new Connection(serverAddress);
		/* Now connect and use the SimpleVerifier */

		conn.connect(new SimpleVerifier(database),connectTimeout,kexTimeout);

		/*
		 * Authenticate. If you get an IOException saying something like
		 * "Authentication method password not supported by the server at
		 * this stage." then please check the FAQ.
		 */

		boolean triedCustomPublicKey = false;
		boolean triedStandardDSAPublicKey = false;
		boolean triedStandardRSAPublicKey = false;
		boolean triedPassword = false;
		boolean triedPasswordInteractive = false;

		boolean isAuthenticated = false;
		if (privateKeyPath != null) {
			triedCustomPublicKey = true;
			isAuthenticated = conn.authenticateWithPublicKey(username,
					new File(privateKeyPath), password);
		} else {

			try {
				triedStandardDSAPublicKey = true;
				isAuthenticated = conn.authenticateWithPublicKey(username,
						new File(home, ".ssh/id_dsa"), password);
			} catch (IOException ex) {
				// dsa key probably can't be found
				LOG.error(ex.getStackTrace());
			}
			if (!isAuthenticated) {
				try {
					triedStandardRSAPublicKey = true;
					isAuthenticated = conn.authenticateWithPublicKey(
							username, new File(home, ".ssh/id_rsa"),
							password);
				} catch (IOException ex) {
					// rsa key probably can't be found
					LOG.error(ex.getStackTrace());
				}
			}
		}

		if (!isAuthenticated) {
			try {
				triedPassword = true;
				isAuthenticated = conn.authenticateWithPassword(username,
						password);
			} catch (IOException ex) {
				// Password authentication probably not supported
				LOG.error(ex.getStackTrace());
			}
			if (!isAuthenticated) {
				try {
					triedPasswordInteractive = true;
					InteractiveCallback icb = new InteractiveCallback() {

						public String[] replyToChallenge(String name,
								String instruction, int numPrompts,
								String[] prompt, boolean[] echo)
								throws Exception {
							String[] responses = new String[numPrompts];
							for (int x = 0; x < numPrompts; x++) {
								responses[x] = password;
							}
							return responses;
						}
					};
					isAuthenticated = conn
							.authenticateWithKeyboardInteractive(username,
									icb);
				} catch (IOException ex) {
					// Password interactive probably not supported
					LOG.error(ex);
				}
			}
		}

		if (!isAuthenticated) {
			throw new IOException(
					"Authentication failed.  Tried \n"
							+ (triedCustomPublicKey ? "\tpublic key using "
									+ privateKeyPath + "\n" : "")
							+ (triedStandardDSAPublicKey ? "\tpublic key using ~/.ssh/id_dsa\n"
									: "")
							+ (triedStandardRSAPublicKey ? "\tpublic key using ~/.ssh/id_rsa\n"
									: "")
							+ (triedPassword ? "\tpassword\n" : "")
							+ (triedPasswordInteractive ? "\tpassword interactive"
									: ""));
		}
		
		return conn;
	}
	
	private InputStream armaZip(List<ControlDescripcionArchivoDTO> plantillaList) throws Exception{
		ByteArrayOutputStream baos = new ByteArrayOutputStream();
	    ZipOutputStream os = new ZipOutputStream(baos);
		os.setLevel(Deflater.DEFAULT_COMPRESSION);
		os.setMethod(Deflater.DEFLATED);
		try {
			int i = 1;
			for(ControlDescripcionArchivoDTO transporte: plantillaList){
				ZipEntry entrada = new ZipEntry(transporte.getNombreArchivo()+ "." +transporte.getExtension());
				os.putNextEntry(entrada);
				
				InputStream fis = transporte.getInputStream();
				byte [] buffer = new byte[1024];
				int leido=0;
				while (0 < (leido=fis.read(buffer))){
					os.write(buffer,0,leido);
				}
				fis.close();
				os.closeEntry();
				i++;
			}
		} catch (IOException e) {
			LOG.error(e);
		} finally {
			os.close();
		}
		InputStream is = new ByteArrayInputStream(baos.toByteArray());
		baos.close();
		
		return is;
	}
	
	private void enviarArchivoSFTP(SFTPv3Client client, InputStream is, String nombreArchivo) throws IOException{
        SFTPv3FileHandle handle = client.createFile(nombreArchivo); 
        byte[] buffer = new byte[1024];
        int i=0;
        long offset=0;
        while ((i = is.read(buffer)) != -1) {
            client.write(handle,offset,buffer,0,i);
            offset+= i;
        }
        client.closeFile(handle);
        if (handle.isClosed()){
        	LOG.trace("closed");
        }
	}
	
	
	@SuppressWarnings("unused")
	@Override
	public InputStream obtenerOrdenRenovacionProveedor(BigDecimal idToOrdenRenovacion){
		LOG.trace("Inicia a Obtener Orden de Proveedor");
		InputStream ordenStream = null;

		String sFTP = sistemaContext.getEnvioProovedorFTP();
		int port = sistemaContext.getEnvioProovedorPort();
		String sUser = sistemaContext.getEnvioProovedorUser();
		String sPassword = sistemaContext.getEnvioProovedorPassword();
		String directory = sistemaContext.getEnvioProovedorDirectory();
		String privateKeyPath = null;
        int connectTimeout  = 30000;
        int kexTimeout   = 30000;
		
		OrdenRenovacionMasiva orden = entidadService.findById(OrdenRenovacionMasiva.class, idToOrdenRenovacion);
		SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMdd");
		SimpleDateFormat sdfH = new SimpleDateFormat("HHmm");
		String nameFile = "OrdendeRenovacion" + idToOrdenRenovacion.toPlainString() + "_" + sdf.format(orden.getFechaCreacion()) + "_" + sdfH.format(orden.getFechaCreacion());
		
        Connection conn = null;
        SFTPv3Client client = null;
		try {
			String remoteDirectory = directory;

			conn = this.getConnectionSFTPAuth();
			
			LOG.trace("Inicia Obtener Archvo Orden de Proveedor");
			client = new SFTPv3Client(conn);
			ordenStream = recibirArchivoSFTP(client, remoteDirectory + "/" + nameFile + ".xls");
			LOG.trace("Termina Obtener Archvo Orden de Proveedor");
		} catch (java.net.ConnectException e) {
			LOG.error(e);
		} catch (IOException e) {
			LOG.error(e);
		} catch (Exception e) {
			LOG.error(e);
		} finally {
            if(client!=null){
                client.close();
          }          
          if(conn!=null){
                conn.close();
          }
		}
		LOG.trace("Termina a Obtener Orden de Proveedor");
		return ordenStream;
	}
	
	private InputStream recibirArchivoSFTP(SFTPv3Client client,String nombreArchivo) throws IOException{
        SFTPv3FileHandle handle = client.openFileRO(nombreArchivo); 

        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        long offset = 0;
        byte[] buffer = new byte[1024];
        while (true) {
        	int len = client.read(handle, offset,buffer, 0, buffer.length);
        	if (len <= 0)
        		break;
        	stream.write(buffer, 0, len);
        	offset += len;
            }      
        client.closeFile(handle);
        if (handle.isClosed()){
        	LOG.trace("closed");
        }
        return new ByteArrayInputStream(stream.toByteArray());
	}
	
	@Override
	public DomicilioImpresion obtieneDomicilioPorId(BigDecimal idDomicilio){
		DomicilioImpresion domicilio = null;
		try {
			domicilio = domicilioFacadeRemote.findDomicilioImpresionById(idDomicilio.longValue());
		} catch (Exception e) {
			LOG.error(e);
		}
		return domicilio;
	}
	
	@Override
	public ClienteGenericoDTO obtieneClienteContratantePorId(BigDecimal idCliente){
		ClienteGenericoDTO cliente = null;
		try {
			ClienteGenericoDTO filtro = new ClienteGenericoDTO();
			filtro.setIdCliente(idCliente);
			cliente = clienteFacade.loadById(filtro);
		} catch (Exception e) {
			LOG.error(e);
		}
		return cliente;
	}
	
	@Override
	public FormaPagoDTO obtieneFormaPagoPorId(BigDecimal idFormaPago){
		FormaPagoDTO formaPagoDTO = null;
		formaPagoDTO = entidadService.findById(FormaPagoDTO.class, idFormaPago.intValue());
		return formaPagoDTO;
	}
	
	@Override
	public MedioPagoDTO obtieneMedioPagoPorId(BigDecimal idMedioPago){
		MedioPagoDTO medioPagoDTO = null;
		MedioPagoDTO filtro = new MedioPagoDTO(); 
		filtro.setIdMedioPago(idMedioPago.intValue());
		medioPagoDTO = medioPagoFacadeRemote.findById(filtro);
		return medioPagoDTO;
	}
	
	@Override
	public TipoPolizaDTO obtieneTipoPolizaPorId(BigDecimal idTipoPoliza){
		TipoPolizaDTO tipoPolizaDTO = new TipoPolizaDTO();
		try{
			tipoPolizaDTO = entidadService.findById(TipoPolizaDTO.class, idTipoPoliza);
		}catch(Exception e){
			LOG.error(e);
		}
		return tipoPolizaDTO;
	}
	
	@Override
	public BigDecimal obtieneValorDescuento(BigDecimal idToCotizacion){
		BigDecimal descuento = BigDecimal.ZERO;
		try{
			CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
			if(cotizacion != null && cotizacion.getPorcentajeDescuentoGlobal() != null){
				Double descuentoGlobal = cotizacion.getPorcentajeDescuentoGlobal();
				descuento = new BigDecimal(descuentoGlobal);
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return descuento;
	}
	
	@Override
	public Short obtieneTipoRiesgo(BigDecimal idToCotizacion){
		Short tipoRiesgo = NegocioRenovacion.TipoRiesgo.NORMAL.getObtenerTipo();
		try{
			List<IncisoCotizacionDTO> incisos = incisoService.findByCotizacionId(idToCotizacion);
			if(incisos != null && !incisos.isEmpty()){
				for(IncisoCotizacionDTO inciso : incisos){
					String estiloId = inciso.getIncisoAutoCot().getEstiloId();
					EstiloVehiculoId id = new EstiloVehiculoId();
					id.setStrId(estiloId);
					EstiloVehiculoDTO estilo = entidadService.findById(EstiloVehiculoDTO.class, id);
					if(estilo != null && estilo.getAltoRiesgo() != null && estilo.getAltoRiesgo()){
						tipoRiesgo = NegocioRenovacion.TipoRiesgo.ALTORIESGO.getObtenerTipo();
						break;
					}
					if(estilo != null && estilo.getAltaFrecuencia() != null && estilo.getAltaFrecuencia()){
						tipoRiesgo = NegocioRenovacion.TipoRiesgo.ALTAFRECUENCIA.getObtenerTipo();
						break;
					}
				}
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return tipoRiesgo;
	}
	
	@Override
	public void calcularCotizacion(CotizacionDTO cotizacion){
		cotizacion = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
		entidadService.refresh(cotizacion);
		calculoService.restaurarPrimaCotizacion(cotizacion.getIdToCotizacion());
	}
	
	@Override
	public CoberturaDTO getCobertura(BigDecimal idToCobertura){
		return entidadService.findById(CoberturaDTO.class, idToCobertura);
	}
	
	@Override
	public String getIdAgente(Long id){
		Agente agente = new Agente();
		agente.setId(id);
		agente = agenteMidasService.loadByIdImpresiones(agente);
		return agente.getIdAgente().toString();
	}
	
	@Override
	public CuentaPagoDTO obtenerConductoCobro(BigDecimal idToCotizacion){
		CuentaPagoDTO cuentaPagoDTO = null;
		try{
			cuentaPagoDTO = pagoService.obtenerConductoCobro(idToCotizacion);
		}catch(Exception e){
		}
		return cuentaPagoDTO;
	}
	
	@Override
	public EstiloVehiculoDTO getEstiloVehiculo(BigDecimal idTcMarcaVehiculo, BigDecimal idToNegSeccion, BigDecimal idMoneda, String claveEstilo){
		EstiloVehiculoDTO estilo = null;
		if(idTcMarcaVehiculo != null){
			EstiloVehiculoDTO estiloVehiculo = new EstiloVehiculoDTO();
			estiloVehiculo.getId().setClaveEstilo(claveEstilo);
			//if (negocioSeccion != null) {
			//	estiloVehiculo.setIdTcTipoVehiculo(negocioSeccion.getSeccionDTO().getTipoVehiculo().getIdTcTipoVehiculo());	
			//}
			MarcaVehiculoDTO marcaVehiculoDTO = marcaVehiculoFacadeRemote.findById(idTcMarcaVehiculo);
			estiloVehiculo.setMarcaVehiculoDTO(marcaVehiculoDTO);
			estiloVehiculo.setTipoBienAutosDTO(marcaVehiculoDTO.getTipoBienAutosDTO());
			NegocioAgrupadorTarifaSeccion negocioAgrupadorTarifaSeccion  = new NegocioAgrupadorTarifaSeccion ();
			negocioAgrupadorTarifaSeccion = negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(idToNegSeccion, idMoneda);
			
			List<EstiloVehiculoDTO> estiloVehiculoList = estiloVehiculoFacadeRemote.listarDistintosFiltradoVersionAgrupador(estiloVehiculo, negocioAgrupadorTarifaSeccion, idToNegSeccion);
			if(estiloVehiculoList != null && !estiloVehiculoList.isEmpty()){
				for(EstiloVehiculoDTO item : estiloVehiculoList){
					if(item.getId().getClaveEstilo().equals(claveEstilo)){
						estilo = item;
						break;
					}
				}
					
			}
		}
		return estilo;
	}
	
	@Override
	public void setNuevoPorcentajeDeducibleDaniosMateriales(CotizacionDTO cotizacion, Double nuevoPorcentajeDeducible){
		try{
		List<IncisoCotizacionDTO> incisosList = incisoService.getIncisos(cotizacion.getIdToCotizacion());
		for(IncisoCotizacionDTO inciso : incisosList){
			//Seccion
			NegocioSeccion negSeccion = entidadService.findById(NegocioSeccion.class,  new BigDecimal(inciso.getIncisoAutoCot().getNegocioSeccionId()));
			
			//Obtener cobertura danios materiales
			Map<String, Object> parametros = new LinkedHashMap<String, Object>();
			parametros.put("id.idToCotizacion", cotizacion.getIdToCotizacion());
			parametros.put("id.numeroInciso", inciso.getId().getNumeroInciso());
			parametros.put("id.idToSeccion", negSeccion.getSeccionDTO().getIdToSeccion());
			parametros.put("coberturaSeccionDTO.coberturaDTO.claveTipoCalculo", CoberturaDTO.CLAVE_CALCULO_DANOS_MATERIALES);
			List<CoberturaCotizacionDTO> coberturaList = entidadService.findByProperties(CoberturaCotizacionDTO.class, parametros);
			if(coberturaList != null && !coberturaList.isEmpty()){
				CoberturaCotizacionDTO cobertura = coberturaList.get(0);
				Double valor = cobertura.getPorcentajeDeducible().doubleValue() - nuevoPorcentajeDeducible.doubleValue();
				
				if(valor.doubleValue() < valorMinimoPorcentaje.doubleValue()){
					valor = valorMinimoPorcentaje;
				}
				cobertura.setPorcentajeDeducible(valor);
				cobertura.setValorDeducible(valor);
				entidadService.save(cobertura);
			}
		}
		}catch(Exception e){
			LOG.error(e);
		}
	}
	
	@Override
	public NegocioDerechoPoliza obtenerDechosPolizaDefault(Long idToNegocio) {
		return negocioDerechosService.obtenerDechosPolizaDefault(idToNegocio);
	}
	
	@Override
	public List<CoberturaCotizacionDTO> obtenerCoberturasNegocio(NegocioPaqueteSeccion negocioPaqueteSeccion, IncisoCotizacionDTO incisoCotizacion){
		List<CoberturaCotizacionDTO> coberturaCotizacionList  = new ArrayList<CoberturaCotizacionDTO>(1);
		
		Long numeroSecuencia = new Long("1");
		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.setStrId(incisoCotizacion.getIncisoAutoCot().getEstiloId());
		
		coberturaCotizacionList = coberturaService.getCoberturas(negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(), 
				negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(), 
				negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(), 
				negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(), 
				negocioPaqueteSeccion.getPaquete().getId(), incisoCotizacion.getIncisoAutoCot().getEstadoId(), incisoCotizacion.getIncisoAutoCot().getMunicipioId(), incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getCotizacionDTO().getIdMoneda().shortValue(), numeroSecuencia,
				estiloVehiculoId.getClaveEstilo(), incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), new BigDecimal(incisoCotizacion.getIncisoAutoCot().getTipoUsoId()), null, null);
		
		return coberturaCotizacionList;
	}
	
	@Override
	public NegocioPaqueteSeccion obtenerNegocioPaqueteSeccionPorId(Long id){
		return entidadService.findById(NegocioPaqueteSeccion.class, id);
	}
	
	@Override
	public String obtenerMontoPrimerRecibo(CotizacionDTO cotizacion){
		String montoPrimerRecibo = "";
		try{
			List<EsquemaPagoCotizacionDTO> esquemaPagoCotizacionList=cotizacionService.setEsquemaPagoCotizacion(cotizacion,cotizacion.getFechaInicioVigencia(), cotizacion.getFechaInicioVigencia(),cotizacion.getFechaFinVigencia(),cotizacion.getValorTotalPrimas(), new BigDecimal(cotizacion.getValorDerechosUsuario()), new BigDecimal(cotizacion.getPorcentajeIva()), cotizacion.getValorPrimaTotal());
			if(esquemaPagoCotizacionList != null && !esquemaPagoCotizacionList.isEmpty()){
				for(EsquemaPagoCotizacionDTO esquema : esquemaPagoCotizacionList){
					if(esquema.getFormaPagoDTO().getIdFormaPago().equals(cotizacion.getIdFormaPago().intValue())){
						montoPrimerRecibo = esquema.getPagoInicial().getImporteFormat();
						break;
					}
				}
			}
		}catch(Exception e){
			LOG.error(e);
		}
		return montoPrimerRecibo;
	}
	
	@Override
	public void eliminaCotizacionFallida(CotizacionDTO cotizacionDTO){
		try{
			cotizacionService.cancelarCotizacion(cotizacionDTO.getIdToCotizacion());
		}catch(Exception e){
		}
	}
	
	@Override
	public void eliminaCotizacionFallidaPolizaAnt(BigDecimal idtoPoliza){
		try{
			cancelarCotizacion(idtoPoliza);
		}catch(Exception e){
		}
	}
	

	@Override
	public MensajeDTO getValidaNuevoNegocio(BigDecimal idToPoliza, BigDecimal pIdToNegSeccion) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_RENOVACION.spAut_ValidaRenNuevoNeg";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			storedHelper.estableceParametro("pIdToNegSeccion", pIdToNegSeccion);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	@Override
	public MensajeDTO programaPago(BigDecimal idToPoliza) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_RENOVACION.spAut_ProgramaPago";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			LOG.error(e);
			mensajeDTO = new MensajeDTO();
			mensajeDTO.setMensaje(e.getMessage());
		}
		return mensajeDTO;
	}
	
	public MensajeDTO polizaConIncisosPerdidaTotal(BigDecimal idToPoliza){
		MensajeDTO mensajeDTO = null;		
		String spName = "MIDAS.PKGAUT_RENOVACION.spAut_ValidaPerdidaTotal";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
				
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza",idToPoliza);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			LOG.error(e);
			mensajeDTO = new MensajeDTO();
			mensajeDTO.setMensaje(e.getMessage());
		}
		return mensajeDTO;
	}
	
	
	
	@Override
	public List<CotizacionEndosoDTO> buscarCotizacionesEndosoFiltrado(CotizacionEndosoDTO filtrosBusqueda){
		return cotizacionEndosoService.buscarCotizacionesEndosoFiltrado(filtrosBusqueda);
	}

	@Override
	public void deleteCotizacionEndoso(CotizacionEndosoDTO pendiente) {
		endosoService.deleteCotizacionEndoso(pendiente.getContinuityId());
	}
	
	@Override
	public CotizacionContinuity findCotizacionContinuity(BitemporalCotizacion cotizacion){
		return entidadContinuityService.findContinuityByKey(CotizacionContinuity.class,cotizacion.getContinuity().getId());
	}
	
	@Override
	public List<ControlEndosoCot> findControlEndosoCot(Map<String,Object> properties){
		return entidadService.findByProperties(ControlEndosoCot.class,properties);
	}
	
	@Override
	public void borrarMovimientos(ControlEndosoCot controlEndosoCot){
		movimientoEndosoBitemporalService.borrarMovimientos(controlEndosoCot);
	}
	
	@Override
	public void removeControlEndosoCot(ControlEndosoCot controlEndosoCot){
		entidadService.remove(controlEndosoCot);
	}
	
	@Override
	public void cancelarSolicitudesAutorizacion(BitemporalCotizacion cotizacion){
		endosoService.cancelarSolicitudesAutorizacion(cotizacion);
	}
	
	@Override
	public PaisDTO getPais(String countryId){
		return entidadService.findById(PaisDTO.class, countryId);
	}
	
	@Override
	public List<EntidadBitemporal> obtenerTexAdicionalCotCotinuityList(Long continuityId){
		return (List<EntidadBitemporal>) entidadBitemporalService.findByParentKey(TexAdicionalCotContinuity.class, 
				TexAdicionalCotContinuity.PARENT_KEY_NAME, continuityId);
	}
	
	@Override
	public Double getPctePagoFraccionado(Integer idFormaPago, Short idMoneda) {
		return listadoService.getPctePagoFraccionado(idFormaPago, idMoneda);
	}
	@Override
	public ResumenCostosDTO resumenCostosCaratulaPoliza(BigDecimal idToPoliza){
		return calculoService.resumenCostosCaratulaPoliza(idToPoliza);
	}
	@Override
	public SolicitudDTO obtieneSolicitudRenovacion(BigDecimal idToCotizacion){
		SolicitudDTO ultSolicitud = null;
		
		Map<String,Object> properties = new HashMap<String,Object>();
		properties.put("cotizacionId", idToCotizacion.longValue());
		properties.put("claveEstatusCot", CotizacionDTO.ESTATUS_COT_EMITIDA);
		
		List<ControlEndosoCot> controlEndosoCotList = entidadService.findByPropertiesWithOrder(ControlEndosoCot.class, properties, "id DESC");
		if(controlEndosoCotList != null && !controlEndosoCotList.isEmpty()){
			ControlEndosoCot controlEndosoCot = controlEndosoCotList.get(0);
			ultSolicitud = controlEndosoCot.getSolicitud();
		}		
		return ultSolicitud;
	}

	@Override
	public void calcularDescuentoPorEstado(CotizacionDTO cotizacion, Double descuentoPorNoSiniestro, NegocioRenovacionDescId id, boolean renovarEmitir){
		CotizacionDTO cotizacionDto = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
		entidadService.refresh(cotizacionDto);
		calculoService.restaurarDescuentoPorEstado(cotizacionDto.getIdToCotizacion(), descuentoPorNoSiniestro, id, renovarEmitir);
	}
	
	@Override
	public MensajeDTO getXmlCartaRenovacion(BigDecimal idToPoliza) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_RENOVACION.spAut_ObtieneXmlCartaRen";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO)storedHelper.obtieneResultadoSencillo();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return mensajeDTO;
	}
	
	@Override
	public void saveCargaMasiva(List<RegistroGuiasProveedorId> ids){
		trackingService.saveCargaMasiva(ids);
	}

	@Override
	public void procesarEstatusPendientes(){
		trackingService.procesarEstatusPendientes();
	}
	
	@Override
	public TransporteImpresionDTO getReporteEfectividad(OrdenRenovacionMasiva ordenRenovacion){
		TransporteImpresionDTO tranporte = new TransporteImpresionDTO();
		try {
			tranporte = trackingService.getReporteEfectividad(ordenRenovacion);
		} catch (Exception e) {
			LOG.error(e);
		}
		return tranporte;
	}
	
	@Override
	public void procesarTareaEstatusPendientes(){
		trackingService.procesarTareaEstatusPendientes();
	}
	
	@Override
	public NegocioRenovacionCobertura obtenerNegocioRenovacionCobertura(NegocioPaqueteSeccion negocioPaqueteSeccion, 
			Short tipoRiesgo, BigDecimal idToCobertura){
		
		NegocioRenovacionCobertura negocioRenovacionCobertura = null;
		try{
			NegocioRenovacionCoberturaId id = new NegocioRenovacionCoberturaId();
			id.setIdToNegocio(negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio());
			id.setTipoRiesgo(tipoRiesgo);
			id.setIdToSeccion(negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion());
			id.setIdToCobertura(idToCobertura);
		
			negocioRenovacionCobertura = entidadService.findById(NegocioRenovacionCobertura.class, id);
		}catch(Exception e){			
		}
		if(negocioRenovacionCobertura == null){
			negocioRenovacionCobertura = new NegocioRenovacionCobertura();
			negocioRenovacionCobertura.setAplicaDefault((short) 0);
		}
		
		return negocioRenovacionCobertura;
	}
	
	@Override
	public MensajeDTO programaPagoEndoso(BigDecimal idToPoliza) {
		MensajeDTO mensajeDTO = null;
		String spName = "MIDAS.PKGAUT_RENOVACION.spAut_ProgramaPagoEndoso";
		String[] atributosDTO = { "mensaje" };
		String[] columnasCursor = { "valido"};
		
		StoredProcedureHelper storedHelper = null;	
		
		try {
			storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pIdToPoliza", idToPoliza);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.MensajeDTO", atributosDTO, columnasCursor);

			mensajeDTO = (MensajeDTO) storedHelper.obtieneResultadoSencillo();
		} catch (SQLException e) {
			LOG.error("Excepcion en BD ..." + this+" ... \n"+e.getMessage());
			Integer codErr = null;
			String descErr = null;
			if (storedHelper != null) {
				codErr = storedHelper.getCodigoRespuesta();
				descErr = storedHelper.getDescripcionRespuesta();
				LOG.error("--> error:"+descErr);
			}
			StoredProcedureErrorLog.doLog(null,StoredProcedureErrorLog.TipoAccion.GUARDAR,spName, "", codErr, descErr);
		}  catch (Exception e) {
			LOG.error(e);
			mensajeDTO = new MensajeDTO();
			mensajeDTO.setMensaje(e.getMessage());
		}
		
		return mensajeDTO;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<OrdenRenovacionMasivaDet> obtienePolizaEnProcesoRenovacion(BigDecimal idToPoliza){
		try{
			String query = "Select t1 FROM OrdenRenovacionMasivaDet t1  " +
					" WHERE t1.id.idToPoliza =  " + idToPoliza + "" +
					" AND t1.claveEstatus IN ("+ OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_COTIZACION + ", "+ OrdenRenovacionMasivaDet.ESTATUS_TERMINADA + ", "  + OrdenRenovacionMasivaDet.ESTATUS_PROCESANDO_EMISION +" )" +
					" ";
			return (List<OrdenRenovacionMasivaDet>) entidadService.executeQueryMultipleResult(query, null);
		}catch(Exception e){
			LOG.error(e.getMessage());
		}
		return null;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<LayoutRenovacion> obtenerListaLayout(String fechaIncio, String fechaFin, Long idToNegocio, Long idToProducto) {
		List<LayoutRenovacion> lista = null;
		String spName = "MIDAS.PKGAUT_RENOVACION.spAut_LayoutRenovacion";
		String[] atributosDTO = { "estatus","idAgente","moneda","centroEmisor","ejecutivo","idCotizacion",
				"numPoliza","numLiquidacion","autProsa","fechaIniVig","fechaEmision","fechaFinVig",
				"lineaNegocio","paquete","idUsuario","tipoPersona","rfc","clienteVip","nombreCliente",
				"paternoCliente","maternoCliente","cp","colonia","telefono","clvAmis","modelo","repuve",
				"clvUso","placas","formaPago","numMotor","numSerie","valorComer","tPasajeros","descVehiculo",
				"primaTotal","deducMateriales","deducRoboTotal","limiteRCTerc","deducRCTerc","limiteGMedicos",
				"limiteMuerte","limiteRCViaj","aJuridica","limiteAdapt","limiteEquipo","deducEquipo","igualacion",
				"derechos","sAutorizacion","cAutorizacion","descEquipo","descAdaptacion","nombreConduc","paternoConduc",
				"maternoConduc","licencia","fechaNacim","ocupacion","idCliente","remolques","tipoCarga","observaciones",
				"nombreAseg","limiteAcciden","recargoPago","sumaDanios","sumaRobo","correo","altaCondicion","bajaCondicion" };
		String[] columnasCursor = { "ESTATUS","IDAGENTE","MONEDA","CENTROEMISOR","EJECUTIVO","IDCOTIZACION",
				"NUMPOLIZA","NUMLIQUIDACION","AUTPROSA","FECHAINIVIG","FECHAEMISION","FECHAFINVIG",
				"LINEANEGOCIO","PAQUETE","IDUSUARIO","TIPOPERSONA","RFC","CLIENTEVIP","NOMBRECLIENTE",
				"PATERNOCLIENTE","MATERNOCLIENTE","CP","COLONIA","TELEFONO","CLVAMIS","MODELO","REPUVE",
				"CLVUSO","PLACAS","FORMAPAGO","NUMMOTOR","NUMSERIE","VALORCOMER","TPASAJEROS","DESCVEHICULO",
				"PRIMATOTAL","DEDUCMATERIALES","DEDUCROBOTOTAL","LIMITERCTERC","DEDUCRCTERC","LIMITEGMEDICOS",
				"LIMITEMUERTE","LIMITERCVIAJ","AJURIDICA","LIMITEADAPT","LIMITEEQUIPO","DEDUCEQUIPO","IGUALACION",
				"DERECHOS","SAUTORIZACION","CAUTORIZACION","DESCEQUIPO","DESCADAPTACION","NOMBRECONDUC","PATERNOCONDUC",
				"MATERNOCONDUC","LICENCIA","FECHANACIM","OCUPACION","IDCLIENTE","REMOLQUES","TIPOCARGA","OBSERVACIONES",
				"NOMBREASEG","LIMITEACCIDEN","RECARGOPAGO","SUMADANIOS","SUMAROBO","CORREO","ALTACONDICION","BAJACONDICION"};
		try {
			StoredProcedureHelper storedHelper = new StoredProcedureHelper(
					spName, StoredProcedureHelper.DATASOURCE_MIDAS);
			storedHelper.estableceParametro("pFechaInicio", fechaIncio);
			storedHelper.estableceParametro("pFechaFin", fechaFin);
			storedHelper.estableceParametro("pIdToNegocio", idToNegocio);
			storedHelper.estableceParametro("pIdToProducto", idToProducto);
			
			storedHelper.estableceMapeoResultados("mx.com.afirme.midas2.dto.LayoutRenovacion", atributosDTO, columnasCursor);

			lista = (List<LayoutRenovacion>)storedHelper.obtieneListaResultados();
		} catch (Exception e) {
			throw new RuntimeException("Error en :" + spName, e);
		}
		return lista;
	}
}
