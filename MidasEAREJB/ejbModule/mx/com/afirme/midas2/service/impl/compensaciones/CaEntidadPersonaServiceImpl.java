/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.compensaciones.CaEntidadPersonaDao;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.compensaciones.CaEntidadPersona;
import mx.com.afirme.midas2.domain.compensaciones.negociovida.DatosSeycos;
import mx.com.afirme.midas2.service.compensaciones.CaEntidadPersonaService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless

public class CaEntidadPersonaServiceImpl  implements CaEntidadPersonaService {

	public static final String PARAMETROS_ID = "parametrosId";
	public static final String NOMBRES = "nombres";
	public static final String APATERNO = "apaterno";
	public static final String AMATERNO = "amaterno";
	
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	public static final String COMPENSADICIOID = "caCompensacion.id";
	public static final String TIPOENTIDADCA = "caTipoEntidad.id";
	public static final String TOAGENTEID = "agente.idAgente";
	public static final String PROVEEDORID = "idProveedor";

	@EJB
	private CaEntidadPersonaDao entidadPersonacaDao;
	
//    @PersistenceContext private EntityManager entityManager;
    private static final Logger LOGGER = Logger.getLogger(CaEntidadPersonaServiceImpl.class);
	
    public void save(CaEntidadPersona entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	entidadPersonacaDao.save(entity);//entityManager.persist(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public void delete(CaEntidadPersona entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	delete	::	INICIO	::	");
	        try {
//        	entity = entityManager.getReference(CaEntidadPersona.class, entity.getId());
	        	entidadPersonacaDao.delete(entity);//entityManager.remove(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaEntidadPersona update(CaEntidadPersona entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaEntidadPersona result = entidadPersonacaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaEntidadPersona 	::		CaEntidadPersonaServiceImpl	::	update	::	ERROR	::	",re);
	        throw re;
        }
    }
    
    public CaEntidadPersona findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaEntidadPersonaServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaEntidadPersona instance = entidadPersonacaDao.findById(id);//entityManager.find(CaEntidadPersona.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id 	::		CaEntidadPersonaServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaEntidadPersonaServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    public List<CaEntidadPersona> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaEntidadPersonaServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
//			final String queryString = "select model from CaEntidadPersona model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaEntidadPersonaServiceImpl	::	findByProperty	::	FIN	::	");
			return entidadPersonacaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaEntidadPersonaServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}
    public CaEntidadPersona findByPropertySingleResult(String propertyName, final Object value){
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaEntidadPersonaServiceImpl	::	findByPropertySingleResult	::	INICIO	::	");
    	try {
//			final String queryString = "select model from CaEntidadPersona model where model." 
//			 						+ propertyName + "= :propertyValue";
//			Query query = entityManager.createQuery(queryString);
//			query.setParameter("propertyValue", value);
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaEntidadPersonaServiceImpl	::	findByPropertySingleResult	::	FIN	::	");
			return  entidadPersonacaDao.findByPropertySingleResult(propertyName, value);//(CaEntidadPersona) query.getSingleResult();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaEntidadPersonaServiceImpl	::	findByPropertySingleResult	::	ERROR	::	",re);
			return null;
		}
    }

    public List<CaEntidadPersona> findByNombres(Object nombres
	) {
		return findByProperty(NOMBRES, nombres
		);
	}
    
    public List<CaEntidadPersona> findByParametrosId(Object parametrosId
	) {
		return findByProperty(PARAMETROS_ID, parametrosId
		);
	}
	
	public List<CaEntidadPersona> findByApaterno(Object apaterno
	) {
		return findByProperty(APATERNO, apaterno
		);
	}
	
	public List<CaEntidadPersona> findByAmaterno(Object amaterno
	) {
		return findByProperty(AMATERNO, amaterno
		);
	}
	    
	public List<CaEntidadPersona> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaEntidadPersona> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}	
	
	public List<CaEntidadPersona> findByProveedorId(Long proveedorId
	) {
		return findByProperty(PROVEEDORID, proveedorId
		);
	}
	
	public List<CaEntidadPersona> findByCompensAdicioId(Long compensAdicioId
	) {
		return findByProperty(COMPENSADICIOID, compensAdicioId
		);
	}
	public List<CaEntidadPersona> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaEntidadPersonaServiceImpl	::	findAll	::	INICIO	::	");
			try {
//				final String queryString = "select model from CaEntidadPersona model";
//				Query query = entityManager.createQuery(queryString);
				LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaEntidadPersonaServiceImpl	::	findAll	::	FIN	::	");
				return entidadPersonacaDao.findAll();//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaEntidadPersonaServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	public CaEntidadPersona findByCompensacionAndTipoEntidadAndClave(Long compensAdicioId, Long valorClave, Long tipoEntidad){
		
		LOGGER.info(">> findByCompensacionAndTipoEntidadAndClave()");
		CaEntidadPersona caEntidadPersona = null;
		String clave = null;
		
		try {			
			
			
			if(ConstantesCompensacionesAdicionales.AGENTE.equals(tipoEntidad)){
				clave = TOAGENTEID;
			}else if(ConstantesCompensacionesAdicionales.PROVEEDOR.equals(tipoEntidad)){
				clave = PROVEEDORID;
			}
			
			caEntidadPersona =  entidadPersonacaDao.findByCompensacionAndTipoEntidadAndClave(compensAdicioId,clave, valorClave ,tipoEntidad);
			
		} catch (Exception e) {
			LOGGER.error("Información del Error", e);			
		}
		LOGGER.info("<< findByCompensacionAndTipoEntidadAndClave()");
		return caEntidadPersona;
		
	}
	
	
	public CaEntidadPersona findByCompensacionAndTipoEntidadAndClave(Long compensAdicioId, DatosSeycos datosSeycos, Long tipoEntidad){
		LOGGER.info(">> findByCompensacionAndTipoEntidadAndClave()");
		CaEntidadPersona caEntidadPersona = null;		
		try {		
			
			caEntidadPersona =  entidadPersonacaDao.findByCompensacionAndTipoEntidadAndClave(compensAdicioId, datosSeycos ,tipoEntidad);
			
		} catch (Exception e) {
			LOGGER.error("Información del Error", e);			
		}
		LOGGER.info("<< findByCompensacionAndTipoEntidadAndClave()");
		return caEntidadPersona;
	}
	
	public List<CaEntidadPersona> findByAgente(Agente agente){
		LOGGER.info("	::	[INF]	::	Buscando por Agente 	::		CaEntidadPersonaServiceImpl	::	findByAgente	::	INICIO	::	");
		try {			
			LOGGER.info("	::	[INF]	::	Se busco por Agente 	::		CaEntidadPersonaServiceImpl	::	findByAgente	::	FIN	::	");
			return entidadPersonacaDao.findByAgente(agente);
		} catch (Exception e) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por CompensacionAdicionalId & Clave 	::		CaEntidadPersonaServiceImpl	::	findByAgente	::	ERROR	::	",e);
			return null;
		}
	}
}