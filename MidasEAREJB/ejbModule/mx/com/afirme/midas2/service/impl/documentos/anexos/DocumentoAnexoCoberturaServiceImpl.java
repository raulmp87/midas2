package mx.com.afirme.midas2.service.impl.documentos.anexos;

import java.io.File;
import java.io.FileInputStream;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.documentoanexo.DocumentoAnexoCoberturaFacadeRemote;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas.sistema.controlArchivo.FileManagerService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.documentos.anexos.DocumentoAnexoCoberturaService;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;

@Stateless
public class DocumentoAnexoCoberturaServiceImpl implements DocumentoAnexoCoberturaService{
	
	private static final Logger LOG = Logger
	.getLogger(DocumentoAnexoCoberturaServiceImpl.class);
	
	@EJB
	private EntidadService entidadService;	
	@EJB
	private DocumentoAnexoCoberturaFacadeRemote documentoAnexoCoberturaFacadeRemote;
	@PersistenceContext
	protected EntityManager entityManager;
	@EJB
	private FileManagerService fileManagerService;
	
	@Override
	public List<byte[]> getAnexosCobertura( List<BigDecimal> idCoberturaList ) throws Exception{
		
		List<DocumentoAnexoCoberturaDTO> list = documentoAnexoCoberturaFacadeRemote.listarAnexosPorCoberturas( idCoberturaList );
		return this.getBytesCoberturas( list );
		
	}
	
	private List<byte[]> getBytesCoberturas( List<DocumentoAnexoCoberturaDTO> documentoAnexoCoberturaList ) throws Exception{
		
		List<byte[]> anexosPdf = new ArrayList<byte[]>();	
		String uploadFolder = null;
		String OSName = System.getProperty("os.name");
		
		if (OSName.toLowerCase().indexOf("windows") != -1) {
			uploadFolder = SistemaPersistencia.UPLOAD_FOLDER;
		} else {
			uploadFolder = SistemaPersistencia.LINUX_UPLOAD_FOLDER;
		}
		
		for ( DocumentoAnexoCoberturaDTO item : documentoAnexoCoberturaList ){
			FileInputStream fin = null;
			
			try{
				
				BigDecimal controlArchivoId = item.getIdToControlArchivo();
				ControlArchivoDTO controlArchivo = entidadService.findById(ControlArchivoDTO.class, controlArchivoId);
				String fileName = controlArchivo.obtenerNombreArchivoFisico();

				if(!fileName.contains(".pdf")) {
					continue;				
				}

				byte byteArray[] = fileManagerService.downloadFile(fileName,  controlArchivo.getCodigoExterno());	
				anexosPdf.add(byteArray);
			} catch (Exception e) {
				LOG.error(e.getMessage(), e);
			} finally {
				IOUtils.closeQuietly(fin);
			}			
		}
		
		return anexosPdf;
	}

	@Override
	public List<byte[]> getAnexosCobertura(PolizaDTO polizaDTO) throws Exception {
		 List<BigDecimal> list = this.obtenerIdCoberturas(polizaDTO);
		 return this.getAnexosCobertura( list );
	}
	
	private List<BigDecimal> obtenerIdCoberturas(PolizaDTO polizaDTO) throws Exception {
		
		List<BigDecimal> idCoberturaList = new ArrayList<BigDecimal>();
		
		StringBuilder queryString = new StringBuilder("select distinct A5.cobertura_id ");		
		queryString.append("from MIDAS.topoliza a0, MIDAS.mcotizacionc a1, ");
		queryString.append("MIDAS.mincisoc a2, MIDAS.mautoincisoc a3, ");
		queryString.append("MIDAS.mseccionincisoc a4, MIDAS.mcoberturaseccionb a5b, ");
		queryString.append("MIDAS.mcoberturaseccionc a5 ");
		
		queryString.append("where a0.codigoproducto = ");
		queryString.append(polizaDTO.getCodigoProducto());
		queryString.append(" and a0.codigotipopoliza = ");
		queryString.append(polizaDTO.getCodigoTipoPoliza());
		queryString.append(" and a0.numeroPoliza = ");
		queryString.append(polizaDTO.getNumeroPoliza());
		queryString.append(" and a0.numerorenovacion = ");
		queryString.append(polizaDTO.getNumeroRenovacion());
		queryString.append(" and a0.idtocotizacion = a1.numero ");
		queryString.append("and a2.mcotizacionc_id = a1.id ");
		queryString.append("and a3.mincisoc_id = a2.id ");
		queryString.append("and a2.id = a4.mincisoc_id ");
		queryString.append("and a4.id = a5.mseccionincisoc_id ");
		queryString.append("and a5.id = a5b.mcoberturaseccionc_id ");
		queryString.append("and a5b.clavecontrato = 1 ");		
		queryString.append("order by cobertura_id desc ");		

		Query query = entityManager.createNativeQuery(queryString.toString());
		idCoberturaList = (List<BigDecimal>)query.getResultList();		
		
		return idCoberturaList;
	}

}
