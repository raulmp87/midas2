package mx.com.afirme.midas2.service.impl.catalogos;

import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.springframework.beans.BeanUtils;

import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas2.dao.catalogos.Entidad;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao.TipoOperacionHistorial;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ActualizacionAgente;
import mx.com.afirme.midas2.service.catalogos.EntidadHistoricoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
/**
 * Servicio para guaradr entidades de catalogo y que aparte ocupa guardar en el historial los cambios
 * @author vmhersil
 *
 */
@Stateless
public class EntidadHistoricoServiceImpl implements EntidadHistoricoService{
	private EntidadHistoricoDao entidadHistoricoDao;	
	private EntidadDao entidadDao;

	@Override
	public List<ActualizacionAgente> getHistory(TipoOperacionHistorial tipoOperacion,Long idRegistro) throws SystemException {
		return entidadHistoricoDao.getHistory(tipoOperacion, idRegistro);
	}

	@Override
	public ActualizacionAgente getLastUpdate(TipoOperacionHistorial tipoOperacion,Long idRegistro) throws SystemException {
		return entidadHistoricoDao.getLastUpdate(tipoOperacion, idRegistro);
	}
	
	@Override
	public ActualizacionAgente getHistoryRecord(TipoOperacionHistorial operationType,Long idRecord, String historyDate)throws SystemException
	{
		return entidadHistoricoDao.getHistoryRecord(operationType, idRecord, historyDate);
	}

	@Override
	public ActualizacionAgente saveHistory(TipoOperacionHistorial tipoOperacion,Long idRegistro, String comentarios, String usuario,String tipoMovimiento){
		return entidadHistoricoDao.saveHistory(tipoOperacion, idRegistro, comentarios, usuario,tipoMovimiento);
	}
	
	@Override
	public <E extends Entidad> List<E> findByProperties(Class<E> arg0,
			Map<String, Object> arg1) {
		return entidadHistoricoDao.findByProperties(arg0, arg1);
	}

	@Override
	public <E extends Entidad> List<E> findByPropertiesWithOrder(Class<E> arg0,
			Map<String, Object> arg1, String... arg2) {
		return entidadHistoricoDao.findByPropertiesWithOrder(arg0, arg1, arg2);
	}
	/***Sets && gets*****************************************************/
	@EJB
	public void setEntidadHistoricoDao(EntidadHistoricoDao entidadHistoricoDao) {
		this.entidadHistoricoDao = entidadHistoricoDao;
	}
	
	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}		
}
