package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.NoResultException;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;

import com.anasoft.os.daofusion.bitemporal.TimeUtils;

import mx.com.afirme.midas.catalogos.codigo.postal.CiudadDTO;
import mx.com.afirme.midas.catalogos.codigo.postal.EstadoFacadeRemote;
import mx.com.afirme.midas.catalogos.codigo.postal.MunicipioDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorDTO;
import mx.com.afirme.midas.catalogos.institucionbancaria.BancoEmisorFacadeRemote;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoDTO;
import mx.com.afirme.midas.catalogos.marcavehiculo.MarcaVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoDTO;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoFacadeRemote;
import mx.com.afirme.midas.catalogos.modelovehiculo.ModeloVehiculoId;
import mx.com.afirme.midas.catalogos.moneda.MonedaDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.CotizacionFacadeRemote;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionId;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteDTO;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacade;
import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas.interfaz.cliente.ClienteGenericoDTO;
import mx.com.afirme.midas.poliza.NumeroPolizaCompleto;
import mx.com.afirme.midas.poliza.NumeroPolizaCompletoMidas;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.poliza.PolizaFacadeRemote;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.SeccionDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.ClientSystemException;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.SystemException;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.seguridad.Rol;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.solicitud.SolicitudFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas2.dao.negocio.producto.NegocioProductoDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccionDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.agentes.VehiculoView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CoberturaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ConductorView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ContratanteView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionCoberturaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.CotizacionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.DatosPaqueteView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.DatosPolizaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.DatosVehiculoView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.EmisionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.EstiloView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.MotivosEndosoView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ParametrosImpresionCotizacionView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ParametrosImpresionPolizaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.ZonaView;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.CotizarEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.CotizarEndosoFlotillaResponse;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaRequest;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.EmitirEndosoFlotillaResponse;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.Inciso;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.ws.flotilla.InformacionDelVehiculo;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.catalogos.Domicilio;
import mx.com.afirme.midas2.domain.catalogos.Paquete;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Ejecutivo;
import mx.com.afirme.midas2.domain.emision.ppct.ReciboSeycos;
import mx.com.afirme.midas2.domain.negocio.Negocio;
import mx.com.afirme.midas2.domain.negocio.cliente.NegocioCliente;
import mx.com.afirme.midas2.domain.negocio.derechos.NegocioDerechoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.agrupadortarifa.NegocioAgrupadorTarifaSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.cobertura.deducibles.NegocioDeducibleCob;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.tipouso.NegocioTipoUso;
import mx.com.afirme.midas2.domain.negocio.zonacirculacion.NegocioEstado;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.IncisoAutoCot;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.tarea.EmisionPendiente.TipoEmision;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.Respuesta;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.endoso.ConductoCobroDTO;
import mx.com.afirme.midas2.dto.endoso.DatosTarjetaDTO;
import mx.com.afirme.midas2.dto.endoso.DatosTitularDTO;
import mx.com.afirme.midas2.dto.endoso.EndosoTransporteDTO;
import mx.com.afirme.midas2.dto.endoso.cotizacion.auto.CotizacionEndosoDTO;
import mx.com.afirme.midas2.dto.endoso.recibos.RecibosEndosoTransporteDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.dto.negocio.producto.tipopoliza.seccion.paquete.RelacionesNegocioPaqueteDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO;
import mx.com.afirme.midas2.dto.pago.CuentaPagoDTO.TipoCobro;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.EsquemaPagoCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.ResumenCostosGeneralDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.TerminarCotizacionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.cargamasiva.LogErroresCargaMasivaDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.recibos.MovimientoReciboDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoPolizaService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.CotizacionEndosoService;
import mx.com.afirme.midas2.service.endoso.cotizacion.auto.EndosoWSMidasAutosService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioAutosService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.EjecutivoService;
import mx.com.afirme.midas2.service.impl.excepcion.ExcepcionSuscripcionNegocioAutosServiceImpl;
import mx.com.afirme.midas2.service.negocio.NegocioService;
import mx.com.afirme.midas2.service.negocio.derechos.NegocioDerechosService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.NegocioSeccionService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccionService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.CotizacionService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.WSCotizacionGenericaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.agentes.CotizadorAgentesService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cargaMasiva.CargaMasivaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobertura.CoberturaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.extrainfo.CotizacionExtService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.IncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.recibos.ImpresionRecibosService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.ws.EmitirEndosoFlotillaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.ws.EndosoFlotillaService;
import mx.com.afirme.midas2.service.suscripcion.emision.EmisionService;
import mx.com.afirme.midas2.service.suscripcion.endoso.GeneraPlantillaReporteBitemporalService;
import mx.com.afirme.midas2.service.suscripcion.pago.PolizaPagoService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;
import mx.com.afirme.midas2.util.DateUtils;
import mx.com.afirme.midas2.util.UtileriasWeb;

@Stateless
public class WSCotizacionGenericaServiceImpl implements WSCotizacionGenericaService{
	
	private static final Logger LOG = Logger.getLogger(WSCotizacionGenericaServiceImpl.class);
	
	private static final Short MEDIO_PAGO_EFECTIVO = 15;
	private static final Short MEDIO_PAGO_TC = 4;
	private static final Short MEDIO_PAGO_DOMICILIACION = 8;
	private static final Short MEDIO_PAGO_CUENTA_AFIRME = 1386;
	private static final BigDecimal ID_PRODUCTO_AUTO_INDIVIDUAL = new BigDecimal(191);
	private static final BigDecimal ID_TIPO_POLIZA = new BigDecimal(1301);
	private static final BigDecimal ID_TIPO_POLIZA_FLOTILLA = new BigDecimal(1302);
	private EndosoService endosoServiceAut;
	
	@EJB
	private CotizacionService cotizacionService;
	@EJB
	private IncisoService incisoService;
	@EJB
	private EntidadService entidadService;
	@EJB
	private CoberturaService coberturaService;
	@EJB
	private CalculoService calculoService;
	@EJB
	private NegocioService negocioService;	
	@EJB
	private NegocioTipoPolizaService negocioTipoPolizaService;
	@EJB
	private NegocioDerechosService negocioDerechosService;
	@EJB
	private NegocioSeccionService negocioSeccionService;
	@EJB
	private NegocioPaqueteSeccionService negocioPaqueteSeccionService;
	@EJB
	private NegocioPaqueteSeccionDao negocioPaqueteSeccionDao;
	@EJB
	private PolizaPagoService pagoService;
	@EJB
	private ClienteFacadeRemote clienteFacade;	
	@EJB
	private EmisionService emisionService;
	@EJB
	private NegocioProductoDao negocioProductoDao;
	@EJB
	private AgenteMidasService agenteMidasService;
	@EJB
	private MarcaVehiculoFacadeRemote marcaVehiculoFacade;
	@EJB
	private GeneraPlantillaReporteBitemporalService generaPlantillaReporteBitemporalService;
	@EJB
	private EndosoService endosoService;
	@EJB
	private NegocioAgrupadorTarifaSeccionDao negAgrupadorTarifaSeccionDao;
	@EJB
	private TarifaAgrupadorTarifaService tarifaAgrupadorService;
	@EJB
	private CotizacionFacadeRemote cotizacionFacadeRemote;
	@EJB
	private EjecutivoService ejecutivoService;
	@EJB
	private SolicitudFacadeRemote solicitudFacadeRemote;
	@EJB 
	private EstiloVehiculoFacadeRemote estiloVehiculoFacade;
	@EJB
	private CargaMasivaService cargaMasivaService;
    @EJB
    private ListadoService listadoService;
    @EJB
    CotizadorAgentesService cotizadorAgentesService;
    @EJB(beanName = "UsuarioServiceDelegate")
    private UsuarioService usuarioService;
    @EJB
    private BancoEmisorFacadeRemote bancoEmisorFacadeRemote;
    @EJB
    private EndosoWSMidasAutosService endosoWSMidasAutosService;
    @EJB
	private PolizaFacadeRemote polizaFacadeRemote;
    @EJB
    private CotizacionEndosoService cotizacionEndosoService;
	@EJB
    private EndosoPolizaService endosoPolizaService;
    @EJB
    private EmisionEndosoBitemporalService emisionEndosoBitemporalService;
    @EJB
    private ImpresionRecibosService impresionRecibosService;
	@EJB 
	private ModeloVehiculoFacadeRemote modeloVehiculoFacade;
	@EJB
	private GenerarPlantillaReporte generarPlantillaReporte;
	@EJB
	private EstadoFacadeRemote estadoFacadeRemote;
	@EJB
	private EndosoFlotillaService endosoFlotillaService;
	@EJB
	private EmitirEndosoFlotillaService emitirEndosoFlotillaService;
	@EJB
	private EntidadBitemporalService entidadBitemporalService;
	@EJB
	private ListadoIncisosDinamicoService listadoIncisosDinamicoService;

	
    private SistemaContext sistemaContext;
    	
	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	
	@Autowired
	@Qualifier("endosoAutoServiceEJB")
	public void setEndosoService(EndosoService endosoService) {
		this.endosoServiceAut = endosoService;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Object> creaCotizacion(Long idNegocio, Double descuento, String folio, Long idPaquete, int idFormaPago, 
			Integer estilo, Integer modelo, Integer marca, String idEstadoCirculacion,String idMunicipioCirculacion, 
			String nombreAsegurado,String apellidoPaterno,String apellidoMaterno, String rfc,String claveSexo,
			String claveEstadoCivil,String idEstadoNacimiento,Date fechaNacimiento,String codigoPostal, String idCiudad, String idEstado,
			String colonia,String calleNumero, String telefonoCasa, String telefonoOficina, String email, BigDecimal idToNegProducto, 
			BigDecimal idToNegTipoPoliza, BigDecimal idToNegSeccion, Long idAgente, String observaciones, BigDecimal idCliente,
			List<CoberturaView> coberturaViewList, Date inicioVigencia, Date finVigencia, String token) throws SystemException {
		LOG.info(this.getClass().getName()+".creaCotizacion ... entrando a crearCotizacion");
		LOG.info("idNegocio => " + idNegocio);
		LOG.info("folio => " + folio);
		LOG.info("idPaquete => " + idPaquete);
		LOG.info("estilo => " + estilo);
		LOG.info("modelo => " + modelo);
		LOG.info("descuento => " + descuento);
		LOG.info("idAgente => " + idAgente);
		LOG.info("cp => " + codigoPostal);
		LOG.info("idFormaPago => " + idFormaPago);
		LOG.info("idToNegTipoPoliza => " + idToNegTipoPoliza);
		
		CotizacionView dto = this.populate(idNegocio, idToNegProducto, idToNegTipoPoliza, new BigDecimal(estilo), idToNegSeccion, new BigDecimal(modelo), 
				new BigDecimal(marca), idPaquete, idFormaPago, idEstadoCirculacion, idMunicipioCirculacion, descuento,
				nombreAsegurado, apellidoPaterno, apellidoMaterno, rfc, claveSexo,
				claveEstadoCivil, idEstadoNacimiento, fechaNacimiento, codigoPostal, idCiudad, idEstado, null,
				colonia, calleNumero, telefonoCasa, telefonoOficina, email, idAgente, observaciones, idCliente,
				coberturaViewList, inicioVigencia, finVigencia, folio);
		
		this.validarParametrosCotizacion(dto);
		
		CotizacionDTO cotizacion;
		BigDecimal idCotizacion = null;
		String user = "";
		Agente agente = new Agente();
		Usuario usuario = null;
		
		usuario = validarToken(token);
				user = usuario.getNombreUsuario();
		
		Integer usuarioExterno = 2;
		if(usuario!=null && usuario.getTipoUsuario()!=null && usuario.getTipoUsuario()>0) {
			usuarioExterno = usuario.getTipoUsuario();
		}
		
		if(usuarioExterno != 1 && observaciones != null && StringUtils.isNotEmpty(observaciones) ) {
			throw new SystemException("Los datos de observaciones no aplica para usuarios externos");
		}
		
		if (idAgente != null && idAgente > 0){
			agente.setIdAgente(idAgente);
			try {
				agente = agenteMidasService.findByClaveAgente(agente);
				
				if (agente.getId() == null || agente.getId() == 0){
					throw new SystemException("No fue posible obtener el agente con la clave "+idAgente+" dada.");	
				}
			} catch (Exception e1) {
				LOG.error("No fue posible obtener el agente\n"+e1.getMessage());
				throw new SystemException("No fue posible obtener el agente con la clave "+idAgente+" dada");
			}
		} else{
			try {
				agente = usuarioService.getAgenteUsuarioActual(usuario);
				if (agente == null){
					throw new SystemException("No se encontr\u00F3 agente para el usuario");
				}
			} catch (Exception e){
				LOG.error("No fue posible obtener el agente del usuario "+usuario.getNombreUsuario()+" \n"+e.getMessage());
				throw new SystemException("No fue posible obtener el agente del usuario "+usuario.getNombreUsuario());
			}
		}
		
		LOG.info("agente encontrado => " + agente.getIdAgente()+" codigoUsuarioAgente:"+agente.getCodigoUsuario()+" nombreUsuario:"+usuario.getNombreUsuario());
		
		if ( inicioVigencia==null && finVigencia==null ){
			inicioVigencia = new Date();		
			Calendar fechaC = Calendar.getInstance();
			fechaC.setTime(inicioVigencia);		
			fechaC.add(Calendar.MONTH, 12);
			finVigencia = fechaC.getTime();
		}
		
		try{
			this.validarFechas(inicioVigencia, finVigencia);
		}
		catch(SystemException e){
			LOG.error("error en la validacion de fechas\n"+e.getMessage());
			throw e;
		}
		
		Negocio negocio = null;
		
		if (idNegocio != null) {
			negocio = negocioService.findById(idNegocio);
		} else {
			throw new SystemException("El id del negocio es requerido.");
		}
			
		NegocioProducto negocioProducto = this.validarNegocioProducto(negocio, idToNegProducto);
		
		if (negocio.getFechaFinVigenciaFija() != null){
			finVigencia = negocio.getFechaFinVigenciaFija();
		}
		
		NegocioTipoPoliza negocioTipoPoliza = null;
		
		if (idToNegTipoPoliza != null) {
			try {
				negocioTipoPoliza = negocioTipoPolizaService.getPorIdNegocioProductoIdToNegTipoPoliza(negocioProducto.getIdToNegProducto(), idToNegTipoPoliza);
			} catch (NoResultException e) {
				throw new SystemException("El idTipoPoliza proporcionado es incorrrecto");
			} catch(Exception e){
			LOG.error("No se pudo obtener el negocioTipoPoliza:\n"+e.getMessage());
				throw new SystemException("No se pudo obtener el negocioTipoPoliza");
			}
		} else {
			throw new SystemException("El idTipoPoliza es requerido");
		}
		
		negocioTipoPoliza.setNegocioProducto(negocioProducto);
		
		NegocioDerechoPoliza negocioDerechoPoliza = null;
		
		try {
			negocioDerechoPoliza = negocioDerechosService.obtenerDechosPolizaDefault(idNegocio);
		} catch (Exception e) {
			throw new SystemException("Error al obtener los derechos de la p\u00F3liza para el negocio "+idNegocio);
		}
		
		boolean isNewCot = true;
		
		if (!folio.isEmpty()){
			isNewCot = evaluaFolioNuevaCotizacion(folio);
		}
		
		LOG.info("Folio: " + folio);

		if(isNewCot){			
			cotizacion = new CotizacionDTO();
			SolicitudDTO solicitud = new SolicitudDTO();		
			solicitud.setAgente(agente);		
			solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
			solicitud.setNegocio(negocio);
			cotizacion.setSolicitudDTO(solicitud);
			cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
			cotizacion.setTipoCotizacion(CotizacionDTO.TIPO_COTIZACION_AUTOINDIVIDUAL);
			

			//Valida forma de pago
			if (idFormaPago > 0) {
				final Map<Integer,String> mapFormasPago = listadoService.getMapFormasdePagoByNegTipoPoliza(negocioTipoPoliza.getIdToNegTipoPoliza());
				if (!mapFormasPago.containsKey(Integer.valueOf(idFormaPago))) {
					throw new SystemException("No se encontr\u00F3 la forma de pago " + idFormaPago);
				}
			} else {
				throw new SystemException("La forma de pago es requerido");
			}
			
			try {
				cotizacion = cotizacionService.crearCotizacion(cotizacion,user);
			} catch (Exception e) {
				throw new SystemException("Error al crear la cotizaci\u00F3n "+e);
			}
			
			cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
			cotizacion.setFechaInicioVigencia(inicioVigencia);
			cotizacion.setFechaFinVigencia(finVigencia);
			cotizacion.setIdFormaPago(BigDecimal.valueOf(idFormaPago));
			cotizacion.setIdMedioPago(new BigDecimal(negocioProducto
					.getNegocioMedioPagos().get(0).getMedioPagoDTO()
					.getIdMedioPago()));
			cotizacion.setIdMoneda(new BigDecimal(MonedaDTO.MONEDA_PESOS));
			cotizacion.setPorcentajePagoFraccionado(listadoService.getPctePagoFraccionado(
					cotizacion.getIdFormaPago().intValue(), cotizacion.getIdMoneda().shortValue()));
			cotizacion.setPorcentajebonifcomision(negocio.getPctPrimaCeder());
							
			cotizacion.setFolio(CotizacionDTO.TIPO_COTIZACION_AUTOINDIVIDUAL+"-"+cotizacion.getIdToCotizacion());
			
			try {
				cotizacionService.guardarCotizacion(cotizacion);
			} catch (Exception e) {
				throw new SystemException("Error al guardar la cotizaci\u00F3n");
			}
			
			idCotizacion = cotizacion.getIdToCotizacion();
			
		} else{
			List<CotizacionDTO> cotizacionByFolio = entidadService.findByProperty(CotizacionDTO.class, "folio", folio);
			cotizacion = cotizacionByFolio.get(0);
			cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
			cotizacion = actualizaDatosCotizacion(cotizacion, agente, negocio, negocioTipoPoliza, dto, user);
			idCotizacion = cotizacion.getIdToCotizacion();
		}
		
		LOG.info("idCotizacion generada:" + idCotizacion+" isNewCot:"+isNewCot);
		cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);

		//Recalcula si cambio negocio, paquete, estilo, modelo, marca, estadoDeCirculacion, municipioCirculacion, valorFactura, descuento
		boolean hasChanges = false;
		
		//Valida estado-municipio
		try{
			HashMap<String,Object> propNeg = new HashMap<String,Object>();
			propNeg.put("negocio.idToNegocio",idNegocio);
			List<NegocioEstado> negEstadoList = entidadService.findByProperties(NegocioEstado.class, propNeg);
			
			if(negEstadoList != null && !negEstadoList.isEmpty()){
				HashMap<String,Object> properties = new HashMap<String,Object>(1);
				properties.put("estadoDTO.stateId",idEstadoCirculacion);
				properties.put("negocio.idToNegocio",idNegocio);
				List<NegocioEstado> negocioEstadoList = entidadService.findByProperties(NegocioEstado.class, properties);
				if(negocioEstadoList == null || negocioEstadoList.isEmpty()){
					throw new SystemException("Estado no valido ("+idEstadoCirculacion+")");
				}
				HashMap<String,Object> propertiesCity = new HashMap<String,Object>(1);
				propertiesCity.put("stateId",idEstadoCirculacion);
				propertiesCity.put("municipalityId",idMunicipioCirculacion);
				List<MunicipioDTO> municipioList = entidadService.findByProperties(MunicipioDTO.class, propertiesCity);			
				if(municipioList == null || municipioList.isEmpty()){
					throw new SystemException("Municipio no valido ("+idMunicipioCirculacion+")");
				}			
			}
		} catch(Exception e){
			throw new SystemException(e.getMessage()!=null?e.getMessage():"Fallo al validar el estado-municipio");
		}
		
		BigDecimal oldValorPrimaTotal = null;
		
		if(!isNewCot){
			oldValorPrimaTotal = cotizacion.getValorPrimaTotal();
			List<IncisoCotizacionDTO> incisos = incisoService.getIncisos(idCotizacion);
			
			if (incisos != null && incisos.size() > 0){
				hasChanges = this.cambioDatosAuto(cotizacion, incisos, idNegocio, idPaquete, estilo, modelo.toString(), marca, idEstadoCirculacion, 
					idMunicipioCirculacion, descuento, null);
			}
		}
		
		IncisoCotizacionDTO incisoCotizacion = null;
		
		
		NegocioSeccion negocioSeccion =  null;
		
		if (idToNegSeccion != null) {
			negocioSeccion = entidadService.findById(NegocioSeccion.class, idToNegSeccion);
		} else {
			throw new SystemException("El idLineaNegocio es requerido");
		}
		
		if (negocioSeccion != null) {
			try {
				incisoCotizacion = this.guardaInciso(cotizacion, negocio, dto, negocioTipoPoliza, negocioSeccion);
			} catch(SystemException e){
			LOG.error("Error al guardar el inciso:\n"+e.getMessage());
				throw new SystemException(e.getMessage());
			} catch(Exception e){
			LOG.error("Error al guardar el inciso.. "+e.getMessage());
				throw new SystemException("Error al guardar el inciso... "+e.getMessage());
			}
		} else {
			throw new SystemException("El idLineaNegocio proporcionado es incorrecto");
		}
		
		List<CoberturaCotizacionDTO> coberturaCotizacionListEnProceso = new LinkedList<CoberturaCotizacionDTO>();
		
		try {
			coberturaCotizacionListEnProceso = coberturaService.getCoberturas(incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId(), incisoCotizacion.getId());
		} catch(Exception e){
			LOG.error("--> Error al obtener las coberturas");
			throw new SystemException("Error al obtener las coberturas");
		}
		
		try {
			incisoCotizacion = incisoService.prepareGuardarIncisoAgente(
				incisoCotizacion.getId().getIdToCotizacion(),
				incisoCotizacion.getIncisoAutoCot(), incisoCotizacion,
				coberturaCotizacionListEnProceso,null, null, null, null);
		} catch (Exception e) {
			LOG.error("--> Error al ejecutar el metodo incisoService.prepareGuardarIncisoAgente");
		}
		
		//Recalcula cotizacion
		incisoCotizacion = calculoService.calcular(incisoCotizacion);
		TerminarCotizacionDTO terminarCotizacionDTO = cotizacionService.terminarCotizacion(cotizacion.getIdToCotizacion(), false);
		
		//Valida resultado terminar
		cotizacionService.validaTerminarCotizacionWS(terminarCotizacionDTO);
		
		ResumenCostosGeneralDTO resumenCostosGeneralDTO = calculoService.obtenerResumenGeneral(incisoCotizacion.getCotizacionDTO());
		
		if(!isNewCot && !hasChanges && BigDecimal.valueOf(resumenCostosGeneralDTO.getCaratula().getPrimaTotal()).compareTo(oldValorPrimaTotal) != 0){
			calculoService.igualarPrima(cotizacion.getIdToCotizacion(), oldValorPrimaTotal.doubleValue(), false);
			cotizacionService.terminarCotizacion(cotizacion.getIdToCotizacion(), false);
			calculoService.obtenerResumenGeneral(incisoCotizacion.getCotizacionDTO());
		}
		
		if(idCliente == null || idCliente.compareTo(BigDecimal.ZERO) <= 0) {
			if(codigoPostal != null && !codigoPostal.isEmpty() && !"0".equalsIgnoreCase(codigoPostal)){
				LOG.info("Antes de guardar el contratante -> cp:"+codigoPostal);
				this.saveClienteCot(cotizacion.getIdToCotizacion(), dto.getContratante());
			}
		} else {
			cotizacionService.actualizarDatosContratanteCotizacion(cotizacion.getIdToCotizacion(), idCliente, null);
		}
		
		HashMap<String, Object> map = new HashMap<String, Object>(1);
		Map<String, Object> excResultado = evalueExcepcion(cotizacion);//JFGG
		LOG.info("--> map: " + map);
		if(excResultado!= null && !excResultado.containsKey("statusExcepcion") && "true".equals(String.valueOf(excResultado.get("statusExcepcion")))){//JFGG
			throw new SystemException( String.valueOf(excResultado.get("mensaje")));
		} else {//JFGG
			map.put("idToCotizacion", cotizacion.getIdToCotizacion());
			
			if (resumenCostosGeneralDTO.getCaratula() != null){
				map.put("primaTotal", resumenCostosGeneralDTO.getCaratula().getPrimaTotal());
				map.put("descuentos", resumenCostosGeneralDTO.getCaratula().getDescuentoComisionCedida());
				map.put("primaNeta", resumenCostosGeneralDTO.getCaratula().getPrimaNetaCoberturas());
				map.put("recargo", resumenCostosGeneralDTO.getCaratula().getRecargo());
				map.put("derechoPago", resumenCostosGeneralDTO.getCaratula().getDerechos());
				map.put("iva", resumenCostosGeneralDTO.getCaratula().getIva());
			}
			map.put("folio", cotizacion.getFolio());
			
			List<CoberturaCotizacionDTO> coberturaCotizacionList = new LinkedList<CoberturaCotizacionDTO>();
			
			try {
				coberturaCotizacionList = coberturaService.getCoberturas(incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId(), incisoCotizacion.getId());
			} catch(Exception e){
				LOG.error("--> Error al obtener las coberturas... "+e.getMessage());
			}
			
			Map<String, Object> coberturasMap =  new HashMap<String, Object>(1);
			int cont = 1;
			LOG.info("--> coberturaCotizacionList.size:"+coberturaCotizacionList.size());
			for (CoberturaCotizacionDTO cob: coberturaCotizacionList){
				if (new Short("1").equals(cob.getClaveContrato())) {
					String sumaAseguradaStr = "";

					switch (Integer.valueOf(cob.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada())) {
					case 1:
						sumaAseguradaStr = "Valor Comercial";
						break;
					case 2:
						sumaAseguradaStr = "Valor Factura";
						break;
					case 9:
						sumaAseguradaStr = "Valor Convenido";
						break;
					case 3:
						sumaAseguradaStr = "Amparada";
						break;
					case 0:
						sumaAseguradaStr = cob.getValorSumaAsegurada().toString();
						break;
					default:
						break;
					}

					Map<String, Object> cobMap = new HashMap<String, Object>(1);
					cobMap.put("idCobertura", cob.getId().getIdToCobertura());
					cobMap.put("descripcion", cob.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
					cobMap.put("sumaAsegurada", sumaAseguradaStr ); 
					cobMap.put("deducible", cob.getValorDeducible());
					cobMap.put("primaNeta", cob.getValorPrimaNeta()); 

					coberturasMap.put(StringUtils.EMPTY + cont, cobMap);
					cont++;
				}
			}
			LOG.info("--> coberturasMap.size:"+coberturasMap.size());
			LOG.info("--> coberturasMap:"+coberturasMap);
			
			map.put("coberturas", coberturasMap);
			
			List<EsquemaPagoCotizacionDTO> esquemaPagoCotizacionList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
			
			try {
				cotizacion.setFechaInicioVigencia(inicioVigencia);
				cotizacion.setFechaFinVigencia(finVigencia);
				cotizacion.setIdFormaPago(BigDecimal.valueOf(idFormaPago));
				cotizacion.setValorTotalPrimas(BigDecimal.valueOf(resumenCostosGeneralDTO.getCaratula().getPrimaNetaCoberturas()));
				cotizacion.setValorDerechosUsuario(resumenCostosGeneralDTO.getCaratula().getDerechos());
				cotizacion.setPorcentajeIva(16d);
				cotizacion.setValorPrimaTotal(BigDecimal.valueOf(resumenCostosGeneralDTO.getCaratula().getPrimaTotal()));
				
				esquemaPagoCotizacionList = cotizadorAgentesService.verEsquemaPagoAgente(cotizacion);
				
				cont = 1;
				Map<String, Object> esquemaPagoMap =  new HashMap<String, Object>(1);
				
				for (EsquemaPagoCotizacionDTO esq: esquemaPagoCotizacionList){
					Map<String, Object> esqMap = new HashMap<String, Object>(1);
					esqMap.put("formaPago", esq.getFormaPagoDTO().getDescripcion());
					esqMap.put("pagoInicial", esq.getPagoInicial().getImporteFormat());
					esqMap.put("noRecibosSubsecuentes", (esq.getPagoSubsecuente() != null && esq.getPagoSubsecuente().getNumExhibicion() != null)? esq.getPagoSubsecuente().getNumExhibicion(): 0);
					esqMap.put("pagoSubsecuente", (esq.getPagoSubsecuente() != null && esq.getPagoSubsecuente().getImporteFormat() != null)? esq.getPagoSubsecuente().getImporteFormat(): 0);
					esquemaPagoMap.put(StringUtils.EMPTY+cont, esqMap);
					cont++;
				}
				
				map.put("esquemaPago", esquemaPagoMap);
			} catch(Exception e){
				LOG.info("--> error al obtener el esquema de pago:\n"+e.getMessage());
			}
		}//JFGG
		return map;
						
	}
	
	private boolean cambioDatosAuto(CotizacionDTO cotizacion, List<IncisoCotizacionDTO> incisos, Long idNegocio, Long idPaquete, 
			Integer estilo, String modelo, Integer marca, String idEstadoCirculacion, String idMunicipioCirculacion, Double descuento,
			String codigoPostalCirculacion) {
		final int claveEstiloLength = 5;
		boolean result = false;
		if(incisos!=null && incisos.get(0)!=null){
			IncisoCotizacionDTO incisoCotizacion = incisos.get(0);
			IncisoAutoCot incisoAutoCot = incisoCotizacion.getIncisoAutoCot();
			NegocioPaqueteSeccion negocioPaqueteSeccion = negocioPaqueteSeccionDao.findById(incisoAutoCot.getNegocioPaqueteId());
			Long oldIdPaquete = negocioPaqueteSeccion.getPaquete().getId(); 
			Long oldIdNegocio = negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio(); 
			String oldEstilo = incisoAutoCot.getDescripcionFinal().substring(0, claveEstiloLength);
			Short oldModelo = incisoAutoCot.getModeloVehiculo();
			BigDecimal oldMarca = incisoAutoCot.getMarcaId();
			String oldIdEstadoCirculacion = incisoAutoCot.getEstadoId();
			String oldIdMunicipioCirculacion = incisoAutoCot.getMunicipioId();
			Double oldDescuento = (cotizacion.getDescuentoOriginacion() != null)? cotizacion.getDescuentoOriginacion() : 0.0;;
			Long oldCodigoPostalCirculacion = incisoAutoCot.getCodigoPostal();
			
			String newEstilo = StringUtils.leftPad(Integer.toString(estilo), claveEstiloLength,"0");
			
			if(idPaquete.intValue() != oldIdPaquete.intValue()){
				LOG.debug("idPaquete antes: "+oldIdPaquete+" idPaquete nuevo: "+idPaquete);
				result = true;
			}else if(idNegocio.intValue() != oldIdNegocio.intValue()){
				LOG.debug("idNegocio antes: "+oldIdNegocio+" idNegocio nuevo: "+idNegocio);
				result = true;
			}else if(!newEstilo.equals(oldEstilo)){
				LOG.debug("estilo antes: "+oldEstilo+" estilo nuevo: "+newEstilo);
				result = true;
			}else if(oldModelo.compareTo(Short.valueOf(modelo)) != 0){
				LOG.debug("modelo antes: "+oldModelo+" modelo nuevo: "+modelo);
				result = true;
			}else if(oldMarca.compareTo(BigDecimal.valueOf(marca)) != 0){
				LOG.debug("marca antes: "+oldMarca+" marca nuevo: "+marca);
				result = true;
			}else if(!oldIdEstadoCirculacion.equals(idEstadoCirculacion)){
				LOG.debug("idEstadoCirculacion antes: "+oldIdEstadoCirculacion+" idEstadoCirculacion nuevo: "+idEstadoCirculacion);
				result = true;
			}else if(!oldIdMunicipioCirculacion.equals(idMunicipioCirculacion)){
				LOG.debug("idMunicipioCirculacion antes: "+oldIdMunicipioCirculacion+" idMunicipioCirculacion nuevo: "+idMunicipioCirculacion);
				result = true;
			}else if(oldDescuento.compareTo(descuento) != 0){
				LOG.debug("descuento antes: "+oldDescuento+" descuento nuevo: "+descuento);
				result = true;
			}else if(oldCodigoPostalCirculacion.compareTo(Utilerias.obtenerLong(codigoPostalCirculacion)) != 0){
				LOG.debug("codigoPostalCirculacion antes: "+oldCodigoPostalCirculacion+" codigoPostalCirculacion nuevo: "+codigoPostalCirculacion);
				result = true;
			}
		}
		
		return result;
	}

	private CotizacionDTO actualizaDatosCotizacion (CotizacionDTO cotizacion, Agente agente, Negocio negocio, 
			NegocioTipoPoliza negocioTipoPoliza, CotizacionView dto, String claveUsuario) throws SystemException{
		SolicitudDTO solicitud = entidadService.findById(SolicitudDTO.class, cotizacion.getSolicitudDTO().getIdToSolicitud());

		Date inicioVigencia = new Date();		
		//Date finVigencia = DateUtils.add(inicioVigencia, Indicador.Days, diasAnio);
		//LPV Se agrega un anio a la poliza para no afectar en anios bisiestos
		Calendar fechaC = Calendar.getInstance();
		fechaC.setTime(inicioVigencia);		
		fechaC.add(Calendar.MONTH, 12);
		Date finVigencia = fechaC.getTime();
		
		//actualizar contratante
		if (dto.getContratante() != null && dto.getContratante().getIdContratante() == 0) {
			if (dto.getContratante().getCodigoPostal() != null && !dto.getContratante().getCodigoPostal().isEmpty() 
					&& !"0".equalsIgnoreCase(dto.getContratante().getCodigoPostal())){
				LOG.info("Antes de guardar el contratante -> cp:"+dto.getContratante().getCodigoPostal());
				this.saveClienteCot(cotizacion.getIdToCotizacion(), dto.getContratante());
			} else {
				throw new SystemException("El c\u00F3digo postal es requerido");
			}
		} else if (dto.getContratante() != null && dto.getContratante().getIdContratante() > 0) {
			cotizacion.setIdToPersonaContratante(new BigDecimal(dto.getContratante().getIdContratante()));
			cotizacionService.actualizarDatosContratanteCotizacion(cotizacion.getIdToCotizacion(), 
					new BigDecimal(dto.getContratante().getIdContratante()), null);
		} 
		
		cotizacion = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
		
		solicitud.setAgente(agente);		
		solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		solicitud.setNegocio(negocio);
		cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
		cotizacion.setFolio(dto.getFolio());
		
		NegocioProducto   negocioProducto = cotizacion.getNegocioTipoPoliza().getNegocioProducto();
		negocioProducto = entidadService.findById(NegocioProducto.class,
				cotizacion.getNegocioTipoPoliza().getNegocioProducto()
						.getIdToNegProducto());

		solicitud.setProductoDTO(negocioProducto.getProductoDTO());
		solicitud.setClaveEstatus(SolicitudDTO.Estatus.TERMINADA.getEstatus());
		solicitud.setFechaCreacion(Calendar.getInstance().getTime());
		solicitud.setClaveTipoSolicitud((short) 1);
		
		if (solicitud.getIdToPolizaAnterior() == null) {
			solicitud.setIdToPolizaAnterior(BigDecimal.ZERO);
		}
		
		if(agente != null){
			solicitud.setCodigoEjecutivo(new BigDecimal(agente.getPromotoria().getEjecutivo().getIdEjecutivo()));
			solicitud.setNombreAgente(agente.getPersona().getNombreCompleto());
			if(agente.getPromotoria().getEjecutivo() != null){
				Ejecutivo filtroEjecutivo =new Ejecutivo();
				filtroEjecutivo.setId(agente.getPromotoria().getEjecutivo().getId());
	            Ejecutivo ejecutivo = ejecutivoService.loadById(filtroEjecutivo);
	            if(ejecutivo != null){
					solicitud.setNombreEjecutivo(ejecutivo.getPersonaResponsable().getNombreCompleto());
					solicitud.setNombreOficina(ejecutivo.getGerencia().getPersonaResponsable().getNombreCompleto());
					solicitud.setIdOficina(new BigDecimal(ejecutivo.getGerencia().getId()));
	            }
			}					
			solicitud.setNombreOficinaAgente(agente.getPromotoria().getDescripcion());
		}
			
		solicitud.setCodigoAgente(solicitud.getCodigoAgente()==null?BigDecimal.ZERO:solicitud.getCodigoAgente());
        solicitud.setClaveOpcionEmision((short) 0);
        solicitud.setFechaCreacion(new Date());
        solicitud.setCodigoUsuarioCreacion(claveUsuario);
        solicitud.setFechaModificacion(new Date());
        solicitud.setCodigoUsuarioModificacion(claveUsuario);
        solicitud.setClaveTipoPersona((short) 1);
        solicitud.setNombrePersona(claveUsuario);
        
        //Actualiza vigencia de la cotizacion
		cotizacion.setFechaInicioVigencia(inicioVigencia);
		cotizacion.setFechaFinVigencia(finVigencia);
		cotizacion.setSolicitudDTO(solicitudFacadeRemote.update(solicitud));
	
		IncisoCotizacionDTO incisoCot = cotizacion.getIncisoCotizacionDTOs().get(0);
		IncisoAutoCot incisoAuto = incisoCot.getIncisoAutoCot();
		boolean actualizarAsegurado = false;
		boolean actualizarConductor = false;
		
		//actualizar conductor
		if (dto.getConductor() != null) {
			incisoAuto.setNombreConductor(dto.getConductor().getNombreConductor() != null ? dto.getConductor().getNombreConductor(): "");
			incisoAuto.setPaternoConductor(dto.getConductor().getApellidoPaternoConductor() != null ? dto.getConductor().getApellidoPaternoConductor(): "");
			incisoAuto.setMaternoConductor(dto.getConductor().getApellidoMaternoConductor() != null ? dto.getConductor().getApellidoMaternoConductor(): "");
			incisoAuto.setNumeroLicencia(dto.getConductor().getNumeroLicencia() != null ? dto.getConductor().getNumeroLicencia(): "");
			incisoAuto.setFechaNacConductor((dto.getConductor().getFechaNacimiento() != null && !dto.getConductor().getFechaNacimiento().isEmpty())? DateUtils.convertirStringToUtilDate(dto.getConductor().getFechaNacimiento(), "yyyy-MM-dd HH:mm:ss"): new Date());
			incisoAuto.setOcupacionConductor(dto.getConductor().getOcupacion() != null ? dto.getConductor().getOcupacion(): "");
			
			actualizarConductor = true;
		}
				
		//actualizar asegurado
		if (dto.getAsegurado() != null) {
			if(dto.getAsegurado().getNombreAsegurado() != null && !StringUtils.isEmpty(dto.getAsegurado().getNombreAsegurado())) {
				incisoAuto.setPersonaAseguradoId(dto.getAsegurado().getClienteAseguradoId() != null? dto.getAsegurado().getClienteAseguradoId(): 0L);
				incisoAuto.setNombreAsegurado(dto.getAsegurado().getNombreAsegurado());
				
				actualizarAsegurado = true;
			}
			
			cotizacion.setNombreAsegurado(dto.getAsegurado().getNombreAsegurado() != null ? dto.getAsegurado().getNombreAsegurado(): "");
		}

		if (actualizarAsegurado || actualizarConductor) {
			try {
				incisoService.guardarIncisoAutoCot(incisoAuto);
			} catch(Exception e){
				throw new SystemException("Error al guardar el Inciso Auto Cot. ", e);
			}
		}
		
		return	cotizacionFacadeRemote.update(cotizacion);
	}
	
	/**
	 * Guarda cliente y lo relaciona a la cotizacion
	 * @param idToCotizacion
	 * @param nombreAsegurado
	 * @param apellidoPaterno
	 * @param apellidoMaterno
	 * @param rfc
	 * @param claveSexo
	 * @param claveEstadoCivil
	 * @param idEstadoNacimiento
	 * @param fechaNacimiento
	 * @param codigoPostal
	 * @param idCiudad
	 * @param estado
	 * @param colonia
	 * @param calleNumero
	 * @param telefonoCasa
	 * @param telefonoOficina
	 * @param email
	 * @throws SystemException 
	 * @throws Exception 
	 */
	private void saveClienteCot (BigDecimal idToCotizacion, ContratanteView dto) throws SystemException {
		
		short claveTipoPersona = 1;
		ClienteGenericoDTO cliente = new ClienteGenericoDTO();
		
		if (dto.getRfc() != null && UtileriasWeb.validarRfcPersonaFisica(dto.getRfc())) {
			LOG.info("Cargando datos del Cliente que es persona f\u00EDsica...");
			cliente.setApellidoPaterno(dto.getApellidoPaterno() != null ? dto.getApellidoPaterno() : "");
			cliente.setApellidoMaterno(dto.getApellidoMaterno() != null ? dto.getApellidoMaterno() : "");
			cliente.setNombre(dto.getNombreContratante() != null ? dto.getNombreContratante() : "");
			cliente.setNombreFiscal(cliente.getNombre());
			cliente.setApellidoPaternoFiscal(cliente.getApellidoPaterno());
			cliente.setApellidoMaternoFiscal(cliente.getApellidoMaterno());
			//cliente.setIdColonia(dto.getIdColonia() != 0 ? new BigDecimal(dto.getIdColonia()): BigDecimal.ZERO);
			cliente.setIdColoniaFiscal(dto.getClaveColonia() != null ? dto.getClaveColonia(): "");
			cliente.setIdColoniaCobranza(dto.getClaveColonia() != null ? dto.getClaveColonia(): "");
			cliente.setNombreColonia(dto.getNombreColonia() != null ? dto.getNombreColonia() : "");
			cliente.setNombreColoniaFiscal(cliente.getNombreColonia());
			cliente.setNombreColoniaCobranza(cliente.getNombreColonia());
			cliente.setCodigoRFC(dto.getRfc() != null ? dto.getRfc() : "");
			cliente.setClaveNacionalidad("PAMEXI");
			cliente.setTipoSituacionString("A");
			cliente.setEstadoCivil(dto.getClaveEstadoCivil() + "");
			cliente.setEstadoNacimiento(dto.getIdEstadoNacimiento() != null? dto.getIdEstadoNacimiento(): "");		
			cliente.setIdPaisString("PAMEXI");
			cliente.setIdEstadoString(dto.getIdEstado() != null ? dto.getIdEstado() : "");
			cliente.setIdMunicipioString(dto.getIdCiudad() != null ? dto.getIdCiudad() : "");
			cliente.setCodigoPostal(dto.getCodigoPostal() != null ? String.format("%05d", Integer.parseInt(dto.getCodigoPostal())) : "");
			cliente.setNombreCalle(dto.getCalle() != null ? dto.getCalle() + " " + (dto.getNumero() != null? dto.getNumero(): "") : "");
			cliente.setNombreCalleCobranza(cliente.getNombreCalle() != null ? cliente.getNombreCalle() : "");
			cliente.setNombreCalleFiscal(cliente.getNombreCalle() != null ? cliente.getNombreCalle() : "");
			cliente.setIdEstadoCobranza(cliente.getIdEstadoString());
			cliente.setIdEstadoFiscal(cliente.getIdEstadoString());
			cliente.setIdMunicipioCobranza(cliente.getIdMunicipioString());
			cliente.setIdMunicipioFiscal(cliente.getIdMunicipioString());
			cliente.setCodigoPostalCobranza(cliente.getCodigoPostal());
			cliente.setCodigoPostalFiscal(cliente.getCodigoPostal());
			cliente.setClaveTipoPersona((short) claveTipoPersona);
			cliente.setEmail(dto.getEmail() != null ? dto.getEmail() : "");
			cliente.setEmailCobranza(cliente.getEmail());
			cliente.setNumeroTelefono(dto.getTelefonoCasa() != null ? dto.getTelefonoCasa(): "");
			cliente.setTelefono(dto.getTelefonoCasa() != null ? dto.getTelefonoCasa(): "");
			cliente.setTelefonoOficinaContacto(dto.getTelefonoOficina() != null ? dto.getTelefonoOficina(): "");
			cliente.setSexo(dto.getClaveSexo() + "");
			cliente.setFechaNacimiento(dto.getFechaNacimiento() != null ? DateUtils.convertirStringToUtilDate(dto.getFechaNacimiento(), "yyyy-MM-dd HH:mm:ss"): new Date());
		} else if (dto.getRfc() != null && UtileriasWeb.validarRfcPersonaMoral(dto.getRfc())) {
			LOG.info("Cargando datos del Cliente que es persona Moral...");
			claveTipoPersona = 2;
			cliente.setRazonSocial(dto.getNombreRazonSocial() != null ? dto.getNombreRazonSocial(): "");
			cliente.setRazonSocialFiscal(dto.getNombreRazonSocial() != null ? dto.getNombreRazonSocial(): "");
			cliente.setFechaConstitucion(dto.getFechaDeConstitucion() != null ? dto.getFechaDeConstitucion(): ( (dto.getFechaConstitucion() != null && !dto.getFechaConstitucion().equals(""))? DateUtils.convertirStringToUtilDate(dto.getFechaConstitucion(), "yyyy-MM-dd HH:mm:ss"): new Date() ));
			
			cliente.setCodigoRFC(dto.getRfc());
			cliente.setCodigoRFCFiscal(dto.getRfc());
			cliente.setCodigoRFCCobranza(dto.getRfc());
			
			//domicilio fiscal
			cliente.setNombreCalle(dto.getCalle() != null ? dto.getCalle() + " " + (dto.getNumero() != null? dto.getNumero(): "") : "");
			cliente.setNombreCalleCobranza(cliente.getNombreCalle() != null ? cliente.getNombreCalle() : "");
			cliente.setNombreCalleFiscal(cliente.getNombreCalle() != null ? cliente.getNombreCalle() : "");
			
			cliente.setCodigoPostal(dto.getCodigoPostal() != null ? String.format("%05d", Integer.parseInt(dto.getCodigoPostal())) : "");
			cliente.setCodigoPostalCobranza(cliente.getCodigoPostal());
			cliente.setCodigoPostalFiscal(cliente.getCodigoPostal());
			
			cliente.setNombreColonia(dto.getNombreColonia() != null ? dto.getNombreColonia() : "");
			cliente.setNombreColoniaFiscal(cliente.getNombreColonia());
			cliente.setNombreColoniaCobranza(cliente.getNombreColonia());
			
			cliente.setEstadoNacimiento(dto.getIdEstadoNacimiento() != null? dto.getIdEstadoNacimiento(): "");
			cliente.setIdEstadoNacimiento(new BigDecimal(cliente.getEstadoNacimiento()));
			cliente.setIdEstadoNacimientoString(cliente.getEstadoNacimiento());
			cliente.setIdEstadoCobranza(cliente.getIdEstadoNacimientoString());
			cliente.setIdEstadoFiscal(cliente.getIdEstadoNacimientoString());
			
			cliente.setIdMunicipio(dto.getIdMunicipioNacimiento() != null? new BigDecimal(dto.getIdMunicipioNacimiento()): BigDecimal.ZERO);
			cliente.setIdMunicipioString(cliente.getIdMunicipio()+"");
			cliente.setIdMunicipioCobranza(cliente.getIdMunicipioString());
			cliente.setIdMunicipioFiscal(cliente.getIdMunicipioString());
			
			cliente.setClaveTipoPersona((short) claveTipoPersona);
			cliente.setClaveNacionalidad("PAMEXI");
			cliente.setIdPaisString("PAMEXI");
			cliente.setTipoSituacionString("A");
		} else {
			LOG.error("El formato del RFC "+dto.getRfc()+" es inv\u00E1lido");
			throw new SystemException("El formato del RFC "+dto.getRfc()+" es inv\u00E1lido");
		}
		
		try {
			LOG.info("Guardando datos del Cliente...");
			cliente = clienteFacade.saveFullData(cliente, null, false);
		} catch (ClientSystemException e) {
			LOG.debug("Ocurri\u00F3 un error al guardar cliente...");
			LOG.error("Error guardando datos del Cliente... "+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al guardar el cliente... "+e.getMessage(), e);
		} catch (Exception e) {
			LOG.debug("Ocurrio un error al guardar cliente");
			LOG.error("Error guardando datos del Cliente: "+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al guardar el cliente ", e);
		}
		
		if (cliente != null) {
			//asociamos el cliente con la cotizacion
			LOG.debug("idCliente => " + cliente.getIdClienteString());
			cotizacionService.actualizarDatosContratanteCotizacion(	idToCotizacion, cliente.getIdCliente(),	null);		
		}
	}
	
	@Override
	public List<Map<String, Object>> buscarCliente(ClienteGenericoDTO filtro, String token) throws Exception {
		validateToken(token);
		List<Map<String, Object>> result = new ArrayList<Map<String, Object>>(1);
		if (filtro.getIdNegocio() == null) {
			final List<ClienteGenericoDTO> lista = clienteFacade.findByFilters(filtro, null);
			for(ClienteGenericoDTO cliente : lista) {
				final Map<String, Object> map = new HashMap<String, Object>();
				map.put("idCliente", cliente.getIdCliente());
				map.put("codigoRFC", cliente.getCodigoRFC());
				if( cliente.getClaveTipoPersona() == ClienteFacade.CLAVE_PERSONA_FISICA) {
					map.put("nombre", cliente.getNombre() + " " + cliente.getApellidoPaterno() + " " + cliente.getApellidoMaterno());
				} else if (cliente.getClaveTipoPersona() == ClienteFacade.CLAVE_PERSONA_MORAL) {
					map.put("nombre", cliente.getRazonSocial());
				}

				result.add(map);
			}
		} else {
			List<NegocioCliente> listNegocioCliente = negocioService.listarClientesAsociados(filtro.getIdNegocio());
			for(NegocioCliente negocioCliente : listNegocioCliente) {
				ClienteDTO cliente = negocioCliente.getClienteDTO();
				final Map<String, Object> map = new HashMap<String, Object>(1);
				map.put("idCliente", cliente.getIdCliente());
				map.put("codigoRFC", cliente.getCodigoRFC());
				map.put("nombre", cliente.getNombre());
				result.add(map);
			}
		}
		return result;
	}
	
	
	/**
	 * Guarda inciso a cotizacion segun sean las caracteristicas del vehiculo
	 * @param cotizacion
	 * @param idPaquete
	 * @param estilo
	 * @param modelo
	 * @param marca
	 * @param idEstado
	 * @param idMunicipio
	 * @param valorFactura
	 * @return InciscoCotizacionDTO
	 * @throws SystemException 
	 */
	private IncisoCotizacionDTO guardaInciso(CotizacionDTO cotizacion, Negocio negocio, CotizacionView dto, NegocioTipoPoliza negocioTipoPoliza,
			NegocioSeccion negocioSeccion) throws SystemException{
		final int claveEstiloLength = 5;
		LOG.info(this.getClass().getName()+".guardaInciso ... entrando a guardar inciso");
		LOG.info("idToCotizacion => " + cotizacion.getIdToCotizacion());
		LOG.info("folio => " + cotizacion.getFolio());
		LOG.info("idNegocio => " + negocio.getIdToNegocio());
		LOG.info("marca => " + dto.getVehiculo().getIdMarca());
		LOG.info("estilo => " + dto.getVehiculo().getIdEstilo());
		LOG.info("modelo => " + dto.getVehiculo().getModelo());
		LOG.info("idEstadoCirculacion => " + dto.getZonaCirculacion().getIdEstadoCirculacion());
		LOG.info("idMunicipioCirculacion => " + dto.getZonaCirculacion().getIdMunicipioCirculacion());
		LOG.info("idNegocioTipoPoliza => "+negocioTipoPoliza.getIdToNegTipoPoliza());
		LOG.info("idSeccion => "+negocioSeccion.getSeccionDTO().getIdToSeccion());
		LOG.info("descuento => "+dto.getPaquete().getPctDescuentoEstado());
		LOG.info("codigo postal circulacion => "+dto.getZonaCirculacion().getCodigoPostalCirculacion());
		
		String claveEstilo = StringUtils.leftPad(dto.getVehiculo().getIdEstilo()+"", claveEstiloLength,"0");
		SeccionDTO seccion = new SeccionDTO();
		List<EstiloVehiculoDTO> estiloVehiculoList = estiloVehiculoFacade.findByProperty("id.claveEstilo", claveEstilo);
		Long tipoUso = 9L;
		
		switch (estiloVehiculoList.get(0).getIdTcTipoVehiculo().intValue()) {
		case 1:
			tipoUso = 9L;
			break;
		case 2:
			tipoUso = 19L;
			break;
		case 3:
			tipoUso = 22L;
			break;
		case 11:
			tipoUso = 46L;
			break;
		default:
			break;
		}
		// Si en el case de arriba se calculo un tipo de uso que no corresponde
		// con los del negocio se toma el primero del negocio como el valido
		final Map<BigDecimal, String> mapaTiposUso = listadoService.getMapTipoUsoVehiculoByNegocio(
				negocioSeccion.getIdToNegSeccion());
		if (!mapaTiposUso.containsKey(BigDecimal.valueOf(tipoUso))) {
			tipoUso = mapaTiposUso.keySet().iterator().next().longValue();
		}		
		LOG.info("tipoUso => "+tipoUso);
		
		//Validar la sección con el tipo de negocio		
		if (!negocioSeccion.getNegocioTipoPoliza().getIdToNegTipoPoliza().equals(negocioTipoPoliza.getIdToNegTipoPoliza())){
			throw new SystemException("La sección "+negocioSeccion.getSeccionDTO().getIdToSeccion()+" proporcionada no corresponde con el negocioTipoPoliza "+negocioTipoPoliza.getIdToNegTipoPoliza()+" dado");
		} else{
			seccion = negocioSeccion.getSeccionDTO();
		}
		
		LOG.info("lineaNegocio => " + seccion.getDescripcion());

		IncisoCotizacionId id = new IncisoCotizacionId();
		id.setIdToCotizacion(cotizacion.getIdToCotizacion());

		cotizacionFacadeRemote.update(cotizacion);
	
		NegocioPaqueteSeccion negocioPaqueteSeccion = null;
		
		try{
			negocioPaqueteSeccion = negocioPaqueteSeccionDao
			.getPorIdNegSeccionIdPaquete(negocioSeccion.getIdToNegSeccion(), dto.getPaquete().getIdPaquete() != null ? dto.getPaquete().getIdPaquete() : Paquete.AMPLIA);
		} catch(NoResultException e){
			throw new SystemException("No se pudo encontrar el negocioPaqueteSeccion con el idNegocioSección y el paquete dado");
		} catch (RuntimeException re) {
			throw new SystemException("No se pudo encontrar el negocioPaqueteSeccion con el idNegocioSección y el paquete dado");
		}
		
		NegocioAgrupadorTarifaSeccion negAgrupadorTarifaSeccion = negAgrupadorTarifaSeccionDao.buscarPorNegocioSeccionYMoneda(negocioSeccion, new BigDecimal(484));
		TarifaAgrupadorTarifa tarifaAgrupador = tarifaAgrupadorService.getPorNegocioAgrupadorTarifaSeccion(negAgrupadorTarifaSeccion);
		
		IncisoCotizacionDTO incisoCotizacion;
		IncisoAutoCot incisoAuto;
		if(cotizacion.getIncisoCotizacionDTOs()!=null && cotizacion.getIncisoCotizacionDTOs().size() > 0){
			incisoCotizacion = cotizacion.getIncisoCotizacionDTOs().get(0);
			
			if(incisoCotizacion.getSeccionCotizacion().getSeccionDTO().getIdToSeccion().longValue() != seccion.getIdToSeccion().longValue()){
				incisoService.borrarInciso(incisoCotizacion);
				incisoCotizacion.getId().setNumeroInciso(null);
				incisoAuto =  new IncisoAutoCot();
				incisoCotizacion = new IncisoCotizacionDTO();
				incisoCotizacion.setId(id);
				incisoCotizacion.setCotizacionDTO(cotizacion);
				incisoCotizacion.setIdToSeccion(seccion.getIdToSeccion());
			}else{
				incisoAuto = incisoCotizacion.getIncisoAutoCot();
			}
			
		}
		else{			
			incisoCotizacion = new IncisoCotizacionDTO();
			incisoCotizacion.setId(id);
			incisoCotizacion.setCotizacionDTO(cotizacion);
			incisoCotizacion.setIdToSeccion(seccion.getIdToSeccion());
			
			incisoAuto = new IncisoAutoCot();
		}
		

		EstiloVehiculoId estiloVehiculoId = new EstiloVehiculoId();
		estiloVehiculoId.setClaveEstilo(claveEstilo);
		
		MarcaVehiculoDTO marcaVehiculo = marcaVehiculoFacade.findById(dto.getVehiculo().getIdMarca());
		
		if (marcaVehiculo != null) {
			estiloVehiculoId.setClaveTipoBien(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien());
		} else {
			throw new SystemException("No fue posible encontrar la marca "+dto.getVehiculo().getIdMarca()+" dada");
		}
		
		estiloVehiculoId.setIdVersionCarga( new BigDecimal(tarifaAgrupador.getId().getIdVertarifa()));
		
		EstiloVehiculoDTO estiloVehiculo = estiloVehiculoFacade.findById(estiloVehiculoId);
		
		if (estiloVehiculo == null){
			throw new SystemException("No fue posible encontrar el estilo "+claveEstilo+" dado");
		}
		
		ModeloVehiculoId modeloVehiculoId = new ModeloVehiculoId();
		modeloVehiculoId.setClaveEstilo(claveEstilo);
		modeloVehiculoId.setClaveTipoBien(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien());
		modeloVehiculoId.setIdMoneda(cotizacion.getIdMoneda());
		modeloVehiculoId.setIdVersionCarga(new BigDecimal(tarifaAgrupador.getId().getIdVertarifa()));
		modeloVehiculoId.setModeloVehiculo(new Short(dto.getVehiculo().getModelo()+""));
		ModeloVehiculoDTO modeloVehiculo = modeloVehiculoFacade.findById(modeloVehiculoId);		
		if (modeloVehiculo == null){
			throw new SystemException("No fue posible encontrar el modelo "+dto.getVehiculo().getModelo()+" dado que corresponda a la clave "+claveEstilo+" y claveTipoBien: "+modeloVehiculoId.getClaveTipoBien());
		}	
		
		//nombre conductor
		incisoAuto.setNombreConductor((dto.getConductor() != null && !StringUtils.isBlank(dto.getConductor().getNombreConductor())) ? dto.getConductor().getNombreConductor(): "");
		incisoAuto.setPaternoConductor((dto.getConductor() != null && !StringUtils.isBlank(dto.getConductor().getApellidoPaternoConductor())) ? dto.getConductor().getApellidoPaternoConductor(): "");
		incisoAuto.setMaternoConductor((dto.getConductor() != null && !StringUtils.isBlank(dto.getConductor().getApellidoMaternoConductor())) ? dto.getConductor().getApellidoMaternoConductor(): "");
		
		incisoAuto.setFechaNacConductor((dto.getConductor() != null && !StringUtils.isBlank(dto.getConductor().getFechaNacimiento()))? DateUtils.convertirStringToUtilDate(dto.getConductor().getFechaNacimiento(), "yyyy-MM-dd HH:mm:ss"): Utilerias.obtenerFechaDeCadena("01/01/1990"));
		incisoAuto.setNumeroLicencia((dto.getConductor() != null && !StringUtils.isBlank(dto.getConductor().getNumeroLicencia()))? dto.getConductor().getNumeroLicencia(): "");
		incisoAuto.setOcupacionConductor((dto.getConductor() != null && !StringUtils.isBlank(dto.getConductor().getOcupacion()))? dto.getConductor().getOcupacion(): "");
		
		incisoAuto.setNombreAsegurado((dto.getAsegurado() != null && !StringUtils.isEmpty(dto.getAsegurado().getNombreAsegurado()))? dto.getAsegurado().getNombreAsegurado(): ".");
		
		incisoAuto.setEstadoId(dto.getZonaCirculacion().getIdEstadoCirculacion());
		incisoAuto.setMunicipioId(dto.getZonaCirculacion().getIdMunicipioCirculacion());
		incisoAuto.setMarcaId(marcaVehiculo.getIdTcMarcaVehiculo());		
		incisoAuto.setDescripcionFinal(claveEstilo.concat(" - ").concat(estiloVehiculo.getDescripcionEstilo()));
		incisoAuto.setEstiloId(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien().concat("_").concat(claveEstilo).concat("_").concat(tarifaAgrupador.getId().getIdVertarifa().toString()));
		incisoAuto.setModeloVehiculo(modeloVehiculo.getModeloVehiculo());	
		incisoAuto.setNegocioSeccionId(negocioSeccion.getIdToNegSeccion().longValue());
		incisoAuto.setClaveTipoBien(marcaVehiculo.getTipoBienAutosDTO().getClaveTipoBien());
		incisoAuto.setNegocioPaqueteId(negocioPaqueteSeccion.getIdToNegPaqueteSeccion());
		
		// Tipo de Uso Normal
		incisoAuto.setTipoUsoId(tipoUso);
		incisoAuto.setAsociadaCotizacion(Integer.valueOf(1));
		incisoAuto.setObservacionesinciso(dto.getPaquete().getObservaciones());
		validaDescuento(dto.getPaquete().getPctDescuentoEstado(), cotizacion.getIdToCotizacion(), negocio.getIdToNegocio(), dto.getZonaCirculacion().getIdEstadoCirculacion());
		incisoAuto.setPctDescuentoEstado(dto.getPaquete().getPctDescuentoEstado());
		incisoAuto.setCodigoPostal(Utilerias.obtenerLong(dto.getZonaCirculacion().getCodigoPostalCirculacion()));		
		incisoCotizacion.setIncisoAutoCot(incisoAuto);		
		
		estiloVehiculoId.valueOf(incisoAuto.getEstiloId());
		
		Long numeroSecuencia = 1l;

		LOG.info("idToNegocio:" + negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getNegocio().getIdToNegocio()+" idNegocio:"+negocio.getIdToNegocio());
		LOG.info("idToProducto:" + negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto());
		LOG.info("idToTipoPoliza:" + negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza());
		LOG.info("idToSeccion:" + negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion()+" idSeccion:"+negocioSeccion.getSeccionDTO().getIdToSeccion());
		LOG.info("paqueteId:" + negocioPaqueteSeccion.getPaquete().getId());
		LOG.info("estadoId:" + incisoCotizacion.getIncisoAutoCot().getEstadoId());
		LOG.info("MunicipioId:" + incisoCotizacion.getIncisoAutoCot().getMunicipioId());
		LOG.info("incisoCotizacion.getId().getIdToCotizacion():" + incisoCotizacion.getId().getIdToCotizacion());
		LOG.info("idMoneda:" + incisoCotizacion.getCotizacionDTO().getIdMoneda().shortValue());
		LOG.info("numeroSecuencia:" + numeroSecuencia);
		LOG.info("claveEstilo:" + estiloVehiculoId.getClaveEstilo());
		LOG.info("modeloVehículo:" + incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue());
		LOG.info("tipoUsoId:" + incisoCotizacion.getIncisoAutoCot().getTipoUsoId());
		LOG.info("codigoPostal circulacion:" + incisoCotizacion.getIncisoAutoCot().getCodigoPostal());
		
		List<CoberturaCotizacionDTO> coberturaCotizacionListProceso = new ArrayList<CoberturaCotizacionDTO>();
		
		List<CoberturaCotizacionDTO> coberturaCotizacionList = 
			coberturaService.getCoberturas(negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto()
						.getNegocio().getIdToNegocio(), negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getNegocioProducto().getProductoDTO().getIdToProducto(),
						negocioPaqueteSeccion.getNegocioSeccion().getNegocioTipoPoliza().getTipoPolizaDTO().getIdToTipoPoliza(),negocioPaqueteSeccion.getNegocioSeccion().getSeccionDTO().getIdToSeccion(),
						negocioPaqueteSeccion.getPaquete().getId(),incisoCotizacion.getIncisoAutoCot().getEstadoId(),incisoCotizacion.getIncisoAutoCot().getMunicipioId(),incisoCotizacion.getId().getIdToCotizacion(),
						incisoCotizacion.getCotizacionDTO().getIdMoneda().shortValue(), numeroSecuencia,estiloVehiculoId.getClaveEstilo(), 
						incisoCotizacion.getIncisoAutoCot().getModeloVehiculo().longValue(), new BigDecimal(incisoCotizacion
								.getIncisoAutoCot().getTipoUsoId()), null, null);
		
		for (CoberturaCotizacionDTO cobertura : coberturaCotizacionList) {
			cobertura.setValorPrimaNeta(cobertura.getValorPrimaNeta()==null?0D:cobertura.getValorPrimaNeta());				
			cobertura.setValorSumaAsegurada(cobertura.getValorSumaAsegurada()==null?0D:cobertura.getValorSumaAsegurada());
			cobertura.setValorCoaseguro(cobertura.getValorCoaseguro()==null?0D:cobertura.getValorCoaseguro());
			cobertura.setValorDeducible(cobertura.getValorDeducible()==null?0D:cobertura.getValorDeducible());
			cobertura.setValorCuota(cobertura.getValorCuota()==null?0D:cobertura.getValorCuota());
			cobertura.setValorCuotaOriginal(cobertura.getValorCuotaOriginal()==null?0D:cobertura.getValorCuotaOriginal());
			cobertura.setValorCuotaOriginal(cobertura.getValorCuotaOriginal()==null?0D:cobertura.getValorCuotaOriginal());
			cobertura.setPorcentajeCoaseguro(cobertura.getPorcentajeCoaseguro()==null?0D:cobertura.getPorcentajeCoaseguro());
			cobertura.setPorcentajeDeducible(cobertura.getPorcentajeDeducible()==null?0D:cobertura.getPorcentajeDeducible());
			
			if (cobertura.getClaveAutCoaseguro().intValue() != 0){
				cobertura.setClaveAutCoaseguro((short)1);
				cobertura.setClaveAutDeducible((short)1);
			} else{
				cobertura.setClaveAutCoaseguro((short)0);
				cobertura.setClaveAutDeducible((short)0);
			}
			
			if (cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES_VALOR_CONVENIDO) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_ROBO_TOTAL_VALOR_CONVENIDO) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_ROBO_TOTAL) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES_VF) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_ROBO_VF) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_DANIOS_MATERIALES_CAMIONES) == 0
				|| cobertura.getId().getIdToCobertura().compareTo(CoberturaDTO.IDTOCOBERTURA_ROBO_CAMIONES) == 0
				) {
				
				cobertura.setValorSumaAsegurada(0D);
			}
			coberturaCotizacionListProceso.add(cobertura);
		}
		
		LOG.info("-> idTcTipoVehiculo:" + estiloVehiculoList.get(0).getIdTcTipoVehiculo().intValue());
		LOG.info("-> coberturaCotizacionList.size:" + coberturaCotizacionList.size());
		
		if (CollectionUtils.isNotEmpty(dto.getCoberturas())) {
			coberturaCotizacionListProceso = this.validarCoberturas(coberturaCotizacionListProceso, dto.getCoberturas());	
		}
		
		if(estiloVehiculoList.get(0).getIdTcTipoVehiculo().intValue() != 1){
			incisoCotizacion = this.datosAdicionalesPaquete(coberturaCotizacionListProceso,cotizacion, incisoCotizacion);
		}else{
			incisoCotizacion = incisoService.prepareGuardarIncisoAgente(incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getIncisoAutoCot(), 
					incisoCotizacion, coberturaCotizacionListProceso, null, null, null, null);
		}
		
		return incisoCotizacion;
	}
	
	private void validaDescuento(Double descuento, BigDecimal idToCotizacion, Long idToNegocio, String stateId) throws SystemException {
		Double descuentoMaximo = listadoService.getPcteDescuentoMaximoPorEstado(idToCotizacion, idToNegocio, stateId);
		if(descuento > descuentoMaximo) {
			throw new SystemException("El porcentaje de descuento m\u00E1ximo permitido es de " + descuentoMaximo);
		}
		
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	private IncisoCotizacionDTO  datosAdicionalesPaquete(List<CoberturaCotizacionDTO> coberturaCotizacionList,CotizacionDTO cotizacion, IncisoCotizacionDTO incisoCotizacion){
		LOG.info("Entrando a método datosAdicionalesPaquete");
		List<DatoIncisoCotAuto> datoIncisoCotAutos = new ArrayList<DatoIncisoCotAuto>();
	
		CoberturaCotizacionDTO responsabilidadCivilCoberturaCotizacion = cargaMasivaService
		.getCoberturaCotizacion(coberturaCotizacionList, CoberturaDTO.RESPONSABILIDAD_CIVIL_AUTOS_NOMBRE_COMERCIAL);	
		
		if (responsabilidadCivilCoberturaCotizacion != null && responsabilidadCivilCoberturaCotizacion.getClaveContratoBoolean()) {
				DatoIncisoCotAuto datoIncisoCotAutoNumeroRemolques = cargaMasivaService.obtieneDatoIncisoCotAutoSubRamo(cotizacion,
						responsabilidadCivilCoberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(), 
						ConfiguracionDatoInciso.DescripcionEtiqueta.NUMERO_REMOLQUES, String.valueOf(0));
				datoIncisoCotAutos.add(datoIncisoCotAutoNumeroRemolques);
		}
		LOG.info("--> coberturaCotizacionList.size:"+coberturaCotizacionList.size());
		CoberturaCotizacionDTO danosOcasionadosPorCargaCoberturaCotizacion = cargaMasivaService
		.getCoberturaCotizacion(coberturaCotizacionList, CoberturaDTO.DANOS_OCASIONADOS_CARGA_NOMBRE_COMERCIAL);
		
		if (danosOcasionadosPorCargaCoberturaCotizacion != null){
			LOG.info("--> antes de llamar a cargaMasivaService.obtieneDatoIncisoCotAutoCobertura ");
			DatoIncisoCotAuto datoIncisoCotAutoTipoCarga = cargaMasivaService.obtieneDatoIncisoCotAutoCobertura(cotizacion,
					danosOcasionadosPorCargaCoberturaCotizacion, incisoCotizacion.getId().getNumeroInciso(),
					ConfiguracionDatoInciso.DescripcionEtiqueta.TIPO_CARGA, String.valueOf(10));
			
			datoIncisoCotAutos.add(datoIncisoCotAutoTipoCarga);
		}
		
		IncisoCotizacionDTO inciso = cargaMasivaService.guardaIncisoCotizacion(incisoCotizacion, 
				coberturaCotizacionList, datoIncisoCotAutos);
		
		return inciso;
	}

	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
    public  Map<String, String> emitir(String numeroSerie, String numeroMotor, String numeroPlaca, int idFormaPago, 
    		BigDecimal idCotizacion, Short medioPago, String tipoTarjeta, int idBanco, 
			String fechaVencimientoTarjeta, String codigoSeguridad, String tarjetaHabiente, String cuenta, 
			String correo, String telefono, String clavePais, String claveEstado, String claveCiudad, String idColonia, 
			String nombreColonia, String calleNumero, String codigoPostal, String rfc, String nombreAsegurado, String token) throws SystemException{
    	LOG.info(this.getClass().getName()+".emitir ... entrando a emitir p\u00F3liza");
    	
    	validateToken( token );
    	
    	if (medioPago == null || medioPago == 0){
    		throw new SystemException("El medio de pago es requerido");
    	}
    	
    	LOG.info("idCotizacion => " + idCotizacion);
    	
    	CotizacionDTO cotizacion = null;
    	
    	if (idCotizacion != null){
			cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
    	} else{
			throw new SystemException("Favor de especificar el id de cotizaci\u00F3n");
    	}
    	
    	/* Primero se ejecuta el proceso de guardar cobranza */
    	LOG.info("idCotizacion encontrada => " + cotizacion.getIdToCotizacion());
    	
    	//Si el medioPago es distinto de Efectivo, se guardan los datos de cobranza y se crea el conducto de cobro. 
		if (medioPago != MEDIO_PAGO_EFECTIVO){
			ConductoCobroDTO conductoCobro = poblarConductoCobro(medioPago, tipoTarjeta, idBanco, fechaVencimientoTarjeta, 
					codigoSeguridad, tarjetaHabiente, cuenta, correo, telefono, clavePais, claveEstado, claveCiudad, 
					idColonia, nombreColonia, calleNumero, codigoPostal, rfc);
			try {
				guardarCobranza(idCotizacion, conductoCobro, false, token);
				cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
			} catch (RuntimeException e) {
				throw new SystemException("Favor de validar : "+e.getMessage());
			} catch (SystemException e) {
				throw new SystemException(e.getMessage());
			} catch (Exception e) {
				LOG.error("Ocurri\u00F3 un error al guardar los datos de cobranza de la cotizaci\u00F3n " + cotizacion.getIdToCotizacion()+ e.getMessage());
				throw new SystemException("Ocurri\u00F3 un error al guardar los datos de cobranza. "+e.getMessage());
			}
		}
		/* FIN del proceso de guardarCobrana */
		
    	LOG.info("folio => " + cotizacion.getFolio());
    	LOG.info("numeroSerie => " + numeroSerie);
    	LOG.info("numeroMotor => " + numeroMotor);
    	LOG.info("tipoCobro(Medio Pago) => " + medioPago);
    	LOG.info("formaPago => " + idFormaPago);
    	
        Map<String, String> result = new HashMap<String, String>(1);
        
        if(cotizacion.getIdToPersonaContratante() == null){
			throw new SystemException("Contratante no guardado en cotizaci\u00F3n, Favor de agregar un contratante a la cotizaci\u00F3n");
		}
		
		IncisoCotizacionDTO incisoCot = cotizacion.getIncisoCotizacionDTOs().get(0);
		
		IncisoAutoCot incisoAuto = incisoCot.getIncisoAutoCot();
		incisoAuto.setNumeroMotor(StringUtils.isBlank(numeroMotor) ? "S/N" : numeroMotor);
		incisoAuto.setNumeroSerie(numeroSerie);
		incisoAuto.setPlaca(StringUtils.isBlank(numeroPlaca) ? "PEND" : numeroPlaca);	

		
		if(StringUtils.isEmpty(nombreAsegurado)) {
			incisoAuto.setPersonaAseguradoId(cotizacion.getIdToPersonaContratante().longValue());
			incisoAuto.setNombreAsegurado(cotizacion.getNombreContratante());
		} else {
			incisoAuto.setNombreAsegurado(nombreAsegurado);
		}

		try {
			incisoService.guardarIncisoAutoCot(incisoAuto);
		} catch(Exception e){
			throw new SystemException("Error al guardar el Inciso Auto Cot. ", e);
		}

		if (medioPago != MEDIO_PAGO_EFECTIVO && medioPago != MEDIO_PAGO_TC && medioPago != MEDIO_PAGO_DOMICILIACION && medioPago != CuentaPagoDTO.TipoCobro.CUENTA_AFIRME.valor()) {
			throw new SystemException("Favor de especificar un medio de pago v\u00E1lido");
		}
		
		cotizacion.setIdMedioPago(BigDecimal.valueOf(medioPago));
		
		//Valida forma de pago
		if (idFormaPago > 0) {
			final Map<Integer,String> mapFormasPago = listadoService.getMapFormasdePago(cotizacion);
			if (!mapFormasPago.containsKey(Integer.valueOf(idFormaPago))) {
				throw new SystemException("No se encontr\u00F3 la forma de pago " + idFormaPago);
			}
			
			cotizacion.setIdFormaPago(BigDecimal.valueOf(idFormaPago));
		}
		
		try {
			entidadService.save(cotizacion);
			LOG.info("despu\u00E9s de actualizar el idFormaPago y el idMedioPago de la cotizacion ...");
		} catch (Exception e) {
            LOG.error("Error al actualizar el idMedioPago y el idFormaPago para la cotizaci\u00F3n "+cotizacion.getIdToCotizacion()+" . "+e.getMessage());
            throw new SystemException("Error al actualizar el idMedioPago y el idFormaPago para la cotizaci\u00F3n "+cotizacion.getIdToCotizacion());
		}
		
        StringBuilder errores = validaEmisionCotizacion(cotizacion.getIdToCotizacion(), true);
        
		if(errores != null && !errores.toString().isEmpty()){
			throw new SystemException("Errores: " + errores);
		}

		try{
			result = emisionService.emitir(cotizacion, false);
		} catch (Exception e){
			throw new SystemException("Error al emitir: "+e.getMessage(), e);
		}
		
		return result;
	}
	
	private StringBuilder validaEmisionCotizacion(BigDecimal idToCotizacion, boolean documentacionCompleta) {
		TerminarCotizacionDTO validacion = cotizacionService.validarEmisionCotizacion(idToCotizacion);
		StringBuilder errores = null;
		if(validacion != null){
			errores = new StringBuilder();
			if(validacion.getExcepcionesList() == null) {
				errores.append(validacion.getMensajeError());
			} else {
				for(ExcepcionSuscripcionReporteDTO exception : validacion.getExcepcionesList()){
					if ( (exception.getIdExcepcion() == 11 || exception.getIdExcepcion() == 5) && documentacionCompleta) {
						LOG.info("Agente indica que el cliente tiene documentaci\u00F3n completa para validación de art140");
					} else {
						if(errores.length() > 0){
							errores.append(", ");							
						}

						errores.append(exception.getDescripcionExcepcion().concat(": ").concat(exception.getDescripcionRegistroConExcepcion()));
					}
				}
			}
		}
		return errores;
	}

	@Override
	public List<String> listPaquetesByIdNegocio(Long idNegocio, String token) throws SystemException {
        LOG.debug("Entrando a listPaqueteByIdNegocio ");
        LOG.debug("idNegocio => " + idNegocio);
        
        validateToken( token );
        
		List<String> paquetes = new ArrayList<String>();
		String seccion = idNegocio.longValue() == 516 ? "AUTOMOVILES INDIVIDUALES" : "AUTOPLAZO AUTOS";
		
		Negocio negocio = negocioService.findById(idNegocio);		
	
		NegocioProducto negocioProducto = negocioProductoDao.getNegocioProductoByIdNegocioIdProducto(negocio.getIdToNegocio(), new BigDecimal (191));

		NegocioTipoPoliza negocioTipoPoliza = negocioTipoPolizaService
				.getPorIdNegocioProductoIdToTipoPoliza(negocioProducto.getIdToNegProducto(),
						negocioProducto.getProductoDTO().getTiposPoliza().get(0).getIdToTipoPoliza());
		negocioTipoPoliza.setNegocioProducto(negocioProducto);
		
		
		NegocioSeccion negocioSeccion = negocioSeccionService
		.getByProductoNegocioTipoPolizaSeccionDescripcion(
				negocioProducto.getProductoDTO(),negocio,
				negocioTipoPoliza.getTipoPolizaDTO(),seccion);
		RelacionesNegocioPaqueteDTO relacionesNegocioPaqueteDTO = negocioPaqueteSeccionService.getRelationLists(negocioSeccion);
		
		for (NegocioPaqueteSeccion negocioPaqueteSeccion : relacionesNegocioPaqueteDTO.getAsociadas()) {
			if (idNegocio.intValue() == 491){
				if (negocioPaqueteSeccion.getPaquete().getId().intValue() == 1){
					paquetes.add(negocioPaqueteSeccion.getPaquete().getId().toString()+"-AMPLIA PLUS");
				}else{
					paquetes.add(negocioPaqueteSeccion.getPaquete().getId().toString()+"-AMPLIA");
				}
			}else{
				paquetes.add(negocioPaqueteSeccion.getPaquete().getId().toString() + "-" + negocioPaqueteSeccion.getPaquete().getDescripcion());				
			}			
		}
		
		return paquetes;
	}
	
	private boolean evaluaFolioNuevaCotizacion(String folio){
		//Revisamos si el folio ya tiene asignada una cotizacion en el sistema.
		List<CotizacionDTO> cotizacionByFolio = entidadService.findByProperty(CotizacionDTO.class, "folio", folio);
		boolean creaNuevaCotizacion = true;		
		
		if(cotizacionByFolio != null && cotizacionByFolio.size() > 0){
			
			if(!cotizacionByFolio.get(0).getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA)){
				creaNuevaCotizacion = false;	
			}
		}
		
		return creaNuevaCotizacion;		
		
	}
	
	@Override
	public byte[] imprimirPoliza(ParametrosImpresionPolizaView params, String token) throws SystemException{

		validateImprimirPoliza(params, token);
		
		final BigDecimal idPoliza = obtenerIdPoliza(params);
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, idPoliza);
		
		long ultimoEndosoValidFromDateTimeStr = 0l;
		long ultimoEndosoRecordFromDateTimeStr = 0l;
		double dias = Utilerias.obtenerDiasEntreFechas(new Date(), poliza.getCotizacionDTO().getFechaInicioVigencia());
		Date fechaInicio = new Date();

		if(dias > 0){
			fechaInicio = poliza.getCotizacionDTO().getFechaInicioVigencia();
		}
		
		try{
			EndosoDTO ultimoEndosoDTO = endosoService.getUltimoEndosoByValidFrom(idPoliza.longValue(), fechaInicio);
			
			// Guardar el no. de orden del brocker
			if(StringUtils.isNotEmpty(params.getNoOrder())) {
				endosoService.guardaNoOrderBroker(idPoliza, params.getNoOrder());
			}

			if (ultimoEndosoDTO != null) {
				ultimoEndosoValidFromDateTimeStr = ultimoEndosoDTO.getValidFrom().getTime();
				ultimoEndosoRecordFromDateTimeStr = ultimoEndosoDTO.getRecordFrom().getTime();
			} else {
				ultimoEndosoValidFromDateTimeStr = fechaInicio.getTime();
				ultimoEndosoRecordFromDateTimeStr = fechaInicio.getTime();					
			}

			DateTime validOnDT = new DateTime(ultimoEndosoValidFromDateTimeStr);
			DateTime recordFromDT = new DateTime(ultimoEndosoRecordFromDateTimeStr);
			Locale locale = new Locale("es", "MX");
			int incisoInicial = 0;
			int incisoFinal = 0;
			
			//Cuando certificadoNUPorInciso = true se debe imprimir el inciso
			if (params.getTodosLosIncisos() || params.getCertificadoNUPorInciso()){
				incisoInicial = 1;
				incisoFinal = 1;
			}
			
			TransporteImpresionDTO transporte = generaPlantillaReporteBitemporalService.imprimirPoliza(
					 idPoliza,locale,validOnDT,recordFromDT, 
					 params.getCaratula(),params.getRecibo(), 
					 params.getCertificadoNUPorInciso(),params.isIncluirReferenciasBancarias(),
					 params.getAnexos(),
					 incisoInicial,incisoFinal,(short)2,params.getSituacionActual(),
					 params.getCondicionesEspeciales(),params.isAviso(),params.isDerechos());
					
			return transporte.getByteArray();				
		}catch (Exception e) {
			LOG.info("Ocurri\u00F3 un error al intentar imprimir la p\u00F3liza: \n" + e.getMessage());
			return "".getBytes();
		}	

	}
	
	
	
	public BigDecimal obtenerIdPoliza(ParametrosImpresionPolizaView params) {
		if(params.getIdPoliza() == null) {
			NumeroPolizaCompleto numeroPolizaCompletoMidas = NumeroPolizaCompletoMidas
					.parseNumeroPolizaCompleto(params.getNumeroPoliza());
			PolizaDTO poliza = polizaFacadeRemote.find(numeroPolizaCompletoMidas);
			return poliza.getIdToPoliza();
		} else {
			return params.getIdPoliza();
		}
	}
	


	private void validateImprimirPoliza(ParametrosImpresionPolizaView params, String token) throws SystemException {
		validateToken(token);
		if (params == null) {
			throw new SystemException("Params no puede ser null");
		}
		if (params.getIdPoliza() == null && StringUtils.isEmpty(params.getNumeroPoliza())) {
			throw new SystemException("Falta el idToPoliza o el numeroPoliza para poder imprimir");
		}
	}

	@Override
	public byte[] imprimirPoliza(BigDecimal idPoliza, String token) throws SystemException{
		final ParametrosImpresionPolizaView params = new ParametrosImpresionPolizaView();
		params.setIdPoliza(idPoliza);
		return imprimirPoliza(params, token);
	}

	@SuppressWarnings("unused")
	@Override
	public Long guardarCobranza(BigDecimal idCotizacion, ConductoCobroDTO conductoCobro, boolean esEndoso, String token) throws Exception {
    	LOG.info("WSCotizacionGenericaServiceImpl.guardarCobranza medioPago => " + conductoCobro.getIdMedioPago());
    	
    	Usuario usuario = validarToken(token);
    	
		CotizacionDTO cotizacion = null;
		CuentaPagoDTO cuentaPagoDTO = new CuentaPagoDTO();
		
		if (idCotizacion != null) {
			cotizacion = entidadService.findById(CotizacionDTO.class,	idCotizacion);
		} else {
			throw new SystemException("Favor de especificar el id de cotización");
		}
		
		if (cotizacion == null) {
			throw new SystemException("Ocurri\u00F3 un error al buscar la cotizaci\u00F3n "+idCotizacion+". No se pudo guardar los datos de cobranza");
		}
		
		TipoCobro[] tiposCobro = CuentaPagoDTO.TipoCobro.values();
		
		for(TipoCobro tCobro: tiposCobro){
			Short tipoCobro = tCobro.valor();
			
			if(tipoCobro.equals(conductoCobro.getIdMedioPago())){
				if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.TARJETA_CREDITO.valor())){
					if (conductoCobro.getDatosTarjeta().getIdTipoTarjeta().equals(CuentaPagoDTO.TipoTarjeta.MASTERCARD.valor()) ){
						cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.MASTERCARD);
					} else if (conductoCobro.getDatosTarjeta().getIdTipoTarjeta().equals(CuentaPagoDTO.TipoTarjeta.VISA.valor())){
						cuentaPagoDTO.setTipoTarjeta(CuentaPagoDTO.TipoTarjeta.VISA);
					} else {
						throw new SystemException("Tipo de Tarjeta inv\u00E1lido " + conductoCobro.getDatosTarjeta().getIdTipoTarjeta());
					}
					
					if (cuentaPagoDTO.getDiaPago() == null) {
						Calendar calendar = Calendar.getInstance();
						cuentaPagoDTO.setDiaPago(calendar.get(Calendar.DAY_OF_MONTH));
					}
				}else if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.CUENTA_AFIRME.valor())){
					cuentaPagoDTO.setIdBanco((long) conductoCobro.getDatosTarjeta().getIdBanco());
				}else if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.EFECTIVO.valor())){
					cuentaPagoDTO = new CuentaPagoDTO();
				} else if(tipoCobro.equals(CuentaPagoDTO.TipoCobro.DOMICILIACION.valor())){
					if (cuentaPagoDTO.getDiaPago() == null){
						Calendar calendar = Calendar.getInstance();
						cuentaPagoDTO.setDiaPago(calendar.get(Calendar.DAY_OF_MONTH));
					}
				}
				
				cuentaPagoDTO.setTipoConductoCobro(tCobro);
				
				break;
			}			
		}
		
		Long idConductoCobroCliente = null;
		
		if (cuentaPagoDTO != null) {
			LOG.info("WSCotizacionGenericaServiceImpl.guardarCobranza cuenta => " + conductoCobro.getDatosTarjeta().getNumeroTarjeta());
			LOG.info("WSCotizacionGenericaServiceImpl.guardarCobranza tipoPersona => " + cotizacion.getSolicitudDTO().getClaveTipoPersona().toString());
			LOG.info("WSCotizacionGenericaServiceImpl.guardarCobranza idCliente => " + cotizacion.getIdToPersonaContratante());
			cuentaPagoDTO.setTipoPersona(cotizacion.getSolicitudDTO().getClaveTipoPersona().toString());
			cuentaPagoDTO.setIdCliente(cotizacion.getIdToPersonaContratante());
			cuentaPagoDTO.setFechaVencimiento(conductoCobro.getDatosTarjeta().getFechaVencimientoTarjeta());
			cuentaPagoDTO.setCodigoSeguridad(conductoCobro.getDatosTarjeta().getCodigoSeguridadTarjeta());
			cuentaPagoDTO.setTarjetaHabiente(conductoCobro.getDatosTitular().getTarjetaHabiente());
			cuentaPagoDTO.setCuenta(conductoCobro.getDatosTarjeta().getNumeroTarjeta());
			cuentaPagoDTO.setIdBanco((long) conductoCobro.getDatosTarjeta().getIdBanco());
			
			cuentaPagoDTO.setCorreo(conductoCobro.getDatosTitular().getCorreo());
			cuentaPagoDTO.setTelefono(conductoCobro.getDatosTitular().getTelefono());
			
			Domicilio domicilio = new Domicilio();
			domicilio.setClavePais(conductoCobro.getDatosTitular().getClavePais());
			domicilio.setClaveEstado(conductoCobro.getDatosTitular().getIdEstado());
			domicilio.setClaveCiudad(conductoCobro.getDatosTitular().getIdMunicipio());
			domicilio.setIdColonia(conductoCobro.getDatosTitular().getIdColonia());
			domicilio.setNombreColonia(conductoCobro.getDatosTitular().getNombreColonia());
			domicilio.setCalleNumero(conductoCobro.getDatosTitular().getCalleNumero());
			domicilio.setCodigoPostal(conductoCobro.getDatosTitular().getCodigoPostal());
			
			cuentaPagoDTO.setDomicilio(domicilio);
			cuentaPagoDTO.setRfc(conductoCobro.getDatosTitular().getRfc());
			
			try {
				LOG.info("WSCotizacionGenericaServiceImpl.guardarCobranza antes de guardarConductoCobro...");
				idConductoCobroCliente = pagoService.guardarConductoCobro(cotizacion.getIdToCotizacion(), cuentaPagoDTO, usuario, esEndoso);
				LOG.info("WSCotizacionGenericaServiceImpl.guardarCobranza despues de guardarConductoCobro...");
				if (!esEndoso) {
					pagoService.guardarConductoCobroCotizacionAInciso(cotizacion.getIdToCotizacion());
					LOG.info("WSCotizacionGenericaServiceImpl.guardarCobranza después de guardarConductoCobroCotizacionAInciso...");
				}
			} catch(RuntimeException e){
				throw new SystemException("error al guardar los datos de cobranza.. "+e.getMessage());
			} catch(Exception e){
				LOG.error("error al guardar los datos de cobranza... "+e.getMessage());
				throw new Exception("Error al guardar el conducto de cobro.\n"+e.getMessage());
			}
			
		} else{
			LOG.error("error al guardar los datos de cobranza... ");
			throw new SystemException("Ocurri\u00F3 un error al guardar los datos de cobranza.");
		}
		
		return idConductoCobroCliente;
	}
	
	@Override
	public Map<Long, String> getListAgentes( String token ) throws SystemException{
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		Agente agente = new Agente();
		boolean tieneRolPromotor = false;
		boolean tieneRolAgente = false;
		List<Agente> list = null;
		Usuario usuario = null;
		List<Rol> roles = null;
		String nombreUsuario = "";

		usuario = validarToken( token );
		
		if (usuario != null){
			roles = usuario.getRoles();
			nombreUsuario = usuario.getNombreUsuario();
		} else{
			throw new SystemException("No se pudo encontrar el usuario "+nombreUsuario);
		}
		
		if (roles != null){
			for (Rol item: roles){
				if ( item.getId() == Integer.parseInt(sistemaContext.getIdRolAgente())) {
					tieneRolAgente = true;
	            }
				if ( item.getId() == Integer.parseInt(sistemaContext.getIdRolPromotor())) {
					tieneRolPromotor = true;
	            }
			}
		} else{
			throw new SystemException("El usuario "+nombreUsuario+" no tiene roles asignados");
		}
		
		try {
			agente = usuarioService.getAgenteUsuarioActual(usuario);
		} catch (Exception e){
			LOG.error("No fue posible obtener el agente del usuario "+usuario.getNombreUsuario()+" \n"+e.getMessage());
			throw new SystemException("No fue posible obtener el agente del usuario "+usuario.getNombreUsuario());
		}
		
		if (tieneRolPromotor){ // si es un promotor
			Long idPromotoria = agente.getPromotoria().getId();
			LOG.info("el usuario es promotor, idPromotoria:"+idPromotoria);
			try {
				list = agenteMidasService.findAgentesByPromotoria(idPromotoria);
			} catch(Exception e){
				LOG.info("Error al obtener los agentes de la promotoria:"+idPromotoria+" \n"+e.getMessage());
				throw new SystemException("Error al obtener los agentes de la promotor\u00EDa "+idPromotoria);
			}
		} else {
			if (agente != null) {
				list = new ArrayList<Agente>();
				list.add(agente);
			}
		}

		if(list != null && !list.isEmpty()){
			for (Agente dto: list){
				map.put(dto.getIdAgente(), dto.getPersona().getNombreCompleto());
			}
		}
		
		return map;
	}
	
	@Override
	public Map<Long, String> getListNegocios(Integer codigoAgente, String token) throws SystemException{
		List<Negocio> list = null;
		validateToken( token );
		
		if (codigoAgente == null || codigoAgente == 0){
			throw new SystemException("El c\u00F3digo de agente es requerido");
		}
		
		Agente agente = new Agente();
		agente.setIdAgente(codigoAgente.longValue());
		try {
			agente = agenteMidasService.findByClaveAgente(agente);
			
			if (agente.getId() == null || agente.getId() == 0){
				throw new SystemException("No fue posible obtener el agente con la clave "+codigoAgente+" dada.");	
			}
		} catch (Exception e1) {
			LOG.error("No fue posible obtener el agente\n"+e1.getMessage());
			throw new SystemException("No fue posible obtener el agente con la clave "+codigoAgente+" dada");
		}
		
		try {
			list = cotizadorAgentesService.getNegociosPorAgente(agente.getId().intValue(), Negocio.CLAVE_NEGOCIO_AUTOS);
		} catch (Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de agentes.\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtner la lista de agentes.");
		}
		
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		
		for (Negocio dto: list){
			map.put(dto.getIdToNegocio(), dto.getDescripcionNegocio());
		}
		
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getListMarcas(BigDecimal idLineaNegocio, String token) throws SystemException{
		validateToken( token );
		
		if (idLineaNegocio == null){
			throw new SystemException("La l\u00EDnea de negocio es requerido");
		}
		
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>(); 
			
		try {
			map = listadoService.getMapMarcaVehiculoPorNegocioSeccion(idLineaNegocio);
		} catch (Exception e){
			throw new SystemException("Ocurri\u00F3 un error al obtner la lista de marcas.\n"+e.getMessage());
		}
		
		return map;
	}
	
	@Override
	@Deprecated
	public Map<String, String> getListEstilos(BigDecimal idTcMarcaVehiculo, String token) throws SystemException{
		Map<String, String> map = new LinkedHashMap<String, String>();
		validateToken( token );
		
		if (idTcMarcaVehiculo == null || idTcMarcaVehiculo.compareTo(BigDecimal.ZERO) == 0){
			throw new SystemException("La marca del veh\u00EDculo es requerido");
		}
		
		try {
			map = listadoService.listarEstilosPorMarcaCveTipoBien(idTcMarcaVehiculo, "AUTOS");
			
		} catch (Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de estilos.\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de estilos");
		}
		
		return map;
	}
	
	
	public Map<String, String> buscarEstilo(EstiloView filtro, String token) throws SystemException {
		validateToken(token);
		validateParamsBuscarEstilo(filtro);
		List<EstiloVehiculoDTO> estilos = listadoService.getListarEstilo(
				filtro.getIdMarca(), filtro.getModelo(),
				filtro.getIdLineaNegocio(), new BigDecimal(MonedaDTO.MONEDA_PESOS), null,
				filtro.getDescripcion());
		Map<String, String> result = new HashMap<String, String>();
		for(EstiloVehiculoDTO estilo : estilos) {
			result.put(estilo.getId().getClaveEstilo(), estilo.getDescription());
		}
		
		return result;

	}
	
	private void validateParamsBuscarEstilo(EstiloView filtro) throws SystemException {
		if (filtro == null || filtro.getIdMarca() == null) {
			throw new SystemException("La marca es obligatoria para buscar el estilo");
		}
	}
	
	@Override
	public Map<String, String> getListEstados(Long idToNegocio, String token) throws SystemException{
		Map<String, String> map = new LinkedHashMap<String, String>();
		validateToken( token );
		
		if (idToNegocio == null || idToNegocio.compareTo(0L) == 0){
			//se obtienen todos los estados
			map = listadoService.getMapEstadosMX();
		} else {
			try {
				map = listadoService.getEstadosPorNegocioId(idToNegocio, false);
			} catch(Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de estados por negocio\n"+e.getMessage());
				throw new SystemException("Ocurri\u00F3 un error al obtener la lista de estados por negocio");
			}
		}
		
		return map;
	}
	
	@Override
	public Map<String, String> getListMunicipios(String idEstado, String token) throws SystemException{
		Map<String, String> map = new LinkedHashMap<String, String>();
		validateToken( token );
		
		if (idEstado == null || idEstado.equals("")){
			throw new SystemException("El estado es requerido");
		}
		
		try {
			map = listadoService.getMapMunicipiosPorEstado(idEstado);
		} catch(Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de municipios\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de municipios");
		}
		
		return map;
	}
	
	@Override
	public Map<Long, String> getListPaquetes(BigDecimal idToNegSeccion, String token) throws SystemException{
		validateToken( token );
		
		if (idToNegSeccion == null){
			throw new SystemException("La l\u00EDnea de negocio es requerido");
		}
		
		Map<Long, String> map = new LinkedHashMap<Long, String>(); 
		
		try {
			map = listadoService.getMapPaquetePorNegocioSeccion(idToNegSeccion);
		} catch(Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de paquetes:\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de paquetes");
		}
		
		return map;
	}
	
	@Override
	public Map<Integer, String> getListFormasPago(BigDecimal idToNegTipoPoliza, String token) throws SystemException{
		validateToken( token );
		
		if (idToNegTipoPoliza == null){
			throw new SystemException("El tipo de  póliza es requerido");
		}
		
		try {
			return listadoService.getMapFormasdePagoByNegTipoPoliza(idToNegTipoPoliza);
		} catch(Exception e){
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de formas de pago");
		}
		
	}
	
	@Override
	public Map<Integer, String> getListBancos( String token ) throws SystemException{
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();		
		List<BancoEmisorDTO> list = new ArrayList<BancoEmisorDTO>();
		validateToken( token );
		
		try {
			list = bancoEmisorFacadeRemote.findAll();
		} catch(Exception e){
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de bancos");
		}
		
		for (BancoEmisorDTO dto: list){
			map.put(dto.getIdBanco(), dto.getNombreBanco());
		}
		
		return map;
	}
	
	@Override
	public Map<Integer, String> getListMediosPago( String token ) throws SystemException{
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		validateToken( token );
		
		map.put(15, "EFECTIVO");
		map.put(8, "COBRANZA DOMICILIADA");
		map.put(4, "TARJETA DE CREDITO");
		map.put(1386, "CUENTA AFIRME (CREDITO O DEBITO)");
		
		return map;
	}
	
	@Override
	public Map<String, String> getListTiposTarjeta( String token ) throws SystemException{
		Map<String, String> map = new LinkedHashMap<String, String>();
		validateToken( token );

		map.put("VS", "VISA");
		map.put("MC", "Master Card");
		return map;
	}
	
	@Override
	public Map<Short, Short> getListModelos(BigDecimal idToNegSeccion, BigDecimal idMoneda, BigDecimal idMarca, String token) throws SystemException{
		Map<Short, Short> map = new LinkedHashMap<Short, Short>(); 
		validateToken( token );
		
		if (idToNegSeccion == null || idToNegSeccion.compareTo(BigDecimal.ZERO) == 0){
			throw new SystemException("La l\u00EDnea de negocio es requerido");
		}
		
		if (idMarca == null || idMarca.compareTo(BigDecimal.ZERO) == 0){
			throw new SystemException("La marca es requerido");
		}
		
		try {
			map = listadoService.getMapModeloVehiculoPorMonedaMarcaNegocioSeccion(idMoneda, idMarca, idToNegSeccion);
		} catch(Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de paquetes\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de paquetes");
		}
		
		return map;
	}
	
	@Override
	public Map<Long, String> getListNegocioProductos(Long idToNegocio, BigDecimal idToProducto, String token) throws SystemException{
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		validateToken( token );
		
		if (idToNegocio == null || idToNegocio.compareTo(0L) == 0){
			throw new SystemException("El negocio es requerido");
		}
		
		if (idToProducto == null || idToProducto.compareTo(BigDecimal.ZERO) == 0){
			throw new SystemException("El producto es requerido");
		}
		
		try {
			NegocioProducto dto = negocioProductoDao.getNegocioProductoByIdNegocioIdProducto(
					idToNegocio, idToProducto);
			
			map.put(dto.getIdToNegProducto(), dto.getProductoDTO().getDescripcion());
		} catch(Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de Productos del negocio "+idToNegocio+" dado...\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de Productos del negocio "+idToNegocio+" dado.");
		}

		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getListTiposPolizaByNegProducto(Long idToNegProducto, String token) throws SystemException{
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		validateToken( token );
		
		if (idToNegProducto == null || idToNegProducto.compareTo(0L) == 0){
			throw new SystemException("El producto es requerido");
		}
		
		try {
			NegocioTipoPoliza dto = negocioTipoPolizaService.getPorIdNegocioProductoIdToTipoPoliza(idToNegProducto, ID_TIPO_POLIZA);
			map.put(dto.getIdToNegTipoPoliza(), dto.getTipoPolizaDTO().getDescripcion());
		} catch(Exception e){
			try {
				NegocioTipoPoliza dto = negocioTipoPolizaService.getPorIdNegocioProductoIdToTipoPoliza(idToNegProducto, ID_TIPO_POLIZA_FLOTILLA);
				map.put(dto.getIdToNegTipoPoliza(), dto.getTipoPolizaDTO().getDescripcion());
			} catch (Exception ex) {
				LOG.error("Ocurrió un error al obtener la lista de Tipos de Poliza del negocio-producto "+idToNegProducto+" dado\n"+e.getMessage());
				throw new SystemException("Ocurrió un error al obtener la lista de Tipos de Poliza del negocio-producto "+idToNegProducto+" dado");
			}
		}
		
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getListLineasNegocio(BigDecimal idToNegTipoPoliza, String token) throws SystemException{
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		validateToken( token );
		
		if (idToNegTipoPoliza == null || idToNegTipoPoliza.compareTo(BigDecimal.ZERO) == 0){
			throw new SystemException("el tipoPoliza es requerido");
		}
		
		List<NegocioSeccion> negocioSeccionAsociadas = new ArrayList<NegocioSeccion>(1);
		
		try {
			negocioSeccionAsociadas = negocioSeccionService.getNegSeccionesPorIdNegTipoPoliza(idToNegTipoPoliza);
			
			for (NegocioSeccion dto: negocioSeccionAsociadas){
				map.put(dto.getIdToNegSeccion(), dto.getSeccionDTO().getDescripcion());	
			}
		} catch(Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener las lineas de negocio\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener las l\u00EDneas de negocio");
		}
		
		return map;
	}
	
	@Override
	public Map<BigDecimal, String> getListTiposUso(BigDecimal idToNegSeccion, String token) throws SystemException{
		validateToken( token );
		if (idToNegSeccion == null || idToNegSeccion.equals(BigDecimal.ZERO)){
			throw new SystemException("La l\u00EDnea de negocio es requerido");
		}
		return listadoService.getMapTipoUsoVehiculoByNegocio(idToNegSeccion);
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
    public  Map<String, String> emitirPoliza(String numeroSerie, String numeroMotor, String numeroPlaca, int idFormaPago, 
    		BigDecimal idCotizacion, Short medioPago, String tipoTarjeta, int idBanco, 
			String fechaVencimientoTarjeta, String codigoSeguridad, String tarjetaHabiente, String cuenta, 
			String correo, String telefono, String clavePais, String claveEstado, String claveCiudad, String idColonia, 
			String nombreColonia, String calleNumero, String codigoPostal, String rfc, String nombreAsegurado, String token) throws SystemException{
    	LOG.info(this.getClass().getName()+".emitir ... entrando a emitir póliza");
    	
    	validateToken( token );
    	
    	if (medioPago == null){
    		throw new SystemException("El medio de pago es requerido");
    	}
    	
    	LOG.info("idCotizacion => " + idCotizacion);
    	
    	CotizacionDTO cotizacion = null;
    	
    	if (idCotizacion != null){
			cotizacion = entidadService.findById(CotizacionDTO.class, idCotizacion);
    	} else{
			throw new SystemException("Favor de especificar el id de cotización");
    	}
    	
    	LOG.info("idCotizacion encontrada => " + cotizacion.getIdToCotizacion());
    	
    	LOG.info("folio => " + cotizacion.getFolio());
    	LOG.info("numeroSerie => " + numeroSerie);
    	LOG.info("numeroMotor => " + numeroMotor);
    	LOG.info("tipoCobro(Medio Pago) => " + medioPago);
    	LOG.info("formaPago => " + idFormaPago);
    	
        Map<String, String> result = new HashMap<String, String>();
        
        if(cotizacion.getIdToPersonaContratante() == null){
			throw new SystemException("Contratante no guardado en cotizacion, Revisar Codigo Postal");
		}
		
		
		IncisoCotizacionDTO incisoCot = cotizacion.getIncisoCotizacionDTOs().get(0);
		
		IncisoAutoCot incisoAuto = incisoCot.getIncisoAutoCot();
		incisoAuto.setNumeroMotor(StringUtils.isBlank(numeroMotor) ? "S/N" : numeroMotor);
		incisoAuto.setNumeroSerie(numeroSerie);
		incisoAuto.setPlaca(StringUtils.isBlank(numeroPlaca) ? "PEND" : numeroPlaca);		

		if(StringUtils.isEmpty(nombreAsegurado)) {
			incisoAuto.setPersonaAseguradoId(cotizacion.getIdToPersonaContratante().longValue());
			incisoAuto.setNombreAsegurado(cotizacion.getNombreContratante());
		} else {
			incisoAuto.setNombreAsegurado(nombreAsegurado);
		}

		try {
			incisoService.guardarIncisoAutoCot(incisoAuto);
		} catch(Exception e){
			throw new SystemException("Error al guardar el Inciso Auto Cot. ", e);
		}

		if (medioPago != MEDIO_PAGO_EFECTIVO && medioPago != MEDIO_PAGO_TC && medioPago != MEDIO_PAGO_DOMICILIACION && medioPago != CuentaPagoDTO.TipoCobro.CUENTA_AFIRME.valor()) {
			throw new SystemException("Favor de especificar un medio de pago v\u00E1lido");
		}
		
		cotizacion.setIdMedioPago(BigDecimal.valueOf(medioPago));
		
		//Valida forma de pago
		if (idFormaPago > 0) {
			final Map<Integer,String> mapFormasPago = listadoService.getMapFormasdePago(cotizacion);
			if (!mapFormasPago.containsKey(Integer.valueOf(idFormaPago))) {
				throw new SystemException("No se encontró la forma de pago " + idFormaPago);
			}
			
			cotizacion.setIdFormaPago(BigDecimal.valueOf(idFormaPago));
		}
		
		try {
			entidadService.save(cotizacion);
			LOG.info("después de actualizar el idFormaPago y el idMedioPago de la cotizacion ...");
		} catch (Exception e) {
            LOG.error("Error al actualizar el medio y forma de pago de la cotizaci\u00F3n "+cotizacion.getIdToCotizacion()+ " - "+ e.getMessage());
            throw new SystemException("Error al actualizar el medio y forma de pago de la cotizaci\u00F3n "+cotizacion.getIdToCotizacion());
		}
		
        StringBuilder errores = validaEmisionCotizacion(cotizacion.getIdToCotizacion(), true);
        
		if(errores != null && !errores.toString().isEmpty()){
			throw new SystemException("Errores: " + errores);
		}

		try{
			result = emisionService.emitir(cotizacion, false);
		} catch (Exception e){
			LOG.error("Error al intentar emitir la póliza para la cotizaci\u00F3n "+cotizacion.getIdToCotizacion()+" ..."+ e.getMessage());
			throw new SystemException("Error al emitir: "+e.getMessage(), e);
		}
		
		return result;
	}
	
	public void validateToken( String token ) throws SystemException{
		if (StringUtils.isBlank(token)){
			throw new SystemException("El token de sesi\u00F3n es requerido");
		} else{
			if(!usuarioService.isTokenActive(token))
				throw new SystemException("El token "+token+" es un token inactivo.");
		}
	}
	
	private Long crearConductoCobro(EndosoTransporteDTO dto)
			throws SystemException {
		Long idConductoCobroCliente = null;
		
		if (dto.getDatosEndoso().getConductoCobro().getIdMedioPago() != MEDIO_PAGO_EFECTIVO) {
			try {
				idConductoCobroCliente = guardarCobranza(dto.getIdCotizacion(),
						dto.getDatosEndoso().getConductoCobro(), true, dto.getToken());
			} catch(SystemException e){
				LOG.error("--> No fue posible guardar el conducto de cobro..\n"+e.getMessage());
				throw new SystemException("No fue posible guardar el conducto de cobro :"+e.getMessage());
			} catch(Exception e){
				LOG.error("--> ocurri\u00F3 un error al guardar el conducto cobro...\n"+e.getMessage());
				throw new SystemException("No fue posible guardar el conducto de cobro :"+e.getMessage());
			}
		}
		
		return idConductoCobroCliente;
	}

	public ListadoService getListadoService() {
		return listadoService;
	}

	public void setListadoService(ListadoService listadoService) {
		this.listadoService = listadoService;
	}

	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	@Override
	public Map<String, Object> cotizarEndoso(EndosoTransporteDTO dto)
			throws SystemException {
		Map<String, Object> result = new HashMap<String, Object>(1);
		Long idConductoCobroCliente = null;
		
		validateToken(dto.getToken());
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		
		dto.setIdCotizacion(poliza.getCotizacionDTO().getIdToCotizacion());
		
		if (dto.getClaveTipoEndoso() == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO) {
			//validar si aplica el endoso de conducto de cobro
			if (!poliza.getCotizacionDTO().getSolicitudDTO().getNegocio().getAplicaEndosoConductoCobro()) {
				throw new SystemException("Este negocio no aplica para el endoso de conducto de cobro");
			}
			
			idConductoCobroCliente = crearConductoCobro(dto);
			
			dto.getDatosEndoso().getConductoCobro().setIdConductoCobroCliente(idConductoCobroCliente);			
		}
		
		result = endosoWSMidasAutosService.cotizar(dto, poliza);
		
		if (dto.getClaveTipoEndoso() == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO) {
			result.put("idConductoCobro", idConductoCobroCliente);
		}
		
		return result;
	}

	@Override
	public Map<String, Object> emitirEndoso(EndosoTransporteDTO dto) throws SystemException {
		Map<String, Object> result = new HashMap<String, Object>();
		
		validateToken(dto.getToken());
		result = endosoWSMidasAutosService.emitir(dto);
		
		return result;
	}

	@Override
	public byte[] imprimirEndoso(EndosoTransporteDTO dto) throws SystemException {
		validateToken(dto.getToken());
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		return endosoWSMidasAutosService.imprimirEndoso(dto, poliza);
	}
	
	@Override
	public PolizaDTO validatePoliza(String numeroPoliza) throws SystemException {
		PolizaDTO poliza = null;
		NumeroPolizaCompleto numeroPolizaCompleto = null;
		
		try {
			numeroPolizaCompleto = NumeroPolizaCompleto.parseNumeroPolizaCompleto(numeroPoliza);
		} catch(Exception e){
			LOG.error("Error de formato en la p\u00F3liza "+numeroPoliza+":\n"+e.getMessage());
			throw new SystemException("P\u00F3liza inválida: "+e.getMessage());
		}
		
		try {
			poliza = polizaFacadeRemote.find(numeroPolizaCompleto);
		} catch(Exception e){
			LOG.error("Error al buscar la p\u00F3liza "+numeroPoliza+"\n"+e.getMessage());
			throw new SystemException("Error al buscar la p\u00F3liza "+numeroPoliza);
		}
		
		if (poliza == null){
			LOG.info("La p\u00F3liza:"+numeroPoliza+" no existe...");
			throw new SystemException("La p\u00F3liza "+numeroPoliza+" no existe...");
		}
		
		return poliza;
	}
	
	private ConductoCobroDTO poblarConductoCobro(Short medioPago, String tipoTarjeta, int idBanco, 
			String fechaVencimientoTarjeta, String codigoSeguridad, String tarjetaHabiente, String cuenta, 
			String correo, String telefono, String clavePais, String claveEstado, String claveCiudad, 
			String idColonia, String nombreColonia, String calleNumero, String codigoPostal, String rfc) throws SystemException {
		ConductoCobroDTO dto = new ConductoCobroDTO();
		
		dto.setIdMedioPago(medioPago);
		
		DatosTarjetaDTO datosTarjeta = new DatosTarjetaDTO();
		datosTarjeta.setIdTipoTarjeta(tipoTarjeta);
		datosTarjeta.setIdBanco(Long.valueOf(idBanco));
		datosTarjeta.setNumeroTarjeta(cuenta);
		datosTarjeta.setFechaVencimientoTarjeta(fechaVencimientoTarjeta);
		datosTarjeta.setCodigoSeguridadTarjeta(codigoSeguridad);
		dto.setDatosTarjeta(datosTarjeta);
		
		//CuentaPagoDTO datosTitular = new CuentaPagoDTO();
		DatosTitularDTO datosTitular = new DatosTitularDTO();
		datosTitular.setTarjetaHabiente(tarjetaHabiente);
		datosTitular.setCorreo(correo);
		datosTitular.setTelefono(telefono);
		
		//Domicilio domicilio = new Domicilio();
		//datosTitular.setDomicilio(domicilio);
		datosTitular.setClavePais(clavePais);
		datosTitular.setIdEstado(claveEstado);
		datosTitular.setIdMunicipio(claveCiudad);
		datosTitular.setIdColonia(idColonia);
		datosTitular.setNombreColonia(nombreColonia);
		datosTitular.setCalleNumero(calleNumero);
		datosTitular.setCodigoPostal(codigoPostal);
		datosTitular.setRfc(rfc);
		dto.setDatosTitular(datosTitular);
		
		return dto;
	}

	@Override
	public List<CotizacionEndosoDTO> buscarCotizacionesEndosoPoliza(EndosoTransporteDTO dto) throws SystemException {
		validateToken(dto.getToken());
		
		List<CotizacionEndosoDTO> listaCotizacionesEndoso = new ArrayList<CotizacionEndosoDTO>();
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		
		CotizacionEndosoDTO filtrosBusqueda = new CotizacionEndosoDTO();
		
		filtrosBusqueda.setNumeroPoliza(poliza.getNumeroPoliza());
		filtrosBusqueda.setNumeroCotizacionEndoso(poliza.getCotizacionDTO().getIdToCotizacion());
		filtrosBusqueda.setPrimerRegistroACargar(0);
		filtrosBusqueda.setNumeroMaximoRegistrosACargar(20);
		
		try {
			listaCotizacionesEndoso = cotizacionEndosoService.buscarCotizacionesEndosoPoliza(filtrosBusqueda);
		} catch (Exception e) {
			LOG.error("Error al buscar los endosos de la p\u00F3liza "+poliza.getNumeroPoliza());
			throw new SystemException("Error al buscar los endosos de la p\u00F3liza "+poliza.getNumeroPoliza());
		}
		return listaCotizacionesEndoso;
	}

	@Override
	public Map<String, String> getListColoniasByCP(String cp, String token)
			throws SystemException {
		validateToken(token);
		Map<String, String> map = new HashMap<String, String>(1);
		
		try {
			map = listadoService.getMapColoniasPorCPMidas(cp);
		} catch(Exception e) {
			LOG.error("Error al buscar las colonias del cp "+cp+"\n"+e.getMessage());
			throw new SystemException("Error al buscar las colonias del cp "+cp);
		}
		
		return map;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Object> creaCotizacion(CotizacionView cotizacionView,
			String token) throws SystemException {
				
		LOG.info(this.getClass().getName()+".creaCotizacion ... entrando a creaCotizacion(simple)");
		
		Agente agente = new Agente();
		Negocio negocio = null;
		NegocioProducto negocioProducto = null;
		NegocioTipoPoliza negocioTipoPoliza = null;
		NegocioSeccion negocioSeccion = null;
		boolean isNewCot = true;
		
		Usuario usuario = this.validarToken(token);
		String user = usuario.getNombreUsuario();
		
		this.validarParametrosCotizacion(cotizacionView);
		
		Integer usuarioExterno = 2;
		if(usuario!=null && usuario.getTipoUsuario()!=null && usuario.getTipoUsuario()>0) {
			usuarioExterno = usuario.getTipoUsuario();
		}
		
		if(usuarioExterno != 1 && cotizacionView != null && cotizacionView.getPaquete() != null && cotizacionView.getPaquete().getObservaciones() != null && StringUtils.isNotEmpty(cotizacionView.getPaquete().getObservaciones()) ) {
			throw new SystemException("Los datos de observaciones no aplica para usuarios externos");
		}
		
		agente = this.validarAgente(cotizacionView.getIdAgente(), usuario);
		negocio = this.validarNegocio(cotizacionView.getDatosPoliza().getIdNegocio());
		negocioProducto = this.validarNegocioProducto(negocio, cotizacionView.getDatosPoliza().getIdProducto());
		negocioTipoPoliza = this.validarNegocioTipoPoliza(cotizacionView.getDatosPoliza().getIdTipoPoliza(), negocioProducto);
		negocioSeccion = this.validarNegocioSeccion(cotizacionView.getVehiculo().getIdLineaNegocio());
		
		LOG.info(this.getClass().getName()+".creaCotizacion ... paso validaciones creaCotizacion(simple)");
		
		CotizacionDTO cotizacion = this.setDatosPoliza(cotizacionView, negocio, negocioTipoPoliza);
		
		cotizacion = this.setDatosCotizacionPaquete(cotizacion, cotizacionView);
		
		IncisoCotizacionDTO incisoCotizacion = this.setIncisoCotizacion(cotizacionView);
		
		if (cotizacionView.getFolio() != null && !cotizacionView.getFolio().isEmpty()){
			isNewCot = evaluaFolioNuevaCotizacion(cotizacionView.getFolio());
		}
		
		LOG.info("Folio: " + cotizacionView.getFolio());
		
		BigDecimal oldValorPrimaTotal = null;
		boolean hasChanges = false;
		
		if (isNewCot) {
			SolicitudDTO solicitud = this.setDatosSolicitud(negocio,agente);
			cotizacion.setSolicitudDTO(solicitud);	
			cotizacion.setTipoCotizacion(CotizacionDTO.TIPO_COTIZACION_AUTOINDIVIDUAL);
			
			try {
				cotizacion = cotizacionService.crearCotizacion(cotizacion, user);
			} catch (Exception e) {
				throw new SystemException("Error al crear la cotizaci\u00F3n "+e);
			}
			
			cotizacion.setFolio(CotizacionDTO.TIPO_COTIZACION_AUTOINDIVIDUAL+"-"+cotizacion.getIdToCotizacion());
			
			try {
				cotizacionService.guardarCotizacion(cotizacion);
			} catch (Exception e) {
				throw new SystemException("Error al guardar la cotizaci\u00F3n");
			}
			
			if (cotizacionView.getContratante() != null && cotizacionView.getContratante().getIdContratante() == 0) {
				if (cotizacionView.getContratante().getCodigoPostal() != null && !cotizacionView.getContratante().getCodigoPostal().isEmpty() 
						&& !"0".equalsIgnoreCase(cotizacionView.getContratante().getCodigoPostal())){
					LOG.info("Antes de guardar el contratante -> cp:"+cotizacionView.getContratante().getCodigoPostal());
					
					this.saveClienteCot(cotizacion.getIdToCotizacion(), cotizacionView.getContratante());
				} else {
					//throw new SystemException("El c\u00F3digo postal es requerido");
				}
			} else if (cotizacionView.getContratante() != null && cotizacionView.getContratante().getIdContratante() > 0) {
				cotizacionService.actualizarDatosContratanteCotizacion(cotizacion.getIdToCotizacion(), new BigDecimal(cotizacionView.getContratante().getIdContratante()), null);
			}
		} else {
			List<CotizacionDTO> cotizacionByFolio = entidadService.findByProperty(CotizacionDTO.class, "folio", cotizacionView.getFolio());
			cotizacion = cotizacionByFolio.get(0);
			//cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
			cotizacion = actualizaDatosCotizacion(cotizacion, agente, negocio, negocioTipoPoliza, cotizacionView, user);
			
			oldValorPrimaTotal = cotizacion.getValorPrimaTotal();
			List<IncisoCotizacionDTO> incisos = incisoService.getIncisos(cotizacion.getIdToCotizacion());
			
			if (incisos != null && incisos.size() > 0){
				hasChanges = this.cambioDatosAuto(cotizacion, incisos, cotizacionView.getDatosPoliza().getIdNegocio(), cotizacionView.getPaquete().getIdPaquete(), 
						new Integer(cotizacionView.getVehiculo().getIdEstilo()+""), cotizacionView.getVehiculo().getModelo().toString(), 
						new Integer(cotizacionView.getVehiculo().getIdMarca()+""), cotizacionView.getZonaCirculacion().getIdEstadoCirculacion(), 
					cotizacionView.getZonaCirculacion().getIdMunicipioCirculacion(), cotizacionView.getPaquete().getPctDescuentoEstado(),
					cotizacionView.getZonaCirculacion().getCodigoPostalCirculacion());
			}
		}
		
		if (negocioSeccion != null) {
			try {
				cotizacion = entidadService.findById(CotizacionDTO.class, cotizacion.getIdToCotizacion());
				
				//Guardar inciso
				incisoCotizacion = this.guardaInciso(cotizacion, negocio, cotizacionView, negocioTipoPoliza, negocioSeccion);
			} catch(SystemException e){
				LOG.error("Error al guardar el inciso:\n"+e.getMessage(), e);
				throw new SystemException(e.getMessage());
			} catch(Exception e){
				LOG.error("Error al guardar el inciso.. "+e.getMessage(), e);
				throw new SystemException("Error al guardar el inciso... "+e.getMessage());
			}
		} else {
			throw new SystemException("El idLineaNegocio proporcionado es incorrecto");
		}
		
		//obtener coberturas para devolver en respuesta
		List<CoberturaCotizacionDTO> coberturaCotizacionList = new LinkedList<CoberturaCotizacionDTO>();
		
		try {
			coberturaCotizacionList = coberturaService.getCoberturas(incisoCotizacion.getIncisoAutoCot().getNegocioPaqueteId(), incisoCotizacion.getId());
		} catch(Exception e){
			LOG.error("--> Error al obtener las coberturas... "+e.getMessage(), e);
		}
		
		//finalizar cotizacion
		boolean riesgosValidos = true;
		boolean requiereDatosAdicionales = false;
		
		if (incisoCotizacion.getId().getNumeroInciso() != null) {	
			riesgosValidos = listadoService.validaDatosRiesgosCompletosCotizacion(
					incisoCotizacion.getId().getIdToCotizacion(), incisoCotizacion.getId().getNumeroInciso());
			
			if (!riesgosValidos) {
				requiereDatosAdicionales = true;
			} else {
				incisoCotizacion = calculoService.calcular(incisoCotizacion);
				
				TerminarCotizacionDTO terminarCotizacionDTO = cotizacionService.terminarCotizacion(cotizacion.getIdToCotizacion(), false);
				//Valida resultado terminar
				cotizacionService.validaTerminarCotizacionWS(terminarCotizacionDTO);
			}
		}
		
		//se crea objeto respuesta
		HashMap<String, Object> map = new HashMap<String, Object>(1);
		List<EsquemaPagoCotizacionDTO> esquemaPagoCotizacionList = new ArrayList<EsquemaPagoCotizacionDTO>(1);
		
		
		Map<String, Object> excResultado = evalueExcepcion(cotizacion);//JFGG
		LOG.info("--> excResultado: " + excResultado);
		if(excResultado!= null && !excResultado.containsKey("statusExcepcion") && "true".equals(String.valueOf(excResultado.get("statusExcepcion")))){//JFGG
			throw new SystemException( String.valueOf(excResultado.get("mensaje")));
		} else {//JFGG
			map.put("RESPUESTA", "EXITOSA");
			map.put("idToCotizacion", cotizacion.getIdToCotizacion());
			map.put("requiereDatosAdicionales", requiereDatosAdicionales);
			
			Map<String, Object> coberturasMap =  new HashMap<String, Object>(1);
			int cont = 1;
			LOG.info("--> coberturaCotizacionList.size:"+coberturaCotizacionList.size());
			for (CoberturaCotizacionDTO cob: coberturaCotizacionList){
				if (new Short("1").equals(cob.getClaveContrato())) {
					String sumaAseguradaStr = "";

					switch (Integer.valueOf(cob.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada())) {
					case 1:
						sumaAseguradaStr = "Valor Comercial";
						break;
					case 2:
						sumaAseguradaStr = "Valor Factura";
						break;
					case 9:
						sumaAseguradaStr = "Valor Convenido";
						break;
					case 3:
						sumaAseguradaStr = "Amparada";
						break;
					case 0:
						sumaAseguradaStr = cob.getValorSumaAsegurada().toString();
						break;
					default:
						break;
					}

					Map<String, Object> cobMap = new HashMap<String, Object>(1);
					cobMap.put("idCobertura", cob.getId().getIdToCobertura());
					cobMap.put("descripcion", cob.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
					cobMap.put("sumaAsegurada", sumaAseguradaStr ); 
					cobMap.put("deducible", cob.getValorDeducible());
					cobMap.put("primaNeta", cob.getValorPrimaNeta()); 

					coberturasMap.put(StringUtils.EMPTY + cont, cobMap);
					cont++;
				}
			}
			
			map.put("coberturas", coberturasMap);
			
			//obotener los esquemas de pago
			ResumenCostosGeneralDTO resumenCostosGeneralDTO = calculoService.obtenerResumenGeneral(incisoCotizacion.getCotizacionDTO());
			
			if(!isNewCot && !hasChanges && BigDecimal.valueOf(resumenCostosGeneralDTO.getCaratula().getPrimaTotal()).compareTo(oldValorPrimaTotal) != 0){
				calculoService.igualarPrima(cotizacion.getIdToCotizacion(), oldValorPrimaTotal.doubleValue(), false);
				cotizacionService.terminarCotizacion(cotizacion.getIdToCotizacion(), false);
				resumenCostosGeneralDTO = calculoService.obtenerResumenGeneral(incisoCotizacion.getCotizacionDTO());
			}
			
			if (resumenCostosGeneralDTO.getCaratula() != null){
				map.put("primaTotal", resumenCostosGeneralDTO.getCaratula().getPrimaTotal());
				map.put("descuentos", resumenCostosGeneralDTO.getCaratula().getDescuentoComisionCedida());
				map.put("primaNeta", resumenCostosGeneralDTO.getCaratula().getPrimaNetaCoberturas());
				map.put("recargo", resumenCostosGeneralDTO.getCaratula().getRecargo());
				map.put("derechoPago", resumenCostosGeneralDTO.getCaratula().getDerechos());
				map.put("iva", resumenCostosGeneralDTO.getCaratula().getIva());
			}
			map.put("folio", cotizacion.getFolio());
			
			try {
				//cotizacion.setFechaInicioVigencia(inicioVigencia);
				//cotizacion.setFechaFinVigencia(finVigencia);
				//cotizacion.setIdFormaPago(BigDecimal.valueOf(idFormaPago));
				cotizacion.setValorTotalPrimas(BigDecimal.valueOf(resumenCostosGeneralDTO.getCaratula().getPrimaNetaCoberturas()));
				cotizacion.setValorDerechosUsuario(resumenCostosGeneralDTO.getCaratula().getDerechos());
				cotizacion.setPorcentajeIva(16d);
				cotizacion.setValorPrimaTotal(BigDecimal.valueOf(resumenCostosGeneralDTO.getCaratula().getPrimaTotal()));
				
				esquemaPagoCotizacionList = cotizadorAgentesService.verEsquemaPagoAgente(cotizacion);
				
				cont = 1;
				Map<String, Object> esquemaPagoMap =  new HashMap<String, Object>(1);
				
				for (EsquemaPagoCotizacionDTO esq: esquemaPagoCotizacionList){
					Map<String, Object> esqMap = new HashMap<String, Object>(1);
					esqMap.put("formaPago", esq.getFormaPagoDTO().getDescripcion());
					esqMap.put("pagoInicial", esq.getPagoInicial().getImporteFormat());
					esqMap.put("noRecibosSubsecuentes", (esq.getPagoSubsecuente() != null && esq.getPagoSubsecuente().getNumExhibicion() != null)? esq.getPagoSubsecuente().getNumExhibicion(): 0);
					esqMap.put("pagoSubsecuente", (esq.getPagoSubsecuente() != null && esq.getPagoSubsecuente().getImporteFormat() != null)? esq.getPagoSubsecuente().getImporteFormat(): 0);
					esquemaPagoMap.put(StringUtils.EMPTY+cont, esqMap);
					cont++;
				}
				
				map.put("esquemaPago", esquemaPagoMap);
			} catch(Exception e){
				LOG.info("--> error al obtener el esquema de pago:\n"+e.getMessage(), e);
			}
		}//JFGG
		return map;
	}
	
	/**
	 * JFGG
	 * @param cotizacionDto
	 * @return
	 */
	public  Map<String, Object> evalueExcepcion (CotizacionDTO cotizacionDto){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean res = false;
		try{
			if(cotizacionDto.getIgualacionNivelCotizacion() == null) {
				cotizacionDto.setIgualacionNivelCotizacion(false);
			}
			if(cotizacionDto.getValorPrimaTotal() == null) {
				cotizacionDto.setValorPrimaTotal(new BigDecimal(0));
			}
			
			List<? extends ExcepcionSuscripcionReporteDTO> listaExcepciones = evaluarCotizacion(cotizacionDto);
			
			//ExcepcionSuscripcion
			if(listaExcepciones != null && !listaExcepciones.isEmpty ()) {
				res = true;
				long idExcepciones = 0;
				for (ExcepcionSuscripcionReporteDTO item : listaExcepciones) {
					if ( idExcepciones == 0 ) { idExcepciones = item.getIdExcepcion (); }
				}
				map.put("mensaje", CotizacionExtService.MensajeExcepcion);
				map.put("idExcepcion", String.valueOf(idExcepciones));
			}
			map.put("statusExpetion", String.valueOf(res));
		} catch(Exception e) {
			LOG.error(this.getClass().getName() + ".EvalueExcepcion.Excepcion: " + e, e);
		}
		return map;
	}
	
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(CotizacionDTO cotizacion){
		ExcepcionSuscripcionNegocioAutosService excepcionSuscripcionNegocioAutosService = new ExcepcionSuscripcionNegocioAutosServiceImpl ();
		return excepcionSuscripcionNegocioAutosService.evaluarCotizacion(cotizacion, null);
	}
	
	/**
	 * 
	 * @param cotizacionView
	 * @param token
	 * @param usuario
	 * @param agente
	 * @param negocio
	 * @throws SystemException
	 */
	private void validarCot(CotizacionView cotizacionView,
			Usuario usuario,
			Agente agente,
			Negocio negocio,
			NegocioTipoPoliza negocioTipoPoliza) throws SystemException{
		
		if ( cotizacionView.getDatosPoliza().getInicioVigencia() == null && 
				cotizacionView.getDatosPoliza().getFinVigencia() == null ){
			cotizacionView.getDatosPoliza().setInicioVigencia(new Date());		
			Calendar fechaC = Calendar.getInstance();
			fechaC.setTime(cotizacionView.getDatosPoliza().getInicioVigencia());		
			fechaC.add(Calendar.MONTH, 12);
			cotizacionView.getDatosPoliza().setFinVigencia(fechaC.getTime());
		}
		
		//validar fechas
		if (cotizacionView.getDatosPoliza().getInicioVigencia() 
				!= null && cotizacionView.getDatosPoliza().getFinVigencia() != null){
			try {
				if ( !this.validarFechas(cotizacionView.getDatosPoliza().getInicioVigencia(),
						cotizacionView.getDatosPoliza().getFinVigencia()) ) {
					throw new SystemException(
							"Ocurri\u00F3 un error en la validacion de fechas");
				}
			} catch (SystemException e) {
				LOG.error("No fue posible validar las fechas"+
						cotizacionView.getDatosPoliza().getInicioVigencia()+
						" "+cotizacionView.getDatosPoliza().getFinVigencia());
				throw new SystemException(e.getMessage());
			}			
		}
		
		//validar forma de pago
		final Map<Integer,String> mapFormasPago = listadoService.getMapFormasdePagoByNegTipoPoliza(
				negocioTipoPoliza.getIdToNegTipoPoliza());
		if (!mapFormasPago.containsKey(Integer.valueOf(cotizacionView.getPaquete().getIdFormaPago()))) {
			throw new SystemException("No se encontro la forma de pago "+
					cotizacionView.getPaquete().getIdFormaPago());
		}
		
		//validar nombre prospecto / contratante
		if (cotizacionView.getDatosPoliza().getProspecto().getNombre() == null){
			throw new SystemException("El nombre del prospecto es requerido.");
		}
		
		if (cotizacionView.getDatosPoliza().getProspecto().getApellidoPaterno() == null){
			throw new SystemException("El apellido paterno del prospecto es requerido.");
		}

		if (cotizacionView.getDatosPoliza().getProspecto().getApellidoMaterno() == null){
			throw new SystemException("El apellido materno del prospecto es requerido.");
		}		
		
		//validar paquete
		if (cotizacionView.getPaquete().getIdPaquete() == null){
			throw new SystemException("El paquete es requerido");
		}
		
		if (cotizacionView.getZonaCirculacion().getIdEstadoCirculacion() == null){
			throw new SystemException("El estado es requerido");
		}
		
		if (cotizacionView.getZonaCirculacion().getIdMunicipioCirculacion() == null){
			throw new SystemException("El municipio es requerido");
		}
		
		if (cotizacionView.getVehiculo().getIdMarca() == null) {
			throw new SystemException("La marca es requerido");
		}
		
		if (cotizacionView.getVehiculo().getModelo() == null ||
				cotizacionView.getVehiculo().getModelo().equals("")) {
			throw new SystemException("El modelo es requerido");
		}
		
		if (cotizacionView.getVehiculo().getIdEstilo() == null ) {
			throw new SystemException("El idEstilo es requerido");
		}
		
	}
	
	/**
	 * 
	 * @param token
	 * @return
	 * @throws SystemException
	 */
	private Usuario validarToken(String token) throws SystemException{
		
		Usuario usuario = null;
		if (token != null && !token.equals("")){
			try {
				usuario = usuarioService.buscarUsuarioRegistrado(token);
			} catch(Exception e){
				LOG.info("No fue posible obtener el usuario actual\n"+e.getMessage());
				throw new SystemException("No fue posible obtener el usuario actual con el token "+token+" dado.");
			}
		} else{
			throw new SystemException("El token de sesion es requerido");
		}
		return usuario;
	}
	
	private boolean validarFechas(Date inicioVigencia, Date finVigencia)
	throws SystemException{
		
		DateTime finicioVigencia = new DateTime(inicioVigencia);
		DateTime ffinVigencia = new DateTime(finVigencia);
		long days = Days.daysBetween(finicioVigencia,ffinVigencia).getDays();
		if (days < 0) {
			throw new SystemException("La fecha de inicio de vigencia no puede ser mayor o igual a la de fin de vigencia");
		}
		return true;
	}
	
	/**
	 * 
	 * @param idAgente
	 * @param usuario
	 * @return
	 * @throws SystemException
	 */
	private Agente validarAgente(Long idAgente, Usuario usuario) throws SystemException{
		
		Agente agente = new Agente();
		if (idAgente != null && idAgente > 0){
			agente.setIdAgente(idAgente);
			try {
				agente = agenteMidasService.findByClaveAgente(agente);
				
				if (agente.getId() == null || agente.getId() == 0){
					throw new SystemException("No fue posible obtener el agente con la clave "+idAgente+" dada.");	
				}
			} catch (Exception e1) {
				LOG.error("No fue posible obtener el agente\n"+e1.getMessage());
				throw new SystemException("No fue posible obtener el agente con la clave "+idAgente+" dada");
			}
		} else{
			try {
				agente = usuarioService.getAgenteUsuarioActual(usuario);
				if (agente == null){
					throw new SystemException("No se encontro agente para el usuario");
				}
			} catch (Exception e){
				LOG.error("No fue posible obtener el agente del usuario "+usuario.getNombreUsuario()+" \n"+e.getMessage());
				throw new SystemException("No fue posible obtener el agente del usuario "+usuario.getNombreUsuario());
			}
		}
		return agente;
	}
	
	/**
	 * 
	 * @param idNegocio
	 * @return
	 * @throws SystemException
	 */
	private Negocio validarNegocio(Long idNegocio) throws SystemException{
		Negocio negocio = null;	
		if (idNegocio != null) {
			negocio = negocioService.findById(idNegocio);
			if (negocio == null){
				throw new SystemException("El id de negocio "+
						idNegocio+
						" proporcionado es incorrecto");
			}
		} else {
			throw new SystemException("El id del negocio es requerido.");
		}		
		return negocio; 
	}
	
	private NegocioProducto validarNegocioProducto(Negocio negocio,
			BigDecimal idToNegProducto) throws SystemException{
		NegocioProducto negocioProducto = null;
		
		if (negocio != null) {
			//se obtiene el negocio producto
			if (idToNegProducto != null) {
				negocioProducto = negocioProductoDao.findById(Long.valueOf(idToNegProducto.toString()));
			} else {
				throw new SystemException("El idProducto es requerido.");	
			}
			
			//se valida que el negocio producto corresponda a Auto Individual
			if (negocioProducto != null) {
				if (!negocioProducto.getProductoDTO().getIdToProducto().equals(ID_PRODUCTO_AUTO_INDIVIDUAL)){
					throw new SystemException("El id de negocio producto No corresponde a Auto Individual");
				}
			} else {
				throw new SystemException("En id de negocio producto "+idToNegProducto+" proporcionado es incorrecto");
			}
		} else {
			throw new SystemException("El negocio proporcionado es incorrecto");
		}

		return negocioProducto;
	}
	
	/***
	 * 	
	 * @param idTipoPoliza
	 * @param negocioProducto
	 * @return
	 * @throws SystemException
	 */
	
	private NegocioTipoPoliza validarNegocioTipoPoliza(BigDecimal idTipoPoliza,
			NegocioProducto negocioProducto) throws SystemException{
		
		NegocioTipoPoliza negocioTipoPoliza = null;		
		try {
			negocioTipoPoliza  = entidadService.findById(NegocioTipoPoliza.class,
					idTipoPoliza);
		} catch(Exception e){
			throw new SystemException("No se pudo obtener el negocioTipoPoliza");
		}
		negocioTipoPoliza.setNegocioProducto(negocioProducto);
		return negocioTipoPoliza;
	}
	
	private NegocioSeccion validarNegocioSeccion (BigDecimal idNegocioSeccion) throws SystemException {
		NegocioSeccion negocioSeccion = null;
		
		try {
			negocioSeccion =  entidadService.findById(NegocioSeccion.class, idNegocioSeccion);
		} catch (Exception e){
			LOG.error("Error al obtener el negocioSeccion\n" + e.getMessage());
			throw new SystemException("Error al validar la l\u00EDnea de negocio");
		}
		
		return negocioSeccion;
	}
	
	//DatosPolizaView
	private CotizacionDTO setDatosPoliza(CotizacionView dto, Negocio negocio, NegocioTipoPoliza negocioTipoPoliza) throws SystemException{
		
		Date inicioVigencia = null;
		Date finVigencia = null;
				
		if ((dto.getDatosPoliza().getInicioVigenciaPoliza() == null || dto.getDatosPoliza().getInicioVigenciaPoliza().equals("")) && 
				(dto.getDatosPoliza().getFinVigenciaPoliza() == null || dto.getDatosPoliza().getFinVigenciaPoliza().equals("")) ) {
			inicioVigencia = new Date();
			Calendar cal = Calendar.getInstance();
			cal.setTime(inicioVigencia);	
			cal.add(Calendar.YEAR, 1);
			finVigencia = cal.getTime();
		} else {
			inicioVigencia = DateUtils.convertirStringToUtilDate(dto.getDatosPoliza().getInicioVigenciaPoliza(), "yyyy-MM-dd HH:mm:ss");
			finVigencia = DateUtils.convertirStringToUtilDate(dto.getDatosPoliza().getFinVigenciaPoliza(), "yyyy-MM-dd HH:mm:ss");
		}
		
		CotizacionDTO cotizacion = new CotizacionDTO();
		cotizacion.setTipoCotizacion(CotizacionDTO.TIPO_COTIZACION_AUTOINDIVIDUAL);
		cotizacion.setFechaInicioVigencia(inicioVigencia);
		cotizacion.setFechaFinVigencia(finVigencia);
		cotizacion.setIdMoneda(new BigDecimal(MonedaDTO.MONEDA_PESOS));
		//cotizacion.setIdFormaPago(BigDecimal.valueOf(dto.getPaquete().getIdFormaPago())); //cotizacion.setPorcentajePagoFraccionado(porcentajePagoFraccionado);
		cotizacion.setPorcentajePagoFraccionado(listadoService.getPctePagoFraccionado(
				dto.getPaquete().getIdFormaPago(), cotizacion.getIdMoneda().shortValue()));
		cotizacion.setPorcentajebonifcomision(negocio.getPctPrimaCeder());
		cotizacion.setNombreContratante( ((dto.getContratante() != null && dto.getContratante().getNombreContratante() != null) ? dto.getContratante().getNombreContratante(): "" ) +" "+
				((dto.getContratante() != null && dto.getContratante().getApellidoPaterno() != null) ? dto.getContratante().getApellidoPaterno(): "") +" "+
				((dto.getContratante() != null && dto.getContratante().getApellidoMaterno() != null) ? dto.getContratante().getApellidoMaterno(): "") );
		cotizacion.setNegocioTipoPoliza(negocioTipoPoliza);
		
		return cotizacion;
	}
	
	//DatosPaqueteView
	private CotizacionDTO setDatosCotizacionPaquete(CotizacionDTO cotizacion, CotizacionView dto) throws SystemException{
		NegocioDerechoPoliza negocioDerechoPoliza = null;
		
		if (dto.getPaquete().getIdDerecho() == 0) {
			try {
				negocioDerechoPoliza = negocioDerechosService.obtenerDechosPolizaDefault(dto.getDatosPoliza().getIdNegocio());
			} catch (Exception e) {
				throw new SystemException("Error al obtener los derechos de la p\u00F3liza para el negocio "+dto.getDatosPoliza().getIdNegocio());
			}
		} else {
			negocioDerechoPoliza = cotizadorAgentesService.getNegocioDerechoPoliza(dto.getDatosPoliza().getIdNegocio());
			//negocioDerechoPoliza.setIdToNegDerechoPoliza(Long.valueOf(dto.getPaquete().getIdDerecho()));
		}
		
		cotizacion.setNegocioDerechoPoliza(negocioDerechoPoliza);
		cotizacion.setIdFormaPago(BigDecimal.valueOf(dto.getPaquete().getIdFormaPago()));
		cotizacion.setPorcentajebonifcomision(Double.valueOf(dto.getPaquete().getComision()));
		cotizacion.setPorcentajeIva(dto.getPaquete().getPorcentajeIva() != 0 ? Double.valueOf(dto.getPaquete().getPorcentajeIva()): 16);
		//cotizacion.setFolio(NUEVOCOTIZADOR);
		//cotizacion.setTipoCotizacion(NUEVOCOTIZADOR);
	    cotizacion.setIdMedioPago(BigDecimal.valueOf(MedioPagoDTO.MEDIO_PAGO_AGENTE));
		return cotizacion;
		
	}
	
	//VehiculoView
	private VehiculoView setDatosVehiculo(CotizacionView dto){
		VehiculoView vehiculoView = new VehiculoView();		
		vehiculoView.setIdNegocioSeccion(dto.getVehiculo().getIdLineaNegocio());
		vehiculoView.setIdTipoUso(dto.getVehiculo().getIdTipoUso());
		vehiculoView.setIdMarcaVehiculo(dto.getVehiculo().getIdMarca());
		vehiculoView.setIdModeloVehiculo(dto.getVehiculo().getModelo().shortValue());
		vehiculoView.setIdEstiloVehiculo(dto.getVehiculo().getIdEstilo().toString());
		vehiculoView.setNumeroSerie(dto.getVehiculo().getNumeroSerieVin());
		
		return vehiculoView;
	}
	
	/**
	 * 
	 * @param idEstilo
	 * @param idNegocioSeccion
	 * @param idEstado
	 * @param idMunicipio
	 * @param idPaquete
	 * @param pctDescuentoEstado
	 * @return
	 */
	private IncisoCotizacionDTO setIncisoCotizacion(CotizacionView dto){
		
		EstiloVehiculoDTO datosEstilo = cotizadorAgentesService.getEstiloVehiculo(
				dto.getVehiculo().getIdEstilo().toString());
		String descripcionFinal = "";
		if(datosEstilo!=null){
			descripcionFinal = datosEstilo.getDescription();
		}
		
		/*if (pctDescuentoEstado == null) {
			pctDescuentoEstado = 0.0;
		}*/
		
		IncisoCotizacionDTO incisoCotizacion = new IncisoCotizacionDTO();
		
		//en caso que no traigan, se pondran los valores defaults
		if (dto.getVehiculo().getIdTipoUso() == null){
			dto.getVehiculo().setIdTipoUso(new BigDecimal(cotizadorAgentesService.getValorParameter(
					ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES,
					ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_TIPOUSO_BASE)) );			
		}
		
		if (dto.getVehiculo().getIdLineaNegocio() == null){
			dto.getVehiculo().setIdLineaNegocio(new BigDecimal(cotizadorAgentesService.getValorParameter(
					ParametroGeneralDTO.GRUPO_PARAM_GENERAL_GENERALES,
					ParametroGeneralDTO.CODIGO_PARAM_GENERAL_COTIZACION_NEGOCIOSECCION_BASE)) );			
		}

		IncisoAutoCot incisoAutoCotDTO = incisoCotizacion.getIncisoAutoCot();
		incisoAutoCotDTO = new IncisoAutoCot();
		incisoAutoCotDTO.setNegocioPaqueteId(dto.getPaquete().getIdPaquete());
		incisoAutoCotDTO.setNegocioSeccionId(dto.getVehiculo().getIdLineaNegocio().longValue());
		incisoAutoCotDTO.setPctDescuentoEstado(dto.getPaquete().getPctDescuentoEstado());
		incisoAutoCotDTO.setTipoUsoId(dto.getVehiculo().getIdTipoUso().longValue());
		incisoAutoCotDTO.setEstadoId(dto.getZonaCirculacion().getIdEstadoCirculacion());
		incisoAutoCotDTO.setMunicipioId(dto.getZonaCirculacion().getIdMunicipioCirculacion());
		incisoAutoCotDTO.setDescripcionFinal(descripcionFinal);
		incisoAutoCotDTO.setModeloVehiculo(
				dto.getVehiculo().getModelo().shortValue());
		incisoAutoCotDTO.setMarcaId(dto.getVehiculo().getIdMarca());
		incisoAutoCotDTO.setNumeroSerie(dto.getVehiculo().getNumeroSerieVin());
		incisoAutoCotDTO.setCodigoPostal(Utilerias.obtenerLong(dto.getZonaCirculacion().getCodigoPostalCirculacion()));
		incisoCotizacion.setIncisoAutoCot(incisoAutoCotDTO);		
		return incisoCotizacion;		
	}
	
	/**
	 * 
	 * @param negocio
	 * @param agente
	 * @return
	 */
	private SolicitudDTO setDatosSolicitud(Negocio negocio,Agente agente){
		SolicitudDTO solicitud = new SolicitudDTO();
		solicitud.setAgente(agente);		
		solicitud.setCodigoAgente(BigDecimal.valueOf(agente.getId()));
		solicitud.setNegocio(negocio);
		return solicitud;
	}
	
	/**
	 * 
	 * @param cotizacionView
	 * @return
	 * @throws SystemException
	 */
	private List<CoberturaCotizacionDTO> validarCoberturas(CotizacionView cotizacionView,
			BigDecimal idToTipoPoliza)
	throws SystemException{
		
		List<CoberturaCotizacionDTO> cobCotListResult = new ArrayList<CoberturaCotizacionDTO>();
		List<CoberturaCotizacionDTO> coberturaCotizacionList = this.getListaCoberturas(cotizacionView,
				idToTipoPoliza);
		Map<BigDecimal, CoberturaView> mCobert = new HashMap<BigDecimal, CoberturaView>();
		 
		List<CoberturaView> coberturaList = cotizacionView.getCoberturas();
		for (CoberturaView item: coberturaList){			
			mCobert.put(item.getIdCobertura(), item);
		}
		
		for (CoberturaCotizacionDTO item: coberturaCotizacionList){			
			CoberturaView cobView = mCobert.get(item.getId().getIdToCobertura());			
			//obligatoriedad
			if ( item.getClaveObligatoriedad() == CoberturaDTO.OBLIGATORIO &&
					!mCobert.containsKey(item.getId().getIdToCobertura()) &&
					!cobView.getContratada() ){
				
				throw new SystemException("La cobertura debe estar contratada: "+
						item.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
			}			
			//sumaasegurada
			if (Integer.valueOf(item.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada())==0){
				
				BigDecimal min = BigDecimal.valueOf(item.getValorSumaAseguradaMin());
				BigDecimal max = BigDecimal.valueOf(item.getValorSumaAseguradaMax());
				BigDecimal sumaAsegurada = new BigDecimal(cobView.getSumaAsegurada().replaceAll(",", ""));
				if ( !((sumaAsegurada.compareTo(min) == 0 ||  sumaAsegurada.compareTo(min) == 1) &&
						(sumaAsegurada.compareTo(max) == 0 || sumaAsegurada.compareTo(max) == -1)) ){
					throw new SystemException("La suma asegurada esta fuera de limite: "+
							item.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
				}
			}			
			//deducible
			List<NegocioDeducibleCob> deducList = item.getDeducibles();
			boolean valido = true;
			if (deducList!=null && deducList.size()>0){
				valido = false;
				for (NegocioDeducibleCob ded: deducList){
					if(cobView.getDeducible().compareTo(ded.getValorDeducible()) == 0){
						valido = true;
						break;
					}
				}				
			}
			if (!valido){
				throw new SystemException("El valor del deducible no es valido "+ 
						cobView.getDeducible() + " Cobertura: " +
						item.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
			}			
			//seteamos los valores 
			item.setValorSumaAsegurada(cobView.getSumaAsegurada());
			item.setClaveContratoBoolean(cobView.getContratada());
			item.setValorDeducible(cobView.getDeducible());
			cobCotListResult.add(item);			
		}
		return cobCotListResult;
	}
	
	/**
	 * @param negocioPaqueteId
	 * @param idIncisoCot
	 * @param coberturaViewList
	 * @return List<CoberturaCotizacionDTO>:
	 * Devuelve la lista de CoberturaCotizacionDTO con los valores validados
	 * del objeto del request (coberturaViewList).
	 * 
	 * Si el objeto del request (coberturaViewList) viene nulo 
	 * entonces devuelve la lista de CoberturaCotizacionDTO con los 
	 * valores por default.
	 * 
	 * @throws SystemException devuelve el mensaje de la cobertura incorrecta
	 */
	private List<CoberturaCotizacionDTO> validarCoberturas(List<CoberturaCotizacionDTO> coberturaCotizacionList,
			List<CoberturaView> coberturaViewList) throws SystemException{
		
		List<CoberturaCotizacionDTO> cobCotListResult = new ArrayList<CoberturaCotizacionDTO>();
		Map<BigDecimal, CoberturaView> mCobert = new HashMap<BigDecimal, CoberturaView>();
		
		for (CoberturaView item: coberturaViewList){			
			mCobert.put(item.getIdCobertura(), item);
		}
		
		for (CoberturaCotizacionDTO item: coberturaCotizacionList){		
			CoberturaView cobView = mCobert.get(item.getId().getIdToCobertura());
			
			//requerida
			if ( item.getClaveObligatoriedad() == CoberturaDTO.OBLIGATORIO ){
				
				if ( cobView == null ) {
					throw new SystemException("La cobertura no se encuentra y debe ser contratada: "+
							item.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial()+
							"("+item.getId().getIdToCobertura()+")");
				} else {
					if ( cobView.getObligatoriedad() !=  CoberturaDTO.OBLIGATORIO ){
						throw new SystemException("La cobertura debe ser contratada: "+
								item.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial()+
								"("+item.getId().getIdToCobertura()+")");
					}
				}
			} else{
				if ( cobView == null ){
					continue;
				}
			}
			
			//sumaasegurada
			if (Integer.valueOf(item.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada()) == 0) {
				BigDecimal min = BigDecimal.valueOf(item.getValorSumaAseguradaMin());
				BigDecimal max = BigDecimal.valueOf(item.getValorSumaAseguradaMax());
				BigDecimal sumaAsegurada = new BigDecimal(cobView.getSumaAsegurada().replaceAll(",", ""));
				
				if ( !((sumaAsegurada.compareTo(min) == 0 ||  sumaAsegurada.compareTo(min) == 1) &&
						(sumaAsegurada.compareTo(max) == 0 || sumaAsegurada.compareTo(max) == -1)) ) {
					throw new SystemException("La suma asegurada esta fuera de limite: "+
							item.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial()+
							"("+item.getId().getIdToCobertura()+"). Valores: "+item.getValorSumaAseguradaMin()+
							"-"+item.getValorSumaAseguradaMax());
				}
			} else {
				cobView.setSumaAsegurada("0");
			}
			
			//deducible
			List<NegocioDeducibleCob> deducList = item.getDeducibles();
			boolean valido = true;
			
			if (deducList!=null && !deducList.isEmpty()) {
				valido = false;
				for (NegocioDeducibleCob ded: deducList) {
					if(cobView.getDeducible().compareTo(ded.getValorDeducible()) == 0){
						valido = true;
						break;
					}
				}				
			}
			
			if (!valido){
				throw new SystemException("El valor del deducible no es valido "+ 
						cobView.getDeducible() + " Cobertura: " +
						item.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial()+
						"("+item.getId().getIdToCobertura()+")");
			}		
			
			//seteamos los valores 
			item.setValorSumaAsegurada( (cobView.getSumaAsegurada() == null || cobView.getSumaAsegurada().isEmpty()) ? 0 : Double.parseDouble(cobView.getSumaAsegurada()));
			item.setClaveContratoBoolean( cobView.getContratada() );
			item.setPorcentajeDeducible(cobView.getDeducible());
			item.setValorDeducible(cobView.getDeducible());
			
			cobCotListResult.add( item );			
		}
		
		return cobCotListResult;
	}
	
	/**
	 * 
	 * @param cotizacionView
	 * @return
	 * @throws SystemException
	 */
	private List<CoberturaCotizacionDTO> getListaCoberturas(CotizacionView cotizacionView,
			BigDecimal idToTipoPoliza)
		throws SystemException{
		
		CotizacionCoberturaView cot = new CotizacionCoberturaView();
		cot.setEstilo(cotizacionView.getVehiculo().getIdEstilo().toString());
		cot.setIdEstadoCirculacion(cotizacionView.getZonaCirculacion().getIdEstadoCirculacion());
		cot.setIdMoneda((short) MonedaDTO.MONEDA_PESOS);
		cot.setIdMunicipioCirculacion(cotizacionView.getZonaCirculacion().getIdMunicipioCirculacion());
		cot.setIdNegocio(cotizacionView.getDatosPoliza().getIdNegocio());
		cot.setIdPaquete(cotizacionView.getPaquete().getIdPaquete());
		cot.setIdToTipoPoliza(idToTipoPoliza);
		cot.setIdToProducto(cotizacionView.getDatosPoliza().getIdProducto());
		cot.setIdToSeccion(cotizacionView.getVehiculo().getIdLineaNegocio());
		cot.setModelo(Long.parseLong(cotizacionView.getVehiculo().getModelo().toString()));
		
		List<CoberturaCotizacionDTO> coberturaCotizacionList = new ArrayList<CoberturaCotizacionDTO>();
		try {
			coberturaCotizacionList = coberturaService.getCoberturas(cot);
		} catch (Exception e) {
			LOG.error("Error al obtener las coberturas\n", e);
			throw new SystemException("Error al obtener las coberturas ");
		}
		return coberturaCotizacionList;
		
	}
	
	@Override
	public List<Object> obtenerCoberturas(CotizacionCoberturaView cot, String token) throws SystemException {
		validateToken(token);
		
		NegocioProducto negocioProducto = null;
		NegocioTipoPoliza negocioTipoPoliza = null;
		NegocioSeccion negocioSeccion = null;
		
		cot.setIdMoneda((short) MonedaDTO.MONEDA_PESOS);
		
		//obteniendo el producto
		try {
			negocioProducto = entidadService.findById(NegocioProducto.class,
				cot.getIdProducto());
				cot.setIdToProducto(negocioProducto.getProductoDTO().getIdToProducto());
		} catch (Exception e) {
			LOG.error("Error al obtener el negocioProducto\n" + e.getMessage());
			throw new SystemException("Error al validar el producto");
		}
		
		//obteniendo el tipoPoliza
		try {
			negocioTipoPoliza  = entidadService.findById(NegocioTipoPoliza.class,
					cot.getIdTipoPoliza());
			cot.setIdToTipoPoliza(negocioTipoPoliza.getTipoPolizaDTO().getIdToTipoPoliza());
		} catch(Exception e){
			LOG.error("Error al obtener el negocioTipoPoliza\n" + e.getMessage());
			throw new SystemException("Error al validar el TipoPoliza");
		}
		
		//obteniendo la sección
		try {
			negocioSeccion =  entidadService.findById(NegocioSeccion.class, cot.getIdLineaNegocio());
			cot.setIdToSeccion(negocioSeccion.getSeccionDTO().getIdToSeccion());
		} catch (Exception e){
			LOG.error("Error al obtener el negocioSeccion\n" + e.getMessage());
			throw new SystemException("Error al validar la l\u00EDnea de negocio");
		}
		
		List<CoberturaCotizacionDTO> coberturaCotizacionList = new LinkedList<CoberturaCotizacionDTO>();
		
		try {
			coberturaCotizacionList = coberturaService.getCoberturas(cot);
		} catch (Exception e) {
			LOG.error("Error al obtener las coberturas\n", e);
			throw new SystemException("Error al obtener las coberturas ");
		}
		
		List<Object> coberturasList = new ArrayList<Object>();
		LOG.info("--> coberturaCotizacionList.size:"+coberturaCotizacionList.size());
		try {
			for (CoberturaCotizacionDTO cob: coberturaCotizacionList){
				String sumaAseguradaStr = "";
				String sumaAseguradaMin = "N/A";
				String sumaAseguradaMax = "N/A";
				
				switch (Integer.valueOf(cob.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada())) {
					case 1:
						sumaAseguradaStr = "Valor Comercial";
						break;
					case 2:
						sumaAseguradaStr = "Valor Factura";
						 break;
					case 9:
						sumaAseguradaStr = "Valor Convenido";
						break;
					case 3:
						sumaAseguradaStr = "Amparada";
						break;
					case 0:
						sumaAseguradaStr = cob.getValorSumaAsegurada().toString();
						sumaAseguradaMin = cob.getValorSumaAseguradaMin().toString();
						sumaAseguradaMax = cob.getValorSumaAseguradaMax().toString();
						break;
					default:
						break;
				}
				
				Map<String, Object> cobMap = new LinkedHashMap<String, Object>(1);
				cobMap.put("idCobertura", cob.getId().getIdToCobertura());
				cobMap.put("obligatoriedad", cob.getClaveObligatoriedad());
				cobMap.put("contratada", cob.getClaveContratoBoolean());
				cobMap.put("descripcion", cob.getCoberturaSeccionDTO().getCoberturaDTO().getNombreComercial());
				cobMap.put("sumaAsegurada", sumaAseguradaStr); 
				cobMap.put("sumaAseguradaMin", sumaAseguradaMin);
				cobMap.put("sumaAseguradaMax", sumaAseguradaMax);
				cobMap.put("claveTipoDeducible", cob.getDescripcionDeducible());
				cobMap.put("deducible", cob.getValorDeducible());
				cobMap.put("claveFuenteSumaAsegurada", cob.getCoberturaSeccionDTO().getCoberturaDTO().getClaveFuenteSumaAsegurada());
				
				List<NegocioDeducibleCob> deducList = cob.getDeducibles();
				List<String> deducLinkList = new LinkedList<String>();
				
				for (NegocioDeducibleCob item: deducList) {
					deducLinkList.add(item.getValorDeducible().toString());
				}
				
				cobMap.put("valoresDeducible", deducLinkList);			
				coberturasList.add(cobMap);
			}
		} catch (Exception e) {
			LOG.error("Error al crear el mapa de coberturas\n", e);
			throw new SystemException("Error al crear el mapa de coberturas ");
		}
		return coberturasList;
	}

	@Override
	public Map<Integer, String> getListMotivoEndoso(MotivosEndosoView params,
			String token) throws SystemException {
		validateToken(token);
		Map<Integer, String> map = new HashMap<Integer, String>(1);
		validateListMotivoEndoso(params, token);
		final Long idPoliza = obtenerIdPoliza(params);
		try {
			map = listadoService.listadoMotivoEndoso(params.getTipoEndoso(), idPoliza);
			
		} catch (Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de motivos de endoso.\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de motivos de endoso");
		}		
		return map;
	}

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public Map<String, Object> cancelarPoliza(EndosoTransporteDTO dto)	
	throws SystemException {
		
		validateToken(dto.getToken());
		SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
    	BigDecimal idToPoliza = null;
		Map<String, Object> result = new HashMap<String, Object>();
		EndosoDTO endoso = null;
    	
    	try {    		
        	LOG.info("Cancelando Poliza " + idToPoliza);
        	PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
        	idToPoliza = poliza.getIdToPoliza();
	        BitemporalCotizacion bitCotizacion = null;
	        BigDecimal idToSolicitud = null;
	        Date fechaIniVigenciaEndoso = format.parse(dto.getFechaInicioVigenciaEndoso());
	        
            bitCotizacion = endosoPolizaService.getCotizacionEndosoCancelacionPoliza(idToPoliza,
            		fechaIniVigenciaEndoso, TipoAccionDTO.getNuevoEndosoCot(), (short)dto.getMotivoEndoso());

            if(bitCotizacion!= null){
                idToSolicitud = bitCotizacion.getValue().getSolicitud().getIdToSolicitud();
            }

            //PrepareCotizacion
            bitCotizacion = endosoPolizaService.prepareBitemporalEndoso(bitCotizacion.getContinuity().getId(), 
            		TimeUtils.getDateTime(fechaIniVigenciaEndoso));

            //Crea Cotizacion
            endosoPolizaService.guardaCotizacionEndosoCancelacionPoliza(bitCotizacion,idToSolicitud,
                    fechaIniVigenciaEndoso);
            
            //prepare emitir
            bitCotizacion = endosoPolizaService.prepareBitemporalEndoso(bitCotizacion.getContinuity().getId(),
                     TimeUtils.getDateTime(fechaIniVigenciaEndoso));
            
            //emitir
            endoso = emisionEndosoBitemporalService.emiteEndoso(bitCotizacion.getContinuity().getId(),
                    TimeUtils.getDateTime(fechaIniVigenciaEndoso),
                    bitCotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());

    		result = this.llenarMap(endoso);
            LOG.info("La poliza idToPoliza => " + idToPoliza + " fue cancelada correctamente");
            
        } catch(NegocioEJBExeption ne){
        	LOG.error("No fue posible cancelar la poliza " + idToPoliza, ne);
        	throw new SystemException("Ocurri\u00F3 un error al cancelar la poliza " + ne.getMessage());
        }catch (Exception e) {
        	LOG.error("No fue posible cancelar la poliza " + idToPoliza, e);
        	throw new SystemException("Ocurri\u00F3 un error al cancelar la poliza " + e.getMessage());
		}
        return result;
	}
	
	private Map<String, Object> llenarMap(EndosoDTO endoso){
		Map<String, Object> result = new HashMap<String, Object>(1);
		
		result.put("idCotizacion", endoso.getIdToCotizacion());
		result.put("fechaInicioVigencia", endoso.getFechaInicioVigencia());
		result.put("fechaFinVigencia", endoso.getFechaFinVigencia());
		result.put("valorPrimaNeta", endoso.getValorPrimaNeta());
		result.put("valorRecargoPagoFrac", endoso.getValorRecargoPagoFrac());
		result.put("valorDerechos", endoso.getValorDerechos());
		result.put("porcentajeBonifComision", endoso.getPorcentajeBonifComision());
		result.put("valorBonifComision", endoso.getValorBonifComision());
		result.put("valorIVA", endoso.getValorIVA());
		result.put("valorPrimaTotal", endoso.getValorPrimaTotal());
		result.put("valorBonifComisionRPF", endoso.getValorBonifComisionRPF());
		result.put("valorComision", endoso.getValorComision());
		result.put("valorComisionRPF", endoso.getValorComisionRPF());
		result.put("tipoCambio", endoso.getTipoCambio());
		result.put("llaveFiscal", endoso.getLlaveFiscal());
		result.put("fechaCreacion", endoso.getFechaCreacion());		
		result.put("descripcionTipoEndoso", Utilerias.calcularDescripcionTipoEndoso(endoso));
		result.put("numeroEndoso", endoso.getId().getNumeroEndoso().intValue());
		
		return result;
		
	}


	@Override
	public Map<BigDecimal, String> getListEndososConRecibosPoliza(
			RecibosEndosoTransporteDTO dto) throws SystemException {
		validateToken(dto.getToken());
		
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		
		try {
			map = impresionRecibosService.getMapEndososRecibos(poliza.getIdToPoliza());
		} catch (Exception e) {
			LOG.error("Error al buscar los endosos con recibo de la p\u00F3liza "+poliza.getNumeroPoliza()+"...\n"+e.getMessage());
			throw new SystemException("Error al buscar los endosos con recibo de la p\u00F3liza "+poliza.getNumeroPoliza());
		}
		
		return map;
	}

	@Override
	public Map<BigDecimal, String> getListProgramasPagosEndosoPoliza(
			RecibosEndosoTransporteDTO dto) throws SystemException {
		validateToken(dto.getToken());
		
		BigDecimal numeroEndoso = dto.getNumeroEndoso();
		
		Map<BigDecimal, String> map = new LinkedHashMap<BigDecimal, String>();
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		
		try {
			map = impresionRecibosService.getMapProgramaPagos(poliza.getIdToPoliza(), numeroEndoso);
		} catch (Exception e) {
			LOG.error("Error al buscar los programas de pago de la p\u00F3liza "+poliza.getNumeroPoliza()+"...\n"+e.getMessage());
			throw new SystemException("Error al buscar los programas de pago de la p\u00F3liza "+poliza.getNumeroPoliza());
		}
		
		return map;
	}

	@Override
	public String getIncisosProgPago(RecibosEndosoTransporteDTO dto)
			throws SystemException {
		validateToken(dto.getToken());
		
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		
		String inciso = "";
		
		try {
			inciso = listadoService.getIncisosProgPago(poliza.getIdToPoliza(), dto.getIdProgramaPago());
		} catch (Exception e) {
			LOG.error("Error al buscar los incisos de la p\u00F3liza "+poliza.getNumeroPoliza()+" y del programa de pago "+dto.getIdProgramaPago()+"...\n"+e.getMessage());
			throw new SystemException("Error al buscar los incisos de la p\u00F3liza "+poliza.getNumeroPoliza()+" y del programa de pago "+dto.getIdProgramaPago());
		}
		
		return inciso;
	}

	@Override
	public List<ReciboSeycos> getRecibosPoliza(RecibosEndosoTransporteDTO dto)
			throws SystemException {
		validateToken(dto.getToken());
		
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		
		List<ReciboSeycos> recibosList = new ArrayList<ReciboSeycos>(1);
		
		try {
			recibosList = impresionRecibosService.listarRecibosPolizaInciso(poliza.getIdToPoliza(), dto.getIdProgramaPago(), dto.getIdInciso());
		} catch (Exception e) {
			LOG.error("Error al obtener los recibos de la p\u00F3liza "+poliza.getNumeroPoliza()+"...\n"+e.getMessage());
			throw new SystemException("Error al obtener los recibos de la p\u00F3liza "+poliza.getNumeroPoliza());
		}
		
		return recibosList;
	}

	@Override
	public byte[] imprimirRecibo(RecibosEndosoTransporteDTO dto)
			throws SystemException {
		validateToken(dto.getToken());
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		byte[] recibo = null;
		
		if(poliza.getIdToPoliza() != null && dto.getIdRecibo() != null){
			try{
				String llaveFiscal = impresionRecibosService.getLlaveFiscalRecibo(poliza.getIdToPoliza(), dto.getIdRecibo());
				if(llaveFiscal == null){
					LOG.error("No se pudo obtener la llaver fiscal del recibo "+dto.getIdRecibo()+" de la p\u00F3liza "+poliza.getNumeroPoliza()+"...");
					throw new SystemException("No se pudo obtener la llaver fiscal del recibo "+dto.getIdRecibo()+" de la p\u00F3liza "+poliza.getNumeroPoliza());
				}
				
				recibo = impresionRecibosService.getReciboFiscal(llaveFiscal);
			}catch(Exception e){
				LOG.error("Error al imprimir el recibo "+dto.getIdRecibo()+" de la p\u00F3liza "+poliza.getNumeroPoliza()+"...\n"+e.getMessage());
				throw new SystemException("Error al imprimir el recibo "+dto.getIdRecibo()+" de la p\u00F3liza "+poliza.getNumeroPoliza());
			}
		}
		
		return recibo;
	}

	@Override
	public List<MovimientoReciboDTO> getMovimientosRecibosPoliza (
			RecibosEndosoTransporteDTO dto) throws SystemException {
		validateToken(dto.getToken());
		
		PolizaDTO poliza = validatePoliza(dto.getNumeroPoliza());
		
		List<MovimientoReciboDTO> list = new ArrayList<MovimientoReciboDTO>(1);
		
		try {
			list = impresionRecibosService.getMovimientosReciboInciso(poliza.getIdToPoliza(), dto.getIdRecibo(), dto.getIdInciso());
		} catch (Exception e) {
			LOG.error("Error al obtener los movimientos del recibo "+dto.getIdRecibo()+" de la p\u00F3liza "+poliza.getNumeroPoliza()+" y el inciso "+dto.getIdInciso()+"...\n"+e.getMessage());
			throw new SystemException("Error al obtener los movimientos del recibo "+dto.getIdRecibo()+" de la p\u00F3liza "+poliza.getNumeroPoliza()+" y el inciso "+dto.getIdInciso());
		}
		
		return list;
	}
	
	private void validateListMotivoEndoso(MotivosEndosoView params, String token) throws SystemException {
		validateToken(token);
		if (params == null) {
			throw new SystemException("Params no puede ser null");
		}
		if (params.getIdPoliza() == null && StringUtils.isEmpty(params.getNumeroPoliza())) {
			throw new SystemException("Falta el idToPoliza o el numeroPoliza para poder traer la lista");
		}
		if (params.getTipoEndoso() == null){
			throw new SystemException("Falta el tipoEndoso para poder traer la lista");
		}
	}
	
	public Long obtenerIdPoliza(MotivosEndosoView params) {
		if(params.getIdPoliza() == null) {
			NumeroPolizaCompleto numeroPolizaCompletoMidas = NumeroPolizaCompletoMidas
					.parseNumeroPolizaCompleto(params.getNumeroPoliza());
			PolizaDTO poliza = polizaFacadeRemote.find(numeroPolizaCompletoMidas);
			return poliza.getIdToPoliza().longValue();
		} else {
			return params.getIdPoliza().longValue();
		}
	}
	
	@Override
	public Map<Integer, String> getListTiposEndoso(String token) throws SystemException{
		List<CatalogoValorFijoDTO> tiposEndoso = new ArrayList<CatalogoValorFijoDTO>(1);
		Map<Integer, String> map = new LinkedHashMap<Integer, String>();
		
		validateToken( token );
		
		Map<String,Object> parametros = new HashMap<String,Object>();
		parametros.put("id.idGrupoValores", CatalogoValorFijoDTO.IDGRUPO_CVES_TIPOS_ENDOSO);
		
		StringBuilder queryWhere = new StringBuilder();
		queryWhere.append(" and model.id.idDato IN ("+SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS+", "+
				SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_CONDUCTO_COBRO+", "+
				SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_FORMA_PAGO+", "+SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA+") ");
		
		try {
			tiposEndoso = entidadService.findByColumnsAndProperties(CatalogoValorFijoDTO.class, "id,descripcion", 
					parametros, new HashMap<String,Object>(), queryWhere, null);
		} catch(Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener la lista de paquetes\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de paquetes");
		}
		
		for (CatalogoValorFijoDTO dto: tiposEndoso) {
			map.put(dto.getId().getIdDato(), dto.getDescripcion());
		}
		
		return map;
	}
	
	@Override
	public Map<Integer, String> getListFormasPagoDePoliza(EndosoTransporteDTO dto) throws SystemException {
		if (dto.getNumeroPoliza() == null){
			throw new SystemException("El n\u00FAmero de  p\u00F3liza es requerido");
		}
		
		validateToken(dto.getToken());
		
		NumeroPolizaCompleto numeroPolizaCompletoMidas = NumeroPolizaCompletoMidas
				.parseNumeroPolizaCompleto(dto.getNumeroPoliza());
		
		PolizaDTO poliza = null;
		
		try {
			poliza = polizaFacadeRemote.find(numeroPolizaCompletoMidas);
		} catch (Exception e) {
			throw new SystemException("No fue posible obtener le número de p\u00F3liza");
		}
		
		try {
			return listadoService.getMapFormasdePago(poliza.getCotizacionDTO());
		} catch(Exception e){
			throw new SystemException("Ocurri\u00F3 un error al obtener la lista de formas de pago");
		}
		
	}
	
	private CotizacionView populate(Long idNegocio, BigDecimal idProducto, BigDecimal idTipoPoliza, 
			BigDecimal idEstilo, BigDecimal idToNegSeccion, BigDecimal modelo, BigDecimal idMarca,
			Long idPaquete, int idFormaPago, String idEstadoCirculacion, String idMunicipioCirculacion, 
			Double descuento, String nombreAsegurado,String apellidoPaterno,String apellidoMaterno, String rfc,
			String claveSexo, String claveEstadoCivil, String idEstadoNacimiento, Date fechaNacimiento, String codigoPostal, 
			String idCiudad, String idEstado, String idColonia, String nombreColonia, String calleNumero, String telefonoCasa, String telefonoOficina, 
			String email, Long idAgente, String observaciones, BigDecimal idCliente, List<CoberturaView> coberturaViewList, 
			Date inicioVigencia, Date finVigencia, String folio) throws SystemException {
		CotizacionView dto = new CotizacionView();
		
		DatosPolizaView datosPoliza = new DatosPolizaView();
		datosPoliza.setIdNegocio(idNegocio);
		datosPoliza.setIdProducto(idProducto);
		datosPoliza.setIdTipoPoliza(idTipoPoliza);
		
		DatosVehiculoView vehiculo = new DatosVehiculoView();
		vehiculo.setIdEstilo(idEstilo);
		vehiculo.setIdLineaNegocio(idToNegSeccion);
		vehiculo.setModelo(modelo);
		vehiculo.setIdMarca(idMarca);
		
		DatosPaqueteView paquete = new DatosPaqueteView();
		paquete.setIdPaquete(idPaquete);
		paquete.setIdFormaPago(idFormaPago);
		paquete.setPctDescuentoEstado( (descuento != null)? descuento: 0.0);
		
		ZonaView zonaCirculacion = new ZonaView();
		zonaCirculacion.setIdEstadoCirculacion(idEstadoCirculacion);
		zonaCirculacion.setIdMunicipioCirculacion(idMunicipioCirculacion);
		
		ContratanteView contratante = new ContratanteView();
		contratante.setIdContratante(idCliente != null ? Integer.valueOf(idCliente+""): 0);
		contratante.setNombreContratante(nombreAsegurado != null? nombreAsegurado: "");
		contratante.setApellidoPaterno(apellidoPaterno != null? apellidoPaterno: "");
		contratante.setApellidoMaterno(apellidoMaterno != null? apellidoMaterno: "");
		contratante.setRfc(rfc != null ? rfc: "");
		contratante.setClaveSexo(claveSexo != null && !claveSexo.isEmpty() ? claveSexo.charAt(0): 'M');
		contratante.setClaveEstadoCivil(claveEstadoCivil != null && !claveEstadoCivil.isEmpty() ? claveEstadoCivil.charAt(0): 'S');
		contratante.setIdEstadoNacimiento(idEstadoNacimiento != null? idEstadoNacimiento: "");
		contratante.setCalle(calleNumero != null ? calleNumero : "");
		contratante.setCodigoPostal(codigoPostal != null? codigoPostal: "");
		contratante.setClaveColonia(idColonia != null ? idColonia: "");
		contratante.setNombreColonia(nombreColonia != null ? nombreColonia: "");
		contratante.setIdCiudad(idCiudad != null ? idCiudad: "");
		contratante.setIdEstado(idEstado != null ? idEstado: "");
		contratante.setTelefonoCasa(telefonoCasa != null? telefonoCasa: "");
		contratante.setTelefonoOficina(telefonoOficina != null ? telefonoOficina: "");
		contratante.setEmail(email != null ? email: "");
		
		if (rfc != null && UtileriasWeb.validarRfcPersonaMoral(rfc)) {
			//datos para persona moral
			String razonSocial = "";
			String representanteLegal = "";
			
			if (UtileriasWeb.esCadenaVacia(nombreAsegurado)) {
				throw new SystemException("La raz\u00F3n social es requerido");
			}
			
			String[] datos = nombreAsegurado.split("_");
			
			if (datos.length == 1) {
				razonSocial = nombreAsegurado;
				representanteLegal = nombreAsegurado;
			} else if (datos.length > 1) {
				razonSocial = datos[0];
				representanteLegal = datos[1];
			}
			
			contratante.setNombreRazonSocial(razonSocial);
			contratante.setRepresentanteLegal(representanteLegal);
			contratante.setFechaDeConstitucion(fechaNacimiento != null ? fechaNacimiento: new Date());
		}
		
		dto.setDatosPoliza(datosPoliza);
		dto.setVehiculo(vehiculo);
		dto.setPaquete(paquete);
		dto.setZonaCirculacion(zonaCirculacion);
		dto.setContratante(contratante);
		
		dto.setFolio(folio);
		dto.setCoberturas(coberturaViewList);
		
		ConductorView conductor = new ConductorView();
		conductor.setNombreConductor(StringUtils.isBlank(nombreAsegurado) ? "." : nombreAsegurado);
		conductor.setApellidoPaternoConductor(StringUtils.isBlank(apellidoPaterno) ? "." : apellidoPaterno);
		conductor.setApellidoMaternoConductor(StringUtils.isBlank(apellidoMaterno) ?"." : apellidoMaterno);
		dto.setConductor(conductor);
		
		return dto;
	}
	
	private Respuesta validarParametrosCotizacion(CotizacionView dto) throws SystemException {
		Respuesta respuesta = new Respuesta();
		
		if (dto.getDatosPoliza() != null) {
			if (dto.getDatosPoliza().getIdNegocio() == null) {
				throw new SystemException("El id del negocio es requerido.");
			}
			
			if (dto.getDatosPoliza().getIdProducto() == null) {
				throw new SystemException("El idProducto es requerido.");
			}
			
			if (dto.getDatosPoliza().getIdTipoPoliza() == null) {
				throw new SystemException("El idTipoPoliza es requerido");
			}
		} else {
			throw new SystemException("Los datos de la p\u00F3liza (idNegocio, idProducto y idTipoPoliza) son requeridos");
		}
		
		if (dto.getPaquete() != null) {
			if (dto.getPaquete().getIdPaquete() == null) {
				throw new SystemException("El paquete es requerido");
			}
			
			if (dto.getPaquete().getIdFormaPago() == 0) {
				throw new SystemException("La forma de pago es requerido");
			}
		} else {
			throw new SystemException("Los datos del paquete (idPaquete y idFormaPago) son requeridos");
		}
		
		if (dto.getVehiculo() != null) {
			if (dto.getVehiculo().getIdEstilo() == null) {
				throw new SystemException("El estilo es requerido");
			}
			
			if (dto.getVehiculo().getIdLineaNegocio() == null) {
				throw new SystemException("El idLineaNegocio es requerido");
			}
			
			if (dto.getVehiculo().getModelo() == null || dto.getVehiculo().getModelo().equals("")) {
				throw new SystemException("El modelo es requerido");
			}
			
			if (dto.getVehiculo().getIdMarca() == null || dto.getVehiculo().getIdMarca().equals("")) {
				throw new SystemException("La marca es requerido");
			}
		} else {
			throw new SystemException("Los datos del vehículo (idEstilo, idLineaNegocio, modelo y idMarca) son requeridos");
		}
		
		if (dto.getZonaCirculacion() != null) {
			if (dto.getZonaCirculacion().getIdEstadoCirculacion() == null || dto.getZonaCirculacion().getIdEstadoCirculacion().equals("")) { 
				throw new SystemException("El estado es requerido");
			}
			
			if (dto.getZonaCirculacion().getIdMunicipioCirculacion() == null || dto.getZonaCirculacion().getIdMunicipioCirculacion().equals("")) {
				throw new SystemException("El municipio es requerido");
			}
		} else {
			throw new SystemException("Los datos de la zona de circulación son requeridos");
		}
		
		this.validarEstadoMunicipio(dto.getDatosPoliza().getIdNegocio(), dto.getZonaCirculacion().getIdEstadoCirculacion(), 
				dto.getZonaCirculacion().getIdMunicipioCirculacion(), dto.getZonaCirculacion().getCodigoPostalCirculacion());
		
		return respuesta;
	}
	
	@Override
	public Map<String, String> getMunicipioByCP(String codigoPostal, String token) throws SystemException {
		Map<String, String> map = new LinkedHashMap<String, String>();
		validateToken( token );
		CiudadDTO ciudad = null;
		
		if (codigoPostal == null || "".equals(codigoPostal)) {
			throw new SystemException("El CP es requerido");
		}
		
		try {
			ciudad = listadoService.getMunicipioPorCp(codigoPostal);
			map.put(ciudad.getCityId(),ciudad.getCityName());
		} catch(Exception e){
			LOG.error("Ocurri\u00F3 un error al obtener el municipio del CP "+codigoPostal+"\n"+e.getMessage());
			throw new SystemException("Ocurri\u00F3 un error al obtener el municipio del CP "+codigoPostal);
		}

		return map;
	}
	
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Override
	public Map<String, String> emitirPoliza(EmisionView emisionView, String token) throws SystemException {
		validateToken( token );
    	validarParametrosEmision(emisionView);
    	CotizacionDTO cotizacion = null;
    	
    	if (emisionView.getIdCotizacion() != null){
			cotizacion = entidadService.findById(CotizacionDTO.class, emisionView.getIdCotizacion());
			
	    	if (cotizacion != null && cotizacion.getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_EMITIDA)) {
	    		throw new SystemException("La cotizaci\u00F3n ya ha sido emitida");
	    	}
    	} else{
			throw new SystemException("Favor de especificar el id de cotizaci\u00F3n");
    	}
    	
    	//Si el medioPago es distinto de Efectivo, se guardan los datos de cobranza y se crea el conducto de cobro. 
		if (emisionView.getConductoCobro().getIdMedioPago() != MEDIO_PAGO_EFECTIVO){
			try {
				guardarCobranza(emisionView.getIdCotizacion(), emisionView.getConductoCobro(), false, token);
				cotizacion = entidadService.findById(CotizacionDTO.class, emisionView.getIdCotizacion());
			} catch (Exception e) {
				LOG.error("Ocurri\u00F3 un error al guardar los datos de cobranza de la cotizaci\u00F3n " + cotizacion.getIdToCotizacion()+ " " + e.getMessage());
				throw new SystemException("Ocurri\u00F3 un error al guardar los datos de cobranza. "+e.getMessage(), e);
			}
		}
    			
		Map<String, String> result = new HashMap<String, String>();
        
		//si en la cotización no se guardó el cliente o si se guardó pero se quiere actualizar
    	if (emisionView.getContratante() != null) {
    		if (emisionView.getContratante().getIdContratante() == 0) {
				if (emisionView.getContratante().getCodigoPostal() != null && !emisionView.getContratante().getCodigoPostal().isEmpty() 
						&& !"0".equalsIgnoreCase(emisionView.getContratante().getCodigoPostal())){
					LOG.info("Antes de guardar el contratante en Emisi\u00F3n AI -> cp:"+ emisionView.getContratante().getCodigoPostal());
					
					this.saveClienteCot(cotizacion.getIdToCotizacion(), emisionView.getContratante());
				} else {
					throw new SystemException("El c\u00F3digo postal es requerido");
				}
			} else if (emisionView.getContratante().getIdContratante() > 0) {
				cotizacionService.actualizarDatosContratanteCotizacion(cotizacion.getIdToCotizacion(), new BigDecimal(emisionView.getContratante().getIdContratante()), null);
			}
    	} else {
    		if (cotizacion.getIdToPersonaContratante() == null) {
    			throw new SystemException("Contratante no guardado en cotizaci\u00F3n, Favor de agregar un contratante en la emisi\u00F3n");
    		}
    	}
		
        cotizacion = entidadService.findById(CotizacionDTO.class, emisionView.getIdCotizacion());
		IncisoCotizacionDTO incisoCot = cotizacion.getIncisoCotizacionDTOs().get(0);
		IncisoAutoCot incisoAuto = incisoCot.getIncisoAutoCot();
		
		//si en la cotización no se guardó el conductor ó si se guardó pero se quiere actualizar
		if (emisionView.getAutoInciso().getConductor() != null) {
			incisoAuto.setNombreConductor(emisionView.getAutoInciso().getConductor().getNombreConductor() != null ? emisionView.getAutoInciso().getConductor().getNombreConductor(): "");
			incisoAuto.setPaternoConductor(emisionView.getAutoInciso().getConductor().getApellidoPaternoConductor() != null ? emisionView.getAutoInciso().getConductor().getApellidoPaternoConductor(): "");
			incisoAuto.setMaternoConductor(emisionView.getAutoInciso().getConductor().getApellidoMaternoConductor() != null ? emisionView.getAutoInciso().getConductor().getApellidoMaternoConductor(): "");
			incisoAuto.setNumeroLicencia(emisionView.getAutoInciso().getConductor().getNumeroLicencia() != null ? emisionView.getAutoInciso().getConductor().getNumeroLicencia(): "");
			incisoAuto.setFechaNacConductor((emisionView.getAutoInciso().getConductor().getFechaNacimiento() != null && !emisionView.getAutoInciso().getConductor().getFechaNacimiento().isEmpty())? DateUtils.convertirStringToUtilDate(emisionView.getAutoInciso().getConductor().getFechaNacimiento(), "yyyy-MM-dd HH:mm:ss"): new Date());
			incisoAuto.setOcupacionConductor(emisionView.getAutoInciso().getConductor().getOcupacion() != null ? emisionView.getAutoInciso().getConductor().getOcupacion(): "");
		} else {
			if (incisoAuto.getNombreConductor() == null || incisoAuto.getNombreConductor().isEmpty()) {
				throw new SystemException("Favor de especificar los datos del conductor");
			}
		}
		
		incisoAuto.setNumeroMotor(StringUtils.isBlank(emisionView.getAutoInciso().getNumeroMotor()) ? "S/N" : emisionView.getAutoInciso().getNumeroMotor());
		incisoAuto.setNumeroSerie(emisionView.getAutoInciso().getNumeroSerie());
		incisoAuto.setPlaca(StringUtils.isBlank(emisionView.getAutoInciso().getPlaca()) ? "PEND" : emisionView.getAutoInciso().getPlaca());	
		incisoAuto.setObservacionesinciso(StringUtils.isBlank(emisionView.getAutoInciso().getObservacionesInciso()) ?incisoAuto.getObservacionesinciso():(incisoAuto.getObservacionesinciso() != null) ?incisoAuto.getObservacionesinciso() + ". " + emisionView.getAutoInciso().getObservacionesInciso():emisionView.getAutoInciso().getObservacionesInciso());
		
		if(StringUtils.isEmpty(emisionView.getAutoInciso().getNombreAsegurado())) {
			incisoAuto.setPersonaAseguradoId(cotizacion.getIdToPersonaContratante().longValue());
			incisoAuto.setNombreAsegurado(cotizacion.getNombreContratante());
		} else {
			incisoAuto.setNombreAsegurado(emisionView.getAutoInciso().getNombreAsegurado());
		}
		
		incisoCot.setEmailContacto(StringUtils.isBlank(emisionView.getAutoInciso().getEmailContacto()) ? "": emisionView.getAutoInciso().getEmailContacto());
		
		try {
			incisoService.guardar(incisoCot);
		} catch (Exception e) {
			throw new SystemException("Error al guardar el email de contacto");
		}
		
		try {
			incisoService.guardarIncisoAutoCot(incisoAuto);
		} catch (Exception e){
			throw new SystemException("Error al guardar el Inciso Auto Cot. ", e);
		}
		
		if (emisionView.getConductoCobro().getIdMedioPago() != MEDIO_PAGO_EFECTIVO && 
				emisionView.getConductoCobro().getIdMedioPago() != MEDIO_PAGO_TC && 
				emisionView.getConductoCobro().getIdMedioPago() != MEDIO_PAGO_DOMICILIACION && 
				emisionView.getConductoCobro().getIdMedioPago() != CuentaPagoDTO.TipoCobro.CUENTA_AFIRME.valor()) {
			throw new SystemException("Favor de especificar un medio de pago v\u00E1lido");
		}
		
		cotizacion.setIdMedioPago(BigDecimal.valueOf(emisionView.getConductoCobro().getIdMedioPago()));
		
		//Valida forma de pago
		if (emisionView.getPaquete() != null && emisionView.getPaquete().getIdFormaPago() > 0) {
			final Map<Integer,String> mapFormasPago = listadoService.getMapFormasdePago(cotizacion);
			if (!mapFormasPago.containsKey(Integer.valueOf(emisionView.getPaquete().getIdFormaPago()))) {
				throw new SystemException("No se encontr\u00F3 la forma de pago " + emisionView.getPaquete().getIdFormaPago());
			}
			
			cotizacion.setIdFormaPago(BigDecimal.valueOf(emisionView.getPaquete().getIdFormaPago()));
		}
		
		try {
			entidadService.save(cotizacion);
			LOG.info("despu\u00E9s de actualizar el idFormaPago y el idMedioPago de la cotizaci\u00F3n ...");
		} catch (Exception e) {
			LOG.error("Error al actualizar el idMedioPago y el idFormaPago para la cotizaci\u00F3n "+cotizacion.getIdToCotizacion()+" . "+e.getMessage());
            throw new SystemException("Error al actualizar el idMedioPago y el idFormaPago para la cotizaci\u00F3n "+cotizacion.getIdToCotizacion());
		}
		
        StringBuilder errores = validaEmisionCotizacion(cotizacion.getIdToCotizacion(), emisionView.isDocumentacionCompleta());
        
		if (errores != null && !errores.toString().isEmpty()) {
			throw new SystemException("Errores: " + errores);
		}
		
		try{
			result = emisionService.emitir(cotizacion, false);
		} catch (Exception e){
			throw new SystemException("Error al emitir: "+e.getMessage(), e);
		}
		
		return result;
	}
	
	private boolean validarEstadoMunicipio (Long idNegocio, String idEstadoCirculacion, String idMunicipioCirculacion,
			String codigoPostalCirculacion)throws SystemException {
		try{
			HashMap<String,Object> propNeg = new HashMap<String,Object>();
			propNeg.put("negocio.idToNegocio",idNegocio);
			List<NegocioEstado> negEstadoList = entidadService.findByProperties(NegocioEstado.class, propNeg);
			
			if(negEstadoList != null && !negEstadoList.isEmpty()){
				HashMap<String,Object> properties = new HashMap<String,Object>();
				properties.put("estadoDTO.stateId",idEstadoCirculacion);
				properties.put("negocio.idToNegocio",idNegocio);
				List<NegocioEstado> negocioEstadoList = entidadService.findByProperties(NegocioEstado.class, properties);
				
				if(negocioEstadoList == null || negocioEstadoList.isEmpty()){
					throw new SystemException("Estado no valido ("+idEstadoCirculacion+")");
				}

				if(codigoPostalCirculacion!=null && !codigoPostalCirculacion.isEmpty()){
					int idEstadoin=Integer.parseInt(idEstadoCirculacion)/1000;
					String resp =estadoFacadeRemote.validarCodigoPostal(codigoPostalCirculacion,idEstadoin);
					if(resp.equals("0")){
						throw new SystemException("Codigo postal de circulacion  no valido ("+codigoPostalCirculacion+")");						
					}
				}
				
				HashMap<String,Object> propertiesCity = new HashMap<String,Object>();
				propertiesCity.put("stateId",idEstadoCirculacion);
				propertiesCity.put("municipalityId",idMunicipioCirculacion);
				List<MunicipioDTO> municipioList = entidadService.findByProperties(MunicipioDTO.class, propertiesCity);
				
				if(municipioList == null || municipioList.isEmpty()){
					throw new SystemException("Municipio no valido ("+idMunicipioCirculacion+")");
				}
			}
		} catch(Exception e){
			throw new SystemException(e.getMessage()!=null?e.getMessage():"Fallo al validar el estado-municipio");
		}
		
		return true;
	}
	
	private void validarParametrosEmision(EmisionView dto) throws SystemException {
		if (dto.getIdCotizacion() == null) {
			throw new SystemException("El n\u00FAmero de cotizaci\u00F3n es requerido");
		}
		
		if (dto.getConductoCobro() == null || dto.getConductoCobro().getIdMedioPago() == null || dto.getConductoCobro().getIdMedioPago() == 0){
    		throw new SystemException("El medio de pago es requerido");
    	}
		
		if (dto.getAutoInciso() == null) {
			throw new SystemException("Favor de especificar los datos del inciso (motor, serie, placa, etc)");
		} else {
			if (dto.getAutoInciso().getNumeroMotor() == null || dto.getAutoInciso().getNumeroMotor().isEmpty()) {
				throw new SystemException("el n\u00FAmero de motor es requerido");
			}
			
			if (dto.getAutoInciso().getNumeroSerie() == null || dto.getAutoInciso().getNumeroSerie().isEmpty()) {
				throw new SystemException("el n\u00FAmero de serie es requerido");
			}
			
			if (dto.getAutoInciso().getPlaca() == null || dto.getAutoInciso().getPlaca().isEmpty()) {
				throw new SystemException("La placa es requerido");
			}
		}
	}

	@Override
	public byte[] imprimirCotizacion(ParametrosImpresionCotizacionView params, String token) throws SystemException {
		validateToken(token);
		
		if (params == null) {
			throw new SystemException("Params no puede ser null");
		}
		
		if (params.getIdToCotizacion() == null) {
			throw new SystemException("El id de la cotización es requerido para poder imprimir");
		}
		
		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, params.getIdToCotizacion());
		
		if (cotizacion == null) {
			throw new SystemException("No se pudo encontrar la cotización "+params.getIdToCotizacion());
		}
		
		try{
			TransporteImpresionDTO transporte = null;
			Locale locale = new Locale("es", "MX");
			
			if (!params.isTodosLosIncisos()){
				
				if (params.getIncisoInicial() < 1) {
					throw new SystemException("El inciso inicial es requerido");
				}
				
				if (params.getIncisoFinal() < 1) {
					throw new SystemException("El inciso final es requerido");
				}
				
				transporte = generarPlantillaReporte.imprimirCotizacion(
						params.getIdToCotizacion(),
						locale,
						params.isCaratula(),
						params.getIncisoInicial(),
						params.getIncisoFinal(),
						false);
			} else {
				transporte = generarPlantillaReporte.imprimirCotizacion(params.getIdToCotizacion(),
						locale,
						params.isCaratula(),
						true,
						false);
			}
			
			return transporte.getByteArray();				
		}catch (Exception e) {
			LOG.info("Ocurri\u00F3 un error al intentar imprimir la cotizaci\u00F3n: \n" + e.getMessage());
			return "".getBytes();
		}	
	}
	
	@Override
	public CotizarEndosoFlotillaResponse cotizarEndosoFlotilla(CotizarEndosoFlotillaRequest request, String token)
			 throws SystemException {
		validarToken(token);
		CotizarEndosoFlotillaResponse response = new CotizarEndosoFlotillaResponse();
		try {
			final PolizaDTO poliza = validatePoliza(request.getNumeroPoliza());
			if(SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO == request.getTipoEndoso()) {
				try {
					try {
						endosoService.getCotizacionEndosoAltaIncisoTrxNew(poliza.getIdToPoliza(), request.getFechaInicioVigencia(),TipoAccionDTO.getNuevoEndosoCot());
					} catch (NegocioEJBExeption e) {
						if("NE_6".equals(e.getErrorCode()) && request.isForzar()) {
							BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME,
									poliza.getCotizacionDTO().getIdToCotizacion().intValue(), TimeUtils.getDateTime(request.getFechaInicioVigencia()));
							endosoFlotillaService.cancelarCotizacion(cotizacion);
							endosoService.getCotizacionEndosoAltaIncisoTrxNew(poliza.getIdToPoliza(), request.getFechaInicioVigencia(),TipoAccionDTO.getNuevoEndosoCot());
						} else {
							throw e;
						}
					}
					BitemporalCotizacion cotizacion = entidadBitemporalService.getInProcessByBusinessKey(CotizacionContinuity.class, CotizacionContinuity.BUSINESS_KEY_NAME,
							poliza.getCotizacionDTO().getIdToCotizacion().intValue(), TimeUtils.getDateTime(request.getFechaInicioVigencia()));
					cotizacion = endosoService.prepareCotizacionEndoso(cotizacion.getContinuity().getId(), TimeUtils.getDateTime(request.getFechaInicioVigencia()));
					//Limpia datos en proceso de la cotizacion
					entidadBitemporalService.rollBackContinuity(poliza.getCotizacionDTO().getCotizacionContinuity().getId());
					List<LogErroresCargaMasivaDTO> listErrores = endosoFlotillaService.validaCargaMasiva(cotizacion, request);
					if(listErrores.isEmpty()) {
						cargaMasivaService.calcularIncisosEndosoAltaInciso(cotizacion, TimeUtils.getDateTime(request.getFechaInicioVigencia()));
						endosoService.validaPrimaNetaCeroEndosoCotizacion(Short.valueOf(request.getTipoEndoso()), poliza.getIdToPoliza().longValue(), request.getFechaInicioVigencia());
						final ResumenCostosDTO resumenCostos = calculoService.resumenCotizacionEndoso(cotizacion.getEntidadContinuity().getBusinessKey(), 
								TimeUtils.getDateTime(request.getFechaInicioVigencia()));
						response.setResumenCostos(resumenCostos);
						List<BitemporalInciso> listaIncisosCotizacion = listadoIncisosDinamicoService.buscarIncisosFiltrado(new IncisoCotizacionDTO(), request.getTipoEndoso(), 
								poliza.getIdToPoliza().longValue(), request.getFechaInicioVigencia(), true, 1);
						response.setLstInciso(convertBitemporalInciso(listaIncisosCotizacion));
						response.setEstatus(EndosoFlotillaService.SUCCESS);
						
					} else {
						response.setListErrores(listErrores);
						response.setEstatus(EndosoFlotillaService.ERROR);
					}
					
				}  catch (NegocioEJBExeption e) {
					proccessNegocioEJBException(e, response);
					response.setEstatus(EndosoFlotillaService.ERROR);
				} catch (Exception e) {
					response.setEstatus(EndosoFlotillaService.ERROR);
				}
			} else if (SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO == request.getTipoEndoso()) {
			    IncisoCotizacionDTO filtros = new IncisoCotizacionDTO();
				filtros.setPrimerRegistroACargar(0);
				filtros.setNumeroMaximoRegistrosACargar(20);
				try{
				List<BitemporalInciso> listaIncisosCotizacion = listadoIncisosDinamicoService.buscarIncisosFiltrado(filtros, request.getTipoEndoso(), 
						poliza.getIdToPoliza().longValue(), request.getFechaInicioVigencia(), true, 4);
				String idsSeleccionados = "";
				String[] continuitiesStringIds = null;
					
				if(CollectionUtils.isNotEmpty(listaIncisosCotizacion)) {
					inciso : for(BitemporalInciso bitemporalInciso : listaIncisosCotizacion) {
						numeroinciso : for(int i=0; i<request.getNumeroInciso().length; i++ ){
							if(request.getNumeroInciso()[i] == bitemporalInciso.getContinuity().getNumero()){
								idsSeleccionados +=bitemporalInciso.getContinuity().getId()+",";
								break numeroinciso;
								}
							}
						}
					}
					if(!idsSeleccionados.isEmpty()) {
						continuitiesStringIds = idsSeleccionados.split(",");			
						}
									
					Object obj = new Object();
					obj = endosoService.validaPagosRealizadosFront(poliza.getIdToPoliza(), null, continuitiesStringIds, request.getFechaInicioVigencia(), 
					TipoEmision.CANC_INCISO_AUTOS, request.getMotivoEndoso());
					BitemporalCotizacion coti = endosoService.getCotizacionEndosoBajaIncisoTrxNew(poliza.getIdToPoliza(), request.getFechaInicioVigencia(), "1", request.getMotivoEndoso());
					if(obj != null && obj instanceof BigDecimal){
							coti.getValue().setImporteNotaCredito((BigDecimal)obj);
						}
					endosoService.guardaCotizacionEndosoBajaInciso(continuitiesStringIds, coti);
				
				final ResumenCostosDTO resumenCostos = calculoService.resumenCotizacionEndoso(coti.getEntidadContinuity().getBusinessKey(), 
						TimeUtils.getDateTime(request.getFechaInicioVigencia()));
				response.setResumenCostos(resumenCostos);
				response.setEstatus(EndosoFlotillaService.SUCCESS);
			/*	List<BitemporalInciso> listaIncisosCotizacionBajaIncisos = listadoIncisosDinamicoService.buscarIncisosFiltrado(new IncisoCotizacionDTO(), Short.valueOf(request.getTipoEndoso()), 
						poliza.getIdToPoliza().longValue(), request.getFechaInicioVigencia(), true, 1);
					response.setLstInciso(convertBitemporalInciso(listaIncisosCotizacionBajaIncisos));*/
				}catch (NegocioEJBExeption e) {
					proccessNegocioEJBException(e, response);
					response.setEstatus(EndosoFlotillaService.ERROR);
				} catch (Exception e) {
					response.setEstatus(EndosoFlotillaService.ERROR);
				
				}
			
			} else {
				response.setEstatus(EndosoFlotillaService.ERROR);
				response.setMensaje(String.format("Tipo de endoso %s no soportado", request.getTipoEndoso()));
			}
		} catch (Exception e) {
			LOG.error("-- cotizarEndosoFlotilla()", e);
			response.setEstatus(EndosoFlotillaService.ERROR);
			response.setMensaje(e.getCause().getMessage());
		}
		
		return response;
	}

	private void proccessNegocioEJBException(NegocioEJBExeption e, CotizarEndosoFlotillaResponse response) {
		if("NE_6".equals(e.getErrorCode())) {
			response.setMensaje("Ya existe una cotizacion iniciada, para borrar use el parametro 'forzar:true'");
		} else if("NE_18".equals(e.getErrorCode())){
			response.setMensaje("No hubo movimientos");
		} else if("NE_20".equals(e.getErrorCode())){
			response.setMensaje("Debe haber al menos un inciso para dar de baja");
		} else if("NE_19".equals(e.getErrorCode())){
			response.setMensaje("No se deben cancelar todos los incisos");
		} else {
			response.setMensaje(Utilerias.getMensajeRecurso(SistemaPersistencia.RECURSO_MENSAJES_EXCEPCIONES,
					e.getErrorCode()) + " " +  Arrays.toString(e.getValues()));
		}
		
	}

	private List<Inciso> convertBitemporalInciso(List<BitemporalInciso> listaIncisoBitemporal) {
		List<Inciso> result = new ArrayList<Inciso>();
		if(CollectionUtils.isNotEmpty(listaIncisoBitemporal)) {
			for(BitemporalInciso bitemporalInciso : listaIncisoBitemporal) {
				Inciso inciso = new Inciso();
				InformacionDelVehiculo informacionDelVehiculo = new InformacionDelVehiculo();
				informacionDelVehiculo.setDescripcion(bitemporalInciso.getValue().getAutoInciso().getDescripcionFinal());
				inciso.setInformacionDelVehiculo(informacionDelVehiculo);
				inciso.setNumeroInciso(bitemporalInciso.getContinuity().getNumero());
				inciso.setPrimaNeta(new BigDecimal(bitemporalInciso.getValue().getValorPrimaNeta()));
				inciso.setPrimaTotal(new BigDecimal(bitemporalInciso.getValue().getValorPrimaTotal()));
				inciso.setPaquete(bitemporalInciso.getValue().getAutoInciso().getPaquete().getDescripcion());
				result.add(inciso);
			}
		}
		return result;
	}

	@Override
	public EmitirEndosoFlotillaResponse emitirEndosoFlotilla(EmitirEndosoFlotillaRequest request, String token)
			 throws SystemException {
		//validarToken(token);
		EmitirEndosoFlotillaResponse response = new EmitirEndosoFlotillaResponse();
		final PolizaDTO poliza = validatePoliza(request.getNumeroPoliza());
		if(SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO == request.getTipoEndoso()){
			request.setIdCotizacion(poliza.getCotizacionDTO().getCotizacionContinuity().getId());
			response = emitirEndosoFlotillaService.terminarCotizacion(request);
			request.setIdCotizacion(poliza.getCotizacionDTO().getCotizacionContinuity().getId());
	
			if(EmitirEndosoFlotillaService.SUCCESS.equals(response.getEstatus())) {
				response = emitirEndosoFlotillaService.complementarIncisos(request);
				if(EmitirEndosoFlotillaService.SUCCESS.equals(response.getEstatus())) { 
					response = emitirEndosoFlotillaService.emitir(request);
				}
			} 
		}else if (SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO == request.getTipoEndoso()) {
			BitemporalCotizacion	 cotizacion = null;

				 cotizacion = endosoService.getCotizacionEndosoBajaIncisoTrxNew(poliza.getIdToPoliza(), request.getFechaInicioVigencia(),"2", request.getMotivoEndoso());
				 EndosoDTO endoso = emisionEndosoBitemporalService.emiteEndoso(cotizacion.getContinuity().getId(),TimeUtils.getDateTime(request.getFechaInicioVigencia()),cotizacion.getValue().getSolicitud().getClaveTipoEndoso().shortValue());
					String numeroSolicitudCheque = "";
					if(endoso!=null){
						if(endoso.getSolictudCheque()!=null){
							numeroSolicitudCheque = ". N\u00Famero solicitud Seycos del cheque: "+endoso.getSolictudCheque(); 
						}
						response.setEstatus(EndosoFlotillaService.SUCCESS);
						response.setNumeroEndoso(endoso.getId().getNumeroEndoso());
					}
		}
		return response;
	}
	
}

