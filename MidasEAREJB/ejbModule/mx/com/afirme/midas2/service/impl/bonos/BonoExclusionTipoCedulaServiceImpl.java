package mx.com.afirme.midas2.service.impl.bonos;

import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dao.bonos.BonoExclusionTipoCedulaDao;
import mx.com.afirme.midas2.domain.bonos.BonoExclusionTipoCedula;
import mx.com.afirme.midas2.service.bonos.BonoExclusionTipoCedulaService;
import mx.com.afirme.midas2.util.MidasException;
/**
 * 
 * @author sams
 *
 */

@Stateless
public class BonoExclusionTipoCedulaServiceImpl implements	BonoExclusionTipoCedulaService {

	private BonoExclusionTipoCedulaDao dao;
	
	@Override
	public List<BonoExclusionTipoCedula> findById(Long id) throws MidasException {
		return dao.findById(id);
	}

/****************************setters and getters****************************************************************/

	@EJB
	public void setDao(BonoExclusionTipoCedulaDao dao) {
		this.dao = dao;
	}

}
