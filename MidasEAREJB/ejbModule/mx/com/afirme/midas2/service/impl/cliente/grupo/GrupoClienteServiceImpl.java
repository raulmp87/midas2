package mx.com.afirme.midas2.service.impl.cliente.grupo;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.interfaz.cliente.ClienteFacadeRemote;
import mx.com.afirme.midas2.dao.cliente.grupo.GrupoClienteDao;
import mx.com.afirme.midas2.domain.cliente.ClienteGrupoCliente;
import mx.com.afirme.midas2.domain.cliente.GrupoCliente;
import mx.com.afirme.midas2.service.cliente.grupo.GrupoClienteService;

@Stateless
public class GrupoClienteServiceImpl implements GrupoClienteService{

	private GrupoClienteDao grupoClienteDao;
	private ClienteFacadeRemote clienteFacadeRemote;
	
	@EJB
	public void setGrupoClienteDao(GrupoClienteDao grupoClienteDao) {
		this.grupoClienteDao = grupoClienteDao;
	}
	
	@EJB
	public void setClienteFacadeRemote(ClienteFacadeRemote clienteFacadeRemote) {
		this.clienteFacadeRemote = clienteFacadeRemote;
	}
	
	public List<GrupoCliente> listarPorDescripcion(String descripcion,String clave){
		GrupoCliente grupoCliente = new GrupoCliente();
		if(descripcion != null){
			grupoCliente.setDescripcion(descripcion);
		}
		if(clave != null){
			grupoCliente.setClave(clave);
		}
		
		return grupoClienteDao.findByFilters(grupoCliente);
	}
	
	
	public GrupoCliente buscarPorClave(String clave) {
		return grupoClienteDao.findByClave(clave);
	}
	
	public boolean isDuplicado(GrupoCliente grupoCliente) {
		String clave = grupoCliente.getClave();
		if (clave == null) {
			return false;
		}
		
		GrupoCliente gc = buscarPorClave(clave);
		if (gc == null) {
			return false;
		}
		
		if (grupoCliente.getId() != null 
				&& grupoCliente.getId().equals(gc.getId())) {
			return false;
		}
		
		return true;
	}
	
	public GrupoCliente buscarPorId(Long id) {
		GrupoCliente grupoCliente = grupoClienteDao.findById(id);
		
		//Inicializar el cliente.
		for (ClienteGrupoCliente clienteGrupoCliente : grupoCliente.getClienteGrupoClientes()) {
			try {
				clienteGrupoCliente.setCliente(clienteFacadeRemote.findById(
						new BigDecimal(clienteGrupoCliente.getIdTcCliente()),
						""));
			} catch (Exception e) {
				throw new RuntimeException(e);
			}
		}
		
		return grupoCliente;
	}

	@Override
	public GrupoCliente guardar(GrupoCliente grupoCliente) {
		if (grupoCliente.getId() == null) {
			grupoClienteDao.persist(grupoCliente);
			grupoCliente.setClave(grupoCliente.getId().toString());
			grupoCliente = grupoClienteDao.update(grupoCliente);
		} else {
			grupoCliente = grupoClienteDao.update(grupoCliente);
		}
		return grupoCliente;
	}
	
	@Override
	public List<GrupoCliente> buscarPorCliente(Long idCliente) {
		return grupoClienteDao.findByCliente(idCliente);
	}
	
	@Override
	public List<GrupoCliente> buscarTodos() {
		return grupoClienteDao.findAll();
	}
	
	@Override
	public void eliminar(GrupoCliente grupoCliente) {
		if (grupoCliente.getId() != null) {
			grupoClienteDao.remove(grupoCliente);
		}
	}
}
