package mx.com.afirme.midas2.service.impl.job.notifica.estratega.escribe.archivo;

import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;

import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.GrupoParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas2.dao.tarea.EscribeArchivoTextoDao;
import mx.com.afirme.midas2.service.job.escribe.archivo.EscribeArchivoTextoEnSFTPJobService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.util.ConnectionSFTPService;
import mx.com.afirme.midas2.util.TextFileUtils;

/**
 * <p>
 * Clase que implementa la interfaz de <b><code>EscribeArchivoTextoEnSFTPJobService</code></b>
 * en esta implementaci&oacute;n se controla las funciones correspondientes a los procesos
 * de escritura de los archivos de: 
 * 		<ul>
 * 			<li>
 * 				<b>ESCRIBE_SELCOR</b>
 * 			</li>
 * 			<li>
 * 				<b>ESCRIBE_CYBER</b>
 * 			</li>
 * 		</ul>
 * </p>
 * 
 * @author SEGUROS AFIRME
 * 
 * @since 07032017
 * 
 * @version 1.0
 *
 */
@Stateless
public class EscribeArchivoTextoEnSFTPJobServiceImpl implements EscribeArchivoTextoEnSFTPJobService {
	
	private static final Logger LOG = LoggerFactory.getLogger(EscribeArchivoTextoEnSFTPJobServiceImpl.class);
	private static final String DISTINTIVO_ARCHIVO_FOREANO = "archivo_WD_For";
	
	private ConnectionSFTPService sftpService;
	private ParametroGeneralDTO parametroGeneral;
	
	@EJB
	private ParametroGeneralService parametroGeneralService;
	@EJB
	private EscribeArchivoTextoDao escribeArchivoDao;

	@Override
	@Schedule(hour ="23")
	public void initEscribeSelcor() {
		LOG.info("::::: [INFO] ::::: Entra a generar el archivo de SELCOR");
		try{
			String contenido = generaCadenaArchivo(escribeArchivoDao.obtenerInformacionSelcor());
			instanciaServidorSFTP();
			if(contenido != null)
				sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_SELCOR, TextFileUtils.PATH_FILE_VENTACRUZADA, contenido);
			
		} catch(Exception err){
			LOG.info("::::: [ERR] ::::: SE PRODUJO UN ERROR AL GENERAR EL ARCHIVO DE SELCOR", err);
			throw new RuntimeException(err);
		} finally {
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}

	@Override
	@Schedule(hour ="23")
	public void initEscribeCyber() {
		LOG.info("::::: [INFO] ::::: Entra a generar el archivo de CYBER");
		try{
			String contenido = generaCadenaArchivo(escribeArchivoDao.obtenerInformacionCYBER());
			instanciaServidorSFTP();
			if(contenido != null)
				sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_CYBER, TextFileUtils.PATH_FILE_VENTACRUZADA, contenido);
		}catch(Exception err){
			throw new RuntimeException(err);
		} finally {
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	@Override
	public void initWinstonData(){
		try{
			StringBuffer contenido = escribeArchivoDao.obtenerWinstonData(new Date());
			
			instanciaServidorSFTP();
			if(contenido != null)
				sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_WINSTON, TextFileUtils.PATH_WINSTON_DATA, contenido.toString());
		}catch(Exception err){
			throw new RuntimeException(err);
		} finally{
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	@Override
	@Schedule(hour ="23")
	public void initEscribeCajero(){
		try{
			StringBuffer resultadoCajero = escribeArchivoDao.obtenerInformacionCajero();
			instanciaServidorSFTP();
			if(resultadoCajero != null)
				sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_CAJERO, TextFileUtils.PATH_CAJERO, resultadoCajero.toString());
			
		} catch (Exception err){
			throw new RuntimeException(err);
		} finally {
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	@Override
	public void initWinstonDataAutos(Long idCotizacion){
		try{
			List<StringBuffer> archivosWD = escribeArchivoDao.obtenerWinstonDataAutos(idCotizacion);
			
			instanciaServidorSFTP();
			
			for(StringBuffer archivo : archivosWD){
				if(archivo != null){
					if(archivo.toString().contains(DISTINTIVO_ARCHIVO_FOREANO)){
						archivo.toString().replace(DISTINTIVO_ARCHIVO_FOREANO, "");
						sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_WINSTON_FORANEO, TextFileUtils.PATH_WINSTON_DATA, archivo.toString());
					} else {
						sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_WINSTON_LOCAL, TextFileUtils.PATH_WINSTON_DATA, archivo.toString());
					}
				}
			}
			
		}catch(Exception err){
			throw new RuntimeException(err);
		}finally{
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	@Override
	public void initWinstonDataVida(Long idCotizacion){
		Map<String, Object> mapaValores = new HashMap<String, Object>();
		try{
			
			mapaValores.put("idCotizacion", idCotizacion);
			
			StringBuffer resultadoWD = escribeArchivoDao.obtenerWinstonDataVida(idCotizacion);
			
			instanciaServidorSFTP();
			
			sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_WINSTON_LOCAL_VID, TextFileUtils.PATH_WINSTON_DATA, resultadoWD.toString());
			
		}catch(Exception err){
			throw new RuntimeException(err);
		}finally{
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	@Override
	public void initWinstonDataAutosDir(Long idCotizacion){
		try{
			List<StringBuffer> archivosWD = escribeArchivoDao.obtenerWinstonDataDir(idCotizacion);
			
			instanciaServidorSFTP();
			
			for(StringBuffer archivo : archivosWD){
				if(archivo != null){
					if(archivo.toString().contains(DISTINTIVO_ARCHIVO_FOREANO)){
						archivo.toString().replace(DISTINTIVO_ARCHIVO_FOREANO, "");
						sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_WINSTON_FORANEO_DIR, TextFileUtils.PATH_WINSTON_DATA, archivo.toString());
					} else {
						sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_WINSTON_LOCAL_DIR, TextFileUtils.PATH_WINSTON_DATA, archivo.toString());
					}
				}
			}
			
		}catch(Exception err){
			throw new RuntimeException(err);
		}finally{
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	@Override
	public void initWinstonDataPrimasDep(){
		try{
			
			StringBuffer contenido = escribeArchivoDao.obtenerPrimasDepositoWinstonData(new Date());
			
			instanciaServidorSFTP();
			if(contenido != null)
				sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_PRIMAS_DEP, TextFileUtils.PATH_WINSTON_DATA, contenido.toString());
		}catch(Exception err){
			throw new RuntimeException(err);
		} finally {
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	@Override
	@Schedule(hour ="23")
	public void initResultCancel(){
		try{
			
			List<StringBuffer> contenido = escribeArchivoDao.obtenerResultadoCancelacion(new Date());
			
			instanciaServidorSFTP();
			
			for(StringBuffer contenidoArchivo: contenido){
				if(contenidoArchivo != null)
					sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_RESULTADO_CANC, TextFileUtils.PATH_WINSTON_DATA, contenidoArchivo.toString());
			}
			
		} catch (Exception err){
			throw new RuntimeException(err);
		} finally {
			if(sftpService != null){
				sftpService.closeServerSession();
			}
		}
	}
	
	@Override
	@Schedule(hour ="23")
	public void initEscribeIDCliente(){
		try{
			
			List<String> resultadoIdCliente = escribeArchivoDao.obtenerInformacionCliente();
			
			StringBuffer resultadoFinal = new StringBuffer();
			
			for(String renglon: resultadoIdCliente){
				resultadoFinal.append(renglon).append("\n");
			}
			
			instanciaServidorSFTP();
			if(resultadoIdCliente != null)
				sftpService.createFileIntoDirectory(TextFileUtils.FILE_NAME_ID_CLIENTE, TextFileUtils.PATH_FILE_VENTACRUZADA, resultadoFinal.toString());
			
		}catch(Exception err){
			throw new RuntimeException(err);
		}finally{
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	@Override
	@Schedule(hour ="23")
	public void initLeeCliente(){
		try{
			instanciaServidorSFTP();
			InputStream file = sftpService.getFile(TextFileUtils.FILE_NAME_RESPCTE, TextFileUtils.PATH_FILE_VENTACRUZADA);
			TextFileUtils fileUtil = new TextFileUtils();
			List<String> contenidoArchivo = fileUtil.getDataFile(file);
			sftpService.closeServerSession();
			for(String contenidoFila : contenidoArchivo){
				if(!StringUtils.isBlank(contenidoFila) && contenidoFila.length() > 700){
					String idCliente = contenidoFila.substring(744, contenidoFila.length());
					String numeroCotizacion = contenidoFila.substring(1, 10);
					String vIdCliente = idCliente.substring(1, idCliente.length() - 1);
					escribeArchivoDao.actualizaInformacionClienteBanca(new Long(vIdCliente), new Long(numeroCotizacion));
				}
			}
		}catch(Exception err){
			throw new RuntimeException(err);
		}finally{
			if(sftpService != null)
				sftpService.closeServerSession();
		}
	}
	
	/**
	 * <p>M&eacute;todo que genera una cadena de 
	 * texto a partir de una lista elementos tipo <code>String</code></p>
	 * 
	 * @param renglonesArchivo <p>Lista de elementos 
	 * de la cual se sacar&aacute; la informaci&oacute;n que compndr&aacute;
	 * la cadena de retorno.</p> 
	 * 
	 * @return <p>Cadena que representa el contenido de un archivo de texto.</p>
	 */
	private String generaCadenaArchivo(List<String> renglonesArchivo){
		StringBuilder contenido = new StringBuilder();
		if(renglonesArchivo != null && !renglonesArchivo.isEmpty()){
			for(String renglon : renglonesArchivo){
				contenido.append(renglon)
							.append("\n");
			}
		}
		return contenido.toString();
	}
	
	/**
	 * M&eacute;todo que realiza una nueva instancia 
	 * del servicio de conexi&oacute;n al servidor sftp
	 */
	private void instanciaServidorSFTP(){
		parametroGeneral = parametroGeneralService.findByDescCode(GrupoParametroGeneralDTO.GRUPO_SERVIDOR_SFTP, ParametroGeneralDTO.PARAMETRO_SERVIDOR_SFTP_INFO);
		sftpService = new ConnectionSFTPService(parametroGeneral);
		sftpService.connectServer();
	}
	
	public void setParametroGeneralService(
			ParametroGeneralService parametroGeneralService) {
		this.parametroGeneralService = parametroGeneralService;
	}

	public void setEscribeArchivoDao(EscribeArchivoTextoDao escribeArchivoDao) {
		this.escribeArchivoDao = escribeArchivoDao;
	}
}
