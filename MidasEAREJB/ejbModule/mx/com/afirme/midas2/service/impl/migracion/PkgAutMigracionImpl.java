package mx.com.afirme.midas2.service.impl.migracion;

import java.math.BigDecimal;
import java.util.Map;

import javax.annotation.Resource;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.sql.DataSource;

import mx.com.afirme.midas2.domain.migracion.MigEndososValidos;
import mx.com.afirme.midas2.domain.migracion.MigEndososValidosEd;
import mx.com.afirme.midas2.domain.migracion.PkgAutMigracion;

import org.apache.log4j.Logger;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.SqlParameterSource;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;

@Stateless
public class PkgAutMigracionImpl implements PkgAutMigracion {

	@SuppressWarnings("unused")
	private DataSource dataSource;
	private SimpleJdbcCall procAutMigraEndoso;
	private SimpleJdbcCall procAutMigraEndososEd;
	private SimpleJdbcCall procAutMigrarPolizasEd;
	private SimpleJdbcCall procAutAjusteFinalEndoso;
	private SimpleJdbcCall procAutMigrarEndososMidas;
	
	private static final Logger LOG = Logger.getLogger(PkgAutMigracionImpl.class);
	private static final String CATALOG_NAME = "PKGAUT_MIGRACION";
	private static final int STORED_PROCEDURE_EXITO = 0;
	private static final String SCHEMA_NAME = "MIDAS";
	
	@Resource(name="jdbc/MidasDataSource")
	public void setDataSource(DataSource dataSource) {
		JdbcTemplate jdbcTemplate = new JdbcTemplate(dataSource);
        jdbcTemplate.setResultsMapCaseInsensitive(true);
		this.procAutMigraEndoso = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName(SCHEMA_NAME)
				.withCatalogName(CATALOG_NAME)
				.withProcedureName("spAUT_MigraEndoso");
		this.procAutMigraEndososEd = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName(SCHEMA_NAME)
				.withCatalogName(CATALOG_NAME)
				.withProcedureName("spAUT_MigraEndosoED");
		this.procAutMigrarPolizasEd = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName(SCHEMA_NAME)
				.withCatalogName(CATALOG_NAME)
				.withProcedureName("spAUT_MigraPolizasED");
		this.procAutAjusteFinalEndoso = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName(SCHEMA_NAME)
				.withCatalogName(CATALOG_NAME)
				.withProcedureName("spAUT_AjusteFinalEndoso");
		this.procAutMigrarEndososMidas = new SimpleJdbcCall(jdbcTemplate)
				.withSchemaName(SCHEMA_NAME)
				.withCatalogName(CATALOG_NAME)
				.withProcedureName("spAUT_MigraEndosoMidas");
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Object> migraEndoso(MigEndososValidos migEndososValidos) {
		//Llamar SP
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("pIdCotizacion", migEndososValidos.getId().getIdCotizacion())
				.addValue("pIdVersionPol", migEndososValidos.getId().getIdVersionPol())
				.addValue("pIdSolicitud", migEndososValidos.getIdSolicitud())
				.addValue("pIdToPoliza", migEndososValidos.getIdToPoliza());
		
		LOG.info("Ejecutando procAutMigraEndoso.");
		return procAutMigraEndoso.execute(in);
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Object> migraEndosoEd(MigEndososValidosEd migEndososValidosEd) {
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("pIdCotizacion", migEndososValidosEd.getId().getIdCotizacion())
				.addValue("pIdVersionPol", migEndososValidosEd.getId().getIdVersionPol())
				.addValue("pIdSolicitud", migEndososValidosEd.getIdSolicitud())
				.addValue("pIdToPoliza", migEndososValidosEd.getIdToPoliza());
		
		Map<String, Object> out = procAutMigraEndososEd.execute(in);
		return out;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public Map<String, Object> ajusteFinalEndoso(Long idCotizacion, Long idVersionPol, Long idSolicitud, Long idToPoliza) {
		SqlParameterSource in = new MapSqlParameterSource()
				.addValue("pIdCotizacion", idCotizacion)
				.addValue("pIdVersionPol", idVersionPol)
				.addValue("pIdSolicitud", idSolicitud)
				.addValue("pIdToPoliza", idToPoliza);
		
		Map<String, Object> out = procAutAjusteFinalEndoso.execute(in);
		return out;
	}
	
	@Override
	public Map<String, Object> migraPolizasEd() {
		SqlParameterSource in = new MapSqlParameterSource();
		Map<String, Object> out = procAutMigrarPolizasEd.execute(in);
		int idCodResp = ((BigDecimal) out.get("pIdCodResp")).intValue();
		String descResp = (String) out.get("pDescResp");
		if (idCodResp != STORED_PROCEDURE_EXITO) {
			throw new RuntimeException("Falló el stored procedure de migracion de polizas de Emisión Delegada: " + descResp);
		}
		return out;
	}
	
	@Override
	public Map<String, Object> migraEndososMidas() {
		SqlParameterSource in = new MapSqlParameterSource();
		Map<String, Object> out = procAutMigrarEndososMidas.execute(in);
		int idCodResp = ((BigDecimal) out.get("pIdCodResp")).intValue();
		String descResp = (String) out.get("pDescResp");
		if (idCodResp != STORED_PROCEDURE_EXITO) {
			throw new RuntimeException("Falló el stored procedure de migracion de polizas de Emisión Delegada: " + descResp);
		}
		return out;
	}	

}
