package mx.com.afirme.midas2.service.impl.bitemporal.poliza.inciso.seccion.dato;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.logging.Level;
import java.util.regex.Pattern;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TemporalType;

import mx.com.afirme.midas.base.CacheableDTO;
import mx.com.afirme.midas.base.MidasInterfaceBase;
import mx.com.afirme.midas.base.MidasInterfaceBaseAuto;
import mx.com.afirme.midas.base.TipoHorizontal;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoDTO;
import mx.com.afirme.midas.catalogos.estilovehiculo.EstiloVehiculoId;
import mx.com.afirme.midas.catalogos.ramo.RamoDTO;
import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionDTO;
import mx.com.afirme.midas.cotizacion.cobertura.CoberturaCotizacionId;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.ramo.RamoTipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.seccion.cobertura.CoberturaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasInterfaz;
import mx.com.afirme.midas.sistema.ServiceLocatorP;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoId;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.dao.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.IncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.BitemporalSeccionInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.SeccionIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.BitemporalCoberturaSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.cobertura.CoberturaSeccionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.BitemporalDatoSeccion;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.seccion.dato.DatoSeccionContinuity;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoInciso;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoId;
import mx.com.afirme.midas2.domain.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAuto;
import mx.com.afirme.midas2.domain.tarifa.TarifaAgrupadorTarifa;
import mx.com.afirme.midas2.dto.MensajeDTO;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.dto.suscripcion.cotizacion.auto.configuracion.inciso.ControlDinamicoRiesgoDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.EntidadContinuityService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.IncisoViewService;
import mx.com.afirme.midas2.service.bitemporal.poliza.inciso.seccion.dato.ConfiguracionDatoIncisoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.tarifa.NegocioTarifaService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.cobranzainciso.CobranzaIncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionDatoIncisoService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoGrupoProliberAJService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.ConfiguracionIncisoGrupoProliberAVService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.inciso.configuracion.DatoIncisoCotAutoService;
import mx.com.afirme.midas2.service.tarifa.TarifaAgrupadorTarifaService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;
import com.js.util.StringUtil;

@Stateless
public class ConfiguracionDatoIncisoBitemporalServiceImpl implements ConfiguracionDatoIncisoBitemporalService {
	public enum NivelConfiguracion {
		RAMO, SUBRAMO, COBERTURA
	}
	
	@PersistenceContext
	private EntityManager entityManager;

	private ConfiguracionDatoIncisoDao configuracionDatoIncisoDao;
	private NegocioTarifaService negocioTarifaService;
	private TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService;
	private EntidadService entidadService;
	private ServiceLocatorP serviceLocator;
	
	private EntidadBitemporalService entidadBitemporalService;
	
	private EntidadContinuityService entidadContinuityService;
	
	private IncisoViewService incisoViewService;
	
	private ConfiguracionDatoIncisoService configuracionDatoIncisoService;
	
	private DatoIncisoCotAutoService datoIncisoCotAutoService;
	
	private CobranzaIncisoService cobranzaIncisoService;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;
	
	
	@Override
	public List<ConfiguracionDatoInciso> getDatosRamoInciso(BigDecimal idTcRamo) {
		return configuracionDatoIncisoDao.getDatosRamoInciso(idTcRamo,ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus());
	}

	@Override
	public List<ConfiguracionDatoInciso> getDatosSubRamoInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo) {
		return configuracionDatoIncisoDao.getDatosSubRamoInciso(idTcRamo,
				idTcSubRamo, ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus());
	}

	@Override
	public List<ConfiguracionDatoInciso> getDatosCoberturaInciso(
			BigDecimal idTcRamo, BigDecimal idTcSubRamo,
			BigDecimal idToCobertura) {
		return configuracionDatoIncisoDao.getDatosCoberturaInciso(idTcRamo,
				idTcSubRamo, idToCobertura,ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus());
	}
	
	@Override
	public ControlDinamicoRiesgoDTO findByConfiguracionDatoIncisoId(ConfiguracionDatoIncisoId id) {
		ConfiguracionDatoInciso configuracionDatoInciso = configuracionDatoIncisoDao.findById(id);
		
		ControlDinamicoRiesgoDTO control = null;
		if (configuracionDatoInciso != null) { 
			control = new ControlDinamicoRiesgoDTO();
			control.setDependencia(configuracionDatoInciso.getDescripcionClaseRemota());
			control.setEtiqueta(configuracionDatoInciso.getDescripcionEtiqueta());
			control.setId(getIdControl(configuracionDatoInciso));
			control.setTipoControl(convertirTipoControl(configuracionDatoInciso.getClaveTipoControl()));
			control.setTipoValidador(convertirTipoValidador(configuracionDatoInciso));
			control.setTipoDependencia(null);
		}
		return control;
	}

	/** getDatosRiesgo3 */
	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn,
			Map<String, String> valores, Long cotizacionContinuityId, String accion, Boolean getInProcess) {	
		LogDeMidasInterfaz.log("Entrando a getDatosRiesgo3", Level.INFO, null);
	
		Date recordFrom = null;
		Short claveTipoEndoso = null;
		List<ControlDinamicoRiesgoDTO> list = getDatosRiesgo(
			incisoContinuityId, validoEn, recordFrom, valores, cotizacionContinuityId, accion, claveTipoEndoso, getInProcess);
		
		LogDeMidasInterfaz.log("Saliendo de getDatosRiesgo3", Level.INFO, null);
		return list;
	}
	
	/** getDatosRiesgo2, este se llama desde ImpresionBitemporalServiceImpl */
	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, 
			Date recordFrom, Map<String, String> valores, Long cotizacionContinuityId, String accion, 
			Short claveTipoEndoso, Boolean getInProcess) {		
			LogDeMidasInterfaz.log("Entrando a getDatosRiesgo2", Level.INFO, null);
			List<ControlDinamicoRiesgoDTO> controlDinamicoRiesgoDTOList = null;
			
			try{
				//Ya contempla cancelados. Se modificó, aunque el objeto ya no se usa más tarde porque 
				//en el código original en el que esta basado este método
				//(la impresión no-bitemporal), así se encontraba
				//BitemporalCotizacion biCotizacion = obtenerBitemporalCotizacion(validoEn, recordFrom,
				//		cotizacionContinuityId, claveTipoEndoso, getInProcess);
				
				//CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, cotizacionContinuityId);
				
				Short estatusConfiguracion = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
				//if (biCotizacion.getValue().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA)) {
				if(getInProcess){
					estatusConfiguracion = ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus();
				}
				//}
				
				//Llama a getDatosRiesgo 4
				controlDinamicoRiesgoDTOList = this.getDatosRiesgo(incisoContinuityId, validoEn, 
					valores, estatusConfiguracion, accion, getInProcess);
				
				//LLama a getDatosRiesgo 1
				if((controlDinamicoRiesgoDTOList == null || controlDinamicoRiesgoDTOList.size() == 0)
						&& recordFrom != null){
					controlDinamicoRiesgoDTOList = this.getDatosRiesgo(incisoContinuityId, validoEn, 
						recordFrom, valores, estatusConfiguracion, accion, claveTipoEndoso, getInProcess);
				}
			}catch(RuntimeException e){
				throw e;
			}
			
			LogDeMidasInterfaz.log("Saliendo de getDatosRiesgo2", Level.INFO, null);
			return controlDinamicoRiesgoDTOList;
	}
	
	@Override
	public List<ControlDinamicoRiesgoDTO> obtenerDatosRiesgo(Long incisoContinuityId, Date validoEn,
			Map<String, String> valores, Short tipoEndoso, String accion, Boolean getInProcess) {
		
		Short claveFiltroRIesgo = null;
		
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
			claveFiltroRIesgo = ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus();
		} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS) {
			claveFiltroRIesgo = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
		}
		
		List<ControlDinamicoRiesgoDTO> getDatosRiesgo = 
			getDatosRiesgo(incisoContinuityId, validoEn, null, valores, claveFiltroRIesgo, accion, null, getInProcess);
		
		return getDatosRiesgo;
	}
	
	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgoCotizacion(Long incisoContinuityId, Date validoEn,
			Map<String, String> valores, String accion, Boolean getInProcess) {
			//TODO formar inciso para mandarlo
			return this.getDatosRiesgo(incisoContinuityId, validoEn, valores, 
					ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus(), 
					accion, getInProcess);
	}

	/** getDatosRiesgo 4 */
	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, 
			Map<String, String> valores, Short claveFiltroRIesgo, String accion, Boolean getInProcess) {
		LogDeMidasInterfaz.log("Entrando a getDatosRiesgo4", Level.INFO, null);
		
		Date recordFrom = null;
		Short claveTipoEndoso = null;
		List<ControlDinamicoRiesgoDTO> getDatosRiesgo = 
			getDatosRiesgo(incisoContinuityId, validoEn, recordFrom, valores, claveFiltroRIesgo, accion, 
					claveTipoEndoso, getInProcess);
		
		LogDeMidasInterfaz.log("Saliendo de getDatosRiesgo4", Level.INFO, null);
		return getDatosRiesgo;
	}
	
	/** getDatosRiesgo1 **/
	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, Date recordFrom, 
			Map<String, String> valores, Short claveFiltroRIesgo, String accion, Short claveTipoEndoso, 
			Boolean getInProcess) {
		return getDatosRiesgo(incisoContinuityId, validoEn, recordFrom, valores, claveFiltroRIesgo,
				accion, claveTipoEndoso, getInProcess, null);
		
	}
		
	@Override
	public List<ControlDinamicoRiesgoDTO> getDatosRiesgo(Long incisoContinuityId, Date validoEn, Date recordFrom, 
				Map<String, String> valores, Short claveFiltroRIesgo, String accion, Short claveTipoEndoso, 
				Boolean getInProcess, Long coberturaId) {
		LogDeMidasInterfaz.log("Entrando a getDatosRiesgo1", Level.INFO, null);
		
		List<ControlDinamicoRiesgoDTO> controles = new ArrayList<ControlDinamicoRiesgoDTO>();		
		
		try{
		
		//BITEMPORAL
			BitemporalInciso biInciso = null;
			if (getInProcess) {
				biInciso = incisoViewService.getIncisoInProcess(incisoContinuityId, validoEn);
			} else {
				biInciso = incisoViewService.getInciso(incisoContinuityId, validoEn);
				 
				if(biInciso == null && recordFrom != null){
					biInciso = incisoViewService.getInciso(incisoContinuityId, validoEn, recordFrom);
				}
				
				if(biInciso == null && recordFrom != null && 
						(claveTipoEndoso != null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION)){
					biInciso = incisoViewService.getInciso(incisoContinuityId, validoEn, recordFrom, 
							claveTipoEndoso);
				}
			}
			
			if(biInciso != null){
				IncisoContinuity incisoContinuity = biInciso.getContinuity();
				Integer cotizacionBusinessKey = incisoContinuity.getCotizacionContinuity().getBusinessKey();
				BigDecimal idToCotizacion = new BigDecimal(cotizacionBusinessKey);
				CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, idToCotizacion);
							
				List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList();
				
				List<SubRamoDTO> subRamos = obtieneSubRamosInciso(incisoContinuity.getId());
				
				List<ConfiguracionDatoInciso> configuracion = 
					configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(claveFiltroRIesgo);
				
				Collection<BitemporalDatoSeccion> riesgoCotizacion = getDatosSeccionByInciso(incisoContinuityId, 
						validoEn, recordFrom, claveTipoEndoso, getInProcess);
				
				validaConfiguracionDatosRiesgo(controles, configuracion, riesgoCotizacion, ramos, 
													subRamos, biInciso, validoEn, recordFrom, accion, 
													claveTipoEndoso, getInProcess, coberturaId);
			}
		
		} catch(Exception ex) {
			ex.printStackTrace();
		}

		LogDeMidasInterfaz.log("Saliendo de getDatosRiesgo1", Level.INFO, null);
		return controles;
	}	
	
	@SuppressWarnings({ "unchecked" })
	private List<SubRamoDTO> obtieneSubRamosInciso(Long incisoContinuityId){
		final String queryString = "select tc.idTcSubRamo, tc.idTcRamo, tc.codigoSubRamo, tc.descripcionSubRamo, tc.porcentajeMaxComisionRo, " +
				" tc.porcentajeMaxComisionRci, tc.porcentajeMaxComisionPrr, tc.idCuentaContable, tc.idSubCuentaContable " +
				" from MIDAS.TCSUBRAMO tc " +
		" where tc.idTcSubRamo in (select distinct mcsb.subramo_id " +
		" from MIDAS.MCOBERTURASECCIONB mcsb, MIDAS.MCOBERTURASECCIONC mcsc, MIDAS.MSECCIONINCISOC msic " +
		" where mcsb.mcoberturaseccionc_id = mcsc.id " +
		" and mcsc.mseccionincisoc_id = msic.id " +
		" and msic.mincisoc_id = "+incisoContinuityId+")";

		List<SubRamoDTO> subRamos = new ArrayList<SubRamoDTO>(1);
		Object result = entidadService.executeNativeQueryMultipleResult(queryString);
		if(result instanceof List)  {
			List<Object> listaResultados = (List<Object>) result;				
			for(Object object : listaResultados){
				Object[] singleResult = (Object[]) object;
				
				SubRamoDTO subRamo = new SubRamoDTO();
				try{
					BigDecimal idTcSubRamo = null;
					if(singleResult[0] instanceof BigDecimal)
						idTcSubRamo = (BigDecimal)singleResult[0];
					else if (singleResult[0] instanceof Long)
						idTcSubRamo = BigDecimal.valueOf((Long)singleResult[0]);
					else if (singleResult[0] instanceof Double)
						idTcSubRamo = BigDecimal.valueOf((Double)singleResult[0]);

					subRamo.setIdTcSubRamo(idTcSubRamo);
				}catch(Exception e){}
				try{
					RamoDTO ramoDTO = new RamoDTO();
					BigDecimal value = null;
					if(singleResult[1] instanceof BigDecimal)
						value = (BigDecimal)singleResult[1];
					else if (singleResult[1] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[1]);
					else if (singleResult[1] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[1]);

					ramoDTO.setIdTcRamo(value);
					subRamo.setRamoDTO(ramoDTO);
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[2] instanceof BigDecimal)
						value = (BigDecimal)singleResult[2];
					else if (singleResult[2] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[2]);
					else if (singleResult[2] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[2]);

					subRamo.setCodigoSubRamo(value);
				}catch(Exception e){}
				try{
					String value = singleResult[3].toString();
					subRamo.setDescripcionSubRamo(value);
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[4] instanceof BigDecimal)
						value = (BigDecimal)singleResult[4];
					else if (singleResult[4] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[4]);
					else if (singleResult[4] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[4]);

					subRamo.setPorcentajeMaximoComisionRO(value.doubleValue());
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[5] instanceof BigDecimal)
						value = (BigDecimal)singleResult[5];
					else if (singleResult[5] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[5]);
					else if (singleResult[5] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[5]);

					subRamo.setPorcentajeMaximoComisionRCI(value.doubleValue());
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[6] instanceof BigDecimal)
						value = (BigDecimal)singleResult[6];
					else if (singleResult[6] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[6]);
					else if (singleResult[6] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[6]);

					subRamo.setPorcentajeMaximoComisionPRR(value.doubleValue());
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[7] instanceof BigDecimal)
						value = (BigDecimal)singleResult[7];
					else if (singleResult[7] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[7]);
					else if (singleResult[7] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[7]);

					subRamo.setIdCuentaContable(value);
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[8] instanceof BigDecimal)
						value = (BigDecimal)singleResult[8];
					else if (singleResult[8] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[8]);
					else if (singleResult[8] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[8]);

					subRamo.setIdSubCuentaContable(value);
				}catch(Exception e){}
				subRamos.add(subRamo);
			}
		}
		

		return subRamos;
	}

	public Collection<BitemporalDatoSeccion> getDatosSeccionByInciso(Long incisoContinuityId, Date validoEn, Boolean getInProcess) {
		
		Date recordFrom = null;
		Short claveTipoEndoso = null;
		Collection<BitemporalDatoSeccion> collection = getDatosSeccionByInciso(incisoContinuityId, 
				validoEn, recordFrom, claveTipoEndoso, getInProcess);
		
		return collection;
	}
	
	@Override
	public Collection<BitemporalDatoSeccion> getDatosSeccionByInciso(Long incisoContinuityId, Date validoEn, 
			Date recordFrom, Short claveTipoEndoso, Boolean getInProcess) {
		LogDeMidasInterfaz.log("Entrando a getDatosSeccionByInciso", Level.INFO, null);
		Collection<BitemporalDatoSeccion> lstDatoSeccionBi = new ArrayList<BitemporalDatoSeccion>(1);
		
		try{
			BitemporalInciso biInciso = null;
			if (getInProcess) {
				biInciso = incisoViewService.getIncisoInProcess(incisoContinuityId, validoEn);
			} else {
				biInciso = incisoViewService.getInciso(incisoContinuityId, validoEn);
				 
				if(biInciso == null && recordFrom != null){
					biInciso = incisoViewService.getInciso(incisoContinuityId, validoEn, recordFrom, claveTipoEndoso);
				}
			}
			
			// Ya contempla cancelados
			Collection<BitemporalSeccionInciso> lstSeccion = obtenerSecciones(
					validoEn, recordFrom, claveTipoEndoso, biInciso, getInProcess);
			
			if (getInProcess) {
				for (BitemporalSeccionInciso seccionInciso : lstSeccion) {
					lstDatoSeccionBi = entidadBitemporalService.findInProcessByParentKey(DatoSeccionContinuity.class, 
							DatoSeccionContinuity.PARENT_KEY_NAME, seccionInciso.getContinuity().getId(), getDateTime(validoEn));
				}
			} else {
				for (BitemporalSeccionInciso seccionInciso : lstSeccion) {
					lstDatoSeccionBi = entidadBitemporalService.findByParentKey(
							DatoSeccionContinuity.class, DatoSeccionContinuity.PARENT_KEY_NAME, 
							seccionInciso.getContinuity().getId(), getDateTime(validoEn));
					
					if((lstDatoSeccionBi == null || lstDatoSeccionBi.size() == 0) && recordFrom != null){
						Collection<DatoSeccionContinuity> collection = entidadContinuityService.findContinuitiesByParentKey(
								DatoSeccionContinuity.class, DatoSeccionContinuity.PARENT_KEY_NAME, 
								seccionInciso.getContinuity().getId());
						
						for (DatoSeccionContinuity datoSeccionContinuity : collection) {
							// getBitemporalProperty().get()
							BitemporalDatoSeccion bds = datoSeccionContinuity.getDatoSecciones().get(
									new DateTime(validoEn), new DateTime(recordFrom));
							lstDatoSeccionBi.add(bds);
						}
						
					}
				}
			}
		}catch(Exception ex){
			ex.printStackTrace();
		}
		if(lstDatoSeccionBi != null && !lstDatoSeccionBi.isEmpty()){
			try{
				Collection<BitemporalDatoSeccion> lstDatoSeccionBiNull = new ArrayList<BitemporalDatoSeccion>(1);
				lstDatoSeccionBiNull.add(null);
				lstDatoSeccionBi.removeAll(lstDatoSeccionBiNull);
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		LogDeMidasInterfaz.log("Saliendo de getDatosSeccionByInciso", Level.INFO, null);
		return lstDatoSeccionBi;
	}

	private Collection<BitemporalSeccionInciso> obtenerSecciones(Date validoEn,
			Date recordFrom, Short claveTipoEndoso, BitemporalInciso biInciso, Boolean getInProcess) {
		Collection<BitemporalSeccionInciso> lstSeccion = null;
		
		if (getInProcess) {
			lstSeccion = 
				biInciso.getContinuity().getIncisoSeccionContinuities().getInProcess(getDateTime(validoEn));
		} else {
			lstSeccion = 
				biInciso.getContinuity().getIncisoSeccionContinuities().get(getDateTime(validoEn));
			
			if((lstSeccion == null || lstSeccion.size() == 0) 
					&& recordFrom != null){
				lstSeccion = biInciso.getContinuity().getIncisoSeccionContinuities().get(
						getDateTime(validoEn), new DateTime(recordFrom));
			}
			
			// Cancelados
			if(lstSeccion.isEmpty() && recordFrom != null 
					&& claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
				Long sic_parentKey = biInciso.getContinuity().getId();
				Collection<SeccionIncisoContinuity> collectionSIC = 
					entidadContinuityService.findContinuitiesByParentKey(SeccionIncisoContinuity.class, 
							SeccionIncisoContinuity.PARENT_KEY_NAME, sic_parentKey);
				if(!collectionSIC.isEmpty()){
					//SeccionIncisoContinuity sic = collectionSIC.iterator().next(); 
					//Long bsi_continuitiId = sic.getId();
					lstSeccion = entidadBitemporalService.obtenerCancelados(collectionSIC, BitemporalSeccionInciso.class, 
						new DateTime(validoEn), new DateTime(recordFrom));	
				}
			}
		}
		
		return lstSeccion;
	}
	
	/**
	 * Valida si existen configuraciones de riego complementadas para un inciso
	 * @param configuracionPrincipal
	 * @param datoRiesgoCotizacion
	 * @param seccion
	 * @param ramos
	 * @return
	 */
	private void validaConfiguracionDatosRiesgo(List<ControlDinamicoRiesgoDTO> controles, 
			List<ConfiguracionDatoInciso> configuracionPrincipal, 
			Collection<BitemporalDatoSeccion> datoRiesgoCotizacion, List<RamoTipoPolizaDTO> ramos, 
			List<SubRamoDTO> subRamos,
			BitemporalInciso biInciso, Date validoEn, Date recordFrom, String accion, 
			Short claveTipoEndoso, Boolean getInProcess, Long coberturaId) {
		LogDeMidasInterfaz.log("Entrando a validaConfiguracionDatosRiesgo", Level.INFO, null);
		
		List<ConfiguracionDatoInciso> configuraciones = null;
		Collection<BitemporalDatoSeccion> datosIncisoCotAuto = null;	
		
		Collection<BitemporalSeccionInciso> lst = null; 
		
		// Ya contempla cancelaciones
		lst = obtenerBitemporalSeccionesCollection(biInciso, validoEn, recordFrom, claveTipoEndoso, getInProcess);
		
		BitemporalSeccionInciso biSeccionInciso = null;
		if (!lst.isEmpty()) {
			 biSeccionInciso = lst.iterator().next();
		}
		
		// Ya contempla cancelaciones
		Collection<BitemporalCoberturaSeccion> lstBiCobertura = obtenerBitemporalCoberturasCollection(
				validoEn, recordFrom, claveTipoEndoso, biSeccionInciso, getInProcess);
		
		for (RamoTipoPolizaDTO ramo : ramos) {	
			
			if (coberturaId == null) {
				configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
						ramo.getId().getIdtcramo(), BigDecimal.valueOf(0d), BigDecimal.valueOf(0d));
				if (configuraciones != null) {
					
					for (ConfiguracionDatoInciso configuracion : configuraciones) {
						datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
								biSeccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
								biSeccionInciso.getContinuity().getIncisoContinuity().getNumero(), 
								BigDecimal.valueOf(0d), 
								ramo.getId().getIdtcramo(), 
								BigDecimal.valueOf(0d), BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
						
						crearControles(controles, configuracion, datosIncisoCotAuto, 
								ramo.getRamoDTO().getDescripcion(), biInciso, validoEn, recordFrom, accion, 
								claveTipoEndoso, getInProcess);		
					}
				}
			}
			
			
			//
			for (SubRamoDTO subRamo : subRamos) {
				//
				if (coberturaId == null) {
					configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
							ramo.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d));
					//
					if (configuraciones != null) {
						for (ConfiguracionDatoInciso configuracion : configuraciones) {
							//
							datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
									biSeccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
									biSeccionInciso.getContinuity().getIncisoContinuity().getNumero(), 
									BigDecimal.valueOf(0d), ramo.getId().getIdtcramo(), 
									subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d), 
									configuracion.getId().getIdDato());
							//
							crearControles(controles, configuracion, datosIncisoCotAuto, 
									subRamo.getDescripcionSubRamo(), biInciso, validoEn, recordFrom, accion, 
									claveTipoEndoso, getInProcess);	
						}
					}	
				}
			}

			//
			for (BitemporalCoberturaSeccion biCobertura: lstBiCobertura) {
				if (biCobertura.getContinuity().getCoberturaDTO() != null && 
						biCobertura.getValue().getClaveContrato().equals((short) 1)) {
					CoberturaDTO cobertura = biCobertura.getContinuity().getCoberturaDTO();
					
					if (coberturaId == null || (coberturaId != null && coberturaId == cobertura.getIdToCobertura().longValue())) {
						BigDecimal idSubRamo =  BigDecimal.valueOf(biCobertura.getValue().getSubRamoId());
						configuraciones = encontrarConfiguracion(configuracionPrincipal, cobertura.getIdToCobertura(), 
								ramo.getId().getIdtcramo(), idSubRamo, BigDecimal.valueOf(0d));
						if (configuraciones != null) {
							for (ConfiguracionDatoInciso configuracion : configuraciones) {
								datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
										biSeccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
										biSeccionInciso.getContinuity().getIncisoContinuity().getNumero(), 
										cobertura.getIdToCobertura(), ramo.getId().getIdtcramo(), 
										idSubRamo, BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
								crearControles(controles, configuracion, datosIncisoCotAuto, 
										cobertura.getNombreComercial(), biInciso, validoEn, recordFrom, accion, 
										claveTipoEndoso, getInProcess);													
							}
						}		
					}				
						
				}				
			}			
		}			
		
		LogDeMidasInterfaz.log("Saliendo de validaConfiguracionDatosRiesgo", Level.INFO, null);
	}

	private Collection<BitemporalSeccionInciso> obtenerBitemporalSeccionesCollection(
			BitemporalInciso biInciso, Date validoEn, Date recordFrom,
			Short claveTipoEndoso, Boolean getInProcess) {
		Collection<BitemporalSeccionInciso> lst = null;
		if (getInProcess) {
			lst = biInciso.getContinuity().getIncisoSeccionContinuities().getInProcess(getDateTime(validoEn));
		} else {
			
			lst = biInciso.getContinuity().getIncisoSeccionContinuities().get(getDateTime(validoEn));
			if(lst.isEmpty() && recordFrom != null){
				lst = biInciso.getContinuity().getIncisoSeccionContinuities().get(new DateTime(validoEn), 
						new DateTime(recordFrom));
			}
			// Cancelados
			if(lst.isEmpty() && recordFrom != null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
				Long sic_parentKey = biInciso.getContinuity().getId();
				Collection<SeccionIncisoContinuity> collectionSIC = entidadContinuityService.findContinuitiesByParentKey(
						SeccionIncisoContinuity.class, SeccionIncisoContinuity.PARENT_KEY_NAME, sic_parentKey);
				if(!collectionSIC.isEmpty()){ 
					lst = entidadBitemporalService.obtenerCancelados(collectionSIC, BitemporalSeccionInciso.class, 
							new DateTime(validoEn), new DateTime(recordFrom));	
				}
			}			
		}
		return lst;
		
	}

	private Collection<BitemporalCoberturaSeccion> obtenerBitemporalCoberturasCollection(
			Date validoEn, Date recordFrom, Short claveTipoEndoso,
			BitemporalSeccionInciso biSeccionInciso, Boolean getInProcess) {
		
		Collection<BitemporalCoberturaSeccion> lstBiCobertura = null;
		if (getInProcess) {
			 lstBiCobertura = 
					biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().getInProcess(getDateTime(validoEn));	
		} else {
			 lstBiCobertura = 
					biSeccionInciso.getContinuity().getCoberturaSeccionContinuities().get(getDateTime(validoEn));	
				if(lstBiCobertura.isEmpty() && recordFrom != null){
					SeccionIncisoContinuity sic = biSeccionInciso.getContinuity();
					lstBiCobertura = sic.getCoberturaSeccionContinuities().get(
							new DateTime(validoEn), new DateTime(recordFrom));
				}
				// Cancelados
				if(lstBiCobertura.isEmpty() && recordFrom != null && claveTipoEndoso == EndosoDTO.TIPO_ENDOSO_CANCELACION){
					Long csc_parentKey = biSeccionInciso.getContinuity().getId();
					Collection<CoberturaSeccionContinuity> collectionCSC = entidadContinuityService.findContinuitiesByParentKey(
							CoberturaSeccionContinuity.class, CoberturaSeccionContinuity.PARENT_KEY_NAME, csc_parentKey);
					
					if(!collectionCSC.isEmpty()){
						Collection<BitemporalCoberturaSeccion> collectionBCS = 
							entidadBitemporalService.obtenerCancelados(collectionCSC, BitemporalCoberturaSeccion.class, new DateTime(validoEn), new DateTime(recordFrom));
						
						lstBiCobertura = collectionBCS;	
					} 
				}
		}
		
		return lstBiCobertura;
	}	

	/*public void populateControlList(
			List<ControlDinamicoRiesgoDTO> controles,			
			NivelConfiguracion nivelConfiguracion,
			List<ConfiguracionDatoInciso> configuracion,
			BigDecimal idCotizacion,
			BigDecimal numeroInciso,
			BigDecimal idTcRamo,
			BigDecimal idTcSubRamo,
			BigDecimal idToCobertura,
			Map<String, String> valores,
			String descripcion, IncisoCotizacionDTO inciso) {
		List<DatoSeccion> datos = null; 
		switch (nivelConfiguracion) {
		case RAMO:
			//Buscar Coincidencias en DatoIncisoAuto por Ramo
			datos = datoIncisoCotAutoService.getDatosRamoInciso(idCotizacion, numeroInciso,idTcRamo);
			if(datos != null && !datos.isEmpty()){
				
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}else{
				//Crear lista de controles por ramo
				// si la lista configuracion es diferente de vacio
				//generar la lista de controles apartir de la configuracion
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}
			break;
		case SUBRAMO:
			//Buscar Coincidencias en DatoIncisoAuto por SubRamo
			datos = datoIncisoCotAutoService.getDatosSubRamoInciso(idCotizacion, numeroInciso,idTcRamo, idTcSubRamo);
			if(datos != null && !datos.isEmpty()){
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}else{
				//Crear lista de controles por ramo
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}			
			break;

		case COBERTURA:
			//Buscar Coincidencias en DatoIncisoAuto por Cobertura
			datos = datoIncisoCotAutoService.getDatosCoberturaInciso(idCotizacion, numeroInciso,idTcRamo, idTcSubRamo,idToCobertura);
			if(datos != null && !datos.isEmpty()){
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}else{
				//Crear lista de controles por ramo
				this.populateControlDinamico(controles, configuracion, valores, idCotizacion, numeroInciso, datos,descripcion,inciso);
			}			
			break;
		default:
			break;
		}
	}*/
	
	public void crearControles(List<ControlDinamicoRiesgoDTO> controles,
			ConfiguracionDatoInciso configuracion, Collection<BitemporalDatoSeccion> datos,
			String descripcion, BitemporalInciso biInciso, Date validoEn, String accion, Boolean getInProcess) {	
		Date recorFrom = null;
		Short claveTipoEndoso = null;
		crearControles(controles, configuracion, datos, descripcion, biInciso, validoEn, recorFrom, 
							accion, claveTipoEndoso, getInProcess);
	}
	
	public void crearControles(List<ControlDinamicoRiesgoDTO> controles,
			ConfiguracionDatoInciso configuracion, Collection<BitemporalDatoSeccion> datos,
			String descripcion, BitemporalInciso biInciso, Date validoEn, Date recordFrom, String accion, 
			Short claveTipoEndoso, Boolean getInProcess) {		
			
			ControlDinamicoRiesgoDTO control = new ControlDinamicoRiesgoDTO();
			control.setDependencia(configuracion.getDescripcionClaseRemota());
			control.setEtiqueta(configuracion.getDescripcionEtiqueta());
			control.setId(getIdControl(configuracion));
			control.setTipoControl(convertirTipoControl(configuracion.getClaveTipoControl()));
			control.setTipoValidador(convertirTipoValidador(configuracion));
			control.setVisible(true);
			control.setIdGrupoValores(configuracion.getIdGrupo());
			control.setLista(
					obtenerLista(control.getTipoControl(), control.getDependencia(), biInciso, validoEn, recordFrom,
							claveTipoEndoso, getInProcess, configuracion.getIdGrupo())
					);
			control.setTipoDependencia(null);
			control.setDescripcionNivel(descripcion);
			control.setIdNivel(getIdNivel(configuracion));
			if (TipoAccionDTO.getVer().equals(accion) || TipoAccionDTO.getConsultarEndosoCot().equalsIgnoreCase(accion)) {
				control.setDeshabilitado(true);
			}
			
			
			if (datos != null) {
				for (BitemporalDatoSeccion dato : datos) {
					if (getIdControl(configuracion).equals(getIdControl(dato))) {
						control.setValor(dato.getValue().getValor());
					}
				}
			}
			controles.add(control);		
	}

	public void crearControles(List<ControlDinamicoRiesgoDTO> controles,
			List<ConfiguracionDatoInciso> configuracion, Collection<BitemporalDatoSeccion> datos,
			String descripcion, BitemporalInciso biInciso, Date validoEn, Boolean getInProcess) {
		for (ConfiguracionDatoInciso item : configuracion) {
			
			ControlDinamicoRiesgoDTO control = new ControlDinamicoRiesgoDTO();
			control.setDependencia(item.getDescripcionClaseRemota());
			control.setEtiqueta(item.getDescripcionEtiqueta());
			control.setId(getIdControl(item));
			control.setTipoControl(convertirTipoControl(item.getClaveTipoControl()));
			control.setTipoValidador(convertirTipoValidador(item));
			control.setVisible(true);
			control.setLista(
					obtenerLista(control.getTipoControl(), control.getDependencia(), biInciso, validoEn, 
							null, null, getInProcess, item.getIdGrupo()
							)
					);
			control.setTipoDependencia(null);
			control.setDescripcionNivel(descripcion.substring(descripcion.indexOf("-")+1));
			control.setIdNivel(getIdNivel(item));
			/*if (TipoAccionDTO.getVer().equals(accion)) {
				control.setDeshabilitado(true);
			}*/
			
			if (datos != null) { 
				for(BitemporalDatoSeccion dato : datos){
					if(getIdControl(item).equals(getIdControl(dato))){
						control.setValor(dato.getValue().getValor());
					}
				}
			}
			controles.add(control);
		}
	}
	public void populateControlDinamico(
			List<ControlDinamicoRiesgoDTO> controles,
			List<ConfiguracionDatoInciso> configuracion,
			Map<String, String> valores, BigDecimal idCotizacion,
			BigDecimal numeroInciso, Collection<BitemporalDatoSeccion> datos,
			String descripcion, BitemporalInciso biInciso, Date validoEn, Boolean getInProcess) {
		if (controles == null) {
			controles = new ArrayList<ControlDinamicoRiesgoDTO>();
		}			

		crearControles(controles, configuracion, datos, descripcion, biInciso, validoEn, getInProcess);
	}
	
	/**
	 * 
	 */
	private String getIdControl(BitemporalDatoSeccion dato) {
		String del = "_";
		
		String controlId = dato.getContinuity().getRamoId() + del
		+ dato.getContinuity().getSubRamoId()+ del + dato.getContinuity().getCoberturaId() + del
		+ dato.getContinuity().getClaveDetalle() + del + dato.getContinuity().getDatoId();

		return controlId;
	}
	
	private String getIdControl(ConfiguracionDatoInciso configuracionDatoInciso) {
		ConfiguracionDatoIncisoId id = configuracionDatoInciso.getId();
		String del = "_";
		String controlId = id.getIdTcRamo() + del
				+ id.getIdTcSubRamo() + del + id.getIdToCobertura() + del
				+ id.getClaveDetalle() + del + id.getIdDato();
		return controlId;
	}
	
	private String getIdNivel(ConfiguracionDatoInciso configuracionDatoInciso) {
		ConfiguracionDatoIncisoId id = configuracionDatoInciso.getId();
		String del = "_";
		String idNivel = id.getIdTcRamo() + del
				+ id.getIdTcSubRamo() + del + id.getIdToCobertura();
		return idNivel;
	}
	
	private Integer convertirTipoControl(Short tipoControl) {
		int tipo = 0;

		switch (tipoControl) {
		case 1:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_CATALOGO_PROPIO;
			break;
		case 2:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_VALORES_FIJOS;
			break;
		case 3:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_TEXTFIELD;
			break;
		case 4:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_TEXTAREA;
			break;
		case 5:
			tipo = ControlDinamicoRiesgoDTO.TIPO_CONTROL_CHECKBOX;
		}
		
		return tipo;
	}
	
	private Integer convertirTipoValidador(ConfiguracionDatoInciso configuracionDatoInciso) {
		Integer tipoValidador = null;
		Short validadorMidas = configuracionDatoInciso.getClaveTipoValidacion();
			if (validadorMidas != null && validadorMidas > 0) {
				switch (validadorMidas) {
				case 1:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_CUATRO;
					break;
				case 2:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO_DOS;
					break;
				case 3:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_DOS;
					break;
				case 4:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_DIECISEIS_CUATRO;
					break;
				case 5:
					tipoValidador = ControlDinamicoRiesgoDTO.TIPO_VALIDADOR_DECIMAL_OCHO;
					break;
				}
			}
		return tipoValidador;
	}
	
	@SuppressWarnings("unchecked")
	private Map<String, String> obtenerLista(Integer tipoControl, String dependencia, BitemporalInciso biInciso, 
			Date validoEn, Date recordFrom, Short claveTipoEndoso, Boolean getInProcess, Short idGrupo) {
		Map<String, String> lista = new LinkedHashMap<String, String>();
		if(tipoControl == ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_CATALOGO_PROPIO 
				|| tipoControl == ControlDinamicoRiesgoDTO.TIPO_CONTROL_SELECT_VALORES_FIJOS){
			try {
				serviceLocator = ServiceLocatorP.getInstance();
				@SuppressWarnings("rawtypes")
				MidasInterfaceBaseAuto beanBase = serviceLocator.getEJB(dependencia);
				@SuppressWarnings("rawtypes")
				List optionList = null;
			
				if(beanBase instanceof ConfiguracionIncisoGrupoProliberAJService 
						|| beanBase instanceof ConfiguracionIncisoGrupoProliberAVService){
					EstiloVehiculoId estiloId = new EstiloVehiculoId();
					
					BitemporalAutoInciso autoInciso = null;
					if (getInProcess) {
						autoInciso = biInciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(
																						TimeUtils.getDateTime(validoEn));
					} else {
						autoInciso = incisoViewService.getAutoInciso(
								biInciso.getContinuity().getId(), validoEn, recordFrom, claveTipoEndoso);
					}
					
					
					
					CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, 
							biInciso.getContinuity().getCotizacionContinuity().getNumero());
					
					EstiloVehiculoDTO estiloDTO = entidadService.findById(EstiloVehiculoDTO.class,estiloId);
					
					TarifaAgrupadorTarifa tarifaAgrupadorTarifa = 
						tarifaAgrupadorTarifaService.getPorNegocioAgrupadorTarifaSeccion(
							negocioTarifaService.getNegocioAgrupadorTarifaSeccionPorNegocioSeccionMoneda(
									new BigDecimal(autoInciso.getValue().getNegocioSeccionId()),
									cotizacion.getIdMoneda()));
					
					NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, 
							new BigDecimal(autoInciso.getValue().getNegocioSeccionId()));
					
					optionList = beanBase.findAll(new BigDecimal[]{new BigDecimal(
							tarifaAgrupadorTarifa.getId().getIdToAgrupadorTarifa()),
							new BigDecimal(tarifaAgrupadorTarifa.getId().getIdVerAgrupadorTarifa()),
							negocioSeccion.getSeccionDTO().getIdToSeccion(), cotizacion.getIdMoneda(), 
							estiloDTO.getIdTcTipoVehiculo()});
				}else if((beanBase instanceof CatalogoValorFijoFacadeRemote) && idGrupo != null){
					int grupoTipoCarga = idGrupo;
					optionList = ((CatalogoValorFijoFacadeRemote) beanBase).findByProperty("id.idGrupoValores", grupoTipoCarga);
				} else {
					optionList = beanBase.findAll();
				}
				
				lista = convertCacheableDTOListToMap(optionList);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		return lista;
	}
	
	private Map<String, String> convertCacheableDTOListToMap(List<? extends CacheableDTO> list) {
		Map<String, String> map = new LinkedHashMap<String, String>();
		for (CacheableDTO cacheableDTO : list) {
			Object id = cacheableDTO.getId();
			String strId = null;
			if (id instanceof BigDecimal) {
				strId = ((BigDecimal) id).toPlainString();
			} else if(id instanceof CatalogoValorFijoId){
				CatalogoValorFijoId catId = (CatalogoValorFijoId) id;
				try{
					strId = String.valueOf(catId.getIdDato());
				}catch(Exception e){
					strId = id.toString();
				}
			}else {
				strId = id.toString();
			}
			map.put(strId, cacheableDTO.getDescription());
		}
		return map;
	}
	

	@Override
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	public boolean isHorizontal(String idDatoRiesgo, String id)
			throws IllegalArgumentException {
		return isHorizontal(getConfiguracionDatoIncisoId(idDatoRiesgo), new BigDecimal(id));
	}
	
	private boolean isHorizontal(ConfiguracionDatoIncisoId configDatoIncisoId, BigDecimal id) {
		boolean isHorizontal = false;
		Object obj = findCacheableDTOById(configDatoIncisoId, id);
		TipoHorizontal tipoHorizontal = null;
		if (obj instanceof TipoHorizontal) {
			tipoHorizontal = (TipoHorizontal) obj;
			Boolean hor = tipoHorizontal.getTipoHorizontal();
			if (hor != null && hor) {
				isHorizontal = true;
			}
		} else {
			throw new IllegalArgumentException("El objeto encontrado para la configuracion no implementa TipoHorizontal.");
		}
		return isHorizontal;
	}
	
	private CacheableDTO findCacheableDTOById(ConfiguracionDatoIncisoId configDatoIncisoId, BigDecimal id) {
		//Ir a buscar la configuracion.
		ConfiguracionDatoInciso configuracion = 
			findConfiguracionDatoIncisoCotizacionDTOById(configDatoIncisoId);
		if (configuracion == null) {
			throw new IllegalArgumentException("No se pudo encontrar ConfiguracionDatoIncisoCotizacion.");
		}
		
		String jndi = configuracion.getDescripcionClaseRemota();
		MidasInterfaceBase<? extends CacheableDTO> midasInterfaceBase = lookupMidasInterfaceBase(jndi);
		CacheableDTO cacheableDTO = midasInterfaceBase.findById(id);
		if (cacheableDTO == null) {
			throw new IllegalArgumentException(
					"No se encontro coincidencia para el id " + id
							+ " por medio de la busqueda realizada con: "
							+ jndi);
		}		
		return cacheableDTO;
	}
	
	@SuppressWarnings("unchecked")
	private MidasInterfaceBase<? extends CacheableDTO> lookupMidasInterfaceBase(String jndiName) {
		return 	(MidasInterfaceBase<? extends CacheableDTO>) lookup(jndiName);
	}
	
	private Object lookup(String jndiName) {		
		System.out.println("buscando EJB: "+jndiName);
		Object obj = null;
		try {
			obj = ServiceLocatorP.getInstance().getEJB(jndiName);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
		System.out.println("encontrado: "+obj);
		return obj; 
	}
	
	private ConfiguracionDatoInciso findConfiguracionDatoIncisoCotizacionDTOById(
			ConfiguracionDatoIncisoId id) {
		return configuracionDatoIncisoDao.findById(id);
	}
	
	/**
	 * Este metodo genera una ConfiguracionDatoIncisoCotizacionId a partir de un id separado por el delimitador.
	 * @param idDatoRiesgo
	 * @return
	 */
	@Override
	public ConfiguracionDatoIncisoId getConfiguracionDatoIncisoId(String idDatoRiesgo) 
		throws IllegalArgumentException{
		//idDatoRiesgo = getIdDatoRiesgo(idDatoRiesgo); //Lo transforma por si no viene en el formato adecuado.
		String del = "_";
		String[] idTokens = idDatoRiesgo.split(del);
		ConfiguracionDatoIncisoId configuracionId = null;
		if(idTokens.length >= 5){
			
			BigDecimal idTcRamo = new BigDecimal(idTokens[0]);
			BigDecimal idTcSubramo = new BigDecimal(idTokens[1]);
			BigDecimal idToCobertura = new BigDecimal(idTokens[2]);
			Short claveDetalle = new Short(idTokens[3]);
			BigDecimal idDato = new BigDecimal(idTokens[4]);
			
			configuracionId = new ConfiguracionDatoIncisoId();
			configuracionId.setIdTcRamo(idTcRamo);
			configuracionId.setIdTcSubRamo(idTcSubramo);
			configuracionId.setIdToCobertura(idToCobertura);
			configuracionId.setClaveDetalle(claveDetalle);
			configuracionId.setIdDato(idDato);
		}
		else{
			throw new IllegalArgumentException("Los datos recibidos no son una referencia válida a la configuración de datos de riesgo.");
		}
		return configuracionId;
	}
	
	@SuppressWarnings("unused")
	private String getIdDatoRiesgo(String id) {
    	int index = id.lastIndexOf("_");
    	if (index == -1) {
    		return id;
    	}
    	id = id.substring(0, id.lastIndexOf("_"));
    	id = id.substring(id.lastIndexOf("_") + 1);
    	id = id.replaceAll(Pattern.quote("'"), "");
    	return id;
	}
	
	
	/**
	 * Encuentra las configuraciones de datos de riego para los parametros proporcionados
	 * @param configuracion
	 * @param idCobertura
	 * @param idRamo
	 * @param idSubRamo
	 * @param claveDetalle
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private List<ConfiguracionDatoInciso> encontrarConfiguracion(List<ConfiguracionDatoInciso> configuracion, final BigDecimal idCobertura, 
			final BigDecimal idRamo, final BigDecimal idSubRamo, final BigDecimal claveDetalle){				
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				ConfiguracionDatoInciso config = (ConfiguracionDatoInciso) arg0;
				ConfiguracionDatoIncisoId id  = config.getId();
				return id.getIdTcRamo().longValue() == idRamo.longValue() && id.getIdTcSubRamo().longValue() == idSubRamo.longValue() &&
				id.getClaveDetalle().longValue() == claveDetalle.longValue() &&	id.getIdToCobertura().longValue() == idCobertura.longValue();
			}
		};		
		return (List<ConfiguracionDatoInciso>)CollectionUtils.select(configuracion, predicate);	
		
	}
	
	/**
	 * Obtiene un registro dato inciso cot auto para la configuracion recibida
	 * @param datoRiesgoCotizacion
	 * @param idSeccion
	 * @param numInciso
	 * @param idCobertura
	 * @param idRamo
	 * @param idSubRamo
	 * @param claveDetalle
	 * @param idDato
	 * @return
	 */
	@SuppressWarnings("unchecked")
	private Collection<BitemporalDatoSeccion> encontrarRiesgoParaConfiguracion( Collection<BitemporalDatoSeccion> datoRiesgoCotizacion, final BigDecimal idSeccion, final Integer numInciso, 
			final BigDecimal idCobertura, final BigDecimal idRamo, final BigDecimal idSubRamo, final BigDecimal claveDetalle, final BigDecimal idDato){		
		Predicate predicate = new Predicate() {			
			@Override
			public boolean evaluate(Object arg0) {
				BitemporalDatoSeccion datoRiesgo = (BitemporalDatoSeccion) arg0;
				
				return datoRiesgo.getContinuity().getRamoId() == idRamo.longValue() && datoRiesgo.getContinuity().getSubRamoId() == idSubRamo.longValue() &&
					datoRiesgo.getContinuity().getClaveDetalle() == claveDetalle.longValue() && datoRiesgo.getContinuity().getDatoId() == idDato.longValue() &&
					datoRiesgo.getContinuity().getSeccionIncisoContinuity().getSeccion().getIdToSeccion() == idSeccion && 
					datoRiesgo.getContinuity().getSeccionIncisoContinuity().getIncisoContinuity().getNumero() == numInciso.longValue() && 
					datoRiesgo.getContinuity().getCoberturaId() == idCobertura.longValue();
			}
		};		
		return (Collection<BitemporalDatoSeccion>) CollectionUtils.select(datoRiesgoCotizacion, predicate);
		
	}
	
	private DateTime getDateTime(Date date) {
		DateTime dateTime = TimeUtils.current();
		
		if (date != null) {
			dateTime = TimeUtils.getDateTime(date);
		}
		return dateTime;
	}
	
	
	
	@Override
	public void guardarDatosAdicionalesPaquete(Map<String, String> datosRiesgo,	Long incisoContinuityId, Date validoEn) {
		if (datosRiesgo != null) {
			

			for (Entry<String, String> entry : datosRiesgo.entrySet()) {
				String key = entry.getKey();
				String value = entry.getValue();
				ConfiguracionDatoIncisoId id = configuracionDatoIncisoService.getConfiguracionDatoIncisoId(key);
				
				Collection<BitemporalSeccionInciso> lstBSeccionInciso = incisoViewService.getLstSeccionIncisoInProcess(
						incisoViewService.getIncisoInProcess(incisoContinuityId, validoEn), validoEn);
				
				BitemporalSeccionInciso biSeccionInciso = lstBSeccionInciso.iterator().next();
				
			
				BitemporalDatoSeccion biDatoSeccion = null;
				Map<String, Object> params = new LinkedHashMap<String, Object>();
				params.put("continuity.seccionIncisoContinuity.id", biSeccionInciso.getContinuity().getId());
				params.put("continuity.coberturaId", id.getIdToCobertura().longValue());
				params.put("continuity.ramoId", id.getIdTcRamo().longValue());
				params.put("continuity.subRamoId", id.getIdTcSubRamo().longValue());
				params.put("continuity.claveDetalle", id.getClaveDetalle().longValue());
				params.put("continuity.datoId", id.getIdDato().longValue());
				String[] order = {"id"};
				List<BitemporalDatoSeccion> datoSeccionList = (List<BitemporalDatoSeccion>) entidadBitemporalService.listarFiltrado(BitemporalDatoSeccion.class, params,TimeUtils.getDateTime(validoEn), false, order);
				if(datoSeccionList == null || datoSeccionList.isEmpty()){
					datoSeccionList = (List<BitemporalDatoSeccion>) entidadBitemporalService.listarFiltrado(BitemporalDatoSeccion.class, params,TimeUtils.getDateTime(validoEn), true, order);
				}
				if(datoSeccionList !=null && !datoSeccionList.isEmpty()){
					biDatoSeccion = datoSeccionList.iterator().next();
				}
				
				
				DatoSeccionContinuity datoSeccionContinuity = new DatoSeccionContinuity();
				if (biDatoSeccion != null) {//biDatoSeccion.setId(null);biDatoSeccion.setValidityInterval(null);biDatoSeccion.setRecordInterval(null);
					datoSeccionContinuity.setId(biDatoSeccion.getEntidadContinuity().getId());
					datoSeccionContinuity.setParentContinuity(biSeccionInciso.getContinuity());
				}else{
					datoSeccionContinuity.setCoberturaId(id.getIdToCobertura().longValue());
					datoSeccionContinuity.setRamoId(id.getIdTcRamo().longValue());
					datoSeccionContinuity.setSubRamoId(id.getIdTcSubRamo().longValue());
					datoSeccionContinuity.setDatoId(id.getIdDato().longValue());
					datoSeccionContinuity.setClaveDetalle(id.getClaveDetalle().longValue());
					datoSeccionContinuity.setParentContinuity(biSeccionInciso.getContinuity());
				}
				
				BitemporalDatoSeccion biDatoSeccionNuevo = new BitemporalDatoSeccion();
				
				biDatoSeccionNuevo.getValue().setValor(value);
				biDatoSeccionNuevo.setEntidadContinuity(datoSeccionContinuity);
				
				entidadBitemporalService.saveInProcess(biDatoSeccionNuevo, TimeUtils.getDateTime(validoEn));
			}
		}
	}
	
	public BitemporalDatoSeccion getDatoSeccionInProcess(BitemporalDatoSeccion biDatoSeccion, Date validoEn) {
		
		//BitemporalAutoInciso biAutoInciso = biInciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(TimeUtils.getDateTime(validoEn));
		Long datoSeccionContinuityId = null;
		//if(biDatoSeccion.getEntidadContinuity().getDatoSecciones().getInProcess(TimeUtils.getDateTime(validoEn)) != null) {
		datoSeccionContinuityId =  biDatoSeccion.getContinuity().getId();
		
		BitemporalDatoSeccion biDatoSeccionOrig = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalDatoSeccion.class, datoSeccionContinuityId, biDatoSeccion.getContinuity().getParentContinuity().getId(), TimeUtils.getDateTime(validoEn));
		BitemporalDatoSeccion biDatoSeccionEditable = (BitemporalDatoSeccion) biDatoSeccionOrig.copyWith(new IntervalWrapper(TimeUtils.from(TimeUtils.getDateTime(validoEn))), true);
		biDatoSeccionEditable.setId(null);
		return biDatoSeccionEditable;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<ControlDinamicoRiesgoDTO> obtieneDescripcionDatosImpresion(Long incisoContinuityId, DateTime validOn, DateTime recordFrom){
		final String queryString = "select distinct upper(CDI.DESCRIPCIONETIQUETA) AS ETIQUETA, " +
				" CASE WHEN CDI.IDTOCOBERTURA = 0 THEN SR.DESCRIPCIONSUBRAMO ELSE upper(CC.NOMBRECOMERCIALCOBERTURA) END AS DESCRIPCIONNIVEL, " +
				" A7B.ID, CDI.CLAVETIPOCONTROL, CDI.IDGRUPO,  A7B.VALOR " +
		" from midas.mincisoc a2, midas.mseccionincisoc a4, MIDAS.MDATOSECCIONC a7, MIDAS.MDATOSECCIONB a7b, MIDAS.TCSUBRAMO sr, MIDAS.TCCONFIGDATOINCISOCOTAUTO cdi " +
		" left join MIDAS.TOCOBERTURA cc on CC.IDTOCOBERTURA = CDI.IDTOCOBERTURA " +
		" where a2.id = ? " +
		" and a2.id = a4.mincisoc_id " +
		" and A7.MSECCIONINCISOC_ID = A4.ID " +
		" and A7B.MDATOSECCIONC_ID = A7.ID " +
		" and A7B.VALIDFROM <= ? " +
		" and A7B.VALIDTO > ? " +
		" and A7B.RECORDFROM <= ? " +
		" and A7B.RECORDTO > ? " +
		" and A7.COBERTURA_ID = CDI.IDTOCOBERTURA " +
		" and A7.DATO_ID = CDI.IDDATO " +
		" and A7.CLAVEDETALLE = CDI.CLAVEDETALLE " +
		" and A7.RAMO_ID = CDI.IDTCRAMO " +
		" and A7.SUBRAMO_ID = CDI.IDTCSUBRAMO " +
		" and CDI.IDTCSUBRAMO = SR.IDTCSUBRAMO " +
		" order by a7B.id ";

		List<ControlDinamicoRiesgoDTO> controles = new ArrayList<ControlDinamicoRiesgoDTO>(1);
		
		Query query = entityManager.createNativeQuery(queryString.toString());
		query.setParameter(1, incisoContinuityId);
		query.setParameter(2, validOn.toDate());
		query.setParameter(3, validOn.toDate());
		query.setParameter(4, recordFrom.toDate(), TemporalType.TIMESTAMP);
		query.setParameter(5, recordFrom.toDate(), TemporalType.TIMESTAMP);
		
		Object result = query.getResultList();
		if(result instanceof List)  {
			List<Object> listaResultados = (List<Object>) result;				
			for(Object object : listaResultados){
				Object[] singleResult = (Object[]) object;
				
				ControlDinamicoRiesgoDTO control = new ControlDinamicoRiesgoDTO();
				try{
					control.setEtiqueta((String) singleResult[0]);
				}catch(Exception e){}
				try{
					control.setDescripcionNivel((String) singleResult[1]);
				}catch(Exception e){}
				try{
					BigDecimal value = null;
					if(singleResult[4] instanceof BigDecimal)
						value = (BigDecimal)singleResult[4];
					else if (singleResult[4] instanceof Long)
						value = BigDecimal.valueOf((Long)singleResult[4]);
					else if (singleResult[4] instanceof Double)
						value = BigDecimal.valueOf((Double)singleResult[4]);
					
					control.setIdGrupoValores(value.shortValue());
				}catch(Exception e){}
				
				try{
					control.setValor((String) singleResult[5]);
				}catch(Exception e){}
				
				try{
					BigDecimal claveTipoControl = null;
					if(singleResult[3] instanceof BigDecimal)
						claveTipoControl = (BigDecimal)singleResult[3];
					else if (singleResult[3] instanceof Long)
						claveTipoControl = BigDecimal.valueOf((Long)singleResult[3]);
					else if (singleResult[3] instanceof Double)
						claveTipoControl = BigDecimal.valueOf((Double)singleResult[3]);

					if(claveTipoControl.intValue() == 2 && control.getIdGrupoValores() > 0 ){
						CatalogoValorFijoId idDSMVGDF = new CatalogoValorFijoId();
						idDSMVGDF.setIdDato(Integer.parseInt(control.getValor()));
						idDSMVGDF.setIdGrupoValores(control.getIdGrupoValores());
						CatalogoValorFijoDTO dsmvgdf = entidadService.findById(CatalogoValorFijoDTO.class, idDSMVGDF);
						control.setValor(dsmvgdf.getDescripcion());
					}
				}catch(Exception e){}
				
				controles.add(control);
			}
		}
		

		return controles;
	}
	
	@EJB
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@EJB
	public void setIncisoViewService(
			IncisoViewService incisoViewService) {
		this.incisoViewService = incisoViewService;
	}
	
	@EJB
	public void setConfiguracionDatoIncisoDao(
			ConfiguracionDatoIncisoDao configuracionDatoIncisoDao) {
		this.configuracionDatoIncisoDao = configuracionDatoIncisoDao;
	}	
	
	@EJB
	public void setNegocioTarifaService(NegocioTarifaService negocioTarifaService) {
		this.negocioTarifaService = negocioTarifaService;
	}
	
	@EJB
	public void setTarifaAgrupadorTarifaService(
			TarifaAgrupadorTarifaService tarifaAgrupadorTarifaService) {
		this.tarifaAgrupadorTarifaService = tarifaAgrupadorTarifaService;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setEntidadContinuityService(EntidadContinuityService entidadContinuityService){
		this.entidadContinuityService = entidadContinuityService;
	}
	
	@EJB
	public void setConfiguracionDatoIncisoService(
			ConfiguracionDatoIncisoService configuracionDatoIncisoService) {
		this.configuracionDatoIncisoService = configuracionDatoIncisoService;
	}
	
	@EJB
	public void setDatoIncisoCotAutoService(
			DatoIncisoCotAutoService datoIncisoCotAutoService) {
		this.datoIncisoCotAutoService = datoIncisoCotAutoService;
	}
	
	@EJB
	public void setCobranzaIncisoService(
			CobranzaIncisoService cobranzaIncisoService) {
		this.cobranzaIncisoService = cobranzaIncisoService;
	}	

	@Override
	public MensajeDTO validarListaParaEmitir(BitemporalCotizacion cotizacion, Short claveTipoEndoso,
			Date validoEn) {
		
		MensajeDTO mensajeDTO = new MensajeDTO();
		boolean isRdyForEmission = true;
		boolean isValid = true;		
		String msj 	= "";		
		int complementados = 0;		
		Map<String, Object> map = new HashMap<String, Object>();
		
		Short estatusConfiguracion = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
		if(cotizacion.getValue().getClaveEstatus().equals(CotizacionDTO.ESTATUS_COT_TERMINADA))
		{
			estatusConfiguracion = ConfiguracionDatoInciso.Estatus.HASTA_EMISION.getEstatus();
		}		
		
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, new BigDecimal(cotizacion.getContinuity().getBusinessKey()));
					
		List<RamoTipoPolizaDTO> ramos = cotizacionDTO.getTipoPolizaDTO().getRamoTipoPolizaList();		
		
		List<ConfiguracionDatoInciso> configuracion = 
			configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(estatusConfiguracion);
		//List<DatoIncisoCotAuto> riesgoCotizacion = datoIncisoCotAutoDao.getDatosIncisoCotAutoPorCotizacion(new BigDecimal(cotizacion.getContinuity().getNumero()));
	
		Collection<BitemporalInciso> incisosInProcess = new ArrayList<BitemporalInciso>();
		
		//Obtener unicamente los incisos in Process
		//Collection<BitemporalInciso> totalIncisos = cotizacion.getContinuity().getIncisoContinuities().getInProcess(TimeUtils.getDateTime(validoEn));
		//Collection<BitemporalInciso> totalIncisos = cotizacion.getContinuity().getIncisoContinuities().getOnlyInProcess(TimeUtils.getDateTime(validoEn));
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("continuity.cotizacionContinuity.id", cotizacion.getContinuity().getId());		
		List<BitemporalInciso> totalIncisos = (List<BitemporalInciso>) entidadContinuityDao.listarBitemporalsFiltrado(BitemporalInciso.class, 
				params, TimeUtils.getDateTime(validoEn), null, true);
		
		Iterator<BitemporalInciso> itTotalIncisos = totalIncisos.iterator();
		while(itTotalIncisos.hasNext())
		{
			BitemporalInciso incisoTemp = itTotalIncisos.next();
			if(incisoTemp.getRecordStatus() == RecordStatus.TO_BE_ADDED)
			{
				incisosInProcess.add(incisoTemp);				
			}			
		}		
		
		for(BitemporalInciso inciso : incisosInProcess){
			isValid = true;
			
			final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
				+ "(select distinct bcs.value.subRamoId from BitemporalCoberturaSeccion as bcs " 
				+ " where bcs.continuity.seccionIncisoContinuity.incisoContinuity.id = :incisoContinuityId "
				+ "  ) ";
			map = new HashMap<String, Object>();
			map.put("incisoContinuityId", new BigDecimal(inciso.getContinuity().getId()));
			
			@SuppressWarnings({ "unchecked" })
			List<SubRamoDTO> subRamos = entidadService.executeQueryMultipleResult(queryString, map);
			
			Collection<BitemporalDatoSeccion> riesgoCotizacion = getDatosSeccionByInciso(inciso.getContinuity().getId(), 
					validoEn, TimeUtils.now().toDate(), claveTipoEndoso, true);
			
			//Si es cobranza por inciso se valida que posea medio de pago en altas
			if(cobranzaIncisoService.habilitaCobranzaInciso(cotizacionDTO.getIdToCotizacion()) &&
					(claveTipoEndoso != null && claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)){
				if(inciso.getValue().getIdMedioPago() == null){
					isValid = false;				
				}else if (inciso.getValue().getIdMedioPago().intValue() != MedioPagoDTO.MEDIO_PAGO_AGENTE.intValue()){
					if(inciso.getValue().getIdClienteCob() == null || inciso.getValue().getIdConductoCobroCliente() == null){
						if(inciso.getValue().getIdMedioPago() == null){
							isValid = false;
						}
					}
				}
			}
			
			BitemporalAutoInciso autoInciso = entidadBitemporalService.getInProcessByKey(AutoIncisoContinuity.class, inciso.getContinuity().getAutoIncisoContinuity().getKey(), TimeUtils.getDateTime(validoEn));
			if(StringUtil.isEmpty(autoInciso.getValue().getNumeroSerie()) || StringUtil.isEmpty(autoInciso.getValue().getNumeroMotor()) || 
					StringUtil.isEmpty(autoInciso.getValue().getPlaca()) || StringUtil.isEmpty(autoInciso.getValue().getNombreAsegurado())){
				isValid = false;				
			}else{
				if(this.infoConductorEsRequerido(inciso,validoEn)){
					if(StringUtil.isEmpty(autoInciso.getValue().getNumeroLicencia()) || StringUtil.isEmpty(autoInciso.getValue().getNombreConductor()) ||
							StringUtil.isEmpty(autoInciso.getValue().getPaternoConductor()) || StringUtil.isEmpty(autoInciso.getValue().getOcupacionConductor()) ||
							autoInciso.getValue().getFechaNacConductor() == null){
						isValid = false;
					}
				}
				if(isValid){
					isValid = validaConfiguracionDatosRiesgo(configuracion, riesgoCotizacion, ramos, subRamos,
							inciso, validoEn, validoEn, claveTipoEndoso, true);		
				}
			}
						
			if(isValid){
				complementados++;
			}else{
				isRdyForEmission = false;
			}
		}		
		msj = "Se tienen complementados " + complementados + " incisos de "+ incisosInProcess.size();
		
		
		//validar contratante y validar forma pago
		if(isRdyForEmission){
			if(cotizacion.getValue().getPersonaContratanteId() == null){
				isRdyForEmission = false;
			}
			//TODO validar forma de pago
		}
		
		mensajeDTO.setMensaje(msj);
		mensajeDTO.setVisible(isRdyForEmission);
		return mensajeDTO;		
	}
	
	@Override
	public int validaDatosComplementariosIncisoBorrador(BitemporalCotizacion cotizacion, BitemporalInciso inciso, Short claveTipoEndoso, Date validoEn){
		int tipoValido = 0;
		
		Short estatusConfiguracion = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, new BigDecimal(cotizacion.getContinuity().getBusinessKey()));
		
		List<RamoTipoPolizaDTO> ramos = cotizacionDTO.getTipoPolizaDTO().getRamoTipoPolizaList();	
		/*
		final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
			+ "(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion )";
		Map<String, Object> map = new HashMap<String, Object>();
		map.put("idToCotizacion", new BigDecimal(cotizacion.getContinuity().getBusinessKey()));		
		@SuppressWarnings({ "unchecked" })
		*/
		List<SubRamoDTO> subRamos = this.obtieneSubRamosInciso(inciso.getContinuity().getId());
		
		List<ConfiguracionDatoInciso> configuracion = 
			configuracionDatoIncisoDao.getConfiguracionDatosIncisoCotAuto(estatusConfiguracion);
		
		Collection<BitemporalDatoSeccion> riesgoCotizacion = getDatosSeccionByInciso(inciso.getContinuity().getId(), 
				validoEn, TimeUtils.now().toDate(), claveTipoEndoso, true);
		
		BitemporalAutoInciso autoInciso = inciso.getContinuity().getAutoIncisoContinuity().getIncisoAutos().getInProcess(TimeUtils.getDateTime(validoEn));
		if(autoInciso == null){
			autoInciso = entidadBitemporalService.getInProcessByKey(AutoIncisoContinuity.class, inciso.getContinuity().getAutoIncisoContinuity().getKey(), TimeUtils.getDateTime(validoEn));
		}
		if(claveTipoEndoso.equals(SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)){
			if(tipoValido == 0){
				boolean isValid = validaConfiguracionDatosRiesgo(configuracion, riesgoCotizacion, ramos, subRamos,
						inciso, validoEn, validoEn, claveTipoEndoso, true);
				if(!isValid){
					tipoValido = 1;
				}
			}
		}
		if(claveTipoEndoso.equals(SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS)){
				if(this.infoConductorEsRequerido(inciso,validoEn)){
					if(StringUtil.isEmpty(autoInciso.getValue().getNumeroLicencia()) || StringUtil.isEmpty(autoInciso.getValue().getNombreConductor()) ||
							StringUtil.isEmpty(autoInciso.getValue().getPaternoConductor()) || StringUtil.isEmpty(autoInciso.getValue().getOcupacionConductor()) ||
							autoInciso.getValue().getFechaNacConductor() == null){
						tipoValido = 2;
					}
				}
				if(tipoValido == 0){
					boolean isValid = validaConfiguracionDatosRiesgo(configuracion, riesgoCotizacion, ramos, subRamos,
							inciso, validoEn, validoEn, claveTipoEndoso, true);
					if(!isValid){
						tipoValido = 1;
					}
				}
		}
			
		return tipoValido;
	}
	
	@Override
	public Boolean infoConductorEsRequerido(BitemporalInciso inciso, Date validoEn) {
		
		Boolean requerido = false;
		BigDecimal idCoberturaRC_USA_CANADA = new BigDecimal(2640);
		BigDecimal idCoberturaRC_USA_CANADA_FRONT = new BigDecimal(3840);
		BigDecimal idCoberturaRC_USA_CANADA_CAM = new BigDecimal(2850);
		Short claveContrato = 1;		

		Long seccionContinuity = inciso.getContinuity().getIncisoSeccionContinuities().getInProcess(TimeUtils.getDateTime(validoEn)).iterator().next().getContinuity().getId();
		
		
		Map<String,Object> params = new HashMap<String,Object>();
		params.put("continuity.seccionIncisoContinuity.id", seccionContinuity);
		params.put("continuity.coberturaDTO.idToCobertura", idCoberturaRC_USA_CANADA);
		params.put("value.claveContrato", claveContrato);
		Collection<BitemporalCoberturaSeccion> listaCoberturas = entidadBitemporalService.listarFiltrado(BitemporalCoberturaSeccion.class, params, TimeUtils.getDateTime(validoEn), false);
		if(listaCoberturas == null || listaCoberturas.isEmpty()){
			listaCoberturas = entidadBitemporalService.listarFiltrado(BitemporalCoberturaSeccion.class, params, TimeUtils.getDateTime(validoEn), true);
		}
		if(!listaCoberturas.isEmpty()){
			requerido = true;			
		}
		if(!requerido){
			try{
			params = new HashMap<String,Object>();
			params.put("continuity.seccionIncisoContinuity.id", seccionContinuity);
			params.put("continuity.coberturaDTO.idToCobertura", idCoberturaRC_USA_CANADA_FRONT);
			params.put("value.claveContrato", claveContrato);
			listaCoberturas = entidadBitemporalService.listarFiltrado(BitemporalCoberturaSeccion.class, params, TimeUtils.getDateTime(validoEn), false);
			if(listaCoberturas == null || listaCoberturas.isEmpty()){
				listaCoberturas = entidadBitemporalService.listarFiltrado(BitemporalCoberturaSeccion.class, params, TimeUtils.getDateTime(validoEn), true);
			}
			if(!listaCoberturas.isEmpty()){
				requerido = true;			
			}
			}catch(Exception e){}
		}
		if(!requerido){
			try{
				params = new HashMap<String,Object>();
				params.put("continuity.seccionIncisoContinuity.id", seccionContinuity);
				params.put("continuity.coberturaDTO.idToCobertura", idCoberturaRC_USA_CANADA_CAM);
				params.put("value.claveContrato", claveContrato);
				listaCoberturas = entidadBitemporalService.listarFiltrado(BitemporalCoberturaSeccion.class, params, TimeUtils.getDateTime(validoEn), false);
				if(listaCoberturas == null || listaCoberturas.isEmpty()){
					listaCoberturas = entidadBitemporalService.listarFiltrado(BitemporalCoberturaSeccion.class, params, TimeUtils.getDateTime(validoEn), true);
				}
				if(!listaCoberturas.isEmpty()){
					requerido = true;			
				}
			}catch(Exception e){}
		}
		
		return requerido;
	}	
	
	private boolean validaConfiguracionDatosRiesgo( 
			List<ConfiguracionDatoInciso> configuracionPrincipal, 
			Collection<BitemporalDatoSeccion> datoRiesgoCotizacion, List<RamoTipoPolizaDTO> ramos, 
			List<SubRamoDTO> subRamos,
			BitemporalInciso biInciso, Date validoEn, Date recordFrom,  
			Short claveTipoEndoso, Boolean getInProcess) {
		
		LogDeMidasInterfaz.log("Entrando a validaConfiguracionDatosRiesgo", Level.INFO, null);
		
		boolean isValid = true;
		
		List<ConfiguracionDatoInciso> configuraciones = null;
		Collection<BitemporalDatoSeccion> datosIncisoCotAuto = null;	
		
		Collection<BitemporalSeccionInciso> lst = null; 
		
		// Ya contempla cancelaciones
		lst = obtenerBitemporalSeccionesCollection(biInciso, validoEn, recordFrom, claveTipoEndoso, getInProcess);
		
		BitemporalSeccionInciso biSeccionInciso = null;
		if (!lst.isEmpty()) {
			 biSeccionInciso = lst.iterator().next();
		}
		
		// Ya contempla cancelaciones
		Collection<BitemporalCoberturaSeccion> lstBiCobertura = obtenerBitemporalCoberturasCollection(
				validoEn, recordFrom, claveTipoEndoso, biSeccionInciso, getInProcess);
		
		for (RamoTipoPolizaDTO ramo : ramos) {	
			configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
					ramo.getId().getIdtcramo(), BigDecimal.valueOf(0d), BigDecimal.valueOf(0d));
			if (configuraciones != null) {
				
				for (ConfiguracionDatoInciso configuracion : configuraciones) {
					try{
					datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
							biSeccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
							biSeccionInciso.getContinuity().getIncisoContinuity().getNumero(), 
							BigDecimal.valueOf(0d), 
							ramo.getId().getIdtcramo(), 
							BigDecimal.valueOf(0d), BigDecimal.valueOf(0d), configuracion.getId().getIdDato());	
					
					if(datosIncisoCotAuto == null || datosIncisoCotAuto.isEmpty()){
						isValid = false;
						break;
					}
					}catch(Exception e){}
				}
			}
			
			
			if(isValid){
				for (SubRamoDTO subRamo : subRamos) {
					//
					configuraciones = encontrarConfiguracion(configuracionPrincipal, BigDecimal.valueOf(0d), 
							ramo.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d));
					//
					if (configuraciones != null) {
						for (ConfiguracionDatoInciso configuracion : configuraciones) {
							//
							try{
							datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
									biSeccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
									biSeccionInciso.getContinuity().getIncisoContinuity().getNumero(), 
									BigDecimal.valueOf(0d), ramo.getId().getIdtcramo(), 
									subRamo.getIdTcSubRamo(), BigDecimal.valueOf(0d), 
									configuracion.getId().getIdDato());
							
							if(datosIncisoCotAuto == null || datosIncisoCotAuto.isEmpty()){
								isValid = false;
								break;
							}	
							}catch(Exception e){}
						}
					}				
				}
		   }
			if(isValid){
				for (BitemporalCoberturaSeccion biCobertura: lstBiCobertura) {
					if (biCobertura.getContinuity().getCoberturaDTO() != null && 
							biCobertura.getValue().getClaveContrato().equals((short) 1)) {
						CoberturaDTO cobertura = biCobertura.getContinuity().getCoberturaDTO();
						
						BigDecimal idSubRamo =  BigDecimal.valueOf(biCobertura.getValue().getSubRamoId());
						configuraciones = encontrarConfiguracion(configuracionPrincipal, cobertura.getIdToCobertura(), 
								ramo.getId().getIdtcramo(), idSubRamo, BigDecimal.valueOf(0d));
						if (configuraciones != null) {
							for (ConfiguracionDatoInciso configuracion : configuraciones) {
								try{
								datosIncisoCotAuto = encontrarRiesgoParaConfiguracion(datoRiesgoCotizacion, 
										biSeccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
										biSeccionInciso.getContinuity().getIncisoContinuity().getNumero(), 
										cobertura.getIdToCobertura(), ramo.getId().getIdtcramo(), 
										idSubRamo, BigDecimal.valueOf(0d), configuracion.getId().getIdDato());
								
								if(datosIncisoCotAuto == null || datosIncisoCotAuto.isEmpty()){
									isValid = false;
									break;
								}
								}catch(Exception e){}
							}
						}			
					}				
				}
			}
		}			
	
		return isValid;		
	}
	
	@Override
	public boolean validaDatosRiesgos(Long incisoContinuityId, Date validoEn, Short tipoEndoso) {
		boolean existDato = false;
		Short claveFiltroRIesgo = null;
		
		BitemporalInciso biInciso = incisoViewService.getIncisoInProcess(incisoContinuityId, validoEn);
		
		BitemporalCotizacion biCotizacion = biInciso.getContinuity().getParentContinuity().getBitemporalProperty().
																								get(new DateTime(validoEn));		
		

		CotizacionDTO cotizacion = entidadService.findById(CotizacionDTO.class, 
																	new BigDecimal(biCotizacion.getContinuity().getBusinessKey()));
					
		List<RamoTipoPolizaDTO> ramos = cotizacion.getTipoPolizaDTO().getRamoTipoPolizaList();
		
		//List<SeccionCotizacionDTO> seccionCotizacionList = incisoCotizacionDTO.getIncisoAutoCot().getIncisoCotizacionDTO().getSeccionCotizacionList();
		Collection<BitemporalSeccionInciso> lstSeccion = obtenerBitemporalSeccionesCollection(biInciso, validoEn, null, null, true);
		
		if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
			claveFiltroRIesgo = ConfiguracionDatoInciso.Estatus.EN_COTIZACION.getEstatus();
		} else if (tipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS) {
			claveFiltroRIesgo = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
		} else {
			claveFiltroRIesgo = ConfiguracionDatoInciso.Estatus.TODOS.getEstatus();
		}
		
		if (!lstSeccion.isEmpty()) {
			BitemporalSeccionInciso seccionInciso = lstSeccion.iterator().next();
			existDato = validaDatosRiesgos(seccionInciso, ramos, cotizacion, biInciso, claveFiltroRIesgo);
		}
	
		return existDato;
	}
	
private boolean validaDatosRiesgos(BitemporalSeccionInciso seccionInciso, List<RamoTipoPolizaDTO> ramos, 
											CotizacionDTO cotizacion, BitemporalInciso biInciso, Short cveFiltroRiesgo) {
		
		boolean existenDatos = false;
		List<ConfiguracionDatoInciso> configuracionRamo 		= new ArrayList<ConfiguracionDatoInciso>();
		List<ConfiguracionDatoInciso> configuracionSubRamo 		= new ArrayList<ConfiguracionDatoInciso>();
		List<ConfiguracionDatoInciso> configuracionCobertura 	= new ArrayList<ConfiguracionDatoInciso>();
		CoberturaCotizacionDTO filtro = new CoberturaCotizacionDTO();
		Map<String, Object> map;
		
		filtro.setId(new CoberturaCotizacionId());
		filtro.getId().setIdToCotizacion(new BigDecimal(biInciso.getContinuity().getParentContinuity().getBusinessKey()));
		filtro.getId().setNumeroInciso(new BigDecimal(biInciso.getContinuity().getNumero()));
		filtro.setClaveContrato((short)1);
		
		ramo: {
 			for (RamoTipoPolizaDTO ramoTipoPolizaDTO : ramos) {
				configuracionRamo = configuracionDatoIncisoDao.getDatosRamoInciso(ramoTipoPolizaDTO.getId().getIdtcramo(), cveFiltroRiesgo);
				if  (configuracionRamo != null && !configuracionRamo.isEmpty()) {
					existenDatos = existDatoIncisoCotAutoEnConfig(cotizacion.getIdToCotizacion(), 
											seccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
											new BigDecimal(biInciso.getContinuity().getNumero()), configuracionRamo);
					if (!existenDatos) {
						break ramo;
					}
				} else {
					existenDatos =true;
				}
				final String queryString = "select model from SubRamoDTO as model where model.idTcSubRamo in "
					+ "(select distinct toc.idTcSubramo from CoberturaCotizacionDTO as toc where toc.id.idToCotizacion = :idToCotizacion "
					+ " and toc.id.numeroInciso= :numeroInciso )";
				map = new HashMap<String, Object>();
				map.put("idToCotizacion", new BigDecimal(biInciso.getContinuity().getParentContinuity().getBusinessKey()));
				map.put("numeroInciso", new BigDecimal(biInciso.getContinuity().getNumero()));
				@SuppressWarnings({ "unchecked" })
				List<SubRamoDTO> subRamos = entidadService.executeQueryMultipleResult(queryString, map);
				
				for (SubRamoDTO subRamo : subRamos) {
					configuracionSubRamo = configuracionDatoIncisoDao.getDatosSubRamoInciso(ramoTipoPolizaDTO.getId().getIdtcramo(), 
							subRamo.getIdTcSubRamo(), cveFiltroRiesgo);
					if  (configuracionSubRamo != null && !configuracionSubRamo.isEmpty()) {
						existenDatos = existDatoIncisoCotAutoEnConfig(cotizacion.getIdToCotizacion(), 
								seccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
								new BigDecimal(biInciso.getContinuity().getNumero()), configuracionSubRamo);
						if (!existenDatos) {
							break ramo;
						}
					} else {
						existenDatos = true;
					}
					map = new HashMap<String, Object>();
					map.put("id.idToCotizacion", cotizacion.getIdToCotizacion());
					map.put("id.idToSeccion", seccionInciso.getContinuity().getSeccion().getIdToSeccion());
					map.put("id.numeroInciso", new BigDecimal(biInciso.getContinuity().getNumero()));
					map.put("idTcSubramo", subRamo.getIdTcSubRamo());
					map.put("claveContrato", (short)1);
					List<CoberturaCotizacionDTO>  coberturas = 	entidadService.findByProperties(CoberturaCotizacionDTO.class, map);
					for(CoberturaCotizacionDTO cobertura: coberturas) {
						configuracionCobertura = configuracionDatoIncisoDao.getDatosCoberturaInciso(
																	ramoTipoPolizaDTO.getId().getIdtcramo(), subRamo.getIdTcSubRamo(), 
																	cobertura.getId().getIdToCobertura(), cveFiltroRiesgo);
						if  (configuracionCobertura != null && !configuracionCobertura.isEmpty()) {
							existenDatos = existDatoIncisoCotAutoEnConfig(cotizacion.getIdToCotizacion(), 
									seccionInciso.getContinuity().getSeccion().getIdToSeccion(), 
									new BigDecimal(biInciso.getContinuity().getNumero()), configuracionCobertura);
							if (!existenDatos) {
								break ramo;
							}
						} else {
							existenDatos = true;
						}
					}
				}
			}
		}
	
		return existenDatos;
	}

	private boolean existDatoIncisoCotAutoEnConfig(BigDecimal idToCotizacion, BigDecimal idToSeccion, BigDecimal numeroInciso, 
																					List<ConfiguracionDatoInciso> configuracion) {		boolean exist = false;
		for (ConfiguracionDatoInciso configuracionDatoInciso : configuracion) {
			DatoIncisoCotAuto datoIncisoCotAuto =	datoIncisoCotAutoService.getDatoIncisoByConfiguracion(idToCotizacion, 
							idToSeccion, numeroInciso, configuracionDatoInciso.getId().getIdTcRamo(), 
							configuracionDatoInciso.getId().getIdTcSubRamo(), configuracionDatoInciso.getId().getIdToCobertura(), 
							configuracionDatoInciso.getId().getClaveDetalle(), configuracionDatoInciso.getId().getIdDato());
			if (datoIncisoCotAuto != null) {
				exist = true;
			}
		}
		return exist;
	}
}
