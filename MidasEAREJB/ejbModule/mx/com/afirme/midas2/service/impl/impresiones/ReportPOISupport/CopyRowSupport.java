package mx.com.afirme.midas2.service.impl.impresiones.ReportPOISupport;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class CopyRowSupport {
	public static void copyRow(Sheet worksheetSourceA, Sheet worksheetSourceB,
			int sourceRowNum, int destinationRowNum) {
		// Get the source / new row
		Row newRow = worksheetSourceB.getRow(destinationRowNum);
		Row sourceRow = worksheetSourceA.getRow(sourceRowNum);

		// If the row exist in destination, push down all rows by 1 else create
		// a new row
		if (newRow != null) {
			// worksheetSourceA.shiftRows(destinationRowNum,
			// worksheetSourceA.getLastRowNum(), 1);
			newRow = worksheetSourceB.createRow(destinationRowNum);
		} else {
			newRow = worksheetSourceB.createRow(destinationRowNum);
		}

		// Loop through source columns to add to new row
		for (int i = 0; i < sourceRow.getLastCellNum(); i++) {
			// Grab a copy of the old/new cell
			Cell oldCell = sourceRow.getCell(i);
			Cell newCell = newRow.createCell(i);

			// If the old cell is null jump to next cell
			if (oldCell == null) {
				newCell = null;
				continue;
			}

			// Use old cell style
			// newCell.setCellStyle(oldCell.getCellStyle());

			// If there is a cell comment, copy
			// if (newCell.getCellComment() != null) {
			// newCell.setCellComment(oldCell.getCellComment());
			// }

			// If there is a cell hyperlink, copy
			// if (oldCell.getHyperlink() != null) {
			// newCell.setHyperlink(oldCell.getHyperlink());
			// }

			// Set the cell data type
			// newCell.setCellType(oldCell.getCellType());

			// Set the cell data value
			switch (oldCell.getCellType()) {
			case Cell.CELL_TYPE_BLANK:
				break;
			case Cell.CELL_TYPE_BOOLEAN:
				newCell.setCellValue(oldCell.getBooleanCellValue());
				break;
			case Cell.CELL_TYPE_ERROR:
				newCell.setCellErrorValue(oldCell.getErrorCellValue());
				break;
			case Cell.CELL_TYPE_FORMULA:
				newCell.setCellFormula(oldCell.getCellFormula());
				break;
			case Cell.CELL_TYPE_NUMERIC:
				newCell.setCellValue(oldCell.getNumericCellValue());
				break;
			case Cell.CELL_TYPE_STRING:
				newCell.setCellValue(oldCell.getRichStringCellValue());
				break;
			}
		}
	}
}
