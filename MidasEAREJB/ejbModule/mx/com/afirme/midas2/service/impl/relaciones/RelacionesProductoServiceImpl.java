package mx.com.afirme.midas2.service.impl.relaciones;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.consultas.formapago.FormaPagoDTO;
import mx.com.afirme.midas.consultas.formapago.FormaPagoFacadeRemote;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoDTO;
import mx.com.afirme.midas.consultas.mediopago.MedioPagoFacadeRemote;
import mx.com.afirme.midas.producto.ProductoDTO;
import mx.com.afirme.midas.producto.ProductoFacadeRemote;
import mx.com.afirme.midas2.dto.RespuestaGridRelacionDTO;
import mx.com.afirme.midas2.service.relaciones.RelacionesProductoService;

@Stateless
public class RelacionesProductoServiceImpl implements RelacionesProductoService {
	
	public List<FormaPagoDTO> obtenerFormasPagoAsociadas(BigDecimal idProducto) {
		
		ProductoDTO producto = productoFacade.findById(idProducto);
		
		return producto.getFormasPago();
	}
	
	public List<FormaPagoDTO> obtenerFormasPagoDisponibles(BigDecimal idProducto) {
		
		ProductoDTO producto = productoFacade.findById(idProducto);
		List<FormaPagoDTO> formasPagoAsociadas = producto.getFormasPago();
		
		List<FormaPagoDTO> formasPago = formaPagoFacade.findAll();
		
		List<FormaPagoDTO> formasPagoDisponibles = new ArrayList<FormaPagoDTO>();
		
		for (FormaPagoDTO formaPago : formasPago) {
			if (!formasPagoAsociadas.contains(formaPago)) {
				formasPagoDisponibles.add(formaPago);
			}
		}
		
		return formasPagoDisponibles;
	}
	
	
	public RespuestaGridRelacionDTO relacionarFormasPago(String accion, BigDecimal idProducto, Integer idFormaPago) {
		
		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		
		ProductoDTO producto = productoFacade.findById(idProducto);
		
		FormaPagoDTO formaPagoFiltro = new FormaPagoDTO();
		formaPagoFiltro.setIdFormaPago(idFormaPago);
		
		FormaPagoDTO formaPago = formaPagoFacade.findById(formaPagoFiltro);
		
		if (accion.equals("inserted") || accion.equals("updated")) {
			
			if (!producto.getFormasPago().contains(formaPago)) {
				producto.getFormasPago().add(formaPago);
			}
			
		} else if (accion.equals("deleted")) {
			if (producto.getFormasPago().contains(formaPago)) {
				producto.getFormasPago().remove(formaPago);
			}
		}
		
		productoFacade.update(producto);
				
		//TODO Preparar la respuesta y regresarla		
		return null;
	}
	
	
	public List<MedioPagoDTO> obtenerMediosPagoAsociadas(BigDecimal idProducto) {
		
		ProductoDTO producto = productoFacade.findById(idProducto);
		System.out.println("Registros = " + producto.getMediosPago().size());
		
		return producto.getMediosPago();
	}

	@Override
	public List<MedioPagoDTO> obtenerMediosPagoDisponibles(BigDecimal idProducto) {
		
		ProductoDTO producto = productoFacade.findById(idProducto);
		List<MedioPagoDTO> mediosPagoAsociadas = producto.getMediosPago();
		
		List<MedioPagoDTO> mediosPago = medioPagoFacade.findAll();
		
		List<MedioPagoDTO> mediosPagoDisponibles = new ArrayList<MedioPagoDTO>();
		
		for (MedioPagoDTO medioPago : mediosPago) {
			if (!mediosPagoAsociadas.contains(medioPago)) {
				mediosPagoDisponibles.add(medioPago);
			}
		}
		
		return mediosPagoDisponibles;
	}

	@Override
	public RespuestaGridRelacionDTO relacionarMediosPago(String accion,
			BigDecimal idProducto, Integer idMedioPago) {

		RespuestaGridRelacionDTO respuesta = new RespuestaGridRelacionDTO();
		
		ProductoDTO producto = productoFacade.findById(idProducto);
		
		MedioPagoDTO medioPagoFiltro = new MedioPagoDTO();
		medioPagoFiltro.setIdMedioPago(idMedioPago);
		
		MedioPagoDTO medioPago = medioPagoFacade.findById(medioPagoFiltro);
		
		if (accion.equals("inserted") || accion.equals("updated")) {
			
			if (!producto.getMediosPago().contains(medioPago)) {
				producto.getMediosPago().add(medioPago);
			}
			
		} else if (accion.equals("deleted")) {
			if (producto.getMediosPago().contains(medioPago)) {
				producto.getMediosPago().remove(medioPago);
			}
		}
		
		productoFacade.update(producto);
				
		//TODO Preparar la respuesta y regresarla		
		return null;
	}

	
		
	private ProductoFacadeRemote productoFacade;
	
	private FormaPagoFacadeRemote formaPagoFacade;
	
	private MedioPagoFacadeRemote medioPagoFacade;
	
	@EJB
	public void setProductoFacade(ProductoFacadeRemote productoFacade) {
		this.productoFacade = productoFacade;
	}

	@EJB
	public void setFormaPagoFacade(FormaPagoFacadeRemote formaPagoFacade) {
		this.formaPagoFacade = formaPagoFacade;
	}

	@EJB
	public void setMedioPagoFacade(MedioPagoFacadeRemote medioPagoFacade) {
		this.medioPagoFacade = medioPagoFacade;
	}

	
	
	
	

}
