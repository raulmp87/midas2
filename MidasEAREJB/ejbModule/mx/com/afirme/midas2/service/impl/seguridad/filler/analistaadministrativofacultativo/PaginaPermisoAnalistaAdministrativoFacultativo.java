/**
 * Clase que llena Paginas y Permisos para el rol de Analista Administrativo Facultativo
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.analistaadministrativofacultativo;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoAnalistaAdministrativoFacultativo {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoAnalistaAdministrativoFacultativo(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		//Permisos  0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 = AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE
		
		PaginaPermiso pp;
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("crearReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/crearReporte.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listaPolizasReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/listarPolizasSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosPoliza.jsp", "/MidasWeb/siniestro/cabina/reportePoliza.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //GU
		pp.getPermisos().add(listaPermiso.get(13)); //RE
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/mostrarListaReporteDetallePoliza.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosPolizaCrear.jsp", "/MidasWeb/siniestro/cabina/datosPolizaCrear.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/obtenerListaAjustadores.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/guardarReporte.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/listar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp", "/MidasWeb/siniestro/cabina/reporteSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/listarTodos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosReporteSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosPolizaRS.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosPoliza.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosEstadoSiniestroRS.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosEstadoSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadoSiniestro.jsp", "/MidasWeb/siniestro/cabina/reporteEstadoSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/repoGuadSini.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/asignarAjustador.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("caratulaSiniestro.jsp", "/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIngresos.jsp", "/MidasWeb/siniestro/finanzas/listarIngresos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarGastos.jsp", "/MidasWeb/siniestro/finanzas/listarGastos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenesDePago.jsp", "/MidasWeb/siniestro/finanzas/listarOrdenesDePago.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentosSiniestro.jsp", "/MidasWeb/siniestro/documentos/listarDocumentosSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIndemnizaciones.jsp", "/MidasWeb/siniestro/finanzas/indemnizacion/listarIndemnizaciones.do"));
		listaPaginaPermiso.add(pp);
		
		//Permisos agregados del excel
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarDatosPreguntasRS.jsp","/MidasWeb/siniestro/cabina/desplegarDatosPreguntasEspeciales.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarDatosTransporteRS.jsp","/MidasWeb/siniestro/cabina/desplegarDatosTransporte.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaIncisosPoliza.jsp","/MidasWeb/siniestro/cabina/listarIncisosPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaSeccionSubIncisos.jsp","/MidasWeb/siniestro/cabina/listarSeccionSubIncisos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaCoberturasRiesgo.jsp","/MidasWeb/siniestro/cabina/listarCoberturasRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaIncisosPoliza.jsp","/MidasWeb/siniestro/cabina/listarIncisosUbicacionPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarDetalleIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/listarDetalleIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarEliminarIndemnizaciones.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/listarEliminarIndemnizaciones.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/agregarIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarEliminarIndemnizaciones.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/eliminarPagoParcial.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("seleccionaTipoIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/seleccionarTipoIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("seleccionaTipoIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/pendientePagosParciales.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/mostrarAgregarIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/mostrarModificarIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarPagoParcial.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/mostrarAgregarPagoParcial.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarPagoParcial.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/agregarPagoParcial.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarIndemnizaciones.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/cancelarPagoParcial.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarPagoParcial.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/mostrarModificarPagoParcial.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarPagoParcial.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/modificarPagoParcial.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/siniestro/salvamento/listar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/siniestro/salvamento/mostrarAgregar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/salvamento/agregar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/siniestro/salvamento/mostrarModificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/salvamento/modificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/siniestro/salvamento/mostrarBorrar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/salvamento/borrar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/siniestro/salvamento/mostrarDetalle.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("vender.jsp","/MidasWeb/siniestro/salvamento/mostrarVender.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/salvamento/mostrarIngresoSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/salvamento/guardarSalvamentoSession.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("vender.jsp","/MidasWeb/siniestro/salvamento/vender.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarGastoAjuste.jsp","/MidasWeb/siniestro/finanzas/gastosAjuste.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarGastoAjuste.jsp","/MidasWeb/siniestro/finanzas/modificarGasto.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarGastos.jsp","/MidasWeb/siniestro/finanzas/listarGastos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarGastos.jsp","/MidasWeb/siniestro/finanzas/eliminarGastos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarGastos.jsp","/MidasWeb/siniestro/finanzas/cancelarGastos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/guardarGastoSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarGastos.jsp","/MidasWeb/siniestro/finanzas/guardarCambiosGastoSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("generarOrdenPago.jsp","/MidasWeb/siniestro/finanzas/generarOrdenPago.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/guardarOrdenDePago.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarOrdenesDePago.jsp","/MidasWeb/siniestro/finanzas/listarOrdenesDePago.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/cancelarOrdenDePago.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("caratulaSiniestro.jsp","/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestroPDF.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteSiniestralidad.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestralidad.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteSiniestralidadAnexo.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestralidadAnexo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSiniestralidadYAnexo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteSiniestrosRRCSONORv7.jsp","/MidasWeb/siniestro/reportes/cargarReporteSiniestrosRRCSONORv7.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSiniestrosRRCSONORv7.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/poliza/guardarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("afectarCoberturaRiesgo.jsp","/MidasWeb/siniestro/poliza/afectarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarAfectarCoberturaRiesgo.jsp","/MidasWeb/siniestro/poliza/desplegarAfectarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/asignarCoordinadorPorZona.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteListarPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/repoGuadSini.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/modificarReporte.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/listarTodos")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/listarFiltrado.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarReporteSiniestro.jsp","/MidasWeb/siniestro/cabina/mostrarModificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarReporteSiniestro.jsp","/MidasWeb/siniestro/cabina/mostrarDetalle.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/comboCoordinadores.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/confirmarAjustador.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/rechazarAjustador.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSPSinParametros.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("estadoSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteGuardarSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("transportes.jsp","/MidasWeb/siniestro/cabina/reporteTransportes.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/direccionPoliza.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizacionTecnicaGasto.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaGasto.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizacionTecnicaIngreso.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIngreso.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("autorizacionTecnicaIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/autorizacionTecnicaIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaGasto.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaIngreso.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/generarAutorizacionTecnicaIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarReporteAT.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregarIngresos.jsp","/MidasWeb/siniestro/finanzas/ingresos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/guardarIngresosSinietro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarIngresos.jsp","/MidasWeb/siniestro/finanzas/modificarIngresos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/guardarModificarIngresosSinietro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarIngresos.jsp","/MidasWeb/siniestro/finanzas/listarIngresos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarIngresos.jsp","/MidasWeb/siniestro/finanzas/eliminarIngresos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarIngresos.jsp","/MidasWeb/siniestro/finanzas/cancelarIngresos.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/mostrarReporteAT.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestroPDF.do"));listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaAutorizacionesPorAutorizar.jsp","/MidasWeb/siniestro/finanzas/autorizaciontecnica/listarAutorizacionesTecnicas.do")); 
		pp.getPermisos().add(listaPermiso.get(13)); //RE
		listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/autorizaciontecnica/cancelarAutorizacionTecnica.do")); listaPaginaPermiso.add(pp);
		
		//Permisos para eventos catastroficos
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listar.jsp","/MidasWeb/catalogos/eventocatastrofico/listar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("agregar.jsp","/MidasWeb/catalogos/eventocatastrofico/mostrarAgregar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/eventocatastrofico/agregar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificar.jsp","/MidasWeb/catalogos/eventocatastrofico/mostrarModificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/eventocatastrofico/modificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegar.jsp","/MidasWeb/catalogos/eventocatastrofico/mostrarDetalle.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("borrar.jsp","/MidasWeb/catalogos/eventocatastrofico/mostrarBorrar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/eventocatastrofico/borrar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/catalogos/eventocatastrofico/listarFiltrado.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/revisarEstatusOrdenDePago.do")); listaPaginaPermiso.add(pp);
		//J@matitla
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/mostrarListarIngresosPorAplicar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/listarIngresosPorAplicar.do")); listaPaginaPermiso.add(pp);		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/mostrarIngresoPorAplicar.do")); listaPaginaPermiso.add(pp);		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/finanzas/aplicarIngreso.do")); listaPaginaPermiso.add(pp);						
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);
		
		return this.listaPaginaPermiso;
	}
}
