package mx.com.afirme.midas2.service.impl.reaseguro.reportesrr4;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraDTO;
import mx.com.afirme.midas.catalogos.agenciacalificadora.AgenciaCalificadoraFacadeRemote;
import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaDTO;
import mx.com.afirme.midas.catalogos.calificacionesreas.CalificacionAgenciaFacadeRemote;
import mx.com.afirme.midas.catalogos.esquemasreas.EsquemasDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCnsfDTO;
import mx.com.afirme.midas.catalogos.reaseguradorcnsf.ReaseguradorCnsfFacadeRemote;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.controlArchivo.ControlArchivoDTO;
import mx.com.afirme.midas2.dao.catalogos.EntidadHistoricoDao;
import mx.com.afirme.midas2.dao.reaseguro.reportesrr4.CargaArchivosRR4Dao;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsf;
import mx.com.afirme.midas2.domain.catalogos.reaseguradorcnsf.ReaseguradorCnsfMov;
import mx.com.afirme.midas2.service.ExcelRowMapper;
import mx.com.afirme.midas2.service.impl.ExcelConverter;
import mx.com.afirme.midas2.service.impl.ExcelConverter.ExcelConfig.ExcelConfigBuilder;
import mx.com.afirme.midas2.service.reaseguro.reportesRR4.CargaArchivosRR4Service;
import mx.com.afirme.midas2.service.sistema.SistemaContext;

@Stateless
public class CargaArchivosRR4ServiceImpl implements CargaArchivosRR4Service {

	private InputStream is;
	
	private String resultado;
	private String extension;
	public static final String EXITOSO = "exitoso";
	public static final String ERROR = "error";
	public static final String EXISTENTE = "existente";
	public static final String ESTATUS = "CARGADO";
	public static final String ESTATCARGA = "bloqueado";
	public static final int estatusCarga = 0;
	public static final int RESQREASXL = 1;
	public static final int RESQREAS = 2;
	public static final int RESQREASVIDA = 4;
	public static final int RCNSFREAS = 3;
	public static BigDecimal idtcreasegurador = new BigDecimal(0);
	public static BigDecimal idCalificacion = new BigDecimal(10000);
	public static BigDecimal idAgencia = new BigDecimal(5);
		
	java.util.Date fCorte;
	
	private List<String> listaErrores = new ArrayList<String>();
	
	@EJB
	private CargaArchivosRR4Dao cargaArchivosRR4Dao;
	
	@EJB
	private EntidadHistoricoDao entidadService;
	
	@EJB
	private SistemaContext sistemaContext;

	@EJB
	private ReaseguradorCnsfFacadeRemote reaseguradorCnsfBeanRemoto;

	@EJB
	private AgenciaCalificadoraFacadeRemote agenciaCalificadoraFacadeRemote;

	@EJB
	private CalificacionAgenciaFacadeRemote calificacionAgenciaFacadeRemote;

	private Date fechaCorte;
	
	private List<EsquemasDTO> listaRESQREAS = new ArrayList<EsquemasDTO>();
	private List<ReaseguradorCnsf> listaReaseguradores = new ArrayList<ReaseguradorCnsf>();
	private List<ReaseguradorCnsfMov> listaReaseguradoresMov = new ArrayList<ReaseguradorCnsfMov>();
	
	public String procesarInfo(BigDecimal idToControlArchivo, String tipoArchivo, String fechaCorte, String usuario, String accion) {
		
		String negocio = "";
		resultado = EXITOSO;
		
		try {
			this.fechaCorte = getFechaFromString(fechaCorte);
			fCorte = Utilerias.obtenerFechaDeCadena(fechaCorte);
		} catch (ParseException ex) {
			LogDeMidasEJB3.log("Falla al parsear la fecha de corte", Level.SEVERE, ex);
		}
		
		switch(Integer.valueOf(tipoArchivo)) {
			case(RESQREASXL): {
				cargarRESQREAS(idToControlArchivo, "2", this.fechaCorte);
				break;
			}
			case(RESQREAS): {
				cargarRESQREAS(idToControlArchivo, "1", this.fechaCorte);
				break;
			}
			case(RESQREASVIDA): {
				cargarRESQREAS(idToControlArchivo, "4", this.fechaCorte);
				break;
			}
			case(RCNSFREAS): {
				cargarRCNSFREAS(idToControlArchivo, negocio, usuario);
				break;
			}
		}
		
		if(resultado.equalsIgnoreCase(ERROR) && listaErrores.size() > 0) {
			resultado = obtenerResultado(listaErrores);
		}
		
		return resultado;
	}
	
	public Date getFechaFromString(String fecha) throws ParseException {
		java.util.Date fechaCorte = null;
		Date sqlDate = null;
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		try {
			fechaCorte = sdf.parse((fecha));
			sqlDate = new java.sql.Date(fechaCorte.getTime());
		} catch (ParseException e) {
			LogDeMidasEJB3.log("Falla al obtnener la fecha", Level.SEVERE, e);
		}
		return sqlDate;
	}
	
	public String obtenerResultado(List<String> listaErrores) {
		StringBuilder result = new StringBuilder("");
		for(String errores : listaErrores) {
			result.append(errores).append("<br>");
		}
		return result.toString();
	}
	
	public boolean existeRegistroMidas(String cveCNSF, java.util.Date corte) {
		boolean band = false;
		ReaseguradorCnsfMov corredorDTO = new ReaseguradorCnsfMov();
		try {
			if(cveCNSF != null) {
				corredorDTO = cargaArchivosRR4Dao.findByCveReaseguradorCnsf(cveCNSF, corte);
				if (corredorDTO.getIdreasegurador().intValue() != 0) {
					idtcreasegurador = corredorDTO.getIdTcReasegurador();
					idAgencia = corredorDTO.getIdagencia();
					idCalificacion = corredorDTO.getIdcalificacion();
					band = true;
				}
			}
		} catch (Exception e) {
			LogDeMidasEJB3.log("Error existeRegistroMidas " + cveCNSF, Level.SEVERE, e);
		}
		return band;
	}
	
	public String cargarRCNSFREAS(BigDecimal idToControlArchivo, String negocio, String usuario){
		
		boolean existe;
		List<ReaseguradorCnsfMov> reaseguradorCorteList;
		Map<String, Object> parameters  = new HashMap<String, Object>();
		List<ReaseguradorCnsfDTO> reaseguradorCalificacionList;
		AgenciaCalificadoraDTO agenciaNR = entidadService.findByProperty(AgenciaCalificadoraDTO.class, "nombreAgencia", "NR").get(0);
		CalificacionAgenciaDTO calificacionNR = entidadService.findByProperty(CalificacionAgenciaDTO.class, "calificacion", "NR").get(0);
		
		//Obtiene listado de reaseguradores existentes en todos los cortes
		List<ReaseguradorCnsf> reaseguradorList = entidadService.findAll(ReaseguradorCnsf.class);
		
        try {
        	is = getArchivoDatos(idToControlArchivo);
            listaReaseguradores = listarReporteRCNSFREAS();
            
            if(resultado.equals(EXITOSO)) {
            	if(!reaseguradorList.isEmpty()) {
            		for(ReaseguradorCnsf eBd : reaseguradorList) {
            			LogDeMidasEJB3.log("Reasegurador: " + (eBd.getNombreReasegurador() != null ? eBd.getNombreReasegurador() : "null") + " " + (eBd.getClaveCnsf() != null ? eBd.getClaveCnsf() : "null"), Level.INFO, null);
            			if(eBd.getClaveCnsf() != null) {
            				if(eBd.getClaveCnsf().equalsIgnoreCase("NR")) {
            					LogDeMidasEJB3.log((eBd.getNombreReasegurador() != null ? eBd.getNombreReasegurador() : "null") + " " + (eBd.getClaveCnsf() != null ? eBd.getClaveCnsf() : "null") + ". LA CLAVE RGRE NO DEBE ACTUALIZARSE A VALOR 'NR', SOLO LA CALIFICACION. NEXT", Level.INFO, null);
            					continue;
            				}
            				if(eBd.getClaveCnsf().startsWith("S")) {
            					LogDeMidasEJB3.log("IGNORANDO " + (eBd.getNombreReasegurador() != null ? eBd.getNombreReasegurador() : "null") + " " + (eBd.getClaveCnsf() != null ? eBd.getClaveCnsf() : "null") + ". REASEGURADOR LOCAL", Level.INFO, null);
            					continue;
            				}
	            			existe = false;
	            			//Listado de reaseguradores del Layout
	            			for(ReaseguradorCnsf eLayout : listaReaseguradores) {
	            				if(eBd.getClaveCnsf().equalsIgnoreCase(eLayout.getClaveCnsf())) {
	            					existe = true;
	            					LogDeMidasEJB3.log("Autorizado! " + (eLayout.getNombreReasegurador() != null ? eLayout.getNombreReasegurador() : "null") + " " + (eLayout.getClaveCnsf() != null ? eLayout.getClaveCnsf() : "null"), Level.INFO, null);
	            					break;
	            				}
	            			}
	            			parameters.clear();
	            			parameters.put("fechacorte", this.fCorte);
	            			parameters.put("idreasegurador", eBd.getIdCnsfReasegurador());
	            			//Obtiene reasegurador al corte especificado
	            			reaseguradorCorteList = entidadService.findByProperties(ReaseguradorCnsfMov.class, parameters);
	            			if(!reaseguradorCorteList.isEmpty()) {
	            				for(ReaseguradorCnsfMov eCal : reaseguradorCorteList) {
	            					if(existe) {
	            						if(eCal.getIdagencia() == null || eCal.getIdcalificacion() == null){
	            							LogDeMidasEJB3.log("No tiene calificacion/agencia. Agencia=" + (eCal.getIdagencia() == null ? "null" : eCal.getIdagencia()) + ", calificacion=" + (eCal.getIdcalificacion() == null ? "null" : eCal.getIdcalificacion()), Level.INFO, null);
	            							break;
	            						}
	            						if(eCal.getIdagencia().longValue() == agenciaNR.getIdagencia().longValue() && eCal.getIdcalificacion().longValue() == calificacionNR.getId().longValue()) {
	            							LogDeMidasEJB3.log("Agencia=" + (eCal.getIdagencia() == null ? "null" : eCal.getIdagencia()) + ", calificacion=" + (eCal.getIdcalificacion() == null ? "null" : eCal.getIdcalificacion()), Level.INFO, null);
	            							parameters.clear();
	            							parameters.put("claveCnsf", eBd.getClaveCnsf());
	            							//Obtiene reasegurador del catálogo intermedio (MIDAS.cnsf_reaseguradores) buscando por clave RGRE
	            							reaseguradorCalificacionList = entidadService.findByProperties(ReaseguradorCnsfDTO.class, parameters);
	            							if(!reaseguradorCalificacionList.isEmpty()) {
	            								if(reaseguradorCalificacionList.get(0).getIdagencia() == null || reaseguradorCalificacionList.get(0).getCalificacionAgenciaDTO() == null || reaseguradorCalificacionList.get(0).getCalificacionAgenciaDTO().getId() == null) {
	            									LogDeMidasEJB3.log("El reasegurador no tiene calificacion en el catalogo intermedio. Agencia=" + (reaseguradorCalificacionList.get(0).getIdagencia() == null ? "null" : reaseguradorCalificacionList.get(0).getIdagencia()) + ", calificacion=" + (reaseguradorCalificacionList.get(0).getCalificacionAgenciaDTO() == null || reaseguradorCalificacionList.get(0).getCalificacionAgenciaDTO().getId() == null ? "null" : reaseguradorCalificacionList.get(0).getCalificacionAgenciaDTO().getId()), Level.INFO, null);
	            									break;
	            								}
	            								eCal.setUsuario(usuario + " - LAY");
	            								eCal.setIdagencia(reaseguradorCalificacionList.get(0).getIdagencia());
	            								eCal.setIdcalificacion(reaseguradorCalificacionList.get(0).getCalificacionAgenciaDTO().getId());
	            								entidadService.persist(eCal);
	            								LogDeMidasEJB3.log("Revivio " + eBd.getClaveCnsf() + " con agencia ID " + reaseguradorCalificacionList.get(0).getIdagencia() + " y calificacion ID " + reaseguradorCalificacionList.get(0).getCalificacionAgenciaDTO().getId(), Level.INFO, null);
	            							}
	            						}
	            					} else {
	            						LogDeMidasEJB3.log("NO Autorizado.", Level.INFO, null);
	            						eCal.setUsuario(usuario + " - LAY");
	            						eCal.setIdagencia(agenciaNR.getIdagencia());
	            						eCal.setIdcalificacion(calificacionNR.getId());
	            						entidadService.persist(eCal);
	            						LogDeMidasEJB3.log("Agencia y Calificacion NR. ID " + (eCal.getIdreaseguradormov() != null ? eCal.getIdreaseguradormov() : ""), Level.INFO, null);
	            					}
	            				}
	            			}
            			}
            		}
            	}
            }
        } catch (RuntimeException ex) {
            resultado = ERROR;
            LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
        }
        return resultado;
    }
	
	public String cargarRESQREAS(BigDecimal idToControlArchivo, String negocio, Date fechaDate){
		try {
		is = getArchivoDatos(idToControlArchivo);
		int contador = 2000;	
		int cer = 0;
		List<BigDecimal> max;	
		listaRESQREAS = listarReporteRESQREAS();
		
		if(resultado.equals(EXITOSO)){	
			
				if(negocio.equals("4")){
					cargaArchivosRR4Dao.deleteEsquemas((fechaDate.getYear()+1900),(fechaDate.getMonth()+1),fechaDate.getDate(),4);
				}
				else{
					cargaArchivosRR4Dao.deleteEsquemas((fechaDate.getYear()+1900),4,1,Integer.parseInt(negocio));
				}
				
				for (EsquemasDTO reporte : listaRESQREAS) {
				
					if(negocio.equals("4"))
					{
						reporte.setAnio(new BigDecimal(fechaDate.getYear()+1900));	
						reporte.setMes(new BigDecimal(fechaDate.getMonth()+1));
						reporte.setDia(new BigDecimal(fechaDate.getDate()));
						reporte.setTipoMoneda(new BigDecimal(0));
						reporte.setCveNegocio(new BigDecimal(4));
					}else if (reporte.getIdContrato().contains("V")){
						reporte.setTipoMoneda(new BigDecimal(1));	
						reporte.setCveNegocio(new BigDecimal(4));
						reporte.setTipoMoneda(new BigDecimal(0));
						negocio = "4";
					}
					else{
						contador = 5000;
					}
					
					if(cer == 0){
						
						max = cargaArchivosRR4Dao.getMaxCER("anio", reporte.getAnio(),"cveNegocio",new BigDecimal(negocio));
						
						if(max.get(0)!=null){
							cer = max.get(0).intValue();
						}else{
							cer = reporte.getAnio().intValue()*10000+contador;
						}
											
					}
					 			
					reporte.setCer(new BigDecimal(cer+reporte.getCer().intValue()));
					cargaArchivosRR4Dao.save(reporte);
					  }
				}
			
			
			} catch (RuntimeException ex) {
				resultado = ERROR;
				LogDeMidasEJB3.log("Falla al leer el archivo", Level.SEVERE, ex);
			}
			
		return resultado;
					
	}
	
	private InputStream getArchivoDatos(BigDecimal idToControlArchivo) {

		InputStream archivo = null;
		ControlArchivoDTO controlArchivoDTO = entidadService.findById(ControlArchivoDTO.class, idToControlArchivo);
		String nombreCompletoArchivo = null;
		String nombreOriginalArchivo = controlArchivoDTO.getNombreArchivoOriginal();
		extension = (nombreOriginalArchivo.lastIndexOf('.') == -1) ? ""	: nombreOriginalArchivo.substring(
						nombreOriginalArchivo.lastIndexOf('.'),
						nombreOriginalArchivo.length());

		LogDeMidasEJB3.log("Buscando el archivo", Level.INFO, null);
		nombreCompletoArchivo = controlArchivoDTO.getIdToControlArchivo().toString() + extension;
		try {
			archivo = new FileInputStream(sistemaContext.getUploadFolder()
					+ nombreCompletoArchivo);
		} catch (FileNotFoundException re) {
			LogDeMidasEJB3.log("Archivo no encontrado", Level.SEVERE, re);
		}

		return archivo;
	}
	
	public List<EsquemasDTO> listarReporteRESQREAS() {
		List<EsquemasDTO> listaReporteEsquemas = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaReporteEsquemas = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<EsquemasDTO>() {
							public EsquemasDTO mapRow(Map<String, String> row,
									int rowNum) {
								EsquemasDTO esquemasDTO = new EsquemasDTO();
								try {
									esquemasDTO.setAnio(new BigDecimal(Integer.parseInt(row.get("anio"))));
									esquemasDTO.setIdContrato(row.get("idcontrato"));
									esquemasDTO.setCer(new BigDecimal(Integer.parseInt(row.get("cer"))));
									esquemasDTO.setTipoCobertura(new BigDecimal(Integer.parseInt(row.get("tipocobertura"))));
									esquemasDTO.setNivel(new BigDecimal(Integer.parseInt(row.get("nivel"))));
									esquemasDTO.setOrdenEntrada(new BigDecimal(Integer.parseInt(row.get("ordenentrada"))));
									esquemasDTO.setRetencion(new BigDecimal(Double.parseDouble(row.get("retencion"))));
									esquemasDTO.setCapLim(new BigDecimal(Double.parseDouble(row.get("caplimite"))));
									esquemasDTO.setRetAdic(new BigDecimal(Double.parseDouble(row.get("retadicional"))));
									esquemasDTO.setReinstalaciones(new BigDecimal(Double.parseDouble(row.get("reinstalacion"))));
									esquemasDTO.setLlaveD(new BigDecimal(Integer.parseInt(row.get("llave"))));
									esquemasDTO.setLlaveRet(new BigDecimal(Integer.parseInt(row.get("llaveretencion"))));
									esquemasDTO.setClaveREAS(row.get("clavereas"));
									esquemasDTO.setClaveReasAlter(row.get("clavereas"));
									esquemasDTO.setPartREAS(new BigDecimal(Double.parseDouble(row.get("partreas"))));
									esquemasDTO.setCalificacion(new BigDecimal(Integer.parseInt(row.get("calificacion"))));
									esquemasDTO.setCombinacion(row.get("idcontrato"));


								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide." + e.getMessage(), Level.SEVERE, e);
								}catch (Exception e) {
									LogDeMidasEJB3.log("Error General." + e.getMessage(), Level.SEVERE, e);
								}
								return esquemasDTO;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteEsquemas;
	}

	public List<ReaseguradorCnsf> listarReporteRCNSFREAS() {
		List<ReaseguradorCnsf> listaReporteREAS = null;
		try {
			ExcelConverter converter = new ExcelConverter(is,
					new ExcelConfigBuilder().withColumnNamesToLowerCase(true)
							.withTrimColumnNames(true).build());

			for (String sheetName : converter.getSheetNames()) {

				listaReporteREAS = converter.parseWithRowMapper(sheetName,
						new ExcelRowMapper<ReaseguradorCnsf>() {
							public ReaseguradorCnsf mapRow(Map<String, String> row,
									int rowNum) {
								ReaseguradorCnsf reaseguradorDTO = new ReaseguradorCnsf();
								try {
									reaseguradorDTO.setClaveCnsf(row.get("cvereasegurador"));
									reaseguradorDTO.setNombreReasegurador(row.get("nombrereasegurador"));
																		   

								} catch (NumberFormatException e) {
									resultado = ERROR;
									LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide." + e.getMessage(), Level.SEVERE, e);
								}catch (Exception e) {
									LogDeMidasEJB3.log("Error General." + e.getMessage(), Level.SEVERE, e);
								}
								return reaseguradorDTO;
							}
						});
			}
		} finally {
			if (is != null) {
				try {
					is.close();
				} catch (IOException e) {
					resultado = ERROR;
					LogDeMidasEJB3.log("Falla al parsear el tipo de la celda o el nombre de la columna no coincide", Level.SEVERE, e);
				}
			}
		}
		return listaReporteREAS;
	}

}
