package mx.com.afirme.midas2.service.impl.siniestros.configuracion.horario.ajustador;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import javax.validation.Validator;

import mx.com.afirme.midas.sistema.entorno.Entorno;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment.TipoArchivo;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.siniestros.configuracion.horario.ajustador.HorarioAjustadorDAO;
import mx.com.afirme.midas2.domain.siniestros.catalogo.servicio.ServicioSiniestro;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustador.TipoDisponibilidad;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.ajustador.HorarioAjustadorId;
import mx.com.afirme.midas2.domain.siniestros.configuracion.horario.laboral.HorarioLaboral;
import mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador.ConfiguracionDeDisponibilidadDTO;
import mx.com.afirme.midas2.dto.siniestros.configuracion.horario.ajustador.ListadoDeDisponibilidadDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.ListadoService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.impl.siniestros.configuracion.horario.base.ServicioCatalogoBase;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.DisponibilidadAjustadorService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.HorarioAjustadorService;
import mx.com.afirme.midas2.service.siniestros.configuracion.horario.ajustador.HorarioAjustadorService.HorarioAjustadorFiltro;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.utils.CommonUtils;

import org.apache.log4j.Logger;
import org.joda.time.Interval;

@Stateless
public class HorarioAjustadorServiceImpl
		extends
		ServicioCatalogoBase<HorarioAjustadorFiltro, HorarioAjustador, HorarioAjustadorId>
		implements HorarioAjustadorService, DisponibilidadAjustadorService {

	private static final int	MINUTOS_HORA	= 60;

	private static final int	HORAS_DIA	= 24;

	public static final Logger			log									= Logger.getLogger(HorarioAjustadorServiceImpl.class);

	@Resource
	private Validator					validator;

	@EJB
	private HorarioAjustadorDAO			horarioAjustadorDAO;

	@EJB(beanName = "UsuarioServiceDelegate")
	private UsuarioService				usuarioService;

	@EJB
	private EntidadService				entidadService;

	@EJB
	private MailService					mailService;
	
	@EJB
	private ListadoService			listadoService;

	private final String				EMAIL_NOMBRE_DE_ARCHIVO				= "Horarios_Laborales.xls";
	private final TipoArchivo			EMAIL_TIPO_DE_ARCHIVO				= TipoArchivo.XLS;
	private final String				EMAIL_TITULO						= "Horarios Laborales";
	private final String				EMAIL_CUERPO						= "Correo con Horarios Laborales de Ajustadores";

	static final String					rolSubDirectorSiniestrosAutos		= "Rol_M2_SubDirector_Siniestros_Autos";
	static final String					rolDirectorIndemnizacionYOperacion	= "Rol_M2_Director_Indemnizacion_y_Operacion";
	// sistemaContext.getRolCoordinadorCabinajustes();
	static final String					rolCoordinadorCabinajustes			= "R0004";

	static final Map<String, String>	mapRoles							= new HashMap<String, String>() {
																				private static final long	serialVersionUID	= 1L;

																				{
																					put("Coordinador Cabina y Ajustes",
																							rolCoordinadorCabinajustes);
																					put("Subdirector Siniestros Autos",
																							rolSubDirectorSiniestrosAutos);
																					put("Director Indemnización y Operación",
																							rolDirectorIndemnizacionYOperacion);
																				}
																			};

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ListadoDeDisponibilidadDTO> listar(
			HorarioAjustadorFiltro horarioAjustadorFiltro) {
		// validarCamposRequeridosFiltro(horarioAjustadorFiltro);

		List<ListadoDeDisponibilidadDTO> listadoDeDisponibilidadDTOARetornar = new ArrayList<ListadoDeDisponibilidadDTO>();
		if (horarioAjustadorFiltro != null) {
			List<HorarioAjustador> horariosAjustadores = horarioAjustadorDAO
					.buscar(horarioAjustadorFiltro);
			// checar si date no duplica keys
			Map<Date, Map<ServicioSiniestro, Map<TipoDisponibilidad, HorarioAjustador>>> mapaPorFecha = new HashMap<Date, Map<ServicioSiniestro, Map<TipoDisponibilidad, HorarioAjustador>>>();

			for (HorarioAjustador horarioAjustador : horariosAjustadores) {
				Date fecha = horarioAjustador.getId().getFecha();
				ServicioSiniestro ajustador = horarioAjustador.getAjustador();
				String dispCode = horarioAjustador.getTipoDisponibilidadCode();
				String enumVal = HorarioAjustador.ctgDisponibilidad
						.get(dispCode);
				TipoDisponibilidad tipoDisponidilidad = TipoDisponibilidad
						.valueOf(enumVal);

				Map<ServicioSiniestro, Map<TipoDisponibilidad, HorarioAjustador>> mapaPorAjustador;
				Map<TipoDisponibilidad, HorarioAjustador> mapaPorDisponibilidad = new HashMap<HorarioAjustador.TipoDisponibilidad, HorarioAjustador>();

				// Verificar si fecha ya existe en el mapa
				if (mapaPorFecha.containsKey(fecha)) {
					mapaPorAjustador = mapaPorFecha.get(fecha);
					// Verificar si ajustador ya existe en el mapa
					if (mapaPorAjustador.containsKey(ajustador)) {
						mapaPorDisponibilidad = mapaPorAjustador.get(ajustador);
					} else {
						mapaPorDisponibilidad = new HashMap<HorarioAjustador.TipoDisponibilidad, HorarioAjustador>();
					}
				} else {
					mapaPorAjustador = new HashMap<ServicioSiniestro, Map<TipoDisponibilidad, HorarioAjustador>>();

					mapaPorDisponibilidad = new HashMap<HorarioAjustador.TipoDisponibilidad, HorarioAjustador>();
				}
				// Tipo de disponibilida unica por ajustador y fecha, siempre es
				// nueva
				mapaPorDisponibilidad.put(tipoDisponidilidad, horarioAjustador);
				mapaPorAjustador.put(ajustador, mapaPorDisponibilidad);
				mapaPorFecha.put(fecha, mapaPorAjustador);
			}

			for (Date fecha : mapaPorFecha.keySet()) {
				Map<ServicioSiniestro, Map<TipoDisponibilidad, HorarioAjustador>> mapaPorAjustador = mapaPorFecha
						.get(fecha);
				for (ServicioSiniestro ajustador : mapaPorAjustador.keySet()) {

					ListadoDeDisponibilidadDTO listadoDeDisponibilidadDTO = new ListadoDeDisponibilidadDTO();
					listadoDeDisponibilidadDTO.setId(ajustador.getId() + "|"
							+ String.valueOf(fecha.getTime()));
					listadoDeDisponibilidadDTO.setFecha(fecha);
					listadoDeDisponibilidadDTO.setAjustador(ajustador
							.getNombrePersona());

					Map<TipoDisponibilidad, HorarioAjustador> mapaPorDisponibilidad = mapaPorAjustador
							.get(ajustador);
					for (TipoDisponibilidad tipoDisponibilidad : mapaPorDisponibilidad
							.keySet()) {
						HorarioAjustador horarioAjustador = mapaPorDisponibilidad
								.get(tipoDisponibilidad);
						switch (tipoDisponibilidad) {
						case Normal:
							listadoDeDisponibilidadDTO
									.setDispNormal(horarioAjustador);
							break;
						case Guardia:
							listadoDeDisponibilidadDTO
									.setDispGuardia(horarioAjustador);
							break;
						case Apoyo:
							listadoDeDisponibilidadDTO
									.setDispApoyo(horarioAjustador);
							break;
						case Descanso:
							listadoDeDisponibilidadDTO
									.setDispDescanso(horarioAjustador);
							break;
						case Incapacidad:
							listadoDeDisponibilidadDTO
									.setDispIncapacidad(horarioAjustador);
							break;
						case Permiso:
							listadoDeDisponibilidadDTO
									.setDispPermiso(horarioAjustador);
							break;
						case Vacaciones:
							listadoDeDisponibilidadDTO
									.setDispVacaciones(horarioAjustador);
							break;
						}
					}
					listadoDeDisponibilidadDTOARetornar
							.add(listadoDeDisponibilidadDTO);
				}

			}
		}
		
		Collections.sort(listadoDeDisponibilidadDTOARetornar, new Comparator<ListadoDeDisponibilidadDTO>(){
				public int compare(ListadoDeDisponibilidadDTO s1,ListadoDeDisponibilidadDTO s2) {
            		if(s1.getAjustador() != null && s2.getAjustador() != null && s1.getAjustador().compareTo(s1.getAjustador()) != 0) {
            			return s1.getAjustador().compareTo(s2.getAjustador());
            		}
            		else {
            			return s1.getFecha().compareTo(s2.getFecha());
            		}
				}
		});
		
		return listadoDeDisponibilidadDTOARetornar;
	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ListadoDeDisponibilidadDTO> listar(
			ListadoDeDisponibilidadDTO listadoDeDisponibilidadDTO) {
		List<ListadoDeDisponibilidadDTO> listaARetornar = new ArrayList<ListadoDeDisponibilidadDTO>();
		listaARetornar.add(listadoDeDisponibilidadDTO);
		return listaARetornar;
	}

	
	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HorarioAjustador> guardarHorariosAjustadorSP(ConfiguracionDeDisponibilidadDTO config) {
			validarSalvarActualizar(config);
			horarioAjustadorDAO.spGuardarHorariosAjustadorInt(config);
		
		
		return null;
	}
	
	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<HorarioAjustador> guardarHorariosAjustador(ConfiguracionDeDisponibilidadDTO config) {
		validarSalvarActualizar(config);
		//obtener el id del horario laboral según sea capturado
		HorarioLaboral horarioLaboral = entidadService.findById(HorarioLaboral.class, config.getHorarioLaboralId());
		
		
		//obtener las fechas exactas de los días seleccionados para el ajustador. Esto dentro del rango de fechas seleccionados.
		List<Date> fechasDias = obtenerFechasDias(config.getFechaInicial(),config.getFechaFinal(), config.getDiasSeleccion());

		//obtener los ajustadores en base a los ids proporcionados
		List<ServicioSiniestro> ajustadores = new ArrayList<ServicioSiniestro>();
		for (String ajustadorId : config.getAjustadoresIds()) 
		{
			ajustadores.add(entidadService.findById(ServicioSiniestro.class, Long.parseLong(ajustadorId)));
		}

		// Iteracion para validar:
		for (Date fecha : fechasDias) {

			for (ServicioSiniestro ajustador : ajustadores) {
				//obtener horarios del ajustador especificado por fecha
				List<HorarioAjustador> horariosPrevios = obtenerHorariosDelDia(ajustador, fecha);

				HorarioAjustador horarioAjustador = new HorarioAjustador();
				horarioAjustador.setAjustador(ajustador);
				horarioAjustador.setOficina(ajustador.getOficina());
				// Fecha es seteada mediante id
				HorarioAjustadorId id = new HorarioAjustadorId();
				id.setFecha(fecha);
				horarioAjustador.setId(id);
				//
				horarioAjustador.setHorarioLaboral(horarioLaboral);
				horarioAjustador.setComentarios(config.getComentarios());
				horarioAjustador.setTipoDisponibilidadCode(config.getTipoDisponibilidadCode());
				
				validarDisponibilidadUnicaDelDia(horariosPrevios,horarioAjustador);
				validarHorariosPreviosDelDia(horariosPrevios, horarioAjustador);
				// if (horarioAjustador.getFechaHoraEntrada().getDay() !=
				// horarioAjustador
				// .getFechaHoraSalida().getDay()) {
				//
				// Date tomorrow = new Date(fecha.getTime()
				// + (1000 * 60 * 60 * 24));
				// List<HorarioAjustador> horariosPreviosMañana =
				// obtenerHorariosDelDia(
				// ajustador, tomorrow);
				//
				// validarHorariosPreviosDelDia(horariosPreviosMañana,
				// horarioAjustador);
				// }
				// setearValoresControl(horarioAjustador, null);
				// guardarActualizar(horarioAjustador);
			}
		}
		// Iteracion para guardar
		for (Date fecha : fechasDias) {

			for (ServicioSiniestro ajustador : ajustadores) {

				HorarioAjustador horarioAjustador = new HorarioAjustador();
				horarioAjustador.setAjustador(ajustador);
				horarioAjustador.setOficina(ajustador.getOficina());
				// Fecha es seteada mediante id
				HorarioAjustadorId id = new HorarioAjustadorId();
				id.setFecha(fecha);
				horarioAjustador.setId(id);
				//
				horarioAjustador.setHorarioLaboral(horarioLaboral);
				horarioAjustador.setComentarios(config.getComentarios());
				horarioAjustador.setTipoDisponibilidadCode(config.getTipoDisponibilidadCode());
				setearValoresControl(horarioAjustador, null);
				guardarActualizar(horarioAjustador);
			}
		}
		return null;
	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enviarPorCorreoGrupal(String[] enviarARolesSeleccionados,
			List<Long> ajustadoresIds, byte[] bytes) {
		List<String> para = new ArrayList<String>();
		ByteArrayAttachment adjunto = new ByteArrayAttachment(
				EMAIL_NOMBRE_DE_ARCHIVO, EMAIL_TIPO_DE_ARCHIVO, bytes);

		for (Long ajustadorId : ajustadoresIds) {
			ServicioSiniestro ajustador = entidadService.findById(
					ServicioSiniestro.class, ajustadorId);
			if(!ajustador.getPersonaMidas().getContacto().getCorreoPrincipal().equals("") &&
					!(ajustador.getPersonaMidas().getContacto().getCorreoPrincipal() == null)){
				para.add(ajustador.getPersonaMidas().getContacto().getCorreoPrincipal());
			}
		}

		int count1, count2;
		for (count1 = count2 = 0; count2 < enviarARolesSeleccionados.length; ++count2)
			if (!"Ajustadores".equals(enviarARolesSeleccionados[count2]))
				enviarARolesSeleccionados[count1++] = enviarARolesSeleccionados[count2];
		enviarARolesSeleccionados = Arrays.copyOf(enviarARolesSeleccionados,
				count1);

		List<String> list = new ArrayList<String>(
				Arrays.asList(enviarARolesSeleccionados));
		list.removeAll(Arrays.asList("Ajustadores"));
		enviarARolesSeleccionados = new String[list.size()];
		enviarARolesSeleccionados = list.toArray(enviarARolesSeleccionados);

		String[] nombresRol = new String[enviarARolesSeleccionados.length];
		// Hacer array de roles

		for (int i = 0; i < enviarARolesSeleccionados.length; i++) {
			String rol = enviarARolesSeleccionados[i];
			String nombreRol = mapRoles.get(rol);
			nombresRol[i] = nombreRol;
		}

		if (enviarARolesSeleccionados.length > 0) {
			try {
				List<Usuario> usuarios = usuarioService
						.buscarUsuariosPorNombreRol(nombresRol);
				if (usuarios != null) {
					for (Usuario usuario : usuarios) {
						para.add(usuario.getEmail());
					}
				}
			} catch (Exception e) {
				log.error(e.getMessage());
				throwNegocioEJBExeption("Error al obtener usuarios con roles seleccionados");
			}
		}
		try {
			enviarPorCorreo(para, adjunto);
		} catch (Exception e) {
			log.error(e.getMessage());
			throwNegocioEJBExeption("Error al enviar correo");
		}

	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void enviarPorCorreoIndividual(Long ajustadorId, byte[] bytes) {
		List<String> para = new ArrayList<String>();
		ByteArrayAttachment adjunto = new ByteArrayAttachment(
				EMAIL_NOMBRE_DE_ARCHIVO, EMAIL_TIPO_DE_ARCHIVO, bytes);

		ServicioSiniestro ajustador = entidadService.findById(
				ServicioSiniestro.class, ajustadorId);
		if(ajustador.getPersonaMidas().getContacto().getCorreoPrincipal() != null){
			para.add(ajustador.getPersonaMidas().getContacto().getCorreoPrincipal());
		}

		enviarPorCorreo(para, adjunto);

	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	@Deprecated
	public HorarioAjustador borrar(HorarioAjustador horarioAjustador) {
		HorarioAjustador horarioAjustadorABorrar = entidadService.findById(
				HorarioAjustador.class, horarioAjustador.getId());
		if (horarioAjustadorABorrar != null) {
			validarHorarioAjustadorABorrar(horarioAjustadorABorrar);
			entidadService.remove(horarioAjustadorABorrar);
		}
		return horarioAjustadorABorrar;
	}
	
	@Override
	public List<HorarioAjustador> borrar(List<String> horariosIdsSeleccionados) throws NegocioEJBExeption{
		List<HorarioAjustador> horariosABorrar = new ArrayList<HorarioAjustador>();
		HorarioAjustador horario = null;
		HorarioAjustadorId horarioId = null;
		SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
		
		for(String horarioIdString : horariosIdsSeleccionados){
			String[] informacionHorarioId = horarioIdString.split("\\|");
			horarioId = new HorarioAjustadorId();
			try {
				horarioId.setFecha(formatter.parse(informacionHorarioId[0]));
				horarioId.setAjustadorId(Long.parseLong(informacionHorarioId[1]));
				horarioId.setOficinaId(Long.parseLong(informacionHorarioId[2]));
				horarioId.setHorarioLaboralId(Long.parseLong(informacionHorarioId[3]));
				horario = entidadService.findById(HorarioAjustador.class, horarioId);
				horariosABorrar.add(horario);
			} catch (ParseException e) {
				e.printStackTrace();
				throwNegocioEJBExeption("Error en el formato de la fecha: " + informacionHorarioId[0]);
			}
		}
		
		entidadService.removeAll(horariosABorrar);
		
		return horariosABorrar;
	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date getFechaInicioLimite() {
		return CommonUtils.setTimeToMidnight(new Date());
	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date getFechaFinalLimite() {
		Calendar sigMesUltimoDia = Calendar.getInstance();
		sigMesUltimoDia.add(Calendar.MONTH, 1);
		sigMesUltimoDia.set(Calendar.DAY_OF_MONTH,
				sigMesUltimoDia.getActualMaximum(Calendar.DAY_OF_MONTH));
		sigMesUltimoDia.set(Calendar.HOUR_OF_DAY, HORAS_DIA - 1);
		sigMesUltimoDia.set(Calendar.MINUTE, MINUTOS_HORA -1);
		return sigMesUltimoDia.getTime();
	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorarioAjustador obtener(HorarioAjustadorId id) {
		return this.obtener(HorarioAjustador.class, id);
	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public HorarioAjustador guardar(HorarioAjustador entidad) {
		return entidadService.save(entidad);
	}

	// HorarioAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public void validarCamposRequeridosFiltro(HorarioAjustadorFiltro filtro)
			throws NegocioEJBExeption {
		try {
			String msgError = "Seleccciona al menos 2 campos de busqueda";
			int camposRequeridos = 2;

			int contador = filtro.validarAjustadoresIds() ? 0 : 1;
			contador += filtro.validarFechaFinal() ? 0 : 1;
			contador += filtro.validarFechaInicial() ? 0 : 1;
			contador += filtro.validarHorarioFinalCode() ? 0 : 1;
			contador += filtro.validarHorarioInicialCode() ? 0 : 1;
			contador += filtro.validarOficinaId() ? 0 : 1;
			contador += filtro.validarTipoDisponibilidadCode() ? 0 : 1;

			if (contador > camposRequeridos) {
				throwNegocioEJBExeption(msgError);
			}

		} catch (NegocioEJBExeption ne) {
			throw ne;
		} catch (Exception e) {
			log.error(e.getMessage() + e.getStackTrace());
		}

	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date obtenerFechaHoraEntrada(ServicioSiniestro ajustador,
			TipoDisponibilidad tipoDisponibilidad) {
		return obtenerFechaHoraDeEntrada(ajustador, new Date(),
				tipoDisponibilidad);
	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date obtenerFechaHoraSalida(ServicioSiniestro ajustador,
			TipoDisponibilidad tipoDisponibilidad) {
		return obtenerFechaHoraDeSalida(ajustador, new Date(),
				tipoDisponibilidad);
	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date obtenerFechaHoraDeEntrada(ServicioSiniestro ajustador,
			Date fecha, TipoDisponibilidad tipoDisponibilidad) {
		HorarioAjustador horarioAjustador = null;

		List<HorarioAjustador> horariosAjustador = obtenerHorariosDelDia(
				ajustador, fecha);

		for (HorarioAjustador horarioAjustadorIter : horariosAjustador) {
			if (horarioAjustador.getTipoDispEnum() == tipoDisponibilidad) {
				horarioAjustador = horarioAjustadorIter;
			}
		}
		if (horarioAjustador != null) {
			TipoDisponibilidad tipoDisponReal = obtenerDisponibilidadEnHorariosDeAjustador(
					horariosAjustador, horarioAjustador.getFechaHoraEntrada());
			if (tipoDisponReal != null && tipoDisponibilidad != tipoDisponReal) {
				HorarioAjustador horarioAjustadorReal = null;
				for (HorarioAjustador horarioAjustadorIter : horariosAjustador) {
					if (horarioAjustador.getTipoDispEnum() == tipoDisponReal) {
						horarioAjustadorReal = horarioAjustadorIter;
					}
				}
				if (horarioAjustador != horarioAjustadorReal) {
					if (horarioAjustadorReal.getFechaHoraSalida().getTime() > horarioAjustador
							.getFechaHoraSalida().getTime()) {
						return null;
					} else {
						return horarioAjustadorReal.getFechaHoraSalida();
					}
				}
			}

			return horarioAjustador.getFechaHoraEntrada();
		} else {
			return null;
		}
	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Date obtenerFechaHoraDeSalida(ServicioSiniestro ajustador,
			Date fecha, TipoDisponibilidad tipoDisponibilidad) {
		HorarioAjustador horarioAjustador = null;

		List<HorarioAjustador> horariosAjustador = obtenerHorariosDelDia(
				ajustador, fecha);

		for (HorarioAjustador horarioAjustadorIter : horariosAjustador) {
			if (horarioAjustador.getTipoDispEnum() == tipoDisponibilidad) {
				horarioAjustador = horarioAjustadorIter;
			}
		}
		if (horarioAjustador != null) {
			TipoDisponibilidad tipoDisponReal = obtenerDisponibilidadEnHorariosDeAjustador(
					horariosAjustador, horarioAjustador.getFechaHoraSalida());
			if (tipoDisponibilidad != tipoDisponReal) {
				HorarioAjustador horarioAjustadorReal = null;
				for (HorarioAjustador horarioAjustadorIter : horariosAjustador) {
					if (horarioAjustador.getTipoDispEnum() == tipoDisponReal) {
						horarioAjustadorReal = horarioAjustadorIter;
					}
				}
				if (horarioAjustador != horarioAjustadorReal) {
					if (horarioAjustadorReal.getFechaHoraEntrada().getTime() < horarioAjustador
							.getFechaHoraEntrada().getTime()) {
						return null;
					} else {
						return horarioAjustadorReal.getFechaHoraEntrada();
					}
				}
			}

			return horarioAjustador.getFechaHoraSalida();
		} else {
			return null;
		}

	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public TipoDisponibilidad obtenerDisponibilidadAjustadorEnFechaHora(
			ServicioSiniestro ajustador, Date fecha) {
		return obtenerDisponibilidadEnHorariosDeAjustador(
				obtenerHorariosDelDia(ajustador, fecha), fecha);
	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ServicioSiniestro> obtenerAjustadoresConDisponibilidad(
			TipoDisponibilidad tipoDisponibilidad, Long oficinaId) {
		return obtenerAjustadoresConDisponibilidad(tipoDisponibilidad,
				new Date(), oficinaId);
	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ServicioSiniestro> obtenerAjustadoresConDisponibilidad(
			TipoDisponibilidad tipoDisponibilidad, Date fechaHora, Long oficinaId) {
		List<ServicioSiniestro> listadoAjustadoresARegresar = new ArrayList<ServicioSiniestro>();
		List<ServicioSiniestro> listadoAjustadores = new ArrayList<ServicioSiniestro>();
		HorarioAjustadorFiltro horarioAjustadorFltro = new HorarioAjustadorFiltro();
		horarioAjustadorFltro.setFechaFinal(fechaHora);
		horarioAjustadorFltro.setFechaInicial(fechaHora);		
		horarioAjustadorFltro.setTipoDisponibilidadCode(tipoDisponibilidad
				.getCodigo());		
		if(oficinaId != null){
			horarioAjustadorFltro.setOficinaId(oficinaId);
		}

		// Busca todos los ajustadores con esa disponibilidad en ese dia
		List<HorarioAjustador> listadoHorarioAjustador = horarioAjustadorDAO
				.buscar(horarioAjustadorFltro);
		for (HorarioAjustador horarioAjustador : listadoHorarioAjustador) {
			if (!listadoAjustadores.contains(horarioAjustador.getAjustador())) {
				listadoAjustadores.add(horarioAjustador.getAjustador());
			}
		}
		// Por cada horario
		for (ServicioSiniestro ajustador : listadoAjustadores) {
			// Busca los horarios del ajustador para la fecha
			listadoHorarioAjustador = obtenerHorariosDelDia(ajustador,
					fechaHora);
			// Si para la fecha hora esta disponible, agregar a lista
			TipoDisponibilidad disponibilidad = obtenerDisponibilidadEnHorariosDeAjustador(
					listadoHorarioAjustador, fechaHora);
			if (disponibilidad == tipoDisponibilidad) {
				listadoAjustadoresARegresar.add(ajustador);
			}
		}
		return listadoAjustadoresARegresar;
	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean estaEnHorarioDisponible(ServicioSiniestro ajustador) {
		return estaEnHorarioDisponible(ajustador, new Date());
	}

	// DisponibilidadAjustadorService
	/**
	 * {@inheritDoc}
	 */
	@Override
	public Boolean estaEnHorarioDisponible(ServicioSiniestro ajustador,
			Date fechaHora) {
		return obtenerDisponibilidadAjustadorEnFechaHora(ajustador, fechaHora)
				.estaDisponible();
	}

	// DisponibilidadAjustadorService
	/**
	 * TODO: {@inheritDoc}
	 */
	@Override
	public Boolean estaDisponible(ServicioSiniestro ajustador) {
		return null;
	}

	// region validates
	/**
	 * Funcion que valida la RDN {XXXXXXXX-XXXXX-XXXX-XXXXX-XXXXXXXXXXXX} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: XXXXX XXX XXXX
	 * - RDN XXXXX
	 */
	private void validarHorariosPreviosDelDia(
			List<HorarioAjustador> horariosPrevios,
			HorarioAjustador horarioAjustador) throws NegocioEJBExeption {
		String msgErrorPlantilla = "Existe un horario para el ajustador: ajustadorTal con horario: horarioTal para el dia: diaTal";
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd/MM/yyyy");

		Long nuevoHoraEntrada = horarioAjustador.getFechaHoraEntrada()
				.getTime();
		Long nuevoHoraSalida = horarioAjustador.getFechaHoraSalida().getTime();
		Interval nuevoHorarioInterval = new Interval(nuevoHoraEntrada,
				nuevoHoraSalida);
		for (HorarioAjustador horarioPrevio : horariosPrevios) {
			if (horarioAjustador.getHorarioLaboral().getId() == horarioPrevio
					.getHorarioLaboral().getId()) {
				continue;
			}
			if (horarioPrevio.getTipoDispEnum().estaDisponible() == horarioAjustador
					.getTipoDispEnum().estaDisponible()) {
				Long previoHoraEntrada = horarioPrevio.getFechaHoraEntrada()
						.getTime();
				Long previoHoraSalida = horarioPrevio.getFechaHoraSalida()
						.getTime();

				Interval previoHorarioInterval = new Interval(
						previoHoraEntrada, previoHoraSalida);
				if (nuevoHorarioInterval.contains(previoHorarioInterval) && !nuevoHorarioInterval.equals(previoHorarioInterval)) {
					String msgError = "";
					msgError = (msgErrorPlantilla.replaceFirst("ajustadorTal",
							horarioAjustador.getAjustador().getNombrePersona()))
							.replaceFirst(
									"horarioTal",
									horarioPrevio.getHorarioLaboral()
											.getDescripcion()).replaceFirst(
									"diaTal",
									dateFormat.format(horarioAjustador
											.getFecha()));
					throwNegocioEJBExeption(msgError);
				}
			}
		}
	}

	/**
	 * Funcion que valida la RDN {XXXXXXXX-XXXXX-XXXX-XXXXX-XXXXXXXXXXXX} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: XXXXX XXX XXXX
	 * - RDN XXXXX
	 */
	private void validarDisponibilidadUnicaDelDia(
			List<HorarioAjustador> horariosPrevios,
			HorarioAjustador horarioAjustador) throws NegocioEJBExeption {
		String msgErrorPlantilla = "Ya existe un horario para el ajustador: ajustadorTal con disponibilidad: disponibilidadTal para el dia: diaTal - Borralo si es necesario cambiar el horario";
		SimpleDateFormat dateFormat = new SimpleDateFormat("EEEE dd/MM/yyyy");
		for (HorarioAjustador horarioPrevio : horariosPrevios) {
			if (horarioPrevio.getTipoDispEnum() == horarioAjustador
					.getTipoDispEnum()
					&& horarioPrevio.getHorarioLaboral() != horarioAjustador
							.getHorarioLaboral()) {
				String msgError = "";
				msgError = (msgErrorPlantilla.replaceFirst("ajustadorTal",
						horarioAjustador.getAjustador().getNombrePersona()))
						.replaceFirst("disponibilidadTal",
								horarioAjustador.getTipoDispEnum().toString())
						.replaceFirst("diaTal",
								dateFormat.format(horarioAjustador.getFecha()));
				throwNegocioEJBExeption(msgError);
			}
		}
	}

	/**
	 * Funcion que valida la RDN {117567D2-C7B2-4333-8FC1-FB6363A0DC22} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: Campos
	 * Obligatorios Configuración de Disponibilidad
	 */
	public void validarSalvarActualizar(ConfiguracionDeDisponibilidadDTO config) {
		Set<ConstraintViolation<ConfiguracionDeDisponibilidadDTO>> constraintViolations = validator
				.validate(config);
		if (constraintViolations.size() > 0) {
			throw new ConstraintViolationException(
					new HashSet<ConstraintViolation<?>>(constraintViolations));
		}

		String msgCampoRequerido = "Campo Requerido";
		String msgSemanaValido = "Selecciona al menos un dia de la semana";
		String msgTipoDisponibilidad = "Selecciona el tipo de disponibilidad";
		String msgFechaFinalLimiteValido = "La fecha final tiene que ser menor";
		String msgFechaInicialLimiteValido = "La fecha inicial tiene que ser mayor";

		if (config.getFechaInicial() == null) {
			throwNegocioEJBExeption("fechaInicial " + msgCampoRequerido);
		} else {
			if (config.getFechaInicial().compareTo(getFechaInicioLimite()) < 0) {
				throwNegocioEJBExeption("fechaInicial "
						+ msgFechaInicialLimiteValido);
			}
		}

		if (config.getFechaFinal() == null) {
			throwNegocioEJBExeption("fechaFinal " + msgCampoRequerido);
		} else {
			if (config.getFechaFinal().compareTo(getFechaFinalLimite()) > 0) {
				throwNegocioEJBExeption("fechaFinal "
						+ msgFechaFinalLimiteValido);
			}
		}

		if (config.getDiasSeleccion() == null
				|| config.getDiasSeleccion().equals("")) {
			throwNegocioEJBExeption("diasSeleccion " + msgSemanaValido);
		}

		if (config.getAjustadoresIds() == null
				|| config.getAjustadoresIds().size() == 0) {
			throwNegocioEJBExeption("ajustadoresIds " + msgCampoRequerido);
		}

		if (config.getOficinaId() == null
				|| config.getOficinaId().equalsIgnoreCase("")) {
			throwNegocioEJBExeption("oficinaId " + msgCampoRequerido);
		}

		if (config.getHorarioLaboralId() == null) {
			throwNegocioEJBExeption("horarioLaboralId " + msgCampoRequerido);
		}

		if (config.getTipoDisponibilidadCode() == null
				|| config.getTipoDisponibilidadCode().equalsIgnoreCase("")) {
			throwNegocioEJBExeption("tipoDisponibilidadCode "
					+ msgTipoDisponibilidad);
		}
	}

	/**
	 * Funcion que valida la RDN {XXXXXXXX-XXXX-XXXXX-XXXX-XXXXXXXXXXXX} /
	 * Analisis MIDAS II.Requirements Model.Requerimientos MODULO SINIESTROS
	 * AUTOS.CABINA.Configurador de Horarios de Ajustadores.RDN: XXXXXX
	 */
	private void validarHorarioAjustadorABorrar(
			HorarioAjustador horarioAjustadorABorrar) {
		String msgError = "No se puede borrar un horario anterior al dia de hoy";
		Date fechaABorrar = CommonUtils
				.setTimeToMidnight(horarioAjustadorABorrar.getFecha());
		Date fechaHoy = CommonUtils.setTimeToMidnight(new Date());

		if (fechaABorrar.getTime() < fechaHoy.getTime()) {
			throwNegocioEJBExeption(msgError);
		}
	}

	// endregion validates

	// region Private functions
	private void guardarActualizar(HorarioAjustador horarioAjustador) {
		try{
		entidadService.save(horarioAjustador);
		}catch(Exception e){
			e.printStackTrace();
			System.out.println(e.getMessage());
		}
	}

	private List<Date> obtenerFechasDias(Date fechaInicial, Date fechaFinal,
			String diasSeleccion) {
		List<Date> listaDatesARetornar = new ArrayList<Date>();

		// 1 - Domingo, 2 - Lunes, 3 - Martes ... 7 - Sabado
		// Lista para guardar el Integer de los dias seleccionados
		List<Integer> days_of_week = new ArrayList<Integer>();

		Calendar calendarioAIterar = Calendar.getInstance();
		// calendarioAIterar.setFirstDayOfWeek(Calendar.MONDAY);

		// Lunes=>2, Martes=>3 ... Sabado=>7, Domingo=>1
		Map<String, Integer> mapDiasSemana = calendarioAIterar.getDisplayNames(
				Calendar.DAY_OF_WEEK, Calendar.LONG, new Locale("es"));

		for (String diaSem : mapDiasSemana.keySet()) {
			if (diasSeleccion.contains(diaSem))
				days_of_week.add(mapDiasSemana.get(diaSem));
		}

		// Inicializar Calendario fecha
		calendarioAIterar.setTime(fechaInicial);

		// HACKME: iterar solo los dias de la semana seleccionados
		while (!calendarioAIterar.getTime().after(fechaFinal)) {
			Integer day_of_week_iterado = calendarioAIterar
					.get(Calendar.DAY_OF_WEEK);
			if (days_of_week.contains(day_of_week_iterado)) {
				listaDatesARetornar.add(calendarioAIterar.getTime());
			}
			calendarioAIterar.add(Calendar.DATE, 1);
		}
		return listaDatesARetornar;
	}

	private List<HorarioAjustador> obtenerHorariosDelDia(ServicioSiniestro ajustador, Date fecha) {
		HorarioAjustadorFiltro filtro = new HorarioAjustadorFiltro();

		List<String> ids = new ArrayList<String>();
		ids.add(ajustador.getId().toString());
		filtro.setAjustadoresIds(ids);
		filtro.setFechaFinal(fecha);
		filtro.setFechaInicial(fecha);

		return horarioAjustadorDAO.buscar(filtro);
	}

	private TipoDisponibilidad obtenerDisponibilidadEnHorariosDeAjustador(
			List<HorarioAjustador> horariosAjustador, Date fechaHora) {
		HorarioAjustador horario = obtenerHorarioValidoEnFechaHora(
				horariosAjustador, fechaHora);
		if (horario != null)
			return getTipoDispEnum(horario);
		else
			return null;
	}

	private TipoDisponibilidad getTipoDispEnum(HorarioAjustador horario){
		Map<String, String> ctgTipoDisponibilidad = listadoService.obtenerCatalogoValorFijo(HorarioAjustador.ctgDisponibilidadTipo);
		String enumVal = ctgTipoDisponibilidad.get(horario.getTipoDisponibilidadCode());
		return TipoDisponibilidad.valueOf(enumVal);
	}


	private void throwNegocioEJBExeption(String msgError)
			throws NegocioEJBExeption {
		String[] mensajesArray = { msgError };
		throw new NegocioEJBExeption("Error", mensajesArray, "validacion");
	}

	private void setearValoresControl(HorarioAjustador horarioAjustador,
			HorarioAjustador horarioPrevio) {
		String codigoUsuarioActual = this.usuarioService.getUsuarioActual()
				.getNombreUsuario();
		String codigoUsuarioCreacion = (horarioPrevio == null) ? codigoUsuarioActual
				: horarioPrevio.getCodigoUsuarioCreacion();
		Date fechaCreacion = (horarioPrevio == null) ? new Date()
				: horarioPrevio.getFechaCreacion();

		horarioAjustador.setFechaModificacion(new Date());
		horarioAjustador.setCodigoUsuarioModificacion(codigoUsuarioActual);

		horarioAjustador.setFechaCreacion(fechaCreacion);
		horarioAjustador.setCodigoUsuarioCreacion(codigoUsuarioCreacion);

	}

	private void enviarPorCorreo(List<String> para,
			ByteArrayAttachment adjunto, List<String> cc, List<String> cco) {
		String titulo = EMAIL_TITULO;
		String cuerpoMensaje = EMAIL_CUERPO;

		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		adjuntos.add(adjunto);

		log.debug("Envio de correo con datos: ");
		log.debug("para: " + para.toString());
		log.debug("cc: " + cc.toString());
		log.debug("cco: " + cco.toString());
		log.debug("titulo:  " + titulo);
		log.debug("mensaje: " + cuerpoMensaje);
		log.debug("aduntos: " + adjuntos.toString());
		try{
			mailService.sendMessageWithExceptionHandler(para, 
														titulo, 
														cuerpoMensaje, 
														Entorno.obtenerVariable("email.emailFromAddress", "midasweb@afirme.com"), 
														null, 
														adjuntos, 
														cc, 
														cco);
		}catch(Exception e){
				throwNegocioEJBExeption("Ocurrió un error al enviar los correos");
		}
	}

	private void enviarPorCorreo(List<String> para, ByteArrayAttachment adjunto) {  
		enviarPorCorreo(para, adjunto, new ArrayList<String>(),
				new ArrayList<String>());
	}

	private HorarioAjustador obtenerHorarioValidoEnFechaHora(
			List<HorarioAjustador> horariosAjustador, Date fechaHora) {
		HorarioAjustador horarioAjustadorValido = null;

		// validar mas de un horario
		if (horariosAjustador == null)
			return null;

		if (horariosAjustador.size() == 0)
			return null;

//		if (horariosAjustador.size() == 1)
//			return horariosAjustador.get(0);

		// validar horarios son del mismo ajustador
		ServicioSiniestro ajustador = null;
		for (HorarioAjustador horarioAjustador : horariosAjustador) {
			if (ajustador == null)
				ajustador = horarioAjustador.getAjustador();

			if (ajustador.getId() != horarioAjustador.getAjustador().getId())
				throwNegocioEJBExeption("Error al obtener horario valido, horarios no son del mismo ajustador");
		}

		List<HorarioAjustador> horariosAjustadorEnFechaHoras = new ArrayList<HorarioAjustador>();

		for (HorarioAjustador horarioAjustador : horariosAjustador) {

			if (horarioAjustador.getFechaHoraEntrada().getTime() < fechaHora
					.getTime()
					&& horarioAjustador.getFechaHoraSalida().getTime() > fechaHora
							.getTime()) {
				horariosAjustadorEnFechaHoras.add(horarioAjustador);
			}
		}

		for (HorarioAjustador horarioAjustador : horariosAjustadorEnFechaHoras) {
			if (horarioAjustadorValido == null)
				horarioAjustadorValido = horarioAjustador;

			if (horarioAjustador.getFechaModificacion().getTime() > horarioAjustadorValido
					.getFechaModificacion().getTime())
				horarioAjustadorValido = horarioAjustador;
		}

		return horarioAjustadorValido;

	}

	// endregion Private functions
}
