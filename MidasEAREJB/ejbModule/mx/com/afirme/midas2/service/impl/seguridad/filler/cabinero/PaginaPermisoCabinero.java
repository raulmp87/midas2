/**
 * Clase que llena Paginas y Permisos para el rol de Cabinero
 */
package mx.com.afirme.midas2.service.impl.seguridad.filler.cabinero;

import java.util.ArrayList;
import java.util.List;

import mx.com.afirme.midas.sistema.seguridad.Pagina;
import mx.com.afirme.midas.sistema.seguridad.PaginaPermiso;
import mx.com.afirme.midas.sistema.seguridad.Permiso;

/**
 * @author andres.avalos
 *
 */
public class PaginaPermisoCabinero {

	private List<Permiso> listaPermiso = new ArrayList<Permiso>();
	private List<PaginaPermiso> listaPaginaPermiso = new ArrayList<PaginaPermiso>();
	
	public PaginaPermisoCabinero(List<Permiso> listaPermiso) {
		this.listaPermiso = listaPermiso;
	}
	
	private Pagina nuevaPagina (String nombrePaginaJSP, String nombreAccionDo) {
		return new Pagina(new Integer("1"), nombrePaginaJSP.trim(), nombreAccionDo.trim(), "Descripcion pagina");
	}
	
	public List<PaginaPermiso> obtienePaginaPermisos() {
		
		//Permisos  0 =AG , 1 =AC , 2 =BR , 3 =RE , 4 =EX , 5 =AD , 6 =VD , 7 = AS, 8 = BU
		// 9 = CO, 10 = CT, 11 = GU, 12 = NV, 13 = RE, 14 = SE
		
		PaginaPermiso pp;
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("crearReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/mostrarAgregar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/crearReporte.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listaPolizasReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/listarPolizasSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosPoliza.jsp", "/MidasWeb/siniestro/cabina/reportePoliza.do"));
		pp.getPermisos().add(listaPermiso.get(11)); //GU
		pp.getPermisos().add(listaPermiso.get(13)); //RE
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/mostrarListaReporteDetallePoliza.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("datosPolizaCrear.jsp", "/MidasWeb/siniestro/cabina/datosPolizaCrear.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/obtenerListaAjustadores.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/guardarReporte.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/listar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp", "/MidasWeb/siniestro/cabina/reporteSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/listarTodos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosReporteSiniestro.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosReporteSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosPolizaRS.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosPoliza.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("desplegarDatosEstadoSiniestroRS.jsp", "/MidasWeb/siniestro/cabina/desplegarDatosEstadoSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("estadoSiniestro.jsp", "/MidasWeb/siniestro/cabina/reporteEstadoSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/repoGuadSini.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("", "/MidasWeb/siniestro/cabina/asignarAjustador.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("caratulaSiniestro.jsp", "/MidasWeb/siniestro/reportes/mostrarCaratulaSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listar.jsp", "/MidasWeb/siniestro/salvamento/listar.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIngresos.jsp", "/MidasWeb/siniestro/finanzas/listarIngresos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarGastos.jsp", "/MidasWeb/siniestro/finanzas/listarGastos.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarOrdenesDePago.jsp", "/MidasWeb/siniestro/finanzas/listarOrdenesDePago.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarDocumentosSiniestro.jsp", "/MidasWeb/siniestro/documentos/listarDocumentosSiniestro.do"));
		listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
		pp.setPagina(nuevaPagina("listarIndemnizaciones.jsp", "/MidasWeb/siniestro/finanzas/indemnizacion/listarIndemnizaciones.do"));
		listaPaginaPermiso.add(pp);
		
		//Permisos agregados del excel
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaIncisosPoliza.jsp","/MidasWeb/siniestro/cabina/listarIncisosPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaSeccionSubIncisos.jsp","/MidasWeb/siniestro/cabina/listarSeccionSubIncisos.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaCoberturasRiesgo.jsp","/MidasWeb/siniestro/cabina/listarCoberturasRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listaIncisosPoliza.jsp","/MidasWeb/siniestro/cabina/listarIncisosUbicacionPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarIndemnizaciones.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/listarIndemnizaciones.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("listarDetalleIndemnizacion.jsp","/MidasWeb/siniestro/finanzas/indemnizacion/listarDetalleIndemnizacion.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/poliza/guardarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("afectarCoberturaRiesgo.jsp","/MidasWeb/siniestro/poliza/afectarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarAfectarCoberturaRiesgo.jsp","/MidasWeb/siniestro/poliza/desplegarAfectarCoberturaRiesgo.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/asignarCoordinadorPorZona.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteListarPoliza.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("reporteDeSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/repoGuadSini.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/modificarReporte.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/listarTodos")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/listarFiltrado.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("modificarReporteSiniestro.jsp","/MidasWeb/siniestro/cabina/mostrarModificar.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("cabina/desplegarReporteSiniestro.jsp","/MidasWeb/siniestro/cabina/mostrarDetalle.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/comboCoordinadores.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/confirmarAjustador.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/rechazarAjustador.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/reportes/mostrarReporteSPSinParametros.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("estadoSiniestro.jsp","/MidasWeb/siniestro/cabina/reporteGuardarSiniestro.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("transportes.jsp","/MidasWeb/siniestro/cabina/reporteTransportes.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("","/MidasWeb/siniestro/cabina/direccionPoliza.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarDatosTransporteRS.jsp","/MidasWeb/siniestro/cabina/desplegarDatosTransporte.do")); listaPaginaPermiso.add(pp);
		pp = new PaginaPermiso();pp.setPagina(nuevaPagina("desplegarDatosPreguntasRS.jsp","/MidasWeb/siniestro/cabina/desplegarDatosPreguntasEspeciales.do")); listaPaginaPermiso.add(pp);
		
		pp = new PaginaPermiso();
	    pp.setPagina(nuevaPagina("","/MidasWeb/sistema/mensajePendiente/mostrarHelp.do"));
	    listaPaginaPermiso.add(pp);

		return this.listaPaginaPermiso;
	}
	
	
	
}
