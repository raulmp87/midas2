package mx.com.afirme.midas2.service.impl.bitemporal.endoso.cancelacion;

import java.math.BigDecimal;
import java.util.Date;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.endoso.EndosoDTO;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.bitemporal.EntidadContinuityDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.BitemporalCotizacion;
import mx.com.afirme.midas2.dto.TipoAccionDTO;
import mx.com.afirme.midas2.exeption.NegocioEJBExeption;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cancelacion.CancelacionBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.endoso.cotizacion.auto.EndosoService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.MovimientoEndosoBitemporalService;
import mx.com.afirme.midas2.service.bitemporal.suscripcion.emision.EmisionEndosoBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;

import org.joda.time.Interval;

import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class CancelacionBitemporalServiceImpl implements CancelacionBitemporalService {

	
	public void cancelacionPolizaAutomatico(BigDecimal polizaId ,short endosoId,
			Date fechaInicioVigencia, short tipoEndoso, short motivoEndoso) {

		cancelacionPoliza(polizaId, endosoId, fechaInicioVigencia, tipoEndoso, motivoEndoso);
	}
	
	
	public EndosoDTO cancelacionPoliza(BigDecimal polizaId,short endosoId, 
			Date fechaInicioVigencia, short tipoEndoso, short motivoEndoso) {

		BitemporalCotizacion cotizacion=null;
		try {
			//se realiza la cancelación de póliza.
			cotizacion = endosoService.getInitCotizacionEndoso(polizaId, fechaInicioVigencia, tipoEndoso, motivoEndoso, TipoAccionDTO.getNuevoEndosoCot());	
		} catch (NegocioEJBExeption e) {
			//si pasa la validación de que tiene Records en proceso, se ignora y se realiza rollback.
			if (e.getErrorCode().equals("NE_6")) {
				//se realiza el rollback para quitar los que estan en proceso.
				cotizacion.getContinuity().getCotizaciones().rollbackTwoPhase();
			}else //de otra manera se propaga la exepción.
				throw e;
		}
		//Se finaliza 
		cotizacion.getContinuity().getCotizaciones().end(new IntervalWrapper(new Interval(TimeUtils.getDateTime(fechaInicioVigencia).getMillis(), TimeUtils.END_OF_TIME)), true);
		
		//se guarda el controlEndosoCot
		endosoService.guardaControlEndosoCotizacion(cotizacion, CotizacionDTO.ESTATUS_COT_EN_PROCESO, fechaInicioVigencia);
		
		// se emite .
		EndosoDTO endosoDTO =  emisionEndosoBitemporal.emiteEndoso(cotizacion.getContinuity(). getId(),TimeUtils.getDateTime(fechaInicioVigencia), tipoEndoso);

		return endosoDTO;
	}
	
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public BitemporalCotizacion guardaCotizacionCancelacionPolizaAutomatico(BigDecimal polizaId ,short endosoId,
			Date fechaInicioVigencia, short tipoEndoso, short claveMotivoEndoso, short migrada) {					

		BitemporalCotizacion cotizacion=null;
		try {
			//se realiza la cancelación de póliza.
			cotizacion = endosoService.getInitCotizacionEndoso(polizaId, fechaInicioVigencia, tipoEndoso, claveMotivoEndoso, TipoAccionDTO.getNuevoEndosoCot(), migrada);	
		} catch (NegocioEJBExeption e) {
			//si pasa la validación de que tiene Records en proceso, se ignora y se realiza rollback.
			if (e.getErrorCode().equals("NE_6")) {
				//se realiza el rollback para quitar los que estan en proceso.
				cotizacion.getContinuity().getCotizaciones().rollbackTwoPhase();
			}else //de otra manera se propaga la exepción.
				throw e;
		}
		//Se finaliza 
		cotizacion.getContinuity().getCotizaciones().end(new IntervalWrapper(new Interval(TimeUtils.getDateTime(fechaInicioVigencia).getMillis(), TimeUtils.END_OF_TIME)), true);
		
		//se guarda el controlEndosoCot
		endosoService.guardaControlEndosoCotizacion(cotizacion, CotizacionDTO.ESTATUS_COT_EN_PROCESO, fechaInicioVigencia);		

		return cotizacion;
	}	


	@EJB
	private EmisionEndosoBitemporalService emisionEndosoBitemporal;
	
	@EJB
	private EntidadService entidadService;
	@EJB
	private EntidadBitemporalService entidadBitemporalService;

	@EJB
	private MovimientoEndosoBitemporalService movEndosoBitemporalServ;
	
	@EJB
	private EndosoService endosoService;
	
	@EJB
	protected MovimientoEndosoBitemporalService movimientoEndosoBitemporalService;
	
	@EJB
	protected EntidadContinuityDao entidadContinuityDao;
}

