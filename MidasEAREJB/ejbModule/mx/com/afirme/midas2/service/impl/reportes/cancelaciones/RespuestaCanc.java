package mx.com.afirme.midas2.service.impl.reportes.cancelaciones;

public class RespuestaCanc {

	private static final long serialVersionUID = 1L;
	private String emailAgente;
	private String idCotizacion;
	private String poliza;
	private String num_solicitante;
	private String desc_moneda;
	private String nom_producto;
	private String id_agente;
	
	private String desc_sit_poliza;
	private String desc_mot_sit_pol;
	private String f_vencto_recibo;
	private String rec_totales;
	private String rec_pendientes;
	private String suma_pendiente;
	private String imp_prima_total;
	private String saldo_primas_deposito;
	private String num_endoso;
	
	private String num_prorroga;
	private String fecha_fin_prorroga;
	private String sit_prorroga;
	private String nom_forma_pago;
	private String num_cond_cobro;
	private String nombre;
	
	
	private String telef_casa;
	private String e_mail_agente;
	private String nom_supervisoria;
	private String nom_gerencia;
	private String nom_oficina;
	private String e_mail_oficina;
	private String siniestro;
	private String fecha_ejecucion;
	private String fecha_vencto_recibo;
	private String prima_total;
	
	public String getEmailAgente() {
		return emailAgente;
	}
	public void setEmailAgente(String emailAgente) {
		this.emailAgente = emailAgente;
	}
	public String getIdCotizacion() {
		return idCotizacion;
	}
	public void setIdCotizacion(String idCotizacion) {
		this.idCotizacion = idCotizacion;
	}
	public String getPoliza() {
		return poliza;
	}
	public void setPoliza(String poliza) {
		this.poliza = poliza;
	}
	public String getNum_solicitante() {
		return num_solicitante;
	}
	public void setNum_solicitante(String num_solicitante) {
		this.num_solicitante = num_solicitante;
	}
	public String getDesc_moneda() {
		return desc_moneda;
	}
	public void setDesc_moneda(String desc_moneda) {
		this.desc_moneda = desc_moneda;
	}
	public String getNom_producto() {
		return nom_producto;
	}
	public void setNom_producto(String nom_producto) {
		this.nom_producto = nom_producto;
	}
	public String getId_agente() {
		return id_agente;
	}
	public void setId_agente(String id_agente) {
		this.id_agente = id_agente;
	}
	public String getDesc_sit_poliza() {
		return desc_sit_poliza;
	}
	public void setDesc_sit_poliza(String desc_sit_poliza) {
		this.desc_sit_poliza = desc_sit_poliza;
	}
	public String getDesc_mot_sit_pol() {
		return desc_mot_sit_pol;
	}
	public void setDesc_mot_sit_pol(String desc_mot_sit_pol) {
		this.desc_mot_sit_pol = desc_mot_sit_pol;
	}
	public String getF_vencto_recibo() {
		return f_vencto_recibo;
	}
	public void setF_vencto_recibo(String f_vencto_recibo) {
		this.f_vencto_recibo = f_vencto_recibo;
	}
	public String getRec_totales() {
		return rec_totales;
	}
	public void setRec_totales(String rec_totales) {
		this.rec_totales = rec_totales;
	}
	public String getRec_pendientes() {
		return rec_pendientes;
	}
	public void setRec_pendientes(String rec_pendientes) {
		this.rec_pendientes = rec_pendientes;
	}
	public String getSuma_pendiente() {
		return suma_pendiente;
	}
	public void setSuma_pendiente(String suma_pendiente) {
		this.suma_pendiente = suma_pendiente;
	}
	public String getImp_prima_total() {
		return imp_prima_total;
	}
	public void setImp_prima_total(String imp_prima_total) {
		this.imp_prima_total = imp_prima_total;
	}
	public String getSaldo_primas_deposito() {
		return saldo_primas_deposito;
	}
	public void setSaldo_primas_deposito(String saldo_primas_deposito) {
		this.saldo_primas_deposito = saldo_primas_deposito;
	}
	public String getNum_endoso() {
		return num_endoso;
	}
	public void setNum_endoso(String num_endoso) {
		this.num_endoso = num_endoso;
	}
	public String getNum_prorroga() {
		return num_prorroga;
	}
	public void setNum_prorroga(String num_prorroga) {
		this.num_prorroga = num_prorroga;
	}
	public String getFecha_fin_prorroga() {
		return fecha_fin_prorroga;
	}
	public void setFecha_fin_prorroga(String fecha_fin_prorroga) {
		this.fecha_fin_prorroga = fecha_fin_prorroga;
	}
	public String getSit_prorroga() {
		return sit_prorroga;
	}
	public void setSit_prorroga(String sit_prorroga) {
		this.sit_prorroga = sit_prorroga;
	}
	public String getNom_forma_pago() {
		return nom_forma_pago;
	}
	public void setNom_forma_pago(String nom_forma_pago) {
		this.nom_forma_pago = nom_forma_pago;
	}
	public String getNum_cond_cobro() {
		return num_cond_cobro;
	}
	public void setNum_cond_cobro(String num_cond_cobro) {
		this.num_cond_cobro = num_cond_cobro;
	}
	public String getNombre() {
		return nombre;
	}
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getTelef_casa() {
		return telef_casa;
	}
	public void setTelef_casa(String telef_casa) {
		this.telef_casa = telef_casa;
	}
	public String getE_mail_agente() {
		return e_mail_agente;
	}
	public void setE_mail_agente(String e_mail_agente) {
		this.e_mail_agente = e_mail_agente;
	}
	public String getNom_supervisoria() {
		return nom_supervisoria;
	}
	public void setNom_supervisoria(String nom_supervisoria) {
		this.nom_supervisoria = nom_supervisoria;
	}
	public String getNom_gerencia() {
		return nom_gerencia;
	}
	public void setNom_gerencia(String nom_gerencia) {
		this.nom_gerencia = nom_gerencia;
	}
	public String getNom_oficina() {
		return nom_oficina;
	}
	public void setNom_oficina(String nom_oficina) {
		this.nom_oficina = nom_oficina;
	}
	public String getE_mail_oficina() {
		return e_mail_oficina;
	}
	public void setE_mail_oficina(String e_mail_oficina) {
		this.e_mail_oficina = e_mail_oficina;
	}
	public String getSiniestro() {
		return siniestro;
	}
	public void setSiniestro(String siniestro) {
		this.siniestro = siniestro;
	}
	public String getFecha_ejecucion() {
		return fecha_ejecucion;
	}
	public void setFecha_ejecucion(String fecha_ejecucion) {
		this.fecha_ejecucion = fecha_ejecucion;
	}
	public String getFecha_vencto_recibo() {
		return fecha_vencto_recibo;
	}
	public void setFecha_vencto_recibo(String fecha_vencto_recibo) {
		this.fecha_vencto_recibo = fecha_vencto_recibo;
	}
	public String getPrima_total() {
		return prima_total;
	}
	public void setPrima_total(String prima_total) {
		this.prima_total = prima_total;
	}
	
	
}
