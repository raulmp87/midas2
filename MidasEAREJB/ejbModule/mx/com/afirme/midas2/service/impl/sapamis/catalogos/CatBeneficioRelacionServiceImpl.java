package mx.com.afirme.midas2.service.impl.sapamis.catalogos;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas2.dto.sapamis.catalogos.CatBeneficioRelacion;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatBeneficioRelacionService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatCausaBeneficioService;
import mx.com.afirme.midas2.service.sapamis.catalogos.CatTipoBeneficioService;

@Stateless
public class CatBeneficioRelacionServiceImpl implements CatBeneficioRelacionService{
	private static final long serialVersionUID = 1L;
	
	private EntidadService entidadService;
	private CatTipoBeneficioService catTipoBeneficioService;
	private CatCausaBeneficioService catCausaBeneficioService;
	
	@Override
	public CatBeneficioRelacion completeObject(CatBeneficioRelacion catBeneficioRelacion) {
		CatBeneficioRelacion retorno = new CatBeneficioRelacion(); 
		if(catBeneficioRelacion != null){
			if(catBeneficioRelacion.getId() < 1){
				retorno = this.findIdByAttributes(catBeneficioRelacion);
			}else{
				if(!validateAttributes(catBeneficioRelacion)){
					retorno = this.findById(catBeneficioRelacion.getId());
				}
			}
		}else{
			retorno.setId(new Long(-1));
		}
		return retorno;
	}

	private CatBeneficioRelacion findById(long id) {
		return entidadService.findById(CatBeneficioRelacion.class, id);
	}

	@Override
	public List<CatBeneficioRelacion> findByStatus(boolean status) {
		return entidadService.findByProperty(CatBeneficioRelacion.class, "estatus", status?0:1);
	}
	
	private boolean validateAttributes(CatBeneficioRelacion catBeneficioRelacion){
		boolean retorno = catBeneficioRelacion.getCatTipoBeneficio() != null;
		if(retorno){
			retorno = catBeneficioRelacion.getCatCausaBeneficio() != null;
		}
		return retorno;
	}

	private CatBeneficioRelacion findIdByAttributes(CatBeneficioRelacion catBeneficioRelacion) {
		if(validateAttributes(catBeneficioRelacion)){
			catBeneficioRelacion.setCatTipoBeneficio(catTipoBeneficioService.completeObject(catBeneficioRelacion.getCatTipoBeneficio()));
			catBeneficioRelacion.setCatCausaBeneficio(catCausaBeneficioService.completeObject(catBeneficioRelacion.getCatCausaBeneficio()));
			if(catBeneficioRelacion.getCatCausaBeneficio().getId() > 0 &&
					catBeneficioRelacion.getCatTipoBeneficio().getId() > 0){
				Map<String,Object> parametros = new HashMap<String,Object>();
				parametros.put("catTipoBeneficio", catBeneficioRelacion.getCatTipoBeneficio());
				parametros.put("catCausaBeneficio", catBeneficioRelacion.getCatCausaBeneficio());
				List<CatBeneficioRelacion> catBeneficioRelacionList = entidadService.findByProperties(CatBeneficioRelacion.class, parametros);
				if(!catBeneficioRelacionList.isEmpty()){
					catBeneficioRelacion.setId(catBeneficioRelacionList.get(0).getId());
				}else{
					catBeneficioRelacion.setId(new Long(0));
				}				
			}else{
				catBeneficioRelacion.setId(new Long(-1));
			}
		}else{
			catBeneficioRelacion.setId(new Long(-1));
		}
		return catBeneficioRelacion;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}

	@EJB
	public void setCatTipoBeneficioService(CatTipoBeneficioService catTipoBeneficioService) {
		this.catTipoBeneficioService = catTipoBeneficioService;
	}

	@EJB
	public void setCatCausaBeneficioService(CatCausaBeneficioService catCausaBeneficioService) {
		this.catCausaBeneficioService = catCausaBeneficioService;
	}
}