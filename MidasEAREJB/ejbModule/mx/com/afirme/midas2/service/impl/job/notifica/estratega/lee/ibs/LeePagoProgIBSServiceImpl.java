package mx.com.afirme.midas2.service.impl.job.notifica.estratega.lee.ibs;

import java.io.InputStream;
import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.Resource;
import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.GrupoParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas2.dao.tarea.LeePagoProgIBSDao;
import mx.com.afirme.midas2.domain.bitacora.pago.programado.ibs.BitacoraPagoProgIBSTO;
import mx.com.afirme.midas2.service.job.lee.ibs.service.LeePagoProgIBSService;
import mx.com.afirme.midas2.service.parametros.ParametroGeneralService;
import mx.com.afirme.midas2.util.ConnectionSFTPService;
import mx.com.afirme.midas2.util.TextFileUtils;

/**
 * Clase que contiene la implementacion para el JOB de lee PAGOPROG.txt
 * que contiene la informacion de IBS
 * 
 * @author SEGUROS AFIRME
 * 
 * @since 20022017
 * 
 * @version 1.0
 *
 */
@Stateless
public class LeePagoProgIBSServiceImpl implements LeePagoProgIBSService {
	
	private static final long serialVersionUID = 1989301870920597653L;
	private static final Logger LOG = LoggerFactory.getLogger(LeePagoProgIBSServiceImpl.class);
	
	private ConnectionSFTPService conexionSFTP;
	private ParametroGeneralDTO parametroGeneral;
	
	@EJB
	private ParametroGeneralService parametroGeneralService;
	@EJB
	private LeePagoProgIBSDao pagoProgDAO;
	
	@Resource
	private javax.ejb.SessionContext sessionContext;
	
	@Override
	public void init() {
		LOG.info(">>>>>>>> Entra al metodo init()");
		LOG.info("::::::: [INF] ::::::: Inicializa el JOB [LEE_PAGOPROG_IBS]");
		this.parametroGeneral = parametroGeneralService.findByDescCode(GrupoParametroGeneralDTO.GRUPO_SERVIDOR_SFTP, ParametroGeneralDTO.PARAMETRO_SERVIDOR_SFTP_INFO);
	}
	
	@Override
	@Schedule(hour = "22")
	public void jobLeePagoProgIBS() {
		LeePagoProgIBSService processor = this.sessionContext.getBusinessObject(LeePagoProgIBSService.class);
		processor.cargaInformacionIBS();
		processor.complementarInformacionIBS();
		processor.migrarInformacionIBS();
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void cargaInformacionIBS(){
		if(parametroGeneral == null)
			init();
		List<String> elementosArchivo = new ArrayList<String>();
		InputStream file = null;
		TextFileUtils util = new TextFileUtils();
		try{
			
			conexionSFTP = new ConnectionSFTPService(parametroGeneral);
			file = conexionSFTP.getFile(TextFileUtils.FILE_NAME_IBS, TextFileUtils.PATH_FILE_IBS);
			if(file != null){
				elementosArchivo = util.getDataFile(file);
			}
			
			pagoProgDAO.saveBitacoraPagoProgIBS(transformIBSData(elementosArchivo));
			
		} catch (Exception e){
			LOG.info("::::::: [ERR] ::::: Fall\u00f3 la ejecucion del job [LEE_PAGOPROG_IBS]", e);
			throw new RuntimeException(e);
		} finally {
			conexionSFTP.closeServerSession();
		}
	}
	
	/**
	 * Metodo que realiza la transformacion de una lista de cadenas a una lista del
	 * objeto <code>BitacoraPagoProgIBSTO</code>
	 * 
	 * @param informacion Lista que contiene la informacion extraida del archivo 
	 * consumido a travez del servidor SFTP
	 * 
	 * @return Lista de objetos <code>BitacoraPagoProgIBSTO</code>
	 */
	private List<BitacoraPagoProgIBSTO> transformIBSData(List<String> informacion){
		List<BitacoraPagoProgIBSTO> informacionIBS = null;
		DateFormat dateFormat = new SimpleDateFormat("yyyy-mm-dd");
		try{
			
			if(informacion.isEmpty()){
				throw new RuntimeException("La lista de cadenas que intenta transformar es vacia o nula");
			} else {
				informacionIBS = new ArrayList<BitacoraPagoProgIBSTO>();
			}
			
			for(String datosIBS: informacion){
				String[] camposIBS = datosIBS.split("\\|");
				BitacoraPagoProgIBSTO ibsTO = new BitacoraPagoProgIBSTO();
				ibsTO.setBanco(camposIBS[0]);
				ibsTO.setSucursal(camposIBS[1]);
				ibsTO.setMoneda(camposIBS[2]);
				ibsTO.setPoliza(camposIBS[3]);
				ibsTO.setRecibo(new BigDecimal(camposIBS[4]));
				ibsTO.setCuentaCargo(new BigDecimal(camposIBS[5]));
				ibsTO.setNumeroCuota(new BigDecimal(camposIBS[7]));
				ibsTO.setImporteCuota(new Double(camposIBS[8]));
				ibsTO.setFechaCuota(dateFormat.parse(camposIBS[9]));
				ibsTO.setImportePago(new Double(camposIBS[10]));	
				ibsTO.setUsuario(camposIBS[11]);
				ibsTO.setFechaPago(dateFormat.parse(camposIBS[12]));
				ibsTO.setFechaOperacion(dateFormat.parse(camposIBS[13]));
				ibsTO.setNumeroCancelacion(new BigDecimal(camposIBS[14]));
				ibsTO.setFechaRegistro(dateFormat.parse(camposIBS[15]));
				ibsTO.setUsuarioCancelacion(camposIBS[16]);
				ibsTO.setMotivo(camposIBS[17]);
				ibsTO.setIntentos(new BigDecimal(camposIBS[18]));
				ibsTO.setError(camposIBS[19]);
				ibsTO.setFechaReciboH(dateFormat.parse(camposIBS[20]));
				ibsTO.setOriginal(camposIBS[21]);
				ibsTO.setEstatus(camposIBS[22]);
				ibsTO.setFechaRecibo(camposIBS[23]);
				ibsTO.setIdSucursal(new BigDecimal(camposIBS[24]));
				ibsTO.setEstatusCuenta(camposIBS[25]);
				informacionIBS.add(ibsTO);
			}
			
		} catch (Exception e){
			throw new RuntimeException(e);
		}
		
		return informacionIBS;
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public  void complementarInformacionIBS(){
		LOG.info("::::::: [INF] >>>>> ENTRA A COMPLEMENTAR LA INFORMACION DE IBS");
		pagoProgDAO.complementarBitacoraPagoProgIBS();
		LOG.info("::::::: [INF] <<<<< SALE DE COMPLEMENTAR LA INFORMACION DE IBS");
	}
	
	@Override
	@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
	public void migrarInformacionIBS(){
		LOG.info("::::::: [INF] >>>>> ENTRA A MIGRAR LA INFORMACION DE IBS");
		pagoProgDAO.migrarInformacionPagoProgIBS();
		LOG.info("::::::: [INF] <<<<< SALE DE MIGRAR LA INFORMACION DE IBS");
	}
	
	public ParametroGeneralService getParametroGeneralService() {
		return parametroGeneralService;
	}
	
	public void setParametroGeneralService(
			ParametroGeneralService parametroGeneralService) {
		this.parametroGeneralService = parametroGeneralService;
	}
	
	public void setSessionContext(javax.ejb.SessionContext sessionContext) {
		this.sessionContext = sessionContext;
	}

	public void setPagoProgDAO(LeePagoProgIBSDao pagoProgDAO) {
		this.pagoProgDAO = pagoProgDAO;
	}
}