package mx.com.afirme.midas2.service.impl.negocio.producto.tipopoliza;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaDTO;
import mx.com.afirme.midas.producto.configuracion.tipopoliza.TipoPolizaFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.negocio.producto.tipopoliza.NegocioTipoPolizaDao;
import mx.com.afirme.midas2.domain.negocio.producto.NegocioProducto;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.NegocioTipoPoliza;
import mx.com.afirme.midas2.dto.negocio.paquete.tipopoliza.RelacionesNegocioTipoPolizaDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.negocio.producto.tipopoliza.NegocioTipoPolizaService;

@Stateless
public class NegocioTipoPolizaServiceImpl implements NegocioTipoPolizaService {

	@Override
	public List<TipoPolizaDTO> getListarNegocioTipopoliza() {
		return tipoPolizaFacadeRemote.listarVigentes();
	}

	@Override
	public RelacionesNegocioTipoPolizaDTO getRelationLists(
			NegocioProducto negocioProducto) {

		List<NegocioTipoPoliza> negocioTipoPolizaAsociadas = new ArrayList<NegocioTipoPoliza>();
		negocioTipoPolizaAsociadas = entidadService.findByProperty(
				NegocioTipoPoliza.class, "negocioProducto.idToNegProducto", negocioProducto.getIdToNegProducto());
		
		List<TipoPolizaDTO> posibles = new ArrayList<TipoPolizaDTO>();
		posibles = tipoPolizaFacadeRemote.listarPorIdProducto(negocioProducto.getProductoDTO().getIdToProducto(), false, true);


		List<NegocioTipoPoliza> negocioTipoPolizaDisponibles = new ArrayList<NegocioTipoPoliza>();

		RelacionesNegocioTipoPolizaDTO relacionesNegocioTipoPolizaDTO = new RelacionesNegocioTipoPolizaDTO();

		List<TipoPolizaDTO> tipopolizaAsociadas = new ArrayList<TipoPolizaDTO>();

		for (NegocioTipoPoliza item : negocioTipoPolizaAsociadas) {
			tipopolizaAsociadas.add(item.getTipoPolizaDTO());
		}

		for (TipoPolizaDTO item : posibles) {
			if (!tipopolizaAsociadas.contains(item)) {
				NegocioTipoPoliza negocioTipoPoliza = new NegocioTipoPoliza();
				negocioTipoPoliza.setTipoPolizaDTO(item);
				negocioTipoPoliza.setNegocioProducto(negocioProducto);
//				negocioTipoPoliza
//						.setIdToNegTipoPoliza(item.getIdToTipoPoliza());
				negocioTipoPolizaDisponibles.add(negocioTipoPoliza);
			}
		}
		// Llenados del Objeto
		relacionesNegocioTipoPolizaDTO.setAsociadas(negocioTipoPolizaAsociadas);
		relacionesNegocioTipoPolizaDTO
				.setDisponibles(negocioTipoPolizaDisponibles);
		return relacionesNegocioTipoPolizaDTO;
	}

	@Override
	public void relacionarNegocioTipoPoliza(String accion,
			NegocioTipoPoliza negocioTipoPoliza) {
		if(negocioTipoPoliza.getIdToNegTipoPoliza() != null)
			negocioTipoPoliza = entidadService.getReference(NegocioTipoPoliza.class, negocioTipoPoliza.getIdToNegTipoPoliza());
		catalogoDao.executeActionGrid(accion, negocioTipoPoliza);
	}
	
	
	@Override
	public NegocioTipoPoliza getPorIdNegocioProductoIdToTipoPoliza(Long idToNegProducto, BigDecimal idToTipoPoliza) {
		NegocioTipoPoliza negocioTipoPoliza = null;
		negocioTipoPoliza = negocioTipoPolizaDao.getPorIdNegocioProductoIdToTipoPoliza(idToNegProducto, idToTipoPoliza);
		return negocioTipoPoliza;

	}
	
	@Override
	public List<NegocioTipoPoliza> listarNegocioTipoPolizaPorProducto(BigDecimal idToProducto) {
		return catalogoDao.findByProperty(NegocioTipoPoliza.class, "negocioProducto.productoDTO.idToProducto", idToProducto);
	}

	public NegocioTipoPoliza getPorIdNegocioProductoIdToNegTipoPoliza(Long idToNegProducto, BigDecimal idToNegTipoPoliza) {
		return negocioTipoPolizaDao.getPorIdNegocioProductoIdToNegTipoPoliza(idToNegProducto, idToNegTipoPoliza);
	}
	
	private TipoPolizaFacadeRemote tipoPolizaFacadeRemote;
	private EntidadDao catalogoDao;
	protected NegocioTipoPolizaDao negocioTipoPolizaDao;
	private EntidadService entidadService;

	@EJB
	public void setTipoPolizaFacadeRemote(
			TipoPolizaFacadeRemote tipoPolizaFacadeRemote) {
		this.tipoPolizaFacadeRemote = tipoPolizaFacadeRemote;
	}

	@EJB
	public void setEntidadDao(EntidadDao catalogoDao) {
		this.catalogoDao = catalogoDao;
	}
	
	@EJB
	public void setNegocioTipoPolizaDao(NegocioTipoPolizaDao negocioTipoPolizaDao) {
		this.negocioTipoPolizaDao = negocioTipoPolizaDao;
	}

	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}	
}
