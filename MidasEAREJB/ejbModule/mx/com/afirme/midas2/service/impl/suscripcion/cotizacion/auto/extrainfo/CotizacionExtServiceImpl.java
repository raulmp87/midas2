package mx.com.afirme.midas2.service.impl.suscripcion.cotizacion.auto.extrainfo;


import java.math.BigDecimal;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.ArrayList;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas.cotizacion.extrainfo.CotizacionExtDTO;
import mx.com.afirme.midas2.dto.excepcion.ExcepcionSuscripcionReporteDTO;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.extrainfo.CotizacionExtService;
import mx.com.afirme.midas2.service.excepcion.ExcepcionSuscripcionNegocioAutosService;
import mx.com.afirme.midas2.service.impl.excepcion.ExcepcionSuscripcionNegocioAutosServiceImpl;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;

@Stateless
public class CotizacionExtServiceImpl implements CotizacionExtService{
	
	public  static final Logger LOG = Logger.getLogger(CotizacionExtServiceImpl.class);
	
	/**
	 * Crea o actualiza registro en BD 
	 * @param cotizacion
	 */
	@Override
	public void crearCotizacionExt(CotizacionExtDTO cotizacion) {
		if(cotizacion != null){
			try{
				entidadService.save(cotizacion);
			}catch (Exception e) {
				LOG.error(this.getClass().getClass()+ ".crearCotizacionExt.Excepcion: " + e, e);
			}
		}
	}
	/**
	 * Crea o actualiza registro en BD 
	 * @param cotizacion
	 */
	@Override
	public void actualizaCotizacionExt(CotizacionExtDTO cotizacion) {
		if(cotizacion != null){
			try{
				entidadService.save(cotizacion);
			}catch (Exception e) {
				LOG.error(this.getClass().getClass()+ ".crearCotizacionExt.Excepcion: " + e, e);
			}
		}
	}
	/**
	 * Recupera el registro de la BD
	 * @param idToCotizacion
	 * @return
	 */
	@Override
	public CotizacionExtDTO getCotizacionExt(BigDecimal idToCotizacion){
		CotizacionExtDTO cotizacion = entidadService.getReference(CotizacionExtDTO.class, idToCotizacion);
		return cotizacion;
	}
	/**
	 * @param idToCotizacion
	 * @return
	 */
	@Override
	public CotizacionDTO getCotizacion(BigDecimal idToCotizacion){
		CotizacionDTO cotizacionDTO = entidadService.findById(CotizacionDTO.class, idToCotizacion);
		return cotizacionDTO;
	}
	/**
	 * recupera las Excepciones que una cotizacion genero
	 * @param cotizacion
	 * @return
	 */
	@Override
	public List<? extends ExcepcionSuscripcionReporteDTO> evaluarCotizacion(CotizacionDTO cotizacion){
		if(excepcionSuscripcionNegocioAutosService == null) {
			excepcionSuscripcionNegocioAutosService = new ExcepcionSuscripcionNegocioAutosServiceImpl();
		}
		return excepcionSuscripcionNegocioAutosService.evaluarCotizacion(cotizacion, null);
	}
	/**
	 * @author JFGG
	 * Ejecuta la validacion de Exceciones para la cotizacvion
	 * cotizacionDto 
	 * 
	 */
	@Override
	public Map<String, Object> evalueExcepcion (CotizacionDTO cotizacionDto) {
		return evalueExcepcion(cotizacionDto, false);
		
	}
	/**
	 * @author JFGG
	 * Ejecuta la validacion de Exceciones para la cotizacvion
	 * cotizacionDto 
	 * Mensaje
	 */
	@Override
	public  Map<String, Object> evalueExcepcion (CotizacionDTO cotizacionDto, boolean armaMensaje){
		Map<String, Object> map = new HashMap<String, Object>();
		boolean res = false;
		Integer statusExpetion = new Integer(0);
		try{
			if(cotizacionDto.getIgualacionNivelCotizacion() == null) {
				cotizacionDto.setIgualacionNivelCotizacion(false);
			}
			if(cotizacionDto.getValorPrimaTotal() == null) {
				cotizacionDto.setValorPrimaTotal(new BigDecimal(0));
			}
			
			List<? extends ExcepcionSuscripcionReporteDTO> listaExcepciones = evaluarCotizacion(cotizacionDto);
			
			//ExcepcionSuscripcion
			if(listaExcepciones != null && !listaExcepciones.isEmpty ()) {
				statusExpetion = new Integer(1);
				res = true;
				long idExcepciones = 0;
				List<ExcepcionSuscripcionReporteDTO> listaExc = new ArrayList<ExcepcionSuscripcionReporteDTO> ();
				for (ExcepcionSuscripcionReporteDTO item : listaExcepciones) {
					listaExc.add(item);
					if ( idExcepciones == 0 ) { idExcepciones = item.getIdExcepcion (); }
				}
				map.put("mensaje", MensajeExcepcion);
				map.put("idExcepcion", String.valueOf(idExcepciones));
				map.put("listaExcepciones", listaExc);
			}
		} catch(Exception e) {
			statusExpetion = 0;
			LOG.error(this.getClass().getName() + ".EvalueExcepcion.Excepcion: " + e, e);
		}
		map.put("statusExpetion", String.valueOf(res));
		try {
			CotizacionExtDTO cotizacionExtDto = new CotizacionExtDTO();
			cotizacionExtDto.setIdToCotizacion(cotizacionDto.getIdToCotizacion());
			cotizacionExtDto.setStatusException(statusExpetion);
			crearCotizacionExt(cotizacionExtDto);
		} catch(Exception e) {
			statusExpetion = 0;
			LOG.error(this.getClass().getName() + ".EvalueExcepcion.Excepcion: " + e, e);
		}
		return map;
	}
	public EntidadService entidadService;
	public ExcepcionSuscripcionNegocioAutosService excepcionSuscripcionNegocioAutosService;
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	@EJB
	public void setExcepcionSuscripcionNegocioAutosService(ExcepcionSuscripcionNegocioAutosService excepcionSuscripcionNegocioAutosService) {
		this.excepcionSuscripcionNegocioAutosService = excepcionSuscripcionNegocioAutosService;
	}
}
