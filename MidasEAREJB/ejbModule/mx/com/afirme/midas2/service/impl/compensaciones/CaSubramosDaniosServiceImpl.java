/**
 * 
 * Powered by AddMotions. 
 * Monterrey, México
 * Noviembre 2015
 * @author nestorreynoso 
 * 
 */
package mx.com.afirme.midas2.service.impl.compensaciones;

import java.math.BigDecimal;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import mx.com.afirme.midas.catalogos.ramo.SubRamoDTO;
import mx.com.afirme.midas.cotizacion.CotizacionDTO;
import mx.com.afirme.midas2.dao.compensaciones.CaSubramosDaniosDao;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaCompensacionDaoImpl;
import mx.com.afirme.midas2.dao.impl.compensaciones.CaSubramosDaniosDaoImpl;
import mx.com.afirme.midas2.domain.compensaciones.CaParametros;
import mx.com.afirme.midas2.domain.compensaciones.CaSubramosDanios;
import mx.com.afirme.midas2.dto.compensaciones.CompensacionesDTO;
import mx.com.afirme.midas2.dto.compensaciones.SubRamosDaniosView;
import mx.com.afirme.midas2.dto.compensaciones.SubRamosDaniosViewDTO;
import mx.com.afirme.midas2.service.compensaciones.CaSubramosDaniosService;
import mx.com.afirme.midas2.util.ConstantesCompensacionesAdicionales;

import org.apache.log4j.Logger;

@Stateless
public class CaSubramosDaniosServiceImpl  implements CaSubramosDaniosService {
	public static final String PORCENTAJECOMPENSACION = "porcentajeCompensacion";
	public static final String USUARIO = "usuario";
	public static final String BORRADOLOGICO = "borradoLogico";
	
	@EJB
	private CaSubramosDaniosDao subramosDanioscaDao;
	
	@PersistenceContext
	private EntityManager entityManager;
	
    private static final Logger LOGGER = Logger.getLogger(CaSubramosDaniosServiceImpl.class);
	
		/**
	 Perform an initial save of a previously unsaved CaSubramosDanios entity. 
	 All subsequent persist actions of this entity should use the #update() method.
	  @param entity CaSubramosDanios entity to persist
	  @throws RuntimeException when the operation fails
	 */
    public void save(CaSubramosDanios entity) {
    	LOGGER.info("	::	[INF]	::	Guardando CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	save	::	INICIO	::	");
	        try {
	        	subramosDanioscaDao.save(entity);
            LOGGER.info("	::	[INF]	::	Se Guardo CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	save	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al guardar CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	save	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Delete a persistent CaSubramosDanios entity.
	  @param entity CaSubramosDanios entity to delete
	 @throws RuntimeException when the operation fails
	 */
    public void delete(CaSubramosDanios entity) {
    	LOGGER.info("	::	[INF]	::	Eliminando CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	delete	::	INICIO	::	");
	        try {
	        subramosDanioscaDao.delete(entity);
            LOGGER.info("	::	[INF]	::	Se Elimino CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	delete	::	FIN	::	");
	        } catch (RuntimeException re) {
	        	LOGGER.error("	::	[ERR]	::	Error al eliminar CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	delete	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    /**
	 Persist a previously saved CaSubramosDanios entity and return it or a copy of it to the sender. 
	 A copy of the CaSubramosDanios entity parameter is returned when the JPA persistence mechanism has not previously been tracking the updated entity. 
	  @param entity CaSubramosDanios entity to update
	 @return CaSubramosDanios the persisted CaSubramosDanios entity instance, may not be the same
	 @throws RuntimeException if the operation fails
	 */
    public CaSubramosDanios update(CaSubramosDanios entity) {
    	LOGGER.info("	::	[INF]	::	Actualizando CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	update	::	INICIO	::	");
	        try {
            CaSubramosDanios result = subramosDanioscaDao.update(entity);//entityManager.merge(entity);
            LOGGER.info("	::	[INF]	::	Se Actualizo CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	update	::	FIN	::	");
            return result;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al actualizar CaSubramosDanios 	::		CaSubramosDaniosServiceImpl	::	update	::	ERROR	::	",re);
	            throw re;
        }
    }
    
    public CaSubramosDanios findById( Long id) {
    	LOGGER.info("	::	[INF]	::	Buscando por Id 	::		CaSubramosDaniosServiceImpl	::	findById	::	INICIO	::	");
	        try {
            CaSubramosDanios instance = subramosDanioscaDao.findById(id);//entityManager.find(CaSubramosDanios.class, id);
            LOGGER.info("	::	[INF]	::	Se busco por Id	::		CaSubramosDaniosServiceImpl	::	findById	::	FIN	::	");
            return instance;
        } catch (RuntimeException re) {
        	LOGGER.error("	::	[ERR]	::	Error al buscar por Id 	::		CaSubramosDaniosServiceImpl	::	findById	::	ERROR	::	",re);
        	return null;
        }
    }    
    

/**
	 * Find all CaSubramosDanios entities with a specific property value.  
	 
	  @param propertyName the name of the CaSubramosDanios property to query
	  @param value the property value to match
	  	  @return List<CaSubramosDanios> found by query
	 */
    public List<CaSubramosDanios> findByProperty(String propertyName, final Object value
        ) {
    	LOGGER.info("	::	[INF]	::	Buscando por Propiedad 	::		CaSubramosDaniosServiceImpl	::	findByProperty	::	INICIO	::	");
			try {
			LOGGER.info("	::	[INF]	::	Se busco por Propiedad 	::		CaSubramosDaniosServiceImpl	::	findByProperty	::	FIN	::	");
			return subramosDanioscaDao.findByProperty(propertyName, value);//query.getResultList();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar por Propiedad 	::		CaSubramosDaniosServiceImpl	::	findByProperty	::	ERROR	::	",re);
			return null;
		}
	}			
	public List<CaSubramosDanios> findByPorcentajecompensacion(Object porcentajecompensacion
	) {
		return findByProperty(PORCENTAJECOMPENSACION, porcentajecompensacion
		);
	}
	
	public List<CaSubramosDanios> findByUsuario(Object usuario
	) {
		return findByProperty(USUARIO, usuario
		);
	}
	
	public List<CaSubramosDanios> findByBorradologico(Object borradologico
	) {
		return findByProperty(BORRADOLOGICO, borradologico
		);
	}
	
	
	/**
	 * Find all CaSubramosDanios entities.
	  	  @return List<CaSubramosDanios> all CaSubramosDanios entities
	 */
	public List<CaSubramosDanios> findAll(
		) {
		LOGGER.info("	::	[INF]	::	Buscando Todo 	::		CaSubramosDaniosServiceImpl	::	findAll	::	INICIO	::	");
			try {
			LOGGER.info("	::	[INF]	::	Se busco todo 	::		CaSubramosDaniosServiceImpl	::	findAll	::	FIN	::	");
			return subramosDanioscaDao.findAll();
		} catch (RuntimeException re) {
			LOGGER.error("	::	[ERR]	::	Error al buscar todo 	::		CaSubramosDaniosServiceImpl	::	findAll	::	ERROR	::	",re);
			return null;
		}
	}
	
	public void  findSubRamos(SubRamosDaniosViewDTO subDaniosViewDTO,CompensacionesDTO compensacionesDTO){
		subramosDanioscaDao.getViewSubRamos(subDaniosViewDTO,compensacionesDTO);
		
	}
	
	public void guardarListadoSubRamosDanios(List<SubRamosDaniosView> listSubRamosDanios, Long cotizacionId, Long cotizacionEndosoId, CaParametros caParametros){
		LOGGER.info(">> guardarListadoSubRamosDanios()");
		CaSubramosDanios caSubramosDanios = null;
		
		//Cotizacion Principal
		CotizacionDTO cotizacionDTO = new CotizacionDTO();
		cotizacionDTO.setIdToCotizacion(new BigDecimal(cotizacionId));
		//Cotizacion Endoso
		CotizacionDTO cotizacionEndosoDTO = null;
		LOGGER.info("cotizacionEndosoId EJB = "+ cotizacionEndosoId);
		/*if(cotizacionEndosoId != null && cotizacionEndosoId > 0){
			cotizacionEndosoDTO = new CotizacionDTO();
			cotizacionDTO.setIdToCotizacion(new BigDecimal(cotizacionEndosoId));
		}*/
		
		SubRamoDTO subRamoDTO = null;
		
		try{
			//Se eliminan los registros anteriores
			if(caParametros.getCaCompensacion().getId() > 0){
				this.subramosDanioscaDao.deleteRecordsSubRamosDanios(cotizacionId, caParametros.getCaEntidadPersona().getId(), caParametros.getCaCompensacion().getId());
			}
			//Se agregan los nuevos registros
			for(SubRamosDaniosView subRamosDaniosView: listSubRamosDanios){
				
				if(!"REMOVE".equalsIgnoreCase(subRamosDaniosView.getExiste())){
					caSubramosDanios = new CaSubramosDanios();
					
					caSubramosDanios.setBorradoLogico(ConstantesCompensacionesAdicionales.BORRADOLOGICONO);
					caSubramosDanios.setCaParametros(caParametros);
					caSubramosDanios.setCotizacionDTO(cotizacionDTO);
				//	caSubramosDanios.setCotizacionEndosoDTO(cotizacionEndosoDTO);
					caSubramosDanios.setUsuario(caParametros.getUsuario());	
					caSubramosDanios.setPorcentajeCompensacion(subRamosDaniosView.getPorcentajeCompensacion());
					
					subRamoDTO = new SubRamoDTO();
					subRamoDTO.setIdTcSubRamo(subRamosDaniosView.getIdTcSubRamo());			
					caSubramosDanios.setSubRamoDTO(subRamoDTO);
				
					caSubramosDanios.setFechaModificacion(caParametros.getFechaModificacion());
					caSubramosDanios.setFechaCreacion(caParametros.getFechaModificacion());
					this.save(caSubramosDanios);
				}
									
			}
			LOGGER.info("<< guardarListadoSubRamosDanios()");
		}catch (RuntimeException re) {
			LOGGER.error("Información del Error", re);
			throw re;
		}
		
	}
	
	public void  calcularProvisionDanios(SubRamosDaniosViewDTO subDaniosViewDTO,CompensacionesDTO compensacionesDTO){
	subramosDanioscaDao.calcularProvisionDanios(subDaniosViewDTO, compensacionesDTO);
	}
	
	public Long findCotizacionForIdCotizacionEndoso(Long idCotizacionEndoso){
		LOGGER.info(">> findCotizacionForIdCotizacionEndoso()");
		Long cotizacionId = null;
		
		try{
			
			StringBuilder queryString = new StringBuilder(" SELECT ");
			queryString.append(" pol.idtocotizacion AS ").append(CaCompensacionDaoImpl.COTIZACION_ID);
			queryString.append(" FROM MIDAS.tocotizacion cotend ");
			queryString.append(" INNER JOIN MIDAS.tosolicitud sol ON(sol.idtosolicitud = cotend.idtosolicitud) ");
			queryString.append(" INNER JOIN MIDAS.topoliza pol ON(pol.idtopoliza = sol.idtopolizaendosada) ");
			queryString.append(" WHERE cotend.IDTOCOTIZACION = ?1 ");
			
			Query query = entityManager.createNativeQuery(queryString.toString());
			query.setParameter(1, idCotizacionEndoso);
			
			cotizacionId = ((BigDecimal) query.getSingleResult()).longValue();
			
		}catch(Exception ex){
			if(ex instanceof NoResultException){
				LOGGER.info(" -- No se encontraron resultados ");
			}else{
				LOGGER.error("Información del Error", ex);
			}
		}
		LOGGER.info("<< findCotizacionForIdCotizacionEndoso()");
		return cotizacionId;
	}
		
	public boolean existeRegistrosEndoso(Long idCotizacionEndoso){
		LOGGER.info(">> existeRegistrosEndoso()");
		boolean result = false;		
		try{
			if(idCotizacionEndoso != null & idCotizacionEndoso > 0){
				String queryString = " SELECT COUNT(ID) FROM MIDAS.CA_SUBRAMOSDANIOS WHERE cotizacionendoso_id = ?1 ";			
				Query query = entityManager.createNativeQuery(queryString);
				query.setParameter(1, idCotizacionEndoso);					
				result = ((BigDecimal) query.getSingleResult()).intValue() > 0;			
			}
		}catch(Exception ex){
			LOGGER.error("Información del Error", ex);
			result = false;
		}
		LOGGER.info("<< existeRegistrosEndoso()");
		return result;
	}
	
	public int deleteRecordsByCotizacionId(Long cotizacionId){
		LOGGER.info(">> deleteRecordsByProperty()");
		int deleteCount = 0;
		try {
			StringBuilder queryString = new StringBuilder("DELETE FROM CaSubramosDanios model");
			queryString.append(" WHERE model.").append(CaSubramosDaniosDaoImpl.COTIZACION_ID).append(" = ").append(" :propertyValue ");
			
			Query query = entityManager.createQuery(queryString.toString());
			query.setParameter("propertyValue", cotizacionId);
			
			deleteCount = query.executeUpdate();
			
		} catch (RuntimeException re) {
			LOGGER.error("Información del Error", re);
			throw re;
		}
		LOGGER.info("<< deleteRecordsByProperty()");
		return deleteCount;
	}
	
}