package mx.com.afirme.midas2.service.impl.tarifa;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaQuery;

import mx.com.afirme.midas2.service.tarifa.TarifaService;

public abstract class JpaTarifaService<K,E> implements TarifaService<K, E>{
	@PersistenceContext
	protected EntityManager entityManager;
	
	public JpaTarifaService() {
	}

	@SuppressWarnings("hiding")
	public <E> List<E> findAll(Class<E> entity) {
		CriteriaQuery<E> criteriaQuery = entityManager.getCriteriaBuilder().createQuery(entity);
		criteriaQuery.from(entity);
		TypedQuery<E> query = entityManager.createQuery(criteriaQuery);
		query.setHint("org.hibernate.cacheable", "true");
		return query.getResultList();
	}
	
	@SuppressWarnings("hiding")
	public <E,K> E findById(Class<E> entity, K id) {
		return entityManager.find(entity,id);
	}

	@SuppressWarnings("hiding")
	public <E, K> E getReference(Class<E> entity, K id) {
		return entityManager.getReference(entity, id);
	}

	public void remove(E entity) {
		entityManager.remove(entity);
	}

	public void save(E entity) {
		entityManager.persist(entity);
	}

	public E update(E entity) {
		return entityManager.merge(entity);
	}
}
