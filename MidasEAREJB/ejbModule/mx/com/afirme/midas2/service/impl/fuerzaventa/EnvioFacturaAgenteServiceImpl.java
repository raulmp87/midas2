package mx.com.afirme.midas2.service.impl.fuerzaventa;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.math.BigDecimal;
import java.net.ConnectException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import javax.ejb.EJB;
import javax.ejb.Schedule;
import javax.ejb.Stateless;
import javax.ejb.Timer;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.PersistenceException;

import org.apache.log4j.Logger;

import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralDTO;
import mx.com.afirme.midas.cotizacion.inspeccion.parametros.ParametroGeneralFacadeRemote;
import mx.com.afirme.midas.sistema.SistemaPersistencia;
import mx.com.afirme.midas.sistema.Utilerias;
import mx.com.afirme.midas.sistema.XMLNodeReader;
import mx.com.afirme.midas.sistema.mail.ByteArrayAttachment;
import mx.com.afirme.midas.sistema.seguridad.Usuario;
import mx.com.afirme.midas2.dao.catalogos.EntidadDao;
import mx.com.afirme.midas2.dao.fuerzaventa.EnvioFacturaAgenteDao;
import mx.com.afirme.midas2.domain.agtSaldoMovto.AgtSaldo;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.Agente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgenteDet;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.EnvioFacturaAgenteEst;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.GrupoCatalogoAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.HonorariosAgente;
import mx.com.afirme.midas2.domain.catalogos.fuerzaventa.ValorCatalogoAgentes;
import mx.com.afirme.midas2.domain.envioxml.EnvioFactura;
import mx.com.afirme.midas2.domain.siniestros.catalogo.prestadorservicio.PrestadorServicio;
import mx.com.afirme.midas2.domain.suscripcion.endoso.movimiento.ArrayListNullAware;
import mx.com.afirme.midas2.dto.fuerzaventa.AgenteFacturaPendiente;
import mx.com.afirme.midas2.dto.impresiones.TransporteImpresionDTO;
import mx.com.afirme.midas2.service.agtSaldoMovto.AgtSaldoService;
import mx.com.afirme.midas2.service.envioxml.EnvioFacturaService;
import mx.com.afirme.midas2.service.fuerzaventa.AgenteMidasService;
import mx.com.afirme.midas2.service.fuerzaventa.EnvioFacturaAgenteService;
import mx.com.afirme.midas2.service.reportes.GenerarPlantillaReporte;
import mx.com.afirme.midas2.service.seguridad.UsuarioService;
import mx.com.afirme.midas2.service.sistema.SistemaContext;
import mx.com.afirme.midas2.util.MailService;
import mx.com.afirme.midas2.ws.axosnet.AxnResponse;
import mx.com.afirme.midas2.ws.axosnet.ValResponse;
import mx.com.afirme.midas2.ws.axosnet.WSValidacionesSATPortProxy;


@Stateless
public class EnvioFacturaAgenteServiceImpl implements EnvioFacturaAgenteService {

	private EnvioFacturaAgenteDao envioFacturaDao;
	private MailService mailService;
	private AgenteMidasService agenteMidasService;
	private ParametroGeneralFacadeRemote parametroGeneral;
	private EnvioFacturaService envioFacturaService;
	private UsuarioService usuarioService;
	private EntidadDao entidadDao;
	private SistemaContext sistemaContext;
	private GenerarPlantillaReporte generarPlantillaReporte;
	private AgtSaldoService agtSaldoService;

	public static final String PARAMETROS_ENVIO_FACTURA_AGENTE = "Parametros Envio Factura Agente"; 
	public static final String ESTATUS_ENVIO_FACTURA_AGENTE = "Estatus Envio Factura Agente"; 
	private static final String TIPO_MSJ_ENVIO_FACTURA_AGENTE = "Tipo Mensaje Envio Factura Agente";
	public static final String CLAVE_STATUS_ACEPTADO = "ACPT";
	public static final String CLAVE_STATUS_RECHAZADO = "RECH";
	private static final String CLAVE_VALIDACION_FISCAL = "VALFIS";
	private static final String CLAVE_VALIDACION_COMERCIAL = "VALCOM";
	private static final String XML_COMPLEMENT_NAME_IMPUESTOS_LOCALES = "implocal:ImpuestosLocales";	
	private static final String XML_ELEMENT_NAME_RETENCIONES_LOCALES = "implocal:RetencionesLocales";
	private static final String XML_ATTRIBUTE_NAME_IMPORTE = "Importe";
	private static Logger log = Logger.getLogger("EnvioFacturaServiceImpl");
	

	@Override
	public List<EnvioFacturaAgente> findByAgente(Long idAgente){
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("idAgente", idAgente);
		List<EnvioFacturaAgente> listaEnvios = envioFacturaDao.findByPropertiesWithOrder(EnvioFacturaAgente.class, params,	"idEnvio DESC");
		return listaEnvios;
	}
	
	@Override
	public List<EnvioFacturaAgenteDet> findByIdEnvio(Long idEnvio){
		List<EnvioFacturaAgenteDet> listaEnviosDet = envioFacturaDao.findByProperty(EnvioFacturaAgenteDet.class, "envioFactura.idEnvio", idEnvio);
		return listaEnviosDet;
	}
	
	@Override
	public EnvioFacturaAgente persistEnvio(Long idAgente, File archivo, String userName) throws FileNotFoundException, PersistenceException, Exception {
		final int tipoRespuestaEnRepositorio = 20;
		final int tipoRespuestaErrores = 500;
		List<ParametroGeneralDTO> parametros = new ArrayList<ParametroGeneralDTO>();
		List<AxnResponse> mensajes = new ArrayList<AxnResponse>();
		List<ValorCatalogoAgentes> listValor = new ArrayList<ValorCatalogoAgentes>();
		List<EnvioFacturaAgenteDet> mensajesEnvio = new ArrayListNullAware<EnvioFacturaAgenteDet>();
		GrupoCatalogoAgente gpo = new GrupoCatalogoAgente();
		EnvioFactura envioFactura = new EnvioFactura();
		EnvioFacturaAgente envio = null;
		EnvioFacturaAgenteDet mensajeEnvio = null;
		FileInputStream fis = null;
		WSValidacionesSATPortProxy proxySAT = null;
		ValResponse response = null;
		Agente agente = new Agente();
		HonorariosAgente honorarios = new  HonorariosAgente();
	    StringBuilder  stringBuilder = new StringBuilder();
	    ValorCatalogoAgentes statusAceptado = new ValorCatalogoAgentes();
	    ValorCatalogoAgentes statusRechazado = new ValorCatalogoAgentes();
	    ValorCatalogoAgentes tipoFiscal = new ValorCatalogoAgentes();
	    ValorCatalogoAgentes tipoComercial = new ValorCatalogoAgentes();
	    BigDecimal subtotal = BigDecimal.ZERO;
	    String facturaString = new String();
	    String line = null;	   
	    String tolerancia = null;
		String empresa = null;
		String departamento = null;
		String uuid = null;
		boolean fisicamenteEnRepositorio = false;
		int numeroErrores = 0;
		String cuentaAfectada = "";
		boolean excepcionImportes = false;
		boolean isRetencionImpuestosLocalesPresent = false;
		boolean isImporteRetencionImpuestosLocalesPresentXML = false;
		boolean isImporteRetencionImpuestosLocalesValid = false;
		
		try {
			
			honorarios = findHonorariosByIdAgente(idAgente);
								
			if (honorarios != null 
					&& usuarioService.tienePermisoUsuarioActual("FN_M2_Agentes_Subir_Factura_Excepcion_Autorizada")
					&& honorarios.getAutorizadorExcepcion1() != null
					&& honorarios.getAutorizadorExcepcion2() != null) {
				
				excepcionImportes = true;
				
			}
			
		} catch(Exception e) {
			log.error("Error al cargar Honorarios para Validacion de XML");
			log.error(e.getMessage(), e);
			throw e;
		}
		
		try {
			
			gpo = envioFacturaDao.findByProperty(GrupoCatalogoAgente.class, "descripcion", ESTATUS_ENVIO_FACTURA_AGENTE).get(0);
			listValor = gpo.getValorCatalogoAgentes();
			
			for (ValorCatalogoAgentes val: listValor) {
				
				if (val.getClave().trim().equals(CLAVE_STATUS_ACEPTADO)) {
					
					statusAceptado = val;
					
				} else if (val.getClave().trim().equals(CLAVE_STATUS_RECHAZADO)) {
					
					statusRechazado = val;
					
				}
				
			}
			
			
			gpo = envioFacturaDao.findByProperty(GrupoCatalogoAgente.class, "descripcion", TIPO_MSJ_ENVIO_FACTURA_AGENTE).get(0);
			listValor = gpo.getValorCatalogoAgentes();
			
			for (ValorCatalogoAgentes val: listValor) {
				
				if (val.getClave().trim().equals(CLAVE_VALIDACION_FISCAL)) {
					
					tipoFiscal = val;
					
				} else if (val.getClave().trim().equals(CLAVE_VALIDACION_COMERCIAL)) {
					
					tipoComercial = val;
					
				}
				
			}
			
			
		} catch(Exception e) {
			log.error("Error al preparar datos de Envio Factura Agente");
			log.error(e.getMessage(), e);
			throw new PersistenceException();
		}
				
		try{
			
			agente.setIdAgente(idAgente);
			agente = agenteMidasService.loadByClave(agente);
			
		} catch(Exception e) {
			log.error("Error al cargar datos de Agente para Validacion de XML");
			log.error(e.getMessage(), e);
			throw e;
		}
		
		try {
			
			//se obtienen los parametros, se colocaron en base de datos para que sean mas faciles de cambiar
			parametros =  parametroGeneral.findByProperty("grupoParametroGeneral.descripcionGrupoParametroGral", PARAMETROS_ENVIO_FACTURA_AGENTE);
		
		} catch(Exception e) {
			log.error("Error al cargar parametros para Validacion de XML");
			log.error(e.getMessage(), e);
			throw e;
		}
		
		//se inicializan los parametros de honorarios
		for (ParametroGeneralDTO parametro : parametros) {
			if("Tolerancia".equals(parametro.getDescripcionParametroGeneral())){
				tolerancia = parametro.getValor();
			}else if("Empresa".equals(parametro.getDescripcionParametroGeneral())){
				empresa = parametro.getValor();
			}else if("Departamento".equals(parametro.getDescripcionParametroGeneral())){
				departamento = parametro.getValor();
			}
		}
		
		 try {
			 XMLNodeReader.Document.xmlnr = new XMLNodeReader();
		     XMLNodeReader node = XMLNodeReader.Document.read(archivo, "tfd:TimbreFiscalDigital", "UUID");
		     uuid = node.getAttributeValue();
		     facturaString = Utilerias.withoutCRLF(archivo); //It is visualized in one line.
		 } catch(Exception e) {
			 uuid = "";
			 facturaString = "";
		     System.err.println(e);
		 }
		
		try {
			if(honorarios.getimporteRetenidoEstatal() != null) {
				isRetencionImpuestosLocalesPresent = true;
				BigDecimal importeRetencionImpuestosLocales = honorarios.getimporteRetenidoEstatal(); 
				String attributeValueXML = null; 
				if(isElementPresentOnXML(facturaString, XML_COMPLEMENT_NAME_IMPUESTOS_LOCALES) && isElementPresentOnXML(facturaString, XML_ELEMENT_NAME_RETENCIONES_LOCALES)) {
					isImporteRetencionImpuestosLocalesPresentXML = true;
					BigDecimal importeRetencionImpuestosLocalesXML = BigDecimal.ZERO;
					if(!excepcionImportes) {
						attributeValueXML = getAttributeValueFromXML(getElementNodeFromXML(facturaString, XML_ELEMENT_NAME_RETENCIONES_LOCALES), XML_ATTRIBUTE_NAME_IMPORTE);
						importeRetencionImpuestosLocalesXML = new BigDecimal(attributeValueXML);
					}
					if(importeRetencionImpuestosLocales.compareTo(importeRetencionImpuestosLocalesXML) == 0) {
						isImporteRetencionImpuestosLocalesValid = true;
					} else {
						isImporteRetencionImpuestosLocalesValid = false;
					}
				} else {
					isImporteRetencionImpuestosLocalesPresentXML = false;
				}
			} else {
				isRetencionImpuestosLocalesPresent = false;
				if(isElementPresentOnXML(facturaString, XML_COMPLEMENT_NAME_IMPUESTOS_LOCALES) || isElementPresentOnXML(facturaString, XML_ELEMENT_NAME_RETENCIONES_LOCALES)) {
					isImporteRetencionImpuestosLocalesPresentXML = true;
				}
			}
		} catch(Exception e) {
			log.error("Error al validar el importe de retencion estatal");
			log.error(e.getMessage(), e);
		}
		
		String anioMes= honorarios.getAnioMes().toString();
		try {
			
			//Para los desarrolladores de siniestros  --> se cambio la regla de negocio 
			//el subtotal de la factura se compara contra el importe gravable + el importe excento
			
			subtotal = honorarios.getImpGravable().add(honorarios.getImpExcento());
			log.info("0rgoncar subtotal");
			proxySAT =  new WSValidacionesSATPortProxy();
			log.info("0rgoncar WSValidacionesSATPortProxy()");
			Object obj = proxySAT.verificaComprobanteFiscalDigital(facturaString, 
																"", 
																(agente.getPersona().getRfc()==null)?"":agente.getPersona().getRfc().replace(" ", ""),
																(subtotal==BigDecimal.ZERO || excepcionImportes)?"":subtotal.toString(), 
																"", 
																(honorarios.getImpRetIsr()==BigDecimal.ZERO || excepcionImportes)?"":honorarios.getImpRetIsr().toString(), 
																(honorarios.getImpRetIva()==BigDecimal.ZERO || excepcionImportes)?"":honorarios.getImpRetIva().toString(), 
																(honorarios.getImpIva()==BigDecimal.ZERO || excepcionImportes)?"":honorarios.getImpIva().toString(), 
																"", 
																(honorarios.getImpTotal()==BigDecimal.ZERO || excepcionImportes)?"":honorarios.getImpTotal().toString(),  
																tolerancia, 
																empresa, 
																departamento, 
																uuid,
																"");
			log.info("0rgoncar proxySAT.verificaComprobanteFiscalDigital " + obj);
			
			/*proxySAT.verificaComprobanteFiscalDigital(comprobante, 
														razonSocial, 
														rfc, 
														importe, se compara contra subtotal
														iva,   el iva que va aqui se compara contra la suma de todos los importes iva del CFDI
														isr, 
														ivaRetenido, 
														ivaTrasladado, se acordo que el iva trasladado es el iva normal
														descuento, 
														total, 
														tolerancia, 
														idSociedad, 
														idDepartamento, 
														referencia, 
														concepto)
														*/
			try{
				/**
				 * Almacena los datos de respuesta de axosnet en los envios generales para tener un concentrado de todos los envios al axosnet
				 * despues hay que dejar de guardar esta informacion en toenvioxmlagente
				 */
				envioFactura = this.envioFacturaService.saveEnvio(obj,  EnvioFacturaService.CLAVEAGENTE+anioMes, idAgente, cuentaAfectada, true);
			} catch(Exception e) {
				log.error("Error al guardar el envio en tabla generica agente: "+idAgente+" aniomes: "+anioMes);
				log.error(e.getMessage(), e);
			}
			
			response = (ValResponse)obj;
			mensajes = response.getMessages();
			
		} catch(Exception e) {
			log.error("Error de comunicacion con WS ValidacionesSAT");
			log.error(e.getMessage(), e);
			throw new ConnectException();
		}
		
		try{
									
			for(AxnResponse mensaje: mensajes){
				
				/* el mensaje.id es el que define que tipo de validacion es, si es menor que 20 es fiscal
				 * los espacios que quedan entre el 12 y 20 se deben llenar convalidaciones fiscales del 30 en 
				 * adelante con validaciones comerciales.
				 * 
				•	10 - Validación de versión (Fiscal)
				•	11 - Validación de primer sello (Fiscal)
				•	12 - Validación de segundo sello (Fiscal)
				•	20 - Validación de factura previamente ingresada (Comercial)
				•	21 - Validación de la razón social del emisor (Comercial)
				•	22 - Validación del RFC del emisor (Comercial)
				•	23 - Validación del subtotal (Comercial)
				•	24 - Validación del IVA (Trasladado + Retenido) (Comercial)
				•	25 - Validación del impuesto retenido ISR (Comercial)
				•	26 - Validación de impuesto IVA retenido (Comercial)
				•	27 - Validación del impuesto IVA trasladado (Comercial)
				•	28 - Validación de descuento (Comercial)
				•	29 - Validación del total (Comercial)
				•	30 - Validación del RFC del receptor (Comercial)
				*/
								
				mensajeEnvio =  new EnvioFacturaAgenteDet();
								
				mensajeEnvio.setTipoMensaje(mensaje.getType());
				mensajeEnvio.setMensaje(mensaje.getMessage());
								
				if(Integer.valueOf(mensaje.getId())<tipoRespuestaEnRepositorio) {
					mensajeEnvio.setTipoMsj(tipoFiscal);
				} else {
					
					mensajeEnvio.setTipoMsj(tipoComercial);
				
					if(Integer.valueOf(mensaje.getId()).intValue() == tipoRespuestaEnRepositorio) {
						fisicamenteEnRepositorio = true;
					}
					
				}
				
				if (mensaje.getCode().trim().equals("ER") && Integer.valueOf(mensaje.getId()).intValue() != tipoRespuestaErrores) {
					numeroErrores++;
				}
				
				mensajesEnvio.add(mensajeEnvio);
				
			}
			if(isRetencionImpuestosLocalesPresent) {
				EnvioFacturaAgenteDet mensajeErrorImpuestoLocalRetenido =  new EnvioFacturaAgenteDet();
				if(!isImporteRetencionImpuestosLocalesPresentXML) {
					mensajeErrorImpuestoLocalRetenido.setTipoMsj(tipoComercial);
					mensajeErrorImpuestoLocalRetenido.setMensaje(
							Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agente.factura.validacion.impuestosLocales.nullnode")
							);
					numeroErrores++;
					mensajesEnvio.add(mensajeErrorImpuestoLocalRetenido);
				} else if(!isImporteRetencionImpuestosLocalesValid) {
					double importeXML = Double.valueOf(getAttributeValueFromXML(getElementNodeFromXML(facturaString, XML_ELEMENT_NAME_RETENCIONES_LOCALES), XML_ATTRIBUTE_NAME_IMPORTE)).doubleValue();
					double importeDB = honorarios.getimporteRetenidoEstatal().doubleValue();
					double diferenciaImportes = importeXML - importeDB;
					mensajeErrorImpuestoLocalRetenido.setTipoMsj(tipoComercial);
					mensajeErrorImpuestoLocalRetenido.setMensaje(
							Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agente.factura.validacion.impuestosLocales.valorIncorrecto", importeXML, diferenciaImportes, tolerancia)
							);
					numeroErrores++;
					mensajesEnvio.add(mensajeErrorImpuestoLocalRetenido);
				}
			} else {
				if(isImporteRetencionImpuestosLocalesPresentXML) {
					EnvioFacturaAgenteDet mensajeErrorImpuestoLocalRetenido =  new EnvioFacturaAgenteDet();
					mensajeErrorImpuestoLocalRetenido.setTipoMsj(tipoComercial);
					mensajeErrorImpuestoLocalRetenido.setMensaje(
							Utilerias.getMensajeRecurso(SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agente.factura.validacion.impuestosLocales.nodepresent")
							);
					numeroErrores++;
					mensajesEnvio.add(mensajeErrorImpuestoLocalRetenido);
				}
			}
		} catch(Exception e) {
			log.error("Error al guardar detalle de Envio Factura  Agente");
			log.error(e.getMessage(), e);
			throw new PersistenceException();
		}
		
				
		try {
			
			envio  = new EnvioFacturaAgente();	
			envio.setIdAgente(idAgente);
			envio.setNombreArchivo(uuid + ".xml");
			envio.setUuid(uuid);
			
			for (EnvioFacturaAgenteDet mensajeEnvioI : mensajesEnvio) {
				
				mensajeEnvioI.setEnvioFactura(envio);
				
				envio.getMensajes().add(mensajeEnvioI);
				
			}
			 
			//Tambien se considera envio aceptado en el caso de que el WS mande un rechazo porque ya existia la factura en el repositorio
			//pero aun no se encuentra aceptado en el Inventario de Facturas
			if (response.isValid() || (numeroErrores == 1 && fisicamenteEnRepositorio && !existeEnInventarioFacturas(uuid))) {
				envio.setHonorarios(honorarios);
				honorarios.setEnvioFactura(envio);
				honorarios.setStatus(statusAceptado);
				envio.setStatus(statusAceptado);
				
				
			} else {
				
				envio.setStatus(statusRechazado);
				
			}
			envio.setUsuario(userName);
			envio = envioFacturaDao.guardaEnvio(envio);
			
			/**
			 * Aqui se relaciona los datos del xml con los cheques para la e-contabilidad siempre y cuando el xml este correcto
			 */
			if (response.isValid() || (numeroErrores == 1 && fisicamenteEnRepositorio && !existeEnInventarioFacturas(uuid))) {
				this.envioFacturaDao.saveRelationXMLCheque(idAgente, anioMes, envioFactura.getIdEnvio());	
			}
			
		} catch(Exception e) {
			log.error("Error al guardar el Envio de Factura Agente");
			log.error(e.getMessage(), e);
			throw e;
		}
		
		enviaCorreoResultado(envio, agente, parametros);
				
		return envio;
	}
	
	@Override
	public HonorariosAgente findHonorariosByIdAgente(Long idAgente) {
		
		//Se obtienen los honorarios de la factura no entregada mas vieja por aniomes
		Map<String, Object> params = new HashMap<String, Object>();
		
		params.put("idAgente", idAgente);
		params.put("envioFactura", null);
		
		List<HonorariosAgente> lista = envioFacturaDao.findByProperties(HonorariosAgente.class, params);
		
		if (lista != null && !lista.isEmpty()) {
		
			Collections.sort(lista);
			return lista.get(0);
			
		}
		
		return null;
	}
	
	@Override
	public void autorizaExcepcion(Long idAgente) {
				
		if ((!usuarioService.tienePermisoUsuarioActual("FN_M2_Agentes_Autorizar_Excepcion_Factura"))
				|| (!usuarioService.tieneRolUsuarioActual("Rol_M2_Autorizador_Excepcion_1") 
						&& !usuarioService.tieneRolUsuarioActual("Rol_M2_Autorizador_Excepcion_2"))) {
			return;
		}
		
		HonorariosAgente honorarios = findHonorariosByIdAgente(idAgente);
		
		if (honorarios == null) return;
		
		Usuario usuario = usuarioService.getUsuarioActual();
		boolean actualizar = false;
		
		if (usuarioService.tieneRolUsuarioActual("Rol_M2_Autorizador_Excepcion_1") && honorarios.getAutorizadorExcepcion1() == null) {
			
			honorarios.setAutorizadorExcepcion1(usuario.getNombreUsuario());
			honorarios.setFechaAutorizacionExcepcion1(new Date());
			
			actualizar = true;
			
		} else if (usuarioService.tieneRolUsuarioActual("Rol_M2_Autorizador_Excepcion_2") && honorarios.getAutorizadorExcepcion2() == null) {
			
			honorarios.setAutorizadorExcepcion2(usuario.getNombreUsuario());
			honorarios.setFechaAutorizacionExcepcion2(new Date());		
			
			actualizar = true;
			
		}
			
		if (actualizar) {
			
			entidadDao.update(honorarios);
			
		}
		
	}
	
	@Override
	public void marcarFacturasAntiguas(Long idAgente) {
		
		if (idAgente == null) {
			
			throw new RuntimeException("idAgente no debe ser null");
			
		}
		
		boolean esAdministrador = usuarioService.tienePermisoUsuarioActual("FN_M2_Carga_Facturas_Administrador");
		
		if (!esAdministrador) {
			
			throw new RuntimeException("El usuario no tiene los permisos necesarios para realizar la operacion");
			
		}
		
		EnvioFacturaAgenteEst registroFacturasAntiguas = obtenerRegistroFacturasAntiguasPendientes(idAgente);
		
		if (registroFacturasAntiguas == null) {
			
			throw new RuntimeException("El agente no tiene facturas antiguas pendientes");
			
		}
				
		registroFacturasAntiguas.setEstatus(EnvioFacturaAgenteEst.ESTATUS_FACTURAS_ENTREGADAS);
		
		entidadDao.update(registroFacturasAntiguas);
		
	}
	
	@Override
	public boolean tieneFacturasAntiguasPendientes(Long idAgente) {
				
		if (obtenerRegistroFacturasAntiguasPendientes(idAgente) != null) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	
	private EnvioFacturaAgenteEst obtenerRegistroFacturasAntiguasPendientes(Long idAgente) {
		
		Map<String, Object> parametros = new LinkedHashMap<String, Object>();
		parametros.put("idAgente", idAgente);
		parametros.put("estatus", EnvioFacturaAgenteEst.ESTATUS_FACTURAS_PENDIENTES);
		
		List<EnvioFacturaAgenteEst> resultado = entidadDao.findByProperties(EnvioFacturaAgenteEst.class, parametros);
		
		if (resultado != null && !resultado.isEmpty()) {
			
			return resultado.get(0);
			
		}
		
		return null;
	}
	
	
	private boolean existeEnInventarioFacturas(String uuid) {
		
		Map<String, Object> params = new HashMap<String, Object>();
		params.put("uuid", uuid);
		params.put("status.clave", CLAVE_STATUS_ACEPTADO);
		
		List<EnvioFacturaAgente> lista = envioFacturaDao.findByProperties(EnvioFacturaAgente.class, params);
		
		if (lista != null && !lista.isEmpty()) {
			
			return true;
			
		}
		
		return false;
		
	}
	
	
	private void enviaCorreoResultado(EnvioFacturaAgente envio, Agente agente, List<ParametroGeneralDTO> parametros) {
		
		List<String> address = new ArrayList<String>();
		List<ByteArrayAttachment> attachment = new ArrayList<ByteArrayAttachment>();
		String saludoCorreo = null;
		String mensajeCorreo = null;
		String mensajeRechazo = null;
		String mensajeDespedida = null;
		String mensajeGeneral = null;
		try {
			
			//se inicializan los parametros de correo
			for(ParametroGeneralDTO parametro: parametros){
				if("Saludo Correo".equals(parametro.getDescripcionParametroGeneral())){
					saludoCorreo = parametro.getValor();
				}else if("Mensaje Correo".equals(parametro.getDescripcionParametroGeneral())){
					mensajeGeneral = parametro.getValor();
				}else if("Mensaje Rechazo".equals(parametro.getDescripcionParametroGeneral())){
					mensajeRechazo = parametro.getValor();
				}else if("Mensaje Despedida".equals(parametro.getDescripcionParametroGeneral())){
					mensajeDespedida = parametro.getValor();
				}
			}
			
			
			//se crea la lista que va en el correo
			
		
		
			//se agrega la respuesta al cuerpo del correo
			mensajeCorreo = mensajeGeneral.replace("estatusEnvioDesc", envio.getStatus().getValor().toUpperCase())+
					(envio.getStatus().getClave().trim().equals(CLAVE_STATUS_RECHAZADO)? mensajeRechazo + obtenerListaMensajes(envio) : null) + mensajeDespedida;
			
			address.add(agente.getPersona().getEmail());
			
			mailService.sendMail(address, "Notificacion de Validacion de Factura", mensajeCorreo, attachment, 
					"Notificacion de Validacion de Factura", saludoCorreo);
			
		} catch(Exception e) {
			log.error("Error al Enviar Correo de Notificacion Envio Factura Agente");
			log.error(e.getMessage(), e);
			throw new RuntimeException(e);
		}
				
	}
		
	public String obtenerListaMensajes(EnvioFacturaAgente envio) {
		StringBuilder listaMensajes = new  StringBuilder("");
		for (EnvioFacturaAgenteDet mensaje : envio.getMensajes()) {
			
			 listaMensajes.append(mensaje.getTipoMensaje() ).append("  " ).append(mensaje.getMensaje()).append("<br />");
			
		}		
		return listaMensajes.toString();
	}

	private String getUUIDFromXML(String xmlString) {
		final int prefijo = 6;
		//Ej. UUID="259F974F-94A4-4FA0-8E69-2A9CEAC9FB00" 
		String chunck = xmlString.substring(xmlString.toUpperCase().indexOf("UUID") + prefijo);
		return chunck.substring(0, chunck.indexOf("\""));
		
		
	}
	
	private String getAttributeValueFromXML(String xmlString, String attributeName) {
		final int prefijo = attributeName.length() + 2;
		String chunck = xmlString.substring(xmlString.indexOf(attributeName) + prefijo);
		return chunck.substring(0, chunck.indexOf("\""));
	}
	
	private boolean isElementPresentOnXML(String xmlString, String elementName) {
		boolean isIt;
		if(xmlString.contains(elementName)) {
			isIt = true;
		} else {
			isIt = false;
		}
		return isIt;
	}
	
	private String getElementNodeFromXML(String xmlString, String elementName) {
		String node = null;
		node = xmlString.substring(xmlString.indexOf(elementName) - 1);
		node = node.substring(0, node.indexOf(">") + 1);
		return node;
	}
			
	
	/*
	 * Notificacion a agentes con facturas pendientes
	 */
		
	@TransactionAttribute(TransactionAttributeType.NOT_SUPPORTED)
	@Schedule(dayOfWeek="Fri", hour="20", persistent=false)
	private void notificarAgentesFacturaPendiente(Timer timer) {
		
		/*
		 Solo se mandará a agentes vigentes, autorizados, que contabilizan, que generan cheque 
		 y que tengan facturas pendientes por entregar desde Enero del 2015 o posteriores
		*/
		
		log.info("Comenzando proceso notificarAgentesFacturaPendiente()...");
		
		log.info("getNextTimeout: " + timer.getNextTimeout());
		
		log.info("getTimeRemaining: " + timer.getTimeRemaining());
		
		int contador = 0;
		
		List<AgenteFacturaPendiente> agentesFacturaPendiente =  envioFacturaDao.getAgentesFacturaPendiente();
		
		log.info("Se encontraron " + agentesFacturaPendiente.size() + " agentes para notificar.");
		
		for (AgenteFacturaPendiente agenteFacturaPendiente : agentesFacturaPendiente) {
			
			try {
				
				contador++;
				
				log.info("Notificacion " + contador + " de " +  agentesFacturaPendiente.size());
				
				enviarCorreoFacturaPendiente(agenteFacturaPendiente);
				
			} catch (Exception ex) {
				
				log.error("Error en notificarAgentesFacturaPendiente :", ex);
				
			}
			
		}
		
		log.info("Proceso notificarAgentesFacturaPendiente() ha terminado.");
		
	}
			
	private void enviarCorreoFacturaPendiente(AgenteFacturaPendiente agenteFacturaPendiente) throws Exception{
				
		Long clave = agenteFacturaPendiente.getIdAgente();
		
		log.info("Se intentara enviar una notificacion de factura pendiente al agente " + clave);
		
		Agente agente = findAgenteByClave(clave);
		
		if (agente.getPersona().getEmail().isEmpty()) {
			
			throw new RuntimeException("El agente no cuenta con un email configurado.");
			
		}
				
		//Se arma el correo
		
		//Para
		
		List<String> para = new ArrayListNullAware<String>();
		
		para.add(agente.getPersona().getEmail());
		
		//Asunto
		
		String asunto = Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agente.factura.pendiente.correo.asunto");
				
		//Cuerpo
		
		String nombre = agente.getPersona().getNombreCompleto();
				
		Long periodoActual = agtSaldoService.actualizarSaldosMesAbierto(clave, 
				new Long(Calendar.getInstance().get(Calendar.YEAR)));
				
		HashMap<String,Object> properties = new HashMap<String,Object>();
		properties.put("verdaderoIdAgente", clave);
		properties.put("anioMes", periodoActual);
		
		AgtSaldo agtSaldo = entidadDao.findByProperties(AgtSaldo.class, properties).get(0);
		
		BigDecimal saldoActual = (agtSaldo.getImporteSaldoFinal() != null ? agtSaldo.getImporteSaldoFinal() : BigDecimal.ZERO);
				
		String descripcionMesPrimeraFacturaPendiente = convertirFormato(agenteFacturaPendiente.getAnioMes().toString());
				
		String anioPrimeraFacturaPendiente = agenteFacturaPendiente.getAnioMes().toString().substring(0,4);
								
		String cuerpo = Utilerias.getMensajeRecurso(
				SistemaPersistencia.ARCHIVO_RECURSOS, "midas.agente.factura.pendiente.correo.cuerpo", clave.toString(), nombre, 
				descripcionMesPrimeraFacturaPendiente, anioPrimeraFacturaPendiente, saldoActual);
		
		//Adjuntos
		
		TransporteImpresionDTO reciboHonorarios = generarPlantillaReporte.imprimirReciboHonorarios(agenteFacturaPendiente.getSolicitudChequeId());
		
		List<ByteArrayAttachment> adjuntos = new ArrayList<ByteArrayAttachment>();
		
		adjuntos.add(new ByteArrayAttachment(
				TransporteImpresionDTO.DOC_RECIBO_HONORARIOS + ".pdf", 
				ByteArrayAttachment.TipoArchivo.PDF, 
				reciboHonorarios.getByteArray()));
		
		//Banners institucionales
		
		Map<String, String> mapInlineImages = new HashMap<String, String>();
				
		mapInlineImages.put("header_img", sistemaContext.getDirectorioImagenesParametroGlobal() +"reportes/mediciones_header.jpg");
		
		mapInlineImages.put("footer_img", sistemaContext.getDirectorioImagenesParametroGlobal() +"reportes/mediciones_footer.jpg");
			
		/*
		 sendMessage(List<String> recipients, String subject, String messageContent, 
		    String from, String[] attachments, List<ByteArrayAttachment> fileAttachments,
		    List<String> cc,List<String> cco, Map<String, String> mapInlineImages)
		 */
		
		mailService.sendMessage(para, asunto, cuerpo,
				"no_reply@afirme.com", null, adjuntos, 
				null, null, mapInlineImages);
		
		log.info("Termino el proceso para el agente " + clave);
		
	}
	
	private Agente findAgenteByClave(Long clave) throws Exception {
		
		Agente filtro = new Agente();
		
		filtro.setIdAgente(clave);
			
		return agenteMidasService.findByClaveAgente(filtro);
		
	}
		
	private String convertirFormato(String periodo) {
		
		try {
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyyMM", new Locale("es", "ES"));
		
			Date fecha;
		
			fecha = sdf.parse(periodo);
		
		
			sdf.applyPattern("MMMMMMMMMM");
				
			return sdf.format(fecha);
			
		} catch (ParseException e) {
		
			throw new RuntimeException(e);
			
		}
		
	}
	
	public EnvioFacturaAgenteDao getEnvioFacturaDao() {
		return envioFacturaDao;
	}
	
	@EJB
	public void setEnvioFacturaDao(EnvioFacturaAgenteDao envioFacturaDao) {
		this.envioFacturaDao = envioFacturaDao;
	}
	
	public MailService getMailService() {
		return mailService;
	}

	@EJB
	public void setMailService(MailService mailService) {
		this.mailService = mailService;
	}
	
	public AgenteMidasService getAgenteMidasService() {
		return agenteMidasService;
	}
	
	@EJB
	public void setAgenteMidasService(AgenteMidasService agenteMidasService) {
		this.agenteMidasService = agenteMidasService;
	}
	
	public ParametroGeneralFacadeRemote getParametroGeneral() {
		return parametroGeneral;
	}
	
	@EJB
	public void setParametroGeneral(ParametroGeneralFacadeRemote parametroGeneral) {
		this.parametroGeneral = parametroGeneral;
	}

	public EnvioFacturaService getEnvioFacturaService() {
		return envioFacturaService;
	}

	@EJB
	public void setEnvioFacturaServiceImpl(
			EnvioFacturaService envioFacturaService) {
		this.envioFacturaService = envioFacturaService;
	}
	
	public UsuarioService getUsuarioService() {
		return usuarioService;
	}

	@EJB(beanName="UsuarioServiceDelegate")
	public void setUsuarioService(UsuarioService usuarioService) {
		this.usuarioService = usuarioService;
	}
	
	public EntidadDao getEntidadDao() {
		return entidadDao;
	}

	@EJB
	public void setEntidadDao(EntidadDao entidadDao) {
		this.entidadDao = entidadDao;
	}
	
	public SistemaContext getSistemaContext() {
		return sistemaContext;
	}

	@EJB
	public void setSistemaContext(SistemaContext sistemaContext) {
		this.sistemaContext = sistemaContext;
	}
	
	public GenerarPlantillaReporte getGenerarPlantillaReporte() {
		return generarPlantillaReporte;
	}

	@EJB
	public void setGenerarPlantillaReporte(GenerarPlantillaReporte generarPlantillaReporte) {
		this.generarPlantillaReporte = generarPlantillaReporte;
	}
	
	public AgtSaldoService getAgtSaldoService() {
		return agtSaldoService;
	}

	@EJB
	public void setAgtSaldoService(AgtSaldoService agtSaldoService) {
		this.agtSaldoService = agtSaldoService;
	}

	@Override
	public EnvioFacturaAgente persistEnvio(Long idAgente, File archivo)
			throws Exception {
		return persistEnvio(idAgente, archivo, null);
	}
	
	@Override
	public Agente getDatosAgente(String codigoUsuario, Integer idPersona, Long idAgente
			) throws Exception {
		return envioFacturaDao.getDatosAgente(codigoUsuario, idPersona, idAgente);
	}
	@Override
	public PrestadorServicio getDatosPrestadorServicio(Integer idPersona, Long idProveedor) throws Exception {
		return envioFacturaDao.getDatosPrestadorServicio(idPersona,idProveedor);
	}	
}
