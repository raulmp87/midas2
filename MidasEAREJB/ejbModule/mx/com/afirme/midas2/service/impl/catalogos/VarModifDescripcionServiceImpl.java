package mx.com.afirme.midas2.service.impl.catalogos;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.EJB;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;

import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoDTO;
import mx.com.afirme.midas.tarifa.configuracion.catalogovalorfijo.CatalogoValorFijoFacadeRemote;
import mx.com.afirme.midas2.dao.catalogos.VarModifDescripcionDao;
import mx.com.afirme.midas2.domain.catalogos.VarModifDescripcion;
import mx.com.afirme.midas2.dto.wrapper.CatalogoValorFijoComboDTO;
import mx.com.afirme.midas2.service.catalogos.VarModifDescripcionService;


@Stateless
@TransactionManagement(TransactionManagementType.CONTAINER)
public class VarModifDescripcionServiceImpl implements VarModifDescripcionService {

	public List<VarModifDescripcion> findAll() {
		return varModifDescripcionDao.findAll();
	}

	public List<VarModifDescripcion> findByFilters(VarModifDescripcion filtroVarModifDescripcion) {
		if(filtroVarModifDescripcion==null)filtroVarModifDescripcion = new VarModifDescripcion();
		return varModifDescripcionDao.findByFilters(filtroVarModifDescripcion);
	}

	public VarModifDescripcion findById(Long id) {
		return varModifDescripcionDao.findById(id);
	}
	@TransactionAttribute(TransactionAttributeType.REQUIRED)
	public void remove(VarModifDescripcion VarModifDescripcion) {
		VarModifDescripcion = varModifDescripcionDao.getReference(VarModifDescripcion.getId());
		varModifDescripcionDao.remove(VarModifDescripcion);
	}

	public VarModifDescripcion save(VarModifDescripcion VarModifDescripcion) {
		
		if (VarModifDescripcion.getId() != null) {
			return varModifDescripcionDao.update(VarModifDescripcion);
		} else {
			varModifDescripcionDao.persist(VarModifDescripcion);
			return VarModifDescripcion;
		}
	}
	
	
	public List<CatalogoValorFijoDTO> listarCombo(){
		
		return catalogoValorFijoFacadeRemote.findByProperty("id.idGrupoValores", 321);
		
	}
	
	
	public List<CatalogoValorFijoComboDTO> listarComboGrupos(){
		
		List<CatalogoValorFijoDTO> grupos = catalogoValorFijoFacadeRemote.findByProperty("id.idGrupoValores", 321);
		
		List<CatalogoValorFijoComboDTO> listaResultado = new ArrayList<CatalogoValorFijoComboDTO>();
		
		for(CatalogoValorFijoDTO grupo : grupos){
			listaResultado.add(new CatalogoValorFijoComboDTO(grupo));
		}
		
		return listaResultado;
		
	}
	
	
	
	
	@Override
	public List<VarModifDescripcion> findByGroup(Integer group) {
		return varModifDescripcionDao.findByGroup(group);
	}

	private VarModifDescripcionDao varModifDescripcionDao;
	
	private CatalogoValorFijoFacadeRemote catalogoValorFijoFacadeRemote;
	
	@EJB
	public void setVarModifDescripcionDao(VarModifDescripcionDao varModifDescripcionDao) {
		this.varModifDescripcionDao = varModifDescripcionDao;
	}
	
	
	@EJB
	public void setCatalogoValorFijoFacadeRemote(CatalogoValorFijoFacadeRemote catalogoValorFijoFacadeRemote) {
		this.catalogoValorFijoFacadeRemote = catalogoValorFijoFacadeRemote;
	}

	
	
	

}
