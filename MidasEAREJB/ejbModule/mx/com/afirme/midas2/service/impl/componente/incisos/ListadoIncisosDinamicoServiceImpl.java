package mx.com.afirme.midas2.service.impl.componente.incisos;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;

import javax.ejb.EJB;
import javax.ejb.Stateless;

import mx.com.afirme.midas.cotizacion.inciso.IncisoCotizacionDTO;
import mx.com.afirme.midas.poliza.PolizaDTO;
import mx.com.afirme.midas.sistema.LogDeMidasEJB3;
import mx.com.afirme.midas.solicitud.SolicitudDTO;
import mx.com.afirme.midas2.dao.componente.incisos.ListadoIncisosDinamicoDao;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.CotizacionContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.BitemporalInciso;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.AutoIncisoContinuity;
import mx.com.afirme.midas2.domain.bitemporal.suscripcion.cotizacion.inciso.auto.BitemporalAutoInciso;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.NegocioSeccion;
import mx.com.afirme.midas2.domain.negocio.producto.tipopoliza.seccion.paquete.NegocioPaqueteSeccion;
import mx.com.afirme.midas2.domain.siniestros.cabina.reportecabina.SiniestroCabina;
import mx.com.afirme.midas2.domain.siniestros.indemnizacion.IndemnizacionSiniestro;
import mx.com.afirme.midas2.dto.impresiones.DatosLineasDeNegocioDTO;
import mx.com.afirme.midas2.dto.impresiones.ReferenciaBancariaDTO;
import mx.com.afirme.midas2.service.bitemporal.EntidadBitemporalService;
import mx.com.afirme.midas2.service.catalogos.EntidadService;
import mx.com.afirme.midas2.service.componente.incisos.ListadoIncisosDinamicoService;
import mx.com.afirme.midas2.service.componente.incisos.TipoQueryListadoIncisos;
import mx.com.afirme.midas2.service.siniestros.valuacion.ordencompra.OrdenCompraService;
import mx.com.afirme.midas2.service.suscripcion.cotizacion.auto.calculo.CalculoService;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;
import org.joda.time.DateTime;

import com.anasoft.os.daofusion.bitemporal.IntervalWrapper;
import com.anasoft.os.daofusion.bitemporal.RecordStatus;
import com.anasoft.os.daofusion.bitemporal.TimeUtils;

@Stateless
public class ListadoIncisosDinamicoServiceImpl implements  ListadoIncisosDinamicoService{
	
	private EntidadService entidadService;
	private ListadoIncisosDinamicoDao listadoIncisosDinamicoDao;
	private EntidadBitemporalService entidadBitemporalService;
	private CalculoService calculoService;
	
	private static Long incisoContinuityId;
	
	public static Long getIncisoContinuityId() {
		return incisoContinuityId;
	}

	public static void setIncisoContinuityId(Long incisoContinuityId) {
		ListadoIncisosDinamicoServiceImpl.incisoContinuityId = incisoContinuityId;
	}

	@Override
	public List<BitemporalInciso> buscarIncisosFiltrado(
			IncisoCotizacionDTO filtros, short claveTipoEndoso,
			Long idToPoliza, Date validoEn, int idTipoVista) {	
			return buscarIncisosFiltrado(filtros, claveTipoEndoso, idToPoliza, validoEn, false, idTipoVista);
	}
	
	@Override
	public List<BitemporalInciso> buscarIncisosFiltrado(
			IncisoCotizacionDTO filtros, short claveTipoEndoso,
			Long idToPoliza, Date validoEn, boolean buscaValido, int tipoVista) {	
		return buscarIncisosFiltrado(filtros, claveTipoEndoso, idToPoliza, validoEn, null, buscaValido, tipoVista);
	}
	
	@Override
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, short claveTipoEndoso,
										Long idToPoliza, Date validoEn, Date recordFrom, boolean buscaValido, int tipoVista) {			
		return buscarIncisosFiltrado(filtros, claveTipoEndoso, idToPoliza, validoEn, recordFrom, buscaValido, false, tipoVista);
	}

	@Override
	public List<BitemporalInciso> buscarIncisosFiltrado(IncisoCotizacionDTO filtros, short claveTipoEndoso,
										Long idToPoliza, Date validoEn, Date recordFrom, boolean buscaValido, boolean esConsultaPoliza, int tipoVista) {			
		
		List<BitemporalInciso> resultsList = new ArrayList<BitemporalInciso>();	
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, new BigDecimal(idToPoliza));
		
		if (poliza == null)	{
			return resultsList;
		}
		
		boolean getCancelados = false;
		
		List<CotizacionContinuity> cotizaciones = entidadService.findByProperty(CotizacionContinuity.class, "numero", poliza
				.getCotizacionDTO().getIdToCotizacion());
		
		if (!cotizaciones.isEmpty()) {
			
			if (esConsultaPoliza) {
				resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(filtros, cotizaciones.get(0).getId(), validoEn, 
						recordFrom, null, false, true);
			} else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
				resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(filtros, cotizaciones.get(0).getId(),RecordStatus.TO_BE_ADDED,false);
			} else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS && tipoVista > 0 && tipoVista == 6) {
				resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(filtros, cotizaciones.get(0).getId(),RecordStatus.TO_BE_ADDED,false);
			} else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS) {
				resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(filtros, cotizaciones.get(0).getId(),  null,true);
				getCancelados = true;
			} else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL || 
					claveTipoEndoso ==  SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL){
				resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltradoPerdidaTotal(filtros, cotizaciones.get(0).getId());
				
				fillAutoIncisosPT(resultsList);
				llenarDatosSiniestroPT(resultsList,idToPoliza);	
				
				return resultsList;
			}else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS)	{
				resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltradoDesagrupacionRecibos(filtros, validoEn, 
						recordFrom, cotizaciones.get(0).getId());				
			} else {
				if (buscaValido) {
					resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(filtros, cotizaciones.get(0).getId(), validoEn, 
																											recordFrom, null, false);
				} else {
					resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(filtros, cotizaciones.get(0).getId(), null,false);
				}				
			}
			
			//Setear Valor Prima Total
			if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS || claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO) {
				setIncisosValorPrimaTotal(resultsList, cotizaciones.get(0), validoEn, claveTipoEndoso);				
			}
						
			if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS || claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_CAMBIO_DATOS) {
				setBlocked(resultsList, validoEn);			
			}
			
			fillAutoIncisos(resultsList, getCancelados, validoEn, recordFrom);	
		}
				
		return resultsList;
	}
	
	@SuppressWarnings("unused")
	private List<BitemporalInciso> mergeIncisosInProcess(Collection<BitemporalInciso> incisosValidCollection,Collection<BitemporalInciso> incisosInProccesCollection)
	{
		List<BitemporalInciso> mergedList = new ArrayList<BitemporalInciso>();
		
		if(!incisosValidCollection.isEmpty())
		{
			mergedList = new ArrayList<BitemporalInciso>(incisosValidCollection);				
		}
		
		if(!incisosInProccesCollection.isEmpty())
		{
			Iterator<BitemporalInciso> itListInProcess = incisosInProccesCollection.iterator();
			while(itListInProcess.hasNext())
			{
				BitemporalInciso incisoInProcess = itListInProcess.next();
				int indexElementToBeReplaced = -1;
				
				Iterator<BitemporalInciso> itMergedList = mergedList.iterator();
				while(itMergedList.hasNext())
				{
					BitemporalInciso incisoValid = itMergedList.next();	
					if(incisoInProcess.getContinuity().getId() == incisoValid.getContinuity().getId())
					{
						indexElementToBeReplaced = mergedList.indexOf(incisoValid);
						break;
					}						
				}
				
				if(indexElementToBeReplaced >= 0)
				{
					mergedList.set(indexElementToBeReplaced, incisoInProcess);	
					mergedList.get(indexElementToBeReplaced).getValue().setChecked(true);
				}
			}				
		}		
		
		return mergedList;		
	}	
	
	private List<BitemporalInciso> fillAutoIncisos(List<BitemporalInciso> incisosList, boolean getCancelados, 
																		Date validoEn, Date recordFrom) {		
		Iterator<BitemporalInciso> itIncisosList = incisosList.iterator();
		while (itIncisosList.hasNext()) {
			BitemporalInciso bitemporalInciso = itIncisosList.next();
			DateTime validoEnDT = null;
			DateTime recordFromDT = null;
			if (validoEn != null && recordFrom != null) {
				validoEnDT = new DateTime(validoEn);
				recordFromDT = new DateTime(recordFrom);
			}
			
			this.fillAutoInciso(bitemporalInciso, getCancelados, validoEnDT, recordFromDT);
						
		}		
		
		return incisosList;	
	}	
	
	private List<BitemporalInciso> fillAutoIncisosPT(
			List<BitemporalInciso> incisosList) {
		
		Iterator<BitemporalInciso> itIncisosList = incisosList.iterator();
		while (itIncisosList.hasNext()) 
		{
			BitemporalInciso bitemporalInciso = itIncisosList.next();
			DateTime validoEnDT = bitemporalInciso.getValidityInterval().getInterval().getStart();
			DateTime recordFromDT = bitemporalInciso.getRecordInterval().getInterval().getStart();
			
			this.fillAutoInciso(bitemporalInciso, false, validoEnDT,
					recordFromDT);
		}

		return incisosList;
	}

	private List<BitemporalInciso> llenarDatosSiniestroPT(List<BitemporalInciso> listadoIncisos, Long idToPoliza) {
		
		Iterator<BitemporalInciso> itListadoIncisos = listadoIncisos.iterator();
		while (itListadoIncisos.hasNext()) {
			
			BitemporalInciso bitemporalInciso = itListadoIncisos.next();	
			
			Map<String,Object> parametros = new HashMap<String,Object>();
			parametros.put("reporteCabina.poliza.idToPoliza", idToPoliza);			
			parametros.put("reporteCabina.seccionReporteCabina.incisoReporteCabina.numeroInciso", bitemporalInciso.getContinuity().getNumero()); 
			
			StringBuilder queryWhere = new StringBuilder();  
			queryWhere.append(" AND model.id = (SELECT MAX(siniestro.id) FROM SiniestroCabina siniestro WHERE siniestro.reporteCabina.poliza.idToPoliza=" + idToPoliza +
					" AND siniestro.reporteCabina.seccionReporteCabina.incisoReporteCabina.numeroInciso = " + bitemporalInciso.getContinuity().getNumero() + ")" );
								
			//Se obtiene el siniestro mas reciente, teóricamente siempre debe corresponder al siniestro que define la pérdida total
			List<SiniestroCabina> listaReportes = entidadService.findByColumnsAndProperties(SiniestroCabina.class, 
					"id,reporteCabina,anioReporte,claveOficina,consecutivoReporte", 
					parametros, new HashMap<String,Object>(), queryWhere, null); 	
			
			if(!listaReportes.isEmpty())
			{
				SiniestroCabina siniestroCabina = listaReportes.get(0); 			
								
				parametros.clear();
				parametros.put("siniestro.id", siniestroCabina.getId());
				parametros.put("estatusIndemnizacion", "A");  
				SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yy HH:mm");
				
				queryWhere = new StringBuilder();  
				queryWhere.append(" AND model.ordenCompra.coberturaReporteCabina.claveTipoCalculo IN ('DM','RT') " +
						"AND model.fechaAutIndemnizacion >= FUNCTION('TO_DATE','" + 
						dateFormat.format(siniestroCabina.getReporteCabina().getFechaHoraOcurrido())+"','DD/MM/YY HH24:MI')"); 
				
				// Se asume que unicamente una de las 2 coberturas puede estar siniestrada(Daños materiales o perdida total)
				List<IndemnizacionSiniestro> listaIndemnizaciones =  entidadService.findByColumnsAndProperties(IndemnizacionSiniestro.class, 
						"id,esPagoDanios,ordenCompra.coberturaReporteCabina.id,fechaAutIndemnizacion,estatusIndemnizacion", 
						parametros, new HashMap<String,Object>(), queryWhere, null);
				  
				IndemnizacionSiniestro indemnizacionSiniestro = listaIndemnizaciones.get(0);
				
				bitemporalInciso.getValue().setSiniestroCabinaId(siniestroCabina.getId());
				bitemporalInciso.getValue().setFechaOcurrenciaSiniestro(siniestroCabina.getReporteCabina().getFechaOcurrido());
				System.out.println(siniestroCabina.getReporteCabina().getSeccionReporteCabina());
				bitemporalInciso.getValue().setTipoIndemnizacion(indemnizacionSiniestro.getEsPagoDanios() ?
						OrdenCompraService.TIPO_INDEMNIZACION_PAGO_DANIOS:
							OrdenCompraService.TIPO_INDEMNIZACION_PERDIDATOTAL);
				bitemporalInciso.getValue().setNumeroSiniestro(siniestroCabina.getReporteCabina().getNumeroReporte());		 		
			}			    
		}  
		
		return listadoIncisos;
	}
	
	@EJB
	public void setEntidadService(EntidadService entidadService) {
		this.entidadService = entidadService;
	}
	
	@EJB
	public void setListadoIncisosDinamicoDao(
			ListadoIncisosDinamicoDao listadoIncisosDinamicoDao) {
		this.listadoIncisosDinamicoDao = listadoIncisosDinamicoDao;
	}

	@EJB
	public void setEntidadBitemporalService(
			EntidadBitemporalService entidadBitemporalService) {
		this.entidadBitemporalService = entidadBitemporalService;
	}
	
	@EJB
	public void setCalculoService(CalculoService calculoService) {
		this.calculoService = calculoService;
	}
	

	@Override
	public Long obtenerTotalPaginacionIncisos(IncisoCotizacionDTO filtros,
			short claveTipoEndoso, Long idToPoliza, Date validoEn) {
		return obtenerTotalPaginacionIncisos(filtros, claveTipoEndoso, idToPoliza, validoEn, false, 0);
	}

	@Override
	public Long obtenerTotalPaginacionIncisos(IncisoCotizacionDTO filtros,
			short claveTipoEndoso, Long idToPoliza, Date validoEn, boolean buscaValido, int tipoVista) {
		
		Long total = 0L;
		
		PolizaDTO poliza = entidadService.findById(PolizaDTO.class, new BigDecimal(idToPoliza));
		
		if (poliza == null)	{
			return total;
		}
		
		List<CotizacionContinuity> cotizaciones = entidadService.findByProperty(CotizacionContinuity.class, "numero", poliza
				.getCotizacionDTO().getIdToCotizacion());
		
		if (!cotizaciones.isEmpty()) {
			
			if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
			{
				total = listadoIncisosDinamicoDao.obtenerTotalPaginacionIncisos(filtros, cotizaciones.get(0).getId(),RecordStatus.TO_BE_ADDED, false);
			}
			else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DE_MOVIMIENTOS && tipoVista > 0 && tipoVista == 6)
			{
				total = listadoIncisosDinamicoDao.obtenerTotalPaginacionIncisos(filtros, cotizaciones.get(0).getId(),RecordStatus.TO_BE_ADDED, false);
			}
			else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS)
			{
				total = listadoIncisosDinamicoDao.obtenerTotalPaginacionIncisos(filtros, cotizaciones.get(0).getId(),null, true);				
			
			}else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL || 
					claveTipoEndoso ==  SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL)
			{
				total = listadoIncisosDinamicoDao.obtenerTotalPaginacionIncisos(filtros, cotizaciones.get(0).getId(), validoEn, null, 
						true, TipoQueryListadoIncisos.INCISOS_PERDIDA_TOTAL);
				
			}else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_DESAGRUPACION_RECIBOS)
			{
				total = listadoIncisosDinamicoDao.obtenerTotalPaginacionIncisos(filtros, cotizaciones.get(0).getId(), validoEn, null, 
						true, TipoQueryListadoIncisos.INCISOS_RECIBOS_AGRUPADOS);				
			}			
			else
			{
				if (buscaValido) {
					total = listadoIncisosDinamicoDao.obtenerTotalPaginacionIncisos(filtros, cotizaciones.get(0).getId(), validoEn, null, false, null);
				} else {
					total = listadoIncisosDinamicoDao.obtenerTotalPaginacionIncisos(filtros, cotizaciones.get(0).getId(), null, false);
				}
															
			}			
		}
				
		return total;
		
	}

	@Override
	public Set<String> listarIncisosDescripcion(short claveTipoEndoso,
			Long cotizacionContinuityId, Date validoEn) {
		
		Set<String> listDescripcion =  new HashSet<String>();
		/*
		List<BitemporalInciso> resultsList = new ArrayList<BitemporalInciso>();
		
		boolean getCancelados = false;
		
		if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizacionContinuityId, RecordStatus.TO_BE_ADDED, false); //Empty Filter					
		}
		else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS)
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizacionContinuityId, null, true);	//Empty Filter	
			getCancelados = true;
		}
		else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL || 
				claveTipoEndoso ==  SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL)
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltradoPerdidaTotal(new IncisoCotizacionDTO(), cotizacionContinuityId); 
			
			fillAutoIncisosPT(resultsList);		
			
			Iterator<BitemporalInciso> iteratorResultsList = resultsList.iterator();
			while(iteratorResultsList.hasNext())
			{
				BitemporalInciso inciso = iteratorResultsList.next();
				listDescripcion.add(inciso.getValue().getAutoInciso().getDescripcionFinal());
			}
			
			return listDescripcion;
			
		}else
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizacionContinuityId, validoEn, null, false);	//Empty Filter	
		}
		
		fillAutoIncisos(resultsList,getCancelados, null, null);
		
				
		Iterator<BitemporalInciso> iteratorResultsList = resultsList.iterator();
		while(iteratorResultsList.hasNext())
		{
			BitemporalInciso inciso = iteratorResultsList.next();
			listDescripcion.add(inciso.getValue().getAutoInciso().getDescripcionFinal());
		}
		*/
		if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
		{
			listDescripcion = listadoIncisosDinamicoDao.listarIncisosDescripcion(cotizacionContinuityId, null, RecordStatus.TO_BE_ADDED, false); //Empty Filter					
		}
		else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS)
		{
			listDescripcion = listadoIncisosDinamicoDao.listarIncisosDescripcion(cotizacionContinuityId, null, null, true); //Empty Filter
		}
		else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_BAJA_INCISO_PERDIDA_TOTAL || 
				claveTipoEndoso ==  SolicitudDTO.CVE_TIPO_ENDOSO_CANCELACION_POLIZA_PERDIDA_TOTAL)
		{
			listDescripcion = listadoIncisosDinamicoDao.listarIncisosDescripcion(cotizacionContinuityId, null, null, false); //Empty Filter
		}else
		{
			listDescripcion = listadoIncisosDinamicoDao.listarIncisosDescripcion(cotizacionContinuityId, validoEn, null, false); //Empty Filter	
		}
		
		return listDescripcion;
	}

	@Override
	public List<NegocioSeccion> getSeccionList(short claveTipoEndoso,
			Long cotizacionContinuityId, Date validoEn) { 
		
		List<NegocioSeccion> negocionSeccionList = new ArrayList<NegocioSeccion>();
		/*
        List<BitemporalInciso> resultsList = new ArrayList<BitemporalInciso>();
        
        boolean getCancelados = false;
		
		if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizacionContinuityId, RecordStatus.TO_BE_ADDED, false); //Empty Filter						
		}
		else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS)
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizacionContinuityId, null, true); //Empty Filter
			getCancelados = true;
		}
		else
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizacionContinuityId, validoEn,null, false);	//Empty Filter			
		}
		
		fillAutoIncisos(resultsList,getCancelados, null, null);
		
		Iterator<BitemporalInciso> iteratorResultsList = resultsList.iterator();
		while(iteratorResultsList.hasNext())
		{
			BitemporalInciso inciso = iteratorResultsList.next();
			if(inciso.getValue().getAutoInciso().getNegocioSeccionId() != null)
			{
				NegocioSeccion negocioSeccion = entidadService.findById(NegocioSeccion.class, new BigDecimal(inciso.getValue().getAutoInciso().getNegocioSeccionId()));
				if(!negocionSeccionList.contains(negocioSeccion))
				{
					negocionSeccionList.add(negocioSeccion);					
				}								
			}			
		}		
		*/
		if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
		{
			negocionSeccionList = listadoIncisosDinamicoDao.getSeccionList(cotizacionContinuityId, null, RecordStatus.TO_BE_ADDED, false); //Empty Filter						
		}
		else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS)
		{
			negocionSeccionList = listadoIncisosDinamicoDao.getSeccionList(cotizacionContinuityId, null, null, true); //Empty Filter
		}
		else
		{
			negocionSeccionList = listadoIncisosDinamicoDao.getSeccionList(cotizacionContinuityId, validoEn, null, false); //Empty Filter			
		}
		return negocionSeccionList;
	}

	@Override
	public Map<Long, String> getMapNegocioPaqueteSeccionPorLineaNegocio(
			short claveTipoEndoso, Long cotizacionContinuityId, Date validoEn,
			BigDecimal idToNegSeccion) { 	
		
		Map<Long, String> map = new LinkedHashMap<Long, String>();
		
        PolizaDTO poliza = entidadService.findById(PolizaDTO.class, new BigDecimal(cotizacionContinuityId));
		
		if (poliza == null)
		{
			return map;
		}
		
		boolean getCancelados = false;
		
		List<CotizacionContinuity> cotizaciones = entidadService.findByProperty(CotizacionContinuity.class, "numero", poliza
				.getCotizacionDTO().getIdToCotizacion());
		
        List<BitemporalInciso> resultsList = new ArrayList<BitemporalInciso>();
		
		if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_ALTA_INCISO)
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizaciones.get(0).getId(), RecordStatus.TO_BE_ADDED, false); //Empty Filter			
		}
		else if(claveTipoEndoso == SolicitudDTO.CVE_TIPO_ENDOSO_REHABILITACION_INCISOS)
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizaciones.get(0).getId(), null, true);	//Empty Filter
			getCancelados = true;
		}
		else
		{
			resultsList = listadoIncisosDinamicoDao.buscarIncisosFiltrado(new IncisoCotizacionDTO(), cotizaciones.get(0).getId(),null, false);	//Empty Filter		
		}
		
		fillAutoIncisos(resultsList,getCancelados, null, null);
		
		Iterator<BitemporalInciso> iteratorResultsList = resultsList.iterator();
		while(iteratorResultsList.hasNext())
		{
			BitemporalInciso inciso = iteratorResultsList.next();
			if(inciso.getValue().getAutoInciso().getNegocioSeccionId() == idToNegSeccion.longValue())
			{
				map.put(inciso.getValue().getAutoInciso().getNegocioPaqueteId(), inciso.getValue().getAutoInciso().getPaquete().getDescripcion());	
			}			
		}		
		
		return map;
	}		
	
	private List<BitemporalInciso> setIncisosValorPrimaTotal(List<BitemporalInciso> incisosList, CotizacionContinuity cotizacionContinuity, Date validoEn, short tipoEndoso)
	{
		Iterator<BitemporalInciso> itIncisosList = incisosList.iterator();
		while(itIncisosList.hasNext())
		{
			BitemporalInciso bitemporalInciso = itIncisosList.next();
			bitemporalInciso.getValue().setValorPrimaTotal(calculoService.obtenerPrimaTotalAltaInciso(bitemporalInciso, cotizacionContinuity, validoEn, tipoEndoso));			
		}
		
		return incisosList;
	}
	
	private List<BitemporalInciso> setBlocked(List<BitemporalInciso> incisosList, Date fechaIniVigencia) {
		
		DateTime validoEn = TimeUtils.getDateTime(fechaIniVigencia);
		DateTime validoHasta1;
		DateTime validoHasta2;
		List<IntervalWrapper> intervalos;
		BitemporalInciso bitemporalInciso = null;
		SimpleDateFormat formatoFecha = new SimpleDateFormat("dd/MM/yyyy");
		
		Iterator<BitemporalInciso> itIncisosList = incisosList.iterator();
		
		while(itIncisosList.hasNext())
		{
			bitemporalInciso = itIncisosList.next();
			
			validoHasta1 = bitemporalInciso.getEntidadContinuity().getBitemporalProperty()
				.get(validoEn).getValidityInterval().getInterval().getEnd();
					
			intervalos = bitemporalInciso.getEntidadContinuity().getBitemporalProperty()
				.getMergedValidityIntervals();
		
			validoHasta2 = intervalos.get(0).getInterval().getEnd();
			
			if (!validoHasta1.equals(validoHasta2)) {
				bitemporalInciso.getValue().setBlocked(true);
				bitemporalInciso.getValue().setSiguienteModificacionProgramada("Modific. Prog. el " + formatoFecha.format(validoHasta1.toDate()));
			} else {
				bitemporalInciso.getValue().setBlocked(false);
			}
		}
		
		return incisosList;
	}
	
	public void fillAutoInciso(BitemporalInciso bitemporalInciso, boolean getCancelados) {
		fillAutoInciso(bitemporalInciso, getCancelados, null, null);
	}
	
	public void fillAutoInciso(BitemporalInciso bitemporalInciso, boolean getCancelados, DateTime validoEn, DateTime recordFrom)
	{
		if(bitemporalInciso.getRecordStatus() == RecordStatus.TO_BE_ENDED) {
			bitemporalInciso.getValue().setAutoInciso(bitemporalInciso.getEntidadContinuity().getAutoIncisoContinuity().getBitemporalProperty()
					.get(bitemporalInciso.getValidityInterval().getInterval().getEnd().minusDays(1)).getValue());				
		} else {
			
		    if (!getCancelados) {
		    	if (validoEn != null && recordFrom != null) {
		    		bitemporalInciso.getValue().setAutoInciso(bitemporalInciso.getEntidadContinuity().getAutoIncisoContinuity().getBitemporalProperty()
							.get(validoEn, recordFrom).getValue());	
		    	} else {
		    		//Ajuste para refresh de persistencia en bajas de inciso
		    		validoEn = bitemporalInciso.getValidityInterval().getInterval().getEnd().minusDays(1);
		    		BitemporalAutoInciso bitemporalAutoInciso = new BitemporalAutoInciso();
					if(bitemporalInciso.getContinuity().getAutoIncisoContinuity() != null){
						bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, bitemporalInciso.getContinuity().getAutoIncisoContinuity().getId(), bitemporalInciso.getContinuity().getId(),validoEn);			
					}else{
						bitemporalAutoInciso = entidadBitemporalService.prepareEndorsementBitemporalEntity(BitemporalAutoInciso.class, new AutoIncisoContinuity().getId(), bitemporalInciso.getContinuity().getId(),validoEn);							
					}
					bitemporalInciso.getValue().setAutoInciso(bitemporalAutoInciso.getValue());
		    	}
		    			    	
		    } else {
		    	Collection<BitemporalAutoInciso> collectionBiAutoIncisos = entidadBitemporalService.obtenerCancelados(
		    			BitemporalAutoInciso.class, bitemporalInciso.getEntidadContinuity().getAutoIncisoContinuity().getId(), 
		    			bitemporalInciso.getValidityInterval().getInterval().getStart(), 
		    			bitemporalInciso.getRecordInterval().getInterval().getEnd());
		    	if(!collectionBiAutoIncisos.isEmpty())
		    	{
		    		bitemporalInciso.getValue().setAutoInciso(collectionBiAutoIncisos.iterator().next().getValue());			    		
		    	}
		    }
		    
		    bitemporalInciso.getValue().setChecked(false);
		}			
		
		// Establece atributo checked			
		if(!getCancelados)
		{
			if(bitemporalInciso.getRecordStatus() == RecordStatus.TO_BE_ADDED || bitemporalInciso.getRecordStatus() == RecordStatus.TO_BE_ENDED
					|| bitemporalInciso.getContinuity().getAutoIncisoContinuity().getBitemporalProperty().hasRecordsInProcess())
			{
				bitemporalInciso.getValue().setChecked(true);
			}				
		}else
		{
			if(bitemporalInciso.getContinuity().getBitemporalProperty().hasRecordsInProcess())
			{
				bitemporalInciso.getValue().setChecked(true);					
			}				
		}
		
		try{
			if(bitemporalInciso.getValue().getAutoInciso().getPaquete() == null){
				NegocioPaqueteSeccion paqueteSeccion = entidadService.findById
					(NegocioPaqueteSeccion.class, bitemporalInciso.getValue().getAutoInciso().getNegocioPaqueteId());
				
				bitemporalInciso.getValue().getAutoInciso().setPaquete(paqueteSeccion.getPaquete());
			}
		}catch(Exception e){
			LogDeMidasEJB3.log("Error al consultar paquete inciso", Level.WARNING, e);
		}
	}
	
	
	@Override
	public List<BitemporalInciso> seleccionarIncisos(List<BitemporalInciso> incisos, String elementosSeleccionados) {	
		
		BitemporalInciso inciso = null;
		Boolean quitarSeleccion = false;
		//System.out.println("AAC elementosSeleccionados -->" + elementosSeleccionados);
		String[] continuitiesStringIds = null;
		elementosSeleccionados = elementosSeleccionados.replaceAll(", ", "");
		if(!elementosSeleccionados.isEmpty()) {
			continuitiesStringIds = elementosSeleccionados.split(",");			
		}	
		if (continuitiesStringIds != null && continuitiesStringIds.length > 0)		
		for (String continuityStringId : continuitiesStringIds) {
			if (!continuityStringId.trim().equals("")) {
				//System.out.println("AAC continuityStringId -->" + continuityStringId);
				
				if (continuityStringId.indexOf("-") > -1) {
					quitarSeleccion = true;
					continuityStringId = continuityStringId.substring(continuityStringId.indexOf("-") + 1);
				} else {
					quitarSeleccion = false;
				}
				
				ListadoIncisosDinamicoServiceImpl.setIncisoContinuityId(Long.parseLong(continuityStringId.trim()));
				
				inciso = (BitemporalInciso) CollectionUtils.find(incisos, new Predicate() {
								@Override
								public boolean evaluate(Object arg) {
									BitemporalInciso incisoEval = (BitemporalInciso) arg;
									boolean eval = incisoEval.getContinuity().getId().equals(ListadoIncisosDinamicoServiceImpl.getIncisoContinuityId());
									return eval;
								}
							});
				
				if (inciso != null) {
					if (!quitarSeleccion && !inciso.getValue().getChecked()) {
						inciso.getValue().setChecked(true);
						//System.out.println("AAC Checo -->" + continuityStringId);
					} else if (quitarSeleccion && inciso.getValue().getChecked()) {
						inciso.getValue().setChecked(false);
						//System.out.println("AAC Des-Checo -->" + continuityStringId);
					}
				} 
				//else {
				//	System.out.println("AAC No se encontro el inciso!! -->" + continuityStringId);
				//}
			}
		}
		
		return incisos;
		
	}
	
	public Long getNumeroIncisos(Long cotizacionContinuityId, DateTime validFromDT, DateTime recordFromDT, boolean getCancelados){
		return listadoIncisosDinamicoDao.getNumeroIncisos(cotizacionContinuityId, validFromDT, recordFromDT, getCancelados);
	}
	/**
	 * 
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	public List<ReferenciaBancariaDTO> getReferenciasBancarias(BigDecimal idToPoliza){
		return listadoIncisosDinamicoDao.getReferenciasBancarias(idToPoliza);
	}
	
	/**
	 * Obtiene un resumen de las lineas de negocio con datos como Número de
	 * incisos y prima neta de una póliza a cierta fecha. Se creó para optimizar
	 * el tiempo de respuesta al momento de imprimir una póliza con muchos
	 * incisos, antes se calculaba todo en java.
	 * 
	 * @param idToPoliza
	 * @param numeroEndoso
	 * @return
	 */
	public List<DatosLineasDeNegocioDTO> getDatosLineaNegocio(BigDecimal idToPoliza, Short numeroEndoso){
		return listadoIncisosDinamicoDao.getDatosLineaNegocio(idToPoliza, numeroEndoso);
		
	}
	
}
